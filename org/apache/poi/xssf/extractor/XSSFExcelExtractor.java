// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.xssf.extractor;

import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.ss.usermodel.Comment;
import java.util.Iterator;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.HeaderFooter;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.POIXMLDocument;
import org.apache.poi.openxml4j.opc.OPCPackage;
import java.io.IOException;
import org.apache.poi.openxml4j.exceptions.OpenXML4JException;
import org.apache.xmlbeans.XmlException;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import java.util.Locale;
import org.apache.poi.xssf.usermodel.XSSFRelation;
import org.apache.poi.ss.extractor.ExcelExtractor;
import org.apache.poi.POIXMLTextExtractor;

public class XSSFExcelExtractor extends POIXMLTextExtractor implements ExcelExtractor
{
    public static final XSSFRelation[] SUPPORTED_TYPES;
    private Locale locale;
    private XSSFWorkbook workbook;
    private boolean includeSheetNames;
    private boolean formulasNotResults;
    private boolean includeCellComments;
    private boolean includeHeadersFooters;
    
    @Deprecated
    public XSSFExcelExtractor(final String path) throws XmlException, OpenXML4JException, IOException {
        this(new XSSFWorkbook(path));
    }
    
    public XSSFExcelExtractor(final OPCPackage container) throws XmlException, OpenXML4JException, IOException {
        this(new XSSFWorkbook(container));
    }
    
    public XSSFExcelExtractor(final XSSFWorkbook workbook) {
        super(workbook);
        this.includeSheetNames = true;
        this.formulasNotResults = false;
        this.includeCellComments = false;
        this.includeHeadersFooters = true;
        this.workbook = workbook;
    }
    
    public static void main(final String[] args) throws Exception {
        if (args.length < 1) {
            System.err.println("Use:");
            System.err.println("  XSSFExcelExtractor <filename.xlsx>");
            System.exit(1);
        }
        final POIXMLTextExtractor extractor = new XSSFExcelExtractor(args[0]);
        System.out.println(extractor.getText());
    }
    
    public void setIncludeSheetNames(final boolean includeSheetNames) {
        this.includeSheetNames = includeSheetNames;
    }
    
    public void setFormulasNotResults(final boolean formulasNotResults) {
        this.formulasNotResults = formulasNotResults;
    }
    
    public void setIncludeCellComments(final boolean includeCellComments) {
        this.includeCellComments = includeCellComments;
    }
    
    public void setIncludeHeadersFooters(final boolean includeHeadersFooters) {
        this.includeHeadersFooters = includeHeadersFooters;
    }
    
    public void setLocale(final Locale locale) {
        this.locale = locale;
    }
    
    @Override
    public String getText() {
        DataFormatter formatter;
        if (this.locale == null) {
            formatter = new DataFormatter();
        }
        else {
            formatter = new DataFormatter(this.locale);
        }
        final StringBuffer text = new StringBuffer();
        for (int i = 0; i < this.workbook.getNumberOfSheets(); ++i) {
            final XSSFSheet sheet = this.workbook.getSheetAt(i);
            if (this.includeSheetNames) {
                text.append(this.workbook.getSheetName(i)).append("\n");
            }
            if (this.includeHeadersFooters) {
                text.append(this.extractHeaderFooter(sheet.getFirstHeader()));
                text.append(this.extractHeaderFooter(sheet.getOddHeader()));
                text.append(this.extractHeaderFooter(sheet.getEvenHeader()));
            }
            for (final Object rawR : sheet) {
                final Row row = (Row)rawR;
                final Iterator<Cell> ri = row.cellIterator();
                while (ri.hasNext()) {
                    final Cell cell = ri.next();
                    if (cell.getCellType() == 2) {
                        if (this.formulasNotResults) {
                            text.append(cell.getCellFormula());
                        }
                        else if (cell.getCachedFormulaResultType() == 1) {
                            this.handleStringCell(text, cell);
                        }
                        else {
                            this.handleNonStringCell(text, cell, formatter);
                        }
                    }
                    else if (cell.getCellType() == 1) {
                        this.handleStringCell(text, cell);
                    }
                    else {
                        this.handleNonStringCell(text, cell, formatter);
                    }
                    final Comment comment = cell.getCellComment();
                    if (this.includeCellComments && comment != null) {
                        final String commentText = comment.getString().getString().replace('\n', ' ');
                        text.append(" Comment by ").append(comment.getAuthor()).append(": ").append(commentText);
                    }
                    if (ri.hasNext()) {
                        text.append("\t");
                    }
                }
                text.append("\n");
            }
            if (this.includeHeadersFooters) {
                text.append(this.extractHeaderFooter(sheet.getFirstFooter()));
                text.append(this.extractHeaderFooter(sheet.getOddFooter()));
                text.append(this.extractHeaderFooter(sheet.getEvenFooter()));
            }
        }
        return text.toString();
    }
    
    private void handleStringCell(final StringBuffer text, final Cell cell) {
        text.append(cell.getRichStringCellValue().getString());
    }
    
    private void handleNonStringCell(final StringBuffer text, final Cell cell, final DataFormatter formatter) {
        int type = cell.getCellType();
        if (type == 2) {
            type = cell.getCachedFormulaResultType();
        }
        if (type == 0) {
            final CellStyle cs = cell.getCellStyle();
            if (cs.getDataFormatString() != null) {
                text.append(formatter.formatRawCellContents(cell.getNumericCellValue(), cs.getDataFormat(), cs.getDataFormatString()));
                return;
            }
        }
        final XSSFCell xcell = (XSSFCell)cell;
        text.append(xcell.getRawValue());
    }
    
    private String extractHeaderFooter(final HeaderFooter hf) {
        return org.apache.poi.hssf.extractor.ExcelExtractor._extractHeaderFooter(hf);
    }
    
    static {
        SUPPORTED_TYPES = new XSSFRelation[] { XSSFRelation.WORKBOOK, XSSFRelation.MACRO_TEMPLATE_WORKBOOK, XSSFRelation.MACRO_ADDIN_WORKBOOK, XSSFRelation.TEMPLATE_WORKBOOK, XSSFRelation.MACROS_WORKBOOK };
    }
}
