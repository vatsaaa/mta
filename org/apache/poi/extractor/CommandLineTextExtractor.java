// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.extractor;

import org.apache.poi.POITextExtractor;
import java.io.File;

public class CommandLineTextExtractor
{
    public static String DIVIDER;
    
    public static void main(final String[] args) throws Exception {
        if (args.length < 1) {
            System.err.println("Use:");
            System.err.println("   CommandLineTextExtractor <filename> [filename] [filename]");
            System.exit(1);
        }
        for (int i = 0; i < args.length; ++i) {
            System.out.println(CommandLineTextExtractor.DIVIDER);
            final File f = new File(args[i]);
            System.out.println(f);
            final POITextExtractor extractor = ExtractorFactory.createExtractor(f);
            final POITextExtractor metadataExtractor = extractor.getMetadataTextExtractor();
            System.out.println("   " + CommandLineTextExtractor.DIVIDER);
            System.out.println(metadataExtractor.getText());
            System.out.println("   " + CommandLineTextExtractor.DIVIDER);
            System.out.println(extractor.getText());
            System.out.println(CommandLineTextExtractor.DIVIDER);
        }
    }
    
    static {
        CommandLineTextExtractor.DIVIDER = "=======================";
    }
}
