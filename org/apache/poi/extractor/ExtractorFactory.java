// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.extractor;

import org.apache.poi.hsmf.datatypes.AttachmentChunks;
import org.apache.poi.hsmf.MAPIMessage;
import java.io.ByteArrayInputStream;
import java.io.FileNotFoundException;
import org.apache.poi.poifs.filesystem.DirectoryEntry;
import java.util.ArrayList;
import java.util.Iterator;
import org.apache.poi.poifs.filesystem.Entry;
import org.apache.poi.hsmf.extractor.OutlookTextExtactor;
import org.apache.poi.hpbf.extractor.PublisherTextExtractor;
import org.apache.poi.hdgf.extractor.VisioTextExtractor;
import org.apache.poi.hslf.extractor.PowerPointExtractor;
import org.apache.poi.hwpf.OldWordFileFormatException;
import org.apache.poi.hwpf.extractor.Word6Extractor;
import org.apache.poi.hwpf.extractor.WordExtractor;
import org.apache.poi.hssf.extractor.ExcelExtractor;
import org.apache.poi.hssf.extractor.EventBasedExcelExtractor;
import org.apache.poi.poifs.filesystem.DirectoryNode;
import org.apache.poi.POIOLE2TextExtractor;
import org.apache.poi.xslf.usermodel.XSLFRelation;
import org.apache.poi.xwpf.usermodel.XWPFRelation;
import org.apache.poi.xssf.usermodel.XSSFRelation;
import org.apache.poi.openxml4j.opc.PackagePart;
import org.apache.poi.openxml4j.opc.PackageRelationshipCollection;
import org.apache.poi.xslf.extractor.XSLFPowerPointExtractor;
import org.apache.poi.xwpf.extractor.XWPFWordExtractor;
import org.apache.poi.xssf.extractor.XSSFEventBasedExcelExtractor;
import org.apache.poi.xssf.extractor.XSSFExcelExtractor;
import org.apache.poi.POIXMLTextExtractor;
import org.apache.xmlbeans.XmlException;
import org.apache.poi.openxml4j.exceptions.OpenXML4JException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import java.io.IOException;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.POIXMLDocument;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import java.io.InputStream;
import java.io.PushbackInputStream;
import java.io.FileInputStream;
import org.apache.poi.POITextExtractor;
import java.io.File;

public class ExtractorFactory
{
    public static final String CORE_DOCUMENT_REL = "http://schemas.openxmlformats.org/officeDocument/2006/relationships/officeDocument";
    private static final ThreadLocal<Boolean> threadPreferEventExtractors;
    private static Boolean allPreferEventExtractors;
    
    public static boolean getThreadPrefersEventExtractors() {
        return ExtractorFactory.threadPreferEventExtractors.get();
    }
    
    public static Boolean getAllThreadsPreferEventExtractors() {
        return ExtractorFactory.allPreferEventExtractors;
    }
    
    public static void setThreadPrefersEventExtractors(final boolean preferEventExtractors) {
        ExtractorFactory.threadPreferEventExtractors.set(preferEventExtractors);
    }
    
    public static void setAllThreadsPreferEventExtractors(final Boolean preferEventExtractors) {
        ExtractorFactory.allPreferEventExtractors = preferEventExtractors;
    }
    
    protected static boolean getPreferEventExtractor() {
        if (ExtractorFactory.allPreferEventExtractors != null) {
            return ExtractorFactory.allPreferEventExtractors;
        }
        return ExtractorFactory.threadPreferEventExtractors.get();
    }
    
    public static POITextExtractor createExtractor(final File f) throws IOException, InvalidFormatException, OpenXML4JException, XmlException {
        InputStream inp = null;
        try {
            inp = new PushbackInputStream(new FileInputStream(f), 8);
            if (POIFSFileSystem.hasPOIFSHeader(inp)) {
                return createExtractor(new POIFSFileSystem(inp));
            }
            if (POIXMLDocument.hasOOXMLHeader(inp)) {
                return createExtractor(OPCPackage.open(f.toString()));
            }
            throw new IllegalArgumentException("Your File was neither an OLE2 file, nor an OOXML file");
        }
        finally {
            if (inp != null) {
                inp.close();
            }
        }
    }
    
    public static POITextExtractor createExtractor(InputStream inp) throws IOException, InvalidFormatException, OpenXML4JException, XmlException {
        if (!inp.markSupported()) {
            inp = new PushbackInputStream(inp, 8);
        }
        if (POIFSFileSystem.hasPOIFSHeader(inp)) {
            return createExtractor(new POIFSFileSystem(inp));
        }
        if (POIXMLDocument.hasOOXMLHeader(inp)) {
            return createExtractor(OPCPackage.open(inp));
        }
        throw new IllegalArgumentException("Your InputStream was neither an OLE2 stream, nor an OOXML stream");
    }
    
    public static POIXMLTextExtractor createExtractor(final OPCPackage pkg) throws IOException, OpenXML4JException, XmlException {
        final PackageRelationshipCollection core = pkg.getRelationshipsByType("http://schemas.openxmlformats.org/officeDocument/2006/relationships/officeDocument");
        if (core.size() != 1) {
            throw new IllegalArgumentException("Invalid OOXML Package received - expected 1 core document, found " + core.size());
        }
        final PackagePart corePart = pkg.getPart(core.getRelationship(0));
        final XSSFRelation[] arr$ = XSSFExcelExtractor.SUPPORTED_TYPES;
        final int len$ = arr$.length;
        int i$ = 0;
        while (i$ < len$) {
            final XSSFRelation rel = arr$[i$];
            if (corePart.getContentType().equals(rel.getContentType())) {
                if (getPreferEventExtractor()) {
                    return new XSSFEventBasedExcelExtractor(pkg);
                }
                return new XSSFExcelExtractor(pkg);
            }
            else {
                ++i$;
            }
        }
        for (final XWPFRelation rel2 : XWPFWordExtractor.SUPPORTED_TYPES) {
            if (corePart.getContentType().equals(rel2.getContentType())) {
                return new XWPFWordExtractor(pkg);
            }
        }
        for (final XSLFRelation rel3 : XSLFPowerPointExtractor.SUPPORTED_TYPES) {
            if (corePart.getContentType().equals(rel3.getContentType())) {
                return new XSLFPowerPointExtractor(pkg);
            }
        }
        throw new IllegalArgumentException("No supported documents found in the OOXML package (found " + corePart.getContentType() + ")");
    }
    
    public static POIOLE2TextExtractor createExtractor(final POIFSFileSystem fs) throws IOException, InvalidFormatException, OpenXML4JException, XmlException {
        return (POIOLE2TextExtractor)createExtractor(fs.getRoot());
    }
    
    @Deprecated
    public static POITextExtractor createExtractor(final DirectoryNode poifsDir, final POIFSFileSystem fs) throws IOException, InvalidFormatException, OpenXML4JException, XmlException {
        return createExtractor(poifsDir);
    }
    
    public static POITextExtractor createExtractor(final DirectoryNode poifsDir) throws IOException, InvalidFormatException, OpenXML4JException, XmlException {
        if (poifsDir.hasEntry("Workbook")) {
            if (getPreferEventExtractor()) {
                return new EventBasedExcelExtractor(poifsDir);
            }
            return new ExcelExtractor(poifsDir);
        }
        else {
            if (poifsDir.hasEntry("WordDocument")) {
                try {
                    return new WordExtractor(poifsDir);
                }
                catch (OldWordFileFormatException e) {
                    return new Word6Extractor(poifsDir);
                }
            }
            if (poifsDir.hasEntry("PowerPoint Document")) {
                return new PowerPointExtractor(poifsDir);
            }
            if (poifsDir.hasEntry("VisioDocument")) {
                return new VisioTextExtractor(poifsDir);
            }
            if (poifsDir.hasEntry("Quill")) {
                return new PublisherTextExtractor(poifsDir);
            }
            if (poifsDir.hasEntry("__substg1.0_1000001E") || poifsDir.hasEntry("__substg1.0_1000001F") || poifsDir.hasEntry("__substg1.0_0047001E") || poifsDir.hasEntry("__substg1.0_0047001F") || poifsDir.hasEntry("__substg1.0_0037001E") || poifsDir.hasEntry("__substg1.0_0037001F")) {
                return new OutlookTextExtactor(poifsDir);
            }
            final Iterator<Entry> entries = poifsDir.getEntries();
            while (entries.hasNext()) {
                final Entry entry = entries.next();
                if (entry.getName().equals("Package")) {
                    final OPCPackage pkg = OPCPackage.open(poifsDir.createDocumentInputStream("Package"));
                    return createExtractor(pkg);
                }
            }
            throw new IllegalArgumentException("No supported documents found in the OLE2 stream");
        }
    }
    
    public static POITextExtractor[] getEmbededDocsTextExtractors(final POIOLE2TextExtractor ext) throws IOException, InvalidFormatException, OpenXML4JException, XmlException {
        final ArrayList<Entry> dirs = new ArrayList<Entry>();
        final ArrayList<InputStream> nonPOIFS = new ArrayList<InputStream>();
        final DirectoryEntry root = ext.getRoot();
        if (root == null) {
            throw new IllegalStateException("The extractor didn't know which POIFS it came from!");
        }
        if (ext instanceof ExcelExtractor) {
            final Iterator<Entry> it = root.getEntries();
            while (it.hasNext()) {
                final Entry entry = it.next();
                if (entry.getName().startsWith("MBD")) {
                    dirs.add(entry);
                }
            }
        }
        else if (ext instanceof WordExtractor) {
            try {
                final DirectoryEntry op = (DirectoryEntry)root.getEntry("ObjectPool");
                final Iterator<Entry> it2 = op.getEntries();
                while (it2.hasNext()) {
                    final Entry entry2 = it2.next();
                    if (entry2.getName().startsWith("_")) {
                        dirs.add(entry2);
                    }
                }
            }
            catch (FileNotFoundException e2) {}
        }
        else if (!(ext instanceof PowerPointExtractor)) {
            if (ext instanceof OutlookTextExtactor) {
                final MAPIMessage msg = ((OutlookTextExtactor)ext).getMAPIMessage();
                for (final AttachmentChunks attachment : msg.getAttachmentFiles()) {
                    if (attachment.attachData != null) {
                        final byte[] data = attachment.attachData.getValue();
                        nonPOIFS.add(new ByteArrayInputStream(data));
                    }
                    else if (attachment.attachmentDirectory != null) {
                        dirs.add(attachment.attachmentDirectory.getDirectory());
                    }
                }
            }
        }
        if ((dirs == null || dirs.size() == 0) && (nonPOIFS == null || nonPOIFS.size() == 0)) {
            return new POITextExtractor[0];
        }
        final ArrayList<POITextExtractor> e = new ArrayList<POITextExtractor>();
        for (int i = 0; i < dirs.size(); ++i) {
            e.add(createExtractor(dirs.get(i)));
        }
        for (int i = 0; i < nonPOIFS.size(); ++i) {
            try {
                e.add(createExtractor(nonPOIFS.get(i)));
            }
            catch (IllegalArgumentException ie) {}
            catch (XmlException xe) {
                throw new IOException(xe.getMessage());
            }
            catch (OpenXML4JException oe) {
                throw new IOException(oe.getMessage());
            }
        }
        return e.toArray(new POITextExtractor[e.size()]);
    }
    
    public static POITextExtractor[] getEmbededDocsTextExtractors(final POIXMLTextExtractor ext) {
        throw new IllegalStateException("Not yet supported");
    }
    
    static {
        threadPreferEventExtractors = new ThreadLocal<Boolean>() {
            @Override
            protected Boolean initialValue() {
                return Boolean.FALSE;
            }
        };
    }
}
