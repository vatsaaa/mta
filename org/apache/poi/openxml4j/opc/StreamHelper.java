// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.openxml4j.opc;

import java.io.InputStream;
import org.dom4j.io.XMLWriter;
import org.dom4j.io.OutputFormat;
import java.io.OutputStream;
import org.dom4j.Document;

public final class StreamHelper
{
    private StreamHelper() {
    }
    
    public static boolean saveXmlInStream(final Document xmlContent, final OutputStream outStream) {
        try {
            final OutputFormat outformat = OutputFormat.createPrettyPrint();
            outformat.setEncoding("UTF-8");
            final XMLWriter writer = new XMLWriter(outStream, outformat);
            writer.write(xmlContent);
        }
        catch (Exception e) {
            return false;
        }
        return true;
    }
    
    public static boolean copyStream(final InputStream inStream, final OutputStream outStream) {
        try {
            final byte[] buffer = new byte[1024];
            int bytesRead;
            while ((bytesRead = inStream.read(buffer)) >= 0) {
                outStream.write(buffer, 0, bytesRead);
            }
        }
        catch (Exception e) {
            return false;
        }
        return true;
    }
}
