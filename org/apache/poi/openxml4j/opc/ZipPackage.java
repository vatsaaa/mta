// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.openxml4j.opc;

import java.util.TreeMap;
import org.apache.poi.util.POILogFactory;
import java.util.Iterator;
import org.apache.poi.openxml4j.exceptions.OpenXML4JRuntimeException;
import org.apache.poi.openxml4j.exceptions.OpenXML4JException;
import org.apache.poi.openxml4j.opc.internal.PartMarshaller;
import org.apache.poi.openxml4j.opc.internal.marshallers.ZipPartMarshaller;
import org.apache.poi.openxml4j.opc.internal.marshallers.ZipPackagePropertiesMarshaller;
import java.util.zip.ZipOutputStream;
import java.io.OutputStream;
import org.apache.poi.openxml4j.opc.internal.FileHelper;
import java.io.File;
import org.apache.poi.openxml4j.opc.internal.MemoryPackagePart;
import java.util.Enumeration;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.openxml4j.opc.internal.ZipContentTypeManager;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import org.apache.poi.openxml4j.util.ZipFileZipEntrySource;
import org.apache.poi.openxml4j.exceptions.InvalidOperationException;
import org.apache.poi.openxml4j.opc.internal.ZipHelper;
import java.io.IOException;
import org.apache.poi.openxml4j.util.ZipInputStreamZipEntrySource;
import java.util.zip.ZipInputStream;
import java.io.InputStream;
import org.apache.poi.openxml4j.util.ZipEntrySource;
import org.apache.poi.util.POILogger;

public final class ZipPackage extends Package
{
    private static POILogger logger;
    private final ZipEntrySource zipArchive;
    
    public ZipPackage() {
        super(ZipPackage.defaultPackageAccess);
        this.zipArchive = null;
    }
    
    ZipPackage(final InputStream in, final PackageAccess access) throws IOException {
        super(access);
        this.zipArchive = new ZipInputStreamZipEntrySource(new ZipInputStream(in));
    }
    
    ZipPackage(final String path, final PackageAccess access) {
        super(access);
        final ZipFile zipFile = ZipHelper.openZipFile(path);
        if (zipFile == null) {
            throw new InvalidOperationException("Can't open the specified file: '" + path + "'");
        }
        this.zipArchive = new ZipFileZipEntrySource(zipFile);
    }
    
    @Override
    protected PackagePart[] getPartsImpl() throws InvalidFormatException {
        if (this.partList == null) {
            this.partList = new PackagePartCollection();
        }
        if (this.zipArchive == null) {
            return ((TreeMap<K, PackagePart>)this.partList).values().toArray(new PackagePart[((TreeMap<K, PackagePart>)this.partList).values().size()]);
        }
        Enumeration<? extends ZipEntry> entries = this.zipArchive.getEntries();
        while (entries.hasMoreElements()) {
            final ZipEntry entry = (ZipEntry)entries.nextElement();
            if (entry.getName().equalsIgnoreCase("[Content_Types].xml")) {
                try {
                    this.contentTypeManager = new ZipContentTypeManager(this.getZipArchive().getInputStream(entry), this);
                    break;
                }
                catch (IOException e) {
                    throw new InvalidFormatException(e.getMessage());
                }
            }
        }
        if (this.contentTypeManager == null) {
            throw new InvalidFormatException("Package should contain a content type part [M1.13]");
        }
        entries = this.zipArchive.getEntries();
        while (entries.hasMoreElements()) {
            final ZipEntry entry = (ZipEntry)entries.nextElement();
            final PackagePartName partName = this.buildPartName(entry);
            if (partName == null) {
                continue;
            }
            final String contentType = this.contentTypeManager.getContentType(partName);
            if (contentType == null || !contentType.equals("application/vnd.openxmlformats-package.relationships+xml")) {
                continue;
            }
            try {
                this.partList.put(partName, new ZipPackagePart(this, entry, partName, contentType));
            }
            catch (InvalidOperationException e2) {
                throw new InvalidFormatException(e2.getMessage());
            }
        }
        entries = this.zipArchive.getEntries();
        while (entries.hasMoreElements()) {
            final ZipEntry entry = (ZipEntry)entries.nextElement();
            final PackagePartName partName = this.buildPartName(entry);
            if (partName == null) {
                continue;
            }
            final String contentType = this.contentTypeManager.getContentType(partName);
            if (contentType != null && contentType.equals("application/vnd.openxmlformats-package.relationships+xml")) {
                continue;
            }
            if (contentType != null) {
                try {
                    this.partList.put(partName, new ZipPackagePart(this, entry, partName, contentType));
                    continue;
                }
                catch (InvalidOperationException e2) {
                    throw new InvalidFormatException(e2.getMessage());
                }
            }
            throw new InvalidFormatException("The part " + partName.getURI().getPath() + " does not have any content type ! Rule: Package require content types when retrieving a part from a package. [M.1.14]");
        }
        return ((TreeMap<K, PackagePart>)this.partList).values().toArray(new ZipPackagePart[this.partList.size()]);
    }
    
    private PackagePartName buildPartName(final ZipEntry entry) {
        try {
            if (entry.getName().equalsIgnoreCase("[Content_Types].xml")) {
                return null;
            }
            return PackagingURIHelper.createPartName(ZipHelper.getOPCNameFromZipItemName(entry.getName()));
        }
        catch (Exception e) {
            ZipPackage.logger.log(POILogger.WARN, "Entry " + entry.getName() + " is not valid, so this part won't be add to the package.", e);
            return null;
        }
    }
    
    @Override
    protected PackagePart createPartImpl(final PackagePartName partName, final String contentType, final boolean loadRelationships) {
        if (contentType == null) {
            throw new IllegalArgumentException("contentType");
        }
        if (partName == null) {
            throw new IllegalArgumentException("partName");
        }
        try {
            return new MemoryPackagePart(this, partName, contentType, loadRelationships);
        }
        catch (InvalidFormatException e) {
            ZipPackage.logger.log(POILogger.WARN, e);
            return null;
        }
    }
    
    @Override
    protected void removePartImpl(final PackagePartName partName) {
        if (partName == null) {
            throw new IllegalArgumentException("partUri");
        }
    }
    
    @Override
    protected void flushImpl() {
    }
    
    @Override
    protected void closeImpl() throws IOException {
        this.flush();
        if (this.originalPackagePath != null && !"".equals(this.originalPackagePath)) {
            final File targetFile = new File(this.originalPackagePath);
            if (!targetFile.exists()) {
                throw new InvalidOperationException("Can't close a package not previously open with the open() method !");
            }
            final File tempFile = File.createTempFile(this.generateTempFileName(FileHelper.getDirectory(targetFile)), ".tmp");
            try {
                this.save(tempFile);
                this.zipArchive.close();
                FileHelper.copyFile(tempFile, targetFile);
            }
            finally {
                if (!tempFile.delete()) {
                    ZipPackage.logger.log(POILogger.WARN, "The temporary file: '" + targetFile.getAbsolutePath() + "' cannot be deleted ! Make sure that no other application use it.");
                }
            }
        }
    }
    
    private synchronized String generateTempFileName(final File directory) {
        File tmpFilename;
        do {
            tmpFilename = new File(directory.getAbsoluteFile() + File.separator + "OpenXML4J" + System.nanoTime());
        } while (tmpFilename.exists());
        return FileHelper.getFilename(tmpFilename.getAbsoluteFile());
    }
    
    @Override
    protected void revertImpl() {
        try {
            if (this.zipArchive != null) {
                this.zipArchive.close();
            }
        }
        catch (IOException ex) {}
    }
    
    @Override
    protected PackagePart getPartImpl(final PackagePartName partName) {
        if (this.partList.containsKey(partName)) {
            return ((TreeMap<K, PackagePart>)this.partList).get(partName);
        }
        return null;
    }
    
    public void saveImpl(final OutputStream outputStream) {
        this.throwExceptionIfReadOnly();
        ZipOutputStream zos = null;
        try {
            if (!(outputStream instanceof ZipOutputStream)) {
                zos = new ZipOutputStream(outputStream);
            }
            else {
                zos = (ZipOutputStream)outputStream;
            }
            if (this.getPartsByRelationshipType("http://schemas.openxmlformats.org/package/2006/relationships/metadata/core-properties").size() == 0 && this.getPartsByRelationshipType("http://schemas.openxmlformats.org/officedocument/2006/relationships/metadata/core-properties").size() == 0) {
                ZipPackage.logger.log(POILogger.DEBUG, "Save core properties part");
                new ZipPackagePropertiesMarshaller().marshall(this.packageProperties, zos);
                this.relationships.addRelationship(this.packageProperties.getPartName().getURI(), TargetMode.INTERNAL, "http://schemas.openxmlformats.org/package/2006/relationships/metadata/core-properties", null);
                if (!this.contentTypeManager.isContentTypeRegister("application/vnd.openxmlformats-package.core-properties+xml")) {
                    this.contentTypeManager.addContentType(this.packageProperties.getPartName(), "application/vnd.openxmlformats-package.core-properties+xml");
                }
            }
            ZipPackage.logger.log(POILogger.DEBUG, "Save package relationships");
            ZipPartMarshaller.marshallRelationshipPart(this.getRelationships(), PackagingURIHelper.PACKAGE_RELATIONSHIPS_ROOT_PART_NAME, zos);
            ZipPackage.logger.log(POILogger.DEBUG, "Save content types part");
            this.contentTypeManager.save(zos);
            for (final PackagePart part : this.getParts()) {
                if (part.isRelationshipPart()) {
                    continue;
                }
                ZipPackage.logger.log(POILogger.DEBUG, "Save part '" + ZipHelper.getZipItemNameFromOPCName(part.getPartName().getName()) + "'");
                final PartMarshaller marshaller = this.partMarshallers.get(part._contentType);
                if (marshaller != null) {
                    if (!marshaller.marshall(part, zos)) {
                        throw new OpenXML4JException("The part " + part.getPartName().getURI() + " fail to be saved in the stream with marshaller " + marshaller);
                    }
                    continue;
                }
                else {
                    if (!this.defaultPartMarshaller.marshall(part, zos)) {
                        throw new OpenXML4JException("The part " + part.getPartName().getURI() + " fail to be saved in the stream with marshaller " + this.defaultPartMarshaller);
                    }
                    continue;
                }
            }
            zos.close();
        }
        catch (Exception e) {
            throw new OpenXML4JRuntimeException("Fail to save: an error occurs while saving the package : " + e.getMessage(), e);
        }
    }
    
    public ZipEntrySource getZipArchive() {
        return this.zipArchive;
    }
    
    static {
        ZipPackage.logger = POILogFactory.getLogger(ZipPackage.class);
    }
}
