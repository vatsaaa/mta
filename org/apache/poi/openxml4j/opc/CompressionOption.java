// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.openxml4j.opc;

public enum CompressionOption
{
    FAST(1), 
    MAXIMUM(9), 
    NORMAL(-1), 
    NOT_COMPRESSED(0);
    
    private final int value;
    
    private CompressionOption(final int value) {
        this.value = value;
    }
    
    public int value() {
        return this.value;
    }
}
