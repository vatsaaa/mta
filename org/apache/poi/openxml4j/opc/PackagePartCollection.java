// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.openxml4j.opc;

import org.apache.poi.openxml4j.exceptions.InvalidOperationException;
import java.util.ArrayList;
import java.util.TreeMap;

public final class PackagePartCollection extends TreeMap<PackagePartName, PackagePart>
{
    private static final long serialVersionUID = 2515031135957635515L;
    private ArrayList<String> registerPartNameStr;
    
    public PackagePartCollection() {
        this.registerPartNameStr = new ArrayList<String>();
    }
    
    @Override
    public Object clone() {
        return super.clone();
    }
    
    @Override
    public PackagePart put(final PackagePartName partName, final PackagePart part) {
        final String[] segments = partName.getURI().toASCIIString().split(PackagingURIHelper.FORWARD_SLASH_STRING);
        final StringBuffer concatSeg = new StringBuffer();
        for (final String seg : segments) {
            if (!seg.equals("")) {
                concatSeg.append(PackagingURIHelper.FORWARD_SLASH_CHAR);
            }
            concatSeg.append(seg);
            if (this.registerPartNameStr.contains(concatSeg.toString())) {
                throw new InvalidOperationException("You can't add a part with a part name derived from another part ! [M1.11]");
            }
        }
        this.registerPartNameStr.add(partName.getName());
        return super.put(partName, part);
    }
    
    @Override
    public PackagePart remove(final Object key) {
        if (key instanceof PackagePartName) {
            this.registerPartNameStr.remove(((PackagePartName)key).getName());
        }
        return super.remove(key);
    }
}
