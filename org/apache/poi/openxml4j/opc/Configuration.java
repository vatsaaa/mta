// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.openxml4j.opc;

import java.io.File;

public final class Configuration
{
    private static String pathForXmlSchema;
    
    public static String getPathForXmlSchema() {
        return Configuration.pathForXmlSchema;
    }
    
    public static void setPathForXmlSchema(final String pathForXmlSchema) {
        Configuration.pathForXmlSchema = pathForXmlSchema;
    }
    
    static {
        Configuration.pathForXmlSchema = System.getProperty("user.dir") + File.separator + "src" + File.separator + "schemas";
    }
}
