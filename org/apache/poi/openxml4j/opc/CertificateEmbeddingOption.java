// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.openxml4j.opc;

public enum CertificateEmbeddingOption
{
    IN_CERTIFICATE_PART, 
    IN_SIGNATURE_PART, 
    NOT_EMBEDDED;
}
