// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.openxml4j.opc;

import org.apache.poi.util.POILogFactory;
import java.util.ArrayList;
import org.dom4j.Attribute;
import org.dom4j.Document;
import java.net.URISyntaxException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import java.net.URI;
import org.apache.poi.openxml4j.exceptions.InvalidOperationException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import java.util.Iterator;
import java.util.TreeMap;
import org.apache.poi.util.POILogger;

public final class PackageRelationshipCollection implements Iterable<PackageRelationship>
{
    private static POILogger logger;
    private TreeMap<String, PackageRelationship> relationshipsByID;
    private TreeMap<String, PackageRelationship> relationshipsByType;
    private PackagePart relationshipPart;
    private PackagePart sourcePart;
    private PackagePartName partName;
    private OPCPackage container;
    
    PackageRelationshipCollection() {
        this.relationshipsByID = new TreeMap<String, PackageRelationship>();
        this.relationshipsByType = new TreeMap<String, PackageRelationship>();
    }
    
    public PackageRelationshipCollection(final PackageRelationshipCollection coll, final String filter) {
        this();
        for (final PackageRelationship rel : coll.relationshipsByID.values()) {
            if (filter == null || rel.getRelationshipType().equals(filter)) {
                this.addRelationship(rel);
            }
        }
    }
    
    public PackageRelationshipCollection(final OPCPackage container) throws InvalidFormatException {
        this(container, null);
    }
    
    public PackageRelationshipCollection(final PackagePart part) throws InvalidFormatException {
        this(part._container, part);
    }
    
    public PackageRelationshipCollection(final OPCPackage container, final PackagePart part) throws InvalidFormatException {
        this();
        if (container == null) {
            throw new IllegalArgumentException("container");
        }
        if (part != null && part.isRelationshipPart()) {
            throw new IllegalArgumentException("part");
        }
        this.container = container;
        this.sourcePart = part;
        this.partName = getRelationshipPartName(part);
        if (container.getPackageAccess() != PackageAccess.WRITE && container.containPart(this.partName)) {
            this.parseRelationshipsPart(this.relationshipPart = container.getPart(this.partName));
        }
    }
    
    private static PackagePartName getRelationshipPartName(final PackagePart part) throws InvalidOperationException {
        PackagePartName partName;
        if (part == null) {
            partName = PackagingURIHelper.PACKAGE_ROOT_PART_NAME;
        }
        else {
            partName = part.getPartName();
        }
        return PackagingURIHelper.getRelationshipPartName(partName);
    }
    
    public void addRelationship(final PackageRelationship relPart) {
        this.relationshipsByID.put(relPart.getId(), relPart);
        this.relationshipsByType.put(relPart.getRelationshipType(), relPart);
    }
    
    public PackageRelationship addRelationship(final URI targetUri, final TargetMode targetMode, final String relationshipType, String id) {
        if (id == null) {
            int i = 0;
            do {
                id = "rId" + ++i;
            } while (this.relationshipsByID.get(id) != null);
        }
        final PackageRelationship rel = new PackageRelationship(this.container, this.sourcePart, targetUri, targetMode, relationshipType, id);
        this.relationshipsByID.put(rel.getId(), rel);
        this.relationshipsByType.put(rel.getRelationshipType(), rel);
        return rel;
    }
    
    public void removeRelationship(final String id) {
        if (this.relationshipsByID != null && this.relationshipsByType != null) {
            final PackageRelationship rel = this.relationshipsByID.get(id);
            if (rel != null) {
                this.relationshipsByID.remove(rel.getId());
                this.relationshipsByType.values().remove(rel);
            }
        }
    }
    
    public void removeRelationship(final PackageRelationship rel) {
        if (rel == null) {
            throw new IllegalArgumentException("rel");
        }
        this.relationshipsByID.values().remove(rel);
        this.relationshipsByType.values().remove(rel);
    }
    
    public PackageRelationship getRelationship(final int index) {
        if (index < 0 || index > this.relationshipsByID.values().size()) {
            throw new IllegalArgumentException("index");
        }
        final PackageRelationship retRel = null;
        int i = 0;
        for (final PackageRelationship rel : this.relationshipsByID.values()) {
            if (index == i++) {
                return rel;
            }
        }
        return retRel;
    }
    
    public PackageRelationship getRelationshipByID(final String id) {
        return this.relationshipsByID.get(id);
    }
    
    public int size() {
        return this.relationshipsByID.values().size();
    }
    
    private void parseRelationshipsPart(final PackagePart relPart) throws InvalidFormatException {
        try {
            final SAXReader reader = new SAXReader();
            PackageRelationshipCollection.logger.log(POILogger.DEBUG, "Parsing relationship: " + relPart.getPartName());
            final Document xmlRelationshipsDoc = reader.read(relPart.getInputStream());
            final Element root = xmlRelationshipsDoc.getRootElement();
            boolean fCorePropertiesRelationship = false;
            final Iterator i = root.elementIterator("Relationship");
            while (i.hasNext()) {
                final Element element = i.next();
                final String id = element.attribute("Id").getValue();
                final String type = element.attribute("Type").getValue();
                if (type.equals("http://schemas.openxmlformats.org/package/2006/relationships/metadata/core-properties")) {
                    if (fCorePropertiesRelationship) {
                        throw new InvalidFormatException("OPC Compliance error [M4.1]: there is more than one core properties relationship in the package !");
                    }
                    fCorePropertiesRelationship = true;
                }
                final Attribute targetModeAttr = element.attribute("TargetMode");
                TargetMode targetMode = TargetMode.INTERNAL;
                if (targetModeAttr != null) {
                    targetMode = (targetModeAttr.getValue().toLowerCase().equals("internal") ? TargetMode.INTERNAL : TargetMode.EXTERNAL);
                }
                String value = "";
                URI target;
                try {
                    value = element.attribute("Target").getValue();
                    target = PackagingURIHelper.toURI(value);
                }
                catch (URISyntaxException e) {
                    PackageRelationshipCollection.logger.log(POILogger.ERROR, "Cannot convert " + value + " in a valid relationship URI-> ignored", e);
                    continue;
                }
                this.addRelationship(target, targetMode, type, id);
            }
        }
        catch (Exception e2) {
            PackageRelationshipCollection.logger.log(POILogger.ERROR, e2);
            throw new InvalidFormatException(e2.getMessage());
        }
    }
    
    public PackageRelationshipCollection getRelationships(final String typeFilter) {
        final PackageRelationshipCollection coll = new PackageRelationshipCollection(this, typeFilter);
        return coll;
    }
    
    public Iterator<PackageRelationship> iterator() {
        return this.relationshipsByID.values().iterator();
    }
    
    public Iterator<PackageRelationship> iterator(final String typeFilter) {
        final ArrayList<PackageRelationship> retArr = new ArrayList<PackageRelationship>();
        for (final PackageRelationship rel : this.relationshipsByID.values()) {
            if (rel.getRelationshipType().equals(typeFilter)) {
                retArr.add(rel);
            }
        }
        return retArr.iterator();
    }
    
    public void clear() {
        this.relationshipsByID.clear();
        this.relationshipsByType.clear();
    }
    
    @Override
    public String toString() {
        String str;
        if (this.relationshipsByID == null) {
            str = "relationshipsByID=null";
        }
        else {
            str = this.relationshipsByID.size() + " relationship(s) = [";
        }
        if (this.relationshipPart != null && this.relationshipPart._partName != null) {
            str = str + "," + this.relationshipPart._partName;
        }
        else {
            str += ",relationshipPart=null";
        }
        if (this.sourcePart != null && this.sourcePart._partName != null) {
            str = str + "," + this.sourcePart._partName;
        }
        else {
            str += ",sourcePart=null";
        }
        if (this.partName != null) {
            str = str + "," + this.partName;
        }
        else {
            str += ",uri=null)";
        }
        return str + "]";
    }
    
    static {
        PackageRelationshipCollection.logger = POILogFactory.getLogger(PackageRelationshipCollection.class);
    }
}
