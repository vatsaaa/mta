// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.openxml4j.opc;

public enum PackageAccess
{
    READ, 
    WRITE, 
    READ_WRITE;
}
