// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.openxml4j.opc.internal.marshallers;

import java.io.IOException;
import org.apache.poi.openxml4j.exceptions.OpenXML4JException;
import org.apache.poi.openxml4j.opc.StreamHelper;
import java.util.zip.ZipEntry;
import org.apache.poi.openxml4j.opc.internal.ZipHelper;
import java.util.zip.ZipOutputStream;
import java.io.OutputStream;
import org.apache.poi.openxml4j.opc.PackagePart;

public final class ZipPackagePropertiesMarshaller extends PackagePropertiesMarshaller
{
    @Override
    public boolean marshall(final PackagePart part, final OutputStream out) throws OpenXML4JException {
        if (!(out instanceof ZipOutputStream)) {
            throw new IllegalArgumentException("ZipOutputStream expected!");
        }
        final ZipOutputStream zos = (ZipOutputStream)out;
        final ZipEntry ctEntry = new ZipEntry(ZipHelper.getZipItemNameFromOPCName(part.getPartName().getURI().toString()));
        try {
            zos.putNextEntry(ctEntry);
            super.marshall(part, out);
            if (!StreamHelper.saveXmlInStream(this.xmlDoc, out)) {
                return false;
            }
            zos.closeEntry();
        }
        catch (IOException e) {
            throw new OpenXML4JException(e.getLocalizedMessage());
        }
        return true;
    }
}
