// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.openxml4j.opc.internal.marshallers;

import org.apache.poi.openxml4j.exceptions.OpenXML4JException;
import org.dom4j.Element;
import org.dom4j.QName;
import org.dom4j.DocumentHelper;
import java.io.OutputStream;
import org.apache.poi.openxml4j.opc.PackagePart;
import org.dom4j.Document;
import org.apache.poi.openxml4j.opc.internal.PackagePropertiesPart;
import org.dom4j.Namespace;
import org.apache.poi.openxml4j.opc.internal.PartMarshaller;

public class PackagePropertiesMarshaller implements PartMarshaller
{
    private static final Namespace namespaceDC;
    private static final Namespace namespaceCoreProperties;
    private static final Namespace namespaceDcTerms;
    private static final Namespace namespaceXSI;
    protected static final String KEYWORD_CATEGORY = "category";
    protected static final String KEYWORD_CONTENT_STATUS = "contentStatus";
    protected static final String KEYWORD_CONTENT_TYPE = "contentType";
    protected static final String KEYWORD_CREATED = "created";
    protected static final String KEYWORD_CREATOR = "creator";
    protected static final String KEYWORD_DESCRIPTION = "description";
    protected static final String KEYWORD_IDENTIFIER = "identifier";
    protected static final String KEYWORD_KEYWORDS = "keywords";
    protected static final String KEYWORD_LANGUAGE = "language";
    protected static final String KEYWORD_LAST_MODIFIED_BY = "lastModifiedBy";
    protected static final String KEYWORD_LAST_PRINTED = "lastPrinted";
    protected static final String KEYWORD_MODIFIED = "modified";
    protected static final String KEYWORD_REVISION = "revision";
    protected static final String KEYWORD_SUBJECT = "subject";
    protected static final String KEYWORD_TITLE = "title";
    protected static final String KEYWORD_VERSION = "version";
    PackagePropertiesPart propsPart;
    Document xmlDoc;
    
    public PackagePropertiesMarshaller() {
        this.xmlDoc = null;
    }
    
    public boolean marshall(final PackagePart part, final OutputStream out) throws OpenXML4JException {
        if (!(part instanceof PackagePropertiesPart)) {
            throw new IllegalArgumentException("'part' must be a PackagePropertiesPart instance.");
        }
        this.propsPart = (PackagePropertiesPart)part;
        this.xmlDoc = DocumentHelper.createDocument();
        final Element rootElem = this.xmlDoc.addElement(new QName("coreProperties", PackagePropertiesMarshaller.namespaceCoreProperties));
        rootElem.addNamespace("cp", "http://schemas.openxmlformats.org/package/2006/metadata/core-properties");
        rootElem.addNamespace("dc", "http://purl.org/dc/elements/1.1/");
        rootElem.addNamespace("dcterms", "http://purl.org/dc/terms/");
        rootElem.addNamespace("xsi", "http://www.w3.org/2001/XMLSchema-instance");
        this.addCategory();
        this.addContentStatus();
        this.addContentType();
        this.addCreated();
        this.addCreator();
        this.addDescription();
        this.addIdentifier();
        this.addKeywords();
        this.addLanguage();
        this.addLastModifiedBy();
        this.addLastPrinted();
        this.addModified();
        this.addRevision();
        this.addSubject();
        this.addTitle();
        this.addVersion();
        return true;
    }
    
    private void addCategory() {
        if (!this.propsPart.getCategoryProperty().hasValue()) {
            return;
        }
        Element elem = this.xmlDoc.getRootElement().element(new QName("category", PackagePropertiesMarshaller.namespaceCoreProperties));
        if (elem == null) {
            elem = this.xmlDoc.getRootElement().addElement(new QName("category", PackagePropertiesMarshaller.namespaceCoreProperties));
        }
        else {
            elem.clearContent();
        }
        elem.addText(this.propsPart.getCategoryProperty().getValue());
    }
    
    private void addContentStatus() {
        if (!this.propsPart.getContentStatusProperty().hasValue()) {
            return;
        }
        Element elem = this.xmlDoc.getRootElement().element(new QName("contentStatus", PackagePropertiesMarshaller.namespaceCoreProperties));
        if (elem == null) {
            elem = this.xmlDoc.getRootElement().addElement(new QName("contentStatus", PackagePropertiesMarshaller.namespaceCoreProperties));
        }
        else {
            elem.clearContent();
        }
        elem.addText(this.propsPart.getContentStatusProperty().getValue());
    }
    
    private void addContentType() {
        if (!this.propsPart.getContentTypeProperty().hasValue()) {
            return;
        }
        Element elem = this.xmlDoc.getRootElement().element(new QName("contentType", PackagePropertiesMarshaller.namespaceCoreProperties));
        if (elem == null) {
            elem = this.xmlDoc.getRootElement().addElement(new QName("contentType", PackagePropertiesMarshaller.namespaceCoreProperties));
        }
        else {
            elem.clearContent();
        }
        elem.addText(this.propsPart.getContentTypeProperty().getValue());
    }
    
    private void addCreated() {
        if (!this.propsPart.getCreatedProperty().hasValue()) {
            return;
        }
        Element elem = this.xmlDoc.getRootElement().element(new QName("created", PackagePropertiesMarshaller.namespaceDcTerms));
        if (elem == null) {
            elem = this.xmlDoc.getRootElement().addElement(new QName("created", PackagePropertiesMarshaller.namespaceDcTerms));
        }
        else {
            elem.clearContent();
        }
        elem.addAttribute(new QName("type", PackagePropertiesMarshaller.namespaceXSI), "dcterms:W3CDTF");
        elem.addText(this.propsPart.getCreatedPropertyString());
    }
    
    private void addCreator() {
        if (!this.propsPart.getCreatorProperty().hasValue()) {
            return;
        }
        Element elem = this.xmlDoc.getRootElement().element(new QName("creator", PackagePropertiesMarshaller.namespaceDC));
        if (elem == null) {
            elem = this.xmlDoc.getRootElement().addElement(new QName("creator", PackagePropertiesMarshaller.namespaceDC));
        }
        else {
            elem.clearContent();
        }
        elem.addText(this.propsPart.getCreatorProperty().getValue());
    }
    
    private void addDescription() {
        if (!this.propsPart.getDescriptionProperty().hasValue()) {
            return;
        }
        Element elem = this.xmlDoc.getRootElement().element(new QName("description", PackagePropertiesMarshaller.namespaceDC));
        if (elem == null) {
            elem = this.xmlDoc.getRootElement().addElement(new QName("description", PackagePropertiesMarshaller.namespaceDC));
        }
        else {
            elem.clearContent();
        }
        elem.addText(this.propsPart.getDescriptionProperty().getValue());
    }
    
    private void addIdentifier() {
        if (!this.propsPart.getIdentifierProperty().hasValue()) {
            return;
        }
        Element elem = this.xmlDoc.getRootElement().element(new QName("identifier", PackagePropertiesMarshaller.namespaceDC));
        if (elem == null) {
            elem = this.xmlDoc.getRootElement().addElement(new QName("identifier", PackagePropertiesMarshaller.namespaceDC));
        }
        else {
            elem.clearContent();
        }
        elem.addText(this.propsPart.getIdentifierProperty().getValue());
    }
    
    private void addKeywords() {
        if (!this.propsPart.getKeywordsProperty().hasValue()) {
            return;
        }
        Element elem = this.xmlDoc.getRootElement().element(new QName("keywords", PackagePropertiesMarshaller.namespaceCoreProperties));
        if (elem == null) {
            elem = this.xmlDoc.getRootElement().addElement(new QName("keywords", PackagePropertiesMarshaller.namespaceCoreProperties));
        }
        else {
            elem.clearContent();
        }
        elem.addText(this.propsPart.getKeywordsProperty().getValue());
    }
    
    private void addLanguage() {
        if (!this.propsPart.getLanguageProperty().hasValue()) {
            return;
        }
        Element elem = this.xmlDoc.getRootElement().element(new QName("language", PackagePropertiesMarshaller.namespaceDC));
        if (elem == null) {
            elem = this.xmlDoc.getRootElement().addElement(new QName("language", PackagePropertiesMarshaller.namespaceDC));
        }
        else {
            elem.clearContent();
        }
        elem.addText(this.propsPart.getLanguageProperty().getValue());
    }
    
    private void addLastModifiedBy() {
        if (!this.propsPart.getLastModifiedByProperty().hasValue()) {
            return;
        }
        Element elem = this.xmlDoc.getRootElement().element(new QName("lastModifiedBy", PackagePropertiesMarshaller.namespaceCoreProperties));
        if (elem == null) {
            elem = this.xmlDoc.getRootElement().addElement(new QName("lastModifiedBy", PackagePropertiesMarshaller.namespaceCoreProperties));
        }
        else {
            elem.clearContent();
        }
        elem.addText(this.propsPart.getLastModifiedByProperty().getValue());
    }
    
    private void addLastPrinted() {
        if (!this.propsPart.getLastPrintedProperty().hasValue()) {
            return;
        }
        Element elem = this.xmlDoc.getRootElement().element(new QName("lastPrinted", PackagePropertiesMarshaller.namespaceCoreProperties));
        if (elem == null) {
            elem = this.xmlDoc.getRootElement().addElement(new QName("lastPrinted", PackagePropertiesMarshaller.namespaceCoreProperties));
        }
        else {
            elem.clearContent();
        }
        elem.addText(this.propsPart.getLastPrintedPropertyString());
    }
    
    private void addModified() {
        if (!this.propsPart.getModifiedProperty().hasValue()) {
            return;
        }
        Element elem = this.xmlDoc.getRootElement().element(new QName("modified", PackagePropertiesMarshaller.namespaceDcTerms));
        if (elem == null) {
            elem = this.xmlDoc.getRootElement().addElement(new QName("modified", PackagePropertiesMarshaller.namespaceDcTerms));
        }
        else {
            elem.clearContent();
        }
        elem.addAttribute(new QName("type", PackagePropertiesMarshaller.namespaceXSI), "dcterms:W3CDTF");
        elem.addText(this.propsPart.getModifiedPropertyString());
    }
    
    private void addRevision() {
        if (!this.propsPart.getRevisionProperty().hasValue()) {
            return;
        }
        Element elem = this.xmlDoc.getRootElement().element(new QName("revision", PackagePropertiesMarshaller.namespaceCoreProperties));
        if (elem == null) {
            elem = this.xmlDoc.getRootElement().addElement(new QName("revision", PackagePropertiesMarshaller.namespaceCoreProperties));
        }
        else {
            elem.clearContent();
        }
        elem.addText(this.propsPart.getRevisionProperty().getValue());
    }
    
    private void addSubject() {
        if (!this.propsPart.getSubjectProperty().hasValue()) {
            return;
        }
        Element elem = this.xmlDoc.getRootElement().element(new QName("subject", PackagePropertiesMarshaller.namespaceDC));
        if (elem == null) {
            elem = this.xmlDoc.getRootElement().addElement(new QName("subject", PackagePropertiesMarshaller.namespaceDC));
        }
        else {
            elem.clearContent();
        }
        elem.addText(this.propsPart.getSubjectProperty().getValue());
    }
    
    private void addTitle() {
        if (!this.propsPart.getTitleProperty().hasValue()) {
            return;
        }
        Element elem = this.xmlDoc.getRootElement().element(new QName("title", PackagePropertiesMarshaller.namespaceDC));
        if (elem == null) {
            elem = this.xmlDoc.getRootElement().addElement(new QName("title", PackagePropertiesMarshaller.namespaceDC));
        }
        else {
            elem.clearContent();
        }
        elem.addText(this.propsPart.getTitleProperty().getValue());
    }
    
    private void addVersion() {
        if (!this.propsPart.getVersionProperty().hasValue()) {
            return;
        }
        Element elem = this.xmlDoc.getRootElement().element(new QName("version", PackagePropertiesMarshaller.namespaceCoreProperties));
        if (elem == null) {
            elem = this.xmlDoc.getRootElement().addElement(new QName("version", PackagePropertiesMarshaller.namespaceCoreProperties));
        }
        else {
            elem.clearContent();
        }
        elem.addText(this.propsPart.getVersionProperty().getValue());
    }
    
    static {
        namespaceDC = new Namespace("dc", "http://purl.org/dc/elements/1.1/");
        namespaceCoreProperties = new Namespace("", "http://schemas.openxmlformats.org/package/2006/metadata/core-properties");
        namespaceDcTerms = new Namespace("dcterms", "http://purl.org/dc/terms/");
        namespaceXSI = new Namespace("xsi", "http://www.w3.org/2001/XMLSchema-instance");
    }
}
