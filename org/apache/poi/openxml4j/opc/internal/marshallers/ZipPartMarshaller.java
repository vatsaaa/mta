// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.openxml4j.opc.internal.marshallers;

import org.apache.poi.util.POILogFactory;
import java.util.Iterator;
import java.net.URI;
import org.dom4j.Element;
import org.dom4j.Document;
import org.apache.poi.openxml4j.opc.StreamHelper;
import org.apache.poi.openxml4j.opc.TargetMode;
import org.apache.poi.openxml4j.opc.PackageRelationship;
import org.dom4j.QName;
import org.dom4j.Namespace;
import org.dom4j.DocumentHelper;
import org.apache.poi.openxml4j.opc.PackageRelationshipCollection;
import org.apache.poi.openxml4j.opc.PackagePartName;
import java.io.InputStream;
import org.apache.poi.openxml4j.opc.PackagingURIHelper;
import java.io.IOException;
import java.util.zip.ZipEntry;
import org.apache.poi.openxml4j.opc.internal.ZipHelper;
import org.apache.poi.openxml4j.exceptions.OpenXML4JException;
import java.util.zip.ZipOutputStream;
import java.io.OutputStream;
import org.apache.poi.openxml4j.opc.PackagePart;
import org.apache.poi.util.POILogger;
import org.apache.poi.openxml4j.opc.internal.PartMarshaller;

public final class ZipPartMarshaller implements PartMarshaller
{
    private static POILogger logger;
    
    public boolean marshall(final PackagePart part, final OutputStream os) throws OpenXML4JException {
        if (!(os instanceof ZipOutputStream)) {
            ZipPartMarshaller.logger.log(POILogger.ERROR, "Unexpected class " + os.getClass().getName());
            throw new OpenXML4JException("ZipOutputStream expected !");
        }
        final ZipOutputStream zos = (ZipOutputStream)os;
        final ZipEntry partEntry = new ZipEntry(ZipHelper.getZipItemNameFromOPCName(part.getPartName().getURI().getPath()));
        try {
            zos.putNextEntry(partEntry);
            final InputStream ins = part.getInputStream();
            final byte[] buff = new byte[8192];
            while (ins.available() > 0) {
                final int resultRead = ins.read(buff);
                if (resultRead == -1) {
                    break;
                }
                zos.write(buff, 0, resultRead);
            }
            zos.closeEntry();
        }
        catch (IOException ioe) {
            ZipPartMarshaller.logger.log(POILogger.ERROR, "Cannot write: " + part.getPartName() + ": in ZIP", ioe);
            return false;
        }
        if (part.hasRelationships()) {
            final PackagePartName relationshipPartName = PackagingURIHelper.getRelationshipPartName(part.getPartName());
            marshallRelationshipPart(part.getRelationships(), relationshipPartName, zos);
        }
        return true;
    }
    
    public static boolean marshallRelationshipPart(final PackageRelationshipCollection rels, final PackagePartName relPartName, final ZipOutputStream zos) {
        final Document xmlOutDoc = DocumentHelper.createDocument();
        final Namespace dfNs = Namespace.get("", "http://schemas.openxmlformats.org/package/2006/relationships");
        final Element root = xmlOutDoc.addElement(new QName("Relationships", dfNs));
        final URI sourcePartURI = PackagingURIHelper.getSourcePartUriFromRelationshipPartUri(relPartName.getURI());
        for (final PackageRelationship rel : rels) {
            final Element relElem = root.addElement("Relationship");
            relElem.addAttribute("Id", rel.getId());
            relElem.addAttribute("Type", rel.getRelationshipType());
            final URI uri = rel.getTargetURI();
            String targetValue;
            if (rel.getTargetMode() == TargetMode.EXTERNAL) {
                targetValue = uri.toString();
                relElem.addAttribute("TargetMode", "External");
            }
            else {
                final URI targetURI = rel.getTargetURI();
                targetValue = PackagingURIHelper.relativizeURI(sourcePartURI, targetURI, true).toString();
            }
            relElem.addAttribute("Target", targetValue);
        }
        xmlOutDoc.normalize();
        final ZipEntry ctEntry = new ZipEntry(ZipHelper.getZipURIFromOPCName(relPartName.getURI().toASCIIString()).getPath());
        try {
            zos.putNextEntry(ctEntry);
            if (!StreamHelper.saveXmlInStream(xmlOutDoc, zos)) {
                return false;
            }
            zos.closeEntry();
        }
        catch (IOException e) {
            ZipPartMarshaller.logger.log(POILogger.ERROR, "Cannot create zip entry " + relPartName, e);
            return false;
        }
        return true;
    }
    
    static {
        ZipPartMarshaller.logger = POILogFactory.getLogger(ZipPartMarshaller.class);
    }
}
