// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.openxml4j.opc.internal;

import java.io.IOException;
import java.io.File;
import java.util.zip.ZipFile;
import java.net.URISyntaxException;
import java.net.URI;
import java.util.Enumeration;
import org.apache.poi.openxml4j.opc.PackageRelationship;
import java.util.zip.ZipEntry;
import org.apache.poi.openxml4j.opc.ZipPackage;

public final class ZipHelper
{
    private static final String FORWARD_SLASH = "/";
    public static final int READ_WRITE_FILE_BUFFER_SIZE = 8192;
    
    private ZipHelper() {
    }
    
    public static ZipEntry getCorePropertiesZipEntry(final ZipPackage pkg) {
        final PackageRelationship corePropsRel = pkg.getRelationshipsByType("http://schemas.openxmlformats.org/package/2006/relationships/metadata/core-properties").getRelationship(0);
        if (corePropsRel == null) {
            return null;
        }
        return new ZipEntry(corePropsRel.getTargetURI().getPath());
    }
    
    public static ZipEntry getContentTypeZipEntry(final ZipPackage pkg) {
        final Enumeration entries = pkg.getZipArchive().getEntries();
        while (entries.hasMoreElements()) {
            final ZipEntry entry = entries.nextElement();
            if (entry.getName().equals("[Content_Types].xml")) {
                return entry;
            }
        }
        return null;
    }
    
    public static String getOPCNameFromZipItemName(final String zipItemName) {
        if (zipItemName == null) {
            throw new IllegalArgumentException("zipItemName");
        }
        if (zipItemName.startsWith("/")) {
            return zipItemName;
        }
        return "/" + zipItemName;
    }
    
    public static String getZipItemNameFromOPCName(final String opcItemName) {
        if (opcItemName == null) {
            throw new IllegalArgumentException("opcItemName");
        }
        String retVal;
        for (retVal = opcItemName; retVal.startsWith("/"); retVal = retVal.substring(1)) {}
        return retVal;
    }
    
    public static URI getZipURIFromOPCName(final String opcItemName) {
        if (opcItemName == null) {
            throw new IllegalArgumentException("opcItemName");
        }
        String retVal;
        for (retVal = opcItemName; retVal.startsWith("/"); retVal = retVal.substring(1)) {}
        try {
            return new URI(retVal);
        }
        catch (URISyntaxException e) {
            return null;
        }
    }
    
    public static ZipFile openZipFile(final String path) {
        final File f = new File(path);
        try {
            if (!f.exists()) {
                return null;
            }
            return new ZipFile(f);
        }
        catch (IOException ioe) {
            return null;
        }
    }
}
