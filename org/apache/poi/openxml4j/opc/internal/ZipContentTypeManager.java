// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.openxml4j.opc.internal;

import org.apache.poi.util.POILogFactory;
import java.io.IOException;
import java.io.ByteArrayInputStream;
import org.apache.poi.openxml4j.opc.StreamHelper;
import java.io.ByteArrayOutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import java.io.OutputStream;
import org.dom4j.Document;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.openxml4j.opc.OPCPackage;
import java.io.InputStream;
import org.apache.poi.util.POILogger;

public class ZipContentTypeManager extends ContentTypeManager
{
    private static POILogger logger;
    
    public ZipContentTypeManager(final InputStream in, final OPCPackage pkg) throws InvalidFormatException {
        super(in, pkg);
    }
    
    @Override
    public boolean saveImpl(final Document content, final OutputStream out) {
        ZipOutputStream zos = null;
        if (out instanceof ZipOutputStream) {
            zos = (ZipOutputStream)out;
        }
        else {
            zos = new ZipOutputStream(out);
        }
        final ZipEntry partEntry = new ZipEntry("[Content_Types].xml");
        try {
            zos.putNextEntry(partEntry);
            final ByteArrayOutputStream outTemp = new ByteArrayOutputStream();
            StreamHelper.saveXmlInStream(content, out);
            final InputStream ins = new ByteArrayInputStream(outTemp.toByteArray());
            final byte[] buff = new byte[8192];
            while (ins.available() > 0) {
                final int resultRead = ins.read(buff);
                if (resultRead == -1) {
                    break;
                }
                zos.write(buff, 0, resultRead);
            }
            zos.closeEntry();
        }
        catch (IOException ioe) {
            ZipContentTypeManager.logger.log(POILogger.ERROR, "Cannot write: [Content_Types].xml in Zip !", ioe);
            return false;
        }
        return true;
    }
    
    static {
        ZipContentTypeManager.logger = POILogFactory.getLogger(ZipContentTypeManager.class);
    }
}
