// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.openxml4j.opc.internal;

import java.util.Map;
import org.dom4j.QName;
import org.dom4j.Namespace;
import org.dom4j.DocumentHelper;
import java.io.OutputStream;
import java.util.List;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import java.net.URISyntaxException;
import org.apache.poi.openxml4j.opc.PackagingURIHelper;
import java.net.URI;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.apache.poi.openxml4j.exceptions.OpenXML4JRuntimeException;
import java.util.Iterator;
import org.apache.poi.openxml4j.exceptions.InvalidOperationException;
import org.apache.poi.openxml4j.opc.PackagePart;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import java.io.InputStream;
import org.apache.poi.openxml4j.opc.PackagePartName;
import java.util.TreeMap;
import org.apache.poi.openxml4j.opc.OPCPackage;

public abstract class ContentTypeManager
{
    public static final String CONTENT_TYPES_PART_NAME = "[Content_Types].xml";
    public static final String TYPES_NAMESPACE_URI = "http://schemas.openxmlformats.org/package/2006/content-types";
    private static final String TYPES_TAG_NAME = "Types";
    private static final String DEFAULT_TAG_NAME = "Default";
    private static final String EXTENSION_ATTRIBUTE_NAME = "Extension";
    private static final String CONTENT_TYPE_ATTRIBUTE_NAME = "ContentType";
    private static final String OVERRIDE_TAG_NAME = "Override";
    private static final String PART_NAME_ATTRIBUTE_NAME = "PartName";
    protected OPCPackage container;
    private TreeMap<String, String> defaultContentType;
    private TreeMap<PackagePartName, String> overrideContentType;
    
    public ContentTypeManager(final InputStream in, final OPCPackage pkg) throws InvalidFormatException {
        this.container = pkg;
        this.defaultContentType = new TreeMap<String, String>();
        if (in != null) {
            try {
                this.parseContentTypesFile(in);
            }
            catch (InvalidFormatException e) {
                throw new InvalidFormatException("Can't read content types part !");
            }
        }
    }
    
    public void addContentType(final PackagePartName partName, final String contentType) {
        boolean defaultCTExists = false;
        final String extension = partName.getExtension().toLowerCase();
        if (extension.length() == 0 || (this.defaultContentType.containsKey(extension) && !(defaultCTExists = this.defaultContentType.containsValue(contentType)))) {
            this.addOverrideContentType(partName, contentType);
        }
        else if (!defaultCTExists) {
            this.addDefaultContentType(extension, contentType);
        }
    }
    
    private void addOverrideContentType(final PackagePartName partName, final String contentType) {
        if (this.overrideContentType == null) {
            this.overrideContentType = new TreeMap<PackagePartName, String>();
        }
        this.overrideContentType.put(partName, contentType);
    }
    
    private void addDefaultContentType(final String extension, final String contentType) {
        this.defaultContentType.put(extension.toLowerCase(), contentType);
    }
    
    public void removeContentType(final PackagePartName partName) throws InvalidOperationException {
        if (partName == null) {
            throw new IllegalArgumentException("partName");
        }
        if (this.overrideContentType != null && this.overrideContentType.get(partName) != null) {
            this.overrideContentType.remove(partName);
            return;
        }
        final String extensionToDelete = partName.getExtension();
        boolean deleteDefaultContentTypeFlag = true;
        if (this.container != null) {
            try {
                for (final PackagePart part : this.container.getParts()) {
                    if (!part.getPartName().equals(partName) && part.getPartName().getExtension().equalsIgnoreCase(extensionToDelete)) {
                        deleteDefaultContentTypeFlag = false;
                        break;
                    }
                }
            }
            catch (InvalidFormatException e) {
                throw new InvalidOperationException(e.getMessage());
            }
        }
        if (deleteDefaultContentTypeFlag) {
            this.defaultContentType.remove(extensionToDelete);
        }
        if (this.container != null) {
            try {
                for (final PackagePart part : this.container.getParts()) {
                    if (!part.getPartName().equals(partName) && this.getContentType(part.getPartName()) == null) {
                        throw new InvalidOperationException("Rule M2.4 is not respected: Nor a default element or override element is associated with the part: " + part.getPartName().getName());
                    }
                }
            }
            catch (InvalidFormatException e) {
                throw new InvalidOperationException(e.getMessage());
            }
        }
    }
    
    public boolean isContentTypeRegister(final String contentType) {
        if (contentType == null) {
            throw new IllegalArgumentException("contentType");
        }
        return this.defaultContentType.values().contains(contentType) || (this.overrideContentType != null && this.overrideContentType.values().contains(contentType));
    }
    
    public String getContentType(final PackagePartName partName) {
        if (partName == null) {
            throw new IllegalArgumentException("partName");
        }
        if (this.overrideContentType != null && this.overrideContentType.containsKey(partName)) {
            return this.overrideContentType.get(partName);
        }
        final String extension = partName.getExtension().toLowerCase();
        if (this.defaultContentType.containsKey(extension)) {
            return this.defaultContentType.get(extension);
        }
        if (this.container != null && this.container.getPart(partName) != null) {
            throw new OpenXML4JRuntimeException("Rule M2.4 exception : this error should NEVER happen, if so please send a mail to the developers team, thanks !");
        }
        return null;
    }
    
    public void clearAll() {
        this.defaultContentType.clear();
        if (this.overrideContentType != null) {
            this.overrideContentType.clear();
        }
    }
    
    public void clearOverrideContentTypes() {
        if (this.overrideContentType != null) {
            this.overrideContentType.clear();
        }
    }
    
    private void parseContentTypesFile(final InputStream in) throws InvalidFormatException {
        try {
            final SAXReader xmlReader = new SAXReader();
            final Document xmlContentTypetDoc = xmlReader.read(in);
            final List defaultTypes = xmlContentTypetDoc.getRootElement().elements("Default");
            for (final Element element : defaultTypes) {
                final String extension = element.attribute("Extension").getValue();
                final String contentType = element.attribute("ContentType").getValue();
                this.addDefaultContentType(extension, contentType);
            }
            final List overrideTypes = xmlContentTypetDoc.getRootElement().elements("Override");
            for (final Element element2 : overrideTypes) {
                final URI uri = new URI(element2.attribute("PartName").getValue());
                final PackagePartName partName = PackagingURIHelper.createPartName(uri);
                final String contentType2 = element2.attribute("ContentType").getValue();
                this.addOverrideContentType(partName, contentType2);
            }
        }
        catch (URISyntaxException urie) {
            throw new InvalidFormatException(urie.getMessage());
        }
        catch (DocumentException e) {
            throw new InvalidFormatException(e.getMessage());
        }
    }
    
    public boolean save(final OutputStream outStream) {
        final Document xmlOutDoc = DocumentHelper.createDocument();
        final Namespace dfNs = Namespace.get("", "http://schemas.openxmlformats.org/package/2006/content-types");
        final Element typesElem = xmlOutDoc.addElement(new QName("Types", dfNs));
        for (final Map.Entry<String, String> entry : this.defaultContentType.entrySet()) {
            this.appendDefaultType(typesElem, entry);
        }
        if (this.overrideContentType != null) {
            for (final Map.Entry<PackagePartName, String> entry2 : this.overrideContentType.entrySet()) {
                this.appendSpecificTypes(typesElem, entry2);
            }
        }
        xmlOutDoc.normalize();
        return this.saveImpl(xmlOutDoc, outStream);
    }
    
    private void appendSpecificTypes(final Element root, final Map.Entry<PackagePartName, String> entry) {
        root.addElement("Override").addAttribute("PartName", entry.getKey().getName()).addAttribute("ContentType", entry.getValue());
    }
    
    private void appendDefaultType(final Element root, final Map.Entry<String, String> entry) {
        root.addElement("Default").addAttribute("Extension", entry.getKey()).addAttribute("ContentType", entry.getValue());
    }
    
    public abstract boolean saveImpl(final Document p0, final OutputStream p1);
}
