// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.openxml4j.opc.internal.unmarshallers;

import java.util.zip.ZipEntry;
import org.apache.poi.openxml4j.opc.PackagePartName;
import org.apache.poi.openxml4j.opc.OPCPackage;

public final class UnmarshallContext
{
    private OPCPackage _package;
    private PackagePartName partName;
    private ZipEntry zipEntry;
    
    public UnmarshallContext(final OPCPackage targetPackage, final PackagePartName partName) {
        this._package = targetPackage;
        this.partName = partName;
    }
    
    OPCPackage getPackage() {
        return this._package;
    }
    
    public void setPackage(final OPCPackage container) {
        this._package = container;
    }
    
    PackagePartName getPartName() {
        return this.partName;
    }
    
    public void setPartName(final PackagePartName partName) {
        this.partName = partName;
    }
    
    ZipEntry getZipEntry() {
        return this.zipEntry;
    }
    
    public void setZipEntry(final ZipEntry zipEntry) {
        this.zipEntry = zipEntry;
    }
}
