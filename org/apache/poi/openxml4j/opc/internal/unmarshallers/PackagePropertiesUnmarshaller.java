// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.openxml4j.opc.internal.unmarshallers;

import org.dom4j.Attribute;
import java.util.Iterator;
import java.util.List;
import org.dom4j.Element;
import org.dom4j.QName;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.dom4j.Document;
import java.util.zip.ZipEntry;
import org.dom4j.DocumentException;
import org.dom4j.io.SAXReader;
import org.apache.poi.openxml4j.opc.internal.ZipHelper;
import java.io.IOException;
import org.apache.poi.openxml4j.opc.ZipPackage;
import org.apache.poi.openxml4j.opc.internal.PackagePropertiesPart;
import org.apache.poi.openxml4j.opc.PackagePart;
import java.io.InputStream;
import org.dom4j.Namespace;
import org.apache.poi.openxml4j.opc.internal.PartUnmarshaller;

public final class PackagePropertiesUnmarshaller implements PartUnmarshaller
{
    private static final Namespace namespaceDC;
    private static final Namespace namespaceCP;
    private static final Namespace namespaceDcTerms;
    private static final Namespace namespaceXML;
    private static final Namespace namespaceXSI;
    protected static final String KEYWORD_CATEGORY = "category";
    protected static final String KEYWORD_CONTENT_STATUS = "contentStatus";
    protected static final String KEYWORD_CONTENT_TYPE = "contentType";
    protected static final String KEYWORD_CREATED = "created";
    protected static final String KEYWORD_CREATOR = "creator";
    protected static final String KEYWORD_DESCRIPTION = "description";
    protected static final String KEYWORD_IDENTIFIER = "identifier";
    protected static final String KEYWORD_KEYWORDS = "keywords";
    protected static final String KEYWORD_LANGUAGE = "language";
    protected static final String KEYWORD_LAST_MODIFIED_BY = "lastModifiedBy";
    protected static final String KEYWORD_LAST_PRINTED = "lastPrinted";
    protected static final String KEYWORD_MODIFIED = "modified";
    protected static final String KEYWORD_REVISION = "revision";
    protected static final String KEYWORD_SUBJECT = "subject";
    protected static final String KEYWORD_TITLE = "title";
    protected static final String KEYWORD_VERSION = "version";
    
    public PackagePart unmarshall(final UnmarshallContext context, InputStream in) throws InvalidFormatException, IOException {
        final PackagePropertiesPart coreProps = new PackagePropertiesPart(context.getPackage(), context.getPartName());
        if (in == null) {
            if (context.getZipEntry() != null) {
                in = ((ZipPackage)context.getPackage()).getZipArchive().getInputStream(context.getZipEntry());
            }
            else {
                if (context.getPackage() == null) {
                    throw new IOException("Error while trying to get the part input stream.");
                }
                final ZipEntry zipEntry = ZipHelper.getCorePropertiesZipEntry((ZipPackage)context.getPackage());
                in = ((ZipPackage)context.getPackage()).getZipArchive().getInputStream(zipEntry);
            }
        }
        final SAXReader xmlReader = new SAXReader();
        Document xmlDoc;
        try {
            xmlDoc = xmlReader.read(in);
            this.checkElementForOPCCompliance(xmlDoc.getRootElement());
        }
        catch (DocumentException e) {
            throw new IOException(e.getMessage());
        }
        coreProps.setCategoryProperty(this.loadCategory(xmlDoc));
        coreProps.setContentStatusProperty(this.loadContentStatus(xmlDoc));
        coreProps.setContentTypeProperty(this.loadContentType(xmlDoc));
        coreProps.setCreatedProperty(this.loadCreated(xmlDoc));
        coreProps.setCreatorProperty(this.loadCreator(xmlDoc));
        coreProps.setDescriptionProperty(this.loadDescription(xmlDoc));
        coreProps.setIdentifierProperty(this.loadIdentifier(xmlDoc));
        coreProps.setKeywordsProperty(this.loadKeywords(xmlDoc));
        coreProps.setLanguageProperty(this.loadLanguage(xmlDoc));
        coreProps.setLastModifiedByProperty(this.loadLastModifiedBy(xmlDoc));
        coreProps.setLastPrintedProperty(this.loadLastPrinted(xmlDoc));
        coreProps.setModifiedProperty(this.loadModified(xmlDoc));
        coreProps.setRevisionProperty(this.loadRevision(xmlDoc));
        coreProps.setSubjectProperty(this.loadSubject(xmlDoc));
        coreProps.setTitleProperty(this.loadTitle(xmlDoc));
        coreProps.setVersionProperty(this.loadVersion(xmlDoc));
        return coreProps;
    }
    
    private String loadCategory(final Document xmlDoc) {
        final Element el = xmlDoc.getRootElement().element(new QName("category", PackagePropertiesUnmarshaller.namespaceCP));
        if (el == null) {
            return null;
        }
        return el.getStringValue();
    }
    
    private String loadContentStatus(final Document xmlDoc) {
        final Element el = xmlDoc.getRootElement().element(new QName("contentStatus", PackagePropertiesUnmarshaller.namespaceCP));
        if (el == null) {
            return null;
        }
        return el.getStringValue();
    }
    
    private String loadContentType(final Document xmlDoc) {
        final Element el = xmlDoc.getRootElement().element(new QName("contentType", PackagePropertiesUnmarshaller.namespaceCP));
        if (el == null) {
            return null;
        }
        return el.getStringValue();
    }
    
    private String loadCreated(final Document xmlDoc) {
        final Element el = xmlDoc.getRootElement().element(new QName("created", PackagePropertiesUnmarshaller.namespaceDcTerms));
        if (el == null) {
            return null;
        }
        return el.getStringValue();
    }
    
    private String loadCreator(final Document xmlDoc) {
        final Element el = xmlDoc.getRootElement().element(new QName("creator", PackagePropertiesUnmarshaller.namespaceDC));
        if (el == null) {
            return null;
        }
        return el.getStringValue();
    }
    
    private String loadDescription(final Document xmlDoc) {
        final Element el = xmlDoc.getRootElement().element(new QName("description", PackagePropertiesUnmarshaller.namespaceDC));
        if (el == null) {
            return null;
        }
        return el.getStringValue();
    }
    
    private String loadIdentifier(final Document xmlDoc) {
        final Element el = xmlDoc.getRootElement().element(new QName("identifier", PackagePropertiesUnmarshaller.namespaceDC));
        if (el == null) {
            return null;
        }
        return el.getStringValue();
    }
    
    private String loadKeywords(final Document xmlDoc) {
        final Element el = xmlDoc.getRootElement().element(new QName("keywords", PackagePropertiesUnmarshaller.namespaceCP));
        if (el == null) {
            return null;
        }
        return el.getStringValue();
    }
    
    private String loadLanguage(final Document xmlDoc) {
        final Element el = xmlDoc.getRootElement().element(new QName("language", PackagePropertiesUnmarshaller.namespaceDC));
        if (el == null) {
            return null;
        }
        return el.getStringValue();
    }
    
    private String loadLastModifiedBy(final Document xmlDoc) {
        final Element el = xmlDoc.getRootElement().element(new QName("lastModifiedBy", PackagePropertiesUnmarshaller.namespaceCP));
        if (el == null) {
            return null;
        }
        return el.getStringValue();
    }
    
    private String loadLastPrinted(final Document xmlDoc) {
        final Element el = xmlDoc.getRootElement().element(new QName("lastPrinted", PackagePropertiesUnmarshaller.namespaceCP));
        if (el == null) {
            return null;
        }
        return el.getStringValue();
    }
    
    private String loadModified(final Document xmlDoc) {
        final Element el = xmlDoc.getRootElement().element(new QName("modified", PackagePropertiesUnmarshaller.namespaceDcTerms));
        if (el == null) {
            return null;
        }
        return el.getStringValue();
    }
    
    private String loadRevision(final Document xmlDoc) {
        final Element el = xmlDoc.getRootElement().element(new QName("revision", PackagePropertiesUnmarshaller.namespaceCP));
        if (el == null) {
            return null;
        }
        return el.getStringValue();
    }
    
    private String loadSubject(final Document xmlDoc) {
        final Element el = xmlDoc.getRootElement().element(new QName("subject", PackagePropertiesUnmarshaller.namespaceDC));
        if (el == null) {
            return null;
        }
        return el.getStringValue();
    }
    
    private String loadTitle(final Document xmlDoc) {
        final Element el = xmlDoc.getRootElement().element(new QName("title", PackagePropertiesUnmarshaller.namespaceDC));
        if (el == null) {
            return null;
        }
        return el.getStringValue();
    }
    
    private String loadVersion(final Document xmlDoc) {
        final Element el = xmlDoc.getRootElement().element(new QName("version", PackagePropertiesUnmarshaller.namespaceCP));
        if (el == null) {
            return null;
        }
        return el.getStringValue();
    }
    
    public void checkElementForOPCCompliance(final Element el) throws InvalidFormatException {
        final List<Namespace> declaredNamespaces = (List<Namespace>)el.declaredNamespaces();
        for (final Namespace ns : declaredNamespaces) {
            if (ns.getURI().equals("http://schemas.openxmlformats.org/markup-compatibility/2006")) {
                throw new InvalidFormatException("OPC Compliance error [M4.2]: A format consumer shall consider the use of the Markup Compatibility namespace to be an error.");
            }
        }
        if (el.getNamespace().getURI().equals("http://purl.org/dc/terms/") && !el.getName().equals("created") && !el.getName().equals("modified")) {
            throw new InvalidFormatException("OPC Compliance error [M4.3]: Producers shall not create a document element that contains refinements to the Dublin Core elements, except for the two specified in the schema: <dcterms:created> and <dcterms:modified> Consumers shall consider a document element that violates this constraint to be an error.");
        }
        if (el.attribute(new QName("lang", PackagePropertiesUnmarshaller.namespaceXML)) != null) {
            throw new InvalidFormatException("OPC Compliance error [M4.4]: Producers shall not create a document element that contains the xml:lang attribute. Consumers shall consider a document element that violates this constraint to be an error.");
        }
        if (el.getNamespace().getURI().equals("http://purl.org/dc/terms/")) {
            final String elName = el.getName();
            if (!elName.equals("created") && !elName.equals("modified")) {
                throw new InvalidFormatException("Namespace error : " + elName + " shouldn't have the following naemspace -> " + "http://purl.org/dc/terms/");
            }
            final Attribute typeAtt = el.attribute(new QName("type", PackagePropertiesUnmarshaller.namespaceXSI));
            if (typeAtt == null) {
                throw new InvalidFormatException("The element '" + elName + "' must have the '" + PackagePropertiesUnmarshaller.namespaceXSI.getPrefix() + ":type' attribute present !");
            }
            if (!typeAtt.getValue().equals("dcterms:W3CDTF")) {
                throw new InvalidFormatException("The element '" + elName + "' must have the '" + PackagePropertiesUnmarshaller.namespaceXSI.getPrefix() + ":type' attribute with the value 'dcterms:W3CDTF' !");
            }
        }
        final Iterator<Element> itChildren = (Iterator<Element>)el.elementIterator();
        while (itChildren.hasNext()) {
            this.checkElementForOPCCompliance(itChildren.next());
        }
    }
    
    static {
        namespaceDC = new Namespace("dc", "http://purl.org/dc/elements/1.1/");
        namespaceCP = new Namespace("cp", "http://schemas.openxmlformats.org/package/2006/metadata/core-properties");
        namespaceDcTerms = new Namespace("dcterms", "http://purl.org/dc/terms/");
        namespaceXML = new Namespace("xml", "http://www.w3.org/XML/1998/namespace");
        namespaceXSI = new Namespace("xsi", "http://www.w3.org/2001/XMLSchema-instance");
    }
}
