// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.openxml4j.opc.internal;

import java.io.OutputStream;
import org.apache.poi.openxml4j.exceptions.InvalidOperationException;
import java.io.InputStream;
import java.text.ParsePosition;
import java.util.TimeZone;
import java.text.SimpleDateFormat;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.openxml4j.opc.PackagePartName;
import org.apache.poi.openxml4j.opc.OPCPackage;
import java.util.Date;
import org.apache.poi.openxml4j.util.Nullable;
import org.apache.poi.openxml4j.opc.PackageProperties;
import org.apache.poi.openxml4j.opc.PackagePart;

public final class PackagePropertiesPart extends PackagePart implements PackageProperties
{
    public static final String NAMESPACE_DC_URI = "http://purl.org/dc/elements/1.1/";
    public static final String NAMESPACE_CP_URI = "http://schemas.openxmlformats.org/package/2006/metadata/core-properties";
    public static final String NAMESPACE_DCTERMS_URI = "http://purl.org/dc/terms/";
    public static final String NAMESPACE_XSI_URI = "http://www.w3.org/2001/XMLSchema-instance";
    protected Nullable<String> category;
    protected Nullable<String> contentStatus;
    protected Nullable<String> contentType;
    protected Nullable<Date> created;
    protected Nullable<String> creator;
    protected Nullable<String> description;
    protected Nullable<String> identifier;
    protected Nullable<String> keywords;
    protected Nullable<String> language;
    protected Nullable<String> lastModifiedBy;
    protected Nullable<Date> lastPrinted;
    protected Nullable<Date> modified;
    protected Nullable<String> revision;
    protected Nullable<String> subject;
    protected Nullable<String> title;
    protected Nullable<String> version;
    
    public PackagePropertiesPart(final OPCPackage pack, final PackagePartName partName) throws InvalidFormatException {
        super(pack, partName, "application/vnd.openxmlformats-package.core-properties+xml");
        this.category = new Nullable<String>();
        this.contentStatus = new Nullable<String>();
        this.contentType = new Nullable<String>();
        this.created = new Nullable<Date>();
        this.creator = new Nullable<String>();
        this.description = new Nullable<String>();
        this.identifier = new Nullable<String>();
        this.keywords = new Nullable<String>();
        this.language = new Nullable<String>();
        this.lastModifiedBy = new Nullable<String>();
        this.lastPrinted = new Nullable<Date>();
        this.modified = new Nullable<Date>();
        this.revision = new Nullable<String>();
        this.subject = new Nullable<String>();
        this.title = new Nullable<String>();
        this.version = new Nullable<String>();
    }
    
    public Nullable<String> getCategoryProperty() {
        return this.category;
    }
    
    public Nullable<String> getContentStatusProperty() {
        return this.contentStatus;
    }
    
    public Nullable<String> getContentTypeProperty() {
        return this.contentType;
    }
    
    public Nullable<Date> getCreatedProperty() {
        return this.created;
    }
    
    public String getCreatedPropertyString() {
        return this.getDateValue(this.created);
    }
    
    public Nullable<String> getCreatorProperty() {
        return this.creator;
    }
    
    public Nullable<String> getDescriptionProperty() {
        return this.description;
    }
    
    public Nullable<String> getIdentifierProperty() {
        return this.identifier;
    }
    
    public Nullable<String> getKeywordsProperty() {
        return this.keywords;
    }
    
    public Nullable<String> getLanguageProperty() {
        return this.language;
    }
    
    public Nullable<String> getLastModifiedByProperty() {
        return this.lastModifiedBy;
    }
    
    public Nullable<Date> getLastPrintedProperty() {
        return this.lastPrinted;
    }
    
    public String getLastPrintedPropertyString() {
        return this.getDateValue(this.lastPrinted);
    }
    
    public Nullable<Date> getModifiedProperty() {
        return this.modified;
    }
    
    public String getModifiedPropertyString() {
        if (this.modified.hasValue()) {
            return this.getDateValue(this.modified);
        }
        return this.getDateValue(new Nullable<Date>(new Date()));
    }
    
    public Nullable<String> getRevisionProperty() {
        return this.revision;
    }
    
    public Nullable<String> getSubjectProperty() {
        return this.subject;
    }
    
    public Nullable<String> getTitleProperty() {
        return this.title;
    }
    
    public Nullable<String> getVersionProperty() {
        return this.version;
    }
    
    public void setCategoryProperty(final String category) {
        this.category = this.setStringValue(category);
    }
    
    public void setContentStatusProperty(final String contentStatus) {
        this.contentStatus = this.setStringValue(contentStatus);
    }
    
    public void setContentTypeProperty(final String contentType) {
        this.contentType = this.setStringValue(contentType);
    }
    
    public void setCreatedProperty(final String created) {
        try {
            this.created = this.setDateValue(created);
        }
        catch (InvalidFormatException e) {
            final IllegalArgumentException ex = new IllegalArgumentException("created  : " + e.getLocalizedMessage());
        }
    }
    
    public void setCreatedProperty(final Nullable<Date> created) {
        if (created.hasValue()) {
            this.created = created;
        }
    }
    
    public void setCreatorProperty(final String creator) {
        this.creator = this.setStringValue(creator);
    }
    
    public void setDescriptionProperty(final String description) {
        this.description = this.setStringValue(description);
    }
    
    public void setIdentifierProperty(final String identifier) {
        this.identifier = this.setStringValue(identifier);
    }
    
    public void setKeywordsProperty(final String keywords) {
        this.keywords = this.setStringValue(keywords);
    }
    
    public void setLanguageProperty(final String language) {
        this.language = this.setStringValue(language);
    }
    
    public void setLastModifiedByProperty(final String lastModifiedBy) {
        this.lastModifiedBy = this.setStringValue(lastModifiedBy);
    }
    
    public void setLastPrintedProperty(final String lastPrinted) {
        try {
            this.lastPrinted = this.setDateValue(lastPrinted);
        }
        catch (InvalidFormatException e) {
            final IllegalArgumentException ex = new IllegalArgumentException("lastPrinted  : " + e.getLocalizedMessage());
        }
    }
    
    public void setLastPrintedProperty(final Nullable<Date> lastPrinted) {
        if (lastPrinted.hasValue()) {
            this.lastPrinted = lastPrinted;
        }
    }
    
    public void setModifiedProperty(final String modified) {
        try {
            this.modified = this.setDateValue(modified);
        }
        catch (InvalidFormatException e) {
            final IllegalArgumentException ex = new IllegalArgumentException("modified  : " + e.getLocalizedMessage());
        }
    }
    
    public void setModifiedProperty(final Nullable<Date> modified) {
        if (modified.hasValue()) {
            this.modified = modified;
        }
    }
    
    public void setRevisionProperty(final String revision) {
        this.revision = this.setStringValue(revision);
    }
    
    public void setSubjectProperty(final String subject) {
        this.subject = this.setStringValue(subject);
    }
    
    public void setTitleProperty(final String title) {
        this.title = this.setStringValue(title);
    }
    
    public void setVersionProperty(final String version) {
        this.version = this.setStringValue(version);
    }
    
    private Nullable<String> setStringValue(final String s) {
        if (s == null || s.equals("")) {
            return new Nullable<String>();
        }
        return new Nullable<String>(s);
    }
    
    private Nullable<Date> setDateValue(final String s) throws InvalidFormatException {
        if (s == null || s.equals("")) {
            return new Nullable<Date>();
        }
        final SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        df.setTimeZone(TimeZone.getTimeZone("UTC"));
        final Date d = df.parse(s, new ParsePosition(0));
        if (d == null) {
            throw new InvalidFormatException("Date not well formated");
        }
        return new Nullable<Date>(d);
    }
    
    private String getDateValue(final Nullable<Date> d) {
        if (d == null) {
            return "";
        }
        final Date date = d.getValue();
        if (date == null) {
            return "";
        }
        final SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        df.setTimeZone(TimeZone.getTimeZone("UTC"));
        return df.format(date);
    }
    
    @Override
    protected InputStream getInputStreamImpl() {
        throw new InvalidOperationException("Operation not authorized. This part may only be manipulated using the getters and setters on PackagePropertiesPart");
    }
    
    @Override
    protected OutputStream getOutputStreamImpl() {
        throw new InvalidOperationException("Can't use output stream to set properties !");
    }
    
    @Override
    public boolean save(final OutputStream zos) {
        throw new InvalidOperationException("Operation not authorized. This part may only be manipulated using the getters and setters on PackagePropertiesPart");
    }
    
    @Override
    public boolean load(final InputStream ios) {
        throw new InvalidOperationException("Operation not authorized. This part may only be manipulated using the getters and setters on PackagePropertiesPart");
    }
    
    @Override
    public void close() {
    }
    
    @Override
    public void flush() {
    }
}
