// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.openxml4j.opc.signature;

import org.apache.poi.openxml4j.exceptions.OpenXML4JException;
import java.io.OutputStream;
import java.io.IOException;
import java.io.InputStream;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.openxml4j.opc.PackagePartName;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.openxml4j.opc.internal.ContentType;
import org.apache.poi.openxml4j.opc.PackagePart;

public final class PackageDigitalSignature extends PackagePart
{
    public PackageDigitalSignature() throws InvalidFormatException {
        super(null, null, new ContentType(""));
    }
    
    @Override
    public void close() {
    }
    
    @Override
    public void flush() {
    }
    
    @Override
    protected InputStream getInputStreamImpl() throws IOException {
        return null;
    }
    
    @Override
    protected OutputStream getOutputStreamImpl() {
        return null;
    }
    
    @Override
    public boolean load(final InputStream ios) throws InvalidFormatException {
        return false;
    }
    
    @Override
    public boolean save(final OutputStream zos) throws OpenXML4JException {
        return false;
    }
}
