// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.openxml4j.opc;

import java.util.Date;
import org.apache.poi.openxml4j.util.Nullable;

public interface PackageProperties
{
    public static final String NAMESPACE_DCTERMS = "http://purl.org/dc/terms/";
    public static final String NAMESPACE_DC = "http://purl.org/dc/elements/1.1/";
    
    Nullable<String> getCategoryProperty();
    
    void setCategoryProperty(final String p0);
    
    Nullable<String> getContentStatusProperty();
    
    void setContentStatusProperty(final String p0);
    
    Nullable<String> getContentTypeProperty();
    
    void setContentTypeProperty(final String p0);
    
    Nullable<Date> getCreatedProperty();
    
    void setCreatedProperty(final String p0);
    
    void setCreatedProperty(final Nullable<Date> p0);
    
    Nullable<String> getCreatorProperty();
    
    void setCreatorProperty(final String p0);
    
    Nullable<String> getDescriptionProperty();
    
    void setDescriptionProperty(final String p0);
    
    Nullable<String> getIdentifierProperty();
    
    void setIdentifierProperty(final String p0);
    
    Nullable<String> getKeywordsProperty();
    
    void setKeywordsProperty(final String p0);
    
    Nullable<String> getLanguageProperty();
    
    void setLanguageProperty(final String p0);
    
    Nullable<String> getLastModifiedByProperty();
    
    void setLastModifiedByProperty(final String p0);
    
    Nullable<Date> getLastPrintedProperty();
    
    void setLastPrintedProperty(final String p0);
    
    void setLastPrintedProperty(final Nullable<Date> p0);
    
    Nullable<Date> getModifiedProperty();
    
    void setModifiedProperty(final String p0);
    
    void setModifiedProperty(final Nullable<Date> p0);
    
    Nullable<String> getRevisionProperty();
    
    void setRevisionProperty(final String p0);
    
    Nullable<String> getSubjectProperty();
    
    void setSubjectProperty(final String p0);
    
    Nullable<String> getTitleProperty();
    
    void setTitleProperty(final String p0);
    
    Nullable<String> getVersionProperty();
    
    void setVersionProperty(final String p0);
}
