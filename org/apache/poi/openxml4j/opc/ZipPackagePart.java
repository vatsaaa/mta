// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.openxml4j.opc;

import org.apache.poi.openxml4j.exceptions.InvalidOperationException;
import org.apache.poi.openxml4j.exceptions.OpenXML4JException;
import org.apache.poi.openxml4j.opc.internal.marshallers.ZipPartMarshaller;
import java.io.OutputStream;
import java.io.IOException;
import java.io.InputStream;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import java.util.zip.ZipEntry;

public class ZipPackagePart extends PackagePart
{
    private ZipEntry zipEntry;
    
    public ZipPackagePart(final OPCPackage container, final PackagePartName partName, final String contentType) throws InvalidFormatException {
        super(container, partName, contentType);
    }
    
    public ZipPackagePart(final OPCPackage container, final ZipEntry zipEntry, final PackagePartName partName, final String contentType) throws InvalidFormatException {
        super(container, partName, contentType);
        this.zipEntry = zipEntry;
    }
    
    public ZipEntry getZipArchive() {
        return this.zipEntry;
    }
    
    @Override
    protected InputStream getInputStreamImpl() throws IOException {
        return ((ZipPackage)this._container).getZipArchive().getInputStream(this.zipEntry);
    }
    
    @Override
    protected OutputStream getOutputStreamImpl() {
        return null;
    }
    
    @Override
    public boolean save(final OutputStream os) throws OpenXML4JException {
        return new ZipPartMarshaller().marshall(this, os);
    }
    
    @Override
    public boolean load(final InputStream ios) {
        throw new InvalidOperationException("Method not implemented !");
    }
    
    @Override
    public void close() {
        throw new InvalidOperationException("Method not implemented !");
    }
    
    @Override
    public void flush() {
        throw new InvalidOperationException("Method not implemented !");
    }
}
