// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.openxml4j.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.zip.ZipEntry;
import java.util.Enumeration;

public interface ZipEntrySource
{
    Enumeration<? extends ZipEntry> getEntries();
    
    InputStream getInputStream(final ZipEntry p0) throws IOException;
    
    void close() throws IOException;
}
