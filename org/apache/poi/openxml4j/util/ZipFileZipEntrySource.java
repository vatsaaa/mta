// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.openxml4j.util;

import java.io.InputStream;
import java.util.zip.ZipEntry;
import java.util.Enumeration;
import java.io.IOException;
import java.util.zip.ZipFile;

public class ZipFileZipEntrySource implements ZipEntrySource
{
    private ZipFile zipArchive;
    
    public ZipFileZipEntrySource(final ZipFile zipFile) {
        this.zipArchive = zipFile;
    }
    
    public void close() throws IOException {
        if (this.zipArchive != null) {
            this.zipArchive.close();
        }
        this.zipArchive = null;
    }
    
    public Enumeration<? extends ZipEntry> getEntries() {
        if (this.zipArchive == null) {
            throw new IllegalStateException("Zip File is closed");
        }
        return this.zipArchive.entries();
    }
    
    public InputStream getInputStream(final ZipEntry entry) throws IOException {
        if (this.zipArchive == null) {
            throw new IllegalStateException("Zip File is closed");
        }
        return this.zipArchive.getInputStream(entry);
    }
}
