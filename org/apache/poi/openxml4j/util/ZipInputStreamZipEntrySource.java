// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.openxml4j.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.Iterator;
import java.io.InputStream;
import java.util.Enumeration;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.ArrayList;

public class ZipInputStreamZipEntrySource implements ZipEntrySource
{
    private ArrayList<FakeZipEntry> zipEntries;
    
    public ZipInputStreamZipEntrySource(final ZipInputStream inp) throws IOException {
        this.zipEntries = new ArrayList<FakeZipEntry>();
        boolean going = true;
        while (going) {
            final ZipEntry zipEntry = inp.getNextEntry();
            if (zipEntry == null) {
                going = false;
            }
            else {
                final FakeZipEntry entry = new FakeZipEntry(zipEntry, inp);
                inp.closeEntry();
                this.zipEntries.add(entry);
            }
        }
        inp.close();
    }
    
    public Enumeration<? extends ZipEntry> getEntries() {
        return new EntryEnumerator();
    }
    
    public InputStream getInputStream(final ZipEntry zipEntry) {
        final FakeZipEntry entry = (FakeZipEntry)zipEntry;
        return entry.getInputStream();
    }
    
    public void close() {
        this.zipEntries = null;
    }
    
    private class EntryEnumerator implements Enumeration<ZipEntry>
    {
        private Iterator<? extends ZipEntry> iterator;
        
        private EntryEnumerator() {
            this.iterator = ZipInputStreamZipEntrySource.this.zipEntries.iterator();
        }
        
        public boolean hasMoreElements() {
            return this.iterator.hasNext();
        }
        
        public ZipEntry nextElement() {
            return (ZipEntry)this.iterator.next();
        }
    }
    
    public static class FakeZipEntry extends ZipEntry
    {
        private byte[] data;
        
        public FakeZipEntry(final ZipEntry entry, final ZipInputStream inp) throws IOException {
            super(entry.getName());
            final long entrySize = entry.getSize();
            ByteArrayOutputStream baos;
            if (entrySize != -1L) {
                if (entrySize >= 2147483647L) {
                    throw new IOException("ZIP entry size is too large");
                }
                baos = new ByteArrayOutputStream((int)entrySize);
            }
            else {
                baos = new ByteArrayOutputStream();
            }
            final byte[] buffer = new byte[4096];
            int read = 0;
            while ((read = inp.read(buffer)) != -1) {
                baos.write(buffer, 0, read);
            }
            this.data = baos.toByteArray();
        }
        
        public InputStream getInputStream() {
            return new ByteArrayInputStream(this.data);
        }
    }
}
