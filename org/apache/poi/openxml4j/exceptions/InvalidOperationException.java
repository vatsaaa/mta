// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.openxml4j.exceptions;

public class InvalidOperationException extends OpenXML4JRuntimeException
{
    public InvalidOperationException(final String message) {
        super(message);
    }
}
