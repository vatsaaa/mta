// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.openxml4j.exceptions;

public final class InvalidFormatException extends OpenXML4JException
{
    public InvalidFormatException(final String message) {
        super(message);
    }
}
