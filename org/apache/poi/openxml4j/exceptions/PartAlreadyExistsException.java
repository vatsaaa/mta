// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.openxml4j.exceptions;

public final class PartAlreadyExistsException extends InvalidOperationException
{
    public PartAlreadyExistsException(final String message) {
        super(message);
    }
}
