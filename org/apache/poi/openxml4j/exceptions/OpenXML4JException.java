// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.openxml4j.exceptions;

public class OpenXML4JException extends Exception
{
    public OpenXML4JException(final String msg) {
        super(msg);
    }
}
