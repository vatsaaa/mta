// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi;

public abstract class POIXMLRelation
{
    protected String _type;
    protected String _relation;
    protected String _defaultName;
    private Class<? extends POIXMLDocumentPart> _cls;
    
    public POIXMLRelation(final String type, final String rel, final String defaultName, final Class<? extends POIXMLDocumentPart> cls) {
        this._type = type;
        this._relation = rel;
        this._defaultName = defaultName;
        this._cls = cls;
    }
    
    public POIXMLRelation(final String type, final String rel, final String defaultName) {
        this(type, rel, defaultName, null);
    }
    
    public String getContentType() {
        return this._type;
    }
    
    public String getRelation() {
        return this._relation;
    }
    
    public String getDefaultFileName() {
        return this._defaultName;
    }
    
    public String getFileName(final int index) {
        if (this._defaultName.indexOf("#") == -1) {
            return this.getDefaultFileName();
        }
        return this._defaultName.replace("#", Integer.toString(index));
    }
    
    public Class<? extends POIXMLDocumentPart> getRelationClass() {
        return this._cls;
    }
}
