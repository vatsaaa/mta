// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hmef.extractor;

import java.util.Iterator;
import org.apache.poi.hmef.Attachment;
import org.apache.poi.hsmf.datatypes.MAPIProperty;
import org.apache.poi.hmef.attribute.MAPIRtfAttribute;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.File;
import org.apache.poi.hmef.HMEFMessage;

public final class HMEFContentsExtractor
{
    private HMEFMessage message;
    
    public static void main(final String[] args) throws Exception {
        if (args.length < 2) {
            System.err.println("Use:");
            System.err.println("  HMEFContentsExtractor <filename> <output dir>");
            System.err.println("");
            System.err.println("");
            System.err.println("Where <filename> is the winmail.dat file to extract,");
            System.err.println(" and <output dir> is where to place the extracted files");
            System.exit(2);
        }
        final HMEFContentsExtractor ext = new HMEFContentsExtractor(new File(args[0]));
        final File dir = new File(args[1]);
        final File rtf = new File(dir, "message.rtf");
        if (!dir.exists()) {
            throw new FileNotFoundException("Output directory " + dir.getName() + " not found");
        }
        System.out.println("Extracting...");
        ext.extractMessageBody(rtf);
        ext.extractAttachments(dir);
        System.out.println("Extraction completed");
    }
    
    public HMEFContentsExtractor(final File filename) throws IOException {
        this(new HMEFMessage(new FileInputStream(filename)));
    }
    
    public HMEFContentsExtractor(final HMEFMessage message) {
        this.message = message;
    }
    
    public void extractMessageBody(final File dest) throws IOException {
        final FileOutputStream fout = new FileOutputStream(dest);
        final MAPIRtfAttribute body = (MAPIRtfAttribute)this.message.getMessageMAPIAttribute(MAPIProperty.RTF_COMPRESSED);
        fout.write(body.getData());
        fout.close();
    }
    
    public void extractAttachments(final File dir) throws IOException {
        int count = 0;
        for (final Attachment att : this.message.getAttachments()) {
            ++count;
            String filename = att.getLongFilename();
            if (filename == null || filename.length() == 0) {
                filename = att.getFilename();
            }
            if (filename == null || filename.length() == 0) {
                filename = "attachment" + count;
                if (att.getExtension() != null) {
                    filename += att.getExtension();
                }
            }
            final File file = new File(dir, filename);
            final FileOutputStream fout = new FileOutputStream(file);
            fout.write(att.getContents());
            fout.close();
        }
    }
}
