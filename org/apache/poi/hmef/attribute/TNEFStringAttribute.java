// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hmef.attribute;

import java.io.IOException;
import org.apache.poi.util.StringUtil;
import java.io.InputStream;

public final class TNEFStringAttribute extends TNEFAttribute
{
    private String data;
    
    protected TNEFStringAttribute(final int id, final int type, final InputStream inp) throws IOException {
        super(id, type, inp);
        String tmpData = null;
        final byte[] data = this.getData();
        if (this.getType() == 2) {
            tmpData = StringUtil.getFromUnicodeLE(data);
        }
        else {
            tmpData = StringUtil.getFromCompressedUnicode(data, 0, data.length);
        }
        if (tmpData.endsWith("\u0000")) {
            tmpData = tmpData.substring(0, tmpData.length() - 1);
        }
        this.data = tmpData;
    }
    
    public String getString() {
        return this.data;
    }
    
    @Override
    public String toString() {
        return "Attribute " + this.getProperty().toString() + ", type=" + this.getType() + ", data=" + this.getString();
    }
    
    public static String getAsString(final TNEFAttribute attr) {
        if (attr == null) {
            return null;
        }
        if (attr instanceof TNEFStringAttribute) {
            return ((TNEFStringAttribute)attr).getString();
        }
        System.err.println("Warning, non string property found: " + attr.toString());
        return null;
    }
}
