// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hmef.attribute;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public final class TNEFMAPIAttribute extends TNEFAttribute
{
    private final List<MAPIAttribute> attributes;
    
    protected TNEFMAPIAttribute(final int id, final int type, final InputStream inp) throws IOException {
        super(id, type, inp);
        this.attributes = MAPIAttribute.create(this);
    }
    
    public List<MAPIAttribute> getMAPIAttributes() {
        return this.attributes;
    }
    
    @Override
    public String toString() {
        return "Attribute " + this.getProperty().toString() + ", type=" + this.getType() + ", " + this.attributes.size() + " MAPI Attributes";
    }
}
