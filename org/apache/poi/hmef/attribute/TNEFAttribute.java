// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hmef.attribute;

import java.io.IOException;
import org.apache.poi.util.IOUtils;
import org.apache.poi.util.LittleEndian;
import java.io.InputStream;

public class TNEFAttribute
{
    private final TNEFProperty property;
    private final int type;
    private final byte[] data;
    private final int checksum;
    
    protected TNEFAttribute(final int id, final int type, final InputStream inp) throws IOException {
        this.type = type;
        final int length = LittleEndian.readInt(inp);
        this.property = TNEFProperty.getBest(id, type);
        IOUtils.readFully(inp, this.data = new byte[length]);
        this.checksum = LittleEndian.readUShort(inp);
    }
    
    public static TNEFAttribute create(final InputStream inp) throws IOException {
        final int id = LittleEndian.readUShort(inp);
        final int type = LittleEndian.readUShort(inp);
        if (id == TNEFProperty.ID_MAPIPROPERTIES.id || id == TNEFProperty.ID_ATTACHMENT.id) {
            return new TNEFMAPIAttribute(id, type, inp);
        }
        if (type == 1 || type == 2) {
            return new TNEFStringAttribute(id, type, inp);
        }
        if (type == 3) {
            return new TNEFDateAttribute(id, type, inp);
        }
        return new TNEFAttribute(id, type, inp);
    }
    
    public TNEFProperty getProperty() {
        return this.property;
    }
    
    public int getType() {
        return this.type;
    }
    
    public byte[] getData() {
        return this.data;
    }
    
    @Override
    public String toString() {
        return "Attribute " + this.property.toString() + ", type=" + this.type + ", data length=" + this.data.length;
    }
}
