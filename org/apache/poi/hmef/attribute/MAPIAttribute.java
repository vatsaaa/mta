// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hmef.attribute;

import java.io.IOException;
import org.apache.poi.util.StringUtil;
import org.apache.poi.util.IOUtils;
import java.util.ArrayList;
import java.io.InputStream;
import org.apache.poi.util.LittleEndian;
import java.io.ByteArrayInputStream;
import java.util.List;
import org.apache.poi.util.HexDump;
import org.apache.poi.hsmf.datatypes.MAPIProperty;

public class MAPIAttribute
{
    private final MAPIProperty property;
    private final int type;
    private final byte[] data;
    
    public MAPIAttribute(final MAPIProperty property, final int type, final byte[] data) {
        this.property = property;
        this.type = type;
        this.data = data;
    }
    
    public MAPIProperty getProperty() {
        return this.property;
    }
    
    public int getType() {
        return this.type;
    }
    
    public byte[] getData() {
        return this.data;
    }
    
    @Override
    public String toString() {
        String hex;
        if (this.data.length <= 16) {
            hex = HexDump.toHex(this.data);
        }
        else {
            final byte[] d = new byte[16];
            System.arraycopy(this.data, 0, d, 0, 16);
            hex = HexDump.toHex(d);
            hex = hex.substring(0, hex.length() - 1) + ", ....]";
        }
        return this.property.toString() + " " + hex;
    }
    
    public static List<MAPIAttribute> create(final TNEFAttribute parent) throws IOException {
        if (parent.getProperty() != TNEFProperty.ID_MAPIPROPERTIES) {
            if (parent.getProperty() != TNEFProperty.ID_ATTACHMENT) {
                throw new IllegalArgumentException("Can only create from a MAPIProperty attribute, instead received a " + parent.getProperty() + " one");
            }
        }
        final ByteArrayInputStream inp = new ByteArrayInputStream(parent.getData());
        final int count = LittleEndian.readInt(inp);
        final List<MAPIAttribute> attrs = new ArrayList<MAPIAttribute>();
        for (int i = 0; i < count; ++i) {
            final int typeAndMV = LittleEndian.readUShort(inp);
            final int id = LittleEndian.readUShort(inp);
            boolean isMV = false;
            boolean isVL = false;
            int type = typeAndMV;
            if ((typeAndMV & 0x1000) > 0) {
                isMV = true;
                type -= 4096;
            }
            if (type == 30 || type == 31 || type == 258 || type == 13) {
                isVL = true;
            }
            MAPIProperty prop = MAPIProperty.get(id);
            if (id >= 32768 && id <= 65535) {
                final byte[] guid = new byte[16];
                IOUtils.readFully(inp, guid);
                final int mptype = LittleEndian.readInt(inp);
                String name;
                if (mptype == 0) {
                    final int mpid = LittleEndian.readInt(inp);
                    final MAPIProperty base = MAPIProperty.get(mpid);
                    name = base.name;
                }
                else {
                    final int mplen = LittleEndian.readInt(inp);
                    final byte[] mpdata = new byte[mplen];
                    IOUtils.readFully(inp, mpdata);
                    name = StringUtil.getFromUnicodeLE(mpdata, 0, mplen / 2 - 1);
                    skipToBoundary(mplen, inp);
                }
                prop = MAPIProperty.createCustom(id, type, name);
            }
            if (prop == MAPIProperty.UNKNOWN) {
                prop = MAPIProperty.createCustom(id, type, "(unknown " + Integer.toHexString(id) + ")");
            }
            int values = 1;
            if (isMV || isVL) {
                values = LittleEndian.readInt(inp);
            }
            for (int j = 0; j < values; ++j) {
                final int len = getLength(type, inp);
                final byte[] data = new byte[len];
                IOUtils.readFully(inp, data);
                skipToBoundary(len, inp);
                MAPIAttribute attr;
                if (type == 31 || type == 30) {
                    attr = new MAPIStringAttribute(prop, type, data);
                }
                else if (type == 7 || type == 64) {
                    attr = new MAPIDateAttribute(prop, type, data);
                }
                else if (id == MAPIProperty.RTF_COMPRESSED.id) {
                    attr = new MAPIRtfAttribute(prop, type, data);
                }
                else {
                    attr = new MAPIAttribute(prop, type, data);
                }
                attrs.add(attr);
            }
        }
        return attrs;
    }
    
    private static int getLength(final int type, final InputStream inp) throws IOException {
        switch (type) {
            case 1: {
                return 0;
            }
            case 2:
            case 11: {
                return 2;
            }
            case 3:
            case 4:
            case 10: {
                return 4;
            }
            case 5:
            case 6:
            case 7:
            case 20:
            case 64: {
                return 8;
            }
            case 72: {
                return 16;
            }
            case 13:
            case 30:
            case 31:
            case 258: {
                return LittleEndian.readInt(inp);
            }
            default: {
                throw new IllegalArgumentException("Unknown type " + type);
            }
        }
    }
    
    private static void skipToBoundary(final int length, final InputStream inp) throws IOException {
        if (length % 4 != 0) {
            final int skip = 4 - length % 4;
            final byte[] padding = new byte[skip];
            IOUtils.readFully(inp, padding);
        }
    }
}
