// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hmef.attribute;

import org.apache.poi.util.StringUtil;
import java.io.UnsupportedEncodingException;
import org.apache.poi.hsmf.datatypes.MAPIProperty;

public final class MAPIStringAttribute extends MAPIAttribute
{
    private static final String CODEPAGE = "CP1252";
    private final String data;
    
    public MAPIStringAttribute(final MAPIProperty property, final int type, final byte[] data) {
        super(property, type, data);
        String tmpData = null;
        Label_0085: {
            if (type == 30) {
                try {
                    tmpData = new String(data, "CP1252");
                    break Label_0085;
                }
                catch (UnsupportedEncodingException e) {
                    throw new RuntimeException("JVM Broken - core encoding CP1252 missing");
                }
            }
            if (type != 31) {
                throw new IllegalArgumentException("Not a string type " + type);
            }
            tmpData = StringUtil.getFromUnicodeLE(data);
        }
        if (tmpData.endsWith("\u0000")) {
            tmpData = tmpData.substring(0, tmpData.length() - 1);
        }
        this.data = tmpData;
    }
    
    public String getDataString() {
        return this.data;
    }
    
    @Override
    public String toString() {
        return this.getProperty().toString() + " " + this.data;
    }
    
    public static String getAsString(final MAPIAttribute attr) {
        if (attr == null) {
            return null;
        }
        if (attr instanceof MAPIStringAttribute) {
            return ((MAPIStringAttribute)attr).getDataString();
        }
        if (attr instanceof MAPIRtfAttribute) {
            return ((MAPIRtfAttribute)attr).getDataString();
        }
        System.err.println("Warning, non string property found: " + attr.toString());
        return null;
    }
}
