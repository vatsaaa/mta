// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hmef.attribute;

import org.apache.poi.hpsf.Util;
import org.apache.poi.util.LittleEndian;
import org.apache.poi.hsmf.datatypes.MAPIProperty;
import java.util.Date;

public final class MAPIDateAttribute extends MAPIAttribute
{
    private Date data;
    
    protected MAPIDateAttribute(final MAPIProperty property, final int type, final byte[] data) {
        super(property, type, data);
        this.data = Util.filetimeToDate(LittleEndian.getLong(data, 0));
    }
    
    public Date getDate() {
        return this.data;
    }
    
    @Override
    public String toString() {
        return this.getProperty().toString() + " " + this.data.toString();
    }
    
    public static Date getAsDate(final MAPIAttribute attr) {
        if (attr == null) {
            return null;
        }
        if (attr instanceof MAPIDateAttribute) {
            return ((MAPIDateAttribute)attr).getDate();
        }
        System.err.println("Warning, non date property found: " + attr.toString());
        return null;
    }
}
