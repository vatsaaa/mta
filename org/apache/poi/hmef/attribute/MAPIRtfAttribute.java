// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hmef.attribute;

import java.io.IOException;
import org.apache.poi.util.StringUtil;
import java.io.InputStream;
import java.io.ByteArrayInputStream;
import org.apache.poi.hmef.CompressedRTF;
import org.apache.poi.hsmf.datatypes.MAPIProperty;

public final class MAPIRtfAttribute extends MAPIAttribute
{
    private final byte[] decompressed;
    private final String data;
    
    public MAPIRtfAttribute(final MAPIProperty property, final int type, final byte[] data) throws IOException {
        super(property, type, data);
        final CompressedRTF rtf = new CompressedRTF();
        final byte[] tmp = rtf.decompress(new ByteArrayInputStream(data));
        if (tmp.length > rtf.getDeCompressedSize()) {
            System.arraycopy(tmp, 0, this.decompressed = new byte[rtf.getDeCompressedSize()], 0, this.decompressed.length);
        }
        else {
            this.decompressed = tmp;
        }
        this.data = StringUtil.getFromCompressedUnicode(this.decompressed, 0, this.decompressed.length);
    }
    
    public byte[] getRawData() {
        return super.getData();
    }
    
    @Override
    public byte[] getData() {
        return this.decompressed;
    }
    
    public String getDataString() {
        return this.data;
    }
    
    @Override
    public String toString() {
        return this.getProperty().toString() + " " + this.data;
    }
}
