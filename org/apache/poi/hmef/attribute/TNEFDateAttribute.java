// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hmef.attribute;

import java.io.IOException;
import java.util.Calendar;
import java.util.TimeZone;
import org.apache.poi.hpsf.Util;
import org.apache.poi.util.LittleEndian;
import java.io.InputStream;
import java.util.Date;

public final class TNEFDateAttribute extends TNEFAttribute
{
    private Date data;
    
    protected TNEFDateAttribute(final int id, final int type, final InputStream inp) throws IOException {
        super(id, type, inp);
        final byte[] data = this.getData();
        if (data.length == 8) {
            this.data = Util.filetimeToDate(LittleEndian.getLong(this.getData(), 0));
        }
        else {
            if (data.length != 14) {
                throw new IllegalArgumentException("Invalid date, found " + data.length + " bytes");
            }
            final Calendar c = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
            c.set(1, LittleEndian.getUShort(data, 0));
            c.set(2, LittleEndian.getUShort(data, 2) - 1);
            c.set(5, LittleEndian.getUShort(data, 4));
            c.set(11, LittleEndian.getUShort(data, 6));
            c.set(12, LittleEndian.getUShort(data, 8));
            c.set(13, LittleEndian.getUShort(data, 10));
            c.set(14, 0);
            this.data = c.getTime();
        }
    }
    
    public Date getDate() {
        return this.data;
    }
    
    @Override
    public String toString() {
        return "Attribute " + this.getProperty().toString() + ", type=" + this.getType() + ", date=" + this.data.toString();
    }
    
    public static Date getAsDate(final TNEFAttribute attr) {
        if (attr == null) {
            return null;
        }
        if (attr instanceof TNEFDateAttribute) {
            return ((TNEFDateAttribute)attr).getDate();
        }
        System.err.println("Warning, non date property found: " + attr.toString());
        return null;
    }
}
