// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hmef;

import org.apache.poi.hmef.attribute.TNEFDateAttribute;
import java.util.Date;
import org.apache.poi.hmef.attribute.TNEFStringAttribute;
import org.apache.poi.hmef.attribute.MAPIStringAttribute;
import org.apache.poi.hsmf.datatypes.MAPIProperty;
import java.util.Iterator;
import org.apache.poi.hmef.attribute.TNEFProperty;
import java.util.Collection;
import org.apache.poi.hmef.attribute.TNEFMAPIAttribute;
import java.util.ArrayList;
import org.apache.poi.hmef.attribute.MAPIAttribute;
import org.apache.poi.hmef.attribute.TNEFAttribute;
import java.util.List;

public final class Attachment
{
    private final List<TNEFAttribute> attributes;
    private final List<MAPIAttribute> mapiAttributes;
    
    public Attachment() {
        this.attributes = new ArrayList<TNEFAttribute>();
        this.mapiAttributes = new ArrayList<MAPIAttribute>();
    }
    
    protected void addAttribute(final TNEFAttribute attr) {
        this.attributes.add(attr);
        if (attr instanceof TNEFMAPIAttribute) {
            final TNEFMAPIAttribute tnefMAPI = (TNEFMAPIAttribute)attr;
            this.mapiAttributes.addAll(tnefMAPI.getMAPIAttributes());
        }
    }
    
    public TNEFAttribute getAttribute(final TNEFProperty id) {
        for (final TNEFAttribute attr : this.attributes) {
            if (attr.getProperty() == id) {
                return attr;
            }
        }
        return null;
    }
    
    public MAPIAttribute getMAPIAttribute(final MAPIProperty id) {
        for (final MAPIAttribute attr : this.mapiAttributes) {
            if (attr.getProperty() == id) {
                return attr;
            }
        }
        return null;
    }
    
    public List<TNEFAttribute> getAttributes() {
        return this.attributes;
    }
    
    public List<MAPIAttribute> getMAPIAttributes() {
        return this.mapiAttributes;
    }
    
    private String getString(final MAPIProperty id) {
        return MAPIStringAttribute.getAsString(this.getMAPIAttribute(id));
    }
    
    private String getString(final TNEFProperty id) {
        return TNEFStringAttribute.getAsString(this.getAttribute(id));
    }
    
    public String getFilename() {
        return this.getString(TNEFProperty.ID_ATTACHTITLE);
    }
    
    public String getLongFilename() {
        return this.getString(MAPIProperty.ATTACH_LONG_FILENAME);
    }
    
    public String getExtension() {
        return this.getString(MAPIProperty.ATTACH_EXTENSION);
    }
    
    public Date getModifiedDate() {
        return TNEFDateAttribute.getAsDate(this.getAttribute(TNEFProperty.ID_ATTACHMODIFYDATE));
    }
    
    public byte[] getContents() {
        final TNEFAttribute contents = this.getAttribute(TNEFProperty.ID_ATTACHDATA);
        if (contents == null) {
            throw new IllegalArgumentException("Attachment corrupt - no Data section");
        }
        return contents.getData();
    }
    
    public byte[] getRenderedMetaFile() {
        final TNEFAttribute meta = this.getAttribute(TNEFProperty.ID_ATTACHMETAFILE);
        if (meta == null) {
            return null;
        }
        return meta.getData();
    }
}
