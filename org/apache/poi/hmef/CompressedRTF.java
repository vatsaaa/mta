// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hmef;

import java.io.UnsupportedEncodingException;
import java.io.IOException;
import org.apache.poi.util.IOUtils;
import org.apache.poi.util.LittleEndian;
import java.io.OutputStream;
import java.io.InputStream;
import org.apache.poi.util.LZWDecompresser;

public final class CompressedRTF extends LZWDecompresser
{
    public static final byte[] COMPRESSED_SIGNATURE;
    public static final byte[] UNCOMPRESSED_SIGNATURE;
    public static final int COMPRESSED_SIGNATURE_INT;
    public static final int UNCOMPRESSED_SIGNATURE_INT;
    public static final String LZW_RTF_PRELOAD = "{\\rtf1\\ansi\\mac\\deff0\\deftab720{\\fonttbl;}{\\f0\\fnil \\froman \\fswiss \\fmodern \\fscript \\fdecor MS Sans SerifSymbolArialTimes New RomanCourier{\\colortbl\\red0\\green0\\blue0\n\r\\par \\pard\\plain\\f0\\fs20\\b\\i\\u\\tab\\tx";
    private int compressedSize;
    private int decompressedSize;
    
    public CompressedRTF() {
        super(true, 2, true);
    }
    
    @Override
    public void decompress(final InputStream src, final OutputStream res) throws IOException {
        this.compressedSize = LittleEndian.readInt(src);
        this.decompressedSize = LittleEndian.readInt(src);
        final int compressionType = LittleEndian.readInt(src);
        final int dataCRC = LittleEndian.readInt(src);
        if (compressionType == CompressedRTF.UNCOMPRESSED_SIGNATURE_INT) {
            IOUtils.copy(src, res);
        }
        else if (compressionType != CompressedRTF.COMPRESSED_SIGNATURE_INT) {
            throw new IllegalArgumentException("Invalid compression signature " + compressionType);
        }
        super.decompress(src, res);
    }
    
    public int getCompressedSize() {
        return this.compressedSize - 12;
    }
    
    public int getDeCompressedSize() {
        return this.decompressedSize;
    }
    
    @Override
    protected int adjustDictionaryOffset(final int offset) {
        return offset;
    }
    
    @Override
    protected int populateDictionary(final byte[] dict) {
        try {
            final byte[] preload = "{\\rtf1\\ansi\\mac\\deff0\\deftab720{\\fonttbl;}{\\f0\\fnil \\froman \\fswiss \\fmodern \\fscript \\fdecor MS Sans SerifSymbolArialTimes New RomanCourier{\\colortbl\\red0\\green0\\blue0\n\r\\par \\pard\\plain\\f0\\fs20\\b\\i\\u\\tab\\tx".getBytes("US-ASCII");
            System.arraycopy(preload, 0, dict, 0, preload.length);
            return preload.length;
        }
        catch (UnsupportedEncodingException e) {
            throw new RuntimeException("Your JVM is broken as it doesn't support US ASCII");
        }
    }
    
    static {
        COMPRESSED_SIGNATURE = new byte[] { 76, 90, 70, 117 };
        UNCOMPRESSED_SIGNATURE = new byte[] { 77, 69, 76, 65 };
        COMPRESSED_SIGNATURE_INT = LittleEndian.getInt(CompressedRTF.COMPRESSED_SIGNATURE);
        UNCOMPRESSED_SIGNATURE_INT = LittleEndian.getInt(CompressedRTF.UNCOMPRESSED_SIGNATURE);
    }
}
