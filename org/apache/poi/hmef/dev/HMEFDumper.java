// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hmef.dev;

import java.util.Iterator;
import java.util.List;
import org.apache.poi.hmef.attribute.MAPIAttribute;
import org.apache.poi.util.HexDump;
import org.apache.poi.hmef.attribute.TNEFDateAttribute;
import org.apache.poi.hmef.attribute.TNEFStringAttribute;
import org.apache.poi.hmef.attribute.TNEFProperty;
import org.apache.poi.hmef.attribute.TNEFAttribute;
import java.io.IOException;
import org.apache.poi.util.LittleEndian;
import java.io.FileInputStream;
import java.io.InputStream;

public final class HMEFDumper
{
    private InputStream inp;
    private boolean truncatePropertyData;
    
    public static void main(final String[] args) throws Exception {
        if (args.length < 1) {
            throw new IllegalArgumentException("Filename must be given");
        }
        boolean truncatePropData = true;
        for (int i = 0; i < args.length; ++i) {
            if (args[i].equalsIgnoreCase("--full")) {
                truncatePropData = false;
            }
            else {
                final HMEFDumper dumper = new HMEFDumper(new FileInputStream(args[i]));
                dumper.setTruncatePropertyData(truncatePropData);
                dumper.dump();
            }
        }
    }
    
    public HMEFDumper(final InputStream inp) throws IOException {
        this.inp = inp;
        final long sig = LittleEndian.readInt(inp);
        if (sig != 574529400L) {
            throw new IllegalArgumentException("TNEF signature not detected in file, expected 574529400 but got " + sig);
        }
        LittleEndian.readUShort(inp);
    }
    
    public void setTruncatePropertyData(final boolean truncate) {
        this.truncatePropertyData = truncate;
    }
    
    private void dump() throws IOException {
        int attachments = 0;
        while (true) {
            final int level = this.inp.read();
            if (level == -1) {
                break;
            }
            final TNEFAttribute attr = TNEFAttribute.create(this.inp);
            if (level == 2 && attr.getProperty() == TNEFProperty.ID_ATTACHRENDERDATA) {
                ++attachments;
                System.out.println();
                System.out.println("Attachment # " + attachments);
                System.out.println();
            }
            System.out.println("Level " + level + " : Type " + attr.getType() + " : ID " + attr.getProperty().toString());
            final String indent = "  ";
            if (attr instanceof TNEFStringAttribute) {
                System.out.println(indent + indent + indent + ((TNEFStringAttribute)attr).getString());
            }
            if (attr instanceof TNEFDateAttribute) {
                System.out.println(indent + indent + indent + ((TNEFDateAttribute)attr).getDate());
            }
            System.out.println(indent + "Data of length " + attr.getData().length);
            if (attr.getData().length > 0) {
                int len = attr.getData().length;
                if (this.truncatePropertyData) {
                    len = Math.min(attr.getData().length, 48);
                }
                int loops = len / 16;
                if (loops == 0) {
                    loops = 1;
                }
                for (int i = 0; i < loops; ++i) {
                    int thisLen = 16;
                    final int offset = i * 16;
                    if (i == loops - 1) {
                        thisLen = len - offset;
                    }
                    final byte[] data = new byte[thisLen];
                    System.arraycopy(attr.getData(), offset, data, 0, thisLen);
                    System.out.print(indent + HexDump.dump(data, 0L, 0));
                }
            }
            System.out.println();
            if (attr.getProperty() != TNEFProperty.ID_MAPIPROPERTIES && attr.getProperty() != TNEFProperty.ID_ATTACHMENT) {
                continue;
            }
            final List<MAPIAttribute> attrs = MAPIAttribute.create(attr);
            for (final MAPIAttribute ma : attrs) {
                System.out.println(indent + indent + ma);
            }
            System.out.println();
        }
    }
}
