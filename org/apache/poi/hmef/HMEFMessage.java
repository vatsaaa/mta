// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hmef;

import org.apache.poi.hmef.attribute.MAPIStringAttribute;
import org.apache.poi.hsmf.datatypes.MAPIProperty;
import java.util.Iterator;
import org.apache.poi.hmef.attribute.TNEFProperty;
import java.util.Collection;
import org.apache.poi.hmef.attribute.TNEFMAPIAttribute;
import java.io.IOException;
import org.apache.poi.util.LittleEndian;
import java.util.ArrayList;
import java.io.InputStream;
import org.apache.poi.hmef.attribute.MAPIAttribute;
import org.apache.poi.hmef.attribute.TNEFAttribute;
import java.util.List;

public final class HMEFMessage
{
    public static final long HEADER_SIGNATURE = 574529400L;
    private int fileId;
    private List<TNEFAttribute> messageAttributes;
    private List<MAPIAttribute> mapiAttributes;
    private List<Attachment> attachments;
    
    public HMEFMessage(final InputStream inp) throws IOException {
        this.messageAttributes = new ArrayList<TNEFAttribute>();
        this.mapiAttributes = new ArrayList<MAPIAttribute>();
        this.attachments = new ArrayList<Attachment>();
        final long sig = LittleEndian.readInt(inp);
        if (sig != 574529400L) {
            throw new IllegalArgumentException("TNEF signature not detected in file, expected 574529400 but got " + sig);
        }
        this.fileId = LittleEndian.readUShort(inp);
        this.process(inp, 0);
    }
    
    private void process(final InputStream inp, final int lastLevel) throws IOException {
        final int level = inp.read();
        if (level == -1) {
            return;
        }
        final TNEFAttribute attr = TNEFAttribute.create(inp);
        if (level == 1) {
            this.messageAttributes.add(attr);
            if (attr instanceof TNEFMAPIAttribute) {
                final TNEFMAPIAttribute tnefMAPI = (TNEFMAPIAttribute)attr;
                this.mapiAttributes.addAll(tnefMAPI.getMAPIAttributes());
            }
        }
        else {
            if (level != 2) {
                throw new IllegalStateException("Unhandled level " + level);
            }
            if (this.attachments.size() == 0 || attr.getProperty() == TNEFProperty.ID_ATTACHRENDERDATA) {
                this.attachments.add(new Attachment());
            }
            final Attachment attach = this.attachments.get(this.attachments.size() - 1);
            attach.addAttribute(attr);
        }
        this.process(inp, level);
    }
    
    public List<TNEFAttribute> getMessageAttributes() {
        return this.messageAttributes;
    }
    
    public List<MAPIAttribute> getMessageMAPIAttributes() {
        return this.mapiAttributes;
    }
    
    public List<Attachment> getAttachments() {
        return this.attachments;
    }
    
    public TNEFAttribute getMessageAttribute(final TNEFProperty id) {
        for (final TNEFAttribute attr : this.messageAttributes) {
            if (attr.getProperty() == id) {
                return attr;
            }
        }
        return null;
    }
    
    public MAPIAttribute getMessageMAPIAttribute(final MAPIProperty id) {
        for (final MAPIAttribute attr : this.mapiAttributes) {
            if (attr.getProperty() == id) {
                return attr;
            }
        }
        return null;
    }
    
    private String getString(final MAPIProperty id) {
        return MAPIStringAttribute.getAsString(this.getMessageMAPIAttribute(id));
    }
    
    public String getSubject() {
        return this.getString(MAPIProperty.CONVERSATION_TOPIC);
    }
    
    public String getBody() {
        return this.getString(MAPIProperty.RTF_COMPRESSED);
    }
}
