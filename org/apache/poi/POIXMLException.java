// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi;

public final class POIXMLException extends RuntimeException
{
    public POIXMLException() {
    }
    
    public POIXMLException(final String msg) {
        super(msg);
    }
    
    public POIXMLException(final String msg, final Throwable cause) {
        super(msg, cause);
    }
    
    public POIXMLException(final Throwable cause) {
        super(cause);
    }
}
