// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hdgf;

import java.io.InputStream;
import java.io.IOException;
import org.apache.poi.util.LZWDecompresser;
import java.io.OutputStream;

final class HDGFLZWCompressor
{
    byte[] dict;
    byte[] buffer;
    int bufferLen;
    byte[] rawCode;
    int rawCodeLen;
    int posInp;
    int posOut;
    int nextMask;
    int maskBitsSet;
    
    public HDGFLZWCompressor() {
        this.dict = new byte[4096];
        this.buffer = new byte[16];
        this.bufferLen = 0;
        this.rawCode = new byte[18];
        this.rawCodeLen = 0;
        this.posInp = 0;
        this.posOut = 0;
        this.nextMask = 0;
        this.maskBitsSet = 0;
    }
    
    private int findRawCodeInBuffer() {
        for (int i = 4096 - this.rawCodeLen; i > 0; --i) {
            boolean matches = true;
            for (int j = 0; matches && j < this.rawCodeLen; ++j) {
                if (this.dict[i + j] != this.rawCode[j]) {
                    matches = false;
                }
            }
            if (matches) {
                return i;
            }
        }
        return -1;
    }
    
    private void outputCompressed(final OutputStream res) throws IOException {
        if (this.rawCodeLen < 3) {
            for (int i = 0; i < this.rawCodeLen; ++i) {
                this.outputUncompressed(this.rawCode[i], res);
            }
            return;
        }
        int codesAt = this.findRawCodeInBuffer();
        codesAt -= 18;
        if (codesAt < 0) {
            codesAt += 4096;
        }
        ++this.maskBitsSet;
        final int bp1 = codesAt & 0xFF;
        final int bp2 = this.rawCodeLen - 3 + (codesAt - bp1 >> 4);
        this.buffer[this.bufferLen] = LZWDecompresser.fromInt(bp1);
        ++this.bufferLen;
        this.buffer[this.bufferLen] = LZWDecompresser.fromInt(bp2);
        ++this.bufferLen;
        for (int j = 0; j < this.rawCodeLen; ++j) {
            this.dict[this.posOut & 0xFFF] = this.rawCode[j];
            ++this.posOut;
        }
        if (this.maskBitsSet == 8) {
            this.output8Codes(res);
        }
    }
    
    private void outputUncompressed(final byte b, final OutputStream res) throws IOException {
        this.nextMask += 1 << this.maskBitsSet;
        ++this.maskBitsSet;
        this.buffer[this.bufferLen] = b;
        ++this.bufferLen;
        this.dict[this.posOut & 0xFFF] = b;
        ++this.posOut;
        if (this.maskBitsSet == 8) {
            this.output8Codes(res);
        }
    }
    
    private void output8Codes(final OutputStream res) throws IOException {
        res.write(new byte[] { LZWDecompresser.fromInt(this.nextMask) });
        res.write(this.buffer, 0, this.bufferLen);
        this.nextMask = 0;
        this.maskBitsSet = 0;
        this.bufferLen = 0;
    }
    
    public void compress(final InputStream src, final OutputStream res) throws IOException {
        boolean going = true;
        while (going) {
            final int dataI = src.read();
            ++this.posInp;
            if (dataI == -1) {
                going = false;
            }
            final byte dataB = LZWDecompresser.fromInt(dataI);
            if (!going) {
                if (this.rawCodeLen <= 0) {
                    break;
                }
                this.outputCompressed(res);
                if (this.maskBitsSet > 0) {
                    this.output8Codes(res);
                    break;
                }
                break;
            }
            else {
                this.rawCode[this.rawCodeLen] = dataB;
                ++this.rawCodeLen;
                final int rawAt = this.findRawCodeInBuffer();
                if (this.rawCodeLen == 18 && rawAt > -1) {
                    this.outputCompressed(res);
                    this.rawCodeLen = 0;
                }
                else {
                    if (rawAt > -1) {
                        continue;
                    }
                    --this.rawCodeLen;
                    if (this.rawCodeLen > 0) {
                        this.outputCompressed(res);
                        this.rawCode[0] = dataB;
                        this.rawCodeLen = 1;
                        if (this.findRawCodeInBuffer() > -1) {
                            continue;
                        }
                        this.outputUncompressed(dataB, res);
                        this.rawCodeLen = 0;
                    }
                    else {
                        this.outputUncompressed(dataB, res);
                    }
                }
            }
        }
    }
}
