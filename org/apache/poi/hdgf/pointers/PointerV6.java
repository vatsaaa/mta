// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hdgf.pointers;

public final class PointerV6 extends Pointer
{
    @Override
    public boolean destinationHasStrings() {
        return 64 <= this.format && this.format < 80;
    }
    
    @Override
    public boolean destinationHasPointers() {
        return this.type == 20 || (this.format == 29 || this.format == 30) || (80 <= this.format && this.format < 96);
    }
    
    @Override
    public boolean destinationHasChunks() {
        return 208 <= this.format && this.format < 223;
    }
    
    @Override
    public boolean destinationCompressed() {
        return (this.format & 0x2) > 0;
    }
    
    @Override
    public int getSizeInBytes() {
        return 18;
    }
}
