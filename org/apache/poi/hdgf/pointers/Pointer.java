// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hdgf.pointers;

public abstract class Pointer
{
    protected int type;
    protected int address;
    protected int offset;
    protected int length;
    protected short format;
    
    public int getAddress() {
        return this.address;
    }
    
    public short getFormat() {
        return this.format;
    }
    
    public int getLength() {
        return this.length;
    }
    
    public int getOffset() {
        return this.offset;
    }
    
    public int getType() {
        return this.type;
    }
    
    public abstract int getSizeInBytes();
    
    public abstract boolean destinationHasStrings();
    
    public abstract boolean destinationHasPointers();
    
    public abstract boolean destinationHasChunks();
    
    public abstract boolean destinationCompressed();
}
