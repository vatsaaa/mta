// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hdgf.pointers;

import org.apache.poi.util.LittleEndian;

public final class PointerFactory
{
    private int version;
    
    public PointerFactory(final int version) {
        this.version = version;
    }
    
    public int getVersion() {
        return this.version;
    }
    
    public Pointer createPointer(final byte[] data, final int offset) {
        if (this.version >= 6) {
            final Pointer p = new PointerV6();
            p.type = LittleEndian.getInt(data, offset + 0);
            p.address = (int)LittleEndian.getUInt(data, offset + 4);
            p.offset = (int)LittleEndian.getUInt(data, offset + 8);
            p.length = (int)LittleEndian.getUInt(data, offset + 12);
            p.format = LittleEndian.getShort(data, offset + 16);
            return p;
        }
        if (this.version == 5) {
            throw new RuntimeException("TODO");
        }
        throw new IllegalArgumentException("Visio files with versions below 5 are not supported, yours was " + this.version);
    }
}
