// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hdgf;

import java.io.InputStream;
import java.io.FileInputStream;
import java.io.OutputStream;
import org.apache.poi.hdgf.streams.StringsStream;
import org.apache.poi.hdgf.streams.PointerContainingStream;
import org.apache.poi.hdgf.streams.Stream;
import org.apache.poi.util.LittleEndian;
import org.apache.poi.poifs.filesystem.DocumentEntry;
import org.apache.poi.poifs.filesystem.DirectoryNode;
import org.apache.poi.poifs.filesystem.NPOIFSFileSystem;
import java.io.IOException;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.hdgf.pointers.PointerFactory;
import org.apache.poi.hdgf.chunks.ChunkFactory;
import org.apache.poi.hdgf.streams.TrailerStream;
import org.apache.poi.hdgf.pointers.Pointer;
import org.apache.poi.POIDocument;

public final class HDGFDiagram extends POIDocument
{
    private static final String VISIO_HEADER = "Visio (TM) Drawing\r\n";
    private byte[] _docstream;
    private short version;
    private long docSize;
    private Pointer trailerPointer;
    private TrailerStream trailer;
    private ChunkFactory chunkFactory;
    private PointerFactory ptrFactory;
    
    public HDGFDiagram(final POIFSFileSystem fs) throws IOException {
        this(fs.getRoot());
    }
    
    public HDGFDiagram(final NPOIFSFileSystem fs) throws IOException {
        this(fs.getRoot());
    }
    
    @Deprecated
    public HDGFDiagram(final DirectoryNode dir, final POIFSFileSystem fs) throws IOException {
        this(dir);
    }
    
    public HDGFDiagram(final DirectoryNode dir) throws IOException {
        super(dir);
        final DocumentEntry docProps = (DocumentEntry)dir.getEntry("VisioDocument");
        this._docstream = new byte[docProps.getSize()];
        dir.createDocumentInputStream("VisioDocument").read(this._docstream);
        final String typeString = new String(this._docstream, 0, 20);
        if (!typeString.equals("Visio (TM) Drawing\r\n")) {
            throw new IllegalArgumentException("Wasn't a valid visio document, started with " + typeString);
        }
        this.version = LittleEndian.getShort(this._docstream, 26);
        this.docSize = LittleEndian.getUInt(this._docstream, 28);
        this.ptrFactory = new PointerFactory(this.version);
        this.chunkFactory = new ChunkFactory(this.version);
        this.trailerPointer = this.ptrFactory.createPointer(this._docstream, 36);
        (this.trailer = (TrailerStream)Stream.createStream(this.trailerPointer, this._docstream, this.chunkFactory, this.ptrFactory)).findChildren(this._docstream);
    }
    
    public TrailerStream getTrailerStream() {
        return this.trailer;
    }
    
    public Stream[] getTopLevelStreams() {
        return this.trailer.getPointedToStreams();
    }
    
    public long getDocumentSize() {
        return this.docSize;
    }
    
    public void debug() {
        System.err.println("Trailer is at " + this.trailerPointer.getOffset());
        System.err.println("Trailer has type " + this.trailerPointer.getType());
        System.err.println("Trailer has length " + this.trailerPointer.getLength());
        System.err.println("Trailer has format " + this.trailerPointer.getFormat());
        for (int i = 0; i < this.trailer.getPointedToStreams().length; ++i) {
            final Stream stream = this.trailer.getPointedToStreams()[i];
            final Pointer ptr = stream.getPointer();
            System.err.println("Looking at pointer " + i);
            System.err.println("\tType is " + ptr.getType() + "\t\t" + Integer.toHexString(ptr.getType()));
            System.err.println("\tOffset is " + ptr.getOffset() + "\t\t" + Long.toHexString(ptr.getOffset()));
            System.err.println("\tAddress is " + ptr.getAddress() + "\t" + Long.toHexString(ptr.getAddress()));
            System.err.println("\tLength is " + ptr.getLength() + "\t\t" + Long.toHexString(ptr.getLength()));
            System.err.println("\tFormat is " + ptr.getFormat() + "\t\t" + Long.toHexString(ptr.getFormat()));
            System.err.println("\tCompressed is " + ptr.destinationCompressed());
            System.err.println("\tStream is " + stream.getClass());
            if (stream instanceof PointerContainingStream) {
                final PointerContainingStream pcs = (PointerContainingStream)stream;
                if (pcs.getPointedToStreams() != null && pcs.getPointedToStreams().length > 0) {
                    System.err.println("\tContains " + pcs.getPointedToStreams().length + " other pointers/streams");
                    for (int j = 0; j < pcs.getPointedToStreams().length; ++j) {
                        final Stream ss = pcs.getPointedToStreams()[j];
                        final Pointer sptr = ss.getPointer();
                        System.err.println("\t\t" + j + " - Type is " + sptr.getType() + "\t\t" + Integer.toHexString(sptr.getType()));
                        System.err.println("\t\t" + j + " - Length is " + sptr.getLength() + "\t\t" + Long.toHexString(sptr.getLength()));
                    }
                }
            }
            if (stream instanceof StringsStream) {
                System.err.println("\t\t**strings**");
                final StringsStream ss2 = (StringsStream)stream;
                System.err.println("\t\t" + ss2._getContentsLength());
            }
        }
    }
    
    @Override
    public void write(final OutputStream out) {
        throw new IllegalStateException("Writing is not yet implemented, see http://poi.apache.org/hdgf/");
    }
    
    public static void main(final String[] args) throws Exception {
        final HDGFDiagram hdgf = new HDGFDiagram(new POIFSFileSystem(new FileInputStream(args[0])));
        hdgf.debug();
    }
}
