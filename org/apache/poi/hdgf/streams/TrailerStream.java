// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hdgf.streams;

import org.apache.poi.hdgf.pointers.PointerFactory;
import org.apache.poi.hdgf.chunks.ChunkFactory;
import org.apache.poi.hdgf.pointers.Pointer;

public class TrailerStream extends PointerContainingStream
{
    protected TrailerStream(final Pointer pointer, final StreamStore store, final ChunkFactory chunkFactory, final PointerFactory pointerFactory) {
        super(pointer, store, chunkFactory, pointerFactory);
    }
}
