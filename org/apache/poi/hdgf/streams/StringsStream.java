// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hdgf.streams;

import org.apache.poi.hdgf.chunks.ChunkFactory;
import org.apache.poi.hdgf.pointers.Pointer;

public final class StringsStream extends Stream
{
    protected StringsStream(final Pointer pointer, final StreamStore store, final ChunkFactory chunkFactory) {
        super(pointer, store);
    }
}
