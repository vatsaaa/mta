// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hdgf.streams;

import org.apache.poi.util.LittleEndian;
import org.apache.poi.hdgf.pointers.PointerFactory;
import org.apache.poi.hdgf.chunks.ChunkFactory;
import org.apache.poi.hdgf.pointers.Pointer;

public class PointerContainingStream extends Stream
{
    private Pointer[] childPointers;
    private Stream[] childStreams;
    private ChunkFactory chunkFactory;
    private PointerFactory pointerFactory;
    private int numPointersLocalOffset;
    
    protected PointerContainingStream(final Pointer pointer, final StreamStore store, final ChunkFactory chunkFactory, final PointerFactory pointerFactory) {
        super(pointer, store);
        this.chunkFactory = chunkFactory;
        this.pointerFactory = pointerFactory;
        this.numPointersLocalOffset = (int)LittleEndian.getUInt(store.getContents(), 0);
        final int numPointers = (int)LittleEndian.getUInt(store.getContents(), this.numPointersLocalOffset);
        this.childPointers = new Pointer[numPointers];
        int pos = this.numPointersLocalOffset + 4 + 4;
        for (int i = 0; i < numPointers; ++i) {
            this.childPointers[i] = pointerFactory.createPointer(store.getContents(), pos);
            pos += this.childPointers[i].getSizeInBytes();
        }
    }
    
    protected Pointer[] getChildPointers() {
        return this.childPointers;
    }
    
    public Stream[] getPointedToStreams() {
        return this.childStreams;
    }
    
    public void findChildren(final byte[] documentData) {
        this.childStreams = new Stream[this.childPointers.length];
        for (int i = 0; i < this.childPointers.length; ++i) {
            final Pointer ptr = this.childPointers[i];
            this.childStreams[i] = Stream.createStream(ptr, documentData, this.chunkFactory, this.pointerFactory);
            if (this.childStreams[i] instanceof ChunkStream) {
                final ChunkStream child = (ChunkStream)this.childStreams[i];
                child.findChunks();
            }
            if (this.childStreams[i] instanceof PointerContainingStream) {
                final PointerContainingStream child2 = (PointerContainingStream)this.childStreams[i];
                child2.findChildren(documentData);
            }
        }
    }
}
