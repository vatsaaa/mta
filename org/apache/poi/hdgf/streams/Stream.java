// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hdgf.streams;

import java.io.IOException;
import org.apache.poi.hdgf.exceptions.HDGFException;
import org.apache.poi.hdgf.pointers.PointerFactory;
import org.apache.poi.hdgf.chunks.ChunkFactory;
import org.apache.poi.hdgf.pointers.Pointer;

public abstract class Stream
{
    private Pointer pointer;
    private StreamStore store;
    
    public Pointer getPointer() {
        return this.pointer;
    }
    
    protected StreamStore getStore() {
        return this.store;
    }
    
    public StreamStore _getStore() {
        return this.store;
    }
    
    public int _getContentsLength() {
        return this.store.getContents().length;
    }
    
    protected Stream(final Pointer pointer, final StreamStore store) {
        this.pointer = pointer;
        this.store = store;
    }
    
    public static Stream createStream(final Pointer pointer, final byte[] documentData, final ChunkFactory chunkFactory, final PointerFactory pointerFactory) {
        StreamStore store = null;
        Label_0058: {
            if (pointer.destinationCompressed()) {
                try {
                    store = new CompressedStreamStore(documentData, pointer.getOffset(), pointer.getLength());
                    break Label_0058;
                }
                catch (IOException e) {
                    throw new HDGFException(e);
                }
            }
            store = new StreamStore(documentData, pointer.getOffset(), pointer.getLength());
        }
        if (pointer.getType() == 20) {
            return new TrailerStream(pointer, store, chunkFactory, pointerFactory);
        }
        if (pointer.destinationHasPointers()) {
            return new PointerContainingStream(pointer, store, chunkFactory, pointerFactory);
        }
        if (pointer.destinationHasChunks()) {
            return new ChunkStream(pointer, store, chunkFactory);
        }
        if (pointer.destinationHasStrings()) {
            return new StringsStream(pointer, store, chunkFactory);
        }
        return new UnknownStream(pointer, store);
    }
}
