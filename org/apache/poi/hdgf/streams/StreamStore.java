// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hdgf.streams;

public class StreamStore
{
    private byte[] contents;
    
    protected StreamStore(final byte[] data, final int offset, final int length) {
        System.arraycopy(data, offset, this.contents = new byte[length], 0, length);
    }
    
    protected void prependContentsWith(final byte[] b) {
        final byte[] newContents = new byte[this.contents.length + b.length];
        System.arraycopy(b, 0, newContents, 0, b.length);
        System.arraycopy(this.contents, 0, newContents, b.length, this.contents.length);
        this.contents = newContents;
    }
    
    protected void copyBlockHeaderToContents() {
    }
    
    protected byte[] getContents() {
        return this.contents;
    }
    
    public byte[] _getContents() {
        return this.contents;
    }
}
