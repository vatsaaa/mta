// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hdgf.streams;

import org.apache.poi.hdgf.chunks.ChunkHeader;
import java.util.ArrayList;
import org.apache.poi.hdgf.pointers.Pointer;
import org.apache.poi.hdgf.chunks.Chunk;
import org.apache.poi.hdgf.chunks.ChunkFactory;

public final class ChunkStream extends Stream
{
    private ChunkFactory chunkFactory;
    private Chunk[] chunks;
    
    protected ChunkStream(final Pointer pointer, final StreamStore store, final ChunkFactory chunkFactory) {
        super(pointer, store);
        this.chunkFactory = chunkFactory;
        store.copyBlockHeaderToContents();
    }
    
    public Chunk[] getChunks() {
        return this.chunks;
    }
    
    public void findChunks() {
        final ArrayList<Chunk> chunksA = new ArrayList<Chunk>();
        if (this.getPointer().getOffset() == 25779) {
            int i = 0;
            ++i;
        }
        int pos = 0;
        final byte[] contents = this.getStore().getContents();
        while (pos < contents.length) {
            final int headerSize = ChunkHeader.getHeaderSize(this.chunkFactory.getVersion());
            if (pos + headerSize <= contents.length) {
                final Chunk chunk = this.chunkFactory.createChunk(contents, pos);
                chunksA.add(chunk);
                pos += chunk.getOnDiskSize();
            }
            else {
                System.err.println("Needed " + headerSize + " bytes to create the next chunk header, but only found " + (contents.length - pos) + " bytes, ignoring rest of data");
                pos = contents.length;
            }
        }
        this.chunks = chunksA.toArray(new Chunk[chunksA.size()]);
    }
}
