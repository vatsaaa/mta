// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hdgf.streams;

import java.io.InputStream;
import org.apache.poi.hdgf.HDGFLZW;
import java.io.ByteArrayInputStream;
import java.io.IOException;

public final class CompressedStreamStore extends StreamStore
{
    private byte[] compressedContents;
    private byte[] blockHeader;
    private boolean blockHeaderInContents;
    
    protected byte[] _getCompressedContents() {
        return this.compressedContents;
    }
    
    protected byte[] _getBlockHeader() {
        return this.blockHeader;
    }
    
    protected CompressedStreamStore(final byte[] data, final int offset, final int length) throws IOException {
        this(decompress(data, offset, length));
        System.arraycopy(data, offset, this.compressedContents = new byte[length], 0, length);
    }
    
    private CompressedStreamStore(final byte[][] decompressedData) {
        super(decompressedData[1], 0, decompressedData[1].length);
        this.blockHeader = new byte[4];
        this.blockHeaderInContents = false;
        this.blockHeader = decompressedData[0];
    }
    
    @Override
    protected void copyBlockHeaderToContents() {
        if (this.blockHeaderInContents) {
            return;
        }
        this.prependContentsWith(this.blockHeader);
        this.blockHeaderInContents = true;
    }
    
    public static byte[][] decompress(final byte[] data, final int offset, final int length) throws IOException {
        final ByteArrayInputStream bais = new ByteArrayInputStream(data, offset, length);
        final HDGFLZW lzw = new HDGFLZW();
        final byte[] decompressed = lzw.decompress(bais);
        final byte[][] ret = { new byte[4], new byte[decompressed.length - 4] };
        System.arraycopy(decompressed, 0, ret[0], 0, 4);
        System.arraycopy(decompressed, 4, ret[1], 0, ret[1].length);
        return ret;
    }
}
