// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hdgf.streams;

import org.apache.poi.hdgf.pointers.Pointer;

public final class UnknownStream extends Stream
{
    protected UnknownStream(final Pointer pointer, final StreamStore store) {
        super(pointer, store);
    }
}
