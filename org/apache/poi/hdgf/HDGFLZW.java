// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hdgf;

import java.io.IOException;
import java.io.OutputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import org.apache.poi.util.LZWDecompresser;

public class HDGFLZW extends LZWDecompresser
{
    public HDGFLZW() {
        super(false, 3, false);
    }
    
    public byte[] compress(final InputStream src) throws IOException {
        final ByteArrayOutputStream res = new ByteArrayOutputStream();
        this.compress(src, res);
        return res.toByteArray();
    }
    
    @Override
    protected int adjustDictionaryOffset(int pntr) {
        if (pntr > 4078) {
            pntr -= 4078;
        }
        else {
            pntr += 18;
        }
        return pntr;
    }
    
    @Override
    protected int populateDictionary(final byte[] dict) {
        return 0;
    }
    
    public void compress(final InputStream src, final OutputStream res) throws IOException {
        final HDGFLZWCompressor c = new HDGFLZWCompressor();
        c.compress(src, res);
    }
}
