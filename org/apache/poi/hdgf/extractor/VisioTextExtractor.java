// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hdgf.extractor;

import java.io.FileInputStream;
import org.apache.poi.hdgf.chunks.Chunk;
import org.apache.poi.hdgf.streams.ChunkStream;
import org.apache.poi.hdgf.streams.PointerContainingStream;
import org.apache.poi.hdgf.streams.Stream;
import java.util.ArrayList;
import java.io.InputStream;
import org.apache.poi.poifs.filesystem.DirectoryNode;
import org.apache.poi.poifs.filesystem.NPOIFSFileSystem;
import java.io.IOException;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.POIDocument;
import org.apache.poi.hdgf.HDGFDiagram;
import org.apache.poi.POIOLE2TextExtractor;

public final class VisioTextExtractor extends POIOLE2TextExtractor
{
    private HDGFDiagram hdgf;
    
    public VisioTextExtractor(final HDGFDiagram hdgf) {
        super(hdgf);
        this.hdgf = hdgf;
    }
    
    public VisioTextExtractor(final POIFSFileSystem fs) throws IOException {
        this(fs.getRoot());
    }
    
    public VisioTextExtractor(final NPOIFSFileSystem fs) throws IOException {
        this(fs.getRoot());
    }
    
    public VisioTextExtractor(final DirectoryNode dir) throws IOException {
        this(new HDGFDiagram(dir));
    }
    
    @Deprecated
    public VisioTextExtractor(final DirectoryNode dir, final POIFSFileSystem fs) throws IOException {
        this(new HDGFDiagram(dir, fs));
    }
    
    public VisioTextExtractor(final InputStream inp) throws IOException {
        this(new POIFSFileSystem(inp));
    }
    
    public String[] getAllText() {
        final ArrayList<String> text = new ArrayList<String>();
        for (int i = 0; i < this.hdgf.getTopLevelStreams().length; ++i) {
            this.findText(this.hdgf.getTopLevelStreams()[i], text);
        }
        return text.toArray(new String[text.size()]);
    }
    
    private void findText(final Stream stream, final ArrayList<String> text) {
        if (stream instanceof PointerContainingStream) {
            final PointerContainingStream ps = (PointerContainingStream)stream;
            for (int i = 0; i < ps.getPointedToStreams().length; ++i) {
                this.findText(ps.getPointedToStreams()[i], text);
            }
        }
        if (stream instanceof ChunkStream) {
            final ChunkStream cs = (ChunkStream)stream;
            for (int i = 0; i < cs.getChunks().length; ++i) {
                final Chunk chunk = cs.getChunks()[i];
                if (chunk != null && chunk.getName() != null && chunk.getName().equals("Text") && chunk.getCommands().length > 0) {
                    final Chunk.Command cmd = chunk.getCommands()[0];
                    if (cmd != null && cmd.getValue() != null) {
                        final String str = cmd.getValue().toString();
                        if (!str.equals("")) {
                            if (!str.equals("\n")) {
                                text.add(str);
                            }
                        }
                    }
                }
            }
        }
    }
    
    @Override
    public String getText() {
        final StringBuffer text = new StringBuffer();
        final String[] allText = this.getAllText();
        for (int i = 0; i < allText.length; ++i) {
            text.append(allText[i]);
            if (!allText[i].endsWith("\r") && !allText[i].endsWith("\n")) {
                text.append("\n");
            }
        }
        return text.toString();
    }
    
    public static void main(final String[] args) throws Exception {
        if (args.length == 0) {
            System.err.println("Use:");
            System.err.println("   VisioTextExtractor <file.vsd>");
            System.exit(1);
        }
        final VisioTextExtractor extractor = new VisioTextExtractor(new FileInputStream(args[0]));
        System.out.print(extractor.getText());
    }
}
