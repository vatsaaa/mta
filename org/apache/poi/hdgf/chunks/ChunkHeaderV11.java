// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hdgf.chunks;

public final class ChunkHeaderV11 extends ChunkHeaderV6
{
    @Override
    public boolean hasSeparator() {
        return this.type != 31 && this.type != 201 && (this.hasTrailer() || (this.unknown2 == 2 && this.unknown3 == 85) || (this.unknown2 == 2 && this.unknown3 == 84 && this.type == 169) || (this.unknown2 == 2 && this.unknown3 == 84 && this.type == 170) || (this.unknown2 == 2 && this.unknown3 == 84 && this.type == 180) || (this.unknown2 == 2 && this.unknown3 == 84 && this.type == 182) || (this.unknown2 == 3 && this.unknown3 != 80) || this.type == 105);
    }
}
