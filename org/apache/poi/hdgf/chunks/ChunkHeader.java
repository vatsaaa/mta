// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hdgf.chunks;

import org.apache.poi.util.LittleEndian;

public abstract class ChunkHeader
{
    protected int type;
    protected int id;
    protected int length;
    protected int unknown1;
    
    public static ChunkHeader createChunkHeader(final int documentVersion, final byte[] data, final int offset) {
        if (documentVersion >= 6) {
            ChunkHeaderV6 ch;
            if (documentVersion > 6) {
                ch = new ChunkHeaderV11();
            }
            else {
                ch = new ChunkHeaderV6();
            }
            ch.type = (int)LittleEndian.getUInt(data, offset + 0);
            ch.id = (int)LittleEndian.getUInt(data, offset + 4);
            ch.unknown1 = (int)LittleEndian.getUInt(data, offset + 8);
            ch.length = (int)LittleEndian.getUInt(data, offset + 12);
            ch.unknown2 = LittleEndian.getShort(data, offset + 16);
            ch.unknown3 = (short)LittleEndian.getUnsignedByte(data, offset + 18);
            return ch;
        }
        if (documentVersion == 5 || documentVersion == 4) {
            final ChunkHeaderV4V5 ch2 = new ChunkHeaderV4V5();
            ch2.type = LittleEndian.getShort(data, offset + 0);
            ch2.id = LittleEndian.getShort(data, offset + 2);
            ch2.unknown2 = (short)LittleEndian.getUnsignedByte(data, offset + 4);
            ch2.unknown3 = (short)LittleEndian.getUnsignedByte(data, offset + 5);
            ch2.unknown1 = LittleEndian.getShort(data, offset + 6);
            ch2.length = (int)LittleEndian.getUInt(data, offset + 8);
            return ch2;
        }
        throw new IllegalArgumentException("Visio files with versions below 4 are not supported, yours was " + documentVersion);
    }
    
    public static int getHeaderSize(final int documentVersion) {
        if (documentVersion > 6) {
            return ChunkHeaderV6.getHeaderSize();
        }
        if (documentVersion == 6) {
            return ChunkHeaderV6.getHeaderSize();
        }
        return ChunkHeaderV4V5.getHeaderSize();
    }
    
    public abstract int getSizeInBytes();
    
    public abstract boolean hasTrailer();
    
    public abstract boolean hasSeparator();
    
    public int getId() {
        return this.id;
    }
    
    public int getLength() {
        return this.length;
    }
    
    public int getType() {
        return this.type;
    }
    
    public int getUnknown1() {
        return this.unknown1;
    }
}
