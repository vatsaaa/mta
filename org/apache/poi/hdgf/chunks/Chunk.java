// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hdgf.chunks;

import org.apache.poi.util.StringUtil;
import org.apache.poi.util.LittleEndian;
import java.util.ArrayList;
import org.apache.poi.util.POILogFactory;
import org.apache.poi.util.POILogger;

public final class Chunk
{
    private byte[] contents;
    private ChunkHeader header;
    private ChunkTrailer trailer;
    private ChunkSeparator separator;
    protected ChunkFactory.CommandDefinition[] commandDefinitions;
    private Command[] commands;
    private String name;
    private POILogger logger;
    
    public Chunk(final ChunkHeader header, final ChunkTrailer trailer, final ChunkSeparator separator, final byte[] contents) {
        this.logger = POILogFactory.getLogger(Chunk.class);
        this.header = header;
        this.trailer = trailer;
        this.separator = separator;
        this.contents = contents;
    }
    
    public byte[] _getContents() {
        return this.contents;
    }
    
    public ChunkHeader getHeader() {
        return this.header;
    }
    
    public ChunkSeparator getSeparator() {
        return this.separator;
    }
    
    public ChunkTrailer getTrailer() {
        return this.trailer;
    }
    
    public ChunkFactory.CommandDefinition[] getCommandDefinitions() {
        return this.commandDefinitions;
    }
    
    public Command[] getCommands() {
        return this.commands;
    }
    
    public String getName() {
        return this.name;
    }
    
    public int getOnDiskSize() {
        int size = this.header.getSizeInBytes() + this.contents.length;
        if (this.trailer != null) {
            size += this.trailer.trailerData.length;
        }
        if (this.separator != null) {
            size += this.separator.separatorData.length;
        }
        return size;
    }
    
    protected void processCommands() {
        if (this.commandDefinitions == null) {
            throw new IllegalStateException("You must supply the command definitions before calling processCommands!");
        }
        final ArrayList<Command> commands = new ArrayList<Command>();
        for (int i = 0; i < this.commandDefinitions.length; ++i) {
            final int type = this.commandDefinitions[i].getType();
            int offset = this.commandDefinitions[i].getOffset();
            if (type == 10) {
                this.name = this.commandDefinitions[i].getName();
            }
            else if (type != 18) {
                Command command;
                if (type == 11 || type == 21) {
                    command = new BlockOffsetCommand(this.commandDefinitions[i]);
                }
                else {
                    command = new Command(this.commandDefinitions[i]);
                }
                switch (type) {
                    case 0:
                    case 1:
                    case 2:
                    case 3:
                    case 4:
                    case 5:
                    case 6:
                    case 7:
                    case 11:
                    case 12:
                    case 16:
                    case 17:
                    case 18:
                    case 21:
                    case 28:
                    case 29: {
                        break;
                    }
                    default: {
                        if (offset >= 19) {
                            offset -= 19;
                            break;
                        }
                        break;
                    }
                }
                if (offset >= this.contents.length) {
                    this.logger.log(POILogger.WARN, "Command offset " + offset + " past end of data at " + this.contents.length);
                }
                else {
                    switch (type) {
                        case 0:
                        case 1:
                        case 2:
                        case 3:
                        case 4:
                        case 5:
                        case 6:
                        case 7: {
                            final int val = this.contents[offset] & 1 << type;
                            command.value = (val > 0);
                            break;
                        }
                        case 8: {
                            command.value = this.contents[offset];
                            break;
                        }
                        case 9: {
                            command.value = new Double(LittleEndian.getDouble(this.contents, offset));
                            break;
                        }
                        case 12: {
                            if (this.contents.length < 8) {
                                command.value = "";
                                break;
                            }
                            int endsAt;
                            int j;
                            int startsAt;
                            for (startsAt = (j = (endsAt = 8)); j < this.contents.length - 1 && endsAt == startsAt; ++j) {
                                if (this.contents[j] == 0 && this.contents[j + 1] == 0) {
                                    endsAt = j;
                                }
                            }
                            if (endsAt == startsAt) {
                                endsAt = this.contents.length;
                            }
                            final int strLen = (endsAt - startsAt) / 2;
                            command.value = StringUtil.getFromUnicodeLE(this.contents, startsAt, strLen);
                            break;
                        }
                        case 25: {
                            command.value = LittleEndian.getShort(this.contents, offset);
                            break;
                        }
                        case 26: {
                            command.value = LittleEndian.getInt(this.contents, offset);
                            break;
                        }
                        case 11:
                        case 21: {
                            if (offset < this.contents.length - 3) {
                                final int bOffset = (int)LittleEndian.getUInt(this.contents, offset);
                                final BlockOffsetCommand bcmd = (BlockOffsetCommand)command;
                                bcmd.setOffset(bOffset);
                                break;
                            }
                            break;
                        }
                        default: {
                            this.logger.log(POILogger.INFO, "Command of type " + type + " not processed!");
                            break;
                        }
                    }
                    commands.add(command);
                }
            }
        }
        this.commands = commands.toArray(new Command[commands.size()]);
    }
    
    public static class Command
    {
        protected Object value;
        private ChunkFactory.CommandDefinition definition;
        
        private Command(final ChunkFactory.CommandDefinition definition, final Object value) {
            this.definition = definition;
            this.value = value;
        }
        
        private Command(final ChunkFactory.CommandDefinition definition) {
            this(definition, (Object)null);
        }
        
        public ChunkFactory.CommandDefinition getDefinition() {
            return this.definition;
        }
        
        public Object getValue() {
            return this.value;
        }
    }
    
    public static class BlockOffsetCommand extends Command
    {
        private int offset;
        
        private BlockOffsetCommand(final ChunkFactory.CommandDefinition definition) {
            super(definition, (Object)null);
        }
        
        private void setOffset(final int offset) {
            this.offset = offset;
            this.value = offset;
        }
    }
}
