// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hdgf.chunks;

public final class ChunkTrailer
{
    protected byte[] trailerData;
    
    public ChunkTrailer(final byte[] data, final int offset) {
        System.arraycopy(data, offset, this.trailerData = new byte[8], 0, 8);
    }
    
    @Override
    public String toString() {
        return "<ChunkTrailer of length " + this.trailerData.length + ">";
    }
}
