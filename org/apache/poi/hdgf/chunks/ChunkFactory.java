// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hdgf.chunks;

import java.io.InputStream;
import java.util.StringTokenizer;
import java.util.ArrayList;
import java.io.Reader;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;
import org.apache.poi.util.POILogFactory;
import org.apache.poi.util.POILogger;
import java.util.Hashtable;

public final class ChunkFactory
{
    private int version;
    private Hashtable<Integer, CommandDefinition[]> chunkCommandDefinitions;
    private static String chunkTableName;
    private POILogger logger;
    
    public ChunkFactory(final int version) throws IOException {
        this.chunkCommandDefinitions = new Hashtable<Integer, CommandDefinition[]>();
        this.logger = POILogFactory.getLogger(ChunkFactory.class);
        this.version = version;
        this.processChunkParseCommands();
    }
    
    private void processChunkParseCommands() throws IOException {
        final InputStream cpd = ChunkFactory.class.getResourceAsStream(ChunkFactory.chunkTableName);
        if (cpd == null) {
            throw new IllegalStateException("Unable to find HDGF chunk definition on the classpath - " + ChunkFactory.chunkTableName);
        }
        final BufferedReader inp = new BufferedReader(new InputStreamReader(cpd));
        String line;
        while ((line = inp.readLine()) != null) {
            if (line.startsWith("#")) {
                continue;
            }
            if (line.startsWith(" ")) {
                continue;
            }
            if (line.startsWith("\t")) {
                continue;
            }
            if (line.length() == 0) {
                continue;
            }
            if (!line.startsWith("start")) {
                throw new IllegalStateException("Expecting start xxx, found " + line);
            }
            final int chunkType = Integer.parseInt(line.substring(6));
            final ArrayList<CommandDefinition> defsL = new ArrayList<CommandDefinition>();
            while (!(line = inp.readLine()).startsWith("end")) {
                final StringTokenizer st = new StringTokenizer(line, " ");
                final int defType = Integer.parseInt(st.nextToken());
                final int offset = Integer.parseInt(st.nextToken());
                final String name = st.nextToken("\uffff").substring(1);
                final CommandDefinition def = new CommandDefinition(defType, offset, name);
                defsL.add(def);
            }
            final CommandDefinition[] defs = defsL.toArray(new CommandDefinition[defsL.size()]);
            this.chunkCommandDefinitions.put(chunkType, defs);
        }
        inp.close();
        cpd.close();
    }
    
    public int getVersion() {
        return this.version;
    }
    
    public Chunk createChunk(final byte[] data, final int offset) {
        final ChunkHeader header = ChunkHeader.createChunkHeader(this.version, data, offset);
        if (header.length < 0) {
            throw new IllegalArgumentException("Found a chunk with a negative length, which isn't allowed");
        }
        int endOfDataPos = offset + header.getLength() + header.getSizeInBytes();
        if (endOfDataPos > data.length) {
            this.logger.log(POILogger.WARN, "Header called for " + header.getLength() + " bytes, but that would take us passed the end of the data!");
            endOfDataPos = data.length;
            header.length = data.length - offset - header.getSizeInBytes();
            if (header.hasTrailer()) {
                final ChunkHeader chunkHeader = header;
                chunkHeader.length -= 8;
                endOfDataPos -= 8;
            }
            if (header.hasSeparator()) {
                final ChunkHeader chunkHeader2 = header;
                chunkHeader2.length -= 4;
                endOfDataPos -= 4;
            }
        }
        ChunkTrailer trailer = null;
        ChunkSeparator separator = null;
        if (header.hasTrailer()) {
            if (endOfDataPos <= data.length - 8) {
                trailer = new ChunkTrailer(data, endOfDataPos);
                endOfDataPos += 8;
            }
            else {
                System.err.println("Header claims a length to " + endOfDataPos + " there's then no space for the trailer in the data (" + data.length + ")");
            }
        }
        if (header.hasSeparator()) {
            if (endOfDataPos <= data.length - 4) {
                separator = new ChunkSeparator(data, endOfDataPos);
            }
            else {
                System.err.println("Header claims a length to " + endOfDataPos + " there's then no space for the separator in the data (" + data.length + ")");
            }
        }
        final byte[] contents = new byte[header.getLength()];
        System.arraycopy(data, offset + header.getSizeInBytes(), contents, 0, contents.length);
        final Chunk chunk = new Chunk(header, trailer, separator, contents);
        CommandDefinition[] defs = this.chunkCommandDefinitions.get(header.getType());
        if (defs == null) {
            defs = new CommandDefinition[0];
        }
        chunk.commandDefinitions = defs;
        chunk.processCommands();
        return chunk;
    }
    
    static {
        ChunkFactory.chunkTableName = "/org/apache/poi/hdgf/chunks_parse_cmds.tbl";
    }
    
    public class CommandDefinition
    {
        private int type;
        private int offset;
        private String name;
        
        public CommandDefinition(final int type, final int offset, final String name) {
            this.type = type;
            this.offset = offset;
            this.name = name;
        }
        
        public String getName() {
            return this.name;
        }
        
        public int getOffset() {
            return this.offset;
        }
        
        public int getType() {
            return this.type;
        }
    }
}
