// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hdgf.chunks;

public final class ChunkSeparator
{
    protected byte[] separatorData;
    
    public ChunkSeparator(final byte[] data, final int offset) {
        System.arraycopy(data, offset, this.separatorData = new byte[4], 0, 4);
    }
    
    @Override
    public String toString() {
        return "<ChunkSeparator of length " + this.separatorData.length + ">";
    }
}
