// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hdgf.chunks;

public class ChunkHeaderV6 extends ChunkHeader
{
    protected short unknown2;
    protected short unknown3;
    
    public short getUnknown2() {
        return this.unknown2;
    }
    
    public short getUnknown3() {
        return this.unknown3;
    }
    
    protected static int getHeaderSize() {
        return 19;
    }
    
    @Override
    public int getSizeInBytes() {
        return getHeaderSize();
    }
    
    @Override
    public boolean hasTrailer() {
        return this.unknown1 != 0 || this.type == 113 || this.type == 112 || (this.type == 107 || this.type == 106 || this.type == 105 || this.type == 102 || this.type == 101 || this.type == 44);
    }
    
    @Override
    public boolean hasSeparator() {
        return false;
    }
}
