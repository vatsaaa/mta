// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hdgf.chunks;

public final class ChunkHeaderV4V5 extends ChunkHeader
{
    protected short unknown2;
    protected short unknown3;
    
    public short getUnknown2() {
        return this.unknown2;
    }
    
    public short getUnknown3() {
        return this.unknown3;
    }
    
    protected static int getHeaderSize() {
        return 12;
    }
    
    @Override
    public int getSizeInBytes() {
        return getHeaderSize();
    }
    
    @Override
    public boolean hasTrailer() {
        return false;
    }
    
    @Override
    public boolean hasSeparator() {
        return false;
    }
}
