// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hdgf.dev;

import org.apache.poi.hdgf.chunks.Chunk;
import org.apache.poi.hdgf.pointers.Pointer;
import org.apache.poi.hdgf.streams.ChunkStream;
import org.apache.poi.hdgf.streams.PointerContainingStream;
import org.apache.poi.hdgf.streams.Stream;
import org.apache.poi.hdgf.HDGFDiagram;
import java.io.InputStream;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import java.io.FileInputStream;

public final class VSDDumper
{
    public static void main(final String[] args) throws Exception {
        if (args.length == 0) {
            System.err.println("Use:");
            System.err.println("  VSDDumper <filename>");
            System.exit(1);
        }
        final HDGFDiagram hdgf = new HDGFDiagram(new POIFSFileSystem(new FileInputStream(args[0])));
        System.out.println("Opened " + args[0]);
        System.out.println("The document claims a size of " + hdgf.getDocumentSize() + "   (" + Long.toHexString(hdgf.getDocumentSize()) + ")");
        System.out.println();
        dumpStream(hdgf.getTrailerStream(), 0);
    }
    
    public static void dumpStream(final Stream stream, final int indent) {
        String ind = "";
        for (int i = 0; i < indent; ++i) {
            ind += "    ";
        }
        final String ind2 = ind + "    ";
        final String ind3 = ind2 + "    ";
        final Pointer ptr = stream.getPointer();
        System.out.println(ind + "Stream at\t" + ptr.getOffset() + " - " + Integer.toHexString(ptr.getOffset()));
        System.out.println(ind + "  Type is\t" + ptr.getType() + " - " + Integer.toHexString(ptr.getType()));
        System.out.println(ind + "  Format is\t" + ptr.getFormat() + " - " + Integer.toHexString(ptr.getFormat()));
        System.out.println(ind + "  Length is\t" + ptr.getLength() + " - " + Integer.toHexString(ptr.getLength()));
        if (ptr.destinationCompressed()) {
            final int decompLen = stream._getContentsLength();
            System.out.println(ind + "  DC.Length is\t" + decompLen + " - " + Integer.toHexString(decompLen));
        }
        System.out.println(ind + "  Compressed is\t" + ptr.destinationCompressed());
        System.out.println(ind + "  Stream is\t" + stream.getClass().getName());
        final byte[] db = stream._getStore()._getContents();
        String ds = "";
        if (db.length >= 8) {
            for (int j = 0; j < 8; ++j) {
                if (j > 0) {
                    ds += ", ";
                }
                ds += db[j];
            }
        }
        System.out.println(ind + "  First few bytes are\t" + ds);
        if (stream instanceof PointerContainingStream) {
            final PointerContainingStream pcs = (PointerContainingStream)stream;
            System.out.println(ind + "  Has " + pcs.getPointedToStreams().length + " children:");
            for (int k = 0; k < pcs.getPointedToStreams().length; ++k) {
                dumpStream(pcs.getPointedToStreams()[k], indent + 1);
            }
        }
        if (stream instanceof ChunkStream) {
            final ChunkStream cs = (ChunkStream)stream;
            System.out.println(ind + "  Has " + cs.getChunks().length + " chunks:");
            for (int k = 0; k < cs.getChunks().length; ++k) {
                final Chunk chunk = cs.getChunks()[k];
                System.out.println(ind2 + "" + chunk.getName());
                System.out.println(ind2 + "  Length is " + chunk._getContents().length + " (" + Integer.toHexString(chunk._getContents().length) + ")");
                System.out.println(ind2 + "  OD Size is " + chunk.getOnDiskSize() + " (" + Integer.toHexString(chunk.getOnDiskSize()) + ")");
                System.out.println(ind2 + "  T / S is " + chunk.getTrailer() + " / " + chunk.getSeparator());
                System.out.println(ind2 + "  Holds " + chunk.getCommands().length + " commands");
                for (int l = 0; l < chunk.getCommands().length; ++l) {
                    final Chunk.Command command = chunk.getCommands()[l];
                    System.out.println(ind3 + "" + command.getDefinition().getName() + " " + command.getValue());
                }
            }
        }
    }
}
