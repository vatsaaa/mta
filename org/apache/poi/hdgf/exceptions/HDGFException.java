// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hdgf.exceptions;

public final class HDGFException extends RuntimeException
{
    public HDGFException() {
    }
    
    public HDGFException(final String message) {
        super(message);
    }
    
    public HDGFException(final String message, final Throwable cause) {
        super(message, cause);
    }
    
    public HDGFException(final Throwable cause) {
        super(cause);
    }
}
