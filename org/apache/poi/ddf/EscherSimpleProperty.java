// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.ddf;

import org.apache.poi.util.HexDump;
import org.apache.poi.util.LittleEndian;

public class EscherSimpleProperty extends EscherProperty
{
    protected int propertyValue;
    
    public EscherSimpleProperty(final short id, final int propertyValue) {
        super(id);
        this.propertyValue = propertyValue;
    }
    
    public EscherSimpleProperty(final short propertyNumber, final boolean isComplex, final boolean isBlipId, final int propertyValue) {
        super(propertyNumber, isComplex, isBlipId);
        this.propertyValue = propertyValue;
    }
    
    @Override
    public int serializeSimplePart(final byte[] data, final int offset) {
        LittleEndian.putShort(data, offset, this.getId());
        LittleEndian.putInt(data, offset + 2, this.propertyValue);
        return 6;
    }
    
    @Override
    public int serializeComplexPart(final byte[] data, final int pos) {
        return 0;
    }
    
    public int getPropertyValue() {
        return this.propertyValue;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof EscherSimpleProperty)) {
            return false;
        }
        final EscherSimpleProperty escherSimpleProperty = (EscherSimpleProperty)o;
        return this.propertyValue == escherSimpleProperty.propertyValue && this.getId() == escherSimpleProperty.getId();
    }
    
    @Override
    public int hashCode() {
        return this.propertyValue;
    }
    
    @Override
    public String toString() {
        return "propNum: " + this.getPropertyNumber() + ", RAW: 0x" + HexDump.toHex(this.getId()) + ", propName: " + EscherProperties.getPropertyName(this.getPropertyNumber()) + ", complex: " + this.isComplex() + ", blipId: " + this.isBlipId() + ", value: " + this.propertyValue + " (0x" + HexDump.toHex(this.propertyValue) + ")";
    }
}
