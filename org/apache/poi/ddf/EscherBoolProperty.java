// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.ddf;

public class EscherBoolProperty extends EscherSimpleProperty
{
    public EscherBoolProperty(final short propertyNumber, final int value) {
        super(propertyNumber, value);
    }
    
    public boolean isTrue() {
        return this.propertyValue != 0;
    }
    
    public boolean isFalse() {
        return this.propertyValue == 0;
    }
}
