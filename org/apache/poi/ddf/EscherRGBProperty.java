// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.ddf;

public class EscherRGBProperty extends EscherSimpleProperty
{
    public EscherRGBProperty(final short propertyNumber, final int rgbColor) {
        super(propertyNumber, rgbColor);
    }
    
    public int getRgbColor() {
        return this.propertyValue;
    }
    
    public byte getRed() {
        return (byte)(this.propertyValue & 0xFF);
    }
    
    public byte getGreen() {
        return (byte)(this.propertyValue >> 8 & 0xFF);
    }
    
    public byte getBlue() {
        return (byte)(this.propertyValue >> 16 & 0xFF);
    }
}
