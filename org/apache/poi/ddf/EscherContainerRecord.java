// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.ddf;

import java.util.NoSuchElementException;
import java.io.PrintWriter;
import org.apache.poi.util.HexDump;
import java.util.Collection;
import java.util.Iterator;
import org.apache.poi.util.LittleEndian;
import java.util.ArrayList;
import java.util.List;

public final class EscherContainerRecord extends EscherRecord
{
    public static final short DGG_CONTAINER = -4096;
    public static final short BSTORE_CONTAINER = -4095;
    public static final short DG_CONTAINER = -4094;
    public static final short SPGR_CONTAINER = -4093;
    public static final short SP_CONTAINER = -4092;
    public static final short SOLVER_CONTAINER = -4091;
    private final List<EscherRecord> _childRecords;
    
    public EscherContainerRecord() {
        this._childRecords = new ArrayList<EscherRecord>();
    }
    
    @Override
    public int fillFields(final byte[] data, final int pOffset, final EscherRecordFactory recordFactory) {
        int bytesRemaining = this.readHeader(data, pOffset);
        int bytesWritten = 8;
        int offset = pOffset + 8;
        while (bytesRemaining > 0 && offset < data.length) {
            final EscherRecord child = recordFactory.createRecord(data, offset);
            final int childBytesWritten = child.fillFields(data, offset, recordFactory);
            bytesWritten += childBytesWritten;
            offset += childBytesWritten;
            bytesRemaining -= childBytesWritten;
            this.addChildRecord(child);
            if (offset >= data.length && bytesRemaining > 0) {
                System.out.println("WARNING: " + bytesRemaining + " bytes remaining but no space left");
            }
        }
        return bytesWritten;
    }
    
    @Override
    public int serialize(final int offset, final byte[] data, final EscherSerializationListener listener) {
        listener.beforeRecordSerialize(offset, this.getRecordId(), this);
        LittleEndian.putShort(data, offset, this.getOptions());
        LittleEndian.putShort(data, offset + 2, this.getRecordId());
        int remainingBytes = 0;
        for (final EscherRecord r : this._childRecords) {
            remainingBytes += r.getRecordSize();
        }
        LittleEndian.putInt(data, offset + 4, remainingBytes);
        int pos = offset + 8;
        for (final EscherRecord r2 : this._childRecords) {
            pos += r2.serialize(pos, data, listener);
        }
        listener.afterRecordSerialize(pos, this.getRecordId(), pos - offset, this);
        return pos - offset;
    }
    
    @Override
    public int getRecordSize() {
        int childRecordsSize = 0;
        for (final EscherRecord r : this._childRecords) {
            childRecordsSize += r.getRecordSize();
        }
        return 8 + childRecordsSize;
    }
    
    public boolean hasChildOfType(final short recordId) {
        for (final EscherRecord r : this._childRecords) {
            if (r.getRecordId() == recordId) {
                return true;
            }
        }
        return false;
    }
    
    @Override
    public EscherRecord getChild(final int index) {
        return this._childRecords.get(index);
    }
    
    @Override
    public List<EscherRecord> getChildRecords() {
        return new ArrayList<EscherRecord>(this._childRecords);
    }
    
    public Iterator<EscherRecord> getChildIterator() {
        return new ReadOnlyIterator(this._childRecords);
    }
    
    @Override
    public void setChildRecords(final List<EscherRecord> childRecords) {
        if (childRecords == this._childRecords) {
            throw new IllegalStateException("Child records private data member has escaped");
        }
        this._childRecords.clear();
        this._childRecords.addAll(childRecords);
    }
    
    public boolean removeChildRecord(final EscherRecord toBeRemoved) {
        return this._childRecords.remove(toBeRemoved);
    }
    
    public List<EscherContainerRecord> getChildContainers() {
        final List<EscherContainerRecord> containers = new ArrayList<EscherContainerRecord>();
        for (final EscherRecord r : this._childRecords) {
            if (r instanceof EscherContainerRecord) {
                containers.add((EscherContainerRecord)r);
            }
        }
        return containers;
    }
    
    @Override
    public String getRecordName() {
        switch (this.getRecordId()) {
            case -4096: {
                return "DggContainer";
            }
            case -4095: {
                return "BStoreContainer";
            }
            case -4094: {
                return "DgContainer";
            }
            case -4093: {
                return "SpgrContainer";
            }
            case -4092: {
                return "SpContainer";
            }
            case -4091: {
                return "SolverContainer";
            }
            default: {
                return "Container 0x" + HexDump.toHex(this.getRecordId());
            }
        }
    }
    
    @Override
    public void display(final PrintWriter w, final int indent) {
        super.display(w, indent);
        for (final EscherRecord escherRecord : this._childRecords) {
            escherRecord.display(w, indent + 1);
        }
    }
    
    public void addChildRecord(final EscherRecord record) {
        this._childRecords.add(record);
    }
    
    public void addChildBefore(final EscherRecord record, final int insertBeforeRecordId) {
        for (int i = 0; i < this._childRecords.size(); ++i) {
            final EscherRecord rec = this._childRecords.get(i);
            if (rec.getRecordId() == insertBeforeRecordId) {
                this._childRecords.add(i++, record);
            }
        }
    }
    
    @Override
    public String toString() {
        final String nl = System.getProperty("line.separator");
        final StringBuffer children = new StringBuffer();
        if (this._childRecords.size() > 0) {
            children.append("  children: " + nl);
            int count = 0;
            for (final EscherRecord record : this._childRecords) {
                children.append("   Child " + count + ":" + nl);
                String childResult = String.valueOf(record);
                childResult = childResult.replaceAll("\n", "\n    ");
                children.append("    ");
                children.append(childResult);
                children.append(nl);
                ++count;
            }
        }
        return this.getClass().getName() + " (" + this.getRecordName() + "):" + nl + "  isContainer: " + this.isContainerRecord() + nl + "  version: 0x" + HexDump.toHex(this.getVersion()) + nl + "  instance: 0x" + HexDump.toHex(this.getInstance()) + nl + "  recordId: 0x" + HexDump.toHex(this.getRecordId()) + nl + "  numchildren: " + this._childRecords.size() + nl + children.toString();
    }
    
    public <T extends EscherRecord> T getChildById(final short recordId) {
        for (final EscherRecord childRecord : this._childRecords) {
            if (childRecord.getRecordId() == recordId) {
                final T result = (T)childRecord;
                return result;
            }
        }
        return null;
    }
    
    public void getRecordsById(final short recordId, final List<EscherRecord> out) {
        for (final EscherRecord r : this._childRecords) {
            if (r instanceof EscherContainerRecord) {
                final EscherContainerRecord c = (EscherContainerRecord)r;
                c.getRecordsById(recordId, out);
            }
            else {
                if (r.getRecordId() != recordId) {
                    continue;
                }
                out.add(r);
            }
        }
    }
    
    private static final class ReadOnlyIterator implements Iterator<EscherRecord>
    {
        private final List<EscherRecord> _list;
        private int _index;
        
        public ReadOnlyIterator(final List<EscherRecord> list) {
            this._list = list;
            this._index = 0;
        }
        
        public boolean hasNext() {
            return this._index < this._list.size();
        }
        
        public EscherRecord next() {
            if (!this.hasNext()) {
                throw new NoSuchElementException();
            }
            return this._list.get(this._index++);
        }
        
        public void remove() {
            throw new UnsupportedOperationException();
        }
    }
}
