// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.ddf;

import org.apache.poi.util.HexDump;
import org.apache.poi.util.LittleEndian;

public class EscherChildAnchorRecord extends EscherRecord
{
    public static final short RECORD_ID = -4081;
    public static final String RECORD_DESCRIPTION = "MsofbtChildAnchor";
    private int field_1_dx1;
    private int field_2_dy1;
    private int field_3_dx2;
    private int field_4_dy2;
    
    @Override
    public int fillFields(final byte[] data, final int offset, final EscherRecordFactory recordFactory) {
        final int bytesRemaining = this.readHeader(data, offset);
        final int pos = offset + 8;
        int size = 0;
        this.field_1_dx1 = LittleEndian.getInt(data, pos + size);
        size += 4;
        this.field_2_dy1 = LittleEndian.getInt(data, pos + size);
        size += 4;
        this.field_3_dx2 = LittleEndian.getInt(data, pos + size);
        size += 4;
        this.field_4_dy2 = LittleEndian.getInt(data, pos + size);
        size += 4;
        return 8 + size;
    }
    
    @Override
    public int serialize(final int offset, final byte[] data, final EscherSerializationListener listener) {
        listener.beforeRecordSerialize(offset, this.getRecordId(), this);
        int pos = offset;
        LittleEndian.putShort(data, pos, this.getOptions());
        pos += 2;
        LittleEndian.putShort(data, pos, this.getRecordId());
        pos += 2;
        LittleEndian.putInt(data, pos, this.getRecordSize() - 8);
        pos += 4;
        LittleEndian.putInt(data, pos, this.field_1_dx1);
        pos += 4;
        LittleEndian.putInt(data, pos, this.field_2_dy1);
        pos += 4;
        LittleEndian.putInt(data, pos, this.field_3_dx2);
        pos += 4;
        LittleEndian.putInt(data, pos, this.field_4_dy2);
        pos += 4;
        listener.afterRecordSerialize(pos, this.getRecordId(), pos - offset, this);
        return pos - offset;
    }
    
    @Override
    public int getRecordSize() {
        return 24;
    }
    
    @Override
    public short getRecordId() {
        return -4081;
    }
    
    @Override
    public String getRecordName() {
        return "ChildAnchor";
    }
    
    @Override
    public String toString() {
        final String nl = System.getProperty("line.separator");
        return this.getClass().getName() + ":" + nl + "  RecordId: 0x" + HexDump.toHex((short)(-4081)) + nl + "  Version: 0x" + HexDump.toHex(this.getVersion()) + nl + "  Instance: 0x" + HexDump.toHex(this.getInstance()) + nl + "  X1: " + this.field_1_dx1 + nl + "  Y1: " + this.field_2_dy1 + nl + "  X2: " + this.field_3_dx2 + nl + "  Y2: " + this.field_4_dy2 + nl;
    }
    
    public int getDx1() {
        return this.field_1_dx1;
    }
    
    public void setDx1(final int field_1_dx1) {
        this.field_1_dx1 = field_1_dx1;
    }
    
    public int getDy1() {
        return this.field_2_dy1;
    }
    
    public void setDy1(final int field_2_dy1) {
        this.field_2_dy1 = field_2_dy1;
    }
    
    public int getDx2() {
        return this.field_3_dx2;
    }
    
    public void setDx2(final int field_3_dx2) {
        this.field_3_dx2 = field_3_dx2;
    }
    
    public int getDy2() {
        return this.field_4_dy2;
    }
    
    public void setDy2(final int field_4_dy2) {
        this.field_4_dy2 = field_4_dy2;
    }
}
