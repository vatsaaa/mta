// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.ddf;

import org.apache.poi.util.HexDump;
import java.util.Iterator;
import org.apache.poi.util.LittleEndian;
import java.util.ArrayList;
import java.util.List;

public final class UnknownEscherRecord extends EscherRecord
{
    private static final byte[] NO_BYTES;
    private byte[] thedata;
    private List<EscherRecord> _childRecords;
    
    public UnknownEscherRecord() {
        this.thedata = UnknownEscherRecord.NO_BYTES;
        this._childRecords = new ArrayList<EscherRecord>();
    }
    
    @Override
    public int fillFields(final byte[] data, int offset, final EscherRecordFactory recordFactory) {
        int bytesRemaining = this.readHeader(data, offset);
        final int avaliable = data.length - (offset + 8);
        if (bytesRemaining > avaliable) {
            bytesRemaining = avaliable;
        }
        if (this.isContainerRecord()) {
            int bytesWritten = 0;
            this.thedata = new byte[0];
            offset += 8;
            bytesWritten += 8;
            while (bytesRemaining > 0) {
                final EscherRecord child = recordFactory.createRecord(data, offset);
                final int childBytesWritten = child.fillFields(data, offset, recordFactory);
                bytesWritten += childBytesWritten;
                offset += childBytesWritten;
                bytesRemaining -= childBytesWritten;
                this.getChildRecords().add(child);
            }
            return bytesWritten;
        }
        System.arraycopy(data, offset + 8, this.thedata = new byte[bytesRemaining], 0, bytesRemaining);
        return bytesRemaining + 8;
    }
    
    @Override
    public int serialize(final int offset, final byte[] data, final EscherSerializationListener listener) {
        listener.beforeRecordSerialize(offset, this.getRecordId(), this);
        LittleEndian.putShort(data, offset, this.getOptions());
        LittleEndian.putShort(data, offset + 2, this.getRecordId());
        int remainingBytes = this.thedata.length;
        for (final EscherRecord r : this._childRecords) {
            remainingBytes += r.getRecordSize();
        }
        LittleEndian.putInt(data, offset + 4, remainingBytes);
        System.arraycopy(this.thedata, 0, data, offset + 8, this.thedata.length);
        int pos = offset + 8 + this.thedata.length;
        for (final EscherRecord r2 : this._childRecords) {
            pos += r2.serialize(pos, data, listener);
        }
        listener.afterRecordSerialize(pos, this.getRecordId(), pos - offset, this);
        return pos - offset;
    }
    
    public byte[] getData() {
        return this.thedata;
    }
    
    @Override
    public int getRecordSize() {
        return 8 + this.thedata.length;
    }
    
    @Override
    public List<EscherRecord> getChildRecords() {
        return this._childRecords;
    }
    
    @Override
    public void setChildRecords(final List<EscherRecord> childRecords) {
        this._childRecords = childRecords;
    }
    
    @Override
    public Object clone() {
        return super.clone();
    }
    
    @Override
    public String getRecordName() {
        return "Unknown 0x" + HexDump.toHex(this.getRecordId());
    }
    
    @Override
    public String toString() {
        final StringBuffer children = new StringBuffer();
        if (this.getChildRecords().size() > 0) {
            children.append("  children: \n");
            for (final EscherRecord record : this._childRecords) {
                children.append(record.toString());
                children.append('\n');
            }
        }
        final String theDumpHex = HexDump.toHex(this.thedata, 32);
        return this.getClass().getName() + ":" + '\n' + "  isContainer: " + this.isContainerRecord() + '\n' + "  version: 0x" + HexDump.toHex(this.getVersion()) + '\n' + "  instance: 0x" + HexDump.toHex(this.getInstance()) + '\n' + "  recordId: 0x" + HexDump.toHex(this.getRecordId()) + '\n' + "  numchildren: " + this.getChildRecords().size() + '\n' + theDumpHex + children.toString();
    }
    
    public void addChildRecord(final EscherRecord childRecord) {
        this.getChildRecords().add(childRecord);
    }
    
    static {
        NO_BYTES = new byte[0];
    }
}
