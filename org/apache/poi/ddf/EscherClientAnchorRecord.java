// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.ddf;

import java.io.OutputStream;
import org.apache.poi.util.HexDump;
import java.io.ByteArrayOutputStream;
import org.apache.poi.util.LittleEndian;

public class EscherClientAnchorRecord extends EscherRecord
{
    public static final short RECORD_ID = -4080;
    public static final String RECORD_DESCRIPTION = "MsofbtClientAnchor";
    private short field_1_flag;
    private short field_2_col1;
    private short field_3_dx1;
    private short field_4_row1;
    private short field_5_dy1;
    private short field_6_col2;
    private short field_7_dx2;
    private short field_8_row2;
    private short field_9_dy2;
    private byte[] remainingData;
    private boolean shortRecord;
    
    public EscherClientAnchorRecord() {
        this.shortRecord = false;
    }
    
    @Override
    public int fillFields(final byte[] data, final int offset, final EscherRecordFactory recordFactory) {
        int bytesRemaining = this.readHeader(data, offset);
        final int pos = offset + 8;
        int size = 0;
        if (bytesRemaining != 4) {
            this.field_1_flag = LittleEndian.getShort(data, pos + size);
            size += 2;
            this.field_2_col1 = LittleEndian.getShort(data, pos + size);
            size += 2;
            this.field_3_dx1 = LittleEndian.getShort(data, pos + size);
            size += 2;
            this.field_4_row1 = LittleEndian.getShort(data, pos + size);
            size += 2;
            if (bytesRemaining >= 18) {
                this.field_5_dy1 = LittleEndian.getShort(data, pos + size);
                size += 2;
                this.field_6_col2 = LittleEndian.getShort(data, pos + size);
                size += 2;
                this.field_7_dx2 = LittleEndian.getShort(data, pos + size);
                size += 2;
                this.field_8_row2 = LittleEndian.getShort(data, pos + size);
                size += 2;
                this.field_9_dy2 = LittleEndian.getShort(data, pos + size);
                size += 2;
                this.shortRecord = false;
            }
            else {
                this.shortRecord = true;
            }
        }
        bytesRemaining -= size;
        System.arraycopy(data, pos + size, this.remainingData = new byte[bytesRemaining], 0, bytesRemaining);
        return 8 + size + bytesRemaining;
    }
    
    @Override
    public int serialize(final int offset, final byte[] data, final EscherSerializationListener listener) {
        listener.beforeRecordSerialize(offset, this.getRecordId(), this);
        if (this.remainingData == null) {
            this.remainingData = new byte[0];
        }
        LittleEndian.putShort(data, offset, this.getOptions());
        LittleEndian.putShort(data, offset + 2, this.getRecordId());
        final int remainingBytes = this.remainingData.length + (this.shortRecord ? 8 : 18);
        LittleEndian.putInt(data, offset + 4, remainingBytes);
        LittleEndian.putShort(data, offset + 8, this.field_1_flag);
        LittleEndian.putShort(data, offset + 10, this.field_2_col1);
        LittleEndian.putShort(data, offset + 12, this.field_3_dx1);
        LittleEndian.putShort(data, offset + 14, this.field_4_row1);
        if (!this.shortRecord) {
            LittleEndian.putShort(data, offset + 16, this.field_5_dy1);
            LittleEndian.putShort(data, offset + 18, this.field_6_col2);
            LittleEndian.putShort(data, offset + 20, this.field_7_dx2);
            LittleEndian.putShort(data, offset + 22, this.field_8_row2);
            LittleEndian.putShort(data, offset + 24, this.field_9_dy2);
        }
        System.arraycopy(this.remainingData, 0, data, offset + (this.shortRecord ? 16 : 26), this.remainingData.length);
        final int pos = offset + 8 + (this.shortRecord ? 8 : 18) + this.remainingData.length;
        listener.afterRecordSerialize(pos, this.getRecordId(), pos - offset, this);
        return pos - offset;
    }
    
    @Override
    public int getRecordSize() {
        return 8 + (this.shortRecord ? 8 : 18) + ((this.remainingData == null) ? 0 : this.remainingData.length);
    }
    
    @Override
    public short getRecordId() {
        return -4080;
    }
    
    @Override
    public String getRecordName() {
        return "ClientAnchor";
    }
    
    @Override
    public String toString() {
        final String nl = System.getProperty("line.separator");
        final ByteArrayOutputStream b = new ByteArrayOutputStream();
        String extraData;
        try {
            HexDump.dump(this.remainingData, 0L, b, 0);
            extraData = b.toString();
        }
        catch (Exception e) {
            extraData = "error\n";
        }
        return this.getClass().getName() + ":" + nl + "  RecordId: 0x" + HexDump.toHex((short)(-4080)) + nl + "  Version: 0x" + HexDump.toHex(this.getVersion()) + nl + "  Instance: 0x" + HexDump.toHex(this.getInstance()) + nl + "  Flag: " + this.field_1_flag + nl + "  Col1: " + this.field_2_col1 + nl + "  DX1: " + this.field_3_dx1 + nl + "  Row1: " + this.field_4_row1 + nl + "  DY1: " + this.field_5_dy1 + nl + "  Col2: " + this.field_6_col2 + nl + "  DX2: " + this.field_7_dx2 + nl + "  Row2: " + this.field_8_row2 + nl + "  DY2: " + this.field_9_dy2 + nl + "  Extra Data:" + nl + extraData;
    }
    
    public short getFlag() {
        return this.field_1_flag;
    }
    
    public void setFlag(final short field_1_flag) {
        this.field_1_flag = field_1_flag;
    }
    
    public short getCol1() {
        return this.field_2_col1;
    }
    
    public void setCol1(final short field_2_col1) {
        this.field_2_col1 = field_2_col1;
    }
    
    public short getDx1() {
        return this.field_3_dx1;
    }
    
    public void setDx1(final short field_3_dx1) {
        this.field_3_dx1 = field_3_dx1;
    }
    
    public short getRow1() {
        return this.field_4_row1;
    }
    
    public void setRow1(final short field_4_row1) {
        this.field_4_row1 = field_4_row1;
    }
    
    public short getDy1() {
        return this.field_5_dy1;
    }
    
    public void setDy1(final short field_5_dy1) {
        this.shortRecord = false;
        this.field_5_dy1 = field_5_dy1;
    }
    
    public short getCol2() {
        return this.field_6_col2;
    }
    
    public void setCol2(final short field_6_col2) {
        this.shortRecord = false;
        this.field_6_col2 = field_6_col2;
    }
    
    public short getDx2() {
        return this.field_7_dx2;
    }
    
    public void setDx2(final short field_7_dx2) {
        this.shortRecord = false;
        this.field_7_dx2 = field_7_dx2;
    }
    
    public short getRow2() {
        return this.field_8_row2;
    }
    
    public void setRow2(final short field_8_row2) {
        this.shortRecord = false;
        this.field_8_row2 = field_8_row2;
    }
    
    public short getDy2() {
        return this.field_9_dy2;
    }
    
    public void setDy2(final short field_9_dy2) {
        this.shortRecord = false;
        this.field_9_dy2 = field_9_dy2;
    }
    
    public byte[] getRemainingData() {
        return this.remainingData;
    }
    
    public void setRemainingData(final byte[] remainingData) {
        this.remainingData = remainingData;
    }
}
