// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.ddf;

import org.apache.poi.util.HexDump;
import org.apache.poi.util.RecordFormatException;
import org.apache.poi.util.LittleEndian;

public class EscherTextboxRecord extends EscherRecord
{
    public static final short RECORD_ID = -4083;
    public static final String RECORD_DESCRIPTION = "msofbtClientTextbox";
    private static final byte[] NO_BYTES;
    private byte[] thedata;
    
    public EscherTextboxRecord() {
        this.thedata = EscherTextboxRecord.NO_BYTES;
    }
    
    @Override
    public int fillFields(final byte[] data, final int offset, final EscherRecordFactory recordFactory) {
        final int bytesRemaining = this.readHeader(data, offset);
        System.arraycopy(data, offset + 8, this.thedata = new byte[bytesRemaining], 0, bytesRemaining);
        return bytesRemaining + 8;
    }
    
    @Override
    public int serialize(final int offset, final byte[] data, final EscherSerializationListener listener) {
        listener.beforeRecordSerialize(offset, this.getRecordId(), this);
        LittleEndian.putShort(data, offset, this.getOptions());
        LittleEndian.putShort(data, offset + 2, this.getRecordId());
        final int remainingBytes = this.thedata.length;
        LittleEndian.putInt(data, offset + 4, remainingBytes);
        System.arraycopy(this.thedata, 0, data, offset + 8, this.thedata.length);
        final int pos = offset + 8 + this.thedata.length;
        listener.afterRecordSerialize(pos, this.getRecordId(), pos - offset, this);
        final int size = pos - offset;
        if (size != this.getRecordSize()) {
            throw new RecordFormatException(size + " bytes written but getRecordSize() reports " + this.getRecordSize());
        }
        return size;
    }
    
    public byte[] getData() {
        return this.thedata;
    }
    
    public void setData(final byte[] b, final int start, final int length) {
        System.arraycopy(b, start, this.thedata = new byte[length], 0, length);
    }
    
    public void setData(final byte[] b) {
        this.setData(b, 0, b.length);
    }
    
    @Override
    public int getRecordSize() {
        return 8 + this.thedata.length;
    }
    
    @Override
    public Object clone() {
        return super.clone();
    }
    
    @Override
    public String getRecordName() {
        return "ClientTextbox";
    }
    
    @Override
    public String toString() {
        final String nl = System.getProperty("line.separator");
        String theDumpHex = "";
        try {
            if (this.thedata.length != 0) {
                theDumpHex = "  Extra Data:" + nl;
                theDumpHex += HexDump.dump(this.thedata, 0L, 0);
            }
        }
        catch (Exception e) {
            theDumpHex = "Error!!";
        }
        return this.getClass().getName() + ":" + nl + "  isContainer: " + this.isContainerRecord() + nl + "  version: 0x" + HexDump.toHex(this.getVersion()) + nl + "  instance: 0x" + HexDump.toHex(this.getInstance()) + nl + "  recordId: 0x" + HexDump.toHex(this.getRecordId()) + nl + "  numchildren: " + this.getChildRecords().size() + nl + theDumpHex;
    }
    
    static {
        NO_BYTES = new byte[0];
    }
}
