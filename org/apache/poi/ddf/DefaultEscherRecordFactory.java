// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.ddf;

import java.util.HashMap;
import org.apache.poi.util.LittleEndian;
import java.lang.reflect.Constructor;
import java.util.Map;

public class DefaultEscherRecordFactory implements EscherRecordFactory
{
    private static Class<?>[] escherRecordClasses;
    private static Map<Short, Constructor<? extends EscherRecord>> recordsMap;
    
    public EscherRecord createRecord(final byte[] data, final int offset) {
        final short options = LittleEndian.getShort(data, offset);
        final short recordId = LittleEndian.getShort(data, offset + 2);
        if ((options & 0xF) == 0xF && recordId != -4083) {
            final EscherContainerRecord r = new EscherContainerRecord();
            r.setRecordId(recordId);
            r.setOptions(options);
            return r;
        }
        if (recordId >= -4072 && recordId <= -3817) {
            EscherBlipRecord r2;
            if (recordId == -4065 || recordId == -4067 || recordId == -4066) {
                r2 = new EscherBitmapBlip();
            }
            else if (recordId == -4070 || recordId == -4069 || recordId == -4068) {
                r2 = new EscherMetafileBlip();
            }
            else {
                r2 = new EscherBlipRecord();
            }
            r2.setRecordId(recordId);
            r2.setOptions(options);
            return r2;
        }
        final Constructor<? extends EscherRecord> recordConstructor = DefaultEscherRecordFactory.recordsMap.get(recordId);
        EscherRecord escherRecord = null;
        if (recordConstructor == null) {
            return new UnknownEscherRecord();
        }
        try {
            escherRecord = (EscherRecord)recordConstructor.newInstance(new Object[0]);
        }
        catch (Exception e) {
            return new UnknownEscherRecord();
        }
        escherRecord.setRecordId(recordId);
        escherRecord.setOptions(options);
        return escherRecord;
    }
    
    private static Map<Short, Constructor<? extends EscherRecord>> recordsToMap(final Class<?>[] recClasses) {
        final Map<Short, Constructor<? extends EscherRecord>> result = new HashMap<Short, Constructor<? extends EscherRecord>>();
        final Class<?>[] EMPTY_CLASS_ARRAY = (Class<?>[])new Class[0];
        for (int i = 0; i < recClasses.length; ++i) {
            final Class<? extends EscherRecord> recCls = (Class<? extends EscherRecord>)recClasses[i];
            short sid;
            try {
                sid = recCls.getField("RECORD_ID").getShort(null);
            }
            catch (IllegalArgumentException e) {
                throw new RuntimeException(e);
            }
            catch (IllegalAccessException e2) {
                throw new RuntimeException(e2);
            }
            catch (NoSuchFieldException e3) {
                throw new RuntimeException(e3);
            }
            Constructor<? extends EscherRecord> constructor;
            try {
                constructor = recCls.getConstructor(EMPTY_CLASS_ARRAY);
            }
            catch (NoSuchMethodException e4) {
                throw new RuntimeException(e4);
            }
            result.put(sid, constructor);
        }
        return result;
    }
    
    static {
        DefaultEscherRecordFactory.escherRecordClasses = (Class<?>[])new Class[] { EscherBSERecord.class, EscherOptRecord.class, EscherTertiaryOptRecord.class, EscherClientAnchorRecord.class, EscherDgRecord.class, EscherSpgrRecord.class, EscherSpRecord.class, EscherClientDataRecord.class, EscherDggRecord.class, EscherSplitMenuColorsRecord.class, EscherChildAnchorRecord.class, EscherTextboxRecord.class };
        DefaultEscherRecordFactory.recordsMap = recordsToMap(DefaultEscherRecordFactory.escherRecordClasses);
    }
}
