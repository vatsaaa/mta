// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.ddf;

import java.util.List;
import java.util.Collections;
import java.util.Collection;
import java.util.ArrayList;
import java.util.Arrays;
import org.apache.poi.util.HexDump;
import org.apache.poi.util.RecordFormatException;
import org.apache.poi.util.LittleEndian;
import java.util.Comparator;

public final class EscherDggRecord extends EscherRecord
{
    public static final short RECORD_ID = -4090;
    public static final String RECORD_DESCRIPTION = "MsofbtDgg";
    private int field_1_shapeIdMax;
    private int field_3_numShapesSaved;
    private int field_4_drawingsSaved;
    private FileIdCluster[] field_5_fileIdClusters;
    private int maxDgId;
    private static final Comparator<FileIdCluster> MY_COMP;
    
    @Override
    public int fillFields(final byte[] data, final int offset, final EscherRecordFactory recordFactory) {
        int bytesRemaining = this.readHeader(data, offset);
        final int pos = offset + 8;
        int size = 0;
        this.field_1_shapeIdMax = LittleEndian.getInt(data, pos + size);
        size += 4;
        LittleEndian.getInt(data, pos + size);
        size += 4;
        this.field_3_numShapesSaved = LittleEndian.getInt(data, pos + size);
        size += 4;
        this.field_4_drawingsSaved = LittleEndian.getInt(data, pos + size);
        size += 4;
        this.field_5_fileIdClusters = new FileIdCluster[(bytesRemaining - size) / 8];
        for (int i = 0; i < this.field_5_fileIdClusters.length; ++i) {
            this.field_5_fileIdClusters[i] = new FileIdCluster(LittleEndian.getInt(data, pos + size), LittleEndian.getInt(data, pos + size + 4));
            this.maxDgId = Math.max(this.maxDgId, this.field_5_fileIdClusters[i].getDrawingGroupId());
            size += 8;
        }
        bytesRemaining -= size;
        if (bytesRemaining != 0) {
            throw new RecordFormatException("Expecting no remaining data but got " + bytesRemaining + " byte(s).");
        }
        return 8 + size + bytesRemaining;
    }
    
    @Override
    public int serialize(final int offset, final byte[] data, final EscherSerializationListener listener) {
        listener.beforeRecordSerialize(offset, this.getRecordId(), this);
        int pos = offset;
        LittleEndian.putShort(data, pos, this.getOptions());
        pos += 2;
        LittleEndian.putShort(data, pos, this.getRecordId());
        pos += 2;
        final int remainingBytes = this.getRecordSize() - 8;
        LittleEndian.putInt(data, pos, remainingBytes);
        pos += 4;
        LittleEndian.putInt(data, pos, this.field_1_shapeIdMax);
        pos += 4;
        LittleEndian.putInt(data, pos, this.getNumIdClusters());
        pos += 4;
        LittleEndian.putInt(data, pos, this.field_3_numShapesSaved);
        pos += 4;
        LittleEndian.putInt(data, pos, this.field_4_drawingsSaved);
        pos += 4;
        for (int i = 0; i < this.field_5_fileIdClusters.length; ++i) {
            LittleEndian.putInt(data, pos, this.field_5_fileIdClusters[i].field_1_drawingGroupId);
            pos += 4;
            LittleEndian.putInt(data, pos, this.field_5_fileIdClusters[i].field_2_numShapeIdsUsed);
            pos += 4;
        }
        listener.afterRecordSerialize(pos, this.getRecordId(), this.getRecordSize(), this);
        return this.getRecordSize();
    }
    
    @Override
    public int getRecordSize() {
        return 24 + 8 * this.field_5_fileIdClusters.length;
    }
    
    @Override
    public short getRecordId() {
        return -4090;
    }
    
    @Override
    public String getRecordName() {
        return "Dgg";
    }
    
    @Override
    public String toString() {
        final StringBuffer field_5_string = new StringBuffer();
        if (this.field_5_fileIdClusters != null) {
            for (int i = 0; i < this.field_5_fileIdClusters.length; ++i) {
                field_5_string.append("  DrawingGroupId").append(i + 1).append(": ");
                field_5_string.append(this.field_5_fileIdClusters[i].field_1_drawingGroupId);
                field_5_string.append('\n');
                field_5_string.append("  NumShapeIdsUsed").append(i + 1).append(": ");
                field_5_string.append(this.field_5_fileIdClusters[i].field_2_numShapeIdsUsed);
                field_5_string.append('\n');
            }
        }
        return this.getClass().getName() + ":" + '\n' + "  RecordId: 0x" + HexDump.toHex((short)(-4090)) + '\n' + "  Version: 0x" + HexDump.toHex(this.getVersion()) + '\n' + "  Instance: 0x" + HexDump.toHex(this.getInstance()) + '\n' + "  ShapeIdMax: " + this.field_1_shapeIdMax + '\n' + "  NumIdClusters: " + this.getNumIdClusters() + '\n' + "  NumShapesSaved: " + this.field_3_numShapesSaved + '\n' + "  DrawingsSaved: " + this.field_4_drawingsSaved + '\n' + "" + field_5_string.toString();
    }
    
    public int getShapeIdMax() {
        return this.field_1_shapeIdMax;
    }
    
    public void setShapeIdMax(final int shapeIdMax) {
        this.field_1_shapeIdMax = shapeIdMax;
    }
    
    public int getNumIdClusters() {
        return (this.field_5_fileIdClusters == null) ? 0 : (this.field_5_fileIdClusters.length + 1);
    }
    
    public int getNumShapesSaved() {
        return this.field_3_numShapesSaved;
    }
    
    public void setNumShapesSaved(final int numShapesSaved) {
        this.field_3_numShapesSaved = numShapesSaved;
    }
    
    public int getDrawingsSaved() {
        return this.field_4_drawingsSaved;
    }
    
    public void setDrawingsSaved(final int drawingsSaved) {
        this.field_4_drawingsSaved = drawingsSaved;
    }
    
    public int getMaxDrawingGroupId() {
        return this.maxDgId;
    }
    
    public void setMaxDrawingGroupId(final int id) {
        this.maxDgId = id;
    }
    
    public FileIdCluster[] getFileIdClusters() {
        return this.field_5_fileIdClusters;
    }
    
    public void setFileIdClusters(final FileIdCluster[] fileIdClusters) {
        this.field_5_fileIdClusters = fileIdClusters;
    }
    
    public void addCluster(final int dgId, final int numShapedUsed) {
        this.addCluster(dgId, numShapedUsed, true);
    }
    
    public void addCluster(final int dgId, final int numShapedUsed, final boolean sort) {
        final List<FileIdCluster> clusters = new ArrayList<FileIdCluster>(Arrays.asList(this.field_5_fileIdClusters));
        clusters.add(new FileIdCluster(dgId, numShapedUsed));
        if (sort) {
            Collections.sort(clusters, EscherDggRecord.MY_COMP);
        }
        this.maxDgId = Math.min(this.maxDgId, dgId);
        this.field_5_fileIdClusters = clusters.toArray(new FileIdCluster[clusters.size()]);
    }
    
    static {
        MY_COMP = new Comparator<FileIdCluster>() {
            public int compare(final FileIdCluster f1, final FileIdCluster f2) {
                if (f1.getDrawingGroupId() == f2.getDrawingGroupId()) {
                    return 0;
                }
                if (f1.getDrawingGroupId() < f2.getDrawingGroupId()) {
                    return -1;
                }
                return 1;
            }
        };
    }
    
    public static class FileIdCluster
    {
        private int field_1_drawingGroupId;
        private int field_2_numShapeIdsUsed;
        
        public FileIdCluster(final int drawingGroupId, final int numShapeIdsUsed) {
            this.field_1_drawingGroupId = drawingGroupId;
            this.field_2_numShapeIdsUsed = numShapeIdsUsed;
        }
        
        public int getDrawingGroupId() {
            return this.field_1_drawingGroupId;
        }
        
        public int getNumShapeIdsUsed() {
            return this.field_2_numShapeIdsUsed;
        }
        
        public void incrementShapeId() {
            ++this.field_2_numShapeIdsUsed;
        }
    }
}
