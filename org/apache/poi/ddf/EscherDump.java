// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.ddf;

import org.apache.poi.util.HexRead;
import java.io.IOException;
import java.util.zip.InflaterInputStream;
import java.io.ByteArrayInputStream;
import org.apache.poi.util.HexDump;
import org.apache.poi.util.LittleEndian;
import java.io.InputStream;
import java.io.PrintStream;

public final class EscherDump
{
    public void dump(final byte[] data, final int offset, final int size, final PrintStream out) {
        final EscherRecordFactory recordFactory = new DefaultEscherRecordFactory();
        int bytesRead;
        for (int pos = offset; pos < offset + size; pos += bytesRead) {
            final EscherRecord r = recordFactory.createRecord(data, pos);
            bytesRead = r.fillFields(data, pos, recordFactory);
            out.println(r.toString());
        }
    }
    
    public void dumpOld(final long maxLength, final InputStream in, final PrintStream out) throws IOException, LittleEndian.BufferUnderrunException {
        long remainingBytes = maxLength;
        StringBuffer stringBuf = new StringBuffer();
        final boolean atEOF = false;
        while (!atEOF && remainingBytes > 0L) {
            stringBuf = new StringBuffer();
            final short options = LittleEndian.readShort(in);
            final short recordId = LittleEndian.readShort(in);
            int recordBytesRemaining = LittleEndian.readInt(in);
            remainingBytes -= 8L;
            String recordName = null;
            switch (recordId) {
                case -4096: {
                    recordName = "MsofbtDggContainer";
                    break;
                }
                case -4090: {
                    recordName = "MsofbtDgg";
                    break;
                }
                case -4074: {
                    recordName = "MsofbtCLSID";
                    break;
                }
                case -4085: {
                    recordName = "MsofbtOPT";
                    break;
                }
                case -3814: {
                    recordName = "MsofbtColorMRU";
                    break;
                }
                case -3810: {
                    recordName = "MsofbtSplitMenuColors";
                    break;
                }
                case -4095: {
                    recordName = "MsofbtBstoreContainer";
                    break;
                }
                case -4089: {
                    recordName = "MsofbtBSE";
                    break;
                }
                case -4094: {
                    recordName = "MsofbtDgContainer";
                    break;
                }
                case -4088: {
                    recordName = "MsofbtDg";
                    break;
                }
                case -3816: {
                    recordName = "MsofbtRegroupItem";
                    break;
                }
                case -3808: {
                    recordName = "MsofbtColorScheme";
                    break;
                }
                case -4093: {
                    recordName = "MsofbtSpgrContainer";
                    break;
                }
                case -4092: {
                    recordName = "MsofbtSpContainer";
                    break;
                }
                case -4087: {
                    recordName = "MsofbtSpgr";
                    break;
                }
                case -4086: {
                    recordName = "MsofbtSp";
                    break;
                }
                case -4084: {
                    recordName = "MsofbtTextbox";
                    break;
                }
                case -4083: {
                    recordName = "MsofbtClientTextbox";
                    break;
                }
                case -4082: {
                    recordName = "MsofbtAnchor";
                    break;
                }
                case -4081: {
                    recordName = "MsofbtChildAnchor";
                    break;
                }
                case -4080: {
                    recordName = "MsofbtClientAnchor";
                    break;
                }
                case -4079: {
                    recordName = "MsofbtClientData";
                    break;
                }
                case -3809: {
                    recordName = "MsofbtOleObject";
                    break;
                }
                case -3811: {
                    recordName = "MsofbtDeletedPspl";
                    break;
                }
                case -4091: {
                    recordName = "MsofbtSolverContainer";
                    break;
                }
                case -4078: {
                    recordName = "MsofbtConnectorRule";
                    break;
                }
                case -4077: {
                    recordName = "MsofbtAlignRule";
                    break;
                }
                case -4076: {
                    recordName = "MsofbtArcRule";
                    break;
                }
                case -4075: {
                    recordName = "MsofbtClientRule";
                    break;
                }
                case -4073: {
                    recordName = "MsofbtCalloutRule";
                    break;
                }
                case -3815: {
                    recordName = "MsofbtSelection";
                    break;
                }
                case -3806: {
                    recordName = "MsofbtUDefProp";
                    break;
                }
                default: {
                    if (recordId >= -4072 && recordId <= -3817) {
                        recordName = "MsofbtBLIP";
                        break;
                    }
                    if ((options & 0xF) == 0xF) {
                        recordName = "UNKNOWN container";
                        break;
                    }
                    recordName = "UNKNOWN ID";
                    break;
                }
            }
            stringBuf.append("  ");
            stringBuf.append(HexDump.toHex(recordId));
            stringBuf.append("  ").append(recordName).append(" [");
            stringBuf.append(HexDump.toHex(options));
            stringBuf.append(',');
            stringBuf.append(HexDump.toHex(recordBytesRemaining));
            stringBuf.append("]  instance: ");
            stringBuf.append(HexDump.toHex((short)(options >> 4)));
            out.println(stringBuf.toString());
            if (recordId == -4089 && 36L <= remainingBytes && 36 <= recordBytesRemaining) {
                stringBuf = new StringBuffer("    btWin32: ");
                byte n8 = (byte)in.read();
                stringBuf.append(HexDump.toHex(n8));
                stringBuf.append(getBlipType(n8));
                stringBuf.append("  btMacOS: ");
                n8 = (byte)in.read();
                stringBuf.append(HexDump.toHex(n8));
                stringBuf.append(getBlipType(n8));
                out.println(stringBuf.toString());
                out.println("    rgbUid:");
                HexDump.dump(in, out, 0, 16);
                out.print("    tag: ");
                this.outHex(2, in, out);
                out.println();
                out.print("    size: ");
                this.outHex(4, in, out);
                out.println();
                out.print("    cRef: ");
                this.outHex(4, in, out);
                out.println();
                out.print("    offs: ");
                this.outHex(4, in, out);
                out.println();
                out.print("    usage: ");
                this.outHex(1, in, out);
                out.println();
                out.print("    cbName: ");
                this.outHex(1, in, out);
                out.println();
                out.print("    unused2: ");
                this.outHex(1, in, out);
                out.println();
                out.print("    unused3: ");
                this.outHex(1, in, out);
                out.println();
                remainingBytes -= 36L;
                recordBytesRemaining = 0;
            }
            else if (recordId == -4080 && 18L <= remainingBytes && 18 <= recordBytesRemaining) {
                out.print("    Flag: ");
                this.outHex(2, in, out);
                out.println();
                out.print("    Col1: ");
                this.outHex(2, in, out);
                out.print("    dX1: ");
                this.outHex(2, in, out);
                out.print("    Row1: ");
                this.outHex(2, in, out);
                out.print("    dY1: ");
                this.outHex(2, in, out);
                out.println();
                out.print("    Col2: ");
                this.outHex(2, in, out);
                out.print("    dX2: ");
                this.outHex(2, in, out);
                out.print("    Row2: ");
                this.outHex(2, in, out);
                out.print("    dY2: ");
                this.outHex(2, in, out);
                out.println();
                remainingBytes -= 18L;
                recordBytesRemaining -= 18;
            }
            else if (recordId == -4085 || recordId == -3806) {
                int nComplex = 0;
                out.println("    PROPID        VALUE");
                while (recordBytesRemaining >= 6 + nComplex && remainingBytes >= 6 + nComplex) {
                    final short n9 = LittleEndian.readShort(in);
                    final int n10 = LittleEndian.readInt(in);
                    recordBytesRemaining -= 6;
                    remainingBytes -= 6L;
                    out.print("    ");
                    out.print(HexDump.toHex(n9));
                    out.print(" (");
                    final int propertyId = n9 & 0x3FFF;
                    out.print(" " + propertyId);
                    if ((n9 & 0xFFFF8000) == 0x0) {
                        if ((n9 & 0x4000) != 0x0) {
                            out.print(", fBlipID");
                        }
                        out.print(")  ");
                        out.print(HexDump.toHex(n10));
                        if ((n9 & 0x4000) == 0x0) {
                            out.print(" (");
                            out.print(this.dec1616(n10));
                            out.print(')');
                            out.print(" {" + this.propName((short)propertyId) + "}");
                        }
                        out.println();
                    }
                    else {
                        out.print(", fComplex)  ");
                        out.print(HexDump.toHex(n10));
                        out.print(" - Complex prop len");
                        out.println(" {" + this.propName((short)propertyId) + "}");
                        nComplex += n10;
                    }
                }
                while (((long)nComplex & remainingBytes) > 0L) {
                    final short nDumpSize = (nComplex > (int)remainingBytes) ? ((short)remainingBytes) : ((short)nComplex);
                    HexDump.dump(in, out, 0, nDumpSize);
                    nComplex -= nDumpSize;
                    recordBytesRemaining -= nDumpSize;
                    remainingBytes -= nDumpSize;
                }
            }
            else if (recordId == -4078) {
                out.print("    Connector rule: ");
                out.print(LittleEndian.readInt(in));
                out.print("    ShapeID A: ");
                out.print(LittleEndian.readInt(in));
                out.print("   ShapeID B: ");
                out.print(LittleEndian.readInt(in));
                out.print("    ShapeID connector: ");
                out.print(LittleEndian.readInt(in));
                out.print("   Connect pt A: ");
                out.print(LittleEndian.readInt(in));
                out.print("   Connect pt B: ");
                out.println(LittleEndian.readInt(in));
                recordBytesRemaining -= 24;
                remainingBytes -= 24L;
            }
            else if (recordId >= -4072 && recordId < -3817) {
                out.println("    Secondary UID: ");
                HexDump.dump(in, out, 0, 16);
                out.println("    Cache of size: " + HexDump.toHex(LittleEndian.readInt(in)));
                out.println("    Boundary top: " + HexDump.toHex(LittleEndian.readInt(in)));
                out.println("    Boundary left: " + HexDump.toHex(LittleEndian.readInt(in)));
                out.println("    Boundary width: " + HexDump.toHex(LittleEndian.readInt(in)));
                out.println("    Boundary height: " + HexDump.toHex(LittleEndian.readInt(in)));
                out.println("    X: " + HexDump.toHex(LittleEndian.readInt(in)));
                out.println("    Y: " + HexDump.toHex(LittleEndian.readInt(in)));
                out.println("    Cache of saved size: " + HexDump.toHex(LittleEndian.readInt(in)));
                out.println("    Compression Flag: " + HexDump.toHex((byte)in.read()));
                out.println("    Filter: " + HexDump.toHex((byte)in.read()));
                out.println("    Data (after decompression): ");
                recordBytesRemaining -= 50;
                remainingBytes -= 50L;
                final short nDumpSize = (recordBytesRemaining > (int)remainingBytes) ? ((short)remainingBytes) : ((short)recordBytesRemaining);
                final byte[] buf = new byte[nDumpSize];
                for (int read = in.read(buf); read != -1 && read < nDumpSize; read += in.read(buf, read, buf.length)) {}
                final ByteArrayInputStream bin = new ByteArrayInputStream(buf);
                final InputStream in2 = new InflaterInputStream(bin);
                final int bytesToDump = -1;
                HexDump.dump(in2, out, 0, bytesToDump);
                recordBytesRemaining -= nDumpSize;
                remainingBytes -= nDumpSize;
            }
            final boolean isContainer = (options & 0xF) == 0xF;
            if (isContainer && remainingBytes >= 0L) {
                if (recordBytesRemaining <= (int)remainingBytes) {
                    out.println("            completed within");
                }
                else {
                    out.println("            continued elsewhere");
                }
            }
            else if (remainingBytes >= 0L) {
                final short nDumpSize = (recordBytesRemaining > (int)remainingBytes) ? ((short)remainingBytes) : ((short)recordBytesRemaining);
                if (nDumpSize == 0) {
                    continue;
                }
                HexDump.dump(in, out, 0, nDumpSize);
                remainingBytes -= nDumpSize;
            }
            else {
                out.println(" >> OVERRUN <<");
            }
        }
    }
    
    private String propName(final short propertyId) {
        final class PropName
        {
            final int _id = 4;
            final String _name = "transform.rotation";
            
            public PropName(final String name) {
            }
        }
        final PropName[] props = { new PropName("transform.rotation"), new PropName("protection.lockrotation"), new PropName("protection.lockaspectratio"), new PropName("protection.lockposition"), new PropName("protection.lockagainstselect"), new PropName("protection.lockcropping"), new PropName("protection.lockvertices"), new PropName("protection.locktext"), new PropName("protection.lockadjusthandles"), new PropName("protection.lockagainstgrouping"), new PropName("text.textid"), new PropName("text.textleft"), new PropName("text.texttop"), new PropName("text.textright"), new PropName("text.textbottom"), new PropName("text.wraptext"), new PropName("text.scaletext"), new PropName("text.anchortext"), new PropName("text.textflow"), new PropName("text.fontrotation"), new PropName("text.idofnextshape"), new PropName("text.bidir"), new PropName("text.singleclickselects"), new PropName("text.usehostmargins"), new PropName("text.rotatetextwithshape"), new PropName("text.sizeshapetofittext"), new PropName("text.sizetexttofitshape"), new PropName("geotext.unicode"), new PropName("geotext.rtftext"), new PropName("geotext.alignmentoncurve"), new PropName("geotext.defaultpointsize"), new PropName("geotext.textspacing"), new PropName("geotext.fontfamilyname"), new PropName("geotext.reverseroworder"), new PropName("geotext.hastexteffect"), new PropName("geotext.rotatecharacters"), new PropName("geotext.kerncharacters"), new PropName("geotext.tightortrack"), new PropName("geotext.stretchtofitshape"), new PropName("geotext.charboundingbox"), new PropName("geotext.scaletextonpath"), new PropName("geotext.stretchcharheight"), new PropName("geotext.nomeasurealongpath"), new PropName("geotext.boldfont"), new PropName("geotext.italicfont"), new PropName("geotext.underlinefont"), new PropName("geotext.shadowfont"), new PropName("geotext.smallcapsfont"), new PropName("geotext.strikethroughfont"), new PropName("blip.cropfromtop"), new PropName("blip.cropfrombottom"), new PropName("blip.cropfromleft"), new PropName("blip.cropfromright"), new PropName("blip.bliptodisplay"), new PropName("blip.blipfilename"), new PropName("blip.blipflags"), new PropName("blip.transparentcolor"), new PropName("blip.contrastsetting"), new PropName("blip.brightnesssetting"), new PropName("blip.gamma"), new PropName("blip.pictureid"), new PropName("blip.doublemod"), new PropName("blip.picturefillmod"), new PropName("blip.pictureline"), new PropName("blip.printblip"), new PropName("blip.printblipfilename"), new PropName("blip.printflags"), new PropName("blip.nohittestpicture"), new PropName("blip.picturegray"), new PropName("blip.picturebilevel"), new PropName("blip.pictureactive"), new PropName("geometry.left"), new PropName("geometry.top"), new PropName("geometry.right"), new PropName("geometry.bottom"), new PropName("geometry.shapepath"), new PropName("geometry.vertices"), new PropName("geometry.segmentinfo"), new PropName("geometry.adjustvalue"), new PropName("geometry.adjust2value"), new PropName("geometry.adjust3value"), new PropName("geometry.adjust4value"), new PropName("geometry.adjust5value"), new PropName("geometry.adjust6value"), new PropName("geometry.adjust7value"), new PropName("geometry.adjust8value"), new PropName("geometry.adjust9value"), new PropName("geometry.adjust10value"), new PropName("geometry.shadowOK"), new PropName("geometry.3dok"), new PropName("geometry.lineok"), new PropName("geometry.geotextok"), new PropName("geometry.fillshadeshapeok"), new PropName("geometry.fillok"), new PropName("fill.filltype"), new PropName("fill.fillcolor"), new PropName("fill.fillopacity"), new PropName("fill.fillbackcolor"), new PropName("fill.backopacity"), new PropName("fill.crmod"), new PropName("fill.patterntexture"), new PropName("fill.blipfilename"), new PropName("fill.blipflags"), new PropName("fill.width"), new PropName("fill.height"), new PropName("fill.angle"), new PropName("fill.focus"), new PropName("fill.toleft"), new PropName("fill.totop"), new PropName("fill.toright"), new PropName("fill.tobottom"), new PropName("fill.rectleft"), new PropName("fill.recttop"), new PropName("fill.rectright"), new PropName("fill.rectbottom"), new PropName("fill.dztype"), new PropName("fill.shadepreset"), new PropName("fill.shadecolors"), new PropName("fill.originx"), new PropName("fill.originy"), new PropName("fill.shapeoriginx"), new PropName("fill.shapeoriginy"), new PropName("fill.shadetype"), new PropName("fill.filled"), new PropName("fill.hittestfill"), new PropName("fill.shape"), new PropName("fill.userect"), new PropName("fill.nofillhittest"), new PropName("linestyle.color"), new PropName("linestyle.opacity"), new PropName("linestyle.backcolor"), new PropName("linestyle.crmod"), new PropName("linestyle.linetype"), new PropName("linestyle.fillblip"), new PropName("linestyle.fillblipname"), new PropName("linestyle.fillblipflags"), new PropName("linestyle.fillwidth"), new PropName("linestyle.fillheight"), new PropName("linestyle.filldztype"), new PropName("linestyle.linewidth"), new PropName("linestyle.linemiterlimit"), new PropName("linestyle.linestyle"), new PropName("linestyle.linedashing"), new PropName("linestyle.linedashstyle"), new PropName("linestyle.linestartarrowhead"), new PropName("linestyle.lineendarrowhead"), new PropName("linestyle.linestartarrowwidth"), new PropName("linestyle.lineestartarrowlength"), new PropName("linestyle.lineendarrowwidth"), new PropName("linestyle.lineendarrowlength"), new PropName("linestyle.linejoinstyle"), new PropName("linestyle.lineendcapstyle"), new PropName("linestyle.arrowheadsok"), new PropName("linestyle.anyline"), new PropName("linestyle.hitlinetest"), new PropName("linestyle.linefillshape"), new PropName("linestyle.nolinedrawdash"), new PropName("shadowstyle.type"), new PropName("shadowstyle.color"), new PropName("shadowstyle.highlight"), new PropName("shadowstyle.crmod"), new PropName("shadowstyle.opacity"), new PropName("shadowstyle.offsetx"), new PropName("shadowstyle.offsety"), new PropName("shadowstyle.secondoffsetx"), new PropName("shadowstyle.secondoffsety"), new PropName("shadowstyle.scalextox"), new PropName("shadowstyle.scaleytox"), new PropName("shadowstyle.scalextoy"), new PropName("shadowstyle.scaleytoy"), new PropName("shadowstyle.perspectivex"), new PropName("shadowstyle.perspectivey"), new PropName("shadowstyle.weight"), new PropName("shadowstyle.originx"), new PropName("shadowstyle.originy"), new PropName("shadowstyle.shadow"), new PropName("shadowstyle.shadowobsured"), new PropName("perspective.type"), new PropName("perspective.offsetx"), new PropName("perspective.offsety"), new PropName("perspective.scalextox"), new PropName("perspective.scaleytox"), new PropName("perspective.scalextoy"), new PropName("perspective.scaleytox"), new PropName("perspective.perspectivex"), new PropName("perspective.perspectivey"), new PropName("perspective.weight"), new PropName("perspective.originx"), new PropName("perspective.originy"), new PropName("perspective.perspectiveon"), new PropName("3d.specularamount"), new PropName("3d.diffuseamount"), new PropName("3d.shininess"), new PropName("3d.edgethickness"), new PropName("3d.extrudeforward"), new PropName("3d.extrudebackward"), new PropName("3d.extrudeplane"), new PropName("3d.extrusioncolor"), new PropName("3d.crmod"), new PropName("3d.3deffect"), new PropName("3d.metallic"), new PropName("3d.useextrusioncolor"), new PropName("3d.lightface"), new PropName("3dstyle.yrotationangle"), new PropName("3dstyle.xrotationangle"), new PropName("3dstyle.rotationaxisx"), new PropName("3dstyle.rotationaxisy"), new PropName("3dstyle.rotationaxisz"), new PropName("3dstyle.rotationangle"), new PropName("3dstyle.rotationcenterx"), new PropName("3dstyle.rotationcentery"), new PropName("3dstyle.rotationcenterz"), new PropName("3dstyle.rendermode"), new PropName("3dstyle.tolerance"), new PropName("3dstyle.xviewpoint"), new PropName("3dstyle.yviewpoint"), new PropName("3dstyle.zviewpoint"), new PropName("3dstyle.originx"), new PropName("3dstyle.originy"), new PropName("3dstyle.skewangle"), new PropName("3dstyle.skewamount"), new PropName("3dstyle.ambientintensity"), new PropName("3dstyle.keyx"), new PropName("3dstyle.keyy"), new PropName("3dstyle.keyz"), new PropName("3dstyle.keyintensity"), new PropName("3dstyle.fillx"), new PropName("3dstyle.filly"), new PropName("3dstyle.fillz"), new PropName("3dstyle.fillintensity"), new PropName("3dstyle.constrainrotation"), new PropName("3dstyle.rotationcenterauto"), new PropName("3dstyle.parallel"), new PropName("3dstyle.keyharsh"), new PropName("3dstyle.fillharsh"), new PropName("shape.master"), new PropName("shape.connectorstyle"), new PropName("shape.blackandwhitesettings"), new PropName("shape.wmodepurebw"), new PropName("shape.wmodebw"), new PropName("shape.oleicon"), new PropName("shape.preferrelativeresize"), new PropName("shape.lockshapetype"), new PropName("shape.deleteattachedobject"), new PropName("shape.backgroundshape"), new PropName("callout.callouttype"), new PropName("callout.xycalloutgap"), new PropName("callout.calloutangle"), new PropName("callout.calloutdroptype"), new PropName("callout.calloutdropspecified"), new PropName("callout.calloutlengthspecified"), new PropName("callout.iscallout"), new PropName("callout.calloutaccentbar"), new PropName("callout.callouttextborder"), new PropName("callout.calloutminusx"), new PropName("callout.calloutminusy"), new PropName("callout.dropauto"), new PropName("callout.lengthspecified"), new PropName("groupshape.shapename"), new PropName("groupshape.description"), new PropName("groupshape.hyperlink"), new PropName("groupshape.wrappolygonvertices"), new PropName("groupshape.wrapdistleft"), new PropName("groupshape.wrapdisttop"), new PropName("groupshape.wrapdistright"), new PropName("groupshape.wrapdistbottom"), new PropName("groupshape.regroupid"), new PropName("groupshape.editedwrap"), new PropName("groupshape.behinddocument"), new PropName("groupshape.ondblclicknotify"), new PropName("groupshape.isbutton"), new PropName("groupshape.1dadjustment"), new PropName("groupshape.hidden"), new PropName("groupshape.print") };
        for (int i = 0; i < props.length; ++i) {
            if (props[i]._id == propertyId) {
                return props[i]._name;
            }
        }
        return "unknown property";
    }
    
    private static String getBlipType(final byte b) {
        return EscherBSERecord.getBlipType(b);
    }
    
    private String dec1616(final int n32) {
        String result = "";
        result += (short)(n32 >> 16);
        result += '.';
        result += (short)(n32 & -1);
        return result;
    }
    
    private void outHex(final int bytes, final InputStream in, final PrintStream out) throws IOException, LittleEndian.BufferUnderrunException {
        switch (bytes) {
            case 1: {
                out.print(HexDump.toHex((byte)in.read()));
                break;
            }
            case 2: {
                out.print(HexDump.toHex(LittleEndian.readShort(in)));
                break;
            }
            case 4: {
                out.print(HexDump.toHex(LittleEndian.readInt(in)));
                break;
            }
            default: {
                throw new IOException("Unable to output variable of that width");
            }
        }
    }
    
    public static void main(final String[] args) {
        final String dump = "0F 00 00 F0 89 07 00 00 00 00 06 F0 18 00 00 00 05 04 00 00 02 00 00 00 05 00 00 00 01 00 00 00 01 00 00 00 05 00 00 00 4F 00 01 F0 2F 07 00 00 42 00 07 F0 B7 01 00 00 03 04 3F 14 AE 6B 0F 65 B0 48 BF 5E 94 63 80 E8 91 73 FF 00 93 01 00 00 01 00 00 00 00 00 00 00 00 00 FF FF 20 54 1C F0 8B 01 00 00 3F 14 AE 6B 0F 65 B0 48 BF 5E 94 63 80 E8 91 73 92 0E 00 00 00 00 00 00 00 00 00 00 D1 07 00 00 DD 05 00 00 4A AD 6F 00 8A C5 53 00 59 01 00 00 00 FE 78 9C E3 9B C4 00 04 AC 77 D9 2F 32 08 32 FD E7 61 F8 FF 0F C8 FD 05 C5 30 19 10 90 63 90 FA 0F 06 0C 8C 0C 5C 70 19 43 30 EB 0E FB 05 86 85 0C DB 18 58 80 72 8C 70 16 0B 83 05 56 51 29 88 C9 60 D9 69 0C 6C 20 26 23 03 C8 74 B0 A8 0E 03 07 FB 45 56 C7 A2 CC C4 1C 06 66 A0 0D 2C 40 39 5E 86 4C 06 3D A0 4E 10 D0 60 D9 C8 58 CC E8 CF B0 80 61 3A 8A 7E 0D C6 23 AC 4F E0 E2 98 B6 12 2B 06 73 9D 12 E3 52 56 59 F6 08 8A CC 52 66 A3 50 FF 96 2B 94 E9 DF 4C A1 FE 2D 3A 03 AB 9F 81 C2 F0 A3 54 BF 0F 85 EE A7 54 FF 40 FB 7F A0 E3 9F D2 F4 4F 71 FE 19 58 FF 2B 31 7F 67 36 3B 25 4F 99 1B 4E 53 A6 5F 89 25 95 E9 C4 00 C7 83 12 F3 1F 26 35 4A D3 D2 47 0E 0A C3 41 8E C9 8A 52 37 DC 15 A1 D0 0D BC 4C 06 0C 2B 28 2C 13 28 D4 EF 43 61 5A A0 58 3F 85 71 E0 4B 69 9E 64 65 FE 39 C0 E5 22 30 1D 30 27 0E 74 3A 18 60 FD 4A CC B1 2C 13 7D 07 36 2D 2A 31 85 B2 6A 0D 74 1D 1D 22 4D 99 FE 60 0A F5 9B EC 1C 58 FD 67 06 56 3F 38 0D 84 3C A5 30 0E 28 D3 AF C4 A4 CA FA 44 7A 0D 65 6E 60 7F 4D A1 1B 24 58 F7 49 AF A5 CC 0D CC DF 19 FE 03 00 F0 B1 25 4D 42 00 07 F0 E1 01 00 00 03 04 39 50 BE 98 B0 6F 57 24 31 70 5D 23 2F 9F 10 66 FF 00 BD 01 00 00 01 00 00 00 00 00 00 00 00 00 FF FF 20 54 1C F0 B5 01 00 00 39 50 BE 98 B0 6F 57 24 31 70 5D 23 2F 9F 10 66 DA 03 00 00 00 00 00 00 00 00 00 00 D1 07 00 00 DD 05 00 00 4A AD 6F 00 8A C5 53 00 83 01 00 00 00 FE 78 9C A5 52 BF 4B 42 51 14 3E F7 DC 77 7A 16 45 48 8B 3C 48 A8 16 15 0D 6C 88 D0 04 C3 40 A3 32 1C 84 96 08 21 04 A1 C5 5C A2 35 82 C0 35 6A AB 1C 6A 6B A8 24 5A 83 68 08 84 84 96 A2 86 A0 7F C2 86 5E E7 5E F5 41 E4 10 BC 03 1F E7 FB F1 CE B9 F7 F1 9E 7C 05 2E 7A 37 9B E0 45 7B 10 EC 6F 96 5F 1D 74 13 55 7E B0 6C 5D 20 60 C0 49 A2 9A BD 99 4F 50 83 1B 30 38 13 0E 33 60 A6 A7 6B B5 37 EB F4 10 FA 14 15 A0 B6 6B 37 0C 1E B3 49 73 5B A5 C2 26 48 3E C1 E0 6C 08 4A 30 C9 93 AA 02 B8 20 13 62 05 4E E1 E8 D7 7C C0 B8 14 95 5E BE B8 A7 CF 1E BE 55 2C 56 B9 78 DF 08 7E 88 4C 27 FF 7B DB FF 7A DD B7 1A 17 67 34 6A AE BA DA 35 D1 E7 72 BE FE EC 6E FE DA E5 7C 3D EC 7A DE 03 FD 50 06 0B 23 F2 0E F3 B2 A5 11 91 0D 4C B5 B5 F3 BF 94 C1 8F 24 F7 D9 6F 60 94 3B C9 9A F3 1C 6B E7 BB F0 2E 49 B2 25 2B C6 B1 EE 69 EE 15 63 4F 71 7D CE 85 CC C8 35 B9 C3 28 28 CE D0 5C 67 79 F2 4A A2 14 23 A4 38 43 73 9D 2D 69 2F C1 08 31 9F C5 5C 9B EB 7B C5 69 19 B3 B4 81 F3 DC E3 B4 8E 8B CC B3 94 53 5A E7 41 2A 63 9A AA 38 C5 3D 48 BB EC 57 59 6F 2B AD 73 1F 1D 60 92 AE 70 8C BB 8F CE 31 C1 3C 49 27 4A EB DC A4 5B 8C D1 0B 0E 73 37 E9 11 A7 99 C7 E8 41 69 B0 7F 00 96 F2 A7 E8 42 00 07 F0 B4 01 00 00 03 04 1A BA F9 D6 A9 B9 3A 03 08 61 E9 90 FF 7B 9E E6 FF 00 90 01 00 00 01 00 00 00 00 00 00 00 00 00 FF FF 20 54 1C F0 88 01 00 00 1A BA F9 D6 A9 B9 3A 03 08 61 E9 90 FF 7B 9E E6 12 0E 00 00 00 00 00 00 00 00 00 00 D1 07 00 00 DD 05 00 00 4A AD 6F 00 8A C5 53 00 56 01 00 00 00 FE 78 9C E3 13 62 00 02 D6 BB EC 17 19 04 99 FE F3 30 FC FF 07 E4 FE 82 62 98 0C 08 C8 31 48 FD 07 03 06 46 06 2E B8 8C 21 98 75 87 FD 02 C3 42 86 6D 0C 2C 40 39 46 38 8B 85 C1 02 AB A8 14 C4 64 B0 EC 34 06 36 10 93 91 01 64 3A 58 54 87 81 83 FD 22 AB 63 51 66 62 0E 03 33 D0 06 16 A0 1C 2F 43 26 83 1E 50 27 08 68 B0 6C 64 2C 66 F4 67 58 C0 30 1D 45 BF 06 E3 11 D6 27 70 71 4C 5B 89 15 83 B9 4E 89 71 29 AB 2C 7B 04 45 66 29 B3 51 A8 7F CB 15 CA F4 6F A6 50 FF 16 9D 81 D5 CF 40 61 F8 51 AA DF 87 42 F7 53 AA 7F A0 FD 3F D0 F1 4F 69 FA A7 38 FF 0C AC FF 95 98 BF 33 9B 9D 92 A7 CC 0D A7 29 D3 AF C4 92 CA 74 62 80 E3 41 89 F9 0F 93 1A A5 69 E9 23 07 85 E1 20 C7 64 45 A9 1B EE 8A 50 E8 06 5E 26 03 86 15 14 96 09 14 EA F7 A1 30 2D 50 AC 9F C2 38 F0 A5 34 4F B2 32 FF 1C E0 72 11 98 0E 98 13 07 38 1D 28 31 C7 B2 4C F4 1D D8 B4 A0 C4 14 CA AA 35 D0 75 64 88 34 65 FA 83 29 D4 6F B2 73 60 F5 9F A1 54 FF 0E CA D3 40 C8 53 0A E3 E0 09 85 6E 50 65 7D 22 BD 86 32 37 B0 BF A6 D0 0D 12 AC FB A4 D7 52 E6 06 E6 EF 0C FF 01 97 1D 12 C7 42 00 07 F0 C3 01 00 00 03 04 BA 4C B6 23 BA 8B 27 BE C8 55 59 86 24 9F 89 D4 FF 00 9F 01 00 00 01 00 00 00 00 00 00 00 00 00 FF FF 20 54 1C F0 97 01 00 00 BA 4C B6 23 BA 8B 27 BE C8 55 59 86 24 9F 89 D4 AE 0E 00 00 00 00 00 00 00 00 00 00 D1 07 00 00 DD 05 00 00 4A AD 6F 00 8A C5 53 00 65 01 00 00 00 FE 78 9C E3 5B C7 00 04 AC 77 D9 2F 32 08 32 FD E7 61 F8 FF 0F C8 FD 05 C5 30 19 10 90 63 90 FA 0F 06 0C 8C 0C 5C 70 19 43 30 EB 0E FB 05 86 85 0C DB 18 58 80 72 8C 70 16 0B 83 05 56 51 29 88 C9 60 D9 69 0C 6C 20 26 23 03 C8 74 B0 A8 0E 03 07 FB 45 56 C7 A2 CC C4 1C 06 66 A0 0D 2C 40 39 5E 86 4C 06 3D A0 4E 10 D0 60 99 C6 B8 98 D1 9F 61 01 C3 74 14 FD 1A 8C 2B D8 84 B1 88 4B A5 A5 75 03 01 50 DF 59 46 77 46 0F A8 3C A6 AB 88 15 83 B9 5E 89 B1 8B D5 97 2D 82 22 B3 94 29 D5 BF E5 CA C0 EA DF AC 43 A1 FD 14 EA 67 A0 30 FC 28 D5 EF 43 A1 FB 7D 87 B8 FF 07 3A FE 07 3A FD 53 EA 7E 0A C3 4F 89 F9 0E 73 EA 69 79 CA DC 70 8A 32 FD 4A 2C 5E 4C DF 87 7A 3C BC E0 A5 30 1E 3E 31 C5 33 AC A0 30 2F 52 A8 DF 87 C2 30 A4 54 3F A5 65 19 85 65 A9 12 D3 2B 16 0D 8A CB 13 4A F3 E3 27 E6 09 03 9D 0E 06 58 BF 12 B3 13 CB C1 01 4E 8B 4A 4C 56 AC 91 03 5D 37 86 48 53 A6 3F 98 42 FD 26 3B 07 56 FF 99 1D 14 EA A7 CC 7E 70 1A 08 79 42 61 1C 3C A5 D0 0D 9C 6C C2 32 6B 29 73 03 DB 6B CA DC C0 F8 97 F5 AD CC 1A CA DC C0 F4 83 32 37 B0 A4 30 CE FC C7 48 99 1B FE 33 32 FC 07 00 6C CC 2E 23 33 00 0B F0 12 00 00 00 BF 00 08 00 08 00 81 01 09 00 00 08 C0 01 40 00 00 08 40 00 1E F1 10 00 00 00 0D 00 00 08 0C 00 00 08 17 00 00 08 F7 00 00 10                                              ";
        final byte[] bytes = HexRead.readFromString(dump);
        final EscherDump dumper = new EscherDump();
        dumper.dump(bytes, 0, bytes.length, System.out);
    }
    
    public void dump(final int recordSize, final byte[] data, final PrintStream out) {
        this.dump(data, 0, recordSize, out);
    }
}
