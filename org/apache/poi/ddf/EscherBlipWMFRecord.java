// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.ddf;

import java.io.InputStream;
import java.util.zip.InflaterInputStream;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import org.apache.poi.util.RecordFormatException;
import java.util.zip.DeflaterOutputStream;
import java.io.OutputStream;
import org.apache.poi.util.HexDump;
import java.io.ByteArrayOutputStream;
import org.apache.poi.util.LittleEndian;

public class EscherBlipWMFRecord extends EscherBlipRecord
{
    public static final String RECORD_DESCRIPTION = "msofbtBlip";
    private static final int HEADER_SIZE = 8;
    private byte[] field_1_secondaryUID;
    private int field_2_cacheOfSize;
    private int field_3_boundaryTop;
    private int field_4_boundaryLeft;
    private int field_5_boundaryWidth;
    private int field_6_boundaryHeight;
    private int field_7_width;
    private int field_8_height;
    private int field_9_cacheOfSavedSize;
    private byte field_10_compressionFlag;
    private byte field_11_filter;
    private byte[] field_12_data;
    
    @Override
    public int fillFields(final byte[] data, final int offset, final EscherRecordFactory recordFactory) {
        final int bytesAfterHeader = this.readHeader(data, offset);
        final int pos = offset + 8;
        int size = 0;
        System.arraycopy(data, pos + size, this.field_1_secondaryUID = new byte[16], 0, 16);
        size += 16;
        this.field_2_cacheOfSize = LittleEndian.getInt(data, pos + size);
        size += 4;
        this.field_3_boundaryTop = LittleEndian.getInt(data, pos + size);
        size += 4;
        this.field_4_boundaryLeft = LittleEndian.getInt(data, pos + size);
        size += 4;
        this.field_5_boundaryWidth = LittleEndian.getInt(data, pos + size);
        size += 4;
        this.field_6_boundaryHeight = LittleEndian.getInt(data, pos + size);
        size += 4;
        this.field_7_width = LittleEndian.getInt(data, pos + size);
        size += 4;
        this.field_8_height = LittleEndian.getInt(data, pos + size);
        size += 4;
        this.field_9_cacheOfSavedSize = LittleEndian.getInt(data, pos + size);
        size += 4;
        this.field_10_compressionFlag = data[pos + size];
        ++size;
        this.field_11_filter = data[pos + size];
        ++size;
        final int bytesRemaining = bytesAfterHeader - size;
        System.arraycopy(data, pos + size, this.field_12_data = new byte[bytesRemaining], 0, bytesRemaining);
        size += bytesRemaining;
        return 8 + size;
    }
    
    @Override
    public int serialize(final int offset, final byte[] data, final EscherSerializationListener listener) {
        listener.beforeRecordSerialize(offset, this.getRecordId(), this);
        LittleEndian.putShort(data, offset, this.getOptions());
        LittleEndian.putShort(data, offset + 2, this.getRecordId());
        final int remainingBytes = this.field_12_data.length + 36;
        LittleEndian.putInt(data, offset + 4, remainingBytes);
        int pos = offset + 8;
        System.arraycopy(this.field_1_secondaryUID, 0, data, pos, 16);
        pos += 16;
        LittleEndian.putInt(data, pos, this.field_2_cacheOfSize);
        pos += 4;
        LittleEndian.putInt(data, pos, this.field_3_boundaryTop);
        pos += 4;
        LittleEndian.putInt(data, pos, this.field_4_boundaryLeft);
        pos += 4;
        LittleEndian.putInt(data, pos, this.field_5_boundaryWidth);
        pos += 4;
        LittleEndian.putInt(data, pos, this.field_6_boundaryHeight);
        pos += 4;
        LittleEndian.putInt(data, pos, this.field_7_width);
        pos += 4;
        LittleEndian.putInt(data, pos, this.field_8_height);
        pos += 4;
        LittleEndian.putInt(data, pos, this.field_9_cacheOfSavedSize);
        pos += 4;
        data[pos++] = this.field_10_compressionFlag;
        data[pos++] = this.field_11_filter;
        System.arraycopy(this.field_12_data, 0, data, pos, this.field_12_data.length);
        pos += this.field_12_data.length;
        listener.afterRecordSerialize(pos, this.getRecordId(), pos - offset, this);
        return pos - offset;
    }
    
    @Override
    public int getRecordSize() {
        return 58 + this.field_12_data.length;
    }
    
    @Override
    public String getRecordName() {
        return "Blip";
    }
    
    public byte[] getSecondaryUID() {
        return this.field_1_secondaryUID;
    }
    
    public void setSecondaryUID(final byte[] field_1_secondaryUID) {
        this.field_1_secondaryUID = field_1_secondaryUID;
    }
    
    public int getCacheOfSize() {
        return this.field_2_cacheOfSize;
    }
    
    public void setCacheOfSize(final int field_2_cacheOfSize) {
        this.field_2_cacheOfSize = field_2_cacheOfSize;
    }
    
    public int getBoundaryTop() {
        return this.field_3_boundaryTop;
    }
    
    public void setBoundaryTop(final int field_3_boundaryTop) {
        this.field_3_boundaryTop = field_3_boundaryTop;
    }
    
    public int getBoundaryLeft() {
        return this.field_4_boundaryLeft;
    }
    
    public void setBoundaryLeft(final int field_4_boundaryLeft) {
        this.field_4_boundaryLeft = field_4_boundaryLeft;
    }
    
    public int getBoundaryWidth() {
        return this.field_5_boundaryWidth;
    }
    
    public void setBoundaryWidth(final int field_5_boundaryWidth) {
        this.field_5_boundaryWidth = field_5_boundaryWidth;
    }
    
    public int getBoundaryHeight() {
        return this.field_6_boundaryHeight;
    }
    
    public void setBoundaryHeight(final int field_6_boundaryHeight) {
        this.field_6_boundaryHeight = field_6_boundaryHeight;
    }
    
    public int getWidth() {
        return this.field_7_width;
    }
    
    public void setWidth(final int width) {
        this.field_7_width = width;
    }
    
    public int getHeight() {
        return this.field_8_height;
    }
    
    public void setHeight(final int height) {
        this.field_8_height = height;
    }
    
    public int getCacheOfSavedSize() {
        return this.field_9_cacheOfSavedSize;
    }
    
    public void setCacheOfSavedSize(final int field_9_cacheOfSavedSize) {
        this.field_9_cacheOfSavedSize = field_9_cacheOfSavedSize;
    }
    
    public byte getCompressionFlag() {
        return this.field_10_compressionFlag;
    }
    
    public void setCompressionFlag(final byte field_10_compressionFlag) {
        this.field_10_compressionFlag = field_10_compressionFlag;
    }
    
    public byte getFilter() {
        return this.field_11_filter;
    }
    
    public void setFilter(final byte field_11_filter) {
        this.field_11_filter = field_11_filter;
    }
    
    public byte[] getData() {
        return this.field_12_data;
    }
    
    public void setData(final byte[] field_12_data) {
        this.field_12_data = field_12_data;
    }
    
    @Override
    public String toString() {
        final String nl = System.getProperty("line.separator");
        final ByteArrayOutputStream b = new ByteArrayOutputStream();
        String extraData;
        try {
            HexDump.dump(this.field_12_data, 0L, b, 0);
            extraData = b.toString();
        }
        catch (Exception e) {
            extraData = e.toString();
        }
        return this.getClass().getName() + ":" + nl + "  RecordId: 0x" + HexDump.toHex(this.getRecordId()) + nl + "  Version: 0x" + HexDump.toHex(this.getVersion()) + nl + "  Instance: 0x" + HexDump.toHex(this.getInstance()) + nl + "  Secondary UID: " + HexDump.toHex(this.field_1_secondaryUID) + nl + "  CacheOfSize: " + this.field_2_cacheOfSize + nl + "  BoundaryTop: " + this.field_3_boundaryTop + nl + "  BoundaryLeft: " + this.field_4_boundaryLeft + nl + "  BoundaryWidth: " + this.field_5_boundaryWidth + nl + "  BoundaryHeight: " + this.field_6_boundaryHeight + nl + "  X: " + this.field_7_width + nl + "  Y: " + this.field_8_height + nl + "  CacheOfSavedSize: " + this.field_9_cacheOfSavedSize + nl + "  CompressionFlag: " + this.field_10_compressionFlag + nl + "  Filter: " + this.field_11_filter + nl + "  Data:" + nl + extraData;
    }
    
    public static byte[] compress(final byte[] data) {
        final ByteArrayOutputStream out = new ByteArrayOutputStream();
        final DeflaterOutputStream deflaterOutputStream = new DeflaterOutputStream(out);
        try {
            for (int i = 0; i < data.length; ++i) {
                deflaterOutputStream.write(data[i]);
            }
        }
        catch (IOException e) {
            throw new RecordFormatException(e.toString());
        }
        return out.toByteArray();
    }
    
    public static byte[] decompress(final byte[] data, final int pos, final int length) {
        final byte[] compressedData = new byte[length];
        System.arraycopy(data, pos + 50, compressedData, 0, length);
        final InputStream compressedInputStream = new ByteArrayInputStream(compressedData);
        final InflaterInputStream inflaterInputStream = new InflaterInputStream(compressedInputStream);
        final ByteArrayOutputStream out = new ByteArrayOutputStream();
        try {
            int c;
            while ((c = inflaterInputStream.read()) != -1) {
                out.write(c);
            }
        }
        catch (IOException e) {
            throw new RecordFormatException(e.toString());
        }
        return out.toByteArray();
    }
}
