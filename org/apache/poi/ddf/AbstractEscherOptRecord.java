// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.ddf;

import org.apache.poi.util.HexDump;
import java.util.Collections;
import java.util.Comparator;
import org.apache.poi.util.LittleEndian;
import java.util.Iterator;
import java.util.ArrayList;
import java.util.List;

public abstract class AbstractEscherOptRecord extends EscherRecord
{
    protected List<EscherProperty> properties;
    
    public AbstractEscherOptRecord() {
        this.properties = new ArrayList<EscherProperty>();
    }
    
    public void addEscherProperty(final EscherProperty prop) {
        this.properties.add(prop);
    }
    
    @Override
    public int fillFields(final byte[] data, final int offset, final EscherRecordFactory recordFactory) {
        final int bytesRemaining = this.readHeader(data, offset);
        final short propertiesCount = EscherRecord.readInstance(data, offset);
        final int pos = offset + 8;
        final EscherPropertyFactory f = new EscherPropertyFactory();
        this.properties = f.createProperties(data, pos, propertiesCount);
        return bytesRemaining + 8;
    }
    
    public List<EscherProperty> getEscherProperties() {
        return this.properties;
    }
    
    public EscherProperty getEscherProperty(final int index) {
        return this.properties.get(index);
    }
    
    private int getPropertiesSize() {
        int totalSize = 0;
        for (final EscherProperty property : this.properties) {
            totalSize += property.getPropertySize();
        }
        return totalSize;
    }
    
    @Override
    public int getRecordSize() {
        return 8 + this.getPropertiesSize();
    }
    
    public <T extends EscherProperty> T lookup(final int propId) {
        for (final EscherProperty prop : this.properties) {
            if (prop.getPropertyNumber() == propId) {
                final T result = (T)prop;
                return result;
            }
        }
        return null;
    }
    
    @Override
    public int serialize(final int offset, final byte[] data, final EscherSerializationListener listener) {
        listener.beforeRecordSerialize(offset, this.getRecordId(), this);
        LittleEndian.putShort(data, offset, this.getOptions());
        LittleEndian.putShort(data, offset + 2, this.getRecordId());
        LittleEndian.putInt(data, offset + 4, this.getPropertiesSize());
        int pos = offset + 8;
        for (final EscherProperty property : this.properties) {
            pos += property.serializeSimplePart(data, pos);
        }
        for (final EscherProperty property : this.properties) {
            pos += property.serializeComplexPart(data, pos);
        }
        listener.afterRecordSerialize(pos, this.getRecordId(), pos - offset, this);
        return pos - offset;
    }
    
    public void sortProperties() {
        Collections.sort(this.properties, new Comparator<EscherProperty>() {
            public int compare(final EscherProperty p1, final EscherProperty p2) {
                final short s1 = p1.getPropertyNumber();
                final short s2 = p2.getPropertyNumber();
                return (s1 < s2) ? -1 : ((s1 == s2) ? 0 : 1);
            }
        });
    }
    
    @Override
    public String toString() {
        final String nl = System.getProperty("line.separator");
        final StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(this.getClass().getName());
        stringBuilder.append(":");
        stringBuilder.append(nl);
        stringBuilder.append("  isContainer: ");
        stringBuilder.append(this.isContainerRecord());
        stringBuilder.append(nl);
        stringBuilder.append("  version: 0x");
        stringBuilder.append(HexDump.toHex(this.getVersion()));
        stringBuilder.append(nl);
        stringBuilder.append("  instance: 0x");
        stringBuilder.append(HexDump.toHex(this.getInstance()));
        stringBuilder.append(nl);
        stringBuilder.append("  recordId: 0x");
        stringBuilder.append(HexDump.toHex(this.getRecordId()));
        stringBuilder.append(nl);
        stringBuilder.append("  numchildren: ");
        stringBuilder.append(this.getChildRecords().size());
        stringBuilder.append(nl);
        stringBuilder.append("  properties:");
        stringBuilder.append(nl);
        for (final EscherProperty property : this.properties) {
            stringBuilder.append("    " + property.toString() + nl);
        }
        return stringBuilder.toString();
    }
}
