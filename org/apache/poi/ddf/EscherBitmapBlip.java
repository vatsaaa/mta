// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.ddf;

import java.io.OutputStream;
import org.apache.poi.util.HexDump;
import java.io.ByteArrayOutputStream;
import org.apache.poi.util.LittleEndian;

public class EscherBitmapBlip extends EscherBlipRecord
{
    public static final short RECORD_ID_JPEG = -4067;
    public static final short RECORD_ID_PNG = -4066;
    public static final short RECORD_ID_DIB = -4065;
    private static final int HEADER_SIZE = 8;
    private byte[] field_1_UID;
    private byte field_2_marker;
    
    public EscherBitmapBlip() {
        this.field_2_marker = -1;
    }
    
    @Override
    public int fillFields(final byte[] data, final int offset, final EscherRecordFactory recordFactory) {
        final int bytesAfterHeader = this.readHeader(data, offset);
        int pos = offset + 8;
        System.arraycopy(data, pos, this.field_1_UID = new byte[16], 0, 16);
        pos += 16;
        this.field_2_marker = data[pos];
        ++pos;
        System.arraycopy(data, pos, this.field_pictureData = new byte[bytesAfterHeader - 17], 0, this.field_pictureData.length);
        return bytesAfterHeader + 8;
    }
    
    @Override
    public int serialize(final int offset, final byte[] data, final EscherSerializationListener listener) {
        listener.beforeRecordSerialize(offset, this.getRecordId(), this);
        LittleEndian.putShort(data, offset, this.getOptions());
        LittleEndian.putShort(data, offset + 2, this.getRecordId());
        LittleEndian.putInt(data, offset + 4, this.getRecordSize() - 8);
        final int pos = offset + 8;
        System.arraycopy(this.field_1_UID, 0, data, pos, 16);
        data[pos + 16] = this.field_2_marker;
        System.arraycopy(this.field_pictureData, 0, data, pos + 17, this.field_pictureData.length);
        listener.afterRecordSerialize(offset + this.getRecordSize(), this.getRecordId(), this.getRecordSize(), this);
        return 25 + this.field_pictureData.length;
    }
    
    @Override
    public int getRecordSize() {
        return 25 + this.field_pictureData.length;
    }
    
    public byte[] getUID() {
        return this.field_1_UID;
    }
    
    public void setUID(final byte[] field_1_UID) {
        this.field_1_UID = field_1_UID;
    }
    
    public byte getMarker() {
        return this.field_2_marker;
    }
    
    public void setMarker(final byte field_2_marker) {
        this.field_2_marker = field_2_marker;
    }
    
    @Override
    public String toString() {
        final String nl = System.getProperty("line.separator");
        final ByteArrayOutputStream b = new ByteArrayOutputStream();
        String extraData;
        try {
            HexDump.dump(this.field_pictureData, 0L, b, 0);
            extraData = b.toString();
        }
        catch (Exception e) {
            extraData = e.toString();
        }
        return this.getClass().getName() + ":" + nl + "  RecordId: 0x" + HexDump.toHex(this.getRecordId()) + nl + "  Version: 0x" + HexDump.toHex(this.getVersion()) + nl + "  Instance: 0x" + HexDump.toHex(this.getInstance()) + nl + "  UID: 0x" + HexDump.toHex(this.field_1_UID) + nl + "  Marker: 0x" + HexDump.toHex(this.field_2_marker) + nl + "  Extra Data:" + nl + extraData;
    }
}
