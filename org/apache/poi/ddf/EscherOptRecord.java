// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.ddf;

import org.apache.poi.util.Internal;

public class EscherOptRecord extends AbstractEscherOptRecord
{
    public static final String RECORD_DESCRIPTION = "msofbtOPT";
    public static final short RECORD_ID = -4085;
    
    @Override
    public short getInstance() {
        this.setInstance((short)this.properties.size());
        return super.getInstance();
    }
    
    @Internal
    @Override
    public short getOptions() {
        this.getInstance();
        this.getVersion();
        return super.getOptions();
    }
    
    @Override
    public String getRecordName() {
        return "Opt";
    }
    
    @Override
    public short getVersion() {
        this.setVersion((short)3);
        return super.getVersion();
    }
    
    @Override
    public void setVersion(final short value) {
        if (value != 3) {
            throw new IllegalArgumentException("msofbtOPT can have only '0x3' version");
        }
        super.setVersion(value);
    }
}
