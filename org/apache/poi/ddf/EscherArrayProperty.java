// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.ddf;

import org.apache.poi.util.HexDump;
import org.apache.poi.util.LittleEndian;

public final class EscherArrayProperty extends EscherComplexProperty
{
    private static final int FIXED_SIZE = 6;
    private boolean sizeIncludesHeaderSize;
    private boolean emptyComplexPart;
    
    public EscherArrayProperty(final short id, final byte[] complexData) {
        super(id, checkComplexData(complexData));
        this.sizeIncludesHeaderSize = true;
        this.emptyComplexPart = false;
        this.emptyComplexPart = (complexData.length == 0);
    }
    
    public EscherArrayProperty(final short propertyNumber, final boolean isBlipId, final byte[] complexData) {
        super(propertyNumber, isBlipId, checkComplexData(complexData));
        this.sizeIncludesHeaderSize = true;
        this.emptyComplexPart = false;
    }
    
    private static byte[] checkComplexData(final byte[] complexData) {
        if (complexData == null || complexData.length == 0) {
            return new byte[6];
        }
        return complexData;
    }
    
    public int getNumberOfElementsInArray() {
        return LittleEndian.getUShort(this._complexData, 0);
    }
    
    public void setNumberOfElementsInArray(final int numberOfElements) {
        final int expectedArraySize = numberOfElements * getActualSizeOfElements(this.getSizeOfElements()) + 6;
        if (expectedArraySize != this._complexData.length) {
            final byte[] newArray = new byte[expectedArraySize];
            System.arraycopy(this._complexData, 0, newArray, 0, this._complexData.length);
            this._complexData = newArray;
        }
        LittleEndian.putShort(this._complexData, 0, (short)numberOfElements);
    }
    
    public int getNumberOfElementsInMemory() {
        return LittleEndian.getUShort(this._complexData, 2);
    }
    
    public void setNumberOfElementsInMemory(final int numberOfElements) {
        final int expectedArraySize = numberOfElements * getActualSizeOfElements(this.getSizeOfElements()) + 6;
        if (expectedArraySize != this._complexData.length) {
            final byte[] newArray = new byte[expectedArraySize];
            System.arraycopy(this._complexData, 0, newArray, 0, expectedArraySize);
            this._complexData = newArray;
        }
        LittleEndian.putShort(this._complexData, 2, (short)numberOfElements);
    }
    
    public short getSizeOfElements() {
        return LittleEndian.getShort(this._complexData, 4);
    }
    
    public void setSizeOfElements(final int sizeOfElements) {
        LittleEndian.putShort(this._complexData, 4, (short)sizeOfElements);
        final int expectedArraySize = this.getNumberOfElementsInArray() * getActualSizeOfElements(this.getSizeOfElements()) + 6;
        if (expectedArraySize != this._complexData.length) {
            final byte[] newArray = new byte[expectedArraySize];
            System.arraycopy(this._complexData, 0, newArray, 0, 6);
            this._complexData = newArray;
        }
    }
    
    public byte[] getElement(final int index) {
        final int actualSize = getActualSizeOfElements(this.getSizeOfElements());
        final byte[] result = new byte[actualSize];
        System.arraycopy(this._complexData, 6 + index * actualSize, result, 0, result.length);
        return result;
    }
    
    public void setElement(final int index, final byte[] element) {
        final int actualSize = getActualSizeOfElements(this.getSizeOfElements());
        System.arraycopy(element, 0, this._complexData, 6 + index * actualSize, actualSize);
    }
    
    @Override
    public String toString() {
        final StringBuffer results = new StringBuffer();
        results.append("    {EscherArrayProperty:\n");
        results.append("     Num Elements: " + this.getNumberOfElementsInArray() + '\n');
        results.append("     Num Elements In Memory: " + this.getNumberOfElementsInMemory() + '\n');
        results.append("     Size of elements: " + this.getSizeOfElements() + '\n');
        for (int i = 0; i < this.getNumberOfElementsInArray(); ++i) {
            results.append("     Element " + i + ": " + HexDump.toHex(this.getElement(i)) + '\n');
        }
        results.append("}\n");
        return "propNum: " + this.getPropertyNumber() + ", propName: " + EscherProperties.getPropertyName(this.getPropertyNumber()) + ", complex: " + this.isComplex() + ", blipId: " + this.isBlipId() + ", data: " + '\n' + results.toString();
    }
    
    public int setArrayData(final byte[] data, final int offset) {
        if (this.emptyComplexPart) {
            this._complexData = new byte[0];
        }
        else {
            final short numElements = LittleEndian.getShort(data, offset);
            LittleEndian.getShort(data, offset + 2);
            final short sizeOfElements = LittleEndian.getShort(data, offset + 4);
            final int arraySize = getActualSizeOfElements(sizeOfElements) * numElements;
            if (arraySize == this._complexData.length) {
                this._complexData = new byte[arraySize + 6];
                this.sizeIncludesHeaderSize = false;
            }
            System.arraycopy(data, offset, this._complexData, 0, this._complexData.length);
        }
        return this._complexData.length;
    }
    
    @Override
    public int serializeSimplePart(final byte[] data, final int pos) {
        LittleEndian.putShort(data, pos, this.getId());
        int recordSize = this._complexData.length;
        if (!this.sizeIncludesHeaderSize) {
            recordSize -= 6;
        }
        LittleEndian.putInt(data, pos + 2, recordSize);
        return 6;
    }
    
    public static int getActualSizeOfElements(final short sizeOfElements) {
        if (sizeOfElements < 0) {
            return (short)(-sizeOfElements >> 2);
        }
        return sizeOfElements;
    }
}
