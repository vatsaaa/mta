// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.ddf;

import java.util.Iterator;
import org.apache.poi.util.LittleEndian;
import java.util.ArrayList;
import java.util.List;

public final class EscherPropertyFactory
{
    public List<EscherProperty> createProperties(final byte[] data, final int offset, final short numProperties) {
        final List<EscherProperty> results = new ArrayList<EscherProperty>();
        int pos = offset;
        for (int i = 0; i < numProperties; ++i) {
            final short propId = LittleEndian.getShort(data, pos);
            final int propData = LittleEndian.getInt(data, pos + 2);
            final short propNumber = (short)(propId & 0x3FFF);
            final boolean isComplex = (propId & 0xFFFF8000) != 0x0;
            final boolean isBlipId = (propId & 0x4000) != 0x0;
            final byte propertyType = EscherProperties.getPropertyType(propNumber);
            if (propertyType == 1) {
                results.add(new EscherBoolProperty(propId, propData));
            }
            else if (propertyType == 2) {
                results.add(new EscherRGBProperty(propId, propData));
            }
            else if (propertyType == 3) {
                results.add(new EscherShapePathProperty(propId, propData));
            }
            else if (!isComplex) {
                results.add(new EscherSimpleProperty(propId, propData));
            }
            else if (propertyType == 5) {
                results.add(new EscherArrayProperty(propId, new byte[propData]));
            }
            else {
                results.add(new EscherComplexProperty(propId, new byte[propData]));
            }
            pos += 6;
        }
        for (final EscherProperty p : results) {
            if (p instanceof EscherComplexProperty) {
                if (p instanceof EscherArrayProperty) {
                    pos += ((EscherArrayProperty)p).setArrayData(data, pos);
                }
                else {
                    final byte[] complexData = ((EscherComplexProperty)p).getComplexData();
                    System.arraycopy(data, pos, complexData, 0, complexData.length);
                    pos += complexData.length;
                }
            }
        }
        return results;
    }
}
