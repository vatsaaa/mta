// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.ddf;

import java.io.OutputStream;
import org.apache.poi.util.HexDump;
import java.io.ByteArrayOutputStream;
import org.apache.poi.util.LittleEndian;

public class EscherClientDataRecord extends EscherRecord
{
    public static final short RECORD_ID = -4079;
    public static final String RECORD_DESCRIPTION = "MsofbtClientData";
    private byte[] remainingData;
    
    @Override
    public int fillFields(final byte[] data, final int offset, final EscherRecordFactory recordFactory) {
        final int bytesRemaining = this.readHeader(data, offset);
        final int pos = offset + 8;
        System.arraycopy(data, pos, this.remainingData = new byte[bytesRemaining], 0, bytesRemaining);
        return 8 + bytesRemaining;
    }
    
    @Override
    public int serialize(final int offset, final byte[] data, final EscherSerializationListener listener) {
        listener.beforeRecordSerialize(offset, this.getRecordId(), this);
        if (this.remainingData == null) {
            this.remainingData = new byte[0];
        }
        LittleEndian.putShort(data, offset, this.getOptions());
        LittleEndian.putShort(data, offset + 2, this.getRecordId());
        LittleEndian.putInt(data, offset + 4, this.remainingData.length);
        System.arraycopy(this.remainingData, 0, data, offset + 8, this.remainingData.length);
        final int pos = offset + 8 + this.remainingData.length;
        listener.afterRecordSerialize(pos, this.getRecordId(), pos - offset, this);
        return pos - offset;
    }
    
    @Override
    public int getRecordSize() {
        return 8 + ((this.remainingData == null) ? 0 : this.remainingData.length);
    }
    
    @Override
    public short getRecordId() {
        return -4079;
    }
    
    @Override
    public String getRecordName() {
        return "ClientData";
    }
    
    @Override
    public String toString() {
        final String nl = System.getProperty("line.separator");
        final ByteArrayOutputStream b = new ByteArrayOutputStream();
        String extraData;
        try {
            HexDump.dump(this.remainingData, 0L, b, 0);
            extraData = b.toString();
        }
        catch (Exception e) {
            extraData = "error\n";
        }
        return this.getClass().getName() + ":" + nl + "  RecordId: 0x" + HexDump.toHex((short)(-4079)) + nl + "  Version: 0x" + HexDump.toHex(this.getVersion()) + nl + "  Instance: 0x" + HexDump.toHex(this.getInstance()) + nl + "  Extra Data:" + nl + extraData;
    }
    
    public byte[] getRemainingData() {
        return this.remainingData;
    }
    
    public void setRemainingData(final byte[] remainingData) {
        this.remainingData = remainingData;
    }
}
