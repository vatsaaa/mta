// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.ddf;

import org.apache.poi.util.HexDump;
import org.apache.poi.util.LittleEndian;

public class EscherBlipRecord extends EscherRecord
{
    public static final short RECORD_ID_START = -4072;
    public static final short RECORD_ID_END = -3817;
    public static final String RECORD_DESCRIPTION = "msofbtBlip";
    private static final int HEADER_SIZE = 8;
    protected byte[] field_pictureData;
    
    @Override
    public int fillFields(final byte[] data, final int offset, final EscherRecordFactory recordFactory) {
        final int bytesAfterHeader = this.readHeader(data, offset);
        final int pos = offset + 8;
        System.arraycopy(data, pos, this.field_pictureData = new byte[bytesAfterHeader], 0, bytesAfterHeader);
        return bytesAfterHeader + 8;
    }
    
    @Override
    public int serialize(final int offset, final byte[] data, final EscherSerializationListener listener) {
        listener.beforeRecordSerialize(offset, this.getRecordId(), this);
        LittleEndian.putShort(data, offset, this.getOptions());
        LittleEndian.putShort(data, offset + 2, this.getRecordId());
        System.arraycopy(this.field_pictureData, 0, data, offset + 4, this.field_pictureData.length);
        listener.afterRecordSerialize(offset + 4 + this.field_pictureData.length, this.getRecordId(), this.field_pictureData.length + 4, this);
        return this.field_pictureData.length + 4;
    }
    
    @Override
    public int getRecordSize() {
        return this.field_pictureData.length + 8;
    }
    
    @Override
    public String getRecordName() {
        return "Blip";
    }
    
    public byte[] getPicturedata() {
        return this.field_pictureData;
    }
    
    public void setPictureData(final byte[] pictureData) {
        this.field_pictureData = pictureData;
    }
    
    @Override
    public String toString() {
        final String extraData = HexDump.toHex(this.field_pictureData, 32);
        return this.getClass().getName() + ":" + '\n' + "  RecordId: 0x" + HexDump.toHex(this.getRecordId()) + '\n' + "  Version: 0x" + HexDump.toHex(this.getVersion()) + '\n' + "  Instance: 0x" + HexDump.toHex(this.getInstance()) + '\n' + "  Extra Data:" + '\n' + extraData;
    }
}
