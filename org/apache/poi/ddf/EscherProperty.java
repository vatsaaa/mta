// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.ddf;

public abstract class EscherProperty
{
    private short _id;
    
    public EscherProperty(final short id) {
        this._id = id;
    }
    
    public EscherProperty(final short propertyNumber, final boolean isComplex, final boolean isBlipId) {
        this._id = (short)(propertyNumber + (isComplex ? 32768 : 0) + (isBlipId ? 16384 : 0));
    }
    
    public short getId() {
        return this._id;
    }
    
    public short getPropertyNumber() {
        return (short)(this._id & 0x3FFF);
    }
    
    public boolean isComplex() {
        return (this._id & 0xFFFF8000) != 0x0;
    }
    
    public boolean isBlipId() {
        return (this._id & 0x4000) != 0x0;
    }
    
    public String getName() {
        return EscherProperties.getPropertyName(this.getPropertyNumber());
    }
    
    public int getPropertySize() {
        return 6;
    }
    
    public abstract int serializeSimplePart(final byte[] p0, final int p1);
    
    public abstract int serializeComplexPart(final byte[] p0, final int p1);
}
