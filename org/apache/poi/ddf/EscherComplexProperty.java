// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.ddf;

import org.apache.poi.util.HexDump;
import java.util.Arrays;
import org.apache.poi.util.LittleEndian;

public class EscherComplexProperty extends EscherProperty
{
    protected byte[] _complexData;
    
    public EscherComplexProperty(final short id, final byte[] complexData) {
        super(id);
        this._complexData = complexData;
    }
    
    public EscherComplexProperty(final short propertyNumber, final boolean isBlipId, final byte[] complexData) {
        super(propertyNumber, true, isBlipId);
        this._complexData = complexData;
    }
    
    @Override
    public int serializeSimplePart(final byte[] data, final int pos) {
        LittleEndian.putShort(data, pos, this.getId());
        LittleEndian.putInt(data, pos + 2, this._complexData.length);
        return 6;
    }
    
    @Override
    public int serializeComplexPart(final byte[] data, final int pos) {
        System.arraycopy(this._complexData, 0, data, pos, this._complexData.length);
        return this._complexData.length;
    }
    
    public byte[] getComplexData() {
        return this._complexData;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof EscherComplexProperty)) {
            return false;
        }
        final EscherComplexProperty escherComplexProperty = (EscherComplexProperty)o;
        return Arrays.equals(this._complexData, escherComplexProperty._complexData);
    }
    
    @Override
    public int getPropertySize() {
        return 6 + this._complexData.length;
    }
    
    @Override
    public int hashCode() {
        return this.getId() * 11;
    }
    
    @Override
    public String toString() {
        final String dataStr = HexDump.toHex(this._complexData, 32);
        return "propNum: " + this.getPropertyNumber() + ", propName: " + EscherProperties.getPropertyName(this.getPropertyNumber()) + ", complex: " + this.isComplex() + ", blipId: " + this.isBlipId() + ", data: " + System.getProperty("line.separator") + dataStr;
    }
}
