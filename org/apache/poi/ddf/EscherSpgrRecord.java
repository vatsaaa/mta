// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.ddf;

import org.apache.poi.util.HexDump;
import org.apache.poi.util.RecordFormatException;
import org.apache.poi.util.LittleEndian;

public class EscherSpgrRecord extends EscherRecord
{
    public static final short RECORD_ID = -4087;
    public static final String RECORD_DESCRIPTION = "MsofbtSpgr";
    private int field_1_rectX1;
    private int field_2_rectY1;
    private int field_3_rectX2;
    private int field_4_rectY2;
    
    @Override
    public int fillFields(final byte[] data, final int offset, final EscherRecordFactory recordFactory) {
        int bytesRemaining = this.readHeader(data, offset);
        final int pos = offset + 8;
        int size = 0;
        this.field_1_rectX1 = LittleEndian.getInt(data, pos + size);
        size += 4;
        this.field_2_rectY1 = LittleEndian.getInt(data, pos + size);
        size += 4;
        this.field_3_rectX2 = LittleEndian.getInt(data, pos + size);
        size += 4;
        this.field_4_rectY2 = LittleEndian.getInt(data, pos + size);
        size += 4;
        bytesRemaining -= size;
        if (bytesRemaining != 0) {
            throw new RecordFormatException("Expected no remaining bytes but got " + bytesRemaining);
        }
        return 8 + size + bytesRemaining;
    }
    
    @Override
    public int serialize(final int offset, final byte[] data, final EscherSerializationListener listener) {
        listener.beforeRecordSerialize(offset, this.getRecordId(), this);
        LittleEndian.putShort(data, offset, this.getOptions());
        LittleEndian.putShort(data, offset + 2, this.getRecordId());
        final int remainingBytes = 16;
        LittleEndian.putInt(data, offset + 4, remainingBytes);
        LittleEndian.putInt(data, offset + 8, this.field_1_rectX1);
        LittleEndian.putInt(data, offset + 12, this.field_2_rectY1);
        LittleEndian.putInt(data, offset + 16, this.field_3_rectX2);
        LittleEndian.putInt(data, offset + 20, this.field_4_rectY2);
        listener.afterRecordSerialize(offset + this.getRecordSize(), this.getRecordId(), offset + this.getRecordSize(), this);
        return 24;
    }
    
    @Override
    public int getRecordSize() {
        return 24;
    }
    
    @Override
    public short getRecordId() {
        return -4087;
    }
    
    @Override
    public String getRecordName() {
        return "Spgr";
    }
    
    @Override
    public String toString() {
        return this.getClass().getName() + ":" + '\n' + "  RecordId: 0x" + HexDump.toHex((short)(-4087)) + '\n' + "  Version: 0x" + HexDump.toHex(this.getVersion()) + '\n' + "  Instance: 0x" + HexDump.toHex(this.getInstance()) + '\n' + "  RectX: " + this.field_1_rectX1 + '\n' + "  RectY: " + this.field_2_rectY1 + '\n' + "  RectWidth: " + this.field_3_rectX2 + '\n' + "  RectHeight: " + this.field_4_rectY2 + '\n';
    }
    
    public int getRectX1() {
        return this.field_1_rectX1;
    }
    
    public void setRectX1(final int x1) {
        this.field_1_rectX1 = x1;
    }
    
    public int getRectY1() {
        return this.field_2_rectY1;
    }
    
    public void setRectY1(final int y1) {
        this.field_2_rectY1 = y1;
    }
    
    public int getRectX2() {
        return this.field_3_rectX2;
    }
    
    public void setRectX2(final int x2) {
        this.field_3_rectX2 = x2;
    }
    
    public int getRectY2() {
        return this.field_4_rectY2;
    }
    
    public void setRectY2(final int rectY2) {
        this.field_4_rectY2 = rectY2;
    }
}
