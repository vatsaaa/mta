// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.xslf.extractor;

import org.apache.poi.xslf.usermodel.DrawingParagraph;
import org.apache.poi.xslf.usermodel.DrawingTextPlaceholder;
import org.apache.poi.xslf.usermodel.DrawingTextBody;
import org.apache.poi.xslf.usermodel.XSLFCommonSlideData;
import org.openxmlformats.schemas.presentationml.x2006.main.CTCommentAuthor;
import java.util.Iterator;
import org.apache.poi.xslf.usermodel.XSLFSlideMaster;
import org.apache.poi.xslf.usermodel.XSLFSlideLayout;
import org.apache.poi.xslf.usermodel.XSLFComments;
import org.apache.poi.xslf.usermodel.XSLFNotes;
import org.apache.poi.xslf.usermodel.XSLFCommentAuthors;
import org.apache.poi.xslf.usermodel.XSLFSlide;
import org.openxmlformats.schemas.presentationml.x2006.main.CTComment;
import org.apache.poi.openxml4j.exceptions.OpenXML4JException;
import org.apache.poi.openxml4j.opc.OPCPackage;
import java.io.IOException;
import org.apache.xmlbeans.XmlException;
import org.apache.poi.xslf.XSLFSlideShow;
import org.apache.poi.POIXMLDocument;
import org.apache.poi.xslf.usermodel.XMLSlideShow;
import org.apache.poi.xslf.usermodel.XSLFRelation;
import org.apache.poi.POIXMLTextExtractor;

public class XSLFPowerPointExtractor extends POIXMLTextExtractor
{
    public static final XSLFRelation[] SUPPORTED_TYPES;
    private XMLSlideShow slideshow;
    private boolean slidesByDefault;
    private boolean notesByDefault;
    private boolean masterByDefault;
    
    public XSLFPowerPointExtractor(final XMLSlideShow slideshow) {
        super(slideshow);
        this.slidesByDefault = true;
        this.notesByDefault = false;
        this.masterByDefault = false;
        this.slideshow = slideshow;
    }
    
    public XSLFPowerPointExtractor(final XSLFSlideShow slideshow) throws XmlException, IOException {
        this(new XMLSlideShow(slideshow.getPackage()));
    }
    
    public XSLFPowerPointExtractor(final OPCPackage container) throws XmlException, OpenXML4JException, IOException {
        this(new XSLFSlideShow(container));
    }
    
    public static void main(final String[] args) throws Exception {
        if (args.length < 1) {
            System.err.println("Use:");
            System.err.println("  XSLFPowerPointExtractor <filename.pptx>");
            System.exit(1);
        }
        final POIXMLTextExtractor extractor = new XSLFPowerPointExtractor(new XSLFSlideShow(args[0]));
        System.out.println(extractor.getText());
    }
    
    public void setSlidesByDefault(final boolean slidesByDefault) {
        this.slidesByDefault = slidesByDefault;
    }
    
    public void setNotesByDefault(final boolean notesByDefault) {
        this.notesByDefault = notesByDefault;
    }
    
    public void setMasterByDefault(final boolean masterByDefault) {
        this.masterByDefault = masterByDefault;
    }
    
    @Override
    public String getText() {
        return this.getText(this.slidesByDefault, this.notesByDefault);
    }
    
    public String getText(final boolean slideText, final boolean notesText) {
        return this.getText(slideText, notesText, this.masterByDefault);
    }
    
    public String getText(final boolean slideText, final boolean notesText, final boolean masterText) {
        final StringBuffer text = new StringBuffer();
        final XSLFSlide[] slides = this.slideshow.getSlides();
        final XSLFCommentAuthors commentAuthors = this.slideshow.getCommentAuthors();
        for (final XSLFSlide slide : slides) {
            try {
                final XSLFNotes notes = slide.getNotes();
                final XSLFComments comments = slide.getComments();
                final XSLFSlideLayout layout = slide.getSlideLayout();
                final XSLFSlideMaster master = layout.getSlideMaster();
                if (slideText) {
                    this.extractText(slide.getCommonSlideData(), false, text);
                    if (masterText) {
                        if (layout != null) {
                            this.extractText(layout.getCommonSlideData(), true, text);
                        }
                        if (master != null) {
                            this.extractText(master.getCommonSlideData(), true, text);
                        }
                    }
                    if (comments != null) {
                        for (final CTComment comment : comments.getCTCommentsList().getCmList()) {
                            if (commentAuthors != null) {
                                final CTCommentAuthor author = commentAuthors.getAuthorById(comment.getAuthorId());
                                if (author != null) {
                                    text.append(author.getName() + ": ");
                                }
                            }
                            text.append(comment.getText());
                            text.append("\n");
                        }
                    }
                }
                if (notesText && notes != null) {
                    this.extractText(notes.getCommonSlideData(), false, text);
                }
            }
            catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
        return text.toString();
    }
    
    private void extractText(final XSLFCommonSlideData data, final boolean skipPlaceholders, final StringBuffer text) {
        for (final DrawingTextBody textBody : data.getDrawingText()) {
            if (skipPlaceholders && textBody instanceof DrawingTextPlaceholder) {
                final DrawingTextPlaceholder ph = (DrawingTextPlaceholder)textBody;
                if (!ph.isPlaceholderCustom()) {
                    continue;
                }
            }
            for (final DrawingParagraph p : textBody.getParagraphs()) {
                text.append(p.getText());
                text.append("\n");
            }
        }
    }
    
    static {
        SUPPORTED_TYPES = new XSLFRelation[] { XSLFRelation.MAIN, XSLFRelation.MACRO, XSLFRelation.MACRO_TEMPLATE, XSLFRelation.PRESENTATIONML, XSLFRelation.PRESENTATIONML_TEMPLATE, XSLFRelation.PRESENTATION_MACRO };
    }
}
