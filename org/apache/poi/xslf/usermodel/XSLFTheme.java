// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.xslf.usermodel;

import org.openxmlformats.schemas.drawingml.x2006.main.CTTextParagraphProperties;
import java.io.OutputStream;
import javax.xml.namespace.QName;
import org.apache.xmlbeans.XmlOptions;
import org.apache.poi.util.Internal;
import org.openxmlformats.schemas.drawingml.x2006.main.CTColorMapping;
import org.apache.xmlbeans.XmlObject;
import org.openxmlformats.schemas.drawingml.x2006.main.CTColorScheme;
import org.openxmlformats.schemas.drawingml.x2006.main.CTBaseStyles;
import java.util.HashMap;
import org.apache.xmlbeans.XmlException;
import java.io.IOException;
import org.openxmlformats.schemas.drawingml.x2006.main.ThemeDocument;
import org.apache.poi.openxml4j.opc.PackageRelationship;
import org.apache.poi.openxml4j.opc.PackagePart;
import org.openxmlformats.schemas.drawingml.x2006.main.CTColor;
import java.util.Map;
import org.openxmlformats.schemas.drawingml.x2006.main.CTOfficeStyleSheet;
import org.apache.poi.POIXMLDocumentPart;

public class XSLFTheme extends POIXMLDocumentPart
{
    private CTOfficeStyleSheet _theme;
    private Map<String, CTColor> _schemeColors;
    
    XSLFTheme() {
        this._theme = CTOfficeStyleSheet.Factory.newInstance();
    }
    
    public XSLFTheme(final PackagePart part, final PackageRelationship rel) throws IOException, XmlException {
        super(part, rel);
        final ThemeDocument doc = ThemeDocument.Factory.parse(this.getPackagePart().getInputStream());
        this._theme = doc.getTheme();
        this.initialize();
    }
    
    private void initialize() {
        final CTBaseStyles elems = this._theme.getThemeElements();
        final CTColorScheme scheme = elems.getClrScheme();
        this._schemeColors = new HashMap<String, CTColor>(12);
        for (final XmlObject o : scheme.selectPath("*")) {
            final CTColor c = (CTColor)o;
            final String name = c.getDomNode().getLocalName();
            this._schemeColors.put(name, c);
        }
    }
    
    void initColorMap(final CTColorMapping cmap) {
        this._schemeColors.put("bg1", this._schemeColors.get(cmap.getBg1().toString()));
        this._schemeColors.put("bg2", this._schemeColors.get(cmap.getBg2().toString()));
        this._schemeColors.put("tx1", this._schemeColors.get(cmap.getTx1().toString()));
        this._schemeColors.put("tx2", this._schemeColors.get(cmap.getTx2().toString()));
    }
    
    public String getName() {
        return this._theme.getName();
    }
    
    public void setName(final String name) {
        this._theme.setName(name);
    }
    
    CTColor getCTColor(final String name) {
        return this._schemeColors.get(name);
    }
    
    @Internal
    public CTOfficeStyleSheet getXmlObject() {
        return this._theme;
    }
    
    @Override
    protected final void commit() throws IOException {
        final XmlOptions xmlOptions = new XmlOptions(XSLFTheme.DEFAULT_XML_OPTIONS);
        final Map<String, String> map = new HashMap<String, String>();
        map.put("http://schemas.openxmlformats.org/drawingml/2006/main", "a");
        xmlOptions.setSaveSuggestedPrefixes(map);
        xmlOptions.setSaveSyntheticDocumentElement(new QName("http://schemas.openxmlformats.org/drawingml/2006/main", "theme"));
        final PackagePart part = this.getPackagePart();
        final OutputStream out = part.getOutputStream();
        this.getXmlObject().save(out, xmlOptions);
        out.close();
    }
    
    public String getMajorFont() {
        return this._theme.getThemeElements().getFontScheme().getMajorFont().getLatin().getTypeface();
    }
    
    public String getMinorFont() {
        return this._theme.getThemeElements().getFontScheme().getMinorFont().getLatin().getTypeface();
    }
    
    CTTextParagraphProperties getDefaultParagraphStyle() {
        final XmlObject[] o = this._theme.selectPath("declare namespace p='http://schemas.openxmlformats.org/presentationml/2006/main' declare namespace a='http://schemas.openxmlformats.org/drawingml/2006/main' .//a:objectDefaults/a:spDef/a:lstStyle/a:defPPr");
        if (o.length == 1) {
            return (CTTextParagraphProperties)o[0];
        }
        return null;
    }
}
