// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.xslf.usermodel;

import java.io.IOException;
import org.apache.poi.openxml4j.opc.PackagePart;
import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.awt.geom.AffineTransform;
import javax.imageio.ImageIO;
import java.awt.geom.Rectangle2D;
import java.awt.Graphics2D;

public class XSLFImageRenderer
{
    public boolean drawImage(final Graphics2D graphics, final XSLFPictureData data, final Rectangle2D anchor) {
        try {
            final BufferedImage img = ImageIO.read(data.getPackagePart().getInputStream());
            final double sx = anchor.getWidth() / img.getWidth();
            final double sy = anchor.getHeight() / img.getHeight();
            final double tx = anchor.getX();
            final double ty = anchor.getY();
            final AffineTransform at = new AffineTransform(sx, 0.0, 0.0, sy, tx, ty);
            graphics.drawRenderedImage(img, at);
            return true;
        }
        catch (Exception e) {
            return false;
        }
    }
    
    public BufferedImage readImage(final PackagePart packagePart) throws IOException {
        return ImageIO.read(packagePart.getInputStream());
    }
}
