// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.xslf.usermodel;

import org.openxmlformats.schemas.drawingml.x2006.main.CTSchemeColor;
import org.apache.poi.util.Units;
import java.awt.geom.Rectangle2D;
import java.awt.Color;
import java.awt.Shape;
import java.awt.Graphics2D;
import org.apache.xmlbeans.XmlObject;
import org.openxmlformats.schemas.drawingml.x2006.main.CTOuterShadowEffect;

public class XSLFShadow extends XSLFSimpleShape
{
    private XSLFSimpleShape _parent;
    
    XSLFShadow(final CTOuterShadowEffect shape, final XSLFSimpleShape parentShape) {
        super(shape, parentShape.getSheet());
        this._parent = parentShape;
    }
    
    public void fill(final Graphics2D graphics, final Shape outline) {
        double shapeRotation = this._parent.getRotation();
        if (this._parent.getFlipVertical()) {
            shapeRotation += 180.0;
        }
        final double angle = this.getAngle() - shapeRotation;
        final double dist = this.getDistance();
        final double dx = dist * Math.cos(Math.toRadians(angle));
        final double dy = dist * Math.sin(Math.toRadians(angle));
        graphics.translate(dx, dy);
        final Color fillColor = this.getFillColor();
        if (fillColor != null) {
            graphics.setColor(fillColor);
            graphics.fill(outline);
        }
        graphics.translate(-dx, -dy);
    }
    
    public void draw(final Graphics2D graphics, final Shape outline) {
        final double angle = this.getAngle();
        final double dist = this.getDistance();
        final double dx = dist * Math.cos(Math.toRadians(angle));
        final double dy = dist * Math.sin(Math.toRadians(angle));
        graphics.translate(dx, dy);
        final Color fillColor = this.getFillColor();
        if (fillColor != null) {
            graphics.setColor(fillColor);
            graphics.draw(outline);
        }
        graphics.translate(-dx, -dy);
    }
    
    @Override
    public Rectangle2D getAnchor() {
        return this._parent.getAnchor();
    }
    
    @Override
    public void setAnchor(final Rectangle2D anchor) {
        throw new IllegalStateException("You can't set anchor of a shadow");
    }
    
    public double getDistance() {
        final CTOuterShadowEffect ct = (CTOuterShadowEffect)this.getXmlObject();
        return ct.isSetDist() ? Units.toPoints(ct.getDist()) : 0.0;
    }
    
    public double getAngle() {
        final CTOuterShadowEffect ct = (CTOuterShadowEffect)this.getXmlObject();
        return ct.isSetDir() ? (ct.getDir() / 60000.0) : 0.0;
    }
    
    public double getBlur() {
        final CTOuterShadowEffect ct = (CTOuterShadowEffect)this.getXmlObject();
        return ct.isSetBlurRad() ? Units.toPoints(ct.getBlurRad()) : 0.0;
    }
    
    @Override
    public Color getFillColor() {
        final XSLFTheme theme = this.getSheet().getTheme();
        final CTOuterShadowEffect ct = (CTOuterShadowEffect)this.getXmlObject();
        if (ct == null) {
            return null;
        }
        final CTSchemeColor phClr = ct.getSchemeClr();
        return new XSLFColor(ct, theme, phClr).getColor();
    }
}
