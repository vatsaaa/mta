// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.xslf.usermodel;

public enum LineDash
{
    SOLID, 
    DOT, 
    DASH, 
    LG_DASH, 
    DASH_DOT, 
    LG_DASH_DOT, 
    LG_DASH_DOT_DOT, 
    SYS_DASH, 
    SYS_DOT, 
    SYS_DASH_DOT, 
    SYS_DASH_DOT_DOT;
}
