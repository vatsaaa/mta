// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.xslf.usermodel;

import org.openxmlformats.schemas.presentationml.x2006.main.CTComment;
import org.apache.xmlbeans.XmlException;
import java.io.IOException;
import org.apache.poi.openxml4j.opc.PackageRelationship;
import org.apache.poi.openxml4j.opc.PackagePart;
import org.openxmlformats.schemas.presentationml.x2006.main.CmLstDocument;
import org.openxmlformats.schemas.presentationml.x2006.main.CTCommentList;
import org.apache.poi.POIXMLDocumentPart;

public class XSLFComments extends POIXMLDocumentPart
{
    private final CTCommentList _comments;
    
    XSLFComments() {
        final CmLstDocument doc = CmLstDocument.Factory.newInstance();
        this._comments = doc.addNewCmLst();
    }
    
    XSLFComments(final PackagePart part, final PackageRelationship rel) throws IOException, XmlException {
        super(part, rel);
        final CmLstDocument doc = CmLstDocument.Factory.parse(this.getPackagePart().getInputStream());
        this._comments = doc.getCmLst();
    }
    
    public CTCommentList getCTCommentsList() {
        return this._comments;
    }
    
    public int getNumberOfComments() {
        return this._comments.sizeOfCmArray();
    }
    
    public CTComment getCommentAt(final int pos) {
        return this._comments.getCmList().get(pos);
    }
}
