// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.xslf.usermodel;

import org.openxmlformats.schemas.presentationml.x2006.main.CTApplicationNonVisualDrawingProps;
import java.awt.RenderingHints;
import java.awt.Graphics2D;
import org.apache.poi.openxml4j.opc.PackageRelationship;
import org.apache.poi.openxml4j.opc.PackagePart;
import org.apache.poi.POIXMLException;
import java.awt.image.BufferedImage;
import java.awt.Rectangle;
import java.awt.geom.Rectangle2D;
import java.io.InputStream;
import javax.imageio.ImageIO;
import java.io.ByteArrayInputStream;
import org.openxmlformats.schemas.drawingml.x2006.main.CTPresetGeometry2D;
import org.openxmlformats.schemas.drawingml.x2006.main.CTShapeProperties;
import org.openxmlformats.schemas.drawingml.x2006.main.CTBlip;
import org.openxmlformats.schemas.drawingml.x2006.main.CTBlipFillProperties;
import org.openxmlformats.schemas.drawingml.x2006.main.CTNonVisualDrawingProps;
import org.openxmlformats.schemas.presentationml.x2006.main.CTPictureNonVisual;
import org.openxmlformats.schemas.drawingml.x2006.main.STShapeType;
import org.apache.xmlbeans.XmlObject;
import org.openxmlformats.schemas.presentationml.x2006.main.CTPicture;

public class XSLFPictureShape extends XSLFSimpleShape
{
    private XSLFPictureData _data;
    
    XSLFPictureShape(final CTPicture shape, final XSLFSheet sheet) {
        super(shape, sheet);
    }
    
    static CTPicture prototype(final int shapeId, final String rel) {
        final CTPicture ct = CTPicture.Factory.newInstance();
        final CTPictureNonVisual nvSpPr = ct.addNewNvPicPr();
        final CTNonVisualDrawingProps cnv = nvSpPr.addNewCNvPr();
        cnv.setName("Picture " + shapeId);
        cnv.setId(shapeId + 1);
        nvSpPr.addNewCNvPicPr().addNewPicLocks().setNoChangeAspect(true);
        nvSpPr.addNewNvPr();
        final CTBlipFillProperties blipFill = ct.addNewBlipFill();
        final CTBlip blip = blipFill.addNewBlip();
        blip.setEmbed(rel);
        blipFill.addNewStretch().addNewFillRect();
        final CTShapeProperties spPr = ct.addNewSpPr();
        final CTPresetGeometry2D prst = spPr.addNewPrstGeom();
        prst.setPrst(STShapeType.RECT);
        prst.addNewAvLst();
        return ct;
    }
    
    public void resize() {
        final XSLFPictureData pict = this.getPictureData();
        try {
            final BufferedImage img = ImageIO.read(new ByteArrayInputStream(pict.getData()));
            this.setAnchor(new Rectangle2D.Double(0.0, 0.0, img.getWidth(), img.getHeight()));
        }
        catch (Exception e) {
            this.setAnchor(new Rectangle(50, 50, 200, 200));
        }
    }
    
    public XSLFPictureData getPictureData() {
        if (this._data == null) {
            final String blipId = this.getBlipId();
            final PackagePart p = this.getSheet().getPackagePart();
            final PackageRelationship rel = p.getRelationship(blipId);
            if (rel != null) {
                try {
                    final PackagePart imgPart = p.getRelatedPart(rel);
                    this._data = new XSLFPictureData(imgPart, rel);
                }
                catch (Exception e) {
                    throw new POIXMLException(e);
                }
            }
        }
        return this._data;
    }
    
    private String getBlipId() {
        final CTPicture ct = (CTPicture)this.getXmlObject();
        return ct.getBlipFill().getBlip().getEmbed();
    }
    
    @Override
    public void drawContent(final Graphics2D graphics) {
        final XSLFPictureData data = this.getPictureData();
        if (data == null) {
            return;
        }
        XSLFImageRenderer renderer = (XSLFImageRenderer)graphics.getRenderingHint(XSLFRenderingHint.IMAGE_RENDERER);
        if (renderer == null) {
            renderer = new XSLFImageRenderer();
        }
        final RenderableShape rShape = new RenderableShape(this);
        final Rectangle2D anchor = rShape.getAnchor(graphics);
        renderer.drawImage(graphics, data, anchor);
    }
    
    @Override
    void copy(final XSLFShape sh) {
        super.copy(sh);
        final XSLFPictureShape p = (XSLFPictureShape)sh;
        final String blipId = p.getBlipId();
        final String relId = this.getSheet().importBlip(blipId, p.getSheet().getPackagePart());
        final CTPicture ct = (CTPicture)this.getXmlObject();
        final CTBlip blip = ct.getBlipFill().getBlip();
        blip.setEmbed(relId);
        final CTApplicationNonVisualDrawingProps nvPr = ct.getNvPicPr().getNvPr();
        if (nvPr.isSetCustDataLst()) {
            nvPr.unsetCustDataLst();
        }
    }
}
