// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.xslf.usermodel;

import org.apache.xmlbeans.XmlObject;
import java.awt.Graphics2D;
import org.openxmlformats.schemas.drawingml.x2006.main.CTPositiveSize2D;
import org.openxmlformats.schemas.drawingml.x2006.main.CTPoint2D;
import org.openxmlformats.schemas.drawingml.x2006.main.CTTransform2D;
import org.apache.poi.util.Units;
import java.awt.geom.Rectangle2D;
import org.openxmlformats.schemas.presentationml.x2006.main.CTGraphicalObjectFrame;

public class XSLFGraphicFrame extends XSLFShape
{
    private final CTGraphicalObjectFrame _shape;
    private final XSLFSheet _sheet;
    
    XSLFGraphicFrame(final CTGraphicalObjectFrame shape, final XSLFSheet sheet) {
        this._shape = shape;
        this._sheet = sheet;
    }
    
    @Override
    public CTGraphicalObjectFrame getXmlObject() {
        return this._shape;
    }
    
    public XSLFSheet getSheet() {
        return this._sheet;
    }
    
    public int getShapeType() {
        throw new RuntimeException("NotImplemented");
    }
    
    @Override
    public int getShapeId() {
        return (int)this._shape.getNvGraphicFramePr().getCNvPr().getId();
    }
    
    @Override
    public String getShapeName() {
        return this._shape.getNvGraphicFramePr().getCNvPr().getName();
    }
    
    @Override
    public Rectangle2D getAnchor() {
        final CTTransform2D xfrm = this._shape.getXfrm();
        final CTPoint2D off = xfrm.getOff();
        final long x = off.getX();
        final long y = off.getY();
        final CTPositiveSize2D ext = xfrm.getExt();
        final long cx = ext.getCx();
        final long cy = ext.getCy();
        return new Rectangle2D.Double(Units.toPoints(x), Units.toPoints(y), Units.toPoints(cx), Units.toPoints(cy));
    }
    
    @Override
    public void setAnchor(final Rectangle2D anchor) {
        final CTTransform2D xfrm = this._shape.getXfrm();
        final CTPoint2D off = xfrm.isSetOff() ? xfrm.getOff() : xfrm.addNewOff();
        final long x = Units.toEMU(anchor.getX());
        final long y = Units.toEMU(anchor.getY());
        off.setX(x);
        off.setY(y);
        final CTPositiveSize2D ext = xfrm.isSetExt() ? xfrm.getExt() : xfrm.addNewExt();
        final long cx = Units.toEMU(anchor.getWidth());
        final long cy = Units.toEMU(anchor.getHeight());
        ext.setCx(cx);
        ext.setCy(cy);
    }
    
    static XSLFGraphicFrame create(final CTGraphicalObjectFrame shape, final XSLFSheet sheet) {
        final String uri = shape.getGraphic().getGraphicData().getUri();
        if (XSLFTable.TABLE_URI.equals(uri)) {
            return new XSLFTable(shape, sheet);
        }
        return new XSLFGraphicFrame(shape, sheet);
    }
    
    @Override
    public void setRotation(final double theta) {
        throw new IllegalArgumentException("Operation not supported");
    }
    
    @Override
    public double getRotation() {
        return 0.0;
    }
    
    @Override
    public void setFlipHorizontal(final boolean flip) {
        throw new IllegalArgumentException("Operation not supported");
    }
    
    @Override
    public void setFlipVertical(final boolean flip) {
        throw new IllegalArgumentException("Operation not supported");
    }
    
    @Override
    public boolean getFlipHorizontal() {
        return false;
    }
    
    @Override
    public boolean getFlipVertical() {
        return false;
    }
    
    @Override
    public void draw(final Graphics2D graphics) {
    }
}
