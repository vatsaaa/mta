// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.xslf.usermodel;

import org.openxmlformats.schemas.drawingml.x2006.main.CTLineProperties;
import org.openxmlformats.schemas.drawingml.x2006.main.CTPresetGeometry2D;
import org.openxmlformats.schemas.drawingml.x2006.main.CTShapeProperties;
import org.openxmlformats.schemas.drawingml.x2006.main.CTNonVisualDrawingProps;
import org.openxmlformats.schemas.presentationml.x2006.main.CTConnectorNonVisual;
import org.openxmlformats.schemas.drawingml.x2006.main.STShapeType;
import org.apache.xmlbeans.XmlObject;
import org.openxmlformats.schemas.presentationml.x2006.main.CTConnector;

public class XSLFConnectorShape extends XSLFSimpleShape
{
    XSLFConnectorShape(final CTConnector shape, final XSLFSheet sheet) {
        super(shape, sheet);
    }
    
    static CTConnector prototype(final int shapeId) {
        final CTConnector ct = CTConnector.Factory.newInstance();
        final CTConnectorNonVisual nvSpPr = ct.addNewNvCxnSpPr();
        final CTNonVisualDrawingProps cnv = nvSpPr.addNewCNvPr();
        cnv.setName("Connector " + shapeId);
        cnv.setId(shapeId + 1);
        nvSpPr.addNewCNvCxnSpPr();
        nvSpPr.addNewNvPr();
        final CTShapeProperties spPr = ct.addNewSpPr();
        final CTPresetGeometry2D prst = spPr.addNewPrstGeom();
        prst.setPrst(STShapeType.LINE);
        prst.addNewAvLst();
        final CTLineProperties ln = spPr.addNewLn();
        return ct;
    }
    
    @Override
    public XSLFShadow getShadow() {
        return null;
    }
}
