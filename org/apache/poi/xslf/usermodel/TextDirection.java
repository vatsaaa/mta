// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.xslf.usermodel;

public enum TextDirection
{
    HORIZONTAL, 
    VERTICAL, 
    VERTICAL_270, 
    STACKED;
}
