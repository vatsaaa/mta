// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.xslf.usermodel;

import org.apache.poi.util.POILogFactory;
import org.apache.xmlbeans.XmlObject;
import org.openxmlformats.schemas.drawingml.x2006.main.CTTextParagraphProperties;
import org.apache.poi.util.IOUtils;
import org.apache.poi.util.Internal;
import org.openxmlformats.schemas.presentationml.x2006.main.CTSlideSize;
import org.apache.poi.util.Units;
import java.awt.Dimension;
import org.apache.poi.openxml4j.opc.PackagePartName;
import org.openxmlformats.schemas.presentationml.x2006.main.CTSlideIdList;
import org.apache.poi.openxml4j.opc.TargetMode;
import org.apache.poi.POIXMLRelation;
import org.apache.poi.openxml4j.opc.PackageRelationship;
import java.util.Collections;
import java.util.regex.Pattern;
import java.io.OutputStream;
import org.apache.poi.openxml4j.opc.PackagePart;
import org.openxmlformats.schemas.officeDocument.x2006.relationships.STRelationshipId;
import org.apache.xmlbeans.XmlOptions;
import java.util.Iterator;
import org.openxmlformats.schemas.presentationml.x2006.main.CTSlideIdListEntry;
import java.util.ArrayList;
import org.apache.poi.POIXMLDocumentPart;
import java.util.HashMap;
import org.openxmlformats.schemas.presentationml.x2006.main.PresentationDocument;
import org.apache.xmlbeans.XmlException;
import org.apache.poi.openxml4j.exceptions.OpenXML4JException;
import org.apache.poi.xslf.XSLFSlideShow;
import java.io.IOException;
import org.apache.poi.util.PackageHelper;
import java.io.InputStream;
import org.apache.poi.POIXMLException;
import org.apache.poi.POIXMLFactory;
import org.apache.poi.openxml4j.opc.OPCPackage;
import java.util.Map;
import java.util.List;
import org.openxmlformats.schemas.presentationml.x2006.main.CTPresentation;
import org.apache.poi.util.POILogger;
import org.apache.poi.POIXMLDocument;

public class XMLSlideShow extends POIXMLDocument
{
    private static POILogger _logger;
    private CTPresentation _presentation;
    private List<XSLFSlide> _slides;
    private Map<String, XSLFSlideMaster> _masters;
    private List<XSLFPictureData> _pictures;
    private XSLFTableStyles _tableStyles;
    private XSLFNotesMaster _notesMaster;
    private XSLFCommentAuthors _commentAuthors;
    
    public XMLSlideShow() {
        this(empty());
    }
    
    public XMLSlideShow(final OPCPackage pkg) {
        super(pkg);
        try {
            if (this.getCorePart().getContentType().equals(XSLFRelation.THEME_MANAGER.getContentType())) {
                this.rebase(this.getPackage());
            }
            this.load(XSLFFactory.getInstance());
        }
        catch (Exception e) {
            throw new POIXMLException(e);
        }
    }
    
    public XMLSlideShow(final InputStream is) throws IOException {
        this(PackageHelper.open(is));
    }
    
    static final OPCPackage empty() {
        final InputStream is = XMLSlideShow.class.getResourceAsStream("empty.pptx");
        if (is == null) {
            throw new RuntimeException("Missing resource 'empty.pptx'");
        }
        try {
            return OPCPackage.open(is);
        }
        catch (Exception e) {
            throw new POIXMLException(e);
        }
    }
    
    @Deprecated
    public XSLFSlideShow _getXSLFSlideShow() throws OpenXML4JException, IOException, XmlException {
        return new XSLFSlideShow(this.getPackage());
    }
    
    @Override
    protected void onDocumentRead() throws IOException {
        try {
            final PresentationDocument doc = PresentationDocument.Factory.parse(this.getCorePart().getInputStream());
            this._presentation = doc.getPresentation();
            final Map<String, XSLFSlide> shIdMap = new HashMap<String, XSLFSlide>();
            this._masters = new HashMap<String, XSLFSlideMaster>();
            for (final POIXMLDocumentPart p : this.getRelations()) {
                if (p instanceof XSLFSlide) {
                    shIdMap.put(p.getPackageRelationship().getId(), (XSLFSlide)p);
                }
                else if (p instanceof XSLFSlideMaster) {
                    final XSLFSlideMaster master = (XSLFSlideMaster)p;
                    this._masters.put(p.getPackageRelationship().getId(), master);
                }
                else if (p instanceof XSLFTableStyles) {
                    this._tableStyles = (XSLFTableStyles)p;
                }
                else if (p instanceof XSLFNotesMaster) {
                    this._notesMaster = (XSLFNotesMaster)p;
                }
                else {
                    if (!(p instanceof XSLFCommentAuthors)) {
                        continue;
                    }
                    this._commentAuthors = (XSLFCommentAuthors)p;
                }
            }
            this._slides = new ArrayList<XSLFSlide>();
            if (this._presentation.isSetSldIdLst()) {
                final List<CTSlideIdListEntry> slideIds = this._presentation.getSldIdLst().getSldIdList();
                for (final CTSlideIdListEntry slId : slideIds) {
                    final XSLFSlide sh = shIdMap.get(slId.getId2());
                    if (sh == null) {
                        XMLSlideShow._logger.log(POILogger.WARN, "Slide with r:id " + slId.getId() + " was defined, but didn't exist in package, skipping");
                    }
                    else {
                        this._slides.add(sh);
                    }
                }
            }
        }
        catch (XmlException e) {
            throw new POIXMLException(e);
        }
    }
    
    @Override
    protected void commit() throws IOException {
        final XmlOptions xmlOptions = new XmlOptions(XMLSlideShow.DEFAULT_XML_OPTIONS);
        final Map<String, String> map = new HashMap<String, String>();
        map.put(STRelationshipId.type.getName().getNamespaceURI(), "r");
        xmlOptions.setSaveSuggestedPrefixes(map);
        final PackagePart part = this.getPackagePart();
        final OutputStream out = part.getOutputStream();
        this._presentation.save(out, xmlOptions);
        out.close();
    }
    
    @Override
    public List<PackagePart> getAllEmbedds() throws OpenXML4JException {
        return Collections.unmodifiableList((List<? extends PackagePart>)this.getPackage().getPartsByName(Pattern.compile("/ppt/embeddings/.*?")));
    }
    
    public List<XSLFPictureData> getAllPictures() {
        if (this._pictures == null) {
            final List<PackagePart> mediaParts = this.getPackage().getPartsByName(Pattern.compile("/ppt/media/.*?"));
            this._pictures = new ArrayList<XSLFPictureData>(mediaParts.size());
            for (final PackagePart part : mediaParts) {
                this._pictures.add(new XSLFPictureData(part, null));
            }
        }
        return Collections.unmodifiableList((List<? extends XSLFPictureData>)this._pictures);
    }
    
    public XSLFSlide createSlide(final XSLFSlideLayout layout) {
        int slideNumber = 256;
        int cnt = 1;
        CTSlideIdList slideList;
        if (!this._presentation.isSetSldIdLst()) {
            slideList = this._presentation.addNewSldIdLst();
        }
        else {
            slideList = this._presentation.getSldIdLst();
            for (final CTSlideIdListEntry slideId : slideList.getSldIdList()) {
                slideNumber = (int)Math.max(slideId.getId() + 1L, slideNumber);
                ++cnt;
            }
        }
        final XSLFSlide slide = (XSLFSlide)this.createRelationship(XSLFRelation.SLIDE, XSLFFactory.getInstance(), cnt);
        CTSlideIdListEntry slideId = slideList.addNewSldId();
        slideId.setId(slideNumber);
        slideId.setId2(slide.getPackageRelationship().getId());
        layout.copyLayout(slide);
        slide.addRelation(layout.getPackageRelationship().getId(), layout);
        final PackagePartName ppName = layout.getPackagePart().getPartName();
        slide.getPackagePart().addRelationship(ppName, TargetMode.INTERNAL, layout.getPackageRelationship().getRelationshipType());
        this._slides.add(slide);
        return slide;
    }
    
    public XSLFSlide createSlide() {
        final String masterId = this._presentation.getSldMasterIdLst().getSldMasterIdArray(0).getId2();
        final XSLFSlideMaster master = this._masters.get(masterId);
        final XSLFSlideLayout layout = master.getLayout(SlideLayout.BLANK);
        if (layout == null) {
            throw new IllegalArgumentException("Blank layout was not found");
        }
        return this.createSlide(layout);
    }
    
    public XSLFNotesMaster getNotesMaster() {
        return this._notesMaster;
    }
    
    public XSLFSlideMaster[] getSlideMasters() {
        return this._masters.values().toArray(new XSLFSlideMaster[this._masters.size()]);
    }
    
    public XSLFSlide[] getSlides() {
        return this._slides.toArray(new XSLFSlide[this._slides.size()]);
    }
    
    public XSLFCommentAuthors getCommentAuthors() {
        return this._commentAuthors;
    }
    
    public void setSlideOrder(final XSLFSlide slide, final int newIndex) {
        final int oldIndex = this._slides.indexOf(slide);
        if (oldIndex == -1) {
            throw new IllegalArgumentException("Slide not found");
        }
        this._slides.add(newIndex, this._slides.remove(oldIndex));
        final List<CTSlideIdListEntry> slideIds = this._presentation.getSldIdLst().getSldIdList();
        final CTSlideIdListEntry oldEntry = slideIds.get(oldIndex);
        slideIds.add(newIndex, oldEntry);
        slideIds.remove(oldEntry);
    }
    
    public XSLFSlide removeSlide(final int index) {
        final XSLFSlide slide = this._slides.remove(index);
        this.removeRelation(slide);
        this._presentation.getSldIdLst().getSldIdList().remove(index);
        return slide;
    }
    
    public Dimension getPageSize() {
        final CTSlideSize sz = this._presentation.getSldSz();
        final int cx = sz.getCx();
        final int cy = sz.getCy();
        return new Dimension((int)Units.toPoints(cx), (int)Units.toPoints(cy));
    }
    
    public void setPageSize(final Dimension pgSize) {
        final CTSlideSize sz = CTSlideSize.Factory.newInstance();
        sz.setCx(Units.toEMU(pgSize.getWidth()));
        sz.setCy(Units.toEMU(pgSize.getHeight()));
        this._presentation.setSldSz(sz);
    }
    
    @Internal
    public CTPresentation getCTPresentation() {
        return this._presentation;
    }
    
    public int addPicture(final byte[] pictureData, final int format) {
        XSLFPictureData img = this.findPictureData(pictureData);
        final POIXMLRelation relDesc = XSLFPictureData.RELATIONS[format];
        if (img == null) {
            final int imageNumber = this._pictures.size();
            img = (XSLFPictureData)this.createRelationship(XSLFPictureData.RELATIONS[format], XSLFFactory.getInstance(), imageNumber + 1, true);
            this._pictures.add(img);
            try {
                final OutputStream out = img.getPackagePart().getOutputStream();
                out.write(pictureData);
                out.close();
            }
            catch (IOException e) {
                throw new POIXMLException(e);
            }
            return this._pictures.size() - 1;
        }
        return this._pictures.indexOf(img);
    }
    
    XSLFPictureData findPictureData(final byte[] pictureData) {
        final long checksum = IOUtils.calculateChecksum(pictureData);
        for (final XSLFPictureData pic : this.getAllPictures()) {
            if (pic.getChecksum() == checksum) {
                return pic;
            }
        }
        return null;
    }
    
    public XSLFTableStyles getTableStyles() {
        return this._tableStyles;
    }
    
    CTTextParagraphProperties getDefaultParagraphStyle(final int level) {
        final XmlObject[] o = this._presentation.selectPath("declare namespace p='http://schemas.openxmlformats.org/presentationml/2006/main' declare namespace a='http://schemas.openxmlformats.org/drawingml/2006/main' .//p:defaultTextStyle/a:lvl" + (level + 1) + "pPr");
        if (o.length == 1) {
            return (CTTextParagraphProperties)o[0];
        }
        return null;
    }
    
    static {
        XMLSlideShow._logger = POILogFactory.getLogger(XMLSlideShow.class);
    }
}
