// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.xslf.usermodel;

import org.apache.xmlbeans.XmlObject;
import org.openxmlformats.schemas.presentationml.x2006.main.CTBackground;
import org.openxmlformats.schemas.presentationml.x2006.main.CTPlaceholder;
import org.openxmlformats.schemas.presentationml.x2006.main.CTSlideMasterTextStyles;
import org.openxmlformats.schemas.drawingml.x2006.main.CTTextListStyle;
import org.openxmlformats.schemas.drawingml.x2006.main.CTColorMapping;
import java.util.Iterator;
import org.apache.poi.POIXMLDocumentPart;
import java.util.HashMap;
import org.apache.xmlbeans.XmlException;
import java.io.IOException;
import org.openxmlformats.schemas.presentationml.x2006.main.SldMasterDocument;
import org.apache.poi.openxml4j.opc.PackageRelationship;
import org.apache.poi.openxml4j.opc.PackagePart;
import java.util.Map;
import org.openxmlformats.schemas.presentationml.x2006.main.CTSlideMaster;

public class XSLFSlideMaster extends XSLFSheet
{
    private CTSlideMaster _slide;
    private Map<String, XSLFSlideLayout> _layouts;
    private XSLFTheme _theme;
    
    XSLFSlideMaster() {
        this._slide = CTSlideMaster.Factory.newInstance();
    }
    
    protected XSLFSlideMaster(final PackagePart part, final PackageRelationship rel) throws IOException, XmlException {
        super(part, rel);
        final SldMasterDocument doc = SldMasterDocument.Factory.parse(this.getPackagePart().getInputStream());
        this._slide = doc.getSldMaster();
        this.setCommonSlideData(this._slide.getCSld());
    }
    
    @Override
    public CTSlideMaster getXmlObject() {
        return this._slide;
    }
    
    @Override
    protected String getRootElementName() {
        return "sldMaster";
    }
    
    @Override
    public XSLFSheet getMasterSheet() {
        return null;
    }
    
    private Map<String, XSLFSlideLayout> getLayouts() {
        if (this._layouts == null) {
            this._layouts = new HashMap<String, XSLFSlideLayout>();
            for (final POIXMLDocumentPart p : this.getRelations()) {
                if (p instanceof XSLFSlideLayout) {
                    final XSLFSlideLayout layout = (XSLFSlideLayout)p;
                    this._layouts.put(layout.getName().toLowerCase(), layout);
                }
            }
        }
        return this._layouts;
    }
    
    public XSLFSlideLayout[] getSlideLayouts() {
        return this.getLayouts().values().toArray(new XSLFSlideLayout[this._layouts.size()]);
    }
    
    public XSLFSlideLayout getLayout(final SlideLayout type) {
        for (final XSLFSlideLayout layout : this.getLayouts().values()) {
            if (layout.getType() == type) {
                return layout;
            }
        }
        return null;
    }
    
    public XSLFTheme getTheme() {
        if (this._theme == null) {
            for (final POIXMLDocumentPart p : this.getRelations()) {
                if (p instanceof XSLFTheme) {
                    this._theme = (XSLFTheme)p;
                    final CTColorMapping cmap = this._slide.getClrMap();
                    if (cmap != null) {
                        this._theme.initColorMap(cmap);
                        break;
                    }
                    break;
                }
            }
        }
        return this._theme;
    }
    
    protected CTTextListStyle getTextProperties(final Placeholder textType) {
        final CTSlideMasterTextStyles txStyles = this.getXmlObject().getTxStyles();
        CTTextListStyle props = null;
        switch (textType) {
            case TITLE:
            case CENTERED_TITLE:
            case SUBTITLE: {
                props = txStyles.getTitleStyle();
                break;
            }
            case BODY: {
                props = txStyles.getBodyStyle();
                break;
            }
            default: {
                props = txStyles.getOtherStyle();
                break;
            }
        }
        return props;
    }
    
    @Override
    protected boolean canDraw(final XSLFShape shape) {
        if (shape instanceof XSLFSimpleShape) {
            final XSLFSimpleShape txt = (XSLFSimpleShape)shape;
            final CTPlaceholder ph = txt.getCTPlaceholder();
            if (ph != null) {
                return false;
            }
        }
        return true;
    }
    
    @Override
    public XSLFBackground getBackground() {
        final CTBackground bg = this._slide.getCSld().getBg();
        if (bg != null) {
            return new XSLFBackground(bg, this);
        }
        return null;
    }
}
