// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.xslf.usermodel;

import org.openxmlformats.schemas.drawingml.x2006.main.CTGeomRect;
import org.openxmlformats.schemas.drawingml.x2006.main.CTShapeProperties;
import org.openxmlformats.schemas.drawingml.x2006.main.CTNonVisualDrawingProps;
import org.openxmlformats.schemas.presentationml.x2006.main.CTShapeNonVisual;
import org.apache.xmlbeans.XmlObject;
import java.util.Iterator;
import org.openxmlformats.schemas.drawingml.x2006.main.CTCustomGeometry2D;
import java.awt.Shape;
import org.openxmlformats.schemas.drawingml.x2006.main.CTPath2DClose;
import org.openxmlformats.schemas.drawingml.x2006.main.CTPath2DLineTo;
import org.openxmlformats.schemas.drawingml.x2006.main.CTPath2DMoveTo;
import org.openxmlformats.schemas.drawingml.x2006.main.CTPath2DCubicBezierTo;
import org.openxmlformats.schemas.drawingml.x2006.main.CTAdjPoint2D;
import java.awt.geom.PathIterator;
import java.awt.geom.Rectangle2D;
import java.awt.geom.AffineTransform;
import org.apache.poi.util.Units;
import org.openxmlformats.schemas.drawingml.x2006.main.CTPath2D;
import java.awt.geom.GeneralPath;
import org.openxmlformats.schemas.presentationml.x2006.main.CTShape;

public class XSLFFreeformShape extends XSLFAutoShape
{
    XSLFFreeformShape(final CTShape shape, final XSLFSheet sheet) {
        super(shape, sheet);
    }
    
    public int setPath(final GeneralPath path) {
        final CTPath2D ctPath = CTPath2D.Factory.newInstance();
        final Rectangle2D bounds = path.getBounds2D();
        final int x0 = Units.toEMU(bounds.getX());
        final int y0 = Units.toEMU(bounds.getY());
        final PathIterator it = path.getPathIterator(new AffineTransform());
        int numPoints = 0;
        ctPath.setH(Units.toEMU(bounds.getHeight()));
        ctPath.setW(Units.toEMU(bounds.getWidth()));
        while (!it.isDone()) {
            final double[] vals = new double[6];
            final int type = it.currentSegment(vals);
            switch (type) {
                case 0: {
                    final CTAdjPoint2D mv = ctPath.addNewMoveTo().addNewPt();
                    mv.setX(Units.toEMU(vals[0]) - x0);
                    mv.setY(Units.toEMU(vals[1]) - y0);
                    ++numPoints;
                    break;
                }
                case 1: {
                    final CTAdjPoint2D ln = ctPath.addNewLnTo().addNewPt();
                    ln.setX(Units.toEMU(vals[0]) - x0);
                    ln.setY(Units.toEMU(vals[1]) - y0);
                    ++numPoints;
                    break;
                }
                case 3: {
                    final CTPath2DCubicBezierTo bez = ctPath.addNewCubicBezTo();
                    final CTAdjPoint2D p1 = bez.addNewPt();
                    p1.setX(Units.toEMU(vals[0]) - x0);
                    p1.setY(Units.toEMU(vals[1]) - y0);
                    final CTAdjPoint2D p2 = bez.addNewPt();
                    p2.setX(Units.toEMU(vals[2]) - x0);
                    p2.setY(Units.toEMU(vals[3]) - y0);
                    final CTAdjPoint2D p3 = bez.addNewPt();
                    p3.setX(Units.toEMU(vals[4]) - x0);
                    p3.setY(Units.toEMU(vals[5]) - y0);
                    numPoints += 3;
                    break;
                }
                case 4: {
                    ++numPoints;
                    ctPath.addNewClose();
                    break;
                }
            }
            it.next();
        }
        this.getSpPr().getCustGeom().getPathLst().setPathArray(new CTPath2D[] { ctPath });
        this.setAnchor(bounds);
        return numPoints;
    }
    
    public GeneralPath getPath() {
        final GeneralPath path = new GeneralPath();
        final Rectangle2D bounds = this.getAnchor();
        final CTCustomGeometry2D geom = this.getSpPr().getCustGeom();
        for (final CTPath2D spPath : geom.getPathLst().getPathList()) {
            final double scaleW = bounds.getWidth() / Units.toPoints(spPath.getW());
            final double scaleH = bounds.getHeight() / Units.toPoints(spPath.getH());
            for (final XmlObject ch : spPath.selectPath("*")) {
                if (ch instanceof CTPath2DMoveTo) {
                    final CTAdjPoint2D pt = ((CTPath2DMoveTo)ch).getPt();
                    path.moveTo((float)(Units.toPoints((long)pt.getX()) * scaleW), (float)(Units.toPoints((long)pt.getY()) * scaleH));
                }
                else if (ch instanceof CTPath2DLineTo) {
                    final CTAdjPoint2D pt = ((CTPath2DLineTo)ch).getPt();
                    path.lineTo((float)Units.toPoints((long)pt.getX()), (float)Units.toPoints((long)pt.getY()));
                }
                else if (ch instanceof CTPath2DCubicBezierTo) {
                    final CTPath2DCubicBezierTo bez = (CTPath2DCubicBezierTo)ch;
                    final CTAdjPoint2D pt2 = bez.getPtArray(0);
                    final CTAdjPoint2D pt3 = bez.getPtArray(1);
                    final CTAdjPoint2D pt4 = bez.getPtArray(2);
                    path.curveTo((float)(Units.toPoints((long)pt2.getX()) * scaleW), (float)(Units.toPoints((long)pt2.getY()) * scaleH), (float)(Units.toPoints((long)pt3.getX()) * scaleW), (float)(Units.toPoints((long)pt3.getY()) * scaleH), (float)(Units.toPoints((long)pt4.getX()) * scaleW), (float)(Units.toPoints((long)pt4.getY()) * scaleH));
                }
                else if (ch instanceof CTPath2DClose) {
                    path.closePath();
                }
            }
        }
        final AffineTransform at = new AffineTransform();
        at.translate(bounds.getX(), bounds.getY());
        return new GeneralPath(at.createTransformedShape(path));
    }
    
    static CTShape prototype(final int shapeId) {
        final CTShape ct = CTShape.Factory.newInstance();
        final CTShapeNonVisual nvSpPr = ct.addNewNvSpPr();
        final CTNonVisualDrawingProps cnv = nvSpPr.addNewCNvPr();
        cnv.setName("Freeform " + shapeId);
        cnv.setId(shapeId + 1);
        nvSpPr.addNewCNvSpPr();
        nvSpPr.addNewNvPr();
        final CTShapeProperties spPr = ct.addNewSpPr();
        final CTCustomGeometry2D geom = spPr.addNewCustGeom();
        geom.addNewAvLst();
        geom.addNewGdLst();
        geom.addNewAhLst();
        geom.addNewCxnLst();
        final CTGeomRect rect = geom.addNewRect();
        rect.setR("r");
        rect.setB("b");
        rect.setT("t");
        rect.setL("l");
        geom.addNewPathLst();
        return ct;
    }
}
