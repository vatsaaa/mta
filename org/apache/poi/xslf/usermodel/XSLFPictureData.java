// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.xslf.usermodel;

import java.io.IOException;
import org.apache.poi.POIXMLException;
import org.apache.poi.util.IOUtils;
import org.apache.poi.openxml4j.opc.PackageRelationship;
import org.apache.poi.openxml4j.opc.PackagePart;
import org.apache.poi.POIXMLRelation;
import org.apache.poi.POIXMLDocumentPart;

public final class XSLFPictureData extends POIXMLDocumentPart
{
    public static final int PICTURE_TYPE_EMF = 2;
    public static final int PICTURE_TYPE_WMF = 3;
    public static final int PICTURE_TYPE_PICT = 4;
    public static final int PICTURE_TYPE_JPEG = 5;
    public static final int PICTURE_TYPE_PNG = 6;
    public static final int PICTURE_TYPE_DIB = 7;
    public static final int PICTURE_TYPE_GIF = 8;
    public static final int PICTURE_TYPE_TIFF = 9;
    public static final int PICTURE_TYPE_EPS = 10;
    public static final int PICTURE_TYPE_BMP = 11;
    public static final int PICTURE_TYPE_WPG = 12;
    protected static final POIXMLRelation[] RELATIONS;
    private Long checksum;
    
    protected XSLFPictureData() {
        this.checksum = null;
    }
    
    public XSLFPictureData(final PackagePart part, final PackageRelationship rel) {
        super(part, rel);
        this.checksum = null;
    }
    
    public byte[] getData() {
        try {
            return IOUtils.toByteArray(this.getPackagePart().getInputStream());
        }
        catch (IOException e) {
            throw new POIXMLException(e);
        }
    }
    
    public String getFileName() {
        final String name = this.getPackagePart().getPartName().getName();
        if (name == null) {
            return null;
        }
        return name.substring(name.lastIndexOf(47) + 1);
    }
    
    public String suggestFileExtension() {
        return this.getPackagePart().getPartName().getExtension();
    }
    
    public int getPictureType() {
        final String contentType = this.getPackagePart().getContentType();
        for (int i = 0; i < XSLFPictureData.RELATIONS.length; ++i) {
            if (XSLFPictureData.RELATIONS[i] != null) {
                if (XSLFPictureData.RELATIONS[i].getContentType().equals(contentType)) {
                    return i;
                }
            }
        }
        return 0;
    }
    
    long getChecksum() {
        if (this.checksum == null) {
            final byte[] pictureData = this.getData();
            this.checksum = IOUtils.calculateChecksum(pictureData);
        }
        return this.checksum;
    }
    
    static {
        (RELATIONS = new POIXMLRelation[13])[2] = XSLFRelation.IMAGE_EMF;
        XSLFPictureData.RELATIONS[3] = XSLFRelation.IMAGE_WMF;
        XSLFPictureData.RELATIONS[4] = XSLFRelation.IMAGE_PICT;
        XSLFPictureData.RELATIONS[5] = XSLFRelation.IMAGE_JPEG;
        XSLFPictureData.RELATIONS[6] = XSLFRelation.IMAGE_PNG;
        XSLFPictureData.RELATIONS[7] = XSLFRelation.IMAGE_DIB;
        XSLFPictureData.RELATIONS[8] = XSLFRelation.IMAGE_GIF;
        XSLFPictureData.RELATIONS[9] = XSLFRelation.IMAGE_TIFF;
        XSLFPictureData.RELATIONS[10] = XSLFRelation.IMAGE_EPS;
        XSLFPictureData.RELATIONS[11] = XSLFRelation.IMAGE_BMP;
        XSLFPictureData.RELATIONS[12] = XSLFRelation.IMAGE_WPG;
    }
}
