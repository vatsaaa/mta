// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.xslf.usermodel;

import org.openxmlformats.schemas.drawingml.x2006.main.CTRegularTextRun;
import org.openxmlformats.schemas.drawingml.x2006.main.CTTextCharacterProperties;

class XSLFLineBreak extends XSLFTextRun
{
    private final CTTextCharacterProperties _brProps;
    
    XSLFLineBreak(final CTRegularTextRun r, final XSLFTextParagraph p, final CTTextCharacterProperties brProps) {
        super(r, p);
        this._brProps = brProps;
    }
    
    @Override
    protected CTTextCharacterProperties getRPr() {
        return this._brProps;
    }
    
    @Override
    public void setText(final String text) {
        throw new IllegalStateException("You cannot change text of a line break, it is always '\\n'");
    }
}
