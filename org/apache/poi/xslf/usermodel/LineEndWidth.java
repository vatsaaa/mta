// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.xslf.usermodel;

public enum LineEndWidth
{
    SMALL, 
    MEDIUM, 
    LARGE;
}
