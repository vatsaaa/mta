// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.xslf.usermodel;

import org.apache.poi.util.Internal;
import java.awt.RenderingHints;

public class XSLFRenderingHint extends RenderingHints.Key
{
    public static final XSLFRenderingHint GSAVE;
    public static final XSLFRenderingHint GRESTORE;
    public static final XSLFRenderingHint IMAGE_RENDERER;
    public static final XSLFRenderingHint TEXT_RENDERING_MODE;
    public static final int TEXT_AS_CHARACTERS = 1;
    public static final int TEXT_AS_SHAPES = 2;
    @Internal
    static final XSLFRenderingHint GROUP_TRANSFORM;
    public static final XSLFRenderingHint FONT_HANDLER;
    
    public XSLFRenderingHint(final int i) {
        super(i);
    }
    
    @Override
    public boolean isCompatibleValue(final Object val) {
        return true;
    }
    
    static {
        GSAVE = new XSLFRenderingHint(1);
        GRESTORE = new XSLFRenderingHint(2);
        IMAGE_RENDERER = new XSLFRenderingHint(3);
        TEXT_RENDERING_MODE = new XSLFRenderingHint(4);
        GROUP_TRANSFORM = new XSLFRenderingHint(5);
        FONT_HANDLER = new XSLFRenderingHint(6);
    }
}
