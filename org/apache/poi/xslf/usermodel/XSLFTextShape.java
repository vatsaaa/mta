// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.xslf.usermodel;

import org.apache.poi.POIXMLException;
import java.awt.geom.Rectangle2D;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import org.openxmlformats.schemas.presentationml.x2006.main.CTApplicationNonVisualDrawingProps;
import org.openxmlformats.schemas.presentationml.x2006.main.STPlaceholderType;
import org.openxmlformats.schemas.presentationml.x2006.main.CTShape;
import org.openxmlformats.schemas.presentationml.x2006.main.CTPlaceholder;
import org.openxmlformats.schemas.drawingml.x2006.main.STTextWrappingType;
import org.apache.poi.util.Units;
import org.openxmlformats.schemas.drawingml.x2006.main.STTextVerticalType;
import org.apache.poi.xslf.model.PropertyFetcher;
import org.apache.poi.xslf.model.TextBodyPropertyFetcher;
import org.openxmlformats.schemas.drawingml.x2006.main.CTTextBodyProperties;
import org.openxmlformats.schemas.drawingml.x2006.main.STTextAnchoringType;
import java.util.Iterator;
import org.openxmlformats.schemas.drawingml.x2006.main.CTTextBody;
import org.openxmlformats.schemas.drawingml.x2006.main.CTTextParagraph;
import java.util.ArrayList;
import org.apache.xmlbeans.XmlObject;
import java.util.List;

public abstract class XSLFTextShape extends XSLFSimpleShape implements Iterable<XSLFTextParagraph>
{
    private final List<XSLFTextParagraph> _paragraphs;
    private boolean _isTextBroken;
    
    XSLFTextShape(final XmlObject shape, final XSLFSheet sheet) {
        super(shape, sheet);
        this._paragraphs = new ArrayList<XSLFTextParagraph>();
        final CTTextBody txBody = this.getTextBody(false);
        if (txBody != null) {
            for (final CTTextParagraph p : txBody.getPList()) {
                this._paragraphs.add(new XSLFTextParagraph(p, this));
            }
        }
    }
    
    public Iterator<XSLFTextParagraph> iterator() {
        return this._paragraphs.iterator();
    }
    
    public String getText() {
        final StringBuilder out = new StringBuilder();
        for (final XSLFTextParagraph p : this._paragraphs) {
            if (out.length() > 0) {
                out.append('\n');
            }
            out.append(p.getText());
        }
        return out.toString();
    }
    
    public void clearText() {
        this._paragraphs.clear();
        final CTTextBody txBody = this.getTextBody(true);
        txBody.setPArray(null);
    }
    
    public void setText(final String text) {
        this.clearText();
        this.addNewTextParagraph().addNewTextRun().setText(text);
    }
    
    public List<XSLFTextParagraph> getTextParagraphs() {
        return this._paragraphs;
    }
    
    public XSLFTextParagraph addNewTextParagraph() {
        final CTTextBody txBody = this.getTextBody(true);
        final CTTextParagraph p = txBody.addNewP();
        final XSLFTextParagraph paragraph = new XSLFTextParagraph(p, this);
        this._paragraphs.add(paragraph);
        return paragraph;
    }
    
    public void setVerticalAlignment(final VerticalAlignment anchor) {
        final CTTextBodyProperties bodyPr = this.getTextBodyPr();
        if (bodyPr != null) {
            if (anchor == null) {
                if (bodyPr.isSetAnchor()) {
                    bodyPr.unsetAnchor();
                }
            }
            else {
                bodyPr.setAnchor(STTextAnchoringType.Enum.forInt(anchor.ordinal() + 1));
            }
        }
    }
    
    public VerticalAlignment getVerticalAlignment() {
        final PropertyFetcher<VerticalAlignment> fetcher = new TextBodyPropertyFetcher<VerticalAlignment>() {
            @Override
            public boolean fetch(final CTTextBodyProperties props) {
                if (props.isSetAnchor()) {
                    final int val = props.getAnchor().intValue();
                    this.setValue(VerticalAlignment.values()[val - 1]);
                    return true;
                }
                return false;
            }
        };
        this.fetchShapeProperty(fetcher);
        return (fetcher.getValue() == null) ? VerticalAlignment.TOP : fetcher.getValue();
    }
    
    public void setTextDirection(final TextDirection orientation) {
        final CTTextBodyProperties bodyPr = this.getTextBodyPr();
        if (bodyPr != null) {
            if (orientation == null) {
                if (bodyPr.isSetVert()) {
                    bodyPr.unsetVert();
                }
            }
            else {
                bodyPr.setVert(STTextVerticalType.Enum.forInt(orientation.ordinal() + 1));
            }
        }
    }
    
    public TextDirection getTextDirection() {
        final CTTextBodyProperties bodyPr = this.getTextBodyPr();
        if (bodyPr != null) {
            final STTextVerticalType.Enum val = bodyPr.getVert();
            if (val != null) {
                return TextDirection.values()[val.intValue() - 1];
            }
        }
        return TextDirection.HORIZONTAL;
    }
    
    public double getBottomInset() {
        final PropertyFetcher<Double> fetcher = new TextBodyPropertyFetcher<Double>() {
            @Override
            public boolean fetch(final CTTextBodyProperties props) {
                if (props.isSetBIns()) {
                    final double val = Units.toPoints(props.getBIns());
                    this.setValue(val);
                    return true;
                }
                return false;
            }
        };
        this.fetchShapeProperty(fetcher);
        return (fetcher.getValue() == null) ? 3.6 : fetcher.getValue();
    }
    
    public double getLeftInset() {
        final PropertyFetcher<Double> fetcher = new TextBodyPropertyFetcher<Double>() {
            @Override
            public boolean fetch(final CTTextBodyProperties props) {
                if (props.isSetLIns()) {
                    final double val = Units.toPoints(props.getLIns());
                    this.setValue(val);
                    return true;
                }
                return false;
            }
        };
        this.fetchShapeProperty(fetcher);
        return (fetcher.getValue() == null) ? 7.2 : fetcher.getValue();
    }
    
    public double getRightInset() {
        final PropertyFetcher<Double> fetcher = new TextBodyPropertyFetcher<Double>() {
            @Override
            public boolean fetch(final CTTextBodyProperties props) {
                if (props.isSetRIns()) {
                    final double val = Units.toPoints(props.getRIns());
                    this.setValue(val);
                    return true;
                }
                return false;
            }
        };
        this.fetchShapeProperty(fetcher);
        return (fetcher.getValue() == null) ? 7.2 : fetcher.getValue();
    }
    
    public double getTopInset() {
        final PropertyFetcher<Double> fetcher = new TextBodyPropertyFetcher<Double>() {
            @Override
            public boolean fetch(final CTTextBodyProperties props) {
                if (props.isSetTIns()) {
                    final double val = Units.toPoints(props.getTIns());
                    this.setValue(val);
                    return true;
                }
                return false;
            }
        };
        this.fetchShapeProperty(fetcher);
        return (fetcher.getValue() == null) ? 3.6 : fetcher.getValue();
    }
    
    public void setBottomInset(final double margin) {
        final CTTextBodyProperties bodyPr = this.getTextBodyPr();
        if (bodyPr != null) {
            if (margin == -1.0) {
                bodyPr.unsetBIns();
            }
            else {
                bodyPr.setBIns(Units.toEMU(margin));
            }
        }
    }
    
    public void setLeftInset(final double margin) {
        final CTTextBodyProperties bodyPr = this.getTextBodyPr();
        if (bodyPr != null) {
            if (margin == -1.0) {
                bodyPr.unsetLIns();
            }
            else {
                bodyPr.setLIns(Units.toEMU(margin));
            }
        }
    }
    
    public void setRightInset(final double margin) {
        final CTTextBodyProperties bodyPr = this.getTextBodyPr();
        if (bodyPr != null) {
            if (margin == -1.0) {
                bodyPr.unsetRIns();
            }
            else {
                bodyPr.setRIns(Units.toEMU(margin));
            }
        }
    }
    
    public void setTopInset(final double margin) {
        final CTTextBodyProperties bodyPr = this.getTextBodyPr();
        if (bodyPr != null) {
            if (margin == -1.0) {
                bodyPr.unsetTIns();
            }
            else {
                bodyPr.setTIns(Units.toEMU(margin));
            }
        }
    }
    
    public boolean getWordWrap() {
        final PropertyFetcher<Boolean> fetcher = new TextBodyPropertyFetcher<Boolean>() {
            @Override
            public boolean fetch(final CTTextBodyProperties props) {
                if (props.isSetWrap()) {
                    this.setValue(props.getWrap() == STTextWrappingType.SQUARE);
                    return true;
                }
                return false;
            }
        };
        this.fetchShapeProperty(fetcher);
        return fetcher.getValue() == null || fetcher.getValue();
    }
    
    public void setWordWrap(final boolean wrap) {
        final CTTextBodyProperties bodyPr = this.getTextBodyPr();
        if (bodyPr != null) {
            bodyPr.setWrap(wrap ? STTextWrappingType.SQUARE : STTextWrappingType.NONE);
        }
    }
    
    public void setTextAutofit(final TextAutofit value) {
        final CTTextBodyProperties bodyPr = this.getTextBodyPr();
        if (bodyPr != null) {
            if (bodyPr.isSetSpAutoFit()) {
                bodyPr.unsetSpAutoFit();
            }
            if (bodyPr.isSetNoAutofit()) {
                bodyPr.unsetNoAutofit();
            }
            if (bodyPr.isSetNormAutofit()) {
                bodyPr.unsetNormAutofit();
            }
            switch (value) {
                case NONE: {
                    bodyPr.addNewNoAutofit();
                    break;
                }
                case NORMAL: {
                    bodyPr.addNewNormAutofit();
                    break;
                }
                case SHAPE: {
                    bodyPr.addNewSpAutoFit();
                    break;
                }
            }
        }
    }
    
    public TextAutofit getTextAutofit() {
        final CTTextBodyProperties bodyPr = this.getTextBodyPr();
        if (bodyPr != null) {
            if (bodyPr.isSetNoAutofit()) {
                return TextAutofit.NONE;
            }
            if (bodyPr.isSetNormAutofit()) {
                return TextAutofit.NORMAL;
            }
            if (bodyPr.isSetSpAutoFit()) {
                return TextAutofit.SHAPE;
            }
        }
        return TextAutofit.NORMAL;
    }
    
    protected CTTextBodyProperties getTextBodyPr() {
        final CTTextBody textBody = this.getTextBody(false);
        return (textBody == null) ? null : textBody.getBodyPr();
    }
    
    protected abstract CTTextBody getTextBody(final boolean p0);
    
    public Placeholder getTextType() {
        final XmlObject[] obj = this.getXmlObject().selectPath("declare namespace p='http://schemas.openxmlformats.org/presentationml/2006/main' .//*/p:nvPr/p:ph");
        if (obj.length == 1) {
            final CTPlaceholder ph = (CTPlaceholder)obj[0];
            final int val = ph.getType().intValue();
            return Placeholder.values()[val - 1];
        }
        return null;
    }
    
    public void setPlaceholder(final Placeholder placeholder) {
        final CTShape sh = (CTShape)this.getXmlObject();
        final CTApplicationNonVisualDrawingProps nv = sh.getNvSpPr().getNvPr();
        if (placeholder == null) {
            if (nv.isSetPh()) {
                nv.unsetPh();
            }
        }
        else {
            nv.addNewPh().setType(STPlaceholderType.Enum.forInt(placeholder.ordinal() + 1));
        }
    }
    
    public double getTextHeight() {
        final BufferedImage img = new BufferedImage(1, 1, 1);
        final Graphics2D graphics = img.createGraphics();
        this.breakText(graphics);
        return this.drawParagraphs(graphics, 0.0, 0.0);
    }
    
    public Rectangle2D resizeToFitText() {
        final Rectangle2D anchor = this.getAnchor();
        if (anchor.getWidth() == 0.0) {
            throw new POIXMLException("Anchor of the shape was not set.");
        }
        double height = this.getTextHeight();
        ++height;
        anchor.setRect(anchor.getX(), anchor.getY(), anchor.getWidth(), height);
        this.setAnchor(anchor);
        return anchor;
    }
    
    private void breakText(final Graphics2D graphics) {
        if (!this._isTextBroken) {
            for (final XSLFTextParagraph p : this._paragraphs) {
                p.breakText(graphics);
            }
            this._isTextBroken = true;
        }
    }
    
    @Override
    public void drawContent(final Graphics2D graphics) {
        this.breakText(graphics);
        final RenderableShape rShape = new RenderableShape(this);
        final Rectangle2D anchor = rShape.getAnchor(graphics);
        final double x = anchor.getX() + this.getLeftInset();
        double y = anchor.getY();
        final double textHeight = this.getTextHeight();
        switch (this.getVerticalAlignment()) {
            case TOP: {
                y += this.getTopInset();
                break;
            }
            case BOTTOM: {
                y += anchor.getHeight() - textHeight - this.getBottomInset();
                break;
            }
            default: {
                final double delta = anchor.getHeight() - textHeight - this.getTopInset() - this.getBottomInset();
                y += this.getTopInset() + delta / 2.0;
                break;
            }
        }
        this.drawParagraphs(graphics, x, y);
    }
    
    private double drawParagraphs(final Graphics2D graphics, final double x, double y) {
        final double y2 = y;
        for (int i = 0; i < this._paragraphs.size(); ++i) {
            final XSLFTextParagraph p = this._paragraphs.get(i);
            final List<TextFragment> lines = p.getTextLines();
            if (i > 0 && lines.size() > 0) {
                final double spaceBefore = p.getSpaceBefore();
                if (spaceBefore > 0.0) {
                    y += spaceBefore * 0.01 * lines.get(0).getHeight();
                }
                else {
                    y += -spaceBefore;
                }
            }
            y += p.draw(graphics, x, y);
            if (i < this._paragraphs.size() - 1) {
                final double spaceAfter = p.getSpaceAfter();
                if (spaceAfter > 0.0) {
                    y += spaceAfter * 0.01 * lines.get(lines.size() - 1).getHeight();
                }
                else {
                    y += -spaceAfter;
                }
            }
        }
        return y - y2;
    }
    
    @Override
    void copy(final XSLFShape sh) {
        super.copy(sh);
        final XSLFTextShape tsh = (XSLFTextShape)sh;
        final boolean srcWordWrap = tsh.getWordWrap();
        if (srcWordWrap != this.getWordWrap()) {
            this.setWordWrap(srcWordWrap);
        }
        final double leftInset = tsh.getLeftInset();
        if (leftInset != this.getLeftInset()) {
            this.setLeftInset(leftInset);
        }
        final double rightInset = tsh.getRightInset();
        if (rightInset != this.getRightInset()) {
            this.setRightInset(rightInset);
        }
        final double topInset = tsh.getTopInset();
        if (topInset != this.getTopInset()) {
            this.setTopInset(topInset);
        }
        final double bottomInset = tsh.getBottomInset();
        if (bottomInset != this.getBottomInset()) {
            this.setBottomInset(bottomInset);
        }
        final VerticalAlignment vAlign = tsh.getVerticalAlignment();
        if (vAlign != this.getVerticalAlignment()) {
            this.setVerticalAlignment(vAlign);
        }
        final List<XSLFTextParagraph> srcP = tsh.getTextParagraphs();
        final List<XSLFTextParagraph> tgtP = this.getTextParagraphs();
        for (int i = 0; i < srcP.size(); ++i) {
            final XSLFTextParagraph p1 = srcP.get(i);
            final XSLFTextParagraph p2 = tgtP.get(i);
            p2.copy(p1);
        }
    }
}
