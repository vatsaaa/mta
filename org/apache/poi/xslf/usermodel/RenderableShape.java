// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.xslf.usermodel;

import java.awt.Shape;
import org.apache.poi.xslf.model.geom.CustomGeometry;
import org.apache.poi.xslf.model.geom.Context;
import org.openxmlformats.schemas.drawingml.x2006.main.CTPresetGeometry2D;
import org.openxmlformats.schemas.drawingml.x2006.main.CTGeomGuide;
import org.apache.poi.xslf.model.geom.Guide;
import org.apache.poi.xslf.model.geom.IAdjustableShape;
import org.apache.poi.xslf.model.geom.Path;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Collection;
import org.apache.poi.xslf.model.geom.Outline;
import java.awt.BasicStroke;
import java.awt.Stroke;
import org.openxmlformats.schemas.drawingml.x2006.main.CTLineProperties;
import org.openxmlformats.schemas.drawingml.x2006.main.CTStyleMatrixReference;
import org.openxmlformats.schemas.drawingml.x2006.main.CTShapeStyle;
import org.openxmlformats.schemas.drawingml.x2006.main.CTShapeProperties;
import org.apache.poi.xslf.model.PropertyFetcher;
import java.lang.reflect.Constructor;
import java.awt.GradientPaint;
import java.awt.geom.Point2D;
import java.awt.geom.AffineTransform;
import java.util.Arrays;
import org.openxmlformats.schemas.drawingml.x2006.main.CTGradientStop;
import java.util.Comparator;
import java.awt.image.BufferedImage;
import org.apache.poi.openxml4j.opc.PackageRelationship;
import org.openxmlformats.schemas.drawingml.x2006.main.CTBlip;
import java.awt.TexturePaint;
import java.awt.Composite;
import java.awt.AlphaComposite;
import java.awt.RenderingHints;
import org.openxmlformats.schemas.drawingml.x2006.main.CTPathShadeProperties;
import java.awt.geom.Rectangle2D;
import org.openxmlformats.schemas.drawingml.x2006.main.STPathShadeType;
import org.openxmlformats.schemas.drawingml.x2006.main.CTGradientFillProperties;
import org.openxmlformats.schemas.drawingml.x2006.main.CTBlipFillProperties;
import org.openxmlformats.schemas.drawingml.x2006.main.CTSolidColorFillProperties;
import org.openxmlformats.schemas.drawingml.x2006.main.CTNoFillProperties;
import java.awt.Paint;
import org.apache.poi.openxml4j.opc.PackagePart;
import org.openxmlformats.schemas.drawingml.x2006.main.CTSchemeColor;
import org.apache.xmlbeans.XmlObject;
import java.awt.Graphics2D;
import java.awt.Color;
import org.apache.poi.util.Internal;

@Internal
class RenderableShape
{
    public static final Color NO_PAINT;
    private XSLFSimpleShape _shape;
    
    public RenderableShape(final XSLFSimpleShape shape) {
        this._shape = shape;
    }
    
    public Paint selectPaint(final Graphics2D graphics, final XmlObject obj, final CTSchemeColor phClr, final PackagePart parentPart) {
        final XSLFTheme theme = this._shape.getSheet().getTheme();
        Paint paint = null;
        if (obj instanceof CTNoFillProperties) {
            paint = RenderableShape.NO_PAINT;
        }
        else if (obj instanceof CTSolidColorFillProperties) {
            final CTSolidColorFillProperties solidFill = (CTSolidColorFillProperties)obj;
            final XSLFColor c = new XSLFColor(solidFill, theme, phClr);
            paint = c.getColor();
        }
        else if (obj instanceof CTBlipFillProperties) {
            final CTBlipFillProperties blipFill = (CTBlipFillProperties)obj;
            paint = this.createTexturePaint(blipFill, graphics, parentPart);
        }
        else if (obj instanceof CTGradientFillProperties) {
            final Rectangle2D anchor = this.getAnchor(graphics);
            final CTGradientFillProperties gradFill = (CTGradientFillProperties)obj;
            if (gradFill.isSetLin()) {
                paint = this.createLinearGradientPaint(graphics, gradFill, anchor, theme, phClr);
            }
            else if (gradFill.isSetPath()) {
                final CTPathShadeProperties ps = gradFill.getPath();
                if (ps.getPath() == STPathShadeType.CIRCLE) {
                    paint = createRadialGradientPaint(gradFill, anchor, theme, phClr);
                }
                else if (ps.getPath() == STPathShadeType.SHAPE) {
                    paint = toRadialGradientPaint(gradFill, anchor, theme, phClr);
                }
            }
        }
        return paint;
    }
    
    private Paint createTexturePaint(final CTBlipFillProperties blipFill, final Graphics2D graphics, final PackagePart parentPart) {
        Paint paint = null;
        final CTBlip blip = blipFill.getBlip();
        final String blipId = blip.getEmbed();
        final PackageRelationship rel = parentPart.getRelationship(blipId);
        if (rel != null) {
            XSLFImageRenderer renderer = null;
            if (graphics != null) {
                renderer = (XSLFImageRenderer)graphics.getRenderingHint(XSLFRenderingHint.IMAGE_RENDERER);
            }
            if (renderer == null) {
                renderer = new XSLFImageRenderer();
            }
            try {
                final BufferedImage img = renderer.readImage(parentPart.getRelatedPart(rel));
                if (blip.sizeOfAlphaModFixArray() > 0) {
                    final float alpha = blip.getAlphaModFixArray(0).getAmt() / 100000.0f;
                    final AlphaComposite ac = AlphaComposite.getInstance(3, alpha);
                    if (graphics != null) {
                        graphics.setComposite(ac);
                    }
                }
                if (img != null) {
                    paint = new TexturePaint(img, new Rectangle2D.Double(0.0, 0.0, img.getWidth(), img.getHeight()));
                }
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
        return paint;
    }
    
    private Paint createLinearGradientPaint(final Graphics2D graphics, final CTGradientFillProperties gradFill, final Rectangle2D anchor, final XSLFTheme theme, final CTSchemeColor phClr) {
        final double angle = gradFill.getLin().getAng() / 60000;
        final CTGradientStop[] gs = gradFill.getGsLst().getGsArray();
        Arrays.sort(gs, new Comparator<CTGradientStop>() {
            public int compare(final CTGradientStop o1, final CTGradientStop o2) {
                final Integer pos1 = o1.getPos();
                final Integer pos2 = o2.getPos();
                return pos1.compareTo(pos2);
            }
        });
        final Color[] colors = new Color[gs.length];
        final float[] fractions = new float[gs.length];
        final AffineTransform at = AffineTransform.getRotateInstance(Math.toRadians(angle), anchor.getX() + anchor.getWidth() / 2.0, anchor.getY() + anchor.getHeight() / 2.0);
        final double diagonal = Math.sqrt(anchor.getHeight() * anchor.getHeight() + anchor.getWidth() * anchor.getWidth());
        Point2D p1 = new Point2D.Double(anchor.getX() + anchor.getWidth() / 2.0 - diagonal / 2.0, anchor.getY() + anchor.getHeight() / 2.0);
        p1 = at.transform(p1, null);
        Point2D p2 = new Point2D.Double(anchor.getX() + anchor.getWidth(), anchor.getY() + anchor.getHeight() / 2.0);
        p2 = at.transform(p2, null);
        snapToAnchor(p1, anchor);
        snapToAnchor(p2, anchor);
        for (int i = 0; i < gs.length; ++i) {
            final CTGradientStop stop = gs[i];
            colors[i] = new XSLFColor(stop, theme, phClr).getColor();
            fractions[i] = stop.getPos() / 100000.0f;
        }
        final AffineTransform grAt = new AffineTransform();
        if (gradFill.isSetRotWithShape() || !gradFill.getRotWithShape()) {
            final double rotation = this._shape.getRotation();
            if (rotation != 0.0) {
                final double centerX = anchor.getX() + anchor.getWidth() / 2.0;
                final double centerY = anchor.getY() + anchor.getHeight() / 2.0;
                grAt.translate(centerX, centerY);
                grAt.rotate(Math.toRadians(-rotation));
                grAt.translate(-centerX, -centerY);
            }
        }
        Paint paint;
        try {
            final Class clz = Class.forName("java.awt.LinearGradientPaint");
            final Class clzCycleMethod = Class.forName("java.awt.MultipleGradientPaint$CycleMethod");
            final Class clzColorSpaceType = Class.forName("java.awt.MultipleGradientPaint$ColorSpaceType");
            final Constructor c = clz.getConstructor(Point2D.class, Point2D.class, float[].class, Color[].class, clzCycleMethod, clzColorSpaceType, AffineTransform.class);
            paint = c.newInstance(p1, p2, fractions, colors, Enum.valueOf((Class<Object>)clzCycleMethod, "NO_CYCLE"), Enum.valueOf((Class<Object>)clzColorSpaceType, "SRGB"), grAt);
        }
        catch (ClassNotFoundException e2) {
            paint = new GradientPaint(p1, colors[0], p2, colors[colors.length - 1]);
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }
        return paint;
    }
    
    private static Paint toRadialGradientPaint(final CTGradientFillProperties gradFill, final Rectangle2D anchor, final XSLFTheme theme, final CTSchemeColor phClr) {
        final CTGradientStop[] gs = gradFill.getGsLst().getGsArray();
        Arrays.sort(gs, new Comparator<CTGradientStop>() {
            public int compare(final CTGradientStop o1, final CTGradientStop o2) {
                final Integer pos1 = o1.getPos();
                final Integer pos2 = o2.getPos();
                return pos1.compareTo(pos2);
            }
        });
        gs[1].setPos(50000);
        final CTGradientFillProperties g = CTGradientFillProperties.Factory.newInstance();
        g.set(gradFill);
        g.getGsLst().setGsArray(new CTGradientStop[] { gs[0], gs[1] });
        return createRadialGradientPaint(g, anchor, theme, phClr);
    }
    
    private static Paint createRadialGradientPaint(final CTGradientFillProperties gradFill, final Rectangle2D anchor, final XSLFTheme theme, final CTSchemeColor phClr) {
        final CTGradientStop[] gs = gradFill.getGsLst().getGsArray();
        final Point2D pCenter = new Point2D.Double(anchor.getX() + anchor.getWidth() / 2.0, anchor.getY() + anchor.getHeight() / 2.0);
        final float radius = (float)Math.max(anchor.getWidth(), anchor.getHeight());
        Arrays.sort(gs, new Comparator<CTGradientStop>() {
            public int compare(final CTGradientStop o1, final CTGradientStop o2) {
                final Integer pos1 = o1.getPos();
                final Integer pos2 = o2.getPos();
                return pos1.compareTo(pos2);
            }
        });
        final Color[] colors = new Color[gs.length];
        final float[] fractions = new float[gs.length];
        for (int i = 0; i < gs.length; ++i) {
            final CTGradientStop stop = gs[i];
            colors[i] = new XSLFColor(stop, theme, phClr).getColor();
            fractions[i] = stop.getPos() / 100000.0f;
        }
        Paint paint;
        try {
            final Class clz = Class.forName("java.awt.RadialGradientPaint");
            final Constructor c = clz.getConstructor(Point2D.class, Float.TYPE, float[].class, Color[].class);
            paint = c.newInstance(pCenter, radius, fractions, colors);
        }
        catch (ClassNotFoundException e2) {
            paint = new GradientPaint(new Point2D.Double(anchor.getX(), anchor.getY()), colors[0], pCenter, colors[colors.length - 1]);
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }
        return paint;
    }
    
    private static void snapToAnchor(final Point2D p, final Rectangle2D anchor) {
        if (p.getX() < anchor.getX()) {
            p.setLocation(anchor.getX(), p.getY());
        }
        else if (p.getX() > anchor.getX() + anchor.getWidth()) {
            p.setLocation(anchor.getX() + anchor.getWidth(), p.getY());
        }
        if (p.getY() < anchor.getY()) {
            p.setLocation(p.getX(), anchor.getY());
        }
        else if (p.getY() > anchor.getY() + anchor.getHeight()) {
            p.setLocation(p.getX(), anchor.getY() + anchor.getHeight());
        }
    }
    
    Paint getPaint(final Graphics2D graphics, final XmlObject spPr, final CTSchemeColor phClr) {
        Paint paint = null;
        for (final XmlObject obj : spPr.selectPath("*")) {
            paint = this.selectPaint(graphics, obj, phClr, this._shape.getSheet().getPackagePart());
            if (paint != null) {
                break;
            }
        }
        return (paint == RenderableShape.NO_PAINT) ? null : paint;
    }
    
    Paint getFillPaint(final Graphics2D graphics) {
        final PropertyFetcher<Paint> fetcher = new PropertyFetcher<Paint>() {
            @Override
            public boolean fetch(final XSLFSimpleShape shape) {
                final CTShapeProperties spPr = shape.getSpPr();
                if (spPr.isSetNoFill()) {
                    ((PropertyFetcher<Color>)this).setValue(RenderableShape.NO_PAINT);
                    return true;
                }
                final Paint paint = RenderableShape.this.getPaint(graphics, spPr, null);
                if (paint != null) {
                    this.setValue(paint);
                    return true;
                }
                return false;
            }
        };
        this._shape.fetchShapeProperty(fetcher);
        Paint paint = fetcher.getValue();
        if (paint == null) {
            final CTShapeStyle style = this._shape.getSpStyle();
            if (style != null) {
                final CTStyleMatrixReference fillRef = style.getFillRef();
                final int idx = (int)fillRef.getIdx();
                final CTSchemeColor phClr = fillRef.getSchemeClr();
                final XSLFSheet sheet = this._shape.getSheet();
                final XSLFTheme theme = sheet.getTheme();
                XmlObject fillProps = null;
                if (idx >= 1 && idx <= 999) {
                    fillProps = theme.getXmlObject().getThemeElements().getFmtScheme().getFillStyleLst().selectPath("*")[idx - 1];
                }
                else if (idx >= 1001) {
                    fillProps = theme.getXmlObject().getThemeElements().getFmtScheme().getBgFillStyleLst().selectPath("*")[idx - 1001];
                }
                if (fillProps != null) {
                    paint = this.selectPaint(graphics, fillProps, phClr, sheet.getPackagePart());
                }
            }
        }
        return (paint == RenderableShape.NO_PAINT) ? null : paint;
    }
    
    public Paint getLinePaint(final Graphics2D graphics) {
        final PropertyFetcher<Paint> fetcher = new PropertyFetcher<Paint>() {
            @Override
            public boolean fetch(final XSLFSimpleShape shape) {
                final CTLineProperties spPr = shape.getSpPr().getLn();
                if (spPr != null) {
                    if (spPr.isSetNoFill()) {
                        ((PropertyFetcher<Color>)this).setValue(RenderableShape.NO_PAINT);
                        return true;
                    }
                    final Paint paint = RenderableShape.this.getPaint(graphics, spPr, null);
                    if (paint != null) {
                        this.setValue(paint);
                        return true;
                    }
                }
                return false;
            }
        };
        this._shape.fetchShapeProperty(fetcher);
        Paint paint = fetcher.getValue();
        if (paint == null) {
            final CTShapeStyle style = this._shape.getSpStyle();
            if (style != null) {
                final CTStyleMatrixReference lnRef = style.getLnRef();
                final int idx = (int)lnRef.getIdx();
                final CTSchemeColor phClr = lnRef.getSchemeClr();
                if (idx > 0) {
                    final XSLFTheme theme = this._shape.getSheet().getTheme();
                    final XmlObject lnProps = theme.getXmlObject().getThemeElements().getFmtScheme().getLnStyleLst().selectPath("*")[idx - 1];
                    paint = this.getPaint(graphics, lnProps, phClr);
                }
            }
        }
        return (paint == RenderableShape.NO_PAINT) ? null : paint;
    }
    
    private static float[] getDashPattern(final LineDash lineDash, final float lineWidth) {
        float[] dash = null;
        switch (lineDash) {
            case SYS_DOT: {
                dash = new float[] { lineWidth, lineWidth };
                break;
            }
            case SYS_DASH: {
                dash = new float[] { 2.0f * lineWidth, 2.0f * lineWidth };
                break;
            }
            case DASH: {
                dash = new float[] { 3.0f * lineWidth, 4.0f * lineWidth };
                break;
            }
            case DASH_DOT: {
                dash = new float[] { 4.0f * lineWidth, 3.0f * lineWidth, lineWidth, 3.0f * lineWidth };
                break;
            }
            case LG_DASH: {
                dash = new float[] { 8.0f * lineWidth, 3.0f * lineWidth };
                break;
            }
            case LG_DASH_DOT: {
                dash = new float[] { 8.0f * lineWidth, 3.0f * lineWidth, lineWidth, 3.0f * lineWidth };
                break;
            }
            case LG_DASH_DOT_DOT: {
                dash = new float[] { 8.0f * lineWidth, 3.0f * lineWidth, lineWidth, 3.0f * lineWidth, lineWidth, 3.0f * lineWidth };
                break;
            }
        }
        return dash;
    }
    
    public Stroke applyStroke(final Graphics2D graphics) {
        float lineWidth = (float)this._shape.getLineWidth();
        if (lineWidth == 0.0f) {
            lineWidth = 0.25f;
        }
        final LineDash lineDash = this._shape.getLineDash();
        float[] dash = null;
        final float dash_phase = 0.0f;
        if (lineDash != null) {
            dash = getDashPattern(lineDash, lineWidth);
        }
        int cap = 0;
        final LineCap lineCap = this._shape.getLineCap();
        if (lineCap != null) {
            switch (lineCap) {
                case ROUND: {
                    cap = 1;
                    break;
                }
                case SQUARE: {
                    cap = 2;
                    break;
                }
                default: {
                    cap = 0;
                    break;
                }
            }
        }
        final int meter = 1;
        final Stroke stroke = new BasicStroke(lineWidth, cap, meter, Math.max(1.0f, lineWidth), dash, dash_phase);
        graphics.setStroke(stroke);
        return stroke;
    }
    
    public void render(final Graphics2D graphics) {
        final Collection<Outline> elems = this.computeOutlines(graphics);
        final XSLFShadow shadow = this._shape.getShadow();
        final Paint fill = this.getFillPaint(graphics);
        final Paint line = this.getLinePaint(graphics);
        this.applyStroke(graphics);
        if (shadow != null) {
            for (final Outline o : elems) {
                if (o.getPath().isFilled()) {
                    if (fill != null) {
                        shadow.fill(graphics, o.getOutline());
                    }
                    else {
                        if (line == null) {
                            continue;
                        }
                        shadow.draw(graphics, o.getOutline());
                    }
                }
            }
        }
        if (fill != null) {
            for (final Outline o : elems) {
                if (o.getPath().isFilled()) {
                    graphics.setPaint(fill);
                    graphics.fill(o.getOutline());
                }
            }
        }
        this._shape.drawContent(graphics);
        if (line != null) {
            for (final Outline o : elems) {
                if (o.getPath().isStroked()) {
                    graphics.setPaint(line);
                    graphics.draw(o.getOutline());
                }
            }
        }
    }
    
    private Collection<Outline> computeOutlines(final Graphics2D graphics) {
        final Collection<Outline> lst = new ArrayList<Outline>();
        final CustomGeometry geom = this._shape.getGeometry();
        if (geom == null) {
            return lst;
        }
        final Rectangle2D anchor = this.getAnchor(graphics);
        for (final Path p : geom) {
            final double w = (p.getW() == -1L) ? (anchor.getWidth() * 12700.0) : ((double)p.getW());
            final double h = (p.getH() == -1L) ? (anchor.getHeight() * 12700.0) : ((double)p.getH());
            final Rectangle2D pathAnchor = new Rectangle2D.Double(0.0, 0.0, w, h);
            final Context ctx = new Context(geom, pathAnchor, new IAdjustableShape() {
                public Guide getAdjustValue(final String name) {
                    final CTPresetGeometry2D prst = RenderableShape.this._shape.getSpPr().getPrstGeom();
                    if (prst.isSetAvLst()) {
                        for (final CTGeomGuide g : prst.getAvLst().getGdList()) {
                            if (g.getName().equals(name)) {
                                return new Guide(g);
                            }
                        }
                    }
                    return null;
                }
            });
            final Shape gp = p.getPath(ctx);
            final AffineTransform at = new AffineTransform();
            at.translate(anchor.getX(), anchor.getY());
            double scaleX;
            if (p.getW() != -1L) {
                scaleX = anchor.getWidth() / p.getW();
            }
            else {
                scaleX = 7.874015748031496E-5;
            }
            double scaleY;
            if (p.getH() != -1L) {
                scaleY = anchor.getHeight() / p.getH();
            }
            else {
                scaleY = 7.874015748031496E-5;
            }
            at.scale(scaleX, scaleY);
            final Shape canvasShape = at.createTransformedShape(gp);
            lst.add(new Outline(canvasShape, p));
        }
        return lst;
    }
    
    public Rectangle2D getAnchor(final Graphics2D graphics) {
        Rectangle2D anchor = this._shape.getAnchor();
        if (graphics == null) {
            return anchor;
        }
        final AffineTransform tx = (AffineTransform)graphics.getRenderingHint(XSLFRenderingHint.GROUP_TRANSFORM);
        if (tx != null) {
            anchor = tx.createTransformedShape(anchor).getBounds2D();
        }
        return anchor;
    }
    
    static {
        NO_PAINT = new Color(255, 255, 255, 0);
    }
}
