// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.xslf.usermodel;

import java.util.List;
import org.openxmlformats.schemas.drawingml.x2006.main.CTTableRow;
import org.openxmlformats.schemas.drawingml.x2006.main.CTTable;

public class DrawingTable
{
    private final CTTable table;
    
    public DrawingTable(final CTTable table) {
        this.table = table;
    }
    
    public DrawingTableRow[] getRows() {
        final List<CTTableRow> ctTableRows = this.table.getTrList();
        final DrawingTableRow[] o = new DrawingTableRow[ctTableRows.size()];
        for (int i = 0; i < o.length; ++i) {
            o[i] = new DrawingTableRow(ctTableRows.get(i));
        }
        return o;
    }
}
