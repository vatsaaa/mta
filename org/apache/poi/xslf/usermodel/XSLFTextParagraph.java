// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.xslf.usermodel;

import org.apache.poi.xslf.model.PropertyFetcher;
import org.openxmlformats.schemas.presentationml.x2006.main.CTPlaceholder;
import java.awt.font.TextLayout;
import java.awt.font.LineBreakMeasurer;
import java.text.AttributedCharacterIterator;
import java.awt.font.TextAttribute;
import java.awt.RenderingHints;
import java.text.AttributedString;
import java.awt.geom.Rectangle2D;
import java.awt.Graphics2D;
import org.openxmlformats.schemas.drawingml.x2006.main.CTTextAutonumberBullet;
import org.openxmlformats.schemas.drawingml.x2006.main.STTextAutonumberScheme;
import org.openxmlformats.schemas.drawingml.x2006.main.CTTextNormalAutofit;
import org.openxmlformats.schemas.drawingml.x2006.main.CTTextSpacing;
import org.openxmlformats.schemas.drawingml.x2006.main.CTTextTabStop;
import org.openxmlformats.schemas.drawingml.x2006.main.CTTextTabStopList;
import org.apache.poi.util.Units;
import org.openxmlformats.schemas.drawingml.x2006.main.CTTextBulletSizePoint;
import org.openxmlformats.schemas.drawingml.x2006.main.CTTextBulletSizePercent;
import org.openxmlformats.schemas.drawingml.x2006.main.CTSRgbColor;
import org.openxmlformats.schemas.drawingml.x2006.main.CTColor;
import org.openxmlformats.schemas.drawingml.x2006.main.CTSchemeColor;
import java.awt.Color;
import org.openxmlformats.schemas.drawingml.x2006.main.CTTextCharBullet;
import org.openxmlformats.schemas.drawingml.x2006.main.CTTextFont;
import org.openxmlformats.schemas.drawingml.x2006.main.STTextAlignType;
import org.openxmlformats.schemas.drawingml.x2006.main.CTTextParagraphProperties;
import org.apache.poi.xslf.model.ParagraphPropertyFetcher;
import org.openxmlformats.schemas.drawingml.x2006.main.CTTextCharacterProperties;
import org.apache.poi.util.Internal;
import java.util.Iterator;
import org.apache.xmlbeans.XmlObject;
import org.openxmlformats.schemas.drawingml.x2006.main.CTTextField;
import org.openxmlformats.schemas.drawingml.x2006.main.CTTextLineBreak;
import org.openxmlformats.schemas.drawingml.x2006.main.CTRegularTextRun;
import java.util.ArrayList;
import java.util.List;
import org.openxmlformats.schemas.drawingml.x2006.main.CTTextParagraph;

public class XSLFTextParagraph implements Iterable<XSLFTextRun>
{
    private final CTTextParagraph _p;
    private final List<XSLFTextRun> _runs;
    private final XSLFTextShape _shape;
    private List<TextFragment> _lines;
    private TextFragment _bullet;
    private double _maxLineHeight;
    
    XSLFTextParagraph(final CTTextParagraph p, final XSLFTextShape shape) {
        this._p = p;
        this._runs = new ArrayList<XSLFTextRun>();
        this._shape = shape;
        for (final XmlObject ch : this._p.selectPath("*")) {
            if (ch instanceof CTRegularTextRun) {
                final CTRegularTextRun r = (CTRegularTextRun)ch;
                this._runs.add(new XSLFTextRun(r, this));
            }
            else if (ch instanceof CTTextLineBreak) {
                final CTTextLineBreak br = (CTTextLineBreak)ch;
                final CTRegularTextRun r2 = CTRegularTextRun.Factory.newInstance();
                r2.setRPr(br.getRPr());
                r2.setT("\n");
                this._runs.add(new XSLFTextRun(r2, this));
            }
            else if (ch instanceof CTTextField) {
                final CTTextField f = (CTTextField)ch;
                final CTRegularTextRun r2 = CTRegularTextRun.Factory.newInstance();
                r2.setRPr(f.getRPr());
                r2.setT(f.getT());
                this._runs.add(new XSLFTextRun(r2, this));
            }
        }
    }
    
    public String getText() {
        final StringBuilder out = new StringBuilder();
        for (final XSLFTextRun r : this._runs) {
            out.append(r.getText());
        }
        return out.toString();
    }
    
    String getRenderableText() {
        final StringBuilder out = new StringBuilder();
        for (final XSLFTextRun r : this._runs) {
            out.append(r.getRenderableText());
        }
        return out.toString();
    }
    
    @Internal
    public CTTextParagraph getXmlObject() {
        return this._p;
    }
    
    XSLFTextShape getParentShape() {
        return this._shape;
    }
    
    public List<XSLFTextRun> getTextRuns() {
        return this._runs;
    }
    
    public Iterator<XSLFTextRun> iterator() {
        return this._runs.iterator();
    }
    
    public XSLFTextRun addNewTextRun() {
        final CTRegularTextRun r = this._p.addNewR();
        final CTTextCharacterProperties rPr = r.addNewRPr();
        rPr.setLang("en-US");
        final XSLFTextRun run = new XSLFTextRun(r, this);
        this._runs.add(run);
        return run;
    }
    
    public XSLFTextRun addLineBreak() {
        final CTTextLineBreak br = this._p.addNewBr();
        final CTTextCharacterProperties brProps = br.addNewRPr();
        if (this._runs.size() > 0) {
            final CTTextCharacterProperties prevRun = this._runs.get(this._runs.size() - 1).getRPr();
            brProps.set(prevRun);
        }
        final CTRegularTextRun r = CTRegularTextRun.Factory.newInstance();
        r.setRPr(brProps);
        r.setT("\n");
        final XSLFTextRun run = new XSLFLineBreak(r, this, brProps);
        this._runs.add(run);
        return run;
    }
    
    public TextAlign getTextAlign() {
        final ParagraphPropertyFetcher<TextAlign> fetcher = new ParagraphPropertyFetcher<TextAlign>(this.getLevel()) {
            @Override
            public boolean fetch(final CTTextParagraphProperties props) {
                if (props.isSetAlgn()) {
                    final TextAlign val = TextAlign.values()[props.getAlgn().intValue() - 1];
                    this.setValue(val);
                    return true;
                }
                return false;
            }
        };
        this.fetchParagraphProperty(fetcher);
        return (fetcher.getValue() == null) ? TextAlign.LEFT : fetcher.getValue();
    }
    
    public void setTextAlign(final TextAlign align) {
        final CTTextParagraphProperties pr = this._p.isSetPPr() ? this._p.getPPr() : this._p.addNewPPr();
        if (align == null) {
            if (pr.isSetAlgn()) {
                pr.unsetAlgn();
            }
        }
        else {
            pr.setAlgn(STTextAlignType.Enum.forInt(align.ordinal() + 1));
        }
    }
    
    public String getBulletFont() {
        final ParagraphPropertyFetcher<String> fetcher = new ParagraphPropertyFetcher<String>(this.getLevel()) {
            @Override
            public boolean fetch(final CTTextParagraphProperties props) {
                if (props.isSetBuFont()) {
                    this.setValue(props.getBuFont().getTypeface());
                    return true;
                }
                return false;
            }
        };
        this.fetchParagraphProperty(fetcher);
        return fetcher.getValue();
    }
    
    public void setBulletFont(final String typeface) {
        final CTTextParagraphProperties pr = this._p.isSetPPr() ? this._p.getPPr() : this._p.addNewPPr();
        final CTTextFont font = pr.isSetBuFont() ? pr.getBuFont() : pr.addNewBuFont();
        font.setTypeface(typeface);
    }
    
    public String getBulletCharacter() {
        final ParagraphPropertyFetcher<String> fetcher = new ParagraphPropertyFetcher<String>(this.getLevel()) {
            @Override
            public boolean fetch(final CTTextParagraphProperties props) {
                if (props.isSetBuChar()) {
                    this.setValue(props.getBuChar().getChar());
                    return true;
                }
                return false;
            }
        };
        this.fetchParagraphProperty(fetcher);
        return fetcher.getValue();
    }
    
    public void setBulletCharacter(final String str) {
        final CTTextParagraphProperties pr = this._p.isSetPPr() ? this._p.getPPr() : this._p.addNewPPr();
        final CTTextCharBullet c = pr.isSetBuChar() ? pr.getBuChar() : pr.addNewBuChar();
        c.setChar(str);
    }
    
    public Color getBulletFontColor() {
        final XSLFTheme theme = this.getParentShape().getSheet().getTheme();
        final ParagraphPropertyFetcher<Color> fetcher = new ParagraphPropertyFetcher<Color>(this.getLevel()) {
            @Override
            public boolean fetch(final CTTextParagraphProperties props) {
                if (props.isSetBuClr()) {
                    final XSLFColor c = new XSLFColor(props.getBuClr(), theme, null);
                    this.setValue(c.getColor());
                    return true;
                }
                return false;
            }
        };
        this.fetchParagraphProperty(fetcher);
        return fetcher.getValue();
    }
    
    public void setBulletFontColor(final Color color) {
        final CTTextParagraphProperties pr = this._p.isSetPPr() ? this._p.getPPr() : this._p.addNewPPr();
        final CTColor c = pr.isSetBuClr() ? pr.getBuClr() : pr.addNewBuClr();
        final CTSRgbColor clr = c.isSetSrgbClr() ? c.getSrgbClr() : c.addNewSrgbClr();
        clr.setVal(new byte[] { (byte)color.getRed(), (byte)color.getGreen(), (byte)color.getBlue() });
    }
    
    public double getBulletFontSize() {
        final ParagraphPropertyFetcher<Double> fetcher = new ParagraphPropertyFetcher<Double>(this.getLevel()) {
            @Override
            public boolean fetch(final CTTextParagraphProperties props) {
                if (props.isSetBuSzPct()) {
                    this.setValue(props.getBuSzPct().getVal() * 0.001);
                    return true;
                }
                if (props.isSetBuSzPts()) {
                    this.setValue(-props.getBuSzPts().getVal() * 0.01);
                    return true;
                }
                return false;
            }
        };
        this.fetchParagraphProperty(fetcher);
        return (fetcher.getValue() == null) ? 100.0 : fetcher.getValue();
    }
    
    public void setBulletFontSize(final double bulletSize) {
        final CTTextParagraphProperties pr = this._p.isSetPPr() ? this._p.getPPr() : this._p.addNewPPr();
        if (bulletSize >= 0.0) {
            final CTTextBulletSizePercent pt = pr.isSetBuSzPct() ? pr.getBuSzPct() : pr.addNewBuSzPct();
            pt.setVal((int)(bulletSize * 1000.0));
            if (pr.isSetBuSzPts()) {
                pr.unsetBuSzPts();
            }
        }
        else {
            final CTTextBulletSizePoint pt2 = pr.isSetBuSzPts() ? pr.getBuSzPts() : pr.addNewBuSzPts();
            pt2.setVal((int)(-bulletSize * 100.0));
            if (pr.isSetBuSzPct()) {
                pr.unsetBuSzPct();
            }
        }
    }
    
    public void setIndent(final double value) {
        final CTTextParagraphProperties pr = this._p.isSetPPr() ? this._p.getPPr() : this._p.addNewPPr();
        if (value == -1.0) {
            if (pr.isSetIndent()) {
                pr.unsetIndent();
            }
        }
        else {
            pr.setIndent(Units.toEMU(value));
        }
    }
    
    public double getIndent() {
        final ParagraphPropertyFetcher<Double> fetcher = new ParagraphPropertyFetcher<Double>(this.getLevel()) {
            @Override
            public boolean fetch(final CTTextParagraphProperties props) {
                if (props.isSetIndent()) {
                    this.setValue(Units.toPoints(props.getIndent()));
                    return true;
                }
                return false;
            }
        };
        this.fetchParagraphProperty(fetcher);
        return (fetcher.getValue() == null) ? 0.0 : fetcher.getValue();
    }
    
    public void setLeftMargin(final double value) {
        final CTTextParagraphProperties pr = this._p.isSetPPr() ? this._p.getPPr() : this._p.addNewPPr();
        if (value == -1.0) {
            if (pr.isSetMarL()) {
                pr.unsetMarL();
            }
        }
        else {
            pr.setMarL(Units.toEMU(value));
        }
    }
    
    public double getLeftMargin() {
        final ParagraphPropertyFetcher<Double> fetcher = new ParagraphPropertyFetcher<Double>(this.getLevel()) {
            @Override
            public boolean fetch(final CTTextParagraphProperties props) {
                if (props.isSetMarL()) {
                    final double val = Units.toPoints(props.getMarL());
                    this.setValue(val);
                    return true;
                }
                return false;
            }
        };
        this.fetchParagraphProperty(fetcher);
        return (fetcher.getValue() == null) ? 0.0 : fetcher.getValue();
    }
    
    public double getDefaultTabSize() {
        final ParagraphPropertyFetcher<Double> fetcher = new ParagraphPropertyFetcher<Double>(this.getLevel()) {
            @Override
            public boolean fetch(final CTTextParagraphProperties props) {
                if (props.isSetDefTabSz()) {
                    final double val = Units.toPoints(props.getDefTabSz());
                    this.setValue(val);
                    return true;
                }
                return false;
            }
        };
        this.fetchParagraphProperty(fetcher);
        return (fetcher.getValue() == null) ? 0.0 : fetcher.getValue();
    }
    
    public double getTabStop(final int idx) {
        final ParagraphPropertyFetcher<Double> fetcher = new ParagraphPropertyFetcher<Double>(this.getLevel()) {
            @Override
            public boolean fetch(final CTTextParagraphProperties props) {
                if (props.isSetTabLst()) {
                    final CTTextTabStopList tabStops = props.getTabLst();
                    if (idx < tabStops.sizeOfTabArray()) {
                        final CTTextTabStop ts = tabStops.getTabArray(idx);
                        final double val = Units.toPoints(ts.getPos());
                        this.setValue(val);
                        return true;
                    }
                }
                return false;
            }
        };
        this.fetchParagraphProperty(fetcher);
        return (fetcher.getValue() == null) ? this.getDefaultTabSize() : fetcher.getValue();
    }
    
    public void setLineSpacing(final double linespacing) {
        final CTTextParagraphProperties pr = this._p.isSetPPr() ? this._p.getPPr() : this._p.addNewPPr();
        final CTTextSpacing spc = CTTextSpacing.Factory.newInstance();
        if (linespacing >= 0.0) {
            spc.addNewSpcPct().setVal((int)(linespacing * 1000.0));
        }
        else {
            spc.addNewSpcPts().setVal((int)(-linespacing * 100.0));
        }
        pr.setLnSpc(spc);
    }
    
    public double getLineSpacing() {
        final ParagraphPropertyFetcher<Double> fetcher = new ParagraphPropertyFetcher<Double>(this.getLevel()) {
            @Override
            public boolean fetch(final CTTextParagraphProperties props) {
                if (props.isSetLnSpc()) {
                    final CTTextSpacing spc = props.getLnSpc();
                    if (spc.isSetSpcPct()) {
                        this.setValue(spc.getSpcPct().getVal() * 0.001);
                    }
                    else if (spc.isSetSpcPts()) {
                        this.setValue(-spc.getSpcPts().getVal() * 0.01);
                    }
                    return true;
                }
                return false;
            }
        };
        this.fetchParagraphProperty(fetcher);
        double lnSpc = (fetcher.getValue() == null) ? 100.0 : fetcher.getValue();
        if (lnSpc > 0.0) {
            final CTTextNormalAutofit normAutofit = this.getParentShape().getTextBodyPr().getNormAutofit();
            if (normAutofit != null) {
                final double scale = 1.0 - normAutofit.getLnSpcReduction() / 100000.0;
                lnSpc *= scale;
            }
        }
        return lnSpc;
    }
    
    public void setSpaceBefore(final double spaceBefore) {
        final CTTextParagraphProperties pr = this._p.isSetPPr() ? this._p.getPPr() : this._p.addNewPPr();
        final CTTextSpacing spc = CTTextSpacing.Factory.newInstance();
        if (spaceBefore >= 0.0) {
            spc.addNewSpcPct().setVal((int)(spaceBefore * 1000.0));
        }
        else {
            spc.addNewSpcPts().setVal((int)(-spaceBefore * 100.0));
        }
        pr.setSpcBef(spc);
    }
    
    public double getSpaceBefore() {
        final ParagraphPropertyFetcher<Double> fetcher = new ParagraphPropertyFetcher<Double>(this.getLevel()) {
            @Override
            public boolean fetch(final CTTextParagraphProperties props) {
                if (props.isSetSpcBef()) {
                    final CTTextSpacing spc = props.getSpcBef();
                    if (spc.isSetSpcPct()) {
                        this.setValue(spc.getSpcPct().getVal() * 0.001);
                    }
                    else if (spc.isSetSpcPts()) {
                        this.setValue(-spc.getSpcPts().getVal() * 0.01);
                    }
                    return true;
                }
                return false;
            }
        };
        this.fetchParagraphProperty(fetcher);
        final double spcBef = (fetcher.getValue() == null) ? 0.0 : fetcher.getValue();
        return spcBef;
    }
    
    public void setSpaceAfter(final double spaceAfter) {
        final CTTextParagraphProperties pr = this._p.isSetPPr() ? this._p.getPPr() : this._p.addNewPPr();
        final CTTextSpacing spc = CTTextSpacing.Factory.newInstance();
        if (spaceAfter >= 0.0) {
            spc.addNewSpcPct().setVal((int)(spaceAfter * 1000.0));
        }
        else {
            spc.addNewSpcPts().setVal((int)(-spaceAfter * 100.0));
        }
        pr.setSpcAft(spc);
    }
    
    public double getSpaceAfter() {
        final ParagraphPropertyFetcher<Double> fetcher = new ParagraphPropertyFetcher<Double>(this.getLevel()) {
            @Override
            public boolean fetch(final CTTextParagraphProperties props) {
                if (props.isSetSpcAft()) {
                    final CTTextSpacing spc = props.getSpcAft();
                    if (spc.isSetSpcPct()) {
                        this.setValue(spc.getSpcPct().getVal() * 0.001);
                    }
                    else if (spc.isSetSpcPts()) {
                        this.setValue(-spc.getSpcPts().getVal() * 0.01);
                    }
                    return true;
                }
                return false;
            }
        };
        this.fetchParagraphProperty(fetcher);
        return (fetcher.getValue() == null) ? 0.0 : fetcher.getValue();
    }
    
    public void setLevel(final int level) {
        final CTTextParagraphProperties pr = this._p.isSetPPr() ? this._p.getPPr() : this._p.addNewPPr();
        pr.setLvl(level);
    }
    
    public int getLevel() {
        final CTTextParagraphProperties pr = this._p.getPPr();
        if (pr == null) {
            return 0;
        }
        return pr.getLvl();
    }
    
    public boolean isBullet() {
        final ParagraphPropertyFetcher<Boolean> fetcher = new ParagraphPropertyFetcher<Boolean>(this.getLevel()) {
            @Override
            public boolean fetch(final CTTextParagraphProperties props) {
                if (props.isSetBuNone()) {
                    this.setValue(false);
                    return true;
                }
                if (props.isSetBuFont() || props.isSetBuChar()) {
                    this.setValue(true);
                    return true;
                }
                return false;
            }
        };
        this.fetchParagraphProperty(fetcher);
        return fetcher.getValue() != null && fetcher.getValue();
    }
    
    public void setBullet(final boolean flag) {
        if (this.isBullet() == flag) {
            return;
        }
        final CTTextParagraphProperties pr = this._p.isSetPPr() ? this._p.getPPr() : this._p.addNewPPr();
        if (!flag) {
            pr.addNewBuNone();
        }
        else {
            pr.addNewBuFont().setTypeface("Arial");
            pr.addNewBuChar().setChar("\u2022");
        }
    }
    
    public void setBulletAutoNumber(final ListAutoNumber scheme, final int startAt) {
        if (startAt < 1) {
            throw new IllegalArgumentException("Start Number must be greater or equal that 1");
        }
        final CTTextParagraphProperties pr = this._p.isSetPPr() ? this._p.getPPr() : this._p.addNewPPr();
        final CTTextAutonumberBullet lst = pr.isSetBuAutoNum() ? pr.getBuAutoNum() : pr.addNewBuAutoNum();
        lst.setType(STTextAutonumberScheme.Enum.forInt(scheme.ordinal() + 1));
        lst.setStartAt(startAt);
    }
    
    @Override
    public String toString() {
        return "[" + this.getClass() + "]" + this.getText();
    }
    
    List<TextFragment> getTextLines() {
        return this._lines;
    }
    
    double getWrappingWidth(final boolean firstLine, final Graphics2D graphics) {
        final double leftInset = this._shape.getLeftInset();
        final double rightInset = this._shape.getRightInset();
        final RenderableShape rShape = new RenderableShape(this._shape);
        final Rectangle2D anchor = rShape.getAnchor(graphics);
        final double leftMargin = this.getLeftMargin();
        final double indent = this.getIndent();
        double width;
        if (!this._shape.getWordWrap()) {
            width = this._shape.getSheet().getSlideShow().getPageSize().getWidth() - anchor.getX();
        }
        else {
            width = anchor.getWidth() - leftInset - rightInset - leftMargin;
            if (firstLine) {
                if (this.isBullet()) {
                    if (indent > 0.0) {
                        width -= indent;
                    }
                }
                else if (indent > 0.0) {
                    width -= indent;
                }
                else if (indent < 0.0) {
                    width += leftMargin;
                }
            }
        }
        return width;
    }
    
    public double draw(final Graphics2D graphics, final double x, final double y) {
        final double leftInset = this._shape.getLeftInset();
        final double rightInset = this._shape.getRightInset();
        final RenderableShape rShape = new RenderableShape(this._shape);
        final Rectangle2D anchor = rShape.getAnchor(graphics);
        double penY = y;
        final double leftMargin = this.getLeftMargin();
        boolean firstLine = true;
        final double indent = this.getIndent();
        final double spacing = this.getLineSpacing();
        for (final TextFragment line : this._lines) {
            double penX = x + leftMargin;
            if (firstLine) {
                if (this._bullet != null) {
                    if (indent < 0.0) {
                        this._bullet.draw(graphics, penX + indent, penY);
                    }
                    else if (indent > 0.0) {
                        this._bullet.draw(graphics, penX, penY);
                        penX += indent;
                    }
                    else {
                        this._bullet.draw(graphics, penX, penY);
                        penX += this._bullet._layout.getAdvance() + 1.0f;
                    }
                }
                else {
                    penX += indent;
                }
            }
            switch (this.getTextAlign()) {
                case CENTER: {
                    penX += (anchor.getWidth() - leftMargin - line.getWidth() - leftInset - rightInset) / 2.0;
                    break;
                }
                case RIGHT: {
                    penX += anchor.getWidth() - line.getWidth() - leftInset - rightInset;
                    break;
                }
            }
            line.draw(graphics, penX, penY);
            if (spacing > 0.0) {
                penY += spacing * 0.01 * line.getHeight();
            }
            else {
                penY += -spacing;
            }
            firstLine = false;
        }
        return penY - y;
    }
    
    AttributedString getAttributedString(final Graphics2D graphics) {
        final String text = this.getRenderableText();
        final AttributedString string = new AttributedString(text);
        final XSLFFontManager fontHandler = (XSLFFontManager)graphics.getRenderingHint(XSLFRenderingHint.FONT_HANDLER);
        int startIndex = 0;
        for (final XSLFTextRun run : this._runs) {
            final int length = run.getRenderableText().length();
            if (length == 0) {
                continue;
            }
            final int endIndex = startIndex + length;
            string.addAttribute(TextAttribute.FOREGROUND, run.getFontColor(), startIndex, endIndex);
            String fontFamily = run.getFontFamily();
            if (fontHandler != null) {
                fontFamily = fontHandler.getRendererableFont(fontFamily, run.getPitchAndFamily());
            }
            string.addAttribute(TextAttribute.FAMILY, fontFamily, startIndex, endIndex);
            final float fontSz = (float)run.getFontSize();
            string.addAttribute(TextAttribute.SIZE, fontSz, startIndex, endIndex);
            if (run.isBold()) {
                string.addAttribute(TextAttribute.WEIGHT, TextAttribute.WEIGHT_BOLD, startIndex, endIndex);
            }
            if (run.isItalic()) {
                string.addAttribute(TextAttribute.POSTURE, TextAttribute.POSTURE_OBLIQUE, startIndex, endIndex);
            }
            if (run.isUnderline()) {
                string.addAttribute(TextAttribute.UNDERLINE, TextAttribute.UNDERLINE_ON, startIndex, endIndex);
                string.addAttribute(TextAttribute.INPUT_METHOD_UNDERLINE, TextAttribute.UNDERLINE_LOW_TWO_PIXEL, startIndex, endIndex);
            }
            if (run.isStrikethrough()) {
                string.addAttribute(TextAttribute.STRIKETHROUGH, TextAttribute.STRIKETHROUGH_ON, startIndex, endIndex);
            }
            if (run.isSubscript()) {
                string.addAttribute(TextAttribute.SUPERSCRIPT, TextAttribute.SUPERSCRIPT_SUB, startIndex, endIndex);
            }
            if (run.isSuperscript()) {
                string.addAttribute(TextAttribute.SUPERSCRIPT, TextAttribute.SUPERSCRIPT_SUPER, startIndex, endIndex);
            }
            startIndex = endIndex;
        }
        return string;
    }
    
    private void ensureNotEmpty() {
        final XSLFTextRun r = this.addNewTextRun();
        r.setText(" ");
        final CTTextCharacterProperties endPr = this._p.getEndParaRPr();
        if (endPr != null && endPr.isSetSz()) {
            r.setFontSize(endPr.getSz() / 100);
        }
    }
    
    List<TextFragment> breakText(final Graphics2D graphics) {
        this._lines = new ArrayList<TextFragment>();
        final boolean emptyParagraph = this._runs.size() == 0;
        if (this._runs.size() == 0) {
            this.ensureNotEmpty();
        }
        final String text = this.getRenderableText();
        if (text.length() == 0) {
            return this._lines;
        }
        final AttributedString at = this.getAttributedString(graphics);
        final AttributedCharacterIterator it = at.getIterator();
        final LineBreakMeasurer measurer = new LineBreakMeasurer(it, graphics.getFontRenderContext());
        int endIndex;
        do {
            final int startIndex = measurer.getPosition();
            double wrappingWidth = this.getWrappingWidth(this._lines.size() == 0, graphics) + 1.0;
            if (wrappingWidth < 0.0) {
                wrappingWidth = 1.0;
            }
            int nextBreak = text.indexOf(10, startIndex + 1);
            if (nextBreak == -1) {
                nextBreak = it.getEndIndex();
            }
            TextLayout layout = measurer.nextLayout((float)wrappingWidth, nextBreak, true);
            if (layout == null) {
                layout = measurer.nextLayout((float)wrappingWidth, nextBreak, false);
            }
            if (layout == null) {
                break;
            }
            endIndex = measurer.getPosition();
            if (endIndex < it.getEndIndex() && text.charAt(endIndex) == '\n') {
                measurer.setPosition(endIndex + 1);
            }
            final TextAlign hAlign = this.getTextAlign();
            if (hAlign == TextAlign.JUSTIFY || hAlign == TextAlign.JUSTIFY_LOW) {
                layout = layout.getJustifiedLayout((float)wrappingWidth);
            }
            final AttributedString str = new AttributedString(it, startIndex, endIndex);
            final TextFragment line = new TextFragment(layout, emptyParagraph ? null : str);
            this._lines.add(line);
            this._maxLineHeight = Math.max(this._maxLineHeight, line.getHeight());
        } while (endIndex != it.getEndIndex());
        if (this.isBullet() && !emptyParagraph) {
            final String buCharacter = this.getBulletCharacter();
            String buFont = this.getBulletFont();
            if (buFont == null) {
                buFont = this.getTextRuns().get(0).getFontFamily();
            }
            if (buCharacter != null && buFont != null && this._lines.size() > 0) {
                final AttributedString str2 = new AttributedString(buCharacter);
                final TextFragment firstLine = this._lines.get(0);
                final AttributedCharacterIterator bit = firstLine._str.getIterator();
                final Color buColor = this.getBulletFontColor();
                str2.addAttribute(TextAttribute.FOREGROUND, (buColor == null) ? bit.getAttribute(TextAttribute.FOREGROUND) : buColor);
                str2.addAttribute(TextAttribute.FAMILY, buFont);
                float fontSize = (float)bit.getAttribute(TextAttribute.SIZE);
                final float buSz = (float)this.getBulletFontSize();
                if (buSz > 0.0f) {
                    fontSize *= (float)(buSz * 0.01);
                }
                else {
                    fontSize = -buSz;
                }
                str2.addAttribute(TextAttribute.SIZE, fontSize);
                final TextLayout layout2 = new TextLayout(str2.getIterator(), graphics.getFontRenderContext());
                this._bullet = new TextFragment(layout2, str2);
            }
        }
        return this._lines;
    }
    
    CTTextParagraphProperties getDefaultMasterStyle() {
        final CTPlaceholder ph = this._shape.getCTPlaceholder();
        String defaultStyleSelector = null;
        if (ph == null) {
            defaultStyleSelector = "otherStyle";
        }
        else {
            switch (ph.getType().intValue()) {
                case 1:
                case 3: {
                    defaultStyleSelector = "titleStyle";
                    break;
                }
                case 5:
                case 6:
                case 7: {
                    defaultStyleSelector = "otherStyle";
                    break;
                }
                default: {
                    defaultStyleSelector = "bodyStyle";
                    break;
                }
            }
        }
        final int level = this.getLevel();
        XSLFSheet masterSheet;
        for (masterSheet = this._shape.getSheet(); masterSheet.getMasterSheet() != null; masterSheet = masterSheet.getMasterSheet()) {}
        final XmlObject[] o = masterSheet.getXmlObject().selectPath("declare namespace p='http://schemas.openxmlformats.org/presentationml/2006/main' declare namespace a='http://schemas.openxmlformats.org/drawingml/2006/main' .//p:txStyles/p:" + defaultStyleSelector + "/a:lvl" + (level + 1) + "pPr");
        if (o.length == 1) {
            return (CTTextParagraphProperties)o[0];
        }
        throw new IllegalArgumentException("Failed to fetch default style for " + defaultStyleSelector + " and level=" + level);
    }
    
    private boolean fetchParagraphProperty(final ParagraphPropertyFetcher visitor) {
        boolean ok = false;
        if (this._p.isSetPPr()) {
            ok = visitor.fetch(this._p.getPPr());
        }
        if (!ok) {
            final XSLFTextShape shape = this.getParentShape();
            ok = shape.fetchShapeProperty(visitor);
            if (!ok) {
                final CTPlaceholder ph = shape.getCTPlaceholder();
                if (ph == null) {
                    final XMLSlideShow ppt = this.getParentShape().getSheet().getSlideShow();
                    final CTTextParagraphProperties themeProps = ppt.getDefaultParagraphStyle(this.getLevel());
                    if (themeProps != null) {
                        ok = visitor.fetch(themeProps);
                    }
                }
                if (!ok) {
                    final CTTextParagraphProperties defaultProps = this.getDefaultMasterStyle();
                    if (defaultProps != null) {
                        ok = visitor.fetch(defaultProps);
                    }
                }
            }
        }
        return ok;
    }
    
    void copy(final XSLFTextParagraph p) {
        final TextAlign srcAlign = p.getTextAlign();
        if (srcAlign != this.getTextAlign()) {
            this.setTextAlign(srcAlign);
        }
        final boolean isBullet = p.isBullet();
        if (isBullet != this.isBullet()) {
            this.setBullet(isBullet);
            if (isBullet) {
                final String buFont = p.getBulletFont();
                if (buFont != null && !buFont.equals(this.getBulletFont())) {
                    this.setBulletFont(buFont);
                }
                final String buChar = p.getBulletCharacter();
                if (buChar != null && !buChar.equals(this.getBulletCharacter())) {
                    this.setBulletCharacter(buChar);
                }
                final Color buColor = p.getBulletFontColor();
                if (buColor != null && !buColor.equals(this.getBulletFontColor())) {
                    this.setBulletFontColor(buColor);
                }
                final double buSize = p.getBulletFontSize();
                if (buSize != this.getBulletFontSize()) {
                    this.setBulletFontSize(buSize);
                }
            }
        }
        final double leftMargin = p.getLeftMargin();
        if (leftMargin != this.getLeftMargin()) {
            this.setLeftMargin(leftMargin);
        }
        final double indent = p.getIndent();
        if (indent != this.getIndent()) {
            this.setIndent(indent);
        }
        final double spaceAfter = p.getSpaceAfter();
        if (spaceAfter != this.getSpaceAfter()) {
            this.setSpaceAfter(spaceAfter);
        }
        final double spaceBefore = p.getSpaceBefore();
        if (spaceBefore != this.getSpaceBefore()) {
            this.setSpaceBefore(spaceBefore);
        }
        final double lineSpacing = p.getLineSpacing();
        if (lineSpacing != this.getLineSpacing()) {
            this.setLineSpacing(lineSpacing);
        }
        final List<XSLFTextRun> srcR = p.getTextRuns();
        final List<XSLFTextRun> tgtR = this.getTextRuns();
        for (int i = 0; i < srcR.size(); ++i) {
            final XSLFTextRun r1 = srcR.get(i);
            final XSLFTextRun r2 = tgtR.get(i);
            r2.copy(r1);
        }
    }
}
