// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.xslf.usermodel;

import java.text.AttributedCharacterIterator;
import java.awt.RenderingHints;
import java.awt.Graphics2D;
import java.text.AttributedString;
import java.awt.font.TextLayout;

class TextFragment
{
    final TextLayout _layout;
    final AttributedString _str;
    
    TextFragment(final TextLayout layout, final AttributedString str) {
        this._layout = layout;
        this._str = str;
    }
    
    void draw(final Graphics2D graphics, final double x, final double y) {
        if (this._str == null) {
            return;
        }
        final double yBaseline = y + this._layout.getAscent();
        final Integer textMode = (Integer)graphics.getRenderingHint(XSLFRenderingHint.TEXT_RENDERING_MODE);
        if (textMode != null && textMode == 2) {
            this._layout.draw(graphics, (float)x, (float)yBaseline);
        }
        else {
            graphics.drawString(this._str.getIterator(), (float)x, (float)yBaseline);
        }
    }
    
    public float getHeight() {
        final double h = Math.ceil(this._layout.getAscent()) + Math.ceil(this._layout.getDescent()) + this._layout.getLeading();
        return (float)h;
    }
    
    public float getWidth() {
        return this._layout.getAdvance();
    }
    
    public String getString() {
        if (this._str == null) {
            return "";
        }
        final AttributedCharacterIterator it = this._str.getIterator();
        final StringBuffer buf = new StringBuffer();
        for (char c = it.first(); c != '\uffff'; c = it.next()) {
            buf.append(c);
        }
        return buf.toString();
    }
    
    @Override
    public String toString() {
        return "[" + this.getClass().getSimpleName() + "] " + this.getString();
    }
}
