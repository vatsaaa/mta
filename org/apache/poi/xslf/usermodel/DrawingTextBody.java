// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.xslf.usermodel;

import java.util.List;
import org.openxmlformats.schemas.drawingml.x2006.main.CTTextParagraph;
import org.openxmlformats.schemas.drawingml.x2006.main.CTTextBody;

public class DrawingTextBody
{
    private final CTTextBody textBody;
    
    public DrawingTextBody(final CTTextBody textBody) {
        this.textBody = textBody;
    }
    
    public DrawingParagraph[] getParagraphs() {
        final List<CTTextParagraph> paragraphs = this.textBody.getPList();
        final DrawingParagraph[] o = new DrawingParagraph[paragraphs.size()];
        for (int i = 0; i < o.length; ++i) {
            o[i] = new DrawingParagraph(paragraphs.get(i));
        }
        return o;
    }
}
