// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.xslf.usermodel;

import org.apache.xmlbeans.XmlObject;
import org.openxmlformats.schemas.drawingml.x2006.main.CTBlip;
import java.awt.Graphics2D;
import org.openxmlformats.schemas.presentationml.x2006.main.CTBackground;
import java.util.Iterator;
import org.apache.poi.POIXMLDocumentPart;
import org.openxmlformats.schemas.drawingml.x2006.main.CTPositiveSize2D;
import org.openxmlformats.schemas.drawingml.x2006.main.CTPoint2D;
import org.openxmlformats.schemas.drawingml.x2006.main.CTGroupTransform2D;
import org.openxmlformats.schemas.drawingml.x2006.main.CTGroupShapeProperties;
import org.openxmlformats.schemas.drawingml.x2006.main.CTNonVisualDrawingProps;
import org.openxmlformats.schemas.presentationml.x2006.main.CTGroupShapeNonVisual;
import org.openxmlformats.schemas.presentationml.x2006.main.CTGroupShape;
import org.openxmlformats.schemas.presentationml.x2006.main.CTCommonSlideData;
import org.apache.xmlbeans.XmlException;
import java.io.IOException;
import org.openxmlformats.schemas.presentationml.x2006.main.SldDocument;
import org.apache.poi.openxml4j.opc.PackageRelationship;
import org.apache.poi.openxml4j.opc.PackagePart;
import org.openxmlformats.schemas.presentationml.x2006.main.CTSlide;

public final class XSLFSlide extends XSLFSheet
{
    private final CTSlide _slide;
    private XSLFSlideLayout _layout;
    private XSLFComments _comments;
    private XSLFNotes _notes;
    
    XSLFSlide() {
        this._slide = prototype();
        this.setCommonSlideData(this._slide.getCSld());
    }
    
    XSLFSlide(final PackagePart part, final PackageRelationship rel) throws IOException, XmlException {
        super(part, rel);
        final SldDocument doc = SldDocument.Factory.parse(this.getPackagePart().getInputStream());
        this._slide = doc.getSld();
        this.setCommonSlideData(this._slide.getCSld());
    }
    
    private static CTSlide prototype() {
        final CTSlide ctSlide = CTSlide.Factory.newInstance();
        final CTCommonSlideData cSld = ctSlide.addNewCSld();
        final CTGroupShape spTree = cSld.addNewSpTree();
        final CTGroupShapeNonVisual nvGrpSpPr = spTree.addNewNvGrpSpPr();
        final CTNonVisualDrawingProps cnvPr = nvGrpSpPr.addNewCNvPr();
        cnvPr.setId(1L);
        cnvPr.setName("");
        nvGrpSpPr.addNewCNvGrpSpPr();
        nvGrpSpPr.addNewNvPr();
        final CTGroupShapeProperties grpSpr = spTree.addNewGrpSpPr();
        final CTGroupTransform2D xfrm = grpSpr.addNewXfrm();
        final CTPoint2D off = xfrm.addNewOff();
        off.setX(0L);
        off.setY(0L);
        final CTPositiveSize2D ext = xfrm.addNewExt();
        ext.setCx(0L);
        ext.setCy(0L);
        final CTPoint2D choff = xfrm.addNewChOff();
        choff.setX(0L);
        choff.setY(0L);
        final CTPositiveSize2D chExt = xfrm.addNewChExt();
        chExt.setCx(0L);
        chExt.setCy(0L);
        ctSlide.addNewClrMapOvr().addNewMasterClrMapping();
        return ctSlide;
    }
    
    @Override
    public CTSlide getXmlObject() {
        return this._slide;
    }
    
    @Override
    protected String getRootElementName() {
        return "sld";
    }
    
    @Override
    public XSLFSlideLayout getMasterSheet() {
        return this.getSlideLayout();
    }
    
    public XSLFSlideLayout getSlideLayout() {
        if (this._layout == null) {
            for (final POIXMLDocumentPart p : this.getRelations()) {
                if (p instanceof XSLFSlideLayout) {
                    this._layout = (XSLFSlideLayout)p;
                }
            }
        }
        if (this._layout == null) {
            throw new IllegalArgumentException("SlideLayout was not found for " + this.toString());
        }
        return this._layout;
    }
    
    public XSLFSlideMaster getSlideMaster() {
        return this.getSlideLayout().getSlideMaster();
    }
    
    public XSLFComments getComments() {
        if (this._comments == null) {
            for (final POIXMLDocumentPart p : this.getRelations()) {
                if (p instanceof XSLFComments) {
                    this._comments = (XSLFComments)p;
                }
            }
        }
        if (this._comments == null) {
            return null;
        }
        return this._comments;
    }
    
    public XSLFNotes getNotes() {
        if (this._notes == null) {
            for (final POIXMLDocumentPart p : this.getRelations()) {
                if (p instanceof XSLFNotes) {
                    this._notes = (XSLFNotes)p;
                }
            }
        }
        if (this._notes == null) {
            return null;
        }
        return this._notes;
    }
    
    public String getTitle() {
        final XSLFTextShape txt = this.getTextShapeByType(Placeholder.TITLE);
        return (txt == null) ? "" : txt.getText();
    }
    
    public XSLFTheme getTheme() {
        return this.getSlideLayout().getSlideMaster().getTheme();
    }
    
    @Override
    public XSLFBackground getBackground() {
        final CTBackground bg = this._slide.getCSld().getBg();
        if (bg != null) {
            return new XSLFBackground(bg, this);
        }
        return this.getMasterSheet().getBackground();
    }
    
    @Override
    public boolean getFollowMasterGraphics() {
        return !this._slide.isSetShowMasterSp() || this._slide.getShowMasterSp();
    }
    
    public void setFollowMasterGraphics(final boolean value) {
        this._slide.setShowMasterSp(value);
    }
    
    @Override
    public void draw(final Graphics2D graphics) {
        final XSLFBackground bg = this.getBackground();
        if (bg != null) {
            bg.draw(graphics);
        }
        super.draw(graphics);
    }
    
    @Override
    public XSLFSlide importContent(final XSLFSheet src) {
        super.importContent(src);
        final XSLFBackground bgShape = this.getBackground();
        if (bgShape != null) {
            final CTBackground bg = (CTBackground)bgShape.getXmlObject();
            if (bg.isSetBgPr() && bg.getBgPr().isSetBlipFill()) {
                final CTBlip blip = bg.getBgPr().getBlipFill().getBlip();
                final String blipId = blip.getEmbed();
                final String relId = this.importBlip(blipId, src.getPackagePart());
                blip.setEmbed(relId);
            }
        }
        return this;
    }
}
