// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.xslf.usermodel;

import org.apache.xmlbeans.XmlCursor;
import org.openxmlformats.schemas.drawingml.x2006.main.CTGraphicalObjectData;
import org.openxmlformats.schemas.drawingml.x2006.main.CTNonVisualDrawingProps;
import org.openxmlformats.schemas.presentationml.x2006.main.CTGraphicalObjectFrameNonVisual;
import javax.xml.namespace.QName;
import java.util.Collections;
import org.apache.poi.util.Units;
import org.apache.poi.util.Internal;
import java.util.Iterator;
import org.apache.xmlbeans.XmlObject;
import org.openxmlformats.schemas.drawingml.x2006.main.CTTableRow;
import java.util.ArrayList;
import org.apache.xmlbeans.XmlException;
import org.apache.poi.POIXMLException;
import org.apache.xmlbeans.impl.values.XmlAnyTypeImpl;
import org.openxmlformats.schemas.presentationml.x2006.main.CTGraphicalObjectFrame;
import java.util.List;
import org.openxmlformats.schemas.drawingml.x2006.main.CTTable;

public class XSLFTable extends XSLFGraphicFrame implements Iterable<XSLFTableRow>
{
    static String TABLE_URI;
    private CTTable _table;
    private List<XSLFTableRow> _rows;
    
    XSLFTable(final CTGraphicalObjectFrame shape, final XSLFSheet sheet) {
        super(shape, sheet);
        final XmlObject[] rs = shape.getGraphic().getGraphicData().selectPath("declare namespace a='http://schemas.openxmlformats.org/drawingml/2006/main' ./a:tbl");
        if (rs.length == 0) {
            throw new IllegalStateException("a:tbl element was not found in\n " + shape.getGraphic().getGraphicData());
        }
        if (rs[0] instanceof XmlAnyTypeImpl) {
            try {
                rs[0] = CTTable.Factory.parse(rs[0].toString());
            }
            catch (XmlException e) {
                throw new POIXMLException(e);
            }
        }
        this._table = (CTTable)rs[0];
        this._rows = new ArrayList<XSLFTableRow>(this._table.sizeOfTrArray());
        for (final CTTableRow row : this._table.getTrList()) {
            this._rows.add(new XSLFTableRow(row, this));
        }
    }
    
    @Internal
    public CTTable getCTTable() {
        return this._table;
    }
    
    public int getNumberOfColumns() {
        return this._table.getTblGrid().sizeOfGridColArray();
    }
    
    public int getNumberOfRows() {
        return this._table.sizeOfTrArray();
    }
    
    public double getColumnWidth(final int idx) {
        return Units.toPoints(this._table.getTblGrid().getGridColArray(idx).getW());
    }
    
    public void setColumnWidth(final int idx, final double width) {
        this._table.getTblGrid().getGridColArray(idx).setW(Units.toEMU(width));
    }
    
    public Iterator<XSLFTableRow> iterator() {
        return this._rows.iterator();
    }
    
    public List<XSLFTableRow> getRows() {
        return Collections.unmodifiableList((List<? extends XSLFTableRow>)this._rows);
    }
    
    public XSLFTableRow addRow() {
        final CTTableRow tr = this._table.addNewTr();
        final XSLFTableRow row = new XSLFTableRow(tr, this);
        row.setHeight(20.0);
        this._rows.add(row);
        return row;
    }
    
    static CTGraphicalObjectFrame prototype(final int shapeId) {
        final CTGraphicalObjectFrame frame = CTGraphicalObjectFrame.Factory.newInstance();
        final CTGraphicalObjectFrameNonVisual nvGr = frame.addNewNvGraphicFramePr();
        final CTNonVisualDrawingProps cnv = nvGr.addNewCNvPr();
        cnv.setName("Table " + shapeId);
        cnv.setId(shapeId + 1);
        nvGr.addNewCNvGraphicFramePr().addNewGraphicFrameLocks().setNoGrp(true);
        nvGr.addNewNvPr();
        frame.addNewXfrm();
        final CTGraphicalObjectData gr = frame.addNewGraphic().addNewGraphicData();
        final XmlCursor cursor = gr.newCursor();
        cursor.toNextToken();
        cursor.beginElement(new QName("http://schemas.openxmlformats.org/drawingml/2006/main", "tbl"));
        cursor.beginElement(new QName("http://schemas.openxmlformats.org/drawingml/2006/main", "tblPr"));
        cursor.toNextToken();
        cursor.beginElement(new QName("http://schemas.openxmlformats.org/drawingml/2006/main", "tblGrid"));
        cursor.dispose();
        gr.setUri(XSLFTable.TABLE_URI);
        return frame;
    }
    
    public void mergeCells(final int firstRow, final int lastRow, final int firstCol, final int lastCol) {
        if (firstRow > lastRow) {
            throw new IllegalArgumentException("Cannot merge, first row > last row : " + firstRow + " > " + lastRow);
        }
        if (firstCol > lastCol) {
            throw new IllegalArgumentException("Cannot merge, first column > last column : " + firstCol + " > " + lastCol);
        }
        final int rowSpan = lastRow - firstRow + 1;
        final boolean mergeRowRequired = rowSpan > 1;
        final int colSpan = lastCol - firstCol + 1;
        final boolean mergeColumnRequired = colSpan > 1;
        for (int i = firstRow; i <= lastRow; ++i) {
            final XSLFTableRow row = this._rows.get(i);
            for (int colPos = firstCol; colPos <= lastCol; ++colPos) {
                final XSLFTableCell cell = row.getCells().get(colPos);
                if (mergeRowRequired) {
                    if (i == firstRow) {
                        cell.setRowSpan(rowSpan);
                    }
                    else {
                        cell.setVMerge(true);
                    }
                }
                if (mergeColumnRequired) {
                    if (colPos == firstCol) {
                        cell.setGridSpan(colSpan);
                    }
                    else {
                        cell.setHMerge(true);
                    }
                }
            }
        }
    }
    
    static {
        XSLFTable.TABLE_URI = "http://schemas.openxmlformats.org/drawingml/2006/table";
    }
}
