// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.xslf.usermodel;

import org.openxmlformats.schemas.presentationml.x2006.main.CTGraphicalObjectFrame;
import org.openxmlformats.schemas.presentationml.x2006.main.CTPicture;
import org.openxmlformats.schemas.presentationml.x2006.main.CTConnector;
import java.awt.Color;
import org.openxmlformats.schemas.presentationml.x2006.main.CTShape;
import java.awt.geom.Rectangle2D;
import java.awt.Rectangle;
import org.apache.xmlbeans.XmlObject;
import org.openxmlformats.schemas.drawingml.x2006.main.CTNonVisualDrawingProps;
import org.openxmlformats.schemas.presentationml.x2006.main.CTGroupShape;

public class XSLFDrawing
{
    private XSLFSheet _sheet;
    private int _shapeId;
    private CTGroupShape _spTree;
    
    XSLFDrawing(final XSLFSheet sheet, final CTGroupShape spTree) {
        this._shapeId = 1;
        this._sheet = sheet;
        this._spTree = spTree;
        final XmlObject[] arr$;
        final XmlObject[] cNvPr = arr$ = sheet.getSpTree().selectPath("declare namespace p='http://schemas.openxmlformats.org/presentationml/2006/main' .//*/p:cNvPr");
        for (final XmlObject o : arr$) {
            final CTNonVisualDrawingProps p = (CTNonVisualDrawingProps)o;
            this._shapeId = (int)Math.max(this._shapeId, p.getId());
        }
    }
    
    public XSLFAutoShape createAutoShape() {
        final CTShape sp = this._spTree.addNewSp();
        sp.set(XSLFAutoShape.prototype(this._shapeId++));
        final XSLFAutoShape shape = new XSLFAutoShape(sp, this._sheet);
        shape.setAnchor(new Rectangle());
        return shape;
    }
    
    public XSLFFreeformShape createFreeform() {
        final CTShape sp = this._spTree.addNewSp();
        sp.set(XSLFFreeformShape.prototype(this._shapeId++));
        final XSLFFreeformShape shape = new XSLFFreeformShape(sp, this._sheet);
        shape.setAnchor(new Rectangle());
        return shape;
    }
    
    public XSLFTextBox createTextBox() {
        final CTShape sp = this._spTree.addNewSp();
        sp.set(XSLFTextBox.prototype(this._shapeId++));
        final XSLFTextBox shape = new XSLFTextBox(sp, this._sheet);
        shape.setAnchor(new Rectangle());
        return shape;
    }
    
    public XSLFConnectorShape createConnector() {
        final CTConnector sp = this._spTree.addNewCxnSp();
        sp.set(XSLFConnectorShape.prototype(this._shapeId++));
        final XSLFConnectorShape shape = new XSLFConnectorShape(sp, this._sheet);
        shape.setAnchor(new Rectangle());
        shape.setLineColor(Color.black);
        shape.setLineWidth(0.75);
        return shape;
    }
    
    public XSLFGroupShape createGroup() {
        final CTGroupShape obj = this._spTree.addNewGrpSp();
        obj.set(XSLFGroupShape.prototype(this._shapeId++));
        final XSLFGroupShape shape = new XSLFGroupShape(obj, this._sheet);
        shape.setAnchor(new Rectangle());
        return shape;
    }
    
    public XSLFPictureShape createPicture(final String rel) {
        final CTPicture obj = this._spTree.addNewPic();
        obj.set(XSLFPictureShape.prototype(this._shapeId++, rel));
        final XSLFPictureShape shape = new XSLFPictureShape(obj, this._sheet);
        shape.setAnchor(new Rectangle());
        return shape;
    }
    
    public XSLFTable createTable() {
        final CTGraphicalObjectFrame obj = this._spTree.addNewGraphicFrame();
        obj.set(XSLFTable.prototype(this._shapeId++));
        final XSLFTable shape = new XSLFTable(obj, this._sheet);
        shape.setAnchor(new Rectangle());
        return shape;
    }
}
