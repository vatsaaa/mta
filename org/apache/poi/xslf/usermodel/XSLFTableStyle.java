// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.xslf.usermodel;

import org.openxmlformats.schemas.drawingml.x2006.main.CTTableStyle;

public class XSLFTableStyle
{
    private CTTableStyle _tblStyle;
    
    XSLFTableStyle(final CTTableStyle style) {
        this._tblStyle = style;
    }
    
    public CTTableStyle getXmlObject() {
        return this._tblStyle;
    }
    
    public String getStyleName() {
        return this._tblStyle.getStyleName();
    }
    
    public String getStyleId() {
        return this._tblStyle.getStyleId();
    }
}
