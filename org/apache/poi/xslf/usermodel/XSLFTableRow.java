// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.xslf.usermodel;

import org.apache.xmlbeans.XmlObject;
import org.apache.poi.util.Units;
import java.util.Collections;
import java.util.Iterator;
import org.openxmlformats.schemas.drawingml.x2006.main.CTTableCell;
import java.util.ArrayList;
import java.util.List;
import org.openxmlformats.schemas.drawingml.x2006.main.CTTableRow;

public class XSLFTableRow implements Iterable<XSLFTableCell>
{
    private CTTableRow _row;
    private List<XSLFTableCell> _cells;
    private XSLFTable _table;
    
    XSLFTableRow(final CTTableRow row, final XSLFTable table) {
        this._row = row;
        this._table = table;
        this._cells = new ArrayList<XSLFTableCell>(this._row.sizeOfTcArray());
        for (final CTTableCell cell : this._row.getTcList()) {
            this._cells.add(new XSLFTableCell(cell, table.getSheet()));
        }
    }
    
    public CTTableRow getXmlObject() {
        return this._row;
    }
    
    public Iterator<XSLFTableCell> iterator() {
        return this._cells.iterator();
    }
    
    public List<XSLFTableCell> getCells() {
        return Collections.unmodifiableList((List<? extends XSLFTableCell>)this._cells);
    }
    
    public double getHeight() {
        return Units.toPoints(this._row.getH());
    }
    
    public void setHeight(final double height) {
        this._row.setH(Units.toEMU(height));
    }
    
    public XSLFTableCell addCell() {
        final CTTableCell c = this._row.addNewTc();
        c.set(XSLFTableCell.prototype());
        final XSLFTableCell cell = new XSLFTableCell(c, this._table.getSheet());
        this._cells.add(cell);
        if (this._table.getNumberOfColumns() < this._row.sizeOfTcArray()) {
            this._table.getCTTable().getTblGrid().addNewGridCol().setW(Units.toEMU(100.0));
        }
        return cell;
    }
}
