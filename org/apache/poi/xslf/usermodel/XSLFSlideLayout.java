// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.xslf.usermodel;

import org.apache.xmlbeans.XmlObject;
import org.openxmlformats.schemas.presentationml.x2006.main.CTBackground;
import org.openxmlformats.schemas.presentationml.x2006.main.CTPlaceholder;
import java.util.Iterator;
import org.apache.poi.POIXMLDocumentPart;
import org.apache.poi.util.Internal;
import org.apache.xmlbeans.XmlException;
import java.io.IOException;
import org.openxmlformats.schemas.presentationml.x2006.main.SldLayoutDocument;
import org.apache.poi.openxml4j.opc.PackageRelationship;
import org.apache.poi.openxml4j.opc.PackagePart;
import org.openxmlformats.schemas.presentationml.x2006.main.CTSlideLayout;

public class XSLFSlideLayout extends XSLFSheet
{
    private CTSlideLayout _layout;
    private XSLFSlideMaster _master;
    
    XSLFSlideLayout() {
        this._layout = CTSlideLayout.Factory.newInstance();
    }
    
    public XSLFSlideLayout(final PackagePart part, final PackageRelationship rel) throws IOException, XmlException {
        super(part, rel);
        final SldLayoutDocument doc = SldLayoutDocument.Factory.parse(this.getPackagePart().getInputStream());
        this._layout = doc.getSldLayout();
        this.setCommonSlideData(this._layout.getCSld());
    }
    
    public String getName() {
        return this._layout.getCSld().getName();
    }
    
    @Internal
    @Override
    public CTSlideLayout getXmlObject() {
        return this._layout;
    }
    
    @Override
    protected String getRootElementName() {
        return "sldLayout";
    }
    
    public XSLFSlideMaster getSlideMaster() {
        if (this._master == null) {
            for (final POIXMLDocumentPart p : this.getRelations()) {
                if (p instanceof XSLFSlideMaster) {
                    this._master = (XSLFSlideMaster)p;
                }
            }
        }
        if (this._master == null) {
            throw new IllegalStateException("SlideMaster was not found for " + this.toString());
        }
        return this._master;
    }
    
    @Override
    public XSLFSlideMaster getMasterSheet() {
        return this.getSlideMaster();
    }
    
    public XSLFTheme getTheme() {
        return this.getSlideMaster().getTheme();
    }
    
    @Override
    public boolean getFollowMasterGraphics() {
        return !this._layout.isSetShowMasterSp() || this._layout.getShowMasterSp();
    }
    
    @Override
    protected boolean canDraw(final XSLFShape shape) {
        if (shape instanceof XSLFSimpleShape) {
            final XSLFSimpleShape txt = (XSLFSimpleShape)shape;
            final CTPlaceholder ph = txt.getCTPlaceholder();
            if (ph != null) {
                return false;
            }
        }
        return true;
    }
    
    @Override
    public XSLFBackground getBackground() {
        final CTBackground bg = this._layout.getCSld().getBg();
        if (bg != null) {
            return new XSLFBackground(bg, this);
        }
        return this.getMasterSheet().getBackground();
    }
    
    public void copyLayout(final XSLFSlide slide) {
        for (final XSLFShape sh : this.getShapes()) {
            if (sh instanceof XSLFTextShape) {
                final XSLFTextShape tsh = (XSLFTextShape)sh;
                final Placeholder ph = tsh.getTextType();
                if (ph != null) {
                    switch (ph) {
                        case DATETIME:
                        case SLIDE_NUMBER:
                        case FOOTER: {
                            break;
                        }
                        default: {
                            slide.getSpTree().addNewSp().set(tsh.getXmlObject().copy());
                            break;
                        }
                    }
                }
            }
        }
    }
    
    public SlideLayout getType() {
        final int ordinal = this._layout.getType().intValue() - 1;
        return SlideLayout.values()[ordinal];
    }
}
