// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.xslf.usermodel;

import java.net.URI;
import org.apache.poi.openxml4j.opc.TargetMode;
import org.apache.poi.openxml4j.opc.PackageRelationship;
import org.apache.poi.util.Internal;
import org.openxmlformats.schemas.drawingml.x2006.main.CTHyperlink;

public class XSLFHyperlink
{
    final XSLFTextRun _r;
    final CTHyperlink _link;
    
    XSLFHyperlink(final CTHyperlink link, final XSLFTextRun r) {
        this._r = r;
        this._link = link;
    }
    
    @Internal
    public CTHyperlink getXmlObject() {
        return this._link;
    }
    
    public void setAddress(final String address) {
        final XSLFSheet sheet = this._r.getParentParagraph().getParentShape().getSheet();
        final PackageRelationship rel = sheet.getPackagePart().addExternalRelationship(address, XSLFRelation.HYPERLINK.getRelation());
        this._link.setId(rel.getId());
    }
    
    public void setAddress(final XSLFSlide slide) {
        final XSLFSheet sheet = this._r.getParentParagraph().getParentShape().getSheet();
        final PackageRelationship rel = sheet.getPackagePart().addRelationship(slide.getPackagePart().getPartName(), TargetMode.INTERNAL, XSLFRelation.SLIDE.getRelation());
        this._link.setId(rel.getId());
        this._link.setAction("ppaction://hlinksldjump");
    }
    
    @Internal
    public URI getTargetURI() {
        final XSLFSheet sheet = this._r.getParentParagraph().getParentShape().getSheet();
        final String id = this._link.getId();
        return sheet.getPackagePart().getRelationship(id).getTargetURI();
    }
}
