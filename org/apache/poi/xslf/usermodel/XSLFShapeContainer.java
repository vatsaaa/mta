// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.xslf.usermodel;

public interface XSLFShapeContainer extends Iterable<XSLFShape>
{
    XSLFAutoShape createAutoShape();
    
    XSLFFreeformShape createFreeform();
    
    XSLFTextBox createTextBox();
    
    XSLFConnectorShape createConnector();
    
    XSLFGroupShape createGroup();
    
    XSLFPictureShape createPicture(final int p0);
    
    XSLFShape[] getShapes();
    
    boolean removeShape(final XSLFShape p0);
    
    void clear();
}
