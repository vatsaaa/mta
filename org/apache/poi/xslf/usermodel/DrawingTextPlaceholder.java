// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.xslf.usermodel;

import org.openxmlformats.schemas.presentationml.x2006.main.STPlaceholderType;
import org.openxmlformats.schemas.drawingml.x2006.main.CTTextBody;
import org.openxmlformats.schemas.presentationml.x2006.main.CTPlaceholder;

public class DrawingTextPlaceholder extends DrawingTextBody
{
    private final CTPlaceholder placeholder;
    
    public DrawingTextPlaceholder(final CTTextBody textBody, final CTPlaceholder placeholder) {
        super(textBody);
        this.placeholder = placeholder;
    }
    
    public String getPlaceholderType() {
        return this.placeholder.getType().toString();
    }
    
    public STPlaceholderType.Enum getPlaceholderTypeEnum() {
        return this.placeholder.getType();
    }
    
    public boolean isPlaceholderCustom() {
        return this.placeholder.getHasCustomPrompt();
    }
}
