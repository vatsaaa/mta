// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.xslf.usermodel;

public enum Placeholder
{
    TITLE, 
    BODY, 
    CENTERED_TITLE, 
    SUBTITLE, 
    DATETIME, 
    SLIDE_NUMBER, 
    FOOTER, 
    HEADER, 
    CONTENT, 
    CHART, 
    TABLE, 
    CLIP_ART, 
    DGM, 
    MEDIA, 
    SLIDE_IMAGE, 
    PICTURE;
}
