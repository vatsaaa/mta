// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.xslf.usermodel;

import org.apache.poi.util.Internal;
import java.awt.Shape;
import java.awt.RenderingHints;
import java.awt.geom.AffineTransform;
import java.awt.Graphics2D;
import org.apache.xmlbeans.XmlObject;
import java.awt.geom.Rectangle2D;

public abstract class XSLFShape
{
    public abstract Rectangle2D getAnchor();
    
    public abstract void setAnchor(final Rectangle2D p0);
    
    public abstract XmlObject getXmlObject();
    
    public abstract String getShapeName();
    
    public abstract int getShapeId();
    
    public abstract void setRotation(final double p0);
    
    public abstract double getRotation();
    
    public abstract void setFlipHorizontal(final boolean p0);
    
    public abstract void setFlipVertical(final boolean p0);
    
    public abstract boolean getFlipHorizontal();
    
    public abstract boolean getFlipVertical();
    
    public abstract void draw(final Graphics2D p0);
    
    protected void applyTransform(final Graphics2D graphics) {
        Rectangle2D anchor = this.getAnchor();
        final AffineTransform tx = (AffineTransform)graphics.getRenderingHint(XSLFRenderingHint.GROUP_TRANSFORM);
        if (tx != null) {
            anchor = tx.createTransformedShape(anchor).getBounds2D();
        }
        final double rotation = this.getRotation();
        if (rotation != 0.0) {
            final double centerX = anchor.getX() + anchor.getWidth() / 2.0;
            final double centerY = anchor.getY() + anchor.getHeight() / 2.0;
            graphics.translate(centerX, centerY);
            graphics.rotate(Math.toRadians(rotation));
            graphics.translate(-centerX, -centerY);
        }
        if (this.getFlipHorizontal()) {
            graphics.translate(anchor.getX() + anchor.getWidth(), anchor.getY());
            graphics.scale(-1.0, 1.0);
            graphics.translate(-anchor.getX(), -anchor.getY());
        }
        if (this.getFlipVertical()) {
            graphics.translate(anchor.getX(), anchor.getY() + anchor.getHeight());
            graphics.scale(1.0, -1.0);
            graphics.translate(-anchor.getX(), -anchor.getY());
        }
    }
    
    @Internal
    void copy(final XSLFShape sh) {
        if (!this.getClass().isInstance(sh)) {
            throw new IllegalArgumentException("Can't copy " + sh.getClass().getSimpleName() + " into " + this.getClass().getSimpleName());
        }
        this.setAnchor(sh.getAnchor());
    }
}
