// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.xslf.usermodel;

public enum TextAlign
{
    LEFT, 
    CENTER, 
    RIGHT, 
    JUSTIFY, 
    JUSTIFY_LOW, 
    DIST, 
    THAI_DIST;
}
