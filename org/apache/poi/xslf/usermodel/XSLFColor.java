// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.xslf.usermodel;

import java.util.HashMap;
import org.w3c.dom.Node;
import org.openxmlformats.schemas.drawingml.x2006.main.CTColor;
import org.openxmlformats.schemas.drawingml.x2006.main.CTSystemColor;
import org.openxmlformats.schemas.drawingml.x2006.main.CTSRgbColor;
import org.openxmlformats.schemas.drawingml.x2006.main.CTScRgbColor;
import org.openxmlformats.schemas.drawingml.x2006.main.CTPresetColor;
import org.openxmlformats.schemas.drawingml.x2006.main.CTHslColor;
import java.util.Map;
import org.openxmlformats.schemas.drawingml.x2006.main.CTSchemeColor;
import java.awt.Color;
import org.apache.xmlbeans.XmlObject;
import org.apache.poi.util.Internal;

@Internal
public class XSLFColor
{
    private XmlObject _xmlObject;
    private Color _color;
    private CTSchemeColor _phClr;
    static final Map<String, Color> presetColors;
    
    public XSLFColor(final XmlObject obj, final XSLFTheme theme, final CTSchemeColor phClr) {
        this._xmlObject = obj;
        this._phClr = phClr;
        this._color = this.toColor(obj, theme);
    }
    
    @Internal
    public XmlObject getXmlObject() {
        return this._xmlObject;
    }
    
    public Color getColor() {
        return (this._color == null) ? null : this.applyColorTransform(this._color);
    }
    
    private Color applyColorTransform(final Color color) {
        Color result = color;
        final int alpha = this.getAlpha();
        if (alpha != -1) {
            result = new Color(result.getRed(), result.getGreen(), result.getBlue(), Math.round(255 * alpha * 0.01f));
        }
        final int lumOff = this.getLumOff();
        final int lumMod = this.getLumMod();
        if (lumMod != -1 || lumOff != -1) {
            result = modulateLuminanace(result, (lumMod == -1) ? 100 : lumMod, (lumOff == -1) ? 0 : lumOff);
        }
        final int shade = this.getShade();
        if (shade != -1) {
            result = shade(result, shade);
        }
        final int tint = this.getTint();
        if (tint != -1) {
            result = tint(result, tint);
        }
        return result;
    }
    
    Color toColor(final XmlObject obj, final XSLFTheme theme) {
        Color color = null;
        for (final XmlObject ch : obj.selectPath("*")) {
            if (ch instanceof CTHslColor) {
                final CTHslColor hsl = (CTHslColor)ch;
                final int h = hsl.getHue2();
                final int s = hsl.getSat2();
                final int l = hsl.getLum2();
                color = Color.getHSBColor(h / 60000.0f, s / 100000.0f, l / 100000.0f);
            }
            else if (ch instanceof CTPresetColor) {
                final CTPresetColor prst = (CTPresetColor)ch;
                final String colorName = prst.getVal().toString();
                color = XSLFColor.presetColors.get(colorName);
            }
            else if (ch instanceof CTSchemeColor) {
                final CTSchemeColor schemeColor = (CTSchemeColor)ch;
                String colorRef = schemeColor.getVal().toString();
                if (this._phClr != null) {
                    colorRef = this._phClr.getVal().toString();
                }
                final CTColor ctColor = theme.getCTColor(colorRef);
                if (ctColor != null) {
                    color = this.toColor(ctColor, null);
                }
            }
            else if (ch instanceof CTScRgbColor) {
                final CTScRgbColor scrgb = (CTScRgbColor)ch;
                final int r = scrgb.getR();
                final int g = scrgb.getG();
                final int b = scrgb.getB();
                color = new Color(255 * r / 100000, 255 * g / 100000, 255 * b / 100000);
            }
            else if (ch instanceof CTSRgbColor) {
                final CTSRgbColor srgb = (CTSRgbColor)ch;
                final byte[] val = srgb.getVal();
                color = new Color(0xFF & val[0], 0xFF & val[1], 0xFF & val[2]);
            }
            else {
                if (!(ch instanceof CTSystemColor)) {
                    throw new IllegalArgumentException("Unexpected color choice: " + ch.getClass());
                }
                final CTSystemColor sys = (CTSystemColor)ch;
                if (sys.isSetLastClr()) {
                    final byte[] val = sys.getLastClr();
                    color = new Color(0xFF & val[0], 0xFF & val[1], 0xFF & val[2]);
                }
                else {
                    final String colorName = sys.getVal().toString();
                    color = Color.black;
                }
            }
        }
        return color;
    }
    
    private int getPercentageValue(final String elem) {
        final String query = "declare namespace a='http://schemas.openxmlformats.org/drawingml/2006/main' $this//a:" + elem;
        if (this._phClr != null) {
            final XmlObject[] obj = this._phClr.selectPath(query);
            if (obj.length == 1) {
                final Node attr = obj[0].getDomNode().getAttributes().getNamedItem("val");
                if (attr != null) {
                    return Integer.parseInt(attr.getNodeValue()) / 1000;
                }
            }
        }
        final XmlObject[] obj = this._xmlObject.selectPath(query);
        if (obj.length == 1) {
            final Node attr = obj[0].getDomNode().getAttributes().getNamedItem("val");
            if (attr != null) {
                return Integer.parseInt(attr.getNodeValue()) / 1000;
            }
        }
        return -1;
    }
    
    private int getAngleValue(final String elem) {
        final String color = "declare namespace a='http://schemas.openxmlformats.org/drawingml/2006/main' $this//a:" + elem;
        if (this._phClr != null) {
            final XmlObject[] obj = this._xmlObject.selectPath(color);
            if (obj.length == 1) {
                final Node attr = obj[0].getDomNode().getAttributes().getNamedItem("val");
                if (attr != null) {
                    return Integer.parseInt(attr.getNodeValue()) / 60000;
                }
            }
        }
        final XmlObject[] obj = this._xmlObject.selectPath(color);
        if (obj.length == 1) {
            final Node attr = obj[0].getDomNode().getAttributes().getNamedItem("val");
            if (attr != null) {
                return Integer.parseInt(attr.getNodeValue()) / 60000;
            }
        }
        return -1;
    }
    
    int getAlpha() {
        return this.getPercentageValue("alpha");
    }
    
    int getAlphaMod() {
        return this.getPercentageValue("alphaMod");
    }
    
    int getAlphaOff() {
        return this.getPercentageValue("alphaOff");
    }
    
    int getHue() {
        return this.getAngleValue("hue");
    }
    
    int getHueMod() {
        return this.getPercentageValue("hueMod");
    }
    
    int getHueOff() {
        return this.getPercentageValue("hueOff");
    }
    
    int getLum() {
        return this.getPercentageValue("lum");
    }
    
    int getLumMod() {
        return this.getPercentageValue("lumMod");
    }
    
    int getLumOff() {
        return this.getPercentageValue("lumOff");
    }
    
    int getSat() {
        return this.getPercentageValue("sat");
    }
    
    int getSatMod() {
        return this.getPercentageValue("satMod");
    }
    
    int getSatOff() {
        return this.getPercentageValue("satOff");
    }
    
    int getRed() {
        return this.getPercentageValue("red");
    }
    
    int getRedMod() {
        return this.getPercentageValue("redMod");
    }
    
    int getRedOff() {
        return this.getPercentageValue("redOff");
    }
    
    int getGreen() {
        return this.getPercentageValue("green");
    }
    
    int getGreenMod() {
        return this.getPercentageValue("greenMod");
    }
    
    int getGreenOff() {
        return this.getPercentageValue("greenOff");
    }
    
    int getBlue() {
        return this.getPercentageValue("blue");
    }
    
    int getBlueMod() {
        return this.getPercentageValue("blueMod");
    }
    
    int getBlueOff() {
        return this.getPercentageValue("blueOff");
    }
    
    int getShade() {
        return this.getPercentageValue("shade");
    }
    
    int getTint() {
        return this.getPercentageValue("tint");
    }
    
    private static Color modulateLuminanace(final Color c, final int lumMod, final int lumOff) {
        Color color;
        if (lumOff > 0) {
            color = new Color((int)Math.round((255 - c.getRed()) * (100.0 - lumMod) / 100.0 + c.getRed()), (int)Math.round((255 - c.getGreen()) * lumOff / 100.0 + c.getGreen()), (int)Math.round((255 - c.getBlue()) * lumOff / 100.0 + c.getBlue()), c.getAlpha());
        }
        else {
            color = new Color((int)Math.round(c.getRed() * lumMod / 100.0), (int)Math.round(c.getGreen() * lumMod / 100.0), (int)Math.round(c.getBlue() * lumMod / 100.0), c.getAlpha());
        }
        return color;
    }
    
    private static Color shade(final Color c, final int shade) {
        return new Color((int)(c.getRed() * shade * 0.01), (int)(c.getGreen() * shade * 0.01), (int)(c.getBlue() * shade * 0.01), c.getAlpha());
    }
    
    private static Color tint(final Color c, final int tint) {
        final int r = c.getRed();
        final int g = c.getGreen();
        final int b = c.getBlue();
        final float ftint = tint / 100.0f;
        final int red = Math.round(ftint * r + (1.0f - ftint) * 255.0f);
        final int green = Math.round(ftint * g + (1.0f - ftint) * 255.0f);
        final int blue = Math.round(ftint * b + (1.0f - ftint) * 255.0f);
        return new Color(red, green, blue);
    }
    
    static {
        (presetColors = new HashMap<String, Color>()).put("aliceBlue", new Color(240, 248, 255));
        XSLFColor.presetColors.put("antiqueWhite", new Color(250, 235, 215));
        XSLFColor.presetColors.put("aqua", new Color(0, 255, 255));
        XSLFColor.presetColors.put("aquamarine", new Color(127, 255, 212));
        XSLFColor.presetColors.put("azure", new Color(240, 255, 255));
        XSLFColor.presetColors.put("beige", new Color(245, 245, 220));
        XSLFColor.presetColors.put("bisque", new Color(255, 228, 196));
        XSLFColor.presetColors.put("black", new Color(0, 0, 0));
        XSLFColor.presetColors.put("blanchedAlmond", new Color(255, 235, 205));
        XSLFColor.presetColors.put("blue", new Color(0, 0, 255));
        XSLFColor.presetColors.put("blueViolet", new Color(138, 43, 226));
        XSLFColor.presetColors.put("brown", new Color(165, 42, 42));
        XSLFColor.presetColors.put("burlyWood", new Color(222, 184, 135));
        XSLFColor.presetColors.put("cadetBlue", new Color(95, 158, 160));
        XSLFColor.presetColors.put("chartreuse", new Color(127, 255, 0));
        XSLFColor.presetColors.put("chocolate", new Color(210, 105, 30));
        XSLFColor.presetColors.put("coral", new Color(255, 127, 80));
        XSLFColor.presetColors.put("cornflowerBlue", new Color(100, 149, 237));
        XSLFColor.presetColors.put("crimson", new Color(220, 20, 60));
        XSLFColor.presetColors.put("cyan", new Color(0, 255, 255));
        XSLFColor.presetColors.put("deepPink", new Color(255, 20, 147));
        XSLFColor.presetColors.put("deepSkyBlue", new Color(0, 191, 255));
        XSLFColor.presetColors.put("dimGray", new Color(105, 105, 105));
        XSLFColor.presetColors.put("dkBlue", new Color(0, 0, 139));
        XSLFColor.presetColors.put("dkCyan", new Color(0, 139, 139));
        XSLFColor.presetColors.put("dkGoldenrod", new Color(184, 134, 11));
        XSLFColor.presetColors.put("dkGray", new Color(169, 169, 169));
        XSLFColor.presetColors.put("dkGreen", new Color(0, 100, 0));
        XSLFColor.presetColors.put("dkKhaki", new Color(189, 183, 107));
        XSLFColor.presetColors.put("dkMagenta", new Color(139, 0, 139));
        XSLFColor.presetColors.put("dkOliveGreen", new Color(85, 107, 47));
        XSLFColor.presetColors.put("dkOrange", new Color(255, 140, 0));
        XSLFColor.presetColors.put("dkOrchid", new Color(153, 50, 204));
        XSLFColor.presetColors.put("dkRed", new Color(139, 0, 0));
        XSLFColor.presetColors.put("dkSalmon", new Color(233, 150, 122));
        XSLFColor.presetColors.put("dkSeaGreen", new Color(143, 188, 139));
        XSLFColor.presetColors.put("dkSlateBlue", new Color(72, 61, 139));
        XSLFColor.presetColors.put("dkSlateGray", new Color(47, 79, 79));
        XSLFColor.presetColors.put("dkTurquoise", new Color(0, 206, 209));
        XSLFColor.presetColors.put("dkViolet", new Color(148, 0, 211));
        XSLFColor.presetColors.put("dodgerBlue", new Color(30, 144, 255));
        XSLFColor.presetColors.put("firebrick", new Color(178, 34, 34));
        XSLFColor.presetColors.put("floralWhite", new Color(255, 250, 240));
        XSLFColor.presetColors.put("forestGreen", new Color(34, 139, 34));
        XSLFColor.presetColors.put("fuchsia", new Color(255, 0, 255));
        XSLFColor.presetColors.put("gainsboro", new Color(220, 220, 220));
        XSLFColor.presetColors.put("ghostWhite", new Color(248, 248, 255));
        XSLFColor.presetColors.put("gold", new Color(255, 215, 0));
        XSLFColor.presetColors.put("goldenrod", new Color(218, 165, 32));
        XSLFColor.presetColors.put("gray", new Color(128, 128, 128));
        XSLFColor.presetColors.put("green", new Color(0, 128, 0));
        XSLFColor.presetColors.put("greenYellow", new Color(173, 255, 47));
        XSLFColor.presetColors.put("honeydew", new Color(240, 255, 240));
        XSLFColor.presetColors.put("hotPink", new Color(255, 105, 180));
        XSLFColor.presetColors.put("indianRed", new Color(205, 92, 92));
        XSLFColor.presetColors.put("indigo", new Color(75, 0, 130));
        XSLFColor.presetColors.put("ivory", new Color(255, 255, 240));
        XSLFColor.presetColors.put("khaki", new Color(240, 230, 140));
        XSLFColor.presetColors.put("lavender", new Color(230, 230, 250));
        XSLFColor.presetColors.put("lavenderBlush", new Color(255, 240, 245));
        XSLFColor.presetColors.put("lawnGreen", new Color(124, 252, 0));
        XSLFColor.presetColors.put("lemonChiffon", new Color(255, 250, 205));
        XSLFColor.presetColors.put("lime", new Color(0, 255, 0));
        XSLFColor.presetColors.put("limeGreen", new Color(50, 205, 50));
        XSLFColor.presetColors.put("linen", new Color(250, 240, 230));
        XSLFColor.presetColors.put("ltBlue", new Color(173, 216, 230));
        XSLFColor.presetColors.put("ltCoral", new Color(240, 128, 128));
        XSLFColor.presetColors.put("ltCyan", new Color(224, 255, 255));
        XSLFColor.presetColors.put("ltGoldenrodYellow", new Color(250, 250, 120));
        XSLFColor.presetColors.put("ltGray", new Color(211, 211, 211));
        XSLFColor.presetColors.put("ltGreen", new Color(144, 238, 144));
        XSLFColor.presetColors.put("ltPink", new Color(255, 182, 193));
        XSLFColor.presetColors.put("ltSalmon", new Color(255, 160, 122));
        XSLFColor.presetColors.put("ltSeaGreen", new Color(32, 178, 170));
        XSLFColor.presetColors.put("ltSkyBlue", new Color(135, 206, 250));
        XSLFColor.presetColors.put("ltSlateGray", new Color(119, 136, 153));
        XSLFColor.presetColors.put("ltSteelBlue", new Color(176, 196, 222));
        XSLFColor.presetColors.put("ltYellow", new Color(255, 255, 224));
        XSLFColor.presetColors.put("magenta", new Color(255, 0, 255));
        XSLFColor.presetColors.put("maroon", new Color(128, 0, 0));
        XSLFColor.presetColors.put("medAquamarine", new Color(102, 205, 170));
        XSLFColor.presetColors.put("medBlue", new Color(0, 0, 205));
        XSLFColor.presetColors.put("medOrchid", new Color(186, 85, 211));
        XSLFColor.presetColors.put("medPurple", new Color(147, 112, 219));
        XSLFColor.presetColors.put("medSeaGreen", new Color(60, 179, 113));
        XSLFColor.presetColors.put("medSlateBlue", new Color(123, 104, 238));
        XSLFColor.presetColors.put("medSpringGreen", new Color(0, 250, 154));
        XSLFColor.presetColors.put("medTurquoise", new Color(72, 209, 204));
        XSLFColor.presetColors.put("medVioletRed", new Color(199, 21, 133));
        XSLFColor.presetColors.put("midnightBlue", new Color(25, 25, 112));
        XSLFColor.presetColors.put("mintCream", new Color(245, 255, 250));
        XSLFColor.presetColors.put("mistyRose", new Color(255, 228, 225));
        XSLFColor.presetColors.put("moccasin", new Color(255, 228, 181));
        XSLFColor.presetColors.put("navajoWhite", new Color(255, 222, 173));
        XSLFColor.presetColors.put("navy", new Color(0, 0, 128));
        XSLFColor.presetColors.put("oldLace", new Color(253, 245, 230));
        XSLFColor.presetColors.put("olive", new Color(128, 128, 0));
        XSLFColor.presetColors.put("oliveDrab", new Color(107, 142, 35));
        XSLFColor.presetColors.put("orange", new Color(255, 165, 0));
        XSLFColor.presetColors.put("orangeRed", new Color(255, 69, 0));
        XSLFColor.presetColors.put("orchid", new Color(218, 112, 214));
        XSLFColor.presetColors.put("paleGoldenrod", new Color(238, 232, 170));
        XSLFColor.presetColors.put("paleGreen", new Color(152, 251, 152));
        XSLFColor.presetColors.put("paleTurquoise", new Color(175, 238, 238));
        XSLFColor.presetColors.put("paleVioletRed", new Color(219, 112, 147));
        XSLFColor.presetColors.put("papayaWhip", new Color(255, 239, 213));
        XSLFColor.presetColors.put("peachPuff", new Color(255, 218, 185));
        XSLFColor.presetColors.put("peru", new Color(205, 133, 63));
        XSLFColor.presetColors.put("pink", new Color(255, 192, 203));
        XSLFColor.presetColors.put("plum", new Color(221, 160, 221));
        XSLFColor.presetColors.put("powderBlue", new Color(176, 224, 230));
        XSLFColor.presetColors.put("purple", new Color(128, 0, 128));
        XSLFColor.presetColors.put("red", new Color(255, 0, 0));
        XSLFColor.presetColors.put("rosyBrown", new Color(188, 143, 143));
        XSLFColor.presetColors.put("royalBlue", new Color(65, 105, 225));
        XSLFColor.presetColors.put("saddleBrown", new Color(139, 69, 19));
        XSLFColor.presetColors.put("salmon", new Color(250, 128, 114));
        XSLFColor.presetColors.put("sandyBrown", new Color(244, 164, 96));
        XSLFColor.presetColors.put("seaGreen", new Color(46, 139, 87));
        XSLFColor.presetColors.put("seaShell", new Color(255, 245, 238));
        XSLFColor.presetColors.put("sienna", new Color(160, 82, 45));
        XSLFColor.presetColors.put("silver", new Color(192, 192, 192));
        XSLFColor.presetColors.put("skyBlue", new Color(135, 206, 235));
        XSLFColor.presetColors.put("slateBlue", new Color(106, 90, 205));
        XSLFColor.presetColors.put("slateGray", new Color(112, 128, 144));
        XSLFColor.presetColors.put("snow", new Color(255, 250, 250));
        XSLFColor.presetColors.put("springGreen", new Color(0, 255, 127));
        XSLFColor.presetColors.put("steelBlue", new Color(70, 130, 180));
        XSLFColor.presetColors.put("tan", new Color(210, 180, 140));
        XSLFColor.presetColors.put("teal", new Color(0, 128, 128));
        XSLFColor.presetColors.put("thistle", new Color(216, 191, 216));
        XSLFColor.presetColors.put("tomato", new Color(255, 99, 71));
        XSLFColor.presetColors.put("turquoise", new Color(64, 224, 208));
        XSLFColor.presetColors.put("violet", new Color(238, 130, 238));
        XSLFColor.presetColors.put("wheat", new Color(245, 222, 179));
        XSLFColor.presetColors.put("white", new Color(255, 255, 255));
        XSLFColor.presetColors.put("whiteSmoke", new Color(245, 245, 245));
        XSLFColor.presetColors.put("yellow", new Color(255, 255, 0));
        XSLFColor.presetColors.put("yellowGreen", new Color(154, 205, 50));
    }
}
