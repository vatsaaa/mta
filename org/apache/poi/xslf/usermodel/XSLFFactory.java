// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.xslf.usermodel;

import org.apache.poi.util.POILogFactory;
import java.lang.reflect.Constructor;
import org.apache.poi.POIXMLRelation;
import org.apache.poi.POIXMLException;
import org.apache.poi.openxml4j.opc.PackagePart;
import org.apache.poi.openxml4j.opc.PackageRelationship;
import org.apache.poi.POIXMLDocumentPart;
import org.apache.poi.util.POILogger;
import org.apache.poi.POIXMLFactory;

public final class XSLFFactory extends POIXMLFactory
{
    private static final POILogger logger;
    private static final XSLFFactory inst;
    
    private XSLFFactory() {
    }
    
    public static XSLFFactory getInstance() {
        return XSLFFactory.inst;
    }
    
    @Override
    public POIXMLDocumentPart createDocumentPart(final POIXMLDocumentPart parent, final PackageRelationship rel, final PackagePart part) {
        final POIXMLRelation descriptor = XSLFRelation.getInstance(rel.getRelationshipType());
        if (descriptor == null || descriptor.getRelationClass() == null) {
            XSLFFactory.logger.log(POILogger.DEBUG, "using default POIXMLDocumentPart for " + rel.getRelationshipType());
            return new POIXMLDocumentPart(part, rel);
        }
        try {
            final Class<? extends POIXMLDocumentPart> cls = descriptor.getRelationClass();
            final Constructor<? extends POIXMLDocumentPart> constructor = cls.getDeclaredConstructor(PackagePart.class, PackageRelationship.class);
            return (POIXMLDocumentPart)constructor.newInstance(part, rel);
        }
        catch (Exception e) {
            throw new POIXMLException(e);
        }
    }
    
    @Override
    public POIXMLDocumentPart newDocumentPart(final POIXMLRelation descriptor) {
        try {
            final Class<? extends POIXMLDocumentPart> cls = descriptor.getRelationClass();
            final Constructor<? extends POIXMLDocumentPart> constructor = cls.getDeclaredConstructor((Class<?>[])new Class[0]);
            return (POIXMLDocumentPart)constructor.newInstance(new Object[0]);
        }
        catch (Exception e) {
            throw new POIXMLException(e);
        }
    }
    
    static {
        logger = POILogFactory.getLogger(XSLFFactory.class);
        inst = new XSLFFactory();
    }
}
