// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.xslf.usermodel;

import java.util.List;
import org.openxmlformats.schemas.drawingml.x2006.main.CTTableCell;
import org.openxmlformats.schemas.drawingml.x2006.main.CTTableRow;

public class DrawingTableRow
{
    private final CTTableRow row;
    
    public DrawingTableRow(final CTTableRow row) {
        this.row = row;
    }
    
    public DrawingTableCell[] getCells() {
        final List<CTTableCell> ctTableCells = this.row.getTcList();
        final DrawingTableCell[] o = new DrawingTableCell[ctTableCells.size()];
        for (int i = 0; i < o.length; ++i) {
            o[i] = new DrawingTableCell(ctTableCells.get(i));
        }
        return o;
    }
}
