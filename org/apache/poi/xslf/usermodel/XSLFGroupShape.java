// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.xslf.usermodel;

import java.awt.RenderingHints;
import java.awt.geom.AffineTransform;
import java.awt.Graphics2D;
import org.apache.poi.openxml4j.opc.PackageRelationship;
import org.apache.poi.openxml4j.opc.TargetMode;
import org.apache.poi.openxml4j.opc.PackagePart;
import java.util.regex.Pattern;
import org.openxmlformats.schemas.drawingml.x2006.main.CTNonVisualDrawingProps;
import org.openxmlformats.schemas.presentationml.x2006.main.CTGroupShapeNonVisual;
import org.apache.xmlbeans.XmlObject;
import org.openxmlformats.schemas.presentationml.x2006.main.CTConnector;
import org.openxmlformats.schemas.presentationml.x2006.main.CTShape;
import java.util.Iterator;
import org.openxmlformats.schemas.drawingml.x2006.main.CTPositiveSize2D;
import org.openxmlformats.schemas.drawingml.x2006.main.CTPoint2D;
import org.openxmlformats.schemas.drawingml.x2006.main.CTGroupTransform2D;
import org.apache.poi.util.Units;
import java.awt.geom.Rectangle2D;
import org.openxmlformats.schemas.drawingml.x2006.main.CTGroupShapeProperties;
import java.util.List;
import org.openxmlformats.schemas.presentationml.x2006.main.CTGroupShape;

public class XSLFGroupShape extends XSLFShape implements XSLFShapeContainer
{
    private final CTGroupShape _shape;
    private final XSLFSheet _sheet;
    private final List<XSLFShape> _shapes;
    private final CTGroupShapeProperties _spPr;
    private XSLFDrawing _drawing;
    
    XSLFGroupShape(final CTGroupShape shape, final XSLFSheet sheet) {
        this._shape = shape;
        this._sheet = sheet;
        this._shapes = this._sheet.buildShapes(this._shape);
        this._spPr = shape.getGrpSpPr();
    }
    
    @Override
    public CTGroupShape getXmlObject() {
        return this._shape;
    }
    
    @Override
    public Rectangle2D getAnchor() {
        final CTGroupTransform2D xfrm = this._spPr.getXfrm();
        final CTPoint2D off = xfrm.getOff();
        final long x = off.getX();
        final long y = off.getY();
        final CTPositiveSize2D ext = xfrm.getExt();
        final long cx = ext.getCx();
        final long cy = ext.getCy();
        return new Rectangle2D.Double(Units.toPoints(x), Units.toPoints(y), Units.toPoints(cx), Units.toPoints(cy));
    }
    
    @Override
    public void setAnchor(final Rectangle2D anchor) {
        final CTGroupTransform2D xfrm = this._spPr.isSetXfrm() ? this._spPr.getXfrm() : this._spPr.addNewXfrm();
        final CTPoint2D off = xfrm.isSetOff() ? xfrm.getOff() : xfrm.addNewOff();
        final long x = Units.toEMU(anchor.getX());
        final long y = Units.toEMU(anchor.getY());
        off.setX(x);
        off.setY(y);
        final CTPositiveSize2D ext = xfrm.isSetExt() ? xfrm.getExt() : xfrm.addNewExt();
        final long cx = Units.toEMU(anchor.getWidth());
        final long cy = Units.toEMU(anchor.getHeight());
        ext.setCx(cx);
        ext.setCy(cy);
    }
    
    public Rectangle2D getInteriorAnchor() {
        final CTGroupTransform2D xfrm = this._spPr.getXfrm();
        final CTPoint2D off = xfrm.getChOff();
        final long x = off.getX();
        final long y = off.getY();
        final CTPositiveSize2D ext = xfrm.getChExt();
        final long cx = ext.getCx();
        final long cy = ext.getCy();
        return new Rectangle2D.Double(Units.toPoints(x), Units.toPoints(y), Units.toPoints(cx), Units.toPoints(cy));
    }
    
    public void setInteriorAnchor(final Rectangle2D anchor) {
        final CTGroupTransform2D xfrm = this._spPr.isSetXfrm() ? this._spPr.getXfrm() : this._spPr.addNewXfrm();
        final CTPoint2D off = xfrm.isSetChOff() ? xfrm.getChOff() : xfrm.addNewChOff();
        final long x = Units.toEMU(anchor.getX());
        final long y = Units.toEMU(anchor.getY());
        off.setX(x);
        off.setY(y);
        final CTPositiveSize2D ext = xfrm.isSetChExt() ? xfrm.getChExt() : xfrm.addNewChExt();
        final long cx = Units.toEMU(anchor.getWidth());
        final long cy = Units.toEMU(anchor.getHeight());
        ext.setCx(cx);
        ext.setCy(cy);
    }
    
    public XSLFShape[] getShapes() {
        return this._shapes.toArray(new XSLFShape[this._shapes.size()]);
    }
    
    public Iterator<XSLFShape> iterator() {
        return this._shapes.iterator();
    }
    
    public boolean removeShape(final XSLFShape xShape) {
        final XmlObject obj = xShape.getXmlObject();
        if (obj instanceof CTShape) {
            this._shape.getSpList().remove(obj);
        }
        else if (obj instanceof CTGroupShape) {
            this._shape.getGrpSpList().remove(obj);
        }
        else {
            if (!(obj instanceof CTConnector)) {
                throw new IllegalArgumentException("Unsupported shape: " + xShape);
            }
            this._shape.getCxnSpList().remove(obj);
        }
        return this._shapes.remove(xShape);
    }
    
    @Override
    public String getShapeName() {
        return this._shape.getNvGrpSpPr().getCNvPr().getName();
    }
    
    @Override
    public int getShapeId() {
        return (int)this._shape.getNvGrpSpPr().getCNvPr().getId();
    }
    
    static CTGroupShape prototype(final int shapeId) {
        final CTGroupShape ct = CTGroupShape.Factory.newInstance();
        final CTGroupShapeNonVisual nvSpPr = ct.addNewNvGrpSpPr();
        final CTNonVisualDrawingProps cnv = nvSpPr.addNewCNvPr();
        cnv.setName("Group " + shapeId);
        cnv.setId(shapeId + 1);
        nvSpPr.addNewCNvGrpSpPr();
        nvSpPr.addNewNvPr();
        ct.addNewGrpSpPr();
        return ct;
    }
    
    private XSLFDrawing getDrawing() {
        if (this._drawing == null) {
            this._drawing = new XSLFDrawing(this._sheet, this._shape);
        }
        return this._drawing;
    }
    
    public XSLFAutoShape createAutoShape() {
        final XSLFAutoShape sh = this.getDrawing().createAutoShape();
        this._shapes.add(sh);
        return sh;
    }
    
    public XSLFFreeformShape createFreeform() {
        final XSLFFreeformShape sh = this.getDrawing().createFreeform();
        this._shapes.add(sh);
        return sh;
    }
    
    public XSLFTextBox createTextBox() {
        final XSLFTextBox sh = this.getDrawing().createTextBox();
        this._shapes.add(sh);
        return sh;
    }
    
    public XSLFConnectorShape createConnector() {
        final XSLFConnectorShape sh = this.getDrawing().createConnector();
        this._shapes.add(sh);
        return sh;
    }
    
    public XSLFGroupShape createGroup() {
        final XSLFGroupShape sh = this.getDrawing().createGroup();
        this._shapes.add(sh);
        return sh;
    }
    
    public XSLFPictureShape createPicture(final int pictureIndex) {
        final List<PackagePart> pics = this._sheet.getPackagePart().getPackage().getPartsByName(Pattern.compile("/ppt/media/image" + (pictureIndex + 1) + ".*?"));
        if (pics.size() == 0) {
            throw new IllegalArgumentException("Picture with index=" + pictureIndex + " was not found");
        }
        final PackagePart pic = pics.get(0);
        final PackageRelationship rel = this._sheet.getPackagePart().addRelationship(pic.getPartName(), TargetMode.INTERNAL, XSLFRelation.IMAGES.getRelation());
        final XSLFPictureShape sh = this.getDrawing().createPicture(rel.getId());
        sh.resize();
        this._shapes.add(sh);
        return sh;
    }
    
    @Override
    public void setFlipHorizontal(final boolean flip) {
        this._spPr.getXfrm().setFlipH(flip);
    }
    
    @Override
    public void setFlipVertical(final boolean flip) {
        this._spPr.getXfrm().setFlipV(flip);
    }
    
    @Override
    public boolean getFlipHorizontal() {
        return this._spPr.getXfrm().getFlipH();
    }
    
    @Override
    public boolean getFlipVertical() {
        return this._spPr.getXfrm().getFlipV();
    }
    
    @Override
    public void setRotation(final double theta) {
        this._spPr.getXfrm().setRot((int)(theta * 60000.0));
    }
    
    @Override
    public double getRotation() {
        return this._spPr.getXfrm().getRot() / 60000.0;
    }
    
    @Override
    public void draw(final Graphics2D graphics) {
        final Rectangle2D interior = this.getInteriorAnchor();
        final Rectangle2D exterior = this.getAnchor();
        final AffineTransform tx = (AffineTransform)graphics.getRenderingHint(XSLFRenderingHint.GROUP_TRANSFORM);
        final AffineTransform tx2 = new AffineTransform(tx);
        final double scaleX = (interior.getWidth() == 0.0) ? 1.0 : (exterior.getWidth() / interior.getWidth());
        final double scaleY = (interior.getHeight() == 0.0) ? 1.0 : (exterior.getHeight() / interior.getHeight());
        tx.translate(exterior.getX(), exterior.getY());
        tx.scale(scaleX, scaleY);
        tx.translate(-interior.getX(), -interior.getY());
        for (final XSLFShape shape : this.getShapes()) {
            final AffineTransform at = graphics.getTransform();
            graphics.setRenderingHint(XSLFRenderingHint.GSAVE, true);
            shape.applyTransform(graphics);
            shape.draw(graphics);
            graphics.setTransform(at);
            graphics.setRenderingHint(XSLFRenderingHint.GRESTORE, true);
        }
        graphics.setRenderingHint(XSLFRenderingHint.GROUP_TRANSFORM, tx2);
    }
    
    @Override
    void copy(final XSLFShape src) {
        final XSLFGroupShape gr = (XSLFGroupShape)src;
        final XSLFShape[] tgtShapes = this.getShapes();
        final XSLFShape[] srcShapes = gr.getShapes();
        for (int i = 0; i < tgtShapes.length; ++i) {
            final XSLFShape s1 = srcShapes[i];
            final XSLFShape s2 = tgtShapes[i];
            s2.copy(s1);
        }
    }
    
    public void clear() {
        for (final XSLFShape shape : this.getShapes()) {
            this.removeShape(shape);
        }
    }
}
