// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.xslf.usermodel;

import org.apache.xmlbeans.XmlObject;
import org.apache.xmlbeans.XmlException;
import java.io.IOException;
import org.openxmlformats.schemas.presentationml.x2006.main.NotesMasterDocument;
import org.apache.poi.openxml4j.opc.PackageRelationship;
import org.apache.poi.openxml4j.opc.PackagePart;
import org.openxmlformats.schemas.presentationml.x2006.main.CTNotesMaster;

public class XSLFNotesMaster extends XSLFSheet
{
    private CTNotesMaster _slide;
    
    XSLFNotesMaster() {
        this._slide = CTNotesMaster.Factory.newInstance();
    }
    
    protected XSLFNotesMaster(final PackagePart part, final PackageRelationship rel) throws IOException, XmlException {
        super(part, rel);
        final NotesMasterDocument doc = NotesMasterDocument.Factory.parse(this.getPackagePart().getInputStream());
        this._slide = doc.getNotesMaster();
        this.setCommonSlideData(this._slide.getCSld());
    }
    
    @Override
    public CTNotesMaster getXmlObject() {
        return this._slide;
    }
    
    @Override
    protected String getRootElementName() {
        return "notesMaster";
    }
    
    @Override
    public XSLFSheet getMasterSheet() {
        return null;
    }
}
