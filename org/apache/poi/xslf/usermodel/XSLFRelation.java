// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.xslf.usermodel;

import java.util.HashMap;
import org.apache.poi.util.POILogFactory;
import org.apache.poi.POIXMLDocumentPart;
import java.util.Map;
import org.apache.poi.util.POILogger;
import org.apache.poi.POIXMLRelation;

public class XSLFRelation extends POIXMLRelation
{
    private static POILogger log;
    protected static Map<String, XSLFRelation> _table;
    public static final XSLFRelation MAIN;
    public static final XSLFRelation MACRO;
    public static final XSLFRelation MACRO_TEMPLATE;
    public static final XSLFRelation PRESENTATIONML;
    public static final XSLFRelation PRESENTATIONML_TEMPLATE;
    public static final XSLFRelation PRESENTATION_MACRO;
    public static final XSLFRelation THEME_MANAGER;
    public static final XSLFRelation NOTES;
    public static final XSLFRelation SLIDE;
    public static final XSLFRelation SLIDE_LAYOUT;
    public static final XSLFRelation SLIDE_MASTER;
    public static final XSLFRelation NOTES_MASTER;
    public static final XSLFRelation COMMENTS;
    public static final XSLFRelation COMMENT_AUTHORS;
    public static final XSLFRelation HYPERLINK;
    public static final XSLFRelation THEME;
    public static final XSLFRelation VML_DRAWING;
    public static final XSLFRelation IMAGE_EMF;
    public static final XSLFRelation IMAGE_WMF;
    public static final XSLFRelation IMAGE_PICT;
    public static final XSLFRelation IMAGE_JPEG;
    public static final XSLFRelation IMAGE_PNG;
    public static final XSLFRelation IMAGE_DIB;
    public static final XSLFRelation IMAGE_GIF;
    public static final XSLFRelation IMAGE_TIFF;
    public static final XSLFRelation IMAGE_EPS;
    public static final XSLFRelation IMAGE_BMP;
    public static final XSLFRelation IMAGE_WPG;
    public static final XSLFRelation IMAGES;
    public static final XSLFRelation TABLE_STYLES;
    
    private XSLFRelation(final String type, final String rel, final String defaultName, final Class<? extends POIXMLDocumentPart> cls) {
        super(type, rel, defaultName, cls);
        if (cls != null && !XSLFRelation._table.containsKey(rel)) {
            XSLFRelation._table.put(rel, this);
        }
    }
    
    public static XSLFRelation getInstance(final String rel) {
        return XSLFRelation._table.get(rel);
    }
    
    static {
        XSLFRelation.log = POILogFactory.getLogger(XSLFRelation.class);
        XSLFRelation._table = new HashMap<String, XSLFRelation>();
        MAIN = new XSLFRelation("application/vnd.openxmlformats-officedocument.presentationml.presentation.main+xml", null, null, null);
        MACRO = new XSLFRelation("application/vnd.ms-powerpoint.slideshow.macroEnabled.main+xml", null, null, null);
        MACRO_TEMPLATE = new XSLFRelation("application/vnd.ms-powerpoint.template.macroEnabled.main+xml", null, null, null);
        PRESENTATIONML = new XSLFRelation("application/vnd.openxmlformats-officedocument.presentationml.slideshow.main+xml", null, null, null);
        PRESENTATIONML_TEMPLATE = new XSLFRelation("application/vnd.openxmlformats-officedocument.presentationml.template.main+xml", null, null, null);
        PRESENTATION_MACRO = new XSLFRelation("application/vnd.ms-powerpoint.presentation.macroEnabled.main+xml", null, null, null);
        THEME_MANAGER = new XSLFRelation("application/vnd.openxmlformats-officedocument.themeManager+xml", null, null, null);
        NOTES = new XSLFRelation("application/vnd.openxmlformats-officedocument.presentationml.notesSlide+xml", "http://schemas.openxmlformats.org/officeDocument/2006/relationships/notesSlide", "/ppt/notesSlides/notesSlide#.xml", XSLFNotes.class);
        SLIDE = new XSLFRelation("application/vnd.openxmlformats-officedocument.presentationml.slide+xml", "http://schemas.openxmlformats.org/officeDocument/2006/relationships/slide", "/ppt/slides/slide#.xml", XSLFSlide.class);
        SLIDE_LAYOUT = new XSLFRelation("application/vnd.openxmlformats-officedocument.presentationml.slideLayout+xml", "http://schemas.openxmlformats.org/officeDocument/2006/relationships/slideLayout", "/ppt/slideLayouts/slideLayout#.xml", XSLFSlideLayout.class);
        SLIDE_MASTER = new XSLFRelation("application/vnd.openxmlformats-officedocument.presentationml.slideMaster+xml", "http://schemas.openxmlformats.org/officeDocument/2006/relationships/slideMaster", "/ppt/slideMasters/slideMaster#.xml", XSLFSlideMaster.class);
        NOTES_MASTER = new XSLFRelation("application/vnd.openxmlformats-officedocument.presentationml.notesMaster+xml", "http://schemas.openxmlformats.org/officeDocument/2006/relationships/notesMaster", "/ppt/notesMasters/notesMaster#.xml", XSLFNotesMaster.class);
        COMMENTS = new XSLFRelation("application/vnd.openxmlformats-officedocument.presentationml.comments+xml", "http://schemas.openxmlformats.org/officeDocument/2006/relationships/comments", "/ppt/comments/comment#.xml", XSLFComments.class);
        COMMENT_AUTHORS = new XSLFRelation("application/vnd.openxmlformats-officedocument.presentationml.commentAuthors+xml", "http://schemas.openxmlformats.org/officeDocument/2006/relationships/commentAuthors", "/ppt/commentAuthors.xml", XSLFCommentAuthors.class);
        HYPERLINK = new XSLFRelation(null, "http://schemas.openxmlformats.org/officeDocument/2006/relationships/hyperlink", null, null);
        THEME = new XSLFRelation("application/vnd.openxmlformats-officedocument.theme+xml", "http://schemas.openxmlformats.org/officeDocument/2006/relationships/theme", "/ppt/theme/theme#.xml", XSLFTheme.class);
        VML_DRAWING = new XSLFRelation("application/vnd.openxmlformats-officedocument.vmlDrawing", "http://schemas.openxmlformats.org/officeDocument/2006/relationships/vmlDrawing", "/ppt/drawings/vmlDrawing#.vml", null);
        IMAGE_EMF = new XSLFRelation("image/x-emf", "http://schemas.openxmlformats.org/officeDocument/2006/relationships/image", "/ppt/media/image#.emf", XSLFPictureData.class);
        IMAGE_WMF = new XSLFRelation("image/x-wmf", "http://schemas.openxmlformats.org/officeDocument/2006/relationships/image", "/ppt/media/image#.wmf", XSLFPictureData.class);
        IMAGE_PICT = new XSLFRelation("image/pict", "http://schemas.openxmlformats.org/officeDocument/2006/relationships/image", "/ppt/media/image#.pict", XSLFPictureData.class);
        IMAGE_JPEG = new XSLFRelation("image/jpeg", "http://schemas.openxmlformats.org/officeDocument/2006/relationships/image", "/ppt/media/image#.jpeg", XSLFPictureData.class);
        IMAGE_PNG = new XSLFRelation("image/png", "http://schemas.openxmlformats.org/officeDocument/2006/relationships/image", "/ppt/media/image#.png", XSLFPictureData.class);
        IMAGE_DIB = new XSLFRelation("image/dib", "http://schemas.openxmlformats.org/officeDocument/2006/relationships/image", "/ppt/media/image#.dib", XSLFPictureData.class);
        IMAGE_GIF = new XSLFRelation("image/gif", "http://schemas.openxmlformats.org/officeDocument/2006/relationships/image", "/ppt/media/image#.gif", XSLFPictureData.class);
        IMAGE_TIFF = new XSLFRelation("image/tiff", "http://schemas.openxmlformats.org/officeDocument/2006/relationships/image", "/ppt/media/image#.tiff", XSLFPictureData.class);
        IMAGE_EPS = new XSLFRelation("image/x-eps", "http://schemas.openxmlformats.org/officeDocument/2006/relationships/image", "/ppt/media/image#.eps", XSLFPictureData.class);
        IMAGE_BMP = new XSLFRelation("image/x-ms-bmp", "http://schemas.openxmlformats.org/officeDocument/2006/relationships/image", "/ppt/media/image#.bmp", XSLFPictureData.class);
        IMAGE_WPG = new XSLFRelation("image/x-wpg", "http://schemas.openxmlformats.org/officeDocument/2006/relationships/image", "/ppt/media/image#.wpg", XSLFPictureData.class);
        IMAGES = new XSLFRelation(null, "http://schemas.openxmlformats.org/officeDocument/2006/relationships/image", null, null);
        TABLE_STYLES = new XSLFRelation("application/vnd.openxmlformats-officedocument.presentationml.tableStyles+xml", "http://schemas.openxmlformats.org/officeDocument/2006/relationships/tableStyles", "/ppt/tableStyles.xml", XSLFTableStyles.class);
    }
}
