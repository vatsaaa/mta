// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.xslf.usermodel;

public interface XSLFFontManager
{
    String getRendererableFont(final String p0, final int p1);
}
