// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.xslf.usermodel;

import org.openxmlformats.schemas.drawingml.x2006.main.CTTextParagraphProperties;
import org.openxmlformats.schemas.presentationml.x2006.main.CTPlaceholder;
import org.apache.poi.xslf.model.PropertyFetcher;
import org.openxmlformats.schemas.drawingml.x2006.main.STTextUnderlineType;
import org.openxmlformats.schemas.drawingml.x2006.main.STTextStrikeType;
import org.openxmlformats.schemas.drawingml.x2006.main.CTTextFont;
import org.openxmlformats.schemas.drawingml.x2006.main.CTTextNormalAutofit;
import org.openxmlformats.schemas.drawingml.x2006.main.CTShapeStyle;
import org.apache.xmlbeans.XmlObject;
import org.openxmlformats.schemas.drawingml.x2006.main.STSchemeColorVal;
import org.openxmlformats.schemas.drawingml.x2006.main.CTSchemeColor;
import org.apache.poi.xslf.model.CharacterPropertyFetcher;
import org.openxmlformats.schemas.drawingml.x2006.main.CTSRgbColor;
import org.openxmlformats.schemas.drawingml.x2006.main.CTSolidColorFillProperties;
import org.openxmlformats.schemas.drawingml.x2006.main.CTTextCharacterProperties;
import java.awt.Color;
import java.awt.font.TextLayout;
import java.awt.geom.AffineTransform;
import java.awt.font.FontRenderContext;
import java.text.AttributedCharacterIterator;
import java.awt.font.TextAttribute;
import java.text.AttributedString;
import org.openxmlformats.schemas.drawingml.x2006.main.CTRegularTextRun;

public class XSLFTextRun
{
    private final CTRegularTextRun _r;
    private final XSLFTextParagraph _p;
    
    XSLFTextRun(final CTRegularTextRun r, final XSLFTextParagraph p) {
        this._r = r;
        this._p = p;
    }
    
    XSLFTextParagraph getParentParagraph() {
        return this._p;
    }
    
    public String getText() {
        return this._r.getT();
    }
    
    String getRenderableText() {
        final String txt = this._r.getT();
        final TextCap cap = this.getTextCap();
        final StringBuffer buf = new StringBuffer();
        for (int i = 0; i < txt.length(); ++i) {
            final char c = txt.charAt(i);
            if (c == '\t') {
                buf.append("  ");
            }
            else {
                switch (cap) {
                    case ALL: {
                        buf.append(Character.toUpperCase(c));
                        break;
                    }
                    case SMALL: {
                        buf.append(Character.toLowerCase(c));
                        break;
                    }
                    default: {
                        buf.append(c);
                        break;
                    }
                }
            }
        }
        return buf.toString();
    }
    
    private String tab2space() {
        final AttributedString string = new AttributedString(" ");
        string.addAttribute(TextAttribute.FAMILY, this.getFontFamily());
        string.addAttribute(TextAttribute.SIZE, (float)this.getFontSize());
        final TextLayout l = new TextLayout(string.getIterator(), new FontRenderContext(null, true, true));
        final double wspace = l.getAdvance();
        final double tabSz = this._p.getDefaultTabSize();
        final int numSpaces = (int)Math.ceil(tabSz / wspace);
        final StringBuffer buf = new StringBuffer();
        for (int i = 0; i < numSpaces; ++i) {
            buf.append(' ');
        }
        return buf.toString();
    }
    
    public void setText(final String text) {
        this._r.setT(text);
    }
    
    public CTRegularTextRun getXmlObject() {
        return this._r;
    }
    
    public void setFontColor(final Color color) {
        final CTTextCharacterProperties rPr = this.getRPr();
        final CTSolidColorFillProperties fill = rPr.isSetSolidFill() ? rPr.getSolidFill() : rPr.addNewSolidFill();
        final CTSRgbColor clr = fill.isSetSrgbClr() ? fill.getSrgbClr() : fill.addNewSrgbClr();
        clr.setVal(new byte[] { (byte)color.getRed(), (byte)color.getGreen(), (byte)color.getBlue() });
        if (fill.isSetHslClr()) {
            fill.unsetHslClr();
        }
        if (fill.isSetPrstClr()) {
            fill.unsetPrstClr();
        }
        if (fill.isSetSchemeClr()) {
            fill.unsetSchemeClr();
        }
        if (fill.isSetScrgbClr()) {
            fill.unsetScrgbClr();
        }
        if (fill.isSetSysClr()) {
            fill.unsetSysClr();
        }
    }
    
    public Color getFontColor() {
        final XSLFTheme theme = this._p.getParentShape().getSheet().getTheme();
        final CTShapeStyle style = this._p.getParentShape().getSpStyle();
        final CTSchemeColor phClr = (style == null) ? null : style.getFontRef().getSchemeClr();
        final CharacterPropertyFetcher<Color> fetcher = new CharacterPropertyFetcher<Color>(this._p.getLevel()) {
            @Override
            public boolean fetch(final CTTextCharacterProperties props) {
                final CTSolidColorFillProperties solidFill = props.getSolidFill();
                if (solidFill != null) {
                    final boolean useCtxColor = (solidFill.isSetSchemeClr() && solidFill.getSchemeClr().getVal() == STSchemeColorVal.PH_CLR) || this.isFetchingFromMaster;
                    final Color c = new XSLFColor(solidFill, theme, useCtxColor ? phClr : null).getColor();
                    this.setValue(c);
                    return true;
                }
                return false;
            }
        };
        this.fetchCharacterProperty(fetcher);
        return fetcher.getValue();
    }
    
    public void setFontSize(final double fontSize) {
        final CTTextCharacterProperties rPr = this.getRPr();
        if (fontSize == -1.0) {
            if (rPr.isSetSz()) {
                rPr.unsetSz();
            }
        }
        else {
            if (fontSize < 1.0) {
                throw new IllegalArgumentException("Minimum font size is 1pt but was " + fontSize);
            }
            rPr.setSz((int)(100.0 * fontSize));
        }
    }
    
    public double getFontSize() {
        double scale = 1.0;
        final CTTextNormalAutofit afit = this.getParentParagraph().getParentShape().getTextBodyPr().getNormAutofit();
        if (afit != null) {
            scale = afit.getFontScale() / 100000.0;
        }
        final CharacterPropertyFetcher<Double> fetcher = new CharacterPropertyFetcher<Double>(this._p.getLevel()) {
            @Override
            public boolean fetch(final CTTextCharacterProperties props) {
                if (props.isSetSz()) {
                    this.setValue(props.getSz() * 0.01);
                    return true;
                }
                return false;
            }
        };
        this.fetchCharacterProperty(fetcher);
        return (fetcher.getValue() == null) ? -1.0 : (fetcher.getValue() * scale);
    }
    
    public double getCharacterSpacing() {
        final CharacterPropertyFetcher<Double> fetcher = new CharacterPropertyFetcher<Double>(this._p.getLevel()) {
            @Override
            public boolean fetch(final CTTextCharacterProperties props) {
                if (props.isSetSpc()) {
                    this.setValue(props.getSpc() * 0.01);
                    return true;
                }
                return false;
            }
        };
        this.fetchCharacterProperty(fetcher);
        return (fetcher.getValue() == null) ? 0.0 : fetcher.getValue();
    }
    
    public void setCharacterSpacing(final double spc) {
        final CTTextCharacterProperties rPr = this.getRPr();
        if (spc == 0.0) {
            if (rPr.isSetSpc()) {
                rPr.unsetSpc();
            }
        }
        else {
            rPr.setSpc((int)(100.0 * spc));
        }
    }
    
    public void setFontFamily(final String typeface) {
        this.setFontFamily(typeface, (byte)(-1), (byte)(-1), false);
    }
    
    public void setFontFamily(final String typeface, final byte charset, final byte pictAndFamily, final boolean isSymbol) {
        final CTTextCharacterProperties rPr = this.getRPr();
        if (typeface == null) {
            if (rPr.isSetLatin()) {
                rPr.unsetLatin();
            }
            if (rPr.isSetCs()) {
                rPr.unsetCs();
            }
            if (rPr.isSetSym()) {
                rPr.unsetSym();
            }
        }
        else if (isSymbol) {
            final CTTextFont font = rPr.isSetSym() ? rPr.getSym() : rPr.addNewSym();
            font.setTypeface(typeface);
        }
        else {
            final CTTextFont latin = rPr.isSetLatin() ? rPr.getLatin() : rPr.addNewLatin();
            latin.setTypeface(typeface);
            if (charset != -1) {
                latin.setCharset(charset);
            }
            if (pictAndFamily != -1) {
                latin.setPitchFamily(pictAndFamily);
            }
        }
    }
    
    public String getFontFamily() {
        final XSLFTheme theme = this._p.getParentShape().getSheet().getTheme();
        final CharacterPropertyFetcher<String> visitor = new CharacterPropertyFetcher<String>(this._p.getLevel()) {
            @Override
            public boolean fetch(final CTTextCharacterProperties props) {
                final CTTextFont font = props.getLatin();
                if (font != null) {
                    String typeface = font.getTypeface();
                    if ("+mj-lt".equals(typeface)) {
                        typeface = theme.getMajorFont();
                    }
                    else if ("+mn-lt".equals(typeface)) {
                        typeface = theme.getMinorFont();
                    }
                    this.setValue(typeface);
                    return true;
                }
                return false;
            }
        };
        this.fetchCharacterProperty(visitor);
        return visitor.getValue();
    }
    
    public byte getPitchAndFamily() {
        final XSLFTheme theme = this._p.getParentShape().getSheet().getTheme();
        final CharacterPropertyFetcher<Byte> visitor = new CharacterPropertyFetcher<Byte>(this._p.getLevel()) {
            @Override
            public boolean fetch(final CTTextCharacterProperties props) {
                final CTTextFont font = props.getLatin();
                if (font != null) {
                    this.setValue(font.getPitchFamily());
                    return true;
                }
                return false;
            }
        };
        this.fetchCharacterProperty(visitor);
        return (byte)((visitor.getValue() == null) ? 0 : ((byte)visitor.getValue()));
    }
    
    public void setStrikethrough(final boolean strike) {
        this.getRPr().setStrike(strike ? STTextStrikeType.SNG_STRIKE : STTextStrikeType.NO_STRIKE);
    }
    
    public boolean isStrikethrough() {
        final CharacterPropertyFetcher<Boolean> fetcher = new CharacterPropertyFetcher<Boolean>(this._p.getLevel()) {
            @Override
            public boolean fetch(final CTTextCharacterProperties props) {
                if (props.isSetStrike()) {
                    this.setValue(props.getStrike() != STTextStrikeType.NO_STRIKE);
                    return true;
                }
                return false;
            }
        };
        this.fetchCharacterProperty(fetcher);
        return fetcher.getValue() != null && fetcher.getValue();
    }
    
    public boolean isSuperscript() {
        final CharacterPropertyFetcher<Boolean> fetcher = new CharacterPropertyFetcher<Boolean>(this._p.getLevel()) {
            @Override
            public boolean fetch(final CTTextCharacterProperties props) {
                if (props.isSetBaseline()) {
                    this.setValue(props.getBaseline() > 0);
                    return true;
                }
                return false;
            }
        };
        this.fetchCharacterProperty(fetcher);
        return fetcher.getValue() != null && fetcher.getValue();
    }
    
    public boolean isSubscript() {
        final CharacterPropertyFetcher<Boolean> fetcher = new CharacterPropertyFetcher<Boolean>(this._p.getLevel()) {
            @Override
            public boolean fetch(final CTTextCharacterProperties props) {
                if (props.isSetBaseline()) {
                    this.setValue(props.getBaseline() < 0);
                    return true;
                }
                return false;
            }
        };
        this.fetchCharacterProperty(fetcher);
        return fetcher.getValue() != null && fetcher.getValue();
    }
    
    public TextCap getTextCap() {
        final CharacterPropertyFetcher<TextCap> fetcher = new CharacterPropertyFetcher<TextCap>(this._p.getLevel()) {
            @Override
            public boolean fetch(final CTTextCharacterProperties props) {
                if (props.isSetCap()) {
                    final int idx = props.getCap().intValue() - 1;
                    this.setValue(TextCap.values()[idx]);
                    return true;
                }
                return false;
            }
        };
        this.fetchCharacterProperty(fetcher);
        return (fetcher.getValue() == null) ? TextCap.NONE : fetcher.getValue();
    }
    
    public void setBold(final boolean bold) {
        this.getRPr().setB(bold);
    }
    
    public boolean isBold() {
        final CharacterPropertyFetcher<Boolean> fetcher = new CharacterPropertyFetcher<Boolean>(this._p.getLevel()) {
            @Override
            public boolean fetch(final CTTextCharacterProperties props) {
                if (props.isSetB()) {
                    this.setValue(props.getB());
                    return true;
                }
                return false;
            }
        };
        this.fetchCharacterProperty(fetcher);
        return fetcher.getValue() != null && fetcher.getValue();
    }
    
    public void setItalic(final boolean italic) {
        this.getRPr().setI(italic);
    }
    
    public boolean isItalic() {
        final CharacterPropertyFetcher<Boolean> fetcher = new CharacterPropertyFetcher<Boolean>(this._p.getLevel()) {
            @Override
            public boolean fetch(final CTTextCharacterProperties props) {
                if (props.isSetI()) {
                    this.setValue(props.getI());
                    return true;
                }
                return false;
            }
        };
        this.fetchCharacterProperty(fetcher);
        return fetcher.getValue() != null && fetcher.getValue();
    }
    
    public void setUnderline(final boolean underline) {
        this.getRPr().setU(underline ? STTextUnderlineType.SNG : STTextUnderlineType.NONE);
    }
    
    public boolean isUnderline() {
        final CharacterPropertyFetcher<Boolean> fetcher = new CharacterPropertyFetcher<Boolean>(this._p.getLevel()) {
            @Override
            public boolean fetch(final CTTextCharacterProperties props) {
                if (props.isSetU()) {
                    this.setValue(props.getU() != STTextUnderlineType.NONE);
                    return true;
                }
                return false;
            }
        };
        this.fetchCharacterProperty(fetcher);
        return fetcher.getValue() != null && fetcher.getValue();
    }
    
    protected CTTextCharacterProperties getRPr() {
        return this._r.isSetRPr() ? this._r.getRPr() : this._r.addNewRPr();
    }
    
    @Override
    public String toString() {
        return "[" + this.getClass() + "]" + this.getText();
    }
    
    public XSLFHyperlink createHyperlink() {
        final XSLFHyperlink link = new XSLFHyperlink(this._r.getRPr().addNewHlinkClick(), this);
        return link;
    }
    
    public XSLFHyperlink getHyperlink() {
        if (!this._r.getRPr().isSetHlinkClick()) {
            return null;
        }
        return new XSLFHyperlink(this._r.getRPr().getHlinkClick(), this);
    }
    
    private boolean fetchCharacterProperty(final CharacterPropertyFetcher fetcher) {
        boolean ok = false;
        if (this._r.isSetRPr()) {
            ok = fetcher.fetch(this.getRPr());
        }
        if (!ok) {
            final XSLFTextShape shape = this._p.getParentShape();
            ok = shape.fetchShapeProperty(fetcher);
            if (!ok) {
                final CTPlaceholder ph = shape.getCTPlaceholder();
                if (ph == null) {
                    final XMLSlideShow ppt = shape.getSheet().getSlideShow();
                    final CTTextParagraphProperties themeProps = ppt.getDefaultParagraphStyle(this._p.getLevel());
                    if (themeProps != null) {
                        fetcher.isFetchingFromMaster = true;
                        ok = fetcher.fetch(themeProps);
                    }
                }
                if (!ok) {
                    final CTTextParagraphProperties defaultProps = this._p.getDefaultMasterStyle();
                    if (defaultProps != null) {
                        fetcher.isFetchingFromMaster = true;
                        ok = fetcher.fetch(defaultProps);
                    }
                }
            }
        }
        return ok;
    }
    
    void copy(final XSLFTextRun r) {
        final String srcFontFamily = r.getFontFamily();
        if (srcFontFamily != null && !srcFontFamily.equals(this.getFontFamily())) {
            this.setFontFamily(srcFontFamily);
        }
        final Color srcFontColor = r.getFontColor();
        if (srcFontColor != null && !srcFontColor.equals(this.getFontColor())) {
            this.setFontColor(srcFontColor);
        }
        final double srcFontSize = r.getFontSize();
        if (srcFontSize != this.getFontSize()) {
            this.setFontSize(srcFontSize);
        }
        final boolean bold = r.isBold();
        if (bold != this.isBold()) {
            this.setBold(bold);
        }
        final boolean italic = r.isItalic();
        if (italic != this.isItalic()) {
            this.setItalic(italic);
        }
        final boolean underline = r.isUnderline();
        if (underline != this.isUnderline()) {
            this.setUnderline(underline);
        }
        final boolean strike = r.isStrikethrough();
        if (strike != this.isStrikethrough()) {
            this.setStrikethrough(strike);
        }
    }
}
