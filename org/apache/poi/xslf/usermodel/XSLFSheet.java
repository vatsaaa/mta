// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.xslf.usermodel;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.POIXMLException;
import java.awt.RenderingHints;
import java.awt.geom.AffineTransform;
import java.awt.Graphics2D;
import org.openxmlformats.schemas.presentationml.x2006.main.CTPlaceholder;
import java.io.IOException;
import java.io.OutputStream;
import javax.xml.namespace.QName;
import org.openxmlformats.schemas.officeDocument.x2006.relationships.STRelationshipId;
import java.util.HashMap;
import org.apache.xmlbeans.XmlOptions;
import java.util.Iterator;
import org.apache.poi.openxml4j.opc.TargetMode;
import java.util.regex.Pattern;
import org.openxmlformats.schemas.presentationml.x2006.main.CTCommonSlideData;
import org.apache.poi.util.Internal;
import org.apache.xmlbeans.XmlObject;
import org.openxmlformats.schemas.presentationml.x2006.main.CTGraphicalObjectFrame;
import org.openxmlformats.schemas.presentationml.x2006.main.CTPicture;
import org.openxmlformats.schemas.presentationml.x2006.main.CTConnector;
import org.openxmlformats.schemas.presentationml.x2006.main.CTShape;
import java.util.ArrayList;
import org.apache.poi.openxml4j.opc.PackageRelationship;
import org.apache.poi.openxml4j.opc.PackagePart;
import java.util.Map;
import org.openxmlformats.schemas.presentationml.x2006.main.CTGroupShape;
import java.util.List;
import org.apache.poi.POIXMLDocumentPart;

public abstract class XSLFSheet extends POIXMLDocumentPart implements XSLFShapeContainer
{
    private XSLFCommonSlideData _commonSlideData;
    private XSLFDrawing _drawing;
    private List<XSLFShape> _shapes;
    private CTGroupShape _spTree;
    private List<XSLFTextShape> _placeholders;
    private Map<Integer, XSLFSimpleShape> _placeholderByIdMap;
    private Map<Integer, XSLFSimpleShape> _placeholderByTypeMap;
    
    public XSLFSheet() {
    }
    
    public XSLFSheet(final PackagePart part, final PackageRelationship rel) {
        super(part, rel);
    }
    
    public XMLSlideShow getSlideShow() {
        for (POIXMLDocumentPart p = this.getParent(); p != null; p = p.getParent()) {
            if (p instanceof XMLSlideShow) {
                return (XMLSlideShow)p;
            }
        }
        throw new IllegalStateException("SlideShow was not found");
    }
    
    protected List<XSLFShape> buildShapes(final CTGroupShape spTree) {
        final List<XSLFShape> shapes = new ArrayList<XSLFShape>();
        for (final XmlObject ch : spTree.selectPath("*")) {
            if (ch instanceof CTShape) {
                final XSLFAutoShape shape = XSLFAutoShape.create((CTShape)ch, this);
                shapes.add(shape);
            }
            else if (ch instanceof CTGroupShape) {
                shapes.add(new XSLFGroupShape((CTGroupShape)ch, this));
            }
            else if (ch instanceof CTConnector) {
                shapes.add(new XSLFConnectorShape((CTConnector)ch, this));
            }
            else if (ch instanceof CTPicture) {
                shapes.add(new XSLFPictureShape((CTPicture)ch, this));
            }
            else if (ch instanceof CTGraphicalObjectFrame) {
                final XSLFGraphicFrame shape2 = XSLFGraphicFrame.create((CTGraphicalObjectFrame)ch, this);
                shapes.add(shape2);
            }
        }
        return shapes;
    }
    
    public abstract XmlObject getXmlObject();
    
    @Internal
    public XSLFCommonSlideData getCommonSlideData() {
        return this._commonSlideData;
    }
    
    protected void setCommonSlideData(final CTCommonSlideData data) {
        if (data == null) {
            this._commonSlideData = null;
        }
        else {
            this._commonSlideData = new XSLFCommonSlideData(data);
        }
    }
    
    private XSLFDrawing getDrawing() {
        if (this._drawing == null) {
            this._drawing = new XSLFDrawing(this, this.getSpTree());
        }
        return this._drawing;
    }
    
    private List<XSLFShape> getShapeList() {
        if (this._shapes == null) {
            this._shapes = this.buildShapes(this.getSpTree());
        }
        return this._shapes;
    }
    
    public XSLFAutoShape createAutoShape() {
        final List<XSLFShape> shapes = this.getShapeList();
        final XSLFAutoShape sh = this.getDrawing().createAutoShape();
        shapes.add(sh);
        return sh;
    }
    
    public XSLFFreeformShape createFreeform() {
        final List<XSLFShape> shapes = this.getShapeList();
        final XSLFFreeformShape sh = this.getDrawing().createFreeform();
        shapes.add(sh);
        return sh;
    }
    
    public XSLFTextBox createTextBox() {
        final List<XSLFShape> shapes = this.getShapeList();
        final XSLFTextBox sh = this.getDrawing().createTextBox();
        shapes.add(sh);
        return sh;
    }
    
    public XSLFConnectorShape createConnector() {
        final List<XSLFShape> shapes = this.getShapeList();
        final XSLFConnectorShape sh = this.getDrawing().createConnector();
        shapes.add(sh);
        return sh;
    }
    
    public XSLFGroupShape createGroup() {
        final List<XSLFShape> shapes = this.getShapeList();
        final XSLFGroupShape sh = this.getDrawing().createGroup();
        shapes.add(sh);
        return sh;
    }
    
    public XSLFPictureShape createPicture(final int pictureIndex) {
        final List<PackagePart> pics = this.getPackagePart().getPackage().getPartsByName(Pattern.compile("/ppt/media/image" + (pictureIndex + 1) + ".*?"));
        if (pics.size() == 0) {
            throw new IllegalArgumentException("Picture with index=" + pictureIndex + " was not found");
        }
        final PackagePart pic = pics.get(0);
        final PackageRelationship rel = this.getPackagePart().addRelationship(pic.getPartName(), TargetMode.INTERNAL, XSLFRelation.IMAGES.getRelation());
        this.addRelation(rel.getId(), new XSLFPictureData(pic, rel));
        final XSLFPictureShape sh = this.getDrawing().createPicture(rel.getId());
        sh.resize();
        this.getShapeList().add(sh);
        return sh;
    }
    
    public XSLFTable createTable() {
        final List<XSLFShape> shapes = this.getShapeList();
        final XSLFTable sh = this.getDrawing().createTable();
        shapes.add(sh);
        return sh;
    }
    
    public XSLFShape[] getShapes() {
        return this.getShapeList().toArray(new XSLFShape[this._shapes.size()]);
    }
    
    public Iterator<XSLFShape> iterator() {
        return this.getShapeList().iterator();
    }
    
    public boolean removeShape(final XSLFShape xShape) {
        final XmlObject obj = xShape.getXmlObject();
        final CTGroupShape spTree = this.getSpTree();
        if (obj instanceof CTShape) {
            spTree.getSpList().remove(obj);
        }
        else if (obj instanceof CTGroupShape) {
            spTree.getGrpSpList().remove(obj);
        }
        else {
            if (!(obj instanceof CTConnector)) {
                throw new IllegalArgumentException("Unsupported shape: " + xShape);
            }
            spTree.getCxnSpList().remove(obj);
        }
        return this.getShapeList().remove(xShape);
    }
    
    public void clear() {
        for (final XSLFShape shape : this.getShapes()) {
            this.removeShape(shape);
        }
    }
    
    protected abstract String getRootElementName();
    
    protected CTGroupShape getSpTree() {
        if (this._spTree == null) {
            final XmlObject root = this.getXmlObject();
            final XmlObject[] sp = root.selectPath("declare namespace p='http://schemas.openxmlformats.org/presentationml/2006/main' .//*/p:spTree");
            if (sp.length == 0) {
                throw new IllegalStateException("CTGroupShape was not found");
            }
            this._spTree = (CTGroupShape)sp[0];
        }
        return this._spTree;
    }
    
    @Override
    protected final void commit() throws IOException {
        final XmlOptions xmlOptions = new XmlOptions(XSLFSheet.DEFAULT_XML_OPTIONS);
        final Map<String, String> map = new HashMap<String, String>();
        map.put(STRelationshipId.type.getName().getNamespaceURI(), "r");
        map.put("http://schemas.openxmlformats.org/drawingml/2006/main", "a");
        map.put("http://schemas.openxmlformats.org/presentationml/2006/main", "p");
        xmlOptions.setSaveSuggestedPrefixes(map);
        final String docName = this.getRootElementName();
        if (docName != null) {
            xmlOptions.setSaveSyntheticDocumentElement(new QName("http://schemas.openxmlformats.org/presentationml/2006/main", docName));
        }
        final PackagePart part = this.getPackagePart();
        final OutputStream out = part.getOutputStream();
        this.getXmlObject().save(out, xmlOptions);
        out.close();
    }
    
    public XSLFSheet importContent(final XSLFSheet src) {
        this._shapes = null;
        this._spTree = null;
        this._drawing = null;
        this._spTree = null;
        this.getSpTree().set(src.getSpTree());
        final List<XSLFShape> tgtShapes = this.getShapeList();
        final List<XSLFShape> srcShapes = src.getShapeList();
        for (int i = 0; i < tgtShapes.size(); ++i) {
            final XSLFShape s1 = srcShapes.get(i);
            final XSLFShape s2 = tgtShapes.get(i);
            s2.copy(s1);
        }
        return this;
    }
    
    public XSLFSheet appendContent(final XSLFSheet src) {
        final CTGroupShape spTree = this.getSpTree();
        final int numShapes = this.getShapeList().size();
        final CTGroupShape srcTree = src.getSpTree();
        for (final XmlObject ch : srcTree.selectPath("*")) {
            if (ch instanceof CTShape) {
                spTree.addNewSp().set(ch);
            }
            else if (ch instanceof CTGroupShape) {
                spTree.addNewGrpSp().set(ch);
            }
            else if (ch instanceof CTConnector) {
                spTree.addNewCxnSp().set(ch);
            }
            else if (ch instanceof CTPicture) {
                spTree.addNewPic().set(ch);
            }
            else if (ch instanceof CTGraphicalObjectFrame) {
                spTree.addNewGraphicFrame().set(ch);
            }
        }
        this._shapes = null;
        this._spTree = null;
        this._drawing = null;
        this._spTree = null;
        final List<XSLFShape> tgtShapes = this.getShapeList();
        final List<XSLFShape> srcShapes = src.getShapeList();
        for (int i = 0; i < srcShapes.size(); ++i) {
            final XSLFShape s1 = srcShapes.get(i);
            final XSLFShape s2 = tgtShapes.get(numShapes + i);
            s2.copy(s1);
        }
        return this;
    }
    
    XSLFTheme getTheme() {
        return null;
    }
    
    public abstract XSLFSheet getMasterSheet();
    
    protected XSLFTextShape getTextShapeByType(final Placeholder type) {
        for (final XSLFShape shape : this.getShapes()) {
            if (shape instanceof XSLFTextShape) {
                final XSLFTextShape txt = (XSLFTextShape)shape;
                if (txt.getTextType() == type) {
                    return txt;
                }
            }
        }
        return null;
    }
    
    XSLFSimpleShape getPlaceholder(final CTPlaceholder ph) {
        XSLFSimpleShape shape = null;
        if (ph.isSetIdx()) {
            shape = this.getPlaceholderById((int)ph.getIdx());
        }
        if (shape == null && ph.isSetType()) {
            shape = this.getPlaceholderByType(ph.getType().intValue());
        }
        return shape;
    }
    
    void initPlaceholders() {
        if (this._placeholders == null) {
            this._placeholders = new ArrayList<XSLFTextShape>();
            this._placeholderByIdMap = new HashMap<Integer, XSLFSimpleShape>();
            this._placeholderByTypeMap = new HashMap<Integer, XSLFSimpleShape>();
            for (final XSLFShape sh : this.getShapes()) {
                if (sh instanceof XSLFTextShape) {
                    final XSLFTextShape sShape = (XSLFTextShape)sh;
                    final CTPlaceholder ph = sShape.getCTPlaceholder();
                    if (ph != null) {
                        this._placeholders.add(sShape);
                        if (ph.isSetIdx()) {
                            final int idx = (int)ph.getIdx();
                            this._placeholderByIdMap.put(idx, sShape);
                        }
                        if (ph.isSetType()) {
                            this._placeholderByTypeMap.put(ph.getType().intValue(), sShape);
                        }
                    }
                }
            }
        }
    }
    
    XSLFSimpleShape getPlaceholderById(final int id) {
        this.initPlaceholders();
        return this._placeholderByIdMap.get(id);
    }
    
    XSLFSimpleShape getPlaceholderByType(final int ordinal) {
        this.initPlaceholders();
        return this._placeholderByTypeMap.get(ordinal);
    }
    
    public XSLFTextShape getPlaceholder(final int idx) {
        this.initPlaceholders();
        return this._placeholders.get(idx);
    }
    
    public XSLFTextShape[] getPlaceholders() {
        this.initPlaceholders();
        return this._placeholders.toArray(new XSLFTextShape[this._placeholders.size()]);
    }
    
    protected boolean canDraw(final XSLFShape shape) {
        return true;
    }
    
    public boolean getFollowMasterGraphics() {
        return false;
    }
    
    public XSLFBackground getBackground() {
        return null;
    }
    
    public void draw(final Graphics2D graphics) {
        final XSLFSheet master = this.getMasterSheet();
        if (this.getFollowMasterGraphics() && master != null) {
            master.draw(graphics);
        }
        graphics.setRenderingHint(XSLFRenderingHint.GROUP_TRANSFORM, new AffineTransform());
        for (final XSLFShape shape : this.getShapeList()) {
            if (!this.canDraw(shape)) {
                continue;
            }
            final AffineTransform at = graphics.getTransform();
            graphics.setRenderingHint(XSLFRenderingHint.GSAVE, true);
            shape.applyTransform(graphics);
            shape.draw(graphics);
            graphics.setTransform(at);
            graphics.setRenderingHint(XSLFRenderingHint.GRESTORE, true);
        }
    }
    
    String importBlip(final String blipId, final PackagePart packagePart) {
        final PackageRelationship blipRel = packagePart.getRelationship(blipId);
        PackagePart blipPart;
        try {
            blipPart = packagePart.getRelatedPart(blipRel);
        }
        catch (InvalidFormatException e) {
            throw new POIXMLException(e);
        }
        final XSLFPictureData data = new XSLFPictureData(blipPart, null);
        final XMLSlideShow ppt = this.getSlideShow();
        final int pictureIdx = ppt.addPicture(data.getData(), data.getPictureType());
        final PackagePart pic = ppt.getAllPictures().get(pictureIdx).getPackagePart();
        final PackageRelationship rel = this.getPackagePart().addRelationship(pic.getPartName(), TargetMode.INTERNAL, blipRel.getRelationshipType());
        this.addRelation(rel.getId(), new XSLFPictureData(pic, rel));
        return rel.getId();
    }
}
