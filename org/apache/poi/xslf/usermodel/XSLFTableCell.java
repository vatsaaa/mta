// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.xslf.usermodel;

import org.openxmlformats.schemas.drawingml.x2006.main.STTextAnchoringType;
import org.openxmlformats.schemas.drawingml.x2006.main.CTSolidColorFillProperties;
import org.openxmlformats.schemas.drawingml.x2006.main.CTLineEndProperties;
import org.openxmlformats.schemas.drawingml.x2006.main.CTSRgbColor;
import org.openxmlformats.schemas.drawingml.x2006.main.STLineEndLength;
import org.openxmlformats.schemas.drawingml.x2006.main.STLineEndWidth;
import org.openxmlformats.schemas.drawingml.x2006.main.STLineEndType;
import org.openxmlformats.schemas.drawingml.x2006.main.STLineCap;
import org.openxmlformats.schemas.drawingml.x2006.main.STPenAlignment;
import org.openxmlformats.schemas.drawingml.x2006.main.STCompoundLine;
import org.openxmlformats.schemas.drawingml.x2006.main.STPresetLineDashVal;
import java.awt.Color;
import org.openxmlformats.schemas.drawingml.x2006.main.CTLineProperties;
import org.apache.poi.util.Units;
import org.openxmlformats.schemas.drawingml.x2006.main.CTTableCellProperties;
import org.openxmlformats.schemas.drawingml.x2006.main.CTTextBody;
import org.apache.xmlbeans.XmlObject;
import org.openxmlformats.schemas.drawingml.x2006.main.CTTableCell;

public class XSLFTableCell extends XSLFTextShape
{
    static double defaultBorderWidth;
    
    XSLFTableCell(final CTTableCell cell, final XSLFSheet sheet) {
        super(cell, sheet);
    }
    
    @Override
    public CTTableCell getXmlObject() {
        return (CTTableCell)super.getXmlObject();
    }
    
    @Override
    protected CTTextBody getTextBody(final boolean create) {
        final CTTableCell cell = this.getXmlObject();
        CTTextBody txBody = cell.getTxBody();
        if (txBody == null && create) {
            txBody = cell.addNewTxBody();
            txBody.addNewBodyPr();
            txBody.addNewLstStyle();
        }
        return txBody;
    }
    
    static CTTableCell prototype() {
        final CTTableCell cell = CTTableCell.Factory.newInstance();
        final CTTableCellProperties pr = cell.addNewTcPr();
        pr.addNewLnL().addNewNoFill();
        pr.addNewLnR().addNewNoFill();
        pr.addNewLnT().addNewNoFill();
        pr.addNewLnB().addNewNoFill();
        return cell;
    }
    
    @Override
    public void setLeftInset(final double margin) {
        CTTableCellProperties pr = this.getXmlObject().getTcPr();
        if (pr == null) {
            pr = this.getXmlObject().addNewTcPr();
        }
        pr.setMarL(Units.toEMU(margin));
    }
    
    @Override
    public void setRightInset(final double margin) {
        CTTableCellProperties pr = this.getXmlObject().getTcPr();
        if (pr == null) {
            pr = this.getXmlObject().addNewTcPr();
        }
        pr.setMarR(Units.toEMU(margin));
    }
    
    @Override
    public void setTopInset(final double margin) {
        CTTableCellProperties pr = this.getXmlObject().getTcPr();
        if (pr == null) {
            pr = this.getXmlObject().addNewTcPr();
        }
        pr.setMarT(Units.toEMU(margin));
    }
    
    @Override
    public void setBottomInset(final double margin) {
        CTTableCellProperties pr = this.getXmlObject().getTcPr();
        if (pr == null) {
            pr = this.getXmlObject().addNewTcPr();
        }
        pr.setMarB(Units.toEMU(margin));
    }
    
    public void setBorderLeft(final double width) {
        final CTTableCellProperties pr = this.getXmlObject().getTcPr();
        final CTLineProperties ln = pr.isSetLnL() ? pr.getLnL() : pr.addNewLnL();
        ln.setW(Units.toEMU(width));
    }
    
    public double getBorderLeft() {
        final CTTableCellProperties pr = this.getXmlObject().getTcPr();
        final CTLineProperties ln = pr.getLnL();
        return (ln == null || !ln.isSetW()) ? XSLFTableCell.defaultBorderWidth : Units.toPoints(ln.getW());
    }
    
    public void setBorderLeftColor(final Color color) {
        final CTTableCellProperties pr = this.getXmlObject().getTcPr();
        final CTLineProperties ln = pr.isSetLnL() ? pr.getLnL() : pr.addNewLnL();
        this.setLineColor(ln, color);
    }
    
    public Color getBorderLeftColor() {
        return this.getLineColor(this.getXmlObject().getTcPr().getLnL());
    }
    
    public void setBorderRight(final double width) {
        final CTTableCellProperties pr = this.getXmlObject().getTcPr();
        final CTLineProperties ln = pr.isSetLnR() ? pr.getLnR() : pr.addNewLnR();
        ln.setW(Units.toEMU(width));
    }
    
    public double getBorderRight() {
        final CTTableCellProperties pr = this.getXmlObject().getTcPr();
        final CTLineProperties ln = pr.getLnR();
        return (ln == null || !ln.isSetW()) ? XSLFTableCell.defaultBorderWidth : Units.toPoints(ln.getW());
    }
    
    public void setBorderRightColor(final Color color) {
        final CTTableCellProperties pr = this.getXmlObject().getTcPr();
        final CTLineProperties ln = pr.isSetLnR() ? pr.getLnR() : pr.addNewLnR();
        this.setLineColor(ln, color);
    }
    
    public Color getBorderRightColor() {
        return this.getLineColor(this.getXmlObject().getTcPr().getLnR());
    }
    
    public void setBorderTop(final double width) {
        final CTTableCellProperties pr = this.getXmlObject().getTcPr();
        final CTLineProperties ln = pr.isSetLnT() ? pr.getLnT() : pr.addNewLnT();
        ln.setW(Units.toEMU(width));
    }
    
    public double getBorderTop() {
        final CTTableCellProperties pr = this.getXmlObject().getTcPr();
        final CTLineProperties ln = pr.getLnT();
        return (ln == null || !ln.isSetW()) ? XSLFTableCell.defaultBorderWidth : Units.toPoints(ln.getW());
    }
    
    public void setBorderTopColor(final Color color) {
        final CTTableCellProperties pr = this.getXmlObject().getTcPr();
        final CTLineProperties ln = pr.isSetLnT() ? pr.getLnT() : pr.addNewLnT();
        this.setLineColor(ln, color);
    }
    
    public Color getBorderTopColor() {
        return this.getLineColor(this.getXmlObject().getTcPr().getLnT());
    }
    
    public void setBorderBottom(final double width) {
        final CTTableCellProperties pr = this.getXmlObject().getTcPr();
        final CTLineProperties ln = pr.isSetLnB() ? pr.getLnB() : pr.addNewLnB();
        ln.setW(Units.toEMU(width));
    }
    
    public double getBorderBottom() {
        final CTTableCellProperties pr = this.getXmlObject().getTcPr();
        final CTLineProperties ln = pr.getLnB();
        return (ln == null || !ln.isSetW()) ? XSLFTableCell.defaultBorderWidth : Units.toPoints(ln.getW());
    }
    
    public void setBorderBottomColor(final Color color) {
        final CTTableCellProperties pr = this.getXmlObject().getTcPr();
        final CTLineProperties ln = pr.isSetLnB() ? pr.getLnB() : pr.addNewLnB();
        this.setLineColor(ln, color);
    }
    
    public Color getBorderBottomColor() {
        return this.getLineColor(this.getXmlObject().getTcPr().getLnB());
    }
    
    private void setLineColor(final CTLineProperties ln, final Color color) {
        if (color == null) {
            ln.addNewNoFill();
            if (ln.isSetSolidFill()) {
                ln.unsetSolidFill();
            }
        }
        else {
            if (ln.isSetNoFill()) {
                ln.unsetNoFill();
            }
            if (!ln.isSetPrstDash()) {
                ln.addNewPrstDash().setVal(STPresetLineDashVal.SOLID);
            }
            ln.setCmpd(STCompoundLine.SNG);
            ln.setAlgn(STPenAlignment.CTR);
            ln.setCap(STLineCap.FLAT);
            ln.addNewRound();
            final CTLineEndProperties hd = ln.addNewHeadEnd();
            hd.setType(STLineEndType.NONE);
            hd.setW(STLineEndWidth.MED);
            hd.setLen(STLineEndLength.MED);
            final CTLineEndProperties tl = ln.addNewTailEnd();
            tl.setType(STLineEndType.NONE);
            tl.setW(STLineEndWidth.MED);
            tl.setLen(STLineEndLength.MED);
            final CTSRgbColor rgb = CTSRgbColor.Factory.newInstance();
            rgb.setVal(new byte[] { (byte)color.getRed(), (byte)color.getGreen(), (byte)color.getBlue() });
            ln.addNewSolidFill().setSrgbClr(rgb);
        }
    }
    
    private Color getLineColor(final CTLineProperties ln) {
        if (ln == null || ln.isSetNoFill() || !ln.isSetSolidFill()) {
            return null;
        }
        final CTSolidColorFillProperties fill = ln.getSolidFill();
        if (!fill.isSetSrgbClr()) {
            return null;
        }
        final byte[] val = fill.getSrgbClr().getVal();
        return new Color(0xFF & val[0], 0xFF & val[1], 0xFF & val[2]);
    }
    
    @Override
    public void setFillColor(final Color color) {
        final CTTableCellProperties spPr = this.getXmlObject().getTcPr();
        if (color == null) {
            if (spPr.isSetSolidFill()) {
                spPr.unsetSolidFill();
            }
        }
        else {
            final CTSolidColorFillProperties fill = spPr.isSetSolidFill() ? spPr.getSolidFill() : spPr.addNewSolidFill();
            final CTSRgbColor rgb = CTSRgbColor.Factory.newInstance();
            rgb.setVal(new byte[] { (byte)color.getRed(), (byte)color.getGreen(), (byte)color.getBlue() });
            fill.setSrgbClr(rgb);
        }
    }
    
    @Override
    public Color getFillColor() {
        final CTTableCellProperties spPr = this.getXmlObject().getTcPr();
        if (!spPr.isSetSolidFill()) {
            return null;
        }
        final CTSolidColorFillProperties fill = spPr.getSolidFill();
        if (!fill.isSetSrgbClr()) {
            return null;
        }
        final byte[] val = fill.getSrgbClr().getVal();
        return new Color(0xFF & val[0], 0xFF & val[1], 0xFF & val[2]);
    }
    
    void setGridSpan(final int gridSpan_) {
        this.getXmlObject().setGridSpan(gridSpan_);
    }
    
    void setRowSpan(final int rowSpan_) {
        this.getXmlObject().setRowSpan(rowSpan_);
    }
    
    void setHMerge(final boolean merge_) {
        this.getXmlObject().setHMerge(merge_);
    }
    
    void setVMerge(final boolean merge_) {
        this.getXmlObject().setVMerge(merge_);
    }
    
    @Override
    public void setVerticalAlignment(final VerticalAlignment anchor) {
        final CTTableCellProperties cellProps = this.getXmlObject().getTcPr();
        if (cellProps != null) {
            if (anchor == null) {
                if (cellProps.isSetAnchor()) {
                    cellProps.unsetAnchor();
                }
            }
            else {
                cellProps.setAnchor(STTextAnchoringType.Enum.forInt(anchor.ordinal() + 1));
            }
        }
    }
    
    @Override
    public VerticalAlignment getVerticalAlignment() {
        final CTTableCellProperties cellProps = this.getXmlObject().getTcPr();
        VerticalAlignment align = VerticalAlignment.TOP;
        if (cellProps != null && cellProps.isSetAnchor()) {
            final int ival = cellProps.getAnchor().intValue();
            align = VerticalAlignment.values()[ival - 1];
        }
        return align;
    }
    
    static {
        XSLFTableCell.defaultBorderWidth = 1.0;
    }
}
