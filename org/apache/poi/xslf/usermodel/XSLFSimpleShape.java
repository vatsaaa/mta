// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.xslf.usermodel;

import java.util.LinkedHashMap;
import java.util.ArrayList;
import java.util.List;
import java.awt.Shape;
import java.awt.geom.GeneralPath;
import java.awt.geom.Ellipse2D;
import org.apache.poi.xslf.model.geom.Path;
import java.awt.geom.AffineTransform;
import org.openxmlformats.schemas.drawingml.x2006.main.STLineEndLength;
import org.openxmlformats.schemas.drawingml.x2006.main.STLineEndWidth;
import org.openxmlformats.schemas.drawingml.x2006.main.CTLineEndProperties;
import org.openxmlformats.schemas.drawingml.x2006.main.STLineEndType;
import org.openxmlformats.schemas.drawingml.x2006.main.CTBlip;
import org.apache.poi.xslf.model.geom.PresetGeometries;
import org.apache.poi.xslf.model.geom.CustomGeometry;
import java.util.Iterator;
import org.apache.poi.xslf.model.geom.Outline;
import org.openxmlformats.schemas.drawingml.x2006.main.CTEffectStyleItem;
import org.openxmlformats.schemas.drawingml.x2006.main.STLineCap;
import org.openxmlformats.schemas.drawingml.x2006.main.STPresetLineDashVal;
import org.openxmlformats.schemas.drawingml.x2006.main.CTPresetLineDashProperties;
import java.awt.Paint;
import java.awt.Graphics2D;
import org.openxmlformats.schemas.drawingml.x2006.main.CTSolidColorFillProperties;
import org.openxmlformats.schemas.drawingml.x2006.main.CTSRgbColor;
import java.awt.Color;
import org.openxmlformats.schemas.drawingml.x2006.main.CTStyleMatrix;
import org.openxmlformats.schemas.drawingml.x2006.main.CTLineProperties;
import org.openxmlformats.schemas.drawingml.x2006.main.CTPositiveSize2D;
import org.openxmlformats.schemas.drawingml.x2006.main.CTPoint2D;
import org.apache.poi.util.Units;
import java.awt.geom.Rectangle2D;
import org.apache.poi.xslf.model.PropertyFetcher;
import org.openxmlformats.schemas.drawingml.x2006.main.CTTransform2D;
import org.openxmlformats.schemas.drawingml.x2006.main.STShapeType;
import org.openxmlformats.schemas.presentationml.x2006.main.CTShape;
import org.openxmlformats.schemas.presentationml.x2006.main.CTPlaceholder;
import org.openxmlformats.schemas.drawingml.x2006.main.CTNonVisualDrawingProps;
import org.openxmlformats.schemas.drawingml.x2006.main.CTShapeStyle;
import org.openxmlformats.schemas.drawingml.x2006.main.CTShapeProperties;
import org.apache.xmlbeans.XmlObject;
import org.openxmlformats.schemas.drawingml.x2006.main.CTOuterShadowEffect;

public abstract class XSLFSimpleShape extends XSLFShape
{
    private static CTOuterShadowEffect NO_SHADOW;
    private final XmlObject _shape;
    private final XSLFSheet _sheet;
    private CTShapeProperties _spPr;
    private CTShapeStyle _spStyle;
    private CTNonVisualDrawingProps _nvPr;
    private CTPlaceholder _ph;
    
    XSLFSimpleShape(final XmlObject shape, final XSLFSheet sheet) {
        this._shape = shape;
        this._sheet = sheet;
    }
    
    @Override
    public XmlObject getXmlObject() {
        return this._shape;
    }
    
    public XSLFSheet getSheet() {
        return this._sheet;
    }
    
    public void setShapeType(final XSLFShapeType type) {
        final CTShape shape = (CTShape)this.getXmlObject();
        final STShapeType.Enum geom = STShapeType.Enum.forInt(type.getIndex());
        shape.getSpPr().getPrstGeom().setPrst(geom);
    }
    
    public XSLFShapeType getShapeType() {
        final CTShape shape = (CTShape)this.getXmlObject();
        final STShapeType.Enum geom = shape.getSpPr().getPrstGeom().getPrst();
        return XSLFShapeType.forInt(geom.intValue());
    }
    
    @Override
    public String getShapeName() {
        return this.getNvPr().getName();
    }
    
    @Override
    public int getShapeId() {
        return (int)this.getNvPr().getId();
    }
    
    protected CTNonVisualDrawingProps getNvPr() {
        if (this._nvPr == null) {
            final XmlObject[] rs = this._shape.selectPath("declare namespace p='http://schemas.openxmlformats.org/presentationml/2006/main' .//*/p:cNvPr");
            if (rs.length != 0) {
                this._nvPr = (CTNonVisualDrawingProps)rs[0];
            }
        }
        return this._nvPr;
    }
    
    protected CTShapeProperties getSpPr() {
        if (this._spPr == null) {
            for (final XmlObject obj : this._shape.selectPath("*")) {
                if (obj instanceof CTShapeProperties) {
                    this._spPr = (CTShapeProperties)obj;
                }
            }
        }
        if (this._spPr == null) {
            throw new IllegalStateException("CTShapeProperties was not found.");
        }
        return this._spPr;
    }
    
    protected CTShapeStyle getSpStyle() {
        if (this._spStyle == null) {
            for (final XmlObject obj : this._shape.selectPath("*")) {
                if (obj instanceof CTShapeStyle) {
                    this._spStyle = (CTShapeStyle)obj;
                }
            }
        }
        return this._spStyle;
    }
    
    protected CTPlaceholder getCTPlaceholder() {
        if (this._ph == null) {
            final XmlObject[] obj = this._shape.selectPath("declare namespace p='http://schemas.openxmlformats.org/presentationml/2006/main' .//*/p:nvPr/p:ph");
            if (obj.length == 1) {
                this._ph = (CTPlaceholder)obj[0];
            }
        }
        return this._ph;
    }
    
    CTTransform2D getXfrm() {
        final PropertyFetcher<CTTransform2D> fetcher = new PropertyFetcher<CTTransform2D>() {
            @Override
            public boolean fetch(final XSLFSimpleShape shape) {
                final CTShapeProperties pr = shape.getSpPr();
                if (pr.isSetXfrm()) {
                    this.setValue(pr.getXfrm());
                    return true;
                }
                return false;
            }
        };
        this.fetchShapeProperty(fetcher);
        return fetcher.getValue();
    }
    
    @Override
    public Rectangle2D getAnchor() {
        final CTTransform2D xfrm = this.getXfrm();
        final CTPoint2D off = xfrm.getOff();
        final long x = off.getX();
        final long y = off.getY();
        final CTPositiveSize2D ext = xfrm.getExt();
        final long cx = ext.getCx();
        final long cy = ext.getCy();
        return new Rectangle2D.Double(Units.toPoints(x), Units.toPoints(y), Units.toPoints(cx), Units.toPoints(cy));
    }
    
    @Override
    public void setAnchor(final Rectangle2D anchor) {
        final CTShapeProperties spPr = this.getSpPr();
        final CTTransform2D xfrm = spPr.isSetXfrm() ? spPr.getXfrm() : spPr.addNewXfrm();
        final CTPoint2D off = xfrm.isSetOff() ? xfrm.getOff() : xfrm.addNewOff();
        final long x = Units.toEMU(anchor.getX());
        final long y = Units.toEMU(anchor.getY());
        off.setX(x);
        off.setY(y);
        final CTPositiveSize2D ext = xfrm.isSetExt() ? xfrm.getExt() : xfrm.addNewExt();
        final long cx = Units.toEMU(anchor.getWidth());
        final long cy = Units.toEMU(anchor.getHeight());
        ext.setCx(cx);
        ext.setCy(cy);
    }
    
    @Override
    public void setRotation(final double theta) {
        final CTShapeProperties spPr = this.getSpPr();
        final CTTransform2D xfrm = spPr.isSetXfrm() ? spPr.getXfrm() : spPr.addNewXfrm();
        xfrm.setRot((int)(theta * 60000.0));
    }
    
    @Override
    public double getRotation() {
        final CTTransform2D xfrm = this.getXfrm();
        return xfrm.getRot() / 60000.0;
    }
    
    @Override
    public void setFlipHorizontal(final boolean flip) {
        final CTShapeProperties spPr = this.getSpPr();
        final CTTransform2D xfrm = spPr.isSetXfrm() ? spPr.getXfrm() : spPr.addNewXfrm();
        xfrm.setFlipH(flip);
    }
    
    @Override
    public void setFlipVertical(final boolean flip) {
        final CTShapeProperties spPr = this.getSpPr();
        final CTTransform2D xfrm = spPr.isSetXfrm() ? spPr.getXfrm() : spPr.addNewXfrm();
        xfrm.setFlipV(flip);
    }
    
    @Override
    public boolean getFlipHorizontal() {
        return this.getXfrm().getFlipH();
    }
    
    @Override
    public boolean getFlipVertical() {
        return this.getXfrm().getFlipV();
    }
    
    CTLineProperties getDefaultLineProperties() {
        CTLineProperties ln = null;
        final CTShapeStyle style = this.getSpStyle();
        if (style != null) {
            final int idx = (int)style.getLnRef().getIdx();
            final CTStyleMatrix styleMatrix = this._sheet.getTheme().getXmlObject().getThemeElements().getFmtScheme();
            ln = styleMatrix.getLnStyleLst().getLnArray(idx - 1);
        }
        return ln;
    }
    
    public void setLineColor(final Color color) {
        final CTShapeProperties spPr = this.getSpPr();
        if (color == null) {
            if (spPr.isSetLn() && spPr.getLn().isSetSolidFill()) {
                spPr.getLn().unsetSolidFill();
            }
        }
        else {
            final CTLineProperties ln = spPr.isSetLn() ? spPr.getLn() : spPr.addNewLn();
            final CTSRgbColor rgb = CTSRgbColor.Factory.newInstance();
            rgb.setVal(new byte[] { (byte)color.getRed(), (byte)color.getGreen(), (byte)color.getBlue() });
            final CTSolidColorFillProperties fill = ln.isSetSolidFill() ? ln.getSolidFill() : ln.addNewSolidFill();
            fill.setSrgbClr(rgb);
            if (fill.isSetHslClr()) {
                fill.unsetHslClr();
            }
            if (fill.isSetPrstClr()) {
                fill.unsetPrstClr();
            }
            if (fill.isSetSchemeClr()) {
                fill.unsetSchemeClr();
            }
            if (fill.isSetScrgbClr()) {
                fill.unsetScrgbClr();
            }
            if (fill.isSetSysClr()) {
                fill.unsetSysClr();
            }
        }
    }
    
    public Color getLineColor() {
        final RenderableShape rShape = new RenderableShape(this);
        final Paint paint = rShape.getLinePaint(null);
        if (paint instanceof Color) {
            return (Color)paint;
        }
        return null;
    }
    
    public void setLineWidth(final double width) {
        final CTShapeProperties spPr = this.getSpPr();
        if (width == 0.0) {
            if (spPr.isSetLn()) {
                spPr.getLn().unsetW();
            }
        }
        else {
            final CTLineProperties ln = spPr.isSetLn() ? spPr.getLn() : spPr.addNewLn();
            ln.setW(Units.toEMU(width));
        }
    }
    
    public double getLineWidth() {
        final PropertyFetcher<Double> fetcher = new PropertyFetcher<Double>() {
            @Override
            public boolean fetch(final XSLFSimpleShape shape) {
                final CTShapeProperties spPr = shape.getSpPr();
                final CTLineProperties ln = spPr.getLn();
                if (ln != null) {
                    if (ln.isSetNoFill()) {
                        this.setValue(0.0);
                        return true;
                    }
                    if (ln.isSetW()) {
                        this.setValue(Units.toPoints(ln.getW()));
                        return true;
                    }
                }
                return false;
            }
        };
        this.fetchShapeProperty(fetcher);
        double lineWidth = 0.0;
        if (fetcher.getValue() == null) {
            final CTLineProperties defaultLn = this.getDefaultLineProperties();
            if (defaultLn != null && defaultLn.isSetW()) {
                lineWidth = Units.toPoints(defaultLn.getW());
            }
        }
        else {
            lineWidth = fetcher.getValue();
        }
        return lineWidth;
    }
    
    public void setLineDash(final LineDash dash) {
        final CTShapeProperties spPr = this.getSpPr();
        if (dash == null) {
            if (spPr.isSetLn()) {
                spPr.getLn().unsetPrstDash();
            }
        }
        else {
            final CTPresetLineDashProperties val = CTPresetLineDashProperties.Factory.newInstance();
            val.setVal(STPresetLineDashVal.Enum.forInt(dash.ordinal() + 1));
            final CTLineProperties ln = spPr.isSetLn() ? spPr.getLn() : spPr.addNewLn();
            ln.setPrstDash(val);
        }
    }
    
    public LineDash getLineDash() {
        final PropertyFetcher<LineDash> fetcher = new PropertyFetcher<LineDash>() {
            @Override
            public boolean fetch(final XSLFSimpleShape shape) {
                final CTShapeProperties spPr = shape.getSpPr();
                final CTLineProperties ln = spPr.getLn();
                if (ln != null) {
                    final CTPresetLineDashProperties ctDash = ln.getPrstDash();
                    if (ctDash != null) {
                        this.setValue(LineDash.values()[ctDash.getVal().intValue() - 1]);
                        return true;
                    }
                }
                return false;
            }
        };
        this.fetchShapeProperty(fetcher);
        LineDash dash = fetcher.getValue();
        if (dash == null) {
            final CTLineProperties defaultLn = this.getDefaultLineProperties();
            if (defaultLn != null) {
                final CTPresetLineDashProperties ctDash = defaultLn.getPrstDash();
                if (ctDash != null) {
                    dash = LineDash.values()[ctDash.getVal().intValue() - 1];
                }
            }
        }
        return dash;
    }
    
    public void setLineCap(final LineCap cap) {
        final CTShapeProperties spPr = this.getSpPr();
        if (cap == null) {
            if (spPr.isSetLn()) {
                spPr.getLn().unsetCap();
            }
        }
        else {
            final CTLineProperties ln = spPr.isSetLn() ? spPr.getLn() : spPr.addNewLn();
            ln.setCap(STLineCap.Enum.forInt(cap.ordinal() + 1));
        }
    }
    
    public LineCap getLineCap() {
        final PropertyFetcher<LineCap> fetcher = new PropertyFetcher<LineCap>() {
            @Override
            public boolean fetch(final XSLFSimpleShape shape) {
                final CTShapeProperties spPr = shape.getSpPr();
                final CTLineProperties ln = spPr.getLn();
                if (ln != null) {
                    final STLineCap.Enum stCap = ln.getCap();
                    if (stCap != null) {
                        this.setValue(LineCap.values()[stCap.intValue() - 1]);
                        return true;
                    }
                }
                return false;
            }
        };
        this.fetchShapeProperty(fetcher);
        LineCap cap = fetcher.getValue();
        if (cap == null) {
            final CTLineProperties defaultLn = this.getDefaultLineProperties();
            if (defaultLn != null) {
                final STLineCap.Enum stCap = defaultLn.getCap();
                if (stCap != null) {
                    cap = LineCap.values()[stCap.intValue() - 1];
                }
            }
        }
        return cap;
    }
    
    public void setFillColor(final Color color) {
        final CTShapeProperties spPr = this.getSpPr();
        if (color == null) {
            if (spPr.isSetSolidFill()) {
                spPr.unsetSolidFill();
            }
            if (!spPr.isSetNoFill()) {
                spPr.addNewNoFill();
            }
        }
        else {
            if (spPr.isSetNoFill()) {
                spPr.unsetNoFill();
            }
            final CTSolidColorFillProperties fill = spPr.isSetSolidFill() ? spPr.getSolidFill() : spPr.addNewSolidFill();
            final CTSRgbColor rgb = CTSRgbColor.Factory.newInstance();
            rgb.setVal(new byte[] { (byte)color.getRed(), (byte)color.getGreen(), (byte)color.getBlue() });
            fill.setSrgbClr(rgb);
            if (fill.isSetHslClr()) {
                fill.unsetHslClr();
            }
            if (fill.isSetPrstClr()) {
                fill.unsetPrstClr();
            }
            if (fill.isSetSchemeClr()) {
                fill.unsetSchemeClr();
            }
            if (fill.isSetScrgbClr()) {
                fill.unsetScrgbClr();
            }
            if (fill.isSetSysClr()) {
                fill.unsetSysClr();
            }
        }
    }
    
    public Color getFillColor() {
        final RenderableShape rShape = new RenderableShape(this);
        final Paint paint = rShape.getFillPaint(null);
        if (paint instanceof Color) {
            return (Color)paint;
        }
        return null;
    }
    
    public XSLFShadow getShadow() {
        final PropertyFetcher<CTOuterShadowEffect> fetcher = new PropertyFetcher<CTOuterShadowEffect>() {
            @Override
            public boolean fetch(final XSLFSimpleShape shape) {
                final CTShapeProperties spPr = shape.getSpPr();
                if (spPr.isSetEffectLst()) {
                    final CTOuterShadowEffect obj = spPr.getEffectLst().getOuterShdw();
                    this.setValue((obj == null) ? XSLFSimpleShape.NO_SHADOW : obj);
                    return true;
                }
                return false;
            }
        };
        this.fetchShapeProperty(fetcher);
        CTOuterShadowEffect obj = fetcher.getValue();
        if (obj == null) {
            final CTShapeStyle style = this.getSpStyle();
            if (style != null) {
                final int idx = (int)style.getEffectRef().getIdx();
                if (idx != 0) {
                    final CTStyleMatrix styleMatrix = this._sheet.getTheme().getXmlObject().getThemeElements().getFmtScheme();
                    final CTEffectStyleItem ef = styleMatrix.getEffectStyleLst().getEffectStyleArray(idx - 1);
                    obj = ef.getEffectLst().getOuterShdw();
                }
            }
        }
        return (obj == null || obj == XSLFSimpleShape.NO_SHADOW) ? null : new XSLFShadow(obj, this);
    }
    
    @Override
    public void draw(final Graphics2D graphics) {
        final RenderableShape rShape = new RenderableShape(this);
        rShape.render(graphics);
        final Color lineColor = this.getLineColor();
        if (lineColor != null) {
            graphics.setPaint(lineColor);
            for (final Outline o : this.getDecorationOutlines(graphics)) {
                if (o.getPath().isFilled()) {
                    graphics.fill(o.getOutline());
                }
                if (o.getPath().isStroked()) {
                    graphics.draw(o.getOutline());
                }
            }
        }
    }
    
    boolean fetchShapeProperty(final PropertyFetcher visitor) {
        boolean ok = visitor.fetch(this);
        final XSLFSheet masterSheet = this.getSheet().getMasterSheet();
        final CTPlaceholder ph = this.getCTPlaceholder();
        if (masterSheet != null && ph != null) {
            if (!ok) {
                final XSLFSimpleShape masterShape = masterSheet.getPlaceholder(ph);
                if (masterShape != null) {
                    ok = visitor.fetch(masterShape);
                }
            }
            if (!ok) {
                int textType = 0;
                if (!ph.isSetType()) {
                    textType = 2;
                }
                else {
                    switch (ph.getType().intValue()) {
                        case 1:
                        case 3: {
                            textType = 1;
                            break;
                        }
                        case 5:
                        case 6:
                        case 7: {
                            textType = ph.getType().intValue();
                            break;
                        }
                        default: {
                            textType = 2;
                            break;
                        }
                    }
                }
                final XSLFSheet master = masterSheet.getMasterSheet();
                if (master != null) {
                    final XSLFSimpleShape masterShape = master.getPlaceholderByType(textType);
                    if (masterShape != null) {
                        ok = visitor.fetch(masterShape);
                    }
                }
            }
        }
        return ok;
    }
    
    CustomGeometry getGeometry() {
        final CTShapeProperties spPr = this.getSpPr();
        final PresetGeometries dict = PresetGeometries.getInstance();
        CustomGeometry geom;
        if (spPr.isSetPrstGeom()) {
            final String name = spPr.getPrstGeom().getPrst().toString();
            geom = ((LinkedHashMap<K, CustomGeometry>)dict).get(name);
            if (geom == null) {
                throw new IllegalStateException("Unknown shape geometry: " + name);
            }
        }
        else if (spPr.isSetCustGeom()) {
            geom = new CustomGeometry(spPr.getCustGeom());
        }
        else {
            geom = ((LinkedHashMap<K, CustomGeometry>)dict).get("rect");
        }
        return geom;
    }
    
    public void drawContent(final Graphics2D graphics) {
    }
    
    @Override
    void copy(final XSLFShape sh) {
        super.copy(sh);
        final XSLFSimpleShape s = (XSLFSimpleShape)sh;
        final Color srsSolidFill = s.getFillColor();
        final Color tgtSoliFill = this.getFillColor();
        if (srsSolidFill != null && !srsSolidFill.equals(tgtSoliFill)) {
            this.setFillColor(srsSolidFill);
        }
        if (this.getSpPr().isSetBlipFill()) {
            final CTBlip blip = this.getSpPr().getBlipFill().getBlip();
            final String blipId = blip.getEmbed();
            final String relId = this.getSheet().importBlip(blipId, s.getSheet().getPackagePart());
            blip.setEmbed(relId);
        }
        final Color srcLineColor = s.getLineColor();
        final Color tgtLineColor = this.getLineColor();
        if (srcLineColor != null && !srcLineColor.equals(tgtLineColor)) {
            this.setLineColor(srcLineColor);
        }
        final double srcLineWidth = s.getLineWidth();
        final double tgtLineWidth = this.getLineWidth();
        if (srcLineWidth != tgtLineWidth) {
            this.setLineWidth(srcLineWidth);
        }
        final LineDash srcLineDash = s.getLineDash();
        final LineDash tgtLineDash = this.getLineDash();
        if (srcLineDash != null && srcLineDash != tgtLineDash) {
            this.setLineDash(srcLineDash);
        }
        final LineCap srcLineCap = s.getLineCap();
        final LineCap tgtLineCap = this.getLineCap();
        if (srcLineCap != null && srcLineCap != tgtLineCap) {
            this.setLineCap(srcLineCap);
        }
    }
    
    public void setLineHeadDecoration(final LineDecoration style) {
        final CTLineProperties ln = this.getSpPr().getLn();
        final CTLineEndProperties lnEnd = ln.isSetHeadEnd() ? ln.getHeadEnd() : ln.addNewHeadEnd();
        if (style == null) {
            if (lnEnd.isSetType()) {
                lnEnd.unsetType();
            }
        }
        else {
            lnEnd.setType(STLineEndType.Enum.forInt(style.ordinal() + 1));
        }
    }
    
    public LineDecoration getLineHeadDecoration() {
        final CTLineProperties ln = this.getSpPr().getLn();
        if (ln == null || !ln.isSetHeadEnd()) {
            return LineDecoration.NONE;
        }
        final STLineEndType.Enum end = ln.getHeadEnd().getType();
        return (end == null) ? LineDecoration.NONE : LineDecoration.values()[end.intValue() - 1];
    }
    
    public void setLineHeadWidth(final LineEndWidth style) {
        final CTLineProperties ln = this.getSpPr().getLn();
        final CTLineEndProperties lnEnd = ln.isSetHeadEnd() ? ln.getHeadEnd() : ln.addNewHeadEnd();
        if (style == null) {
            if (lnEnd.isSetW()) {
                lnEnd.unsetW();
            }
        }
        else {
            lnEnd.setW(STLineEndWidth.Enum.forInt(style.ordinal() + 1));
        }
    }
    
    public LineEndWidth getLineHeadWidth() {
        final CTLineProperties ln = this.getSpPr().getLn();
        if (ln == null || !ln.isSetHeadEnd()) {
            return LineEndWidth.MEDIUM;
        }
        final STLineEndWidth.Enum w = ln.getHeadEnd().getW();
        return (w == null) ? LineEndWidth.MEDIUM : LineEndWidth.values()[w.intValue() - 1];
    }
    
    public void setLineHeadLength(final LineEndLength style) {
        final CTLineProperties ln = this.getSpPr().getLn();
        final CTLineEndProperties lnEnd = ln.isSetHeadEnd() ? ln.getHeadEnd() : ln.addNewHeadEnd();
        if (style == null) {
            if (lnEnd.isSetLen()) {
                lnEnd.unsetLen();
            }
        }
        else {
            lnEnd.setLen(STLineEndLength.Enum.forInt(style.ordinal() + 1));
        }
    }
    
    public LineEndLength getLineHeadLength() {
        final CTLineProperties ln = this.getSpPr().getLn();
        if (ln == null || !ln.isSetHeadEnd()) {
            return LineEndLength.MEDIUM;
        }
        final STLineEndLength.Enum len = ln.getHeadEnd().getLen();
        return (len == null) ? LineEndLength.MEDIUM : LineEndLength.values()[len.intValue() - 1];
    }
    
    public void setLineTailDecoration(final LineDecoration style) {
        final CTLineProperties ln = this.getSpPr().getLn();
        final CTLineEndProperties lnEnd = ln.isSetTailEnd() ? ln.getTailEnd() : ln.addNewTailEnd();
        if (style == null) {
            if (lnEnd.isSetType()) {
                lnEnd.unsetType();
            }
        }
        else {
            lnEnd.setType(STLineEndType.Enum.forInt(style.ordinal() + 1));
        }
    }
    
    public LineDecoration getLineTailDecoration() {
        final CTLineProperties ln = this.getSpPr().getLn();
        if (ln == null || !ln.isSetTailEnd()) {
            return LineDecoration.NONE;
        }
        final STLineEndType.Enum end = ln.getTailEnd().getType();
        return (end == null) ? LineDecoration.NONE : LineDecoration.values()[end.intValue() - 1];
    }
    
    public void setLineTailWidth(final LineEndWidth style) {
        final CTLineProperties ln = this.getSpPr().getLn();
        final CTLineEndProperties lnEnd = ln.isSetTailEnd() ? ln.getTailEnd() : ln.addNewTailEnd();
        if (style == null) {
            if (lnEnd.isSetW()) {
                lnEnd.unsetW();
            }
        }
        else {
            lnEnd.setW(STLineEndWidth.Enum.forInt(style.ordinal() + 1));
        }
    }
    
    public LineEndWidth getLineTailWidth() {
        final CTLineProperties ln = this.getSpPr().getLn();
        if (ln == null || !ln.isSetTailEnd()) {
            return LineEndWidth.MEDIUM;
        }
        final STLineEndWidth.Enum w = ln.getTailEnd().getW();
        return (w == null) ? LineEndWidth.MEDIUM : LineEndWidth.values()[w.intValue() - 1];
    }
    
    public void setLineTailLength(final LineEndLength style) {
        final CTLineProperties ln = this.getSpPr().getLn();
        final CTLineEndProperties lnEnd = ln.isSetTailEnd() ? ln.getTailEnd() : ln.addNewTailEnd();
        if (style == null) {
            if (lnEnd.isSetLen()) {
                lnEnd.unsetLen();
            }
        }
        else {
            lnEnd.setLen(STLineEndLength.Enum.forInt(style.ordinal() + 1));
        }
    }
    
    public LineEndLength getLineTailLength() {
        final CTLineProperties ln = this.getSpPr().getLn();
        if (ln == null || !ln.isSetTailEnd()) {
            return LineEndLength.MEDIUM;
        }
        final STLineEndLength.Enum len = ln.getTailEnd().getLen();
        return (len == null) ? LineEndLength.MEDIUM : LineEndLength.values()[len.intValue() - 1];
    }
    
    Outline getTailDecoration(final Graphics2D graphics) {
        final LineEndLength tailLength = this.getLineTailLength();
        final LineEndWidth tailWidth = this.getLineTailWidth();
        final double lineWidth = Math.max(2.5, this.getLineWidth());
        final Rectangle2D anchor = new RenderableShape(this).getAnchor(graphics);
        final double x2 = anchor.getX() + anchor.getWidth();
        final double y2 = anchor.getY() + anchor.getHeight();
        final double alpha = Math.atan(anchor.getHeight() / anchor.getWidth());
        final AffineTransform at = new AffineTransform();
        Shape shape = null;
        Path p = null;
        double scaleY = Math.pow(2.0, tailWidth.ordinal());
        double scaleX = Math.pow(2.0, tailLength.ordinal());
        switch (this.getLineTailDecoration()) {
            case OVAL: {
                p = new Path();
                shape = new Ellipse2D.Double(0.0, 0.0, lineWidth * scaleX, lineWidth * scaleY);
                final Rectangle2D bounds = shape.getBounds2D();
                at.translate(x2 - bounds.getWidth() / 2.0, y2 - bounds.getHeight() / 2.0);
                at.rotate(alpha, bounds.getX() + bounds.getWidth() / 2.0, bounds.getY() + bounds.getHeight() / 2.0);
                break;
            }
            case ARROW: {
                p = new Path();
                final GeneralPath arrow = new GeneralPath();
                arrow.moveTo((float)(-lineWidth * 3.0), (float)(-lineWidth * 2.0));
                arrow.lineTo(0.0f, 0.0f);
                arrow.lineTo((float)(-lineWidth * 3.0), (float)(lineWidth * 2.0));
                shape = arrow;
                at.translate(x2, y2);
                at.rotate(alpha);
                break;
            }
            case TRIANGLE: {
                p = new Path();
                scaleY = tailWidth.ordinal() + 1;
                scaleX = tailLength.ordinal() + 1;
                final GeneralPath triangle = new GeneralPath();
                triangle.moveTo((float)(-lineWidth * scaleX), (float)(-lineWidth * scaleY / 2.0));
                triangle.lineTo(0.0f, 0.0f);
                triangle.lineTo((float)(-lineWidth * scaleX), (float)(lineWidth * scaleY / 2.0));
                triangle.closePath();
                shape = triangle;
                at.translate(x2, y2);
                at.rotate(alpha);
                break;
            }
        }
        if (shape != null) {
            shape = at.createTransformedShape(shape);
        }
        return (shape == null) ? null : new Outline(shape, p);
    }
    
    Outline getHeadDecoration(final Graphics2D graphics) {
        final LineEndLength headLength = this.getLineHeadLength();
        final LineEndWidth headWidth = this.getLineHeadWidth();
        final double lineWidth = Math.max(2.5, this.getLineWidth());
        final Rectangle2D anchor = new RenderableShape(this).getAnchor(graphics);
        final double x1 = anchor.getX();
        final double y1 = anchor.getY();
        final double alpha = Math.atan(anchor.getHeight() / anchor.getWidth());
        final AffineTransform at = new AffineTransform();
        Shape shape = null;
        Path p = null;
        double scaleY = 1.0;
        double scaleX = 1.0;
        switch (this.getLineHeadDecoration()) {
            case OVAL: {
                p = new Path();
                shape = new Ellipse2D.Double(0.0, 0.0, lineWidth * scaleX, lineWidth * scaleY);
                final Rectangle2D bounds = shape.getBounds2D();
                at.translate(x1 - bounds.getWidth() / 2.0, y1 - bounds.getHeight() / 2.0);
                at.rotate(alpha, bounds.getX() + bounds.getWidth() / 2.0, bounds.getY() + bounds.getHeight() / 2.0);
                break;
            }
            case ARROW:
            case STEALTH: {
                p = new Path(false, true);
                final GeneralPath arrow = new GeneralPath();
                arrow.moveTo((float)(lineWidth * 3.0 * scaleX), (float)(-lineWidth * scaleY * 2.0));
                arrow.lineTo(0.0f, 0.0f);
                arrow.lineTo((float)(lineWidth * 3.0 * scaleX), (float)(lineWidth * scaleY * 2.0));
                shape = arrow;
                at.translate(x1, y1);
                at.rotate(alpha);
                break;
            }
            case TRIANGLE: {
                p = new Path();
                scaleY = headWidth.ordinal() + 1;
                scaleX = headLength.ordinal() + 1;
                final GeneralPath triangle = new GeneralPath();
                triangle.moveTo((float)(lineWidth * scaleX), (float)(-lineWidth * scaleY / 2.0));
                triangle.lineTo(0.0f, 0.0f);
                triangle.lineTo((float)(lineWidth * scaleX), (float)(lineWidth * scaleY / 2.0));
                triangle.closePath();
                shape = triangle;
                at.translate(x1, y1);
                at.rotate(alpha);
                break;
            }
        }
        if (shape != null) {
            shape = at.createTransformedShape(shape);
        }
        return (shape == null) ? null : new Outline(shape, p);
    }
    
    private List<Outline> getDecorationOutlines(final Graphics2D graphics) {
        final List<Outline> lst = new ArrayList<Outline>();
        final Outline head = this.getHeadDecoration(graphics);
        if (head != null) {
            lst.add(head);
        }
        final Outline tail = this.getTailDecoration(graphics);
        if (tail != null) {
            lst.add(tail);
        }
        return lst;
    }
    
    static {
        XSLFSimpleShape.NO_SHADOW = CTOuterShadowEffect.Factory.newInstance();
    }
}
