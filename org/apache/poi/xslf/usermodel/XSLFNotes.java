// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.xslf.usermodel;

import org.apache.xmlbeans.XmlObject;
import java.util.Iterator;
import org.apache.poi.POIXMLDocumentPart;
import org.openxmlformats.schemas.presentationml.x2006.main.CTCommonSlideData;
import org.apache.xmlbeans.XmlException;
import java.io.IOException;
import org.openxmlformats.schemas.presentationml.x2006.main.NotesDocument;
import org.apache.poi.openxml4j.opc.PackageRelationship;
import org.apache.poi.openxml4j.opc.PackagePart;
import org.openxmlformats.schemas.presentationml.x2006.main.CTNotesSlide;

public final class XSLFNotes extends XSLFSheet
{
    private CTNotesSlide _notes;
    
    XSLFNotes() {
        this._notes = prototype();
        this.setCommonSlideData(this._notes.getCSld());
    }
    
    XSLFNotes(final PackagePart part, final PackageRelationship rel) throws IOException, XmlException {
        super(part, rel);
        final NotesDocument doc = NotesDocument.Factory.parse(this.getPackagePart().getInputStream());
        this._notes = doc.getNotes();
        this.setCommonSlideData(this._notes.getCSld());
    }
    
    private static CTNotesSlide prototype() {
        final CTNotesSlide ctNotes = CTNotesSlide.Factory.newInstance();
        final CTCommonSlideData cSld = ctNotes.addNewCSld();
        return ctNotes;
    }
    
    @Override
    public CTNotesSlide getXmlObject() {
        return this._notes;
    }
    
    @Override
    protected String getRootElementName() {
        return "notes";
    }
    
    @Override
    public XSLFNotesMaster getMasterSheet() {
        for (final POIXMLDocumentPart p : this.getRelations()) {
            if (p instanceof XSLFNotesMaster) {
                return (XSLFNotesMaster)p;
            }
        }
        return null;
    }
}
