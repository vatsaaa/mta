// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.xslf.usermodel;

import org.openxmlformats.schemas.drawingml.x2006.main.CTTransform2D;
import java.awt.Color;
import org.openxmlformats.schemas.drawingml.x2006.main.CTBackgroundFillStyleList;
import org.openxmlformats.schemas.drawingml.x2006.main.CTStyleMatrixReference;
import org.openxmlformats.schemas.drawingml.x2006.main.CTSchemeColor;
import java.awt.Paint;
import java.awt.Shape;
import java.awt.Graphics2D;
import java.awt.Dimension;
import java.awt.geom.Rectangle2D;
import org.apache.xmlbeans.XmlObject;
import org.openxmlformats.schemas.presentationml.x2006.main.CTBackground;

public class XSLFBackground extends XSLFSimpleShape
{
    XSLFBackground(final CTBackground shape, final XSLFSheet sheet) {
        super(shape, sheet);
    }
    
    @Override
    public Rectangle2D getAnchor() {
        final Dimension pg = this.getSheet().getSlideShow().getPageSize();
        return new Rectangle2D.Double(0.0, 0.0, pg.getWidth(), pg.getHeight());
    }
    
    @Override
    public void draw(final Graphics2D graphics) {
        final Rectangle2D anchor = this.getAnchor();
        final Paint fill = this.getPaint(graphics);
        if (fill != null) {
            graphics.setPaint(fill);
            graphics.fill(anchor);
        }
    }
    
    Paint getPaint(final Graphics2D graphics) {
        final RenderableShape rShape = new RenderableShape(this);
        Paint fill = null;
        final CTBackground bg = (CTBackground)this.getXmlObject();
        if (bg.isSetBgPr()) {
            final XmlObject spPr = bg.getBgPr();
            fill = rShape.getPaint(graphics, spPr, null);
        }
        else if (bg.isSetBgRef()) {
            final CTStyleMatrixReference bgRef = bg.getBgRef();
            final CTSchemeColor phClr = bgRef.getSchemeClr();
            final int idx = (int)bgRef.getIdx() - 1001;
            final XSLFTheme theme = this.getSheet().getTheme();
            final CTBackgroundFillStyleList bgStyles = theme.getXmlObject().getThemeElements().getFmtScheme().getBgFillStyleLst();
            final XmlObject bgStyle = bgStyles.selectPath("*")[idx];
            fill = rShape.selectPaint(graphics, bgStyle, phClr, theme.getPackagePart());
        }
        return fill;
    }
    
    @Override
    public Color getFillColor() {
        final Paint p = this.getPaint(null);
        if (p instanceof Color) {
            return (Color)p;
        }
        return null;
    }
    
    @Override
    CTTransform2D getXfrm() {
        return CTTransform2D.Factory.newInstance();
    }
}
