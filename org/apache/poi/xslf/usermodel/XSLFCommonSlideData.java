// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.xslf.usermodel;

import org.openxmlformats.schemas.presentationml.x2006.main.CTApplicationNonVisualDrawingProps;
import org.openxmlformats.schemas.drawingml.x2006.main.CTTextBody;
import org.openxmlformats.schemas.presentationml.x2006.main.CTShape;
import java.util.Collection;
import java.util.Arrays;
import org.apache.xmlbeans.XmlObject;
import org.apache.xmlbeans.XmlCursor;
import org.openxmlformats.schemas.drawingml.x2006.main.CTGraphicalObjectData;
import java.util.Iterator;
import org.apache.xmlbeans.XmlException;
import org.apache.poi.POIXMLException;
import org.apache.xmlbeans.impl.values.XmlAnyTypeImpl;
import org.openxmlformats.schemas.drawingml.x2006.main.CTTable;
import org.openxmlformats.schemas.presentationml.x2006.main.CTGraphicalObjectFrame;
import org.openxmlformats.schemas.presentationml.x2006.main.CTGroupShape;
import java.util.ArrayList;
import java.util.List;
import org.openxmlformats.schemas.presentationml.x2006.main.CTCommonSlideData;

public class XSLFCommonSlideData
{
    private final CTCommonSlideData data;
    
    public XSLFCommonSlideData(final CTCommonSlideData data) {
        this.data = data;
    }
    
    public List<DrawingTextBody> getDrawingText() {
        final CTGroupShape gs = this.data.getSpTree();
        final List<DrawingTextBody> out = new ArrayList<DrawingTextBody>();
        this.processShape(gs, out);
        for (final CTGroupShape shape : gs.getGrpSpList()) {
            this.processShape(shape, out);
        }
        for (final CTGraphicalObjectFrame frame : gs.getGraphicFrameList()) {
            final CTGraphicalObjectData data = frame.getGraphic().getGraphicData();
            final XmlCursor c = data.newCursor();
            c.selectPath("declare namespace pic='" + CTTable.type.getName().getNamespaceURI() + "' .//pic:tbl");
            while (c.toNextSelection()) {
                XmlObject o = c.getObject();
                if (o instanceof XmlAnyTypeImpl) {
                    try {
                        o = CTTable.Factory.parse(o.toString());
                    }
                    catch (XmlException e) {
                        throw new POIXMLException(e);
                    }
                }
                if (o instanceof CTTable) {
                    final DrawingTable table = new DrawingTable((CTTable)o);
                    for (final DrawingTableRow row : table.getRows()) {
                        for (final DrawingTableCell cell : row.getCells()) {
                            final DrawingTextBody textBody = cell.getTextBody();
                            out.add(textBody);
                        }
                    }
                }
            }
            c.dispose();
        }
        return out;
    }
    
    public List<DrawingParagraph> getText() {
        final List<DrawingParagraph> paragraphs = new ArrayList<DrawingParagraph>();
        for (final DrawingTextBody textBody : this.getDrawingText()) {
            paragraphs.addAll(Arrays.asList(textBody.getParagraphs()));
        }
        return paragraphs;
    }
    
    private void processShape(final CTGroupShape gs, final List<DrawingTextBody> out) {
        final List<CTShape> shapes = gs.getSpList();
        for (final CTShape shape : shapes) {
            final CTTextBody ctTextBody = shape.getTxBody();
            if (ctTextBody == null) {
                continue;
            }
            final CTApplicationNonVisualDrawingProps nvpr = shape.getNvSpPr().getNvPr();
            DrawingTextBody textBody;
            if (nvpr.isSetPh()) {
                textBody = new DrawingTextPlaceholder(ctTextBody, nvpr.getPh());
            }
            else {
                textBody = new DrawingTextBody(ctTextBody);
            }
            out.add(textBody);
        }
    }
}
