// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.xslf.usermodel;

import org.apache.xmlbeans.XmlObject;
import org.apache.xmlbeans.XmlCursor;
import org.openxmlformats.schemas.drawingml.x2006.main.CTTextLineBreak;
import org.openxmlformats.schemas.drawingml.x2006.main.CTRegularTextRun;
import org.openxmlformats.schemas.drawingml.x2006.main.CTTextParagraph;

public class DrawingParagraph
{
    private final CTTextParagraph p;
    
    public DrawingParagraph(final CTTextParagraph p) {
        this.p = p;
    }
    
    public CharSequence getText() {
        final StringBuilder text = new StringBuilder();
        final XmlCursor c = this.p.newCursor();
        c.selectPath("./*");
        while (c.toNextSelection()) {
            final XmlObject o = c.getObject();
            if (o instanceof CTRegularTextRun) {
                final CTRegularTextRun txrun = (CTRegularTextRun)o;
                text.append(txrun.getT());
            }
            else {
                if (!(o instanceof CTTextLineBreak)) {
                    continue;
                }
                text.append('\n');
            }
        }
        c.dispose();
        return text;
    }
}
