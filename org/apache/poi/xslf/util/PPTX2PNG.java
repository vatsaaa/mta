// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.xslf.util;

import java.awt.Graphics2D;
import org.apache.poi.xslf.usermodel.XSLFSlide;
import java.awt.Dimension;
import java.io.OutputStream;
import java.awt.image.RenderedImage;
import javax.imageio.ImageIO;
import java.io.FileOutputStream;
import java.awt.Color;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import org.apache.poi.xslf.usermodel.XMLSlideShow;
import org.apache.poi.openxml4j.opc.OPCPackage;

public class PPTX2PNG
{
    static void usage() {
        System.out.println("Usage: PPTX2PNG [options] <pptx file>");
        System.out.println("Options:");
        System.out.println("    -scale <float>   scale factor");
        System.out.println("    -slide <integer> 1-based index of a slide to render");
    }
    
    public static void main(final String[] args) throws Exception {
        if (args.length == 0) {
            usage();
            return;
        }
        int slidenum = -1;
        float scale = 1.0f;
        String file = null;
        for (int i = 0; i < args.length; ++i) {
            if (args[i].startsWith("-")) {
                if ("-scale".equals(args[i])) {
                    scale = Float.parseFloat(args[++i]);
                }
                else if ("-slide".equals(args[i])) {
                    slidenum = Integer.parseInt(args[++i]);
                }
            }
            else {
                file = args[i];
            }
        }
        if (file == null) {
            usage();
            return;
        }
        System.out.println("Processing " + file);
        final XMLSlideShow ppt = new XMLSlideShow(OPCPackage.open(file));
        final Dimension pgsize = ppt.getPageSize();
        final int width = (int)(pgsize.width * scale);
        final int height = (int)(pgsize.height * scale);
        final XSLFSlide[] slide = ppt.getSlides();
        for (int j = 0; j < slide.length; ++j) {
            if (slidenum == -1 || slidenum == j + 1) {
                final String title = slide[j].getTitle();
                System.out.println("Rendering slide " + (j + 1) + ((title == null) ? "" : (": " + title)));
                final BufferedImage img = new BufferedImage(width, height, 1);
                final Graphics2D graphics = img.createGraphics();
                graphics.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
                graphics.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
                graphics.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BICUBIC);
                graphics.setRenderingHint(RenderingHints.KEY_FRACTIONALMETRICS, RenderingHints.VALUE_FRACTIONALMETRICS_ON);
                graphics.setColor(Color.white);
                graphics.clearRect(0, 0, width, height);
                graphics.scale(scale, scale);
                slide[j].draw(graphics);
                final int sep = file.lastIndexOf(".");
                final String fname = file.substring(0, (sep == -1) ? file.length() : sep) + "-" + (j + 1) + ".png";
                final FileOutputStream out = new FileOutputStream(fname);
                ImageIO.write(img, "png", out);
                out.close();
            }
        }
        System.out.println("Done");
    }
}
