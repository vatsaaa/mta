// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.xslf.model.geom;

import org.openxmlformats.schemas.drawingml.x2006.main.CTGeomGuide;

public class AdjustValue extends Guide
{
    public AdjustValue(final CTGeomGuide gd) {
        super(gd.getName(), gd.getFmla());
    }
    
    @Override
    public double evaluate(final Context ctx) {
        final String name = this.getName();
        final Guide adj = ctx.getAdjustValue(name);
        if (adj != null) {
            return adj.evaluate(ctx);
        }
        return super.evaluate(ctx);
    }
}
