// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.xslf.model.geom;

import java.util.regex.Matcher;

public class SinExpression implements Expression
{
    private String arg1;
    private String arg2;
    
    SinExpression(final Matcher m) {
        this.arg1 = m.group(1);
        this.arg2 = m.group(2);
    }
    
    public double evaluate(final Context ctx) {
        final double x = ctx.getValue(this.arg1);
        final double y = ctx.getValue(this.arg2) / 60000.0;
        return x * Math.sin(Math.toRadians(y));
    }
}
