// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.xslf.model.geom;

import org.openxmlformats.schemas.drawingml.x2006.main.CTCustomGeometry2D;
import org.apache.xmlbeans.XmlObject;
import java.io.InputStream;
import org.apache.poi.xslf.usermodel.XMLSlideShow;
import java.util.LinkedHashMap;

public class PresetGeometries extends LinkedHashMap<String, CustomGeometry>
{
    private static PresetGeometries _inst;
    
    private PresetGeometries() {
        try {
            final InputStream is = XMLSlideShow.class.getResourceAsStream("presetShapeDefinitions.xml");
            this.read(is);
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
    
    private void read(final InputStream is) throws Exception {
        final XmlObject obj = XmlObject.Factory.parse(is);
        for (final XmlObject def : obj.selectPath("*/*")) {
            final String name = def.getDomNode().getLocalName();
            final CTCustomGeometry2D geom = CTCustomGeometry2D.Factory.parse(def.toString());
            if (this.containsKey(name)) {
                System.out.println("Duplicate definoition of " + name);
            }
            this.put(name, new CustomGeometry(geom));
        }
    }
    
    public static PresetGeometries getInstance() {
        if (PresetGeometries._inst == null) {
            PresetGeometries._inst = new PresetGeometries();
        }
        return PresetGeometries._inst;
    }
}
