// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.xslf.model.geom;

public interface Expression
{
    double evaluate(final Context p0);
}
