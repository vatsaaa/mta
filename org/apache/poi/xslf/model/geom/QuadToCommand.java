// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.xslf.model.geom;

import java.awt.geom.GeneralPath;
import org.openxmlformats.schemas.drawingml.x2006.main.CTAdjPoint2D;

public class QuadToCommand implements PathCommand
{
    private String arg1;
    private String arg2;
    private String arg3;
    private String arg4;
    
    QuadToCommand(final CTAdjPoint2D pt1, final CTAdjPoint2D pt2) {
        this.arg1 = pt1.getX().toString();
        this.arg2 = pt1.getY().toString();
        this.arg3 = pt2.getX().toString();
        this.arg4 = pt2.getY().toString();
    }
    
    public void execute(final GeneralPath path, final Context ctx) {
        final double x1 = ctx.getValue(this.arg1);
        final double y1 = ctx.getValue(this.arg2);
        final double x2 = ctx.getValue(this.arg3);
        final double y2 = ctx.getValue(this.arg4);
        path.quadTo((float)x1, (float)y1, (float)x2, (float)y2);
    }
}
