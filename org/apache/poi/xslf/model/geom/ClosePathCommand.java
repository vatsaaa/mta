// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.xslf.model.geom;

import java.awt.geom.GeneralPath;

public class ClosePathCommand implements PathCommand
{
    ClosePathCommand() {
    }
    
    public void execute(final GeneralPath path, final Context ctx) {
        path.closePath();
    }
}
