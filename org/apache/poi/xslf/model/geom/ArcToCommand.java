// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.xslf.model.geom;

import java.awt.geom.Point2D;
import java.awt.Shape;
import java.awt.geom.Arc2D;
import java.awt.geom.GeneralPath;
import org.openxmlformats.schemas.drawingml.x2006.main.CTPath2DArcTo;

public class ArcToCommand implements PathCommand
{
    private String hr;
    private String wr;
    private String stAng;
    private String swAng;
    
    ArcToCommand(final CTPath2DArcTo arc) {
        this.hr = arc.getHR().toString();
        this.wr = arc.getWR().toString();
        this.stAng = arc.getStAng().toString();
        this.swAng = arc.getSwAng().toString();
    }
    
    public void execute(final GeneralPath path, final Context ctx) {
        final double rx = ctx.getValue(this.wr);
        final double ry = ctx.getValue(this.hr);
        final double start = ctx.getValue(this.stAng) / 60000.0;
        final double extent = ctx.getValue(this.swAng) / 60000.0;
        final Point2D pt = path.getCurrentPoint();
        final double x0 = pt.getX() - rx - rx * Math.cos(Math.toRadians(start));
        final double y0 = pt.getY() - ry - ry * Math.sin(Math.toRadians(start));
        final Arc2D arc = new Arc2D.Double(x0, y0, 2.0 * rx, 2.0 * ry, -start, -extent, 0);
        path.append(arc, true);
    }
}
