// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.xslf.model.geom;

import java.util.regex.Matcher;

public class ModExpression implements Expression
{
    private String arg1;
    private String arg2;
    private String arg3;
    
    ModExpression(final Matcher m) {
        this.arg1 = m.group(1);
        this.arg2 = m.group(2);
        this.arg3 = m.group(3);
    }
    
    public double evaluate(final Context ctx) {
        final double x = ctx.getValue(this.arg1);
        final double y = ctx.getValue(this.arg2);
        final double z = ctx.getValue(this.arg3);
        return Math.sqrt(x * x + y * y + z * z);
    }
}
