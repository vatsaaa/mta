// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.xslf.model.geom;

import java.util.regex.Matcher;

public class LiteralValueExpression implements Expression
{
    private String arg;
    
    LiteralValueExpression(final Matcher m) {
        this.arg = m.group(1);
    }
    
    public double evaluate(final Context ctx) {
        return ctx.getValue(this.arg);
    }
}
