// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.xslf.model.geom;

import java.awt.geom.Rectangle2D;
import java.util.HashMap;
import java.util.Map;

public abstract class Formula
{
    static Map<String, Formula> builtInFormulas;
    
    String getName() {
        return null;
    }
    
    abstract double evaluate(final Context p0);
    
    static {
        (Formula.builtInFormulas = new HashMap<String, Formula>()).put("3cd4", new Formula() {
            @Override
            double evaluate(final Context ctx) {
                return 1.62E7;
            }
        });
        Formula.builtInFormulas.put("3cd8", new Formula() {
            @Override
            double evaluate(final Context ctx) {
                return 8100000.0;
            }
        });
        Formula.builtInFormulas.put("5cd8", new Formula() {
            @Override
            double evaluate(final Context ctx) {
                return 1.62E7;
            }
        });
        Formula.builtInFormulas.put("7cd8", new Formula() {
            @Override
            double evaluate(final Context ctx) {
                return 1.62E7;
            }
        });
        Formula.builtInFormulas.put("b", new Formula() {
            @Override
            double evaluate(final Context ctx) {
                final Rectangle2D anchor = ctx.getShapeAnchor();
                return anchor.getY() + anchor.getHeight();
            }
        });
        Formula.builtInFormulas.put("cd2", new Formula() {
            @Override
            double evaluate(final Context ctx) {
                return 1.08E7;
            }
        });
        Formula.builtInFormulas.put("cd4", new Formula() {
            @Override
            double evaluate(final Context ctx) {
                return 5400000.0;
            }
        });
        Formula.builtInFormulas.put("cd8", new Formula() {
            @Override
            double evaluate(final Context ctx) {
                return 2700000.0;
            }
        });
        Formula.builtInFormulas.put("hc", new Formula() {
            @Override
            double evaluate(final Context ctx) {
                final Rectangle2D anchor = ctx.getShapeAnchor();
                return anchor.getX() + anchor.getWidth() / 2.0;
            }
        });
        Formula.builtInFormulas.put("h", new Formula() {
            @Override
            double evaluate(final Context ctx) {
                final Rectangle2D anchor = ctx.getShapeAnchor();
                return anchor.getHeight();
            }
        });
        Formula.builtInFormulas.put("hd2", new Formula() {
            @Override
            double evaluate(final Context ctx) {
                final Rectangle2D anchor = ctx.getShapeAnchor();
                return anchor.getHeight() / 2.0;
            }
        });
        Formula.builtInFormulas.put("hd3", new Formula() {
            @Override
            double evaluate(final Context ctx) {
                final Rectangle2D anchor = ctx.getShapeAnchor();
                return anchor.getHeight() / 3.0;
            }
        });
        Formula.builtInFormulas.put("hd4", new Formula() {
            @Override
            double evaluate(final Context ctx) {
                final Rectangle2D anchor = ctx.getShapeAnchor();
                return anchor.getHeight() / 4.0;
            }
        });
        Formula.builtInFormulas.put("hd5", new Formula() {
            @Override
            double evaluate(final Context ctx) {
                final Rectangle2D anchor = ctx.getShapeAnchor();
                return anchor.getHeight() / 5.0;
            }
        });
        Formula.builtInFormulas.put("hd6", new Formula() {
            @Override
            double evaluate(final Context ctx) {
                final Rectangle2D anchor = ctx.getShapeAnchor();
                return anchor.getHeight() / 6.0;
            }
        });
        Formula.builtInFormulas.put("hd8", new Formula() {
            @Override
            double evaluate(final Context ctx) {
                final Rectangle2D anchor = ctx.getShapeAnchor();
                return anchor.getHeight() / 8.0;
            }
        });
        Formula.builtInFormulas.put("l", new Formula() {
            @Override
            double evaluate(final Context ctx) {
                final Rectangle2D anchor = ctx.getShapeAnchor();
                return anchor.getX();
            }
        });
        Formula.builtInFormulas.put("ls", new Formula() {
            @Override
            double evaluate(final Context ctx) {
                final Rectangle2D anchor = ctx.getShapeAnchor();
                return Math.max(anchor.getWidth(), anchor.getHeight());
            }
        });
        Formula.builtInFormulas.put("r", new Formula() {
            @Override
            double evaluate(final Context ctx) {
                final Rectangle2D anchor = ctx.getShapeAnchor();
                return anchor.getX() + anchor.getWidth();
            }
        });
        Formula.builtInFormulas.put("ss", new Formula() {
            @Override
            double evaluate(final Context ctx) {
                final Rectangle2D anchor = ctx.getShapeAnchor();
                return Math.min(anchor.getWidth(), anchor.getHeight());
            }
        });
        Formula.builtInFormulas.put("ssd2", new Formula() {
            @Override
            double evaluate(final Context ctx) {
                final Rectangle2D anchor = ctx.getShapeAnchor();
                final double ss = Math.min(anchor.getWidth(), anchor.getHeight());
                return ss / 2.0;
            }
        });
        Formula.builtInFormulas.put("ssd4", new Formula() {
            @Override
            double evaluate(final Context ctx) {
                final Rectangle2D anchor = ctx.getShapeAnchor();
                final double ss = Math.min(anchor.getWidth(), anchor.getHeight());
                return ss / 4.0;
            }
        });
        Formula.builtInFormulas.put("ssd6", new Formula() {
            @Override
            double evaluate(final Context ctx) {
                final Rectangle2D anchor = ctx.getShapeAnchor();
                final double ss = Math.min(anchor.getWidth(), anchor.getHeight());
                return ss / 6.0;
            }
        });
        Formula.builtInFormulas.put("ssd8", new Formula() {
            @Override
            double evaluate(final Context ctx) {
                final Rectangle2D anchor = ctx.getShapeAnchor();
                final double ss = Math.min(anchor.getWidth(), anchor.getHeight());
                return ss / 8.0;
            }
        });
        Formula.builtInFormulas.put("ssd16", new Formula() {
            @Override
            double evaluate(final Context ctx) {
                final Rectangle2D anchor = ctx.getShapeAnchor();
                final double ss = Math.min(anchor.getWidth(), anchor.getHeight());
                return ss / 16.0;
            }
        });
        Formula.builtInFormulas.put("ssd32", new Formula() {
            @Override
            double evaluate(final Context ctx) {
                final Rectangle2D anchor = ctx.getShapeAnchor();
                final double ss = Math.min(anchor.getWidth(), anchor.getHeight());
                return ss / 32.0;
            }
        });
        Formula.builtInFormulas.put("t", new Formula() {
            @Override
            double evaluate(final Context ctx) {
                return ctx.getShapeAnchor().getY();
            }
        });
        Formula.builtInFormulas.put("vc", new Formula() {
            @Override
            double evaluate(final Context ctx) {
                final Rectangle2D anchor = ctx.getShapeAnchor();
                return anchor.getY() + anchor.getHeight() / 2.0;
            }
        });
        Formula.builtInFormulas.put("w", new Formula() {
            @Override
            double evaluate(final Context ctx) {
                return ctx.getShapeAnchor().getWidth();
            }
        });
        Formula.builtInFormulas.put("wd2", new Formula() {
            @Override
            double evaluate(final Context ctx) {
                return ctx.getShapeAnchor().getWidth() / 2.0;
            }
        });
        Formula.builtInFormulas.put("wd3", new Formula() {
            @Override
            double evaluate(final Context ctx) {
                return ctx.getShapeAnchor().getWidth() / 3.0;
            }
        });
        Formula.builtInFormulas.put("wd4", new Formula() {
            @Override
            double evaluate(final Context ctx) {
                return ctx.getShapeAnchor().getWidth() / 4.0;
            }
        });
        Formula.builtInFormulas.put("wd5", new Formula() {
            @Override
            double evaluate(final Context ctx) {
                return ctx.getShapeAnchor().getWidth() / 5.0;
            }
        });
        Formula.builtInFormulas.put("wd6", new Formula() {
            @Override
            double evaluate(final Context ctx) {
                return ctx.getShapeAnchor().getWidth() / 6.0;
            }
        });
        Formula.builtInFormulas.put("wd8", new Formula() {
            @Override
            double evaluate(final Context ctx) {
                return ctx.getShapeAnchor().getWidth() / 8.0;
            }
        });
        Formula.builtInFormulas.put("wd10", new Formula() {
            @Override
            double evaluate(final Context ctx) {
                return ctx.getShapeAnchor().getWidth() / 10.0;
            }
        });
        Formula.builtInFormulas.put("wd32", new Formula() {
            @Override
            double evaluate(final Context ctx) {
                return ctx.getShapeAnchor().getWidth() / 32.0;
            }
        });
    }
}
