// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.xslf.model.geom;

import java.util.Iterator;
import java.awt.geom.GeneralPath;
import org.openxmlformats.schemas.drawingml.x2006.main.CTAdjPoint2D;
import org.apache.xmlbeans.XmlObject;
import org.openxmlformats.schemas.drawingml.x2006.main.CTPath2DClose;
import org.openxmlformats.schemas.drawingml.x2006.main.CTPath2DCubicBezierTo;
import org.openxmlformats.schemas.drawingml.x2006.main.CTPath2DQuadBezierTo;
import org.openxmlformats.schemas.drawingml.x2006.main.CTPath2DArcTo;
import org.openxmlformats.schemas.drawingml.x2006.main.CTPath2DLineTo;
import org.openxmlformats.schemas.drawingml.x2006.main.CTPath2DMoveTo;
import org.openxmlformats.schemas.drawingml.x2006.main.STPathFillMode;
import org.openxmlformats.schemas.drawingml.x2006.main.CTPath2D;
import java.util.ArrayList;
import java.util.List;

public class Path
{
    private final List<PathCommand> commands;
    boolean _fill;
    boolean _stroke;
    long _w;
    long _h;
    
    public Path() {
        this(true, true);
    }
    
    public Path(final boolean fill, final boolean stroke) {
        this.commands = new ArrayList<PathCommand>();
        this._w = -1L;
        this._h = -1L;
        this._fill = fill;
        this._stroke = stroke;
    }
    
    public Path(final CTPath2D spPath) {
        this._fill = (spPath.getFill() != STPathFillMode.NONE);
        this._stroke = spPath.getStroke();
        this._w = (spPath.isSetW() ? spPath.getW() : -1L);
        this._h = (spPath.isSetH() ? spPath.getH() : -1L);
        this.commands = new ArrayList<PathCommand>();
        for (final XmlObject ch : spPath.selectPath("*")) {
            if (ch instanceof CTPath2DMoveTo) {
                final CTAdjPoint2D pt = ((CTPath2DMoveTo)ch).getPt();
                this.commands.add(new MoveToCommand(pt));
            }
            else if (ch instanceof CTPath2DLineTo) {
                final CTAdjPoint2D pt = ((CTPath2DLineTo)ch).getPt();
                this.commands.add(new LineToCommand(pt));
            }
            else if (ch instanceof CTPath2DArcTo) {
                final CTPath2DArcTo arc = (CTPath2DArcTo)ch;
                this.commands.add(new ArcToCommand(arc));
            }
            else if (ch instanceof CTPath2DQuadBezierTo) {
                final CTPath2DQuadBezierTo bez = (CTPath2DQuadBezierTo)ch;
                final CTAdjPoint2D pt2 = bez.getPtArray(0);
                final CTAdjPoint2D pt3 = bez.getPtArray(1);
                this.commands.add(new QuadToCommand(pt2, pt3));
            }
            else if (ch instanceof CTPath2DCubicBezierTo) {
                final CTPath2DCubicBezierTo bez2 = (CTPath2DCubicBezierTo)ch;
                final CTAdjPoint2D pt2 = bez2.getPtArray(0);
                final CTAdjPoint2D pt3 = bez2.getPtArray(1);
                final CTAdjPoint2D pt4 = bez2.getPtArray(2);
                this.commands.add(new CurveToCommand(pt2, pt3, pt4));
            }
            else {
                if (!(ch instanceof CTPath2DClose)) {
                    throw new IllegalStateException("Unsupported path segment: " + ch);
                }
                this.commands.add(new ClosePathCommand());
            }
        }
    }
    
    public void addCommand(final PathCommand cmd) {
        this.commands.add(cmd);
    }
    
    public GeneralPath getPath(final Context ctx) {
        final GeneralPath path = new GeneralPath();
        for (final PathCommand cmd : this.commands) {
            cmd.execute(path, ctx);
        }
        return path;
    }
    
    public boolean isStroked() {
        return this._stroke;
    }
    
    public boolean isFilled() {
        return this._fill;
    }
    
    public long getW() {
        return this._w;
    }
    
    public long getH() {
        return this._h;
    }
}
