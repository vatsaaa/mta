// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.xslf.model.geom;

import org.openxmlformats.schemas.drawingml.x2006.main.CTGeomRect;
import org.openxmlformats.schemas.drawingml.x2006.main.CTPath2DList;
import java.util.Iterator;
import org.openxmlformats.schemas.drawingml.x2006.main.CTGeomGuideList;
import org.openxmlformats.schemas.drawingml.x2006.main.CTPath2D;
import org.openxmlformats.schemas.drawingml.x2006.main.CTGeomGuide;
import java.util.ArrayList;
import org.openxmlformats.schemas.drawingml.x2006.main.CTCustomGeometry2D;
import java.util.List;

public class CustomGeometry implements Iterable<Path>
{
    List<Guide> adjusts;
    List<Guide> guides;
    List<Path> paths;
    Path textBounds;
    
    public CustomGeometry(final CTCustomGeometry2D geom) {
        this.adjusts = new ArrayList<Guide>();
        this.guides = new ArrayList<Guide>();
        this.paths = new ArrayList<Path>();
        final CTGeomGuideList avLst = geom.getAvLst();
        if (avLst != null) {
            for (final CTGeomGuide gd : avLst.getGdList()) {
                this.adjusts.add(new AdjustValue(gd));
            }
        }
        final CTGeomGuideList gdLst = geom.getGdLst();
        if (gdLst != null) {
            for (final CTGeomGuide gd2 : gdLst.getGdList()) {
                this.guides.add(new Guide(gd2));
            }
        }
        final CTPath2DList pathLst = geom.getPathLst();
        if (pathLst != null) {
            for (final CTPath2D spPath : pathLst.getPathList()) {
                this.paths.add(new Path(spPath));
            }
        }
        if (geom.isSetRect()) {
            final CTGeomRect rect = geom.getRect();
            (this.textBounds = new Path()).addCommand(new MoveToCommand(rect.getL().toString(), rect.getT().toString()));
            this.textBounds.addCommand(new LineToCommand(rect.getR().toString(), rect.getT().toString()));
            this.textBounds.addCommand(new LineToCommand(rect.getR().toString(), rect.getB().toString()));
            this.textBounds.addCommand(new LineToCommand(rect.getL().toString(), rect.getB().toString()));
            this.textBounds.addCommand(new ClosePathCommand());
        }
    }
    
    public Iterator<Path> iterator() {
        return this.paths.iterator();
    }
    
    public Path getTextBounds() {
        return this.textBounds;
    }
}
