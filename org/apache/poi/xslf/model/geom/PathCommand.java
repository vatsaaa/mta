// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.xslf.model.geom;

import java.awt.geom.GeneralPath;

public interface PathCommand
{
    void execute(final GeneralPath p0, final Context p1);
}
