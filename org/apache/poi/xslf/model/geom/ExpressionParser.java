// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.xslf.model.geom;

import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.HashMap;

public class ExpressionParser
{
    static final HashMap<String, Class> impls;
    
    public static Expression parse(final String str) {
        for (final String regexp : ExpressionParser.impls.keySet()) {
            final Pattern ptrn = Pattern.compile(regexp);
            final Matcher m = ptrn.matcher(str);
            if (m.matches()) {
                final Class c = ExpressionParser.impls.get(regexp);
                try {
                    return c.getDeclaredConstructor(Matcher.class).newInstance(m);
                }
                catch (Exception e) {
                    throw new RuntimeException(e);
                }
            }
        }
        throw new RuntimeException("Unsupported formula: " + str);
    }
    
    static {
        (impls = new HashMap<String, Class>()).put("\\*/ +([\\-\\w]+) +([\\-\\w]+) +([\\-\\w]+)", MultiplyDivideExpression.class);
        ExpressionParser.impls.put("\\+- +([\\-\\w]+) +([\\-\\w]+) +([\\-\\w]+)( 0)?", AddSubtractExpression.class);
        ExpressionParser.impls.put("\\+/ +([\\-\\w]+) +([\\-\\w]+) +([\\-\\w]+)", AddDivideExpression.class);
        ExpressionParser.impls.put("\\?: +([\\-\\w]+) +([\\-\\w]+) +([\\-\\w]+)", IfElseExpression.class);
        ExpressionParser.impls.put("val +([\\-\\w]+)", LiteralValueExpression.class);
        ExpressionParser.impls.put("abs +([\\-\\w]+)", AbsExpression.class);
        ExpressionParser.impls.put("sqrt +([\\-\\w]+)", SqrtExpression.class);
        ExpressionParser.impls.put("max +([\\-\\w]+) +([\\-\\w]+)", MaxExpression.class);
        ExpressionParser.impls.put("min +([\\-\\w]+) +([\\-\\w]+)", MinExpression.class);
        ExpressionParser.impls.put("at2 +([\\-\\w]+) +([\\-\\w]+)", ArcTanExpression.class);
        ExpressionParser.impls.put("sin +([\\-\\w]+) +([\\-\\w]+)", SinExpression.class);
        ExpressionParser.impls.put("cos +([\\-\\w]+) +([\\-\\w]+)", CosExpression.class);
        ExpressionParser.impls.put("tan +([\\-\\w]+) +([\\-\\w]+)", TanExpression.class);
        ExpressionParser.impls.put("cat2 +([\\-\\w]+) +([\\-\\w]+) +([\\-\\w]+)", CosineArcTanExpression.class);
        ExpressionParser.impls.put("sat2 +([\\-\\w]+) +([\\-\\w]+) +([\\-\\w]+)", SinArcTanExpression.class);
        ExpressionParser.impls.put("pin +([\\-\\w]+) +([\\-\\w]+) +([\\-\\w]+)", PinExpression.class);
        ExpressionParser.impls.put("mod +([\\-\\w]+) +([\\-\\w]+) +([\\-\\w]+)", ModExpression.class);
    }
}
