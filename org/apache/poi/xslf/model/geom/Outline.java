// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.xslf.model.geom;

import java.awt.Shape;

public class Outline
{
    private Shape shape;
    private Path path;
    
    public Outline(final Shape shape, final Path path) {
        this.shape = shape;
        this.path = path;
    }
    
    public Path getPath() {
        return this.path;
    }
    
    public Shape getOutline() {
        return this.shape;
    }
}
