// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.xslf.model.geom;

import java.util.Iterator;
import java.util.HashMap;
import java.awt.geom.Rectangle2D;
import java.util.Map;

public class Context
{
    final Map<String, Double> _ctx;
    final IAdjustableShape _props;
    final Rectangle2D _anchor;
    
    public Context(final CustomGeometry geom, final Rectangle2D anchor, final IAdjustableShape props) {
        this._ctx = new HashMap<String, Double>();
        this._props = props;
        this._anchor = anchor;
        for (final Guide gd : geom.adjusts) {
            this.evaluate(gd);
        }
        for (final Guide gd : geom.guides) {
            this.evaluate(gd);
        }
    }
    
    public Rectangle2D getShapeAnchor() {
        return this._anchor;
    }
    
    public Guide getAdjustValue(final String name) {
        return this._props.getAdjustValue(name);
    }
    
    public double getValue(final String key) {
        if (key.matches("(\\+|-)?\\d+")) {
            return Double.parseDouble(key);
        }
        final Formula builtIn = Formula.builtInFormulas.get(key);
        if (builtIn != null) {
            return builtIn.evaluate(this);
        }
        if (!this._ctx.containsKey(key)) {
            throw new RuntimeException("undefined variable: " + key);
        }
        return this._ctx.get(key);
    }
    
    public double evaluate(final Formula fmla) {
        final double result = fmla.evaluate(this);
        final String key = fmla.getName();
        if (key != null) {
            this._ctx.put(key, result);
        }
        return result;
    }
}
