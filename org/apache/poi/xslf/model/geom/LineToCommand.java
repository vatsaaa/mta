// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.xslf.model.geom;

import java.awt.geom.GeneralPath;
import org.openxmlformats.schemas.drawingml.x2006.main.CTAdjPoint2D;

public class LineToCommand implements PathCommand
{
    private String arg1;
    private String arg2;
    
    LineToCommand(final CTAdjPoint2D pt) {
        this.arg1 = pt.getX().toString();
        this.arg2 = pt.getY().toString();
    }
    
    LineToCommand(final String s1, final String s2) {
        this.arg1 = s1;
        this.arg2 = s2;
    }
    
    public void execute(final GeneralPath path, final Context ctx) {
        final double x = ctx.getValue(this.arg1);
        final double y = ctx.getValue(this.arg2);
        path.lineTo((float)x, (float)y);
    }
}
