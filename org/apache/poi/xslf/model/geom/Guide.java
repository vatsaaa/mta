// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.xslf.model.geom;

import org.openxmlformats.schemas.drawingml.x2006.main.CTGeomGuide;

public class Guide extends Formula
{
    private String name;
    private String fmla;
    private Expression expr;
    
    public Guide(final CTGeomGuide gd) {
        this(gd.getName(), gd.getFmla());
    }
    
    public Guide(final String nm, final String fm) {
        this.name = nm;
        this.fmla = fm;
        this.expr = ExpressionParser.parse(fm);
    }
    
    @Override
    String getName() {
        return this.name;
    }
    
    String getFormula() {
        return this.fmla;
    }
    
    public double evaluate(final Context ctx) {
        return this.expr.evaluate(ctx);
    }
}
