// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.xslf.model.geom;

import java.util.regex.Matcher;

public class SqrtExpression implements Expression
{
    private String arg;
    
    SqrtExpression(final Matcher m) {
        this.arg = m.group(1);
    }
    
    public double evaluate(final Context ctx) {
        final double val = ctx.getValue(this.arg);
        return Math.sqrt(val);
    }
}
