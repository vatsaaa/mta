// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.xslf.model;

import org.openxmlformats.schemas.drawingml.x2006.main.CTTextCharacterProperties;
import org.openxmlformats.schemas.drawingml.x2006.main.CTTextParagraphProperties;

public abstract class CharacterPropertyFetcher<T> extends ParagraphPropertyFetcher<T>
{
    public boolean isFetchingFromMaster;
    
    public CharacterPropertyFetcher(final int level) {
        super(level);
        this.isFetchingFromMaster = false;
    }
    
    @Override
    public boolean fetch(final CTTextParagraphProperties props) {
        return props.isSetDefRPr() && this.fetch(props.getDefRPr());
    }
    
    public abstract boolean fetch(final CTTextCharacterProperties p0);
}
