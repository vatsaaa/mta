// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.xslf.model;

import org.apache.xmlbeans.XmlObject;
import org.openxmlformats.schemas.drawingml.x2006.main.CTTextParagraphProperties;
import org.apache.poi.xslf.usermodel.XSLFSimpleShape;

public abstract class ParagraphPropertyFetcher<T> extends PropertyFetcher<T>
{
    int _level;
    
    public ParagraphPropertyFetcher(final int level) {
        this._level = level;
    }
    
    @Override
    public boolean fetch(final XSLFSimpleShape shape) {
        final XmlObject[] o = shape.getXmlObject().selectPath("declare namespace p='http://schemas.openxmlformats.org/presentationml/2006/main' declare namespace a='http://schemas.openxmlformats.org/drawingml/2006/main' .//p:txBody/a:lstStyle/a:lvl" + (this._level + 1) + "pPr");
        if (o.length == 1) {
            final CTTextParagraphProperties props = (CTTextParagraphProperties)o[0];
            return this.fetch(props);
        }
        return false;
    }
    
    public abstract boolean fetch(final CTTextParagraphProperties p0);
}
