// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.xslf.model;

import org.apache.xmlbeans.XmlObject;
import org.openxmlformats.schemas.drawingml.x2006.main.CTTextBodyProperties;
import org.apache.poi.xslf.usermodel.XSLFSimpleShape;

public abstract class TextBodyPropertyFetcher<T> extends PropertyFetcher<T>
{
    @Override
    public boolean fetch(final XSLFSimpleShape shape) {
        final XmlObject[] o = shape.getXmlObject().selectPath("declare namespace p='http://schemas.openxmlformats.org/presentationml/2006/main' declare namespace a='http://schemas.openxmlformats.org/drawingml/2006/main' .//p:txBody/a:bodyPr");
        if (o.length == 1) {
            final CTTextBodyProperties props = (CTTextBodyProperties)o[0];
            return this.fetch(props);
        }
        return false;
    }
    
    public abstract boolean fetch(final CTTextBodyProperties p0);
}
