// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hssf.eventusermodel.dummyrecord;

public final class MissingRowDummyRecord extends DummyRecordBase
{
    private int rowNumber;
    
    public MissingRowDummyRecord(final int rowNumber) {
        this.rowNumber = rowNumber;
    }
    
    public int getRowNumber() {
        return this.rowNumber;
    }
}
