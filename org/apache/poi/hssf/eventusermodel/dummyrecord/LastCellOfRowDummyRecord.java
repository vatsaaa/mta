// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hssf.eventusermodel.dummyrecord;

public final class LastCellOfRowDummyRecord extends DummyRecordBase
{
    private int row;
    private int lastColumnNumber;
    
    public LastCellOfRowDummyRecord(final int row, final int lastColumnNumber) {
        this.row = row;
        this.lastColumnNumber = lastColumnNumber;
    }
    
    public int getRow() {
        return this.row;
    }
    
    public int getLastColumnNumber() {
        return this.lastColumnNumber;
    }
}
