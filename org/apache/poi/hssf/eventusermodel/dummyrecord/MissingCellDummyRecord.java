// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hssf.eventusermodel.dummyrecord;

public final class MissingCellDummyRecord extends DummyRecordBase
{
    private int row;
    private int column;
    
    public MissingCellDummyRecord(final int row, final int column) {
        this.row = row;
        this.column = column;
    }
    
    public int getRow() {
        return this.row;
    }
    
    public int getColumn() {
        return this.column;
    }
}
