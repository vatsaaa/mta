// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hssf.eventusermodel;

import org.apache.poi.hssf.record.Record;
import org.apache.poi.hssf.record.RecordFactory;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class HSSFRequest
{
    private final Map<Short, List<HSSFListener>> _records;
    
    public HSSFRequest() {
        this._records = new HashMap<Short, List<HSSFListener>>(50);
    }
    
    public void addListener(final HSSFListener lsnr, final short sid) {
        List<HSSFListener> list = this._records.get(sid);
        if (list == null) {
            list = new ArrayList<HSSFListener>(1);
            this._records.put(sid, list);
        }
        list.add(lsnr);
    }
    
    public void addListenerForAllRecords(final HSSFListener lsnr) {
        final short[] rectypes = RecordFactory.getAllKnownRecordSIDs();
        for (int k = 0; k < rectypes.length; ++k) {
            this.addListener(lsnr, rectypes[k]);
        }
    }
    
    protected short processRecord(final Record rec) throws HSSFUserException {
        final Object obj = this._records.get(rec.getSid());
        short userCode = 0;
        if (obj != null) {
            final List listeners = (List)obj;
            for (int k = 0; k < listeners.size(); ++k) {
                final Object listenObj = listeners.get(k);
                if (listenObj instanceof AbortableHSSFListener) {
                    final AbortableHSSFListener listener = (AbortableHSSFListener)listenObj;
                    userCode = listener.abortableProcessRecord(rec);
                    if (userCode != 0) {
                        break;
                    }
                }
                else {
                    final HSSFListener listener2 = (HSSFListener)listenObj;
                    listener2.processRecord(rec);
                }
            }
        }
        return userCode;
    }
}
