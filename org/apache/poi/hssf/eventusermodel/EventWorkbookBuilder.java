// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hssf.eventusermodel;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.record.Record;
import java.util.List;
import org.apache.poi.hssf.record.EOFRecord;
import org.apache.poi.hssf.record.SupBookRecord;
import java.util.ArrayList;
import org.apache.poi.hssf.model.InternalWorkbook;
import org.apache.poi.hssf.record.SSTRecord;
import org.apache.poi.hssf.record.BoundSheetRecord;
import org.apache.poi.hssf.record.ExternSheetRecord;

public class EventWorkbookBuilder
{
    public static InternalWorkbook createStubWorkbook(final ExternSheetRecord[] externs, final BoundSheetRecord[] bounds, final SSTRecord sst) {
        final List wbRecords = new ArrayList();
        if (bounds != null) {
            for (int i = 0; i < bounds.length; ++i) {
                wbRecords.add(bounds[i]);
            }
        }
        if (sst != null) {
            wbRecords.add(sst);
        }
        if (externs != null) {
            wbRecords.add(SupBookRecord.createInternalReferences((short)externs.length));
            for (int i = 0; i < externs.length; ++i) {
                wbRecords.add(externs[i]);
            }
        }
        wbRecords.add(EOFRecord.instance);
        return InternalWorkbook.createWorkbook(wbRecords);
    }
    
    public static InternalWorkbook createStubWorkbook(final ExternSheetRecord[] externs, final BoundSheetRecord[] bounds) {
        return createStubWorkbook(externs, bounds, null);
    }
    
    public static class SheetRecordCollectingListener implements HSSFListener
    {
        private HSSFListener childListener;
        private List boundSheetRecords;
        private List externSheetRecords;
        private SSTRecord sstRecord;
        
        public SheetRecordCollectingListener(final HSSFListener childListener) {
            this.boundSheetRecords = new ArrayList();
            this.externSheetRecords = new ArrayList();
            this.sstRecord = null;
            this.childListener = childListener;
        }
        
        public BoundSheetRecord[] getBoundSheetRecords() {
            return this.boundSheetRecords.toArray(new BoundSheetRecord[this.boundSheetRecords.size()]);
        }
        
        public ExternSheetRecord[] getExternSheetRecords() {
            return this.externSheetRecords.toArray(new ExternSheetRecord[this.externSheetRecords.size()]);
        }
        
        public SSTRecord getSSTRecord() {
            return this.sstRecord;
        }
        
        public HSSFWorkbook getStubHSSFWorkbook() {
            return HSSFWorkbook.create(this.getStubWorkbook());
        }
        
        public InternalWorkbook getStubWorkbook() {
            return EventWorkbookBuilder.createStubWorkbook(this.getExternSheetRecords(), this.getBoundSheetRecords(), this.getSSTRecord());
        }
        
        public void processRecord(final Record record) {
            this.processRecordInternally(record);
            this.childListener.processRecord(record);
        }
        
        public void processRecordInternally(final Record record) {
            if (record instanceof BoundSheetRecord) {
                this.boundSheetRecords.add(record);
            }
            else if (record instanceof ExternSheetRecord) {
                this.externSheetRecords.add(record);
            }
            else if (record instanceof SSTRecord) {
                this.sstRecord = (SSTRecord)record;
            }
        }
    }
}
