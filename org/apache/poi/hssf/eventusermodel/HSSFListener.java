// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hssf.eventusermodel;

import org.apache.poi.hssf.record.Record;

public interface HSSFListener
{
    void processRecord(final Record p0);
}
