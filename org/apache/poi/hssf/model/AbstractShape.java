// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hssf.model;

import org.apache.poi.ddf.EscherSimpleProperty;
import org.apache.poi.ddf.EscherRGBProperty;
import org.apache.poi.ddf.EscherProperty;
import org.apache.poi.ddf.EscherBoolProperty;
import org.apache.poi.ddf.EscherOptRecord;
import org.apache.poi.ddf.EscherRecord;
import org.apache.poi.hssf.usermodel.HSSFAnchor;
import org.apache.poi.hssf.record.ObjRecord;
import org.apache.poi.ddf.EscherContainerRecord;
import org.apache.poi.ddf.EscherSpRecord;
import org.apache.poi.hssf.usermodel.HSSFSimpleShape;
import org.apache.poi.hssf.usermodel.HSSFPolygon;
import org.apache.poi.hssf.usermodel.HSSFTextbox;
import org.apache.poi.hssf.usermodel.HSSFComment;
import org.apache.poi.hssf.usermodel.HSSFShape;

public abstract class AbstractShape
{
    public static AbstractShape createShape(final HSSFShape hssfShape, final int shapeId) {
        AbstractShape shape;
        if (hssfShape instanceof HSSFComment) {
            shape = new CommentShape((HSSFComment)hssfShape, shapeId);
        }
        else if (hssfShape instanceof HSSFTextbox) {
            shape = new TextboxShape((HSSFTextbox)hssfShape, shapeId);
        }
        else if (hssfShape instanceof HSSFPolygon) {
            shape = new PolygonShape((HSSFPolygon)hssfShape, shapeId);
        }
        else {
            if (!(hssfShape instanceof HSSFSimpleShape)) {
                throw new IllegalArgumentException("Unknown shape type");
            }
            final HSSFSimpleShape simpleShape = (HSSFSimpleShape)hssfShape;
            switch (simpleShape.getShapeType()) {
                case 8: {
                    shape = new PictureShape(simpleShape, shapeId);
                    break;
                }
                case 1: {
                    shape = new LineShape(simpleShape, shapeId);
                    break;
                }
                case 2:
                case 3: {
                    shape = new SimpleFilledShape(simpleShape, shapeId);
                    break;
                }
                case 20: {
                    shape = new ComboboxShape(simpleShape, shapeId);
                    break;
                }
                default: {
                    throw new IllegalArgumentException("Do not know how to handle this type of shape");
                }
            }
        }
        final EscherSpRecord sp = shape.getSpContainer().getChildById((short)(-4086));
        if (hssfShape.getParent() != null) {
            sp.setFlags(sp.getFlags() | 0x2);
        }
        return shape;
    }
    
    protected AbstractShape() {
    }
    
    public abstract EscherContainerRecord getSpContainer();
    
    public abstract ObjRecord getObjRecord();
    
    protected EscherRecord createAnchor(final HSSFAnchor userAnchor) {
        return ConvertAnchor.createAnchor(userAnchor);
    }
    
    protected int addStandardOptions(final HSSFShape shape, final EscherOptRecord opt) {
        opt.addEscherProperty(new EscherBoolProperty((short)191, 524288));
        if (shape.isNoFill()) {
            opt.addEscherProperty(new EscherBoolProperty((short)447, 1114112));
        }
        else {
            opt.addEscherProperty(new EscherBoolProperty((short)447, 65536));
        }
        opt.addEscherProperty(new EscherRGBProperty((short)385, shape.getFillColor()));
        opt.addEscherProperty(new EscherBoolProperty((short)959, 524288));
        opt.addEscherProperty(new EscherRGBProperty((short)448, shape.getLineStyleColor()));
        int options = 5;
        if (shape.getLineWidth() != 9525) {
            opt.addEscherProperty(new EscherSimpleProperty((short)459, shape.getLineWidth()));
            ++options;
        }
        if (shape.getLineStyle() != 0) {
            opt.addEscherProperty(new EscherSimpleProperty((short)462, shape.getLineStyle()));
            opt.addEscherProperty(new EscherSimpleProperty((short)471, 0));
            if (shape.getLineStyle() == -1) {
                opt.addEscherProperty(new EscherBoolProperty((short)511, 524288));
            }
            else {
                opt.addEscherProperty(new EscherBoolProperty((short)511, 524296));
            }
            options += 3;
        }
        opt.sortProperties();
        return options;
    }
    
    int getCmoObjectId(final int shapeId) {
        return shapeId - 1024;
    }
}
