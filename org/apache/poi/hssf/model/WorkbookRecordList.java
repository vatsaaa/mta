// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hssf.model;

import java.util.Iterator;
import java.util.ArrayList;
import java.util.List;
import org.apache.poi.hssf.record.Record;

public final class WorkbookRecordList implements Iterable<Record>
{
    private List<Record> records;
    private int protpos;
    private int bspos;
    private int tabpos;
    private int fontpos;
    private int xfpos;
    private int backuppos;
    private int namepos;
    private int supbookpos;
    private int externsheetPos;
    private int palettepos;
    
    public WorkbookRecordList() {
        this.records = new ArrayList<Record>();
        this.protpos = 0;
        this.bspos = 0;
        this.tabpos = 0;
        this.fontpos = 0;
        this.xfpos = 0;
        this.backuppos = 0;
        this.namepos = 0;
        this.supbookpos = 0;
        this.externsheetPos = 0;
        this.palettepos = -1;
    }
    
    public void setRecords(final List<Record> records) {
        this.records = records;
    }
    
    public int size() {
        return this.records.size();
    }
    
    public Record get(final int i) {
        return this.records.get(i);
    }
    
    public void add(final int pos, final Record r) {
        this.records.add(pos, r);
        if (this.getProtpos() >= pos) {
            this.setProtpos(this.protpos + 1);
        }
        if (this.getBspos() >= pos) {
            this.setBspos(this.bspos + 1);
        }
        if (this.getTabpos() >= pos) {
            this.setTabpos(this.tabpos + 1);
        }
        if (this.getFontpos() >= pos) {
            this.setFontpos(this.fontpos + 1);
        }
        if (this.getXfpos() >= pos) {
            this.setXfpos(this.xfpos + 1);
        }
        if (this.getBackuppos() >= pos) {
            this.setBackuppos(this.backuppos + 1);
        }
        if (this.getNamepos() >= pos) {
            this.setNamepos(this.namepos + 1);
        }
        if (this.getSupbookpos() >= pos) {
            this.setSupbookpos(this.supbookpos + 1);
        }
        if (this.getPalettepos() != -1 && this.getPalettepos() >= pos) {
            this.setPalettepos(this.palettepos + 1);
        }
        if (this.getExternsheetPos() >= pos) {
            this.setExternsheetPos(this.getExternsheetPos() + 1);
        }
    }
    
    public List<Record> getRecords() {
        return this.records;
    }
    
    public Iterator<Record> iterator() {
        return this.records.iterator();
    }
    
    public void remove(final Object record) {
        final int i = this.records.indexOf(record);
        this.remove(i);
    }
    
    public void remove(final int pos) {
        this.records.remove(pos);
        if (this.getProtpos() >= pos) {
            this.setProtpos(this.protpos - 1);
        }
        if (this.getBspos() >= pos) {
            this.setBspos(this.bspos - 1);
        }
        if (this.getTabpos() >= pos) {
            this.setTabpos(this.tabpos - 1);
        }
        if (this.getFontpos() >= pos) {
            this.setFontpos(this.fontpos - 1);
        }
        if (this.getXfpos() >= pos) {
            this.setXfpos(this.xfpos - 1);
        }
        if (this.getBackuppos() >= pos) {
            this.setBackuppos(this.backuppos - 1);
        }
        if (this.getNamepos() >= pos) {
            this.setNamepos(this.getNamepos() - 1);
        }
        if (this.getSupbookpos() >= pos) {
            this.setSupbookpos(this.getSupbookpos() - 1);
        }
        if (this.getPalettepos() != -1 && this.getPalettepos() >= pos) {
            this.setPalettepos(this.palettepos - 1);
        }
        if (this.getExternsheetPos() >= pos) {
            this.setExternsheetPos(this.getExternsheetPos() - 1);
        }
    }
    
    public int getProtpos() {
        return this.protpos;
    }
    
    public void setProtpos(final int protpos) {
        this.protpos = protpos;
    }
    
    public int getBspos() {
        return this.bspos;
    }
    
    public void setBspos(final int bspos) {
        this.bspos = bspos;
    }
    
    public int getTabpos() {
        return this.tabpos;
    }
    
    public void setTabpos(final int tabpos) {
        this.tabpos = tabpos;
    }
    
    public int getFontpos() {
        return this.fontpos;
    }
    
    public void setFontpos(final int fontpos) {
        this.fontpos = fontpos;
    }
    
    public int getXfpos() {
        return this.xfpos;
    }
    
    public void setXfpos(final int xfpos) {
        this.xfpos = xfpos;
    }
    
    public int getBackuppos() {
        return this.backuppos;
    }
    
    public void setBackuppos(final int backuppos) {
        this.backuppos = backuppos;
    }
    
    public int getPalettepos() {
        return this.palettepos;
    }
    
    public void setPalettepos(final int palettepos) {
        this.palettepos = palettepos;
    }
    
    public int getNamepos() {
        return this.namepos;
    }
    
    public int getSupbookpos() {
        return this.supbookpos;
    }
    
    public void setNamepos(final int namepos) {
        this.namepos = namepos;
    }
    
    public void setSupbookpos(final int supbookpos) {
        this.supbookpos = supbookpos;
    }
    
    public int getExternsheetPos() {
        return this.externsheetPos;
    }
    
    public void setExternsheetPos(final int externsheetPos) {
        this.externsheetPos = externsheetPos;
    }
}
