// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hssf.model;

import org.apache.poi.hssf.record.SubRecord;
import org.apache.poi.hssf.record.EndSubRecord;
import org.apache.poi.hssf.record.CommonObjectDataSubRecord;
import org.apache.poi.hssf.usermodel.HSSFAnchor;
import org.apache.poi.ddf.EscherRecord;
import org.apache.poi.ddf.EscherBoolProperty;
import org.apache.poi.ddf.EscherProperty;
import org.apache.poi.ddf.EscherShapePathProperty;
import org.apache.poi.ddf.EscherClientDataRecord;
import org.apache.poi.ddf.EscherClientAnchorRecord;
import org.apache.poi.ddf.EscherOptRecord;
import org.apache.poi.ddf.EscherSpRecord;
import org.apache.poi.hssf.usermodel.HSSFShape;
import org.apache.poi.hssf.usermodel.HSSFSimpleShape;
import org.apache.poi.hssf.record.ObjRecord;
import org.apache.poi.ddf.EscherContainerRecord;

public class LineShape extends AbstractShape
{
    private EscherContainerRecord spContainer;
    private ObjRecord objRecord;
    
    LineShape(final HSSFSimpleShape hssfShape, final int shapeId) {
        this.spContainer = this.createSpContainer(hssfShape, shapeId);
        this.objRecord = this.createObjRecord(hssfShape, shapeId);
    }
    
    private EscherContainerRecord createSpContainer(final HSSFSimpleShape hssfShape, final int shapeId) {
        final HSSFShape shape = hssfShape;
        final EscherContainerRecord spContainer = new EscherContainerRecord();
        final EscherSpRecord sp = new EscherSpRecord();
        final EscherOptRecord opt = new EscherOptRecord();
        EscherRecord anchor = new EscherClientAnchorRecord();
        final EscherClientDataRecord clientData = new EscherClientDataRecord();
        spContainer.setRecordId((short)(-4092));
        spContainer.setOptions((short)15);
        sp.setRecordId((short)(-4086));
        sp.setOptions((short)322);
        sp.setShapeId(shapeId);
        sp.setFlags(2560);
        opt.setRecordId((short)(-4085));
        opt.addEscherProperty(new EscherShapePathProperty((short)324, 4));
        opt.addEscherProperty(new EscherBoolProperty((short)511, 1048592));
        this.addStandardOptions(shape, opt);
        final HSSFAnchor userAnchor = shape.getAnchor();
        if (userAnchor.isHorizontallyFlipped()) {
            sp.setFlags(sp.getFlags() | 0x40);
        }
        if (userAnchor.isVerticallyFlipped()) {
            sp.setFlags(sp.getFlags() | 0x80);
        }
        anchor = this.createAnchor(userAnchor);
        clientData.setRecordId((short)(-4079));
        clientData.setOptions((short)0);
        spContainer.addChildRecord(sp);
        spContainer.addChildRecord(opt);
        spContainer.addChildRecord(anchor);
        spContainer.addChildRecord(clientData);
        return spContainer;
    }
    
    private ObjRecord createObjRecord(final HSSFShape hssfShape, final int shapeId) {
        final HSSFShape shape = hssfShape;
        final ObjRecord obj = new ObjRecord();
        final CommonObjectDataSubRecord c = new CommonObjectDataSubRecord();
        c.setObjectType((short)((HSSFSimpleShape)shape).getShapeType());
        c.setObjectId(this.getCmoObjectId(shapeId));
        c.setLocked(true);
        c.setPrintable(true);
        c.setAutofill(true);
        c.setAutoline(true);
        final EndSubRecord e = new EndSubRecord();
        obj.addSubRecord(c);
        obj.addSubRecord(e);
        return obj;
    }
    
    @Override
    public EscherContainerRecord getSpContainer() {
        return this.spContainer;
    }
    
    @Override
    public ObjRecord getObjRecord() {
        return this.objRecord;
    }
}
