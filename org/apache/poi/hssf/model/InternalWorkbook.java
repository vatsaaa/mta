// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hssf.model;

import org.apache.poi.util.POILogFactory;
import org.apache.poi.hssf.record.RecalcIdRecord;
import org.apache.poi.ss.formula.FormulaShifter;
import org.apache.poi.ss.formula.ptg.Ptg;
import org.apache.poi.ss.formula.ptg.Ref3DPtg;
import org.apache.poi.ss.formula.ptg.OperandPtg;
import org.apache.poi.ss.formula.ptg.Area3DPtg;
import org.apache.poi.ddf.EscherSimpleProperty;
import org.apache.poi.ddf.EscherSpRecord;
import org.apache.poi.ddf.EscherDgRecord;
import org.apache.poi.hssf.record.EscherAggregate;
import org.apache.poi.ss.formula.ptg.NameXPtg;
import org.apache.poi.ss.formula.udf.UDFFinder;
import org.apache.poi.ddf.EscherRGBProperty;
import org.apache.poi.ddf.EscherProperty;
import org.apache.poi.ddf.EscherBoolProperty;
import org.apache.poi.ddf.EscherSplitMenuColorsRecord;
import org.apache.poi.ddf.EscherOptRecord;
import org.apache.poi.ddf.EscherContainerRecord;
import org.apache.poi.ddf.EscherDggRecord;
import org.apache.poi.ddf.EscherRecord;
import org.apache.poi.hssf.record.DrawingGroupRecord;
import java.util.Iterator;
import org.apache.poi.ss.formula.EvaluationWorkbook;
import org.apache.poi.hssf.record.ExtSSTRecord;
import java.util.Locale;
import org.apache.poi.hssf.record.CountryRecord;
import org.apache.poi.hssf.record.UseSelFSRecord;
import org.apache.poi.hssf.record.PaletteRecord;
import org.apache.poi.ss.usermodel.BuiltinFormats;
import org.apache.poi.hssf.record.BookBoolRecord;
import org.apache.poi.hssf.record.RefreshAllRecord;
import org.apache.poi.hssf.record.PrecisionRecord;
import org.apache.poi.hssf.record.HideObjRecord;
import org.apache.poi.hssf.record.PasswordRev4Record;
import org.apache.poi.hssf.record.ProtectionRev4Record;
import org.apache.poi.hssf.record.PasswordRecord;
import org.apache.poi.hssf.record.ProtectRecord;
import org.apache.poi.hssf.record.WindowProtectRecord;
import org.apache.poi.hssf.record.FnGroupCountRecord;
import org.apache.poi.hssf.record.DSFRecord;
import org.apache.poi.hssf.record.CodepageRecord;
import java.security.AccessControlException;
import org.apache.poi.hssf.record.MMSRecord;
import org.apache.poi.hssf.record.BOFRecord;
import org.apache.poi.hssf.record.common.UnicodeString;
import org.apache.poi.hssf.record.StyleRecord;
import org.apache.poi.hssf.record.ExtendedFormatRecord;
import org.apache.poi.hssf.record.TabIdRecord;
import org.apache.poi.hssf.record.BackupRecord;
import org.apache.poi.hssf.record.FontRecord;
import org.apache.poi.hssf.record.NameRecord;
import org.apache.poi.hssf.record.EOFRecord;
import org.apache.poi.hssf.record.InterfaceEndRecord;
import org.apache.poi.hssf.record.InterfaceHdrRecord;
import org.apache.poi.hssf.record.DateWindow1904Record;
import org.apache.poi.hssf.record.Record;
import java.util.LinkedHashMap;
import java.util.ArrayList;
import org.apache.poi.hssf.record.NameCommentRecord;
import java.util.Map;
import org.apache.poi.hssf.record.WriteProtectRecord;
import org.apache.poi.hssf.record.WriteAccessRecord;
import org.apache.poi.hssf.record.FileSharingRecord;
import org.apache.poi.hssf.record.WindowOneRecord;
import org.apache.poi.ddf.EscherBSERecord;
import org.apache.poi.hssf.record.HyperlinkRecord;
import org.apache.poi.hssf.record.FormatRecord;
import org.apache.poi.hssf.record.BoundSheetRecord;
import java.util.List;
import org.apache.poi.hssf.record.SSTRecord;
import org.apache.poi.util.POILogger;
import org.apache.poi.util.Internal;

@Internal
public final class InternalWorkbook
{
    private static final int MAX_SENSITIVE_SHEET_NAME_LEN = 31;
    private static final POILogger log;
    private static final int DEBUG;
    private static final short CODEPAGE = 1200;
    private final WorkbookRecordList records;
    protected SSTRecord sst;
    private LinkTable linkTable;
    private final List<BoundSheetRecord> boundsheets;
    private final List<FormatRecord> formats;
    private final List<HyperlinkRecord> hyperlinks;
    private int numxfs;
    private int numfonts;
    private int maxformatid;
    private boolean uses1904datewindowing;
    private DrawingManager2 drawingManager;
    private List<EscherBSERecord> escherBSERecords;
    private WindowOneRecord windowOne;
    private FileSharingRecord fileShare;
    private WriteAccessRecord writeAccess;
    private WriteProtectRecord writeProtect;
    private final Map<String, NameCommentRecord> commentRecords;
    
    private InternalWorkbook() {
        this.records = new WorkbookRecordList();
        this.boundsheets = new ArrayList<BoundSheetRecord>();
        this.formats = new ArrayList<FormatRecord>();
        this.hyperlinks = new ArrayList<HyperlinkRecord>();
        this.numxfs = 0;
        this.numfonts = 0;
        this.maxformatid = -1;
        this.uses1904datewindowing = false;
        this.escherBSERecords = new ArrayList<EscherBSERecord>();
        this.commentRecords = new LinkedHashMap<String, NameCommentRecord>();
    }
    
    public static InternalWorkbook createWorkbook(final List<Record> recs) {
        if (InternalWorkbook.log.check(POILogger.DEBUG)) {
            InternalWorkbook.log.log(InternalWorkbook.DEBUG, "Workbook (readfile) created with reclen=", recs.size());
        }
        final InternalWorkbook retval = new InternalWorkbook();
        final List<Record> records = new ArrayList<Record>(recs.size() / 3);
        retval.records.setRecords(records);
        int k = 0;
        while (k < recs.size()) {
            final Record rec = recs.get(k);
            if (rec.getSid() == 10) {
                records.add(rec);
                if (InternalWorkbook.log.check(POILogger.DEBUG)) {
                    InternalWorkbook.log.log(InternalWorkbook.DEBUG, "found workbook eof record at " + k);
                    break;
                }
                break;
            }
            else {
                Label_1275: {
                    switch (rec.getSid()) {
                        case 133: {
                            if (InternalWorkbook.log.check(POILogger.DEBUG)) {
                                InternalWorkbook.log.log(InternalWorkbook.DEBUG, "found boundsheet record at " + k);
                            }
                            retval.boundsheets.add((BoundSheetRecord)rec);
                            retval.records.setBspos(k);
                            break;
                        }
                        case 252: {
                            if (InternalWorkbook.log.check(POILogger.DEBUG)) {
                                InternalWorkbook.log.log(InternalWorkbook.DEBUG, "found sst record at " + k);
                            }
                            retval.sst = (SSTRecord)rec;
                            break;
                        }
                        case 49: {
                            if (InternalWorkbook.log.check(POILogger.DEBUG)) {
                                InternalWorkbook.log.log(InternalWorkbook.DEBUG, "found font record at " + k);
                            }
                            retval.records.setFontpos(k);
                            final InternalWorkbook internalWorkbook = retval;
                            ++internalWorkbook.numfonts;
                            break;
                        }
                        case 224: {
                            if (InternalWorkbook.log.check(POILogger.DEBUG)) {
                                InternalWorkbook.log.log(InternalWorkbook.DEBUG, "found XF record at " + k);
                            }
                            retval.records.setXfpos(k);
                            final InternalWorkbook internalWorkbook2 = retval;
                            ++internalWorkbook2.numxfs;
                            break;
                        }
                        case 317: {
                            if (InternalWorkbook.log.check(POILogger.DEBUG)) {
                                InternalWorkbook.log.log(InternalWorkbook.DEBUG, "found tabid record at " + k);
                            }
                            retval.records.setTabpos(k);
                            break;
                        }
                        case 18: {
                            if (InternalWorkbook.log.check(POILogger.DEBUG)) {
                                InternalWorkbook.log.log(InternalWorkbook.DEBUG, "found protect record at " + k);
                            }
                            retval.records.setProtpos(k);
                            break;
                        }
                        case 64: {
                            if (InternalWorkbook.log.check(POILogger.DEBUG)) {
                                InternalWorkbook.log.log(InternalWorkbook.DEBUG, "found backup record at " + k);
                            }
                            retval.records.setBackuppos(k);
                            break;
                        }
                        case 23: {
                            throw new RuntimeException("Extern sheet is part of LinkTable");
                        }
                        case 24:
                        case 430: {
                            if (InternalWorkbook.log.check(POILogger.DEBUG)) {
                                InternalWorkbook.log.log(InternalWorkbook.DEBUG, "found SupBook record at " + k);
                            }
                            retval.linkTable = new LinkTable(recs, k, retval.records, retval.commentRecords);
                            k += retval.linkTable.getRecordCount() - 1;
                            break Label_1275;
                        }
                        case 1054: {
                            if (InternalWorkbook.log.check(POILogger.DEBUG)) {
                                InternalWorkbook.log.log(InternalWorkbook.DEBUG, "found format record at " + k);
                            }
                            retval.formats.add((FormatRecord)rec);
                            retval.maxformatid = ((retval.maxformatid >= ((FormatRecord)rec).getIndexCode()) ? retval.maxformatid : ((FormatRecord)rec).getIndexCode());
                            break;
                        }
                        case 34: {
                            if (InternalWorkbook.log.check(POILogger.DEBUG)) {
                                InternalWorkbook.log.log(InternalWorkbook.DEBUG, "found datewindow1904 record at " + k);
                            }
                            retval.uses1904datewindowing = (((DateWindow1904Record)rec).getWindowing() == 1);
                            break;
                        }
                        case 146: {
                            if (InternalWorkbook.log.check(POILogger.DEBUG)) {
                                InternalWorkbook.log.log(InternalWorkbook.DEBUG, "found palette record at " + k);
                            }
                            retval.records.setPalettepos(k);
                            break;
                        }
                        case 61: {
                            if (InternalWorkbook.log.check(POILogger.DEBUG)) {
                                InternalWorkbook.log.log(InternalWorkbook.DEBUG, "found WindowOneRecord at " + k);
                            }
                            retval.windowOne = (WindowOneRecord)rec;
                            break;
                        }
                        case 92: {
                            if (InternalWorkbook.log.check(POILogger.DEBUG)) {
                                InternalWorkbook.log.log(InternalWorkbook.DEBUG, "found WriteAccess at " + k);
                            }
                            retval.writeAccess = (WriteAccessRecord)rec;
                            break;
                        }
                        case 134: {
                            if (InternalWorkbook.log.check(POILogger.DEBUG)) {
                                InternalWorkbook.log.log(InternalWorkbook.DEBUG, "found WriteProtect at " + k);
                            }
                            retval.writeProtect = (WriteProtectRecord)rec;
                            break;
                        }
                        case 91: {
                            if (InternalWorkbook.log.check(POILogger.DEBUG)) {
                                InternalWorkbook.log.log(InternalWorkbook.DEBUG, "found FileSharing at " + k);
                            }
                            retval.fileShare = (FileSharingRecord)rec;
                            break;
                        }
                        case 2196: {
                            final NameCommentRecord ncr = (NameCommentRecord)rec;
                            if (InternalWorkbook.log.check(POILogger.DEBUG)) {
                                InternalWorkbook.log.log(InternalWorkbook.DEBUG, "found NameComment at " + k);
                            }
                            retval.commentRecords.put(ncr.getNameText(), ncr);
                            break;
                        }
                    }
                    records.add(rec);
                }
                ++k;
            }
        }
        while (k < recs.size()) {
            final Record rec = recs.get(k);
            switch (rec.getSid()) {
                case 440: {
                    retval.hyperlinks.add((HyperlinkRecord)rec);
                    break;
                }
            }
            ++k;
        }
        if (retval.windowOne == null) {
            retval.windowOne = createWindowOne();
        }
        if (InternalWorkbook.log.check(POILogger.DEBUG)) {
            InternalWorkbook.log.log(InternalWorkbook.DEBUG, "exit create workbook from existing file function");
        }
        return retval;
    }
    
    public static InternalWorkbook createWorkbook() {
        if (InternalWorkbook.log.check(POILogger.DEBUG)) {
            InternalWorkbook.log.log(InternalWorkbook.DEBUG, "creating new workbook from scratch");
        }
        final InternalWorkbook retval = new InternalWorkbook();
        final List<Record> records = new ArrayList<Record>(30);
        retval.records.setRecords(records);
        final List<FormatRecord> formats = retval.formats;
        records.add(createBOF());
        records.add(new InterfaceHdrRecord(1200));
        records.add(createMMS());
        records.add(InterfaceEndRecord.instance);
        records.add(createWriteAccess());
        records.add(createCodepage());
        records.add(createDSF());
        records.add(createTabId());
        retval.records.setTabpos(records.size() - 1);
        records.add(createFnGroupCount());
        records.add(createWindowProtect());
        records.add(createProtect());
        retval.records.setProtpos(records.size() - 1);
        records.add(createPassword());
        records.add(createProtectionRev4());
        records.add(createPasswordRev4());
        records.add(retval.windowOne = createWindowOne());
        records.add(createBackup());
        retval.records.setBackuppos(records.size() - 1);
        records.add(createHideObj());
        records.add(createDateWindow1904());
        records.add(createPrecision());
        records.add(createRefreshAll());
        records.add(createBookBool());
        records.add(createFont());
        records.add(createFont());
        records.add(createFont());
        records.add(createFont());
        retval.records.setFontpos(records.size() - 1);
        retval.numfonts = 4;
        for (int i = 0; i <= 7; ++i) {
            final FormatRecord rec = createFormat(i);
            retval.maxformatid = ((retval.maxformatid >= rec.getIndexCode()) ? retval.maxformatid : rec.getIndexCode());
            formats.add(rec);
            records.add(rec);
        }
        for (int k = 0; k < 21; ++k) {
            records.add(createExtendedFormat(k));
            final InternalWorkbook internalWorkbook = retval;
            ++internalWorkbook.numxfs;
        }
        retval.records.setXfpos(records.size() - 1);
        for (int k = 0; k < 6; ++k) {
            records.add(createStyle(k));
        }
        records.add(createUseSelFS());
        final int nBoundSheets = 1;
        for (int j = 0; j < nBoundSheets; ++j) {
            final BoundSheetRecord bsr = createBoundSheet(j);
            records.add(bsr);
            retval.boundsheets.add(bsr);
            retval.records.setBspos(records.size() - 1);
        }
        records.add(createCountry());
        for (int j = 0; j < nBoundSheets; ++j) {
            retval.getOrCreateLinkTable().checkExternSheet(j);
        }
        records.add(retval.sst = new SSTRecord());
        records.add(createExtendedSST());
        records.add(EOFRecord.instance);
        if (InternalWorkbook.log.check(POILogger.DEBUG)) {
            InternalWorkbook.log.log(InternalWorkbook.DEBUG, "exit create new workbook from scratch");
        }
        return retval;
    }
    
    public NameRecord getSpecificBuiltinRecord(final byte name, final int sheetNumber) {
        return this.getOrCreateLinkTable().getSpecificBuiltinRecord(name, sheetNumber);
    }
    
    public void removeBuiltinRecord(final byte name, final int sheetIndex) {
        this.linkTable.removeBuiltinRecord(name, sheetIndex);
    }
    
    public int getNumRecords() {
        return this.records.size();
    }
    
    public FontRecord getFontRecordAt(final int idx) {
        int index = idx;
        if (index > 4) {
            --index;
        }
        if (index > this.numfonts - 1) {
            throw new ArrayIndexOutOfBoundsException("There are only " + this.numfonts + " font records, you asked for " + idx);
        }
        final FontRecord retval = (FontRecord)this.records.get(this.records.getFontpos() - (this.numfonts - 1) + index);
        return retval;
    }
    
    public int getFontIndex(final FontRecord font) {
        int i = 0;
        while (i <= this.numfonts) {
            final FontRecord thisFont = (FontRecord)this.records.get(this.records.getFontpos() - (this.numfonts - 1) + i);
            if (thisFont == font) {
                if (i > 3) {
                    return i + 1;
                }
                return i;
            }
            else {
                ++i;
            }
        }
        throw new IllegalArgumentException("Could not find that font!");
    }
    
    public FontRecord createNewFont() {
        final FontRecord rec = createFont();
        this.records.add(this.records.getFontpos() + 1, rec);
        this.records.setFontpos(this.records.getFontpos() + 1);
        ++this.numfonts;
        return rec;
    }
    
    public void removeFontRecord(final FontRecord rec) {
        this.records.remove(rec);
        --this.numfonts;
    }
    
    public int getNumberOfFontRecords() {
        return this.numfonts;
    }
    
    public void setSheetBof(final int sheetIndex, final int pos) {
        if (InternalWorkbook.log.check(POILogger.DEBUG)) {
            InternalWorkbook.log.log(InternalWorkbook.DEBUG, "setting bof for sheetnum =", sheetIndex, " at pos=", pos);
        }
        this.checkSheets(sheetIndex);
        this.getBoundSheetRec(sheetIndex).setPositionOfBof(pos);
    }
    
    private BoundSheetRecord getBoundSheetRec(final int sheetIndex) {
        return this.boundsheets.get(sheetIndex);
    }
    
    public BackupRecord getBackupRecord() {
        return (BackupRecord)this.records.get(this.records.getBackuppos());
    }
    
    public void setSheetName(final int sheetnum, String sheetname) {
        this.checkSheets(sheetnum);
        if (sheetname.length() > 31) {
            sheetname = sheetname.substring(0, 31);
        }
        final BoundSheetRecord sheet = this.boundsheets.get(sheetnum);
        sheet.setSheetname(sheetname);
    }
    
    public boolean doesContainsSheetName(final String name, final int excludeSheetIdx) {
        String aName = name;
        if (aName.length() > 31) {
            aName = aName.substring(0, 31);
        }
        for (int i = 0; i < this.boundsheets.size(); ++i) {
            final BoundSheetRecord boundSheetRecord = this.getBoundSheetRec(i);
            if (excludeSheetIdx != i) {
                String bName = boundSheetRecord.getSheetname();
                if (bName.length() > 31) {
                    bName = bName.substring(0, 31);
                }
                if (aName.equalsIgnoreCase(bName)) {
                    return true;
                }
            }
        }
        return false;
    }
    
    public void setSheetOrder(final String sheetname, final int pos) {
        final int sheetNumber = this.getSheetIndex(sheetname);
        this.boundsheets.add(pos, this.boundsheets.remove(sheetNumber));
    }
    
    public String getSheetName(final int sheetIndex) {
        return this.getBoundSheetRec(sheetIndex).getSheetname();
    }
    
    public boolean isSheetHidden(final int sheetnum) {
        return this.getBoundSheetRec(sheetnum).isHidden();
    }
    
    public boolean isSheetVeryHidden(final int sheetnum) {
        return this.getBoundSheetRec(sheetnum).isVeryHidden();
    }
    
    public void setSheetHidden(final int sheetnum, final boolean hidden) {
        this.getBoundSheetRec(sheetnum).setHidden(hidden);
    }
    
    public void setSheetHidden(final int sheetnum, final int hidden) {
        final BoundSheetRecord bsr = this.getBoundSheetRec(sheetnum);
        boolean h = false;
        boolean vh = false;
        if (hidden != 0) {
            if (hidden == 1) {
                h = true;
            }
            else {
                if (hidden != 2) {
                    throw new IllegalArgumentException("Invalid hidden flag " + hidden + " given, must be 0, 1 or 2");
                }
                vh = true;
            }
        }
        bsr.setHidden(h);
        bsr.setVeryHidden(vh);
    }
    
    public int getSheetIndex(final String name) {
        int retval = -1;
        for (int k = 0; k < this.boundsheets.size(); ++k) {
            final String sheet = this.getSheetName(k);
            if (sheet.equalsIgnoreCase(name)) {
                retval = k;
                break;
            }
        }
        return retval;
    }
    
    private void checkSheets(final int sheetnum) {
        if (this.boundsheets.size() <= sheetnum) {
            if (this.boundsheets.size() + 1 <= sheetnum) {
                throw new RuntimeException("Sheet number out of bounds!");
            }
            final BoundSheetRecord bsr = createBoundSheet(sheetnum);
            this.records.add(this.records.getBspos() + 1, bsr);
            this.records.setBspos(this.records.getBspos() + 1);
            this.boundsheets.add(bsr);
            this.getOrCreateLinkTable().checkExternSheet(sheetnum);
            this.fixTabIdRecord();
        }
        else if (this.records.getTabpos() > 0) {
            final TabIdRecord tir = (TabIdRecord)this.records.get(this.records.getTabpos());
            if (tir._tabids.length < this.boundsheets.size()) {
                this.fixTabIdRecord();
            }
        }
    }
    
    public void removeSheet(final int sheetIndex) {
        if (this.boundsheets.size() > sheetIndex) {
            this.records.remove(this.records.getBspos() - (this.boundsheets.size() - 1) + sheetIndex);
            this.boundsheets.remove(sheetIndex);
            this.fixTabIdRecord();
        }
        final int sheetNum1Based = sheetIndex + 1;
        for (int i = 0; i < this.getNumNames(); ++i) {
            final NameRecord nr = this.getNameRecord(i);
            if (nr.getSheetNumber() == sheetNum1Based) {
                nr.setSheetNumber(0);
            }
            else if (nr.getSheetNumber() > sheetNum1Based) {
                nr.setSheetNumber(nr.getSheetNumber() - 1);
            }
        }
    }
    
    private void fixTabIdRecord() {
        final TabIdRecord tir = (TabIdRecord)this.records.get(this.records.getTabpos());
        final short[] tia = new short[this.boundsheets.size()];
        for (short k = 0; k < tia.length; ++k) {
            tia[k] = k;
        }
        tir.setTabIdArray(tia);
    }
    
    public int getNumSheets() {
        if (InternalWorkbook.log.check(POILogger.DEBUG)) {
            InternalWorkbook.log.log(InternalWorkbook.DEBUG, "getNumSheets=", this.boundsheets.size());
        }
        return this.boundsheets.size();
    }
    
    public int getNumExFormats() {
        if (InternalWorkbook.log.check(POILogger.DEBUG)) {
            InternalWorkbook.log.log(InternalWorkbook.DEBUG, "getXF=", this.numxfs);
        }
        return this.numxfs;
    }
    
    public ExtendedFormatRecord getExFormatAt(final int index) {
        int xfptr = this.records.getXfpos() - (this.numxfs - 1);
        xfptr += index;
        final ExtendedFormatRecord retval = (ExtendedFormatRecord)this.records.get(xfptr);
        return retval;
    }
    
    public void removeExFormatRecord(final ExtendedFormatRecord rec) {
        this.records.remove(rec);
        --this.numxfs;
    }
    
    public ExtendedFormatRecord createCellXF() {
        final ExtendedFormatRecord xf = createExtendedFormat();
        this.records.add(this.records.getXfpos() + 1, xf);
        this.records.setXfpos(this.records.getXfpos() + 1);
        ++this.numxfs;
        return xf;
    }
    
    public StyleRecord getStyleRecord(final int xfIndex) {
        for (int i = this.records.getXfpos(); i < this.records.size(); ++i) {
            final Record r = this.records.get(i);
            if (!(r instanceof ExtendedFormatRecord)) {
                if (r instanceof StyleRecord) {
                    final StyleRecord sr = (StyleRecord)r;
                    if (sr.getXFIndex() == xfIndex) {
                        return sr;
                    }
                }
            }
        }
        return null;
    }
    
    public StyleRecord createStyleRecord(final int xfIndex) {
        final StyleRecord newSR = new StyleRecord();
        newSR.setXFIndex(xfIndex);
        int addAt = -1;
        for (int i = this.records.getXfpos(); i < this.records.size() && addAt == -1; ++i) {
            final Record r = this.records.get(i);
            if (!(r instanceof ExtendedFormatRecord)) {
                if (!(r instanceof StyleRecord)) {
                    addAt = i;
                }
            }
        }
        if (addAt == -1) {
            throw new IllegalStateException("No XF Records found!");
        }
        this.records.add(addAt, newSR);
        return newSR;
    }
    
    public int addSSTString(final UnicodeString string) {
        if (InternalWorkbook.log.check(POILogger.DEBUG)) {
            InternalWorkbook.log.log(InternalWorkbook.DEBUG, "insert to sst string='", string);
        }
        if (this.sst == null) {
            this.insertSST();
        }
        return this.sst.addString(string);
    }
    
    public UnicodeString getSSTString(final int str) {
        if (this.sst == null) {
            this.insertSST();
        }
        final UnicodeString retval = this.sst.getString(str);
        if (InternalWorkbook.log.check(POILogger.DEBUG)) {
            InternalWorkbook.log.log(InternalWorkbook.DEBUG, "Returning SST for index=", str, " String= ", retval);
        }
        return retval;
    }
    
    public void insertSST() {
        if (InternalWorkbook.log.check(POILogger.DEBUG)) {
            InternalWorkbook.log.log(InternalWorkbook.DEBUG, "creating new SST via insertSST!");
        }
        this.sst = new SSTRecord();
        this.records.add(this.records.size() - 1, createExtendedSST());
        this.records.add(this.records.size() - 2, this.sst);
    }
    
    public int serialize(final int offset, final byte[] data) {
        if (InternalWorkbook.log.check(POILogger.DEBUG)) {
            InternalWorkbook.log.log(InternalWorkbook.DEBUG, "Serializing Workbook with offsets");
        }
        int pos = 0;
        SSTRecord sst = null;
        int sstPos = 0;
        boolean wroteBoundSheets = false;
        for (int k = 0; k < this.records.size(); ++k) {
            Record record = this.records.get(k);
            int len = 0;
            if (record instanceof SSTRecord) {
                sst = (SSTRecord)record;
                sstPos = pos;
            }
            if (record.getSid() == 255 && sst != null) {
                record = sst.createExtSSTRecord(sstPos + offset);
            }
            if (record instanceof BoundSheetRecord) {
                if (!wroteBoundSheets) {
                    for (int i = 0; i < this.boundsheets.size(); ++i) {
                        len += this.getBoundSheetRec(i).serialize(pos + offset + len, data);
                    }
                    wroteBoundSheets = true;
                }
            }
            else {
                len = record.serialize(pos + offset, data);
            }
            pos += len;
        }
        if (InternalWorkbook.log.check(POILogger.DEBUG)) {
            InternalWorkbook.log.log(InternalWorkbook.DEBUG, "Exiting serialize workbook");
        }
        return pos;
    }
    
    public int getSize() {
        int retval = 0;
        SSTRecord sst = null;
        for (int k = 0; k < this.records.size(); ++k) {
            final Record record = this.records.get(k);
            if (record instanceof SSTRecord) {
                sst = (SSTRecord)record;
            }
            if (record.getSid() == 255 && sst != null) {
                retval += sst.calcExtSSTRecordSize();
            }
            else {
                retval += record.getRecordSize();
            }
        }
        return retval;
    }
    
    private static BOFRecord createBOF() {
        final BOFRecord retval = new BOFRecord();
        retval.setVersion(1536);
        retval.setType(5);
        retval.setBuild(4307);
        retval.setBuildYear(1996);
        retval.setHistoryBitMask(65);
        retval.setRequiredVersion(6);
        return retval;
    }
    
    private static MMSRecord createMMS() {
        final MMSRecord retval = new MMSRecord();
        retval.setAddMenuCount((byte)0);
        retval.setDelMenuCount((byte)0);
        return retval;
    }
    
    private static WriteAccessRecord createWriteAccess() {
        final WriteAccessRecord retval = new WriteAccessRecord();
        try {
            retval.setUsername(System.getProperty("user.name"));
        }
        catch (AccessControlException e) {
            retval.setUsername("POI");
        }
        return retval;
    }
    
    private static CodepageRecord createCodepage() {
        final CodepageRecord retval = new CodepageRecord();
        retval.setCodepage((short)1200);
        return retval;
    }
    
    private static DSFRecord createDSF() {
        return new DSFRecord(false);
    }
    
    private static TabIdRecord createTabId() {
        return new TabIdRecord();
    }
    
    private static FnGroupCountRecord createFnGroupCount() {
        final FnGroupCountRecord retval = new FnGroupCountRecord();
        retval.setCount((short)14);
        return retval;
    }
    
    private static WindowProtectRecord createWindowProtect() {
        return new WindowProtectRecord(false);
    }
    
    private static ProtectRecord createProtect() {
        return new ProtectRecord(false);
    }
    
    private static PasswordRecord createPassword() {
        return new PasswordRecord(0);
    }
    
    private static ProtectionRev4Record createProtectionRev4() {
        return new ProtectionRev4Record(false);
    }
    
    private static PasswordRev4Record createPasswordRev4() {
        return new PasswordRev4Record(0);
    }
    
    private static WindowOneRecord createWindowOne() {
        final WindowOneRecord retval = new WindowOneRecord();
        retval.setHorizontalHold((short)360);
        retval.setVerticalHold((short)270);
        retval.setWidth((short)14940);
        retval.setHeight((short)9150);
        retval.setOptions((short)56);
        retval.setActiveSheetIndex(0);
        retval.setFirstVisibleTab(0);
        retval.setNumSelectedTabs((short)1);
        retval.setTabWidthRatio((short)600);
        return retval;
    }
    
    private static BackupRecord createBackup() {
        final BackupRecord retval = new BackupRecord();
        retval.setBackup((short)0);
        return retval;
    }
    
    private static HideObjRecord createHideObj() {
        final HideObjRecord retval = new HideObjRecord();
        retval.setHideObj((short)0);
        return retval;
    }
    
    private static DateWindow1904Record createDateWindow1904() {
        final DateWindow1904Record retval = new DateWindow1904Record();
        retval.setWindowing((short)0);
        return retval;
    }
    
    private static PrecisionRecord createPrecision() {
        final PrecisionRecord retval = new PrecisionRecord();
        retval.setFullPrecision(true);
        return retval;
    }
    
    private static RefreshAllRecord createRefreshAll() {
        return new RefreshAllRecord(false);
    }
    
    private static BookBoolRecord createBookBool() {
        final BookBoolRecord retval = new BookBoolRecord();
        retval.setSaveLinkValues((short)0);
        return retval;
    }
    
    private static FontRecord createFont() {
        final FontRecord retval = new FontRecord();
        retval.setFontHeight((short)200);
        retval.setAttributes((short)0);
        retval.setColorPaletteIndex((short)32767);
        retval.setBoldWeight((short)400);
        retval.setFontName("Arial");
        return retval;
    }
    
    private static FormatRecord createFormat(final int id) {
        switch (id) {
            case 0: {
                return new FormatRecord(5, BuiltinFormats.getBuiltinFormat(5));
            }
            case 1: {
                return new FormatRecord(6, BuiltinFormats.getBuiltinFormat(6));
            }
            case 2: {
                return new FormatRecord(7, BuiltinFormats.getBuiltinFormat(7));
            }
            case 3: {
                return new FormatRecord(8, BuiltinFormats.getBuiltinFormat(8));
            }
            case 4: {
                return new FormatRecord(42, BuiltinFormats.getBuiltinFormat(42));
            }
            case 5: {
                return new FormatRecord(41, BuiltinFormats.getBuiltinFormat(41));
            }
            case 6: {
                return new FormatRecord(44, BuiltinFormats.getBuiltinFormat(44));
            }
            case 7: {
                return new FormatRecord(43, BuiltinFormats.getBuiltinFormat(43));
            }
            default: {
                throw new IllegalArgumentException("Unexpected id " + id);
            }
        }
    }
    
    private static ExtendedFormatRecord createExtendedFormat(final int id) {
        final ExtendedFormatRecord retval = new ExtendedFormatRecord();
        switch (id) {
            case 0: {
                retval.setFontIndex((short)0);
                retval.setFormatIndex((short)0);
                retval.setCellOptions((short)(-11));
                retval.setAlignmentOptions((short)32);
                retval.setIndentionOptions((short)0);
                retval.setBorderOptions((short)0);
                retval.setPaletteOptions((short)0);
                retval.setAdtlPaletteOptions((short)0);
                retval.setFillPaletteOptions((short)8384);
                break;
            }
            case 1: {
                retval.setFontIndex((short)1);
                retval.setFormatIndex((short)0);
                retval.setCellOptions((short)(-11));
                retval.setAlignmentOptions((short)32);
                retval.setIndentionOptions((short)(-3072));
                retval.setBorderOptions((short)0);
                retval.setPaletteOptions((short)0);
                retval.setAdtlPaletteOptions((short)0);
                retval.setFillPaletteOptions((short)8384);
                break;
            }
            case 2: {
                retval.setFontIndex((short)1);
                retval.setFormatIndex((short)0);
                retval.setCellOptions((short)(-11));
                retval.setAlignmentOptions((short)32);
                retval.setIndentionOptions((short)(-3072));
                retval.setBorderOptions((short)0);
                retval.setPaletteOptions((short)0);
                retval.setAdtlPaletteOptions((short)0);
                retval.setFillPaletteOptions((short)8384);
                break;
            }
            case 3: {
                retval.setFontIndex((short)2);
                retval.setFormatIndex((short)0);
                retval.setCellOptions((short)(-11));
                retval.setAlignmentOptions((short)32);
                retval.setIndentionOptions((short)(-3072));
                retval.setBorderOptions((short)0);
                retval.setPaletteOptions((short)0);
                retval.setAdtlPaletteOptions((short)0);
                retval.setFillPaletteOptions((short)8384);
                break;
            }
            case 4: {
                retval.setFontIndex((short)2);
                retval.setFormatIndex((short)0);
                retval.setCellOptions((short)(-11));
                retval.setAlignmentOptions((short)32);
                retval.setIndentionOptions((short)(-3072));
                retval.setBorderOptions((short)0);
                retval.setPaletteOptions((short)0);
                retval.setAdtlPaletteOptions((short)0);
                retval.setFillPaletteOptions((short)8384);
                break;
            }
            case 5: {
                retval.setFontIndex((short)0);
                retval.setFormatIndex((short)0);
                retval.setCellOptions((short)(-11));
                retval.setAlignmentOptions((short)32);
                retval.setIndentionOptions((short)(-3072));
                retval.setBorderOptions((short)0);
                retval.setPaletteOptions((short)0);
                retval.setAdtlPaletteOptions((short)0);
                retval.setFillPaletteOptions((short)8384);
                break;
            }
            case 6: {
                retval.setFontIndex((short)0);
                retval.setFormatIndex((short)0);
                retval.setCellOptions((short)(-11));
                retval.setAlignmentOptions((short)32);
                retval.setIndentionOptions((short)(-3072));
                retval.setBorderOptions((short)0);
                retval.setPaletteOptions((short)0);
                retval.setAdtlPaletteOptions((short)0);
                retval.setFillPaletteOptions((short)8384);
                break;
            }
            case 7: {
                retval.setFontIndex((short)0);
                retval.setFormatIndex((short)0);
                retval.setCellOptions((short)(-11));
                retval.setAlignmentOptions((short)32);
                retval.setIndentionOptions((short)(-3072));
                retval.setBorderOptions((short)0);
                retval.setPaletteOptions((short)0);
                retval.setAdtlPaletteOptions((short)0);
                retval.setFillPaletteOptions((short)8384);
                break;
            }
            case 8: {
                retval.setFontIndex((short)0);
                retval.setFormatIndex((short)0);
                retval.setCellOptions((short)(-11));
                retval.setAlignmentOptions((short)32);
                retval.setIndentionOptions((short)(-3072));
                retval.setBorderOptions((short)0);
                retval.setPaletteOptions((short)0);
                retval.setAdtlPaletteOptions((short)0);
                retval.setFillPaletteOptions((short)8384);
                break;
            }
            case 9: {
                retval.setFontIndex((short)0);
                retval.setFormatIndex((short)0);
                retval.setCellOptions((short)(-11));
                retval.setAlignmentOptions((short)32);
                retval.setIndentionOptions((short)(-3072));
                retval.setBorderOptions((short)0);
                retval.setPaletteOptions((short)0);
                retval.setAdtlPaletteOptions((short)0);
                retval.setFillPaletteOptions((short)8384);
                break;
            }
            case 10: {
                retval.setFontIndex((short)0);
                retval.setFormatIndex((short)0);
                retval.setCellOptions((short)(-11));
                retval.setAlignmentOptions((short)32);
                retval.setIndentionOptions((short)(-3072));
                retval.setBorderOptions((short)0);
                retval.setPaletteOptions((short)0);
                retval.setAdtlPaletteOptions((short)0);
                retval.setFillPaletteOptions((short)8384);
                break;
            }
            case 11: {
                retval.setFontIndex((short)0);
                retval.setFormatIndex((short)0);
                retval.setCellOptions((short)(-11));
                retval.setAlignmentOptions((short)32);
                retval.setIndentionOptions((short)(-3072));
                retval.setBorderOptions((short)0);
                retval.setPaletteOptions((short)0);
                retval.setAdtlPaletteOptions((short)0);
                retval.setFillPaletteOptions((short)8384);
                break;
            }
            case 12: {
                retval.setFontIndex((short)0);
                retval.setFormatIndex((short)0);
                retval.setCellOptions((short)(-11));
                retval.setAlignmentOptions((short)32);
                retval.setIndentionOptions((short)(-3072));
                retval.setBorderOptions((short)0);
                retval.setPaletteOptions((short)0);
                retval.setAdtlPaletteOptions((short)0);
                retval.setFillPaletteOptions((short)8384);
                break;
            }
            case 13: {
                retval.setFontIndex((short)0);
                retval.setFormatIndex((short)0);
                retval.setCellOptions((short)(-11));
                retval.setAlignmentOptions((short)32);
                retval.setIndentionOptions((short)(-3072));
                retval.setBorderOptions((short)0);
                retval.setPaletteOptions((short)0);
                retval.setAdtlPaletteOptions((short)0);
                retval.setFillPaletteOptions((short)8384);
                break;
            }
            case 14: {
                retval.setFontIndex((short)0);
                retval.setFormatIndex((short)0);
                retval.setCellOptions((short)(-11));
                retval.setAlignmentOptions((short)32);
                retval.setIndentionOptions((short)(-3072));
                retval.setBorderOptions((short)0);
                retval.setPaletteOptions((short)0);
                retval.setAdtlPaletteOptions((short)0);
                retval.setFillPaletteOptions((short)8384);
                break;
            }
            case 15: {
                retval.setFontIndex((short)0);
                retval.setFormatIndex((short)0);
                retval.setCellOptions((short)1);
                retval.setAlignmentOptions((short)32);
                retval.setIndentionOptions((short)0);
                retval.setBorderOptions((short)0);
                retval.setPaletteOptions((short)0);
                retval.setAdtlPaletteOptions((short)0);
                retval.setFillPaletteOptions((short)8384);
                break;
            }
            case 16: {
                retval.setFontIndex((short)1);
                retval.setFormatIndex((short)43);
                retval.setCellOptions((short)(-11));
                retval.setAlignmentOptions((short)32);
                retval.setIndentionOptions((short)(-2048));
                retval.setBorderOptions((short)0);
                retval.setPaletteOptions((short)0);
                retval.setAdtlPaletteOptions((short)0);
                retval.setFillPaletteOptions((short)8384);
                break;
            }
            case 17: {
                retval.setFontIndex((short)1);
                retval.setFormatIndex((short)41);
                retval.setCellOptions((short)(-11));
                retval.setAlignmentOptions((short)32);
                retval.setIndentionOptions((short)(-2048));
                retval.setBorderOptions((short)0);
                retval.setPaletteOptions((short)0);
                retval.setAdtlPaletteOptions((short)0);
                retval.setFillPaletteOptions((short)8384);
                break;
            }
            case 18: {
                retval.setFontIndex((short)1);
                retval.setFormatIndex((short)44);
                retval.setCellOptions((short)(-11));
                retval.setAlignmentOptions((short)32);
                retval.setIndentionOptions((short)(-2048));
                retval.setBorderOptions((short)0);
                retval.setPaletteOptions((short)0);
                retval.setAdtlPaletteOptions((short)0);
                retval.setFillPaletteOptions((short)8384);
                break;
            }
            case 19: {
                retval.setFontIndex((short)1);
                retval.setFormatIndex((short)42);
                retval.setCellOptions((short)(-11));
                retval.setAlignmentOptions((short)32);
                retval.setIndentionOptions((short)(-2048));
                retval.setBorderOptions((short)0);
                retval.setPaletteOptions((short)0);
                retval.setAdtlPaletteOptions((short)0);
                retval.setFillPaletteOptions((short)8384);
                break;
            }
            case 20: {
                retval.setFontIndex((short)1);
                retval.setFormatIndex((short)9);
                retval.setCellOptions((short)(-11));
                retval.setAlignmentOptions((short)32);
                retval.setIndentionOptions((short)(-2048));
                retval.setBorderOptions((short)0);
                retval.setPaletteOptions((short)0);
                retval.setAdtlPaletteOptions((short)0);
                retval.setFillPaletteOptions((short)8384);
                break;
            }
            case 21: {
                retval.setFontIndex((short)5);
                retval.setFormatIndex((short)0);
                retval.setCellOptions((short)1);
                retval.setAlignmentOptions((short)32);
                retval.setIndentionOptions((short)2048);
                retval.setBorderOptions((short)0);
                retval.setPaletteOptions((short)0);
                retval.setAdtlPaletteOptions((short)0);
                retval.setFillPaletteOptions((short)8384);
                break;
            }
            case 22: {
                retval.setFontIndex((short)6);
                retval.setFormatIndex((short)0);
                retval.setCellOptions((short)1);
                retval.setAlignmentOptions((short)32);
                retval.setIndentionOptions((short)23552);
                retval.setBorderOptions((short)0);
                retval.setPaletteOptions((short)0);
                retval.setAdtlPaletteOptions((short)0);
                retval.setFillPaletteOptions((short)8384);
                break;
            }
            case 23: {
                retval.setFontIndex((short)0);
                retval.setFormatIndex((short)49);
                retval.setCellOptions((short)1);
                retval.setAlignmentOptions((short)32);
                retval.setIndentionOptions((short)23552);
                retval.setBorderOptions((short)0);
                retval.setPaletteOptions((short)0);
                retval.setAdtlPaletteOptions((short)0);
                retval.setFillPaletteOptions((short)8384);
                break;
            }
            case 24: {
                retval.setFontIndex((short)0);
                retval.setFormatIndex((short)8);
                retval.setCellOptions((short)1);
                retval.setAlignmentOptions((short)32);
                retval.setIndentionOptions((short)23552);
                retval.setBorderOptions((short)0);
                retval.setPaletteOptions((short)0);
                retval.setAdtlPaletteOptions((short)0);
                retval.setFillPaletteOptions((short)8384);
                break;
            }
            case 25: {
                retval.setFontIndex((short)6);
                retval.setFormatIndex((short)8);
                retval.setCellOptions((short)1);
                retval.setAlignmentOptions((short)32);
                retval.setIndentionOptions((short)23552);
                retval.setBorderOptions((short)0);
                retval.setPaletteOptions((short)0);
                retval.setAdtlPaletteOptions((short)0);
                retval.setFillPaletteOptions((short)8384);
                break;
            }
        }
        return retval;
    }
    
    private static ExtendedFormatRecord createExtendedFormat() {
        final ExtendedFormatRecord retval = new ExtendedFormatRecord();
        retval.setFontIndex((short)0);
        retval.setFormatIndex((short)0);
        retval.setCellOptions((short)1);
        retval.setAlignmentOptions((short)32);
        retval.setIndentionOptions((short)0);
        retval.setBorderOptions((short)0);
        retval.setPaletteOptions((short)0);
        retval.setAdtlPaletteOptions((short)0);
        retval.setFillPaletteOptions((short)8384);
        retval.setTopBorderPaletteIdx((short)8);
        retval.setBottomBorderPaletteIdx((short)8);
        retval.setLeftBorderPaletteIdx((short)8);
        retval.setRightBorderPaletteIdx((short)8);
        return retval;
    }
    
    private static StyleRecord createStyle(final int id) {
        final StyleRecord retval = new StyleRecord();
        switch (id) {
            case 0: {
                retval.setXFIndex(16);
                retval.setBuiltinStyle(3);
                retval.setOutlineStyleLevel(-1);
                break;
            }
            case 1: {
                retval.setXFIndex(17);
                retval.setBuiltinStyle(6);
                retval.setOutlineStyleLevel(-1);
                break;
            }
            case 2: {
                retval.setXFIndex(18);
                retval.setBuiltinStyle(4);
                retval.setOutlineStyleLevel(-1);
                break;
            }
            case 3: {
                retval.setXFIndex(19);
                retval.setBuiltinStyle(7);
                retval.setOutlineStyleLevel(-1);
                break;
            }
            case 4: {
                retval.setXFIndex(0);
                retval.setBuiltinStyle(0);
                retval.setOutlineStyleLevel(-1);
                break;
            }
            case 5: {
                retval.setXFIndex(20);
                retval.setBuiltinStyle(5);
                retval.setOutlineStyleLevel(-1);
                break;
            }
        }
        return retval;
    }
    
    private static PaletteRecord createPalette() {
        return new PaletteRecord();
    }
    
    private static UseSelFSRecord createUseSelFS() {
        return new UseSelFSRecord(false);
    }
    
    private static BoundSheetRecord createBoundSheet(final int id) {
        return new BoundSheetRecord("Sheet" + (id + 1));
    }
    
    private static CountryRecord createCountry() {
        final CountryRecord retval = new CountryRecord();
        retval.setDefaultCountry((short)1);
        if (Locale.getDefault().toString().equals("ru_RU")) {
            retval.setCurrentCountry((short)7);
        }
        else {
            retval.setCurrentCountry((short)1);
        }
        return retval;
    }
    
    private static ExtSSTRecord createExtendedSST() {
        final ExtSSTRecord retval = new ExtSSTRecord();
        retval.setNumStringsPerBucket((short)8);
        return retval;
    }
    
    private LinkTable getOrCreateLinkTable() {
        if (this.linkTable == null) {
            this.linkTable = new LinkTable((short)this.getNumSheets(), this.records);
        }
        return this.linkTable;
    }
    
    public String findSheetNameFromExternSheet(final int externSheetIndex) {
        final int indexToSheet = this.linkTable.getIndexToInternalSheet(externSheetIndex);
        if (indexToSheet < 0) {
            return "";
        }
        if (indexToSheet >= this.boundsheets.size()) {
            return "";
        }
        return this.getSheetName(indexToSheet);
    }
    
    public EvaluationWorkbook.ExternalSheet getExternalSheet(final int externSheetIndex) {
        final String[] extNames = this.linkTable.getExternalBookAndSheetName(externSheetIndex);
        if (extNames == null) {
            return null;
        }
        return new EvaluationWorkbook.ExternalSheet(extNames[0], extNames[1]);
    }
    
    public EvaluationWorkbook.ExternalName getExternalName(final int externSheetIndex, final int externNameIndex) {
        final String nameName = this.linkTable.resolveNameXText(externSheetIndex, externNameIndex);
        if (nameName == null) {
            return null;
        }
        final int ix = this.linkTable.resolveNameXIx(externSheetIndex, externNameIndex);
        return new EvaluationWorkbook.ExternalName(nameName, externNameIndex, ix);
    }
    
    public int getSheetIndexFromExternSheetIndex(final int externSheetNumber) {
        return this.linkTable.getSheetIndexFromExternSheetIndex(externSheetNumber);
    }
    
    public short checkExternSheet(final int sheetNumber) {
        return (short)this.getOrCreateLinkTable().checkExternSheet(sheetNumber);
    }
    
    public int getExternalSheetIndex(final String workbookName, final String sheetName) {
        return this.getOrCreateLinkTable().getExternalSheetIndex(workbookName, sheetName);
    }
    
    public int getNumNames() {
        if (this.linkTable == null) {
            return 0;
        }
        return this.linkTable.getNumNames();
    }
    
    public NameRecord getNameRecord(final int index) {
        return this.linkTable.getNameRecord(index);
    }
    
    public NameCommentRecord getNameCommentRecord(final NameRecord nameRecord) {
        return this.commentRecords.get(nameRecord.getNameText());
    }
    
    public NameRecord createName() {
        return this.addName(new NameRecord());
    }
    
    public NameRecord addName(final NameRecord name) {
        final LinkTable linkTable = this.getOrCreateLinkTable();
        linkTable.addName(name);
        return name;
    }
    
    public NameRecord createBuiltInName(final byte builtInName, final int sheetNumber) {
        if (sheetNumber < 0 || sheetNumber + 1 > 32767) {
            throw new IllegalArgumentException("Sheet number [" + sheetNumber + "]is not valid ");
        }
        final NameRecord name = new NameRecord(builtInName, sheetNumber);
        if (this.linkTable.nameAlreadyExists(name)) {
            throw new RuntimeException("Builtin (" + builtInName + ") already exists for sheet (" + sheetNumber + ")");
        }
        this.addName(name);
        return name;
    }
    
    public void removeName(final int nameIndex) {
        if (this.linkTable.getNumNames() > nameIndex) {
            final int idx = this.findFirstRecordLocBySid((short)24);
            this.records.remove(idx + nameIndex);
            this.linkTable.removeName(nameIndex);
        }
    }
    
    public void updateNameCommentRecordCache(final NameCommentRecord commentRecord) {
        if (this.commentRecords.containsValue(commentRecord)) {
            for (final Map.Entry<String, NameCommentRecord> entry : this.commentRecords.entrySet()) {
                if (entry.getValue().equals(commentRecord)) {
                    this.commentRecords.remove(entry.getKey());
                    break;
                }
            }
        }
        this.commentRecords.put(commentRecord.getNameText(), commentRecord);
    }
    
    public short getFormat(final String format, final boolean createIfNotFound) {
        for (final FormatRecord r : this.formats) {
            if (r.getFormatString().equals(format)) {
                return (short)r.getIndexCode();
            }
        }
        if (createIfNotFound) {
            return (short)this.createFormat(format);
        }
        return -1;
    }
    
    public List<FormatRecord> getFormats() {
        return this.formats;
    }
    
    public int createFormat(final String formatString) {
        this.maxformatid = ((this.maxformatid >= 164) ? (this.maxformatid + 1) : 164);
        final FormatRecord rec = new FormatRecord(this.maxformatid, formatString);
        int pos;
        for (pos = 0; pos < this.records.size() && this.records.get(pos).getSid() != 1054; ++pos) {}
        pos += this.formats.size();
        this.formats.add(rec);
        this.records.add(pos, rec);
        return this.maxformatid;
    }
    
    public Record findFirstRecordBySid(final short sid) {
        for (final Record record : this.records) {
            if (record.getSid() == sid) {
                return record;
            }
        }
        return null;
    }
    
    public int findFirstRecordLocBySid(final short sid) {
        int index = 0;
        for (final Record record : this.records) {
            if (record.getSid() == sid) {
                return index;
            }
            ++index;
        }
        return -1;
    }
    
    public Record findNextRecordBySid(final short sid, final int pos) {
        int matches = 0;
        for (final Record record : this.records) {
            if (record.getSid() == sid && matches++ == pos) {
                return record;
            }
        }
        return null;
    }
    
    public List<HyperlinkRecord> getHyperlinks() {
        return this.hyperlinks;
    }
    
    public List<Record> getRecords() {
        return this.records.getRecords();
    }
    
    public boolean isUsing1904DateWindowing() {
        return this.uses1904datewindowing;
    }
    
    public PaletteRecord getCustomPalette() {
        final int palettePos = this.records.getPalettepos();
        PaletteRecord palette;
        if (palettePos != -1) {
            final Record rec = this.records.get(palettePos);
            if (!(rec instanceof PaletteRecord)) {
                throw new RuntimeException("InternalError: Expected PaletteRecord but got a '" + rec + "'");
            }
            palette = (PaletteRecord)rec;
        }
        else {
            palette = createPalette();
            this.records.add(1, palette);
            this.records.setPalettepos(1);
        }
        return palette;
    }
    
    public DrawingManager2 findDrawingGroup() {
        if (this.drawingManager != null) {
            return this.drawingManager;
        }
        for (final Record r : this.records) {
            if (r instanceof DrawingGroupRecord) {
                final DrawingGroupRecord dg = (DrawingGroupRecord)r;
                dg.processChildRecords();
                final EscherContainerRecord cr = dg.getEscherContainer();
                if (cr == null) {
                    continue;
                }
                EscherDggRecord dgg = null;
                EscherContainerRecord bStore = null;
                final Iterator<EscherRecord> it = cr.getChildIterator();
                while (it.hasNext()) {
                    final EscherRecord er = it.next();
                    if (er instanceof EscherDggRecord) {
                        dgg = (EscherDggRecord)er;
                    }
                    else {
                        if (er.getRecordId() != -4095) {
                            continue;
                        }
                        bStore = (EscherContainerRecord)er;
                    }
                }
                if (dgg != null) {
                    this.drawingManager = new DrawingManager2(dgg);
                    if (bStore != null) {
                        for (final EscherRecord bs : bStore.getChildRecords()) {
                            if (bs instanceof EscherBSERecord) {
                                this.escherBSERecords.add((EscherBSERecord)bs);
                            }
                        }
                    }
                    return this.drawingManager;
                }
                continue;
            }
        }
        final int dgLoc = this.findFirstRecordLocBySid((short)235);
        if (dgLoc != -1) {
            final DrawingGroupRecord dg2 = (DrawingGroupRecord)this.records.get(dgLoc);
            EscherDggRecord dgg2 = null;
            EscherContainerRecord bStore2 = null;
            for (final EscherRecord er2 : dg2.getEscherRecords()) {
                if (er2 instanceof EscherDggRecord) {
                    dgg2 = (EscherDggRecord)er2;
                }
                else {
                    if (er2.getRecordId() != -4095) {
                        continue;
                    }
                    bStore2 = (EscherContainerRecord)er2;
                }
            }
            if (dgg2 != null) {
                this.drawingManager = new DrawingManager2(dgg2);
                if (bStore2 != null) {
                    for (final EscherRecord bs2 : bStore2.getChildRecords()) {
                        if (bs2 instanceof EscherBSERecord) {
                            this.escherBSERecords.add((EscherBSERecord)bs2);
                        }
                    }
                }
            }
        }
        return this.drawingManager;
    }
    
    public void createDrawingGroup() {
        if (this.drawingManager == null) {
            final EscherContainerRecord dggContainer = new EscherContainerRecord();
            final EscherDggRecord dgg = new EscherDggRecord();
            final EscherOptRecord opt = new EscherOptRecord();
            final EscherSplitMenuColorsRecord splitMenuColors = new EscherSplitMenuColorsRecord();
            dggContainer.setRecordId((short)(-4096));
            dggContainer.setOptions((short)15);
            dgg.setRecordId((short)(-4090));
            dgg.setOptions((short)0);
            dgg.setShapeIdMax(1024);
            dgg.setNumShapesSaved(0);
            dgg.setDrawingsSaved(0);
            dgg.setFileIdClusters(new EscherDggRecord.FileIdCluster[0]);
            this.drawingManager = new DrawingManager2(dgg);
            EscherContainerRecord bstoreContainer = null;
            if (this.escherBSERecords.size() > 0) {
                bstoreContainer = new EscherContainerRecord();
                bstoreContainer.setRecordId((short)(-4095));
                bstoreContainer.setOptions((short)(this.escherBSERecords.size() << 4 | 0xF));
                for (final EscherRecord escherRecord : this.escherBSERecords) {
                    bstoreContainer.addChildRecord(escherRecord);
                }
            }
            opt.setRecordId((short)(-4085));
            opt.setOptions((short)51);
            opt.addEscherProperty(new EscherBoolProperty((short)191, 524296));
            opt.addEscherProperty(new EscherRGBProperty((short)385, 134217793));
            opt.addEscherProperty(new EscherRGBProperty((short)448, 134217792));
            splitMenuColors.setRecordId((short)(-3810));
            splitMenuColors.setOptions((short)64);
            splitMenuColors.setColor1(134217741);
            splitMenuColors.setColor2(134217740);
            splitMenuColors.setColor3(134217751);
            splitMenuColors.setColor4(268435703);
            dggContainer.addChildRecord(dgg);
            if (bstoreContainer != null) {
                dggContainer.addChildRecord(bstoreContainer);
            }
            dggContainer.addChildRecord(opt);
            dggContainer.addChildRecord(splitMenuColors);
            final int dgLoc = this.findFirstRecordLocBySid((short)235);
            if (dgLoc == -1) {
                final DrawingGroupRecord drawingGroup = new DrawingGroupRecord();
                drawingGroup.addEscherRecord(dggContainer);
                final int loc = this.findFirstRecordLocBySid((short)140);
                this.getRecords().add(loc + 1, drawingGroup);
            }
            else {
                final DrawingGroupRecord drawingGroup = new DrawingGroupRecord();
                drawingGroup.addEscherRecord(dggContainer);
                this.getRecords().set(dgLoc, drawingGroup);
            }
        }
    }
    
    public WindowOneRecord getWindowOne() {
        return this.windowOne;
    }
    
    public EscherBSERecord getBSERecord(final int pictureIndex) {
        return this.escherBSERecords.get(pictureIndex - 1);
    }
    
    public int addBSERecord(final EscherBSERecord e) {
        this.createDrawingGroup();
        this.escherBSERecords.add(e);
        final int dgLoc = this.findFirstRecordLocBySid((short)235);
        final DrawingGroupRecord drawingGroup = this.getRecords().get(dgLoc);
        final EscherContainerRecord dggContainer = (EscherContainerRecord)drawingGroup.getEscherRecord(0);
        EscherContainerRecord bstoreContainer;
        if (dggContainer.getChild(1).getRecordId() == -4095) {
            bstoreContainer = (EscherContainerRecord)dggContainer.getChild(1);
        }
        else {
            bstoreContainer = new EscherContainerRecord();
            bstoreContainer.setRecordId((short)(-4095));
            final List<EscherRecord> childRecords = dggContainer.getChildRecords();
            childRecords.add(1, bstoreContainer);
            dggContainer.setChildRecords(childRecords);
        }
        bstoreContainer.setOptions((short)(this.escherBSERecords.size() << 4 | 0xF));
        bstoreContainer.addChildRecord(e);
        return this.escherBSERecords.size();
    }
    
    public DrawingManager2 getDrawingManager() {
        return this.drawingManager;
    }
    
    public WriteProtectRecord getWriteProtect() {
        if (this.writeProtect == null) {
            this.writeProtect = new WriteProtectRecord();
            int i;
            for (i = 0, i = 0; i < this.records.size() && !(this.records.get(i) instanceof BOFRecord); ++i) {}
            this.records.add(i + 1, this.writeProtect);
        }
        return this.writeProtect;
    }
    
    public WriteAccessRecord getWriteAccess() {
        if (this.writeAccess == null) {
            this.writeAccess = createWriteAccess();
            int i;
            for (i = 0, i = 0; i < this.records.size() && !(this.records.get(i) instanceof InterfaceEndRecord); ++i) {}
            this.records.add(i + 1, this.writeAccess);
        }
        return this.writeAccess;
    }
    
    public FileSharingRecord getFileSharing() {
        if (this.fileShare == null) {
            this.fileShare = new FileSharingRecord();
            int i;
            for (i = 0, i = 0; i < this.records.size() && !(this.records.get(i) instanceof WriteAccessRecord); ++i) {}
            this.records.add(i + 1, this.fileShare);
        }
        return this.fileShare;
    }
    
    public boolean isWriteProtected() {
        if (this.fileShare == null) {
            return false;
        }
        final FileSharingRecord frec = this.getFileSharing();
        return frec.getReadOnly() == 1;
    }
    
    public void writeProtectWorkbook(final String password, final String username) {
        final int protIdx = -1;
        final FileSharingRecord frec = this.getFileSharing();
        final WriteAccessRecord waccess = this.getWriteAccess();
        final WriteProtectRecord wprotect = this.getWriteProtect();
        frec.setReadOnly((short)1);
        frec.setPassword(FileSharingRecord.hashPassword(password));
        frec.setUsername(username);
        waccess.setUsername(username);
    }
    
    public void unwriteProtectWorkbook() {
        this.records.remove(this.fileShare);
        this.records.remove(this.writeProtect);
        this.fileShare = null;
        this.writeProtect = null;
    }
    
    public String resolveNameXText(final int refIndex, final int definedNameIndex) {
        return this.linkTable.resolveNameXText(refIndex, definedNameIndex);
    }
    
    public NameXPtg getNameXPtg(final String name, final UDFFinder udf) {
        final LinkTable lnk = this.getOrCreateLinkTable();
        NameXPtg xptg = lnk.getNameXPtg(name);
        if (xptg == null && udf.findFunction(name) != null) {
            xptg = lnk.addNameXPtg(name);
        }
        return xptg;
    }
    
    public void cloneDrawings(final InternalSheet sheet) {
        this.findDrawingGroup();
        if (this.drawingManager == null) {
            return;
        }
        final int aggLoc = sheet.aggregateDrawingRecords(this.drawingManager, false);
        if (aggLoc != -1) {
            final EscherAggregate agg = (EscherAggregate)sheet.findFirstRecordBySid((short)9876);
            final EscherContainerRecord escherContainer = agg.getEscherContainer();
            if (escherContainer == null) {
                return;
            }
            final EscherDggRecord dgg = this.drawingManager.getDgg();
            final int dgId = this.drawingManager.findNewDrawingGroupId();
            dgg.addCluster(dgId, 0);
            dgg.setDrawingsSaved(dgg.getDrawingsSaved() + 1);
            EscherDgRecord dg = null;
            final Iterator<EscherRecord> it = escherContainer.getChildIterator();
            while (it.hasNext()) {
                final EscherRecord er = it.next();
                if (er instanceof EscherDgRecord) {
                    dg = (EscherDgRecord)er;
                    dg.setOptions((short)(dgId << 4));
                }
                else {
                    if (!(er instanceof EscherContainerRecord)) {
                        continue;
                    }
                    final EscherContainerRecord cp = (EscherContainerRecord)er;
                    for (final EscherContainerRecord shapeContainer : cp.getChildRecords()) {
                        for (final EscherRecord shapeChildRecord : shapeContainer.getChildRecords()) {
                            final int recordId = shapeChildRecord.getRecordId();
                            if (recordId == -4086) {
                                final EscherSpRecord sp = (EscherSpRecord)shapeChildRecord;
                                final int shapeId = this.drawingManager.allocateShapeId((short)dgId, dg);
                                dg.setNumShapes(dg.getNumShapes() - 1);
                                sp.setShapeId(shapeId);
                            }
                            else {
                                if (recordId != -4085) {
                                    continue;
                                }
                                final EscherOptRecord opt = (EscherOptRecord)shapeChildRecord;
                                final EscherSimpleProperty prop = opt.lookup(260);
                                if (prop == null) {
                                    continue;
                                }
                                final int pictureIndex = prop.getPropertyValue();
                                final EscherBSERecord bse = this.getBSERecord(pictureIndex);
                                bse.setRef(bse.getRef() + 1);
                            }
                        }
                    }
                }
            }
        }
    }
    
    public NameRecord cloneFilter(final int filterDbNameIndex, final int newSheetIndex) {
        final NameRecord origNameRecord = this.getNameRecord(filterDbNameIndex);
        final int newExtSheetIx = this.checkExternSheet(newSheetIndex);
        final Ptg[] ptgs = origNameRecord.getNameDefinition();
        for (int i = 0; i < ptgs.length; ++i) {
            final Ptg ptg = ptgs[i];
            if (ptg instanceof Area3DPtg) {
                final Area3DPtg a3p = (Area3DPtg)((OperandPtg)ptg).copy();
                a3p.setExternSheetIndex(newExtSheetIx);
                ptgs[i] = a3p;
            }
            else if (ptg instanceof Ref3DPtg) {
                final Ref3DPtg r3p = (Ref3DPtg)((OperandPtg)ptg).copy();
                r3p.setExternSheetIndex(newExtSheetIx);
                ptgs[i] = r3p;
            }
        }
        final NameRecord newNameRecord = this.createBuiltInName((byte)13, newSheetIndex + 1);
        newNameRecord.setNameDefinition(ptgs);
        newNameRecord.setHidden(true);
        return newNameRecord;
    }
    
    public void updateNamesAfterCellShift(final FormulaShifter shifter) {
        for (int i = 0; i < this.getNumNames(); ++i) {
            final NameRecord nr = this.getNameRecord(i);
            final Ptg[] ptgs = nr.getNameDefinition();
            if (shifter.adjustFormula(ptgs, nr.getSheetNumber())) {
                nr.setNameDefinition(ptgs);
            }
        }
    }
    
    public RecalcIdRecord getRecalcId() {
        RecalcIdRecord record = (RecalcIdRecord)this.findFirstRecordBySid((short)449);
        if (record == null) {
            record = new RecalcIdRecord();
            final int pos = this.findFirstRecordLocBySid((short)140);
            this.records.add(pos + 1, record);
        }
        return record;
    }
    
    public boolean changeExternalReference(final String oldUrl, final String newUrl) {
        return this.linkTable.changeExternalReference(oldUrl, newUrl);
    }
    
    static {
        log = POILogFactory.getLogger(InternalWorkbook.class);
        DEBUG = POILogger.DEBUG;
    }
}
