// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hssf.model;

import org.apache.poi.hssf.record.SubRecord;
import org.apache.poi.hssf.record.EndSubRecord;
import org.apache.poi.hssf.record.CommonObjectDataSubRecord;
import org.apache.poi.ddf.EscherRecord;
import org.apache.poi.util.LittleEndian;
import org.apache.poi.ddf.EscherArrayProperty;
import org.apache.poi.ddf.EscherShapePathProperty;
import org.apache.poi.ddf.EscherProperty;
import org.apache.poi.ddf.EscherSimpleProperty;
import org.apache.poi.ddf.EscherClientDataRecord;
import org.apache.poi.ddf.EscherOptRecord;
import org.apache.poi.ddf.EscherSpRecord;
import org.apache.poi.hssf.usermodel.HSSFShape;
import org.apache.poi.hssf.usermodel.HSSFPolygon;
import org.apache.poi.hssf.record.ObjRecord;
import org.apache.poi.ddf.EscherContainerRecord;

public class PolygonShape extends AbstractShape
{
    public static final short OBJECT_TYPE_MICROSOFT_OFFICE_DRAWING = 30;
    private EscherContainerRecord spContainer;
    private ObjRecord objRecord;
    
    PolygonShape(final HSSFPolygon hssfShape, final int shapeId) {
        this.spContainer = this.createSpContainer(hssfShape, shapeId);
        this.objRecord = this.createObjRecord(hssfShape, shapeId);
    }
    
    private EscherContainerRecord createSpContainer(final HSSFPolygon hssfShape, final int shapeId) {
        final HSSFShape shape = hssfShape;
        final EscherContainerRecord spContainer = new EscherContainerRecord();
        final EscherSpRecord sp = new EscherSpRecord();
        final EscherOptRecord opt = new EscherOptRecord();
        final EscherClientDataRecord clientData = new EscherClientDataRecord();
        spContainer.setRecordId((short)(-4092));
        spContainer.setOptions((short)15);
        sp.setRecordId((short)(-4086));
        sp.setOptions((short)370);
        sp.setShapeId(shapeId);
        if (hssfShape.getParent() == null) {
            sp.setFlags(2560);
        }
        else {
            sp.setFlags(2562);
        }
        opt.setRecordId((short)(-4085));
        opt.addEscherProperty(new EscherSimpleProperty((short)4, false, false, 0));
        opt.addEscherProperty(new EscherSimpleProperty((short)322, false, false, hssfShape.getDrawAreaWidth()));
        opt.addEscherProperty(new EscherSimpleProperty((short)323, false, false, hssfShape.getDrawAreaHeight()));
        opt.addEscherProperty(new EscherShapePathProperty((short)324, 4));
        final EscherArrayProperty verticesProp = new EscherArrayProperty((short)325, false, new byte[0]);
        verticesProp.setNumberOfElementsInArray(hssfShape.getXPoints().length + 1);
        verticesProp.setNumberOfElementsInMemory(hssfShape.getXPoints().length + 1);
        verticesProp.setSizeOfElements(65520);
        for (int i = 0; i < hssfShape.getXPoints().length; ++i) {
            final byte[] data = new byte[4];
            LittleEndian.putShort(data, 0, (short)hssfShape.getXPoints()[i]);
            LittleEndian.putShort(data, 2, (short)hssfShape.getYPoints()[i]);
            verticesProp.setElement(i, data);
        }
        final int point = hssfShape.getXPoints().length;
        final byte[] data = new byte[4];
        LittleEndian.putShort(data, 0, (short)hssfShape.getXPoints()[0]);
        LittleEndian.putShort(data, 2, (short)hssfShape.getYPoints()[0]);
        verticesProp.setElement(point, data);
        opt.addEscherProperty(verticesProp);
        final EscherArrayProperty segmentsProp = new EscherArrayProperty((short)326, false, null);
        segmentsProp.setSizeOfElements(2);
        segmentsProp.setNumberOfElementsInArray(hssfShape.getXPoints().length * 2 + 4);
        segmentsProp.setNumberOfElementsInMemory(hssfShape.getXPoints().length * 2 + 4);
        segmentsProp.setElement(0, new byte[] { 0, 64 });
        segmentsProp.setElement(1, new byte[] { 0, -84 });
        for (int j = 0; j < hssfShape.getXPoints().length; ++j) {
            segmentsProp.setElement(2 + j * 2, new byte[] { 1, 0 });
            segmentsProp.setElement(3 + j * 2, new byte[] { 0, -84 });
        }
        segmentsProp.setElement(segmentsProp.getNumberOfElementsInArray() - 2, new byte[] { 1, 96 });
        segmentsProp.setElement(segmentsProp.getNumberOfElementsInArray() - 1, new byte[] { 0, -128 });
        opt.addEscherProperty(segmentsProp);
        opt.addEscherProperty(new EscherSimpleProperty((short)383, false, false, 65537));
        opt.addEscherProperty(new EscherSimpleProperty((short)464, false, false, 0));
        opt.addEscherProperty(new EscherSimpleProperty((short)465, false, false, 0));
        opt.addEscherProperty(new EscherSimpleProperty((short)471, false, false, 0));
        this.addStandardOptions(shape, opt);
        final EscherRecord anchor = this.createAnchor(shape.getAnchor());
        clientData.setRecordId((short)(-4079));
        clientData.setOptions((short)0);
        spContainer.addChildRecord(sp);
        spContainer.addChildRecord(opt);
        spContainer.addChildRecord(anchor);
        spContainer.addChildRecord(clientData);
        return spContainer;
    }
    
    private ObjRecord createObjRecord(final HSSFShape hssfShape, final int shapeId) {
        final HSSFShape shape = hssfShape;
        final ObjRecord obj = new ObjRecord();
        final CommonObjectDataSubRecord c = new CommonObjectDataSubRecord();
        c.setObjectType((short)30);
        c.setObjectId(this.getCmoObjectId(shapeId));
        c.setLocked(true);
        c.setPrintable(true);
        c.setAutofill(true);
        c.setAutoline(true);
        final EndSubRecord e = new EndSubRecord();
        obj.addSubRecord(c);
        obj.addSubRecord(e);
        return obj;
    }
    
    @Override
    public EscherContainerRecord getSpContainer() {
        return this.spContainer;
    }
    
    @Override
    public ObjRecord getObjRecord() {
        return this.objRecord;
    }
}
