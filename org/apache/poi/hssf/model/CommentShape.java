// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hssf.model;

import java.util.Iterator;
import org.apache.poi.ddf.EscherSimpleProperty;
import org.apache.poi.ddf.EscherProperty;
import org.apache.poi.ddf.EscherOptRecord;
import org.apache.poi.hssf.usermodel.HSSFShape;
import java.util.List;
import org.apache.poi.hssf.record.ObjRecord;
import org.apache.poi.hssf.record.SubRecord;
import org.apache.poi.hssf.record.NoteStructureSubRecord;
import org.apache.poi.hssf.record.CommonObjectDataSubRecord;
import org.apache.poi.hssf.usermodel.HSSFTextbox;
import org.apache.poi.hssf.usermodel.HSSFComment;
import org.apache.poi.hssf.record.NoteRecord;

public final class CommentShape extends TextboxShape
{
    private NoteRecord _note;
    
    public CommentShape(final HSSFComment hssfShape, final int shapeId) {
        super(hssfShape, shapeId);
        this._note = this.createNoteRecord(hssfShape, shapeId);
        final ObjRecord obj = this.getObjRecord();
        final List<SubRecord> records = obj.getSubRecords();
        int cmoIdx = 0;
        for (int i = 0; i < records.size(); ++i) {
            final Object r = records.get(i);
            if (r instanceof CommonObjectDataSubRecord) {
                final CommonObjectDataSubRecord cmo = (CommonObjectDataSubRecord)r;
                cmo.setAutofill(false);
                cmoIdx = i;
            }
        }
        final NoteStructureSubRecord u = new NoteStructureSubRecord();
        obj.addSubRecord(cmoIdx + 1, u);
    }
    
    private NoteRecord createNoteRecord(final HSSFComment shape, final int shapeId) {
        final NoteRecord note = new NoteRecord();
        note.setColumn(shape.getColumn());
        note.setRow(shape.getRow());
        note.setFlags((short)(shape.isVisible() ? 2 : 0));
        note.setShapeId(shapeId);
        note.setAuthor((shape.getAuthor() == null) ? "" : shape.getAuthor());
        return note;
    }
    
    @Override
    protected int addStandardOptions(final HSSFShape shape, final EscherOptRecord opt) {
        super.addStandardOptions(shape, opt);
        final List<EscherProperty> props = opt.getEscherProperties();
        final Iterator<EscherProperty> iterator = props.iterator();
        while (iterator.hasNext()) {
            final EscherProperty prop = iterator.next();
            switch (prop.getId()) {
                case 129:
                case 130:
                case 131:
                case 132:
                case 387:
                case 448:
                case 959: {
                    iterator.remove();
                    continue;
                }
            }
        }
        final HSSFComment comment = (HSSFComment)shape;
        opt.addEscherProperty(new EscherSimpleProperty((short)959, comment.isVisible() ? 655360 : 655362));
        opt.addEscherProperty(new EscherSimpleProperty((short)575, 196611));
        opt.addEscherProperty(new EscherSimpleProperty((short)513, 0));
        opt.sortProperties();
        return opt.getEscherProperties().size();
    }
    
    public NoteRecord getNoteRecord() {
        return this._note;
    }
    
    @Override
    int getCmoObjectId(final int shapeId) {
        return shapeId;
    }
}
