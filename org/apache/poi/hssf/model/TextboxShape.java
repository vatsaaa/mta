// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hssf.model;

import org.apache.poi.hssf.usermodel.HSSFAnchor;
import org.apache.poi.ddf.EscherRecord;
import org.apache.poi.ddf.EscherProperty;
import org.apache.poi.ddf.EscherSimpleProperty;
import org.apache.poi.ddf.EscherClientDataRecord;
import org.apache.poi.ddf.EscherClientAnchorRecord;
import org.apache.poi.ddf.EscherOptRecord;
import org.apache.poi.ddf.EscherSpRecord;
import org.apache.poi.hssf.usermodel.HSSFShape;
import org.apache.poi.hssf.record.SubRecord;
import org.apache.poi.hssf.record.EndSubRecord;
import org.apache.poi.hssf.usermodel.HSSFSimpleShape;
import org.apache.poi.hssf.record.CommonObjectDataSubRecord;
import org.apache.poi.hssf.usermodel.HSSFTextbox;
import org.apache.poi.ddf.EscherTextboxRecord;
import org.apache.poi.hssf.record.ObjRecord;
import org.apache.poi.hssf.record.TextObjectRecord;
import org.apache.poi.ddf.EscherContainerRecord;

public class TextboxShape extends AbstractShape
{
    private EscherContainerRecord spContainer;
    private TextObjectRecord textObjectRecord;
    private ObjRecord objRecord;
    private EscherTextboxRecord escherTextbox;
    
    TextboxShape(final HSSFTextbox hssfShape, final int shapeId) {
        this.spContainer = this.createSpContainer(hssfShape, shapeId);
        this.objRecord = this.createObjRecord(hssfShape, shapeId);
        this.textObjectRecord = this.createTextObjectRecord(hssfShape, shapeId);
    }
    
    private ObjRecord createObjRecord(final HSSFTextbox hssfShape, final int shapeId) {
        final HSSFShape shape = hssfShape;
        final ObjRecord obj = new ObjRecord();
        final CommonObjectDataSubRecord c = new CommonObjectDataSubRecord();
        c.setObjectType((short)((HSSFSimpleShape)shape).getShapeType());
        c.setObjectId(this.getCmoObjectId(shapeId));
        c.setLocked(true);
        c.setPrintable(true);
        c.setAutofill(true);
        c.setAutoline(true);
        final EndSubRecord e = new EndSubRecord();
        obj.addSubRecord(c);
        obj.addSubRecord(e);
        return obj;
    }
    
    private EscherContainerRecord createSpContainer(final HSSFTextbox hssfShape, final int shapeId) {
        final HSSFTextbox shape = hssfShape;
        final EscherContainerRecord spContainer = new EscherContainerRecord();
        final EscherSpRecord sp = new EscherSpRecord();
        final EscherOptRecord opt = new EscherOptRecord();
        EscherRecord anchor = new EscherClientAnchorRecord();
        final EscherClientDataRecord clientData = new EscherClientDataRecord();
        this.escherTextbox = new EscherTextboxRecord();
        spContainer.setRecordId((short)(-4092));
        spContainer.setOptions((short)15);
        sp.setRecordId((short)(-4086));
        sp.setOptions((short)3234);
        sp.setShapeId(shapeId);
        sp.setFlags(2560);
        opt.setRecordId((short)(-4085));
        opt.addEscherProperty(new EscherSimpleProperty((short)128, 0));
        opt.addEscherProperty(new EscherSimpleProperty((short)129, shape.getMarginLeft()));
        opt.addEscherProperty(new EscherSimpleProperty((short)131, shape.getMarginRight()));
        opt.addEscherProperty(new EscherSimpleProperty((short)132, shape.getMarginBottom()));
        opt.addEscherProperty(new EscherSimpleProperty((short)130, shape.getMarginTop()));
        opt.addEscherProperty(new EscherSimpleProperty((short)133, 0));
        opt.addEscherProperty(new EscherSimpleProperty((short)135, 0));
        opt.addEscherProperty(new EscherSimpleProperty((short)959, 524288));
        this.addStandardOptions(shape, opt);
        final HSSFAnchor userAnchor = shape.getAnchor();
        anchor = this.createAnchor(userAnchor);
        clientData.setRecordId((short)(-4079));
        clientData.setOptions((short)0);
        this.escherTextbox.setRecordId((short)(-4083));
        this.escherTextbox.setOptions((short)0);
        spContainer.addChildRecord(sp);
        spContainer.addChildRecord(opt);
        spContainer.addChildRecord(anchor);
        spContainer.addChildRecord(clientData);
        spContainer.addChildRecord(this.escherTextbox);
        return spContainer;
    }
    
    private TextObjectRecord createTextObjectRecord(final HSSFTextbox hssfShape, final int shapeId) {
        final HSSFTextbox shape = hssfShape;
        final TextObjectRecord obj = new TextObjectRecord();
        obj.setHorizontalTextAlignment(hssfShape.getHorizontalAlignment());
        obj.setVerticalTextAlignment(hssfShape.getVerticalAlignment());
        obj.setTextLocked(true);
        obj.setTextOrientation(0);
        obj.setStr(shape.getString());
        return obj;
    }
    
    @Override
    public EscherContainerRecord getSpContainer() {
        return this.spContainer;
    }
    
    @Override
    public ObjRecord getObjRecord() {
        return this.objRecord;
    }
    
    public TextObjectRecord getTextObjectRecord() {
        return this.textObjectRecord;
    }
    
    public EscherRecord getEscherTextbox() {
        return this.escherTextbox;
    }
}
