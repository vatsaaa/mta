// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hssf.model;

import org.apache.poi.ddf.EscherDgRecord;
import java.util.ArrayList;
import java.util.List;
import org.apache.poi.ddf.EscherDggRecord;

public class DrawingManager2
{
    EscherDggRecord dgg;
    List drawingGroups;
    
    public DrawingManager2(final EscherDggRecord dgg) {
        this.drawingGroups = new ArrayList();
        this.dgg = dgg;
    }
    
    public void clearDrawingGroups() {
        this.drawingGroups.clear();
    }
    
    public EscherDgRecord createDgRecord() {
        final EscherDgRecord dg = new EscherDgRecord();
        dg.setRecordId((short)(-4088));
        final short dgId = this.findNewDrawingGroupId();
        dg.setOptions((short)(dgId << 4));
        dg.setNumShapes(0);
        dg.setLastMSOSPID(-1);
        this.drawingGroups.add(dg);
        this.dgg.addCluster(dgId, 0);
        this.dgg.setDrawingsSaved(this.dgg.getDrawingsSaved() + 1);
        return dg;
    }
    
    public int allocateShapeId(final short drawingGroupId) {
        final EscherDgRecord dg = this.getDrawingGroup(drawingGroupId);
        return this.allocateShapeId(drawingGroupId, dg);
    }
    
    public int allocateShapeId(final short drawingGroupId, final EscherDgRecord dg) {
        this.dgg.setNumShapesSaved(this.dgg.getNumShapesSaved() + 1);
        for (int i = 0; i < this.dgg.getFileIdClusters().length; ++i) {
            final EscherDggRecord.FileIdCluster c = this.dgg.getFileIdClusters()[i];
            if (c.getDrawingGroupId() == drawingGroupId && c.getNumShapeIdsUsed() != 1024) {
                final int result = c.getNumShapeIdsUsed() + 1024 * (i + 1);
                c.incrementShapeId();
                dg.setNumShapes(dg.getNumShapes() + 1);
                dg.setLastMSOSPID(result);
                if (result >= this.dgg.getShapeIdMax()) {
                    this.dgg.setShapeIdMax(result + 1);
                }
                return result;
            }
        }
        this.dgg.addCluster(drawingGroupId, 0);
        this.dgg.getFileIdClusters()[this.dgg.getFileIdClusters().length - 1].incrementShapeId();
        dg.setNumShapes(dg.getNumShapes() + 1);
        final int result2 = 1024 * this.dgg.getFileIdClusters().length;
        dg.setLastMSOSPID(result2);
        if (result2 >= this.dgg.getShapeIdMax()) {
            this.dgg.setShapeIdMax(result2 + 1);
        }
        return result2;
    }
    
    short findNewDrawingGroupId() {
        short dgId;
        for (dgId = 1; this.drawingGroupExists(dgId); ++dgId) {}
        return dgId;
    }
    
    EscherDgRecord getDrawingGroup(final int drawingGroupId) {
        return this.drawingGroups.get(drawingGroupId - 1);
    }
    
    boolean drawingGroupExists(final short dgId) {
        for (int i = 0; i < this.dgg.getFileIdClusters().length; ++i) {
            if (this.dgg.getFileIdClusters()[i].getDrawingGroupId() == dgId) {
                return true;
            }
        }
        return false;
    }
    
    int findFreeSPIDBlock() {
        final int max = this.dgg.getShapeIdMax();
        final int next = (max / 1024 + 1) * 1024;
        return next;
    }
    
    public EscherDggRecord getDgg() {
        return this.dgg;
    }
}
