// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hssf;

import org.apache.poi.OldFileFormatException;

public class OldExcelFormatException extends OldFileFormatException
{
    public OldExcelFormatException(final String s) {
        super(s);
    }
}
