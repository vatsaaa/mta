// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hssf.record;

import org.apache.poi.util.LittleEndianOutput;
import org.apache.poi.util.HexDump;
import org.apache.poi.util.LittleEndianInput;

public final class FtCblsSubRecord extends SubRecord
{
    public static final short sid = 12;
    private static final int ENCODED_SIZE = 20;
    private byte[] reserved;
    
    public FtCblsSubRecord() {
        this.reserved = new byte[20];
    }
    
    public FtCblsSubRecord(final LittleEndianInput in, final int size) {
        if (size != 20) {
            throw new RecordFormatException("Unexpected size (" + size + ")");
        }
        final byte[] buf = new byte[size];
        in.readFully(buf);
        this.reserved = buf;
    }
    
    @Override
    public String toString() {
        final StringBuffer buffer = new StringBuffer();
        buffer.append("[FtCbls ]").append("\n");
        buffer.append("  size     = ").append(this.getDataSize()).append("\n");
        buffer.append("  reserved = ").append(HexDump.toHex(this.reserved)).append("\n");
        buffer.append("[/FtCbls ]").append("\n");
        return buffer.toString();
    }
    
    @Override
    public void serialize(final LittleEndianOutput out) {
        out.writeShort(12);
        out.writeShort(this.reserved.length);
        out.write(this.reserved);
    }
    
    @Override
    protected int getDataSize() {
        return this.reserved.length;
    }
    
    public short getSid() {
        return 12;
    }
    
    @Override
    public Object clone() {
        final FtCblsSubRecord rec = new FtCblsSubRecord();
        final byte[] recdata = new byte[this.reserved.length];
        System.arraycopy(this.reserved, 0, recdata, 0, recdata.length);
        rec.reserved = recdata;
        return rec;
    }
}
