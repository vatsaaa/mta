// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hssf.record;

import org.apache.poi.util.LittleEndianOutput;
import org.apache.poi.util.HexDump;

public final class InterfaceHdrRecord extends StandardRecord
{
    public static final short sid = 225;
    private final int _codepage;
    public static final int CODEPAGE = 1200;
    
    public InterfaceHdrRecord(final int codePage) {
        this._codepage = codePage;
    }
    
    public InterfaceHdrRecord(final RecordInputStream in) {
        this._codepage = in.readShort();
    }
    
    @Override
    public String toString() {
        final StringBuffer buffer = new StringBuffer();
        buffer.append("[INTERFACEHDR]\n");
        buffer.append("    .codepage = ").append(HexDump.shortToHex(this._codepage)).append("\n");
        buffer.append("[/INTERFACEHDR]\n");
        return buffer.toString();
    }
    
    public void serialize(final LittleEndianOutput out) {
        out.writeShort(this._codepage);
    }
    
    @Override
    protected int getDataSize() {
        return 2;
    }
    
    @Override
    public short getSid() {
        return 225;
    }
}
