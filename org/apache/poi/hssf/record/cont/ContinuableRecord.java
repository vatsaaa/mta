// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hssf.record.cont;

import org.apache.poi.util.LittleEndianOutput;
import org.apache.poi.util.LittleEndianByteArrayOutputStream;
import org.apache.poi.hssf.record.Record;

public abstract class ContinuableRecord extends Record
{
    protected ContinuableRecord() {
    }
    
    protected abstract void serialize(final ContinuableRecordOutput p0);
    
    @Override
    public final int getRecordSize() {
        final ContinuableRecordOutput out = ContinuableRecordOutput.createForCountingOnly();
        this.serialize(out);
        out.terminate();
        return out.getTotalSize();
    }
    
    @Override
    public final int serialize(final int offset, final byte[] data) {
        final LittleEndianOutput leo = new LittleEndianByteArrayOutputStream(data, offset);
        final ContinuableRecordOutput out = new ContinuableRecordOutput(leo, this.getSid());
        this.serialize(out);
        out.terminate();
        return out.getTotalSize();
    }
}
