// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hssf.record;

import org.apache.poi.util.LittleEndianOutput;

public final class RightMarginRecord extends StandardRecord implements Margin
{
    public static final short sid = 39;
    private double field_1_margin;
    
    public RightMarginRecord() {
    }
    
    public RightMarginRecord(final RecordInputStream in) {
        this.field_1_margin = in.readDouble();
    }
    
    @Override
    public String toString() {
        final StringBuffer buffer = new StringBuffer();
        buffer.append("[RightMargin]\n");
        buffer.append("    .margin               = ").append(" (").append(this.getMargin()).append(" )\n");
        buffer.append("[/RightMargin]\n");
        return buffer.toString();
    }
    
    public void serialize(final LittleEndianOutput out) {
        out.writeDouble(this.field_1_margin);
    }
    
    @Override
    protected int getDataSize() {
        return 8;
    }
    
    @Override
    public short getSid() {
        return 39;
    }
    
    public double getMargin() {
        return this.field_1_margin;
    }
    
    public void setMargin(final double field_1_margin) {
        this.field_1_margin = field_1_margin;
    }
    
    @Override
    public Object clone() {
        final RightMarginRecord rec = new RightMarginRecord();
        rec.field_1_margin = this.field_1_margin;
        return rec;
    }
}
