// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hssf.record.common;

import org.apache.poi.util.LittleEndianOutput;
import org.apache.poi.hssf.record.RecordInputStream;

public final class FtrHeader
{
    private short recordType;
    private short grbitFrt;
    private byte[] reserved;
    
    public FtrHeader() {
        this.reserved = new byte[8];
    }
    
    public FtrHeader(final RecordInputStream in) {
        this.recordType = in.readShort();
        this.grbitFrt = in.readShort();
        in.read(this.reserved = new byte[8], 0, 8);
    }
    
    @Override
    public String toString() {
        final StringBuffer buffer = new StringBuffer();
        buffer.append(" [FUTURE HEADER]\n");
        buffer.append("   Type " + this.recordType);
        buffer.append("   Flags " + this.grbitFrt);
        buffer.append(" [/FUTURE HEADER]\n");
        return buffer.toString();
    }
    
    public void serialize(final LittleEndianOutput out) {
        out.writeShort(this.recordType);
        out.writeShort(this.grbitFrt);
        out.write(this.reserved);
    }
    
    public static int getDataSize() {
        return 12;
    }
    
    public short getRecordType() {
        return this.recordType;
    }
    
    public void setRecordType(final short recordType) {
        this.recordType = recordType;
    }
    
    public short getGrbitFrt() {
        return this.grbitFrt;
    }
    
    public void setGrbitFrt(final short grbitFrt) {
        this.grbitFrt = grbitFrt;
    }
    
    public byte[] getReserved() {
        return this.reserved;
    }
    
    public void setReserved(final byte[] reserved) {
        this.reserved = reserved;
    }
}
