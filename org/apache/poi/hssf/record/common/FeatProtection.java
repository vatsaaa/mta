// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hssf.record.common;

import org.apache.poi.util.LittleEndianOutput;
import org.apache.poi.util.LittleEndianInput;
import org.apache.poi.util.StringUtil;
import org.apache.poi.hssf.record.RecordInputStream;

public final class FeatProtection implements SharedFeature
{
    public static long NO_SELF_RELATIVE_SECURITY_FEATURE;
    public static long HAS_SELF_RELATIVE_SECURITY_FEATURE;
    private int fSD;
    private int passwordVerifier;
    private String title;
    private byte[] securityDescriptor;
    
    public FeatProtection() {
        this.securityDescriptor = new byte[0];
    }
    
    public FeatProtection(final RecordInputStream in) {
        this.fSD = in.readInt();
        this.passwordVerifier = in.readInt();
        this.title = StringUtil.readUnicodeString(in);
        this.securityDescriptor = in.readRemainder();
    }
    
    @Override
    public String toString() {
        final StringBuffer buffer = new StringBuffer();
        buffer.append(" [FEATURE PROTECTION]\n");
        buffer.append("   Self Relative = " + this.fSD);
        buffer.append("   Password Verifier = " + this.passwordVerifier);
        buffer.append("   Title = " + this.title);
        buffer.append("   Security Descriptor Size = " + this.securityDescriptor.length);
        buffer.append(" [/FEATURE PROTECTION]\n");
        return buffer.toString();
    }
    
    public void serialize(final LittleEndianOutput out) {
        out.writeInt(this.fSD);
        out.writeInt(this.passwordVerifier);
        StringUtil.writeUnicodeString(out, this.title);
        out.write(this.securityDescriptor);
    }
    
    public int getDataSize() {
        return 8 + StringUtil.getEncodedSize(this.title) + this.securityDescriptor.length;
    }
    
    public int getPasswordVerifier() {
        return this.passwordVerifier;
    }
    
    public void setPasswordVerifier(final int passwordVerifier) {
        this.passwordVerifier = passwordVerifier;
    }
    
    public String getTitle() {
        return this.title;
    }
    
    public void setTitle(final String title) {
        this.title = title;
    }
    
    public int getFSD() {
        return this.fSD;
    }
    
    static {
        FeatProtection.NO_SELF_RELATIVE_SECURITY_FEATURE = 0L;
        FeatProtection.HAS_SELF_RELATIVE_SECURITY_FEATURE = 1L;
    }
}
