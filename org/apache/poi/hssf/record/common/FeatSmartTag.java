// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hssf.record.common;

import org.apache.poi.util.LittleEndianOutput;
import org.apache.poi.hssf.record.RecordInputStream;

public final class FeatSmartTag implements SharedFeature
{
    private byte[] data;
    
    public FeatSmartTag() {
        this.data = new byte[0];
    }
    
    public FeatSmartTag(final RecordInputStream in) {
        this.data = in.readRemainder();
    }
    
    @Override
    public String toString() {
        final StringBuffer buffer = new StringBuffer();
        buffer.append(" [FEATURE SMART TAGS]\n");
        buffer.append(" [/FEATURE SMART TAGS]\n");
        return buffer.toString();
    }
    
    public int getDataSize() {
        return this.data.length;
    }
    
    public void serialize(final LittleEndianOutput out) {
        out.write(this.data);
    }
}
