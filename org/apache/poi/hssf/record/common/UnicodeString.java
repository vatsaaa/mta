// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hssf.record.common;

import org.apache.poi.util.StringUtil;
import org.apache.poi.util.BitFieldFactory;
import org.apache.poi.util.POILogFactory;
import org.apache.poi.util.LittleEndianOutput;
import org.apache.poi.hssf.record.cont.ContinuableRecordOutput;
import java.util.Iterator;
import java.util.Collections;
import org.apache.poi.hssf.record.cont.ContinuableRecordInput;
import org.apache.poi.util.LittleEndianInput;
import java.util.ArrayList;
import org.apache.poi.hssf.record.RecordInputStream;
import org.apache.poi.util.BitField;
import java.util.List;
import org.apache.poi.util.POILogger;

public class UnicodeString implements Comparable<UnicodeString>
{
    private static POILogger _logger;
    private short field_1_charCount;
    private byte field_2_optionflags;
    private String field_3_string;
    private List<FormatRun> field_4_format_runs;
    private ExtRst field_5_ext_rst;
    private static final BitField highByte;
    private static final BitField extBit;
    private static final BitField richText;
    
    private UnicodeString() {
    }
    
    public UnicodeString(final String str) {
        this.setString(str);
    }
    
    @Override
    public int hashCode() {
        int stringHash = 0;
        if (this.field_3_string != null) {
            stringHash = this.field_3_string.hashCode();
        }
        return this.field_1_charCount + stringHash;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (!(o instanceof UnicodeString)) {
            return false;
        }
        final UnicodeString other = (UnicodeString)o;
        final boolean eq = this.field_1_charCount == other.field_1_charCount && this.field_2_optionflags == other.field_2_optionflags && this.field_3_string.equals(other.field_3_string);
        if (!eq) {
            return false;
        }
        if (this.field_4_format_runs == null && other.field_4_format_runs == null) {
            return true;
        }
        if ((this.field_4_format_runs == null && other.field_4_format_runs != null) || (this.field_4_format_runs != null && other.field_4_format_runs == null)) {
            return false;
        }
        final int size = this.field_4_format_runs.size();
        if (size != other.field_4_format_runs.size()) {
            return false;
        }
        for (int i = 0; i < size; ++i) {
            final FormatRun run1 = this.field_4_format_runs.get(i);
            final FormatRun run2 = other.field_4_format_runs.get(i);
            if (!run1.equals(run2)) {
                return false;
            }
        }
        if (this.field_5_ext_rst != null || other.field_5_ext_rst != null) {
            if (this.field_5_ext_rst == null || other.field_5_ext_rst == null) {
                return false;
            }
            final int extCmp = this.field_5_ext_rst.compareTo(other.field_5_ext_rst);
            if (extCmp != 0) {
                return false;
            }
        }
        return true;
    }
    
    public UnicodeString(final RecordInputStream in) {
        this.field_1_charCount = in.readShort();
        this.field_2_optionflags = in.readByte();
        int runCount = 0;
        int extensionLength = 0;
        if (this.isRichText()) {
            runCount = in.readShort();
        }
        if (this.isExtendedText()) {
            extensionLength = in.readInt();
        }
        final boolean isCompressed = (this.field_2_optionflags & 0x1) == 0x0;
        if (isCompressed) {
            this.field_3_string = in.readCompressedUnicode(this.getCharCount());
        }
        else {
            this.field_3_string = in.readUnicodeLEString(this.getCharCount());
        }
        if (this.isRichText() && runCount > 0) {
            this.field_4_format_runs = new ArrayList<FormatRun>(runCount);
            for (int i = 0; i < runCount; ++i) {
                this.field_4_format_runs.add(new FormatRun(in));
            }
        }
        if (this.isExtendedText() && extensionLength > 0) {
            this.field_5_ext_rst = new ExtRst(new ContinuableRecordInput(in), extensionLength);
            if (this.field_5_ext_rst.getDataSize() + 4 != extensionLength) {
                UnicodeString._logger.log(POILogger.WARN, "ExtRst was supposed to be " + extensionLength + " bytes long, but seems to actually be " + (this.field_5_ext_rst.getDataSize() + 4));
            }
        }
    }
    
    public int getCharCount() {
        if (this.field_1_charCount < 0) {
            return this.field_1_charCount + 65536;
        }
        return this.field_1_charCount;
    }
    
    public short getCharCountShort() {
        return this.field_1_charCount;
    }
    
    public void setCharCount(final short cc) {
        this.field_1_charCount = cc;
    }
    
    public byte getOptionFlags() {
        return this.field_2_optionflags;
    }
    
    public void setOptionFlags(final byte of) {
        this.field_2_optionflags = of;
    }
    
    public String getString() {
        return this.field_3_string;
    }
    
    public void setString(final String string) {
        this.field_3_string = string;
        this.setCharCount((short)this.field_3_string.length());
        boolean useUTF16 = false;
        for (int strlen = string.length(), j = 0; j < strlen; ++j) {
            if (string.charAt(j) > '\u00ff') {
                useUTF16 = true;
                break;
            }
        }
        if (useUTF16) {
            this.field_2_optionflags = UnicodeString.highByte.setByte(this.field_2_optionflags);
        }
        else {
            this.field_2_optionflags = UnicodeString.highByte.clearByte(this.field_2_optionflags);
        }
    }
    
    public int getFormatRunCount() {
        if (this.field_4_format_runs == null) {
            return 0;
        }
        return this.field_4_format_runs.size();
    }
    
    public FormatRun getFormatRun(final int index) {
        if (this.field_4_format_runs == null) {
            return null;
        }
        if (index < 0 || index >= this.field_4_format_runs.size()) {
            return null;
        }
        return this.field_4_format_runs.get(index);
    }
    
    private int findFormatRunAt(final int characterPos) {
        for (int size = this.field_4_format_runs.size(), i = 0; i < size; ++i) {
            final FormatRun r = this.field_4_format_runs.get(i);
            if (r._character == characterPos) {
                return i;
            }
            if (r._character > characterPos) {
                return -1;
            }
        }
        return -1;
    }
    
    public void addFormatRun(final FormatRun r) {
        if (this.field_4_format_runs == null) {
            this.field_4_format_runs = new ArrayList<FormatRun>();
        }
        final int index = this.findFormatRunAt(r._character);
        if (index != -1) {
            this.field_4_format_runs.remove(index);
        }
        this.field_4_format_runs.add(r);
        Collections.sort(this.field_4_format_runs);
        this.field_2_optionflags = UnicodeString.richText.setByte(this.field_2_optionflags);
    }
    
    public Iterator<FormatRun> formatIterator() {
        if (this.field_4_format_runs != null) {
            return this.field_4_format_runs.iterator();
        }
        return null;
    }
    
    public void removeFormatRun(final FormatRun r) {
        this.field_4_format_runs.remove(r);
        if (this.field_4_format_runs.size() == 0) {
            this.field_4_format_runs = null;
            this.field_2_optionflags = UnicodeString.richText.clearByte(this.field_2_optionflags);
        }
    }
    
    public void clearFormatting() {
        this.field_4_format_runs = null;
        this.field_2_optionflags = UnicodeString.richText.clearByte(this.field_2_optionflags);
    }
    
    public ExtRst getExtendedRst() {
        return this.field_5_ext_rst;
    }
    
    void setExtendedRst(final ExtRst ext_rst) {
        if (ext_rst != null) {
            this.field_2_optionflags = UnicodeString.extBit.setByte(this.field_2_optionflags);
        }
        else {
            this.field_2_optionflags = UnicodeString.extBit.clearByte(this.field_2_optionflags);
        }
        this.field_5_ext_rst = ext_rst;
    }
    
    public void swapFontUse(final short oldFontIndex, final short newFontIndex) {
        for (final FormatRun run : this.field_4_format_runs) {
            if (run._fontIndex == oldFontIndex) {
                run._fontIndex = newFontIndex;
            }
        }
    }
    
    @Override
    public String toString() {
        return this.getString();
    }
    
    public String getDebugInfo() {
        final StringBuffer buffer = new StringBuffer();
        buffer.append("[UNICODESTRING]\n");
        buffer.append("    .charcount       = ").append(Integer.toHexString(this.getCharCount())).append("\n");
        buffer.append("    .optionflags     = ").append(Integer.toHexString(this.getOptionFlags())).append("\n");
        buffer.append("    .string          = ").append(this.getString()).append("\n");
        if (this.field_4_format_runs != null) {
            for (int i = 0; i < this.field_4_format_runs.size(); ++i) {
                final FormatRun r = this.field_4_format_runs.get(i);
                buffer.append("      .format_run" + i + "          = ").append(r.toString()).append("\n");
            }
        }
        if (this.field_5_ext_rst != null) {
            buffer.append("    .field_5_ext_rst          = ").append("\n");
            buffer.append(this.field_5_ext_rst.toString()).append("\n");
        }
        buffer.append("[/UNICODESTRING]\n");
        return buffer.toString();
    }
    
    public void serialize(final ContinuableRecordOutput out) {
        int numberOfRichTextRuns = 0;
        int extendedDataSize = 0;
        if (this.isRichText() && this.field_4_format_runs != null) {
            numberOfRichTextRuns = this.field_4_format_runs.size();
        }
        if (this.isExtendedText() && this.field_5_ext_rst != null) {
            extendedDataSize = 4 + this.field_5_ext_rst.getDataSize();
        }
        out.writeString(this.field_3_string, numberOfRichTextRuns, extendedDataSize);
        if (numberOfRichTextRuns > 0) {
            for (int i = 0; i < numberOfRichTextRuns; ++i) {
                if (out.getAvailableSpace() < 4) {
                    out.writeContinue();
                }
                final FormatRun r = this.field_4_format_runs.get(i);
                r.serialize(out);
            }
        }
        if (extendedDataSize > 0) {
            this.field_5_ext_rst.serialize(out);
        }
    }
    
    public int compareTo(final UnicodeString str) {
        int result = this.getString().compareTo(str.getString());
        if (result != 0) {
            return result;
        }
        if (this.field_4_format_runs == null && str.field_4_format_runs == null) {
            return 0;
        }
        if (this.field_4_format_runs == null && str.field_4_format_runs != null) {
            return 1;
        }
        if (this.field_4_format_runs != null && str.field_4_format_runs == null) {
            return -1;
        }
        final int size = this.field_4_format_runs.size();
        if (size != str.field_4_format_runs.size()) {
            return size - str.field_4_format_runs.size();
        }
        for (int i = 0; i < size; ++i) {
            final FormatRun run1 = this.field_4_format_runs.get(i);
            final FormatRun run2 = str.field_4_format_runs.get(i);
            result = run1.compareTo(run2);
            if (result != 0) {
                return result;
            }
        }
        if (this.field_5_ext_rst == null && str.field_5_ext_rst == null) {
            return 0;
        }
        if (this.field_5_ext_rst == null && str.field_5_ext_rst != null) {
            return 1;
        }
        if (this.field_5_ext_rst != null && str.field_5_ext_rst == null) {
            return -1;
        }
        result = this.field_5_ext_rst.compareTo(str.field_5_ext_rst);
        if (result != 0) {
            return result;
        }
        return 0;
    }
    
    private boolean isRichText() {
        return UnicodeString.richText.isSet(this.getOptionFlags());
    }
    
    private boolean isExtendedText() {
        return UnicodeString.extBit.isSet(this.getOptionFlags());
    }
    
    public Object clone() {
        final UnicodeString str = new UnicodeString();
        str.field_1_charCount = this.field_1_charCount;
        str.field_2_optionflags = this.field_2_optionflags;
        str.field_3_string = this.field_3_string;
        if (this.field_4_format_runs != null) {
            str.field_4_format_runs = new ArrayList<FormatRun>();
            for (final FormatRun r : this.field_4_format_runs) {
                str.field_4_format_runs.add(new FormatRun(r._character, r._fontIndex));
            }
        }
        if (this.field_5_ext_rst != null) {
            str.field_5_ext_rst = this.field_5_ext_rst.clone();
        }
        return str;
    }
    
    static {
        UnicodeString._logger = POILogFactory.getLogger(UnicodeString.class);
        highByte = BitFieldFactory.getInstance(1);
        extBit = BitFieldFactory.getInstance(4);
        richText = BitFieldFactory.getInstance(8);
    }
    
    public static class FormatRun implements Comparable<FormatRun>
    {
        final short _character;
        short _fontIndex;
        
        public FormatRun(final short character, final short fontIndex) {
            this._character = character;
            this._fontIndex = fontIndex;
        }
        
        public FormatRun(final LittleEndianInput in) {
            this(in.readShort(), in.readShort());
        }
        
        public short getCharacterPos() {
            return this._character;
        }
        
        public short getFontIndex() {
            return this._fontIndex;
        }
        
        @Override
        public boolean equals(final Object o) {
            if (!(o instanceof FormatRun)) {
                return false;
            }
            final FormatRun other = (FormatRun)o;
            return this._character == other._character && this._fontIndex == other._fontIndex;
        }
        
        public int compareTo(final FormatRun r) {
            if (this._character == r._character && this._fontIndex == r._fontIndex) {
                return 0;
            }
            if (this._character == r._character) {
                return this._fontIndex - r._fontIndex;
            }
            return this._character - r._character;
        }
        
        @Override
        public String toString() {
            return "character=" + this._character + ",fontIndex=" + this._fontIndex;
        }
        
        public void serialize(final LittleEndianOutput out) {
            out.writeShort(this._character);
            out.writeShort(this._fontIndex);
        }
    }
    
    public static class ExtRst implements Comparable<ExtRst>
    {
        private short reserved;
        private short formattingFontIndex;
        private short formattingOptions;
        private int numberOfRuns;
        private String phoneticText;
        private PhRun[] phRuns;
        private byte[] extraData;
        
        private void populateEmpty() {
            this.reserved = 1;
            this.phoneticText = "";
            this.phRuns = new PhRun[0];
            this.extraData = new byte[0];
        }
        
        protected ExtRst() {
            this.populateEmpty();
        }
        
        protected ExtRst(final LittleEndianInput in, final int expectedLength) {
            this.reserved = in.readShort();
            if (this.reserved == -1) {
                this.populateEmpty();
                return;
            }
            if (this.reserved != 1) {
                UnicodeString._logger.log(POILogger.WARN, "Warning - ExtRst has wrong magic marker, expecting 1 but found " + this.reserved + " - ignoring");
                for (int i = 0; i < expectedLength - 2; ++i) {
                    in.readByte();
                }
                this.populateEmpty();
                return;
            }
            final short stringDataSize = in.readShort();
            this.formattingFontIndex = in.readShort();
            this.formattingOptions = in.readShort();
            this.numberOfRuns = in.readUShort();
            final short length1 = in.readShort();
            short length2 = in.readShort();
            if (length1 == 0 && length2 > 0) {
                length2 = 0;
            }
            if (length1 != length2) {
                throw new IllegalStateException("The two length fields of the Phonetic Text don't agree! " + length1 + " vs " + length2);
            }
            this.phoneticText = StringUtil.readUnicodeLE(in, length1);
            final int runData = stringDataSize - 4 - 6 - 2 * this.phoneticText.length();
            final int numRuns = runData / 6;
            this.phRuns = new PhRun[numRuns];
            for (int j = 0; j < this.phRuns.length; ++j) {
                this.phRuns[j] = new PhRun(in);
            }
            int extraDataLength = runData - numRuns * 6;
            if (extraDataLength < 0) {
                System.err.println("Warning - ExtRst overran by " + (0 - extraDataLength) + " bytes");
                extraDataLength = 0;
            }
            this.extraData = new byte[extraDataLength];
            for (int k = 0; k < this.extraData.length; ++k) {
                this.extraData[k] = in.readByte();
            }
        }
        
        protected int getDataSize() {
            return 10 + 2 * this.phoneticText.length() + 6 * this.phRuns.length + this.extraData.length;
        }
        
        protected void serialize(final ContinuableRecordOutput out) {
            final int dataSize = this.getDataSize();
            out.writeContinueIfRequired(8);
            out.writeShort(this.reserved);
            out.writeShort(dataSize);
            out.writeShort(this.formattingFontIndex);
            out.writeShort(this.formattingOptions);
            out.writeContinueIfRequired(6);
            out.writeShort(this.numberOfRuns);
            out.writeShort(this.phoneticText.length());
            out.writeShort(this.phoneticText.length());
            out.writeContinueIfRequired(this.phoneticText.length() * 2);
            StringUtil.putUnicodeLE(this.phoneticText, out);
            for (int i = 0; i < this.phRuns.length; ++i) {
                this.phRuns[i].serialize(out);
            }
            out.write(this.extraData);
        }
        
        @Override
        public boolean equals(final Object obj) {
            if (!(obj instanceof ExtRst)) {
                return false;
            }
            final ExtRst other = (ExtRst)obj;
            return this.compareTo(other) == 0;
        }
        
        public int compareTo(final ExtRst o) {
            int result = this.reserved - o.reserved;
            if (result != 0) {
                return result;
            }
            result = this.formattingFontIndex - o.formattingFontIndex;
            if (result != 0) {
                return result;
            }
            result = this.formattingOptions - o.formattingOptions;
            if (result != 0) {
                return result;
            }
            result = this.numberOfRuns - o.numberOfRuns;
            if (result != 0) {
                return result;
            }
            result = this.phoneticText.compareTo(o.phoneticText);
            if (result != 0) {
                return result;
            }
            result = this.phRuns.length - o.phRuns.length;
            if (result != 0) {
                return result;
            }
            for (int i = 0; i < this.phRuns.length; ++i) {
                result = this.phRuns[i].phoneticTextFirstCharacterOffset - o.phRuns[i].phoneticTextFirstCharacterOffset;
                if (result != 0) {
                    return result;
                }
                result = this.phRuns[i].realTextFirstCharacterOffset - o.phRuns[i].realTextFirstCharacterOffset;
                if (result != 0) {
                    return result;
                }
                result = this.phRuns[i].realTextFirstCharacterOffset - o.phRuns[i].realTextLength;
                if (result != 0) {
                    return result;
                }
            }
            result = this.extraData.length - o.extraData.length;
            if (result != 0) {
                return result;
            }
            return 0;
        }
        
        @Override
        protected ExtRst clone() {
            final ExtRst ext = new ExtRst();
            ext.reserved = this.reserved;
            ext.formattingFontIndex = this.formattingFontIndex;
            ext.formattingOptions = this.formattingOptions;
            ext.numberOfRuns = this.numberOfRuns;
            ext.phoneticText = this.phoneticText;
            ext.phRuns = new PhRun[this.phRuns.length];
            for (int i = 0; i < ext.phRuns.length; ++i) {
                ext.phRuns[i] = new PhRun(this.phRuns[i].phoneticTextFirstCharacterOffset, this.phRuns[i].realTextFirstCharacterOffset, this.phRuns[i].realTextLength);
            }
            return ext;
        }
        
        public short getFormattingFontIndex() {
            return this.formattingFontIndex;
        }
        
        public short getFormattingOptions() {
            return this.formattingOptions;
        }
        
        public int getNumberOfRuns() {
            return this.numberOfRuns;
        }
        
        public String getPhoneticText() {
            return this.phoneticText;
        }
        
        public PhRun[] getPhRuns() {
            return this.phRuns;
        }
    }
    
    public static class PhRun
    {
        private int phoneticTextFirstCharacterOffset;
        private int realTextFirstCharacterOffset;
        private int realTextLength;
        
        public PhRun(final int phoneticTextFirstCharacterOffset, final int realTextFirstCharacterOffset, final int realTextLength) {
            this.phoneticTextFirstCharacterOffset = phoneticTextFirstCharacterOffset;
            this.realTextFirstCharacterOffset = realTextFirstCharacterOffset;
            this.realTextLength = realTextLength;
        }
        
        private PhRun(final LittleEndianInput in) {
            this.phoneticTextFirstCharacterOffset = in.readUShort();
            this.realTextFirstCharacterOffset = in.readUShort();
            this.realTextLength = in.readUShort();
        }
        
        private void serialize(final ContinuableRecordOutput out) {
            out.writeContinueIfRequired(6);
            out.writeShort(this.phoneticTextFirstCharacterOffset);
            out.writeShort(this.realTextFirstCharacterOffset);
            out.writeShort(this.realTextLength);
        }
    }
}
