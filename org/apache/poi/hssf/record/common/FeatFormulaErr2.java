// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hssf.record.common;

import org.apache.poi.util.BitFieldFactory;
import org.apache.poi.util.LittleEndianOutput;
import org.apache.poi.hssf.record.RecordInputStream;
import org.apache.poi.util.BitField;

public final class FeatFormulaErr2 implements SharedFeature
{
    static BitField checkCalculationErrors;
    static BitField checkEmptyCellRef;
    static BitField checkNumbersAsText;
    static BitField checkInconsistentRanges;
    static BitField checkInconsistentFormulas;
    static BitField checkDateTimeFormats;
    static BitField checkUnprotectedFormulas;
    static BitField performDataValidation;
    private int errorCheck;
    
    public FeatFormulaErr2() {
    }
    
    public FeatFormulaErr2(final RecordInputStream in) {
        this.errorCheck = in.readInt();
    }
    
    @Override
    public String toString() {
        final StringBuffer buffer = new StringBuffer();
        buffer.append(" [FEATURE FORMULA ERRORS]\n");
        buffer.append("  checkCalculationErrors    = ");
        buffer.append("  checkEmptyCellRef         = ");
        buffer.append("  checkNumbersAsText        = ");
        buffer.append("  checkInconsistentRanges   = ");
        buffer.append("  checkInconsistentFormulas = ");
        buffer.append("  checkDateTimeFormats      = ");
        buffer.append("  checkUnprotectedFormulas  = ");
        buffer.append("  performDataValidation     = ");
        buffer.append(" [/FEATURE FORMULA ERRORS]\n");
        return buffer.toString();
    }
    
    public void serialize(final LittleEndianOutput out) {
        out.writeInt(this.errorCheck);
    }
    
    public int getDataSize() {
        return 4;
    }
    
    public int _getRawErrorCheckValue() {
        return this.errorCheck;
    }
    
    public boolean getCheckCalculationErrors() {
        return FeatFormulaErr2.checkCalculationErrors.isSet(this.errorCheck);
    }
    
    public void setCheckCalculationErrors(final boolean checkCalculationErrors) {
        FeatFormulaErr2.checkCalculationErrors.setBoolean(this.errorCheck, checkCalculationErrors);
    }
    
    public boolean getCheckEmptyCellRef() {
        return FeatFormulaErr2.checkEmptyCellRef.isSet(this.errorCheck);
    }
    
    public void setCheckEmptyCellRef(final boolean checkEmptyCellRef) {
        FeatFormulaErr2.checkEmptyCellRef.setBoolean(this.errorCheck, checkEmptyCellRef);
    }
    
    public boolean getCheckNumbersAsText() {
        return FeatFormulaErr2.checkNumbersAsText.isSet(this.errorCheck);
    }
    
    public void setCheckNumbersAsText(final boolean checkNumbersAsText) {
        FeatFormulaErr2.checkNumbersAsText.setBoolean(this.errorCheck, checkNumbersAsText);
    }
    
    public boolean getCheckInconsistentRanges() {
        return FeatFormulaErr2.checkInconsistentRanges.isSet(this.errorCheck);
    }
    
    public void setCheckInconsistentRanges(final boolean checkInconsistentRanges) {
        FeatFormulaErr2.checkInconsistentRanges.setBoolean(this.errorCheck, checkInconsistentRanges);
    }
    
    public boolean getCheckInconsistentFormulas() {
        return FeatFormulaErr2.checkInconsistentFormulas.isSet(this.errorCheck);
    }
    
    public void setCheckInconsistentFormulas(final boolean checkInconsistentFormulas) {
        FeatFormulaErr2.checkInconsistentFormulas.setBoolean(this.errorCheck, checkInconsistentFormulas);
    }
    
    public boolean getCheckDateTimeFormats() {
        return FeatFormulaErr2.checkDateTimeFormats.isSet(this.errorCheck);
    }
    
    public void setCheckDateTimeFormats(final boolean checkDateTimeFormats) {
        FeatFormulaErr2.checkDateTimeFormats.setBoolean(this.errorCheck, checkDateTimeFormats);
    }
    
    public boolean getCheckUnprotectedFormulas() {
        return FeatFormulaErr2.checkUnprotectedFormulas.isSet(this.errorCheck);
    }
    
    public void setCheckUnprotectedFormulas(final boolean checkUnprotectedFormulas) {
        FeatFormulaErr2.checkUnprotectedFormulas.setBoolean(this.errorCheck, checkUnprotectedFormulas);
    }
    
    public boolean getPerformDataValidation() {
        return FeatFormulaErr2.performDataValidation.isSet(this.errorCheck);
    }
    
    public void setPerformDataValidation(final boolean performDataValidation) {
        FeatFormulaErr2.performDataValidation.setBoolean(this.errorCheck, performDataValidation);
    }
    
    static {
        FeatFormulaErr2.checkCalculationErrors = BitFieldFactory.getInstance(1);
        FeatFormulaErr2.checkEmptyCellRef = BitFieldFactory.getInstance(2);
        FeatFormulaErr2.checkNumbersAsText = BitFieldFactory.getInstance(4);
        FeatFormulaErr2.checkInconsistentRanges = BitFieldFactory.getInstance(8);
        FeatFormulaErr2.checkInconsistentFormulas = BitFieldFactory.getInstance(16);
        FeatFormulaErr2.checkDateTimeFormats = BitFieldFactory.getInstance(32);
        FeatFormulaErr2.checkUnprotectedFormulas = BitFieldFactory.getInstance(64);
        FeatFormulaErr2.performDataValidation = BitFieldFactory.getInstance(128);
    }
}
