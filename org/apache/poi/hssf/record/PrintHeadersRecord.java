// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hssf.record;

import org.apache.poi.util.LittleEndianOutput;

public final class PrintHeadersRecord extends StandardRecord
{
    public static final short sid = 42;
    private short field_1_print_headers;
    
    public PrintHeadersRecord() {
    }
    
    public PrintHeadersRecord(final RecordInputStream in) {
        this.field_1_print_headers = in.readShort();
    }
    
    public void setPrintHeaders(final boolean p) {
        if (p) {
            this.field_1_print_headers = 1;
        }
        else {
            this.field_1_print_headers = 0;
        }
    }
    
    public boolean getPrintHeaders() {
        return this.field_1_print_headers == 1;
    }
    
    @Override
    public String toString() {
        final StringBuffer buffer = new StringBuffer();
        buffer.append("[PRINTHEADERS]\n");
        buffer.append("    .printheaders   = ").append(this.getPrintHeaders()).append("\n");
        buffer.append("[/PRINTHEADERS]\n");
        return buffer.toString();
    }
    
    public void serialize(final LittleEndianOutput out) {
        out.writeShort(this.field_1_print_headers);
    }
    
    @Override
    protected int getDataSize() {
        return 2;
    }
    
    @Override
    public short getSid() {
        return 42;
    }
    
    @Override
    public Object clone() {
        final PrintHeadersRecord rec = new PrintHeadersRecord();
        rec.field_1_print_headers = this.field_1_print_headers;
        return rec;
    }
}
