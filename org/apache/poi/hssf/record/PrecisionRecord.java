// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hssf.record;

import org.apache.poi.util.LittleEndianOutput;

public final class PrecisionRecord extends StandardRecord
{
    public static final short sid = 14;
    public short field_1_precision;
    
    public PrecisionRecord() {
    }
    
    public PrecisionRecord(final RecordInputStream in) {
        this.field_1_precision = in.readShort();
    }
    
    public void setFullPrecision(final boolean fullprecision) {
        if (fullprecision) {
            this.field_1_precision = 1;
        }
        else {
            this.field_1_precision = 0;
        }
    }
    
    public boolean getFullPrecision() {
        return this.field_1_precision == 1;
    }
    
    @Override
    public String toString() {
        final StringBuffer buffer = new StringBuffer();
        buffer.append("[PRECISION]\n");
        buffer.append("    .precision       = ").append(this.getFullPrecision()).append("\n");
        buffer.append("[/PRECISION]\n");
        return buffer.toString();
    }
    
    public void serialize(final LittleEndianOutput out) {
        out.writeShort(this.field_1_precision);
    }
    
    @Override
    protected int getDataSize() {
        return 2;
    }
    
    @Override
    public short getSid() {
        return 14;
    }
}
