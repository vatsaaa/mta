// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hssf.record.crypto;

import org.apache.poi.util.HexDump;

final class RC4
{
    private int _i;
    private int _j;
    private final byte[] _s;
    
    public RC4(final byte[] key) {
        this._s = new byte[256];
        final int key_length = key.length;
        for (int i = 0; i < 256; ++i) {
            this._s[i] = (byte)i;
        }
        int i = 0;
        int j = 0;
        while (i < 256) {
            j = (j + key[i % key_length] + this._s[i] & 0xFF);
            final byte temp = this._s[i];
            this._s[i] = this._s[j];
            this._s[j] = temp;
            ++i;
        }
        this._i = 0;
        this._j = 0;
    }
    
    public byte output() {
        this._i = (this._i + 1 & 0xFF);
        this._j = (this._j + this._s[this._i] & 0xFF);
        final byte temp = this._s[this._i];
        this._s[this._i] = this._s[this._j];
        this._s[this._j] = temp;
        return this._s[this._s[this._i] + this._s[this._j] & 0xFF];
    }
    
    public void encrypt(final byte[] in) {
        for (int i = 0; i < in.length; ++i) {
            in[i] ^= this.output();
        }
    }
    
    public void encrypt(final byte[] in, final int offset, final int len) {
        for (int end = offset + len, i = offset; i < end; ++i) {
            in[i] ^= this.output();
        }
    }
    
    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer();
        sb.append(this.getClass().getName()).append(" [");
        sb.append("i=").append(this._i);
        sb.append(" j=").append(this._j);
        sb.append("]");
        sb.append("\n");
        sb.append(HexDump.dump(this._s, 0L, 0));
        return sb.toString();
    }
}
