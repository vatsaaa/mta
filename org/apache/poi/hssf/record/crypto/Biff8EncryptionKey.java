// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hssf.record.crypto;

import java.io.OutputStream;
import org.apache.poi.util.LittleEndianOutputStream;
import java.io.ByteArrayOutputStream;
import java.util.Arrays;
import java.security.NoSuchAlgorithmException;
import java.security.MessageDigest;
import org.apache.poi.util.HexDump;

public final class Biff8EncryptionKey
{
    private static final int KEY_DIGEST_LENGTH = 5;
    private static final int PASSWORD_HASH_NUMBER_OF_BYTES_USED = 5;
    private final byte[] _keyDigest;
    private static final ThreadLocal<String> _userPasswordTLS;
    
    public static Biff8EncryptionKey create(final byte[] docId) {
        return new Biff8EncryptionKey(createKeyDigest("VelvetSweatshop", docId));
    }
    
    public static Biff8EncryptionKey create(final String password, final byte[] docIdData) {
        return new Biff8EncryptionKey(createKeyDigest(password, docIdData));
    }
    
    Biff8EncryptionKey(final byte[] keyDigest) {
        if (keyDigest.length != 5) {
            throw new IllegalArgumentException("Expected 5 byte key digest, but got " + HexDump.toHex(keyDigest));
        }
        this._keyDigest = keyDigest;
    }
    
    static byte[] createKeyDigest(final String password, final byte[] docIdData) {
        check16Bytes(docIdData, "docId");
        final int nChars = Math.min(password.length(), 16);
        final byte[] passwordData = new byte[nChars * 2];
        for (int i = 0; i < nChars; ++i) {
            final char ch = password.charAt(i);
            passwordData[i * 2 + 0] = (byte)(ch << 0 & 0xFF);
            passwordData[i * 2 + 1] = (byte)(ch << 8 & 0xFF);
        }
        MessageDigest md5;
        try {
            md5 = MessageDigest.getInstance("MD5");
        }
        catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
        md5.update(passwordData);
        final byte[] passwordHash = md5.digest();
        md5.reset();
        for (int j = 0; j < 16; ++j) {
            md5.update(passwordHash, 0, 5);
            md5.update(docIdData, 0, docIdData.length);
        }
        final byte[] kd = md5.digest();
        final byte[] result = new byte[5];
        System.arraycopy(kd, 0, result, 0, 5);
        return result;
    }
    
    public boolean validate(final byte[] saltData, final byte[] saltHash) {
        check16Bytes(saltData, "saltData");
        check16Bytes(saltHash, "saltHash");
        final RC4 rc4 = this.createRC4(0);
        final byte[] saltDataPrime = saltData.clone();
        rc4.encrypt(saltDataPrime);
        final byte[] saltHashPrime = saltHash.clone();
        rc4.encrypt(saltHashPrime);
        MessageDigest md5;
        try {
            md5 = MessageDigest.getInstance("MD5");
        }
        catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
        md5.update(saltDataPrime);
        final byte[] finalSaltResult = md5.digest();
        return Arrays.equals(saltHashPrime, finalSaltResult);
    }
    
    private static byte[] xor(final byte[] a, final byte[] b) {
        final byte[] c = new byte[a.length];
        for (int i = 0; i < c.length; ++i) {
            c[i] = (byte)(a[i] ^ b[i]);
        }
        return c;
    }
    
    private static void check16Bytes(final byte[] data, final String argName) {
        if (data.length != 16) {
            throw new IllegalArgumentException("Expected 16 byte " + argName + ", but got " + HexDump.toHex(data));
        }
    }
    
    RC4 createRC4(final int keyBlockNo) {
        MessageDigest md5;
        try {
            md5 = MessageDigest.getInstance("MD5");
        }
        catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
        md5.update(this._keyDigest);
        final ByteArrayOutputStream baos = new ByteArrayOutputStream(4);
        new LittleEndianOutputStream(baos).writeInt(keyBlockNo);
        md5.update(baos.toByteArray());
        final byte[] digest = md5.digest();
        return new RC4(digest);
    }
    
    public static void setCurrentUserPassword(final String password) {
        Biff8EncryptionKey._userPasswordTLS.set(password);
    }
    
    public static String getCurrentUserPassword() {
        return Biff8EncryptionKey._userPasswordTLS.get();
    }
    
    static {
        _userPasswordTLS = new ThreadLocal<String>();
    }
}
