// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hssf.record.crypto;

final class Biff8RC4
{
    private static final int RC4_REKEYING_INTERVAL = 1024;
    private RC4 _rc4;
    private int _streamPos;
    private int _nextRC4BlockStart;
    private int _currentKeyIndex;
    private boolean _shouldSkipEncryptionOnCurrentRecord;
    private final Biff8EncryptionKey _key;
    
    public Biff8RC4(final int initialOffset, final Biff8EncryptionKey key) {
        if (initialOffset >= 1024) {
            throw new RuntimeException("initialOffset (" + initialOffset + ")>" + 1024 + " not supported yet");
        }
        this._key = key;
        this._streamPos = 0;
        this.rekeyForNextBlock();
        this._streamPos = initialOffset;
        for (int i = initialOffset; i > 0; --i) {
            this._rc4.output();
        }
        this._shouldSkipEncryptionOnCurrentRecord = false;
    }
    
    private void rekeyForNextBlock() {
        this._currentKeyIndex = this._streamPos / 1024;
        this._rc4 = this._key.createRC4(this._currentKeyIndex);
        this._nextRC4BlockStart = (this._currentKeyIndex + 1) * 1024;
    }
    
    private int getNextRC4Byte() {
        if (this._streamPos >= this._nextRC4BlockStart) {
            this.rekeyForNextBlock();
        }
        final byte mask = this._rc4.output();
        ++this._streamPos;
        if (this._shouldSkipEncryptionOnCurrentRecord) {
            return 0;
        }
        return mask & 0xFF;
    }
    
    public void startRecord(final int currentSid) {
        this._shouldSkipEncryptionOnCurrentRecord = isNeverEncryptedRecord(currentSid);
    }
    
    private static boolean isNeverEncryptedRecord(final int sid) {
        switch (sid) {
            case 47:
            case 225:
            case 2057: {
                return true;
            }
            default: {
                return false;
            }
        }
    }
    
    public void skipTwoBytes() {
        this.getNextRC4Byte();
        this.getNextRC4Byte();
    }
    
    public void xor(final byte[] buf, final int pOffset, final int pLen) {
        final int nLeftInBlock = this._nextRC4BlockStart - this._streamPos;
        if (pLen <= nLeftInBlock) {
            this._rc4.encrypt(buf, pOffset, pLen);
            this._streamPos += pLen;
            return;
        }
        int offset = pOffset;
        int len = pLen;
        if (len > nLeftInBlock) {
            if (nLeftInBlock > 0) {
                this._rc4.encrypt(buf, offset, nLeftInBlock);
                this._streamPos += nLeftInBlock;
                offset += nLeftInBlock;
                len -= nLeftInBlock;
            }
            this.rekeyForNextBlock();
        }
        while (len > 1024) {
            this._rc4.encrypt(buf, offset, 1024);
            this._streamPos += 1024;
            offset += 1024;
            len -= 1024;
            this.rekeyForNextBlock();
        }
        this._rc4.encrypt(buf, offset, len);
        this._streamPos += len;
    }
    
    public int xorByte(final int rawVal) {
        final int mask = this.getNextRC4Byte();
        return (byte)(rawVal ^ mask);
    }
    
    public int xorShort(final int rawVal) {
        final int b0 = this.getNextRC4Byte();
        final int b2 = this.getNextRC4Byte();
        final int mask = (b2 << 8) + (b0 << 0);
        return rawVal ^ mask;
    }
    
    public int xorInt(final int rawVal) {
        final int b0 = this.getNextRC4Byte();
        final int b2 = this.getNextRC4Byte();
        final int b3 = this.getNextRC4Byte();
        final int b4 = this.getNextRC4Byte();
        final int mask = (b4 << 24) + (b3 << 16) + (b2 << 8) + (b0 << 0);
        return rawVal ^ mask;
    }
    
    public long xorLong(final long rawVal) {
        final int b0 = this.getNextRC4Byte();
        final int b2 = this.getNextRC4Byte();
        final int b3 = this.getNextRC4Byte();
        final int b4 = this.getNextRC4Byte();
        final int b5 = this.getNextRC4Byte();
        final int b6 = this.getNextRC4Byte();
        final int b7 = this.getNextRC4Byte();
        final int b8 = this.getNextRC4Byte();
        final long mask = ((long)b8 << 56) + ((long)b7 << 48) + ((long)b6 << 40) + ((long)b5 << 32) + ((long)b4 << 24) + (b3 << 16) + (b2 << 8) + (b0 << 0);
        return rawVal ^ mask;
    }
}
