// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hssf.record;

import org.apache.poi.util.LittleEndianOutput;
import org.apache.poi.util.HexDump;

public final class PasswordRecord extends StandardRecord
{
    public static final short sid = 19;
    private int field_1_password;
    
    public PasswordRecord(final int password) {
        this.field_1_password = password;
    }
    
    public PasswordRecord(final RecordInputStream in) {
        this.field_1_password = in.readShort();
    }
    
    public static short hashPassword(final String password) {
        final byte[] passwordCharacters = password.getBytes();
        int hash = 0;
        if (passwordCharacters.length > 0) {
            int charIndex = passwordCharacters.length;
            while (charIndex-- > 0) {
                hash = ((hash >> 14 & 0x1) | (hash << 1 & 0x7FFF));
                hash ^= passwordCharacters[charIndex];
            }
            hash = ((hash >> 14 & 0x1) | (hash << 1 & 0x7FFF));
            hash ^= passwordCharacters.length;
            hash ^= 0xCE4B;
        }
        return (short)hash;
    }
    
    public void setPassword(final int password) {
        this.field_1_password = password;
    }
    
    public int getPassword() {
        return this.field_1_password;
    }
    
    @Override
    public String toString() {
        final StringBuffer buffer = new StringBuffer();
        buffer.append("[PASSWORD]\n");
        buffer.append("    .password = ").append(HexDump.shortToHex(this.field_1_password)).append("\n");
        buffer.append("[/PASSWORD]\n");
        return buffer.toString();
    }
    
    public void serialize(final LittleEndianOutput out) {
        out.writeShort(this.field_1_password);
    }
    
    @Override
    protected int getDataSize() {
        return 2;
    }
    
    @Override
    public short getSid() {
        return 19;
    }
    
    @Override
    public Object clone() {
        return new PasswordRecord(this.field_1_password);
    }
}
