// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hssf.record;

public final class FooterRecord extends HeaderFooterBase
{
    public static final short sid = 21;
    
    public FooterRecord(final String text) {
        super(text);
    }
    
    public FooterRecord(final RecordInputStream in) {
        super(in);
    }
    
    @Override
    public String toString() {
        final StringBuffer buffer = new StringBuffer();
        buffer.append("[FOOTER]\n");
        buffer.append("    .footer = ").append(this.getText()).append("\n");
        buffer.append("[/FOOTER]\n");
        return buffer.toString();
    }
    
    @Override
    public short getSid() {
        return 21;
    }
    
    @Override
    public Object clone() {
        return new FooterRecord(this.getText());
    }
}
