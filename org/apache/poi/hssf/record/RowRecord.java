// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hssf.record;

import org.apache.poi.util.BitFieldFactory;
import org.apache.poi.util.LittleEndianOutput;
import org.apache.poi.util.HexDump;
import org.apache.poi.util.BitField;

public final class RowRecord extends StandardRecord
{
    public static final short sid = 520;
    public static final int ENCODED_SIZE = 20;
    private static final int OPTION_BITS_ALWAYS_SET = 256;
    private static final int DEFAULT_HEIGHT_BIT = 32768;
    private int field_1_row_number;
    private int field_2_first_col;
    private int field_3_last_col;
    private short field_4_height;
    private short field_5_optimize;
    private short field_6_reserved;
    private int field_7_option_flags;
    private static final BitField outlineLevel;
    private static final BitField colapsed;
    private static final BitField zeroHeight;
    private static final BitField badFontHeight;
    private static final BitField formatted;
    private short field_8_xf_index;
    
    public RowRecord(final int rowNumber) {
        this.field_1_row_number = rowNumber;
        this.field_4_height = 255;
        this.field_5_optimize = 0;
        this.field_6_reserved = 0;
        this.field_7_option_flags = 256;
        this.field_8_xf_index = 15;
        this.setEmpty();
    }
    
    public RowRecord(final RecordInputStream in) {
        this.field_1_row_number = in.readUShort();
        this.field_2_first_col = in.readShort();
        this.field_3_last_col = in.readShort();
        this.field_4_height = in.readShort();
        this.field_5_optimize = in.readShort();
        this.field_6_reserved = in.readShort();
        this.field_7_option_flags = in.readShort();
        this.field_8_xf_index = in.readShort();
    }
    
    public void setEmpty() {
        this.field_2_first_col = 0;
        this.field_3_last_col = 0;
    }
    
    public boolean isEmpty() {
        return (this.field_2_first_col | this.field_3_last_col) == 0x0;
    }
    
    public void setRowNumber(final int row) {
        this.field_1_row_number = row;
    }
    
    public void setFirstCol(final int col) {
        this.field_2_first_col = col;
    }
    
    public void setLastCol(final int col) {
        this.field_3_last_col = col;
    }
    
    public void setHeight(final short height) {
        this.field_4_height = height;
    }
    
    public void setOptimize(final short optimize) {
        this.field_5_optimize = optimize;
    }
    
    public void setOutlineLevel(final short ol) {
        this.field_7_option_flags = RowRecord.outlineLevel.setValue(this.field_7_option_flags, ol);
    }
    
    public void setColapsed(final boolean c) {
        this.field_7_option_flags = RowRecord.colapsed.setBoolean(this.field_7_option_flags, c);
    }
    
    public void setZeroHeight(final boolean z) {
        this.field_7_option_flags = RowRecord.zeroHeight.setBoolean(this.field_7_option_flags, z);
    }
    
    public void setBadFontHeight(final boolean f) {
        this.field_7_option_flags = RowRecord.badFontHeight.setBoolean(this.field_7_option_flags, f);
    }
    
    public void setFormatted(final boolean f) {
        this.field_7_option_flags = RowRecord.formatted.setBoolean(this.field_7_option_flags, f);
    }
    
    public void setXFIndex(final short index) {
        this.field_8_xf_index = index;
    }
    
    public int getRowNumber() {
        return this.field_1_row_number;
    }
    
    public int getFirstCol() {
        return this.field_2_first_col;
    }
    
    public int getLastCol() {
        return this.field_3_last_col;
    }
    
    public short getHeight() {
        return this.field_4_height;
    }
    
    public short getOptimize() {
        return this.field_5_optimize;
    }
    
    public short getOptionFlags() {
        return (short)this.field_7_option_flags;
    }
    
    public short getOutlineLevel() {
        return (short)RowRecord.outlineLevel.getValue(this.field_7_option_flags);
    }
    
    public boolean getColapsed() {
        return RowRecord.colapsed.isSet(this.field_7_option_flags);
    }
    
    public boolean getZeroHeight() {
        return RowRecord.zeroHeight.isSet(this.field_7_option_flags);
    }
    
    public boolean getBadFontHeight() {
        return RowRecord.badFontHeight.isSet(this.field_7_option_flags);
    }
    
    public boolean getFormatted() {
        return RowRecord.formatted.isSet(this.field_7_option_flags);
    }
    
    public short getXFIndex() {
        return this.field_8_xf_index;
    }
    
    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer();
        sb.append("[ROW]\n");
        sb.append("    .rownumber      = ").append(Integer.toHexString(this.getRowNumber())).append("\n");
        sb.append("    .firstcol       = ").append(HexDump.shortToHex(this.getFirstCol())).append("\n");
        sb.append("    .lastcol        = ").append(HexDump.shortToHex(this.getLastCol())).append("\n");
        sb.append("    .height         = ").append(HexDump.shortToHex(this.getHeight())).append("\n");
        sb.append("    .optimize       = ").append(HexDump.shortToHex(this.getOptimize())).append("\n");
        sb.append("    .reserved       = ").append(HexDump.shortToHex(this.field_6_reserved)).append("\n");
        sb.append("    .optionflags    = ").append(HexDump.shortToHex(this.getOptionFlags())).append("\n");
        sb.append("        .outlinelvl = ").append(Integer.toHexString(this.getOutlineLevel())).append("\n");
        sb.append("        .colapsed   = ").append(this.getColapsed()).append("\n");
        sb.append("        .zeroheight = ").append(this.getZeroHeight()).append("\n");
        sb.append("        .badfontheig= ").append(this.getBadFontHeight()).append("\n");
        sb.append("        .formatted  = ").append(this.getFormatted()).append("\n");
        sb.append("    .xfindex        = ").append(Integer.toHexString(this.getXFIndex())).append("\n");
        sb.append("[/ROW]\n");
        return sb.toString();
    }
    
    public void serialize(final LittleEndianOutput out) {
        out.writeShort(this.getRowNumber());
        out.writeShort((this.getFirstCol() == -1) ? 0 : this.getFirstCol());
        out.writeShort((this.getLastCol() == -1) ? 0 : this.getLastCol());
        out.writeShort(this.getHeight());
        out.writeShort(this.getOptimize());
        out.writeShort(this.field_6_reserved);
        out.writeShort(this.getOptionFlags());
        out.writeShort(this.getXFIndex());
    }
    
    @Override
    protected int getDataSize() {
        return 16;
    }
    
    @Override
    public short getSid() {
        return 520;
    }
    
    @Override
    public Object clone() {
        final RowRecord rec = new RowRecord(this.field_1_row_number);
        rec.field_2_first_col = this.field_2_first_col;
        rec.field_3_last_col = this.field_3_last_col;
        rec.field_4_height = this.field_4_height;
        rec.field_5_optimize = this.field_5_optimize;
        rec.field_6_reserved = this.field_6_reserved;
        rec.field_7_option_flags = this.field_7_option_flags;
        rec.field_8_xf_index = this.field_8_xf_index;
        return rec;
    }
    
    static {
        outlineLevel = BitFieldFactory.getInstance(7);
        colapsed = BitFieldFactory.getInstance(16);
        zeroHeight = BitFieldFactory.getInstance(32);
        badFontHeight = BitFieldFactory.getInstance(64);
        formatted = BitFieldFactory.getInstance(128);
    }
}
