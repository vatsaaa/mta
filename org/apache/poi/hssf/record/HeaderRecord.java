// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hssf.record;

public final class HeaderRecord extends HeaderFooterBase
{
    public static final short sid = 20;
    
    public HeaderRecord(final String text) {
        super(text);
    }
    
    public HeaderRecord(final RecordInputStream in) {
        super(in);
    }
    
    @Override
    public String toString() {
        final StringBuffer buffer = new StringBuffer();
        buffer.append("[HEADER]\n");
        buffer.append("    .header = ").append(this.getText()).append("\n");
        buffer.append("[/HEADER]\n");
        return buffer.toString();
    }
    
    @Override
    public short getSid() {
        return 20;
    }
    
    @Override
    public Object clone() {
        return new HeaderRecord(this.getText());
    }
}
