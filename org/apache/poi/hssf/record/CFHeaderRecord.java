// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hssf.record;

import org.apache.poi.util.LittleEndianOutput;
import org.apache.poi.hssf.record.cf.CellRangeUtil;
import org.apache.poi.ss.util.CellRangeAddressList;
import org.apache.poi.ss.util.CellRangeAddress;

public final class CFHeaderRecord extends StandardRecord
{
    public static final short sid = 432;
    private int field_1_numcf;
    private int field_2_need_recalculation;
    private CellRangeAddress field_3_enclosing_cell_range;
    private CellRangeAddressList field_4_cell_ranges;
    
    public CFHeaderRecord() {
        this.field_4_cell_ranges = new CellRangeAddressList();
    }
    
    public CFHeaderRecord(final CellRangeAddress[] regions, final int nRules) {
        final CellRangeAddress[] unmergedRanges = regions;
        final CellRangeAddress[] mergeCellRanges = CellRangeUtil.mergeCellRanges(unmergedRanges);
        this.setCellRanges(mergeCellRanges);
        this.field_1_numcf = nRules;
    }
    
    public CFHeaderRecord(final RecordInputStream in) {
        this.field_1_numcf = in.readShort();
        this.field_2_need_recalculation = in.readShort();
        this.field_3_enclosing_cell_range = new CellRangeAddress(in);
        this.field_4_cell_ranges = new CellRangeAddressList(in);
    }
    
    public int getNumberOfConditionalFormats() {
        return this.field_1_numcf;
    }
    
    public void setNumberOfConditionalFormats(final int n) {
        this.field_1_numcf = n;
    }
    
    public boolean getNeedRecalculation() {
        return this.field_2_need_recalculation == 1;
    }
    
    public void setNeedRecalculation(final boolean b) {
        this.field_2_need_recalculation = (b ? 1 : 0);
    }
    
    public CellRangeAddress getEnclosingCellRange() {
        return this.field_3_enclosing_cell_range;
    }
    
    public void setEnclosingCellRange(final CellRangeAddress cr) {
        this.field_3_enclosing_cell_range = cr;
    }
    
    public void setCellRanges(final CellRangeAddress[] cellRanges) {
        if (cellRanges == null) {
            throw new IllegalArgumentException("cellRanges must not be null");
        }
        final CellRangeAddressList cral = new CellRangeAddressList();
        CellRangeAddress enclosingRange = null;
        for (int i = 0; i < cellRanges.length; ++i) {
            final CellRangeAddress cr = cellRanges[i];
            enclosingRange = CellRangeUtil.createEnclosingCellRange(cr, enclosingRange);
            cral.addCellRangeAddress(cr);
        }
        this.field_3_enclosing_cell_range = enclosingRange;
        this.field_4_cell_ranges = cral;
    }
    
    public CellRangeAddress[] getCellRanges() {
        return this.field_4_cell_ranges.getCellRangeAddresses();
    }
    
    @Override
    public String toString() {
        final StringBuffer buffer = new StringBuffer();
        buffer.append("[CFHEADER]\n");
        buffer.append("\t.id\t\t= ").append(Integer.toHexString(432)).append("\n");
        buffer.append("\t.numCF\t\t\t= ").append(this.getNumberOfConditionalFormats()).append("\n");
        buffer.append("\t.needRecalc\t   = ").append(this.getNeedRecalculation()).append("\n");
        buffer.append("\t.enclosingCellRange= ").append(this.getEnclosingCellRange()).append("\n");
        buffer.append("\t.cfranges=[");
        for (int i = 0; i < this.field_4_cell_ranges.countRanges(); ++i) {
            buffer.append((i == 0) ? "" : ",").append(this.field_4_cell_ranges.getCellRangeAddress(i).toString());
        }
        buffer.append("]\n");
        buffer.append("[/CFHEADER]\n");
        return buffer.toString();
    }
    
    @Override
    protected int getDataSize() {
        return 12 + this.field_4_cell_ranges.getSize();
    }
    
    public void serialize(final LittleEndianOutput out) {
        out.writeShort(this.field_1_numcf);
        out.writeShort(this.field_2_need_recalculation);
        this.field_3_enclosing_cell_range.serialize(out);
        this.field_4_cell_ranges.serialize(out);
    }
    
    @Override
    public short getSid() {
        return 432;
    }
    
    @Override
    public Object clone() {
        final CFHeaderRecord result = new CFHeaderRecord();
        result.field_1_numcf = this.field_1_numcf;
        result.field_2_need_recalculation = this.field_2_need_recalculation;
        result.field_3_enclosing_cell_range = this.field_3_enclosing_cell_range;
        result.field_4_cell_ranges = this.field_4_cell_ranges.copy();
        return result;
    }
}
