// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hssf.record;

import org.apache.poi.util.HexDump;
import org.apache.poi.util.LittleEndianOutput;

public final class FilePassRecord extends StandardRecord
{
    public static final short sid = 47;
    private int _encryptionType;
    private int _encryptionInfo;
    private int _minorVersionNo;
    private byte[] _docId;
    private byte[] _saltData;
    private byte[] _saltHash;
    private static final int ENCRYPTION_XOR = 0;
    private static final int ENCRYPTION_OTHER = 1;
    private static final int ENCRYPTION_OTHER_RC4 = 1;
    private static final int ENCRYPTION_OTHER_CAPI_2 = 2;
    private static final int ENCRYPTION_OTHER_CAPI_3 = 3;
    
    public FilePassRecord(final RecordInputStream in) {
        switch (this._encryptionType = in.readUShort()) {
            case 0: {
                throw new RecordFormatException("HSSF does not currently support XOR obfuscation");
            }
            case 1: {
                switch (this._encryptionInfo = in.readUShort()) {
                    case 1: {
                        this._minorVersionNo = in.readUShort();
                        if (this._minorVersionNo != 1) {
                            throw new RecordFormatException("Unexpected VersionInfo number for RC4Header " + this._minorVersionNo);
                        }
                        this._docId = read(in, 16);
                        this._saltData = read(in, 16);
                        this._saltHash = read(in, 16);
                        return;
                    }
                    case 2:
                    case 3: {
                        throw new RecordFormatException("HSSF does not currently support CryptoAPI encryption");
                    }
                    default: {
                        throw new RecordFormatException("Unknown encryption info " + this._encryptionInfo);
                    }
                }
                break;
            }
            default: {
                throw new RecordFormatException("Unknown encryption type " + this._encryptionType);
            }
        }
    }
    
    private static byte[] read(final RecordInputStream in, final int size) {
        final byte[] result = new byte[size];
        in.readFully(result);
        return result;
    }
    
    public void serialize(final LittleEndianOutput out) {
        out.writeShort(this._encryptionType);
        out.writeShort(this._encryptionInfo);
        out.writeShort(this._minorVersionNo);
        out.write(this._docId);
        out.write(this._saltData);
        out.write(this._saltHash);
    }
    
    @Override
    protected int getDataSize() {
        return 54;
    }
    
    public byte[] getDocId() {
        return this._docId.clone();
    }
    
    public void setDocId(final byte[] docId) {
        this._docId = docId.clone();
    }
    
    public byte[] getSaltData() {
        return this._saltData.clone();
    }
    
    public void setSaltData(final byte[] saltData) {
        this._saltData = saltData.clone();
    }
    
    public byte[] getSaltHash() {
        return this._saltHash.clone();
    }
    
    public void setSaltHash(final byte[] saltHash) {
        this._saltHash = saltHash.clone();
    }
    
    @Override
    public short getSid() {
        return 47;
    }
    
    @Override
    public Object clone() {
        return this;
    }
    
    @Override
    public String toString() {
        final StringBuffer buffer = new StringBuffer();
        buffer.append("[FILEPASS]\n");
        buffer.append("    .type = ").append(HexDump.shortToHex(this._encryptionType)).append("\n");
        buffer.append("    .info = ").append(HexDump.shortToHex(this._encryptionInfo)).append("\n");
        buffer.append("    .ver  = ").append(HexDump.shortToHex(this._minorVersionNo)).append("\n");
        buffer.append("    .docId= ").append(HexDump.toHex(this._docId)).append("\n");
        buffer.append("    .salt = ").append(HexDump.toHex(this._saltData)).append("\n");
        buffer.append("    .hash = ").append(HexDump.toHex(this._saltHash)).append("\n");
        buffer.append("[/FILEPASS]\n");
        return buffer.toString();
    }
}
