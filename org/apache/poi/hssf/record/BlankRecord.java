// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hssf.record;

import org.apache.poi.util.LittleEndianOutput;
import org.apache.poi.util.HexDump;

public final class BlankRecord extends StandardRecord implements CellValueRecordInterface
{
    public static final short sid = 513;
    private int field_1_row;
    private short field_2_col;
    private short field_3_xf;
    
    public BlankRecord() {
    }
    
    public BlankRecord(final RecordInputStream in) {
        this.field_1_row = in.readUShort();
        this.field_2_col = in.readShort();
        this.field_3_xf = in.readShort();
    }
    
    public void setRow(final int row) {
        this.field_1_row = row;
    }
    
    public int getRow() {
        return this.field_1_row;
    }
    
    public short getColumn() {
        return this.field_2_col;
    }
    
    public void setXFIndex(final short xf) {
        this.field_3_xf = xf;
    }
    
    public short getXFIndex() {
        return this.field_3_xf;
    }
    
    public void setColumn(final short col) {
        this.field_2_col = col;
    }
    
    @Override
    public short getSid() {
        return 513;
    }
    
    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer();
        sb.append("[BLANK]\n");
        sb.append("    row= ").append(HexDump.shortToHex(this.getRow())).append("\n");
        sb.append("    col= ").append(HexDump.shortToHex(this.getColumn())).append("\n");
        sb.append("    xf = ").append(HexDump.shortToHex(this.getXFIndex())).append("\n");
        sb.append("[/BLANK]\n");
        return sb.toString();
    }
    
    public void serialize(final LittleEndianOutput out) {
        out.writeShort(this.getRow());
        out.writeShort(this.getColumn());
        out.writeShort(this.getXFIndex());
    }
    
    @Override
    protected int getDataSize() {
        return 6;
    }
    
    @Override
    public Object clone() {
        final BlankRecord rec = new BlankRecord();
        rec.field_1_row = this.field_1_row;
        rec.field_2_col = this.field_2_col;
        rec.field_3_xf = this.field_3_xf;
        return rec;
    }
}
