// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hssf.record;

import org.apache.poi.util.LittleEndianOutput;

public final class SaveRecalcRecord extends StandardRecord
{
    public static final short sid = 95;
    private short field_1_recalc;
    
    public SaveRecalcRecord() {
    }
    
    public SaveRecalcRecord(final RecordInputStream in) {
        this.field_1_recalc = in.readShort();
    }
    
    public void setRecalc(final boolean recalc) {
        this.field_1_recalc = (short)(recalc ? 1 : 0);
    }
    
    public boolean getRecalc() {
        return this.field_1_recalc == 1;
    }
    
    @Override
    public String toString() {
        final StringBuffer buffer = new StringBuffer();
        buffer.append("[SAVERECALC]\n");
        buffer.append("    .recalc         = ").append(this.getRecalc()).append("\n");
        buffer.append("[/SAVERECALC]\n");
        return buffer.toString();
    }
    
    public void serialize(final LittleEndianOutput out) {
        out.writeShort(this.field_1_recalc);
    }
    
    @Override
    protected int getDataSize() {
        return 2;
    }
    
    @Override
    public short getSid() {
        return 95;
    }
    
    @Override
    public Object clone() {
        final SaveRecalcRecord rec = new SaveRecalcRecord();
        rec.field_1_recalc = this.field_1_recalc;
        return rec;
    }
}
