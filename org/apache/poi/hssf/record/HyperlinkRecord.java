// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hssf.record;

import org.apache.poi.util.LittleEndianByteArrayInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.DataOutputStream;
import java.io.ByteArrayOutputStream;
import org.apache.poi.util.HexRead;
import org.apache.poi.util.LittleEndianOutput;
import org.apache.poi.util.HexDump;
import org.apache.poi.util.StringUtil;
import org.apache.poi.util.LittleEndianInput;
import org.apache.poi.util.POILogFactory;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.util.POILogger;

public final class HyperlinkRecord extends StandardRecord
{
    public static final short sid = 440;
    private POILogger logger;
    static final int HLINK_URL = 1;
    static final int HLINK_ABS = 2;
    static final int HLINK_LABEL = 20;
    static final int HLINK_PLACE = 8;
    private static final int HLINK_TARGET_FRAME = 128;
    private static final int HLINK_UNC_PATH = 256;
    static final GUID STD_MONIKER;
    static final GUID URL_MONIKER;
    static final GUID FILE_MONIKER;
    private static final byte[] URL_TAIL;
    private static final byte[] FILE_TAIL;
    private static final int TAIL_SIZE;
    private CellRangeAddress _range;
    private GUID _guid;
    private int _fileOpts;
    private int _linkOpts;
    private String _label;
    private String _targetFrame;
    private GUID _moniker;
    private String _shortFilename;
    private String _address;
    private String _textMark;
    private byte[] _uninterpretedTail;
    
    public HyperlinkRecord() {
        this.logger = POILogFactory.getLogger(this.getClass());
    }
    
    public int getFirstColumn() {
        return this._range.getFirstColumn();
    }
    
    public void setFirstColumn(final int col) {
        this._range.setFirstColumn(col);
    }
    
    public int getLastColumn() {
        return this._range.getLastColumn();
    }
    
    public void setLastColumn(final int col) {
        this._range.setLastColumn(col);
    }
    
    public int getFirstRow() {
        return this._range.getFirstRow();
    }
    
    public void setFirstRow(final int col) {
        this._range.setFirstRow(col);
    }
    
    public int getLastRow() {
        return this._range.getLastRow();
    }
    
    public void setLastRow(final int col) {
        this._range.setLastRow(col);
    }
    
    GUID getGuid() {
        return this._guid;
    }
    
    GUID getMoniker() {
        return this._moniker;
    }
    
    private static String cleanString(final String s) {
        if (s == null) {
            return null;
        }
        final int idx = s.indexOf(0);
        if (idx < 0) {
            return s;
        }
        return s.substring(0, idx);
    }
    
    private static String appendNullTerm(final String s) {
        if (s == null) {
            return null;
        }
        return s + '\0';
    }
    
    public String getLabel() {
        return cleanString(this._label);
    }
    
    public void setLabel(final String label) {
        this._label = appendNullTerm(label);
    }
    
    public String getTargetFrame() {
        return cleanString(this._targetFrame);
    }
    
    public String getAddress() {
        if ((this._linkOpts & 0x1) != 0x0 && HyperlinkRecord.FILE_MONIKER.equals(this._moniker)) {
            return cleanString((this._address != null) ? this._address : this._shortFilename);
        }
        if ((this._linkOpts & 0x8) != 0x0) {
            return cleanString(this._textMark);
        }
        return cleanString(this._address);
    }
    
    public void setAddress(final String address) {
        if ((this._linkOpts & 0x1) != 0x0 && HyperlinkRecord.FILE_MONIKER.equals(this._moniker)) {
            this._shortFilename = appendNullTerm(address);
        }
        else if ((this._linkOpts & 0x8) != 0x0) {
            this._textMark = appendNullTerm(address);
        }
        else {
            this._address = appendNullTerm(address);
        }
    }
    
    public String getShortFilename() {
        return cleanString(this._shortFilename);
    }
    
    public void setShortFilename(final String shortFilename) {
        this._shortFilename = appendNullTerm(shortFilename);
    }
    
    public String getTextMark() {
        return cleanString(this._textMark);
    }
    
    public void setTextMark(final String textMark) {
        this._textMark = appendNullTerm(textMark);
    }
    
    int getLinkOptions() {
        return this._linkOpts;
    }
    
    public int getLabelOptions() {
        return 2;
    }
    
    public int getFileOptions() {
        return this._fileOpts;
    }
    
    public HyperlinkRecord(final RecordInputStream in) {
        this.logger = POILogFactory.getLogger(this.getClass());
        this._range = new CellRangeAddress(in);
        this._guid = new GUID(in);
        final int streamVersion = in.readInt();
        if (streamVersion != 2) {
            throw new RecordFormatException("Stream Version must be 0x2 but found " + streamVersion);
        }
        this._linkOpts = in.readInt();
        if ((this._linkOpts & 0x14) != 0x0) {
            final int label_len = in.readInt();
            this._label = in.readUnicodeLEString(label_len);
        }
        if ((this._linkOpts & 0x80) != 0x0) {
            final int len = in.readInt();
            this._targetFrame = in.readUnicodeLEString(len);
        }
        if ((this._linkOpts & 0x1) != 0x0 && (this._linkOpts & 0x100) != 0x0) {
            this._moniker = null;
            final int nChars = in.readInt();
            this._address = in.readUnicodeLEString(nChars);
        }
        if ((this._linkOpts & 0x1) != 0x0 && (this._linkOpts & 0x100) == 0x0) {
            this._moniker = new GUID(in);
            if (HyperlinkRecord.URL_MONIKER.equals(this._moniker)) {
                final int length = in.readInt();
                final int remaining = in.remaining();
                if (length == remaining) {
                    final int nChars2 = length / 2;
                    this._address = in.readUnicodeLEString(nChars2);
                }
                else {
                    final int nChars2 = (length - HyperlinkRecord.TAIL_SIZE) / 2;
                    this._address = in.readUnicodeLEString(nChars2);
                    this._uninterpretedTail = readTail(HyperlinkRecord.URL_TAIL, in);
                }
            }
            else if (HyperlinkRecord.FILE_MONIKER.equals(this._moniker)) {
                this._fileOpts = in.readShort();
                final int len = in.readInt();
                this._shortFilename = StringUtil.readCompressedUnicode(in, len);
                this._uninterpretedTail = readTail(HyperlinkRecord.FILE_TAIL, in);
                final int size = in.readInt();
                if (size > 0) {
                    final int charDataSize = in.readInt();
                    final int usKeyValue = in.readUShort();
                    this._address = StringUtil.readUnicodeLE(in, charDataSize / 2);
                }
                else {
                    this._address = null;
                }
            }
            else if (HyperlinkRecord.STD_MONIKER.equals(this._moniker)) {
                this._fileOpts = in.readShort();
                final int len = in.readInt();
                final byte[] path_bytes = new byte[len];
                in.readFully(path_bytes);
                this._address = new String(path_bytes);
            }
        }
        if ((this._linkOpts & 0x8) != 0x0) {
            final int len = in.readInt();
            this._textMark = in.readUnicodeLEString(len);
        }
        if (in.remaining() > 0) {
            this.logger.log(POILogger.WARN, "Hyperlink data remains: " + in.remaining() + " : " + HexDump.toHex(in.readRemainder()));
        }
    }
    
    public void serialize(final LittleEndianOutput out) {
        this._range.serialize(out);
        this._guid.serialize(out);
        out.writeInt(2);
        out.writeInt(this._linkOpts);
        if ((this._linkOpts & 0x14) != 0x0) {
            out.writeInt(this._label.length());
            StringUtil.putUnicodeLE(this._label, out);
        }
        if ((this._linkOpts & 0x80) != 0x0) {
            out.writeInt(this._targetFrame.length());
            StringUtil.putUnicodeLE(this._targetFrame, out);
        }
        if ((this._linkOpts & 0x1) != 0x0 && (this._linkOpts & 0x100) != 0x0) {
            out.writeInt(this._address.length());
            StringUtil.putUnicodeLE(this._address, out);
        }
        if ((this._linkOpts & 0x1) != 0x0 && (this._linkOpts & 0x100) == 0x0) {
            this._moniker.serialize(out);
            if (HyperlinkRecord.URL_MONIKER.equals(this._moniker)) {
                if (this._uninterpretedTail == null) {
                    out.writeInt(this._address.length() * 2);
                    StringUtil.putUnicodeLE(this._address, out);
                }
                else {
                    out.writeInt(this._address.length() * 2 + HyperlinkRecord.TAIL_SIZE);
                    StringUtil.putUnicodeLE(this._address, out);
                    writeTail(this._uninterpretedTail, out);
                }
            }
            else if (HyperlinkRecord.FILE_MONIKER.equals(this._moniker)) {
                out.writeShort(this._fileOpts);
                out.writeInt(this._shortFilename.length());
                StringUtil.putCompressedUnicode(this._shortFilename, out);
                writeTail(this._uninterpretedTail, out);
                if (this._address == null) {
                    out.writeInt(0);
                }
                else {
                    final int addrLen = this._address.length() * 2;
                    out.writeInt(addrLen + 6);
                    out.writeInt(addrLen);
                    out.writeShort(3);
                    StringUtil.putUnicodeLE(this._address, out);
                }
            }
        }
        if ((this._linkOpts & 0x8) != 0x0) {
            out.writeInt(this._textMark.length());
            StringUtil.putUnicodeLE(this._textMark, out);
        }
    }
    
    @Override
    protected int getDataSize() {
        int size = 0;
        size += 8;
        size += 16;
        size += 4;
        size += 4;
        if ((this._linkOpts & 0x14) != 0x0) {
            size += 4;
            size += this._label.length() * 2;
        }
        if ((this._linkOpts & 0x80) != 0x0) {
            size += 4;
            size += this._targetFrame.length() * 2;
        }
        if ((this._linkOpts & 0x1) != 0x0 && (this._linkOpts & 0x100) != 0x0) {
            size += 4;
            size += this._address.length() * 2;
        }
        if ((this._linkOpts & 0x1) != 0x0 && (this._linkOpts & 0x100) == 0x0) {
            size += 16;
            if (HyperlinkRecord.URL_MONIKER.equals(this._moniker)) {
                size += 4;
                size += this._address.length() * 2;
                if (this._uninterpretedTail != null) {
                    size += HyperlinkRecord.TAIL_SIZE;
                }
            }
            else if (HyperlinkRecord.FILE_MONIKER.equals(this._moniker)) {
                size += 2;
                size += 4;
                size += this._shortFilename.length();
                size += HyperlinkRecord.TAIL_SIZE;
                size += 4;
                if (this._address != null) {
                    size += 6;
                    size += this._address.length() * 2;
                }
            }
        }
        if ((this._linkOpts & 0x8) != 0x0) {
            size += 4;
            size += this._textMark.length() * 2;
        }
        return size;
    }
    
    private static byte[] readTail(final byte[] expectedTail, final LittleEndianInput in) {
        final byte[] result = new byte[HyperlinkRecord.TAIL_SIZE];
        in.readFully(result);
        return result;
    }
    
    private static void writeTail(final byte[] tail, final LittleEndianOutput out) {
        out.write(tail);
    }
    
    @Override
    public short getSid() {
        return 440;
    }
    
    @Override
    public String toString() {
        final StringBuffer buffer = new StringBuffer();
        buffer.append("[HYPERLINK RECORD]\n");
        buffer.append("    .range   = ").append(this._range.formatAsString()).append("\n");
        buffer.append("    .guid    = ").append(this._guid.formatAsString()).append("\n");
        buffer.append("    .linkOpts= ").append(HexDump.intToHex(this._linkOpts)).append("\n");
        buffer.append("    .label   = ").append(this.getLabel()).append("\n");
        if ((this._linkOpts & 0x80) != 0x0) {
            buffer.append("    .targetFrame= ").append(this.getTargetFrame()).append("\n");
        }
        if ((this._linkOpts & 0x1) != 0x0 && this._moniker != null) {
            buffer.append("    .moniker   = ").append(this._moniker.formatAsString()).append("\n");
        }
        if ((this._linkOpts & 0x8) != 0x0) {
            buffer.append("    .textMark= ").append(this.getTextMark()).append("\n");
        }
        buffer.append("    .address   = ").append(this.getAddress()).append("\n");
        buffer.append("[/HYPERLINK RECORD]\n");
        return buffer.toString();
    }
    
    public boolean isUrlLink() {
        return (this._linkOpts & 0x1) > 0 && (this._linkOpts & 0x2) > 0;
    }
    
    public boolean isFileLink() {
        return (this._linkOpts & 0x1) > 0 && (this._linkOpts & 0x2) == 0x0;
    }
    
    public boolean isDocumentLink() {
        return (this._linkOpts & 0x8) > 0;
    }
    
    public void newUrlLink() {
        this._range = new CellRangeAddress(0, 0, 0, 0);
        this._guid = HyperlinkRecord.STD_MONIKER;
        this._linkOpts = 23;
        this.setLabel("");
        this._moniker = HyperlinkRecord.URL_MONIKER;
        this.setAddress("");
        this._uninterpretedTail = HyperlinkRecord.URL_TAIL;
    }
    
    public void newFileLink() {
        this._range = new CellRangeAddress(0, 0, 0, 0);
        this._guid = HyperlinkRecord.STD_MONIKER;
        this._linkOpts = 21;
        this._fileOpts = 0;
        this.setLabel("");
        this._moniker = HyperlinkRecord.FILE_MONIKER;
        this.setAddress(null);
        this.setShortFilename("");
        this._uninterpretedTail = HyperlinkRecord.FILE_TAIL;
    }
    
    public void newDocumentLink() {
        this._range = new CellRangeAddress(0, 0, 0, 0);
        this._guid = HyperlinkRecord.STD_MONIKER;
        this._linkOpts = 28;
        this.setLabel("");
        this._moniker = HyperlinkRecord.FILE_MONIKER;
        this.setAddress("");
        this.setTextMark("");
    }
    
    @Override
    public Object clone() {
        final HyperlinkRecord rec = new HyperlinkRecord();
        rec._range = this._range.copy();
        rec._guid = this._guid;
        rec._linkOpts = this._linkOpts;
        rec._fileOpts = this._fileOpts;
        rec._label = this._label;
        rec._address = this._address;
        rec._moniker = this._moniker;
        rec._shortFilename = this._shortFilename;
        rec._targetFrame = this._targetFrame;
        rec._textMark = this._textMark;
        rec._uninterpretedTail = this._uninterpretedTail;
        return rec;
    }
    
    static {
        STD_MONIKER = GUID.parse("79EAC9D0-BAF9-11CE-8C82-00AA004BA90B");
        URL_MONIKER = GUID.parse("79EAC9E0-BAF9-11CE-8C82-00AA004BA90B");
        FILE_MONIKER = GUID.parse("00000303-0000-0000-C000-000000000046");
        URL_TAIL = HexRead.readFromString("79 58 81 F4  3B 1D 7F 48   AF 2C 82 5D  C4 85 27 63   00 00 00 00  A5 AB 00 00");
        FILE_TAIL = HexRead.readFromString("FF FF AD DE  00 00 00 00   00 00 00 00  00 00 00 00   00 00 00 00  00 00 00 00");
        TAIL_SIZE = HyperlinkRecord.FILE_TAIL.length;
    }
    
    static final class GUID
    {
        private static final int TEXT_FORMAT_LENGTH = 36;
        public static final int ENCODED_SIZE = 16;
        private final int _d1;
        private final int _d2;
        private final int _d3;
        private final long _d4;
        
        public GUID(final LittleEndianInput in) {
            this(in.readInt(), in.readUShort(), in.readUShort(), in.readLong());
        }
        
        public GUID(final int d1, final int d2, final int d3, final long d4) {
            this._d1 = d1;
            this._d2 = d2;
            this._d3 = d3;
            this._d4 = d4;
        }
        
        public void serialize(final LittleEndianOutput out) {
            out.writeInt(this._d1);
            out.writeShort(this._d2);
            out.writeShort(this._d3);
            out.writeLong(this._d4);
        }
        
        @Override
        public boolean equals(final Object obj) {
            final GUID other = (GUID)obj;
            return obj != null && obj instanceof GUID && this._d1 == other._d1 && this._d2 == other._d2 && this._d3 == other._d3 && this._d4 == other._d4;
        }
        
        public int getD1() {
            return this._d1;
        }
        
        public int getD2() {
            return this._d2;
        }
        
        public int getD3() {
            return this._d3;
        }
        
        public long getD4() {
            final ByteArrayOutputStream baos = new ByteArrayOutputStream(8);
            try {
                new DataOutputStream(baos).writeLong(this._d4);
            }
            catch (IOException e) {
                throw new RuntimeException(e);
            }
            final byte[] buf = baos.toByteArray();
            return new LittleEndianByteArrayInputStream(buf).readLong();
        }
        
        public String formatAsString() {
            final StringBuilder sb = new StringBuilder(36);
            final int PREFIX_LEN = "0x".length();
            sb.append(HexDump.intToHex(this._d1), PREFIX_LEN, 8);
            sb.append("-");
            sb.append(HexDump.shortToHex(this._d2), PREFIX_LEN, 4);
            sb.append("-");
            sb.append(HexDump.shortToHex(this._d3), PREFIX_LEN, 4);
            sb.append("-");
            final char[] d4Chars = HexDump.longToHex(this.getD4());
            sb.append(d4Chars, PREFIX_LEN, 4);
            sb.append("-");
            sb.append(d4Chars, PREFIX_LEN + 4, 12);
            return sb.toString();
        }
        
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder(64);
            sb.append(this.getClass().getName()).append(" [");
            sb.append(this.formatAsString());
            sb.append("]");
            return sb.toString();
        }
        
        public static GUID parse(final String rep) {
            final char[] cc = rep.toCharArray();
            if (cc.length != 36) {
                throw new RecordFormatException("supplied text is the wrong length for a GUID");
            }
            final int d0 = (parseShort(cc, 0) << 16) + (parseShort(cc, 4) << 0);
            final int d2 = parseShort(cc, 9);
            final int d3 = parseShort(cc, 14);
            for (int i = 23; i > 19; --i) {
                cc[i] = cc[i - 1];
            }
            final long d4 = parseLELong(cc, 20);
            return new GUID(d0, d2, d3, d4);
        }
        
        private static long parseLELong(final char[] cc, final int startIndex) {
            long acc = 0L;
            for (int i = startIndex + 14; i >= startIndex; i -= 2) {
                acc <<= 4;
                acc += parseHexChar(cc[i + 0]);
                acc <<= 4;
                acc += parseHexChar(cc[i + 1]);
            }
            return acc;
        }
        
        private static int parseShort(final char[] cc, final int startIndex) {
            int acc = 0;
            for (int i = 0; i < 4; ++i) {
                acc <<= 4;
                acc += parseHexChar(cc[startIndex + i]);
            }
            return acc;
        }
        
        private static int parseHexChar(final char c) {
            if (c >= '0' && c <= '9') {
                return c - '0';
            }
            if (c >= 'A' && c <= 'F') {
                return c - 'A' + 10;
            }
            if (c >= 'a' && c <= 'f') {
                return c - 'a' + 10;
            }
            throw new RecordFormatException("Bad hex char '" + c + "'");
        }
    }
}
