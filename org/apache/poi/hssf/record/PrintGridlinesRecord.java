// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hssf.record;

import org.apache.poi.util.LittleEndianOutput;

public final class PrintGridlinesRecord extends StandardRecord
{
    public static final short sid = 43;
    private short field_1_print_gridlines;
    
    public PrintGridlinesRecord() {
    }
    
    public PrintGridlinesRecord(final RecordInputStream in) {
        this.field_1_print_gridlines = in.readShort();
    }
    
    public void setPrintGridlines(final boolean pg) {
        if (pg) {
            this.field_1_print_gridlines = 1;
        }
        else {
            this.field_1_print_gridlines = 0;
        }
    }
    
    public boolean getPrintGridlines() {
        return this.field_1_print_gridlines == 1;
    }
    
    @Override
    public String toString() {
        final StringBuffer buffer = new StringBuffer();
        buffer.append("[PRINTGRIDLINES]\n");
        buffer.append("    .printgridlines = ").append(this.getPrintGridlines()).append("\n");
        buffer.append("[/PRINTGRIDLINES]\n");
        return buffer.toString();
    }
    
    public void serialize(final LittleEndianOutput out) {
        out.writeShort(this.field_1_print_gridlines);
    }
    
    @Override
    protected int getDataSize() {
        return 2;
    }
    
    @Override
    public short getSid() {
        return 43;
    }
    
    @Override
    public Object clone() {
        final PrintGridlinesRecord rec = new PrintGridlinesRecord();
        rec.field_1_print_gridlines = this.field_1_print_gridlines;
        return rec;
    }
}
