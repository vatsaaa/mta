// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hssf.record;

import org.apache.poi.util.LittleEndianOutput;

public final class CalcCountRecord extends StandardRecord
{
    public static final short sid = 12;
    private short field_1_iterations;
    
    public CalcCountRecord() {
    }
    
    public CalcCountRecord(final RecordInputStream in) {
        this.field_1_iterations = in.readShort();
    }
    
    public void setIterations(final short iterations) {
        this.field_1_iterations = iterations;
    }
    
    public short getIterations() {
        return this.field_1_iterations;
    }
    
    @Override
    public String toString() {
        final StringBuffer buffer = new StringBuffer();
        buffer.append("[CALCCOUNT]\n");
        buffer.append("    .iterations     = ").append(Integer.toHexString(this.getIterations())).append("\n");
        buffer.append("[/CALCCOUNT]\n");
        return buffer.toString();
    }
    
    public void serialize(final LittleEndianOutput out) {
        out.writeShort(this.getIterations());
    }
    
    @Override
    protected int getDataSize() {
        return 2;
    }
    
    @Override
    public short getSid() {
        return 12;
    }
    
    @Override
    public Object clone() {
        final CalcCountRecord rec = new CalcCountRecord();
        rec.field_1_iterations = this.field_1_iterations;
        return rec;
    }
}
