// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hssf.record;

import org.apache.poi.util.LittleEndianOutput;

public final class GutsRecord extends StandardRecord
{
    public static final short sid = 128;
    private short field_1_left_row_gutter;
    private short field_2_top_col_gutter;
    private short field_3_row_level_max;
    private short field_4_col_level_max;
    
    public GutsRecord() {
    }
    
    public GutsRecord(final RecordInputStream in) {
        this.field_1_left_row_gutter = in.readShort();
        this.field_2_top_col_gutter = in.readShort();
        this.field_3_row_level_max = in.readShort();
        this.field_4_col_level_max = in.readShort();
    }
    
    public void setLeftRowGutter(final short gut) {
        this.field_1_left_row_gutter = gut;
    }
    
    public void setTopColGutter(final short gut) {
        this.field_2_top_col_gutter = gut;
    }
    
    public void setRowLevelMax(final short max) {
        this.field_3_row_level_max = max;
    }
    
    public void setColLevelMax(final short max) {
        this.field_4_col_level_max = max;
    }
    
    public short getLeftRowGutter() {
        return this.field_1_left_row_gutter;
    }
    
    public short getTopColGutter() {
        return this.field_2_top_col_gutter;
    }
    
    public short getRowLevelMax() {
        return this.field_3_row_level_max;
    }
    
    public short getColLevelMax() {
        return this.field_4_col_level_max;
    }
    
    @Override
    public String toString() {
        final StringBuffer buffer = new StringBuffer();
        buffer.append("[GUTS]\n");
        buffer.append("    .leftgutter     = ").append(Integer.toHexString(this.getLeftRowGutter())).append("\n");
        buffer.append("    .topgutter      = ").append(Integer.toHexString(this.getTopColGutter())).append("\n");
        buffer.append("    .rowlevelmax    = ").append(Integer.toHexString(this.getRowLevelMax())).append("\n");
        buffer.append("    .collevelmax    = ").append(Integer.toHexString(this.getColLevelMax())).append("\n");
        buffer.append("[/GUTS]\n");
        return buffer.toString();
    }
    
    public void serialize(final LittleEndianOutput out) {
        out.writeShort(this.getLeftRowGutter());
        out.writeShort(this.getTopColGutter());
        out.writeShort(this.getRowLevelMax());
        out.writeShort(this.getColLevelMax());
    }
    
    @Override
    protected int getDataSize() {
        return 8;
    }
    
    @Override
    public short getSid() {
        return 128;
    }
    
    @Override
    public Object clone() {
        final GutsRecord rec = new GutsRecord();
        rec.field_1_left_row_gutter = this.field_1_left_row_gutter;
        rec.field_2_top_col_gutter = this.field_2_top_col_gutter;
        rec.field_3_row_level_max = this.field_3_row_level_max;
        rec.field_4_col_level_max = this.field_4_col_level_max;
        return rec;
    }
}
