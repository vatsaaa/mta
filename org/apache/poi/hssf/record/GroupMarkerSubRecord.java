// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hssf.record;

import org.apache.poi.util.LittleEndianOutput;
import org.apache.poi.util.HexDump;
import org.apache.poi.util.LittleEndianInput;

public final class GroupMarkerSubRecord extends SubRecord
{
    public static final short sid = 6;
    private static final byte[] EMPTY_BYTE_ARRAY;
    private byte[] reserved;
    
    public GroupMarkerSubRecord() {
        this.reserved = GroupMarkerSubRecord.EMPTY_BYTE_ARRAY;
    }
    
    public GroupMarkerSubRecord(final LittleEndianInput in, final int size) {
        final byte[] buf = new byte[size];
        in.readFully(buf);
        this.reserved = buf;
    }
    
    @Override
    public String toString() {
        final StringBuffer buffer = new StringBuffer();
        final String nl = System.getProperty("line.separator");
        buffer.append("[ftGmo]" + nl);
        buffer.append("  reserved = ").append(HexDump.toHex(this.reserved)).append(nl);
        buffer.append("[/ftGmo]" + nl);
        return buffer.toString();
    }
    
    @Override
    public void serialize(final LittleEndianOutput out) {
        out.writeShort(6);
        out.writeShort(this.reserved.length);
        out.write(this.reserved);
    }
    
    @Override
    protected int getDataSize() {
        return this.reserved.length;
    }
    
    public short getSid() {
        return 6;
    }
    
    @Override
    public Object clone() {
        final GroupMarkerSubRecord rec = new GroupMarkerSubRecord();
        rec.reserved = new byte[this.reserved.length];
        for (int i = 0; i < this.reserved.length; ++i) {
            rec.reserved[i] = this.reserved[i];
        }
        return rec;
    }
    
    static {
        EMPTY_BYTE_ARRAY = new byte[0];
    }
}
