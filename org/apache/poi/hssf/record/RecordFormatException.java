// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hssf.record;

public class RecordFormatException extends org.apache.poi.util.RecordFormatException
{
    public RecordFormatException(final String exception) {
        super(exception);
    }
    
    public RecordFormatException(final String exception, final Throwable thr) {
        super(exception, thr);
    }
    
    public RecordFormatException(final Throwable thr) {
        super(thr);
    }
}
