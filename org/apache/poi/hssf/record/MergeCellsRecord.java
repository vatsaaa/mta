// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hssf.record;

import org.apache.poi.util.LittleEndianOutput;
import org.apache.poi.ss.util.CellRangeAddressList;
import org.apache.poi.ss.util.CellRangeAddress;

public final class MergeCellsRecord extends StandardRecord
{
    public static final short sid = 229;
    private CellRangeAddress[] _regions;
    private final int _startIndex;
    private final int _numberOfRegions;
    
    public MergeCellsRecord(final CellRangeAddress[] regions, final int startIndex, final int numberOfRegions) {
        this._regions = regions;
        this._startIndex = startIndex;
        this._numberOfRegions = numberOfRegions;
    }
    
    public MergeCellsRecord(final RecordInputStream in) {
        final int nRegions = in.readUShort();
        final CellRangeAddress[] cras = new CellRangeAddress[nRegions];
        for (int i = 0; i < nRegions; ++i) {
            cras[i] = new CellRangeAddress(in);
        }
        this._numberOfRegions = nRegions;
        this._startIndex = 0;
        this._regions = cras;
    }
    
    public short getNumAreas() {
        return (short)this._numberOfRegions;
    }
    
    public CellRangeAddress getAreaAt(final int index) {
        return this._regions[this._startIndex + index];
    }
    
    @Override
    protected int getDataSize() {
        return CellRangeAddressList.getEncodedSize(this._numberOfRegions);
    }
    
    @Override
    public short getSid() {
        return 229;
    }
    
    public void serialize(final LittleEndianOutput out) {
        final int nItems = this._numberOfRegions;
        out.writeShort(nItems);
        for (int i = 0; i < this._numberOfRegions; ++i) {
            this._regions[this._startIndex + i].serialize(out);
        }
    }
    
    @Override
    public String toString() {
        final StringBuffer retval = new StringBuffer();
        retval.append("[MERGEDCELLS]").append("\n");
        retval.append("     .numregions =").append(this.getNumAreas()).append("\n");
        for (int k = 0; k < this._numberOfRegions; ++k) {
            final CellRangeAddress r = this._regions[this._startIndex + k];
            retval.append("     .rowfrom =").append(r.getFirstRow()).append("\n");
            retval.append("     .rowto   =").append(r.getLastRow()).append("\n");
            retval.append("     .colfrom =").append(r.getFirstColumn()).append("\n");
            retval.append("     .colto   =").append(r.getLastColumn()).append("\n");
        }
        retval.append("[MERGEDCELLS]").append("\n");
        return retval.toString();
    }
    
    @Override
    public Object clone() {
        final int nRegions = this._numberOfRegions;
        final CellRangeAddress[] clonedRegions = new CellRangeAddress[nRegions];
        for (int i = 0; i < clonedRegions.length; ++i) {
            clonedRegions[i] = this._regions[this._startIndex + i].copy();
        }
        return new MergeCellsRecord(clonedRegions, 0, nRegions);
    }
}
