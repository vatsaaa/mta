// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hssf.record;

import org.apache.poi.util.HexDump;
import org.apache.poi.util.LittleEndianOutput;

public final class ContinueRecord extends StandardRecord
{
    public static final short sid = 60;
    private byte[] _data;
    
    public ContinueRecord(final byte[] data) {
        this._data = data;
    }
    
    @Override
    protected int getDataSize() {
        return this._data.length;
    }
    
    public void serialize(final LittleEndianOutput out) {
        out.write(this._data);
    }
    
    public byte[] getData() {
        return this._data;
    }
    
    @Override
    public String toString() {
        final StringBuffer buffer = new StringBuffer();
        buffer.append("[CONTINUE RECORD]\n");
        buffer.append("    .data = ").append(HexDump.toHex(this._data)).append("\n");
        buffer.append("[/CONTINUE RECORD]\n");
        return buffer.toString();
    }
    
    @Override
    public short getSid() {
        return 60;
    }
    
    public ContinueRecord(final RecordInputStream in) {
        this._data = in.readRemainder();
    }
    
    @Override
    public Object clone() {
        return new ContinueRecord(this._data);
    }
}
