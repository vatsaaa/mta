// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hssf.record;

import org.apache.poi.util.LittleEndianOutput;
import org.apache.poi.util.HexDump;
import org.apache.poi.util.StringUtil;

public final class FormatRecord extends StandardRecord
{
    public static final short sid = 1054;
    private final int field_1_index_code;
    private final boolean field_3_hasMultibyte;
    private final String field_4_formatstring;
    
    public FormatRecord(final int indexCode, final String fs) {
        this.field_1_index_code = indexCode;
        this.field_4_formatstring = fs;
        this.field_3_hasMultibyte = StringUtil.hasMultibyte(fs);
    }
    
    public FormatRecord(final RecordInputStream in) {
        this.field_1_index_code = in.readShort();
        final int field_3_unicode_len = in.readUShort();
        this.field_3_hasMultibyte = ((in.readByte() & 0x1) != 0x0);
        if (this.field_3_hasMultibyte) {
            this.field_4_formatstring = in.readUnicodeLEString(field_3_unicode_len);
        }
        else {
            this.field_4_formatstring = in.readCompressedUnicode(field_3_unicode_len);
        }
    }
    
    public int getIndexCode() {
        return this.field_1_index_code;
    }
    
    public String getFormatString() {
        return this.field_4_formatstring;
    }
    
    @Override
    public String toString() {
        final StringBuffer buffer = new StringBuffer();
        buffer.append("[FORMAT]\n");
        buffer.append("    .indexcode       = ").append(HexDump.shortToHex(this.getIndexCode())).append("\n");
        buffer.append("    .isUnicode       = ").append(this.field_3_hasMultibyte).append("\n");
        buffer.append("    .formatstring    = ").append(this.getFormatString()).append("\n");
        buffer.append("[/FORMAT]\n");
        return buffer.toString();
    }
    
    public void serialize(final LittleEndianOutput out) {
        final String formatString = this.getFormatString();
        out.writeShort(this.getIndexCode());
        out.writeShort(formatString.length());
        out.writeByte(this.field_3_hasMultibyte ? 1 : 0);
        if (this.field_3_hasMultibyte) {
            StringUtil.putUnicodeLE(formatString, out);
        }
        else {
            StringUtil.putCompressedUnicode(formatString, out);
        }
    }
    
    @Override
    protected int getDataSize() {
        return 5 + this.getFormatString().length() * (this.field_3_hasMultibyte ? 2 : 1);
    }
    
    @Override
    public short getSid() {
        return 1054;
    }
    
    @Override
    public Object clone() {
        return this;
    }
}
