// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hssf.record;

public final class UnicodeString extends org.apache.poi.hssf.record.common.UnicodeString
{
    @Deprecated
    public UnicodeString(final RecordInputStream in) {
        super(in);
    }
    
    @Deprecated
    public UnicodeString(final String str) {
        super(str);
    }
}
