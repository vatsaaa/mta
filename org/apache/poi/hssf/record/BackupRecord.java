// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hssf.record;

import org.apache.poi.util.LittleEndianOutput;

public final class BackupRecord extends StandardRecord
{
    public static final short sid = 64;
    private short field_1_backup;
    
    public BackupRecord() {
    }
    
    public BackupRecord(final RecordInputStream in) {
        this.field_1_backup = in.readShort();
    }
    
    public void setBackup(final short backup) {
        this.field_1_backup = backup;
    }
    
    public short getBackup() {
        return this.field_1_backup;
    }
    
    @Override
    public String toString() {
        final StringBuffer buffer = new StringBuffer();
        buffer.append("[BACKUP]\n");
        buffer.append("    .backup          = ").append(Integer.toHexString(this.getBackup())).append("\n");
        buffer.append("[/BACKUP]\n");
        return buffer.toString();
    }
    
    public void serialize(final LittleEndianOutput out) {
        out.writeShort(this.getBackup());
    }
    
    @Override
    protected int getDataSize() {
        return 2;
    }
    
    @Override
    public short getSid() {
        return 64;
    }
}
