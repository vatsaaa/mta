// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hssf.record;

import org.apache.poi.util.LittleEndianOutput;

public final class HCenterRecord extends StandardRecord
{
    public static final short sid = 131;
    private short field_1_hcenter;
    
    public HCenterRecord() {
    }
    
    public HCenterRecord(final RecordInputStream in) {
        this.field_1_hcenter = in.readShort();
    }
    
    public void setHCenter(final boolean hc) {
        if (hc) {
            this.field_1_hcenter = 1;
        }
        else {
            this.field_1_hcenter = 0;
        }
    }
    
    public boolean getHCenter() {
        return this.field_1_hcenter == 1;
    }
    
    @Override
    public String toString() {
        final StringBuffer buffer = new StringBuffer();
        buffer.append("[HCENTER]\n");
        buffer.append("    .hcenter        = ").append(this.getHCenter()).append("\n");
        buffer.append("[/HCENTER]\n");
        return buffer.toString();
    }
    
    public void serialize(final LittleEndianOutput out) {
        out.writeShort(this.field_1_hcenter);
    }
    
    @Override
    protected int getDataSize() {
        return 2;
    }
    
    @Override
    public short getSid() {
        return 131;
    }
    
    @Override
    public Object clone() {
        final HCenterRecord rec = new HCenterRecord();
        rec.field_1_hcenter = this.field_1_hcenter;
        return rec;
    }
}
