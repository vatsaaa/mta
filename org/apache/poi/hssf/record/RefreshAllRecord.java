// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hssf.record;

import org.apache.poi.util.BitFieldFactory;
import org.apache.poi.util.LittleEndianOutput;
import org.apache.poi.util.HexDump;
import org.apache.poi.util.BitField;

public final class RefreshAllRecord extends StandardRecord
{
    public static final short sid = 439;
    private static final BitField refreshFlag;
    private int _options;
    
    private RefreshAllRecord(final int options) {
        this._options = options;
    }
    
    public RefreshAllRecord(final RecordInputStream in) {
        this(in.readUShort());
    }
    
    public RefreshAllRecord(final boolean refreshAll) {
        this(0);
        this.setRefreshAll(refreshAll);
    }
    
    public void setRefreshAll(final boolean refreshAll) {
        this._options = RefreshAllRecord.refreshFlag.setBoolean(this._options, refreshAll);
    }
    
    public boolean getRefreshAll() {
        return RefreshAllRecord.refreshFlag.isSet(this._options);
    }
    
    @Override
    public String toString() {
        final StringBuffer buffer = new StringBuffer();
        buffer.append("[REFRESHALL]\n");
        buffer.append("    .options      = ").append(HexDump.shortToHex(this._options)).append("\n");
        buffer.append("[/REFRESHALL]\n");
        return buffer.toString();
    }
    
    public void serialize(final LittleEndianOutput out) {
        out.writeShort(this._options);
    }
    
    @Override
    protected int getDataSize() {
        return 2;
    }
    
    @Override
    public short getSid() {
        return 439;
    }
    
    @Override
    public Object clone() {
        return new RefreshAllRecord(this._options);
    }
    
    static {
        refreshFlag = BitFieldFactory.getInstance(1);
    }
}
