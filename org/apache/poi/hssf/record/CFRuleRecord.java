// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hssf.record;

import org.apache.poi.hssf.model.HSSFFormulaParser;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.util.LittleEndianOutput;
import org.apache.poi.util.LittleEndianInput;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.ss.formula.ptg.Ptg;
import org.apache.poi.util.BitFieldFactory;
import org.apache.poi.ss.formula.Formula;
import org.apache.poi.hssf.record.cf.PatternFormatting;
import org.apache.poi.hssf.record.cf.BorderFormatting;
import org.apache.poi.hssf.record.cf.FontFormatting;
import org.apache.poi.util.BitField;

public final class CFRuleRecord extends StandardRecord
{
    public static final short sid = 433;
    private byte field_1_condition_type;
    public static final byte CONDITION_TYPE_CELL_VALUE_IS = 1;
    public static final byte CONDITION_TYPE_FORMULA = 2;
    private byte field_2_comparison_operator;
    private int field_5_options;
    private static final BitField modificationBits;
    private static final BitField alignHor;
    private static final BitField alignVer;
    private static final BitField alignWrap;
    private static final BitField alignRot;
    private static final BitField alignJustLast;
    private static final BitField alignIndent;
    private static final BitField alignShrin;
    private static final BitField notUsed1;
    private static final BitField protLocked;
    private static final BitField protHidden;
    private static final BitField bordLeft;
    private static final BitField bordRight;
    private static final BitField bordTop;
    private static final BitField bordBot;
    private static final BitField bordTlBr;
    private static final BitField bordBlTr;
    private static final BitField pattStyle;
    private static final BitField pattCol;
    private static final BitField pattBgCol;
    private static final BitField notUsed2;
    private static final BitField undocumented;
    private static final BitField fmtBlockBits;
    private static final BitField font;
    private static final BitField align;
    private static final BitField bord;
    private static final BitField patt;
    private static final BitField prot;
    private static final BitField alignTextDir;
    private short field_6_not_used;
    private FontFormatting _fontFormatting;
    private BorderFormatting _borderFormatting;
    private PatternFormatting _patternFormatting;
    private Formula field_17_formula1;
    private Formula field_18_formula2;
    
    private static BitField bf(final int i) {
        return BitFieldFactory.getInstance(i);
    }
    
    private CFRuleRecord(final byte conditionType, final byte comparisonOperation) {
        this.field_1_condition_type = conditionType;
        this.field_2_comparison_operator = comparisonOperation;
        this.field_5_options = CFRuleRecord.modificationBits.setValue(this.field_5_options, -1);
        this.field_5_options = CFRuleRecord.fmtBlockBits.setValue(this.field_5_options, 0);
        this.field_5_options = CFRuleRecord.undocumented.clear(this.field_5_options);
        this.field_6_not_used = -32766;
        this._fontFormatting = null;
        this._borderFormatting = null;
        this._patternFormatting = null;
        this.field_17_formula1 = Formula.create(Ptg.EMPTY_PTG_ARRAY);
        this.field_18_formula2 = Formula.create(Ptg.EMPTY_PTG_ARRAY);
    }
    
    private CFRuleRecord(final byte conditionType, final byte comparisonOperation, final Ptg[] formula1, final Ptg[] formula2) {
        this(conditionType, comparisonOperation);
        this.field_17_formula1 = Formula.create(formula1);
        this.field_18_formula2 = Formula.create(formula2);
    }
    
    public static CFRuleRecord create(final HSSFSheet sheet, final String formulaText) {
        final Ptg[] formula1 = parseFormula(formulaText, sheet);
        return new CFRuleRecord((byte)2, (byte)0, formula1, null);
    }
    
    public static CFRuleRecord create(final HSSFSheet sheet, final byte comparisonOperation, final String formulaText1, final String formulaText2) {
        final Ptg[] formula1 = parseFormula(formulaText1, sheet);
        final Ptg[] formula2 = parseFormula(formulaText2, sheet);
        return new CFRuleRecord((byte)1, comparisonOperation, formula1, formula2);
    }
    
    public CFRuleRecord(final RecordInputStream in) {
        this.field_1_condition_type = in.readByte();
        this.field_2_comparison_operator = in.readByte();
        final int field_3_formula1_len = in.readUShort();
        final int field_4_formula2_len = in.readUShort();
        this.field_5_options = in.readInt();
        this.field_6_not_used = in.readShort();
        if (this.containsFontFormattingBlock()) {
            this._fontFormatting = new FontFormatting(in);
        }
        if (this.containsBorderFormattingBlock()) {
            this._borderFormatting = new BorderFormatting(in);
        }
        if (this.containsPatternFormattingBlock()) {
            this._patternFormatting = new PatternFormatting(in);
        }
        this.field_17_formula1 = Formula.read(field_3_formula1_len, in);
        this.field_18_formula2 = Formula.read(field_4_formula2_len, in);
    }
    
    public byte getConditionType() {
        return this.field_1_condition_type;
    }
    
    public boolean containsFontFormattingBlock() {
        return this.getOptionFlag(CFRuleRecord.font);
    }
    
    public void setFontFormatting(final FontFormatting fontFormatting) {
        this._fontFormatting = fontFormatting;
        this.setOptionFlag(fontFormatting != null, CFRuleRecord.font);
    }
    
    public FontFormatting getFontFormatting() {
        if (this.containsFontFormattingBlock()) {
            return this._fontFormatting;
        }
        return null;
    }
    
    public boolean containsAlignFormattingBlock() {
        return this.getOptionFlag(CFRuleRecord.align);
    }
    
    public void setAlignFormattingUnchanged() {
        this.setOptionFlag(false, CFRuleRecord.align);
    }
    
    public boolean containsBorderFormattingBlock() {
        return this.getOptionFlag(CFRuleRecord.bord);
    }
    
    public void setBorderFormatting(final BorderFormatting borderFormatting) {
        this._borderFormatting = borderFormatting;
        this.setOptionFlag(borderFormatting != null, CFRuleRecord.bord);
    }
    
    public BorderFormatting getBorderFormatting() {
        if (this.containsBorderFormattingBlock()) {
            return this._borderFormatting;
        }
        return null;
    }
    
    public boolean containsPatternFormattingBlock() {
        return this.getOptionFlag(CFRuleRecord.patt);
    }
    
    public void setPatternFormatting(final PatternFormatting patternFormatting) {
        this._patternFormatting = patternFormatting;
        this.setOptionFlag(patternFormatting != null, CFRuleRecord.patt);
    }
    
    public PatternFormatting getPatternFormatting() {
        if (this.containsPatternFormattingBlock()) {
            return this._patternFormatting;
        }
        return null;
    }
    
    public boolean containsProtectionFormattingBlock() {
        return this.getOptionFlag(CFRuleRecord.prot);
    }
    
    public void setProtectionFormattingUnchanged() {
        this.setOptionFlag(false, CFRuleRecord.prot);
    }
    
    public void setComparisonOperation(final byte operation) {
        this.field_2_comparison_operator = operation;
    }
    
    public byte getComparisonOperation() {
        return this.field_2_comparison_operator;
    }
    
    public int getOptions() {
        return this.field_5_options;
    }
    
    private boolean isModified(final BitField field) {
        return !field.isSet(this.field_5_options);
    }
    
    private void setModified(final boolean modified, final BitField field) {
        this.field_5_options = field.setBoolean(this.field_5_options, !modified);
    }
    
    public boolean isLeftBorderModified() {
        return this.isModified(CFRuleRecord.bordLeft);
    }
    
    public void setLeftBorderModified(final boolean modified) {
        this.setModified(modified, CFRuleRecord.bordLeft);
    }
    
    public boolean isRightBorderModified() {
        return this.isModified(CFRuleRecord.bordRight);
    }
    
    public void setRightBorderModified(final boolean modified) {
        this.setModified(modified, CFRuleRecord.bordRight);
    }
    
    public boolean isTopBorderModified() {
        return this.isModified(CFRuleRecord.bordTop);
    }
    
    public void setTopBorderModified(final boolean modified) {
        this.setModified(modified, CFRuleRecord.bordTop);
    }
    
    public boolean isBottomBorderModified() {
        return this.isModified(CFRuleRecord.bordBot);
    }
    
    public void setBottomBorderModified(final boolean modified) {
        this.setModified(modified, CFRuleRecord.bordBot);
    }
    
    public boolean isTopLeftBottomRightBorderModified() {
        return this.isModified(CFRuleRecord.bordTlBr);
    }
    
    public void setTopLeftBottomRightBorderModified(final boolean modified) {
        this.setModified(modified, CFRuleRecord.bordTlBr);
    }
    
    public boolean isBottomLeftTopRightBorderModified() {
        return this.isModified(CFRuleRecord.bordBlTr);
    }
    
    public void setBottomLeftTopRightBorderModified(final boolean modified) {
        this.setModified(modified, CFRuleRecord.bordBlTr);
    }
    
    public boolean isPatternStyleModified() {
        return this.isModified(CFRuleRecord.pattStyle);
    }
    
    public void setPatternStyleModified(final boolean modified) {
        this.setModified(modified, CFRuleRecord.pattStyle);
    }
    
    public boolean isPatternColorModified() {
        return this.isModified(CFRuleRecord.pattCol);
    }
    
    public void setPatternColorModified(final boolean modified) {
        this.setModified(modified, CFRuleRecord.pattCol);
    }
    
    public boolean isPatternBackgroundColorModified() {
        return this.isModified(CFRuleRecord.pattBgCol);
    }
    
    public void setPatternBackgroundColorModified(final boolean modified) {
        this.setModified(modified, CFRuleRecord.pattBgCol);
    }
    
    private boolean getOptionFlag(final BitField field) {
        return field.isSet(this.field_5_options);
    }
    
    private void setOptionFlag(final boolean flag, final BitField field) {
        this.field_5_options = field.setBoolean(this.field_5_options, flag);
    }
    
    public Ptg[] getParsedExpression1() {
        return this.field_17_formula1.getTokens();
    }
    
    public void setParsedExpression1(final Ptg[] ptgs) {
        this.field_17_formula1 = Formula.create(ptgs);
    }
    
    public Ptg[] getParsedExpression2() {
        return Formula.getTokens(this.field_18_formula2);
    }
    
    public void setParsedExpression2(final Ptg[] ptgs) {
        this.field_18_formula2 = Formula.create(ptgs);
    }
    
    @Override
    public short getSid() {
        return 433;
    }
    
    private static int getFormulaSize(final Formula formula) {
        return formula.getEncodedTokenSize();
    }
    
    public void serialize(final LittleEndianOutput out) {
        final int formula1Len = getFormulaSize(this.field_17_formula1);
        final int formula2Len = getFormulaSize(this.field_18_formula2);
        out.writeByte(this.field_1_condition_type);
        out.writeByte(this.field_2_comparison_operator);
        out.writeShort(formula1Len);
        out.writeShort(formula2Len);
        out.writeInt(this.field_5_options);
        out.writeShort(this.field_6_not_used);
        if (this.containsFontFormattingBlock()) {
            final byte[] fontFormattingRawRecord = this._fontFormatting.getRawRecord();
            out.write(fontFormattingRawRecord);
        }
        if (this.containsBorderFormattingBlock()) {
            this._borderFormatting.serialize(out);
        }
        if (this.containsPatternFormattingBlock()) {
            this._patternFormatting.serialize(out);
        }
        this.field_17_formula1.serializeTokens(out);
        this.field_18_formula2.serializeTokens(out);
    }
    
    @Override
    protected int getDataSize() {
        return 12 + (this.containsFontFormattingBlock() ? this._fontFormatting.getRawRecord().length : 0) + (this.containsBorderFormattingBlock() ? 8 : 0) + (this.containsPatternFormattingBlock() ? 4 : 0) + getFormulaSize(this.field_17_formula1) + getFormulaSize(this.field_18_formula2);
    }
    
    @Override
    public String toString() {
        final StringBuffer buffer = new StringBuffer();
        buffer.append("[CFRULE]\n");
        buffer.append("    .condition_type   =" + this.field_1_condition_type);
        buffer.append("    OPTION FLAGS=0x" + Integer.toHexString(this.getOptions()));
        return buffer.toString();
    }
    
    @Override
    public Object clone() {
        final CFRuleRecord rec = new CFRuleRecord(this.field_1_condition_type, this.field_2_comparison_operator);
        rec.field_5_options = this.field_5_options;
        rec.field_6_not_used = this.field_6_not_used;
        if (this.containsFontFormattingBlock()) {
            rec._fontFormatting = (FontFormatting)this._fontFormatting.clone();
        }
        if (this.containsBorderFormattingBlock()) {
            rec._borderFormatting = (BorderFormatting)this._borderFormatting.clone();
        }
        if (this.containsPatternFormattingBlock()) {
            rec._patternFormatting = (PatternFormatting)this._patternFormatting.clone();
        }
        rec.field_17_formula1 = this.field_17_formula1.copy();
        rec.field_18_formula2 = this.field_17_formula1.copy();
        return rec;
    }
    
    private static Ptg[] parseFormula(final String formula, final HSSFSheet sheet) {
        if (formula == null) {
            return null;
        }
        final int sheetIndex = sheet.getWorkbook().getSheetIndex(sheet);
        return HSSFFormulaParser.parse(formula, sheet.getWorkbook(), 0, sheetIndex);
    }
    
    static {
        modificationBits = bf(4194303);
        alignHor = bf(1);
        alignVer = bf(2);
        alignWrap = bf(4);
        alignRot = bf(8);
        alignJustLast = bf(16);
        alignIndent = bf(32);
        alignShrin = bf(64);
        notUsed1 = bf(128);
        protLocked = bf(256);
        protHidden = bf(512);
        bordLeft = bf(1024);
        bordRight = bf(2048);
        bordTop = bf(4096);
        bordBot = bf(8192);
        bordTlBr = bf(16384);
        bordBlTr = bf(32768);
        pattStyle = bf(65536);
        pattCol = bf(131072);
        pattBgCol = bf(262144);
        notUsed2 = bf(3670016);
        undocumented = bf(62914560);
        fmtBlockBits = bf(2080374784);
        font = bf(67108864);
        align = bf(134217728);
        bord = bf(268435456);
        patt = bf(536870912);
        prot = bf(1073741824);
        alignTextDir = bf(Integer.MIN_VALUE);
    }
    
    public static final class ComparisonOperator
    {
        public static final byte NO_COMPARISON = 0;
        public static final byte BETWEEN = 1;
        public static final byte NOT_BETWEEN = 2;
        public static final byte EQUAL = 3;
        public static final byte NOT_EQUAL = 4;
        public static final byte GT = 5;
        public static final byte LT = 6;
        public static final byte GE = 7;
        public static final byte LE = 8;
    }
}
