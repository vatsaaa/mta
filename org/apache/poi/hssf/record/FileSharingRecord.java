// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hssf.record;

import org.apache.poi.util.StringUtil;
import org.apache.poi.util.LittleEndianOutput;

public final class FileSharingRecord extends StandardRecord
{
    public static final short sid = 91;
    private short field_1_readonly;
    private short field_2_password;
    private byte field_3_username_unicode_options;
    private String field_3_username_value;
    
    public FileSharingRecord() {
    }
    
    public FileSharingRecord(final RecordInputStream in) {
        this.field_1_readonly = in.readShort();
        this.field_2_password = in.readShort();
        final int nameLen = in.readShort();
        if (nameLen > 0) {
            this.field_3_username_unicode_options = in.readByte();
            this.field_3_username_value = in.readCompressedUnicode(nameLen);
        }
        else {
            this.field_3_username_value = "";
        }
    }
    
    public static short hashPassword(final String password) {
        final byte[] passwordCharacters = password.getBytes();
        int hash = 0;
        if (passwordCharacters.length > 0) {
            int charIndex = passwordCharacters.length;
            while (charIndex-- > 0) {
                hash = ((hash >> 14 & 0x1) | (hash << 1 & 0x7FFF));
                hash ^= passwordCharacters[charIndex];
            }
            hash = ((hash >> 14 & 0x1) | (hash << 1 & 0x7FFF));
            hash ^= passwordCharacters.length;
            hash ^= 0xCE4B;
        }
        return (short)hash;
    }
    
    public void setReadOnly(final short readonly) {
        this.field_1_readonly = readonly;
    }
    
    public short getReadOnly() {
        return this.field_1_readonly;
    }
    
    public void setPassword(final short password) {
        this.field_2_password = password;
    }
    
    public short getPassword() {
        return this.field_2_password;
    }
    
    public String getUsername() {
        return this.field_3_username_value;
    }
    
    public void setUsername(final String username) {
        this.field_3_username_value = username;
    }
    
    @Override
    public String toString() {
        final StringBuffer buffer = new StringBuffer();
        buffer.append("[FILESHARING]\n");
        buffer.append("    .readonly       = ").append((this.getReadOnly() == 1) ? "true" : "false").append("\n");
        buffer.append("    .password       = ").append(Integer.toHexString(this.getPassword())).append("\n");
        buffer.append("    .username       = ").append(this.getUsername()).append("\n");
        buffer.append("[/FILESHARING]\n");
        return buffer.toString();
    }
    
    public void serialize(final LittleEndianOutput out) {
        out.writeShort(this.getReadOnly());
        out.writeShort(this.getPassword());
        out.writeShort(this.field_3_username_value.length());
        if (this.field_3_username_value.length() > 0) {
            out.writeByte(this.field_3_username_unicode_options);
            StringUtil.putCompressedUnicode(this.getUsername(), out);
        }
    }
    
    @Override
    protected int getDataSize() {
        final int nameLen = this.field_3_username_value.length();
        if (nameLen < 1) {
            return 6;
        }
        return 7 + nameLen;
    }
    
    @Override
    public short getSid() {
        return 91;
    }
    
    @Override
    public Object clone() {
        final FileSharingRecord clone = new FileSharingRecord();
        clone.setReadOnly(this.field_1_readonly);
        clone.setPassword(this.field_2_password);
        clone.setUsername(this.field_3_username_value);
        return clone;
    }
}
