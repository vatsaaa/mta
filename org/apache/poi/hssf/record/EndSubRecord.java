// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hssf.record;

import org.apache.poi.util.LittleEndianOutput;
import org.apache.poi.util.LittleEndianInput;

public final class EndSubRecord extends SubRecord
{
    public static final short sid = 0;
    private static final int ENCODED_SIZE = 0;
    
    public EndSubRecord() {
    }
    
    public EndSubRecord(final LittleEndianInput in, final int size) {
        if ((size & 0xFF) != 0x0) {
            throw new RecordFormatException("Unexpected size (" + size + ")");
        }
    }
    
    @Override
    public boolean isTerminating() {
        return true;
    }
    
    @Override
    public String toString() {
        final StringBuffer buffer = new StringBuffer();
        buffer.append("[ftEnd]\n");
        buffer.append("[/ftEnd]\n");
        return buffer.toString();
    }
    
    @Override
    public void serialize(final LittleEndianOutput out) {
        out.writeShort(0);
        out.writeShort(0);
    }
    
    @Override
    protected int getDataSize() {
        return 0;
    }
    
    public short getSid() {
        return 0;
    }
    
    @Override
    public Object clone() {
        final EndSubRecord rec = new EndSubRecord();
        return rec;
    }
}
