// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hssf.record;

import org.apache.poi.util.LittleEndianOutput;
import org.apache.poi.util.HexDump;
import org.apache.poi.ss.usermodel.ErrorConstants;

public final class BoolErrRecord extends CellRecord
{
    public static final short sid = 517;
    private int _value;
    private boolean _isError;
    
    public BoolErrRecord() {
    }
    
    public BoolErrRecord(final RecordInputStream in) {
        super(in);
        switch (in.remaining()) {
            case 2: {
                this._value = in.readByte();
                break;
            }
            case 3: {
                this._value = in.readUShort();
                break;
            }
            default: {
                throw new RecordFormatException("Unexpected size (" + in.remaining() + ") for BOOLERR record.");
            }
        }
        final int flag = in.readUByte();
        switch (flag) {
            case 0: {
                this._isError = false;
                break;
            }
            case 1: {
                this._isError = true;
                break;
            }
            default: {
                throw new RecordFormatException("Unexpected isError flag (" + flag + ") for BOOLERR record.");
            }
        }
    }
    
    public void setValue(final boolean value) {
        this._value = (value ? 1 : 0);
        this._isError = false;
    }
    
    public void setValue(final byte value) {
        switch (value) {
            case 0:
            case 7:
            case 15:
            case 23:
            case 29:
            case 36:
            case 42: {
                this._value = value;
                this._isError = true;
            }
            default: {
                throw new IllegalArgumentException("Error Value can only be 0,7,15,23,29,36 or 42. It cannot be " + value);
            }
        }
    }
    
    public boolean getBooleanValue() {
        return this._value != 0;
    }
    
    public byte getErrorValue() {
        return (byte)this._value;
    }
    
    public boolean isBoolean() {
        return !this._isError;
    }
    
    public boolean isError() {
        return this._isError;
    }
    
    @Override
    protected String getRecordName() {
        return "BOOLERR";
    }
    
    @Override
    protected void appendValueText(final StringBuilder sb) {
        if (this.isBoolean()) {
            sb.append("  .boolVal = ");
            sb.append(this.getBooleanValue());
        }
        else {
            sb.append("  .errCode = ");
            sb.append(ErrorConstants.getText(this.getErrorValue()));
            sb.append(" (").append(HexDump.byteToHex(this.getErrorValue())).append(")");
        }
    }
    
    @Override
    protected void serializeValue(final LittleEndianOutput out) {
        out.writeByte(this._value);
        out.writeByte(this._isError ? 1 : 0);
    }
    
    @Override
    protected int getValueDataSize() {
        return 2;
    }
    
    @Override
    public short getSid() {
        return 517;
    }
    
    @Override
    public Object clone() {
        final BoolErrRecord rec = new BoolErrRecord();
        this.copyBaseFields(rec);
        rec._value = this._value;
        rec._isError = this._isError;
        return rec;
    }
}
