// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hssf.record;

import org.apache.poi.util.LittleEndianOutput;

public final class ObjectProtectRecord extends StandardRecord
{
    public static final short sid = 99;
    private short field_1_protect;
    
    public ObjectProtectRecord() {
    }
    
    public ObjectProtectRecord(final RecordInputStream in) {
        this.field_1_protect = in.readShort();
    }
    
    public void setProtect(final boolean protect) {
        if (protect) {
            this.field_1_protect = 1;
        }
        else {
            this.field_1_protect = 0;
        }
    }
    
    public boolean getProtect() {
        return this.field_1_protect == 1;
    }
    
    @Override
    public String toString() {
        final StringBuffer buffer = new StringBuffer();
        buffer.append("[SCENARIOPROTECT]\n");
        buffer.append("    .protect         = ").append(this.getProtect()).append("\n");
        buffer.append("[/SCENARIOPROTECT]\n");
        return buffer.toString();
    }
    
    public void serialize(final LittleEndianOutput out) {
        out.writeShort(this.field_1_protect);
    }
    
    @Override
    protected int getDataSize() {
        return 2;
    }
    
    @Override
    public short getSid() {
        return 99;
    }
    
    @Override
    public Object clone() {
        final ObjectProtectRecord rec = new ObjectProtectRecord();
        rec.field_1_protect = this.field_1_protect;
        return rec;
    }
}
