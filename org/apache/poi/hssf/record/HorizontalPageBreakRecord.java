// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hssf.record;

import java.util.Iterator;

public final class HorizontalPageBreakRecord extends PageBreakRecord
{
    public static final short sid = 27;
    
    public HorizontalPageBreakRecord() {
    }
    
    public HorizontalPageBreakRecord(final RecordInputStream in) {
        super(in);
    }
    
    @Override
    public short getSid() {
        return 27;
    }
    
    @Override
    public Object clone() {
        final PageBreakRecord result = new HorizontalPageBreakRecord();
        final Iterator iterator = this.getBreaksIterator();
        while (iterator.hasNext()) {
            final Break original = iterator.next();
            result.addBreak(original.main, original.subFrom, original.subTo);
        }
        return result;
    }
}
