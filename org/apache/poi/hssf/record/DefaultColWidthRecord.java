// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hssf.record;

import org.apache.poi.util.LittleEndianOutput;

public final class DefaultColWidthRecord extends StandardRecord
{
    public static final short sid = 85;
    private int field_1_col_width;
    public static final int DEFAULT_COLUMN_WIDTH = 8;
    
    public DefaultColWidthRecord() {
        this.field_1_col_width = 8;
    }
    
    public DefaultColWidthRecord(final RecordInputStream in) {
        this.field_1_col_width = in.readUShort();
    }
    
    public void setColWidth(final int width) {
        this.field_1_col_width = width;
    }
    
    public int getColWidth() {
        return this.field_1_col_width;
    }
    
    @Override
    public String toString() {
        final StringBuffer buffer = new StringBuffer();
        buffer.append("[DEFAULTCOLWIDTH]\n");
        buffer.append("    .colwidth      = ").append(Integer.toHexString(this.getColWidth())).append("\n");
        buffer.append("[/DEFAULTCOLWIDTH]\n");
        return buffer.toString();
    }
    
    public void serialize(final LittleEndianOutput out) {
        out.writeShort(this.getColWidth());
    }
    
    @Override
    protected int getDataSize() {
        return 2;
    }
    
    @Override
    public short getSid() {
        return 85;
    }
    
    @Override
    public Object clone() {
        final DefaultColWidthRecord rec = new DefaultColWidthRecord();
        rec.field_1_col_width = this.field_1_col_width;
        return rec;
    }
}
