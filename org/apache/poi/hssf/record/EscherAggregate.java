// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hssf.record;

import org.apache.poi.util.POILogFactory;
import org.apache.poi.ddf.EscherDgRecord;
import org.apache.poi.hssf.model.ConvertAnchor;
import org.apache.poi.ddf.EscherProperty;
import org.apache.poi.ddf.EscherBoolProperty;
import org.apache.poi.ddf.EscherClientDataRecord;
import org.apache.poi.hssf.model.CommentShape;
import org.apache.poi.hssf.model.TextboxShape;
import org.apache.poi.hssf.model.AbstractShape;
import org.apache.poi.ddf.EscherTextboxRecord;
import org.apache.poi.hssf.usermodel.HSSFChildAnchor;
import org.apache.poi.hssf.usermodel.HSSFSimpleShape;
import org.apache.poi.hssf.usermodel.HSSFPicture;
import org.apache.poi.ddf.EscherChildAnchorRecord;
import org.apache.poi.ddf.EscherClientAnchorRecord;
import org.apache.poi.ddf.EscherSimpleProperty;
import org.apache.poi.ddf.EscherOptRecord;
import org.apache.poi.hssf.usermodel.HSSFTextbox;
import org.apache.poi.ddf.EscherSpRecord;
import org.apache.poi.hssf.usermodel.HSSFAnchor;
import org.apache.poi.hssf.usermodel.HSSFShapeGroup;
import org.apache.poi.hssf.usermodel.HSSFClientAnchor;
import org.apache.poi.ddf.EscherDggRecord;
import org.apache.poi.hssf.usermodel.HSSFShape;
import org.apache.poi.hssf.usermodel.HSSFShapeContainer;
import org.apache.poi.ddf.EscherSpgrRecord;
import org.apache.poi.ddf.EscherContainerRecord;
import org.apache.poi.ddf.EscherSerializationListener;
import org.apache.poi.ddf.EscherRecordFactory;
import org.apache.poi.ddf.DefaultEscherRecordFactory;
import java.util.Iterator;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.apache.poi.hssf.model.DrawingManager2;
import org.apache.poi.ddf.EscherRecord;
import java.util.Map;
import org.apache.poi.hssf.usermodel.HSSFPatriarch;
import org.apache.poi.util.POILogger;

public final class EscherAggregate extends AbstractEscherHolderRecord
{
    public static final short sid = 9876;
    private static POILogger log;
    public static final short ST_MIN = 0;
    public static final short ST_NOT_PRIMATIVE = 0;
    public static final short ST_RECTANGLE = 1;
    public static final short ST_ROUNDRECTANGLE = 2;
    public static final short ST_ELLIPSE = 3;
    public static final short ST_DIAMOND = 4;
    public static final short ST_ISOCELESTRIANGLE = 5;
    public static final short ST_RIGHTTRIANGLE = 6;
    public static final short ST_PARALLELOGRAM = 7;
    public static final short ST_TRAPEZOID = 8;
    public static final short ST_HEXAGON = 9;
    public static final short ST_OCTAGON = 10;
    public static final short ST_PLUS = 11;
    public static final short ST_STAR = 12;
    public static final short ST_ARROW = 13;
    public static final short ST_THICKARROW = 14;
    public static final short ST_HOMEPLATE = 15;
    public static final short ST_CUBE = 16;
    public static final short ST_BALLOON = 17;
    public static final short ST_SEAL = 18;
    public static final short ST_ARC = 19;
    public static final short ST_LINE = 20;
    public static final short ST_PLAQUE = 21;
    public static final short ST_CAN = 22;
    public static final short ST_DONUT = 23;
    public static final short ST_TEXTSIMPLE = 24;
    public static final short ST_TEXTOCTAGON = 25;
    public static final short ST_TEXTHEXAGON = 26;
    public static final short ST_TEXTCURVE = 27;
    public static final short ST_TEXTWAVE = 28;
    public static final short ST_TEXTRING = 29;
    public static final short ST_TEXTONCURVE = 30;
    public static final short ST_TEXTONRING = 31;
    public static final short ST_STRAIGHTCONNECTOR1 = 32;
    public static final short ST_BENTCONNECTOR2 = 33;
    public static final short ST_BENTCONNECTOR3 = 34;
    public static final short ST_BENTCONNECTOR4 = 35;
    public static final short ST_BENTCONNECTOR5 = 36;
    public static final short ST_CURVEDCONNECTOR2 = 37;
    public static final short ST_CURVEDCONNECTOR3 = 38;
    public static final short ST_CURVEDCONNECTOR4 = 39;
    public static final short ST_CURVEDCONNECTOR5 = 40;
    public static final short ST_CALLOUT1 = 41;
    public static final short ST_CALLOUT2 = 42;
    public static final short ST_CALLOUT3 = 43;
    public static final short ST_ACCENTCALLOUT1 = 44;
    public static final short ST_ACCENTCALLOUT2 = 45;
    public static final short ST_ACCENTCALLOUT3 = 46;
    public static final short ST_BORDERCALLOUT1 = 47;
    public static final short ST_BORDERCALLOUT2 = 48;
    public static final short ST_BORDERCALLOUT3 = 49;
    public static final short ST_ACCENTBORDERCALLOUT1 = 50;
    public static final short ST_ACCENTBORDERCALLOUT2 = 51;
    public static final short ST_ACCENTBORDERCALLOUT3 = 52;
    public static final short ST_RIBBON = 53;
    public static final short ST_RIBBON2 = 54;
    public static final short ST_CHEVRON = 55;
    public static final short ST_PENTAGON = 56;
    public static final short ST_NOSMOKING = 57;
    public static final short ST_SEAL8 = 58;
    public static final short ST_SEAL16 = 59;
    public static final short ST_SEAL32 = 60;
    public static final short ST_WEDGERECTCALLOUT = 61;
    public static final short ST_WEDGERRECTCALLOUT = 62;
    public static final short ST_WEDGEELLIPSECALLOUT = 63;
    public static final short ST_WAVE = 64;
    public static final short ST_FOLDEDCORNER = 65;
    public static final short ST_LEFTARROW = 66;
    public static final short ST_DOWNARROW = 67;
    public static final short ST_UPARROW = 68;
    public static final short ST_LEFTRIGHTARROW = 69;
    public static final short ST_UPDOWNARROW = 70;
    public static final short ST_IRREGULARSEAL1 = 71;
    public static final short ST_IRREGULARSEAL2 = 72;
    public static final short ST_LIGHTNINGBOLT = 73;
    public static final short ST_HEART = 74;
    public static final short ST_PICTUREFRAME = 75;
    public static final short ST_QUADARROW = 76;
    public static final short ST_LEFTARROWCALLOUT = 77;
    public static final short ST_RIGHTARROWCALLOUT = 78;
    public static final short ST_UPARROWCALLOUT = 79;
    public static final short ST_DOWNARROWCALLOUT = 80;
    public static final short ST_LEFTRIGHTARROWCALLOUT = 81;
    public static final short ST_UPDOWNARROWCALLOUT = 82;
    public static final short ST_QUADARROWCALLOUT = 83;
    public static final short ST_BEVEL = 84;
    public static final short ST_LEFTBRACKET = 85;
    public static final short ST_RIGHTBRACKET = 86;
    public static final short ST_LEFTBRACE = 87;
    public static final short ST_RIGHTBRACE = 88;
    public static final short ST_LEFTUPARROW = 89;
    public static final short ST_BENTUPARROW = 90;
    public static final short ST_BENTARROW = 91;
    public static final short ST_SEAL24 = 92;
    public static final short ST_STRIPEDRIGHTARROW = 93;
    public static final short ST_NOTCHEDRIGHTARROW = 94;
    public static final short ST_BLOCKARC = 95;
    public static final short ST_SMILEYFACE = 96;
    public static final short ST_VERTICALSCROLL = 97;
    public static final short ST_HORIZONTALSCROLL = 98;
    public static final short ST_CIRCULARARROW = 99;
    public static final short ST_NOTCHEDCIRCULARARROW = 100;
    public static final short ST_UTURNARROW = 101;
    public static final short ST_CURVEDRIGHTARROW = 102;
    public static final short ST_CURVEDLEFTARROW = 103;
    public static final short ST_CURVEDUPARROW = 104;
    public static final short ST_CURVEDDOWNARROW = 105;
    public static final short ST_CLOUDCALLOUT = 106;
    public static final short ST_ELLIPSERIBBON = 107;
    public static final short ST_ELLIPSERIBBON2 = 108;
    public static final short ST_FLOWCHARTPROCESS = 109;
    public static final short ST_FLOWCHARTDECISION = 110;
    public static final short ST_FLOWCHARTINPUTOUTPUT = 111;
    public static final short ST_FLOWCHARTPREDEFINEDPROCESS = 112;
    public static final short ST_FLOWCHARTINTERNALSTORAGE = 113;
    public static final short ST_FLOWCHARTDOCUMENT = 114;
    public static final short ST_FLOWCHARTMULTIDOCUMENT = 115;
    public static final short ST_FLOWCHARTTERMINATOR = 116;
    public static final short ST_FLOWCHARTPREPARATION = 117;
    public static final short ST_FLOWCHARTMANUALINPUT = 118;
    public static final short ST_FLOWCHARTMANUALOPERATION = 119;
    public static final short ST_FLOWCHARTCONNECTOR = 120;
    public static final short ST_FLOWCHARTPUNCHEDCARD = 121;
    public static final short ST_FLOWCHARTPUNCHEDTAPE = 122;
    public static final short ST_FLOWCHARTSUMMINGJUNCTION = 123;
    public static final short ST_FLOWCHARTOR = 124;
    public static final short ST_FLOWCHARTCOLLATE = 125;
    public static final short ST_FLOWCHARTSORT = 126;
    public static final short ST_FLOWCHARTEXTRACT = 127;
    public static final short ST_FLOWCHARTMERGE = 128;
    public static final short ST_FLOWCHARTOFFLINESTORAGE = 129;
    public static final short ST_FLOWCHARTONLINESTORAGE = 130;
    public static final short ST_FLOWCHARTMAGNETICTAPE = 131;
    public static final short ST_FLOWCHARTMAGNETICDISK = 132;
    public static final short ST_FLOWCHARTMAGNETICDRUM = 133;
    public static final short ST_FLOWCHARTDISPLAY = 134;
    public static final short ST_FLOWCHARTDELAY = 135;
    public static final short ST_TEXTPLAINTEXT = 136;
    public static final short ST_TEXTSTOP = 137;
    public static final short ST_TEXTTRIANGLE = 138;
    public static final short ST_TEXTTRIANGLEINVERTED = 139;
    public static final short ST_TEXTCHEVRON = 140;
    public static final short ST_TEXTCHEVRONINVERTED = 141;
    public static final short ST_TEXTRINGINSIDE = 142;
    public static final short ST_TEXTRINGOUTSIDE = 143;
    public static final short ST_TEXTARCHUPCURVE = 144;
    public static final short ST_TEXTARCHDOWNCURVE = 145;
    public static final short ST_TEXTCIRCLECURVE = 146;
    public static final short ST_TEXTBUTTONCURVE = 147;
    public static final short ST_TEXTARCHUPPOUR = 148;
    public static final short ST_TEXTARCHDOWNPOUR = 149;
    public static final short ST_TEXTCIRCLEPOUR = 150;
    public static final short ST_TEXTBUTTONPOUR = 151;
    public static final short ST_TEXTCURVEUP = 152;
    public static final short ST_TEXTCURVEDOWN = 153;
    public static final short ST_TEXTCASCADEUP = 154;
    public static final short ST_TEXTCASCADEDOWN = 155;
    public static final short ST_TEXTWAVE1 = 156;
    public static final short ST_TEXTWAVE2 = 157;
    public static final short ST_TEXTWAVE3 = 158;
    public static final short ST_TEXTWAVE4 = 159;
    public static final short ST_TEXTINFLATE = 160;
    public static final short ST_TEXTDEFLATE = 161;
    public static final short ST_TEXTINFLATEBOTTOM = 162;
    public static final short ST_TEXTDEFLATEBOTTOM = 163;
    public static final short ST_TEXTINFLATETOP = 164;
    public static final short ST_TEXTDEFLATETOP = 165;
    public static final short ST_TEXTDEFLATEINFLATE = 166;
    public static final short ST_TEXTDEFLATEINFLATEDEFLATE = 167;
    public static final short ST_TEXTFADERIGHT = 168;
    public static final short ST_TEXTFADELEFT = 169;
    public static final short ST_TEXTFADEUP = 170;
    public static final short ST_TEXTFADEDOWN = 171;
    public static final short ST_TEXTSLANTUP = 172;
    public static final short ST_TEXTSLANTDOWN = 173;
    public static final short ST_TEXTCANUP = 174;
    public static final short ST_TEXTCANDOWN = 175;
    public static final short ST_FLOWCHARTALTERNATEPROCESS = 176;
    public static final short ST_FLOWCHARTOFFPAGECONNECTOR = 177;
    public static final short ST_CALLOUT90 = 178;
    public static final short ST_ACCENTCALLOUT90 = 179;
    public static final short ST_BORDERCALLOUT90 = 180;
    public static final short ST_ACCENTBORDERCALLOUT90 = 181;
    public static final short ST_LEFTRIGHTUPARROW = 182;
    public static final short ST_SUN = 183;
    public static final short ST_MOON = 184;
    public static final short ST_BRACKETPAIR = 185;
    public static final short ST_BRACEPAIR = 186;
    public static final short ST_SEAL4 = 187;
    public static final short ST_DOUBLEWAVE = 188;
    public static final short ST_ACTIONBUTTONBLANK = 189;
    public static final short ST_ACTIONBUTTONHOME = 190;
    public static final short ST_ACTIONBUTTONHELP = 191;
    public static final short ST_ACTIONBUTTONINFORMATION = 192;
    public static final short ST_ACTIONBUTTONFORWARDNEXT = 193;
    public static final short ST_ACTIONBUTTONBACKPREVIOUS = 194;
    public static final short ST_ACTIONBUTTONEND = 195;
    public static final short ST_ACTIONBUTTONBEGINNING = 196;
    public static final short ST_ACTIONBUTTONRETURN = 197;
    public static final short ST_ACTIONBUTTONDOCUMENT = 198;
    public static final short ST_ACTIONBUTTONSOUND = 199;
    public static final short ST_ACTIONBUTTONMOVIE = 200;
    public static final short ST_HOSTCONTROL = 201;
    public static final short ST_TEXTBOX = 202;
    public static final short ST_NIL = 4095;
    protected HSSFPatriarch patriarch;
    private Map<EscherRecord, Record> shapeToObj;
    private DrawingManager2 drawingManager;
    private short drawingGroupId;
    private List tailRec;
    
    public EscherAggregate(final DrawingManager2 drawingManager) {
        this.shapeToObj = new HashMap<EscherRecord, Record>();
        this.tailRec = new ArrayList();
        this.drawingManager = drawingManager;
    }
    
    @Override
    public short getSid() {
        return 9876;
    }
    
    @Override
    public String toString() {
        final String nl = System.getProperty("line.separtor");
        final StringBuffer result = new StringBuffer();
        result.append('[').append(this.getRecordName()).append(']' + nl);
        for (final EscherRecord escherRecord : this.getEscherRecords()) {
            result.append(escherRecord.toString());
        }
        result.append("[/").append(this.getRecordName()).append(']' + nl);
        return result.toString();
    }
    
    public static EscherAggregate createAggregate(final List records, final int locFirstDrawingRecord, final DrawingManager2 drawingManager) {
        final List<EscherRecord> shapeRecords = new ArrayList<EscherRecord>();
        final EscherRecordFactory recordFactory = new DefaultEscherRecordFactory() {
            @Override
            public EscherRecord createRecord(final byte[] data, final int offset) {
                final EscherRecord r = super.createRecord(data, offset);
                if (r.getRecordId() == -4079 || r.getRecordId() == -4083) {
                    shapeRecords.add(r);
                }
                return r;
            }
        };
        final EscherAggregate agg = new EscherAggregate(drawingManager);
        int loc = locFirstDrawingRecord;
        int dataSize = 0;
        while (loc + 1 < records.size() && sid(records, loc) == 236 && isObjectRecord(records, loc + 1)) {
            dataSize += records.get(loc).getData().length;
            loc += 2;
        }
        final byte[] buffer = new byte[dataSize];
        int offset = 0;
        for (loc = locFirstDrawingRecord; loc + 1 < records.size() && sid(records, loc) == 236 && isObjectRecord(records, loc + 1); loc += 2) {
            final DrawingRecord drawingRecord = records.get(loc);
            System.arraycopy(drawingRecord.getData(), 0, buffer, offset, drawingRecord.getData().length);
            offset += drawingRecord.getData().length;
        }
        int bytesRead;
        for (int pos = 0; pos < dataSize; pos += bytesRead) {
            final EscherRecord r = recordFactory.createRecord(buffer, pos);
            bytesRead = r.fillFields(buffer, pos, recordFactory);
            agg.addEscherRecord(r);
        }
        loc = locFirstDrawingRecord;
        int shapeIndex = 0;
        agg.shapeToObj = new HashMap<EscherRecord, Record>();
        while (loc + 1 < records.size() && sid(records, loc) == 236 && isObjectRecord(records, loc + 1)) {
            final Record objRecord = records.get(loc + 1);
            agg.shapeToObj.put(shapeRecords.get(shapeIndex++), objRecord);
            loc += 2;
        }
        return agg;
    }
    
    @Override
    public int serialize(final int offset, final byte[] data) {
        this.convertUserModelToRecords();
        final List records = this.getEscherRecords();
        final int size = this.getEscherRecordSize(records);
        final byte[] buffer = new byte[size];
        final List spEndingOffsets = new ArrayList();
        final List shapes = new ArrayList();
        int pos = 0;
        for (final EscherRecord e : records) {
            pos += e.serialize(pos, buffer, new EscherSerializationListener() {
                public void beforeRecordSerialize(final int offset, final short recordId, final EscherRecord record) {
                }
                
                public void afterRecordSerialize(final int offset, final short recordId, final int size, final EscherRecord record) {
                    if (recordId == -4079 || recordId == -4083) {
                        spEndingOffsets.add(offset);
                        shapes.add(record);
                    }
                }
            });
        }
        shapes.add(0, null);
        spEndingOffsets.add(0, null);
        pos = offset;
        for (int i = 1; i < shapes.size(); ++i) {
            final int endOffset = spEndingOffsets.get(i) - 1;
            int startOffset;
            if (i == 1) {
                startOffset = 0;
            }
            else {
                startOffset = spEndingOffsets.get(i - 1);
            }
            final DrawingRecord drawing = new DrawingRecord();
            final byte[] drawingData = new byte[endOffset - startOffset + 1];
            System.arraycopy(buffer, startOffset, drawingData, 0, drawingData.length);
            drawing.setData(drawingData);
            int temp = drawing.serialize(pos, data);
            pos += temp;
            final Record obj = this.shapeToObj.get(shapes.get(i));
            temp = obj.serialize(pos, data);
            pos += temp;
        }
        for (int i = 0; i < this.tailRec.size(); ++i) {
            final Record rec = this.tailRec.get(i);
            pos += rec.serialize(pos, data);
        }
        final int bytesWritten = pos - offset;
        if (bytesWritten != this.getRecordSize()) {
            throw new RecordFormatException(bytesWritten + " bytes written but getRecordSize() reports " + this.getRecordSize());
        }
        return bytesWritten;
    }
    
    private int getEscherRecordSize(final List records) {
        int size = 0;
        final Iterator iterator = records.iterator();
        while (iterator.hasNext()) {
            size += iterator.next().getRecordSize();
        }
        return size;
    }
    
    @Override
    public int getRecordSize() {
        this.convertUserModelToRecords();
        final List records = this.getEscherRecords();
        final int rawEscherSize = this.getEscherRecordSize(records);
        final int drawingRecordSize = rawEscherSize + this.shapeToObj.size() * 4;
        int objRecordSize = 0;
        for (final Record r : this.shapeToObj.values()) {
            objRecordSize += r.getRecordSize();
        }
        int tailRecordSize = 0;
        for (final Record r2 : this.tailRec) {
            tailRecordSize += r2.getRecordSize();
        }
        return drawingRecordSize + objRecordSize + tailRecordSize;
    }
    
    Object associateShapeToObjRecord(final EscherRecord r, final ObjRecord objRecord) {
        return this.shapeToObj.put(r, objRecord);
    }
    
    public HSSFPatriarch getPatriarch() {
        return this.patriarch;
    }
    
    public void setPatriarch(final HSSFPatriarch patriarch) {
        this.patriarch = patriarch;
    }
    
    public void convertRecordsToUserModel() {
        if (this.patriarch == null) {
            throw new IllegalStateException("Must call setPatriarch() first");
        }
        EscherContainerRecord topContainer = this.getEscherContainer();
        if (topContainer == null) {
            return;
        }
        topContainer = topContainer.getChildContainers().get(0);
        final List tcc = topContainer.getChildContainers();
        if (tcc.size() == 0) {
            throw new IllegalStateException("No child escher containers at the point that should hold the patriach data, and one container per top level shape!");
        }
        final EscherContainerRecord patriachContainer = tcc.get(0);
        EscherSpgrRecord spgr = null;
        final Iterator<EscherRecord> it = patriachContainer.getChildIterator();
        while (it.hasNext()) {
            final EscherRecord r = it.next();
            if (r instanceof EscherSpgrRecord) {
                spgr = (EscherSpgrRecord)r;
                break;
            }
        }
        if (spgr != null) {
            this.patriarch.setCoordinates(spgr.getRectX1(), spgr.getRectY1(), spgr.getRectX2(), spgr.getRectY2());
        }
        convertRecordsToUserModelRecursive(tcc, this.patriarch, null);
        this.drawingManager.getDgg().setFileIdClusters(new EscherDggRecord.FileIdCluster[0]);
    }
    
    private static void convertRecordsToUserModelRecursive(final List tcc, final HSSFShapeContainer container, final HSSFShape parent) {
        for (int i = 1; i < tcc.size(); ++i) {
            final EscherContainerRecord shapeContainer = tcc.get(i);
            if (shapeContainer.getRecordId() == -4093) {
                final int shapeChildren = shapeContainer.getChildRecords().size();
                if (shapeChildren > 0) {
                    final HSSFShapeGroup group = new HSSFShapeGroup(parent, new HSSFClientAnchor());
                    addToParentOrContainer(group, container, parent);
                    final EscherContainerRecord groupContainer = (EscherContainerRecord)shapeContainer.getChild(0);
                    convertRecordsToUserModel(groupContainer, group);
                    if (shapeChildren > 1) {
                        convertRecordsToUserModelRecursive(shapeContainer.getChildRecords(), container, group);
                    }
                }
                else {
                    EscherAggregate.log.log(POILogger.WARN, "Found drawing group without children.");
                }
            }
            else if (shapeContainer.getRecordId() == -4092) {
                final EscherSpRecord spRecord = shapeContainer.getChildById((short)(-4086));
                final int type = spRecord.getShapeType();
                switch (type) {
                    case 202: {
                        final HSSFTextbox box = new HSSFTextbox(parent, new HSSFClientAnchor());
                        addToParentOrContainer(box, container, parent);
                        convertRecordsToUserModel(shapeContainer, box);
                        break;
                    }
                    case 75: {
                        final EscherOptRecord opt = (EscherOptRecord)getEscherChild(shapeContainer, -4085);
                        final EscherSimpleProperty prop = opt.lookup(260);
                        if (prop == null) {
                            EscherAggregate.log.log(POILogger.WARN, "Picture index for picture shape not found.");
                            break;
                        }
                        final int pictureIndex = prop.getPropertyValue();
                        final EscherClientAnchorRecord anchorRecord = (EscherClientAnchorRecord)getEscherChild(shapeContainer, -4080);
                        final EscherChildAnchorRecord childRecord = (EscherChildAnchorRecord)getEscherChild(shapeContainer, -4081);
                        if (anchorRecord != null && childRecord != null) {
                            EscherAggregate.log.log(POILogger.WARN, "Picture with both CLIENT and CHILD anchor: " + type);
                        }
                        HSSFAnchor anchor;
                        if (anchorRecord != null) {
                            anchor = toClientAnchor(anchorRecord);
                        }
                        else {
                            anchor = toChildAnchor(childRecord);
                        }
                        final HSSFPicture picture = new HSSFPicture(parent, anchor);
                        picture.setPictureIndex(pictureIndex);
                        addToParentOrContainer(picture, container, parent);
                        break;
                    }
                    default: {
                        final HSSFSimpleShape shape = new HSSFSimpleShape(parent, new HSSFClientAnchor());
                        addToParentOrContainer(shape, container, parent);
                        convertRecordsToUserModel(shapeContainer, shape);
                        EscherAggregate.log.log(POILogger.WARN, "Unhandled shape type: " + type);
                        break;
                    }
                }
            }
            else {
                EscherAggregate.log.log(POILogger.WARN, "Unexpected record id of shape group.");
            }
        }
    }
    
    private static void addToParentOrContainer(final HSSFShape shape, final HSSFShapeContainer container, final HSSFShape parent) {
        if (parent instanceof HSSFShapeGroup) {
            ((HSSFShapeGroup)parent).addShape(shape);
        }
        else if (container instanceof HSSFPatriarch) {
            ((HSSFPatriarch)container).addShape(shape);
        }
        else {
            container.getChildren().add(shape);
        }
    }
    
    private static HSSFClientAnchor toClientAnchor(final EscherClientAnchorRecord anchorRecord) {
        final HSSFClientAnchor anchor = new HSSFClientAnchor();
        anchor.setAnchorType(anchorRecord.getFlag());
        anchor.setCol1(anchorRecord.getCol1());
        anchor.setCol2(anchorRecord.getCol2());
        anchor.setDx1(anchorRecord.getDx1());
        anchor.setDx2(anchorRecord.getDx2());
        anchor.setDy1(anchorRecord.getDy1());
        anchor.setDy2(anchorRecord.getDy2());
        anchor.setRow1(anchorRecord.getRow1());
        anchor.setRow2(anchorRecord.getRow2());
        return anchor;
    }
    
    private static HSSFChildAnchor toChildAnchor(final EscherChildAnchorRecord anchorRecord) {
        final HSSFChildAnchor anchor = new HSSFChildAnchor();
        anchor.setDx1(anchorRecord.getDx1());
        anchor.setDx2(anchorRecord.getDx2());
        anchor.setDy1(anchorRecord.getDy1());
        anchor.setDy2(anchorRecord.getDy2());
        return anchor;
    }
    
    private static void convertRecordsToUserModel(final EscherContainerRecord shapeContainer, final Object model) {
        final Iterator<EscherRecord> it = shapeContainer.getChildIterator();
        while (it.hasNext()) {
            final EscherRecord r = it.next();
            if (r instanceof EscherSpgrRecord) {
                final EscherSpgrRecord spgr = (EscherSpgrRecord)r;
                if (!(model instanceof HSSFShapeGroup)) {
                    throw new IllegalStateException("Got top level anchor but not processing a group");
                }
                final HSSFShapeGroup g = (HSSFShapeGroup)model;
                g.setCoordinates(spgr.getRectX1(), spgr.getRectY1(), spgr.getRectX2(), spgr.getRectY2());
            }
            else if (r instanceof EscherClientAnchorRecord) {
                final EscherClientAnchorRecord car = (EscherClientAnchorRecord)r;
                if (!(model instanceof HSSFShape)) {
                    throw new IllegalStateException("Got top level anchor but not processing a group or shape");
                }
                final HSSFShape g2 = (HSSFShape)model;
                g2.getAnchor().setDx1(car.getDx1());
                g2.getAnchor().setDx2(car.getDx2());
                g2.getAnchor().setDy1(car.getDy1());
                g2.getAnchor().setDy2(car.getDy2());
            }
            else if (r instanceof EscherTextboxRecord) {
                final EscherTextboxRecord tbr = (EscherTextboxRecord)r;
            }
            else if (r instanceof EscherSpRecord) {
                final EscherSpRecord spr = (EscherSpRecord)r;
                if (!(model instanceof HSSFShape)) {
                    continue;
                }
                final HSSFShape s = (HSSFShape)model;
            }
            else if (r instanceof EscherOptRecord) {}
        }
    }
    
    public void clear() {
        this.clearEscherRecords();
        this.shapeToObj.clear();
    }
    
    @Override
    protected String getRecordName() {
        return "ESCHERAGGREGATE";
    }
    
    private static boolean isObjectRecord(final List records, final int loc) {
        return sid(records, loc) == 93 || sid(records, loc) == 438;
    }
    
    private void convertUserModelToRecords() {
        if (this.patriarch != null) {
            this.shapeToObj.clear();
            this.tailRec.clear();
            this.clearEscherRecords();
            if (this.patriarch.getChildren().size() != 0) {
                this.convertPatriarch(this.patriarch);
                final EscherContainerRecord dgContainer = (EscherContainerRecord)this.getEscherRecord(0);
                EscherContainerRecord spgrContainer = null;
                final Iterator<EscherRecord> iter = dgContainer.getChildIterator();
                while (iter.hasNext()) {
                    final EscherRecord child = iter.next();
                    if (child.getRecordId() == -4093) {
                        spgrContainer = (EscherContainerRecord)child;
                    }
                }
                this.convertShapes(this.patriarch, spgrContainer, this.shapeToObj);
                this.patriarch = null;
            }
        }
    }
    
    private void convertShapes(final HSSFShapeContainer parent, final EscherContainerRecord escherParent, final Map shapeToObj) {
        if (escherParent == null) {
            throw new IllegalArgumentException("Parent record required");
        }
        final List shapes = parent.getChildren();
        for (final HSSFShape shape : shapes) {
            if (shape instanceof HSSFShapeGroup) {
                this.convertGroup((HSSFShapeGroup)shape, escherParent, shapeToObj);
            }
            else {
                final AbstractShape shapeModel = AbstractShape.createShape(shape, this.drawingManager.allocateShapeId(this.drawingGroupId));
                shapeToObj.put(this.findClientData(shapeModel.getSpContainer()), shapeModel.getObjRecord());
                if (shapeModel instanceof TextboxShape) {
                    final EscherRecord escherTextbox = ((TextboxShape)shapeModel).getEscherTextbox();
                    shapeToObj.put(escherTextbox, ((TextboxShape)shapeModel).getTextObjectRecord());
                    if (shapeModel instanceof CommentShape) {
                        final CommentShape comment = (CommentShape)shapeModel;
                        this.tailRec.add(comment.getNoteRecord());
                    }
                }
                escherParent.addChildRecord(shapeModel.getSpContainer());
            }
        }
    }
    
    private void convertGroup(final HSSFShapeGroup shape, final EscherContainerRecord escherParent, final Map shapeToObj) {
        final EscherContainerRecord spgrContainer = new EscherContainerRecord();
        final EscherContainerRecord spContainer = new EscherContainerRecord();
        final EscherSpgrRecord spgr = new EscherSpgrRecord();
        final EscherSpRecord sp = new EscherSpRecord();
        final EscherOptRecord opt = new EscherOptRecord();
        final EscherClientDataRecord clientData = new EscherClientDataRecord();
        spgrContainer.setRecordId((short)(-4093));
        spgrContainer.setOptions((short)15);
        spContainer.setRecordId((short)(-4092));
        spContainer.setOptions((short)15);
        spgr.setRecordId((short)(-4087));
        spgr.setOptions((short)1);
        spgr.setRectX1(shape.getX1());
        spgr.setRectY1(shape.getY1());
        spgr.setRectX2(shape.getX2());
        spgr.setRectY2(shape.getY2());
        sp.setRecordId((short)(-4086));
        sp.setOptions((short)2);
        final int shapeId = this.drawingManager.allocateShapeId(this.drawingGroupId);
        sp.setShapeId(shapeId);
        if (shape.getAnchor() instanceof HSSFClientAnchor) {
            sp.setFlags(513);
        }
        else {
            sp.setFlags(515);
        }
        opt.setRecordId((short)(-4085));
        opt.setOptions((short)35);
        opt.addEscherProperty(new EscherBoolProperty((short)127, 262148));
        opt.addEscherProperty(new EscherBoolProperty((short)959, 524288));
        final EscherRecord anchor = ConvertAnchor.createAnchor(shape.getAnchor());
        clientData.setRecordId((short)(-4079));
        clientData.setOptions((short)0);
        spgrContainer.addChildRecord(spContainer);
        spContainer.addChildRecord(spgr);
        spContainer.addChildRecord(sp);
        spContainer.addChildRecord(opt);
        spContainer.addChildRecord(anchor);
        spContainer.addChildRecord(clientData);
        final ObjRecord obj = new ObjRecord();
        final CommonObjectDataSubRecord cmo = new CommonObjectDataSubRecord();
        cmo.setObjectType((short)0);
        cmo.setObjectId(shapeId);
        cmo.setLocked(true);
        cmo.setPrintable(true);
        cmo.setAutofill(true);
        cmo.setAutoline(true);
        final GroupMarkerSubRecord gmo = new GroupMarkerSubRecord();
        final EndSubRecord end = new EndSubRecord();
        obj.addSubRecord(cmo);
        obj.addSubRecord(gmo);
        obj.addSubRecord(end);
        shapeToObj.put(clientData, obj);
        escherParent.addChildRecord(spgrContainer);
        this.convertShapes(shape, spgrContainer, shapeToObj);
    }
    
    private EscherRecord findClientData(final EscherContainerRecord spContainer) {
        final Iterator<EscherRecord> iterator = spContainer.getChildIterator();
        while (iterator.hasNext()) {
            final EscherRecord r = iterator.next();
            if (r.getRecordId() == -4079) {
                return r;
            }
        }
        throw new IllegalArgumentException("Can not find client data record");
    }
    
    private void convertPatriarch(final HSSFPatriarch patriarch) {
        final EscherContainerRecord dgContainer = new EscherContainerRecord();
        final EscherContainerRecord spgrContainer = new EscherContainerRecord();
        final EscherContainerRecord spContainer1 = new EscherContainerRecord();
        final EscherSpgrRecord spgr = new EscherSpgrRecord();
        final EscherSpRecord sp1 = new EscherSpRecord();
        dgContainer.setRecordId((short)(-4094));
        dgContainer.setOptions((short)15);
        final EscherDgRecord dg = this.drawingManager.createDgRecord();
        this.drawingGroupId = dg.getDrawingGroupId();
        spgrContainer.setRecordId((short)(-4093));
        spgrContainer.setOptions((short)15);
        spContainer1.setRecordId((short)(-4092));
        spContainer1.setOptions((short)15);
        spgr.setRecordId((short)(-4087));
        spgr.setOptions((short)1);
        spgr.setRectX1(patriarch.getX1());
        spgr.setRectY1(patriarch.getY1());
        spgr.setRectX2(patriarch.getX2());
        spgr.setRectY2(patriarch.getY2());
        sp1.setRecordId((short)(-4086));
        sp1.setOptions((short)2);
        sp1.setShapeId(this.drawingManager.allocateShapeId(dg.getDrawingGroupId()));
        sp1.setFlags(5);
        dgContainer.addChildRecord(dg);
        dgContainer.addChildRecord(spgrContainer);
        spgrContainer.addChildRecord(spContainer1);
        spContainer1.addChildRecord(spgr);
        spContainer1.addChildRecord(sp1);
        this.addEscherRecord(dgContainer);
    }
    
    private static short sid(final List records, final int loc) {
        return records.get(loc).getSid();
    }
    
    private static EscherRecord getEscherChild(final EscherContainerRecord owner, final int recordId) {
        for (final EscherRecord escherRecord : owner.getChildRecords()) {
            if (escherRecord.getRecordId() == recordId) {
                return escherRecord;
            }
        }
        return null;
    }
    
    static {
        EscherAggregate.log = POILogFactory.getLogger(EscherAggregate.class);
    }
}
