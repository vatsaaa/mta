// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hssf.record;

import org.apache.poi.util.HexDump;
import org.apache.poi.util.LittleEndianInput;
import org.apache.poi.util.StringUtil;
import org.apache.poi.util.LittleEndianOutput;

public final class NameCommentRecord extends StandardRecord
{
    public static final short sid = 2196;
    private final short field_1_record_type;
    private final short field_2_frt_cell_ref_flag;
    private final long field_3_reserved;
    private String field_6_name_text;
    private String field_7_comment_text;
    
    public NameCommentRecord(final String name, final String comment) {
        this.field_1_record_type = 0;
        this.field_2_frt_cell_ref_flag = 0;
        this.field_3_reserved = 0L;
        this.field_6_name_text = name;
        this.field_7_comment_text = comment;
    }
    
    public void serialize(final LittleEndianOutput out) {
        final int field_4_name_length = this.field_6_name_text.length();
        final int field_5_comment_length = this.field_7_comment_text.length();
        out.writeShort(this.field_1_record_type);
        out.writeShort(this.field_2_frt_cell_ref_flag);
        out.writeLong(this.field_3_reserved);
        out.writeShort(field_4_name_length);
        out.writeShort(field_5_comment_length);
        out.writeByte(0);
        StringUtil.putCompressedUnicode(this.field_6_name_text, out);
        out.writeByte(0);
        StringUtil.putCompressedUnicode(this.field_7_comment_text, out);
    }
    
    @Override
    protected int getDataSize() {
        return 18 + this.field_6_name_text.length() + this.field_7_comment_text.length();
    }
    
    public NameCommentRecord(final RecordInputStream ris) {
        final LittleEndianInput in = ris;
        this.field_1_record_type = in.readShort();
        this.field_2_frt_cell_ref_flag = in.readShort();
        this.field_3_reserved = in.readLong();
        final int field_4_name_length = in.readShort();
        final int field_5_comment_length = in.readShort();
        in.readByte();
        this.field_6_name_text = StringUtil.readCompressedUnicode(in, field_4_name_length);
        in.readByte();
        this.field_7_comment_text = StringUtil.readCompressedUnicode(in, field_5_comment_length);
    }
    
    @Override
    public short getSid() {
        return 2196;
    }
    
    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer();
        sb.append("[NAMECMT]\n");
        sb.append("    .record type            = ").append(HexDump.shortToHex(this.field_1_record_type)).append("\n");
        sb.append("    .frt cell ref flag      = ").append(HexDump.byteToHex(this.field_2_frt_cell_ref_flag)).append("\n");
        sb.append("    .reserved               = ").append(this.field_3_reserved).append("\n");
        sb.append("    .name length            = ").append(this.field_6_name_text.length()).append("\n");
        sb.append("    .comment length         = ").append(this.field_7_comment_text.length()).append("\n");
        sb.append("    .name                   = ").append(this.field_6_name_text).append("\n");
        sb.append("    .comment                = ").append(this.field_7_comment_text).append("\n");
        sb.append("[/NAMECMT]\n");
        return sb.toString();
    }
    
    public String getNameText() {
        return this.field_6_name_text;
    }
    
    public void setNameText(final String newName) {
        this.field_6_name_text = newName;
    }
    
    public String getCommentText() {
        return this.field_7_comment_text;
    }
    
    public void setCommentText(final String comment) {
        this.field_7_comment_text = comment;
    }
    
    public short getRecordType() {
        return this.field_1_record_type;
    }
}
