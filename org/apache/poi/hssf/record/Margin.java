// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hssf.record;

public interface Margin
{
    double getMargin();
    
    void setMargin(final double p0);
}
