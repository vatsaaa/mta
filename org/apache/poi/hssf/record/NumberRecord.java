// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hssf.record;

import org.apache.poi.util.LittleEndianOutput;
import org.apache.poi.ss.util.NumberToTextConverter;

public final class NumberRecord extends CellRecord
{
    public static final short sid = 515;
    private double field_4_value;
    
    public NumberRecord() {
    }
    
    public NumberRecord(final RecordInputStream in) {
        super(in);
        this.field_4_value = in.readDouble();
    }
    
    public void setValue(final double value) {
        this.field_4_value = value;
    }
    
    public double getValue() {
        return this.field_4_value;
    }
    
    @Override
    protected String getRecordName() {
        return "NUMBER";
    }
    
    @Override
    protected void appendValueText(final StringBuilder sb) {
        sb.append("  .value= ").append(NumberToTextConverter.toText(this.field_4_value));
    }
    
    @Override
    protected void serializeValue(final LittleEndianOutput out) {
        out.writeDouble(this.getValue());
    }
    
    @Override
    protected int getValueDataSize() {
        return 8;
    }
    
    @Override
    public short getSid() {
        return 515;
    }
    
    @Override
    public Object clone() {
        final NumberRecord rec = new NumberRecord();
        this.copyBaseFields(rec);
        rec.field_4_value = this.field_4_value;
        return rec;
    }
}
