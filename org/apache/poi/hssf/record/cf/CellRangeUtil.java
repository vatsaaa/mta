// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hssf.record.cf;

import java.util.List;
import java.util.ArrayList;
import org.apache.poi.ss.util.CellRangeAddress;

public final class CellRangeUtil
{
    public static final int NO_INTERSECTION = 1;
    public static final int OVERLAP = 2;
    public static final int INSIDE = 3;
    public static final int ENCLOSES = 4;
    
    private CellRangeUtil() {
    }
    
    public static int intersect(final CellRangeAddress crA, final CellRangeAddress crB) {
        final int firstRow = crB.getFirstRow();
        final int lastRow = crB.getLastRow();
        final int firstCol = crB.getFirstColumn();
        final int lastCol = crB.getLastColumn();
        if (gt(crA.getFirstRow(), lastRow) || lt(crA.getLastRow(), firstRow) || gt(crA.getFirstColumn(), lastCol) || lt(crA.getLastColumn(), firstCol)) {
            return 1;
        }
        if (contains(crA, crB)) {
            return 3;
        }
        if (contains(crB, crA)) {
            return 4;
        }
        return 2;
    }
    
    public static CellRangeAddress[] mergeCellRanges(final CellRangeAddress[] cellRanges) {
        if (cellRanges.length < 1) {
            return cellRanges;
        }
        final List<CellRangeAddress> lst = new ArrayList<CellRangeAddress>();
        for (final CellRangeAddress cr : cellRanges) {
            lst.add(cr);
        }
        final List temp = mergeCellRanges(lst);
        return toArray(temp);
    }
    
    private static List mergeCellRanges(final List cellRangeList) {
        while (cellRangeList.size() > 1) {
            boolean somethingGotMerged = false;
            for (int i = 0; i < cellRangeList.size(); ++i) {
                final CellRangeAddress range1 = cellRangeList.get(i);
                for (int j = i + 1; j < cellRangeList.size(); ++j) {
                    final CellRangeAddress range2 = cellRangeList.get(j);
                    final CellRangeAddress[] mergeResult = mergeRanges(range1, range2);
                    if (mergeResult != null) {
                        somethingGotMerged = true;
                        cellRangeList.set(i, mergeResult[0]);
                        cellRangeList.remove(j--);
                        for (int k = 1; k < mergeResult.length; ++k) {
                            ++j;
                            cellRangeList.add(j, mergeResult[k]);
                        }
                    }
                }
            }
            if (!somethingGotMerged) {
                break;
            }
        }
        return cellRangeList;
    }
    
    private static CellRangeAddress[] mergeRanges(final CellRangeAddress range1, final CellRangeAddress range2) {
        final int x = intersect(range1, range2);
        switch (x) {
            case 1: {
                if (hasExactSharedBorder(range1, range2)) {
                    return new CellRangeAddress[] { createEnclosingCellRange(range1, range2) };
                }
                return null;
            }
            case 2: {
                return resolveRangeOverlap(range1, range2);
            }
            case 3: {
                return new CellRangeAddress[] { range1 };
            }
            case 4: {
                return new CellRangeAddress[] { range2 };
            }
            default: {
                throw new RuntimeException("unexpected intersection result (" + x + ")");
            }
        }
    }
    
    static CellRangeAddress[] resolveRangeOverlap(final CellRangeAddress rangeA, final CellRangeAddress rangeB) {
        if (rangeA.isFullColumnRange()) {
            if (rangeA.isFullRowRange()) {
                return null;
            }
            return sliceUp(rangeA, rangeB);
        }
        else if (rangeA.isFullRowRange()) {
            if (rangeB.isFullColumnRange()) {
                return null;
            }
            return sliceUp(rangeA, rangeB);
        }
        else {
            if (rangeB.isFullColumnRange()) {
                return sliceUp(rangeB, rangeA);
            }
            if (rangeB.isFullRowRange()) {
                return sliceUp(rangeB, rangeA);
            }
            return sliceUp(rangeA, rangeB);
        }
    }
    
    private static CellRangeAddress[] sliceUp(final CellRangeAddress crA, final CellRangeAddress crB) {
        List temp = new ArrayList();
        temp.add(crB);
        if (!crA.isFullColumnRange()) {
            temp = cutHorizontally(crA.getFirstRow(), temp);
            temp = cutHorizontally(crA.getLastRow() + 1, temp);
        }
        if (!crA.isFullRowRange()) {
            temp = cutVertically(crA.getFirstColumn(), temp);
            temp = cutVertically(crA.getLastColumn() + 1, temp);
        }
        final CellRangeAddress[] crParts = toArray(temp);
        temp.clear();
        temp.add(crA);
        for (int i = 0; i < crParts.length; ++i) {
            final CellRangeAddress crPart = crParts[i];
            if (intersect(crA, crPart) != 4) {
                temp.add(crPart);
            }
        }
        return toArray(temp);
    }
    
    private static List cutHorizontally(final int cutRow, final List input) {
        final List result = new ArrayList();
        final CellRangeAddress[] crs = toArray(input);
        for (int i = 0; i < crs.length; ++i) {
            final CellRangeAddress cr = crs[i];
            if (cr.getFirstRow() < cutRow && cutRow < cr.getLastRow()) {
                result.add(new CellRangeAddress(cr.getFirstRow(), cutRow, cr.getFirstColumn(), cr.getLastColumn()));
                result.add(new CellRangeAddress(cutRow + 1, cr.getLastRow(), cr.getFirstColumn(), cr.getLastColumn()));
            }
            else {
                result.add(cr);
            }
        }
        return result;
    }
    
    private static List cutVertically(final int cutColumn, final List input) {
        final List result = new ArrayList();
        final CellRangeAddress[] crs = toArray(input);
        for (int i = 0; i < crs.length; ++i) {
            final CellRangeAddress cr = crs[i];
            if (cr.getFirstColumn() < cutColumn && cutColumn < cr.getLastColumn()) {
                result.add(new CellRangeAddress(cr.getFirstRow(), cr.getLastRow(), cr.getFirstColumn(), cutColumn));
                result.add(new CellRangeAddress(cr.getFirstRow(), cr.getLastRow(), cutColumn + 1, cr.getLastColumn()));
            }
            else {
                result.add(cr);
            }
        }
        return result;
    }
    
    private static CellRangeAddress[] toArray(final List temp) {
        final CellRangeAddress[] result = new CellRangeAddress[temp.size()];
        temp.toArray(result);
        return result;
    }
    
    public static boolean contains(final CellRangeAddress crA, final CellRangeAddress crB) {
        final int firstRow = crB.getFirstRow();
        final int lastRow = crB.getLastRow();
        final int firstCol = crB.getFirstColumn();
        final int lastCol = crB.getLastColumn();
        return le(crA.getFirstRow(), firstRow) && ge(crA.getLastRow(), lastRow) && le(crA.getFirstColumn(), firstCol) && ge(crA.getLastColumn(), lastCol);
    }
    
    public static boolean hasExactSharedBorder(final CellRangeAddress crA, final CellRangeAddress crB) {
        final int oFirstRow = crB.getFirstRow();
        final int oLastRow = crB.getLastRow();
        final int oFirstCol = crB.getFirstColumn();
        final int oLastCol = crB.getLastColumn();
        if ((crA.getFirstRow() > 0 && crA.getFirstRow() - 1 == oLastRow) || (oFirstRow > 0 && oFirstRow - 1 == crA.getLastRow())) {
            return crA.getFirstColumn() == oFirstCol && crA.getLastColumn() == oLastCol;
        }
        return ((crA.getFirstColumn() > 0 && crA.getFirstColumn() - 1 == oLastCol) || (oFirstCol > 0 && crA.getLastColumn() == oFirstCol - 1)) && crA.getFirstRow() == oFirstRow && crA.getLastRow() == oLastRow;
    }
    
    public static CellRangeAddress createEnclosingCellRange(final CellRangeAddress crA, final CellRangeAddress crB) {
        if (crB == null) {
            return crA.copy();
        }
        return new CellRangeAddress(lt(crB.getFirstRow(), crA.getFirstRow()) ? crB.getFirstRow() : crA.getFirstRow(), gt(crB.getLastRow(), crA.getLastRow()) ? crB.getLastRow() : crA.getLastRow(), lt(crB.getFirstColumn(), crA.getFirstColumn()) ? crB.getFirstColumn() : crA.getFirstColumn(), gt(crB.getLastColumn(), crA.getLastColumn()) ? crB.getLastColumn() : crA.getLastColumn());
    }
    
    private static boolean lt(final int a, final int b) {
        return a != -1 && (b == -1 || a < b);
    }
    
    private static boolean le(final int a, final int b) {
        return a == b || lt(a, b);
    }
    
    private static boolean gt(final int a, final int b) {
        return lt(b, a);
    }
    
    private static boolean ge(final int a, final int b) {
        return !lt(a, b);
    }
}
