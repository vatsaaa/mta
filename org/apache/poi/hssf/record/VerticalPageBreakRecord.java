// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hssf.record;

import java.util.Iterator;

public final class VerticalPageBreakRecord extends PageBreakRecord
{
    public static final short sid = 26;
    
    public VerticalPageBreakRecord() {
    }
    
    public VerticalPageBreakRecord(final RecordInputStream in) {
        super(in);
    }
    
    @Override
    public short getSid() {
        return 26;
    }
    
    @Override
    public Object clone() {
        final PageBreakRecord result = new VerticalPageBreakRecord();
        final Iterator iterator = this.getBreaksIterator();
        while (iterator.hasNext()) {
            final Break original = iterator.next();
            result.addBreak(original.main, original.subFrom, original.subTo);
        }
        return result;
    }
}
