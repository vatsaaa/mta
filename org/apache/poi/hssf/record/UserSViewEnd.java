// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hssf.record;

import org.apache.poi.util.HexDump;
import org.apache.poi.util.LittleEndianOutput;

public final class UserSViewEnd extends StandardRecord
{
    public static final short sid = 427;
    private byte[] _rawData;
    
    public UserSViewEnd(final byte[] data) {
        this._rawData = data;
    }
    
    public UserSViewEnd(final RecordInputStream in) {
        this._rawData = in.readRemainder();
    }
    
    public void serialize(final LittleEndianOutput out) {
        out.write(this._rawData);
    }
    
    @Override
    protected int getDataSize() {
        return this._rawData.length;
    }
    
    @Override
    public short getSid() {
        return 427;
    }
    
    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer();
        sb.append("[").append("USERSVIEWEND").append("] (0x");
        sb.append(Integer.toHexString(427).toUpperCase() + ")\n");
        sb.append("  rawData=").append(HexDump.toHex(this._rawData)).append("\n");
        sb.append("[/").append("USERSVIEWEND").append("]\n");
        return sb.toString();
    }
    
    @Override
    public Object clone() {
        return this.cloneViaReserialise();
    }
}
