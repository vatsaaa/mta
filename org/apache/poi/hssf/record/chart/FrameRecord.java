// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hssf.record.chart;

import org.apache.poi.util.BitFieldFactory;
import org.apache.poi.util.LittleEndianOutput;
import org.apache.poi.util.HexDump;
import org.apache.poi.hssf.record.RecordInputStream;
import org.apache.poi.util.BitField;
import org.apache.poi.hssf.record.StandardRecord;

public final class FrameRecord extends StandardRecord
{
    public static final short sid = 4146;
    private static final BitField autoSize;
    private static final BitField autoPosition;
    private short field_1_borderType;
    public static final short BORDER_TYPE_REGULAR = 0;
    public static final short BORDER_TYPE_SHADOW = 1;
    private short field_2_options;
    
    public FrameRecord() {
    }
    
    public FrameRecord(final RecordInputStream in) {
        this.field_1_borderType = in.readShort();
        this.field_2_options = in.readShort();
    }
    
    @Override
    public String toString() {
        final StringBuffer buffer = new StringBuffer();
        buffer.append("[FRAME]\n");
        buffer.append("    .borderType           = ").append("0x").append(HexDump.toHex(this.getBorderType())).append(" (").append(this.getBorderType()).append(" )");
        buffer.append(System.getProperty("line.separator"));
        buffer.append("    .options              = ").append("0x").append(HexDump.toHex(this.getOptions())).append(" (").append(this.getOptions()).append(" )");
        buffer.append(System.getProperty("line.separator"));
        buffer.append("         .autoSize                 = ").append(this.isAutoSize()).append('\n');
        buffer.append("         .autoPosition             = ").append(this.isAutoPosition()).append('\n');
        buffer.append("[/FRAME]\n");
        return buffer.toString();
    }
    
    public void serialize(final LittleEndianOutput out) {
        out.writeShort(this.field_1_borderType);
        out.writeShort(this.field_2_options);
    }
    
    @Override
    protected int getDataSize() {
        return 4;
    }
    
    @Override
    public short getSid() {
        return 4146;
    }
    
    @Override
    public Object clone() {
        final FrameRecord rec = new FrameRecord();
        rec.field_1_borderType = this.field_1_borderType;
        rec.field_2_options = this.field_2_options;
        return rec;
    }
    
    public short getBorderType() {
        return this.field_1_borderType;
    }
    
    public void setBorderType(final short field_1_borderType) {
        this.field_1_borderType = field_1_borderType;
    }
    
    public short getOptions() {
        return this.field_2_options;
    }
    
    public void setOptions(final short field_2_options) {
        this.field_2_options = field_2_options;
    }
    
    public void setAutoSize(final boolean value) {
        this.field_2_options = FrameRecord.autoSize.setShortBoolean(this.field_2_options, value);
    }
    
    public boolean isAutoSize() {
        return FrameRecord.autoSize.isSet(this.field_2_options);
    }
    
    public void setAutoPosition(final boolean value) {
        this.field_2_options = FrameRecord.autoPosition.setShortBoolean(this.field_2_options, value);
    }
    
    public boolean isAutoPosition() {
        return FrameRecord.autoPosition.isSet(this.field_2_options);
    }
    
    static {
        autoSize = BitFieldFactory.getInstance(1);
        autoPosition = BitFieldFactory.getInstance(2);
    }
}
