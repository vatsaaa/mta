// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hssf.record.chart;

import org.apache.poi.util.LittleEndianOutput;
import org.apache.poi.util.HexDump;
import org.apache.poi.hssf.record.RecordInputStream;
import org.apache.poi.hssf.record.StandardRecord;

public final class SeriesToChartGroupRecord extends StandardRecord
{
    public static final short sid = 4165;
    private short field_1_chartGroupIndex;
    
    public SeriesToChartGroupRecord() {
    }
    
    public SeriesToChartGroupRecord(final RecordInputStream in) {
        this.field_1_chartGroupIndex = in.readShort();
    }
    
    @Override
    public String toString() {
        final StringBuffer buffer = new StringBuffer();
        buffer.append("[SeriesToChartGroup]\n");
        buffer.append("    .chartGroupIndex      = ").append("0x").append(HexDump.toHex(this.getChartGroupIndex())).append(" (").append(this.getChartGroupIndex()).append(" )");
        buffer.append(System.getProperty("line.separator"));
        buffer.append("[/SeriesToChartGroup]\n");
        return buffer.toString();
    }
    
    public void serialize(final LittleEndianOutput out) {
        out.writeShort(this.field_1_chartGroupIndex);
    }
    
    @Override
    protected int getDataSize() {
        return 2;
    }
    
    @Override
    public short getSid() {
        return 4165;
    }
    
    @Override
    public Object clone() {
        final SeriesToChartGroupRecord rec = new SeriesToChartGroupRecord();
        rec.field_1_chartGroupIndex = this.field_1_chartGroupIndex;
        return rec;
    }
    
    public short getChartGroupIndex() {
        return this.field_1_chartGroupIndex;
    }
    
    public void setChartGroupIndex(final short field_1_chartGroupIndex) {
        this.field_1_chartGroupIndex = field_1_chartGroupIndex;
    }
}
