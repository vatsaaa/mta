// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hssf.record.chart;

import org.apache.poi.util.LittleEndianOutput;
import org.apache.poi.util.HexDump;
import org.apache.poi.hssf.record.RecordInputStream;
import org.apache.poi.hssf.record.StandardRecord;

public final class UnitsRecord extends StandardRecord
{
    public static final short sid = 4097;
    private short field_1_units;
    
    public UnitsRecord() {
    }
    
    public UnitsRecord(final RecordInputStream in) {
        this.field_1_units = in.readShort();
    }
    
    @Override
    public String toString() {
        final StringBuffer buffer = new StringBuffer();
        buffer.append("[UNITS]\n");
        buffer.append("    .units                = ").append("0x").append(HexDump.toHex(this.getUnits())).append(" (").append(this.getUnits()).append(" )");
        buffer.append(System.getProperty("line.separator"));
        buffer.append("[/UNITS]\n");
        return buffer.toString();
    }
    
    public void serialize(final LittleEndianOutput out) {
        out.writeShort(this.field_1_units);
    }
    
    @Override
    protected int getDataSize() {
        return 2;
    }
    
    @Override
    public short getSid() {
        return 4097;
    }
    
    @Override
    public Object clone() {
        final UnitsRecord rec = new UnitsRecord();
        rec.field_1_units = this.field_1_units;
        return rec;
    }
    
    public short getUnits() {
        return this.field_1_units;
    }
    
    public void setUnits(final short field_1_units) {
        this.field_1_units = field_1_units;
    }
}
