// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hssf.record.chart;

import org.apache.poi.util.LittleEndianOutput;
import org.apache.poi.hssf.record.RecordInputStream;
import org.apache.poi.hssf.record.StandardRecord;

public final class SeriesListRecord extends StandardRecord
{
    public static final short sid = 4118;
    private short[] field_1_seriesNumbers;
    
    public SeriesListRecord(final short[] seriesNumbers) {
        this.field_1_seriesNumbers = seriesNumbers;
    }
    
    public SeriesListRecord(final RecordInputStream in) {
        final int nItems = in.readUShort();
        final short[] ss = new short[nItems];
        for (int i = 0; i < nItems; ++i) {
            ss[i] = in.readShort();
        }
        this.field_1_seriesNumbers = ss;
    }
    
    @Override
    public String toString() {
        final StringBuffer buffer = new StringBuffer();
        buffer.append("[SERIESLIST]\n");
        buffer.append("    .seriesNumbers= ").append(" (").append(this.getSeriesNumbers()).append(" )");
        buffer.append("\n");
        buffer.append("[/SERIESLIST]\n");
        return buffer.toString();
    }
    
    public void serialize(final LittleEndianOutput out) {
        final int nItems = this.field_1_seriesNumbers.length;
        out.writeShort(nItems);
        for (int i = 0; i < nItems; ++i) {
            out.writeShort(this.field_1_seriesNumbers[i]);
        }
    }
    
    @Override
    protected int getDataSize() {
        return this.field_1_seriesNumbers.length * 2 + 2;
    }
    
    @Override
    public short getSid() {
        return 4118;
    }
    
    @Override
    public Object clone() {
        return new SeriesListRecord(this.field_1_seriesNumbers.clone());
    }
    
    public short[] getSeriesNumbers() {
        return this.field_1_seriesNumbers;
    }
}
