// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hssf.record.chart;

import org.apache.poi.util.LittleEndianOutput;
import org.apache.poi.util.HexDump;
import org.apache.poi.hssf.record.RecordInputStream;
import org.apache.poi.hssf.record.StandardRecord;

public final class SeriesIndexRecord extends StandardRecord
{
    public static final short sid = 4197;
    private short field_1_index;
    
    public SeriesIndexRecord() {
    }
    
    public SeriesIndexRecord(final RecordInputStream in) {
        this.field_1_index = in.readShort();
    }
    
    @Override
    public String toString() {
        final StringBuffer buffer = new StringBuffer();
        buffer.append("[SINDEX]\n");
        buffer.append("    .index                = ").append("0x").append(HexDump.toHex(this.getIndex())).append(" (").append(this.getIndex()).append(" )");
        buffer.append(System.getProperty("line.separator"));
        buffer.append("[/SINDEX]\n");
        return buffer.toString();
    }
    
    public void serialize(final LittleEndianOutput out) {
        out.writeShort(this.field_1_index);
    }
    
    @Override
    protected int getDataSize() {
        return 2;
    }
    
    @Override
    public short getSid() {
        return 4197;
    }
    
    @Override
    public Object clone() {
        final SeriesIndexRecord rec = new SeriesIndexRecord();
        rec.field_1_index = this.field_1_index;
        return rec;
    }
    
    public short getIndex() {
        return this.field_1_index;
    }
    
    public void setIndex(final short field_1_index) {
        this.field_1_index = field_1_index;
    }
}
