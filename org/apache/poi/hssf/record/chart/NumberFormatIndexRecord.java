// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hssf.record.chart;

import org.apache.poi.util.LittleEndianOutput;
import org.apache.poi.util.HexDump;
import org.apache.poi.hssf.record.RecordInputStream;
import org.apache.poi.hssf.record.StandardRecord;

public final class NumberFormatIndexRecord extends StandardRecord
{
    public static final short sid = 4174;
    private short field_1_formatIndex;
    
    public NumberFormatIndexRecord() {
    }
    
    public NumberFormatIndexRecord(final RecordInputStream in) {
        this.field_1_formatIndex = in.readShort();
    }
    
    @Override
    public String toString() {
        final StringBuffer buffer = new StringBuffer();
        buffer.append("[IFMT]\n");
        buffer.append("    .formatIndex          = ").append("0x").append(HexDump.toHex(this.getFormatIndex())).append(" (").append(this.getFormatIndex()).append(" )");
        buffer.append(System.getProperty("line.separator"));
        buffer.append("[/IFMT]\n");
        return buffer.toString();
    }
    
    public void serialize(final LittleEndianOutput out) {
        out.writeShort(this.field_1_formatIndex);
    }
    
    @Override
    protected int getDataSize() {
        return 2;
    }
    
    @Override
    public short getSid() {
        return 4174;
    }
    
    @Override
    public Object clone() {
        final NumberFormatIndexRecord rec = new NumberFormatIndexRecord();
        rec.field_1_formatIndex = this.field_1_formatIndex;
        return rec;
    }
    
    public short getFormatIndex() {
        return this.field_1_formatIndex;
    }
    
    public void setFormatIndex(final short field_1_formatIndex) {
        this.field_1_formatIndex = field_1_formatIndex;
    }
}
