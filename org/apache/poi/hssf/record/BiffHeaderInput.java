// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hssf.record;

public interface BiffHeaderInput
{
    int readRecordSID();
    
    int readDataSize();
    
    int available();
}
