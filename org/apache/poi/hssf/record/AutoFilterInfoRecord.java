// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hssf.record;

import org.apache.poi.util.LittleEndianOutput;

public final class AutoFilterInfoRecord extends StandardRecord
{
    public static final short sid = 157;
    private short _cEntries;
    
    public AutoFilterInfoRecord() {
    }
    
    public AutoFilterInfoRecord(final RecordInputStream in) {
        this._cEntries = in.readShort();
    }
    
    public void setNumEntries(final short num) {
        this._cEntries = num;
    }
    
    public short getNumEntries() {
        return this._cEntries;
    }
    
    @Override
    public String toString() {
        final StringBuffer buffer = new StringBuffer();
        buffer.append("[AUTOFILTERINFO]\n");
        buffer.append("    .numEntries          = ").append(this._cEntries).append("\n");
        buffer.append("[/AUTOFILTERINFO]\n");
        return buffer.toString();
    }
    
    public void serialize(final LittleEndianOutput out) {
        out.writeShort(this._cEntries);
    }
    
    @Override
    protected int getDataSize() {
        return 2;
    }
    
    @Override
    public short getSid() {
        return 157;
    }
    
    @Override
    public Object clone() {
        return this.cloneViaReserialise();
    }
}
