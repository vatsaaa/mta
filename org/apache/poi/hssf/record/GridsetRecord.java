// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hssf.record;

import org.apache.poi.util.LittleEndianOutput;

public final class GridsetRecord extends StandardRecord
{
    public static final short sid = 130;
    public short field_1_gridset_flag;
    
    public GridsetRecord() {
    }
    
    public GridsetRecord(final RecordInputStream in) {
        this.field_1_gridset_flag = in.readShort();
    }
    
    public void setGridset(final boolean gridset) {
        if (gridset) {
            this.field_1_gridset_flag = 1;
        }
        else {
            this.field_1_gridset_flag = 0;
        }
    }
    
    public boolean getGridset() {
        return this.field_1_gridset_flag == 1;
    }
    
    @Override
    public String toString() {
        final StringBuffer buffer = new StringBuffer();
        buffer.append("[GRIDSET]\n");
        buffer.append("    .gridset        = ").append(this.getGridset()).append("\n");
        buffer.append("[/GRIDSET]\n");
        return buffer.toString();
    }
    
    public void serialize(final LittleEndianOutput out) {
        out.writeShort(this.field_1_gridset_flag);
    }
    
    @Override
    protected int getDataSize() {
        return 2;
    }
    
    @Override
    public short getSid() {
        return 130;
    }
    
    @Override
    public Object clone() {
        final GridsetRecord rec = new GridsetRecord();
        rec.field_1_gridset_flag = this.field_1_gridset_flag;
        return rec;
    }
}
