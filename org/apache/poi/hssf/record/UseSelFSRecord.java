// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hssf.record;

import org.apache.poi.util.BitFieldFactory;
import org.apache.poi.util.LittleEndianOutput;
import org.apache.poi.util.HexDump;
import org.apache.poi.util.BitField;

public final class UseSelFSRecord extends StandardRecord
{
    public static final short sid = 352;
    private static final BitField useNaturalLanguageFormulasFlag;
    private int _options;
    
    private UseSelFSRecord(final int options) {
        this._options = options;
    }
    
    public UseSelFSRecord(final RecordInputStream in) {
        this(in.readUShort());
    }
    
    public UseSelFSRecord(final boolean b) {
        this(0);
        this._options = UseSelFSRecord.useNaturalLanguageFormulasFlag.setBoolean(this._options, b);
    }
    
    @Override
    public String toString() {
        final StringBuffer buffer = new StringBuffer();
        buffer.append("[USESELFS]\n");
        buffer.append("    .options = ").append(HexDump.shortToHex(this._options)).append("\n");
        buffer.append("[/USESELFS]\n");
        return buffer.toString();
    }
    
    public void serialize(final LittleEndianOutput out) {
        out.writeShort(this._options);
    }
    
    @Override
    protected int getDataSize() {
        return 2;
    }
    
    @Override
    public short getSid() {
        return 352;
    }
    
    @Override
    public Object clone() {
        return new UseSelFSRecord(this._options);
    }
    
    static {
        useNaturalLanguageFormulasFlag = BitFieldFactory.getInstance(1);
    }
}
