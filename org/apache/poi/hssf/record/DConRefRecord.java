// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hssf.record;

import org.apache.poi.util.ArrayUtil;
import org.apache.poi.util.LittleEndianOutput;
import org.apache.poi.util.LittleEndian;

public class DConRefRecord extends StandardRecord
{
    public static final short sid = 81;
    private int firstRow;
    private int lastRow;
    private int firstCol;
    private int lastCol;
    private int charCount;
    private int charType;
    private byte[] path;
    private byte[] _unused;
    
    public DConRefRecord(final byte[] data) {
        int offset = 0;
        if (LittleEndian.getShort(data, offset) != 81) {
            throw new RecordFormatException("incompatible sid.");
        }
        offset += 2;
        offset += 2;
        this.firstRow = LittleEndian.getUShort(data, offset);
        offset += 2;
        this.lastRow = LittleEndian.getUShort(data, offset);
        offset += 2;
        this.firstCol = LittleEndian.getUByte(data, offset);
        ++offset;
        this.lastCol = LittleEndian.getUByte(data, offset);
        ++offset;
        this.charCount = LittleEndian.getUShort(data, offset);
        offset += 2;
        if (this.charCount < 2) {
            throw new RecordFormatException("Character count must be >= 2");
        }
        this.charType = LittleEndian.getUByte(data, offset);
        ++offset;
        final int byteLength = this.charCount * ((this.charType & 0x1) + 1);
        this.path = LittleEndian.getByteArray(data, offset, byteLength);
        offset += byteLength;
        if (this.path[0] == 2) {
            this._unused = LittleEndian.getByteArray(data, offset, this.charType + 1);
        }
    }
    
    public DConRefRecord(final RecordInputStream inStream) {
        if (inStream.getSid() != 81) {
            throw new RecordFormatException("Wrong sid: " + inStream.getSid());
        }
        this.firstRow = inStream.readUShort();
        this.lastRow = inStream.readUShort();
        this.firstCol = inStream.readUByte();
        this.lastCol = inStream.readUByte();
        this.charCount = inStream.readUShort();
        this.charType = (inStream.readUByte() & 0x1);
        final int byteLength = this.charCount * (this.charType + 1);
        inStream.readFully(this.path = new byte[byteLength]);
        if (this.path[0] == 2) {
            this._unused = inStream.readRemainder();
        }
    }
    
    @Override
    protected int getDataSize() {
        int sz = 9 + this.path.length;
        if (this.path[0] == 2) {
            sz += this._unused.length;
        }
        return sz;
    }
    
    @Override
    protected void serialize(final LittleEndianOutput out) {
        out.writeShort(this.firstRow);
        out.writeShort(this.lastRow);
        out.writeByte(this.firstCol);
        out.writeByte(this.lastCol);
        out.writeShort(this.charCount);
        out.writeByte(this.charType);
        out.write(this.path);
        if (this.path[0] == 2) {
            out.write(this._unused);
        }
    }
    
    @Override
    public short getSid() {
        return 81;
    }
    
    public int getFirstColumn() {
        return this.firstCol;
    }
    
    public int getFirstRow() {
        return this.firstRow;
    }
    
    public int getLastColumn() {
        return this.lastCol;
    }
    
    public int getLastRow() {
        return this.lastRow;
    }
    
    @Override
    public String toString() {
        final StringBuilder b = new StringBuilder();
        b.append("[DCONREF]\n");
        b.append("    .ref\n");
        b.append("        .firstrow   = ").append(this.firstRow).append("\n");
        b.append("        .lastrow    = ").append(this.lastRow).append("\n");
        b.append("        .firstcol   = ").append(this.firstCol).append("\n");
        b.append("        .lastcol    = ").append(this.lastCol).append("\n");
        b.append("    .cch            = ").append(this.charCount).append("\n");
        b.append("    .stFile\n");
        b.append("        .h          = ").append(this.charType).append("\n");
        b.append("        .rgb        = ").append(this.getReadablePath()).append("\n");
        b.append("[/DCONREF]\n");
        return b.toString();
    }
    
    public byte[] getPath() {
        return ArrayUtil.copyOf(this.path, this.path.length);
    }
    
    public String getReadablePath() {
        if (this.path != null) {
            int offset;
            for (offset = 1; this.path[offset] < 32 && offset < this.path.length; ++offset) {}
            String out = new String(ArrayUtil.copyOfRange(this.path, offset, this.path.length));
            out = out.replaceAll("\u0003", "/");
            return out;
        }
        return null;
    }
    
    public boolean isExternalRef() {
        return this.path[0] == 1;
    }
}
