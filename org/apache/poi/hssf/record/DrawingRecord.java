// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hssf.record;

import org.apache.poi.util.LittleEndianOutput;

public final class DrawingRecord extends StandardRecord
{
    public static final short sid = 236;
    private static final byte[] EMPTY_BYTE_ARRAY;
    private byte[] recordData;
    private byte[] contd;
    
    public DrawingRecord() {
        this.recordData = DrawingRecord.EMPTY_BYTE_ARRAY;
    }
    
    public DrawingRecord(final RecordInputStream in) {
        this.recordData = in.readRemainder();
    }
    
    public void processContinueRecord(final byte[] record) {
        this.contd = record;
    }
    
    public void serialize(final LittleEndianOutput out) {
        out.write(this.recordData);
    }
    
    @Override
    protected int getDataSize() {
        return this.recordData.length;
    }
    
    @Override
    public short getSid() {
        return 236;
    }
    
    public byte[] getData() {
        if (this.contd != null) {
            final byte[] newBuffer = new byte[this.recordData.length + this.contd.length];
            System.arraycopy(this.recordData, 0, newBuffer, 0, this.recordData.length);
            System.arraycopy(this.contd, 0, newBuffer, this.recordData.length, this.contd.length);
            return newBuffer;
        }
        return this.recordData;
    }
    
    public void setData(final byte[] thedata) {
        if (thedata == null) {
            throw new IllegalArgumentException("data must not be null");
        }
        this.recordData = thedata;
    }
    
    @Override
    public Object clone() {
        final DrawingRecord rec = new DrawingRecord();
        rec.recordData = this.recordData.clone();
        if (this.contd != null) {
            rec.contd = this.contd.clone();
        }
        return rec;
    }
    
    static {
        EMPTY_BYTE_ARRAY = new byte[0];
    }
}
