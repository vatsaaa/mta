// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hssf.record;

import org.apache.poi.util.HexDump;
import java.util.Arrays;
import org.apache.poi.util.LittleEndianOutput;

public final class HeaderFooterRecord extends StandardRecord
{
    private static final byte[] BLANK_GUID;
    public static final short sid = 2204;
    private byte[] _rawData;
    
    public HeaderFooterRecord(final byte[] data) {
        this._rawData = data;
    }
    
    public HeaderFooterRecord(final RecordInputStream in) {
        this._rawData = in.readRemainder();
    }
    
    public void serialize(final LittleEndianOutput out) {
        out.write(this._rawData);
    }
    
    @Override
    protected int getDataSize() {
        return this._rawData.length;
    }
    
    @Override
    public short getSid() {
        return 2204;
    }
    
    public byte[] getGuid() {
        final byte[] guid = new byte[16];
        System.arraycopy(this._rawData, 12, guid, 0, guid.length);
        return guid;
    }
    
    public boolean isCurrentSheet() {
        return Arrays.equals(this.getGuid(), HeaderFooterRecord.BLANK_GUID);
    }
    
    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer();
        sb.append("[").append("HEADERFOOTER").append("] (0x");
        sb.append(Integer.toHexString(2204).toUpperCase() + ")\n");
        sb.append("  rawData=").append(HexDump.toHex(this._rawData)).append("\n");
        sb.append("[/").append("HEADERFOOTER").append("]\n");
        return sb.toString();
    }
    
    @Override
    public Object clone() {
        return this.cloneViaReserialise();
    }
    
    static {
        BLANK_GUID = new byte[16];
    }
}
