// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hssf.record;

import org.apache.poi.ss.formula.eval.ErrorEval;
import org.apache.poi.util.BitFieldFactory;
import org.apache.poi.util.HexDump;
import org.apache.poi.util.LittleEndianOutput;
import org.apache.poi.util.LittleEndianInput;
import org.apache.poi.ss.formula.ptg.Ptg;
import org.apache.poi.ss.formula.Formula;
import org.apache.poi.util.BitField;

public final class FormulaRecord extends CellRecord
{
    public static final short sid = 6;
    private static int FIXED_SIZE;
    private static final BitField alwaysCalc;
    private static final BitField calcOnLoad;
    private static final BitField sharedFormula;
    private double field_4_value;
    private short field_5_options;
    private int field_6_zero;
    private Formula field_8_parsed_expr;
    private SpecialCachedValue specialCachedValue;
    
    public FormulaRecord() {
        this.field_8_parsed_expr = Formula.create(Ptg.EMPTY_PTG_ARRAY);
    }
    
    public FormulaRecord(final RecordInputStream ris) {
        super(ris);
        final LittleEndianInput in = ris;
        final long valueLongBits = in.readLong();
        this.field_5_options = in.readShort();
        this.specialCachedValue = SpecialCachedValue.create(valueLongBits);
        if (this.specialCachedValue == null) {
            this.field_4_value = Double.longBitsToDouble(valueLongBits);
        }
        this.field_6_zero = in.readInt();
        final int field_7_expression_len = in.readShort();
        final int nBytesAvailable = in.available();
        this.field_8_parsed_expr = Formula.read(field_7_expression_len, in, nBytesAvailable);
    }
    
    public void setValue(final double value) {
        this.field_4_value = value;
        this.specialCachedValue = null;
    }
    
    public void setCachedResultTypeEmptyString() {
        this.specialCachedValue = SpecialCachedValue.createCachedEmptyValue();
    }
    
    public void setCachedResultTypeString() {
        this.specialCachedValue = SpecialCachedValue.createForString();
    }
    
    public void setCachedResultErrorCode(final int errorCode) {
        this.specialCachedValue = SpecialCachedValue.createCachedErrorCode(errorCode);
    }
    
    public void setCachedResultBoolean(final boolean value) {
        this.specialCachedValue = SpecialCachedValue.createCachedBoolean(value);
    }
    
    public boolean hasCachedResultString() {
        return this.specialCachedValue != null && this.specialCachedValue.getTypeCode() == 0;
    }
    
    public int getCachedResultType() {
        if (this.specialCachedValue == null) {
            return 0;
        }
        return this.specialCachedValue.getValueType();
    }
    
    public boolean getCachedBooleanValue() {
        return this.specialCachedValue.getBooleanValue();
    }
    
    public int getCachedErrorValue() {
        return this.specialCachedValue.getErrorValue();
    }
    
    public void setOptions(final short options) {
        this.field_5_options = options;
    }
    
    public double getValue() {
        return this.field_4_value;
    }
    
    public short getOptions() {
        return this.field_5_options;
    }
    
    public boolean isSharedFormula() {
        return FormulaRecord.sharedFormula.isSet(this.field_5_options);
    }
    
    public void setSharedFormula(final boolean flag) {
        this.field_5_options = FormulaRecord.sharedFormula.setShortBoolean(this.field_5_options, flag);
    }
    
    public boolean isAlwaysCalc() {
        return FormulaRecord.alwaysCalc.isSet(this.field_5_options);
    }
    
    public void setAlwaysCalc(final boolean flag) {
        this.field_5_options = FormulaRecord.alwaysCalc.setShortBoolean(this.field_5_options, flag);
    }
    
    public boolean isCalcOnLoad() {
        return FormulaRecord.calcOnLoad.isSet(this.field_5_options);
    }
    
    public void setCalcOnLoad(final boolean flag) {
        this.field_5_options = FormulaRecord.calcOnLoad.setShortBoolean(this.field_5_options, flag);
    }
    
    public Ptg[] getParsedExpression() {
        return this.field_8_parsed_expr.getTokens();
    }
    
    public Formula getFormula() {
        return this.field_8_parsed_expr;
    }
    
    public void setParsedExpression(final Ptg[] ptgs) {
        this.field_8_parsed_expr = Formula.create(ptgs);
    }
    
    @Override
    public short getSid() {
        return 6;
    }
    
    @Override
    protected int getValueDataSize() {
        return FormulaRecord.FIXED_SIZE + this.field_8_parsed_expr.getEncodedSize();
    }
    
    @Override
    protected void serializeValue(final LittleEndianOutput out) {
        if (this.specialCachedValue == null) {
            out.writeDouble(this.field_4_value);
        }
        else {
            this.specialCachedValue.serialize(out);
        }
        out.writeShort(this.getOptions());
        out.writeInt(this.field_6_zero);
        this.field_8_parsed_expr.serialize(out);
    }
    
    @Override
    protected String getRecordName() {
        return "FORMULA";
    }
    
    @Override
    protected void appendValueText(final StringBuilder sb) {
        sb.append("  .value\t = ");
        if (this.specialCachedValue == null) {
            sb.append(this.field_4_value).append("\n");
        }
        else {
            sb.append(this.specialCachedValue.formatDebugString()).append("\n");
        }
        sb.append("  .options   = ").append(HexDump.shortToHex(this.getOptions())).append("\n");
        sb.append("    .alwaysCalc= ").append(this.isAlwaysCalc()).append("\n");
        sb.append("    .calcOnLoad= ").append(this.isCalcOnLoad()).append("\n");
        sb.append("    .shared    = ").append(this.isSharedFormula()).append("\n");
        sb.append("  .zero      = ").append(HexDump.intToHex(this.field_6_zero)).append("\n");
        final Ptg[] ptgs = this.field_8_parsed_expr.getTokens();
        for (int k = 0; k < ptgs.length; ++k) {
            if (k > 0) {
                sb.append("\n");
            }
            sb.append("    Ptg[").append(k).append("]=");
            final Ptg ptg = ptgs[k];
            sb.append(ptg.toString()).append(ptg.getRVAType());
        }
    }
    
    @Override
    public Object clone() {
        final FormulaRecord rec = new FormulaRecord();
        this.copyBaseFields(rec);
        rec.field_4_value = this.field_4_value;
        rec.field_5_options = this.field_5_options;
        rec.field_6_zero = this.field_6_zero;
        rec.field_8_parsed_expr = this.field_8_parsed_expr;
        rec.specialCachedValue = this.specialCachedValue;
        return rec;
    }
    
    static {
        FormulaRecord.FIXED_SIZE = 14;
        alwaysCalc = BitFieldFactory.getInstance(1);
        calcOnLoad = BitFieldFactory.getInstance(2);
        sharedFormula = BitFieldFactory.getInstance(8);
    }
    
    private static final class SpecialCachedValue
    {
        private static final long BIT_MARKER = -281474976710656L;
        private static final int VARIABLE_DATA_LENGTH = 6;
        private static final int DATA_INDEX = 2;
        public static final int STRING = 0;
        public static final int BOOLEAN = 1;
        public static final int ERROR_CODE = 2;
        public static final int EMPTY = 3;
        private final byte[] _variableData;
        
        private SpecialCachedValue(final byte[] data) {
            this._variableData = data;
        }
        
        public int getTypeCode() {
            return this._variableData[0];
        }
        
        public static SpecialCachedValue create(final long valueLongBits) {
            if ((0xFFFF000000000000L & valueLongBits) != 0xFFFF000000000000L) {
                return null;
            }
            final byte[] result = new byte[6];
            long x = valueLongBits;
            for (int i = 0; i < 6; ++i) {
                result[i] = (byte)x;
                x >>= 8;
            }
            switch (result[0]) {
                case 0:
                case 1:
                case 2:
                case 3: {
                    return new SpecialCachedValue(result);
                }
                default: {
                    throw new RecordFormatException("Bad special value code (" + result[0] + ")");
                }
            }
        }
        
        public void serialize(final LittleEndianOutput out) {
            out.write(this._variableData);
            out.writeShort(65535);
        }
        
        public String formatDebugString() {
            return this.formatValue() + ' ' + HexDump.toHex(this._variableData);
        }
        
        private String formatValue() {
            final int typeCode = this.getTypeCode();
            switch (typeCode) {
                case 0: {
                    return "<string>";
                }
                case 1: {
                    return (this.getDataValue() == 0) ? "FALSE" : "TRUE";
                }
                case 2: {
                    return ErrorEval.getText(this.getDataValue());
                }
                case 3: {
                    return "<empty>";
                }
                default: {
                    return "#error(type=" + typeCode + ")#";
                }
            }
        }
        
        private int getDataValue() {
            return this._variableData[2];
        }
        
        public static SpecialCachedValue createCachedEmptyValue() {
            return create(3, 0);
        }
        
        public static SpecialCachedValue createForString() {
            return create(0, 0);
        }
        
        public static SpecialCachedValue createCachedBoolean(final boolean b) {
            return create(1, b ? 1 : 0);
        }
        
        public static SpecialCachedValue createCachedErrorCode(final int errorCode) {
            return create(2, errorCode);
        }
        
        private static SpecialCachedValue create(final int code, final int data) {
            final byte[] vd = { (byte)code, 0, (byte)data, 0, 0, 0 };
            return new SpecialCachedValue(vd);
        }
        
        @Override
        public String toString() {
            final StringBuffer sb = new StringBuffer(64);
            sb.append(this.getClass().getName());
            sb.append('[').append(this.formatValue()).append(']');
            return sb.toString();
        }
        
        public int getValueType() {
            final int typeCode = this.getTypeCode();
            switch (typeCode) {
                case 0: {
                    return 1;
                }
                case 1: {
                    return 4;
                }
                case 2: {
                    return 5;
                }
                case 3: {
                    return 1;
                }
                default: {
                    throw new IllegalStateException("Unexpected type id (" + typeCode + ")");
                }
            }
        }
        
        public boolean getBooleanValue() {
            if (this.getTypeCode() != 1) {
                throw new IllegalStateException("Not a boolean cached value - " + this.formatValue());
            }
            return this.getDataValue() != 0;
        }
        
        public int getErrorValue() {
            if (this.getTypeCode() != 2) {
                throw new IllegalStateException("Not an error cached value - " + this.formatValue());
            }
            return this.getDataValue();
        }
    }
}
