// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hssf.record.aggregates;

import org.apache.poi.hssf.record.Record;
import org.apache.poi.hssf.record.DVRecord;
import java.util.ArrayList;
import org.apache.poi.hssf.model.RecordStream;
import java.util.List;
import org.apache.poi.hssf.record.DVALRecord;

public final class DataValidityTable extends RecordAggregate
{
    private final DVALRecord _headerRec;
    private final List _validationList;
    
    public DataValidityTable(final RecordStream rs) {
        this._headerRec = (DVALRecord)rs.getNext();
        final List temp = new ArrayList();
        while (rs.peekNextClass() == DVRecord.class) {
            temp.add(rs.getNext());
        }
        this._validationList = temp;
    }
    
    public DataValidityTable() {
        this._headerRec = new DVALRecord();
        this._validationList = new ArrayList();
    }
    
    @Override
    public void visitContainedRecords(final RecordVisitor rv) {
        if (this._validationList.isEmpty()) {
            return;
        }
        rv.visitRecord(this._headerRec);
        for (int i = 0; i < this._validationList.size(); ++i) {
            rv.visitRecord(this._validationList.get(i));
        }
    }
    
    public void addDataValidation(final DVRecord dvRecord) {
        this._validationList.add(dvRecord);
        this._headerRec.setDVRecNo(this._validationList.size());
    }
}
