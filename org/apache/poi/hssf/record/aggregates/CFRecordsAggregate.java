// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hssf.record.aggregates;

import org.apache.poi.ss.formula.ptg.AreaErrPtg;
import org.apache.poi.ss.formula.ptg.AreaPtg;
import org.apache.poi.ss.formula.ptg.Ptg;
import org.apache.poi.ss.formula.FormulaShifter;
import org.apache.poi.hssf.record.Record;
import org.apache.poi.hssf.model.RecordStream;
import org.apache.poi.ss.util.CellRangeAddress;
import java.util.ArrayList;
import org.apache.poi.hssf.record.CFRuleRecord;
import java.util.List;
import org.apache.poi.hssf.record.CFHeaderRecord;

public final class CFRecordsAggregate extends RecordAggregate
{
    private static final int MAX_CONDTIONAL_FORMAT_RULES = 3;
    private final CFHeaderRecord header;
    private final List rules;
    
    private CFRecordsAggregate(final CFHeaderRecord pHeader, final CFRuleRecord[] pRules) {
        if (pHeader == null) {
            throw new IllegalArgumentException("header must not be null");
        }
        if (pRules == null) {
            throw new IllegalArgumentException("rules must not be null");
        }
        if (pRules.length > 3) {
            throw new IllegalArgumentException("No more than 3 rules may be specified");
        }
        if (pRules.length != pHeader.getNumberOfConditionalFormats()) {
            throw new RuntimeException("Mismatch number of rules");
        }
        this.header = pHeader;
        this.rules = new ArrayList(3);
        for (int i = 0; i < pRules.length; ++i) {
            this.rules.add(pRules[i]);
        }
    }
    
    public CFRecordsAggregate(final CellRangeAddress[] regions, final CFRuleRecord[] rules) {
        this(new CFHeaderRecord(regions, rules.length), rules);
    }
    
    public static CFRecordsAggregate createCFAggregate(final RecordStream rs) {
        final Record rec = rs.getNext();
        if (rec.getSid() != 432) {
            throw new IllegalStateException("next record sid was " + rec.getSid() + " instead of " + 432 + " as expected");
        }
        final CFHeaderRecord header = (CFHeaderRecord)rec;
        final int nRules = header.getNumberOfConditionalFormats();
        final CFRuleRecord[] rules = new CFRuleRecord[nRules];
        for (int i = 0; i < rules.length; ++i) {
            rules[i] = (CFRuleRecord)rs.getNext();
        }
        return new CFRecordsAggregate(header, rules);
    }
    
    public CFRecordsAggregate cloneCFAggregate() {
        final CFRuleRecord[] newRecs = new CFRuleRecord[this.rules.size()];
        for (int i = 0; i < newRecs.length; ++i) {
            newRecs[i] = (CFRuleRecord)this.getRule(i).clone();
        }
        return new CFRecordsAggregate((CFHeaderRecord)this.header.clone(), newRecs);
    }
    
    public CFHeaderRecord getHeader() {
        return this.header;
    }
    
    private void checkRuleIndex(final int idx) {
        if (idx < 0 || idx >= this.rules.size()) {
            throw new IllegalArgumentException("Bad rule record index (" + idx + ") nRules=" + this.rules.size());
        }
    }
    
    public CFRuleRecord getRule(final int idx) {
        this.checkRuleIndex(idx);
        return this.rules.get(idx);
    }
    
    public void setRule(final int idx, final CFRuleRecord r) {
        if (r == null) {
            throw new IllegalArgumentException("r must not be null");
        }
        this.checkRuleIndex(idx);
        this.rules.set(idx, r);
    }
    
    public void addRule(final CFRuleRecord r) {
        if (r == null) {
            throw new IllegalArgumentException("r must not be null");
        }
        if (this.rules.size() >= 3) {
            throw new IllegalStateException("Cannot have more than 3 conditional format rules");
        }
        this.rules.add(r);
        this.header.setNumberOfConditionalFormats(this.rules.size());
    }
    
    public int getNumberOfRules() {
        return this.rules.size();
    }
    
    @Override
    public String toString() {
        final StringBuffer buffer = new StringBuffer();
        buffer.append("[CF]\n");
        if (this.header != null) {
            buffer.append(this.header.toString());
        }
        for (int i = 0; i < this.rules.size(); ++i) {
            final CFRuleRecord cfRule = this.rules.get(i);
            buffer.append(cfRule.toString());
        }
        buffer.append("[/CF]\n");
        return buffer.toString();
    }
    
    @Override
    public void visitContainedRecords(final RecordVisitor rv) {
        rv.visitRecord(this.header);
        for (int i = 0; i < this.rules.size(); ++i) {
            final CFRuleRecord rule = this.rules.get(i);
            rv.visitRecord(rule);
        }
    }
    
    public boolean updateFormulasAfterCellShift(final FormulaShifter shifter, final int currentExternSheetIx) {
        final CellRangeAddress[] cellRanges = this.header.getCellRanges();
        boolean changed = false;
        final List temp = new ArrayList();
        for (int i = 0; i < cellRanges.length; ++i) {
            final CellRangeAddress craOld = cellRanges[i];
            final CellRangeAddress craNew = shiftRange(shifter, craOld, currentExternSheetIx);
            if (craNew == null) {
                changed = true;
            }
            else {
                temp.add(craNew);
                if (craNew != craOld) {
                    changed = true;
                }
            }
        }
        if (changed) {
            final int nRanges = temp.size();
            if (nRanges == 0) {
                return false;
            }
            final CellRangeAddress[] newRanges = new CellRangeAddress[nRanges];
            temp.toArray(newRanges);
            this.header.setCellRanges(newRanges);
        }
        for (int i = 0; i < this.rules.size(); ++i) {
            final CFRuleRecord rule = this.rules.get(i);
            Ptg[] ptgs = rule.getParsedExpression1();
            if (ptgs != null && shifter.adjustFormula(ptgs, currentExternSheetIx)) {
                rule.setParsedExpression1(ptgs);
            }
            ptgs = rule.getParsedExpression2();
            if (ptgs != null && shifter.adjustFormula(ptgs, currentExternSheetIx)) {
                rule.setParsedExpression2(ptgs);
            }
        }
        return true;
    }
    
    private static CellRangeAddress shiftRange(final FormulaShifter shifter, final CellRangeAddress cra, final int currentExternSheetIx) {
        final AreaPtg aptg = new AreaPtg(cra.getFirstRow(), cra.getLastRow(), cra.getFirstColumn(), cra.getLastColumn(), false, false, false, false);
        final Ptg[] ptgs = { aptg };
        if (!shifter.adjustFormula(ptgs, currentExternSheetIx)) {
            return cra;
        }
        final Ptg ptg0 = ptgs[0];
        if (ptg0 instanceof AreaPtg) {
            final AreaPtg bptg = (AreaPtg)ptg0;
            return new CellRangeAddress(bptg.getFirstRow(), bptg.getLastRow(), bptg.getFirstColumn(), bptg.getLastColumn());
        }
        if (ptg0 instanceof AreaErrPtg) {
            return null;
        }
        throw new IllegalStateException("Unexpected shifted ptg class (" + ptg0.getClass().getName() + ")");
    }
}
