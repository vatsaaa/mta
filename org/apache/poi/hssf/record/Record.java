// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hssf.record;

import java.io.InputStream;
import java.io.ByteArrayInputStream;

public abstract class Record extends RecordBase
{
    protected Record() {
    }
    
    public final byte[] serialize() {
        final byte[] retval = new byte[this.getRecordSize()];
        this.serialize(0, retval);
        return retval;
    }
    
    @Override
    public String toString() {
        return super.toString();
    }
    
    public abstract short getSid();
    
    public Object clone() {
        throw new RuntimeException("The class " + this.getClass().getName() + " needs to define a clone method");
    }
    
    public Record cloneViaReserialise() {
        final byte[] b = this.serialize();
        final RecordInputStream rinp = new RecordInputStream(new ByteArrayInputStream(b));
        rinp.nextRecord();
        final Record[] r = RecordFactory.createRecord(rinp);
        if (r.length != 1) {
            throw new IllegalStateException("Re-serialised a record to clone it, but got " + r.length + " records back!");
        }
        return r[0];
    }
}
