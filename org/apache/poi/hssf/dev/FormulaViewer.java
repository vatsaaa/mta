// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hssf.dev;

import org.apache.poi.hssf.model.HSSFFormulaParser;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.formula.ptg.Ptg;
import org.apache.poi.ss.formula.ptg.OperationPtg;
import org.apache.poi.ss.formula.ptg.ExpPtg;
import org.apache.poi.ss.formula.ptg.FuncPtg;
import java.util.List;
import org.apache.poi.hssf.record.FormulaRecord;
import org.apache.poi.hssf.record.Record;
import org.apache.poi.hssf.record.RecordFactory;
import java.io.InputStream;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import java.io.FileInputStream;

public class FormulaViewer
{
    private String file;
    private boolean list;
    
    public FormulaViewer() {
        this.list = false;
    }
    
    public void run() throws Exception {
        final POIFSFileSystem fs = new POIFSFileSystem(new FileInputStream(this.file));
        final List<Record> records = RecordFactory.createRecords(fs.createDocumentInputStream("Workbook"));
        for (int k = 0; k < records.size(); ++k) {
            final Record record = records.get(k);
            if (record.getSid() == 6) {
                if (this.list) {
                    this.listFormula((FormulaRecord)record);
                }
                else {
                    this.parseFormulaRecord((FormulaRecord)record);
                }
            }
        }
    }
    
    private void listFormula(final FormulaRecord record) {
        final String sep = "~";
        final Ptg[] tokens = record.getParsedExpression();
        final int numptgs = tokens.length;
        Ptg token = tokens[numptgs - 1];
        String numArg;
        if (token instanceof FuncPtg) {
            numArg = String.valueOf(numptgs - 1);
        }
        else {
            numArg = String.valueOf(-1);
        }
        final StringBuffer buf = new StringBuffer();
        if (token instanceof ExpPtg) {
            return;
        }
        buf.append(((OperationPtg)token).toFormulaString());
        buf.append(sep);
        switch (token.getPtgClass()) {
            case 0: {
                buf.append("REF");
                break;
            }
            case 32: {
                buf.append("VALUE");
                break;
            }
            case 64: {
                buf.append("ARRAY");
                break;
            }
        }
        buf.append(sep);
        if (numptgs > 1) {
            token = tokens[numptgs - 2];
            switch (token.getPtgClass()) {
                case 0: {
                    buf.append("REF");
                    break;
                }
                case 32: {
                    buf.append("VALUE");
                    break;
                }
                case 64: {
                    buf.append("ARRAY");
                    break;
                }
            }
        }
        else {
            buf.append("VALUE");
        }
        buf.append(sep);
        buf.append(numArg);
        System.out.println(buf.toString());
    }
    
    public void parseFormulaRecord(final FormulaRecord record) {
        System.out.println("==============================");
        System.out.print("row = " + record.getRow());
        System.out.println(", col = " + record.getColumn());
        System.out.println("value = " + record.getValue());
        System.out.print("xf = " + record.getXFIndex());
        System.out.print(", number of ptgs = " + record.getParsedExpression().length);
        System.out.println(", options = " + record.getOptions());
        System.out.println("RPN List = " + this.formulaString(record));
        System.out.println("Formula text = " + composeFormula(record));
    }
    
    private String formulaString(final FormulaRecord record) {
        final StringBuffer buf = new StringBuffer();
        final Ptg[] tokens = record.getParsedExpression();
        for (int i = 0; i < tokens.length; ++i) {
            final Ptg token = tokens[i];
            buf.append(token.toFormulaString());
            switch (token.getPtgClass()) {
                case 0: {
                    buf.append("(R)");
                    break;
                }
                case 32: {
                    buf.append("(V)");
                    break;
                }
                case 64: {
                    buf.append("(A)");
                    break;
                }
            }
            buf.append(' ');
        }
        return buf.toString();
    }
    
    private static String composeFormula(final FormulaRecord record) {
        return HSSFFormulaParser.toFormulaString(null, record.getParsedExpression());
    }
    
    public void setFile(final String file) {
        this.file = file;
    }
    
    public void setList(final boolean list) {
        this.list = list;
    }
    
    public static void main(final String[] args) {
        if (args == null || args.length > 2 || args[0].equals("--help")) {
            System.out.println("FormulaViewer .8 proof that the devil lies in the details (or just in BIFF8 files in general)");
            System.out.println("usage: Give me a big fat file name");
        }
        else if (args[0].equals("--listFunctions")) {
            try {
                final FormulaViewer viewer = new FormulaViewer();
                viewer.setFile(args[1]);
                viewer.setList(true);
                viewer.run();
            }
            catch (Exception e) {
                System.out.println("Whoops!");
                e.printStackTrace();
            }
        }
        else {
            try {
                final FormulaViewer viewer = new FormulaViewer();
                viewer.setFile(args[0]);
                viewer.run();
            }
            catch (Exception e) {
                System.out.println("Whoops!");
                e.printStackTrace();
            }
        }
    }
}
