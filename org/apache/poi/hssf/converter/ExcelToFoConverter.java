// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hssf.converter;

import org.apache.poi.util.POILogFactory;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.hpsf.SummaryInformation;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hwpf.converter.FontReplacer;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.Workbook;
import org.w3c.dom.Text;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.formula.eval.ErrorEval;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.w3c.dom.Element;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import org.w3c.dom.Document;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.TransformerFactory;
import java.io.Writer;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Node;
import javax.xml.transform.dom.DOMSource;
import java.io.FileWriter;
import java.io.File;
import org.apache.poi.hwpf.converter.FoDocumentFacade;
import org.apache.poi.util.POILogger;

public class ExcelToFoConverter extends AbstractExcelConverter
{
    private static final float CM_PER_INCH = 2.54f;
    private static final float DPI = 72.0f;
    private static final POILogger logger;
    private static final float PAPER_A4_HEIGHT_INCHES = 11.574803f;
    private static final float PAPER_A4_WIDTH_INCHES = 8.267716f;
    private final FoDocumentFacade foDocumentFacade;
    private float pageMarginInches;
    
    public static void main(final String[] args) {
        if (args.length < 2) {
            System.err.println("Usage: ExcelToFoConverter <inputFile.xls> <saveTo.xml>");
            return;
        }
        System.out.println("Converting " + args[0]);
        System.out.println("Saving output to " + args[1]);
        try {
            final Document doc = ExcelToHtmlConverter.process(new File(args[0]));
            final FileWriter out = new FileWriter(args[1]);
            final DOMSource domSource = new DOMSource(doc);
            final StreamResult streamResult = new StreamResult(out);
            final TransformerFactory tf = TransformerFactory.newInstance();
            final Transformer serializer = tf.newTransformer();
            serializer.setOutputProperty("encoding", "UTF-8");
            serializer.setOutputProperty("indent", "no");
            serializer.setOutputProperty("method", "xml");
            serializer.transform(domSource, streamResult);
            out.close();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public static Document process(final File xlsFile) throws Exception {
        final HSSFWorkbook workbook = AbstractExcelUtils.loadXls(xlsFile);
        final ExcelToFoConverter excelToHtmlConverter = new ExcelToFoConverter(DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument());
        excelToHtmlConverter.processWorkbook(workbook);
        return excelToHtmlConverter.getDocument();
    }
    
    public ExcelToFoConverter(final Document document) {
        this.pageMarginInches = 0.4f;
        this.foDocumentFacade = new FoDocumentFacade(document);
    }
    
    public ExcelToFoConverter(final FoDocumentFacade foDocumentFacade) {
        this.pageMarginInches = 0.4f;
        this.foDocumentFacade = foDocumentFacade;
    }
    
    protected String createPageMaster(final float tableWidthIn, final String pageMasterName) {
        final float requiredWidthIn = tableWidthIn + 2.0f * this.getPageMarginInches();
        float paperWidthIn;
        float paperHeightIn;
        if (requiredWidthIn < 8.267716f) {
            paperWidthIn = 8.267716f;
            paperHeightIn = 11.574803f;
        }
        else {
            paperWidthIn = requiredWidthIn;
            paperHeightIn = paperWidthIn * 0.7142857f;
        }
        final float leftMargin = this.getPageMarginInches();
        final float rightMargin = this.getPageMarginInches();
        final float topMargin = this.getPageMarginInches();
        final float bottomMargin = this.getPageMarginInches();
        final Element pageMaster = this.foDocumentFacade.addSimplePageMaster(pageMasterName);
        pageMaster.setAttribute("page-height", paperHeightIn + "in");
        pageMaster.setAttribute("page-width", paperWidthIn + "in");
        final Element regionBody = this.foDocumentFacade.addRegionBody(pageMaster);
        regionBody.setAttribute("margin", topMargin + "in " + rightMargin + "in " + bottomMargin + "in " + leftMargin + "in");
        return pageMasterName;
    }
    
    @Override
    protected Document getDocument() {
        return this.foDocumentFacade.getDocument();
    }
    
    public float getPageMarginInches() {
        return this.pageMarginInches;
    }
    
    protected boolean isEmptyStyle(final CellStyle cellStyle) {
        return cellStyle.getFillPattern() == 0 && cellStyle.getBorderTop() == 0 && cellStyle.getBorderRight() == 0 && cellStyle.getBorderBottom() == 0 && cellStyle.getBorderLeft() == 0;
    }
    
    protected boolean processCell(final HSSFWorkbook workbook, final HSSFCell cell, final Element tableCellElement, final int normalWidthPx, final int maxSpannedWidthPx, final float normalHeightPt) {
        final HSSFCellStyle cellStyle = cell.getCellStyle();
        String value = null;
        switch (cell.getCellType()) {
            case 1: {
                value = cell.getRichStringCellValue().getString();
                break;
            }
            case 2: {
                switch (cell.getCachedFormulaResultType()) {
                    case 1: {
                        final HSSFRichTextString str = cell.getRichStringCellValue();
                        if (str != null && str.length() > 0) {
                            value = str.toString();
                            break;
                        }
                        value = "";
                        break;
                    }
                    case 0: {
                        final HSSFCellStyle style = cellStyle;
                        if (style == null) {
                            value = String.valueOf(cell.getNumericCellValue());
                            break;
                        }
                        value = this._formatter.formatRawCellContents(cell.getNumericCellValue(), style.getDataFormat(), style.getDataFormatString());
                        break;
                    }
                    case 4: {
                        value = String.valueOf(cell.getBooleanCellValue());
                        break;
                    }
                    case 5: {
                        value = ErrorEval.getText(cell.getErrorCellValue());
                        break;
                    }
                    default: {
                        ExcelToFoConverter.logger.log(POILogger.WARN, "Unexpected cell cachedFormulaResultType (" + cell.getCachedFormulaResultType() + ")");
                        value = "";
                        break;
                    }
                }
                break;
            }
            case 3: {
                value = "";
                break;
            }
            case 0: {
                value = this._formatter.formatCellValue(cell);
                break;
            }
            case 4: {
                value = String.valueOf(cell.getBooleanCellValue());
                break;
            }
            case 5: {
                value = ErrorEval.getText(cell.getErrorCellValue());
                break;
            }
            default: {
                ExcelToFoConverter.logger.log(POILogger.WARN, "Unexpected cell type (" + cell.getCellType() + ")");
                return true;
            }
        }
        final boolean noText = AbstractExcelUtils.isEmpty(value);
        final boolean wrapInDivs = !noText && !cellStyle.getWrapText();
        final boolean emptyStyle = this.isEmptyStyle(cellStyle);
        if (!emptyStyle && noText) {
            value = " ";
        }
        if (this.isOutputLeadingSpacesAsNonBreaking() && value.startsWith(" ")) {
            final StringBuilder builder = new StringBuilder();
            for (int c = 0; c < value.length() && value.charAt(c) == ' '; ++c) {
                builder.append(' ');
            }
            if (value.length() != builder.length()) {
                builder.append(value.substring(builder.length()));
            }
            value = builder.toString();
        }
        final Text text = this.foDocumentFacade.createText(value);
        final Element block = this.foDocumentFacade.createBlock();
        if (wrapInDivs) {
            block.setAttribute("absolute-position", "fixed");
            block.setAttribute("left", "0px");
            block.setAttribute("top", "0px");
            block.setAttribute("bottom", "0px");
            block.setAttribute("min-width", normalWidthPx + "px");
            if (maxSpannedWidthPx != Integer.MAX_VALUE) {
                block.setAttribute("max-width", maxSpannedWidthPx + "px");
            }
            block.setAttribute("overflow", "hidden");
            block.setAttribute("height", normalHeightPt + "pt");
            block.setAttribute("keep-together.within-line", "always");
            block.setAttribute("wrap-option", "no-wrap");
        }
        this.processCellStyle(workbook, cell.getCellStyle(), tableCellElement, block);
        block.appendChild(text);
        tableCellElement.appendChild(block);
        return AbstractExcelUtils.isEmpty(value) && emptyStyle;
    }
    
    protected void processCellStyle(final HSSFWorkbook workbook, final HSSFCellStyle cellStyle, final Element cellTarget, final Element blockTarget) {
        blockTarget.setAttribute("white-space-collapse", "false");
        final String textAlign = AbstractExcelUtils.getAlign(cellStyle.getAlignment());
        if (AbstractExcelUtils.isNotEmpty(textAlign)) {
            blockTarget.setAttribute("text-align", textAlign);
        }
        if (cellStyle.getFillPattern() != 0) {
            if (cellStyle.getFillPattern() == 1) {
                final HSSFColor foregroundColor = cellStyle.getFillForegroundColorColor();
                if (foregroundColor != null) {
                    cellTarget.setAttribute("background-color", AbstractExcelUtils.getColor(foregroundColor));
                }
            }
            else {
                final HSSFColor backgroundColor = cellStyle.getFillBackgroundColorColor();
                if (backgroundColor != null) {
                    cellTarget.setAttribute("background-color", AbstractExcelUtils.getColor(backgroundColor));
                }
            }
        }
        this.processCellStyleBorder(workbook, cellTarget, "top", cellStyle.getBorderTop(), cellStyle.getTopBorderColor());
        this.processCellStyleBorder(workbook, cellTarget, "right", cellStyle.getBorderRight(), cellStyle.getRightBorderColor());
        this.processCellStyleBorder(workbook, cellTarget, "bottom", cellStyle.getBorderBottom(), cellStyle.getBottomBorderColor());
        this.processCellStyleBorder(workbook, cellTarget, "left", cellStyle.getBorderLeft(), cellStyle.getLeftBorderColor());
        final HSSFFont font = cellStyle.getFont(workbook);
        this.processCellStyleFont(workbook, blockTarget, font);
    }
    
    protected void processCellStyleBorder(final HSSFWorkbook workbook, final Element cellTarget, final String type, final short xlsBorder, final short borderColor) {
        if (xlsBorder == 0) {
            return;
        }
        final StringBuilder borderStyle = new StringBuilder();
        borderStyle.append(AbstractExcelUtils.getBorderWidth(xlsBorder));
        final HSSFColor color = workbook.getCustomPalette().getColor(borderColor);
        if (color != null) {
            borderStyle.append(' ');
            borderStyle.append(AbstractExcelUtils.getColor(color));
            borderStyle.append(' ');
            borderStyle.append(AbstractExcelUtils.getBorderStyle(xlsBorder));
        }
        cellTarget.setAttribute("border-" + type, borderStyle.toString());
    }
    
    protected void processCellStyleFont(final HSSFWorkbook workbook, final Element blockTarget, final HSSFFont font) {
        final FontReplacer.Triplet triplet = new FontReplacer.Triplet();
        triplet.fontName = font.getFontName();
        switch (font.getBoldweight()) {
            case 700: {
                triplet.bold = true;
                break;
            }
            case 400: {
                triplet.bold = false;
                break;
            }
        }
        if (font.getItalic()) {
            triplet.italic = true;
        }
        this.getFontReplacer().update(triplet);
        this.setBlockProperties(blockTarget, triplet);
        final HSSFColor fontColor = workbook.getCustomPalette().getColor(font.getColor());
        if (fontColor != null) {
            blockTarget.setAttribute("color", AbstractExcelUtils.getColor(fontColor));
        }
        if (font.getFontHeightInPoints() != 0) {
            blockTarget.setAttribute("font-size", font.getFontHeightInPoints() + "pt");
        }
    }
    
    protected void processColumnHeaders(final HSSFSheet sheet, final int maxSheetColumns, final Element table) {
        final Element tableHeader = this.foDocumentFacade.createTableHeader();
        final Element row = this.foDocumentFacade.createTableRow();
        if (this.isOutputRowNumbers()) {
            final Element tableCellElement = this.foDocumentFacade.createTableCell();
            tableCellElement.appendChild(this.foDocumentFacade.createBlock());
            row.appendChild(tableCellElement);
        }
        for (int c = 0; c < maxSheetColumns; ++c) {
            if (this.isOutputHiddenColumns() || !sheet.isColumnHidden(c)) {
                final Element cell = this.foDocumentFacade.createTableCell();
                final Element block = this.foDocumentFacade.createBlock();
                block.setAttribute("text-align", "center");
                block.setAttribute("font-weight", "bold");
                final String text = this.getColumnName(c);
                block.appendChild(this.foDocumentFacade.createText(text));
                cell.appendChild(block);
                row.appendChild(cell);
            }
        }
        tableHeader.appendChild(row);
        table.appendChild(tableHeader);
    }
    
    protected float processColumnWidths(final HSSFSheet sheet, final int maxSheetColumns, final Element table) {
        float tableWidth = 0.0f;
        if (this.isOutputRowNumbers()) {
            final float columnWidthIn = AbstractExcelConverter.getDefaultColumnWidth(sheet) / 72.0f;
            final Element rowNumberColumn = this.foDocumentFacade.createTableColumn();
            rowNumberColumn.setAttribute("column-width", columnWidthIn + "in");
            table.appendChild(rowNumberColumn);
            tableWidth += columnWidthIn;
        }
        for (int c = 0; c < maxSheetColumns; ++c) {
            if (this.isOutputHiddenColumns() || !sheet.isColumnHidden(c)) {
                final float columnWidthIn2 = AbstractExcelConverter.getColumnWidth(sheet, c) / 72.0f;
                final Element col = this.foDocumentFacade.createTableColumn();
                col.setAttribute("column-width", columnWidthIn2 + "in");
                table.appendChild(col);
                tableWidth += columnWidthIn2;
            }
        }
        table.setAttribute("width", tableWidth + "in");
        return tableWidth;
    }
    
    protected void processDocumentInformation(final SummaryInformation summaryInformation) {
        if (AbstractExcelUtils.isNotEmpty(summaryInformation.getTitle())) {
            this.foDocumentFacade.setTitle(summaryInformation.getTitle());
        }
        if (AbstractExcelUtils.isNotEmpty(summaryInformation.getAuthor())) {
            this.foDocumentFacade.setCreator(summaryInformation.getAuthor());
        }
        if (AbstractExcelUtils.isNotEmpty(summaryInformation.getKeywords())) {
            this.foDocumentFacade.setKeywords(summaryInformation.getKeywords());
        }
        if (AbstractExcelUtils.isNotEmpty(summaryInformation.getComments())) {
            this.foDocumentFacade.setDescription(summaryInformation.getComments());
        }
    }
    
    protected int processRow(final HSSFWorkbook workbook, final CellRangeAddress[][] mergedRanges, final HSSFRow row, final Element tableRowElement) {
        final HSSFSheet sheet = row.getSheet();
        final short maxColIx = row.getLastCellNum();
        if (maxColIx <= 0) {
            return 0;
        }
        final List<Element> emptyCells = new ArrayList<Element>(maxColIx);
        if (this.isOutputRowNumbers()) {
            final Element tableRowNumberCellElement = this.processRowNumber(row);
            emptyCells.add(tableRowNumberCellElement);
        }
        int maxRenderedColumn = 0;
        for (int colIx = 0; colIx < maxColIx; ++colIx) {
            if (this.isOutputHiddenColumns() || !sheet.isColumnHidden(colIx)) {
                final CellRangeAddress range = AbstractExcelUtils.getMergedRange(mergedRanges, row.getRowNum(), colIx);
                if (range != null) {
                    if (range.getFirstColumn() != colIx) {
                        continue;
                    }
                    if (range.getFirstRow() != row.getRowNum()) {
                        continue;
                    }
                }
                final HSSFCell cell = row.getCell(colIx);
                int divWidthPx = 0;
                divWidthPx = AbstractExcelConverter.getColumnWidth(sheet, colIx);
                boolean hasBreaks = false;
                for (int nextColumnIndex = colIx + 1; nextColumnIndex < maxColIx; ++nextColumnIndex) {
                    if (this.isOutputHiddenColumns() || !sheet.isColumnHidden(nextColumnIndex)) {
                        if (row.getCell(nextColumnIndex) != null && !this.isTextEmpty(row.getCell(nextColumnIndex))) {
                            hasBreaks = true;
                            break;
                        }
                        divWidthPx += AbstractExcelConverter.getColumnWidth(sheet, nextColumnIndex);
                    }
                }
                if (!hasBreaks) {
                    divWidthPx = Integer.MAX_VALUE;
                }
                final Element tableCellElement = this.foDocumentFacade.createTableCell();
                if (range != null) {
                    if (range.getFirstColumn() != range.getLastColumn()) {
                        tableCellElement.setAttribute("number-columns-spanned", String.valueOf(range.getLastColumn() - range.getFirstColumn() + 1));
                    }
                    if (range.getFirstRow() != range.getLastRow()) {
                        tableCellElement.setAttribute("number-rows-spanned", String.valueOf(range.getLastRow() - range.getFirstRow() + 1));
                    }
                }
                boolean emptyCell;
                if (cell != null) {
                    emptyCell = this.processCell(workbook, cell, tableCellElement, AbstractExcelConverter.getColumnWidth(sheet, colIx), divWidthPx, row.getHeight() / 20.0f);
                }
                else {
                    tableCellElement.appendChild(this.foDocumentFacade.createBlock());
                    emptyCell = true;
                }
                if (emptyCell) {
                    emptyCells.add(tableCellElement);
                }
                else {
                    for (final Element emptyCellElement : emptyCells) {
                        tableRowElement.appendChild(emptyCellElement);
                    }
                    emptyCells.clear();
                    tableRowElement.appendChild(tableCellElement);
                    maxRenderedColumn = colIx;
                }
            }
        }
        return maxRenderedColumn + 1;
    }
    
    protected Element processRowNumber(final HSSFRow row) {
        final Element tableRowNumberCellElement = this.foDocumentFacade.createTableCell();
        final Element block = this.foDocumentFacade.createBlock();
        block.setAttribute("text-align", "right");
        block.setAttribute("font-weight", "bold");
        final Text text = this.foDocumentFacade.createText(this.getRowName(row));
        block.appendChild(text);
        tableRowNumberCellElement.appendChild(block);
        return tableRowNumberCellElement;
    }
    
    protected float processSheet(final HSSFWorkbook workbook, final HSSFSheet sheet, final Element flow) {
        final int physicalNumberOfRows = sheet.getPhysicalNumberOfRows();
        if (physicalNumberOfRows <= 0) {
            return 0.0f;
        }
        this.processSheetName(sheet, flow);
        final Element table = this.foDocumentFacade.createTable();
        table.setAttribute("table-layout", "fixed");
        final Element tableBody = this.foDocumentFacade.createTableBody();
        final CellRangeAddress[][] mergedRanges = ExcelToHtmlUtils.buildMergedRangesMap(sheet);
        final List<Element> emptyRowElements = new ArrayList<Element>(physicalNumberOfRows);
        int maxSheetColumns = 1;
        for (int r = sheet.getFirstRowNum(); r <= sheet.getLastRowNum(); ++r) {
            final HSSFRow row = sheet.getRow(r);
            if (row != null) {
                if (this.isOutputHiddenRows() || !row.getZeroHeight()) {
                    final Element tableRowElement = this.foDocumentFacade.createTableRow();
                    tableRowElement.setAttribute("height", row.getHeight() / 20.0f + "pt");
                    final int maxRowColumnNumber = this.processRow(workbook, mergedRanges, row, tableRowElement);
                    if (tableRowElement.getChildNodes().getLength() == 0) {
                        final Element emptyCellElement = this.foDocumentFacade.createTableCell();
                        emptyCellElement.appendChild(this.foDocumentFacade.createBlock());
                        tableRowElement.appendChild(emptyCellElement);
                    }
                    if (maxRowColumnNumber == 0) {
                        emptyRowElements.add(tableRowElement);
                    }
                    else {
                        if (!emptyRowElements.isEmpty()) {
                            for (final Element emptyRowElement : emptyRowElements) {
                                tableBody.appendChild(emptyRowElement);
                            }
                            emptyRowElements.clear();
                        }
                        tableBody.appendChild(tableRowElement);
                    }
                    maxSheetColumns = Math.max(maxSheetColumns, maxRowColumnNumber);
                }
            }
        }
        final float tableWidthIn = this.processColumnWidths(sheet, maxSheetColumns, table);
        if (this.isOutputColumnHeaders()) {
            this.processColumnHeaders(sheet, maxSheetColumns, table);
        }
        table.appendChild(tableBody);
        flow.appendChild(table);
        return tableWidthIn;
    }
    
    protected boolean processSheet(final HSSFWorkbook workbook, final int sheetIndex) {
        final String pageMasterName = "sheet-" + sheetIndex;
        final Element pageSequence = this.foDocumentFacade.createPageSequence(pageMasterName);
        final Element flow = this.foDocumentFacade.addFlowToPageSequence(pageSequence, "xsl-region-body");
        final HSSFSheet sheet = workbook.getSheetAt(sheetIndex);
        final float tableWidthIn = this.processSheet(workbook, sheet, flow);
        if (tableWidthIn == 0.0f) {
            return false;
        }
        this.createPageMaster(tableWidthIn, pageMasterName);
        this.foDocumentFacade.addPageSequence(pageSequence);
        return true;
    }
    
    protected void processSheetName(final HSSFSheet sheet, final Element flow) {
        final Element titleBlock = this.foDocumentFacade.createBlock();
        final FontReplacer.Triplet triplet = new FontReplacer.Triplet();
        triplet.bold = true;
        triplet.italic = false;
        triplet.fontName = "Arial";
        this.getFontReplacer().update(triplet);
        this.setBlockProperties(titleBlock, triplet);
        titleBlock.setAttribute("font-size", "200%");
        final Element titleInline = this.foDocumentFacade.createInline();
        titleInline.appendChild(this.foDocumentFacade.createText(sheet.getSheetName()));
        titleBlock.appendChild(titleInline);
        flow.appendChild(titleBlock);
        final Element titleBlock2 = this.foDocumentFacade.createBlock();
        final Element titleInline2 = this.foDocumentFacade.createInline();
        titleBlock2.appendChild(titleInline2);
        flow.appendChild(titleBlock2);
    }
    
    public void processWorkbook(final HSSFWorkbook workbook) {
        final SummaryInformation summaryInformation = workbook.getSummaryInformation();
        if (summaryInformation != null) {
            this.processDocumentInformation(summaryInformation);
        }
        for (int s = 0; s < workbook.getNumberOfSheets(); ++s) {
            this.processSheet(workbook, s);
        }
    }
    
    private void setBlockProperties(final Element textBlock, final FontReplacer.Triplet triplet) {
        if (triplet.bold) {
            textBlock.setAttribute("font-weight", "bold");
        }
        if (triplet.italic) {
            textBlock.setAttribute("font-style", "italic");
        }
        if (AbstractExcelUtils.isNotEmpty(triplet.fontName)) {
            textBlock.setAttribute("font-family", triplet.fontName);
        }
    }
    
    public void setPageMarginInches(final float pageMarginInches) {
        this.pageMarginInches = pageMarginInches;
    }
    
    static {
        logger = POILogFactory.getLogger(ExcelToFoConverter.class);
    }
}
