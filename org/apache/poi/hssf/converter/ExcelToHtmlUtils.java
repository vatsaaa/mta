// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hssf.converter;

import java.util.Arrays;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.hssf.usermodel.HSSFSheet;

public class ExcelToHtmlUtils extends AbstractExcelUtils
{
    public static void appendAlign(final StringBuilder style, final short alignment) {
        final String cssAlign = AbstractExcelUtils.getAlign(alignment);
        if (AbstractExcelUtils.isEmpty(cssAlign)) {
            return;
        }
        style.append("text-align:");
        style.append(cssAlign);
        style.append(";");
    }
    
    public static CellRangeAddress[][] buildMergedRangesMap(final HSSFSheet sheet) {
        CellRangeAddress[][] mergedRanges = { null };
        for (int m = 0; m < sheet.getNumMergedRegions(); ++m) {
            final CellRangeAddress cellRangeAddress = sheet.getMergedRegion(m);
            final int requiredHeight = cellRangeAddress.getLastRow() + 1;
            if (mergedRanges.length < requiredHeight) {
                final CellRangeAddress[][] newArray = new CellRangeAddress[requiredHeight][];
                System.arraycopy(mergedRanges, 0, newArray, 0, mergedRanges.length);
                mergedRanges = newArray;
            }
            for (int r = cellRangeAddress.getFirstRow(); r <= cellRangeAddress.getLastRow(); ++r) {
                final int requiredWidth = cellRangeAddress.getLastColumn() + 1;
                CellRangeAddress[] rowMerged = mergedRanges[r];
                if (rowMerged == null) {
                    rowMerged = new CellRangeAddress[requiredWidth];
                    mergedRanges[r] = rowMerged;
                }
                else {
                    final int rowMergedLength = rowMerged.length;
                    if (rowMergedLength < requiredWidth) {
                        final CellRangeAddress[] newRow = new CellRangeAddress[requiredWidth];
                        System.arraycopy(rowMerged, 0, newRow, 0, rowMergedLength);
                        mergedRanges[r] = newRow;
                        rowMerged = newRow;
                    }
                }
                Arrays.fill(rowMerged, cellRangeAddress.getFirstColumn(), cellRangeAddress.getLastColumn() + 1, cellRangeAddress);
            }
        }
        return mergedRanges;
    }
}
