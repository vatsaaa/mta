// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hssf.converter;

import org.apache.poi.util.POILogFactory;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.hpsf.SummaryInformation;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.w3c.dom.Text;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.formula.eval.ErrorEval;
import org.w3c.dom.Element;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import java.util.LinkedHashMap;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import org.w3c.dom.Document;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.TransformerFactory;
import java.io.Writer;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Node;
import javax.xml.transform.dom.DOMSource;
import java.io.FileWriter;
import java.io.File;
import org.apache.poi.hwpf.converter.HtmlDocumentFacade;
import java.util.Map;
import org.apache.poi.util.POILogger;

public class ExcelToHtmlConverter extends AbstractExcelConverter
{
    private static final POILogger logger;
    private String cssClassContainerCell;
    private String cssClassContainerDiv;
    private String cssClassPrefixCell;
    private String cssClassPrefixDiv;
    private String cssClassPrefixRow;
    private String cssClassPrefixTable;
    private Map<Short, String> excelStyleToClass;
    private final HtmlDocumentFacade htmlDocumentFacade;
    private boolean useDivsToSpan;
    
    public static void main(final String[] args) {
        if (args.length < 2) {
            System.err.println("Usage: ExcelToHtmlConverter <inputFile.xls> <saveTo.html>");
            return;
        }
        System.out.println("Converting " + args[0]);
        System.out.println("Saving output to " + args[1]);
        try {
            final Document doc = process(new File(args[0]));
            final FileWriter out = new FileWriter(args[1]);
            final DOMSource domSource = new DOMSource(doc);
            final StreamResult streamResult = new StreamResult(out);
            final TransformerFactory tf = TransformerFactory.newInstance();
            final Transformer serializer = tf.newTransformer();
            serializer.setOutputProperty("encoding", "UTF-8");
            serializer.setOutputProperty("indent", "no");
            serializer.setOutputProperty("method", "html");
            serializer.transform(domSource, streamResult);
            out.close();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public static Document process(final File xlsFile) throws Exception {
        final HSSFWorkbook workbook = AbstractExcelUtils.loadXls(xlsFile);
        final ExcelToHtmlConverter excelToHtmlConverter = new ExcelToHtmlConverter(DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument());
        excelToHtmlConverter.processWorkbook(workbook);
        return excelToHtmlConverter.getDocument();
    }
    
    public ExcelToHtmlConverter(final Document doc) {
        this.cssClassContainerCell = null;
        this.cssClassContainerDiv = null;
        this.cssClassPrefixCell = "c";
        this.cssClassPrefixDiv = "d";
        this.cssClassPrefixRow = "r";
        this.cssClassPrefixTable = "t";
        this.excelStyleToClass = new LinkedHashMap<Short, String>();
        this.useDivsToSpan = false;
        this.htmlDocumentFacade = new HtmlDocumentFacade(doc);
    }
    
    public ExcelToHtmlConverter(final HtmlDocumentFacade htmlDocumentFacade) {
        this.cssClassContainerCell = null;
        this.cssClassContainerDiv = null;
        this.cssClassPrefixCell = "c";
        this.cssClassPrefixDiv = "d";
        this.cssClassPrefixRow = "r";
        this.cssClassPrefixTable = "t";
        this.excelStyleToClass = new LinkedHashMap<Short, String>();
        this.useDivsToSpan = false;
        this.htmlDocumentFacade = htmlDocumentFacade;
    }
    
    protected String buildStyle(final HSSFWorkbook workbook, final HSSFCellStyle cellStyle) {
        final StringBuilder style = new StringBuilder();
        style.append("white-space:pre-wrap;");
        ExcelToHtmlUtils.appendAlign(style, cellStyle.getAlignment());
        if (cellStyle.getFillPattern() != 0) {
            if (cellStyle.getFillPattern() == 1) {
                final HSSFColor foregroundColor = cellStyle.getFillForegroundColorColor();
                if (foregroundColor != null) {
                    style.append("background-color:" + AbstractExcelUtils.getColor(foregroundColor) + ";");
                }
            }
            else {
                final HSSFColor backgroundColor = cellStyle.getFillBackgroundColorColor();
                if (backgroundColor != null) {
                    style.append("background-color:" + AbstractExcelUtils.getColor(backgroundColor) + ";");
                }
            }
        }
        this.buildStyle_border(workbook, style, "top", cellStyle.getBorderTop(), cellStyle.getTopBorderColor());
        this.buildStyle_border(workbook, style, "right", cellStyle.getBorderRight(), cellStyle.getRightBorderColor());
        this.buildStyle_border(workbook, style, "bottom", cellStyle.getBorderBottom(), cellStyle.getBottomBorderColor());
        this.buildStyle_border(workbook, style, "left", cellStyle.getBorderLeft(), cellStyle.getLeftBorderColor());
        final HSSFFont font = cellStyle.getFont(workbook);
        this.buildStyle_font(workbook, style, font);
        return style.toString();
    }
    
    private void buildStyle_border(final HSSFWorkbook workbook, final StringBuilder style, final String type, final short xlsBorder, final short borderColor) {
        if (xlsBorder == 0) {
            return;
        }
        final StringBuilder borderStyle = new StringBuilder();
        borderStyle.append(AbstractExcelUtils.getBorderWidth(xlsBorder));
        borderStyle.append(' ');
        borderStyle.append(AbstractExcelUtils.getBorderStyle(xlsBorder));
        final HSSFColor color = workbook.getCustomPalette().getColor(borderColor);
        if (color != null) {
            borderStyle.append(' ');
            borderStyle.append(AbstractExcelUtils.getColor(color));
        }
        style.append("border-" + type + ":" + (Object)borderStyle + ";");
    }
    
    void buildStyle_font(final HSSFWorkbook workbook, final StringBuilder style, final HSSFFont font) {
        switch (font.getBoldweight()) {
            case 700: {
                style.append("font-weight:bold;");
                break;
            }
        }
        final HSSFColor fontColor = workbook.getCustomPalette().getColor(font.getColor());
        if (fontColor != null) {
            style.append("color: " + AbstractExcelUtils.getColor(fontColor) + "; ");
        }
        if (font.getFontHeightInPoints() != 0) {
            style.append("font-size:" + font.getFontHeightInPoints() + "pt;");
        }
        if (font.getItalic()) {
            style.append("font-style:italic;");
        }
    }
    
    public String getCssClassPrefixCell() {
        return this.cssClassPrefixCell;
    }
    
    public String getCssClassPrefixDiv() {
        return this.cssClassPrefixDiv;
    }
    
    public String getCssClassPrefixRow() {
        return this.cssClassPrefixRow;
    }
    
    public String getCssClassPrefixTable() {
        return this.cssClassPrefixTable;
    }
    
    public Document getDocument() {
        return this.htmlDocumentFacade.getDocument();
    }
    
    protected String getStyleClassName(final HSSFWorkbook workbook, final HSSFCellStyle cellStyle) {
        final Short cellStyleKey = cellStyle.getIndex();
        final String knownClass = this.excelStyleToClass.get(cellStyleKey);
        if (knownClass != null) {
            return knownClass;
        }
        final String cssStyle = this.buildStyle(workbook, cellStyle);
        final String cssClass = this.htmlDocumentFacade.getOrCreateCssClass(this.cssClassPrefixCell, cssStyle);
        this.excelStyleToClass.put(cellStyleKey, cssClass);
        return cssClass;
    }
    
    public boolean isUseDivsToSpan() {
        return this.useDivsToSpan;
    }
    
    protected boolean processCell(final HSSFCell cell, final Element tableCellElement, final int normalWidthPx, final int maxSpannedWidthPx, final float normalHeightPt) {
        final HSSFCellStyle cellStyle = cell.getCellStyle();
        String value = null;
        switch (cell.getCellType()) {
            case 1: {
                value = cell.getRichStringCellValue().getString();
                break;
            }
            case 2: {
                switch (cell.getCachedFormulaResultType()) {
                    case 1: {
                        final HSSFRichTextString str = cell.getRichStringCellValue();
                        if (str != null && str.length() > 0) {
                            value = str.toString();
                            break;
                        }
                        value = "";
                        break;
                    }
                    case 0: {
                        final HSSFCellStyle style = cellStyle;
                        if (style == null) {
                            value = String.valueOf(cell.getNumericCellValue());
                            break;
                        }
                        value = this._formatter.formatRawCellContents(cell.getNumericCellValue(), style.getDataFormat(), style.getDataFormatString());
                        break;
                    }
                    case 4: {
                        value = String.valueOf(cell.getBooleanCellValue());
                        break;
                    }
                    case 5: {
                        value = ErrorEval.getText(cell.getErrorCellValue());
                        break;
                    }
                    default: {
                        ExcelToHtmlConverter.logger.log(POILogger.WARN, "Unexpected cell cachedFormulaResultType (" + cell.getCachedFormulaResultType() + ")");
                        value = "";
                        break;
                    }
                }
                break;
            }
            case 3: {
                value = "";
                break;
            }
            case 0: {
                value = this._formatter.formatCellValue(cell);
                break;
            }
            case 4: {
                value = String.valueOf(cell.getBooleanCellValue());
                break;
            }
            case 5: {
                value = ErrorEval.getText(cell.getErrorCellValue());
                break;
            }
            default: {
                ExcelToHtmlConverter.logger.log(POILogger.WARN, "Unexpected cell type (" + cell.getCellType() + ")");
                return true;
            }
        }
        final boolean noText = AbstractExcelUtils.isEmpty(value);
        final boolean wrapInDivs = !noText && this.isUseDivsToSpan() && !cellStyle.getWrapText();
        final short cellStyleIndex = cellStyle.getIndex();
        if (cellStyleIndex != 0) {
            final HSSFWorkbook workbook = cell.getRow().getSheet().getWorkbook();
            final String mainCssClass = this.getStyleClassName(workbook, cellStyle);
            if (wrapInDivs) {
                tableCellElement.setAttribute("class", mainCssClass + " " + this.cssClassContainerCell);
            }
            else {
                tableCellElement.setAttribute("class", mainCssClass);
            }
            if (noText) {
                value = " ";
            }
        }
        if (this.isOutputLeadingSpacesAsNonBreaking() && value.startsWith(" ")) {
            final StringBuilder builder = new StringBuilder();
            for (int c = 0; c < value.length() && value.charAt(c) == ' '; ++c) {
                builder.append(' ');
            }
            if (value.length() != builder.length()) {
                builder.append(value.substring(builder.length()));
            }
            value = builder.toString();
        }
        final Text text = this.htmlDocumentFacade.createText(value);
        if (wrapInDivs) {
            final Element outerDiv = this.htmlDocumentFacade.createBlock();
            outerDiv.setAttribute("class", this.cssClassContainerDiv);
            final Element innerDiv = this.htmlDocumentFacade.createBlock();
            final StringBuilder innerDivStyle = new StringBuilder();
            innerDivStyle.append("position:absolute;min-width:");
            innerDivStyle.append(normalWidthPx);
            innerDivStyle.append("px;");
            if (maxSpannedWidthPx != Integer.MAX_VALUE) {
                innerDivStyle.append("max-width:");
                innerDivStyle.append(maxSpannedWidthPx);
                innerDivStyle.append("px;");
            }
            innerDivStyle.append("overflow:hidden;max-height:");
            innerDivStyle.append(normalHeightPt);
            innerDivStyle.append("pt;white-space:nowrap;");
            ExcelToHtmlUtils.appendAlign(innerDivStyle, cellStyle.getAlignment());
            this.htmlDocumentFacade.addStyleClass(outerDiv, this.cssClassPrefixDiv, innerDivStyle.toString());
            innerDiv.appendChild(text);
            outerDiv.appendChild(innerDiv);
            tableCellElement.appendChild(outerDiv);
        }
        else {
            tableCellElement.appendChild(text);
        }
        return AbstractExcelUtils.isEmpty(value) && cellStyleIndex == 0;
    }
    
    protected void processColumnHeaders(final HSSFSheet sheet, final int maxSheetColumns, final Element table) {
        final Element tableHeader = this.htmlDocumentFacade.createTableHeader();
        table.appendChild(tableHeader);
        final Element tr = this.htmlDocumentFacade.createTableRow();
        if (this.isOutputRowNumbers()) {
            tr.appendChild(this.htmlDocumentFacade.createTableHeaderCell());
        }
        for (int c = 0; c < maxSheetColumns; ++c) {
            if (this.isOutputHiddenColumns() || !sheet.isColumnHidden(c)) {
                final Element th = this.htmlDocumentFacade.createTableHeaderCell();
                final String text = this.getColumnName(c);
                th.appendChild(this.htmlDocumentFacade.createText(text));
                tr.appendChild(th);
            }
        }
        tableHeader.appendChild(tr);
    }
    
    protected void processColumnWidths(final HSSFSheet sheet, final int maxSheetColumns, final Element table) {
        final Element columnGroup = this.htmlDocumentFacade.createTableColumnGroup();
        if (this.isOutputRowNumbers()) {
            columnGroup.appendChild(this.htmlDocumentFacade.createTableColumn());
        }
        for (int c = 0; c < maxSheetColumns; ++c) {
            if (this.isOutputHiddenColumns() || !sheet.isColumnHidden(c)) {
                final Element col = this.htmlDocumentFacade.createTableColumn();
                col.setAttribute("width", String.valueOf(AbstractExcelConverter.getColumnWidth(sheet, c)));
                columnGroup.appendChild(col);
            }
        }
        table.appendChild(columnGroup);
    }
    
    protected void processDocumentInformation(final SummaryInformation summaryInformation) {
        if (AbstractExcelUtils.isNotEmpty(summaryInformation.getTitle())) {
            this.htmlDocumentFacade.setTitle(summaryInformation.getTitle());
        }
        if (AbstractExcelUtils.isNotEmpty(summaryInformation.getAuthor())) {
            this.htmlDocumentFacade.addAuthor(summaryInformation.getAuthor());
        }
        if (AbstractExcelUtils.isNotEmpty(summaryInformation.getKeywords())) {
            this.htmlDocumentFacade.addKeywords(summaryInformation.getKeywords());
        }
        if (AbstractExcelUtils.isNotEmpty(summaryInformation.getComments())) {
            this.htmlDocumentFacade.addDescription(summaryInformation.getComments());
        }
    }
    
    protected int processRow(final CellRangeAddress[][] mergedRanges, final HSSFRow row, final Element tableRowElement) {
        final HSSFSheet sheet = row.getSheet();
        final short maxColIx = row.getLastCellNum();
        if (maxColIx <= 0) {
            return 0;
        }
        final List<Element> emptyCells = new ArrayList<Element>(maxColIx);
        if (this.isOutputRowNumbers()) {
            final Element tableRowNumberCellElement = this.htmlDocumentFacade.createTableHeaderCell();
            this.processRowNumber(row, tableRowNumberCellElement);
            emptyCells.add(tableRowNumberCellElement);
        }
        int maxRenderedColumn = 0;
        for (int colIx = 0; colIx < maxColIx; ++colIx) {
            if (this.isOutputHiddenColumns() || !sheet.isColumnHidden(colIx)) {
                final CellRangeAddress range = AbstractExcelUtils.getMergedRange(mergedRanges, row.getRowNum(), colIx);
                if (range != null) {
                    if (range.getFirstColumn() != colIx) {
                        continue;
                    }
                    if (range.getFirstRow() != row.getRowNum()) {
                        continue;
                    }
                }
                final HSSFCell cell = row.getCell(colIx);
                int divWidthPx = 0;
                if (this.isUseDivsToSpan()) {
                    divWidthPx = AbstractExcelConverter.getColumnWidth(sheet, colIx);
                    boolean hasBreaks = false;
                    for (int nextColumnIndex = colIx + 1; nextColumnIndex < maxColIx; ++nextColumnIndex) {
                        if (this.isOutputHiddenColumns() || !sheet.isColumnHidden(nextColumnIndex)) {
                            if (row.getCell(nextColumnIndex) != null && !this.isTextEmpty(row.getCell(nextColumnIndex))) {
                                hasBreaks = true;
                                break;
                            }
                            divWidthPx += AbstractExcelConverter.getColumnWidth(sheet, nextColumnIndex);
                        }
                    }
                    if (!hasBreaks) {
                        divWidthPx = Integer.MAX_VALUE;
                    }
                }
                final Element tableCellElement = this.htmlDocumentFacade.createTableCell();
                if (range != null) {
                    if (range.getFirstColumn() != range.getLastColumn()) {
                        tableCellElement.setAttribute("colspan", String.valueOf(range.getLastColumn() - range.getFirstColumn() + 1));
                    }
                    if (range.getFirstRow() != range.getLastRow()) {
                        tableCellElement.setAttribute("rowspan", String.valueOf(range.getLastRow() - range.getFirstRow() + 1));
                    }
                }
                final boolean emptyCell = cell == null || this.processCell(cell, tableCellElement, AbstractExcelConverter.getColumnWidth(sheet, colIx), divWidthPx, row.getHeight() / 20.0f);
                if (emptyCell) {
                    emptyCells.add(tableCellElement);
                }
                else {
                    for (final Element emptyCellElement : emptyCells) {
                        tableRowElement.appendChild(emptyCellElement);
                    }
                    emptyCells.clear();
                    tableRowElement.appendChild(tableCellElement);
                    maxRenderedColumn = colIx;
                }
            }
        }
        return maxRenderedColumn + 1;
    }
    
    protected void processRowNumber(final HSSFRow row, final Element tableRowNumberCellElement) {
        tableRowNumberCellElement.setAttribute("class", "rownumber");
        final Text text = this.htmlDocumentFacade.createText(this.getRowName(row));
        tableRowNumberCellElement.appendChild(text);
    }
    
    protected void processSheet(final HSSFSheet sheet) {
        this.processSheetHeader(this.htmlDocumentFacade.getBody(), sheet);
        final int physicalNumberOfRows = sheet.getPhysicalNumberOfRows();
        if (physicalNumberOfRows <= 0) {
            return;
        }
        final Element table = this.htmlDocumentFacade.createTable();
        this.htmlDocumentFacade.addStyleClass(table, this.cssClassPrefixTable, "border-collapse:collapse;border-spacing:0;");
        final Element tableBody = this.htmlDocumentFacade.createTableBody();
        final CellRangeAddress[][] mergedRanges = ExcelToHtmlUtils.buildMergedRangesMap(sheet);
        final List<Element> emptyRowElements = new ArrayList<Element>(physicalNumberOfRows);
        int maxSheetColumns = 1;
        for (int r = sheet.getFirstRowNum(); r <= sheet.getLastRowNum(); ++r) {
            final HSSFRow row = sheet.getRow(r);
            if (row != null) {
                if (this.isOutputHiddenRows() || !row.getZeroHeight()) {
                    final Element tableRowElement = this.htmlDocumentFacade.createTableRow();
                    this.htmlDocumentFacade.addStyleClass(tableRowElement, this.cssClassPrefixRow, "height:" + row.getHeight() / 20.0f + "pt;");
                    final int maxRowColumnNumber = this.processRow(mergedRanges, row, tableRowElement);
                    if (maxRowColumnNumber == 0) {
                        emptyRowElements.add(tableRowElement);
                    }
                    else {
                        if (!emptyRowElements.isEmpty()) {
                            for (final Element emptyRowElement : emptyRowElements) {
                                tableBody.appendChild(emptyRowElement);
                            }
                            emptyRowElements.clear();
                        }
                        tableBody.appendChild(tableRowElement);
                    }
                    maxSheetColumns = Math.max(maxSheetColumns, maxRowColumnNumber);
                }
            }
        }
        this.processColumnWidths(sheet, maxSheetColumns, table);
        if (this.isOutputColumnHeaders()) {
            this.processColumnHeaders(sheet, maxSheetColumns, table);
        }
        table.appendChild(tableBody);
        this.htmlDocumentFacade.getBody().appendChild(table);
    }
    
    protected void processSheetHeader(final Element htmlBody, final HSSFSheet sheet) {
        final Element h2 = this.htmlDocumentFacade.createHeader2();
        h2.appendChild(this.htmlDocumentFacade.createText(sheet.getSheetName()));
        htmlBody.appendChild(h2);
    }
    
    public void processWorkbook(final HSSFWorkbook workbook) {
        final SummaryInformation summaryInformation = workbook.getSummaryInformation();
        if (summaryInformation != null) {
            this.processDocumentInformation(summaryInformation);
        }
        if (this.isUseDivsToSpan()) {
            this.cssClassContainerCell = this.htmlDocumentFacade.getOrCreateCssClass(this.cssClassPrefixCell, "padding:0;margin:0;align:left;vertical-align:top;");
            this.cssClassContainerDiv = this.htmlDocumentFacade.getOrCreateCssClass(this.cssClassPrefixDiv, "position:relative;");
        }
        for (int s = 0; s < workbook.getNumberOfSheets(); ++s) {
            final HSSFSheet sheet = workbook.getSheetAt(s);
            this.processSheet(sheet);
        }
        this.htmlDocumentFacade.updateStylesheet();
    }
    
    public void setCssClassPrefixCell(final String cssClassPrefixCell) {
        this.cssClassPrefixCell = cssClassPrefixCell;
    }
    
    public void setCssClassPrefixDiv(final String cssClassPrefixDiv) {
        this.cssClassPrefixDiv = cssClassPrefixDiv;
    }
    
    public void setCssClassPrefixRow(final String cssClassPrefixRow) {
        this.cssClassPrefixRow = cssClassPrefixRow;
    }
    
    public void setCssClassPrefixTable(final String cssClassPrefixTable) {
        this.cssClassPrefixTable = cssClassPrefixTable;
    }
    
    public void setUseDivsToSpan(final boolean useDivsToSpan) {
        this.useDivsToSpan = useDivsToSpan;
    }
    
    static {
        logger = POILogFactory.getLogger(ExcelToHtmlConverter.class);
    }
}
