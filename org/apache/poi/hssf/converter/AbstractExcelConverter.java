// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hssf.converter;

import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.formula.eval.ErrorEval;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.w3c.dom.Document;
import org.apache.poi.hwpf.converter.NumberFormatter;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hwpf.converter.DefaultFontReplacer;
import org.apache.poi.hwpf.converter.FontReplacer;
import org.apache.poi.hssf.usermodel.HSSFDataFormatter;

public abstract class AbstractExcelConverter
{
    protected final HSSFDataFormatter _formatter;
    private FontReplacer fontReplacer;
    private boolean outputColumnHeaders;
    private boolean outputHiddenColumns;
    private boolean outputHiddenRows;
    private boolean outputLeadingSpacesAsNonBreaking;
    private boolean outputRowNumbers;
    
    public AbstractExcelConverter() {
        this._formatter = new HSSFDataFormatter();
        this.fontReplacer = new DefaultFontReplacer();
        this.outputColumnHeaders = true;
        this.outputHiddenColumns = false;
        this.outputHiddenRows = false;
        this.outputLeadingSpacesAsNonBreaking = true;
        this.outputRowNumbers = true;
    }
    
    protected static int getColumnWidth(final HSSFSheet sheet, final int columnIndex) {
        return AbstractExcelUtils.getColumnWidthInPx(sheet.getColumnWidth(columnIndex));
    }
    
    protected static int getDefaultColumnWidth(final HSSFSheet sheet) {
        return AbstractExcelUtils.getColumnWidthInPx(sheet.getDefaultColumnWidth());
    }
    
    protected String getColumnName(final int columnIndex) {
        return NumberFormatter.getNumber(columnIndex + 1, 3);
    }
    
    protected abstract Document getDocument();
    
    public FontReplacer getFontReplacer() {
        return this.fontReplacer;
    }
    
    protected String getRowName(final HSSFRow row) {
        return String.valueOf(row.getRowNum() + 1);
    }
    
    public boolean isOutputColumnHeaders() {
        return this.outputColumnHeaders;
    }
    
    public boolean isOutputHiddenColumns() {
        return this.outputHiddenColumns;
    }
    
    public boolean isOutputHiddenRows() {
        return this.outputHiddenRows;
    }
    
    public boolean isOutputLeadingSpacesAsNonBreaking() {
        return this.outputLeadingSpacesAsNonBreaking;
    }
    
    public boolean isOutputRowNumbers() {
        return this.outputRowNumbers;
    }
    
    protected boolean isTextEmpty(final HSSFCell cell) {
        String value = null;
        switch (cell.getCellType()) {
            case 1: {
                value = cell.getRichStringCellValue().getString();
                break;
            }
            case 2: {
                switch (cell.getCachedFormulaResultType()) {
                    case 1: {
                        final HSSFRichTextString str = cell.getRichStringCellValue();
                        if (str == null || str.length() <= 0) {
                            return false;
                        }
                        value = str.toString();
                        break;
                    }
                    case 0: {
                        final HSSFCellStyle style = cell.getCellStyle();
                        if (style == null) {
                            return false;
                        }
                        value = this._formatter.formatRawCellContents(cell.getNumericCellValue(), style.getDataFormat(), style.getDataFormatString());
                        break;
                    }
                    case 4: {
                        value = String.valueOf(cell.getBooleanCellValue());
                        break;
                    }
                    case 5: {
                        value = ErrorEval.getText(cell.getErrorCellValue());
                        break;
                    }
                    default: {
                        value = "";
                        break;
                    }
                }
                break;
            }
            case 3: {
                value = "";
                break;
            }
            case 0: {
                value = this._formatter.formatCellValue(cell);
                break;
            }
            case 4: {
                value = String.valueOf(cell.getBooleanCellValue());
                break;
            }
            case 5: {
                value = ErrorEval.getText(cell.getErrorCellValue());
                break;
            }
            default: {
                return true;
            }
        }
        return AbstractExcelUtils.isEmpty(value);
    }
    
    public void setFontReplacer(final FontReplacer fontReplacer) {
        this.fontReplacer = fontReplacer;
    }
    
    public void setOutputColumnHeaders(final boolean outputColumnHeaders) {
        this.outputColumnHeaders = outputColumnHeaders;
    }
    
    public void setOutputHiddenColumns(final boolean outputZeroWidthColumns) {
        this.outputHiddenColumns = outputZeroWidthColumns;
    }
    
    public void setOutputHiddenRows(final boolean outputZeroHeightRows) {
        this.outputHiddenRows = outputZeroHeightRows;
    }
    
    public void setOutputLeadingSpacesAsNonBreaking(final boolean outputPrePostSpacesAsNonBreaking) {
        this.outputLeadingSpacesAsNonBreaking = outputPrePostSpacesAsNonBreaking;
    }
    
    public void setOutputRowNumbers(final boolean outputRowNumbers) {
        this.outputRowNumbers = outputRowNumbers;
    }
}
