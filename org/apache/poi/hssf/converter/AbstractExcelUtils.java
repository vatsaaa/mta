// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hssf.converter;

import java.io.IOException;
import java.io.Closeable;
import org.apache.poi.util.IOUtils;
import java.io.InputStream;
import java.io.FileInputStream;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import java.io.File;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.hssf.util.HSSFColor;

public class AbstractExcelUtils
{
    static final String EMPTY = "";
    private static final short EXCEL_COLUMN_WIDTH_FACTOR = 256;
    private static final int UNIT_OFFSET_LENGTH = 7;
    
    public static String getAlign(final short alignment) {
        switch (alignment) {
            case 2: {
                return "center";
            }
            case 6: {
                return "center";
            }
            case 4: {
                return "";
            }
            case 0: {
                return "";
            }
            case 5: {
                return "justify";
            }
            case 1: {
                return "left";
            }
            case 3: {
                return "right";
            }
            default: {
                return "";
            }
        }
    }
    
    public static String getBorderStyle(final short xlsBorder) {
        String borderStyle = null;
        switch (xlsBorder) {
            case 0: {
                borderStyle = "none";
                break;
            }
            case 4:
            case 7:
            case 9:
            case 10:
            case 11:
            case 12:
            case 13: {
                borderStyle = "dotted";
                break;
            }
            case 3:
            case 8: {
                borderStyle = "dashed";
                break;
            }
            case 6: {
                borderStyle = "double";
                break;
            }
            default: {
                borderStyle = "solid";
                break;
            }
        }
        return borderStyle;
    }
    
    public static String getBorderWidth(final short xlsBorder) {
        String borderWidth = null;
        switch (xlsBorder) {
            case 8:
            case 10:
            case 12: {
                borderWidth = "2pt";
                break;
            }
            case 5: {
                borderWidth = "thick";
                break;
            }
            default: {
                borderWidth = "thin";
                break;
            }
        }
        return borderWidth;
    }
    
    public static String getColor(final HSSFColor color) {
        final StringBuilder stringBuilder = new StringBuilder(7);
        stringBuilder.append('#');
        for (final short s : color.getTriplet()) {
            if (s < 10) {
                stringBuilder.append('0');
            }
            stringBuilder.append(Integer.toHexString(s));
        }
        final String result = stringBuilder.toString();
        if (result.equals("#ffffff")) {
            return "white";
        }
        if (result.equals("#c0c0c0")) {
            return "silver";
        }
        if (result.equals("#808080")) {
            return "gray";
        }
        if (result.equals("#000000")) {
            return "black";
        }
        return result;
    }
    
    public static int getColumnWidthInPx(final int widthUnits) {
        int pixels = widthUnits / 256 * 7;
        final int offsetWidthUnits = widthUnits % 256;
        pixels += Math.round(offsetWidthUnits / 36.57143f);
        return pixels;
    }
    
    public static CellRangeAddress getMergedRange(final CellRangeAddress[][] mergedRanges, final int rowNumber, final int columnNumber) {
        final CellRangeAddress[] mergedRangeRowInfo = (CellRangeAddress[])((rowNumber < mergedRanges.length) ? mergedRanges[rowNumber] : null);
        final CellRangeAddress cellRangeAddress = (mergedRangeRowInfo != null && columnNumber < mergedRangeRowInfo.length) ? mergedRangeRowInfo[columnNumber] : null;
        return cellRangeAddress;
    }
    
    static boolean isEmpty(final String str) {
        return str == null || str.length() == 0;
    }
    
    static boolean isNotEmpty(final String str) {
        return !isEmpty(str);
    }
    
    public static HSSFWorkbook loadXls(final File xlsFile) throws IOException {
        final FileInputStream inputStream = new FileInputStream(xlsFile);
        try {
            return new HSSFWorkbook(inputStream);
        }
        finally {
            IOUtils.closeQuietly(inputStream);
        }
    }
}
