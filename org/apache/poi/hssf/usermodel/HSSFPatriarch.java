// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hssf.usermodel;

import org.apache.poi.ss.usermodel.Picture;
import org.apache.poi.ss.usermodel.Comment;
import org.apache.poi.ss.usermodel.Chart;
import org.apache.poi.util.StringUtil;
import org.apache.poi.ddf.EscherComplexProperty;
import org.apache.poi.ddf.EscherProperty;
import org.apache.poi.ddf.EscherOptRecord;
import java.util.Iterator;
import org.apache.poi.util.Internal;
import org.apache.poi.ss.usermodel.ClientAnchor;
import org.apache.poi.ddf.EscherBSERecord;
import java.util.ArrayList;
import org.apache.poi.hssf.record.EscherAggregate;
import java.util.List;
import org.apache.poi.ss.usermodel.Drawing;

public final class HSSFPatriarch implements HSSFShapeContainer, Drawing
{
    private final List<HSSFShape> _shapes;
    private int _x1;
    private int _y1;
    private int _x2;
    private int _y2;
    private EscherAggregate _boundAggregate;
    final HSSFSheet _sheet;
    
    HSSFPatriarch(final HSSFSheet sheet, final EscherAggregate boundAggregate) {
        this._shapes = new ArrayList<HSSFShape>();
        this._x1 = 0;
        this._y1 = 0;
        this._x2 = 1023;
        this._y2 = 255;
        this._sheet = sheet;
        this._boundAggregate = boundAggregate;
    }
    
    public HSSFShapeGroup createGroup(final HSSFClientAnchor anchor) {
        final HSSFShapeGroup group = new HSSFShapeGroup(null, anchor);
        group.anchor = anchor;
        this.addShape(group);
        return group;
    }
    
    public HSSFSimpleShape createSimpleShape(final HSSFClientAnchor anchor) {
        final HSSFSimpleShape shape = new HSSFSimpleShape(null, anchor);
        shape.anchor = anchor;
        this.addShape(shape);
        return shape;
    }
    
    public HSSFPicture createPicture(final HSSFClientAnchor anchor, final int pictureIndex) {
        final HSSFPicture shape = new HSSFPicture(null, anchor);
        shape.setPictureIndex(pictureIndex);
        shape.anchor = anchor;
        this.addShape(shape);
        final EscherBSERecord bse = this._sheet.getWorkbook().getWorkbook().getBSERecord(pictureIndex);
        bse.setRef(bse.getRef() + 1);
        return shape;
    }
    
    public HSSFPicture createPicture(final ClientAnchor anchor, final int pictureIndex) {
        return this.createPicture((HSSFClientAnchor)anchor, pictureIndex);
    }
    
    public HSSFPolygon createPolygon(final HSSFClientAnchor anchor) {
        final HSSFPolygon shape = new HSSFPolygon(null, anchor);
        shape.anchor = anchor;
        this.addShape(shape);
        return shape;
    }
    
    public HSSFTextbox createTextbox(final HSSFClientAnchor anchor) {
        final HSSFTextbox shape = new HSSFTextbox(null, anchor);
        shape.anchor = anchor;
        this.addShape(shape);
        return shape;
    }
    
    public HSSFComment createComment(final HSSFAnchor anchor) {
        final HSSFComment shape = new HSSFComment(null, anchor);
        shape.anchor = anchor;
        this.addShape(shape);
        return shape;
    }
    
    HSSFSimpleShape createComboBox(final HSSFAnchor anchor) {
        final HSSFSimpleShape shape = new HSSFSimpleShape(null, anchor);
        shape.setShapeType(20);
        shape.anchor = anchor;
        this.addShape(shape);
        return shape;
    }
    
    public HSSFComment createCellComment(final ClientAnchor anchor) {
        return this.createComment((HSSFAnchor)anchor);
    }
    
    public List<HSSFShape> getChildren() {
        return this._shapes;
    }
    
    @Internal
    public void addShape(final HSSFShape shape) {
        shape._patriarch = this;
        this._shapes.add(shape);
    }
    
    public int countOfAllChildren() {
        int count = this._shapes.size();
        for (final HSSFShape shape : this._shapes) {
            count += shape.countOfAllChildren();
        }
        return count;
    }
    
    public void setCoordinates(final int x1, final int y1, final int x2, final int y2) {
        this._x1 = x1;
        this._y1 = y1;
        this._x2 = x2;
        this._y2 = y2;
    }
    
    public boolean containsChart() {
        final EscherOptRecord optRecord = (EscherOptRecord)this._boundAggregate.findFirstWithId((short)(-4085));
        if (optRecord == null) {
            return false;
        }
        for (final EscherProperty prop : optRecord.getEscherProperties()) {
            if (prop.getPropertyNumber() == 896 && prop.isComplex()) {
                final EscherComplexProperty cp = (EscherComplexProperty)prop;
                final String str = StringUtil.getFromUnicodeLE(cp.getComplexData());
                if (str.equals("Chart 1\u0000")) {
                    return true;
                }
                continue;
            }
        }
        return false;
    }
    
    public int getX1() {
        return this._x1;
    }
    
    public int getY1() {
        return this._y1;
    }
    
    public int getX2() {
        return this._x2;
    }
    
    public int getY2() {
        return this._y2;
    }
    
    protected EscherAggregate _getBoundAggregate() {
        return this._boundAggregate;
    }
    
    public HSSFClientAnchor createAnchor(final int dx1, final int dy1, final int dx2, final int dy2, final int col1, final int row1, final int col2, final int row2) {
        return new HSSFClientAnchor(dx1, dy1, dx2, dy2, (short)col1, row1, (short)col2, row2);
    }
    
    public Chart createChart(final ClientAnchor anchor) {
        throw new RuntimeException("NotImplemented");
    }
}
