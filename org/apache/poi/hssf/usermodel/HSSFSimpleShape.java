// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hssf.usermodel;

public class HSSFSimpleShape extends HSSFShape
{
    public static final short OBJECT_TYPE_LINE = 1;
    public static final short OBJECT_TYPE_RECTANGLE = 2;
    public static final short OBJECT_TYPE_OVAL = 3;
    public static final short OBJECT_TYPE_PICTURE = 8;
    public static final short OBJECT_TYPE_COMBO_BOX = 20;
    public static final short OBJECT_TYPE_COMMENT = 25;
    int shapeType;
    
    public HSSFSimpleShape(final HSSFShape parent, final HSSFAnchor anchor) {
        super(parent, anchor);
        this.shapeType = 1;
    }
    
    public int getShapeType() {
        return this.shapeType;
    }
    
    public void setShapeType(final int shapeType) {
        this.shapeType = shapeType;
    }
}
