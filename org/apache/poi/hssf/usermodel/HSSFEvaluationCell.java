// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hssf.usermodel;

import org.apache.poi.ss.formula.EvaluationSheet;
import org.apache.poi.ss.formula.EvaluationCell;

final class HSSFEvaluationCell implements EvaluationCell
{
    private final EvaluationSheet _evalSheet;
    private final HSSFCell _cell;
    
    public HSSFEvaluationCell(final HSSFCell cell, final EvaluationSheet evalSheet) {
        this._cell = cell;
        this._evalSheet = evalSheet;
    }
    
    public HSSFEvaluationCell(final HSSFCell cell) {
        this(cell, new HSSFEvaluationSheet(cell.getSheet()));
    }
    
    public Object getIdentityKey() {
        return this._cell;
    }
    
    public HSSFCell getHSSFCell() {
        return this._cell;
    }
    
    public boolean getBooleanCellValue() {
        return this._cell.getBooleanCellValue();
    }
    
    public int getCellType() {
        return this._cell.getCellType();
    }
    
    public int getColumnIndex() {
        return this._cell.getColumnIndex();
    }
    
    public int getErrorCellValue() {
        return this._cell.getErrorCellValue();
    }
    
    public double getNumericCellValue() {
        return this._cell.getNumericCellValue();
    }
    
    public int getRowIndex() {
        return this._cell.getRowIndex();
    }
    
    public EvaluationSheet getSheet() {
        return this._evalSheet;
    }
    
    public String getStringCellValue() {
        return this._cell.getRichStringCellValue().getString();
    }
    
    public int getCachedFormulaResultType() {
        return this._cell.getCachedFormulaResultType();
    }
}
