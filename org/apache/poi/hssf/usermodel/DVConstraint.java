// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hssf.usermodel;

import org.apache.poi.ss.formula.ptg.NumberPtg;
import org.apache.poi.ss.formula.ptg.StringPtg;
import org.apache.poi.hssf.model.HSSFFormulaParser;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.formula.ptg.Ptg;
import java.util.Date;
import java.text.ParseException;
import org.apache.poi.ss.usermodel.DateUtil;
import java.text.SimpleDateFormat;
import org.apache.poi.ss.usermodel.DataValidationConstraint;

public class DVConstraint implements DataValidationConstraint
{
    private static final ValidationType VT;
    private final int _validationType;
    private int _operator;
    private String[] _explicitListValues;
    private String _formula1;
    private String _formula2;
    private Double _value1;
    private Double _value2;
    
    private DVConstraint(final int validationType, final int comparisonOperator, final String formulaA, final String formulaB, final Double value1, final Double value2, final String[] excplicitListValues) {
        this._validationType = validationType;
        this._operator = comparisonOperator;
        this._formula1 = formulaA;
        this._formula2 = formulaB;
        this._value1 = value1;
        this._value2 = value2;
        this._explicitListValues = excplicitListValues;
    }
    
    private DVConstraint(final String listFormula, final String[] excplicitListValues) {
        this(3, 0, listFormula, null, null, null, excplicitListValues);
    }
    
    public static DVConstraint createNumericConstraint(final int validationType, final int comparisonOperator, final String expr1, final String expr2) {
        switch (validationType) {
            case 0: {
                if (expr1 != null || expr2 != null) {
                    throw new IllegalArgumentException("expr1 and expr2 must be null for validation type 'any'");
                }
                break;
            }
            case 1:
            case 2:
            case 6: {
                if (expr1 == null) {
                    throw new IllegalArgumentException("expr1 must be supplied");
                }
                OperatorType.validateSecondArg(comparisonOperator, expr2);
                break;
            }
            default: {
                throw new IllegalArgumentException("Validation Type (" + validationType + ") not supported with this method");
            }
        }
        final String formula1 = getFormulaFromTextExpression(expr1);
        final Double value1 = (formula1 == null) ? convertNumber(expr1) : null;
        final String formula2 = getFormulaFromTextExpression(expr2);
        final Double value2 = (formula2 == null) ? convertNumber(expr2) : null;
        return new DVConstraint(validationType, comparisonOperator, formula1, formula2, value1, value2, null);
    }
    
    public static DVConstraint createFormulaListConstraint(final String listFormula) {
        return new DVConstraint(listFormula, null);
    }
    
    public static DVConstraint createExplicitListConstraint(final String[] explicitListValues) {
        return new DVConstraint(null, explicitListValues);
    }
    
    public static DVConstraint createTimeConstraint(final int comparisonOperator, final String expr1, final String expr2) {
        if (expr1 == null) {
            throw new IllegalArgumentException("expr1 must be supplied");
        }
        OperatorType.validateSecondArg(comparisonOperator, expr1);
        final String formula1 = getFormulaFromTextExpression(expr1);
        final Double value1 = (formula1 == null) ? convertTime(expr1) : null;
        final String formula2 = getFormulaFromTextExpression(expr2);
        final Double value2 = (formula2 == null) ? convertTime(expr2) : null;
        final ValidationType vt = DVConstraint.VT;
        return new DVConstraint(5, comparisonOperator, formula1, formula2, value1, value2, null);
    }
    
    public static DVConstraint createDateConstraint(final int comparisonOperator, final String expr1, final String expr2, final String dateFormat) {
        if (expr1 == null) {
            throw new IllegalArgumentException("expr1 must be supplied");
        }
        OperatorType.validateSecondArg(comparisonOperator, expr2);
        final SimpleDateFormat df = (dateFormat == null) ? null : new SimpleDateFormat(dateFormat);
        final String formula1 = getFormulaFromTextExpression(expr1);
        final Double value1 = (formula1 == null) ? convertDate(expr1, df) : null;
        final String formula2 = getFormulaFromTextExpression(expr2);
        final Double value2 = (formula2 == null) ? convertDate(expr2, df) : null;
        final ValidationType vt = DVConstraint.VT;
        return new DVConstraint(4, comparisonOperator, formula1, formula2, value1, value2, null);
    }
    
    private static String getFormulaFromTextExpression(final String textExpr) {
        if (textExpr == null) {
            return null;
        }
        if (textExpr.length() < 1) {
            throw new IllegalArgumentException("Empty string is not a valid formula/value expression");
        }
        if (textExpr.charAt(0) == '=') {
            return textExpr.substring(1);
        }
        return null;
    }
    
    private static Double convertNumber(final String numberStr) {
        if (numberStr == null) {
            return null;
        }
        try {
            return new Double(numberStr);
        }
        catch (NumberFormatException e) {
            throw new RuntimeException("The supplied text '" + numberStr + "' could not be parsed as a number");
        }
    }
    
    private static Double convertTime(final String timeStr) {
        if (timeStr == null) {
            return null;
        }
        return new Double(DateUtil.convertTime(timeStr));
    }
    
    private static Double convertDate(final String dateStr, final SimpleDateFormat dateFormat) {
        if (dateStr == null) {
            return null;
        }
        Date dateVal;
        if (dateFormat == null) {
            dateVal = DateUtil.parseYYYYMMDDDate(dateStr);
        }
        else {
            try {
                dateVal = dateFormat.parse(dateStr);
            }
            catch (ParseException e) {
                throw new RuntimeException("Failed to parse date '" + dateStr + "' using specified format '" + dateFormat + "'", e);
            }
        }
        return new Double(DateUtil.getExcelDate(dateVal));
    }
    
    public static DVConstraint createCustomFormulaConstraint(final String formula) {
        if (formula == null) {
            throw new IllegalArgumentException("formula must be supplied");
        }
        final ValidationType vt = DVConstraint.VT;
        return new DVConstraint(7, 0, formula, null, null, null, null);
    }
    
    public int getValidationType() {
        return this._validationType;
    }
    
    public boolean isListValidationType() {
        final int validationType = this._validationType;
        final ValidationType vt = DVConstraint.VT;
        return validationType == 3;
    }
    
    public boolean isExplicitList() {
        final int validationType = this._validationType;
        final ValidationType vt = DVConstraint.VT;
        return validationType == 3 && this._explicitListValues != null;
    }
    
    public int getOperator() {
        return this._operator;
    }
    
    public void setOperator(final int operator) {
        this._operator = operator;
    }
    
    public String[] getExplicitListValues() {
        return this._explicitListValues;
    }
    
    public void setExplicitListValues(final String[] explicitListValues) {
        final int validationType = this._validationType;
        final ValidationType vt = DVConstraint.VT;
        if (validationType != 3) {
            throw new RuntimeException("Cannot setExplicitListValues on non-list constraint");
        }
        this._formula1 = null;
        this._explicitListValues = explicitListValues;
    }
    
    public String getFormula1() {
        return this._formula1;
    }
    
    public void setFormula1(final String formula1) {
        this._value1 = null;
        this._explicitListValues = null;
        this._formula1 = formula1;
    }
    
    public String getFormula2() {
        return this._formula2;
    }
    
    public void setFormula2(final String formula2) {
        this._value2 = null;
        this._formula2 = formula2;
    }
    
    public Double getValue1() {
        return this._value1;
    }
    
    public void setValue1(final double value1) {
        this._formula1 = null;
        this._value1 = new Double(value1);
    }
    
    public Double getValue2() {
        return this._value2;
    }
    
    public void setValue2(final double value2) {
        this._formula2 = null;
        this._value2 = new Double(value2);
    }
    
    FormulaPair createFormulas(final HSSFSheet sheet) {
        Ptg[] formula1;
        Ptg[] formula2;
        if (this.isListValidationType()) {
            formula1 = this.createListFormula(sheet);
            formula2 = Ptg.EMPTY_PTG_ARRAY;
        }
        else {
            formula1 = convertDoubleFormula(this._formula1, this._value1, sheet);
            formula2 = convertDoubleFormula(this._formula2, this._value2, sheet);
        }
        return new FormulaPair(formula1, formula2);
    }
    
    private Ptg[] createListFormula(final HSSFSheet sheet) {
        if (this._explicitListValues == null) {
            final HSSFWorkbook wb = sheet.getWorkbook();
            return HSSFFormulaParser.parse(this._formula1, wb, 5, wb.getSheetIndex(sheet));
        }
        final StringBuffer sb = new StringBuffer(this._explicitListValues.length * 16);
        for (int i = 0; i < this._explicitListValues.length; ++i) {
            if (i > 0) {
                sb.append('\0');
            }
            sb.append(this._explicitListValues[i]);
        }
        return new Ptg[] { new StringPtg(sb.toString()) };
    }
    
    private static Ptg[] convertDoubleFormula(final String formula, final Double value, final HSSFSheet sheet) {
        if (formula == null) {
            if (value == null) {
                return Ptg.EMPTY_PTG_ARRAY;
            }
            return new Ptg[] { new NumberPtg(value) };
        }
        else {
            if (value != null) {
                throw new IllegalStateException("Both formula and value cannot be present");
            }
            final HSSFWorkbook wb = sheet.getWorkbook();
            return HSSFFormulaParser.parse(formula, wb, 0, wb.getSheetIndex(sheet));
        }
    }
    
    static {
        VT = null;
    }
    
    public static final class FormulaPair
    {
        private final Ptg[] _formula1;
        private final Ptg[] _formula2;
        
        public FormulaPair(final Ptg[] formula1, final Ptg[] formula2) {
            this._formula1 = formula1;
            this._formula2 = formula2;
        }
        
        public Ptg[] getFormula1() {
            return this._formula1;
        }
        
        public Ptg[] getFormula2() {
            return this._formula2;
        }
    }
}
