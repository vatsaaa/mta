// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hssf.usermodel;

public class HSSFPolygon extends HSSFShape
{
    int[] xPoints;
    int[] yPoints;
    int drawAreaWidth;
    int drawAreaHeight;
    
    HSSFPolygon(final HSSFShape parent, final HSSFAnchor anchor) {
        super(parent, anchor);
        this.drawAreaWidth = 100;
        this.drawAreaHeight = 100;
    }
    
    public int[] getXPoints() {
        return this.xPoints;
    }
    
    public int[] getYPoints() {
        return this.yPoints;
    }
    
    public void setPoints(final int[] xPoints, final int[] yPoints) {
        this.xPoints = this.cloneArray(xPoints);
        this.yPoints = this.cloneArray(yPoints);
    }
    
    private int[] cloneArray(final int[] a) {
        final int[] result = new int[a.length];
        for (int i = 0; i < a.length; ++i) {
            result[i] = a[i];
        }
        return result;
    }
    
    public void setPolygonDrawArea(final int width, final int height) {
        this.drawAreaWidth = width;
        this.drawAreaHeight = height;
    }
    
    public int getDrawAreaWidth() {
        return this.drawAreaWidth;
    }
    
    public int getDrawAreaHeight() {
        return this.drawAreaHeight;
    }
}
