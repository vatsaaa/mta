// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hssf.usermodel;

import org.apache.poi.hssf.record.SubRecord;
import java.util.Iterator;
import org.apache.poi.poifs.filesystem.Entry;
import org.apache.poi.hssf.record.EmbeddedObjectRefSubRecord;
import java.io.IOException;
import org.apache.poi.util.HexDump;
import org.apache.poi.poifs.filesystem.DirectoryEntry;
import org.apache.poi.hssf.record.ObjRecord;

public final class HSSFObjectData
{
    private final ObjRecord _record;
    private final DirectoryEntry _root;
    
    public HSSFObjectData(final ObjRecord record, final DirectoryEntry root) {
        this._record = record;
        this._root = root;
    }
    
    public String getOLE2ClassName() {
        return this.findObjectRecord().getOLEClassName();
    }
    
    public DirectoryEntry getDirectory() throws IOException {
        final EmbeddedObjectRefSubRecord subRecord = this.findObjectRecord();
        final int streamId = subRecord.getStreamId();
        final String streamName = "MBD" + HexDump.toHex(streamId);
        final Entry entry = this._root.getEntry(streamName);
        if (entry instanceof DirectoryEntry) {
            return (DirectoryEntry)entry;
        }
        throw new IOException("Stream " + streamName + " was not an OLE2 directory");
    }
    
    public byte[] getObjectData() {
        return this.findObjectRecord().getObjectData();
    }
    
    public boolean hasDirectoryEntry() {
        final EmbeddedObjectRefSubRecord subRecord = this.findObjectRecord();
        final Integer streamId = subRecord.getStreamId();
        return streamId != null && streamId != 0;
    }
    
    protected EmbeddedObjectRefSubRecord findObjectRecord() {
        for (final Object subRecord : this._record.getSubRecords()) {
            if (subRecord instanceof EmbeddedObjectRefSubRecord) {
                return (EmbeddedObjectRefSubRecord)subRecord;
            }
        }
        throw new IllegalStateException("Object data does not contain a reference to an embedded object OLE2 directory");
    }
}
