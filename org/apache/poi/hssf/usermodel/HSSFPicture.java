// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hssf.usermodel;

import org.apache.poi.ss.usermodel.ClientAnchor;
import org.apache.poi.ss.usermodel.PictureData;
import org.apache.poi.ddf.EscherBlipRecord;
import org.apache.poi.hssf.model.InternalWorkbook;
import org.apache.poi.ddf.EscherBSERecord;
import java.io.InputStream;
import org.apache.poi.ss.util.ImageUtils;
import java.io.ByteArrayInputStream;
import java.awt.Dimension;
import org.apache.poi.ss.usermodel.Picture;

public final class HSSFPicture extends HSSFSimpleShape implements Picture
{
    public static final int PICTURE_TYPE_EMF = 2;
    public static final int PICTURE_TYPE_WMF = 3;
    public static final int PICTURE_TYPE_PICT = 4;
    public static final int PICTURE_TYPE_JPEG = 5;
    public static final int PICTURE_TYPE_PNG = 6;
    public static final int PICTURE_TYPE_DIB = 7;
    private static final float PX_DEFAULT = 32.0f;
    private static final float PX_MODIFIED = 36.56f;
    private static final int PX_ROW = 15;
    private int _pictureIndex;
    
    public HSSFPicture(final HSSFShape parent, final HSSFAnchor anchor) {
        super(parent, anchor);
        this.setShapeType(8);
    }
    
    public int getPictureIndex() {
        return this._pictureIndex;
    }
    
    public void setPictureIndex(final int pictureIndex) {
        this._pictureIndex = pictureIndex;
    }
    
    public void resize(final double scale) {
        final HSSFClientAnchor anchor = (HSSFClientAnchor)this.getAnchor();
        anchor.setAnchorType(2);
        final HSSFClientAnchor pref = this.getPreferredSize(scale);
        final int row2 = anchor.getRow1() + (pref.getRow2() - pref.getRow1());
        final int col2 = anchor.getCol1() + (pref.getCol2() - pref.getCol1());
        anchor.setCol2((short)col2);
        anchor.setDx1(0);
        anchor.setDx2(pref.getDx2());
        anchor.setRow2(row2);
        anchor.setDy1(0);
        anchor.setDy2(pref.getDy2());
    }
    
    public void resize() {
        this.resize(1.0);
    }
    
    public HSSFClientAnchor getPreferredSize() {
        return this.getPreferredSize(1.0);
    }
    
    public HSSFClientAnchor getPreferredSize(final double scale) {
        final HSSFClientAnchor anchor = (HSSFClientAnchor)this.getAnchor();
        final Dimension size = this.getImageDimension();
        final double scaledWidth = size.getWidth() * scale;
        final double scaledHeight = size.getHeight() * scale;
        float w = 0.0f;
        w += this.getColumnWidthInPixels(anchor.col1) * (1.0f - anchor.dx1 / 1024.0f);
        short col2 = (short)(anchor.col1 + 1);
        int dx2 = 0;
        while (w < scaledWidth) {
            final float n = w;
            final short column = col2;
            ++col2;
            w = n + this.getColumnWidthInPixels(column);
        }
        if (w > scaledWidth) {
            --col2;
            final double cw = this.getColumnWidthInPixels(col2);
            final double delta = w - scaledWidth;
            dx2 = (int)((cw - delta) / cw * 1024.0);
        }
        anchor.col2 = col2;
        anchor.dx2 = dx2;
        float h = 0.0f;
        h += (1.0f - anchor.dy1 / 256.0f) * this.getRowHeightInPixels(anchor.row1);
        int row2 = anchor.row1 + 1;
        int dy2 = 0;
        while (h < scaledHeight) {
            h += this.getRowHeightInPixels(row2++);
        }
        if (h > scaledHeight) {
            --row2;
            final double ch = this.getRowHeightInPixels(row2);
            final double delta2 = h - scaledHeight;
            dy2 = (int)((ch - delta2) / ch * 256.0);
        }
        anchor.row2 = row2;
        anchor.dy2 = dy2;
        return anchor;
    }
    
    private float getColumnWidthInPixels(final int column) {
        final int cw = this._patriarch._sheet.getColumnWidth(column);
        final float px = this.getPixelWidth(column);
        return cw / px;
    }
    
    private float getRowHeightInPixels(final int i) {
        final HSSFRow row = this._patriarch._sheet.getRow(i);
        float height;
        if (row != null) {
            height = row.getHeight();
        }
        else {
            height = this._patriarch._sheet.getDefaultRowHeight();
        }
        return height / 15.0f;
    }
    
    private float getPixelWidth(final int column) {
        final int def = this._patriarch._sheet.getDefaultColumnWidth() * 256;
        final int cw = this._patriarch._sheet.getColumnWidth(column);
        return (cw == def) ? 32.0f : 36.56f;
    }
    
    public Dimension getImageDimension() {
        final EscherBSERecord bse = this._patriarch._sheet._book.getBSERecord(this._pictureIndex);
        final byte[] data = bse.getBlipRecord().getPicturedata();
        final int type = bse.getBlipTypeWin32();
        return ImageUtils.getImageDimension(new ByteArrayInputStream(data), type);
    }
    
    public HSSFPictureData getPictureData() {
        final InternalWorkbook iwb = this._patriarch._sheet.getWorkbook().getWorkbook();
        final EscherBlipRecord blipRecord = iwb.getBSERecord(this._pictureIndex).getBlipRecord();
        return new HSSFPictureData(blipRecord);
    }
}
