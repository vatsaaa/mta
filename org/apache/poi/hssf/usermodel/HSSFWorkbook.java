// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hssf.usermodel;

import org.apache.poi.util.POILogFactory;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Name;
import org.apache.poi.ss.usermodel.DataFormat;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.hssf.record.RecalcIdRecord;
import org.apache.poi.ss.formula.udf.AggregatingUDFFinder;
import org.apache.poi.poifs.filesystem.DirectoryEntry;
import org.apache.poi.hssf.record.EmbeddedObjectRefSubRecord;
import org.apache.poi.hssf.record.SubRecord;
import org.apache.poi.hssf.record.ObjRecord;
import org.apache.poi.hssf.record.RecordBase;
import org.apache.poi.hssf.record.AbstractEscherHolderRecord;
import org.apache.poi.ddf.EscherBlipRecord;
import org.apache.poi.ddf.EscherBSERecord;
import org.apache.poi.ddf.EscherBitmapBlip;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.poi.hssf.model.DrawingManager2;
import org.apache.poi.ddf.EscherRecord;
import java.io.PrintWriter;
import org.apache.poi.hssf.record.DrawingGroupRecord;
import org.apache.poi.hssf.record.UnknownRecord;
import org.apache.poi.hssf.util.CellReference;
import org.apache.poi.hssf.model.HSSFFormulaParser;
import org.apache.poi.ss.formula.SheetNameFormatter;
import org.apache.poi.hssf.record.aggregates.RecordAggregate;
import java.io.ByteArrayInputStream;
import java.io.OutputStream;
import org.apache.poi.hssf.record.ExtendedFormatRecord;
import org.apache.poi.hssf.record.FontRecord;
import org.apache.poi.ss.formula.ptg.Ptg;
import org.apache.poi.ss.formula.ptg.UnionPtg;
import org.apache.poi.ss.formula.ptg.Area3DPtg;
import org.apache.poi.ss.formula.ptg.MemFuncPtg;
import org.apache.poi.hssf.record.BackupRecord;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.util.WorkbookUtil;
import java.util.Iterator;
import org.apache.poi.ss.formula.FormulaShifter;
import org.apache.poi.hssf.record.common.UnicodeString;
import org.apache.poi.hssf.record.LabelSSTRecord;
import org.apache.poi.hssf.record.LabelRecord;
import org.apache.poi.hssf.record.NameRecord;
import org.apache.poi.hssf.record.Record;
import java.io.InputStream;
import org.apache.poi.hssf.model.InternalSheet;
import org.apache.poi.hssf.model.RecordStream;
import org.apache.poi.hssf.record.RecordFactory;
import org.apache.poi.hssf.OldExcelFormatException;
import java.io.FileNotFoundException;
import java.io.IOException;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.poifs.filesystem.DirectoryNode;
import org.apache.poi.ss.formula.udf.UDFFinder;
import org.apache.poi.util.POILogger;
import org.apache.poi.ss.usermodel.Row;
import java.util.Hashtable;
import java.util.ArrayList;
import java.util.List;
import org.apache.poi.hssf.model.InternalWorkbook;
import java.util.regex.Pattern;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.POIDocument;

public final class HSSFWorkbook extends POIDocument implements Workbook
{
    private static final Pattern COMMA_PATTERN;
    private static final int MAX_ROW = 65535;
    private static final short MAX_COLUMN = 255;
    private static final int MAX_STYLES = 4030;
    private static final int DEBUG;
    public static final int INITIAL_CAPACITY = 3;
    private InternalWorkbook workbook;
    protected List<HSSFSheet> _sheets;
    private ArrayList<HSSFName> names;
    private Hashtable fonts;
    private boolean preserveNodes;
    private HSSFDataFormat formatter;
    private Row.MissingCellPolicy missingCellPolicy;
    private static POILogger log;
    private UDFFinder _udfFinder;
    private static final String[] WORKBOOK_DIR_ENTRY_NAMES;
    
    public static HSSFWorkbook create(final InternalWorkbook book) {
        return new HSSFWorkbook(book);
    }
    
    public HSSFWorkbook() {
        this(InternalWorkbook.createWorkbook());
    }
    
    private HSSFWorkbook(final InternalWorkbook book) {
        super((DirectoryNode)null);
        this.missingCellPolicy = HSSFRow.RETURN_NULL_AND_BLANK;
        this._udfFinder = UDFFinder.DEFAULT;
        this.workbook = book;
        this._sheets = new ArrayList<HSSFSheet>(3);
        this.names = new ArrayList<HSSFName>(3);
    }
    
    public HSSFWorkbook(final POIFSFileSystem fs) throws IOException {
        this(fs, true);
    }
    
    public HSSFWorkbook(final POIFSFileSystem fs, final boolean preserveNodes) throws IOException {
        this(fs.getRoot(), fs, preserveNodes);
    }
    
    private static String getWorkbookDirEntryName(final DirectoryNode directory) {
        final String[] potentialNames = HSSFWorkbook.WORKBOOK_DIR_ENTRY_NAMES;
        int i = 0;
        while (i < potentialNames.length) {
            final String wbName = potentialNames[i];
            try {
                directory.getEntry(wbName);
                return wbName;
            }
            catch (FileNotFoundException e) {
                ++i;
                continue;
            }
            break;
        }
        try {
            directory.getEntry("Book");
            throw new OldExcelFormatException("The supplied spreadsheet seems to be Excel 5.0/7.0 (BIFF5) format. POI only supports BIFF8 format (from Excel versions 97/2000/XP/2003)");
        }
        catch (FileNotFoundException e2) {
            throw new IllegalArgumentException("The supplied POIFSFileSystem does not contain a BIFF8 'Workbook' entry. Is it really an excel file?");
        }
    }
    
    public HSSFWorkbook(final DirectoryNode directory, final POIFSFileSystem fs, final boolean preserveNodes) throws IOException {
        this(directory, preserveNodes);
    }
    
    public HSSFWorkbook(final DirectoryNode directory, final boolean preserveNodes) throws IOException {
        super(directory);
        this.missingCellPolicy = HSSFRow.RETURN_NULL_AND_BLANK;
        this._udfFinder = UDFFinder.DEFAULT;
        final String workbookName = getWorkbookDirEntryName(directory);
        if (!(this.preserveNodes = preserveNodes)) {
            this.directory = null;
        }
        this._sheets = new ArrayList<HSSFSheet>(3);
        this.names = new ArrayList<HSSFName>(3);
        final InputStream stream = directory.createDocumentInputStream(workbookName);
        final List<Record> records = RecordFactory.createRecords(stream);
        this.setPropertiesFromWorkbook(this.workbook = InternalWorkbook.createWorkbook(records));
        final int recOffset = this.workbook.getNumRecords();
        final int sheetNum = 0;
        this.convertLabelRecords(records, recOffset);
        final RecordStream rs = new RecordStream(records, recOffset);
        while (rs.hasNext()) {
            final InternalSheet sheet = InternalSheet.createSheet(rs);
            this._sheets.add(new HSSFSheet(this, sheet));
        }
        for (int i = 0; i < this.workbook.getNumNames(); ++i) {
            final NameRecord nameRecord = this.workbook.getNameRecord(i);
            final HSSFName name = new HSSFName(this, nameRecord, this.workbook.getNameCommentRecord(nameRecord));
            this.names.add(name);
        }
    }
    
    public HSSFWorkbook(final InputStream s) throws IOException {
        this(s, true);
    }
    
    public HSSFWorkbook(final InputStream s, final boolean preserveNodes) throws IOException {
        this(new POIFSFileSystem(s), preserveNodes);
    }
    
    private void setPropertiesFromWorkbook(final InternalWorkbook book) {
        this.workbook = book;
    }
    
    private void convertLabelRecords(final List records, final int offset) {
        if (HSSFWorkbook.log.check(POILogger.DEBUG)) {
            HSSFWorkbook.log.log(POILogger.DEBUG, "convertLabelRecords called");
        }
        for (int k = offset; k < records.size(); ++k) {
            final Record rec = records.get(k);
            if (rec.getSid() == 516) {
                final LabelRecord oldrec = (LabelRecord)rec;
                records.remove(k);
                final LabelSSTRecord newrec = new LabelSSTRecord();
                final int stringid = this.workbook.addSSTString(new UnicodeString(oldrec.getValue()));
                newrec.setRow(oldrec.getRow());
                newrec.setColumn(oldrec.getColumn());
                newrec.setXFIndex(oldrec.getXFIndex());
                newrec.setSSTIndex(stringid);
                records.add(k, newrec);
            }
        }
        if (HSSFWorkbook.log.check(POILogger.DEBUG)) {
            HSSFWorkbook.log.log(POILogger.DEBUG, "convertLabelRecords exit");
        }
    }
    
    public Row.MissingCellPolicy getMissingCellPolicy() {
        return this.missingCellPolicy;
    }
    
    public void setMissingCellPolicy(final Row.MissingCellPolicy missingCellPolicy) {
        this.missingCellPolicy = missingCellPolicy;
    }
    
    public void setSheetOrder(final String sheetname, final int pos) {
        final int oldSheetIndex = this.getSheetIndex(sheetname);
        this._sheets.add(pos, this._sheets.remove(oldSheetIndex));
        this.workbook.setSheetOrder(sheetname, pos);
        final FormulaShifter shifter = FormulaShifter.createForSheetShift(oldSheetIndex, pos);
        for (final HSSFSheet sheet : this._sheets) {
            sheet.getSheet().updateFormulasAfterCellShift(shifter, -1);
        }
        this.workbook.updateNamesAfterCellShift(shifter);
    }
    
    private void validateSheetIndex(final int index) {
        final int lastSheetIx = this._sheets.size() - 1;
        if (index < 0 || index > lastSheetIx) {
            throw new IllegalArgumentException("Sheet index (" + index + ") is out of range (0.." + lastSheetIx + ")");
        }
    }
    
    public void setSelectedTab(final int index) {
        this.validateSheetIndex(index);
        for (int nSheets = this._sheets.size(), i = 0; i < nSheets; ++i) {
            this.getSheetAt(i).setSelected(i == index);
        }
        this.workbook.getWindowOne().setNumSelectedTabs((short)1);
    }
    
    @Deprecated
    public void setSelectedTab(final short index) {
        this.setSelectedTab((int)index);
    }
    
    public void setSelectedTabs(final int[] indexes) {
        for (int i = 0; i < indexes.length; ++i) {
            this.validateSheetIndex(indexes[i]);
        }
        for (int nSheets = this._sheets.size(), j = 0; j < nSheets; ++j) {
            boolean bSelect = false;
            for (int k = 0; k < indexes.length; ++k) {
                if (indexes[k] == j) {
                    bSelect = true;
                    break;
                }
            }
            this.getSheetAt(j).setSelected(bSelect);
        }
        this.workbook.getWindowOne().setNumSelectedTabs((short)indexes.length);
    }
    
    public void setActiveSheet(final int index) {
        this.validateSheetIndex(index);
        for (int nSheets = this._sheets.size(), i = 0; i < nSheets; ++i) {
            this.getSheetAt(i).setActive(i == index);
        }
        this.workbook.getWindowOne().setActiveSheetIndex(index);
    }
    
    public int getActiveSheetIndex() {
        return this.workbook.getWindowOne().getActiveSheetIndex();
    }
    
    @Deprecated
    public short getSelectedTab() {
        return (short)this.getActiveSheetIndex();
    }
    
    public void setFirstVisibleTab(final int index) {
        this.workbook.getWindowOne().setFirstVisibleTab(index);
    }
    
    @Deprecated
    public void setDisplayedTab(final short index) {
        this.setFirstVisibleTab(index);
    }
    
    public int getFirstVisibleTab() {
        return this.workbook.getWindowOne().getFirstVisibleTab();
    }
    
    @Deprecated
    public short getDisplayedTab() {
        return (short)this.getFirstVisibleTab();
    }
    
    public void setSheetName(final int sheetIx, final String name) {
        if (name == null) {
            throw new IllegalArgumentException("sheetName must not be null");
        }
        if (this.workbook.doesContainsSheetName(name, sheetIx)) {
            throw new IllegalArgumentException("The workbook already contains a sheet with this name");
        }
        this.validateSheetIndex(sheetIx);
        this.workbook.setSheetName(sheetIx, name);
    }
    
    public String getSheetName(final int sheetIndex) {
        this.validateSheetIndex(sheetIndex);
        return this.workbook.getSheetName(sheetIndex);
    }
    
    public boolean isHidden() {
        return this.workbook.getWindowOne().getHidden();
    }
    
    public void setHidden(final boolean hiddenFlag) {
        this.workbook.getWindowOne().setHidden(hiddenFlag);
    }
    
    public boolean isSheetHidden(final int sheetIx) {
        this.validateSheetIndex(sheetIx);
        return this.workbook.isSheetHidden(sheetIx);
    }
    
    public boolean isSheetVeryHidden(final int sheetIx) {
        this.validateSheetIndex(sheetIx);
        return this.workbook.isSheetVeryHidden(sheetIx);
    }
    
    public void setSheetHidden(final int sheetIx, final boolean hidden) {
        this.validateSheetIndex(sheetIx);
        this.workbook.setSheetHidden(sheetIx, hidden);
    }
    
    public void setSheetHidden(final int sheetIx, final int hidden) {
        this.validateSheetIndex(sheetIx);
        WorkbookUtil.validateSheetState(hidden);
        this.workbook.setSheetHidden(sheetIx, hidden);
    }
    
    public int getSheetIndex(final String name) {
        return this.workbook.getSheetIndex(name);
    }
    
    public int getSheetIndex(final Sheet sheet) {
        for (int i = 0; i < this._sheets.size(); ++i) {
            if (this._sheets.get(i) == sheet) {
                return i;
            }
        }
        return -1;
    }
    
    @Deprecated
    public int getExternalSheetIndex(final int internalSheetIndex) {
        return this.workbook.checkExternSheet(internalSheetIndex);
    }
    
    @Deprecated
    public String findSheetNameFromExternSheet(final int externSheetIndex) {
        return this.workbook.findSheetNameFromExternSheet(externSheetIndex);
    }
    
    @Deprecated
    public String resolveNameXText(final int refIndex, final int definedNameIndex) {
        return this.workbook.resolveNameXText(refIndex, definedNameIndex);
    }
    
    public HSSFSheet createSheet() {
        final HSSFSheet sheet = new HSSFSheet(this);
        this._sheets.add(sheet);
        this.workbook.setSheetName(this._sheets.size() - 1, "Sheet" + (this._sheets.size() - 1));
        final boolean isOnlySheet = this._sheets.size() == 1;
        sheet.setSelected(isOnlySheet);
        sheet.setActive(isOnlySheet);
        return sheet;
    }
    
    public HSSFSheet cloneSheet(final int sheetIndex) {
        this.validateSheetIndex(sheetIndex);
        final HSSFSheet srcSheet = this._sheets.get(sheetIndex);
        final String srcName = this.workbook.getSheetName(sheetIndex);
        final HSSFSheet clonedSheet = srcSheet.cloneSheet(this);
        clonedSheet.setSelected(false);
        clonedSheet.setActive(false);
        final String name = this.getUniqueSheetName(srcName);
        final int newSheetIndex = this._sheets.size();
        this._sheets.add(clonedSheet);
        this.workbook.setSheetName(newSheetIndex, name);
        final int filterDbNameIndex = this.findExistingBuiltinNameRecordIdx(sheetIndex, (byte)13);
        if (filterDbNameIndex != -1) {
            final NameRecord newNameRecord = this.workbook.cloneFilter(filterDbNameIndex, newSheetIndex);
            final HSSFName newName = new HSSFName(this, newNameRecord);
            this.names.add(newName);
        }
        this.workbook.cloneDrawings(clonedSheet.getSheet());
        return clonedSheet;
    }
    
    private String getUniqueSheetName(final String srcName) {
        int uniqueIndex = 2;
        String baseName = srcName;
        final int bracketPos = srcName.lastIndexOf(40);
        if (bracketPos > 0 && srcName.endsWith(")")) {
            final String suffix = srcName.substring(bracketPos + 1, srcName.length() - ")".length());
            try {
                uniqueIndex = Integer.parseInt(suffix.trim());
                ++uniqueIndex;
                baseName = srcName.substring(0, bracketPos).trim();
            }
            catch (NumberFormatException ex) {}
        }
        String name;
        do {
            final String index = Integer.toString(uniqueIndex++);
            if (baseName.length() + index.length() + 2 < 31) {
                name = baseName + " (" + index + ")";
            }
            else {
                name = baseName.substring(0, 31 - index.length() - 2) + "(" + index + ")";
            }
        } while (this.workbook.getSheetIndex(name) != -1);
        return name;
    }
    
    public HSSFSheet createSheet(final String sheetname) {
        if (sheetname == null) {
            throw new IllegalArgumentException("sheetName must not be null");
        }
        if (this.workbook.doesContainsSheetName(sheetname, this._sheets.size())) {
            throw new IllegalArgumentException("The workbook already contains a sheet of this name");
        }
        final HSSFSheet sheet = new HSSFSheet(this);
        this.workbook.setSheetName(this._sheets.size(), sheetname);
        this._sheets.add(sheet);
        final boolean isOnlySheet = this._sheets.size() == 1;
        sheet.setSelected(isOnlySheet);
        sheet.setActive(isOnlySheet);
        return sheet;
    }
    
    public int getNumberOfSheets() {
        return this._sheets.size();
    }
    
    public int getSheetIndexFromExternSheetIndex(final int externSheetNumber) {
        return this.workbook.getSheetIndexFromExternSheetIndex(externSheetNumber);
    }
    
    private HSSFSheet[] getSheets() {
        final HSSFSheet[] result = new HSSFSheet[this._sheets.size()];
        this._sheets.toArray(result);
        return result;
    }
    
    public HSSFSheet getSheetAt(final int index) {
        this.validateSheetIndex(index);
        return this._sheets.get(index);
    }
    
    public HSSFSheet getSheet(final String name) {
        HSSFSheet retval = null;
        for (int k = 0; k < this._sheets.size(); ++k) {
            final String sheetname = this.workbook.getSheetName(k);
            if (sheetname.equalsIgnoreCase(name)) {
                retval = this._sheets.get(k);
            }
        }
        return retval;
    }
    
    public void removeSheetAt(final int index) {
        this.validateSheetIndex(index);
        final boolean wasActive = this.getSheetAt(index).isActive();
        final boolean wasSelected = this.getSheetAt(index).isSelected();
        this._sheets.remove(index);
        this.workbook.removeSheet(index);
        final int nSheets = this._sheets.size();
        if (nSheets < 1) {
            return;
        }
        int newSheetIndex = index;
        if (newSheetIndex >= nSheets) {
            newSheetIndex = nSheets - 1;
        }
        if (wasActive) {
            this.setActiveSheet(newSheetIndex);
        }
        if (wasSelected) {
            boolean someOtherSheetIsStillSelected = false;
            for (int i = 0; i < nSheets; ++i) {
                if (this.getSheetAt(i).isSelected()) {
                    someOtherSheetIsStillSelected = true;
                    break;
                }
            }
            if (!someOtherSheetIsStillSelected) {
                this.setSelectedTab(newSheetIndex);
            }
        }
    }
    
    public void setBackupFlag(final boolean backupValue) {
        final BackupRecord backupRecord = this.workbook.getBackupRecord();
        backupRecord.setBackup((short)(backupValue ? 1 : 0));
    }
    
    public boolean getBackupFlag() {
        final BackupRecord backupRecord = this.workbook.getBackupRecord();
        return backupRecord.getBackup() != 0;
    }
    
    public void setRepeatingRowsAndColumns(final int sheetIndex, final int startColumn, final int endColumn, final int startRow, final int endRow) {
        if (startColumn == -1 && endColumn != -1) {
            throw new IllegalArgumentException("Invalid column range specification");
        }
        if (startRow == -1 && endRow != -1) {
            throw new IllegalArgumentException("Invalid row range specification");
        }
        if (startColumn < -1 || startColumn >= 255) {
            throw new IllegalArgumentException("Invalid column range specification");
        }
        if (endColumn < -1 || endColumn >= 255) {
            throw new IllegalArgumentException("Invalid column range specification");
        }
        if (startRow < -1 || startRow > 65535) {
            throw new IllegalArgumentException("Invalid row range specification");
        }
        if (endRow < -1 || endRow > 65535) {
            throw new IllegalArgumentException("Invalid row range specification");
        }
        if (startColumn > endColumn) {
            throw new IllegalArgumentException("Invalid column range specification");
        }
        if (startRow > endRow) {
            throw new IllegalArgumentException("Invalid row range specification");
        }
        final HSSFSheet sheet = this.getSheetAt(sheetIndex);
        final short externSheetIndex = this.getWorkbook().checkExternSheet(sheetIndex);
        final boolean settingRowAndColumn = startColumn != -1 && endColumn != -1 && startRow != -1 && endRow != -1;
        final boolean removingRange = startColumn == -1 && endColumn == -1 && startRow == -1 && endRow == -1;
        final int rowColHeaderNameIndex = this.findExistingBuiltinNameRecordIdx(sheetIndex, (byte)7);
        if (removingRange) {
            if (rowColHeaderNameIndex >= 0) {
                this.workbook.removeName(rowColHeaderNameIndex);
            }
            return;
        }
        NameRecord nameRecord;
        boolean isNewRecord;
        if (rowColHeaderNameIndex < 0) {
            nameRecord = this.workbook.createBuiltInName((byte)7, sheetIndex + 1);
            isNewRecord = true;
        }
        else {
            nameRecord = this.workbook.getNameRecord(rowColHeaderNameIndex);
            isNewRecord = false;
        }
        final List temp = new ArrayList();
        if (settingRowAndColumn) {
            final int exprsSize = 23;
            temp.add(new MemFuncPtg(23));
        }
        if (startColumn >= 0) {
            final Area3DPtg colArea = new Area3DPtg(0, 65535, startColumn, endColumn, false, false, false, false, externSheetIndex);
            temp.add(colArea);
        }
        if (startRow >= 0) {
            final Area3DPtg rowArea = new Area3DPtg(startRow, endRow, 0, 255, false, false, false, false, externSheetIndex);
            temp.add(rowArea);
        }
        if (settingRowAndColumn) {
            temp.add(UnionPtg.instance);
        }
        final Ptg[] ptgs = new Ptg[temp.size()];
        temp.toArray(ptgs);
        nameRecord.setNameDefinition(ptgs);
        if (isNewRecord) {
            final HSSFName newName = new HSSFName(this, nameRecord, nameRecord.isBuiltInName() ? null : this.workbook.getNameCommentRecord(nameRecord));
            this.names.add(newName);
        }
        final HSSFPrintSetup printSetup = sheet.getPrintSetup();
        printSetup.setValidSettings(false);
        sheet.setActive(true);
    }
    
    private int findExistingBuiltinNameRecordIdx(final int sheetIndex, final byte builtinCode) {
        for (int defNameIndex = 0; defNameIndex < this.names.size(); ++defNameIndex) {
            final NameRecord r = this.workbook.getNameRecord(defNameIndex);
            if (r == null) {
                throw new RuntimeException("Unable to find all defined names to iterate over");
            }
            if (r.isBuiltInName()) {
                if (r.getBuiltInName() == builtinCode) {
                    if (r.getSheetNumber() - 1 == sheetIndex) {
                        return defNameIndex;
                    }
                }
            }
        }
        return -1;
    }
    
    public HSSFFont createFont() {
        final FontRecord font = this.workbook.createNewFont();
        short fontindex = (short)(this.getNumberOfFonts() - 1);
        if (fontindex > 3) {
            ++fontindex;
        }
        if (fontindex == 32767) {
            throw new IllegalArgumentException("Maximum number of fonts was exceeded");
        }
        return this.getFontAt(fontindex);
    }
    
    public HSSFFont findFont(final short boldWeight, final short color, final short fontHeight, final String name, final boolean italic, final boolean strikeout, final short typeOffset, final byte underline) {
        for (short i = 0; i <= this.getNumberOfFonts(); ++i) {
            if (i != 4) {
                final HSSFFont hssfFont = this.getFontAt(i);
                if (hssfFont.getBoldweight() == boldWeight && hssfFont.getColor() == color && hssfFont.getFontHeight() == fontHeight && hssfFont.getFontName().equals(name) && hssfFont.getItalic() == italic && hssfFont.getStrikeout() == strikeout && hssfFont.getTypeOffset() == typeOffset && hssfFont.getUnderline() == underline) {
                    return hssfFont;
                }
            }
        }
        return null;
    }
    
    public short getNumberOfFonts() {
        return (short)this.workbook.getNumberOfFontRecords();
    }
    
    public HSSFFont getFontAt(final short idx) {
        if (this.fonts == null) {
            this.fonts = new Hashtable();
        }
        final Short sIdx = idx;
        if (this.fonts.containsKey(sIdx)) {
            return this.fonts.get(sIdx);
        }
        final FontRecord font = this.workbook.getFontRecordAt(idx);
        final HSSFFont retval = new HSSFFont(idx, font);
        this.fonts.put(sIdx, retval);
        return retval;
    }
    
    protected void resetFontCache() {
        this.fonts = new Hashtable();
    }
    
    public HSSFCellStyle createCellStyle() {
        if (this.workbook.getNumExFormats() == 4030) {
            throw new IllegalStateException("The maximum number of cell styles was exceeded. You can define up to 4000 styles in a .xls workbook");
        }
        final ExtendedFormatRecord xfr = this.workbook.createCellXF();
        final short index = (short)(this.getNumCellStyles() - 1);
        final HSSFCellStyle style = new HSSFCellStyle(index, xfr, this);
        return style;
    }
    
    public short getNumCellStyles() {
        return (short)this.workbook.getNumExFormats();
    }
    
    public HSSFCellStyle getCellStyleAt(final short idx) {
        final ExtendedFormatRecord xfr = this.workbook.getExFormatAt(idx);
        final HSSFCellStyle style = new HSSFCellStyle(idx, xfr, this);
        return style;
    }
    
    @Override
    public void write(final OutputStream stream) throws IOException {
        final byte[] bytes = this.getBytes();
        final POIFSFileSystem fs = new POIFSFileSystem();
        final List<String> excepts = new ArrayList<String>(1);
        fs.createDocument(new ByteArrayInputStream(bytes), "Workbook");
        this.writeProperties(fs, excepts);
        if (this.preserveNodes) {
            excepts.add("Workbook");
            excepts.add("WORKBOOK");
            this.copyNodes(this.directory, fs.getRoot(), excepts);
            fs.getRoot().setStorageClsid(this.directory.getStorageClsid());
        }
        fs.writeFilesystem(stream);
    }
    
    public byte[] getBytes() {
        if (HSSFWorkbook.log.check(POILogger.DEBUG)) {
            HSSFWorkbook.log.log(HSSFWorkbook.DEBUG, "HSSFWorkbook.getBytes()");
        }
        final HSSFSheet[] sheets = this.getSheets();
        final int nSheets = sheets.length;
        for (int i = 0; i < nSheets; ++i) {
            sheets[i].getSheet().preSerialize();
        }
        int totalsize = this.workbook.getSize();
        final SheetRecordCollector[] srCollectors = new SheetRecordCollector[nSheets];
        for (int k = 0; k < nSheets; ++k) {
            this.workbook.setSheetBof(k, totalsize);
            final SheetRecordCollector src = new SheetRecordCollector();
            sheets[k].getSheet().visitContainedRecords(src, totalsize);
            totalsize += src.getTotalSize();
            srCollectors[k] = src;
        }
        final byte[] retval = new byte[totalsize];
        int pos = this.workbook.serialize(0, retval);
        for (int j = 0; j < nSheets; ++j) {
            final SheetRecordCollector src2 = srCollectors[j];
            final int serializedSize = src2.serialize(pos, retval);
            if (serializedSize != src2.getTotalSize()) {
                throw new IllegalStateException("Actual serialized sheet size (" + serializedSize + ") differs from pre-calculated size (" + src2.getTotalSize() + ") for sheet (" + j + ")");
            }
            pos += serializedSize;
        }
        return retval;
    }
    
    @Deprecated
    public int addSSTString(final String string) {
        return this.workbook.addSSTString(new UnicodeString(string));
    }
    
    @Deprecated
    public String getSSTString(final int index) {
        return this.workbook.getSSTString(index).getString();
    }
    
    InternalWorkbook getWorkbook() {
        return this.workbook;
    }
    
    public int getNumberOfNames() {
        final int result = this.names.size();
        return result;
    }
    
    public HSSFName getName(final String name) {
        final int nameIndex = this.getNameIndex(name);
        if (nameIndex < 0) {
            return null;
        }
        return this.names.get(nameIndex);
    }
    
    public HSSFName getNameAt(final int nameIndex) {
        final int nNames = this.names.size();
        if (nNames < 1) {
            throw new IllegalStateException("There are no defined names in this workbook");
        }
        if (nameIndex < 0 || nameIndex > nNames) {
            throw new IllegalArgumentException("Specified name index " + nameIndex + " is outside the allowable range (0.." + (nNames - 1) + ").");
        }
        return this.names.get(nameIndex);
    }
    
    public NameRecord getNameRecord(final int nameIndex) {
        return this.getWorkbook().getNameRecord(nameIndex);
    }
    
    public String getNameName(final int index) {
        final String result = this.getNameAt(index).getNameName();
        return result;
    }
    
    public void setPrintArea(final int sheetIndex, final String reference) {
        NameRecord name = this.workbook.getSpecificBuiltinRecord((byte)6, sheetIndex + 1);
        if (name == null) {
            name = this.workbook.createBuiltInName((byte)6, sheetIndex + 1);
        }
        final String[] parts = HSSFWorkbook.COMMA_PATTERN.split(reference);
        final StringBuffer sb = new StringBuffer(32);
        for (int i = 0; i < parts.length; ++i) {
            if (i > 0) {
                sb.append(",");
            }
            SheetNameFormatter.appendFormat(sb, this.getSheetName(sheetIndex));
            sb.append("!");
            sb.append(parts[i]);
        }
        name.setNameDefinition(HSSFFormulaParser.parse(sb.toString(), this, 4, sheetIndex));
    }
    
    public void setPrintArea(final int sheetIndex, final int startColumn, final int endColumn, final int startRow, final int endRow) {
        CellReference cell = new CellReference(startRow, startColumn, true, true);
        String reference = cell.formatAsString();
        cell = new CellReference(endRow, endColumn, true, true);
        reference = reference + ":" + cell.formatAsString();
        this.setPrintArea(sheetIndex, reference);
    }
    
    public String getPrintArea(final int sheetIndex) {
        final NameRecord name = this.workbook.getSpecificBuiltinRecord((byte)6, sheetIndex + 1);
        if (name == null) {
            return null;
        }
        return HSSFFormulaParser.toFormulaString(this, name.getNameDefinition());
    }
    
    public void removePrintArea(final int sheetIndex) {
        this.getWorkbook().removeBuiltinRecord((byte)6, sheetIndex + 1);
    }
    
    public HSSFName createName() {
        final NameRecord nameRecord = this.workbook.createName();
        final HSSFName newName = new HSSFName(this, nameRecord);
        this.names.add(newName);
        return newName;
    }
    
    public int getNameIndex(final String name) {
        for (int k = 0; k < this.names.size(); ++k) {
            final String nameName = this.getNameName(k);
            if (nameName.equalsIgnoreCase(name)) {
                return k;
            }
        }
        return -1;
    }
    
    public void removeName(final int index) {
        this.names.remove(index);
        this.workbook.removeName(index);
    }
    
    public HSSFDataFormat createDataFormat() {
        if (this.formatter == null) {
            this.formatter = new HSSFDataFormat(this.workbook);
        }
        return this.formatter;
    }
    
    public void removeName(final String name) {
        final int index = this.getNameIndex(name);
        this.removeName(index);
    }
    
    public HSSFPalette getCustomPalette() {
        return new HSSFPalette(this.workbook.getCustomPalette());
    }
    
    public void insertChartRecord() {
        final int loc = this.workbook.findFirstRecordLocBySid((short)252);
        final byte[] data = { 15, 0, 0, -16, 82, 0, 0, 0, 0, 0, 6, -16, 24, 0, 0, 0, 1, 8, 0, 0, 2, 0, 0, 0, 2, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 3, 0, 0, 0, 51, 0, 11, -16, 18, 0, 0, 0, -65, 0, 8, 0, 8, 0, -127, 1, 9, 0, 0, 8, -64, 1, 64, 0, 0, 8, 64, 0, 30, -15, 16, 0, 0, 0, 13, 0, 0, 8, 12, 0, 0, 8, 23, 0, 0, 8, -9, 0, 0, 16 };
        final UnknownRecord r = new UnknownRecord(235, data);
        this.workbook.getRecords().add(loc, r);
    }
    
    public void dumpDrawingGroupRecords(final boolean fat) {
        final DrawingGroupRecord r = (DrawingGroupRecord)this.workbook.findFirstRecordBySid((short)235);
        r.decode();
        final List escherRecords = r.getEscherRecords();
        final PrintWriter w = new PrintWriter(System.out);
        for (final EscherRecord escherRecord : escherRecords) {
            if (fat) {
                System.out.println(escherRecord.toString());
            }
            else {
                escherRecord.display(w, 0);
            }
        }
        w.flush();
    }
    
    void initDrawings() {
        final DrawingManager2 mgr = this.workbook.findDrawingGroup();
        if (mgr != null) {
            for (int i = 0; i < this.getNumberOfSheets(); ++i) {
                this.getSheetAt(i).getDrawingPatriarch();
            }
        }
        else {
            this.workbook.createDrawingGroup();
        }
    }
    
    public int addPicture(final byte[] pictureData, final int format) {
        this.initDrawings();
        final byte[] uid = DigestUtils.md5(pictureData);
        final EscherBitmapBlip blipRecord = new EscherBitmapBlip();
        blipRecord.setRecordId((short)(-4072 + format));
        switch (format) {
            case 2: {
                blipRecord.setOptions((short)15680);
                break;
            }
            case 3: {
                blipRecord.setOptions((short)8544);
                break;
            }
            case 4: {
                blipRecord.setOptions((short)21536);
                break;
            }
            case 6: {
                blipRecord.setOptions((short)28160);
                break;
            }
            case 5: {
                blipRecord.setOptions((short)18080);
                break;
            }
            case 7: {
                blipRecord.setOptions((short)31360);
                break;
            }
        }
        blipRecord.setUID(uid);
        blipRecord.setMarker((byte)(-1));
        blipRecord.setPictureData(pictureData);
        final EscherBSERecord r = new EscherBSERecord();
        r.setRecordId((short)(-4089));
        r.setOptions((short)(0x2 | format << 4));
        r.setBlipTypeMacOS((byte)format);
        r.setBlipTypeWin32((byte)format);
        r.setUid(uid);
        r.setTag((short)255);
        r.setSize(pictureData.length + 25);
        r.setRef(1);
        r.setOffset(0);
        r.setBlipRecord(blipRecord);
        return this.workbook.addBSERecord(r);
    }
    
    public List<HSSFPictureData> getAllPictures() {
        final List<HSSFPictureData> pictures = new ArrayList<HSSFPictureData>();
        for (final Record r : this.workbook.getRecords()) {
            if (r instanceof AbstractEscherHolderRecord) {
                ((AbstractEscherHolderRecord)r).decode();
                final List<EscherRecord> escherRecords = ((AbstractEscherHolderRecord)r).getEscherRecords();
                this.searchForPictures(escherRecords, pictures);
            }
        }
        return pictures;
    }
    
    private void searchForPictures(final List<EscherRecord> escherRecords, final List<HSSFPictureData> pictures) {
        for (final EscherRecord escherRecord : escherRecords) {
            if (escherRecord instanceof EscherBSERecord) {
                final EscherBlipRecord blip = ((EscherBSERecord)escherRecord).getBlipRecord();
                if (blip != null) {
                    final HSSFPictureData picture = new HSSFPictureData(blip);
                    pictures.add(picture);
                }
            }
            this.searchForPictures(escherRecord.getChildRecords(), pictures);
        }
    }
    
    public boolean isWriteProtected() {
        return this.workbook.isWriteProtected();
    }
    
    public void writeProtectWorkbook(final String password, final String username) {
        this.workbook.writeProtectWorkbook(password, username);
    }
    
    public void unwriteProtectWorkbook() {
        this.workbook.unwriteProtectWorkbook();
    }
    
    public List<HSSFObjectData> getAllEmbeddedObjects() {
        final List<HSSFObjectData> objects = new ArrayList<HSSFObjectData>();
        for (int i = 0; i < this.getNumberOfSheets(); ++i) {
            this.getAllEmbeddedObjects(this.getSheetAt(i).getSheet().getRecords(), objects);
        }
        return objects;
    }
    
    private void getAllEmbeddedObjects(final List<RecordBase> records, final List<HSSFObjectData> objects) {
        for (final RecordBase obj : records) {
            if (obj instanceof ObjRecord) {
                for (final SubRecord sub : ((ObjRecord)obj).getSubRecords()) {
                    if (sub instanceof EmbeddedObjectRefSubRecord) {
                        objects.add(new HSSFObjectData((ObjRecord)obj, this.directory));
                    }
                }
            }
        }
    }
    
    public HSSFCreationHelper getCreationHelper() {
        return new HSSFCreationHelper(this);
    }
    
    UDFFinder getUDFFinder() {
        return this._udfFinder;
    }
    
    public void addToolPack(final UDFFinder toopack) {
        final AggregatingUDFFinder udfs = (AggregatingUDFFinder)this._udfFinder;
        udfs.add(toopack);
    }
    
    public void setForceFormulaRecalculation(final boolean value) {
        final InternalWorkbook iwb = this.getWorkbook();
        final RecalcIdRecord recalc = iwb.getRecalcId();
        recalc.setEngineId(0);
    }
    
    public boolean getForceFormulaRecalculation() {
        final InternalWorkbook iwb = this.getWorkbook();
        final RecalcIdRecord recalc = (RecalcIdRecord)iwb.findFirstRecordBySid((short)449);
        return recalc != null && recalc.getEngineId() != 0;
    }
    
    public boolean changeExternalReference(final String oldUrl, final String newUrl) {
        return this.workbook.changeExternalReference(oldUrl, newUrl);
    }
    
    static {
        COMMA_PATTERN = Pattern.compile(",");
        DEBUG = POILogger.DEBUG;
        HSSFWorkbook.log = POILogFactory.getLogger(HSSFWorkbook.class);
        WORKBOOK_DIR_ENTRY_NAMES = new String[] { "Workbook", "WORKBOOK" };
    }
    
    private static final class SheetRecordCollector implements RecordAggregate.RecordVisitor
    {
        private List _list;
        private int _totalSize;
        
        public SheetRecordCollector() {
            this._totalSize = 0;
            this._list = new ArrayList(128);
        }
        
        public int getTotalSize() {
            return this._totalSize;
        }
        
        public void visitRecord(final Record r) {
            this._list.add(r);
            this._totalSize += r.getRecordSize();
        }
        
        public int serialize(final int offset, final byte[] data) {
            int result = 0;
            for (int nRecs = this._list.size(), i = 0; i < nRecs; ++i) {
                final Record rec = this._list.get(i);
                result += rec.serialize(offset + result, data);
            }
            return result;
        }
    }
}
