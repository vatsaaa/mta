// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hssf.usermodel;

import org.apache.poi.ss.usermodel.ClientAnchor;

public final class HSSFClientAnchor extends HSSFAnchor implements ClientAnchor
{
    short col1;
    int row1;
    short col2;
    int row2;
    int anchorType;
    
    public HSSFClientAnchor() {
    }
    
    public HSSFClientAnchor(final int dx1, final int dy1, final int dx2, final int dy2, final short col1, final int row1, final short col2, final int row2) {
        super(dx1, dy1, dx2, dy2);
        this.checkRange(dx1, 0, 1023, "dx1");
        this.checkRange(dx2, 0, 1023, "dx2");
        this.checkRange(dy1, 0, 255, "dy1");
        this.checkRange(dy2, 0, 255, "dy2");
        this.checkRange(col1, 0, 255, "col1");
        this.checkRange(col2, 0, 255, "col2");
        this.checkRange(row1, 0, 65280, "row1");
        this.checkRange(row2, 0, 65280, "row2");
        this.col1 = col1;
        this.row1 = row1;
        this.col2 = col2;
        this.row2 = row2;
    }
    
    public float getAnchorHeightInPoints(final HSSFSheet sheet) {
        final int y1 = this.getDy1();
        final int y2 = this.getDy2();
        final int row1 = Math.min(this.getRow1(), this.getRow2());
        final int row2 = Math.max(this.getRow1(), this.getRow2());
        float points = 0.0f;
        if (row1 == row2) {
            points = (y2 - y1) / 256.0f * this.getRowHeightInPoints(sheet, row2);
        }
        else {
            points += (256.0f - y1) / 256.0f * this.getRowHeightInPoints(sheet, row1);
            for (int i = row1 + 1; i < row2; ++i) {
                points += this.getRowHeightInPoints(sheet, i);
            }
            points += y2 / 256.0f * this.getRowHeightInPoints(sheet, row2);
        }
        return points;
    }
    
    private float getRowHeightInPoints(final HSSFSheet sheet, final int rowNum) {
        final HSSFRow row = sheet.getRow(rowNum);
        if (row == null) {
            return sheet.getDefaultRowHeightInPoints();
        }
        return row.getHeightInPoints();
    }
    
    public short getCol1() {
        return this.col1;
    }
    
    public void setCol1(final short col1) {
        this.checkRange(col1, 0, 255, "col1");
        this.col1 = col1;
    }
    
    public void setCol1(final int col1) {
        this.setCol1((short)col1);
    }
    
    public short getCol2() {
        return this.col2;
    }
    
    public void setCol2(final short col2) {
        this.checkRange(col2, 0, 255, "col2");
        this.col2 = col2;
    }
    
    public void setCol2(final int col2) {
        this.setCol2((short)col2);
    }
    
    public int getRow1() {
        return this.row1;
    }
    
    public void setRow1(final int row1) {
        this.checkRange(row1, 0, 65536, "row1");
        this.row1 = row1;
    }
    
    public int getRow2() {
        return this.row2;
    }
    
    public void setRow2(final int row2) {
        this.checkRange(row2, 0, 65536, "row2");
        this.row2 = row2;
    }
    
    public void setAnchor(final short col1, final int row1, final int x1, final int y1, final short col2, final int row2, final int x2, final int y2) {
        this.checkRange(this.dx1, 0, 1023, "dx1");
        this.checkRange(this.dx2, 0, 1023, "dx2");
        this.checkRange(this.dy1, 0, 255, "dy1");
        this.checkRange(this.dy2, 0, 255, "dy2");
        this.checkRange(col1, 0, 255, "col1");
        this.checkRange(col2, 0, 255, "col2");
        this.checkRange(row1, 0, 65280, "row1");
        this.checkRange(row2, 0, 65280, "row2");
        this.col1 = col1;
        this.row1 = row1;
        this.dx1 = x1;
        this.dy1 = y1;
        this.col2 = col2;
        this.row2 = row2;
        this.dx2 = x2;
        this.dy2 = y2;
    }
    
    @Override
    public boolean isHorizontallyFlipped() {
        if (this.col1 == this.col2) {
            return this.dx1 > this.dx2;
        }
        return this.col1 > this.col2;
    }
    
    @Override
    public boolean isVerticallyFlipped() {
        if (this.row1 == this.row2) {
            return this.dy1 > this.dy2;
        }
        return this.row1 > this.row2;
    }
    
    public int getAnchorType() {
        return this.anchorType;
    }
    
    public void setAnchorType(final int anchorType) {
        this.anchorType = anchorType;
    }
    
    private void checkRange(final int value, final int minRange, final int maxRange, final String varName) {
        if (value < minRange || value > maxRange) {
            throw new IllegalArgumentException(varName + " must be between " + minRange + " and " + maxRange);
        }
    }
}
