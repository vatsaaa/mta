// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hssf.usermodel;

import org.apache.poi.ss.usermodel.RichTextString;

public class HSSFTextbox extends HSSFSimpleShape
{
    public static final short OBJECT_TYPE_TEXT = 6;
    public static final short HORIZONTAL_ALIGNMENT_LEFT = 1;
    public static final short HORIZONTAL_ALIGNMENT_CENTERED = 2;
    public static final short HORIZONTAL_ALIGNMENT_RIGHT = 3;
    public static final short HORIZONTAL_ALIGNMENT_JUSTIFIED = 4;
    public static final short HORIZONTAL_ALIGNMENT_DISTRIBUTED = 7;
    public static final short VERTICAL_ALIGNMENT_TOP = 1;
    public static final short VERTICAL_ALIGNMENT_CENTER = 2;
    public static final short VERTICAL_ALIGNMENT_BOTTOM = 3;
    public static final short VERTICAL_ALIGNMENT_JUSTIFY = 4;
    public static final short VERTICAL_ALIGNMENT_DISTRIBUTED = 7;
    int marginLeft;
    int marginRight;
    int marginTop;
    int marginBottom;
    short halign;
    short valign;
    HSSFRichTextString string;
    
    public HSSFTextbox(final HSSFShape parent, final HSSFAnchor anchor) {
        super(parent, anchor);
        this.string = new HSSFRichTextString("");
        this.setShapeType(6);
        this.halign = 1;
        this.valign = 1;
    }
    
    public HSSFRichTextString getString() {
        return this.string;
    }
    
    public void setString(final RichTextString string) {
        final HSSFRichTextString rtr = (HSSFRichTextString)string;
        if (rtr.numFormattingRuns() == 0) {
            rtr.applyFont((short)0);
        }
        this.string = rtr;
    }
    
    public int getMarginLeft() {
        return this.marginLeft;
    }
    
    public void setMarginLeft(final int marginLeft) {
        this.marginLeft = marginLeft;
    }
    
    public int getMarginRight() {
        return this.marginRight;
    }
    
    public void setMarginRight(final int marginRight) {
        this.marginRight = marginRight;
    }
    
    public int getMarginTop() {
        return this.marginTop;
    }
    
    public void setMarginTop(final int marginTop) {
        this.marginTop = marginTop;
    }
    
    public int getMarginBottom() {
        return this.marginBottom;
    }
    
    public void setMarginBottom(final int marginBottom) {
        this.marginBottom = marginBottom;
    }
    
    public short getHorizontalAlignment() {
        return this.halign;
    }
    
    public void setHorizontalAlignment(final short align) {
        this.halign = align;
    }
    
    public short getVerticalAlignment() {
        return this.valign;
    }
    
    public void setVerticalAlignment(final short align) {
        this.valign = align;
    }
}
