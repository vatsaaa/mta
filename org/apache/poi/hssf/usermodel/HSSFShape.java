// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hssf.usermodel;

public abstract class HSSFShape
{
    public static final int LINEWIDTH_ONE_PT = 12700;
    public static final int LINEWIDTH_DEFAULT = 9525;
    public static final int LINESTYLE_SOLID = 0;
    public static final int LINESTYLE_DASHSYS = 1;
    public static final int LINESTYLE_DOTSYS = 2;
    public static final int LINESTYLE_DASHDOTSYS = 3;
    public static final int LINESTYLE_DASHDOTDOTSYS = 4;
    public static final int LINESTYLE_DOTGEL = 5;
    public static final int LINESTYLE_DASHGEL = 6;
    public static final int LINESTYLE_LONGDASHGEL = 7;
    public static final int LINESTYLE_DASHDOTGEL = 8;
    public static final int LINESTYLE_LONGDASHDOTGEL = 9;
    public static final int LINESTYLE_LONGDASHDOTDOTGEL = 10;
    public static final int LINESTYLE_NONE = -1;
    final HSSFShape parent;
    HSSFAnchor anchor;
    HSSFPatriarch _patriarch;
    private int _lineStyleColor;
    int _fillColor;
    private int _lineWidth;
    private int _lineStyle;
    private boolean _noFill;
    
    HSSFShape(final HSSFShape parent, final HSSFAnchor anchor) {
        this._lineStyleColor = 134217792;
        this._fillColor = 134217737;
        this._lineWidth = 9525;
        this._lineStyle = 0;
        this._noFill = false;
        this.parent = parent;
        this.anchor = anchor;
    }
    
    public HSSFShape getParent() {
        return this.parent;
    }
    
    public HSSFAnchor getAnchor() {
        return this.anchor;
    }
    
    public void setAnchor(final HSSFAnchor anchor) {
        if (this.parent == null) {
            if (anchor instanceof HSSFChildAnchor) {
                throw new IllegalArgumentException("Must use client anchors for shapes directly attached to sheet.");
            }
        }
        else if (anchor instanceof HSSFClientAnchor) {
            throw new IllegalArgumentException("Must use child anchors for shapes attached to groups.");
        }
        this.anchor = anchor;
    }
    
    public int getLineStyleColor() {
        return this._lineStyleColor;
    }
    
    public void setLineStyleColor(final int lineStyleColor) {
        this._lineStyleColor = lineStyleColor;
    }
    
    public void setLineStyleColor(final int red, final int green, final int blue) {
        this._lineStyleColor = (blue << 16 | green << 8 | red);
    }
    
    public int getFillColor() {
        return this._fillColor;
    }
    
    public void setFillColor(final int fillColor) {
        this._fillColor = fillColor;
    }
    
    public void setFillColor(final int red, final int green, final int blue) {
        this._fillColor = (blue << 16 | green << 8 | red);
    }
    
    public int getLineWidth() {
        return this._lineWidth;
    }
    
    public void setLineWidth(final int lineWidth) {
        this._lineWidth = lineWidth;
    }
    
    public int getLineStyle() {
        return this._lineStyle;
    }
    
    public void setLineStyle(final int lineStyle) {
        this._lineStyle = lineStyle;
    }
    
    public boolean isNoFill() {
        return this._noFill;
    }
    
    public void setNoFill(final boolean noFill) {
        this._noFill = noFill;
    }
    
    public int countOfAllChildren() {
        return 1;
    }
}
