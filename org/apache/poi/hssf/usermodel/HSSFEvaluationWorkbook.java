// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hssf.usermodel;

import org.apache.poi.ss.SpreadsheetVersion;
import org.apache.poi.ss.formula.udf.UDFFinder;
import org.apache.poi.hssf.record.aggregates.FormulaRecordAggregate;
import org.apache.poi.ss.formula.ptg.Ptg;
import org.apache.poi.ss.formula.EvaluationCell;
import org.apache.poi.ss.formula.ptg.NamePtg;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.formula.EvaluationSheet;
import org.apache.poi.hssf.record.NameRecord;
import org.apache.poi.ss.formula.EvaluationName;
import org.apache.poi.ss.formula.ptg.NameXPtg;
import org.apache.poi.hssf.model.InternalWorkbook;
import org.apache.poi.ss.formula.FormulaParsingWorkbook;
import org.apache.poi.ss.formula.EvaluationWorkbook;
import org.apache.poi.ss.formula.FormulaRenderingWorkbook;

public final class HSSFEvaluationWorkbook implements FormulaRenderingWorkbook, EvaluationWorkbook, FormulaParsingWorkbook
{
    private final HSSFWorkbook _uBook;
    private final InternalWorkbook _iBook;
    
    public static HSSFEvaluationWorkbook create(final HSSFWorkbook book) {
        if (book == null) {
            return null;
        }
        return new HSSFEvaluationWorkbook(book);
    }
    
    private HSSFEvaluationWorkbook(final HSSFWorkbook book) {
        this._uBook = book;
        this._iBook = book.getWorkbook();
    }
    
    public int getExternalSheetIndex(final String sheetName) {
        final int sheetIndex = this._uBook.getSheetIndex(sheetName);
        return this._iBook.checkExternSheet(sheetIndex);
    }
    
    public int getExternalSheetIndex(final String workbookName, final String sheetName) {
        return this._iBook.getExternalSheetIndex(workbookName, sheetName);
    }
    
    public NameXPtg getNameXPtg(final String name) {
        return this._iBook.getNameXPtg(name, this._uBook.getUDFFinder());
    }
    
    public EvaluationName getName(final String name, final int sheetIndex) {
        for (int i = 0; i < this._iBook.getNumNames(); ++i) {
            final NameRecord nr = this._iBook.getNameRecord(i);
            if (nr.getSheetNumber() == sheetIndex + 1 && name.equalsIgnoreCase(nr.getNameText())) {
                return new Name(nr, i);
            }
        }
        return (sheetIndex == -1) ? null : this.getName(name, -1);
    }
    
    public int getSheetIndex(final EvaluationSheet evalSheet) {
        final HSSFSheet sheet = ((HSSFEvaluationSheet)evalSheet).getHSSFSheet();
        return this._uBook.getSheetIndex(sheet);
    }
    
    public int getSheetIndex(final String sheetName) {
        return this._uBook.getSheetIndex(sheetName);
    }
    
    public String getSheetName(final int sheetIndex) {
        return this._uBook.getSheetName(sheetIndex);
    }
    
    public EvaluationSheet getSheet(final int sheetIndex) {
        return new HSSFEvaluationSheet(this._uBook.getSheetAt(sheetIndex));
    }
    
    public int convertFromExternSheetIndex(final int externSheetIndex) {
        return this._iBook.getSheetIndexFromExternSheetIndex(externSheetIndex);
    }
    
    public ExternalSheet getExternalSheet(final int externSheetIndex) {
        return this._iBook.getExternalSheet(externSheetIndex);
    }
    
    public ExternalName getExternalName(final int externSheetIndex, final int externNameIndex) {
        return this._iBook.getExternalName(externSheetIndex, externNameIndex);
    }
    
    public String resolveNameXText(final NameXPtg n) {
        return this._iBook.resolveNameXText(n.getSheetRefIndex(), n.getNameIndex());
    }
    
    public String getSheetNameByExternSheet(final int externSheetIndex) {
        return this._iBook.findSheetNameFromExternSheet(externSheetIndex);
    }
    
    public String getNameText(final NamePtg namePtg) {
        return this._iBook.getNameRecord(namePtg.getIndex()).getNameText();
    }
    
    public EvaluationName getName(final NamePtg namePtg) {
        final int ix = namePtg.getIndex();
        return new Name(this._iBook.getNameRecord(ix), ix);
    }
    
    public Ptg[] getFormulaTokens(final EvaluationCell evalCell) {
        final HSSFCell cell = ((HSSFEvaluationCell)evalCell).getHSSFCell();
        final FormulaRecordAggregate fra = (FormulaRecordAggregate)cell.getCellValueRecord();
        return fra.getFormulaTokens();
    }
    
    public UDFFinder getUDFFinder() {
        return this._uBook.getUDFFinder();
    }
    
    public SpreadsheetVersion getSpreadsheetVersion() {
        return SpreadsheetVersion.EXCEL97;
    }
    
    private static final class Name implements EvaluationName
    {
        private final NameRecord _nameRecord;
        private final int _index;
        
        public Name(final NameRecord nameRecord, final int index) {
            this._nameRecord = nameRecord;
            this._index = index;
        }
        
        public Ptg[] getNameDefinition() {
            return this._nameRecord.getNameDefinition();
        }
        
        public String getNameText() {
            return this._nameRecord.getNameText();
        }
        
        public boolean hasFormula() {
            return this._nameRecord.hasFormula();
        }
        
        public boolean isFunctionName() {
            return this._nameRecord.isFunctionName();
        }
        
        public boolean isRange() {
            return this._nameRecord.hasFormula();
        }
        
        public NamePtg createPtg() {
            return new NamePtg(this._index);
        }
    }
}
