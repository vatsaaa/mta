// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hssf.usermodel;

import org.apache.poi.ss.usermodel.RichTextString;
import org.apache.poi.hssf.record.TextObjectRecord;
import org.apache.poi.hssf.record.NoteRecord;
import org.apache.poi.ss.usermodel.Comment;

public class HSSFComment extends HSSFTextbox implements Comment
{
    private boolean _visible;
    private int _row;
    private int _col;
    private String _author;
    private NoteRecord _note;
    private TextObjectRecord _txo;
    
    public HSSFComment(final HSSFShape parent, final HSSFAnchor anchor) {
        super(parent, anchor);
        this.setShapeType(25);
        this._fillColor = 134217808;
        this._visible = false;
        this._author = "";
    }
    
    protected HSSFComment(final NoteRecord note, final TextObjectRecord txo) {
        this(null, (HSSFAnchor)null);
        this._txo = txo;
        this._note = note;
    }
    
    public void setVisible(final boolean visible) {
        if (this._note != null) {
            this._note.setFlags((short)(visible ? 2 : 0));
        }
        this._visible = visible;
    }
    
    public boolean isVisible() {
        return this._visible;
    }
    
    public int getRow() {
        return this._row;
    }
    
    public void setRow(final int row) {
        if (this._note != null) {
            this._note.setRow(row);
        }
        this._row = row;
    }
    
    public int getColumn() {
        return this._col;
    }
    
    public void setColumn(final int col) {
        if (this._note != null) {
            this._note.setColumn(col);
        }
        this._col = col;
    }
    
    @Deprecated
    public void setColumn(final short col) {
        this.setColumn((int)col);
    }
    
    public String getAuthor() {
        return this._author;
    }
    
    public void setAuthor(final String author) {
        if (this._note != null) {
            this._note.setAuthor(author);
        }
        this._author = author;
    }
    
    @Override
    public void setString(final RichTextString string) {
        final HSSFRichTextString hstring = (HSSFRichTextString)string;
        if (hstring.numFormattingRuns() == 0) {
            hstring.applyFont((short)0);
        }
        if (this._txo != null) {
            this._txo.setStr(hstring);
        }
        super.setString(string);
    }
    
    protected NoteRecord getNoteRecord() {
        return this._note;
    }
    
    protected TextObjectRecord getTextObjectRecord() {
        return this._txo;
    }
}
