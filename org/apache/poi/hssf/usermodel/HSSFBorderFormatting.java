// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hssf.usermodel;

import org.apache.poi.hssf.record.CFRuleRecord;
import org.apache.poi.ss.usermodel.BorderFormatting;

public final class HSSFBorderFormatting implements BorderFormatting
{
    private final CFRuleRecord cfRuleRecord;
    private final org.apache.poi.hssf.record.cf.BorderFormatting borderFormatting;
    
    protected HSSFBorderFormatting(final CFRuleRecord cfRuleRecord) {
        this.cfRuleRecord = cfRuleRecord;
        this.borderFormatting = cfRuleRecord.getBorderFormatting();
    }
    
    protected org.apache.poi.hssf.record.cf.BorderFormatting getBorderFormattingBlock() {
        return this.borderFormatting;
    }
    
    public short getBorderBottom() {
        return (short)this.borderFormatting.getBorderBottom();
    }
    
    public short getBorderDiagonal() {
        return (short)this.borderFormatting.getBorderDiagonal();
    }
    
    public short getBorderLeft() {
        return (short)this.borderFormatting.getBorderLeft();
    }
    
    public short getBorderRight() {
        return (short)this.borderFormatting.getBorderRight();
    }
    
    public short getBorderTop() {
        return (short)this.borderFormatting.getBorderTop();
    }
    
    public short getBottomBorderColor() {
        return (short)this.borderFormatting.getBottomBorderColor();
    }
    
    public short getDiagonalBorderColor() {
        return (short)this.borderFormatting.getDiagonalBorderColor();
    }
    
    public short getLeftBorderColor() {
        return (short)this.borderFormatting.getLeftBorderColor();
    }
    
    public short getRightBorderColor() {
        return (short)this.borderFormatting.getRightBorderColor();
    }
    
    public short getTopBorderColor() {
        return (short)this.borderFormatting.getTopBorderColor();
    }
    
    public boolean isBackwardDiagonalOn() {
        return this.borderFormatting.isBackwardDiagonalOn();
    }
    
    public boolean isForwardDiagonalOn() {
        return this.borderFormatting.isForwardDiagonalOn();
    }
    
    public void setBackwardDiagonalOn(final boolean on) {
        this.borderFormatting.setBackwardDiagonalOn(on);
        if (on) {
            this.cfRuleRecord.setTopLeftBottomRightBorderModified(on);
        }
    }
    
    public void setBorderBottom(final short border) {
        this.borderFormatting.setBorderBottom(border);
        if (border != 0) {
            this.cfRuleRecord.setBottomBorderModified(true);
        }
    }
    
    public void setBorderDiagonal(final short border) {
        this.borderFormatting.setBorderDiagonal(border);
        if (border != 0) {
            this.cfRuleRecord.setBottomLeftTopRightBorderModified(true);
            this.cfRuleRecord.setTopLeftBottomRightBorderModified(true);
        }
    }
    
    public void setBorderLeft(final short border) {
        this.borderFormatting.setBorderLeft(border);
        if (border != 0) {
            this.cfRuleRecord.setLeftBorderModified(true);
        }
    }
    
    public void setBorderRight(final short border) {
        this.borderFormatting.setBorderRight(border);
        if (border != 0) {
            this.cfRuleRecord.setRightBorderModified(true);
        }
    }
    
    public void setBorderTop(final short border) {
        this.borderFormatting.setBorderTop(border);
        if (border != 0) {
            this.cfRuleRecord.setTopBorderModified(true);
        }
    }
    
    public void setBottomBorderColor(final short color) {
        this.borderFormatting.setBottomBorderColor(color);
        if (color != 0) {
            this.cfRuleRecord.setBottomBorderModified(true);
        }
    }
    
    public void setDiagonalBorderColor(final short color) {
        this.borderFormatting.setDiagonalBorderColor(color);
        if (color != 0) {
            this.cfRuleRecord.setBottomLeftTopRightBorderModified(true);
            this.cfRuleRecord.setTopLeftBottomRightBorderModified(true);
        }
    }
    
    public void setForwardDiagonalOn(final boolean on) {
        this.borderFormatting.setForwardDiagonalOn(on);
        if (on) {
            this.cfRuleRecord.setBottomLeftTopRightBorderModified(on);
        }
    }
    
    public void setLeftBorderColor(final short color) {
        this.borderFormatting.setLeftBorderColor(color);
        if (color != 0) {
            this.cfRuleRecord.setLeftBorderModified(true);
        }
    }
    
    public void setRightBorderColor(final short color) {
        this.borderFormatting.setRightBorderColor(color);
        if (color != 0) {
            this.cfRuleRecord.setRightBorderModified(true);
        }
    }
    
    public void setTopBorderColor(final short color) {
        this.borderFormatting.setTopBorderColor(color);
        if (color != 0) {
            this.cfRuleRecord.setTopBorderModified(true);
        }
    }
}
