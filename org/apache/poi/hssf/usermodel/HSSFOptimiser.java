// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hssf.usermodel;

import org.apache.poi.hssf.record.common.UnicodeString;
import java.util.Iterator;
import org.apache.poi.hssf.record.ExtendedFormatRecord;
import java.util.HashSet;
import org.apache.poi.hssf.record.FontRecord;

public class HSSFOptimiser
{
    public static void optimiseFonts(final HSSFWorkbook workbook) {
        final short[] newPos = new short[workbook.getWorkbook().getNumberOfFontRecords() + 1];
        final boolean[] zapRecords = new boolean[newPos.length];
        for (int i = 0; i < newPos.length; ++i) {
            newPos[i] = (short)i;
            zapRecords[i] = false;
        }
        final FontRecord[] frecs = new FontRecord[newPos.length];
        for (int j = 0; j < newPos.length; ++j) {
            if (j != 4) {
                frecs[j] = workbook.getWorkbook().getFontRecordAt(j);
            }
        }
        for (int j = 5; j < newPos.length; ++j) {
            int earlierDuplicate = -1;
            for (int k = 0; k < j && earlierDuplicate == -1; ++k) {
                if (k != 4) {
                    final FontRecord frCheck = workbook.getWorkbook().getFontRecordAt(k);
                    if (frCheck.sameProperties(frecs[j])) {
                        earlierDuplicate = k;
                    }
                }
            }
            if (earlierDuplicate != -1) {
                newPos[j] = (short)earlierDuplicate;
                zapRecords[j] = true;
            }
        }
        for (int j = 5; j < newPos.length; ++j) {
            short newPosition;
            final short preDeletePos = newPosition = newPos[j];
            for (int l = 0; l < preDeletePos; ++l) {
                if (zapRecords[l]) {
                    --newPosition;
                }
            }
            newPos[j] = newPosition;
        }
        for (int j = 5; j < newPos.length; ++j) {
            if (zapRecords[j]) {
                workbook.getWorkbook().removeFontRecord(frecs[j]);
            }
        }
        workbook.resetFontCache();
        for (int j = 0; j < workbook.getWorkbook().getNumExFormats(); ++j) {
            final ExtendedFormatRecord xfr = workbook.getWorkbook().getExFormatAt(j);
            xfr.setFontIndex(newPos[xfr.getFontIndex()]);
        }
        final HashSet doneUnicodeStrings = new HashSet();
        for (int sheetNum = 0; sheetNum < workbook.getNumberOfSheets(); ++sheetNum) {
            final HSSFSheet s = workbook.getSheetAt(sheetNum);
            final Iterator rIt = s.rowIterator();
            while (rIt.hasNext()) {
                final HSSFRow row = rIt.next();
                final Iterator cIt = row.cellIterator();
                while (cIt.hasNext()) {
                    final HSSFCell cell = cIt.next();
                    if (cell.getCellType() == 1) {
                        final HSSFRichTextString rtr = cell.getRichStringCellValue();
                        final UnicodeString u = rtr.getRawUnicodeString();
                        if (doneUnicodeStrings.contains(u)) {
                            continue;
                        }
                        for (short m = 5; m < newPos.length; ++m) {
                            if (m != newPos[m]) {
                                u.swapFontUse(m, newPos[m]);
                            }
                        }
                        doneUnicodeStrings.add(u);
                    }
                }
            }
        }
    }
    
    public static void optimiseCellStyles(final HSSFWorkbook workbook) {
        final short[] newPos = new short[workbook.getWorkbook().getNumExFormats()];
        final boolean[] zapRecords = new boolean[newPos.length];
        for (int i = 0; i < newPos.length; ++i) {
            newPos[i] = (short)i;
            zapRecords[i] = false;
        }
        final ExtendedFormatRecord[] xfrs = new ExtendedFormatRecord[newPos.length];
        for (int j = 0; j < newPos.length; ++j) {
            xfrs[j] = workbook.getWorkbook().getExFormatAt(j);
        }
        for (int j = 21; j < newPos.length; ++j) {
            int earlierDuplicate = -1;
            for (int k = 0; k < j && earlierDuplicate == -1; ++k) {
                final ExtendedFormatRecord xfCheck = workbook.getWorkbook().getExFormatAt(k);
                if (xfCheck.equals(xfrs[j])) {
                    earlierDuplicate = k;
                }
            }
            if (earlierDuplicate != -1) {
                newPos[j] = (short)earlierDuplicate;
                zapRecords[j] = true;
            }
        }
        for (int j = 21; j < newPos.length; ++j) {
            short newPosition;
            final short preDeletePos = newPosition = newPos[j];
            for (int l = 0; l < preDeletePos; ++l) {
                if (zapRecords[l]) {
                    --newPosition;
                }
            }
            newPos[j] = newPosition;
        }
        for (int j = 21; j < newPos.length; ++j) {
            if (zapRecords[j]) {
                workbook.getWorkbook().removeExFormatRecord(xfrs[j]);
            }
        }
        for (int sheetNum = 0; sheetNum < workbook.getNumberOfSheets(); ++sheetNum) {
            final HSSFSheet s = workbook.getSheetAt(sheetNum);
            final Iterator rIt = s.rowIterator();
            while (rIt.hasNext()) {
                final HSSFRow row = rIt.next();
                final Iterator cIt = row.cellIterator();
                while (cIt.hasNext()) {
                    final HSSFCell cell = cIt.next();
                    final short oldXf = cell.getCellValueRecord().getXFIndex();
                    final HSSFCellStyle newStyle = workbook.getCellStyleAt(newPos[oldXf]);
                    cell.setCellStyle(newStyle);
                }
            }
        }
    }
}
