// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hssf.usermodel;

public final class HSSFChildAnchor extends HSSFAnchor
{
    public HSSFChildAnchor() {
    }
    
    public HSSFChildAnchor(final int dx1, final int dy1, final int dx2, final int dy2) {
        super(dx1, dy1, dx2, dy2);
    }
    
    public void setAnchor(final int dx1, final int dy1, final int dx2, final int dy2) {
        this.dx1 = dx1;
        this.dy1 = dy1;
        this.dx2 = dx2;
        this.dy2 = dy2;
    }
    
    @Override
    public boolean isHorizontallyFlipped() {
        return this.dx1 > this.dx2;
    }
    
    @Override
    public boolean isVerticallyFlipped() {
        return this.dy1 > this.dy2;
    }
}
