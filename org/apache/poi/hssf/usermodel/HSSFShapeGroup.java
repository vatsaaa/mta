// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hssf.usermodel;

import java.util.Iterator;
import java.util.ArrayList;
import java.util.List;

public class HSSFShapeGroup extends HSSFShape implements HSSFShapeContainer
{
    List<HSSFShape> shapes;
    int x1;
    int y1;
    int x2;
    int y2;
    
    public HSSFShapeGroup(final HSSFShape parent, final HSSFAnchor anchor) {
        super(parent, anchor);
        this.shapes = new ArrayList<HSSFShape>();
        this.x1 = 0;
        this.y1 = 0;
        this.x2 = 1023;
        this.y2 = 255;
    }
    
    public HSSFShapeGroup createGroup(final HSSFChildAnchor anchor) {
        final HSSFShapeGroup group = new HSSFShapeGroup(this, anchor);
        group.anchor = anchor;
        this.shapes.add(group);
        return group;
    }
    
    public void addShape(final HSSFShape shape) {
        shape._patriarch = this._patriarch;
        this.shapes.add(shape);
    }
    
    public HSSFSimpleShape createShape(final HSSFChildAnchor anchor) {
        final HSSFSimpleShape shape = new HSSFSimpleShape(this, anchor);
        shape.anchor = anchor;
        this.shapes.add(shape);
        return shape;
    }
    
    public HSSFTextbox createTextbox(final HSSFChildAnchor anchor) {
        final HSSFTextbox shape = new HSSFTextbox(this, anchor);
        shape.anchor = anchor;
        this.shapes.add(shape);
        return shape;
    }
    
    public HSSFPolygon createPolygon(final HSSFChildAnchor anchor) {
        final HSSFPolygon shape = new HSSFPolygon(this, anchor);
        shape.anchor = anchor;
        this.shapes.add(shape);
        return shape;
    }
    
    public HSSFPicture createPicture(final HSSFChildAnchor anchor, final int pictureIndex) {
        final HSSFPicture shape = new HSSFPicture(this, anchor);
        shape.anchor = anchor;
        shape.setPictureIndex(pictureIndex);
        this.shapes.add(shape);
        return shape;
    }
    
    public List<HSSFShape> getChildren() {
        return this.shapes;
    }
    
    public void setCoordinates(final int x1, final int y1, final int x2, final int y2) {
        this.x1 = x1;
        this.y1 = y1;
        this.x2 = x2;
        this.y2 = y2;
    }
    
    public int getX1() {
        return this.x1;
    }
    
    public int getY1() {
        return this.y1;
    }
    
    public int getX2() {
        return this.x2;
    }
    
    public int getY2() {
        return this.y2;
    }
    
    @Override
    public int countOfAllChildren() {
        int count = this.shapes.size();
        for (final HSSFShape shape : this.shapes) {
            count += shape.countOfAllChildren();
        }
        return count;
    }
}
