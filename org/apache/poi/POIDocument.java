// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi;

import org.apache.poi.util.POILogFactory;
import org.apache.poi.util.Internal;
import org.apache.poi.poifs.filesystem.Entry;
import org.apache.poi.poifs.filesystem.DirectoryEntry;
import org.apache.poi.poifs.filesystem.EntryUtils;
import org.apache.poi.hpsf.WritingNotSupportedException;
import java.io.ByteArrayInputStream;
import java.io.OutputStream;
import java.io.ByteArrayOutputStream;
import org.apache.poi.hpsf.MutablePropertySet;
import java.util.List;
import org.apache.poi.poifs.filesystem.DocumentInputStream;
import org.apache.poi.hpsf.HPSFException;
import java.io.InputStream;
import java.io.IOException;
import org.apache.poi.hpsf.PropertySet;
import org.apache.poi.hpsf.PropertySetFactory;
import org.apache.poi.poifs.filesystem.NPOIFSFileSystem;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.util.POILogger;
import org.apache.poi.poifs.filesystem.DirectoryNode;
import org.apache.poi.hpsf.DocumentSummaryInformation;
import org.apache.poi.hpsf.SummaryInformation;

public abstract class POIDocument
{
    private SummaryInformation sInf;
    private DocumentSummaryInformation dsInf;
    protected DirectoryNode directory;
    private static final POILogger logger;
    private boolean initialized;
    
    protected POIDocument(final DirectoryNode dir) {
        this.initialized = false;
        this.directory = dir;
    }
    
    @Deprecated
    protected POIDocument(final DirectoryNode dir, final POIFSFileSystem fs) {
        this.initialized = false;
        this.directory = dir;
    }
    
    protected POIDocument(final POIFSFileSystem fs) {
        this(fs.getRoot());
    }
    
    protected POIDocument(final NPOIFSFileSystem fs) {
        this(fs.getRoot());
    }
    
    public DocumentSummaryInformation getDocumentSummaryInformation() {
        if (!this.initialized) {
            this.readProperties();
        }
        return this.dsInf;
    }
    
    public SummaryInformation getSummaryInformation() {
        if (!this.initialized) {
            this.readProperties();
        }
        return this.sInf;
    }
    
    public void createInformationProperties() {
        if (!this.initialized) {
            this.readProperties();
        }
        if (this.sInf == null) {
            this.sInf = PropertySetFactory.newSummaryInformation();
        }
        if (this.dsInf == null) {
            this.dsInf = PropertySetFactory.newDocumentSummaryInformation();
        }
    }
    
    protected void readProperties() {
        PropertySet ps = this.getPropertySet("\u0005DocumentSummaryInformation");
        if (ps != null && ps instanceof DocumentSummaryInformation) {
            this.dsInf = (DocumentSummaryInformation)ps;
        }
        else if (ps != null) {
            POIDocument.logger.log(POILogger.WARN, "DocumentSummaryInformation property set came back with wrong class - ", ps.getClass());
        }
        ps = this.getPropertySet("\u0005SummaryInformation");
        if (ps instanceof SummaryInformation) {
            this.sInf = (SummaryInformation)ps;
        }
        else if (ps != null) {
            POIDocument.logger.log(POILogger.WARN, "SummaryInformation property set came back with wrong class - ", ps.getClass());
        }
        this.initialized = true;
    }
    
    protected PropertySet getPropertySet(final String setName) {
        if (this.directory == null) {
            return null;
        }
        DocumentInputStream dis;
        try {
            dis = this.directory.createDocumentInputStream(this.directory.getEntry(setName));
        }
        catch (IOException ie) {
            POIDocument.logger.log(POILogger.WARN, "Error getting property set with name " + setName + "\n" + ie);
            return null;
        }
        try {
            final PropertySet set = PropertySetFactory.create(dis);
            return set;
        }
        catch (IOException ie) {
            POIDocument.logger.log(POILogger.WARN, "Error creating property set with name " + setName + "\n" + ie);
        }
        catch (HPSFException he) {
            POIDocument.logger.log(POILogger.WARN, "Error creating property set with name " + setName + "\n" + he);
        }
        return null;
    }
    
    protected void writeProperties(final POIFSFileSystem outFS) throws IOException {
        this.writeProperties(outFS, null);
    }
    
    protected void writeProperties(final POIFSFileSystem outFS, final List<String> writtenEntries) throws IOException {
        final SummaryInformation si = this.getSummaryInformation();
        if (si != null) {
            this.writePropertySet("\u0005SummaryInformation", si, outFS);
            if (writtenEntries != null) {
                writtenEntries.add("\u0005SummaryInformation");
            }
        }
        final DocumentSummaryInformation dsi = this.getDocumentSummaryInformation();
        if (dsi != null) {
            this.writePropertySet("\u0005DocumentSummaryInformation", dsi, outFS);
            if (writtenEntries != null) {
                writtenEntries.add("\u0005DocumentSummaryInformation");
            }
        }
    }
    
    protected void writePropertySet(final String name, final PropertySet set, final POIFSFileSystem outFS) throws IOException {
        try {
            final MutablePropertySet mSet = new MutablePropertySet(set);
            final ByteArrayOutputStream bOut = new ByteArrayOutputStream();
            mSet.write(bOut);
            final byte[] data = bOut.toByteArray();
            final ByteArrayInputStream bIn = new ByteArrayInputStream(data);
            outFS.createDocument(bIn, name);
            POIDocument.logger.log(POILogger.INFO, "Wrote property set " + name + " of size " + data.length);
        }
        catch (WritingNotSupportedException wnse) {
            System.err.println("Couldn't write property set with name " + name + " as not supported by HPSF yet");
        }
    }
    
    public abstract void write(final OutputStream p0) throws IOException;
    
    @Deprecated
    protected void copyNodes(final POIFSFileSystem source, final POIFSFileSystem target, final List<String> excepts) throws IOException {
        EntryUtils.copyNodes(source, target, excepts);
    }
    
    @Deprecated
    protected void copyNodes(final DirectoryNode sourceRoot, final DirectoryNode targetRoot, final List<String> excepts) throws IOException {
        EntryUtils.copyNodes(sourceRoot, targetRoot, excepts);
    }
    
    @Internal
    @Deprecated
    protected void copyNodeRecursively(final Entry entry, final DirectoryEntry target) throws IOException {
        EntryUtils.copyNodeRecursively(entry, target);
    }
    
    static {
        logger = POILogFactory.getLogger(POIDocument.class);
    }
}
