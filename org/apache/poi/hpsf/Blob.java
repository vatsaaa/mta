// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hpsf;

import org.apache.poi.util.LittleEndian;
import org.apache.poi.util.Internal;

@Internal
class Blob
{
    private byte[] _value;
    
    Blob(final byte[] data, final int offset) {
        final int size = LittleEndian.getInt(data, offset);
        if (size == 0) {
            this._value = new byte[0];
            return;
        }
        this._value = LittleEndian.getByteArray(data, offset + 4, size);
    }
    
    int getSize() {
        return 4 + this._value.length;
    }
}
