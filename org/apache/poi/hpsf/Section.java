// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hpsf;

import org.apache.poi.hpsf.wellknown.SectionIDMap;
import java.io.UnsupportedEncodingException;
import java.util.Iterator;
import java.util.List;
import java.util.Collections;
import java.util.ArrayList;
import org.apache.poi.util.LittleEndian;
import java.util.Map;

public class Section
{
    protected Map<Long, String> dictionary;
    protected ClassID formatID;
    protected long offset;
    protected int size;
    protected Property[] properties;
    private boolean wasNull;
    
    public ClassID getFormatID() {
        return this.formatID;
    }
    
    public long getOffset() {
        return this.offset;
    }
    
    public int getSize() {
        return this.size;
    }
    
    public int getPropertyCount() {
        return this.properties.length;
    }
    
    public Property[] getProperties() {
        return this.properties;
    }
    
    protected Section() {
    }
    
    public Section(final byte[] src, final int offset) throws UnsupportedEncodingException {
        int o1 = offset;
        this.formatID = new ClassID(src, o1);
        o1 += 16;
        this.offset = LittleEndian.getUInt(src, o1);
        o1 = (int)this.offset;
        this.size = (int)LittleEndian.getUInt(src, o1);
        o1 += 4;
        final int propertyCount = (int)LittleEndian.getUInt(src, o1);
        o1 += 4;
        this.properties = new Property[propertyCount];
        int pass1Offset = o1;
        final List<PropertyListEntry> propertyList = new ArrayList<PropertyListEntry>(propertyCount);
        for (int i = 0; i < this.properties.length; ++i) {
            final PropertyListEntry ple = new PropertyListEntry();
            ple.id = (int)LittleEndian.getUInt(src, pass1Offset);
            pass1Offset += 4;
            ple.offset = (int)LittleEndian.getUInt(src, pass1Offset);
            pass1Offset += 4;
            propertyList.add(ple);
        }
        Collections.sort(propertyList);
        for (int i = 0; i < propertyCount - 1; ++i) {
            final PropertyListEntry ple2 = propertyList.get(i);
            final PropertyListEntry ple3 = propertyList.get(i + 1);
            ple2.length = ple3.offset - ple2.offset;
        }
        if (propertyCount > 0) {
            final PropertyListEntry ple = propertyList.get(propertyCount - 1);
            ple.length = this.size - ple.offset;
        }
        int codepage = -1;
        int o2 = 0;
        for (Iterator<PropertyListEntry> j = propertyList.iterator(); codepage == -1 && j.hasNext(); codepage = LittleEndian.getUShort(src, o2)) {
            final PropertyListEntry ple = j.next();
            if (ple.id == 1) {
                o2 = (int)(this.offset + ple.offset);
                final long type = LittleEndian.getUInt(src, o2);
                o2 += 4;
                if (type != 2L) {
                    throw new HPSFRuntimeException("Value type of property ID 1 is not VT_I2 but " + type + ".");
                }
            }
        }
        int i2 = 0;
        final Iterator<PropertyListEntry> k = propertyList.iterator();
        while (k.hasNext()) {
            final PropertyListEntry ple = k.next();
            Property p = new Property(ple.id, src, this.offset + ple.offset, ple.length, codepage);
            if (p.getID() == 1L) {
                p = new Property(p.getID(), p.getType(), codepage);
            }
            this.properties[i2++] = p;
        }
        this.dictionary = (Map<Long, String>)this.getProperty(0L);
    }
    
    public Object getProperty(final long id) {
        this.wasNull = false;
        for (int i = 0; i < this.properties.length; ++i) {
            if (id == this.properties[i].getID()) {
                return this.properties[i].getValue();
            }
        }
        this.wasNull = true;
        return null;
    }
    
    protected int getPropertyIntValue(final long id) {
        final Object o = this.getProperty(id);
        if (o == null) {
            return 0;
        }
        if (!(o instanceof Long) && !(o instanceof Integer)) {
            throw new HPSFRuntimeException("This property is not an integer type, but " + o.getClass().getName() + ".");
        }
        final Number i = (Number)o;
        return i.intValue();
    }
    
    protected boolean getPropertyBooleanValue(final int id) {
        final Boolean b = (Boolean)this.getProperty(id);
        return b != null && b;
    }
    
    public boolean wasNull() {
        return this.wasNull;
    }
    
    public String getPIDString(final long pid) {
        String s = null;
        if (this.dictionary != null) {
            s = this.dictionary.get(pid);
        }
        if (s == null) {
            s = SectionIDMap.getPIDString(this.getFormatID().getBytes(), pid);
        }
        if (s == null) {
            s = "[undefined]";
        }
        return s;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (o == null || !(o instanceof Section)) {
            return false;
        }
        final Section s = (Section)o;
        if (!s.getFormatID().equals(this.getFormatID())) {
            return false;
        }
        Property[] pa1 = new Property[this.getProperties().length];
        Property[] pa2 = new Property[s.getProperties().length];
        System.arraycopy(this.getProperties(), 0, pa1, 0, pa1.length);
        System.arraycopy(s.getProperties(), 0, pa2, 0, pa2.length);
        Property p10 = null;
        Property p11 = null;
        for (int i = 0; i < pa1.length; ++i) {
            final long id = pa1[i].getID();
            if (id == 0L) {
                p10 = pa1[i];
                pa1 = this.remove(pa1, i);
                --i;
            }
            if (id == 1L) {
                pa1 = this.remove(pa1, i);
                --i;
            }
        }
        for (int i = 0; i < pa2.length; ++i) {
            final long id = pa2[i].getID();
            if (id == 0L) {
                p11 = pa2[i];
                pa2 = this.remove(pa2, i);
                --i;
            }
            if (id == 1L) {
                pa2 = this.remove(pa2, i);
                --i;
            }
        }
        if (pa1.length != pa2.length) {
            return false;
        }
        boolean dictionaryEqual = true;
        if (p10 != null && p11 != null) {
            dictionaryEqual = p10.getValue().equals(p11.getValue());
        }
        else if (p10 != null || p11 != null) {
            dictionaryEqual = false;
        }
        return dictionaryEqual && Util.equals(pa1, pa2);
    }
    
    private Property[] remove(final Property[] pa, final int i) {
        final Property[] h = new Property[pa.length - 1];
        if (i > 0) {
            System.arraycopy(pa, 0, h, 0, i);
        }
        System.arraycopy(pa, i + 1, h, i, h.length - i);
        return h;
    }
    
    @Override
    public int hashCode() {
        long hashCode = 0L;
        hashCode += this.getFormatID().hashCode();
        final Property[] pa = this.getProperties();
        for (int i = 0; i < pa.length; ++i) {
            hashCode += pa[i].hashCode();
        }
        final int returnHashCode = (int)(hashCode & 0xFFFFFFFFL);
        return returnHashCode;
    }
    
    @Override
    public String toString() {
        final StringBuffer b = new StringBuffer();
        final Property[] pa = this.getProperties();
        b.append(this.getClass().getName());
        b.append('[');
        b.append("formatID: ");
        b.append(this.getFormatID());
        b.append(", offset: ");
        b.append(this.getOffset());
        b.append(", propertyCount: ");
        b.append(this.getPropertyCount());
        b.append(", size: ");
        b.append(this.getSize());
        b.append(", properties: [\n");
        for (int i = 0; i < pa.length; ++i) {
            b.append(pa[i].toString());
            b.append(",\n");
        }
        b.append(']');
        b.append(']');
        return b.toString();
    }
    
    public Map<Long, String> getDictionary() {
        return this.dictionary;
    }
    
    public int getCodepage() {
        final Integer codepage = (Integer)this.getProperty(1L);
        if (codepage == null) {
            return -1;
        }
        final int cp = codepage;
        return cp;
    }
    
    class PropertyListEntry implements Comparable<PropertyListEntry>
    {
        int id;
        int offset;
        int length;
        
        public int compareTo(final PropertyListEntry o) {
            final int otherOffset = o.offset;
            if (this.offset < otherOffset) {
                return -1;
            }
            if (this.offset == otherOffset) {
                return 0;
            }
            return 1;
        }
        
        @Override
        public String toString() {
            final StringBuffer b = new StringBuffer();
            b.append(this.getClass().getName());
            b.append("[id=");
            b.append(this.id);
            b.append(", offset=");
            b.append(this.offset);
            b.append(", length=");
            b.append(this.length);
            b.append(']');
            return b.toString();
        }
    }
}
