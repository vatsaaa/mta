// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hpsf;

import org.apache.poi.util.LittleEndian;
import org.apache.poi.util.Internal;

@Internal
class Array
{
    private ArrayHeader _header;
    private TypedPropertyValue[] _values;
    
    Array() {
    }
    
    Array(final byte[] data, final int offset) {
        this.read(data, offset);
    }
    
    int read(final byte[] data, final int startOffset) {
        int offset = startOffset;
        this._header = new ArrayHeader(data, offset);
        offset += this._header.getSize();
        final long numberOfScalarsLong = this._header.getNumberOfScalarValues();
        if (numberOfScalarsLong > 2147483647L) {
            throw new UnsupportedOperationException("Sorry, but POI can't store array of properties with size of " + numberOfScalarsLong + " in memory");
        }
        final int numberOfScalars = (int)numberOfScalarsLong;
        this._values = new TypedPropertyValue[numberOfScalars];
        final int type = this._header._type;
        if (type == 12) {
            for (int i = 0; i < numberOfScalars; ++i) {
                final TypedPropertyValue typedPropertyValue = new TypedPropertyValue();
                offset += typedPropertyValue.read(data, offset);
            }
        }
        else {
            for (int i = 0; i < numberOfScalars; ++i) {
                final TypedPropertyValue typedPropertyValue = new TypedPropertyValue(type, null);
                offset += typedPropertyValue.readValuePadded(data, offset);
            }
        }
        return offset - startOffset;
    }
    
    static class ArrayDimension
    {
        static final int SIZE = 8;
        private int _indexOffset;
        private long _size;
        
        ArrayDimension(final byte[] data, final int offset) {
            this._size = LittleEndian.getUInt(data, offset);
            this._indexOffset = LittleEndian.getInt(data, offset + 4);
        }
    }
    
    static class ArrayHeader
    {
        private ArrayDimension[] _dimensions;
        private int _type;
        
        ArrayHeader(final byte[] data, final int startOffset) {
            int offset = startOffset;
            this._type = LittleEndian.getInt(data, offset);
            offset += 4;
            final long numDimensionsUnsigned = LittleEndian.getUInt(data, offset);
            offset += 4;
            if (1L > numDimensionsUnsigned || numDimensionsUnsigned > 31L) {
                throw new IllegalPropertySetDataException("Array dimension number " + numDimensionsUnsigned + " is not in [1; 31] range");
            }
            final int numDimensions = (int)numDimensionsUnsigned;
            this._dimensions = new ArrayDimension[numDimensions];
            for (int i = 0; i < numDimensions; ++i) {
                this._dimensions[i] = new ArrayDimension(data, offset);
                offset += 8;
            }
        }
        
        long getNumberOfScalarValues() {
            long result = 1L;
            for (final ArrayDimension dimension : this._dimensions) {
                result *= dimension._size;
            }
            return result;
        }
        
        int getSize() {
            return 8 + this._dimensions.length * 8;
        }
        
        int getType() {
            return this._type;
        }
    }
}
