// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hpsf;

import org.apache.poi.util.POILogFactory;
import org.apache.poi.util.LittleEndian;
import org.apache.poi.util.POILogger;
import org.apache.poi.util.Internal;

@Internal
class VariantBool
{
    private static final POILogger logger;
    static final int SIZE = 2;
    private boolean _value;
    
    VariantBool(final byte[] data, final int offset) {
        final short value = LittleEndian.getShort(data, offset);
        if (value == 0) {
            this._value = false;
            return;
        }
        if (value == 65535) {
            this._value = true;
            return;
        }
        VariantBool.logger.log(POILogger.WARN, "VARIANT_BOOL value '", value, "' is incorrect");
        this._value = (value != 0);
    }
    
    boolean getValue() {
        return this._value;
    }
    
    void setValue(final boolean value) {
        this._value = value;
    }
    
    static {
        logger = POILogFactory.getLogger(VariantBool.class);
    }
}
