// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hpsf;

import java.util.Date;
import org.apache.poi.hpsf.wellknown.PropertyIDMap;

public final class SummaryInformation extends SpecialPropertySet
{
    public static final String DEFAULT_STREAM_NAME = "\u0005SummaryInformation";
    
    @Override
    public PropertyIDMap getPropertySetIDMap() {
        return PropertyIDMap.getSummaryInformationProperties();
    }
    
    public SummaryInformation(final PropertySet ps) throws UnexpectedPropertySetTypeException {
        super(ps);
        if (!this.isSummaryInformation()) {
            throw new UnexpectedPropertySetTypeException("Not a " + this.getClass().getName());
        }
    }
    
    public String getTitle() {
        return (String)this.getProperty(2);
    }
    
    public void setTitle(final String title) {
        final MutableSection s = (MutableSection)this.getFirstSection();
        s.setProperty(2, title);
    }
    
    public void removeTitle() {
        final MutableSection s = (MutableSection)this.getFirstSection();
        s.removeProperty(2L);
    }
    
    public String getSubject() {
        return (String)this.getProperty(3);
    }
    
    public void setSubject(final String subject) {
        final MutableSection s = (MutableSection)this.getFirstSection();
        s.setProperty(3, subject);
    }
    
    public void removeSubject() {
        final MutableSection s = (MutableSection)this.getFirstSection();
        s.removeProperty(3L);
    }
    
    public String getAuthor() {
        return (String)this.getProperty(4);
    }
    
    public void setAuthor(final String author) {
        final MutableSection s = (MutableSection)this.getFirstSection();
        s.setProperty(4, author);
    }
    
    public void removeAuthor() {
        final MutableSection s = (MutableSection)this.getFirstSection();
        s.removeProperty(4L);
    }
    
    public String getKeywords() {
        return (String)this.getProperty(5);
    }
    
    public void setKeywords(final String keywords) {
        final MutableSection s = (MutableSection)this.getFirstSection();
        s.setProperty(5, keywords);
    }
    
    public void removeKeywords() {
        final MutableSection s = (MutableSection)this.getFirstSection();
        s.removeProperty(5L);
    }
    
    public String getComments() {
        return (String)this.getProperty(6);
    }
    
    public void setComments(final String comments) {
        final MutableSection s = (MutableSection)this.getFirstSection();
        s.setProperty(6, comments);
    }
    
    public void removeComments() {
        final MutableSection s = (MutableSection)this.getFirstSection();
        s.removeProperty(6L);
    }
    
    public String getTemplate() {
        return (String)this.getProperty(7);
    }
    
    public void setTemplate(final String template) {
        final MutableSection s = (MutableSection)this.getFirstSection();
        s.setProperty(7, template);
    }
    
    public void removeTemplate() {
        final MutableSection s = (MutableSection)this.getFirstSection();
        s.removeProperty(7L);
    }
    
    public String getLastAuthor() {
        return (String)this.getProperty(8);
    }
    
    public void setLastAuthor(final String lastAuthor) {
        final MutableSection s = (MutableSection)this.getFirstSection();
        s.setProperty(8, lastAuthor);
    }
    
    public void removeLastAuthor() {
        final MutableSection s = (MutableSection)this.getFirstSection();
        s.removeProperty(8L);
    }
    
    public String getRevNumber() {
        return (String)this.getProperty(9);
    }
    
    public void setRevNumber(final String revNumber) {
        final MutableSection s = (MutableSection)this.getFirstSection();
        s.setProperty(9, revNumber);
    }
    
    public void removeRevNumber() {
        final MutableSection s = (MutableSection)this.getFirstSection();
        s.removeProperty(9L);
    }
    
    public long getEditTime() {
        final Date d = (Date)this.getProperty(10);
        if (d == null) {
            return 0L;
        }
        return Util.dateToFileTime(d);
    }
    
    public void setEditTime(final long time) {
        final Date d = Util.filetimeToDate(time);
        final MutableSection s = (MutableSection)this.getFirstSection();
        s.setProperty(10, 64L, d);
    }
    
    public void removeEditTime() {
        final MutableSection s = (MutableSection)this.getFirstSection();
        s.removeProperty(10L);
    }
    
    public Date getLastPrinted() {
        return (Date)this.getProperty(11);
    }
    
    public void setLastPrinted(final Date lastPrinted) {
        final MutableSection s = (MutableSection)this.getFirstSection();
        s.setProperty(11, 64L, lastPrinted);
    }
    
    public void removeLastPrinted() {
        final MutableSection s = (MutableSection)this.getFirstSection();
        s.removeProperty(11L);
    }
    
    public Date getCreateDateTime() {
        return (Date)this.getProperty(12);
    }
    
    public void setCreateDateTime(final Date createDateTime) {
        final MutableSection s = (MutableSection)this.getFirstSection();
        s.setProperty(12, 64L, createDateTime);
    }
    
    public void removeCreateDateTime() {
        final MutableSection s = (MutableSection)this.getFirstSection();
        s.removeProperty(12L);
    }
    
    public Date getLastSaveDateTime() {
        return (Date)this.getProperty(13);
    }
    
    public void setLastSaveDateTime(final Date time) {
        final MutableSection s = (MutableSection)this.getFirstSection();
        s.setProperty(13, 64L, time);
    }
    
    public void removeLastSaveDateTime() {
        final MutableSection s = (MutableSection)this.getFirstSection();
        s.removeProperty(13L);
    }
    
    public int getPageCount() {
        return this.getPropertyIntValue(14);
    }
    
    public void setPageCount(final int pageCount) {
        final MutableSection s = (MutableSection)this.getFirstSection();
        s.setProperty(14, pageCount);
    }
    
    public void removePageCount() {
        final MutableSection s = (MutableSection)this.getFirstSection();
        s.removeProperty(14L);
    }
    
    public int getWordCount() {
        return this.getPropertyIntValue(15);
    }
    
    public void setWordCount(final int wordCount) {
        final MutableSection s = (MutableSection)this.getFirstSection();
        s.setProperty(15, wordCount);
    }
    
    public void removeWordCount() {
        final MutableSection s = (MutableSection)this.getFirstSection();
        s.removeProperty(15L);
    }
    
    public int getCharCount() {
        return this.getPropertyIntValue(16);
    }
    
    public void setCharCount(final int charCount) {
        final MutableSection s = (MutableSection)this.getFirstSection();
        s.setProperty(16, charCount);
    }
    
    public void removeCharCount() {
        final MutableSection s = (MutableSection)this.getFirstSection();
        s.removeProperty(16L);
    }
    
    public byte[] getThumbnail() {
        return (byte[])this.getProperty(17);
    }
    
    public void setThumbnail(final byte[] thumbnail) {
        final MutableSection s = (MutableSection)this.getFirstSection();
        s.setProperty(17, 30L, thumbnail);
    }
    
    public void removeThumbnail() {
        final MutableSection s = (MutableSection)this.getFirstSection();
        s.removeProperty(17L);
    }
    
    public String getApplicationName() {
        return (String)this.getProperty(18);
    }
    
    public void setApplicationName(final String applicationName) {
        final MutableSection s = (MutableSection)this.getFirstSection();
        s.setProperty(18, applicationName);
    }
    
    public void removeApplicationName() {
        final MutableSection s = (MutableSection)this.getFirstSection();
        s.removeProperty(18L);
    }
    
    public int getSecurity() {
        return this.getPropertyIntValue(19);
    }
    
    public void setSecurity(final int security) {
        final MutableSection s = (MutableSection)this.getFirstSection();
        s.setProperty(19, security);
    }
    
    public void removeSecurity() {
        final MutableSection s = (MutableSection)this.getFirstSection();
        s.removeProperty(19L);
    }
}
