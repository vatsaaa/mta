// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hpsf;

import org.apache.poi.util.LittleEndian;
import org.apache.poi.util.Internal;

@Internal
class Currency
{
    static final int SIZE = 8;
    private byte[] _value;
    
    Currency(final byte[] data, final int offset) {
        this._value = LittleEndian.getByteArray(data, offset, 8);
    }
}
