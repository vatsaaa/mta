// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hpsf;

import java.util.HashMap;
import org.apache.poi.hpsf.wellknown.SectionIDMap;
import java.util.Iterator;
import java.util.Map;
import org.apache.poi.hpsf.wellknown.PropertyIDMap;

public class DocumentSummaryInformation extends SpecialPropertySet
{
    public static final String DEFAULT_STREAM_NAME = "\u0005DocumentSummaryInformation";
    
    @Override
    public PropertyIDMap getPropertySetIDMap() {
        return PropertyIDMap.getDocumentSummaryInformationProperties();
    }
    
    public DocumentSummaryInformation(final PropertySet ps) throws UnexpectedPropertySetTypeException {
        super(ps);
        if (!this.isDocumentSummaryInformation()) {
            throw new UnexpectedPropertySetTypeException("Not a " + this.getClass().getName());
        }
    }
    
    public String getCategory() {
        return (String)this.getProperty(2);
    }
    
    public void setCategory(final String category) {
        final MutableSection s = (MutableSection)this.getFirstSection();
        s.setProperty(2, category);
    }
    
    public void removeCategory() {
        final MutableSection s = (MutableSection)this.getFirstSection();
        s.removeProperty(2L);
    }
    
    public String getPresentationFormat() {
        return (String)this.getProperty(3);
    }
    
    public void setPresentationFormat(final String presentationFormat) {
        final MutableSection s = (MutableSection)this.getFirstSection();
        s.setProperty(3, presentationFormat);
    }
    
    public void removePresentationFormat() {
        final MutableSection s = (MutableSection)this.getFirstSection();
        s.removeProperty(3L);
    }
    
    public int getByteCount() {
        return this.getPropertyIntValue(4);
    }
    
    public void setByteCount(final int byteCount) {
        final MutableSection s = (MutableSection)this.getFirstSection();
        s.setProperty(4, byteCount);
    }
    
    public void removeByteCount() {
        final MutableSection s = (MutableSection)this.getFirstSection();
        s.removeProperty(4L);
    }
    
    public int getLineCount() {
        return this.getPropertyIntValue(5);
    }
    
    public void setLineCount(final int lineCount) {
        final MutableSection s = (MutableSection)this.getFirstSection();
        s.setProperty(5, lineCount);
    }
    
    public void removeLineCount() {
        final MutableSection s = (MutableSection)this.getFirstSection();
        s.removeProperty(5L);
    }
    
    public int getParCount() {
        return this.getPropertyIntValue(6);
    }
    
    public void setParCount(final int parCount) {
        final MutableSection s = (MutableSection)this.getFirstSection();
        s.setProperty(6, parCount);
    }
    
    public void removeParCount() {
        final MutableSection s = (MutableSection)this.getFirstSection();
        s.removeProperty(6L);
    }
    
    public int getSlideCount() {
        return this.getPropertyIntValue(7);
    }
    
    public void setSlideCount(final int slideCount) {
        final MutableSection s = (MutableSection)this.getFirstSection();
        s.setProperty(7, slideCount);
    }
    
    public void removeSlideCount() {
        final MutableSection s = (MutableSection)this.getFirstSection();
        s.removeProperty(7L);
    }
    
    public int getNoteCount() {
        return this.getPropertyIntValue(8);
    }
    
    public void setNoteCount(final int noteCount) {
        final MutableSection s = (MutableSection)this.getFirstSection();
        s.setProperty(8, noteCount);
    }
    
    public void removeNoteCount() {
        final MutableSection s = (MutableSection)this.getFirstSection();
        s.removeProperty(8L);
    }
    
    public int getHiddenCount() {
        return this.getPropertyIntValue(9);
    }
    
    public void setHiddenCount(final int hiddenCount) {
        final MutableSection s = this.getSections().get(0);
        s.setProperty(9, hiddenCount);
    }
    
    public void removeHiddenCount() {
        final MutableSection s = (MutableSection)this.getFirstSection();
        s.removeProperty(9L);
    }
    
    public int getMMClipCount() {
        return this.getPropertyIntValue(10);
    }
    
    public void setMMClipCount(final int mmClipCount) {
        final MutableSection s = (MutableSection)this.getFirstSection();
        s.setProperty(10, mmClipCount);
    }
    
    public void removeMMClipCount() {
        final MutableSection s = (MutableSection)this.getFirstSection();
        s.removeProperty(10L);
    }
    
    public boolean getScale() {
        return this.getPropertyBooleanValue(11);
    }
    
    public void setScale(final boolean scale) {
        final MutableSection s = (MutableSection)this.getFirstSection();
        s.setProperty(11, scale);
    }
    
    public void removeScale() {
        final MutableSection s = (MutableSection)this.getFirstSection();
        s.removeProperty(11L);
    }
    
    public byte[] getHeadingPair() {
        this.notYetImplemented("Reading byte arrays ");
        return (byte[])this.getProperty(12);
    }
    
    public void setHeadingPair(final byte[] headingPair) {
        this.notYetImplemented("Writing byte arrays ");
    }
    
    public void removeHeadingPair() {
        final MutableSection s = (MutableSection)this.getFirstSection();
        s.removeProperty(12L);
    }
    
    public byte[] getDocparts() {
        this.notYetImplemented("Reading byte arrays");
        return (byte[])this.getProperty(13);
    }
    
    public void setDocparts(final byte[] docparts) {
        this.notYetImplemented("Writing byte arrays");
    }
    
    public void removeDocparts() {
        final MutableSection s = (MutableSection)this.getFirstSection();
        s.removeProperty(13L);
    }
    
    public String getManager() {
        return (String)this.getProperty(14);
    }
    
    public void setManager(final String manager) {
        final MutableSection s = (MutableSection)this.getFirstSection();
        s.setProperty(14, manager);
    }
    
    public void removeManager() {
        final MutableSection s = (MutableSection)this.getFirstSection();
        s.removeProperty(14L);
    }
    
    public String getCompany() {
        return (String)this.getProperty(15);
    }
    
    public void setCompany(final String company) {
        final MutableSection s = (MutableSection)this.getFirstSection();
        s.setProperty(15, company);
    }
    
    public void removeCompany() {
        final MutableSection s = (MutableSection)this.getFirstSection();
        s.removeProperty(15L);
    }
    
    public boolean getLinksDirty() {
        return this.getPropertyBooleanValue(16);
    }
    
    public void setLinksDirty(final boolean linksDirty) {
        final MutableSection s = (MutableSection)this.getFirstSection();
        s.setProperty(16, linksDirty);
    }
    
    public void removeLinksDirty() {
        final MutableSection s = (MutableSection)this.getFirstSection();
        s.removeProperty(16L);
    }
    
    public CustomProperties getCustomProperties() {
        CustomProperties cps = null;
        if (this.getSectionCount() >= 2) {
            cps = new CustomProperties();
            final Section section = this.getSections().get(1);
            final Map<Long, String> dictionary = section.getDictionary();
            final Property[] properties = section.getProperties();
            int propertyCount = 0;
            for (int i = 0; i < properties.length; ++i) {
                final Property p = properties[i];
                final long id = p.getID();
                if (id != 0L && id != 1L) {
                    ++propertyCount;
                    final CustomProperty cp = new CustomProperty(p, dictionary.get(id));
                    cps.put(cp.getName(), cp);
                }
            }
            if (cps.size() != propertyCount) {
                cps.setPure(false);
            }
        }
        return cps;
    }
    
    public void setCustomProperties(final CustomProperties customProperties) {
        this.ensureSection2();
        final MutableSection section = this.getSections().get(1);
        final Map<Long, String> dictionary = customProperties.getDictionary();
        section.clear();
        int cpCodepage = customProperties.getCodepage();
        if (cpCodepage < 0) {
            cpCodepage = section.getCodepage();
        }
        if (cpCodepage < 0) {
            cpCodepage = 1200;
        }
        customProperties.setCodepage(cpCodepage);
        section.setCodepage(cpCodepage);
        section.setDictionary(dictionary);
        for (final Property p : ((HashMap<K, CustomProperty>)customProperties).values()) {
            section.setProperty(p);
        }
    }
    
    private void ensureSection2() {
        if (this.getSectionCount() < 2) {
            final MutableSection s2 = new MutableSection();
            s2.setFormatID(SectionIDMap.DOCUMENT_SUMMARY_INFORMATION_ID[1]);
            this.addSection(s2);
        }
    }
    
    public void removeCustomProperties() {
        if (this.getSectionCount() >= 2) {
            this.getSections().remove(1);
            return;
        }
        throw new HPSFRuntimeException("Illegal internal format of Document SummaryInformation stream: second section is missing.");
    }
    
    private void notYetImplemented(final String msg) {
        throw new UnsupportedOperationException(msg + " is not yet implemented.");
    }
}
