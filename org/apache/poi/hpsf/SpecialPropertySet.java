// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hpsf;

import java.io.OutputStream;
import org.apache.poi.poifs.filesystem.DirectoryEntry;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import org.apache.poi.hpsf.wellknown.PropertyIDMap;

public abstract class SpecialPropertySet extends MutablePropertySet
{
    private MutablePropertySet delegate;
    
    public abstract PropertyIDMap getPropertySetIDMap();
    
    public SpecialPropertySet(final PropertySet ps) {
        this.delegate = new MutablePropertySet(ps);
    }
    
    public SpecialPropertySet(final MutablePropertySet ps) {
        this.delegate = ps;
    }
    
    @Override
    public int getByteOrder() {
        return this.delegate.getByteOrder();
    }
    
    @Override
    public int getFormat() {
        return this.delegate.getFormat();
    }
    
    @Override
    public int getOSVersion() {
        return this.delegate.getOSVersion();
    }
    
    @Override
    public ClassID getClassID() {
        return this.delegate.getClassID();
    }
    
    @Override
    public int getSectionCount() {
        return this.delegate.getSectionCount();
    }
    
    @Override
    public List getSections() {
        return this.delegate.getSections();
    }
    
    @Override
    public boolean isSummaryInformation() {
        return this.delegate.isSummaryInformation();
    }
    
    @Override
    public boolean isDocumentSummaryInformation() {
        return this.delegate.isDocumentSummaryInformation();
    }
    
    @Override
    public Section getFirstSection() {
        return this.delegate.getFirstSection();
    }
    
    @Override
    public void addSection(final Section section) {
        this.delegate.addSection(section);
    }
    
    @Override
    public void clearSections() {
        this.delegate.clearSections();
    }
    
    @Override
    public void setByteOrder(final int byteOrder) {
        this.delegate.setByteOrder(byteOrder);
    }
    
    @Override
    public void setClassID(final ClassID classID) {
        this.delegate.setClassID(classID);
    }
    
    @Override
    public void setFormat(final int format) {
        this.delegate.setFormat(format);
    }
    
    @Override
    public void setOSVersion(final int osVersion) {
        this.delegate.setOSVersion(osVersion);
    }
    
    @Override
    public InputStream toInputStream() throws IOException, WritingNotSupportedException {
        return this.delegate.toInputStream();
    }
    
    @Override
    public void write(final DirectoryEntry dir, final String name) throws WritingNotSupportedException, IOException {
        this.delegate.write(dir, name);
    }
    
    @Override
    public void write(final OutputStream out) throws WritingNotSupportedException, IOException {
        this.delegate.write(out);
    }
    
    @Override
    public boolean equals(final Object o) {
        return this.delegate.equals(o);
    }
    
    @Override
    public Property[] getProperties() throws NoSingleSectionException {
        return this.delegate.getProperties();
    }
    
    @Override
    protected Object getProperty(final int id) throws NoSingleSectionException {
        return this.delegate.getProperty(id);
    }
    
    @Override
    protected boolean getPropertyBooleanValue(final int id) throws NoSingleSectionException {
        return this.delegate.getPropertyBooleanValue(id);
    }
    
    @Override
    protected int getPropertyIntValue(final int id) throws NoSingleSectionException {
        return this.delegate.getPropertyIntValue(id);
    }
    
    @Override
    public int hashCode() {
        return this.delegate.hashCode();
    }
    
    @Override
    public String toString() {
        return this.delegate.toString();
    }
    
    @Override
    public boolean wasNull() throws NoSingleSectionException {
        return this.delegate.wasNull();
    }
}
