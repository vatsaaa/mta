// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hpsf;

import org.apache.poi.hpsf.wellknown.SectionIDMap;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.rmi.UnexpectedException;
import java.io.InputStream;

public class PropertySetFactory
{
    public static PropertySet create(final InputStream stream) throws NoPropertySetStreamException, MarkUnsupportedException, UnsupportedEncodingException, IOException {
        final PropertySet ps = new PropertySet(stream);
        try {
            if (ps.isSummaryInformation()) {
                return new SummaryInformation(ps);
            }
            if (ps.isDocumentSummaryInformation()) {
                return new DocumentSummaryInformation(ps);
            }
            return ps;
        }
        catch (UnexpectedPropertySetTypeException ex) {
            throw new UnexpectedException(ex.toString());
        }
    }
    
    public static SummaryInformation newSummaryInformation() {
        final MutablePropertySet ps = new MutablePropertySet();
        final MutableSection s = (MutableSection)ps.getFirstSection();
        s.setFormatID(SectionIDMap.SUMMARY_INFORMATION_ID);
        try {
            return new SummaryInformation((PropertySet)ps);
        }
        catch (UnexpectedPropertySetTypeException ex) {
            throw new HPSFRuntimeException(ex);
        }
    }
    
    public static DocumentSummaryInformation newDocumentSummaryInformation() {
        final MutablePropertySet ps = new MutablePropertySet();
        final MutableSection s = (MutableSection)ps.getFirstSection();
        s.setFormatID(SectionIDMap.DOCUMENT_SUMMARY_INFORMATION_ID[0]);
        try {
            return new DocumentSummaryInformation((PropertySet)ps);
        }
        catch (UnexpectedPropertySetTypeException ex) {
            throw new HPSFRuntimeException(ex);
        }
    }
}
