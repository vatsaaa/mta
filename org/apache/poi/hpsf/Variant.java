// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hpsf;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class Variant
{
    public static final int VT_EMPTY = 0;
    public static final int VT_NULL = 1;
    public static final int VT_I2 = 2;
    public static final int VT_I4 = 3;
    public static final int VT_R4 = 4;
    public static final int VT_R8 = 5;
    public static final int VT_CY = 6;
    public static final int VT_DATE = 7;
    public static final int VT_BSTR = 8;
    public static final int VT_DISPATCH = 9;
    public static final int VT_ERROR = 10;
    public static final int VT_BOOL = 11;
    public static final int VT_VARIANT = 12;
    public static final int VT_UNKNOWN = 13;
    public static final int VT_DECIMAL = 14;
    public static final int VT_I1 = 16;
    public static final int VT_UI1 = 17;
    public static final int VT_UI2 = 18;
    public static final int VT_UI4 = 19;
    public static final int VT_I8 = 20;
    public static final int VT_UI8 = 21;
    public static final int VT_INT = 22;
    public static final int VT_UINT = 23;
    public static final int VT_VOID = 24;
    public static final int VT_HRESULT = 25;
    public static final int VT_PTR = 26;
    public static final int VT_SAFEARRAY = 27;
    public static final int VT_CARRAY = 28;
    public static final int VT_USERDEFINED = 29;
    public static final int VT_LPSTR = 30;
    public static final int VT_LPWSTR = 31;
    public static final int VT_FILETIME = 64;
    public static final int VT_BLOB = 65;
    public static final int VT_STREAM = 66;
    public static final int VT_STORAGE = 67;
    public static final int VT_STREAMED_OBJECT = 68;
    public static final int VT_STORED_OBJECT = 69;
    public static final int VT_BLOB_OBJECT = 70;
    public static final int VT_CF = 71;
    public static final int VT_CLSID = 72;
    public static final int VT_VERSIONED_STREAM = 73;
    public static final int VT_VECTOR = 4096;
    public static final int VT_ARRAY = 8192;
    public static final int VT_BYREF = 16384;
    public static final int VT_RESERVED = 32768;
    public static final int VT_ILLEGAL = 65535;
    public static final int VT_ILLEGALMASKED = 4095;
    public static final int VT_TYPEMASK = 4095;
    private static Map numberToName;
    private static Map numberToLength;
    public static final Integer LENGTH_UNKNOWN;
    public static final Integer LENGTH_VARIABLE;
    public static final Integer LENGTH_0;
    public static final Integer LENGTH_2;
    public static final Integer LENGTH_4;
    public static final Integer LENGTH_8;
    
    public static String getVariantName(final long variantType) {
        final String name = Variant.numberToName.get(variantType);
        return (name != null) ? name : "unknown variant type";
    }
    
    public static int getVariantLength(final long variantType) {
        final Long key = (long)(int)variantType;
        final Long length = Variant.numberToLength.get(key);
        if (length == null) {
            return -2;
        }
        return length.intValue();
    }
    
    static {
        LENGTH_UNKNOWN = -2;
        LENGTH_VARIABLE = -1;
        LENGTH_0 = 0;
        LENGTH_2 = 2;
        LENGTH_4 = 4;
        LENGTH_8 = 8;
        final Map tm1 = new HashMap();
        tm1.put(0L, "VT_EMPTY");
        tm1.put(1L, "VT_NULL");
        tm1.put(2L, "VT_I2");
        tm1.put(3L, "VT_I4");
        tm1.put(4L, "VT_R4");
        tm1.put(5L, "VT_R8");
        tm1.put(6L, "VT_CY");
        tm1.put(7L, "VT_DATE");
        tm1.put(8L, "VT_BSTR");
        tm1.put(9L, "VT_DISPATCH");
        tm1.put(10L, "VT_ERROR");
        tm1.put(11L, "VT_BOOL");
        tm1.put(12L, "VT_VARIANT");
        tm1.put(13L, "VT_UNKNOWN");
        tm1.put(14L, "VT_DECIMAL");
        tm1.put(16L, "VT_I1");
        tm1.put(17L, "VT_UI1");
        tm1.put(18L, "VT_UI2");
        tm1.put(19L, "VT_UI4");
        tm1.put(20L, "VT_I8");
        tm1.put(21L, "VT_UI8");
        tm1.put(22L, "VT_INT");
        tm1.put(23L, "VT_UINT");
        tm1.put(24L, "VT_VOID");
        tm1.put(25L, "VT_HRESULT");
        tm1.put(26L, "VT_PTR");
        tm1.put(27L, "VT_SAFEARRAY");
        tm1.put(28L, "VT_CARRAY");
        tm1.put(29L, "VT_USERDEFINED");
        tm1.put(30L, "VT_LPSTR");
        tm1.put(31L, "VT_LPWSTR");
        tm1.put(64L, "VT_FILETIME");
        tm1.put(65L, "VT_BLOB");
        tm1.put(66L, "VT_STREAM");
        tm1.put(67L, "VT_STORAGE");
        tm1.put(68L, "VT_STREAMED_OBJECT");
        tm1.put(69L, "VT_STORED_OBJECT");
        tm1.put(70L, "VT_BLOB_OBJECT");
        tm1.put(71L, "VT_CF");
        tm1.put(72L, "VT_CLSID");
        Map tm2 = new HashMap(tm1.size(), 1.0f);
        tm2.putAll(tm1);
        Variant.numberToName = Collections.unmodifiableMap((Map<?, ?>)tm2);
        tm1.clear();
        tm1.put(0L, Variant.LENGTH_0);
        tm1.put(1L, Variant.LENGTH_UNKNOWN);
        tm1.put(2L, Variant.LENGTH_2);
        tm1.put(3L, Variant.LENGTH_4);
        tm1.put(4L, Variant.LENGTH_4);
        tm1.put(5L, Variant.LENGTH_8);
        tm1.put(6L, Variant.LENGTH_UNKNOWN);
        tm1.put(7L, Variant.LENGTH_UNKNOWN);
        tm1.put(8L, Variant.LENGTH_UNKNOWN);
        tm1.put(9L, Variant.LENGTH_UNKNOWN);
        tm1.put(10L, Variant.LENGTH_UNKNOWN);
        tm1.put(11L, Variant.LENGTH_UNKNOWN);
        tm1.put(12L, Variant.LENGTH_UNKNOWN);
        tm1.put(13L, Variant.LENGTH_UNKNOWN);
        tm1.put(14L, Variant.LENGTH_UNKNOWN);
        tm1.put(16L, Variant.LENGTH_UNKNOWN);
        tm1.put(17L, Variant.LENGTH_UNKNOWN);
        tm1.put(18L, Variant.LENGTH_UNKNOWN);
        tm1.put(19L, Variant.LENGTH_UNKNOWN);
        tm1.put(20L, Variant.LENGTH_UNKNOWN);
        tm1.put(21L, Variant.LENGTH_UNKNOWN);
        tm1.put(22L, Variant.LENGTH_UNKNOWN);
        tm1.put(23L, Variant.LENGTH_UNKNOWN);
        tm1.put(24L, Variant.LENGTH_UNKNOWN);
        tm1.put(25L, Variant.LENGTH_UNKNOWN);
        tm1.put(26L, Variant.LENGTH_UNKNOWN);
        tm1.put(27L, Variant.LENGTH_UNKNOWN);
        tm1.put(28L, Variant.LENGTH_UNKNOWN);
        tm1.put(29L, Variant.LENGTH_UNKNOWN);
        tm1.put(30L, Variant.LENGTH_VARIABLE);
        tm1.put(31L, Variant.LENGTH_UNKNOWN);
        tm1.put(64L, Variant.LENGTH_8);
        tm1.put(65L, Variant.LENGTH_UNKNOWN);
        tm1.put(66L, Variant.LENGTH_UNKNOWN);
        tm1.put(67L, Variant.LENGTH_UNKNOWN);
        tm1.put(68L, Variant.LENGTH_UNKNOWN);
        tm1.put(69L, Variant.LENGTH_UNKNOWN);
        tm1.put(70L, Variant.LENGTH_UNKNOWN);
        tm1.put(71L, Variant.LENGTH_UNKNOWN);
        tm1.put(72L, Variant.LENGTH_UNKNOWN);
        tm2 = new HashMap(tm1.size(), 1.0f);
        tm2.putAll(tm1);
        Variant.numberToLength = Collections.unmodifiableMap((Map<?, ?>)tm2);
    }
}
