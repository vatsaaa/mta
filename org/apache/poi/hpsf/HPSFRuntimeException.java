// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hpsf;

import java.io.PrintWriter;
import java.io.PrintStream;

public class HPSFRuntimeException extends RuntimeException
{
    private Throwable reason;
    
    public HPSFRuntimeException() {
    }
    
    public HPSFRuntimeException(final String msg) {
        super(msg);
    }
    
    public HPSFRuntimeException(final Throwable reason) {
        this.reason = reason;
    }
    
    public HPSFRuntimeException(final String msg, final Throwable reason) {
        super(msg);
        this.reason = reason;
    }
    
    public Throwable getReason() {
        return this.reason;
    }
    
    @Override
    public void printStackTrace() {
        this.printStackTrace(System.err);
    }
    
    @Override
    public void printStackTrace(final PrintStream p) {
        final Throwable reason = this.getReason();
        super.printStackTrace(p);
        if (reason != null) {
            p.println("Caused by:");
            reason.printStackTrace(p);
        }
    }
    
    @Override
    public void printStackTrace(final PrintWriter p) {
        final Throwable reason = this.getReason();
        super.printStackTrace(p);
        if (reason != null) {
            p.println("Caused by:");
            reason.printStackTrace(p);
        }
    }
}
