// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hpsf;

import org.apache.poi.util.LittleEndian;

public final class Thumbnail
{
    public static int OFFSET_CFTAG;
    public static int OFFSET_CF;
    public static int OFFSET_WMFDATA;
    public static int CFTAG_WINDOWS;
    public static int CFTAG_MACINTOSH;
    public static int CFTAG_FMTID;
    public static int CFTAG_NODATA;
    public static int CF_METAFILEPICT;
    public static int CF_DIB;
    public static int CF_ENHMETAFILE;
    public static int CF_BITMAP;
    private byte[] _thumbnailData;
    
    public Thumbnail() {
        this._thumbnailData = null;
    }
    
    public Thumbnail(final byte[] thumbnailData) {
        this._thumbnailData = null;
        this._thumbnailData = thumbnailData;
    }
    
    public byte[] getThumbnail() {
        return this._thumbnailData;
    }
    
    public void setThumbnail(final byte[] thumbnail) {
        this._thumbnailData = thumbnail;
    }
    
    public long getClipboardFormatTag() {
        final long clipboardFormatTag = LittleEndian.getUInt(this.getThumbnail(), Thumbnail.OFFSET_CFTAG);
        return clipboardFormatTag;
    }
    
    public long getClipboardFormat() throws HPSFException {
        if (this.getClipboardFormatTag() != Thumbnail.CFTAG_WINDOWS) {
            throw new HPSFException("Clipboard Format Tag of Thumbnail must be CFTAG_WINDOWS.");
        }
        return LittleEndian.getUInt(this.getThumbnail(), Thumbnail.OFFSET_CF);
    }
    
    public byte[] getThumbnailAsWMF() throws HPSFException {
        if (this.getClipboardFormatTag() != Thumbnail.CFTAG_WINDOWS) {
            throw new HPSFException("Clipboard Format Tag of Thumbnail must be CFTAG_WINDOWS.");
        }
        if (this.getClipboardFormat() != Thumbnail.CF_METAFILEPICT) {
            throw new HPSFException("Clipboard Format of Thumbnail must be CF_METAFILEPICT.");
        }
        final byte[] thumbnail = this.getThumbnail();
        final int wmfImageLength = thumbnail.length - Thumbnail.OFFSET_WMFDATA;
        final byte[] wmfImage = new byte[wmfImageLength];
        System.arraycopy(thumbnail, Thumbnail.OFFSET_WMFDATA, wmfImage, 0, wmfImageLength);
        return wmfImage;
    }
    
    static {
        Thumbnail.OFFSET_CFTAG = 4;
        Thumbnail.OFFSET_CF = 8;
        Thumbnail.OFFSET_WMFDATA = 20;
        Thumbnail.CFTAG_WINDOWS = -1;
        Thumbnail.CFTAG_MACINTOSH = -2;
        Thumbnail.CFTAG_FMTID = -3;
        Thumbnail.CFTAG_NODATA = 0;
        Thumbnail.CF_METAFILEPICT = 3;
        Thumbnail.CF_DIB = 8;
        Thumbnail.CF_ENHMETAFILE = 14;
        Thumbnail.CF_BITMAP = 2;
    }
}
