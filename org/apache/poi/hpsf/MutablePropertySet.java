// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hpsf;

import org.apache.poi.poifs.filesystem.Entry;
import java.io.FileNotFoundException;
import org.apache.poi.poifs.filesystem.DirectoryEntry;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.IOException;
import java.util.ListIterator;
import java.io.UnsupportedEncodingException;
import java.io.OutputStream;
import java.util.Iterator;
import java.util.LinkedList;
import org.apache.poi.util.LittleEndian;

public class MutablePropertySet extends PropertySet
{
    private final int OFFSET_HEADER;
    
    public MutablePropertySet() {
        this.OFFSET_HEADER = MutablePropertySet.BYTE_ORDER_ASSERTION.length + MutablePropertySet.FORMAT_ASSERTION.length + 4 + 16 + 4;
        this.byteOrder = LittleEndian.getUShort(MutablePropertySet.BYTE_ORDER_ASSERTION);
        this.format = LittleEndian.getUShort(MutablePropertySet.FORMAT_ASSERTION);
        this.osVersion = 133636;
        this.classID = new ClassID();
        (this.sections = new LinkedList<Section>()).add(new MutableSection());
    }
    
    public MutablePropertySet(final PropertySet ps) {
        this.OFFSET_HEADER = MutablePropertySet.BYTE_ORDER_ASSERTION.length + MutablePropertySet.FORMAT_ASSERTION.length + 4 + 16 + 4;
        this.byteOrder = ps.getByteOrder();
        this.format = ps.getFormat();
        this.osVersion = ps.getOSVersion();
        this.setClassID(ps.getClassID());
        this.clearSections();
        if (this.sections == null) {
            this.sections = new LinkedList<Section>();
        }
        final Iterator i = ps.getSections().iterator();
        while (i.hasNext()) {
            final MutableSection s = new MutableSection(i.next());
            this.addSection(s);
        }
    }
    
    public void setByteOrder(final int byteOrder) {
        this.byteOrder = byteOrder;
    }
    
    public void setFormat(final int format) {
        this.format = format;
    }
    
    public void setOSVersion(final int osVersion) {
        this.osVersion = osVersion;
    }
    
    public void setClassID(final ClassID classID) {
        this.classID = classID;
    }
    
    public void clearSections() {
        this.sections = null;
    }
    
    public void addSection(final Section section) {
        if (this.sections == null) {
            this.sections = new LinkedList<Section>();
        }
        this.sections.add(section);
    }
    
    public void write(final OutputStream out) throws WritingNotSupportedException, IOException {
        final int nrSections = this.sections.size();
        int length = 0;
        length += TypeWriter.writeToStream(out, (short)this.getByteOrder());
        length += TypeWriter.writeToStream(out, (short)this.getFormat());
        length += TypeWriter.writeToStream(out, this.getOSVersion());
        length += TypeWriter.writeToStream(out, this.getClassID());
        length += TypeWriter.writeToStream(out, nrSections);
        int offset = this.OFFSET_HEADER;
        final int sectionsBegin;
        offset = (sectionsBegin = offset + nrSections * 20);
        ListIterator i = this.sections.listIterator();
        while (i.hasNext()) {
            final MutableSection s = i.next();
            final ClassID formatID = s.getFormatID();
            if (formatID == null) {
                throw new NoFormatIDException();
            }
            length += TypeWriter.writeToStream(out, s.getFormatID());
            length += TypeWriter.writeUIntToStream(out, offset);
            try {
                offset += s.getSize();
            }
            catch (HPSFRuntimeException ex) {
                final Throwable cause = ex.getReason();
                if (cause instanceof UnsupportedEncodingException) {
                    throw new IllegalPropertySetDataException(cause);
                }
                throw ex;
            }
        }
        offset = sectionsBegin;
        i = this.sections.listIterator();
        while (i.hasNext()) {
            final MutableSection s = i.next();
            offset += s.write(out);
        }
    }
    
    public InputStream toInputStream() throws IOException, WritingNotSupportedException {
        final ByteArrayOutputStream psStream = new ByteArrayOutputStream();
        this.write(psStream);
        psStream.close();
        final byte[] streamData = psStream.toByteArray();
        return new ByteArrayInputStream(streamData);
    }
    
    public void write(final DirectoryEntry dir, final String name) throws WritingNotSupportedException, IOException {
        try {
            final Entry e = dir.getEntry(name);
            e.delete();
        }
        catch (FileNotFoundException ex) {}
        dir.createDocument(name, this.toInputStream());
    }
}
