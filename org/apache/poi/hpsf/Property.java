// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hpsf;

import org.apache.poi.util.HexDump;
import org.apache.poi.util.POILogger;
import org.apache.poi.util.POILogFactory;
import java.util.LinkedHashMap;
import java.util.Map;
import java.io.UnsupportedEncodingException;
import org.apache.poi.util.LittleEndian;

public class Property
{
    protected long id;
    protected long type;
    protected Object value;
    
    public long getID() {
        return this.id;
    }
    
    public long getType() {
        return this.type;
    }
    
    public Object getValue() {
        return this.value;
    }
    
    public Property(final long id, final long type, final Object value) {
        this.id = id;
        this.type = type;
        this.value = value;
    }
    
    public Property(final long id, final byte[] src, final long offset, final int length, final int codepage) throws UnsupportedEncodingException {
        this.id = id;
        if (id == 0L) {
            this.value = this.readDictionary(src, offset, length, codepage);
            return;
        }
        int o = (int)offset;
        this.type = LittleEndian.getUInt(src, o);
        o += 4;
        try {
            this.value = VariantSupport.read(src, o, length, (int)this.type, codepage);
        }
        catch (UnsupportedVariantTypeException ex) {
            VariantSupport.writeUnsupportedTypeMessage(ex);
            this.value = ex.getValue();
        }
    }
    
    protected Property() {
    }
    
    protected Map<?, ?> readDictionary(final byte[] src, final long offset, final int length, final int codepage) throws UnsupportedEncodingException {
        if (offset < 0L || offset > src.length) {
            throw new HPSFRuntimeException("Illegal offset " + offset + " while HPSF stream contains " + length + " bytes.");
        }
        int o = (int)offset;
        final long nrEntries = LittleEndian.getUInt(src, o);
        o += 4;
        final Map<Object, Object> m = new LinkedHashMap<Object, Object>((int)nrEntries, 1.0f);
        try {
            for (int i = 0; i < nrEntries; ++i) {
                final Long id = LittleEndian.getUInt(src, o);
                o += 4;
                long sLength = LittleEndian.getUInt(src, o);
                o += 4;
                final StringBuffer b = new StringBuffer();
                switch (codepage) {
                    case -1: {
                        b.append(new String(src, o, (int)sLength));
                        break;
                    }
                    case 1200: {
                        final int nrBytes = (int)(sLength * 2L);
                        final byte[] h = new byte[nrBytes];
                        for (int i2 = 0; i2 < nrBytes; i2 += 2) {
                            h[i2] = src[o + i2 + 1];
                            h[i2 + 1] = src[o + i2];
                        }
                        b.append(new String(h, 0, nrBytes, VariantSupport.codepageToEncoding(codepage)));
                        break;
                    }
                    default: {
                        b.append(new String(src, o, (int)sLength, VariantSupport.codepageToEncoding(codepage)));
                        break;
                    }
                }
                while (b.length() > 0 && b.charAt(b.length() - 1) == '\0') {
                    b.setLength(b.length() - 1);
                }
                if (codepage == 1200) {
                    if (sLength % 2L == 1L) {
                        ++sLength;
                    }
                    o += (int)(sLength + sLength);
                }
                else {
                    o += (int)sLength;
                }
                m.put(id, b.toString());
            }
        }
        catch (RuntimeException ex) {
            final POILogger l = POILogFactory.getLogger(this.getClass());
            l.log(POILogger.WARN, "The property set's dictionary contains bogus data. All dictionary entries starting with the one with ID " + this.id + " will be ignored.", ex);
        }
        return m;
    }
    
    protected int getSize() throws WritingNotSupportedException {
        int length = Variant.getVariantLength(this.type);
        if (length >= 0) {
            return length;
        }
        if (length == -2) {
            throw new WritingNotSupportedException(this.type, null);
        }
        final int PADDING = 4;
        switch ((int)this.type) {
            case 30: {
                int l = ((String)this.value).length() + 1;
                final int r = l % 4;
                if (r > 0) {
                    l += 4 - r;
                }
                length += l;
                break;
            }
            case 0: {
                break;
            }
            default: {
                throw new WritingNotSupportedException(this.type, this.value);
            }
        }
        return length;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (!(o instanceof Property)) {
            return false;
        }
        final Property p = (Property)o;
        final Object pValue = p.getValue();
        final long pId = p.getID();
        if (this.id != pId || (this.id != 0L && !this.typesAreEqual(this.type, p.getType()))) {
            return false;
        }
        if (this.value == null && pValue == null) {
            return true;
        }
        if (this.value == null || pValue == null) {
            return false;
        }
        final Class<?> valueClass = this.value.getClass();
        final Class<?> pValueClass = pValue.getClass();
        if (!valueClass.isAssignableFrom(pValueClass) && !pValueClass.isAssignableFrom(valueClass)) {
            return false;
        }
        if (this.value instanceof byte[]) {
            return Util.equal((byte[])this.value, (byte[])pValue);
        }
        return this.value.equals(pValue);
    }
    
    private boolean typesAreEqual(final long t1, final long t2) {
        return t1 == t2 || (t1 == 30L && t2 == 31L) || (t2 == 30L && t1 == 31L);
    }
    
    @Override
    public int hashCode() {
        long hashCode = 0L;
        hashCode += this.id;
        hashCode += this.type;
        if (this.value != null) {
            hashCode += this.value.hashCode();
        }
        final int returnHashCode = (int)(hashCode & 0xFFFFFFFFL);
        return returnHashCode;
    }
    
    @Override
    public String toString() {
        final StringBuffer b = new StringBuffer();
        b.append(this.getClass().getName());
        b.append('[');
        b.append("id: ");
        b.append(this.getID());
        b.append(", type: ");
        b.append(this.getType());
        final Object value = this.getValue();
        b.append(", value: ");
        if (value instanceof String) {
            b.append(value.toString());
            final String s = (String)value;
            final int l = s.length();
            final byte[] bytes = new byte[l * 2];
            for (int i = 0; i < l; ++i) {
                final char c = s.charAt(i);
                final byte high = (byte)((c & '\uff00') >> 8);
                final byte low = (byte)((c & '\u00ff') >> 0);
                bytes[i * 2] = high;
                bytes[i * 2 + 1] = low;
            }
            b.append(" [");
            if (bytes.length > 0) {
                final String hex = HexDump.dump(bytes, 0L, 0);
                b.append(hex);
            }
            b.append("]");
        }
        else if (value instanceof byte[]) {
            final byte[] bytes2 = (byte[])value;
            if (bytes2.length > 0) {
                final String hex2 = HexDump.dump(bytes2, 0L, 0);
                b.append(hex2);
            }
        }
        else {
            b.append(value.toString());
        }
        b.append(']');
        return b.toString();
    }
}
