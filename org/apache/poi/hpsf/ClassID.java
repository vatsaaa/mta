// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hpsf;

import org.apache.poi.util.HexDump;

public class ClassID
{
    protected byte[] bytes;
    public static final int LENGTH = 16;
    
    public ClassID(final byte[] src, final int offset) {
        this.read(src, offset);
    }
    
    public ClassID() {
        this.bytes = new byte[16];
        for (int i = 0; i < 16; ++i) {
            this.bytes[i] = 0;
        }
    }
    
    public int length() {
        return 16;
    }
    
    public byte[] getBytes() {
        return this.bytes;
    }
    
    public void setBytes(final byte[] bytes) {
        for (int i = 0; i < this.bytes.length; ++i) {
            this.bytes[i] = bytes[i];
        }
    }
    
    public byte[] read(final byte[] src, final int offset) {
        (this.bytes = new byte[16])[0] = src[3 + offset];
        this.bytes[1] = src[2 + offset];
        this.bytes[2] = src[1 + offset];
        this.bytes[3] = src[0 + offset];
        this.bytes[4] = src[5 + offset];
        this.bytes[5] = src[4 + offset];
        this.bytes[6] = src[7 + offset];
        this.bytes[7] = src[6 + offset];
        for (int i = 8; i < 16; ++i) {
            this.bytes[i] = src[i + offset];
        }
        return this.bytes;
    }
    
    public void write(final byte[] dst, final int offset) throws ArrayStoreException {
        if (dst.length < 16) {
            throw new ArrayStoreException("Destination byte[] must have room for at least 16 bytes, but has a length of only " + dst.length + ".");
        }
        dst[0 + offset] = this.bytes[3];
        dst[1 + offset] = this.bytes[2];
        dst[2 + offset] = this.bytes[1];
        dst[3 + offset] = this.bytes[0];
        dst[4 + offset] = this.bytes[5];
        dst[5 + offset] = this.bytes[4];
        dst[6 + offset] = this.bytes[7];
        dst[7 + offset] = this.bytes[6];
        for (int i = 8; i < 16; ++i) {
            dst[i + offset] = this.bytes[i];
        }
    }
    
    @Override
    public boolean equals(final Object o) {
        if (o == null || !(o instanceof ClassID)) {
            return false;
        }
        final ClassID cid = (ClassID)o;
        if (this.bytes.length != cid.bytes.length) {
            return false;
        }
        for (int i = 0; i < this.bytes.length; ++i) {
            if (this.bytes[i] != cid.bytes[i]) {
                return false;
            }
        }
        return true;
    }
    
    @Override
    public int hashCode() {
        return new String(this.bytes).hashCode();
    }
    
    @Override
    public String toString() {
        final StringBuffer sbClassId = new StringBuffer(38);
        sbClassId.append('{');
        for (int i = 0; i < 16; ++i) {
            sbClassId.append(HexDump.toHex(this.bytes[i]));
            if (i == 3 || i == 5 || i == 7 || i == 9) {
                sbClassId.append('-');
            }
        }
        sbClassId.append('}');
        return sbClassId.toString();
    }
}
