// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hpsf;

import org.apache.poi.util.LittleEndian;
import org.apache.poi.util.Internal;

@Internal
class Vector
{
    private final short _type;
    private TypedPropertyValue[] _values;
    
    Vector(final byte[] data, final int startOffset, final short type) {
        this._type = type;
        this.read(data, startOffset);
    }
    
    Vector(final short type) {
        this._type = type;
    }
    
    int read(final byte[] data, final int startOffset) {
        int offset = startOffset;
        final long longLength = LittleEndian.getUInt(data, offset);
        offset += 4;
        if (longLength > 2147483647L) {
            throw new UnsupportedOperationException("Vector is too long -- " + longLength);
        }
        final int length = (int)longLength;
        this._values = new TypedPropertyValue[length];
        if (this._type == 12) {
            for (int i = 0; i < length; ++i) {
                final TypedPropertyValue value = new TypedPropertyValue();
                offset += value.read(data, offset);
                this._values[i] = value;
            }
        }
        else {
            for (int i = 0; i < length; ++i) {
                final TypedPropertyValue value = new TypedPropertyValue(this._type, null);
                offset += value.readValue(data, offset);
                this._values[i] = value;
            }
        }
        return offset - startOffset;
    }
    
    TypedPropertyValue[] getValues() {
        return this._values;
    }
}
