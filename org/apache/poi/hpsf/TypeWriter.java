// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hpsf;

import java.io.IOException;
import org.apache.poi.util.LittleEndian;
import java.io.OutputStream;

public class TypeWriter
{
    public static int writeToStream(final OutputStream out, final short n) throws IOException {
        LittleEndian.putShort(out, n);
        return 2;
    }
    
    public static int writeToStream(final OutputStream out, final int n) throws IOException {
        LittleEndian.putInt(n, out);
        return 4;
    }
    
    public static int writeToStream(final OutputStream out, final long n) throws IOException {
        LittleEndian.putLong(n, out);
        return 8;
    }
    
    public static void writeUShortToStream(final OutputStream out, final int n) throws IOException {
        final int high = n & 0xFFFF0000;
        if (high != 0) {
            throw new IllegalPropertySetDataException("Value " + n + " cannot be represented by 2 bytes.");
        }
        LittleEndian.putUShort(n, out);
    }
    
    public static int writeUIntToStream(final OutputStream out, final long n) throws IOException {
        final long high = n & 0xFFFFFFFF00000000L;
        if (high != 0L && high != -4294967296L) {
            throw new IllegalPropertySetDataException("Value " + n + " cannot be represented by 4 bytes.");
        }
        LittleEndian.putUInt(n, out);
        return 4;
    }
    
    public static int writeToStream(final OutputStream out, final ClassID n) throws IOException {
        final byte[] b = new byte[16];
        n.write(b, 0);
        out.write(b, 0, b.length);
        return b.length;
    }
    
    public static void writeToStream(final OutputStream out, final Property[] properties, final int codepage) throws IOException, UnsupportedVariantTypeException {
        if (properties == null) {
            return;
        }
        for (int i = 0; i < properties.length; ++i) {
            final Property p = properties[i];
            writeUIntToStream(out, p.getID());
            writeUIntToStream(out, p.getSize());
        }
        for (int i = 0; i < properties.length; ++i) {
            final Property p = properties[i];
            final long type = p.getType();
            writeUIntToStream(out, type);
            VariantSupport.write(out, (int)type, p.getValue(), codepage);
        }
    }
    
    public static int writeToStream(final OutputStream out, final double n) throws IOException {
        LittleEndian.putDouble(n, out);
        return 8;
    }
}
