// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hpsf;

import java.io.IOException;
import java.util.Date;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.util.LinkedList;
import java.util.List;

public class VariantSupport extends Variant
{
    private static boolean logUnsupportedTypes;
    protected static List unsupportedMessage;
    public static final int[] SUPPORTED_TYPES;
    
    public static void setLogUnsupportedTypes(final boolean logUnsupportedTypes) {
        VariantSupport.logUnsupportedTypes = logUnsupportedTypes;
    }
    
    public static boolean isLogUnsupportedTypes() {
        return VariantSupport.logUnsupportedTypes;
    }
    
    protected static void writeUnsupportedTypeMessage(final UnsupportedVariantTypeException ex) {
        if (isLogUnsupportedTypes()) {
            if (VariantSupport.unsupportedMessage == null) {
                VariantSupport.unsupportedMessage = new LinkedList();
            }
            final Long vt = ex.getVariantType();
            if (!VariantSupport.unsupportedMessage.contains(vt)) {
                System.err.println(ex.getMessage());
                VariantSupport.unsupportedMessage.add(vt);
            }
        }
    }
    
    public boolean isSupportedType(final int variantType) {
        for (int i = 0; i < VariantSupport.SUPPORTED_TYPES.length; ++i) {
            if (variantType == VariantSupport.SUPPORTED_TYPES[i]) {
                return true;
            }
        }
        return false;
    }
    
    public static Object read(final byte[] src, final int offset, final int length, final long type, final int codepage) throws ReadingNotSupportedException, UnsupportedEncodingException {
        final TypedPropertyValue typedPropertyValue = new TypedPropertyValue((int)type, null);
        int unpadded;
        try {
            unpadded = typedPropertyValue.readValue(src, offset);
        }
        catch (UnsupportedOperationException exc) {
            final int propLength = Math.min(length, src.length - offset);
            final byte[] v = new byte[propLength];
            System.arraycopy(src, offset, v, 0, propLength);
            throw new ReadingNotSupportedException(type, v);
        }
        switch ((int)type) {
            case 0:
            case 3:
            case 5:
            case 20: {
                return typedPropertyValue.getValue();
            }
            case 2: {
                return typedPropertyValue.getValue();
            }
            case 64: {
                final Filetime filetime = (Filetime)typedPropertyValue.getValue();
                return Util.filetimeToDate((int)filetime.getHigh(), (int)filetime.getLow());
            }
            case 30: {
                final CodePageString string = (CodePageString)typedPropertyValue.getValue();
                return string.getJavaValue(codepage);
            }
            case 31: {
                final UnicodeString string2 = (UnicodeString)typedPropertyValue.getValue();
                return string2.toJavaString();
            }
            case 71: {
                final ClipboardData clipboardData = (ClipboardData)typedPropertyValue.getValue();
                return clipboardData.toByteArray();
            }
            case 11: {
                final VariantBool bool = (VariantBool)typedPropertyValue.getValue();
                return bool.getValue();
            }
            default: {
                final byte[] v2 = new byte[unpadded];
                System.arraycopy(src, offset, v2, 0, unpadded);
                throw new ReadingNotSupportedException(type, v2);
            }
        }
    }
    
    public static String codepageToEncoding(final int codepage) throws UnsupportedEncodingException {
        if (codepage <= 0) {
            throw new UnsupportedEncodingException("Codepage number may not be " + codepage);
        }
        switch (codepage) {
            case 1200: {
                return "UTF-16";
            }
            case 1201: {
                return "UTF-16BE";
            }
            case 65001: {
                return "UTF-8";
            }
            case 37: {
                return "cp037";
            }
            case 936: {
                return "GBK";
            }
            case 949: {
                return "ms949";
            }
            case 1250: {
                return "windows-1250";
            }
            case 1251: {
                return "windows-1251";
            }
            case 1252: {
                return "windows-1252";
            }
            case 1253: {
                return "windows-1253";
            }
            case 1254: {
                return "windows-1254";
            }
            case 1255: {
                return "windows-1255";
            }
            case 1256: {
                return "windows-1256";
            }
            case 1257: {
                return "windows-1257";
            }
            case 1258: {
                return "windows-1258";
            }
            case 1361: {
                return "johab";
            }
            case 10000: {
                return "MacRoman";
            }
            case 10001: {
                return "SJIS";
            }
            case 10002: {
                return "Big5";
            }
            case 10003: {
                return "EUC-KR";
            }
            case 10004: {
                return "MacArabic";
            }
            case 10005: {
                return "MacHebrew";
            }
            case 10006: {
                return "MacGreek";
            }
            case 10007: {
                return "MacCyrillic";
            }
            case 10008: {
                return "EUC_CN";
            }
            case 10010: {
                return "MacRomania";
            }
            case 10017: {
                return "MacUkraine";
            }
            case 10021: {
                return "MacThai";
            }
            case 10029: {
                return "MacCentralEurope";
            }
            case 10079: {
                return "MacIceland";
            }
            case 10081: {
                return "MacTurkish";
            }
            case 10082: {
                return "MacCroatian";
            }
            case 20127:
            case 65000: {
                return "US-ASCII";
            }
            case 20866: {
                return "KOI8-R";
            }
            case 28591: {
                return "ISO-8859-1";
            }
            case 28592: {
                return "ISO-8859-2";
            }
            case 28593: {
                return "ISO-8859-3";
            }
            case 28594: {
                return "ISO-8859-4";
            }
            case 28595: {
                return "ISO-8859-5";
            }
            case 28596: {
                return "ISO-8859-6";
            }
            case 28597: {
                return "ISO-8859-7";
            }
            case 28598: {
                return "ISO-8859-8";
            }
            case 28599: {
                return "ISO-8859-9";
            }
            case 50220:
            case 50221:
            case 50222: {
                return "ISO-2022-JP";
            }
            case 50225: {
                return "ISO-2022-KR";
            }
            case 51932: {
                return "EUC-JP";
            }
            case 51949: {
                return "EUC-KR";
            }
            case 52936: {
                return "GB2312";
            }
            case 54936: {
                return "GB18030";
            }
            case 932: {
                return "SJIS";
            }
            default: {
                return "cp" + codepage;
            }
        }
    }
    
    public static int write(final OutputStream out, final long type, final Object value, final int codepage) throws IOException, WritingNotSupportedException {
        int length = 0;
        switch ((int)type) {
            case 11: {
                if (value) {
                    out.write(255);
                    out.write(255);
                }
                else {
                    out.write(0);
                    out.write(0);
                }
                length += 2;
                break;
            }
            case 30: {
                final CodePageString codePageString = new CodePageString((String)value, codepage);
                length += codePageString.write(out);
                break;
            }
            case 31: {
                final int nrOfChars = ((String)value).length() + 1;
                length += TypeWriter.writeUIntToStream(out, nrOfChars);
                final char[] s = ((String)value).toCharArray();
                for (int i = 0; i < s.length; ++i) {
                    final int high = (s[i] & '\uff00') >> 8;
                    final int low = s[i] & '\u00ff';
                    final byte highb = (byte)high;
                    final byte lowb = (byte)low;
                    out.write(lowb);
                    out.write(highb);
                    length += 2;
                }
                out.write(0);
                out.write(0);
                length += 2;
                break;
            }
            case 71: {
                final byte[] b = (byte[])value;
                out.write(b);
                length = b.length;
                break;
            }
            case 0: {
                length += TypeWriter.writeUIntToStream(out, 0L);
                break;
            }
            case 2: {
                length += TypeWriter.writeToStream(out, ((Integer)value).shortValue());
                break;
            }
            case 3: {
                if (!(value instanceof Integer)) {
                    throw new ClassCastException("Could not cast an object to " + Integer.class.toString() + ": " + value.getClass().toString() + ", " + value.toString());
                }
                length += TypeWriter.writeToStream(out, (int)value);
                break;
            }
            case 20: {
                length += TypeWriter.writeToStream(out, (long)value);
                break;
            }
            case 5: {
                length += TypeWriter.writeToStream(out, (double)value);
                break;
            }
            case 64: {
                final long filetime = Util.dateToFileTime((Date)value);
                final int high2 = (int)(filetime >> 32 & 0xFFFFFFFFL);
                final int low2 = (int)(filetime & 0xFFFFFFFFL);
                final Filetime filetimeValue = new Filetime(low2, high2);
                length += filetimeValue.write(out);
                break;
            }
            default: {
                if (value instanceof byte[]) {
                    final byte[] b = (byte[])value;
                    out.write(b);
                    length = b.length;
                    writeUnsupportedTypeMessage(new WritingNotSupportedException(type, value));
                    break;
                }
                throw new WritingNotSupportedException(type, value);
            }
        }
        while ((length & 0x3) != 0x0) {
            out.write(0);
            ++length;
        }
        return length;
    }
    
    static {
        VariantSupport.logUnsupportedTypes = false;
        SUPPORTED_TYPES = new int[] { 0, 2, 3, 20, 5, 64, 30, 31, 71, 11 };
    }
}
