// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hpsf.extractor;

import java.io.OutputStream;
import java.io.IOException;
import java.io.File;
import org.apache.poi.util.LittleEndian;
import org.apache.poi.hpsf.Property;
import org.apache.poi.hpsf.wellknown.PropertyIDMap;
import org.apache.poi.hpsf.SummaryInformation;
import java.util.Iterator;
import org.apache.poi.hpsf.CustomProperties;
import org.apache.poi.hpsf.DocumentSummaryInformation;
import org.apache.poi.hpsf.SpecialPropertySet;
import org.apache.poi.poifs.filesystem.NPOIFSFileSystem;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.POIDocument;
import org.apache.poi.POITextExtractor;

public class HPSFPropertiesExtractor extends POITextExtractor
{
    public HPSFPropertiesExtractor(final POITextExtractor mainExtractor) {
        super(mainExtractor);
    }
    
    public HPSFPropertiesExtractor(final POIDocument doc) {
        super(doc);
    }
    
    public HPSFPropertiesExtractor(final POIFSFileSystem fs) {
        super(new PropertiesOnlyDocument(fs));
    }
    
    public HPSFPropertiesExtractor(final NPOIFSFileSystem fs) {
        super(new PropertiesOnlyDocument(fs));
    }
    
    public String getDocumentSummaryInformationText() {
        final DocumentSummaryInformation dsi = this.document.getDocumentSummaryInformation();
        final StringBuffer text = new StringBuffer();
        text.append(getPropertiesText(dsi));
        final CustomProperties cps = (dsi == null) ? null : dsi.getCustomProperties();
        if (cps != null) {
            for (final String key : cps.nameSet()) {
                final String val = getPropertyValueText(cps.get(key));
                text.append(key + " = " + val + "\n");
            }
        }
        return text.toString();
    }
    
    public String getSummaryInformationText() {
        final SummaryInformation si = this.document.getSummaryInformation();
        return getPropertiesText(si);
    }
    
    private static String getPropertiesText(final SpecialPropertySet ps) {
        if (ps == null) {
            return "";
        }
        final StringBuffer text = new StringBuffer();
        final PropertyIDMap idMap = ps.getPropertySetIDMap();
        final Property[] props = ps.getProperties();
        for (int i = 0; i < props.length; ++i) {
            String type = Long.toString(props[i].getID());
            final Object typeObj = idMap.get(props[i].getID());
            if (typeObj != null) {
                type = typeObj.toString();
            }
            final String val = getPropertyValueText(props[i].getValue());
            text.append(type + " = " + val + "\n");
        }
        return text.toString();
    }
    
    private static String getPropertyValueText(final Object val) {
        if (val == null) {
            return "(not set)";
        }
        if (!(val instanceof byte[])) {
            return val.toString();
        }
        final byte[] b = (byte[])val;
        if (b.length == 0) {
            return "";
        }
        if (b.length == 1) {
            return Byte.toString(b[0]);
        }
        if (b.length == 2) {
            return Integer.toString(LittleEndian.getUShort(b));
        }
        if (b.length == 4) {
            return Long.toString(LittleEndian.getUInt(b));
        }
        return new String(b);
    }
    
    @Override
    public String getText() {
        return this.getSummaryInformationText() + this.getDocumentSummaryInformationText();
    }
    
    @Override
    public POITextExtractor getMetadataTextExtractor() {
        throw new IllegalStateException("You already have the Metadata Text Extractor, not recursing!");
    }
    
    public static void main(final String[] args) throws IOException {
        for (final String file : args) {
            final HPSFPropertiesExtractor ext = new HPSFPropertiesExtractor(new NPOIFSFileSystem(new File(file)));
            System.out.println(ext.getText());
        }
    }
    
    private static final class PropertiesOnlyDocument extends POIDocument
    {
        public PropertiesOnlyDocument(final NPOIFSFileSystem fs) {
            super(fs.getRoot());
        }
        
        public PropertiesOnlyDocument(final POIFSFileSystem fs) {
            super(fs);
        }
        
        @Override
        public void write(final OutputStream out) {
            throw new IllegalStateException("Unable to write, only for properties!");
        }
    }
}
