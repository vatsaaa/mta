// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hpsf;

import java.util.Iterator;
import java.util.Collection;
import org.apache.poi.hpsf.wellknown.SectionIDMap;
import java.util.ArrayList;
import org.apache.poi.util.LittleEndian;
import java.io.UnsupportedEncodingException;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public class PropertySet
{
    static final byte[] BYTE_ORDER_ASSERTION;
    protected int byteOrder;
    static final byte[] FORMAT_ASSERTION;
    protected int format;
    protected int osVersion;
    public static final int OS_WIN16 = 0;
    public static final int OS_MACINTOSH = 1;
    public static final int OS_WIN32 = 2;
    protected ClassID classID;
    protected List<Section> sections;
    
    public int getByteOrder() {
        return this.byteOrder;
    }
    
    public int getFormat() {
        return this.format;
    }
    
    public int getOSVersion() {
        return this.osVersion;
    }
    
    public ClassID getClassID() {
        return this.classID;
    }
    
    public int getSectionCount() {
        return this.sections.size();
    }
    
    public List<Section> getSections() {
        return this.sections;
    }
    
    protected PropertySet() {
    }
    
    public PropertySet(final InputStream stream) throws NoPropertySetStreamException, MarkUnsupportedException, IOException, UnsupportedEncodingException {
        if (isPropertySetStream(stream)) {
            final int avail = stream.available();
            final byte[] buffer = new byte[avail];
            stream.read(buffer, 0, buffer.length);
            this.init(buffer, 0, buffer.length);
            return;
        }
        throw new NoPropertySetStreamException();
    }
    
    public PropertySet(final byte[] stream, final int offset, final int length) throws NoPropertySetStreamException, UnsupportedEncodingException {
        if (isPropertySetStream(stream, offset, length)) {
            this.init(stream, offset, length);
            return;
        }
        throw new NoPropertySetStreamException();
    }
    
    public PropertySet(final byte[] stream) throws NoPropertySetStreamException, UnsupportedEncodingException {
        this(stream, 0, stream.length);
    }
    
    public static boolean isPropertySetStream(final InputStream stream) throws MarkUnsupportedException, IOException {
        final int BUFFER_SIZE = 50;
        if (!stream.markSupported()) {
            throw new MarkUnsupportedException(stream.getClass().getName());
        }
        stream.mark(50);
        final byte[] buffer = new byte[50];
        final int bytes = stream.read(buffer, 0, Math.min(buffer.length, stream.available()));
        final boolean isPropertySetStream = isPropertySetStream(buffer, 0, bytes);
        stream.reset();
        return isPropertySetStream;
    }
    
    public static boolean isPropertySetStream(final byte[] src, final int offset, final int length) {
        int o = offset;
        final int byteOrder = LittleEndian.getUShort(src, o);
        o += 2;
        byte[] temp = new byte[2];
        LittleEndian.putShort(temp, 0, (short)byteOrder);
        if (!Util.equal(temp, PropertySet.BYTE_ORDER_ASSERTION)) {
            return false;
        }
        final int format = LittleEndian.getUShort(src, o);
        o += 2;
        temp = new byte[2];
        LittleEndian.putShort(temp, 0, (short)format);
        if (!Util.equal(temp, PropertySet.FORMAT_ASSERTION)) {
            return false;
        }
        o += 4;
        o += 16;
        final long sectionCount = LittleEndian.getUInt(src, o);
        o += 4;
        return sectionCount >= 0L;
    }
    
    private void init(final byte[] src, final int offset, final int length) throws UnsupportedEncodingException {
        int o = offset;
        this.byteOrder = LittleEndian.getUShort(src, o);
        o += 2;
        this.format = LittleEndian.getUShort(src, o);
        o += 2;
        this.osVersion = (int)LittleEndian.getUInt(src, o);
        o += 4;
        this.classID = new ClassID(src, o);
        o += 16;
        final int sectionCount = LittleEndian.getInt(src, o);
        o += 4;
        if (sectionCount < 0) {
            throw new HPSFRuntimeException("Section count " + sectionCount + " is negative.");
        }
        this.sections = new ArrayList<Section>(sectionCount);
        for (int i = 0; i < sectionCount; ++i) {
            final Section s = new Section(src, o);
            o += 20;
            this.sections.add(s);
        }
    }
    
    public boolean isSummaryInformation() {
        return this.sections.size() > 0 && Util.equal(this.sections.get(0).getFormatID().getBytes(), SectionIDMap.SUMMARY_INFORMATION_ID);
    }
    
    public boolean isDocumentSummaryInformation() {
        return this.sections.size() > 0 && Util.equal(this.sections.get(0).getFormatID().getBytes(), SectionIDMap.DOCUMENT_SUMMARY_INFORMATION_ID[0]);
    }
    
    public Property[] getProperties() throws NoSingleSectionException {
        return this.getFirstSection().getProperties();
    }
    
    protected Object getProperty(final int id) throws NoSingleSectionException {
        return this.getFirstSection().getProperty(id);
    }
    
    protected boolean getPropertyBooleanValue(final int id) throws NoSingleSectionException {
        return this.getFirstSection().getPropertyBooleanValue(id);
    }
    
    protected int getPropertyIntValue(final int id) throws NoSingleSectionException {
        return this.getFirstSection().getPropertyIntValue(id);
    }
    
    public boolean wasNull() throws NoSingleSectionException {
        return this.getFirstSection().wasNull();
    }
    
    public Section getFirstSection() {
        if (this.getSectionCount() < 1) {
            throw new MissingSectionException("Property set does not contain any sections.");
        }
        return this.sections.get(0);
    }
    
    public Section getSingleSection() {
        final int sectionCount = this.getSectionCount();
        if (sectionCount != 1) {
            throw new NoSingleSectionException("Property set contains " + sectionCount + " sections.");
        }
        return this.sections.get(0);
    }
    
    @Override
    public boolean equals(final Object o) {
        if (o == null || !(o instanceof PropertySet)) {
            return false;
        }
        final PropertySet ps = (PropertySet)o;
        final int byteOrder1 = ps.getByteOrder();
        final int byteOrder2 = this.getByteOrder();
        final ClassID classID1 = ps.getClassID();
        final ClassID classID2 = this.getClassID();
        final int format1 = ps.getFormat();
        final int format2 = this.getFormat();
        final int osVersion1 = ps.getOSVersion();
        final int osVersion2 = this.getOSVersion();
        final int sectionCount1 = ps.getSectionCount();
        final int sectionCount2 = this.getSectionCount();
        return byteOrder1 == byteOrder2 && classID1.equals(classID2) && format1 == format2 && osVersion1 == osVersion2 && sectionCount1 == sectionCount2 && Util.equals(this.getSections(), ps.getSections());
    }
    
    @Override
    public int hashCode() {
        throw new UnsupportedOperationException("FIXME: Not yet implemented.");
    }
    
    @Override
    public String toString() {
        final StringBuffer b = new StringBuffer();
        final int sectionCount = this.getSectionCount();
        b.append(this.getClass().getName());
        b.append('[');
        b.append("byteOrder: ");
        b.append(this.getByteOrder());
        b.append(", classID: ");
        b.append(this.getClassID());
        b.append(", format: ");
        b.append(this.getFormat());
        b.append(", OSVersion: ");
        b.append(this.getOSVersion());
        b.append(", sectionCount: ");
        b.append(sectionCount);
        b.append(", sections: [\n");
        for (final Section section : this.getSections()) {
            b.append(section);
        }
        b.append(']');
        b.append(']');
        return b.toString();
    }
    
    static {
        BYTE_ORDER_ASSERTION = new byte[] { -2, -1 };
        FORMAT_ASSERTION = new byte[] { 0, 0 };
    }
}
