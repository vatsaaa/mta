// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hpsf;

import org.apache.poi.util.POILogFactory;
import org.apache.poi.util.StringUtil;
import org.apache.poi.util.LittleEndian;
import org.apache.poi.util.POILogger;
import org.apache.poi.util.Internal;

@Internal
class UnicodeString
{
    private static final POILogger logger;
    private byte[] _value;
    
    UnicodeString(final byte[] data, final int offset) {
        final int length = LittleEndian.getInt(data, offset);
        if (length == 0) {
            this._value = new byte[0];
            return;
        }
        this._value = LittleEndian.getByteArray(data, offset + 4, length * 2);
        if (this._value[length * 2 - 1] != 0 || this._value[length * 2 - 2] != 0) {
            throw new IllegalPropertySetDataException("UnicodeString started at offset #" + offset + " is not NULL-terminated");
        }
    }
    
    int getSize() {
        return 4 + this._value.length;
    }
    
    byte[] getValue() {
        return this._value;
    }
    
    String toJavaString() {
        if (this._value.length == 0) {
            return null;
        }
        final String result = StringUtil.getFromUnicodeLE(this._value, 0, this._value.length >> 1);
        final int terminator = result.indexOf(0);
        if (terminator == -1) {
            UnicodeString.logger.log(POILogger.WARN, "String terminator (\\0) for UnicodeString property value not found.Continue without trimming and hope for the best.");
            return result;
        }
        if (terminator != result.length() - 1) {
            UnicodeString.logger.log(POILogger.WARN, "String terminator (\\0) for UnicodeString property value occured before the end of string. Trimming and hope for the best.");
        }
        return result.substring(0, terminator);
    }
    
    static {
        logger = POILogFactory.getLogger(UnicodeString.class);
    }
}
