// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hpsf;

import java.util.Date;
import java.util.Map;
import java.util.ListIterator;
import java.util.Collections;
import java.util.Comparator;
import java.io.IOException;
import java.io.OutputStream;
import java.io.ByteArrayOutputStream;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class MutableSection extends Section
{
    private boolean dirty;
    private List<Property> preprops;
    private byte[] sectionBytes;
    
    public MutableSection() {
        this.dirty = true;
        this.dirty = true;
        this.formatID = null;
        this.offset = -1L;
        this.preprops = new LinkedList<Property>();
    }
    
    public MutableSection(final Section s) {
        this.dirty = true;
        this.setFormatID(s.getFormatID());
        final Property[] pa = s.getProperties();
        final MutableProperty[] mpa = new MutableProperty[pa.length];
        for (int i = 0; i < pa.length; ++i) {
            mpa[i] = new MutableProperty(pa[i]);
        }
        this.setProperties(mpa);
        this.setDictionary(s.getDictionary());
    }
    
    public void setFormatID(final ClassID formatID) {
        this.formatID = formatID;
    }
    
    public void setFormatID(final byte[] formatID) {
        ClassID fid = this.getFormatID();
        if (fid == null) {
            fid = new ClassID();
            this.setFormatID(fid);
        }
        fid.setBytes(formatID);
    }
    
    public void setProperties(final Property[] properties) {
        this.properties = properties;
        this.preprops = new LinkedList<Property>();
        for (int i = 0; i < properties.length; ++i) {
            this.preprops.add(properties[i]);
        }
        this.dirty = true;
    }
    
    public void setProperty(final int id, final String value) {
        this.setProperty(id, 31L, value);
        this.dirty = true;
    }
    
    public void setProperty(final int id, final int value) {
        this.setProperty(id, 3L, value);
        this.dirty = true;
    }
    
    public void setProperty(final int id, final long value) {
        this.setProperty(id, 20L, value);
        this.dirty = true;
    }
    
    public void setProperty(final int id, final boolean value) {
        this.setProperty(id, 11L, value);
        this.dirty = true;
    }
    
    public void setProperty(final int id, final long variantType, final Object value) {
        final MutableProperty p = new MutableProperty();
        p.setID(id);
        p.setType(variantType);
        p.setValue(value);
        this.setProperty(p);
        this.dirty = true;
    }
    
    public void setProperty(final Property p) {
        final long id = p.getID();
        this.removeProperty(id);
        this.preprops.add(p);
        this.dirty = true;
    }
    
    public void removeProperty(final long id) {
        final Iterator<Property> i = this.preprops.iterator();
        while (i.hasNext()) {
            if (i.next().getID() == id) {
                i.remove();
                break;
            }
        }
        this.dirty = true;
    }
    
    protected void setPropertyBooleanValue(final int id, final boolean value) {
        this.setProperty(id, 11L, value);
    }
    
    @Override
    public int getSize() {
        if (this.dirty) {
            try {
                this.size = this.calcSize();
                this.dirty = false;
            }
            catch (HPSFRuntimeException ex) {
                throw ex;
            }
            catch (Exception ex2) {
                throw new HPSFRuntimeException(ex2);
            }
        }
        return this.size;
    }
    
    private int calcSize() throws WritingNotSupportedException, IOException {
        final ByteArrayOutputStream out = new ByteArrayOutputStream();
        this.write(out);
        out.close();
        this.sectionBytes = Util.pad4(out.toByteArray());
        return this.sectionBytes.length;
    }
    
    public int write(final OutputStream out) throws WritingNotSupportedException, IOException {
        if (!this.dirty && this.sectionBytes != null) {
            out.write(this.sectionBytes);
            return this.sectionBytes.length;
        }
        final ByteArrayOutputStream propertyStream = new ByteArrayOutputStream();
        final ByteArrayOutputStream propertyListStream = new ByteArrayOutputStream();
        int position = 0;
        position += 8 + this.getPropertyCount() * 2 * 4;
        int codepage = -1;
        if (this.getProperty(0L) != null) {
            final Object p1 = this.getProperty(1L);
            if (p1 != null) {
                if (!(p1 instanceof Integer)) {
                    throw new IllegalPropertySetDataException("The codepage property (ID = 1) must be an Integer object.");
                }
            }
            else {
                this.setProperty(1, 2L, 1200);
            }
            codepage = this.getCodepage();
        }
        Collections.sort(this.preprops, new Comparator<Property>() {
            public int compare(final Property p1, final Property p2) {
                if (p1.getID() < p2.getID()) {
                    return -1;
                }
                if (p1.getID() == p2.getID()) {
                    return 0;
                }
                return 1;
            }
        });
        final ListIterator<Property> i = this.preprops.listIterator();
        while (i.hasNext()) {
            final MutableProperty p2 = i.next();
            final long id = p2.getID();
            TypeWriter.writeUIntToStream(propertyListStream, p2.getID());
            TypeWriter.writeUIntToStream(propertyListStream, position);
            if (id != 0L) {
                position += p2.write(propertyStream, this.getCodepage());
            }
            else {
                if (codepage == -1) {
                    throw new IllegalPropertySetDataException("Codepage (property 1) is undefined.");
                }
                position += writeDictionary(propertyStream, this.dictionary, codepage);
            }
        }
        propertyStream.close();
        propertyListStream.close();
        final byte[] pb1 = propertyListStream.toByteArray();
        final byte[] pb2 = propertyStream.toByteArray();
        TypeWriter.writeToStream(out, 8 + pb1.length + pb2.length);
        TypeWriter.writeToStream(out, this.getPropertyCount());
        out.write(pb1);
        out.write(pb2);
        final int streamLength = 8 + pb1.length + pb2.length;
        return streamLength;
    }
    
    private static int writeDictionary(final OutputStream out, final Map<Long, String> dictionary, final int codepage) throws IOException {
        int length = TypeWriter.writeUIntToStream(out, dictionary.size());
        for (final Long key : dictionary.keySet()) {
            final String value = dictionary.get(key);
            if (codepage == 1200) {
                int sLength = value.length() + 1;
                if (sLength % 2 == 1) {
                    ++sLength;
                }
                length += TypeWriter.writeUIntToStream(out, key);
                length += TypeWriter.writeUIntToStream(out, sLength);
                final byte[] ca = value.getBytes(VariantSupport.codepageToEncoding(codepage));
                for (int j = 2; j < ca.length; j += 2) {
                    out.write(ca[j + 1]);
                    out.write(ca[j]);
                    length += 2;
                }
                for (sLength -= value.length(); sLength > 0; --sLength) {
                    out.write(0);
                    out.write(0);
                    length += 2;
                }
            }
            else {
                length += TypeWriter.writeUIntToStream(out, key);
                length += TypeWriter.writeUIntToStream(out, value.length() + 1);
                final byte[] ba = value.getBytes(VariantSupport.codepageToEncoding(codepage));
                for (int k = 0; k < ba.length; ++k) {
                    out.write(ba[k]);
                    ++length;
                }
                out.write(0);
                ++length;
            }
        }
        return length;
    }
    
    @Override
    public int getPropertyCount() {
        return this.preprops.size();
    }
    
    @Override
    public Property[] getProperties() {
        return this.properties = this.preprops.toArray(new Property[0]);
    }
    
    @Override
    public Object getProperty(final long id) {
        this.getProperties();
        return super.getProperty(id);
    }
    
    public void setDictionary(final Map<Long, String> dictionary) throws IllegalPropertySetDataException {
        if (dictionary != null) {
            this.setProperty(0, -1L, this.dictionary = dictionary);
            final Integer codepage = (Integer)this.getProperty(1L);
            if (codepage == null) {
                this.setProperty(1, 2L, 1200);
            }
        }
        else {
            this.removeProperty(0L);
        }
    }
    
    public void setProperty(final int id, final Object value) {
        if (value instanceof String) {
            this.setProperty(id, (String)value);
        }
        else if (value instanceof Long) {
            this.setProperty(id, (long)value);
        }
        else if (value instanceof Integer) {
            this.setProperty(id, (int)value);
        }
        else if (value instanceof Short) {
            this.setProperty(id, (int)value);
        }
        else if (value instanceof Boolean) {
            this.setProperty(id, (boolean)value);
        }
        else {
            if (!(value instanceof Date)) {
                throw new HPSFRuntimeException("HPSF does not support properties of type " + value.getClass().getName() + ".");
            }
            this.setProperty(id, 64L, value);
        }
    }
    
    public void clear() {
        final Property[] properties = this.getProperties();
        for (int i = 0; i < properties.length; ++i) {
            final Property p = properties[i];
            this.removeProperty(p.getID());
        }
    }
    
    public void setCodepage(final int codepage) {
        this.setProperty(1, 2L, codepage);
    }
}
