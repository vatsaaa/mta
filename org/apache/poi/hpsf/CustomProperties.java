// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hpsf;

import java.util.Set;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import java.util.HashMap;

public class CustomProperties extends HashMap<Object, CustomProperty>
{
    private Map<Long, String> dictionaryIDToName;
    private Map<String, Long> dictionaryNameToID;
    private boolean isPure;
    
    public CustomProperties() {
        this.dictionaryIDToName = new HashMap<Long, String>();
        this.dictionaryNameToID = new HashMap<String, Long>();
        this.isPure = true;
    }
    
    public CustomProperty put(final String name, final CustomProperty cp) {
        if (name == null) {
            this.isPure = false;
            return null;
        }
        if (!name.equals(cp.getName())) {
            throw new IllegalArgumentException("Parameter \"name\" (" + name + ") and custom property's name (" + cp.getName() + ") do not match.");
        }
        final Long idKey = cp.getID();
        final Long oldID = this.dictionaryNameToID.get(name);
        this.dictionaryIDToName.remove(oldID);
        this.dictionaryNameToID.put(name, idKey);
        this.dictionaryIDToName.put(idKey, name);
        final CustomProperty oldCp = super.remove(oldID);
        super.put(idKey, cp);
        return oldCp;
    }
    
    private Object put(final CustomProperty customProperty) throws ClassCastException {
        final String name = customProperty.getName();
        final Long oldId = this.dictionaryNameToID.get(name);
        if (oldId != null) {
            customProperty.setID(oldId);
        }
        else {
            long max = 1L;
            for (final long id : this.dictionaryIDToName.keySet()) {
                if (id > max) {
                    max = id;
                }
            }
            customProperty.setID(max + 1L);
        }
        return this.put(name, customProperty);
    }
    
    public Object remove(final String name) {
        final Long id = this.dictionaryNameToID.get(name);
        if (id == null) {
            return null;
        }
        this.dictionaryIDToName.remove(id);
        this.dictionaryNameToID.remove(name);
        return super.remove(id);
    }
    
    public Object put(final String name, final String value) {
        final MutableProperty p = new MutableProperty();
        p.setID(-1L);
        p.setType(31L);
        p.setValue(value);
        final CustomProperty cp = new CustomProperty(p, name);
        return this.put(cp);
    }
    
    public Object put(final String name, final Long value) {
        final MutableProperty p = new MutableProperty();
        p.setID(-1L);
        p.setType(20L);
        p.setValue(value);
        final CustomProperty cp = new CustomProperty(p, name);
        return this.put(cp);
    }
    
    public Object put(final String name, final Double value) {
        final MutableProperty p = new MutableProperty();
        p.setID(-1L);
        p.setType(5L);
        p.setValue(value);
        final CustomProperty cp = new CustomProperty(p, name);
        return this.put(cp);
    }
    
    public Object put(final String name, final Integer value) {
        final MutableProperty p = new MutableProperty();
        p.setID(-1L);
        p.setType(3L);
        p.setValue(value);
        final CustomProperty cp = new CustomProperty(p, name);
        return this.put(cp);
    }
    
    public Object put(final String name, final Boolean value) {
        final MutableProperty p = new MutableProperty();
        p.setID(-1L);
        p.setType(11L);
        p.setValue(value);
        final CustomProperty cp = new CustomProperty(p, name);
        return this.put(cp);
    }
    
    public Object get(final String name) {
        final Long id = this.dictionaryNameToID.get(name);
        final CustomProperty cp = super.get(id);
        return (cp != null) ? cp.getValue() : null;
    }
    
    public Object put(final String name, final Date value) {
        final MutableProperty p = new MutableProperty();
        p.setID(-1L);
        p.setType(64L);
        p.setValue(value);
        final CustomProperty cp = new CustomProperty(p, name);
        return this.put(cp);
    }
    
    @Override
    public Set keySet() {
        return this.dictionaryNameToID.keySet();
    }
    
    public Set<String> nameSet() {
        return this.dictionaryNameToID.keySet();
    }
    
    public Set<String> idSet() {
        return this.dictionaryNameToID.keySet();
    }
    
    public void setCodepage(final int codepage) {
        final MutableProperty p = new MutableProperty();
        p.setID(1L);
        p.setType(2L);
        p.setValue(codepage);
        this.put(new CustomProperty(p));
    }
    
    Map<Long, String> getDictionary() {
        return this.dictionaryIDToName;
    }
    
    @Override
    public boolean containsKey(final Object key) {
        if (key instanceof Long) {
            return super.containsKey(key);
        }
        return key instanceof String && super.containsKey(this.dictionaryNameToID.get(key));
    }
    
    @Override
    public boolean containsValue(final Object value) {
        if (value instanceof CustomProperty) {
            return super.containsValue(value);
        }
        for (final CustomProperty cp : super.values()) {
            if (cp.getValue() == value) {
                return true;
            }
        }
        return false;
    }
    
    public int getCodepage() {
        int codepage = -1;
        CustomProperty cp;
        for (Iterator<CustomProperty> i = ((HashMap<K, CustomProperty>)this).values().iterator(); codepage == -1 && i.hasNext(); codepage = (int)cp.getValue()) {
            cp = i.next();
            if (cp.getID() == 1L) {}
        }
        return codepage;
    }
    
    public boolean isPure() {
        return this.isPure;
    }
    
    public void setPure(final boolean isPure) {
        this.isPure = isPure;
    }
}
