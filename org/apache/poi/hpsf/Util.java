// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hpsf;

import java.io.IOException;
import java.io.Writer;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Collection;
import java.util.Date;

public class Util
{
    public static final long EPOCH_DIFF = 11644473600000L;
    
    public static boolean equal(final byte[] a, final byte[] b) {
        if (a.length != b.length) {
            return false;
        }
        for (int i = 0; i < a.length; ++i) {
            if (a[i] != b[i]) {
                return false;
            }
        }
        return true;
    }
    
    public static void copy(final byte[] src, final int srcOffset, final int length, final byte[] dst, final int dstOffset) {
        for (int i = 0; i < length; ++i) {
            dst[dstOffset + i] = src[srcOffset + i];
        }
    }
    
    public static byte[] cat(final byte[][] byteArrays) {
        int capacity = 0;
        for (int i = 0; i < byteArrays.length; ++i) {
            capacity += byteArrays[i].length;
        }
        final byte[] result = new byte[capacity];
        int r = 0;
        for (int j = 0; j < byteArrays.length; ++j) {
            for (int k = 0; k < byteArrays[j].length; ++k) {
                result[r++] = byteArrays[j][k];
            }
        }
        return result;
    }
    
    public static byte[] copy(final byte[] src, final int offset, final int length) {
        final byte[] result = new byte[length];
        copy(src, offset, length, result, 0);
        return result;
    }
    
    public static Date filetimeToDate(final int high, final int low) {
        final long filetime = (long)high << 32 | ((long)low & 0xFFFFFFFFL);
        return filetimeToDate(filetime);
    }
    
    public static Date filetimeToDate(final long filetime) {
        final long ms_since_16010101 = filetime / 10000L;
        final long ms_since_16010102 = ms_since_16010101 - 11644473600000L;
        return new Date(ms_since_16010102);
    }
    
    public static long dateToFileTime(final Date date) {
        final long ms_since_19700101 = date.getTime();
        final long ms_since_19700102 = ms_since_19700101 + 11644473600000L;
        return ms_since_19700102 * 10000L;
    }
    
    public static boolean equals(final Collection<?> c1, final Collection<?> c2) {
        final Object[] o1 = c1.toArray();
        final Object[] o2 = c2.toArray();
        return internalEquals(o1, o2);
    }
    
    public static boolean equals(final Object[] c1, final Object[] c2) {
        final Object[] o1 = c1.clone();
        final Object[] o2 = c2.clone();
        return internalEquals(o1, o2);
    }
    
    private static boolean internalEquals(final Object[] o1, final Object[] o2) {
        for (int i1 = 0; i1 < o1.length; ++i1) {
            final Object obj1 = o1[i1];
            boolean matchFound = false;
            for (int i2 = 0; !matchFound && i2 < o1.length; ++i2) {
                final Object obj2 = o2[i2];
                if (obj1.equals(obj2)) {
                    matchFound = true;
                    o2[i2] = null;
                }
            }
            if (!matchFound) {
                return false;
            }
        }
        return true;
    }
    
    public static byte[] pad4(final byte[] ba) {
        final int PAD = 4;
        int l = ba.length % 4;
        byte[] result;
        if (l == 0) {
            result = ba;
        }
        else {
            l = 4 - l;
            result = new byte[ba.length + l];
            System.arraycopy(ba, 0, result, 0, ba.length);
        }
        return result;
    }
    
    public static char[] pad4(final char[] ca) {
        final int PAD = 4;
        int l = ca.length % 4;
        char[] result;
        if (l == 0) {
            result = ca;
        }
        else {
            l = 4 - l;
            result = new char[ca.length + l];
            System.arraycopy(ca, 0, result, 0, ca.length);
        }
        return result;
    }
    
    public static char[] pad4(final String s) {
        return pad4(s.toCharArray());
    }
    
    public static String toString(final Throwable t) {
        final StringWriter sw = new StringWriter();
        final PrintWriter pw = new PrintWriter(sw);
        t.printStackTrace(pw);
        pw.close();
        try {
            sw.close();
            return sw.toString();
        }
        catch (IOException e) {
            final StringBuffer b = new StringBuffer(t.getMessage());
            b.append("\n");
            b.append("Could not create a stacktrace. Reason: ");
            b.append(e.getMessage());
            return b.toString();
        }
    }
}
