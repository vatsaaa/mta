// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hpsf.wellknown;

import java.util.HashMap;

public class SectionIDMap extends HashMap
{
    public static final byte[] SUMMARY_INFORMATION_ID;
    public static final byte[][] DOCUMENT_SUMMARY_INFORMATION_ID;
    public static final String UNDEFINED = "[undefined]";
    private static SectionIDMap defaultMap;
    
    public static SectionIDMap getInstance() {
        if (SectionIDMap.defaultMap == null) {
            final SectionIDMap m = new SectionIDMap();
            m.put(SectionIDMap.SUMMARY_INFORMATION_ID, PropertyIDMap.getSummaryInformationProperties());
            m.put(SectionIDMap.DOCUMENT_SUMMARY_INFORMATION_ID[0], PropertyIDMap.getDocumentSummaryInformationProperties());
            SectionIDMap.defaultMap = m;
        }
        return SectionIDMap.defaultMap;
    }
    
    public static String getPIDString(final byte[] sectionFormatID, final long pid) {
        final PropertyIDMap m = getInstance().get(sectionFormatID);
        if (m == null) {
            return "[undefined]";
        }
        final String s = (String)m.get(pid);
        if (s == null) {
            return "[undefined]";
        }
        return s;
    }
    
    public PropertyIDMap get(final byte[] sectionFormatID) {
        return super.get(new String(sectionFormatID));
    }
    
    @Override
    @Deprecated
    public Object get(final Object sectionFormatID) {
        return this.get((byte[])sectionFormatID);
    }
    
    public Object put(final byte[] sectionFormatID, final PropertyIDMap propertyIDMap) {
        return super.put(new String(sectionFormatID), propertyIDMap);
    }
    
    @Override
    @Deprecated
    public Object put(final Object key, final Object value) {
        return this.put((byte[])key, (PropertyIDMap)value);
    }
    
    static {
        SUMMARY_INFORMATION_ID = new byte[] { -14, -97, -123, -32, 79, -7, 16, 104, -85, -111, 8, 0, 43, 39, -77, -39 };
        DOCUMENT_SUMMARY_INFORMATION_ID = new byte[][] { { -43, -51, -43, 2, 46, -100, 16, 27, -109, -105, 8, 0, 43, 44, -7, -82 }, { -43, -51, -43, 5, 46, -100, 16, 27, -109, -105, 8, 0, 43, 44, -7, -82 } };
    }
}
