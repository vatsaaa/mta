// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hpsf;

import java.io.IOException;
import java.io.OutputStream;

public class MutableProperty extends Property
{
    public MutableProperty() {
    }
    
    public MutableProperty(final Property p) {
        this.setID(p.getID());
        this.setType(p.getType());
        this.setValue(p.getValue());
    }
    
    public void setID(final long id) {
        this.id = id;
    }
    
    public void setType(final long type) {
        this.type = type;
    }
    
    public void setValue(final Object value) {
        this.value = value;
    }
    
    public int write(final OutputStream out, final int codepage) throws IOException, WritingNotSupportedException {
        int length = 0;
        long variantType = this.getType();
        if (codepage == 1200 && variantType == 30L) {
            variantType = 31L;
        }
        length += TypeWriter.writeUIntToStream(out, variantType);
        length += VariantSupport.write(out, variantType, this.getValue(), codepage);
        return length;
    }
}
