// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hpsf;

import org.apache.poi.util.POILogFactory;
import java.io.IOException;
import java.io.OutputStream;
import org.apache.poi.util.LittleEndian;
import java.io.UnsupportedEncodingException;
import org.apache.poi.util.POILogger;
import org.apache.poi.util.Internal;

@Internal
class CodePageString
{
    private static final POILogger logger;
    private byte[] _value;
    
    private static String codepageToEncoding(final int codepage) throws UnsupportedEncodingException {
        if (codepage <= 0) {
            throw new UnsupportedEncodingException("Codepage number may not be " + codepage);
        }
        switch (codepage) {
            case 1200: {
                return "UTF-16";
            }
            case 1201: {
                return "UTF-16BE";
            }
            case 65001: {
                return "UTF-8";
            }
            case 37: {
                return "cp037";
            }
            case 936: {
                return "GBK";
            }
            case 949: {
                return "ms949";
            }
            case 1250: {
                return "windows-1250";
            }
            case 1251: {
                return "windows-1251";
            }
            case 1252: {
                return "windows-1252";
            }
            case 1253: {
                return "windows-1253";
            }
            case 1254: {
                return "windows-1254";
            }
            case 1255: {
                return "windows-1255";
            }
            case 1256: {
                return "windows-1256";
            }
            case 1257: {
                return "windows-1257";
            }
            case 1258: {
                return "windows-1258";
            }
            case 1361: {
                return "johab";
            }
            case 10000: {
                return "MacRoman";
            }
            case 10001: {
                return "SJIS";
            }
            case 10002: {
                return "Big5";
            }
            case 10003: {
                return "EUC-KR";
            }
            case 10004: {
                return "MacArabic";
            }
            case 10005: {
                return "MacHebrew";
            }
            case 10006: {
                return "MacGreek";
            }
            case 10007: {
                return "MacCyrillic";
            }
            case 10008: {
                return "EUC_CN";
            }
            case 10010: {
                return "MacRomania";
            }
            case 10017: {
                return "MacUkraine";
            }
            case 10021: {
                return "MacThai";
            }
            case 10029: {
                return "MacCentralEurope";
            }
            case 10079: {
                return "MacIceland";
            }
            case 10081: {
                return "MacTurkish";
            }
            case 10082: {
                return "MacCroatian";
            }
            case 20127:
            case 65000: {
                return "US-ASCII";
            }
            case 20866: {
                return "KOI8-R";
            }
            case 28591: {
                return "ISO-8859-1";
            }
            case 28592: {
                return "ISO-8859-2";
            }
            case 28593: {
                return "ISO-8859-3";
            }
            case 28594: {
                return "ISO-8859-4";
            }
            case 28595: {
                return "ISO-8859-5";
            }
            case 28596: {
                return "ISO-8859-6";
            }
            case 28597: {
                return "ISO-8859-7";
            }
            case 28598: {
                return "ISO-8859-8";
            }
            case 28599: {
                return "ISO-8859-9";
            }
            case 50220:
            case 50221:
            case 50222: {
                return "ISO-2022-JP";
            }
            case 50225: {
                return "ISO-2022-KR";
            }
            case 51932: {
                return "EUC-JP";
            }
            case 51949: {
                return "EUC-KR";
            }
            case 52936: {
                return "GB2312";
            }
            case 54936: {
                return "GB18030";
            }
            case 932: {
                return "SJIS";
            }
            default: {
                return "cp" + codepage;
            }
        }
    }
    
    CodePageString(final byte[] data, final int startOffset) {
        int offset = startOffset;
        final int size = LittleEndian.getInt(data, offset);
        offset += 4;
        this._value = LittleEndian.getByteArray(data, offset, size);
        if (this._value[size - 1] != 0) {
            CodePageString.logger.log(POILogger.WARN, "CodePageString started at offset #" + offset + " is not NULL-terminated");
        }
    }
    
    CodePageString(final String string, final int codepage) throws UnsupportedEncodingException {
        this.setJavaValue(string, codepage);
    }
    
    String getJavaValue(final int codepage) throws UnsupportedEncodingException {
        String result;
        if (codepage == -1) {
            result = new String(this._value);
        }
        else {
            result = new String(this._value, codepageToEncoding(codepage));
        }
        final int terminator = result.indexOf(0);
        if (terminator == -1) {
            CodePageString.logger.log(POILogger.WARN, "String terminator (\\0) for CodePageString property value not found.Continue without trimming and hope for the best.");
            return result;
        }
        if (terminator != result.length() - 1) {
            CodePageString.logger.log(POILogger.WARN, "String terminator (\\0) for CodePageString property value occured before the end of string. Trimming and hope for the best.");
        }
        return result.substring(0, terminator);
    }
    
    int getSize() {
        return 4 + this._value.length;
    }
    
    void setJavaValue(final String string, final int codepage) throws UnsupportedEncodingException {
        if (codepage == -1) {
            this._value = (string + "\u0000").getBytes();
        }
        else {
            this._value = (string + "\u0000").getBytes(codepageToEncoding(codepage));
        }
    }
    
    int write(final OutputStream out) throws IOException {
        LittleEndian.putInt(this._value.length, out);
        out.write(this._value);
        return 4 + this._value.length;
    }
    
    static {
        logger = POILogFactory.getLogger(CodePageString.class);
    }
}
