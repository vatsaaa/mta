// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hpsf;

public class WritingNotSupportedException extends UnsupportedVariantTypeException
{
    public WritingNotSupportedException(final long variantType, final Object value) {
        super(variantType, value);
    }
}
