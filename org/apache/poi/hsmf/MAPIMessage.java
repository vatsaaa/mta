// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hsmf;

import java.io.OutputStream;
import java.util.Calendar;
import org.apache.poi.hsmf.datatypes.Chunk;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.poi.hsmf.datatypes.ByteChunk;
import org.apache.poi.hmef.attribute.MAPIRtfAttribute;
import org.apache.poi.hsmf.datatypes.MAPIProperty;
import org.apache.poi.hsmf.exceptions.ChunkNotFoundException;
import org.apache.poi.hsmf.datatypes.StringChunk;
import org.apache.poi.hsmf.datatypes.ChunkGroup;
import java.util.Comparator;
import java.util.Arrays;
import java.util.ArrayList;
import org.apache.poi.hsmf.parsers.POIFSChunkParser;
import org.apache.poi.poifs.filesystem.DirectoryNode;
import org.apache.poi.poifs.filesystem.NPOIFSFileSystem;
import java.io.IOException;
import java.io.InputStream;
import java.io.FileInputStream;
import java.io.File;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.hsmf.datatypes.AttachmentChunks;
import org.apache.poi.hsmf.datatypes.RecipientChunks;
import org.apache.poi.hsmf.datatypes.NameIdChunks;
import org.apache.poi.hsmf.datatypes.Chunks;
import org.apache.poi.POIDocument;

public class MAPIMessage extends POIDocument
{
    private Chunks mainChunks;
    private NameIdChunks nameIdChunks;
    private RecipientChunks[] recipientChunks;
    private AttachmentChunks[] attachmentChunks;
    private boolean returnNullOnMissingChunk;
    
    public MAPIMessage() {
        super(new POIFSFileSystem());
        this.returnNullOnMissingChunk = false;
    }
    
    public MAPIMessage(final String filename) throws IOException {
        this(new FileInputStream(new File(filename)));
    }
    
    public MAPIMessage(final InputStream in) throws IOException {
        this(new POIFSFileSystem(in));
    }
    
    public MAPIMessage(final POIFSFileSystem fs) throws IOException {
        this(fs.getRoot());
    }
    
    public MAPIMessage(final NPOIFSFileSystem fs) throws IOException {
        this(fs.getRoot());
    }
    
    @Deprecated
    public MAPIMessage(final DirectoryNode poifsDir, final POIFSFileSystem fs) throws IOException {
        this(poifsDir);
    }
    
    public MAPIMessage(final DirectoryNode poifsDir) throws IOException {
        super(poifsDir);
        this.returnNullOnMissingChunk = false;
        final ChunkGroup[] chunkGroups = POIFSChunkParser.parse(poifsDir);
        final ArrayList<AttachmentChunks> attachments = new ArrayList<AttachmentChunks>();
        final ArrayList<RecipientChunks> recipients = new ArrayList<RecipientChunks>();
        for (final ChunkGroup group : chunkGroups) {
            if (group instanceof Chunks) {
                this.mainChunks = (Chunks)group;
            }
            else if (group instanceof NameIdChunks) {
                this.nameIdChunks = (NameIdChunks)group;
            }
            else if (group instanceof RecipientChunks) {
                recipients.add((RecipientChunks)group);
            }
            if (group instanceof AttachmentChunks) {
                attachments.add((AttachmentChunks)group);
            }
        }
        this.attachmentChunks = attachments.toArray(new AttachmentChunks[attachments.size()]);
        this.recipientChunks = recipients.toArray(new RecipientChunks[recipients.size()]);
        Arrays.sort(this.attachmentChunks, new AttachmentChunks.AttachmentChunksSorter());
        Arrays.sort(this.recipientChunks, new RecipientChunks.RecipientChunksSorter());
    }
    
    public String getStringFromChunk(final StringChunk chunk) throws ChunkNotFoundException {
        if (chunk != null) {
            return chunk.getValue();
        }
        if (this.returnNullOnMissingChunk) {
            return null;
        }
        throw new ChunkNotFoundException();
    }
    
    public String getTextBody() throws ChunkNotFoundException {
        return this.getStringFromChunk(this.mainChunks.textBodyChunk);
    }
    
    public String getHtmlBody() throws ChunkNotFoundException {
        if (this.mainChunks.htmlBodyChunkBinary != null) {
            return this.mainChunks.htmlBodyChunkBinary.getAs7bitString();
        }
        return this.getStringFromChunk(this.mainChunks.htmlBodyChunkString);
    }
    
    @Deprecated
    public String getHmtlBody() throws ChunkNotFoundException {
        return this.getHtmlBody();
    }
    
    public String getRtfBody() throws ChunkNotFoundException {
        final ByteChunk chunk = this.mainChunks.rtfBodyChunk;
        if (chunk == null) {
            if (this.returnNullOnMissingChunk) {
                return null;
            }
            throw new ChunkNotFoundException();
        }
        else {
            try {
                final MAPIRtfAttribute rtf = new MAPIRtfAttribute(MAPIProperty.RTF_COMPRESSED, 258, chunk.getValue());
                return rtf.getDataString();
            }
            catch (IOException e) {
                throw new RuntimeException("Shouldn't happen", e);
            }
        }
    }
    
    public String getSubject() throws ChunkNotFoundException {
        return this.getStringFromChunk(this.mainChunks.subjectChunk);
    }
    
    public String getDisplayFrom() throws ChunkNotFoundException {
        return this.getStringFromChunk(this.mainChunks.displayFromChunk);
    }
    
    public String getDisplayTo() throws ChunkNotFoundException {
        return this.getStringFromChunk(this.mainChunks.displayToChunk);
    }
    
    public String getDisplayCC() throws ChunkNotFoundException {
        return this.getStringFromChunk(this.mainChunks.displayCCChunk);
    }
    
    public String getDisplayBCC() throws ChunkNotFoundException {
        return this.getStringFromChunk(this.mainChunks.displayBCCChunk);
    }
    
    public String getRecipientEmailAddress() throws ChunkNotFoundException {
        return this.toSemicolonList(this.getRecipientEmailAddressList());
    }
    
    public String[] getRecipientEmailAddressList() throws ChunkNotFoundException {
        if (this.recipientChunks == null || this.recipientChunks.length == 0) {
            throw new ChunkNotFoundException("No recipients section present");
        }
        final String[] emails = new String[this.recipientChunks.length];
        for (int i = 0; i < emails.length; ++i) {
            final RecipientChunks rc = this.recipientChunks[i];
            final String email = rc.getRecipientEmailAddress();
            if (email != null) {
                emails[i] = email;
            }
            else {
                if (!this.returnNullOnMissingChunk) {
                    throw new ChunkNotFoundException("No email address holding chunks found for the " + (i + 1) + "th recipient");
                }
                emails[i] = null;
            }
        }
        return emails;
    }
    
    public String getRecipientNames() throws ChunkNotFoundException {
        return this.toSemicolonList(this.getRecipientNamesList());
    }
    
    public String[] getRecipientNamesList() throws ChunkNotFoundException {
        if (this.recipientChunks == null || this.recipientChunks.length == 0) {
            throw new ChunkNotFoundException("No recipients section present");
        }
        final String[] names = new String[this.recipientChunks.length];
        for (int i = 0; i < names.length; ++i) {
            final RecipientChunks rc = this.recipientChunks[i];
            final String name = rc.getRecipientName();
            if (name == null) {
                throw new ChunkNotFoundException("No display name holding chunks found for the " + (i + 1) + "th recipient");
            }
            names[i] = name;
        }
        return names;
    }
    
    public void guess7BitEncoding() {
        try {
            final String[] headers = this.getHeaders();
            if (headers != null && headers.length > 0) {
                final Pattern p = Pattern.compile("Content-Type:.*?charset=[\"']?([^;'\"]+)[\"']?", 2);
                for (final String header : headers) {
                    if (header.startsWith("Content-Type")) {
                        final Matcher m = p.matcher(header);
                        if (m.matches()) {
                            final String charset = m.group(1);
                            if (!charset.equalsIgnoreCase("utf-8")) {
                                this.set7BitEncoding(charset);
                            }
                            return;
                        }
                    }
                }
            }
        }
        catch (ChunkNotFoundException ex) {}
        try {
            final String html = this.getHmtlBody();
            if (html != null && html.length() > 0) {
                final Pattern p = Pattern.compile("<META\\s+HTTP-EQUIV=\"Content-Type\"\\s+CONTENT=\"text/html;\\s+charset=(.*?)\"");
                final Matcher i = p.matcher(html);
                if (i.find()) {
                    final String charset2 = i.group(1);
                    this.set7BitEncoding(charset2);
                }
            }
        }
        catch (ChunkNotFoundException ex2) {}
    }
    
    public void set7BitEncoding(final String charset) {
        for (final Chunk c : this.mainChunks.getAll()) {
            if (c instanceof StringChunk) {
                ((StringChunk)c).set7BitEncoding(charset);
            }
        }
        if (this.nameIdChunks != null) {
            for (final Chunk c : this.nameIdChunks.getAll()) {
                if (c instanceof StringChunk) {
                    ((StringChunk)c).set7BitEncoding(charset);
                }
            }
        }
        for (final RecipientChunks rc : this.recipientChunks) {
            for (final Chunk c2 : rc.getAll()) {
                if (c2 instanceof StringChunk) {
                    ((StringChunk)c2).set7BitEncoding(charset);
                }
            }
        }
    }
    
    public boolean has7BitEncodingStrings() {
        for (final Chunk c : this.mainChunks.getAll()) {
            if (c instanceof StringChunk && ((StringChunk)c).getType() == 30) {
                return true;
            }
        }
        if (this.nameIdChunks != null) {
            for (final Chunk c : this.nameIdChunks.getAll()) {
                if (c instanceof StringChunk && ((StringChunk)c).getType() == 30) {
                    return true;
                }
            }
        }
        for (final RecipientChunks rc : this.recipientChunks) {
            for (final Chunk c2 : rc.getAll()) {
                if (c2 instanceof StringChunk && ((StringChunk)c2).getType() == 30) {
                    return true;
                }
            }
        }
        return false;
    }
    
    public String[] getHeaders() throws ChunkNotFoundException {
        final String headers = this.getStringFromChunk(this.mainChunks.messageHeaders);
        if (headers == null) {
            return null;
        }
        return headers.split("\\r?\\n");
    }
    
    public String getConversationTopic() throws ChunkNotFoundException {
        return this.getStringFromChunk(this.mainChunks.conversationTopic);
    }
    
    public String getMessageClass() throws ChunkNotFoundException {
        return this.getStringFromChunk(this.mainChunks.messageClass);
    }
    
    public Calendar getMessageDate() throws ChunkNotFoundException {
        if (this.mainChunks.submissionChunk != null) {
            return this.mainChunks.submissionChunk.getAcceptedAtTime();
        }
        if (this.returnNullOnMissingChunk) {
            return null;
        }
        throw new ChunkNotFoundException();
    }
    
    public Chunks getMainChunks() {
        return this.mainChunks;
    }
    
    public RecipientChunks[] getRecipientDetailsChunks() {
        return this.recipientChunks;
    }
    
    public NameIdChunks getNameIdChunks() {
        return this.nameIdChunks;
    }
    
    public AttachmentChunks[] getAttachmentFiles() {
        return this.attachmentChunks;
    }
    
    @Override
    public void write(final OutputStream out) throws IOException {
        throw new UnsupportedOperationException("Writing isn't yet supported for HSMF, sorry");
    }
    
    public boolean isReturnNullOnMissingChunk() {
        return this.returnNullOnMissingChunk;
    }
    
    public void setReturnNullOnMissingChunk(final boolean returnNullOnMissingChunk) {
        this.returnNullOnMissingChunk = returnNullOnMissingChunk;
    }
    
    private String toSemicolonList(final String[] l) {
        final StringBuffer list = new StringBuffer();
        boolean first = true;
        for (final String s : l) {
            if (s != null) {
                if (first) {
                    first = false;
                }
                else {
                    list.append("; ");
                }
                list.append(s);
            }
        }
        return list.toString();
    }
}
