// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hsmf.parsers;

import org.apache.poi.hsmf.datatypes.Chunk;
import java.io.InputStream;
import org.apache.poi.poifs.filesystem.DocumentEntry;
import org.apache.poi.poifs.filesystem.DocumentInputStream;
import org.apache.poi.hsmf.datatypes.StringChunk;
import org.apache.poi.hsmf.datatypes.DirectoryChunk;
import org.apache.poi.hsmf.datatypes.ByteChunk;
import org.apache.poi.hsmf.datatypes.MessageSubmissionChunk;
import org.apache.poi.hsmf.datatypes.MAPIProperty;
import org.apache.poi.hsmf.datatypes.Types;
import org.apache.poi.poifs.filesystem.DocumentNode;
import java.util.Iterator;
import org.apache.poi.hsmf.datatypes.RecipientChunks;
import org.apache.poi.hsmf.datatypes.NameIdChunks;
import org.apache.poi.hsmf.datatypes.AttachmentChunks;
import org.apache.poi.poifs.filesystem.Entry;
import java.util.ArrayList;
import org.apache.poi.hsmf.datatypes.Chunks;
import org.apache.poi.poifs.filesystem.DirectoryNode;
import java.io.IOException;
import org.apache.poi.hsmf.datatypes.ChunkGroup;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;

public final class POIFSChunkParser
{
    public static ChunkGroup[] parse(final POIFSFileSystem fs) throws IOException {
        return parse(fs.getRoot());
    }
    
    public static ChunkGroup[] parse(final DirectoryNode node) throws IOException {
        final Chunks mainChunks = new Chunks();
        final ArrayList<ChunkGroup> groups = new ArrayList<ChunkGroup>();
        groups.add(mainChunks);
        for (final Entry entry : node) {
            if (entry instanceof DirectoryNode) {
                final DirectoryNode dir = (DirectoryNode)entry;
                ChunkGroup group = null;
                if (dir.getName().startsWith("__attach_version1.0_#")) {
                    group = new AttachmentChunks(dir.getName());
                }
                if (dir.getName().startsWith("__nameid_version1.0")) {
                    group = new NameIdChunks();
                }
                if (dir.getName().startsWith("__recip_version1.0_#")) {
                    group = new RecipientChunks(dir.getName());
                }
                if (group == null) {
                    continue;
                }
                processChunks(dir, group);
                groups.add(group);
            }
        }
        processChunks(node, mainChunks);
        return groups.toArray(new ChunkGroup[groups.size()]);
    }
    
    protected static void processChunks(final DirectoryNode node, final ChunkGroup grouping) {
        for (final Entry entry : node) {
            if (entry instanceof DocumentNode) {
                process(entry, grouping);
            }
            else {
                if (!(entry instanceof DirectoryNode) || !entry.getName().endsWith(Types.asFileEnding(13))) {
                    continue;
                }
                process(entry, grouping);
            }
        }
    }
    
    protected static void process(final Entry entry, final ChunkGroup grouping) {
        final String entryName = entry.getName();
        if (entryName.length() < 9) {
            return;
        }
        if (entryName.indexOf(95) == -1) {
            return;
        }
        final int splitAt = entryName.lastIndexOf(95);
        final String namePrefix = entryName.substring(0, splitAt + 1);
        final String ids = entryName.substring(splitAt + 1);
        if (namePrefix.equals("Olk10SideProps") || namePrefix.equals("Olk10SideProps_")) {
            return;
        }
        if (splitAt <= entryName.length() - 8) {
            try {
                final int chunkId = Integer.parseInt(ids.substring(0, 4), 16);
                final int type = Integer.parseInt(ids.substring(4, 8), 16);
                Chunk chunk = null;
                if (chunkId == MAPIProperty.MESSAGE_SUBMISSION_ID.id) {
                    chunk = new MessageSubmissionChunk(namePrefix, chunkId, type);
                }
                else {
                    switch (type) {
                        case 258: {
                            chunk = new ByteChunk(namePrefix, chunkId, type);
                            break;
                        }
                        case 13: {
                            if (entry instanceof DirectoryNode) {
                                chunk = new DirectoryChunk((DirectoryNode)entry, namePrefix, chunkId, type);
                                break;
                            }
                            break;
                        }
                        case 30:
                        case 31: {
                            chunk = new StringChunk(namePrefix, chunkId, type);
                            break;
                        }
                    }
                }
                if (chunk != null) {
                    if (entry instanceof DocumentNode) {
                        try {
                            final DocumentInputStream inp = new DocumentInputStream((DocumentEntry)entry);
                            chunk.readValue(inp);
                            grouping.record(chunk);
                        }
                        catch (IOException e) {
                            System.err.println("Error reading from part " + entry.getName() + " - " + e.toString());
                        }
                    }
                    else {
                        grouping.record(chunk);
                    }
                }
            }
            catch (NumberFormatException e2) {}
            return;
        }
        throw new IllegalArgumentException("Invalid chunk name " + entryName);
    }
}
