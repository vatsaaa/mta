// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hsmf.exceptions;

public final class ChunkNotFoundException extends Exception
{
    private static final long serialVersionUID = 1L;
    
    public ChunkNotFoundException() {
        super("Chunk not found");
    }
    
    public ChunkNotFoundException(final String chunkName) {
        super(chunkName + " was named, but not found in POIFS object");
    }
}
