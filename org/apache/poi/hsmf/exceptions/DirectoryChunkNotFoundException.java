// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hsmf.exceptions;

public final class DirectoryChunkNotFoundException extends Exception
{
    private static final long serialVersionUID = 1L;
    
    public DirectoryChunkNotFoundException(final String directory) {
        super("Directory Chunk " + directory + " was not found!");
    }
}
