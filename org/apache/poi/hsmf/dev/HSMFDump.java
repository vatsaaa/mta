// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hsmf.dev;

import java.io.InputStream;
import java.io.FileInputStream;
import java.io.IOException;
import org.apache.poi.hsmf.datatypes.Chunk;
import org.apache.poi.hsmf.datatypes.ChunkGroup;
import org.apache.poi.hsmf.datatypes.Types;
import org.apache.poi.hsmf.datatypes.MAPIProperty;
import org.apache.poi.hsmf.parsers.POIFSChunkParser;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;

public class HSMFDump
{
    private POIFSFileSystem fs;
    
    public HSMFDump(final POIFSFileSystem fs) {
        this.fs = fs;
    }
    
    public void dump() throws IOException {
        final ChunkGroup[] arr$;
        final ChunkGroup[] chunkGroups = arr$ = POIFSChunkParser.parse(this.fs);
        for (final ChunkGroup chunks : arr$) {
            System.out.println(chunks.getClass().getSimpleName());
            for (final Chunk chunk : chunks.getChunks()) {
                final MAPIProperty attr = MAPIProperty.get(chunk.getChunkId());
                String idName = attr.id + " - " + attr.name;
                if (attr == MAPIProperty.UNKNOWN) {
                    idName = chunk.getChunkId() + " - (unknown)";
                }
                System.out.println("   " + idName + " - " + Types.asName(chunk.getType()));
                System.out.println("       " + chunk.toString());
            }
            System.out.println();
        }
    }
    
    public static void main(final String[] args) throws Exception {
        for (final String file : args) {
            final POIFSFileSystem fs = new POIFSFileSystem(new FileInputStream(file));
            final HSMFDump dump = new HSMFDump(fs);
            dump.dump();
        }
    }
}
