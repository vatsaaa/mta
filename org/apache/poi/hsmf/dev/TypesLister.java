// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hsmf.dev;

import java.util.Iterator;
import org.apache.poi.hsmf.datatypes.Types;
import java.util.List;
import java.util.Collections;
import java.util.Comparator;
import java.util.Collection;
import java.util.ArrayList;
import org.apache.poi.hsmf.datatypes.MAPIProperty;
import java.io.PrintStream;

public class TypesLister
{
    public void listByName(final PrintStream out) {
        final ArrayList<MAPIProperty> all = new ArrayList<MAPIProperty>(MAPIProperty.getAll());
        Collections.sort(all, new Comparator<MAPIProperty>() {
            public int compare(final MAPIProperty a, final MAPIProperty b) {
                return a.name.compareTo(b.name);
            }
        });
        this.list(all, out);
    }
    
    public void listById(final PrintStream out) {
        final ArrayList<MAPIProperty> all = new ArrayList<MAPIProperty>(MAPIProperty.getAll());
        Collections.sort(all, new Comparator<MAPIProperty>() {
            public int compare(final MAPIProperty a, final MAPIProperty b) {
                if (a.id < b.id) {
                    return -1;
                }
                if (a.id > b.id) {
                    return 1;
                }
                return 0;
            }
        });
        this.list(all, out);
    }
    
    private void list(final ArrayList<MAPIProperty> list, final PrintStream out) {
        for (final MAPIProperty attr : list) {
            String id;
            for (id = Integer.toHexString(attr.id); id.length() < 4; id = "0" + id) {}
            out.println("0x" + id + " - " + attr.name);
            out.println("   " + attr.id + " - " + Types.asName(attr.usualType) + " (" + attr.usualType + ") - " + attr.mapiProperty);
        }
    }
    
    public static void main(final String[] args) {
        final TypesLister lister = new TypesLister();
        lister.listByName(System.out);
        System.out.println();
        lister.listById(System.out);
    }
}
