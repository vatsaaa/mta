// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hsmf.extractor;

import org.apache.poi.hsmf.datatypes.AttachmentChunks;
import java.text.SimpleDateFormat;
import org.apache.poi.hsmf.exceptions.ChunkNotFoundException;
import org.apache.poi.util.StringUtil;
import java.io.File;
import java.io.InputStream;
import org.apache.poi.poifs.filesystem.NPOIFSFileSystem;
import java.io.IOException;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.poifs.filesystem.DirectoryNode;
import org.apache.poi.POIDocument;
import org.apache.poi.hsmf.MAPIMessage;
import org.apache.poi.POIOLE2TextExtractor;

public class OutlookTextExtactor extends POIOLE2TextExtractor
{
    public OutlookTextExtactor(final MAPIMessage msg) {
        super(msg);
    }
    
    @Deprecated
    public OutlookTextExtactor(final DirectoryNode poifsDir, final POIFSFileSystem fs) throws IOException {
        this(new MAPIMessage(poifsDir, fs));
    }
    
    public OutlookTextExtactor(final DirectoryNode poifsDir) throws IOException {
        this(new MAPIMessage(poifsDir));
    }
    
    public OutlookTextExtactor(final POIFSFileSystem fs) throws IOException {
        this(new MAPIMessage(fs));
    }
    
    public OutlookTextExtactor(final NPOIFSFileSystem fs) throws IOException {
        this(new MAPIMessage(fs));
    }
    
    public OutlookTextExtactor(final InputStream inp) throws IOException {
        this(new MAPIMessage(inp));
    }
    
    public static void main(final String[] args) throws Exception {
        for (final String filename : args) {
            final OutlookTextExtactor extractor = new OutlookTextExtactor(new NPOIFSFileSystem(new File(filename)));
            System.out.println(extractor.getText());
        }
    }
    
    public MAPIMessage getMAPIMessage() {
        return (MAPIMessage)this.document;
    }
    
    @Override
    public String getText() {
        final MAPIMessage msg = (MAPIMessage)this.document;
        final StringBuffer s = new StringBuffer();
        msg.guess7BitEncoding();
        StringUtil.StringsIterator emails;
        try {
            emails = new StringUtil.StringsIterator(msg.getRecipientEmailAddressList());
        }
        catch (ChunkNotFoundException e) {
            emails = new StringUtil.StringsIterator(new String[0]);
        }
        try {
            s.append("From: " + msg.getDisplayFrom() + "\n");
        }
        catch (ChunkNotFoundException ex) {}
        try {
            this.handleEmails(s, "To", msg.getDisplayTo(), emails);
        }
        catch (ChunkNotFoundException ex2) {}
        try {
            this.handleEmails(s, "CC", msg.getDisplayCC(), emails);
        }
        catch (ChunkNotFoundException ex3) {}
        try {
            this.handleEmails(s, "BCC", msg.getDisplayBCC(), emails);
        }
        catch (ChunkNotFoundException ex4) {}
        try {
            final SimpleDateFormat f = new SimpleDateFormat("E, d MMM yyyy HH:mm:ss");
            s.append("Date: " + f.format(msg.getMessageDate().getTime()) + "\n");
        }
        catch (ChunkNotFoundException e) {
            try {
                final String[] arr$;
                final String[] headers = arr$ = msg.getHeaders();
                for (final String header : arr$) {
                    if (header.toLowerCase().startsWith("date:")) {
                        s.append("Date:" + header.substring(header.indexOf(58) + 1) + "\n");
                        break;
                    }
                }
            }
            catch (ChunkNotFoundException ex5) {}
        }
        try {
            s.append("Subject: " + msg.getSubject() + "\n");
        }
        catch (ChunkNotFoundException ex6) {}
        for (final AttachmentChunks att : msg.getAttachmentFiles()) {
            String ats = att.attachLongFileName.getValue();
            if (att.attachMimeTag != null && att.attachMimeTag.getValue() != null) {
                ats = att.attachMimeTag.getValue() + " = " + ats;
            }
            s.append("Attachment: " + ats + "\n");
        }
        try {
            s.append("\n" + msg.getTextBody() + "\n");
        }
        catch (ChunkNotFoundException ex7) {}
        return s.toString();
    }
    
    protected void handleEmails(final StringBuffer s, final String type, final String displayText, final StringUtil.StringsIterator emails) {
        if (displayText == null || displayText.length() == 0) {
            return;
        }
        final String[] names = displayText.split(";\\s*");
        boolean first = true;
        s.append(type + ": ");
        for (final String name : names) {
            if (first) {
                first = false;
            }
            else {
                s.append("; ");
            }
            s.append(name);
            if (emails.hasNext()) {
                final String email = emails.next();
                if (!email.equals(name)) {
                    s.append(" <" + email + ">");
                }
            }
        }
        s.append("\n");
    }
}
