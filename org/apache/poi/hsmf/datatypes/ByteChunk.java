// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hsmf.datatypes;

import java.io.OutputStream;
import java.io.IOException;
import org.apache.poi.util.IOUtils;
import java.io.InputStream;

public class ByteChunk extends Chunk
{
    private byte[] value;
    
    public ByteChunk(final String namePrefix, final int chunkId, final int type) {
        super(namePrefix, chunkId, type);
    }
    
    public ByteChunk(final int chunkId, final int type) {
        super(chunkId, type);
    }
    
    @Override
    public void readValue(final InputStream value) throws IOException {
        this.value = IOUtils.toByteArray(value);
    }
    
    @Override
    public void writeValue(final OutputStream out) throws IOException {
        out.write(this.value);
    }
    
    public byte[] getValue() {
        return this.value;
    }
    
    public void setValue(final byte[] value) {
        this.value = value;
    }
    
    public String getAs7bitString() {
        return StringChunk.parseAs7BitData(this.value);
    }
}
