// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hsmf.datatypes;

public final class Types
{
    public static final int UNSPECIFIED = 0;
    public static final int NULL = 1;
    public static final int SHORT = 2;
    public static final int LONG = 3;
    public static final int FLOAT = 4;
    public static final int DOUBLE = 5;
    public static final int CURRENCY = 6;
    public static final int APP_TIME = 7;
    public static final int ERROR = 10;
    public static final int BOOLEAN = 11;
    public static final int DIRECTORY = 13;
    public static final int LONG_LONG = 20;
    public static final int TIME = 64;
    public static final int CLS_ID = 72;
    public static final int BINARY = 258;
    public static final int ASCII_STRING = 30;
    public static final int UNICODE_STRING = 31;
    public static final int MULTIVALUED_FLAG = 4096;
    
    public static String asFileEnding(final int type) {
        String str;
        for (str = Integer.toHexString(type).toUpperCase(); str.length() < 4; str = "0" + str) {}
        return str;
    }
    
    public static String asName(final int type) {
        switch (type) {
            case 258: {
                return "Binary";
            }
            case 30: {
                return "ASCII String";
            }
            case 31: {
                return "Unicode String";
            }
            case 0: {
                return "Unspecified";
            }
            case 1: {
                return "Null";
            }
            case 2: {
                return "Short";
            }
            case 3: {
                return "Long";
            }
            case 20: {
                return "Long Long";
            }
            case 4: {
                return "Float";
            }
            case 5: {
                return "Double";
            }
            case 6: {
                return "Currency";
            }
            case 7: {
                return "Application Time";
            }
            case 10: {
                return "Error";
            }
            case 64: {
                return "Time";
            }
            case 11: {
                return "Boolean";
            }
            case 72: {
                return "CLS ID GUID";
            }
            case 13: {
                return "Directory";
            }
            case -1: {
                return "Unknown";
            }
            default: {
                return "0x" + Integer.toHexString(type);
            }
        }
    }
}
