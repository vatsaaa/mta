// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hsmf.datatypes;

import java.io.InputStream;
import java.io.IOException;
import java.io.OutputStream;

public abstract class Chunk
{
    public static final String DEFAULT_NAME_PREFIX = "__substg1.0_";
    protected int chunkId;
    protected int type;
    protected String namePrefix;
    
    protected Chunk(final String namePrefix, final int chunkId, final int type) {
        this.namePrefix = namePrefix;
        this.chunkId = chunkId;
        this.type = type;
    }
    
    protected Chunk(final int chunkId, final int type) {
        this("__substg1.0_", chunkId, type);
    }
    
    public int getChunkId() {
        return this.chunkId;
    }
    
    public int getType() {
        return this.type;
    }
    
    public String getEntryName() {
        String type;
        for (type = Integer.toHexString(this.type); type.length() < 4; type = "0" + type) {}
        String chunkId;
        for (chunkId = Integer.toHexString(this.chunkId); chunkId.length() < 4; chunkId = "0" + chunkId) {}
        return this.namePrefix + chunkId.toUpperCase() + type.toUpperCase();
    }
    
    public abstract void writeValue(final OutputStream p0) throws IOException;
    
    public abstract void readValue(final InputStream p0) throws IOException;
}
