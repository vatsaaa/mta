// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hsmf.datatypes;

import java.io.UnsupportedEncodingException;
import java.io.OutputStream;
import org.apache.poi.util.StringUtil;
import java.io.IOException;
import org.apache.poi.util.IOUtils;
import java.io.InputStream;

public class StringChunk extends Chunk
{
    private static final String DEFAULT_ENCODING = "CP1252";
    private String encoding7Bit;
    private byte[] rawValue;
    private String value;
    
    public StringChunk(final String namePrefix, final int chunkId, final int type) {
        super(namePrefix, chunkId, type);
        this.encoding7Bit = "CP1252";
    }
    
    public StringChunk(final int chunkId, final int type) {
        super(chunkId, type);
        this.encoding7Bit = "CP1252";
    }
    
    public String get7BitEncoding() {
        return this.encoding7Bit;
    }
    
    public void set7BitEncoding(final String encoding) {
        this.encoding7Bit = encoding;
        if (this.type == 30) {
            this.parseString();
        }
    }
    
    @Override
    public void readValue(final InputStream value) throws IOException {
        this.rawValue = IOUtils.toByteArray(value);
        this.parseString();
    }
    
    private void parseString() {
        String tmpValue = null;
        switch (this.type) {
            case 30: {
                tmpValue = parseAs7BitData(this.rawValue, this.encoding7Bit);
                break;
            }
            case 31: {
                tmpValue = StringUtil.getFromUnicodeLE(this.rawValue);
                break;
            }
            default: {
                throw new IllegalArgumentException("Invalid type " + this.type + " for String Chunk");
            }
        }
        this.value = tmpValue.replace("\u0000", "");
    }
    
    @Override
    public void writeValue(final OutputStream out) throws IOException {
        out.write(this.rawValue);
    }
    
    private void storeString() {
        switch (this.type) {
            case 30: {
                try {
                    this.rawValue = this.value.getBytes(this.encoding7Bit);
                    break;
                }
                catch (UnsupportedEncodingException e) {
                    throw new RuntimeException("Encoding not found - " + this.encoding7Bit, e);
                }
            }
            case 31: {
                this.rawValue = new byte[this.value.length() * 2];
                StringUtil.putUnicodeLE(this.value, this.rawValue, 0);
                break;
            }
            default: {
                throw new IllegalArgumentException("Invalid type " + this.type + " for String Chunk");
            }
        }
    }
    
    public String getValue() {
        return this.value;
    }
    
    public byte[] getRawValue() {
        return this.rawValue;
    }
    
    public void setValue(final String str) {
        this.value = str;
        this.storeString();
    }
    
    @Override
    public String toString() {
        return this.value;
    }
    
    protected static String parseAs7BitData(final byte[] data) {
        return parseAs7BitData(data, "CP1252");
    }
    
    protected static String parseAs7BitData(final byte[] data, final String encoding) {
        try {
            return new String(data, encoding);
        }
        catch (UnsupportedEncodingException e) {
            throw new RuntimeException("Encoding not found - " + encoding, e);
        }
    }
}
