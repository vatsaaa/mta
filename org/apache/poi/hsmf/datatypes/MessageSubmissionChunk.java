// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hsmf.datatypes;

import java.io.OutputStream;
import java.io.IOException;
import java.util.regex.Matcher;
import java.io.UnsupportedEncodingException;
import org.apache.poi.util.IOUtils;
import java.io.InputStream;
import java.util.regex.Pattern;
import java.util.Calendar;

public class MessageSubmissionChunk extends Chunk
{
    private String rawId;
    private Calendar date;
    private static final Pattern datePatern;
    
    public MessageSubmissionChunk(final String namePrefix, final int chunkId, final int type) {
        super(namePrefix, chunkId, type);
    }
    
    public MessageSubmissionChunk(final int chunkId, final int type) {
        super(chunkId, type);
    }
    
    @Override
    public void readValue(final InputStream value) throws IOException {
        try {
            final byte[] data = IOUtils.toByteArray(value);
            this.rawId = new String(data, "ASCII");
        }
        catch (UnsupportedEncodingException e) {
            throw new RuntimeException("Core encoding not found, JVM broken?", e);
        }
        final String[] arr$;
        final String[] parts = arr$ = this.rawId.split(";");
        for (final String part : arr$) {
            if (part.startsWith("l=") && part.indexOf(45) != -1 && part.indexOf(45) != part.lastIndexOf(45)) {
                final String dateS = part.substring(part.indexOf(45) + 1, part.lastIndexOf(45));
                final Matcher m = MessageSubmissionChunk.datePatern.matcher(dateS);
                if (m.matches()) {
                    (this.date = Calendar.getInstance()).set(1, Integer.parseInt(m.group(1)) + 2000);
                    this.date.set(2, Integer.parseInt(m.group(2)) - 1);
                    this.date.set(5, Integer.parseInt(m.group(3)));
                    this.date.set(11, Integer.parseInt(m.group(4)));
                    this.date.set(12, Integer.parseInt(m.group(5)));
                    this.date.set(13, Integer.parseInt(m.group(6)));
                    this.date.set(14, 0);
                }
                else {
                    System.err.println("Warning - unable to make sense of date " + dateS);
                }
            }
        }
    }
    
    @Override
    public void writeValue(final OutputStream out) throws IOException {
        try {
            final byte[] data = this.rawId.getBytes("ASCII");
            out.write(data);
        }
        catch (UnsupportedEncodingException e) {
            throw new RuntimeException("Core encoding not found, JVM broken?", e);
        }
    }
    
    public Calendar getAcceptedAtTime() {
        return this.date;
    }
    
    public String getSubmissionId() {
        return this.rawId;
    }
    
    static {
        datePatern = Pattern.compile("(\\d\\d)(\\d\\d)(\\d\\d)(\\d\\d)(\\d\\d)(\\d\\d)Z?");
    }
}
