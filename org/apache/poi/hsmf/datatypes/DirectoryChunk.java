// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hsmf.datatypes;

import java.io.OutputStream;
import java.io.InputStream;
import java.io.IOException;
import org.apache.poi.hsmf.MAPIMessage;
import org.apache.poi.poifs.filesystem.DirectoryNode;

public class DirectoryChunk extends Chunk
{
    private DirectoryNode dir;
    
    public DirectoryChunk(final DirectoryNode dir, final String namePrefix, final int chunkId, final int type) {
        super(namePrefix, chunkId, type);
        this.dir = dir;
    }
    
    public DirectoryNode getDirectory() {
        return this.dir;
    }
    
    public MAPIMessage getAsEmbededMessage() throws IOException {
        return new MAPIMessage(this.dir);
    }
    
    @Override
    public void readValue(final InputStream value) {
    }
    
    @Override
    public void writeValue(final OutputStream out) {
    }
}
