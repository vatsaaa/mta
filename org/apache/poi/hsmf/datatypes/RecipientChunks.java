// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hsmf.datatypes;

import java.util.Comparator;
import java.util.ArrayList;
import java.util.List;

public final class RecipientChunks implements ChunkGroup
{
    public static final String PREFIX = "__recip_version1.0_#";
    public static final MAPIProperty RECIPIENT_NAME;
    public static final MAPIProperty DELIVERY_TYPE;
    public static final MAPIProperty RECIPIENT_EMAIL_ADDRESS;
    public static final MAPIProperty RECIPIENT_SEARCH;
    public static final MAPIProperty RECIPIENT_SMTP_ADDRESS;
    public static final MAPIProperty RECIPIENT_DISPLAY_NAME;
    public int recipientNumber;
    public ByteChunk recipientSearchChunk;
    public StringChunk recipientNameChunk;
    public StringChunk recipientEmailChunk;
    public StringChunk recipientSMTPChunk;
    public StringChunk deliveryTypeChunk;
    public StringChunk recipientDisplayNameChunk;
    private List<Chunk> allChunks;
    
    public RecipientChunks(final String name) {
        this.allChunks = new ArrayList<Chunk>();
        this.recipientNumber = -1;
        final int splitAt = name.lastIndexOf(35);
        if (splitAt > -1) {
            final String number = name.substring(splitAt + 1);
            try {
                this.recipientNumber = Integer.parseInt(number, 16);
            }
            catch (NumberFormatException e) {
                System.err.println("Invalid recipient number in name " + name);
            }
        }
    }
    
    public String getRecipientName() {
        if (this.recipientNameChunk != null) {
            return this.recipientNameChunk.getValue();
        }
        if (this.recipientDisplayNameChunk != null) {
            return this.recipientDisplayNameChunk.getValue();
        }
        return null;
    }
    
    public String getRecipientEmailAddress() {
        if (this.recipientSMTPChunk != null) {
            return this.recipientSMTPChunk.getValue();
        }
        if (this.recipientEmailChunk == null) {
            if (this.recipientNameChunk != null) {
                final String name = this.recipientNameChunk.getValue();
                if (name.indexOf(64) > -1) {
                    if (name.startsWith("'") && name.endsWith("'")) {
                        return name.substring(1, name.length() - 1);
                    }
                    return name;
                }
            }
            if (this.recipientSearchChunk != null) {
                final String search = this.recipientSearchChunk.getAs7bitString();
                if (search.indexOf("SMTP:") != -1) {
                    return search.substring(search.indexOf("SMTP:") + 5);
                }
            }
            return null;
        }
        final String email = this.recipientEmailChunk.getValue();
        final int cne = email.indexOf("/CN=");
        if (cne == -1) {
            return email;
        }
        return email.substring(cne + 4);
    }
    
    public Chunk[] getAll() {
        return this.allChunks.toArray(new Chunk[this.allChunks.size()]);
    }
    
    public Chunk[] getChunks() {
        return this.getAll();
    }
    
    public void record(final Chunk chunk) {
        if (chunk.getChunkId() == RecipientChunks.RECIPIENT_SEARCH.id) {
            this.recipientSearchChunk = (ByteChunk)chunk;
        }
        else if (chunk.getChunkId() == RecipientChunks.RECIPIENT_NAME.id) {
            this.recipientDisplayNameChunk = (StringChunk)chunk;
        }
        else if (chunk.getChunkId() == RecipientChunks.RECIPIENT_DISPLAY_NAME.id) {
            this.recipientNameChunk = (StringChunk)chunk;
        }
        else if (chunk.getChunkId() == RecipientChunks.RECIPIENT_EMAIL_ADDRESS.id) {
            this.recipientEmailChunk = (StringChunk)chunk;
        }
        else if (chunk.getChunkId() == RecipientChunks.RECIPIENT_SMTP_ADDRESS.id) {
            this.recipientSMTPChunk = (StringChunk)chunk;
        }
        else if (chunk.getChunkId() == RecipientChunks.DELIVERY_TYPE.id) {
            this.deliveryTypeChunk = (StringChunk)chunk;
        }
        this.allChunks.add(chunk);
    }
    
    static {
        RECIPIENT_NAME = MAPIProperty.DISPLAY_NAME;
        DELIVERY_TYPE = MAPIProperty.ADDRTYPE;
        RECIPIENT_EMAIL_ADDRESS = MAPIProperty.EMAIL_ADDRESS;
        RECIPIENT_SEARCH = MAPIProperty.SEARCH_KEY;
        RECIPIENT_SMTP_ADDRESS = MAPIProperty.SMTP_ADDRESS;
        RECIPIENT_DISPLAY_NAME = MAPIProperty.RECIPIENT_DISPLAY_NAME;
    }
    
    public static class RecipientChunksSorter implements Comparator<RecipientChunks>
    {
        public int compare(final RecipientChunks a, final RecipientChunks b) {
            if (a.recipientNumber < b.recipientNumber) {
                return -1;
            }
            if (a.recipientNumber > b.recipientNumber) {
                return 1;
            }
            return 0;
        }
    }
}
