// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hsmf.datatypes;

public interface ChunkGroup
{
    Chunk[] getChunks();
    
    void record(final Chunk p0);
}
