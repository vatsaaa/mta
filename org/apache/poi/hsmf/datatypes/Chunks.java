// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hsmf.datatypes;

import java.util.ArrayList;
import java.util.List;

public final class Chunks implements ChunkGroup
{
    private List<Chunk> allChunks;
    public StringChunk messageClass;
    public StringChunk textBodyChunk;
    public StringChunk htmlBodyChunkString;
    public ByteChunk htmlBodyChunkBinary;
    public ByteChunk rtfBodyChunk;
    public StringChunk subjectChunk;
    public StringChunk displayToChunk;
    public StringChunk displayFromChunk;
    public StringChunk displayCCChunk;
    public StringChunk displayBCCChunk;
    public StringChunk conversationTopic;
    public StringChunk sentByServerType;
    public StringChunk messageHeaders;
    public MessageSubmissionChunk submissionChunk;
    public StringChunk emailFromChunk;
    public StringChunk messageId;
    
    public Chunks() {
        this.allChunks = new ArrayList<Chunk>();
    }
    
    public Chunk[] getAll() {
        return this.allChunks.toArray(new Chunk[this.allChunks.size()]);
    }
    
    public Chunk[] getChunks() {
        return this.getAll();
    }
    
    public void record(final Chunk chunk) {
        if (chunk.getChunkId() == MAPIProperty.MESSAGE_CLASS.id) {
            this.messageClass = (StringChunk)chunk;
        }
        else if (chunk.getChunkId() == MAPIProperty.INTERNET_MESSAGE_ID.id) {
            this.messageId = (StringChunk)chunk;
        }
        else if (chunk.getChunkId() == MAPIProperty.MESSAGE_SUBMISSION_ID.id) {
            this.submissionChunk = (MessageSubmissionChunk)chunk;
        }
        else if (chunk.getChunkId() == MAPIProperty.RECEIVED_BY_ADDRTYPE.id) {
            this.sentByServerType = (StringChunk)chunk;
        }
        else if (chunk.getChunkId() == MAPIProperty.TRANSPORT_MESSAGE_HEADERS.id) {
            this.messageHeaders = (StringChunk)chunk;
        }
        else if (chunk.getChunkId() == MAPIProperty.CONVERSATION_TOPIC.id) {
            this.conversationTopic = (StringChunk)chunk;
        }
        else if (chunk.getChunkId() == MAPIProperty.SUBJECT.id) {
            this.subjectChunk = (StringChunk)chunk;
        }
        else if (chunk.getChunkId() != MAPIProperty.ORIGINAL_SUBJECT.id) {
            if (chunk.getChunkId() == MAPIProperty.DISPLAY_TO.id) {
                this.displayToChunk = (StringChunk)chunk;
            }
            else if (chunk.getChunkId() == MAPIProperty.DISPLAY_CC.id) {
                this.displayCCChunk = (StringChunk)chunk;
            }
            else if (chunk.getChunkId() == MAPIProperty.DISPLAY_BCC.id) {
                this.displayBCCChunk = (StringChunk)chunk;
            }
            else if (chunk.getChunkId() == MAPIProperty.SENDER_EMAIL_ADDRESS.id) {
                this.emailFromChunk = (StringChunk)chunk;
            }
            else if (chunk.getChunkId() == MAPIProperty.SENDER_NAME.id) {
                this.displayFromChunk = (StringChunk)chunk;
            }
            else if (chunk.getChunkId() == MAPIProperty.BODY.id) {
                this.textBodyChunk = (StringChunk)chunk;
            }
            else if (chunk.getChunkId() == MAPIProperty.BODY_HTML.id) {
                if (chunk instanceof StringChunk) {
                    this.htmlBodyChunkString = (StringChunk)chunk;
                }
                if (chunk instanceof ByteChunk) {
                    this.htmlBodyChunkBinary = (ByteChunk)chunk;
                }
            }
            else if (chunk.getChunkId() == MAPIProperty.RTF_COMPRESSED.id) {
                this.rtfBodyChunk = (ByteChunk)chunk;
            }
        }
        this.allChunks.add(chunk);
    }
}
