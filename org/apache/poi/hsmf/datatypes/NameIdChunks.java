// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hsmf.datatypes;

import java.util.ArrayList;
import java.util.List;

public final class NameIdChunks implements ChunkGroup
{
    public static final String PREFIX = "__nameid_version1.0";
    private List<Chunk> allChunks;
    
    public NameIdChunks() {
        this.allChunks = new ArrayList<Chunk>();
    }
    
    public Chunk[] getAll() {
        return this.allChunks.toArray(new Chunk[this.allChunks.size()]);
    }
    
    public Chunk[] getChunks() {
        return this.getAll();
    }
    
    public void record(final Chunk chunk) {
        this.allChunks.add(chunk);
    }
}
