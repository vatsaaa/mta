// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hsmf.datatypes;

import java.util.Comparator;
import java.util.ArrayList;
import java.util.List;

public class AttachmentChunks implements ChunkGroup
{
    public static final String PREFIX = "__attach_version1.0_#";
    public ByteChunk attachData;
    public StringChunk attachExtension;
    public StringChunk attachFileName;
    public StringChunk attachLongFileName;
    public StringChunk attachMimeTag;
    public DirectoryChunk attachmentDirectory;
    public ByteChunk attachRenderingWMF;
    private String poifsName;
    private List<Chunk> allChunks;
    
    public AttachmentChunks(final String poifsName) {
        this.allChunks = new ArrayList<Chunk>();
        this.poifsName = poifsName;
    }
    
    public Chunk[] getAll() {
        return this.allChunks.toArray(new Chunk[this.allChunks.size()]);
    }
    
    public Chunk[] getChunks() {
        return this.getAll();
    }
    
    public String getPOIFSName() {
        return this.poifsName;
    }
    
    public void record(final Chunk chunk) {
        if (chunk.getChunkId() != MAPIProperty.ATTACH_ADDITIONAL_INFO.id) {
            if (chunk.getChunkId() != MAPIProperty.ATTACH_CONTENT_BASE.id) {
                if (chunk.getChunkId() != MAPIProperty.ATTACH_CONTENT_LOCATION.id) {
                    if (chunk.getChunkId() == MAPIProperty.ATTACH_DATA.id) {
                        if (chunk instanceof ByteChunk) {
                            this.attachData = (ByteChunk)chunk;
                        }
                        else if (chunk instanceof DirectoryChunk) {
                            this.attachmentDirectory = (DirectoryChunk)chunk;
                        }
                        else {
                            System.err.println("Unexpected data chunk of type " + chunk);
                        }
                    }
                    else if (chunk.getChunkId() != MAPIProperty.ATTACH_DISPOSITION.id) {
                        if (chunk.getChunkId() != MAPIProperty.ATTACH_ENCODING.id) {
                            if (chunk.getChunkId() == MAPIProperty.ATTACH_EXTENSION.id) {
                                this.attachExtension = (StringChunk)chunk;
                            }
                            else if (chunk.getChunkId() == MAPIProperty.ATTACH_FILENAME.id) {
                                this.attachFileName = (StringChunk)chunk;
                            }
                            else if (chunk.getChunkId() != MAPIProperty.ATTACH_FLAGS.id) {
                                if (chunk.getChunkId() == MAPIProperty.ATTACH_LONG_FILENAME.id) {
                                    this.attachLongFileName = (StringChunk)chunk;
                                }
                                else if (chunk.getChunkId() != MAPIProperty.ATTACH_LONG_PATHNAME.id) {
                                    if (chunk.getChunkId() == MAPIProperty.ATTACH_MIME_TAG.id) {
                                        this.attachMimeTag = (StringChunk)chunk;
                                    }
                                    else if (chunk.getChunkId() == MAPIProperty.ATTACH_RENDERING.id) {
                                        this.attachRenderingWMF = (ByteChunk)chunk;
                                    }
                                    else if (chunk.getChunkId() == MAPIProperty.ATTACH_SIZE.id) {}
                                }
                            }
                        }
                    }
                }
            }
        }
        this.allChunks.add(chunk);
    }
    
    public static class AttachmentChunksSorter implements Comparator<AttachmentChunks>
    {
        public int compare(final AttachmentChunks a, final AttachmentChunks b) {
            return a.poifsName.compareTo(b.poifsName);
        }
    }
}
