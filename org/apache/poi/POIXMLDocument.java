// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi;

import java.util.Set;
import java.util.HashSet;
import java.io.OutputStream;
import java.util.Map;
import java.util.HashMap;
import org.apache.poi.openxml4j.exceptions.OpenXML4JException;
import java.util.List;
import org.apache.poi.poifs.common.POIFSConstants;
import java.io.PushbackInputStream;
import org.apache.poi.util.IOUtils;
import java.io.InputStream;
import java.util.Iterator;
import org.apache.poi.openxml4j.opc.PackageRelationshipCollection;
import org.apache.poi.openxml4j.opc.PackageRelationship;
import org.apache.poi.openxml4j.opc.PackagePart;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import java.io.IOException;
import org.apache.poi.openxml4j.opc.OPCPackage;

public abstract class POIXMLDocument extends POIXMLDocumentPart
{
    public static final String DOCUMENT_CREATOR = "Apache POI";
    public static final String OLE_OBJECT_REL_TYPE = "http://schemas.openxmlformats.org/officeDocument/2006/relationships/oleObject";
    public static final String PACK_OBJECT_REL_TYPE = "http://schemas.openxmlformats.org/officeDocument/2006/relationships/package";
    private OPCPackage pkg;
    private POIXMLProperties properties;
    
    protected POIXMLDocument(final OPCPackage pkg) {
        super(pkg);
        this.pkg = pkg;
    }
    
    public static OPCPackage openPackage(final String path) throws IOException {
        try {
            return OPCPackage.open(path);
        }
        catch (InvalidFormatException e) {
            throw new IOException(e.toString());
        }
    }
    
    public OPCPackage getPackage() {
        return this.pkg;
    }
    
    protected PackagePart getCorePart() {
        return this.getPackagePart();
    }
    
    protected PackagePart[] getRelatedByType(final String contentType) throws InvalidFormatException {
        final PackageRelationshipCollection partsC = this.getPackagePart().getRelationshipsByType(contentType);
        final PackagePart[] parts = new PackagePart[partsC.size()];
        int count = 0;
        for (final PackageRelationship rel : partsC) {
            parts[count] = this.getPackagePart().getRelatedPart(rel);
            ++count;
        }
        return parts;
    }
    
    public static boolean hasOOXMLHeader(final InputStream inp) throws IOException {
        inp.mark(4);
        final byte[] header = new byte[4];
        IOUtils.readFully(inp, header);
        if (inp instanceof PushbackInputStream) {
            final PushbackInputStream pin = (PushbackInputStream)inp;
            pin.unread(header);
        }
        else {
            inp.reset();
        }
        return header[0] == POIFSConstants.OOXML_FILE_HEADER[0] && header[1] == POIFSConstants.OOXML_FILE_HEADER[1] && header[2] == POIFSConstants.OOXML_FILE_HEADER[2] && header[3] == POIFSConstants.OOXML_FILE_HEADER[3];
    }
    
    public POIXMLProperties getProperties() {
        if (this.properties == null) {
            try {
                this.properties = new POIXMLProperties(this.pkg);
            }
            catch (Exception e) {
                throw new POIXMLException(e);
            }
        }
        return this.properties;
    }
    
    public abstract List<PackagePart> getAllEmbedds() throws OpenXML4JException;
    
    protected final void load(final POIXMLFactory factory) throws IOException {
        final Map<PackagePart, POIXMLDocumentPart> context = new HashMap<PackagePart, POIXMLDocumentPart>();
        try {
            this.read(factory, context);
        }
        catch (OpenXML4JException e) {
            throw new POIXMLException(e);
        }
        this.onDocumentRead();
        context.clear();
    }
    
    public final void write(final OutputStream stream) throws IOException {
        final Set<PackagePart> context = new HashSet<PackagePart>();
        this.onSave(context);
        context.clear();
        this.getProperties().commit();
        this.getPackage().save(stream);
    }
}
