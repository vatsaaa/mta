// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi;

public class EncryptedDocumentException extends IllegalStateException
{
    public EncryptedDocumentException(final String s) {
        super(s);
    }
}
