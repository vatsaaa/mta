// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hwpf.converter;

import org.apache.poi.util.POILogFactory;
import org.apache.poi.hwpf.usermodel.TableCell;
import org.apache.poi.hwpf.usermodel.TableRow;
import org.apache.poi.hwpf.usermodel.Table;
import org.apache.poi.hwpf.usermodel.Paragraph;
import org.w3c.dom.NodeList;
import org.apache.poi.hwpf.usermodel.Picture;
import org.apache.poi.hwpf.usermodel.OfficeDrawing;
import org.apache.poi.hwpf.HWPFDocument;
import org.apache.poi.hpsf.SummaryInformation;
import java.util.Iterator;
import org.apache.poi.hwpf.usermodel.Bookmark;
import org.apache.poi.hwpf.usermodel.Range;
import org.w3c.dom.Text;
import org.apache.poi.hwpf.usermodel.CharacterRun;
import org.apache.poi.hwpf.usermodel.Section;
import java.util.LinkedHashSet;
import java.util.ArrayList;
import org.apache.poi.hwpf.HWPFDocumentCore;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import org.w3c.dom.Document;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.TransformerFactory;
import java.io.Writer;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Node;
import javax.xml.transform.dom.DOMSource;
import java.io.FileWriter;
import java.io.File;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import org.w3c.dom.Element;
import java.util.List;
import org.apache.poi.util.POILogger;

public class WordToFoConverter extends AbstractWordConverter
{
    private static final POILogger logger;
    private List<Element> endnotes;
    protected final FoDocumentFacade foDocumentFacade;
    private AtomicInteger internalLinkCounter;
    private boolean outputCharactersLanguage;
    private Set<String> usedIds;
    
    public static void main(final String[] args) {
        if (args.length < 2) {
            System.err.println("Usage: WordToFoConverter <inputFile.doc> <saveTo.fo>");
            return;
        }
        System.out.println("Converting " + args[0]);
        System.out.println("Saving output to " + args[1]);
        try {
            final Document doc = process(new File(args[0]));
            final FileWriter out = new FileWriter(args[1]);
            final DOMSource domSource = new DOMSource(doc);
            final StreamResult streamResult = new StreamResult(out);
            final TransformerFactory tf = TransformerFactory.newInstance();
            final Transformer serializer = tf.newTransformer();
            serializer.setOutputProperty("encoding", "UTF-8");
            serializer.setOutputProperty("indent", "yes");
            serializer.transform(domSource, streamResult);
            out.close();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    static Document process(final File docFile) throws Exception {
        final HWPFDocumentCore hwpfDocument = AbstractWordUtils.loadDoc(docFile);
        final WordToFoConverter wordToFoConverter = new WordToFoConverter(DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument());
        wordToFoConverter.processDocument(hwpfDocument);
        return wordToFoConverter.getDocument();
    }
    
    public WordToFoConverter(final Document document) {
        this.endnotes = new ArrayList<Element>(0);
        this.internalLinkCounter = new AtomicInteger(0);
        this.outputCharactersLanguage = false;
        this.usedIds = new LinkedHashSet<String>();
        this.foDocumentFacade = new FoDocumentFacade(document);
    }
    
    public WordToFoConverter(final FoDocumentFacade foDocumentFacade) {
        this.endnotes = new ArrayList<Element>(0);
        this.internalLinkCounter = new AtomicInteger(0);
        this.outputCharactersLanguage = false;
        this.usedIds = new LinkedHashSet<String>();
        this.foDocumentFacade = foDocumentFacade;
    }
    
    protected Element createNoteInline(final String noteIndexText) {
        final Element inline = this.foDocumentFacade.createInline();
        inline.setTextContent(noteIndexText);
        inline.setAttribute("baseline-shift", "super");
        inline.setAttribute("font-size", "smaller");
        return inline;
    }
    
    protected String createPageMaster(final Section section, final String type, final int sectionIndex) {
        final float height = section.getPageHeight() / 1440.0f;
        final float width = section.getPageWidth() / 1440.0f;
        final float leftMargin = section.getMarginLeft() / 1440.0f;
        final float rightMargin = section.getMarginRight() / 1440.0f;
        final float topMargin = section.getMarginTop() / 1440.0f;
        final float bottomMargin = section.getMarginBottom() / 1440.0f;
        final String pageMasterName = type + "-page" + sectionIndex;
        final Element pageMaster = this.foDocumentFacade.addSimplePageMaster(pageMasterName);
        pageMaster.setAttribute("page-height", height + "in");
        pageMaster.setAttribute("page-width", width + "in");
        final Element regionBody = this.foDocumentFacade.addRegionBody(pageMaster);
        regionBody.setAttribute("margin", topMargin + "in " + rightMargin + "in " + bottomMargin + "in " + leftMargin + "in");
        if (section.getNumColumns() > 1) {
            regionBody.setAttribute("column-count", "" + section.getNumColumns());
            if (section.isColumnsEvenlySpaced()) {
                final float distance = section.getDistanceBetweenColumns() / 1440.0f;
                regionBody.setAttribute("column-gap", distance + "in");
            }
            else {
                regionBody.setAttribute("column-gap", "0.25in");
            }
        }
        return pageMasterName;
    }
    
    @Override
    public Document getDocument() {
        return this.foDocumentFacade.getDocument();
    }
    
    public boolean isOutputCharactersLanguage() {
        return this.outputCharactersLanguage;
    }
    
    @Override
    protected void outputCharacters(final Element block, final CharacterRun characterRun, final String text) {
        final Element inline = this.foDocumentFacade.createInline();
        final FontReplacer.Triplet triplet = this.getCharacterRunTriplet(characterRun);
        if (AbstractWordUtils.isNotEmpty(triplet.fontName)) {
            WordToFoUtils.setFontFamily(inline, triplet.fontName);
        }
        WordToFoUtils.setBold(inline, triplet.bold);
        WordToFoUtils.setItalic(inline, triplet.italic);
        WordToFoUtils.setFontSize(inline, characterRun.getFontSize() / 2);
        WordToFoUtils.setCharactersProperties(characterRun, inline);
        if (this.isOutputCharactersLanguage()) {
            WordToFoUtils.setLanguage(characterRun, inline);
        }
        block.appendChild(inline);
        final Text textNode = this.foDocumentFacade.createText(text);
        inline.appendChild(textNode);
    }
    
    @Override
    protected void processBookmarks(final HWPFDocumentCore wordDocument, final Element currentBlock, final Range range, final int currentTableLevel, final List<Bookmark> rangeBookmarks) {
        Element parent = currentBlock;
        for (final Bookmark bookmark : rangeBookmarks) {
            final Element bookmarkElement = this.foDocumentFacade.createInline();
            final String idName = "bookmark_" + bookmark.getName();
            if (this.setId(bookmarkElement, idName)) {
                parent.appendChild(bookmarkElement);
                parent = bookmarkElement;
            }
        }
        if (range != null) {
            this.processCharacters(wordDocument, currentTableLevel, range, parent);
        }
    }
    
    @Override
    protected void processDocumentInformation(final SummaryInformation summaryInformation) {
        if (AbstractWordUtils.isNotEmpty(summaryInformation.getTitle())) {
            this.foDocumentFacade.setTitle(summaryInformation.getTitle());
        }
        if (AbstractWordUtils.isNotEmpty(summaryInformation.getAuthor())) {
            this.foDocumentFacade.setCreator(summaryInformation.getAuthor());
        }
        if (AbstractWordUtils.isNotEmpty(summaryInformation.getKeywords())) {
            this.foDocumentFacade.setKeywords(summaryInformation.getKeywords());
        }
        if (AbstractWordUtils.isNotEmpty(summaryInformation.getComments())) {
            this.foDocumentFacade.setDescription(summaryInformation.getComments());
        }
    }
    
    @Override
    protected void processDrawnObject(final HWPFDocument doc, final CharacterRun characterRun, final OfficeDrawing officeDrawing, final String path, final Element block) {
        final Element externalGraphic = this.foDocumentFacade.createExternalGraphic(path);
        block.appendChild(externalGraphic);
    }
    
    @Override
    protected void processEndnoteAutonumbered(final HWPFDocument wordDocument, final int noteIndex, final Element block, final Range endnoteTextRange) {
        final String textIndex = String.valueOf(this.internalLinkCounter.incrementAndGet());
        final String forwardLinkName = "endnote_" + textIndex;
        final String backwardLinkName = "endnote_back_" + textIndex;
        final Element forwardLink = this.foDocumentFacade.createBasicLinkInternal(forwardLinkName);
        forwardLink.appendChild(this.createNoteInline(textIndex));
        this.setId(forwardLink, backwardLinkName);
        block.appendChild(forwardLink);
        final Element endnote = this.foDocumentFacade.createBlock();
        final Element backwardLink = this.foDocumentFacade.createBasicLinkInternal(backwardLinkName);
        backwardLink.appendChild(this.createNoteInline(textIndex + " "));
        this.setId(backwardLink, forwardLinkName);
        endnote.appendChild(backwardLink);
        this.processCharacters(wordDocument, Integer.MIN_VALUE, endnoteTextRange, endnote);
        WordToFoUtils.compactInlines(endnote);
        this.endnotes.add(endnote);
    }
    
    @Override
    protected void processFootnoteAutonumbered(final HWPFDocument wordDocument, final int noteIndex, final Element block, final Range footnoteTextRange) {
        final String textIndex = String.valueOf(this.internalLinkCounter.incrementAndGet());
        final String forwardLinkName = "footnote_" + textIndex;
        final String backwardLinkName = "footnote_back_" + textIndex;
        final Element footNote = this.foDocumentFacade.createFootnote();
        block.appendChild(footNote);
        final Element inline = this.foDocumentFacade.createInline();
        final Element forwardLink = this.foDocumentFacade.createBasicLinkInternal(forwardLinkName);
        forwardLink.appendChild(this.createNoteInline(textIndex));
        this.setId(forwardLink, backwardLinkName);
        inline.appendChild(forwardLink);
        footNote.appendChild(inline);
        final Element footnoteBody = this.foDocumentFacade.createFootnoteBody();
        final Element footnoteBlock = this.foDocumentFacade.createBlock();
        final Element backwardLink = this.foDocumentFacade.createBasicLinkInternal(backwardLinkName);
        backwardLink.appendChild(this.createNoteInline(textIndex + " "));
        this.setId(backwardLink, forwardLinkName);
        footnoteBlock.appendChild(backwardLink);
        footnoteBody.appendChild(footnoteBlock);
        footNote.appendChild(footnoteBody);
        this.processCharacters(wordDocument, Integer.MIN_VALUE, footnoteTextRange, footnoteBlock);
        WordToFoUtils.compactInlines(footnoteBlock);
    }
    
    @Override
    protected void processHyperlink(final HWPFDocumentCore wordDocument, final Element currentBlock, final Range textRange, final int currentTableLevel, final String hyperlink) {
        final Element basicLink = this.foDocumentFacade.createBasicLinkExternal(hyperlink);
        currentBlock.appendChild(basicLink);
        if (textRange != null) {
            this.processCharacters(wordDocument, currentTableLevel, textRange, basicLink);
        }
    }
    
    @Override
    protected void processImage(final Element currentBlock, final boolean inlined, final Picture picture, final String url) {
        final Element externalGraphic = this.foDocumentFacade.createExternalGraphic(url);
        WordToFoUtils.setPictureProperties(picture, externalGraphic);
        currentBlock.appendChild(externalGraphic);
    }
    
    @Override
    protected void processImageWithoutPicturesManager(final Element currentBlock, final boolean inlined, final Picture picture) {
        currentBlock.appendChild(this.foDocumentFacade.document.createComment("Image link to '" + picture.suggestFullFileName() + "' can be here"));
    }
    
    @Override
    protected void processLineBreak(final Element block, final CharacterRun characterRun) {
        block.appendChild(this.foDocumentFacade.createBlock());
    }
    
    @Override
    protected void processPageBreak(final HWPFDocumentCore wordDocument, final Element flow) {
        Element block = null;
        final NodeList childNodes = flow.getChildNodes();
        if (childNodes.getLength() > 0) {
            final Node lastChild = childNodes.item(childNodes.getLength() - 1);
            if (lastChild instanceof Element) {
                final Element lastElement = (Element)lastChild;
                if (!lastElement.hasAttribute("break-after")) {
                    block = lastElement;
                }
            }
        }
        if (block == null) {
            block = this.foDocumentFacade.createBlock();
            flow.appendChild(block);
        }
        block.setAttribute("break-after", "page");
    }
    
    @Override
    protected void processPageref(final HWPFDocumentCore hwpfDocument, final Element currentBlock, final Range textRange, final int currentTableLevel, final String pageref) {
        final Element basicLink = this.foDocumentFacade.createBasicLinkInternal("bookmark_" + pageref);
        currentBlock.appendChild(basicLink);
        if (textRange != null) {
            this.processCharacters(hwpfDocument, currentTableLevel, textRange, basicLink);
        }
    }
    
    @Override
    protected void processParagraph(final HWPFDocumentCore hwpfDocument, final Element parentFopElement, final int currentTableLevel, final Paragraph paragraph, final String bulletText) {
        final Element block = this.foDocumentFacade.createBlock();
        parentFopElement.appendChild(block);
        WordToFoUtils.setParagraphProperties(paragraph, block);
        final int charRuns = paragraph.numCharacterRuns();
        if (charRuns == 0) {
            return;
        }
        boolean haveAnyText = false;
        if (AbstractWordUtils.isNotEmpty(bulletText)) {
            final Element inline = this.foDocumentFacade.createInline();
            block.appendChild(inline);
            final Text textNode = this.foDocumentFacade.createText(bulletText);
            inline.appendChild(textNode);
            haveAnyText |= (bulletText.trim().length() != 0);
        }
        haveAnyText = this.processCharacters(hwpfDocument, currentTableLevel, paragraph, block);
        if (!haveAnyText) {
            final Element leader = this.foDocumentFacade.createLeader();
            block.appendChild(leader);
        }
        WordToFoUtils.compactInlines(block);
    }
    
    @Override
    protected void processSection(final HWPFDocumentCore wordDocument, final Section section, final int sectionCounter) {
        final String regularPage = this.createPageMaster(section, "page", sectionCounter);
        final Element pageSequence = this.foDocumentFacade.addPageSequence(regularPage);
        final Element flow = this.foDocumentFacade.addFlowToPageSequence(pageSequence, "xsl-region-body");
        this.processParagraphes(wordDocument, flow, section, Integer.MIN_VALUE);
        if (this.endnotes != null && !this.endnotes.isEmpty()) {
            for (final Element endnote : this.endnotes) {
                flow.appendChild(endnote);
            }
            this.endnotes.clear();
        }
    }
    
    @Override
    protected void processTable(final HWPFDocumentCore wordDocument, final Element flow, final Table table) {
        final Element tableHeader = this.foDocumentFacade.createTableHeader();
        final Element tableBody = this.foDocumentFacade.createTableBody();
        final int[] tableCellEdges = AbstractWordUtils.buildTableCellEdgesArray(table);
        final int tableRows = table.numRows();
        int maxColumns = Integer.MIN_VALUE;
        for (int r = 0; r < tableRows; ++r) {
            maxColumns = Math.max(maxColumns, table.getRow(r).numCells());
        }
        for (int r = 0; r < tableRows; ++r) {
            final TableRow tableRow = table.getRow(r);
            final Element tableRowElement = this.foDocumentFacade.createTableRow();
            WordToFoUtils.setTableRowProperties(tableRow, tableRowElement);
            int currentEdgeIndex = 0;
            for (int rowCells = tableRow.numCells(), c = 0; c < rowCells; ++c) {
                final TableCell tableCell = tableRow.getCell(c);
                if (tableCell.isVerticallyMerged() && !tableCell.isFirstVerticallyMerged()) {
                    currentEdgeIndex += this.getNumberColumnsSpanned(tableCellEdges, currentEdgeIndex, tableCell);
                }
                else {
                    final Element tableCellElement = this.foDocumentFacade.createTableCell();
                    WordToFoUtils.setTableCellProperties(tableRow, tableCell, tableCellElement, r == 0, r == tableRows - 1, c == 0, c == rowCells - 1);
                    final int colSpan = this.getNumberColumnsSpanned(tableCellEdges, currentEdgeIndex, tableCell);
                    currentEdgeIndex += colSpan;
                    if (colSpan != 0) {
                        if (colSpan != 1) {
                            tableCellElement.setAttribute("number-columns-spanned", String.valueOf(colSpan));
                        }
                        final int rowSpan = this.getNumberRowsSpanned(table, tableCellEdges, r, c, tableCell);
                        if (rowSpan > 1) {
                            tableCellElement.setAttribute("number-rows-spanned", String.valueOf(rowSpan));
                        }
                        this.processParagraphes(wordDocument, tableCellElement, tableCell, table.getTableLevel());
                        if (!tableCellElement.hasChildNodes()) {
                            tableCellElement.appendChild(this.foDocumentFacade.createBlock());
                        }
                        tableRowElement.appendChild(tableCellElement);
                    }
                }
            }
            if (tableRowElement.hasChildNodes()) {
                if (tableRow.isTableHeader()) {
                    tableHeader.appendChild(tableRowElement);
                }
                else {
                    tableBody.appendChild(tableRowElement);
                }
            }
        }
        final Element tableElement = this.foDocumentFacade.createTable();
        tableElement.setAttribute("table-layout", "fixed");
        if (tableHeader.hasChildNodes()) {
            tableElement.appendChild(tableHeader);
        }
        if (tableBody.hasChildNodes()) {
            tableElement.appendChild(tableBody);
            flow.appendChild(tableElement);
        }
        else {
            WordToFoConverter.logger.log(POILogger.WARN, "Table without body starting on offset " + table.getStartOffset() + " -- " + table.getEndOffset());
        }
    }
    
    protected boolean setId(final Element element, final String id) {
        if (this.usedIds.contains(id)) {
            WordToFoConverter.logger.log(POILogger.WARN, "Tried to create element with same ID '", id, "'. Skipped");
            return false;
        }
        element.setAttribute("id", id);
        this.usedIds.add(id);
        return true;
    }
    
    public void setOutputCharactersLanguage(final boolean outputCharactersLanguage) {
        this.outputCharactersLanguage = outputCharactersLanguage;
    }
    
    static {
        logger = POILogFactory.getLogger(WordToFoConverter.class);
    }
}
