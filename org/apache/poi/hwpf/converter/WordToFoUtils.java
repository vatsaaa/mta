// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hwpf.converter;

import org.apache.poi.hwpf.usermodel.TableCell;
import org.apache.poi.hwpf.usermodel.TableRow;
import org.apache.poi.hwpf.usermodel.Picture;
import org.apache.poi.hwpf.usermodel.Paragraph;
import org.apache.poi.hwpf.usermodel.CharacterRun;
import org.apache.poi.hwpf.usermodel.BorderCode;
import org.w3c.dom.Element;

public class WordToFoUtils extends AbstractWordUtils
{
    static void compactInlines(final Element blockElement) {
        AbstractWordUtils.compactChildNodesR(blockElement, "fo:inline");
    }
    
    public static void setBold(final Element element, final boolean bold) {
        element.setAttribute("font-weight", bold ? "bold" : "normal");
    }
    
    public static void setBorder(final Element element, final BorderCode borderCode, final String where) {
        if (element == null) {
            throw new IllegalArgumentException("element is null");
        }
        if (borderCode == null || borderCode.isEmpty()) {
            return;
        }
        if (AbstractWordUtils.isEmpty(where)) {
            element.setAttribute("border-style", AbstractWordUtils.getBorderType(borderCode));
            element.setAttribute("border-color", AbstractWordUtils.getColor(borderCode.getColor()));
            element.setAttribute("border-width", AbstractWordUtils.getBorderWidth(borderCode));
        }
        else {
            element.setAttribute("border-" + where + "-style", AbstractWordUtils.getBorderType(borderCode));
            element.setAttribute("border-" + where + "-color", AbstractWordUtils.getColor(borderCode.getColor()));
            element.setAttribute("border-" + where + "-width", AbstractWordUtils.getBorderWidth(borderCode));
        }
    }
    
    public static void setCharactersProperties(final CharacterRun characterRun, final Element inline) {
        final StringBuilder textDecorations = new StringBuilder();
        setBorder(inline, characterRun.getBorder(), "");
        if (characterRun.getIco24() != -1) {
            inline.setAttribute("color", AbstractWordUtils.getColor24(characterRun.getIco24()));
        }
        if (characterRun.isCapitalized()) {
            inline.setAttribute("text-transform", "uppercase");
        }
        if (characterRun.isHighlighted()) {
            inline.setAttribute("background-color", AbstractWordUtils.getColor(characterRun.getHighlightedColor()));
        }
        if (characterRun.isStrikeThrough()) {
            if (textDecorations.length() > 0) {
                textDecorations.append(" ");
            }
            textDecorations.append("line-through");
        }
        if (characterRun.isShadowed()) {
            inline.setAttribute("text-shadow", characterRun.getFontSize() / 24 + "pt");
        }
        if (characterRun.isSmallCaps()) {
            inline.setAttribute("font-variant", "small-caps");
        }
        if (characterRun.getSubSuperScriptIndex() == 1) {
            inline.setAttribute("baseline-shift", "super");
            inline.setAttribute("font-size", "smaller");
        }
        if (characterRun.getSubSuperScriptIndex() == 2) {
            inline.setAttribute("baseline-shift", "sub");
            inline.setAttribute("font-size", "smaller");
        }
        if (characterRun.getUnderlineCode() > 0) {
            if (textDecorations.length() > 0) {
                textDecorations.append(" ");
            }
            textDecorations.append("underline");
        }
        if (characterRun.isVanished()) {
            inline.setAttribute("visibility", "hidden");
        }
        if (textDecorations.length() > 0) {
            inline.setAttribute("text-decoration", textDecorations.toString());
        }
    }
    
    public static void setFontFamily(final Element element, final String fontFamily) {
        if (AbstractWordUtils.isEmpty(fontFamily)) {
            return;
        }
        element.setAttribute("font-family", fontFamily);
    }
    
    public static void setFontSize(final Element element, final int fontSize) {
        element.setAttribute("font-size", String.valueOf(fontSize));
    }
    
    public static void setIndent(final Paragraph paragraph, final Element block) {
        if (paragraph.getFirstLineIndent() != 0) {
            block.setAttribute("text-indent", String.valueOf(paragraph.getFirstLineIndent() / 20) + "pt");
        }
        if (paragraph.getIndentFromLeft() != 0) {
            block.setAttribute("start-indent", String.valueOf(paragraph.getIndentFromLeft() / 20) + "pt");
        }
        if (paragraph.getIndentFromRight() != 0) {
            block.setAttribute("end-indent", String.valueOf(paragraph.getIndentFromRight() / 20) + "pt");
        }
        if (paragraph.getSpacingBefore() != 0) {
            block.setAttribute("space-before", String.valueOf(paragraph.getSpacingBefore() / 20) + "pt");
        }
        if (paragraph.getSpacingAfter() != 0) {
            block.setAttribute("space-after", String.valueOf(paragraph.getSpacingAfter() / 20) + "pt");
        }
    }
    
    public static void setItalic(final Element element, final boolean italic) {
        element.setAttribute("font-style", italic ? "italic" : "normal");
    }
    
    public static void setJustification(final Paragraph paragraph, final Element element) {
        final String justification = AbstractWordUtils.getJustification(paragraph.getJustification());
        if (AbstractWordUtils.isNotEmpty(justification)) {
            element.setAttribute("text-align", justification);
        }
    }
    
    public static void setLanguage(final CharacterRun characterRun, final Element inline) {
        if (characterRun.getLanguageCode() != 0) {
            final String language = AbstractWordUtils.getLanguage(characterRun.getLanguageCode());
            if (AbstractWordUtils.isNotEmpty(language)) {
                inline.setAttribute("language", language);
            }
        }
    }
    
    public static void setParagraphProperties(final Paragraph paragraph, final Element block) {
        setIndent(paragraph, block);
        setJustification(paragraph, block);
        setBorder(block, paragraph.getBottomBorder(), "bottom");
        setBorder(block, paragraph.getLeftBorder(), "left");
        setBorder(block, paragraph.getRightBorder(), "right");
        setBorder(block, paragraph.getTopBorder(), "top");
        if (paragraph.pageBreakBefore()) {
            block.setAttribute("break-before", "page");
        }
        block.setAttribute("hyphenate", String.valueOf(paragraph.isAutoHyphenated()));
        if (paragraph.keepOnPage()) {
            block.setAttribute("keep-together.within-page", "always");
        }
        if (paragraph.keepWithNext()) {
            block.setAttribute("keep-with-next.within-page", "always");
        }
        block.setAttribute("linefeed-treatment", "preserve");
        block.setAttribute("white-space-collapse", "false");
    }
    
    public static void setPictureProperties(final Picture picture, final Element graphicElement) {
        final int horizontalScale = picture.getHorizontalScalingFactor();
        final int verticalScale = picture.getVerticalScalingFactor();
        if (horizontalScale > 0) {
            graphicElement.setAttribute("content-width", picture.getDxaGoal() * horizontalScale / 1000 / 20 + "pt");
        }
        else {
            graphicElement.setAttribute("content-width", picture.getDxaGoal() / 20 + "pt");
        }
        if (verticalScale > 0) {
            graphicElement.setAttribute("content-height", picture.getDyaGoal() * verticalScale / 1000 / 20 + "pt");
        }
        else {
            graphicElement.setAttribute("content-height", picture.getDyaGoal() / 20 + "pt");
        }
        if (horizontalScale <= 0 || verticalScale <= 0) {
            graphicElement.setAttribute("scaling", "uniform");
        }
        else {
            graphicElement.setAttribute("scaling", "non-uniform");
        }
        graphicElement.setAttribute("vertical-align", "text-bottom");
        if (picture.getDyaCropTop() != 0 || picture.getDxaCropRight() != 0 || picture.getDyaCropBottom() != 0 || picture.getDxaCropLeft() != 0) {
            final int rectTop = picture.getDyaCropTop() / 20;
            final int rectRight = picture.getDxaCropRight() / 20;
            final int rectBottom = picture.getDyaCropBottom() / 20;
            final int rectLeft = picture.getDxaCropLeft() / 20;
            graphicElement.setAttribute("clip", "rect(" + rectTop + "pt, " + rectRight + "pt, " + rectBottom + "pt, " + rectLeft + "pt)");
            graphicElement.setAttribute("overflow", "hidden");
        }
    }
    
    public static void setTableCellProperties(final TableRow tableRow, final TableCell tableCell, final Element element, final boolean toppest, final boolean bottomest, final boolean leftest, final boolean rightest) {
        element.setAttribute("width", tableCell.getWidth() / 1440.0f + "in");
        element.setAttribute("padding-start", tableRow.getGapHalf() / 1440.0f + "in");
        element.setAttribute("padding-end", tableRow.getGapHalf() / 1440.0f + "in");
        final BorderCode top = (tableCell.getBrcTop() != null && tableCell.getBrcTop().getBorderType() != 0) ? tableCell.getBrcTop() : (toppest ? tableRow.getTopBorder() : tableRow.getHorizontalBorder());
        final BorderCode bottom = (tableCell.getBrcBottom() != null && tableCell.getBrcBottom().getBorderType() != 0) ? tableCell.getBrcBottom() : (bottomest ? tableRow.getBottomBorder() : tableRow.getHorizontalBorder());
        final BorderCode left = (tableCell.getBrcLeft() != null && tableCell.getBrcLeft().getBorderType() != 0) ? tableCell.getBrcLeft() : (leftest ? tableRow.getLeftBorder() : tableRow.getVerticalBorder());
        final BorderCode right = (tableCell.getBrcRight() != null && tableCell.getBrcRight().getBorderType() != 0) ? tableCell.getBrcRight() : (rightest ? tableRow.getRightBorder() : tableRow.getVerticalBorder());
        setBorder(element, bottom, "bottom");
        setBorder(element, left, "left");
        setBorder(element, right, "right");
        setBorder(element, top, "top");
    }
    
    public static void setTableRowProperties(final TableRow tableRow, final Element tableRowElement) {
        if (tableRow.getRowHeight() > 0) {
            tableRowElement.setAttribute("height", tableRow.getRowHeight() / 1440.0f + "in");
        }
        if (!tableRow.cantSplit()) {
            tableRowElement.setAttribute("keep-together.within-column", "always");
        }
    }
}
