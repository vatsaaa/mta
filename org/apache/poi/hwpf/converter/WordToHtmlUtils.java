// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hwpf.converter;

import org.w3c.dom.Element;
import org.apache.poi.hwpf.usermodel.TableCell;
import org.apache.poi.hwpf.usermodel.TableRow;
import org.apache.poi.hwpf.usermodel.Paragraph;
import org.apache.poi.hwpf.usermodel.CharacterRun;
import org.apache.poi.hwpf.usermodel.BorderCode;

public class WordToHtmlUtils extends AbstractWordUtils
{
    public static void addBold(final boolean bold, final StringBuilder style) {
        style.append("font-weight:" + (bold ? "bold" : "normal") + ";");
    }
    
    public static void addBorder(final BorderCode borderCode, final String where, final StringBuilder style) {
        if (borderCode == null || borderCode.isEmpty()) {
            return;
        }
        if (AbstractWordUtils.isEmpty(where)) {
            style.append("border:");
        }
        else {
            style.append("border-");
            style.append(where);
        }
        style.append(":");
        if (borderCode.getLineWidth() < 8) {
            style.append("thin");
        }
        else {
            style.append(AbstractWordUtils.getBorderWidth(borderCode));
        }
        style.append(' ');
        style.append(AbstractWordUtils.getBorderType(borderCode));
        style.append(' ');
        style.append(AbstractWordUtils.getColor(borderCode.getColor()));
        style.append(';');
    }
    
    public static void addCharactersProperties(final CharacterRun characterRun, final StringBuilder style) {
        addBorder(characterRun.getBorder(), "", style);
        if (characterRun.isCapitalized()) {
            style.append("text-transform:uppercase;");
        }
        if (characterRun.getIco24() != -1) {
            style.append("color:" + AbstractWordUtils.getColor24(characterRun.getIco24()) + ";");
        }
        if (characterRun.isHighlighted()) {
            style.append("background-color:" + AbstractWordUtils.getColor(characterRun.getHighlightedColor()) + ";");
        }
        if (characterRun.isStrikeThrough()) {
            style.append("text-decoration:line-through;");
        }
        if (characterRun.isShadowed()) {
            style.append("text-shadow:" + characterRun.getFontSize() / 24 + "pt;");
        }
        if (characterRun.isSmallCaps()) {
            style.append("font-variant:small-caps;");
        }
        if (characterRun.getSubSuperScriptIndex() == 1) {
            style.append("vertical-align:super;");
            style.append("font-size:smaller;");
        }
        if (characterRun.getSubSuperScriptIndex() == 2) {
            style.append("vertical-align:sub;");
            style.append("font-size:smaller;");
        }
        if (characterRun.getUnderlineCode() > 0) {
            style.append("text-decoration:underline;");
        }
        if (characterRun.isVanished()) {
            style.append("visibility:hidden;");
        }
    }
    
    public static void addFontFamily(final String fontFamily, final StringBuilder style) {
        if (AbstractWordUtils.isEmpty(fontFamily)) {
            return;
        }
        style.append("font-family:" + fontFamily + ";");
    }
    
    public static void addFontSize(final int fontSize, final StringBuilder style) {
        style.append("font-size:" + fontSize + "pt;");
    }
    
    public static void addIndent(final Paragraph paragraph, final StringBuilder style) {
        addIndent(style, "text-indent", paragraph.getFirstLineIndent());
        addIndent(style, "margin-left", paragraph.getIndentFromLeft());
        addIndent(style, "margin-right", paragraph.getIndentFromRight());
        addIndent(style, "margin-top", paragraph.getSpacingBefore());
        addIndent(style, "margin-bottom", paragraph.getSpacingAfter());
    }
    
    private static void addIndent(final StringBuilder style, final String cssName, final int twipsValue) {
        if (twipsValue == 0) {
            return;
        }
        style.append(cssName + ":" + twipsValue / 1440.0f + "in;");
    }
    
    public static void addJustification(final Paragraph paragraph, final StringBuilder style) {
        final String justification = AbstractWordUtils.getJustification(paragraph.getJustification());
        if (AbstractWordUtils.isNotEmpty(justification)) {
            style.append("text-align:" + justification + ";");
        }
    }
    
    public static void addParagraphProperties(final Paragraph paragraph, final StringBuilder style) {
        addIndent(paragraph, style);
        addJustification(paragraph, style);
        addBorder(paragraph.getBottomBorder(), "bottom", style);
        addBorder(paragraph.getLeftBorder(), "left", style);
        addBorder(paragraph.getRightBorder(), "right", style);
        addBorder(paragraph.getTopBorder(), "top", style);
        if (paragraph.pageBreakBefore()) {
            style.append("break-before:page;");
        }
        style.append("hyphenate:" + (paragraph.isAutoHyphenated() ? "auto" : "none") + ";");
        if (paragraph.keepOnPage()) {
            style.append("keep-together.within-page:always;");
        }
        if (paragraph.keepWithNext()) {
            style.append("keep-with-next.within-page:always;");
        }
    }
    
    public static void addTableCellProperties(final TableRow tableRow, final TableCell tableCell, final boolean toppest, final boolean bottomest, final boolean leftest, final boolean rightest, final StringBuilder style) {
        style.append("width:" + tableCell.getWidth() / 1440.0f + "in;");
        style.append("padding-start:" + tableRow.getGapHalf() / 1440.0f + "in;");
        style.append("padding-end:" + tableRow.getGapHalf() / 1440.0f + "in;");
        final BorderCode top = (tableCell.getBrcTop() != null && tableCell.getBrcTop().getBorderType() != 0) ? tableCell.getBrcTop() : (toppest ? tableRow.getTopBorder() : tableRow.getHorizontalBorder());
        final BorderCode bottom = (tableCell.getBrcBottom() != null && tableCell.getBrcBottom().getBorderType() != 0) ? tableCell.getBrcBottom() : (bottomest ? tableRow.getBottomBorder() : tableRow.getHorizontalBorder());
        final BorderCode left = (tableCell.getBrcLeft() != null && tableCell.getBrcLeft().getBorderType() != 0) ? tableCell.getBrcLeft() : (leftest ? tableRow.getLeftBorder() : tableRow.getVerticalBorder());
        final BorderCode right = (tableCell.getBrcRight() != null && tableCell.getBrcRight().getBorderType() != 0) ? tableCell.getBrcRight() : (rightest ? tableRow.getRightBorder() : tableRow.getVerticalBorder());
        addBorder(bottom, "bottom", style);
        addBorder(left, "left", style);
        addBorder(right, "right", style);
        addBorder(top, "top", style);
    }
    
    public static void addTableRowProperties(final TableRow tableRow, final StringBuilder style) {
        if (tableRow.getRowHeight() > 0) {
            style.append("height:" + tableRow.getRowHeight() / 1440.0f + "in;");
        }
        if (!tableRow.cantSplit()) {
            style.append("keep-together:always;");
        }
    }
    
    static void compactSpans(final Element pElement) {
        AbstractWordUtils.compactChildNodesR(pElement, "span");
    }
}
