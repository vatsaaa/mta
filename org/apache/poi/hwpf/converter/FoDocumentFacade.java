// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hwpf.converter;

import org.w3c.dom.NodeList;
import org.w3c.dom.Text;
import org.w3c.dom.Node;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class FoDocumentFacade
{
    private static final String NS_RDF = "http://www.w3.org/1999/02/22-rdf-syntax-ns#";
    private static final String NS_XSLFO = "http://www.w3.org/1999/XSL/Format";
    protected final Element declarations;
    protected final Document document;
    protected final Element layoutMasterSet;
    protected Element propertiesRoot;
    protected final Element root;
    
    public FoDocumentFacade(final Document document) {
        (this.document = document).appendChild(this.root = document.createElementNS("http://www.w3.org/1999/XSL/Format", "fo:root"));
        this.layoutMasterSet = document.createElementNS("http://www.w3.org/1999/XSL/Format", "fo:layout-master-set");
        this.root.appendChild(this.layoutMasterSet);
        this.declarations = document.createElementNS("http://www.w3.org/1999/XSL/Format", "fo:declarations");
        this.root.appendChild(this.declarations);
    }
    
    public Element addFlowToPageSequence(final Element pageSequence, final String flowName) {
        final Element flow = this.document.createElementNS("http://www.w3.org/1999/XSL/Format", "fo:flow");
        flow.setAttribute("flow-name", flowName);
        pageSequence.appendChild(flow);
        return flow;
    }
    
    public Element addListItem(final Element listBlock) {
        final Element result = this.createListItem();
        listBlock.appendChild(result);
        return result;
    }
    
    public Element addListItemBody(final Element listItem) {
        final Element result = this.createListItemBody();
        listItem.appendChild(result);
        return result;
    }
    
    public Element addListItemLabel(final Element listItem, final String text) {
        final Element result = this.createListItemLabel(text);
        listItem.appendChild(result);
        return result;
    }
    
    public void addPageSequence(final Element pageSequence) {
        this.root.appendChild(pageSequence);
    }
    
    public Element addPageSequence(final String pageMaster) {
        final Element pageSequence = this.createPageSequence(pageMaster);
        this.root.appendChild(pageSequence);
        return pageSequence;
    }
    
    public Element addRegionBody(final Element pageMaster) {
        final Element regionBody = this.document.createElementNS("http://www.w3.org/1999/XSL/Format", "fo:region-body");
        pageMaster.appendChild(regionBody);
        return regionBody;
    }
    
    public Element addSimplePageMaster(final String masterName) {
        final Element simplePageMaster = this.document.createElementNS("http://www.w3.org/1999/XSL/Format", "fo:simple-page-master");
        simplePageMaster.setAttribute("master-name", masterName);
        this.layoutMasterSet.appendChild(simplePageMaster);
        return simplePageMaster;
    }
    
    public Element createBasicLinkExternal(final String externalDestination) {
        final Element basicLink = this.document.createElementNS("http://www.w3.org/1999/XSL/Format", "fo:basic-link");
        basicLink.setAttribute("external-destination", externalDestination);
        return basicLink;
    }
    
    public Element createBasicLinkInternal(final String internalDestination) {
        final Element basicLink = this.document.createElementNS("http://www.w3.org/1999/XSL/Format", "fo:basic-link");
        basicLink.setAttribute("internal-destination", internalDestination);
        return basicLink;
    }
    
    public Element createBlock() {
        return this.document.createElementNS("http://www.w3.org/1999/XSL/Format", "fo:block");
    }
    
    public Element createExternalGraphic(final String source) {
        final Element result = this.document.createElementNS("http://www.w3.org/1999/XSL/Format", "fo:external-graphic");
        result.setAttribute("src", "url('" + source + "')");
        return result;
    }
    
    public Element createFootnote() {
        return this.document.createElementNS("http://www.w3.org/1999/XSL/Format", "fo:footnote");
    }
    
    public Element createFootnoteBody() {
        return this.document.createElementNS("http://www.w3.org/1999/XSL/Format", "fo:footnote-body");
    }
    
    public Element createInline() {
        return this.document.createElementNS("http://www.w3.org/1999/XSL/Format", "fo:inline");
    }
    
    public Element createLeader() {
        return this.document.createElementNS("http://www.w3.org/1999/XSL/Format", "fo:leader");
    }
    
    public Element createListBlock() {
        return this.document.createElementNS("http://www.w3.org/1999/XSL/Format", "fo:list-block");
    }
    
    public Element createListItem() {
        return this.document.createElementNS("http://www.w3.org/1999/XSL/Format", "fo:list-item");
    }
    
    public Element createListItemBody() {
        return this.document.createElementNS("http://www.w3.org/1999/XSL/Format", "fo:list-item-body");
    }
    
    public Element createListItemLabel(final String text) {
        final Element result = this.document.createElementNS("http://www.w3.org/1999/XSL/Format", "fo:list-item-label");
        final Element block = this.createBlock();
        block.appendChild(this.document.createTextNode(text));
        result.appendChild(block);
        return result;
    }
    
    public Element createPageSequence(final String pageMaster) {
        final Element pageSequence = this.document.createElementNS("http://www.w3.org/1999/XSL/Format", "fo:page-sequence");
        pageSequence.setAttribute("master-reference", pageMaster);
        return pageSequence;
    }
    
    public Element createTable() {
        return this.document.createElementNS("http://www.w3.org/1999/XSL/Format", "fo:table");
    }
    
    public Element createTableBody() {
        return this.document.createElementNS("http://www.w3.org/1999/XSL/Format", "fo:table-body");
    }
    
    public Element createTableCell() {
        return this.document.createElementNS("http://www.w3.org/1999/XSL/Format", "fo:table-cell");
    }
    
    public Element createTableColumn() {
        return this.document.createElementNS("http://www.w3.org/1999/XSL/Format", "fo:table-column");
    }
    
    public Element createTableHeader() {
        return this.document.createElementNS("http://www.w3.org/1999/XSL/Format", "fo:table-header");
    }
    
    public Element createTableRow() {
        return this.document.createElementNS("http://www.w3.org/1999/XSL/Format", "fo:table-row");
    }
    
    public Text createText(final String data) {
        return this.document.createTextNode(data);
    }
    
    public Document getDocument() {
        return this.document;
    }
    
    protected Element getOrCreatePropertiesRoot() {
        if (this.propertiesRoot != null) {
            return this.propertiesRoot;
        }
        final Element xmpmeta = this.document.createElementNS("adobe:ns:meta/", "x:xmpmeta");
        this.declarations.appendChild(xmpmeta);
        final Element rdf = this.document.createElementNS("http://www.w3.org/1999/02/22-rdf-syntax-ns#", "rdf:RDF");
        xmpmeta.appendChild(rdf);
        (this.propertiesRoot = this.document.createElementNS("http://www.w3.org/1999/02/22-rdf-syntax-ns#", "rdf:Description")).setAttributeNS("http://www.w3.org/1999/02/22-rdf-syntax-ns#", "rdf:about", "");
        rdf.appendChild(this.propertiesRoot);
        return this.propertiesRoot;
    }
    
    public void setCreator(final String value) {
        this.setDublinCoreProperty("creator", value);
    }
    
    public void setCreatorTool(final String value) {
        this.setXmpProperty("CreatorTool", value);
    }
    
    public void setDescription(final String value) {
        final Element element = this.setDublinCoreProperty("description", value);
        if (element != null) {
            element.setAttributeNS("http://www.w3.org/XML/1998/namespace", "xml:lang", "x-default");
        }
    }
    
    public Element setDublinCoreProperty(final String name, final String value) {
        return this.setProperty("http://purl.org/dc/elements/1.1/", "dc", name, value);
    }
    
    public void setKeywords(final String value) {
        this.setPdfProperty("Keywords", value);
    }
    
    public Element setPdfProperty(final String name, final String value) {
        return this.setProperty("http://ns.adobe.com/pdf/1.3/", "pdf", name, value);
    }
    
    public void setProducer(final String value) {
        this.setPdfProperty("Producer", value);
    }
    
    protected Element setProperty(final String namespace, final String prefix, final String name, final String value) {
        final Element propertiesRoot = this.getOrCreatePropertiesRoot();
        final NodeList existingChildren = propertiesRoot.getChildNodes();
        for (int i = 0; i < existingChildren.getLength(); ++i) {
            final Node child = existingChildren.item(i);
            if (child.getNodeType() == 1) {
                final Element childElement = (Element)child;
                if (AbstractWordUtils.isNotEmpty(childElement.getNamespaceURI()) && AbstractWordUtils.isNotEmpty(childElement.getLocalName()) && namespace.equals(childElement.getNamespaceURI()) && name.equals(childElement.getLocalName())) {
                    propertiesRoot.removeChild(childElement);
                    break;
                }
            }
        }
        if (AbstractWordUtils.isNotEmpty(value)) {
            final Element property = this.document.createElementNS(namespace, prefix + ":" + name);
            property.appendChild(this.document.createTextNode(value));
            propertiesRoot.appendChild(property);
            return property;
        }
        return null;
    }
    
    public void setSubject(final String value) {
        this.setDublinCoreProperty("title", value);
    }
    
    public void setTitle(final String value) {
        this.setDublinCoreProperty("title", value);
    }
    
    public Element setXmpProperty(final String name, final String value) {
        return this.setProperty("http://ns.adobe.com/xap/1.0/", "xmp", name, value);
    }
}
