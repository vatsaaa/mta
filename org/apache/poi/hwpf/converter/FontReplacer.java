// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hwpf.converter;

public interface FontReplacer
{
    Triplet update(final Triplet p0);
    
    public static class Triplet
    {
        public String fontName;
        public boolean bold;
        public boolean italic;
    }
}
