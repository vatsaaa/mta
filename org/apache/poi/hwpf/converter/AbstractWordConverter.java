// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hwpf.converter;

import org.apache.poi.util.POILogFactory;
import org.apache.poi.hwpf.usermodel.Section;
import org.apache.poi.hwpf.model.ListFormatOverride;
import org.apache.poi.hwpf.model.ListTables;
import org.apache.poi.hwpf.usermodel.Paragraph;
import org.apache.poi.poifs.filesystem.Entry;
import org.apache.poi.hwpf.usermodel.Notes;
import org.apache.poi.util.Internal;
import org.apache.poi.hwpf.usermodel.OfficeDrawing;
import org.apache.poi.hwpf.usermodel.PictureType;
import org.apache.poi.hpsf.SummaryInformation;
import java.util.regex.Matcher;
import org.apache.poi.hwpf.usermodel.Picture;
import java.util.Map;
import org.apache.poi.hwpf.usermodel.Field;
import java.util.Collections;
import java.util.Collection;
import java.util.ArrayList;
import org.apache.poi.hwpf.model.FieldsDocumentPart;
import org.apache.poi.hwpf.HWPFDocument;
import java.util.LinkedList;
import org.apache.poi.hwpf.usermodel.Range;
import org.apache.poi.hwpf.HWPFDocumentCore;
import org.w3c.dom.Element;
import org.apache.poi.hwpf.usermodel.TableRow;
import org.apache.poi.hwpf.usermodel.Table;
import org.apache.poi.hwpf.usermodel.TableCell;
import org.w3c.dom.Document;
import org.apache.poi.hwpf.usermodel.CharacterRun;
import java.util.Iterator;
import java.util.List;
import java.util.LinkedHashSet;
import org.apache.poi.hwpf.usermodel.Bookmark;
import java.util.Set;
import java.util.regex.Pattern;
import org.apache.poi.util.POILogger;

public abstract class AbstractWordConverter
{
    private static final byte BEL_MARK = 7;
    private static final byte FIELD_BEGIN_MARK = 19;
    private static final byte FIELD_END_MARK = 21;
    private static final byte FIELD_SEPARATOR_MARK = 20;
    private static final POILogger logger;
    private static final Pattern PATTERN_HYPERLINK_EXTERNAL;
    private static final Pattern PATTERN_HYPERLINK_LOCAL;
    private static final Pattern PATTERN_PAGEREF;
    private static final byte SPECCHAR_AUTONUMBERED_FOOTNOTE_REFERENCE = 2;
    private static final byte SPECCHAR_DRAWN_OBJECT = 8;
    protected static final char UNICODECHAR_NO_BREAK_SPACE = ' ';
    protected static final char UNICODECHAR_NONBREAKING_HYPHEN = '\u2011';
    protected static final char UNICODECHAR_ZERO_WIDTH_SPACE = '\u200b';
    private final Set<Bookmark> bookmarkStack;
    private FontReplacer fontReplacer;
    private PicturesManager picturesManager;
    
    public AbstractWordConverter() {
        this.bookmarkStack = new LinkedHashSet<Bookmark>();
        this.fontReplacer = new DefaultFontReplacer();
    }
    
    private static void addToStructures(final List<Structure> structures, final Structure structure) {
        final Iterator<Structure> iterator = structures.iterator();
        while (iterator.hasNext()) {
            final Structure another = iterator.next();
            if (another.start <= structure.start && another.end >= structure.start) {
                return;
            }
            if ((structure.start >= another.start || another.start >= structure.end) && (structure.start >= another.start || another.end > structure.end) && (structure.start > another.start || another.end >= structure.end)) {
                continue;
            }
            iterator.remove();
        }
        structures.add(structure);
    }
    
    protected void afterProcess() {
    }
    
    protected FontReplacer.Triplet getCharacterRunTriplet(final CharacterRun characterRun) {
        final FontReplacer.Triplet original = new FontReplacer.Triplet();
        original.bold = characterRun.isBold();
        original.italic = characterRun.isItalic();
        original.fontName = characterRun.getFontName();
        final FontReplacer.Triplet updated = this.getFontReplacer().update(original);
        return updated;
    }
    
    public abstract Document getDocument();
    
    public FontReplacer getFontReplacer() {
        return this.fontReplacer;
    }
    
    protected int getNumberColumnsSpanned(final int[] tableCellEdges, final int currentEdgeIndex, final TableCell tableCell) {
        int nextEdgeIndex = currentEdgeIndex;
        int colSpan = 0;
        for (int cellRightEdge = tableCell.getLeftEdge() + tableCell.getWidth(); tableCellEdges[nextEdgeIndex] < cellRightEdge; ++nextEdgeIndex) {
            ++colSpan;
        }
        return colSpan;
    }
    
    protected int getNumberRowsSpanned(final Table table, final int[] tableCellEdges, final int currentRowIndex, final int currentColumnIndex, final TableCell tableCell) {
        if (!tableCell.isFirstVerticallyMerged()) {
            return 1;
        }
        final int numRows = table.numRows();
        int count = 1;
        for (int r1 = currentRowIndex + 1; r1 < numRows; ++r1) {
            final TableRow nextRow = table.getRow(r1);
            if (currentColumnIndex >= nextRow.numCells()) {
                break;
            }
            boolean hasCells = false;
            int currentEdgeIndex = 0;
            for (int c = 0; c < nextRow.numCells(); ++c) {
                final TableCell nextTableCell = nextRow.getCell(c);
                if (!nextTableCell.isVerticallyMerged() || nextTableCell.isFirstVerticallyMerged()) {
                    final int colSpan = this.getNumberColumnsSpanned(tableCellEdges, currentEdgeIndex, nextTableCell);
                    currentEdgeIndex += colSpan;
                    if (colSpan != 0) {
                        hasCells = true;
                        break;
                    }
                }
                else {
                    currentEdgeIndex += this.getNumberColumnsSpanned(tableCellEdges, currentEdgeIndex, nextTableCell);
                }
            }
            if (hasCells) {
                final TableCell nextCell = nextRow.getCell(currentColumnIndex);
                if (!nextCell.isVerticallyMerged()) {
                    break;
                }
                if (nextCell.isFirstVerticallyMerged()) {
                    break;
                }
                ++count;
            }
        }
        return count;
    }
    
    public PicturesManager getPicturesManager() {
        return this.picturesManager;
    }
    
    protected abstract void outputCharacters(final Element p0, final CharacterRun p1, final String p2);
    
    protected abstract void processBookmarks(final HWPFDocumentCore p0, final Element p1, final Range p2, final int p3, final List<Bookmark> p4);
    
    protected boolean processCharacters(final HWPFDocumentCore wordDocument, final int currentTableLevel, final Range range, final Element block) {
        if (range == null) {
            return false;
        }
        boolean haveAnyText = false;
        List<Structure> structures = new LinkedList<Structure>();
        if (wordDocument instanceof HWPFDocument) {
            final HWPFDocument doc = (HWPFDocument)wordDocument;
            final Map<Integer, List<Bookmark>> rangeBookmarks = doc.getBookmarks().getBookmarksStartedBetween(range.getStartOffset(), range.getEndOffset());
            if (rangeBookmarks != null) {
                for (final List<Bookmark> lists : rangeBookmarks.values()) {
                    for (final Bookmark bookmark : lists) {
                        if (!this.bookmarkStack.contains(bookmark)) {
                            addToStructures(structures, new Structure(bookmark));
                        }
                    }
                }
            }
            final int skipUntil = -1;
            for (int c = 0; c < range.numCharacterRuns(); ++c) {
                final CharacterRun characterRun = range.getCharacterRun(c);
                if (characterRun == null) {
                    throw new AssertionError();
                }
                if (characterRun.getStartOffset() >= skipUntil) {
                    final String text = characterRun.text();
                    if (text != null && text.length() != 0) {
                        if (text.charAt(0) == '\u0013') {
                            final Field aliveField = ((HWPFDocument)wordDocument).getFields().getFieldByStartOffset(FieldsDocumentPart.MAIN, characterRun.getStartOffset());
                            if (aliveField != null) {
                                addToStructures(structures, new Structure(aliveField));
                            }
                            else {
                                final int[] separatorEnd = this.tryDeadField_lookupFieldSeparatorEnd(wordDocument, range, c);
                                if (separatorEnd != null) {
                                    addToStructures(structures, new Structure(new DeadFieldBoundaries(c, separatorEnd[0], separatorEnd[1]), characterRun.getStartOffset(), range.getCharacterRun(separatorEnd[1]).getEndOffset()));
                                    c = separatorEnd[1];
                                }
                            }
                        }
                    }
                }
            }
        }
        structures = new ArrayList<Structure>(structures);
        Collections.sort(structures);
        int previous = range.getStartOffset();
        for (final Structure structure : structures) {
            if (structure.start != previous) {
                final Range subrange = new Range(previous, structure.start, range) {
                    @Override
                    public String toString() {
                        return "BetweenStructuresSubrange " + super.toString();
                    }
                };
                this.processCharacters(wordDocument, currentTableLevel, subrange, block);
            }
            if (structure.structure instanceof Bookmark) {
                final List<Bookmark> bookmarks = new LinkedList<Bookmark>();
                for (final Bookmark bookmark : ((HWPFDocument)wordDocument).getBookmarks().getBookmarksStartedBetween(structure.start, structure.start + 1).values().iterator().next()) {
                    if (bookmark.getStart() == structure.start && bookmark.getEnd() == structure.end) {
                        bookmarks.add(bookmark);
                    }
                }
                this.bookmarkStack.addAll(bookmarks);
                try {
                    final int end = Math.min(range.getEndOffset(), structure.end);
                    final Range subrange2 = new Range(structure.start, end, range) {
                        @Override
                        public String toString() {
                            return "BookmarksSubrange " + super.toString();
                        }
                    };
                    this.processBookmarks(wordDocument, block, subrange2, currentTableLevel, bookmarks);
                }
                finally {
                    this.bookmarkStack.removeAll(bookmarks);
                }
            }
            else if (structure.structure instanceof Field) {
                final Field field = (Field)structure.structure;
                this.processField((HWPFDocument)wordDocument, range, currentTableLevel, field, block);
            }
            else {
                if (!(structure.structure instanceof DeadFieldBoundaries)) {
                    throw new UnsupportedOperationException("NYI: " + structure.structure.getClass());
                }
                final DeadFieldBoundaries boundaries = (DeadFieldBoundaries)structure.structure;
                this.processDeadField(wordDocument, block, range, currentTableLevel, boundaries.beginMark, boundaries.separatorMark, boundaries.endMark);
            }
            previous = Math.min(range.getEndOffset(), structure.end);
        }
        if (previous == range.getStartOffset()) {
            for (int c2 = 0; c2 < range.numCharacterRuns(); ++c2) {
                final CharacterRun characterRun2 = range.getCharacterRun(c2);
                if (characterRun2 == null) {
                    throw new AssertionError();
                }
                if (wordDocument instanceof HWPFDocument && ((HWPFDocument)wordDocument).getPicturesTable().hasPicture(characterRun2)) {
                    final HWPFDocument newFormat = (HWPFDocument)wordDocument;
                    final Picture picture = newFormat.getPicturesTable().extractPicture(characterRun2, true);
                    this.processImage(block, characterRun2.text().charAt(0) == '\u0001', picture);
                }
                else {
                    String text2 = characterRun2.text();
                    if (text2.getBytes().length != 0) {
                        if (characterRun2.isSpecialCharacter()) {
                            if (text2.charAt(0) == '\u0002' && wordDocument instanceof HWPFDocument) {
                                final HWPFDocument doc2 = (HWPFDocument)wordDocument;
                                this.processNoteAnchor(doc2, characterRun2, block);
                                continue;
                            }
                            if (text2.charAt(0) == '\b' && wordDocument instanceof HWPFDocument) {
                                final HWPFDocument doc2 = (HWPFDocument)wordDocument;
                                this.processDrawnObject(doc2, characterRun2, block);
                                continue;
                            }
                            if (characterRun2.isOle2() && wordDocument instanceof HWPFDocument) {
                                final HWPFDocument doc2 = (HWPFDocument)wordDocument;
                                this.processOle2(doc2, characterRun2, block);
                                continue;
                            }
                        }
                        if (text2.getBytes()[0] == 19) {
                            if (wordDocument instanceof HWPFDocument) {
                                final Field aliveField2 = ((HWPFDocument)wordDocument).getFields().getFieldByStartOffset(FieldsDocumentPart.MAIN, characterRun2.getStartOffset());
                                if (aliveField2 != null) {
                                    this.processField((HWPFDocument)wordDocument, range, currentTableLevel, aliveField2, block);
                                    for (int continueAfter = aliveField2.getFieldEndOffset(); c2 < range.numCharacterRuns() && range.getCharacterRun(c2).getEndOffset() <= continueAfter; ++c2) {}
                                    if (c2 < range.numCharacterRuns()) {
                                        --c2;
                                    }
                                    continue;
                                }
                            }
                            final int skipTo = this.tryDeadField(wordDocument, range, currentTableLevel, c2, block);
                            if (skipTo != c2) {
                                c2 = skipTo;
                            }
                        }
                        else if (text2.getBytes()[0] != 20) {
                            if (text2.getBytes()[0] != 21) {
                                if (!characterRun2.isSpecialCharacter() && !characterRun2.isObj()) {
                                    if (!characterRun2.isOle2()) {
                                        if (text2.endsWith("\r") || (text2.charAt(text2.length() - 1) == '\u0007' && currentTableLevel != Integer.MIN_VALUE)) {
                                            text2 = text2.substring(0, text2.length() - 1);
                                        }
                                        final StringBuilder stringBuilder = new StringBuilder();
                                        for (final char charChar : text2.toCharArray()) {
                                            if (charChar == '\u000b') {
                                                if (stringBuilder.length() > 0) {
                                                    this.outputCharacters(block, characterRun2, stringBuilder.toString());
                                                    stringBuilder.setLength(0);
                                                }
                                                this.processLineBreak(block, characterRun2);
                                            }
                                            else if (charChar == '\u001e') {
                                                stringBuilder.append('\u2011');
                                            }
                                            else if (charChar == '\u001f') {
                                                stringBuilder.append('\u200b');
                                            }
                                            else if (charChar >= ' ' || charChar == '\t' || charChar == '\n' || charChar == '\r') {
                                                stringBuilder.append(charChar);
                                            }
                                        }
                                        if (stringBuilder.length() > 0) {
                                            this.outputCharacters(block, characterRun2, stringBuilder.toString());
                                            stringBuilder.setLength(0);
                                        }
                                        haveAnyText |= (text2.trim().length() != 0);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return haveAnyText;
        }
        if (previous > range.getEndOffset()) {
            AbstractWordConverter.logger.log(POILogger.WARN, "Latest structure in ", range, " ended at #" + previous, " after range boundaries [", range.getStartOffset() + "; " + range.getEndOffset(), ")");
            return true;
        }
        if (previous < range.getEndOffset()) {
            final Range subrange3 = new Range(previous, range.getEndOffset(), range) {
                @Override
                public String toString() {
                    return "AfterStructureSubrange " + super.toString();
                }
            };
            this.processCharacters(wordDocument, currentTableLevel, subrange3, block);
        }
        return true;
    }
    
    protected void processDeadField(final HWPFDocumentCore wordDocument, final Element currentBlock, final Range range, final int currentTableLevel, final int beginMark, final int separatorMark, final int endMark) {
        if (beginMark + 1 < separatorMark && separatorMark + 1 < endMark) {
            final Range formulaRange = new Range(range.getCharacterRun(beginMark + 1).getStartOffset(), range.getCharacterRun(separatorMark - 1).getEndOffset(), range) {
                @Override
                public String toString() {
                    return "Dead field formula subrange: " + super.toString();
                }
            };
            final Range valueRange = new Range(range.getCharacterRun(separatorMark + 1).getStartOffset(), range.getCharacterRun(endMark - 1).getEndOffset(), range) {
                @Override
                public String toString() {
                    return "Dead field value subrange: " + super.toString();
                }
            };
            final String formula = formulaRange.text();
            final Matcher matcher = AbstractWordConverter.PATTERN_HYPERLINK_LOCAL.matcher(formula);
            if (matcher.matches()) {
                final String localref = matcher.group(1);
                this.processPageref(wordDocument, currentBlock, valueRange, currentTableLevel, localref);
                return;
            }
        }
        final StringBuilder debug = new StringBuilder("Unsupported field type: \n");
        for (int i = beginMark; i <= endMark; ++i) {
            debug.append("\t");
            debug.append(range.getCharacterRun(i));
            debug.append("\n");
        }
        AbstractWordConverter.logger.log(POILogger.WARN, debug);
        final Range deadFieldValueSubrage = new Range(range.getCharacterRun(separatorMark).getStartOffset() + 1, range.getCharacterRun(endMark).getStartOffset(), range) {
            @Override
            public String toString() {
                return "DeadFieldValueSubrange (" + super.toString() + ")";
            }
        };
        if (separatorMark + 1 < endMark) {
            this.processCharacters(wordDocument, currentTableLevel, deadFieldValueSubrage, currentBlock);
        }
    }
    
    public void processDocument(final HWPFDocumentCore wordDocument) {
        try {
            final SummaryInformation summaryInformation = wordDocument.getSummaryInformation();
            if (summaryInformation != null) {
                this.processDocumentInformation(summaryInformation);
            }
        }
        catch (Exception exc) {
            AbstractWordConverter.logger.log(POILogger.WARN, "Unable to process document summary information: ", exc, exc);
        }
        final Range docRange = wordDocument.getRange();
        if (docRange.numSections() == 1) {
            this.processSingleSection(wordDocument, docRange.getSection(0));
            this.afterProcess();
            return;
        }
        this.processDocumentPart(wordDocument, docRange);
        this.afterProcess();
    }
    
    protected abstract void processDocumentInformation(final SummaryInformation p0);
    
    protected void processDocumentPart(final HWPFDocumentCore wordDocument, final Range range) {
        for (int s = 0; s < range.numSections(); ++s) {
            this.processSection(wordDocument, range.getSection(s), s);
        }
    }
    
    protected void processDrawnObject(final HWPFDocument doc, final CharacterRun characterRun, final Element block) {
        if (this.getPicturesManager() == null) {
            return;
        }
        final OfficeDrawing officeDrawing = doc.getOfficeDrawingsMain().getOfficeDrawingAt(characterRun.getStartOffset());
        if (officeDrawing == null) {
            AbstractWordConverter.logger.log(POILogger.WARN, "Characters #" + characterRun + " references missing drawn object");
            return;
        }
        final byte[] pictureData = officeDrawing.getPictureData();
        if (pictureData == null) {
            return;
        }
        final float width = (officeDrawing.getRectangleRight() - officeDrawing.getRectangleLeft()) / 1440.0f;
        final float height = (officeDrawing.getRectangleBottom() - officeDrawing.getRectangleTop()) / 1440.0f;
        final PictureType type = PictureType.findMatchingType(pictureData);
        final String path = this.getPicturesManager().savePicture(pictureData, type, "s" + characterRun.getStartOffset() + "." + type, width, height);
        this.processDrawnObject(doc, characterRun, officeDrawing, path, block);
    }
    
    protected abstract void processDrawnObject(final HWPFDocument p0, final CharacterRun p1, final OfficeDrawing p2, final String p3, final Element p4);
    
    protected abstract void processEndnoteAutonumbered(final HWPFDocument p0, final int p1, final Element p2, final Range p3);
    
    protected void processField(final HWPFDocument wordDocument, final Range parentRange, final int currentTableLevel, final Field field, final Element currentBlock) {
        switch (field.getType()) {
            case 37: {
                final Range firstSubrange = field.firstSubrange(parentRange);
                if (firstSubrange == null) {
                    break;
                }
                final String formula = firstSubrange.text();
                final Matcher matcher = AbstractWordConverter.PATTERN_PAGEREF.matcher(formula);
                if (matcher.find()) {
                    final String pageref = matcher.group(1);
                    this.processPageref(wordDocument, currentBlock, field.secondSubrange(parentRange), currentTableLevel, pageref);
                    return;
                }
                break;
            }
            case 58: {
                if (!field.hasSeparator()) {
                    AbstractWordConverter.logger.log(POILogger.WARN, parentRange + " contains " + field + " with 'Embedded Object' but without separator mark");
                    return;
                }
                final CharacterRun separator = field.getMarkSeparatorCharacterRun(parentRange);
                if (separator.isOle2()) {
                    final boolean processed = this.processOle2(wordDocument, separator, currentBlock);
                    if (!processed) {
                        this.processCharacters(wordDocument, currentTableLevel, field.secondSubrange(parentRange), currentBlock);
                    }
                    return;
                }
                break;
            }
            case 88: {
                final Range firstSubrange = field.firstSubrange(parentRange);
                if (firstSubrange == null) {
                    break;
                }
                final String formula = firstSubrange.text();
                final Matcher matcher = AbstractWordConverter.PATTERN_HYPERLINK_EXTERNAL.matcher(formula);
                if (matcher.matches()) {
                    final String hyperlink = matcher.group(1);
                    this.processHyperlink(wordDocument, currentBlock, field.secondSubrange(parentRange), currentTableLevel, hyperlink);
                    return;
                }
                matcher.usePattern(AbstractWordConverter.PATTERN_HYPERLINK_LOCAL);
                if (matcher.matches()) {
                    final String hyperlink = matcher.group(1);
                    Range textRange = null;
                    final String text = matcher.group(2);
                    if (AbstractWordUtils.isNotEmpty(text)) {
                        textRange = new Range(firstSubrange.getStartOffset() + matcher.start(2), firstSubrange.getStartOffset() + matcher.end(2), firstSubrange) {
                            @Override
                            public String toString() {
                                return "Local hyperlink text";
                            }
                        };
                    }
                    this.processPageref(wordDocument, currentBlock, textRange, currentTableLevel, hyperlink);
                    return;
                }
                break;
            }
        }
        AbstractWordConverter.logger.log(POILogger.WARN, parentRange + " contains " + field + " with unsupported type or format");
        this.processCharacters(wordDocument, currentTableLevel, field.secondSubrange(parentRange), currentBlock);
    }
    
    protected abstract void processFootnoteAutonumbered(final HWPFDocument p0, final int p1, final Element p2, final Range p3);
    
    protected abstract void processHyperlink(final HWPFDocumentCore p0, final Element p1, final Range p2, final int p3, final String p4);
    
    protected void processImage(final Element currentBlock, final boolean inlined, final Picture picture) {
        final PicturesManager fileManager = this.getPicturesManager();
        if (fileManager != null) {
            final int aspectRatioX = picture.getHorizontalScalingFactor();
            final int aspectRatioY = picture.getVerticalScalingFactor();
            final float imageWidth = (aspectRatioX > 0) ? (picture.getDxaGoal() * aspectRatioX / 1000 / 1440.0f) : (picture.getDxaGoal() / 1440.0f);
            final float imageHeight = (aspectRatioY > 0) ? (picture.getDyaGoal() * aspectRatioY / 1000 / 1440.0f) : (picture.getDyaGoal() / 1440.0f);
            final String url = fileManager.savePicture(picture.getContent(), picture.suggestPictureType(), picture.suggestFullFileName(), imageWidth, imageHeight);
            if (AbstractWordUtils.isNotEmpty(url)) {
                this.processImage(currentBlock, inlined, picture, url);
                return;
            }
        }
        this.processImageWithoutPicturesManager(currentBlock, inlined, picture);
    }
    
    protected abstract void processImage(final Element p0, final boolean p1, final Picture p2, final String p3);
    
    @Internal
    protected abstract void processImageWithoutPicturesManager(final Element p0, final boolean p1, final Picture p2);
    
    protected abstract void processLineBreak(final Element p0, final CharacterRun p1);
    
    protected void processNoteAnchor(final HWPFDocument doc, final CharacterRun characterRun, final Element block) {
        final Notes footnotes = doc.getFootnotes();
        int noteIndex = footnotes.getNoteIndexByAnchorPosition(characterRun.getStartOffset());
        if (noteIndex != -1) {
            final Range footnoteRange = doc.getFootnoteRange();
            final int rangeStartOffset = footnoteRange.getStartOffset();
            final int noteTextStartOffset = footnotes.getNoteTextStartOffset(noteIndex);
            final int noteTextEndOffset = footnotes.getNoteTextEndOffset(noteIndex);
            final Range noteTextRange = new Range(rangeStartOffset + noteTextStartOffset, rangeStartOffset + noteTextEndOffset, doc);
            this.processFootnoteAutonumbered(doc, noteIndex, block, noteTextRange);
            return;
        }
        final Notes endnotes = doc.getEndnotes();
        noteIndex = endnotes.getNoteIndexByAnchorPosition(characterRun.getStartOffset());
        if (noteIndex != -1) {
            final Range endnoteRange = doc.getEndnoteRange();
            final int rangeStartOffset = endnoteRange.getStartOffset();
            final int noteTextStartOffset = endnotes.getNoteTextStartOffset(noteIndex);
            final int noteTextEndOffset = endnotes.getNoteTextEndOffset(noteIndex);
            final Range noteTextRange = new Range(rangeStartOffset + noteTextStartOffset, rangeStartOffset + noteTextEndOffset, doc);
            this.processEndnoteAutonumbered(doc, noteIndex, block, noteTextRange);
        }
    }
    
    private boolean processOle2(final HWPFDocument doc, final CharacterRun characterRun, final Element block) {
        final Entry entry = doc.getObjectsPool().getObjectById("_" + characterRun.getPicOffset());
        if (entry == null) {
            AbstractWordConverter.logger.log(POILogger.WARN, "Referenced OLE2 object '", characterRun.getPicOffset(), "' not found in ObjectPool");
            return false;
        }
        try {
            return this.processOle2(doc, block, entry);
        }
        catch (Exception exc) {
            AbstractWordConverter.logger.log(POILogger.WARN, "Unable to convert internal OLE2 object '", characterRun.getPicOffset(), "': ", exc, exc);
            return false;
        }
    }
    
    protected boolean processOle2(final HWPFDocument wordDocument, final Element block, final Entry entry) throws Exception {
        return false;
    }
    
    protected abstract void processPageBreak(final HWPFDocumentCore p0, final Element p1);
    
    protected abstract void processPageref(final HWPFDocumentCore p0, final Element p1, final Range p2, final int p3, final String p4);
    
    protected abstract void processParagraph(final HWPFDocumentCore p0, final Element p1, final int p2, final Paragraph p3, final String p4);
    
    protected void processParagraphes(final HWPFDocumentCore wordDocument, final Element flow, final Range range, final int currentTableLevel) {
        final ListTables listTables = wordDocument.getListTables();
        int currentListInfo = 0;
        for (int paragraphs = range.numParagraphs(), p = 0; p < paragraphs; ++p) {
            final Paragraph paragraph = range.getParagraph(p);
            if (paragraph.isInTable() && paragraph.getTableLevel() != currentTableLevel) {
                if (paragraph.getTableLevel() < currentTableLevel) {
                    throw new IllegalStateException("Trying to process table cell with higher level (" + paragraph.getTableLevel() + ") than current table level (" + currentTableLevel + ") as inner table part");
                }
                final Table table = range.getTable(paragraph);
                this.processTable(wordDocument, flow, table);
                p += table.numParagraphs();
                --p;
            }
            else {
                if (paragraph.text().equals("\f")) {
                    this.processPageBreak(wordDocument, flow);
                }
                if (paragraph.getIlfo() != currentListInfo) {
                    currentListInfo = paragraph.getIlfo();
                }
                if (currentListInfo != 0) {
                    if (listTables != null) {
                        final ListFormatOverride listFormatOverride = listTables.getOverride(paragraph.getIlfo());
                        final String label = AbstractWordUtils.getBulletText(listTables, paragraph, listFormatOverride.getLsid());
                        this.processParagraph(wordDocument, flow, currentTableLevel, paragraph, label);
                    }
                    else {
                        AbstractWordConverter.logger.log(POILogger.WARN, "Paragraph #" + paragraph.getStartOffset() + "-" + paragraph.getEndOffset() + " has reference to list structure #" + currentListInfo + ", but listTables not defined in file");
                        this.processParagraph(wordDocument, flow, currentTableLevel, paragraph, "");
                    }
                }
                else {
                    this.processParagraph(wordDocument, flow, currentTableLevel, paragraph, "");
                }
            }
        }
    }
    
    protected abstract void processSection(final HWPFDocumentCore p0, final Section p1, final int p2);
    
    protected void processSingleSection(final HWPFDocumentCore wordDocument, final Section section) {
        this.processSection(wordDocument, section, 0);
    }
    
    protected abstract void processTable(final HWPFDocumentCore p0, final Element p1, final Table p2);
    
    public void setFontReplacer(final FontReplacer fontReplacer) {
        this.fontReplacer = fontReplacer;
    }
    
    public void setPicturesManager(final PicturesManager fileManager) {
        this.picturesManager = fileManager;
    }
    
    protected int tryDeadField(final HWPFDocumentCore wordDocument, final Range range, final int currentTableLevel, final int beginMark, final Element currentBlock) {
        final int[] separatorEnd = this.tryDeadField_lookupFieldSeparatorEnd(wordDocument, range, beginMark);
        if (separatorEnd == null) {
            return beginMark;
        }
        this.processDeadField(wordDocument, currentBlock, range, currentTableLevel, beginMark, separatorEnd[0], separatorEnd[1]);
        return separatorEnd[1];
    }
    
    private int[] tryDeadField_lookupFieldSeparatorEnd(final HWPFDocumentCore wordDocument, final Range range, final int beginMark) {
        int separatorMark = -1;
        int endMark = -1;
        for (int c = beginMark + 1; c < range.numCharacterRuns(); ++c) {
            final CharacterRun characterRun = range.getCharacterRun(c);
            final String text = characterRun.text();
            if (text.getBytes().length != 0) {
                final byte firstByte = text.getBytes()[0];
                if (firstByte == 19) {
                    final int[] nested = this.tryDeadField_lookupFieldSeparatorEnd(wordDocument, range, c);
                    if (nested != null) {
                        c = nested[1];
                    }
                }
                else if (firstByte == 20) {
                    if (separatorMark != -1) {
                        return null;
                    }
                    separatorMark = c;
                }
                else if (text.getBytes()[0] == 21) {
                    if (endMark != -1) {
                        return null;
                    }
                    endMark = c;
                    break;
                }
            }
        }
        if (separatorMark == -1 || endMark == -1) {
            return null;
        }
        return new int[] { separatorMark, endMark };
    }
    
    static {
        logger = POILogFactory.getLogger(AbstractWordConverter.class);
        PATTERN_HYPERLINK_EXTERNAL = Pattern.compile("^[ \\t\\r\\n]*HYPERLINK \"(.*)\".*$");
        PATTERN_HYPERLINK_LOCAL = Pattern.compile("^[ \\t\\r\\n]*HYPERLINK \\\\l \"(.*)\"[ ](.*)$");
        PATTERN_PAGEREF = Pattern.compile("^[ \\t\\r\\n]*PAGEREF ([^ ]*)[ \\t\\r\\n]*\\\\h.*$");
    }
    
    private static class DeadFieldBoundaries
    {
        final int beginMark;
        final int endMark;
        final int separatorMark;
        
        public DeadFieldBoundaries(final int beginMark, final int separatorMark, final int endMark) {
            this.beginMark = beginMark;
            this.separatorMark = separatorMark;
            this.endMark = endMark;
        }
    }
    
    private static final class Structure implements Comparable<Structure>
    {
        final int end;
        final int start;
        final Object structure;
        
        Structure(final Bookmark bookmark) {
            this.start = bookmark.getStart();
            this.end = bookmark.getEnd();
            this.structure = bookmark;
        }
        
        Structure(final DeadFieldBoundaries deadFieldBoundaries, final int start, final int end) {
            this.start = start;
            this.end = end;
            this.structure = deadFieldBoundaries;
        }
        
        Structure(final Field field) {
            this.start = field.getFieldStartOffset();
            this.end = field.getFieldEndOffset();
            this.structure = field;
        }
        
        public int compareTo(final Structure o) {
            return (this.start < o.start) ? -1 : ((this.start == o.start) ? 0 : 1);
        }
        
        @Override
        public String toString() {
            return "Structure [" + this.start + "; " + this.end + "): " + this.structure.toString();
        }
    }
}
