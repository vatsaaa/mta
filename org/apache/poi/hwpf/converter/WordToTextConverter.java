// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hwpf.converter;

import org.apache.poi.util.POILogFactory;
import org.apache.poi.hwpf.usermodel.TableCell;
import org.apache.poi.hwpf.usermodel.TableRow;
import org.apache.poi.hwpf.usermodel.Table;
import org.apache.poi.hwpf.usermodel.Section;
import org.apache.poi.hwpf.usermodel.Paragraph;
import java.lang.reflect.Method;
import org.apache.poi.poifs.filesystem.Entry;
import org.apache.poi.hwpf.usermodel.Picture;
import org.apache.poi.hwpf.usermodel.OfficeDrawing;
import org.apache.poi.hwpf.HWPFDocument;
import org.apache.poi.hpsf.SummaryInformation;
import org.apache.poi.hwpf.usermodel.Bookmark;
import java.util.List;
import org.apache.poi.hwpf.usermodel.Range;
import org.apache.poi.hwpf.usermodel.CharacterRun;
import java.io.StringWriter;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import org.w3c.dom.Document;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.TransformerFactory;
import java.io.Writer;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Node;
import javax.xml.transform.dom.DOMSource;
import java.io.FileWriter;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import org.apache.poi.hwpf.HWPFDocumentCore;
import org.apache.poi.poifs.filesystem.DirectoryNode;
import org.w3c.dom.Element;
import java.util.concurrent.atomic.AtomicInteger;
import org.apache.poi.util.POILogger;

public class WordToTextConverter extends AbstractWordConverter
{
    private static final POILogger logger;
    private AtomicInteger noteCounters;
    private Element notes;
    private boolean outputSummaryInformation;
    private final TextDocumentFacade textDocumentFacade;
    
    public static String getText(final DirectoryNode root) throws Exception {
        final HWPFDocumentCore wordDocument = AbstractWordUtils.loadDoc(root);
        return getText(wordDocument);
    }
    
    public static String getText(final File docFile) throws Exception {
        final HWPFDocumentCore wordDocument = AbstractWordUtils.loadDoc(docFile);
        return getText(wordDocument);
    }
    
    public static String getText(final HWPFDocumentCore wordDocument) throws Exception {
        final WordToTextConverter wordToTextConverter = new WordToTextConverter(DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument());
        wordToTextConverter.processDocument(wordDocument);
        return wordToTextConverter.getText();
    }
    
    public static void main(final String[] args) {
        if (args.length < 2) {
            System.err.println("Usage: WordToTextConverter <inputFile.doc> <saveTo.txt>");
            return;
        }
        System.out.println("Converting " + args[0]);
        System.out.println("Saving output to " + args[1]);
        try {
            final Document doc = process(new File(args[0]));
            final FileWriter out = new FileWriter(args[1]);
            final DOMSource domSource = new DOMSource(doc);
            final StreamResult streamResult = new StreamResult(out);
            final TransformerFactory tf = TransformerFactory.newInstance();
            final Transformer serializer = tf.newTransformer();
            serializer.setOutputProperty("encoding", "UTF-8");
            serializer.setOutputProperty("indent", "no");
            serializer.setOutputProperty("method", "text");
            serializer.transform(domSource, streamResult);
            out.close();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    static Document process(final File docFile) throws Exception {
        final HWPFDocumentCore wordDocument = AbstractWordUtils.loadDoc(docFile);
        final WordToTextConverter wordToTextConverter = new WordToTextConverter(DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument());
        wordToTextConverter.processDocument(wordDocument);
        return wordToTextConverter.getDocument();
    }
    
    public WordToTextConverter() throws ParserConfigurationException {
        this.noteCounters = new AtomicInteger(1);
        this.notes = null;
        this.outputSummaryInformation = false;
        this.textDocumentFacade = new TextDocumentFacade(DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument());
    }
    
    public WordToTextConverter(final Document document) {
        this.noteCounters = new AtomicInteger(1);
        this.notes = null;
        this.outputSummaryInformation = false;
        this.textDocumentFacade = new TextDocumentFacade(document);
    }
    
    public WordToTextConverter(final TextDocumentFacade textDocumentFacade) {
        this.noteCounters = new AtomicInteger(1);
        this.notes = null;
        this.outputSummaryInformation = false;
        this.textDocumentFacade = textDocumentFacade;
    }
    
    @Override
    protected void afterProcess() {
        if (this.notes != null) {
            this.textDocumentFacade.getBody().appendChild(this.notes);
        }
    }
    
    @Override
    public Document getDocument() {
        return this.textDocumentFacade.getDocument();
    }
    
    public String getText() throws Exception {
        final StringWriter stringWriter = new StringWriter();
        final DOMSource domSource = new DOMSource(this.getDocument());
        final StreamResult streamResult = new StreamResult(stringWriter);
        final TransformerFactory tf = TransformerFactory.newInstance();
        final Transformer serializer = tf.newTransformer();
        serializer.setOutputProperty("encoding", "UTF-8");
        serializer.setOutputProperty("indent", "no");
        serializer.setOutputProperty("method", "text");
        serializer.transform(domSource, streamResult);
        return stringWriter.toString();
    }
    
    public boolean isOutputSummaryInformation() {
        return this.outputSummaryInformation;
    }
    
    @Override
    protected void outputCharacters(final Element block, final CharacterRun characterRun, final String text) {
        block.appendChild(this.textDocumentFacade.createText(text));
    }
    
    @Override
    protected void processBookmarks(final HWPFDocumentCore wordDocument, final Element currentBlock, final Range range, final int currentTableLevel, final List<Bookmark> rangeBookmarks) {
        this.processCharacters(wordDocument, currentTableLevel, range, currentBlock);
    }
    
    @Override
    protected void processDocumentInformation(final SummaryInformation summaryInformation) {
        if (this.isOutputSummaryInformation()) {
            if (AbstractWordUtils.isNotEmpty(summaryInformation.getTitle())) {
                this.textDocumentFacade.setTitle(summaryInformation.getTitle());
            }
            if (AbstractWordUtils.isNotEmpty(summaryInformation.getAuthor())) {
                this.textDocumentFacade.addAuthor(summaryInformation.getAuthor());
            }
            if (AbstractWordUtils.isNotEmpty(summaryInformation.getComments())) {
                this.textDocumentFacade.addDescription(summaryInformation.getComments());
            }
            if (AbstractWordUtils.isNotEmpty(summaryInformation.getKeywords())) {
                this.textDocumentFacade.addKeywords(summaryInformation.getKeywords());
            }
        }
    }
    
    public void processDocumentPart(final HWPFDocumentCore wordDocument, final Range range) {
        super.processDocumentPart(wordDocument, range);
        this.afterProcess();
    }
    
    @Override
    protected void processDrawnObject(final HWPFDocument doc, final CharacterRun characterRun, final OfficeDrawing officeDrawing, final String path, final Element block) {
    }
    
    @Override
    protected void processEndnoteAutonumbered(final HWPFDocument wordDocument, final int noteIndex, final Element block, final Range endnoteTextRange) {
        this.processNote(wordDocument, block, endnoteTextRange);
    }
    
    @Override
    protected void processFootnoteAutonumbered(final HWPFDocument wordDocument, final int noteIndex, final Element block, final Range footnoteTextRange) {
        this.processNote(wordDocument, block, footnoteTextRange);
    }
    
    @Override
    protected void processHyperlink(final HWPFDocumentCore wordDocument, final Element currentBlock, final Range textRange, final int currentTableLevel, final String hyperlink) {
        this.processCharacters(wordDocument, currentTableLevel, textRange, currentBlock);
        currentBlock.appendChild(this.textDocumentFacade.createText(" (\u200b" + hyperlink.replaceAll("\\/", "\u200b\\/\u200b") + '\u200b' + ")"));
    }
    
    @Override
    protected void processImage(final Element currentBlock, final boolean inlined, final Picture picture) {
    }
    
    @Override
    protected void processImage(final Element currentBlock, final boolean inlined, final Picture picture, final String url) {
    }
    
    @Override
    protected void processImageWithoutPicturesManager(final Element currentBlock, final boolean inlined, final Picture picture) {
    }
    
    @Override
    protected void processLineBreak(final Element block, final CharacterRun characterRun) {
        block.appendChild(this.textDocumentFacade.createText("\n"));
    }
    
    protected void processNote(final HWPFDocument wordDocument, final Element block, final Range noteTextRange) {
        final int noteIndex = this.noteCounters.getAndIncrement();
        block.appendChild(this.textDocumentFacade.createText("\u200b[" + noteIndex + "]" + '\u200b'));
        if (this.notes == null) {
            this.notes = this.textDocumentFacade.createBlock();
        }
        final Element note = this.textDocumentFacade.createBlock();
        this.notes.appendChild(note);
        note.appendChild(this.textDocumentFacade.createText("^" + noteIndex + "\t "));
        this.processCharacters(wordDocument, Integer.MIN_VALUE, noteTextRange, note);
        note.appendChild(this.textDocumentFacade.createText("\n"));
    }
    
    @Override
    protected boolean processOle2(final HWPFDocument wordDocument, final Element block, final Entry entry) throws Exception {
        if (!(entry instanceof DirectoryNode)) {
            return false;
        }
        final DirectoryNode directoryNode = (DirectoryNode)entry;
        if (directoryNode.hasEntry("WordDocument")) {
            final String text = getText((DirectoryNode)entry);
            block.appendChild(this.textDocumentFacade.createText('\u200b' + text + '\u200b'));
            return true;
        }
        Object extractor;
        try {
            final Class<?> cls = Class.forName("org.apache.poi.extractor.ExtractorFactory");
            final Method createExtractor = cls.getMethod("createExtractor", DirectoryNode.class);
            extractor = createExtractor.invoke(null, directoryNode);
        }
        catch (Error exc) {
            WordToTextConverter.logger.log(POILogger.WARN, "There is an OLE object entry '", entry.getName(), "', but there is no text extractor for this object type ", "or text extractor factory is not available: ", "" + exc);
            return false;
        }
        try {
            final Method getText = extractor.getClass().getMethod("getText", (Class<?>[])new Class[0]);
            final String text2 = (String)getText.invoke(extractor, new Object[0]);
            block.appendChild(this.textDocumentFacade.createText('\u200b' + text2 + '\u200b'));
            return true;
        }
        catch (Exception exc2) {
            WordToTextConverter.logger.log(POILogger.ERROR, "Unable to extract text from OLE entry '", entry.getName(), "': ", exc2, exc2);
            return false;
        }
    }
    
    @Override
    protected void processPageBreak(final HWPFDocumentCore wordDocument, final Element flow) {
        final Element block = this.textDocumentFacade.createBlock();
        block.appendChild(this.textDocumentFacade.createText("\n"));
        flow.appendChild(block);
    }
    
    @Override
    protected void processPageref(final HWPFDocumentCore wordDocument, final Element currentBlock, final Range textRange, final int currentTableLevel, final String pageref) {
        this.processCharacters(wordDocument, currentTableLevel, textRange, currentBlock);
    }
    
    @Override
    protected void processParagraph(final HWPFDocumentCore wordDocument, final Element parentElement, final int currentTableLevel, final Paragraph paragraph, final String bulletText) {
        final Element pElement = this.textDocumentFacade.createParagraph();
        pElement.appendChild(this.textDocumentFacade.createText(bulletText));
        this.processCharacters(wordDocument, currentTableLevel, paragraph, pElement);
        pElement.appendChild(this.textDocumentFacade.createText("\n"));
        parentElement.appendChild(pElement);
    }
    
    @Override
    protected void processSection(final HWPFDocumentCore wordDocument, final Section section, final int s) {
        final Element sectionElement = this.textDocumentFacade.createBlock();
        this.processParagraphes(wordDocument, sectionElement, section, Integer.MIN_VALUE);
        sectionElement.appendChild(this.textDocumentFacade.createText("\n"));
        this.textDocumentFacade.body.appendChild(sectionElement);
    }
    
    @Override
    protected void processTable(final HWPFDocumentCore wordDocument, final Element flow, final Table table) {
        for (int tableRows = table.numRows(), r = 0; r < tableRows; ++r) {
            final TableRow tableRow = table.getRow(r);
            final Element tableRowElement = this.textDocumentFacade.createTableRow();
            for (int rowCells = tableRow.numCells(), c = 0; c < rowCells; ++c) {
                final TableCell tableCell = tableRow.getCell(c);
                final Element tableCellElement = this.textDocumentFacade.createTableCell();
                if (c != 0) {
                    tableCellElement.appendChild(this.textDocumentFacade.createText("\t"));
                }
                this.processCharacters(wordDocument, table.getTableLevel(), tableCell, tableCellElement);
                tableRowElement.appendChild(tableCellElement);
            }
            tableRowElement.appendChild(this.textDocumentFacade.createText("\n"));
            flow.appendChild(tableRowElement);
        }
    }
    
    public void setOutputSummaryInformation(final boolean outputDocumentInformation) {
        this.outputSummaryInformation = outputDocumentInformation;
    }
    
    static {
        logger = POILogFactory.getLogger(WordToTextConverter.class);
    }
}
