// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hwpf.converter;

import org.apache.poi.util.POILogFactory;
import org.apache.poi.hwpf.usermodel.TableCell;
import org.apache.poi.hwpf.usermodel.TableRow;
import org.apache.poi.hwpf.usermodel.Table;
import org.apache.poi.hwpf.usermodel.Paragraph;
import org.apache.poi.hwpf.usermodel.Picture;
import org.apache.poi.hwpf.usermodel.OfficeDrawing;
import org.apache.poi.hwpf.HWPFDocument;
import org.apache.poi.hpsf.SummaryInformation;
import java.util.Iterator;
import org.apache.poi.hwpf.usermodel.Bookmark;
import java.util.List;
import org.apache.poi.hwpf.usermodel.Range;
import org.w3c.dom.Text;
import org.apache.poi.hwpf.usermodel.CharacterRun;
import org.apache.poi.hwpf.HWPFDocumentCore;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import org.w3c.dom.Document;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.TransformerFactory;
import java.io.Writer;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Node;
import javax.xml.transform.dom.DOMSource;
import java.io.FileWriter;
import java.io.File;
import org.apache.poi.hwpf.usermodel.Section;
import org.w3c.dom.Element;
import java.util.Stack;
import org.apache.poi.util.POILogger;

public class WordToHtmlConverter extends AbstractWordConverter
{
    private static final POILogger logger;
    private final Stack<BlockProperies> blocksProperies;
    private final HtmlDocumentFacade htmlDocumentFacade;
    private Element notes;
    
    private static String getSectionStyle(final Section section) {
        final float leftMargin = section.getMarginLeft() / 1440.0f;
        final float rightMargin = section.getMarginRight() / 1440.0f;
        final float topMargin = section.getMarginTop() / 1440.0f;
        final float bottomMargin = section.getMarginBottom() / 1440.0f;
        String style = "margin: " + topMargin + "in " + rightMargin + "in " + bottomMargin + "in " + leftMargin + "in;";
        if (section.getNumColumns() > 1) {
            style = style + "column-count: " + section.getNumColumns() + ";";
            if (section.isColumnsEvenlySpaced()) {
                final float distance = section.getDistanceBetweenColumns() / 1440.0f;
                style = style + "column-gap: " + distance + "in;";
            }
            else {
                style += "column-gap: 0.25in;";
            }
        }
        return style;
    }
    
    public static void main(final String[] args) {
        if (args.length < 2) {
            System.err.println("Usage: WordToHtmlConverter <inputFile.doc> <saveTo.html>");
            return;
        }
        System.out.println("Converting " + args[0]);
        System.out.println("Saving output to " + args[1]);
        try {
            final Document doc = process(new File(args[0]));
            final FileWriter out = new FileWriter(args[1]);
            final DOMSource domSource = new DOMSource(doc);
            final StreamResult streamResult = new StreamResult(out);
            final TransformerFactory tf = TransformerFactory.newInstance();
            final Transformer serializer = tf.newTransformer();
            serializer.setOutputProperty("encoding", "UTF-8");
            serializer.setOutputProperty("indent", "yes");
            serializer.setOutputProperty("method", "html");
            serializer.transform(domSource, streamResult);
            out.close();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    static Document process(final File docFile) throws Exception {
        final HWPFDocumentCore wordDocument = AbstractWordUtils.loadDoc(docFile);
        final WordToHtmlConverter wordToHtmlConverter = new WordToHtmlConverter(DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument());
        wordToHtmlConverter.processDocument(wordDocument);
        return wordToHtmlConverter.getDocument();
    }
    
    public WordToHtmlConverter(final Document document) {
        this.blocksProperies = new Stack<BlockProperies>();
        this.notes = null;
        this.htmlDocumentFacade = new HtmlDocumentFacade(document);
    }
    
    public WordToHtmlConverter(final HtmlDocumentFacade htmlDocumentFacade) {
        this.blocksProperies = new Stack<BlockProperies>();
        this.notes = null;
        this.htmlDocumentFacade = htmlDocumentFacade;
    }
    
    @Override
    protected void afterProcess() {
        if (this.notes != null) {
            this.htmlDocumentFacade.getBody().appendChild(this.notes);
        }
        this.htmlDocumentFacade.updateStylesheet();
    }
    
    @Override
    public Document getDocument() {
        return this.htmlDocumentFacade.getDocument();
    }
    
    @Override
    protected void outputCharacters(final Element pElement, final CharacterRun characterRun, final String text) {
        final Element span = this.htmlDocumentFacade.document.createElement("span");
        pElement.appendChild(span);
        final StringBuilder style = new StringBuilder();
        final BlockProperies blockProperies = this.blocksProperies.peek();
        final FontReplacer.Triplet triplet = this.getCharacterRunTriplet(characterRun);
        if (AbstractWordUtils.isNotEmpty(triplet.fontName) && !AbstractWordUtils.equals(triplet.fontName, blockProperies.pFontName)) {
            style.append("font-family:" + triplet.fontName + ";");
        }
        if (characterRun.getFontSize() / 2 != blockProperies.pFontSize) {
            style.append("font-size:" + characterRun.getFontSize() / 2 + "pt;");
        }
        if (triplet.bold) {
            style.append("font-weight:bold;");
        }
        if (triplet.italic) {
            style.append("font-style:italic;");
        }
        WordToHtmlUtils.addCharactersProperties(characterRun, style);
        if (style.length() != 0) {
            this.htmlDocumentFacade.addStyleClass(span, "s", style.toString());
        }
        final Text textNode = this.htmlDocumentFacade.createText(text);
        span.appendChild(textNode);
    }
    
    @Override
    protected void processBookmarks(final HWPFDocumentCore wordDocument, final Element currentBlock, final Range range, final int currentTableLevel, final List<Bookmark> rangeBookmarks) {
        Element parent = currentBlock;
        for (final Bookmark bookmark : rangeBookmarks) {
            final Element bookmarkElement = this.htmlDocumentFacade.createBookmark(bookmark.getName());
            parent.appendChild(bookmarkElement);
            parent = bookmarkElement;
        }
        if (range != null) {
            this.processCharacters(wordDocument, currentTableLevel, range, parent);
        }
    }
    
    @Override
    protected void processDocumentInformation(final SummaryInformation summaryInformation) {
        if (AbstractWordUtils.isNotEmpty(summaryInformation.getTitle())) {
            this.htmlDocumentFacade.setTitle(summaryInformation.getTitle());
        }
        if (AbstractWordUtils.isNotEmpty(summaryInformation.getAuthor())) {
            this.htmlDocumentFacade.addAuthor(summaryInformation.getAuthor());
        }
        if (AbstractWordUtils.isNotEmpty(summaryInformation.getKeywords())) {
            this.htmlDocumentFacade.addKeywords(summaryInformation.getKeywords());
        }
        if (AbstractWordUtils.isNotEmpty(summaryInformation.getComments())) {
            this.htmlDocumentFacade.addDescription(summaryInformation.getComments());
        }
    }
    
    public void processDocumentPart(final HWPFDocumentCore wordDocument, final Range range) {
        super.processDocumentPart(wordDocument, range);
        this.afterProcess();
    }
    
    @Override
    protected void processDrawnObject(final HWPFDocument doc, final CharacterRun characterRun, final OfficeDrawing officeDrawing, final String path, final Element block) {
        final Element img = this.htmlDocumentFacade.createImage(path);
        block.appendChild(img);
    }
    
    @Override
    protected void processEndnoteAutonumbered(final HWPFDocument wordDocument, final int noteIndex, final Element block, final Range endnoteTextRange) {
        this.processNoteAutonumbered(wordDocument, "end", noteIndex, block, endnoteTextRange);
    }
    
    @Override
    protected void processFootnoteAutonumbered(final HWPFDocument wordDocument, final int noteIndex, final Element block, final Range footnoteTextRange) {
        this.processNoteAutonumbered(wordDocument, "foot", noteIndex, block, footnoteTextRange);
    }
    
    @Override
    protected void processHyperlink(final HWPFDocumentCore wordDocument, final Element currentBlock, final Range textRange, final int currentTableLevel, final String hyperlink) {
        final Element basicLink = this.htmlDocumentFacade.createHyperlink(hyperlink);
        currentBlock.appendChild(basicLink);
        if (textRange != null) {
            this.processCharacters(wordDocument, currentTableLevel, textRange, basicLink);
        }
    }
    
    @Override
    protected void processImage(final Element currentBlock, final boolean inlined, final Picture picture, final String imageSourcePath) {
        final int aspectRatioX = picture.getHorizontalScalingFactor();
        final int aspectRatioY = picture.getVerticalScalingFactor();
        final StringBuilder style = new StringBuilder();
        float imageWidth;
        float cropRight;
        float cropLeft;
        if (aspectRatioX > 0) {
            imageWidth = picture.getDxaGoal() * aspectRatioX / 1000 / 1440.0f;
            cropRight = picture.getDxaCropRight() * aspectRatioX / 1000 / 1440.0f;
            cropLeft = picture.getDxaCropLeft() * aspectRatioX / 1000 / 1440.0f;
        }
        else {
            imageWidth = picture.getDxaGoal() / 1440.0f;
            cropRight = picture.getDxaCropRight() / 1440.0f;
            cropLeft = picture.getDxaCropLeft() / 1440.0f;
        }
        float imageHeight;
        float cropTop;
        float cropBottom;
        if (aspectRatioY > 0) {
            imageHeight = picture.getDyaGoal() * aspectRatioY / 1000 / 1440.0f;
            cropTop = picture.getDyaCropTop() * aspectRatioY / 1000 / 1440.0f;
            cropBottom = picture.getDyaCropBottom() * aspectRatioY / 1000 / 1440.0f;
        }
        else {
            imageHeight = picture.getDyaGoal() / 1440.0f;
            cropTop = picture.getDyaCropTop() / 1440.0f;
            cropBottom = picture.getDyaCropBottom() / 1440.0f;
        }
        Element root;
        if (cropTop != 0.0f || cropRight != 0.0f || cropBottom != 0.0f || cropLeft != 0.0f) {
            final float visibleWidth = Math.max(0.0f, imageWidth - cropLeft - cropRight);
            final float visibleHeight = Math.max(0.0f, imageHeight - cropTop - cropBottom);
            root = this.htmlDocumentFacade.createBlock();
            this.htmlDocumentFacade.addStyleClass(root, "d", "vertical-align:text-bottom;width:" + visibleWidth + "in;height:" + visibleHeight + "in;");
            final Element inner = this.htmlDocumentFacade.createBlock();
            this.htmlDocumentFacade.addStyleClass(inner, "d", "position:relative;width:" + visibleWidth + "in;height:" + visibleHeight + "in;overflow:hidden;");
            root.appendChild(inner);
            final Element image = this.htmlDocumentFacade.createImage(imageSourcePath);
            this.htmlDocumentFacade.addStyleClass(image, "i", "position:absolute;left:-" + cropLeft + ";top:-" + cropTop + ";width:" + imageWidth + "in;height:" + imageHeight + "in;");
            inner.appendChild(image);
            style.append("overflow:hidden;");
        }
        else {
            root = this.htmlDocumentFacade.createImage(imageSourcePath);
            root.setAttribute("style", "width:" + imageWidth + "in;height:" + imageHeight + "in;vertical-align:text-bottom;");
        }
        currentBlock.appendChild(root);
    }
    
    @Override
    protected void processImageWithoutPicturesManager(final Element currentBlock, final boolean inlined, final Picture picture) {
        currentBlock.appendChild(this.htmlDocumentFacade.document.createComment("Image link to '" + picture.suggestFullFileName() + "' can be here"));
    }
    
    @Override
    protected void processLineBreak(final Element block, final CharacterRun characterRun) {
        block.appendChild(this.htmlDocumentFacade.createLineBreak());
    }
    
    protected void processNoteAutonumbered(final HWPFDocument doc, final String type, final int noteIndex, final Element block, final Range noteTextRange) {
        final String textIndex = String.valueOf(noteIndex + 1);
        final String textIndexClass = this.htmlDocumentFacade.getOrCreateCssClass("a", "vertical-align:super;font-size:smaller;");
        final String forwardNoteLink = type + "note_" + textIndex;
        final String backwardNoteLink = type + "note_back_" + textIndex;
        final Element anchor = this.htmlDocumentFacade.createHyperlink("#" + forwardNoteLink);
        anchor.setAttribute("name", backwardNoteLink);
        anchor.setAttribute("class", textIndexClass + " " + type + "noteanchor");
        anchor.setTextContent(textIndex);
        block.appendChild(anchor);
        if (this.notes == null) {
            (this.notes = this.htmlDocumentFacade.createBlock()).setAttribute("class", "notes");
        }
        final Element note = this.htmlDocumentFacade.createBlock();
        note.setAttribute("class", type + "note");
        this.notes.appendChild(note);
        final Element bookmark = this.htmlDocumentFacade.createBookmark(forwardNoteLink);
        bookmark.setAttribute("href", "#" + backwardNoteLink);
        bookmark.setTextContent(textIndex);
        bookmark.setAttribute("class", textIndexClass + " " + type + "noteindex");
        note.appendChild(bookmark);
        note.appendChild(this.htmlDocumentFacade.createText(" "));
        final Element span = this.htmlDocumentFacade.getDocument().createElement("span");
        span.setAttribute("class", type + "notetext");
        note.appendChild(span);
        this.blocksProperies.add(new BlockProperies("", -1));
        try {
            this.processCharacters(doc, Integer.MIN_VALUE, noteTextRange, span);
        }
        finally {
            this.blocksProperies.pop();
        }
    }
    
    @Override
    protected void processPageBreak(final HWPFDocumentCore wordDocument, final Element flow) {
        flow.appendChild(this.htmlDocumentFacade.createLineBreak());
    }
    
    @Override
    protected void processPageref(final HWPFDocumentCore hwpfDocument, final Element currentBlock, final Range textRange, final int currentTableLevel, final String pageref) {
        final Element basicLink = this.htmlDocumentFacade.createHyperlink("#" + pageref);
        currentBlock.appendChild(basicLink);
        if (textRange != null) {
            this.processCharacters(hwpfDocument, currentTableLevel, textRange, basicLink);
        }
    }
    
    @Override
    protected void processParagraph(final HWPFDocumentCore hwpfDocument, final Element parentElement, final int currentTableLevel, final Paragraph paragraph, final String bulletText) {
        final Element pElement = this.htmlDocumentFacade.createParagraph();
        parentElement.appendChild(pElement);
        final StringBuilder style = new StringBuilder();
        WordToHtmlUtils.addParagraphProperties(paragraph, style);
        final int charRuns = paragraph.numCharacterRuns();
        if (charRuns == 0) {
            return;
        }
        final CharacterRun characterRun = paragraph.getCharacterRun(0);
        int pFontSize;
        String pFontName;
        if (characterRun != null) {
            final FontReplacer.Triplet triplet = this.getCharacterRunTriplet(characterRun);
            pFontSize = characterRun.getFontSize() / 2;
            pFontName = triplet.fontName;
            WordToHtmlUtils.addFontFamily(pFontName, style);
            WordToHtmlUtils.addFontSize(pFontSize, style);
        }
        else {
            pFontSize = -1;
            pFontName = "";
        }
        this.blocksProperies.push(new BlockProperies(pFontName, pFontSize));
        try {
            if (AbstractWordUtils.isNotEmpty(bulletText)) {
                if (bulletText.endsWith("\t")) {
                    final float defaultTab = 720.0f;
                    final float firstLinePosition = (float)(paragraph.getIndentFromLeft() + paragraph.getFirstLineIndent() + 20);
                    final float nextStop = (float)(Math.ceil(firstLinePosition / 720.0f) * 720.0);
                    final float spanMinWidth = nextStop - firstLinePosition;
                    final Element span = this.htmlDocumentFacade.getDocument().createElement("span");
                    this.htmlDocumentFacade.addStyleClass(span, "s", "display: inline-block; text-indent: 0; min-width: " + spanMinWidth / 1440.0f + "in;");
                    pElement.appendChild(span);
                    final Text textNode = this.htmlDocumentFacade.createText(bulletText.substring(0, bulletText.length() - 1) + '\u200b' + ' ');
                    span.appendChild(textNode);
                }
                else {
                    final Text textNode2 = this.htmlDocumentFacade.createText(bulletText.substring(0, bulletText.length() - 1));
                    pElement.appendChild(textNode2);
                }
            }
            this.processCharacters(hwpfDocument, currentTableLevel, paragraph, pElement);
        }
        finally {
            this.blocksProperies.pop();
        }
        if (style.length() > 0) {
            this.htmlDocumentFacade.addStyleClass(pElement, "p", style.toString());
        }
        WordToHtmlUtils.compactSpans(pElement);
    }
    
    @Override
    protected void processSection(final HWPFDocumentCore wordDocument, final Section section, final int sectionCounter) {
        final Element div = this.htmlDocumentFacade.createBlock();
        this.htmlDocumentFacade.addStyleClass(div, "d", getSectionStyle(section));
        this.htmlDocumentFacade.body.appendChild(div);
        this.processParagraphes(wordDocument, div, section, Integer.MIN_VALUE);
    }
    
    @Override
    protected void processSingleSection(final HWPFDocumentCore wordDocument, final Section section) {
        this.htmlDocumentFacade.addStyleClass(this.htmlDocumentFacade.body, "b", getSectionStyle(section));
        this.processParagraphes(wordDocument, this.htmlDocumentFacade.body, section, Integer.MIN_VALUE);
    }
    
    @Override
    protected void processTable(final HWPFDocumentCore hwpfDocument, final Element flow, final Table table) {
        final Element tableHeader = this.htmlDocumentFacade.createTableHeader();
        final Element tableBody = this.htmlDocumentFacade.createTableBody();
        final int[] tableCellEdges = AbstractWordUtils.buildTableCellEdgesArray(table);
        final int tableRows = table.numRows();
        int maxColumns = Integer.MIN_VALUE;
        for (int r = 0; r < tableRows; ++r) {
            maxColumns = Math.max(maxColumns, table.getRow(r).numCells());
        }
        for (int r = 0; r < tableRows; ++r) {
            final TableRow tableRow = table.getRow(r);
            final Element tableRowElement = this.htmlDocumentFacade.createTableRow();
            final StringBuilder tableRowStyle = new StringBuilder();
            WordToHtmlUtils.addTableRowProperties(tableRow, tableRowStyle);
            int currentEdgeIndex = 0;
            for (int rowCells = tableRow.numCells(), c = 0; c < rowCells; ++c) {
                final TableCell tableCell = tableRow.getCell(c);
                if (tableCell.isVerticallyMerged() && !tableCell.isFirstVerticallyMerged()) {
                    currentEdgeIndex += this.getNumberColumnsSpanned(tableCellEdges, currentEdgeIndex, tableCell);
                }
                else {
                    Element tableCellElement;
                    if (tableRow.isTableHeader()) {
                        tableCellElement = this.htmlDocumentFacade.createTableHeaderCell();
                    }
                    else {
                        tableCellElement = this.htmlDocumentFacade.createTableCell();
                    }
                    final StringBuilder tableCellStyle = new StringBuilder();
                    WordToHtmlUtils.addTableCellProperties(tableRow, tableCell, r == 0, r == tableRows - 1, c == 0, c == rowCells - 1, tableCellStyle);
                    final int colSpan = this.getNumberColumnsSpanned(tableCellEdges, currentEdgeIndex, tableCell);
                    currentEdgeIndex += colSpan;
                    if (colSpan != 0) {
                        if (colSpan != 1) {
                            tableCellElement.setAttribute("colspan", String.valueOf(colSpan));
                        }
                        final int rowSpan = this.getNumberRowsSpanned(table, tableCellEdges, r, c, tableCell);
                        if (rowSpan > 1) {
                            tableCellElement.setAttribute("rowspan", String.valueOf(rowSpan));
                        }
                        this.processParagraphes(hwpfDocument, tableCellElement, tableCell, table.getTableLevel());
                        if (!tableCellElement.hasChildNodes()) {
                            tableCellElement.appendChild(this.htmlDocumentFacade.createParagraph());
                        }
                        if (tableCellStyle.length() > 0) {
                            this.htmlDocumentFacade.addStyleClass(tableCellElement, tableCellElement.getTagName(), tableCellStyle.toString());
                        }
                        tableRowElement.appendChild(tableCellElement);
                    }
                }
            }
            if (tableRowStyle.length() > 0) {
                tableRowElement.setAttribute("class", this.htmlDocumentFacade.getOrCreateCssClass("r", tableRowStyle.toString()));
            }
            if (tableRow.isTableHeader()) {
                tableHeader.appendChild(tableRowElement);
            }
            else {
                tableBody.appendChild(tableRowElement);
            }
        }
        final Element tableElement = this.htmlDocumentFacade.createTable();
        tableElement.setAttribute("class", this.htmlDocumentFacade.getOrCreateCssClass("t", "table-layout:fixed;border-collapse:collapse;border-spacing:0;"));
        if (tableHeader.hasChildNodes()) {
            tableElement.appendChild(tableHeader);
        }
        if (tableBody.hasChildNodes()) {
            tableElement.appendChild(tableBody);
            flow.appendChild(tableElement);
        }
        else {
            WordToHtmlConverter.logger.log(POILogger.WARN, "Table without body starting at [", table.getStartOffset(), "; ", table.getEndOffset(), ")");
        }
    }
    
    static {
        logger = POILogFactory.getLogger(WordToHtmlConverter.class);
    }
    
    private static class BlockProperies
    {
        final String pFontName;
        final int pFontSize;
        
        public BlockProperies(final String pFontName, final int pFontSize) {
            this.pFontName = pFontName;
            this.pFontSize = pFontSize;
        }
    }
}
