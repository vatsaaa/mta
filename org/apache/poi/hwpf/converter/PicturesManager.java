// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hwpf.converter;

import org.apache.poi.hwpf.usermodel.PictureType;

public interface PicturesManager
{
    String savePicture(final byte[] p0, final PictureType p1, final String p2, final float p3, final float p4);
}
