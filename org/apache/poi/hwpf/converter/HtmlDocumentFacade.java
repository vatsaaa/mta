// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hwpf.converter;

import java.util.Iterator;
import org.w3c.dom.Node;
import java.util.LinkedHashMap;
import org.w3c.dom.Text;
import java.util.Map;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class HtmlDocumentFacade
{
    protected final Element body;
    protected final Document document;
    protected final Element head;
    protected final Element html;
    private Map<String, Map<String, String>> stylesheet;
    private Element stylesheetElement;
    protected Element title;
    protected Text titleText;
    
    public HtmlDocumentFacade(final Document document) {
        this.stylesheet = new LinkedHashMap<String, Map<String, String>>();
        (this.document = document).appendChild(this.html = document.createElement("html"));
        this.body = document.createElement("body");
        this.head = document.createElement("head");
        (this.stylesheetElement = document.createElement("style")).setAttribute("type", "text/css");
        this.html.appendChild(this.head);
        this.html.appendChild(this.body);
        this.head.appendChild(this.stylesheetElement);
        this.addStyleClass(this.body, "b", "white-space-collapsing:preserve;");
    }
    
    public void addAuthor(final String value) {
        this.addMeta("author", value);
    }
    
    public void addDescription(final String value) {
        this.addMeta("description", value);
    }
    
    public void addKeywords(final String value) {
        this.addMeta("keywords", value);
    }
    
    public void addMeta(final String name, final String value) {
        final Element meta = this.document.createElement("meta");
        meta.setAttribute("name", name);
        meta.setAttribute("content", value);
        this.head.appendChild(meta);
    }
    
    public void addStyleClass(final Element element, final String classNamePrefix, final String style) {
        final String exising = element.getAttribute("class");
        final String addition = this.getOrCreateCssClass(classNamePrefix, style);
        final String newClassValue = AbstractWordUtils.isEmpty(exising) ? addition : (exising + " " + addition);
        element.setAttribute("class", newClassValue);
    }
    
    protected String buildStylesheet(final Map<String, Map<String, String>> prefixToMapOfStyles) {
        final StringBuilder stringBuilder = new StringBuilder();
        for (final Map<String, String> byPrefix : prefixToMapOfStyles.values()) {
            for (final Map.Entry<String, String> byStyle : byPrefix.entrySet()) {
                final String style = byStyle.getKey();
                final String className = byStyle.getValue();
                stringBuilder.append(".");
                stringBuilder.append(className);
                stringBuilder.append("{");
                stringBuilder.append(style);
                stringBuilder.append("}\n");
            }
        }
        final String stylesheetText = stringBuilder.toString();
        return stylesheetText;
    }
    
    public Element createBlock() {
        return this.document.createElement("div");
    }
    
    public Element createBookmark(final String name) {
        final Element basicLink = this.document.createElement("a");
        basicLink.setAttribute("name", name);
        return basicLink;
    }
    
    public Element createHeader1() {
        return this.document.createElement("h1");
    }
    
    public Element createHeader2() {
        return this.document.createElement("h2");
    }
    
    public Element createHyperlink(final String internalDestination) {
        final Element basicLink = this.document.createElement("a");
        basicLink.setAttribute("href", internalDestination);
        return basicLink;
    }
    
    public Element createImage(final String src) {
        final Element result = this.document.createElement("img");
        result.setAttribute("src", src);
        return result;
    }
    
    public Element createLineBreak() {
        return this.document.createElement("br");
    }
    
    public Element createListItem() {
        return this.document.createElement("li");
    }
    
    public Element createParagraph() {
        return this.document.createElement("p");
    }
    
    public Element createTable() {
        return this.document.createElement("table");
    }
    
    public Element createTableBody() {
        return this.document.createElement("tbody");
    }
    
    public Element createTableCell() {
        return this.document.createElement("td");
    }
    
    public Element createTableColumn() {
        return this.document.createElement("col");
    }
    
    public Element createTableColumnGroup() {
        return this.document.createElement("colgroup");
    }
    
    public Element createTableHeader() {
        return this.document.createElement("thead");
    }
    
    public Element createTableHeaderCell() {
        return this.document.createElement("th");
    }
    
    public Element createTableRow() {
        return this.document.createElement("tr");
    }
    
    public Text createText(final String data) {
        return this.document.createTextNode(data);
    }
    
    public Element createUnorderedList() {
        return this.document.createElement("ul");
    }
    
    public Element getBody() {
        return this.body;
    }
    
    public Document getDocument() {
        return this.document;
    }
    
    public Element getHead() {
        return this.head;
    }
    
    public String getOrCreateCssClass(final String classNamePrefix, final String style) {
        if (!this.stylesheet.containsKey(classNamePrefix)) {
            this.stylesheet.put(classNamePrefix, new LinkedHashMap<String, String>(1));
        }
        final Map<String, String> styleToClassName = this.stylesheet.get(classNamePrefix);
        final String knownClass = styleToClassName.get(style);
        if (knownClass != null) {
            return knownClass;
        }
        final String newClassName = classNamePrefix + (styleToClassName.size() + 1);
        styleToClassName.put(style, newClassName);
        return newClassName;
    }
    
    public String getTitle() {
        if (this.title == null) {
            return null;
        }
        return this.titleText.getTextContent();
    }
    
    public void setTitle(final String titleText) {
        if (AbstractWordUtils.isEmpty(titleText) && this.title != null) {
            this.head.removeChild(this.title);
            this.title = null;
            this.titleText = null;
        }
        if (this.title == null) {
            this.title = this.document.createElement("title");
            this.titleText = this.document.createTextNode(titleText);
            this.title.appendChild(this.titleText);
            this.head.appendChild(this.title);
        }
        this.titleText.setData(titleText);
    }
    
    public void updateStylesheet() {
        this.stylesheetElement.setTextContent(this.buildStylesheet(this.stylesheet));
    }
}
