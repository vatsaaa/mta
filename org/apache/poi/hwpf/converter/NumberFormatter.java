// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hwpf.converter;

public final class NumberFormatter
{
    private static final String[] ENGLISH_LETTERS;
    private static final String[] ROMAN_LETTERS;
    private static final int[] ROMAN_VALUES;
    private static final int T_ARABIC = 0;
    private static final int T_LOWER_LETTER = 4;
    private static final int T_LOWER_ROMAN = 2;
    private static final int T_ORDINAL = 5;
    private static final int T_UPPER_LETTER = 3;
    private static final int T_UPPER_ROMAN = 1;
    
    public static String getNumber(final int num, final int style) {
        switch (style) {
            case 1: {
                return toRoman(num).toUpperCase();
            }
            case 2: {
                return toRoman(num);
            }
            case 3: {
                return toLetters(num).toUpperCase();
            }
            case 4: {
                return toLetters(num);
            }
            default: {
                return String.valueOf(num);
            }
        }
    }
    
    private static String toLetters(final int number) {
        final int base = 26;
        if (number <= 0) {
            throw new IllegalArgumentException("Unsupported number: " + number);
        }
        if (number < 27) {
            return NumberFormatter.ENGLISH_LETTERS[number - 1];
        }
        long toProcess = number;
        final StringBuilder stringBuilder = new StringBuilder();
        int maxPower = 0;
        int boundary = 0;
        while (toProcess > boundary) {
            ++maxPower;
            boundary = boundary * 26 + 26;
            if (boundary > Integer.MAX_VALUE) {
                throw new IllegalArgumentException("Unsupported number: " + toProcess);
            }
        }
        for (int p = --maxPower; p > 0; --p) {
            long boundary2 = 0L;
            long shift = 1L;
            for (int i = 0; i < p; ++i) {
                shift *= 26L;
                boundary2 = boundary2 * 26L + 26L;
            }
            int count = 0;
            while (toProcess > boundary2) {
                ++count;
                toProcess -= shift;
            }
            stringBuilder.append(NumberFormatter.ENGLISH_LETTERS[count - 1]);
        }
        stringBuilder.append(NumberFormatter.ENGLISH_LETTERS[(int)toProcess - 1]);
        return stringBuilder.toString();
    }
    
    private static String toRoman(int number) {
        if (number <= 0) {
            throw new IllegalArgumentException("Unsupported number: " + number);
        }
        final StringBuilder result = new StringBuilder();
        for (int i = 0; i < NumberFormatter.ROMAN_LETTERS.length; ++i) {
            final String letter = NumberFormatter.ROMAN_LETTERS[i];
            final int value = NumberFormatter.ROMAN_VALUES[i];
            while (number >= value) {
                number -= value;
                result.append(letter);
            }
        }
        return result.toString();
    }
    
    static {
        ENGLISH_LETTERS = new String[] { "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z" };
        ROMAN_LETTERS = new String[] { "m", "cm", "d", "cd", "c", "xc", "l", "xl", "x", "ix", "v", "iv", "i" };
        ROMAN_VALUES = new int[] { 1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1 };
    }
}
