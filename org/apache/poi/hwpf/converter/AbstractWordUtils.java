// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hwpf.converter;

import org.apache.poi.util.POILogFactory;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import java.io.Closeable;
import org.apache.poi.util.IOUtils;
import java.io.InputStream;
import java.io.FileInputStream;
import java.io.File;
import java.io.IOException;
import org.apache.poi.hwpf.OldWordFileFormatException;
import org.apache.poi.hwpf.HWPFOldDocument;
import org.apache.poi.hwpf.HWPFDocument;
import org.apache.poi.hwpf.HWPFDocumentCore;
import org.apache.poi.poifs.filesystem.DirectoryNode;
import org.apache.poi.hwpf.model.ListLevel;
import org.apache.poi.hwpf.usermodel.Paragraph;
import org.apache.poi.hwpf.model.ListTables;
import org.apache.poi.hwpf.usermodel.BorderCode;
import org.w3c.dom.NodeList;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Attr;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.apache.poi.hwpf.usermodel.TableCell;
import org.apache.poi.hwpf.usermodel.TableRow;
import java.util.Set;
import java.util.TreeSet;
import org.apache.poi.hwpf.usermodel.Table;
import org.apache.poi.util.POILogger;

public class AbstractWordUtils
{
    static final String EMPTY = "";
    private static final POILogger logger;
    public static final float TWIPS_PER_INCH = 1440.0f;
    public static final int TWIPS_PER_PT = 20;
    
    static int[] buildTableCellEdgesArray(final Table table) {
        final Set<Integer> edges = new TreeSet<Integer>();
        for (int r = 0; r < table.numRows(); ++r) {
            final TableRow tableRow = table.getRow(r);
            for (int c = 0; c < tableRow.numCells(); ++c) {
                final TableCell tableCell = tableRow.getCell(c);
                edges.add(tableCell.getLeftEdge());
                edges.add(tableCell.getLeftEdge() + tableCell.getWidth());
            }
        }
        final Integer[] sorted = edges.toArray(new Integer[edges.size()]);
        final int[] result = new int[sorted.length];
        for (int i = 0; i < sorted.length; ++i) {
            result[i] = sorted[i];
        }
        return result;
    }
    
    static boolean canBeMerged(final Node node1, final Node node2, final String requiredTagName) {
        if (node1.getNodeType() != 1 || node2.getNodeType() != 1) {
            return false;
        }
        final Element element1 = (Element)node1;
        final Element element2 = (Element)node2;
        if (!equals(requiredTagName, element1.getTagName()) || !equals(requiredTagName, element2.getTagName())) {
            return false;
        }
        final NamedNodeMap attributes1 = element1.getAttributes();
        final NamedNodeMap attributes2 = element2.getAttributes();
        if (attributes1.getLength() != attributes2.getLength()) {
            return false;
        }
        for (int i = 0; i < attributes1.getLength(); ++i) {
            final Attr attr1 = (Attr)attributes1.item(i);
            Attr attr2;
            if (isNotEmpty(attr1.getNamespaceURI())) {
                attr2 = (Attr)attributes2.getNamedItemNS(attr1.getNamespaceURI(), attr1.getLocalName());
            }
            else {
                attr2 = (Attr)attributes2.getNamedItem(attr1.getName());
            }
            if (attr2 == null || !equals(attr1.getTextContent(), attr2.getTextContent())) {
                return false;
            }
        }
        return true;
    }
    
    static void compactChildNodesR(final Element parentElement, final String childTagName) {
        NodeList childNodes = parentElement.getChildNodes();
        for (int i = 0; i < childNodes.getLength() - 1; ++i) {
            final Node child1 = childNodes.item(i);
            final Node child2 = childNodes.item(i + 1);
            if (canBeMerged(child1, child2, childTagName)) {
                while (child2.getChildNodes().getLength() > 0) {
                    child1.appendChild(child2.getFirstChild());
                }
                child2.getParentNode().removeChild(child2);
                --i;
            }
        }
        childNodes = parentElement.getChildNodes();
        for (int i = 0; i < childNodes.getLength() - 1; ++i) {
            final Node child3 = childNodes.item(i);
            if (child3 instanceof Element) {
                compactChildNodesR((Element)child3, childTagName);
            }
        }
    }
    
    static boolean equals(final String str1, final String str2) {
        return (str1 == null) ? (str2 == null) : str1.equals(str2);
    }
    
    public static String getBorderType(final BorderCode borderCode) {
        if (borderCode == null) {
            throw new IllegalArgumentException("borderCode is null");
        }
        switch (borderCode.getBorderType()) {
            case 1:
            case 2: {
                return "solid";
            }
            case 3: {
                return "double";
            }
            case 5: {
                return "solid";
            }
            case 6: {
                return "dotted";
            }
            case 7:
            case 8: {
                return "dashed";
            }
            case 9: {
                return "dotted";
            }
            case 10:
            case 11:
            case 12:
            case 13:
            case 14:
            case 15:
            case 16:
            case 17:
            case 18:
            case 19: {
                return "double";
            }
            case 20: {
                return "solid";
            }
            case 21: {
                return "double";
            }
            case 22: {
                return "dashed";
            }
            case 23: {
                return "dashed";
            }
            case 24: {
                return "ridge";
            }
            case 25: {
                return "grooved";
            }
            default: {
                return "solid";
            }
        }
    }
    
    public static String getBorderWidth(final BorderCode borderCode) {
        final int lineWidth = borderCode.getLineWidth();
        final int pt = lineWidth / 8;
        final int pte = lineWidth - pt * 8;
        final StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(pt);
        stringBuilder.append(".");
        stringBuilder.append(125 * pte);
        stringBuilder.append("pt");
        return stringBuilder.toString();
    }
    
    public static String getBulletText(final ListTables listTables, final Paragraph paragraph, final int listId) {
        final ListLevel listLevel = listTables.getLevel(listId, paragraph.getIlvl());
        if (listLevel.getNumberText() == null) {
            return "";
        }
        final StringBuffer bulletBuffer = new StringBuffer();
        final char[] arr$;
        final char[] xst = arr$ = listLevel.getNumberText().toCharArray();
        for (final char element : arr$) {
            if (element < '\t') {
                final ListLevel numLevel = listTables.getLevel(listId, element);
                final int num = numLevel.getStartAt();
                bulletBuffer.append(NumberFormatter.getNumber(num, listLevel.getNumberFormat()));
                if (numLevel == listLevel) {
                    numLevel.setStartAt(numLevel.getStartAt() + 1);
                }
            }
            else {
                bulletBuffer.append(element);
            }
        }
        final byte follow = listLevel.getTypeOfCharFollowingTheNumber();
        switch (follow) {
            case 0: {
                bulletBuffer.append("\t");
                break;
            }
            case 1: {
                bulletBuffer.append(" ");
                break;
            }
        }
        return bulletBuffer.toString();
    }
    
    public static String getColor(final int ico) {
        switch (ico) {
            case 1: {
                return "black";
            }
            case 2: {
                return "blue";
            }
            case 3: {
                return "cyan";
            }
            case 4: {
                return "green";
            }
            case 5: {
                return "magenta";
            }
            case 6: {
                return "red";
            }
            case 7: {
                return "yellow";
            }
            case 8: {
                return "white";
            }
            case 9: {
                return "darkblue";
            }
            case 10: {
                return "darkcyan";
            }
            case 11: {
                return "darkgreen";
            }
            case 12: {
                return "darkmagenta";
            }
            case 13: {
                return "darkred";
            }
            case 14: {
                return "darkyellow";
            }
            case 15: {
                return "darkgray";
            }
            case 16: {
                return "lightgray";
            }
            default: {
                return "black";
            }
        }
    }
    
    public static String getOpacity(final int argbValue) {
        final int opacity = (int)(((long)argbValue & 0xFF000000L) >>> 24);
        if (opacity == 0 || opacity == 255) {
            return ".0";
        }
        return "" + opacity / 255.0f;
    }
    
    public static String getColor24(final int argbValue) {
        if (argbValue == -1) {
            throw new IllegalArgumentException("This colorref is empty");
        }
        final int bgrValue = argbValue & 0xFFFFFF;
        final int rgbValue = (bgrValue & 0xFF) << 16 | (bgrValue & 0xFF00) | (bgrValue & 0xFF0000) >> 16;
        switch (rgbValue) {
            case 16777215: {
                return "white";
            }
            case 12632256: {
                return "silver";
            }
            case 8421504: {
                return "gray";
            }
            case 0: {
                return "black";
            }
            case 16711680: {
                return "red";
            }
            case 8388608: {
                return "maroon";
            }
            case 16776960: {
                return "yellow";
            }
            case 8421376: {
                return "olive";
            }
            case 65280: {
                return "lime";
            }
            case 32768: {
                return "green";
            }
            case 65535: {
                return "aqua";
            }
            case 32896: {
                return "teal";
            }
            case 255: {
                return "blue";
            }
            case 128: {
                return "navy";
            }
            case 16711935: {
                return "fuchsia";
            }
            case 8388736: {
                return "purple";
            }
            default: {
                final StringBuilder result = new StringBuilder("#");
                final String hex = Integer.toHexString(rgbValue);
                for (int i = hex.length(); i < 6; ++i) {
                    result.append('0');
                }
                result.append(hex);
                return result.toString();
            }
        }
    }
    
    public static String getJustification(final int js) {
        switch (js) {
            case 0: {
                return "start";
            }
            case 1: {
                return "center";
            }
            case 2: {
                return "end";
            }
            case 3:
            case 4: {
                return "justify";
            }
            case 5: {
                return "center";
            }
            case 6: {
                return "left";
            }
            case 7: {
                return "start";
            }
            case 8: {
                return "end";
            }
            case 9: {
                return "justify";
            }
            default: {
                return "";
            }
        }
    }
    
    public static String getLanguage(final int languageCode) {
        switch (languageCode) {
            case 1024: {
                return "";
            }
            case 1033: {
                return "en-us";
            }
            case 1049: {
                return "ru-ru";
            }
            case 2057: {
                return "en-uk";
            }
            default: {
                AbstractWordUtils.logger.log(POILogger.WARN, "Uknown or unmapped language code: ", languageCode);
                return "";
            }
        }
    }
    
    public static String getListItemNumberLabel(final int number, final int format) {
        if (format != 0) {
            System.err.println("NYI: toListItemNumberLabel(): " + format);
        }
        return String.valueOf(number);
    }
    
    static boolean isEmpty(final String str) {
        return str == null || str.length() == 0;
    }
    
    static boolean isNotEmpty(final String str) {
        return !isEmpty(str);
    }
    
    public static HWPFDocumentCore loadDoc(final DirectoryNode root) throws IOException {
        try {
            return new HWPFDocument(root);
        }
        catch (OldWordFileFormatException exc) {
            return new HWPFOldDocument(root);
        }
    }
    
    public static HWPFDocumentCore loadDoc(final File docFile) throws IOException {
        final FileInputStream istream = new FileInputStream(docFile);
        try {
            return loadDoc(istream);
        }
        finally {
            IOUtils.closeQuietly(istream);
        }
    }
    
    public static HWPFDocumentCore loadDoc(final InputStream inputStream) throws IOException {
        return loadDoc(HWPFDocumentCore.verifyAndBuildPOIFS(inputStream));
    }
    
    public static HWPFDocumentCore loadDoc(final POIFSFileSystem poifsFileSystem) throws IOException {
        return loadDoc(poifsFileSystem.getRoot());
    }
    
    static String substringBeforeLast(final String str, final String separator) {
        if (isEmpty(str) || isEmpty(separator)) {
            return str;
        }
        final int pos = str.lastIndexOf(separator);
        if (pos == -1) {
            return str;
        }
        return str.substring(0, pos);
    }
    
    static {
        logger = POILogFactory.getLogger(AbstractWordUtils.class);
    }
}
