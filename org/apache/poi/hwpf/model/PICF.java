// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hwpf.model;

import java.util.Arrays;
import org.apache.poi.util.Internal;
import org.apache.poi.hwpf.model.types.PICFAbstractType;

@Internal
public class PICF extends PICFAbstractType
{
    public PICF() {
    }
    
    public PICF(final byte[] std, final int offset) {
        this.fillFields(std, offset);
    }
    
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        final PICF other = (PICF)obj;
        return this.field_10_padding2 == other.field_10_padding2 && this.field_11_dxaGoal == other.field_11_dxaGoal && this.field_12_dyaGoal == other.field_12_dyaGoal && this.field_13_mx == other.field_13_mx && this.field_14_my == other.field_14_my && this.field_15_dxaReserved1 == other.field_15_dxaReserved1 && this.field_16_dyaReserved1 == other.field_16_dyaReserved1 && this.field_17_dxaReserved2 == other.field_17_dxaReserved2 && this.field_18_dyaReserved2 == other.field_18_dyaReserved2 && this.field_19_fReserved == other.field_19_fReserved && this.field_1_lcb == other.field_1_lcb && this.field_20_bpp == other.field_20_bpp && Arrays.equals(this.field_21_brcTop80, other.field_21_brcTop80) && Arrays.equals(this.field_22_brcLeft80, other.field_22_brcLeft80) && Arrays.equals(this.field_23_brcBottom80, other.field_23_brcBottom80) && Arrays.equals(this.field_24_brcRight80, other.field_24_brcRight80) && this.field_25_dxaReserved3 == other.field_25_dxaReserved3 && this.field_26_dyaReserved3 == other.field_26_dyaReserved3 && this.field_27_cProps == other.field_27_cProps && this.field_2_cbHeader == other.field_2_cbHeader && this.field_3_mm == other.field_3_mm && this.field_4_xExt == other.field_4_xExt && this.field_5_yExt == other.field_5_yExt && this.field_6_swHMF == other.field_6_swHMF && this.field_7_grf == other.field_7_grf && this.field_8_padding == other.field_8_padding && this.field_9_mmPM == other.field_9_mmPM;
    }
    
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = 31 * result + this.field_10_padding2;
        result = 31 * result + this.field_11_dxaGoal;
        result = 31 * result + this.field_12_dyaGoal;
        result = 31 * result + this.field_13_mx;
        result = 31 * result + this.field_14_my;
        result = 31 * result + this.field_15_dxaReserved1;
        result = 31 * result + this.field_16_dyaReserved1;
        result = 31 * result + this.field_17_dxaReserved2;
        result = 31 * result + this.field_18_dyaReserved2;
        result = 31 * result + this.field_19_fReserved;
        result = 31 * result + this.field_1_lcb;
        result = 31 * result + this.field_20_bpp;
        result = 31 * result + Arrays.hashCode(this.field_21_brcTop80);
        result = 31 * result + Arrays.hashCode(this.field_22_brcLeft80);
        result = 31 * result + Arrays.hashCode(this.field_23_brcBottom80);
        result = 31 * result + Arrays.hashCode(this.field_24_brcRight80);
        result = 31 * result + this.field_25_dxaReserved3;
        result = 31 * result + this.field_26_dyaReserved3;
        result = 31 * result + this.field_27_cProps;
        result = 31 * result + this.field_2_cbHeader;
        result = 31 * result + this.field_3_mm;
        result = 31 * result + this.field_4_xExt;
        result = 31 * result + this.field_5_yExt;
        result = 31 * result + this.field_6_swHMF;
        result = 31 * result + this.field_7_grf;
        result = 31 * result + this.field_8_padding;
        result = 31 * result + this.field_9_mmPM;
        return result;
    }
}
