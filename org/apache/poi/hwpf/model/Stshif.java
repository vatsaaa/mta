// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hwpf.model;

import org.apache.poi.util.Internal;
import org.apache.poi.hwpf.model.types.StshifAbstractType;

@Internal
class Stshif extends StshifAbstractType
{
    public Stshif() {
    }
    
    public Stshif(final byte[] std, final int offset) {
        this.fillFields(std, offset);
    }
    
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        final Stshif other = (Stshif)obj;
        return this.field_1_cstd == other.field_1_cstd && this.field_2_cbSTDBaseInFile == other.field_2_cbSTDBaseInFile && this.field_3_info3 == other.field_3_info3 && this.field_4_stiMaxWhenSaved == other.field_4_stiMaxWhenSaved && this.field_5_istdMaxFixedWhenSaved == other.field_5_istdMaxFixedWhenSaved && this.field_6_nVerBuiltInNamesWhenSaved == other.field_6_nVerBuiltInNamesWhenSaved && this.field_7_ftcAsci == other.field_7_ftcAsci && this.field_8_ftcFE == other.field_8_ftcFE && this.field_9_ftcOther == other.field_9_ftcOther;
    }
    
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = 31 * result + this.field_1_cstd;
        result = 31 * result + this.field_2_cbSTDBaseInFile;
        result = 31 * result + this.field_3_info3;
        result = 31 * result + this.field_4_stiMaxWhenSaved;
        result = 31 * result + this.field_5_istdMaxFixedWhenSaved;
        result = 31 * result + this.field_6_nVerBuiltInNamesWhenSaved;
        result = 31 * result + this.field_7_ftcAsci;
        result = 31 * result + this.field_8_ftcFE;
        result = 31 * result + this.field_9_ftcOther;
        return result;
    }
    
    public byte[] serialize() {
        final byte[] result = new byte[StshifAbstractType.getSize()];
        this.serialize(result, 0);
        return result;
    }
}
