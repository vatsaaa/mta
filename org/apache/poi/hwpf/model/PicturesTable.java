// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hwpf.model;

import org.apache.poi.util.POILogFactory;
import org.apache.poi.hwpf.usermodel.Range;
import java.util.ArrayList;
import org.apache.poi.ddf.EscherRecordFactory;
import java.util.Iterator;
import org.apache.poi.ddf.EscherBlipRecord;
import org.apache.poi.ddf.DefaultEscherRecordFactory;
import org.apache.poi.ddf.EscherBSERecord;
import org.apache.poi.ddf.EscherRecord;
import java.util.List;
import org.apache.poi.hwpf.usermodel.Picture;
import org.apache.poi.util.LittleEndian;
import org.apache.poi.hwpf.usermodel.CharacterRun;
import org.apache.poi.hwpf.HWPFDocument;
import org.apache.poi.util.POILogger;
import org.apache.poi.util.Internal;

@Internal
public final class PicturesTable
{
    private static final POILogger logger;
    static final int TYPE_IMAGE = 8;
    static final int TYPE_IMAGE_WORD2000 = 0;
    static final int TYPE_IMAGE_PASTED_FROM_CLIPBOARD = 10;
    static final int TYPE_IMAGE_PASTED_FROM_CLIPBOARD_WORD2000 = 2;
    static final int TYPE_HORIZONTAL_LINE = 14;
    static final int BLOCK_TYPE_OFFSET = 14;
    static final int MM_MODE_TYPE_OFFSET = 6;
    private HWPFDocument _document;
    private byte[] _dataStream;
    private byte[] _mainStream;
    @Deprecated
    private FSPATable _fspa;
    @Deprecated
    private EscherRecordHolder _dgg;
    
    @Deprecated
    public PicturesTable(final HWPFDocument _document, final byte[] _dataStream, final byte[] _mainStream, final FSPATable fspa, final EscherRecordHolder dgg) {
        this._document = _document;
        this._dataStream = _dataStream;
        this._mainStream = _mainStream;
        this._fspa = fspa;
        this._dgg = dgg;
    }
    
    public PicturesTable(final HWPFDocument _document, final byte[] _dataStream, final byte[] _mainStream) {
        this._document = _document;
        this._dataStream = _dataStream;
        this._mainStream = _mainStream;
    }
    
    public boolean hasPicture(final CharacterRun run) {
        return run != null && (run.isSpecialCharacter() && !run.isObj() && !run.isOle2() && !run.isData() && ("\u0001".equals(run.text()) || "\u0001\u0015".equals(run.text()))) && this.isBlockContainsImage(run.getPicOffset());
    }
    
    public boolean hasEscherPicture(final CharacterRun run) {
        return run.isSpecialCharacter() && !run.isObj() && !run.isOle2() && !run.isData() && run.text().startsWith("\b");
    }
    
    public boolean hasHorizontalLine(final CharacterRun run) {
        return run.isSpecialCharacter() && "\u0001".equals(run.text()) && this.isBlockContainsHorizontalLine(run.getPicOffset());
    }
    
    private boolean isPictureRecognized(final short blockType, final short mappingModeOfMETAFILEPICT) {
        return blockType == 8 || blockType == 10 || (blockType == 0 && mappingModeOfMETAFILEPICT == 100) || (blockType == 2 && mappingModeOfMETAFILEPICT == 100);
    }
    
    private static short getBlockType(final byte[] dataStream, final int pictOffset) {
        return LittleEndian.getShort(dataStream, pictOffset + 14);
    }
    
    private static short getMmMode(final byte[] dataStream, final int pictOffset) {
        return LittleEndian.getShort(dataStream, pictOffset + 6);
    }
    
    public Picture extractPicture(final CharacterRun run, final boolean fillBytes) {
        if (this.hasPicture(run)) {
            return new Picture(run.getPicOffset(), this._dataStream, fillBytes);
        }
        return null;
    }
    
    private void searchForPictures(final List<EscherRecord> escherRecords, final List<Picture> pictures) {
        for (final EscherRecord escherRecord : escherRecords) {
            if (escherRecord instanceof EscherBSERecord) {
                final EscherBSERecord bse = (EscherBSERecord)escherRecord;
                EscherBlipRecord blip = bse.getBlipRecord();
                if (blip != null) {
                    pictures.add(new Picture(blip));
                }
                else if (bse.getOffset() > 0) {
                    try {
                        final EscherRecordFactory recordFactory = new DefaultEscherRecordFactory();
                        final EscherRecord record = recordFactory.createRecord(this._mainStream, bse.getOffset());
                        if (record instanceof EscherBlipRecord) {
                            record.fillFields(this._mainStream, bse.getOffset(), recordFactory);
                            blip = (EscherBlipRecord)record;
                            pictures.add(new Picture(blip));
                        }
                    }
                    catch (Exception exc) {
                        PicturesTable.logger.log(POILogger.WARN, "Unable to load picture from BLIB record at offset #", bse.getOffset(), exc);
                    }
                }
            }
            this.searchForPictures(escherRecord.getChildRecords(), pictures);
        }
    }
    
    public List<Picture> getAllPictures() {
        final ArrayList<Picture> pictures = new ArrayList<Picture>();
        final Range range = this._document.getOverallRange();
        for (int i = 0; i < range.numCharacterRuns(); ++i) {
            final CharacterRun run = range.getCharacterRun(i);
            if (run != null) {
                final Picture picture = this.extractPicture(run, false);
                if (picture != null) {
                    pictures.add(picture);
                }
            }
        }
        this.searchForPictures(this._dgg.getEscherRecords(), pictures);
        return pictures;
    }
    
    private boolean isBlockContainsImage(final int i) {
        return this.isPictureRecognized(getBlockType(this._dataStream, i), getMmMode(this._dataStream, i));
    }
    
    private boolean isBlockContainsHorizontalLine(final int i) {
        return getBlockType(this._dataStream, i) == 14 && getMmMode(this._dataStream, i) == 100;
    }
    
    static {
        logger = POILogFactory.getLogger(PicturesTable.class);
    }
}
