// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hwpf.model;

import org.apache.poi.util.Internal;

@Internal
public final class GenericPropertyNode extends PropertyNode<GenericPropertyNode>
{
    public GenericPropertyNode(final int start, final int end, final byte[] buf) {
        super(start, end, buf);
    }
    
    public byte[] getBytes() {
        return (byte[])this._buf;
    }
    
    @Override
    public String toString() {
        return "GenericPropertyNode [" + this.getStart() + "; " + this.getEnd() + ") " + ((this.getBytes() != null) ? (this.getBytes().length + " byte(s)") : "null");
    }
}
