// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hwpf.model;

import org.apache.poi.hwpf.model.types.TBDAbstractType;

public class TabDescriptor extends TBDAbstractType
{
    public TabDescriptor() {
    }
    
    public TabDescriptor(final byte[] bytes, final int offset) {
        this.fillFields(bytes, offset);
    }
    
    public byte[] toByteArray() {
        final byte[] buf = new byte[TBDAbstractType.getSize()];
        this.serialize(buf, 0);
        return buf;
    }
}
