// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hwpf.model;

import java.util.Iterator;
import java.util.Comparator;
import java.util.List;
import java.util.Collections;
import org.apache.poi.util.LittleEndian;
import org.apache.poi.util.Internal;

@Internal
public final class OldCHPBinTable extends CHPBinTable
{
    public OldCHPBinTable(final byte[] documentStream, final int offset, final int size, final int fcMin, final TextPieceTable tpt) {
        final PlexOfCps binTable = new PlexOfCps(documentStream, offset, size, 2);
        for (int length = binTable.length(), x = 0; x < length; ++x) {
            final GenericPropertyNode node = binTable.getProperty(x);
            final int pageNum = LittleEndian.getUShort(node.getBytes());
            final int pageOffset = 512 * pageNum;
            final CHPFormattedDiskPage cfkp = new CHPFormattedDiskPage(documentStream, pageOffset, tpt);
            for (final CHPX chpx : cfkp.getCHPXs()) {
                if (chpx != null) {
                    this._textRuns.add(chpx);
                }
            }
        }
        Collections.sort(this._textRuns, PropertyNode.StartComparator.instance);
    }
}
