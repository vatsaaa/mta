// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hwpf.model;

import org.apache.poi.util.POILogFactory;
import java.io.IOException;
import org.apache.poi.hwpf.model.io.HWPFOutputStream;
import java.util.Arrays;
import org.apache.poi.util.LittleEndian;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import org.apache.poi.util.POILogger;
import org.apache.poi.util.Internal;

@Internal
public final class FIBFieldHandler
{
    public static final int STSHFORIG = 0;
    public static final int STSHF = 1;
    public static final int PLCFFNDREF = 2;
    public static final int PLCFFNDTXT = 3;
    public static final int PLCFANDREF = 4;
    public static final int PLCFANDTXT = 5;
    public static final int PLCFSED = 6;
    public static final int PLCFPAD = 7;
    public static final int PLCFPHE = 8;
    public static final int STTBGLSY = 9;
    public static final int PLCFGLSY = 10;
    public static final int PLCFHDD = 11;
    public static final int PLCFBTECHPX = 12;
    public static final int PLCFBTEPAPX = 13;
    public static final int PLCFSEA = 14;
    public static final int STTBFFFN = 15;
    public static final int PLCFFLDMOM = 16;
    public static final int PLCFFLDHDR = 17;
    public static final int PLCFFLDFTN = 18;
    public static final int PLCFFLDATN = 19;
    public static final int PLCFFLDMCR = 20;
    public static final int STTBFBKMK = 21;
    public static final int PLCFBKF = 22;
    public static final int PLCFBKL = 23;
    public static final int CMDS = 24;
    public static final int PLCMCR = 25;
    public static final int STTBFMCR = 26;
    public static final int PRDRVR = 27;
    public static final int PRENVPORT = 28;
    public static final int PRENVLAND = 29;
    public static final int WSS = 30;
    public static final int DOP = 31;
    public static final int STTBFASSOC = 32;
    public static final int CLX = 33;
    public static final int PLCFPGDFTN = 34;
    public static final int AUTOSAVESOURCE = 35;
    public static final int GRPXSTATNOWNERS = 36;
    public static final int STTBFATNBKMK = 37;
    public static final int PLCFDOAMOM = 38;
    public static final int PLCDOAHDR = 39;
    public static final int PLCSPAMOM = 40;
    public static final int PLCSPAHDR = 41;
    public static final int PLCFATNBKF = 42;
    public static final int PLCFATNBKL = 43;
    public static final int PMS = 44;
    public static final int FORMFLDSTTBS = 45;
    public static final int PLCFENDREF = 46;
    public static final int PLCFENDTXT = 47;
    public static final int PLCFFLDEDN = 48;
    public static final int PLCFPGDEDN = 49;
    public static final int DGGINFO = 50;
    public static final int STTBFRMARK = 51;
    public static final int STTBCAPTION = 52;
    public static final int STTBAUTOCAPTION = 53;
    public static final int PLCFWKB = 54;
    public static final int PLCFSPL = 55;
    public static final int PLCFTXBXTXT = 56;
    public static final int PLCFFLDTXBX = 57;
    public static final int PLCFHDRTXBXTXT = 58;
    public static final int PLCFFLDHDRTXBX = 59;
    public static final int STWUSER = 60;
    public static final int STTBTTMBD = 61;
    public static final int UNUSED = 62;
    public static final int PGDMOTHER = 63;
    public static final int BKDMOTHER = 64;
    public static final int PGDFTN = 65;
    public static final int BKDFTN = 66;
    public static final int PGDEDN = 67;
    public static final int BKDEDN = 68;
    public static final int STTBFINTFLD = 69;
    public static final int ROUTESLIP = 70;
    public static final int STTBSAVEDBY = 71;
    public static final int STTBFNM = 72;
    public static final int PLCFLST = 73;
    public static final int PLFLFO = 74;
    public static final int PLCFTXBXBKD = 75;
    public static final int PLCFTXBXHDRBKD = 76;
    public static final int DOCUNDO = 77;
    public static final int RGBUSE = 78;
    public static final int USP = 79;
    public static final int USKF = 80;
    public static final int PLCUPCRGBUSE = 81;
    public static final int PLCUPCUSP = 82;
    public static final int STTBGLSYSTYLE = 83;
    public static final int PLGOSL = 84;
    public static final int PLCOCX = 85;
    public static final int PLCFBTELVC = 86;
    public static final int MODIFIED = 87;
    public static final int PLCFLVC = 88;
    public static final int PLCASUMY = 89;
    public static final int PLCFGRAM = 90;
    public static final int STTBLISTNAMES = 91;
    public static final int STTBFUSSR = 92;
    private static POILogger log;
    private static final int FIELD_SIZE = 8;
    private Map<Integer, UnhandledDataStructure> _unknownMap;
    private int[] _fields;
    
    FIBFieldHandler(final byte[] mainStream, final int offset, final int cbRgFcLcb, final byte[] tableStream, final HashSet<Integer> offsetList, final boolean areKnown) {
        this._unknownMap = new HashMap<Integer, UnhandledDataStructure>();
        this._fields = new int[cbRgFcLcb * 2];
        for (int x = 0; x < cbRgFcLcb; ++x) {
            int fieldOffset = x * 8 + offset;
            final int dsOffset = LittleEndian.getInt(mainStream, fieldOffset);
            fieldOffset += 4;
            final int dsSize = LittleEndian.getInt(mainStream, fieldOffset);
            if ((offsetList.contains(x) ^ areKnown) && dsSize > 0) {
                if (dsOffset + dsSize > tableStream.length) {
                    FIBFieldHandler.log.log(POILogger.WARN, "Unhandled data structure points to outside the buffer. offset = " + dsOffset + ", length = " + dsSize + ", buffer length = " + tableStream.length);
                }
                else {
                    final UnhandledDataStructure unhandled = new UnhandledDataStructure(tableStream, dsOffset, dsSize);
                    this._unknownMap.put(x, unhandled);
                }
            }
            this._fields[x * 2] = dsOffset;
            this._fields[x * 2 + 1] = dsSize;
        }
    }
    
    public void clearFields() {
        Arrays.fill(this._fields, 0);
    }
    
    public int getFieldOffset(final int field) {
        return this._fields[field * 2];
    }
    
    public int getFieldSize(final int field) {
        return this._fields[field * 2 + 1];
    }
    
    public void setFieldOffset(final int field, final int offset) {
        this._fields[field * 2] = offset;
    }
    
    public void setFieldSize(final int field, final int size) {
        this._fields[field * 2 + 1] = size;
    }
    
    public int sizeInBytes() {
        return this._fields.length * 4;
    }
    
    public int getFieldsCount() {
        return this._fields.length / 2;
    }
    
    void writeTo(final byte[] mainStream, int offset, final HWPFOutputStream tableStream) throws IOException {
        for (int x = 0; x < this._fields.length / 2; ++x) {
            final UnhandledDataStructure ds = this._unknownMap.get(x);
            if (ds != null) {
                this._fields[x * 2] = tableStream.getOffset();
                LittleEndian.putInt(mainStream, offset, tableStream.getOffset());
                offset += 4;
                final byte[] buf = ds.getBuf();
                tableStream.write(buf);
                this._fields[x * 2 + 1] = buf.length;
                LittleEndian.putInt(mainStream, offset, buf.length);
                offset += 4;
            }
            else {
                LittleEndian.putInt(mainStream, offset, this._fields[x * 2]);
                offset += 4;
                LittleEndian.putInt(mainStream, offset, this._fields[x * 2 + 1]);
                offset += 4;
            }
        }
    }
    
    private static String leftPad(final String text, final int value, final char padChar) {
        if (text.length() >= value) {
            return text;
        }
        final StringBuilder result = new StringBuilder();
        for (int i = 0; i < value - text.length(); ++i) {
            result.append(padChar);
        }
        result.append(text);
        return result.toString();
    }
    
    @Override
    public String toString() {
        final StringBuilder result = new StringBuilder();
        result.append("[FIBFieldHandler]:\n");
        result.append("\tFields:\n");
        result.append("\t");
        result.append(leftPad("Index", 8, ' '));
        result.append(leftPad("FIB offset", 15, ' '));
        result.append(leftPad("Offset", 8, ' '));
        result.append(leftPad("Size", 8, ' '));
        result.append('\n');
        for (int x = 0; x < this._fields.length / 2; ++x) {
            result.append('\t');
            result.append(leftPad(Integer.toString(x), 8, ' '));
            result.append(leftPad(Integer.toString(154 + x * 4 * 2), 6, ' '));
            result.append("   0x");
            result.append(leftPad(Integer.toHexString(154 + x * 4 * 2), 4, '0'));
            result.append(leftPad(Integer.toString(this.getFieldOffset(x)), 8, ' '));
            result.append(leftPad(Integer.toString(this.getFieldSize(x)), 8, ' '));
            final UnhandledDataStructure structure = this._unknownMap.get(x);
            if (structure != null) {
                result.append(" => Unknown structure of size ");
                result.append(structure._buf.length);
            }
            result.append('\n');
        }
        return result.toString();
    }
    
    static {
        FIBFieldHandler.log = POILogFactory.getLogger(FIBFieldHandler.class);
    }
}
