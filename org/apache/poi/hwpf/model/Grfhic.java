// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hwpf.model;

import org.apache.poi.util.Internal;
import org.apache.poi.hwpf.model.types.GrfhicAbstractType;

@Internal
public class Grfhic extends GrfhicAbstractType
{
    public Grfhic() {
    }
    
    public Grfhic(final byte[] bytes, final int offset) {
        this.fillFields(bytes, offset);
    }
    
    public byte[] toByteArray() {
        final byte[] buf = new byte[GrfhicAbstractType.getSize()];
        this.serialize(buf, 0);
        return buf;
    }
}
