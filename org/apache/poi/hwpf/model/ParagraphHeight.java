// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hwpf.model;

import java.io.IOException;
import java.io.OutputStream;
import org.apache.poi.util.LittleEndian;
import org.apache.poi.util.BitFieldFactory;
import org.apache.poi.util.BitField;
import org.apache.poi.util.Internal;

@Internal
public final class ParagraphHeight
{
    private short infoField;
    private BitField fSpare;
    private BitField fUnk;
    private BitField fDiffLines;
    private BitField clMac;
    private short reserved;
    private int dxaCol;
    private int dymLineOrHeight;
    
    public ParagraphHeight(final byte[] buf, int offset) {
        this.fSpare = BitFieldFactory.getInstance(1);
        this.fUnk = BitFieldFactory.getInstance(2);
        this.fDiffLines = BitFieldFactory.getInstance(4);
        this.clMac = BitFieldFactory.getInstance(65280);
        this.infoField = LittleEndian.getShort(buf, offset);
        offset += 2;
        this.reserved = LittleEndian.getShort(buf, offset);
        offset += 2;
        this.dxaCol = LittleEndian.getInt(buf, offset);
        offset += 4;
        this.dymLineOrHeight = LittleEndian.getInt(buf, offset);
    }
    
    public ParagraphHeight() {
        this.fSpare = BitFieldFactory.getInstance(1);
        this.fUnk = BitFieldFactory.getInstance(2);
        this.fDiffLines = BitFieldFactory.getInstance(4);
        this.clMac = BitFieldFactory.getInstance(65280);
    }
    
    public void write(final OutputStream out) throws IOException {
        out.write(this.toByteArray());
    }
    
    protected byte[] toByteArray() {
        final byte[] buf = new byte[12];
        int offset = 0;
        LittleEndian.putShort(buf, offset, this.infoField);
        offset += 2;
        LittleEndian.putShort(buf, offset, this.reserved);
        offset += 2;
        LittleEndian.putInt(buf, offset, this.dxaCol);
        offset += 4;
        LittleEndian.putInt(buf, offset, this.dymLineOrHeight);
        return buf;
    }
    
    @Override
    public boolean equals(final Object o) {
        final ParagraphHeight ph = (ParagraphHeight)o;
        return this.infoField == ph.infoField && this.reserved == ph.reserved && this.dxaCol == ph.dxaCol && this.dymLineOrHeight == ph.dymLineOrHeight;
    }
}
