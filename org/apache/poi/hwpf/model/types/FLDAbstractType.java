// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hwpf.model.types;

import org.apache.poi.util.BitField;
import org.apache.poi.util.Internal;
import org.apache.poi.hdf.model.hdftypes.HDFType;

@Internal
public abstract class FLDAbstractType implements HDFType
{
    protected byte field_1_chHolder;
    private static BitField ch;
    private static BitField reserved;
    protected byte field_2_flt;
    private static BitField fDiffer;
    private static BitField fZombieEmbed;
    private static BitField fResultDirty;
    private static BitField fResultEdited;
    private static BitField fLocked;
    private static BitField fPrivateResult;
    private static BitField fNested;
    private static BitField fHasSep;
    
    protected void fillFields(final byte[] data, final int offset) {
        this.field_1_chHolder = data[0 + offset];
        this.field_2_flt = data[1 + offset];
    }
    
    public void serialize(final byte[] data, final int offset) {
        data[0 + offset] = this.field_1_chHolder;
        data[1 + offset] = this.field_2_flt;
    }
    
    @Override
    public String toString() {
        final StringBuffer buffer = new StringBuffer();
        buffer.append("[FLD]\n");
        buffer.append("    .chHolder             = ");
        buffer.append(" (").append(this.getChHolder()).append(" )\n");
        buffer.append("         .ch                       = ").append(this.getCh()).append('\n');
        buffer.append("         .reserved                 = ").append(this.getReserved()).append('\n');
        buffer.append("    .flt                  = ");
        buffer.append(" (").append(this.getFlt()).append(" )\n");
        buffer.append("         .fDiffer                  = ").append(this.isFDiffer()).append('\n');
        buffer.append("         .fZombieEmbed             = ").append(this.isFZombieEmbed()).append('\n');
        buffer.append("         .fResultDirty             = ").append(this.isFResultDirty()).append('\n');
        buffer.append("         .fResultEdited            = ").append(this.isFResultEdited()).append('\n');
        buffer.append("         .fLocked                  = ").append(this.isFLocked()).append('\n');
        buffer.append("         .fPrivateResult           = ").append(this.isFPrivateResult()).append('\n');
        buffer.append("         .fNested                  = ").append(this.isFNested()).append('\n');
        buffer.append("         .fHasSep                  = ").append(this.isFHasSep()).append('\n');
        buffer.append("[/FLD]\n");
        return buffer.toString();
    }
    
    public static int getSize() {
        return 6;
    }
    
    public byte getChHolder() {
        return this.field_1_chHolder;
    }
    
    public void setChHolder(final byte field_1_chHolder) {
        this.field_1_chHolder = field_1_chHolder;
    }
    
    public byte getFlt() {
        return this.field_2_flt;
    }
    
    public void setFlt(final byte field_2_flt) {
        this.field_2_flt = field_2_flt;
    }
    
    public void setCh(final byte value) {
        this.field_1_chHolder = (byte)FLDAbstractType.ch.setValue(this.field_1_chHolder, value);
    }
    
    public byte getCh() {
        return (byte)FLDAbstractType.ch.getValue(this.field_1_chHolder);
    }
    
    public void setReserved(final byte value) {
        this.field_1_chHolder = (byte)FLDAbstractType.reserved.setValue(this.field_1_chHolder, value);
    }
    
    public byte getReserved() {
        return (byte)FLDAbstractType.reserved.getValue(this.field_1_chHolder);
    }
    
    public void setFDiffer(final boolean value) {
        this.field_2_flt = (byte)FLDAbstractType.fDiffer.setBoolean(this.field_2_flt, value);
    }
    
    public boolean isFDiffer() {
        return FLDAbstractType.fDiffer.isSet(this.field_2_flt);
    }
    
    public void setFZombieEmbed(final boolean value) {
        this.field_2_flt = (byte)FLDAbstractType.fZombieEmbed.setBoolean(this.field_2_flt, value);
    }
    
    public boolean isFZombieEmbed() {
        return FLDAbstractType.fZombieEmbed.isSet(this.field_2_flt);
    }
    
    public void setFResultDirty(final boolean value) {
        this.field_2_flt = (byte)FLDAbstractType.fResultDirty.setBoolean(this.field_2_flt, value);
    }
    
    public boolean isFResultDirty() {
        return FLDAbstractType.fResultDirty.isSet(this.field_2_flt);
    }
    
    public void setFResultEdited(final boolean value) {
        this.field_2_flt = (byte)FLDAbstractType.fResultEdited.setBoolean(this.field_2_flt, value);
    }
    
    public boolean isFResultEdited() {
        return FLDAbstractType.fResultEdited.isSet(this.field_2_flt);
    }
    
    public void setFLocked(final boolean value) {
        this.field_2_flt = (byte)FLDAbstractType.fLocked.setBoolean(this.field_2_flt, value);
    }
    
    public boolean isFLocked() {
        return FLDAbstractType.fLocked.isSet(this.field_2_flt);
    }
    
    public void setFPrivateResult(final boolean value) {
        this.field_2_flt = (byte)FLDAbstractType.fPrivateResult.setBoolean(this.field_2_flt, value);
    }
    
    public boolean isFPrivateResult() {
        return FLDAbstractType.fPrivateResult.isSet(this.field_2_flt);
    }
    
    public void setFNested(final boolean value) {
        this.field_2_flt = (byte)FLDAbstractType.fNested.setBoolean(this.field_2_flt, value);
    }
    
    public boolean isFNested() {
        return FLDAbstractType.fNested.isSet(this.field_2_flt);
    }
    
    public void setFHasSep(final boolean value) {
        this.field_2_flt = (byte)FLDAbstractType.fHasSep.setBoolean(this.field_2_flt, value);
    }
    
    public boolean isFHasSep() {
        return FLDAbstractType.fHasSep.isSet(this.field_2_flt);
    }
    
    static {
        FLDAbstractType.ch = new BitField(31);
        FLDAbstractType.reserved = new BitField(224);
        FLDAbstractType.fDiffer = new BitField(1);
        FLDAbstractType.fZombieEmbed = new BitField(2);
        FLDAbstractType.fResultDirty = new BitField(4);
        FLDAbstractType.fResultEdited = new BitField(8);
        FLDAbstractType.fLocked = new BitField(16);
        FLDAbstractType.fPrivateResult = new BitField(32);
        FLDAbstractType.fNested = new BitField(64);
        FLDAbstractType.fHasSep = new BitField(64);
    }
}
