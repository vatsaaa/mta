// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hwpf.model.types;

import org.apache.poi.util.LittleEndian;
import org.apache.poi.util.Internal;

@Internal
public abstract class FRDAbstractType
{
    protected short field_1_nAuto;
    
    protected FRDAbstractType() {
    }
    
    protected void fillFields(final byte[] data, final int offset) {
        this.field_1_nAuto = LittleEndian.getShort(data, 0 + offset);
    }
    
    public void serialize(final byte[] data, final int offset) {
        LittleEndian.putShort(data, 0 + offset, this.field_1_nAuto);
    }
    
    public static int getSize() {
        return 2;
    }
    
    @Override
    public String toString() {
        final StringBuilder builder = new StringBuilder();
        builder.append("[FRD]\n");
        builder.append("    .nAuto                = ");
        builder.append(" (").append(this.getNAuto()).append(" )\n");
        builder.append("[/FRD]\n");
        return builder.toString();
    }
    
    public short getNAuto() {
        return this.field_1_nAuto;
    }
    
    public void setNAuto(final short field_1_nAuto) {
        this.field_1_nAuto = field_1_nAuto;
    }
}
