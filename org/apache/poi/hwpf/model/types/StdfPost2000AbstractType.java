// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hwpf.model.types;

import org.apache.poi.util.LittleEndian;
import org.apache.poi.util.BitField;
import org.apache.poi.util.Internal;

@Internal
public abstract class StdfPost2000AbstractType
{
    protected short field_1_info1;
    private static final BitField istdLink;
    private static final BitField fHasOriginalStyle;
    private static final BitField fSpare;
    protected long field_2_rsid;
    protected short field_3_info3;
    private static final BitField iftcHtml;
    private static final BitField unused;
    private static final BitField iPriority;
    
    protected StdfPost2000AbstractType() {
    }
    
    protected void fillFields(final byte[] data, final int offset) {
        this.field_1_info1 = LittleEndian.getShort(data, 0 + offset);
        this.field_2_rsid = LittleEndian.getUInt(data, 2 + offset);
        this.field_3_info3 = LittleEndian.getShort(data, 6 + offset);
    }
    
    public void serialize(final byte[] data, final int offset) {
        LittleEndian.putShort(data, 0 + offset, this.field_1_info1);
        LittleEndian.putUInt(data, 2 + offset, this.field_2_rsid);
        LittleEndian.putShort(data, 6 + offset, this.field_3_info3);
    }
    
    public static int getSize() {
        return 8;
    }
    
    @Override
    public String toString() {
        final StringBuilder builder = new StringBuilder();
        builder.append("[StdfPost2000]\n");
        builder.append("    .info1                = ");
        builder.append(" (").append(this.getInfo1()).append(" )\n");
        builder.append("         .istdLink                 = ").append(this.getIstdLink()).append('\n');
        builder.append("         .fHasOriginalStyle        = ").append(this.isFHasOriginalStyle()).append('\n');
        builder.append("         .fSpare                   = ").append(this.getFSpare()).append('\n');
        builder.append("    .rsid                 = ");
        builder.append(" (").append(this.getRsid()).append(" )\n");
        builder.append("    .info3                = ");
        builder.append(" (").append(this.getInfo3()).append(" )\n");
        builder.append("         .iftcHtml                 = ").append(this.getIftcHtml()).append('\n');
        builder.append("         .unused                   = ").append(this.isUnused()).append('\n');
        builder.append("         .iPriority                = ").append(this.getIPriority()).append('\n');
        builder.append("[/StdfPost2000]\n");
        return builder.toString();
    }
    
    @Internal
    public short getInfo1() {
        return this.field_1_info1;
    }
    
    @Internal
    public void setInfo1(final short field_1_info1) {
        this.field_1_info1 = field_1_info1;
    }
    
    @Internal
    public long getRsid() {
        return this.field_2_rsid;
    }
    
    @Internal
    public void setRsid(final long field_2_rsid) {
        this.field_2_rsid = field_2_rsid;
    }
    
    @Internal
    public short getInfo3() {
        return this.field_3_info3;
    }
    
    @Internal
    public void setInfo3(final short field_3_info3) {
        this.field_3_info3 = field_3_info3;
    }
    
    @Internal
    public void setIstdLink(final short value) {
        this.field_1_info1 = (short)StdfPost2000AbstractType.istdLink.setValue(this.field_1_info1, value);
    }
    
    @Internal
    public short getIstdLink() {
        return (short)StdfPost2000AbstractType.istdLink.getValue(this.field_1_info1);
    }
    
    @Internal
    public void setFHasOriginalStyle(final boolean value) {
        this.field_1_info1 = (short)StdfPost2000AbstractType.fHasOriginalStyle.setBoolean(this.field_1_info1, value);
    }
    
    @Internal
    public boolean isFHasOriginalStyle() {
        return StdfPost2000AbstractType.fHasOriginalStyle.isSet(this.field_1_info1);
    }
    
    @Internal
    public void setFSpare(final byte value) {
        this.field_1_info1 = (short)StdfPost2000AbstractType.fSpare.setValue(this.field_1_info1, value);
    }
    
    @Internal
    public byte getFSpare() {
        return (byte)StdfPost2000AbstractType.fSpare.getValue(this.field_1_info1);
    }
    
    @Internal
    public void setIftcHtml(final byte value) {
        this.field_3_info3 = (short)StdfPost2000AbstractType.iftcHtml.setValue(this.field_3_info3, value);
    }
    
    @Internal
    public byte getIftcHtml() {
        return (byte)StdfPost2000AbstractType.iftcHtml.getValue(this.field_3_info3);
    }
    
    @Internal
    public void setUnused(final boolean value) {
        this.field_3_info3 = (short)StdfPost2000AbstractType.unused.setBoolean(this.field_3_info3, value);
    }
    
    @Internal
    public boolean isUnused() {
        return StdfPost2000AbstractType.unused.isSet(this.field_3_info3);
    }
    
    @Internal
    public void setIPriority(final short value) {
        this.field_3_info3 = (short)StdfPost2000AbstractType.iPriority.setValue(this.field_3_info3, value);
    }
    
    @Internal
    public short getIPriority() {
        return (short)StdfPost2000AbstractType.iPriority.getValue(this.field_3_info3);
    }
    
    static {
        istdLink = new BitField(4095);
        fHasOriginalStyle = new BitField(4096);
        fSpare = new BitField(57344);
        iftcHtml = new BitField(7);
        unused = new BitField(8);
        iPriority = new BitField(65520);
    }
}
