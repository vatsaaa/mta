// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hwpf.model.types;

import org.apache.poi.util.Internal;

@Internal
public abstract class HRESIAbstractType
{
    protected byte field_1_hres;
    public static final byte HRES_NO = 0;
    public static final byte HRES_NORMAL = 1;
    public static final byte HRES_ADD_LETTER_BEFORE = 2;
    public static final byte HRES_CHANGE_LETTER_BEFORE = 3;
    public static final byte HRES_DELETE_LETTER_BEFORE = 4;
    public static final byte HRES_CHANGE_LETTER_AFTER = 5;
    public static final byte HRES_DELETE_BEFORE_CHANGE_BEFORE = 6;
    protected byte field_2_chHres;
    
    protected HRESIAbstractType() {
    }
    
    protected void fillFields(final byte[] data, final int offset) {
        this.field_1_hres = data[0 + offset];
        this.field_2_chHres = data[1 + offset];
    }
    
    public void serialize(final byte[] data, final int offset) {
        data[0 + offset] = this.field_1_hres;
        data[1 + offset] = this.field_2_chHres;
    }
    
    public static int getSize() {
        return 6;
    }
    
    @Override
    public String toString() {
        final StringBuilder builder = new StringBuilder();
        builder.append("[HRESI]\n");
        builder.append("    .hres                 = ");
        builder.append(" (").append(this.getHres()).append(" )\n");
        builder.append("    .chHres               = ");
        builder.append(" (").append(this.getChHres()).append(" )\n");
        builder.append("[/HRESI]\n");
        return builder.toString();
    }
    
    public byte getHres() {
        return this.field_1_hres;
    }
    
    public void setHres(final byte field_1_hres) {
        this.field_1_hres = field_1_hres;
    }
    
    public byte getChHres() {
        return this.field_2_chHres;
    }
    
    public void setChHres(final byte field_2_chHres) {
        this.field_2_chHres = field_2_chHres;
    }
}
