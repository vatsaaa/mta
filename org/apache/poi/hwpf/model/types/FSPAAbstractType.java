// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hwpf.model.types;

import org.apache.poi.util.LittleEndian;
import org.apache.poi.util.BitField;
import org.apache.poi.util.Internal;

@Internal
public abstract class FSPAAbstractType
{
    protected int field_1_spid;
    protected int field_2_xaLeft;
    protected int field_3_yaTop;
    protected int field_4_xaRight;
    protected int field_5_yaBottom;
    protected short field_6_flags;
    private static BitField fHdr;
    private static BitField bx;
    private static BitField by;
    private static BitField wr;
    private static BitField wrk;
    private static BitField fRcaSimple;
    private static BitField fBelowText;
    private static BitField fAnchorLock;
    protected int field_7_cTxbx;
    
    protected FSPAAbstractType() {
    }
    
    protected void fillFields(final byte[] data, final int offset) {
        this.field_1_spid = LittleEndian.getInt(data, 0 + offset);
        this.field_2_xaLeft = LittleEndian.getInt(data, 4 + offset);
        this.field_3_yaTop = LittleEndian.getInt(data, 8 + offset);
        this.field_4_xaRight = LittleEndian.getInt(data, 12 + offset);
        this.field_5_yaBottom = LittleEndian.getInt(data, 16 + offset);
        this.field_6_flags = LittleEndian.getShort(data, 20 + offset);
        this.field_7_cTxbx = LittleEndian.getInt(data, 22 + offset);
    }
    
    public void serialize(final byte[] data, final int offset) {
        LittleEndian.putInt(data, 0 + offset, this.field_1_spid);
        LittleEndian.putInt(data, 4 + offset, this.field_2_xaLeft);
        LittleEndian.putInt(data, 8 + offset, this.field_3_yaTop);
        LittleEndian.putInt(data, 12 + offset, this.field_4_xaRight);
        LittleEndian.putInt(data, 16 + offset, this.field_5_yaBottom);
        LittleEndian.putShort(data, 20 + offset, this.field_6_flags);
        LittleEndian.putInt(data, 22 + offset, this.field_7_cTxbx);
    }
    
    public static int getSize() {
        return 26;
    }
    
    @Override
    public String toString() {
        final StringBuilder builder = new StringBuilder();
        builder.append("[FSPA]\n");
        builder.append("    .spid                 = ");
        builder.append(" (").append(this.getSpid()).append(" )\n");
        builder.append("    .xaLeft               = ");
        builder.append(" (").append(this.getXaLeft()).append(" )\n");
        builder.append("    .yaTop                = ");
        builder.append(" (").append(this.getYaTop()).append(" )\n");
        builder.append("    .xaRight              = ");
        builder.append(" (").append(this.getXaRight()).append(" )\n");
        builder.append("    .yaBottom             = ");
        builder.append(" (").append(this.getYaBottom()).append(" )\n");
        builder.append("    .flags                = ");
        builder.append(" (").append(this.getFlags()).append(" )\n");
        builder.append("         .fHdr                     = ").append(this.isFHdr()).append('\n');
        builder.append("         .bx                       = ").append(this.getBx()).append('\n');
        builder.append("         .by                       = ").append(this.getBy()).append('\n');
        builder.append("         .wr                       = ").append(this.getWr()).append('\n');
        builder.append("         .wrk                      = ").append(this.getWrk()).append('\n');
        builder.append("         .fRcaSimple               = ").append(this.isFRcaSimple()).append('\n');
        builder.append("         .fBelowText               = ").append(this.isFBelowText()).append('\n');
        builder.append("         .fAnchorLock              = ").append(this.isFAnchorLock()).append('\n');
        builder.append("    .cTxbx                = ");
        builder.append(" (").append(this.getCTxbx()).append(" )\n");
        builder.append("[/FSPA]\n");
        return builder.toString();
    }
    
    @Internal
    public int getSpid() {
        return this.field_1_spid;
    }
    
    @Internal
    public void setSpid(final int field_1_spid) {
        this.field_1_spid = field_1_spid;
    }
    
    @Internal
    public int getXaLeft() {
        return this.field_2_xaLeft;
    }
    
    @Internal
    public void setXaLeft(final int field_2_xaLeft) {
        this.field_2_xaLeft = field_2_xaLeft;
    }
    
    @Internal
    public int getYaTop() {
        return this.field_3_yaTop;
    }
    
    @Internal
    public void setYaTop(final int field_3_yaTop) {
        this.field_3_yaTop = field_3_yaTop;
    }
    
    @Internal
    public int getXaRight() {
        return this.field_4_xaRight;
    }
    
    @Internal
    public void setXaRight(final int field_4_xaRight) {
        this.field_4_xaRight = field_4_xaRight;
    }
    
    @Internal
    public int getYaBottom() {
        return this.field_5_yaBottom;
    }
    
    @Internal
    public void setYaBottom(final int field_5_yaBottom) {
        this.field_5_yaBottom = field_5_yaBottom;
    }
    
    @Internal
    public short getFlags() {
        return this.field_6_flags;
    }
    
    @Internal
    public void setFlags(final short field_6_flags) {
        this.field_6_flags = field_6_flags;
    }
    
    @Internal
    public int getCTxbx() {
        return this.field_7_cTxbx;
    }
    
    @Internal
    public void setCTxbx(final int field_7_cTxbx) {
        this.field_7_cTxbx = field_7_cTxbx;
    }
    
    @Internal
    public void setFHdr(final boolean value) {
        this.field_6_flags = (short)FSPAAbstractType.fHdr.setBoolean(this.field_6_flags, value);
    }
    
    @Internal
    public boolean isFHdr() {
        return FSPAAbstractType.fHdr.isSet(this.field_6_flags);
    }
    
    @Internal
    public void setBx(final byte value) {
        this.field_6_flags = (short)FSPAAbstractType.bx.setValue(this.field_6_flags, value);
    }
    
    @Internal
    public byte getBx() {
        return (byte)FSPAAbstractType.bx.getValue(this.field_6_flags);
    }
    
    @Internal
    public void setBy(final byte value) {
        this.field_6_flags = (short)FSPAAbstractType.by.setValue(this.field_6_flags, value);
    }
    
    @Internal
    public byte getBy() {
        return (byte)FSPAAbstractType.by.getValue(this.field_6_flags);
    }
    
    @Internal
    public void setWr(final byte value) {
        this.field_6_flags = (short)FSPAAbstractType.wr.setValue(this.field_6_flags, value);
    }
    
    @Internal
    public byte getWr() {
        return (byte)FSPAAbstractType.wr.getValue(this.field_6_flags);
    }
    
    @Internal
    public void setWrk(final byte value) {
        this.field_6_flags = (short)FSPAAbstractType.wrk.setValue(this.field_6_flags, value);
    }
    
    @Internal
    public byte getWrk() {
        return (byte)FSPAAbstractType.wrk.getValue(this.field_6_flags);
    }
    
    @Internal
    public void setFRcaSimple(final boolean value) {
        this.field_6_flags = (short)FSPAAbstractType.fRcaSimple.setBoolean(this.field_6_flags, value);
    }
    
    @Internal
    public boolean isFRcaSimple() {
        return FSPAAbstractType.fRcaSimple.isSet(this.field_6_flags);
    }
    
    @Internal
    public void setFBelowText(final boolean value) {
        this.field_6_flags = (short)FSPAAbstractType.fBelowText.setBoolean(this.field_6_flags, value);
    }
    
    @Internal
    public boolean isFBelowText() {
        return FSPAAbstractType.fBelowText.isSet(this.field_6_flags);
    }
    
    @Internal
    public void setFAnchorLock(final boolean value) {
        this.field_6_flags = (short)FSPAAbstractType.fAnchorLock.setBoolean(this.field_6_flags, value);
    }
    
    @Internal
    public boolean isFAnchorLock() {
        return FSPAAbstractType.fAnchorLock.isSet(this.field_6_flags);
    }
    
    static {
        FSPAAbstractType.fHdr = new BitField(1);
        FSPAAbstractType.bx = new BitField(6);
        FSPAAbstractType.by = new BitField(24);
        FSPAAbstractType.wr = new BitField(480);
        FSPAAbstractType.wrk = new BitField(7680);
        FSPAAbstractType.fRcaSimple = new BitField(8192);
        FSPAAbstractType.fBelowText = new BitField(16384);
        FSPAAbstractType.fAnchorLock = new BitField(32768);
    }
}
