// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hwpf.model.types;

import org.apache.poi.util.LittleEndian;
import org.apache.poi.util.BitField;
import org.apache.poi.util.Internal;

@Internal
public abstract class StdfBaseAbstractType
{
    protected short field_1_info1;
    private static final BitField sti;
    private static final BitField fScratch;
    private static final BitField fInvalHeight;
    private static final BitField fHasUpe;
    private static final BitField fMassCopy;
    protected short field_2_info2;
    private static final BitField stk;
    private static final BitField istdBase;
    protected short field_3_info3;
    private static final BitField cupx;
    private static final BitField istdNext;
    protected int field_4_bchUpe;
    protected short field_5_grfstd;
    private static final BitField fAutoRedef;
    private static final BitField fHidden;
    private static final BitField f97LidsSet;
    private static final BitField fCopyLang;
    private static final BitField fPersonalCompose;
    private static final BitField fPersonalReply;
    private static final BitField fPersonal;
    private static final BitField fNoHtmlExport;
    private static final BitField fSemiHidden;
    private static final BitField fLocked;
    private static final BitField fInternalUse;
    private static final BitField fUnhideWhenUsed;
    private static final BitField fQFormat;
    private static final BitField fReserved;
    
    protected StdfBaseAbstractType() {
    }
    
    protected void fillFields(final byte[] data, final int offset) {
        this.field_1_info1 = LittleEndian.getShort(data, 0 + offset);
        this.field_2_info2 = LittleEndian.getShort(data, 2 + offset);
        this.field_3_info3 = LittleEndian.getShort(data, 4 + offset);
        this.field_4_bchUpe = LittleEndian.getShort(data, 6 + offset);
        this.field_5_grfstd = LittleEndian.getShort(data, 8 + offset);
    }
    
    public void serialize(final byte[] data, final int offset) {
        LittleEndian.putShort(data, 0 + offset, this.field_1_info1);
        LittleEndian.putShort(data, 2 + offset, this.field_2_info2);
        LittleEndian.putShort(data, 4 + offset, this.field_3_info3);
        LittleEndian.putUShort(data, 6 + offset, this.field_4_bchUpe);
        LittleEndian.putShort(data, 8 + offset, this.field_5_grfstd);
    }
    
    public static int getSize() {
        return 10;
    }
    
    @Override
    public String toString() {
        final StringBuilder builder = new StringBuilder();
        builder.append("[StdfBase]\n");
        builder.append("    .info1                = ");
        builder.append(" (").append(this.getInfo1()).append(" )\n");
        builder.append("         .sti                      = ").append(this.getSti()).append('\n');
        builder.append("         .fScratch                 = ").append(this.isFScratch()).append('\n');
        builder.append("         .fInvalHeight             = ").append(this.isFInvalHeight()).append('\n');
        builder.append("         .fHasUpe                  = ").append(this.isFHasUpe()).append('\n');
        builder.append("         .fMassCopy                = ").append(this.isFMassCopy()).append('\n');
        builder.append("    .info2                = ");
        builder.append(" (").append(this.getInfo2()).append(" )\n");
        builder.append("         .stk                      = ").append(this.getStk()).append('\n');
        builder.append("         .istdBase                 = ").append(this.getIstdBase()).append('\n');
        builder.append("    .info3                = ");
        builder.append(" (").append(this.getInfo3()).append(" )\n");
        builder.append("         .cupx                     = ").append(this.getCupx()).append('\n');
        builder.append("         .istdNext                 = ").append(this.getIstdNext()).append('\n');
        builder.append("    .bchUpe               = ");
        builder.append(" (").append(this.getBchUpe()).append(" )\n");
        builder.append("    .grfstd               = ");
        builder.append(" (").append(this.getGrfstd()).append(" )\n");
        builder.append("         .fAutoRedef               = ").append(this.isFAutoRedef()).append('\n');
        builder.append("         .fHidden                  = ").append(this.isFHidden()).append('\n');
        builder.append("         .f97LidsSet               = ").append(this.isF97LidsSet()).append('\n');
        builder.append("         .fCopyLang                = ").append(this.isFCopyLang()).append('\n');
        builder.append("         .fPersonalCompose         = ").append(this.isFPersonalCompose()).append('\n');
        builder.append("         .fPersonalReply           = ").append(this.isFPersonalReply()).append('\n');
        builder.append("         .fPersonal                = ").append(this.isFPersonal()).append('\n');
        builder.append("         .fNoHtmlExport            = ").append(this.isFNoHtmlExport()).append('\n');
        builder.append("         .fSemiHidden              = ").append(this.isFSemiHidden()).append('\n');
        builder.append("         .fLocked                  = ").append(this.isFLocked()).append('\n');
        builder.append("         .fInternalUse             = ").append(this.isFInternalUse()).append('\n');
        builder.append("         .fUnhideWhenUsed          = ").append(this.isFUnhideWhenUsed()).append('\n');
        builder.append("         .fQFormat                 = ").append(this.isFQFormat()).append('\n');
        builder.append("         .fReserved                = ").append(this.getFReserved()).append('\n');
        builder.append("[/StdfBase]\n");
        return builder.toString();
    }
    
    @Internal
    public short getInfo1() {
        return this.field_1_info1;
    }
    
    @Internal
    public void setInfo1(final short field_1_info1) {
        this.field_1_info1 = field_1_info1;
    }
    
    @Internal
    public short getInfo2() {
        return this.field_2_info2;
    }
    
    @Internal
    public void setInfo2(final short field_2_info2) {
        this.field_2_info2 = field_2_info2;
    }
    
    @Internal
    public short getInfo3() {
        return this.field_3_info3;
    }
    
    @Internal
    public void setInfo3(final short field_3_info3) {
        this.field_3_info3 = field_3_info3;
    }
    
    @Internal
    public int getBchUpe() {
        return this.field_4_bchUpe;
    }
    
    @Internal
    public void setBchUpe(final int field_4_bchUpe) {
        this.field_4_bchUpe = field_4_bchUpe;
    }
    
    @Internal
    public short getGrfstd() {
        return this.field_5_grfstd;
    }
    
    @Internal
    public void setGrfstd(final short field_5_grfstd) {
        this.field_5_grfstd = field_5_grfstd;
    }
    
    @Internal
    public void setSti(final short value) {
        this.field_1_info1 = (short)StdfBaseAbstractType.sti.setValue(this.field_1_info1, value);
    }
    
    @Internal
    public short getSti() {
        return (short)StdfBaseAbstractType.sti.getValue(this.field_1_info1);
    }
    
    @Internal
    public void setFScratch(final boolean value) {
        this.field_1_info1 = (short)StdfBaseAbstractType.fScratch.setBoolean(this.field_1_info1, value);
    }
    
    @Internal
    public boolean isFScratch() {
        return StdfBaseAbstractType.fScratch.isSet(this.field_1_info1);
    }
    
    @Internal
    public void setFInvalHeight(final boolean value) {
        this.field_1_info1 = (short)StdfBaseAbstractType.fInvalHeight.setBoolean(this.field_1_info1, value);
    }
    
    @Internal
    public boolean isFInvalHeight() {
        return StdfBaseAbstractType.fInvalHeight.isSet(this.field_1_info1);
    }
    
    @Internal
    public void setFHasUpe(final boolean value) {
        this.field_1_info1 = (short)StdfBaseAbstractType.fHasUpe.setBoolean(this.field_1_info1, value);
    }
    
    @Internal
    public boolean isFHasUpe() {
        return StdfBaseAbstractType.fHasUpe.isSet(this.field_1_info1);
    }
    
    @Internal
    public void setFMassCopy(final boolean value) {
        this.field_1_info1 = (short)StdfBaseAbstractType.fMassCopy.setBoolean(this.field_1_info1, value);
    }
    
    @Internal
    public boolean isFMassCopy() {
        return StdfBaseAbstractType.fMassCopy.isSet(this.field_1_info1);
    }
    
    @Internal
    public void setStk(final byte value) {
        this.field_2_info2 = (short)StdfBaseAbstractType.stk.setValue(this.field_2_info2, value);
    }
    
    @Internal
    public byte getStk() {
        return (byte)StdfBaseAbstractType.stk.getValue(this.field_2_info2);
    }
    
    @Internal
    public void setIstdBase(final short value) {
        this.field_2_info2 = (short)StdfBaseAbstractType.istdBase.setValue(this.field_2_info2, value);
    }
    
    @Internal
    public short getIstdBase() {
        return (short)StdfBaseAbstractType.istdBase.getValue(this.field_2_info2);
    }
    
    @Internal
    public void setCupx(final byte value) {
        this.field_3_info3 = (short)StdfBaseAbstractType.cupx.setValue(this.field_3_info3, value);
    }
    
    @Internal
    public byte getCupx() {
        return (byte)StdfBaseAbstractType.cupx.getValue(this.field_3_info3);
    }
    
    @Internal
    public void setIstdNext(final short value) {
        this.field_3_info3 = (short)StdfBaseAbstractType.istdNext.setValue(this.field_3_info3, value);
    }
    
    @Internal
    public short getIstdNext() {
        return (short)StdfBaseAbstractType.istdNext.getValue(this.field_3_info3);
    }
    
    @Internal
    public void setFAutoRedef(final boolean value) {
        this.field_5_grfstd = (short)StdfBaseAbstractType.fAutoRedef.setBoolean(this.field_5_grfstd, value);
    }
    
    @Internal
    public boolean isFAutoRedef() {
        return StdfBaseAbstractType.fAutoRedef.isSet(this.field_5_grfstd);
    }
    
    @Internal
    public void setFHidden(final boolean value) {
        this.field_5_grfstd = (short)StdfBaseAbstractType.fHidden.setBoolean(this.field_5_grfstd, value);
    }
    
    @Internal
    public boolean isFHidden() {
        return StdfBaseAbstractType.fHidden.isSet(this.field_5_grfstd);
    }
    
    @Internal
    public void setF97LidsSet(final boolean value) {
        this.field_5_grfstd = (short)StdfBaseAbstractType.f97LidsSet.setBoolean(this.field_5_grfstd, value);
    }
    
    @Internal
    public boolean isF97LidsSet() {
        return StdfBaseAbstractType.f97LidsSet.isSet(this.field_5_grfstd);
    }
    
    @Internal
    public void setFCopyLang(final boolean value) {
        this.field_5_grfstd = (short)StdfBaseAbstractType.fCopyLang.setBoolean(this.field_5_grfstd, value);
    }
    
    @Internal
    public boolean isFCopyLang() {
        return StdfBaseAbstractType.fCopyLang.isSet(this.field_5_grfstd);
    }
    
    @Internal
    public void setFPersonalCompose(final boolean value) {
        this.field_5_grfstd = (short)StdfBaseAbstractType.fPersonalCompose.setBoolean(this.field_5_grfstd, value);
    }
    
    @Internal
    public boolean isFPersonalCompose() {
        return StdfBaseAbstractType.fPersonalCompose.isSet(this.field_5_grfstd);
    }
    
    @Internal
    public void setFPersonalReply(final boolean value) {
        this.field_5_grfstd = (short)StdfBaseAbstractType.fPersonalReply.setBoolean(this.field_5_grfstd, value);
    }
    
    @Internal
    public boolean isFPersonalReply() {
        return StdfBaseAbstractType.fPersonalReply.isSet(this.field_5_grfstd);
    }
    
    @Internal
    public void setFPersonal(final boolean value) {
        this.field_5_grfstd = (short)StdfBaseAbstractType.fPersonal.setBoolean(this.field_5_grfstd, value);
    }
    
    @Internal
    public boolean isFPersonal() {
        return StdfBaseAbstractType.fPersonal.isSet(this.field_5_grfstd);
    }
    
    @Internal
    public void setFNoHtmlExport(final boolean value) {
        this.field_5_grfstd = (short)StdfBaseAbstractType.fNoHtmlExport.setBoolean(this.field_5_grfstd, value);
    }
    
    @Internal
    public boolean isFNoHtmlExport() {
        return StdfBaseAbstractType.fNoHtmlExport.isSet(this.field_5_grfstd);
    }
    
    @Internal
    public void setFSemiHidden(final boolean value) {
        this.field_5_grfstd = (short)StdfBaseAbstractType.fSemiHidden.setBoolean(this.field_5_grfstd, value);
    }
    
    @Internal
    public boolean isFSemiHidden() {
        return StdfBaseAbstractType.fSemiHidden.isSet(this.field_5_grfstd);
    }
    
    @Internal
    public void setFLocked(final boolean value) {
        this.field_5_grfstd = (short)StdfBaseAbstractType.fLocked.setBoolean(this.field_5_grfstd, value);
    }
    
    @Internal
    public boolean isFLocked() {
        return StdfBaseAbstractType.fLocked.isSet(this.field_5_grfstd);
    }
    
    @Internal
    public void setFInternalUse(final boolean value) {
        this.field_5_grfstd = (short)StdfBaseAbstractType.fInternalUse.setBoolean(this.field_5_grfstd, value);
    }
    
    @Internal
    public boolean isFInternalUse() {
        return StdfBaseAbstractType.fInternalUse.isSet(this.field_5_grfstd);
    }
    
    @Internal
    public void setFUnhideWhenUsed(final boolean value) {
        this.field_5_grfstd = (short)StdfBaseAbstractType.fUnhideWhenUsed.setBoolean(this.field_5_grfstd, value);
    }
    
    @Internal
    public boolean isFUnhideWhenUsed() {
        return StdfBaseAbstractType.fUnhideWhenUsed.isSet(this.field_5_grfstd);
    }
    
    @Internal
    public void setFQFormat(final boolean value) {
        this.field_5_grfstd = (short)StdfBaseAbstractType.fQFormat.setBoolean(this.field_5_grfstd, value);
    }
    
    @Internal
    public boolean isFQFormat() {
        return StdfBaseAbstractType.fQFormat.isSet(this.field_5_grfstd);
    }
    
    @Internal
    public void setFReserved(final byte value) {
        this.field_5_grfstd = (short)StdfBaseAbstractType.fReserved.setValue(this.field_5_grfstd, value);
    }
    
    @Internal
    public byte getFReserved() {
        return (byte)StdfBaseAbstractType.fReserved.getValue(this.field_5_grfstd);
    }
    
    static {
        sti = new BitField(4095);
        fScratch = new BitField(4096);
        fInvalHeight = new BitField(8192);
        fHasUpe = new BitField(16384);
        fMassCopy = new BitField(32768);
        stk = new BitField(15);
        istdBase = new BitField(65520);
        cupx = new BitField(15);
        istdNext = new BitField(65520);
        fAutoRedef = new BitField(1);
        fHidden = new BitField(2);
        f97LidsSet = new BitField(4);
        fCopyLang = new BitField(8);
        fPersonalCompose = new BitField(16);
        fPersonalReply = new BitField(32);
        fPersonal = new BitField(64);
        fNoHtmlExport = new BitField(128);
        fSemiHidden = new BitField(256);
        fLocked = new BitField(512);
        fInternalUse = new BitField(1024);
        fUnhideWhenUsed = new BitField(2048);
        fQFormat = new BitField(4096);
        fReserved = new BitField(57344);
    }
}
