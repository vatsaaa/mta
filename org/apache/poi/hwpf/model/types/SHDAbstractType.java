// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hwpf.model.types;

import org.apache.poi.util.LittleEndian;
import org.apache.poi.hwpf.model.Colorref;
import org.apache.poi.util.Internal;

@Internal
public abstract class SHDAbstractType
{
    protected Colorref field_1_cvFore;
    protected Colorref field_2_cvBack;
    protected int field_3_ipat;
    
    protected SHDAbstractType() {
        this.field_1_cvFore = new Colorref();
        this.field_2_cvBack = new Colorref();
    }
    
    protected void fillFields(final byte[] data, final int offset) {
        this.field_1_cvFore = new Colorref(data, 0 + offset);
        this.field_2_cvBack = new Colorref(data, 4 + offset);
        this.field_3_ipat = LittleEndian.getShort(data, 8 + offset);
    }
    
    public void serialize(final byte[] data, final int offset) {
        this.field_1_cvFore.serialize(data, 0 + offset);
        this.field_2_cvBack.serialize(data, 4 + offset);
        LittleEndian.putShort(data, 8 + offset, (short)this.field_3_ipat);
    }
    
    public static int getSize() {
        return 10;
    }
    
    @Override
    public String toString() {
        final StringBuilder builder = new StringBuilder();
        builder.append("[SHD]\n");
        builder.append("    .cvFore               = ");
        builder.append(" (").append(this.getCvFore()).append(" )\n");
        builder.append("    .cvBack               = ");
        builder.append(" (").append(this.getCvBack()).append(" )\n");
        builder.append("    .ipat                 = ");
        builder.append(" (").append(this.getIpat()).append(" )\n");
        builder.append("[/SHD]\n");
        return builder.toString();
    }
    
    @Internal
    public Colorref getCvFore() {
        return this.field_1_cvFore;
    }
    
    @Internal
    public void setCvFore(final Colorref field_1_cvFore) {
        this.field_1_cvFore = field_1_cvFore;
    }
    
    @Internal
    public Colorref getCvBack() {
        return this.field_2_cvBack;
    }
    
    @Internal
    public void setCvBack(final Colorref field_2_cvBack) {
        this.field_2_cvBack = field_2_cvBack;
    }
    
    @Internal
    public int getIpat() {
        return this.field_3_ipat;
    }
    
    @Internal
    public void setIpat(final int field_3_ipat) {
        this.field_3_ipat = field_3_ipat;
    }
}
