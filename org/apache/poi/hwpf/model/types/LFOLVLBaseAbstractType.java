// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hwpf.model.types;

import org.apache.poi.util.LittleEndian;
import org.apache.poi.util.BitField;
import org.apache.poi.util.Internal;

@Internal
public abstract class LFOLVLBaseAbstractType
{
    protected int field_1_iStartAt;
    protected int field_2_flags;
    private static final BitField iLvl;
    private static final BitField fStartAt;
    private static final BitField fFormatting;
    private static final BitField grfhic;
    private static final BitField unused1;
    private static final BitField unused2;
    
    protected LFOLVLBaseAbstractType() {
    }
    
    protected void fillFields(final byte[] data, final int offset) {
        this.field_1_iStartAt = LittleEndian.getInt(data, 0 + offset);
        this.field_2_flags = LittleEndian.getInt(data, 4 + offset);
    }
    
    public void serialize(final byte[] data, final int offset) {
        LittleEndian.putInt(data, 0 + offset, this.field_1_iStartAt);
        LittleEndian.putInt(data, 4 + offset, this.field_2_flags);
    }
    
    public byte[] serialize() {
        final byte[] result = new byte[getSize()];
        this.serialize(result, 0);
        return result;
    }
    
    public static int getSize() {
        return 8;
    }
    
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        final LFOLVLBaseAbstractType other = (LFOLVLBaseAbstractType)obj;
        return this.field_1_iStartAt == other.field_1_iStartAt && this.field_2_flags == other.field_2_flags;
    }
    
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = 31 * result + this.field_1_iStartAt;
        result = 31 * result + this.field_2_flags;
        return result;
    }
    
    @Override
    public String toString() {
        final StringBuilder builder = new StringBuilder();
        builder.append("[LFOLVLBase]\n");
        builder.append("    .iStartAt             = ");
        builder.append(" (").append(this.getIStartAt()).append(" )\n");
        builder.append("    .flags                = ");
        builder.append(" (").append(this.getFlags()).append(" )\n");
        builder.append("         .iLvl                     = ").append(this.getILvl()).append('\n');
        builder.append("         .fStartAt                 = ").append(this.isFStartAt()).append('\n');
        builder.append("         .fFormatting              = ").append(this.isFFormatting()).append('\n');
        builder.append("         .grfhic                   = ").append(this.getGrfhic()).append('\n');
        builder.append("         .unused1                  = ").append(this.getUnused1()).append('\n');
        builder.append("         .unused2                  = ").append(this.getUnused2()).append('\n');
        builder.append("[/LFOLVLBase]\n");
        return builder.toString();
    }
    
    @Internal
    public int getIStartAt() {
        return this.field_1_iStartAt;
    }
    
    @Internal
    public void setIStartAt(final int field_1_iStartAt) {
        this.field_1_iStartAt = field_1_iStartAt;
    }
    
    @Internal
    public int getFlags() {
        return this.field_2_flags;
    }
    
    @Internal
    public void setFlags(final int field_2_flags) {
        this.field_2_flags = field_2_flags;
    }
    
    @Internal
    public void setILvl(final byte value) {
        this.field_2_flags = LFOLVLBaseAbstractType.iLvl.setValue(this.field_2_flags, value);
    }
    
    @Internal
    public byte getILvl() {
        return (byte)LFOLVLBaseAbstractType.iLvl.getValue(this.field_2_flags);
    }
    
    @Internal
    public void setFStartAt(final boolean value) {
        this.field_2_flags = LFOLVLBaseAbstractType.fStartAt.setBoolean(this.field_2_flags, value);
    }
    
    @Internal
    public boolean isFStartAt() {
        return LFOLVLBaseAbstractType.fStartAt.isSet(this.field_2_flags);
    }
    
    @Internal
    public void setFFormatting(final boolean value) {
        this.field_2_flags = LFOLVLBaseAbstractType.fFormatting.setBoolean(this.field_2_flags, value);
    }
    
    @Internal
    public boolean isFFormatting() {
        return LFOLVLBaseAbstractType.fFormatting.isSet(this.field_2_flags);
    }
    
    @Internal
    public void setGrfhic(final short value) {
        this.field_2_flags = LFOLVLBaseAbstractType.grfhic.setValue(this.field_2_flags, value);
    }
    
    @Internal
    public short getGrfhic() {
        return (short)LFOLVLBaseAbstractType.grfhic.getValue(this.field_2_flags);
    }
    
    @Internal
    public void setUnused1(final short value) {
        this.field_2_flags = LFOLVLBaseAbstractType.unused1.setValue(this.field_2_flags, value);
    }
    
    @Internal
    @Deprecated
    public short getUnused1() {
        return (short)LFOLVLBaseAbstractType.unused1.getValue(this.field_2_flags);
    }
    
    @Internal
    public void setUnused2(final byte value) {
        this.field_2_flags = LFOLVLBaseAbstractType.unused2.setValue(this.field_2_flags, value);
    }
    
    @Internal
    @Deprecated
    public byte getUnused2() {
        return (byte)LFOLVLBaseAbstractType.unused2.getValue(this.field_2_flags);
    }
    
    static {
        iLvl = new BitField(15);
        fStartAt = new BitField(16);
        fFormatting = new BitField(32);
        grfhic = new BitField(16320);
        unused1 = new BitField(536854528);
        unused2 = new BitField(-536870912);
    }
}
