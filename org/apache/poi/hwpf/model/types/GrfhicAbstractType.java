// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hwpf.model.types;

import org.apache.poi.util.BitField;
import org.apache.poi.util.Internal;

@Internal
public abstract class GrfhicAbstractType
{
    protected byte field_1_grfhic;
    private static final BitField fHtmlChecked;
    private static final BitField fHtmlUnsupported;
    private static final BitField fHtmlListTextNotSharpDot;
    private static final BitField fHtmlNotPeriod;
    private static final BitField fHtmlFirstLineMismatch;
    private static final BitField fHtmlTabLeftIndentMismatch;
    private static final BitField fHtmlHangingIndentBeneathNumber;
    private static final BitField fHtmlBuiltInBullet;
    
    protected GrfhicAbstractType() {
    }
    
    protected void fillFields(final byte[] data, final int offset) {
        this.field_1_grfhic = data[0 + offset];
    }
    
    public void serialize(final byte[] data, final int offset) {
        data[0 + offset] = this.field_1_grfhic;
    }
    
    public byte[] serialize() {
        final byte[] result = new byte[getSize()];
        this.serialize(result, 0);
        return result;
    }
    
    public static int getSize() {
        return 1;
    }
    
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        final GrfhicAbstractType other = (GrfhicAbstractType)obj;
        return this.field_1_grfhic == other.field_1_grfhic;
    }
    
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = 31 * result + this.field_1_grfhic;
        return result;
    }
    
    @Override
    public String toString() {
        final StringBuilder builder = new StringBuilder();
        builder.append("[Grfhic]\n");
        builder.append("    .grfhic               = ");
        builder.append(" (").append(this.getGrfhic()).append(" )\n");
        builder.append("         .fHtmlChecked             = ").append(this.isFHtmlChecked()).append('\n');
        builder.append("         .fHtmlUnsupported         = ").append(this.isFHtmlUnsupported()).append('\n');
        builder.append("         .fHtmlListTextNotSharpDot     = ").append(this.isFHtmlListTextNotSharpDot()).append('\n');
        builder.append("         .fHtmlNotPeriod           = ").append(this.isFHtmlNotPeriod()).append('\n');
        builder.append("         .fHtmlFirstLineMismatch     = ").append(this.isFHtmlFirstLineMismatch()).append('\n');
        builder.append("         .fHtmlTabLeftIndentMismatch     = ").append(this.isFHtmlTabLeftIndentMismatch()).append('\n');
        builder.append("         .fHtmlHangingIndentBeneathNumber     = ").append(this.isFHtmlHangingIndentBeneathNumber()).append('\n');
        builder.append("         .fHtmlBuiltInBullet       = ").append(this.isFHtmlBuiltInBullet()).append('\n');
        builder.append("[/Grfhic]\n");
        return builder.toString();
    }
    
    @Internal
    public byte getGrfhic() {
        return this.field_1_grfhic;
    }
    
    @Internal
    public void setGrfhic(final byte field_1_grfhic) {
        this.field_1_grfhic = field_1_grfhic;
    }
    
    @Internal
    public void setFHtmlChecked(final boolean value) {
        this.field_1_grfhic = (byte)GrfhicAbstractType.fHtmlChecked.setBoolean(this.field_1_grfhic, value);
    }
    
    @Internal
    public boolean isFHtmlChecked() {
        return GrfhicAbstractType.fHtmlChecked.isSet(this.field_1_grfhic);
    }
    
    @Internal
    public void setFHtmlUnsupported(final boolean value) {
        this.field_1_grfhic = (byte)GrfhicAbstractType.fHtmlUnsupported.setBoolean(this.field_1_grfhic, value);
    }
    
    @Internal
    public boolean isFHtmlUnsupported() {
        return GrfhicAbstractType.fHtmlUnsupported.isSet(this.field_1_grfhic);
    }
    
    @Internal
    public void setFHtmlListTextNotSharpDot(final boolean value) {
        this.field_1_grfhic = (byte)GrfhicAbstractType.fHtmlListTextNotSharpDot.setBoolean(this.field_1_grfhic, value);
    }
    
    @Internal
    public boolean isFHtmlListTextNotSharpDot() {
        return GrfhicAbstractType.fHtmlListTextNotSharpDot.isSet(this.field_1_grfhic);
    }
    
    @Internal
    public void setFHtmlNotPeriod(final boolean value) {
        this.field_1_grfhic = (byte)GrfhicAbstractType.fHtmlNotPeriod.setBoolean(this.field_1_grfhic, value);
    }
    
    @Internal
    public boolean isFHtmlNotPeriod() {
        return GrfhicAbstractType.fHtmlNotPeriod.isSet(this.field_1_grfhic);
    }
    
    @Internal
    public void setFHtmlFirstLineMismatch(final boolean value) {
        this.field_1_grfhic = (byte)GrfhicAbstractType.fHtmlFirstLineMismatch.setBoolean(this.field_1_grfhic, value);
    }
    
    @Internal
    public boolean isFHtmlFirstLineMismatch() {
        return GrfhicAbstractType.fHtmlFirstLineMismatch.isSet(this.field_1_grfhic);
    }
    
    @Internal
    public void setFHtmlTabLeftIndentMismatch(final boolean value) {
        this.field_1_grfhic = (byte)GrfhicAbstractType.fHtmlTabLeftIndentMismatch.setBoolean(this.field_1_grfhic, value);
    }
    
    @Internal
    public boolean isFHtmlTabLeftIndentMismatch() {
        return GrfhicAbstractType.fHtmlTabLeftIndentMismatch.isSet(this.field_1_grfhic);
    }
    
    @Internal
    public void setFHtmlHangingIndentBeneathNumber(final boolean value) {
        this.field_1_grfhic = (byte)GrfhicAbstractType.fHtmlHangingIndentBeneathNumber.setBoolean(this.field_1_grfhic, value);
    }
    
    @Internal
    public boolean isFHtmlHangingIndentBeneathNumber() {
        return GrfhicAbstractType.fHtmlHangingIndentBeneathNumber.isSet(this.field_1_grfhic);
    }
    
    @Internal
    public void setFHtmlBuiltInBullet(final boolean value) {
        this.field_1_grfhic = (byte)GrfhicAbstractType.fHtmlBuiltInBullet.setBoolean(this.field_1_grfhic, value);
    }
    
    @Internal
    public boolean isFHtmlBuiltInBullet() {
        return GrfhicAbstractType.fHtmlBuiltInBullet.isSet(this.field_1_grfhic);
    }
    
    static {
        fHtmlChecked = new BitField(1);
        fHtmlUnsupported = new BitField(2);
        fHtmlListTextNotSharpDot = new BitField(4);
        fHtmlNotPeriod = new BitField(8);
        fHtmlFirstLineMismatch = new BitField(16);
        fHtmlTabLeftIndentMismatch = new BitField(32);
        fHtmlHangingIndentBeneathNumber = new BitField(64);
        fHtmlBuiltInBullet = new BitField(128);
    }
}
