// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hwpf.model.types;

import org.apache.poi.util.LittleEndian;
import org.apache.poi.util.BitField;
import org.apache.poi.util.Internal;

@Internal
public abstract class FibBaseAbstractType
{
    protected int field_1_wIdent;
    protected int field_2_nFib;
    protected int field_3_unused;
    protected int field_4_lid;
    protected int field_5_pnNext;
    protected short field_6_flags1;
    private static final BitField fDot;
    private static final BitField fGlsy;
    private static final BitField fComplex;
    private static final BitField fHasPic;
    private static final BitField cQuickSaves;
    private static final BitField fEncrypted;
    private static final BitField fWhichTblStm;
    private static final BitField fReadOnlyRecommended;
    private static final BitField fWriteReservation;
    private static final BitField fExtChar;
    private static final BitField fLoadOverride;
    private static final BitField fFarEast;
    private static final BitField fObfuscated;
    protected int field_7_nFibBack;
    protected int field_8_lKey;
    @Deprecated
    protected byte field_9_envr;
    protected byte field_10_flags2;
    private static final BitField fMac;
    private static final BitField fEmptySpecial;
    private static final BitField fLoadOverridePage;
    private static final BitField reserved1;
    private static final BitField reserved2;
    private static final BitField fSpare0;
    @Deprecated
    protected short field_11_Chs;
    @Deprecated
    protected short field_12_chsTables;
    @Deprecated
    protected int field_13_fcMin;
    @Deprecated
    protected int field_14_fcMac;
    
    protected FibBaseAbstractType() {
    }
    
    protected void fillFields(final byte[] data, final int offset) {
        this.field_1_wIdent = LittleEndian.getShort(data, 0 + offset);
        this.field_2_nFib = LittleEndian.getShort(data, 2 + offset);
        this.field_3_unused = LittleEndian.getShort(data, 4 + offset);
        this.field_4_lid = LittleEndian.getShort(data, 6 + offset);
        this.field_5_pnNext = LittleEndian.getShort(data, 8 + offset);
        this.field_6_flags1 = LittleEndian.getShort(data, 10 + offset);
        this.field_7_nFibBack = LittleEndian.getShort(data, 12 + offset);
        this.field_8_lKey = LittleEndian.getInt(data, 14 + offset);
        this.field_9_envr = data[18 + offset];
        this.field_10_flags2 = data[19 + offset];
        this.field_11_Chs = LittleEndian.getShort(data, 20 + offset);
        this.field_12_chsTables = LittleEndian.getShort(data, 22 + offset);
        this.field_13_fcMin = LittleEndian.getInt(data, 24 + offset);
        this.field_14_fcMac = LittleEndian.getInt(data, 28 + offset);
    }
    
    public void serialize(final byte[] data, final int offset) {
        LittleEndian.putUShort(data, 0 + offset, this.field_1_wIdent);
        LittleEndian.putUShort(data, 2 + offset, this.field_2_nFib);
        LittleEndian.putUShort(data, 4 + offset, this.field_3_unused);
        LittleEndian.putUShort(data, 6 + offset, this.field_4_lid);
        LittleEndian.putUShort(data, 8 + offset, this.field_5_pnNext);
        LittleEndian.putShort(data, 10 + offset, this.field_6_flags1);
        LittleEndian.putUShort(data, 12 + offset, this.field_7_nFibBack);
        LittleEndian.putInt(data, 14 + offset, this.field_8_lKey);
        data[18 + offset] = this.field_9_envr;
        data[19 + offset] = this.field_10_flags2;
        LittleEndian.putShort(data, 20 + offset, this.field_11_Chs);
        LittleEndian.putShort(data, 22 + offset, this.field_12_chsTables);
        LittleEndian.putInt(data, 24 + offset, this.field_13_fcMin);
        LittleEndian.putInt(data, 28 + offset, this.field_14_fcMac);
    }
    
    public byte[] serialize() {
        final byte[] result = new byte[getSize()];
        this.serialize(result, 0);
        return result;
    }
    
    public static int getSize() {
        return 32;
    }
    
    @Override
    public String toString() {
        final StringBuilder builder = new StringBuilder();
        builder.append("[FibBase]\n");
        builder.append("    .wIdent               = ");
        builder.append(" (").append(this.getWIdent()).append(" )\n");
        builder.append("    .nFib                 = ");
        builder.append(" (").append(this.getNFib()).append(" )\n");
        builder.append("    .unused               = ");
        builder.append(" (").append(this.getUnused()).append(" )\n");
        builder.append("    .lid                  = ");
        builder.append(" (").append(this.getLid()).append(" )\n");
        builder.append("    .pnNext               = ");
        builder.append(" (").append(this.getPnNext()).append(" )\n");
        builder.append("    .flags1               = ");
        builder.append(" (").append(this.getFlags1()).append(" )\n");
        builder.append("         .fDot                     = ").append(this.isFDot()).append('\n');
        builder.append("         .fGlsy                    = ").append(this.isFGlsy()).append('\n');
        builder.append("         .fComplex                 = ").append(this.isFComplex()).append('\n');
        builder.append("         .fHasPic                  = ").append(this.isFHasPic()).append('\n');
        builder.append("         .cQuickSaves              = ").append(this.getCQuickSaves()).append('\n');
        builder.append("         .fEncrypted               = ").append(this.isFEncrypted()).append('\n');
        builder.append("         .fWhichTblStm             = ").append(this.isFWhichTblStm()).append('\n');
        builder.append("         .fReadOnlyRecommended     = ").append(this.isFReadOnlyRecommended()).append('\n');
        builder.append("         .fWriteReservation        = ").append(this.isFWriteReservation()).append('\n');
        builder.append("         .fExtChar                 = ").append(this.isFExtChar()).append('\n');
        builder.append("         .fLoadOverride            = ").append(this.isFLoadOverride()).append('\n');
        builder.append("         .fFarEast                 = ").append(this.isFFarEast()).append('\n');
        builder.append("         .fObfuscated              = ").append(this.isFObfuscated()).append('\n');
        builder.append("    .nFibBack             = ");
        builder.append(" (").append(this.getNFibBack()).append(" )\n");
        builder.append("    .lKey                 = ");
        builder.append(" (").append(this.getLKey()).append(" )\n");
        builder.append("    .envr                 = ");
        builder.append(" (").append(this.getEnvr()).append(" )\n");
        builder.append("    .flags2               = ");
        builder.append(" (").append(this.getFlags2()).append(" )\n");
        builder.append("         .fMac                     = ").append(this.isFMac()).append('\n');
        builder.append("         .fEmptySpecial            = ").append(this.isFEmptySpecial()).append('\n');
        builder.append("         .fLoadOverridePage        = ").append(this.isFLoadOverridePage()).append('\n');
        builder.append("         .reserved1                = ").append(this.isReserved1()).append('\n');
        builder.append("         .reserved2                = ").append(this.isReserved2()).append('\n');
        builder.append("         .fSpare0                  = ").append(this.getFSpare0()).append('\n');
        builder.append("    .Chs                  = ");
        builder.append(" (").append(this.getChs()).append(" )\n");
        builder.append("    .chsTables            = ");
        builder.append(" (").append(this.getChsTables()).append(" )\n");
        builder.append("    .fcMin                = ");
        builder.append(" (").append(this.getFcMin()).append(" )\n");
        builder.append("    .fcMac                = ");
        builder.append(" (").append(this.getFcMac()).append(" )\n");
        builder.append("[/FibBase]\n");
        return builder.toString();
    }
    
    @Internal
    public int getWIdent() {
        return this.field_1_wIdent;
    }
    
    @Internal
    public void setWIdent(final int field_1_wIdent) {
        this.field_1_wIdent = field_1_wIdent;
    }
    
    @Internal
    public int getNFib() {
        return this.field_2_nFib;
    }
    
    @Internal
    public void setNFib(final int field_2_nFib) {
        this.field_2_nFib = field_2_nFib;
    }
    
    @Internal
    public int getUnused() {
        return this.field_3_unused;
    }
    
    @Internal
    public void setUnused(final int field_3_unused) {
        this.field_3_unused = field_3_unused;
    }
    
    @Internal
    public int getLid() {
        return this.field_4_lid;
    }
    
    @Internal
    public void setLid(final int field_4_lid) {
        this.field_4_lid = field_4_lid;
    }
    
    @Internal
    public int getPnNext() {
        return this.field_5_pnNext;
    }
    
    @Internal
    public void setPnNext(final int field_5_pnNext) {
        this.field_5_pnNext = field_5_pnNext;
    }
    
    @Internal
    public short getFlags1() {
        return this.field_6_flags1;
    }
    
    @Internal
    public void setFlags1(final short field_6_flags1) {
        this.field_6_flags1 = field_6_flags1;
    }
    
    @Internal
    public int getNFibBack() {
        return this.field_7_nFibBack;
    }
    
    @Internal
    public void setNFibBack(final int field_7_nFibBack) {
        this.field_7_nFibBack = field_7_nFibBack;
    }
    
    @Internal
    public int getLKey() {
        return this.field_8_lKey;
    }
    
    @Internal
    public void setLKey(final int field_8_lKey) {
        this.field_8_lKey = field_8_lKey;
    }
    
    @Internal
    public byte getEnvr() {
        return this.field_9_envr;
    }
    
    @Internal
    public void setEnvr(final byte field_9_envr) {
        this.field_9_envr = field_9_envr;
    }
    
    @Internal
    public byte getFlags2() {
        return this.field_10_flags2;
    }
    
    @Internal
    public void setFlags2(final byte field_10_flags2) {
        this.field_10_flags2 = field_10_flags2;
    }
    
    @Internal
    public short getChs() {
        return this.field_11_Chs;
    }
    
    @Internal
    public void setChs(final short field_11_Chs) {
        this.field_11_Chs = field_11_Chs;
    }
    
    @Internal
    public short getChsTables() {
        return this.field_12_chsTables;
    }
    
    @Internal
    public void setChsTables(final short field_12_chsTables) {
        this.field_12_chsTables = field_12_chsTables;
    }
    
    @Internal
    public int getFcMin() {
        return this.field_13_fcMin;
    }
    
    @Internal
    public void setFcMin(final int field_13_fcMin) {
        this.field_13_fcMin = field_13_fcMin;
    }
    
    @Internal
    public int getFcMac() {
        return this.field_14_fcMac;
    }
    
    @Internal
    public void setFcMac(final int field_14_fcMac) {
        this.field_14_fcMac = field_14_fcMac;
    }
    
    @Internal
    public void setFDot(final boolean value) {
        this.field_6_flags1 = (short)FibBaseAbstractType.fDot.setBoolean(this.field_6_flags1, value);
    }
    
    @Internal
    public boolean isFDot() {
        return FibBaseAbstractType.fDot.isSet(this.field_6_flags1);
    }
    
    @Internal
    public void setFGlsy(final boolean value) {
        this.field_6_flags1 = (short)FibBaseAbstractType.fGlsy.setBoolean(this.field_6_flags1, value);
    }
    
    @Internal
    public boolean isFGlsy() {
        return FibBaseAbstractType.fGlsy.isSet(this.field_6_flags1);
    }
    
    @Internal
    public void setFComplex(final boolean value) {
        this.field_6_flags1 = (short)FibBaseAbstractType.fComplex.setBoolean(this.field_6_flags1, value);
    }
    
    @Internal
    public boolean isFComplex() {
        return FibBaseAbstractType.fComplex.isSet(this.field_6_flags1);
    }
    
    @Internal
    public void setFHasPic(final boolean value) {
        this.field_6_flags1 = (short)FibBaseAbstractType.fHasPic.setBoolean(this.field_6_flags1, value);
    }
    
    @Internal
    public boolean isFHasPic() {
        return FibBaseAbstractType.fHasPic.isSet(this.field_6_flags1);
    }
    
    @Internal
    public void setCQuickSaves(final byte value) {
        this.field_6_flags1 = (short)FibBaseAbstractType.cQuickSaves.setValue(this.field_6_flags1, value);
    }
    
    @Internal
    public byte getCQuickSaves() {
        return (byte)FibBaseAbstractType.cQuickSaves.getValue(this.field_6_flags1);
    }
    
    @Internal
    public void setFEncrypted(final boolean value) {
        this.field_6_flags1 = (short)FibBaseAbstractType.fEncrypted.setBoolean(this.field_6_flags1, value);
    }
    
    @Internal
    public boolean isFEncrypted() {
        return FibBaseAbstractType.fEncrypted.isSet(this.field_6_flags1);
    }
    
    @Internal
    public void setFWhichTblStm(final boolean value) {
        this.field_6_flags1 = (short)FibBaseAbstractType.fWhichTblStm.setBoolean(this.field_6_flags1, value);
    }
    
    @Internal
    public boolean isFWhichTblStm() {
        return FibBaseAbstractType.fWhichTblStm.isSet(this.field_6_flags1);
    }
    
    @Internal
    public void setFReadOnlyRecommended(final boolean value) {
        this.field_6_flags1 = (short)FibBaseAbstractType.fReadOnlyRecommended.setBoolean(this.field_6_flags1, value);
    }
    
    @Internal
    public boolean isFReadOnlyRecommended() {
        return FibBaseAbstractType.fReadOnlyRecommended.isSet(this.field_6_flags1);
    }
    
    @Internal
    public void setFWriteReservation(final boolean value) {
        this.field_6_flags1 = (short)FibBaseAbstractType.fWriteReservation.setBoolean(this.field_6_flags1, value);
    }
    
    @Internal
    public boolean isFWriteReservation() {
        return FibBaseAbstractType.fWriteReservation.isSet(this.field_6_flags1);
    }
    
    @Internal
    public void setFExtChar(final boolean value) {
        this.field_6_flags1 = (short)FibBaseAbstractType.fExtChar.setBoolean(this.field_6_flags1, value);
    }
    
    @Internal
    public boolean isFExtChar() {
        return FibBaseAbstractType.fExtChar.isSet(this.field_6_flags1);
    }
    
    @Internal
    public void setFLoadOverride(final boolean value) {
        this.field_6_flags1 = (short)FibBaseAbstractType.fLoadOverride.setBoolean(this.field_6_flags1, value);
    }
    
    @Internal
    public boolean isFLoadOverride() {
        return FibBaseAbstractType.fLoadOverride.isSet(this.field_6_flags1);
    }
    
    @Internal
    public void setFFarEast(final boolean value) {
        this.field_6_flags1 = (short)FibBaseAbstractType.fFarEast.setBoolean(this.field_6_flags1, value);
    }
    
    @Internal
    public boolean isFFarEast() {
        return FibBaseAbstractType.fFarEast.isSet(this.field_6_flags1);
    }
    
    @Internal
    public void setFObfuscated(final boolean value) {
        this.field_6_flags1 = (short)FibBaseAbstractType.fObfuscated.setBoolean(this.field_6_flags1, value);
    }
    
    @Internal
    public boolean isFObfuscated() {
        return FibBaseAbstractType.fObfuscated.isSet(this.field_6_flags1);
    }
    
    @Internal
    public void setFMac(final boolean value) {
        this.field_10_flags2 = (byte)FibBaseAbstractType.fMac.setBoolean(this.field_10_flags2, value);
    }
    
    @Internal
    @Deprecated
    public boolean isFMac() {
        return FibBaseAbstractType.fMac.isSet(this.field_10_flags2);
    }
    
    @Internal
    public void setFEmptySpecial(final boolean value) {
        this.field_10_flags2 = (byte)FibBaseAbstractType.fEmptySpecial.setBoolean(this.field_10_flags2, value);
    }
    
    @Internal
    @Deprecated
    public boolean isFEmptySpecial() {
        return FibBaseAbstractType.fEmptySpecial.isSet(this.field_10_flags2);
    }
    
    @Internal
    public void setFLoadOverridePage(final boolean value) {
        this.field_10_flags2 = (byte)FibBaseAbstractType.fLoadOverridePage.setBoolean(this.field_10_flags2, value);
    }
    
    @Internal
    public boolean isFLoadOverridePage() {
        return FibBaseAbstractType.fLoadOverridePage.isSet(this.field_10_flags2);
    }
    
    @Internal
    public void setReserved1(final boolean value) {
        this.field_10_flags2 = (byte)FibBaseAbstractType.reserved1.setBoolean(this.field_10_flags2, value);
    }
    
    @Internal
    @Deprecated
    public boolean isReserved1() {
        return FibBaseAbstractType.reserved1.isSet(this.field_10_flags2);
    }
    
    @Internal
    public void setReserved2(final boolean value) {
        this.field_10_flags2 = (byte)FibBaseAbstractType.reserved2.setBoolean(this.field_10_flags2, value);
    }
    
    @Internal
    @Deprecated
    public boolean isReserved2() {
        return FibBaseAbstractType.reserved2.isSet(this.field_10_flags2);
    }
    
    @Internal
    public void setFSpare0(final byte value) {
        this.field_10_flags2 = (byte)FibBaseAbstractType.fSpare0.setValue(this.field_10_flags2, value);
    }
    
    @Internal
    @Deprecated
    public byte getFSpare0() {
        return (byte)FibBaseAbstractType.fSpare0.getValue(this.field_10_flags2);
    }
    
    static {
        fDot = new BitField(1);
        fGlsy = new BitField(2);
        fComplex = new BitField(4);
        fHasPic = new BitField(8);
        cQuickSaves = new BitField(240);
        fEncrypted = new BitField(256);
        fWhichTblStm = new BitField(512);
        fReadOnlyRecommended = new BitField(1024);
        fWriteReservation = new BitField(2048);
        fExtChar = new BitField(4096);
        fLoadOverride = new BitField(8192);
        fFarEast = new BitField(16384);
        fObfuscated = new BitField(32768);
        fMac = new BitField(1);
        fEmptySpecial = new BitField(2);
        fLoadOverridePage = new BitField(4);
        reserved1 = new BitField(8);
        reserved2 = new BitField(16);
        fSpare0 = new BitField(254);
    }
}
