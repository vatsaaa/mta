// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hwpf.model.types;

import org.apache.poi.hwpf.usermodel.DateAndTime;
import org.apache.poi.hwpf.usermodel.BorderCode;
import org.apache.poi.hwpf.usermodel.ShadingDescriptor;
import org.apache.poi.hwpf.model.Hyphenation;
import org.apache.poi.hwpf.model.Colorref;
import org.apache.poi.util.BitField;
import org.apache.poi.util.Internal;

@Internal
public abstract class CHPAbstractType
{
    protected int field_1_grpfChp;
    private static BitField fBold;
    private static BitField fItalic;
    private static BitField fRMarkDel;
    private static BitField fOutline;
    private static BitField fFldVanish;
    private static BitField fSmallCaps;
    private static BitField fCaps;
    private static BitField fVanish;
    private static BitField fRMark;
    private static BitField fSpec;
    private static BitField fStrike;
    private static BitField fObj;
    private static BitField fShadow;
    private static BitField fLowerCase;
    private static BitField fData;
    private static BitField fOle2;
    private static BitField fEmboss;
    private static BitField fImprint;
    private static BitField fDStrike;
    private static BitField fUsePgsuSettings;
    private static BitField fBoldBi;
    private static BitField fComplexScripts;
    private static BitField fItalicBi;
    private static BitField fBiDi;
    protected int field_2_hps;
    protected int field_3_ftcAscii;
    protected int field_4_ftcFE;
    protected int field_5_ftcOther;
    protected int field_6_ftcBi;
    protected int field_7_dxaSpace;
    protected Colorref field_8_cv;
    protected byte field_9_ico;
    protected int field_10_pctCharWidth;
    protected int field_11_lidDefault;
    protected int field_12_lidFE;
    protected byte field_13_kcd;
    protected static final byte KCD_NON = 0;
    protected static final byte KCD_DOT = 1;
    protected static final byte KCD_COMMA = 2;
    protected static final byte KCD_CIRCLE = 3;
    protected static final byte KCD_UNDER_DOT = 4;
    protected boolean field_14_fUndetermine;
    protected byte field_15_iss;
    protected static final byte ISS_NONE = 0;
    protected static final byte ISS_SUPERSCRIPTED = 1;
    protected static final byte ISS_SUBSCRIPTED = 2;
    protected boolean field_16_fSpecSymbol;
    protected byte field_17_idct;
    protected byte field_18_idctHint;
    protected byte field_19_kul;
    protected static final byte KUL_NONE = 0;
    protected static final byte KUL_SINGLE = 1;
    protected static final byte KUL_BY_WORD = 2;
    protected static final byte KUL_DOUBLE = 3;
    protected static final byte KUL_DOTTED = 4;
    protected static final byte KUL_HIDDEN = 5;
    protected static final byte KUL_THICK = 6;
    protected static final byte KUL_DASH = 7;
    protected static final byte KUL_DOT = 8;
    protected static final byte KUL_DOT_DASH = 9;
    protected static final byte KUL_DOT_DOT_DASH = 10;
    protected static final byte KUL_WAVE = 11;
    protected static final byte KUL_DOTTED_HEAVY = 20;
    protected static final byte KUL_DASHED_HEAVY = 23;
    protected static final byte KUL_DOT_DASH_HEAVY = 25;
    protected static final byte KUL_DOT_DOT_DASH_HEAVY = 26;
    protected static final byte KUL_WAVE_HEAVY = 27;
    protected static final byte KUL_DASH_LONG = 39;
    protected static final byte KUL_WAVE_DOUBLE = 43;
    protected static final byte KUL_DASH_LONG_HEAVY = 55;
    protected Hyphenation field_20_hresi;
    protected int field_21_hpsKern;
    protected short field_22_hpsPos;
    protected ShadingDescriptor field_23_shd;
    protected BorderCode field_24_brc;
    protected int field_25_ibstRMark;
    protected byte field_26_sfxtText;
    protected static final byte SFXTTEXT_NO = 0;
    protected static final byte SFXTTEXT_LAS_VEGAS_LIGHTS = 1;
    protected static final byte SFXTTEXT_BACKGROUND_BLINK = 2;
    protected static final byte SFXTTEXT_SPARKLE_TEXT = 3;
    protected static final byte SFXTTEXT_MARCHING_ANTS = 4;
    protected static final byte SFXTTEXT_MARCHING_RED_ANTS = 5;
    protected static final byte SFXTTEXT_SHIMMER = 6;
    protected boolean field_27_fDblBdr;
    protected boolean field_28_fBorderWS;
    protected short field_29_ufel;
    private static BitField itypFELayout;
    private static BitField fTNY;
    private static BitField fWarichu;
    private static BitField fKumimoji;
    private static BitField fRuby;
    private static BitField fLSFitText;
    private static BitField spare;
    protected byte field_30_copt;
    private static BitField iWarichuBracket;
    private static BitField fWarichuNoOpenBracket;
    private static BitField fTNYCompress;
    private static BitField fTNYFetchTxm;
    private static BitField fCellFitText;
    private static BitField unused;
    protected int field_31_hpsAsci;
    protected int field_32_hpsFE;
    protected int field_33_hpsBi;
    protected int field_34_ftcSym;
    protected int field_35_xchSym;
    protected int field_36_fcPic;
    protected int field_37_fcObj;
    protected int field_38_lTagObj;
    protected int field_39_fcData;
    protected Hyphenation field_40_hresiOld;
    protected int field_41_ibstRMarkDel;
    protected DateAndTime field_42_dttmRMark;
    protected DateAndTime field_43_dttmRMarkDel;
    protected int field_44_istd;
    protected int field_45_idslRMReason;
    protected int field_46_idslReasonDel;
    protected int field_47_cpg;
    protected short field_48_Highlight;
    private static BitField icoHighlight;
    private static BitField fHighlight;
    protected short field_49_CharsetFlags;
    private static BitField fChsDiff;
    private static BitField fMacChs;
    protected short field_50_chse;
    protected boolean field_51_fPropRMark;
    protected int field_52_ibstPropRMark;
    protected DateAndTime field_53_dttmPropRMark;
    protected boolean field_54_fConflictOrig;
    protected boolean field_55_fConflictOtherDel;
    protected int field_56_wConflict;
    protected int field_57_IbstConflict;
    protected DateAndTime field_58_dttmConflict;
    protected boolean field_59_fDispFldRMark;
    protected int field_60_ibstDispFldRMark;
    protected DateAndTime field_61_dttmDispFldRMark;
    protected byte[] field_62_xstDispFldRMark;
    protected int field_63_fcObjp;
    protected byte field_64_lbrCRJ;
    protected static final byte LBRCRJ_NONE = 0;
    protected static final byte LBRCRJ_LEFT = 1;
    protected static final byte LBRCRJ_RIGHT = 2;
    protected static final byte LBRCRJ_BOTH = 3;
    protected boolean field_65_fSpecVanish;
    protected boolean field_66_fHasOldProps;
    protected boolean field_67_fSdtVanish;
    protected int field_68_wCharScale;
    
    protected CHPAbstractType() {
        this.field_2_hps = 20;
        this.field_8_cv = new Colorref();
        this.field_11_lidDefault = 1024;
        this.field_12_lidFE = 1024;
        this.field_20_hresi = new Hyphenation();
        this.field_23_shd = new ShadingDescriptor();
        this.field_24_brc = new BorderCode();
        this.field_36_fcPic = -1;
        this.field_40_hresiOld = new Hyphenation();
        this.field_42_dttmRMark = new DateAndTime();
        this.field_43_dttmRMarkDel = new DateAndTime();
        this.field_44_istd = 10;
        this.field_53_dttmPropRMark = new DateAndTime();
        this.field_58_dttmConflict = new DateAndTime();
        this.field_61_dttmDispFldRMark = new DateAndTime();
        this.field_62_xstDispFldRMark = new byte[0];
        this.field_68_wCharScale = 100;
    }
    
    @Override
    public String toString() {
        final StringBuilder builder = new StringBuilder();
        builder.append("[CHP]\n");
        builder.append("    .grpfChp              = ");
        builder.append(" (").append(this.getGrpfChp()).append(" )\n");
        builder.append("         .fBold                    = ").append(this.isFBold()).append('\n');
        builder.append("         .fItalic                  = ").append(this.isFItalic()).append('\n');
        builder.append("         .fRMarkDel                = ").append(this.isFRMarkDel()).append('\n');
        builder.append("         .fOutline                 = ").append(this.isFOutline()).append('\n');
        builder.append("         .fFldVanish               = ").append(this.isFFldVanish()).append('\n');
        builder.append("         .fSmallCaps               = ").append(this.isFSmallCaps()).append('\n');
        builder.append("         .fCaps                    = ").append(this.isFCaps()).append('\n');
        builder.append("         .fVanish                  = ").append(this.isFVanish()).append('\n');
        builder.append("         .fRMark                   = ").append(this.isFRMark()).append('\n');
        builder.append("         .fSpec                    = ").append(this.isFSpec()).append('\n');
        builder.append("         .fStrike                  = ").append(this.isFStrike()).append('\n');
        builder.append("         .fObj                     = ").append(this.isFObj()).append('\n');
        builder.append("         .fShadow                  = ").append(this.isFShadow()).append('\n');
        builder.append("         .fLowerCase               = ").append(this.isFLowerCase()).append('\n');
        builder.append("         .fData                    = ").append(this.isFData()).append('\n');
        builder.append("         .fOle2                    = ").append(this.isFOle2()).append('\n');
        builder.append("         .fEmboss                  = ").append(this.isFEmboss()).append('\n');
        builder.append("         .fImprint                 = ").append(this.isFImprint()).append('\n');
        builder.append("         .fDStrike                 = ").append(this.isFDStrike()).append('\n');
        builder.append("         .fUsePgsuSettings         = ").append(this.isFUsePgsuSettings()).append('\n');
        builder.append("         .fBoldBi                  = ").append(this.isFBoldBi()).append('\n');
        builder.append("         .fComplexScripts          = ").append(this.isFComplexScripts()).append('\n');
        builder.append("         .fItalicBi                = ").append(this.isFItalicBi()).append('\n');
        builder.append("         .fBiDi                    = ").append(this.isFBiDi()).append('\n');
        builder.append("    .hps                  = ");
        builder.append(" (").append(this.getHps()).append(" )\n");
        builder.append("    .ftcAscii             = ");
        builder.append(" (").append(this.getFtcAscii()).append(" )\n");
        builder.append("    .ftcFE                = ");
        builder.append(" (").append(this.getFtcFE()).append(" )\n");
        builder.append("    .ftcOther             = ");
        builder.append(" (").append(this.getFtcOther()).append(" )\n");
        builder.append("    .ftcBi                = ");
        builder.append(" (").append(this.getFtcBi()).append(" )\n");
        builder.append("    .dxaSpace             = ");
        builder.append(" (").append(this.getDxaSpace()).append(" )\n");
        builder.append("    .cv                   = ");
        builder.append(" (").append(this.getCv()).append(" )\n");
        builder.append("    .ico                  = ");
        builder.append(" (").append(this.getIco()).append(" )\n");
        builder.append("    .pctCharWidth         = ");
        builder.append(" (").append(this.getPctCharWidth()).append(" )\n");
        builder.append("    .lidDefault           = ");
        builder.append(" (").append(this.getLidDefault()).append(" )\n");
        builder.append("    .lidFE                = ");
        builder.append(" (").append(this.getLidFE()).append(" )\n");
        builder.append("    .kcd                  = ");
        builder.append(" (").append(this.getKcd()).append(" )\n");
        builder.append("    .fUndetermine         = ");
        builder.append(" (").append(this.getFUndetermine()).append(" )\n");
        builder.append("    .iss                  = ");
        builder.append(" (").append(this.getIss()).append(" )\n");
        builder.append("    .fSpecSymbol          = ");
        builder.append(" (").append(this.getFSpecSymbol()).append(" )\n");
        builder.append("    .idct                 = ");
        builder.append(" (").append(this.getIdct()).append(" )\n");
        builder.append("    .idctHint             = ");
        builder.append(" (").append(this.getIdctHint()).append(" )\n");
        builder.append("    .kul                  = ");
        builder.append(" (").append(this.getKul()).append(" )\n");
        builder.append("    .hresi                = ");
        builder.append(" (").append(this.getHresi()).append(" )\n");
        builder.append("    .hpsKern              = ");
        builder.append(" (").append(this.getHpsKern()).append(" )\n");
        builder.append("    .hpsPos               = ");
        builder.append(" (").append(this.getHpsPos()).append(" )\n");
        builder.append("    .shd                  = ");
        builder.append(" (").append(this.getShd()).append(" )\n");
        builder.append("    .brc                  = ");
        builder.append(" (").append(this.getBrc()).append(" )\n");
        builder.append("    .ibstRMark            = ");
        builder.append(" (").append(this.getIbstRMark()).append(" )\n");
        builder.append("    .sfxtText             = ");
        builder.append(" (").append(this.getSfxtText()).append(" )\n");
        builder.append("    .fDblBdr              = ");
        builder.append(" (").append(this.getFDblBdr()).append(" )\n");
        builder.append("    .fBorderWS            = ");
        builder.append(" (").append(this.getFBorderWS()).append(" )\n");
        builder.append("    .ufel                 = ");
        builder.append(" (").append(this.getUfel()).append(" )\n");
        builder.append("         .itypFELayout             = ").append(this.getItypFELayout()).append('\n');
        builder.append("         .fTNY                     = ").append(this.isFTNY()).append('\n');
        builder.append("         .fWarichu                 = ").append(this.isFWarichu()).append('\n');
        builder.append("         .fKumimoji                = ").append(this.isFKumimoji()).append('\n');
        builder.append("         .fRuby                    = ").append(this.isFRuby()).append('\n');
        builder.append("         .fLSFitText               = ").append(this.isFLSFitText()).append('\n');
        builder.append("         .spare                    = ").append(this.getSpare()).append('\n');
        builder.append("    .copt                 = ");
        builder.append(" (").append(this.getCopt()).append(" )\n");
        builder.append("         .iWarichuBracket          = ").append(this.getIWarichuBracket()).append('\n');
        builder.append("         .fWarichuNoOpenBracket     = ").append(this.isFWarichuNoOpenBracket()).append('\n');
        builder.append("         .fTNYCompress             = ").append(this.isFTNYCompress()).append('\n');
        builder.append("         .fTNYFetchTxm             = ").append(this.isFTNYFetchTxm()).append('\n');
        builder.append("         .fCellFitText             = ").append(this.isFCellFitText()).append('\n');
        builder.append("         .unused                   = ").append(this.isUnused()).append('\n');
        builder.append("    .hpsAsci              = ");
        builder.append(" (").append(this.getHpsAsci()).append(" )\n");
        builder.append("    .hpsFE                = ");
        builder.append(" (").append(this.getHpsFE()).append(" )\n");
        builder.append("    .hpsBi                = ");
        builder.append(" (").append(this.getHpsBi()).append(" )\n");
        builder.append("    .ftcSym               = ");
        builder.append(" (").append(this.getFtcSym()).append(" )\n");
        builder.append("    .xchSym               = ");
        builder.append(" (").append(this.getXchSym()).append(" )\n");
        builder.append("    .fcPic                = ");
        builder.append(" (").append(this.getFcPic()).append(" )\n");
        builder.append("    .fcObj                = ");
        builder.append(" (").append(this.getFcObj()).append(" )\n");
        builder.append("    .lTagObj              = ");
        builder.append(" (").append(this.getLTagObj()).append(" )\n");
        builder.append("    .fcData               = ");
        builder.append(" (").append(this.getFcData()).append(" )\n");
        builder.append("    .hresiOld             = ");
        builder.append(" (").append(this.getHresiOld()).append(" )\n");
        builder.append("    .ibstRMarkDel         = ");
        builder.append(" (").append(this.getIbstRMarkDel()).append(" )\n");
        builder.append("    .dttmRMark            = ");
        builder.append(" (").append(this.getDttmRMark()).append(" )\n");
        builder.append("    .dttmRMarkDel         = ");
        builder.append(" (").append(this.getDttmRMarkDel()).append(" )\n");
        builder.append("    .istd                 = ");
        builder.append(" (").append(this.getIstd()).append(" )\n");
        builder.append("    .idslRMReason         = ");
        builder.append(" (").append(this.getIdslRMReason()).append(" )\n");
        builder.append("    .idslReasonDel        = ");
        builder.append(" (").append(this.getIdslReasonDel()).append(" )\n");
        builder.append("    .cpg                  = ");
        builder.append(" (").append(this.getCpg()).append(" )\n");
        builder.append("    .Highlight            = ");
        builder.append(" (").append(this.getHighlight()).append(" )\n");
        builder.append("         .icoHighlight             = ").append(this.getIcoHighlight()).append('\n');
        builder.append("         .fHighlight               = ").append(this.isFHighlight()).append('\n');
        builder.append("    .CharsetFlags         = ");
        builder.append(" (").append(this.getCharsetFlags()).append(" )\n");
        builder.append("         .fChsDiff                 = ").append(this.isFChsDiff()).append('\n');
        builder.append("         .fMacChs                  = ").append(this.isFMacChs()).append('\n');
        builder.append("    .chse                 = ");
        builder.append(" (").append(this.getChse()).append(" )\n");
        builder.append("    .fPropRMark           = ");
        builder.append(" (").append(this.getFPropRMark()).append(" )\n");
        builder.append("    .ibstPropRMark        = ");
        builder.append(" (").append(this.getIbstPropRMark()).append(" )\n");
        builder.append("    .dttmPropRMark        = ");
        builder.append(" (").append(this.getDttmPropRMark()).append(" )\n");
        builder.append("    .fConflictOrig        = ");
        builder.append(" (").append(this.getFConflictOrig()).append(" )\n");
        builder.append("    .fConflictOtherDel    = ");
        builder.append(" (").append(this.getFConflictOtherDel()).append(" )\n");
        builder.append("    .wConflict            = ");
        builder.append(" (").append(this.getWConflict()).append(" )\n");
        builder.append("    .IbstConflict         = ");
        builder.append(" (").append(this.getIbstConflict()).append(" )\n");
        builder.append("    .dttmConflict         = ");
        builder.append(" (").append(this.getDttmConflict()).append(" )\n");
        builder.append("    .fDispFldRMark        = ");
        builder.append(" (").append(this.getFDispFldRMark()).append(" )\n");
        builder.append("    .ibstDispFldRMark     = ");
        builder.append(" (").append(this.getIbstDispFldRMark()).append(" )\n");
        builder.append("    .dttmDispFldRMark     = ");
        builder.append(" (").append(this.getDttmDispFldRMark()).append(" )\n");
        builder.append("    .xstDispFldRMark      = ");
        builder.append(" (").append(this.getXstDispFldRMark()).append(" )\n");
        builder.append("    .fcObjp               = ");
        builder.append(" (").append(this.getFcObjp()).append(" )\n");
        builder.append("    .lbrCRJ               = ");
        builder.append(" (").append(this.getLbrCRJ()).append(" )\n");
        builder.append("    .fSpecVanish          = ");
        builder.append(" (").append(this.getFSpecVanish()).append(" )\n");
        builder.append("    .fHasOldProps         = ");
        builder.append(" (").append(this.getFHasOldProps()).append(" )\n");
        builder.append("    .fSdtVanish           = ");
        builder.append(" (").append(this.getFSdtVanish()).append(" )\n");
        builder.append("    .wCharScale           = ");
        builder.append(" (").append(this.getWCharScale()).append(" )\n");
        builder.append("[/CHP]\n");
        return builder.toString();
    }
    
    @Internal
    public int getGrpfChp() {
        return this.field_1_grpfChp;
    }
    
    @Internal
    public void setGrpfChp(final int field_1_grpfChp) {
        this.field_1_grpfChp = field_1_grpfChp;
    }
    
    @Internal
    public int getHps() {
        return this.field_2_hps;
    }
    
    @Internal
    public void setHps(final int field_2_hps) {
        this.field_2_hps = field_2_hps;
    }
    
    @Internal
    public int getFtcAscii() {
        return this.field_3_ftcAscii;
    }
    
    @Internal
    public void setFtcAscii(final int field_3_ftcAscii) {
        this.field_3_ftcAscii = field_3_ftcAscii;
    }
    
    @Internal
    public int getFtcFE() {
        return this.field_4_ftcFE;
    }
    
    @Internal
    public void setFtcFE(final int field_4_ftcFE) {
        this.field_4_ftcFE = field_4_ftcFE;
    }
    
    @Internal
    public int getFtcOther() {
        return this.field_5_ftcOther;
    }
    
    @Internal
    public void setFtcOther(final int field_5_ftcOther) {
        this.field_5_ftcOther = field_5_ftcOther;
    }
    
    @Internal
    public int getFtcBi() {
        return this.field_6_ftcBi;
    }
    
    @Internal
    public void setFtcBi(final int field_6_ftcBi) {
        this.field_6_ftcBi = field_6_ftcBi;
    }
    
    @Internal
    public int getDxaSpace() {
        return this.field_7_dxaSpace;
    }
    
    @Internal
    public void setDxaSpace(final int field_7_dxaSpace) {
        this.field_7_dxaSpace = field_7_dxaSpace;
    }
    
    @Internal
    public Colorref getCv() {
        return this.field_8_cv;
    }
    
    @Internal
    public void setCv(final Colorref field_8_cv) {
        this.field_8_cv = field_8_cv;
    }
    
    @Internal
    public byte getIco() {
        return this.field_9_ico;
    }
    
    @Internal
    public void setIco(final byte field_9_ico) {
        this.field_9_ico = field_9_ico;
    }
    
    @Internal
    public int getPctCharWidth() {
        return this.field_10_pctCharWidth;
    }
    
    @Internal
    public void setPctCharWidth(final int field_10_pctCharWidth) {
        this.field_10_pctCharWidth = field_10_pctCharWidth;
    }
    
    @Internal
    public int getLidDefault() {
        return this.field_11_lidDefault;
    }
    
    @Internal
    public void setLidDefault(final int field_11_lidDefault) {
        this.field_11_lidDefault = field_11_lidDefault;
    }
    
    @Internal
    public int getLidFE() {
        return this.field_12_lidFE;
    }
    
    @Internal
    public void setLidFE(final int field_12_lidFE) {
        this.field_12_lidFE = field_12_lidFE;
    }
    
    @Internal
    public byte getKcd() {
        return this.field_13_kcd;
    }
    
    @Internal
    public void setKcd(final byte field_13_kcd) {
        this.field_13_kcd = field_13_kcd;
    }
    
    @Internal
    public boolean getFUndetermine() {
        return this.field_14_fUndetermine;
    }
    
    @Internal
    public void setFUndetermine(final boolean field_14_fUndetermine) {
        this.field_14_fUndetermine = field_14_fUndetermine;
    }
    
    @Internal
    public byte getIss() {
        return this.field_15_iss;
    }
    
    @Internal
    public void setIss(final byte field_15_iss) {
        this.field_15_iss = field_15_iss;
    }
    
    @Internal
    public boolean getFSpecSymbol() {
        return this.field_16_fSpecSymbol;
    }
    
    @Internal
    public void setFSpecSymbol(final boolean field_16_fSpecSymbol) {
        this.field_16_fSpecSymbol = field_16_fSpecSymbol;
    }
    
    @Internal
    public byte getIdct() {
        return this.field_17_idct;
    }
    
    @Internal
    public void setIdct(final byte field_17_idct) {
        this.field_17_idct = field_17_idct;
    }
    
    @Internal
    public byte getIdctHint() {
        return this.field_18_idctHint;
    }
    
    @Internal
    public void setIdctHint(final byte field_18_idctHint) {
        this.field_18_idctHint = field_18_idctHint;
    }
    
    @Internal
    public byte getKul() {
        return this.field_19_kul;
    }
    
    @Internal
    public void setKul(final byte field_19_kul) {
        this.field_19_kul = field_19_kul;
    }
    
    @Internal
    public Hyphenation getHresi() {
        return this.field_20_hresi;
    }
    
    @Internal
    public void setHresi(final Hyphenation field_20_hresi) {
        this.field_20_hresi = field_20_hresi;
    }
    
    @Internal
    public int getHpsKern() {
        return this.field_21_hpsKern;
    }
    
    @Internal
    public void setHpsKern(final int field_21_hpsKern) {
        this.field_21_hpsKern = field_21_hpsKern;
    }
    
    @Internal
    public short getHpsPos() {
        return this.field_22_hpsPos;
    }
    
    @Internal
    public void setHpsPos(final short field_22_hpsPos) {
        this.field_22_hpsPos = field_22_hpsPos;
    }
    
    @Internal
    public ShadingDescriptor getShd() {
        return this.field_23_shd;
    }
    
    @Internal
    public void setShd(final ShadingDescriptor field_23_shd) {
        this.field_23_shd = field_23_shd;
    }
    
    @Internal
    public BorderCode getBrc() {
        return this.field_24_brc;
    }
    
    @Internal
    public void setBrc(final BorderCode field_24_brc) {
        this.field_24_brc = field_24_brc;
    }
    
    @Internal
    public int getIbstRMark() {
        return this.field_25_ibstRMark;
    }
    
    @Internal
    public void setIbstRMark(final int field_25_ibstRMark) {
        this.field_25_ibstRMark = field_25_ibstRMark;
    }
    
    @Internal
    public byte getSfxtText() {
        return this.field_26_sfxtText;
    }
    
    @Internal
    public void setSfxtText(final byte field_26_sfxtText) {
        this.field_26_sfxtText = field_26_sfxtText;
    }
    
    @Internal
    public boolean getFDblBdr() {
        return this.field_27_fDblBdr;
    }
    
    @Internal
    public void setFDblBdr(final boolean field_27_fDblBdr) {
        this.field_27_fDblBdr = field_27_fDblBdr;
    }
    
    @Internal
    public boolean getFBorderWS() {
        return this.field_28_fBorderWS;
    }
    
    @Internal
    public void setFBorderWS(final boolean field_28_fBorderWS) {
        this.field_28_fBorderWS = field_28_fBorderWS;
    }
    
    @Internal
    public short getUfel() {
        return this.field_29_ufel;
    }
    
    @Internal
    public void setUfel(final short field_29_ufel) {
        this.field_29_ufel = field_29_ufel;
    }
    
    @Internal
    public byte getCopt() {
        return this.field_30_copt;
    }
    
    @Internal
    public void setCopt(final byte field_30_copt) {
        this.field_30_copt = field_30_copt;
    }
    
    @Internal
    public int getHpsAsci() {
        return this.field_31_hpsAsci;
    }
    
    @Internal
    public void setHpsAsci(final int field_31_hpsAsci) {
        this.field_31_hpsAsci = field_31_hpsAsci;
    }
    
    @Internal
    public int getHpsFE() {
        return this.field_32_hpsFE;
    }
    
    @Internal
    public void setHpsFE(final int field_32_hpsFE) {
        this.field_32_hpsFE = field_32_hpsFE;
    }
    
    @Internal
    public int getHpsBi() {
        return this.field_33_hpsBi;
    }
    
    @Internal
    public void setHpsBi(final int field_33_hpsBi) {
        this.field_33_hpsBi = field_33_hpsBi;
    }
    
    @Internal
    public int getFtcSym() {
        return this.field_34_ftcSym;
    }
    
    @Internal
    public void setFtcSym(final int field_34_ftcSym) {
        this.field_34_ftcSym = field_34_ftcSym;
    }
    
    @Internal
    public int getXchSym() {
        return this.field_35_xchSym;
    }
    
    @Internal
    public void setXchSym(final int field_35_xchSym) {
        this.field_35_xchSym = field_35_xchSym;
    }
    
    @Internal
    public int getFcPic() {
        return this.field_36_fcPic;
    }
    
    @Internal
    public void setFcPic(final int field_36_fcPic) {
        this.field_36_fcPic = field_36_fcPic;
    }
    
    @Internal
    public int getFcObj() {
        return this.field_37_fcObj;
    }
    
    @Internal
    public void setFcObj(final int field_37_fcObj) {
        this.field_37_fcObj = field_37_fcObj;
    }
    
    @Internal
    public int getLTagObj() {
        return this.field_38_lTagObj;
    }
    
    @Internal
    public void setLTagObj(final int field_38_lTagObj) {
        this.field_38_lTagObj = field_38_lTagObj;
    }
    
    @Internal
    public int getFcData() {
        return this.field_39_fcData;
    }
    
    @Internal
    public void setFcData(final int field_39_fcData) {
        this.field_39_fcData = field_39_fcData;
    }
    
    @Internal
    public Hyphenation getHresiOld() {
        return this.field_40_hresiOld;
    }
    
    @Internal
    public void setHresiOld(final Hyphenation field_40_hresiOld) {
        this.field_40_hresiOld = field_40_hresiOld;
    }
    
    @Internal
    public int getIbstRMarkDel() {
        return this.field_41_ibstRMarkDel;
    }
    
    @Internal
    public void setIbstRMarkDel(final int field_41_ibstRMarkDel) {
        this.field_41_ibstRMarkDel = field_41_ibstRMarkDel;
    }
    
    @Internal
    public DateAndTime getDttmRMark() {
        return this.field_42_dttmRMark;
    }
    
    @Internal
    public void setDttmRMark(final DateAndTime field_42_dttmRMark) {
        this.field_42_dttmRMark = field_42_dttmRMark;
    }
    
    @Internal
    public DateAndTime getDttmRMarkDel() {
        return this.field_43_dttmRMarkDel;
    }
    
    @Internal
    public void setDttmRMarkDel(final DateAndTime field_43_dttmRMarkDel) {
        this.field_43_dttmRMarkDel = field_43_dttmRMarkDel;
    }
    
    @Internal
    public int getIstd() {
        return this.field_44_istd;
    }
    
    @Internal
    public void setIstd(final int field_44_istd) {
        this.field_44_istd = field_44_istd;
    }
    
    @Internal
    public int getIdslRMReason() {
        return this.field_45_idslRMReason;
    }
    
    @Internal
    public void setIdslRMReason(final int field_45_idslRMReason) {
        this.field_45_idslRMReason = field_45_idslRMReason;
    }
    
    @Internal
    public int getIdslReasonDel() {
        return this.field_46_idslReasonDel;
    }
    
    @Internal
    public void setIdslReasonDel(final int field_46_idslReasonDel) {
        this.field_46_idslReasonDel = field_46_idslReasonDel;
    }
    
    @Internal
    public int getCpg() {
        return this.field_47_cpg;
    }
    
    @Internal
    public void setCpg(final int field_47_cpg) {
        this.field_47_cpg = field_47_cpg;
    }
    
    @Internal
    public short getHighlight() {
        return this.field_48_Highlight;
    }
    
    @Internal
    public void setHighlight(final short field_48_Highlight) {
        this.field_48_Highlight = field_48_Highlight;
    }
    
    @Internal
    public short getCharsetFlags() {
        return this.field_49_CharsetFlags;
    }
    
    @Internal
    public void setCharsetFlags(final short field_49_CharsetFlags) {
        this.field_49_CharsetFlags = field_49_CharsetFlags;
    }
    
    @Internal
    public short getChse() {
        return this.field_50_chse;
    }
    
    @Internal
    public void setChse(final short field_50_chse) {
        this.field_50_chse = field_50_chse;
    }
    
    @Internal
    public boolean getFPropRMark() {
        return this.field_51_fPropRMark;
    }
    
    @Internal
    public void setFPropRMark(final boolean field_51_fPropRMark) {
        this.field_51_fPropRMark = field_51_fPropRMark;
    }
    
    @Internal
    public int getIbstPropRMark() {
        return this.field_52_ibstPropRMark;
    }
    
    @Internal
    public void setIbstPropRMark(final int field_52_ibstPropRMark) {
        this.field_52_ibstPropRMark = field_52_ibstPropRMark;
    }
    
    @Internal
    public DateAndTime getDttmPropRMark() {
        return this.field_53_dttmPropRMark;
    }
    
    @Internal
    public void setDttmPropRMark(final DateAndTime field_53_dttmPropRMark) {
        this.field_53_dttmPropRMark = field_53_dttmPropRMark;
    }
    
    @Internal
    public boolean getFConflictOrig() {
        return this.field_54_fConflictOrig;
    }
    
    @Internal
    public void setFConflictOrig(final boolean field_54_fConflictOrig) {
        this.field_54_fConflictOrig = field_54_fConflictOrig;
    }
    
    @Internal
    public boolean getFConflictOtherDel() {
        return this.field_55_fConflictOtherDel;
    }
    
    @Internal
    public void setFConflictOtherDel(final boolean field_55_fConflictOtherDel) {
        this.field_55_fConflictOtherDel = field_55_fConflictOtherDel;
    }
    
    @Internal
    public int getWConflict() {
        return this.field_56_wConflict;
    }
    
    @Internal
    public void setWConflict(final int field_56_wConflict) {
        this.field_56_wConflict = field_56_wConflict;
    }
    
    @Internal
    public int getIbstConflict() {
        return this.field_57_IbstConflict;
    }
    
    @Internal
    public void setIbstConflict(final int field_57_IbstConflict) {
        this.field_57_IbstConflict = field_57_IbstConflict;
    }
    
    @Internal
    public DateAndTime getDttmConflict() {
        return this.field_58_dttmConflict;
    }
    
    @Internal
    public void setDttmConflict(final DateAndTime field_58_dttmConflict) {
        this.field_58_dttmConflict = field_58_dttmConflict;
    }
    
    @Internal
    public boolean getFDispFldRMark() {
        return this.field_59_fDispFldRMark;
    }
    
    @Internal
    public void setFDispFldRMark(final boolean field_59_fDispFldRMark) {
        this.field_59_fDispFldRMark = field_59_fDispFldRMark;
    }
    
    @Internal
    public int getIbstDispFldRMark() {
        return this.field_60_ibstDispFldRMark;
    }
    
    @Internal
    public void setIbstDispFldRMark(final int field_60_ibstDispFldRMark) {
        this.field_60_ibstDispFldRMark = field_60_ibstDispFldRMark;
    }
    
    @Internal
    public DateAndTime getDttmDispFldRMark() {
        return this.field_61_dttmDispFldRMark;
    }
    
    @Internal
    public void setDttmDispFldRMark(final DateAndTime field_61_dttmDispFldRMark) {
        this.field_61_dttmDispFldRMark = field_61_dttmDispFldRMark;
    }
    
    @Internal
    public byte[] getXstDispFldRMark() {
        return this.field_62_xstDispFldRMark;
    }
    
    @Internal
    public void setXstDispFldRMark(final byte[] field_62_xstDispFldRMark) {
        this.field_62_xstDispFldRMark = field_62_xstDispFldRMark;
    }
    
    @Internal
    public int getFcObjp() {
        return this.field_63_fcObjp;
    }
    
    @Internal
    public void setFcObjp(final int field_63_fcObjp) {
        this.field_63_fcObjp = field_63_fcObjp;
    }
    
    @Internal
    public byte getLbrCRJ() {
        return this.field_64_lbrCRJ;
    }
    
    @Internal
    public void setLbrCRJ(final byte field_64_lbrCRJ) {
        this.field_64_lbrCRJ = field_64_lbrCRJ;
    }
    
    @Internal
    public boolean getFSpecVanish() {
        return this.field_65_fSpecVanish;
    }
    
    @Internal
    public void setFSpecVanish(final boolean field_65_fSpecVanish) {
        this.field_65_fSpecVanish = field_65_fSpecVanish;
    }
    
    @Internal
    public boolean getFHasOldProps() {
        return this.field_66_fHasOldProps;
    }
    
    @Internal
    public void setFHasOldProps(final boolean field_66_fHasOldProps) {
        this.field_66_fHasOldProps = field_66_fHasOldProps;
    }
    
    @Internal
    public boolean getFSdtVanish() {
        return this.field_67_fSdtVanish;
    }
    
    @Internal
    public void setFSdtVanish(final boolean field_67_fSdtVanish) {
        this.field_67_fSdtVanish = field_67_fSdtVanish;
    }
    
    @Internal
    public int getWCharScale() {
        return this.field_68_wCharScale;
    }
    
    @Internal
    public void setWCharScale(final int field_68_wCharScale) {
        this.field_68_wCharScale = field_68_wCharScale;
    }
    
    @Internal
    public void setFBold(final boolean value) {
        this.field_1_grpfChp = CHPAbstractType.fBold.setBoolean(this.field_1_grpfChp, value);
    }
    
    @Internal
    public boolean isFBold() {
        return CHPAbstractType.fBold.isSet(this.field_1_grpfChp);
    }
    
    @Internal
    public void setFItalic(final boolean value) {
        this.field_1_grpfChp = CHPAbstractType.fItalic.setBoolean(this.field_1_grpfChp, value);
    }
    
    @Internal
    public boolean isFItalic() {
        return CHPAbstractType.fItalic.isSet(this.field_1_grpfChp);
    }
    
    @Internal
    public void setFRMarkDel(final boolean value) {
        this.field_1_grpfChp = CHPAbstractType.fRMarkDel.setBoolean(this.field_1_grpfChp, value);
    }
    
    @Internal
    public boolean isFRMarkDel() {
        return CHPAbstractType.fRMarkDel.isSet(this.field_1_grpfChp);
    }
    
    @Internal
    public void setFOutline(final boolean value) {
        this.field_1_grpfChp = CHPAbstractType.fOutline.setBoolean(this.field_1_grpfChp, value);
    }
    
    @Internal
    public boolean isFOutline() {
        return CHPAbstractType.fOutline.isSet(this.field_1_grpfChp);
    }
    
    @Internal
    public void setFFldVanish(final boolean value) {
        this.field_1_grpfChp = CHPAbstractType.fFldVanish.setBoolean(this.field_1_grpfChp, value);
    }
    
    @Internal
    public boolean isFFldVanish() {
        return CHPAbstractType.fFldVanish.isSet(this.field_1_grpfChp);
    }
    
    @Internal
    public void setFSmallCaps(final boolean value) {
        this.field_1_grpfChp = CHPAbstractType.fSmallCaps.setBoolean(this.field_1_grpfChp, value);
    }
    
    @Internal
    public boolean isFSmallCaps() {
        return CHPAbstractType.fSmallCaps.isSet(this.field_1_grpfChp);
    }
    
    @Internal
    public void setFCaps(final boolean value) {
        this.field_1_grpfChp = CHPAbstractType.fCaps.setBoolean(this.field_1_grpfChp, value);
    }
    
    @Internal
    public boolean isFCaps() {
        return CHPAbstractType.fCaps.isSet(this.field_1_grpfChp);
    }
    
    @Internal
    public void setFVanish(final boolean value) {
        this.field_1_grpfChp = CHPAbstractType.fVanish.setBoolean(this.field_1_grpfChp, value);
    }
    
    @Internal
    public boolean isFVanish() {
        return CHPAbstractType.fVanish.isSet(this.field_1_grpfChp);
    }
    
    @Internal
    public void setFRMark(final boolean value) {
        this.field_1_grpfChp = CHPAbstractType.fRMark.setBoolean(this.field_1_grpfChp, value);
    }
    
    @Internal
    public boolean isFRMark() {
        return CHPAbstractType.fRMark.isSet(this.field_1_grpfChp);
    }
    
    @Internal
    public void setFSpec(final boolean value) {
        this.field_1_grpfChp = CHPAbstractType.fSpec.setBoolean(this.field_1_grpfChp, value);
    }
    
    @Internal
    public boolean isFSpec() {
        return CHPAbstractType.fSpec.isSet(this.field_1_grpfChp);
    }
    
    @Internal
    public void setFStrike(final boolean value) {
        this.field_1_grpfChp = CHPAbstractType.fStrike.setBoolean(this.field_1_grpfChp, value);
    }
    
    @Internal
    public boolean isFStrike() {
        return CHPAbstractType.fStrike.isSet(this.field_1_grpfChp);
    }
    
    @Internal
    public void setFObj(final boolean value) {
        this.field_1_grpfChp = CHPAbstractType.fObj.setBoolean(this.field_1_grpfChp, value);
    }
    
    @Internal
    public boolean isFObj() {
        return CHPAbstractType.fObj.isSet(this.field_1_grpfChp);
    }
    
    @Internal
    public void setFShadow(final boolean value) {
        this.field_1_grpfChp = CHPAbstractType.fShadow.setBoolean(this.field_1_grpfChp, value);
    }
    
    @Internal
    public boolean isFShadow() {
        return CHPAbstractType.fShadow.isSet(this.field_1_grpfChp);
    }
    
    @Internal
    public void setFLowerCase(final boolean value) {
        this.field_1_grpfChp = CHPAbstractType.fLowerCase.setBoolean(this.field_1_grpfChp, value);
    }
    
    @Internal
    public boolean isFLowerCase() {
        return CHPAbstractType.fLowerCase.isSet(this.field_1_grpfChp);
    }
    
    @Internal
    public void setFData(final boolean value) {
        this.field_1_grpfChp = CHPAbstractType.fData.setBoolean(this.field_1_grpfChp, value);
    }
    
    @Internal
    public boolean isFData() {
        return CHPAbstractType.fData.isSet(this.field_1_grpfChp);
    }
    
    @Internal
    public void setFOle2(final boolean value) {
        this.field_1_grpfChp = CHPAbstractType.fOle2.setBoolean(this.field_1_grpfChp, value);
    }
    
    @Internal
    public boolean isFOle2() {
        return CHPAbstractType.fOle2.isSet(this.field_1_grpfChp);
    }
    
    @Internal
    public void setFEmboss(final boolean value) {
        this.field_1_grpfChp = CHPAbstractType.fEmboss.setBoolean(this.field_1_grpfChp, value);
    }
    
    @Internal
    public boolean isFEmboss() {
        return CHPAbstractType.fEmboss.isSet(this.field_1_grpfChp);
    }
    
    @Internal
    public void setFImprint(final boolean value) {
        this.field_1_grpfChp = CHPAbstractType.fImprint.setBoolean(this.field_1_grpfChp, value);
    }
    
    @Internal
    public boolean isFImprint() {
        return CHPAbstractType.fImprint.isSet(this.field_1_grpfChp);
    }
    
    @Internal
    public void setFDStrike(final boolean value) {
        this.field_1_grpfChp = CHPAbstractType.fDStrike.setBoolean(this.field_1_grpfChp, value);
    }
    
    @Internal
    public boolean isFDStrike() {
        return CHPAbstractType.fDStrike.isSet(this.field_1_grpfChp);
    }
    
    @Internal
    public void setFUsePgsuSettings(final boolean value) {
        this.field_1_grpfChp = CHPAbstractType.fUsePgsuSettings.setBoolean(this.field_1_grpfChp, value);
    }
    
    @Internal
    public boolean isFUsePgsuSettings() {
        return CHPAbstractType.fUsePgsuSettings.isSet(this.field_1_grpfChp);
    }
    
    @Internal
    public void setFBoldBi(final boolean value) {
        this.field_1_grpfChp = CHPAbstractType.fBoldBi.setBoolean(this.field_1_grpfChp, value);
    }
    
    @Internal
    public boolean isFBoldBi() {
        return CHPAbstractType.fBoldBi.isSet(this.field_1_grpfChp);
    }
    
    @Internal
    public void setFComplexScripts(final boolean value) {
        this.field_1_grpfChp = CHPAbstractType.fComplexScripts.setBoolean(this.field_1_grpfChp, value);
    }
    
    @Internal
    public boolean isFComplexScripts() {
        return CHPAbstractType.fComplexScripts.isSet(this.field_1_grpfChp);
    }
    
    @Internal
    public void setFItalicBi(final boolean value) {
        this.field_1_grpfChp = CHPAbstractType.fItalicBi.setBoolean(this.field_1_grpfChp, value);
    }
    
    @Internal
    public boolean isFItalicBi() {
        return CHPAbstractType.fItalicBi.isSet(this.field_1_grpfChp);
    }
    
    @Internal
    public void setFBiDi(final boolean value) {
        this.field_1_grpfChp = CHPAbstractType.fBiDi.setBoolean(this.field_1_grpfChp, value);
    }
    
    @Internal
    public boolean isFBiDi() {
        return CHPAbstractType.fBiDi.isSet(this.field_1_grpfChp);
    }
    
    @Internal
    public void setItypFELayout(final short value) {
        this.field_29_ufel = (short)CHPAbstractType.itypFELayout.setValue(this.field_29_ufel, value);
    }
    
    @Internal
    public short getItypFELayout() {
        return (short)CHPAbstractType.itypFELayout.getValue(this.field_29_ufel);
    }
    
    @Internal
    public void setFTNY(final boolean value) {
        this.field_29_ufel = (short)CHPAbstractType.fTNY.setBoolean(this.field_29_ufel, value);
    }
    
    @Internal
    public boolean isFTNY() {
        return CHPAbstractType.fTNY.isSet(this.field_29_ufel);
    }
    
    @Internal
    public void setFWarichu(final boolean value) {
        this.field_29_ufel = (short)CHPAbstractType.fWarichu.setBoolean(this.field_29_ufel, value);
    }
    
    @Internal
    public boolean isFWarichu() {
        return CHPAbstractType.fWarichu.isSet(this.field_29_ufel);
    }
    
    @Internal
    public void setFKumimoji(final boolean value) {
        this.field_29_ufel = (short)CHPAbstractType.fKumimoji.setBoolean(this.field_29_ufel, value);
    }
    
    @Internal
    public boolean isFKumimoji() {
        return CHPAbstractType.fKumimoji.isSet(this.field_29_ufel);
    }
    
    @Internal
    public void setFRuby(final boolean value) {
        this.field_29_ufel = (short)CHPAbstractType.fRuby.setBoolean(this.field_29_ufel, value);
    }
    
    @Internal
    public boolean isFRuby() {
        return CHPAbstractType.fRuby.isSet(this.field_29_ufel);
    }
    
    @Internal
    public void setFLSFitText(final boolean value) {
        this.field_29_ufel = (short)CHPAbstractType.fLSFitText.setBoolean(this.field_29_ufel, value);
    }
    
    @Internal
    public boolean isFLSFitText() {
        return CHPAbstractType.fLSFitText.isSet(this.field_29_ufel);
    }
    
    @Internal
    public void setSpare(final byte value) {
        this.field_29_ufel = (short)CHPAbstractType.spare.setValue(this.field_29_ufel, value);
    }
    
    @Internal
    public byte getSpare() {
        return (byte)CHPAbstractType.spare.getValue(this.field_29_ufel);
    }
    
    @Internal
    public void setIWarichuBracket(final byte value) {
        this.field_30_copt = (byte)CHPAbstractType.iWarichuBracket.setValue(this.field_30_copt, value);
    }
    
    @Internal
    public byte getIWarichuBracket() {
        return (byte)CHPAbstractType.iWarichuBracket.getValue(this.field_30_copt);
    }
    
    @Internal
    public void setFWarichuNoOpenBracket(final boolean value) {
        this.field_30_copt = (byte)CHPAbstractType.fWarichuNoOpenBracket.setBoolean(this.field_30_copt, value);
    }
    
    @Internal
    public boolean isFWarichuNoOpenBracket() {
        return CHPAbstractType.fWarichuNoOpenBracket.isSet(this.field_30_copt);
    }
    
    @Internal
    public void setFTNYCompress(final boolean value) {
        this.field_30_copt = (byte)CHPAbstractType.fTNYCompress.setBoolean(this.field_30_copt, value);
    }
    
    @Internal
    public boolean isFTNYCompress() {
        return CHPAbstractType.fTNYCompress.isSet(this.field_30_copt);
    }
    
    @Internal
    public void setFTNYFetchTxm(final boolean value) {
        this.field_30_copt = (byte)CHPAbstractType.fTNYFetchTxm.setBoolean(this.field_30_copt, value);
    }
    
    @Internal
    public boolean isFTNYFetchTxm() {
        return CHPAbstractType.fTNYFetchTxm.isSet(this.field_30_copt);
    }
    
    @Internal
    public void setFCellFitText(final boolean value) {
        this.field_30_copt = (byte)CHPAbstractType.fCellFitText.setBoolean(this.field_30_copt, value);
    }
    
    @Internal
    public boolean isFCellFitText() {
        return CHPAbstractType.fCellFitText.isSet(this.field_30_copt);
    }
    
    @Internal
    public void setUnused(final boolean value) {
        this.field_30_copt = (byte)CHPAbstractType.unused.setBoolean(this.field_30_copt, value);
    }
    
    @Internal
    public boolean isUnused() {
        return CHPAbstractType.unused.isSet(this.field_30_copt);
    }
    
    @Internal
    public void setIcoHighlight(final byte value) {
        this.field_48_Highlight = (short)CHPAbstractType.icoHighlight.setValue(this.field_48_Highlight, value);
    }
    
    @Internal
    public byte getIcoHighlight() {
        return (byte)CHPAbstractType.icoHighlight.getValue(this.field_48_Highlight);
    }
    
    @Internal
    public void setFHighlight(final boolean value) {
        this.field_48_Highlight = (short)CHPAbstractType.fHighlight.setBoolean(this.field_48_Highlight, value);
    }
    
    @Internal
    public boolean isFHighlight() {
        return CHPAbstractType.fHighlight.isSet(this.field_48_Highlight);
    }
    
    @Internal
    public void setFChsDiff(final boolean value) {
        this.field_49_CharsetFlags = (short)CHPAbstractType.fChsDiff.setBoolean(this.field_49_CharsetFlags, value);
    }
    
    @Internal
    public boolean isFChsDiff() {
        return CHPAbstractType.fChsDiff.isSet(this.field_49_CharsetFlags);
    }
    
    @Internal
    public void setFMacChs(final boolean value) {
        this.field_49_CharsetFlags = (short)CHPAbstractType.fMacChs.setBoolean(this.field_49_CharsetFlags, value);
    }
    
    @Internal
    public boolean isFMacChs() {
        return CHPAbstractType.fMacChs.isSet(this.field_49_CharsetFlags);
    }
    
    static {
        CHPAbstractType.fBold = new BitField(1);
        CHPAbstractType.fItalic = new BitField(2);
        CHPAbstractType.fRMarkDel = new BitField(4);
        CHPAbstractType.fOutline = new BitField(8);
        CHPAbstractType.fFldVanish = new BitField(16);
        CHPAbstractType.fSmallCaps = new BitField(32);
        CHPAbstractType.fCaps = new BitField(64);
        CHPAbstractType.fVanish = new BitField(128);
        CHPAbstractType.fRMark = new BitField(256);
        CHPAbstractType.fSpec = new BitField(512);
        CHPAbstractType.fStrike = new BitField(1024);
        CHPAbstractType.fObj = new BitField(2048);
        CHPAbstractType.fShadow = new BitField(4096);
        CHPAbstractType.fLowerCase = new BitField(8192);
        CHPAbstractType.fData = new BitField(16384);
        CHPAbstractType.fOle2 = new BitField(32768);
        CHPAbstractType.fEmboss = new BitField(65536);
        CHPAbstractType.fImprint = new BitField(131072);
        CHPAbstractType.fDStrike = new BitField(262144);
        CHPAbstractType.fUsePgsuSettings = new BitField(524288);
        CHPAbstractType.fBoldBi = new BitField(1048576);
        CHPAbstractType.fComplexScripts = new BitField(2097152);
        CHPAbstractType.fItalicBi = new BitField(4194304);
        CHPAbstractType.fBiDi = new BitField(8388608);
        CHPAbstractType.itypFELayout = new BitField(255);
        CHPAbstractType.fTNY = new BitField(256);
        CHPAbstractType.fWarichu = new BitField(512);
        CHPAbstractType.fKumimoji = new BitField(1024);
        CHPAbstractType.fRuby = new BitField(2048);
        CHPAbstractType.fLSFitText = new BitField(4096);
        CHPAbstractType.spare = new BitField(57344);
        CHPAbstractType.iWarichuBracket = new BitField(7);
        CHPAbstractType.fWarichuNoOpenBracket = new BitField(8);
        CHPAbstractType.fTNYCompress = new BitField(16);
        CHPAbstractType.fTNYFetchTxm = new BitField(32);
        CHPAbstractType.fCellFitText = new BitField(64);
        CHPAbstractType.unused = new BitField(128);
        CHPAbstractType.icoHighlight = new BitField(31);
        CHPAbstractType.fHighlight = new BitField(32);
        CHPAbstractType.fChsDiff = new BitField(1);
        CHPAbstractType.fMacChs = new BitField(32);
    }
}
