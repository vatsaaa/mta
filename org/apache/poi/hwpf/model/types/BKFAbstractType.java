// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hwpf.model.types;

import org.apache.poi.util.LittleEndian;
import org.apache.poi.util.BitField;
import org.apache.poi.util.Internal;

@Internal
public abstract class BKFAbstractType
{
    protected short field_1_ibkl;
    protected short field_2_bkf_flags;
    private static BitField itcFirst;
    private static BitField fPub;
    private static BitField itcLim;
    private static BitField fCol;
    
    protected BKFAbstractType() {
    }
    
    protected void fillFields(final byte[] data, final int offset) {
        this.field_1_ibkl = LittleEndian.getShort(data, 0 + offset);
        this.field_2_bkf_flags = LittleEndian.getShort(data, 2 + offset);
    }
    
    public void serialize(final byte[] data, final int offset) {
        LittleEndian.putShort(data, 0 + offset, this.field_1_ibkl);
        LittleEndian.putShort(data, 2 + offset, this.field_2_bkf_flags);
    }
    
    public static int getSize() {
        return 4;
    }
    
    @Override
    public String toString() {
        final StringBuilder builder = new StringBuilder();
        builder.append("[BKF]\n");
        builder.append("    .ibkl                 = ");
        builder.append(" (").append(this.getIbkl()).append(" )\n");
        builder.append("    .bkf_flags            = ");
        builder.append(" (").append(this.getBkf_flags()).append(" )\n");
        builder.append("         .itcFirst                 = ").append(this.getItcFirst()).append('\n');
        builder.append("         .fPub                     = ").append(this.isFPub()).append('\n');
        builder.append("         .itcLim                   = ").append(this.getItcLim()).append('\n');
        builder.append("         .fCol                     = ").append(this.isFCol()).append('\n');
        builder.append("[/BKF]\n");
        return builder.toString();
    }
    
    public short getIbkl() {
        return this.field_1_ibkl;
    }
    
    public void setIbkl(final short field_1_ibkl) {
        this.field_1_ibkl = field_1_ibkl;
    }
    
    public short getBkf_flags() {
        return this.field_2_bkf_flags;
    }
    
    public void setBkf_flags(final short field_2_bkf_flags) {
        this.field_2_bkf_flags = field_2_bkf_flags;
    }
    
    public void setItcFirst(final byte value) {
        this.field_2_bkf_flags = (short)BKFAbstractType.itcFirst.setValue(this.field_2_bkf_flags, value);
    }
    
    public byte getItcFirst() {
        return (byte)BKFAbstractType.itcFirst.getValue(this.field_2_bkf_flags);
    }
    
    public void setFPub(final boolean value) {
        this.field_2_bkf_flags = (short)BKFAbstractType.fPub.setBoolean(this.field_2_bkf_flags, value);
    }
    
    public boolean isFPub() {
        return BKFAbstractType.fPub.isSet(this.field_2_bkf_flags);
    }
    
    public void setItcLim(final byte value) {
        this.field_2_bkf_flags = (short)BKFAbstractType.itcLim.setValue(this.field_2_bkf_flags, value);
    }
    
    public byte getItcLim() {
        return (byte)BKFAbstractType.itcLim.getValue(this.field_2_bkf_flags);
    }
    
    public void setFCol(final boolean value) {
        this.field_2_bkf_flags = (short)BKFAbstractType.fCol.setBoolean(this.field_2_bkf_flags, value);
    }
    
    public boolean isFCol() {
        return BKFAbstractType.fCol.isSet(this.field_2_bkf_flags);
    }
    
    static {
        BKFAbstractType.itcFirst = new BitField(127);
        BKFAbstractType.fPub = new BitField(128);
        BKFAbstractType.itcLim = new BitField(32512);
        BKFAbstractType.fCol = new BitField(32768);
    }
}
