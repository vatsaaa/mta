// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hwpf.model.types;

import org.apache.poi.util.LittleEndian;
import org.apache.poi.util.BitField;
import org.apache.poi.util.Internal;

@Internal
public abstract class SHD80AbstractType
{
    protected short field_1_value;
    private static BitField icoFore;
    private static BitField icoBack;
    private static BitField ipat;
    
    protected SHD80AbstractType() {
    }
    
    protected void fillFields(final byte[] data, final int offset) {
        this.field_1_value = LittleEndian.getShort(data, 0 + offset);
    }
    
    public void serialize(final byte[] data, final int offset) {
        LittleEndian.putShort(data, 0 + offset, this.field_1_value);
    }
    
    public static int getSize() {
        return 2;
    }
    
    @Override
    public String toString() {
        final StringBuilder builder = new StringBuilder();
        builder.append("[SHD80]\n");
        builder.append("    .value                = ");
        builder.append(" (").append(this.getValue()).append(" )\n");
        builder.append("         .icoFore                  = ").append(this.getIcoFore()).append('\n');
        builder.append("         .icoBack                  = ").append(this.getIcoBack()).append('\n');
        builder.append("         .ipat                     = ").append(this.getIpat()).append('\n');
        builder.append("[/SHD80]\n");
        return builder.toString();
    }
    
    @Internal
    public short getValue() {
        return this.field_1_value;
    }
    
    @Internal
    public void setValue(final short field_1_value) {
        this.field_1_value = field_1_value;
    }
    
    @Internal
    public void setIcoFore(final byte value) {
        this.field_1_value = (short)SHD80AbstractType.icoFore.setValue(this.field_1_value, value);
    }
    
    @Internal
    public byte getIcoFore() {
        return (byte)SHD80AbstractType.icoFore.getValue(this.field_1_value);
    }
    
    @Internal
    public void setIcoBack(final byte value) {
        this.field_1_value = (short)SHD80AbstractType.icoBack.setValue(this.field_1_value, value);
    }
    
    @Internal
    public byte getIcoBack() {
        return (byte)SHD80AbstractType.icoBack.getValue(this.field_1_value);
    }
    
    @Internal
    public void setIpat(final byte value) {
        this.field_1_value = (short)SHD80AbstractType.ipat.setValue(this.field_1_value, value);
    }
    
    @Internal
    public byte getIpat() {
        return (byte)SHD80AbstractType.ipat.getValue(this.field_1_value);
    }
    
    static {
        SHD80AbstractType.icoFore = new BitField(31);
        SHD80AbstractType.icoBack = new BitField(992);
        SHD80AbstractType.ipat = new BitField(64512);
    }
}
