// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hwpf.model.types;

import org.apache.poi.hwpf.usermodel.BorderCode;
import org.apache.poi.hwpf.usermodel.ShadingDescriptor;
import org.apache.poi.util.BitField;
import org.apache.poi.util.Internal;

@Internal
public abstract class TCAbstractType
{
    protected short field_1_rgf;
    private static BitField fFirstMerged;
    private static BitField fMerged;
    private static BitField fVertical;
    private static BitField fBackward;
    private static BitField fRotateFont;
    private static BitField fVertMerge;
    private static BitField fVertRestart;
    private static BitField vertAlign;
    private static BitField ftsWidth;
    private static BitField fFitText;
    private static BitField fNoWrap;
    private static BitField fUnused;
    protected short field_2_wWidth;
    protected ShadingDescriptor field_3_shd;
    protected short field_4_wCellPaddingLeft;
    protected short field_5_wCellPaddingTop;
    protected short field_6_wCellPaddingBottom;
    protected short field_7_wCellPaddingRight;
    protected byte field_8_ftsCellPaddingLeft;
    protected byte field_9_ftsCellPaddingTop;
    protected byte field_10_ftsCellPaddingBottom;
    protected byte field_11_ftsCellPaddingRight;
    protected short field_12_wCellSpacingLeft;
    protected short field_13_wCellSpacingTop;
    protected short field_14_wCellSpacingBottom;
    protected short field_15_wCellSpacingRight;
    protected byte field_16_ftsCellSpacingLeft;
    protected byte field_17_ftsCellSpacingTop;
    protected byte field_18_ftsCellSpacingBottom;
    protected byte field_19_ftsCellSpacingRight;
    protected BorderCode field_20_brcTop;
    protected BorderCode field_21_brcLeft;
    protected BorderCode field_22_brcBottom;
    protected BorderCode field_23_brcRight;
    
    protected TCAbstractType() {
        this.field_3_shd = new ShadingDescriptor();
        this.field_20_brcTop = new BorderCode();
        this.field_21_brcLeft = new BorderCode();
        this.field_22_brcBottom = new BorderCode();
        this.field_23_brcRight = new BorderCode();
    }
    
    @Override
    public String toString() {
        final StringBuilder builder = new StringBuilder();
        builder.append("[TC]\n");
        builder.append("    .rgf                  = ");
        builder.append(" (").append(this.getRgf()).append(" )\n");
        builder.append("         .fFirstMerged             = ").append(this.isFFirstMerged()).append('\n');
        builder.append("         .fMerged                  = ").append(this.isFMerged()).append('\n');
        builder.append("         .fVertical                = ").append(this.isFVertical()).append('\n');
        builder.append("         .fBackward                = ").append(this.isFBackward()).append('\n');
        builder.append("         .fRotateFont              = ").append(this.isFRotateFont()).append('\n');
        builder.append("         .fVertMerge               = ").append(this.isFVertMerge()).append('\n');
        builder.append("         .fVertRestart             = ").append(this.isFVertRestart()).append('\n');
        builder.append("         .vertAlign                = ").append(this.getVertAlign()).append('\n');
        builder.append("         .ftsWidth                 = ").append(this.getFtsWidth()).append('\n');
        builder.append("         .fFitText                 = ").append(this.isFFitText()).append('\n');
        builder.append("         .fNoWrap                  = ").append(this.isFNoWrap()).append('\n');
        builder.append("         .fUnused                  = ").append(this.getFUnused()).append('\n');
        builder.append("    .wWidth               = ");
        builder.append(" (").append(this.getWWidth()).append(" )\n");
        builder.append("    .shd                  = ");
        builder.append(" (").append(this.getShd()).append(" )\n");
        builder.append("    .wCellPaddingLeft     = ");
        builder.append(" (").append(this.getWCellPaddingLeft()).append(" )\n");
        builder.append("    .wCellPaddingTop      = ");
        builder.append(" (").append(this.getWCellPaddingTop()).append(" )\n");
        builder.append("    .wCellPaddingBottom   = ");
        builder.append(" (").append(this.getWCellPaddingBottom()).append(" )\n");
        builder.append("    .wCellPaddingRight    = ");
        builder.append(" (").append(this.getWCellPaddingRight()).append(" )\n");
        builder.append("    .ftsCellPaddingLeft   = ");
        builder.append(" (").append(this.getFtsCellPaddingLeft()).append(" )\n");
        builder.append("    .ftsCellPaddingTop    = ");
        builder.append(" (").append(this.getFtsCellPaddingTop()).append(" )\n");
        builder.append("    .ftsCellPaddingBottom = ");
        builder.append(" (").append(this.getFtsCellPaddingBottom()).append(" )\n");
        builder.append("    .ftsCellPaddingRight  = ");
        builder.append(" (").append(this.getFtsCellPaddingRight()).append(" )\n");
        builder.append("    .wCellSpacingLeft     = ");
        builder.append(" (").append(this.getWCellSpacingLeft()).append(" )\n");
        builder.append("    .wCellSpacingTop      = ");
        builder.append(" (").append(this.getWCellSpacingTop()).append(" )\n");
        builder.append("    .wCellSpacingBottom   = ");
        builder.append(" (").append(this.getWCellSpacingBottom()).append(" )\n");
        builder.append("    .wCellSpacingRight    = ");
        builder.append(" (").append(this.getWCellSpacingRight()).append(" )\n");
        builder.append("    .ftsCellSpacingLeft   = ");
        builder.append(" (").append(this.getFtsCellSpacingLeft()).append(" )\n");
        builder.append("    .ftsCellSpacingTop    = ");
        builder.append(" (").append(this.getFtsCellSpacingTop()).append(" )\n");
        builder.append("    .ftsCellSpacingBottom = ");
        builder.append(" (").append(this.getFtsCellSpacingBottom()).append(" )\n");
        builder.append("    .ftsCellSpacingRight  = ");
        builder.append(" (").append(this.getFtsCellSpacingRight()).append(" )\n");
        builder.append("    .brcTop               = ");
        builder.append(" (").append(this.getBrcTop()).append(" )\n");
        builder.append("    .brcLeft              = ");
        builder.append(" (").append(this.getBrcLeft()).append(" )\n");
        builder.append("    .brcBottom            = ");
        builder.append(" (").append(this.getBrcBottom()).append(" )\n");
        builder.append("    .brcRight             = ");
        builder.append(" (").append(this.getBrcRight()).append(" )\n");
        builder.append("[/TC]\n");
        return builder.toString();
    }
    
    @Internal
    public short getRgf() {
        return this.field_1_rgf;
    }
    
    @Internal
    public void setRgf(final short field_1_rgf) {
        this.field_1_rgf = field_1_rgf;
    }
    
    @Internal
    public short getWWidth() {
        return this.field_2_wWidth;
    }
    
    @Internal
    public void setWWidth(final short field_2_wWidth) {
        this.field_2_wWidth = field_2_wWidth;
    }
    
    @Internal
    public ShadingDescriptor getShd() {
        return this.field_3_shd;
    }
    
    @Internal
    public void setShd(final ShadingDescriptor field_3_shd) {
        this.field_3_shd = field_3_shd;
    }
    
    @Internal
    public short getWCellPaddingLeft() {
        return this.field_4_wCellPaddingLeft;
    }
    
    @Internal
    public void setWCellPaddingLeft(final short field_4_wCellPaddingLeft) {
        this.field_4_wCellPaddingLeft = field_4_wCellPaddingLeft;
    }
    
    @Internal
    public short getWCellPaddingTop() {
        return this.field_5_wCellPaddingTop;
    }
    
    @Internal
    public void setWCellPaddingTop(final short field_5_wCellPaddingTop) {
        this.field_5_wCellPaddingTop = field_5_wCellPaddingTop;
    }
    
    @Internal
    public short getWCellPaddingBottom() {
        return this.field_6_wCellPaddingBottom;
    }
    
    @Internal
    public void setWCellPaddingBottom(final short field_6_wCellPaddingBottom) {
        this.field_6_wCellPaddingBottom = field_6_wCellPaddingBottom;
    }
    
    @Internal
    public short getWCellPaddingRight() {
        return this.field_7_wCellPaddingRight;
    }
    
    @Internal
    public void setWCellPaddingRight(final short field_7_wCellPaddingRight) {
        this.field_7_wCellPaddingRight = field_7_wCellPaddingRight;
    }
    
    @Internal
    public byte getFtsCellPaddingLeft() {
        return this.field_8_ftsCellPaddingLeft;
    }
    
    @Internal
    public void setFtsCellPaddingLeft(final byte field_8_ftsCellPaddingLeft) {
        this.field_8_ftsCellPaddingLeft = field_8_ftsCellPaddingLeft;
    }
    
    @Internal
    public byte getFtsCellPaddingTop() {
        return this.field_9_ftsCellPaddingTop;
    }
    
    @Internal
    public void setFtsCellPaddingTop(final byte field_9_ftsCellPaddingTop) {
        this.field_9_ftsCellPaddingTop = field_9_ftsCellPaddingTop;
    }
    
    @Internal
    public byte getFtsCellPaddingBottom() {
        return this.field_10_ftsCellPaddingBottom;
    }
    
    @Internal
    public void setFtsCellPaddingBottom(final byte field_10_ftsCellPaddingBottom) {
        this.field_10_ftsCellPaddingBottom = field_10_ftsCellPaddingBottom;
    }
    
    @Internal
    public byte getFtsCellPaddingRight() {
        return this.field_11_ftsCellPaddingRight;
    }
    
    @Internal
    public void setFtsCellPaddingRight(final byte field_11_ftsCellPaddingRight) {
        this.field_11_ftsCellPaddingRight = field_11_ftsCellPaddingRight;
    }
    
    @Internal
    public short getWCellSpacingLeft() {
        return this.field_12_wCellSpacingLeft;
    }
    
    @Internal
    public void setWCellSpacingLeft(final short field_12_wCellSpacingLeft) {
        this.field_12_wCellSpacingLeft = field_12_wCellSpacingLeft;
    }
    
    @Internal
    public short getWCellSpacingTop() {
        return this.field_13_wCellSpacingTop;
    }
    
    @Internal
    public void setWCellSpacingTop(final short field_13_wCellSpacingTop) {
        this.field_13_wCellSpacingTop = field_13_wCellSpacingTop;
    }
    
    @Internal
    public short getWCellSpacingBottom() {
        return this.field_14_wCellSpacingBottom;
    }
    
    @Internal
    public void setWCellSpacingBottom(final short field_14_wCellSpacingBottom) {
        this.field_14_wCellSpacingBottom = field_14_wCellSpacingBottom;
    }
    
    @Internal
    public short getWCellSpacingRight() {
        return this.field_15_wCellSpacingRight;
    }
    
    @Internal
    public void setWCellSpacingRight(final short field_15_wCellSpacingRight) {
        this.field_15_wCellSpacingRight = field_15_wCellSpacingRight;
    }
    
    @Internal
    public byte getFtsCellSpacingLeft() {
        return this.field_16_ftsCellSpacingLeft;
    }
    
    @Internal
    public void setFtsCellSpacingLeft(final byte field_16_ftsCellSpacingLeft) {
        this.field_16_ftsCellSpacingLeft = field_16_ftsCellSpacingLeft;
    }
    
    @Internal
    public byte getFtsCellSpacingTop() {
        return this.field_17_ftsCellSpacingTop;
    }
    
    @Internal
    public void setFtsCellSpacingTop(final byte field_17_ftsCellSpacingTop) {
        this.field_17_ftsCellSpacingTop = field_17_ftsCellSpacingTop;
    }
    
    @Internal
    public byte getFtsCellSpacingBottom() {
        return this.field_18_ftsCellSpacingBottom;
    }
    
    @Internal
    public void setFtsCellSpacingBottom(final byte field_18_ftsCellSpacingBottom) {
        this.field_18_ftsCellSpacingBottom = field_18_ftsCellSpacingBottom;
    }
    
    @Internal
    public byte getFtsCellSpacingRight() {
        return this.field_19_ftsCellSpacingRight;
    }
    
    @Internal
    public void setFtsCellSpacingRight(final byte field_19_ftsCellSpacingRight) {
        this.field_19_ftsCellSpacingRight = field_19_ftsCellSpacingRight;
    }
    
    @Internal
    public BorderCode getBrcTop() {
        return this.field_20_brcTop;
    }
    
    @Internal
    public void setBrcTop(final BorderCode field_20_brcTop) {
        this.field_20_brcTop = field_20_brcTop;
    }
    
    @Internal
    public BorderCode getBrcLeft() {
        return this.field_21_brcLeft;
    }
    
    @Internal
    public void setBrcLeft(final BorderCode field_21_brcLeft) {
        this.field_21_brcLeft = field_21_brcLeft;
    }
    
    @Internal
    public BorderCode getBrcBottom() {
        return this.field_22_brcBottom;
    }
    
    @Internal
    public void setBrcBottom(final BorderCode field_22_brcBottom) {
        this.field_22_brcBottom = field_22_brcBottom;
    }
    
    @Internal
    public BorderCode getBrcRight() {
        return this.field_23_brcRight;
    }
    
    @Internal
    public void setBrcRight(final BorderCode field_23_brcRight) {
        this.field_23_brcRight = field_23_brcRight;
    }
    
    @Internal
    public void setFFirstMerged(final boolean value) {
        this.field_1_rgf = (short)TCAbstractType.fFirstMerged.setBoolean(this.field_1_rgf, value);
    }
    
    @Internal
    public boolean isFFirstMerged() {
        return TCAbstractType.fFirstMerged.isSet(this.field_1_rgf);
    }
    
    @Internal
    public void setFMerged(final boolean value) {
        this.field_1_rgf = (short)TCAbstractType.fMerged.setBoolean(this.field_1_rgf, value);
    }
    
    @Internal
    public boolean isFMerged() {
        return TCAbstractType.fMerged.isSet(this.field_1_rgf);
    }
    
    @Internal
    public void setFVertical(final boolean value) {
        this.field_1_rgf = (short)TCAbstractType.fVertical.setBoolean(this.field_1_rgf, value);
    }
    
    @Internal
    public boolean isFVertical() {
        return TCAbstractType.fVertical.isSet(this.field_1_rgf);
    }
    
    @Internal
    public void setFBackward(final boolean value) {
        this.field_1_rgf = (short)TCAbstractType.fBackward.setBoolean(this.field_1_rgf, value);
    }
    
    @Internal
    public boolean isFBackward() {
        return TCAbstractType.fBackward.isSet(this.field_1_rgf);
    }
    
    @Internal
    public void setFRotateFont(final boolean value) {
        this.field_1_rgf = (short)TCAbstractType.fRotateFont.setBoolean(this.field_1_rgf, value);
    }
    
    @Internal
    public boolean isFRotateFont() {
        return TCAbstractType.fRotateFont.isSet(this.field_1_rgf);
    }
    
    @Internal
    public void setFVertMerge(final boolean value) {
        this.field_1_rgf = (short)TCAbstractType.fVertMerge.setBoolean(this.field_1_rgf, value);
    }
    
    @Internal
    public boolean isFVertMerge() {
        return TCAbstractType.fVertMerge.isSet(this.field_1_rgf);
    }
    
    @Internal
    public void setFVertRestart(final boolean value) {
        this.field_1_rgf = (short)TCAbstractType.fVertRestart.setBoolean(this.field_1_rgf, value);
    }
    
    @Internal
    public boolean isFVertRestart() {
        return TCAbstractType.fVertRestart.isSet(this.field_1_rgf);
    }
    
    @Internal
    public void setVertAlign(final byte value) {
        this.field_1_rgf = (short)TCAbstractType.vertAlign.setValue(this.field_1_rgf, value);
    }
    
    @Internal
    public byte getVertAlign() {
        return (byte)TCAbstractType.vertAlign.getValue(this.field_1_rgf);
    }
    
    @Internal
    public void setFtsWidth(final byte value) {
        this.field_1_rgf = (short)TCAbstractType.ftsWidth.setValue(this.field_1_rgf, value);
    }
    
    @Internal
    public byte getFtsWidth() {
        return (byte)TCAbstractType.ftsWidth.getValue(this.field_1_rgf);
    }
    
    @Internal
    public void setFFitText(final boolean value) {
        this.field_1_rgf = (short)TCAbstractType.fFitText.setBoolean(this.field_1_rgf, value);
    }
    
    @Internal
    public boolean isFFitText() {
        return TCAbstractType.fFitText.isSet(this.field_1_rgf);
    }
    
    @Internal
    public void setFNoWrap(final boolean value) {
        this.field_1_rgf = (short)TCAbstractType.fNoWrap.setBoolean(this.field_1_rgf, value);
    }
    
    @Internal
    public boolean isFNoWrap() {
        return TCAbstractType.fNoWrap.isSet(this.field_1_rgf);
    }
    
    @Internal
    public void setFUnused(final byte value) {
        this.field_1_rgf = (short)TCAbstractType.fUnused.setValue(this.field_1_rgf, value);
    }
    
    @Internal
    public byte getFUnused() {
        return (byte)TCAbstractType.fUnused.getValue(this.field_1_rgf);
    }
    
    static {
        TCAbstractType.fFirstMerged = new BitField(1);
        TCAbstractType.fMerged = new BitField(2);
        TCAbstractType.fVertical = new BitField(4);
        TCAbstractType.fBackward = new BitField(8);
        TCAbstractType.fRotateFont = new BitField(16);
        TCAbstractType.fVertMerge = new BitField(32);
        TCAbstractType.fVertRestart = new BitField(64);
        TCAbstractType.vertAlign = new BitField(384);
        TCAbstractType.ftsWidth = new BitField(3584);
        TCAbstractType.fFitText = new BitField(4096);
        TCAbstractType.fNoWrap = new BitField(8192);
        TCAbstractType.fUnused = new BitField(49152);
    }
}
