// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hwpf.model.types;

import org.apache.poi.hwpf.usermodel.TableCellDescriptor;
import org.apache.poi.hwpf.usermodel.BorderCode;
import org.apache.poi.hwpf.usermodel.ShadingDescriptor;
import org.apache.poi.util.BitField;
import org.apache.poi.hwpf.usermodel.TableAutoformatLookSpecifier;
import org.apache.poi.util.Internal;

@Internal
public abstract class TAPAbstractType
{
    protected short field_1_istd;
    protected short field_2_jc;
    protected int field_3_dxaGapHalf;
    protected int field_4_dyaRowHeight;
    protected boolean field_5_fCantSplit;
    protected boolean field_6_fCantSplit90;
    protected boolean field_7_fTableHeader;
    protected TableAutoformatLookSpecifier field_8_tlp;
    protected short field_9_wWidth;
    protected short field_10_wWidthIndent;
    protected short field_11_wWidthBefore;
    protected short field_12_wWidthAfter;
    protected int field_13_widthAndFitsFlags;
    private static BitField fAutofit;
    private static BitField fKeepFollow;
    private static BitField ftsWidth;
    private static BitField ftsWidthIndent;
    private static BitField ftsWidthBefore;
    private static BitField ftsWidthAfter;
    private static BitField fNeverBeenAutofit;
    private static BitField fInvalAutofit;
    private static BitField widthAndFitsFlags_empty1;
    private static BitField fVert;
    private static BitField pcVert;
    private static BitField pcHorz;
    private static BitField widthAndFitsFlags_empty2;
    protected int field_14_dxaAbs;
    protected int field_15_dyaAbs;
    protected int field_16_dxaFromText;
    protected int field_17_dyaFromText;
    protected int field_18_dxaFromTextRight;
    protected int field_19_dyaFromTextBottom;
    protected byte field_20_fBiDi;
    protected byte field_21_fRTL;
    protected byte field_22_fNoAllowOverlap;
    protected byte field_23_fSpare;
    protected int field_24_grpfTap;
    protected int field_25_internalFlags;
    private static BitField fFirstRow;
    private static BitField fLastRow;
    private static BitField fOutline;
    private static BitField fOrigWordTableRules;
    private static BitField fCellSpacing;
    private static BitField grpfTap_unused;
    protected short field_26_itcMac;
    protected int field_27_dxaAdjust;
    protected int field_28_dxaWebView;
    protected int field_29_dxaRTEWrapWidth;
    protected int field_30_dxaColWidthWwd;
    protected short field_31_pctWwd;
    protected int field_32_viewFlags;
    private static BitField fWrapToWwd;
    private static BitField fNotPageView;
    private static BitField viewFlags_unused1;
    private static BitField fWebView;
    private static BitField fAdjusted;
    private static BitField viewFlags_unused2;
    protected short[] field_33_rgdxaCenter;
    protected short[] field_34_rgdxaCenterPrint;
    protected ShadingDescriptor field_35_shdTable;
    protected BorderCode field_36_brcBottom;
    protected BorderCode field_37_brcTop;
    protected BorderCode field_38_brcLeft;
    protected BorderCode field_39_brcRight;
    protected BorderCode field_40_brcVertical;
    protected BorderCode field_41_brcHorizontal;
    protected short field_42_wCellPaddingDefaultTop;
    protected short field_43_wCellPaddingDefaultLeft;
    protected short field_44_wCellPaddingDefaultBottom;
    protected short field_45_wCellPaddingDefaultRight;
    protected byte field_46_ftsCellPaddingDefaultTop;
    protected byte field_47_ftsCellPaddingDefaultLeft;
    protected byte field_48_ftsCellPaddingDefaultBottom;
    protected byte field_49_ftsCellPaddingDefaultRight;
    protected short field_50_wCellSpacingDefaultTop;
    protected short field_51_wCellSpacingDefaultLeft;
    protected short field_52_wCellSpacingDefaultBottom;
    protected short field_53_wCellSpacingDefaultRight;
    protected byte field_54_ftsCellSpacingDefaultTop;
    protected byte field_55_ftsCellSpacingDefaultLeft;
    protected byte field_56_ftsCellSpacingDefaultBottom;
    protected byte field_57_ftsCellSpacingDefaultRight;
    protected short field_58_wCellPaddingOuterTop;
    protected short field_59_wCellPaddingOuterLeft;
    protected short field_60_wCellPaddingOuterBottom;
    protected short field_61_wCellPaddingOuterRight;
    protected byte field_62_ftsCellPaddingOuterTop;
    protected byte field_63_ftsCellPaddingOuterLeft;
    protected byte field_64_ftsCellPaddingOuterBottom;
    protected byte field_65_ftsCellPaddingOuterRight;
    protected short field_66_wCellSpacingOuterTop;
    protected short field_67_wCellSpacingOuterLeft;
    protected short field_68_wCellSpacingOuterBottom;
    protected short field_69_wCellSpacingOuterRight;
    protected byte field_70_ftsCellSpacingOuterTop;
    protected byte field_71_ftsCellSpacingOuterLeft;
    protected byte field_72_ftsCellSpacingOuterBottom;
    protected byte field_73_ftsCellSpacingOuterRight;
    protected TableCellDescriptor[] field_74_rgtc;
    protected ShadingDescriptor[] field_75_rgshd;
    protected byte field_76_fPropRMark;
    protected byte field_77_fHasOldProps;
    protected short field_78_cHorzBands;
    protected short field_79_cVertBands;
    protected BorderCode field_80_rgbrcInsideDefault_0;
    protected BorderCode field_81_rgbrcInsideDefault_1;
    
    protected TAPAbstractType() {
        this.field_8_tlp = new TableAutoformatLookSpecifier();
        this.field_33_rgdxaCenter = new short[0];
        this.field_34_rgdxaCenterPrint = new short[0];
        this.field_35_shdTable = new ShadingDescriptor();
        this.field_36_brcBottom = new BorderCode();
        this.field_37_brcTop = new BorderCode();
        this.field_38_brcLeft = new BorderCode();
        this.field_39_brcRight = new BorderCode();
        this.field_40_brcVertical = new BorderCode();
        this.field_41_brcHorizontal = new BorderCode();
        this.field_74_rgtc = new TableCellDescriptor[0];
        this.field_75_rgshd = new ShadingDescriptor[0];
        this.field_80_rgbrcInsideDefault_0 = new BorderCode();
        this.field_81_rgbrcInsideDefault_1 = new BorderCode();
    }
    
    @Override
    public String toString() {
        final StringBuilder builder = new StringBuilder();
        builder.append("[TAP]\n");
        builder.append("    .istd                 = ");
        builder.append(" (").append(this.getIstd()).append(" )\n");
        builder.append("    .jc                   = ");
        builder.append(" (").append(this.getJc()).append(" )\n");
        builder.append("    .dxaGapHalf           = ");
        builder.append(" (").append(this.getDxaGapHalf()).append(" )\n");
        builder.append("    .dyaRowHeight         = ");
        builder.append(" (").append(this.getDyaRowHeight()).append(" )\n");
        builder.append("    .fCantSplit           = ");
        builder.append(" (").append(this.getFCantSplit()).append(" )\n");
        builder.append("    .fCantSplit90         = ");
        builder.append(" (").append(this.getFCantSplit90()).append(" )\n");
        builder.append("    .fTableHeader         = ");
        builder.append(" (").append(this.getFTableHeader()).append(" )\n");
        builder.append("    .tlp                  = ");
        builder.append(" (").append(this.getTlp()).append(" )\n");
        builder.append("    .wWidth               = ");
        builder.append(" (").append(this.getWWidth()).append(" )\n");
        builder.append("    .wWidthIndent         = ");
        builder.append(" (").append(this.getWWidthIndent()).append(" )\n");
        builder.append("    .wWidthBefore         = ");
        builder.append(" (").append(this.getWWidthBefore()).append(" )\n");
        builder.append("    .wWidthAfter          = ");
        builder.append(" (").append(this.getWWidthAfter()).append(" )\n");
        builder.append("    .widthAndFitsFlags    = ");
        builder.append(" (").append(this.getWidthAndFitsFlags()).append(" )\n");
        builder.append("         .fAutofit                 = ").append(this.isFAutofit()).append('\n');
        builder.append("         .fKeepFollow              = ").append(this.isFKeepFollow()).append('\n');
        builder.append("         .ftsWidth                 = ").append(this.getFtsWidth()).append('\n');
        builder.append("         .ftsWidthIndent           = ").append(this.getFtsWidthIndent()).append('\n');
        builder.append("         .ftsWidthBefore           = ").append(this.getFtsWidthBefore()).append('\n');
        builder.append("         .ftsWidthAfter            = ").append(this.getFtsWidthAfter()).append('\n');
        builder.append("         .fNeverBeenAutofit        = ").append(this.isFNeverBeenAutofit()).append('\n');
        builder.append("         .fInvalAutofit            = ").append(this.isFInvalAutofit()).append('\n');
        builder.append("         .widthAndFitsFlags_empty1     = ").append(this.getWidthAndFitsFlags_empty1()).append('\n');
        builder.append("         .fVert                    = ").append(this.isFVert()).append('\n');
        builder.append("         .pcVert                   = ").append(this.getPcVert()).append('\n');
        builder.append("         .pcHorz                   = ").append(this.getPcHorz()).append('\n');
        builder.append("         .widthAndFitsFlags_empty2     = ").append(this.getWidthAndFitsFlags_empty2()).append('\n');
        builder.append("    .dxaAbs               = ");
        builder.append(" (").append(this.getDxaAbs()).append(" )\n");
        builder.append("    .dyaAbs               = ");
        builder.append(" (").append(this.getDyaAbs()).append(" )\n");
        builder.append("    .dxaFromText          = ");
        builder.append(" (").append(this.getDxaFromText()).append(" )\n");
        builder.append("    .dyaFromText          = ");
        builder.append(" (").append(this.getDyaFromText()).append(" )\n");
        builder.append("    .dxaFromTextRight     = ");
        builder.append(" (").append(this.getDxaFromTextRight()).append(" )\n");
        builder.append("    .dyaFromTextBottom    = ");
        builder.append(" (").append(this.getDyaFromTextBottom()).append(" )\n");
        builder.append("    .fBiDi                = ");
        builder.append(" (").append(this.getFBiDi()).append(" )\n");
        builder.append("    .fRTL                 = ");
        builder.append(" (").append(this.getFRTL()).append(" )\n");
        builder.append("    .fNoAllowOverlap      = ");
        builder.append(" (").append(this.getFNoAllowOverlap()).append(" )\n");
        builder.append("    .fSpare               = ");
        builder.append(" (").append(this.getFSpare()).append(" )\n");
        builder.append("    .grpfTap              = ");
        builder.append(" (").append(this.getGrpfTap()).append(" )\n");
        builder.append("    .internalFlags        = ");
        builder.append(" (").append(this.getInternalFlags()).append(" )\n");
        builder.append("         .fFirstRow                = ").append(this.isFFirstRow()).append('\n');
        builder.append("         .fLastRow                 = ").append(this.isFLastRow()).append('\n');
        builder.append("         .fOutline                 = ").append(this.isFOutline()).append('\n');
        builder.append("         .fOrigWordTableRules      = ").append(this.isFOrigWordTableRules()).append('\n');
        builder.append("         .fCellSpacing             = ").append(this.isFCellSpacing()).append('\n');
        builder.append("         .grpfTap_unused           = ").append(this.getGrpfTap_unused()).append('\n');
        builder.append("    .itcMac               = ");
        builder.append(" (").append(this.getItcMac()).append(" )\n");
        builder.append("    .dxaAdjust            = ");
        builder.append(" (").append(this.getDxaAdjust()).append(" )\n");
        builder.append("    .dxaWebView           = ");
        builder.append(" (").append(this.getDxaWebView()).append(" )\n");
        builder.append("    .dxaRTEWrapWidth      = ");
        builder.append(" (").append(this.getDxaRTEWrapWidth()).append(" )\n");
        builder.append("    .dxaColWidthWwd       = ");
        builder.append(" (").append(this.getDxaColWidthWwd()).append(" )\n");
        builder.append("    .pctWwd               = ");
        builder.append(" (").append(this.getPctWwd()).append(" )\n");
        builder.append("    .viewFlags            = ");
        builder.append(" (").append(this.getViewFlags()).append(" )\n");
        builder.append("         .fWrapToWwd               = ").append(this.isFWrapToWwd()).append('\n');
        builder.append("         .fNotPageView             = ").append(this.isFNotPageView()).append('\n');
        builder.append("         .viewFlags_unused1        = ").append(this.isViewFlags_unused1()).append('\n');
        builder.append("         .fWebView                 = ").append(this.isFWebView()).append('\n');
        builder.append("         .fAdjusted                = ").append(this.isFAdjusted()).append('\n');
        builder.append("         .viewFlags_unused2        = ").append(this.getViewFlags_unused2()).append('\n');
        builder.append("    .rgdxaCenter          = ");
        builder.append(" (").append(this.getRgdxaCenter()).append(" )\n");
        builder.append("    .rgdxaCenterPrint     = ");
        builder.append(" (").append(this.getRgdxaCenterPrint()).append(" )\n");
        builder.append("    .shdTable             = ");
        builder.append(" (").append(this.getShdTable()).append(" )\n");
        builder.append("    .brcBottom            = ");
        builder.append(" (").append(this.getBrcBottom()).append(" )\n");
        builder.append("    .brcTop               = ");
        builder.append(" (").append(this.getBrcTop()).append(" )\n");
        builder.append("    .brcLeft              = ");
        builder.append(" (").append(this.getBrcLeft()).append(" )\n");
        builder.append("    .brcRight             = ");
        builder.append(" (").append(this.getBrcRight()).append(" )\n");
        builder.append("    .brcVertical          = ");
        builder.append(" (").append(this.getBrcVertical()).append(" )\n");
        builder.append("    .brcHorizontal        = ");
        builder.append(" (").append(this.getBrcHorizontal()).append(" )\n");
        builder.append("    .wCellPaddingDefaultTop = ");
        builder.append(" (").append(this.getWCellPaddingDefaultTop()).append(" )\n");
        builder.append("    .wCellPaddingDefaultLeft = ");
        builder.append(" (").append(this.getWCellPaddingDefaultLeft()).append(" )\n");
        builder.append("    .wCellPaddingDefaultBottom = ");
        builder.append(" (").append(this.getWCellPaddingDefaultBottom()).append(" )\n");
        builder.append("    .wCellPaddingDefaultRight = ");
        builder.append(" (").append(this.getWCellPaddingDefaultRight()).append(" )\n");
        builder.append("    .ftsCellPaddingDefaultTop = ");
        builder.append(" (").append(this.getFtsCellPaddingDefaultTop()).append(" )\n");
        builder.append("    .ftsCellPaddingDefaultLeft = ");
        builder.append(" (").append(this.getFtsCellPaddingDefaultLeft()).append(" )\n");
        builder.append("    .ftsCellPaddingDefaultBottom = ");
        builder.append(" (").append(this.getFtsCellPaddingDefaultBottom()).append(" )\n");
        builder.append("    .ftsCellPaddingDefaultRight = ");
        builder.append(" (").append(this.getFtsCellPaddingDefaultRight()).append(" )\n");
        builder.append("    .wCellSpacingDefaultTop = ");
        builder.append(" (").append(this.getWCellSpacingDefaultTop()).append(" )\n");
        builder.append("    .wCellSpacingDefaultLeft = ");
        builder.append(" (").append(this.getWCellSpacingDefaultLeft()).append(" )\n");
        builder.append("    .wCellSpacingDefaultBottom = ");
        builder.append(" (").append(this.getWCellSpacingDefaultBottom()).append(" )\n");
        builder.append("    .wCellSpacingDefaultRight = ");
        builder.append(" (").append(this.getWCellSpacingDefaultRight()).append(" )\n");
        builder.append("    .ftsCellSpacingDefaultTop = ");
        builder.append(" (").append(this.getFtsCellSpacingDefaultTop()).append(" )\n");
        builder.append("    .ftsCellSpacingDefaultLeft = ");
        builder.append(" (").append(this.getFtsCellSpacingDefaultLeft()).append(" )\n");
        builder.append("    .ftsCellSpacingDefaultBottom = ");
        builder.append(" (").append(this.getFtsCellSpacingDefaultBottom()).append(" )\n");
        builder.append("    .ftsCellSpacingDefaultRight = ");
        builder.append(" (").append(this.getFtsCellSpacingDefaultRight()).append(" )\n");
        builder.append("    .wCellPaddingOuterTop = ");
        builder.append(" (").append(this.getWCellPaddingOuterTop()).append(" )\n");
        builder.append("    .wCellPaddingOuterLeft = ");
        builder.append(" (").append(this.getWCellPaddingOuterLeft()).append(" )\n");
        builder.append("    .wCellPaddingOuterBottom = ");
        builder.append(" (").append(this.getWCellPaddingOuterBottom()).append(" )\n");
        builder.append("    .wCellPaddingOuterRight = ");
        builder.append(" (").append(this.getWCellPaddingOuterRight()).append(" )\n");
        builder.append("    .ftsCellPaddingOuterTop = ");
        builder.append(" (").append(this.getFtsCellPaddingOuterTop()).append(" )\n");
        builder.append("    .ftsCellPaddingOuterLeft = ");
        builder.append(" (").append(this.getFtsCellPaddingOuterLeft()).append(" )\n");
        builder.append("    .ftsCellPaddingOuterBottom = ");
        builder.append(" (").append(this.getFtsCellPaddingOuterBottom()).append(" )\n");
        builder.append("    .ftsCellPaddingOuterRight = ");
        builder.append(" (").append(this.getFtsCellPaddingOuterRight()).append(" )\n");
        builder.append("    .wCellSpacingOuterTop = ");
        builder.append(" (").append(this.getWCellSpacingOuterTop()).append(" )\n");
        builder.append("    .wCellSpacingOuterLeft = ");
        builder.append(" (").append(this.getWCellSpacingOuterLeft()).append(" )\n");
        builder.append("    .wCellSpacingOuterBottom = ");
        builder.append(" (").append(this.getWCellSpacingOuterBottom()).append(" )\n");
        builder.append("    .wCellSpacingOuterRight = ");
        builder.append(" (").append(this.getWCellSpacingOuterRight()).append(" )\n");
        builder.append("    .ftsCellSpacingOuterTop = ");
        builder.append(" (").append(this.getFtsCellSpacingOuterTop()).append(" )\n");
        builder.append("    .ftsCellSpacingOuterLeft = ");
        builder.append(" (").append(this.getFtsCellSpacingOuterLeft()).append(" )\n");
        builder.append("    .ftsCellSpacingOuterBottom = ");
        builder.append(" (").append(this.getFtsCellSpacingOuterBottom()).append(" )\n");
        builder.append("    .ftsCellSpacingOuterRight = ");
        builder.append(" (").append(this.getFtsCellSpacingOuterRight()).append(" )\n");
        builder.append("    .rgtc                 = ");
        builder.append(" (").append(this.getRgtc()).append(" )\n");
        builder.append("    .rgshd                = ");
        builder.append(" (").append(this.getRgshd()).append(" )\n");
        builder.append("    .fPropRMark           = ");
        builder.append(" (").append(this.getFPropRMark()).append(" )\n");
        builder.append("    .fHasOldProps         = ");
        builder.append(" (").append(this.getFHasOldProps()).append(" )\n");
        builder.append("    .cHorzBands           = ");
        builder.append(" (").append(this.getCHorzBands()).append(" )\n");
        builder.append("    .cVertBands           = ");
        builder.append(" (").append(this.getCVertBands()).append(" )\n");
        builder.append("    .rgbrcInsideDefault_0 = ");
        builder.append(" (").append(this.getRgbrcInsideDefault_0()).append(" )\n");
        builder.append("    .rgbrcInsideDefault_1 = ");
        builder.append(" (").append(this.getRgbrcInsideDefault_1()).append(" )\n");
        builder.append("[/TAP]\n");
        return builder.toString();
    }
    
    @Internal
    public short getIstd() {
        return this.field_1_istd;
    }
    
    @Internal
    public void setIstd(final short field_1_istd) {
        this.field_1_istd = field_1_istd;
    }
    
    @Internal
    public short getJc() {
        return this.field_2_jc;
    }
    
    @Internal
    public void setJc(final short field_2_jc) {
        this.field_2_jc = field_2_jc;
    }
    
    @Internal
    public int getDxaGapHalf() {
        return this.field_3_dxaGapHalf;
    }
    
    @Internal
    public void setDxaGapHalf(final int field_3_dxaGapHalf) {
        this.field_3_dxaGapHalf = field_3_dxaGapHalf;
    }
    
    @Internal
    public int getDyaRowHeight() {
        return this.field_4_dyaRowHeight;
    }
    
    @Internal
    public void setDyaRowHeight(final int field_4_dyaRowHeight) {
        this.field_4_dyaRowHeight = field_4_dyaRowHeight;
    }
    
    @Internal
    public boolean getFCantSplit() {
        return this.field_5_fCantSplit;
    }
    
    @Internal
    public void setFCantSplit(final boolean field_5_fCantSplit) {
        this.field_5_fCantSplit = field_5_fCantSplit;
    }
    
    @Internal
    public boolean getFCantSplit90() {
        return this.field_6_fCantSplit90;
    }
    
    @Internal
    public void setFCantSplit90(final boolean field_6_fCantSplit90) {
        this.field_6_fCantSplit90 = field_6_fCantSplit90;
    }
    
    @Internal
    public boolean getFTableHeader() {
        return this.field_7_fTableHeader;
    }
    
    @Internal
    public void setFTableHeader(final boolean field_7_fTableHeader) {
        this.field_7_fTableHeader = field_7_fTableHeader;
    }
    
    @Internal
    public TableAutoformatLookSpecifier getTlp() {
        return this.field_8_tlp;
    }
    
    @Internal
    public void setTlp(final TableAutoformatLookSpecifier field_8_tlp) {
        this.field_8_tlp = field_8_tlp;
    }
    
    @Internal
    public short getWWidth() {
        return this.field_9_wWidth;
    }
    
    @Internal
    public void setWWidth(final short field_9_wWidth) {
        this.field_9_wWidth = field_9_wWidth;
    }
    
    @Internal
    public short getWWidthIndent() {
        return this.field_10_wWidthIndent;
    }
    
    @Internal
    public void setWWidthIndent(final short field_10_wWidthIndent) {
        this.field_10_wWidthIndent = field_10_wWidthIndent;
    }
    
    @Internal
    public short getWWidthBefore() {
        return this.field_11_wWidthBefore;
    }
    
    @Internal
    public void setWWidthBefore(final short field_11_wWidthBefore) {
        this.field_11_wWidthBefore = field_11_wWidthBefore;
    }
    
    @Internal
    public short getWWidthAfter() {
        return this.field_12_wWidthAfter;
    }
    
    @Internal
    public void setWWidthAfter(final short field_12_wWidthAfter) {
        this.field_12_wWidthAfter = field_12_wWidthAfter;
    }
    
    @Internal
    public int getWidthAndFitsFlags() {
        return this.field_13_widthAndFitsFlags;
    }
    
    @Internal
    public void setWidthAndFitsFlags(final int field_13_widthAndFitsFlags) {
        this.field_13_widthAndFitsFlags = field_13_widthAndFitsFlags;
    }
    
    @Internal
    public int getDxaAbs() {
        return this.field_14_dxaAbs;
    }
    
    @Internal
    public void setDxaAbs(final int field_14_dxaAbs) {
        this.field_14_dxaAbs = field_14_dxaAbs;
    }
    
    @Internal
    public int getDyaAbs() {
        return this.field_15_dyaAbs;
    }
    
    @Internal
    public void setDyaAbs(final int field_15_dyaAbs) {
        this.field_15_dyaAbs = field_15_dyaAbs;
    }
    
    @Internal
    public int getDxaFromText() {
        return this.field_16_dxaFromText;
    }
    
    @Internal
    public void setDxaFromText(final int field_16_dxaFromText) {
        this.field_16_dxaFromText = field_16_dxaFromText;
    }
    
    @Internal
    public int getDyaFromText() {
        return this.field_17_dyaFromText;
    }
    
    @Internal
    public void setDyaFromText(final int field_17_dyaFromText) {
        this.field_17_dyaFromText = field_17_dyaFromText;
    }
    
    @Internal
    public int getDxaFromTextRight() {
        return this.field_18_dxaFromTextRight;
    }
    
    @Internal
    public void setDxaFromTextRight(final int field_18_dxaFromTextRight) {
        this.field_18_dxaFromTextRight = field_18_dxaFromTextRight;
    }
    
    @Internal
    public int getDyaFromTextBottom() {
        return this.field_19_dyaFromTextBottom;
    }
    
    @Internal
    public void setDyaFromTextBottom(final int field_19_dyaFromTextBottom) {
        this.field_19_dyaFromTextBottom = field_19_dyaFromTextBottom;
    }
    
    @Internal
    public byte getFBiDi() {
        return this.field_20_fBiDi;
    }
    
    @Internal
    public void setFBiDi(final byte field_20_fBiDi) {
        this.field_20_fBiDi = field_20_fBiDi;
    }
    
    @Internal
    public byte getFRTL() {
        return this.field_21_fRTL;
    }
    
    @Internal
    public void setFRTL(final byte field_21_fRTL) {
        this.field_21_fRTL = field_21_fRTL;
    }
    
    @Internal
    public byte getFNoAllowOverlap() {
        return this.field_22_fNoAllowOverlap;
    }
    
    @Internal
    public void setFNoAllowOverlap(final byte field_22_fNoAllowOverlap) {
        this.field_22_fNoAllowOverlap = field_22_fNoAllowOverlap;
    }
    
    @Internal
    public byte getFSpare() {
        return this.field_23_fSpare;
    }
    
    @Internal
    public void setFSpare(final byte field_23_fSpare) {
        this.field_23_fSpare = field_23_fSpare;
    }
    
    @Internal
    public int getGrpfTap() {
        return this.field_24_grpfTap;
    }
    
    @Internal
    public void setGrpfTap(final int field_24_grpfTap) {
        this.field_24_grpfTap = field_24_grpfTap;
    }
    
    @Internal
    public int getInternalFlags() {
        return this.field_25_internalFlags;
    }
    
    @Internal
    public void setInternalFlags(final int field_25_internalFlags) {
        this.field_25_internalFlags = field_25_internalFlags;
    }
    
    @Internal
    public short getItcMac() {
        return this.field_26_itcMac;
    }
    
    @Internal
    public void setItcMac(final short field_26_itcMac) {
        this.field_26_itcMac = field_26_itcMac;
    }
    
    @Internal
    public int getDxaAdjust() {
        return this.field_27_dxaAdjust;
    }
    
    @Internal
    public void setDxaAdjust(final int field_27_dxaAdjust) {
        this.field_27_dxaAdjust = field_27_dxaAdjust;
    }
    
    @Internal
    public int getDxaWebView() {
        return this.field_28_dxaWebView;
    }
    
    @Internal
    public void setDxaWebView(final int field_28_dxaWebView) {
        this.field_28_dxaWebView = field_28_dxaWebView;
    }
    
    @Internal
    public int getDxaRTEWrapWidth() {
        return this.field_29_dxaRTEWrapWidth;
    }
    
    @Internal
    public void setDxaRTEWrapWidth(final int field_29_dxaRTEWrapWidth) {
        this.field_29_dxaRTEWrapWidth = field_29_dxaRTEWrapWidth;
    }
    
    @Internal
    public int getDxaColWidthWwd() {
        return this.field_30_dxaColWidthWwd;
    }
    
    @Internal
    public void setDxaColWidthWwd(final int field_30_dxaColWidthWwd) {
        this.field_30_dxaColWidthWwd = field_30_dxaColWidthWwd;
    }
    
    @Internal
    public short getPctWwd() {
        return this.field_31_pctWwd;
    }
    
    @Internal
    public void setPctWwd(final short field_31_pctWwd) {
        this.field_31_pctWwd = field_31_pctWwd;
    }
    
    @Internal
    public int getViewFlags() {
        return this.field_32_viewFlags;
    }
    
    @Internal
    public void setViewFlags(final int field_32_viewFlags) {
        this.field_32_viewFlags = field_32_viewFlags;
    }
    
    @Internal
    public short[] getRgdxaCenter() {
        return this.field_33_rgdxaCenter;
    }
    
    @Internal
    public void setRgdxaCenter(final short[] field_33_rgdxaCenter) {
        this.field_33_rgdxaCenter = field_33_rgdxaCenter;
    }
    
    @Internal
    public short[] getRgdxaCenterPrint() {
        return this.field_34_rgdxaCenterPrint;
    }
    
    @Internal
    public void setRgdxaCenterPrint(final short[] field_34_rgdxaCenterPrint) {
        this.field_34_rgdxaCenterPrint = field_34_rgdxaCenterPrint;
    }
    
    @Internal
    public ShadingDescriptor getShdTable() {
        return this.field_35_shdTable;
    }
    
    @Internal
    public void setShdTable(final ShadingDescriptor field_35_shdTable) {
        this.field_35_shdTable = field_35_shdTable;
    }
    
    @Internal
    public BorderCode getBrcBottom() {
        return this.field_36_brcBottom;
    }
    
    @Internal
    public void setBrcBottom(final BorderCode field_36_brcBottom) {
        this.field_36_brcBottom = field_36_brcBottom;
    }
    
    @Internal
    public BorderCode getBrcTop() {
        return this.field_37_brcTop;
    }
    
    @Internal
    public void setBrcTop(final BorderCode field_37_brcTop) {
        this.field_37_brcTop = field_37_brcTop;
    }
    
    @Internal
    public BorderCode getBrcLeft() {
        return this.field_38_brcLeft;
    }
    
    @Internal
    public void setBrcLeft(final BorderCode field_38_brcLeft) {
        this.field_38_brcLeft = field_38_brcLeft;
    }
    
    @Internal
    public BorderCode getBrcRight() {
        return this.field_39_brcRight;
    }
    
    @Internal
    public void setBrcRight(final BorderCode field_39_brcRight) {
        this.field_39_brcRight = field_39_brcRight;
    }
    
    @Internal
    public BorderCode getBrcVertical() {
        return this.field_40_brcVertical;
    }
    
    @Internal
    public void setBrcVertical(final BorderCode field_40_brcVertical) {
        this.field_40_brcVertical = field_40_brcVertical;
    }
    
    @Internal
    public BorderCode getBrcHorizontal() {
        return this.field_41_brcHorizontal;
    }
    
    @Internal
    public void setBrcHorizontal(final BorderCode field_41_brcHorizontal) {
        this.field_41_brcHorizontal = field_41_brcHorizontal;
    }
    
    @Internal
    public short getWCellPaddingDefaultTop() {
        return this.field_42_wCellPaddingDefaultTop;
    }
    
    @Internal
    public void setWCellPaddingDefaultTop(final short field_42_wCellPaddingDefaultTop) {
        this.field_42_wCellPaddingDefaultTop = field_42_wCellPaddingDefaultTop;
    }
    
    @Internal
    public short getWCellPaddingDefaultLeft() {
        return this.field_43_wCellPaddingDefaultLeft;
    }
    
    @Internal
    public void setWCellPaddingDefaultLeft(final short field_43_wCellPaddingDefaultLeft) {
        this.field_43_wCellPaddingDefaultLeft = field_43_wCellPaddingDefaultLeft;
    }
    
    @Internal
    public short getWCellPaddingDefaultBottom() {
        return this.field_44_wCellPaddingDefaultBottom;
    }
    
    @Internal
    public void setWCellPaddingDefaultBottom(final short field_44_wCellPaddingDefaultBottom) {
        this.field_44_wCellPaddingDefaultBottom = field_44_wCellPaddingDefaultBottom;
    }
    
    @Internal
    public short getWCellPaddingDefaultRight() {
        return this.field_45_wCellPaddingDefaultRight;
    }
    
    @Internal
    public void setWCellPaddingDefaultRight(final short field_45_wCellPaddingDefaultRight) {
        this.field_45_wCellPaddingDefaultRight = field_45_wCellPaddingDefaultRight;
    }
    
    @Internal
    public byte getFtsCellPaddingDefaultTop() {
        return this.field_46_ftsCellPaddingDefaultTop;
    }
    
    @Internal
    public void setFtsCellPaddingDefaultTop(final byte field_46_ftsCellPaddingDefaultTop) {
        this.field_46_ftsCellPaddingDefaultTop = field_46_ftsCellPaddingDefaultTop;
    }
    
    @Internal
    public byte getFtsCellPaddingDefaultLeft() {
        return this.field_47_ftsCellPaddingDefaultLeft;
    }
    
    @Internal
    public void setFtsCellPaddingDefaultLeft(final byte field_47_ftsCellPaddingDefaultLeft) {
        this.field_47_ftsCellPaddingDefaultLeft = field_47_ftsCellPaddingDefaultLeft;
    }
    
    @Internal
    public byte getFtsCellPaddingDefaultBottom() {
        return this.field_48_ftsCellPaddingDefaultBottom;
    }
    
    @Internal
    public void setFtsCellPaddingDefaultBottom(final byte field_48_ftsCellPaddingDefaultBottom) {
        this.field_48_ftsCellPaddingDefaultBottom = field_48_ftsCellPaddingDefaultBottom;
    }
    
    @Internal
    public byte getFtsCellPaddingDefaultRight() {
        return this.field_49_ftsCellPaddingDefaultRight;
    }
    
    @Internal
    public void setFtsCellPaddingDefaultRight(final byte field_49_ftsCellPaddingDefaultRight) {
        this.field_49_ftsCellPaddingDefaultRight = field_49_ftsCellPaddingDefaultRight;
    }
    
    @Internal
    public short getWCellSpacingDefaultTop() {
        return this.field_50_wCellSpacingDefaultTop;
    }
    
    @Internal
    public void setWCellSpacingDefaultTop(final short field_50_wCellSpacingDefaultTop) {
        this.field_50_wCellSpacingDefaultTop = field_50_wCellSpacingDefaultTop;
    }
    
    @Internal
    public short getWCellSpacingDefaultLeft() {
        return this.field_51_wCellSpacingDefaultLeft;
    }
    
    @Internal
    public void setWCellSpacingDefaultLeft(final short field_51_wCellSpacingDefaultLeft) {
        this.field_51_wCellSpacingDefaultLeft = field_51_wCellSpacingDefaultLeft;
    }
    
    @Internal
    public short getWCellSpacingDefaultBottom() {
        return this.field_52_wCellSpacingDefaultBottom;
    }
    
    @Internal
    public void setWCellSpacingDefaultBottom(final short field_52_wCellSpacingDefaultBottom) {
        this.field_52_wCellSpacingDefaultBottom = field_52_wCellSpacingDefaultBottom;
    }
    
    @Internal
    public short getWCellSpacingDefaultRight() {
        return this.field_53_wCellSpacingDefaultRight;
    }
    
    @Internal
    public void setWCellSpacingDefaultRight(final short field_53_wCellSpacingDefaultRight) {
        this.field_53_wCellSpacingDefaultRight = field_53_wCellSpacingDefaultRight;
    }
    
    @Internal
    public byte getFtsCellSpacingDefaultTop() {
        return this.field_54_ftsCellSpacingDefaultTop;
    }
    
    @Internal
    public void setFtsCellSpacingDefaultTop(final byte field_54_ftsCellSpacingDefaultTop) {
        this.field_54_ftsCellSpacingDefaultTop = field_54_ftsCellSpacingDefaultTop;
    }
    
    @Internal
    public byte getFtsCellSpacingDefaultLeft() {
        return this.field_55_ftsCellSpacingDefaultLeft;
    }
    
    @Internal
    public void setFtsCellSpacingDefaultLeft(final byte field_55_ftsCellSpacingDefaultLeft) {
        this.field_55_ftsCellSpacingDefaultLeft = field_55_ftsCellSpacingDefaultLeft;
    }
    
    @Internal
    public byte getFtsCellSpacingDefaultBottom() {
        return this.field_56_ftsCellSpacingDefaultBottom;
    }
    
    @Internal
    public void setFtsCellSpacingDefaultBottom(final byte field_56_ftsCellSpacingDefaultBottom) {
        this.field_56_ftsCellSpacingDefaultBottom = field_56_ftsCellSpacingDefaultBottom;
    }
    
    @Internal
    public byte getFtsCellSpacingDefaultRight() {
        return this.field_57_ftsCellSpacingDefaultRight;
    }
    
    @Internal
    public void setFtsCellSpacingDefaultRight(final byte field_57_ftsCellSpacingDefaultRight) {
        this.field_57_ftsCellSpacingDefaultRight = field_57_ftsCellSpacingDefaultRight;
    }
    
    @Internal
    public short getWCellPaddingOuterTop() {
        return this.field_58_wCellPaddingOuterTop;
    }
    
    @Internal
    public void setWCellPaddingOuterTop(final short field_58_wCellPaddingOuterTop) {
        this.field_58_wCellPaddingOuterTop = field_58_wCellPaddingOuterTop;
    }
    
    @Internal
    public short getWCellPaddingOuterLeft() {
        return this.field_59_wCellPaddingOuterLeft;
    }
    
    @Internal
    public void setWCellPaddingOuterLeft(final short field_59_wCellPaddingOuterLeft) {
        this.field_59_wCellPaddingOuterLeft = field_59_wCellPaddingOuterLeft;
    }
    
    @Internal
    public short getWCellPaddingOuterBottom() {
        return this.field_60_wCellPaddingOuterBottom;
    }
    
    @Internal
    public void setWCellPaddingOuterBottom(final short field_60_wCellPaddingOuterBottom) {
        this.field_60_wCellPaddingOuterBottom = field_60_wCellPaddingOuterBottom;
    }
    
    @Internal
    public short getWCellPaddingOuterRight() {
        return this.field_61_wCellPaddingOuterRight;
    }
    
    @Internal
    public void setWCellPaddingOuterRight(final short field_61_wCellPaddingOuterRight) {
        this.field_61_wCellPaddingOuterRight = field_61_wCellPaddingOuterRight;
    }
    
    @Internal
    public byte getFtsCellPaddingOuterTop() {
        return this.field_62_ftsCellPaddingOuterTop;
    }
    
    @Internal
    public void setFtsCellPaddingOuterTop(final byte field_62_ftsCellPaddingOuterTop) {
        this.field_62_ftsCellPaddingOuterTop = field_62_ftsCellPaddingOuterTop;
    }
    
    @Internal
    public byte getFtsCellPaddingOuterLeft() {
        return this.field_63_ftsCellPaddingOuterLeft;
    }
    
    @Internal
    public void setFtsCellPaddingOuterLeft(final byte field_63_ftsCellPaddingOuterLeft) {
        this.field_63_ftsCellPaddingOuterLeft = field_63_ftsCellPaddingOuterLeft;
    }
    
    @Internal
    public byte getFtsCellPaddingOuterBottom() {
        return this.field_64_ftsCellPaddingOuterBottom;
    }
    
    @Internal
    public void setFtsCellPaddingOuterBottom(final byte field_64_ftsCellPaddingOuterBottom) {
        this.field_64_ftsCellPaddingOuterBottom = field_64_ftsCellPaddingOuterBottom;
    }
    
    @Internal
    public byte getFtsCellPaddingOuterRight() {
        return this.field_65_ftsCellPaddingOuterRight;
    }
    
    @Internal
    public void setFtsCellPaddingOuterRight(final byte field_65_ftsCellPaddingOuterRight) {
        this.field_65_ftsCellPaddingOuterRight = field_65_ftsCellPaddingOuterRight;
    }
    
    @Internal
    public short getWCellSpacingOuterTop() {
        return this.field_66_wCellSpacingOuterTop;
    }
    
    @Internal
    public void setWCellSpacingOuterTop(final short field_66_wCellSpacingOuterTop) {
        this.field_66_wCellSpacingOuterTop = field_66_wCellSpacingOuterTop;
    }
    
    @Internal
    public short getWCellSpacingOuterLeft() {
        return this.field_67_wCellSpacingOuterLeft;
    }
    
    @Internal
    public void setWCellSpacingOuterLeft(final short field_67_wCellSpacingOuterLeft) {
        this.field_67_wCellSpacingOuterLeft = field_67_wCellSpacingOuterLeft;
    }
    
    @Internal
    public short getWCellSpacingOuterBottom() {
        return this.field_68_wCellSpacingOuterBottom;
    }
    
    @Internal
    public void setWCellSpacingOuterBottom(final short field_68_wCellSpacingOuterBottom) {
        this.field_68_wCellSpacingOuterBottom = field_68_wCellSpacingOuterBottom;
    }
    
    @Internal
    public short getWCellSpacingOuterRight() {
        return this.field_69_wCellSpacingOuterRight;
    }
    
    @Internal
    public void setWCellSpacingOuterRight(final short field_69_wCellSpacingOuterRight) {
        this.field_69_wCellSpacingOuterRight = field_69_wCellSpacingOuterRight;
    }
    
    @Internal
    public byte getFtsCellSpacingOuterTop() {
        return this.field_70_ftsCellSpacingOuterTop;
    }
    
    @Internal
    public void setFtsCellSpacingOuterTop(final byte field_70_ftsCellSpacingOuterTop) {
        this.field_70_ftsCellSpacingOuterTop = field_70_ftsCellSpacingOuterTop;
    }
    
    @Internal
    public byte getFtsCellSpacingOuterLeft() {
        return this.field_71_ftsCellSpacingOuterLeft;
    }
    
    @Internal
    public void setFtsCellSpacingOuterLeft(final byte field_71_ftsCellSpacingOuterLeft) {
        this.field_71_ftsCellSpacingOuterLeft = field_71_ftsCellSpacingOuterLeft;
    }
    
    @Internal
    public byte getFtsCellSpacingOuterBottom() {
        return this.field_72_ftsCellSpacingOuterBottom;
    }
    
    @Internal
    public void setFtsCellSpacingOuterBottom(final byte field_72_ftsCellSpacingOuterBottom) {
        this.field_72_ftsCellSpacingOuterBottom = field_72_ftsCellSpacingOuterBottom;
    }
    
    @Internal
    public byte getFtsCellSpacingOuterRight() {
        return this.field_73_ftsCellSpacingOuterRight;
    }
    
    @Internal
    public void setFtsCellSpacingOuterRight(final byte field_73_ftsCellSpacingOuterRight) {
        this.field_73_ftsCellSpacingOuterRight = field_73_ftsCellSpacingOuterRight;
    }
    
    @Internal
    public TableCellDescriptor[] getRgtc() {
        return this.field_74_rgtc;
    }
    
    @Internal
    public void setRgtc(final TableCellDescriptor[] field_74_rgtc) {
        this.field_74_rgtc = field_74_rgtc;
    }
    
    @Internal
    public ShadingDescriptor[] getRgshd() {
        return this.field_75_rgshd;
    }
    
    @Internal
    public void setRgshd(final ShadingDescriptor[] field_75_rgshd) {
        this.field_75_rgshd = field_75_rgshd;
    }
    
    @Internal
    public byte getFPropRMark() {
        return this.field_76_fPropRMark;
    }
    
    @Internal
    public void setFPropRMark(final byte field_76_fPropRMark) {
        this.field_76_fPropRMark = field_76_fPropRMark;
    }
    
    @Internal
    public byte getFHasOldProps() {
        return this.field_77_fHasOldProps;
    }
    
    @Internal
    public void setFHasOldProps(final byte field_77_fHasOldProps) {
        this.field_77_fHasOldProps = field_77_fHasOldProps;
    }
    
    @Internal
    public short getCHorzBands() {
        return this.field_78_cHorzBands;
    }
    
    @Internal
    public void setCHorzBands(final short field_78_cHorzBands) {
        this.field_78_cHorzBands = field_78_cHorzBands;
    }
    
    @Internal
    public short getCVertBands() {
        return this.field_79_cVertBands;
    }
    
    @Internal
    public void setCVertBands(final short field_79_cVertBands) {
        this.field_79_cVertBands = field_79_cVertBands;
    }
    
    @Internal
    public BorderCode getRgbrcInsideDefault_0() {
        return this.field_80_rgbrcInsideDefault_0;
    }
    
    @Internal
    public void setRgbrcInsideDefault_0(final BorderCode field_80_rgbrcInsideDefault_0) {
        this.field_80_rgbrcInsideDefault_0 = field_80_rgbrcInsideDefault_0;
    }
    
    @Internal
    public BorderCode getRgbrcInsideDefault_1() {
        return this.field_81_rgbrcInsideDefault_1;
    }
    
    @Internal
    public void setRgbrcInsideDefault_1(final BorderCode field_81_rgbrcInsideDefault_1) {
        this.field_81_rgbrcInsideDefault_1 = field_81_rgbrcInsideDefault_1;
    }
    
    @Internal
    public void setFAutofit(final boolean value) {
        this.field_13_widthAndFitsFlags = TAPAbstractType.fAutofit.setBoolean(this.field_13_widthAndFitsFlags, value);
    }
    
    @Internal
    public boolean isFAutofit() {
        return TAPAbstractType.fAutofit.isSet(this.field_13_widthAndFitsFlags);
    }
    
    @Internal
    public void setFKeepFollow(final boolean value) {
        this.field_13_widthAndFitsFlags = TAPAbstractType.fKeepFollow.setBoolean(this.field_13_widthAndFitsFlags, value);
    }
    
    @Internal
    public boolean isFKeepFollow() {
        return TAPAbstractType.fKeepFollow.isSet(this.field_13_widthAndFitsFlags);
    }
    
    @Internal
    public void setFtsWidth(final byte value) {
        this.field_13_widthAndFitsFlags = TAPAbstractType.ftsWidth.setValue(this.field_13_widthAndFitsFlags, value);
    }
    
    @Internal
    public byte getFtsWidth() {
        return (byte)TAPAbstractType.ftsWidth.getValue(this.field_13_widthAndFitsFlags);
    }
    
    @Internal
    public void setFtsWidthIndent(final byte value) {
        this.field_13_widthAndFitsFlags = TAPAbstractType.ftsWidthIndent.setValue(this.field_13_widthAndFitsFlags, value);
    }
    
    @Internal
    public byte getFtsWidthIndent() {
        return (byte)TAPAbstractType.ftsWidthIndent.getValue(this.field_13_widthAndFitsFlags);
    }
    
    @Internal
    public void setFtsWidthBefore(final byte value) {
        this.field_13_widthAndFitsFlags = TAPAbstractType.ftsWidthBefore.setValue(this.field_13_widthAndFitsFlags, value);
    }
    
    @Internal
    public byte getFtsWidthBefore() {
        return (byte)TAPAbstractType.ftsWidthBefore.getValue(this.field_13_widthAndFitsFlags);
    }
    
    @Internal
    public void setFtsWidthAfter(final byte value) {
        this.field_13_widthAndFitsFlags = TAPAbstractType.ftsWidthAfter.setValue(this.field_13_widthAndFitsFlags, value);
    }
    
    @Internal
    public byte getFtsWidthAfter() {
        return (byte)TAPAbstractType.ftsWidthAfter.getValue(this.field_13_widthAndFitsFlags);
    }
    
    @Internal
    public void setFNeverBeenAutofit(final boolean value) {
        this.field_13_widthAndFitsFlags = TAPAbstractType.fNeverBeenAutofit.setBoolean(this.field_13_widthAndFitsFlags, value);
    }
    
    @Internal
    public boolean isFNeverBeenAutofit() {
        return TAPAbstractType.fNeverBeenAutofit.isSet(this.field_13_widthAndFitsFlags);
    }
    
    @Internal
    public void setFInvalAutofit(final boolean value) {
        this.field_13_widthAndFitsFlags = TAPAbstractType.fInvalAutofit.setBoolean(this.field_13_widthAndFitsFlags, value);
    }
    
    @Internal
    public boolean isFInvalAutofit() {
        return TAPAbstractType.fInvalAutofit.isSet(this.field_13_widthAndFitsFlags);
    }
    
    @Internal
    public void setWidthAndFitsFlags_empty1(final byte value) {
        this.field_13_widthAndFitsFlags = TAPAbstractType.widthAndFitsFlags_empty1.setValue(this.field_13_widthAndFitsFlags, value);
    }
    
    @Internal
    public byte getWidthAndFitsFlags_empty1() {
        return (byte)TAPAbstractType.widthAndFitsFlags_empty1.getValue(this.field_13_widthAndFitsFlags);
    }
    
    @Internal
    public void setFVert(final boolean value) {
        this.field_13_widthAndFitsFlags = TAPAbstractType.fVert.setBoolean(this.field_13_widthAndFitsFlags, value);
    }
    
    @Internal
    public boolean isFVert() {
        return TAPAbstractType.fVert.isSet(this.field_13_widthAndFitsFlags);
    }
    
    @Internal
    public void setPcVert(final byte value) {
        this.field_13_widthAndFitsFlags = TAPAbstractType.pcVert.setValue(this.field_13_widthAndFitsFlags, value);
    }
    
    @Internal
    public byte getPcVert() {
        return (byte)TAPAbstractType.pcVert.getValue(this.field_13_widthAndFitsFlags);
    }
    
    @Internal
    public void setPcHorz(final byte value) {
        this.field_13_widthAndFitsFlags = TAPAbstractType.pcHorz.setValue(this.field_13_widthAndFitsFlags, value);
    }
    
    @Internal
    public byte getPcHorz() {
        return (byte)TAPAbstractType.pcHorz.getValue(this.field_13_widthAndFitsFlags);
    }
    
    @Internal
    public void setWidthAndFitsFlags_empty2(final short value) {
        this.field_13_widthAndFitsFlags = TAPAbstractType.widthAndFitsFlags_empty2.setValue(this.field_13_widthAndFitsFlags, value);
    }
    
    @Internal
    public short getWidthAndFitsFlags_empty2() {
        return (short)TAPAbstractType.widthAndFitsFlags_empty2.getValue(this.field_13_widthAndFitsFlags);
    }
    
    @Internal
    public void setFFirstRow(final boolean value) {
        this.field_25_internalFlags = TAPAbstractType.fFirstRow.setBoolean(this.field_25_internalFlags, value);
    }
    
    @Internal
    public boolean isFFirstRow() {
        return TAPAbstractType.fFirstRow.isSet(this.field_25_internalFlags);
    }
    
    @Internal
    public void setFLastRow(final boolean value) {
        this.field_25_internalFlags = TAPAbstractType.fLastRow.setBoolean(this.field_25_internalFlags, value);
    }
    
    @Internal
    public boolean isFLastRow() {
        return TAPAbstractType.fLastRow.isSet(this.field_25_internalFlags);
    }
    
    @Internal
    public void setFOutline(final boolean value) {
        this.field_25_internalFlags = TAPAbstractType.fOutline.setBoolean(this.field_25_internalFlags, value);
    }
    
    @Internal
    public boolean isFOutline() {
        return TAPAbstractType.fOutline.isSet(this.field_25_internalFlags);
    }
    
    @Internal
    public void setFOrigWordTableRules(final boolean value) {
        this.field_25_internalFlags = TAPAbstractType.fOrigWordTableRules.setBoolean(this.field_25_internalFlags, value);
    }
    
    @Internal
    public boolean isFOrigWordTableRules() {
        return TAPAbstractType.fOrigWordTableRules.isSet(this.field_25_internalFlags);
    }
    
    @Internal
    public void setFCellSpacing(final boolean value) {
        this.field_25_internalFlags = TAPAbstractType.fCellSpacing.setBoolean(this.field_25_internalFlags, value);
    }
    
    @Internal
    public boolean isFCellSpacing() {
        return TAPAbstractType.fCellSpacing.isSet(this.field_25_internalFlags);
    }
    
    @Internal
    public void setGrpfTap_unused(final short value) {
        this.field_25_internalFlags = TAPAbstractType.grpfTap_unused.setValue(this.field_25_internalFlags, value);
    }
    
    @Internal
    public short getGrpfTap_unused() {
        return (short)TAPAbstractType.grpfTap_unused.getValue(this.field_25_internalFlags);
    }
    
    @Internal
    public void setFWrapToWwd(final boolean value) {
        this.field_32_viewFlags = TAPAbstractType.fWrapToWwd.setBoolean(this.field_32_viewFlags, value);
    }
    
    @Internal
    public boolean isFWrapToWwd() {
        return TAPAbstractType.fWrapToWwd.isSet(this.field_32_viewFlags);
    }
    
    @Internal
    public void setFNotPageView(final boolean value) {
        this.field_32_viewFlags = TAPAbstractType.fNotPageView.setBoolean(this.field_32_viewFlags, value);
    }
    
    @Internal
    public boolean isFNotPageView() {
        return TAPAbstractType.fNotPageView.isSet(this.field_32_viewFlags);
    }
    
    @Internal
    public void setViewFlags_unused1(final boolean value) {
        this.field_32_viewFlags = TAPAbstractType.viewFlags_unused1.setBoolean(this.field_32_viewFlags, value);
    }
    
    @Internal
    public boolean isViewFlags_unused1() {
        return TAPAbstractType.viewFlags_unused1.isSet(this.field_32_viewFlags);
    }
    
    @Internal
    public void setFWebView(final boolean value) {
        this.field_32_viewFlags = TAPAbstractType.fWebView.setBoolean(this.field_32_viewFlags, value);
    }
    
    @Internal
    public boolean isFWebView() {
        return TAPAbstractType.fWebView.isSet(this.field_32_viewFlags);
    }
    
    @Internal
    public void setFAdjusted(final boolean value) {
        this.field_32_viewFlags = TAPAbstractType.fAdjusted.setBoolean(this.field_32_viewFlags, value);
    }
    
    @Internal
    public boolean isFAdjusted() {
        return TAPAbstractType.fAdjusted.isSet(this.field_32_viewFlags);
    }
    
    @Internal
    public void setViewFlags_unused2(final short value) {
        this.field_32_viewFlags = TAPAbstractType.viewFlags_unused2.setValue(this.field_32_viewFlags, value);
    }
    
    @Internal
    public short getViewFlags_unused2() {
        return (short)TAPAbstractType.viewFlags_unused2.getValue(this.field_32_viewFlags);
    }
    
    static {
        TAPAbstractType.fAutofit = new BitField(1);
        TAPAbstractType.fKeepFollow = new BitField(2);
        TAPAbstractType.ftsWidth = new BitField(28);
        TAPAbstractType.ftsWidthIndent = new BitField(224);
        TAPAbstractType.ftsWidthBefore = new BitField(1792);
        TAPAbstractType.ftsWidthAfter = new BitField(14336);
        TAPAbstractType.fNeverBeenAutofit = new BitField(16384);
        TAPAbstractType.fInvalAutofit = new BitField(32768);
        TAPAbstractType.widthAndFitsFlags_empty1 = new BitField(458752);
        TAPAbstractType.fVert = new BitField(524288);
        TAPAbstractType.pcVert = new BitField(3145728);
        TAPAbstractType.pcHorz = new BitField(12582912);
        TAPAbstractType.widthAndFitsFlags_empty2 = new BitField(-16777216);
        TAPAbstractType.fFirstRow = new BitField(1);
        TAPAbstractType.fLastRow = new BitField(2);
        TAPAbstractType.fOutline = new BitField(4);
        TAPAbstractType.fOrigWordTableRules = new BitField(8);
        TAPAbstractType.fCellSpacing = new BitField(16);
        TAPAbstractType.grpfTap_unused = new BitField(65504);
        TAPAbstractType.fWrapToWwd = new BitField(1);
        TAPAbstractType.fNotPageView = new BitField(2);
        TAPAbstractType.viewFlags_unused1 = new BitField(4);
        TAPAbstractType.fWebView = new BitField(8);
        TAPAbstractType.fAdjusted = new BitField(16);
        TAPAbstractType.viewFlags_unused2 = new BitField(65504);
    }
}
