// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hwpf.model.types;

import org.apache.poi.hwpf.model.TabDescriptor;
import org.apache.poi.hwpf.usermodel.DateAndTime;
import org.apache.poi.hwpf.usermodel.ShadingDescriptor;
import org.apache.poi.hwpf.usermodel.BorderCode;
import org.apache.poi.util.BitField;
import org.apache.poi.hwpf.usermodel.DropCapSpecifier;
import org.apache.poi.hwpf.usermodel.LineSpacingDescriptor;
import org.apache.poi.util.Internal;

@Internal
public abstract class PAPAbstractType
{
    protected int field_1_istd;
    protected boolean field_2_fSideBySide;
    protected boolean field_3_fKeep;
    protected boolean field_4_fKeepFollow;
    protected boolean field_5_fPageBreakBefore;
    protected byte field_6_brcl;
    protected static final byte BRCL_SINGLE = 0;
    protected static final byte BRCL_THICK = 1;
    protected static final byte BRCL_DOUBLE = 2;
    protected static final byte BRCL_SHADOW = 3;
    protected byte field_7_brcp;
    protected static final byte BRCP_NONE = 0;
    protected static final byte BRCP_BORDER_ABOVE = 1;
    protected static final byte BRCP_BORDER_BELOW = 2;
    protected static final byte BRCP_BOX_AROUND = 15;
    protected static final byte BRCP_BAR_TO_LEFT_OF_PARAGRAPH = 16;
    protected byte field_8_ilvl;
    protected int field_9_ilfo;
    protected boolean field_10_fNoLnn;
    protected LineSpacingDescriptor field_11_lspd;
    protected int field_12_dyaBefore;
    protected int field_13_dyaAfter;
    protected boolean field_14_fInTable;
    protected boolean field_15_finTableW97;
    protected boolean field_16_fTtp;
    protected int field_17_dxaAbs;
    protected int field_18_dyaAbs;
    protected int field_19_dxaWidth;
    protected boolean field_20_fBrLnAbove;
    protected boolean field_21_fBrLnBelow;
    protected byte field_22_pcVert;
    protected byte field_23_pcHorz;
    protected byte field_24_wr;
    protected boolean field_25_fNoAutoHyph;
    protected int field_26_dyaHeight;
    protected boolean field_27_fMinHeight;
    protected static final boolean FMINHEIGHT_EXACT = false;
    protected static final boolean FMINHEIGHT_AT_LEAST = true;
    protected DropCapSpecifier field_28_dcs;
    protected int field_29_dyaFromText;
    protected int field_30_dxaFromText;
    protected boolean field_31_fLocked;
    protected boolean field_32_fWidowControl;
    protected boolean field_33_fKinsoku;
    protected boolean field_34_fWordWrap;
    protected boolean field_35_fOverflowPunct;
    protected boolean field_36_fTopLinePunct;
    protected boolean field_37_fAutoSpaceDE;
    protected boolean field_38_fAutoSpaceDN;
    protected int field_39_wAlignFont;
    protected static final byte WALIGNFONT_HANGING = 0;
    protected static final byte WALIGNFONT_CENTERED = 1;
    protected static final byte WALIGNFONT_ROMAN = 2;
    protected static final byte WALIGNFONT_VARIABLE = 3;
    protected static final byte WALIGNFONT_AUTO = 4;
    protected short field_40_fontAlign;
    private static BitField fVertical;
    private static BitField fBackward;
    private static BitField fRotateFont;
    protected byte field_41_lvl;
    protected boolean field_42_fBiDi;
    protected boolean field_43_fNumRMIns;
    protected boolean field_44_fCrLf;
    protected boolean field_45_fUsePgsuSettings;
    protected boolean field_46_fAdjustRight;
    protected int field_47_itap;
    protected boolean field_48_fInnerTableCell;
    protected boolean field_49_fOpenTch;
    protected boolean field_50_fTtpEmbedded;
    protected short field_51_dxcRight;
    protected short field_52_dxcLeft;
    protected short field_53_dxcLeft1;
    protected boolean field_54_fDyaBeforeAuto;
    protected boolean field_55_fDyaAfterAuto;
    protected int field_56_dxaRight;
    protected int field_57_dxaLeft;
    protected int field_58_dxaLeft1;
    protected byte field_59_jc;
    protected BorderCode field_60_brcTop;
    protected BorderCode field_61_brcLeft;
    protected BorderCode field_62_brcBottom;
    protected BorderCode field_63_brcRight;
    protected BorderCode field_64_brcBetween;
    protected BorderCode field_65_brcBar;
    protected ShadingDescriptor field_66_shd;
    protected byte[] field_67_anld;
    protected byte[] field_68_phe;
    protected boolean field_69_fPropRMark;
    protected int field_70_ibstPropRMark;
    protected DateAndTime field_71_dttmPropRMark;
    protected int field_72_itbdMac;
    protected int[] field_73_rgdxaTab;
    protected TabDescriptor[] field_74_rgtbd;
    protected byte[] field_75_numrm;
    protected byte[] field_76_ptap;
    protected boolean field_77_fNoAllowOverlap;
    protected long field_78_ipgp;
    protected long field_79_rsid;
    
    protected PAPAbstractType() {
        this.field_11_lspd = new LineSpacingDescriptor();
        this.field_11_lspd = new LineSpacingDescriptor();
        this.field_28_dcs = new DropCapSpecifier();
        this.field_32_fWidowControl = true;
        this.field_41_lvl = 9;
        this.field_60_brcTop = new BorderCode();
        this.field_61_brcLeft = new BorderCode();
        this.field_62_brcBottom = new BorderCode();
        this.field_63_brcRight = new BorderCode();
        this.field_64_brcBetween = new BorderCode();
        this.field_65_brcBar = new BorderCode();
        this.field_66_shd = new ShadingDescriptor();
        this.field_67_anld = new byte[0];
        this.field_68_phe = new byte[0];
        this.field_71_dttmPropRMark = new DateAndTime();
        this.field_73_rgdxaTab = new int[0];
        this.field_74_rgtbd = new TabDescriptor[0];
        this.field_75_numrm = new byte[0];
        this.field_76_ptap = new byte[0];
    }
    
    @Override
    public String toString() {
        final StringBuilder builder = new StringBuilder();
        builder.append("[PAP]\n");
        builder.append("    .istd                 = ");
        builder.append(" (").append(this.getIstd()).append(" )\n");
        builder.append("    .fSideBySide          = ");
        builder.append(" (").append(this.getFSideBySide()).append(" )\n");
        builder.append("    .fKeep                = ");
        builder.append(" (").append(this.getFKeep()).append(" )\n");
        builder.append("    .fKeepFollow          = ");
        builder.append(" (").append(this.getFKeepFollow()).append(" )\n");
        builder.append("    .fPageBreakBefore     = ");
        builder.append(" (").append(this.getFPageBreakBefore()).append(" )\n");
        builder.append("    .brcl                 = ");
        builder.append(" (").append(this.getBrcl()).append(" )\n");
        builder.append("    .brcp                 = ");
        builder.append(" (").append(this.getBrcp()).append(" )\n");
        builder.append("    .ilvl                 = ");
        builder.append(" (").append(this.getIlvl()).append(" )\n");
        builder.append("    .ilfo                 = ");
        builder.append(" (").append(this.getIlfo()).append(" )\n");
        builder.append("    .fNoLnn               = ");
        builder.append(" (").append(this.getFNoLnn()).append(" )\n");
        builder.append("    .lspd                 = ");
        builder.append(" (").append(this.getLspd()).append(" )\n");
        builder.append("    .dyaBefore            = ");
        builder.append(" (").append(this.getDyaBefore()).append(" )\n");
        builder.append("    .dyaAfter             = ");
        builder.append(" (").append(this.getDyaAfter()).append(" )\n");
        builder.append("    .fInTable             = ");
        builder.append(" (").append(this.getFInTable()).append(" )\n");
        builder.append("    .finTableW97          = ");
        builder.append(" (").append(this.getFinTableW97()).append(" )\n");
        builder.append("    .fTtp                 = ");
        builder.append(" (").append(this.getFTtp()).append(" )\n");
        builder.append("    .dxaAbs               = ");
        builder.append(" (").append(this.getDxaAbs()).append(" )\n");
        builder.append("    .dyaAbs               = ");
        builder.append(" (").append(this.getDyaAbs()).append(" )\n");
        builder.append("    .dxaWidth             = ");
        builder.append(" (").append(this.getDxaWidth()).append(" )\n");
        builder.append("    .fBrLnAbove           = ");
        builder.append(" (").append(this.getFBrLnAbove()).append(" )\n");
        builder.append("    .fBrLnBelow           = ");
        builder.append(" (").append(this.getFBrLnBelow()).append(" )\n");
        builder.append("    .pcVert               = ");
        builder.append(" (").append(this.getPcVert()).append(" )\n");
        builder.append("    .pcHorz               = ");
        builder.append(" (").append(this.getPcHorz()).append(" )\n");
        builder.append("    .wr                   = ");
        builder.append(" (").append(this.getWr()).append(" )\n");
        builder.append("    .fNoAutoHyph          = ");
        builder.append(" (").append(this.getFNoAutoHyph()).append(" )\n");
        builder.append("    .dyaHeight            = ");
        builder.append(" (").append(this.getDyaHeight()).append(" )\n");
        builder.append("    .fMinHeight           = ");
        builder.append(" (").append(this.getFMinHeight()).append(" )\n");
        builder.append("    .dcs                  = ");
        builder.append(" (").append(this.getDcs()).append(" )\n");
        builder.append("    .dyaFromText          = ");
        builder.append(" (").append(this.getDyaFromText()).append(" )\n");
        builder.append("    .dxaFromText          = ");
        builder.append(" (").append(this.getDxaFromText()).append(" )\n");
        builder.append("    .fLocked              = ");
        builder.append(" (").append(this.getFLocked()).append(" )\n");
        builder.append("    .fWidowControl        = ");
        builder.append(" (").append(this.getFWidowControl()).append(" )\n");
        builder.append("    .fKinsoku             = ");
        builder.append(" (").append(this.getFKinsoku()).append(" )\n");
        builder.append("    .fWordWrap            = ");
        builder.append(" (").append(this.getFWordWrap()).append(" )\n");
        builder.append("    .fOverflowPunct       = ");
        builder.append(" (").append(this.getFOverflowPunct()).append(" )\n");
        builder.append("    .fTopLinePunct        = ");
        builder.append(" (").append(this.getFTopLinePunct()).append(" )\n");
        builder.append("    .fAutoSpaceDE         = ");
        builder.append(" (").append(this.getFAutoSpaceDE()).append(" )\n");
        builder.append("    .fAutoSpaceDN         = ");
        builder.append(" (").append(this.getFAutoSpaceDN()).append(" )\n");
        builder.append("    .wAlignFont           = ");
        builder.append(" (").append(this.getWAlignFont()).append(" )\n");
        builder.append("    .fontAlign            = ");
        builder.append(" (").append(this.getFontAlign()).append(" )\n");
        builder.append("         .fVertical                = ").append(this.isFVertical()).append('\n');
        builder.append("         .fBackward                = ").append(this.isFBackward()).append('\n');
        builder.append("         .fRotateFont              = ").append(this.isFRotateFont()).append('\n');
        builder.append("    .lvl                  = ");
        builder.append(" (").append(this.getLvl()).append(" )\n");
        builder.append("    .fBiDi                = ");
        builder.append(" (").append(this.getFBiDi()).append(" )\n");
        builder.append("    .fNumRMIns            = ");
        builder.append(" (").append(this.getFNumRMIns()).append(" )\n");
        builder.append("    .fCrLf                = ");
        builder.append(" (").append(this.getFCrLf()).append(" )\n");
        builder.append("    .fUsePgsuSettings     = ");
        builder.append(" (").append(this.getFUsePgsuSettings()).append(" )\n");
        builder.append("    .fAdjustRight         = ");
        builder.append(" (").append(this.getFAdjustRight()).append(" )\n");
        builder.append("    .itap                 = ");
        builder.append(" (").append(this.getItap()).append(" )\n");
        builder.append("    .fInnerTableCell      = ");
        builder.append(" (").append(this.getFInnerTableCell()).append(" )\n");
        builder.append("    .fOpenTch             = ");
        builder.append(" (").append(this.getFOpenTch()).append(" )\n");
        builder.append("    .fTtpEmbedded         = ");
        builder.append(" (").append(this.getFTtpEmbedded()).append(" )\n");
        builder.append("    .dxcRight             = ");
        builder.append(" (").append(this.getDxcRight()).append(" )\n");
        builder.append("    .dxcLeft              = ");
        builder.append(" (").append(this.getDxcLeft()).append(" )\n");
        builder.append("    .dxcLeft1             = ");
        builder.append(" (").append(this.getDxcLeft1()).append(" )\n");
        builder.append("    .fDyaBeforeAuto       = ");
        builder.append(" (").append(this.getFDyaBeforeAuto()).append(" )\n");
        builder.append("    .fDyaAfterAuto        = ");
        builder.append(" (").append(this.getFDyaAfterAuto()).append(" )\n");
        builder.append("    .dxaRight             = ");
        builder.append(" (").append(this.getDxaRight()).append(" )\n");
        builder.append("    .dxaLeft              = ");
        builder.append(" (").append(this.getDxaLeft()).append(" )\n");
        builder.append("    .dxaLeft1             = ");
        builder.append(" (").append(this.getDxaLeft1()).append(" )\n");
        builder.append("    .jc                   = ");
        builder.append(" (").append(this.getJc()).append(" )\n");
        builder.append("    .brcTop               = ");
        builder.append(" (").append(this.getBrcTop()).append(" )\n");
        builder.append("    .brcLeft              = ");
        builder.append(" (").append(this.getBrcLeft()).append(" )\n");
        builder.append("    .brcBottom            = ");
        builder.append(" (").append(this.getBrcBottom()).append(" )\n");
        builder.append("    .brcRight             = ");
        builder.append(" (").append(this.getBrcRight()).append(" )\n");
        builder.append("    .brcBetween           = ");
        builder.append(" (").append(this.getBrcBetween()).append(" )\n");
        builder.append("    .brcBar               = ");
        builder.append(" (").append(this.getBrcBar()).append(" )\n");
        builder.append("    .shd                  = ");
        builder.append(" (").append(this.getShd()).append(" )\n");
        builder.append("    .anld                 = ");
        builder.append(" (").append(this.getAnld()).append(" )\n");
        builder.append("    .phe                  = ");
        builder.append(" (").append(this.getPhe()).append(" )\n");
        builder.append("    .fPropRMark           = ");
        builder.append(" (").append(this.getFPropRMark()).append(" )\n");
        builder.append("    .ibstPropRMark        = ");
        builder.append(" (").append(this.getIbstPropRMark()).append(" )\n");
        builder.append("    .dttmPropRMark        = ");
        builder.append(" (").append(this.getDttmPropRMark()).append(" )\n");
        builder.append("    .itbdMac              = ");
        builder.append(" (").append(this.getItbdMac()).append(" )\n");
        builder.append("    .rgdxaTab             = ");
        builder.append(" (").append(this.getRgdxaTab()).append(" )\n");
        builder.append("    .rgtbd                = ");
        builder.append(" (").append(this.getRgtbd()).append(" )\n");
        builder.append("    .numrm                = ");
        builder.append(" (").append(this.getNumrm()).append(" )\n");
        builder.append("    .ptap                 = ");
        builder.append(" (").append(this.getPtap()).append(" )\n");
        builder.append("    .fNoAllowOverlap      = ");
        builder.append(" (").append(this.getFNoAllowOverlap()).append(" )\n");
        builder.append("    .ipgp                 = ");
        builder.append(" (").append(this.getIpgp()).append(" )\n");
        builder.append("    .rsid                 = ");
        builder.append(" (").append(this.getRsid()).append(" )\n");
        builder.append("[/PAP]\n");
        return builder.toString();
    }
    
    @Internal
    public int getIstd() {
        return this.field_1_istd;
    }
    
    @Internal
    public void setIstd(final int field_1_istd) {
        this.field_1_istd = field_1_istd;
    }
    
    @Internal
    public boolean getFSideBySide() {
        return this.field_2_fSideBySide;
    }
    
    @Internal
    public void setFSideBySide(final boolean field_2_fSideBySide) {
        this.field_2_fSideBySide = field_2_fSideBySide;
    }
    
    @Internal
    public boolean getFKeep() {
        return this.field_3_fKeep;
    }
    
    @Internal
    public void setFKeep(final boolean field_3_fKeep) {
        this.field_3_fKeep = field_3_fKeep;
    }
    
    @Internal
    public boolean getFKeepFollow() {
        return this.field_4_fKeepFollow;
    }
    
    @Internal
    public void setFKeepFollow(final boolean field_4_fKeepFollow) {
        this.field_4_fKeepFollow = field_4_fKeepFollow;
    }
    
    @Internal
    public boolean getFPageBreakBefore() {
        return this.field_5_fPageBreakBefore;
    }
    
    @Internal
    public void setFPageBreakBefore(final boolean field_5_fPageBreakBefore) {
        this.field_5_fPageBreakBefore = field_5_fPageBreakBefore;
    }
    
    @Internal
    public byte getBrcl() {
        return this.field_6_brcl;
    }
    
    @Internal
    public void setBrcl(final byte field_6_brcl) {
        this.field_6_brcl = field_6_brcl;
    }
    
    @Internal
    public byte getBrcp() {
        return this.field_7_brcp;
    }
    
    @Internal
    public void setBrcp(final byte field_7_brcp) {
        this.field_7_brcp = field_7_brcp;
    }
    
    @Internal
    public byte getIlvl() {
        return this.field_8_ilvl;
    }
    
    @Internal
    public void setIlvl(final byte field_8_ilvl) {
        this.field_8_ilvl = field_8_ilvl;
    }
    
    @Internal
    public int getIlfo() {
        return this.field_9_ilfo;
    }
    
    @Internal
    public void setIlfo(final int field_9_ilfo) {
        this.field_9_ilfo = field_9_ilfo;
    }
    
    @Internal
    public boolean getFNoLnn() {
        return this.field_10_fNoLnn;
    }
    
    @Internal
    public void setFNoLnn(final boolean field_10_fNoLnn) {
        this.field_10_fNoLnn = field_10_fNoLnn;
    }
    
    @Internal
    public LineSpacingDescriptor getLspd() {
        return this.field_11_lspd;
    }
    
    @Internal
    public void setLspd(final LineSpacingDescriptor field_11_lspd) {
        this.field_11_lspd = field_11_lspd;
    }
    
    @Internal
    public int getDyaBefore() {
        return this.field_12_dyaBefore;
    }
    
    @Internal
    public void setDyaBefore(final int field_12_dyaBefore) {
        this.field_12_dyaBefore = field_12_dyaBefore;
    }
    
    @Internal
    public int getDyaAfter() {
        return this.field_13_dyaAfter;
    }
    
    @Internal
    public void setDyaAfter(final int field_13_dyaAfter) {
        this.field_13_dyaAfter = field_13_dyaAfter;
    }
    
    @Internal
    public boolean getFInTable() {
        return this.field_14_fInTable;
    }
    
    @Internal
    public void setFInTable(final boolean field_14_fInTable) {
        this.field_14_fInTable = field_14_fInTable;
    }
    
    @Internal
    public boolean getFinTableW97() {
        return this.field_15_finTableW97;
    }
    
    @Internal
    public void setFinTableW97(final boolean field_15_finTableW97) {
        this.field_15_finTableW97 = field_15_finTableW97;
    }
    
    @Internal
    public boolean getFTtp() {
        return this.field_16_fTtp;
    }
    
    @Internal
    public void setFTtp(final boolean field_16_fTtp) {
        this.field_16_fTtp = field_16_fTtp;
    }
    
    @Internal
    public int getDxaAbs() {
        return this.field_17_dxaAbs;
    }
    
    @Internal
    public void setDxaAbs(final int field_17_dxaAbs) {
        this.field_17_dxaAbs = field_17_dxaAbs;
    }
    
    @Internal
    public int getDyaAbs() {
        return this.field_18_dyaAbs;
    }
    
    @Internal
    public void setDyaAbs(final int field_18_dyaAbs) {
        this.field_18_dyaAbs = field_18_dyaAbs;
    }
    
    @Internal
    public int getDxaWidth() {
        return this.field_19_dxaWidth;
    }
    
    @Internal
    public void setDxaWidth(final int field_19_dxaWidth) {
        this.field_19_dxaWidth = field_19_dxaWidth;
    }
    
    @Internal
    public boolean getFBrLnAbove() {
        return this.field_20_fBrLnAbove;
    }
    
    @Internal
    public void setFBrLnAbove(final boolean field_20_fBrLnAbove) {
        this.field_20_fBrLnAbove = field_20_fBrLnAbove;
    }
    
    @Internal
    public boolean getFBrLnBelow() {
        return this.field_21_fBrLnBelow;
    }
    
    @Internal
    public void setFBrLnBelow(final boolean field_21_fBrLnBelow) {
        this.field_21_fBrLnBelow = field_21_fBrLnBelow;
    }
    
    @Internal
    public byte getPcVert() {
        return this.field_22_pcVert;
    }
    
    @Internal
    public void setPcVert(final byte field_22_pcVert) {
        this.field_22_pcVert = field_22_pcVert;
    }
    
    @Internal
    public byte getPcHorz() {
        return this.field_23_pcHorz;
    }
    
    @Internal
    public void setPcHorz(final byte field_23_pcHorz) {
        this.field_23_pcHorz = field_23_pcHorz;
    }
    
    @Internal
    public byte getWr() {
        return this.field_24_wr;
    }
    
    @Internal
    public void setWr(final byte field_24_wr) {
        this.field_24_wr = field_24_wr;
    }
    
    @Internal
    public boolean getFNoAutoHyph() {
        return this.field_25_fNoAutoHyph;
    }
    
    @Internal
    public void setFNoAutoHyph(final boolean field_25_fNoAutoHyph) {
        this.field_25_fNoAutoHyph = field_25_fNoAutoHyph;
    }
    
    @Internal
    public int getDyaHeight() {
        return this.field_26_dyaHeight;
    }
    
    @Internal
    public void setDyaHeight(final int field_26_dyaHeight) {
        this.field_26_dyaHeight = field_26_dyaHeight;
    }
    
    @Internal
    public boolean getFMinHeight() {
        return this.field_27_fMinHeight;
    }
    
    @Internal
    public void setFMinHeight(final boolean field_27_fMinHeight) {
        this.field_27_fMinHeight = field_27_fMinHeight;
    }
    
    @Internal
    public DropCapSpecifier getDcs() {
        return this.field_28_dcs;
    }
    
    @Internal
    public void setDcs(final DropCapSpecifier field_28_dcs) {
        this.field_28_dcs = field_28_dcs;
    }
    
    @Internal
    public int getDyaFromText() {
        return this.field_29_dyaFromText;
    }
    
    @Internal
    public void setDyaFromText(final int field_29_dyaFromText) {
        this.field_29_dyaFromText = field_29_dyaFromText;
    }
    
    @Internal
    public int getDxaFromText() {
        return this.field_30_dxaFromText;
    }
    
    @Internal
    public void setDxaFromText(final int field_30_dxaFromText) {
        this.field_30_dxaFromText = field_30_dxaFromText;
    }
    
    @Internal
    public boolean getFLocked() {
        return this.field_31_fLocked;
    }
    
    @Internal
    public void setFLocked(final boolean field_31_fLocked) {
        this.field_31_fLocked = field_31_fLocked;
    }
    
    @Internal
    public boolean getFWidowControl() {
        return this.field_32_fWidowControl;
    }
    
    @Internal
    public void setFWidowControl(final boolean field_32_fWidowControl) {
        this.field_32_fWidowControl = field_32_fWidowControl;
    }
    
    @Internal
    public boolean getFKinsoku() {
        return this.field_33_fKinsoku;
    }
    
    @Internal
    public void setFKinsoku(final boolean field_33_fKinsoku) {
        this.field_33_fKinsoku = field_33_fKinsoku;
    }
    
    @Internal
    public boolean getFWordWrap() {
        return this.field_34_fWordWrap;
    }
    
    @Internal
    public void setFWordWrap(final boolean field_34_fWordWrap) {
        this.field_34_fWordWrap = field_34_fWordWrap;
    }
    
    @Internal
    public boolean getFOverflowPunct() {
        return this.field_35_fOverflowPunct;
    }
    
    @Internal
    public void setFOverflowPunct(final boolean field_35_fOverflowPunct) {
        this.field_35_fOverflowPunct = field_35_fOverflowPunct;
    }
    
    @Internal
    public boolean getFTopLinePunct() {
        return this.field_36_fTopLinePunct;
    }
    
    @Internal
    public void setFTopLinePunct(final boolean field_36_fTopLinePunct) {
        this.field_36_fTopLinePunct = field_36_fTopLinePunct;
    }
    
    @Internal
    public boolean getFAutoSpaceDE() {
        return this.field_37_fAutoSpaceDE;
    }
    
    @Internal
    public void setFAutoSpaceDE(final boolean field_37_fAutoSpaceDE) {
        this.field_37_fAutoSpaceDE = field_37_fAutoSpaceDE;
    }
    
    @Internal
    public boolean getFAutoSpaceDN() {
        return this.field_38_fAutoSpaceDN;
    }
    
    @Internal
    public void setFAutoSpaceDN(final boolean field_38_fAutoSpaceDN) {
        this.field_38_fAutoSpaceDN = field_38_fAutoSpaceDN;
    }
    
    @Internal
    public int getWAlignFont() {
        return this.field_39_wAlignFont;
    }
    
    @Internal
    public void setWAlignFont(final int field_39_wAlignFont) {
        this.field_39_wAlignFont = field_39_wAlignFont;
    }
    
    @Internal
    public short getFontAlign() {
        return this.field_40_fontAlign;
    }
    
    @Internal
    public void setFontAlign(final short field_40_fontAlign) {
        this.field_40_fontAlign = field_40_fontAlign;
    }
    
    @Internal
    public byte getLvl() {
        return this.field_41_lvl;
    }
    
    @Internal
    public void setLvl(final byte field_41_lvl) {
        this.field_41_lvl = field_41_lvl;
    }
    
    @Internal
    public boolean getFBiDi() {
        return this.field_42_fBiDi;
    }
    
    @Internal
    public void setFBiDi(final boolean field_42_fBiDi) {
        this.field_42_fBiDi = field_42_fBiDi;
    }
    
    @Internal
    public boolean getFNumRMIns() {
        return this.field_43_fNumRMIns;
    }
    
    @Internal
    public void setFNumRMIns(final boolean field_43_fNumRMIns) {
        this.field_43_fNumRMIns = field_43_fNumRMIns;
    }
    
    @Internal
    public boolean getFCrLf() {
        return this.field_44_fCrLf;
    }
    
    @Internal
    public void setFCrLf(final boolean field_44_fCrLf) {
        this.field_44_fCrLf = field_44_fCrLf;
    }
    
    @Internal
    public boolean getFUsePgsuSettings() {
        return this.field_45_fUsePgsuSettings;
    }
    
    @Internal
    public void setFUsePgsuSettings(final boolean field_45_fUsePgsuSettings) {
        this.field_45_fUsePgsuSettings = field_45_fUsePgsuSettings;
    }
    
    @Internal
    public boolean getFAdjustRight() {
        return this.field_46_fAdjustRight;
    }
    
    @Internal
    public void setFAdjustRight(final boolean field_46_fAdjustRight) {
        this.field_46_fAdjustRight = field_46_fAdjustRight;
    }
    
    @Internal
    public int getItap() {
        return this.field_47_itap;
    }
    
    @Internal
    public void setItap(final int field_47_itap) {
        this.field_47_itap = field_47_itap;
    }
    
    @Internal
    public boolean getFInnerTableCell() {
        return this.field_48_fInnerTableCell;
    }
    
    @Internal
    public void setFInnerTableCell(final boolean field_48_fInnerTableCell) {
        this.field_48_fInnerTableCell = field_48_fInnerTableCell;
    }
    
    @Internal
    public boolean getFOpenTch() {
        return this.field_49_fOpenTch;
    }
    
    @Internal
    public void setFOpenTch(final boolean field_49_fOpenTch) {
        this.field_49_fOpenTch = field_49_fOpenTch;
    }
    
    @Internal
    public boolean getFTtpEmbedded() {
        return this.field_50_fTtpEmbedded;
    }
    
    @Internal
    public void setFTtpEmbedded(final boolean field_50_fTtpEmbedded) {
        this.field_50_fTtpEmbedded = field_50_fTtpEmbedded;
    }
    
    @Internal
    public short getDxcRight() {
        return this.field_51_dxcRight;
    }
    
    @Internal
    public void setDxcRight(final short field_51_dxcRight) {
        this.field_51_dxcRight = field_51_dxcRight;
    }
    
    @Internal
    public short getDxcLeft() {
        return this.field_52_dxcLeft;
    }
    
    @Internal
    public void setDxcLeft(final short field_52_dxcLeft) {
        this.field_52_dxcLeft = field_52_dxcLeft;
    }
    
    @Internal
    public short getDxcLeft1() {
        return this.field_53_dxcLeft1;
    }
    
    @Internal
    public void setDxcLeft1(final short field_53_dxcLeft1) {
        this.field_53_dxcLeft1 = field_53_dxcLeft1;
    }
    
    @Internal
    public boolean getFDyaBeforeAuto() {
        return this.field_54_fDyaBeforeAuto;
    }
    
    @Internal
    public void setFDyaBeforeAuto(final boolean field_54_fDyaBeforeAuto) {
        this.field_54_fDyaBeforeAuto = field_54_fDyaBeforeAuto;
    }
    
    @Internal
    public boolean getFDyaAfterAuto() {
        return this.field_55_fDyaAfterAuto;
    }
    
    @Internal
    public void setFDyaAfterAuto(final boolean field_55_fDyaAfterAuto) {
        this.field_55_fDyaAfterAuto = field_55_fDyaAfterAuto;
    }
    
    @Internal
    public int getDxaRight() {
        return this.field_56_dxaRight;
    }
    
    @Internal
    public void setDxaRight(final int field_56_dxaRight) {
        this.field_56_dxaRight = field_56_dxaRight;
    }
    
    @Internal
    public int getDxaLeft() {
        return this.field_57_dxaLeft;
    }
    
    @Internal
    public void setDxaLeft(final int field_57_dxaLeft) {
        this.field_57_dxaLeft = field_57_dxaLeft;
    }
    
    @Internal
    public int getDxaLeft1() {
        return this.field_58_dxaLeft1;
    }
    
    @Internal
    public void setDxaLeft1(final int field_58_dxaLeft1) {
        this.field_58_dxaLeft1 = field_58_dxaLeft1;
    }
    
    @Internal
    public byte getJc() {
        return this.field_59_jc;
    }
    
    @Internal
    public void setJc(final byte field_59_jc) {
        this.field_59_jc = field_59_jc;
    }
    
    @Internal
    public BorderCode getBrcTop() {
        return this.field_60_brcTop;
    }
    
    @Internal
    public void setBrcTop(final BorderCode field_60_brcTop) {
        this.field_60_brcTop = field_60_brcTop;
    }
    
    @Internal
    public BorderCode getBrcLeft() {
        return this.field_61_brcLeft;
    }
    
    @Internal
    public void setBrcLeft(final BorderCode field_61_brcLeft) {
        this.field_61_brcLeft = field_61_brcLeft;
    }
    
    @Internal
    public BorderCode getBrcBottom() {
        return this.field_62_brcBottom;
    }
    
    @Internal
    public void setBrcBottom(final BorderCode field_62_brcBottom) {
        this.field_62_brcBottom = field_62_brcBottom;
    }
    
    @Internal
    public BorderCode getBrcRight() {
        return this.field_63_brcRight;
    }
    
    @Internal
    public void setBrcRight(final BorderCode field_63_brcRight) {
        this.field_63_brcRight = field_63_brcRight;
    }
    
    @Internal
    public BorderCode getBrcBetween() {
        return this.field_64_brcBetween;
    }
    
    @Internal
    public void setBrcBetween(final BorderCode field_64_brcBetween) {
        this.field_64_brcBetween = field_64_brcBetween;
    }
    
    @Internal
    public BorderCode getBrcBar() {
        return this.field_65_brcBar;
    }
    
    @Internal
    public void setBrcBar(final BorderCode field_65_brcBar) {
        this.field_65_brcBar = field_65_brcBar;
    }
    
    @Internal
    public ShadingDescriptor getShd() {
        return this.field_66_shd;
    }
    
    @Internal
    public void setShd(final ShadingDescriptor field_66_shd) {
        this.field_66_shd = field_66_shd;
    }
    
    @Internal
    public byte[] getAnld() {
        return this.field_67_anld;
    }
    
    @Internal
    public void setAnld(final byte[] field_67_anld) {
        this.field_67_anld = field_67_anld;
    }
    
    @Internal
    public byte[] getPhe() {
        return this.field_68_phe;
    }
    
    @Internal
    public void setPhe(final byte[] field_68_phe) {
        this.field_68_phe = field_68_phe;
    }
    
    @Internal
    public boolean getFPropRMark() {
        return this.field_69_fPropRMark;
    }
    
    @Internal
    public void setFPropRMark(final boolean field_69_fPropRMark) {
        this.field_69_fPropRMark = field_69_fPropRMark;
    }
    
    @Internal
    public int getIbstPropRMark() {
        return this.field_70_ibstPropRMark;
    }
    
    @Internal
    public void setIbstPropRMark(final int field_70_ibstPropRMark) {
        this.field_70_ibstPropRMark = field_70_ibstPropRMark;
    }
    
    @Internal
    public DateAndTime getDttmPropRMark() {
        return this.field_71_dttmPropRMark;
    }
    
    @Internal
    public void setDttmPropRMark(final DateAndTime field_71_dttmPropRMark) {
        this.field_71_dttmPropRMark = field_71_dttmPropRMark;
    }
    
    @Internal
    public int getItbdMac() {
        return this.field_72_itbdMac;
    }
    
    @Internal
    public void setItbdMac(final int field_72_itbdMac) {
        this.field_72_itbdMac = field_72_itbdMac;
    }
    
    @Internal
    public int[] getRgdxaTab() {
        return this.field_73_rgdxaTab;
    }
    
    @Internal
    public void setRgdxaTab(final int[] field_73_rgdxaTab) {
        this.field_73_rgdxaTab = field_73_rgdxaTab;
    }
    
    @Internal
    public TabDescriptor[] getRgtbd() {
        return this.field_74_rgtbd;
    }
    
    @Internal
    public void setRgtbd(final TabDescriptor[] field_74_rgtbd) {
        this.field_74_rgtbd = field_74_rgtbd;
    }
    
    @Internal
    public byte[] getNumrm() {
        return this.field_75_numrm;
    }
    
    @Internal
    public void setNumrm(final byte[] field_75_numrm) {
        this.field_75_numrm = field_75_numrm;
    }
    
    @Internal
    public byte[] getPtap() {
        return this.field_76_ptap;
    }
    
    @Internal
    public void setPtap(final byte[] field_76_ptap) {
        this.field_76_ptap = field_76_ptap;
    }
    
    @Internal
    public boolean getFNoAllowOverlap() {
        return this.field_77_fNoAllowOverlap;
    }
    
    @Internal
    public void setFNoAllowOverlap(final boolean field_77_fNoAllowOverlap) {
        this.field_77_fNoAllowOverlap = field_77_fNoAllowOverlap;
    }
    
    @Internal
    public long getIpgp() {
        return this.field_78_ipgp;
    }
    
    @Internal
    public void setIpgp(final long field_78_ipgp) {
        this.field_78_ipgp = field_78_ipgp;
    }
    
    @Internal
    public long getRsid() {
        return this.field_79_rsid;
    }
    
    @Internal
    public void setRsid(final long field_79_rsid) {
        this.field_79_rsid = field_79_rsid;
    }
    
    @Internal
    public void setFVertical(final boolean value) {
        this.field_40_fontAlign = (short)PAPAbstractType.fVertical.setBoolean(this.field_40_fontAlign, value);
    }
    
    @Internal
    public boolean isFVertical() {
        return PAPAbstractType.fVertical.isSet(this.field_40_fontAlign);
    }
    
    @Internal
    public void setFBackward(final boolean value) {
        this.field_40_fontAlign = (short)PAPAbstractType.fBackward.setBoolean(this.field_40_fontAlign, value);
    }
    
    @Internal
    public boolean isFBackward() {
        return PAPAbstractType.fBackward.isSet(this.field_40_fontAlign);
    }
    
    @Internal
    public void setFRotateFont(final boolean value) {
        this.field_40_fontAlign = (short)PAPAbstractType.fRotateFont.setBoolean(this.field_40_fontAlign, value);
    }
    
    @Internal
    public boolean isFRotateFont() {
        return PAPAbstractType.fRotateFont.isSet(this.field_40_fontAlign);
    }
    
    static {
        PAPAbstractType.fVertical = new BitField(1);
        PAPAbstractType.fBackward = new BitField(2);
        PAPAbstractType.fRotateFont = new BitField(4);
    }
}
