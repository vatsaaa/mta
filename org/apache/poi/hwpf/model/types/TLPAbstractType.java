// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hwpf.model.types;

import org.apache.poi.util.LittleEndian;
import org.apache.poi.util.BitField;
import org.apache.poi.util.Internal;
import org.apache.poi.hdf.model.hdftypes.HDFType;

@Internal
public abstract class TLPAbstractType implements HDFType
{
    protected short field_1_itl;
    protected byte field_2_tlp_flags;
    private static BitField fBorders;
    private static BitField fShading;
    private static BitField fFont;
    private static BitField fColor;
    private static BitField fBestFit;
    private static BitField fHdrRows;
    private static BitField fLastRow;
    
    protected void fillFields(final byte[] data, final int offset) {
        this.field_1_itl = LittleEndian.getShort(data, 0 + offset);
        this.field_2_tlp_flags = data[2 + offset];
    }
    
    public void serialize(final byte[] data, final int offset) {
        LittleEndian.putShort(data, 0 + offset, this.field_1_itl);
        data[2 + offset] = this.field_2_tlp_flags;
    }
    
    @Override
    public String toString() {
        final StringBuffer buffer = new StringBuffer();
        buffer.append("[TLP]\n");
        buffer.append("    .itl                  = ");
        buffer.append(" (").append(this.getItl()).append(" )\n");
        buffer.append("    .tlp_flags            = ");
        buffer.append(" (").append(this.getTlp_flags()).append(" )\n");
        buffer.append("         .fBorders                 = ").append(this.isFBorders()).append('\n');
        buffer.append("         .fShading                 = ").append(this.isFShading()).append('\n');
        buffer.append("         .fFont                    = ").append(this.isFFont()).append('\n');
        buffer.append("         .fColor                   = ").append(this.isFColor()).append('\n');
        buffer.append("         .fBestFit                 = ").append(this.isFBestFit()).append('\n');
        buffer.append("         .fHdrRows                 = ").append(this.isFHdrRows()).append('\n');
        buffer.append("         .fLastRow                 = ").append(this.isFLastRow()).append('\n');
        buffer.append("[/TLP]\n");
        return buffer.toString();
    }
    
    public int getSize() {
        return 7;
    }
    
    public short getItl() {
        return this.field_1_itl;
    }
    
    public void setItl(final short field_1_itl) {
        this.field_1_itl = field_1_itl;
    }
    
    public byte getTlp_flags() {
        return this.field_2_tlp_flags;
    }
    
    public void setTlp_flags(final byte field_2_tlp_flags) {
        this.field_2_tlp_flags = field_2_tlp_flags;
    }
    
    public void setFBorders(final boolean value) {
        this.field_2_tlp_flags = (byte)TLPAbstractType.fBorders.setBoolean(this.field_2_tlp_flags, value);
    }
    
    public boolean isFBorders() {
        return TLPAbstractType.fBorders.isSet(this.field_2_tlp_flags);
    }
    
    public void setFShading(final boolean value) {
        this.field_2_tlp_flags = (byte)TLPAbstractType.fShading.setBoolean(this.field_2_tlp_flags, value);
    }
    
    public boolean isFShading() {
        return TLPAbstractType.fShading.isSet(this.field_2_tlp_flags);
    }
    
    public void setFFont(final boolean value) {
        this.field_2_tlp_flags = (byte)TLPAbstractType.fFont.setBoolean(this.field_2_tlp_flags, value);
    }
    
    public boolean isFFont() {
        return TLPAbstractType.fFont.isSet(this.field_2_tlp_flags);
    }
    
    public void setFColor(final boolean value) {
        this.field_2_tlp_flags = (byte)TLPAbstractType.fColor.setBoolean(this.field_2_tlp_flags, value);
    }
    
    public boolean isFColor() {
        return TLPAbstractType.fColor.isSet(this.field_2_tlp_flags);
    }
    
    public void setFBestFit(final boolean value) {
        this.field_2_tlp_flags = (byte)TLPAbstractType.fBestFit.setBoolean(this.field_2_tlp_flags, value);
    }
    
    public boolean isFBestFit() {
        return TLPAbstractType.fBestFit.isSet(this.field_2_tlp_flags);
    }
    
    public void setFHdrRows(final boolean value) {
        this.field_2_tlp_flags = (byte)TLPAbstractType.fHdrRows.setBoolean(this.field_2_tlp_flags, value);
    }
    
    public boolean isFHdrRows() {
        return TLPAbstractType.fHdrRows.isSet(this.field_2_tlp_flags);
    }
    
    public void setFLastRow(final boolean value) {
        this.field_2_tlp_flags = (byte)TLPAbstractType.fLastRow.setBoolean(this.field_2_tlp_flags, value);
    }
    
    public boolean isFLastRow() {
        return TLPAbstractType.fLastRow.isSet(this.field_2_tlp_flags);
    }
    
    static {
        TLPAbstractType.fBorders = new BitField(1);
        TLPAbstractType.fShading = new BitField(2);
        TLPAbstractType.fFont = new BitField(4);
        TLPAbstractType.fColor = new BitField(8);
        TLPAbstractType.fBestFit = new BitField(16);
        TLPAbstractType.fHdrRows = new BitField(32);
        TLPAbstractType.fLastRow = new BitField(64);
    }
}
