// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hwpf.model.types;

import org.apache.poi.util.BitField;
import org.apache.poi.util.Internal;

@Internal
public abstract class TBDAbstractType
{
    protected byte field_1_value;
    private static BitField jc;
    private static BitField tlc;
    private static BitField reserved;
    
    protected TBDAbstractType() {
    }
    
    protected void fillFields(final byte[] data, final int offset) {
        this.field_1_value = data[0 + offset];
    }
    
    public void serialize(final byte[] data, final int offset) {
        data[0 + offset] = this.field_1_value;
    }
    
    public static int getSize() {
        return 1;
    }
    
    @Override
    public String toString() {
        final StringBuilder builder = new StringBuilder();
        builder.append("[TBD]\n");
        builder.append("    .value                = ");
        builder.append(" (").append(this.getValue()).append(" )\n");
        builder.append("         .jc                       = ").append(this.getJc()).append('\n');
        builder.append("         .tlc                      = ").append(this.getTlc()).append('\n');
        builder.append("         .reserved                 = ").append(this.getReserved()).append('\n');
        builder.append("[/TBD]\n");
        return builder.toString();
    }
    
    @Internal
    public byte getValue() {
        return this.field_1_value;
    }
    
    @Internal
    public void setValue(final byte field_1_value) {
        this.field_1_value = field_1_value;
    }
    
    @Internal
    public void setJc(final byte value) {
        this.field_1_value = (byte)TBDAbstractType.jc.setValue(this.field_1_value, value);
    }
    
    @Internal
    public byte getJc() {
        return (byte)TBDAbstractType.jc.getValue(this.field_1_value);
    }
    
    @Internal
    public void setTlc(final byte value) {
        this.field_1_value = (byte)TBDAbstractType.tlc.setValue(this.field_1_value, value);
    }
    
    @Internal
    public byte getTlc() {
        return (byte)TBDAbstractType.tlc.getValue(this.field_1_value);
    }
    
    @Internal
    public void setReserved(final byte value) {
        this.field_1_value = (byte)TBDAbstractType.reserved.setValue(this.field_1_value, value);
    }
    
    @Internal
    public byte getReserved() {
        return (byte)TBDAbstractType.reserved.getValue(this.field_1_value);
    }
    
    static {
        TBDAbstractType.jc = new BitField(7);
        TBDAbstractType.tlc = new BitField(56);
        TBDAbstractType.reserved = new BitField(192);
    }
}
