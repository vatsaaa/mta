// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hwpf.model.types;

import org.apache.poi.util.LittleEndian;
import org.apache.poi.hwpf.model.Grfhic;
import org.apache.poi.util.Internal;

@Internal
public abstract class LFOAbstractType
{
    protected int field_1_lsid;
    protected int field_2_reserved1;
    protected int field_3_reserved2;
    protected byte field_4_clfolvl;
    protected byte field_5_ibstFltAutoNum;
    protected Grfhic field_6_grfhic;
    protected byte field_7_reserved3;
    
    protected LFOAbstractType() {
        this.field_6_grfhic = new Grfhic();
    }
    
    protected void fillFields(final byte[] data, final int offset) {
        this.field_1_lsid = LittleEndian.getInt(data, 0 + offset);
        this.field_2_reserved1 = LittleEndian.getInt(data, 4 + offset);
        this.field_3_reserved2 = LittleEndian.getInt(data, 8 + offset);
        this.field_4_clfolvl = data[12 + offset];
        this.field_5_ibstFltAutoNum = data[13 + offset];
        this.field_6_grfhic = new Grfhic(data, 14 + offset);
        this.field_7_reserved3 = data[15 + offset];
    }
    
    public void serialize(final byte[] data, final int offset) {
        LittleEndian.putInt(data, 0 + offset, this.field_1_lsid);
        LittleEndian.putInt(data, 4 + offset, this.field_2_reserved1);
        LittleEndian.putInt(data, 8 + offset, this.field_3_reserved2);
        data[12 + offset] = this.field_4_clfolvl;
        data[13 + offset] = this.field_5_ibstFltAutoNum;
        this.field_6_grfhic.serialize(data, 14 + offset);
        data[15 + offset] = this.field_7_reserved3;
    }
    
    public byte[] serialize() {
        final byte[] result = new byte[getSize()];
        this.serialize(result, 0);
        return result;
    }
    
    public static int getSize() {
        return 16;
    }
    
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        final LFOAbstractType other = (LFOAbstractType)obj;
        if (this.field_1_lsid != other.field_1_lsid) {
            return false;
        }
        if (this.field_2_reserved1 != other.field_2_reserved1) {
            return false;
        }
        if (this.field_3_reserved2 != other.field_3_reserved2) {
            return false;
        }
        if (this.field_4_clfolvl != other.field_4_clfolvl) {
            return false;
        }
        if (this.field_5_ibstFltAutoNum != other.field_5_ibstFltAutoNum) {
            return false;
        }
        if (this.field_6_grfhic == null) {
            if (other.field_6_grfhic != null) {
                return false;
            }
        }
        else if (!this.field_6_grfhic.equals(other.field_6_grfhic)) {
            return false;
        }
        return this.field_7_reserved3 == other.field_7_reserved3;
    }
    
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = 31 * result + this.field_1_lsid;
        result = 31 * result + this.field_2_reserved1;
        result = 31 * result + this.field_3_reserved2;
        result = 31 * result + this.field_4_clfolvl;
        result = 31 * result + this.field_5_ibstFltAutoNum;
        result = 31 * result + this.field_6_grfhic.hashCode();
        result = 31 * result + this.field_7_reserved3;
        return result;
    }
    
    @Override
    public String toString() {
        final StringBuilder builder = new StringBuilder();
        builder.append("[LFO]\n");
        builder.append("    .lsid                 = ");
        builder.append(" (").append(this.getLsid()).append(" )\n");
        builder.append("    .reserved1            = ");
        builder.append(" (").append(this.getReserved1()).append(" )\n");
        builder.append("    .reserved2            = ");
        builder.append(" (").append(this.getReserved2()).append(" )\n");
        builder.append("    .clfolvl              = ");
        builder.append(" (").append(this.getClfolvl()).append(" )\n");
        builder.append("    .ibstFltAutoNum       = ");
        builder.append(" (").append(this.getIbstFltAutoNum()).append(" )\n");
        builder.append("    .grfhic               = ");
        builder.append(" (").append(this.getGrfhic()).append(" )\n");
        builder.append("    .reserved3            = ");
        builder.append(" (").append(this.getReserved3()).append(" )\n");
        builder.append("[/LFO]\n");
        return builder.toString();
    }
    
    @Internal
    public int getLsid() {
        return this.field_1_lsid;
    }
    
    @Internal
    public void setLsid(final int field_1_lsid) {
        this.field_1_lsid = field_1_lsid;
    }
    
    @Internal
    public int getReserved1() {
        return this.field_2_reserved1;
    }
    
    @Internal
    public void setReserved1(final int field_2_reserved1) {
        this.field_2_reserved1 = field_2_reserved1;
    }
    
    @Internal
    public int getReserved2() {
        return this.field_3_reserved2;
    }
    
    @Internal
    public void setReserved2(final int field_3_reserved2) {
        this.field_3_reserved2 = field_3_reserved2;
    }
    
    @Internal
    public byte getClfolvl() {
        return this.field_4_clfolvl;
    }
    
    @Internal
    public void setClfolvl(final byte field_4_clfolvl) {
        this.field_4_clfolvl = field_4_clfolvl;
    }
    
    @Internal
    public byte getIbstFltAutoNum() {
        return this.field_5_ibstFltAutoNum;
    }
    
    @Internal
    public void setIbstFltAutoNum(final byte field_5_ibstFltAutoNum) {
        this.field_5_ibstFltAutoNum = field_5_ibstFltAutoNum;
    }
    
    @Internal
    public Grfhic getGrfhic() {
        return this.field_6_grfhic;
    }
    
    @Internal
    public void setGrfhic(final Grfhic field_6_grfhic) {
        this.field_6_grfhic = field_6_grfhic;
    }
    
    @Internal
    public byte getReserved3() {
        return this.field_7_reserved3;
    }
    
    @Internal
    public void setReserved3(final byte field_7_reserved3) {
        this.field_7_reserved3 = field_7_reserved3;
    }
}
