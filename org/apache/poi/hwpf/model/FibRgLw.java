// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hwpf.model;

interface FibRgLw
{
    int getCbMac();
    
    int getSubdocumentTextStreamLength(final SubdocumentType p0);
    
    void setCbMac(final int p0);
    
    void setSubdocumentTextStreamLength(final SubdocumentType p0, final int p1);
}
