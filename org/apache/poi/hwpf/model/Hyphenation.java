// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hwpf.model;

import org.apache.poi.util.LittleEndian;
import org.apache.poi.util.Internal;
import org.apache.poi.hwpf.model.types.HRESIAbstractType;

@Internal
public final class Hyphenation extends HRESIAbstractType implements Cloneable
{
    public Hyphenation() {
    }
    
    public Hyphenation(final short hres) {
        final byte[] data = new byte[2];
        LittleEndian.putShort(data, hres);
        this.fillFields(data, 0);
    }
    
    public Hyphenation clone() {
        try {
            return (Hyphenation)super.clone();
        }
        catch (CloneNotSupportedException e) {
            throw new RuntimeException(e);
        }
    }
    
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        final Hyphenation other = (Hyphenation)obj;
        return this.field_1_hres == other.field_1_hres && this.field_2_chHres == other.field_2_chHres;
    }
    
    public short getValue() {
        final byte[] data = new byte[2];
        this.serialize(data, 0);
        return LittleEndian.getShort(data);
    }
    
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = 31 * result + this.field_1_hres;
        result = 31 * result + this.field_2_chHres;
        return result;
    }
    
    public boolean isEmpty() {
        return this.field_1_hres == 0 && this.field_2_chHres == 0;
    }
    
    @Override
    public String toString() {
        if (this.isEmpty()) {
            return "[HRESI] EMPTY";
        }
        return super.toString();
    }
}
