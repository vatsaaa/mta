// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hwpf.model;

import org.apache.poi.util.POILogFactory;
import java.io.IOException;
import org.apache.poi.hwpf.model.io.HWPFOutputStream;
import org.apache.poi.hwpf.model.io.HWPFFileSystem;
import java.util.Comparator;
import java.util.Collections;
import org.apache.poi.util.LittleEndian;
import java.util.List;
import java.util.ArrayList;
import org.apache.poi.util.POILogger;
import org.apache.poi.util.Internal;

@Internal
public class SectionTable
{
    private static final POILogger _logger;
    private static final int SED_SIZE = 12;
    protected ArrayList<SEPX> _sections;
    protected List<TextPiece> _text;
    private TextPieceTable tpt;
    
    public SectionTable() {
        this._sections = new ArrayList<SEPX>();
    }
    
    public SectionTable(final byte[] documentStream, final byte[] tableStream, final int offset, final int size, final int fcMin, final TextPieceTable tpt, final int mainLength) {
        this._sections = new ArrayList<SEPX>();
        final PlexOfCps sedPlex = new PlexOfCps(tableStream, offset, size, 12);
        this.tpt = tpt;
        this._text = tpt.getTextPieces();
        for (int length = sedPlex.length(), x = 0; x < length; ++x) {
            final GenericPropertyNode node = sedPlex.getProperty(x);
            final SectionDescriptor sed = new SectionDescriptor(node.getBytes(), 0);
            int fileOffset = sed.getFc();
            final int startAt = node.getStart();
            final int endAt = node.getEnd();
            if (fileOffset == -1) {
                this._sections.add(new SEPX(sed, startAt, endAt, new byte[0]));
            }
            else {
                final int sepxSize = LittleEndian.getShort(documentStream, fileOffset);
                final byte[] buf = new byte[sepxSize];
                fileOffset += 2;
                System.arraycopy(documentStream, fileOffset, buf, 0, buf.length);
                this._sections.add(new SEPX(sed, startAt, endAt, buf));
            }
        }
        final int mainEndsAt = mainLength;
        boolean matchAt = false;
        boolean matchHalf = false;
        for (int i = 0; i < this._sections.size(); ++i) {
            final SEPX s = this._sections.get(i);
            if (s.getEnd() == mainEndsAt) {
                matchAt = true;
            }
            else if (s.getEnd() == mainEndsAt || s.getEnd() == mainEndsAt - 1) {
                matchHalf = true;
            }
        }
        if (!matchAt && matchHalf) {
            SectionTable._logger.log(POILogger.WARN, "Your document seemed to be mostly unicode, but the section definition was in bytes! Trying anyway, but things may well go wrong!");
            for (int i = 0; i < this._sections.size(); ++i) {
                final SEPX s = this._sections.get(i);
                final GenericPropertyNode node2 = sedPlex.getProperty(i);
                final int startAt2 = node2.getStart();
                final int endAt2 = node2.getEnd();
                s.setStart(startAt2);
                s.setEnd(endAt2);
            }
        }
        Collections.sort(this._sections, PropertyNode.StartComparator.instance);
    }
    
    public void adjustForInsert(final int listIndex, final int length) {
        final int size = this._sections.size();
        SEPX sepx = this._sections.get(listIndex);
        sepx.setEnd(sepx.getEnd() + length);
        for (int x = listIndex + 1; x < size; ++x) {
            sepx = this._sections.get(x);
            sepx.setStart(sepx.getStart() + length);
            sepx.setEnd(sepx.getEnd() + length);
        }
    }
    
    public ArrayList<SEPX> getSections() {
        return this._sections;
    }
    
    @Deprecated
    public void writeTo(final HWPFFileSystem sys, final int fcMin) throws IOException {
        final HWPFOutputStream docStream = sys.getStream("WordDocument");
        final HWPFOutputStream tableStream = sys.getStream("1Table");
        this.writeTo(docStream, tableStream);
    }
    
    public void writeTo(final HWPFOutputStream wordDocumentStream, final HWPFOutputStream tableStream) throws IOException {
        int offset = wordDocumentStream.getOffset();
        final int len = this._sections.size();
        final PlexOfCps plex = new PlexOfCps(12);
        for (int x = 0; x < len; ++x) {
            final SEPX sepx = this._sections.get(x);
            final byte[] grpprl = sepx.getGrpprl();
            final byte[] shortBuf = new byte[2];
            LittleEndian.putShort(shortBuf, (short)grpprl.length);
            wordDocumentStream.write(shortBuf);
            wordDocumentStream.write(grpprl);
            final SectionDescriptor sed = sepx.getSectionDescriptor();
            sed.setFc(offset);
            final GenericPropertyNode property = new GenericPropertyNode(sepx.getStart(), sepx.getEnd(), sed.toByteArray());
            plex.addProperty(property);
            offset = wordDocumentStream.getOffset();
        }
        tableStream.write(plex.toByteArray());
    }
    
    static {
        _logger = POILogFactory.getLogger(SectionTable.class);
    }
}
