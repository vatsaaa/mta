// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hwpf.model;

import org.apache.poi.ddf.EscherContainerRecord;
import java.util.Iterator;
import java.util.List;
import org.apache.poi.ddf.EscherRecordFactory;
import org.apache.poi.ddf.DefaultEscherRecordFactory;
import org.apache.poi.ddf.EscherRecord;
import java.util.ArrayList;
import org.apache.poi.util.Internal;

@Internal
public final class EscherRecordHolder
{
    private final ArrayList<EscherRecord> escherRecords;
    
    public EscherRecordHolder() {
        this.escherRecords = new ArrayList<EscherRecord>();
    }
    
    public EscherRecordHolder(final byte[] data, final int offset, final int size) {
        this();
        this.fillEscherRecords(data, offset, size);
    }
    
    private void fillEscherRecords(final byte[] data, final int offset, final int size) {
        final EscherRecordFactory recordFactory = new DefaultEscherRecordFactory();
        int bytesRead;
        for (int pos = offset; pos < offset + size; pos += bytesRead + 1) {
            final EscherRecord r = recordFactory.createRecord(data, pos);
            this.escherRecords.add(r);
            bytesRead = r.fillFields(data, pos, recordFactory);
        }
    }
    
    public List<EscherRecord> getEscherRecords() {
        return this.escherRecords;
    }
    
    @Override
    public String toString() {
        final StringBuffer buffer = new StringBuffer();
        if (this.escherRecords.size() == 0) {
            buffer.append("No Escher Records Decoded").append("\n");
        }
        for (final EscherRecord r : this.escherRecords) {
            buffer.append(r.toString());
        }
        return buffer.toString();
    }
    
    public EscherContainerRecord getEscherContainer() {
        for (final Object er : this.escherRecords) {
            if (er instanceof EscherContainerRecord) {
                return (EscherContainerRecord)er;
            }
        }
        return null;
    }
    
    public EscherRecord findFirstWithId(final short id) {
        return findFirstWithId(id, this.getEscherRecords());
    }
    
    private static EscherRecord findFirstWithId(final short id, final List<EscherRecord> records) {
        for (final EscherRecord r : records) {
            if (r.getRecordId() == id) {
                return r;
            }
        }
        for (final EscherRecord r : records) {
            if (r.isContainerRecord()) {
                final EscherRecord found = findFirstWithId(id, r.getChildRecords());
                if (found != null) {
                    return found;
                }
                continue;
            }
        }
        return null;
    }
    
    public List<? extends EscherContainerRecord> getDgContainers() {
        final List<EscherContainerRecord> dgContainers = new ArrayList<EscherContainerRecord>(1);
        for (final EscherRecord escherRecord : this.getEscherRecords()) {
            if (escherRecord.getRecordId() == -4094) {
                dgContainers.add((EscherContainerRecord)escherRecord);
            }
        }
        return dgContainers;
    }
    
    public List<? extends EscherContainerRecord> getDggContainers() {
        final List<EscherContainerRecord> dggContainers = new ArrayList<EscherContainerRecord>(1);
        for (final EscherRecord escherRecord : this.getEscherRecords()) {
            if (escherRecord.getRecordId() == -4096) {
                dggContainers.add((EscherContainerRecord)escherRecord);
            }
        }
        return dggContainers;
    }
    
    public List<? extends EscherContainerRecord> getBStoreContainers() {
        final List<EscherContainerRecord> bStoreContainers = new ArrayList<EscherContainerRecord>(1);
        for (final EscherContainerRecord dggContainer : this.getDggContainers()) {
            for (final EscherRecord escherRecord : dggContainer.getChildRecords()) {
                if (escherRecord.getRecordId() == -4095) {
                    bStoreContainers.add((EscherContainerRecord)escherRecord);
                }
            }
        }
        return bStoreContainers;
    }
    
    public List<? extends EscherContainerRecord> getSpgrContainers() {
        final List<EscherContainerRecord> spgrContainers = new ArrayList<EscherContainerRecord>(1);
        for (final EscherContainerRecord dgContainer : this.getDgContainers()) {
            for (final EscherRecord escherRecord : dgContainer.getChildRecords()) {
                if (escherRecord.getRecordId() == -4093) {
                    spgrContainers.add((EscherContainerRecord)escherRecord);
                }
            }
        }
        return spgrContainers;
    }
    
    public List<? extends EscherContainerRecord> getSpContainers() {
        final List<EscherContainerRecord> spContainers = new ArrayList<EscherContainerRecord>(1);
        for (final EscherContainerRecord spgrContainer : this.getSpgrContainers()) {
            for (final EscherRecord escherRecord : spgrContainer.getChildRecords()) {
                if (escherRecord.getRecordId() == -4092) {
                    spContainers.add((EscherContainerRecord)escherRecord);
                }
            }
        }
        return spContainers;
    }
}
