// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hwpf.model.io;

import org.apache.poi.util.Internal;
import java.io.ByteArrayOutputStream;

@Internal
public final class HWPFOutputStream extends ByteArrayOutputStream
{
    int _offset;
    
    public int getOffset() {
        return this._offset;
    }
    
    @Override
    public synchronized void reset() {
        super.reset();
        this._offset = 0;
    }
    
    @Override
    public synchronized void write(final byte[] buf, final int off, final int len) {
        super.write(buf, off, len);
        this._offset += len;
    }
    
    @Override
    public synchronized void write(final int b) {
        super.write(b);
        ++this._offset;
    }
}
