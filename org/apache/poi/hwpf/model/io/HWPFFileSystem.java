// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hwpf.model.io;

import java.util.HashMap;
import java.util.Map;
import org.apache.poi.util.Internal;

@Internal
public final class HWPFFileSystem
{
    Map<String, HWPFOutputStream> _streams;
    
    public HWPFFileSystem() {
        (this._streams = new HashMap<String, HWPFOutputStream>()).put("WordDocument", new HWPFOutputStream());
        this._streams.put("1Table", new HWPFOutputStream());
        this._streams.put("Data", new HWPFOutputStream());
    }
    
    public HWPFOutputStream getStream(final String name) {
        return this._streams.get(name);
    }
}
