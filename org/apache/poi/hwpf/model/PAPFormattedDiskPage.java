// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hwpf.model;

import java.io.IOException;
import java.util.Arrays;
import org.apache.poi.hwpf.model.io.HWPFOutputStream;
import org.apache.poi.util.LittleEndian;
import java.util.Collections;
import java.util.Collection;
import java.util.List;
import java.util.ArrayList;
import org.apache.poi.util.Internal;

@Internal
public final class PAPFormattedDiskPage extends FormattedDiskPage
{
    private static final int BX_SIZE = 13;
    private static final int FC_SIZE = 4;
    private ArrayList<PAPX> _papxList;
    private ArrayList<PAPX> _overFlow;
    
    @Deprecated
    public PAPFormattedDiskPage(final byte[] dataStream) {
        this();
    }
    
    public PAPFormattedDiskPage() {
        this._papxList = new ArrayList<PAPX>();
    }
    
    @Deprecated
    public PAPFormattedDiskPage(final byte[] documentStream, final byte[] dataStream, final int offset, final int fcMin, final TextPieceTable tpt) {
        this(documentStream, dataStream, offset, tpt);
    }
    
    public PAPFormattedDiskPage(final byte[] documentStream, final byte[] dataStream, final int offset, final CharIndexTranslator translator) {
        super(documentStream, offset);
        this._papxList = new ArrayList<PAPX>();
        for (int x = 0; x < this._crun; ++x) {
            final int bytesStartAt = this.getStart(x);
            final int bytesEndAt = this.getEnd(x);
            for (final int[] range : translator.getCharIndexRanges(bytesStartAt, bytesEndAt)) {
                final PAPX papx = new PAPX(range[0], range[1], this.getGrpprl(x), this.getParagraphHeight(x), dataStream);
                this._papxList.add(papx);
            }
        }
        this._fkp = null;
    }
    
    public void fill(final List<PAPX> filler) {
        this._papxList.addAll(filler);
    }
    
    ArrayList<PAPX> getOverflow() {
        return this._overFlow;
    }
    
    public PAPX getPAPX(final int index) {
        return this._papxList.get(index);
    }
    
    public List<PAPX> getPAPXs() {
        return Collections.unmodifiableList((List<? extends PAPX>)this._papxList);
    }
    
    @Override
    protected byte[] getGrpprl(final int index) {
        int papxOffset = 2 * LittleEndian.getUnsignedByte(this._fkp, this._offset + ((this._crun + 1) * 4 + index * 13));
        int size = 2 * LittleEndian.getUnsignedByte(this._fkp, this._offset + papxOffset);
        if (size == 0) {
            size = 2 * LittleEndian.getUnsignedByte(this._fkp, this._offset + ++papxOffset);
        }
        else {
            --size;
        }
        final byte[] papx = new byte[size];
        System.arraycopy(this._fkp, this._offset + ++papxOffset, papx, 0, size);
        return papx;
    }
    
    protected byte[] toByteArray(final HWPFOutputStream dataStream, final CharIndexTranslator translator) throws IOException {
        final byte[] buf = new byte[512];
        final int size = this._papxList.size();
        int grpprlOffset = 0;
        int bxOffset = 0;
        int fcOffset = 0;
        byte[] lastGrpprl = new byte[0];
        int totalSize = 4;
        int index;
        for (index = 0; index < size; ++index) {
            final byte[] grpprl = this._papxList.get(index).getGrpprl();
            int grpprlLength = grpprl.length;
            if (grpprlLength > 488) {
                grpprlLength = 8;
            }
            int addition = 0;
            if (!Arrays.equals(grpprl, lastGrpprl)) {
                addition = 17 + grpprlLength + 1;
            }
            else {
                addition = 17;
            }
            totalSize += addition;
            if (totalSize > 511 + index % 2) {
                totalSize -= addition;
                break;
            }
            if (grpprlLength % 2 > 0) {
                ++totalSize;
            }
            else {
                totalSize += 2;
            }
            lastGrpprl = grpprl;
        }
        if (index != size) {
            (this._overFlow = new ArrayList<PAPX>()).addAll(this._papxList.subList(index, size));
        }
        buf[511] = (byte)index;
        bxOffset = 4 * index + 4;
        grpprlOffset = 511;
        PAPX papx = null;
        lastGrpprl = new byte[0];
        for (int x = 0; x < index; ++x) {
            papx = this._papxList.get(x);
            final byte[] phe = papx.getParagraphHeight().toByteArray();
            byte[] grpprl2 = papx.getGrpprl();
            if (grpprl2.length > 488) {
                final byte[] hugePapx = new byte[grpprl2.length - 2];
                System.arraycopy(grpprl2, 2, hugePapx, 0, grpprl2.length - 2);
                final int dataStreamOffset = dataStream.getOffset();
                dataStream.write(hugePapx);
                final int istd = LittleEndian.getUShort(grpprl2, 0);
                grpprl2 = new byte[8];
                LittleEndian.putUShort(grpprl2, 0, istd);
                LittleEndian.putUShort(grpprl2, 2, 26182);
                LittleEndian.putInt(grpprl2, 4, dataStreamOffset);
            }
            final boolean same = Arrays.equals(lastGrpprl, grpprl2);
            if (!same) {
                grpprlOffset -= grpprl2.length + (2 - grpprl2.length % 2);
                grpprlOffset -= grpprlOffset % 2;
            }
            LittleEndian.putInt(buf, fcOffset, translator.getByteIndex(papx.getStart()));
            buf[bxOffset] = (byte)(grpprlOffset / 2);
            System.arraycopy(phe, 0, buf, bxOffset + 1, phe.length);
            if (!same) {
                int copyOffset = grpprlOffset;
                if (grpprl2.length % 2 > 0) {
                    buf[copyOffset++] = (byte)((grpprl2.length + 1) / 2);
                }
                else {
                    buf[++copyOffset] = (byte)(grpprl2.length / 2);
                    ++copyOffset;
                }
                System.arraycopy(grpprl2, 0, buf, copyOffset, grpprl2.length);
                lastGrpprl = grpprl2;
            }
            bxOffset += 13;
            fcOffset += 4;
        }
        LittleEndian.putInt(buf, fcOffset, translator.getByteIndex(papx.getEnd()));
        return buf;
    }
    
    private ParagraphHeight getParagraphHeight(final int index) {
        final int pheOffset = this._offset + 1 + ((this._crun + 1) * 4 + index * 13);
        final ParagraphHeight phe = new ParagraphHeight(this._fkp, pheOffset);
        return phe;
    }
}
