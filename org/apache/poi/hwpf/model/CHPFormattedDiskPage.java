// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hwpf.model;

import org.apache.poi.util.LittleEndian;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import org.apache.poi.hwpf.sprm.SprmBuffer;
import java.util.ArrayList;
import org.apache.poi.util.Internal;

@Internal
public final class CHPFormattedDiskPage extends FormattedDiskPage
{
    private static final int FC_SIZE = 4;
    private ArrayList<CHPX> _chpxList;
    private ArrayList<CHPX> _overFlow;
    
    public CHPFormattedDiskPage() {
        this._chpxList = new ArrayList<CHPX>();
    }
    
    @Deprecated
    public CHPFormattedDiskPage(final byte[] documentStream, final int offset, final int fcMin, final TextPieceTable tpt) {
        this(documentStream, offset, tpt);
    }
    
    public CHPFormattedDiskPage(final byte[] documentStream, final int offset, final CharIndexTranslator translator) {
        super(documentStream, offset);
        this._chpxList = new ArrayList<CHPX>();
        for (int x = 0; x < this._crun; ++x) {
            final int bytesStartAt = this.getStart(x);
            final int bytesEndAt = this.getEnd(x);
            for (final int[] range : translator.getCharIndexRanges(bytesStartAt, bytesEndAt)) {
                final CHPX chpx = new CHPX(range[0], range[1], new SprmBuffer(this.getGrpprl(x), 0));
                this._chpxList.add(chpx);
            }
        }
    }
    
    public CHPX getCHPX(final int index) {
        return this._chpxList.get(index);
    }
    
    public List<CHPX> getCHPXs() {
        return Collections.unmodifiableList((List<? extends CHPX>)this._chpxList);
    }
    
    public void fill(final List<CHPX> filler) {
        this._chpxList.addAll(filler);
    }
    
    public ArrayList<CHPX> getOverflow() {
        return this._overFlow;
    }
    
    @Override
    protected byte[] getGrpprl(final int index) {
        int chpxOffset = 2 * LittleEndian.getUnsignedByte(this._fkp, this._offset + ((this._crun + 1) * 4 + index));
        if (chpxOffset == 0) {
            return new byte[0];
        }
        final int size = LittleEndian.getUnsignedByte(this._fkp, this._offset + chpxOffset);
        final byte[] chpx = new byte[size];
        System.arraycopy(this._fkp, this._offset + ++chpxOffset, chpx, 0, size);
        return chpx;
    }
    
    @Deprecated
    protected byte[] toByteArray(final CharIndexTranslator translator, final int fcMin) {
        return this.toByteArray(translator);
    }
    
    protected byte[] toByteArray(final CharIndexTranslator translator) {
        final byte[] buf = new byte[512];
        final int size = this._chpxList.size();
        int grpprlOffset = 511;
        int offsetOffset = 0;
        int fcOffset = 0;
        int totalSize = 6;
        int index;
        for (index = 0; index < size; ++index) {
            final int grpprlLength = this._chpxList.get(index).getGrpprl().length;
            totalSize += 6 + grpprlLength;
            if (totalSize > 511 + index % 2) {
                totalSize -= 6 + grpprlLength;
                break;
            }
            if ((1 + grpprlLength) % 2 > 0) {
                ++totalSize;
            }
        }
        if (index != size) {
            (this._overFlow = new ArrayList<CHPX>()).addAll(this._chpxList.subList(index, size));
        }
        buf[511] = (byte)index;
        offsetOffset = 4 * index + 4;
        CHPX chpx = null;
        for (int x = 0; x < index; ++x) {
            chpx = this._chpxList.get(x);
            final byte[] grpprl = chpx.getGrpprl();
            LittleEndian.putInt(buf, fcOffset, translator.getByteIndex(chpx.getStart()));
            grpprlOffset -= 1 + grpprl.length;
            grpprlOffset -= grpprlOffset % 2;
            buf[offsetOffset] = (byte)(grpprlOffset / 2);
            buf[grpprlOffset] = (byte)grpprl.length;
            System.arraycopy(grpprl, 0, buf, grpprlOffset + 1, grpprl.length);
            ++offsetOffset;
            fcOffset += 4;
        }
        LittleEndian.putInt(buf, fcOffset, translator.getByteIndex(chpx.getEnd()));
        return buf;
    }
}
