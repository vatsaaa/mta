// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hwpf.model;

import org.apache.poi.util.Internal;
import org.apache.poi.hwpf.model.types.FibBaseAbstractType;

@Internal
public class FibBase extends FibBaseAbstractType
{
    public FibBase() {
    }
    
    public FibBase(final byte[] std, final int offset) {
        this.fillFields(std, offset);
    }
    
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        final FibBase other = (FibBase)obj;
        return this.field_10_flags2 == other.field_10_flags2 && this.field_11_Chs == other.field_11_Chs && this.field_12_chsTables == other.field_12_chsTables && this.field_13_fcMin == other.field_13_fcMin && this.field_14_fcMac == other.field_14_fcMac && this.field_1_wIdent == other.field_1_wIdent && this.field_2_nFib == other.field_2_nFib && this.field_3_unused == other.field_3_unused && this.field_4_lid == other.field_4_lid && this.field_5_pnNext == other.field_5_pnNext && this.field_6_flags1 == other.field_6_flags1 && this.field_7_nFibBack == other.field_7_nFibBack && this.field_8_lKey == other.field_8_lKey && this.field_9_envr == other.field_9_envr;
    }
    
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = 31 * result + this.field_10_flags2;
        result = 31 * result + this.field_11_Chs;
        result = 31 * result + this.field_12_chsTables;
        result = 31 * result + this.field_13_fcMin;
        result = 31 * result + this.field_14_fcMac;
        result = 31 * result + this.field_1_wIdent;
        result = 31 * result + this.field_2_nFib;
        result = 31 * result + this.field_3_unused;
        result = 31 * result + this.field_4_lid;
        result = 31 * result + this.field_5_pnNext;
        result = 31 * result + this.field_6_flags1;
        result = 31 * result + this.field_7_nFibBack;
        result = 31 * result + this.field_8_lKey;
        result = 31 * result + this.field_9_envr;
        return result;
    }
}
