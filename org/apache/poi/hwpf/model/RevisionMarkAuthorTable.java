// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hwpf.model;

import org.apache.poi.hwpf.model.io.HWPFOutputStream;
import java.util.Collections;
import java.util.Arrays;
import java.util.List;
import java.io.IOException;
import org.apache.poi.util.Internal;

@Internal
public final class RevisionMarkAuthorTable
{
    private String[] entries;
    
    public RevisionMarkAuthorTable(final byte[] tableStream, final int offset, final int size) throws IOException {
        this.entries = SttbUtils.readSttbfRMark(tableStream, offset);
    }
    
    public List<String> getEntries() {
        return Collections.unmodifiableList((List<? extends String>)Arrays.asList((T[])this.entries));
    }
    
    public String getAuthor(final int index) {
        String auth = null;
        if (index >= 0 && index < this.entries.length) {
            auth = this.entries[index];
        }
        return auth;
    }
    
    public int getSize() {
        return this.entries.length;
    }
    
    public void writeTo(final HWPFOutputStream tableStream) throws IOException {
        SttbUtils.writeSttbfRMark(this.entries, tableStream);
    }
}
