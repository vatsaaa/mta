// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hwpf.model;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.HashMap;
import java.util.Map;
import org.apache.poi.util.POILogFactory;
import java.util.NoSuchElementException;
import java.io.OutputStream;
import java.io.IOException;
import java.util.Iterator;
import java.io.ByteArrayOutputStream;
import org.apache.poi.hwpf.model.io.HWPFOutputStream;
import org.apache.poi.util.LittleEndian;
import java.util.ArrayList;
import org.apache.poi.util.POILogger;
import org.apache.poi.util.Internal;

@Internal
public final class ListTables
{
    private static POILogger log;
    ListMap _listMap;
    ArrayList<ListFormatOverride> _overrideList;
    
    public ListTables() {
        this._listMap = new ListMap();
        this._overrideList = new ArrayList<ListFormatOverride>();
    }
    
    public ListTables(final byte[] tableStream, final int lstOffset, final int lfoOffset) {
        this._listMap = new ListMap();
        this._overrideList = new ArrayList<ListFormatOverride>();
        int offset = lstOffset;
        final int cLst = LittleEndian.getShort(tableStream, offset);
        offset += 2;
        int levelOffset = offset + cLst * LSTF.getSize();
        for (int x = 0; x < cLst; ++x) {
            final ListData lst = new ListData(tableStream, offset);
            this._listMap.put(Integer.valueOf(lst.getLsid()), lst);
            offset += LSTF.getSize();
            for (int num = lst.numLevels(), y = 0; y < num; ++y) {
                final ListLevel lvl = new ListLevel();
                levelOffset += lvl.read(tableStream, levelOffset);
                lst.setLevel(y, lvl);
            }
        }
        offset = lfoOffset;
        final long lfoMac = LittleEndian.getUInt(tableStream, offset);
        offset += 4;
        for (int x = 0; x < lfoMac; ++x) {
            final ListFormatOverride lfo = new ListFormatOverride(tableStream, offset);
            offset += LFO.getSize();
            this._overrideList.add(lfo);
        }
        for (int x = 0; x < lfoMac; ++x) {
            final ListFormatOverride lfo = this._overrideList.get(x);
            final LFOData lfoData = new LFOData(tableStream, offset, lfo.numOverrides());
            lfo.setLfoData(lfoData);
            offset += lfoData.getSizeInBytes();
        }
    }
    
    public int addList(final ListData lst, final ListFormatOverride override) {
        int lsid = lst.getLsid();
        while (this._listMap.get((Object)lsid) != null) {
            lsid = lst.resetListID();
            override.setLsid(lsid);
        }
        this._listMap.put(Integer.valueOf(lsid), lst);
        this._overrideList.add(override);
        return lsid;
    }
    
    public void writeListDataTo(final FileInformationBlock fib, final HWPFOutputStream tableStream) throws IOException {
        final int startOffset = tableStream.getOffset();
        fib.setFcPlcfLst(startOffset);
        final int listSize = this._listMap.size();
        final ByteArrayOutputStream levelBuf = new ByteArrayOutputStream();
        final byte[] shortHolder = new byte[2];
        LittleEndian.putShort(shortHolder, (short)listSize);
        tableStream.write(shortHolder);
        for (final Integer x : this._listMap.sortedKeys()) {
            final ListData lst = this._listMap.get((Object)x);
            tableStream.write(lst.toByteArray());
            final ListLevel[] lvls = lst.getLevels();
            for (int y = 0; y < lvls.length; ++y) {
                levelBuf.write(lvls[y].toByteArray());
            }
        }
        fib.setLcbPlcfLst(tableStream.getOffset() - startOffset);
        tableStream.write(levelBuf.toByteArray());
    }
    
    public void writeListOverridesTo(final HWPFOutputStream tableStream) throws IOException {
        LittleEndian.putUInt(this._overrideList.size(), tableStream);
        for (final ListFormatOverride lfo : this._overrideList) {
            tableStream.write(lfo.getLfo().serialize());
        }
        for (final ListFormatOverride lfo : this._overrideList) {
            lfo.getLfoData().writeTo(tableStream);
        }
    }
    
    public ListFormatOverride getOverride(final int lfoIndex) {
        return this._overrideList.get(lfoIndex - 1);
    }
    
    public int getOverrideCount() {
        return this._overrideList.size();
    }
    
    public int getOverrideIndexFromListID(final int lstid) {
        int returnVal = -1;
        for (int size = this._overrideList.size(), x = 0; x < size; ++x) {
            final ListFormatOverride next = this._overrideList.get(x);
            if (next.getLsid() == lstid) {
                returnVal = x + 1;
                break;
            }
        }
        if (returnVal == -1) {
            throw new NoSuchElementException("No list found with the specified ID");
        }
        return returnVal;
    }
    
    public ListLevel getLevel(final int listID, final int level) {
        final ListData lst = this._listMap.get((Object)listID);
        if (level < lst.numLevels()) {
            final ListLevel lvl = lst.getLevels()[level];
            return lvl;
        }
        ListTables.log.log(POILogger.WARN, "Requested level " + level + " which was greater than the maximum defined (" + lst.numLevels() + ")");
        return null;
    }
    
    public ListData getListData(final int listID) {
        return this._listMap.get((Object)listID);
    }
    
    @Override
    public boolean equals(final Object obj) {
        if (obj == null) {
            return false;
        }
        final ListTables tables = (ListTables)obj;
        if (this._listMap.size() == tables._listMap.size()) {
            for (final Integer key : this._listMap.keySet()) {
                final ListData lst1 = this._listMap.get((Object)key);
                final ListData lst2 = tables._listMap.get((Object)key);
                if (!lst1.equals(lst2)) {
                    return false;
                }
            }
            final int size = this._overrideList.size();
            if (size == tables._overrideList.size()) {
                for (int x = 0; x < size; ++x) {
                    if (!this._overrideList.get(x).equals(tables._overrideList.get(x))) {
                        return false;
                    }
                }
                return true;
            }
        }
        return false;
    }
    
    static {
        ListTables.log = POILogFactory.getLogger(ListTables.class);
    }
    
    private static class ListMap implements Map<Integer, ListData>
    {
        private ArrayList<Integer> keyList;
        private HashMap<Integer, ListData> parent;
        
        private ListMap() {
            this.keyList = new ArrayList<Integer>();
            this.parent = new HashMap<Integer, ListData>();
        }
        
        public void clear() {
            this.keyList.clear();
            this.parent.clear();
        }
        
        public boolean containsKey(final Object key) {
            return this.parent.containsKey(key);
        }
        
        public boolean containsValue(final Object value) {
            return this.parent.containsValue(value);
        }
        
        public ListData get(final Object key) {
            return this.parent.get(key);
        }
        
        public boolean isEmpty() {
            return this.parent.isEmpty();
        }
        
        public ListData put(final Integer key, final ListData value) {
            this.keyList.add(key);
            return this.parent.put(key, value);
        }
        
        public void putAll(final Map<? extends Integer, ? extends ListData> map) {
            for (final Entry<? extends Integer, ? extends ListData> entry : map.entrySet()) {
                this.put((Integer)entry.getKey(), (ListData)entry.getValue());
            }
        }
        
        public ListData remove(final Object key) {
            this.keyList.remove(key);
            return this.parent.remove(key);
        }
        
        public int size() {
            return this.parent.size();
        }
        
        public Set<Entry<Integer, ListData>> entrySet() {
            throw new IllegalStateException("Use sortedKeys() + get() instead");
        }
        
        public List<Integer> sortedKeys() {
            return Collections.unmodifiableList((List<? extends Integer>)this.keyList);
        }
        
        public Set<Integer> keySet() {
            throw new IllegalStateException("Use sortedKeys() instead");
        }
        
        public Collection<ListData> values() {
            final ArrayList<ListData> values = new ArrayList<ListData>();
            for (final Integer key : this.keyList) {
                values.add(this.parent.get(key));
            }
            return values;
        }
    }
}
