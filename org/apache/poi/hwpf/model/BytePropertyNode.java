// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hwpf.model;

@Deprecated
public abstract class BytePropertyNode<T extends BytePropertyNode<T>> extends PropertyNode<T>
{
    private final int startBytes;
    private final int endBytes;
    
    @Deprecated
    public BytePropertyNode(final int fcStart, final int fcEnd, final CharIndexTranslator translator, final Object buf) {
        super(translator.getCharIndex(fcStart), translator.getCharIndex(fcEnd, translator.getCharIndex(fcStart)), buf);
        if (fcStart > fcEnd) {
            throw new IllegalArgumentException("fcStart (" + fcStart + ") > fcEnd (" + fcEnd + ")");
        }
        this.startBytes = fcStart;
        this.endBytes = fcEnd;
    }
    
    public BytePropertyNode(final int charStart, final int charEnd, final Object buf) {
        super(charStart, charEnd, buf);
        if (charStart > charEnd) {
            throw new IllegalArgumentException("charStart (" + charStart + ") > charEnd (" + charEnd + ")");
        }
        this.startBytes = -1;
        this.endBytes = -1;
    }
    
    @Deprecated
    public int getStartBytes() {
        return this.startBytes;
    }
    
    @Deprecated
    public int getEndBytes() {
        return this.endBytes;
    }
}
