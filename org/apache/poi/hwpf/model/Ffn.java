// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hwpf.model;

import org.apache.poi.util.BitFieldFactory;
import java.util.Arrays;
import org.apache.poi.util.LittleEndian;
import org.apache.poi.util.BitField;
import org.apache.poi.util.Internal;

@Internal
public final class Ffn
{
    private int _cbFfnM1;
    private byte _info;
    private static BitField _prq;
    private static BitField _fTrueType;
    private static BitField _ff;
    private short _wWeight;
    private byte _chs;
    private byte _ixchSzAlt;
    private byte[] _panose;
    private byte[] _fontSig;
    private char[] _xszFfn;
    private int _xszFfnLength;
    
    public Ffn(final byte[] buf, int offset) {
        this._panose = new byte[10];
        this._fontSig = new byte[24];
        int offsetTmp = offset;
        this._cbFfnM1 = LittleEndian.getUnsignedByte(buf, offset);
        ++offset;
        this._info = buf[offset];
        ++offset;
        this._wWeight = LittleEndian.getShort(buf, offset);
        offset += 2;
        this._chs = buf[offset];
        ++offset;
        this._ixchSzAlt = buf[offset];
        ++offset;
        System.arraycopy(buf, offset, this._panose, 0, this._panose.length);
        offset += this._panose.length;
        System.arraycopy(buf, offset, this._fontSig, 0, this._fontSig.length);
        offset += this._fontSig.length;
        offsetTmp = offset - offsetTmp;
        this._xszFfnLength = (this.getSize() - offsetTmp) / 2;
        this._xszFfn = new char[this._xszFfnLength];
        for (int i = 0; i < this._xszFfnLength; ++i) {
            this._xszFfn[i] = (char)LittleEndian.getShort(buf, offset);
            offset += 2;
        }
    }
    
    public int get_cbFfnM1() {
        return this._cbFfnM1;
    }
    
    public short getWeight() {
        return this._wWeight;
    }
    
    public byte getChs() {
        return this._chs;
    }
    
    public byte[] getPanose() {
        return this._panose;
    }
    
    public byte[] getFontSig() {
        return this._fontSig;
    }
    
    public int getSize() {
        return this._cbFfnM1 + 1;
    }
    
    public String getMainFontName() {
        int index;
        for (index = 0; index < this._xszFfnLength && this._xszFfn[index] != '\0'; ++index) {}
        return new String(this._xszFfn, 0, index);
    }
    
    public String getAltFontName() {
        int index;
        for (index = this._ixchSzAlt; index < this._xszFfnLength && this._xszFfn[index] != '\0'; ++index) {}
        return new String(this._xszFfn, this._ixchSzAlt, index);
    }
    
    public void set_cbFfnM1(final int _cbFfnM1) {
        this._cbFfnM1 = _cbFfnM1;
    }
    
    public byte[] toByteArray() {
        int offset = 0;
        final byte[] buf = new byte[this.getSize()];
        buf[offset] = (byte)this._cbFfnM1;
        ++offset;
        buf[offset] = this._info;
        ++offset;
        LittleEndian.putShort(buf, offset, this._wWeight);
        offset += 2;
        buf[offset] = this._chs;
        ++offset;
        buf[offset] = this._ixchSzAlt;
        ++offset;
        System.arraycopy(this._panose, 0, buf, offset, this._panose.length);
        offset += this._panose.length;
        System.arraycopy(this._fontSig, 0, buf, offset, this._fontSig.length);
        offset += this._fontSig.length;
        for (int i = 0; i < this._xszFfn.length; ++i) {
            LittleEndian.putShort(buf, offset, (short)this._xszFfn[i]);
            offset += 2;
        }
        return buf;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean retVal = true;
        if (((Ffn)o).get_cbFfnM1() == this._cbFfnM1) {
            if (((Ffn)o)._info == this._info) {
                if (((Ffn)o)._wWeight == this._wWeight) {
                    if (((Ffn)o)._chs == this._chs) {
                        if (((Ffn)o)._ixchSzAlt == this._ixchSzAlt) {
                            if (Arrays.equals(((Ffn)o)._panose, this._panose)) {
                                if (Arrays.equals(((Ffn)o)._fontSig, this._fontSig)) {
                                    if (!Arrays.equals(((Ffn)o)._xszFfn, this._xszFfn)) {
                                        retVal = false;
                                    }
                                }
                                else {
                                    retVal = false;
                                }
                            }
                            else {
                                retVal = false;
                            }
                        }
                        else {
                            retVal = false;
                        }
                    }
                    else {
                        retVal = false;
                    }
                }
                else {
                    retVal = false;
                }
            }
            else {
                retVal = false;
            }
        }
        else {
            retVal = false;
        }
        return retVal;
    }
    
    static {
        Ffn._prq = BitFieldFactory.getInstance(3);
        Ffn._fTrueType = BitFieldFactory.getInstance(4);
        Ffn._ff = BitFieldFactory.getInstance(112);
    }
}
