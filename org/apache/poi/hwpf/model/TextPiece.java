// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hwpf.model;

import java.io.UnsupportedEncodingException;
import org.apache.poi.util.Internal;

@Internal
public class TextPiece extends PropertyNode<TextPiece>
{
    private boolean _usesUnicode;
    private PieceDescriptor _pd;
    
    @Deprecated
    public TextPiece(final int start, final int end, final byte[] text, final PieceDescriptor pd, final int cpStart) {
        this(start, end, text, pd);
    }
    
    public TextPiece(final int start, final int end, final byte[] text, final PieceDescriptor pd) {
        super(start, end, buildInitSB(text, pd));
        this._usesUnicode = pd.isUnicode();
        this._pd = pd;
        final int textLength = ((CharSequence)this._buf).length();
        if (end - start != textLength) {
            throw new IllegalStateException("Told we're for characters " + start + " -> " + end + ", but actually covers " + textLength + " characters!");
        }
        if (end < start) {
            throw new IllegalStateException("Told we're of negative size! start=" + start + " end=" + end);
        }
    }
    
    private static StringBuilder buildInitSB(final byte[] text, final PieceDescriptor pd) {
        String str;
        try {
            if (pd.isUnicode()) {
                str = new String(text, "UTF-16LE");
            }
            else {
                str = new String(text, "Cp1252");
            }
        }
        catch (UnsupportedEncodingException e) {
            throw new RuntimeException("Your Java is broken! It doesn't know about basic, required character encodings!");
        }
        return new StringBuilder(str);
    }
    
    public boolean isUnicode() {
        return this._usesUnicode;
    }
    
    public PieceDescriptor getPieceDescriptor() {
        return this._pd;
    }
    
    @Deprecated
    public StringBuffer getStringBuffer() {
        return new StringBuffer(this.getStringBuilder());
    }
    
    public StringBuilder getStringBuilder() {
        return (StringBuilder)this._buf;
    }
    
    public byte[] getRawBytes() {
        try {
            return ((CharSequence)this._buf).toString().getBytes(this._usesUnicode ? "UTF-16LE" : "Cp1252");
        }
        catch (UnsupportedEncodingException ignore) {
            throw new RuntimeException("Your Java is broken! It doesn't know about basic, required character encodings!");
        }
    }
    
    @Deprecated
    public String substring(final int start, final int end) {
        final StringBuilder buf = (StringBuilder)this._buf;
        if (start < 0) {
            throw new StringIndexOutOfBoundsException("Can't request a substring before 0 - asked for " + start);
        }
        if (end > buf.length()) {
            throw new StringIndexOutOfBoundsException("Index " + end + " out of range 0 -> " + buf.length());
        }
        if (end < start) {
            throw new StringIndexOutOfBoundsException("Asked for text from " + start + " to " + end + ", which has an end before the start!");
        }
        return buf.substring(start, end);
    }
    
    @Deprecated
    @Override
    public void adjustForDelete(final int start, final int length) {
        final int numChars = length;
        final int myStart = this.getStart();
        final int myEnd = this.getEnd();
        final int end = start + numChars;
        if (start <= myEnd && end >= myStart) {
            final int overlapStart = Math.max(myStart, start);
            final int overlapEnd = Math.min(myEnd, end);
            final int bufStart = overlapStart - myStart;
            final int bufEnd = overlapEnd - myStart;
            ((StringBuilder)this._buf).delete(bufStart, bufEnd);
        }
        super.adjustForDelete(start, length);
    }
    
    @Deprecated
    public int characterLength() {
        return this.getEnd() - this.getStart();
    }
    
    public int bytesLength() {
        return (this.getEnd() - this.getStart()) * (this._usesUnicode ? 2 : 1);
    }
    
    @Override
    public boolean equals(final Object o) {
        if (this.limitsAreEqual(o)) {
            final TextPiece tp = (TextPiece)o;
            return this.getStringBuilder().toString().equals(tp.getStringBuilder().toString()) && tp._usesUnicode == this._usesUnicode && this._pd.equals(tp._pd);
        }
        return false;
    }
    
    public int getCP() {
        return this.getStart();
    }
    
    @Override
    public String toString() {
        return "TextPiece from " + this.getStart() + " to " + this.getEnd() + " (" + this.getPieceDescriptor() + ")";
    }
}
