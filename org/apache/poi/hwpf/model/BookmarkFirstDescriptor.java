// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hwpf.model;

import org.apache.poi.util.Internal;
import org.apache.poi.hwpf.model.types.BKFAbstractType;

@Internal
public final class BookmarkFirstDescriptor extends BKFAbstractType implements Cloneable
{
    public BookmarkFirstDescriptor() {
    }
    
    public BookmarkFirstDescriptor(final byte[] data, final int offset) {
        this.fillFields(data, offset);
    }
    
    @Override
    protected BookmarkFirstDescriptor clone() {
        try {
            return (BookmarkFirstDescriptor)super.clone();
        }
        catch (CloneNotSupportedException e) {
            throw new RuntimeException(e);
        }
    }
    
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        final BookmarkFirstDescriptor other = (BookmarkFirstDescriptor)obj;
        return this.field_1_ibkl == other.field_1_ibkl && this.field_2_bkf_flags == other.field_2_bkf_flags;
    }
    
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = 31 * result + this.field_1_ibkl;
        result = 31 * result + this.field_2_bkf_flags;
        return result;
    }
    
    public boolean isEmpty() {
        return this.field_1_ibkl == 0 && this.field_2_bkf_flags == 0;
    }
    
    @Override
    public String toString() {
        if (this.isEmpty()) {
            return "[BKF] EMPTY";
        }
        return super.toString();
    }
}
