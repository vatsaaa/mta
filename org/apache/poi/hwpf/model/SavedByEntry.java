// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hwpf.model;

import org.apache.poi.util.Internal;

@Internal
public final class SavedByEntry
{
    private String userName;
    private String saveLocation;
    
    public SavedByEntry(final String userName, final String saveLocation) {
        this.userName = userName;
        this.saveLocation = saveLocation;
    }
    
    public String getUserName() {
        return this.userName;
    }
    
    public String getSaveLocation() {
        return this.saveLocation;
    }
    
    @Override
    public boolean equals(final Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof SavedByEntry)) {
            return false;
        }
        final SavedByEntry that = (SavedByEntry)other;
        return that.userName.equals(this.userName) && that.saveLocation.equals(this.saveLocation);
    }
    
    @Override
    public int hashCode() {
        int hash = 29;
        hash = hash * 13 + this.userName.hashCode();
        hash = hash * 13 + this.saveLocation.hashCode();
        return hash;
    }
    
    @Override
    public String toString() {
        return "SavedByEntry[userName=" + this.getUserName() + ",saveLocation=" + this.getSaveLocation() + "]";
    }
}
