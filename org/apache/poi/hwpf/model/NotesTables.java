// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hwpf.model;

import java.io.IOException;
import org.apache.poi.hwpf.model.io.HWPFOutputStream;
import org.apache.poi.hwpf.model.types.FRDAbstractType;
import org.apache.poi.util.Internal;

@Internal
public class NotesTables
{
    private PlexOfCps descriptors;
    private final NoteType noteType;
    private PlexOfCps textPositions;
    
    public NotesTables(final NoteType noteType) {
        this.descriptors = new PlexOfCps(FRDAbstractType.getSize());
        this.textPositions = new PlexOfCps(0);
        this.noteType = noteType;
        this.textPositions.addProperty(new GenericPropertyNode(0, 1, new byte[0]));
    }
    
    public NotesTables(final NoteType noteType, final byte[] tableStream, final FileInformationBlock fib) {
        this.descriptors = new PlexOfCps(FRDAbstractType.getSize());
        this.textPositions = new PlexOfCps(0);
        this.noteType = noteType;
        this.read(tableStream, fib);
    }
    
    public GenericPropertyNode getDescriptor(final int index) {
        return this.descriptors.getProperty(index);
    }
    
    public int getDescriptorsCount() {
        return this.descriptors.length();
    }
    
    public GenericPropertyNode getTextPosition(final int index) {
        return this.textPositions.getProperty(index);
    }
    
    private void read(final byte[] tableStream, final FileInformationBlock fib) {
        final int referencesStart = fib.getNotesDescriptorsOffset(this.noteType);
        final int referencesLength = fib.getNotesDescriptorsSize(this.noteType);
        if (referencesStart != 0 && referencesLength != 0) {
            this.descriptors = new PlexOfCps(tableStream, referencesStart, referencesLength, FRDAbstractType.getSize());
        }
        final int textPositionsStart = fib.getNotesTextPositionsOffset(this.noteType);
        final int textPositionsLength = fib.getNotesTextPositionsSize(this.noteType);
        if (textPositionsStart != 0 && textPositionsLength != 0) {
            this.textPositions = new PlexOfCps(tableStream, textPositionsStart, textPositionsLength, 0);
        }
    }
    
    public void writeRef(final FileInformationBlock fib, final HWPFOutputStream tableStream) throws IOException {
        if (this.descriptors == null || this.descriptors.length() == 0) {
            fib.setNotesDescriptorsOffset(this.noteType, tableStream.getOffset());
            fib.setNotesDescriptorsSize(this.noteType, 0);
            return;
        }
        final int start = tableStream.getOffset();
        tableStream.write(this.descriptors.toByteArray());
        final int end = tableStream.getOffset();
        fib.setNotesDescriptorsOffset(this.noteType, start);
        fib.setNotesDescriptorsSize(this.noteType, end - start);
    }
    
    public void writeTxt(final FileInformationBlock fib, final HWPFOutputStream tableStream) throws IOException {
        if (this.textPositions == null || this.textPositions.length() == 0) {
            fib.setNotesTextPositionsOffset(this.noteType, tableStream.getOffset());
            fib.setNotesTextPositionsSize(this.noteType, 0);
            return;
        }
        final int start = tableStream.getOffset();
        tableStream.write(this.textPositions.toByteArray());
        final int end = tableStream.getOffset();
        fib.setNotesTextPositionsOffset(this.noteType, start);
        fib.setNotesTextPositionsSize(this.noteType, end - start);
    }
}
