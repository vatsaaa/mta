// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hwpf.model;

import java.util.ArrayList;
import org.apache.poi.hwpf.usermodel.Shape;
import java.util.List;
import org.apache.poi.util.Internal;

@Internal
@Deprecated
public final class ShapesTable
{
    private List<Shape> _shapes;
    private List<Shape> _shapesVisibili;
    
    public ShapesTable(final byte[] tblStream, final FileInformationBlock fib) {
        final PlexOfCps binTable = new PlexOfCps(tblStream, fib.getFcPlcspaMom(), fib.getLcbPlcspaMom(), 26);
        this._shapes = new ArrayList<Shape>();
        this._shapesVisibili = new ArrayList<Shape>();
        for (int i = 0; i < binTable.length(); ++i) {
            final GenericPropertyNode nodo = binTable.getProperty(i);
            final Shape sh = new Shape(nodo);
            this._shapes.add(sh);
            if (sh.isWithinDocument()) {
                this._shapesVisibili.add(sh);
            }
        }
    }
    
    public List<Shape> getAllShapes() {
        return this._shapes;
    }
    
    public List<Shape> getVisibleShapes() {
        return this._shapesVisibili;
    }
}
