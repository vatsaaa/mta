// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hwpf.model;

import org.apache.poi.util.POILogFactory;
import java.io.IOException;
import org.apache.poi.hwpf.model.io.HWPFOutputStream;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.HashSet;
import org.apache.poi.util.LittleEndian;
import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.hwpf.model.types.FibBaseAbstractType;
import org.apache.poi.util.POILogger;
import org.apache.poi.util.Internal;

@Internal
public final class FileInformationBlock implements Cloneable
{
    public static final POILogger logger;
    private FibBase _fibBase;
    private int _csw;
    private FibRgW97 _fibRgW;
    private int _cslw;
    private FibRgLw _fibRgLw;
    private int _cbRgFcLcb;
    private FIBFieldHandler _fieldHandler;
    private int _cswNew;
    private int _nFibNew;
    private byte[] _fibRgCswNew;
    
    public FileInformationBlock(final byte[] mainDocument) {
        int offset = 0;
        this._fibBase = new FibBase(mainDocument, offset);
        offset = FibBaseAbstractType.getSize();
        assert offset == 32;
        if (this._fibBase.isFEncrypted()) {
            throw new EncryptedDocumentException("Cannot process encrypted word file");
        }
        this._csw = LittleEndian.getUShort(mainDocument, offset);
        offset += 2;
        assert offset == 34;
        this._fibRgW = new FibRgW97(mainDocument, offset);
        offset += FibRgW97.getSize();
        assert offset == 62;
        this._cslw = LittleEndian.getUShort(mainDocument, offset);
        offset += 2;
        assert offset == 64;
        if (this._fibBase.getNFib() < 105) {
            this._fibRgLw = new FibRgLw95(mainDocument, offset);
            offset += FibRgLw97.getSize();
            this._cbRgFcLcb = 74;
            offset += this._cbRgFcLcb * 4 * 2;
            this._cswNew = LittleEndian.getUShort(mainDocument, offset);
            offset += 2;
            this._cswNew = 0;
            this._nFibNew = -1;
            this._fibRgCswNew = new byte[0];
            return;
        }
        this._fibRgLw = new FibRgLw97(mainDocument, offset);
        offset += FibRgLw97.getSize();
        assert offset == 152;
        this._cbRgFcLcb = LittleEndian.getUShort(mainDocument, offset);
        offset += 2;
        assert offset == 154;
        offset += this._cbRgFcLcb * 4 * 2;
        this._cswNew = LittleEndian.getUShort(mainDocument, offset);
        offset += 2;
        if (this._cswNew != 0) {
            this._nFibNew = LittleEndian.getUShort(mainDocument, offset);
            offset += 2;
            final int fibRgCswNewLength = (this._cswNew - 1) * 2;
            this._fibRgCswNew = new byte[fibRgCswNewLength];
            LittleEndian.getByteArray(mainDocument, offset, fibRgCswNewLength);
            offset += fibRgCswNewLength;
        }
        else {
            this._nFibNew = -1;
            this._fibRgCswNew = new byte[0];
        }
        this.assertCbRgFcLcb();
        this.assertCswNew();
    }
    
    private void assertCbRgFcLcb() {
        switch (this.getNFib()) {
            case 193: {
                assertCbRgFcLcb("0x00C1", 93, "0x005D", this._cbRgFcLcb);
                break;
            }
            case 217: {
                assertCbRgFcLcb("0x00D9", 108, "0x006C", this._cbRgFcLcb);
                break;
            }
            case 257: {
                assertCbRgFcLcb("0x0101", 136, "0x0088", this._cbRgFcLcb);
                break;
            }
            case 268: {
                assertCbRgFcLcb("0x010C", 164, "0x00A4", this._cbRgFcLcb);
                break;
            }
            case 274: {
                assertCbRgFcLcb("0x0112", 183, "0x00B7", this._cbRgFcLcb);
                break;
            }
        }
    }
    
    private static void assertCbRgFcLcb(final String strNFib, final int expectedCbRgFcLcb, final String strCbRgFcLcb, final int cbRgFcLcb) {
        if (cbRgFcLcb == expectedCbRgFcLcb) {
            return;
        }
        FileInformationBlock.logger.log(POILogger.WARN, "Since FIB.nFib == ", strNFib, " value of FIB.cbRgFcLcb MUST be ", strCbRgFcLcb + ", not 0x", Integer.toHexString(cbRgFcLcb));
    }
    
    private void assertCswNew() {
        switch (this.getNFib()) {
            case 193: {
                assertCswNew("0x00C1", 0, "0x0000", this._cswNew);
                break;
            }
            case 217: {
                assertCswNew("0x00D9", 2, "0x0002", this._cswNew);
                break;
            }
            case 257: {
                assertCswNew("0x0101", 2, "0x0002", this._cswNew);
                break;
            }
            case 268: {
                assertCswNew("0x010C", 2, "0x0002", this._cswNew);
                break;
            }
            case 274: {
                assertCswNew("0x0112", 5, "0x0005", this._cswNew);
                break;
            }
        }
    }
    
    private static void assertCswNew(final String strNFib, final int expectedCswNew, final String strExpectedCswNew, final int cswNew) {
        if (cswNew == expectedCswNew) {
            return;
        }
        FileInformationBlock.logger.log(POILogger.WARN, "Since FIB.nFib == ", strNFib, " value of FIB.cswNew MUST be ", strExpectedCswNew + ", not 0x", Integer.toHexString(cswNew));
    }
    
    public void fillVariableFields(final byte[] mainDocument, final byte[] tableStream) {
        final HashSet<Integer> knownFieldSet = new HashSet<Integer>();
        knownFieldSet.add(1);
        knownFieldSet.add(33);
        knownFieldSet.add(31);
        knownFieldSet.add(12);
        knownFieldSet.add(13);
        knownFieldSet.add(6);
        knownFieldSet.add(73);
        knownFieldSet.add(74);
        for (final FieldsDocumentPart part : FieldsDocumentPart.values()) {
            knownFieldSet.add(part.getFibFieldsField());
        }
        knownFieldSet.add(22);
        knownFieldSet.add(23);
        knownFieldSet.add(21);
        for (final NoteType noteType : NoteType.values()) {
            knownFieldSet.add(noteType.getFibDescriptorsFieldIndex());
            knownFieldSet.add(noteType.getFibTextPositionsFieldIndex());
        }
        knownFieldSet.add(15);
        knownFieldSet.add(51);
        knownFieldSet.add(71);
        knownFieldSet.add(87);
        this._fieldHandler = new FIBFieldHandler(mainDocument, 154, this._cbRgFcLcb, tableStream, knownFieldSet, true);
    }
    
    @Override
    public String toString() {
        final StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(this._fibBase);
        stringBuilder.append("[FIB2]\n");
        stringBuilder.append("\tSubdocuments info:\n");
        for (final SubdocumentType type : SubdocumentType.values()) {
            stringBuilder.append("\t\t");
            stringBuilder.append(type);
            stringBuilder.append(" has length of ");
            stringBuilder.append(this.getSubdocumentTextStreamLength(type));
            stringBuilder.append(" char(s)\n");
        }
        stringBuilder.append("\tFields PLCF info:\n");
        for (final FieldsDocumentPart part : FieldsDocumentPart.values()) {
            stringBuilder.append("\t\t");
            stringBuilder.append(part);
            stringBuilder.append(": PLCF starts at ");
            stringBuilder.append(this.getFieldsPlcfOffset(part));
            stringBuilder.append(" and have length of ");
            stringBuilder.append(this.getFieldsPlcfLength(part));
            stringBuilder.append("\n");
        }
        stringBuilder.append("\tNotes PLCF info:\n");
        for (final NoteType noteType : NoteType.values()) {
            stringBuilder.append("\t\t");
            stringBuilder.append(noteType);
            stringBuilder.append(": descriptions starts ");
            stringBuilder.append(this.getNotesDescriptorsOffset(noteType));
            stringBuilder.append(" and have length of ");
            stringBuilder.append(this.getNotesDescriptorsSize(noteType));
            stringBuilder.append(" bytes\n");
            stringBuilder.append("\t\t");
            stringBuilder.append(noteType);
            stringBuilder.append(": text positions starts ");
            stringBuilder.append(this.getNotesTextPositionsOffset(noteType));
            stringBuilder.append(" and have length of ");
            stringBuilder.append(this.getNotesTextPositionsSize(noteType));
            stringBuilder.append(" bytes\n");
        }
        stringBuilder.append(this._fieldHandler);
        try {
            stringBuilder.append("\tJava reflection info:\n");
            for (final Method method : FileInformationBlock.class.getMethods()) {
                if (method.getName().startsWith("get") && Modifier.isPublic(method.getModifiers()) && !Modifier.isStatic(method.getModifiers())) {
                    if (method.getParameterTypes().length <= 0) {
                        stringBuilder.append("\t\t");
                        stringBuilder.append(method.getName());
                        stringBuilder.append(" => ");
                        stringBuilder.append(method.invoke(this, new Object[0]));
                        stringBuilder.append("\n");
                    }
                }
            }
        }
        catch (Exception exc) {
            stringBuilder.append("(exc: " + exc.getMessage() + ")");
        }
        stringBuilder.append("[/FIB2]\n");
        return stringBuilder.toString();
    }
    
    public int getNFib() {
        if (this._cswNew == 0) {
            return this._fibBase.getNFib();
        }
        return this._nFibNew;
    }
    
    public int getFcDop() {
        return this._fieldHandler.getFieldOffset(31);
    }
    
    public void setFcDop(final int fcDop) {
        this._fieldHandler.setFieldOffset(31, fcDop);
    }
    
    public int getLcbDop() {
        return this._fieldHandler.getFieldSize(31);
    }
    
    public void setLcbDop(final int lcbDop) {
        this._fieldHandler.setFieldSize(31, lcbDop);
    }
    
    public int getFcStshf() {
        return this._fieldHandler.getFieldOffset(1);
    }
    
    public int getLcbStshf() {
        return this._fieldHandler.getFieldSize(1);
    }
    
    public void setFcStshf(final int fcStshf) {
        this._fieldHandler.setFieldOffset(1, fcStshf);
    }
    
    public void setLcbStshf(final int lcbStshf) {
        this._fieldHandler.setFieldSize(1, lcbStshf);
    }
    
    public int getFcClx() {
        return this._fieldHandler.getFieldOffset(33);
    }
    
    public int getLcbClx() {
        return this._fieldHandler.getFieldSize(33);
    }
    
    public void setFcClx(final int fcClx) {
        this._fieldHandler.setFieldOffset(33, fcClx);
    }
    
    public void setLcbClx(final int lcbClx) {
        this._fieldHandler.setFieldSize(33, lcbClx);
    }
    
    public int getFcPlcfbteChpx() {
        return this._fieldHandler.getFieldOffset(12);
    }
    
    public int getLcbPlcfbteChpx() {
        return this._fieldHandler.getFieldSize(12);
    }
    
    public void setFcPlcfbteChpx(final int fcPlcfBteChpx) {
        this._fieldHandler.setFieldOffset(12, fcPlcfBteChpx);
    }
    
    public void setLcbPlcfbteChpx(final int lcbPlcfBteChpx) {
        this._fieldHandler.setFieldSize(12, lcbPlcfBteChpx);
    }
    
    public int getFcPlcfbtePapx() {
        return this._fieldHandler.getFieldOffset(13);
    }
    
    public int getLcbPlcfbtePapx() {
        return this._fieldHandler.getFieldSize(13);
    }
    
    public void setFcPlcfbtePapx(final int fcPlcfBtePapx) {
        this._fieldHandler.setFieldOffset(13, fcPlcfBtePapx);
    }
    
    public void setLcbPlcfbtePapx(final int lcbPlcfBtePapx) {
        this._fieldHandler.setFieldSize(13, lcbPlcfBtePapx);
    }
    
    public int getFcPlcfsed() {
        return this._fieldHandler.getFieldOffset(6);
    }
    
    public int getLcbPlcfsed() {
        return this._fieldHandler.getFieldSize(6);
    }
    
    public void setFcPlcfsed(final int fcPlcfSed) {
        this._fieldHandler.setFieldOffset(6, fcPlcfSed);
    }
    
    public void setLcbPlcfsed(final int lcbPlcfSed) {
        this._fieldHandler.setFieldSize(6, lcbPlcfSed);
    }
    
    public int getFcPlcfLst() {
        return this._fieldHandler.getFieldOffset(73);
    }
    
    public int getLcbPlcfLst() {
        return this._fieldHandler.getFieldSize(73);
    }
    
    public void setFcPlcfLst(final int fcPlcfLst) {
        this._fieldHandler.setFieldOffset(73, fcPlcfLst);
    }
    
    public void setLcbPlcfLst(final int lcbPlcfLst) {
        this._fieldHandler.setFieldSize(73, lcbPlcfLst);
    }
    
    public int getFcPlfLfo() {
        return this._fieldHandler.getFieldOffset(74);
    }
    
    public int getLcbPlfLfo() {
        return this._fieldHandler.getFieldSize(74);
    }
    
    public int getFcSttbfbkmk() {
        return this._fieldHandler.getFieldOffset(21);
    }
    
    public void setFcSttbfbkmk(final int offset) {
        this._fieldHandler.setFieldOffset(21, offset);
    }
    
    public int getLcbSttbfbkmk() {
        return this._fieldHandler.getFieldSize(21);
    }
    
    public void setLcbSttbfbkmk(final int length) {
        this._fieldHandler.setFieldSize(21, length);
    }
    
    public int getFcPlcfbkf() {
        return this._fieldHandler.getFieldOffset(22);
    }
    
    public void setFcPlcfbkf(final int offset) {
        this._fieldHandler.setFieldOffset(22, offset);
    }
    
    public int getLcbPlcfbkf() {
        return this._fieldHandler.getFieldSize(22);
    }
    
    public void setLcbPlcfbkf(final int length) {
        this._fieldHandler.setFieldSize(22, length);
    }
    
    public int getFcPlcfbkl() {
        return this._fieldHandler.getFieldOffset(23);
    }
    
    public void setFcPlcfbkl(final int offset) {
        this._fieldHandler.setFieldOffset(23, offset);
    }
    
    public int getLcbPlcfbkl() {
        return this._fieldHandler.getFieldSize(23);
    }
    
    public void setLcbPlcfbkl(final int length) {
        this._fieldHandler.setFieldSize(23, length);
    }
    
    public void setFcPlfLfo(final int fcPlfLfo) {
        this._fieldHandler.setFieldOffset(74, fcPlfLfo);
    }
    
    public void setLcbPlfLfo(final int lcbPlfLfo) {
        this._fieldHandler.setFieldSize(74, lcbPlfLfo);
    }
    
    public int getFcSttbfffn() {
        return this._fieldHandler.getFieldOffset(15);
    }
    
    public int getLcbSttbfffn() {
        return this._fieldHandler.getFieldSize(15);
    }
    
    public void setFcSttbfffn(final int fcSttbFffn) {
        this._fieldHandler.setFieldOffset(15, fcSttbFffn);
    }
    
    public void setLcbSttbfffn(final int lcbSttbFffn) {
        this._fieldHandler.setFieldSize(15, lcbSttbFffn);
    }
    
    public int getFcSttbfRMark() {
        return this._fieldHandler.getFieldOffset(51);
    }
    
    public int getLcbSttbfRMark() {
        return this._fieldHandler.getFieldSize(51);
    }
    
    public void setFcSttbfRMark(final int fcSttbfRMark) {
        this._fieldHandler.setFieldOffset(51, fcSttbfRMark);
    }
    
    public void setLcbSttbfRMark(final int lcbSttbfRMark) {
        this._fieldHandler.setFieldSize(51, lcbSttbfRMark);
    }
    
    public int getPlcfHddOffset() {
        return this._fieldHandler.getFieldOffset(11);
    }
    
    public int getPlcfHddSize() {
        return this._fieldHandler.getFieldSize(11);
    }
    
    public void setPlcfHddOffset(final int fcPlcfHdd) {
        this._fieldHandler.setFieldOffset(11, fcPlcfHdd);
    }
    
    public void setPlcfHddSize(final int lcbPlcfHdd) {
        this._fieldHandler.setFieldSize(11, lcbPlcfHdd);
    }
    
    public int getFcSttbSavedBy() {
        return this._fieldHandler.getFieldOffset(71);
    }
    
    public int getLcbSttbSavedBy() {
        return this._fieldHandler.getFieldSize(71);
    }
    
    public void setFcSttbSavedBy(final int fcSttbSavedBy) {
        this._fieldHandler.setFieldOffset(71, fcSttbSavedBy);
    }
    
    public void setLcbSttbSavedBy(final int fcSttbSavedBy) {
        this._fieldHandler.setFieldSize(71, fcSttbSavedBy);
    }
    
    public int getModifiedLow() {
        return this._fieldHandler.getFieldOffset(74);
    }
    
    public int getModifiedHigh() {
        return this._fieldHandler.getFieldSize(74);
    }
    
    public void setModifiedLow(final int modifiedLow) {
        this._fieldHandler.setFieldOffset(74, modifiedLow);
    }
    
    public void setModifiedHigh(final int modifiedHigh) {
        this._fieldHandler.setFieldSize(74, modifiedHigh);
    }
    
    public int getCbMac() {
        return this._fibRgLw.getCbMac();
    }
    
    public void setCbMac(final int cbMac) {
        this._fibRgLw.setCbMac(cbMac);
    }
    
    public int getSubdocumentTextStreamLength(final SubdocumentType type) {
        if (type == null) {
            throw new IllegalArgumentException("argument 'type' is null");
        }
        return this._fibRgLw.getSubdocumentTextStreamLength(type);
    }
    
    public void setSubdocumentTextStreamLength(final SubdocumentType type, final int length) {
        if (type == null) {
            throw new IllegalArgumentException("argument 'type' is null");
        }
        if (length < 0) {
            throw new IllegalArgumentException("Subdocument length can't be less than 0 (passed value is " + length + "). " + "If there is no subdocument " + "length must be set to zero.");
        }
        this._fibRgLw.setSubdocumentTextStreamLength(type, length);
    }
    
    public void clearOffsetsSizes() {
        this._fieldHandler.clearFields();
    }
    
    public int getFieldsPlcfOffset(final FieldsDocumentPart part) {
        return this._fieldHandler.getFieldOffset(part.getFibFieldsField());
    }
    
    public int getFieldsPlcfLength(final FieldsDocumentPart part) {
        return this._fieldHandler.getFieldSize(part.getFibFieldsField());
    }
    
    public void setFieldsPlcfOffset(final FieldsDocumentPart part, final int offset) {
        this._fieldHandler.setFieldOffset(part.getFibFieldsField(), offset);
    }
    
    public void setFieldsPlcfLength(final FieldsDocumentPart part, final int length) {
        this._fieldHandler.setFieldSize(part.getFibFieldsField(), length);
    }
    
    @Deprecated
    public int getFcPlcffldAtn() {
        return this._fieldHandler.getFieldOffset(19);
    }
    
    @Deprecated
    public int getLcbPlcffldAtn() {
        return this._fieldHandler.getFieldSize(19);
    }
    
    @Deprecated
    public void setFcPlcffldAtn(final int offset) {
        this._fieldHandler.setFieldOffset(19, offset);
    }
    
    @Deprecated
    public void setLcbPlcffldAtn(final int size) {
        this._fieldHandler.setFieldSize(19, size);
    }
    
    @Deprecated
    public int getFcPlcffldEdn() {
        return this._fieldHandler.getFieldOffset(48);
    }
    
    @Deprecated
    public int getLcbPlcffldEdn() {
        return this._fieldHandler.getFieldSize(48);
    }
    
    @Deprecated
    public void setFcPlcffldEdn(final int offset) {
        this._fieldHandler.setFieldOffset(48, offset);
    }
    
    @Deprecated
    public void setLcbPlcffldEdn(final int size) {
        this._fieldHandler.setFieldSize(48, size);
    }
    
    @Deprecated
    public int getFcPlcffldFtn() {
        return this._fieldHandler.getFieldOffset(18);
    }
    
    @Deprecated
    public int getLcbPlcffldFtn() {
        return this._fieldHandler.getFieldSize(18);
    }
    
    @Deprecated
    public void setFcPlcffldFtn(final int offset) {
        this._fieldHandler.setFieldOffset(18, offset);
    }
    
    @Deprecated
    public void setLcbPlcffldFtn(final int size) {
        this._fieldHandler.setFieldSize(18, size);
    }
    
    @Deprecated
    public int getFcPlcffldHdr() {
        return this._fieldHandler.getFieldOffset(17);
    }
    
    @Deprecated
    public int getLcbPlcffldHdr() {
        return this._fieldHandler.getFieldSize(17);
    }
    
    @Deprecated
    public void setFcPlcffldHdr(final int offset) {
        this._fieldHandler.setFieldOffset(17, offset);
    }
    
    @Deprecated
    public void setLcbPlcffldHdr(final int size) {
        this._fieldHandler.setFieldSize(17, size);
    }
    
    @Deprecated
    public int getFcPlcffldHdrtxbx() {
        return this._fieldHandler.getFieldOffset(59);
    }
    
    @Deprecated
    public int getLcbPlcffldHdrtxbx() {
        return this._fieldHandler.getFieldSize(59);
    }
    
    @Deprecated
    public void setFcPlcffldHdrtxbx(final int offset) {
        this._fieldHandler.setFieldOffset(59, offset);
    }
    
    @Deprecated
    public void setLcbPlcffldHdrtxbx(final int size) {
        this._fieldHandler.setFieldSize(59, size);
    }
    
    @Deprecated
    public int getFcPlcffldMom() {
        return this._fieldHandler.getFieldOffset(16);
    }
    
    @Deprecated
    public int getLcbPlcffldMom() {
        return this._fieldHandler.getFieldSize(16);
    }
    
    @Deprecated
    public void setFcPlcffldMom(final int offset) {
        this._fieldHandler.setFieldOffset(16, offset);
    }
    
    @Deprecated
    public void setLcbPlcffldMom(final int size) {
        this._fieldHandler.setFieldSize(16, size);
    }
    
    @Deprecated
    public int getFcPlcffldTxbx() {
        return this._fieldHandler.getFieldOffset(57);
    }
    
    @Deprecated
    public int getLcbPlcffldTxbx() {
        return this._fieldHandler.getFieldSize(57);
    }
    
    @Deprecated
    public void setFcPlcffldTxbx(final int offset) {
        this._fieldHandler.setFieldOffset(57, offset);
    }
    
    @Deprecated
    public void setLcbPlcffldTxbx(final int size) {
        this._fieldHandler.setFieldSize(57, size);
    }
    
    public int getFSPAPlcfOffset(final FSPADocumentPart part) {
        return this._fieldHandler.getFieldOffset(part.getFibFieldsField());
    }
    
    public int getFSPAPlcfLength(final FSPADocumentPart part) {
        return this._fieldHandler.getFieldSize(part.getFibFieldsField());
    }
    
    public void setFSPAPlcfOffset(final FSPADocumentPart part, final int offset) {
        this._fieldHandler.setFieldOffset(part.getFibFieldsField(), offset);
    }
    
    public void setFSPAPlcfLength(final FSPADocumentPart part, final int length) {
        this._fieldHandler.setFieldSize(part.getFibFieldsField(), length);
    }
    
    @Deprecated
    public int getFcPlcspaMom() {
        return this._fieldHandler.getFieldOffset(40);
    }
    
    @Deprecated
    public int getLcbPlcspaMom() {
        return this._fieldHandler.getFieldSize(40);
    }
    
    public int getFcDggInfo() {
        return this._fieldHandler.getFieldOffset(50);
    }
    
    public int getLcbDggInfo() {
        return this._fieldHandler.getFieldSize(50);
    }
    
    public int getNotesDescriptorsOffset(final NoteType noteType) {
        return this._fieldHandler.getFieldOffset(noteType.getFibDescriptorsFieldIndex());
    }
    
    public void setNotesDescriptorsOffset(final NoteType noteType, final int offset) {
        this._fieldHandler.setFieldOffset(noteType.getFibDescriptorsFieldIndex(), offset);
    }
    
    public int getNotesDescriptorsSize(final NoteType noteType) {
        return this._fieldHandler.getFieldSize(noteType.getFibDescriptorsFieldIndex());
    }
    
    public void setNotesDescriptorsSize(final NoteType noteType, final int offset) {
        this._fieldHandler.setFieldSize(noteType.getFibDescriptorsFieldIndex(), offset);
    }
    
    public int getNotesTextPositionsOffset(final NoteType noteType) {
        return this._fieldHandler.getFieldOffset(noteType.getFibTextPositionsFieldIndex());
    }
    
    public void setNotesTextPositionsOffset(final NoteType noteType, final int offset) {
        this._fieldHandler.setFieldOffset(noteType.getFibTextPositionsFieldIndex(), offset);
    }
    
    public int getNotesTextPositionsSize(final NoteType noteType) {
        return this._fieldHandler.getFieldSize(noteType.getFibTextPositionsFieldIndex());
    }
    
    public void setNotesTextPositionsSize(final NoteType noteType, final int offset) {
        this._fieldHandler.setFieldSize(noteType.getFibTextPositionsFieldIndex(), offset);
    }
    
    public void writeTo(final byte[] mainStream, final HWPFOutputStream tableStream) throws IOException {
        this._cbRgFcLcb = this._fieldHandler.getFieldsCount();
        this._fibBase.serialize(mainStream, 0);
        int offset = FibBaseAbstractType.getSize();
        LittleEndian.putUShort(mainStream, offset, this._csw);
        offset += 2;
        this._fibRgW.serialize(mainStream, offset);
        offset += FibRgW97.getSize();
        LittleEndian.putUShort(mainStream, offset, this._cslw);
        offset += 2;
        ((FibRgLw97)this._fibRgLw).serialize(mainStream, offset);
        offset += FibRgLw97.getSize();
        LittleEndian.putUShort(mainStream, offset, this._cbRgFcLcb);
        offset += 2;
        this._fieldHandler.writeTo(mainStream, offset, tableStream);
        offset += this._cbRgFcLcb * 4 * 2;
        LittleEndian.putUShort(mainStream, offset, this._cswNew);
        offset += 2;
        if (this._cswNew != 0) {
            LittleEndian.putUShort(mainStream, offset, this._nFibNew);
            offset += 2;
            System.arraycopy(this._fibRgCswNew, 0, mainStream, offset, this._fibRgCswNew.length);
            offset += this._fibRgCswNew.length;
        }
    }
    
    public int getSize() {
        return FibBase.getSize() + 2 + FibRgW97.getSize() + 2 + FibRgLw97.getSize() + 2 + this._fieldHandler.sizeInBytes();
    }
    
    public FibBase getFibBase() {
        return this._fibBase;
    }
    
    static {
        logger = POILogFactory.getLogger(FileInformationBlock.class);
    }
}
