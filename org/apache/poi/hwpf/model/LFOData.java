// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hwpf.model;

import java.io.IOException;
import java.io.OutputStream;
import org.apache.poi.hwpf.model.io.HWPFOutputStream;
import org.apache.poi.util.LittleEndian;
import org.apache.poi.util.Internal;

@Internal
class LFOData
{
    private int _cp;
    private ListFormatOverrideLevel[] _rgLfoLvl;
    
    LFOData(final byte[] buf, final int startOffset, final int cLfolvl) {
        int offset = startOffset;
        this._cp = LittleEndian.getInt(buf, offset);
        offset += 4;
        this._rgLfoLvl = new ListFormatOverrideLevel[cLfolvl];
        for (int x = 0; x < cLfolvl; ++x) {
            this._rgLfoLvl[x] = new ListFormatOverrideLevel(buf, offset);
            offset += this._rgLfoLvl[x].getSizeInBytes();
        }
    }
    
    int getCp() {
        return this._cp;
    }
    
    ListFormatOverrideLevel[] getRgLfoLvl() {
        return this._rgLfoLvl;
    }
    
    int getSizeInBytes() {
        int result = 0;
        result += 4;
        for (final ListFormatOverrideLevel lfolvl : this._rgLfoLvl) {
            result += lfolvl.getSizeInBytes();
        }
        return result;
    }
    
    void writeTo(final HWPFOutputStream tableStream) throws IOException {
        LittleEndian.putInt(this._cp, tableStream);
        for (final ListFormatOverrideLevel lfolvl : this._rgLfoLvl) {
            tableStream.write(lfolvl.toByteArray());
        }
    }
}
