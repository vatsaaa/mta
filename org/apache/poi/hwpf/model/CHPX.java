// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hwpf.model;

import org.apache.poi.hwpf.sprm.CharacterSprmUncompressor;
import org.apache.poi.hwpf.usermodel.CharacterProperties;
import org.apache.poi.hwpf.sprm.SprmBuffer;
import org.apache.poi.util.Internal;

@Internal
public final class CHPX extends BytePropertyNode<CHPX>
{
    @Deprecated
    public CHPX(final int fcStart, final int fcEnd, final CharIndexTranslator translator, final byte[] grpprl) {
        super(fcStart, translator.lookIndexBackward(fcEnd), translator, new SprmBuffer(grpprl, 0));
    }
    
    @Deprecated
    public CHPX(final int fcStart, final int fcEnd, final CharIndexTranslator translator, final SprmBuffer buf) {
        super(fcStart, translator.lookIndexBackward(fcEnd), translator, buf);
    }
    
    CHPX(final int charStart, final int charEnd, final SprmBuffer buf) {
        super(charStart, charEnd, buf);
    }
    
    public byte[] getGrpprl() {
        return ((SprmBuffer)this._buf).toByteArray();
    }
    
    public SprmBuffer getSprmBuf() {
        return (SprmBuffer)this._buf;
    }
    
    public CharacterProperties getCharacterProperties(final StyleSheet ss, final short istd) {
        if (ss == null) {
            return new CharacterProperties();
        }
        CharacterProperties baseStyle = ss.getCharacterStyle(istd);
        if (baseStyle == null) {
            baseStyle = new CharacterProperties();
        }
        final CharacterProperties props = CharacterSprmUncompressor.uncompressCHP(baseStyle, this.getGrpprl(), 0);
        return props;
    }
    
    @Override
    public String toString() {
        return "CHPX from " + this.getStart() + " to " + this.getEnd() + " (in bytes " + this.getStartBytes() + " to " + this.getEndBytes() + ")";
    }
}
