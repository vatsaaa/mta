// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hwpf.model;

import java.io.IOException;
import org.apache.poi.hwpf.model.io.HWPFOutputStream;
import java.util.HashMap;
import java.util.ArrayList;
import java.util.Map;
import org.apache.poi.util.Internal;

@Internal
public class FieldsTables
{
    private static final int FLD_SIZE = 2;
    @Deprecated
    public static final int PLCFFLDATN = 0;
    @Deprecated
    public static final int PLCFFLDEDN = 1;
    @Deprecated
    public static final int PLCFFLDFTN = 2;
    @Deprecated
    public static final int PLCFFLDHDR = 3;
    @Deprecated
    public static final int PLCFFLDHDRTXBX = 4;
    @Deprecated
    public static final int PLCFFLDMOM = 5;
    @Deprecated
    public static final int PLCFFLDTXBX = 6;
    private Map<FieldsDocumentPart, PlexOfCps> _tables;
    
    private static ArrayList<PlexOfField> toArrayList(final PlexOfCps plexOfCps) {
        if (plexOfCps == null) {
            return new ArrayList<PlexOfField>();
        }
        final ArrayList<PlexOfField> fields = new ArrayList<PlexOfField>(plexOfCps.length());
        for (int i = 0; i < plexOfCps.length(); ++i) {
            final GenericPropertyNode propNode = plexOfCps.getProperty(i);
            final PlexOfField plex = new PlexOfField(propNode);
            fields.add(plex);
        }
        return fields;
    }
    
    public FieldsTables(final byte[] tableStream, final FileInformationBlock fib) {
        this._tables = new HashMap<FieldsDocumentPart, PlexOfCps>(FieldsDocumentPart.values().length);
        for (final FieldsDocumentPart part : FieldsDocumentPart.values()) {
            final PlexOfCps plexOfCps = this.readPLCF(tableStream, fib, part);
            this._tables.put(part, plexOfCps);
        }
    }
    
    public ArrayList<PlexOfField> getFieldsPLCF(final FieldsDocumentPart part) {
        return toArrayList(this._tables.get(part));
    }
    
    @Deprecated
    public ArrayList<PlexOfField> getFieldsPLCF(final int partIndex) {
        return this.getFieldsPLCF(FieldsDocumentPart.values()[partIndex]);
    }
    
    private PlexOfCps readPLCF(final byte[] tableStream, final FileInformationBlock fib, final FieldsDocumentPart documentPart) {
        final int start = fib.getFieldsPlcfOffset(documentPart);
        final int length = fib.getFieldsPlcfLength(documentPart);
        if (start <= 0 || length <= 0) {
            return null;
        }
        return new PlexOfCps(tableStream, start, length, 2);
    }
    
    private int savePlex(final FileInformationBlock fib, final FieldsDocumentPart part, final PlexOfCps plexOfCps, final HWPFOutputStream outputStream) throws IOException {
        if (plexOfCps == null || plexOfCps.length() == 0) {
            fib.setFieldsPlcfOffset(part, outputStream.getOffset());
            fib.setFieldsPlcfLength(part, 0);
            return 0;
        }
        final byte[] data = plexOfCps.toByteArray();
        final int start = outputStream.getOffset();
        final int length = data.length;
        outputStream.write(data);
        fib.setFieldsPlcfOffset(part, start);
        fib.setFieldsPlcfLength(part, length);
        return length;
    }
    
    public void write(final FileInformationBlock fib, final HWPFOutputStream tableStream) throws IOException {
        for (final FieldsDocumentPart part : FieldsDocumentPart.values()) {
            final PlexOfCps plexOfCps = this._tables.get(part);
            this.savePlex(fib, part, plexOfCps, tableStream);
        }
    }
}
