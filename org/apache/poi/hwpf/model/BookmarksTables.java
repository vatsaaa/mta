// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hwpf.model;

import org.apache.poi.util.POILogFactory;
import java.io.IOException;
import org.apache.poi.hwpf.model.io.HWPFOutputStream;
import org.apache.poi.hwpf.model.types.BKFAbstractType;
import java.util.Collection;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.List;
import org.apache.poi.util.POILogger;
import org.apache.poi.util.Internal;

@Internal
public class BookmarksTables
{
    private static final POILogger logger;
    private PlexOfCps descriptorsFirst;
    private PlexOfCps descriptorsLim;
    private List<String> names;
    
    public BookmarksTables(final byte[] tableStream, final FileInformationBlock fib) {
        this.descriptorsFirst = new PlexOfCps(4);
        this.descriptorsLim = new PlexOfCps(0);
        this.names = new ArrayList<String>(0);
        this.read(tableStream, fib);
    }
    
    public void afterDelete(final int startCp, final int length) {
        this.descriptorsFirst.adjust(startCp, -length);
        this.descriptorsLim.adjust(startCp, -length);
        for (int i = 0; i < this.descriptorsFirst.length(); ++i) {
            final GenericPropertyNode startNode = this.descriptorsFirst.getProperty(i);
            final GenericPropertyNode endNode = this.descriptorsLim.getProperty(i);
            if (startNode.getStart() == endNode.getStart()) {
                BookmarksTables.logger.log(POILogger.DEBUG, "Removing bookmark #", i, "...");
                this.remove(i);
                --i;
            }
        }
    }
    
    public void afterInsert(final int startCp, final int length) {
        this.descriptorsFirst.adjust(startCp, length);
        this.descriptorsLim.adjust(startCp - 1, length);
    }
    
    public int getBookmarksCount() {
        return this.descriptorsFirst.length();
    }
    
    public GenericPropertyNode getDescriptorFirst(final int index) throws IndexOutOfBoundsException {
        return this.descriptorsFirst.getProperty(index);
    }
    
    public int getDescriptorFirstIndex(final GenericPropertyNode descriptorFirst) {
        return Arrays.asList(this.descriptorsFirst.toPropertiesArray()).indexOf(descriptorFirst);
    }
    
    public GenericPropertyNode getDescriptorLim(final int index) throws IndexOutOfBoundsException {
        return this.descriptorsLim.getProperty(index);
    }
    
    public int getDescriptorsFirstCount() {
        return this.descriptorsFirst.length();
    }
    
    public int getDescriptorsLimCount() {
        return this.descriptorsLim.length();
    }
    
    public String getName(final int index) {
        return this.names.get(index);
    }
    
    public int getNamesCount() {
        return this.names.size();
    }
    
    private void read(final byte[] tableStream, final FileInformationBlock fib) {
        final int namesStart = fib.getFcSttbfbkmk();
        final int namesLength = fib.getLcbSttbfbkmk();
        if (namesStart != 0 && namesLength != 0) {
            this.names = new ArrayList<String>(Arrays.asList(SttbUtils.readSttbfBkmk(tableStream, namesStart)));
        }
        final int firstDescriptorsStart = fib.getFcPlcfbkf();
        final int firstDescriptorsLength = fib.getLcbPlcfbkf();
        if (firstDescriptorsStart != 0 && firstDescriptorsLength != 0) {
            this.descriptorsFirst = new PlexOfCps(tableStream, firstDescriptorsStart, firstDescriptorsLength, BKFAbstractType.getSize());
        }
        final int limDescriptorsStart = fib.getFcPlcfbkl();
        final int limDescriptorsLength = fib.getLcbPlcfbkl();
        if (limDescriptorsStart != 0 && limDescriptorsLength != 0) {
            this.descriptorsLim = new PlexOfCps(tableStream, limDescriptorsStart, limDescriptorsLength, 0);
        }
    }
    
    public void remove(final int index) {
        this.descriptorsFirst.remove(index);
        this.descriptorsLim.remove(index);
        this.names.remove(index);
    }
    
    public void setName(final int index, final String name) {
        this.names.set(index, name);
    }
    
    public void writePlcfBkmkf(final FileInformationBlock fib, final HWPFOutputStream tableStream) throws IOException {
        if (this.descriptorsFirst == null || this.descriptorsFirst.length() == 0) {
            fib.setFcPlcfbkf(0);
            fib.setLcbPlcfbkf(0);
            return;
        }
        final int start = tableStream.getOffset();
        tableStream.write(this.descriptorsFirst.toByteArray());
        final int end = tableStream.getOffset();
        fib.setFcPlcfbkf(start);
        fib.setLcbPlcfbkf(end - start);
    }
    
    public void writePlcfBkmkl(final FileInformationBlock fib, final HWPFOutputStream tableStream) throws IOException {
        if (this.descriptorsLim == null || this.descriptorsLim.length() == 0) {
            fib.setFcPlcfbkl(0);
            fib.setLcbPlcfbkl(0);
            return;
        }
        final int start = tableStream.getOffset();
        tableStream.write(this.descriptorsLim.toByteArray());
        final int end = tableStream.getOffset();
        fib.setFcPlcfbkl(start);
        fib.setLcbPlcfbkl(end - start);
    }
    
    public void writeSttbfBkmk(final FileInformationBlock fib, final HWPFOutputStream tableStream) throws IOException {
        if (this.names == null || this.names.isEmpty()) {
            fib.setFcSttbfbkmk(0);
            fib.setLcbSttbfbkmk(0);
            return;
        }
        final int start = tableStream.getOffset();
        SttbUtils.writeSttbfBkmk(this.names.toArray(new String[this.names.size()]), tableStream);
        final int end = tableStream.getOffset();
        fib.setFcSttbfbkmk(start);
        fib.setLcbSttbfbkmk(end - start);
    }
    
    static {
        logger = POILogFactory.getLogger(BookmarksTables.class);
    }
}
