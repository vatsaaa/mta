// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hwpf.model;

import java.io.IOException;
import org.apache.poi.hwpf.model.io.HWPFOutputStream;
import org.apache.poi.util.StringUtil;
import org.apache.poi.util.LittleEndian;
import org.apache.poi.util.Internal;

@Internal
class SttbUtils
{
    private static final int CBEXTRA_STTB_SAVED_BY = 0;
    private static final int CBEXTRA_STTBF_BKMK = 0;
    private static final int CBEXTRA_STTBF_R_MARK = 0;
    private static final int CDATA_SIZE_STTB_SAVED_BY = 2;
    private static final int CDATA_SIZE_STTBF_BKMK = 2;
    private static final int CDATA_SIZE_STTBF_R_MARK = 2;
    
    static Sttb read(final int cDataLength, final byte[] buffer, final int startOffset) {
        final short ffff = LittleEndian.getShort(buffer, startOffset);
        int offset = startOffset + 2;
        if (ffff != -1) {
            throw new UnsupportedOperationException("Non-extended character Pascal strings are not supported right now. Please, contact POI developers for update.");
        }
        final int cData = (cDataLength == 2) ? LittleEndian.getUShort(buffer, offset) : LittleEndian.getInt(buffer, offset);
        offset += cDataLength;
        final Sttb sttb = new Sttb();
        sttb.cDataLength = cDataLength;
        sttb.cbExtra = LittleEndian.getUShort(buffer, offset);
        offset += 2;
        sttb.data = new String[cData];
        sttb.extraData = new byte[cData][];
        for (int i = 0; i < cData; ++i) {
            final int cchData = LittleEndian.getShort(buffer, offset);
            offset += 2;
            if (cchData >= 0) {
                sttb.data[i] = StringUtil.getFromUnicodeLE(buffer, offset, cchData);
                offset += cchData * 2;
                sttb.extraData[i] = LittleEndian.getByteArray(buffer, offset, sttb.cbExtra);
                offset += sttb.cbExtra;
            }
        }
        return sttb;
    }
    
    static String[] readSttbfBkmk(final byte[] buffer, final int startOffset) {
        return read(2, buffer, startOffset).data;
    }
    
    static String[] readSttbfRMark(final byte[] buffer, final int startOffset) {
        return read(2, buffer, startOffset).data;
    }
    
    static String[] readSttbSavedBy(final byte[] buffer, final int startOffset) {
        return read(2, buffer, startOffset).data;
    }
    
    static void write(final Sttb sttb, final HWPFOutputStream tableStream) throws IOException {
        final int headerSize = (sttb.cDataLength == 2) ? 6 : 8;
        final byte[] header = new byte[headerSize];
        LittleEndian.putShort(header, 0, (short)(-1));
        if (sttb.data != null && sttb.data.length != 0) {
            if (sttb.cDataLength == 4) {
                LittleEndian.putInt(header, 2, sttb.data.length);
                LittleEndian.putUShort(header, 6, sttb.cbExtra);
                tableStream.write(header);
            }
            else {
                LittleEndian.putUShort(header, 2, sttb.data.length);
                LittleEndian.putUShort(header, 4, sttb.cbExtra);
                tableStream.write(header);
            }
            for (int i = 0; i < sttb.data.length; ++i) {
                final String entry = sttb.data[i];
                if (entry == null) {
                    tableStream.write(new byte[] { -1, 0 });
                }
                else {
                    final byte[] buf = new byte[entry.length() * 2 + sttb.cbExtra + 2];
                    LittleEndian.putShort(buf, 0, (short)entry.length());
                    StringUtil.putUnicodeLE(entry, buf, 2);
                    if (sttb.extraData != null && i < sttb.extraData.length && sttb.extraData[i] != null) {
                        System.arraycopy(sttb.extraData[i], 0, buf, entry.length() * 2, Math.min(sttb.extraData[i].length, sttb.cbExtra));
                    }
                    tableStream.write(buf);
                }
            }
            return;
        }
        if (sttb.cDataLength == 4) {
            LittleEndian.putInt(header, 2, 0);
            LittleEndian.putUShort(header, 6, sttb.cbExtra);
            tableStream.write(header);
            return;
        }
        LittleEndian.putUShort(header, 2, 0);
        LittleEndian.putUShort(header, 4, sttb.cbExtra);
        tableStream.write(header);
    }
    
    static void writeSttbfBkmk(final String[] data, final HWPFOutputStream tableStream) throws IOException {
        final Sttb sttb = new Sttb();
        sttb.cDataLength = 2;
        sttb.data = data;
        sttb.cbExtra = 0;
        write(sttb, tableStream);
    }
    
    static void writeSttbfRMark(final String[] data, final HWPFOutputStream tableStream) throws IOException {
        final Sttb sttb = new Sttb();
        sttb.cDataLength = 2;
        sttb.data = data;
        sttb.cbExtra = 0;
        write(sttb, tableStream);
    }
    
    static void writeSttbSavedBy(final String[] data, final HWPFOutputStream tableStream) throws IOException {
        final Sttb sttb = new Sttb();
        sttb.cDataLength = 2;
        sttb.data = data;
        sttb.cbExtra = 0;
        write(sttb, tableStream);
    }
    
    static class Sttb
    {
        public int cbExtra;
        public int cDataLength;
        public String[] data;
        public byte[][] extraData;
    }
}
