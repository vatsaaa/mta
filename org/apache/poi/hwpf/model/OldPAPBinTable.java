// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hwpf.model;

import java.util.Iterator;
import org.apache.poi.util.LittleEndian;
import org.apache.poi.util.Internal;

@Internal
public final class OldPAPBinTable extends PAPBinTable
{
    public OldPAPBinTable(final byte[] documentStream, final int offset, final int size, final int fcMin, final TextPieceTable tpt) {
        final PlexOfCps binTable = new PlexOfCps(documentStream, offset, size, 2);
        for (int length = binTable.length(), x = 0; x < length; ++x) {
            final GenericPropertyNode node = binTable.getProperty(x);
            final int pageNum = LittleEndian.getUShort(node.getBytes());
            final int pageOffset = 512 * pageNum;
            final PAPFormattedDiskPage pfkp = new PAPFormattedDiskPage(documentStream, documentStream, pageOffset, tpt);
            for (final PAPX papx : pfkp.getPAPXs()) {
                if (papx != null) {
                    this._paragraphs.add(papx);
                }
            }
        }
    }
}
