// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hwpf.model;

import java.text.MessageFormat;
import org.apache.poi.util.Internal;

@Internal
public class PlexOfField
{
    private final GenericPropertyNode propertyNode;
    private final FieldDescriptor fld;
    
    @Deprecated
    public PlexOfField(final int fcStart, final int fcEnd, final byte[] data) {
        this.propertyNode = new GenericPropertyNode(fcStart, fcEnd, data);
        this.fld = new FieldDescriptor(data);
    }
    
    public PlexOfField(final GenericPropertyNode propertyNode) {
        this.propertyNode = propertyNode;
        this.fld = new FieldDescriptor(propertyNode.getBytes());
    }
    
    public int getFcStart() {
        return this.propertyNode.getStart();
    }
    
    public int getFcEnd() {
        return this.propertyNode.getEnd();
    }
    
    public FieldDescriptor getFld() {
        return this.fld;
    }
    
    @Override
    public String toString() {
        return MessageFormat.format("[{0}, {1}) - FLD - 0x{2}; 0x{3}", this.getFcStart(), this.getFcEnd(), Integer.toHexString(0xFF & this.fld.getBoundaryType()), Integer.toHexString(0xFF & this.fld.getFlt()));
    }
}
