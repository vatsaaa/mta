// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hwpf.model;

import org.apache.poi.hwpf.sprm.CharacterSprmUncompressor;
import org.apache.poi.hwpf.sprm.ParagraphSprmUncompressor;
import java.io.IOException;
import org.apache.poi.hwpf.model.io.HWPFOutputStream;
import org.apache.poi.util.LittleEndian;
import org.apache.poi.hwpf.usermodel.CharacterProperties;
import org.apache.poi.hwpf.usermodel.ParagraphProperties;
import org.apache.poi.util.Internal;

@Internal
public final class StyleSheet implements HDFType
{
    public static final int NIL_STYLE = 4095;
    private static final int PAP_TYPE = 1;
    private static final int CHP_TYPE = 2;
    private static final int SEP_TYPE = 4;
    private static final int TAP_TYPE = 5;
    @Deprecated
    private static final ParagraphProperties NIL_PAP;
    @Deprecated
    private static final CharacterProperties NIL_CHP;
    private static final byte[] NIL_CHPX;
    private static final byte[] NIL_PAPX;
    private int _cbStshi;
    private Stshif _stshif;
    StyleDescription[] _styleDescriptions;
    
    public StyleSheet(final byte[] tableStream, int offset) {
        final int startOffset = offset;
        this._cbStshi = LittleEndian.getShort(tableStream, offset);
        offset += 2;
        this._stshif = new Stshif(tableStream, offset);
        offset += Stshif.getSize();
        offset = startOffset + 2 + this._cbStshi;
        this._styleDescriptions = new StyleDescription[this._stshif.getCstd()];
        for (int x = 0; x < this._stshif.getCstd(); ++x) {
            final int stdSize = LittleEndian.getShort(tableStream, offset);
            offset += 2;
            if (stdSize > 0) {
                final StyleDescription aStyle = new StyleDescription(tableStream, this._stshif.getCbSTDBaseInFile(), offset, true);
                this._styleDescriptions[x] = aStyle;
            }
            offset += stdSize;
        }
        for (int x = 0; x < this._styleDescriptions.length; ++x) {
            if (this._styleDescriptions[x] != null) {
                this.createPap(x);
                this.createChp(x);
            }
        }
    }
    
    public void writeTo(final HWPFOutputStream out) throws IOException {
        int offset = 0;
        this._cbStshi = 18;
        final byte[] buf = new byte[this._cbStshi + 2];
        LittleEndian.putUShort(buf, offset, (short)this._cbStshi);
        offset += 2;
        this._stshif.setCstd(this._styleDescriptions.length);
        this._stshif.serialize(buf, offset);
        offset += Stshif.getSize();
        out.write(buf);
        final byte[] sizeHolder = new byte[2];
        for (int x = 0; x < this._styleDescriptions.length; ++x) {
            if (this._styleDescriptions[x] != null) {
                final byte[] std = this._styleDescriptions[x].toByteArray();
                LittleEndian.putShort(sizeHolder, (short)(std.length + std.length % 2));
                out.write(sizeHolder);
                out.write(std);
                if (std.length % 2 == 1) {
                    out.write(0);
                }
            }
            else {
                sizeHolder[1] = (sizeHolder[0] = 0);
                out.write(sizeHolder);
            }
        }
    }
    
    @Override
    public boolean equals(final Object o) {
        final StyleSheet ss = (StyleSheet)o;
        if (ss._stshif.equals(this._stshif) && ss._cbStshi == this._cbStshi && ss._styleDescriptions.length == this._styleDescriptions.length) {
            for (int x = 0; x < this._styleDescriptions.length; ++x) {
                if (ss._styleDescriptions[x] != this._styleDescriptions[x] && !ss._styleDescriptions[x].equals(this._styleDescriptions[x])) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }
    
    @Deprecated
    private void createPap(final int istd) {
        final StyleDescription sd = this._styleDescriptions[istd];
        ParagraphProperties pap = sd.getPAP();
        final byte[] papx = sd.getPAPX();
        final int baseIndex = sd.getBaseStyle();
        if (pap == null && papx != null) {
            ParagraphProperties parentPAP = new ParagraphProperties();
            if (baseIndex != 4095) {
                parentPAP = this._styleDescriptions[baseIndex].getPAP();
                if (parentPAP == null) {
                    if (baseIndex == istd) {
                        throw new IllegalStateException("Pap style " + istd + " claimed to have itself as its parent, which isn't allowed");
                    }
                    this.createPap(baseIndex);
                    parentPAP = this._styleDescriptions[baseIndex].getPAP();
                }
            }
            if (parentPAP == null) {
                parentPAP = new ParagraphProperties();
            }
            pap = ParagraphSprmUncompressor.uncompressPAP(parentPAP, papx, 2);
            sd.setPAP(pap);
        }
    }
    
    @Deprecated
    private void createChp(final int istd) {
        final StyleDescription sd = this._styleDescriptions[istd];
        CharacterProperties chp = sd.getCHP();
        final byte[] chpx = sd.getCHPX();
        int baseIndex = sd.getBaseStyle();
        if (baseIndex == istd) {
            baseIndex = 4095;
        }
        if (chp == null && chpx != null) {
            CharacterProperties parentCHP = new CharacterProperties();
            if (baseIndex != 4095) {
                parentCHP = this._styleDescriptions[baseIndex].getCHP();
                if (parentCHP == null) {
                    this.createChp(baseIndex);
                    parentCHP = this._styleDescriptions[baseIndex].getCHP();
                }
            }
            chp = CharacterSprmUncompressor.uncompressCHP(parentCHP, chpx, 0);
            sd.setCHP(chp);
        }
    }
    
    public int numStyles() {
        return this._styleDescriptions.length;
    }
    
    public StyleDescription getStyleDescription(final int styleIndex) {
        return this._styleDescriptions[styleIndex];
    }
    
    @Deprecated
    public CharacterProperties getCharacterStyle(final int styleIndex) {
        if (styleIndex == 4095) {
            return StyleSheet.NIL_CHP;
        }
        if (styleIndex >= this._styleDescriptions.length) {
            return StyleSheet.NIL_CHP;
        }
        return (this._styleDescriptions[styleIndex] != null) ? this._styleDescriptions[styleIndex].getCHP() : StyleSheet.NIL_CHP;
    }
    
    @Deprecated
    public ParagraphProperties getParagraphStyle(final int styleIndex) {
        if (styleIndex == 4095) {
            return StyleSheet.NIL_PAP;
        }
        if (styleIndex >= this._styleDescriptions.length) {
            return StyleSheet.NIL_PAP;
        }
        if (this._styleDescriptions[styleIndex] == null) {
            return StyleSheet.NIL_PAP;
        }
        if (this._styleDescriptions[styleIndex].getPAP() == null) {
            return StyleSheet.NIL_PAP;
        }
        return this._styleDescriptions[styleIndex].getPAP();
    }
    
    public byte[] getCHPX(final int styleIndex) {
        if (styleIndex == 4095) {
            return StyleSheet.NIL_CHPX;
        }
        if (styleIndex >= this._styleDescriptions.length) {
            return StyleSheet.NIL_CHPX;
        }
        if (this._styleDescriptions[styleIndex] == null) {
            return StyleSheet.NIL_CHPX;
        }
        if (this._styleDescriptions[styleIndex].getCHPX() == null) {
            return StyleSheet.NIL_CHPX;
        }
        return this._styleDescriptions[styleIndex].getCHPX();
    }
    
    public byte[] getPAPX(final int styleIndex) {
        if (styleIndex == 4095) {
            return StyleSheet.NIL_PAPX;
        }
        if (styleIndex >= this._styleDescriptions.length) {
            return StyleSheet.NIL_PAPX;
        }
        if (this._styleDescriptions[styleIndex] == null) {
            return StyleSheet.NIL_PAPX;
        }
        if (this._styleDescriptions[styleIndex].getPAPX() == null) {
            return StyleSheet.NIL_PAPX;
        }
        return this._styleDescriptions[styleIndex].getPAPX();
    }
    
    static {
        NIL_PAP = new ParagraphProperties();
        NIL_CHP = new CharacterProperties();
        NIL_CHPX = new byte[0];
        NIL_PAPX = new byte[] { 0, 0 };
    }
}
