// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hwpf.model;

import org.apache.poi.util.Internal;

@Internal
public interface CharIndexTranslator
{
    int getByteIndex(final int p0);
    
    @Deprecated
    int getCharIndex(final int p0);
    
    @Deprecated
    int getCharIndex(final int p0, final int p1);
    
    int[][] getCharIndexRanges(final int p0, final int p1);
    
    boolean isIndexInTable(final int p0);
    
    int lookIndexForward(final int p0);
    
    int lookIndexBackward(final int p0);
}
