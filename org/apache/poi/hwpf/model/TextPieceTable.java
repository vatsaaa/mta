// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hwpf.model;

import org.apache.poi.util.POILogFactory;
import java.io.IOException;
import org.apache.poi.hwpf.model.io.HWPFOutputStream;
import java.util.LinkedList;
import java.util.Iterator;
import java.util.Comparator;
import java.util.Collection;
import java.util.List;
import java.util.Collections;
import java.util.ArrayList;
import org.apache.poi.util.POILogger;
import org.apache.poi.util.Internal;

@Internal
public class TextPieceTable implements CharIndexTranslator
{
    private static final POILogger logger;
    int _cpMin;
    protected ArrayList<TextPiece> _textPieces;
    protected ArrayList<TextPiece> _textPiecesFCOrder;
    
    public TextPieceTable() {
        this._textPieces = new ArrayList<TextPiece>();
        this._textPiecesFCOrder = new ArrayList<TextPiece>();
    }
    
    public TextPieceTable(final byte[] documentStream, final byte[] tableStream, final int offset, final int size, final int fcMin) {
        this._textPieces = new ArrayList<TextPiece>();
        this._textPiecesFCOrder = new ArrayList<TextPiece>();
        final PlexOfCps pieceTable = new PlexOfCps(tableStream, offset, size, PieceDescriptor.getSizeInBytes());
        final int length = pieceTable.length();
        final PieceDescriptor[] pieces = new PieceDescriptor[length];
        for (int x = 0; x < length; ++x) {
            final GenericPropertyNode node = pieceTable.getProperty(x);
            pieces[x] = new PieceDescriptor(node.getBytes(), 0);
        }
        this._cpMin = pieces[0].getFilePosition() - fcMin;
        for (int x = 0; x < pieces.length; ++x) {
            final int start = pieces[x].getFilePosition() - fcMin;
            if (start < this._cpMin) {
                this._cpMin = start;
            }
        }
        for (int x = 0; x < pieces.length; ++x) {
            final int start = pieces[x].getFilePosition();
            final GenericPropertyNode node2 = pieceTable.getProperty(x);
            final int nodeStartChars = node2.getStart();
            final int nodeEndChars = node2.getEnd();
            final boolean unicode = pieces[x].isUnicode();
            int multiple = 1;
            if (unicode) {
                multiple = 2;
            }
            final int textSizeChars = nodeEndChars - nodeStartChars;
            final int textSizeBytes = textSizeChars * multiple;
            final byte[] buf = new byte[textSizeBytes];
            System.arraycopy(documentStream, start, buf, 0, textSizeBytes);
            final TextPiece newTextPiece = new TextPiece(nodeStartChars, nodeEndChars, buf, pieces[x]);
            this._textPieces.add(newTextPiece);
        }
        Collections.sort(this._textPieces);
        Collections.sort(this._textPiecesFCOrder = new ArrayList<TextPiece>(this._textPieces), new FCComparator());
    }
    
    public void add(final TextPiece piece) {
        this._textPieces.add(piece);
        this._textPiecesFCOrder.add(piece);
        Collections.sort(this._textPieces);
        Collections.sort(this._textPiecesFCOrder, new FCComparator());
    }
    
    public int adjustForInsert(final int listIndex, final int length) {
        final int size = this._textPieces.size();
        TextPiece tp = this._textPieces.get(listIndex);
        tp.setEnd(tp.getEnd() + length);
        for (int x = listIndex + 1; x < size; ++x) {
            tp = this._textPieces.get(x);
            tp.setStart(tp.getStart() + length);
            tp.setEnd(tp.getEnd() + length);
        }
        return length;
    }
    
    @Override
    public boolean equals(final Object o) {
        final TextPieceTable tpt = (TextPieceTable)o;
        final int size = tpt._textPieces.size();
        if (size == this._textPieces.size()) {
            for (int x = 0; x < size; ++x) {
                if (!tpt._textPieces.get(x).equals(this._textPieces.get(x))) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }
    
    public int getByteIndex(final int charPos) {
        int byteCount = 0;
        for (final TextPiece tp : this._textPieces) {
            if (charPos >= tp.getEnd()) {
                byteCount = tp.getPieceDescriptor().getFilePosition() + (tp.getEnd() - tp.getStart()) * (tp.isUnicode() ? 2 : 1);
                if (charPos == tp.getEnd()) {
                    break;
                }
                continue;
            }
            else {
                if (charPos < tp.getEnd()) {
                    final int left = charPos - tp.getStart();
                    byteCount = tp.getPieceDescriptor().getFilePosition() + left * (tp.isUnicode() ? 2 : 1);
                    break;
                }
                continue;
            }
        }
        return byteCount;
    }
    
    @Deprecated
    public int getCharIndex(final int bytePos) {
        return this.getCharIndex(bytePos, 0);
    }
    
    @Deprecated
    public int getCharIndex(final int startBytePos, final int startCP) {
        int charCount = 0;
        final int bytePos = this.lookIndexForward(startBytePos);
        for (final TextPiece tp : this._textPieces) {
            final int pieceStart = tp.getPieceDescriptor().getFilePosition();
            final int bytesLength = tp.bytesLength();
            final int pieceEnd = pieceStart + bytesLength;
            int toAdd;
            if (bytePos < pieceStart || bytePos > pieceEnd) {
                toAdd = bytesLength;
            }
            else if (bytePos > pieceStart && bytePos < pieceEnd) {
                toAdd = bytePos - pieceStart;
            }
            else {
                toAdd = bytesLength - (pieceEnd - bytePos);
            }
            if (tp.isUnicode()) {
                charCount += toAdd / 2;
            }
            else {
                charCount += toAdd;
            }
            if (bytePos >= pieceStart && bytePos <= pieceEnd && charCount >= startCP) {
                break;
            }
        }
        return charCount;
    }
    
    public int[][] getCharIndexRanges(final int startBytePosInclusive, final int endBytePosExclusive) {
        final List<int[]> result = new LinkedList<int[]>();
        for (final TextPiece textPiece : this._textPiecesFCOrder) {
            final int tpStart = textPiece.getPieceDescriptor().getFilePosition();
            final int tpEnd = textPiece.getPieceDescriptor().getFilePosition() + textPiece.bytesLength();
            if (startBytePosInclusive > tpEnd) {
                continue;
            }
            if (endBytePosExclusive < tpStart) {
                break;
            }
            final int rangeStartBytes = Math.max(tpStart, startBytePosInclusive);
            final int rangeEndBytes = Math.min(tpEnd, endBytePosExclusive);
            final int rangeLengthBytes = rangeEndBytes - rangeStartBytes;
            if (rangeStartBytes > rangeEndBytes) {
                continue;
            }
            final int encodingMultiplier = textPiece.isUnicode() ? 2 : 1;
            final int rangeStartCp = textPiece.getStart() + (rangeStartBytes - tpStart) / encodingMultiplier;
            final int rangeEndCp = rangeStartCp + rangeLengthBytes / encodingMultiplier;
            result.add(new int[] { rangeStartCp, rangeEndCp });
        }
        return result.toArray(new int[result.size()][]);
    }
    
    public int getCpMin() {
        return this._cpMin;
    }
    
    public StringBuilder getText() {
        final long start = System.currentTimeMillis();
        final StringBuilder docText = new StringBuilder();
        for (final TextPiece textPiece : this._textPieces) {
            final String toAppend = textPiece.getStringBuilder().toString();
            final int toAppendLength = toAppend.length();
            if (toAppendLength != textPiece.getEnd() - textPiece.getStart()) {
                TextPieceTable.logger.log(POILogger.WARN, "Text piece has boundaries [", textPiece.getStart(), "; ", textPiece.getEnd(), ") but length ", textPiece.getEnd() - textPiece.getStart());
            }
            docText.replace(textPiece.getStart(), textPiece.getStart() + toAppendLength, toAppend);
        }
        TextPieceTable.logger.log(POILogger.DEBUG, "Document text were rebuilded in ", System.currentTimeMillis() - start, " ms (", docText.length(), " chars)");
        return docText;
    }
    
    public List<TextPiece> getTextPieces() {
        return this._textPieces;
    }
    
    @Override
    public int hashCode() {
        return this._textPieces.size();
    }
    
    public boolean isIndexInTable(final int bytePos) {
        for (final TextPiece tp : this._textPiecesFCOrder) {
            final int pieceStart = tp.getPieceDescriptor().getFilePosition();
            if (bytePos > pieceStart + tp.bytesLength()) {
                continue;
            }
            return pieceStart <= bytePos;
        }
        return false;
    }
    
    boolean isIndexInTable(final int startBytePos, final int endBytePos) {
        for (final TextPiece tp : this._textPiecesFCOrder) {
            final int pieceStart = tp.getPieceDescriptor().getFilePosition();
            if (startBytePos >= pieceStart + tp.bytesLength()) {
                continue;
            }
            final int left = Math.max(startBytePos, pieceStart);
            final int right = Math.min(endBytePos, pieceStart + tp.bytesLength());
            return left < right;
        }
        return false;
    }
    
    public int lookIndexBackward(final int startBytePos) {
        int bytePos = startBytePos;
        int lastEnd = 0;
        for (final TextPiece tp : this._textPiecesFCOrder) {
            final int pieceStart = tp.getPieceDescriptor().getFilePosition();
            if (bytePos > pieceStart + tp.bytesLength()) {
                lastEnd = pieceStart + tp.bytesLength();
            }
            else {
                if (pieceStart > bytePos) {
                    bytePos = lastEnd;
                    break;
                }
                break;
            }
        }
        return bytePos;
    }
    
    public int lookIndexForward(final int startBytePos) {
        if (this._textPiecesFCOrder.isEmpty()) {
            throw new IllegalStateException("Text pieces table is empty");
        }
        if (this._textPiecesFCOrder.get(0).getPieceDescriptor().getFilePosition() > startBytePos) {
            return this._textPiecesFCOrder.get(0).getPieceDescriptor().getFilePosition();
        }
        if (this._textPiecesFCOrder.get(this._textPiecesFCOrder.size() - 1).getPieceDescriptor().getFilePosition() <= startBytePos) {
            return startBytePos;
        }
        int low = 0;
        int high = this._textPiecesFCOrder.size() - 1;
        while (low <= high) {
            final int mid = low + high >>> 1;
            final TextPiece textPiece = this._textPiecesFCOrder.get(mid);
            final int midVal = textPiece.getPieceDescriptor().getFilePosition();
            if (midVal < startBytePos) {
                low = mid + 1;
            }
            else {
                if (midVal <= startBytePos) {
                    return textPiece.getPieceDescriptor().getFilePosition();
                }
                high = mid - 1;
            }
        }
        assert low == high;
        assert this._textPiecesFCOrder.get(low).getPieceDescriptor().getFilePosition() < startBytePos;
        assert this._textPiecesFCOrder.get(low + 1).getPieceDescriptor().getFilePosition() > startBytePos;
        return this._textPiecesFCOrder.get(low + 1).getPieceDescriptor().getFilePosition();
    }
    
    public byte[] writeTo(final HWPFOutputStream docStream) throws IOException {
        final PlexOfCps textPlex = new PlexOfCps(PieceDescriptor.getSizeInBytes());
        for (int size = this._textPieces.size(), x = 0; x < size; ++x) {
            final TextPiece next = this._textPieces.get(x);
            final PieceDescriptor pd = next.getPieceDescriptor();
            final int offset = docStream.getOffset();
            int mod = offset % 512;
            if (mod != 0) {
                mod = 512 - mod;
                final byte[] buf = new byte[mod];
                docStream.write(buf);
            }
            pd.setFilePosition(docStream.getOffset());
            docStream.write(next.getRawBytes());
            final int nodeStart = next.getStart();
            final int nodeEnd = next.getEnd();
            textPlex.addProperty(new GenericPropertyNode(nodeStart, nodeEnd, pd.toByteArray()));
        }
        return textPlex.toByteArray();
    }
    
    static {
        logger = POILogFactory.getLogger(TextPieceTable.class);
    }
    
    private static class FCComparator implements Comparator<TextPiece>
    {
        public int compare(final TextPiece textPiece, final TextPiece textPiece1) {
            if (textPiece.getPieceDescriptor().fc > textPiece1.getPieceDescriptor().fc) {
                return 1;
            }
            if (textPiece.getPieceDescriptor().fc < textPiece1.getPieceDescriptor().fc) {
                return -1;
            }
            return 0;
        }
    }
}
