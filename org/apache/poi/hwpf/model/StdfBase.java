// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hwpf.model;

import org.apache.poi.util.Internal;
import org.apache.poi.hwpf.model.types.StdfBaseAbstractType;

@Internal
class StdfBase extends StdfBaseAbstractType
{
    public StdfBase() {
    }
    
    public StdfBase(final byte[] std, final int offset) {
        this.fillFields(std, offset);
    }
    
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        final StdfBase other = (StdfBase)obj;
        return this.field_1_info1 == other.field_1_info1 && this.field_2_info2 == other.field_2_info2 && this.field_3_info3 == other.field_3_info3 && this.field_4_bchUpe == other.field_4_bchUpe && this.field_5_grfstd == other.field_5_grfstd;
    }
    
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = 31 * result + this.field_1_info1;
        result = 31 * result + this.field_2_info2;
        result = 31 * result + this.field_3_info3;
        result = 31 * result + this.field_4_bchUpe;
        result = 31 * result + this.field_5_grfstd;
        return result;
    }
    
    public byte[] serialize() {
        final byte[] result = new byte[StdfBaseAbstractType.getSize()];
        this.serialize(result, 0);
        return result;
    }
}
