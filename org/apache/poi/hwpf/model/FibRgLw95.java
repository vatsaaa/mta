// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hwpf.model;

import org.apache.poi.util.Internal;
import org.apache.poi.hwpf.model.types.FibRgLw95AbstractType;

@Internal
class FibRgLw95 extends FibRgLw95AbstractType implements FibRgLw
{
    public FibRgLw95() {
    }
    
    public FibRgLw95(final byte[] std, final int offset) {
        this.fillFields(std, offset);
    }
    
    public int getSubdocumentTextStreamLength(final SubdocumentType subdocumentType) {
        switch (subdocumentType) {
            case MAIN: {
                return this.getCcpText();
            }
            case FOOTNOTE: {
                return this.getCcpFtn();
            }
            case HEADER: {
                return this.getCcpHdd();
            }
            case MACRO: {
                return this.getCcpMcr();
            }
            case ANNOTATION: {
                return this.getCcpAtn();
            }
            case ENDNOTE: {
                return this.getCcpEdn();
            }
            case TEXTBOX: {
                return this.getCcpTxbx();
            }
            case HEADER_TEXTBOX: {
                return this.getCcpHdrTxbx();
            }
            default: {
                throw new UnsupportedOperationException("Unsupported: " + subdocumentType);
            }
        }
    }
    
    public void setSubdocumentTextStreamLength(final SubdocumentType subdocumentType, final int newLength) {
        switch (subdocumentType) {
            case MAIN: {
                this.setCcpText(newLength);
            }
            case FOOTNOTE: {
                this.setCcpFtn(newLength);
            }
            case HEADER: {
                this.setCcpHdd(newLength);
            }
            case MACRO: {
                this.setCbMac(newLength);
            }
            case ANNOTATION: {
                this.setCcpAtn(newLength);
            }
            case ENDNOTE: {
                this.setCcpEdn(newLength);
            }
            case TEXTBOX: {
                this.setCcpTxbx(newLength);
            }
            case HEADER_TEXTBOX: {
                this.setCcpHdrTxbx(newLength);
            }
            default: {
                throw new UnsupportedOperationException("Unsupported: " + subdocumentType);
            }
        }
    }
}
