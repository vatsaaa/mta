// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hwpf.model;

import org.apache.poi.util.Internal;
import org.apache.poi.hwpf.model.types.StdfPost2000AbstractType;

@Internal
class StdfPost2000 extends StdfPost2000AbstractType
{
    public StdfPost2000() {
    }
    
    public StdfPost2000(final byte[] std, final int offset) {
        this.fillFields(std, offset);
    }
    
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        final StdfPost2000 other = (StdfPost2000)obj;
        return this.field_1_info1 == other.field_1_info1 && this.field_2_rsid == other.field_2_rsid && this.field_3_info3 == other.field_3_info3;
    }
    
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = 31 * result + this.field_1_info1;
        result = 31 * result + (int)(this.field_2_rsid ^ this.field_2_rsid >>> 32);
        result = 31 * result + this.field_3_info3;
        return result;
    }
    
    public byte[] serialize() {
        final byte[] result = new byte[StdfPost2000AbstractType.getSize()];
        this.serialize(result, 0);
        return result;
    }
}
