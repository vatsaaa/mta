// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hwpf.model;

import java.io.IOException;
import org.apache.poi.util.Internal;

@Internal
public class SinglentonTextPiece extends TextPiece
{
    public SinglentonTextPiece(final StringBuilder buffer) throws IOException {
        super(0, buffer.length(), buffer.toString().getBytes("UTF-16LE"), new PieceDescriptor(new byte[8], 0));
    }
    
    @Override
    public int bytesLength() {
        return this.getStringBuilder().length() * 2;
    }
    
    @Override
    public int characterLength() {
        return this.getStringBuilder().length();
    }
    
    @Override
    public int getCP() {
        return 0;
    }
    
    @Override
    public int getEnd() {
        return this.characterLength();
    }
    
    @Override
    public int getStart() {
        return 0;
    }
    
    @Override
    public String toString() {
        return "SinglentonTextPiece (" + this.characterLength() + " chars)";
    }
}
