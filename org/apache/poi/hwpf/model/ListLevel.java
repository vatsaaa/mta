// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hwpf.model;

import org.apache.poi.util.POILogFactory;
import org.apache.poi.util.LittleEndian;
import java.util.Arrays;
import org.apache.poi.util.POILogger;
import org.apache.poi.util.Internal;

@Internal
public final class ListLevel
{
    private static final POILogger logger;
    private byte[] _grpprlChpx;
    private byte[] _grpprlPapx;
    private LVLF _lvlf;
    private char[] _xst;
    
    ListLevel() {
        this._xst = new char[0];
    }
    
    @Deprecated
    public ListLevel(final byte[] buf, final int startOffset) {
        this._xst = new char[0];
        this.read(buf, startOffset);
    }
    
    public ListLevel(final int level, final boolean numbered) {
        this._xst = new char[0];
        this._lvlf = new LVLF();
        this.setStartAt(1);
        this._grpprlPapx = new byte[0];
        this._grpprlChpx = new byte[0];
        if (numbered) {
            this._lvlf.getRgbxchNums()[0] = 1;
            this._xst = new char[] { (char)level, '.' };
        }
        else {
            this._xst = new char[] { '\u2022' };
        }
    }
    
    public ListLevel(final int startAt, final int numberFormatCode, final int alignment, final byte[] numberProperties, final byte[] entryProperties, final String numberText) {
        this._xst = new char[0];
        this._lvlf = new LVLF();
        this.setStartAt(startAt);
        this._lvlf.setNfc((byte)numberFormatCode);
        this._lvlf.setJc((byte)alignment);
        this._grpprlChpx = numberProperties;
        this._grpprlPapx = entryProperties;
        this._xst = numberText.toCharArray();
    }
    
    @Override
    public boolean equals(final Object obj) {
        if (obj == null) {
            return false;
        }
        final ListLevel lvl = (ListLevel)obj;
        return lvl._lvlf.equals(this._lvlf) && Arrays.equals(lvl._grpprlChpx, this._grpprlChpx) && Arrays.equals(lvl._grpprlPapx, this._grpprlPapx) && Arrays.equals(lvl._xst, this._xst);
    }
    
    public int getAlignment() {
        return this._lvlf.getJc();
    }
    
    public byte[] getGrpprlChpx() {
        return this._grpprlChpx;
    }
    
    public byte[] getGrpprlPapx() {
        return this._grpprlPapx;
    }
    
    public byte[] getLevelProperties() {
        return this._grpprlPapx;
    }
    
    public int getNumberFormat() {
        return this._lvlf.getNfc();
    }
    
    public String getNumberText() {
        if (this._xst.length < 2) {
            return null;
        }
        return new String(this._xst, 0, this._xst.length - 1);
    }
    
    public int getSizeInBytes() {
        return LVLF.getSize() + this._lvlf.getCbGrpprlChpx() + this._lvlf.getCbGrpprlPapx() + 2 + this._xst.length * 2;
    }
    
    public int getStartAt() {
        return this._lvlf.getIStartAt();
    }
    
    public byte getTypeOfCharFollowingTheNumber() {
        return this._lvlf.getIxchFollow();
    }
    
    int read(final byte[] data, final int startOffset) {
        int offset = startOffset;
        this._lvlf = new LVLF(data, offset);
        offset += LVLF.getSize();
        System.arraycopy(data, offset, this._grpprlPapx = new byte[this._lvlf.getCbGrpprlPapx()], 0, this._lvlf.getCbGrpprlPapx());
        offset += this._lvlf.getCbGrpprlPapx();
        System.arraycopy(data, offset, this._grpprlChpx = new byte[this._lvlf.getCbGrpprlChpx()], 0, this._lvlf.getCbGrpprlChpx());
        offset += this._lvlf.getCbGrpprlChpx();
        if (this._lvlf.getNfc() == 23) {
            final int cch = LittleEndian.getUShort(data, offset);
            offset += 2;
            if (cch != 1) {
                ListLevel.logger.log(POILogger.WARN, "LVL at offset ", startOffset, " has nfc == 0x17 (bullets), but cch != 1 (", cch, ")");
            }
            this._xst = new char[cch];
            for (int x = 0; x < cch; ++x) {
                this._xst[x] = (char)LittleEndian.getShort(data, offset);
                offset += 2;
            }
        }
        else {
            final int cch = LittleEndian.getUShort(data, offset);
            offset += 2;
            if (cch > 0) {
                this._xst = new char[cch];
                for (int x = 0; x < cch; ++x) {
                    this._xst[x] = (char)LittleEndian.getShort(data, offset);
                    offset += 2;
                }
            }
            else {
                ListLevel.logger.log(POILogger.WARN, "LVL.xst.cch <= 0: ", cch);
                this._xst = new char[0];
            }
        }
        return offset - startOffset;
    }
    
    public void setAlignment(final int alignment) {
        this._lvlf.setJc((byte)alignment);
    }
    
    public void setLevelProperties(final byte[] grpprl) {
        this._grpprlPapx = grpprl;
    }
    
    public void setNumberFormat(final int numberFormatCode) {
        this._lvlf.setNfc((byte)numberFormatCode);
    }
    
    public void setNumberProperties(final byte[] grpprl) {
        this._grpprlChpx = grpprl;
    }
    
    public void setStartAt(final int startAt) {
        this._lvlf.setIStartAt(startAt);
    }
    
    public void setTypeOfCharFollowingTheNumber(final byte value) {
        this._lvlf.setIxchFollow(value);
    }
    
    public byte[] toByteArray() {
        final byte[] buf = new byte[this.getSizeInBytes()];
        int offset = 0;
        this._lvlf.setCbGrpprlChpx((short)this._grpprlChpx.length);
        this._lvlf.setCbGrpprlPapx((short)this._grpprlPapx.length);
        this._lvlf.serialize(buf, offset);
        offset += LVLF.getSize();
        System.arraycopy(this._grpprlPapx, 0, buf, offset, this._grpprlPapx.length);
        offset += this._grpprlPapx.length;
        System.arraycopy(this._grpprlChpx, 0, buf, offset, this._grpprlChpx.length);
        offset += this._grpprlChpx.length;
        if (this._lvlf.getNfc() == 23) {
            LittleEndian.putUShort(buf, offset, 1);
            offset += 2;
            LittleEndian.putUShort(buf, offset, this._xst[0]);
            offset += 2;
        }
        else {
            LittleEndian.putUShort(buf, offset, this._xst.length);
            offset += 2;
            for (final char c : this._xst) {
                LittleEndian.putUShort(buf, offset, c);
                offset += 2;
            }
        }
        return buf;
    }
    
    @Override
    public String toString() {
        return "LVL: " + ("\n" + this._lvlf).replaceAll("\n", "\n    ") + "\n" + "PAPX's grpprl: " + Arrays.toString(this._grpprlPapx) + "\n" + "CHPX's grpprl: " + Arrays.toString(this._grpprlChpx) + "\n" + "xst: " + Arrays.toString(this._xst) + "\n";
    }
    
    static {
        logger = POILogFactory.getLogger(ListLevel.class);
    }
}
