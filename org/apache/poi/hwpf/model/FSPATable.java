// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hwpf.model;

import java.util.Iterator;
import java.util.ArrayList;
import java.util.List;
import org.apache.poi.hwpf.model.types.FSPAAbstractType;
import java.util.LinkedHashMap;
import java.util.Map;
import org.apache.poi.util.Internal;

@Internal
public final class FSPATable
{
    private final Map<Integer, GenericPropertyNode> _byStart;
    
    public FSPATable(final byte[] tableStream, final FileInformationBlock fib, final FSPADocumentPart part) {
        this._byStart = new LinkedHashMap<Integer, GenericPropertyNode>();
        final int offset = fib.getFSPAPlcfOffset(part);
        final int length = fib.getFSPAPlcfLength(part);
        final PlexOfCps plex = new PlexOfCps(tableStream, offset, length, FSPAAbstractType.getSize());
        for (int i = 0; i < plex.length(); ++i) {
            final GenericPropertyNode property = plex.getProperty(i);
            this._byStart.put(property.getStart(), property);
        }
    }
    
    @Deprecated
    public FSPATable(final byte[] tableStream, final int fcPlcspa, final int lcbPlcspa, final List<TextPiece> tpt) {
        this._byStart = new LinkedHashMap<Integer, GenericPropertyNode>();
        if (fcPlcspa == 0) {
            return;
        }
        final PlexOfCps plex = new PlexOfCps(tableStream, fcPlcspa, lcbPlcspa, FSPA.FSPA_SIZE);
        for (int i = 0; i < plex.length(); ++i) {
            final GenericPropertyNode property = plex.getProperty(i);
            this._byStart.put(property.getStart(), property);
        }
    }
    
    public FSPA getFspaFromCp(final int cp) {
        final GenericPropertyNode propertyNode = this._byStart.get(cp);
        if (propertyNode == null) {
            return null;
        }
        return new FSPA(propertyNode.getBytes(), 0);
    }
    
    public FSPA[] getShapes() {
        final List<FSPA> result = new ArrayList<FSPA>(this._byStart.size());
        for (final GenericPropertyNode propertyNode : this._byStart.values()) {
            result.add(new FSPA(propertyNode.getBytes(), 0));
        }
        return result.toArray(new FSPA[result.size()]);
    }
    
    @Override
    public String toString() {
        final StringBuffer buf = new StringBuffer();
        buf.append("[FPSA PLC size=").append(this._byStart.size()).append("]\n");
        for (final Map.Entry<Integer, GenericPropertyNode> entry : this._byStart.entrySet()) {
            final Integer i = entry.getKey();
            buf.append("  ").append(i.toString()).append(" => \t");
            try {
                final FSPA fspa = this.getFspaFromCp(i);
                buf.append(fspa.toString());
            }
            catch (Exception exc) {
                buf.append(exc.getMessage());
            }
            buf.append("\n");
        }
        buf.append("[/FSPA PLC]");
        return buf.toString();
    }
}
