// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hwpf.model;

import org.apache.poi.util.Internal;

@Internal
public enum SubdocumentType
{
    MAIN, 
    FOOTNOTE, 
    HEADER, 
    @Deprecated
    MACRO, 
    ANNOTATION, 
    ENDNOTE, 
    TEXTBOX, 
    HEADER_TEXTBOX;
    
    public static final SubdocumentType[] ORDERED;
    
    static {
        ORDERED = new SubdocumentType[] { SubdocumentType.MAIN, SubdocumentType.FOOTNOTE, SubdocumentType.HEADER, SubdocumentType.MACRO, SubdocumentType.ANNOTATION, SubdocumentType.ENDNOTE, SubdocumentType.TEXTBOX, SubdocumentType.HEADER_TEXTBOX };
    }
}
