// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hwpf.model;

import java.io.IOException;
import org.apache.poi.hwpf.model.io.HWPFOutputStream;
import java.util.Collections;
import java.util.Arrays;
import java.util.List;
import org.apache.poi.util.Internal;

@Internal
public final class SavedByTable
{
    private SavedByEntry[] entries;
    
    public SavedByTable(final byte[] tableStream, final int offset, final int size) {
        final String[] strings = SttbUtils.readSttbSavedBy(tableStream, offset);
        final int numEntries = strings.length / 2;
        this.entries = new SavedByEntry[numEntries];
        for (int i = 0; i < numEntries; ++i) {
            this.entries[i] = new SavedByEntry(strings[i * 2], strings[i * 2 + 1]);
        }
    }
    
    public List<SavedByEntry> getEntries() {
        return Collections.unmodifiableList((List<? extends SavedByEntry>)Arrays.asList((T[])this.entries));
    }
    
    public void writeTo(final HWPFOutputStream tableStream) throws IOException {
        final String[] toSave = new String[this.entries.length * 2];
        int counter = 0;
        for (final SavedByEntry entry : this.entries) {
            toSave[counter++] = entry.getUserName();
            toSave[counter++] = entry.getSaveLocation();
        }
        SttbUtils.writeSttbSavedBy(toSave, tableStream);
    }
}
