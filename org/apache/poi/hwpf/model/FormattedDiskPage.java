// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hwpf.model;

import org.apache.poi.util.LittleEndian;
import org.apache.poi.util.Internal;

@Internal
public abstract class FormattedDiskPage
{
    protected byte[] _fkp;
    protected int _crun;
    protected int _offset;
    
    public FormattedDiskPage() {
    }
    
    public FormattedDiskPage(final byte[] documentStream, final int offset) {
        this._crun = LittleEndian.getUnsignedByte(documentStream, offset + 511);
        this._fkp = documentStream;
        this._offset = offset;
    }
    
    protected int getStart(final int index) {
        return LittleEndian.getInt(this._fkp, this._offset + index * 4);
    }
    
    protected int getEnd(final int index) {
        return LittleEndian.getInt(this._fkp, this._offset + (index + 1) * 4);
    }
    
    public int size() {
        return this._crun;
    }
    
    protected abstract byte[] getGrpprl(final int p0);
}
