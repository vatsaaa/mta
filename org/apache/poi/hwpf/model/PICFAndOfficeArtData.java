// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hwpf.model;

import java.util.LinkedList;
import org.apache.poi.ddf.EscherRecordFactory;
import org.apache.poi.ddf.DefaultEscherRecordFactory;
import org.apache.poi.util.LittleEndian;
import org.apache.poi.ddf.EscherContainerRecord;
import org.apache.poi.ddf.EscherRecord;
import java.util.List;
import org.apache.poi.util.Internal;

@Internal
public class PICFAndOfficeArtData
{
    private List<EscherRecord> _blipRecords;
    private short _cchPicName;
    private PICF _picf;
    private EscherContainerRecord _shape;
    private byte[] _stPicName;
    
    public PICFAndOfficeArtData(final byte[] dataStream, final int startOffset) {
        int offset = startOffset;
        this._picf = new PICF(dataStream, offset);
        offset += PICF.getSize();
        if (this._picf.getMm() == 102) {
            this._cchPicName = LittleEndian.getUByte(dataStream, offset);
            ++offset;
            this._stPicName = LittleEndian.getByteArray(dataStream, offset, this._cchPicName);
            offset += this._cchPicName;
        }
        final DefaultEscherRecordFactory escherRecordFactory = new DefaultEscherRecordFactory();
        this._shape = new EscherContainerRecord();
        final int recordSize = this._shape.fillFields(dataStream, offset, escherRecordFactory);
        offset += recordSize;
        this._blipRecords = new LinkedList<EscherRecord>();
        while (offset - startOffset < this._picf.getLcb()) {
            final EscherRecord nextRecord = escherRecordFactory.createRecord(dataStream, offset);
            if (nextRecord.getRecordId() != -4089) {
                if (nextRecord.getRecordId() < -4072) {
                    break;
                }
                if (nextRecord.getRecordId() > -3817) {
                    break;
                }
            }
            final int blipRecordSize = nextRecord.fillFields(dataStream, offset, escherRecordFactory);
            offset += blipRecordSize;
            this._blipRecords.add(nextRecord);
        }
    }
    
    public List<EscherRecord> getBlipRecords() {
        return this._blipRecords;
    }
    
    public PICF getPicf() {
        return this._picf;
    }
    
    public EscherContainerRecord getShape() {
        return this._shape;
    }
    
    public byte[] getStPicName() {
        return this._stPicName;
    }
}
