// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hwpf.model;

import org.apache.poi.util.BitFieldFactory;
import org.apache.poi.util.LittleEndian;
import org.apache.poi.util.BitField;
import org.apache.poi.util.Internal;

@Internal
public final class PieceDescriptor
{
    short descriptor;
    private static BitField fNoParaLast;
    private static BitField fPaphNil;
    private static BitField fCopied;
    int fc;
    PropertyModifier prm;
    boolean unicode;
    
    public PieceDescriptor(final byte[] buf, int offset) {
        this.descriptor = LittleEndian.getShort(buf, offset);
        offset += 2;
        this.fc = LittleEndian.getInt(buf, offset);
        offset += 4;
        this.prm = new PropertyModifier(LittleEndian.getShort(buf, offset));
        if ((this.fc & 0x40000000) == 0x0) {
            this.unicode = true;
        }
        else {
            this.unicode = false;
            this.fc &= 0xBFFFFFFF;
            this.fc /= 2;
        }
    }
    
    public int getFilePosition() {
        return this.fc;
    }
    
    public void setFilePosition(final int pos) {
        this.fc = pos;
    }
    
    public boolean isUnicode() {
        return this.unicode;
    }
    
    public PropertyModifier getPrm() {
        return this.prm;
    }
    
    protected byte[] toByteArray() {
        int tempFc = this.fc;
        if (!this.unicode) {
            tempFc *= 2;
            tempFc |= 0x40000000;
        }
        int offset = 0;
        final byte[] buf = new byte[8];
        LittleEndian.putShort(buf, offset, this.descriptor);
        offset += 2;
        LittleEndian.putInt(buf, offset, tempFc);
        offset += 4;
        LittleEndian.putShort(buf, offset, this.prm.getValue());
        return buf;
    }
    
    public static int getSizeInBytes() {
        return 8;
    }
    
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = 31 * result + this.descriptor;
        result = 31 * result + ((this.prm == null) ? 0 : this.prm.hashCode());
        result = 31 * result + (this.unicode ? 1231 : 1237);
        return result;
    }
    
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        final PieceDescriptor other = (PieceDescriptor)obj;
        if (this.descriptor != other.descriptor) {
            return false;
        }
        if (this.prm == null) {
            if (other.prm != null) {
                return false;
            }
        }
        else if (!this.prm.equals(other.prm)) {
            return false;
        }
        return this.unicode == other.unicode;
    }
    
    @Override
    public String toString() {
        return "PieceDescriptor (pos: " + this.getFilePosition() + "; " + (this.isUnicode() ? "unicode" : "non-unicode") + "; prm: " + this.getPrm() + ")";
    }
    
    static {
        PieceDescriptor.fNoParaLast = BitFieldFactory.getInstance(1);
        PieceDescriptor.fPaphNil = BitFieldFactory.getInstance(2);
        PieceDescriptor.fCopied = BitFieldFactory.getInstance(4);
    }
}
