// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hwpf.model;

import org.apache.poi.util.BitField;
import org.apache.poi.util.Internal;

@Internal
public final class PropertyModifier implements Cloneable
{
    private static BitField _fComplex;
    private static BitField _figrpprl;
    private static BitField _fisprm;
    private static BitField _fval;
    private short value;
    
    public PropertyModifier(final short value) {
        this.value = value;
    }
    
    @Override
    protected PropertyModifier clone() throws CloneNotSupportedException {
        return new PropertyModifier(this.value);
    }
    
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        final PropertyModifier other = (PropertyModifier)obj;
        return this.value == other.value;
    }
    
    public short getIgrpprl() {
        if (!this.isComplex()) {
            throw new IllegalStateException("Not complex");
        }
        return PropertyModifier._figrpprl.getShortValue(this.value);
    }
    
    public short getIsprm() {
        if (this.isComplex()) {
            throw new IllegalStateException("Not simple");
        }
        return PropertyModifier._fisprm.getShortValue(this.value);
    }
    
    public short getVal() {
        if (this.isComplex()) {
            throw new IllegalStateException("Not simple");
        }
        return PropertyModifier._fval.getShortValue(this.value);
    }
    
    public short getValue() {
        return this.value;
    }
    
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = 31 * result + this.value;
        return result;
    }
    
    public boolean isComplex() {
        return PropertyModifier._fComplex.isSet(this.value);
    }
    
    @Override
    public String toString() {
        final StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("[PRM] (complex: ");
        stringBuilder.append(this.isComplex());
        stringBuilder.append("; ");
        if (this.isComplex()) {
            stringBuilder.append("igrpprl: ");
            stringBuilder.append(this.getIgrpprl());
            stringBuilder.append("; ");
        }
        else {
            stringBuilder.append("isprm: ");
            stringBuilder.append(this.getIsprm());
            stringBuilder.append("; ");
            stringBuilder.append("val: ");
            stringBuilder.append(this.getVal());
            stringBuilder.append("; ");
        }
        stringBuilder.append(")");
        return stringBuilder.toString();
    }
    
    static {
        PropertyModifier._fComplex = new BitField(1);
        PropertyModifier._figrpprl = new BitField(65534);
        PropertyModifier._fisprm = new BitField(254);
        PropertyModifier._fval = new BitField(65280);
    }
}
