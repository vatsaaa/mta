// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hwpf.model;

import org.apache.poi.util.Internal;
import org.apache.poi.hwpf.model.types.LVLFAbstractType;

@Internal
class LVLF extends LVLFAbstractType
{
    public LVLF() {
    }
    
    public LVLF(final byte[] std, final int offset) {
        this.fillFields(std, offset);
    }
}
