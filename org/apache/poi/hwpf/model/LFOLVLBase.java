// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hwpf.model;

import org.apache.poi.hwpf.model.types.LFOLVLBaseAbstractType;

class LFOLVLBase extends LFOLVLBaseAbstractType
{
    LFOLVLBase() {
    }
    
    LFOLVLBase(final byte[] buf, final int offset) {
        this.fillFields(buf, offset);
    }
}
