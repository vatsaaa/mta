// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hwpf.model;

import org.apache.poi.util.Internal;
import org.apache.poi.hwpf.model.types.LFOAbstractType;

@Internal
class LFO extends LFOAbstractType
{
    public LFO() {
    }
    
    public LFO(final byte[] std, final int offset) {
        this.fillFields(std, offset);
    }
}
