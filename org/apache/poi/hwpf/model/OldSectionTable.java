// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hwpf.model;

import java.util.Comparator;
import java.util.List;
import java.util.Collections;
import org.apache.poi.util.LittleEndian;
import org.apache.poi.util.Internal;

@Internal
public final class OldSectionTable extends SectionTable
{
    @Deprecated
    public OldSectionTable(final byte[] documentStream, final int offset, final int size, final int fcMin, final TextPieceTable tpt) {
        this(documentStream, offset, size);
    }
    
    public OldSectionTable(final byte[] documentStream, final int offset, final int size) {
        final PlexOfCps sedPlex = new PlexOfCps(documentStream, offset, size, 12);
        for (int length = sedPlex.length(), x = 0; x < length; ++x) {
            final GenericPropertyNode node = sedPlex.getProperty(x);
            final SectionDescriptor sed = new SectionDescriptor(node.getBytes(), 0);
            int fileOffset = sed.getFc();
            final int startAt = node.getStart();
            final int endAt = node.getEnd();
            SEPX sepx;
            if (fileOffset == -1) {
                sepx = new SEPX(sed, startAt, endAt, new byte[0]);
            }
            else {
                final int sepxSize = LittleEndian.getShort(documentStream, fileOffset);
                final byte[] buf = new byte[sepxSize + 2];
                fileOffset += 2;
                System.arraycopy(documentStream, fileOffset, buf, 0, buf.length);
                sepx = new SEPX(sed, startAt, endAt, buf);
            }
            this._sections.add(sepx);
        }
        Collections.sort(this._sections, PropertyNode.StartComparator.instance);
    }
}
