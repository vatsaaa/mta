// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hwpf.model;

import org.apache.poi.util.POILogFactory;
import java.io.IOException;
import org.apache.poi.hwpf.model.io.HWPFOutputStream;
import org.apache.poi.hwpf.model.io.HWPFFileSystem;
import org.apache.poi.util.LittleEndian;
import org.apache.poi.util.POILogger;
import org.apache.poi.util.Internal;

@Internal
public final class FontTable
{
    private static final POILogger _logger;
    private short _stringCount;
    private short _extraDataSz;
    private int lcbSttbfffn;
    private int fcSttbfffn;
    private Ffn[] _fontNames;
    
    public FontTable(final byte[] buf, int offset, final int lcbSttbfffn) {
        this._fontNames = null;
        this.lcbSttbfffn = lcbSttbfffn;
        this.fcSttbfffn = offset;
        this._stringCount = LittleEndian.getShort(buf, offset);
        offset += 2;
        this._extraDataSz = LittleEndian.getShort(buf, offset);
        offset += 2;
        this._fontNames = new Ffn[this._stringCount];
        for (int i = 0; i < this._stringCount; ++i) {
            this._fontNames[i] = new Ffn(buf, offset);
            offset += this._fontNames[i].getSize();
        }
    }
    
    public short getStringCount() {
        return this._stringCount;
    }
    
    public short getExtraDataSz() {
        return this._extraDataSz;
    }
    
    public Ffn[] getFontNames() {
        return this._fontNames;
    }
    
    public int getSize() {
        return this.lcbSttbfffn;
    }
    
    public String getMainFont(final int chpFtc) {
        if (chpFtc >= this._stringCount) {
            FontTable._logger.log(POILogger.INFO, "Mismatch in chpFtc with stringCount");
            return null;
        }
        return this._fontNames[chpFtc].getMainFontName();
    }
    
    public String getAltFont(final int chpFtc) {
        if (chpFtc >= this._stringCount) {
            FontTable._logger.log(POILogger.INFO, "Mismatch in chpFtc with stringCount");
            return null;
        }
        return this._fontNames[chpFtc].getAltFontName();
    }
    
    public void setStringCount(final short stringCount) {
        this._stringCount = stringCount;
    }
    
    @Deprecated
    public void writeTo(final HWPFFileSystem sys) throws IOException {
        final HWPFOutputStream tableStream = sys.getStream("1Table");
        this.writeTo(tableStream);
    }
    
    public void writeTo(final HWPFOutputStream tableStream) throws IOException {
        final byte[] buf = new byte[2];
        LittleEndian.putShort(buf, this._stringCount);
        tableStream.write(buf);
        LittleEndian.putShort(buf, this._extraDataSz);
        tableStream.write(buf);
        for (int i = 0; i < this._fontNames.length; ++i) {
            tableStream.write(this._fontNames[i].toByteArray());
        }
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean retVal = true;
        if (((FontTable)o).getStringCount() == this._stringCount) {
            if (((FontTable)o).getExtraDataSz() == this._extraDataSz) {
                final Ffn[] fontNamesNew = ((FontTable)o).getFontNames();
                for (int i = 0; i < this._stringCount; ++i) {
                    if (!this._fontNames[i].equals(fontNamesNew[i])) {
                        retVal = false;
                    }
                }
            }
            else {
                retVal = false;
            }
        }
        else {
            retVal = false;
        }
        return retVal;
    }
    
    static {
        _logger = POILogFactory.getLogger(FontTable.class);
    }
}
