// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hwpf.model;

import org.apache.poi.util.LittleEndian;
import org.apache.poi.util.Internal;

@Internal
public final class SectionDescriptor
{
    private short fn;
    private int fcSepx;
    private short fnMpr;
    private int fcMpr;
    
    public SectionDescriptor() {
    }
    
    public SectionDescriptor(final byte[] buf, int offset) {
        this.fn = LittleEndian.getShort(buf, offset);
        offset += 2;
        this.fcSepx = LittleEndian.getInt(buf, offset);
        offset += 4;
        this.fnMpr = LittleEndian.getShort(buf, offset);
        offset += 2;
        this.fcMpr = LittleEndian.getInt(buf, offset);
    }
    
    public int getFc() {
        return this.fcSepx;
    }
    
    public void setFc(final int fc) {
        this.fcSepx = fc;
    }
    
    @Override
    public boolean equals(final Object o) {
        final SectionDescriptor sed = (SectionDescriptor)o;
        return sed.fn == this.fn && sed.fnMpr == this.fnMpr;
    }
    
    public byte[] toByteArray() {
        int offset = 0;
        final byte[] buf = new byte[12];
        LittleEndian.putShort(buf, offset, this.fn);
        offset += 2;
        LittleEndian.putInt(buf, offset, this.fcSepx);
        offset += 4;
        LittleEndian.putShort(buf, offset, this.fnMpr);
        offset += 2;
        LittleEndian.putInt(buf, offset, this.fcMpr);
        return buf;
    }
    
    @Override
    public String toString() {
        return "[SED] (fn: " + this.fn + "; fcSepx: " + this.fcSepx + "; fnMpr: " + this.fnMpr + "; fcMpr: " + this.fcMpr + ")";
    }
}
