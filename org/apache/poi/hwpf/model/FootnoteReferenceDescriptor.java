// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hwpf.model;

import org.apache.poi.util.Internal;
import org.apache.poi.hwpf.model.types.FRDAbstractType;

@Internal
public final class FootnoteReferenceDescriptor extends FRDAbstractType implements Cloneable
{
    public FootnoteReferenceDescriptor() {
    }
    
    public FootnoteReferenceDescriptor(final byte[] data, final int offset) {
        this.fillFields(data, offset);
    }
    
    @Override
    protected FootnoteReferenceDescriptor clone() {
        try {
            return (FootnoteReferenceDescriptor)super.clone();
        }
        catch (CloneNotSupportedException e) {
            throw new RuntimeException(e);
        }
    }
    
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        final FootnoteReferenceDescriptor other = (FootnoteReferenceDescriptor)obj;
        return this.field_1_nAuto == other.field_1_nAuto;
    }
    
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = 31 * result + this.field_1_nAuto;
        return result;
    }
    
    public boolean isEmpty() {
        return this.field_1_nAuto == 0;
    }
    
    @Override
    public String toString() {
        if (this.isEmpty()) {
            return "[FRD] EMPTY";
        }
        return super.toString();
    }
}
