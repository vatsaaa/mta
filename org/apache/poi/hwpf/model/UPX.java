// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hwpf.model;

import java.util.Arrays;
import org.apache.poi.util.Internal;

@Internal
public final class UPX
{
    private byte[] _upx;
    
    public UPX(final byte[] upx) {
        this._upx = upx;
    }
    
    public byte[] getUPX() {
        return this._upx;
    }
    
    public int size() {
        return this._upx.length;
    }
    
    @Override
    public boolean equals(final Object o) {
        final UPX upx = (UPX)o;
        return Arrays.equals(this._upx, upx._upx);
    }
    
    @Override
    public String toString() {
        return "[UPX] " + Arrays.toString(this._upx);
    }
}
