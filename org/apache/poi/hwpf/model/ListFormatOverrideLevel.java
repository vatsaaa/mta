// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hwpf.model;

import org.apache.poi.hwpf.model.types.LFOLVLBaseAbstractType;
import org.apache.poi.util.Internal;

@Internal
public final class ListFormatOverrideLevel
{
    private LFOLVLBase _base;
    private ListLevel _lvl;
    
    public ListFormatOverrideLevel(final byte[] buf, int offset) {
        this._base = new LFOLVLBase(buf, offset);
        offset += LFOLVLBase.getSize();
        if (this._base.isFFormatting()) {
            this._lvl = new ListLevel(buf, offset);
        }
    }
    
    @Override
    public boolean equals(final Object obj) {
        if (obj == null) {
            return false;
        }
        final ListFormatOverrideLevel lfolvl = (ListFormatOverrideLevel)obj;
        boolean lvlEquality = false;
        if (this._lvl != null) {
            lvlEquality = this._lvl.equals(lfolvl._lvl);
        }
        else {
            lvlEquality = (lfolvl._lvl == null);
        }
        return lvlEquality && lfolvl._base.equals(this._base);
    }
    
    public int getIStartAt() {
        return this._base.getIStartAt();
    }
    
    public ListLevel getLevel() {
        return this._lvl;
    }
    
    public int getLevelNum() {
        return this._base.getILvl();
    }
    
    public int getSizeInBytes() {
        return (this._lvl == null) ? LFOLVLBaseAbstractType.getSize() : (LFOLVLBase.getSize() + this._lvl.getSizeInBytes());
    }
    
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = 31 * result + this._base.hashCode();
        result = 31 * result + ((this._lvl != null) ? this._lvl.hashCode() : 0);
        return result;
    }
    
    public boolean isFormatting() {
        return this._base.isFFormatting();
    }
    
    public boolean isStartAt() {
        return this._base.isFStartAt();
    }
    
    public byte[] toByteArray() {
        int offset = 0;
        final byte[] buf = new byte[this.getSizeInBytes()];
        this._base.serialize(buf, offset);
        offset += LFOLVLBase.getSize();
        if (this._lvl != null) {
            final byte[] levelBuf = this._lvl.toByteArray();
            System.arraycopy(levelBuf, 0, buf, offset, levelBuf.length);
        }
        return buf;
    }
}
