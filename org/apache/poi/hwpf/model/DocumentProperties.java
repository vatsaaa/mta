// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hwpf.model;

import java.io.IOException;
import org.apache.poi.hwpf.model.io.HWPFOutputStream;
import org.apache.poi.util.LittleEndian;
import org.apache.poi.util.Internal;
import org.apache.poi.hwpf.model.types.DOPAbstractType;

@Internal
public final class DocumentProperties extends DOPAbstractType
{
    private byte[] _preserved;
    
    @Deprecated
    public DocumentProperties(final byte[] tableStream, final int offset) {
        this(tableStream, offset, DOPAbstractType.getSize());
    }
    
    public DocumentProperties(final byte[] tableStream, final int offset, final int length) {
        super.fillFields(tableStream, offset);
        final int supportedSize = DOPAbstractType.getSize();
        if (length != supportedSize) {
            this._preserved = LittleEndian.getByteArray(tableStream, offset + supportedSize, length - supportedSize);
        }
        else {
            this._preserved = new byte[0];
        }
    }
    
    @Override
    public void serialize(final byte[] data, final int offset) {
        super.serialize(data, offset);
    }
    
    public void writeTo(final HWPFOutputStream tableStream) throws IOException {
        final byte[] supported = new byte[DOPAbstractType.getSize()];
        this.serialize(supported, 0);
        tableStream.write(supported);
        tableStream.write(this._preserved);
    }
}
