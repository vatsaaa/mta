// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hwpf.model;

import java.util.Comparator;
import org.apache.poi.util.POILogFactory;
import java.util.Arrays;
import org.apache.poi.util.POILogger;
import org.apache.poi.util.Internal;

@Internal
public abstract class PropertyNode<T extends PropertyNode<T>> implements Comparable<T>, Cloneable
{
    private static final POILogger _logger;
    protected Object _buf;
    private int _cpStart;
    private int _cpEnd;
    
    protected PropertyNode(final int fcStart, final int fcEnd, final Object buf) {
        this._cpStart = fcStart;
        this._cpEnd = fcEnd;
        this._buf = buf;
        if (this._cpStart < 0) {
            PropertyNode._logger.log(POILogger.WARN, "A property claimed to start before zero, at " + this._cpStart + "! Resetting it to zero, and hoping for the best");
            this._cpStart = 0;
        }
        if (this._cpEnd < this._cpStart) {
            PropertyNode._logger.log(POILogger.WARN, "A property claimed to end (" + this._cpEnd + ") before start! " + "Resetting end to start, and hoping for the best");
            this._cpEnd = this._cpStart;
        }
    }
    
    public int getStart() {
        return this._cpStart;
    }
    
    public void setStart(final int start) {
        this._cpStart = start;
    }
    
    public int getEnd() {
        return this._cpEnd;
    }
    
    public void setEnd(final int end) {
        this._cpEnd = end;
    }
    
    public void adjustForDelete(final int start, final int length) {
        final int end = start + length;
        if (this._cpEnd > start) {
            if (this._cpStart < end) {
                this._cpEnd = ((end >= this._cpEnd) ? start : (this._cpEnd - length));
                this._cpStart = Math.min(start, this._cpStart);
            }
            else {
                this._cpEnd -= length;
                this._cpStart -= length;
            }
        }
    }
    
    protected boolean limitsAreEqual(final Object o) {
        return ((PropertyNode)o).getStart() == this._cpStart && ((PropertyNode)o).getEnd() == this._cpEnd;
    }
    
    @Override
    public int hashCode() {
        return this._cpStart * 31 + this._buf.hashCode();
    }
    
    @Override
    public boolean equals(final Object o) {
        if (!this.limitsAreEqual(o)) {
            return false;
        }
        final Object testBuf = ((PropertyNode)o)._buf;
        if (testBuf instanceof byte[] && this._buf instanceof byte[]) {
            return Arrays.equals((byte[])testBuf, (byte[])this._buf);
        }
        return this._buf.equals(testBuf);
    }
    
    public T clone() throws CloneNotSupportedException {
        return (T)super.clone();
    }
    
    public int compareTo(final T o) {
        final int cpEnd = o.getEnd();
        if (this._cpEnd == cpEnd) {
            return 0;
        }
        if (this._cpEnd < cpEnd) {
            return -1;
        }
        return 1;
    }
    
    static {
        _logger = POILogFactory.getLogger(PropertyNode.class);
    }
    
    public static final class EndComparator implements Comparator<PropertyNode<?>>
    {
        public static EndComparator instance;
        
        public int compare(final PropertyNode<?> o1, final PropertyNode<?> o2) {
            final int thisVal = o1.getEnd();
            final int anotherVal = o2.getEnd();
            return (thisVal < anotherVal) ? -1 : ((thisVal == anotherVal) ? 0 : 1);
        }
        
        static {
            EndComparator.instance = new EndComparator();
        }
    }
    
    public static final class StartComparator implements Comparator<PropertyNode<?>>
    {
        public static StartComparator instance;
        
        public int compare(final PropertyNode<?> o1, final PropertyNode<?> o2) {
            final int thisVal = o1.getStart();
            final int anotherVal = o2.getStart();
            return (thisVal < anotherVal) ? -1 : ((thisVal == anotherVal) ? 0 : 1);
        }
        
        static {
            StartComparator.instance = new StartComparator();
        }
    }
}
