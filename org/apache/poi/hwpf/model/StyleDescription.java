// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hwpf.model;

import org.apache.poi.util.POILogFactory;
import java.util.Arrays;
import java.io.UnsupportedEncodingException;
import org.apache.poi.util.LittleEndian;
import org.apache.poi.hwpf.usermodel.CharacterProperties;
import org.apache.poi.hwpf.usermodel.ParagraphProperties;
import org.apache.poi.util.POILogger;
import org.apache.poi.util.Internal;

@Internal
public final class StyleDescription implements HDFType
{
    private static final POILogger logger;
    private static final int PARAGRAPH_STYLE = 1;
    private static final int CHARACTER_STYLE = 2;
    private static final int TABLE_STYLE = 3;
    private static final int NUMBERING_STYLE = 4;
    private int _baseLength;
    private StdfBase _stdfBase;
    private StdfPost2000 _stdfPost2000;
    UPX[] _upxs;
    String _name;
    @Deprecated
    ParagraphProperties _pap;
    @Deprecated
    CharacterProperties _chp;
    
    public StyleDescription() {
    }
    
    public StyleDescription(final byte[] std, final int baseLength, int offset, final boolean word9) {
        this._baseLength = baseLength;
        int nameStart = offset + baseLength;
        boolean readStdfPost2000 = false;
        if (baseLength == 18) {
            readStdfPost2000 = true;
        }
        else if (baseLength == 10) {
            readStdfPost2000 = false;
        }
        else {
            StyleDescription.logger.log(POILogger.WARN, "Style definition has non-standard size of ", baseLength);
        }
        this._stdfBase = new StdfBase(std, offset);
        offset += StdfBase.getSize();
        if (readStdfPost2000) {
            this._stdfPost2000 = new StdfPost2000(std, offset);
            offset += StdfPost2000.getSize();
        }
        int nameLength = 0;
        int multiplier = 1;
        if (word9) {
            nameLength = LittleEndian.getShort(std, nameStart);
            multiplier = 2;
            nameStart += 2;
        }
        else {
            nameLength = std[nameStart];
        }
        try {
            this._name = new String(std, nameStart, nameLength * multiplier, "UTF-16LE");
        }
        catch (UnsupportedEncodingException ex) {}
        int varOffset;
        final int grupxStart = varOffset = (nameLength + 1) * multiplier + nameStart;
        final int countOfUPX = this._stdfBase.getCupx();
        this._upxs = new UPX[countOfUPX];
        for (int x = 0; x < countOfUPX; ++x) {
            final int upxSize = LittleEndian.getShort(std, varOffset);
            varOffset += 2;
            final byte[] upx = new byte[upxSize];
            System.arraycopy(std, varOffset, upx, 0, upxSize);
            this._upxs[x] = new UPX(upx);
            varOffset += upxSize;
            if (upxSize % 2 == 1) {
                ++varOffset;
            }
        }
    }
    
    public int getBaseStyle() {
        return this._stdfBase.getIstdBase();
    }
    
    public byte[] getCHPX() {
        switch (this._stdfBase.getStk()) {
            case 1: {
                if (this._upxs.length > 1) {
                    return this._upxs[1].getUPX();
                }
                return null;
            }
            case 2: {
                return this._upxs[0].getUPX();
            }
            default: {
                return null;
            }
        }
    }
    
    public byte[] getPAPX() {
        switch (this._stdfBase.getStk()) {
            case 1: {
                return this._upxs[0].getUPX();
            }
            default: {
                return null;
            }
        }
    }
    
    @Deprecated
    public ParagraphProperties getPAP() {
        return this._pap;
    }
    
    @Deprecated
    public CharacterProperties getCHP() {
        return this._chp;
    }
    
    @Deprecated
    void setPAP(final ParagraphProperties pap) {
        this._pap = pap;
    }
    
    @Deprecated
    void setCHP(final CharacterProperties chp) {
        this._chp = chp;
    }
    
    public String getName() {
        return this._name;
    }
    
    public byte[] toByteArray() {
        int size = this._baseLength + 2 + (this._name.length() + 1) * 2;
        size += this._upxs[0].size() + 2;
        for (int x = 1; x < this._upxs.length; ++x) {
            size += this._upxs[x - 1].size() % 2;
            size += this._upxs[x].size() + 2;
        }
        final byte[] buf = new byte[size];
        this._stdfBase.serialize(buf, 0);
        int offset = this._baseLength;
        final char[] letters = this._name.toCharArray();
        LittleEndian.putShort(buf, this._baseLength, (short)letters.length);
        offset += 2;
        for (int x2 = 0; x2 < letters.length; ++x2) {
            LittleEndian.putShort(buf, offset, (short)letters[x2]);
            offset += 2;
        }
        offset += 2;
        for (int x2 = 0; x2 < this._upxs.length; ++x2) {
            final short upxSize = (short)this._upxs[x2].size();
            LittleEndian.putShort(buf, offset, upxSize);
            offset += 2;
            System.arraycopy(this._upxs[x2].getUPX(), 0, buf, offset, upxSize);
            offset += upxSize + upxSize % 2;
        }
        return buf;
    }
    
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = 31 * result + ((this._name == null) ? 0 : this._name.hashCode());
        result = 31 * result + ((this._stdfBase == null) ? 0 : this._stdfBase.hashCode());
        result = 31 * result + Arrays.hashCode(this._upxs);
        return result;
    }
    
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        final StyleDescription other = (StyleDescription)obj;
        if (this._name == null) {
            if (other._name != null) {
                return false;
            }
        }
        else if (!this._name.equals(other._name)) {
            return false;
        }
        if (this._stdfBase == null) {
            if (other._stdfBase != null) {
                return false;
            }
        }
        else if (!this._stdfBase.equals(other._stdfBase)) {
            return false;
        }
        return Arrays.equals(this._upxs, other._upxs);
    }
    
    @Override
    public String toString() {
        final StringBuilder result = new StringBuilder();
        result.append("[STD]: '");
        result.append(this._name);
        result.append("'");
        result.append(("\nStdfBase:\t" + this._stdfBase).replaceAll("\n", "\n    "));
        result.append(("\nStdfPost2000:\t" + this._stdfPost2000).replaceAll("\n", "\n    "));
        for (final UPX upx : this._upxs) {
            result.append(("\nUPX:\t" + upx).replaceAll("\n", "\n    "));
        }
        return result.toString();
    }
    
    static {
        logger = POILogFactory.getLogger(StyleDescription.class);
    }
}
