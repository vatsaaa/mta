// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hwpf.model;

import org.apache.poi.hwpf.model.types.LSTFAbstractType;

class LSTF extends LSTFAbstractType
{
    LSTF() {
    }
    
    LSTF(final byte[] buf, final int offset) {
        this.fillFields(buf, offset);
    }
}
