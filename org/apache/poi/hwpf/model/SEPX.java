// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hwpf.model;

import org.apache.poi.hwpf.sprm.SectionSprmUncompressor;
import org.apache.poi.hwpf.sprm.SectionSprmCompressor;
import org.apache.poi.hwpf.sprm.SprmBuffer;
import org.apache.poi.hwpf.usermodel.SectionProperties;
import org.apache.poi.util.Internal;

@Internal
public final class SEPX extends PropertyNode<SEPX>
{
    SectionProperties sectionProperties;
    SectionDescriptor _sed;
    
    public SEPX(final SectionDescriptor sed, final int start, final int end, final byte[] grpprl) {
        super(start, end, new SprmBuffer(grpprl, 0));
        this._sed = sed;
    }
    
    public byte[] getGrpprl() {
        if (this.sectionProperties != null) {
            final byte[] grpprl = SectionSprmCompressor.compressSectionProperty(this.sectionProperties);
            this._buf = new SprmBuffer(grpprl, 0);
        }
        return ((SprmBuffer)this._buf).toByteArray();
    }
    
    public SectionDescriptor getSectionDescriptor() {
        return this._sed;
    }
    
    public SectionProperties getSectionProperties() {
        if (this.sectionProperties == null) {
            this.sectionProperties = SectionSprmUncompressor.uncompressSEP(((SprmBuffer)this._buf).toByteArray(), 0);
        }
        return this.sectionProperties;
    }
    
    @Override
    public boolean equals(final Object o) {
        final SEPX sepx = (SEPX)o;
        return super.equals(o) && sepx._sed.equals(this._sed);
    }
    
    @Override
    public String toString() {
        return "SEPX from " + this.getStart() + " to " + this.getEnd();
    }
}
