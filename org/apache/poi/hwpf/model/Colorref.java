// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hwpf.model;

import org.apache.poi.util.LittleEndian;
import org.apache.poi.util.Internal;

@Internal
public class Colorref implements Cloneable
{
    private int value;
    
    public static Colorref valueOfIco(final int ico) {
        switch (ico) {
            case 1: {
                return new Colorref(0);
            }
            case 2: {
                return new Colorref(16711680);
            }
            case 3: {
                return new Colorref(16776960);
            }
            case 4: {
                return new Colorref(65280);
            }
            case 5: {
                return new Colorref(16711935);
            }
            case 6: {
                return new Colorref(255);
            }
            case 7: {
                return new Colorref(65535);
            }
            case 8: {
                return new Colorref(16777215);
            }
            case 9: {
                return new Colorref(9109504);
            }
            case 10: {
                return new Colorref(9145088);
            }
            case 11: {
                return new Colorref(25600);
            }
            case 12: {
                return new Colorref(9109643);
            }
            case 13: {
                return new Colorref(139);
            }
            case 14: {
                return new Colorref(52479);
            }
            case 15: {
                return new Colorref(11119017);
            }
            case 16: {
                return new Colorref(12632256);
            }
            default: {
                return new Colorref(0);
            }
        }
    }
    
    public Colorref() {
        this.value = -1;
    }
    
    public Colorref(final byte[] data, final int offset) {
        this.value = LittleEndian.getInt(data, offset);
    }
    
    public Colorref(final int value) {
        this.value = value;
    }
    
    public Colorref clone() throws CloneNotSupportedException {
        return new Colorref(this.value);
    }
    
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        final Colorref other = (Colorref)obj;
        return this.value == other.value;
    }
    
    public int getValue() {
        return this.value;
    }
    
    @Override
    public int hashCode() {
        return this.value;
    }
    
    public boolean isEmpty() {
        return this.value == -1;
    }
    
    public void serialize(final byte[] data, final int offset) {
        LittleEndian.putInt(data, offset, this.value);
    }
    
    public void setValue(final int value) {
        this.value = value;
    }
    
    public byte[] toByteArray() {
        if (this.isEmpty()) {
            throw new IllegalStateException("Structure state (EMPTY) is not good for serialization");
        }
        final byte[] bs = new byte[4];
        this.serialize(bs, 0);
        return bs;
    }
    
    @Override
    public String toString() {
        if (this.isEmpty()) {
            return "[COLORREF] EMPTY";
        }
        return "[COLORREF] 0x" + Integer.toHexString(this.value);
    }
}
