// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hwpf.model;

import org.apache.poi.util.POILogFactory;
import java.io.IOException;
import org.apache.poi.hwpf.model.io.HWPFOutputStream;
import org.apache.poi.hwpf.model.io.HWPFFileSystem;
import org.apache.poi.hwpf.sprm.SprmOperation;
import org.apache.poi.hwpf.sprm.SprmIterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.IdentityHashMap;
import java.util.Comparator;
import java.util.Collections;
import java.util.Collection;
import java.util.List;
import java.util.Iterator;
import org.apache.poi.hwpf.sprm.SprmBuffer;
import org.apache.poi.util.LittleEndian;
import java.util.ArrayList;
import org.apache.poi.util.POILogger;
import org.apache.poi.util.Internal;

@Internal
public class PAPBinTable
{
    private static final POILogger logger;
    protected ArrayList<PAPX> _paragraphs;
    
    public PAPBinTable() {
        this._paragraphs = new ArrayList<PAPX>();
    }
    
    @Deprecated
    public PAPBinTable(final byte[] documentStream, final byte[] tableStream, final byte[] dataStream, final int offset, final int size, final int fcMin, final TextPieceTable tpt) {
        this(documentStream, tableStream, dataStream, offset, size, tpt);
    }
    
    public PAPBinTable(final byte[] documentStream, final byte[] tableStream, final byte[] dataStream, final int offset, final int size, final CharIndexTranslator charIndexTranslator) {
        this._paragraphs = new ArrayList<PAPX>();
        final long start = System.currentTimeMillis();
        final PlexOfCps binTable = new PlexOfCps(tableStream, offset, size, 4);
        for (int length = binTable.length(), x = 0; x < length; ++x) {
            final GenericPropertyNode node = binTable.getProperty(x);
            final int pageNum = LittleEndian.getInt(node.getBytes());
            final int pageOffset = 512 * pageNum;
            final PAPFormattedDiskPage pfkp = new PAPFormattedDiskPage(documentStream, dataStream, pageOffset, charIndexTranslator);
            for (final PAPX papx : pfkp.getPAPXs()) {
                if (papx != null) {
                    this._paragraphs.add(papx);
                }
            }
        }
        PAPBinTable.logger.log(POILogger.DEBUG, "PAPX tables loaded in ", System.currentTimeMillis() - start, " ms (", this._paragraphs.size(), " elements)");
        if (this._paragraphs.isEmpty()) {
            PAPBinTable.logger.log(POILogger.WARN, "PAPX FKPs are empty");
            this._paragraphs.add(new PAPX(0, 0, new SprmBuffer(2)));
        }
    }
    
    public void rebuild(final StringBuilder docText, final ComplexFileTable complexFileTable) {
        rebuild(docText, complexFileTable, this._paragraphs);
    }
    
    static void rebuild(final StringBuilder docText, final ComplexFileTable complexFileTable, final List<PAPX> paragraphs) {
        long start = System.currentTimeMillis();
        if (complexFileTable != null) {
            final SprmBuffer[] sprmBuffers = complexFileTable.getGrpprls();
            for (final TextPiece textPiece : complexFileTable.getTextPieceTable().getTextPieces()) {
                final PropertyModifier prm = textPiece.getPieceDescriptor().getPrm();
                if (!prm.isComplex()) {
                    continue;
                }
                final int igrpprl = prm.getIgrpprl();
                if (igrpprl < 0 || igrpprl >= sprmBuffers.length) {
                    PAPBinTable.logger.log(POILogger.WARN, textPiece + "'s PRM references to unknown grpprl");
                }
                else {
                    boolean hasPap = false;
                    final SprmBuffer sprmBuffer = sprmBuffers[igrpprl];
                    for (final SprmOperation sprmOperation : sprmBuffer) {
                        if (sprmOperation.getType() == 1) {
                            hasPap = true;
                            break;
                        }
                    }
                    if (!hasPap) {
                        continue;
                    }
                    final SprmBuffer newSprmBuffer = new SprmBuffer(2);
                    newSprmBuffer.append(sprmBuffer.toByteArray());
                    final PAPX papx = new PAPX(textPiece.getStart(), textPiece.getEnd(), newSprmBuffer);
                    paragraphs.add(papx);
                }
            }
            PAPBinTable.logger.log(POILogger.DEBUG, "Merged (?) with PAPX from complex file table in ", System.currentTimeMillis() - start, " ms (", paragraphs.size(), " elements in total)");
            start = System.currentTimeMillis();
        }
        final List<PAPX> oldPapxSortedByEndPos = new ArrayList<PAPX>(paragraphs);
        Collections.sort(oldPapxSortedByEndPos, PropertyNode.EndComparator.instance);
        PAPBinTable.logger.log(POILogger.DEBUG, "PAPX sorted by end position in ", System.currentTimeMillis() - start, " ms");
        start = System.currentTimeMillis();
        final Map<PAPX, Integer> papxToFileOrder = new IdentityHashMap<PAPX, Integer>();
        int counter = 0;
        for (final PAPX papx2 : paragraphs) {
            papxToFileOrder.put(papx2, counter++);
        }
        final Comparator<PAPX> papxFileOrderComparator = new Comparator<PAPX>() {
            public int compare(final PAPX o1, final PAPX o2) {
                final Integer i1 = papxToFileOrder.get(o1);
                final Integer i2 = papxToFileOrder.get(o2);
                return i1.compareTo(i2);
            }
        };
        PAPBinTable.logger.log(POILogger.DEBUG, "PAPX's order map created in ", System.currentTimeMillis() - start, " ms");
        start = System.currentTimeMillis();
        final List<PAPX> newPapxs = new LinkedList<PAPX>();
        int lastParStart = 0;
        int lastPapxIndex = 0;
        for (int charIndex = 0; charIndex < docText.length(); ++charIndex) {
            final char c = docText.charAt(charIndex);
            if (c == '\r' || c == '\u0007' || c == '\f') {
                final int startInclusive = lastParStart;
                final int endExclusive = charIndex + 1;
                boolean broken = false;
                final List<PAPX> papxs = new LinkedList<PAPX>();
                for (int papxIndex = lastPapxIndex; papxIndex < oldPapxSortedByEndPos.size(); ++papxIndex) {
                    broken = false;
                    final PAPX papx3 = oldPapxSortedByEndPos.get(papxIndex);
                    assert papx3.getEnd() > startInclusive;
                    if (papx3.getEnd() - 1 > charIndex) {
                        lastPapxIndex = papxIndex;
                        broken = true;
                        break;
                    }
                    papxs.add(papx3);
                }
                if (!broken) {
                    lastPapxIndex = oldPapxSortedByEndPos.size() - 1;
                }
                if (papxs.size() == 0) {
                    PAPBinTable.logger.log(POILogger.WARN, "Paragraph [", startInclusive, "; ", endExclusive, ") has no PAPX. Creating new one.");
                    final PAPX papx4 = new PAPX(startInclusive, endExclusive, new SprmBuffer(2));
                    newPapxs.add(papx4);
                    lastParStart = endExclusive;
                }
                else {
                    if (papxs.size() == 1) {
                        final PAPX existing = papxs.get(0);
                        if (existing.getStart() == startInclusive && existing.getEnd() == endExclusive) {
                            newPapxs.add(existing);
                            lastParStart = endExclusive;
                            continue;
                        }
                    }
                    Collections.sort(papxs, papxFileOrderComparator);
                    SprmBuffer sprmBuffer2 = null;
                    for (final PAPX papx5 : papxs) {
                        if (papx5.getGrpprl() != null) {
                            if (papx5.getGrpprl().length == 0) {
                                continue;
                            }
                            if (sprmBuffer2 == null) {
                                try {
                                    sprmBuffer2 = (SprmBuffer)papx5.getSprmBuf().clone();
                                    continue;
                                }
                                catch (CloneNotSupportedException e) {
                                    throw new Error(e);
                                }
                            }
                            sprmBuffer2.append(papx5.getGrpprl(), 2);
                        }
                    }
                    final PAPX newPapx = new PAPX(startInclusive, endExclusive, sprmBuffer2);
                    newPapxs.add(newPapx);
                    lastParStart = endExclusive;
                }
            }
        }
        paragraphs.clear();
        paragraphs.addAll(newPapxs);
        PAPBinTable.logger.log(POILogger.DEBUG, "PAPX rebuilded from document text in ", System.currentTimeMillis() - start, " ms (", paragraphs.size(), " elements)");
        start = System.currentTimeMillis();
    }
    
    public void insert(final int listIndex, final int cpStart, final SprmBuffer buf) {
        final PAPX forInsert = new PAPX(0, 0, buf);
        forInsert.setStart(cpStart);
        forInsert.setEnd(cpStart);
        if (listIndex == this._paragraphs.size()) {
            this._paragraphs.add(forInsert);
        }
        else {
            final PAPX currentPap = this._paragraphs.get(listIndex);
            if (currentPap != null && currentPap.getStart() < cpStart) {
                SprmBuffer clonedBuf = null;
                try {
                    clonedBuf = (SprmBuffer)currentPap.getSprmBuf().clone();
                }
                catch (CloneNotSupportedException exc) {
                    exc.printStackTrace();
                }
                final PAPX clone = new PAPX(0, 0, clonedBuf);
                clone.setStart(cpStart);
                clone.setEnd(currentPap.getEnd());
                currentPap.setEnd(cpStart);
                this._paragraphs.add(listIndex + 1, forInsert);
                this._paragraphs.add(listIndex + 2, clone);
            }
            else {
                this._paragraphs.add(listIndex, forInsert);
            }
        }
    }
    
    public void adjustForDelete(final int listIndex, final int offset, final int length) {
        final int size = this._paragraphs.size();
        final int endMark = offset + length;
        int endIndex = listIndex;
        for (PAPX papx = this._paragraphs.get(endIndex); papx.getEnd() < endMark; papx = this._paragraphs.get(++endIndex)) {}
        if (listIndex == endIndex) {
            final PAPX papx = this._paragraphs.get(endIndex);
            papx.setEnd(papx.getEnd() - endMark + offset);
        }
        else {
            PAPX papx = this._paragraphs.get(listIndex);
            papx.setEnd(offset);
            for (int x = listIndex + 1; x < endIndex; ++x) {
                papx = this._paragraphs.get(x);
                papx.setStart(offset);
                papx.setEnd(offset);
            }
            papx = this._paragraphs.get(endIndex);
            papx.setEnd(papx.getEnd() - endMark + offset);
        }
        for (int x = endIndex + 1; x < size; ++x) {
            final PAPX papx = this._paragraphs.get(x);
            papx.setStart(papx.getStart() - length);
            papx.setEnd(papx.getEnd() - length);
        }
    }
    
    public void adjustForInsert(final int listIndex, final int length) {
        final int size = this._paragraphs.size();
        PAPX papx = this._paragraphs.get(listIndex);
        papx.setEnd(papx.getEnd() + length);
        for (int x = listIndex + 1; x < size; ++x) {
            papx = this._paragraphs.get(x);
            papx.setStart(papx.getStart() + length);
            papx.setEnd(papx.getEnd() + length);
        }
    }
    
    public ArrayList<PAPX> getParagraphs() {
        return this._paragraphs;
    }
    
    @Deprecated
    public void writeTo(final HWPFFileSystem sys, final CharIndexTranslator translator) throws IOException {
        final HWPFOutputStream wordDocumentStream = sys.getStream("WordDocument");
        final HWPFOutputStream tableStream = sys.getStream("1Table");
        this.writeTo(wordDocumentStream, tableStream, translator);
    }
    
    public void writeTo(final HWPFOutputStream wordDocumentStream, final HWPFOutputStream tableStream, final CharIndexTranslator translator) throws IOException {
        final PlexOfCps binTable = new PlexOfCps(4);
        int docOffset = wordDocumentStream.getOffset();
        final int mod = docOffset % 512;
        if (mod != 0) {
            final byte[] padding = new byte[512 - mod];
            wordDocumentStream.write(padding);
        }
        docOffset = wordDocumentStream.getOffset();
        int pageNum = docOffset / 512;
        final int endingFc = translator.getByteIndex(this._paragraphs.get(this._paragraphs.size() - 1).getEnd());
        ArrayList<PAPX> overflow = this._paragraphs;
        do {
            final PAPX startingProp = overflow.get(0);
            final int start = translator.getByteIndex(startingProp.getStart());
            final PAPFormattedDiskPage pfkp = new PAPFormattedDiskPage();
            pfkp.fill(overflow);
            final byte[] bufFkp = pfkp.toByteArray(tableStream, translator);
            wordDocumentStream.write(bufFkp);
            overflow = pfkp.getOverflow();
            int end = endingFc;
            if (overflow != null) {
                end = translator.getByteIndex(overflow.get(0).getStart());
            }
            final byte[] intHolder = new byte[4];
            LittleEndian.putInt(intHolder, pageNum++);
            binTable.addProperty(new GenericPropertyNode(start, end, intHolder));
        } while (overflow != null);
        tableStream.write(binTable.toByteArray());
    }
    
    static {
        logger = POILogFactory.getLogger(PAPBinTable.class);
    }
}
