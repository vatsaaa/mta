// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hwpf.model;

import org.apache.poi.util.Internal;

@Internal
public final class UnhandledDataStructure
{
    byte[] _buf;
    
    public UnhandledDataStructure(final byte[] buf, final int offset, final int length) {
        this._buf = new byte[length];
        if (offset + length > buf.length) {
            throw new IndexOutOfBoundsException("buffer length is " + buf.length + "but code is trying to read " + length + " from offset " + offset);
        }
        System.arraycopy(buf, offset, this._buf, 0, length);
    }
    
    byte[] getBuf() {
        return this._buf;
    }
}
