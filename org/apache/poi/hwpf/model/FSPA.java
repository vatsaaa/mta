// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hwpf.model;

import org.apache.poi.util.Internal;
import org.apache.poi.hwpf.model.types.FSPAAbstractType;

@Internal
public final class FSPA extends FSPAAbstractType
{
    @Deprecated
    public static final int FSPA_SIZE;
    
    public FSPA() {
    }
    
    public FSPA(final byte[] bytes, final int offset) {
        this.fillFields(bytes, offset);
    }
    
    public byte[] toByteArray() {
        final byte[] buf = new byte[FSPA.FSPA_SIZE];
        this.serialize(buf, 0);
        return buf;
    }
    
    static {
        FSPA_SIZE = FSPAAbstractType.getSize();
    }
}
