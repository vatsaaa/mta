// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hwpf.model;

import org.apache.poi.hwpf.sprm.ParagraphSprmUncompressor;
import org.apache.poi.hwpf.usermodel.ParagraphProperties;
import org.apache.poi.util.LittleEndian;
import org.apache.poi.hwpf.sprm.SprmOperation;
import org.apache.poi.hwpf.sprm.SprmBuffer;
import org.apache.poi.util.Internal;

@Internal
public final class PAPX extends BytePropertyNode<PAPX>
{
    private ParagraphHeight _phe;
    
    public PAPX(final int fcStart, final int fcEnd, final CharIndexTranslator translator, final byte[] papx, final ParagraphHeight phe, final byte[] dataStream) {
        super(fcStart, fcEnd, translator, new SprmBuffer(papx, 2));
        this._phe = phe;
        final SprmBuffer buf = this.findHuge(new SprmBuffer(papx, 2), dataStream);
        if (buf != null) {
            this._buf = buf;
        }
    }
    
    public PAPX(final int charStart, final int charEnd, final byte[] papx, final ParagraphHeight phe, final byte[] dataStream) {
        super(charStart, charEnd, new SprmBuffer(papx, 2));
        this._phe = phe;
        final SprmBuffer buf = this.findHuge(new SprmBuffer(papx, 2), dataStream);
        if (buf != null) {
            this._buf = buf;
        }
    }
    
    @Deprecated
    public PAPX(final int fcStart, final int fcEnd, final CharIndexTranslator translator, SprmBuffer buf, final byte[] dataStream) {
        super(fcStart, fcEnd, translator, buf);
        this._phe = new ParagraphHeight();
        buf = this.findHuge(buf, dataStream);
        if (buf != null) {
            this._buf = buf;
        }
    }
    
    public PAPX(final int charStart, final int charEnd, final SprmBuffer buf) {
        super(charStart, charEnd, buf);
        this._phe = new ParagraphHeight();
    }
    
    private SprmBuffer findHuge(final SprmBuffer buf, final byte[] datastream) {
        final byte[] grpprl = buf.toByteArray();
        if (grpprl.length == 8 && datastream != null) {
            final SprmOperation sprm = new SprmOperation(grpprl, 2);
            if ((sprm.getOperation() == 69 || sprm.getOperation() == 70) && sprm.getSizeCode() == 3) {
                final int hugeGrpprlOffset = sprm.getOperand();
                if (hugeGrpprlOffset + 1 < datastream.length) {
                    final int grpprlSize = LittleEndian.getShort(datastream, hugeGrpprlOffset);
                    if (hugeGrpprlOffset + grpprlSize < datastream.length) {
                        final byte[] hugeGrpprl = new byte[grpprlSize + 2];
                        hugeGrpprl[0] = grpprl[0];
                        hugeGrpprl[1] = grpprl[1];
                        System.arraycopy(datastream, hugeGrpprlOffset + 2, hugeGrpprl, 2, grpprlSize);
                        return new SprmBuffer(hugeGrpprl, 2);
                    }
                }
            }
        }
        return null;
    }
    
    public ParagraphHeight getParagraphHeight() {
        return this._phe;
    }
    
    public byte[] getGrpprl() {
        if (this._buf == null) {
            return new byte[0];
        }
        return ((SprmBuffer)this._buf).toByteArray();
    }
    
    public short getIstd() {
        if (this._buf == null) {
            return 0;
        }
        final byte[] buf = this.getGrpprl();
        if (buf.length == 0) {
            return 0;
        }
        if (buf.length == 1) {
            return (short)LittleEndian.getUnsignedByte(buf, 0);
        }
        return LittleEndian.getShort(buf);
    }
    
    public SprmBuffer getSprmBuf() {
        return (SprmBuffer)this._buf;
    }
    
    @Deprecated
    @Internal
    public ParagraphProperties getParagraphProperties(final StyleSheet ss) {
        if (ss == null) {
            return new ParagraphProperties();
        }
        final short istd = this.getIstd();
        final ParagraphProperties baseStyle = ss.getParagraphStyle(istd);
        final ParagraphProperties props = ParagraphSprmUncompressor.uncompressPAP(baseStyle, this.getGrpprl(), 2);
        return props;
    }
    
    @Override
    public boolean equals(final Object o) {
        return super.equals(o) && this._phe.equals(((PAPX)o)._phe);
    }
    
    @Override
    public String toString() {
        return "PAPX from " + this.getStart() + " to " + this.getEnd() + " (in bytes " + this.getStartBytes() + " to " + this.getEndBytes() + ")";
    }
}
