// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hwpf.model;

import org.apache.poi.util.POILogFactory;
import java.io.IOException;
import org.apache.poi.hwpf.model.io.HWPFOutputStream;
import org.apache.poi.hwpf.model.io.HWPFFileSystem;
import java.util.Set;
import java.util.List;
import org.apache.poi.hwpf.sprm.SprmOperation;
import org.apache.poi.hwpf.sprm.SprmIterator;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.HashSet;
import java.util.Map;
import java.util.IdentityHashMap;
import java.util.Comparator;
import java.util.Collections;
import java.util.Collection;
import java.util.Iterator;
import org.apache.poi.hwpf.sprm.SprmBuffer;
import org.apache.poi.util.LittleEndian;
import java.util.ArrayList;
import org.apache.poi.util.POILogger;
import org.apache.poi.util.Internal;

@Internal
public class CHPBinTable
{
    private static final POILogger logger;
    protected ArrayList<CHPX> _textRuns;
    
    public CHPBinTable() {
        this._textRuns = new ArrayList<CHPX>();
    }
    
    @Deprecated
    public CHPBinTable(final byte[] documentStream, final byte[] tableStream, final int offset, final int size, final int fcMin, final TextPieceTable tpt) {
        this(documentStream, tableStream, offset, size, tpt);
    }
    
    public CHPBinTable(final byte[] documentStream, final byte[] tableStream, final int offset, final int size, final CharIndexTranslator translator) {
        this._textRuns = new ArrayList<CHPX>();
        final long start = System.currentTimeMillis();
        final PlexOfCps bte = new PlexOfCps(tableStream, offset, size, 4);
        for (int length = bte.length(), x = 0; x < length; ++x) {
            final GenericPropertyNode node = bte.getProperty(x);
            final int pageNum = LittleEndian.getInt(node.getBytes());
            final int pageOffset = 512 * pageNum;
            final CHPFormattedDiskPage cfkp = new CHPFormattedDiskPage(documentStream, pageOffset, translator);
            for (final CHPX chpx : cfkp.getCHPXs()) {
                if (chpx != null) {
                    this._textRuns.add(chpx);
                }
            }
        }
        CHPBinTable.logger.log(POILogger.DEBUG, "CHPX FKPs loaded in ", System.currentTimeMillis() - start, " ms (", this._textRuns.size(), " elements)");
        if (this._textRuns.isEmpty()) {
            CHPBinTable.logger.log(POILogger.WARN, "CHPX FKPs are empty");
            this._textRuns.add(new CHPX(0, 0, new SprmBuffer(0)));
        }
    }
    
    public void rebuild(final ComplexFileTable complexFileTable) {
        long start = System.currentTimeMillis();
        if (complexFileTable != null) {
            final SprmBuffer[] sprmBuffers = complexFileTable.getGrpprls();
            for (final TextPiece textPiece : complexFileTable.getTextPieceTable().getTextPieces()) {
                final PropertyModifier prm = textPiece.getPieceDescriptor().getPrm();
                if (!prm.isComplex()) {
                    continue;
                }
                final int igrpprl = prm.getIgrpprl();
                if (igrpprl < 0 || igrpprl >= sprmBuffers.length) {
                    CHPBinTable.logger.log(POILogger.WARN, textPiece + "'s PRM references to unknown grpprl");
                }
                else {
                    boolean hasChp = false;
                    final SprmBuffer sprmBuffer = sprmBuffers[igrpprl];
                    for (final SprmOperation sprmOperation : sprmBuffer) {
                        if (sprmOperation.getType() == 2) {
                            hasChp = true;
                            break;
                        }
                    }
                    if (!hasChp) {
                        continue;
                    }
                    SprmBuffer newSprmBuffer;
                    try {
                        newSprmBuffer = (SprmBuffer)sprmBuffer.clone();
                    }
                    catch (CloneNotSupportedException e) {
                        throw new Error(e);
                    }
                    final CHPX chpx = new CHPX(textPiece.getStart(), textPiece.getEnd(), newSprmBuffer);
                    this._textRuns.add(chpx);
                }
            }
            CHPBinTable.logger.log(POILogger.DEBUG, "Merged with CHPX from complex file table in ", System.currentTimeMillis() - start, " ms (", this._textRuns.size(), " elements in total)");
            start = System.currentTimeMillis();
        }
        final List<CHPX> oldChpxSortedByStartPos = new ArrayList<CHPX>(this._textRuns);
        Collections.sort(oldChpxSortedByStartPos, PropertyNode.StartComparator.instance);
        CHPBinTable.logger.log(POILogger.DEBUG, "CHPX sorted by start position in ", System.currentTimeMillis() - start, " ms");
        start = System.currentTimeMillis();
        final Map<CHPX, Integer> chpxToFileOrder = new IdentityHashMap<CHPX, Integer>();
        int counter = 0;
        for (final CHPX chpx2 : this._textRuns) {
            chpxToFileOrder.put(chpx2, counter++);
        }
        final Comparator<CHPX> chpxFileOrderComparator = new Comparator<CHPX>() {
            public int compare(final CHPX o1, final CHPX o2) {
                final Integer i1 = chpxToFileOrder.get(o1);
                final Integer i2 = chpxToFileOrder.get(o2);
                return i1.compareTo(i2);
            }
        };
        CHPBinTable.logger.log(POILogger.DEBUG, "CHPX's order map created in ", System.currentTimeMillis() - start, " ms");
        start = System.currentTimeMillis();
        final Set<Integer> textRunsBoundariesSet = new HashSet<Integer>();
        for (final CHPX chpx3 : this._textRuns) {
            textRunsBoundariesSet.add(chpx3.getStart());
            textRunsBoundariesSet.add(chpx3.getEnd());
        }
        textRunsBoundariesSet.remove(0);
        final List<Integer> textRunsBoundariesList = new ArrayList<Integer>(textRunsBoundariesSet);
        Collections.sort(textRunsBoundariesList);
        CHPBinTable.logger.log(POILogger.DEBUG, "Texts CHPX boundaries collected in ", System.currentTimeMillis() - start, " ms");
        start = System.currentTimeMillis();
        final List<CHPX> newChpxs = new LinkedList<CHPX>();
        int lastTextRunStart = 0;
        for (final Integer objBoundary : textRunsBoundariesList) {
            final int boundary = objBoundary;
            final int startInclusive = lastTextRunStart;
            final int endExclusive = lastTextRunStart = boundary;
            int startPosition;
            for (startPosition = binarySearch(oldChpxSortedByStartPos, boundary), startPosition = Math.abs(startPosition); startPosition >= oldChpxSortedByStartPos.size(); --startPosition) {}
            while (startPosition > 0 && oldChpxSortedByStartPos.get(startPosition).getStart() >= boundary) {
                --startPosition;
            }
            final List<CHPX> chpxs = new LinkedList<CHPX>();
            for (int c = startPosition; c < oldChpxSortedByStartPos.size(); ++c) {
                final CHPX chpx4 = oldChpxSortedByStartPos.get(c);
                if (boundary < chpx4.getStart()) {
                    break;
                }
                final int left = Math.max(startInclusive, chpx4.getStart());
                final int right = Math.min(endExclusive, chpx4.getEnd());
                if (left < right) {
                    chpxs.add(chpx4);
                }
            }
            if (chpxs.size() == 0) {
                CHPBinTable.logger.log(POILogger.WARN, "Text piece [", startInclusive, "; ", endExclusive, ") has no CHPX. Creating new one.");
                final CHPX chpx5 = new CHPX(startInclusive, endExclusive, new SprmBuffer(0));
                newChpxs.add(chpx5);
            }
            else {
                if (chpxs.size() == 1) {
                    final CHPX existing = chpxs.get(0);
                    if (existing.getStart() == startInclusive && existing.getEnd() == endExclusive) {
                        newChpxs.add(existing);
                        continue;
                    }
                }
                Collections.sort(chpxs, chpxFileOrderComparator);
                final SprmBuffer sprmBuffer2 = new SprmBuffer(0);
                for (final CHPX chpx6 : chpxs) {
                    sprmBuffer2.append(chpx6.getGrpprl(), 0);
                }
                final CHPX newChpx = new CHPX(startInclusive, endExclusive, sprmBuffer2);
                newChpxs.add(newChpx);
            }
        }
        this._textRuns = new ArrayList<CHPX>(newChpxs);
        CHPBinTable.logger.log(POILogger.DEBUG, "CHPX rebuilded in ", System.currentTimeMillis() - start, " ms (", this._textRuns.size(), " elements)");
        start = System.currentTimeMillis();
        CHPX previous = null;
        final Iterator<CHPX> iterator2 = this._textRuns.iterator();
        while (iterator2.hasNext()) {
            final CHPX current = iterator2.next();
            if (previous == null) {
                previous = current;
            }
            else if (previous.getEnd() == current.getStart() && Arrays.equals(previous.getGrpprl(), current.getGrpprl())) {
                previous.setEnd(current.getEnd());
                iterator2.remove();
            }
            else {
                previous = current;
            }
        }
        CHPBinTable.logger.log(POILogger.DEBUG, "CHPX compacted in ", System.currentTimeMillis() - start, " ms (", this._textRuns.size(), " elements)");
    }
    
    private static int binarySearch(final List<CHPX> chpxs, final int startPosition) {
        int low = 0;
        int high = chpxs.size() - 1;
        while (low <= high) {
            final int mid = low + high >>> 1;
            final CHPX midVal = chpxs.get(mid);
            final int midValue = midVal.getStart();
            if (midValue < startPosition) {
                low = mid + 1;
            }
            else {
                if (midValue <= startPosition) {
                    return mid;
                }
                high = mid - 1;
            }
        }
        return -(low + 1);
    }
    
    public void adjustForDelete(final int listIndex, final int offset, final int length) {
        final int size = this._textRuns.size();
        final int endMark = offset + length;
        int endIndex = listIndex;
        for (CHPX chpx = this._textRuns.get(endIndex); chpx.getEnd() < endMark; chpx = this._textRuns.get(++endIndex)) {}
        if (listIndex == endIndex) {
            final CHPX chpx = this._textRuns.get(endIndex);
            chpx.setEnd(chpx.getEnd() - endMark + offset);
        }
        else {
            CHPX chpx = this._textRuns.get(listIndex);
            chpx.setEnd(offset);
            for (int x = listIndex + 1; x < endIndex; ++x) {
                chpx = this._textRuns.get(x);
                chpx.setStart(offset);
                chpx.setEnd(offset);
            }
            chpx = this._textRuns.get(endIndex);
            chpx.setEnd(chpx.getEnd() - endMark + offset);
        }
        for (int x = endIndex + 1; x < size; ++x) {
            final CHPX chpx = this._textRuns.get(x);
            chpx.setStart(chpx.getStart() - length);
            chpx.setEnd(chpx.getEnd() - length);
        }
    }
    
    public void insert(final int listIndex, final int cpStart, final SprmBuffer buf) {
        final CHPX insertChpx = new CHPX(0, 0, buf);
        insertChpx.setStart(cpStart);
        insertChpx.setEnd(cpStart);
        if (listIndex == this._textRuns.size()) {
            this._textRuns.add(insertChpx);
        }
        else {
            final CHPX chpx = this._textRuns.get(listIndex);
            if (chpx.getStart() < cpStart) {
                final CHPX clone = new CHPX(0, 0, chpx.getSprmBuf());
                clone.setStart(cpStart);
                clone.setEnd(chpx.getEnd());
                chpx.setEnd(cpStart);
                this._textRuns.add(listIndex + 1, insertChpx);
                this._textRuns.add(listIndex + 2, clone);
            }
            else {
                this._textRuns.add(listIndex, insertChpx);
            }
        }
    }
    
    public void adjustForInsert(final int listIndex, final int length) {
        final int size = this._textRuns.size();
        CHPX chpx = this._textRuns.get(listIndex);
        chpx.setEnd(chpx.getEnd() + length);
        for (int x = listIndex + 1; x < size; ++x) {
            chpx = this._textRuns.get(x);
            chpx.setStart(chpx.getStart() + length);
            chpx.setEnd(chpx.getEnd() + length);
        }
    }
    
    public List<CHPX> getTextRuns() {
        return this._textRuns;
    }
    
    @Deprecated
    public void writeTo(final HWPFFileSystem sys, final int fcMin, final CharIndexTranslator translator) throws IOException {
        final HWPFOutputStream docStream = sys.getStream("WordDocument");
        final HWPFOutputStream tableStream = sys.getStream("1Table");
        this.writeTo(docStream, tableStream, fcMin, translator);
    }
    
    public void writeTo(final HWPFOutputStream wordDocumentStream, final HWPFOutputStream tableStream, final int fcMin, final CharIndexTranslator translator) throws IOException {
        final PlexOfCps bte = new PlexOfCps(4);
        int docOffset = wordDocumentStream.getOffset();
        final int mod = docOffset % 512;
        if (mod != 0) {
            final byte[] padding = new byte[512 - mod];
            wordDocumentStream.write(padding);
        }
        docOffset = wordDocumentStream.getOffset();
        int pageNum = docOffset / 512;
        final int endingFc = translator.getByteIndex(this._textRuns.get(this._textRuns.size() - 1).getEnd());
        ArrayList<CHPX> overflow = this._textRuns;
        do {
            final CHPX startingProp = overflow.get(0);
            final int start = translator.getByteIndex(startingProp.getStart());
            final CHPFormattedDiskPage cfkp = new CHPFormattedDiskPage();
            cfkp.fill(overflow);
            final byte[] bufFkp = cfkp.toByteArray(translator);
            wordDocumentStream.write(bufFkp);
            overflow = cfkp.getOverflow();
            int end = endingFc;
            if (overflow != null) {
                end = translator.getByteIndex(overflow.get(0).getStart());
            }
            final byte[] intHolder = new byte[4];
            LittleEndian.putInt(intHolder, pageNum++);
            bte.addProperty(new GenericPropertyNode(start, end, intHolder));
        } while (overflow != null);
        tableStream.write(bte.toByteArray());
    }
    
    static {
        logger = POILogFactory.getLogger(CHPBinTable.class);
    }
}
