// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hwpf.model;

import org.apache.poi.util.Internal;
import org.apache.poi.hwpf.model.types.FibRgW97AbstractType;

@Internal
class FibRgW97 extends FibRgW97AbstractType
{
    public FibRgW97() {
    }
    
    public FibRgW97(final byte[] std, final int offset) {
        this.fillFields(std, offset);
    }
    
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        final FibRgW97 other = (FibRgW97)obj;
        return this.field_10_reserved10 == other.field_10_reserved10 && this.field_11_reserved11 == other.field_11_reserved11 && this.field_12_reserved12 == other.field_12_reserved12 && this.field_13_reserved13 == other.field_13_reserved13 && this.field_14_lidFE == other.field_14_lidFE && this.field_1_reserved1 == other.field_1_reserved1 && this.field_2_reserved2 == other.field_2_reserved2 && this.field_3_reserved3 == other.field_3_reserved3 && this.field_4_reserved4 == other.field_4_reserved4 && this.field_5_reserved5 == other.field_5_reserved5 && this.field_6_reserved6 == other.field_6_reserved6 && this.field_7_reserved7 == other.field_7_reserved7 && this.field_8_reserved8 == other.field_8_reserved8 && this.field_9_reserved9 == other.field_9_reserved9;
    }
    
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = 31 * result + this.field_10_reserved10;
        result = 31 * result + this.field_11_reserved11;
        result = 31 * result + this.field_12_reserved12;
        result = 31 * result + this.field_13_reserved13;
        result = 31 * result + this.field_14_lidFE;
        result = 31 * result + this.field_1_reserved1;
        result = 31 * result + this.field_2_reserved2;
        result = 31 * result + this.field_3_reserved3;
        result = 31 * result + this.field_4_reserved4;
        result = 31 * result + this.field_5_reserved5;
        result = 31 * result + this.field_6_reserved6;
        result = 31 * result + this.field_7_reserved7;
        result = 31 * result + this.field_8_reserved8;
        result = 31 * result + this.field_9_reserved9;
        return result;
    }
}
