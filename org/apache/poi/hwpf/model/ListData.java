// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hwpf.model;

import java.util.Arrays;
import org.apache.poi.util.Internal;

@Internal
public final class ListData
{
    private ListLevel[] _levels;
    private LSTF _lstf;
    
    ListData(final byte[] buf, final int offset) {
        this._lstf = new LSTF(buf, offset);
        if (this._lstf.isFSimpleList()) {
            this._levels = new ListLevel[1];
        }
        else {
            this._levels = new ListLevel[9];
        }
    }
    
    public ListData(final int listID, final boolean numbered) {
        (this._lstf = new LSTF()).setLsid(listID);
        this._lstf.setRgistdPara(new short[9]);
        Arrays.fill(this._lstf.getRgistdPara(), (short)4095);
        this._levels = new ListLevel[9];
        for (int x = 0; x < this._levels.length; ++x) {
            this._levels[x] = new ListLevel(x, numbered);
        }
    }
    
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        final ListData other = (ListData)obj;
        if (!Arrays.equals(this._levels, other._levels)) {
            return false;
        }
        if (this._lstf == null) {
            if (other._lstf != null) {
                return false;
            }
        }
        else if (!this._lstf.equals(other._lstf)) {
            return false;
        }
        return true;
    }
    
    public ListLevel getLevel(final int index) {
        return this._levels[index - 1];
    }
    
    public ListLevel[] getLevels() {
        return this._levels;
    }
    
    public int getLevelStyle(final int index) {
        return this._lstf.getRgistdPara()[index];
    }
    
    public int getLsid() {
        return this._lstf.getLsid();
    }
    
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = 31 * result + Arrays.hashCode(this._levels);
        result = 31 * result + ((this._lstf == null) ? 0 : this._lstf.hashCode());
        return result;
    }
    
    public int numLevels() {
        return this._levels.length;
    }
    
    int resetListID() {
        this._lstf.setLsid((int)(Math.random() * System.currentTimeMillis()));
        return this._lstf.getLsid();
    }
    
    public void setLevel(final int index, final ListLevel level) {
        this._levels[index] = level;
    }
    
    public void setLevelStyle(final int index, final int styleIndex) {
        this._lstf.getRgistdPara()[index] = (short)styleIndex;
    }
    
    public byte[] toByteArray() {
        return this._lstf.serialize();
    }
}
