// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hwpf.model;

import org.apache.poi.hwpf.model.io.HWPFOutputStream;
import org.apache.poi.hwpf.model.io.HWPFFileSystem;
import java.util.List;
import java.io.IOException;
import org.apache.poi.util.LittleEndian;
import java.util.LinkedList;
import org.apache.poi.hwpf.sprm.SprmBuffer;
import org.apache.poi.util.Internal;

@Internal
public final class ComplexFileTable
{
    private static final byte GRPPRL_TYPE = 1;
    private static final byte TEXT_PIECE_TABLE_TYPE = 2;
    protected TextPieceTable _tpt;
    private SprmBuffer[] _grpprls;
    
    public ComplexFileTable() {
        this._tpt = new TextPieceTable();
    }
    
    public ComplexFileTable(final byte[] documentStream, final byte[] tableStream, int offset, final int fcMin) throws IOException {
        final List<SprmBuffer> sprmBuffers = new LinkedList<SprmBuffer>();
        while (tableStream[offset] == 1) {
            ++offset;
            final int size = LittleEndian.getShort(tableStream, offset);
            offset += 2;
            final byte[] bs = LittleEndian.getByteArray(tableStream, offset, size);
            offset += size;
            final SprmBuffer sprmBuffer = new SprmBuffer(bs, false, 0);
            sprmBuffers.add(sprmBuffer);
        }
        this._grpprls = sprmBuffers.toArray(new SprmBuffer[sprmBuffers.size()]);
        if (tableStream[offset] != 2) {
            throw new IOException("The text piece table is corrupted");
        }
        final int pieceTableSize = LittleEndian.getInt(tableStream, ++offset);
        offset += 4;
        this._tpt = new TextPieceTable(documentStream, tableStream, offset, pieceTableSize, fcMin);
    }
    
    public TextPieceTable getTextPieceTable() {
        return this._tpt;
    }
    
    public SprmBuffer[] getGrpprls() {
        return this._grpprls;
    }
    
    @Deprecated
    public void writeTo(final HWPFFileSystem sys) throws IOException {
        final HWPFOutputStream docStream = sys.getStream("WordDocument");
        final HWPFOutputStream tableStream = sys.getStream("1Table");
        this.writeTo(docStream, tableStream);
    }
    
    public void writeTo(final HWPFOutputStream wordDocumentStream, final HWPFOutputStream tableStream) throws IOException {
        tableStream.write(2);
        final byte[] table = this._tpt.writeTo(wordDocumentStream);
        final byte[] numHolder = new byte[4];
        LittleEndian.putInt(numHolder, table.length);
        tableStream.write(numHolder);
        tableStream.write(table);
    }
}
