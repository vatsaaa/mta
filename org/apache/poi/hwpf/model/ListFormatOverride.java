// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hwpf.model;

import org.apache.poi.util.Internal;

@Internal
public final class ListFormatOverride
{
    private LFO _lfo;
    private LFOData _lfoData;
    
    public ListFormatOverride(final byte[] buf, final int offset) {
        this._lfo = new LFO(buf, offset);
    }
    
    public ListFormatOverride(final int lsid) {
        (this._lfo = new LFO()).setLsid(lsid);
    }
    
    public ListFormatOverrideLevel[] getLevelOverrides() {
        return this._lfoData.getRgLfoLvl();
    }
    
    LFO getLfo() {
        return this._lfo;
    }
    
    LFOData getLfoData() {
        return this._lfoData;
    }
    
    public int getLsid() {
        return this._lfo.getLsid();
    }
    
    public ListFormatOverrideLevel getOverrideLevel(final int level) {
        ListFormatOverrideLevel retLevel = null;
        for (int x = 0; x < this.getLevelOverrides().length; ++x) {
            if (this.getLevelOverrides()[x].getLevelNum() == level) {
                retLevel = this.getLevelOverrides()[x];
            }
        }
        return retLevel;
    }
    
    public int numOverrides() {
        return this._lfo.getClfolvl();
    }
    
    void setLfoData(final LFOData _lfoData) {
        this._lfoData = _lfoData;
    }
    
    public void setLsid(final int lsid) {
        this._lfo.setLsid(lsid);
    }
    
    public void setOverride(final int index, final ListFormatOverrideLevel lfolvl) {
        this.getLevelOverrides()[index] = lfolvl;
    }
    
    public byte[] toByteArray() {
        return this._lfo.serialize();
    }
}
