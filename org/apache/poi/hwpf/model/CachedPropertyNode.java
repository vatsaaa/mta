// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hwpf.model;

import org.apache.poi.hwpf.sprm.SprmBuffer;
import java.lang.ref.SoftReference;
import org.apache.poi.util.Internal;

@Internal
public final class CachedPropertyNode extends PropertyNode<CachedPropertyNode>
{
    protected SoftReference<Object> _propCache;
    
    public CachedPropertyNode(final int start, final int end, final SprmBuffer buf) {
        super(start, end, buf);
    }
    
    protected void fillCache(final Object ref) {
        this._propCache = new SoftReference<Object>(ref);
    }
    
    protected Object getCacheContents() {
        return (this._propCache == null) ? null : this._propCache.get();
    }
    
    public SprmBuffer getSprmBuf() {
        return (SprmBuffer)this._buf;
    }
}
