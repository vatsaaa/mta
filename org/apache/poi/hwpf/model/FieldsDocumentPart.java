// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hwpf.model;

import org.apache.poi.util.Internal;

@Internal
public enum FieldsDocumentPart
{
    ANNOTATIONS(19), 
    ENDNOTES(48), 
    FOOTNOTES(18), 
    HEADER(17), 
    HEADER_TEXTBOX(59), 
    MAIN(16), 
    TEXTBOX(57);
    
    private final int fibFieldsField;
    
    private FieldsDocumentPart(final int fibHandlerField) {
        this.fibFieldsField = fibHandlerField;
    }
    
    public int getFibFieldsField() {
        return this.fibFieldsField;
    }
}
