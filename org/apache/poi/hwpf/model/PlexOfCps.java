// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hwpf.model;

import org.apache.poi.util.LittleEndian;
import org.apache.poi.util.Internal;
import java.util.Iterator;
import java.util.ArrayList;

public final class PlexOfCps
{
    private int _iMac;
    private int _offset;
    private int _cbStruct;
    private ArrayList<GenericPropertyNode> _props;
    
    public PlexOfCps(final int sizeOfStruct) {
        this._props = new ArrayList<GenericPropertyNode>();
        this._cbStruct = sizeOfStruct;
    }
    
    public PlexOfCps(final byte[] buf, final int start, final int cb, final int cbStruct) {
        this._iMac = (cb - 4) / (4 + cbStruct);
        this._cbStruct = cbStruct;
        this._props = new ArrayList<GenericPropertyNode>(this._iMac);
        for (int x = 0; x < this._iMac; ++x) {
            this._props.add(this.getProperty(x, buf, start));
        }
    }
    
    @Internal
    void adjust(final int startCp, final int shift) {
        for (final GenericPropertyNode node : this._props) {
            if (node.getStart() > startCp) {
                if (node.getStart() + shift < startCp) {
                    node.setStart(startCp);
                }
                else {
                    node.setStart(node.getStart() + shift);
                }
            }
            if (node.getEnd() >= startCp) {
                if (node.getEnd() + shift < startCp) {
                    node.setEnd(startCp);
                }
                else {
                    node.setEnd(node.getEnd() + shift);
                }
            }
        }
    }
    
    public GenericPropertyNode getProperty(final int index) {
        return this._props.get(index);
    }
    
    public void addProperty(final GenericPropertyNode node) {
        this._props.add(node);
        ++this._iMac;
    }
    
    void remove(final int index) {
        this._props.remove(index);
        --this._iMac;
    }
    
    public byte[] toByteArray() {
        final int size = this._props.size();
        final int cpBufSize = (size + 1) * 4;
        final int structBufSize = this._cbStruct * size;
        final int bufSize = cpBufSize + structBufSize;
        final byte[] buf = new byte[bufSize];
        GenericPropertyNode node = null;
        for (int x = 0; x < size; ++x) {
            node = this._props.get(x);
            LittleEndian.putInt(buf, 4 * x, node.getStart());
            System.arraycopy(node.getBytes(), 0, buf, cpBufSize + x * this._cbStruct, this._cbStruct);
        }
        LittleEndian.putInt(buf, 4 * size, node.getEnd());
        return buf;
    }
    
    private GenericPropertyNode getProperty(final int index, final byte[] buf, final int offset) {
        final int start = LittleEndian.getInt(buf, offset + this.getIntOffset(index));
        final int end = LittleEndian.getInt(buf, offset + this.getIntOffset(index + 1));
        final byte[] struct = new byte[this._cbStruct];
        System.arraycopy(buf, offset + this.getStructOffset(index), struct, 0, this._cbStruct);
        return new GenericPropertyNode(start, end, struct);
    }
    
    private int getIntOffset(final int index) {
        return index * 4;
    }
    
    public int length() {
        return this._iMac;
    }
    
    private int getStructOffset(final int index) {
        return 4 * (this._iMac + 1) + this._cbStruct * index;
    }
    
    GenericPropertyNode[] toPropertiesArray() {
        if (this._props == null || this._props.isEmpty()) {
            return new GenericPropertyNode[0];
        }
        return this._props.toArray(new GenericPropertyNode[this._props.size()]);
    }
    
    @Override
    public String toString() {
        return "PLCF (cbStruct: " + this._cbStruct + "; iMac: " + this._iMac + ")";
    }
}
