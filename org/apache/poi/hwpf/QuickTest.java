// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hwpf;

import org.apache.poi.hwpf.usermodel.CharacterRun;
import org.apache.poi.hwpf.usermodel.Paragraph;
import org.apache.poi.hwpf.usermodel.Section;
import org.apache.poi.hwpf.usermodel.Range;
import java.io.InputStream;
import java.io.FileInputStream;

public final class QuickTest
{
    public static void main(final String[] args) {
        try {
            final HWPFDocument doc = new HWPFDocument(new FileInputStream(args[0]));
            final Range r = doc.getRange();
            System.out.println("Example you supplied:");
            System.out.println("---------------------");
            for (int x = 0; x < r.numSections(); ++x) {
                final Section s = r.getSection(x);
                for (int y = 0; y < s.numParagraphs(); ++y) {
                    final Paragraph p = s.getParagraph(y);
                    for (int z = 0; z < p.numCharacterRuns(); ++z) {
                        final CharacterRun run = p.getCharacterRun(z);
                        final String text = run.text();
                        System.out.print(text);
                    }
                    System.out.println();
                }
            }
        }
        catch (Throwable t) {
            t.printStackTrace();
        }
    }
}
