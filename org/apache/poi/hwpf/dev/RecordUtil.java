// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hwpf.dev;

import org.apache.poi.util.Internal;

@Internal
public class RecordUtil
{
    public static String getBitFieldFunction(final String name, final String bitMask, final String parentType, final String withType) {
        final String type = getBitFieldType(name, bitMask, parentType);
        String retVal = new String();
        if (withType.equals("true")) {
            retVal = type + " ";
        }
        if (type.equals("boolean")) {
            retVal = retVal + "is" + getFieldName1stCap(name, 0);
        }
        else {
            retVal = retVal + "get" + getFieldName1stCap(name, 0);
        }
        return retVal;
    }
    
    public static String getBitFieldGet(final String name, final String bitMask, final String parentType, final String parentField) {
        final String type = getBitFieldType(name, bitMask, parentType);
        String retVal = null;
        if (type.equals("boolean")) {
            retVal = name + ".isSet(" + parentField + ");";
        }
        else {
            retVal = "( " + type + " )" + name + ".getValue(" + parentField + ");";
        }
        return retVal;
    }
    
    public static String getBitFieldSet(final String name, final String bitMask, final String parentType, final String parentField) {
        final String type = getBitFieldType(name, bitMask, parentType);
        String retVal = null;
        if (type.equals("boolean")) {
            if (parentType.equals("int")) {
                retVal = getFieldName(name, 0) + ".setBoolean(" + parentField + ", value)";
            }
            else {
                retVal = "(" + parentType + ")" + getFieldName(name, 0) + ".setBoolean(" + parentField + ", value)";
            }
        }
        else if (parentType.equals("int")) {
            retVal = getFieldName(name, 0) + ".setValue(" + parentField + ", value)";
        }
        else {
            retVal = "(" + parentType + ")" + getFieldName(name, 0) + ".setValue(" + parentField + ", value)";
        }
        return retVal;
    }
    
    public static String getBitFieldType(final String name, final String bitMask, final String parentType) {
        byte parentSize = 0;
        byte numBits = 0;
        final int mask = (int)Long.parseLong(bitMask.substring(2), 16);
        if (parentType.equals("byte")) {
            parentSize = 8;
        }
        else if (parentType.equals("short")) {
            parentSize = 16;
        }
        else if (parentType.equals("int")) {
            parentSize = 32;
        }
        for (int x = 0; x < parentSize; ++x) {
            final int temp = mask;
            numBits += (byte)(temp >> x & 0x1);
        }
        if (numBits == 1) {
            return "boolean";
        }
        if (numBits < 8) {
            return "byte";
        }
        if (numBits < 16) {
            return "short";
        }
        return "int";
    }
    
    public static String getConstName(final String parentName, final String constName, final int padTo) {
        final StringBuffer fieldName = new StringBuffer();
        toConstIdentifier(parentName, fieldName);
        fieldName.append('_');
        toConstIdentifier(constName, fieldName);
        pad(fieldName, padTo);
        return fieldName.toString();
    }
    
    public static String getFieldName(final int position, final String name, final int padTo) {
        final StringBuffer fieldName = new StringBuffer("field_" + position + "_");
        toIdentifier(name, fieldName);
        pad(fieldName, padTo);
        return fieldName.toString();
    }
    
    public static String getFieldName(final String name, final int padTo) {
        final StringBuffer fieldName = new StringBuffer();
        toIdentifier(name, fieldName);
        pad(fieldName, padTo);
        return fieldName.toString();
    }
    
    public static String getFieldName1stCap(final String name, final int padTo) {
        final StringBuffer fieldName = new StringBuffer();
        toIdentifier(name, fieldName);
        fieldName.setCharAt(0, Character.toUpperCase(fieldName.charAt(0)));
        pad(fieldName, padTo);
        return fieldName.toString();
    }
    
    public static String getType1stCap(final String size, final String type, final int padTo) {
        StringBuffer result = new StringBuffer();
        result.append(type);
        result = pad(result, padTo);
        result.setCharAt(0, Character.toUpperCase(result.charAt(0)));
        return result.toString();
    }
    
    protected static StringBuffer pad(final StringBuffer fieldName, final int padTo) {
        for (int i = fieldName.length(); i < padTo; ++i) {
            fieldName.append(' ');
        }
        return fieldName;
    }
    
    private static void toConstIdentifier(final String name, final StringBuffer fieldName) {
        for (int i = 0; i < name.length(); ++i) {
            if (name.charAt(i) == ' ') {
                fieldName.append('_');
            }
            else {
                fieldName.append(Character.toUpperCase(name.charAt(i)));
            }
        }
    }
    
    private static void toIdentifier(final String name, final StringBuffer fieldName) {
        for (int i = 0; i < name.length(); ++i) {
            if (name.charAt(i) == ' ') {
                fieldName.append(Character.toUpperCase(name.charAt(++i)));
            }
            else {
                fieldName.append(name.charAt(i));
            }
        }
    }
}
