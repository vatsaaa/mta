// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hwpf.dev;

import org.apache.poi.util.Internal;

@Internal
public class FieldIterator
{
    protected int offset;
    
    public String calcSize(final int fieldNumber, final String fieldName, final String size, final String type) {
        final String result = " + ";
        if (type.startsWith("custom:")) {
            final String javaFieldName = RecordUtil.getFieldName(fieldNumber, fieldName, 0);
            return result + javaFieldName + ".getSize()";
        }
        if ("var".equals(size)) {
            final String javaFieldName = RecordUtil.getFieldName(fieldNumber, fieldName, 0);
            return result + " ( " + javaFieldName + ".length() *2)";
        }
        if ("varword".equals(size)) {
            final String javaFieldName = RecordUtil.getFieldName(fieldNumber, fieldName, 0);
            return result + javaFieldName + ".length * 2 + 2";
        }
        return result + size;
    }
    
    public String fillDecoder(final String size, final String type) {
        String result = "";
        if (type.equals("short[]")) {
            result = "LittleEndian.getShortArray( data, 0x" + Integer.toHexString(this.offset) + " + offset, " + size + " )";
        }
        else if (type.equals("byte[]")) {
            result = "LittleEndian.getByteArray( data, 0x" + Integer.toHexString(this.offset) + " + offset," + size + " )";
        }
        else if (type.equals("BorderCode")) {
            result = "new BorderCode( data, 0x" + Integer.toHexString(this.offset) + " + offset )";
        }
        else if (type.equals("Colorref")) {
            result = "new Colorref( data, 0x" + Integer.toHexString(this.offset) + " + offset )";
        }
        else if (type.equals("DateAndTime")) {
            result = "new DateAndTime( data, 0x" + Integer.toHexString(this.offset) + " + offset )";
        }
        else if (type.equals("Grfhic")) {
            result = "new Grfhic( data, 0x" + Integer.toHexString(this.offset) + " + offset )";
        }
        else if (size.equals("2")) {
            result = "LittleEndian.getShort( data, 0x" + Integer.toHexString(this.offset) + " + offset )";
        }
        else if (size.equals("4")) {
            if (type.equals("long")) {
                result = "LittleEndian.getUInt( data, 0x" + Integer.toHexString(this.offset) + " + offset )";
            }
            else {
                result = "LittleEndian.getInt( data, 0x" + Integer.toHexString(this.offset) + " + offset )";
            }
        }
        else if (size.equals("1")) {
            if (type.equals("short")) {
                result = "LittleEndian.getUByte( data, 0x" + Integer.toHexString(this.offset) + " + offset )";
            }
            else if (type.equals("int") || type.equals("long")) {
                result = "LittleEndian.getUnsignedByte( data, 0x" + Integer.toHexString(this.offset) + " + offset )";
            }
            else {
                result = "data[ 0x" + Integer.toHexString(this.offset) + " + offset ]";
            }
        }
        else if (type.equals("double")) {
            result = "LittleEndian.getDouble(data, 0x" + Integer.toHexString(this.offset) + " + offset )";
        }
        try {
            this.offset += Integer.parseInt(size);
        }
        catch (NumberFormatException ex) {}
        return result;
    }
    
    public String serialiseEncoder(final int fieldNumber, final String fieldName, final String size, final String type) {
        final String javaFieldName = RecordUtil.getFieldName(fieldNumber, fieldName, 0);
        String result = "";
        if (type.equals("short[]")) {
            result = "LittleEndian.putShortArray( data, 0x" + Integer.toHexString(this.offset) + " + offset, " + javaFieldName + " );";
        }
        else if (type.equals("byte[]")) {
            result = "System.arraycopy( " + javaFieldName + ", 0, data, 0x" + Integer.toHexString(this.offset) + " + offset, " + javaFieldName + ".length );";
        }
        else if (type.equals("BorderCode")) {
            result = javaFieldName + ".serialize( data, 0x" + Integer.toHexString(this.offset) + " + offset );";
        }
        else if (type.equals("Colorref")) {
            result = javaFieldName + ".serialize( data, 0x" + Integer.toHexString(this.offset) + " + offset );";
        }
        else if (type.equals("DateAndTime")) {
            result = javaFieldName + ".serialize( data, 0x" + Integer.toHexString(this.offset) + " + offset );";
        }
        else if (type.equals("Grfhic")) {
            result = javaFieldName + ".serialize( data, 0x" + Integer.toHexString(this.offset) + " + offset );";
        }
        else if (size.equals("2")) {
            if (type.equals("short")) {
                result = "LittleEndian.putShort( data, 0x" + Integer.toHexString(this.offset) + " + offset, " + javaFieldName + " );";
            }
            else if (type.equals("int")) {
                result = "LittleEndian.putUShort( data, 0x" + Integer.toHexString(this.offset) + " + offset, " + javaFieldName + " );";
            }
            else {
                result = "LittleEndian.putShort( data, 0x" + Integer.toHexString(this.offset) + " + offset, (short)" + javaFieldName + " );";
            }
        }
        else if (size.equals("4")) {
            if (type.equals("long")) {
                result = "LittleEndian.putUInt( data, 0x" + Integer.toHexString(this.offset) + " + offset, " + javaFieldName + " );";
            }
            else {
                result = "LittleEndian.putInt( data, 0x" + Integer.toHexString(this.offset) + " + offset, " + javaFieldName + " );";
            }
        }
        else if (size.equals("1")) {
            if (type.equals("byte")) {
                result = "data[ 0x" + Integer.toHexString(this.offset) + " + offset ] = " + javaFieldName + ";";
            }
            else {
                result = "LittleEndian.putUByte( data, 0x" + Integer.toHexString(this.offset) + " + offset, " + javaFieldName + " );";
            }
        }
        else if (type.equals("double")) {
            result = "LittleEndian.putDouble(data, 0x" + Integer.toHexString(this.offset) + " + offset, " + javaFieldName + " );";
        }
        try {
            this.offset += Integer.parseInt(size);
        }
        catch (NumberFormatException ex) {}
        return result;
    }
}
