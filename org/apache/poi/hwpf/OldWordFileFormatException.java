// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hwpf;

import org.apache.poi.OldFileFormatException;

public class OldWordFileFormatException extends OldFileFormatException
{
    public OldWordFileFormatException(final String s) {
        super(s);
    }
}
