// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hwpf.sprm;

import org.apache.poi.util.POILogFactory;
import java.util.List;
import java.util.Map;
import java.util.Collections;
import java.util.Collection;
import java.util.ArrayList;
import org.apache.poi.hwpf.model.TabDescriptor;
import java.util.HashMap;
import org.apache.poi.hwpf.usermodel.ShadingDescriptor;
import org.apache.poi.hwpf.usermodel.DateAndTime;
import org.apache.poi.util.LittleEndian;
import org.apache.poi.hwpf.usermodel.ShadingDescriptor80;
import org.apache.poi.hwpf.usermodel.DropCapSpecifier;
import org.apache.poi.hwpf.usermodel.BorderCode;
import org.apache.poi.hwpf.usermodel.LineSpacingDescriptor;
import org.apache.poi.hwpf.usermodel.ParagraphProperties;
import org.apache.poi.util.POILogger;
import org.apache.poi.util.Internal;

@Internal
public final class ParagraphSprmUncompressor extends SprmUncompressor
{
    private static final POILogger logger;
    
    public static ParagraphProperties uncompressPAP(final ParagraphProperties parent, final byte[] grpprl, final int offset) {
        ParagraphProperties newProperties = null;
        try {
            newProperties = (ParagraphProperties)parent.clone();
        }
        catch (CloneNotSupportedException cnse) {
            throw new RuntimeException("There is no way this exception should happen!!");
        }
        final SprmIterator sprmIt = new SprmIterator(grpprl, offset);
        while (sprmIt.hasNext()) {
            final SprmOperation sprm = sprmIt.next();
            if (sprm.getType() == 1) {
                try {
                    unCompressPAPOperation(newProperties, sprm);
                }
                catch (Exception exc) {
                    ParagraphSprmUncompressor.logger.log(POILogger.ERROR, "Unable to apply SPRM operation '" + sprm.getOperation() + "': ", exc);
                }
            }
        }
        return newProperties;
    }
    
    static void unCompressPAPOperation(final ParagraphProperties newPAP, final SprmOperation sprm) {
        switch (sprm.getOperation()) {
            case 0: {
                newPAP.setIstd(sprm.getOperand());
                break;
            }
            case 1: {
                break;
            }
            case 2: {
                if (newPAP.getIstd() <= 9 || newPAP.getIstd() >= 1) {
                    final byte paramTmp = (byte)sprm.getOperand();
                    newPAP.setIstd(newPAP.getIstd() + paramTmp);
                    newPAP.setLvl((byte)(newPAP.getLvl() + paramTmp));
                    if ((paramTmp >> 7 & 0x1) == 0x1) {
                        newPAP.setIstd(Math.max(newPAP.getIstd(), 1));
                    }
                    else {
                        newPAP.setIstd(Math.min(newPAP.getIstd(), 9));
                    }
                    break;
                }
                break;
            }
            case 3: {
                newPAP.setJc((byte)sprm.getOperand());
                break;
            }
            case 4: {
                newPAP.setFSideBySide(sprm.getOperand() != 0);
                break;
            }
            case 5: {
                newPAP.setFKeep(sprm.getOperand() != 0);
                break;
            }
            case 6: {
                newPAP.setFKeepFollow(sprm.getOperand() != 0);
                break;
            }
            case 7: {
                newPAP.setFPageBreakBefore(sprm.getOperand() != 0);
                break;
            }
            case 8: {
                newPAP.setBrcl((byte)sprm.getOperand());
                break;
            }
            case 9: {
                newPAP.setBrcp((byte)sprm.getOperand());
                break;
            }
            case 10: {
                newPAP.setIlvl((byte)sprm.getOperand());
                break;
            }
            case 11: {
                newPAP.setIlfo(sprm.getOperand());
                break;
            }
            case 12: {
                newPAP.setFNoLnn(sprm.getOperand() != 0);
                break;
            }
            case 13: {
                handleTabs(newPAP, sprm);
                break;
            }
            case 14: {
                newPAP.setDxaRight(sprm.getOperand());
                break;
            }
            case 15: {
                newPAP.setDxaLeft(sprm.getOperand());
                break;
            }
            case 16: {
                newPAP.setDxaLeft(newPAP.getDxaLeft() + sprm.getOperand());
                newPAP.setDxaLeft(Math.max(0, newPAP.getDxaLeft()));
                break;
            }
            case 17: {
                newPAP.setDxaLeft1(sprm.getOperand());
                break;
            }
            case 18: {
                newPAP.setLspd(new LineSpacingDescriptor(sprm.getGrpprl(), sprm.getGrpprlOffset()));
                break;
            }
            case 19: {
                newPAP.setDyaBefore(sprm.getOperand());
                break;
            }
            case 20: {
                newPAP.setDyaAfter(sprm.getOperand());
                break;
            }
            case 21: {
                break;
            }
            case 22: {
                newPAP.setFInTable(sprm.getOperand() != 0);
                break;
            }
            case 23: {
                newPAP.setFTtp(sprm.getOperand() != 0);
                break;
            }
            case 24: {
                newPAP.setDxaAbs(sprm.getOperand());
                break;
            }
            case 25: {
                newPAP.setDyaAbs(sprm.getOperand());
                break;
            }
            case 26: {
                newPAP.setDxaWidth(sprm.getOperand());
                break;
            }
            case 27: {
                final byte param = (byte)sprm.getOperand();
                final byte pcVert = (byte)((param & 0xC) >> 2);
                final byte pcHorz = (byte)(param & 0x3);
                if (pcVert != 3) {
                    newPAP.setPcVert(pcVert);
                }
                if (pcHorz != 3) {
                    newPAP.setPcHorz(pcHorz);
                    break;
                }
                break;
            }
            case 34: {
                newPAP.setDxaFromText(sprm.getOperand());
                break;
            }
            case 35: {
                newPAP.setWr((byte)sprm.getOperand());
                break;
            }
            case 36: {
                newPAP.setBrcTop(new BorderCode(sprm.getGrpprl(), sprm.getGrpprlOffset()));
                break;
            }
            case 37: {
                newPAP.setBrcLeft(new BorderCode(sprm.getGrpprl(), sprm.getGrpprlOffset()));
                break;
            }
            case 38: {
                newPAP.setBrcBottom(new BorderCode(sprm.getGrpprl(), sprm.getGrpprlOffset()));
                break;
            }
            case 39: {
                newPAP.setBrcRight(new BorderCode(sprm.getGrpprl(), sprm.getGrpprlOffset()));
                break;
            }
            case 40: {
                newPAP.setBrcBetween(new BorderCode(sprm.getGrpprl(), sprm.getGrpprlOffset()));
                break;
            }
            case 41: {
                newPAP.setBrcBar(new BorderCode(sprm.getGrpprl(), sprm.getGrpprlOffset()));
                break;
            }
            case 42: {
                newPAP.setFNoAutoHyph(sprm.getOperand() != 0);
                break;
            }
            case 43: {
                newPAP.setDyaHeight(sprm.getOperand());
                break;
            }
            case 44: {
                newPAP.setDcs(new DropCapSpecifier((short)sprm.getOperand()));
                break;
            }
            case 45: {
                newPAP.setShd(new ShadingDescriptor80((short)sprm.getOperand()).toShadingDescriptor());
                break;
            }
            case 46: {
                newPAP.setDyaFromText(sprm.getOperand());
                break;
            }
            case 47: {
                newPAP.setDxaFromText(sprm.getOperand());
                break;
            }
            case 48: {
                newPAP.setFLocked(sprm.getOperand() != 0);
                break;
            }
            case 49: {
                newPAP.setFWidowControl(sprm.getOperand() != 0);
                break;
            }
            case 51: {
                newPAP.setFKinsoku(sprm.getOperand() != 0);
                break;
            }
            case 52: {
                newPAP.setFWordWrap(sprm.getOperand() != 0);
                break;
            }
            case 53: {
                newPAP.setFOverflowPunct(sprm.getOperand() != 0);
                break;
            }
            case 54: {
                newPAP.setFTopLinePunct(sprm.getOperand() != 0);
                break;
            }
            case 55: {
                newPAP.setFAutoSpaceDE(sprm.getOperand() != 0);
                break;
            }
            case 56: {
                newPAP.setFAutoSpaceDN(sprm.getOperand() != 0);
                break;
            }
            case 57: {
                newPAP.setWAlignFont(sprm.getOperand());
                break;
            }
            case 58: {
                newPAP.setFontAlign((short)sprm.getOperand());
                break;
            }
            case 59: {
                break;
            }
            case 62: {
                final byte[] buf = new byte[sprm.size() - 3];
                System.arraycopy(buf, 0, sprm.getGrpprl(), sprm.getGrpprlOffset(), buf.length);
                newPAP.setAnld(buf);
                break;
            }
            case 63: {
                final byte[] varParam = sprm.getGrpprl();
                final int offset = sprm.getGrpprlOffset();
                newPAP.setFPropRMark(varParam[offset] != 0);
                newPAP.setIbstPropRMark(LittleEndian.getShort(varParam, offset + 1));
                newPAP.setDttmPropRMark(new DateAndTime(varParam, offset + 3));
                break;
            }
            case 64: {
                newPAP.setLvl((byte)sprm.getOperand());
                break;
            }
            case 65: {
                newPAP.setFBiDi(sprm.getOperand() != 0);
                break;
            }
            case 67: {
                newPAP.setFNumRMIns(sprm.getOperand() != 0);
                break;
            }
            case 68: {
                break;
            }
            case 69: {
                if (sprm.getSizeCode() == 6) {
                    final byte[] buf2 = new byte[sprm.size() - 3];
                    System.arraycopy(buf2, 0, sprm.getGrpprl(), sprm.getGrpprlOffset(), buf2.length);
                    newPAP.setNumrm(buf2);
                    break;
                }
                break;
            }
            case 71: {
                newPAP.setFUsePgsuSettings(sprm.getOperand() != 0);
                break;
            }
            case 72: {
                newPAP.setFAdjustRight(sprm.getOperand() != 0);
                break;
            }
            case 73: {
                newPAP.setItap(sprm.getOperand());
                break;
            }
            case 74: {
                newPAP.setItap((byte)(newPAP.getItap() + sprm.getOperand()));
                break;
            }
            case 75: {
                newPAP.setFInnerTableCell(sprm.getOperand() != 0);
                break;
            }
            case 76: {
                newPAP.setFTtpEmbedded(sprm.getOperand() != 0);
                break;
            }
            case 77: {
                final ShadingDescriptor shadingDescriptor = new ShadingDescriptor(sprm.getGrpprl(), 3);
                newPAP.setShading(shadingDescriptor);
                break;
            }
            case 93: {
                newPAP.setDxaRight(sprm.getOperand());
                break;
            }
            case 94: {
                newPAP.setDxaLeft(sprm.getOperand());
                break;
            }
            case 96: {
                newPAP.setDxaLeft1(sprm.getOperand());
                break;
            }
            case 97: {
                newPAP.setJustificationLogical((byte)sprm.getOperand());
                break;
            }
            case 103: {
                newPAP.setRsid(sprm.getOperand());
                break;
            }
            default: {
                ParagraphSprmUncompressor.logger.log(POILogger.DEBUG, "Unknown PAP sprm ignored: " + sprm);
                break;
            }
        }
    }
    
    private static void handleTabs(final ParagraphProperties pap, final SprmOperation sprm) {
        final byte[] grpprl = sprm.getGrpprl();
        int offset = sprm.getGrpprlOffset();
        final int delSize = grpprl[offset++];
        int[] tabPositions = pap.getRgdxaTab();
        TabDescriptor[] tabDescriptors = pap.getRgtbd();
        final Map<Integer, TabDescriptor> tabMap = new HashMap<Integer, TabDescriptor>();
        for (int x = 0; x < tabPositions.length; ++x) {
            tabMap.put(tabPositions[x], tabDescriptors[x]);
        }
        for (int x = 0; x < delSize; ++x) {
            tabMap.remove((int)LittleEndian.getShort(grpprl, offset));
            offset += 2;
        }
        final int addSize = grpprl[offset++];
        final int start = offset;
        for (int x2 = 0; x2 < addSize; ++x2) {
            final Integer key = (Integer)LittleEndian.getShort(grpprl, offset);
            final TabDescriptor val = new TabDescriptor(grpprl, start + (TabDescriptor.getSize() * addSize + x2));
            tabMap.put(key, val);
            offset += 2;
        }
        tabPositions = new int[tabMap.size()];
        tabDescriptors = new TabDescriptor[tabPositions.length];
        final List<Integer> list = new ArrayList<Integer>(tabMap.keySet());
        Collections.sort(list);
        for (int x3 = 0; x3 < tabPositions.length; ++x3) {
            final Integer key2 = list.get(x3);
            tabPositions[x3] = key2;
            if (tabMap.containsKey(key2)) {
                tabDescriptors[x3] = tabMap.get(key2);
            }
            else {
                tabDescriptors[x3] = new TabDescriptor();
            }
        }
        pap.setRgdxaTab(tabPositions);
        pap.setRgtbd(tabDescriptors);
    }
    
    static {
        logger = POILogFactory.getLogger(ParagraphSprmUncompressor.class);
    }
}
