// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hwpf.sprm;

import org.apache.poi.util.BitFieldFactory;
import org.apache.poi.util.LittleEndian;
import org.apache.poi.util.BitField;
import org.apache.poi.util.Internal;

@Internal
public final class SprmOperation
{
    private static final BitField BITFIELD_OP;
    private static final BitField BITFIELD_SIZECODE;
    private static final BitField BITFIELD_SPECIAL;
    private static final BitField BITFIELD_TYPE;
    private static final short SPRM_LONG_PARAGRAPH = -14827;
    private static final short SPRM_LONG_TABLE = -10744;
    public static final int TYPE_PAP = 1;
    public static final int TYPE_CHP = 2;
    public static final int TYPE_PIC = 3;
    public static final int TYPE_SEP = 4;
    public static final int TYPE_TAP = 5;
    @Deprecated
    public static final int PAP_TYPE = 1;
    @Deprecated
    public static final int TAP_TYPE = 5;
    private int _offset;
    private int _gOffset;
    private byte[] _grpprl;
    private int _size;
    private short _value;
    
    public static int getOperationFromOpcode(final short opcode) {
        return SprmOperation.BITFIELD_OP.getValue(opcode);
    }
    
    public static int getTypeFromOpcode(final short opcode) {
        return SprmOperation.BITFIELD_TYPE.getValue(opcode);
    }
    
    public SprmOperation(final byte[] grpprl, final int offset) {
        this._grpprl = grpprl;
        this._value = LittleEndian.getShort(grpprl, offset);
        this._offset = offset;
        this._gOffset = offset + 2;
        this._size = this.initSize(this._value);
    }
    
    public byte[] toByteArray() {
        final byte[] result = new byte[this.size()];
        System.arraycopy(this._grpprl, this._offset, result, 0, this.size());
        return result;
    }
    
    public byte[] getGrpprl() {
        return this._grpprl;
    }
    
    public int getGrpprlOffset() {
        return this._gOffset;
    }
    
    public int getOperand() {
        switch (this.getSizeCode()) {
            case 0:
            case 1: {
                return this._grpprl[this._gOffset];
            }
            case 2:
            case 4:
            case 5: {
                return LittleEndian.getShort(this._grpprl, this._gOffset);
            }
            case 3: {
                return LittleEndian.getInt(this._grpprl, this._gOffset);
            }
            case 6: {
                final byte operandLength = this._grpprl[this._gOffset + 1];
                final byte[] codeBytes = new byte[4];
                for (int i = 0; i < operandLength; ++i) {
                    if (this._gOffset + i < this._grpprl.length) {
                        codeBytes[i] = this._grpprl[this._gOffset + 1 + i];
                    }
                }
                return LittleEndian.getInt(codeBytes, 0);
            }
            case 7: {
                final byte[] threeByteInt = { this._grpprl[this._gOffset], this._grpprl[this._gOffset + 1], this._grpprl[this._gOffset + 2], 0 };
                return LittleEndian.getInt(threeByteInt, 0);
            }
            default: {
                throw new IllegalArgumentException("SPRM contains an invalid size code");
            }
        }
    }
    
    public int getOperation() {
        return SprmOperation.BITFIELD_OP.getValue(this._value);
    }
    
    public int getSizeCode() {
        return SprmOperation.BITFIELD_SIZECODE.getValue(this._value);
    }
    
    public int getType() {
        return SprmOperation.BITFIELD_TYPE.getValue(this._value);
    }
    
    private int initSize(final short sprm) {
        switch (this.getSizeCode()) {
            case 0:
            case 1: {
                return 3;
            }
            case 2:
            case 4:
            case 5: {
                return 4;
            }
            case 3: {
                return 6;
            }
            case 6: {
                final int offset = this._gOffset;
                if (sprm == -10744 || sprm == -14827) {
                    final int retVal = (0xFFFF & LittleEndian.getShort(this._grpprl, offset)) + 3;
                    this._gOffset += 2;
                    return retVal;
                }
                return (0xFF & this._grpprl[this._gOffset++]) + 3;
            }
            case 7: {
                return 5;
            }
            default: {
                throw new IllegalArgumentException("SPRM contains an invalid size code");
            }
        }
    }
    
    public int size() {
        return this._size;
    }
    
    @Override
    public String toString() {
        final StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("[SPRM] (0x");
        stringBuilder.append(Integer.toHexString(this._value & 0xFFFF));
        stringBuilder.append("): ");
        try {
            stringBuilder.append(this.getOperand());
        }
        catch (Exception exc) {
            stringBuilder.append("(error)");
        }
        return stringBuilder.toString();
    }
    
    static {
        BITFIELD_OP = BitFieldFactory.getInstance(511);
        BITFIELD_SIZECODE = BitFieldFactory.getInstance(57344);
        BITFIELD_SPECIAL = BitFieldFactory.getInstance(512);
        BITFIELD_TYPE = BitFieldFactory.getInstance(7168);
    }
}
