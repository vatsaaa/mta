// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hwpf.sprm;

import org.apache.poi.util.POILogFactory;
import org.apache.poi.util.LittleEndian;
import org.apache.poi.hwpf.usermodel.TableCellDescriptor;
import org.apache.poi.hwpf.usermodel.BorderCode;
import org.apache.poi.hwpf.usermodel.TableProperties;
import org.apache.poi.util.POILogger;
import org.apache.poi.util.Internal;

@Internal
public final class TableSprmUncompressor extends SprmUncompressor
{
    private static final POILogger logger;
    
    @Deprecated
    public static TableProperties uncompressTAP(final byte[] grpprl, final int offset) {
        final TableProperties newProperties = new TableProperties();
        final SprmIterator sprmIt = new SprmIterator(grpprl, offset);
        while (sprmIt.hasNext()) {
            final SprmOperation sprm = sprmIt.next();
            if (sprm.getType() == 5) {
                try {
                    unCompressTAPOperation(newProperties, sprm);
                }
                catch (ArrayIndexOutOfBoundsException ex) {
                    TableSprmUncompressor.logger.log(POILogger.ERROR, "Unable to apply ", sprm, ": ", ex, ex);
                }
            }
        }
        return newProperties;
    }
    
    public static TableProperties uncompressTAP(final SprmBuffer sprmBuffer) {
        final SprmOperation sprmOperation = sprmBuffer.findSprm((short)(-10744));
        TableProperties tableProperties;
        if (sprmOperation != null) {
            final byte[] grpprl = sprmOperation.getGrpprl();
            final int offset = sprmOperation.getGrpprlOffset();
            final short itcMac = grpprl[offset];
            tableProperties = new TableProperties(itcMac);
        }
        else {
            TableSprmUncompressor.logger.log(POILogger.WARN, "Some table rows didn't specify number of columns in SPRMs");
            tableProperties = new TableProperties((short)1);
        }
        for (final SprmOperation sprm : sprmBuffer) {
            if (sprm.getType() == 5) {
                try {
                    unCompressTAPOperation(tableProperties, sprm);
                }
                catch (ArrayIndexOutOfBoundsException ex) {
                    TableSprmUncompressor.logger.log(POILogger.ERROR, "Unable to apply ", sprm, ": ", ex, ex);
                }
            }
        }
        return tableProperties;
    }
    
    static void unCompressTAPOperation(final TableProperties newTAP, final SprmOperation sprm) {
        switch (sprm.getOperation()) {
            case 0: {
                newTAP.setJc((short)sprm.getOperand());
                break;
            }
            case 1: {
                final short[] rgdxaCenter = newTAP.getRgdxaCenter();
                final short itcMac = newTAP.getItcMac();
                final int adjust = sprm.getOperand() - (rgdxaCenter[0] + newTAP.getDxaGapHalf());
                for (int x = 0; x < itcMac; ++x) {
                    final short[] array = rgdxaCenter;
                    final int n = x;
                    array[n] += (short)adjust;
                }
                break;
            }
            case 2: {
                final short[] rgdxaCenter = newTAP.getRgdxaCenter();
                if (rgdxaCenter != null) {
                    final int adjust2 = newTAP.getDxaGapHalf() - sprm.getOperand();
                    final short[] array2 = rgdxaCenter;
                    final int n2 = 0;
                    array2[n2] += (short)adjust2;
                }
                newTAP.setDxaGapHalf(sprm.getOperand());
                break;
            }
            case 3: {
                newTAP.setFCantSplit(SprmUncompressor.getFlag(sprm.getOperand()));
                break;
            }
            case 4: {
                newTAP.setFTableHeader(SprmUncompressor.getFlag(sprm.getOperand()));
                break;
            }
            case 5: {
                final byte[] buf = sprm.getGrpprl();
                int offset = sprm.getGrpprlOffset();
                newTAP.setBrcTop(new BorderCode(buf, offset));
                offset += 4;
                newTAP.setBrcLeft(new BorderCode(buf, offset));
                offset += 4;
                newTAP.setBrcBottom(new BorderCode(buf, offset));
                offset += 4;
                newTAP.setBrcRight(new BorderCode(buf, offset));
                offset += 4;
                newTAP.setBrcHorizontal(new BorderCode(buf, offset));
                offset += 4;
                newTAP.setBrcVertical(new BorderCode(buf, offset));
            }
            case 7: {
                newTAP.setDyaRowHeight(sprm.getOperand());
                break;
            }
            case 8: {
                final byte[] grpprl = sprm.getGrpprl();
                final int offset = sprm.getGrpprlOffset();
                final short itcMac2 = grpprl[offset];
                final short[] rgdxaCenter2 = new short[itcMac2 + 1];
                final TableCellDescriptor[] rgtc = new TableCellDescriptor[itcMac2];
                newTAP.setItcMac(itcMac2);
                newTAP.setRgdxaCenter(rgdxaCenter2);
                newTAP.setRgtc(rgtc);
                for (int x2 = 0; x2 < itcMac2; ++x2) {
                    rgdxaCenter2[x2] = LittleEndian.getShort(grpprl, offset + (1 + x2 * 2));
                }
                final int endOfSprm = offset + sprm.size() - 6;
                final int startOfTCs = offset + (1 + (itcMac2 + 1) * 2);
                final boolean hasTCs = startOfTCs < endOfSprm;
                for (int x3 = 0; x3 < itcMac2; ++x3) {
                    if (hasTCs && offset + (1 + (itcMac2 + 1) * 2 + x3 * 20) < grpprl.length) {
                        rgtc[x3] = TableCellDescriptor.convertBytesToTC(grpprl, offset + (1 + (itcMac2 + 1) * 2 + x3 * 20));
                    }
                    else {
                        rgtc[x3] = new TableCellDescriptor();
                    }
                }
                rgdxaCenter2[itcMac2] = LittleEndian.getShort(grpprl, offset + (1 + itcMac2 * 2));
            }
            case 9: {}
            case 10: {}
            case 33: {
                final int param = sprm.getOperand();
                int index = (param & 0xFF000000) >> 24;
                final int count = (param & 0xFF0000) >> 16;
                final int width = param & 0xFFFF;
                final int itcMac3 = newTAP.getItcMac();
                final short[] rgdxaCenter3 = new short[itcMac3 + count + 1];
                final TableCellDescriptor[] rgtc2 = new TableCellDescriptor[itcMac3 + count];
                if (index >= itcMac3) {
                    index = itcMac3;
                    System.arraycopy(newTAP.getRgdxaCenter(), 0, rgdxaCenter3, 0, itcMac3 + 1);
                    System.arraycopy(newTAP.getRgtc(), 0, rgtc2, 0, itcMac3);
                }
                else {
                    System.arraycopy(newTAP.getRgdxaCenter(), 0, rgdxaCenter3, 0, index + 1);
                    System.arraycopy(newTAP.getRgdxaCenter(), index + 1, rgdxaCenter3, index + count, itcMac3 - index);
                    System.arraycopy(newTAP.getRgtc(), 0, rgtc2, 0, index);
                    System.arraycopy(newTAP.getRgtc(), index, rgtc2, index + count, itcMac3 - index);
                }
                for (int x4 = index; x4 < index + count; ++x4) {
                    rgtc2[x4] = new TableCellDescriptor();
                    rgdxaCenter3[x4] = (short)(rgdxaCenter3[x4 - 1] + width);
                }
                rgdxaCenter3[index + count] = (short)(rgdxaCenter3[index + count - 1] + width);
            }
            case 52: {
                final byte itcFirst = sprm.getGrpprl()[sprm.getGrpprlOffset()];
                final byte itcLim = sprm.getGrpprl()[sprm.getGrpprlOffset() + 1];
                final byte grfbrc = sprm.getGrpprl()[sprm.getGrpprlOffset() + 2];
                final byte ftsWidth = sprm.getGrpprl()[sprm.getGrpprlOffset() + 3];
                final short wWidth = LittleEndian.getShort(sprm.getGrpprl(), sprm.getGrpprlOffset() + 4);
                for (int c = itcFirst; c < itcLim; ++c) {
                    final TableCellDescriptor tableCellDescriptor = newTAP.getRgtc()[c];
                    if ((grfbrc & 0x1) != 0x0) {
                        tableCellDescriptor.setFtsCellPaddingTop(ftsWidth);
                        tableCellDescriptor.setWCellPaddingTop(wWidth);
                    }
                    if ((grfbrc & 0x2) != 0x0) {
                        tableCellDescriptor.setFtsCellPaddingLeft(ftsWidth);
                        tableCellDescriptor.setWCellPaddingLeft(wWidth);
                    }
                    if ((grfbrc & 0x4) != 0x0) {
                        tableCellDescriptor.setFtsCellPaddingBottom(ftsWidth);
                        tableCellDescriptor.setWCellPaddingBottom(wWidth);
                    }
                    if ((grfbrc & 0x8) != 0x0) {
                        tableCellDescriptor.setFtsCellPaddingRight(ftsWidth);
                        tableCellDescriptor.setWCellPaddingRight(wWidth);
                    }
                }
                break;
            }
        }
    }
    
    static {
        logger = POILogFactory.getLogger(TableSprmUncompressor.class);
    }
}
