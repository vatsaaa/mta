// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hwpf.sprm;

import org.apache.poi.util.Internal;

@Internal
public abstract class SprmUncompressor
{
    protected SprmUncompressor() {
    }
    
    public static boolean getFlag(final int x) {
        return x != 0;
    }
}
