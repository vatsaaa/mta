// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hwpf.sprm;

import org.apache.poi.hwpf.usermodel.BorderCode;
import org.apache.poi.hwpf.usermodel.SectionProperties;
import org.apache.poi.util.Internal;

@Internal
public final class SectionSprmUncompressor extends SprmUncompressor
{
    public static SectionProperties uncompressSEP(final byte[] grpprl, final int offset) {
        final SectionProperties newProperties = new SectionProperties();
        final SprmIterator sprmIt = new SprmIterator(grpprl, offset);
        while (sprmIt.hasNext()) {
            final SprmOperation sprm = sprmIt.next();
            unCompressSEPOperation(newProperties, sprm);
        }
        return newProperties;
    }
    
    static void unCompressSEPOperation(final SectionProperties newSEP, final SprmOperation sprm) {
        switch (sprm.getOperation()) {
            case 0: {
                newSEP.setCnsPgn((byte)sprm.getOperand());
                break;
            }
            case 1: {
                newSEP.setIHeadingPgn((byte)sprm.getOperand());
                break;
            }
            case 2: {
                final byte[] buf = new byte[sprm.size() - 3];
                System.arraycopy(sprm.getGrpprl(), sprm.getGrpprlOffset(), buf, 0, buf.length);
                newSEP.setOlstAnm(buf);
            }
            case 3: {}
            case 5: {
                newSEP.setFEvenlySpaced(SprmUncompressor.getFlag(sprm.getOperand()));
                break;
            }
            case 6: {
                newSEP.setFUnlocked(SprmUncompressor.getFlag(sprm.getOperand()));
                break;
            }
            case 7: {
                newSEP.setDmBinFirst((short)sprm.getOperand());
                break;
            }
            case 8: {
                newSEP.setDmBinOther((short)sprm.getOperand());
                break;
            }
            case 9: {
                newSEP.setBkc((byte)sprm.getOperand());
                break;
            }
            case 10: {
                newSEP.setFTitlePage(SprmUncompressor.getFlag(sprm.getOperand()));
                break;
            }
            case 11: {
                newSEP.setCcolM1((short)sprm.getOperand());
                break;
            }
            case 12: {
                newSEP.setDxaColumns(sprm.getOperand());
                break;
            }
            case 13: {
                newSEP.setFAutoPgn(SprmUncompressor.getFlag(sprm.getOperand()));
                break;
            }
            case 14: {
                newSEP.setNfcPgn((byte)sprm.getOperand());
                break;
            }
            case 15: {
                newSEP.setDyaPgn((short)sprm.getOperand());
                break;
            }
            case 16: {
                newSEP.setDxaPgn((short)sprm.getOperand());
                break;
            }
            case 17: {
                newSEP.setFPgnRestart(SprmUncompressor.getFlag(sprm.getOperand()));
                break;
            }
            case 18: {
                newSEP.setFEndNote(SprmUncompressor.getFlag(sprm.getOperand()));
                break;
            }
            case 19: {
                newSEP.setLnc((byte)sprm.getOperand());
                break;
            }
            case 20: {
                newSEP.setGrpfIhdt((byte)sprm.getOperand());
                break;
            }
            case 21: {
                newSEP.setNLnnMod((short)sprm.getOperand());
                break;
            }
            case 22: {
                newSEP.setDxaLnn(sprm.getOperand());
                break;
            }
            case 23: {
                newSEP.setDyaHdrTop(sprm.getOperand());
                break;
            }
            case 24: {
                newSEP.setDyaHdrBottom(sprm.getOperand());
                break;
            }
            case 25: {
                newSEP.setFLBetween(SprmUncompressor.getFlag(sprm.getOperand()));
                break;
            }
            case 26: {
                newSEP.setVjc((byte)sprm.getOperand());
                break;
            }
            case 27: {
                newSEP.setLnnMin((short)sprm.getOperand());
                break;
            }
            case 28: {
                newSEP.setPgnStart((short)sprm.getOperand());
                break;
            }
            case 29: {
                newSEP.setDmOrientPage(sprm.getOperand() != 0);
            }
            case 31: {
                newSEP.setXaPage(sprm.getOperand());
                break;
            }
            case 32: {
                newSEP.setYaPage(sprm.getOperand());
                break;
            }
            case 33: {
                newSEP.setDxaLeft(sprm.getOperand());
                break;
            }
            case 34: {
                newSEP.setDxaRight(sprm.getOperand());
                break;
            }
            case 35: {
                newSEP.setDyaTop(sprm.getOperand());
                break;
            }
            case 36: {
                newSEP.setDyaBottom(sprm.getOperand());
                break;
            }
            case 37: {
                newSEP.setDzaGutter(sprm.getOperand());
                break;
            }
            case 38: {
                newSEP.setDmPaperReq((short)sprm.getOperand());
                break;
            }
            case 39: {
                newSEP.setFPropMark(SprmUncompressor.getFlag(sprm.getOperand()));
            }
            case 40: {}
            case 41: {}
            case 43: {
                newSEP.setBrcTop(new BorderCode(sprm.getGrpprl(), sprm.getGrpprlOffset()));
                break;
            }
            case 44: {
                newSEP.setBrcLeft(new BorderCode(sprm.getGrpprl(), sprm.getGrpprlOffset()));
                break;
            }
            case 45: {
                newSEP.setBrcBottom(new BorderCode(sprm.getGrpprl(), sprm.getGrpprlOffset()));
                break;
            }
            case 46: {
                newSEP.setBrcRight(new BorderCode(sprm.getGrpprl(), sprm.getGrpprlOffset()));
                break;
            }
            case 47: {
                newSEP.setPgbProp(sprm.getOperand());
                break;
            }
            case 48: {
                newSEP.setDxtCharSpace(sprm.getOperand());
                break;
            }
            case 49: {
                newSEP.setDyaLinePitch(sprm.getOperand());
                break;
            }
            case 51: {
                newSEP.setWTextFlow((short)sprm.getOperand());
                break;
            }
        }
    }
}
