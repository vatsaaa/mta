// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hwpf.sprm;

import java.util.Arrays;
import org.apache.poi.util.LittleEndian;
import org.apache.poi.util.Internal;

@Internal
public final class SprmBuffer implements Cloneable
{
    byte[] _buf;
    boolean _istd;
    int _offset;
    private final int _sprmsStartOffset;
    
    @Deprecated
    public SprmBuffer() {
        this(0);
    }
    
    @Deprecated
    public SprmBuffer(final byte[] buf) {
        this(buf, 0);
    }
    
    @Deprecated
    public SprmBuffer(final byte[] buf, final boolean istd) {
        this(buf, istd, 0);
    }
    
    public SprmBuffer(final byte[] buf, final boolean istd, final int sprmsStartOffset) {
        this._offset = buf.length;
        this._buf = buf;
        this._istd = istd;
        this._sprmsStartOffset = sprmsStartOffset;
    }
    
    public SprmBuffer(final byte[] buf, final int _sprmsStartOffset) {
        this(buf, false, _sprmsStartOffset);
    }
    
    public SprmBuffer(final int sprmsStartOffset) {
        this._buf = new byte[sprmsStartOffset + 4];
        this._offset = sprmsStartOffset;
        this._sprmsStartOffset = sprmsStartOffset;
    }
    
    public void addSprm(final short opcode, final byte operand) {
        final int addition = 3;
        this.ensureCapacity(addition);
        LittleEndian.putShort(this._buf, this._offset, opcode);
        this._offset += 2;
        this._buf[this._offset++] = operand;
    }
    
    public void addSprm(final short opcode, final byte[] operand) {
        final int addition = 3 + operand.length;
        this.ensureCapacity(addition);
        LittleEndian.putShort(this._buf, this._offset, opcode);
        this._offset += 2;
        this._buf[this._offset++] = (byte)operand.length;
        System.arraycopy(operand, 0, this._buf, this._offset, operand.length);
    }
    
    public void addSprm(final short opcode, final int operand) {
        final int addition = 6;
        this.ensureCapacity(addition);
        LittleEndian.putShort(this._buf, this._offset, opcode);
        this._offset += 2;
        LittleEndian.putInt(this._buf, this._offset, operand);
        this._offset += 4;
    }
    
    public void addSprm(final short opcode, final short operand) {
        final int addition = 4;
        this.ensureCapacity(addition);
        LittleEndian.putShort(this._buf, this._offset, opcode);
        this._offset += 2;
        LittleEndian.putShort(this._buf, this._offset, operand);
        this._offset += 2;
    }
    
    public void append(final byte[] grpprl) {
        this.append(grpprl, 0);
    }
    
    public void append(final byte[] grpprl, final int offset) {
        this.ensureCapacity(grpprl.length - offset);
        System.arraycopy(grpprl, offset, this._buf, this._offset, grpprl.length - offset);
        this._offset += grpprl.length - offset;
    }
    
    public Object clone() throws CloneNotSupportedException {
        final SprmBuffer retVal = (SprmBuffer)super.clone();
        retVal._buf = new byte[this._buf.length];
        System.arraycopy(this._buf, 0, retVal._buf, 0, this._buf.length);
        return retVal;
    }
    
    private void ensureCapacity(final int addition) {
        if (this._offset + addition >= this._buf.length) {
            final byte[] newBuf = new byte[this._offset + addition];
            System.arraycopy(this._buf, 0, newBuf, 0, this._buf.length);
            this._buf = newBuf;
        }
    }
    
    @Override
    public boolean equals(final Object obj) {
        final SprmBuffer sprmBuf = (SprmBuffer)obj;
        return Arrays.equals(this._buf, sprmBuf._buf);
    }
    
    public SprmOperation findSprm(final short opcode) {
        final int operation = SprmOperation.getOperationFromOpcode(opcode);
        final int type = SprmOperation.getTypeFromOpcode(opcode);
        final SprmIterator si = new SprmIterator(this._buf, 2);
        while (si.hasNext()) {
            final SprmOperation i = si.next();
            if (i.getOperation() == operation && i.getType() == type) {
                return i;
            }
        }
        return null;
    }
    
    private int findSprmOffset(final short opcode) {
        final SprmOperation sprmOperation = this.findSprm(opcode);
        if (sprmOperation == null) {
            return -1;
        }
        return sprmOperation.getGrpprlOffset();
    }
    
    public byte[] toByteArray() {
        return this._buf;
    }
    
    public SprmIterator iterator() {
        return new SprmIterator(this._buf, this._sprmsStartOffset);
    }
    
    public void updateSprm(final short opcode, final byte operand) {
        final int grpprlOffset = this.findSprmOffset(opcode);
        if (grpprlOffset != -1) {
            this._buf[grpprlOffset] = operand;
            return;
        }
        this.addSprm(opcode, operand);
    }
    
    public void updateSprm(final short opcode, final boolean operand) {
        final int grpprlOffset = this.findSprmOffset(opcode);
        if (grpprlOffset != -1) {
            this._buf[grpprlOffset] = (byte)(operand ? 1 : 0);
            return;
        }
        this.addSprm(opcode, operand ? 1 : 0);
    }
    
    public void updateSprm(final short opcode, final int operand) {
        final int grpprlOffset = this.findSprmOffset(opcode);
        if (grpprlOffset != -1) {
            LittleEndian.putInt(this._buf, grpprlOffset, operand);
            return;
        }
        this.addSprm(opcode, operand);
    }
    
    public void updateSprm(final short opcode, final short operand) {
        final int grpprlOffset = this.findSprmOffset(opcode);
        if (grpprlOffset != -1) {
            LittleEndian.putShort(this._buf, grpprlOffset, operand);
            return;
        }
        this.addSprm(opcode, operand);
    }
    
    @Override
    public String toString() {
        final StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Sprms (");
        stringBuilder.append(this._buf.length);
        stringBuilder.append(" byte(s)): ");
        final SprmIterator iterator = this.iterator();
        while (iterator.hasNext()) {
            try {
                stringBuilder.append(iterator.next());
            }
            catch (Exception exc) {
                stringBuilder.append("error");
            }
            stringBuilder.append("; ");
        }
        return stringBuilder.toString();
    }
}
