// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hwpf.sprm;

import java.util.List;
import org.apache.poi.util.LittleEndian;
import java.util.ArrayList;
import org.apache.poi.hwpf.usermodel.CharacterProperties;
import org.apache.poi.util.Internal;

@Internal
public final class CharacterSprmCompressor
{
    public static byte[] compressCharacterProperty(final CharacterProperties newCHP, final CharacterProperties oldCHP) {
        final List<byte[]> sprmList = new ArrayList<byte[]>();
        int size = 0;
        if (newCHP.isFRMarkDel() != oldCHP.isFRMarkDel()) {
            int value = 0;
            if (newCHP.isFRMarkDel()) {
                value = 1;
            }
            size += SprmUtils.addSprm((short)2048, value, null, sprmList);
        }
        if (newCHP.isFRMark() != oldCHP.isFRMark()) {
            int value = 0;
            if (newCHP.isFRMark()) {
                value = 1;
            }
            size += SprmUtils.addSprm((short)2049, value, null, sprmList);
        }
        if (newCHP.isFFldVanish() != oldCHP.isFFldVanish()) {
            int value = 0;
            if (newCHP.isFFldVanish()) {
                value = 1;
            }
            size += SprmUtils.addSprm((short)2050, value, null, sprmList);
        }
        if (newCHP.isFSpec() != oldCHP.isFSpec() || newCHP.getFcPic() != oldCHP.getFcPic()) {
            size += SprmUtils.addSprm((short)27139, newCHP.getFcPic(), null, sprmList);
        }
        if (newCHP.getIbstRMark() != oldCHP.getIbstRMark()) {
            size += SprmUtils.addSprm((short)18436, newCHP.getIbstRMark(), null, sprmList);
        }
        if (!newCHP.getDttmRMark().equals(oldCHP.getDttmRMark())) {
            final byte[] buf = new byte[4];
            newCHP.getDttmRMark().serialize(buf, 0);
            size += SprmUtils.addSprm((short)26629, LittleEndian.getInt(buf), null, sprmList);
        }
        if (newCHP.isFData() != oldCHP.isFData()) {
            int value = 0;
            if (newCHP.isFData()) {
                value = 1;
            }
            size += SprmUtils.addSprm((short)2054, value, null, sprmList);
        }
        if (newCHP.isFSpec() && newCHP.getFtcSym() != 0) {
            final byte[] varParam = new byte[4];
            LittleEndian.putShort(varParam, 0, (short)newCHP.getFtcSym());
            LittleEndian.putShort(varParam, 2, (short)newCHP.getXchSym());
            size += SprmUtils.addSprm((short)27145, 0, varParam, sprmList);
        }
        if (newCHP.isFOle2() != newCHP.isFOle2()) {
            int value = 0;
            if (newCHP.isFOle2()) {
                value = 1;
            }
            size += SprmUtils.addSprm((short)2058, value, null, sprmList);
        }
        if (newCHP.getIcoHighlight() != oldCHP.getIcoHighlight()) {
            size += SprmUtils.addSprm((short)10764, newCHP.getIcoHighlight(), null, sprmList);
        }
        if (newCHP.getFcObj() != oldCHP.getFcObj()) {
            size += SprmUtils.addSprm((short)26638, newCHP.getFcObj(), null, sprmList);
        }
        if (newCHP.getIstd() != oldCHP.getIstd()) {
            size += SprmUtils.addSprm((short)18992, newCHP.getIstd(), null, sprmList);
        }
        if (newCHP.isFBold() != oldCHP.isFBold()) {
            int value = 0;
            if (newCHP.isFBold()) {
                value = 1;
            }
            size += SprmUtils.addSprm((short)2101, value, null, sprmList);
        }
        if (newCHP.isFItalic() != oldCHP.isFItalic()) {
            int value = 0;
            if (newCHP.isFItalic()) {
                value = 1;
            }
            size += SprmUtils.addSprm((short)2102, value, null, sprmList);
        }
        if (newCHP.isFStrike() != oldCHP.isFStrike()) {
            int value = 0;
            if (newCHP.isFStrike()) {
                value = 1;
            }
            size += SprmUtils.addSprm((short)2103, value, null, sprmList);
        }
        if (newCHP.isFOutline() != oldCHP.isFOutline()) {
            int value = 0;
            if (newCHP.isFOutline()) {
                value = 1;
            }
            size += SprmUtils.addSprm((short)2104, value, null, sprmList);
        }
        if (newCHP.isFShadow() != oldCHP.isFShadow()) {
            int value = 0;
            if (newCHP.isFShadow()) {
                value = 1;
            }
            size += SprmUtils.addSprm((short)2105, value, null, sprmList);
        }
        if (newCHP.isFSmallCaps() != oldCHP.isFSmallCaps()) {
            int value = 0;
            if (newCHP.isFSmallCaps()) {
                value = 1;
            }
            size += SprmUtils.addSprm((short)2106, value, null, sprmList);
        }
        if (newCHP.isFCaps() != oldCHP.isFCaps()) {
            int value = 0;
            if (newCHP.isFCaps()) {
                value = 1;
            }
            size += SprmUtils.addSprm((short)2107, value, null, sprmList);
        }
        if (newCHP.isFVanish() != oldCHP.isFVanish()) {
            int value = 0;
            if (newCHP.isFVanish()) {
                value = 1;
            }
            size += SprmUtils.addSprm((short)2108, value, null, sprmList);
        }
        if (newCHP.getKul() != oldCHP.getKul()) {
            size += SprmUtils.addSprm((short)10814, newCHP.getKul(), null, sprmList);
        }
        if (newCHP.getDxaSpace() != oldCHP.getDxaSpace()) {
            size += SprmUtils.addSprm((short)(-30656), newCHP.getDxaSpace(), null, sprmList);
        }
        if (newCHP.getIco() != oldCHP.getIco()) {
            size += SprmUtils.addSprm((short)10818, newCHP.getIco(), null, sprmList);
        }
        if (newCHP.getHps() != oldCHP.getHps()) {
            size += SprmUtils.addSprm((short)19011, newCHP.getHps(), null, sprmList);
        }
        if (newCHP.getHpsPos() != oldCHP.getHpsPos()) {
            size += SprmUtils.addSprm((short)18501, newCHP.getHpsPos(), null, sprmList);
        }
        if (newCHP.getHpsKern() != oldCHP.getHpsKern()) {
            size += SprmUtils.addSprm((short)18507, newCHP.getHpsKern(), null, sprmList);
        }
        if (newCHP.getHresi().equals(oldCHP.getHresi())) {
            size += SprmUtils.addSprm((short)18510, newCHP.getHresi().getValue(), null, sprmList);
        }
        if (newCHP.getFtcAscii() != oldCHP.getFtcAscii()) {
            size += SprmUtils.addSprm((short)19023, newCHP.getFtcAscii(), null, sprmList);
        }
        if (newCHP.getFtcFE() != oldCHP.getFtcFE()) {
            size += SprmUtils.addSprm((short)19024, newCHP.getFtcFE(), null, sprmList);
        }
        if (newCHP.getFtcOther() != oldCHP.getFtcOther()) {
            size += SprmUtils.addSprm((short)19025, newCHP.getFtcOther(), null, sprmList);
        }
        if (newCHP.isFDStrike() != oldCHP.isFDStrike()) {
            int value = 0;
            if (newCHP.isFDStrike()) {
                value = 1;
            }
            size += SprmUtils.addSprm((short)10835, value, null, sprmList);
        }
        if (newCHP.isFImprint() != oldCHP.isFImprint()) {
            int value = 0;
            if (newCHP.isFImprint()) {
                value = 1;
            }
            size += SprmUtils.addSprm((short)2132, value, null, sprmList);
        }
        if (newCHP.isFSpec() != oldCHP.isFSpec()) {
            int value = 0;
            if (newCHP.isFSpec()) {
                value = 1;
            }
            size += SprmUtils.addSprm((short)2133, value, null, sprmList);
        }
        if (newCHP.isFObj() != oldCHP.isFObj()) {
            int value = 0;
            if (newCHP.isFObj()) {
                value = 1;
            }
            size += SprmUtils.addSprm((short)2134, value, null, sprmList);
        }
        if (newCHP.isFEmboss() != oldCHP.isFEmboss()) {
            int value = 0;
            if (newCHP.isFEmboss()) {
                value = 1;
            }
            size += SprmUtils.addSprm((short)2136, value, null, sprmList);
        }
        if (newCHP.getSfxtText() != oldCHP.getSfxtText()) {
            size += SprmUtils.addSprm((short)10329, newCHP.getSfxtText(), null, sprmList);
        }
        if (!newCHP.getCv().equals(oldCHP.getCv()) && !newCHP.getCv().isEmpty()) {
            size += SprmUtils.addSprm((short)26736, newCHP.getCv().getValue(), null, sprmList);
        }
        return SprmUtils.getGrpprl(sprmList, size);
    }
}
