// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hwpf.sprm;

import java.util.List;
import org.apache.poi.util.LittleEndian;
import org.apache.poi.util.Internal;

@Internal
public final class SprmUtils
{
    public static byte[] shortArrayToByteArray(final short[] convert) {
        final byte[] buf = new byte[convert.length * 2];
        for (int x = 0; x < convert.length; ++x) {
            LittleEndian.putShort(buf, x * 2, convert[x]);
        }
        return buf;
    }
    
    public static int addSpecialSprm(final short instruction, final byte[] varParam, final List<byte[]> list) {
        final byte[] sprm = new byte[varParam.length + 4];
        System.arraycopy(varParam, 0, sprm, 4, varParam.length);
        LittleEndian.putShort(sprm, instruction);
        LittleEndian.putShort(sprm, 2, (short)(varParam.length + 1));
        list.add(sprm);
        return sprm.length;
    }
    
    public static int addSprm(final short instruction, final boolean param, final List<byte[]> list) {
        return addSprm(instruction, param ? 1 : 0, null, list);
    }
    
    public static int addSprm(final short instruction, final int param, final byte[] varParam, final List<byte[]> list) {
        final int type = (instruction & 0xE000) >> 13;
        byte[] sprm = null;
        switch (type) {
            case 0:
            case 1: {
                sprm = new byte[] { 0, 0, (byte)param };
                break;
            }
            case 2: {
                sprm = new byte[4];
                LittleEndian.putShort(sprm, 2, (short)param);
                break;
            }
            case 3: {
                sprm = new byte[6];
                LittleEndian.putInt(sprm, 2, param);
                break;
            }
            case 4:
            case 5: {
                sprm = new byte[4];
                LittleEndian.putShort(sprm, 2, (short)param);
                break;
            }
            case 6: {
                sprm = new byte[3 + varParam.length];
                sprm[2] = (byte)varParam.length;
                System.arraycopy(varParam, 0, sprm, 3, varParam.length);
                break;
            }
            case 7: {
                sprm = new byte[5];
                final byte[] temp = new byte[4];
                LittleEndian.putInt(temp, 0, param);
                System.arraycopy(temp, 0, sprm, 2, 3);
                break;
            }
        }
        LittleEndian.putShort(sprm, 0, instruction);
        list.add(sprm);
        return sprm.length;
    }
    
    public static byte[] getGrpprl(final List<byte[]> sprmList, final int size) {
        final byte[] grpprl = new byte[size];
        int listSize = sprmList.size() - 1;
        int index = 0;
        while (listSize >= 0) {
            final byte[] sprm = sprmList.remove(0);
            System.arraycopy(sprm, 0, grpprl, index, sprm.length);
            index += sprm.length;
            --listSize;
        }
        return grpprl;
    }
    
    public static int convertBrcToInt(final short[] brc) {
        final byte[] buf = new byte[4];
        LittleEndian.putShort(buf, brc[0]);
        LittleEndian.putShort(buf, 2, brc[1]);
        return LittleEndian.getInt(buf);
    }
}
