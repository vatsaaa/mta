// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hwpf.sprm;

import org.apache.poi.util.Internal;

@Internal
public final class SprmIterator
{
    private byte[] _grpprl;
    int _offset;
    
    public SprmIterator(final byte[] grpprl, final int offset) {
        this._grpprl = grpprl;
        this._offset = offset;
    }
    
    public boolean hasNext() {
        return this._offset < this._grpprl.length - 1;
    }
    
    public SprmOperation next() {
        final SprmOperation op = new SprmOperation(this._grpprl, this._offset);
        this._offset += op.size();
        return op;
    }
}
