// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hwpf.sprm;

import org.apache.poi.util.POILogFactory;
import org.apache.poi.hwpf.model.Colorref;
import org.apache.poi.hwpf.usermodel.ShadingDescriptor;
import org.apache.poi.hwpf.usermodel.BorderCode;
import org.apache.poi.hwpf.model.Hyphenation;
import org.apache.poi.util.LittleEndian;
import org.apache.poi.hwpf.usermodel.DateAndTime;
import org.apache.poi.hwpf.usermodel.CharacterProperties;
import org.apache.poi.util.POILogger;
import org.apache.poi.util.Internal;

@Internal
public final class CharacterSprmUncompressor extends SprmUncompressor
{
    private static final POILogger logger;
    
    public static CharacterProperties uncompressCHP(final CharacterProperties parent, final byte[] grpprl, final int offset) {
        CharacterProperties newProperties = null;
        try {
            newProperties = (CharacterProperties)parent.clone();
        }
        catch (CloneNotSupportedException cnse) {
            throw new RuntimeException("There is no way this exception should happen!!");
        }
        final SprmIterator sprmIt = new SprmIterator(grpprl, offset);
        while (sprmIt.hasNext()) {
            final SprmOperation sprm = sprmIt.next();
            if (sprm.getType() != 2) {
                CharacterSprmUncompressor.logger.log(POILogger.WARN, "Non-CHP SPRM returned by SprmIterator: " + sprm);
            }
            else {
                unCompressCHPOperation(parent, newProperties, sprm);
            }
        }
        return newProperties;
    }
    
    static void unCompressCHPOperation(final CharacterProperties oldCHP, CharacterProperties newCHP, final SprmOperation sprm) {
        switch (sprm.getOperation()) {
            case 0: {
                newCHP.setFRMarkDel(SprmUncompressor.getFlag(sprm.getOperand()));
                break;
            }
            case 1: {
                newCHP.setFRMark(SprmUncompressor.getFlag(sprm.getOperand()));
                break;
            }
            case 2: {
                newCHP.setFFldVanish(SprmUncompressor.getFlag(sprm.getOperand()));
                break;
            }
            case 3: {
                newCHP.setFcPic(sprm.getOperand());
                newCHP.setFSpec(true);
                break;
            }
            case 4: {
                newCHP.setIbstRMark((short)sprm.getOperand());
                break;
            }
            case 5: {
                newCHP.setDttmRMark(new DateAndTime(sprm.getGrpprl(), sprm.getGrpprlOffset()));
                break;
            }
            case 6: {
                newCHP.setFData(SprmUncompressor.getFlag(sprm.getOperand()));
                break;
            }
            case 7: {
                break;
            }
            case 8: {
                final int operand = sprm.getOperand();
                final short chsDiff = (short)(operand & 0xFF);
                newCHP.setFChsDiff(SprmUncompressor.getFlag(chsDiff));
                newCHP.setChse((short)(operand & 0xFFFF00));
                break;
            }
            case 9: {
                newCHP.setFSpec(true);
                newCHP.setFtcSym(LittleEndian.getShort(sprm.getGrpprl(), sprm.getGrpprlOffset()));
                newCHP.setXchSym(LittleEndian.getShort(sprm.getGrpprl(), sprm.getGrpprlOffset() + 2));
                break;
            }
            case 10: {
                newCHP.setFOle2(SprmUncompressor.getFlag(sprm.getOperand()));
                break;
            }
            case 11: {
                break;
            }
            case 12: {
                newCHP.setIcoHighlight((byte)sprm.getOperand());
                newCHP.setFHighlight(SprmUncompressor.getFlag(sprm.getOperand()));
                break;
            }
            case 13: {
                break;
            }
            case 14: {
                newCHP.setFcObj(sprm.getOperand());
                break;
            }
            case 15: {
                break;
            }
            case 16: {
                break;
            }
            case 17: {
                break;
            }
            case 18: {
                break;
            }
            case 19: {
                break;
            }
            case 20: {
                break;
            }
            case 21: {
                break;
            }
            case 22: {
                break;
            }
            case 23: {
                break;
            }
            case 24: {
                break;
            }
            case 25: {
                break;
            }
            case 26: {
                break;
            }
            case 27: {
                break;
            }
            case 28: {
                break;
            }
            case 29: {
                break;
            }
            case 30: {
                break;
            }
            case 31: {
                break;
            }
            case 32: {
                break;
            }
            case 33: {
                break;
            }
            case 34: {
                break;
            }
            case 35: {
                break;
            }
            case 36: {
                break;
            }
            case 37: {
                break;
            }
            case 38: {
                break;
            }
            case 39: {
                break;
            }
            case 40: {
                break;
            }
            case 41: {
                break;
            }
            case 42: {
                break;
            }
            case 43: {
                break;
            }
            case 44: {
                break;
            }
            case 45: {
                break;
            }
            case 46: {
                break;
            }
            case 47: {
                break;
            }
            case 48: {
                newCHP.setIstd(sprm.getOperand());
                break;
            }
            case 49: {
                break;
            }
            case 50: {
                newCHP.setFBold(false);
                newCHP.setFItalic(false);
                newCHP.setFOutline(false);
                newCHP.setFStrike(false);
                newCHP.setFShadow(false);
                newCHP.setFSmallCaps(false);
                newCHP.setFCaps(false);
                newCHP.setFVanish(false);
                newCHP.setKul((byte)0);
                newCHP.setIco((byte)0);
                break;
            }
            case 51: {
                try {
                    final boolean fSpec = newCHP.isFSpec();
                    newCHP = (CharacterProperties)oldCHP.clone();
                    newCHP.setFSpec(fSpec);
                }
                catch (CloneNotSupportedException ex) {}
            }
            case 52: {
                break;
            }
            case 53: {
                newCHP.setFBold(getCHPFlag((byte)sprm.getOperand(), oldCHP.isFBold()));
                break;
            }
            case 54: {
                newCHP.setFItalic(getCHPFlag((byte)sprm.getOperand(), oldCHP.isFItalic()));
                break;
            }
            case 55: {
                newCHP.setFStrike(getCHPFlag((byte)sprm.getOperand(), oldCHP.isFStrike()));
                break;
            }
            case 56: {
                newCHP.setFOutline(getCHPFlag((byte)sprm.getOperand(), oldCHP.isFOutline()));
                break;
            }
            case 57: {
                newCHP.setFShadow(getCHPFlag((byte)sprm.getOperand(), oldCHP.isFShadow()));
                break;
            }
            case 58: {
                newCHP.setFSmallCaps(getCHPFlag((byte)sprm.getOperand(), oldCHP.isFSmallCaps()));
                break;
            }
            case 59: {
                newCHP.setFCaps(getCHPFlag((byte)sprm.getOperand(), oldCHP.isFCaps()));
                break;
            }
            case 60: {
                newCHP.setFVanish(getCHPFlag((byte)sprm.getOperand(), oldCHP.isFVanish()));
                break;
            }
            case 61: {
                newCHP.setFtcAscii((short)sprm.getOperand());
                break;
            }
            case 62: {
                newCHP.setKul((byte)sprm.getOperand());
                break;
            }
            case 63: {
                final int operand = sprm.getOperand();
                final int hps = operand & 0xFF;
                if (hps != 0) {
                    newCHP.setHps(hps);
                }
                byte cInc = (byte)((operand & 0xFF00) >>> 8);
                cInc >>>= 1;
                if (cInc != 0) {
                    newCHP.setHps(Math.max(newCHP.getHps() + cInc * 2, 2));
                }
                final byte hpsPos = (byte)((operand & 0xFF0000) >>> 16);
                if (hpsPos != 128) {
                    newCHP.setHpsPos(hpsPos);
                }
                final boolean fAdjust = (operand & 0x100) > 0;
                if (fAdjust && hpsPos != 128 && hpsPos != 0 && oldCHP.getHpsPos() == 0) {
                    newCHP.setHps(Math.max(newCHP.getHps() - 2, 2));
                }
                if (fAdjust && hpsPos == 0 && oldCHP.getHpsPos() != 0) {
                    newCHP.setHps(Math.max(newCHP.getHps() + 2, 2));
                    break;
                }
                break;
            }
            case 64: {
                newCHP.setDxaSpace(sprm.getOperand());
                break;
            }
            case 65: {
                newCHP.setLidDefault((short)sprm.getOperand());
                break;
            }
            case 66: {
                newCHP.setIco((byte)sprm.getOperand());
                break;
            }
            case 67: {
                newCHP.setHps(sprm.getOperand());
                break;
            }
            case 68: {
                final byte hpsLvl = (byte)sprm.getOperand();
                newCHP.setHps(Math.max(newCHP.getHps() + hpsLvl * 2, 2));
                break;
            }
            case 69: {
                newCHP.setHpsPos((short)sprm.getOperand());
                break;
            }
            case 70: {
                if (sprm.getOperand() != 0) {
                    if (oldCHP.getHpsPos() == 0) {
                        newCHP.setHps(Math.max(newCHP.getHps() - 2, 2));
                        break;
                    }
                    break;
                }
                else {
                    if (oldCHP.getHpsPos() != 0) {
                        newCHP.setHps(Math.max(newCHP.getHps() + 2, 2));
                        break;
                    }
                    break;
                }
                break;
            }
            case 71: {
                break;
            }
            case 72: {
                newCHP.setIss((byte)sprm.getOperand());
                break;
            }
            case 73: {
                newCHP.setHps(LittleEndian.getShort(sprm.getGrpprl(), sprm.getGrpprlOffset()));
                break;
            }
            case 74: {
                final int increment = LittleEndian.getShort(sprm.getGrpprl(), sprm.getGrpprlOffset());
                newCHP.setHps(Math.max(newCHP.getHps() + increment, 8));
                break;
            }
            case 75: {
                newCHP.setHpsKern(sprm.getOperand());
                break;
            }
            case 76: {
                break;
            }
            case 77: {
                final float percentage = sprm.getOperand() / 100.0f;
                final int add = (int)(percentage * newCHP.getHps());
                newCHP.setHps(newCHP.getHps() + add);
                break;
            }
            case 78: {
                final Hyphenation hyphenation = new Hyphenation((short)sprm.getOperand());
                newCHP.setHresi(hyphenation);
                break;
            }
            case 79: {
                newCHP.setFtcAscii((short)sprm.getOperand());
                break;
            }
            case 80: {
                newCHP.setFtcFE((short)sprm.getOperand());
                break;
            }
            case 81: {
                newCHP.setFtcOther((short)sprm.getOperand());
                break;
            }
            case 82: {
                break;
            }
            case 83: {
                newCHP.setFDStrike(SprmUncompressor.getFlag(sprm.getOperand()));
                break;
            }
            case 84: {
                newCHP.setFImprint(SprmUncompressor.getFlag(sprm.getOperand()));
                break;
            }
            case 85: {
                newCHP.setFSpec(SprmUncompressor.getFlag(sprm.getOperand()));
                break;
            }
            case 86: {
                newCHP.setFObj(SprmUncompressor.getFlag(sprm.getOperand()));
                break;
            }
            case 87: {
                final byte[] buf = sprm.getGrpprl();
                final int offset = sprm.getGrpprlOffset();
                newCHP.setFPropRMark(buf[offset] != 0);
                newCHP.setIbstPropRMark(LittleEndian.getShort(buf, offset + 1));
                newCHP.setDttmPropRMark(new DateAndTime(buf, offset + 3));
                break;
            }
            case 88: {
                newCHP.setFEmboss(SprmUncompressor.getFlag(sprm.getOperand()));
                break;
            }
            case 89: {
                newCHP.setSfxtText((byte)sprm.getOperand());
                break;
            }
            case 90: {
                break;
            }
            case 91: {
                break;
            }
            case 92: {
                break;
            }
            case 93: {
                break;
            }
            case 94: {
                break;
            }
            case 95: {
                break;
            }
            case 96: {
                break;
            }
            case 97: {
                break;
            }
            case 98: {
                final byte[] xstDispFldRMark = new byte[32];
                final byte[] buf = sprm.getGrpprl();
                final int offset = sprm.getGrpprlOffset();
                newCHP.setFDispFldRMark(0 != buf[offset]);
                newCHP.setIbstDispFldRMark(LittleEndian.getShort(buf, offset + 1));
                newCHP.setDttmDispFldRMark(new DateAndTime(buf, offset + 3));
                System.arraycopy(buf, offset + 7, xstDispFldRMark, 0, 32);
                newCHP.setXstDispFldRMark(xstDispFldRMark);
                break;
            }
            case 99: {
                newCHP.setIbstRMarkDel((short)sprm.getOperand());
                break;
            }
            case 100: {
                newCHP.setDttmRMarkDel(new DateAndTime(sprm.getGrpprl(), sprm.getGrpprlOffset()));
                break;
            }
            case 101: {
                newCHP.setBrc(new BorderCode(sprm.getGrpprl(), sprm.getGrpprlOffset()));
                break;
            }
            case 102: {
                newCHP.setShd(new ShadingDescriptor(sprm.getGrpprl(), sprm.getGrpprlOffset()));
                break;
            }
            case 103: {
                break;
            }
            case 104: {
                break;
            }
            case 105: {
                break;
            }
            case 106: {
                break;
            }
            case 107: {
                break;
            }
            case 108: {
                break;
            }
            case 109: {
                newCHP.setLidDefault((short)sprm.getOperand());
                break;
            }
            case 110: {
                newCHP.setLidFE((short)sprm.getOperand());
                break;
            }
            case 111: {
                newCHP.setIdctHint((byte)sprm.getOperand());
                break;
            }
            case 112: {
                newCHP.setCv(new Colorref(sprm.getOperand()));
                break;
            }
            case 113: {
                break;
            }
            case 114: {
                break;
            }
            case 115: {
                break;
            }
            case 116: {
                break;
            }
            default: {
                CharacterSprmUncompressor.logger.log(POILogger.DEBUG, "Unknown CHP sprm ignored: " + sprm);
                break;
            }
        }
    }
    
    private static boolean getCHPFlag(final byte x, final boolean oldVal) {
        if (x == 0) {
            return false;
        }
        if (x == 1) {
            return true;
        }
        if ((x & 0x81) == 0x80) {
            return oldVal;
        }
        return (x & 0x81) == 0x81 && !oldVal;
    }
    
    static {
        logger = POILogFactory.getLogger(CharacterSprmUncompressor.class);
    }
}
