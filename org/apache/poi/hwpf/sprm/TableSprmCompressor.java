// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hwpf.sprm;

import org.apache.poi.hwpf.usermodel.TableCellDescriptor;
import java.util.List;
import org.apache.poi.util.LittleEndian;
import java.util.Arrays;
import java.util.ArrayList;
import org.apache.poi.hwpf.usermodel.TableProperties;
import org.apache.poi.util.Internal;

@Internal
public final class TableSprmCompressor
{
    public static byte[] compressTableProperty(final TableProperties newTAP) {
        int size = 0;
        final List<byte[]> sprmList = new ArrayList<byte[]>();
        if (newTAP.getJc() != 0) {
            size += SprmUtils.addSprm((short)21504, newTAP.getJc(), null, sprmList);
        }
        if (newTAP.getFCantSplit()) {
            size += SprmUtils.addSprm((short)13315, 1, null, sprmList);
        }
        if (newTAP.getFTableHeader()) {
            size += SprmUtils.addSprm((short)13316, 1, null, sprmList);
        }
        final byte[] brcBuf = new byte[24];
        int offset = 0;
        newTAP.getBrcTop().serialize(brcBuf, offset);
        offset += 4;
        newTAP.getBrcLeft().serialize(brcBuf, offset);
        offset += 4;
        newTAP.getBrcBottom().serialize(brcBuf, offset);
        offset += 4;
        newTAP.getBrcRight().serialize(brcBuf, offset);
        offset += 4;
        newTAP.getBrcHorizontal().serialize(brcBuf, offset);
        offset += 4;
        newTAP.getBrcVertical().serialize(brcBuf, offset);
        final byte[] compare = new byte[24];
        if (!Arrays.equals(brcBuf, compare)) {
            size += SprmUtils.addSprm((short)(-10747), 0, brcBuf, sprmList);
        }
        if (newTAP.getDyaRowHeight() != 0) {
            size += SprmUtils.addSprm((short)(-27641), newTAP.getDyaRowHeight(), null, sprmList);
        }
        if (newTAP.getItcMac() > 0) {
            final int itcMac = newTAP.getItcMac();
            final byte[] buf = new byte[1 + 2 * (itcMac + 1) + 20 * itcMac];
            buf[0] = (byte)itcMac;
            final short[] dxaCenters = newTAP.getRgdxaCenter();
            for (int x = 0; x < dxaCenters.length; ++x) {
                LittleEndian.putShort(buf, 1 + x * 2, dxaCenters[x]);
            }
            final TableCellDescriptor[] cellDescriptors = newTAP.getRgtc();
            for (int x2 = 0; x2 < cellDescriptors.length; ++x2) {
                cellDescriptors[x2].serialize(buf, 1 + (itcMac + 1) * 2 + x2 * 20);
            }
            size += SprmUtils.addSpecialSprm((short)(-10744), buf, sprmList);
        }
        if (newTAP.getTlp() != null && !newTAP.getTlp().isEmpty()) {
            final byte[] buf2 = new byte[4];
            newTAP.getTlp().serialize(buf2, 0);
            size += SprmUtils.addSprm((short)29706, 0, buf2, sprmList);
        }
        return SprmUtils.getGrpprl(sprmList, size);
    }
}
