// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hwpf;

import org.apache.poi.hwpf.model.TextPieceTable;
import org.apache.poi.hwpf.usermodel.ObjectsPool;
import org.apache.poi.util.Internal;
import org.apache.poi.hwpf.usermodel.Range;
import java.io.FileNotFoundException;
import org.apache.poi.poifs.filesystem.DirectoryEntry;
import org.apache.poi.poifs.filesystem.DocumentEntry;
import java.io.IOException;
import java.io.PushbackInputStream;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import java.io.InputStream;
import org.apache.poi.poifs.filesystem.DirectoryNode;
import org.apache.poi.hwpf.model.ListTables;
import org.apache.poi.hwpf.model.FontTable;
import org.apache.poi.hwpf.model.SectionTable;
import org.apache.poi.hwpf.model.PAPBinTable;
import org.apache.poi.hwpf.model.CHPBinTable;
import org.apache.poi.hwpf.model.StyleSheet;
import org.apache.poi.hwpf.model.FileInformationBlock;
import org.apache.poi.hwpf.usermodel.ObjectPoolImpl;
import org.apache.poi.POIDocument;

public abstract class HWPFDocumentCore extends POIDocument
{
    protected static final String STREAM_OBJECT_POOL = "ObjectPool";
    protected static final String STREAM_WORD_DOCUMENT = "WordDocument";
    protected ObjectPoolImpl _objectPool;
    protected FileInformationBlock _fib;
    protected StyleSheet _ss;
    protected CHPBinTable _cbt;
    protected PAPBinTable _pbt;
    protected SectionTable _st;
    protected FontTable _ft;
    protected ListTables _lt;
    protected byte[] _mainStream;
    
    protected HWPFDocumentCore() {
        super((DirectoryNode)null);
    }
    
    public static POIFSFileSystem verifyAndBuildPOIFS(final InputStream istream) throws IOException {
        final PushbackInputStream pis = new PushbackInputStream(istream, 6);
        final byte[] first6 = new byte[6];
        pis.read(first6);
        if (first6[0] == 123 && first6[1] == 92 && first6[2] == 114 && first6[3] == 116 && first6[4] == 102) {
            throw new IllegalArgumentException("The document is really a RTF file");
        }
        pis.unread(first6);
        return new POIFSFileSystem(pis);
    }
    
    public HWPFDocumentCore(final InputStream istream) throws IOException {
        this(verifyAndBuildPOIFS(istream));
    }
    
    public HWPFDocumentCore(final POIFSFileSystem pfilesystem) throws IOException {
        this(pfilesystem.getRoot());
    }
    
    public HWPFDocumentCore(final DirectoryNode directory) throws IOException {
        super(directory);
        final DocumentEntry documentProps = (DocumentEntry)directory.getEntry("WordDocument");
        this._mainStream = new byte[documentProps.getSize()];
        directory.createDocumentInputStream("WordDocument").read(this._mainStream);
        this._fib = new FileInformationBlock(this._mainStream);
        DirectoryEntry objectPoolEntry;
        try {
            objectPoolEntry = (DirectoryEntry)directory.getEntry("ObjectPool");
        }
        catch (FileNotFoundException exc) {
            objectPoolEntry = null;
        }
        this._objectPool = new ObjectPoolImpl(objectPoolEntry);
    }
    
    public abstract Range getRange();
    
    public abstract Range getOverallRange();
    
    public String getDocumentText() {
        return this.getText().toString();
    }
    
    @Internal
    public abstract StringBuilder getText();
    
    public CHPBinTable getCharacterTable() {
        return this._cbt;
    }
    
    public PAPBinTable getParagraphTable() {
        return this._pbt;
    }
    
    public SectionTable getSectionTable() {
        return this._st;
    }
    
    public StyleSheet getStyleSheet() {
        return this._ss;
    }
    
    public ListTables getListTables() {
        return this._lt;
    }
    
    public FontTable getFontTable() {
        return this._ft;
    }
    
    public FileInformationBlock getFileInformationBlock() {
        return this._fib;
    }
    
    public ObjectsPool getObjectsPool() {
        return this._objectPool;
    }
    
    public abstract TextPieceTable getTextTable();
}
