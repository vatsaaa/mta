// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hwpf.usermodel;

import org.apache.poi.hwpf.model.types.TAPAbstractType;

public final class TableProperties extends TAPAbstractType implements Cloneable
{
    public TableProperties() {
        this.setTlp(new TableAutoformatLookSpecifier());
        this.setShdTable(new ShadingDescriptor());
        this.setBrcBottom(new BorderCode());
        this.setBrcHorizontal(new BorderCode());
        this.setBrcLeft(new BorderCode());
        this.setBrcRight(new BorderCode());
        this.setBrcTop(new BorderCode());
        this.setBrcVertical(new BorderCode());
        this.setRgbrcInsideDefault_0(new BorderCode());
        this.setRgbrcInsideDefault_1(new BorderCode());
        this.setRgdxaCenter(new short[0]);
        this.setRgdxaCenterPrint(new short[0]);
        this.setRgshd(new ShadingDescriptor[0]);
        this.setRgtc(new TableCellDescriptor[0]);
    }
    
    public TableProperties(final short columns) {
        this();
        this.setItcMac(columns);
        this.setRgshd(new ShadingDescriptor[columns]);
        for (int x = 0; x < columns; ++x) {
            this.getRgshd()[x] = new ShadingDescriptor();
        }
        final TableCellDescriptor[] tableCellDescriptors = new TableCellDescriptor[columns];
        for (int x2 = 0; x2 < columns; ++x2) {
            tableCellDescriptors[x2] = new TableCellDescriptor();
        }
        this.setRgtc(tableCellDescriptors);
        this.setRgdxaCenter(new short[columns]);
        this.setRgdxaCenterPrint(new short[columns]);
    }
    
    public Object clone() throws CloneNotSupportedException {
        final TableProperties tap = (TableProperties)super.clone();
        tap.setTlp(this.getTlp().clone());
        tap.setRgshd(new ShadingDescriptor[this.getRgshd().length]);
        for (int x = 0; x < this.getRgshd().length; ++x) {
            tap.getRgshd()[x] = this.getRgshd()[x].clone();
        }
        tap.setBrcBottom((BorderCode)this.getBrcBottom().clone());
        tap.setBrcHorizontal((BorderCode)this.getBrcHorizontal().clone());
        tap.setBrcLeft((BorderCode)this.getBrcLeft().clone());
        tap.setBrcRight((BorderCode)this.getBrcRight().clone());
        tap.setBrcTop((BorderCode)this.getBrcTop().clone());
        tap.setBrcVertical((BorderCode)this.getBrcVertical().clone());
        tap.setShdTable(this.getShdTable().clone());
        tap.setRgbrcInsideDefault_0((BorderCode)this.getRgbrcInsideDefault_0().clone());
        tap.setRgbrcInsideDefault_1((BorderCode)this.getRgbrcInsideDefault_1().clone());
        tap.setRgdxaCenter(this.getRgdxaCenter().clone());
        tap.setRgdxaCenterPrint(this.getRgdxaCenterPrint().clone());
        tap.setRgtc(new TableCellDescriptor[this.getRgtc().length]);
        for (int x = 0; x < this.getRgtc().length; ++x) {
            tap.getRgtc()[x] = (TableCellDescriptor)this.getRgtc()[x].clone();
        }
        return tap;
    }
}
