// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hwpf.usermodel;

import org.apache.poi.hwpf.HWPFOldDocument;
import org.apache.poi.hwpf.model.SEPX;

public final class Section extends Range
{
    private SectionProperties _props;
    
    public Section(final SEPX sepx, final Range parent) {
        super(Math.max(parent._start, sepx.getStart()), Math.min(parent._end, sepx.getEnd()), parent);
        if (parent.getDocument() instanceof HWPFOldDocument) {
            this._props = new SectionProperties();
        }
        else {
            this._props = sepx.getSectionProperties();
        }
    }
    
    public Object clone() throws CloneNotSupportedException {
        final Section s = (Section)super.clone();
        s._props = (SectionProperties)this._props.clone();
        return s;
    }
    
    public int getDistanceBetweenColumns() {
        return this._props.getDxaColumns();
    }
    
    public int getMarginBottom() {
        return this._props.getDyaBottom();
    }
    
    public int getMarginLeft() {
        return this._props.getDxaLeft();
    }
    
    public int getMarginRight() {
        return this._props.getDxaRight();
    }
    
    public int getMarginTop() {
        return this._props.getDyaTop();
    }
    
    public int getNumColumns() {
        return this._props.getCcolM1() + 1;
    }
    
    public int getPageHeight() {
        return this._props.getYaPage();
    }
    
    public int getPageWidth() {
        return this._props.getXaPage();
    }
    
    public boolean isColumnsEvenlySpaced() {
        return this._props.getFEvenlySpaced();
    }
    
    @Override
    public String toString() {
        return "Section [" + this.getStartOffset() + "; " + this.getEndOffset() + ")";
    }
    
    @Override
    public int type() {
        return 2;
    }
}
