// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hwpf.usermodel;

import org.apache.poi.hwpf.model.PlexOfField;
import org.apache.poi.util.Internal;

@Internal
class FieldImpl implements Field
{
    private PlexOfField endPlex;
    private PlexOfField separatorPlex;
    private PlexOfField startPlex;
    
    public FieldImpl(final PlexOfField startPlex, final PlexOfField separatorPlex, final PlexOfField endPlex) {
        if (startPlex == null) {
            throw new IllegalArgumentException("startPlex == null");
        }
        if (endPlex == null) {
            throw new IllegalArgumentException("endPlex == null");
        }
        if (startPlex.getFld().getBoundaryType() != 19) {
            throw new IllegalArgumentException("startPlex (" + startPlex + ") is not type of FIELD_BEGIN");
        }
        if (separatorPlex != null && separatorPlex.getFld().getBoundaryType() != 20) {
            throw new IllegalArgumentException("separatorPlex" + separatorPlex + ") is not type of FIELD_SEPARATOR");
        }
        if (endPlex.getFld().getBoundaryType() != 21) {
            throw new IllegalArgumentException("endPlex (" + endPlex + ") is not type of FIELD_END");
        }
        this.startPlex = startPlex;
        this.separatorPlex = separatorPlex;
        this.endPlex = endPlex;
    }
    
    public Range firstSubrange(final Range parent) {
        if (this.hasSeparator()) {
            if (this.getMarkStartOffset() + 1 == this.getMarkSeparatorOffset()) {
                return null;
            }
            return new Range(this.getMarkStartOffset() + 1, this.getMarkSeparatorOffset(), parent) {
                @Override
                public String toString() {
                    return "FieldSubrange1 (" + super.toString() + ")";
                }
            };
        }
        else {
            if (this.getMarkStartOffset() + 1 == this.getMarkEndOffset()) {
                return null;
            }
            return new Range(this.getMarkStartOffset() + 1, this.getMarkEndOffset(), parent) {
                @Override
                public String toString() {
                    return "FieldSubrange1 (" + super.toString() + ")";
                }
            };
        }
    }
    
    public int getFieldEndOffset() {
        return this.endPlex.getFcStart() + 1;
    }
    
    public int getFieldStartOffset() {
        return this.startPlex.getFcStart();
    }
    
    public CharacterRun getMarkEndCharacterRun(final Range parent) {
        return new Range(this.getMarkEndOffset(), this.getMarkEndOffset() + 1, parent).getCharacterRun(0);
    }
    
    public int getMarkEndOffset() {
        return this.endPlex.getFcStart();
    }
    
    public CharacterRun getMarkSeparatorCharacterRun(final Range parent) {
        if (!this.hasSeparator()) {
            return null;
        }
        return new Range(this.getMarkSeparatorOffset(), this.getMarkSeparatorOffset() + 1, parent).getCharacterRun(0);
    }
    
    public int getMarkSeparatorOffset() {
        return this.separatorPlex.getFcStart();
    }
    
    public CharacterRun getMarkStartCharacterRun(final Range parent) {
        return new Range(this.getMarkStartOffset(), this.getMarkStartOffset() + 1, parent).getCharacterRun(0);
    }
    
    public int getMarkStartOffset() {
        return this.startPlex.getFcStart();
    }
    
    public int getType() {
        return this.startPlex.getFld().getFieldType();
    }
    
    public boolean hasSeparator() {
        return this.separatorPlex != null;
    }
    
    public boolean isHasSep() {
        return this.endPlex.getFld().isFHasSep();
    }
    
    public boolean isLocked() {
        return this.endPlex.getFld().isFLocked();
    }
    
    public boolean isNested() {
        return this.endPlex.getFld().isFNested();
    }
    
    public boolean isPrivateResult() {
        return this.endPlex.getFld().isFPrivateResult();
    }
    
    public boolean isResultDirty() {
        return this.endPlex.getFld().isFResultDirty();
    }
    
    public boolean isResultEdited() {
        return this.endPlex.getFld().isFResultEdited();
    }
    
    public boolean isZombieEmbed() {
        return this.endPlex.getFld().isFZombieEmbed();
    }
    
    public Range secondSubrange(final Range parent) {
        if (!this.hasSeparator() || this.getMarkSeparatorOffset() + 1 == this.getMarkEndOffset()) {
            return null;
        }
        return new Range(this.getMarkSeparatorOffset() + 1, this.getMarkEndOffset(), parent) {
            @Override
            public String toString() {
                return "FieldSubrange2 (" + super.toString() + ")";
            }
        };
    }
    
    @Override
    public String toString() {
        return "Field [" + this.getFieldStartOffset() + "; " + this.getFieldEndOffset() + "] (type: 0x" + Integer.toHexString(this.getType()) + " = " + this.getType() + " )";
    }
}
