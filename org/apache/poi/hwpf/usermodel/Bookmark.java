// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hwpf.usermodel;

public interface Bookmark
{
    int getEnd();
    
    String getName();
    
    int getStart();
    
    void setName(final String p0);
}
