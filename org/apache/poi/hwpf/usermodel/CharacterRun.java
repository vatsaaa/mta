// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hwpf.usermodel;

import org.apache.poi.hwpf.model.Ffn;
import org.apache.poi.hwpf.model.StyleSheet;
import org.apache.poi.hwpf.model.CHPX;
import org.apache.poi.hwpf.sprm.SprmBuffer;

public final class CharacterRun extends Range implements Cloneable
{
    public static final short SPRM_FRMARKDEL = 2048;
    public static final short SPRM_FRMARK = 2049;
    public static final short SPRM_FFLDVANISH = 2050;
    public static final short SPRM_PICLOCATION = 27139;
    public static final short SPRM_IBSTRMARK = 18436;
    public static final short SPRM_DTTMRMARK = 26629;
    public static final short SPRM_FDATA = 2054;
    public static final short SPRM_SYMBOL = 27145;
    public static final short SPRM_FOLE2 = 2058;
    public static final short SPRM_HIGHLIGHT = 10764;
    public static final short SPRM_OBJLOCATION = 26638;
    public static final short SPRM_ISTD = 18992;
    public static final short SPRM_FBOLD = 2101;
    public static final short SPRM_FITALIC = 2102;
    public static final short SPRM_FSTRIKE = 2103;
    public static final short SPRM_FOUTLINE = 2104;
    public static final short SPRM_FSHADOW = 2105;
    public static final short SPRM_FSMALLCAPS = 2106;
    public static final short SPRM_FCAPS = 2107;
    public static final short SPRM_FVANISH = 2108;
    public static final short SPRM_KUL = 10814;
    public static final short SPRM_DXASPACE = -30656;
    public static final short SPRM_LID = 19009;
    public static final short SPRM_ICO = 10818;
    public static final short SPRM_HPS = 19011;
    public static final short SPRM_HPSPOS = 18501;
    public static final short SPRM_ISS = 10824;
    public static final short SPRM_HPSKERN = 18507;
    public static final short SPRM_YSRI = 18510;
    public static final short SPRM_RGFTCASCII = 19023;
    public static final short SPRM_RGFTCFAREAST = 19024;
    public static final short SPRM_RGFTCNOTFAREAST = 19025;
    public static final short SPRM_CHARSCALE = 18514;
    public static final short SPRM_FDSTRIKE = 10835;
    public static final short SPRM_FIMPRINT = 2132;
    public static final short SPRM_FSPEC = 2133;
    public static final short SPRM_FOBJ = 2134;
    public static final short SPRM_PROPRMARK = -13737;
    public static final short SPRM_FEMBOSS = 2136;
    public static final short SPRM_SFXTEXT = 10329;
    public static final short SPRM_DISPFLDRMARK = -13726;
    public static final short SPRM_IBSTRMARKDEL = 18531;
    public static final short SPRM_DTTMRMARKDEL = 26724;
    public static final short SPRM_BRC = 26725;
    public static final short SPRM_SHD = 18534;
    public static final short SPRM_IDSIRMARKDEL = 18535;
    public static final short SPRM_CPG = 18539;
    public static final short SPRM_NONFELID = 18541;
    public static final short SPRM_FELID = 18542;
    public static final short SPRM_IDCTHINT = 10351;
    SprmBuffer _chpx;
    CharacterProperties _props;
    
    CharacterRun(final CHPX chpx, final StyleSheet ss, final short istd, final Range parent) {
        super(Math.max(parent._start, chpx.getStart()), Math.min(parent._end, chpx.getEnd()), parent);
        this._props = chpx.getCharacterProperties(ss, istd);
        this._chpx = chpx.getSprmBuf();
    }
    
    @Override
    public int type() {
        return 1;
    }
    
    public boolean isMarkedDeleted() {
        return this._props.isFRMarkDel();
    }
    
    public void markDeleted(final boolean mark) {
        this._props.setFRMarkDel(mark);
        final byte newVal = (byte)(mark ? 1 : 0);
        this._chpx.updateSprm((short)2048, newVal);
    }
    
    public boolean isBold() {
        return this._props.isFBold();
    }
    
    public void setBold(final boolean bold) {
        this._props.setFBold(bold);
        final byte newVal = (byte)(bold ? 1 : 0);
        this._chpx.updateSprm((short)2101, newVal);
    }
    
    public boolean isItalic() {
        return this._props.isFItalic();
    }
    
    public void setItalic(final boolean italic) {
        this._props.setFItalic(italic);
        final byte newVal = (byte)(italic ? 1 : 0);
        this._chpx.updateSprm((short)2102, newVal);
    }
    
    public boolean isOutlined() {
        return this._props.isFOutline();
    }
    
    public void setOutline(final boolean outlined) {
        this._props.setFOutline(outlined);
        final byte newVal = (byte)(outlined ? 1 : 0);
        this._chpx.updateSprm((short)2104, newVal);
    }
    
    public boolean isFldVanished() {
        return this._props.isFFldVanish();
    }
    
    public void setFldVanish(final boolean fldVanish) {
        this._props.setFFldVanish(fldVanish);
        final byte newVal = (byte)(fldVanish ? 1 : 0);
        this._chpx.updateSprm((short)2050, newVal);
    }
    
    public boolean isSmallCaps() {
        return this._props.isFSmallCaps();
    }
    
    public void setSmallCaps(final boolean smallCaps) {
        this._props.setFSmallCaps(smallCaps);
        final byte newVal = (byte)(smallCaps ? 1 : 0);
        this._chpx.updateSprm((short)2106, newVal);
    }
    
    public boolean isCapitalized() {
        return this._props.isFCaps();
    }
    
    public void setCapitalized(final boolean caps) {
        this._props.setFCaps(caps);
        final byte newVal = (byte)(caps ? 1 : 0);
        this._chpx.updateSprm((short)2107, newVal);
    }
    
    public boolean isVanished() {
        return this._props.isFVanish();
    }
    
    public void setVanished(final boolean vanish) {
        this._props.setFVanish(vanish);
        final byte newVal = (byte)(vanish ? 1 : 0);
        this._chpx.updateSprm((short)2108, newVal);
    }
    
    public boolean isMarkedInserted() {
        return this._props.isFRMark();
    }
    
    public void markInserted(final boolean mark) {
        this._props.setFRMark(mark);
        final byte newVal = (byte)(mark ? 1 : 0);
        this._chpx.updateSprm((short)2049, newVal);
    }
    
    public boolean isStrikeThrough() {
        return this._props.isFStrike();
    }
    
    public void strikeThrough(final boolean strike) {
        this._props.setFStrike(strike);
        final byte newVal = (byte)(strike ? 1 : 0);
        this._chpx.updateSprm((short)2103, newVal);
    }
    
    public boolean isShadowed() {
        return this._props.isFShadow();
    }
    
    public void setShadow(final boolean shadow) {
        this._props.setFShadow(shadow);
        final byte newVal = (byte)(shadow ? 1 : 0);
        this._chpx.updateSprm((short)2105, newVal);
    }
    
    public boolean isEmbossed() {
        return this._props.isFEmboss();
    }
    
    public void setEmbossed(final boolean emboss) {
        this._props.setFEmboss(emboss);
        final byte newVal = (byte)(emboss ? 1 : 0);
        this._chpx.updateSprm((short)2136, newVal);
    }
    
    public boolean isImprinted() {
        return this._props.isFImprint();
    }
    
    public void setImprinted(final boolean imprint) {
        this._props.setFImprint(imprint);
        final byte newVal = (byte)(imprint ? 1 : 0);
        this._chpx.updateSprm((short)2132, newVal);
    }
    
    public boolean isDoubleStrikeThrough() {
        return this._props.isFDStrike();
    }
    
    public void setDoubleStrikethrough(final boolean dstrike) {
        this._props.setFDStrike(dstrike);
        final byte newVal = (byte)(dstrike ? 1 : 0);
        this._chpx.updateSprm((short)10835, newVal);
    }
    
    public void setFtcAscii(final int ftcAscii) {
        this._props.setFtcAscii(ftcAscii);
        this._chpx.updateSprm((short)19023, (short)ftcAscii);
    }
    
    public void setFtcFE(final int ftcFE) {
        this._props.setFtcFE(ftcFE);
        this._chpx.updateSprm((short)19024, (short)ftcFE);
    }
    
    public void setFtcOther(final int ftcOther) {
        this._props.setFtcOther(ftcOther);
        this._chpx.updateSprm((short)19025, (short)ftcOther);
    }
    
    public int getFontSize() {
        return this._props.getHps();
    }
    
    public void setFontSize(final int halfPoints) {
        this._props.setHps(halfPoints);
        this._chpx.updateSprm((short)19011, (short)halfPoints);
    }
    
    public int getCharacterSpacing() {
        return this._props.getDxaSpace();
    }
    
    public void setCharacterSpacing(final int twips) {
        this._props.setDxaSpace(twips);
        this._chpx.updateSprm((short)(-30656), twips);
    }
    
    public short getSubSuperScriptIndex() {
        return this._props.getIss();
    }
    
    public void setSubSuperScriptIndex(final short iss) {
        this._props.setDxaSpace(iss);
        this._chpx.updateSprm((short)(-30656), iss);
    }
    
    public int getUnderlineCode() {
        return this._props.getKul();
    }
    
    public void setUnderlineCode(final int kul) {
        this._props.setKul((byte)kul);
        this._chpx.updateSprm((short)10814, (byte)kul);
    }
    
    public int getColor() {
        return this._props.getIco();
    }
    
    public void setColor(final int color) {
        this._props.setIco((byte)color);
        this._chpx.updateSprm((short)10818, (byte)color);
    }
    
    public int getVerticalOffset() {
        return this._props.getHpsPos();
    }
    
    public void setVerticalOffset(final int hpsPos) {
        this._props.setHpsPos((short)hpsPos);
        this._chpx.updateSprm((short)18501, (byte)hpsPos);
    }
    
    public int getKerning() {
        return this._props.getHpsKern();
    }
    
    public void setKerning(final int kern) {
        this._props.setHpsKern(kern);
        this._chpx.updateSprm((short)18507, (short)kern);
    }
    
    public boolean isHighlighted() {
        return this._props.isFHighlight();
    }
    
    public byte getHighlightedColor() {
        return this._props.getIcoHighlight();
    }
    
    public void setHighlighted(final byte color) {
        this._props.setFHighlight(true);
        this._props.setIcoHighlight(color);
        this._chpx.updateSprm((short)10764, color);
    }
    
    public String getFontName() {
        if (this._doc.getFontTable() == null) {
            return null;
        }
        return this._doc.getFontTable().getMainFont(this._props.getFtcAscii());
    }
    
    public boolean isSpecialCharacter() {
        return this._props.isFSpec();
    }
    
    public void setSpecialCharacter(final boolean spec) {
        this._props.setFSpec(spec);
        final byte newVal = (byte)(spec ? 1 : 0);
        this._chpx.updateSprm((short)2133, newVal);
    }
    
    public boolean isObj() {
        return this._props.isFObj();
    }
    
    public void setObj(final boolean obj) {
        this._props.setFObj(obj);
        final byte newVal = (byte)(obj ? 1 : 0);
        this._chpx.updateSprm((short)2134, newVal);
    }
    
    public int getPicOffset() {
        return this._props.getFcPic();
    }
    
    public void setPicOffset(final int offset) {
        this._props.setFcPic(offset);
        this._chpx.updateSprm((short)27139, offset);
    }
    
    public boolean isData() {
        return this._props.isFData();
    }
    
    public void setData(final boolean data) {
        this._props.setFData(data);
        final byte newVal = (byte)(data ? 1 : 0);
        this._chpx.updateSprm((short)2134, newVal);
    }
    
    public boolean isOle2() {
        return this._props.isFOle2();
    }
    
    public void setOle2(final boolean ole) {
        this._props.setFOle2(ole);
        final byte newVal = (byte)(ole ? 1 : 0);
        this._chpx.updateSprm((short)2134, newVal);
    }
    
    public int getObjOffset() {
        return this._props.getFcObj();
    }
    
    public void setObjOffset(final int obj) {
        this._props.setFcObj(obj);
        this._chpx.updateSprm((short)26638, obj);
    }
    
    public int getIco24() {
        return this._props.getIco24();
    }
    
    public void setIco24(final int colour24) {
        this._props.setIco24(colour24);
    }
    
    @Deprecated
    public CharacterProperties cloneProperties() {
        try {
            return (CharacterProperties)this._props.clone();
        }
        catch (CloneNotSupportedException e) {
            throw new RuntimeException(e);
        }
    }
    
    public Object clone() throws CloneNotSupportedException {
        final CharacterRun cp = (CharacterRun)super.clone();
        cp._props.setDttmRMark((DateAndTime)this._props.getDttmRMark().clone());
        cp._props.setDttmRMarkDel((DateAndTime)this._props.getDttmRMarkDel().clone());
        cp._props.setDttmPropRMark((DateAndTime)this._props.getDttmPropRMark().clone());
        cp._props.setDttmDispFldRMark((DateAndTime)this._props.getDttmDispFldRMark().clone());
        cp._props.setXstDispFldRMark(this._props.getXstDispFldRMark().clone());
        cp._props.setShd(this._props.getShd().clone());
        return cp;
    }
    
    public boolean isSymbol() {
        return this.isSpecialCharacter() && this.text().equals("(");
    }
    
    public char getSymbolCharacter() {
        if (this.isSymbol()) {
            return (char)this._props.getXchSym();
        }
        throw new IllegalStateException("Not a symbol CharacterRun");
    }
    
    public Ffn getSymbolFont() {
        if (!this.isSymbol()) {
            throw new IllegalStateException("Not a symbol CharacterRun");
        }
        if (this._doc.getFontTable() == null) {
            return null;
        }
        final Ffn[] fontNames = this._doc.getFontTable().getFontNames();
        if (fontNames.length <= this._props.getFtcSym()) {
            return null;
        }
        return fontNames[this._props.getFtcSym()];
    }
    
    public BorderCode getBorder() {
        return this._props.getBrc();
    }
    
    public int getLanguageCode() {
        return this._props.getLidDefault();
    }
    
    @Override
    public String toString() {
        final String text = this.text();
        return "CharacterRun of " + text.length() + " characters - " + text;
    }
}
