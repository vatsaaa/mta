// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hwpf.usermodel;

import java.util.Comparator;
import org.apache.poi.hwpf.model.PropertyNode;
import java.util.Collection;
import java.util.LinkedList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Arrays;
import java.util.Iterator;
import java.util.ArrayList;
import java.util.Collections;
import org.apache.poi.hwpf.model.GenericPropertyNode;
import java.util.List;
import java.util.Map;
import org.apache.poi.hwpf.model.BookmarksTables;

public class BookmarksImpl implements Bookmarks
{
    private final BookmarksTables bookmarksTables;
    private Map<Integer, List<GenericPropertyNode>> sortedDescriptors;
    private int[] sortedStartPositions;
    
    public BookmarksImpl(final BookmarksTables bookmarksTables) {
        this.sortedDescriptors = null;
        this.sortedStartPositions = null;
        this.bookmarksTables = bookmarksTables;
        this.reset();
    }
    
    void afterDelete(final int startCp, final int length) {
        this.bookmarksTables.afterDelete(startCp, length);
        this.reset();
    }
    
    void afterInsert(final int startCp, final int length) {
        this.bookmarksTables.afterInsert(startCp, length);
        this.reset();
    }
    
    private Bookmark getBookmark(final GenericPropertyNode first) {
        return new BookmarkImpl(first);
    }
    
    public Bookmark getBookmark(final int index) {
        final GenericPropertyNode first = this.bookmarksTables.getDescriptorFirst(index);
        return this.getBookmark(first);
    }
    
    public List<Bookmark> getBookmarksAt(final int startCp) {
        this.updateSortedDescriptors();
        final List<GenericPropertyNode> nodes = this.sortedDescriptors.get(startCp);
        if (nodes == null || nodes.isEmpty()) {
            return Collections.emptyList();
        }
        final List<Bookmark> result = new ArrayList<Bookmark>(nodes.size());
        for (final GenericPropertyNode node : nodes) {
            result.add(this.getBookmark(node));
        }
        return Collections.unmodifiableList((List<? extends Bookmark>)result);
    }
    
    public int getBookmarksCount() {
        return this.bookmarksTables.getDescriptorsFirstCount();
    }
    
    public Map<Integer, List<Bookmark>> getBookmarksStartedBetween(final int startInclusive, final int endExclusive) {
        this.updateSortedDescriptors();
        int startLookupIndex = Arrays.binarySearch(this.sortedStartPositions, startInclusive);
        if (startLookupIndex < 0) {
            startLookupIndex = -(startLookupIndex + 1);
        }
        int endLookupIndex = Arrays.binarySearch(this.sortedStartPositions, endExclusive);
        if (endLookupIndex < 0) {
            endLookupIndex = -(endLookupIndex + 1);
        }
        final Map<Integer, List<Bookmark>> result = new LinkedHashMap<Integer, List<Bookmark>>();
        for (int lookupIndex = startLookupIndex; lookupIndex < endLookupIndex; ++lookupIndex) {
            final int s = this.sortedStartPositions[lookupIndex];
            if (s >= startInclusive) {
                if (s >= endExclusive) {
                    break;
                }
                final List<Bookmark> startedAt = this.getBookmarksAt(s);
                if (startedAt != null) {
                    result.put(s, startedAt);
                }
            }
        }
        return Collections.unmodifiableMap((Map<? extends Integer, ? extends List<Bookmark>>)result);
    }
    
    public void remove(final int index) {
        this.bookmarksTables.remove(index);
    }
    
    private void reset() {
        this.sortedDescriptors = null;
        this.sortedStartPositions = null;
    }
    
    private void updateSortedDescriptors() {
        if (this.sortedDescriptors != null) {
            return;
        }
        final Map<Integer, List<GenericPropertyNode>> result = new HashMap<Integer, List<GenericPropertyNode>>();
        for (int b = 0; b < this.bookmarksTables.getDescriptorsFirstCount(); ++b) {
            final GenericPropertyNode property = this.bookmarksTables.getDescriptorFirst(b);
            final Integer positionKey = property.getStart();
            List<GenericPropertyNode> atPositionList = result.get(positionKey);
            if (atPositionList == null) {
                atPositionList = new LinkedList<GenericPropertyNode>();
                result.put(positionKey, atPositionList);
            }
            atPositionList.add(property);
        }
        int counter = 0;
        final int[] indices = new int[result.size()];
        for (final Map.Entry<Integer, List<GenericPropertyNode>> entry : result.entrySet()) {
            indices[counter++] = entry.getKey();
            final List<GenericPropertyNode> updated = new ArrayList<GenericPropertyNode>(entry.getValue());
            Collections.sort(updated, PropertyNode.EndComparator.instance);
            entry.setValue(updated);
        }
        Arrays.sort(indices);
        this.sortedDescriptors = result;
        this.sortedStartPositions = indices;
    }
    
    private final class BookmarkImpl implements Bookmark
    {
        private final GenericPropertyNode first;
        
        private BookmarkImpl(final GenericPropertyNode first) {
            this.first = first;
        }
        
        @Override
        public boolean equals(final Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null) {
                return false;
            }
            if (this.getClass() != obj.getClass()) {
                return false;
            }
            final BookmarkImpl other = (BookmarkImpl)obj;
            if (this.first == null) {
                if (other.first != null) {
                    return false;
                }
            }
            else if (!this.first.equals(other.first)) {
                return false;
            }
            return true;
        }
        
        public int getEnd() {
            final int currentIndex = BookmarksImpl.this.bookmarksTables.getDescriptorFirstIndex(this.first);
            try {
                final GenericPropertyNode descriptorLim = BookmarksImpl.this.bookmarksTables.getDescriptorLim(currentIndex);
                return descriptorLim.getStart();
            }
            catch (IndexOutOfBoundsException exc) {
                return this.first.getEnd();
            }
        }
        
        public String getName() {
            final int currentIndex = BookmarksImpl.this.bookmarksTables.getDescriptorFirstIndex(this.first);
            try {
                return BookmarksImpl.this.bookmarksTables.getName(currentIndex);
            }
            catch (ArrayIndexOutOfBoundsException exc) {
                return "";
            }
        }
        
        public int getStart() {
            return this.first.getStart();
        }
        
        @Override
        public int hashCode() {
            return 31 + ((this.first == null) ? 0 : this.first.hashCode());
        }
        
        public void setName(final String name) {
            final int currentIndex = BookmarksImpl.this.bookmarksTables.getDescriptorFirstIndex(this.first);
            BookmarksImpl.this.bookmarksTables.setName(currentIndex, name);
        }
        
        @Override
        public String toString() {
            return "Bookmark [" + this.getStart() + "; " + this.getEnd() + "): name: " + this.getName();
        }
    }
}
