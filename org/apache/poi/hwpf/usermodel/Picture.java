// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hwpf.usermodel;

import org.apache.poi.util.POILogFactory;
import java.io.OutputStream;
import org.apache.poi.ddf.EscherBSERecord;
import java.io.IOException;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.zip.InflaterInputStream;
import java.io.ByteArrayInputStream;
import java.util.Arrays;
import org.apache.poi.ddf.EscherBlipRecord;
import org.apache.poi.ddf.EscherRecord;
import java.util.List;
import org.apache.poi.hwpf.model.PICFAndOfficeArtData;
import org.apache.poi.hwpf.model.PICF;
import org.apache.poi.util.POILogger;

public final class Picture
{
    @Deprecated
    public static final byte[] BMP;
    public static final byte[] COMPRESSED1;
    public static final byte[] COMPRESSED2;
    @Deprecated
    public static final byte[] EMF;
    @Deprecated
    public static final byte[] GIF;
    public static final byte[] IHDR;
    @Deprecated
    public static final byte[] JPG;
    private static final POILogger log;
    @Deprecated
    public static final byte[] PNG;
    @Deprecated
    public static final byte[] TIFF;
    @Deprecated
    public static final byte[] TIFF1;
    @Deprecated
    public static final byte[] WMF1;
    @Deprecated
    public static final byte[] WMF2;
    private PICF _picf;
    private PICFAndOfficeArtData _picfAndOfficeArtData;
    private List<? extends EscherRecord> _blipRecords;
    private byte[] content;
    private int dataBlockStartOfsset;
    private int height;
    private int width;
    
    private static int getBigEndianInt(final byte[] data, final int offset) {
        return ((data[offset] & 0xFF) << 24) + ((data[offset + 1] & 0xFF) << 16) + ((data[offset + 2] & 0xFF) << 8) + (data[offset + 3] & 0xFF);
    }
    
    private static int getBigEndianShort(final byte[] data, final int offset) {
        return ((data[offset] & 0xFF) << 8) + (data[offset + 1] & 0xFF);
    }
    
    private static boolean matchSignature(final byte[] pictureData, final byte[] signature, final int offset) {
        boolean matched = offset < pictureData.length;
        for (int i = 0; i + offset < pictureData.length && i < signature.length; ++i) {
            if (pictureData[i + offset] != signature[i]) {
                matched = false;
                break;
            }
        }
        return matched;
    }
    
    public Picture(final EscherBlipRecord blipRecord) {
        this.height = -1;
        this.width = -1;
        this._blipRecords = Arrays.asList(blipRecord);
    }
    
    public Picture(final int dataBlockStartOfsset, final byte[] _dataStream, final boolean fillBytes) {
        this.height = -1;
        this.width = -1;
        this._picfAndOfficeArtData = new PICFAndOfficeArtData(_dataStream, dataBlockStartOfsset);
        this._picf = this._picfAndOfficeArtData.getPicf();
        this.dataBlockStartOfsset = dataBlockStartOfsset;
        if (this._picfAndOfficeArtData != null && this._picfAndOfficeArtData.getBlipRecords() != null) {
            this._blipRecords = this._picfAndOfficeArtData.getBlipRecords();
        }
        if (fillBytes) {
            this.fillImageContent();
        }
    }
    
    private void fillImageContent() {
        if (this.content != null && this.content.length > 0) {
            return;
        }
        final byte[] rawContent = this.getRawContent();
        if (!matchSignature(rawContent, Picture.COMPRESSED1, 32)) {
            if (!matchSignature(rawContent, Picture.COMPRESSED2, 32)) {
                this.content = rawContent;
                return;
            }
        }
        try {
            final InflaterInputStream in = new InflaterInputStream(new ByteArrayInputStream(rawContent, 33, rawContent.length - 33));
            final ByteArrayOutputStream out = new ByteArrayOutputStream();
            final byte[] buf = new byte[4096];
            int readBytes;
            while ((readBytes = in.read(buf)) > 0) {
                out.write(buf, 0, readBytes);
            }
            this.content = out.toByteArray();
        }
        catch (IOException e) {
            Picture.log.log(POILogger.INFO, "Possibly corrupt compression or non-compressed data", e);
        }
    }
    
    private void fillJPGWidthHeight() {
        final byte[] jpegContent = this.getContent();
        int pointer = 2;
        int firstByte = jpegContent[pointer];
        int secondByte = jpegContent[pointer + 1];
        final int endOfPicture = jpegContent.length;
        while (pointer < endOfPicture - 1) {
            do {
                firstByte = jpegContent[pointer];
                secondByte = jpegContent[pointer + 1];
                pointer += 2;
            } while (firstByte != -1 && pointer < endOfPicture - 1);
            if (firstByte == -1 && pointer < endOfPicture - 1) {
                if (secondByte == -39) {
                    break;
                }
                if (secondByte == -38) {
                    break;
                }
                if ((secondByte & 0xF0) == 0xC0 && secondByte != -60 && secondByte != -56 && secondByte != -52) {
                    pointer += 5;
                    this.height = getBigEndianShort(jpegContent, pointer);
                    this.width = getBigEndianShort(jpegContent, pointer + 2);
                    break;
                }
                ++pointer;
                ++pointer;
                final int length = getBigEndianShort(jpegContent, pointer);
                pointer += length;
            }
            else {
                ++pointer;
            }
        }
    }
    
    void fillPNGWidthHeight() {
        final byte[] pngContent = this.getContent();
        final int HEADER_START = Picture.PNG.length + 4;
        if (matchSignature(pngContent, Picture.IHDR, HEADER_START)) {
            final int IHDR_CHUNK_WIDTH = HEADER_START + 4;
            this.width = getBigEndianInt(pngContent, IHDR_CHUNK_WIDTH);
            this.height = getBigEndianInt(pngContent, IHDR_CHUNK_WIDTH + 4);
        }
    }
    
    private void fillWidthHeight() {
        final PictureType pictureType = this.suggestPictureType();
        switch (pictureType) {
            case JPEG: {
                this.fillJPGWidthHeight();
                break;
            }
            case PNG: {
                this.fillPNGWidthHeight();
                break;
            }
        }
    }
    
    @Deprecated
    public int getAspectRatioX() {
        return this._picf.getMx() / 10;
    }
    
    @Deprecated
    public int getAspectRatioY() {
        return this._picf.getMy() / 10;
    }
    
    public byte[] getContent() {
        this.fillImageContent();
        return this.content;
    }
    
    @Deprecated
    public int getDxaCropLeft() {
        return this._picf.getDxaReserved1();
    }
    
    @Deprecated
    public int getDxaCropRight() {
        return this._picf.getDxaReserved2();
    }
    
    public int getDxaGoal() {
        return this._picf.getDxaGoal();
    }
    
    @Deprecated
    public int getDyaCropBottom() {
        return this._picf.getDyaReserved2();
    }
    
    @Deprecated
    public int getDyaCropTop() {
        return this._picf.getDyaReserved1();
    }
    
    public int getDyaGoal() {
        return this._picf.getDyaGoal();
    }
    
    public int getHeight() {
        if (this.height == -1) {
            this.fillWidthHeight();
        }
        return this.height;
    }
    
    public int getHorizontalScalingFactor() {
        return this._picf.getMx();
    }
    
    public String getMimeType() {
        return this.suggestPictureType().getMime();
    }
    
    public byte[] getRawContent() {
        if (this._blipRecords == null || this._blipRecords.size() != 1) {
            return new byte[0];
        }
        final EscherRecord escherRecord = (EscherRecord)this._blipRecords.get(0);
        if (escherRecord instanceof EscherBlipRecord) {
            return ((EscherBlipRecord)escherRecord).getPicturedata();
        }
        if (escherRecord instanceof EscherBSERecord) {
            return ((EscherBSERecord)escherRecord).getBlipRecord().getPicturedata();
        }
        return new byte[0];
    }
    
    public int getSize() {
        return this.getContent().length;
    }
    
    public int getStartOffset() {
        return this.dataBlockStartOfsset;
    }
    
    public int getVerticalScalingFactor() {
        return this._picf.getMy();
    }
    
    public int getWidth() {
        if (this.width == -1) {
            this.fillWidthHeight();
        }
        return this.width;
    }
    
    public String suggestFileExtension() {
        return this.suggestPictureType().getExtension();
    }
    
    public String suggestFullFileName() {
        final String fileExt = this.suggestFileExtension();
        return Integer.toHexString(this.dataBlockStartOfsset) + ((fileExt.length() > 0) ? ("." + fileExt) : "");
    }
    
    public PictureType suggestPictureType() {
        if (this._blipRecords == null || this._blipRecords.size() != 1) {
            return PictureType.UNKNOWN;
        }
        final EscherRecord escherRecord = (EscherRecord)this._blipRecords.get(0);
        switch (escherRecord.getRecordId()) {
            case -4089: {
                final EscherBSERecord bseRecord = (EscherBSERecord)escherRecord;
                switch (bseRecord.getBlipTypeWin32()) {
                    case 0: {
                        return PictureType.UNKNOWN;
                    }
                    case 1: {
                        return PictureType.UNKNOWN;
                    }
                    case 2: {
                        return PictureType.EMF;
                    }
                    case 3: {
                        return PictureType.WMF;
                    }
                    case 4: {
                        return PictureType.PICT;
                    }
                    case 5: {
                        return PictureType.JPEG;
                    }
                    case 6: {
                        return PictureType.PNG;
                    }
                    case 7: {
                        return PictureType.BMP;
                    }
                    case 17: {
                        return PictureType.TIFF;
                    }
                    case 18: {
                        return PictureType.JPEG;
                    }
                    default: {
                        return PictureType.UNKNOWN;
                    }
                }
                break;
            }
            case -4070: {
                return PictureType.EMF;
            }
            case -4069: {
                return PictureType.WMF;
            }
            case -4068: {
                return PictureType.PICT;
            }
            case -4067: {
                return PictureType.JPEG;
            }
            case -4066: {
                return PictureType.PNG;
            }
            case -4065: {
                return PictureType.BMP;
            }
            case -4055: {
                return PictureType.TIFF;
            }
            case -4054: {
                return PictureType.JPEG;
            }
            default: {
                return PictureType.UNKNOWN;
            }
        }
    }
    
    public void writeImageContent(final OutputStream out) throws IOException {
        final byte[] content = this.getContent();
        if (content != null && content.length > 0) {
            out.write(content, 0, content.length);
        }
    }
    
    static {
        BMP = new byte[] { 66, 77 };
        COMPRESSED1 = new byte[] { -2, 120, -38 };
        COMPRESSED2 = new byte[] { -2, 120, -100 };
        EMF = new byte[] { 1, 0, 0, 0 };
        GIF = new byte[] { 71, 73, 70 };
        IHDR = new byte[] { 73, 72, 68, 82 };
        JPG = new byte[] { -1, -40 };
        log = POILogFactory.getLogger(Picture.class);
        PNG = new byte[] { -119, 80, 78, 71, 13, 10, 26, 10 };
        TIFF = new byte[] { 73, 73, 42, 0 };
        TIFF1 = new byte[] { 77, 77, 0, 42 };
        WMF1 = new byte[] { -41, -51, -58, -102, 0, 0 };
        WMF2 = new byte[] { 1, 0, 9, 0, 0, 3 };
    }
}
