// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hwpf.usermodel;

public final class TableIterator
{
    Range _range;
    int _index;
    int _levelNum;
    
    TableIterator(final Range range, final int levelNum) {
        this._range = range;
        this._index = 0;
        this._levelNum = levelNum;
    }
    
    public TableIterator(final Range range) {
        this(range, 1);
    }
    
    public boolean hasNext() {
        final int numParagraphs = this._range.numParagraphs();
        while (this._index < numParagraphs) {
            final Paragraph paragraph = this._range.getParagraph(this._index);
            if (paragraph.isInTable() && paragraph.getTableLevel() == this._levelNum) {
                return true;
            }
            ++this._index;
        }
        return false;
    }
    
    public Table next() {
        final int numParagraphs = this._range.numParagraphs();
        final int startIndex = this._index;
        int endIndex = this._index;
        while (this._index < numParagraphs) {
            final Paragraph paragraph = this._range.getParagraph(this._index);
            if (!paragraph.isInTable() || paragraph.getTableLevel() < this._levelNum) {
                endIndex = this._index;
                break;
            }
            ++this._index;
        }
        return new Table(this._range.getParagraph(startIndex).getStartOffset(), this._range.getParagraph(endIndex - 1).getEndOffset(), this._range, this._levelNum);
    }
}
