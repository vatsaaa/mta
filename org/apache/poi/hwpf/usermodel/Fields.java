// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hwpf.usermodel;

import java.util.Collection;
import org.apache.poi.hwpf.model.FieldsDocumentPart;

public interface Fields
{
    Field getFieldByStartOffset(final FieldsDocumentPart p0, final int p1);
    
    Collection<Field> getFields(final FieldsDocumentPart p0);
}
