// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hwpf.usermodel;

import org.apache.poi.util.BitFieldFactory;
import java.util.Calendar;
import org.apache.poi.util.LittleEndian;
import org.apache.poi.util.BitField;

public final class DateAndTime implements Cloneable
{
    public static final int SIZE = 4;
    private short _info;
    private static final BitField _minutes;
    private static final BitField _hours;
    private static final BitField _dom;
    private short _info2;
    private static final BitField _months;
    private static final BitField _years;
    private static final BitField _weekday;
    
    public DateAndTime() {
    }
    
    public DateAndTime(final byte[] buf, final int offset) {
        this._info = LittleEndian.getShort(buf, offset);
        this._info2 = LittleEndian.getShort(buf, offset + 2);
    }
    
    public Calendar getDate() {
        final Calendar cal = Calendar.getInstance();
        cal.set(DateAndTime._years.getValue(this._info2) + 1900, DateAndTime._months.getValue(this._info2) - 1, DateAndTime._dom.getValue(this._info), DateAndTime._hours.getValue(this._info), DateAndTime._minutes.getValue(this._info), 0);
        cal.set(14, 0);
        return cal;
    }
    
    public void serialize(final byte[] buf, final int offset) {
        LittleEndian.putShort(buf, offset, this._info);
        LittleEndian.putShort(buf, offset + 2, this._info2);
    }
    
    @Override
    public boolean equals(final Object o) {
        final DateAndTime dttm = (DateAndTime)o;
        return this._info == dttm._info && this._info2 == dttm._info2;
    }
    
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
    
    public boolean isEmpty() {
        return this._info == 0 && this._info2 == 0;
    }
    
    @Override
    public String toString() {
        if (this.isEmpty()) {
            return "[DTTM] EMPTY";
        }
        return "[DTTM] " + this.getDate();
    }
    
    static {
        _minutes = BitFieldFactory.getInstance(63);
        _hours = BitFieldFactory.getInstance(1984);
        _dom = BitFieldFactory.getInstance(63488);
        _months = BitFieldFactory.getInstance(15);
        _years = BitFieldFactory.getInstance(8176);
        _weekday = BitFieldFactory.getInstance(57344);
    }
}
