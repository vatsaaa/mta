// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hwpf.usermodel;

import org.apache.poi.hwpf.sprm.ParagraphSprmCompressor;
import org.apache.poi.hwpf.model.ListLevel;
import org.apache.poi.hwpf.sprm.CharacterSprmCompressor;
import org.apache.poi.hwpf.model.StyleSheet;
import org.apache.poi.hwpf.model.ListFormatOverride;
import org.apache.poi.hwpf.model.ListData;

public final class HWPFList
{
    private ListData _listData;
    private ListFormatOverride _override;
    private boolean _registered;
    private StyleSheet _styleSheet;
    
    public HWPFList(final boolean numbered, final StyleSheet styleSheet) {
        this._listData = new ListData((int)(Math.random() * System.currentTimeMillis()), numbered);
        this._override = new ListFormatOverride(this._listData.getLsid());
        this._styleSheet = styleSheet;
    }
    
    public void setLevelNumberProperties(final int level, final CharacterProperties chp) {
        final ListLevel listLevel = this._listData.getLevel(level);
        final int styleIndex = this._listData.getLevelStyle(level);
        final CharacterProperties base = this._styleSheet.getCharacterStyle(styleIndex);
        final byte[] grpprl = CharacterSprmCompressor.compressCharacterProperty(chp, base);
        listLevel.setNumberProperties(grpprl);
    }
    
    public void setLevelParagraphProperties(final int level, final ParagraphProperties pap) {
        final ListLevel listLevel = this._listData.getLevel(level);
        final int styleIndex = this._listData.getLevelStyle(level);
        final ParagraphProperties base = this._styleSheet.getParagraphStyle(styleIndex);
        final byte[] grpprl = ParagraphSprmCompressor.compressParagraphProperty(pap, base);
        listLevel.setLevelProperties(grpprl);
    }
    
    public void setLevelStyle(final int level, final int styleIndex) {
        this._listData.setLevelStyle(level, styleIndex);
    }
    
    public ListData getListData() {
        return this._listData;
    }
    
    public ListFormatOverride getOverride() {
        return this._override;
    }
}
