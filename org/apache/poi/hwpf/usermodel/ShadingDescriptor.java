// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hwpf.usermodel;

import org.apache.poi.hwpf.model.types.SHDAbstractType;

public final class ShadingDescriptor extends SHDAbstractType implements Cloneable
{
    public ShadingDescriptor() {
    }
    
    public ShadingDescriptor(final byte[] buf, final int offset) {
        this.fillFields(buf, offset);
    }
    
    public ShadingDescriptor clone() throws CloneNotSupportedException {
        return (ShadingDescriptor)super.clone();
    }
    
    public boolean isEmpty() {
        return this.field_3_ipat == 0;
    }
    
    public byte[] serialize() {
        final byte[] result = new byte[SHDAbstractType.getSize()];
        this.serialize(result, 0);
        return result;
    }
    
    @Override
    public String toString() {
        if (this.isEmpty()) {
            return "[SHD] EMPTY";
        }
        return "[SHD] (cvFore: " + this.getCvFore() + "; cvBack: " + this.getCvBack() + "; iPat: " + this.getIpat() + ")";
    }
}
