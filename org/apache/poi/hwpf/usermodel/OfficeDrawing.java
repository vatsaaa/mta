// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hwpf.usermodel;

import org.apache.poi.ddf.EscherContainerRecord;

public interface OfficeDrawing
{
    HorizontalPositioning getHorizontalPositioning();
    
    HorizontalRelativeElement getHorizontalRelative();
    
    EscherContainerRecord getOfficeArtSpContainer();
    
    byte[] getPictureData();
    
    int getRectangleBottom();
    
    int getRectangleLeft();
    
    int getRectangleRight();
    
    int getRectangleTop();
    
    int getShapeId();
    
    VerticalPositioning getVerticalPositioning();
    
    VerticalRelativeElement getVerticalRelativeElement();
    
    public enum HorizontalPositioning
    {
        ABSOLUTE, 
        CENTER, 
        INSIDE, 
        LEFT, 
        OUTSIDE, 
        RIGHT;
    }
    
    public enum HorizontalRelativeElement
    {
        CHAR, 
        MARGIN, 
        PAGE, 
        TEXT;
    }
    
    public enum VerticalPositioning
    {
        ABSOLUTE, 
        BOTTOM, 
        CENTER, 
        INSIDE, 
        OUTSIDE, 
        TOP;
    }
    
    public enum VerticalRelativeElement
    {
        LINE, 
        MARGIN, 
        PAGE, 
        TEXT;
    }
}
