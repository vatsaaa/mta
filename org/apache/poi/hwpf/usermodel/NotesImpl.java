// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hwpf.usermodel;

import java.util.HashMap;
import org.apache.poi.hwpf.model.NotesTables;
import java.util.Map;

public class NotesImpl implements Notes
{
    private Map<Integer, Integer> anchorToIndexMap;
    private final NotesTables notesTables;
    
    public NotesImpl(final NotesTables notesTables) {
        this.anchorToIndexMap = null;
        this.notesTables = notesTables;
    }
    
    public int getNoteAnchorPosition(final int index) {
        return this.notesTables.getDescriptor(index).getStart();
    }
    
    public int getNoteIndexByAnchorPosition(final int anchorPosition) {
        this.updateAnchorToIndexMap();
        final Integer index = this.anchorToIndexMap.get(anchorPosition);
        if (index == null) {
            return -1;
        }
        return index;
    }
    
    public int getNotesCount() {
        return this.notesTables.getDescriptorsCount();
    }
    
    public int getNoteTextEndOffset(final int index) {
        return this.notesTables.getTextPosition(index).getEnd();
    }
    
    public int getNoteTextStartOffset(final int index) {
        return this.notesTables.getTextPosition(index).getStart();
    }
    
    private void updateAnchorToIndexMap() {
        if (this.anchorToIndexMap != null) {
            return;
        }
        final Map<Integer, Integer> result = new HashMap<Integer, Integer>();
        for (int n = 0; n < this.notesTables.getDescriptorsCount(); ++n) {
            final int anchorPosition = this.notesTables.getDescriptor(n).getStart();
            result.put(anchorPosition, n);
        }
        this.anchorToIndexMap = result;
    }
}
