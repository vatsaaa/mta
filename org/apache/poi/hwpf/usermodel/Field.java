// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hwpf.usermodel;

public interface Field
{
    Range firstSubrange(final Range p0);
    
    int getFieldEndOffset();
    
    int getFieldStartOffset();
    
    CharacterRun getMarkEndCharacterRun(final Range p0);
    
    int getMarkEndOffset();
    
    CharacterRun getMarkSeparatorCharacterRun(final Range p0);
    
    int getMarkSeparatorOffset();
    
    CharacterRun getMarkStartCharacterRun(final Range p0);
    
    int getMarkStartOffset();
    
    int getType();
    
    boolean hasSeparator();
    
    boolean isHasSep();
    
    boolean isLocked();
    
    boolean isNested();
    
    boolean isPrivateResult();
    
    boolean isResultDirty();
    
    boolean isResultEdited();
    
    boolean isZombieEmbed();
    
    Range secondSubrange(final Range p0);
}
