// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hwpf.usermodel;

import org.apache.poi.util.POILogFactory;
import org.apache.poi.util.Internal;
import org.apache.poi.hwpf.model.ListFormatOverride;
import org.apache.poi.hwpf.model.ListTables;
import org.apache.poi.hwpf.model.PAPX;
import org.apache.poi.hwpf.model.ListFormatOverrideLevel;
import org.apache.poi.hwpf.model.ListLevel;
import org.apache.poi.util.POILogger;

public final class ListEntry extends Paragraph
{
    private static POILogger log;
    ListLevel _level;
    ListFormatOverrideLevel _overrideLevel;
    
    @Internal
    ListEntry(final PAPX papx, final ParagraphProperties properties, final Range parent) {
        super(papx, properties, parent);
        final ListTables tables = parent._doc.getListTables();
        if (tables != null && this._props.getIlfo() < tables.getOverrideCount()) {
            final ListFormatOverride override = tables.getOverride(this._props.getIlfo());
            this._overrideLevel = override.getOverrideLevel(this._props.getIlvl());
            this._level = tables.getLevel(override.getLsid(), this._props.getIlvl());
        }
        else {
            ListEntry.log.log(POILogger.WARN, "No ListTables found for ListEntry - document probably partly corrupt, and you may experience problems");
        }
    }
    
    @Deprecated
    ListEntry(final PAPX papx, final Range parent, final ListTables tables) {
        super(papx, parent);
        if (tables != null && this._props.getIlfo() < tables.getOverrideCount()) {
            final ListFormatOverride override = tables.getOverride(this._props.getIlfo());
            this._overrideLevel = override.getOverrideLevel(this._props.getIlvl());
            this._level = tables.getLevel(override.getLsid(), this._props.getIlvl());
        }
        else {
            ListEntry.log.log(POILogger.WARN, "No ListTables found for ListEntry - document probably partly corrupt, and you may experience problems");
        }
    }
    
    @Deprecated
    @Override
    public int type() {
        return 4;
    }
    
    static {
        ListEntry.log = POILogFactory.getLogger(ListEntry.class);
    }
}
