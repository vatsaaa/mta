// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hwpf.usermodel;

import org.apache.poi.util.LittleEndian;
import org.apache.poi.hwpf.model.types.TCAbstractType;

public final class TableCellDescriptor extends TCAbstractType implements Cloneable
{
    public static final int SIZE = 20;
    
    protected void fillFields(final byte[] data, final int offset) {
        this.field_1_rgf = LittleEndian.getShort(data, 0 + offset);
        this.field_2_wWidth = LittleEndian.getShort(data, 2 + offset);
        this.setBrcTop(new BorderCode(data, 4 + offset));
        this.setBrcLeft(new BorderCode(data, 8 + offset));
        this.setBrcBottom(new BorderCode(data, 12 + offset));
        this.setBrcRight(new BorderCode(data, 16 + offset));
    }
    
    public void serialize(final byte[] data, final int offset) {
        LittleEndian.putShort(data, 0 + offset, this.field_1_rgf);
        LittleEndian.putShort(data, 2 + offset, this.field_2_wWidth);
        this.getBrcTop().serialize(data, 4 + offset);
        this.getBrcLeft().serialize(data, 8 + offset);
        this.getBrcBottom().serialize(data, 12 + offset);
        this.getBrcRight().serialize(data, 16 + offset);
    }
    
    public Object clone() throws CloneNotSupportedException {
        final TableCellDescriptor tc = (TableCellDescriptor)super.clone();
        tc.setShd(this.getShd().clone());
        tc.setBrcTop((BorderCode)this.getBrcTop().clone());
        tc.setBrcLeft((BorderCode)this.getBrcLeft().clone());
        tc.setBrcBottom((BorderCode)this.getBrcBottom().clone());
        tc.setBrcRight((BorderCode)this.getBrcRight().clone());
        return tc;
    }
    
    public static TableCellDescriptor convertBytesToTC(final byte[] buf, final int offset) {
        final TableCellDescriptor tc = new TableCellDescriptor();
        tc.fillFields(buf, offset);
        return tc;
    }
}
