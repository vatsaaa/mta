// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hwpf.usermodel;

import org.apache.poi.hwpf.model.Colorref;
import org.apache.poi.hwpf.model.types.SHD80AbstractType;

public final class ShadingDescriptor80 extends SHD80AbstractType implements Cloneable
{
    public ShadingDescriptor80() {
    }
    
    public ShadingDescriptor80(final byte[] buf, final int offset) {
        this.fillFields(buf, offset);
    }
    
    public ShadingDescriptor80(final short value) {
        this.field_1_value = value;
    }
    
    public ShadingDescriptor80 clone() throws CloneNotSupportedException {
        return (ShadingDescriptor80)super.clone();
    }
    
    public boolean isEmpty() {
        return this.field_1_value == 0;
    }
    
    public byte[] serialize() {
        final byte[] result = new byte[SHD80AbstractType.getSize()];
        this.serialize(result, 0);
        return result;
    }
    
    public ShadingDescriptor toShadingDescriptor() {
        final ShadingDescriptor result = new ShadingDescriptor();
        result.setCvFore(Colorref.valueOfIco(this.getIcoFore()));
        result.setCvBack(Colorref.valueOfIco(this.getIcoBack()));
        result.setIpat(this.getIpat());
        return result;
    }
    
    @Override
    public String toString() {
        if (this.isEmpty()) {
            return "[SHD80] EMPTY";
        }
        return "[SHD80] (icoFore: " + this.getIcoFore() + "; icoBack: " + this.getIcoBack() + "; iPat: " + this.getIpat() + ")";
    }
}
