// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hwpf.usermodel;

import org.apache.poi.hwpf.model.FileInformationBlock;
import org.apache.poi.hwpf.model.SubdocumentType;
import org.apache.poi.hwpf.model.PropertyNode;
import org.apache.poi.util.Internal;
import org.apache.poi.hwpf.model.ListTables;
import java.util.NoSuchElementException;
import org.apache.poi.util.LittleEndian;
import org.apache.poi.hwpf.sprm.ParagraphSprmCompressor;
import org.apache.poi.hwpf.model.StyleSheet;
import org.apache.poi.hwpf.sprm.SprmBuffer;
import org.apache.poi.hwpf.sprm.CharacterSprmCompressor;
import org.apache.poi.hwpf.HWPFDocument;
import org.apache.poi.util.POILogFactory;
import org.apache.poi.hwpf.model.CHPX;
import org.apache.poi.hwpf.model.PAPX;
import org.apache.poi.hwpf.model.SEPX;
import java.util.List;
import org.apache.poi.hwpf.HWPFDocumentCore;
import java.lang.ref.WeakReference;
import org.apache.poi.util.POILogger;

public class Range
{
    private POILogger logger;
    @Deprecated
    public static final int TYPE_PARAGRAPH = 0;
    @Deprecated
    public static final int TYPE_CHARACTER = 1;
    @Deprecated
    public static final int TYPE_SECTION = 2;
    @Deprecated
    public static final int TYPE_TEXT = 3;
    @Deprecated
    public static final int TYPE_LISTENTRY = 4;
    @Deprecated
    public static final int TYPE_TABLE = 5;
    @Deprecated
    public static final int TYPE_UNDEFINED = 6;
    private WeakReference<Range> _parent;
    protected int _start;
    protected int _end;
    protected HWPFDocumentCore _doc;
    boolean _sectionRangeFound;
    protected List<SEPX> _sections;
    protected int _sectionStart;
    protected int _sectionEnd;
    protected boolean _parRangeFound;
    protected List<PAPX> _paragraphs;
    protected int _parStart;
    protected int _parEnd;
    protected boolean _charRangeFound;
    protected List<CHPX> _characters;
    protected int _charStart;
    protected int _charEnd;
    protected StringBuilder _text;
    
    public Range(final int start, final int end, final HWPFDocumentCore doc) {
        this.logger = POILogFactory.getLogger(Range.class);
        this._start = start;
        this._end = end;
        this._doc = doc;
        this._sections = this._doc.getSectionTable().getSections();
        this._paragraphs = this._doc.getParagraphTable().getParagraphs();
        this._characters = this._doc.getCharacterTable().getTextRuns();
        this._text = this._doc.getText();
        this._parent = new WeakReference<Range>(null);
        this.sanityCheckStartEnd();
    }
    
    protected Range(final int start, final int end, final Range parent) {
        this.logger = POILogFactory.getLogger(Range.class);
        this._start = start;
        this._end = end;
        this._doc = parent._doc;
        this._sections = parent._sections;
        this._paragraphs = parent._paragraphs;
        this._characters = parent._characters;
        this._text = parent._text;
        this._parent = new WeakReference<Range>(parent);
        this.sanityCheckStartEnd();
        assert this.sanityCheck();
    }
    
    @Deprecated
    protected Range(final int startIdx, final int endIdx, final int idxType, final Range parent) {
        this.logger = POILogFactory.getLogger(Range.class);
        this._doc = parent._doc;
        this._sections = parent._sections;
        this._paragraphs = parent._paragraphs;
        this._characters = parent._characters;
        this._text = parent._text;
        this._parent = new WeakReference<Range>(parent);
        this.sanityCheckStartEnd();
    }
    
    private void sanityCheckStartEnd() {
        if (this._start < 0) {
            throw new IllegalArgumentException("Range start must not be negative. Given " + this._start);
        }
        if (this._end < this._start) {
            throw new IllegalArgumentException("The end (" + this._end + ") must not be before the start (" + this._start + ")");
        }
    }
    
    @Deprecated
    public boolean usesUnicode() {
        return true;
    }
    
    public String text() {
        return this._text.substring(this._start, this._end);
    }
    
    public static String stripFields(String text) {
        if (text.indexOf(19) == -1) {
            return text;
        }
        while (text.indexOf(19) > -1 && text.indexOf(21) > -1) {
            final int first13 = text.indexOf(19);
            final int next13 = text.indexOf(19, first13 + 1);
            final int first14 = text.indexOf(20, first13 + 1);
            final int last15 = text.lastIndexOf(21);
            if (last15 < first13) {
                break;
            }
            if (next13 == -1 && first14 == -1) {
                text = text.substring(0, first13) + text.substring(last15 + 1);
                break;
            }
            if (first14 != -1 && (first14 < next13 || next13 == -1)) {
                text = text.substring(0, first13) + text.substring(first14 + 1, last15) + text.substring(last15 + 1);
            }
            else {
                text = text.substring(0, first13) + text.substring(last15 + 1);
            }
        }
        return text;
    }
    
    public int numSections() {
        this.initSections();
        return this._sectionEnd - this._sectionStart;
    }
    
    public int numParagraphs() {
        this.initParagraphs();
        return this._parEnd - this._parStart;
    }
    
    public int numCharacterRuns() {
        this.initCharacterRuns();
        return this._charEnd - this._charStart;
    }
    
    public CharacterRun insertBefore(final String text) {
        this.initAll();
        this._text.insert(this._start, text);
        this._doc.getCharacterTable().adjustForInsert(this._charStart, text.length());
        this._doc.getParagraphTable().adjustForInsert(this._parStart, text.length());
        this._doc.getSectionTable().adjustForInsert(this._sectionStart, text.length());
        if (this._doc instanceof HWPFDocument) {
            ((BookmarksImpl)((HWPFDocument)this._doc).getBookmarks()).afterInsert(this._start, text.length());
        }
        this.adjustForInsert(text.length());
        this.adjustFIB(text.length());
        assert this.sanityCheck();
        return this.getCharacterRun(0);
    }
    
    public CharacterRun insertAfter(final String text) {
        this.initAll();
        this._text.insert(this._end, text);
        this._doc.getCharacterTable().adjustForInsert(this._charEnd - 1, text.length());
        this._doc.getParagraphTable().adjustForInsert(this._parEnd - 1, text.length());
        this._doc.getSectionTable().adjustForInsert(this._sectionEnd - 1, text.length());
        if (this._doc instanceof HWPFDocument) {
            ((BookmarksImpl)((HWPFDocument)this._doc).getBookmarks()).afterInsert(this._end, text.length());
        }
        this.adjustForInsert(text.length());
        assert this.sanityCheck();
        return this.getCharacterRun(this.numCharacterRuns() - 1);
    }
    
    @Deprecated
    public CharacterRun insertBefore(final String text, final CharacterProperties props) {
        this.initAll();
        final PAPX papx = this._paragraphs.get(this._parStart);
        final short istd = papx.getIstd();
        final StyleSheet ss = this._doc.getStyleSheet();
        final CharacterProperties baseStyle = ss.getCharacterStyle(istd);
        final byte[] grpprl = CharacterSprmCompressor.compressCharacterProperty(props, baseStyle);
        final SprmBuffer buf = new SprmBuffer(grpprl, 0);
        this._doc.getCharacterTable().insert(this._charStart, this._start, buf);
        return this.insertBefore(text);
    }
    
    @Deprecated
    public CharacterRun insertAfter(final String text, final CharacterProperties props) {
        this.initAll();
        final PAPX papx = this._paragraphs.get(this._parEnd - 1);
        final short istd = papx.getIstd();
        final StyleSheet ss = this._doc.getStyleSheet();
        final CharacterProperties baseStyle = ss.getCharacterStyle(istd);
        final byte[] grpprl = CharacterSprmCompressor.compressCharacterProperty(props, baseStyle);
        final SprmBuffer buf = new SprmBuffer(grpprl, 0);
        this._doc.getCharacterTable().insert(this._charEnd, this._end, buf);
        ++this._charEnd;
        return this.insertAfter(text);
    }
    
    @Deprecated
    public Paragraph insertBefore(final ParagraphProperties props, final int styleIndex) {
        return this.insertBefore(props, styleIndex, "\r");
    }
    
    @Deprecated
    protected Paragraph insertBefore(final ParagraphProperties props, final int styleIndex, final String text) {
        this.initAll();
        final StyleSheet ss = this._doc.getStyleSheet();
        final ParagraphProperties baseStyle = ss.getParagraphStyle(styleIndex);
        final CharacterProperties baseChp = ss.getCharacterStyle(styleIndex);
        final byte[] grpprl = ParagraphSprmCompressor.compressParagraphProperty(props, baseStyle);
        final byte[] withIndex = new byte[grpprl.length + 2];
        LittleEndian.putShort(withIndex, (short)styleIndex);
        System.arraycopy(grpprl, 0, withIndex, 2, grpprl.length);
        final SprmBuffer buf = new SprmBuffer(withIndex, 2);
        this._doc.getParagraphTable().insert(this._parStart, this._start, buf);
        this.insertBefore(text, baseChp);
        return this.getParagraph(0);
    }
    
    @Deprecated
    public Paragraph insertAfter(final ParagraphProperties props, final int styleIndex) {
        return this.insertAfter(props, styleIndex, "\r");
    }
    
    @Deprecated
    protected Paragraph insertAfter(final ParagraphProperties props, final int styleIndex, final String text) {
        this.initAll();
        final StyleSheet ss = this._doc.getStyleSheet();
        final ParagraphProperties baseStyle = ss.getParagraphStyle(styleIndex);
        final CharacterProperties baseChp = ss.getCharacterStyle(styleIndex);
        final byte[] grpprl = ParagraphSprmCompressor.compressParagraphProperty(props, baseStyle);
        final byte[] withIndex = new byte[grpprl.length + 2];
        LittleEndian.putShort(withIndex, (short)styleIndex);
        System.arraycopy(grpprl, 0, withIndex, 2, grpprl.length);
        final SprmBuffer buf = new SprmBuffer(withIndex, 2);
        this._doc.getParagraphTable().insert(this._parEnd, this._end, buf);
        ++this._parEnd;
        this.insertAfter(text, baseChp);
        return this.getParagraph(this.numParagraphs() - 1);
    }
    
    public void delete() {
        this.initAll();
        final int numSections = this._sections.size();
        final int numRuns = this._characters.size();
        final int numParagraphs = this._paragraphs.size();
        for (int x = this._charStart; x < numRuns; ++x) {
            final CHPX chpx = this._characters.get(x);
            chpx.adjustForDelete(this._start, this._end - this._start);
        }
        for (int x = this._parStart; x < numParagraphs; ++x) {
            final PAPX papx = this._paragraphs.get(x);
            papx.adjustForDelete(this._start, this._end - this._start);
        }
        for (int x = this._sectionStart; x < numSections; ++x) {
            final SEPX sepx = this._sections.get(x);
            sepx.adjustForDelete(this._start, this._end - this._start);
        }
        if (this._doc instanceof HWPFDocument) {
            ((BookmarksImpl)((HWPFDocument)this._doc).getBookmarks()).afterDelete(this._start, this._end - this._start);
        }
        this._text.delete(this._start, this._end);
        final Range parent = this._parent.get();
        if (parent != null) {
            parent.adjustForInsert(-(this._end - this._start));
        }
        this.adjustFIB(-(this._end - this._start));
    }
    
    @Deprecated
    public Table insertBefore(final TableProperties props, final int rows) {
        final ParagraphProperties parProps = new ParagraphProperties();
        parProps.setFInTable(true);
        parProps.setItap(1);
        final int oldEnd = this._end;
        final int columns = props.getItcMac();
        for (int x = 0; x < rows; ++x) {
            Paragraph cell = this.insertBefore(parProps, 4095);
            cell.insertAfter(String.valueOf('\u0007'));
            for (int y = 1; y < columns; ++y) {
                cell = cell.insertAfter(parProps, 4095);
                cell.insertAfter(String.valueOf('\u0007'));
            }
            cell = cell.insertAfter(parProps, 4095, String.valueOf('\u0007'));
            cell.setTableRowEnd(props);
        }
        final int newEnd = this._end;
        final int diff = newEnd - oldEnd;
        return new Table(this._start, this._start + diff, this, 1);
    }
    
    public Table insertTableBefore(final short columns, final int rows) {
        final ParagraphProperties parProps = new ParagraphProperties();
        parProps.setFInTable(true);
        parProps.setItap(1);
        final int oldEnd = this._end;
        for (int x = 0; x < rows; ++x) {
            Paragraph cell = this.insertBefore(parProps, 4095);
            cell.insertAfter(String.valueOf('\u0007'));
            for (int y = 1; y < columns; ++y) {
                cell = cell.insertAfter(parProps, 4095);
                cell.insertAfter(String.valueOf('\u0007'));
            }
            cell = cell.insertAfter(parProps, 4095, String.valueOf('\u0007'));
            cell.setTableRowEnd(new TableProperties(columns));
        }
        final int newEnd = this._end;
        final int diff = newEnd - oldEnd;
        return new Table(this._start, this._start + diff, this, 1);
    }
    
    @Deprecated
    public ListEntry insertBefore(final ParagraphProperties props, final int listID, final int level, final int styleIndex) {
        final ListTables lt = this._doc.getListTables();
        if (lt.getLevel(listID, level) == null) {
            throw new NoSuchElementException("The specified list and level do not exist");
        }
        final int ilfo = lt.getOverrideIndexFromListID(listID);
        props.setIlfo(ilfo);
        props.setIlvl((byte)level);
        return (ListEntry)this.insertBefore(props, styleIndex);
    }
    
    @Deprecated
    public ListEntry insertAfter(final ParagraphProperties props, final int listID, final int level, final int styleIndex) {
        final ListTables lt = this._doc.getListTables();
        if (lt.getLevel(listID, level) == null) {
            throw new NoSuchElementException("The specified list and level do not exist");
        }
        final int ilfo = lt.getOverrideIndexFromListID(listID);
        props.setIlfo(ilfo);
        props.setIlvl((byte)level);
        return (ListEntry)this.insertAfter(props, styleIndex);
    }
    
    public void replaceText(final String newText, final boolean addAfter) {
        if (addAfter) {
            final int originalEnd = this.getEndOffset();
            this.insertAfter(newText);
            new Range(this.getStartOffset(), originalEnd, this).delete();
        }
        else {
            final int originalStart = this.getStartOffset();
            final int originalEnd2 = this.getEndOffset();
            this.insertBefore(newText);
            new Range(originalStart + newText.length(), originalEnd2 + newText.length(), this).delete();
        }
    }
    
    @Internal
    public void replaceText(final String pPlaceHolder, final String pValue, final int pOffset) {
        final int absPlaceHolderIndex = this.getStartOffset() + pOffset;
        Range subRange = new Range(absPlaceHolderIndex, absPlaceHolderIndex + pPlaceHolder.length(), this);
        subRange.insertBefore(pValue);
        subRange = new Range(absPlaceHolderIndex + pValue.length(), absPlaceHolderIndex + pPlaceHolder.length() + pValue.length(), this);
        subRange.delete();
    }
    
    public void replaceText(final String pPlaceHolder, final String pValue) {
        boolean keepLooking = true;
        while (keepLooking) {
            final String text = this.text();
            final int offset = text.indexOf(pPlaceHolder);
            if (offset >= 0) {
                this.replaceText(pPlaceHolder, pValue, offset);
            }
            else {
                keepLooking = false;
            }
        }
    }
    
    public CharacterRun getCharacterRun(final int index) {
        this.initCharacterRuns();
        if (index + this._charStart >= this._charEnd) {
            throw new IndexOutOfBoundsException("CHPX #" + index + " (" + (index + this._charStart) + ") not in range [" + this._charStart + "; " + this._charEnd + ")");
        }
        final CHPX chpx = this._characters.get(index + this._charStart);
        if (chpx == null) {
            return null;
        }
        short istd;
        if (this instanceof Paragraph) {
            istd = ((Paragraph)this)._istd;
        }
        else {
            final int[] point = this.findRange(this._paragraphs, Math.max(chpx.getStart(), this._start), Math.min(chpx.getEnd(), this._end));
            this.initParagraphs();
            final int parStart = Math.max(point[0], this._parStart);
            if (parStart >= this._paragraphs.size()) {
                return null;
            }
            final PAPX papx = this._paragraphs.get(point[0]);
            istd = papx.getIstd();
        }
        final CharacterRun chp = new CharacterRun(chpx, this._doc.getStyleSheet(), istd, this);
        return chp;
    }
    
    public Section getSection(final int index) {
        this.initSections();
        final SEPX sepx = this._sections.get(index + this._sectionStart);
        final Section sep = new Section(sepx, this);
        return sep;
    }
    
    public Paragraph getParagraph(final int index) {
        this.initParagraphs();
        if (index + this._parStart >= this._parEnd) {
            throw new IndexOutOfBoundsException("Paragraph #" + index + " (" + (index + this._parStart) + ") not in range [" + this._parStart + "; " + this._parEnd + ")");
        }
        final PAPX papx = this._paragraphs.get(index + this._parStart);
        return Paragraph.newParagraph(this, papx);
    }
    
    @Deprecated
    public int type() {
        return 6;
    }
    
    public Table getTable(final Paragraph paragraph) {
        if (!paragraph.isInTable()) {
            throw new IllegalArgumentException("This paragraph doesn't belong to a table");
        }
        final Range r = paragraph;
        if (r._parent.get() != this) {
            throw new IllegalArgumentException("This paragraph is not a child of this range instance");
        }
        r.initAll();
        final int tableLevel = paragraph.getTableLevel();
        int tableEndInclusive = r._parStart;
        if (r._parStart != 0) {
            final Paragraph previous = Paragraph.newParagraph(this, this._paragraphs.get(r._parStart - 1));
            if (previous.isInTable() && previous.getTableLevel() == tableLevel && previous._sectionEnd >= r._sectionStart) {
                throw new IllegalArgumentException("This paragraph is not the first one in the table");
            }
        }
        final Range overallRange = this._doc.getOverallRange();
        for (int limit = this._paragraphs.size(); tableEndInclusive < limit - 1; ++tableEndInclusive) {
            final Paragraph next = Paragraph.newParagraph(overallRange, this._paragraphs.get(tableEndInclusive + 1));
            if (!next.isInTable()) {
                break;
            }
            if (next.getTableLevel() < tableLevel) {
                break;
            }
        }
        this.initAll();
        if (tableEndInclusive >= this._parEnd) {
            this.logger.log(POILogger.WARN, "The table's bounds ", "[" + this._parStart + "; " + tableEndInclusive + ")", " fall outside of this Range paragraphs numbers ", "[" + this._parStart + "; " + this._parEnd + ")");
        }
        if (tableEndInclusive < 0) {
            throw new ArrayIndexOutOfBoundsException("The table's end is negative, which isn't allowed!");
        }
        final int endOffsetExclusive = this._paragraphs.get(tableEndInclusive).getEnd();
        return new Table(paragraph.getStartOffset(), endOffsetExclusive, this, paragraph.getTableLevel());
    }
    
    protected void initAll() {
        this.initCharacterRuns();
        this.initParagraphs();
        this.initSections();
    }
    
    private void initParagraphs() {
        if (!this._parRangeFound) {
            final int[] point = this.findRange(this._paragraphs, this._start, this._end);
            this._parStart = point[0];
            this._parEnd = point[1];
            this._parRangeFound = true;
        }
    }
    
    private void initCharacterRuns() {
        if (!this._charRangeFound) {
            final int[] point = this.findRange(this._characters, this._start, this._end);
            this._charStart = point[0];
            this._charEnd = point[1];
            this._charRangeFound = true;
        }
    }
    
    private void initSections() {
        if (!this._sectionRangeFound) {
            final int[] point = this.findRange(this._sections, this._sectionStart, this._start, this._end);
            this._sectionStart = point[0];
            this._sectionEnd = point[1];
            this._sectionRangeFound = true;
        }
    }
    
    private static int binarySearchStart(final List<? extends PropertyNode<?>> rpl, final int start) {
        if (((PropertyNode)rpl.get(0)).getStart() >= start) {
            return 0;
        }
        int low = 0;
        int high = rpl.size() - 1;
        while (low <= high) {
            final int mid = low + high >>> 1;
            final PropertyNode<?> node = (PropertyNode<?>)rpl.get(mid);
            if (node.getStart() < start) {
                low = mid + 1;
            }
            else if (node.getStart() > start) {
                high = mid - 1;
            }
            else {
                assert node.getStart() == start;
                return mid;
            }
        }
        assert low != 0;
        return low - 1;
    }
    
    private static int binarySearchEnd(final List<? extends PropertyNode<?>> rpl, final int foundStart, final int end) {
        if (((PropertyNode)rpl.get(rpl.size() - 1)).getEnd() <= end) {
            return rpl.size() - 1;
        }
        int low = foundStart;
        int high = rpl.size() - 1;
        while (low <= high) {
            final int mid = low + high >>> 1;
            final PropertyNode<?> node = (PropertyNode<?>)rpl.get(mid);
            if (node.getEnd() < end) {
                low = mid + 1;
            }
            else if (node.getEnd() > end) {
                high = mid - 1;
            }
            else {
                assert node.getEnd() == end;
                return mid;
            }
        }
        assert 0 <= low && low < rpl.size();
        return low;
    }
    
    private int[] findRange(final List<? extends PropertyNode<?>> rpl, final int start, final int end) {
        int startIndex;
        for (startIndex = binarySearchStart(rpl, start); startIndex > 0 && ((PropertyNode)rpl.get(startIndex - 1)).getStart() >= start; --startIndex) {}
        int endIndex;
        for (endIndex = binarySearchEnd(rpl, startIndex, end); endIndex < rpl.size() - 1 && ((PropertyNode)rpl.get(endIndex + 1)).getEnd() <= end; ++endIndex) {}
        if (startIndex < 0 || startIndex >= rpl.size() || startIndex > endIndex || endIndex < 0 || endIndex >= rpl.size()) {
            throw new AssertionError();
        }
        return new int[] { startIndex, endIndex + 1 };
    }
    
    private int[] findRange(final List<? extends PropertyNode<?>> rpl, final int min, final int start, final int end) {
        int x = min;
        if (rpl.size() == min) {
            return new int[] { min, min };
        }
        PropertyNode<?> node;
        for (node = (PropertyNode<?>)rpl.get(x); node == null || (node.getEnd() <= start && x < rpl.size() - 1); node = (PropertyNode<?>)rpl.get(x)) {
            if (++x >= rpl.size()) {
                return new int[] { 0, 0 };
            }
        }
        if (node.getStart() > end) {
            return new int[] { 0, 0 };
        }
        if (node.getEnd() <= start) {
            return new int[] { rpl.size(), rpl.size() };
        }
        for (int y = x; y < rpl.size(); ++y) {
            node = (PropertyNode<?>)rpl.get(y);
            if (node != null) {
                if (node.getStart() >= end || node.getEnd() > end) {
                    if (node.getStart() < end) {
                        return new int[] { x, y + 1 };
                    }
                    return new int[] { x, y };
                }
            }
        }
        return new int[] { x, rpl.size() };
    }
    
    protected void reset() {
        this._charRangeFound = false;
        this._parRangeFound = false;
        this._sectionRangeFound = false;
    }
    
    protected void adjustFIB(final int adjustment) {
        assert this._doc instanceof HWPFDocument;
        final FileInformationBlock fib = this._doc.getFileInformationBlock();
        int currentEnd = 0;
        for (final SubdocumentType type : SubdocumentType.ORDERED) {
            final int currentLength = fib.getSubdocumentTextStreamLength(type);
            currentEnd += currentLength;
            if (this._start <= currentEnd) {
                fib.setSubdocumentTextStreamLength(type, currentLength + adjustment);
                break;
            }
        }
    }
    
    private void adjustForInsert(final int length) {
        this._end += length;
        this.reset();
        final Range parent = this._parent.get();
        if (parent != null) {
            parent.adjustForInsert(length);
        }
    }
    
    public int getStartOffset() {
        return this._start;
    }
    
    public int getEndOffset() {
        return this._end;
    }
    
    protected HWPFDocumentCore getDocument() {
        return this._doc;
    }
    
    @Override
    public String toString() {
        return "Range from " + this.getStartOffset() + " to " + this.getEndOffset() + " (chars)";
    }
    
    public boolean sanityCheck() {
        if (this._start < 0) {
            throw new AssertionError();
        }
        if (this._start > this._text.length()) {
            throw new AssertionError();
        }
        if (this._end < 0) {
            throw new AssertionError();
        }
        if (this._end > this._text.length()) {
            throw new AssertionError();
        }
        if (this._start > this._end) {
            throw new AssertionError();
        }
        if (this._charRangeFound) {
            for (int c = this._charStart; c < this._charEnd; ++c) {
                final CHPX chpx = this._characters.get(c);
                final int left = Math.max(this._start, chpx.getStart());
                final int right = Math.min(this._end, chpx.getEnd());
                if (left >= right) {
                    throw new AssertionError();
                }
            }
        }
        if (this._parRangeFound) {
            for (int p = this._parStart; p < this._parEnd; ++p) {
                final PAPX papx = this._paragraphs.get(p);
                final int left = Math.max(this._start, papx.getStart());
                final int right = Math.min(this._end, papx.getEnd());
                if (left >= right) {
                    throw new AssertionError();
                }
            }
        }
        return true;
    }
}
