// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hwpf.usermodel;

import org.apache.poi.util.LittleEndian;

public final class LineSpacingDescriptor implements Cloneable
{
    short _dyaLine;
    short _fMultiLinespace;
    
    public LineSpacingDescriptor() {
        this._dyaLine = 240;
        this._fMultiLinespace = 1;
    }
    
    public LineSpacingDescriptor(final byte[] buf, final int offset) {
        this._dyaLine = LittleEndian.getShort(buf, offset);
        this._fMultiLinespace = LittleEndian.getShort(buf, offset + 2);
    }
    
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
    
    public void setMultiLinespace(final short fMultiLinespace) {
        this._fMultiLinespace = fMultiLinespace;
    }
    
    public int toInt() {
        final byte[] intHolder = new byte[4];
        this.serialize(intHolder, 0);
        return LittleEndian.getInt(intHolder);
    }
    
    public void serialize(final byte[] buf, final int offset) {
        LittleEndian.putShort(buf, offset, this._dyaLine);
        LittleEndian.putShort(buf, offset + 2, this._fMultiLinespace);
    }
    
    public void setDyaLine(final short dyaLine) {
        this._dyaLine = dyaLine;
    }
    
    @Override
    public boolean equals(final Object o) {
        final LineSpacingDescriptor lspd = (LineSpacingDescriptor)o;
        return this._dyaLine == lspd._dyaLine && this._fMultiLinespace == lspd._fMultiLinespace;
    }
    
    public boolean isEmpty() {
        return this._dyaLine == 0 && this._fMultiLinespace == 0;
    }
    
    @Override
    public String toString() {
        if (this.isEmpty()) {
            return "[LSPD] EMPTY";
        }
        return "[LSPD] (dyaLine: " + this._dyaLine + "; fMultLinespace: " + this._fMultiLinespace + ")";
    }
}
