// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hwpf.usermodel;

import org.apache.poi.util.LittleEndian;
import org.apache.poi.hwpf.model.GenericPropertyNode;

@Deprecated
public final class Shape
{
    int _id;
    int _left;
    int _right;
    int _top;
    int _bottom;
    boolean _inDoc;
    
    public Shape(final GenericPropertyNode nodo) {
        final byte[] contenuto = nodo.getBytes();
        this._id = LittleEndian.getInt(contenuto);
        this._left = LittleEndian.getInt(contenuto, 4);
        this._top = LittleEndian.getInt(contenuto, 8);
        this._right = LittleEndian.getInt(contenuto, 12);
        this._bottom = LittleEndian.getInt(contenuto, 16);
        this._inDoc = (this._left >= 0 && this._right >= 0 && this._top >= 0 && this._bottom >= 0);
    }
    
    public int getId() {
        return this._id;
    }
    
    public int getLeft() {
        return this._left;
    }
    
    public int getRight() {
        return this._right;
    }
    
    public int getTop() {
        return this._top;
    }
    
    public int getBottom() {
        return this._bottom;
    }
    
    public int getWidth() {
        return this._right - this._left + 1;
    }
    
    public int getHeight() {
        return this._bottom - this._top + 1;
    }
    
    public boolean isWithinDocument() {
        return this._inDoc;
    }
}
