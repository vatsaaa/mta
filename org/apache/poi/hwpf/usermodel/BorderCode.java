// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hwpf.usermodel;

import org.apache.poi.util.BitFieldFactory;
import org.apache.poi.util.LittleEndian;
import org.apache.poi.util.BitField;

public final class BorderCode implements Cloneable
{
    public static final int SIZE = 4;
    private short _info;
    private static final BitField _dptLineWidth;
    private static final BitField _brcType;
    private short _info2;
    private static final BitField _ico;
    private static final BitField _dptSpace;
    private static final BitField _fShadow;
    private static final BitField _fFrame;
    
    public BorderCode() {
    }
    
    public BorderCode(final byte[] buf, final int offset) {
        this._info = LittleEndian.getShort(buf, offset);
        this._info2 = LittleEndian.getShort(buf, offset + 2);
    }
    
    public void serialize(final byte[] buf, final int offset) {
        LittleEndian.putShort(buf, offset, this._info);
        LittleEndian.putShort(buf, offset + 2, this._info2);
    }
    
    public int toInt() {
        final byte[] buf = new byte[4];
        this.serialize(buf, 0);
        return LittleEndian.getInt(buf);
    }
    
    public boolean isEmpty() {
        return (this._info == 0 && this._info2 == 0) || this._info == -1;
    }
    
    @Override
    public boolean equals(final Object o) {
        final BorderCode brc = (BorderCode)o;
        return this._info == brc._info && this._info2 == brc._info2;
    }
    
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
    
    public int getLineWidth() {
        return BorderCode._dptLineWidth.getShortValue(this._info);
    }
    
    public void setLineWidth(final int lineWidth) {
        BorderCode._dptLineWidth.setValue(this._info, lineWidth);
    }
    
    public int getBorderType() {
        return BorderCode._brcType.getShortValue(this._info);
    }
    
    public void setBorderType(final int borderType) {
        BorderCode._brcType.setValue(this._info, borderType);
    }
    
    public short getColor() {
        return BorderCode._ico.getShortValue(this._info2);
    }
    
    public void setColor(final short color) {
        BorderCode._ico.setValue(this._info2, color);
    }
    
    public int getSpace() {
        return BorderCode._dptSpace.getShortValue(this._info2);
    }
    
    public void setSpace(final int space) {
        BorderCode._dptSpace.setValue(this._info2, space);
    }
    
    public boolean isShadow() {
        return BorderCode._fShadow.getValue(this._info2) != 0;
    }
    
    public void setShadow(final boolean shadow) {
        BorderCode._fShadow.setValue(this._info2, shadow ? 1 : 0);
    }
    
    public boolean isFrame() {
        return BorderCode._fFrame.getValue(this._info2) != 0;
    }
    
    public void setFrame(final boolean frame) {
        BorderCode._fFrame.setValue(this._info2, frame ? 1 : 0);
    }
    
    @Override
    public String toString() {
        if (this.isEmpty()) {
            return "[BRC] EMPTY";
        }
        final StringBuffer buffer = new StringBuffer();
        buffer.append("[BRC]\n");
        buffer.append("        .dptLineWidth         = ");
        buffer.append(" (").append(this.getLineWidth()).append(" )\n");
        buffer.append("        .brcType              = ");
        buffer.append(" (").append(this.getBorderType()).append(" )\n");
        buffer.append("        .ico                  = ");
        buffer.append(" (").append(this.getColor()).append(" )\n");
        buffer.append("        .dptSpace             = ");
        buffer.append(" (").append(this.getSpace()).append(" )\n");
        buffer.append("        .fShadow              = ");
        buffer.append(" (").append(this.isShadow()).append(" )\n");
        buffer.append("        .fFrame               = ");
        buffer.append(" (").append(this.isFrame()).append(" )\n");
        return buffer.toString();
    }
    
    static {
        _dptLineWidth = BitFieldFactory.getInstance(255);
        _brcType = BitFieldFactory.getInstance(65280);
        _ico = BitFieldFactory.getInstance(255);
        _dptSpace = BitFieldFactory.getInstance(7936);
        _fShadow = BitFieldFactory.getInstance(8192);
        _fFrame = BitFieldFactory.getInstance(16384);
    }
}
