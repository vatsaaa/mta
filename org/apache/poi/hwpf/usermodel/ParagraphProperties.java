// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hwpf.usermodel;

import org.apache.poi.hwpf.model.types.PAPAbstractType;

public final class ParagraphProperties extends PAPAbstractType implements Cloneable
{
    private boolean jcLogical;
    
    public ParagraphProperties() {
        this.jcLogical = false;
        this.setAnld(new byte[84]);
        this.setPhe(new byte[12]);
    }
    
    public Object clone() throws CloneNotSupportedException {
        final ParagraphProperties pp = (ParagraphProperties)super.clone();
        pp.setAnld(this.getAnld().clone());
        pp.setBrcTop((BorderCode)this.getBrcTop().clone());
        pp.setBrcLeft((BorderCode)this.getBrcLeft().clone());
        pp.setBrcBottom((BorderCode)this.getBrcBottom().clone());
        pp.setBrcRight((BorderCode)this.getBrcRight().clone());
        pp.setBrcBetween((BorderCode)this.getBrcBetween().clone());
        pp.setBrcBar((BorderCode)this.getBrcBar().clone());
        pp.setDcs(this.getDcs().clone());
        pp.setLspd((LineSpacingDescriptor)this.getLspd().clone());
        pp.setShd(this.getShd().clone());
        pp.setPhe(this.getPhe().clone());
        return pp;
    }
    
    public BorderCode getBarBorder() {
        return super.getBrcBar();
    }
    
    public BorderCode getBottomBorder() {
        return super.getBrcBottom();
    }
    
    public DropCapSpecifier getDropCap() {
        return super.getDcs();
    }
    
    public int getFirstLineIndent() {
        return super.getDxaLeft1();
    }
    
    public int getFontAlignment() {
        return super.getWAlignFont();
    }
    
    public int getIndentFromLeft() {
        return super.getDxaLeft();
    }
    
    public int getIndentFromRight() {
        return super.getDxaRight();
    }
    
    public int getJustification() {
        if (!this.jcLogical) {
            return this.getJc();
        }
        if (!this.getFBiDi()) {
            return this.getJc();
        }
        switch (this.getJc()) {
            case 0: {
                return 2;
            }
            case 2: {
                return 0;
            }
            default: {
                return this.getJc();
            }
        }
    }
    
    public BorderCode getLeftBorder() {
        return super.getBrcLeft();
    }
    
    public LineSpacingDescriptor getLineSpacing() {
        return super.getLspd();
    }
    
    public BorderCode getRightBorder() {
        return super.getBrcRight();
    }
    
    public ShadingDescriptor getShading() {
        return super.getShd();
    }
    
    public int getSpacingAfter() {
        return super.getDyaAfter();
    }
    
    public int getSpacingBefore() {
        return super.getDyaBefore();
    }
    
    public BorderCode getTopBorder() {
        return super.getBrcTop();
    }
    
    public boolean isAutoHyphenated() {
        return !super.getFNoAutoHyph();
    }
    
    public boolean isBackward() {
        return super.isFBackward();
    }
    
    public boolean isKinsoku() {
        return super.getFKinsoku();
    }
    
    public boolean isLineNotNumbered() {
        return super.getFNoLnn();
    }
    
    public boolean isSideBySide() {
        return super.getFSideBySide();
    }
    
    public boolean isVertical() {
        return super.isFVertical();
    }
    
    public boolean isWidowControlled() {
        return super.getFWidowControl();
    }
    
    public boolean isWordWrapped() {
        return super.getFWordWrap();
    }
    
    public boolean keepOnPage() {
        return super.getFKeep();
    }
    
    public boolean keepWithNext() {
        return super.getFKeepFollow();
    }
    
    public boolean pageBreakBefore() {
        return super.getFPageBreakBefore();
    }
    
    public void setAutoHyphenated(final boolean auto) {
        super.setFNoAutoHyph(!auto);
    }
    
    public void setBackward(final boolean bward) {
        super.setFBackward(bward);
    }
    
    public void setBarBorder(final BorderCode bar) {
        super.setBrcBar(bar);
    }
    
    public void setBottomBorder(final BorderCode bottom) {
        super.setBrcBottom(bottom);
    }
    
    public void setDropCap(final DropCapSpecifier dcs) {
        super.setDcs(dcs);
    }
    
    public void setFirstLineIndent(final int first) {
        super.setDxaLeft1(first);
    }
    
    public void setFontAlignment(final int align) {
        super.setWAlignFont(align);
    }
    
    public void setIndentFromLeft(final int dxaLeft) {
        super.setDxaLeft(dxaLeft);
    }
    
    public void setIndentFromRight(final int dxaRight) {
        super.setDxaRight(dxaRight);
    }
    
    public void setJustification(final byte jc) {
        super.setJc(jc);
        this.jcLogical = false;
    }
    
    public void setJustificationLogical(final byte jc) {
        super.setJc(jc);
        this.jcLogical = true;
    }
    
    public void setKeepOnPage(final boolean fKeep) {
        super.setFKeep(fKeep);
    }
    
    public void setKeepWithNext(final boolean fKeepFollow) {
        super.setFKeepFollow(fKeepFollow);
    }
    
    public void setKinsoku(final boolean kinsoku) {
        super.setFKinsoku(kinsoku);
    }
    
    public void setLeftBorder(final BorderCode left) {
        super.setBrcLeft(left);
    }
    
    public void setLineNotNumbered(final boolean fNoLnn) {
        super.setFNoLnn(fNoLnn);
    }
    
    public void setLineSpacing(final LineSpacingDescriptor lspd) {
        super.setLspd(lspd);
    }
    
    public void setPageBreakBefore(final boolean fPageBreak) {
        super.setFPageBreakBefore(fPageBreak);
    }
    
    public void setRightBorder(final BorderCode right) {
        super.setBrcRight(right);
    }
    
    public void setShading(final ShadingDescriptor shd) {
        super.setShd(shd);
    }
    
    public void setSideBySide(final boolean fSideBySide) {
        super.setFSideBySide(fSideBySide);
    }
    
    public void setSpacingAfter(final int after) {
        super.setDyaAfter(after);
    }
    
    public void setSpacingBefore(final int before) {
        super.setDyaBefore(before);
    }
    
    public void setTopBorder(final BorderCode top) {
        super.setBrcTop(top);
    }
    
    public void setVertical(final boolean vertical) {
        super.setFVertical(vertical);
    }
    
    public void setWidowControl(final boolean widowControl) {
        super.setFWidowControl(widowControl);
    }
    
    public void setWordWrapped(final boolean wrap) {
        super.setFWordWrap(wrap);
    }
}
