// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hwpf.usermodel;

import java.util.List;
import java.util.Map;

public interface Bookmarks
{
    Bookmark getBookmark(final int p0) throws IndexOutOfBoundsException;
    
    int getBookmarksCount();
    
    Map<Integer, List<Bookmark>> getBookmarksStartedBetween(final int p0, final int p1);
    
    void remove(final int p0);
}
