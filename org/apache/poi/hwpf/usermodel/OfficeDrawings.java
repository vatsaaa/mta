// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hwpf.usermodel;

import java.util.Collection;

public interface OfficeDrawings
{
    OfficeDrawing getOfficeDrawingAt(final int p0);
    
    Collection<OfficeDrawing> getOfficeDrawings();
}
