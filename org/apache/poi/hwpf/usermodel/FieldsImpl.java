// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hwpf.usermodel;

import java.util.Iterator;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Collections;
import java.util.Collection;
import java.util.HashMap;
import org.apache.poi.hwpf.model.FieldsTables;
import org.apache.poi.hwpf.model.PlexOfField;
import java.util.List;
import org.apache.poi.hwpf.model.FieldsDocumentPart;
import java.util.Map;
import org.apache.poi.util.Internal;

@Internal
public class FieldsImpl implements Fields
{
    private Map<FieldsDocumentPart, Map<Integer, FieldImpl>> _fieldsByOffset;
    private PlexOfFieldComparator comparator;
    
    private static <T> int binarySearch(final List<PlexOfField> list, final int startIndex, final int endIndex, final int requiredStartOffset) {
        checkIndexForBinarySearch(list.size(), startIndex, endIndex);
        int low = startIndex;
        int mid = -1;
        int high = endIndex - 1;
        final int result = 0;
        while (low <= high) {
            mid = low + high >>> 1;
            final int midStart = list.get(mid).getFcStart();
            if (midStart == requiredStartOffset) {
                return mid;
            }
            if (midStart < requiredStartOffset) {
                low = mid + 1;
            }
            else {
                high = mid - 1;
            }
        }
        if (mid < 0) {
            int insertPoint = endIndex;
            for (int index = startIndex; index < endIndex; ++index) {
                if (requiredStartOffset < list.get(index).getFcStart()) {
                    insertPoint = index;
                }
            }
            return -insertPoint - 1;
        }
        return -mid - ((result >= 0) ? 1 : 2);
    }
    
    private static void checkIndexForBinarySearch(final int length, final int start, final int end) {
        if (start > end) {
            throw new IllegalArgumentException();
        }
        if (length < end || 0 > start) {
            throw new ArrayIndexOutOfBoundsException();
        }
    }
    
    public FieldsImpl(final FieldsTables fieldsTables) {
        this.comparator = new PlexOfFieldComparator();
        this._fieldsByOffset = new HashMap<FieldsDocumentPart, Map<Integer, FieldImpl>>(FieldsDocumentPart.values().length);
        for (final FieldsDocumentPart part : FieldsDocumentPart.values()) {
            final List<PlexOfField> plexOfCps = fieldsTables.getFieldsPLCF(part);
            this._fieldsByOffset.put(part, this.parseFieldStructure(plexOfCps));
        }
    }
    
    public Collection<Field> getFields(final FieldsDocumentPart part) {
        final Map<Integer, FieldImpl> map = this._fieldsByOffset.get(part);
        if (map == null || map.isEmpty()) {
            return (Collection<Field>)Collections.emptySet();
        }
        return Collections.unmodifiableCollection((Collection<? extends Field>)map.values());
    }
    
    public FieldImpl getFieldByStartOffset(final FieldsDocumentPart documentPart, final int offset) {
        final Map<Integer, FieldImpl> map = this._fieldsByOffset.get(documentPart);
        if (map == null || map.isEmpty()) {
            return null;
        }
        return map.get(offset);
    }
    
    private Map<Integer, FieldImpl> parseFieldStructure(final List<PlexOfField> plexOfFields) {
        if (plexOfFields == null || plexOfFields.isEmpty()) {
            return new HashMap<Integer, FieldImpl>();
        }
        Collections.sort(plexOfFields, this.comparator);
        final List<FieldImpl> fields = new ArrayList<FieldImpl>(plexOfFields.size() / 3 + 1);
        this.parseFieldStructureImpl(plexOfFields, 0, plexOfFields.size(), fields);
        final HashMap<Integer, FieldImpl> result = new HashMap<Integer, FieldImpl>(fields.size());
        for (final FieldImpl field : fields) {
            result.put(field.getFieldStartOffset(), field);
        }
        return result;
    }
    
    private void parseFieldStructureImpl(final List<PlexOfField> plexOfFields, final int startOffsetInclusive, final int endOffsetExclusive, final List<FieldImpl> result) {
        int next = startOffsetInclusive;
        while (next < endOffsetExclusive) {
            final PlexOfField startPlexOfField = plexOfFields.get(next);
            if (startPlexOfField.getFld().getBoundaryType() != 19) {
                ++next;
            }
            else {
                final int nextNodePositionInList = binarySearch(plexOfFields, next + 1, endOffsetExclusive, startPlexOfField.getFcEnd());
                if (nextNodePositionInList < 0) {
                    ++next;
                }
                else {
                    final PlexOfField nextPlexOfField = plexOfFields.get(nextNodePositionInList);
                    switch (nextPlexOfField.getFld().getBoundaryType()) {
                        case 20: {
                            final PlexOfField separatorPlexOfField = nextPlexOfField;
                            final int endNodePositionInList = binarySearch(plexOfFields, nextNodePositionInList, endOffsetExclusive, separatorPlexOfField.getFcEnd());
                            if (endNodePositionInList < 0) {
                                ++next;
                                continue;
                            }
                            final PlexOfField endPlexOfField = plexOfFields.get(endNodePositionInList);
                            if (endPlexOfField.getFld().getBoundaryType() != 21) {
                                ++next;
                                continue;
                            }
                            final FieldImpl field = new FieldImpl(startPlexOfField, separatorPlexOfField, endPlexOfField);
                            result.add(field);
                            if (startPlexOfField.getFcStart() + 1 < separatorPlexOfField.getFcStart() - 1) {
                                this.parseFieldStructureImpl(plexOfFields, next + 1, nextNodePositionInList, result);
                            }
                            if (separatorPlexOfField.getFcStart() + 1 < endPlexOfField.getFcStart() - 1) {
                                this.parseFieldStructureImpl(plexOfFields, nextNodePositionInList + 1, endNodePositionInList, result);
                            }
                            next = endNodePositionInList + 1;
                            continue;
                        }
                        case 21: {
                            final FieldImpl field2 = new FieldImpl(startPlexOfField, null, nextPlexOfField);
                            result.add(field2);
                            if (startPlexOfField.getFcStart() + 1 < nextPlexOfField.getFcStart() - 1) {
                                this.parseFieldStructureImpl(plexOfFields, next + 1, nextNodePositionInList, result);
                            }
                            next = nextNodePositionInList + 1;
                            continue;
                        }
                        default: {
                            ++next;
                            continue;
                        }
                    }
                }
            }
        }
    }
    
    private static final class PlexOfFieldComparator implements Comparator<PlexOfField>
    {
        public int compare(final PlexOfField o1, final PlexOfField o2) {
            final int thisVal = o1.getFcStart();
            final int anotherVal = o2.getFcStart();
            return (thisVal < anotherVal) ? -1 : ((thisVal == anotherVal) ? 0 : 1);
        }
    }
}
