// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hwpf.usermodel;

import org.apache.poi.hwpf.HWPFDocumentCore;
import org.apache.poi.hwpf.HWPFDocument;

public final class DocumentPosition extends Range
{
    public DocumentPosition(final HWPFDocument doc, final int pos) {
        super(pos, pos, doc);
    }
}
