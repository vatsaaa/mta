// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hwpf.usermodel;

import org.apache.poi.poifs.filesystem.Entry;

public interface ObjectsPool
{
    Entry getObjectById(final String p0);
}
