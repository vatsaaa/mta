// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hwpf.usermodel;

import org.apache.poi.util.BitFieldFactory;
import org.apache.poi.util.LittleEndian;
import org.apache.poi.util.BitField;

public final class DropCapSpecifier implements Cloneable
{
    private short _fdct;
    private static BitField _lines;
    private static BitField _type;
    
    public DropCapSpecifier() {
        this._fdct = 0;
    }
    
    public DropCapSpecifier(final byte[] buf, final int offset) {
        this(LittleEndian.getShort(buf, offset));
    }
    
    public DropCapSpecifier(final short fdct) {
        this._fdct = fdct;
    }
    
    public DropCapSpecifier clone() {
        return new DropCapSpecifier(this._fdct);
    }
    
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        final DropCapSpecifier other = (DropCapSpecifier)obj;
        return this._fdct == other._fdct;
    }
    
    public byte getCountOfLinesToDrop() {
        return (byte)DropCapSpecifier._lines.getValue(this._fdct);
    }
    
    public byte getDropCapType() {
        return (byte)DropCapSpecifier._type.getValue(this._fdct);
    }
    
    @Override
    public int hashCode() {
        return this._fdct;
    }
    
    public boolean isEmpty() {
        return this._fdct == 0;
    }
    
    public void setCountOfLinesToDrop(final byte value) {
        this._fdct = (short)DropCapSpecifier._lines.setValue(this._fdct, value);
    }
    
    public void setDropCapType(final byte value) {
        this._fdct = (short)DropCapSpecifier._type.setValue(this._fdct, value);
    }
    
    public short toShort() {
        return this._fdct;
    }
    
    @Override
    public String toString() {
        if (this.isEmpty()) {
            return "[DCS] EMPTY";
        }
        return "[DCS] (type: " + this.getDropCapType() + "; count: " + this.getCountOfLinesToDrop() + ")";
    }
    
    static {
        DropCapSpecifier._lines = BitFieldFactory.getInstance(248);
        DropCapSpecifier._type = BitFieldFactory.getInstance(7);
    }
}
