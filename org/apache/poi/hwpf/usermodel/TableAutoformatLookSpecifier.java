// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hwpf.usermodel;

import org.apache.poi.hwpf.model.types.TLPAbstractType;

public class TableAutoformatLookSpecifier extends TLPAbstractType implements Cloneable
{
    public static final int SIZE = 4;
    
    public TableAutoformatLookSpecifier() {
    }
    
    public TableAutoformatLookSpecifier(final byte[] data, final int offset) {
        this.fillFields(data, offset);
    }
    
    public TableAutoformatLookSpecifier clone() {
        try {
            return (TableAutoformatLookSpecifier)super.clone();
        }
        catch (CloneNotSupportedException e) {
            throw new Error(e.getMessage(), e);
        }
    }
    
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        final TableAutoformatLookSpecifier other = (TableAutoformatLookSpecifier)obj;
        return this.field_1_itl == other.field_1_itl && this.field_2_tlp_flags == other.field_2_tlp_flags;
    }
    
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = 31 * result + this.field_1_itl;
        result = 31 * result + this.field_2_tlp_flags;
        return result;
    }
    
    public boolean isEmpty() {
        return this.field_1_itl == 0 && this.field_2_tlp_flags == 0;
    }
}
