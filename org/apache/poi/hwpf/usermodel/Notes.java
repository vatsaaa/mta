// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hwpf.usermodel;

public interface Notes
{
    int getNoteAnchorPosition(final int p0);
    
    int getNotesCount();
    
    int getNoteIndexByAnchorPosition(final int p0);
    
    int getNoteTextEndOffset(final int p0);
    
    int getNoteTextStartOffset(final int p0);
}
