// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hwpf.usermodel;

import java.util.ArrayList;

public final class Table extends Range
{
    private ArrayList<TableRow> _rows;
    private boolean _rowsFound;
    private int _tableLevel;
    
    Table(final int startIdxInclusive, final int endIdxExclusive, final Range parent, final int levelNum) {
        super(startIdxInclusive, endIdxExclusive, parent);
        this._rowsFound = false;
        this._tableLevel = levelNum;
        this.initRows();
    }
    
    public TableRow getRow(final int index) {
        this.initRows();
        return this._rows.get(index);
    }
    
    public int getTableLevel() {
        return this._tableLevel;
    }
    
    private void initRows() {
        if (this._rowsFound) {
            return;
        }
        this._rows = new ArrayList<TableRow>();
        int rowStart = 0;
        int rowEnd = 0;
        final int numParagraphs = this.numParagraphs();
        while (rowEnd < numParagraphs) {
            final Paragraph startRowP = this.getParagraph(rowStart);
            final Paragraph endRowP = this.getParagraph(rowEnd);
            ++rowEnd;
            if (endRowP.isTableRowEnd() && endRowP.getTableLevel() == this._tableLevel) {
                this._rows.add(new TableRow(startRowP.getStartOffset(), endRowP.getEndOffset(), this, this._tableLevel));
                rowStart = rowEnd;
            }
        }
        this._rowsFound = true;
    }
    
    public int numRows() {
        this.initRows();
        return this._rows.size();
    }
    
    @Override
    protected void reset() {
        this._rowsFound = false;
    }
    
    @Override
    public int type() {
        return 5;
    }
}
