// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hwpf.usermodel;

import java.lang.reflect.Field;
import java.lang.reflect.AccessibleObject;
import org.apache.poi.hwpf.model.types.SEPAbstractType;

public final class SectionProperties extends SEPAbstractType
{
    public SectionProperties() {
        this.field_20_brcTop = new BorderCode();
        this.field_21_brcLeft = new BorderCode();
        this.field_22_brcBottom = new BorderCode();
        this.field_23_brcRight = new BorderCode();
        this.field_26_dttmPropRMark = new DateAndTime();
    }
    
    public Object clone() throws CloneNotSupportedException {
        final SectionProperties copy = (SectionProperties)super.clone();
        copy.field_20_brcTop = (BorderCode)this.field_20_brcTop.clone();
        copy.field_21_brcLeft = (BorderCode)this.field_21_brcLeft.clone();
        copy.field_22_brcBottom = (BorderCode)this.field_22_brcBottom.clone();
        copy.field_23_brcRight = (BorderCode)this.field_23_brcRight.clone();
        copy.field_26_dttmPropRMark = (DateAndTime)this.field_26_dttmPropRMark.clone();
        return copy;
    }
    
    @Override
    public boolean equals(final Object obj) {
        final Field[] fields = SectionProperties.class.getSuperclass().getDeclaredFields();
        AccessibleObject.setAccessible(fields, true);
        try {
            for (int x = 0; x < fields.length; ++x) {
                final Object obj2 = fields[x].get(this);
                final Object obj3 = fields[x].get(obj);
                if (obj2 != null || obj3 != null) {
                    if (!obj2.equals(obj3)) {
                        return false;
                    }
                }
            }
            return true;
        }
        catch (Exception e) {
            return false;
        }
    }
}
