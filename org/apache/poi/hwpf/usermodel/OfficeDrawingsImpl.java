// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hwpf.usermodel;

import java.util.Collections;
import java.util.ArrayList;
import java.util.Collection;
import org.apache.poi.ddf.EscherTertiaryOptRecord;
import org.apache.poi.ddf.EscherSimpleProperty;
import org.apache.poi.ddf.EscherOptRecord;
import org.apache.poi.hwpf.model.FSPA;
import java.util.Iterator;
import org.apache.poi.ddf.EscherSpRecord;
import org.apache.poi.ddf.EscherRecordFactory;
import java.util.List;
import org.apache.poi.ddf.DefaultEscherRecordFactory;
import org.apache.poi.ddf.EscherBSERecord;
import org.apache.poi.ddf.EscherRecord;
import org.apache.poi.ddf.EscherContainerRecord;
import org.apache.poi.ddf.EscherBlipRecord;
import org.apache.poi.hwpf.model.FSPATable;
import org.apache.poi.hwpf.model.EscherRecordHolder;

public class OfficeDrawingsImpl implements OfficeDrawings
{
    private final EscherRecordHolder _escherRecordHolder;
    private final FSPATable _fspaTable;
    private final byte[] _mainStream;
    
    public OfficeDrawingsImpl(final FSPATable fspaTable, final EscherRecordHolder escherRecordHolder, final byte[] mainStream) {
        this._fspaTable = fspaTable;
        this._escherRecordHolder = escherRecordHolder;
        this._mainStream = mainStream;
    }
    
    private EscherBlipRecord getBitmapRecord(final int bitmapIndex) {
        final List<? extends EscherContainerRecord> bContainers = this._escherRecordHolder.getBStoreContainers();
        if (bContainers == null || bContainers.size() != 1) {
            return null;
        }
        final EscherContainerRecord bContainer = (EscherContainerRecord)bContainers.get(0);
        final List<EscherRecord> bitmapRecords = bContainer.getChildRecords();
        if (bitmapRecords.size() < bitmapIndex) {
            return null;
        }
        final EscherRecord imageRecord = bitmapRecords.get(bitmapIndex - 1);
        if (imageRecord instanceof EscherBlipRecord) {
            return (EscherBlipRecord)imageRecord;
        }
        if (imageRecord instanceof EscherBSERecord) {
            final EscherBSERecord bseRecord = (EscherBSERecord)imageRecord;
            final EscherBlipRecord blip = bseRecord.getBlipRecord();
            if (blip != null) {
                return blip;
            }
            if (bseRecord.getOffset() > 0) {
                final EscherRecordFactory recordFactory = new DefaultEscherRecordFactory();
                final EscherRecord record = recordFactory.createRecord(this._mainStream, bseRecord.getOffset());
                if (record instanceof EscherBlipRecord) {
                    record.fillFields(this._mainStream, bseRecord.getOffset(), recordFactory);
                    return (EscherBlipRecord)record;
                }
            }
        }
        return null;
    }
    
    private EscherContainerRecord getEscherShapeRecordContainer(final int shapeId) {
        for (final EscherContainerRecord spContainer : this._escherRecordHolder.getSpContainers()) {
            final EscherSpRecord escherSpRecord = spContainer.getChildById((short)(-4086));
            if (escherSpRecord != null && escherSpRecord.getShapeId() == shapeId) {
                return spContainer;
            }
        }
        return null;
    }
    
    private OfficeDrawing getOfficeDrawing(final FSPA fspa) {
        return new OfficeDrawing() {
            public HorizontalPositioning getHorizontalPositioning() {
                final int value = this.getTertiaryPropertyValue(911, -1);
                switch (value) {
                    case 0: {
                        return HorizontalPositioning.ABSOLUTE;
                    }
                    case 1: {
                        return HorizontalPositioning.LEFT;
                    }
                    case 2: {
                        return HorizontalPositioning.CENTER;
                    }
                    case 3: {
                        return HorizontalPositioning.RIGHT;
                    }
                    case 4: {
                        return HorizontalPositioning.INSIDE;
                    }
                    case 5: {
                        return HorizontalPositioning.OUTSIDE;
                    }
                    default: {
                        return HorizontalPositioning.ABSOLUTE;
                    }
                }
            }
            
            public HorizontalRelativeElement getHorizontalRelative() {
                final int value = this.getTertiaryPropertyValue(912, -1);
                switch (value) {
                    case 1: {
                        return HorizontalRelativeElement.MARGIN;
                    }
                    case 2: {
                        return HorizontalRelativeElement.PAGE;
                    }
                    case 3: {
                        return HorizontalRelativeElement.TEXT;
                    }
                    case 4: {
                        return HorizontalRelativeElement.CHAR;
                    }
                    default: {
                        return HorizontalRelativeElement.TEXT;
                    }
                }
            }
            
            public EscherContainerRecord getOfficeArtSpContainer() {
                return OfficeDrawingsImpl.this.getEscherShapeRecordContainer(this.getShapeId());
            }
            
            public byte[] getPictureData() {
                final EscherContainerRecord shapeDescription = OfficeDrawingsImpl.this.getEscherShapeRecordContainer(this.getShapeId());
                if (shapeDescription == null) {
                    return null;
                }
                final EscherOptRecord escherOptRecord = shapeDescription.getChildById((short)(-4085));
                if (escherOptRecord == null) {
                    return null;
                }
                final EscherSimpleProperty escherProperty = escherOptRecord.lookup(260);
                if (escherProperty == null) {
                    return null;
                }
                final int bitmapIndex = escherProperty.getPropertyValue();
                final EscherBlipRecord escherBlipRecord = OfficeDrawingsImpl.this.getBitmapRecord(bitmapIndex);
                if (escherBlipRecord == null) {
                    return null;
                }
                return escherBlipRecord.getPicturedata();
            }
            
            public int getRectangleBottom() {
                return fspa.getYaBottom();
            }
            
            public int getRectangleLeft() {
                return fspa.getXaLeft();
            }
            
            public int getRectangleRight() {
                return fspa.getXaRight();
            }
            
            public int getRectangleTop() {
                return fspa.getYaTop();
            }
            
            public int getShapeId() {
                return fspa.getSpid();
            }
            
            private int getTertiaryPropertyValue(final int propertyId, final int defaultValue) {
                final EscherContainerRecord shapeDescription = OfficeDrawingsImpl.this.getEscherShapeRecordContainer(this.getShapeId());
                if (shapeDescription == null) {
                    return defaultValue;
                }
                final EscherTertiaryOptRecord escherTertiaryOptRecord = shapeDescription.getChildById((short)(-3806));
                if (escherTertiaryOptRecord == null) {
                    return defaultValue;
                }
                final EscherSimpleProperty escherProperty = escherTertiaryOptRecord.lookup(propertyId);
                if (escherProperty == null) {
                    return defaultValue;
                }
                final int value = escherProperty.getPropertyValue();
                return value;
            }
            
            public VerticalPositioning getVerticalPositioning() {
                final int value = this.getTertiaryPropertyValue(913, -1);
                switch (value) {
                    case 0: {
                        return VerticalPositioning.ABSOLUTE;
                    }
                    case 1: {
                        return VerticalPositioning.TOP;
                    }
                    case 2: {
                        return VerticalPositioning.CENTER;
                    }
                    case 3: {
                        return VerticalPositioning.BOTTOM;
                    }
                    case 4: {
                        return VerticalPositioning.INSIDE;
                    }
                    case 5: {
                        return VerticalPositioning.OUTSIDE;
                    }
                    default: {
                        return VerticalPositioning.ABSOLUTE;
                    }
                }
            }
            
            public VerticalRelativeElement getVerticalRelativeElement() {
                final int value = this.getTertiaryPropertyValue(913, -1);
                switch (value) {
                    case 1: {
                        return VerticalRelativeElement.MARGIN;
                    }
                    case 2: {
                        return VerticalRelativeElement.PAGE;
                    }
                    case 3: {
                        return VerticalRelativeElement.TEXT;
                    }
                    case 4: {
                        return VerticalRelativeElement.LINE;
                    }
                    default: {
                        return VerticalRelativeElement.TEXT;
                    }
                }
            }
            
            @Override
            public String toString() {
                return "OfficeDrawingImpl: " + fspa.toString();
            }
        };
    }
    
    public OfficeDrawing getOfficeDrawingAt(final int characterPosition) {
        final FSPA fspa = this._fspaTable.getFspaFromCp(characterPosition);
        if (fspa == null) {
            return null;
        }
        return this.getOfficeDrawing(fspa);
    }
    
    public Collection<OfficeDrawing> getOfficeDrawings() {
        final List<OfficeDrawing> result = new ArrayList<OfficeDrawing>();
        for (final FSPA fspa : this._fspaTable.getShapes()) {
            result.add(this.getOfficeDrawing(fspa));
        }
        return (Collection<OfficeDrawing>)Collections.unmodifiableList((List<?>)result);
    }
}
