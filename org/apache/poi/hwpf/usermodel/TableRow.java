// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hwpf.usermodel;

import org.apache.poi.util.POILogFactory;
import java.util.List;
import java.util.ArrayList;
import org.apache.poi.hwpf.sprm.TableSprmUncompressor;
import org.apache.poi.hwpf.sprm.SprmBuffer;
import org.apache.poi.util.POILogger;

public final class TableRow extends Range
{
    private static final POILogger logger;
    private static final short SPRM_DXAGAPHALF = -27134;
    private static final short SPRM_DYAROWHEIGHT = -27641;
    private static final short SPRM_FCANTSPLIT = 13315;
    private static final short SPRM_FTABLEHEADER = 13316;
    private static final short SPRM_TJC = 21504;
    private static final char TABLE_CELL_MARK = '\u0007';
    private TableCell[] _cells;
    private boolean _cellsFound;
    int _levelNum;
    private SprmBuffer _papx;
    private TableProperties _tprops;
    
    public TableRow(final int startIdxInclusive, final int endIdxExclusive, final Table parent, final int levelNum) {
        super(startIdxInclusive, endIdxExclusive, parent);
        this._cellsFound = false;
        final Paragraph last = this.getParagraph(this.numParagraphs() - 1);
        this._papx = last._papx;
        this._tprops = TableSprmUncompressor.uncompressTAP(this._papx);
        this._levelNum = levelNum;
        this.initCells();
    }
    
    public boolean cantSplit() {
        return this._tprops.getFCantSplit();
    }
    
    public BorderCode getBarBorder() {
        throw new UnsupportedOperationException("not applicable for TableRow");
    }
    
    public BorderCode getBottomBorder() {
        return this._tprops.getBrcBottom();
    }
    
    public TableCell getCell(final int index) {
        this.initCells();
        return this._cells[index];
    }
    
    public int getGapHalf() {
        return this._tprops.getDxaGapHalf();
    }
    
    public BorderCode getHorizontalBorder() {
        return this._tprops.getBrcHorizontal();
    }
    
    public BorderCode getLeftBorder() {
        return this._tprops.getBrcLeft();
    }
    
    public BorderCode getRightBorder() {
        return this._tprops.getBrcRight();
    }
    
    public int getRowHeight() {
        return this._tprops.getDyaRowHeight();
    }
    
    public int getRowJustification() {
        return this._tprops.getJc();
    }
    
    public BorderCode getTopBorder() {
        return this._tprops.getBrcBottom();
    }
    
    public BorderCode getVerticalBorder() {
        return this._tprops.getBrcVertical();
    }
    
    private void initCells() {
        if (this._cellsFound) {
            return;
        }
        final short expectedCellsCount = this._tprops.getItcMac();
        int lastCellStart = 0;
        final List<TableCell> cells = new ArrayList<TableCell>(expectedCellsCount + 1);
        for (int p = 0; p < this.numParagraphs(); ++p) {
            final Paragraph paragraph = this.getParagraph(p);
            final String s = paragraph.text();
            if (((s.length() > 0 && s.charAt(s.length() - 1) == '\u0007') || paragraph.isEmbeddedCellMark()) && paragraph.getTableLevel() == this._levelNum) {
                final TableCellDescriptor tableCellDescriptor = (this._tprops.getRgtc() != null && this._tprops.getRgtc().length > cells.size()) ? this._tprops.getRgtc()[cells.size()] : new TableCellDescriptor();
                final short leftEdge = (short)((this._tprops.getRgdxaCenter() != null && this._tprops.getRgdxaCenter().length > cells.size()) ? this._tprops.getRgdxaCenter()[cells.size()] : 0);
                final short rightEdge = (short)((this._tprops.getRgdxaCenter() != null && this._tprops.getRgdxaCenter().length > cells.size() + 1) ? this._tprops.getRgdxaCenter()[cells.size() + 1] : 0);
                final TableCell tableCell = new TableCell(this.getParagraph(lastCellStart).getStartOffset(), this.getParagraph(p).getEndOffset(), this, this._levelNum, tableCellDescriptor, leftEdge, rightEdge - leftEdge);
                cells.add(tableCell);
                lastCellStart = p + 1;
            }
        }
        if (lastCellStart < this.numParagraphs() - 1) {
            final TableCellDescriptor tableCellDescriptor2 = (this._tprops.getRgtc() != null && this._tprops.getRgtc().length > cells.size()) ? this._tprops.getRgtc()[cells.size()] : new TableCellDescriptor();
            final short leftEdge2 = (short)((this._tprops.getRgdxaCenter() != null && this._tprops.getRgdxaCenter().length > cells.size()) ? this._tprops.getRgdxaCenter()[cells.size()] : 0);
            final short rightEdge2 = (short)((this._tprops.getRgdxaCenter() != null && this._tprops.getRgdxaCenter().length > cells.size() + 1) ? this._tprops.getRgdxaCenter()[cells.size() + 1] : 0);
            final TableCell tableCell2 = new TableCell(lastCellStart, this.numParagraphs() - 1, this, this._levelNum, tableCellDescriptor2, leftEdge2, rightEdge2 - leftEdge2);
            cells.add(tableCell2);
        }
        if (!cells.isEmpty()) {
            final TableCell lastCell = cells.get(cells.size() - 1);
            if (lastCell.numParagraphs() == 1 && lastCell.getParagraph(0).isTableRowEnd()) {
                cells.remove(cells.size() - 1);
            }
        }
        if (cells.size() != expectedCellsCount) {
            TableRow.logger.log(POILogger.WARN, "Number of found table cells (" + cells.size() + ") for table row [" + this.getStartOffset() + "c; " + this.getEndOffset() + "c] not equals to stored property value " + expectedCellsCount);
            this._tprops.setItcMac((short)cells.size());
        }
        this._cells = cells.toArray(new TableCell[cells.size()]);
        this._cellsFound = true;
    }
    
    public boolean isTableHeader() {
        return this._tprops.getFTableHeader();
    }
    
    public int numCells() {
        this.initCells();
        return this._cells.length;
    }
    
    @Override
    protected void reset() {
        this._cellsFound = false;
    }
    
    public void setCantSplit(final boolean cantSplit) {
        this._tprops.setFCantSplit(cantSplit);
        this._papx.updateSprm((short)13315, (byte)(cantSplit ? 1 : 0));
    }
    
    public void setGapHalf(final int dxaGapHalf) {
        this._tprops.setDxaGapHalf(dxaGapHalf);
        this._papx.updateSprm((short)(-27134), (short)dxaGapHalf);
    }
    
    public void setRowHeight(final int dyaRowHeight) {
        this._tprops.setDyaRowHeight(dyaRowHeight);
        this._papx.updateSprm((short)(-27641), (short)dyaRowHeight);
    }
    
    public void setRowJustification(final int jc) {
        this._tprops.setJc((short)jc);
        this._papx.updateSprm((short)21504, (short)jc);
    }
    
    public void setTableHeader(final boolean tableHeader) {
        this._tprops.setFTableHeader(tableHeader);
        this._papx.updateSprm((short)13316, (byte)(tableHeader ? 1 : 0));
    }
    
    static {
        logger = POILogFactory.getLogger(TableRow.class);
    }
}
