// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hwpf;

import org.apache.poi.hwpf.usermodel.HWPFList;
import java.util.Iterator;
import org.apache.poi.hwpf.model.io.HWPFOutputStream;
import org.apache.poi.poifs.filesystem.EntryUtils;
import org.apache.poi.poifs.filesystem.DirectoryEntry;
import java.io.ByteArrayInputStream;
import org.apache.poi.poifs.filesystem.Entry;
import org.apache.poi.hwpf.model.io.HWPFFileSystem;
import java.io.OutputStream;
import org.apache.poi.hwpf.usermodel.OfficeDrawings;
import org.apache.poi.hwpf.usermodel.Range;
import org.apache.poi.util.Internal;
import org.apache.poi.hwpf.model.TextPiece;
import org.apache.poi.hwpf.model.TextPieceTable;
import org.apache.poi.hwpf.usermodel.FieldsImpl;
import org.apache.poi.hwpf.usermodel.BookmarksImpl;
import org.apache.poi.hwpf.model.ListTables;
import org.apache.poi.hwpf.model.FontTable;
import org.apache.poi.hwpf.model.StyleSheet;
import org.apache.poi.hwpf.model.SectionTable;
import org.apache.poi.hwpf.model.SubdocumentType;
import org.apache.poi.hwpf.model.FSPADocumentPart;
import org.apache.poi.hwpf.model.SinglentonTextPiece;
import org.apache.poi.hwpf.model.PAPBinTable;
import org.apache.poi.hwpf.model.CharIndexTranslator;
import org.apache.poi.hwpf.model.CHPBinTable;
import java.io.FileNotFoundException;
import org.apache.poi.poifs.filesystem.DocumentEntry;
import org.apache.poi.poifs.filesystem.DirectoryNode;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import java.io.IOException;
import java.io.InputStream;
import org.apache.poi.hwpf.usermodel.NotesImpl;
import org.apache.poi.hwpf.model.NoteType;
import org.apache.poi.hwpf.usermodel.Fields;
import org.apache.poi.hwpf.model.FieldsTables;
import org.apache.poi.hwpf.usermodel.Notes;
import org.apache.poi.hwpf.model.NotesTables;
import org.apache.poi.hwpf.usermodel.Bookmarks;
import org.apache.poi.hwpf.model.BookmarksTables;
import org.apache.poi.hwpf.usermodel.OfficeDrawingsImpl;
import org.apache.poi.hwpf.model.ShapesTable;
import org.apache.poi.hwpf.model.PicturesTable;
import org.apache.poi.hwpf.model.EscherRecordHolder;
import org.apache.poi.hwpf.model.FSPATable;
import org.apache.poi.hwpf.model.RevisionMarkAuthorTable;
import org.apache.poi.hwpf.model.SavedByTable;
import org.apache.poi.hwpf.model.ComplexFileTable;
import org.apache.poi.hwpf.model.DocumentProperties;

public final class HWPFDocument extends HWPFDocumentCore
{
    static final String PROPERTY_PRESERVE_BIN_TABLES = "org.apache.poi.hwpf.preserveBinTables";
    private static final String PROPERTY_PRESERVE_TEXT_TABLE = "org.apache.poi.hwpf.preserveTextTable";
    private static final String STREAM_DATA = "Data";
    private static final String STREAM_TABLE_0 = "0Table";
    private static final String STREAM_TABLE_1 = "1Table";
    protected byte[] _tableStream;
    protected byte[] _dataStream;
    protected DocumentProperties _dop;
    protected ComplexFileTable _cft;
    protected StringBuilder _text;
    protected SavedByTable _sbt;
    protected RevisionMarkAuthorTable _rmat;
    private FSPATable _fspaHeaders;
    private FSPATable _fspaMain;
    protected EscherRecordHolder _escherRecordHolder;
    protected PicturesTable _pictures;
    @Deprecated
    protected ShapesTable _officeArts;
    protected OfficeDrawingsImpl _officeDrawingsHeaders;
    protected OfficeDrawingsImpl _officeDrawingsMain;
    protected BookmarksTables _bookmarksTables;
    protected Bookmarks _bookmarks;
    protected NotesTables _endnotesTables;
    protected Notes _endnotes;
    protected NotesTables _footnotesTables;
    protected Notes _footnotes;
    protected FieldsTables _fieldsTables;
    protected Fields _fields;
    
    protected HWPFDocument() {
        this._endnotesTables = new NotesTables(NoteType.ENDNOTE);
        this._endnotes = new NotesImpl(this._endnotesTables);
        this._footnotesTables = new NotesTables(NoteType.FOOTNOTE);
        this._footnotes = new NotesImpl(this._footnotesTables);
        this._text = new StringBuilder("\r");
    }
    
    public HWPFDocument(final InputStream istream) throws IOException {
        this(HWPFDocumentCore.verifyAndBuildPOIFS(istream));
    }
    
    public HWPFDocument(final POIFSFileSystem pfilesystem) throws IOException {
        this(pfilesystem.getRoot());
    }
    
    @Deprecated
    public HWPFDocument(final DirectoryNode directory, final POIFSFileSystem pfilesystem) throws IOException {
        this(directory);
    }
    
    public HWPFDocument(final DirectoryNode directory) throws IOException {
        super(directory);
        this._endnotesTables = new NotesTables(NoteType.ENDNOTE);
        this._endnotes = new NotesImpl(this._endnotesTables);
        this._footnotesTables = new NotesTables(NoteType.FOOTNOTE);
        this._footnotes = new NotesImpl(this._footnotesTables);
        if (this._fib.getFibBase().getNFib() < 106) {
            throw new OldWordFileFormatException("The document is too old - Word 95 or older. Try HWPFOldDocument instead?");
        }
        String name = "0Table";
        if (this._fib.getFibBase().isFWhichTblStm()) {
            name = "1Table";
        }
        DocumentEntry tableProps;
        try {
            tableProps = (DocumentEntry)directory.getEntry(name);
        }
        catch (FileNotFoundException fnfe) {
            throw new IllegalStateException("Table Stream '" + name + "' wasn't found - Either the document is corrupt, or is Word95 (or earlier)");
        }
        this._tableStream = new byte[tableProps.getSize()];
        directory.createDocumentInputStream(name).read(this._tableStream);
        this._fib.fillVariableFields(this._mainStream, this._tableStream);
        try {
            final DocumentEntry dataProps = (DocumentEntry)directory.getEntry("Data");
            this._dataStream = new byte[dataProps.getSize()];
            directory.createDocumentInputStream("Data").read(this._dataStream);
        }
        catch (FileNotFoundException e) {
            this._dataStream = new byte[0];
        }
        final int fcMin = 0;
        this._dop = new DocumentProperties(this._tableStream, this._fib.getFcDop(), this._fib.getLcbDop());
        this._cft = new ComplexFileTable(this._mainStream, this._tableStream, this._fib.getFcClx(), fcMin);
        TextPieceTable _tpt = this._cft.getTextPieceTable();
        this._cbt = new CHPBinTable(this._mainStream, this._tableStream, this._fib.getFcPlcfbteChpx(), this._fib.getLcbPlcfbteChpx(), _tpt);
        this._pbt = new PAPBinTable(this._mainStream, this._tableStream, this._dataStream, this._fib.getFcPlcfbtePapx(), this._fib.getLcbPlcfbtePapx(), _tpt);
        this._text = _tpt.getText();
        boolean preserveBinTables = false;
        try {
            preserveBinTables = Boolean.parseBoolean(System.getProperty("org.apache.poi.hwpf.preserveBinTables"));
        }
        catch (Exception ex) {}
        if (!preserveBinTables) {
            this._cbt.rebuild(this._cft);
            this._pbt.rebuild(this._text, this._cft);
        }
        boolean preserveTextTable = false;
        try {
            preserveTextTable = Boolean.parseBoolean(System.getProperty("org.apache.poi.hwpf.preserveTextTable"));
        }
        catch (Exception ex2) {}
        if (!preserveTextTable) {
            this._cft = new ComplexFileTable();
            _tpt = this._cft.getTextPieceTable();
            final TextPiece textPiece = new SinglentonTextPiece(this._text);
            _tpt.add(textPiece);
            this._text = textPiece.getStringBuilder();
        }
        this._fspaHeaders = new FSPATable(this._tableStream, this._fib, FSPADocumentPart.HEADER);
        this._fspaMain = new FSPATable(this._tableStream, this._fib, FSPADocumentPart.MAIN);
        if (this._fib.getFcDggInfo() != 0) {
            this._escherRecordHolder = new EscherRecordHolder(this._tableStream, this._fib.getFcDggInfo(), this._fib.getLcbDggInfo());
        }
        else {
            this._escherRecordHolder = new EscherRecordHolder();
        }
        this._pictures = new PicturesTable(this, this._dataStream, this._mainStream, this._fspaMain, this._escherRecordHolder);
        this._officeArts = new ShapesTable(this._tableStream, this._fib);
        this._officeDrawingsHeaders = new OfficeDrawingsImpl(this._fspaHeaders, this._escherRecordHolder, this._mainStream);
        this._officeDrawingsMain = new OfficeDrawingsImpl(this._fspaMain, this._escherRecordHolder, this._mainStream);
        this._st = new SectionTable(this._mainStream, this._tableStream, this._fib.getFcPlcfsed(), this._fib.getLcbPlcfsed(), fcMin, _tpt, this._fib.getSubdocumentTextStreamLength(SubdocumentType.MAIN));
        this._ss = new StyleSheet(this._tableStream, this._fib.getFcStshf());
        this._ft = new FontTable(this._tableStream, this._fib.getFcSttbfffn(), this._fib.getLcbSttbfffn());
        final int listOffset = this._fib.getFcPlcfLst();
        final int lfoOffset = this._fib.getFcPlfLfo();
        if (listOffset != 0 && this._fib.getLcbPlcfLst() != 0) {
            this._lt = new ListTables(this._tableStream, this._fib.getFcPlcfLst(), this._fib.getFcPlfLfo());
        }
        final int sbtOffset = this._fib.getFcSttbSavedBy();
        final int sbtLength = this._fib.getLcbSttbSavedBy();
        if (sbtOffset != 0 && sbtLength != 0) {
            this._sbt = new SavedByTable(this._tableStream, sbtOffset, sbtLength);
        }
        final int rmarkOffset = this._fib.getFcSttbfRMark();
        final int rmarkLength = this._fib.getLcbSttbfRMark();
        if (rmarkOffset != 0 && rmarkLength != 0) {
            this._rmat = new RevisionMarkAuthorTable(this._tableStream, rmarkOffset, rmarkLength);
        }
        this._bookmarksTables = new BookmarksTables(this._tableStream, this._fib);
        this._bookmarks = new BookmarksImpl(this._bookmarksTables);
        this._endnotesTables = new NotesTables(NoteType.ENDNOTE, this._tableStream, this._fib);
        this._endnotes = new NotesImpl(this._endnotesTables);
        this._footnotesTables = new NotesTables(NoteType.FOOTNOTE, this._tableStream, this._fib);
        this._footnotes = new NotesImpl(this._footnotesTables);
        this._fieldsTables = new FieldsTables(this._tableStream, this._fib);
        this._fields = new FieldsImpl(this._fieldsTables);
    }
    
    @Internal
    @Override
    public TextPieceTable getTextTable() {
        return this._cft.getTextPieceTable();
    }
    
    @Internal
    @Override
    public StringBuilder getText() {
        return this._text;
    }
    
    public DocumentProperties getDocProperties() {
        return this._dop;
    }
    
    @Override
    public Range getOverallRange() {
        return new Range(0, this._text.length(), this);
    }
    
    @Override
    public Range getRange() {
        return this.getRange(SubdocumentType.MAIN);
    }
    
    private Range getRange(final SubdocumentType subdocument) {
        int startCp = 0;
        for (final SubdocumentType previos : SubdocumentType.ORDERED) {
            final int length = this.getFileInformationBlock().getSubdocumentTextStreamLength(previos);
            if (subdocument == previos) {
                return new Range(startCp, startCp + length, this);
            }
            startCp += length;
        }
        throw new UnsupportedOperationException("Subdocument type not supported: " + subdocument);
    }
    
    public Range getFootnoteRange() {
        return this.getRange(SubdocumentType.FOOTNOTE);
    }
    
    public Range getEndnoteRange() {
        return this.getRange(SubdocumentType.ENDNOTE);
    }
    
    public Range getCommentsRange() {
        return this.getRange(SubdocumentType.ANNOTATION);
    }
    
    public Range getMainTextboxRange() {
        return this.getRange(SubdocumentType.TEXTBOX);
    }
    
    public Range getHeaderStoryRange() {
        return this.getRange(SubdocumentType.HEADER);
    }
    
    public int characterLength() {
        return this._text.length();
    }
    
    @Internal
    public SavedByTable getSavedByTable() {
        return this._sbt;
    }
    
    @Internal
    public RevisionMarkAuthorTable getRevisionMarkAuthorTable() {
        return this._rmat;
    }
    
    public PicturesTable getPicturesTable() {
        return this._pictures;
    }
    
    @Internal
    public EscherRecordHolder getEscherRecordHolder() {
        return this._escherRecordHolder;
    }
    
    @Deprecated
    @Internal
    public ShapesTable getShapesTable() {
        return this._officeArts;
    }
    
    public OfficeDrawings getOfficeDrawingsHeaders() {
        return this._officeDrawingsHeaders;
    }
    
    public OfficeDrawings getOfficeDrawingsMain() {
        return this._officeDrawingsMain;
    }
    
    public Bookmarks getBookmarks() {
        return this._bookmarks;
    }
    
    public Notes getEndnotes() {
        return this._endnotes;
    }
    
    public Notes getFootnotes() {
        return this._footnotes;
    }
    
    @Deprecated
    @Internal
    public FieldsTables getFieldsTables() {
        return this._fieldsTables;
    }
    
    public Fields getFields() {
        return this._fields;
    }
    
    @Override
    public void write(final OutputStream out) throws IOException {
        final HWPFFileSystem docSys = new HWPFFileSystem();
        final HWPFOutputStream wordDocumentStream = docSys.getStream("WordDocument");
        final HWPFOutputStream tableStream = docSys.getStream("1Table");
        int tableOffset = 0;
        this._fib.clearOffsetsSizes();
        int fibSize = this._fib.getSize();
        fibSize += 512 - fibSize % 512;
        final byte[] placeHolder = new byte[fibSize];
        wordDocumentStream.write(placeHolder);
        final int mainOffset = wordDocumentStream.getOffset();
        this._fib.setFcStshf(tableOffset);
        this._ss.writeTo(tableStream);
        this._fib.setLcbStshf(tableStream.getOffset() - tableOffset);
        tableOffset = tableStream.getOffset();
        final int fcMin = mainOffset;
        this._fib.setFcClx(tableOffset);
        this._cft.writeTo(wordDocumentStream, tableStream);
        this._fib.setLcbClx(tableStream.getOffset() - tableOffset);
        tableOffset = tableStream.getOffset();
        final int fcMac = wordDocumentStream.getOffset();
        this._fib.setFcDop(tableOffset);
        this._dop.writeTo(tableStream);
        this._fib.setLcbDop(tableStream.getOffset() - tableOffset);
        tableOffset = tableStream.getOffset();
        if (this._bookmarksTables != null) {
            this._bookmarksTables.writePlcfBkmkf(this._fib, tableStream);
            tableOffset = tableStream.getOffset();
        }
        if (this._bookmarksTables != null) {
            this._bookmarksTables.writePlcfBkmkl(this._fib, tableStream);
            tableOffset = tableStream.getOffset();
        }
        this._fib.setFcPlcfbteChpx(tableOffset);
        this._cbt.writeTo(wordDocumentStream, tableStream, fcMin, this._cft.getTextPieceTable());
        this._fib.setLcbPlcfbteChpx(tableStream.getOffset() - tableOffset);
        tableOffset = tableStream.getOffset();
        this._fib.setFcPlcfbtePapx(tableOffset);
        this._pbt.writeTo(wordDocumentStream, tableStream, this._cft.getTextPieceTable());
        this._fib.setLcbPlcfbtePapx(tableStream.getOffset() - tableOffset);
        tableOffset = tableStream.getOffset();
        this._endnotesTables.writeRef(this._fib, tableStream);
        this._endnotesTables.writeTxt(this._fib, tableStream);
        tableOffset = tableStream.getOffset();
        if (this._fieldsTables != null) {
            this._fieldsTables.write(this._fib, tableStream);
            tableOffset = tableStream.getOffset();
        }
        this._footnotesTables.writeRef(this._fib, tableStream);
        this._footnotesTables.writeTxt(this._fib, tableStream);
        tableOffset = tableStream.getOffset();
        this._fib.setFcPlcfsed(tableOffset);
        this._st.writeTo(wordDocumentStream, tableStream);
        this._fib.setLcbPlcfsed(tableStream.getOffset() - tableOffset);
        tableOffset = tableStream.getOffset();
        if (this._lt != null) {
            this._lt.writeListDataTo(this._fib, tableStream);
            tableOffset = tableStream.getOffset();
            this._fib.setFcPlfLfo(tableStream.getOffset());
            this._lt.writeListOverridesTo(tableStream);
            this._fib.setLcbPlfLfo(tableStream.getOffset() - tableOffset);
            tableOffset = tableStream.getOffset();
        }
        if (this._bookmarksTables != null) {
            this._bookmarksTables.writeSttbfBkmk(this._fib, tableStream);
            tableOffset = tableStream.getOffset();
        }
        if (this._sbt != null) {
            this._fib.setFcSttbSavedBy(tableOffset);
            this._sbt.writeTo(tableStream);
            this._fib.setLcbSttbSavedBy(tableStream.getOffset() - tableOffset);
            tableOffset = tableStream.getOffset();
        }
        if (this._rmat != null) {
            this._fib.setFcSttbfRMark(tableOffset);
            this._rmat.writeTo(tableStream);
            this._fib.setLcbSttbfRMark(tableStream.getOffset() - tableOffset);
            tableOffset = tableStream.getOffset();
        }
        this._fib.setFcSttbfffn(tableOffset);
        this._ft.writeTo(tableStream);
        this._fib.setLcbSttbfffn(tableStream.getOffset() - tableOffset);
        tableOffset = tableStream.getOffset();
        this._fib.getFibBase().setFcMin(fcMin);
        this._fib.getFibBase().setFcMac(fcMac);
        this._fib.setCbMac(wordDocumentStream.getOffset());
        byte[] mainBuf = wordDocumentStream.toByteArray();
        if (mainBuf.length < 4096) {
            final byte[] tempBuf = new byte[4096];
            System.arraycopy(mainBuf, 0, tempBuf, 0, mainBuf.length);
            mainBuf = tempBuf;
        }
        this._fib.getFibBase().setFWhichTblStm(true);
        this._fib.writeTo(mainBuf, tableStream);
        byte[] tableBuf = tableStream.toByteArray();
        if (tableBuf.length < 4096) {
            final byte[] tempBuf2 = new byte[4096];
            System.arraycopy(tableBuf, 0, tempBuf2, 0, tableBuf.length);
            tableBuf = tempBuf2;
        }
        byte[] dataBuf = this._dataStream;
        if (dataBuf == null) {
            dataBuf = new byte[4096];
        }
        if (dataBuf.length < 4096) {
            final byte[] tempBuf3 = new byte[4096];
            System.arraycopy(dataBuf, 0, tempBuf3, 0, dataBuf.length);
            dataBuf = tempBuf3;
        }
        final POIFSFileSystem pfs = new POIFSFileSystem();
        boolean docWritten = false;
        boolean dataWritten = false;
        boolean objectPoolWritten = false;
        boolean tableWritten = false;
        boolean propertiesWritten = false;
        final Iterator<Entry> iter = this.directory.getEntries();
        while (iter.hasNext()) {
            final Entry entry = iter.next();
            if (entry.getName().equals("WordDocument")) {
                if (docWritten) {
                    continue;
                }
                pfs.createDocument(new ByteArrayInputStream(mainBuf), "WordDocument");
                docWritten = true;
            }
            else if (entry.getName().equals("ObjectPool")) {
                if (objectPoolWritten) {
                    continue;
                }
                this._objectPool.writeTo(pfs.getRoot());
                objectPoolWritten = true;
            }
            else if (entry.getName().equals("0Table") || entry.getName().equals("1Table")) {
                if (tableWritten) {
                    continue;
                }
                pfs.createDocument(new ByteArrayInputStream(tableBuf), "1Table");
                tableWritten = true;
            }
            else if (entry.getName().equals("\u0005SummaryInformation") || entry.getName().equals("\u0005DocumentSummaryInformation")) {
                if (propertiesWritten) {
                    continue;
                }
                this.writeProperties(pfs);
                propertiesWritten = true;
            }
            else if (entry.getName().equals("Data")) {
                if (dataWritten) {
                    continue;
                }
                pfs.createDocument(new ByteArrayInputStream(dataBuf), "Data");
                dataWritten = true;
            }
            else {
                EntryUtils.copyNodeRecursively(entry, pfs.getRoot());
            }
        }
        if (!docWritten) {
            pfs.createDocument(new ByteArrayInputStream(mainBuf), "WordDocument");
        }
        if (!tableWritten) {
            pfs.createDocument(new ByteArrayInputStream(tableBuf), "1Table");
        }
        if (!propertiesWritten) {
            this.writeProperties(pfs);
        }
        if (!dataWritten) {
            pfs.createDocument(new ByteArrayInputStream(dataBuf), "Data");
        }
        if (!objectPoolWritten) {
            this._objectPool.writeTo(pfs.getRoot());
        }
        pfs.writeFilesystem(out);
        this.directory = pfs.getRoot();
        this.directory = pfs.getRoot();
        this._tableStream = tableStream.toByteArray();
        this._dataStream = dataBuf;
    }
    
    @Internal
    public byte[] getDataStream() {
        return this._dataStream;
    }
    
    @Internal
    public byte[] getTableStream() {
        return this._tableStream;
    }
    
    public int registerList(final HWPFList list) {
        if (this._lt == null) {
            this._lt = new ListTables();
        }
        return this._lt.addList(list.getListData(), list.getOverride());
    }
    
    public void delete(final int start, final int length) {
        final Range r = new Range(start, start + length, this);
        r.delete();
    }
}
