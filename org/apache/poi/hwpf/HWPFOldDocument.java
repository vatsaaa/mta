// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hwpf;

import java.io.OutputStream;
import org.apache.poi.hwpf.usermodel.Range;
import java.util.Iterator;
import org.apache.poi.hwpf.model.OldSectionTable;
import org.apache.poi.hwpf.model.OldPAPBinTable;
import org.apache.poi.hwpf.model.OldCHPBinTable;
import org.apache.poi.hwpf.model.PieceDescriptor;
import org.apache.poi.hwpf.model.TextPiece;
import org.apache.poi.hwpf.model.ComplexFileTable;
import org.apache.poi.util.LittleEndian;
import org.apache.poi.poifs.filesystem.DirectoryNode;
import java.io.IOException;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.hwpf.model.TextPieceTable;

public class HWPFOldDocument extends HWPFDocumentCore
{
    private TextPieceTable tpt;
    private StringBuilder _text;
    
    public HWPFOldDocument(final POIFSFileSystem fs) throws IOException {
        this(fs.getRoot());
    }
    
    @Deprecated
    public HWPFOldDocument(final DirectoryNode directory, final POIFSFileSystem fs) throws IOException {
        this(directory);
    }
    
    public HWPFOldDocument(final DirectoryNode directory) throws IOException {
        super(directory);
        final int sedTableOffset = LittleEndian.getInt(this._mainStream, 136);
        final int sedTableSize = LittleEndian.getInt(this._mainStream, 140);
        final int chpTableOffset = LittleEndian.getInt(this._mainStream, 184);
        final int chpTableSize = LittleEndian.getInt(this._mainStream, 188);
        final int papTableOffset = LittleEndian.getInt(this._mainStream, 192);
        final int papTableSize = LittleEndian.getInt(this._mainStream, 196);
        final int complexTableOffset = LittleEndian.getInt(this._mainStream, 352);
        ComplexFileTable cft = null;
        final StringBuffer text = new StringBuffer();
        if (this._fib.getFibBase().isFComplex()) {
            cft = new ComplexFileTable(this._mainStream, this._mainStream, complexTableOffset, this._fib.getFibBase().getFcMin());
            this.tpt = cft.getTextPieceTable();
            for (final TextPiece tp : this.tpt.getTextPieces()) {
                text.append((CharSequence)tp.getStringBuilder());
            }
        }
        else {
            final PieceDescriptor pd = new PieceDescriptor(new byte[] { 0, 0, 0, 0, 0, 127, 0, 0 }, 0);
            pd.setFilePosition(this._fib.getFibBase().getFcMin());
            this.tpt = new TextPieceTable();
            final byte[] textData = new byte[this._fib.getFibBase().getFcMac() - this._fib.getFibBase().getFcMin()];
            System.arraycopy(this._mainStream, this._fib.getFibBase().getFcMin(), textData, 0, textData.length);
            final TextPiece tp2 = new TextPiece(0, textData.length, textData, pd);
            this.tpt.add(tp2);
            text.append((CharSequence)tp2.getStringBuilder());
        }
        this._text = this.tpt.getText();
        this._cbt = new OldCHPBinTable(this._mainStream, chpTableOffset, chpTableSize, this._fib.getFibBase().getFcMin(), this.tpt);
        this._pbt = new OldPAPBinTable(this._mainStream, papTableOffset, papTableSize, this._fib.getFibBase().getFcMin(), this.tpt);
        this._st = new OldSectionTable(this._mainStream, sedTableOffset, sedTableSize, this._fib.getFibBase().getFcMin(), this.tpt);
        boolean preserveBinTables = false;
        try {
            preserveBinTables = Boolean.parseBoolean(System.getProperty("org.apache.poi.hwpf.preserveBinTables"));
        }
        catch (Exception ex) {}
        if (!preserveBinTables) {
            this._cbt.rebuild(cft);
            this._pbt.rebuild(this._text, cft);
        }
    }
    
    @Override
    public Range getOverallRange() {
        return new Range(0, this._fib.getFibBase().getFcMac() - this._fib.getFibBase().getFcMin(), this);
    }
    
    @Override
    public Range getRange() {
        return this.getOverallRange();
    }
    
    @Override
    public TextPieceTable getTextTable() {
        return this.tpt;
    }
    
    @Override
    public StringBuilder getText() {
        return this._text;
    }
    
    @Override
    public void write(final OutputStream out) throws IOException {
        throw new IllegalStateException("Writing is not available for the older file formats");
    }
}
