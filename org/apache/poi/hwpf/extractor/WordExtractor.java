// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hwpf.extractor;

import org.apache.poi.hwpf.converter.WordToTextConverter;
import org.apache.poi.hwpf.usermodel.HeaderStories;
import org.apache.poi.hwpf.usermodel.Paragraph;
import org.apache.poi.hwpf.usermodel.Range;
import java.io.FileInputStream;
import org.apache.poi.POIDocument;
import org.apache.poi.poifs.filesystem.DirectoryNode;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import java.io.IOException;
import org.apache.poi.hwpf.HWPFDocumentCore;
import java.io.InputStream;
import org.apache.poi.hwpf.HWPFDocument;
import org.apache.poi.POIOLE2TextExtractor;

public final class WordExtractor extends POIOLE2TextExtractor
{
    private HWPFDocument doc;
    
    public WordExtractor(final InputStream is) throws IOException {
        this(HWPFDocumentCore.verifyAndBuildPOIFS(is));
    }
    
    public WordExtractor(final POIFSFileSystem fs) throws IOException {
        this(new HWPFDocument(fs));
    }
    
    @Deprecated
    public WordExtractor(final DirectoryNode dir, final POIFSFileSystem fs) throws IOException {
        this(dir);
    }
    
    public WordExtractor(final DirectoryNode dir) throws IOException {
        this(new HWPFDocument(dir));
    }
    
    public WordExtractor(final HWPFDocument doc) {
        super(doc);
        this.doc = doc;
    }
    
    public static void main(final String[] args) throws IOException {
        if (args.length == 0) {
            System.err.println("Use:");
            System.err.println("   java org.apache.poi.hwpf.extractor.WordExtractor <filename>");
            System.exit(1);
        }
        final FileInputStream fin = new FileInputStream(args[0]);
        final WordExtractor extractor = new WordExtractor(fin);
        System.out.println(extractor.getText());
    }
    
    public String[] getParagraphText() {
        String[] ret;
        try {
            final Range r = this.doc.getRange();
            ret = getParagraphText(r);
        }
        catch (Exception e) {
            ret = new String[] { this.getTextFromPieces() };
        }
        return ret;
    }
    
    public String[] getFootnoteText() {
        final Range r = this.doc.getFootnoteRange();
        return getParagraphText(r);
    }
    
    public String[] getMainTextboxText() {
        final Range r = this.doc.getMainTextboxRange();
        return getParagraphText(r);
    }
    
    public String[] getEndnoteText() {
        final Range r = this.doc.getEndnoteRange();
        return getParagraphText(r);
    }
    
    public String[] getCommentsText() {
        final Range r = this.doc.getCommentsRange();
        return getParagraphText(r);
    }
    
    protected static String[] getParagraphText(final Range r) {
        final String[] ret = new String[r.numParagraphs()];
        for (int i = 0; i < ret.length; ++i) {
            final Paragraph p = r.getParagraph(i);
            ret[i] = p.text();
            if (ret[i].endsWith("\r")) {
                ret[i] += "\n";
            }
        }
        return ret;
    }
    
    private void appendHeaderFooter(String text, final StringBuffer out) {
        if (text == null || text.length() == 0) {
            return;
        }
        text = text.replace('\r', '\n');
        if (!text.endsWith("\n")) {
            out.append(text);
            out.append('\n');
            return;
        }
        if (text.endsWith("\n\n")) {
            out.append(text.substring(0, text.length() - 1));
            return;
        }
        out.append(text);
    }
    
    @Deprecated
    public String getHeaderText() {
        final HeaderStories hs = new HeaderStories(this.doc);
        final StringBuffer ret = new StringBuffer();
        if (hs.getFirstHeader() != null) {
            this.appendHeaderFooter(hs.getFirstHeader(), ret);
        }
        if (hs.getEvenHeader() != null) {
            this.appendHeaderFooter(hs.getEvenHeader(), ret);
        }
        if (hs.getOddHeader() != null) {
            this.appendHeaderFooter(hs.getOddHeader(), ret);
        }
        return ret.toString();
    }
    
    @Deprecated
    public String getFooterText() {
        final HeaderStories hs = new HeaderStories(this.doc);
        final StringBuffer ret = new StringBuffer();
        if (hs.getFirstFooter() != null) {
            this.appendHeaderFooter(hs.getFirstFooter(), ret);
        }
        if (hs.getEvenFooter() != null) {
            this.appendHeaderFooter(hs.getEvenFooter(), ret);
        }
        if (hs.getOddFooter() != null) {
            this.appendHeaderFooter(hs.getOddFooter(), ret);
        }
        return ret.toString();
    }
    
    public String getTextFromPieces() {
        String text = this.doc.getDocumentText();
        text = text.replaceAll("\r\r\r", "\r\n\r\n\r\n");
        text = text.replaceAll("\r\r", "\r\n\r\n");
        if (text.endsWith("\r")) {
            text += "\n";
        }
        return text;
    }
    
    @Override
    public String getText() {
        try {
            final WordToTextConverter wordToTextConverter = new WordToTextConverter();
            final HeaderStories hs = new HeaderStories(this.doc);
            if (hs.getFirstHeaderSubrange() != null) {
                wordToTextConverter.processDocumentPart(this.doc, hs.getFirstHeaderSubrange());
            }
            if (hs.getEvenHeaderSubrange() != null) {
                wordToTextConverter.processDocumentPart(this.doc, hs.getEvenHeaderSubrange());
            }
            if (hs.getOddHeaderSubrange() != null) {
                wordToTextConverter.processDocumentPart(this.doc, hs.getOddHeaderSubrange());
            }
            wordToTextConverter.processDocument(this.doc);
            wordToTextConverter.processDocumentPart(this.doc, this.doc.getMainTextboxRange());
            if (hs.getFirstFooterSubrange() != null) {
                wordToTextConverter.processDocumentPart(this.doc, hs.getFirstFooterSubrange());
            }
            if (hs.getEvenFooterSubrange() != null) {
                wordToTextConverter.processDocumentPart(this.doc, hs.getEvenFooterSubrange());
            }
            if (hs.getOddFooterSubrange() != null) {
                wordToTextConverter.processDocumentPart(this.doc, hs.getOddFooterSubrange());
            }
            return wordToTextConverter.getText();
        }
        catch (Exception exc) {
            throw new RuntimeException(exc);
        }
    }
    
    public static String stripFields(final String text) {
        return Range.stripFields(text);
    }
}
