// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi.hwpf.extractor;

import org.apache.poi.hwpf.HWPFDocumentCore;
import org.apache.poi.hwpf.converter.WordToTextConverter;
import org.apache.poi.hwpf.usermodel.Range;
import org.apache.poi.hwpf.model.TextPiece;
import org.apache.poi.POIDocument;
import org.apache.poi.poifs.filesystem.DirectoryNode;
import java.io.IOException;
import java.io.InputStream;
import org.apache.poi.hwpf.HWPFOldDocument;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.POIOLE2TextExtractor;

public final class Word6Extractor extends POIOLE2TextExtractor
{
    private POIFSFileSystem fs;
    private HWPFOldDocument doc;
    
    public Word6Extractor(final InputStream is) throws IOException {
        this(new POIFSFileSystem(is));
    }
    
    public Word6Extractor(final POIFSFileSystem fs) throws IOException {
        this(fs.getRoot());
    }
    
    @Deprecated
    public Word6Extractor(final DirectoryNode dir, final POIFSFileSystem fs) throws IOException {
        this(dir);
    }
    
    public Word6Extractor(final DirectoryNode dir) throws IOException {
        this(new HWPFOldDocument(dir));
    }
    
    public Word6Extractor(final HWPFOldDocument doc) {
        super(doc);
        this.doc = doc;
    }
    
    @Deprecated
    public String[] getParagraphText() {
        String[] ret;
        try {
            final Range r = this.doc.getRange();
            ret = WordExtractor.getParagraphText(r);
        }
        catch (Exception e) {
            ret = new String[this.doc.getTextTable().getTextPieces().size()];
            for (int i = 0; i < ret.length; ++i) {
                (ret[i] = this.doc.getTextTable().getTextPieces().get(i).getStringBuilder().toString()).replaceAll("\r", "\ufffe");
                ret[i].replaceAll("\ufffe", "\r\n");
            }
        }
        return ret;
    }
    
    @Override
    public String getText() {
        try {
            final WordToTextConverter wordToTextConverter = new WordToTextConverter();
            wordToTextConverter.processDocument(this.doc);
            return wordToTextConverter.getText();
        }
        catch (Exception exc) {
            final StringBuffer text = new StringBuffer();
            for (final String t : this.getParagraphText()) {
                text.append(t);
            }
            return text.toString();
        }
    }
}
