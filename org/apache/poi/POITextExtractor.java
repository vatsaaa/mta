// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi;

public abstract class POITextExtractor
{
    protected POIDocument document;
    
    public POITextExtractor(final POIDocument document) {
        this.document = document;
    }
    
    protected POITextExtractor(final POITextExtractor otherExtractor) {
        this.document = otherExtractor.document;
    }
    
    public abstract String getText();
    
    public abstract POITextExtractor getMetadataTextExtractor();
}
