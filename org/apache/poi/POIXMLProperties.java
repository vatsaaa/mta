// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.poi;

import java.util.Iterator;
import org.openxmlformats.schemas.officeDocument.x2006.customProperties.CTProperty;
import org.openxmlformats.schemas.officeDocument.x2006.extendedProperties.CTProperties;
import org.apache.poi.openxml4j.util.Nullable;
import java.util.Date;
import java.io.OutputStream;
import org.apache.poi.openxml4j.opc.PackagePartName;
import java.util.Map;
import java.util.HashMap;
import org.apache.xmlbeans.XmlOptions;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.openxml4j.opc.TargetMode;
import org.apache.poi.openxml4j.opc.PackagingURIHelper;
import org.apache.xmlbeans.XmlException;
import org.apache.poi.openxml4j.exceptions.OpenXML4JException;
import java.io.IOException;
import org.apache.poi.openxml4j.opc.PackageRelationshipCollection;
import org.apache.poi.openxml4j.opc.internal.PackagePropertiesPart;
import org.openxmlformats.schemas.officeDocument.x2006.extendedProperties.PropertiesDocument;
import org.apache.poi.openxml4j.opc.PackagePart;
import org.apache.poi.openxml4j.opc.OPCPackage;

public class POIXMLProperties
{
    private OPCPackage pkg;
    private CoreProperties core;
    private ExtendedProperties ext;
    private CustomProperties cust;
    private PackagePart extPart;
    private PackagePart custPart;
    private static final PropertiesDocument NEW_EXT_INSTANCE;
    private static final org.openxmlformats.schemas.officeDocument.x2006.customProperties.PropertiesDocument NEW_CUST_INSTANCE;
    
    public POIXMLProperties(final OPCPackage docPackage) throws IOException, OpenXML4JException, XmlException {
        this.pkg = docPackage;
        this.core = new CoreProperties((PackagePropertiesPart)this.pkg.getPackageProperties());
        final PackageRelationshipCollection extRel = this.pkg.getRelationshipsByType("http://schemas.openxmlformats.org/officeDocument/2006/relationships/extended-properties");
        if (extRel.size() == 1) {
            this.extPart = this.pkg.getPart(extRel.getRelationship(0));
            final PropertiesDocument props = PropertiesDocument.Factory.parse(this.extPart.getInputStream());
            this.ext = new ExtendedProperties(props);
        }
        else {
            this.extPart = null;
            this.ext = new ExtendedProperties((PropertiesDocument)POIXMLProperties.NEW_EXT_INSTANCE.copy());
        }
        final PackageRelationshipCollection custRel = this.pkg.getRelationshipsByType("http://schemas.openxmlformats.org/officeDocument/2006/relationships/custom-properties");
        if (custRel.size() == 1) {
            this.custPart = this.pkg.getPart(custRel.getRelationship(0));
            final org.openxmlformats.schemas.officeDocument.x2006.customProperties.PropertiesDocument props2 = org.openxmlformats.schemas.officeDocument.x2006.customProperties.PropertiesDocument.Factory.parse(this.custPart.getInputStream());
            this.cust = new CustomProperties(props2);
        }
        else {
            this.custPart = null;
            this.cust = new CustomProperties((org.openxmlformats.schemas.officeDocument.x2006.customProperties.PropertiesDocument)POIXMLProperties.NEW_CUST_INSTANCE.copy());
        }
    }
    
    public CoreProperties getCoreProperties() {
        return this.core;
    }
    
    public ExtendedProperties getExtendedProperties() {
        return this.ext;
    }
    
    public CustomProperties getCustomProperties() {
        return this.cust;
    }
    
    public void commit() throws IOException {
        if (this.extPart == null && !POIXMLProperties.NEW_EXT_INSTANCE.toString().equals(this.ext.props.toString())) {
            try {
                final PackagePartName prtname = PackagingURIHelper.createPartName("/docProps/app.xml");
                this.pkg.addRelationship(prtname, TargetMode.INTERNAL, "http://schemas.openxmlformats.org/officeDocument/2006/relationships/extended-properties");
                this.extPart = this.pkg.createPart(prtname, "application/vnd.openxmlformats-officedocument.extended-properties+xml");
            }
            catch (InvalidFormatException e) {
                throw new POIXMLException(e);
            }
        }
        if (this.custPart == null && !POIXMLProperties.NEW_CUST_INSTANCE.toString().equals(this.cust.props.toString())) {
            try {
                final PackagePartName prtname = PackagingURIHelper.createPartName("/docProps/custom.xml");
                this.pkg.addRelationship(prtname, TargetMode.INTERNAL, "http://schemas.openxmlformats.org/officeDocument/2006/relationships/custom-properties");
                this.custPart = this.pkg.createPart(prtname, "application/vnd.openxmlformats-officedocument.custom-properties+xml");
            }
            catch (InvalidFormatException e) {
                throw new POIXMLException(e);
            }
        }
        if (this.extPart != null) {
            final XmlOptions xmlOptions = new XmlOptions(POIXMLDocumentPart.DEFAULT_XML_OPTIONS);
            final Map<String, String> map = new HashMap<String, String>();
            map.put("http://schemas.openxmlformats.org/officeDocument/2006/docPropsVTypes", "vt");
            xmlOptions.setSaveSuggestedPrefixes(map);
            final OutputStream out = this.extPart.getOutputStream();
            this.ext.props.save(out, xmlOptions);
            out.close();
        }
        if (this.custPart != null) {
            final XmlOptions xmlOptions = new XmlOptions(POIXMLDocumentPart.DEFAULT_XML_OPTIONS);
            final Map<String, String> map = new HashMap<String, String>();
            map.put("http://schemas.openxmlformats.org/officeDocument/2006/docPropsVTypes", "vt");
            xmlOptions.setSaveSuggestedPrefixes(map);
            final OutputStream out = this.custPart.getOutputStream();
            this.cust.props.save(out, xmlOptions);
            out.close();
        }
    }
    
    static {
        (NEW_EXT_INSTANCE = PropertiesDocument.Factory.newInstance()).addNewProperties();
        (NEW_CUST_INSTANCE = org.openxmlformats.schemas.officeDocument.x2006.customProperties.PropertiesDocument.Factory.newInstance()).addNewProperties();
    }
    
    public class CoreProperties
    {
        private PackagePropertiesPart part;
        
        private CoreProperties(final PackagePropertiesPart part) {
            this.part = part;
        }
        
        public String getCategory() {
            return this.part.getCategoryProperty().getValue();
        }
        
        public void setCategory(final String category) {
            this.part.setCategoryProperty(category);
        }
        
        public String getContentStatus() {
            return this.part.getContentStatusProperty().getValue();
        }
        
        public void setContentStatus(final String contentStatus) {
            this.part.setContentStatusProperty(contentStatus);
        }
        
        public String getContentType() {
            return this.part.getContentTypeProperty().getValue();
        }
        
        public void setContentType(final String contentType) {
            this.part.setContentTypeProperty(contentType);
        }
        
        public Date getCreated() {
            return this.part.getCreatedProperty().getValue();
        }
        
        public void setCreated(final Nullable<Date> date) {
            this.part.setCreatedProperty(date);
        }
        
        public void setCreated(final String date) {
            this.part.setCreatedProperty(date);
        }
        
        public String getCreator() {
            return this.part.getCreatorProperty().getValue();
        }
        
        public void setCreator(final String creator) {
            this.part.setCreatorProperty(creator);
        }
        
        public String getDescription() {
            return this.part.getDescriptionProperty().getValue();
        }
        
        public void setDescription(final String description) {
            this.part.setDescriptionProperty(description);
        }
        
        public String getIdentifier() {
            return this.part.getIdentifierProperty().getValue();
        }
        
        public void setIdentifier(final String identifier) {
            this.part.setIdentifierProperty(identifier);
        }
        
        public String getKeywords() {
            return this.part.getKeywordsProperty().getValue();
        }
        
        public void setKeywords(final String keywords) {
            this.part.setKeywordsProperty(keywords);
        }
        
        public Date getLastPrinted() {
            return this.part.getLastPrintedProperty().getValue();
        }
        
        public void setLastPrinted(final Nullable<Date> date) {
            this.part.setLastPrintedProperty(date);
        }
        
        public void setLastPrinted(final String date) {
            this.part.setLastPrintedProperty(date);
        }
        
        public Date getModified() {
            return this.part.getModifiedProperty().getValue();
        }
        
        public void setModified(final Nullable<Date> date) {
            this.part.setModifiedProperty(date);
        }
        
        public void setModified(final String date) {
            this.part.setModifiedProperty(date);
        }
        
        public String getSubject() {
            return this.part.getSubjectProperty().getValue();
        }
        
        public void setSubjectProperty(final String subject) {
            this.part.setSubjectProperty(subject);
        }
        
        public void setTitle(final String title) {
            this.part.setTitleProperty(title);
        }
        
        public String getTitle() {
            return this.part.getTitleProperty().getValue();
        }
        
        public String getRevision() {
            return this.part.getRevisionProperty().getValue();
        }
        
        public void setRevision(final String revision) {
            try {
                Long.valueOf(revision);
                this.part.setRevisionProperty(revision);
            }
            catch (NumberFormatException ex) {}
        }
        
        public PackagePropertiesPart getUnderlyingProperties() {
            return this.part;
        }
    }
    
    public class ExtendedProperties
    {
        private PropertiesDocument props;
        
        private ExtendedProperties(final PropertiesDocument props) {
            this.props = props;
        }
        
        public CTProperties getUnderlyingProperties() {
            return this.props.getProperties();
        }
    }
    
    public class CustomProperties
    {
        public static final String FORMAT_ID = "{D5CDD505-2E9C-101B-9397-08002B2CF9AE}";
        private org.openxmlformats.schemas.officeDocument.x2006.customProperties.PropertiesDocument props;
        
        private CustomProperties(final org.openxmlformats.schemas.officeDocument.x2006.customProperties.PropertiesDocument props) {
            this.props = props;
        }
        
        public org.openxmlformats.schemas.officeDocument.x2006.customProperties.CTProperties getUnderlyingProperties() {
            return this.props.getProperties();
        }
        
        private CTProperty add(final String name) {
            if (this.contains(name)) {
                throw new IllegalArgumentException("A property with this name already exists in the custom properties");
            }
            final CTProperty p = this.props.getProperties().addNewProperty();
            final int pid = this.nextPid();
            p.setPid(pid);
            p.setFmtid("{D5CDD505-2E9C-101B-9397-08002B2CF9AE}");
            p.setName(name);
            return p;
        }
        
        public void addProperty(final String name, final String value) {
            final CTProperty p = this.add(name);
            p.setLpwstr(value);
        }
        
        public void addProperty(final String name, final double value) {
            final CTProperty p = this.add(name);
            p.setR8(value);
        }
        
        public void addProperty(final String name, final int value) {
            final CTProperty p = this.add(name);
            p.setI4(value);
        }
        
        public void addProperty(final String name, final boolean value) {
            final CTProperty p = this.add(name);
            p.setBool(value);
        }
        
        protected int nextPid() {
            int propid = 1;
            for (final CTProperty p : this.props.getProperties().getPropertyList()) {
                if (p.getPid() > propid) {
                    propid = p.getPid();
                }
            }
            return propid + 1;
        }
        
        public boolean contains(final String name) {
            for (final CTProperty p : this.props.getProperties().getPropertyList()) {
                if (p.getName().equals(name)) {
                    return true;
                }
            }
            return false;
        }
    }
}
