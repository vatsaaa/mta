// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdfwriter;

import org.apache.pdfbox.util.ImageParameters;
import java.util.Iterator;
import org.apache.pdfbox.util.PDFOperator;
import org.apache.pdfbox.cos.COSBase;
import java.util.Map;
import org.apache.pdfbox.cos.COSDictionary;
import org.apache.pdfbox.cos.COSArray;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.cos.COSBoolean;
import org.apache.pdfbox.cos.COSInteger;
import org.apache.pdfbox.cos.COSFloat;
import org.apache.pdfbox.cos.COSString;
import java.io.IOException;
import java.util.List;
import java.io.OutputStream;

public class ContentStreamWriter
{
    private OutputStream output;
    public static final byte[] SPACE;
    public static final byte[] EOL;
    
    public ContentStreamWriter(final OutputStream out) {
        this.output = out;
    }
    
    public void writeTokens(final List tokens, final int start, final int end) throws IOException {
        for (int i = start; i < end; ++i) {
            final Object o = tokens.get(i);
            this.writeObject(o);
            this.output.write(32);
        }
        this.output.flush();
    }
    
    private void writeObject(final Object o) throws IOException {
        if (o instanceof COSString) {
            ((COSString)o).writePDF(this.output);
        }
        else if (o instanceof COSFloat) {
            ((COSFloat)o).writePDF(this.output);
        }
        else if (o instanceof COSInteger) {
            ((COSInteger)o).writePDF(this.output);
        }
        else if (o instanceof COSBoolean) {
            ((COSBoolean)o).writePDF(this.output);
        }
        else if (o instanceof COSName) {
            ((COSName)o).writePDF(this.output);
        }
        else if (o instanceof COSArray) {
            final COSArray array = (COSArray)o;
            this.output.write(COSWriter.ARRAY_OPEN);
            for (int i = 0; i < array.size(); ++i) {
                this.writeObject(array.get(i));
                this.output.write(ContentStreamWriter.SPACE);
            }
            this.output.write(COSWriter.ARRAY_CLOSE);
        }
        else if (o instanceof COSDictionary) {
            final COSDictionary obj = (COSDictionary)o;
            this.output.write(COSWriter.DICT_OPEN);
            for (final Map.Entry<COSName, COSBase> entry : obj.entrySet()) {
                if (entry.getValue() != null) {
                    this.writeObject(entry.getKey());
                    this.output.write(ContentStreamWriter.SPACE);
                    this.writeObject(entry.getValue());
                    this.output.write(ContentStreamWriter.SPACE);
                }
            }
            this.output.write(COSWriter.DICT_CLOSE);
            this.output.write(ContentStreamWriter.SPACE);
        }
        else {
            if (!(o instanceof PDFOperator)) {
                throw new IOException("Error:Unknown type in content stream:" + o);
            }
            final PDFOperator op = (PDFOperator)o;
            if (op.getOperation().equals("BI")) {
                this.output.write("BI".getBytes("ISO-8859-1"));
                final ImageParameters params = op.getImageParameters();
                final COSDictionary dic = params.getDictionary();
                for (final COSName key : dic.keySet()) {
                    final Object value = dic.getDictionaryObject(key);
                    key.writePDF(this.output);
                    this.output.write(ContentStreamWriter.SPACE);
                    this.writeObject(value);
                    this.output.write(ContentStreamWriter.EOL);
                }
                this.output.write("ID".getBytes("ISO-8859-1"));
                this.output.write(ContentStreamWriter.EOL);
                this.output.write(op.getImageData());
            }
            else {
                this.output.write(op.getOperation().getBytes("ISO-8859-1"));
                this.output.write(ContentStreamWriter.EOL);
            }
        }
    }
    
    public void writeTokens(final List tokens) throws IOException {
        this.writeTokens(tokens, 0, tokens.size());
    }
    
    static {
        SPACE = new byte[] { 32 };
        EOL = new byte[] { 10 };
    }
}
