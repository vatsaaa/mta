// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdfwriter;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.FilterInputStream;

public class COSFilterInputStream extends FilterInputStream
{
    int[] byteRange;
    long position;
    
    public COSFilterInputStream(final InputStream in, final int[] byteRange) {
        super(in);
        this.position = 0L;
        this.byteRange = byteRange;
    }
    
    public COSFilterInputStream(final byte[] in, final int[] byteRange) {
        super(new ByteArrayInputStream(in));
        this.position = 0L;
        this.byteRange = byteRange;
    }
    
    @Override
    public int read() throws IOException {
        this.nextAvailable();
        final int i = super.read();
        if (i > -1) {
            ++this.position;
        }
        return i;
    }
    
    @Override
    public int read(final byte[] b) throws IOException {
        return this.read(b, 0, b.length);
    }
    
    @Override
    public int read(final byte[] b, final int off, final int len) throws IOException {
        if (b == null) {
            throw new NullPointerException();
        }
        if (off < 0 || len < 0 || len > b.length - off) {
            throw new IndexOutOfBoundsException();
        }
        if (len == 0) {
            return 0;
        }
        int c = this.read();
        if (c == -1) {
            return -1;
        }
        b[off] = (byte)c;
        int i = 1;
        try {
            while (i < len) {
                c = this.read();
                if (c == -1) {
                    break;
                }
                b[off + i] = (byte)c;
                ++i;
            }
        }
        catch (IOException ex) {}
        return i;
    }
    
    private boolean inRange() throws IOException {
        final long pos = this.position;
        for (int i = 0; i < this.byteRange.length / 2; ++i) {
            if (this.byteRange[i * 2] <= pos && this.byteRange[i * 2] + this.byteRange[i * 2 + 1] > pos) {
                return true;
            }
        }
        return false;
    }
    
    private void nextAvailable() throws IOException {
        while (!this.inRange()) {
            ++this.position;
            if (super.read() < 0) {
                break;
            }
        }
    }
    
    public byte[] toByteArray() throws IOException {
        final ByteArrayOutputStream byteOS = new ByteArrayOutputStream();
        final byte[] buffer = new byte[1024];
        int c;
        while ((c = this.read(buffer)) != -1) {
            byteOS.write(buffer, 0, c);
        }
        return byteOS.toByteArray();
    }
}
