// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdfwriter;

import org.apache.pdfbox.util.StringUtil;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.IOException;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.FileDescriptor;
import java.nio.channels.FileChannel;
import java.io.FilterOutputStream;

public class COSStandardOutputStream extends FilterOutputStream
{
    public static final byte[] CRLF;
    public static final byte[] LF;
    public static final byte[] EOL;
    private long pos;
    private boolean onNewLine;
    private FileChannel fileChannel;
    private FileDescriptor fileDescriptor;
    private long mark;
    
    public COSStandardOutputStream(final OutputStream out) {
        super(out);
        this.pos = 0L;
        this.onNewLine = false;
        this.fileChannel = null;
        this.fileDescriptor = null;
        this.mark = -1L;
        if (out instanceof FileOutputStream) {
            try {
                this.fileChannel = ((FileOutputStream)out).getChannel();
                this.fileDescriptor = ((FileOutputStream)out).getFD();
                this.pos = this.fileChannel.position();
            }
            catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
    
    public long getPos() {
        return this.pos;
    }
    
    public void setPos(final long pos) throws IOException {
        if (this.fileChannel != null) {
            this.checkPos();
            this.pos = pos;
            this.fileChannel.position(pos);
        }
    }
    
    public boolean isOnNewLine() {
        return this.onNewLine;
    }
    
    public void setOnNewLine(final boolean newOnNewLine) {
        this.onNewLine = newOnNewLine;
    }
    
    @Override
    public void write(final byte[] b, final int off, final int len) throws IOException {
        this.checkPos();
        this.setOnNewLine(false);
        this.out.write(b, off, len);
        this.pos += len;
    }
    
    @Override
    public void write(final int b) throws IOException {
        this.checkPos();
        this.setOnNewLine(false);
        this.out.write(b);
        ++this.pos;
    }
    
    public void writeCRLF() throws IOException {
        this.write(COSStandardOutputStream.CRLF);
    }
    
    public void writeEOL() throws IOException {
        if (!this.isOnNewLine()) {
            this.write(COSStandardOutputStream.EOL);
            this.setOnNewLine(true);
        }
    }
    
    public void writeLF() throws IOException {
        this.write(COSStandardOutputStream.LF);
    }
    
    public void mark() throws IOException {
        this.checkPos();
        this.mark = this.getPos();
    }
    
    public void reset() throws IOException {
        if (this.mark < 0L) {
            return;
        }
        this.setPos(this.mark);
    }
    
    private void checkPos() throws IOException {
        if (this.fileChannel != null && this.fileChannel.position() != this.getPos()) {
            throw new IOException("OutputStream has an invalid position");
        }
    }
    
    public byte[] getFileInBytes(final int[] byteRange) throws IOException {
        return null;
    }
    
    public InputStream getFilterInputStream(final int[] byteRange) {
        return new COSFilterInputStream(new FileInputStream(this.fileDescriptor), byteRange);
    }
    
    static {
        CRLF = StringUtil.getBytes("\r\n");
        LF = StringUtil.getBytes("\n");
        EOL = StringUtil.getBytes("\n");
    }
}
