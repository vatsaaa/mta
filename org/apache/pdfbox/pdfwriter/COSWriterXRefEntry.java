// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdfwriter;

import org.apache.pdfbox.persistence.util.COSObjectKey;
import org.apache.pdfbox.cos.COSBase;

public class COSWriterXRefEntry implements Comparable<COSWriterXRefEntry>
{
    private long offset;
    private COSBase object;
    private COSObjectKey key;
    private boolean free;
    private static COSWriterXRefEntry nullEntry;
    
    public int compareTo(final COSWriterXRefEntry obj) {
        if (obj instanceof COSWriterXRefEntry) {
            return (int)(this.getKey().getNumber() - obj.getKey().getNumber());
        }
        return -1;
    }
    
    public static COSWriterXRefEntry getNullEntry() {
        if (COSWriterXRefEntry.nullEntry == null) {
            (COSWriterXRefEntry.nullEntry = new COSWriterXRefEntry(0L, null, new COSObjectKey(0L, 65535L))).setFree(true);
        }
        return COSWriterXRefEntry.nullEntry;
    }
    
    public COSObjectKey getKey() {
        return this.key;
    }
    
    public long getOffset() {
        return this.offset;
    }
    
    public boolean isFree() {
        return this.free;
    }
    
    public void setFree(final boolean newFree) {
        this.free = newFree;
    }
    
    private void setKey(final COSObjectKey newKey) {
        this.key = newKey;
    }
    
    public void setOffset(final long newOffset) {
        this.offset = newOffset;
    }
    
    public COSWriterXRefEntry(final long start, final COSBase obj, final COSObjectKey keyValue) {
        this.free = false;
        this.setOffset(start);
        this.setObject(obj);
        this.setKey(keyValue);
    }
    
    public COSBase getObject() {
        return this.object;
    }
    
    private void setObject(final COSBase newObject) {
        this.object = newObject;
    }
}
