// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdfwriter;

import org.apache.pdfbox.util.StringUtil;
import org.apache.pdfbox.pdmodel.encryption.SecurityHandler;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.security.MessageDigest;
import org.apache.pdfbox.exceptions.CryptographyException;
import org.apache.pdfbox.cos.COSStream;
import org.apache.pdfbox.cos.COSInteger;
import org.apache.pdfbox.cos.COSFloat;
import org.apache.pdfbox.cos.COSBoolean;
import org.apache.pdfbox.cos.COSNull;
import org.apache.pdfbox.cos.COSArray;
import org.apache.pdfbox.exceptions.SignatureException;
import org.apache.pdfbox.pdmodel.interactive.digitalsignature.SignatureInterface;
import org.apache.pdfbox.cos.COSString;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.Collections;
import org.apache.pdfbox.cos.COSObject;
import org.apache.pdfbox.exceptions.COSVisitorException;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.cos.COSDictionary;
import java.util.Iterator;
import org.apache.pdfbox.cos.COSDocument;
import java.io.IOException;
import org.apache.pdfbox.cos.COSNumber;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Locale;
import java.text.DecimalFormat;
import java.io.FileInputStream;
import org.apache.pdfbox.pdmodel.PDDocument;
import java.util.Set;
import java.util.LinkedList;
import java.util.HashSet;
import java.util.List;
import org.apache.pdfbox.persistence.util.COSObjectKey;
import org.apache.pdfbox.cos.COSBase;
import java.util.Map;
import java.io.OutputStream;
import java.text.NumberFormat;
import org.apache.pdfbox.cos.ICOSVisitor;

public class COSWriter implements ICOSVisitor
{
    public static final byte[] DICT_OPEN;
    public static final byte[] DICT_CLOSE;
    public static final byte[] SPACE;
    public static final byte[] COMMENT;
    public static final byte[] VERSION;
    public static final byte[] GARBAGE;
    public static final byte[] EOF;
    public static final byte[] REFERENCE;
    public static final byte[] XREF;
    public static final byte[] XREF_FREE;
    public static final byte[] XREF_USED;
    public static final byte[] TRAILER;
    public static final byte[] STARTXREF;
    public static final byte[] OBJ;
    public static final byte[] ENDOBJ;
    public static final byte[] ARRAY_OPEN;
    public static final byte[] ARRAY_CLOSE;
    public static final byte[] STREAM;
    public static final byte[] ENDSTREAM;
    private NumberFormat formatXrefOffset;
    private NumberFormat formatXrefGeneration;
    private NumberFormat formatDecimal;
    private OutputStream output;
    private COSStandardOutputStream standardOutput;
    private long startxref;
    private long number;
    private Map<COSBase, COSObjectKey> objectKeys;
    private Map<COSObjectKey, COSBase> keyObject;
    private List<COSWriterXRefEntry> xRefEntries;
    private HashSet<COSBase> objectsToWriteSet;
    private LinkedList<COSBase> objectsToWrite;
    private Set<COSBase> writtenObjects;
    private Set<COSBase> actualsAdded;
    private COSObjectKey currentObjectKey;
    private PDDocument document;
    private boolean willEncrypt;
    private boolean incrementalUpdate;
    private boolean reachedSignature;
    private int[] signaturePosition;
    private int[] byterangePosition;
    private FileInputStream in;
    
    public COSWriter(final OutputStream os) {
        this.formatXrefOffset = new DecimalFormat("0000000000");
        this.formatXrefGeneration = new DecimalFormat("00000");
        this.formatDecimal = NumberFormat.getNumberInstance(Locale.US);
        this.startxref = 0L;
        this.number = 0L;
        this.objectKeys = new Hashtable<COSBase, COSObjectKey>();
        this.keyObject = new Hashtable<COSObjectKey, COSBase>();
        this.xRefEntries = new ArrayList<COSWriterXRefEntry>();
        this.objectsToWriteSet = new HashSet<COSBase>();
        this.objectsToWrite = new LinkedList<COSBase>();
        this.writtenObjects = new HashSet<COSBase>();
        this.actualsAdded = new HashSet<COSBase>();
        this.currentObjectKey = null;
        this.document = null;
        this.willEncrypt = false;
        this.incrementalUpdate = false;
        this.reachedSignature = false;
        this.signaturePosition = new int[2];
        this.byterangePosition = new int[2];
        this.setOutput(os);
        this.setStandardOutput(new COSStandardOutputStream(this.output));
        this.formatDecimal.setMaximumFractionDigits(10);
        this.formatDecimal.setGroupingUsed(false);
    }
    
    public COSWriter(final OutputStream os, final FileInputStream is) {
        this(os);
        this.in = is;
        this.incrementalUpdate = true;
    }
    
    private void prepareIncrement(final PDDocument doc) {
        try {
            if (doc != null) {
                final COSDocument cosDoc = doc.getDocument();
                final Map<COSObjectKey, Long> xrefTable = cosDoc.getXrefTable();
                final Set<COSObjectKey> keySet = xrefTable.keySet();
                long highestNumber = 0L;
                for (final COSObjectKey cosObjectKey : keySet) {
                    final COSBase object = cosDoc.getObjectFromPool(cosObjectKey).getObject();
                    if (object != null && cosObjectKey != null && !(object instanceof COSNumber)) {
                        this.objectKeys.put(object, cosObjectKey);
                        this.keyObject.put(cosObjectKey, object);
                    }
                    final long num = cosObjectKey.getNumber();
                    if (num > highestNumber) {
                        highestNumber = num;
                    }
                }
                this.setNumber(highestNumber);
            }
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    protected void addXRefEntry(final COSWriterXRefEntry entry) {
        this.getXRefEntries().add(entry);
    }
    
    public void close() throws IOException {
        if (this.getStandardOutput() != null) {
            this.getStandardOutput().close();
        }
        if (this.getOutput() != null) {
            this.getOutput().close();
        }
    }
    
    protected long getNumber() {
        return this.number;
    }
    
    public Map<COSBase, COSObjectKey> getObjectKeys() {
        return this.objectKeys;
    }
    
    protected OutputStream getOutput() {
        return this.output;
    }
    
    protected COSStandardOutputStream getStandardOutput() {
        return this.standardOutput;
    }
    
    protected long getStartxref() {
        return this.startxref;
    }
    
    protected List<COSWriterXRefEntry> getXRefEntries() {
        return this.xRefEntries;
    }
    
    protected void setNumber(final long newNumber) {
        this.number = newNumber;
    }
    
    private void setOutput(final OutputStream newOutput) {
        this.output = newOutput;
    }
    
    private void setStandardOutput(final COSStandardOutputStream newStandardOutput) {
        this.standardOutput = newStandardOutput;
    }
    
    protected void setStartxref(final long newStartxref) {
        this.startxref = newStartxref;
    }
    
    protected void doWriteBody(final COSDocument doc) throws IOException, COSVisitorException {
        final COSDictionary trailer = doc.getTrailer();
        final COSDictionary root = (COSDictionary)trailer.getDictionaryObject(COSName.ROOT);
        final COSDictionary info = (COSDictionary)trailer.getDictionaryObject(COSName.INFO);
        final COSDictionary encrypt = (COSDictionary)trailer.getDictionaryObject(COSName.ENCRYPT);
        if (root != null) {
            this.addObjectToWrite(root);
        }
        if (info != null) {
            this.addObjectToWrite(info);
        }
        while (this.objectsToWrite.size() > 0) {
            final COSBase nextObject = this.objectsToWrite.removeFirst();
            this.objectsToWriteSet.remove(nextObject);
            this.doWriteObject(nextObject);
        }
        this.willEncrypt = false;
        if (encrypt != null) {
            this.addObjectToWrite(encrypt);
        }
        while (this.objectsToWrite.size() > 0) {
            final COSBase nextObject = this.objectsToWrite.removeFirst();
            this.objectsToWriteSet.remove(nextObject);
            this.doWriteObject(nextObject);
        }
    }
    
    private void addObjectToWrite(final COSBase object) {
        COSBase actual = object;
        if (actual instanceof COSObject) {
            actual = ((COSObject)actual).getObject();
        }
        if (!this.writtenObjects.contains(object) && !this.objectsToWriteSet.contains(object) && !this.actualsAdded.contains(actual)) {
            COSBase cosBase = null;
            COSObjectKey cosObjectKey = null;
            if (actual != null) {
                cosObjectKey = this.objectKeys.get(actual);
            }
            if (cosObjectKey != null) {
                cosBase = this.keyObject.get(cosObjectKey);
            }
            if (actual != null && this.objectKeys.containsKey(actual) && !object.isNeedToBeUpdate() && cosBase != null && !cosBase.isNeedToBeUpdate()) {
                return;
            }
            this.objectsToWrite.add(object);
            this.objectsToWriteSet.add(object);
            if (actual != null) {
                this.actualsAdded.add(actual);
            }
        }
    }
    
    public void doWriteObject(final COSBase obj) throws COSVisitorException {
        try {
            this.writtenObjects.add(obj);
            if (obj instanceof COSDictionary) {
                final COSDictionary dict = (COSDictionary)obj;
                final COSName item = (COSName)dict.getItem(COSName.TYPE);
                if (COSName.SIG.equals(item)) {
                    this.reachedSignature = true;
                }
            }
            this.currentObjectKey = this.getObjectKey(obj);
            this.addXRefEntry(new COSWriterXRefEntry(this.getStandardOutput().getPos(), obj, this.currentObjectKey));
            this.getStandardOutput().write(String.valueOf(this.currentObjectKey.getNumber()).getBytes("ISO-8859-1"));
            this.getStandardOutput().write(COSWriter.SPACE);
            this.getStandardOutput().write(String.valueOf(this.currentObjectKey.getGeneration()).getBytes("ISO-8859-1"));
            this.getStandardOutput().write(COSWriter.SPACE);
            this.getStandardOutput().write(COSWriter.OBJ);
            this.getStandardOutput().writeEOL();
            obj.accept(this);
            this.getStandardOutput().writeEOL();
            this.getStandardOutput().write(COSWriter.ENDOBJ);
            this.getStandardOutput().writeEOL();
        }
        catch (IOException e) {
            throw new COSVisitorException(e);
        }
    }
    
    protected void doWriteHeader(final COSDocument doc) throws IOException {
        this.getStandardOutput().write(doc.getHeaderString().getBytes("ISO-8859-1"));
        this.getStandardOutput().writeEOL();
        this.getStandardOutput().write(COSWriter.COMMENT);
        this.getStandardOutput().write(COSWriter.GARBAGE);
        this.getStandardOutput().writeEOL();
    }
    
    protected void doWriteTrailer(final COSDocument doc) throws IOException, COSVisitorException {
        this.getStandardOutput().write(COSWriter.TRAILER);
        this.getStandardOutput().writeEOL();
        final COSDictionary trailer = doc.getTrailer();
        Collections.sort(this.getXRefEntries());
        final COSWriterXRefEntry lastEntry = this.getXRefEntries().get(this.getXRefEntries().size() - 1);
        trailer.setInt(COSName.SIZE, (int)lastEntry.getKey().getNumber() + 1);
        if (!this.incrementalUpdate) {
            trailer.removeItem(COSName.PREV);
        }
        trailer.removeItem(COSName.DOC_CHECKSUM);
        trailer.accept(this);
        this.getStandardOutput().write(COSWriter.STARTXREF);
        this.getStandardOutput().writeEOL();
        this.getStandardOutput().write(String.valueOf(this.getStartxref()).getBytes("ISO-8859-1"));
        this.getStandardOutput().writeEOL();
        this.getStandardOutput().write(COSWriter.EOF);
        this.getStandardOutput().writeEOL();
    }
    
    protected void doWriteXRef(final COSDocument doc) throws IOException {
        Collections.sort(this.getXRefEntries());
        final COSWriterXRefEntry lastEntry = this.getXRefEntries().get(this.getXRefEntries().size() - 1);
        this.setStartxref(this.getStandardOutput().getPos());
        this.getStandardOutput().write(COSWriter.XREF);
        this.getStandardOutput().writeEOL();
        this.writeXrefRange(0L, lastEntry.getKey().getNumber() + 1L);
        this.writeXrefEntry(COSWriterXRefEntry.getNullEntry());
        long lastObjectNumber = 0L;
        for (final COSWriterXRefEntry entry : this.getXRefEntries()) {
            while (lastObjectNumber < entry.getKey().getNumber() - 1L) {
                this.writeXrefEntry(COSWriterXRefEntry.getNullEntry());
            }
            lastObjectNumber = entry.getKey().getNumber();
            this.writeXrefEntry(entry);
        }
    }
    
    private void doWriteXRefInc(final COSDocument doc) throws IOException {
        final COSDictionary trailer = doc.getTrailer();
        trailer.setLong(COSName.PREV, doc.getStartXref());
        this.addXRefEntry(COSWriterXRefEntry.getNullEntry());
        Collections.sort(this.getXRefEntries());
        this.setStartxref(this.getStandardOutput().getPos());
        this.getStandardOutput().write(COSWriter.XREF);
        this.getStandardOutput().writeEOL();
        final Integer[] xRefRanges = this.getXRefRanges(this.getXRefEntries());
        final int xRefLength = xRefRanges.length;
        int x = 0;
        int j = 0;
        while (x < xRefLength && xRefLength % 2 == 0) {
            this.writeXrefRange(xRefRanges[x], xRefRanges[x + 1]);
            for (int i = 0; i < xRefRanges[x + 1]; ++i) {
                this.writeXrefEntry(this.xRefEntries.get(j++));
            }
            x += 2;
        }
    }
    
    private void doWriteSignature(final COSDocument doc) throws IOException, SignatureException {
        if (this.signaturePosition[0] > 0 && this.byterangePosition[1] > 0) {
            final int left = (int)this.getStandardOutput().getPos() - this.signaturePosition[1];
            final String newByteRange = "0 " + this.signaturePosition[0] + " " + this.signaturePosition[1] + " " + left + "]";
            final int leftByterange = this.byterangePosition[1] - this.byterangePosition[0] - newByteRange.length();
            if (leftByterange < 0) {
                throw new IOException("Can't write new ByteRange, not enough space");
            }
            this.getStandardOutput().setPos(this.byterangePosition[0]);
            this.getStandardOutput().write(newByteRange.getBytes());
            for (int i = 0; i < leftByterange; ++i) {
                this.getStandardOutput().write(32);
            }
            this.getStandardOutput().setPos(0L);
            final InputStream filterInputStream = new COSFilterInputStream(this.in, new int[] { 0, this.signaturePosition[0], this.signaturePosition[1], left });
            final ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            try {
                final byte[] buffer = new byte[1024];
                int c;
                while ((c = filterInputStream.read(buffer)) != -1) {
                    bytes.write(buffer, 0, c);
                }
            }
            finally {
                if (filterInputStream != null) {
                    filterInputStream.close();
                }
            }
            final byte[] pdfContent = bytes.toByteArray();
            final SignatureInterface signatureInterface = doc.getSignatureInterface();
            final byte[] sign = signatureInterface.sign(new ByteArrayInputStream(pdfContent));
            final String signature = new COSString(sign).getHexString();
            final int leftSignaturerange = this.signaturePosition[1] - this.signaturePosition[0] - signature.length();
            if (leftSignaturerange < 0) {
                throw new IOException("Can't write signature, not enough space");
            }
            this.getStandardOutput().setPos(this.signaturePosition[0] + 1);
            this.getStandardOutput().write(signature.getBytes());
        }
    }
    
    private void writeXrefRange(final long x, final long y) throws IOException {
        this.getStandardOutput().write(String.valueOf(x).getBytes());
        this.getStandardOutput().write(COSWriter.SPACE);
        this.getStandardOutput().write(String.valueOf(y).getBytes());
        this.getStandardOutput().writeEOL();
    }
    
    private void writeXrefEntry(final COSWriterXRefEntry entry) throws IOException {
        final String offset = this.formatXrefOffset.format(entry.getOffset());
        final String generation = this.formatXrefGeneration.format(entry.getKey().getGeneration());
        this.getStandardOutput().write(offset.getBytes("ISO-8859-1"));
        this.getStandardOutput().write(COSWriter.SPACE);
        this.getStandardOutput().write(generation.getBytes("ISO-8859-1"));
        this.getStandardOutput().write(COSWriter.SPACE);
        this.getStandardOutput().write(entry.isFree() ? COSWriter.XREF_FREE : COSWriter.XREF_USED);
        this.getStandardOutput().writeCRLF();
    }
    
    protected Integer[] getXRefRanges(final List<COSWriterXRefEntry> xRefEntriesList) {
        int nr = 0;
        int last = -2;
        int count = 1;
        final ArrayList<Integer> list = new ArrayList<Integer>();
        for (final Object object : xRefEntriesList) {
            nr = (int)((COSWriterXRefEntry)object).getKey().getNumber();
            if (nr == last + 1) {
                ++count;
                last = nr;
            }
            else if (last == -2) {
                last = nr;
            }
            else {
                list.add(last - count + 1);
                list.add(count);
                last = nr;
                count = 1;
            }
        }
        if (xRefEntriesList.size() > 0) {
            list.add(last - count + 1);
            list.add(count);
        }
        return list.toArray(new Integer[list.size()]);
    }
    
    private COSObjectKey getObjectKey(final COSBase obj) {
        COSBase actual = obj;
        if (actual instanceof COSObject) {
            actual = ((COSObject)obj).getObject();
        }
        COSObjectKey key = null;
        if (actual != null) {
            key = this.objectKeys.get(actual);
        }
        if (key == null) {
            key = this.objectKeys.get(obj);
        }
        if (key == null) {
            this.setNumber(this.getNumber() + 1L);
            key = new COSObjectKey(this.getNumber(), 0L);
            this.objectKeys.put(obj, key);
            if (actual != null) {
                this.objectKeys.put(actual, key);
            }
        }
        return key;
    }
    
    public Object visitFromArray(final COSArray obj) throws COSVisitorException {
        try {
            int count = 0;
            this.getStandardOutput().write(COSWriter.ARRAY_OPEN);
            final Iterator<COSBase> i = obj.iterator();
            while (i.hasNext()) {
                final COSBase current = i.next();
                if (current instanceof COSDictionary) {
                    this.addObjectToWrite(current);
                    this.writeReference(current);
                }
                else if (current instanceof COSObject) {
                    final COSBase subValue = ((COSObject)current).getObject();
                    if (subValue instanceof COSDictionary || subValue == null) {
                        this.addObjectToWrite(current);
                        this.writeReference(current);
                    }
                    else {
                        subValue.accept(this);
                    }
                }
                else if (current == null) {
                    COSNull.NULL.accept(this);
                }
                else if (current instanceof COSString) {
                    final COSString copy = new COSString(((COSString)current).getString());
                    copy.accept(this);
                }
                else {
                    current.accept(this);
                }
                ++count;
                if (i.hasNext()) {
                    if (count % 10 == 0) {
                        this.getStandardOutput().writeEOL();
                    }
                    else {
                        this.getStandardOutput().write(COSWriter.SPACE);
                    }
                }
            }
            this.getStandardOutput().write(COSWriter.ARRAY_CLOSE);
            this.getStandardOutput().writeEOL();
            return null;
        }
        catch (IOException e) {
            throw new COSVisitorException(e);
        }
    }
    
    public Object visitFromBoolean(final COSBoolean obj) throws COSVisitorException {
        try {
            obj.writePDF(this.getStandardOutput());
            return null;
        }
        catch (IOException e) {
            throw new COSVisitorException(e);
        }
    }
    
    public Object visitFromDictionary(final COSDictionary obj) throws COSVisitorException {
        try {
            this.getStandardOutput().write(COSWriter.DICT_OPEN);
            this.getStandardOutput().writeEOL();
            for (final Map.Entry<COSName, COSBase> entry : obj.entrySet()) {
                final COSBase value = entry.getValue();
                if (value != null) {
                    entry.getKey().accept(this);
                    this.getStandardOutput().write(COSWriter.SPACE);
                    if (value instanceof COSDictionary) {
                        final COSDictionary dict = (COSDictionary)value;
                        COSBase item = dict.getItem(COSName.XOBJECT);
                        if (item != null) {
                            item.setDirect(true);
                        }
                        item = dict.getItem(COSName.RESOURCES);
                        if (item != null) {
                            item.setDirect(true);
                        }
                        if (dict.isDirect()) {
                            this.visitFromDictionary(dict);
                        }
                        else {
                            this.addObjectToWrite(dict);
                            this.writeReference(dict);
                        }
                    }
                    else if (value instanceof COSObject) {
                        final COSBase subValue = ((COSObject)value).getObject();
                        if (subValue instanceof COSDictionary || subValue == null) {
                            this.addObjectToWrite(value);
                            this.writeReference(value);
                        }
                        else {
                            subValue.accept(this);
                        }
                    }
                    else if (this.reachedSignature && COSName.CONTENTS.equals(entry.getKey())) {
                        (this.signaturePosition = new int[2])[0] = (int)this.getStandardOutput().getPos();
                        value.accept(this);
                        this.signaturePosition[1] = (int)this.getStandardOutput().getPos();
                    }
                    else if (this.reachedSignature && COSName.BYTERANGE.equals(entry.getKey())) {
                        (this.byterangePosition = new int[2])[0] = (int)this.getStandardOutput().getPos() + 1;
                        value.accept(this);
                        this.byterangePosition[1] = (int)this.getStandardOutput().getPos() - 1;
                        this.reachedSignature = false;
                    }
                    else {
                        value.accept(this);
                    }
                    this.getStandardOutput().writeEOL();
                }
            }
            this.getStandardOutput().write(COSWriter.DICT_CLOSE);
            this.getStandardOutput().writeEOL();
            return null;
        }
        catch (IOException e) {
            throw new COSVisitorException(e);
        }
    }
    
    public Object visitFromDocument(final COSDocument doc) throws COSVisitorException {
        try {
            if (!this.incrementalUpdate) {
                this.doWriteHeader(doc);
            }
            this.doWriteBody(doc);
            if (this.incrementalUpdate) {
                this.doWriteXRefInc(doc);
            }
            else {
                this.doWriteXRef(doc);
            }
            this.doWriteTrailer(doc);
            if (this.incrementalUpdate) {
                this.doWriteSignature(doc);
            }
            return null;
        }
        catch (IOException e) {
            throw new COSVisitorException(e);
        }
        catch (SignatureException e2) {
            throw new COSVisitorException(e2);
        }
    }
    
    public Object visitFromFloat(final COSFloat obj) throws COSVisitorException {
        try {
            obj.writePDF(this.getStandardOutput());
            return null;
        }
        catch (IOException e) {
            throw new COSVisitorException(e);
        }
    }
    
    public Object visitFromInt(final COSInteger obj) throws COSVisitorException {
        try {
            obj.writePDF(this.getStandardOutput());
            return null;
        }
        catch (IOException e) {
            throw new COSVisitorException(e);
        }
    }
    
    public Object visitFromName(final COSName obj) throws COSVisitorException {
        try {
            obj.writePDF(this.getStandardOutput());
            return null;
        }
        catch (IOException e) {
            throw new COSVisitorException(e);
        }
    }
    
    public Object visitFromNull(final COSNull obj) throws COSVisitorException {
        try {
            obj.writePDF(this.getStandardOutput());
            return null;
        }
        catch (IOException e) {
            throw new COSVisitorException(e);
        }
    }
    
    public void writeReference(final COSBase obj) throws COSVisitorException {
        try {
            final COSObjectKey key = this.getObjectKey(obj);
            this.getStandardOutput().write(String.valueOf(key.getNumber()).getBytes("ISO-8859-1"));
            this.getStandardOutput().write(COSWriter.SPACE);
            this.getStandardOutput().write(String.valueOf(key.getGeneration()).getBytes("ISO-8859-1"));
            this.getStandardOutput().write(COSWriter.SPACE);
            this.getStandardOutput().write(COSWriter.REFERENCE);
        }
        catch (IOException e) {
            throw new COSVisitorException(e);
        }
    }
    
    public Object visitFromStream(final COSStream obj) throws COSVisitorException {
        InputStream input = null;
        try {
            if (this.willEncrypt) {
                this.document.getSecurityHandler().encryptStream(obj, this.currentObjectKey.getNumber(), this.currentObjectKey.getGeneration());
            }
            input = obj.getFilteredStream();
            final COSObject lengthObject = new COSObject(null);
            obj.setItem(COSName.LENGTH, lengthObject);
            this.visitFromDictionary(obj);
            this.getStandardOutput().write(COSWriter.STREAM);
            this.getStandardOutput().writeCRLF();
            final byte[] buffer = new byte[1024];
            int amountRead = 0;
            int totalAmountWritten = 0;
            while ((amountRead = input.read(buffer, 0, 1024)) != -1) {
                this.getStandardOutput().write(buffer, 0, amountRead);
                totalAmountWritten += amountRead;
            }
            lengthObject.setObject(COSInteger.get(totalAmountWritten));
            this.getStandardOutput().writeCRLF();
            this.getStandardOutput().write(COSWriter.ENDSTREAM);
            this.getStandardOutput().writeEOL();
            return null;
        }
        catch (Exception e) {
            throw new COSVisitorException(e);
        }
        finally {
            if (input != null) {
                try {
                    input.close();
                }
                catch (IOException e2) {
                    throw new COSVisitorException(e2);
                }
            }
        }
    }
    
    public Object visitFromString(final COSString obj) throws COSVisitorException {
        try {
            if (this.willEncrypt) {
                this.document.getSecurityHandler().decryptString(obj, this.currentObjectKey.getNumber(), this.currentObjectKey.getGeneration());
            }
            obj.writePDF(this.getStandardOutput());
        }
        catch (Exception e) {
            throw new COSVisitorException(e);
        }
        return null;
    }
    
    public void write(final COSDocument doc) throws COSVisitorException {
        final PDDocument pdDoc = new PDDocument(doc);
        this.write(pdDoc);
    }
    
    public void write(final PDDocument doc) throws COSVisitorException {
        this.document = doc;
        if (this.incrementalUpdate) {
            this.prepareIncrement(doc);
        }
        Label_0102: {
            if (doc.isAllSecurityToBeRemoved()) {
                this.willEncrypt = false;
                final COSDocument cosDoc = doc.getDocument();
                final COSDictionary trailer = cosDoc.getTrailer();
                trailer.removeItem(COSName.ENCRYPT);
            }
            else {
                final SecurityHandler securityHandler = this.document.getSecurityHandler();
                if (securityHandler != null) {
                    try {
                        securityHandler.prepareDocumentForEncryption(this.document);
                        this.willEncrypt = true;
                        break Label_0102;
                    }
                    catch (IOException e) {
                        throw new COSVisitorException(e);
                    }
                    catch (CryptographyException e2) {
                        throw new COSVisitorException(e2);
                    }
                }
                this.willEncrypt = false;
            }
        }
        final COSDocument cosDoc = this.document.getDocument();
        final COSDictionary trailer = cosDoc.getTrailer();
        COSArray idArray = (COSArray)trailer.getDictionaryObject(COSName.ID);
        Label_0300: {
            if (idArray != null) {
                if (!this.incrementalUpdate) {
                    break Label_0300;
                }
            }
            try {
                final MessageDigest md = MessageDigest.getInstance("MD5");
                md.update(Long.toString(System.currentTimeMillis()).getBytes("ISO-8859-1"));
                final COSDictionary info = (COSDictionary)trailer.getDictionaryObject(COSName.INFO);
                if (info != null) {
                    final Iterator<COSBase> values = info.getValues().iterator();
                    while (values.hasNext()) {
                        md.update(values.next().toString().getBytes("ISO-8859-1"));
                    }
                }
                idArray = new COSArray();
                final COSString id = new COSString(md.digest());
                idArray.add(id);
                idArray.add(id);
                trailer.setItem(COSName.ID, idArray);
            }
            catch (NoSuchAlgorithmException e3) {
                throw new COSVisitorException(e3);
            }
            catch (UnsupportedEncodingException e4) {
                throw new COSVisitorException(e4);
            }
        }
        cosDoc.accept(this);
    }
    
    static {
        DICT_OPEN = StringUtil.getBytes("<<");
        DICT_CLOSE = StringUtil.getBytes(">>");
        SPACE = StringUtil.getBytes(" ");
        COMMENT = StringUtil.getBytes("%");
        VERSION = StringUtil.getBytes("PDF-1.4");
        GARBAGE = new byte[] { -10, -28, -4, -33 };
        EOF = StringUtil.getBytes("%%EOF");
        REFERENCE = StringUtil.getBytes("R");
        XREF = StringUtil.getBytes("xref");
        XREF_FREE = StringUtil.getBytes("f");
        XREF_USED = StringUtil.getBytes("n");
        TRAILER = StringUtil.getBytes("trailer");
        STARTXREF = StringUtil.getBytes("startxref");
        OBJ = StringUtil.getBytes("obj");
        ENDOBJ = StringUtil.getBytes("endobj");
        ARRAY_OPEN = StringUtil.getBytes("[");
        ARRAY_CLOSE = StringUtil.getBytes("]");
        STREAM = StringUtil.getBytes("stream");
        ENDSTREAM = StringUtil.getBytes("endstream");
    }
}
