// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.encoding;

import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.cos.COSBase;

public class StandardEncoding extends Encoding
{
    public static final StandardEncoding INSTANCE;
    
    public StandardEncoding() {
        this.addCharacterEncoding(65, "A");
        this.addCharacterEncoding(225, "AE");
        this.addCharacterEncoding(66, "B");
        this.addCharacterEncoding(67, "C");
        this.addCharacterEncoding(68, "D");
        this.addCharacterEncoding(69, "E");
        this.addCharacterEncoding(70, "F");
        this.addCharacterEncoding(71, "G");
        this.addCharacterEncoding(72, "H");
        this.addCharacterEncoding(73, "I");
        this.addCharacterEncoding(74, "J");
        this.addCharacterEncoding(75, "K");
        this.addCharacterEncoding(76, "L");
        this.addCharacterEncoding(232, "Lslash");
        this.addCharacterEncoding(77, "M");
        this.addCharacterEncoding(78, "N");
        this.addCharacterEncoding(79, "O");
        this.addCharacterEncoding(234, "OE");
        this.addCharacterEncoding(233, "Oslash");
        this.addCharacterEncoding(80, "P");
        this.addCharacterEncoding(81, "Q");
        this.addCharacterEncoding(82, "R");
        this.addCharacterEncoding(83, "S");
        this.addCharacterEncoding(84, "T");
        this.addCharacterEncoding(85, "U");
        this.addCharacterEncoding(86, "V");
        this.addCharacterEncoding(87, "W");
        this.addCharacterEncoding(88, "X");
        this.addCharacterEncoding(89, "Y");
        this.addCharacterEncoding(90, "Z");
        this.addCharacterEncoding(97, "a");
        this.addCharacterEncoding(194, "acute");
        this.addCharacterEncoding(241, "ae");
        this.addCharacterEncoding(38, "ampersand");
        this.addCharacterEncoding(94, "asciicircum");
        this.addCharacterEncoding(126, "asciitilde");
        this.addCharacterEncoding(42, "asterisk");
        this.addCharacterEncoding(64, "at");
        this.addCharacterEncoding(98, "b");
        this.addCharacterEncoding(92, "backslash");
        this.addCharacterEncoding(124, "bar");
        this.addCharacterEncoding(123, "braceleft");
        this.addCharacterEncoding(125, "braceright");
        this.addCharacterEncoding(91, "bracketleft");
        this.addCharacterEncoding(93, "bracketright");
        this.addCharacterEncoding(198, "breve");
        this.addCharacterEncoding(183, "bullet");
        this.addCharacterEncoding(99, "c");
        this.addCharacterEncoding(207, "caron");
        this.addCharacterEncoding(203, "cedilla");
        this.addCharacterEncoding(162, "cent");
        this.addCharacterEncoding(195, "circumflex");
        this.addCharacterEncoding(58, "colon");
        this.addCharacterEncoding(44, "comma");
        this.addCharacterEncoding(168, "currency");
        this.addCharacterEncoding(100, "d");
        this.addCharacterEncoding(178, "dagger");
        this.addCharacterEncoding(179, "daggerdbl");
        this.addCharacterEncoding(200, "dieresis");
        this.addCharacterEncoding(36, "dollar");
        this.addCharacterEncoding(199, "dotaccent");
        this.addCharacterEncoding(245, "dotlessi");
        this.addCharacterEncoding(101, "e");
        this.addCharacterEncoding(56, "eight");
        this.addCharacterEncoding(188, "ellipsis");
        this.addCharacterEncoding(208, "emdash");
        this.addCharacterEncoding(177, "endash");
        this.addCharacterEncoding(61, "equal");
        this.addCharacterEncoding(33, "exclam");
        this.addCharacterEncoding(161, "exclamdown");
        this.addCharacterEncoding(102, "f");
        this.addCharacterEncoding(174, "fi");
        this.addCharacterEncoding(53, "five");
        this.addCharacterEncoding(175, "fl");
        this.addCharacterEncoding(166, "florin");
        this.addCharacterEncoding(52, "four");
        this.addCharacterEncoding(164, "fraction");
        this.addCharacterEncoding(103, "g");
        this.addCharacterEncoding(251, "germandbls");
        this.addCharacterEncoding(193, "grave");
        this.addCharacterEncoding(62, "greater");
        this.addCharacterEncoding(171, "guillemotleft");
        this.addCharacterEncoding(187, "guillemotright");
        this.addCharacterEncoding(172, "guilsinglleft");
        this.addCharacterEncoding(173, "guilsinglright");
        this.addCharacterEncoding(104, "h");
        this.addCharacterEncoding(205, "hungarumlaut");
        this.addCharacterEncoding(45, "hyphen");
        this.addCharacterEncoding(105, "i");
        this.addCharacterEncoding(106, "j");
        this.addCharacterEncoding(107, "k");
        this.addCharacterEncoding(108, "l");
        this.addCharacterEncoding(60, "less");
        this.addCharacterEncoding(248, "lslash");
        this.addCharacterEncoding(109, "m");
        this.addCharacterEncoding(197, "macron");
        this.addCharacterEncoding(110, "n");
        this.addCharacterEncoding(57, "nine");
        this.addCharacterEncoding(35, "numbersign");
        this.addCharacterEncoding(111, "o");
        this.addCharacterEncoding(250, "oe");
        this.addCharacterEncoding(206, "ogonek");
        this.addCharacterEncoding(49, "one");
        this.addCharacterEncoding(227, "ordfeminine");
        this.addCharacterEncoding(235, "ordmasculine");
        this.addCharacterEncoding(249, "oslash");
        this.addCharacterEncoding(112, "p");
        this.addCharacterEncoding(182, "paragraph");
        this.addCharacterEncoding(40, "parenleft");
        this.addCharacterEncoding(41, "parenright");
        this.addCharacterEncoding(37, "percent");
        this.addCharacterEncoding(46, "period");
        this.addCharacterEncoding(180, "periodcentered");
        this.addCharacterEncoding(189, "perthousand");
        this.addCharacterEncoding(43, "plus");
        this.addCharacterEncoding(113, "q");
        this.addCharacterEncoding(63, "question");
        this.addCharacterEncoding(191, "questiondown");
        this.addCharacterEncoding(34, "quotedbl");
        this.addCharacterEncoding(185, "quotedblbase");
        this.addCharacterEncoding(170, "quotedblleft");
        this.addCharacterEncoding(186, "quotedblright");
        this.addCharacterEncoding(96, "quoteleft");
        this.addCharacterEncoding(39, "quoteright");
        this.addCharacterEncoding(184, "quotesinglbase");
        this.addCharacterEncoding(169, "quotesingle");
        this.addCharacterEncoding(114, "r");
        this.addCharacterEncoding(202, "ring");
        this.addCharacterEncoding(115, "s");
        this.addCharacterEncoding(167, "section");
        this.addCharacterEncoding(59, "semicolon");
        this.addCharacterEncoding(55, "seven");
        this.addCharacterEncoding(54, "six");
        this.addCharacterEncoding(47, "slash");
        this.addCharacterEncoding(32, "space");
        this.addCharacterEncoding(163, "sterling");
        this.addCharacterEncoding(116, "t");
        this.addCharacterEncoding(51, "three");
        this.addCharacterEncoding(196, "tilde");
        this.addCharacterEncoding(50, "two");
        this.addCharacterEncoding(117, "u");
        this.addCharacterEncoding(95, "underscore");
        this.addCharacterEncoding(118, "v");
        this.addCharacterEncoding(119, "w");
        this.addCharacterEncoding(120, "x");
        this.addCharacterEncoding(121, "y");
        this.addCharacterEncoding(165, "yen");
        this.addCharacterEncoding(122, "z");
        this.addCharacterEncoding(48, "zero");
    }
    
    public COSBase getCOSObject() {
        return COSName.STANDARD_ENCODING;
    }
    
    static {
        INSTANCE = new StandardEncoding();
    }
}
