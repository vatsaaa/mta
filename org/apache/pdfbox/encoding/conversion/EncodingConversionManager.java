// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.encoding.conversion;

import java.util.Iterator;
import java.util.HashMap;

public class EncodingConversionManager
{
    private static HashMap encodingMap;
    
    private EncodingConversionManager() {
    }
    
    public static final EncodingConverter getConverter(final String encoding) {
        return EncodingConversionManager.encodingMap.get(encoding);
    }
    
    static {
        EncodingConversionManager.encodingMap = new HashMap();
        final Iterator it = CJKEncodings.getEncodingIterator();
        while (it.hasNext()) {
            final String encodingName = it.next();
            EncodingConversionManager.encodingMap.put(encodingName, new CJKConverter(encodingName));
        }
    }
}
