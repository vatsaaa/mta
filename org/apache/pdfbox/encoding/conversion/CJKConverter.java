// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.encoding.conversion;

import org.apache.fontbox.cmap.CMap;
import java.io.UnsupportedEncodingException;

public class CJKConverter implements EncodingConverter
{
    private String encodingName;
    private String charsetName;
    
    public CJKConverter(final String encoding) {
        this.encodingName = null;
        this.charsetName = null;
        this.encodingName = encoding;
        this.charsetName = CJKEncodings.getCharset(encoding);
    }
    
    public String convertString(final String s) {
        if (s.length() == 1) {
            return s;
        }
        if (this.charsetName.equalsIgnoreCase("UTF-16BE")) {
            return s;
        }
        try {
            return new String(s.getBytes("UTF-16BE"), this.charsetName);
        }
        catch (UnsupportedEncodingException uee) {
            return s;
        }
    }
    
    public String convertBytes(final byte[] c, final int offset, final int length, final CMap cmap) {
        if (cmap != null) {
            try {
                if (cmap.isInCodeSpaceRanges(c, offset, length)) {
                    return new String(c, offset, length, this.charsetName);
                }
                return null;
            }
            catch (UnsupportedEncodingException uee) {
                return new String(c, offset, length);
            }
        }
        return null;
    }
}
