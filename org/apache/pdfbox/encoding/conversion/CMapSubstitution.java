// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.encoding.conversion;

import java.util.HashMap;

public class CMapSubstitution
{
    private static HashMap<String, String> cmapSubstitutions;
    
    private CMapSubstitution() {
    }
    
    public static String substituteCMap(final String cmapName) {
        if (CMapSubstitution.cmapSubstitutions.containsKey(cmapName)) {
            return CMapSubstitution.cmapSubstitutions.get(cmapName);
        }
        return cmapName;
    }
    
    static {
        (CMapSubstitution.cmapSubstitutions = new HashMap<String, String>()).put("Adobe-GB1-4", "Adobe-GB1-UCS2");
        CMapSubstitution.cmapSubstitutions.put("GBK-EUC-H", "GBK-EUC-UCS2");
        CMapSubstitution.cmapSubstitutions.put("GBK-EUC-V", "GBK-EUC-UCS2");
        CMapSubstitution.cmapSubstitutions.put("GBpc-EUC-H", "GBpc-EUC-UCS2C");
        CMapSubstitution.cmapSubstitutions.put("GBpc-EUC-V", "GBpc-EUC-UCS2C");
        CMapSubstitution.cmapSubstitutions.put("Adobe-CNS1-4", "Adobe-CNS1-UCS2");
        CMapSubstitution.cmapSubstitutions.put("B5pc-H", "B5pc-UCS2");
        CMapSubstitution.cmapSubstitutions.put("B5pc-V", "B5pc-UCS2");
        CMapSubstitution.cmapSubstitutions.put("ETen-B5-H", "ETen-B5-UCS2");
        CMapSubstitution.cmapSubstitutions.put("ETen-B5-V", "ETen-B5-UCS2");
        CMapSubstitution.cmapSubstitutions.put("ETenms-B5-H", "ETen-B5-UCS2");
        CMapSubstitution.cmapSubstitutions.put("ETenms-B5-V", "ETen-B5-UCS2");
        CMapSubstitution.cmapSubstitutions.put("90ms-RKSJ-H", "90ms-RKSJ-UCS2");
        CMapSubstitution.cmapSubstitutions.put("90ms-RKSJ-V", "90ms-RKSJ-UCS2");
        CMapSubstitution.cmapSubstitutions.put("90msp-RKSJ-H", "90ms-RKSJ-UCS2");
        CMapSubstitution.cmapSubstitutions.put("90msp-RKSJ-V", "90ms-RKSJ-UCS2");
        CMapSubstitution.cmapSubstitutions.put("90pv-RKSJ-H", "90pv-RKSJ-UCS2");
        CMapSubstitution.cmapSubstitutions.put("UniJIS-UCS2-HW-H", "UniJIS-UCS2-H");
        CMapSubstitution.cmapSubstitutions.put("Adobe-Japan1-4", "Adobe-Japan1-UCS2");
        CMapSubstitution.cmapSubstitutions.put("Adobe-Identity-0", "Identity-H");
        CMapSubstitution.cmapSubstitutions.put("Adobe-Identity-1", "Identity-H");
    }
}
