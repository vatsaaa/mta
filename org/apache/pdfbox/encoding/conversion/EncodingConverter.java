// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.encoding.conversion;

import org.apache.fontbox.cmap.CMap;

public interface EncodingConverter
{
    String convertString(final String p0);
    
    String convertBytes(final byte[] p0, final int p1, final int p2, final CMap p3);
}
