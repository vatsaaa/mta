// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.encoding.conversion;

import java.util.Iterator;
import java.util.HashMap;

class CJKEncodings
{
    private static HashMap charsetMapping;
    
    private CJKEncodings() {
    }
    
    public static final String getCharset(String encoding) {
        if (encoding.startsWith("COSName")) {
            encoding = encoding.substring(8, encoding.length() - 1);
        }
        return CJKEncodings.charsetMapping.get(encoding);
    }
    
    public static final Iterator getEncodingIterator() {
        return CJKEncodings.charsetMapping.keySet().iterator();
    }
    
    static {
        (CJKEncodings.charsetMapping = new HashMap()).put("GB-EUC-H", "GB2312");
        CJKEncodings.charsetMapping.put("GB-EUC-V", "GB2312");
        CJKEncodings.charsetMapping.put("GBpc-EUC-H", "GB2312");
        CJKEncodings.charsetMapping.put("GBpc-EUC-V", "GB2312");
        CJKEncodings.charsetMapping.put("GBK-EUC-H", "GBK");
        CJKEncodings.charsetMapping.put("GBK-EUC-V", "GBK");
        CJKEncodings.charsetMapping.put("GBKp-EUC-H", "GBK");
        CJKEncodings.charsetMapping.put("GBKp-EUC-V", "GBK");
        CJKEncodings.charsetMapping.put("GBK2K-H", "GB18030");
        CJKEncodings.charsetMapping.put("GBK2K-V", "GB18030");
        CJKEncodings.charsetMapping.put("UniGB-UCS2-H", "ISO-10646-UCS-2");
        CJKEncodings.charsetMapping.put("UniGB-UCS2-V", "ISO-10646-UCS-2");
        CJKEncodings.charsetMapping.put("UniGB-UTF16-H", "UTF-16BE");
        CJKEncodings.charsetMapping.put("UniGB-UTF16-V", "UTF-16BE");
        CJKEncodings.charsetMapping.put("B5pc-H", "BIG5");
        CJKEncodings.charsetMapping.put("B5pc-V", "BIG5");
        CJKEncodings.charsetMapping.put("HKscs-B5-H", "Big5-HKSCS");
        CJKEncodings.charsetMapping.put("HKscs-B5-V", "Big5-HKSCS");
        CJKEncodings.charsetMapping.put("ETen-B5-H", "BIG5");
        CJKEncodings.charsetMapping.put("ETen-B5-V", "BIG5");
        CJKEncodings.charsetMapping.put("ETenms-B5-H", "BIG5");
        CJKEncodings.charsetMapping.put("ETenms-B5-V", "BIG5");
        CJKEncodings.charsetMapping.put("CNS-EUC-H", "HZ");
        CJKEncodings.charsetMapping.put("CNS-EUC-V", "HZ");
        CJKEncodings.charsetMapping.put("UniCNS-UCS2-H", "ISO-10646-UCS-2");
        CJKEncodings.charsetMapping.put("UniCNS-UCS2-V", "ISO-10646-UCS-2");
        CJKEncodings.charsetMapping.put("UniCNS-UTF16-H", "UTF-16BE");
        CJKEncodings.charsetMapping.put("UniCNS-UTF16-V", "UTF-16BE");
        CJKEncodings.charsetMapping.put("83pv-RKSJ-H", "JIS");
        CJKEncodings.charsetMapping.put("90ms-RKSJ-H", "JIS");
        CJKEncodings.charsetMapping.put("90ms-RKSJ-V", "JIS");
        CJKEncodings.charsetMapping.put("90msp-RKSJ-H", "JIS");
        CJKEncodings.charsetMapping.put("90msp-RKSJ-V", "JIS");
        CJKEncodings.charsetMapping.put("90pv-RKSJ-H", "JIS");
        CJKEncodings.charsetMapping.put("Add-RKSJ-H", "JIS");
        CJKEncodings.charsetMapping.put("Add-RKSJ-V", "JIS");
        CJKEncodings.charsetMapping.put("EUC-H", "JIS");
        CJKEncodings.charsetMapping.put("EUC-V", "JIS");
        CJKEncodings.charsetMapping.put("Ext-RKSJ-H", "JIS");
        CJKEncodings.charsetMapping.put("Ext-RKSJ-V", "JIS");
        CJKEncodings.charsetMapping.put("H", "JIS");
        CJKEncodings.charsetMapping.put("V", "JIS");
        CJKEncodings.charsetMapping.put("UniJIS-UCS2-H", "ISO-10646-UCS-2");
        CJKEncodings.charsetMapping.put("UniJIS-UCS2-V", "ISO-10646-UCS-2");
        CJKEncodings.charsetMapping.put("UniJIS-UCS2-HW-H", "ISO-10646-UCS-2");
        CJKEncodings.charsetMapping.put("UniJIS-UCS2-HW-V", "ISO-10646-UCS-2");
        CJKEncodings.charsetMapping.put("UniJIS-UTF16-H", "UTF-16BE");
        CJKEncodings.charsetMapping.put("UniJIS-UTF16-V", "UTF-16BE");
        CJKEncodings.charsetMapping.put("Identity-H", "JIS");
        CJKEncodings.charsetMapping.put("Identity-V", "JIS");
        CJKEncodings.charsetMapping.put("KSC-EUC-H", "KSC");
        CJKEncodings.charsetMapping.put("KSC-EUC-V", "KSC");
        CJKEncodings.charsetMapping.put("KSCms-UHC-H", "KSC");
        CJKEncodings.charsetMapping.put("KSCms-UHC-V", "KSC");
        CJKEncodings.charsetMapping.put("KSCms-UHC-HW-H", "KSC");
        CJKEncodings.charsetMapping.put("KSCms-UHC-HW-V", "KSC");
        CJKEncodings.charsetMapping.put("KSCpc-EUC-H", "KSC");
        CJKEncodings.charsetMapping.put("UniKS-UCS2-H", "ISO-10646-UCS-2");
        CJKEncodings.charsetMapping.put("UniKS-UCS2-V", "ISO-10646-UCS-2");
        CJKEncodings.charsetMapping.put("UniKS-UTF16-H", "UTF-16BE");
        CJKEncodings.charsetMapping.put("UniKS-UTF16-V", "UTF-16BE");
    }
}
