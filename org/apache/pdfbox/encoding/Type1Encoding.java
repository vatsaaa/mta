// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.encoding;

import org.apache.pdfbox.cos.COSBase;

public class Type1Encoding extends Encoding
{
    public Type1Encoding(final int size) {
        for (int i = 1; i < size; ++i) {
            this.addCharacterEncoding(i, ".notdef");
        }
    }
    
    public COSBase getCOSObject() {
        return null;
    }
}
