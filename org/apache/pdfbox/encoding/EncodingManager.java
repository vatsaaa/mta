// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.encoding;

import java.io.IOException;
import org.apache.pdfbox.cos.COSName;

public class EncodingManager
{
    public static final EncodingManager INSTANCE;
    
    public Encoding getStandardEncoding() {
        return StandardEncoding.INSTANCE;
    }
    
    public Encoding getEncoding(final COSName name) throws IOException {
        if (COSName.STANDARD_ENCODING.equals(name)) {
            return StandardEncoding.INSTANCE;
        }
        if (COSName.WIN_ANSI_ENCODING.equals(name)) {
            return WinAnsiEncoding.INSTANCE;
        }
        if (COSName.MAC_ROMAN_ENCODING.equals(name)) {
            return MacRomanEncoding.INSTANCE;
        }
        if (COSName.PDF_DOC_ENCODING.equals(name)) {
            return PdfDocEncoding.INSTANCE;
        }
        throw new IOException("Unknown encoding for '" + name.getName() + "'");
    }
    
    static {
        INSTANCE = new EncodingManager();
    }
}
