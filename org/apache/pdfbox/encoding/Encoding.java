// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.encoding;

import java.util.Iterator;
import java.io.File;
import org.apache.commons.logging.LogFactory;
import java.util.Collections;
import java.io.InputStream;
import java.io.IOException;
import java.util.StringTokenizer;
import java.io.Reader;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.MissingResourceException;
import org.apache.pdfbox.util.ResourceLoader;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.logging.Log;
import org.apache.pdfbox.pdmodel.common.COSObjectable;

public abstract class Encoding implements COSObjectable
{
    private static final Log LOG;
    public static final String NOTDEF = ".notdef";
    protected final Map<Integer, String> codeToName;
    protected final Map<String, Integer> nameToCode;
    private static final Map<String, String> NAME_TO_CHARACTER;
    private static final Map<String, String> CHARACTER_TO_NAME;
    
    public Encoding() {
        this.codeToName = new HashMap<Integer, String>();
        this.nameToCode = new HashMap<String, Integer>();
    }
    
    private static void loadGlyphList(final String location) {
        BufferedReader glyphStream = null;
        try {
            final InputStream resource = ResourceLoader.loadResource(location);
            if (resource == null) {
                throw new MissingResourceException("Glyphlist not found: " + location, Encoding.class.getName(), location);
            }
            glyphStream = new BufferedReader(new InputStreamReader(resource));
            String line = null;
            while ((line = glyphStream.readLine()) != null) {
                line = line.trim();
                if (!line.startsWith("#")) {
                    final int semicolonIndex = line.indexOf(59);
                    if (semicolonIndex < 0) {
                        continue;
                    }
                    String unicodeValue = null;
                    try {
                        final String characterName = line.substring(0, semicolonIndex);
                        unicodeValue = line.substring(semicolonIndex + 1, line.length());
                        final StringTokenizer tokenizer = new StringTokenizer(unicodeValue, " ", false);
                        final StringBuilder value = new StringBuilder();
                        while (tokenizer.hasMoreTokens()) {
                            final int characterCode = Integer.parseInt(tokenizer.nextToken(), 16);
                            value.append((char)characterCode);
                        }
                        if (Encoding.NAME_TO_CHARACTER.containsKey(characterName)) {
                            Encoding.LOG.warn("duplicate value for characterName=" + characterName + "," + (Object)value);
                        }
                        else {
                            Encoding.NAME_TO_CHARACTER.put(characterName, value.toString());
                        }
                    }
                    catch (NumberFormatException nfe) {
                        Encoding.LOG.error("malformed unicode value " + unicodeValue, nfe);
                    }
                }
            }
        }
        catch (IOException io) {
            Encoding.LOG.error("error while reading the glyph list.", io);
            if (glyphStream != null) {
                try {
                    glyphStream.close();
                }
                catch (IOException e) {
                    Encoding.LOG.error("error when closing the glyph list.", e);
                }
            }
        }
        finally {
            if (glyphStream != null) {
                try {
                    glyphStream.close();
                }
                catch (IOException e2) {
                    Encoding.LOG.error("error when closing the glyph list.", e2);
                }
            }
        }
    }
    
    public Map<Integer, String> getCodeToNameMap() {
        return Collections.unmodifiableMap((Map<? extends Integer, ? extends String>)this.codeToName);
    }
    
    public Map<String, Integer> getNameToCodeMap() {
        return Collections.unmodifiableMap((Map<? extends String, ? extends Integer>)this.nameToCode);
    }
    
    public void addCharacterEncoding(final int code, final String name) {
        this.codeToName.put(code, name);
        this.nameToCode.put(name, code);
    }
    
    public int getCode(final String name) throws IOException {
        final Integer code = this.nameToCode.get(name);
        if (code == null) {
            throw new IOException("No character code for character name '" + name + "'");
        }
        return code;
    }
    
    public String getName(final int code) throws IOException {
        return this.codeToName.get(code);
    }
    
    public String getNameFromCharacter(final char c) throws IOException {
        final String name = Encoding.CHARACTER_TO_NAME.get(Character.toString(c));
        if (name == null) {
            throw new IOException("No name for character '" + c + "'");
        }
        return name;
    }
    
    public String getCharacter(final int code) throws IOException {
        final String name = this.getName(code);
        if (name != null) {
            return this.getCharacter(this.getName(code));
        }
        return null;
    }
    
    public String getCharacter(final String name) {
        String character = Encoding.NAME_TO_CHARACTER.get(name);
        if (character == null) {
            if (name.indexOf(46) > 0) {
                character = this.getCharacter(name.substring(0, name.indexOf(46)));
            }
            else if (name.startsWith("uni")) {
                final int nameLength = name.length();
                final StringBuilder uniStr = new StringBuilder();
                try {
                    for (int chPos = 3; chPos + 4 <= nameLength; chPos += 4) {
                        final int characterCode = Integer.parseInt(name.substring(chPos, chPos + 4), 16);
                        if (characterCode > 55295 && characterCode < 57344) {
                            Encoding.LOG.warn("Unicode character name with not allowed code area: " + name);
                        }
                        else {
                            uniStr.append((char)characterCode);
                        }
                    }
                    character = uniStr.toString();
                    Encoding.NAME_TO_CHARACTER.put(name, character);
                }
                catch (NumberFormatException nfe) {
                    Encoding.LOG.warn("Not a number in Unicode character name: " + name);
                    character = name;
                }
            }
            else if (name.startsWith("u")) {
                try {
                    final int characterCode2 = Integer.parseInt(name.substring(1), 16);
                    if (characterCode2 > 55295 && characterCode2 < 57344) {
                        Encoding.LOG.warn("Unicode character name with not allowed code area: " + name);
                    }
                    else {
                        character = String.valueOf((char)characterCode2);
                        Encoding.NAME_TO_CHARACTER.put(name, character);
                    }
                }
                catch (NumberFormatException nfe2) {
                    Encoding.LOG.warn("Not a number in Unicode character name: " + name);
                    character = name;
                }
            }
            else if (this.nameToCode.containsKey(name)) {
                final int code = this.nameToCode.get(name);
                character = Character.toString((char)code);
            }
            else {
                character = name;
            }
        }
        return character;
    }
    
    static {
        LOG = LogFactory.getLog(Encoding.class);
        NAME_TO_CHARACTER = new HashMap<String, String>();
        CHARACTER_TO_NAME = new HashMap<String, String>();
        loadGlyphList("org/apache/pdfbox/resources/glyphlist.txt");
        loadGlyphList("org/apache/pdfbox/resources/additional_glyphlist.txt");
        final String location = System.getProperty("glyphlist_ext");
        if (location != null) {
            final File external = new File(location);
            if (external.exists()) {
                loadGlyphList(location);
            }
        }
        Encoding.NAME_TO_CHARACTER.put(".notdef", "");
        Encoding.NAME_TO_CHARACTER.put("fi", "fi");
        Encoding.NAME_TO_CHARACTER.put("fl", "fl");
        Encoding.NAME_TO_CHARACTER.put("ffi", "ffi");
        Encoding.NAME_TO_CHARACTER.put("ff", "ff");
        Encoding.NAME_TO_CHARACTER.put("pi", "pi");
        for (final Map.Entry<String, String> entry : Encoding.NAME_TO_CHARACTER.entrySet()) {
            Encoding.CHARACTER_TO_NAME.put(entry.getValue(), entry.getKey());
        }
    }
}
