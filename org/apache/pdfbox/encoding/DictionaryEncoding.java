// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.encoding;

import java.io.IOException;
import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSNumber;
import org.apache.pdfbox.cos.COSArray;
import java.util.Map;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.cos.COSDictionary;

public class DictionaryEncoding extends Encoding
{
    private COSDictionary encoding;
    
    public DictionaryEncoding(final COSDictionary fontEncoding) throws IOException {
        this.encoding = null;
        this.encoding = fontEncoding;
        Encoding baseEncoding = StandardEncoding.INSTANCE;
        final COSName baseEncodingName = (COSName)this.encoding.getDictionaryObject(COSName.BASE_ENCODING);
        if (baseEncodingName != null) {
            baseEncoding = EncodingManager.INSTANCE.getEncoding(baseEncodingName);
        }
        this.nameToCode.putAll(baseEncoding.nameToCode);
        this.codeToName.putAll(baseEncoding.codeToName);
        final COSArray differences = (COSArray)this.encoding.getDictionaryObject(COSName.DIFFERENCES);
        int currentIndex = -1;
        for (int i = 0; differences != null && i < differences.size(); ++i) {
            final COSBase next = differences.getObject(i);
            if (next instanceof COSNumber) {
                currentIndex = ((COSNumber)next).intValue();
            }
            else if (next instanceof COSName) {
                final COSName name = (COSName)next;
                this.addCharacterEncoding(currentIndex++, name.getName());
            }
        }
    }
    
    public COSBase getCOSObject() {
        return this.encoding;
    }
}
