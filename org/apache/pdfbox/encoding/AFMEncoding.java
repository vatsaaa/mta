// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.encoding;

import org.apache.pdfbox.cos.COSBase;
import java.util.Iterator;
import org.apache.fontbox.afm.CharMetric;
import org.apache.fontbox.afm.FontMetric;

public class AFMEncoding extends Encoding
{
    private FontMetric metric;
    
    public AFMEncoding(final FontMetric fontInfo) {
        this.metric = null;
        this.metric = fontInfo;
        for (final CharMetric nextMetric : this.metric.getCharMetrics()) {
            this.addCharacterEncoding(nextMetric.getCharacterCode(), nextMetric.getName());
        }
    }
    
    public COSBase getCOSObject() {
        return null;
    }
}
