// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox;

import org.apache.pdfbox.pdmodel.encryption.AccessPermission;
import org.apache.pdfbox.pdmodel.encryption.DecryptionMaterial;
import java.io.IOException;
import org.apache.pdfbox.pdmodel.encryption.StandardDecryptionMaterial;
import org.apache.pdfbox.pdmodel.encryption.PublicKeyDecryptionMaterial;
import java.io.InputStream;
import java.io.FileInputStream;
import java.security.KeyStore;
import org.apache.pdfbox.pdmodel.PDDocument;

public class Decrypt
{
    private static final String ALIAS = "-alias";
    private static final String PASSWORD = "-password";
    private static final String KEYSTORE = "-keyStore";
    
    private Decrypt() {
    }
    
    public static void main(final String[] args) throws Exception {
        final Decrypt decrypt = new Decrypt();
        decrypt.decrypt(args);
    }
    
    private void decrypt(final String[] args) throws Exception {
        if (args.length < 2 || args.length > 5) {
            usage();
        }
        else {
            String password = null;
            String infile = null;
            String outfile = null;
            String alias = null;
            String keyStore = null;
            for (int i = 0; i < args.length; ++i) {
                if (args[i].equals("-alias")) {
                    if (++i >= args.length) {
                        usage();
                    }
                    alias = args[i];
                }
                else if (args[i].equals("-keyStore")) {
                    if (++i >= args.length) {
                        usage();
                    }
                    keyStore = args[i];
                }
                else if (args[i].equals("-password")) {
                    if (++i >= args.length) {
                        usage();
                    }
                    password = args[i];
                }
                else if (infile == null) {
                    infile = args[i];
                }
                else if (outfile == null) {
                    outfile = args[i];
                }
                else {
                    usage();
                }
            }
            if (infile == null) {
                usage();
            }
            if (outfile == null) {
                outfile = infile;
            }
            if (password == null) {
                password = "";
            }
            PDDocument document = null;
            try {
                document = PDDocument.load(infile);
                if (document.isEncrypted()) {
                    DecryptionMaterial decryptionMaterial = null;
                    if (keyStore != null) {
                        final KeyStore ks = KeyStore.getInstance("PKCS12");
                        ks.load(new FileInputStream(keyStore), password.toCharArray());
                        decryptionMaterial = new PublicKeyDecryptionMaterial(ks, alias, password);
                    }
                    else {
                        decryptionMaterial = new StandardDecryptionMaterial(password);
                    }
                    document.openProtection(decryptionMaterial);
                    final AccessPermission ap = document.getCurrentAccessPermission();
                    if (!ap.isOwnerPermission()) {
                        throw new IOException("Error: You are only allowed to decrypt a document with the owner password.");
                    }
                    document.setAllSecurityToBeRemoved(true);
                    document.save(outfile);
                }
                else {
                    System.err.println("Error: Document is not encrypted.");
                }
            }
            finally {
                if (document != null) {
                    document.close();
                }
            }
        }
    }
    
    private static void usage() {
        System.err.println("usage: java -jar pdfbox-app-x.y.z.jar Decrypt [options] <inputfile> [outputfile]");
        System.err.println("-alias      The alias of the key in the certificate file (mandatory if several keys are available)");
        System.err.println("-password   The password to open the certificate and extract the private key from it.");
        System.err.println("-keyStore   The KeyStore that holds the certificate.");
        System.exit(-1);
    }
}
