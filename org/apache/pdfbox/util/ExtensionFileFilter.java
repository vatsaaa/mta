// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.util;

import java.io.File;
import javax.swing.filechooser.FileFilter;

public class ExtensionFileFilter extends FileFilter
{
    private String[] extensions;
    private String desc;
    
    public ExtensionFileFilter(final String[] ext, final String description) {
        this.extensions = null;
        this.extensions = ext;
        this.desc = description;
    }
    
    @Override
    public boolean accept(final File pathname) {
        if (pathname.isDirectory()) {
            return true;
        }
        boolean acceptable = false;
        final String name = pathname.getName().toUpperCase();
        for (int i = 0; !acceptable && i < this.extensions.length; ++i) {
            if (name.endsWith(this.extensions[i].toUpperCase())) {
                acceptable = true;
            }
        }
        return acceptable;
    }
    
    @Override
    public String getDescription() {
        return this.desc;
    }
}
