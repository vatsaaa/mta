// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.util;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class PDFOperator
{
    private String theOperator;
    private byte[] imageData;
    private ImageParameters imageParameters;
    private static Map operators;
    
    private PDFOperator(final String aOperator) {
        this.theOperator = aOperator;
        if (aOperator.startsWith("/")) {
            throw new RuntimeException("Operators are not allowed to start with / '" + aOperator + "'");
        }
    }
    
    public static PDFOperator getOperator(final String operator) {
        PDFOperator operation = null;
        if (operator.equals("ID") || operator.equals("BI")) {
            operation = new PDFOperator(operator);
        }
        else {
            operation = PDFOperator.operators.get(operator);
            if (operation == null) {
                operation = new PDFOperator(operator);
                PDFOperator.operators.put(operator, operation);
            }
        }
        return operation;
    }
    
    public String getOperation() {
        return this.theOperator;
    }
    
    @Override
    public String toString() {
        return "PDFOperator{" + this.theOperator + "}";
    }
    
    public byte[] getImageData() {
        return this.imageData;
    }
    
    public void setImageData(final byte[] imageDataArray) {
        this.imageData = imageDataArray;
    }
    
    public ImageParameters getImageParameters() {
        return this.imageParameters;
    }
    
    public void setImageParameters(final ImageParameters params) {
        this.imageParameters = params;
    }
    
    static {
        PDFOperator.operators = Collections.synchronizedMap(new HashMap<Object, Object>());
    }
}
