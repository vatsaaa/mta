// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.util;

import org.apache.pdfbox.cos.COSStream;
import org.apache.pdfbox.pdmodel.common.PDStream;
import java.util.Iterator;
import org.apache.pdfbox.pdmodel.PDPage;
import java.util.Properties;
import java.io.IOException;
import java.util.HashMap;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Vector;
import java.awt.geom.Rectangle2D;
import java.util.Map;
import java.util.List;

public class PDFTextStripperByArea extends PDFTextStripper
{
    private List<String> regions;
    private Map<String, Rectangle2D> regionArea;
    private Map<String, Vector<ArrayList<TextPosition>>> regionCharacterList;
    private Map<String, StringWriter> regionText;
    
    public PDFTextStripperByArea() throws IOException {
        this.regions = new ArrayList<String>();
        this.regionArea = new HashMap<String, Rectangle2D>();
        this.regionCharacterList = new HashMap<String, Vector<ArrayList<TextPosition>>>();
        this.regionText = new HashMap<String, StringWriter>();
        this.setPageSeparator("");
    }
    
    public PDFTextStripperByArea(final Properties props) throws IOException {
        super(props);
        this.regions = new ArrayList<String>();
        this.regionArea = new HashMap<String, Rectangle2D>();
        this.regionCharacterList = new HashMap<String, Vector<ArrayList<TextPosition>>>();
        this.regionText = new HashMap<String, StringWriter>();
        this.setPageSeparator("");
    }
    
    public PDFTextStripperByArea(final String encoding) throws IOException {
        super(encoding);
        this.regions = new ArrayList<String>();
        this.regionArea = new HashMap<String, Rectangle2D>();
        this.regionCharacterList = new HashMap<String, Vector<ArrayList<TextPosition>>>();
        this.regionText = new HashMap<String, StringWriter>();
        this.setPageSeparator("");
    }
    
    public void addRegion(final String regionName, final Rectangle2D rect) {
        this.regions.add(regionName);
        this.regionArea.put(regionName, rect);
    }
    
    public List<String> getRegions() {
        return this.regions;
    }
    
    public String getTextForRegion(final String regionName) {
        final StringWriter text = this.regionText.get(regionName);
        return text.toString();
    }
    
    public void extractRegions(final PDPage page) throws IOException {
        final Iterator<String> regionIter = this.regions.iterator();
        while (regionIter.hasNext()) {
            this.setStartPage(this.getCurrentPageNo());
            this.setEndPage(this.getCurrentPageNo());
            final String regionName = regionIter.next();
            final Vector<ArrayList<TextPosition>> regionCharactersByArticle = new Vector<ArrayList<TextPosition>>();
            regionCharactersByArticle.add(new ArrayList<TextPosition>());
            this.regionCharacterList.put(regionName, regionCharactersByArticle);
            this.regionText.put(regionName, new StringWriter());
        }
        final PDStream contentStream = page.getContents();
        if (contentStream != null) {
            final COSStream contents = contentStream.getStream();
            this.processPage(page, contents);
        }
    }
    
    @Override
    protected void processTextPosition(final TextPosition text) {
        for (final String region : this.regionArea.keySet()) {
            final Rectangle2D rect = this.regionArea.get(region);
            if (rect.contains(text.getX(), text.getY())) {
                this.charactersByArticle = (Vector<List<TextPosition>>)this.regionCharacterList.get(region);
                super.processTextPosition(text);
            }
        }
    }
    
    @Override
    protected void writePage() throws IOException {
        for (final String region : this.regionArea.keySet()) {
            this.charactersByArticle = (Vector<List<TextPosition>>)this.regionCharacterList.get(region);
            this.output = this.regionText.get(region);
            super.writePage();
        }
    }
}
