// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.util;

import java.util.Map;

public class MapUtil
{
    private MapUtil() {
    }
    
    public static final String getNextUniqueKey(final Map map, final String prefix) {
        int counter;
        for (counter = 0; map.get(prefix + counter) != null; ++counter) {}
        return prefix + counter;
    }
}
