// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.util;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDDocument;

public class PageExtractor
{
    protected PDDocument sourceDocument;
    protected int startPage;
    protected int endPage;
    
    public PageExtractor(final PDDocument sourceDocument) {
        this.startPage = 1;
        this.endPage = 0;
        this.sourceDocument = sourceDocument;
        this.endPage = sourceDocument.getNumberOfPages();
    }
    
    public PageExtractor(final PDDocument sourceDocument, final int startPage, final int endPage) {
        this(sourceDocument);
        this.startPage = startPage;
        this.endPage = endPage;
    }
    
    public PDDocument extract() throws IOException {
        final PDDocument extractedDocument = new PDDocument();
        extractedDocument.setDocumentInformation(this.sourceDocument.getDocumentInformation());
        extractedDocument.getDocumentCatalog().setViewerPreferences(this.sourceDocument.getDocumentCatalog().getViewerPreferences());
        final List<PDPage> pages = (List<PDPage>)this.sourceDocument.getDocumentCatalog().getAllPages();
        int pageCounter = 1;
        for (final PDPage page : pages) {
            if (pageCounter >= this.startPage && pageCounter <= this.endPage) {
                final PDPage imported = extractedDocument.importPage(page);
                imported.setCropBox(page.findCropBox());
                imported.setMediaBox(page.findMediaBox());
                imported.setResources(page.findResources());
                imported.setRotation(page.findRotation());
            }
            ++pageCounter;
        }
        return extractedDocument;
    }
    
    public int getStartPage() {
        return this.startPage;
    }
    
    public void setStartPage(final int startPage) {
        this.startPage = startPage;
    }
    
    public int getEndPage() {
        return this.endPage;
    }
    
    public void setEndPage(final int endPage) {
        this.endPage = endPage;
    }
}
