// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.util;

public class PositionWrapper
{
    private boolean isLineStart;
    private boolean isParagraphStart;
    private boolean isPageBreak;
    private boolean isHangingIndent;
    private boolean isArticleStart;
    private TextPosition position;
    
    protected TextPosition getTextPosition() {
        return this.position;
    }
    
    public boolean isLineStart() {
        return this.isLineStart;
    }
    
    public void setLineStart() {
        this.isLineStart = true;
    }
    
    public boolean isParagraphStart() {
        return this.isParagraphStart;
    }
    
    public void setParagraphStart() {
        this.isParagraphStart = true;
    }
    
    public boolean isArticleStart() {
        return this.isArticleStart;
    }
    
    public void setArticleStart() {
        this.isArticleStart = true;
    }
    
    public boolean isPageBreak() {
        return this.isPageBreak;
    }
    
    public void setPageBreak() {
        this.isPageBreak = true;
    }
    
    public boolean isHangingIndent() {
        return this.isHangingIndent;
    }
    
    public void setHangingIndent() {
        this.isHangingIndent = true;
    }
    
    public PositionWrapper(final TextPosition position) {
        this.isLineStart = false;
        this.isParagraphStart = false;
        this.isPageBreak = false;
        this.isHangingIndent = false;
        this.isArticleStart = false;
        this.position = null;
        this.position = position;
    }
}
