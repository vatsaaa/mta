// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.util;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;
import org.w3c.dom.Element;
import javax.xml.parsers.DocumentBuilder;
import java.io.IOException;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import java.io.InputStream;

public class XMLUtil
{
    private XMLUtil() {
    }
    
    public static Document parse(final InputStream is) throws IOException {
        try {
            final DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
            final DocumentBuilder builder = builderFactory.newDocumentBuilder();
            return builder.parse(is);
        }
        catch (Exception e) {
            final IOException thrown = new IOException(e.getMessage());
            throw thrown;
        }
    }
    
    public static String getNodeValue(final Element node) {
        String retval = "";
        final NodeList children = node.getChildNodes();
        for (int i = 0; i < children.getLength(); ++i) {
            final Node next = children.item(i);
            if (next instanceof Text) {
                retval = next.getNodeValue();
            }
        }
        return retval;
    }
}
