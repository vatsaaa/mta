// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.util;

import java.io.UnsupportedEncodingException;

public class StringUtil
{
    public static byte[] getBytes(final String s) {
        try {
            return s.getBytes("ISO-8859-1");
        }
        catch (UnsupportedEncodingException e) {
            throw new RuntimeException("Unsupported Encoding", e);
        }
    }
}
