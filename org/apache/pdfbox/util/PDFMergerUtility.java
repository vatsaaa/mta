// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.util;

import org.apache.pdfbox.pdmodel.interactive.form.PDFieldFactory;
import org.apache.pdfbox.pdmodel.interactive.form.PDField;
import org.apache.pdfbox.pdmodel.common.COSArrayList;
import org.apache.pdfbox.pdmodel.PDDocumentNameDictionary;
import org.apache.pdfbox.pdmodel.interactive.form.PDAcroForm;
import org.apache.pdfbox.pdmodel.PDDocumentCatalog;
import org.apache.pdfbox.pdmodel.PDDocumentInformation;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.common.PDStream;
import org.apache.pdfbox.cos.COSStream;
import org.apache.pdfbox.cos.COSInteger;
import org.apache.pdfbox.cos.COSNumber;
import org.apache.pdfbox.pdmodel.interactive.documentnavigation.outline.PDOutlineItem;
import org.apache.pdfbox.pdmodel.interactive.documentnavigation.outline.PDDocumentOutline;
import org.apache.pdfbox.pdmodel.common.COSObjectable;
import org.apache.pdfbox.cos.COSArray;
import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.cos.COSDictionary;
import org.apache.pdfbox.exceptions.COSVisitorException;
import java.io.IOException;
import java.util.Iterator;
import org.apache.pdfbox.pdmodel.PDDocument;
import java.util.Vector;
import java.util.Collection;
import java.io.FileInputStream;
import java.io.File;
import java.util.ArrayList;
import java.io.OutputStream;
import java.io.InputStream;
import java.util.List;

public class PDFMergerUtility
{
    private List<InputStream> sources;
    private String destinationFileName;
    private OutputStream destinationStream;
    private boolean ignoreAcroFormErrors;
    private int nextFieldNum;
    
    public PDFMergerUtility() {
        this.ignoreAcroFormErrors = false;
        this.nextFieldNum = 1;
        this.sources = new ArrayList<InputStream>();
    }
    
    public String getDestinationFileName() {
        return this.destinationFileName;
    }
    
    public void setDestinationFileName(final String destination) {
        this.destinationFileName = destination;
    }
    
    public OutputStream getDestinationStream() {
        return this.destinationStream;
    }
    
    public void setDestinationStream(final OutputStream destStream) {
        this.destinationStream = destStream;
    }
    
    public void addSource(final String source) {
        try {
            this.sources.add(new FileInputStream(new File(source)));
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
    
    public void addSource(final File source) {
        try {
            this.sources.add(new FileInputStream(source));
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
    
    public void addSource(final InputStream source) {
        this.sources.add(source);
    }
    
    public void addSources(final List<InputStream> sourcesList) {
        this.sources.addAll(this.sources);
    }
    
    public void mergeDocuments() throws IOException, COSVisitorException {
        PDDocument destination = null;
        if (this.sources != null && this.sources.size() > 0) {
            final Vector<PDDocument> tobeclosed = new Vector<PDDocument>();
            try {
                final Iterator<InputStream> sit = this.sources.iterator();
                InputStream sourceFile = sit.next();
                destination = PDDocument.load(sourceFile);
                while (sit.hasNext()) {
                    sourceFile = sit.next();
                    final PDDocument source = PDDocument.load(sourceFile);
                    tobeclosed.add(source);
                    this.appendDocument(destination, source);
                }
                if (this.destinationStream == null) {
                    destination.save(this.destinationFileName);
                }
                else {
                    destination.save(this.destinationStream);
                }
            }
            finally {
                if (destination != null) {
                    destination.close();
                }
                for (final PDDocument doc : tobeclosed) {
                    doc.close();
                }
            }
        }
    }
    
    public void appendDocument(final PDDocument destination, final PDDocument source) throws IOException {
        if (destination.isEncrypted()) {
            throw new IOException("Error: destination PDF is encrypted, can't append encrypted PDF documents.");
        }
        if (source.isEncrypted()) {
            throw new IOException("Error: source PDF is encrypted, can't append encrypted PDF documents.");
        }
        final PDDocumentInformation destInfo = destination.getDocumentInformation();
        final PDDocumentInformation srcInfo = source.getDocumentInformation();
        destInfo.getDictionary().mergeInto(srcInfo.getDictionary());
        final PDDocumentCatalog destCatalog = destination.getDocumentCatalog();
        final PDDocumentCatalog srcCatalog = source.getDocumentCatalog();
        final float destVersion = destination.getDocument().getVersion();
        final float srcVersion = source.getDocument().getVersion();
        if (destVersion < srcVersion) {
            destination.getDocument().setVersion(srcVersion);
        }
        if (destCatalog.getOpenAction() == null) {
            destCatalog.setOpenAction(srcCatalog.getOpenAction());
        }
        final COSDictionary srcPages = (COSDictionary)srcCatalog.getCOSDictionary().getDictionaryObject(COSName.PAGES);
        final COSDictionary srcResources = (COSDictionary)srcPages.getDictionaryObject(COSName.RESOURCES);
        final COSDictionary destPages = (COSDictionary)destCatalog.getCOSDictionary().getDictionaryObject(COSName.PAGES);
        final COSDictionary destResources = (COSDictionary)destPages.getDictionaryObject(COSName.RESOURCES);
        if (srcResources != null) {
            if (destResources != null) {
                destResources.mergeInto(srcResources);
            }
            else {
                destPages.setItem(COSName.RESOURCES, srcResources);
            }
        }
        final PDFCloneUtility cloner = new PDFCloneUtility(destination);
        try {
            final PDAcroForm destAcroForm = destCatalog.getAcroForm();
            final PDAcroForm srcAcroForm = srcCatalog.getAcroForm();
            if (destAcroForm == null) {
                cloner.cloneForNewDocument(srcAcroForm);
                destCatalog.setAcroForm(srcAcroForm);
            }
            else if (srcAcroForm != null) {
                this.mergeAcroForm(cloner, destAcroForm, srcAcroForm);
            }
        }
        catch (Exception e) {
            if (!this.ignoreAcroFormErrors) {
                throw (IOException)e;
            }
        }
        final COSArray destThreads = (COSArray)destCatalog.getCOSDictionary().getDictionaryObject(COSName.THREADS);
        final COSArray srcThreads = (COSArray)cloner.cloneForNewDocument(destCatalog.getCOSDictionary().getDictionaryObject(COSName.THREADS));
        if (destThreads == null) {
            destCatalog.getCOSDictionary().setItem(COSName.THREADS, srcThreads);
        }
        else {
            destThreads.addAll(srcThreads);
        }
        final PDDocumentNameDictionary destNames = destCatalog.getNames();
        final PDDocumentNameDictionary srcNames = srcCatalog.getNames();
        if (srcNames != null) {
            if (destNames == null) {
                destCatalog.getCOSDictionary().setItem(COSName.NAMES, cloner.cloneForNewDocument(srcNames));
            }
            else {
                cloner.cloneMerge(srcNames, destNames);
            }
        }
        final PDDocumentOutline destOutline = destCatalog.getDocumentOutline();
        final PDDocumentOutline srcOutline = srcCatalog.getDocumentOutline();
        if (srcOutline != null) {
            if (destOutline == null) {
                final PDDocumentOutline cloned = new PDDocumentOutline((COSDictionary)cloner.cloneForNewDocument(srcOutline));
                destCatalog.setDocumentOutline(cloned);
            }
            else {
                final PDOutlineItem first = srcOutline.getFirstChild();
                if (first != null) {
                    final PDOutlineItem clonedFirst = new PDOutlineItem((COSDictionary)cloner.cloneForNewDocument(first));
                    destOutline.appendChild(clonedFirst);
                }
            }
        }
        final String destPageMode = destCatalog.getPageMode();
        final String srcPageMode = srcCatalog.getPageMode();
        if (destPageMode == null) {
            destCatalog.setPageMode(srcPageMode);
        }
        COSDictionary destLabels = (COSDictionary)destCatalog.getCOSDictionary().getDictionaryObject(COSName.PAGE_LABELS);
        final COSDictionary srcLabels = (COSDictionary)srcCatalog.getCOSDictionary().getDictionaryObject(COSName.PAGE_LABELS);
        if (srcLabels != null) {
            final int destPageCount = destination.getNumberOfPages();
            COSArray destNums = null;
            if (destLabels == null) {
                destLabels = new COSDictionary();
                destNums = new COSArray();
                destLabels.setItem(COSName.NUMS, destNums);
                destCatalog.getCOSDictionary().setItem(COSName.PAGE_LABELS, destLabels);
            }
            else {
                destNums = (COSArray)destLabels.getDictionaryObject(COSName.NUMS);
            }
            final COSArray srcNums = (COSArray)srcLabels.getDictionaryObject(COSName.NUMS);
            if (srcNums != null) {
                for (int i = 0; i < srcNums.size(); i += 2) {
                    final COSNumber labelIndex = (COSNumber)srcNums.getObject(i);
                    final long labelIndexValue = labelIndex.intValue();
                    destNums.add(COSInteger.get(labelIndexValue + destPageCount));
                    destNums.add(cloner.cloneForNewDocument(srcNums.getObject(i + 1)));
                }
            }
        }
        final COSStream destMetadata = (COSStream)destCatalog.getCOSDictionary().getDictionaryObject(COSName.METADATA);
        final COSStream srcMetadata = (COSStream)srcCatalog.getCOSDictionary().getDictionaryObject(COSName.METADATA);
        if (destMetadata == null && srcMetadata != null) {
            final PDStream newStream = new PDStream(destination, srcMetadata.getUnfilteredStream(), false);
            newStream.getStream().mergeInto(srcMetadata);
            newStream.addCompression();
            destCatalog.getCOSDictionary().setItem(COSName.METADATA, newStream);
        }
        final List<PDPage> pages = (List<PDPage>)srcCatalog.getAllPages();
        for (final PDPage page : pages) {
            final PDPage newPage = new PDPage((COSDictionary)cloner.cloneForNewDocument(page.getCOSDictionary()));
            newPage.setCropBox(page.findCropBox());
            newPage.setMediaBox(page.findMediaBox());
            newPage.setRotation(page.findRotation());
            destination.addPage(newPage);
        }
    }
    
    private void mergeAcroForm(final PDFCloneUtility cloner, final PDAcroForm destAcroForm, final PDAcroForm srcAcroForm) throws IOException {
        List destFields = destAcroForm.getFields();
        final List srcFields = srcAcroForm.getFields();
        if (srcFields != null) {
            if (destFields == null) {
                destFields = new COSArrayList();
                destAcroForm.setFields(destFields);
            }
            for (final PDField srcField : srcFields) {
                final PDField destField = PDFieldFactory.createField(destAcroForm, (COSDictionary)cloner.cloneForNewDocument(srcField.getDictionary()));
                if (destAcroForm.getField(destField.getFullyQualifiedName()) != null) {
                    destField.setPartialName("dummyFieldName" + this.nextFieldNum++);
                }
                destFields.add(destField);
            }
        }
    }
    
    public boolean isIgnoreAcroFormErrors() {
        return this.ignoreAcroFormErrors;
    }
    
    public void setIgnoreAcroFormErrors(final boolean ignoreAcroFormErrors) {
        this.ignoreAcroFormErrors = ignoreAcroFormErrors;
    }
}
