// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.util;

import java.util.Date;
import java.text.ParseException;
import java.util.GregorianCalendar;
import java.util.SimpleTimeZone;
import java.io.IOException;
import org.apache.pdfbox.cos.COSString;
import java.util.TimeZone;
import java.util.Locale;
import java.util.Calendar;
import java.text.SimpleDateFormat;

public class DateConverter
{
    private static final SimpleDateFormat[] POTENTIAL_FORMATS;
    
    private DateConverter() {
    }
    
    public static String toString(final Calendar date) {
        String retval = null;
        if (date != null) {
            final StringBuffer buffer = new StringBuffer();
            final TimeZone zone = date.getTimeZone();
            final long offsetInMinutes = zone.getOffset(date.getTimeInMillis()) / 1000 / 60;
            final long hours = Math.abs(offsetInMinutes / 60L);
            final long minutes = Math.abs(offsetInMinutes % 60L);
            buffer.append("D:");
            buffer.append(new SimpleDateFormat("yyyyMMddHHmmss", Locale.ENGLISH).format(date.getTime()));
            if (offsetInMinutes == 0L) {
                buffer.append("Z");
            }
            else if (offsetInMinutes < 0L) {
                buffer.append("-");
            }
            else {
                buffer.append("+");
            }
            if (hours < 10L) {
                buffer.append("0");
            }
            buffer.append(hours);
            buffer.append("'");
            if (minutes < 10L) {
                buffer.append("0");
            }
            buffer.append(minutes);
            buffer.append("'");
            retval = buffer.toString();
        }
        return retval;
    }
    
    public static Calendar toCalendar(final COSString date) throws IOException {
        Calendar retval = null;
        if (date != null) {
            retval = toCalendar(date.getString());
        }
        return retval;
    }
    
    public static Calendar toCalendar(String date) throws IOException {
        Calendar retval = null;
        if (date != null && date.trim().length() > 0) {
            int year = 0;
            int month = 1;
            int day = 1;
            int hour = 0;
            int minute = 0;
            int second = 0;
            try {
                SimpleTimeZone zone = null;
                if (date.startsWith("D:")) {
                    date = date.substring(2, date.length());
                }
                if (date.length() < 4) {
                    throw new IOException("Error: Invalid date format '" + date + "'");
                }
                year = Integer.parseInt(date.substring(0, 4));
                if (date.length() >= 6) {
                    month = Integer.parseInt(date.substring(4, 6));
                }
                if (date.length() >= 8) {
                    day = Integer.parseInt(date.substring(6, 8));
                }
                if (date.length() >= 10) {
                    hour = Integer.parseInt(date.substring(8, 10));
                }
                if (date.length() >= 12) {
                    minute = Integer.parseInt(date.substring(10, 12));
                }
                if (date.length() >= 14) {
                    second = Integer.parseInt(date.substring(12, 14));
                }
                if (date.length() >= 15) {
                    final char sign = date.charAt(14);
                    if (sign == 'Z') {
                        zone = new SimpleTimeZone(0, "Unknown");
                    }
                    else {
                        int hours = 0;
                        int minutes = 0;
                        if (date.length() >= 17) {
                            if (sign == '+') {
                                hours = Integer.parseInt(date.substring(15, 17));
                            }
                            else if (sign == '-') {
                                hours = -Integer.parseInt(date.substring(15, 17));
                            }
                            else {
                                hours = -Integer.parseInt(date.substring(14, 16));
                            }
                        }
                        if (date.length() > 20) {
                            minutes = Integer.parseInt(date.substring(18, 20));
                        }
                        zone = new SimpleTimeZone(hours * 60 * 60 * 1000 + minutes * 60 * 1000, "Unknown");
                    }
                }
                if (zone != null) {
                    retval = new GregorianCalendar(zone);
                }
                else {
                    retval = new GregorianCalendar();
                }
                retval.set(year, month - 1, day, hour, minute, second);
                retval.set(14, 0);
            }
            catch (NumberFormatException e) {
                for (int i = 0; retval == null && i < DateConverter.POTENTIAL_FORMATS.length; ++i) {
                    try {
                        final Date utilDate = DateConverter.POTENTIAL_FORMATS[i].parse(date);
                        retval = new GregorianCalendar();
                        retval.setTime(utilDate);
                    }
                    catch (ParseException ex) {}
                }
                if (retval == null) {
                    throw new IOException("Error converting date:" + date);
                }
            }
        }
        return retval;
    }
    
    private static final void zeroAppend(final StringBuffer out, final int number) {
        if (number < 10) {
            out.append("0");
        }
        out.append(number);
    }
    
    public static String toISO8601(final Calendar cal) {
        final StringBuffer retval = new StringBuffer();
        retval.append(cal.get(1));
        retval.append("-");
        zeroAppend(retval, cal.get(2) + 1);
        retval.append("-");
        zeroAppend(retval, cal.get(5));
        retval.append("T");
        zeroAppend(retval, cal.get(11));
        retval.append(":");
        zeroAppend(retval, cal.get(12));
        retval.append(":");
        zeroAppend(retval, cal.get(13));
        int timeZone = cal.get(15) + cal.get(16);
        if (timeZone < 0) {
            retval.append("-");
        }
        else {
            retval.append("+");
        }
        timeZone = Math.abs(timeZone);
        final int hours = timeZone / 1000 / 60 / 60;
        final int minutes = (timeZone - hours * 1000 * 60 * 60) / 1000 / 1000;
        if (hours < 10) {
            retval.append("0");
        }
        retval.append(Integer.toString(hours));
        retval.append(":");
        if (minutes < 10) {
            retval.append("0");
        }
        retval.append(Integer.toString(minutes));
        return retval.toString();
    }
    
    static {
        POTENTIAL_FORMATS = new SimpleDateFormat[] { new SimpleDateFormat("EEEE, dd MMM yyyy hh:mm:ss a", Locale.ENGLISH), new SimpleDateFormat("EEEE, MMM dd, yyyy hh:mm:ss a", Locale.ENGLISH), new SimpleDateFormat("MM/dd/yyyy hh:mm:ss", Locale.ENGLISH), new SimpleDateFormat("MM/dd/yyyy", Locale.ENGLISH), new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.ENGLISH), new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssz", Locale.ENGLISH), new SimpleDateFormat("EEEE, MMM dd, yyyy", Locale.ENGLISH), new SimpleDateFormat("EEEE MMM dd, yyyy HH:mm:ss", Locale.ENGLISH), new SimpleDateFormat("EEEE MMM dd HH:mm:ss z yyyy", Locale.ENGLISH), new SimpleDateFormat("EEEE, MMM dd, yyyy 'at' hh:mma", Locale.ENGLISH), new SimpleDateFormat("d/MM/yyyy hh:mm:ss", Locale.ENGLISH), new SimpleDateFormat("dd/MM/yyyy hh:mm:ss", Locale.ENGLISH), new SimpleDateFormat("EEEEEEEEEE, MMMMMMMMMMMM dd, yyyy", Locale.ENGLISH), new SimpleDateFormat("dd MMM yyyy hh:mm:ss", Locale.ENGLISH), new SimpleDateFormat("dd MMM yyyy hh:mm", Locale.ENGLISH), new SimpleDateFormat("M/dd/yyyy hh:mm:ss", Locale.ENGLISH), new SimpleDateFormat("MM/d/yyyy hh:mm:ss", Locale.ENGLISH), new SimpleDateFormat("M/dd/yyyy", Locale.ENGLISH), new SimpleDateFormat("MM/d/yyyy", Locale.ENGLISH), new SimpleDateFormat("M/d/yyyy hh:mm:ss", Locale.ENGLISH), new SimpleDateFormat("M/d/yyyy", Locale.ENGLISH), new SimpleDateFormat("M/d/yy hh:mm:ss", Locale.ENGLISH), new SimpleDateFormat("M/d/yy", Locale.ENGLISH), new SimpleDateFormat("yyyymmdd hh:mm:ss Z"), new SimpleDateFormat("yyyymmdd hh:mm:ss"), new SimpleDateFormat("yyyymmdd'+00''00'''"), new SimpleDateFormat("yyyymmdd'+01''00'''"), new SimpleDateFormat("yyyymmdd'+02''00'''"), new SimpleDateFormat("yyyymmdd'+03''00'''"), new SimpleDateFormat("yyyymmdd'+04''00'''"), new SimpleDateFormat("yyyymmdd'+05''00'''"), new SimpleDateFormat("yyyymmdd'+06''00'''"), new SimpleDateFormat("yyyymmdd'+07''00'''"), new SimpleDateFormat("yyyymmdd'+08''00'''"), new SimpleDateFormat("yyyymmdd'+09''00'''"), new SimpleDateFormat("yyyymmdd'+10''00'''"), new SimpleDateFormat("yyyymmdd'+11''00'''"), new SimpleDateFormat("yyyymmdd'+12''00'''"), new SimpleDateFormat("yyyymmdd'-01''00'''"), new SimpleDateFormat("yyyymmdd'-02''00'''"), new SimpleDateFormat("yyyymmdd'-03''00'''"), new SimpleDateFormat("yyyymmdd'-04''00'''"), new SimpleDateFormat("yyyymmdd'-05''00'''"), new SimpleDateFormat("yyyymmdd'-06''00'''"), new SimpleDateFormat("yyyymmdd'-07''00'''"), new SimpleDateFormat("yyyymmdd'-08''00'''"), new SimpleDateFormat("yyyymmdd'-09''00'''"), new SimpleDateFormat("yyyymmdd'-10''00'''"), new SimpleDateFormat("yyyymmdd'-11''00'''"), new SimpleDateFormat("yyyymmdd'-12''00'''"), new SimpleDateFormat("yyyymmdd") };
    }
}
