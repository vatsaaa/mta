// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.util;

import com.ibm.icu.text.Normalizer;
import com.ibm.icu.text.Bidi;

public class ICU4JImpl
{
    Bidi bidi;
    
    public ICU4JImpl() {
        (this.bidi = new Bidi()).setReorderingMode(5);
    }
    
    public String makeLineLogicalOrder(final String str, final boolean isRtlDominant) {
        this.bidi.setPara(str, (byte)(byte)(isRtlDominant ? 1 : 0), (byte[])null);
        return this.bidi.writeReordered(2);
    }
    
    public String normalizePres(final String str) {
        StringBuilder builder = null;
        int p = 0;
        int q = 0;
        for (int strLength = str.length(); q < strLength; ++q) {
            final char c = str.charAt(q);
            if (('\ufb00' <= c && c <= '\ufdff') || ('\ufe70' <= c && c <= '\ufeff')) {
                if (builder == null) {
                    builder = new StringBuilder(strLength * 2);
                }
                builder.append(str.substring(p, q));
                if (c == '\ufdf2' && q > 0 && (str.charAt(q - 1) == '\u0627' || str.charAt(q - 1) == '\ufe8d')) {
                    builder.append("\u0644\u0644\u0647");
                }
                else {
                    builder.append(Normalizer.normalize((int)c, Normalizer.NFKC).trim());
                }
                p = q + 1;
            }
        }
        if (builder == null) {
            return str;
        }
        builder.append(str.substring(p, q));
        return builder.toString();
    }
    
    public String normalizeDiac(final String str) {
        final StringBuilder retStr = new StringBuilder();
        for (int strLength = str.length(), i = 0; i < strLength; ++i) {
            final char c = str.charAt(i);
            final int type = Character.getType(c);
            if (type == 6 || type == 27 || type == 4) {
                retStr.append(Normalizer.normalize((int)c, Normalizer.NFKC).trim());
            }
            else {
                retStr.append(str.charAt(i));
            }
        }
        return retStr.toString();
    }
}
