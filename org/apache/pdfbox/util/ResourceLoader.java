// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.util;

import java.util.Properties;
import java.io.IOException;
import java.io.FileInputStream;
import java.io.File;
import java.io.InputStream;

public class ResourceLoader
{
    private ResourceLoader() {
    }
    
    public static InputStream loadResource(final String resourceName) throws IOException {
        ClassLoader loader = ResourceLoader.class.getClassLoader();
        InputStream is = null;
        if (loader != null) {
            is = loader.getResourceAsStream(resourceName);
        }
        if (is == null) {
            loader = ClassLoader.getSystemClassLoader();
            if (loader != null) {
                is = loader.getResourceAsStream(resourceName);
            }
        }
        if (is == null) {
            final File f = new File(resourceName);
            if (f.exists()) {
                is = new FileInputStream(f);
            }
        }
        return is;
    }
    
    public static Properties loadProperties(final String resourceName, final boolean failIfNotFound) throws IOException {
        Properties properties = null;
        InputStream is = null;
        try {
            is = loadResource(resourceName);
            if (is != null) {
                properties = new Properties();
                properties.load(is);
            }
            else if (failIfNotFound) {
                throw new IOException("Error: could not find resource '" + resourceName + "' on classpath.");
            }
        }
        finally {
            if (is != null) {
                is.close();
            }
        }
        return properties;
    }
    
    public static Properties loadProperties(final String resourceName, final Properties defaults) throws IOException {
        InputStream is = null;
        try {
            is = loadResource(resourceName);
            if (is != null) {
                defaults.load(is);
            }
        }
        finally {
            if (is != null) {
                is.close();
            }
        }
        return defaults;
    }
}
