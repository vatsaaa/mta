// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.util.operator;

import org.apache.pdfbox.pdmodel.graphics.color.PDColorState;
import java.io.IOException;
import org.apache.pdfbox.cos.COSNumber;
import org.apache.pdfbox.pdmodel.graphics.color.PDColorSpace;
import org.apache.pdfbox.pdmodel.graphics.color.PDDeviceGray;
import org.apache.pdfbox.cos.COSBase;
import java.util.List;
import org.apache.pdfbox.util.PDFOperator;

public class SetStrokingGrayColor extends OperatorProcessor
{
    @Override
    public void process(final PDFOperator operator, final List<COSBase> arguments) throws IOException {
        final PDColorState color = this.context.getGraphicsState().getStrokingColor();
        color.setColorSpace(new PDDeviceGray());
        final float[] values = { 0.0f };
        if (arguments.size() >= 1) {
            values[0] = arguments.get(0).floatValue();
            color.setColorSpaceValue(values);
            return;
        }
        throw new IOException("Error: Expected at least one argument when setting non stroking gray color");
    }
}
