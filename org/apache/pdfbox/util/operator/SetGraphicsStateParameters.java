// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.util.operator;

import java.io.IOException;
import org.apache.pdfbox.pdmodel.graphics.PDExtendedGraphicsState;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.cos.COSBase;
import java.util.List;
import org.apache.pdfbox.util.PDFOperator;

public class SetGraphicsStateParameters extends OperatorProcessor
{
    @Override
    public void process(final PDFOperator operator, final List<COSBase> arguments) throws IOException {
        final COSName graphicsName = arguments.get(0);
        final PDExtendedGraphicsState gs = this.context.getGraphicsStates().get(graphicsName.getName());
        gs.copyIntoGraphicsState(this.context.getGraphicsState());
    }
}
