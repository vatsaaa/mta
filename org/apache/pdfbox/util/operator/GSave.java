// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.util.operator;

import org.apache.pdfbox.pdmodel.graphics.PDGraphicsState;
import org.apache.pdfbox.cos.COSBase;
import java.util.List;
import org.apache.pdfbox.util.PDFOperator;

public class GSave extends OperatorProcessor
{
    @Override
    public void process(final PDFOperator operator, final List<COSBase> arguments) {
        this.context.getGraphicsStack().push((PDGraphicsState)this.context.getGraphicsState().clone());
    }
}
