// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.util.operator;

import org.apache.pdfbox.cos.COSNumber;
import org.apache.pdfbox.cos.COSBase;
import java.util.List;
import org.apache.pdfbox.util.PDFOperator;

public class SetCharSpacing extends OperatorProcessor
{
    @Override
    public void process(final PDFOperator operator, final List<COSBase> arguments) {
        if (arguments.size() > 0) {
            final Object charSpacing = arguments.get(arguments.size() - 1);
            if (charSpacing instanceof COSNumber) {
                final COSNumber characterSpacing = (COSNumber)charSpacing;
                this.context.getGraphicsState().getTextState().setCharacterSpacing(characterSpacing.floatValue());
            }
        }
    }
}
