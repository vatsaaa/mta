// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.util.operator;

import java.io.IOException;
import org.apache.pdfbox.cos.COSString;
import org.apache.pdfbox.util.Matrix;
import org.apache.pdfbox.cos.COSNumber;
import org.apache.pdfbox.cos.COSArray;
import org.apache.pdfbox.cos.COSBase;
import java.util.List;
import org.apache.pdfbox.util.PDFOperator;

public class ShowTextGlyph extends OperatorProcessor
{
    @Override
    public void process(final PDFOperator operator, final List<COSBase> arguments) throws IOException {
        final COSArray array = arguments.get(0);
        final int arraySize = array.size();
        final float fontsize = this.context.getGraphicsState().getTextState().getFontSize();
        final float horizontalScaling = this.context.getGraphicsState().getTextState().getHorizontalScalingPercent() / 100.0f;
        for (int i = 0; i < arraySize; ++i) {
            final COSBase next = array.get(i);
            if (next instanceof COSNumber) {
                float adjustment = ((COSNumber)next).floatValue();
                final Matrix adjMatrix = new Matrix();
                adjustment = -(adjustment / 1000.0f) * horizontalScaling * fontsize;
                adjMatrix.setValue(2, 0, adjustment);
                this.context.setTextMatrix(adjMatrix.multiply(this.context.getTextMatrix(), adjMatrix));
            }
            else {
                if (!(next instanceof COSString)) {
                    throw new IOException("Unknown type in array for TJ operation:" + next);
                }
                this.context.processEncodedText(((COSString)next).getBytes());
            }
        }
    }
}
