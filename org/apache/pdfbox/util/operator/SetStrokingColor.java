// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.util.operator;

import org.apache.commons.logging.LogFactory;
import java.io.IOException;
import org.apache.pdfbox.pdmodel.graphics.color.PDColorSpace;
import org.apache.pdfbox.pdmodel.graphics.color.PDPattern;
import org.apache.pdfbox.pdmodel.graphics.color.PDDeviceN;
import org.apache.pdfbox.pdmodel.graphics.color.PDSeparation;
import org.apache.pdfbox.pdmodel.graphics.color.PDCalRGB;
import org.apache.pdfbox.pdmodel.graphics.color.PDICCBased;
import org.apache.pdfbox.pdmodel.graphics.color.PDDeviceCMYK;
import org.apache.pdfbox.pdmodel.graphics.color.PDDeviceRGB;
import org.apache.pdfbox.pdmodel.graphics.color.PDDeviceGray;
import org.apache.pdfbox.cos.COSBase;
import java.util.List;
import org.apache.pdfbox.util.PDFOperator;
import org.apache.commons.logging.Log;

public class SetStrokingColor extends OperatorProcessor
{
    private static final Log log;
    
    @Override
    public void process(final PDFOperator operator, final List<COSBase> arguments) throws IOException {
        final PDColorSpace colorSpace = this.context.getGraphicsState().getStrokingColor().getColorSpace();
        if (colorSpace != null) {
            OperatorProcessor newOperator = null;
            if (colorSpace instanceof PDDeviceGray) {
                newOperator = new SetStrokingGrayColor();
            }
            else if (colorSpace instanceof PDDeviceRGB) {
                newOperator = new SetStrokingRGBColor();
            }
            else if (colorSpace instanceof PDDeviceCMYK) {
                newOperator = new SetStrokingCMYKColor();
            }
            else if (colorSpace instanceof PDICCBased) {
                newOperator = new SetStrokingICCBasedColor();
            }
            else if (colorSpace instanceof PDCalRGB) {
                newOperator = new SetStrokingCalRGBColor();
            }
            else if (colorSpace instanceof PDSeparation) {
                newOperator = new SetStrokingSeparation();
            }
            else if (colorSpace instanceof PDDeviceN) {
                newOperator = new SetStrokingDeviceN();
            }
            else if (colorSpace instanceof PDPattern) {
                newOperator = new SetStrokingPattern();
            }
            if (newOperator != null) {
                newOperator.setContext(this.getContext());
                newOperator.process(operator, arguments);
            }
            else {
                SetStrokingColor.log.info("Not supported colorspace " + colorSpace.getName() + " within operator " + operator.getOperation());
            }
        }
        else {
            SetStrokingColor.log.warn("Colorspace not found in " + this.getClass().getName() + ".process!!");
        }
    }
    
    static {
        log = LogFactory.getLog(SetStrokingColor.class);
    }
}
