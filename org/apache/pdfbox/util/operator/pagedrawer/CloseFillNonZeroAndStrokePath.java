// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.util.operator.pagedrawer;

import java.io.IOException;
import org.apache.pdfbox.cos.COSBase;
import java.util.List;
import org.apache.pdfbox.util.PDFOperator;
import org.apache.pdfbox.util.operator.OperatorProcessor;

public class CloseFillNonZeroAndStrokePath extends OperatorProcessor
{
    @Override
    public void process(final PDFOperator operator, final List<COSBase> arguments) throws IOException {
        this.context.processOperator("h", arguments);
        this.context.processOperator("B", arguments);
    }
}
