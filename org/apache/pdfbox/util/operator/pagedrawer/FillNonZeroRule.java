// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.util.operator.pagedrawer;

import org.apache.commons.logging.LogFactory;
import java.io.IOException;
import org.apache.pdfbox.pdfviewer.PageDrawer;
import org.apache.pdfbox.cos.COSBase;
import java.util.List;
import org.apache.pdfbox.util.PDFOperator;
import org.apache.commons.logging.Log;
import org.apache.pdfbox.util.operator.OperatorProcessor;

public class FillNonZeroRule extends OperatorProcessor
{
    private static final Log log;
    
    @Override
    public void process(final PDFOperator operator, final List<COSBase> arguments) throws IOException {
        try {
            final PageDrawer drawer = (PageDrawer)this.context;
            drawer.fillPath(1);
        }
        catch (Exception e) {
            FillNonZeroRule.log.warn(e, e);
        }
    }
    
    static {
        log = LogFactory.getLog(FillNonZeroRule.class);
    }
}
