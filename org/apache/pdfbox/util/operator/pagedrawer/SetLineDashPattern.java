// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.util.operator.pagedrawer;

import java.io.IOException;
import org.apache.pdfbox.pdmodel.graphics.PDLineDashPattern;
import java.awt.BasicStroke;
import org.apache.pdfbox.pdfviewer.PageDrawer;
import org.apache.pdfbox.cos.COSBase;
import java.util.List;
import org.apache.pdfbox.util.PDFOperator;

public class SetLineDashPattern extends org.apache.pdfbox.util.operator.SetLineDashPattern
{
    @Override
    public void process(final PDFOperator operator, final List<COSBase> arguments) throws IOException {
        super.process(operator, arguments);
        final PDLineDashPattern lineDashPattern = this.context.getGraphicsState().getLineDashPattern();
        final PageDrawer drawer = (PageDrawer)this.context;
        final BasicStroke stroke = drawer.getStroke();
        if (stroke == null) {
            if (lineDashPattern.isDashPatternEmpty()) {
                drawer.setStroke(new BasicStroke(1.0f, 2, 0, 10.0f));
            }
            else {
                drawer.setStroke(new BasicStroke(1.0f, 2, 0, 10.0f, lineDashPattern.getCOSDashPattern().toFloatArray(), (float)lineDashPattern.getPhaseStart()));
            }
        }
        else if (lineDashPattern.isDashPatternEmpty()) {
            drawer.setStroke(new BasicStroke(stroke.getLineWidth(), stroke.getEndCap(), stroke.getLineJoin(), stroke.getMiterLimit()));
        }
        else {
            drawer.setStroke(new BasicStroke(stroke.getLineWidth(), stroke.getEndCap(), stroke.getLineJoin(), stroke.getMiterLimit(), lineDashPattern.getCOSDashPattern().toFloatArray(), (float)lineDashPattern.getPhaseStart()));
        }
    }
}
