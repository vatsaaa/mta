// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.util.operator.pagedrawer;

import org.apache.commons.logging.LogFactory;
import java.io.IOException;
import java.awt.image.BufferedImage;
import org.apache.pdfbox.util.ImageParameters;
import org.apache.pdfbox.pdmodel.PDPage;
import java.awt.Image;
import java.awt.geom.AffineTransform;
import org.apache.pdfbox.util.Matrix;
import java.util.Map;
import org.apache.pdfbox.pdmodel.graphics.xobject.PDInlinedImage;
import org.apache.pdfbox.pdfviewer.PageDrawer;
import org.apache.pdfbox.cos.COSBase;
import java.util.List;
import org.apache.pdfbox.util.PDFOperator;
import org.apache.commons.logging.Log;
import org.apache.pdfbox.util.operator.OperatorProcessor;

public class BeginInlineImage extends OperatorProcessor
{
    private static final Log log;
    
    @Override
    public void process(final PDFOperator operator, final List<COSBase> arguments) throws IOException {
        final PageDrawer drawer = (PageDrawer)this.context;
        final PDPage page = drawer.getPage();
        final ImageParameters params = operator.getImageParameters();
        final PDInlinedImage image = new PDInlinedImage();
        image.setImageParameters(params);
        image.setImageData(operator.getImageData());
        final BufferedImage awtImage = image.createImage(this.context.getColorSpaces());
        if (awtImage == null) {
            BeginInlineImage.log.warn("BeginInlineImage.process(): createImage returned NULL");
            return;
        }
        final int imageWidth = awtImage.getWidth();
        final int imageHeight = awtImage.getHeight();
        final double pageHeight = drawer.getPageSize().getHeight();
        final Matrix ctm = drawer.getGraphicsState().getCurrentTransformationMatrix();
        final int pageRotation = page.findRotation();
        final AffineTransform ctmAT = ctm.createAffineTransform();
        ctmAT.scale(1.0f / imageWidth, 1.0f / imageHeight);
        Matrix rotationMatrix = new Matrix();
        rotationMatrix.setFromAffineTransform(ctmAT);
        final double angle = Math.atan(ctmAT.getShearX() / ctmAT.getScaleX());
        Matrix translationMatrix = null;
        if (pageRotation == 0 || pageRotation == 180) {
            translationMatrix = Matrix.getTranslatingInstance((float)(Math.sin(angle) * ctm.getXScale()), (float)(pageHeight - 2.0f * ctm.getYPosition() - Math.cos(angle) * ctm.getYScale()));
        }
        else if (pageRotation == 90 || pageRotation == 270) {
            translationMatrix = Matrix.getTranslatingInstance((float)(Math.sin(angle) * ctm.getYScale()), (float)(pageHeight - 2.0f * ctm.getYPosition()));
        }
        rotationMatrix = rotationMatrix.multiply(translationMatrix);
        rotationMatrix.setValue(0, 1, -1.0f * rotationMatrix.getValue(0, 1));
        rotationMatrix.setValue(1, 0, -1.0f * rotationMatrix.getValue(1, 0));
        final AffineTransform at = new AffineTransform(rotationMatrix.getValue(0, 0), rotationMatrix.getValue(0, 1), rotationMatrix.getValue(1, 0), rotationMatrix.getValue(1, 1), rotationMatrix.getValue(2, 0), rotationMatrix.getValue(2, 1));
        drawer.drawImage(awtImage, at);
    }
    
    static {
        log = LogFactory.getLog(BeginInlineImage.class);
    }
}
