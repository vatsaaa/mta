// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.util.operator.pagedrawer;

import org.apache.commons.logging.LogFactory;
import java.io.IOException;
import org.apache.pdfbox.util.Matrix;
import java.awt.BasicStroke;
import org.apache.pdfbox.pdfviewer.PageDrawer;
import org.apache.pdfbox.cos.COSBase;
import java.util.List;
import org.apache.pdfbox.util.PDFOperator;
import org.apache.commons.logging.Log;
import org.apache.pdfbox.util.operator.OperatorProcessor;

public class StrokePath extends OperatorProcessor
{
    private static final Log log;
    
    @Override
    public void process(final PDFOperator operator, final List<COSBase> arguments) throws IOException {
        try {
            final PageDrawer drawer = (PageDrawer)this.context;
            float lineWidth = (float)this.context.getGraphicsState().getLineWidth();
            final Matrix ctm = this.context.getGraphicsState().getCurrentTransformationMatrix();
            if (ctm != null && ctm.getXScale() > 0.0f) {
                lineWidth *= ctm.getXScale();
            }
            final BasicStroke stroke = drawer.getStroke();
            if (stroke == null) {
                drawer.setStroke(new BasicStroke(lineWidth));
            }
            else {
                drawer.setStroke(new BasicStroke(lineWidth, stroke.getEndCap(), stroke.getLineJoin(), stroke.getMiterLimit(), stroke.getDashArray(), stroke.getDashPhase()));
            }
            drawer.strokePath();
        }
        catch (Exception exception) {
            StrokePath.log.warn(exception, exception);
        }
    }
    
    static {
        log = LogFactory.getLog(StrokePath.class);
    }
}
