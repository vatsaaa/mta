// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.util.operator.pagedrawer;

import java.io.IOException;
import java.awt.geom.GeneralPath;
import org.apache.pdfbox.pdfviewer.PageDrawer;
import org.apache.pdfbox.cos.COSBase;
import java.util.List;
import org.apache.pdfbox.util.PDFOperator;
import org.apache.pdfbox.util.operator.OperatorProcessor;

public class FillNonZeroAndStrokePath extends OperatorProcessor
{
    @Override
    public void process(final PDFOperator operator, final List<COSBase> arguments) throws IOException {
        final PageDrawer drawer = (PageDrawer)this.context;
        final GeneralPath currentPath = (GeneralPath)drawer.getLinePath().clone();
        this.context.processOperator("f", arguments);
        drawer.setLinePath(currentPath);
        this.context.processOperator("S", arguments);
    }
}
