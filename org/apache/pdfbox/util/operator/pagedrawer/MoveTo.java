// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.util.operator.pagedrawer;

import org.apache.commons.logging.LogFactory;
import java.io.IOException;
import java.awt.geom.Point2D;
import org.apache.pdfbox.cos.COSNumber;
import org.apache.pdfbox.pdfviewer.PageDrawer;
import org.apache.pdfbox.cos.COSBase;
import java.util.List;
import org.apache.pdfbox.util.PDFOperator;
import org.apache.commons.logging.Log;
import org.apache.pdfbox.util.operator.OperatorProcessor;

public class MoveTo extends OperatorProcessor
{
    private static final Log log;
    
    @Override
    public void process(final PDFOperator operator, final List<COSBase> arguments) throws IOException {
        try {
            final PageDrawer drawer = (PageDrawer)this.context;
            final COSNumber x = arguments.get(0);
            final COSNumber y = arguments.get(1);
            final Point2D pos = drawer.transformedPoint(x.doubleValue(), y.doubleValue());
            drawer.getLinePath().moveTo((float)pos.getX(), (float)pos.getY());
        }
        catch (Exception exception) {
            MoveTo.log.warn(exception, exception);
        }
    }
    
    static {
        log = LogFactory.getLog(MoveTo.class);
    }
}
