// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.util.operator.pagedrawer;

import java.io.IOException;
import java.awt.BasicStroke;
import org.apache.pdfbox.pdfviewer.PageDrawer;
import org.apache.pdfbox.cos.COSBase;
import java.util.List;
import org.apache.pdfbox.util.PDFOperator;

public class SetLineCapStyle extends org.apache.pdfbox.util.operator.SetLineCapStyle
{
    @Override
    public void process(final PDFOperator operator, final List<COSBase> arguments) throws IOException {
        super.process(operator, arguments);
        final int lineCapStyle = this.context.getGraphicsState().getLineCap();
        final PageDrawer drawer = (PageDrawer)this.context;
        final BasicStroke stroke = drawer.getStroke();
        if (stroke == null) {
            drawer.setStroke(new BasicStroke(1.0f, lineCapStyle, 0));
        }
        else {
            drawer.setStroke(new BasicStroke(stroke.getLineWidth(), lineCapStyle, stroke.getLineJoin(), stroke.getMiterLimit(), stroke.getDashArray(), stroke.getDashPhase()));
        }
    }
}
