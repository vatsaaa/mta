// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.util.operator.pagedrawer;

import java.awt.geom.Point2D;
import org.apache.pdfbox.cos.COSNumber;
import org.apache.pdfbox.pdfviewer.PageDrawer;
import org.apache.pdfbox.cos.COSBase;
import java.util.List;
import org.apache.pdfbox.util.PDFOperator;
import org.apache.pdfbox.util.operator.OperatorProcessor;

public class CurveTo extends OperatorProcessor
{
    @Override
    public void process(final PDFOperator operator, final List<COSBase> arguments) {
        final PageDrawer drawer = (PageDrawer)this.context;
        final COSNumber x1 = arguments.get(0);
        final COSNumber y1 = arguments.get(1);
        final COSNumber x2 = arguments.get(2);
        final COSNumber y2 = arguments.get(3);
        final COSNumber x3 = arguments.get(4);
        final COSNumber y3 = arguments.get(5);
        final Point2D point1 = drawer.transformedPoint(x1.doubleValue(), y1.doubleValue());
        final Point2D point2 = drawer.transformedPoint(x2.doubleValue(), y2.doubleValue());
        final Point2D point3 = drawer.transformedPoint(x3.doubleValue(), y3.doubleValue());
        drawer.getLinePath().curveTo((float)point1.getX(), (float)point1.getY(), (float)point2.getX(), (float)point2.getY(), (float)point3.getX(), (float)point3.getY());
    }
}
