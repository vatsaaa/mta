// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.util.operator.pagedrawer;

import java.awt.geom.GeneralPath;
import java.awt.geom.Point2D;
import org.apache.pdfbox.cos.COSNumber;
import org.apache.pdfbox.pdfviewer.PageDrawer;
import org.apache.pdfbox.cos.COSBase;
import java.util.List;
import org.apache.pdfbox.util.PDFOperator;
import org.apache.pdfbox.util.operator.OperatorProcessor;

public class AppendRectangleToPath extends OperatorProcessor
{
    @Override
    public void process(final PDFOperator operator, final List<COSBase> arguments) {
        final PageDrawer drawer = (PageDrawer)this.context;
        final COSNumber x = arguments.get(0);
        final COSNumber y = arguments.get(1);
        final COSNumber w = arguments.get(2);
        final COSNumber h = arguments.get(3);
        final double x2 = x.doubleValue();
        final double y2 = y.doubleValue();
        final double x3 = w.doubleValue() + x2;
        final double y3 = h.doubleValue() + y2;
        final Point2D startCoords = drawer.transformedPoint(x2, y2);
        final Point2D endCoords = drawer.transformedPoint(x3, y3);
        final float width = (float)(endCoords.getX() - startCoords.getX());
        final float height = (float)(endCoords.getY() - startCoords.getY());
        final float xStart = (float)startCoords.getX();
        final float yStart = (float)startCoords.getY();
        final GeneralPath path = drawer.getLinePath();
        path.moveTo(xStart, yStart);
        path.lineTo(xStart + width, yStart);
        path.lineTo(xStart + width, yStart + height);
        path.lineTo(xStart, yStart + height);
        path.lineTo(xStart, yStart);
    }
}
