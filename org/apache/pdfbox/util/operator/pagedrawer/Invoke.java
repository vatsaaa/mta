// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.util.operator.pagedrawer;

import org.apache.commons.logging.LogFactory;
import java.io.IOException;
import org.apache.pdfbox.pdmodel.PDResources;
import java.awt.geom.AffineTransform;
import org.apache.pdfbox.util.Matrix;
import java.awt.image.BufferedImage;
import java.util.Map;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.cos.COSStream;
import org.apache.pdfbox.pdmodel.graphics.PDGraphicsState;
import org.apache.pdfbox.pdmodel.graphics.xobject.PDXObjectForm;
import java.awt.Image;
import org.apache.pdfbox.pdmodel.graphics.xobject.PDXObjectImage;
import org.apache.pdfbox.pdmodel.graphics.xobject.PDXObject;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.pdfviewer.PageDrawer;
import org.apache.pdfbox.cos.COSBase;
import java.util.List;
import org.apache.pdfbox.util.PDFOperator;
import org.apache.commons.logging.Log;
import org.apache.pdfbox.util.operator.OperatorProcessor;

public class Invoke extends OperatorProcessor
{
    private static final Log LOG;
    
    @Override
    public void process(final PDFOperator operator, final List<COSBase> arguments) throws IOException {
        final PageDrawer drawer = (PageDrawer)this.context;
        final PDPage page = drawer.getPage();
        final COSName objectName = arguments.get(0);
        final Map<String, PDXObject> xobjects = drawer.getResources().getXObjects();
        final PDXObject xobject = xobjects.get(objectName.getName());
        if (xobject == null) {
            Invoke.LOG.warn("Can't find the XObject for '" + objectName.getName() + "'");
        }
        else if (xobject instanceof PDXObjectImage) {
            final PDXObjectImage image = (PDXObjectImage)xobject;
            try {
                if (image.getImageMask()) {
                    image.setStencilColor(drawer.getGraphicsState().getNonStrokingColor());
                }
                final BufferedImage awtImage = image.getRGBImage();
                if (awtImage == null) {
                    Invoke.LOG.warn("getRGBImage returned NULL");
                    return;
                }
                final int imageWidth = awtImage.getWidth();
                final int imageHeight = awtImage.getHeight();
                final double pageHeight = drawer.getPageSize().getHeight();
                Invoke.LOG.debug("imageWidth: " + imageWidth + "\t\timageHeight: " + imageHeight);
                final Matrix ctm = drawer.getGraphicsState().getCurrentTransformationMatrix();
                final float yScaling = ctm.getYScale();
                float angle = (float)Math.acos(ctm.getValue(0, 0) / ctm.getXScale());
                if (ctm.getValue(0, 1) < 0.0f && ctm.getValue(1, 0) > 0.0f) {
                    angle *= -1.0f;
                }
                ctm.setValue(2, 1, (float)(pageHeight - ctm.getYPosition() - Math.cos(angle) * yScaling));
                ctm.setValue(2, 0, (float)(ctm.getXPosition() - Math.sin(angle) * yScaling));
                ctm.setValue(0, 1, -1.0f * ctm.getValue(0, 1));
                ctm.setValue(1, 0, -1.0f * ctm.getValue(1, 0));
                final AffineTransform ctmAT = ctm.createAffineTransform();
                ctmAT.scale(1.0f / imageWidth, 1.0f / imageHeight);
                drawer.drawImage(awtImage, ctmAT);
            }
            catch (Exception e) {
                e.printStackTrace();
                Invoke.LOG.error(e, e);
            }
        }
        else if (xobject instanceof PDXObjectForm) {
            this.context.getGraphicsStack().push((PDGraphicsState)this.context.getGraphicsState().clone());
            final PDXObjectForm form = (PDXObjectForm)xobject;
            final COSStream invoke = (COSStream)form.getCOSObject();
            PDResources pdResources = form.getResources();
            if (pdResources == null) {
                pdResources = page.findResources();
            }
            final Matrix matrix = form.getMatrix();
            if (matrix != null) {
                final Matrix xobjectCTM = matrix.multiply(this.context.getGraphicsState().getCurrentTransformationMatrix());
                this.context.getGraphicsState().setCurrentTransformationMatrix(xobjectCTM);
            }
            this.getContext().processSubStream(page, pdResources, invoke);
            this.context.setGraphicsState(this.context.getGraphicsStack().pop());
        }
    }
    
    static {
        LOG = LogFactory.getLog(Invoke.class);
    }
}
