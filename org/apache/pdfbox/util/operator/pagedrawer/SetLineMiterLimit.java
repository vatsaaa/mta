// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.util.operator.pagedrawer;

import java.io.IOException;
import java.awt.BasicStroke;
import org.apache.pdfbox.pdfviewer.PageDrawer;
import org.apache.pdfbox.cos.COSBase;
import java.util.List;
import org.apache.pdfbox.util.PDFOperator;

public class SetLineMiterLimit extends org.apache.pdfbox.util.operator.SetLineMiterLimit
{
    @Override
    public void process(final PDFOperator operator, final List<COSBase> arguments) throws IOException {
        super.process(operator, arguments);
        final float miterLimit = (float)this.context.getGraphicsState().getMiterLimit();
        final PageDrawer drawer = (PageDrawer)this.context;
        final BasicStroke stroke = drawer.getStroke();
        if (stroke == null) {
            drawer.setStroke(new BasicStroke(1.0f, 2, 0, miterLimit, null, 0.0f));
        }
        else {
            drawer.setStroke(new BasicStroke(stroke.getLineWidth(), stroke.getEndCap(), stroke.getLineJoin(), miterLimit, null, 0.0f));
        }
    }
}
