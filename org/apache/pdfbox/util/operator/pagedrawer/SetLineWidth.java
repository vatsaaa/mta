// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.util.operator.pagedrawer;

import java.io.IOException;
import java.awt.BasicStroke;
import org.apache.pdfbox.pdfviewer.PageDrawer;
import org.apache.pdfbox.cos.COSBase;
import java.util.List;
import org.apache.pdfbox.util.PDFOperator;

public class SetLineWidth extends org.apache.pdfbox.util.operator.SetLineWidth
{
    @Override
    public void process(final PDFOperator operator, final List<COSBase> arguments) throws IOException {
        super.process(operator, arguments);
        float lineWidth = (float)this.context.getGraphicsState().getLineWidth();
        if (lineWidth == 0.0f) {
            lineWidth = 1.0f;
        }
        final PageDrawer drawer = (PageDrawer)this.context;
        final BasicStroke stroke = drawer.getStroke();
        if (stroke == null) {
            drawer.setStroke(new BasicStroke(lineWidth));
        }
        else {
            drawer.setStroke(new BasicStroke(lineWidth, stroke.getEndCap(), stroke.getLineJoin(), stroke.getMiterLimit(), stroke.getDashArray(), stroke.getDashPhase()));
        }
    }
}
