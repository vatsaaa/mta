// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.util.operator;

import java.io.IOException;
import org.apache.pdfbox.cos.COSFloat;
import java.util.ArrayList;
import org.apache.pdfbox.cos.COSBase;
import java.util.List;
import org.apache.pdfbox.util.PDFOperator;

public class NextLine extends OperatorProcessor
{
    @Override
    public void process(final PDFOperator operator, final List<COSBase> arguments) throws IOException {
        final ArrayList<COSBase> args = new ArrayList<COSBase>();
        args.add(new COSFloat(0.0f));
        args.add(new COSFloat(-1.0f * this.context.getGraphicsState().getTextState().getLeading()));
        this.context.processOperator("Td", args);
    }
}
