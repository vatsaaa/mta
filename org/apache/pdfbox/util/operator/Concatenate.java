// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.util.operator;

import java.io.IOException;
import org.apache.pdfbox.util.Matrix;
import org.apache.pdfbox.cos.COSNumber;
import org.apache.pdfbox.cos.COSBase;
import java.util.List;
import org.apache.pdfbox.util.PDFOperator;

public class Concatenate extends OperatorProcessor
{
    @Override
    public void process(final PDFOperator operator, final List<COSBase> arguments) throws IOException {
        final COSNumber a = arguments.get(0);
        final COSNumber b = arguments.get(1);
        final COSNumber c = arguments.get(2);
        final COSNumber d = arguments.get(3);
        final COSNumber e = arguments.get(4);
        final COSNumber f = arguments.get(5);
        final Matrix newMatrix = new Matrix();
        newMatrix.setValue(0, 0, a.floatValue());
        newMatrix.setValue(0, 1, b.floatValue());
        newMatrix.setValue(1, 0, c.floatValue());
        newMatrix.setValue(1, 1, d.floatValue());
        newMatrix.setValue(2, 0, e.floatValue());
        newMatrix.setValue(2, 1, f.floatValue());
        this.context.getGraphicsState().setCurrentTransformationMatrix(newMatrix.multiply(this.context.getGraphicsState().getCurrentTransformationMatrix()));
    }
}
