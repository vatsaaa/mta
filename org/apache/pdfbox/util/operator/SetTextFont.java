// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.util.operator;

import java.io.IOException;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.cos.COSNumber;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.cos.COSBase;
import java.util.List;
import org.apache.pdfbox.util.PDFOperator;

public class SetTextFont extends OperatorProcessor
{
    @Override
    public void process(final PDFOperator operator, final List<COSBase> arguments) throws IOException {
        if (arguments.size() >= 2) {
            final COSName fontName = arguments.get(0);
            final float fontSize = arguments.get(1).floatValue();
            this.context.getGraphicsState().getTextState().setFontSize(fontSize);
            this.context.getGraphicsState().getTextState().setFont(this.context.getFonts().get(fontName.getName()));
            if (this.context.getGraphicsState().getTextState().getFont() == null) {
                throw new IOException("Error: Could not find font(" + fontName + ") in map=" + this.context.getFonts());
            }
        }
    }
}
