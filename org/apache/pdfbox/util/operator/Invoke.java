// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.util.operator;

import java.io.IOException;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDResources;
import java.util.Map;
import org.apache.pdfbox.cos.COSStream;
import org.apache.pdfbox.pdmodel.graphics.xobject.PDXObjectForm;
import org.apache.pdfbox.util.PDFMarkedContentExtractor;
import org.apache.pdfbox.pdmodel.graphics.xobject.PDXObject;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.cos.COSBase;
import java.util.List;
import org.apache.pdfbox.util.PDFOperator;

public class Invoke extends OperatorProcessor
{
    @Override
    public void process(final PDFOperator operator, final List<COSBase> arguments) throws IOException {
        final COSName name = arguments.get(0);
        final Map xobjects = this.context.getXObjects();
        final PDXObject xobject = xobjects.get(name.getName());
        if (this.context instanceof PDFMarkedContentExtractor) {
            ((PDFMarkedContentExtractor)this.context).xobject(xobject);
        }
        if (xobject instanceof PDXObjectForm) {
            final PDXObjectForm form = (PDXObjectForm)xobject;
            final COSStream invoke = (COSStream)form.getCOSObject();
            PDResources pdResources = form.getResources();
            final PDPage page = this.context.getCurrentPage();
            if (pdResources == null) {
                pdResources = page.findResources();
            }
            this.getContext().processSubStream(page, pdResources, invoke);
        }
    }
}
