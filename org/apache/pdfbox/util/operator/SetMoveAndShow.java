// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.util.operator;

import java.io.IOException;
import org.apache.pdfbox.cos.COSBase;
import java.util.List;
import org.apache.pdfbox.util.PDFOperator;

public class SetMoveAndShow extends OperatorProcessor
{
    @Override
    public void process(final PDFOperator operator, final List<COSBase> arguments) throws IOException {
        this.context.processOperator("Tw", arguments.subList(0, 1));
        this.context.processOperator("Tc", arguments.subList(1, 2));
        this.context.processOperator("'", arguments.subList(2, 3));
    }
}
