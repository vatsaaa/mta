// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.util.operator;

import java.io.IOException;
import org.apache.pdfbox.cos.COSBase;
import java.util.List;
import org.apache.pdfbox.util.PDFOperator;
import org.apache.pdfbox.util.PDFStreamEngine;

public abstract class OperatorProcessor
{
    protected PDFStreamEngine context;
    
    protected OperatorProcessor() {
        this.context = null;
    }
    
    protected PDFStreamEngine getContext() {
        return this.context;
    }
    
    public void setContext(final PDFStreamEngine ctx) {
        this.context = ctx;
    }
    
    public abstract void process(final PDFOperator p0, final List<COSBase> p1) throws IOException;
}
