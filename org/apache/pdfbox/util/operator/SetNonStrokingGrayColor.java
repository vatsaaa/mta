// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.util.operator;

import org.apache.pdfbox.pdmodel.graphics.color.PDColorState;
import org.apache.pdfbox.pdmodel.graphics.color.PDColorSpace;
import java.io.IOException;
import org.apache.pdfbox.cos.COSNumber;
import org.apache.pdfbox.pdmodel.graphics.color.PDDeviceGray;
import org.apache.pdfbox.cos.COSBase;
import java.util.List;
import org.apache.pdfbox.util.PDFOperator;

public class SetNonStrokingGrayColor extends OperatorProcessor
{
    @Override
    public void process(final PDFOperator operator, final List<COSBase> arguments) throws IOException {
        final PDColorSpace cs = new PDDeviceGray();
        final PDColorState colorInstance = this.context.getGraphicsState().getNonStrokingColor();
        colorInstance.setColorSpace(cs);
        final float[] values = { 0.0f };
        if (arguments.size() >= 1) {
            values[0] = arguments.get(0).floatValue();
            colorInstance.setColorSpaceValue(values);
            return;
        }
        throw new IOException("Error: Expected at least one argument when setting non stroking gray color");
    }
}
