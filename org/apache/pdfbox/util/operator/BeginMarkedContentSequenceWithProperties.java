// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.util.operator;

import java.io.IOException;
import java.util.Iterator;
import org.apache.pdfbox.util.PDFMarkedContentExtractor;
import org.apache.pdfbox.cos.COSDictionary;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.cos.COSBase;
import java.util.List;
import org.apache.pdfbox.util.PDFOperator;

public class BeginMarkedContentSequenceWithProperties extends OperatorProcessor
{
    @Override
    public void process(final PDFOperator operator, final List<COSBase> arguments) throws IOException {
        COSName tag = null;
        COSDictionary properties = null;
        for (final COSBase argument : arguments) {
            if (argument instanceof COSName) {
                tag = (COSName)argument;
            }
            else {
                if (!(argument instanceof COSDictionary)) {
                    continue;
                }
                properties = (COSDictionary)argument;
            }
        }
        if (this.context instanceof PDFMarkedContentExtractor) {
            ((PDFMarkedContentExtractor)this.context).beginMarkedContentSequence(tag, properties);
        }
    }
}
