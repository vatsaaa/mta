// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.util.operator;

import org.apache.pdfbox.util.Matrix;
import org.apache.pdfbox.cos.COSNumber;
import org.apache.pdfbox.cos.COSBase;
import java.util.List;
import org.apache.pdfbox.util.PDFOperator;

public class SetMatrix extends OperatorProcessor
{
    @Override
    public void process(final PDFOperator operator, final List<COSBase> arguments) {
        final COSNumber a = arguments.get(0);
        final COSNumber b = arguments.get(1);
        final COSNumber c = arguments.get(2);
        final COSNumber d = arguments.get(3);
        final COSNumber e = arguments.get(4);
        final COSNumber f = arguments.get(5);
        final Matrix textMatrix = new Matrix();
        textMatrix.setValue(0, 0, a.floatValue());
        textMatrix.setValue(0, 1, b.floatValue());
        textMatrix.setValue(1, 0, c.floatValue());
        textMatrix.setValue(1, 1, d.floatValue());
        textMatrix.setValue(2, 0, e.floatValue());
        textMatrix.setValue(2, 1, f.floatValue());
        this.context.setTextMatrix(textMatrix);
        this.context.setTextLineMatrix(textMatrix.copy());
    }
}
