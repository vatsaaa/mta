// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.util.operator;

import java.io.IOException;
import org.apache.pdfbox.pdmodel.graphics.color.PDColorSpace;
import org.apache.pdfbox.pdmodel.graphics.color.PDColorState;
import org.apache.pdfbox.cos.COSNumber;
import org.apache.pdfbox.cos.COSBase;
import java.util.List;
import org.apache.pdfbox.util.PDFOperator;

public class SetStrokingICCBasedColor extends OperatorProcessor
{
    @Override
    public void process(final PDFOperator operator, final List<COSBase> arguments) throws IOException {
        final PDColorState color = this.context.getGraphicsState().getStrokingColor();
        final PDColorSpace cs = color.getColorSpace();
        final int numberOfComponents = cs.getNumberOfComponents();
        final float[] values = new float[numberOfComponents];
        for (int i = 0; i < numberOfComponents; ++i) {
            values[i] = arguments.get(i).floatValue();
        }
        color.setColorSpaceValue(values);
    }
}
