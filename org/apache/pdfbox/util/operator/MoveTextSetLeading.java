// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.util.operator;

import java.io.IOException;
import org.apache.pdfbox.cos.COSFloat;
import java.util.ArrayList;
import org.apache.pdfbox.cos.COSNumber;
import org.apache.pdfbox.cos.COSBase;
import java.util.List;
import org.apache.pdfbox.util.PDFOperator;

public class MoveTextSetLeading extends OperatorProcessor
{
    @Override
    public void process(final PDFOperator operator, final List<COSBase> arguments) throws IOException {
        final COSNumber y = arguments.get(1);
        final ArrayList<COSBase> args = new ArrayList<COSBase>();
        args.add(new COSFloat(-1.0f * y.floatValue()));
        this.context.processOperator("TL", args);
        this.context.processOperator("Td", arguments);
    }
}
