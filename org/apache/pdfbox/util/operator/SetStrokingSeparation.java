// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.util.operator;

import java.io.IOException;
import org.apache.pdfbox.cos.COSArray;
import org.apache.pdfbox.pdmodel.graphics.color.PDColorSpace;
import org.apache.pdfbox.pdmodel.graphics.color.PDColorState;
import org.apache.pdfbox.pdmodel.graphics.color.PDSeparation;
import org.apache.pdfbox.cos.COSBase;
import java.util.List;
import org.apache.pdfbox.util.PDFOperator;

public class SetStrokingSeparation extends OperatorProcessor
{
    @Override
    public void process(final PDFOperator operator, final List<COSBase> arguments) throws IOException {
        final PDColorState colorInstance = this.context.getGraphicsState().getStrokingColor();
        PDColorSpace colorSpace = colorInstance.getColorSpace();
        if (colorSpace != null) {
            final PDSeparation sep = (PDSeparation)colorSpace;
            colorSpace = sep.getAlternateColorSpace();
            final COSArray values = sep.calculateColorValues(arguments.get(0));
            colorInstance.setColorSpaceValue(values.toFloatArray());
        }
    }
}
