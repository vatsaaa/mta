// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.util.operator;

import java.io.IOException;
import org.apache.pdfbox.pdmodel.graphics.PDLineDashPattern;
import org.apache.pdfbox.cos.COSNumber;
import org.apache.pdfbox.cos.COSArray;
import org.apache.pdfbox.cos.COSBase;
import java.util.List;
import org.apache.pdfbox.util.PDFOperator;

public class SetLineDashPattern extends OperatorProcessor
{
    @Override
    public void process(final PDFOperator operator, final List<COSBase> arguments) throws IOException {
        final COSArray dashArray = arguments.get(0);
        final int dashPhase = arguments.get(1).intValue();
        final PDLineDashPattern lineDash = new PDLineDashPattern(dashArray, dashPhase);
        this.context.getGraphicsState().setLineDashPattern(lineDash);
    }
}
