// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.util.operator;

import java.io.IOException;
import org.apache.pdfbox.cos.COSNumber;
import org.apache.pdfbox.cos.COSBase;
import java.util.List;
import org.apache.pdfbox.util.PDFOperator;

public class SetLineJoinStyle extends OperatorProcessor
{
    @Override
    public void process(final PDFOperator operator, final List<COSBase> arguments) throws IOException {
        final int lineJoinStyle = arguments.get(0).intValue();
        this.context.getGraphicsState().setLineJoin(lineJoinStyle);
    }
}
