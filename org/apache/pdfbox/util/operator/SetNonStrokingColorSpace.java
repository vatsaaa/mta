// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.util.operator;

import java.io.IOException;
import org.apache.pdfbox.pdmodel.graphics.color.PDColorState;
import org.apache.pdfbox.pdmodel.graphics.color.PDColorSpace;
import org.apache.pdfbox.pdmodel.graphics.color.PDDeviceCMYK;
import org.apache.pdfbox.pdmodel.graphics.color.PDColorSpaceFactory;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.cos.COSBase;
import java.util.List;
import org.apache.pdfbox.util.PDFOperator;

public class SetNonStrokingColorSpace extends OperatorProcessor
{
    private static final float[] EMPTY_FLOAT_ARRAY;
    
    @Override
    public void process(final PDFOperator operator, final List<COSBase> arguments) throws IOException {
        final COSName name = arguments.get(0);
        final PDColorSpace cs = PDColorSpaceFactory.createColorSpace(name, this.context.getColorSpaces(), this.context.getResources().getPatterns());
        final PDColorState colorInstance = this.context.getGraphicsState().getNonStrokingColor();
        colorInstance.setColorSpace(cs);
        final int numComponents = cs.getNumberOfComponents();
        float[] values = SetNonStrokingColorSpace.EMPTY_FLOAT_ARRAY;
        if (numComponents >= 0) {
            values = new float[numComponents];
            for (int i = 0; i < numComponents; ++i) {
                values[i] = 0.0f;
            }
            if (cs instanceof PDDeviceCMYK) {
                values[3] = 1.0f;
            }
        }
        colorInstance.setColorSpaceValue(values);
    }
    
    static {
        EMPTY_FLOAT_ARRAY = new float[0];
    }
}
