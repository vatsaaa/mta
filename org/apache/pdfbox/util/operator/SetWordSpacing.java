// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.util.operator;

import org.apache.pdfbox.cos.COSNumber;
import org.apache.pdfbox.cos.COSBase;
import java.util.List;
import org.apache.pdfbox.util.PDFOperator;

public class SetWordSpacing extends OperatorProcessor
{
    @Override
    public void process(final PDFOperator operator, final List<COSBase> arguments) {
        final COSNumber wordSpacing = arguments.get(0);
        this.context.getGraphicsState().getTextState().setWordSpacing(wordSpacing.floatValue());
    }
}
