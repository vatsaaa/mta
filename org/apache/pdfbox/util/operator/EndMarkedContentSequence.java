// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.util.operator;

import java.io.IOException;
import org.apache.pdfbox.util.PDFMarkedContentExtractor;
import org.apache.pdfbox.cos.COSBase;
import java.util.List;
import org.apache.pdfbox.util.PDFOperator;

public class EndMarkedContentSequence extends OperatorProcessor
{
    @Override
    public void process(final PDFOperator operator, final List<COSBase> arguments) throws IOException {
        if (this.context instanceof PDFMarkedContentExtractor) {
            ((PDFMarkedContentExtractor)this.context).endMarkedContentSequence();
        }
    }
}
