// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.util.operator;

import org.apache.pdfbox.util.Matrix;
import org.apache.pdfbox.cos.COSNumber;
import org.apache.pdfbox.cos.COSBase;
import java.util.List;
import org.apache.pdfbox.util.PDFOperator;

public class MoveText extends OperatorProcessor
{
    @Override
    public void process(final PDFOperator operator, final List<COSBase> arguments) {
        final COSNumber x = arguments.get(0);
        final COSNumber y = arguments.get(1);
        final Matrix td = new Matrix();
        td.setValue(2, 0, x.floatValue());
        td.setValue(2, 1, y.floatValue());
        this.context.setTextLineMatrix(td.multiply(this.context.getTextLineMatrix()));
        this.context.setTextMatrix(this.context.getTextLineMatrix().copy());
    }
}
