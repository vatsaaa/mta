// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.util.operator;

import java.io.IOException;
import org.apache.pdfbox.pdmodel.graphics.color.PDColorState;
import org.apache.pdfbox.pdmodel.graphics.color.PDColorSpace;
import org.apache.pdfbox.cos.COSNumber;
import org.apache.pdfbox.pdmodel.graphics.color.PDDeviceRGB;
import org.apache.pdfbox.cos.COSBase;
import java.util.List;
import org.apache.pdfbox.util.PDFOperator;

public class SetNonStrokingRGBColor extends OperatorProcessor
{
    @Override
    public void process(final PDFOperator operator, final List<COSBase> arguments) throws IOException {
        final PDColorSpace cs = PDDeviceRGB.INSTANCE;
        final PDColorState colorInstance = this.context.getGraphicsState().getNonStrokingColor();
        colorInstance.setColorSpace(cs);
        final float[] values = new float[3];
        for (int i = 0; i < arguments.size(); ++i) {
            values[i] = arguments.get(i).floatValue();
        }
        colorInstance.setColorSpaceValue(values);
    }
}
