// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.util.operator;

import java.io.IOException;
import java.util.Map;
import org.apache.pdfbox.pdmodel.graphics.pattern.PDPatternResources;
import org.apache.pdfbox.cos.COSArray;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.cos.COSBase;
import java.util.List;
import org.apache.pdfbox.util.PDFOperator;

public class SetNonStrokingPattern extends OperatorProcessor
{
    @Override
    public void process(final PDFOperator operator, final List<COSBase> arguments) throws IOException {
        final int numberOfArguments = arguments.size();
        COSName selectedPattern;
        if (numberOfArguments == 1) {
            selectedPattern = arguments.get(0);
        }
        else {
            final COSArray colorValues = new COSArray();
            for (int i = 0; i < numberOfArguments - 1; ++i) {
                colorValues.add(arguments.get(i));
            }
            selectedPattern = arguments.get(numberOfArguments - 1);
        }
        final Map<String, PDPatternResources> patterns = this.getContext().getResources().getPatterns();
        final PDPatternResources pattern = patterns.get(selectedPattern.getName());
        this.getContext().getGraphicsState().getNonStrokingColor().setPattern(pattern);
    }
}
