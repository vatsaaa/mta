// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.pdfbox.pdmodel.PDPage;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import org.apache.pdfbox.pdmodel.PDDocument;
import java.io.IOException;
import java.io.ByteArrayOutputStream;
import java.io.Writer;

public class PDFHighlighter extends PDFTextStripper
{
    private Writer highlighterOutput;
    private String[] searchedWords;
    private ByteArrayOutputStream textOS;
    private Writer textWriter;
    private static final String ENCODING = "UTF-16";
    
    public PDFHighlighter() throws IOException {
        super("UTF-16");
        this.highlighterOutput = null;
        this.textOS = null;
        this.textWriter = null;
        super.setLineSeparator("");
        super.setPageSeparator("");
        super.setWordSeparator("");
        super.setShouldSeparateByBeads(false);
        super.setSuppressDuplicateOverlappingText(false);
    }
    
    public void generateXMLHighlight(final PDDocument pdDocument, final String highlightWord, final Writer xmlOutput) throws IOException {
        this.generateXMLHighlight(pdDocument, new String[] { highlightWord }, xmlOutput);
    }
    
    public void generateXMLHighlight(final PDDocument pdDocument, final String[] sWords, final Writer xmlOutput) throws IOException {
        this.highlighterOutput = xmlOutput;
        this.searchedWords = sWords;
        this.highlighterOutput.write("<XML>\n<Body units=characters  version=2>\n<Highlight>\n");
        this.textOS = new ByteArrayOutputStream();
        this.writeText(pdDocument, this.textWriter = new OutputStreamWriter(this.textOS, "UTF-16"));
        this.highlighterOutput.write("</Highlight>\n</Body>\n</XML>");
        this.highlighterOutput.flush();
    }
    
    @Override
    protected void endPage(final PDPage pdPage) throws IOException {
        this.textWriter.flush();
        String page = new String(this.textOS.toByteArray(), "UTF-16");
        this.textOS.reset();
        if (page.indexOf("a") != -1) {
            page = page.replaceAll("a[0-9]{1,3}", ".");
        }
        for (int i = 0; i < this.searchedWords.length; ++i) {
            final Pattern pattern = Pattern.compile(this.searchedWords[i], 2);
            final Matcher matcher = pattern.matcher(page);
            while (matcher.find()) {
                final int begin = matcher.start();
                final int end = matcher.end();
                this.highlighterOutput.write("    <loc pg=" + (this.getCurrentPageNo() - 1) + " pos=" + begin + " len=" + (end - begin) + ">\n");
            }
        }
    }
    
    public static void main(final String[] args) throws IOException {
        final PDFHighlighter xmlExtractor = new PDFHighlighter();
        PDDocument doc = null;
        try {
            if (args.length < 2) {
                usage();
            }
            final String[] highlightStrings = new String[args.length - 1];
            System.arraycopy(args, 1, highlightStrings, 0, highlightStrings.length);
            doc = PDDocument.load(args[0]);
            xmlExtractor.generateXMLHighlight(doc, highlightStrings, new OutputStreamWriter(System.out));
        }
        finally {
            if (doc != null) {
                doc.close();
            }
        }
    }
    
    private static void usage() {
        System.err.println("usage: java " + PDFHighlighter.class.getName() + " <pdf file> word1 word2 word3 ...");
        System.exit(1);
    }
}
