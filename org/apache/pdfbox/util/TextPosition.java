// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.util;

import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.font.PDFont;

public class TextPosition
{
    private Matrix textPos;
    private float endX;
    private float endY;
    private float maxTextHeight;
    private int rot;
    private float x;
    private float y;
    private float pageHeight;
    private float pageWidth;
    private float[] widths;
    private float widthOfSpace;
    private String str;
    private int[] unicodeCP;
    private PDFont font;
    private float fontSize;
    private int fontSizePt;
    private float wordSpacing;
    
    protected TextPosition() {
        this.x = Float.NEGATIVE_INFINITY;
        this.y = Float.NEGATIVE_INFINITY;
    }
    
    public TextPosition(final PDPage page, final Matrix textPositionSt, final Matrix textPositionEnd, final float maxFontH, final float[] individualWidths, final float spaceWidth, final String string, final PDFont currentFont, final float fontSizeValue, final int fontSizeInPt, final float ws) {
        this.x = Float.NEGATIVE_INFINITY;
        this.y = Float.NEGATIVE_INFINITY;
        this.textPos = textPositionSt;
        this.endX = textPositionEnd.getXPosition();
        this.endY = textPositionEnd.getYPosition();
        this.rot = page.findRotation();
        if (this.rot < 0) {
            this.rot += 360;
        }
        this.maxTextHeight = maxFontH;
        this.pageHeight = page.findMediaBox().getHeight();
        this.pageWidth = page.findMediaBox().getWidth();
        this.widths = individualWidths;
        this.widthOfSpace = spaceWidth;
        this.str = string;
        this.font = currentFont;
        this.fontSize = fontSizeValue;
        this.fontSizePt = fontSizeInPt;
        this.wordSpacing = ws;
    }
    
    @Deprecated
    public TextPosition(final int pageRotation, final float pageWidthValue, final float pageHeightValue, final Matrix textPositionSt, final Matrix textPositionEnd, final float maxFontH, final float individualWidth, final float spaceWidth, final String string, final PDFont currentFont, final float fontSizeValue, final int fontSizeInPt) {
        this(pageRotation, pageWidthValue, pageHeightValue, textPositionSt, textPositionEnd.getXPosition(), textPositionEnd.getYPosition(), maxFontH, individualWidth, spaceWidth, string, null, currentFont, fontSizeValue, fontSizeInPt);
    }
    
    @Deprecated
    public TextPosition(final int pageRotation, final float pageWidthValue, final float pageHeightValue, final Matrix textPositionSt, final float endXValue, final float endYValue, final float maxFontH, final float individualWidth, final float spaceWidth, final String string, final PDFont currentFont, final float fontSizeValue, final int fontSizeInPt) {
        this(pageRotation, pageWidthValue, pageHeightValue, textPositionSt, endXValue, endYValue, maxFontH, individualWidth, spaceWidth, string, null, currentFont, fontSizeValue, fontSizeInPt);
    }
    
    public TextPosition(final int pageRotation, final float pageWidthValue, final float pageHeightValue, final Matrix textPositionSt, final float endXValue, final float endYValue, final float maxFontH, final float individualWidth, final float spaceWidth, final String string, final int[] codePoints, final PDFont currentFont, final float fontSizeValue, final int fontSizeInPt) {
        this.x = Float.NEGATIVE_INFINITY;
        this.y = Float.NEGATIVE_INFINITY;
        this.textPos = textPositionSt;
        this.endX = endXValue;
        this.endY = endYValue;
        this.rot = pageRotation;
        if (this.rot < 0) {
            this.rot += 360;
        }
        this.maxTextHeight = maxFontH;
        this.pageHeight = pageHeightValue;
        this.pageWidth = pageWidthValue;
        this.widths = new float[] { individualWidth };
        this.widthOfSpace = spaceWidth;
        this.str = string;
        this.unicodeCP = codePoints;
        this.font = currentFont;
        this.fontSize = fontSizeValue;
        this.fontSizePt = fontSizeInPt;
    }
    
    public String getCharacter() {
        return this.str;
    }
    
    public int[] getCodePoints() {
        return this.unicodeCP;
    }
    
    public Matrix getTextPos() {
        return this.textPos;
    }
    
    public float getDir() {
        final float a = this.textPos.getValue(0, 0);
        final float b = this.textPos.getValue(0, 1);
        final float c = this.textPos.getValue(1, 0);
        final float d = this.textPos.getValue(1, 1);
        if (a > 0.0f && Math.abs(b) < d && Math.abs(c) < a && d > 0.0f) {
            return 0.0f;
        }
        if (a < 0.0f && Math.abs(b) < Math.abs(d) && Math.abs(c) < Math.abs(a) && d < 0.0f) {
            return 180.0f;
        }
        if (Math.abs(a) < Math.abs(c) && b > 0.0f && c < 0.0f && Math.abs(d) < b) {
            return 90.0f;
        }
        if (Math.abs(a) < c && b < 0.0f && c > 0.0f && Math.abs(d) < Math.abs(b)) {
            return 270.0f;
        }
        return 0.0f;
    }
    
    private float getXRot(final float rotation) {
        if (rotation == 0.0f) {
            return this.textPos.getValue(2, 0);
        }
        if (rotation == 90.0f) {
            return this.textPos.getValue(2, 1);
        }
        if (rotation == 180.0f) {
            return this.pageWidth - this.textPos.getValue(2, 0);
        }
        if (rotation == 270.0f) {
            return this.pageHeight - this.textPos.getValue(2, 1);
        }
        return 0.0f;
    }
    
    public float getX() {
        if (this.x == Float.NEGATIVE_INFINITY) {
            this.x = this.getXRot((float)this.rot);
        }
        return this.x;
    }
    
    public float getXDirAdj() {
        return this.getXRot(this.getDir());
    }
    
    private float getYLowerLeftRot(final float rotation) {
        if (rotation == 0.0f) {
            return this.textPos.getValue(2, 1);
        }
        if (rotation == 90.0f) {
            return this.pageWidth - this.textPos.getValue(2, 0);
        }
        if (rotation == 180.0f) {
            return this.pageHeight - this.textPos.getValue(2, 1);
        }
        if (rotation == 270.0f) {
            return this.textPos.getValue(2, 0);
        }
        return 0.0f;
    }
    
    public float getY() {
        if (this.y == Float.NEGATIVE_INFINITY) {
            if (this.rot == 0 || this.rot == 180) {
                this.y = this.pageHeight - this.getYLowerLeftRot((float)this.rot);
            }
            else {
                this.y = this.pageWidth - this.getYLowerLeftRot((float)this.rot);
            }
        }
        return this.y;
    }
    
    public float getYDirAdj() {
        final float dir = this.getDir();
        if (dir == 0.0f || dir == 180.0f) {
            return this.pageHeight - this.getYLowerLeftRot(dir);
        }
        return this.pageWidth - this.getYLowerLeftRot(dir);
    }
    
    private float getWidthRot(final float rotation) {
        if (rotation == 90.0f || rotation == 270.0f) {
            return Math.abs(this.endY - this.textPos.getYPosition());
        }
        return Math.abs(this.endX - this.textPos.getXPosition());
    }
    
    public float getWidth() {
        return this.getWidthRot((float)this.rot);
    }
    
    public float getWidthDirAdj() {
        return this.getWidthRot(this.getDir());
    }
    
    public float getHeight() {
        return this.maxTextHeight;
    }
    
    public float getHeightDir() {
        return this.maxTextHeight;
    }
    
    public float getFontSize() {
        return this.fontSize;
    }
    
    public float getFontSizeInPt() {
        return (float)this.fontSizePt;
    }
    
    public PDFont getFont() {
        return this.font;
    }
    
    @Deprecated
    public float getWordSpacing() {
        return this.wordSpacing;
    }
    
    public float getWidthOfSpace() {
        return this.widthOfSpace;
    }
    
    public float getXScale() {
        return this.textPos.getXScale();
    }
    
    public float getYScale() {
        return this.textPos.getYScale();
    }
    
    public float[] getIndividualWidths() {
        return this.widths;
    }
    
    @Override
    public String toString() {
        return this.getCharacter();
    }
    
    public boolean contains(final TextPosition tp2) {
        final double thisXstart = this.getXDirAdj();
        final double thisXend = this.getXDirAdj() + this.getWidthDirAdj();
        final double tp2Xstart = tp2.getXDirAdj();
        final double tp2Xend = tp2.getXDirAdj() + tp2.getWidthDirAdj();
        if (tp2Xend <= thisXstart || tp2Xstart >= thisXend) {
            return false;
        }
        if (tp2.getYDirAdj() + tp2.getHeightDir() < this.getYDirAdj() || tp2.getYDirAdj() > this.getYDirAdj() + this.getHeightDir()) {
            return false;
        }
        if (tp2Xstart > thisXstart && tp2Xend > thisXend) {
            final double overlap = thisXend - tp2Xstart;
            final double overlapPercent = overlap / this.getWidthDirAdj();
            return overlapPercent > 0.15;
        }
        if (tp2Xstart < thisXstart && tp2Xend < thisXend) {
            final double overlap = tp2Xend - thisXstart;
            final double overlapPercent = overlap / this.getWidthDirAdj();
            return overlapPercent > 0.15;
        }
        return true;
    }
    
    public void mergeDiacritic(final TextPosition diacritic, final TextNormalize normalize) {
        if (diacritic.getCharacter().length() > 1) {
            return;
        }
        final float diacXStart = diacritic.getXDirAdj();
        final float diacXEnd = diacXStart + diacritic.widths[0];
        float currCharXStart = this.getXDirAdj();
        final int strLen = this.str.length();
        boolean wasAdded = false;
        for (int i = 0; i < strLen && !wasAdded; ++i) {
            final float currCharXEnd = currCharXStart + this.widths[i];
            if (diacXStart < currCharXStart && diacXEnd <= currCharXEnd) {
                if (i == 0) {
                    this.insertDiacritic(i, diacritic, normalize);
                }
                else {
                    final float distanceOverlapping1 = diacXEnd - currCharXStart;
                    final float percentage1 = distanceOverlapping1 / this.widths[i];
                    final float distanceOverlapping2 = currCharXStart - diacXStart;
                    final float percentage2 = distanceOverlapping2 / this.widths[i - 1];
                    if (percentage1 >= percentage2) {
                        this.insertDiacritic(i, diacritic, normalize);
                    }
                    else {
                        this.insertDiacritic(i - 1, diacritic, normalize);
                    }
                }
                wasAdded = true;
            }
            else if (diacXStart < currCharXStart && diacXEnd > currCharXEnd) {
                this.insertDiacritic(i, diacritic, normalize);
                wasAdded = true;
            }
            else if (diacXStart >= currCharXStart && diacXEnd <= currCharXEnd) {
                this.insertDiacritic(i, diacritic, normalize);
                wasAdded = true;
            }
            else if (diacXStart >= currCharXStart && diacXEnd > currCharXEnd && i == strLen - 1) {
                this.insertDiacritic(i, diacritic, normalize);
                wasAdded = true;
            }
            currCharXStart += this.widths[i];
        }
    }
    
    private void insertDiacritic(final int i, final TextPosition diacritic, final TextNormalize normalize) {
        final int dir = Character.getDirectionality(this.str.charAt(i));
        final StringBuffer buf = new StringBuffer();
        buf.append(this.str.substring(0, i));
        final float[] widths2 = new float[this.widths.length + 1];
        System.arraycopy(this.widths, 0, widths2, 0, i);
        if (dir == 1 || dir == 2 || dir == 16 || dir == 17) {
            buf.append(normalize.normalizeDiac(diacritic.getCharacter()));
            widths2[i] = 0.0f;
            buf.append(this.str.charAt(i));
            widths2[i + 1] = this.widths[i];
        }
        else {
            buf.append(this.str.charAt(i));
            widths2[i] = this.widths[i];
            buf.append(normalize.normalizeDiac(diacritic.getCharacter()));
            widths2[i + 1] = 0.0f;
        }
        buf.append(this.str.substring(i + 1, this.str.length()));
        System.arraycopy(this.widths, i + 1, widths2, i + 2, this.widths.length - i - 1);
        this.str = buf.toString();
        this.widths = widths2;
    }
    
    public boolean isDiacritic() {
        final String cText = this.getCharacter();
        if (cText.length() != 1) {
            return false;
        }
        final int type = Character.getType(cText.charAt(0));
        return type == 6 || type == 27 || type == 4;
    }
}
