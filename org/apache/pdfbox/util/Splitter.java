// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.util;

import java.util.Iterator;
import org.apache.pdfbox.pdmodel.PDPage;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.apache.pdfbox.pdmodel.PDDocument;

public class Splitter
{
    protected PDDocument pdfDocument;
    protected PDDocument currentDocument;
    private int splitAtPage;
    private int startPage;
    private int endPage;
    private List<PDDocument> newDocuments;
    protected int pageNumber;
    
    public Splitter() {
        this.currentDocument = null;
        this.splitAtPage = 1;
        this.startPage = Integer.MIN_VALUE;
        this.endPage = Integer.MAX_VALUE;
        this.newDocuments = null;
        this.pageNumber = 0;
    }
    
    public List<PDDocument> split(final PDDocument document) throws IOException {
        this.newDocuments = new ArrayList<PDDocument>();
        this.pdfDocument = document;
        final List pages = this.pdfDocument.getDocumentCatalog().getAllPages();
        this.processPages(pages);
        return this.newDocuments;
    }
    
    public void setSplitAtPage(final int split) {
        if (split <= 0) {
            throw new RuntimeException("Error split must be at least one page.");
        }
        this.splitAtPage = split;
    }
    
    public int getSplitAtPage() {
        return this.splitAtPage;
    }
    
    public void setStartPage(final int start) {
        if (start <= 0) {
            throw new RuntimeException("Error split must be at least one page.");
        }
        this.startPage = start;
    }
    
    public int getStartPage() {
        return this.startPage;
    }
    
    public void setEndPage(final int end) {
        if (end <= 0) {
            throw new RuntimeException("Error split must be at least one page.");
        }
        this.endPage = end;
    }
    
    public int getEndPage() {
        return this.endPage;
    }
    
    protected void processPages(final List pages) throws IOException {
        for (final PDPage page : pages) {
            if (this.pageNumber + 1 >= this.startPage && this.pageNumber + 1 <= this.endPage) {
                this.processNextPage(page);
            }
            else {
                if (this.pageNumber > this.endPage) {
                    break;
                }
                ++this.pageNumber;
            }
        }
    }
    
    protected void createNewDocumentIfNecessary() throws IOException {
        if (this.isNewDocNecessary()) {
            this.createNewDocument();
        }
    }
    
    protected boolean isNewDocNecessary() {
        return this.pageNumber % this.splitAtPage == 0 || this.currentDocument == null;
    }
    
    protected void createNewDocument() throws IOException {
        (this.currentDocument = new PDDocument()).setDocumentInformation(this.pdfDocument.getDocumentInformation());
        this.currentDocument.getDocumentCatalog().setViewerPreferences(this.pdfDocument.getDocumentCatalog().getViewerPreferences());
        this.newDocuments.add(this.currentDocument);
    }
    
    protected void processNextPage(final PDPage page) throws IOException {
        this.createNewDocumentIfNecessary();
        final PDPage imported = this.currentDocument.importPage(page);
        imported.setCropBox(page.findCropBox());
        imported.setMediaBox(page.findMediaBox());
        imported.setResources(page.getResources());
        imported.setRotation(page.findRotation());
        ++this.pageNumber;
    }
}
