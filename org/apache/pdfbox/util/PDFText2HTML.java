// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.util;

import java.util.Iterator;
import java.util.List;
import org.apache.pdfbox.pdmodel.PDDocument;
import java.io.IOException;

public class PDFText2HTML extends PDFTextStripper
{
    private static final int INITIAL_PDF_TO_HTML_BYTES = 8192;
    private boolean onFirstPage;
    
    public PDFText2HTML(final String encoding) throws IOException {
        super(encoding);
        this.onFirstPage = true;
        this.setLineSeparator(this.systemLineSeparator);
        this.setParagraphStart("<p>");
        this.setParagraphEnd("</p>" + this.systemLineSeparator);
        this.setPageStart("<div style=\"page-break-before:always; page-break-after:always\">");
        this.setPageEnd("</div>" + this.systemLineSeparator);
        this.setArticleStart(this.systemLineSeparator);
        this.setArticleEnd(this.systemLineSeparator);
    }
    
    protected void writeHeader() throws IOException {
        final StringBuffer buf = new StringBuffer(8192);
        buf.append("<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\"\n\"http://www.w3.org/TR/html4/loose.dtd\">\n");
        buf.append("<html><head>");
        buf.append("<title>" + this.escape(this.getTitle()) + "</title>\n");
        if (this.outputEncoding != null) {
            buf.append("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=" + this.outputEncoding + "\">\n");
        }
        buf.append("</head>\n");
        buf.append("<body>\n");
        super.writeString(buf.toString());
    }
    
    @Override
    protected void writePage() throws IOException {
        if (this.onFirstPage) {
            this.writeHeader();
            this.onFirstPage = false;
        }
        super.writePage();
    }
    
    public void endDocument(final PDDocument pdf) throws IOException {
        super.writeString("</body></html>");
    }
    
    protected String getTitle() {
        final String titleGuess = this.document.getDocumentInformation().getTitle();
        if (titleGuess != null && titleGuess.length() > 0) {
            return titleGuess;
        }
        final Iterator<List<TextPosition>> textIter = this.getCharactersByArticle().iterator();
        float lastFontSize = -1.0f;
        final StringBuffer titleText = new StringBuffer();
        while (textIter.hasNext()) {
            for (final TextPosition position : textIter.next()) {
                final float currentFontSize = position.getFontSize();
                if (currentFontSize != lastFontSize || titleText.length() > 64) {
                    if (titleText.length() > 0) {
                        return titleText.toString();
                    }
                    lastFontSize = currentFontSize;
                }
                if (currentFontSize > 13.0f) {
                    titleText.append(position.getCharacter());
                }
            }
        }
        return "";
    }
    
    @Override
    protected void startArticle(final boolean isltr) throws IOException {
        if (isltr) {
            super.writeString("<div>");
        }
        else {
            super.writeString("<div dir=\"RTL\">");
        }
    }
    
    @Override
    protected void endArticle() throws IOException {
        super.endArticle();
        super.writeString("</div>");
    }
    
    @Override
    protected void writeString(final String chars) throws IOException {
        super.writeString(this.escape(chars));
    }
    
    private String escape(final String chars) {
        final StringBuilder builder = new StringBuilder(chars.length());
        for (int i = 0; i < chars.length(); ++i) {
            final char c = chars.charAt(i);
            if (c < ' ' || c > '~') {
                final int charAsInt = c;
                builder.append("&#").append(charAsInt).append(";");
            }
            else {
                switch (c) {
                    case '\"': {
                        builder.append("&quot;");
                        break;
                    }
                    case '&': {
                        builder.append("&amp;");
                        break;
                    }
                    case '<': {
                        builder.append("&lt;");
                        break;
                    }
                    case '>': {
                        builder.append("&gt;");
                        break;
                    }
                    default: {
                        builder.append(String.valueOf(c));
                        break;
                    }
                }
            }
        }
        return builder.toString();
    }
}
