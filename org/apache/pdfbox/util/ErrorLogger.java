// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.util;

public class ErrorLogger
{
    private ErrorLogger() {
    }
    
    public static void log(final String errorMessage) {
        System.err.println(errorMessage);
    }
    
    public static void log(final String errorMessage, final Throwable t) {
        System.err.println(errorMessage);
        t.printStackTrace();
    }
}
