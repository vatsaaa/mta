// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.util;

import org.apache.commons.logging.LogFactory;
import org.apache.pdfbox.pdmodel.graphics.PDExtendedGraphicsState;
import org.apache.pdfbox.pdmodel.graphics.xobject.PDXObject;
import org.apache.pdfbox.pdmodel.graphics.color.PDColorSpace;
import org.apache.pdfbox.pdmodel.common.PDMatrix;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDType3Font;
import java.util.List;
import org.apache.pdfbox.cos.COSObject;
import org.apache.pdfbox.pdfparser.PDFStreamParser;
import org.apache.pdfbox.cos.COSBase;
import java.util.ArrayList;
import org.apache.pdfbox.cos.COSStream;
import java.io.IOException;
import java.util.Iterator;
import java.util.Enumeration;
import org.apache.pdfbox.exceptions.WrappedIOException;
import java.util.Collections;
import java.util.Properties;
import java.util.HashMap;
import java.util.HashSet;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDResources;
import org.apache.pdfbox.util.operator.OperatorProcessor;
import java.util.Map;
import java.util.Stack;
import org.apache.pdfbox.pdmodel.graphics.PDGraphicsState;
import java.util.Set;
import org.apache.commons.logging.Log;

public class PDFStreamEngine
{
    private static final Log LOG;
    private final Set<String> unsupportedOperators;
    private static final byte[] SPACE_BYTES;
    private PDGraphicsState graphicsState;
    private Matrix textMatrix;
    private Matrix textLineMatrix;
    private Stack<PDGraphicsState> graphicsStack;
    private Map<String, OperatorProcessor> operators;
    private Stack<PDResources> streamResourcesStack;
    private PDPage page;
    private int validCharCnt;
    private int totalCharCnt;
    private boolean forceParsing;
    
    public PDFStreamEngine() {
        this.unsupportedOperators = new HashSet<String>();
        this.graphicsState = null;
        this.textMatrix = null;
        this.textLineMatrix = null;
        this.graphicsStack = new Stack<PDGraphicsState>();
        this.operators = new HashMap<String, OperatorProcessor>();
        this.streamResourcesStack = new Stack<PDResources>();
        this.forceParsing = false;
        this.validCharCnt = 0;
        this.totalCharCnt = 0;
    }
    
    public PDFStreamEngine(final Properties properties) throws IOException {
        this.unsupportedOperators = new HashSet<String>();
        this.graphicsState = null;
        this.textMatrix = null;
        this.textLineMatrix = null;
        this.graphicsStack = new Stack<PDGraphicsState>();
        this.operators = new HashMap<String, OperatorProcessor>();
        this.streamResourcesStack = new Stack<PDResources>();
        this.forceParsing = false;
        if (properties == null) {
            throw new NullPointerException("properties cannot be null");
        }
        final Enumeration<?> names = properties.propertyNames();
        for (final Object name : Collections.list(names)) {
            final String operator = name.toString();
            final String processorClassName = properties.getProperty(operator);
            if ("".equals(processorClassName)) {
                this.unsupportedOperators.add(operator);
            }
            else {
                try {
                    final Class<?> klass = Class.forName(processorClassName);
                    final OperatorProcessor processor = (OperatorProcessor)klass.newInstance();
                    this.registerOperatorProcessor(operator, processor);
                }
                catch (Exception e) {
                    throw new WrappedIOException("OperatorProcessor class " + processorClassName + " could not be instantiated", e);
                }
            }
        }
        this.validCharCnt = 0;
        this.totalCharCnt = 0;
    }
    
    public boolean isForceParsing() {
        return this.forceParsing;
    }
    
    public void setForceParsing(final boolean forceParsingValue) {
        this.forceParsing = forceParsingValue;
    }
    
    public void registerOperatorProcessor(final String operator, final OperatorProcessor op) {
        op.setContext(this);
        this.operators.put(operator, op);
    }
    
    public void resetEngine() {
        this.validCharCnt = 0;
        this.totalCharCnt = 0;
    }
    
    public void processStream(final PDPage aPage, final PDResources resources, final COSStream cosStream) throws IOException {
        this.graphicsState = new PDGraphicsState(aPage.findCropBox());
        this.textMatrix = null;
        this.textLineMatrix = null;
        this.graphicsStack.clear();
        this.streamResourcesStack.clear();
        this.processSubStream(aPage, resources, cosStream);
    }
    
    public void processSubStream(final PDPage aPage, final PDResources resources, final COSStream cosStream) throws IOException {
        this.page = aPage;
        if (resources != null) {
            this.streamResourcesStack.push(resources);
            try {
                this.processSubStream(cosStream);
            }
            finally {
                this.streamResourcesStack.pop().clear();
            }
        }
        else {
            this.processSubStream(cosStream);
        }
    }
    
    private void processSubStream(final COSStream cosStream) throws IOException {
        List<COSBase> arguments = new ArrayList<COSBase>();
        final PDFStreamParser parser = new PDFStreamParser(cosStream, this.forceParsing);
        try {
            final Iterator<Object> iter = parser.getTokenIterator();
            while (iter.hasNext()) {
                final Object next = iter.next();
                if (PDFStreamEngine.LOG.isDebugEnabled()) {
                    PDFStreamEngine.LOG.debug("processing substream token: " + next);
                }
                if (next instanceof COSObject) {
                    arguments.add(((COSObject)next).getObject());
                }
                else if (next instanceof PDFOperator) {
                    this.processOperator((PDFOperator)next, arguments);
                    arguments = new ArrayList<COSBase>();
                }
                else {
                    arguments.add((COSBase)next);
                }
            }
        }
        finally {
            parser.close();
        }
    }
    
    protected void processTextPosition(final TextPosition text) {
    }
    
    protected String inspectFontEncoding(final String str) {
        return str;
    }
    
    public void processEncodedText(final byte[] string) throws IOException {
        final float fontSizeText = this.graphicsState.getTextState().getFontSize();
        final float horizontalScalingText = this.graphicsState.getTextState().getHorizontalScalingPercent() / 100.0f;
        final float riseText = this.graphicsState.getTextState().getRise();
        final float wordSpacingText = this.graphicsState.getTextState().getWordSpacing();
        final float characterSpacingText = this.graphicsState.getTextState().getCharacterSpacing();
        final PDFont font = this.graphicsState.getTextState().getFont();
        float fontMatrixXScaling = 0.001f;
        float fontMatrixYScaling = 0.001f;
        float glyphSpaceToTextSpaceFactor = 0.001f;
        if (font instanceof PDType3Font) {
            final PDMatrix fontMatrix = font.getFontMatrix();
            fontMatrixXScaling = fontMatrix.getValue(0, 0);
            fontMatrixYScaling = fontMatrix.getValue(1, 1);
            glyphSpaceToTextSpaceFactor = 1.0f / fontMatrix.getValue(0, 0);
        }
        float spaceWidthText = 0.0f;
        try {
            spaceWidthText = font.getFontWidth(PDFStreamEngine.SPACE_BYTES, 0, 1) * glyphSpaceToTextSpaceFactor;
        }
        catch (Throwable exception) {
            PDFStreamEngine.LOG.warn(exception, exception);
        }
        if (spaceWidthText == 0.0f) {
            spaceWidthText = font.getAverageFontWidth() * glyphSpaceToTextSpaceFactor;
            spaceWidthText *= 0.8f;
        }
        float maxVerticalDisplacementText = 0.0f;
        final Matrix textStateParameters = new Matrix();
        textStateParameters.setValue(0, 0, fontSizeText * horizontalScalingText);
        textStateParameters.setValue(1, 1, fontSizeText);
        textStateParameters.setValue(2, 1, riseText);
        final int pageRotation = this.page.findRotation();
        final float pageHeight = this.page.findMediaBox().getHeight();
        final float pageWidth = this.page.findMediaBox().getWidth();
        final Matrix ctm = this.getGraphicsState().getCurrentTransformationMatrix();
        Matrix textXctm = new Matrix();
        Matrix textMatrixEnd = new Matrix();
        final Matrix td = new Matrix();
        Matrix tempMatrix = new Matrix();
        for (int codeLength = 1, i = 0; i < string.length; i += codeLength) {
            codeLength = 1;
            String c = font.encode(string, i, codeLength);
            int[] codePoints = null;
            if (c == null && i + 1 < string.length) {
                ++codeLength;
                c = font.encode(string, i, codeLength);
                codePoints = new int[] { font.getCodeFromArray(string, i, codeLength) };
            }
            final float spaceWidthDisp = spaceWidthText * fontSizeText * horizontalScalingText * this.textMatrix.getValue(0, 0) * ctm.getValue(0, 0);
            float characterHorizontalDisplacementText = font.getFontWidth(string, i, codeLength);
            float characterVerticalDisplacementText = font.getFontHeight(string, i, codeLength);
            characterHorizontalDisplacementText *= fontMatrixXScaling;
            characterVerticalDisplacementText *= fontMatrixYScaling;
            maxVerticalDisplacementText = Math.max(maxVerticalDisplacementText, characterVerticalDisplacementText);
            float spacingText = 0.0f;
            if (string[i] == 32 && codeLength == 1) {
                spacingText += wordSpacingText;
            }
            textXctm = this.textMatrix.multiply(ctm, textXctm);
            final Matrix textMatrixStart = textStateParameters.multiply(textXctm);
            float tx = characterHorizontalDisplacementText * fontSizeText * horizontalScalingText;
            final float ty = 0.0f;
            td.reset();
            td.setValue(2, 0, tx);
            td.setValue(2, 1, ty);
            tempMatrix = textStateParameters.multiply(td, tempMatrix);
            textMatrixEnd = tempMatrix.multiply(textXctm, textMatrixEnd);
            final float endXPosition = textMatrixEnd.getXPosition();
            final float endYPosition = textMatrixEnd.getYPosition();
            tx = (characterHorizontalDisplacementText * fontSizeText + characterSpacingText + spacingText) * horizontalScalingText;
            td.setValue(2, 0, tx);
            this.textMatrix = td.multiply(this.textMatrix, this.textMatrix);
            final float startXPosition = textMatrixStart.getXPosition();
            final float widthText = endXPosition - startXPosition;
            if (c != null) {
                ++this.validCharCnt;
            }
            else {
                c = "?";
            }
            ++this.totalCharCnt;
            final float totalVerticalDisplacementDisp = maxVerticalDisplacementText * fontSizeText * textXctm.getYScale();
            this.processTextPosition(new TextPosition(pageRotation, pageWidth, pageHeight, textMatrixStart, endXPosition, endYPosition, totalVerticalDisplacementDisp, widthText, spaceWidthDisp, c, codePoints, font, fontSizeText, (int)(fontSizeText * this.textMatrix.getXScale())));
        }
    }
    
    public void processOperator(final String operation, final List<COSBase> arguments) throws IOException {
        try {
            final PDFOperator oper = PDFOperator.getOperator(operation);
            this.processOperator(oper, arguments);
        }
        catch (IOException e) {
            PDFStreamEngine.LOG.warn(e, e);
        }
    }
    
    protected void processOperator(final PDFOperator operator, final List<COSBase> arguments) throws IOException {
        try {
            final String operation = operator.getOperation();
            final OperatorProcessor processor = this.operators.get(operation);
            if (processor != null) {
                processor.setContext(this);
                processor.process(operator, arguments);
            }
            else if (!this.unsupportedOperators.contains(operation)) {
                PDFStreamEngine.LOG.info("unsupported/disabled operation: " + operation);
                this.unsupportedOperators.add(operation);
            }
        }
        catch (Exception e) {
            PDFStreamEngine.LOG.warn(e, e);
        }
    }
    
    public Map<String, PDColorSpace> getColorSpaces() {
        return this.streamResourcesStack.peek().getColorSpaces();
    }
    
    public Map<String, PDXObject> getXObjects() {
        return this.streamResourcesStack.peek().getXObjects();
    }
    
    public void setColorSpaces(final Map<String, PDColorSpace> value) {
        this.streamResourcesStack.peek().setColorSpaces(value);
    }
    
    public Map<String, PDFont> getFonts() {
        return this.streamResourcesStack.peek().getFonts();
    }
    
    public void setFonts(final Map<String, PDFont> value) {
        this.streamResourcesStack.peek().setFonts(value);
    }
    
    public Stack<PDGraphicsState> getGraphicsStack() {
        return this.graphicsStack;
    }
    
    public void setGraphicsStack(final Stack<PDGraphicsState> value) {
        this.graphicsStack = value;
    }
    
    public PDGraphicsState getGraphicsState() {
        return this.graphicsState;
    }
    
    public void setGraphicsState(final PDGraphicsState value) {
        this.graphicsState = value;
    }
    
    public Map<String, PDExtendedGraphicsState> getGraphicsStates() {
        return this.streamResourcesStack.peek().getGraphicsStates();
    }
    
    public void setGraphicsStates(final Map<String, PDExtendedGraphicsState> value) {
        this.streamResourcesStack.peek().setGraphicsStates(value);
    }
    
    public Matrix getTextLineMatrix() {
        return this.textLineMatrix;
    }
    
    public void setTextLineMatrix(final Matrix value) {
        this.textLineMatrix = value;
    }
    
    public Matrix getTextMatrix() {
        return this.textMatrix;
    }
    
    public void setTextMatrix(final Matrix value) {
        this.textMatrix = value;
    }
    
    public PDResources getResources() {
        return this.streamResourcesStack.peek();
    }
    
    public PDPage getCurrentPage() {
        return this.page;
    }
    
    public int getValidCharCnt() {
        return this.validCharCnt;
    }
    
    public int getTotalCharCnt() {
        return this.totalCharCnt;
    }
    
    static {
        LOG = LogFactory.getLog(PDFStreamEngine.class);
        SPACE_BYTES = new byte[] { 32 };
    }
}
