// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.util;

import java.util.LinkedList;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import java.util.SortedSet;
import java.util.SortedMap;
import java.util.Comparator;
import java.util.Collections;
import java.util.ArrayList;
import org.apache.pdfbox.cos.COSStream;
import org.apache.pdfbox.pdmodel.common.PDStream;
import java.util.Iterator;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.common.COSObjectable;
import org.apache.pdfbox.exceptions.InvalidPasswordException;
import org.apache.pdfbox.exceptions.CryptographyException;
import org.apache.pdfbox.exceptions.WrappedIOException;
import org.apache.pdfbox.cos.COSDocument;
import java.io.StringWriter;
import java.util.Properties;
import java.io.IOException;
import java.util.HashMap;
import java.util.regex.Pattern;
import java.io.Writer;
import org.apache.pdfbox.pdmodel.PDDocument;
import java.util.TreeSet;
import java.util.TreeMap;
import java.util.Map;
import java.util.Vector;
import org.apache.pdfbox.pdmodel.interactive.pagenavigation.PDThreadBead;
import java.util.List;
import org.apache.pdfbox.pdmodel.interactive.documentnavigation.outline.PDOutlineItem;

public class PDFTextStripper extends PDFStreamEngine
{
    private static final String thisClassName;
    private static float DEFAULT_INDENT_THRESHOLD;
    private static float DEFAULT_DROP_THRESHOLD;
    protected final String systemLineSeparator;
    private String lineSeparator;
    private String pageSeparator;
    private String wordSeparator;
    private String paragraphStart;
    private String paragraphEnd;
    private String pageStart;
    private String pageEnd;
    private String articleStart;
    private String articleEnd;
    private int currentPageNo;
    private int startPage;
    private int endPage;
    private PDOutlineItem startBookmark;
    private int startBookmarkPageNumber;
    private PDOutlineItem endBookmark;
    private int endBookmarkPageNumber;
    private boolean suppressDuplicateOverlappingText;
    private boolean shouldSeparateByBeads;
    private boolean sortByPosition;
    private boolean addMoreFormatting;
    private float indentThreshold;
    private float dropThreshold;
    private float spacingTolerance;
    private float averageCharTolerance;
    private List<PDThreadBead> pageArticles;
    protected Vector<List<TextPosition>> charactersByArticle;
    private Map<String, TreeMap<Float, TreeSet<Float>>> characterListMapping;
    protected String outputEncoding;
    protected PDDocument document;
    protected Writer output;
    private TextNormalize normalize;
    private static final float ENDOFLASTTEXTX_RESET_VALUE = -1.0f;
    private static final float MAXYFORLINE_RESET_VALUE = -3.4028235E38f;
    private static final float EXPECTEDSTARTOFNEXTWORDX_RESET_VALUE = -3.4028235E38f;
    private static final float MAXHEIGHTFORLINE_RESET_VALUE = -1.0f;
    private static final float MINYTOPFORLINE_RESET_VALUE = Float.MAX_VALUE;
    private static final float LASTWORDSPACING_RESET_VALUE = -1.0f;
    private static final String[] LIST_ITEM_EXPRESSIONS;
    private List<Pattern> liPatterns;
    
    public PDFTextStripper() throws IOException {
        super(ResourceLoader.loadProperties("org/apache/pdfbox/resources/PDFTextStripper.properties", true));
        this.systemLineSeparator = System.getProperty("line.separator");
        this.lineSeparator = this.systemLineSeparator;
        this.pageSeparator = this.systemLineSeparator;
        this.wordSeparator = " ";
        this.paragraphStart = "";
        this.paragraphEnd = "";
        this.pageStart = "";
        this.pageEnd = this.pageSeparator;
        this.articleStart = "";
        this.articleEnd = "";
        this.currentPageNo = 0;
        this.startPage = 1;
        this.endPage = Integer.MAX_VALUE;
        this.startBookmark = null;
        this.startBookmarkPageNumber = -1;
        this.endBookmark = null;
        this.endBookmarkPageNumber = -1;
        this.suppressDuplicateOverlappingText = true;
        this.shouldSeparateByBeads = true;
        this.sortByPosition = false;
        this.addMoreFormatting = false;
        this.indentThreshold = PDFTextStripper.DEFAULT_INDENT_THRESHOLD;
        this.dropThreshold = PDFTextStripper.DEFAULT_DROP_THRESHOLD;
        this.spacingTolerance = 0.5f;
        this.averageCharTolerance = 0.3f;
        this.pageArticles = null;
        this.charactersByArticle = new Vector<List<TextPosition>>();
        this.characterListMapping = new HashMap<String, TreeMap<Float, TreeSet<Float>>>();
        this.normalize = null;
        this.liPatterns = null;
        this.outputEncoding = null;
        this.normalize = new TextNormalize(this.outputEncoding);
    }
    
    public PDFTextStripper(final Properties props) throws IOException {
        super(props);
        this.systemLineSeparator = System.getProperty("line.separator");
        this.lineSeparator = this.systemLineSeparator;
        this.pageSeparator = this.systemLineSeparator;
        this.wordSeparator = " ";
        this.paragraphStart = "";
        this.paragraphEnd = "";
        this.pageStart = "";
        this.pageEnd = this.pageSeparator;
        this.articleStart = "";
        this.articleEnd = "";
        this.currentPageNo = 0;
        this.startPage = 1;
        this.endPage = Integer.MAX_VALUE;
        this.startBookmark = null;
        this.startBookmarkPageNumber = -1;
        this.endBookmark = null;
        this.endBookmarkPageNumber = -1;
        this.suppressDuplicateOverlappingText = true;
        this.shouldSeparateByBeads = true;
        this.sortByPosition = false;
        this.addMoreFormatting = false;
        this.indentThreshold = PDFTextStripper.DEFAULT_INDENT_THRESHOLD;
        this.dropThreshold = PDFTextStripper.DEFAULT_DROP_THRESHOLD;
        this.spacingTolerance = 0.5f;
        this.averageCharTolerance = 0.3f;
        this.pageArticles = null;
        this.charactersByArticle = new Vector<List<TextPosition>>();
        this.characterListMapping = new HashMap<String, TreeMap<Float, TreeSet<Float>>>();
        this.normalize = null;
        this.liPatterns = null;
        this.outputEncoding = null;
        this.normalize = new TextNormalize(this.outputEncoding);
    }
    
    public PDFTextStripper(final String encoding) throws IOException {
        super(ResourceLoader.loadProperties("org/apache/pdfbox/resources/PDFTextStripper.properties", true));
        this.systemLineSeparator = System.getProperty("line.separator");
        this.lineSeparator = this.systemLineSeparator;
        this.pageSeparator = this.systemLineSeparator;
        this.wordSeparator = " ";
        this.paragraphStart = "";
        this.paragraphEnd = "";
        this.pageStart = "";
        this.pageEnd = this.pageSeparator;
        this.articleStart = "";
        this.articleEnd = "";
        this.currentPageNo = 0;
        this.startPage = 1;
        this.endPage = Integer.MAX_VALUE;
        this.startBookmark = null;
        this.startBookmarkPageNumber = -1;
        this.endBookmark = null;
        this.endBookmarkPageNumber = -1;
        this.suppressDuplicateOverlappingText = true;
        this.shouldSeparateByBeads = true;
        this.sortByPosition = false;
        this.addMoreFormatting = false;
        this.indentThreshold = PDFTextStripper.DEFAULT_INDENT_THRESHOLD;
        this.dropThreshold = PDFTextStripper.DEFAULT_DROP_THRESHOLD;
        this.spacingTolerance = 0.5f;
        this.averageCharTolerance = 0.3f;
        this.pageArticles = null;
        this.charactersByArticle = new Vector<List<TextPosition>>();
        this.characterListMapping = new HashMap<String, TreeMap<Float, TreeSet<Float>>>();
        this.normalize = null;
        this.liPatterns = null;
        this.outputEncoding = encoding;
        this.normalize = new TextNormalize(this.outputEncoding);
    }
    
    public String getText(final PDDocument doc) throws IOException {
        final StringWriter outputStream = new StringWriter();
        this.writeText(doc, outputStream);
        return outputStream.toString();
    }
    
    @Deprecated
    public String getText(final COSDocument doc) throws IOException {
        return this.getText(new PDDocument(doc));
    }
    
    @Deprecated
    public void writeText(final COSDocument doc, final Writer outputStream) throws IOException {
        this.writeText(new PDDocument(doc), outputStream);
    }
    
    @Override
    public void resetEngine() {
        super.resetEngine();
        this.currentPageNo = 0;
    }
    
    public void writeText(final PDDocument doc, final Writer outputStream) throws IOException {
        this.resetEngine();
        this.document = doc;
        this.output = outputStream;
        if (this.getAddMoreFormatting()) {
            this.paragraphEnd = this.lineSeparator;
            this.pageStart = this.lineSeparator;
            this.articleStart = this.lineSeparator;
            this.articleEnd = this.lineSeparator;
        }
        this.startDocument(this.document);
        if (this.document.isEncrypted()) {
            try {
                this.document.decrypt("");
            }
            catch (CryptographyException e) {
                throw new WrappedIOException("Error decrypting document, details: ", e);
            }
            catch (InvalidPasswordException e2) {
                throw new WrappedIOException("Error: document is encrypted", e2);
            }
        }
        this.processPages(this.document.getDocumentCatalog().getAllPages());
        this.endDocument(this.document);
    }
    
    protected void processPages(final List<COSObjectable> pages) throws IOException {
        if (this.startBookmark != null) {
            this.startBookmarkPageNumber = this.getPageNumber(this.startBookmark, pages);
        }
        if (this.endBookmark != null) {
            this.endBookmarkPageNumber = this.getPageNumber(this.endBookmark, pages);
        }
        if (this.startBookmarkPageNumber == -1 && this.startBookmark != null && this.endBookmarkPageNumber == -1 && this.endBookmark != null && this.startBookmark.getCOSObject() == this.endBookmark.getCOSObject()) {
            this.startBookmarkPageNumber = 0;
            this.endBookmarkPageNumber = 0;
        }
        for (final PDPage nextPage : pages) {
            final PDStream contentStream = nextPage.getContents();
            ++this.currentPageNo;
            if (contentStream != null) {
                final COSStream contents = contentStream.getStream();
                this.processPage(nextPage, contents);
            }
        }
    }
    
    private int getPageNumber(final PDOutlineItem bookmark, final List<COSObjectable> allPages) throws IOException {
        int pageNumber = -1;
        final PDPage page = bookmark.findDestinationPage(this.document);
        if (page != null) {
            pageNumber = allPages.indexOf(page) + 1;
        }
        return pageNumber;
    }
    
    protected void startDocument(final PDDocument pdf) throws IOException {
    }
    
    protected void endDocument(final PDDocument pdf) throws IOException {
    }
    
    protected void processPage(final PDPage page, final COSStream content) throws IOException {
        if (this.currentPageNo >= this.startPage && this.currentPageNo <= this.endPage && (this.startBookmarkPageNumber == -1 || this.currentPageNo >= this.startBookmarkPageNumber) && (this.endBookmarkPageNumber == -1 || this.currentPageNo <= this.endBookmarkPageNumber)) {
            this.startPage(page);
            this.pageArticles = (List<PDThreadBead>)page.getThreadBeads();
            int numberOfArticleSections = 1 + this.pageArticles.size() * 2;
            if (!this.shouldSeparateByBeads) {
                numberOfArticleSections = 1;
            }
            final int originalSize = this.charactersByArticle.size();
            this.charactersByArticle.setSize(numberOfArticleSections);
            for (int i = 0; i < numberOfArticleSections; ++i) {
                if (numberOfArticleSections < originalSize) {
                    this.charactersByArticle.get(i).clear();
                }
                else {
                    this.charactersByArticle.set(i, new ArrayList<TextPosition>());
                }
            }
            this.characterListMapping.clear();
            this.processStream(page, page.findResources(), content);
            this.writePage();
            this.endPage(page);
        }
    }
    
    protected void startArticle() throws IOException {
        this.startArticle(true);
    }
    
    protected void startArticle(final boolean isltr) throws IOException {
        this.output.write(this.getArticleStart());
    }
    
    protected void endArticle() throws IOException {
        this.output.write(this.getArticleEnd());
    }
    
    protected void startPage(final PDPage page) throws IOException {
    }
    
    protected void endPage(final PDPage page) throws IOException {
    }
    
    protected void writePage() throws IOException {
        float maxYForLine = -3.4028235E38f;
        float minYTopForLine = Float.MAX_VALUE;
        float endOfLastTextX = -1.0f;
        float lastWordSpacing = -1.0f;
        float maxHeightForLine = -1.0f;
        PositionWrapper lastPosition = null;
        PositionWrapper lastLineStartPosition = null;
        boolean startOfPage = true;
        boolean startOfArticle = true;
        if (this.charactersByArticle.size() > 0) {
            this.writePageStart();
        }
        for (int i = 0; i < this.charactersByArticle.size(); ++i) {
            final List<TextPosition> textList = this.charactersByArticle.get(i);
            if (this.getSortByPosition()) {
                final TextPositionComparator comparator = new TextPositionComparator();
                Collections.sort(textList, comparator);
            }
            Iterator<TextPosition> textIter = textList.iterator();
            int ltrCnt = 0;
            int rtlCnt = 0;
            while (textIter.hasNext()) {
                final TextPosition position = textIter.next();
                final String stringValue = position.getCharacter();
                for (int a = 0; a < stringValue.length(); ++a) {
                    final byte dir = Character.getDirectionality(stringValue.charAt(a));
                    if (dir == 0 || dir == 14 || dir == 15) {
                        ++ltrCnt;
                    }
                    else if (dir == 1 || dir == 2 || dir == 16 || dir == 17) {
                        ++rtlCnt;
                    }
                }
            }
            final boolean isRtlDominant = rtlCnt > ltrCnt;
            this.startArticle(!isRtlDominant);
            startOfArticle = true;
            final boolean hasRtl = rtlCnt > 0;
            final List<TextPosition> line = new ArrayList<TextPosition>();
            textIter = textList.iterator();
            float previousAveCharWidth = -1.0f;
            while (textIter.hasNext()) {
                final TextPosition position2 = textIter.next();
                final PositionWrapper current = new PositionWrapper(position2);
                final String characterValue = position2.getCharacter();
                if (lastPosition != null && (position2.getFont() != lastPosition.getTextPosition().getFont() || position2.getFontSize() != lastPosition.getTextPosition().getFontSize())) {
                    previousAveCharWidth = -1.0f;
                }
                float positionX;
                float positionY;
                float positionWidth;
                float positionHeight;
                if (this.getSortByPosition()) {
                    positionX = position2.getXDirAdj();
                    positionY = position2.getYDirAdj();
                    positionWidth = position2.getWidthDirAdj();
                    positionHeight = position2.getHeightDir();
                }
                else {
                    positionX = position2.getX();
                    positionY = position2.getY();
                    positionWidth = position2.getWidth();
                    positionHeight = position2.getHeight();
                }
                final int wordCharCount = position2.getIndividualWidths().length;
                final float wordSpacing = position2.getWidthOfSpace();
                float deltaSpace = 0.0f;
                if (wordSpacing == 0.0f || wordSpacing == Float.NaN) {
                    deltaSpace = Float.MAX_VALUE;
                }
                else if (lastWordSpacing < 0.0f) {
                    deltaSpace = wordSpacing * this.getSpacingTolerance();
                }
                else {
                    deltaSpace = (wordSpacing + lastWordSpacing) / 2.0f * this.getSpacingTolerance();
                }
                float averageCharWidth = -1.0f;
                if (previousAveCharWidth < 0.0f) {
                    averageCharWidth = positionWidth / wordCharCount;
                }
                else {
                    averageCharWidth = (previousAveCharWidth + positionWidth / wordCharCount) / 2.0f;
                }
                final float deltaCharWidth = averageCharWidth * this.getAverageCharTolerance();
                float expectedStartOfNextWordX = -3.4028235E38f;
                if (endOfLastTextX != -1.0f) {
                    if (deltaCharWidth > deltaSpace) {
                        expectedStartOfNextWordX = endOfLastTextX + deltaSpace;
                    }
                    else {
                        expectedStartOfNextWordX = endOfLastTextX + deltaCharWidth;
                    }
                }
                if (lastPosition != null) {
                    if (startOfArticle) {
                        lastPosition.setArticleStart();
                        startOfArticle = false;
                    }
                    if (!this.overlap(positionY, positionHeight, maxYForLine, maxHeightForLine)) {
                        this.writeLine(this.normalize(line, isRtlDominant, hasRtl), isRtlDominant);
                        line.clear();
                        lastLineStartPosition = this.handleLineSeparation(current, lastPosition, lastLineStartPosition, maxHeightForLine);
                        endOfLastTextX = -1.0f;
                        expectedStartOfNextWordX = -3.4028235E38f;
                        maxYForLine = -3.4028235E38f;
                        maxHeightForLine = -1.0f;
                        minYTopForLine = Float.MAX_VALUE;
                    }
                    if (expectedStartOfNextWordX != -3.4028235E38f && expectedStartOfNextWordX < positionX && lastPosition.getTextPosition().getCharacter() != null && !lastPosition.getTextPosition().getCharacter().endsWith(" ")) {
                        line.add(WordSeparator.getSeparator());
                    }
                }
                if (positionY >= maxYForLine) {
                    maxYForLine = positionY;
                }
                endOfLastTextX = positionX + positionWidth;
                if (characterValue != null) {
                    if (startOfPage && lastPosition == null) {
                        this.writeParagraphStart();
                    }
                    line.add(position2);
                }
                maxHeightForLine = Math.max(maxHeightForLine, positionHeight);
                minYTopForLine = Math.min(minYTopForLine, positionY - positionHeight);
                lastPosition = current;
                if (startOfPage) {
                    lastPosition.setParagraphStart();
                    lastPosition.setLineStart();
                    lastLineStartPosition = lastPosition;
                    startOfPage = false;
                }
                lastWordSpacing = wordSpacing;
                previousAveCharWidth = averageCharWidth;
            }
            if (line.size() > 0) {
                this.writeLine(this.normalize(line, isRtlDominant, hasRtl), isRtlDominant);
                this.writeParagraphEnd();
            }
            this.endArticle();
        }
        this.writePageEnd();
    }
    
    private boolean overlap(final float y1, final float height1, final float y2, final float height2) {
        return this.within(y1, y2, 0.1f) || (y2 <= y1 && y2 >= y1 - height1) || (y1 <= y2 && y1 >= y2 - height2);
    }
    
    protected void writePageSeperator() throws IOException {
        this.output.write(this.getPageSeparator());
        this.output.flush();
    }
    
    protected void writeLineSeparator() throws IOException {
        this.output.write(this.getLineSeparator());
    }
    
    protected void writeWordSeparator() throws IOException {
        this.output.write(this.getWordSeparator());
    }
    
    protected void writeCharacters(final TextPosition text) throws IOException {
        this.output.write(text.getCharacter());
    }
    
    protected void writeString(final String text) throws IOException {
        this.output.write(text);
    }
    
    private boolean within(final float first, final float second, final float variance) {
        return second < first + variance && second > first - variance;
    }
    
    @Override
    protected void processTextPosition(final TextPosition text) {
        boolean showCharacter = true;
        if (this.suppressDuplicateOverlappingText) {
            showCharacter = false;
            final String textCharacter = text.getCharacter();
            final float textX = text.getX();
            final float textY = text.getY();
            TreeMap<Float, TreeSet<Float>> sameTextCharacters = this.characterListMapping.get(textCharacter);
            if (sameTextCharacters == null) {
                sameTextCharacters = new TreeMap<Float, TreeSet<Float>>();
                this.characterListMapping.put(textCharacter, sameTextCharacters);
            }
            boolean suppressCharacter = false;
            final float tolerance = text.getWidth() / textCharacter.length() / 3.0f;
            final SortedMap<Float, TreeSet<Float>> xMatches = sameTextCharacters.subMap(textX - tolerance, textX + tolerance);
            for (final TreeSet<Float> xMatch : xMatches.values()) {
                final SortedSet<Float> yMatches = xMatch.subSet(textY - tolerance, textY + tolerance);
                if (!yMatches.isEmpty()) {
                    suppressCharacter = true;
                    break;
                }
            }
            if (!suppressCharacter) {
                TreeSet<Float> ySet = sameTextCharacters.get(textX);
                if (ySet == null) {
                    ySet = new TreeSet<Float>();
                    sameTextCharacters.put(textX, ySet);
                }
                ySet.add(textY);
                showCharacter = true;
            }
        }
        if (showCharacter) {
            int foundArticleDivisionIndex = -1;
            int notFoundButFirstLeftAndAboveArticleDivisionIndex = -1;
            int notFoundButFirstLeftArticleDivisionIndex = -1;
            int notFoundButFirstAboveArticleDivisionIndex = -1;
            final float x = text.getX();
            final float y = text.getY();
            if (this.shouldSeparateByBeads) {
                for (int i = 0; i < this.pageArticles.size() && foundArticleDivisionIndex == -1; ++i) {
                    final PDThreadBead bead = this.pageArticles.get(i);
                    if (bead != null) {
                        final PDRectangle rect = bead.getRectangle();
                        if (rect.contains(x, y)) {
                            foundArticleDivisionIndex = i * 2 + 1;
                        }
                        else if ((x < rect.getLowerLeftX() || y < rect.getUpperRightY()) && notFoundButFirstLeftAndAboveArticleDivisionIndex == -1) {
                            notFoundButFirstLeftAndAboveArticleDivisionIndex = i * 2;
                        }
                        else if (x < rect.getLowerLeftX() && notFoundButFirstLeftArticleDivisionIndex == -1) {
                            notFoundButFirstLeftArticleDivisionIndex = i * 2;
                        }
                        else if (y < rect.getUpperRightY() && notFoundButFirstAboveArticleDivisionIndex == -1) {
                            notFoundButFirstAboveArticleDivisionIndex = i * 2;
                        }
                    }
                    else {
                        foundArticleDivisionIndex = 0;
                    }
                }
            }
            else {
                foundArticleDivisionIndex = 0;
            }
            int articleDivisionIndex = -1;
            if (foundArticleDivisionIndex != -1) {
                articleDivisionIndex = foundArticleDivisionIndex;
            }
            else if (notFoundButFirstLeftAndAboveArticleDivisionIndex != -1) {
                articleDivisionIndex = notFoundButFirstLeftAndAboveArticleDivisionIndex;
            }
            else if (notFoundButFirstLeftArticleDivisionIndex != -1) {
                articleDivisionIndex = notFoundButFirstLeftArticleDivisionIndex;
            }
            else if (notFoundButFirstAboveArticleDivisionIndex != -1) {
                articleDivisionIndex = notFoundButFirstAboveArticleDivisionIndex;
            }
            else {
                articleDivisionIndex = this.charactersByArticle.size() - 1;
            }
            final List<TextPosition> textList = this.charactersByArticle.get(articleDivisionIndex);
            if (textList.isEmpty()) {
                textList.add(text);
            }
            else {
                final TextPosition previousTextPosition = textList.get(textList.size() - 1);
                if (text.isDiacritic() && previousTextPosition.contains(text)) {
                    previousTextPosition.mergeDiacritic(text, this.normalize);
                }
                else if (previousTextPosition.isDiacritic() && text.contains(previousTextPosition)) {
                    text.mergeDiacritic(previousTextPosition, this.normalize);
                    textList.remove(textList.size() - 1);
                    textList.add(text);
                }
                else {
                    textList.add(text);
                }
            }
        }
    }
    
    public int getStartPage() {
        return this.startPage;
    }
    
    public void setStartPage(final int startPageValue) {
        this.startPage = startPageValue;
    }
    
    public int getEndPage() {
        return this.endPage;
    }
    
    public void setEndPage(final int endPageValue) {
        this.endPage = endPageValue;
    }
    
    public void setLineSeparator(final String separator) {
        this.lineSeparator = separator;
    }
    
    public String getLineSeparator() {
        return this.lineSeparator;
    }
    
    public void setPageSeparator(final String separator) {
        this.pageSeparator = separator;
    }
    
    public String getWordSeparator() {
        return this.wordSeparator;
    }
    
    public void setWordSeparator(final String separator) {
        this.wordSeparator = separator;
    }
    
    public String getPageSeparator() {
        return this.pageSeparator;
    }
    
    public boolean getSuppressDuplicateOverlappingText() {
        return this.suppressDuplicateOverlappingText;
    }
    
    protected int getCurrentPageNo() {
        return this.currentPageNo;
    }
    
    protected Writer getOutput() {
        return this.output;
    }
    
    protected Vector<List<TextPosition>> getCharactersByArticle() {
        return this.charactersByArticle;
    }
    
    public void setSuppressDuplicateOverlappingText(final boolean suppressDuplicateOverlappingTextValue) {
        this.suppressDuplicateOverlappingText = suppressDuplicateOverlappingTextValue;
    }
    
    public boolean getSeparateByBeads() {
        return this.shouldSeparateByBeads;
    }
    
    public void setShouldSeparateByBeads(final boolean aShouldSeparateByBeads) {
        this.shouldSeparateByBeads = aShouldSeparateByBeads;
    }
    
    public PDOutlineItem getEndBookmark() {
        return this.endBookmark;
    }
    
    public void setEndBookmark(final PDOutlineItem aEndBookmark) {
        this.endBookmark = aEndBookmark;
    }
    
    public PDOutlineItem getStartBookmark() {
        return this.startBookmark;
    }
    
    public void setStartBookmark(final PDOutlineItem aStartBookmark) {
        this.startBookmark = aStartBookmark;
    }
    
    public boolean getAddMoreFormatting() {
        return this.addMoreFormatting;
    }
    
    public void setAddMoreFormatting(final boolean newAddMoreFormatting) {
        this.addMoreFormatting = newAddMoreFormatting;
    }
    
    public boolean getSortByPosition() {
        return this.sortByPosition;
    }
    
    public void setSortByPosition(final boolean newSortByPosition) {
        this.sortByPosition = newSortByPosition;
    }
    
    public float getSpacingTolerance() {
        return this.spacingTolerance;
    }
    
    public void setSpacingTolerance(final float spacingToleranceValue) {
        this.spacingTolerance = spacingToleranceValue;
    }
    
    public float getAverageCharTolerance() {
        return this.averageCharTolerance;
    }
    
    public void setAverageCharTolerance(final float averageCharToleranceValue) {
        this.averageCharTolerance = averageCharToleranceValue;
    }
    
    public float getIndentThreshold() {
        return this.indentThreshold;
    }
    
    public void setIndentThreshold(final float indentThresholdValue) {
        this.indentThreshold = indentThresholdValue;
    }
    
    public float getDropThreshold() {
        return this.dropThreshold;
    }
    
    public void setDropThreshold(final float dropThresholdValue) {
        this.dropThreshold = dropThresholdValue;
    }
    
    public String getParagraphStart() {
        return this.paragraphStart;
    }
    
    public void setParagraphStart(final String s) {
        this.paragraphStart = s;
    }
    
    public String getParagraphEnd() {
        return this.paragraphEnd;
    }
    
    public void setParagraphEnd(final String s) {
        this.paragraphEnd = s;
    }
    
    public String getPageStart() {
        return this.pageStart;
    }
    
    public void setPageStart(final String pageStartValue) {
        this.pageStart = pageStartValue;
    }
    
    public String getPageEnd() {
        return this.pageEnd;
    }
    
    public void setPageEnd(final String pageEndValue) {
        this.pageEnd = pageEndValue;
    }
    
    public String getArticleStart() {
        return this.articleStart;
    }
    
    public void setArticleStart(final String articleStartValue) {
        this.articleStart = articleStartValue;
    }
    
    public String getArticleEnd() {
        return this.articleEnd;
    }
    
    public void setArticleEnd(final String articleEndValue) {
        this.articleEnd = articleEndValue;
    }
    
    public String inspectFontEncoding(final String str) {
        if (!this.sortByPosition || str == null || str.length() < 2) {
            return str;
        }
        for (int i = 0; i < str.length(); ++i) {
            if (Character.getDirectionality(str.charAt(i)) != 2) {
                return str;
            }
        }
        final StringBuilder reversed = new StringBuilder(str.length());
        for (int j = str.length() - 1; j >= 0; --j) {
            reversed.append(str.charAt(j));
        }
        return reversed.toString();
    }
    
    protected PositionWrapper handleLineSeparation(final PositionWrapper current, final PositionWrapper lastPosition, PositionWrapper lastLineStartPosition, final float maxHeightForLine) throws IOException {
        current.setLineStart();
        this.isParagraphSeparation(current, lastPosition, lastLineStartPosition, maxHeightForLine);
        lastLineStartPosition = current;
        if (current.isParagraphStart()) {
            if (lastPosition.isArticleStart()) {
                this.writeParagraphStart();
            }
            else {
                this.writeLineSeparator();
                this.writeParagraphSeparator();
            }
        }
        else {
            this.writeLineSeparator();
        }
        return lastLineStartPosition;
    }
    
    protected void isParagraphSeparation(final PositionWrapper position, final PositionWrapper lastPosition, final PositionWrapper lastLineStartPosition, final float maxHeightForLine) {
        boolean result = false;
        if (lastLineStartPosition == null) {
            result = true;
        }
        else {
            final float yGap = Math.abs(position.getTextPosition().getYDirAdj() - lastPosition.getTextPosition().getYDirAdj());
            final float xGap = position.getTextPosition().getXDirAdj() - lastLineStartPosition.getTextPosition().getXDirAdj();
            if (yGap > this.getDropThreshold() * maxHeightForLine) {
                result = true;
            }
            else if (xGap > this.getIndentThreshold() * position.getTextPosition().getWidthOfSpace()) {
                if (!lastLineStartPosition.isParagraphStart()) {
                    result = true;
                }
                else {
                    position.setHangingIndent();
                }
            }
            else if (xGap < -position.getTextPosition().getWidthOfSpace()) {
                if (!lastLineStartPosition.isParagraphStart()) {
                    result = true;
                }
            }
            else if (Math.abs(xGap) < 0.25 * position.getTextPosition().getWidth()) {
                if (lastLineStartPosition.isHangingIndent()) {
                    position.setHangingIndent();
                }
                else if (lastLineStartPosition.isParagraphStart()) {
                    final Pattern liPattern = this.matchListItemPattern(lastLineStartPosition);
                    if (liPattern != null) {
                        final Pattern currentPattern = this.matchListItemPattern(position);
                        if (liPattern == currentPattern) {
                            result = true;
                        }
                    }
                }
            }
        }
        if (result) {
            position.setParagraphStart();
        }
    }
    
    protected void writeParagraphSeparator() throws IOException {
        this.writeParagraphEnd();
        this.writeParagraphStart();
    }
    
    protected void writeParagraphStart() throws IOException {
        this.output.write(this.getParagraphStart());
    }
    
    protected void writeParagraphEnd() throws IOException {
        this.output.write(this.getParagraphEnd());
    }
    
    protected void writePageStart() throws IOException {
        this.output.write(this.getPageStart());
    }
    
    protected void writePageEnd() throws IOException {
        this.output.write(this.getPageEnd());
    }
    
    protected Pattern matchListItemPattern(final PositionWrapper pw) {
        final TextPosition tp = pw.getTextPosition();
        final String txt = tp.getCharacter();
        final Pattern p = matchPattern(txt, this.getListItemPatterns());
        return p;
    }
    
    protected void setListItemPatterns(final List<Pattern> patterns) {
        this.liPatterns = patterns;
    }
    
    protected List<Pattern> getListItemPatterns() {
        if (this.liPatterns == null) {
            this.liPatterns = new ArrayList<Pattern>();
            for (final String expression : PDFTextStripper.LIST_ITEM_EXPRESSIONS) {
                final Pattern p = Pattern.compile(expression);
                this.liPatterns.add(p);
            }
        }
        return this.liPatterns;
    }
    
    protected static final Pattern matchPattern(final String s, final List<Pattern> patterns) {
        final Pattern matchedPattern = null;
        for (final Pattern p : patterns) {
            if (p.matcher(s).matches()) {
                return p;
            }
        }
        return matchedPattern;
    }
    
    private void writeLine(final List<String> line, final boolean isRtlDominant) throws IOException {
        final int numberOfStrings = line.size();
        if (isRtlDominant) {
            for (int i = numberOfStrings - 1; i >= 0; --i) {
                if (i < numberOfStrings - 1) {
                    this.writeWordSeparator();
                }
                this.writeString(line.get(i));
            }
        }
        else {
            for (int i = 0; i < numberOfStrings; ++i) {
                this.writeString(line.get(i));
                if (!isRtlDominant && i < numberOfStrings - 1) {
                    this.writeWordSeparator();
                }
            }
        }
    }
    
    private List<String> normalize(final List<TextPosition> line, final boolean isRtlDominant, final boolean hasRtl) {
        final LinkedList<String> normalized = new LinkedList<String>();
        StringBuilder lineBuilder = new StringBuilder();
        for (final TextPosition text : line) {
            if (text instanceof WordSeparator) {
                String lineStr = lineBuilder.toString();
                if (hasRtl) {
                    lineStr = this.normalize.makeLineLogicalOrder(lineStr, isRtlDominant);
                }
                lineStr = this.normalize.normalizePres(lineStr);
                normalized.add(lineStr);
                lineBuilder = new StringBuilder();
            }
            else {
                lineBuilder.append(text.getCharacter());
            }
        }
        if (lineBuilder.length() > 0) {
            String lineStr2 = lineBuilder.toString();
            if (hasRtl) {
                lineStr2 = this.normalize.makeLineLogicalOrder(lineStr2, isRtlDominant);
            }
            lineStr2 = this.normalize.normalizePres(lineStr2);
            normalized.add(lineStr2);
        }
        return normalized;
    }
    
    static {
        thisClassName = PDFTextStripper.class.getSimpleName().toLowerCase();
        PDFTextStripper.DEFAULT_INDENT_THRESHOLD = 2.0f;
        PDFTextStripper.DEFAULT_DROP_THRESHOLD = 2.5f;
        String prop = PDFTextStripper.thisClassName + ".indent";
        String s = System.getProperty(prop);
        if (s != null && s.length() > 0) {
            try {
                final float f = PDFTextStripper.DEFAULT_INDENT_THRESHOLD = Float.parseFloat(s);
            }
            catch (NumberFormatException ex) {}
        }
        prop = PDFTextStripper.thisClassName + ".drop";
        s = System.getProperty(prop);
        if (s != null && s.length() > 0) {
            try {
                final float f = PDFTextStripper.DEFAULT_DROP_THRESHOLD = Float.parseFloat(s);
            }
            catch (NumberFormatException ex2) {}
        }
        LIST_ITEM_EXPRESSIONS = new String[] { "\\.", "\\d+\\.", "\\[\\d+\\]", "\\d+\\)", "[A-Z]\\.", "[a-z]\\.", "[A-Z]\\)", "[a-z]\\)", "[IVXL]+\\.", "[ivxl]+\\." };
    }
    
    private static final class WordSeparator extends TextPosition
    {
        private static final WordSeparator separator;
        
        public static final WordSeparator getSeparator() {
            return WordSeparator.separator;
        }
        
        static {
            separator = new WordSeparator();
        }
    }
}
