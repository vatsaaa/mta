// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.util;

import org.apache.pdfbox.cos.COSArray;
import org.apache.pdfbox.pdmodel.common.COSArrayList;
import java.util.List;
import org.apache.pdfbox.pdmodel.graphics.color.PDColorSpaceFactory;
import java.io.IOException;
import java.util.Map;
import org.apache.pdfbox.pdmodel.graphics.color.PDColorSpace;
import org.apache.pdfbox.cos.COSNumber;
import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.cos.COSDictionary;

public class ImageParameters
{
    private COSDictionary dictionary;
    
    public ImageParameters() {
        this.dictionary = new COSDictionary();
    }
    
    public ImageParameters(final COSDictionary params) {
        this.dictionary = params;
    }
    
    public COSDictionary getDictionary() {
        return this.dictionary;
    }
    
    private COSBase getCOSObject(final COSName abbreviatedName, final COSName name) {
        COSBase retval = this.dictionary.getDictionaryObject(abbreviatedName);
        if (retval == null) {
            retval = this.dictionary.getDictionaryObject(name);
        }
        return retval;
    }
    
    private int getNumberOrNegativeOne(final COSName abbreviatedName, final COSName name) {
        int retval = -1;
        final COSNumber number = (COSNumber)this.getCOSObject(abbreviatedName, name);
        if (number != null) {
            retval = number.intValue();
        }
        return retval;
    }
    
    public int getBitsPerComponent() {
        return this.getNumberOrNegativeOne(COSName.BPC, COSName.BITS_PER_COMPONENT);
    }
    
    public void setBitsPerComponent(final int bpc) {
        this.dictionary.setInt(COSName.BPC, bpc);
    }
    
    public PDColorSpace getColorSpace() throws IOException {
        return this.getColorSpace(null);
    }
    
    public PDColorSpace getColorSpace(final Map colorSpaces) throws IOException {
        final COSBase cs = this.getCOSObject(COSName.CS, COSName.COLORSPACE);
        PDColorSpace retval = null;
        if (cs != null) {
            retval = PDColorSpaceFactory.createColorSpace(cs, colorSpaces);
        }
        return retval;
    }
    
    public void setColorSpace(final PDColorSpace cs) {
        COSBase base = null;
        if (cs != null) {
            base = cs.getCOSObject();
        }
        this.dictionary.setItem(COSName.CS, base);
    }
    
    public int getHeight() {
        return this.getNumberOrNegativeOne(COSName.H, COSName.HEIGHT);
    }
    
    public void setHeight(final int h) {
        this.dictionary.setInt(COSName.H, h);
    }
    
    public int getWidth() {
        return this.getNumberOrNegativeOne(COSName.W, COSName.WIDTH);
    }
    
    public void setWidth(final int w) {
        this.dictionary.setInt(COSName.W, w);
    }
    
    public List getFilters() {
        List retval = null;
        final COSBase filters = this.dictionary.getDictionaryObject(new String[] { "Filter", "F" });
        if (filters instanceof COSName) {
            final COSName name = (COSName)filters;
            retval = new COSArrayList(name.getName(), name, this.dictionary, COSName.FILTER);
        }
        else if (filters instanceof COSArray) {
            retval = COSArrayList.convertCOSNameCOSArrayToList((COSArray)filters);
        }
        return retval;
    }
    
    public void setFilters(final List filters) {
        final COSBase obj = COSArrayList.convertStringListToCOSNameCOSArray(filters);
        this.dictionary.setItem("Filter", obj);
    }
}
