// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.util;

import org.w3c.dom.NodeList;
import javax.imageio.metadata.IIOInvalidTreeException;
import org.w3c.dom.Node;
import javax.imageio.metadata.IIOMetadataNode;
import javax.imageio.ImageTypeSpecifier;
import javax.imageio.ImageWriteParam;
import java.util.Iterator;
import javax.imageio.stream.ImageOutputStream;
import javax.imageio.IIOException;
import javax.imageio.metadata.IIOMetadata;
import java.util.List;
import javax.imageio.IIOImage;
import java.awt.image.RenderedImage;
import javax.imageio.ImageWriter;
import javax.imageio.ImageIO;
import java.io.OutputStream;
import java.io.IOException;
import java.io.File;
import java.awt.image.BufferedImage;

public class ImageIOUtil
{
    private static final int DEFAULT_SCREEN_RESOLUTION = 72;
    private static final String STANDARD_METADATA_FORMAT = "javax_imageio_1.0";
    
    private ImageIOUtil() {
    }
    
    public static boolean writeImage(final BufferedImage image, final String imageFormat, final String filename, final int imageType, final int resolution) throws IOException {
        final String fileName = filename + "." + imageFormat;
        final File file = new File(fileName);
        return writeImage(image, imageFormat, file, resolution);
    }
    
    public static boolean writeImage(final BufferedImage image, final String imageFormat, final OutputStream outputStream) throws IOException {
        return writeImage(image, imageFormat, outputStream, 72);
    }
    
    private static boolean writeImage(final BufferedImage image, final String imageFormat, final Object outputStream, final int resolution) throws IOException {
        boolean bSuccess = true;
        ImageOutputStream output = null;
        ImageWriter imageWriter = null;
        try {
            output = ImageIO.createImageOutputStream(outputStream);
            boolean foundWriter = false;
            final Iterator<ImageWriter> writerIter = ImageIO.getImageWritersByFormatName(imageFormat);
            while (writerIter.hasNext() && !foundWriter) {
                try {
                    imageWriter = writerIter.next();
                    final ImageWriteParam writerParams = imageWriter.getDefaultWriteParam();
                    if (writerParams.canWriteCompressed()) {
                        writerParams.setCompressionMode(2);
                        if (writerParams.getCompressionType() == null) {
                            writerParams.setCompressionType(writerParams.getCompressionTypes()[0]);
                        }
                        writerParams.setCompressionQuality(1.0f);
                    }
                    final IIOMetadata meta = createMetadata(image, imageWriter, writerParams, resolution);
                    imageWriter.setOutput(output);
                    imageWriter.write(null, new IIOImage(image, null, meta), writerParams);
                    foundWriter = true;
                }
                catch (IIOException io) {
                    throw new IOException(io.getMessage());
                }
                finally {
                    if (imageWriter != null) {
                        imageWriter.dispose();
                    }
                }
            }
            if (!foundWriter) {
                bSuccess = false;
            }
        }
        finally {
            if (output != null) {
                output.flush();
                output.close();
            }
        }
        return bSuccess;
    }
    
    private static IIOMetadata createMetadata(final RenderedImage image, final ImageWriter imageWriter, final ImageWriteParam writerParams, final int resolution) {
        ImageTypeSpecifier type;
        if (writerParams.getDestinationType() != null) {
            type = writerParams.getDestinationType();
        }
        else {
            type = ImageTypeSpecifier.createFromRenderedImage(image);
        }
        final IIOMetadata meta = imageWriter.getDefaultImageMetadata(type, writerParams);
        return addResolution(meta, resolution) ? meta : null;
    }
    
    private static boolean addResolution(final IIOMetadata meta, final int resolution) {
        if (!meta.isReadOnly() && meta.isStandardMetadataFormatSupported()) {
            final IIOMetadataNode root = (IIOMetadataNode)meta.getAsTree("javax_imageio_1.0");
            IIOMetadataNode dim = getChildNode(root, "Dimension");
            if (dim == null) {
                dim = new IIOMetadataNode("Dimension");
                root.appendChild(dim);
            }
            IIOMetadataNode child = getChildNode(dim, "HorizontalPixelSize");
            if (child == null) {
                child = new IIOMetadataNode("HorizontalPixelSize");
                dim.appendChild(child);
            }
            child.setAttribute("value", Double.toString(resolution / 25.4));
            child = getChildNode(dim, "VerticalPixelSize");
            if (child == null) {
                child = new IIOMetadataNode("VerticalPixelSize");
                dim.appendChild(child);
            }
            child.setAttribute("value", Double.toString(resolution / 25.4));
            try {
                meta.mergeTree("javax_imageio_1.0", root);
            }
            catch (IIOInvalidTreeException e) {
                throw new RuntimeException("Cannot update image metadata: " + e.getMessage());
            }
            return true;
        }
        return false;
    }
    
    private static IIOMetadataNode getChildNode(final Node n, final String name) {
        final NodeList nodes = n.getChildNodes();
        for (int i = 0; i < nodes.getLength(); ++i) {
            final Node child = nodes.item(i);
            if (name.equals(child.getNodeName())) {
                return (IIOMetadataNode)child;
            }
        }
        return null;
    }
}
