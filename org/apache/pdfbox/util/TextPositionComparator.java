// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.util;

import java.util.Comparator;

public class TextPositionComparator implements Comparator
{
    public int compare(final Object o1, final Object o2) {
        int retval = 0;
        final TextPosition pos1 = (TextPosition)o1;
        final TextPosition pos2 = (TextPosition)o2;
        if (pos1.getDir() < pos2.getDir()) {
            return -1;
        }
        if (pos1.getDir() > pos2.getDir()) {
            return 1;
        }
        final float x1 = pos1.getXDirAdj();
        final float x2 = pos2.getXDirAdj();
        final float pos1YBottom = pos1.getYDirAdj();
        final float pos2YBottom = pos2.getYDirAdj();
        final float pos1YTop = pos1YBottom - pos1.getHeightDir();
        final float pos2YTop = pos2YBottom - pos2.getHeightDir();
        final float yDifference = Math.abs(pos1YBottom - pos2YBottom);
        if (yDifference < 0.1 || (pos2YBottom >= pos1YTop && pos2YBottom <= pos1YBottom) || (pos1YBottom >= pos2YTop && pos1YBottom <= pos2YBottom)) {
            if (x1 < x2) {
                retval = -1;
            }
            else if (x1 > x2) {
                retval = 1;
            }
            else {
                retval = 0;
            }
        }
        else {
            if (pos1YBottom >= pos2YBottom) {
                return 1;
            }
            retval = -1;
        }
        return retval;
    }
}
