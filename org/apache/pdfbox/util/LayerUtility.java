// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.util;

import java.util.Collection;
import java.util.HashSet;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Map;
import org.apache.pdfbox.pdmodel.PDDocumentCatalog;
import org.apache.pdfbox.pdmodel.graphics.xobject.PDXObject;
import org.apache.pdfbox.pdmodel.edit.PDPageContentStream;
import org.apache.pdfbox.pdmodel.markedcontent.PDPropertyList;
import org.apache.pdfbox.pdmodel.graphics.optionalcontent.PDOptionalContentProperties;
import org.apache.pdfbox.pdmodel.graphics.optionalcontent.PDOptionalContentGroup;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.fontbox.util.BoundingBox;
import java.awt.geom.AffineTransform;
import org.apache.pdfbox.pdmodel.common.COSObjectable;
import org.apache.pdfbox.pdmodel.PDResources;
import org.apache.pdfbox.pdmodel.common.PDStream;
import org.apache.pdfbox.pdmodel.graphics.xobject.PDXObjectForm;
import java.io.OutputStream;
import java.io.IOException;
import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSArray;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.cos.COSStream;
import org.apache.pdfbox.cos.COSDictionary;
import org.apache.pdfbox.pdmodel.PDPage;
import java.util.Set;
import org.apache.pdfbox.pdmodel.PDDocument;

public class LayerUtility
{
    private static final boolean DEBUG = true;
    private PDDocument targetDoc;
    private PDFCloneUtility cloner;
    private static final Set<String> PAGE_TO_FORM_FILTER;
    
    public LayerUtility(final PDDocument document) {
        this.targetDoc = document;
        this.cloner = new PDFCloneUtility(document);
    }
    
    public PDDocument getDocument() {
        return this.targetDoc;
    }
    
    public void wrapInSaveRestore(final PDPage page) throws IOException {
        final COSDictionary saveGraphicsStateDic = new COSDictionary();
        final COSStream saveGraphicsStateStream = new COSStream(saveGraphicsStateDic, this.getDocument().getDocument().getScratchFile());
        final OutputStream saveStream = saveGraphicsStateStream.createUnfilteredStream();
        saveStream.write("q\n".getBytes("ISO-8859-1"));
        saveStream.flush();
        final COSStream restoreGraphicsStateStream = new COSStream(saveGraphicsStateDic, this.getDocument().getDocument().getScratchFile());
        final OutputStream restoreStream = restoreGraphicsStateStream.createUnfilteredStream();
        restoreStream.write("Q\n".getBytes("ISO-8859-1"));
        restoreStream.flush();
        final COSDictionary pageDictionary = page.getCOSDictionary();
        final COSBase contents = pageDictionary.getDictionaryObject(COSName.CONTENTS);
        if (contents instanceof COSStream) {
            final COSStream contentsStream = (COSStream)contents;
            final COSArray array = new COSArray();
            array.add(saveGraphicsStateStream);
            array.add(contentsStream);
            array.add(restoreGraphicsStateStream);
            pageDictionary.setItem(COSName.CONTENTS, array);
        }
        else {
            if (!(contents instanceof COSArray)) {
                throw new IOException("Contents are unknown type: " + contents.getClass().getName());
            }
            final COSArray contentsArray = (COSArray)contents;
            contentsArray.add(0, saveGraphicsStateStream);
            contentsArray.add(restoreGraphicsStateStream);
        }
    }
    
    public PDXObjectForm importPageAsForm(final PDDocument sourceDoc, final int pageNumber) throws IOException {
        final PDPage page = sourceDoc.getDocumentCatalog().getAllPages().get(pageNumber);
        return this.importPageAsForm(sourceDoc, page);
    }
    
    public PDXObjectForm importPageAsForm(final PDDocument sourceDoc, final PDPage page) throws IOException {
        final COSStream pageStream = (COSStream)page.getContents().getCOSObject();
        final PDStream newStream = new PDStream(this.targetDoc, pageStream.getUnfilteredStream(), false);
        final PDXObjectForm form = new PDXObjectForm(newStream);
        final PDResources pageRes = page.findResources();
        final PDResources formRes = new PDResources();
        this.cloner.cloneMerge(pageRes, formRes);
        form.setResources(formRes);
        this.transferDict(page.getCOSDictionary(), form.getCOSStream(), LayerUtility.PAGE_TO_FORM_FILTER, true);
        final Matrix matrix = form.getMatrix();
        final AffineTransform at = (matrix != null) ? matrix.createAffineTransform() : new AffineTransform();
        final PDRectangle mediaBox = page.findMediaBox();
        final PDRectangle cropBox = page.findCropBox();
        final PDRectangle viewBox = (cropBox != null) ? cropBox : mediaBox;
        final int rotation = getNormalizedRotation(page);
        at.translate(mediaBox.getLowerLeftX() - viewBox.getLowerLeftX(), mediaBox.getLowerLeftY() - viewBox.getLowerLeftY());
        switch (rotation) {
            case 90: {
                at.scale(viewBox.getWidth() / viewBox.getHeight(), viewBox.getHeight() / viewBox.getWidth());
                at.translate(0.0, viewBox.getWidth());
                at.rotate(-1.5707963267948966);
                break;
            }
            case 180: {
                at.translate(viewBox.getWidth(), viewBox.getHeight());
                at.rotate(-3.141592653589793);
                break;
            }
            case 270: {
                at.scale(viewBox.getWidth() / viewBox.getHeight(), viewBox.getHeight() / viewBox.getWidth());
                at.translate(viewBox.getHeight(), 0.0);
                at.rotate(-4.71238898038469);
                break;
            }
        }
        at.translate(-viewBox.getLowerLeftX(), -viewBox.getLowerLeftY());
        if (!at.isIdentity()) {
            form.setMatrix(at);
        }
        final BoundingBox bbox = new BoundingBox();
        bbox.setLowerLeftX(viewBox.getLowerLeftX());
        bbox.setLowerLeftY(viewBox.getLowerLeftY());
        bbox.setUpperRightX(viewBox.getUpperRightX());
        bbox.setUpperRightY(viewBox.getUpperRightY());
        form.setBBox(new PDRectangle(bbox));
        return form;
    }
    
    public PDOptionalContentGroup appendFormAsLayer(final PDPage targetPage, final PDXObjectForm form, final AffineTransform transform, final String layerName) throws IOException {
        final PDDocumentCatalog catalog = this.targetDoc.getDocumentCatalog();
        PDOptionalContentProperties ocprops = catalog.getOCProperties();
        if (ocprops == null) {
            ocprops = new PDOptionalContentProperties();
            catalog.setOCProperties(ocprops);
        }
        if (ocprops.hasGroup(layerName)) {
            throw new IllegalArgumentException("Optional group (layer) already exists: " + layerName);
        }
        final PDOptionalContentGroup layer = new PDOptionalContentGroup(layerName);
        ocprops.addGroup(layer);
        final PDResources resources = targetPage.findResources();
        PDPropertyList props = resources.getProperties();
        if (props == null) {
            props = new PDPropertyList();
            resources.setProperties(props);
        }
        int index = 0;
        PDOptionalContentGroup ocg;
        COSName resourceName;
        do {
            resourceName = COSName.getPDFName("MC" + index);
            ocg = props.getOptionalContentGroup(resourceName);
            ++index;
        } while (ocg != null);
        props.putMapping(resourceName, layer);
        final PDPageContentStream contentStream = new PDPageContentStream(this.targetDoc, targetPage, true, false);
        contentStream.beginMarkedContentSequence(COSName.OC, resourceName);
        contentStream.drawXObject(form, transform);
        contentStream.endMarkedContentSequence();
        contentStream.close();
        return layer;
    }
    
    private void transferDict(final COSDictionary orgDict, final COSDictionary targetDict, final Set<String> filter, final boolean inclusive) throws IOException {
        for (final Map.Entry<COSName, COSBase> entry : orgDict.entrySet()) {
            final COSName key = entry.getKey();
            if (inclusive && !filter.contains(key.getName())) {
                continue;
            }
            if (!inclusive && filter.contains(key.getName())) {
                continue;
            }
            targetDict.setItem(key, this.cloner.cloneForNewDocument(entry.getValue()));
        }
    }
    
    private static int getNormalizedRotation(final PDPage page) {
        int rotation;
        for (rotation = page.findRotation(); rotation >= 360; rotation -= 360) {}
        if (rotation < 0) {
            rotation = 0;
        }
        switch (rotation) {
            case 90:
            case 180:
            case 270: {
                return rotation;
            }
            default: {
                return 0;
            }
        }
    }
    
    static {
        PAGE_TO_FORM_FILTER = new HashSet<String>(Arrays.asList("Group", "LastModified", "Metadata"));
    }
}
