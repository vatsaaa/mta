// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.util;

import java.awt.image.BufferedImage;
import java.util.List;
import org.apache.pdfbox.pdmodel.PDPage;
import java.awt.HeadlessException;
import java.awt.Toolkit;
import org.apache.pdfbox.pdmodel.PDDocument;
import java.io.IOException;
import java.util.Properties;

public class PDFImageWriter extends PDFStreamEngine
{
    public PDFImageWriter() {
    }
    
    public PDFImageWriter(final Properties props) throws IOException {
        super(props);
    }
    
    public boolean writeImage(final PDDocument document, final String imageType, final String password, final int startPage, final int endPage, final String outputPrefix) throws IOException {
        int resolution;
        try {
            resolution = Toolkit.getDefaultToolkit().getScreenResolution();
        }
        catch (HeadlessException e) {
            resolution = 96;
        }
        return this.writeImage(document, imageType, password, startPage, endPage, outputPrefix, 8, resolution);
    }
    
    public boolean writeImage(final PDDocument document, final String imageFormat, final String password, final int startPage, final int endPage, final String outputPrefix, final int imageType, final int resolution) throws IOException {
        boolean bSuccess = true;
        final List pages = document.getDocumentCatalog().getAllPages();
        for (int i = startPage - 1; i < endPage && i < pages.size(); ++i) {
            final PDPage page = pages.get(i);
            final BufferedImage image = page.convertToImage(imageType, resolution);
            final String fileName = outputPrefix + (i + 1);
            System.out.println("Writing: " + fileName + "." + imageFormat);
            bSuccess &= ImageIOUtil.writeImage(image, imageFormat, fileName, imageType, resolution);
        }
        return bSuccess;
    }
}
