// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.util;

import org.apache.pdfbox.pdmodel.graphics.xobject.PDXObject;
import org.apache.pdfbox.cos.COSDictionary;
import org.apache.pdfbox.cos.COSName;
import java.util.Properties;
import java.io.IOException;
import java.util.HashMap;
import java.util.ArrayList;
import java.util.Map;
import java.util.Stack;
import org.apache.pdfbox.pdmodel.documentinterchange.markedcontent.PDMarkedContent;
import java.util.List;

public class PDFMarkedContentExtractor extends PDFStreamEngine
{
    private boolean suppressDuplicateOverlappingText;
    private List<PDMarkedContent> markedContents;
    private Stack<PDMarkedContent> currentMarkedContents;
    private Map<String, List<TextPosition>> characterListMapping;
    protected String outputEncoding;
    private TextNormalize normalize;
    
    public PDFMarkedContentExtractor() throws IOException {
        super(ResourceLoader.loadProperties("org/apache/pdfbox/resources/PDFMarkedContentExtractor.properties", true));
        this.suppressDuplicateOverlappingText = true;
        this.markedContents = new ArrayList<PDMarkedContent>();
        this.currentMarkedContents = new Stack<PDMarkedContent>();
        this.characterListMapping = new HashMap<String, List<TextPosition>>();
        this.normalize = null;
        this.outputEncoding = null;
        this.normalize = new TextNormalize(this.outputEncoding);
    }
    
    public PDFMarkedContentExtractor(final Properties props) throws IOException {
        super(props);
        this.suppressDuplicateOverlappingText = true;
        this.markedContents = new ArrayList<PDMarkedContent>();
        this.currentMarkedContents = new Stack<PDMarkedContent>();
        this.characterListMapping = new HashMap<String, List<TextPosition>>();
        this.normalize = null;
        this.outputEncoding = null;
        this.normalize = new TextNormalize(this.outputEncoding);
    }
    
    public PDFMarkedContentExtractor(final String encoding) throws IOException {
        super(ResourceLoader.loadProperties("org/apache/pdfbox/resources/PDFMarkedContentExtractor.properties", true));
        this.suppressDuplicateOverlappingText = true;
        this.markedContents = new ArrayList<PDMarkedContent>();
        this.currentMarkedContents = new Stack<PDMarkedContent>();
        this.characterListMapping = new HashMap<String, List<TextPosition>>();
        this.normalize = null;
        this.outputEncoding = encoding;
        this.normalize = new TextNormalize(this.outputEncoding);
    }
    
    private boolean within(final float first, final float second, final float variance) {
        return second > first - variance && second < first + variance;
    }
    
    public void beginMarkedContentSequence(final COSName tag, final COSDictionary properties) {
        final PDMarkedContent markedContent = PDMarkedContent.create(tag, properties);
        if (this.currentMarkedContents.isEmpty()) {
            this.markedContents.add(markedContent);
        }
        else {
            final PDMarkedContent currentMarkedContent = this.currentMarkedContents.peek();
            if (currentMarkedContent != null) {
                currentMarkedContent.addMarkedContent(markedContent);
            }
        }
        this.currentMarkedContents.push(markedContent);
    }
    
    public void endMarkedContentSequence() {
        if (!this.currentMarkedContents.isEmpty()) {
            this.currentMarkedContents.pop();
        }
    }
    
    public void xobject(final PDXObject xobject) {
        if (!this.currentMarkedContents.isEmpty()) {
            this.currentMarkedContents.peek().addXObject(xobject);
        }
    }
    
    @Override
    protected void processTextPosition(final TextPosition text) {
        boolean showCharacter = true;
        if (this.suppressDuplicateOverlappingText) {
            showCharacter = false;
            final String textCharacter = text.getCharacter();
            final float textX = text.getX();
            final float textY = text.getY();
            List<TextPosition> sameTextCharacters = this.characterListMapping.get(textCharacter);
            if (sameTextCharacters == null) {
                sameTextCharacters = new ArrayList<TextPosition>();
                this.characterListMapping.put(textCharacter, sameTextCharacters);
            }
            boolean suppressCharacter = false;
            final float tolerance = text.getWidth() / textCharacter.length() / 3.0f;
            for (int i = 0; i < sameTextCharacters.size() && textCharacter != null; ++i) {
                final TextPosition character = sameTextCharacters.get(i);
                final String charCharacter = character.getCharacter();
                final float charX = character.getX();
                final float charY = character.getY();
                if (charCharacter != null && this.within(charX, textX, tolerance) && this.within(charY, textY, tolerance)) {
                    suppressCharacter = true;
                }
            }
            if (!suppressCharacter) {
                sameTextCharacters.add(text);
                showCharacter = true;
            }
        }
        if (showCharacter) {
            final List<TextPosition> textList = new ArrayList<TextPosition>();
            if (textList.isEmpty()) {
                textList.add(text);
            }
            else {
                final TextPosition previousTextPosition = textList.get(textList.size() - 1);
                if (text.isDiacritic() && previousTextPosition.contains(text)) {
                    previousTextPosition.mergeDiacritic(text, this.normalize);
                }
                else if (previousTextPosition.isDiacritic() && text.contains(previousTextPosition)) {
                    text.mergeDiacritic(previousTextPosition, this.normalize);
                    textList.remove(textList.size() - 1);
                    textList.add(text);
                }
                else {
                    textList.add(text);
                }
            }
            if (!this.currentMarkedContents.isEmpty()) {
                this.currentMarkedContents.peek().addText(text);
            }
        }
    }
    
    public List<PDMarkedContent> getMarkedContents() {
        return this.markedContents;
    }
}
