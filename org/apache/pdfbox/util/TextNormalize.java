// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.util;

import java.util.HashMap;

public class TextNormalize
{
    private ICU4JImpl icu4j;
    private static final HashMap DIACHASH;
    private String outputEncoding;
    
    public TextNormalize(final String encoding) {
        this.icu4j = null;
        this.findICU4J();
        this.populateDiacHash();
        this.outputEncoding = encoding;
    }
    
    private void findICU4J() {
        try {
            this.getClass().getClassLoader().loadClass("com.ibm.icu.text.Bidi");
            this.getClass().getClassLoader().loadClass("com.ibm.icu.text.Normalizer");
            this.icu4j = new ICU4JImpl();
        }
        catch (ClassNotFoundException e) {
            this.icu4j = null;
        }
    }
    
    private void populateDiacHash() {
        TextNormalize.DIACHASH.put(new Integer(96), "\u0300");
        TextNormalize.DIACHASH.put(new Integer(715), "\u0300");
        TextNormalize.DIACHASH.put(new Integer(39), "\u0301");
        TextNormalize.DIACHASH.put(new Integer(697), "\u0301");
        TextNormalize.DIACHASH.put(new Integer(714), "\u0301");
        TextNormalize.DIACHASH.put(new Integer(94), "\u0302");
        TextNormalize.DIACHASH.put(new Integer(710), "\u0302");
        TextNormalize.DIACHASH.put(new Integer(126), "\u0303");
        TextNormalize.DIACHASH.put(new Integer(713), "\u0304");
        TextNormalize.DIACHASH.put(new Integer(176), "\u030a");
        TextNormalize.DIACHASH.put(new Integer(698), "\u030b");
        TextNormalize.DIACHASH.put(new Integer(711), "\u030c");
        TextNormalize.DIACHASH.put(new Integer(712), "\u030d");
        TextNormalize.DIACHASH.put(new Integer(34), "\u030e");
        TextNormalize.DIACHASH.put(new Integer(699), "\u0312");
        TextNormalize.DIACHASH.put(new Integer(700), "\u0313");
        TextNormalize.DIACHASH.put(new Integer(1158), "\u0313");
        TextNormalize.DIACHASH.put(new Integer(1370), "\u0313");
        TextNormalize.DIACHASH.put(new Integer(701), "\u0314");
        TextNormalize.DIACHASH.put(new Integer(1157), "\u0314");
        TextNormalize.DIACHASH.put(new Integer(1369), "\u0314");
        TextNormalize.DIACHASH.put(new Integer(724), "\u031d");
        TextNormalize.DIACHASH.put(new Integer(725), "\u031e");
        TextNormalize.DIACHASH.put(new Integer(726), "\u031f");
        TextNormalize.DIACHASH.put(new Integer(727), "\u0320");
        TextNormalize.DIACHASH.put(new Integer(690), "\u0321");
        TextNormalize.DIACHASH.put(new Integer(716), "\u0329");
        TextNormalize.DIACHASH.put(new Integer(695), "\u032b");
        TextNormalize.DIACHASH.put(new Integer(717), "\u0331");
        TextNormalize.DIACHASH.put(new Integer(95), "\u0332");
        TextNormalize.DIACHASH.put(new Integer(8270), "\u0359");
    }
    
    public String makeLineLogicalOrder(final String str, final boolean isRtlDominant) {
        if (this.icu4j != null) {
            return this.icu4j.makeLineLogicalOrder(str, isRtlDominant);
        }
        return str;
    }
    
    public String normalizePres(final String str) {
        if (this.icu4j != null) {
            return this.icu4j.normalizePres(str);
        }
        return str;
    }
    
    public String normalizeDiac(final String str) {
        if (this.outputEncoding == null || !this.outputEncoding.toUpperCase().startsWith("UTF")) {
            return str;
        }
        final Integer c = new Integer(str.charAt(0));
        if (TextNormalize.DIACHASH.containsKey(c)) {
            return TextNormalize.DIACHASH.get(c);
        }
        if (this.icu4j != null) {
            return this.icu4j.normalizeDiac(str);
        }
        return str;
    }
    
    static {
        DIACHASH = new HashMap();
    }
}
