// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.util;

import java.awt.geom.AffineTransform;

public class Matrix implements Cloneable
{
    static final float[] DEFAULT_SINGLE;
    private float[] single;
    
    public Matrix() {
        this.single = new float[Matrix.DEFAULT_SINGLE.length];
        this.reset();
    }
    
    public void reset() {
        System.arraycopy(Matrix.DEFAULT_SINGLE, 0, this.single, 0, Matrix.DEFAULT_SINGLE.length);
    }
    
    public AffineTransform createAffineTransform() {
        final AffineTransform retval = new AffineTransform(this.single[0], this.single[1], this.single[3], this.single[4], this.single[6], this.single[7]);
        return retval;
    }
    
    public void setFromAffineTransform(final AffineTransform af) {
        this.single[0] = (float)af.getScaleX();
        this.single[1] = (float)af.getShearY();
        this.single[3] = (float)af.getShearX();
        this.single[4] = (float)af.getScaleY();
        this.single[6] = (float)af.getTranslateX();
        this.single[7] = (float)af.getTranslateY();
    }
    
    public float getValue(final int row, final int column) {
        return this.single[row * 3 + column];
    }
    
    public void setValue(final int row, final int column, final float value) {
        this.single[row * 3 + column] = value;
    }
    
    public float[][] getValues() {
        final float[][] retval = new float[3][3];
        retval[0][0] = this.single[0];
        retval[0][1] = this.single[1];
        retval[0][2] = this.single[2];
        retval[1][0] = this.single[3];
        retval[1][1] = this.single[4];
        retval[1][2] = this.single[5];
        retval[2][0] = this.single[6];
        retval[2][1] = this.single[7];
        retval[2][2] = this.single[8];
        return retval;
    }
    
    public double[][] getValuesAsDouble() {
        final double[][] retval = new double[3][3];
        retval[0][0] = this.single[0];
        retval[0][1] = this.single[1];
        retval[0][2] = this.single[2];
        retval[1][0] = this.single[3];
        retval[1][1] = this.single[4];
        retval[1][2] = this.single[5];
        retval[2][0] = this.single[6];
        retval[2][1] = this.single[7];
        retval[2][2] = this.single[8];
        return retval;
    }
    
    public Matrix multiply(final Matrix b) {
        return this.multiply(b, new Matrix());
    }
    
    public Matrix multiply(final Matrix other, Matrix result) {
        if (result == null) {
            result = new Matrix();
        }
        if (other != null && other.single != null) {
            float[] thisOperand = this.single;
            float[] otherOperand = other.single;
            if (this == result) {
                final float[] thisOrigVals = new float[this.single.length];
                System.arraycopy(this.single, 0, thisOrigVals, 0, this.single.length);
                thisOperand = thisOrigVals;
            }
            if (other == result) {
                final float[] otherOrigVals = new float[other.single.length];
                System.arraycopy(other.single, 0, otherOrigVals, 0, other.single.length);
                otherOperand = otherOrigVals;
            }
            result.single[0] = thisOperand[0] * otherOperand[0] + thisOperand[1] * otherOperand[3] + thisOperand[2] * otherOperand[6];
            result.single[1] = thisOperand[0] * otherOperand[1] + thisOperand[1] * otherOperand[4] + thisOperand[2] * otherOperand[7];
            result.single[2] = thisOperand[0] * otherOperand[2] + thisOperand[1] * otherOperand[5] + thisOperand[2] * otherOperand[8];
            result.single[3] = thisOperand[3] * otherOperand[0] + thisOperand[4] * otherOperand[3] + thisOperand[5] * otherOperand[6];
            result.single[4] = thisOperand[3] * otherOperand[1] + thisOperand[4] * otherOperand[4] + thisOperand[5] * otherOperand[7];
            result.single[5] = thisOperand[3] * otherOperand[2] + thisOperand[4] * otherOperand[5] + thisOperand[5] * otherOperand[8];
            result.single[6] = thisOperand[6] * otherOperand[0] + thisOperand[7] * otherOperand[3] + thisOperand[8] * otherOperand[6];
            result.single[7] = thisOperand[6] * otherOperand[1] + thisOperand[7] * otherOperand[4] + thisOperand[8] * otherOperand[7];
            result.single[8] = thisOperand[6] * otherOperand[2] + thisOperand[7] * otherOperand[5] + thisOperand[8] * otherOperand[8];
        }
        return result;
    }
    
    public Matrix extractScaling() {
        final Matrix retval = new Matrix();
        retval.single[0] = this.single[0];
        retval.single[4] = this.single[4];
        return retval;
    }
    
    public static Matrix getScaleInstance(final float x, final float y) {
        final Matrix retval = new Matrix();
        retval.single[0] = x;
        retval.single[4] = y;
        return retval;
    }
    
    public Matrix extractTranslating() {
        final Matrix retval = new Matrix();
        retval.single[6] = this.single[6];
        retval.single[7] = this.single[7];
        return retval;
    }
    
    public static Matrix getTranslatingInstance(final float x, final float y) {
        final Matrix retval = new Matrix();
        retval.single[6] = x;
        retval.single[7] = y;
        return retval;
    }
    
    public Object clone() {
        final Matrix clone = new Matrix();
        System.arraycopy(this.single, 0, clone.single, 0, 9);
        return clone;
    }
    
    public Matrix copy() {
        return (Matrix)this.clone();
    }
    
    @Override
    public String toString() {
        final StringBuffer result = new StringBuffer("");
        result.append("[[");
        result.append(this.single[0] + ",");
        result.append(this.single[1] + ",");
        result.append(this.single[2] + "][");
        result.append(this.single[3] + ",");
        result.append(this.single[4] + ",");
        result.append(this.single[5] + "][");
        result.append(this.single[6] + ",");
        result.append(this.single[7] + ",");
        result.append(this.single[8] + "]]");
        return result.toString();
    }
    
    public float getXScale() {
        float xScale = this.single[0];
        if (this.single[1] != 0.0f || this.single[3] != 0.0f) {
            xScale = (float)Math.sqrt(Math.pow(this.single[0], 2.0) + Math.pow(this.single[1], 2.0));
        }
        return xScale;
    }
    
    public float getYScale() {
        float yScale = this.single[4];
        if (this.single[1] != 0.0f || this.single[3] != 0.0f) {
            yScale = (float)Math.sqrt(Math.pow(this.single[3], 2.0) + Math.pow(this.single[4], 2.0));
        }
        return yScale;
    }
    
    public float getXPosition() {
        return this.single[6];
    }
    
    public float getYPosition() {
        return this.single[7];
    }
    
    static {
        DEFAULT_SINGLE = new float[] { 1.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f };
    }
}
