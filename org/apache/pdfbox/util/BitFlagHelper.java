// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.util;

import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.cos.COSDictionary;

public class BitFlagHelper
{
    private BitFlagHelper() {
    }
    
    @Deprecated
    public static final void setFlag(final COSDictionary dic, final String field, final int bitFlag, final boolean value) {
        setFlag(dic, COSName.getPDFName(field), bitFlag, value);
    }
    
    public static final void setFlag(final COSDictionary dic, final COSName field, final int bitFlag, final boolean value) {
        int currentFlags = dic.getInt(field, 0);
        if (value) {
            currentFlags |= bitFlag;
        }
        else {
            currentFlags = (currentFlags &= ~bitFlag);
        }
        dic.setInt(field, currentFlags);
    }
    
    @Deprecated
    public static final boolean getFlag(final COSDictionary dic, final String field, final int bitFlag) {
        return getFlag(dic, COSName.getPDFName(field), bitFlag);
    }
    
    public static final boolean getFlag(final COSDictionary dic, final COSName field, final int bitFlag) {
        final int ff = dic.getInt(field, 0);
        return (ff & bitFlag) == bitFlag;
    }
}
