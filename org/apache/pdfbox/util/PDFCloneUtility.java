// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.util;

import java.io.IOException;
import java.util.Iterator;
import org.apache.pdfbox.cos.COSDictionary;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.pdmodel.common.PDStream;
import org.apache.pdfbox.cos.COSStream;
import org.apache.pdfbox.cos.COSObject;
import org.apache.pdfbox.pdmodel.common.COSObjectable;
import org.apache.pdfbox.cos.COSArray;
import java.util.List;
import java.util.HashMap;
import org.apache.pdfbox.cos.COSBase;
import java.util.Map;
import org.apache.pdfbox.pdmodel.PDDocument;

public class PDFCloneUtility
{
    private PDDocument destination;
    private Map<Object, COSBase> clonedVersion;
    
    public PDFCloneUtility(final PDDocument dest) {
        this.clonedVersion = new HashMap<Object, COSBase>();
        this.destination = dest;
    }
    
    public PDDocument getDestination() {
        return this.destination;
    }
    
    public COSBase cloneForNewDocument(final Object base) throws IOException {
        if (base == null) {
            return null;
        }
        COSBase retval = this.clonedVersion.get(base);
        if (retval == null) {
            if (base instanceof List) {
                final COSArray array = new COSArray();
                final List list = (List)base;
                for (int i = 0; i < list.size(); ++i) {
                    array.add(this.cloneForNewDocument(list.get(i)));
                }
                retval = array;
            }
            else if (base instanceof COSObjectable && !(base instanceof COSBase)) {
                retval = this.cloneForNewDocument(((COSObjectable)base).getCOSObject());
                this.clonedVersion.put(base, retval);
            }
            else if (base instanceof COSObject) {
                final COSObject object = (COSObject)base;
                retval = this.cloneForNewDocument(object.getObject());
                this.clonedVersion.put(base, retval);
            }
            else if (base instanceof COSArray) {
                final COSArray newArray = new COSArray();
                final COSArray array2 = (COSArray)base;
                for (int i = 0; i < array2.size(); ++i) {
                    newArray.add(this.cloneForNewDocument(array2.get(i)));
                }
                retval = newArray;
                this.clonedVersion.put(base, retval);
            }
            else if (base instanceof COSStream) {
                final COSStream originalStream = (COSStream)base;
                final PDStream stream = new PDStream(this.destination, originalStream.getFilteredStream(), true);
                this.clonedVersion.put(base, stream.getStream());
                for (final Map.Entry<COSName, COSBase> entry : originalStream.entrySet()) {
                    stream.getStream().setItem(entry.getKey(), this.cloneForNewDocument(entry.getValue()));
                }
                retval = stream.getStream();
            }
            else if (base instanceof COSDictionary) {
                final COSDictionary dic = (COSDictionary)base;
                retval = new COSDictionary();
                this.clonedVersion.put(base, retval);
                for (final Map.Entry<COSName, COSBase> entry2 : dic.entrySet()) {
                    ((COSDictionary)retval).setItem(entry2.getKey(), this.cloneForNewDocument(entry2.getValue()));
                }
            }
            else {
                retval = (COSBase)base;
            }
        }
        this.clonedVersion.put(base, retval);
        return retval;
    }
    
    public void cloneMerge(final COSObjectable base, COSObjectable target) throws IOException {
        if (base == null) {
            return;
        }
        COSBase retval = this.clonedVersion.get(base);
        if (retval != null) {
            return;
        }
        if (base instanceof List) {
            final COSArray array = new COSArray();
            final List list = (List)base;
            for (int i = 0; i < list.size(); ++i) {
                array.add(this.cloneForNewDocument(list.get(i)));
            }
            ((List)target).add(array);
        }
        else if (base instanceof COSObjectable && !(base instanceof COSBase)) {
            this.cloneMerge(base.getCOSObject(), target.getCOSObject());
            this.clonedVersion.put(base, retval);
        }
        else if (base instanceof COSObject) {
            if (target instanceof COSObject) {
                this.cloneMerge(((COSObject)base).getObject(), ((COSObject)target).getObject());
            }
            else if (target instanceof COSDictionary) {
                this.cloneMerge(((COSObject)base).getObject(), target);
            }
            this.clonedVersion.put(base, retval);
        }
        else if (base instanceof COSArray) {
            final COSArray array = (COSArray)base;
            for (int j = 0; j < array.size(); ++j) {
                ((COSArray)target).add(this.cloneForNewDocument(array.get(j)));
            }
            this.clonedVersion.put(base, retval);
        }
        else if (base instanceof COSStream) {
            final COSStream originalStream = (COSStream)base;
            final PDStream stream = new PDStream(this.destination, originalStream.getFilteredStream(), true);
            this.clonedVersion.put(base, stream.getStream());
            for (final Map.Entry<COSName, COSBase> entry : originalStream.entrySet()) {
                stream.getStream().setItem(entry.getKey(), this.cloneForNewDocument(entry.getValue()));
            }
            retval = (COSBase)(target = stream.getStream());
        }
        else if (base instanceof COSDictionary) {
            final COSDictionary dic = (COSDictionary)base;
            this.clonedVersion.put(base, retval);
            for (final Map.Entry<COSName, COSBase> entry2 : dic.entrySet()) {
                final COSName key = entry2.getKey();
                final COSBase value = entry2.getValue();
                if (((COSDictionary)target).getItem(key) != null) {
                    this.cloneMerge(value, ((COSDictionary)target).getItem(key));
                }
                else {
                    ((COSDictionary)target).setItem(key, this.cloneForNewDocument(value));
                }
            }
        }
        else {
            retval = (COSBase)base;
        }
        this.clonedVersion.put(base, retval);
    }
}
