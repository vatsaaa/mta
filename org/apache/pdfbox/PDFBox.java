// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox;

public class PDFBox
{
    public static void main(final String[] args) {
        if (args.length > 0) {
            final String command = args[0];
            final String[] arguments = new String[args.length - 1];
            System.arraycopy(args, 1, arguments, 0, arguments.length);
            boolean exitAfterCallingMain = true;
            try {
                if (command.equals("ConvertColorspace")) {
                    ConvertColorspace.main(arguments);
                }
                else if (command.equals("Decrypt")) {
                    Decrypt.main(arguments);
                }
                else if (command.equals("Encrypt")) {
                    Encrypt.main(arguments);
                }
                else if (command.equals("ExtractText")) {
                    ExtractText.main(arguments);
                }
                else if (command.equals("Overlay")) {
                    Overlay.main(arguments);
                }
                else if (command.equals("PrintPDF")) {
                    PrintPDF.main(arguments);
                }
                else if (command.equals("PDFDebugger")) {
                    PDFDebugger.main(arguments);
                    exitAfterCallingMain = false;
                }
                else if (command.equals("PDFMerger")) {
                    PDFMerger.main(arguments);
                }
                else if (command.equals("PDFReader")) {
                    PDFReader.main(arguments);
                    exitAfterCallingMain = false;
                }
                else if (command.equals("PDFSplit")) {
                    PDFSplit.main(arguments);
                }
                else if (command.equals("PDFToImage")) {
                    PDFToImage.main(arguments);
                }
                else if (command.equals("TextToPDF")) {
                    TextToPDF.main(arguments);
                }
                else if (command.equals("WriteDecodedDoc")) {
                    WriteDecodedDoc.main(arguments);
                }
                else {
                    showMessageAndExit();
                }
                if (exitAfterCallingMain) {
                    System.exit(0);
                }
            }
            catch (Exception e) {
                System.err.println(command + " failed with the following exception:");
                e.printStackTrace();
                System.exit(1);
            }
        }
        else {
            showMessageAndExit();
        }
    }
    
    private static void showMessageAndExit() {
        System.err.println("usage: java pdfbox-app-x.y.z.jar <command> <args..>");
        System.exit(1);
    }
}
