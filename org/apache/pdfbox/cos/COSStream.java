// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.cos;

import java.io.BufferedOutputStream;
import org.apache.pdfbox.filter.Filter;
import org.apache.pdfbox.filter.FilterManager;
import java.io.OutputStream;
import org.apache.pdfbox.exceptions.COSVisitorException;
import java.io.ByteArrayInputStream;
import java.io.BufferedInputStream;
import org.apache.pdfbox.io.RandomAccessFileInputStream;
import java.io.InputStream;
import java.io.IOException;
import org.apache.pdfbox.pdfparser.PDFStreamParser;
import java.util.List;
import org.apache.pdfbox.io.RandomAccessFileOutputStream;
import org.apache.pdfbox.io.RandomAccess;

public class COSStream extends COSDictionary
{
    private static final int BUFFER_SIZE = 16384;
    private RandomAccess file;
    private RandomAccessFileOutputStream filteredStream;
    private RandomAccessFileOutputStream unFilteredStream;
    
    public COSStream(final RandomAccess storage) {
        this.file = storage;
    }
    
    public COSStream(final COSDictionary dictionary, final RandomAccess storage) {
        super(dictionary);
        this.file = storage;
    }
    
    public void replaceWithStream(final COSStream stream) {
        this.clear();
        this.addAll(stream);
        this.file = stream.file;
        this.filteredStream = stream.filteredStream;
        this.unFilteredStream = stream.unFilteredStream;
    }
    
    public RandomAccess getScratchFile() {
        return this.file;
    }
    
    public List<Object> getStreamTokens() throws IOException {
        final PDFStreamParser parser = new PDFStreamParser(this);
        parser.parse();
        return parser.getTokens();
    }
    
    public InputStream getFilteredStream() throws IOException {
        if (this.filteredStream == null) {
            this.doEncode();
        }
        final long position = this.filteredStream.getPosition();
        final long length = this.filteredStream.getLength();
        final RandomAccessFileInputStream input = new RandomAccessFileInputStream(this.file, position, length);
        return new BufferedInputStream(input, 16384);
    }
    
    public InputStream getUnfilteredStream() throws IOException {
        InputStream retval = null;
        if (this.unFilteredStream == null) {
            this.doDecode();
        }
        if (this.unFilteredStream != null) {
            final long position = this.unFilteredStream.getPosition();
            final long length = this.unFilteredStream.getLength();
            final RandomAccessFileInputStream input = new RandomAccessFileInputStream(this.file, position, length);
            retval = new BufferedInputStream(input, 16384);
        }
        else {
            retval = new ByteArrayInputStream(new byte[0]);
        }
        return retval;
    }
    
    @Override
    public Object accept(final ICOSVisitor visitor) throws COSVisitorException {
        return visitor.visitFromStream(this);
    }
    
    private void doDecode() throws IOException {
        this.unFilteredStream = this.filteredStream;
        final COSBase filters = this.getFilters();
        if (filters != null) {
            if (filters instanceof COSName) {
                this.doDecode((COSName)filters, 0);
            }
            else {
                if (!(filters instanceof COSArray)) {
                    throw new IOException("Error: Unknown filter type:" + filters);
                }
                final COSArray filterArray = (COSArray)filters;
                for (int i = 0; i < filterArray.size(); ++i) {
                    final COSName filterName = (COSName)filterArray.get(i);
                    this.doDecode(filterName, i);
                }
            }
        }
    }
    
    private void doDecode(final COSName filterName, final int filterIndex) throws IOException {
        final FilterManager manager = this.getFilterManager();
        final Filter filter = manager.getFilter(filterName);
        boolean done = false;
        IOException exception = null;
        final long position = this.unFilteredStream.getPosition();
        long length = this.unFilteredStream.getLength();
        final long writtenLength = this.unFilteredStream.getLengthWritten();
        if (length == 0L) {
            this.unFilteredStream = new RandomAccessFileOutputStream(this.file);
            done = true;
        }
        else {
            for (int tryCount = 0; !done && tryCount < 5; ++tryCount) {
                try {
                    final InputStream input = new BufferedInputStream(new RandomAccessFileInputStream(this.file, position, length), 16384);
                    filter.decode(input, this.unFilteredStream = new RandomAccessFileOutputStream(this.file), this, filterIndex);
                    done = true;
                }
                catch (IOException io) {
                    --length;
                    exception = io;
                }
            }
            if (!done) {
                length = writtenLength;
                for (int tryCount = 0; !done && tryCount < 5; ++tryCount) {
                    try {
                        final InputStream input = new BufferedInputStream(new RandomAccessFileInputStream(this.file, position, length), 16384);
                        filter.decode(input, this.unFilteredStream = new RandomAccessFileOutputStream(this.file), this, filterIndex);
                        done = true;
                    }
                    catch (IOException io) {
                        --length;
                        exception = io;
                    }
                }
            }
        }
        if (!done) {
            throw exception;
        }
    }
    
    private void doEncode() throws IOException {
        this.filteredStream = this.unFilteredStream;
        final COSBase filters = this.getFilters();
        if (filters != null) {
            if (filters instanceof COSName) {
                this.doEncode((COSName)filters, 0);
            }
            else if (filters instanceof COSArray) {
                final COSArray filterArray = (COSArray)filters;
                for (int i = filterArray.size() - 1; i >= 0; --i) {
                    final COSName filterName = (COSName)filterArray.get(i);
                    this.doEncode(filterName, i);
                }
            }
        }
    }
    
    private void doEncode(final COSName filterName, final int filterIndex) throws IOException {
        final FilterManager manager = this.getFilterManager();
        final Filter filter = manager.getFilter(filterName);
        final InputStream input = new BufferedInputStream(new RandomAccessFileInputStream(this.file, this.filteredStream.getPosition(), this.filteredStream.getLength()), 16384);
        filter.encode(input, this.filteredStream = new RandomAccessFileOutputStream(this.file), this, filterIndex);
    }
    
    public COSBase getFilters() {
        return this.getDictionaryObject(COSName.FILTER);
    }
    
    public OutputStream createFilteredStream() throws IOException {
        this.filteredStream = new RandomAccessFileOutputStream(this.file);
        this.unFilteredStream = null;
        return new BufferedOutputStream(this.filteredStream, 16384);
    }
    
    public OutputStream createFilteredStream(final COSBase expectedLength) throws IOException {
        (this.filteredStream = new RandomAccessFileOutputStream(this.file)).setExpectedLength(expectedLength);
        this.unFilteredStream = null;
        return new BufferedOutputStream(this.filteredStream, 16384);
    }
    
    public void setFilters(final COSBase filters) throws IOException {
        this.setItem(COSName.FILTER, filters);
        this.filteredStream = null;
    }
    
    public OutputStream createUnfilteredStream() throws IOException {
        this.unFilteredStream = new RandomAccessFileOutputStream(this.file);
        this.filteredStream = null;
        return new BufferedOutputStream(this.unFilteredStream, 16384);
    }
}
