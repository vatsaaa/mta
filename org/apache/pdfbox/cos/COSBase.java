// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.cos;

import org.apache.pdfbox.exceptions.COSVisitorException;
import org.apache.pdfbox.filter.FilterManager;
import org.apache.pdfbox.pdmodel.common.COSObjectable;

public abstract class COSBase implements COSObjectable
{
    private boolean needToBeUpdate;
    private boolean direct;
    
    public COSBase() {
        this.needToBeUpdate = false;
    }
    
    public FilterManager getFilterManager() {
        return new FilterManager();
    }
    
    public COSBase getCOSObject() {
        return this;
    }
    
    public abstract Object accept(final ICOSVisitor p0) throws COSVisitorException;
    
    public void setNeedToBeUpdate(final boolean flag) {
        this.needToBeUpdate = flag;
    }
    
    public boolean isDirect() {
        return this.direct;
    }
    
    public void setDirect(final boolean direct) {
        this.direct = direct;
    }
    
    public boolean isNeedToBeUpdate() {
        return this.needToBeUpdate;
    }
}
