// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.cos;

import org.apache.pdfbox.exceptions.COSVisitorException;
import java.util.Iterator;
import java.util.Collection;
import org.apache.pdfbox.pdmodel.common.COSObjectable;
import java.util.ArrayList;
import java.util.List;

public class COSArray extends COSBase implements Iterable<COSBase>
{
    private List<COSBase> objects;
    
    public COSArray() {
        this.objects = new ArrayList<COSBase>();
    }
    
    public void add(final COSBase object) {
        this.objects.add(object);
    }
    
    public void add(final COSObjectable object) {
        this.objects.add(object.getCOSObject());
    }
    
    public void add(final int i, final COSBase object) {
        this.objects.add(i, object);
    }
    
    public void clear() {
        this.objects.clear();
    }
    
    public void removeAll(final Collection<COSBase> objectsList) {
        this.objects.removeAll(objectsList);
    }
    
    public void retainAll(final Collection<COSBase> objectsList) {
        this.objects.retainAll(objectsList);
    }
    
    public void addAll(final Collection<COSBase> objectsList) {
        this.objects.addAll(objectsList);
    }
    
    public void addAll(final COSArray objectList) {
        if (objectList != null) {
            this.objects.addAll(objectList.objects);
        }
    }
    
    public void addAll(final int i, final Collection<COSBase> objectList) {
        this.objects.addAll(i, objectList);
    }
    
    public void set(final int index, final COSBase object) {
        this.objects.set(index, object);
    }
    
    public void set(final int index, final int intVal) {
        this.objects.set(index, COSInteger.get(intVal));
    }
    
    public void set(final int index, final COSObjectable object) {
        COSBase base = null;
        if (object != null) {
            base = object.getCOSObject();
        }
        this.objects.set(index, base);
    }
    
    public COSBase getObject(final int index) {
        Object obj = this.objects.get(index);
        if (obj instanceof COSObject) {
            obj = ((COSObject)obj).getObject();
        }
        else if (obj instanceof COSNull) {
            obj = null;
        }
        return (COSBase)obj;
    }
    
    public COSBase get(final int index) {
        return this.objects.get(index);
    }
    
    public int getInt(final int index) {
        return this.getInt(index, -1);
    }
    
    public int getInt(final int index, final int defaultValue) {
        int retval = defaultValue;
        if (index < this.size()) {
            final Object obj = this.objects.get(index);
            if (obj instanceof COSNumber) {
                retval = ((COSNumber)obj).intValue();
            }
        }
        return retval;
    }
    
    public void setInt(final int index, final int value) {
        this.set(index, COSInteger.get(value));
    }
    
    public void setName(final int index, final String name) {
        this.set(index, COSName.getPDFName(name));
    }
    
    public String getName(final int index) {
        return this.getName(index, null);
    }
    
    public String getName(final int index, final String defaultValue) {
        String retval = defaultValue;
        if (index < this.size()) {
            final Object obj = this.objects.get(index);
            if (obj instanceof COSName) {
                retval = ((COSName)obj).getName();
            }
        }
        return retval;
    }
    
    public void setString(final int index, final String string) {
        this.set(index, new COSString(string));
    }
    
    public String getString(final int index) {
        return this.getString(index, null);
    }
    
    public String getString(final int index, final String defaultValue) {
        String retval = defaultValue;
        if (index < this.size()) {
            final Object obj = this.objects.get(index);
            if (obj instanceof COSString) {
                retval = ((COSString)obj).getString();
            }
        }
        return retval;
    }
    
    public int size() {
        return this.objects.size();
    }
    
    public COSBase remove(final int i) {
        return this.objects.remove(i);
    }
    
    public boolean remove(final COSBase o) {
        return this.objects.remove(o);
    }
    
    public boolean removeObject(final COSBase o) {
        final boolean removed = this.remove(o);
        if (!removed) {
            for (int i = 0; i < this.size(); ++i) {
                final COSBase entry = this.get(i);
                if (entry instanceof COSObject) {
                    final COSObject objEntry = (COSObject)entry;
                    if (objEntry.getObject().equals(o)) {
                        return this.remove(entry);
                    }
                }
            }
        }
        return removed;
    }
    
    @Override
    public String toString() {
        return "COSArray{" + this.objects + "}";
    }
    
    public Iterator<COSBase> iterator() {
        return this.objects.iterator();
    }
    
    public int indexOf(final COSBase object) {
        int retval = -1;
        for (int i = 0; retval < 0 && i < this.size(); ++i) {
            if (this.get(i).equals(object)) {
                retval = i;
            }
        }
        return retval;
    }
    
    public int indexOfObject(final COSBase object) {
        int retval = -1;
        for (int i = 0; retval < 0 && i < this.size(); ++i) {
            final COSBase item = this.get(i);
            if (item.equals(object)) {
                retval = i;
                break;
            }
            if (item instanceof COSObject && ((COSObject)item).getObject().equals(object)) {
                retval = i;
                break;
            }
        }
        return retval;
    }
    
    public void growToSize(final int size) {
        this.growToSize(size, null);
    }
    
    public void growToSize(final int size, final COSBase object) {
        while (this.size() < size) {
            this.add(object);
        }
    }
    
    @Override
    public Object accept(final ICOSVisitor visitor) throws COSVisitorException {
        return visitor.visitFromArray(this);
    }
    
    public float[] toFloatArray() {
        final float[] retval = new float[this.size()];
        for (int i = 0; i < this.size(); ++i) {
            retval[i] = ((COSNumber)this.getObject(i)).floatValue();
        }
        return retval;
    }
    
    public void setFloatArray(final float[] value) {
        this.clear();
        for (int i = 0; i < value.length; ++i) {
            this.add(new COSFloat(value[i]));
        }
    }
    
    public List<COSBase> toList() {
        final ArrayList<COSBase> retList = new ArrayList<COSBase>(this.size());
        for (int i = 0; i < this.size(); ++i) {
            retList.add(this.get(i));
        }
        return retList;
    }
}
