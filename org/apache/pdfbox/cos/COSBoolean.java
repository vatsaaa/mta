// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.cos;

import java.io.IOException;
import java.io.OutputStream;
import org.apache.pdfbox.exceptions.COSVisitorException;

public class COSBoolean extends COSBase
{
    public static final byte[] TRUE_BYTES;
    public static final byte[] FALSE_BYTES;
    public static final COSBoolean TRUE;
    public static final COSBoolean FALSE;
    private boolean value;
    
    private COSBoolean(final boolean aValue) {
        this.value = aValue;
    }
    
    public boolean getValue() {
        return this.value;
    }
    
    public Boolean getValueAsObject() {
        return this.value ? Boolean.TRUE : Boolean.FALSE;
    }
    
    public static COSBoolean getBoolean(final boolean value) {
        return value ? COSBoolean.TRUE : COSBoolean.FALSE;
    }
    
    public static COSBoolean getBoolean(final Boolean value) {
        return getBoolean((boolean)value);
    }
    
    @Override
    public Object accept(final ICOSVisitor visitor) throws COSVisitorException {
        return visitor.visitFromBoolean(this);
    }
    
    @Override
    public String toString() {
        return String.valueOf(this.value);
    }
    
    public void writePDF(final OutputStream output) throws IOException {
        if (this.value) {
            output.write(COSBoolean.TRUE_BYTES);
        }
        else {
            output.write(COSBoolean.FALSE_BYTES);
        }
    }
    
    static {
        TRUE_BYTES = new byte[] { 116, 114, 117, 101 };
        FALSE_BYTES = new byte[] { 102, 97, 108, 115, 101 };
        TRUE = new COSBoolean(true);
        FALSE = new COSBoolean(false);
    }
}
