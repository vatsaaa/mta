// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.cos;

import java.io.IOException;

public abstract class COSNumber extends COSBase
{
    @Deprecated
    public static final COSInteger ZERO;
    @Deprecated
    public static final COSInteger ONE;
    
    public abstract float floatValue();
    
    public abstract double doubleValue();
    
    public abstract int intValue();
    
    public abstract long longValue();
    
    public static COSNumber get(final String number) throws IOException {
        if (number.length() != 1) {
            if (number.indexOf(46) == -1 && number.toLowerCase().indexOf(101) == -1) {
                try {
                    return COSInteger.get(Long.parseLong(number));
                }
                catch (NumberFormatException e) {
                    throw new IOException("Value is not an integer: " + number);
                }
            }
            return new COSFloat(number);
        }
        final char digit = number.charAt(0);
        if ('0' <= digit && digit <= '9') {
            return COSInteger.get(digit - '0');
        }
        if (digit == '-' || digit == '.') {
            return COSInteger.ZERO;
        }
        throw new IOException("Not a number: " + number);
    }
    
    static {
        ZERO = COSInteger.ZERO;
        ONE = COSInteger.ONE;
    }
}
