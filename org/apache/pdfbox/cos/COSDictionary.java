// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.cos;

import org.apache.pdfbox.exceptions.COSVisitorException;
import java.util.Set;
import java.util.Collection;
import java.util.ArrayList;
import java.util.List;
import java.io.IOException;
import org.apache.pdfbox.util.DateConverter;
import java.util.Calendar;
import org.apache.pdfbox.pdmodel.common.COSObjectable;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

public class COSDictionary extends COSBase
{
    private static final String PATH_SEPARATOR = "/";
    protected final Map<COSName, COSBase> items;
    
    public COSDictionary() {
        this.items = new LinkedHashMap<COSName, COSBase>();
    }
    
    public COSDictionary(final COSDictionary dict) {
        (this.items = new LinkedHashMap<COSName, COSBase>()).putAll(dict.items);
    }
    
    public boolean containsValue(final Object value) {
        boolean contains = this.items.containsValue(value);
        if (!contains && value instanceof COSObject) {
            contains = this.items.containsValue(((COSObject)value).getObject());
        }
        return contains;
    }
    
    public COSName getKeyForValue(final Object value) {
        for (final Map.Entry<COSName, COSBase> entry : this.items.entrySet()) {
            final Object nextValue = entry.getValue();
            if (nextValue.equals(value) || (nextValue instanceof COSObject && ((COSObject)nextValue).getObject().equals(value))) {
                return entry.getKey();
            }
        }
        return null;
    }
    
    public int size() {
        return this.items.size();
    }
    
    public void clear() {
        this.items.clear();
    }
    
    public COSBase getDictionaryObject(final String key) {
        return this.getDictionaryObject(COSName.getPDFName(key));
    }
    
    @Deprecated
    public COSBase getDictionaryObject(final String firstKey, final String secondKey) {
        COSBase retval = this.getDictionaryObject(COSName.getPDFName(firstKey));
        if (retval == null) {
            retval = this.getDictionaryObject(COSName.getPDFName(secondKey));
        }
        return retval;
    }
    
    public COSBase getDictionaryObject(final COSName firstKey, final COSName secondKey) {
        COSBase retval = this.getDictionaryObject(firstKey);
        if (retval == null && secondKey != null) {
            retval = this.getDictionaryObject(secondKey);
        }
        return retval;
    }
    
    public COSBase getDictionaryObject(final String[] keyList) {
        COSBase retval = null;
        for (int i = 0; i < keyList.length && retval == null; retval = this.getDictionaryObject(COSName.getPDFName(keyList[i])), ++i) {}
        return retval;
    }
    
    public COSBase getDictionaryObject(final COSName key) {
        COSBase retval = this.items.get(key);
        if (retval instanceof COSObject) {
            retval = ((COSObject)retval).getObject();
        }
        if (retval instanceof COSNull) {
            retval = null;
        }
        return retval;
    }
    
    public void setItem(final COSName key, final COSBase value) {
        if (value == null) {
            this.removeItem(key);
        }
        else {
            this.items.put(key, value);
        }
    }
    
    public void setItem(final COSName key, final COSObjectable value) {
        COSBase base = null;
        if (value != null) {
            base = value.getCOSObject();
        }
        this.setItem(key, base);
    }
    
    public void setItem(final String key, final COSObjectable value) {
        this.setItem(COSName.getPDFName(key), value);
    }
    
    public void setBoolean(final String key, final boolean value) {
        this.setItem(COSName.getPDFName(key), COSBoolean.getBoolean(value));
    }
    
    public void setBoolean(final COSName key, final boolean value) {
        this.setItem(key, COSBoolean.getBoolean(value));
    }
    
    public void setItem(final String key, final COSBase value) {
        this.setItem(COSName.getPDFName(key), value);
    }
    
    public void setName(final String key, final String value) {
        this.setName(COSName.getPDFName(key), value);
    }
    
    public void setName(final COSName key, final String value) {
        COSName name = null;
        if (value != null) {
            name = COSName.getPDFName(value);
        }
        this.setItem(key, name);
    }
    
    public void setDate(final String key, final Calendar date) {
        this.setDate(COSName.getPDFName(key), date);
    }
    
    public void setDate(final COSName key, final Calendar date) {
        this.setString(key, DateConverter.toString(date));
    }
    
    public void setEmbeddedDate(final String embedded, final String key, final Calendar date) {
        this.setEmbeddedDate(embedded, COSName.getPDFName(key), date);
    }
    
    public void setEmbeddedDate(final String embedded, final COSName key, final Calendar date) {
        COSDictionary dic = (COSDictionary)this.getDictionaryObject(embedded);
        if (dic == null && date != null) {
            dic = new COSDictionary();
            this.setItem(embedded, dic);
        }
        if (dic != null) {
            dic.setDate(key, date);
        }
    }
    
    public void setString(final String key, final String value) {
        this.setString(COSName.getPDFName(key), value);
    }
    
    public void setString(final COSName key, final String value) {
        COSString name = null;
        if (value != null) {
            name = new COSString(value);
        }
        this.setItem(key, name);
    }
    
    public void setEmbeddedString(final String embedded, final String key, final String value) {
        this.setEmbeddedString(embedded, COSName.getPDFName(key), value);
    }
    
    public void setEmbeddedString(final String embedded, final COSName key, final String value) {
        COSDictionary dic = (COSDictionary)this.getDictionaryObject(embedded);
        if (dic == null && value != null) {
            dic = new COSDictionary();
            this.setItem(embedded, dic);
        }
        if (dic != null) {
            dic.setString(key, value);
        }
    }
    
    public void setInt(final String key, final int value) {
        this.setInt(COSName.getPDFName(key), value);
    }
    
    public void setInt(final COSName key, final int value) {
        this.setItem(key, COSInteger.get(value));
    }
    
    public void setLong(final String key, final long value) {
        this.setLong(COSName.getPDFName(key), value);
    }
    
    public void setLong(final COSName key, final long value) {
        COSInteger intVal = null;
        intVal = COSInteger.get(value);
        this.setItem(key, intVal);
    }
    
    public void setEmbeddedInt(final String embeddedDictionary, final String key, final int value) {
        this.setEmbeddedInt(embeddedDictionary, COSName.getPDFName(key), value);
    }
    
    public void setEmbeddedInt(final String embeddedDictionary, final COSName key, final int value) {
        COSDictionary embedded = (COSDictionary)this.getDictionaryObject(embeddedDictionary);
        if (embedded == null) {
            embedded = new COSDictionary();
            this.setItem(embeddedDictionary, embedded);
        }
        embedded.setInt(key, value);
    }
    
    public void setFloat(final String key, final float value) {
        this.setFloat(COSName.getPDFName(key), value);
    }
    
    public void setFloat(final COSName key, final float value) {
        final COSFloat fltVal = new COSFloat(value);
        this.setItem(key, fltVal);
    }
    
    public String getNameAsString(final String key) {
        return this.getNameAsString(COSName.getPDFName(key));
    }
    
    public String getNameAsString(final COSName key) {
        String retval = null;
        final COSBase name = this.getDictionaryObject(key);
        if (name != null) {
            if (name instanceof COSName) {
                retval = ((COSName)name).getName();
            }
            else if (name instanceof COSString) {
                retval = ((COSString)name).getString();
            }
        }
        return retval;
    }
    
    public String getNameAsString(final String key, final String defaultValue) {
        return this.getNameAsString(COSName.getPDFName(key), defaultValue);
    }
    
    public String getNameAsString(final COSName key, final String defaultValue) {
        String retval = this.getNameAsString(key);
        if (retval == null) {
            retval = defaultValue;
        }
        return retval;
    }
    
    public String getString(final String key) {
        return this.getString(COSName.getPDFName(key));
    }
    
    public String getString(final COSName key) {
        String retval = null;
        final COSBase value = this.getDictionaryObject(key);
        if (value != null && value instanceof COSString) {
            retval = ((COSString)value).getString();
        }
        return retval;
    }
    
    public String getString(final String key, final String defaultValue) {
        return this.getString(COSName.getPDFName(key), defaultValue);
    }
    
    public String getString(final COSName key, final String defaultValue) {
        String retval = this.getString(key);
        if (retval == null) {
            retval = defaultValue;
        }
        return retval;
    }
    
    public String getEmbeddedString(final String embedded, final String key) {
        return this.getEmbeddedString(embedded, COSName.getPDFName(key), null);
    }
    
    public String getEmbeddedString(final String embedded, final COSName key) {
        return this.getEmbeddedString(embedded, key, null);
    }
    
    public String getEmbeddedString(final String embedded, final String key, final String defaultValue) {
        return this.getEmbeddedString(embedded, COSName.getPDFName(key), defaultValue);
    }
    
    public String getEmbeddedString(final String embedded, final COSName key, final String defaultValue) {
        String retval = defaultValue;
        final COSDictionary dic = (COSDictionary)this.getDictionaryObject(embedded);
        if (dic != null) {
            retval = dic.getString(key, defaultValue);
        }
        return retval;
    }
    
    public Calendar getDate(final String key) throws IOException {
        return this.getDate(COSName.getPDFName(key));
    }
    
    public Calendar getDate(final COSName key) throws IOException {
        final COSString date = (COSString)this.getDictionaryObject(key);
        return DateConverter.toCalendar(date);
    }
    
    public Calendar getDate(final String key, final Calendar defaultValue) throws IOException {
        return this.getDate(COSName.getPDFName(key), defaultValue);
    }
    
    public Calendar getDate(final COSName key, final Calendar defaultValue) throws IOException {
        Calendar retval = this.getDate(key);
        if (retval == null) {
            retval = defaultValue;
        }
        return retval;
    }
    
    public Calendar getEmbeddedDate(final String embedded, final String key) throws IOException {
        return this.getEmbeddedDate(embedded, COSName.getPDFName(key), null);
    }
    
    public Calendar getEmbeddedDate(final String embedded, final COSName key) throws IOException {
        return this.getEmbeddedDate(embedded, key, null);
    }
    
    public Calendar getEmbeddedDate(final String embedded, final String key, final Calendar defaultValue) throws IOException {
        return this.getEmbeddedDate(embedded, COSName.getPDFName(key), defaultValue);
    }
    
    public Calendar getEmbeddedDate(final String embedded, final COSName key, final Calendar defaultValue) throws IOException {
        Calendar retval = defaultValue;
        final COSDictionary eDic = (COSDictionary)this.getDictionaryObject(embedded);
        if (eDic != null) {
            retval = eDic.getDate(key, defaultValue);
        }
        return retval;
    }
    
    public boolean getBoolean(final String key, final boolean defaultValue) {
        return this.getBoolean(COSName.getPDFName(key), defaultValue);
    }
    
    public boolean getBoolean(final COSName key, final boolean defaultValue) {
        return this.getBoolean(key, null, defaultValue);
    }
    
    public boolean getBoolean(final COSName firstKey, final COSName secondKey, final boolean defaultValue) {
        boolean retval = defaultValue;
        final COSBase bool = this.getDictionaryObject(firstKey, secondKey);
        if (bool != null && bool instanceof COSBoolean) {
            retval = ((COSBoolean)bool).getValue();
        }
        return retval;
    }
    
    public int getEmbeddedInt(final String embeddedDictionary, final String key) {
        return this.getEmbeddedInt(embeddedDictionary, COSName.getPDFName(key));
    }
    
    public int getEmbeddedInt(final String embeddedDictionary, final COSName key) {
        return this.getEmbeddedInt(embeddedDictionary, key, -1);
    }
    
    public int getEmbeddedInt(final String embeddedDictionary, final String key, final int defaultValue) {
        return this.getEmbeddedInt(embeddedDictionary, COSName.getPDFName(key), defaultValue);
    }
    
    public int getEmbeddedInt(final String embeddedDictionary, final COSName key, final int defaultValue) {
        int retval = defaultValue;
        final COSDictionary embedded = (COSDictionary)this.getDictionaryObject(embeddedDictionary);
        if (embedded != null) {
            retval = embedded.getInt(key, defaultValue);
        }
        return retval;
    }
    
    public int getInt(final String key) {
        return this.getInt(COSName.getPDFName(key), -1);
    }
    
    public int getInt(final COSName key) {
        return this.getInt(key, -1);
    }
    
    public int getInt(final String[] keyList, final int defaultValue) {
        int retval = defaultValue;
        final COSBase obj = this.getDictionaryObject(keyList);
        if (obj != null && obj instanceof COSNumber) {
            retval = ((COSNumber)obj).intValue();
        }
        return retval;
    }
    
    public int getInt(final String key, final int defaultValue) {
        return this.getInt(COSName.getPDFName(key), defaultValue);
    }
    
    public int getInt(final COSName key, final int defaultValue) {
        return this.getInt(key, null, defaultValue);
    }
    
    public int getInt(final COSName firstKey, final COSName secondKey) {
        return this.getInt(firstKey, secondKey, -1);
    }
    
    public int getInt(final COSName firstKey, final COSName secondKey, final int defaultValue) {
        int retval = defaultValue;
        final COSBase obj = this.getDictionaryObject(firstKey, secondKey);
        if (obj != null && obj instanceof COSNumber) {
            retval = ((COSNumber)obj).intValue();
        }
        return retval;
    }
    
    public long getLong(final String key) {
        return this.getLong(COSName.getPDFName(key), -1L);
    }
    
    public long getLong(final COSName key) {
        return this.getLong(key, -1L);
    }
    
    public long getLong(final String[] keyList, final long defaultValue) {
        long retval = defaultValue;
        final COSBase obj = this.getDictionaryObject(keyList);
        if (obj != null && obj instanceof COSNumber) {
            retval = ((COSNumber)obj).longValue();
        }
        return retval;
    }
    
    public long getLong(final String key, final long defaultValue) {
        return this.getLong(COSName.getPDFName(key), defaultValue);
    }
    
    public long getLong(final COSName key, final long defaultValue) {
        long retval = defaultValue;
        final COSBase obj = this.getDictionaryObject(key);
        if (obj != null && obj instanceof COSNumber) {
            retval = ((COSNumber)obj).longValue();
        }
        return retval;
    }
    
    public float getFloat(final String key) {
        return this.getFloat(COSName.getPDFName(key), -1.0f);
    }
    
    public float getFloat(final COSName key) {
        return this.getFloat(key, -1.0f);
    }
    
    public float getFloat(final String key, final float defaultValue) {
        return this.getFloat(COSName.getPDFName(key), defaultValue);
    }
    
    public float getFloat(final COSName key, final float defaultValue) {
        float retval = defaultValue;
        final COSBase obj = this.getDictionaryObject(key);
        if (obj != null && obj instanceof COSNumber) {
            retval = ((COSNumber)obj).floatValue();
        }
        return retval;
    }
    
    public void removeItem(final COSName key) {
        this.items.remove(key);
    }
    
    public COSBase getItem(final COSName key) {
        return this.items.get(key);
    }
    
    @Deprecated
    public List<COSName> keyList() {
        return new ArrayList<COSName>(this.items.keySet());
    }
    
    public Set<COSName> keySet() {
        return this.items.keySet();
    }
    
    public Set<Map.Entry<COSName, COSBase>> entrySet() {
        return this.items.entrySet();
    }
    
    public Collection<COSBase> getValues() {
        return this.items.values();
    }
    
    @Override
    public Object accept(final ICOSVisitor visitor) throws COSVisitorException {
        return visitor.visitFromDictionary(this);
    }
    
    public void addAll(final COSDictionary dic) {
        for (final Map.Entry<COSName, COSBase> entry : dic.entrySet()) {
            if (!entry.getKey().getName().equals("Size") || !this.items.containsKey(COSName.getPDFName("Size"))) {
                this.setItem(entry.getKey(), entry.getValue());
            }
        }
    }
    
    public void mergeInto(final COSDictionary dic) {
        for (final Map.Entry<COSName, COSBase> entry : dic.entrySet()) {
            if (this.getItem(entry.getKey()) == null) {
                this.setItem(entry.getKey(), entry.getValue());
            }
        }
    }
    
    public COSBase getObjectFromPath(final String objPath) {
        COSBase retval = null;
        final String[] path = objPath.split("/");
        retval = this;
        for (int i = 0; i < path.length; ++i) {
            if (retval instanceof COSArray) {
                final int idx = new Integer(path[i].replaceAll("\\[", "").replaceAll("\\]", ""));
                retval = ((COSArray)retval).getObject(idx);
            }
            else if (retval instanceof COSDictionary) {
                retval = ((COSDictionary)retval).getDictionaryObject(path[i]);
            }
        }
        return retval;
    }
    
    @Override
    public String toString() {
        final StringBuilder retVal = new StringBuilder("COSDictionary{");
        for (final COSName key : this.items.keySet()) {
            retVal.append("(");
            retVal.append(key);
            retVal.append(":");
            if (this.getDictionaryObject(key) != null) {
                retVal.append(this.getDictionaryObject(key).toString());
            }
            else {
                retVal.append("<null>");
            }
            retVal.append(") ");
        }
        retVal.append("}");
        return retVal.toString();
    }
}
