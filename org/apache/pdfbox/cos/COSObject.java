// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.cos;

import org.apache.pdfbox.exceptions.COSVisitorException;
import java.io.IOException;

public class COSObject extends COSBase
{
    private COSBase baseObject;
    private COSInteger objectNumber;
    private COSInteger generationNumber;
    
    public COSObject(final COSBase object) throws IOException {
        this.setObject(object);
    }
    
    public COSBase getDictionaryObject(final COSName key) {
        COSBase retval = null;
        if (this.baseObject instanceof COSDictionary) {
            retval = ((COSDictionary)this.baseObject).getDictionaryObject(key);
        }
        return retval;
    }
    
    public COSBase getItem(final COSName key) {
        COSBase retval = null;
        if (this.baseObject instanceof COSDictionary) {
            retval = ((COSDictionary)this.baseObject).getItem(key);
        }
        return retval;
    }
    
    public COSBase getObject() {
        return this.baseObject;
    }
    
    public void setObject(final COSBase object) throws IOException {
        this.baseObject = object;
    }
    
    @Override
    public String toString() {
        return "COSObject{" + ((this.objectNumber == null) ? "unknown" : ("" + this.objectNumber.intValue())) + ", " + ((this.generationNumber == null) ? "unknown" : ("" + this.generationNumber.intValue())) + "}";
    }
    
    public COSInteger getObjectNumber() {
        return this.objectNumber;
    }
    
    public void setObjectNumber(final COSInteger objectNum) {
        this.objectNumber = objectNum;
    }
    
    public COSInteger getGenerationNumber() {
        return this.generationNumber;
    }
    
    public void setGenerationNumber(final COSInteger generationNumberValue) {
        this.generationNumber = generationNumberValue;
    }
    
    @Override
    public Object accept(final ICOSVisitor visitor) throws COSVisitorException {
        return (this.getObject() != null) ? this.getObject().accept(visitor) : COSNull.NULL.accept(visitor);
    }
}
