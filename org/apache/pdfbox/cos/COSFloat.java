// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.cos;

import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.text.DecimalFormat;
import java.io.OutputStream;
import org.apache.pdfbox.exceptions.COSVisitorException;
import java.io.IOException;

public class COSFloat extends COSNumber
{
    private float value;
    
    public COSFloat(final float aFloat) {
        this.value = aFloat;
    }
    
    public COSFloat(final String aFloat) throws IOException {
        try {
            this.value = Float.parseFloat(aFloat);
        }
        catch (NumberFormatException e) {
            throw new IOException("Error expected floating point number actual='" + aFloat + "'");
        }
    }
    
    public void setValue(final float floatValue) {
        this.value = floatValue;
    }
    
    @Override
    public float floatValue() {
        return this.value;
    }
    
    @Override
    public double doubleValue() {
        return this.value;
    }
    
    @Override
    public long longValue() {
        return (long)this.value;
    }
    
    @Override
    public int intValue() {
        return (int)this.value;
    }
    
    @Override
    public boolean equals(final Object o) {
        return o instanceof COSFloat && Float.floatToIntBits(((COSFloat)o).value) == Float.floatToIntBits(this.value);
    }
    
    @Override
    public int hashCode() {
        return Float.floatToIntBits(this.value);
    }
    
    @Override
    public String toString() {
        return "COSFloat{" + this.value + "}";
    }
    
    @Override
    public Object accept(final ICOSVisitor visitor) throws COSVisitorException {
        return visitor.visitFromFloat(this);
    }
    
    public void writePDF(final OutputStream output) throws IOException {
        final DecimalFormat formatDecimal = (DecimalFormat)NumberFormat.getNumberInstance();
        formatDecimal.setMaximumFractionDigits(10);
        formatDecimal.setGroupingUsed(false);
        final DecimalFormatSymbols symbols = formatDecimal.getDecimalFormatSymbols();
        symbols.setDecimalSeparator('.');
        formatDecimal.setDecimalFormatSymbols(symbols);
        output.write(formatDecimal.format(this.value).getBytes("ISO-8859-1"));
    }
}
