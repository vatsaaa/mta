// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.cos;

import org.apache.pdfbox.exceptions.COSVisitorException;

public interface ICOSVisitor
{
    Object visitFromArray(final COSArray p0) throws COSVisitorException;
    
    Object visitFromBoolean(final COSBoolean p0) throws COSVisitorException;
    
    Object visitFromDictionary(final COSDictionary p0) throws COSVisitorException;
    
    Object visitFromDocument(final COSDocument p0) throws COSVisitorException;
    
    Object visitFromFloat(final COSFloat p0) throws COSVisitorException;
    
    Object visitFromInt(final COSInteger p0) throws COSVisitorException;
    
    Object visitFromName(final COSName p0) throws COSVisitorException;
    
    Object visitFromNull(final COSNull p0) throws COSVisitorException;
    
    Object visitFromStream(final COSStream p0) throws COSVisitorException;
    
    Object visitFromString(final COSString p0) throws COSVisitorException;
}
