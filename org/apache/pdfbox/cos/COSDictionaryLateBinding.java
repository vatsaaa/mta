// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.cos;

import org.apache.commons.logging.LogFactory;
import org.apache.pdfbox.pdfparser.ConformingPDFParser;
import org.apache.commons.logging.Log;

public class COSDictionaryLateBinding extends COSDictionary
{
    public static final Log log;
    ConformingPDFParser parser;
    
    public COSDictionaryLateBinding(final ConformingPDFParser parser) {
        this.parser = parser;
    }
    
    @Override
    public COSBase getDictionaryObject(final COSName key) {
        COSBase retval = this.items.get(key);
        if (retval instanceof COSObject) {
            final int objectNumber = ((COSObject)retval).getObjectNumber().intValue();
            final int generation = ((COSObject)retval).getGenerationNumber().intValue();
            try {
                retval = this.parser.getObject(objectNumber, generation);
            }
            catch (Exception e) {
                COSDictionaryLateBinding.log.warn("Unable to read information for object " + objectNumber);
            }
        }
        if (retval instanceof COSNull) {
            retval = null;
        }
        return retval;
    }
    
    static {
        log = LogFactory.getLog(COSDictionaryLateBinding.class);
    }
}
