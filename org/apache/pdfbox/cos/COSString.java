// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.cos;

import org.apache.pdfbox.exceptions.COSVisitorException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import org.apache.pdfbox.persistence.util.COSHEXTable;
import java.io.IOException;
import java.io.ByteArrayOutputStream;

public class COSString extends COSBase
{
    public static final byte[] STRING_OPEN;
    public static final byte[] STRING_CLOSE;
    public static final byte[] HEX_STRING_OPEN;
    public static final byte[] HEX_STRING_CLOSE;
    public static final byte[] ESCAPE;
    public static final byte[] CR_ESCAPE;
    public static final byte[] LF_ESCAPE;
    public static final byte[] HT_ESCAPE;
    public static final byte[] BS_ESCAPE;
    public static final byte[] FF_ESCAPE;
    private ByteArrayOutputStream out;
    private String str;
    private boolean forceHexForm;
    
    public COSString() {
        this.out = null;
        this.str = null;
        this.forceHexForm = false;
        this.out = new ByteArrayOutputStream();
    }
    
    public COSString(final String value) {
        this.out = null;
        this.str = null;
        this.forceHexForm = false;
        try {
            boolean unicode16 = false;
            final char[] chars = value.toCharArray();
            for (int length = chars.length, i = 0; i < length; ++i) {
                if (chars[i] > '\u00ff') {
                    unicode16 = true;
                    break;
                }
            }
            if (unicode16) {
                final byte[] data = value.getBytes("UTF-16BE");
                (this.out = new ByteArrayOutputStream(data.length + 2)).write(254);
                this.out.write(255);
                this.out.write(data);
            }
            else {
                final byte[] data = value.getBytes("ISO-8859-1");
                (this.out = new ByteArrayOutputStream(data.length)).write(data);
            }
        }
        catch (IOException ignore) {
            ignore.printStackTrace();
        }
    }
    
    public COSString(final byte[] value) {
        this.out = null;
        this.str = null;
        this.forceHexForm = false;
        try {
            (this.out = new ByteArrayOutputStream(value.length)).write(value);
        }
        catch (IOException ignore) {
            ignore.printStackTrace();
        }
    }
    
    public void setForceLiteralForm(final boolean v) {
        this.forceHexForm = !v;
    }
    
    public void setForceHexForm(final boolean v) {
        this.forceHexForm = v;
    }
    
    public static COSString createFromHexString(final String hex) throws IOException {
        return createFromHexString(hex, false);
    }
    
    public static COSString createFromHexString(final String hex, final boolean force) throws IOException {
        final COSString retval = new COSString();
        final StringBuilder hexBuffer = new StringBuilder(hex.trim());
        if (hexBuffer.length() % 2 != 0) {
            hexBuffer.append('0');
        }
        for (int length = hexBuffer.length(), i = 0; i < length; i += 2) {
            try {
                retval.append(Integer.parseInt(hexBuffer.substring(i, i + 2), 16));
            }
            catch (NumberFormatException e) {
                if (!force) {
                    final IOException exception = new IOException("Invalid hex string: " + hex);
                    exception.initCause(e);
                    throw exception;
                }
                retval.append(63);
            }
        }
        return retval;
    }
    
    public String getHexString() {
        final StringBuilder retval = new StringBuilder(this.out.size() * 2);
        final byte[] data = this.getBytes();
        for (int length = data.length, i = 0; i < length; ++i) {
            retval.append(COSHEXTable.HEX_TABLE[(data[i] + 256) % 256]);
        }
        return retval.toString();
    }
    
    public String getString() {
        if (this.str != null) {
            return this.str;
        }
        String encoding = "ISO-8859-1";
        final byte[] data = this.getBytes();
        int start = 0;
        if (data.length > 2) {
            if (data[0] == -1 && data[1] == -2) {
                encoding = "UTF-16LE";
                start = 2;
            }
            else if (data[0] == -2 && data[1] == -1) {
                encoding = "UTF-16BE";
                start = 2;
            }
        }
        String retval;
        try {
            retval = new String(this.getBytes(), start, data.length - start, encoding);
        }
        catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            retval = new String(this.getBytes());
        }
        return this.str = retval;
    }
    
    public void append(final byte[] data) throws IOException {
        this.out.write(data);
        this.str = null;
    }
    
    public void append(final int in) throws IOException {
        this.out.write(in);
        this.str = null;
    }
    
    public void reset() {
        this.out.reset();
        this.str = null;
    }
    
    public byte[] getBytes() {
        return this.out.toByteArray();
    }
    
    @Override
    public String toString() {
        return "COSString{" + this.getString() + "}";
    }
    
    public void writePDF(final OutputStream output) throws IOException {
        boolean outsideASCII = false;
        final byte[] bytes = this.getBytes();
        final int length = bytes.length;
        for (int i = 0; i < length && !outsideASCII; outsideASCII = (bytes[i] < 0), ++i) {}
        if (!outsideASCII && !this.forceHexForm) {
            output.write(COSString.STRING_OPEN);
            for (int i = 0; i < length; ++i) {
                final int b = (bytes[i] + 256) % 256;
                switch (b) {
                    case 40:
                    case 41:
                    case 92: {
                        output.write(COSString.ESCAPE);
                        output.write((byte)b);
                        break;
                    }
                    case 10: {
                        output.write(COSString.LF_ESCAPE);
                        break;
                    }
                    case 13: {
                        output.write(COSString.CR_ESCAPE);
                        break;
                    }
                    case 9: {
                        output.write(COSString.HT_ESCAPE);
                        break;
                    }
                    case 8: {
                        output.write(COSString.BS_ESCAPE);
                        break;
                    }
                    case 12: {
                        output.write(COSString.FF_ESCAPE);
                        break;
                    }
                    default: {
                        output.write((byte)b);
                        break;
                    }
                }
            }
            output.write(COSString.STRING_CLOSE);
        }
        else {
            output.write(COSString.HEX_STRING_OPEN);
            for (int i = 0; i < length; ++i) {
                output.write(COSHEXTable.TABLE[(bytes[i] + 256) % 256]);
            }
            output.write(COSString.HEX_STRING_CLOSE);
        }
    }
    
    @Override
    public Object accept(final ICOSVisitor visitor) throws COSVisitorException {
        return visitor.visitFromString(this);
    }
    
    @Override
    public boolean equals(final Object obj) {
        if (obj instanceof COSString) {
            final COSString strObj = (COSString)obj;
            return this.getString().equals(strObj.getString()) && this.forceHexForm == strObj.forceHexForm;
        }
        return false;
    }
    
    @Override
    public int hashCode() {
        int result = this.getString().hashCode();
        return result += (this.forceHexForm ? 17 : 0);
    }
    
    static {
        STRING_OPEN = new byte[] { 40 };
        STRING_CLOSE = new byte[] { 41 };
        HEX_STRING_OPEN = new byte[] { 60 };
        HEX_STRING_CLOSE = new byte[] { 62 };
        ESCAPE = new byte[] { 92 };
        CR_ESCAPE = new byte[] { 92, 114 };
        LF_ESCAPE = new byte[] { 92, 110 };
        HT_ESCAPE = new byte[] { 92, 116 };
        BS_ESCAPE = new byte[] { 92, 98 };
        FF_ESCAPE = new byte[] { 92, 102 };
    }
}
