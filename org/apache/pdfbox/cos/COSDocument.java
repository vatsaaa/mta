// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.cos;

import org.apache.commons.logging.LogFactory;
import org.apache.pdfbox.pdfparser.PDFObjectStreamParser;
import org.apache.pdfbox.exceptions.COSVisitorException;
import java.util.Collection;
import java.util.ArrayList;
import java.util.List;
import java.util.Iterator;
import org.apache.pdfbox.io.RandomAccessBuffer;
import java.io.IOException;
import org.apache.pdfbox.io.RandomAccessFile;
import java.util.HashMap;
import java.io.File;
import org.apache.pdfbox.io.RandomAccess;
import org.apache.pdfbox.pdmodel.interactive.digitalsignature.SignatureInterface;
import org.apache.pdfbox.persistence.util.COSObjectKey;
import java.util.Map;
import org.apache.commons.logging.Log;

public class COSDocument extends COSBase
{
    private static final Log LOG;
    private float version;
    private final Map<COSObjectKey, COSObject> objectPool;
    private final Map<COSObjectKey, Long> xrefTable;
    private COSDictionary trailer;
    private COSDictionary signDictionary;
    private SignatureInterface signatureInterface;
    private final RandomAccess scratchFile;
    private final File tmpFile;
    private String headerString;
    private boolean warnMissingClose;
    private boolean isDecrypted;
    private long startXref;
    private boolean closed;
    private final boolean forceParsing;
    
    public COSDocument(final RandomAccess scratchFileValue, final boolean forceParsingValue) {
        this.objectPool = new HashMap<COSObjectKey, COSObject>();
        this.xrefTable = new HashMap<COSObjectKey, Long>();
        this.signDictionary = null;
        this.headerString = "%PDF-1.4";
        this.warnMissingClose = true;
        this.isDecrypted = false;
        this.closed = false;
        this.scratchFile = scratchFileValue;
        this.tmpFile = null;
        this.forceParsing = forceParsingValue;
    }
    
    public COSDocument(final File scratchDir, final boolean forceParsingValue) throws IOException {
        this.objectPool = new HashMap<COSObjectKey, COSObject>();
        this.xrefTable = new HashMap<COSObjectKey, Long>();
        this.signDictionary = null;
        this.headerString = "%PDF-1.4";
        this.warnMissingClose = true;
        this.isDecrypted = false;
        this.closed = false;
        this.tmpFile = File.createTempFile("pdfbox-", ".tmp", scratchDir);
        this.scratchFile = new RandomAccessFile(this.tmpFile, "rw");
        this.forceParsing = forceParsingValue;
    }
    
    public COSDocument() throws IOException {
        this(new RandomAccessBuffer(), false);
    }
    
    public COSDocument(final File scratchDir) throws IOException {
        this(scratchDir, false);
    }
    
    public COSDocument(final RandomAccess file) {
        this(file, false);
    }
    
    public RandomAccess getScratchFile() {
        return this.scratchFile;
    }
    
    public COSObject getObjectByType(final String type) throws IOException {
        return this.getObjectByType(COSName.getPDFName(type));
    }
    
    public COSObject getObjectByType(final COSName type) throws IOException {
        for (final COSObject object : this.objectPool.values()) {
            final COSBase realObject = object.getObject();
            if (realObject instanceof COSDictionary) {
                try {
                    final COSDictionary dic = (COSDictionary)realObject;
                    final COSName objectType = (COSName)dic.getItem(COSName.TYPE);
                    if (objectType != null && objectType.equals(type)) {
                        return object;
                    }
                    continue;
                }
                catch (ClassCastException e) {
                    COSDocument.LOG.warn(e, e);
                }
            }
        }
        return null;
    }
    
    public List<COSObject> getObjectsByType(final String type) throws IOException {
        return this.getObjectsByType(COSName.getPDFName(type));
    }
    
    public List<COSObject> getObjectsByType(final COSName type) throws IOException {
        final List<COSObject> retval = new ArrayList<COSObject>();
        for (final COSObject object : this.objectPool.values()) {
            final COSBase realObject = object.getObject();
            if (realObject instanceof COSDictionary) {
                try {
                    final COSDictionary dic = (COSDictionary)realObject;
                    final COSName objectType = (COSName)dic.getItem(COSName.TYPE);
                    if (objectType == null || !objectType.equals(type)) {
                        continue;
                    }
                    retval.add(object);
                }
                catch (ClassCastException e) {
                    COSDocument.LOG.warn(e, e);
                }
            }
        }
        return retval;
    }
    
    public void print() {
        for (final COSObject object : this.objectPool.values()) {
            System.out.println(object);
        }
    }
    
    public void setVersion(final float versionValue) {
        if (versionValue != this.version) {
            this.headerString = this.headerString.replaceFirst(String.valueOf(this.version), String.valueOf(versionValue));
        }
        this.version = versionValue;
    }
    
    public float getVersion() {
        return this.version;
    }
    
    public void setDecrypted() {
        this.isDecrypted = true;
    }
    
    public boolean isEncrypted() {
        if (this.isDecrypted) {
            return false;
        }
        boolean encrypted = false;
        if (this.trailer != null) {
            encrypted = (this.trailer.getDictionaryObject(COSName.ENCRYPT) != null);
        }
        return encrypted;
    }
    
    public COSDictionary getEncryptionDictionary() {
        return (COSDictionary)this.trailer.getDictionaryObject(COSName.ENCRYPT);
    }
    
    public SignatureInterface getSignatureInterface() {
        return this.signatureInterface;
    }
    
    public void setEncryptionDictionary(final COSDictionary encDictionary) {
        this.trailer.setItem(COSName.ENCRYPT, encDictionary);
    }
    
    public COSDictionary getLastSignatureDictionary() throws IOException {
        if (this.signDictionary == null) {
            final COSObject documentCatalog = this.getCatalog();
            if (documentCatalog != null) {
                final COSDictionary acroForm = (COSDictionary)documentCatalog.getDictionaryObject(COSName.ACRO_FORM);
                if (acroForm != null) {
                    final COSArray fields = (COSArray)acroForm.getDictionaryObject(COSName.FIELDS);
                    for (final Object object : fields) {
                        final COSObject dict = (COSObject)object;
                        if (dict.getItem(COSName.FT).equals(COSName.SIG)) {
                            final COSBase dictionaryObject = dict.getDictionaryObject(COSName.V);
                            if (dictionaryObject == null) {
                                continue;
                            }
                            this.signDictionary = (COSDictionary)dictionaryObject;
                        }
                    }
                }
            }
        }
        return this.signDictionary;
    }
    
    public COSArray getDocumentID() {
        return (COSArray)this.getTrailer().getDictionaryObject(COSName.ID);
    }
    
    public void setDocumentID(final COSArray id) {
        this.getTrailer().setItem(COSName.ID, id);
    }
    
    public void setSignatureInterface(final SignatureInterface sigInterface) {
        this.signatureInterface = sigInterface;
    }
    
    public COSObject getCatalog() throws IOException {
        final COSObject catalog = this.getObjectByType(COSName.CATALOG);
        if (catalog == null) {
            throw new IOException("Catalog cannot be found");
        }
        return catalog;
    }
    
    public List<COSObject> getObjects() {
        return new ArrayList<COSObject>(this.objectPool.values());
    }
    
    public COSDictionary getTrailer() {
        return this.trailer;
    }
    
    public void setTrailer(final COSDictionary newTrailer) {
        this.trailer = newTrailer;
    }
    
    @Override
    public Object accept(final ICOSVisitor visitor) throws COSVisitorException {
        return visitor.visitFromDocument(this);
    }
    
    public void close() throws IOException {
        if (!this.closed) {
            this.scratchFile.close();
            if (this.tmpFile != null) {
                this.tmpFile.delete();
            }
            this.closed = true;
        }
    }
    
    @Override
    protected void finalize() throws IOException {
        if (!this.closed) {
            if (this.warnMissingClose) {
                COSDocument.LOG.warn("Warning: You did not close a PDF Document");
            }
            this.close();
        }
    }
    
    public void setWarnMissingClose(final boolean warn) {
        this.warnMissingClose = warn;
    }
    
    public String getHeaderString() {
        return this.headerString;
    }
    
    public void setHeaderString(final String header) {
        this.headerString = header;
    }
    
    public void dereferenceObjectStreams() throws IOException {
        for (final COSObject objStream : this.getObjectsByType(COSName.OBJ_STM)) {
            final COSStream stream = (COSStream)objStream.getObject();
            final PDFObjectStreamParser parser = new PDFObjectStreamParser(stream, this, this.forceParsing);
            parser.parse();
            for (final COSObject next : parser.getObjects()) {
                final COSObjectKey key = new COSObjectKey(next);
                if (this.objectPool.get(key) == null || this.objectPool.get(key).getObject() == null) {
                    final COSObject obj = this.getObjectFromPool(key);
                    obj.setObject(next.getObject());
                }
            }
        }
    }
    
    public COSObject getObjectFromPool(final COSObjectKey key) throws IOException {
        COSObject obj = null;
        if (key != null) {
            obj = this.objectPool.get(key);
        }
        if (obj == null) {
            obj = new COSObject(null);
            if (key != null) {
                obj.setObjectNumber(COSInteger.get(key.getNumber()));
                obj.setGenerationNumber(COSInteger.get(key.getGeneration()));
                this.objectPool.put(key, obj);
            }
        }
        return obj;
    }
    
    public COSObject removeObject(final COSObjectKey key) {
        return this.objectPool.remove(key);
    }
    
    public void addXRefTable(final Map<COSObjectKey, Long> xrefTableValues) {
        this.xrefTable.putAll(xrefTableValues);
    }
    
    public Map<COSObjectKey, Long> getXrefTable() {
        return this.xrefTable;
    }
    
    public void setStartXref(final long startXrefValue) {
        this.startXref = startXrefValue;
    }
    
    public long getStartXref() {
        return this.startXref;
    }
    
    static {
        LOG = LogFactory.getLog(COSDocument.class);
    }
}
