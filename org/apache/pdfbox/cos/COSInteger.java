// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.cos;

import java.io.OutputStream;
import org.apache.pdfbox.exceptions.COSVisitorException;
import java.io.IOException;

public class COSInteger extends COSNumber
{
    private static int LOW;
    private static int HIGH;
    private static final COSInteger[] STATIC;
    public static final COSInteger ZERO;
    public static final COSInteger ONE;
    public static final COSInteger TWO;
    public static final COSInteger THREE;
    private long value;
    
    public static COSInteger get(final long val) {
        if (COSInteger.LOW <= val && val <= COSInteger.HIGH) {
            final int index = (int)val - COSInteger.LOW;
            if (COSInteger.STATIC[index] == null) {
                COSInteger.STATIC[index] = new COSInteger(val);
            }
            return COSInteger.STATIC[index];
        }
        return new COSInteger(val);
    }
    
    @Deprecated
    public COSInteger(final long val) {
        this.value = val;
    }
    
    @Deprecated
    public COSInteger(final int val) {
        this((long)val);
    }
    
    @Deprecated
    public COSInteger(final String val) throws IOException {
        try {
            this.value = Long.parseLong(val);
        }
        catch (NumberFormatException e) {
            throw new IOException("Error: value is not an integer type actual='" + val + "'");
        }
    }
    
    @Override
    public boolean equals(final Object o) {
        return o instanceof COSInteger && ((COSInteger)o).intValue() == this.intValue();
    }
    
    @Override
    public int hashCode() {
        return (int)(this.value ^ this.value >> 32);
    }
    
    @Override
    public String toString() {
        return "COSInt{" + this.value + "}";
    }
    
    public void setValue(final long newValue) {
        this.value = newValue;
    }
    
    @Override
    public float floatValue() {
        return (float)this.value;
    }
    
    @Override
    public double doubleValue() {
        return (double)this.value;
    }
    
    @Override
    public int intValue() {
        return (int)this.value;
    }
    
    @Override
    public long longValue() {
        return this.value;
    }
    
    @Override
    public Object accept(final ICOSVisitor visitor) throws COSVisitorException {
        return visitor.visitFromInt(this);
    }
    
    public void writePDF(final OutputStream output) throws IOException {
        output.write(String.valueOf(this.value).getBytes("ISO-8859-1"));
    }
    
    static {
        COSInteger.LOW = -100;
        COSInteger.HIGH = 256;
        STATIC = new COSInteger[COSInteger.HIGH - COSInteger.LOW + 1];
        ZERO = get(0L);
        ONE = get(1L);
        TWO = get(2L);
        THREE = get(3L);
    }
}
