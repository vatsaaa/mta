// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.cos;

import java.io.IOException;
import java.io.OutputStream;
import org.apache.pdfbox.exceptions.COSVisitorException;

public class COSNull extends COSBase
{
    public static final byte[] NULL_BYTES;
    public static final COSNull NULL;
    
    private COSNull() {
    }
    
    @Override
    public Object accept(final ICOSVisitor visitor) throws COSVisitorException {
        return visitor.visitFromNull(this);
    }
    
    public void writePDF(final OutputStream output) throws IOException {
        output.write(COSNull.NULL_BYTES);
    }
    
    static {
        NULL_BYTES = new byte[] { 110, 117, 108, 108 };
        NULL = new COSNull();
    }
}
