// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.cos;

import org.apache.pdfbox.exceptions.COSVisitorException;
import org.apache.pdfbox.pdfparser.ConformingPDFParser;

public class COSUnread extends COSBase
{
    private long objectNumber;
    private long generation;
    private ConformingPDFParser parser;
    
    public COSUnread() {
    }
    
    public COSUnread(final long objectNumber, final long generation) {
        this();
        this.objectNumber = objectNumber;
        this.generation = generation;
    }
    
    public COSUnread(final long objectNumber, final long generation, final ConformingPDFParser parser) {
        this(objectNumber, generation);
        this.parser = parser;
    }
    
    @Override
    public Object accept(final ICOSVisitor visitor) throws COSVisitorException {
        throw new UnsupportedOperationException("COSUnread can not be written/visited.");
    }
    
    @Override
    public String toString() {
        return "COSUnread{" + this.objectNumber + "," + this.generation + "}";
    }
    
    public long getObjectNumber() {
        return this.objectNumber;
    }
    
    public void setObjectNumber(final long objectNumber) {
        this.objectNumber = objectNumber;
    }
    
    public long getGeneration() {
        return this.generation;
    }
    
    public void setGeneration(final long generation) {
        this.generation = generation;
    }
    
    public ConformingPDFParser getParser() {
        return this.parser;
    }
    
    public void setParser(final ConformingPDFParser parser) {
        this.parser = parser;
    }
}
