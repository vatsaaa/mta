// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox;

import java.io.IOException;
import org.apache.pdfbox.pdmodel.interactive.form.PDAcroForm;
import org.apache.pdfbox.pdmodel.fdf.FDFDocument;
import org.apache.pdfbox.pdmodel.PDDocument;

public class ExportFDF
{
    public static void main(final String[] args) throws Exception {
        final ExportFDF exporter = new ExportFDF();
        exporter.exportFDF(args);
    }
    
    private void exportFDF(final String[] args) throws Exception {
        PDDocument pdf = null;
        FDFDocument fdf = null;
        try {
            if (args.length != 1 && args.length != 2) {
                usage();
            }
            else {
                pdf = PDDocument.load(args[0]);
                final PDAcroForm form = pdf.getDocumentCatalog().getAcroForm();
                if (form == null) {
                    System.err.println("Error: This PDF does not contain a form.");
                }
                else {
                    String fdfName = null;
                    if (args.length == 2) {
                        fdfName = args[1];
                    }
                    else if (args[0].length() > 4) {
                        fdfName = args[0].substring(0, args[0].length() - 4) + ".fdf";
                    }
                    fdf = form.exportFDF();
                    fdf.save(fdfName);
                }
            }
        }
        finally {
            this.close(fdf);
            this.close(pdf);
        }
    }
    
    private static void usage() {
        System.err.println("usage: org.apache.pdfbox.ExortFDF <pdf-file> [output-fdf-file]");
        System.err.println("    [output-fdf-file] - Default is pdf name, test.pdf->test.fdf");
    }
    
    public void close(final FDFDocument doc) throws IOException {
        if (doc != null) {
            doc.close();
        }
    }
    
    public void close(final PDDocument doc) throws IOException {
        if (doc != null) {
            doc.close();
        }
    }
}
