// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox;

import java.io.InputStream;
import org.apache.pdfbox.pdmodel.common.filespecification.PDEmbeddedFile;
import java.util.Iterator;
import org.apache.pdfbox.pdmodel.PDEmbeddedFilesNameTreeNode;
import org.apache.pdfbox.pdmodel.PDDocumentNameDictionary;
import org.apache.pdfbox.pdmodel.PDDocumentCatalog;
import org.apache.pdfbox.pdmodel.encryption.AccessPermission;
import java.io.Writer;
import org.apache.pdfbox.pdmodel.common.filespecification.PDComplexFileSpecification;
import java.util.Map;
import org.apache.pdfbox.util.PDFTextStripper;
import org.apache.pdfbox.util.PDFText2HTML;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.IOException;
import org.apache.pdfbox.pdmodel.encryption.DecryptionMaterial;
import org.apache.pdfbox.pdmodel.encryption.StandardDecryptionMaterial;
import org.apache.pdfbox.io.RandomAccess;
import org.apache.pdfbox.pdmodel.PDDocument;
import java.io.File;

public class ExtractText
{
    private static final String PASSWORD = "-password";
    private static final String ENCODING = "-encoding";
    private static final String CONSOLE = "-console";
    private static final String START_PAGE = "-startPage";
    private static final String END_PAGE = "-endPage";
    private static final String SORT = "-sort";
    private static final String IGNORE_BEADS = "-ignoreBeads";
    private static final String DEBUG = "-debug";
    private static final String HTML = "-html";
    private static final String FORCE = "-force";
    private static final String NONSEQ = "-nonSeq";
    private boolean debug;
    
    private ExtractText() {
        this.debug = false;
    }
    
    public static void main(final String[] args) throws Exception {
        final ExtractText extractor = new ExtractText();
        extractor.startExtraction(args);
    }
    
    public void startExtraction(final String[] args) throws Exception {
        boolean toConsole = false;
        boolean toHTML = false;
        boolean force = false;
        boolean sort = false;
        boolean separateBeads = true;
        boolean useNonSeqParser = false;
        String password = "";
        String encoding = null;
        String pdfFile = null;
        String outputFile = null;
        String ext = ".txt";
        int startPage = 1;
        int endPage = Integer.MAX_VALUE;
        for (int i = 0; i < args.length; ++i) {
            if (args[i].equals("-password")) {
                if (++i >= args.length) {
                    usage();
                }
                password = args[i];
            }
            else if (args[i].equals("-encoding")) {
                if (++i >= args.length) {
                    usage();
                }
                encoding = args[i];
            }
            else if (args[i].equals("-startPage")) {
                if (++i >= args.length) {
                    usage();
                }
                startPage = Integer.parseInt(args[i]);
            }
            else if (args[i].equals("-html")) {
                toHTML = true;
                ext = ".html";
            }
            else if (args[i].equals("-sort")) {
                sort = true;
            }
            else if (args[i].equals("-ignoreBeads")) {
                separateBeads = false;
            }
            else if (args[i].equals("-debug")) {
                this.debug = true;
            }
            else if (args[i].equals("-endPage")) {
                if (++i >= args.length) {
                    usage();
                }
                endPage = Integer.parseInt(args[i]);
            }
            else if (args[i].equals("-console")) {
                toConsole = true;
            }
            else if (args[i].equals("-force")) {
                force = true;
            }
            else if (args[i].equals("-nonSeq")) {
                useNonSeqParser = true;
            }
            else if (pdfFile == null) {
                pdfFile = args[i];
            }
            else {
                outputFile = args[i];
            }
        }
        if (pdfFile == null) {
            usage();
        }
        else {
            Writer output = null;
            PDDocument document = null;
            try {
                long startTime = this.startProcessing("Loading PDF " + pdfFile);
                if (outputFile == null && pdfFile.length() > 4) {
                    outputFile = new File(pdfFile.substring(0, pdfFile.length() - 4) + ext).getAbsolutePath();
                }
                if (useNonSeqParser) {
                    document = PDDocument.loadNonSeq(new File(pdfFile), null, password);
                }
                else {
                    document = PDDocument.load(pdfFile, force);
                    if (document.isEncrypted()) {
                        final StandardDecryptionMaterial sdm = new StandardDecryptionMaterial(password);
                        document.openProtection(sdm);
                    }
                }
                final AccessPermission ap = document.getCurrentAccessPermission();
                if (!ap.canExtractContent()) {
                    throw new IOException("You do not have permission to extract text");
                }
                this.stopProcessing("Time for loading: ", startTime);
                if (encoding == null && toHTML) {
                    encoding = "UTF-8";
                }
                if (toConsole) {
                    output = new OutputStreamWriter(System.out);
                }
                else if (encoding != null) {
                    output = new OutputStreamWriter(new FileOutputStream(outputFile), encoding);
                }
                else {
                    output = new OutputStreamWriter(new FileOutputStream(outputFile));
                }
                PDFTextStripper stripper = null;
                if (toHTML) {
                    stripper = new PDFText2HTML(encoding);
                }
                else {
                    stripper = new PDFTextStripper(encoding);
                }
                stripper.setForceParsing(force);
                stripper.setSortByPosition(sort);
                stripper.setShouldSeparateByBeads(separateBeads);
                stripper.setStartPage(startPage);
                stripper.setEndPage(endPage);
                startTime = this.startProcessing("Starting text extraction");
                if (this.debug) {
                    System.err.println("Writing to " + outputFile);
                }
                stripper.writeText(document, output);
                final PDDocumentCatalog catalog = document.getDocumentCatalog();
                final PDDocumentNameDictionary names = catalog.getNames();
                if (names != null) {
                    final PDEmbeddedFilesNameTreeNode embeddedFiles = names.getEmbeddedFiles();
                    if (embeddedFiles != null) {
                        final Map<String, Object> embeddedFileNames = embeddedFiles.getNames();
                        if (embeddedFileNames != null) {
                            for (final Map.Entry<String, Object> ent : embeddedFileNames.entrySet()) {
                                if (this.debug) {
                                    System.err.println("Processing embedded file " + ent.getKey() + ":");
                                }
                                final PDComplexFileSpecification spec = ent.getValue();
                                final PDEmbeddedFile file = spec.getEmbeddedFile();
                                if (file.getSubtype().equals("application/pdf")) {
                                    if (this.debug) {
                                        System.err.println("  is PDF (size=" + file.getSize() + ")");
                                    }
                                    final InputStream fis = file.createInputStream();
                                    PDDocument subDoc = null;
                                    try {
                                        subDoc = PDDocument.load(fis);
                                    }
                                    finally {
                                        fis.close();
                                    }
                                    try {
                                        stripper.writeText(subDoc, output);
                                    }
                                    finally {
                                        subDoc.close();
                                    }
                                }
                            }
                        }
                    }
                }
                this.stopProcessing("Time for extraction: ", startTime);
            }
            finally {
                if (output != null) {
                    output.close();
                }
                if (document != null) {
                    document.close();
                }
            }
        }
    }
    
    private long startProcessing(final String message) {
        if (this.debug) {
            System.err.println(message);
        }
        return System.currentTimeMillis();
    }
    
    private void stopProcessing(final String message, final long startTime) {
        if (this.debug) {
            final long stopTime = System.currentTimeMillis();
            final float elapsedTime = (stopTime - startTime) / 1000.0f;
            System.err.println(message + elapsedTime + " seconds");
        }
    }
    
    private static void usage() {
        System.err.println("Usage: java -jar pdfbox-app-x.y.z.jar ExtractText [OPTIONS] <PDF file> [Text File]\n  -password  <password>        Password to decrypt document\n  -encoding  <output encoding> (ISO-8859-1,UTF-16BE,UTF-16LE,...)\n  -console                     Send text to console instead of file\n  -html                        Output in HTML format instead of raw text\n  -sort                        Sort the text before writing\n  -ignoreBeads                 Disables the separation by beads\n  -force                       Enables pdfbox to ignore corrupt objects\n  -debug                       Enables debug output about the time consumption of every stage\n  -startPage <number>          The first page to start extraction(1 based)\n  -endPage <number>            The last page to extract(inclusive)\n  -nonSeq                      Enables the new non-sequential parser\n  <PDF file>                   The PDF document to use\n  [Text File]                  The file to write the text to\n");
        System.exit(1);
    }
}
