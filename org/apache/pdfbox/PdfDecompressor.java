// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox;

import java.util.Iterator;
import org.apache.pdfbox.persistence.util.COSObjectKey;
import org.apache.pdfbox.pdfparser.PDFObjectStreamParser;
import org.apache.pdfbox.cos.COSStream;
import org.apache.pdfbox.cos.COSObject;
import org.apache.pdfbox.pdmodel.PDDocument;

public class PdfDecompressor
{
    public static void main(final String[] args) {
        if (args.length < 1) {
            usage();
        }
        final String inputFilename = args[0];
        String outputFilename;
        if (args.length > 1) {
            outputFilename = args[1];
        }
        else if (inputFilename.matches(".*\\.[pP][dD][fF]$")) {
            outputFilename = inputFilename.replaceAll("\\.[pP][dD][fF]$", ".unc.pdf");
        }
        else {
            outputFilename = inputFilename + ".unc.pdf";
        }
        PDDocument doc = null;
        try {
            doc = PDDocument.load(inputFilename);
            for (final COSObject objStream : doc.getDocument().getObjectsByType("ObjStm")) {
                final COSStream stream = (COSStream)objStream.getObject();
                final PDFObjectStreamParser sp = new PDFObjectStreamParser(stream, doc.getDocument());
                sp.parse();
                for (final COSObject next : sp.getObjects()) {
                    final COSObjectKey key = new COSObjectKey(next);
                    final COSObject obj = doc.getDocument().getObjectFromPool(key);
                    obj.setObject(next.getObject());
                }
                doc.getDocument().removeObject(new COSObjectKey(objStream));
            }
            doc.save(outputFilename);
        }
        catch (Exception e) {
            System.out.println("Error processing file: " + e.getMessage());
        }
        finally {
            if (doc != null) {
                try {
                    doc.close();
                }
                catch (Exception ex) {}
            }
        }
    }
    
    private static void usage() {
        System.err.println("Usage: java -cp /path/to/pdfbox.jar;/path/to/commons-logging-api.jar org.apache.pdfbox.PdfDecompressor <input PDF File> [<Output PDF File>]\n  <input PDF File>       The PDF document to decompress\n  <output PDF File>      The output filename (default is to replace .pdf with .unc.pdf)");
        System.exit(1);
    }
}
