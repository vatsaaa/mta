// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox;

import java.io.IOException;
import org.apache.pdfbox.pdmodel.interactive.form.PDAcroForm;
import org.apache.pdfbox.pdmodel.PDDocumentCatalog;
import org.apache.pdfbox.pdmodel.fdf.FDFDocument;
import org.apache.pdfbox.pdmodel.PDDocument;

public class ImportXFDF
{
    public void importFDF(final PDDocument pdfDocument, final FDFDocument fdfDocument) throws IOException {
        final PDDocumentCatalog docCatalog = pdfDocument.getDocumentCatalog();
        final PDAcroForm acroForm = docCatalog.getAcroForm();
        acroForm.setCacheFields(true);
        acroForm.importFDF(fdfDocument);
    }
    
    public static void main(final String[] args) throws Exception {
        final ImportXFDF importer = new ImportXFDF();
        importer.importXFDF(args);
    }
    
    private void importXFDF(final String[] args) throws Exception {
        PDDocument pdf = null;
        FDFDocument fdf = null;
        try {
            if (args.length != 3) {
                usage();
            }
            else {
                final ImportFDF importer = new ImportFDF();
                pdf = PDDocument.load(args[0]);
                fdf = FDFDocument.loadXFDF(args[1]);
                importer.importFDF(pdf, fdf);
                pdf.save(args[2]);
                fdf.save("tmp/outputXFDFtoPDF.fdf");
            }
        }
        finally {
            this.close(fdf);
            this.close(pdf);
        }
    }
    
    private static void usage() {
        System.err.println("usage: org.apache.pdfbox.ImportXFDF <pdf-file> <fdf-file> <output-file>");
    }
    
    public void close(final FDFDocument doc) throws IOException {
        if (doc != null) {
            doc.close();
        }
    }
    
    public void close(final PDDocument doc) throws IOException {
        if (doc != null) {
            doc.close();
        }
    }
}
