// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.filter;

import org.apache.commons.logging.LogFactory;
import java.io.IOException;
import java.awt.image.DataBuffer;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import javax.imageio.ImageIO;
import org.apache.pdfbox.cos.COSDictionary;
import java.io.OutputStream;
import java.io.InputStream;
import org.apache.commons.logging.Log;

public class JPXFilter implements Filter
{
    private static final Log log;
    
    public void decode(final InputStream compressedData, final OutputStream result, final COSDictionary options, final int filterIndex) throws IOException {
        final BufferedImage bi = ImageIO.read(compressedData);
        if (bi != null) {
            final DataBuffer dBuf = bi.getData().getDataBuffer();
            if (dBuf.getDataType() == 0) {
                result.write(((DataBufferByte)dBuf).getData());
            }
            else {
                JPXFilter.log.error("Image data buffer not of type byte but type " + dBuf.getDataType());
            }
        }
    }
    
    public void encode(final InputStream rawData, final OutputStream result, final COSDictionary options, final int filterIndex) throws IOException {
        System.err.println("Warning: JPXFilter.encode is not implemented yet, skipping this stream.");
    }
    
    static {
        log = LogFactory.getLog(JPXFilter.class);
    }
}
