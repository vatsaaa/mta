// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.filter;

import org.apache.commons.logging.LogFactory;
import java.io.IOException;
import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.io.ccitt.FillOrderChangeInputStream;
import org.apache.pdfbox.io.ccitt.CCITTFaxG31DDecodeInputStream;
import java.io.ByteArrayInputStream;
import org.apache.pdfbox.io.IOUtils;
import org.apache.pdfbox.cos.COSArray;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.cos.COSDictionary;
import java.io.OutputStream;
import java.io.InputStream;
import org.apache.commons.logging.Log;

public class CCITTFaxDecodeFilter implements Filter
{
    private static final Log log;
    
    public void decode(final InputStream compressedData, final OutputStream result, final COSDictionary options, final int filterIndex) throws IOException {
        final COSBase decodeP = options.getDictionaryObject(COSName.DECODE_PARMS, COSName.DP);
        COSDictionary decodeParms = null;
        if (decodeP instanceof COSDictionary) {
            decodeParms = (COSDictionary)decodeP;
        }
        else if (decodeP instanceof COSArray) {
            decodeParms = (COSDictionary)((COSArray)decodeP).get(filterIndex);
        }
        final int length = options.getInt(COSName.LENGTH, -1);
        byte[] compressed = null;
        if (length != -1) {
            compressed = new byte[length];
            final long written = IOUtils.populateBuffer(compressedData, compressed);
            if (written != compressed.length) {
                CCITTFaxDecodeFilter.log.warn("Buffer for compressed data did not match the length of the actual compressed data");
            }
        }
        else {
            compressed = IOUtils.toByteArray(compressedData);
        }
        final int cols = decodeParms.getInt(COSName.COLUMNS, 1728);
        int rows = decodeParms.getInt(COSName.ROWS, 0);
        final int height = options.getInt(COSName.HEIGHT, COSName.H, 0);
        if (rows > 0 && height > 0) {
            rows = Math.min(rows, height);
        }
        else {
            rows = Math.max(rows, height);
        }
        final int k = decodeParms.getInt(COSName.K, 0);
        final int arraySize = (cols + 7) / 8 * rows;
        final TIFFFaxDecoder faxDecoder = new TIFFFaxDecoder(1, cols, rows);
        final long tiffOptions = 0L;
        if (k == 0) {
            InputStream in = new CCITTFaxG31DDecodeInputStream(new ByteArrayInputStream(compressed), cols);
            in = new FillOrderChangeInputStream(in);
            IOUtils.copy(in, result);
            in.close();
        }
        else if (k > 0) {
            final byte[] decompressed = new byte[arraySize];
            faxDecoder.decode2D(decompressed, compressed, 0, rows, tiffOptions);
            result.write(decompressed);
        }
        else if (k < 0) {
            final byte[] decompressed = new byte[arraySize];
            faxDecoder.decodeT6(decompressed, compressed, 0, rows, tiffOptions);
            result.write(decompressed);
        }
    }
    
    public void encode(final InputStream rawData, final OutputStream result, final COSDictionary options, final int filterIndex) throws IOException {
        CCITTFaxDecodeFilter.log.warn("CCITTFaxDecode.encode is not implemented yet, skipping this stream.");
    }
    
    static {
        log = LogFactory.getLog(CCITTFaxDecodeFilter.class);
    }
}
