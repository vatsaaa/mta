// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.filter;

import org.apache.commons.logging.LogFactory;
import java.io.IOException;
import javax.imageio.ImageReader;
import java.util.Iterator;
import java.awt.image.DataBuffer;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import javax.imageio.ImageIO;
import org.apache.pdfbox.cos.COSDictionary;
import java.io.OutputStream;
import java.io.InputStream;
import org.apache.commons.logging.Log;

public class JBIG2Filter implements Filter
{
    private static final Log log;
    
    public void decode(final InputStream compressedData, final OutputStream result, final COSDictionary options, final int filterIndex) throws IOException {
        final BufferedImage bi = ImageIO.read(compressedData);
        if (bi != null) {
            final DataBuffer dBuf = bi.getData().getDataBuffer();
            if (dBuf.getDataType() == 0) {
                result.write(((DataBufferByte)dBuf).getData());
            }
            else {
                JBIG2Filter.log.error("Image data buffer not of type byte but type " + dBuf.getDataType());
            }
        }
        else {
            final Iterator<ImageReader> reader = ImageIO.getImageReadersByFormatName("JBIG2");
            if (!reader.hasNext()) {
                JBIG2Filter.log.error("Can't find an ImageIO plugin to decode the JBIG2 encoded datastream.");
            }
            else {
                JBIG2Filter.log.error("Something went wrong when decoding the JBIG2 encoded datastream.");
            }
        }
    }
    
    public void encode(final InputStream rawData, final OutputStream result, final COSDictionary options, final int filterIndex) throws IOException {
        System.err.println("Warning: JBIG2.encode is not implemented yet, skipping this stream.");
    }
    
    static {
        log = LogFactory.getLog(JBIG2Filter.class);
    }
}
