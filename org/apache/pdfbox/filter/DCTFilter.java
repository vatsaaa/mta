// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.filter;

import org.apache.commons.logging.LogFactory;
import java.io.IOException;
import org.apache.pdfbox.cos.COSDictionary;
import java.io.OutputStream;
import java.io.InputStream;
import org.apache.commons.logging.Log;

public class DCTFilter implements Filter
{
    private static final Log log;
    
    public void decode(final InputStream compressedData, final OutputStream result, final COSDictionary options, final int filterIndex) throws IOException {
        DCTFilter.log.warn("DCTFilter.decode is not implemented yet, skipping this stream.");
    }
    
    public void encode(final InputStream rawData, final OutputStream result, final COSDictionary options, final int filterIndex) throws IOException {
        DCTFilter.log.warn("DCTFilter.encode is not implemented yet, skipping this stream.");
    }
    
    static {
        log = LogFactory.getLog(DCTFilter.class);
    }
}
