// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.filter;

import org.apache.pdfbox.io.ASCII85OutputStream;
import java.io.IOException;
import org.apache.pdfbox.io.ASCII85InputStream;
import org.apache.pdfbox.cos.COSDictionary;
import java.io.OutputStream;
import java.io.InputStream;

public class ASCII85Filter implements Filter
{
    public void decode(final InputStream compressedData, final OutputStream result, final COSDictionary options, final int filterIndex) throws IOException {
        ASCII85InputStream is = null;
        try {
            is = new ASCII85InputStream(compressedData);
            final byte[] buffer = new byte[1024];
            int amountRead = 0;
            while ((amountRead = is.read(buffer, 0, 1024)) != -1) {
                result.write(buffer, 0, amountRead);
            }
            result.flush();
        }
        finally {
            if (is != null) {
                is.close();
            }
        }
    }
    
    public void encode(final InputStream rawData, final OutputStream result, final COSDictionary options, final int filterIndex) throws IOException {
        final ASCII85OutputStream os = new ASCII85OutputStream(result);
        final byte[] buffer = new byte[1024];
        int amountRead = 0;
        while ((amountRead = rawData.read(buffer, 0, 1024)) != -1) {
            os.write(buffer, 0, amountRead);
        }
        os.close();
        result.flush();
    }
}
