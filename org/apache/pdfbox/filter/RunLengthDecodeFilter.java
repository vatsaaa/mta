// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.filter;

import org.apache.commons.logging.LogFactory;
import java.io.IOException;
import org.apache.pdfbox.cos.COSDictionary;
import java.io.OutputStream;
import java.io.InputStream;
import org.apache.commons.logging.Log;

public class RunLengthDecodeFilter implements Filter
{
    private static final Log log;
    private static final int RUN_LENGTH_EOD = 128;
    
    public void decode(final InputStream compressedData, final OutputStream result, final COSDictionary options, final int filterIndex) throws IOException {
        int dupAmount = -1;
        final byte[] buffer = new byte[128];
        while ((dupAmount = compressedData.read()) != -1 && dupAmount != 128) {
            if (dupAmount <= 127) {
                for (int amountToCopy = dupAmount + 1, compressedRead = 0; amountToCopy > 0; amountToCopy -= compressedRead) {
                    compressedRead = compressedData.read(buffer, 0, amountToCopy);
                    result.write(buffer, 0, compressedRead);
                }
            }
            else {
                final int dupByte = compressedData.read();
                for (int i = 0; i < 257 - dupAmount; ++i) {
                    result.write(dupByte);
                }
            }
        }
    }
    
    public void encode(final InputStream rawData, final OutputStream result, final COSDictionary options, final int filterIndex) throws IOException {
        RunLengthDecodeFilter.log.warn("RunLengthDecodeFilter.encode is not implemented yet, skipping this stream.");
    }
    
    static {
        log = LogFactory.getLog(RunLengthDecodeFilter.class);
    }
}
