// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.filter;

import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import org.apache.pdfbox.cos.COSName;
import java.util.Map;

public class FilterManager
{
    private Map<COSName, Filter> filters;
    
    public FilterManager() {
        this.filters = new HashMap<COSName, Filter>();
        final Filter flateFilter = new FlateFilter();
        final Filter dctFilter = new DCTFilter();
        final Filter ccittFaxFilter = new CCITTFaxDecodeFilter();
        final Filter lzwFilter = new LZWFilter();
        final Filter asciiHexFilter = new ASCIIHexFilter();
        final Filter ascii85Filter = new ASCII85Filter();
        final Filter runLengthFilter = new RunLengthDecodeFilter();
        final Filter cryptFilter = new CryptFilter();
        final Filter jpxFilter = new JPXFilter();
        final Filter jbig2Filter = new JBIG2Filter();
        this.addFilter(COSName.FLATE_DECODE, flateFilter);
        this.addFilter(COSName.FLATE_DECODE_ABBREVIATION, flateFilter);
        this.addFilter(COSName.DCT_DECODE, dctFilter);
        this.addFilter(COSName.DCT_DECODE_ABBREVIATION, dctFilter);
        this.addFilter(COSName.CCITTFAX_DECODE, ccittFaxFilter);
        this.addFilter(COSName.CCITTFAX_DECODE_ABBREVIATION, ccittFaxFilter);
        this.addFilter(COSName.LZW_DECODE, lzwFilter);
        this.addFilter(COSName.LZW_DECODE_ABBREVIATION, lzwFilter);
        this.addFilter(COSName.ASCII_HEX_DECODE, asciiHexFilter);
        this.addFilter(COSName.ASCII_HEX_DECODE_ABBREVIATION, asciiHexFilter);
        this.addFilter(COSName.ASCII85_DECODE, ascii85Filter);
        this.addFilter(COSName.ASCII85_DECODE_ABBREVIATION, ascii85Filter);
        this.addFilter(COSName.RUN_LENGTH_DECODE, runLengthFilter);
        this.addFilter(COSName.RUN_LENGTH_DECODE_ABBREVIATION, runLengthFilter);
        this.addFilter(COSName.CRYPT, cryptFilter);
        this.addFilter(COSName.JPX_DECODE, jpxFilter);
        this.addFilter(COSName.JBIG2_DECODE, jbig2Filter);
    }
    
    public Collection<Filter> getFilters() {
        return this.filters.values();
    }
    
    public void addFilter(final COSName filterName, final Filter filter) {
        this.filters.put(filterName, filter);
    }
    
    public Filter getFilter(final COSName filterName) throws IOException {
        final Filter filter = this.filters.get(filterName);
        if (filter == null) {
            throw new IOException("Unknown stream filter:" + filterName);
        }
        return filter;
    }
    
    public Filter getFilter(final String filterName) throws IOException {
        return this.getFilter(COSName.getPDFName(filterName));
    }
}
