// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.filter;

import java.util.HashMap;
import java.util.Map;

class LZWNode
{
    private final long code;
    private final Map<Byte, LZWNode> subNodes;
    
    public LZWNode(final long codeValue) {
        this.subNodes = new HashMap<Byte, LZWNode>();
        this.code = codeValue;
    }
    
    public int childCount() {
        return this.subNodes.size();
    }
    
    public void setNode(final byte b, final LZWNode node) {
        this.subNodes.put(b, node);
    }
    
    public LZWNode getNode(final byte data) {
        return this.subNodes.get(data);
    }
    
    public LZWNode getNode(final byte[] data) {
        LZWNode current = this;
        for (int i = 0; i < data.length && current != null; current = current.getNode(data[i]), ++i) {}
        return current;
    }
    
    public long getCode() {
        return this.code;
    }
}
