// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.filter;

import java.io.IOException;
import org.apache.pdfbox.cos.COSDictionary;
import java.io.OutputStream;
import java.io.InputStream;

public interface Filter
{
    void decode(final InputStream p0, final OutputStream p1, final COSDictionary p2, final int p3) throws IOException;
    
    void encode(final InputStream p0, final OutputStream p1, final COSDictionary p2, final int p3) throws IOException;
}
