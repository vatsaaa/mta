// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.filter;

import java.io.IOException;
import org.apache.pdfbox.cos.COSDictionary;
import java.io.OutputStream;
import java.io.InputStream;

public class IdentityFilter implements Filter
{
    private static final int BUFFER_SIZE = 1024;
    
    public void decode(final InputStream compressedData, final OutputStream result, final COSDictionary options, final int filterIndex) throws IOException {
        final byte[] buffer = new byte[1024];
        int amountRead = 0;
        while ((amountRead = compressedData.read(buffer, 0, 1024)) != -1) {
            result.write(buffer, 0, amountRead);
        }
        result.flush();
    }
    
    public void encode(final InputStream rawData, final OutputStream result, final COSDictionary options, final int filterIndex) throws IOException {
        final byte[] buffer = new byte[1024];
        int amountRead = 0;
        while ((amountRead = rawData.read(buffer, 0, 1024)) != -1) {
            result.write(buffer, 0, amountRead);
        }
        result.flush();
    }
}
