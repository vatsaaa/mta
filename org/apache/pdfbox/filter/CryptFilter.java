// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.filter;

import java.io.IOException;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.cos.COSDictionary;
import java.io.OutputStream;
import java.io.InputStream;

public class CryptFilter implements Filter
{
    public void decode(final InputStream compressedData, final OutputStream result, final COSDictionary options, final int filterIndex) throws IOException {
        final COSName encryptionName = (COSName)options.getDictionaryObject(COSName.NAME);
        if (encryptionName == null || encryptionName.equals(COSName.IDENTITY)) {
            final Filter identityFilter = new IdentityFilter();
            identityFilter.decode(compressedData, result, options, filterIndex);
            return;
        }
        throw new IOException("Unsupported crypt filter " + encryptionName.getName());
    }
    
    public void encode(final InputStream rawData, final OutputStream result, final COSDictionary options, final int filterIndex) throws IOException {
        final COSName encryptionName = (COSName)options.getDictionaryObject(COSName.NAME);
        if (encryptionName == null || encryptionName.equals(COSName.IDENTITY)) {
            final Filter identityFilter = new IdentityFilter();
            identityFilter.encode(rawData, result, options, filterIndex);
            return;
        }
        throw new IOException("Unsupported crypt filter " + encryptionName.getName());
    }
}
