// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.filter;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

final class LZWDictionary
{
    private Map<Long, byte[]> codeToData;
    private LZWNode root;
    private byte[] buffer;
    private int bufferNextWrite;
    private long nextCode;
    private int codeSize;
    private LZWNode previous;
    private LZWNode current;
    
    LZWDictionary() {
        this.codeToData = new HashMap<Long, byte[]>();
        this.root = new LZWNode(0L);
        this.buffer = new byte[8];
        this.bufferNextWrite = 0;
        this.nextCode = 258L;
        this.codeSize = 9;
        this.previous = null;
        this.current = this.root;
    }
    
    public byte[] getData(final long code) {
        byte[] result = this.codeToData.get(code);
        if (result == null && code < 256L) {
            this.addRootNode((byte)code);
            result = this.codeToData.get(code);
        }
        return result;
    }
    
    public void visit(final byte[] data) throws IOException {
        for (int i = 0; i < data.length; ++i) {
            this.visit(data[i]);
        }
    }
    
    public void visit(final byte data) throws IOException {
        if (this.buffer.length == this.bufferNextWrite) {
            final byte[] nextBuffer = new byte[2 * this.buffer.length];
            System.arraycopy(this.buffer, 0, nextBuffer, 0, this.buffer.length);
            this.buffer = nextBuffer;
        }
        this.buffer[this.bufferNextWrite++] = data;
        this.previous = this.current;
        this.current = this.current.getNode(data);
        if (this.current == null) {
            long code;
            if (this.previous == this.root) {
                code = (data & 0xFF);
            }
            else {
                code = this.nextCode++;
            }
            this.current = new LZWNode(code);
            this.previous.setNode(data, this.current);
            final byte[] sav = new byte[this.bufferNextWrite];
            System.arraycopy(this.buffer, 0, sav, 0, this.bufferNextWrite);
            this.codeToData.put(code, sav);
            this.bufferNextWrite = 0;
            this.current = this.root;
            this.visit(data);
            this.resetCodeSize();
        }
    }
    
    public long getNextCode() {
        return this.nextCode;
    }
    
    public int getCodeSize() {
        return this.codeSize;
    }
    
    private void resetCodeSize() {
        if (this.nextCode < 512L) {
            this.codeSize = 9;
        }
        else if (this.nextCode < 1024L) {
            this.codeSize = 10;
        }
        else if (this.nextCode < 2048L) {
            this.codeSize = 11;
        }
        else {
            this.codeSize = 12;
        }
    }
    
    public void clear() {
        this.bufferNextWrite = 0;
        this.current = this.root;
        this.previous = null;
    }
    
    public LZWNode getNode(final byte[] data) {
        LZWNode result = this.root.getNode(data);
        if (result == null && data.length == 1) {
            result = this.addRootNode(data[0]);
        }
        return result;
    }
    
    private LZWNode addRootNode(final byte b) {
        final long code = b & 0xFF;
        final LZWNode result = new LZWNode(code);
        this.root.setNode(b, result);
        this.codeToData.put(code, new byte[] { b });
        return result;
    }
}
