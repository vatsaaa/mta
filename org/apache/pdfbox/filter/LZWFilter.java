// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.filter;

import java.io.ByteArrayOutputStream;
import org.apache.pdfbox.io.NBitOutputStream;
import java.io.PushbackInputStream;
import java.io.IOException;
import java.io.StreamCorruptedException;
import org.apache.pdfbox.io.NBitInputStream;
import org.apache.pdfbox.cos.COSDictionary;
import java.io.OutputStream;
import java.io.InputStream;

public class LZWFilter implements Filter
{
    public static final long CLEAR_TABLE = 256L;
    public static final long EOD = 257L;
    
    public void decode(final InputStream compressedData, final OutputStream result, final COSDictionary options, final int filterIndex) throws IOException {
        NBitInputStream in = null;
        in = new NBitInputStream(compressedData);
        in.setBitsInChunk(9);
        LZWDictionary dic = new LZWDictionary();
        byte firstByte = 0;
        long nextCommand = 0L;
        while ((nextCommand = in.read()) != 257L) {
            if (nextCommand == 256L) {
                in.setBitsInChunk(9);
                dic = new LZWDictionary();
            }
            else {
                byte[] data = dic.getData(nextCommand);
                if (data == null) {
                    dic.visit(firstByte);
                    data = dic.getData(nextCommand);
                    dic.clear();
                }
                if (data == null) {
                    throw new StreamCorruptedException("Error: data is null");
                }
                dic.visit(data);
                if (dic.getNextCode() >= 2047L) {
                    in.setBitsInChunk(12);
                }
                else if (dic.getNextCode() >= 1023L) {
                    in.setBitsInChunk(11);
                }
                else if (dic.getNextCode() >= 511L) {
                    in.setBitsInChunk(10);
                }
                else {
                    in.setBitsInChunk(9);
                }
                firstByte = data[0];
                result.write(data);
            }
        }
        result.flush();
    }
    
    public void encode(final InputStream rawData, final OutputStream result, final COSDictionary options, final int filterIndex) throws IOException {
        final PushbackInputStream input = new PushbackInputStream(rawData, 4096);
        LZWDictionary dic = new LZWDictionary();
        final NBitOutputStream out = new NBitOutputStream(result);
        out.setBitsInChunk(9);
        out.write(256L);
        final ByteArrayOutputStream buffer = new ByteArrayOutputStream();
        int byteRead = 0;
        int i = 0;
        while ((byteRead = input.read()) != -1) {
            buffer.write(byteRead);
            dic.visit((byte)byteRead);
            out.setBitsInChunk(dic.getCodeSize());
            final LZWNode node = dic.getNode(buffer.toByteArray());
            final int nextByte = input.read();
            if (nextByte == -1) {
                out.write(node.getCode());
                buffer.reset();
                break;
            }
            final LZWNode next = node.getNode((byte)nextByte);
            if (next == null) {
                out.write(node.getCode());
                buffer.reset();
            }
            input.unread(nextByte);
            if (dic.getNextCode() == 4096L) {
                out.write(256L);
                dic = new LZWDictionary();
                input.unread(buffer.toByteArray());
                buffer.reset();
            }
            ++i;
        }
        if (dic.getNextCode() >= 2047L) {
            out.setBitsInChunk(12);
        }
        else if (dic.getNextCode() >= 1023L) {
            out.setBitsInChunk(11);
        }
        else if (dic.getNextCode() >= 511L) {
            out.setBitsInChunk(10);
        }
        else {
            out.setBitsInChunk(9);
        }
        out.write(257L);
        out.close();
        result.flush();
    }
}
