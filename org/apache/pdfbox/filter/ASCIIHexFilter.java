// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.filter;

import org.apache.commons.logging.LogFactory;
import org.apache.pdfbox.persistence.util.COSHEXTable;
import java.io.IOException;
import org.apache.pdfbox.cos.COSDictionary;
import java.io.OutputStream;
import java.io.InputStream;
import org.apache.commons.logging.Log;

public class ASCIIHexFilter implements Filter
{
    private static final Log log;
    private static final int[] REVERSE_HEX;
    
    private boolean isWhitespace(final int c) {
        return c == 0 || c == 9 || c == 10 || c == 12 || c == 13 || c == 32;
    }
    
    private boolean isEOD(final int c) {
        return c == 62;
    }
    
    public void decode(final InputStream compressedData, final OutputStream result, final COSDictionary options, final int filterIndex) throws IOException {
        int value = 0;
        int firstByte = 0;
        int secondByte = 0;
        while ((firstByte = compressedData.read()) != -1) {
            while (this.isWhitespace(firstByte)) {
                firstByte = compressedData.read();
            }
            if (this.isEOD(firstByte)) {
                break;
            }
            if (ASCIIHexFilter.REVERSE_HEX[firstByte] == -1) {
                ASCIIHexFilter.log.error("Invalid Hex Code; int: " + firstByte + " char: " + (char)firstByte);
            }
            value = ASCIIHexFilter.REVERSE_HEX[firstByte] * 16;
            secondByte = compressedData.read();
            if (this.isEOD(secondByte)) {
                result.write(value);
                break;
            }
            if (secondByte >= 0) {
                if (ASCIIHexFilter.REVERSE_HEX[secondByte] == -1) {
                    ASCIIHexFilter.log.error("Invalid Hex Code; int: " + secondByte + " char: " + (char)secondByte);
                }
                value += ASCIIHexFilter.REVERSE_HEX[secondByte];
            }
            result.write(value);
        }
        result.flush();
    }
    
    public void encode(final InputStream rawData, final OutputStream result, final COSDictionary options, final int filterIndex) throws IOException {
        int byteRead = 0;
        while ((byteRead = rawData.read()) != -1) {
            final int value = (byteRead + 256) % 256;
            result.write(COSHEXTable.TABLE[value]);
        }
        result.flush();
    }
    
    static {
        log = LogFactory.getLog(ASCIIHexFilter.class);
        REVERSE_HEX = new int[] { -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, -1, -1, -1, -1, -1, -1, -1, 10, 11, 12, 13, 14, 15, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 10, 11, 12, 13, 14, 15 };
    }
}
