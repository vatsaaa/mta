// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.filter;

import org.apache.commons.logging.LogFactory;
import java.util.zip.DeflaterOutputStream;
import java.util.zip.Inflater;
import java.io.ByteArrayOutputStream;
import org.apache.pdfbox.cos.COSBase;
import java.util.zip.DataFormatException;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import org.apache.pdfbox.cos.COSArray;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.cos.COSDictionary;
import java.io.OutputStream;
import java.io.InputStream;
import org.apache.commons.logging.Log;

public class FlateFilter implements Filter
{
    private static final Log LOG;
    private static final int BUFFER_SIZE = 16348;
    
    public void decode(final InputStream compressedData, final OutputStream result, final COSDictionary options, final int filterIndex) throws IOException {
        final COSBase baseObj = options.getDictionaryObject(COSName.DECODE_PARMS, COSName.DP);
        COSDictionary dict = null;
        if (baseObj instanceof COSDictionary) {
            dict = (COSDictionary)baseObj;
        }
        else if (baseObj instanceof COSArray) {
            final COSArray paramArray = (COSArray)baseObj;
            if (filterIndex < paramArray.size()) {
                dict = (COSDictionary)paramArray.getObject(filterIndex);
            }
        }
        else if (baseObj != null) {
            throw new IOException("Error: Expected COSArray or COSDictionary and not " + baseObj.getClass().getName());
        }
        int predictor = -1;
        int colors = -1;
        int bitsPerPixel = -1;
        int columns = -1;
        ByteArrayInputStream bais = null;
        ByteArrayOutputStream baos = null;
        if (dict != null) {
            predictor = dict.getInt(COSName.PREDICTOR);
            if (predictor > 1) {
                colors = dict.getInt(COSName.COLORS);
                bitsPerPixel = dict.getInt(COSName.BITS_PER_COMPONENT);
                columns = dict.getInt(COSName.COLUMNS);
            }
        }
        try {
            baos = this.decompress(compressedData);
            if (predictor == -1 || predictor == 1) {
                result.write(baos.toByteArray());
            }
            else {
                if (colors == -1) {
                    colors = 1;
                }
                if (bitsPerPixel == -1) {
                    bitsPerPixel = 8;
                }
                if (columns == -1) {
                    columns = 1;
                }
                bais = new ByteArrayInputStream(baos.toByteArray());
                final byte[] decodedData = this.decodePredictor(predictor, colors, bitsPerPixel, columns, bais);
                bais.close();
                bais = null;
                result.write(decodedData);
            }
            result.flush();
        }
        catch (DataFormatException exception) {
            FlateFilter.LOG.error("FlateFilter: stop reading corrupt stream due to a DataFormatException");
            final IOException io = new IOException();
            io.initCause(exception);
            throw io;
        }
        finally {
            if (bais != null) {
                bais.close();
            }
            if (baos != null) {
                baos.close();
            }
        }
    }
    
    private ByteArrayOutputStream decompress(final InputStream in) throws IOException, DataFormatException {
        final ByteArrayOutputStream out = new ByteArrayOutputStream();
        final byte[] buf = new byte[2048];
        int read = in.read(buf);
        if (read > 0) {
            final Inflater inflater = new Inflater();
            inflater.setInput(buf, 0, read);
            final byte[] res = new byte[2048];
            while (true) {
                final int resRead = inflater.inflate(res);
                if (resRead != 0) {
                    out.write(res, 0, resRead);
                }
                else {
                    if (inflater.finished() || inflater.needsDictionary()) {
                        break;
                    }
                    if (in.available() == 0) {
                        break;
                    }
                    read = in.read(buf);
                    inflater.setInput(buf, 0, read);
                }
            }
        }
        out.close();
        return out;
    }
    
    private byte[] decodePredictor(final int predictor, final int colors, final int bitsPerComponent, final int columns, final InputStream data) throws IOException {
        final ByteArrayOutputStream baos = new ByteArrayOutputStream();
        final byte[] buffer = new byte[2048];
        if (predictor == 1) {
            int i = 0;
            while ((i = data.read(buffer)) != -1) {
                baos.write(buffer, 0, i);
            }
        }
        else {
            final int bitsPerPixel = colors * bitsPerComponent;
            final int bytesPerPixel = (bitsPerPixel + 7) / 8;
            final int rowlength = (columns * bitsPerPixel + 7) / 8;
            final byte[] actline = new byte[rowlength];
            byte[] lastline = new byte[rowlength];
            boolean done = false;
            int linepredictor = predictor;
            while (!done && data.available() > 0) {
                if (predictor >= 10) {
                    linepredictor = data.read();
                    if (linepredictor == -1) {
                        done = true;
                        break;
                    }
                    linepredictor += 10;
                }
                for (int j = 0, offset = 0; offset < rowlength && (j = data.read(actline, offset, rowlength - offset)) != -1; offset += j) {}
                switch (linepredictor) {
                    case 2: {
                        if (bitsPerComponent != 8) {
                            throw new IOException("TIFF-Predictor with " + bitsPerComponent + " bits per component not supported");
                        }
                        for (int p = 0; p < rowlength; ++p) {
                            final int sub = actline[p] & 0xFF;
                            final int left = (p - bytesPerPixel >= 0) ? (actline[p - bytesPerPixel] & 0xFF) : 0;
                            actline[p] = (byte)(sub + left);
                        }
                        break;
                    }
                    case 11: {
                        for (int p = 0; p < rowlength; ++p) {
                            final int sub = actline[p];
                            final int left = (p - bytesPerPixel >= 0) ? actline[p - bytesPerPixel] : 0;
                            actline[p] = (byte)(sub + left);
                        }
                        break;
                    }
                    case 12: {
                        for (int p = 0; p < rowlength; ++p) {
                            final int up = actline[p] & 0xFF;
                            final int prior = lastline[p] & 0xFF;
                            actline[p] = (byte)(up + prior & 0xFF);
                        }
                        break;
                    }
                    case 13: {
                        for (int p = 0; p < rowlength; ++p) {
                            final int avg = actline[p] & 0xFF;
                            final int left = (p - bytesPerPixel >= 0) ? (actline[p - bytesPerPixel] & 0xFF) : 0;
                            final int up2 = lastline[p] & 0xFF;
                            actline[p] = (byte)(avg + (int)Math.floor((left + up2) / 2) & 0xFF);
                        }
                        break;
                    }
                    case 14: {
                        for (int p = 0; p < rowlength; ++p) {
                            final int paeth = actline[p] & 0xFF;
                            final int a = (p - bytesPerPixel >= 0) ? (actline[p - bytesPerPixel] & 0xFF) : 0;
                            final int b = lastline[p] & 0xFF;
                            final int c = (p - bytesPerPixel >= 0) ? (lastline[p - bytesPerPixel] & 0xFF) : 0;
                            final int value = a + b - c;
                            final int absa = Math.abs(value - a);
                            final int absb = Math.abs(value - b);
                            final int absc = Math.abs(value - c);
                            if (absa <= absb && absa <= absc) {
                                actline[p] = (byte)(paeth + a & 0xFF);
                            }
                            else if (absb <= absc) {
                                actline[p] = (byte)(paeth + b & 0xFF);
                            }
                            else {
                                actline[p] = (byte)(paeth + c & 0xFF);
                            }
                        }
                        break;
                    }
                }
                lastline = actline.clone();
                baos.write(actline, 0, actline.length);
            }
        }
        return baos.toByteArray();
    }
    
    public void encode(final InputStream rawData, final OutputStream result, final COSDictionary options, final int filterIndex) throws IOException {
        final DeflaterOutputStream out = new DeflaterOutputStream(result);
        int amountRead = 0;
        final int mayRead = rawData.available();
        if (mayRead > 0) {
            final byte[] buffer = new byte[Math.min(mayRead, 16348)];
            while ((amountRead = rawData.read(buffer, 0, Math.min(mayRead, 16348))) != -1) {
                out.write(buffer, 0, amountRead);
            }
        }
        out.close();
        result.flush();
    }
    
    static {
        LOG = LogFactory.getLog(FlateFilter.class);
    }
}
