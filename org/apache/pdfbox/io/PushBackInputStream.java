// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.io;

import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.io.PushbackInputStream;

public class PushBackInputStream extends PushbackInputStream
{
    private long offset;
    private final RandomAccessRead raInput;
    
    public PushBackInputStream(final InputStream input, final int size) throws IOException {
        super(input, size);
        this.offset = 0L;
        if (input == null) {
            throw new IOException("Error: input was null");
        }
        this.raInput = ((input instanceof RandomAccessRead) ? input : null);
    }
    
    public int peek() throws IOException {
        final int result = this.read();
        if (result != -1) {
            this.unread(result);
        }
        return result;
    }
    
    public long getOffset() {
        return this.offset;
    }
    
    @Override
    public int read() throws IOException {
        final int retval = super.read();
        if (retval != -1) {
            ++this.offset;
        }
        return retval;
    }
    
    @Override
    public int read(final byte[] b) throws IOException {
        return this.read(b, 0, b.length);
    }
    
    @Override
    public int read(final byte[] b, final int off, final int len) throws IOException {
        final int retval = super.read(b, off, len);
        if (retval != -1) {
            this.offset += retval;
        }
        return retval;
    }
    
    @Override
    public void unread(final int b) throws IOException {
        --this.offset;
        super.unread(b);
    }
    
    @Override
    public void unread(final byte[] b) throws IOException {
        this.unread(b, 0, b.length);
    }
    
    @Override
    public void unread(final byte[] b, final int off, final int len) throws IOException {
        if (len > 0) {
            this.offset -= len;
            super.unread(b, off, len);
        }
    }
    
    public boolean isEOF() throws IOException {
        final int peek = this.peek();
        return peek == -1;
    }
    
    public void fillBuffer() throws IOException {
        final int bufferLength = this.buf.length;
        final byte[] tmpBuffer = new byte[bufferLength];
        int amountRead;
        int totalAmountRead;
        for (amountRead = 0, totalAmountRead = 0; amountRead != -1 && totalAmountRead < bufferLength; totalAmountRead += amountRead) {
            amountRead = this.read(tmpBuffer, totalAmountRead, bufferLength - totalAmountRead);
            if (amountRead != -1) {}
        }
        this.unread(tmpBuffer, 0, totalAmountRead);
    }
    
    public byte[] readFully(final int length) throws IOException {
        final byte[] data = new byte[length];
        int amountRead;
        for (int pos = 0; pos < length; pos += amountRead) {
            amountRead = this.read(data, pos, length - pos);
            if (amountRead < 0) {
                throw new EOFException("Premature end of file");
            }
        }
        return data;
    }
    
    public void seek(final long newOffset) throws IOException {
        if (this.raInput == null) {
            throw new IOException("Provided stream of type " + this.in.getClass().getSimpleName() + " is not seekable.");
        }
        final int unreadLength = this.buf.length - this.pos;
        if (unreadLength > 0) {
            this.skip(unreadLength);
        }
        this.raInput.seek(newOffset);
        this.offset = newOffset;
    }
}
