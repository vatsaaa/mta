// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.io;

import java.io.IOException;
import java.io.InputStream;

public class NBitInputStream
{
    private int bitsInChunk;
    private InputStream in;
    private int currentByte;
    private int bitsLeftInCurrentByte;
    
    public NBitInputStream(final InputStream is) {
        this.in = is;
        this.bitsLeftInCurrentByte = 0;
        this.bitsInChunk = 8;
    }
    
    public void unread(long data) {
        data <<= this.bitsLeftInCurrentByte;
        this.currentByte = (int)((long)this.currentByte | data);
        this.bitsLeftInCurrentByte += this.bitsInChunk;
    }
    
    public long read() throws IOException {
        long retval = 0L;
        for (int i = 0; i < this.bitsInChunk && retval != -1L; ++i) {
            if (this.bitsLeftInCurrentByte == 0) {
                this.currentByte = this.in.read();
                this.bitsLeftInCurrentByte = 8;
            }
            if (this.currentByte == -1) {
                retval = -1L;
            }
            else {
                retval <<= 1;
                retval |= (this.currentByte >> this.bitsLeftInCurrentByte - 1 & 0x1);
                --this.bitsLeftInCurrentByte;
            }
        }
        return retval;
    }
    
    public int getBitsInChunk() {
        return this.bitsInChunk;
    }
    
    public void setBitsInChunk(final int bitsInChunkValue) {
        this.bitsInChunk = bitsInChunkValue;
    }
}
