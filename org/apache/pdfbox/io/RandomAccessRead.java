// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.io;

import java.io.IOException;

public interface RandomAccessRead extends SequentialRead
{
    void seek(final long p0) throws IOException;
    
    long length() throws IOException;
}
