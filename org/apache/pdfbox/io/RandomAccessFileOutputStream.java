// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.io;

import org.apache.pdfbox.cos.COSObject;
import org.apache.pdfbox.cos.COSNumber;
import java.io.IOException;
import org.apache.pdfbox.cos.COSBase;
import java.io.OutputStream;

public class RandomAccessFileOutputStream extends OutputStream
{
    private RandomAccess file;
    private long position;
    private long lengthWritten;
    private COSBase expectedLength;
    
    public RandomAccessFileOutputStream(final RandomAccess raf) throws IOException {
        this.lengthWritten = 0L;
        this.expectedLength = null;
        this.file = raf;
        this.position = raf.length();
    }
    
    public long getPosition() {
        return this.position;
    }
    
    public long getLengthWritten() {
        return this.lengthWritten;
    }
    
    public long getLength() {
        long length = -1L;
        if (this.expectedLength instanceof COSNumber) {
            length = ((COSNumber)this.expectedLength).intValue();
        }
        else if (this.expectedLength instanceof COSObject && ((COSObject)this.expectedLength).getObject() instanceof COSNumber) {
            length = ((COSNumber)((COSObject)this.expectedLength).getObject()).intValue();
        }
        if (length == -1L) {
            length = this.lengthWritten;
        }
        return length;
    }
    
    @Override
    public void write(final byte[] b, final int offset, final int length) throws IOException {
        this.file.seek(this.position + this.lengthWritten);
        this.lengthWritten += length;
        this.file.write(b, offset, length);
    }
    
    @Override
    public void write(final int b) throws IOException {
        this.file.seek(this.position + this.lengthWritten);
        ++this.lengthWritten;
        this.file.write(b);
    }
    
    public COSBase getExpectedLength() {
        return this.expectedLength;
    }
    
    public void setExpectedLength(final COSBase value) {
        this.expectedLength = value;
    }
}
