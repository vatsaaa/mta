// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.io;

import java.io.IOException;

public interface RandomAccess extends RandomAccessRead
{
    void write(final int p0) throws IOException;
    
    void write(final byte[] p0, final int p1, final int p2) throws IOException;
}
