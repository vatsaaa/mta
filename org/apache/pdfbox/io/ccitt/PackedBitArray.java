// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.io.ccitt;

public class PackedBitArray
{
    private int bitCount;
    private byte[] data;
    
    public PackedBitArray(final int bitCount) {
        this.bitCount = bitCount;
        final int byteCount = (bitCount + 7) / 8;
        this.data = new byte[byteCount];
    }
    
    private int byteOffset(final int offset) {
        return offset / 8;
    }
    
    private int bitOffset(final int offset) {
        return offset % 8;
    }
    
    public void set(final int offset) {
        final int byteOffset = this.byteOffset(offset);
        final byte[] data = this.data;
        final int n = byteOffset;
        data[n] |= (byte)(1 << this.bitOffset(offset));
    }
    
    public void clear(final int offset) {
        final int byteOffset = this.byteOffset(offset);
        final int bitOffset = this.bitOffset(offset);
        final byte[] data = this.data;
        final int n = byteOffset;
        data[n] &= (byte)~(1 << bitOffset);
    }
    
    public void setBits(final int offset, final int length, final int bit) {
        if (bit == 0) {
            this.clearBits(offset, length);
        }
        else {
            this.setBits(offset, length);
        }
    }
    
    public void setBits(final int offset, final int length) {
        if (length == 0) {
            return;
        }
        final int startBitOffset = this.bitOffset(offset);
        final int firstByte = this.byteOffset(offset);
        final int lastBitOffset = offset + length;
        if (lastBitOffset > this.getBitCount()) {
            throw new IndexOutOfBoundsException("offset + length > bit count");
        }
        final int lastByte = this.byteOffset(lastBitOffset);
        final int endBitOffset = this.bitOffset(lastBitOffset);
        if (firstByte == lastByte) {
            final int mask = (1 << endBitOffset) - (1 << startBitOffset);
            final byte[] data = this.data;
            final int n = firstByte;
            data[n] |= (byte)mask;
        }
        else {
            final byte[] data2 = this.data;
            final int n2 = firstByte;
            data2[n2] |= (byte)(255 << startBitOffset);
            for (int i = firstByte + 1; i < lastByte; ++i) {
                this.data[i] = -1;
            }
            if (endBitOffset > 0) {
                final byte[] data3 = this.data;
                final int n3 = lastByte;
                data3[n3] |= (byte)(255 >> 8 - endBitOffset);
            }
        }
    }
    
    public void clearBits(final int offset, final int length) {
        if (length == 0) {
            return;
        }
        final int startBitOffset = offset % 8;
        final int firstByte = this.byteOffset(offset);
        final int lastBitOffset = offset + length;
        final int lastByte = this.byteOffset(lastBitOffset);
        final int endBitOffset = lastBitOffset % 8;
        if (firstByte == lastByte) {
            final int mask = (1 << endBitOffset) - (1 << startBitOffset);
            final byte[] data = this.data;
            final int n = firstByte;
            data[n] &= (byte)~mask;
        }
        else {
            final byte[] data2 = this.data;
            final int n2 = firstByte;
            data2[n2] &= (byte)~(255 << startBitOffset);
            for (int i = firstByte + 1; i < lastByte; ++i) {
                this.data[i] = 0;
            }
            if (endBitOffset > 0) {
                final byte[] data3 = this.data;
                final int n3 = lastByte;
                data3[n3] &= (byte)~(255 >> 8 - endBitOffset);
            }
        }
    }
    
    public void clear() {
        this.clearBits(0, this.getBitCount());
    }
    
    public int getBitCount() {
        return this.bitCount;
    }
    
    public int getByteCount() {
        return this.data.length;
    }
    
    public byte[] getData() {
        return this.data;
    }
    
    @Override
    public String toString() {
        return toBitString(this.data).substring(0, this.bitCount);
    }
    
    public static String toBitString(final byte data) {
        final byte[] buf = { data };
        return toBitString(buf);
    }
    
    public static String toBitString(final byte[] data) {
        return toBitString(data, 0, data.length);
    }
    
    public static String toBitString(final byte[] data, final int start, final int len) {
        final StringBuffer sb = new StringBuffer();
        for (int x = start, end = start + len; x < end; ++x) {
            for (int i = 0; i < 8; ++i) {
                final int mask = 1 << i;
                final int value = data[x] & mask;
                sb.append((value != 0) ? '1' : '0');
            }
        }
        return sb.toString();
    }
}
