// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.io.ccitt;

import java.io.IOException;
import java.io.InputStream;

public class CCITTFaxG31DDecodeInputStream extends InputStream implements CCITTFaxConstants
{
    private static final int CODE_WORD = 0;
    private static final int SIGNAL_EOD = -1;
    private static final int SIGNAL_EOL = -2;
    private InputStream source;
    private int columns;
    private int rows;
    private int bits;
    private int bitPos;
    private PackedBitArray decodedLine;
    private int decodedWritePos;
    private int decodedReadPos;
    private int y;
    private int accumulatedRunLength;
    private static final NonLeafLookupTreeNode WHITE_LOOKUP_TREE_ROOT;
    private static final NonLeafLookupTreeNode BLACK_LOOKUP_TREE_ROOT;
    private static final int[] BIT_POS_MASKS;
    private static final short EOL_STARTER = 2816;
    
    public CCITTFaxG31DDecodeInputStream(final InputStream source, final int columns, final int rows) {
        this.bitPos = 8;
        this.y = -1;
        this.source = source;
        this.columns = columns;
        this.rows = rows;
        this.decodedLine = new PackedBitArray(columns);
        this.decodedReadPos = this.decodedLine.getByteCount();
    }
    
    public CCITTFaxG31DDecodeInputStream(final InputStream source, final int columns) {
        this(source, columns, 0);
    }
    
    @Override
    public boolean markSupported() {
        return false;
    }
    
    @Override
    public int read() throws IOException {
        if (this.decodedReadPos >= this.decodedLine.getByteCount()) {
            final boolean hasLine = this.decodeLine();
            if (!hasLine) {
                return -1;
            }
        }
        final byte data = this.decodedLine.getData()[this.decodedReadPos++];
        return data & 0xFF;
    }
    
    private boolean decodeLine() throws IOException {
        if (this.bits < 0) {
            return false;
        }
        ++this.y;
        int x = 0;
        if (this.rows > 0 && this.y >= this.rows) {
            return false;
        }
        this.decodedLine.clear();
        this.decodedWritePos = 0;
        int expectRTC = 6;
        boolean white = true;
        while (x < this.columns || this.accumulatedRunLength > 0) {
            final LookupTreeNode root = white ? CCITTFaxG31DDecodeInputStream.WHITE_LOOKUP_TREE_ROOT : CCITTFaxG31DDecodeInputStream.BLACK_LOOKUP_TREE_ROOT;
            final CodeWord code = root.getNextCodeWord(this);
            if (code == null) {
                if (x > 0) {
                    this.decodedReadPos = 0;
                    return true;
                }
                return false;
            }
            else if (code.getType() == -2) {
                if (--expectRTC == 0) {
                    return false;
                }
                if (x == 0) {
                    continue;
                }
                continue;
            }
            else {
                expectRTC = -1;
                x += code.execute(this);
                if (this.accumulatedRunLength != 0) {
                    continue;
                }
                white = !white;
            }
        }
        this.decodedReadPos = 0;
        return true;
    }
    
    private void writeRun(final int bit, final int length) {
        this.accumulatedRunLength += length;
        if (bit != 0) {
            this.decodedLine.setBits(this.decodedWritePos, this.accumulatedRunLength);
        }
        this.decodedWritePos += this.accumulatedRunLength;
        this.accumulatedRunLength = 0;
    }
    
    private void writeNonTerminating(final int length) {
        this.accumulatedRunLength += length;
    }
    
    private int readBit() throws IOException {
        if (this.bitPos >= 8) {
            this.readByte();
            if (this.bits < 0) {
                return -1;
            }
        }
        final int bit = ((this.bits & CCITTFaxG31DDecodeInputStream.BIT_POS_MASKS[this.bitPos++]) != 0x0) ? 1 : 0;
        return bit;
    }
    
    private void readByte() throws IOException {
        this.bits = this.source.read();
        this.bitPos = 0;
    }
    
    private static void buildLookupTree() {
        buildUpTerminating(CCITTFaxG31DDecodeInputStream.WHITE_TERMINATING, CCITTFaxG31DDecodeInputStream.WHITE_LOOKUP_TREE_ROOT, true);
        buildUpTerminating(CCITTFaxG31DDecodeInputStream.BLACK_TERMINATING, CCITTFaxG31DDecodeInputStream.BLACK_LOOKUP_TREE_ROOT, false);
        buildUpMakeUp(CCITTFaxG31DDecodeInputStream.WHITE_MAKE_UP, CCITTFaxG31DDecodeInputStream.WHITE_LOOKUP_TREE_ROOT);
        buildUpMakeUp(CCITTFaxG31DDecodeInputStream.BLACK_MAKE_UP, CCITTFaxG31DDecodeInputStream.BLACK_LOOKUP_TREE_ROOT);
        buildUpMakeUpLong(CCITTFaxG31DDecodeInputStream.LONG_MAKE_UP, CCITTFaxG31DDecodeInputStream.WHITE_LOOKUP_TREE_ROOT);
        buildUpMakeUpLong(CCITTFaxG31DDecodeInputStream.LONG_MAKE_UP, CCITTFaxG31DDecodeInputStream.BLACK_LOOKUP_TREE_ROOT);
        final LookupTreeNode eolNode = new EndOfLineTreeNode();
        addLookupTreeNode((short)2816, CCITTFaxG31DDecodeInputStream.WHITE_LOOKUP_TREE_ROOT, eolNode);
        addLookupTreeNode((short)2816, CCITTFaxG31DDecodeInputStream.BLACK_LOOKUP_TREE_ROOT, eolNode);
    }
    
    private static void buildUpTerminating(final short[] codes, final NonLeafLookupTreeNode root, final boolean white) {
        for (int len = 0, c = codes.length; len < c; ++len) {
            final LookupTreeNode leaf = new RunLengthTreeNode(white ? 0 : 1, len);
            addLookupTreeNode(codes[len], root, leaf);
        }
    }
    
    private static void buildUpMakeUp(final short[] codes, final NonLeafLookupTreeNode root) {
        for (int len = 0, c = codes.length; len < c; ++len) {
            final LookupTreeNode leaf = new MakeUpTreeNode((len + 1) * 64);
            addLookupTreeNode(codes[len], root, leaf);
        }
    }
    
    private static void buildUpMakeUpLong(final short[] codes, final NonLeafLookupTreeNode root) {
        for (int len = 0, c = codes.length; len < c; ++len) {
            final LookupTreeNode leaf = new MakeUpTreeNode((len + 28) * 64);
            addLookupTreeNode(codes[len], root, leaf);
        }
    }
    
    private static void addLookupTreeNode(final short code, final NonLeafLookupTreeNode root, final LookupTreeNode leaf) {
        final int codeLength = code >> 8;
        final int pattern = code & 0xFF;
        NonLeafLookupTreeNode node = root;
        for (int p = codeLength - 1; p > 0; --p) {
            final int bit = pattern >> p & 0x1;
            LookupTreeNode child = node.get(bit);
            if (child == null) {
                child = new NonLeafLookupTreeNode();
                node.set(bit, child);
            }
            if (!(child instanceof NonLeafLookupTreeNode)) {
                throw new IllegalStateException("NonLeafLookupTreeNode expected, was " + child.getClass().getName());
            }
            node = (NonLeafLookupTreeNode)child;
        }
        final int bit2 = pattern & 0x1;
        if (node.get(bit2) != null) {
            throw new IllegalStateException("Two codes conflicting in lookup tree");
        }
        node.set(bit2, leaf);
    }
    
    static {
        WHITE_LOOKUP_TREE_ROOT = new NonLeafLookupTreeNode();
        BLACK_LOOKUP_TREE_ROOT = new NonLeafLookupTreeNode();
        buildLookupTree();
        BIT_POS_MASKS = new int[] { 128, 64, 32, 16, 8, 4, 2, 1 };
    }
    
    private abstract static class LookupTreeNode
    {
        public abstract CodeWord getNextCodeWord(final CCITTFaxG31DDecodeInputStream p0) throws IOException;
    }
    
    private static class NonLeafLookupTreeNode extends LookupTreeNode
    {
        private LookupTreeNode zero;
        private LookupTreeNode one;
        
        public void set(final int bit, final LookupTreeNode node) {
            if (bit == 0) {
                this.zero = node;
            }
            else {
                this.one = node;
            }
        }
        
        public LookupTreeNode get(final int bit) {
            return (bit == 0) ? this.zero : this.one;
        }
        
        @Override
        public CodeWord getNextCodeWord(final CCITTFaxG31DDecodeInputStream decoder) throws IOException {
            final int bit = decoder.readBit();
            if (bit < 0) {
                return null;
            }
            final LookupTreeNode node = this.get(bit);
            if (node != null) {
                return node.getNextCodeWord(decoder);
            }
            throw new IOException("Invalid code word encountered");
        }
    }
    
    private static class RunLengthTreeNode extends LookupTreeNode implements CodeWord
    {
        private final int bit;
        private final int length;
        
        public RunLengthTreeNode(final int bit, final int length) {
            this.bit = bit;
            this.length = length;
        }
        
        @Override
        public CodeWord getNextCodeWord(final CCITTFaxG31DDecodeInputStream decoder) throws IOException {
            return this;
        }
        
        public int execute(final CCITTFaxG31DDecodeInputStream decoder) {
            decoder.writeRun(this.bit, this.length);
            return this.length;
        }
        
        public int getType() {
            return 0;
        }
        
        @Override
        public String toString() {
            return "Run Length for " + this.length + " bits of " + ((this.bit == 0) ? "white" : "black");
        }
    }
    
    private static class MakeUpTreeNode extends LookupTreeNode implements CodeWord
    {
        private final int length;
        
        public MakeUpTreeNode(final int length) {
            this.length = length;
        }
        
        @Override
        public CodeWord getNextCodeWord(final CCITTFaxG31DDecodeInputStream decoder) throws IOException {
            return this;
        }
        
        public int execute(final CCITTFaxG31DDecodeInputStream decoder) throws IOException {
            decoder.writeNonTerminating(this.length);
            return this.length;
        }
        
        public int getType() {
            return 0;
        }
        
        @Override
        public String toString() {
            return "Make up code for length " + this.length;
        }
    }
    
    private static class EndOfLineTreeNode extends LookupTreeNode implements CodeWord
    {
        @Override
        public CodeWord getNextCodeWord(final CCITTFaxG31DDecodeInputStream decoder) throws IOException {
            int bit;
            do {
                bit = decoder.readBit();
            } while (bit == 0);
            if (bit < 0) {
                return null;
            }
            return this;
        }
        
        public int execute(final CCITTFaxG31DDecodeInputStream decoder) throws IOException {
            return 0;
        }
        
        public int getType() {
            return -2;
        }
        
        @Override
        public String toString() {
            return "EOL";
        }
    }
    
    private interface CodeWord
    {
        int getType();
        
        int execute(final CCITTFaxG31DDecodeInputStream p0) throws IOException;
    }
}
