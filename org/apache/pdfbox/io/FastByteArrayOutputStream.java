// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.io;

import java.io.ByteArrayOutputStream;

public class FastByteArrayOutputStream extends ByteArrayOutputStream
{
    public FastByteArrayOutputStream(final int size) {
        super(size);
    }
    
    public byte[] getByteArray() {
        return this.buf;
    }
}
