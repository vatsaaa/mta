// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.io;

import java.io.IOException;
import java.io.InputStream;

public class RandomAccessFileInputStream extends InputStream
{
    private RandomAccess file;
    private long currentPosition;
    private long endPosition;
    
    public RandomAccessFileInputStream(final RandomAccess raFile, final long startPosition, final long length) {
        this.file = raFile;
        this.currentPosition = startPosition;
        this.endPosition = this.currentPosition + length;
    }
    
    @Override
    public int available() {
        return (int)(this.endPosition - this.currentPosition);
    }
    
    @Override
    public void close() {
    }
    
    @Override
    public int read() throws IOException {
        synchronized (this.file) {
            int retval = -1;
            if (this.currentPosition < this.endPosition) {
                this.file.seek(this.currentPosition);
                ++this.currentPosition;
                retval = this.file.read();
            }
            return retval;
        }
    }
    
    @Override
    public int read(final byte[] b, final int offset, int length) throws IOException {
        if (length > this.available()) {
            length = this.available();
        }
        int amountRead = -1;
        if (this.available() > 0) {
            synchronized (this.file) {
                this.file.seek(this.currentPosition);
                amountRead = this.file.read(b, offset, length);
            }
        }
        if (amountRead > 0) {
            this.currentPosition += amountRead;
        }
        return amountRead;
    }
    
    @Override
    public long skip(final long amountToSkip) {
        final long amountSkipped = Math.min(amountToSkip, this.available());
        this.currentPosition += amountSkipped;
        return amountSkipped;
    }
}
