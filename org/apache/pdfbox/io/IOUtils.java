// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.io;

import java.io.Writer;
import java.io.Reader;
import java.io.IOException;
import java.io.OutputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;

public class IOUtils
{
    private IOUtils() {
    }
    
    public static byte[] toByteArray(final InputStream in) throws IOException {
        final ByteArrayOutputStream baout = new ByteArrayOutputStream();
        copy(in, baout);
        return baout.toByteArray();
    }
    
    public static long copy(final InputStream input, final OutputStream output) throws IOException {
        final byte[] buffer = new byte[4096];
        long count = 0L;
        int n = 0;
        while (-1 != (n = input.read(buffer))) {
            output.write(buffer, 0, n);
            count += n;
        }
        return count;
    }
    
    public static long populateBuffer(final InputStream in, final byte[] buffer) throws IOException {
        int remaining;
        int bytesRead;
        for (remaining = buffer.length; remaining > 0; remaining -= bytesRead) {
            final int bufferWritePos = buffer.length - remaining;
            bytesRead = in.read(buffer, bufferWritePos, remaining);
            if (bytesRead < 0) {
                break;
            }
        }
        return buffer.length - remaining;
    }
    
    public static void closeQuietly(final InputStream input) {
        try {
            if (input != null) {
                input.close();
            }
        }
        catch (IOException ex) {}
    }
    
    public static void closeQuietly(final Reader input) {
        try {
            if (input != null) {
                input.close();
            }
        }
        catch (IOException ex) {}
    }
    
    public static void closeQuietly(final Writer output) {
        try {
            if (output != null) {
                output.close();
            }
        }
        catch (IOException ex) {}
    }
    
    public static void closeQuietly(final OutputStream output) {
        try {
            if (output != null) {
                output.close();
            }
        }
        catch (IOException ex) {}
    }
}
