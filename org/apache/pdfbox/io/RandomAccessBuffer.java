// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.io;

import java.io.IOException;

public class RandomAccessBuffer implements RandomAccess
{
    private byte[] buffer;
    private long pointer;
    private long size;
    
    public RandomAccessBuffer() {
        this.buffer = new byte[16384];
        this.pointer = 0L;
        this.size = 0L;
    }
    
    public void close() throws IOException {
        this.buffer = null;
        this.pointer = 0L;
        this.size = 0L;
    }
    
    public void seek(final long position) throws IOException {
        this.pointer = position;
    }
    
    public int read() throws IOException {
        if (this.pointer >= this.size) {
            return -1;
        }
        return this.buffer[(int)(this.pointer++)] & 0xFF;
    }
    
    public int read(final byte[] b, final int offset, final int length) throws IOException {
        if (this.pointer >= this.size) {
            return 0;
        }
        final int maxLength = (int)Math.min(length, this.size - this.pointer);
        System.arraycopy(this.buffer, (int)this.pointer, b, offset, maxLength);
        this.pointer += maxLength;
        return maxLength;
    }
    
    public long length() throws IOException {
        return this.size;
    }
    
    public void write(final int b) throws IOException {
        if (this.pointer >= this.buffer.length) {
            if (this.pointer >= 2147483647L) {
                throw new IOException("RandomAccessBuffer overflow");
            }
            this.buffer = this.expandBuffer(this.buffer, (int)Math.min(2L * this.buffer.length, 2147483647L));
        }
        this.buffer[(int)(this.pointer++)] = (byte)b;
        if (this.pointer > this.size) {
            this.size = this.pointer;
        }
    }
    
    public void write(final byte[] b, final int offset, final int length) throws IOException {
        long newSize = this.pointer + length;
        if (newSize >= this.buffer.length) {
            if (newSize > 2147483647L) {
                throw new IOException("RandomAccessBuffer overflow");
            }
            newSize = Math.min(Math.max(2L * this.buffer.length, newSize), 2147483647L);
            this.buffer = this.expandBuffer(this.buffer, (int)newSize);
        }
        System.arraycopy(b, offset, this.buffer, (int)this.pointer, length);
        this.pointer += length;
        if (this.pointer > this.size) {
            this.size = this.pointer;
        }
    }
    
    private byte[] expandBuffer(final byte[] buffer, final int newSize) {
        final byte[] expandedBuffer = new byte[newSize];
        System.arraycopy(buffer, 0, expandedBuffer, 0, buffer.length);
        return expandedBuffer;
    }
}
