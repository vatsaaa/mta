// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.io;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

public class ByteArrayPushBackInputStream extends PushBackInputStream
{
    private byte[] data;
    private int datapos;
    private int datalen;
    private int save;
    private static final InputStream DUMMY;
    
    public ByteArrayPushBackInputStream(final byte[] input) throws IOException {
        super(ByteArrayPushBackInputStream.DUMMY, 1);
        this.data = input;
        this.datapos = 0;
        this.save = this.datapos;
        this.datalen = ((input != null) ? input.length : 0);
    }
    
    @Override
    public int peek() {
        try {
            return this.data[this.datapos] + 256 & 0xFF;
        }
        catch (ArrayIndexOutOfBoundsException ex) {
            return -1;
        }
    }
    
    @Override
    public boolean isEOF() {
        return this.datapos >= this.datalen;
    }
    
    @Override
    public void mark(final int readlimit) {
        this.save = this.datapos;
    }
    
    @Override
    public boolean markSupported() {
        return true;
    }
    
    @Override
    public void reset() {
        this.datapos = this.save;
    }
    
    @Override
    public int available() {
        final int av = this.datalen - this.datapos;
        return (av > 0) ? av : 0;
    }
    
    public int size() {
        return this.datalen;
    }
    
    @Override
    public void unread(final int by) throws IOException {
        if (this.datapos == 0) {
            throw new IOException("ByteArrayParserInputStream.unread(int): cannot unread 1 byte at buffer position " + this.datapos);
        }
        --this.datapos;
        this.data[this.datapos] = (byte)by;
    }
    
    @Override
    public void unread(final byte[] buffer, int off, int len) throws IOException {
        if (len <= 0 || off >= buffer.length) {
            return;
        }
        if (off < 0) {
            off = 0;
        }
        if (len > buffer.length) {
            len = buffer.length;
        }
        this.localUnread(buffer, off, len);
    }
    
    @Override
    public void unread(final byte[] buffer) throws IOException {
        this.localUnread(buffer, 0, buffer.length);
    }
    
    private void localUnread(final byte[] buffer, final int off, final int len) throws IOException {
        if (this.datapos < len) {
            throw new IOException("ByteArrayParserInputStream.unread(int): cannot unread " + len + " bytes at buffer position " + this.datapos);
        }
        this.datapos -= len;
        System.arraycopy(buffer, off, this.data, this.datapos, len);
    }
    
    @Override
    public int read() {
        try {
            return this.data[this.datapos++] + 256 & 0xFF;
        }
        catch (ArrayIndexOutOfBoundsException ex) {
            this.datapos = this.datalen;
            return -1;
        }
    }
    
    @Override
    public int read(final byte[] buffer) {
        return this.localRead(buffer, 0, buffer.length);
    }
    
    @Override
    public int read(final byte[] buffer, int off, int len) {
        if (len <= 0 || off >= buffer.length) {
            return 0;
        }
        if (off < 0) {
            off = 0;
        }
        if (len > buffer.length) {
            len = buffer.length;
        }
        return this.localRead(buffer, off, len);
    }
    
    public int localRead(final byte[] buffer, final int off, int len) {
        if (len == 0) {
            return 0;
        }
        if (this.datapos >= this.datalen) {
            return -1;
        }
        int newpos = this.datapos + len;
        if (newpos > this.datalen) {
            newpos = this.datalen;
            len = newpos - this.datapos;
        }
        System.arraycopy(this.data, this.datapos, buffer, off, len);
        this.datapos = newpos;
        return len;
    }
    
    @Override
    public long skip(long num) {
        if (num <= 0L) {
            return 0L;
        }
        final long newpos = this.datapos + num;
        if (newpos >= this.datalen) {
            num = this.datalen - this.datapos;
            this.datapos = this.datalen;
        }
        else {
            this.datapos = (int)newpos;
        }
        return num;
    }
    
    public int seek(int newpos) {
        if (newpos < 0) {
            newpos = 0;
        }
        else if (newpos > this.datalen) {
            newpos = this.datalen;
        }
        final int oldpos = this.pos;
        this.pos = newpos;
        return oldpos;
    }
    
    static {
        DUMMY = new ByteArrayInputStream("".getBytes());
    }
}
