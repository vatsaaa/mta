// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.io;

import java.io.IOException;

public interface SequentialRead
{
    void close() throws IOException;
    
    int read() throws IOException;
    
    int read(final byte[] p0, final int p1, final int p2) throws IOException;
}
