// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.io;

import java.io.IOException;
import java.io.InputStream;
import java.io.FilterInputStream;

public class ASCII85InputStream extends FilterInputStream
{
    private int index;
    private int n;
    private boolean eof;
    private byte[] ascii;
    private byte[] b;
    
    public ASCII85InputStream(final InputStream is) {
        super(is);
        this.index = 0;
        this.n = 0;
        this.eof = false;
        this.ascii = new byte[5];
        this.b = new byte[4];
    }
    
    @Override
    public final int read() throws IOException {
        if (this.index >= this.n) {
            if (this.eof) {
                return -1;
            }
            this.index = 0;
            byte z;
            do {
                final int zz = (byte)this.in.read();
                if (zz == -1) {
                    this.eof = true;
                    return -1;
                }
                z = (byte)zz;
            } while (z == 10 || z == 13 || z == 32);
            if (z == 126 || z == 120) {
                this.eof = true;
                final byte[] array = null;
                this.b = array;
                this.ascii = array;
                this.n = 0;
                return -1;
            }
            if (z == 122) {
                final byte[] b = this.b;
                final int n = 0;
                final byte[] b2 = this.b;
                final int n2 = 1;
                final byte[] b3 = this.b;
                final int n3 = 2;
                final byte[] b4 = this.b;
                final int n4 = 3;
                final byte b5 = 0;
                b3[n3] = (b4[n4] = b5);
                b[n] = (b2[n2] = b5);
                this.n = 4;
            }
            else {
                this.ascii[0] = z;
                int k;
                for (k = 1; k < 5; ++k) {
                    do {
                        final int zz = (byte)this.in.read();
                        if (zz == -1) {
                            this.eof = true;
                            return -1;
                        }
                        z = (byte)zz;
                    } while (z == 10 || z == 13 || z == 32);
                    if ((this.ascii[k] = z) == 126) {
                        break;
                    }
                    if (z == 120) {
                        break;
                    }
                }
                this.n = k - 1;
                if (this.n == 0) {
                    this.eof = true;
                    this.ascii = null;
                    this.b = null;
                    return -1;
                }
                if (k < 5) {
                    ++k;
                    while (k < 5) {
                        this.ascii[k] = 33;
                        ++k;
                    }
                    this.eof = true;
                }
                long t = 0L;
                for (k = 0; k < 5; ++k) {
                    z = (byte)(this.ascii[k] - 33);
                    if (z < 0 || z > 93) {
                        this.n = 0;
                        this.eof = true;
                        this.ascii = null;
                        this.b = null;
                        throw new IOException("Invalid data in Ascii85 stream");
                    }
                    t = t * 85L + z;
                }
                for (k = 3; k >= 0; --k) {
                    this.b[k] = (byte)(t & 0xFFL);
                    t >>>= 8;
                }
            }
        }
        return this.b[this.index++] & 0xFF;
    }
    
    @Override
    public final int read(final byte[] data, final int offset, final int len) throws IOException {
        if (this.eof && this.index >= this.n) {
            return -1;
        }
        for (int i = 0; i < len; ++i) {
            if (this.index < this.n) {
                data[i + offset] = this.b[this.index++];
            }
            else {
                final int t = this.read();
                if (t == -1) {
                    return i;
                }
                data[i + offset] = (byte)t;
            }
        }
        return len;
    }
    
    @Override
    public void close() throws IOException {
        this.ascii = null;
        this.eof = true;
        this.b = null;
        super.close();
    }
    
    @Override
    public boolean markSupported() {
        return false;
    }
    
    @Override
    public long skip(final long nValue) {
        return 0L;
    }
    
    @Override
    public int available() {
        return 0;
    }
    
    @Override
    public void mark(final int readlimit) {
    }
    
    @Override
    public void reset() throws IOException {
        throw new IOException("Reset is not supported");
    }
}
