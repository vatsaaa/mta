// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.io;

import java.io.IOException;
import java.io.OutputStream;

public class NBitOutputStream
{
    private int bitsInChunk;
    private OutputStream out;
    private int currentByte;
    private int positionInCurrentByte;
    
    public NBitOutputStream(final OutputStream os) {
        this.out = os;
        this.currentByte = 0;
        this.positionInCurrentByte = 7;
    }
    
    public void write(final long chunk) throws IOException {
        for (int i = this.bitsInChunk - 1; i >= 0; --i) {
            long bitToWrite = chunk >> i & 0x1L;
            bitToWrite <<= this.positionInCurrentByte;
            this.currentByte = (int)((long)this.currentByte | bitToWrite);
            --this.positionInCurrentByte;
            if (this.positionInCurrentByte < 0) {
                this.out.write(this.currentByte);
                this.currentByte = 0;
                this.positionInCurrentByte = 7;
            }
        }
    }
    
    public void close() throws IOException {
        if (this.positionInCurrentByte < 7) {
            this.out.write(this.currentByte);
        }
    }
    
    public int getBitsInChunk() {
        return this.bitsInChunk;
    }
    
    public void setBitsInChunk(final int bitsInChunkValue) {
        this.bitsInChunk = bitsInChunkValue;
    }
}
