// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.io;

import java.io.IOException;
import java.io.FileNotFoundException;
import java.util.Map;
import java.io.File;
import java.io.RandomAccessFile;
import java.util.LinkedHashMap;
import java.io.InputStream;

public class RandomAccessBufferedFileInputStream extends InputStream implements RandomAccessRead
{
    private int pageSizeShift;
    private int pageSize;
    private long pageOffsetMask;
    private int maxCachedPages;
    private byte[] lastRemovedCachePage;
    private final LinkedHashMap<Long, byte[]> pageCache;
    private long curPageOffset;
    private byte[] curPage;
    private int offsetWithinPage;
    private final RandomAccessFile raFile;
    private final long fileLength;
    private long fileOffset;
    
    public RandomAccessBufferedFileInputStream(final File _file) throws FileNotFoundException, IOException {
        this.pageSizeShift = 12;
        this.pageSize = 1 << this.pageSizeShift;
        this.pageOffsetMask = -1L << this.pageSizeShift;
        this.maxCachedPages = 1000;
        this.lastRemovedCachePage = null;
        this.pageCache = new LinkedHashMap<Long, byte[]>(this.maxCachedPages, 0.75f, true) {
            private static final long serialVersionUID = -6302488539257741101L;
            
            @Override
            protected boolean removeEldestEntry(final Map.Entry<Long, byte[]> _eldest) {
                final boolean doRemove = this.size() > RandomAccessBufferedFileInputStream.this.maxCachedPages;
                if (doRemove) {
                    RandomAccessBufferedFileInputStream.this.lastRemovedCachePage = _eldest.getValue();
                }
                return doRemove;
            }
        };
        this.curPageOffset = -1L;
        this.curPage = new byte[this.pageSize];
        this.offsetWithinPage = 0;
        this.fileOffset = 0L;
        this.raFile = new RandomAccessFile(_file, "r");
        this.fileLength = _file.length();
        this.seek(0L);
    }
    
    public long getFilePointer() {
        return this.fileOffset;
    }
    
    public void seek(final long newOffset) throws IOException {
        final long newPageOffset = newOffset & this.pageOffsetMask;
        if (newPageOffset != this.curPageOffset) {
            byte[] newPage = this.pageCache.get(newPageOffset);
            if (newPage == null) {
                this.raFile.seek(newPageOffset);
                newPage = this.readPage();
                this.pageCache.put(newPageOffset, newPage);
            }
            this.curPageOffset = newPageOffset;
            this.curPage = newPage;
        }
        this.offsetWithinPage = (int)(newOffset - this.curPageOffset);
        this.fileOffset = newOffset;
    }
    
    private final byte[] readPage() throws IOException {
        byte[] page;
        if (this.lastRemovedCachePage != null) {
            page = this.lastRemovedCachePage;
            this.lastRemovedCachePage = null;
        }
        else {
            page = new byte[this.pageSize];
        }
        int curBytesRead;
        for (int readBytes = 0; readBytes < this.pageSize; readBytes += curBytesRead) {
            curBytesRead = this.raFile.read(page, readBytes, this.pageSize - readBytes);
            if (curBytesRead < 0) {
                break;
            }
        }
        return page;
    }
    
    @Override
    public int read() throws IOException {
        if (this.fileOffset >= this.fileLength) {
            return -1;
        }
        if (this.offsetWithinPage == this.pageSize) {
            this.seek(this.fileOffset);
        }
        ++this.fileOffset;
        return this.curPage[this.offsetWithinPage++] & 0xFF;
    }
    
    @Override
    public int read(final byte[] b, final int off, final int len) throws IOException {
        if (this.fileOffset >= this.fileLength) {
            return -1;
        }
        if (this.offsetWithinPage == this.pageSize) {
            this.seek(this.fileOffset);
        }
        int commonLen = Math.min(this.pageSize - this.offsetWithinPage, len);
        if (this.fileLength - this.fileOffset < this.pageSize) {
            commonLen = Math.min(commonLen, (int)(this.fileLength - this.fileOffset));
        }
        System.arraycopy(this.curPage, this.offsetWithinPage, b, off, commonLen);
        this.offsetWithinPage += commonLen;
        this.fileOffset += commonLen;
        return commonLen;
    }
    
    @Override
    public int available() throws IOException {
        return (int)Math.min(this.fileLength - this.fileOffset, 2147483647L);
    }
    
    @Override
    public long skip(final long n) throws IOException {
        long toSkip = n;
        if (this.fileLength - this.fileOffset < toSkip) {
            toSkip = this.fileLength - this.fileOffset;
        }
        if (toSkip < this.pageSize && this.offsetWithinPage + toSkip <= this.pageSize) {
            this.offsetWithinPage += (int)toSkip;
            this.fileOffset += toSkip;
        }
        else {
            this.seek(this.fileOffset + toSkip);
        }
        return toSkip;
    }
    
    public long length() throws IOException {
        return this.fileLength;
    }
    
    @Override
    public void close() throws IOException {
        this.raFile.close();
        this.pageCache.clear();
    }
}
