// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.io;

import java.io.IOException;
import java.io.FileNotFoundException;
import java.io.File;

public class RandomAccessFile implements RandomAccess
{
    private java.io.RandomAccessFile ras;
    
    public RandomAccessFile(final File file, final String mode) throws FileNotFoundException {
        this.ras = new java.io.RandomAccessFile(file, mode);
    }
    
    public void close() throws IOException {
        this.ras.close();
    }
    
    public void seek(final long position) throws IOException {
        this.ras.seek(position);
    }
    
    public int read() throws IOException {
        return this.ras.read();
    }
    
    public int read(final byte[] b, final int offset, final int length) throws IOException {
        return this.ras.read(b, offset, length);
    }
    
    public long length() throws IOException {
        return this.ras.length();
    }
    
    public void write(final byte[] b, final int offset, final int length) throws IOException {
        this.ras.write(b, offset, length);
    }
    
    public void write(final int b) throws IOException {
        this.ras.write(b);
    }
}
