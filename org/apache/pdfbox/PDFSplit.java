// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox;

import org.apache.pdfbox.exceptions.COSVisitorException;
import java.io.IOException;
import java.io.OutputStream;
import org.apache.pdfbox.pdfwriter.COSWriter;
import java.io.FileOutputStream;
import java.util.List;
import org.apache.pdfbox.exceptions.InvalidPasswordException;
import org.apache.pdfbox.io.RandomAccess;
import org.apache.pdfbox.pdmodel.PDDocument;
import java.io.File;
import org.apache.pdfbox.util.Splitter;

public class PDFSplit
{
    private static final String PASSWORD = "-password";
    private static final String SPLIT = "-split";
    private static final String START_PAGE = "-startPage";
    private static final String END_PAGE = "-endPage";
    private static final String NONSEQ = "-nonSeq";
    
    private PDFSplit() {
    }
    
    public static void main(final String[] args) throws Exception {
        final PDFSplit split = new PDFSplit();
        split.split(args);
    }
    
    private void split(final String[] args) throws Exception {
        String password = "";
        String split = null;
        String startPage = null;
        String endPage = null;
        boolean useNonSeqParser = false;
        final Splitter splitter = new Splitter();
        String pdfFile = null;
        for (int i = 0; i < args.length; ++i) {
            if (args[i].equals("-password")) {
                if (++i >= args.length) {
                    usage();
                }
                password = args[i];
            }
            else if (args[i].equals("-split")) {
                if (++i >= args.length) {
                    usage();
                }
                split = args[i];
            }
            else if (args[i].equals("-startPage")) {
                if (++i >= args.length) {
                    usage();
                }
                startPage = args[i];
            }
            else if (args[i].equals("-endPage")) {
                if (++i >= args.length) {
                    usage();
                }
                endPage = args[i];
            }
            else if (args[i].equals("-nonSeq")) {
                useNonSeqParser = true;
            }
            else if (pdfFile == null) {
                pdfFile = args[i];
            }
        }
        if (pdfFile == null) {
            usage();
        }
        else {
            PDDocument document = null;
            List<PDDocument> documents = null;
            try {
                if (useNonSeqParser) {
                    document = PDDocument.loadNonSeq(new File(pdfFile), null, password);
                }
                else {
                    document = PDDocument.load(pdfFile);
                    if (document.isEncrypted()) {
                        try {
                            document.decrypt(password);
                        }
                        catch (InvalidPasswordException e) {
                            if (args.length == 4) {
                                System.err.println("Error: The supplied password is incorrect.");
                                System.exit(2);
                            }
                            else {
                                System.err.println("Error: The document is encrypted.");
                                usage();
                            }
                        }
                    }
                }
                final int numberOfPages = document.getNumberOfPages();
                final boolean startEndPageSet = false;
                if (startPage != null) {
                    splitter.setStartPage(Integer.parseInt(startPage));
                    if (split == null) {
                        splitter.setSplitAtPage(numberOfPages);
                    }
                }
                if (endPage != null) {
                    splitter.setEndPage(Integer.parseInt(endPage));
                    if (split == null) {
                        splitter.setSplitAtPage(Integer.parseInt(endPage));
                    }
                }
                if (split != null) {
                    splitter.setSplitAtPage(Integer.parseInt(split));
                }
                else if (!startEndPageSet) {
                    splitter.setSplitAtPage(1);
                }
                documents = splitter.split(document);
                for (int j = 0; j < documents.size(); ++j) {
                    final PDDocument doc = documents.get(j);
                    final String fileName = pdfFile.substring(0, pdfFile.length() - 4) + "-" + j + ".pdf";
                    writeDocument(doc, fileName);
                    doc.close();
                }
            }
            finally {
                if (document != null) {
                    document.close();
                }
                for (int k = 0; documents != null && k < documents.size(); ++k) {
                    final PDDocument doc2 = documents.get(k);
                    doc2.close();
                }
            }
        }
    }
    
    private static final void writeDocument(final PDDocument doc, final String fileName) throws IOException, COSVisitorException {
        FileOutputStream output = null;
        COSWriter writer = null;
        try {
            output = new FileOutputStream(fileName);
            writer = new COSWriter(output);
            writer.write(doc);
        }
        finally {
            if (output != null) {
                output.close();
            }
            if (writer != null) {
                writer.close();
            }
        }
    }
    
    private static void usage() {
        System.err.println("Usage: java -jar pdfbox-app-x.y.z.jar PDFSplit [OPTIONS] <PDF file>\n  -password  <password>  Password to decrypt document\n  -split     <integer>   split after this many pages (default 1, if startPage and endPage are unset)\n  -startPage <integer>   start page\n  -endPage   <integer>   end page\n  -nonSeq                Enables the new non-sequential parser\n  <PDF file>             The PDF document to use\n");
        System.exit(1);
    }
}
