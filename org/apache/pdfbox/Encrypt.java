// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox;

import java.io.InputStream;
import org.apache.pdfbox.pdmodel.encryption.StandardProtectionPolicy;
import org.apache.pdfbox.pdmodel.encryption.ProtectionPolicy;
import java.security.cert.X509Certificate;
import java.io.FileInputStream;
import java.security.cert.CertificateFactory;
import org.apache.pdfbox.pdmodel.encryption.PublicKeyRecipient;
import org.apache.pdfbox.pdmodel.encryption.PublicKeyProtectionPolicy;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.encryption.AccessPermission;

public class Encrypt
{
    private Encrypt() {
    }
    
    public static void main(final String[] args) throws Exception {
        final Encrypt encrypt = new Encrypt();
        encrypt.encrypt(args);
    }
    
    private void encrypt(final String[] args) throws Exception {
        if (args.length < 1) {
            usage();
        }
        else {
            final AccessPermission ap = new AccessPermission();
            String infile = null;
            String outfile = null;
            String certFile = null;
            String userPassword = "";
            String ownerPassword = "";
            int keyLength = 40;
            PDDocument document = null;
            try {
                for (int i = 0; i < args.length; ++i) {
                    final String key = args[i];
                    if (key.equals("-O")) {
                        ownerPassword = args[++i];
                    }
                    else if (key.equals("-U")) {
                        userPassword = args[++i];
                    }
                    else if (key.equals("-canAssemble")) {
                        ap.setCanAssembleDocument(args[++i].equalsIgnoreCase("true"));
                    }
                    else if (key.equals("-canExtractContent")) {
                        ap.setCanExtractContent(args[++i].equalsIgnoreCase("true"));
                    }
                    else if (key.equals("-canExtractForAccessibility")) {
                        ap.setCanExtractForAccessibility(args[++i].equalsIgnoreCase("true"));
                    }
                    else if (key.equals("-canFillInForm")) {
                        ap.setCanFillInForm(args[++i].equalsIgnoreCase("true"));
                    }
                    else if (key.equals("-canModify")) {
                        ap.setCanModify(args[++i].equalsIgnoreCase("true"));
                    }
                    else if (key.equals("-canModifyAnnotations")) {
                        ap.setCanModifyAnnotations(args[++i].equalsIgnoreCase("true"));
                    }
                    else if (key.equals("-canPrint")) {
                        ap.setCanPrint(args[++i].equalsIgnoreCase("true"));
                    }
                    else if (key.equals("-canPrintDegraded")) {
                        ap.setCanPrintDegraded(args[++i].equalsIgnoreCase("true"));
                    }
                    else if (key.equals("-certFile")) {
                        certFile = args[++i];
                    }
                    else {
                        if (key.equals("-keyLength")) {
                            try {
                                keyLength = Integer.parseInt(args[++i]);
                                continue;
                            }
                            catch (NumberFormatException e) {
                                throw new NumberFormatException("Error: -keyLength is not an integer '" + args[i] + "'");
                            }
                        }
                        if (infile == null) {
                            infile = key;
                        }
                        else if (outfile == null) {
                            outfile = key;
                        }
                        else {
                            usage();
                        }
                    }
                }
                if (infile == null) {
                    usage();
                }
                if (outfile == null) {
                    outfile = infile;
                }
                document = PDDocument.load(infile);
                if (!document.isEncrypted()) {
                    if (certFile != null) {
                        final PublicKeyProtectionPolicy ppp = new PublicKeyProtectionPolicy();
                        final PublicKeyRecipient recip = new PublicKeyRecipient();
                        recip.setPermission(ap);
                        final CertificateFactory cf = CertificateFactory.getInstance("X.509");
                        final InputStream inStream = new FileInputStream(certFile);
                        final X509Certificate certificate = (X509Certificate)cf.generateCertificate(inStream);
                        inStream.close();
                        recip.setX509(certificate);
                        ppp.addRecipient(recip);
                        ppp.setEncryptionKeyLength(keyLength);
                        document.protect(ppp);
                    }
                    else {
                        final StandardProtectionPolicy spp = new StandardProtectionPolicy(ownerPassword, userPassword, ap);
                        spp.setEncryptionKeyLength(keyLength);
                        document.protect(spp);
                    }
                    document.save(outfile);
                }
                else {
                    System.err.println("Error: Document is already encrypted.");
                }
            }
            finally {
                if (document != null) {
                    document.close();
                }
            }
        }
    }
    
    private static void usage() {
        System.err.println("usage: java -jar pdfbox-app-x.y.z.jar Encrypt [options] <inputfile> [outputfile]");
        System.err.println("   -O <password>                            Set the owner password(ignored if cert is set)");
        System.err.println("   -U <password>                            Set the user password(ignored if cert is set)");
        System.err.println("   -certFile <path to cert>                 Path to X.509 certificate");
        System.err.println("   -canAssemble <true|false>                Set the assemble permission");
        System.err.println("   -canExtractContent <true|false>          Set the extraction permission");
        System.err.println("   -canExtractForAccessibility <true|false> Set the extraction permission");
        System.err.println("   -canFillInForm <true|false>              Set the fill in form permission");
        System.err.println("   -canModify <true|false>                  Set the modify permission");
        System.err.println("   -canModifyAnnotations <true|false>       Set the modify annots permission");
        System.err.println("   -canPrint <true|false>                   Set the print permission");
        System.err.println("   -canPrintDegraded <true|false>           Set the print degraded permission");
        System.err.println("   -keyLength <length>                      The length of the key in bits(40)");
        System.err.println("\nNote: By default all permissions are set to true!");
        System.exit(1);
    }
}
