// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox;

import java.util.Map;
import org.apache.pdfbox.pdmodel.graphics.xobject.PDXObjectForm;
import org.apache.pdfbox.pdmodel.graphics.xobject.PDXObjectImage;
import org.apache.pdfbox.pdmodel.graphics.xobject.PDXObject;
import org.apache.pdfbox.pdmodel.PDResources;
import java.util.Iterator;
import java.util.List;
import org.apache.pdfbox.pdmodel.encryption.AccessPermission;
import org.apache.pdfbox.pdmodel.PDPage;
import java.io.IOException;
import org.apache.pdfbox.pdmodel.encryption.DecryptionMaterial;
import org.apache.pdfbox.pdmodel.encryption.StandardDecryptionMaterial;
import org.apache.pdfbox.io.RandomAccess;
import org.apache.pdfbox.pdmodel.PDDocument;
import java.io.File;

public class ExtractImages
{
    private int imageCounter;
    private static final String PASSWORD = "-password";
    private static final String PREFIX = "-prefix";
    private static final String ADDKEY = "-addkey";
    private static final String NONSEQ = "-nonSeq";
    
    private ExtractImages() {
        this.imageCounter = 1;
    }
    
    public static void main(final String[] args) throws Exception {
        final ExtractImages extractor = new ExtractImages();
        extractor.extractImages(args);
    }
    
    private void extractImages(final String[] args) throws Exception {
        if (args.length < 1 || args.length > 4) {
            usage();
        }
        else {
            String pdfFile = null;
            String password = "";
            String prefix = null;
            boolean addKey = false;
            boolean useNonSeqParser = false;
            for (int i = 0; i < args.length; ++i) {
                if (args[i].equals("-password")) {
                    if (++i >= args.length) {
                        usage();
                    }
                    password = args[i];
                }
                else if (args[i].equals("-prefix")) {
                    if (++i >= args.length) {
                        usage();
                    }
                    prefix = args[i];
                }
                else if (args[i].equals("-addkey")) {
                    addKey = true;
                }
                else if (args[i].equals("-nonSeq")) {
                    useNonSeqParser = true;
                }
                else if (pdfFile == null) {
                    pdfFile = args[i];
                }
            }
            if (pdfFile == null) {
                usage();
            }
            else {
                if (prefix == null && pdfFile.length() > 4) {
                    prefix = pdfFile.substring(0, pdfFile.length() - 4);
                }
                PDDocument document = null;
                try {
                    if (useNonSeqParser) {
                        document = PDDocument.loadNonSeq(new File(pdfFile), null, password);
                    }
                    else {
                        document = PDDocument.load(pdfFile);
                        if (document.isEncrypted()) {
                            final StandardDecryptionMaterial spm = new StandardDecryptionMaterial(password);
                            document.openProtection(spm);
                        }
                    }
                    final AccessPermission ap = document.getCurrentAccessPermission();
                    if (!ap.canExtractContent()) {
                        throw new IOException("Error: You do not have permission to extract images.");
                    }
                    final List pages = document.getDocumentCatalog().getAllPages();
                    for (final PDPage page : pages) {
                        final PDResources resources = page.getResources();
                        this.processResources(resources, prefix, addKey);
                    }
                }
                finally {
                    if (document != null) {
                        document.close();
                    }
                }
            }
        }
    }
    
    private void processResources(final PDResources resources, final String prefix, final boolean addKey) throws IOException {
        if (resources == null) {
            return;
        }
        final Map<String, PDXObject> xobjects = resources.getXObjects();
        if (xobjects != null) {
            for (final String key : xobjects.keySet()) {
                final PDXObject xobject = xobjects.get(key);
                if (xobject instanceof PDXObjectImage) {
                    final PDXObjectImage image = (PDXObjectImage)xobject;
                    String name = null;
                    if (addKey) {
                        name = this.getUniqueFileName(prefix + "_" + key, image.getSuffix());
                    }
                    else {
                        name = this.getUniqueFileName(prefix, image.getSuffix());
                    }
                    System.out.println("Writing image:" + name);
                    image.write2file(name);
                }
                else {
                    if (!(xobject instanceof PDXObjectForm)) {
                        continue;
                    }
                    final PDXObjectForm xObjectForm = (PDXObjectForm)xobject;
                    final PDResources formResources = xObjectForm.getResources();
                    this.processResources(formResources, prefix, addKey);
                }
            }
        }
    }
    
    private String getUniqueFileName(final String prefix, final String suffix) {
        String uniqueName = null;
        for (File f = null; f == null || f.exists(); f = new File(uniqueName + "." + suffix), ++this.imageCounter) {
            uniqueName = prefix + "-" + this.imageCounter;
        }
        return uniqueName;
    }
    
    private static void usage() {
        System.err.println("Usage: java org.apache.pdfbox.ExtractImages [OPTIONS] <PDF file>\n  -password  <password>        Password to decrypt document\n  -prefix  <image-prefix>      Image prefix(default to pdf name)\n  -addkey                      add the internal image key to the file name\n  -nonSeq                      Enables the new non-sequential parser\n  <PDF file>                   The PDF document to use\n");
        System.exit(1);
    }
}
