// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdfviewer;

public class ArrayEntry
{
    private int index;
    private Object value;
    
    public Object getValue() {
        return this.value;
    }
    
    public void setValue(final Object val) {
        this.value = val;
    }
    
    public int getIndex() {
        return this.index;
    }
    
    public void setIndex(final int i) {
        this.index = i;
    }
}
