// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdfviewer;

import org.apache.pdfbox.cos.COSName;

public class MapEntry
{
    private Object key;
    private Object value;
    
    public Object getKey() {
        return this.key;
    }
    
    public void setKey(final Object k) {
        this.key = k;
    }
    
    public Object getValue() {
        return this.value;
    }
    
    public void setValue(final Object val) {
        this.value = val;
    }
    
    @Override
    public String toString() {
        String retval = null;
        if (this.key instanceof COSName) {
            retval = ((COSName)this.key).getName();
        }
        else {
            retval = "" + this.key;
        }
        return retval;
    }
}
