// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdfviewer;

import org.apache.commons.logging.LogFactory;
import org.apache.pdfbox.pdmodel.graphics.PDShading;
import org.apache.pdfbox.pdmodel.graphics.shading.RadialShadingPaint;
import org.apache.pdfbox.pdmodel.graphics.shading.PDShadingType3;
import org.apache.pdfbox.pdmodel.graphics.shading.AxialShadingPaint;
import org.apache.pdfbox.pdmodel.graphics.shading.PDShadingType2;
import org.apache.pdfbox.pdmodel.graphics.shading.PDShadingResources;
import org.apache.pdfbox.cos.COSName;
import java.awt.image.ImageObserver;
import java.awt.Image;
import java.awt.geom.Area;
import java.awt.geom.Point2D;
import java.awt.Stroke;
import java.awt.BasicStroke;
import java.awt.Shape;
import org.apache.pdfbox.pdmodel.common.PDMatrix;
import java.awt.geom.AffineTransform;
import org.apache.pdfbox.util.Matrix;
import org.apache.pdfbox.pdmodel.font.PDFont;
import java.awt.Paint;
import java.awt.Composite;
import org.apache.pdfbox.pdmodel.graphics.PDGraphicsState;
import java.awt.Color;
import org.apache.pdfbox.util.TextPosition;
import java.util.Map;
import org.apache.pdfbox.pdmodel.interactive.annotation.PDAppearanceDictionary;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import java.util.List;
import org.apache.pdfbox.pdmodel.PDResources;
import org.apache.pdfbox.pdmodel.interactive.annotation.PDAppearanceStream;
import org.apache.pdfbox.pdmodel.interactive.annotation.PDAnnotation;
import java.awt.RenderingHints;
import java.awt.Graphics;
import java.io.IOException;
import org.apache.pdfbox.util.ResourceLoader;
import java.awt.geom.GeneralPath;
import org.apache.pdfbox.pdmodel.PDPage;
import java.awt.Dimension;
import java.awt.Graphics2D;
import org.apache.commons.logging.Log;
import org.apache.pdfbox.util.PDFStreamEngine;

public class PageDrawer extends PDFStreamEngine
{
    private static final Log LOG;
    private Graphics2D graphics;
    protected Dimension pageSize;
    protected PDPage page;
    private GeneralPath linePath;
    
    public PageDrawer() throws IOException {
        super(ResourceLoader.loadProperties("org/apache/pdfbox/resources/PageDrawer.properties", true));
        this.linePath = new GeneralPath();
    }
    
    public void drawPage(final Graphics g, final PDPage p, final Dimension pageDimension) throws IOException {
        this.graphics = (Graphics2D)g;
        this.page = p;
        this.pageSize = pageDimension;
        this.graphics.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        this.graphics.setRenderingHint(RenderingHints.KEY_FRACTIONALMETRICS, RenderingHints.VALUE_FRACTIONALMETRICS_ON);
        if (this.page.getContents() != null) {
            final PDResources resources = this.page.findResources();
            this.processStream(this.page, resources, this.page.getContents().getStream());
        }
        final List annotations = this.page.getAnnotations();
        for (int i = 0; i < annotations.size(); ++i) {
            final PDAnnotation annot = annotations.get(i);
            final PDRectangle rect = annot.getRectangle();
            String appearanceName = annot.getAppearanceStream();
            final PDAppearanceDictionary appearDictionary = annot.getAppearance();
            if (appearDictionary != null) {
                if (appearanceName == null) {
                    appearanceName = "default";
                }
                final Map appearanceMap = appearDictionary.getNormalAppearance();
                if (appearanceMap != null) {
                    final PDAppearanceStream appearance = appearanceMap.get(appearanceName);
                    if (appearance != null) {
                        g.translate((int)rect.getLowerLeftX(), (int)(-rect.getLowerLeftY()));
                        this.processSubStream(this.page, appearance.getResources(), appearance.getStream());
                        g.translate((int)(-rect.getLowerLeftX()), (int)rect.getLowerLeftY());
                    }
                }
            }
        }
    }
    
    @Override
    protected void processTextPosition(final TextPosition text) {
        try {
            final PDGraphicsState graphicsState = this.getGraphicsState();
            Composite composite = null;
            Paint paint = null;
            switch (graphicsState.getTextState().getRenderingMode()) {
                case 0: {
                    composite = graphicsState.getNonStrokeJavaComposite();
                    paint = graphicsState.getNonStrokingColor().getJavaColor();
                    if (paint == null) {
                        paint = graphicsState.getNonStrokingColor().getPaint(this.pageSize.height);
                        break;
                    }
                    break;
                }
                case 1: {
                    composite = graphicsState.getStrokeJavaComposite();
                    paint = graphicsState.getStrokingColor().getJavaColor();
                    if (paint == null) {
                        paint = graphicsState.getStrokingColor().getPaint(this.pageSize.height);
                        break;
                    }
                    break;
                }
                case 3: {
                    final Color nsc = graphicsState.getStrokingColor().getJavaColor();
                    final float[] components = { (float)Color.black.getRed(), (float)Color.black.getGreen(), (float)Color.black.getBlue() };
                    paint = new Color(nsc.getColorSpace(), components, 0.0f);
                    composite = graphicsState.getStrokeJavaComposite();
                    break;
                }
                default: {
                    PageDrawer.LOG.debug("Unsupported RenderingMode " + this.getGraphicsState().getTextState().getRenderingMode() + " in PageDrawer.processTextPosition()." + " Using RenderingMode " + 0 + " instead");
                    composite = graphicsState.getNonStrokeJavaComposite();
                    paint = graphicsState.getNonStrokingColor().getJavaColor();
                    break;
                }
            }
            this.graphics.setComposite(composite);
            this.graphics.setPaint(paint);
            final PDFont font = text.getFont();
            final Matrix textPos = text.getTextPos().copy();
            final float x = textPos.getXPosition();
            final float y = this.pageSize.height - textPos.getYPosition();
            textPos.setValue(2, 0, 0.0f);
            textPos.setValue(2, 1, 0.0f);
            textPos.setValue(0, 1, -1.0f * textPos.getValue(0, 1));
            textPos.setValue(1, 0, -1.0f * textPos.getValue(1, 0));
            final AffineTransform at = textPos.createAffineTransform();
            final PDMatrix fontMatrix = font.getFontMatrix();
            at.scale(fontMatrix.getValue(0, 0) * 1000.0f, fontMatrix.getValue(1, 1) * 1000.0f);
            this.graphics.setClip(graphicsState.getCurrentClippingPath());
            font.drawString(text.getCharacter(), text.getCodePoints(), this.graphics, 1.0f, at, x, y);
        }
        catch (IOException io) {
            io.printStackTrace();
        }
    }
    
    public Graphics2D getGraphics() {
        return this.graphics;
    }
    
    public PDPage getPage() {
        return this.page;
    }
    
    public Dimension getPageSize() {
        return this.pageSize;
    }
    
    public double fixY(final double y) {
        return this.pageSize.getHeight() - y;
    }
    
    public GeneralPath getLinePath() {
        return this.linePath;
    }
    
    public void setLinePath(final GeneralPath newLinePath) {
        if (this.linePath == null || this.linePath.getCurrentPoint() == null) {
            this.linePath = newLinePath;
        }
        else {
            this.linePath.append(newLinePath, false);
        }
    }
    
    public void fillPath(final int windingRule) throws IOException {
        this.graphics.setComposite(this.getGraphicsState().getNonStrokeJavaComposite());
        Paint nonStrokingPaint = this.getGraphicsState().getNonStrokingColor().getJavaColor();
        if (nonStrokingPaint == null) {
            nonStrokingPaint = this.getGraphicsState().getNonStrokingColor().getPaint(this.pageSize.height);
        }
        if (nonStrokingPaint == null) {
            PageDrawer.LOG.info("ColorSpace " + this.getGraphicsState().getNonStrokingColor().getColorSpace().getName() + " doesn't provide a non-stroking color, using white instead!");
            nonStrokingPaint = Color.WHITE;
        }
        this.graphics.setPaint(nonStrokingPaint);
        this.getLinePath().setWindingRule(windingRule);
        this.graphics.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_OFF);
        this.graphics.setClip(this.getGraphicsState().getCurrentClippingPath());
        this.graphics.fill(this.getLinePath());
        this.getLinePath().reset();
    }
    
    public void setStroke(final BasicStroke newStroke) {
        this.getGraphics().setStroke(newStroke);
    }
    
    public BasicStroke getStroke() {
        return (BasicStroke)this.getGraphics().getStroke();
    }
    
    public void strokePath() throws IOException {
        this.graphics.setComposite(this.getGraphicsState().getStrokeJavaComposite());
        Paint strokingPaint = this.getGraphicsState().getStrokingColor().getJavaColor();
        if (strokingPaint == null) {
            strokingPaint = this.getGraphicsState().getStrokingColor().getPaint(this.pageSize.height);
        }
        if (strokingPaint == null) {
            PageDrawer.LOG.info("ColorSpace " + this.getGraphicsState().getStrokingColor().getColorSpace().getName() + " doesn't provide a stroking color, using white instead!");
            strokingPaint = Color.WHITE;
        }
        this.graphics.setPaint(strokingPaint);
        this.graphics.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_OFF);
        this.graphics.setClip(this.getGraphicsState().getCurrentClippingPath());
        final GeneralPath path = this.getLinePath();
        this.graphics.draw(path);
        path.reset();
    }
    
    @Deprecated
    public void colorChanged(final boolean bStroking) throws IOException {
    }
    
    public Point2D.Double transformedPoint(final double x, final double y) {
        final double[] position = { x, y };
        this.getGraphicsState().getCurrentTransformationMatrix().createAffineTransform().transform(position, 0, position, 0, 1);
        position[1] = this.fixY(position[1]);
        return new Point2D.Double(position[0], position[1]);
    }
    
    public void setClippingPath(final int windingRule) {
        final PDGraphicsState graphicsState = this.getGraphicsState();
        final GeneralPath clippingPath = (GeneralPath)this.getLinePath().clone();
        clippingPath.setWindingRule(windingRule);
        if (graphicsState.getCurrentClippingPath() != null) {
            final Area currentArea = new Area(this.getGraphicsState().getCurrentClippingPath());
            final Area newArea = new Area(clippingPath);
            currentArea.intersect(newArea);
            graphicsState.setCurrentClippingPath(currentArea);
        }
        else {
            graphicsState.setCurrentClippingPath(clippingPath);
        }
        this.getLinePath().reset();
    }
    
    public void drawImage(final Image awtImage, final AffineTransform at) {
        this.graphics.setComposite(this.getGraphicsState().getStrokeJavaComposite());
        this.graphics.setClip(this.getGraphicsState().getCurrentClippingPath());
        this.graphics.drawImage(awtImage, at, null);
    }
    
    @Deprecated
    public void SHFill(final COSName ShadingName) throws IOException {
        this.shFill(ShadingName);
    }
    
    public void shFill(final COSName shadingName) throws IOException {
        final PDShadingResources shading = this.getResources().getShadings().get(shadingName.getName());
        PageDrawer.LOG.debug("Shading = " + shading.toString());
        final int shadingType = shading.getShadingType();
        final Matrix ctm = this.getGraphicsState().getCurrentTransformationMatrix();
        Paint paint = null;
        switch (shadingType) {
            case 1: {
                PageDrawer.LOG.debug("Function based shading not yet supported");
                break;
            }
            case 2: {
                paint = new AxialShadingPaint((PDShadingType2)shading, ctm, this.pageSize.height);
                break;
            }
            case 3: {
                paint = new RadialShadingPaint((PDShadingType3)shading, ctm, this.pageSize.height);
                break;
            }
            case 4:
            case 5:
            case 6:
            case 7: {
                PageDrawer.LOG.debug("Shading type " + shadingType + " not yet supported");
                break;
            }
            default: {
                throw new IOException("Invalid ShadingType " + shadingType + " for Shading " + shadingName);
            }
        }
        this.graphics.setComposite(this.getGraphicsState().getNonStrokeJavaComposite());
        this.graphics.setPaint(paint);
        this.graphics.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_OFF);
        this.graphics.fill(this.getGraphicsState().getCurrentClippingPath());
    }
    
    protected void SHFill_Function(final PDShading Shading) throws IOException {
        throw new IOException("Not Implemented");
    }
    
    protected void SHFill_Axial(final PDShading Shading) throws IOException {
        throw new IOException("Not Implemented");
    }
    
    protected void SHFill_Radial(final PDShading Shading) throws IOException {
        throw new IOException("Not Implemented");
    }
    
    protected void SHFill_FreeGourad(final PDShading Shading) throws IOException {
        throw new IOException("Not Implemented");
    }
    
    protected void SHFill_LatticeGourad(final PDShading Shading) throws IOException {
        throw new IOException("Not Implemented");
    }
    
    protected void SHFill_CoonsPatch(final PDShading Shading) throws IOException {
        throw new IOException("Not Implemented");
    }
    
    protected void SHFill_TensorPatch(final PDShading Shading) throws IOException {
        throw new IOException("Not Implemented");
    }
    
    static {
        LOG = LogFactory.getLog(PageDrawer.class);
    }
}
