// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdfviewer;

import java.awt.Graphics2D;
import java.awt.Graphics;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import java.awt.Color;
import java.io.IOException;
import java.awt.Dimension;
import org.apache.pdfbox.pdmodel.PDPage;
import javax.swing.JPanel;

public class PDFPagePanel extends JPanel
{
    private static final long serialVersionUID = -4629033339560890669L;
    private PDPage page;
    private PageDrawer drawer;
    private Dimension pageDimension;
    private Dimension drawDimension;
    
    public PDFPagePanel() throws IOException {
        this.drawer = null;
        this.pageDimension = null;
        this.drawDimension = null;
        this.drawer = new PageDrawer();
    }
    
    public void setPage(final PDPage pdfPage) {
        this.page = pdfPage;
        final PDRectangle cropBox = this.page.findCropBox();
        this.drawDimension = cropBox.createDimension();
        final int rotation = this.page.findRotation();
        if (rotation == 90 || rotation == 270) {
            this.pageDimension = new Dimension(this.drawDimension.height, this.drawDimension.width);
        }
        else {
            this.pageDimension = this.drawDimension;
        }
        this.setSize(this.pageDimension);
        this.setBackground(Color.white);
    }
    
    @Override
    public void paint(final Graphics g) {
        try {
            g.setColor(this.getBackground());
            g.fillRect(0, 0, this.getWidth(), this.getHeight());
            final int rotation = this.page.findRotation();
            if (rotation == 90 || rotation == 270) {
                final Graphics2D g2D = (Graphics2D)g;
                g2D.translate(this.pageDimension.getWidth(), 0.0);
                g2D.rotate(Math.toRadians(rotation));
            }
            this.drawer.drawPage(g, this.page, this.drawDimension);
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }
}
