// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdfviewer;

import org.apache.pdfbox.cos.COSArray;
import org.apache.pdfbox.cos.COSStream;
import org.apache.pdfbox.cos.COSDictionary;
import org.apache.pdfbox.cos.COSNull;
import org.apache.pdfbox.cos.COSString;
import org.apache.pdfbox.cos.COSInteger;
import org.apache.pdfbox.cos.COSFloat;
import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSName;
import java.awt.Component;
import javax.swing.JTree;
import javax.swing.tree.DefaultTreeCellRenderer;

public class PDFTreeCellRenderer extends DefaultTreeCellRenderer
{
    @Override
    public Component getTreeCellRendererComponent(final JTree tree, Object nodeValue, final boolean isSelected, final boolean expanded, final boolean leaf, final int row, final boolean componentHasFocus) {
        nodeValue = this.convertToTreeObject(nodeValue);
        return super.getTreeCellRendererComponent(tree, nodeValue, isSelected, expanded, leaf, row, componentHasFocus);
    }
    
    private Object convertToTreeObject(Object nodeValue) {
        if (nodeValue instanceof MapEntry) {
            final MapEntry entry = (MapEntry)nodeValue;
            final COSName key = (COSName)entry.getKey();
            final COSBase value = (COSBase)entry.getValue();
            nodeValue = key.getName() + ":" + this.convertToTreeObject(value);
        }
        else if (nodeValue instanceof COSFloat) {
            nodeValue = "" + ((COSFloat)nodeValue).floatValue();
        }
        else if (nodeValue instanceof COSInteger) {
            nodeValue = "" + ((COSInteger)nodeValue).intValue();
        }
        else if (nodeValue instanceof COSString) {
            nodeValue = ((COSString)nodeValue).getString();
        }
        else if (nodeValue instanceof COSName) {
            nodeValue = ((COSName)nodeValue).getName();
        }
        else if (nodeValue instanceof ArrayEntry) {
            final ArrayEntry entry2 = (ArrayEntry)nodeValue;
            nodeValue = "[" + entry2.getIndex() + "]" + this.convertToTreeObject(entry2.getValue());
        }
        else if (nodeValue instanceof COSNull) {
            nodeValue = "null";
        }
        else if (nodeValue instanceof COSDictionary) {
            final COSDictionary dict = (COSDictionary)nodeValue;
            if (nodeValue instanceof COSStream) {
                nodeValue = "Stream";
            }
            else {
                nodeValue = "Dictionary";
            }
            final COSName type = (COSName)dict.getDictionaryObject(COSName.TYPE);
            if (type != null) {
                nodeValue = nodeValue + "(" + type.getName();
                final COSName subType = (COSName)dict.getDictionaryObject(COSName.SUBTYPE);
                if (subType != null) {
                    nodeValue = nodeValue + ":" + subType.getName();
                }
                nodeValue += ")";
            }
        }
        else if (nodeValue instanceof COSArray) {
            nodeValue = "Array";
        }
        else if (nodeValue instanceof COSString) {
            nodeValue = ((COSString)nodeValue).getString();
        }
        return nodeValue;
    }
}
