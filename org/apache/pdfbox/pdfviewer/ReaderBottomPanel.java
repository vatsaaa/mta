// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdfviewer;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.ComponentOrientation;
import java.awt.LayoutManager;
import java.awt.FlowLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class ReaderBottomPanel extends JPanel
{
    private JLabel statusLabel;
    
    public ReaderBottomPanel() {
        this.statusLabel = null;
        this.initialize();
    }
    
    private void initialize() {
        final FlowLayout flowLayout1 = new FlowLayout();
        this.setLayout(flowLayout1);
        this.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
        this.setPreferredSize(new Dimension(1000, 20));
        flowLayout1.setAlignment(0);
        this.add(this.getStatusLabel(), null);
    }
    
    public JLabel getStatusLabel() {
        if (this.statusLabel == null) {
            (this.statusLabel = new JLabel()).setText("Ready");
        }
        return this.statusLabel;
    }
}
