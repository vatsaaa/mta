// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdfviewer;

import javax.swing.tree.TreePath;
import org.apache.pdfbox.cos.COSBase;
import java.util.List;
import org.apache.pdfbox.cos.COSObject;
import org.apache.pdfbox.cos.COSDocument;
import java.util.Collections;
import java.util.Collection;
import org.apache.pdfbox.cos.COSName;
import java.util.ArrayList;
import org.apache.pdfbox.cos.COSDictionary;
import org.apache.pdfbox.cos.COSArray;
import javax.swing.event.TreeModelListener;
import org.apache.pdfbox.pdmodel.PDDocument;
import javax.swing.tree.TreeModel;

public class PDFTreeModel implements TreeModel
{
    private PDDocument document;
    
    public PDFTreeModel() {
    }
    
    public PDFTreeModel(final PDDocument doc) {
        this.setDocument(doc);
    }
    
    public void setDocument(final PDDocument doc) {
        this.document = doc;
    }
    
    public void addTreeModelListener(final TreeModelListener l) {
    }
    
    public Object getChild(final Object parent, final int index) {
        Object retval = null;
        if (parent instanceof COSArray) {
            final ArrayEntry entry = new ArrayEntry();
            entry.setIndex(index);
            entry.setValue(((COSArray)parent).getObject(index));
            retval = entry;
        }
        else if (parent instanceof COSDictionary) {
            final COSDictionary dict = (COSDictionary)parent;
            final List<COSName> keys = new ArrayList<COSName>(dict.keySet());
            Collections.sort(keys);
            final Object key = keys.get(index);
            final Object value = dict.getDictionaryObject((COSName)key);
            final MapEntry entry2 = new MapEntry();
            entry2.setKey(key);
            entry2.setValue(value);
            retval = entry2;
        }
        else if (parent instanceof MapEntry) {
            retval = this.getChild(((MapEntry)parent).getValue(), index);
        }
        else if (parent instanceof ArrayEntry) {
            retval = this.getChild(((ArrayEntry)parent).getValue(), index);
        }
        else if (parent instanceof COSDocument) {
            retval = ((COSDocument)parent).getObjects().get(index);
        }
        else {
            if (!(parent instanceof COSObject)) {
                throw new RuntimeException("Unknown COS type " + parent.getClass().getName());
            }
            retval = ((COSObject)parent).getObject();
        }
        return retval;
    }
    
    public int getChildCount(final Object parent) {
        int retval = 0;
        if (parent instanceof COSArray) {
            retval = ((COSArray)parent).size();
        }
        else if (parent instanceof COSDictionary) {
            retval = ((COSDictionary)parent).size();
        }
        else if (parent instanceof MapEntry) {
            retval = this.getChildCount(((MapEntry)parent).getValue());
        }
        else if (parent instanceof ArrayEntry) {
            retval = this.getChildCount(((ArrayEntry)parent).getValue());
        }
        else if (parent instanceof COSDocument) {
            retval = ((COSDocument)parent).getObjects().size();
        }
        else if (parent instanceof COSObject) {
            retval = 1;
        }
        return retval;
    }
    
    public int getIndexOfChild(final Object parent, final Object child) {
        int retval = -1;
        if (parent != null && child != null) {
            if (parent instanceof COSArray) {
                final COSArray array = (COSArray)parent;
                if (child instanceof ArrayEntry) {
                    final ArrayEntry arrayEntry = (ArrayEntry)child;
                    retval = arrayEntry.getIndex();
                }
                else {
                    retval = array.indexOf((COSBase)child);
                }
            }
            else if (parent instanceof COSDictionary) {
                final MapEntry entry = (MapEntry)child;
                final COSDictionary dict = (COSDictionary)parent;
                final List<COSName> keys = new ArrayList<COSName>(dict.keySet());
                Collections.sort(keys);
                for (int i = 0; retval == -1 && i < keys.size(); ++i) {
                    if (keys.get(i).equals(entry.getKey())) {
                        retval = i;
                    }
                }
            }
            else if (parent instanceof MapEntry) {
                retval = this.getIndexOfChild(((MapEntry)parent).getValue(), child);
            }
            else if (parent instanceof ArrayEntry) {
                retval = this.getIndexOfChild(((ArrayEntry)parent).getValue(), child);
            }
            else if (parent instanceof COSDocument) {
                retval = ((COSDocument)parent).getObjects().indexOf(child);
            }
            else {
                if (!(parent instanceof COSObject)) {
                    throw new RuntimeException("Unknown COS type " + parent.getClass().getName());
                }
                retval = 0;
            }
        }
        return retval;
    }
    
    public Object getRoot() {
        return this.document.getDocument().getTrailer();
    }
    
    public boolean isLeaf(final Object node) {
        final boolean isLeaf = !(node instanceof COSDictionary) && !(node instanceof COSArray) && !(node instanceof COSDocument) && !(node instanceof COSObject) && (!(node instanceof MapEntry) || this.isLeaf(((MapEntry)node).getValue())) && (!(node instanceof ArrayEntry) || this.isLeaf(((ArrayEntry)node).getValue()));
        return isLeaf;
    }
    
    public void removeTreeModelListener(final TreeModelListener l) {
    }
    
    public void valueForPathChanged(final TreePath path, final Object newValue) {
    }
}
