// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdfviewer;

import java.awt.event.MouseEvent;
import java.awt.Dimension;
import org.apache.pdfbox.pdmodel.PDPage;
import java.io.IOException;
import javax.swing.border.LineBorder;
import java.awt.Component;
import java.awt.LayoutManager;
import org.apache.pdfbox.PDFReader;
import javax.swing.JPanel;
import java.awt.event.MouseMotionListener;

public class PageWrapper implements MouseMotionListener
{
    private JPanel pageWrapper;
    private PDFPagePanel pagePanel;
    private PDFReader reader;
    private static final int SPACE_AROUND_DOCUMENT = 20;
    
    public PageWrapper(final PDFReader aReader) throws IOException {
        this.pageWrapper = new JPanel();
        this.pagePanel = null;
        this.reader = null;
        this.reader = aReader;
        this.pagePanel = new PDFPagePanel();
        this.pageWrapper.setLayout(null);
        this.pageWrapper.add(this.pagePanel);
        this.pagePanel.setLocation(20, 20);
        this.pageWrapper.setBorder(LineBorder.createBlackLineBorder());
        this.pagePanel.addMouseMotionListener(this);
    }
    
    public void displayPage(final PDPage page) {
        this.pagePanel.setPage(page);
        this.pagePanel.setPreferredSize(this.pagePanel.getSize());
        final Dimension size;
        final Dimension d = size = this.pagePanel.getSize();
        size.width += 40;
        final Dimension dimension = d;
        dimension.height += 40;
        this.pageWrapper.setPreferredSize(d);
        this.pageWrapper.validate();
    }
    
    public JPanel getPanel() {
        return this.pageWrapper;
    }
    
    public void mouseDragged(final MouseEvent e) {
    }
    
    public void mouseMoved(final MouseEvent e) {
        this.reader.getBottomStatusPanel().getStatusLabel().setText(e.getX() + "," + e.getY());
    }
}
