// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.encryption;

import java.io.OutputStream;
import java.io.ByteArrayOutputStream;
import java.io.ByteArrayInputStream;
import java.util.Map;
import java.io.InputStream;
import org.apache.pdfbox.cos.COSStream;
import org.apache.pdfbox.cos.COSName;
import java.util.Iterator;
import java.util.List;
import org.apache.pdfbox.cos.COSObject;
import org.apache.pdfbox.cos.COSDictionary;
import org.apache.pdfbox.exceptions.InvalidPasswordException;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import org.apache.pdfbox.exceptions.CryptographyException;
import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSString;
import java.math.BigInteger;
import java.security.MessageDigest;
import org.apache.pdfbox.cos.COSArray;
import org.apache.pdfbox.pdmodel.encryption.PDStandardEncryption;
import java.util.HashSet;
import java.util.Set;
import org.apache.pdfbox.cos.COSDocument;
import org.apache.pdfbox.pdmodel.PDDocument;

public class DocumentEncryption
{
    private PDDocument pdDocument;
    private COSDocument document;
    private byte[] encryptionKey;
    private PDFEncryption encryption;
    private Set objects;
    private Set potentialSignatures;
    
    public DocumentEncryption(final PDDocument doc) {
        this.pdDocument = null;
        this.document = null;
        this.encryptionKey = null;
        this.encryption = new PDFEncryption();
        this.objects = new HashSet();
        this.potentialSignatures = new HashSet();
        this.pdDocument = doc;
        this.document = doc.getDocument();
    }
    
    public DocumentEncryption(final COSDocument doc) {
        this.pdDocument = null;
        this.document = null;
        this.encryptionKey = null;
        this.encryption = new PDFEncryption();
        this.objects = new HashSet();
        this.potentialSignatures = new HashSet();
        this.pdDocument = new PDDocument(doc);
        this.document = doc;
    }
    
    public void initForEncryption() throws CryptographyException, IOException {
        String ownerPassword = this.pdDocument.getOwnerPasswordForEncryption();
        String userPassword = this.pdDocument.getUserPasswordForEncryption();
        if (ownerPassword == null) {
            ownerPassword = "";
        }
        if (userPassword == null) {
            userPassword = "";
        }
        final PDStandardEncryption encParameters = (PDStandardEncryption)this.pdDocument.getEncryptionDictionary();
        final int permissionInt = encParameters.getPermissions();
        final int revision = encParameters.getRevision();
        final int length = encParameters.getLength() / 8;
        COSArray idArray = this.document.getDocumentID();
        if (idArray == null || idArray.size() < 2) {
            idArray = new COSArray();
            try {
                final MessageDigest md = MessageDigest.getInstance("MD5");
                final BigInteger time = BigInteger.valueOf(System.currentTimeMillis());
                md.update(time.toByteArray());
                md.update(ownerPassword.getBytes("ISO-8859-1"));
                md.update(userPassword.getBytes("ISO-8859-1"));
                md.update(this.document.toString().getBytes());
                final byte[] id = md.digest(this.toString().getBytes("ISO-8859-1"));
                final COSString idString = new COSString();
                idString.append(id);
                idArray.add(idString);
                idArray.add(idString);
                this.document.setDocumentID(idArray);
            }
            catch (NoSuchAlgorithmException e) {
                throw new CryptographyException(e);
            }
        }
        final COSString id2 = (COSString)idArray.getObject(0);
        this.encryption = new PDFEncryption();
        final byte[] o = this.encryption.computeOwnerPassword(ownerPassword.getBytes("ISO-8859-1"), userPassword.getBytes("ISO-8859-1"), revision, length);
        final byte[] u = this.encryption.computeUserPassword(userPassword.getBytes("ISO-8859-1"), o, permissionInt, id2.getBytes(), revision, length);
        this.encryptionKey = this.encryption.computeEncryptedKey(userPassword.getBytes("ISO-8859-1"), o, permissionInt, id2.getBytes(), revision, length);
        encParameters.setOwnerKey(o);
        encParameters.setUserKey(u);
        this.document.setEncryptionDictionary(encParameters.getCOSDictionary());
    }
    
    public void decryptDocument(String password) throws CryptographyException, IOException, InvalidPasswordException {
        if (password == null) {
            password = "";
        }
        final PDStandardEncryption encParameters = (PDStandardEncryption)this.pdDocument.getEncryptionDictionary();
        final int permissions = encParameters.getPermissions();
        final int revision = encParameters.getRevision();
        final int length = encParameters.getLength() / 8;
        final COSString id = (COSString)this.document.getDocumentID().getObject(0);
        final byte[] u = encParameters.getUserKey();
        final byte[] o = encParameters.getOwnerKey();
        final boolean isUserPassword = this.encryption.isUserPassword(password.getBytes("ISO-8859-1"), u, o, permissions, id.getBytes(), revision, length);
        final boolean isOwnerPassword = this.encryption.isOwnerPassword(password.getBytes("ISO-8859-1"), u, o, permissions, id.getBytes(), revision, length);
        if (isUserPassword) {
            this.encryptionKey = this.encryption.computeEncryptedKey(password.getBytes("ISO-8859-1"), o, permissions, id.getBytes(), revision, length);
        }
        else {
            if (!isOwnerPassword) {
                throw new InvalidPasswordException("Error: The supplied password does not match either the owner or user password in the document.");
            }
            final byte[] computedUserPassword = this.encryption.getUserPassword(password.getBytes("ISO-8859-1"), o, revision, length);
            this.encryptionKey = this.encryption.computeEncryptedKey(computedUserPassword, o, permissions, id.getBytes(), revision, length);
        }
        final COSDictionary trailer = this.document.getTrailer();
        final COSArray fields = (COSArray)trailer.getObjectFromPath("Root/AcroForm/Fields");
        if (fields != null) {
            for (int i = 0; i < fields.size(); ++i) {
                final COSDictionary field = (COSDictionary)fields.getObject(i);
                this.addDictionaryAndSubDictionary(this.potentialSignatures, field);
            }
        }
        final List allObjects = this.document.getObjects();
        final Iterator objectIter = allObjects.iterator();
        while (objectIter.hasNext()) {
            this.decryptObject(objectIter.next());
        }
        this.document.setEncryptionDictionary(null);
    }
    
    private void addDictionaryAndSubDictionary(final Set set, final COSDictionary dic) {
        set.add(dic);
        final COSArray kids = (COSArray)dic.getDictionaryObject(COSName.KIDS);
        for (int i = 0; kids != null && i < kids.size(); ++i) {
            this.addDictionaryAndSubDictionary(set, (COSDictionary)kids.getObject(i));
        }
        final COSBase value = dic.getDictionaryObject(COSName.V);
        if (value instanceof COSDictionary) {
            this.addDictionaryAndSubDictionary(set, (COSDictionary)value);
        }
    }
    
    private void decryptObject(final COSObject object) throws CryptographyException, IOException {
        final long objNum = object.getObjectNumber().intValue();
        final long genNum = object.getGenerationNumber().intValue();
        final COSBase base = object.getObject();
        this.decrypt(base, objNum, genNum);
    }
    
    public void decrypt(final Object obj, final long objNum, final long genNum) throws CryptographyException, IOException {
        if (!this.objects.contains(obj)) {
            this.objects.add(obj);
            if (obj instanceof COSString) {
                this.decryptString((COSString)obj, objNum, genNum);
            }
            else if (obj instanceof COSStream) {
                this.decryptStream((COSStream)obj, objNum, genNum);
            }
            else if (obj instanceof COSDictionary) {
                this.decryptDictionary((COSDictionary)obj, objNum, genNum);
            }
            else if (obj instanceof COSArray) {
                this.decryptArray((COSArray)obj, objNum, genNum);
            }
        }
    }
    
    private void decryptStream(final COSStream stream, final long objNum, final long genNum) throws CryptographyException, IOException {
        this.decryptDictionary(stream, objNum, genNum);
        final InputStream encryptedStream = stream.getFilteredStream();
        this.encryption.encryptData(objNum, genNum, this.encryptionKey, encryptedStream, stream.createFilteredStream());
    }
    
    private void decryptDictionary(final COSDictionary dictionary, final long objNum, final long genNum) throws CryptographyException, IOException {
        for (final Map.Entry<COSName, COSBase> entry : dictionary.entrySet()) {
            if (!entry.getKey().getName().equals("Contents") || !(entry.getValue() instanceof COSString) || !this.potentialSignatures.contains(dictionary)) {
                this.decrypt(entry.getValue(), objNum, genNum);
            }
        }
    }
    
    private void decryptString(final COSString string, final long objNum, final long genNum) throws CryptographyException, IOException {
        final ByteArrayInputStream data = new ByteArrayInputStream(string.getBytes());
        final ByteArrayOutputStream buffer = new ByteArrayOutputStream();
        this.encryption.encryptData(objNum, genNum, this.encryptionKey, data, buffer);
        string.reset();
        string.append(buffer.toByteArray());
    }
    
    private void decryptArray(final COSArray array, final long objNum, final long genNum) throws CryptographyException, IOException {
        for (int i = 0; i < array.size(); ++i) {
            this.decrypt(array.get(i), objNum, genNum);
        }
    }
}
