// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.encryption;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import org.apache.pdfbox.exceptions.CryptographyException;
import java.security.MessageDigest;
import java.io.OutputStream;
import java.io.InputStream;

public final class PDFEncryption
{
    private ARCFour rc4;
    public static final byte[] ENCRYPT_PADDING;
    
    public PDFEncryption() {
        this.rc4 = new ARCFour();
    }
    
    public final void encryptData(final long objectNumber, final long genNumber, final byte[] key, final InputStream data, final OutputStream output) throws CryptographyException, IOException {
        final byte[] newKey = new byte[key.length + 5];
        System.arraycopy(key, 0, newKey, 0, key.length);
        newKey[newKey.length - 5] = (byte)(objectNumber & 0xFFL);
        newKey[newKey.length - 4] = (byte)(objectNumber >> 8 & 0xFFL);
        newKey[newKey.length - 3] = (byte)(objectNumber >> 16 & 0xFFL);
        newKey[newKey.length - 2] = (byte)(genNumber & 0xFFL);
        newKey[newKey.length - 1] = (byte)(genNumber >> 8 & 0xFFL);
        byte[] digestedKey = null;
        try {
            final MessageDigest md = MessageDigest.getInstance("MD5");
            digestedKey = md.digest(newKey);
        }
        catch (NoSuchAlgorithmException e) {
            throw new CryptographyException(e);
        }
        final int length = Math.min(newKey.length, 16);
        final byte[] finalKey = new byte[length];
        System.arraycopy(digestedKey, 0, finalKey, 0, length);
        this.rc4.setKey(finalKey);
        this.rc4.write(data, output);
        output.flush();
    }
    
    public final byte[] getUserPassword(final byte[] ownerPassword, final byte[] o, final int revision, final long length) throws CryptographyException, IOException {
        try {
            final ByteArrayOutputStream result = new ByteArrayOutputStream();
            final byte[] ownerPadded = this.truncateOrPad(ownerPassword);
            final MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(ownerPadded);
            byte[] digest = md.digest();
            if (revision == 3 || revision == 4) {
                for (int i = 0; i < 50; ++i) {
                    md.reset();
                    md.update(digest);
                    digest = md.digest();
                }
            }
            if (revision == 2 && length != 5L) {
                throw new CryptographyException("Error: Expected length=5 actual=" + length);
            }
            final byte[] rc4Key = new byte[(int)length];
            System.arraycopy(digest, 0, rc4Key, 0, (int)length);
            if (revision == 2) {
                this.rc4.setKey(rc4Key);
                this.rc4.write(o, result);
            }
            else if (revision == 3 || revision == 4) {
                final byte[] iterationKey = new byte[rc4Key.length];
                byte[] otemp = new byte[o.length];
                System.arraycopy(o, 0, otemp, 0, o.length);
                this.rc4.write(o, result);
                for (int j = 19; j >= 0; --j) {
                    System.arraycopy(rc4Key, 0, iterationKey, 0, rc4Key.length);
                    for (int k = 0; k < iterationKey.length; ++k) {
                        iterationKey[k] ^= (byte)j;
                    }
                    this.rc4.setKey(iterationKey);
                    result.reset();
                    this.rc4.write(otemp, result);
                    otemp = result.toByteArray();
                }
            }
            return result.toByteArray();
        }
        catch (NoSuchAlgorithmException e) {
            throw new CryptographyException(e);
        }
    }
    
    public final boolean isOwnerPassword(final byte[] ownerPassword, final byte[] u, final byte[] o, final int permissions, final byte[] id, final int revision, final int length) throws CryptographyException, IOException {
        final byte[] userPassword = this.getUserPassword(ownerPassword, o, revision, length);
        return this.isUserPassword(userPassword, u, o, permissions, id, revision, length);
    }
    
    public final boolean isUserPassword(final byte[] password, final byte[] u, final byte[] o, final int permissions, final byte[] id, final int revision, final int length) throws CryptographyException, IOException {
        boolean matches = false;
        final byte[] computedValue = this.computeUserPassword(password, o, permissions, id, revision, length);
        if (revision == 2) {
            matches = this.arraysEqual(u, computedValue);
        }
        else if (revision == 3 || revision == 4) {
            matches = this.arraysEqual(u, computedValue, 16);
        }
        return matches;
    }
    
    private final boolean arraysEqual(final byte[] first, final byte[] second, final int count) {
        boolean equal = first.length >= count && second.length >= count;
        for (int i = 0; i < count && equal; equal = (first[i] == second[i]), ++i) {}
        return equal;
    }
    
    private final boolean arraysEqual(final byte[] first, final byte[] second) {
        boolean equal = first.length == second.length;
        for (int i = 0; i < first.length && equal; equal = (first[i] == second[i]), ++i) {}
        return equal;
    }
    
    public final byte[] computeUserPassword(final byte[] password, final byte[] o, final int permissions, final byte[] id, final int revision, final int length) throws CryptographyException, IOException {
        final ByteArrayOutputStream result = new ByteArrayOutputStream();
        final byte[] encryptionKey = this.computeEncryptedKey(password, o, permissions, id, revision, length);
        if (revision == 2) {
            this.rc4.setKey(encryptionKey);
            this.rc4.write(PDFEncryption.ENCRYPT_PADDING, result);
        }
        else {
            if (revision != 3) {
                if (revision != 4) {
                    return result.toByteArray();
                }
            }
            try {
                final MessageDigest md = MessageDigest.getInstance("MD5");
                md.update(PDFEncryption.ENCRYPT_PADDING);
                md.update(id);
                result.write(md.digest());
                final byte[] iterationKey = new byte[encryptionKey.length];
                for (int i = 0; i < 20; ++i) {
                    System.arraycopy(encryptionKey, 0, iterationKey, 0, iterationKey.length);
                    for (int j = 0; j < iterationKey.length; ++j) {
                        iterationKey[j] ^= (byte)i;
                    }
                    this.rc4.setKey(iterationKey);
                    final ByteArrayInputStream input = new ByteArrayInputStream(result.toByteArray());
                    result.reset();
                    this.rc4.write(input, result);
                }
                final byte[] finalResult = new byte[32];
                System.arraycopy(result.toByteArray(), 0, finalResult, 0, 16);
                System.arraycopy(PDFEncryption.ENCRYPT_PADDING, 0, finalResult, 16, 16);
                result.reset();
                result.write(finalResult);
            }
            catch (NoSuchAlgorithmException e) {
                throw new CryptographyException(e);
            }
        }
        return result.toByteArray();
    }
    
    public final byte[] computeEncryptedKey(final byte[] password, final byte[] o, final int permissions, final byte[] id, final int revision, final int length) throws CryptographyException {
        final byte[] result = new byte[length];
        try {
            final byte[] padded = this.truncateOrPad(password);
            final MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(padded);
            md.update(o);
            final byte zero = (byte)(permissions >>> 0);
            final byte one = (byte)(permissions >>> 8);
            final byte two = (byte)(permissions >>> 16);
            final byte three = (byte)(permissions >>> 24);
            md.update(zero);
            md.update(one);
            md.update(two);
            md.update(three);
            md.update(id);
            byte[] digest = md.digest();
            if (revision == 3 || revision == 4) {
                for (int i = 0; i < 50; ++i) {
                    md.reset();
                    md.update(digest, 0, length);
                    digest = md.digest();
                }
            }
            if (revision == 2 && length != 5) {
                throw new CryptographyException("Error: length should be 5 when revision is two actual=" + length);
            }
            System.arraycopy(digest, 0, result, 0, length);
        }
        catch (NoSuchAlgorithmException e) {
            throw new CryptographyException(e);
        }
        return result;
    }
    
    public final byte[] computeOwnerPassword(final byte[] ownerPassword, final byte[] userPassword, final int revision, final int length) throws CryptographyException, IOException {
        try {
            final byte[] ownerPadded = this.truncateOrPad(ownerPassword);
            final MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(ownerPadded);
            byte[] digest = md.digest();
            if (revision == 3 || revision == 4) {
                for (int i = 0; i < 50; ++i) {
                    md.reset();
                    md.update(digest, 0, length);
                    digest = md.digest();
                }
            }
            if (revision == 2 && length != 5) {
                throw new CryptographyException("Error: Expected length=5 actual=" + length);
            }
            final byte[] rc4Key = new byte[length];
            System.arraycopy(digest, 0, rc4Key, 0, length);
            final byte[] paddedUser = this.truncateOrPad(userPassword);
            this.rc4.setKey(rc4Key);
            final ByteArrayOutputStream crypted = new ByteArrayOutputStream();
            this.rc4.write(new ByteArrayInputStream(paddedUser), crypted);
            if (revision == 3 || revision == 4) {
                final byte[] iterationKey = new byte[rc4Key.length];
                for (int j = 1; j < 20; ++j) {
                    System.arraycopy(rc4Key, 0, iterationKey, 0, rc4Key.length);
                    for (int k = 0; k < iterationKey.length; ++k) {
                        iterationKey[k] ^= (byte)j;
                    }
                    this.rc4.setKey(iterationKey);
                    final ByteArrayInputStream input = new ByteArrayInputStream(crypted.toByteArray());
                    crypted.reset();
                    this.rc4.write(input, crypted);
                }
            }
            return crypted.toByteArray();
        }
        catch (NoSuchAlgorithmException e) {
            throw new CryptographyException(e.getMessage());
        }
    }
    
    private final byte[] truncateOrPad(final byte[] password) {
        final byte[] padded = new byte[PDFEncryption.ENCRYPT_PADDING.length];
        final int bytesBeforePad = Math.min(password.length, padded.length);
        System.arraycopy(password, 0, padded, 0, bytesBeforePad);
        System.arraycopy(PDFEncryption.ENCRYPT_PADDING, 0, padded, bytesBeforePad, PDFEncryption.ENCRYPT_PADDING.length - bytesBeforePad);
        return padded;
    }
    
    static {
        ENCRYPT_PADDING = new byte[] { 40, -65, 78, 94, 78, 117, -118, 65, 100, 0, 78, 86, -1, -6, 1, 8, 46, 46, 0, -74, -48, 104, 62, -128, 47, 12, -87, -2, 100, 83, 105, 122 };
    }
}
