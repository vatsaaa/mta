// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.encryption;

import java.io.InputStream;
import java.io.IOException;
import java.io.OutputStream;

public class ARCFour
{
    private int[] salt;
    private int b;
    private int c;
    
    public ARCFour() {
        this.salt = new int[256];
    }
    
    public void setKey(final byte[] key) {
        this.b = 0;
        this.c = 0;
        if (key.length < 1 || key.length > 32) {
            throw new IllegalArgumentException("number of bytes must be between 1 and 32");
        }
        for (int i = 0; i < this.salt.length; ++i) {
            this.salt[i] = i;
        }
        int keyIndex = 0;
        int saltIndex = 0;
        for (int j = 0; j < this.salt.length; ++j) {
            saltIndex = (fixByte(key[keyIndex]) + this.salt[j] + saltIndex) % 256;
            swap(this.salt, j, saltIndex);
            keyIndex = (keyIndex + 1) % key.length;
        }
    }
    
    private static final int fixByte(final byte aByte) {
        return (aByte < 0) ? (256 + aByte) : aByte;
    }
    
    private static final void swap(final int[] data, final int firstIndex, final int secondIndex) {
        final int tmp = data[firstIndex];
        data[firstIndex] = data[secondIndex];
        data[secondIndex] = tmp;
    }
    
    public void write(final byte aByte, final OutputStream output) throws IOException {
        this.b = (this.b + 1) % 256;
        this.c = (this.salt[this.b] + this.c) % 256;
        swap(this.salt, this.b, this.c);
        final int saltIndex = (this.salt[this.b] + this.salt[this.c]) % 256;
        output.write(aByte ^ (byte)this.salt[saltIndex]);
    }
    
    public void write(final byte[] data, final OutputStream output) throws IOException {
        for (int i = 0; i < data.length; ++i) {
            this.write(data[i], output);
        }
    }
    
    public void write(final InputStream data, final OutputStream output) throws IOException {
        final byte[] buffer = new byte[1024];
        int amountRead = 0;
        while ((amountRead = data.read(buffer)) != -1) {
            this.write(buffer, 0, amountRead, output);
        }
    }
    
    public void write(final byte[] data, final int offset, final int len, final OutputStream output) throws IOException {
        for (int i = offset; i < offset + len; ++i) {
            this.write(data[i], output);
        }
    }
}
