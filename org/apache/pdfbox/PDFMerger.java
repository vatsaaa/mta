// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox;

import org.apache.pdfbox.util.PDFMergerUtility;

public class PDFMerger
{
    private PDFMerger() {
    }
    
    public static void main(final String[] args) throws Exception {
        final PDFMerger merge = new PDFMerger();
        merge.merge(args);
    }
    
    private void merge(final String[] args) throws Exception {
        String destinationFileName = "";
        String sourceFileName = null;
        if (args.length < 3) {
            usage();
        }
        final PDFMergerUtility merger = new PDFMergerUtility();
        for (int i = 0; i < args.length - 1; ++i) {
            sourceFileName = args[i];
            merger.addSource(sourceFileName);
        }
        destinationFileName = args[args.length - 1];
        merger.setDestinationFileName(destinationFileName);
        merger.mergeDocuments();
    }
    
    private static void usage() {
        System.err.println("Usage: java -jar pdfbox-app-x.y.z.jar PDFMerger <Source PDF File 2..n> <Destination PDF File>\n  <Source PDF File 2..n>       2 or more source PDF documents to merge\n  <Destination PDF File>       The PDF document to save the merged documents to\n");
        System.exit(1);
    }
}
