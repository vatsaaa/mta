// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox;

import java.io.FileReader;
import org.apache.pdfbox.pdmodel.font.PDTrueTypeFont;
import java.io.File;
import java.io.IOException;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.edit.PDPageContentStream;
import org.apache.pdfbox.pdmodel.PDPage;
import java.io.BufferedReader;
import org.apache.pdfbox.pdmodel.PDDocument;
import java.io.Reader;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.apache.pdfbox.pdmodel.font.PDSimpleFont;

public class TextToPDF
{
    private int fontSize;
    private PDSimpleFont font;
    
    public TextToPDF() {
        this.fontSize = 10;
        this.font = PDType1Font.HELVETICA;
    }
    
    public PDDocument createPDFFromText(final Reader text) throws IOException {
        PDDocument doc = null;
        try {
            final int margin = 40;
            float height = this.font.getFontDescriptor().getFontBoundingBox().getHeight() / 1000.0f;
            height = height * this.fontSize * 1.05f;
            doc = new PDDocument();
            final BufferedReader data = new BufferedReader(text);
            String nextLine = null;
            PDPage page = new PDPage();
            PDPageContentStream contentStream = null;
            float y = -1.0f;
            final float maxStringLength = page.getMediaBox().getWidth() - 80.0f;
            boolean textIsEmpty = true;
            while ((nextLine = data.readLine()) != null) {
                textIsEmpty = false;
                final String[] lineWords = nextLine.trim().split(" ");
                int lineIndex = 0;
                while (lineIndex < lineWords.length) {
                    final StringBuffer nextLineToDraw = new StringBuffer();
                    float lengthIfUsingNextWord = 0.0f;
                    do {
                        nextLineToDraw.append(lineWords[lineIndex]);
                        nextLineToDraw.append(" ");
                        if (++lineIndex < lineWords.length) {
                            final String lineWithNextWord = nextLineToDraw.toString() + lineWords[lineIndex];
                            lengthIfUsingNextWord = this.font.getStringWidth(lineWithNextWord) / 1000.0f * this.fontSize;
                        }
                    } while (lineIndex < lineWords.length && lengthIfUsingNextWord < maxStringLength);
                    if (y < 40.0f) {
                        page = new PDPage();
                        doc.addPage(page);
                        if (contentStream != null) {
                            contentStream.endText();
                            contentStream.close();
                        }
                        contentStream = new PDPageContentStream(doc, page);
                        contentStream.setFont(this.font, (float)this.fontSize);
                        contentStream.beginText();
                        y = page.getMediaBox().getHeight() - 40.0f + height;
                        contentStream.moveTextPositionByAmount(40.0f, y);
                    }
                    if (contentStream == null) {
                        throw new IOException("Error:Expected non-null content stream.");
                    }
                    contentStream.moveTextPositionByAmount(0.0f, -height);
                    y -= height;
                    contentStream.drawString(nextLineToDraw.toString());
                }
            }
            if (textIsEmpty) {
                doc.addPage(page);
            }
            if (contentStream != null) {
                contentStream.endText();
                contentStream.close();
            }
        }
        catch (IOException io) {
            if (doc != null) {
                doc.close();
            }
            throw io;
        }
        return doc;
    }
    
    public static void main(final String[] args) throws IOException {
        final TextToPDF app = new TextToPDF();
        PDDocument doc = null;
        try {
            if (args.length < 2) {
                app.usage();
            }
            else {
                for (int i = 0; i < args.length - 2; ++i) {
                    if (args[i].equals("-standardFont")) {
                        ++i;
                        app.setFont(PDType1Font.getStandardFont(args[i]));
                    }
                    else if (args[i].equals("-ttf")) {
                        ++i;
                        final PDTrueTypeFont font = PDTrueTypeFont.loadTTF(doc, new File(args[i]));
                        app.setFont(font);
                    }
                    else {
                        if (!args[i].equals("-fontSize")) {
                            throw new IOException("Unknown argument:" + args[i]);
                        }
                        ++i;
                        app.setFontSize(Integer.parseInt(args[i]));
                    }
                }
                doc = app.createPDFFromText(new FileReader(args[args.length - 1]));
                doc.save(args[args.length - 2]);
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            if (doc != null) {
                doc.close();
            }
        }
    }
    
    private void usage() {
        final String[] std14 = PDType1Font.getStandard14Names();
        System.err.println("usage: jar -jar pdfbox-app-x.y.z.jar TextToPDF [options] <output-file> <text-file>");
        System.err.println("    -standardFont <name>    default:" + PDType1Font.HELVETICA.getBaseFont());
        for (int i = 0; i < std14.length; ++i) {
            System.err.println("                                    " + std14[i]);
        }
        System.err.println("    -ttf <ttf file>         The TTF font to use.");
        System.err.println("    -fontSize <fontSize>    default:10");
    }
    
    public PDSimpleFont getFont() {
        return this.font;
    }
    
    public void setFont(final PDSimpleFont aFont) {
        this.font = aFont;
    }
    
    public int getFontSize() {
        return this.fontSize;
    }
    
    public void setFontSize(final int aFontSize) {
        this.fontSize = aFontSize;
    }
}
