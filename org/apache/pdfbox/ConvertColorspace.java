// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox;

import java.util.regex.Matcher;
import org.apache.pdfbox.exceptions.InvalidPasswordException;
import java.util.regex.Pattern;
import org.apache.pdfbox.cos.COSFloat;
import org.apache.pdfbox.cos.COSNumber;
import java.util.List;
import org.apache.pdfbox.pdfwriter.ContentStreamWriter;
import org.apache.pdfbox.pdmodel.common.PDStream;
import org.apache.pdfbox.util.PDFOperator;
import java.util.ArrayList;
import org.apache.pdfbox.pdfparser.PDFStreamParser;
import org.apache.pdfbox.pdmodel.PDPage;
import java.io.IOException;
import java.util.Hashtable;
import org.apache.pdfbox.pdmodel.PDDocument;

public class ConvertColorspace
{
    private static final String PASSWORD = "-password";
    private static final String CONVERSION = "-equiv";
    private static final String DEST_COLORSPACE = "-toColorspace";
    
    private ConvertColorspace() {
    }
    
    private void replaceColors(final PDDocument inputFile, final Hashtable colorEquivalents, final String destColorspace) throws IOException {
        if (!destColorspace.equals("CMYK")) {
            throw new IOException("Error: Unknown colorspace " + destColorspace);
        }
        final List pagesList = inputFile.getDocumentCatalog().getAllPages();
        PDPage currentPage = null;
        PDFStreamParser parser = null;
        List pageTokens = null;
        List editedPageTokens = null;
        for (int pageCounter = 0; pageCounter < pagesList.size(); ++pageCounter) {
            currentPage = pagesList.get(pageCounter);
            parser = new PDFStreamParser(currentPage.getContents().getStream());
            parser.parse();
            pageTokens = parser.getTokens();
            editedPageTokens = new ArrayList();
            for (int counter = 0; counter < pageTokens.size(); ++counter) {
                final Object token = pageTokens.get(counter);
                if (token instanceof PDFOperator) {
                    final PDFOperator tokenOperator = (PDFOperator)token;
                    if (tokenOperator.getOperation().equals("rg")) {
                        if (destColorspace.equals("CMYK")) {
                            this.replaceRGBTokensWithCMYKTokens(editedPageTokens, pageTokens, counter, colorEquivalents);
                            editedPageTokens.add(PDFOperator.getOperator("k"));
                        }
                    }
                    else if (tokenOperator.getOperation().equals("RG")) {
                        if (destColorspace.equals("CMYK")) {
                            this.replaceRGBTokensWithCMYKTokens(editedPageTokens, pageTokens, counter, colorEquivalents);
                            editedPageTokens.add(PDFOperator.getOperator("K"));
                        }
                    }
                    else if (tokenOperator.getOperation().equals("g")) {
                        if (destColorspace.equals("CMYK")) {
                            this.replaceGrayTokensWithCMYKTokens(editedPageTokens, pageTokens, counter, colorEquivalents);
                            editedPageTokens.add(PDFOperator.getOperator("k"));
                        }
                    }
                    else if (tokenOperator.getOperation().equals("G")) {
                        if (destColorspace.equals("CMYK")) {
                            this.replaceGrayTokensWithCMYKTokens(editedPageTokens, pageTokens, counter, colorEquivalents);
                            editedPageTokens.add(PDFOperator.getOperator("K"));
                        }
                    }
                    else {
                        editedPageTokens.add(token);
                    }
                }
                else {
                    editedPageTokens.add(token);
                }
            }
            final PDStream updatedPageContents = new PDStream(inputFile);
            final ContentStreamWriter contentWriter = new ContentStreamWriter(updatedPageContents.createOutputStream());
            contentWriter.writeTokens(editedPageTokens);
            currentPage.setContents(updatedPageContents);
        }
    }
    
    private void replaceRGBTokensWithCMYKTokens(final List editedPageTokens, final List pageTokens, final int counter, final Hashtable colorEquivalents) {
        final float red = pageTokens.get(counter - 3).floatValue();
        final float green = pageTokens.get(counter - 2).floatValue();
        final float blue = pageTokens.get(counter - 1).floatValue();
        final int intRed = Math.round(red * 255.0f);
        final int intGreen = Math.round(green * 255.0f);
        final int intBlue = Math.round(blue * 255.0f);
        final ColorSpaceInstance rgbColor = new ColorSpaceInstance();
        rgbColor.colorspace = "RGB";
        rgbColor.colorspaceValues = new int[] { intRed, intGreen, intBlue };
        final ColorSpaceInstance cmykColor = colorEquivalents.get(rgbColor);
        float[] cmyk = null;
        if (cmykColor != null) {
            cmyk = new float[] { cmykColor.colorspaceValues[0] / 100.0f, cmykColor.colorspaceValues[1] / 100.0f, cmykColor.colorspaceValues[2] / 100.0f, cmykColor.colorspaceValues[3] / 100.0f };
        }
        else {
            cmyk = convertRGBToCMYK(red, green, blue);
        }
        editedPageTokens.remove(editedPageTokens.size() - 1);
        editedPageTokens.remove(editedPageTokens.size() - 1);
        editedPageTokens.remove(editedPageTokens.size() - 1);
        editedPageTokens.add(new COSFloat(cmyk[0]));
        editedPageTokens.add(new COSFloat(cmyk[1]));
        editedPageTokens.add(new COSFloat(cmyk[2]));
        editedPageTokens.add(new COSFloat(cmyk[3]));
    }
    
    private void replaceGrayTokensWithCMYKTokens(final List editedPageTokens, final List pageTokens, final int counter, final Hashtable colorEquivalents) {
        final float gray = pageTokens.get(counter - 1).floatValue();
        final ColorSpaceInstance grayColor = new ColorSpaceInstance();
        grayColor.colorspace = "Grayscale";
        grayColor.colorspaceValues = new int[] { Math.round(gray * 100.0f) };
        final ColorSpaceInstance cmykColor = colorEquivalents.get(grayColor);
        float[] cmyk = null;
        if (cmykColor != null) {
            cmyk = new float[] { cmykColor.colorspaceValues[0] / 100.0f, cmykColor.colorspaceValues[1] / 100.0f, cmykColor.colorspaceValues[2] / 100.0f, cmykColor.colorspaceValues[3] / 100.0f };
        }
        else {
            cmyk = new float[] { 0.0f, 0.0f, 0.0f, gray };
        }
        editedPageTokens.remove(editedPageTokens.size() - 1);
        editedPageTokens.add(new COSFloat(cmyk[0]));
        editedPageTokens.add(new COSFloat(cmyk[1]));
        editedPageTokens.add(new COSFloat(cmyk[2]));
        editedPageTokens.add(new COSFloat(cmyk[3]));
    }
    
    private static float[] convertRGBToCMYK(final float red, final float green, final float blue) {
        float c = 1.0f - red;
        float m = 1.0f - green;
        float y = 1.0f - blue;
        float k = 1.0f;
        k = Math.min(Math.min(Math.min(c, k), m), y);
        c = (c - k) / (1.0f - k);
        m = (m - k) / (1.0f - k);
        y = (y - k) / (1.0f - k);
        return new float[] { c, m, y, k };
    }
    
    private static int[] stringToIntArray(final String string) {
        final String[] ints = string.split(",");
        final int[] retval = new int[ints.length];
        for (int i = 0; i < ints.length; ++i) {
            retval[i] = Integer.parseInt(ints[i]);
        }
        return retval;
    }
    
    public static void main(final String[] args) throws Exception {
        String password = "";
        String inputFile = null;
        String outputFile = null;
        String destColorspace = "CMYK";
        final Pattern colorEquivalentPattern = Pattern.compile("^(.*):\\((.*)\\)=(.*):\\((.*)\\)$");
        Matcher colorEquivalentMatcher = null;
        final Hashtable colorEquivalents = new Hashtable();
        for (int i = 0; i < args.length; ++i) {
            if (args[i].equals("-password")) {
                if (++i >= args.length) {
                    usage();
                }
                password = args[i];
            }
            if (args[i].equals("-toColorspace")) {
                if (++i >= args.length) {
                    usage();
                }
                destColorspace = args[i];
            }
            if (args[i].equals("-equiv")) {
                if (++i >= args.length) {
                    usage();
                }
                colorEquivalentMatcher = colorEquivalentPattern.matcher(args[i]);
                if (!colorEquivalentMatcher.matches()) {
                    usage();
                }
                final String srcColorSpace = colorEquivalentMatcher.group(1);
                final String srcColorvalues = colorEquivalentMatcher.group(2);
                final String destColorSpace = colorEquivalentMatcher.group(3);
                final String destColorvalues = colorEquivalentMatcher.group(4);
                final ColorSpaceInstance source = new ColorSpaceInstance();
                source.colorspace = srcColorSpace;
                source.colorspaceValues = stringToIntArray(srcColorvalues);
                final ColorSpaceInstance dest = new ColorSpaceInstance();
                dest.colorspace = destColorSpace;
                dest.colorspaceValues = stringToIntArray(destColorvalues);
                colorEquivalents.put(source, dest);
            }
            else if (inputFile == null) {
                inputFile = args[i];
            }
            else {
                outputFile = args[i];
            }
        }
        if (inputFile == null) {
            usage();
        }
        if (outputFile == null || outputFile.equals(inputFile)) {
            usage();
        }
        PDDocument doc = null;
        try {
            doc = PDDocument.load(inputFile);
            if (doc.isEncrypted()) {
                try {
                    doc.decrypt(password);
                }
                catch (InvalidPasswordException e) {
                    if (!password.equals("")) {
                        System.err.println("Error: The supplied password is incorrect.");
                        System.exit(2);
                    }
                    else {
                        System.err.println("Error: The document is encrypted.");
                        usage();
                    }
                }
            }
            final ConvertColorspace converter = new ConvertColorspace();
            converter.replaceColors(doc, colorEquivalents, destColorspace);
            doc.save(outputFile);
        }
        finally {
            if (doc != null) {
                doc.close();
            }
        }
    }
    
    private static void usage() {
        System.err.println("Usage: java org.apache.pdfbox.ConvertColorspace [OPTIONS] <PDF Input file> <PDF Output File>\n  -password  <password>                Password to decrypt document\n  -equiv <color equivalent>            Color equivalent to use for conversion.\n  -destColorspace <color equivalent>   The destination colorspace, CMYK is the only 'supported colorspace.  \n The equiv format is : <source colorspace>:(colorspace value)=<dest colorspace>:(colorspace value) This option can be used as many times as necessary\n The supported equiv colorspaces are RGB and CMYK.\n RGB color values are integers between 0 and 255 CMYK color values are integer between 0 and 100.\n Example: java org.apache.pdfbox.ConvertColorspace -equiv RGB:(255,0,0)=CMYK(0,99,100,0) input.pdf output.pdf\n  <PDF Input file>             The PDF document to use\n  <PDF Output file>            The PDF file to write the result to. Must be different of input file\n");
        System.exit(1);
    }
    
    private static class ColorSpaceInstance
    {
        private String colorspace;
        private int[] colorspaceValues;
        
        private ColorSpaceInstance() {
            this.colorspace = null;
            this.colorspaceValues = null;
        }
        
        @Override
        public int hashCode() {
            int code = this.colorspace.hashCode();
            for (int i = 0; i < this.colorspaceValues.length; ++i) {
                code += this.colorspaceValues[i];
            }
            return code;
        }
        
        @Override
        public boolean equals(final Object o) {
            boolean retval = false;
            if (o instanceof ColorSpaceInstance) {
                final ColorSpaceInstance other = (ColorSpaceInstance)o;
                if (this.colorspace.equals(other.colorspace) && this.colorspaceValues.length == other.colorspaceValues.length) {
                    retval = true;
                    for (int i = 0; i < this.colorspaceValues.length && retval; retval = (retval && this.colorspaceValues[i] == other.colorspaceValues[i]), ++i) {}
                }
            }
            return retval;
        }
    }
}
