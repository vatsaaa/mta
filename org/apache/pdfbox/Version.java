// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox;

import java.util.Properties;
import java.io.IOException;
import org.apache.pdfbox.util.ResourceLoader;

public class Version
{
    private static final String PDFBOX_VERSION_PROPERTIES = "org/apache/pdfbox/resources/pdfbox.properties";
    
    private Version() {
    }
    
    public static String getVersion() {
        String version = "unknown";
        try {
            final Properties props = ResourceLoader.loadProperties("org/apache/pdfbox/resources/pdfbox.properties", false);
            version = props.getProperty("pdfbox.version", version);
        }
        catch (IOException io) {
            io.printStackTrace();
        }
        return version;
    }
    
    public static void main(final String[] args) {
        if (args.length != 0) {
            usage();
            return;
        }
        System.out.println("Version:" + getVersion());
    }
    
    private static void usage() {
        System.err.println("usage: " + Version.class.getName());
    }
}
