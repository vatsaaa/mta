// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox;

import org.apache.pdfbox.exceptions.CryptographyException;
import org.apache.pdfbox.exceptions.InvalidPasswordException;
import org.apache.pdfbox.io.RandomAccess;
import java.awt.image.BufferedImage;
import org.apache.pdfbox.util.ImageIOUtil;
import org.apache.pdfbox.pdfviewer.PageWrapper;
import java.io.IOException;
import javax.swing.filechooser.FileFilter;
import org.apache.pdfbox.util.ExtensionFileFilter;
import javax.swing.JFileChooser;
import java.awt.Dimension;
import java.awt.Toolkit;
import javax.swing.KeyStroke;
import java.awt.print.PrinterException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Component;
import javax.swing.JScrollPane;
import java.awt.event.WindowListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowAdapter;
import org.apache.pdfbox.pdmodel.PDPage;
import java.util.List;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdfviewer.ReaderBottomPanel;
import javax.swing.JPanel;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import java.io.File;
import javax.swing.JFrame;

public class PDFReader extends JFrame
{
    private File currentDir;
    private JMenuItem saveAsImageMenuItem;
    private JMenuItem exitMenuItem;
    private JMenu fileMenu;
    private JMenuBar menuBar;
    private JMenuItem openMenuItem;
    private JMenuItem printMenuItem;
    private JMenu viewMenu;
    private JMenuItem nextPageItem;
    private JMenuItem previousPageItem;
    private JPanel documentPanel;
    private ReaderBottomPanel bottomStatusPanel;
    private PDDocument document;
    private List<PDPage> pages;
    private int currentPage;
    private int numberOfPages;
    private String currentFilename;
    private static final String PASSWORD = "-password";
    private static final String NONSEQ = "-nonSeq";
    private static boolean useNonSeqParser;
    
    public PDFReader() {
        this.currentDir = new File(".");
        this.documentPanel = new JPanel();
        this.bottomStatusPanel = new ReaderBottomPanel();
        this.document = null;
        this.pages = null;
        this.currentPage = 0;
        this.numberOfPages = 0;
        this.currentFilename = null;
        this.initComponents();
    }
    
    private void initComponents() {
        this.menuBar = new JMenuBar();
        this.fileMenu = new JMenu();
        this.openMenuItem = new JMenuItem();
        this.saveAsImageMenuItem = new JMenuItem();
        this.exitMenuItem = new JMenuItem();
        this.printMenuItem = new JMenuItem();
        this.viewMenu = new JMenu();
        this.nextPageItem = new JMenuItem();
        this.previousPageItem = new JMenuItem();
        this.setTitle("PDFBox - PDF Reader");
        this.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(final WindowEvent evt) {
                PDFReader.this.exitApplication();
            }
        });
        final JScrollPane documentScroller = new JScrollPane();
        documentScroller.setViewportView(this.documentPanel);
        this.getContentPane().add(documentScroller, "Center");
        this.getContentPane().add(this.bottomStatusPanel, "South");
        this.fileMenu.setText("File");
        this.openMenuItem.setText("Open");
        this.openMenuItem.setToolTipText("Open PDF file");
        this.openMenuItem.addActionListener(new ActionListener() {
            public void actionPerformed(final ActionEvent evt) {
                PDFReader.this.openMenuItemActionPerformed(evt);
            }
        });
        this.fileMenu.add(this.openMenuItem);
        this.printMenuItem.setText("Print");
        this.printMenuItem.addActionListener(new ActionListener() {
            public void actionPerformed(final ActionEvent evt) {
                try {
                    if (PDFReader.this.document != null) {
                        PDFReader.this.document.print();
                    }
                }
                catch (PrinterException e) {
                    e.printStackTrace();
                }
            }
        });
        this.fileMenu.add(this.printMenuItem);
        this.saveAsImageMenuItem.setText("Save as image");
        this.saveAsImageMenuItem.addActionListener(new ActionListener() {
            public void actionPerformed(final ActionEvent evt) {
                if (PDFReader.this.document != null) {
                    PDFReader.this.saveImage();
                }
            }
        });
        this.fileMenu.add(this.saveAsImageMenuItem);
        this.exitMenuItem.setText("Exit");
        this.exitMenuItem.addActionListener(new ActionListener() {
            public void actionPerformed(final ActionEvent evt) {
                PDFReader.this.exitApplication();
            }
        });
        this.fileMenu.add(this.exitMenuItem);
        this.menuBar.add(this.fileMenu);
        this.viewMenu.setText("View");
        this.nextPageItem.setText("Next page");
        this.nextPageItem.setAccelerator(KeyStroke.getKeyStroke('+'));
        this.nextPageItem.addActionListener(new ActionListener() {
            public void actionPerformed(final ActionEvent evt) {
                PDFReader.this.nextPage();
            }
        });
        this.viewMenu.add(this.nextPageItem);
        this.previousPageItem.setText("Previous page");
        this.previousPageItem.setAccelerator(KeyStroke.getKeyStroke('-'));
        this.previousPageItem.addActionListener(new ActionListener() {
            public void actionPerformed(final ActionEvent evt) {
                PDFReader.this.previousPage();
            }
        });
        this.viewMenu.add(this.previousPageItem);
        this.menuBar.add(this.viewMenu);
        this.setJMenuBar(this.menuBar);
        final Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        this.setBounds((screenSize.width - 700) / 2, (screenSize.height - 600) / 2, 700, 600);
    }
    
    private void updateTitle() {
        this.setTitle("PDFBox - " + this.currentFilename + " (" + (this.currentPage + 1) + "/" + this.numberOfPages + ")");
    }
    
    private void nextPage() {
        if (this.currentPage < this.numberOfPages - 1) {
            ++this.currentPage;
            this.updateTitle();
            this.showPage(this.currentPage);
        }
    }
    
    private void previousPage() {
        if (this.currentPage > 0) {
            --this.currentPage;
            this.updateTitle();
            this.showPage(this.currentPage);
        }
    }
    
    private void openMenuItemActionPerformed(final ActionEvent evt) {
        final JFileChooser chooser = new JFileChooser();
        chooser.setCurrentDirectory(this.currentDir);
        final ExtensionFileFilter pdfFilter = new ExtensionFileFilter(new String[] { "PDF" }, "PDF Files");
        chooser.setFileFilter(pdfFilter);
        final int result = chooser.showOpenDialog(this);
        if (result == 0) {
            final String name = chooser.getSelectedFile().getPath();
            this.currentDir = new File(name).getParentFile();
            try {
                this.openPDFFile(name, "");
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    
    private void exitApplication() {
        try {
            if (this.document != null) {
                this.document.close();
            }
        }
        catch (IOException ex) {}
        this.setVisible(false);
        this.dispose();
    }
    
    public static void main(final String[] args) throws Exception {
        final PDFReader viewer = new PDFReader();
        String password = "";
        String filename = null;
        for (int i = 0; i < args.length; ++i) {
            if (args[i].equals("-password")) {
                if (++i >= args.length) {
                    usage();
                }
                password = args[i];
            }
            if (args[i].equals("-nonSeq")) {
                PDFReader.useNonSeqParser = true;
            }
            else {
                filename = args[i];
            }
        }
        if (filename != null) {
            viewer.openPDFFile(filename, password);
        }
        viewer.setVisible(true);
    }
    
    private void openPDFFile(final String filename, final String password) throws Exception {
        if (this.document != null) {
            this.document.close();
            this.documentPanel.removeAll();
        }
        final File file = new File(filename);
        this.parseDocument(file, password);
        this.pages = (List<PDPage>)this.document.getDocumentCatalog().getAllPages();
        this.numberOfPages = this.pages.size();
        this.currentFilename = file.getAbsolutePath();
        this.currentPage = 0;
        this.updateTitle();
        this.showPage(0);
    }
    
    private void showPage(final int pageNumber) {
        try {
            final PageWrapper wrapper = new PageWrapper(this);
            wrapper.displayPage(this.pages.get(pageNumber));
            if (this.documentPanel.getComponentCount() > 0) {
                this.documentPanel.remove(0);
            }
            this.documentPanel.add(wrapper.getPanel());
            this.pack();
        }
        catch (IOException exception) {
            exception.printStackTrace();
        }
    }
    
    private void saveImage() {
        try {
            final PDPage pageToSave = this.pages.get(this.currentPage);
            final BufferedImage pageAsImage = pageToSave.convertToImage();
            String imageFilename = this.currentFilename;
            if (imageFilename.toLowerCase().endsWith(".pdf")) {
                imageFilename = imageFilename.substring(0, imageFilename.length() - 4);
            }
            imageFilename = imageFilename + "_" + (this.currentPage + 1);
            ImageIOUtil.writeImage(pageAsImage, "png", imageFilename, 8, 300);
        }
        catch (IOException exception) {
            exception.printStackTrace();
        }
    }
    
    private void parseDocument(final File file, final String password) throws IOException {
        this.document = null;
        if (PDFReader.useNonSeqParser) {
            this.document = PDDocument.loadNonSeq(file, null, password);
        }
        else {
            this.document = PDDocument.load(file);
            if (this.document.isEncrypted()) {
                try {
                    this.document.decrypt(password);
                }
                catch (InvalidPasswordException e2) {
                    System.err.println("Error: The document is encrypted.");
                }
                catch (CryptographyException e) {
                    e.printStackTrace();
                }
            }
        }
    }
    
    public ReaderBottomPanel getBottomStatusPanel() {
        return this.bottomStatusPanel;
    }
    
    private static void usage() {
        System.err.println("usage: java -jar pdfbox-app-x.y.z.jar PDFReader [OPTIONS] <input-file>\n  -password <password>      Password to decrypt the document\n  -nonSeq                   Enables the new non-sequential parser\n  <input-file>              The PDF document to be loaded\n");
    }
    
    static {
        PDFReader.useNonSeqParser = false;
    }
}
