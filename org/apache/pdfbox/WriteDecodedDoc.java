// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox;

import java.util.Iterator;
import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSStream;
import org.apache.pdfbox.cos.COSObject;
import org.apache.pdfbox.exceptions.CryptographyException;
import org.apache.pdfbox.exceptions.InvalidPasswordException;
import org.apache.pdfbox.io.RandomAccess;
import org.apache.pdfbox.pdmodel.PDDocument;
import java.io.File;
import org.apache.pdfbox.exceptions.COSVisitorException;
import java.io.IOException;

public class WriteDecodedDoc
{
    private static final String PASSWORD = "-password";
    private static final String NONSEQ = "-nonSeq";
    
    @Deprecated
    public void doIt(final String in, final String out) throws IOException, COSVisitorException {
        this.doIt(in, out, "", false);
    }
    
    public void doIt(final String in, final String out, final String password, final boolean useNonSeqParser) throws IOException, COSVisitorException {
        PDDocument doc = null;
        try {
            if (useNonSeqParser) {
                doc = PDDocument.loadNonSeq(new File(in), null, password);
                doc.setAllSecurityToBeRemoved(true);
            }
            else {
                doc = PDDocument.load(in);
                if (doc.isEncrypted()) {
                    try {
                        doc.decrypt(password);
                        doc.setAllSecurityToBeRemoved(true);
                    }
                    catch (InvalidPasswordException e2) {
                        if (password.trim().length() == 0) {
                            System.err.println("Password needed!!");
                        }
                        else {
                            System.err.println("Wrong password!!");
                        }
                        return;
                    }
                    catch (CryptographyException e) {
                        e.printStackTrace();
                        return;
                    }
                }
            }
            final Iterator<COSObject> i = doc.getDocument().getObjects().iterator();
            while (i.hasNext()) {
                final COSBase base = i.next().getObject();
                if (base instanceof COSStream) {
                    final COSStream cosStream = (COSStream)base;
                    cosStream.getUnfilteredStream();
                    cosStream.setFilters(null);
                }
            }
            doc.save(out);
        }
        finally {
            if (doc != null) {
                doc.close();
            }
        }
    }
    
    public static void main(final String[] args) {
        final WriteDecodedDoc app = new WriteDecodedDoc();
        String password = "";
        boolean useNonSeqParser = false;
        String pdfFile = null;
        String outputFile = null;
        for (int i = 0; i < args.length; ++i) {
            if (args[i].equals("-password")) {
                if (++i >= args.length) {
                    usage();
                }
                password = args[i];
            }
            else if (args[i].equals("-nonSeq")) {
                useNonSeqParser = true;
            }
            else if (pdfFile == null) {
                pdfFile = args[i];
            }
            else {
                outputFile = args[i];
            }
        }
        if (pdfFile == null) {
            usage();
        }
        else {
            try {
                if (outputFile == null) {
                    outputFile = calculateOutputFilename(pdfFile);
                }
                app.doIt(pdfFile, outputFile, password, useNonSeqParser);
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    
    private static String calculateOutputFilename(final String filename) {
        String outputFilename;
        if (filename.toLowerCase().endsWith(".pdf")) {
            outputFilename = filename.substring(0, filename.length() - 4);
        }
        else {
            outputFilename = filename;
        }
        outputFilename += "_unc.pdf";
        return outputFilename;
    }
    
    private static void usage() {
        System.err.println("usage: java -jar pdfbox-app-x.y.z.jar WriteDecodedDoc [OPTIONS] <input-file> [output-file]\n  -password <password>      Password to decrypt the document\n  -nonSeq                   Enables the new non-sequential parser\n  <input-file>              The PDF document to be decompressed\n  [output-file]             The filename for the decompressed pdf\n");
    }
}
