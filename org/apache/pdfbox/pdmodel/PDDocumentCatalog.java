// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel;

import org.apache.pdfbox.pdmodel.graphics.optionalcontent.PDOptionalContentProperties;
import org.apache.pdfbox.pdmodel.common.PDPageLabels;
import org.apache.pdfbox.pdmodel.documentinterchange.logicalstructure.PDStructureTreeRoot;
import org.apache.pdfbox.pdmodel.interactive.action.type.PDURIDictionary;
import org.apache.pdfbox.pdmodel.documentinterchange.logicalstructure.PDMarkInfo;
import org.apache.pdfbox.pdmodel.interactive.action.PDDocumentCatalogAdditionalActions;
import org.apache.pdfbox.pdmodel.interactive.documentnavigation.destination.PDDestination;
import java.io.IOException;
import org.apache.pdfbox.pdmodel.interactive.action.PDActionFactory;
import org.apache.pdfbox.pdmodel.common.PDDestinationOrAction;
import org.apache.pdfbox.cos.COSStream;
import org.apache.pdfbox.pdmodel.common.PDMetadata;
import org.apache.pdfbox.pdmodel.common.COSArrayList;
import org.apache.pdfbox.pdmodel.interactive.pagenavigation.PDThread;
import org.apache.pdfbox.cos.COSArray;
import org.apache.pdfbox.pdmodel.interactive.documentnavigation.outline.PDDocumentOutline;
import org.apache.pdfbox.pdmodel.interactive.viewerpreferences.PDViewerPreferences;
import java.util.ArrayList;
import java.util.List;
import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.pdmodel.interactive.form.PDAcroForm;
import org.apache.pdfbox.cos.COSDictionary;
import org.apache.pdfbox.pdmodel.common.COSObjectable;

public class PDDocumentCatalog implements COSObjectable
{
    private COSDictionary root;
    private PDDocument document;
    private PDAcroForm acroForm;
    public static final String PAGE_MODE_USE_NONE = "UseNone";
    public static final String PAGE_MODE_USE_OUTLINES = "UseOutlines";
    public static final String PAGE_MODE_USE_THUMBS = "UseThumbs";
    public static final String PAGE_MODE_FULL_SCREEN = "FullScreen";
    public static final String PAGE_MODE_USE_OPTIONAL_CONTENT = "UseOC";
    public static final String PAGE_MODE_USE_ATTACHMENTS = "UseAttachments";
    public static final String PAGE_LAYOUT_SINGLE_PAGE = "SinglePage";
    public static final String PAGE_LAYOUT_ONE_COLUMN = "OneColumn";
    public static final String PAGE_LAYOUT_TWO_COLUMN_LEFT = "TwoColumnLeft";
    public static final String PAGE_LAYOUT_TWO_COLUMN_RIGHT = "TwoColumnRight";
    public static final String PAGE_LAYOUT_TWO_PAGE_LEFT = "TwoPageLeft";
    public static final String PAGE_LAYOUT_TWO_PAGE_RIGHT = "TwoPageRight";
    
    public PDDocumentCatalog(final PDDocument doc) {
        this.acroForm = null;
        this.document = doc;
        (this.root = new COSDictionary()).setItem(COSName.TYPE, COSName.CATALOG);
        this.document.getDocument().getTrailer().setItem(COSName.ROOT, this.root);
    }
    
    public PDDocumentCatalog(final PDDocument doc, final COSDictionary rootDictionary) {
        this.acroForm = null;
        this.document = doc;
        this.root = rootDictionary;
    }
    
    public COSBase getCOSObject() {
        return this.root;
    }
    
    public COSDictionary getCOSDictionary() {
        return this.root;
    }
    
    public PDAcroForm getAcroForm() {
        if (this.acroForm == null) {
            final COSDictionary acroFormDic = (COSDictionary)this.root.getDictionaryObject(COSName.ACRO_FORM);
            if (acroFormDic != null) {
                this.acroForm = new PDAcroForm(this.document, acroFormDic);
            }
        }
        return this.acroForm;
    }
    
    public void setAcroForm(final PDAcroForm acro) {
        this.root.setItem(COSName.ACRO_FORM, acro);
    }
    
    public PDPageNode getPages() {
        return new PDPageNode((COSDictionary)this.root.getDictionaryObject(COSName.PAGES));
    }
    
    public List getAllPages() {
        final List retval = new ArrayList();
        final PDPageNode rootNode = this.getPages();
        rootNode.getAllKids(retval);
        return retval;
    }
    
    public PDViewerPreferences getViewerPreferences() {
        PDViewerPreferences retval = null;
        final COSDictionary dict = (COSDictionary)this.root.getDictionaryObject(COSName.VIEWER_PREFERENCES);
        if (dict != null) {
            retval = new PDViewerPreferences(dict);
        }
        return retval;
    }
    
    public void setViewerPreferences(final PDViewerPreferences prefs) {
        this.root.setItem(COSName.VIEWER_PREFERENCES, prefs);
    }
    
    public PDDocumentOutline getDocumentOutline() {
        PDDocumentOutline retval = null;
        final COSDictionary dict = (COSDictionary)this.root.getDictionaryObject(COSName.OUTLINES);
        if (dict != null) {
            retval = new PDDocumentOutline(dict);
        }
        return retval;
    }
    
    public void setDocumentOutline(final PDDocumentOutline outlines) {
        this.root.setItem(COSName.OUTLINES, outlines);
    }
    
    public List getThreads() {
        COSArray array = (COSArray)this.root.getDictionaryObject(COSName.THREADS);
        if (array == null) {
            array = new COSArray();
            this.root.setItem(COSName.THREADS, array);
        }
        final List pdObjects = new ArrayList();
        for (int i = 0; i < array.size(); ++i) {
            pdObjects.add(new PDThread((COSDictionary)array.getObject(i)));
        }
        return new COSArrayList(pdObjects, array);
    }
    
    public void setThreads(final List threads) {
        this.root.setItem(COSName.THREADS, COSArrayList.converterToCOSArray(threads));
    }
    
    public PDMetadata getMetadata() {
        PDMetadata retval = null;
        final COSStream stream = (COSStream)this.root.getDictionaryObject(COSName.METADATA);
        if (stream != null) {
            retval = new PDMetadata(stream);
        }
        return retval;
    }
    
    public void setMetadata(final PDMetadata meta) {
        this.root.setItem(COSName.METADATA, meta);
    }
    
    public void setOpenAction(final PDDestinationOrAction action) {
        this.root.setItem(COSName.OPEN_ACTION, action);
    }
    
    public PDDestinationOrAction getOpenAction() throws IOException {
        PDDestinationOrAction action = null;
        final COSBase actionObj = this.root.getDictionaryObject(COSName.OPEN_ACTION);
        if (actionObj != null) {
            if (actionObj instanceof COSDictionary) {
                action = PDActionFactory.createAction((COSDictionary)actionObj);
            }
            else {
                if (!(actionObj instanceof COSArray)) {
                    throw new IOException("Unknown OpenAction " + actionObj);
                }
                action = PDDestination.create(actionObj);
            }
        }
        return action;
    }
    
    public PDDocumentCatalogAdditionalActions getActions() {
        COSDictionary addAct = (COSDictionary)this.root.getDictionaryObject(COSName.AA);
        if (addAct == null) {
            addAct = new COSDictionary();
            this.root.setItem(COSName.AA, addAct);
        }
        return new PDDocumentCatalogAdditionalActions(addAct);
    }
    
    public void setActions(final PDDocumentCatalogAdditionalActions actions) {
        this.root.setItem(COSName.AA, actions);
    }
    
    public PDDocumentNameDictionary getNames() {
        PDDocumentNameDictionary nameDic = null;
        final COSDictionary names = (COSDictionary)this.root.getDictionaryObject(COSName.NAMES);
        if (names != null) {
            nameDic = new PDDocumentNameDictionary(this, names);
        }
        return nameDic;
    }
    
    public void setNames(final PDDocumentNameDictionary names) {
        this.root.setItem(COSName.NAMES, names);
    }
    
    public PDMarkInfo getMarkInfo() {
        PDMarkInfo retval = null;
        final COSDictionary dic = (COSDictionary)this.root.getDictionaryObject(COSName.MARK_INFO);
        if (dic != null) {
            retval = new PDMarkInfo(dic);
        }
        return retval;
    }
    
    public void setMarkInfo(final PDMarkInfo markInfo) {
        this.root.setItem(COSName.MARK_INFO, markInfo);
    }
    
    public String getPageMode() {
        return this.root.getNameAsString(COSName.PAGE_MODE, "UseNone");
    }
    
    public void setPageMode(final String mode) {
        this.root.setName(COSName.PAGE_MODE, mode);
    }
    
    public String getPageLayout() {
        return this.root.getNameAsString(COSName.PAGE_LAYOUT, "SinglePage");
    }
    
    public void setPageLayout(final String layout) {
        this.root.setName(COSName.PAGE_LAYOUT, layout);
    }
    
    public PDURIDictionary getURI() {
        PDURIDictionary retval = null;
        final COSDictionary uri = (COSDictionary)this.root.getDictionaryObject(COSName.URI);
        if (uri != null) {
            retval = new PDURIDictionary(uri);
        }
        return retval;
    }
    
    public void setURI(final PDURIDictionary uri) {
        this.root.setItem(COSName.URI, uri);
    }
    
    public PDStructureTreeRoot getStructureTreeRoot() {
        PDStructureTreeRoot treeRoot = null;
        final COSDictionary dic = (COSDictionary)this.root.getDictionaryObject(COSName.STRUCT_TREE_ROOT);
        if (dic != null) {
            treeRoot = new PDStructureTreeRoot(dic);
        }
        return treeRoot;
    }
    
    public void setStructureTreeRoot(final PDStructureTreeRoot treeRoot) {
        this.root.setItem(COSName.STRUCT_TREE_ROOT, treeRoot);
    }
    
    public String getLanguage() {
        return this.root.getString(COSName.LANG);
    }
    
    public void setLanguage(final String language) {
        this.root.setString(COSName.LANG, language);
    }
    
    public String getVersion() {
        return this.root.getNameAsString(COSName.VERSION);
    }
    
    public void setVersion(final String version) {
        this.root.setName(COSName.VERSION, version);
    }
    
    public PDPageLabels getPageLabels() throws IOException {
        PDPageLabels labels = null;
        final COSDictionary dict = (COSDictionary)this.root.getDictionaryObject(COSName.PAGE_LABELS);
        if (dict != null) {
            labels = new PDPageLabels(this.document, dict);
        }
        return labels;
    }
    
    public void setPageLabels(final PDPageLabels labels) {
        this.root.setItem(COSName.PAGE_LABELS, labels);
    }
    
    public PDOptionalContentProperties getOCProperties() {
        PDOptionalContentProperties retval = null;
        final COSDictionary dict = (COSDictionary)this.root.getDictionaryObject(COSName.OCPROPERTIES);
        if (dict != null) {
            retval = new PDOptionalContentProperties(dict);
        }
        return retval;
    }
    
    public void setOCProperties(final PDOptionalContentProperties ocProperties) {
        this.root.setItem(COSName.OCPROPERTIES, ocProperties);
    }
}
