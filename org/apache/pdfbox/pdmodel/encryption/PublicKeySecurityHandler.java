// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.encryption;

import org.bouncycastle.asn1.cms.RecipientIdentifier;
import org.bouncycastle.asn1.cms.IssuerAndSerialNumber;
import org.bouncycastle.asn1.x509.TBSCertificateStructure;
import java.security.GeneralSecurityException;
import org.bouncycastle.asn1.cms.KeyTransRecipientInfo;
import java.security.AlgorithmParameters;
import org.bouncycastle.asn1.cms.ContentInfo;
import org.bouncycastle.asn1.ASN1Set;
import org.bouncycastle.asn1.cms.OriginatorInfo;
import org.bouncycastle.asn1.cms.EnvelopedData;
import org.bouncycastle.asn1.ASN1OctetString;
import org.bouncycastle.asn1.cms.EncryptedContentInfo;
import org.bouncycastle.asn1.pkcs.PKCSObjectIdentifiers;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import org.bouncycastle.asn1.DERObjectIdentifier;
import org.bouncycastle.asn1.DEREncodable;
import org.bouncycastle.asn1.DERSet;
import org.bouncycastle.asn1.cms.RecipientInfo;
import org.bouncycastle.asn1.DEROctetString;
import java.security.Key;
import javax.crypto.Cipher;
import java.io.InputStream;
import org.bouncycastle.asn1.ASN1InputStream;
import java.io.ByteArrayInputStream;
import java.security.AlgorithmParameterGenerator;
import org.bouncycastle.asn1.DERObject;
import java.security.cert.X509Certificate;
import javax.crypto.SecretKey;
import java.io.OutputStream;
import org.bouncycastle.asn1.DEROutputStream;
import java.io.ByteArrayOutputStream;
import java.security.SecureRandom;
import javax.crypto.KeyGenerator;
import java.security.Provider;
import java.security.Security;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import java.util.Iterator;
import org.apache.pdfbox.cos.COSString;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.KeyStoreException;
import org.bouncycastle.cms.CMSException;
import java.security.MessageDigest;
import java.security.cert.Certificate;
import org.bouncycastle.cms.RecipientInformation;
import org.bouncycastle.cms.CMSEnvelopedData;
import org.apache.pdfbox.cos.COSArray;
import java.io.IOException;
import org.apache.pdfbox.exceptions.CryptographyException;
import org.apache.pdfbox.pdmodel.PDDocument;

public class PublicKeySecurityHandler extends SecurityHandler
{
    public static final String FILTER = "Adobe.PubSec";
    private static final String SUBFILTER = "adbe.pkcs7.s4";
    private PublicKeyProtectionPolicy policy;
    
    public PublicKeySecurityHandler() {
        this.policy = null;
    }
    
    public PublicKeySecurityHandler(final PublicKeyProtectionPolicy p) {
        this.policy = null;
        this.policy = p;
        this.keyLength = this.policy.getEncryptionKeyLength();
    }
    
    @Override
    public void decryptDocument(final PDDocument doc, final DecryptionMaterial decryptionMaterial) throws CryptographyException, IOException {
        this.document = doc;
        final PDEncryptionDictionary dictionary = doc.getEncryptionDictionary();
        this.prepareForDecryption(dictionary, doc.getDocument().getDocumentID(), decryptionMaterial);
        this.proceedDecryption();
    }
    
    @Override
    public void prepareForDecryption(final PDEncryptionDictionary encDictionary, final COSArray documentIDArray, final DecryptionMaterial decryptionMaterial) throws CryptographyException, IOException {
        if (encDictionary.getLength() != 0) {
            this.keyLength = encDictionary.getLength();
        }
        if (!(decryptionMaterial instanceof PublicKeyDecryptionMaterial)) {
            throw new CryptographyException("Provided decryption material is not compatible with the document");
        }
        final PublicKeyDecryptionMaterial material = (PublicKeyDecryptionMaterial)decryptionMaterial;
        try {
            boolean foundRecipient = false;
            byte[] envelopedData = null;
            final byte[][] recipientFieldsBytes = new byte[encDictionary.getRecipientsLength()][];
            int recipientFieldsLength = 0;
            for (int i = 0; i < encDictionary.getRecipientsLength(); ++i) {
                final COSString recipientFieldString = encDictionary.getRecipientStringAt(i);
                final byte[] recipientBytes = recipientFieldString.getBytes();
                final CMSEnvelopedData data = new CMSEnvelopedData(recipientBytes);
                for (final RecipientInformation ri : data.getRecipientInfos().getRecipients()) {
                    if (ri.getRID().match(material.getCertificate()) && !foundRecipient) {
                        foundRecipient = true;
                        envelopedData = ri.getContent(material.getPrivateKey(), "BC");
                    }
                }
                recipientFieldsBytes[i] = recipientBytes;
                recipientFieldsLength += recipientBytes.length;
            }
            if (!foundRecipient || envelopedData == null) {
                throw new CryptographyException("The certificate matches no recipient entry");
            }
            if (envelopedData.length != 24) {
                throw new CryptographyException("The enveloped data does not contain 24 bytes");
            }
            final byte[] accessBytes = new byte[4];
            System.arraycopy(envelopedData, 20, accessBytes, 0, 4);
            (this.currentAccessPermission = new AccessPermission(accessBytes)).setReadOnly();
            final byte[] sha1Input = new byte[recipientFieldsLength + 20];
            System.arraycopy(envelopedData, 0, sha1Input, 0, 20);
            int sha1InputOffset = 20;
            for (int j = 0; j < recipientFieldsBytes.length; ++j) {
                System.arraycopy(recipientFieldsBytes[j], 0, sha1Input, sha1InputOffset, recipientFieldsBytes[j].length);
                sha1InputOffset += recipientFieldsBytes[j].length;
            }
            final MessageDigest md = MessageDigest.getInstance("SHA-1");
            final byte[] mdResult = md.digest(sha1Input);
            System.arraycopy(mdResult, 0, this.encryptionKey = new byte[this.keyLength / 8], 0, this.keyLength / 8);
        }
        catch (CMSException e) {
            throw new CryptographyException(e);
        }
        catch (KeyStoreException e2) {
            throw new CryptographyException(e2);
        }
        catch (NoSuchProviderException e3) {
            throw new CryptographyException(e3);
        }
        catch (NoSuchAlgorithmException e4) {
            throw new CryptographyException(e4);
        }
    }
    
    @Override
    public void prepareDocumentForEncryption(final PDDocument doc) throws CryptographyException {
        try {
            Security.addProvider(new BouncyCastleProvider());
            PDEncryptionDictionary dictionary = doc.getEncryptionDictionary();
            if (dictionary == null) {
                dictionary = new PDEncryptionDictionary();
            }
            dictionary.setFilter("Adobe.PubSec");
            dictionary.setLength(this.keyLength);
            dictionary.setVersion(2);
            dictionary.setSubFilter("adbe.pkcs7.s4");
            final byte[][] recipientsField = new byte[this.policy.getRecipientsNumber()][];
            final byte[] seed = new byte[20];
            final KeyGenerator key = KeyGenerator.getInstance("AES");
            key.init(192, new SecureRandom());
            final SecretKey sk = key.generateKey();
            System.arraycopy(sk.getEncoded(), 0, seed, 0, 20);
            final Iterator it = this.policy.getRecipientsIterator();
            int i = 0;
            while (it.hasNext()) {
                final PublicKeyRecipient recipient = it.next();
                final X509Certificate certificate = recipient.getX509();
                final int permission = recipient.getPermission().getPermissionBytesForPublicKey();
                final byte[] pkcs7input = new byte[24];
                final byte one = (byte)permission;
                final byte two = (byte)(permission >>> 8);
                final byte three = (byte)(permission >>> 16);
                final byte four = (byte)(permission >>> 24);
                System.arraycopy(seed, 0, pkcs7input, 0, 20);
                pkcs7input[20] = four;
                pkcs7input[21] = three;
                pkcs7input[22] = two;
                pkcs7input[23] = one;
                final DERObject obj = this.createDERForRecipient(pkcs7input, certificate);
                final ByteArrayOutputStream baos = new ByteArrayOutputStream();
                final DEROutputStream k = new DEROutputStream(baos);
                k.writeObject(obj);
                recipientsField[i] = baos.toByteArray();
                ++i;
            }
            dictionary.setRecipients(recipientsField);
            int sha1InputLength = seed.length;
            for (int j = 0; j < dictionary.getRecipientsLength(); ++j) {
                final COSString string = dictionary.getRecipientStringAt(j);
                sha1InputLength += string.getBytes().length;
            }
            final byte[] sha1Input = new byte[sha1InputLength];
            System.arraycopy(seed, 0, sha1Input, 0, 20);
            int sha1InputOffset = 20;
            for (int l = 0; l < dictionary.getRecipientsLength(); ++l) {
                final COSString string2 = dictionary.getRecipientStringAt(l);
                System.arraycopy(string2.getBytes(), 0, sha1Input, sha1InputOffset, string2.getBytes().length);
                sha1InputOffset += string2.getBytes().length;
            }
            final MessageDigest md = MessageDigest.getInstance("SHA-1");
            final byte[] mdResult = md.digest(sha1Input);
            System.arraycopy(mdResult, 0, this.encryptionKey = new byte[this.keyLength / 8], 0, this.keyLength / 8);
            doc.setEncryptionDictionary(dictionary);
            doc.getDocument().setEncryptionDictionary(dictionary.encryptionDictionary);
        }
        catch (NoSuchAlgorithmException ex) {
            throw new CryptographyException(ex);
        }
        catch (NoSuchProviderException ex2) {
            throw new CryptographyException(ex2);
        }
        catch (Exception e) {
            e.printStackTrace();
            throw new CryptographyException(e);
        }
    }
    
    private DERObject createDERForRecipient(final byte[] in, final X509Certificate cert) throws IOException, GeneralSecurityException {
        final String s = "1.2.840.113549.3.2";
        final AlgorithmParameterGenerator algorithmparametergenerator = AlgorithmParameterGenerator.getInstance(s);
        final AlgorithmParameters algorithmparameters = algorithmparametergenerator.generateParameters();
        final ByteArrayInputStream bytearrayinputstream = new ByteArrayInputStream(algorithmparameters.getEncoded("ASN.1"));
        final ASN1InputStream asn1inputstream = new ASN1InputStream(bytearrayinputstream);
        final DERObject derobject = asn1inputstream.readObject();
        final KeyGenerator keygenerator = KeyGenerator.getInstance(s);
        keygenerator.init(128);
        final SecretKey secretkey = keygenerator.generateKey();
        final Cipher cipher = Cipher.getInstance(s);
        cipher.init(1, secretkey, algorithmparameters);
        final byte[] abyte1 = cipher.doFinal(in);
        final DEROctetString deroctetstring = new DEROctetString(abyte1);
        final KeyTransRecipientInfo keytransrecipientinfo = this.computeRecipientInfo(cert, secretkey.getEncoded());
        final DERSet derset = new DERSet(new RecipientInfo(keytransrecipientinfo));
        final AlgorithmIdentifier algorithmidentifier = new AlgorithmIdentifier(new DERObjectIdentifier(s), derobject);
        final EncryptedContentInfo encryptedcontentinfo = new EncryptedContentInfo(PKCSObjectIdentifiers.data, algorithmidentifier, deroctetstring);
        final EnvelopedData env = new EnvelopedData(null, derset, encryptedcontentinfo, null);
        final ContentInfo contentinfo = new ContentInfo(PKCSObjectIdentifiers.envelopedData, env);
        return contentinfo.getDERObject();
    }
    
    private KeyTransRecipientInfo computeRecipientInfo(final X509Certificate x509certificate, final byte[] abyte0) throws GeneralSecurityException, IOException {
        final ASN1InputStream asn1inputstream = new ASN1InputStream(new ByteArrayInputStream(x509certificate.getTBSCertificate()));
        final TBSCertificateStructure tbscertificatestructure = TBSCertificateStructure.getInstance(asn1inputstream.readObject());
        final AlgorithmIdentifier algorithmidentifier = tbscertificatestructure.getSubjectPublicKeyInfo().getAlgorithmId();
        final IssuerAndSerialNumber issuerandserialnumber = new IssuerAndSerialNumber(tbscertificatestructure.getIssuer(), tbscertificatestructure.getSerialNumber().getValue());
        final Cipher cipher = Cipher.getInstance(algorithmidentifier.getObjectId().getId());
        cipher.init(1, x509certificate.getPublicKey());
        final DEROctetString deroctetstring = new DEROctetString(cipher.doFinal(abyte0));
        final RecipientIdentifier recipId = new RecipientIdentifier(issuerandserialnumber);
        return new KeyTransRecipientInfo(recipId, algorithmidentifier, deroctetstring);
    }
}
