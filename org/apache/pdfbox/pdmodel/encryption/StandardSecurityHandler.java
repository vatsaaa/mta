// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.encryption;

import java.io.InputStream;
import java.io.ByteArrayInputStream;
import java.io.OutputStream;
import java.io.ByteArrayOutputStream;
import java.security.NoSuchAlgorithmException;
import org.apache.pdfbox.cos.COSBase;
import java.math.BigInteger;
import java.security.MessageDigest;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.cos.COSString;
import java.io.IOException;
import org.apache.pdfbox.exceptions.CryptographyException;
import org.apache.pdfbox.cos.COSArray;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.encryption.ARCFour;

public class StandardSecurityHandler extends SecurityHandler
{
    public static final String FILTER = "Standard";
    private static final int DEFAULT_VERSION = 1;
    private static final int DEFAULT_REVISION = 3;
    private int revision;
    private StandardProtectionPolicy policy;
    private ARCFour rc4;
    public static final Class PROTECTION_POLICY_CLASS;
    public static final byte[] ENCRYPT_PADDING;
    
    public StandardSecurityHandler() {
        this.revision = 3;
        this.rc4 = new ARCFour();
    }
    
    public StandardSecurityHandler(final StandardProtectionPolicy p) {
        this.revision = 3;
        this.rc4 = new ARCFour();
        this.policy = p;
        this.keyLength = this.policy.getEncryptionKeyLength();
    }
    
    private int computeVersionNumber() {
        if (this.keyLength == 40) {
            return 1;
        }
        return 2;
    }
    
    private int computeRevisionNumber() {
        if (this.version == 2 && !this.policy.getPermissions().canFillInForm() && !this.policy.getPermissions().canExtractForAccessibility() && !this.policy.getPermissions().canPrintDegraded()) {
            return 2;
        }
        return 3;
    }
    
    @Override
    public void decryptDocument(final PDDocument doc, final DecryptionMaterial decryptionMaterial) throws CryptographyException, IOException {
        this.document = doc;
        final PDEncryptionDictionary dictionary = this.document.getEncryptionDictionary();
        final COSArray documentIDArray = this.document.getDocument().getDocumentID();
        this.prepareForDecryption(dictionary, documentIDArray, decryptionMaterial);
        this.proceedDecryption();
    }
    
    @Override
    public void prepareForDecryption(final PDEncryptionDictionary encDictionary, final COSArray documentIDArray, final DecryptionMaterial decryptionMaterial) throws CryptographyException, IOException {
        if (!(decryptionMaterial instanceof StandardDecryptionMaterial)) {
            throw new CryptographyException("Provided decryption material is not compatible with the document");
        }
        final StandardDecryptionMaterial material = (StandardDecryptionMaterial)decryptionMaterial;
        String password = material.getPassword();
        if (password == null) {
            password = "";
        }
        final int dicPermissions = encDictionary.getPermissions();
        final int dicRevision = encDictionary.getRevision();
        final int dicLength = encDictionary.getLength() / 8;
        byte[] documentIDBytes = null;
        if (documentIDArray != null && documentIDArray.size() >= 1) {
            final COSString id = (COSString)documentIDArray.getObject(0);
            documentIDBytes = id.getBytes();
        }
        else {
            documentIDBytes = new byte[0];
        }
        final boolean encryptMetadata = encDictionary.isEncryptMetaData();
        final byte[] u = encDictionary.getUserKey();
        final byte[] o = encDictionary.getOwnerKey();
        final boolean isUserPassword = this.isUserPassword(password.getBytes("ISO-8859-1"), u, o, dicPermissions, documentIDBytes, dicRevision, dicLength, encryptMetadata);
        final boolean isOwnerPassword = this.isOwnerPassword(password.getBytes("ISO-8859-1"), u, o, dicPermissions, documentIDBytes, dicRevision, dicLength, encryptMetadata);
        if (isUserPassword) {
            this.currentAccessPermission = new AccessPermission(dicPermissions);
            this.encryptionKey = this.computeEncryptedKey(password.getBytes("ISO-8859-1"), o, dicPermissions, documentIDBytes, dicRevision, dicLength, encryptMetadata);
        }
        else {
            if (!isOwnerPassword) {
                throw new CryptographyException("Error: The supplied password does not match either the owner or user password in the document.");
            }
            this.currentAccessPermission = AccessPermission.getOwnerAccessPermission();
            final byte[] computedUserPassword = this.getUserPassword(password.getBytes("ISO-8859-1"), o, dicRevision, dicLength);
            this.encryptionKey = this.computeEncryptedKey(computedUserPassword, o, dicPermissions, documentIDBytes, dicRevision, dicLength, encryptMetadata);
        }
        final PDCryptFilterDictionary stdCryptFilterDictionary = encDictionary.getStdCryptFilterDictionary();
        if (stdCryptFilterDictionary != null) {
            final COSName cryptFilterMethod = stdCryptFilterDictionary.getCryptFilterMethod();
            if (cryptFilterMethod != null) {
                this.setAES("AESV2".equalsIgnoreCase(cryptFilterMethod.getName()));
            }
        }
    }
    
    @Override
    public void prepareDocumentForEncryption(final PDDocument doc) throws CryptographyException, IOException {
        this.document = doc;
        PDEncryptionDictionary encryptionDictionary = this.document.getEncryptionDictionary();
        if (encryptionDictionary == null) {
            encryptionDictionary = new PDEncryptionDictionary();
        }
        this.version = this.computeVersionNumber();
        this.revision = this.computeRevisionNumber();
        encryptionDictionary.setFilter("Standard");
        encryptionDictionary.setVersion(this.version);
        encryptionDictionary.setRevision(this.revision);
        encryptionDictionary.setLength(this.keyLength);
        String ownerPassword = this.policy.getOwnerPassword();
        String userPassword = this.policy.getUserPassword();
        if (ownerPassword == null) {
            ownerPassword = "";
        }
        if (userPassword == null) {
            userPassword = "";
        }
        final int permissionInt = this.policy.getPermissions().getPermissionBytes();
        encryptionDictionary.setPermissions(permissionInt);
        final int length = this.keyLength / 8;
        COSArray idArray = this.document.getDocument().getDocumentID();
        if (idArray == null || idArray.size() < 2) {
            idArray = new COSArray();
            try {
                final MessageDigest md = MessageDigest.getInstance("MD5");
                final BigInteger time = BigInteger.valueOf(System.currentTimeMillis());
                md.update(time.toByteArray());
                md.update(ownerPassword.getBytes("ISO-8859-1"));
                md.update(userPassword.getBytes("ISO-8859-1"));
                md.update(this.document.getDocument().toString().getBytes());
                final byte[] id = md.digest(this.toString().getBytes("ISO-8859-1"));
                final COSString idString = new COSString();
                idString.append(id);
                idArray.add(idString);
                idArray.add(idString);
                this.document.getDocument().setDocumentID(idArray);
            }
            catch (NoSuchAlgorithmException e) {
                throw new CryptographyException(e);
            }
            catch (IOException e2) {
                throw new CryptographyException(e2);
            }
        }
        final COSString id2 = (COSString)idArray.getObject(0);
        final byte[] o = this.computeOwnerPassword(ownerPassword.getBytes("ISO-8859-1"), userPassword.getBytes("ISO-8859-1"), this.revision, length);
        final byte[] u = this.computeUserPassword(userPassword.getBytes("ISO-8859-1"), o, permissionInt, id2.getBytes(), this.revision, length, true);
        this.encryptionKey = this.computeEncryptedKey(userPassword.getBytes("ISO-8859-1"), o, permissionInt, id2.getBytes(), this.revision, length, true);
        encryptionDictionary.setOwnerKey(o);
        encryptionDictionary.setUserKey(u);
        this.document.setEncryptionDictionary(encryptionDictionary);
        this.document.getDocument().setEncryptionDictionary(encryptionDictionary.getCOSDictionary());
    }
    
    public final boolean isOwnerPassword(final byte[] ownerPassword, final byte[] u, final byte[] o, final int permissions, final byte[] id, final int encRevision, final int length, final boolean encryptMetadata) throws CryptographyException, IOException {
        final byte[] userPassword = this.getUserPassword(ownerPassword, o, encRevision, length);
        return this.isUserPassword(userPassword, u, o, permissions, id, encRevision, length, encryptMetadata);
    }
    
    public final byte[] getUserPassword(final byte[] ownerPassword, final byte[] o, final int encRevision, final long length) throws CryptographyException, IOException {
        try {
            final ByteArrayOutputStream result = new ByteArrayOutputStream();
            final byte[] ownerPadded = this.truncateOrPad(ownerPassword);
            final MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(ownerPadded);
            byte[] digest = md.digest();
            if (encRevision == 3 || encRevision == 4) {
                for (int i = 0; i < 50; ++i) {
                    md.reset();
                    md.update(digest);
                    digest = md.digest();
                }
            }
            if (encRevision == 2 && length != 5L) {
                throw new CryptographyException("Error: Expected length=5 actual=" + length);
            }
            final byte[] rc4Key = new byte[(int)length];
            System.arraycopy(digest, 0, rc4Key, 0, (int)length);
            if (encRevision == 2) {
                this.rc4.setKey(rc4Key);
                this.rc4.write(o, result);
            }
            else if (encRevision == 3 || encRevision == 4) {
                final byte[] iterationKey = new byte[rc4Key.length];
                byte[] otemp = new byte[o.length];
                System.arraycopy(o, 0, otemp, 0, o.length);
                this.rc4.write(o, result);
                for (int j = 19; j >= 0; --j) {
                    System.arraycopy(rc4Key, 0, iterationKey, 0, rc4Key.length);
                    for (int k = 0; k < iterationKey.length; ++k) {
                        iterationKey[k] ^= (byte)j;
                    }
                    this.rc4.setKey(iterationKey);
                    result.reset();
                    this.rc4.write(otemp, result);
                    otemp = result.toByteArray();
                }
            }
            return result.toByteArray();
        }
        catch (NoSuchAlgorithmException e) {
            throw new CryptographyException(e);
        }
    }
    
    public final byte[] computeEncryptedKey(final byte[] password, final byte[] o, final int permissions, final byte[] id, final int encRevision, final int length, final boolean encryptMetadata) throws CryptographyException {
        final byte[] result = new byte[length];
        try {
            final byte[] padded = this.truncateOrPad(password);
            final MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(padded);
            md.update(o);
            final byte zero = (byte)(permissions >>> 0);
            final byte one = (byte)(permissions >>> 8);
            final byte two = (byte)(permissions >>> 16);
            final byte three = (byte)(permissions >>> 24);
            md.update(zero);
            md.update(one);
            md.update(two);
            md.update(three);
            md.update(id);
            if (encRevision == 4 && !encryptMetadata) {
                md.update(new byte[] { -1, -1, -1, -1 });
            }
            byte[] digest = md.digest();
            if (encRevision == 3 || encRevision == 4) {
                for (int i = 0; i < 50; ++i) {
                    md.reset();
                    md.update(digest, 0, length);
                    digest = md.digest();
                }
            }
            if (encRevision == 2 && length != 5) {
                throw new CryptographyException("Error: length should be 5 when revision is two actual=" + length);
            }
            System.arraycopy(digest, 0, result, 0, length);
        }
        catch (NoSuchAlgorithmException e) {
            throw new CryptographyException(e);
        }
        return result;
    }
    
    public final byte[] computeUserPassword(final byte[] password, final byte[] o, final int permissions, final byte[] id, final int encRevision, final int length, final boolean encryptMetadata) throws CryptographyException, IOException {
        final ByteArrayOutputStream result = new ByteArrayOutputStream();
        final byte[] encryptionKey = this.computeEncryptedKey(password, o, permissions, id, encRevision, length, encryptMetadata);
        if (encRevision == 2) {
            this.rc4.setKey(encryptionKey);
            this.rc4.write(StandardSecurityHandler.ENCRYPT_PADDING, result);
        }
        else {
            if (encRevision != 3) {
                if (encRevision != 4) {
                    return result.toByteArray();
                }
            }
            try {
                final MessageDigest md = MessageDigest.getInstance("MD5");
                md.update(StandardSecurityHandler.ENCRYPT_PADDING);
                md.update(id);
                result.write(md.digest());
                final byte[] iterationKey = new byte[encryptionKey.length];
                for (int i = 0; i < 20; ++i) {
                    System.arraycopy(encryptionKey, 0, iterationKey, 0, iterationKey.length);
                    for (int j = 0; j < iterationKey.length; ++j) {
                        iterationKey[j] ^= (byte)i;
                    }
                    this.rc4.setKey(iterationKey);
                    final ByteArrayInputStream input = new ByteArrayInputStream(result.toByteArray());
                    result.reset();
                    this.rc4.write(input, result);
                }
                final byte[] finalResult = new byte[32];
                System.arraycopy(result.toByteArray(), 0, finalResult, 0, 16);
                System.arraycopy(StandardSecurityHandler.ENCRYPT_PADDING, 0, finalResult, 16, 16);
                result.reset();
                result.write(finalResult);
            }
            catch (NoSuchAlgorithmException e) {
                throw new CryptographyException(e);
            }
        }
        return result.toByteArray();
    }
    
    public final byte[] computeOwnerPassword(final byte[] ownerPassword, final byte[] userPassword, final int encRevision, final int length) throws CryptographyException, IOException {
        try {
            final byte[] ownerPadded = this.truncateOrPad(ownerPassword);
            final MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(ownerPadded);
            byte[] digest = md.digest();
            if (encRevision == 3 || encRevision == 4) {
                for (int i = 0; i < 50; ++i) {
                    md.reset();
                    md.update(digest, 0, length);
                    digest = md.digest();
                }
            }
            if (encRevision == 2 && length != 5) {
                throw new CryptographyException("Error: Expected length=5 actual=" + length);
            }
            final byte[] rc4Key = new byte[length];
            System.arraycopy(digest, 0, rc4Key, 0, length);
            final byte[] paddedUser = this.truncateOrPad(userPassword);
            this.rc4.setKey(rc4Key);
            final ByteArrayOutputStream crypted = new ByteArrayOutputStream();
            this.rc4.write(new ByteArrayInputStream(paddedUser), crypted);
            if (encRevision == 3 || encRevision == 4) {
                final byte[] iterationKey = new byte[rc4Key.length];
                for (int j = 1; j < 20; ++j) {
                    System.arraycopy(rc4Key, 0, iterationKey, 0, rc4Key.length);
                    for (int k = 0; k < iterationKey.length; ++k) {
                        iterationKey[k] ^= (byte)j;
                    }
                    this.rc4.setKey(iterationKey);
                    final ByteArrayInputStream input = new ByteArrayInputStream(crypted.toByteArray());
                    crypted.reset();
                    this.rc4.write(input, crypted);
                }
            }
            return crypted.toByteArray();
        }
        catch (NoSuchAlgorithmException e) {
            throw new CryptographyException(e.getMessage());
        }
    }
    
    private final byte[] truncateOrPad(final byte[] password) {
        final byte[] padded = new byte[StandardSecurityHandler.ENCRYPT_PADDING.length];
        final int bytesBeforePad = Math.min(password.length, padded.length);
        System.arraycopy(password, 0, padded, 0, bytesBeforePad);
        System.arraycopy(StandardSecurityHandler.ENCRYPT_PADDING, 0, padded, bytesBeforePad, StandardSecurityHandler.ENCRYPT_PADDING.length - bytesBeforePad);
        return padded;
    }
    
    public final boolean isUserPassword(final byte[] password, final byte[] u, final byte[] o, final int permissions, final byte[] id, final int encRevision, final int length, final boolean encryptMetadata) throws CryptographyException, IOException {
        boolean matches = false;
        final byte[] computedValue = this.computeUserPassword(password, o, permissions, id, encRevision, length, encryptMetadata);
        if (encRevision == 2) {
            matches = arraysEqual(u, computedValue);
        }
        else {
            if (encRevision != 3 && encRevision != 4) {
                throw new IOException("Unknown Encryption Revision " + encRevision);
            }
            matches = arraysEqual(u, computedValue, 16);
        }
        return matches;
    }
    
    public final boolean isUserPassword(final String password, final byte[] u, final byte[] o, final int permissions, final byte[] id, final int encRevision, final int length, final boolean encryptMetadata) throws CryptographyException, IOException {
        return this.isUserPassword(password.getBytes("ISO-8859-1"), u, o, permissions, id, encRevision, length, encryptMetadata);
    }
    
    public final boolean isOwnerPassword(final String password, final byte[] u, final byte[] o, final int permissions, final byte[] id, final int encRevision, final int length, final boolean encryptMetadata) throws CryptographyException, IOException {
        return this.isOwnerPassword(password.getBytes("ISO-8859-1"), u, o, permissions, id, encRevision, length, encryptMetadata);
    }
    
    private static final boolean arraysEqual(final byte[] first, final byte[] second, final int count) {
        boolean equal = first.length >= count && second.length >= count;
        for (int i = 0; i < count && equal; equal = (first[i] == second[i]), ++i) {}
        return equal;
    }
    
    private static final boolean arraysEqual(final byte[] first, final byte[] second) {
        boolean equal = first.length == second.length;
        for (int i = 0; i < first.length && equal; equal = (first[i] == second[i]), ++i) {}
        return equal;
    }
    
    static {
        PROTECTION_POLICY_CLASS = StandardProtectionPolicy.class;
        ENCRYPT_PADDING = new byte[] { 40, -65, 78, 94, 78, 117, -118, 65, 100, 0, 78, 86, -1, -6, 1, 8, 46, 46, 0, -74, -48, 104, 62, -128, 47, 12, -87, -2, 100, 83, 105, 122 };
    }
}
