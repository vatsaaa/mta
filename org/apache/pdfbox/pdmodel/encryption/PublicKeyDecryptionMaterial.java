// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.encryption;

import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.Key;
import java.util.Enumeration;
import java.security.KeyStoreException;
import java.security.cert.X509Certificate;
import java.security.KeyStore;

public class PublicKeyDecryptionMaterial extends DecryptionMaterial
{
    private String password;
    private KeyStore keyStore;
    private String alias;
    
    public PublicKeyDecryptionMaterial(final KeyStore keystore, final String a, final String pwd) {
        this.password = null;
        this.keyStore = null;
        this.alias = null;
        this.keyStore = keystore;
        this.alias = a;
        this.password = pwd;
    }
    
    public X509Certificate getCertificate() throws KeyStoreException {
        if (this.keyStore.size() == 1) {
            final Enumeration aliases = this.keyStore.aliases();
            final String keyStoreAlias = aliases.nextElement();
            return (X509Certificate)this.keyStore.getCertificate(keyStoreAlias);
        }
        if (this.keyStore.containsAlias(this.alias)) {
            return (X509Certificate)this.keyStore.getCertificate(this.alias);
        }
        throw new KeyStoreException("the keystore does not contain the given alias");
    }
    
    public String getPassword() {
        return this.password;
    }
    
    public Key getPrivateKey() throws KeyStoreException {
        try {
            if (this.keyStore.size() == 1) {
                final Enumeration aliases = this.keyStore.aliases();
                final String keyStoreAlias = aliases.nextElement();
                return this.keyStore.getKey(keyStoreAlias, this.password.toCharArray());
            }
            if (this.keyStore.containsAlias(this.alias)) {
                return this.keyStore.getKey(this.alias, this.password.toCharArray());
            }
            throw new KeyStoreException("the keystore does not contain the given alias");
        }
        catch (UnrecoverableKeyException ex) {
            throw new KeyStoreException("the private key is not recoverable");
        }
        catch (NoSuchAlgorithmException ex2) {
            throw new KeyStoreException("the algorithm necessary to recover the key is not available");
        }
    }
}
