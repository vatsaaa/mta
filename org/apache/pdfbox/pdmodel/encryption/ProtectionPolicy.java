// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.encryption;

public abstract class ProtectionPolicy
{
    private static final int DEFAULT_KEY_LENGTH = 40;
    private int encryptionKeyLength;
    
    public ProtectionPolicy() {
        this.encryptionKeyLength = 40;
    }
    
    public void setEncryptionKeyLength(final int l) {
        if (l != 40 && l != 128) {
            throw new RuntimeException("Invalid key length '" + l + "' value must be 40 or 128!");
        }
        this.encryptionKeyLength = l;
    }
    
    public int getEncryptionKeyLength() {
        return this.encryptionKeyLength;
    }
}
