// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.encryption;

public class StandardProtectionPolicy extends ProtectionPolicy
{
    private AccessPermission permissions;
    private String ownerPassword;
    private String userPassword;
    
    public StandardProtectionPolicy(final String ownerPass, final String userPass, final AccessPermission perms) {
        this.ownerPassword = "";
        this.userPassword = "";
        this.permissions = perms;
        this.userPassword = userPass;
        this.ownerPassword = ownerPass;
    }
    
    public AccessPermission getPermissions() {
        return this.permissions;
    }
    
    public void setPermissions(final AccessPermission perms) {
        this.permissions = perms;
    }
    
    public String getOwnerPassword() {
        return this.ownerPassword;
    }
    
    public void setOwnerPassword(final String ownerPass) {
        this.ownerPassword = ownerPass;
    }
    
    public String getUserPassword() {
        return this.userPassword;
    }
    
    public void setUserPassword(final String userPass) {
        this.userPassword = userPass;
    }
}
