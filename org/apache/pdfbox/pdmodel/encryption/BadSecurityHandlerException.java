// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.encryption;

public class BadSecurityHandlerException extends Exception
{
    public BadSecurityHandlerException() {
    }
    
    public BadSecurityHandlerException(final Exception e) {
        super(e);
    }
    
    public BadSecurityHandlerException(final String msg) {
        super(msg);
    }
}
