// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.encryption;

public class StandardDecryptionMaterial extends DecryptionMaterial
{
    private String password;
    
    public StandardDecryptionMaterial(final String pwd) {
        this.password = null;
        this.password = pwd;
    }
    
    public String getPassword() {
        return this.password;
    }
}
