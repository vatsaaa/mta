// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.encryption;

import java.util.Iterator;
import java.security.cert.X509Certificate;
import java.util.ArrayList;

public class PublicKeyProtectionPolicy extends ProtectionPolicy
{
    private ArrayList recipients;
    private X509Certificate decryptionCertificate;
    
    public PublicKeyProtectionPolicy() {
        this.recipients = null;
        this.recipients = new ArrayList();
    }
    
    public void addRecipient(final PublicKeyRecipient r) {
        this.recipients.add(r);
    }
    
    public boolean removeRecipient(final PublicKeyRecipient r) {
        return this.recipients.remove(r);
    }
    
    public Iterator getRecipientsIterator() {
        return this.recipients.iterator();
    }
    
    public X509Certificate getDecryptionCertificate() {
        return this.decryptionCertificate;
    }
    
    public void setDecryptionCertificate(final X509Certificate aDecryptionCertificate) {
        this.decryptionCertificate = aDecryptionCertificate;
    }
    
    public int getRecipientsNumber() {
        return this.recipients.size();
    }
}
