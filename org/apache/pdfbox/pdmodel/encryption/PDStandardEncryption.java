// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.encryption;

import org.apache.pdfbox.cos.COSInteger;
import java.io.IOException;
import org.apache.pdfbox.cos.COSString;
import org.apache.pdfbox.cos.COSNumber;
import org.apache.pdfbox.cos.COSDictionary;
import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSName;

public class PDStandardEncryption extends PDEncryptionDictionary
{
    public static final String FILTER_NAME = "Standard";
    public static final int DEFAULT_REVISION = 3;
    public static final int REVISION2 = 2;
    public static final int REVISION3 = 3;
    public static final int REVISION4 = 4;
    public static final int DEFAULT_PERMISSIONS = -4;
    private static final int PRINT_BIT = 3;
    private static final int MODIFICATION_BIT = 4;
    private static final int EXTRACT_BIT = 5;
    private static final int MODIFY_ANNOTATIONS_BIT = 6;
    private static final int FILL_IN_FORM_BIT = 9;
    private static final int EXTRACT_FOR_ACCESSIBILITY_BIT = 10;
    private static final int ASSEMBLE_DOCUMENT_BIT = 11;
    private static final int DEGRADED_PRINT_BIT = 12;
    
    public PDStandardEncryption() {
        this.encryptionDictionary.setItem(COSName.FILTER, COSName.getPDFName("Standard"));
        this.setVersion(1);
        this.setRevision(2);
        this.setPermissions(-4);
    }
    
    public PDStandardEncryption(final COSDictionary dict) {
        super(dict);
    }
    
    @Override
    public int getRevision() {
        int revision = 0;
        final COSNumber cosRevision = (COSNumber)this.encryptionDictionary.getDictionaryObject(COSName.getPDFName("R"));
        if (cosRevision != null) {
            revision = cosRevision.intValue();
        }
        return revision;
    }
    
    @Override
    public void setRevision(final int revision) {
        this.encryptionDictionary.setInt(COSName.getPDFName("R"), revision);
    }
    
    @Override
    public byte[] getOwnerKey() {
        byte[] o = null;
        final COSString owner = (COSString)this.encryptionDictionary.getDictionaryObject(COSName.getPDFName("O"));
        if (owner != null) {
            o = owner.getBytes();
        }
        return o;
    }
    
    @Override
    public void setOwnerKey(final byte[] o) throws IOException {
        final COSString owner = new COSString();
        owner.append(o);
        this.encryptionDictionary.setItem(COSName.getPDFName("O"), owner);
    }
    
    @Override
    public byte[] getUserKey() {
        byte[] u = null;
        final COSString user = (COSString)this.encryptionDictionary.getDictionaryObject(COSName.getPDFName("U"));
        if (user != null) {
            u = user.getBytes();
        }
        return u;
    }
    
    @Override
    public void setUserKey(final byte[] u) throws IOException {
        final COSString user = new COSString();
        user.append(u);
        this.encryptionDictionary.setItem(COSName.getPDFName("U"), user);
    }
    
    @Override
    public int getPermissions() {
        int permissions = 0;
        final COSInteger p = (COSInteger)this.encryptionDictionary.getDictionaryObject(COSName.getPDFName("P"));
        if (p != null) {
            permissions = p.intValue();
        }
        return permissions;
    }
    
    @Override
    public void setPermissions(final int p) {
        this.encryptionDictionary.setInt(COSName.getPDFName("P"), p);
    }
    
    private boolean isPermissionBitOn(final int bit) {
        return (this.getPermissions() & 1 << bit - 1) != 0x0;
    }
    
    private boolean setPermissionBit(final int bit, final boolean value) {
        int permissions = this.getPermissions();
        if (value) {
            permissions |= 1 << bit - 1;
        }
        else {
            permissions &= (-1 ^ 1 << bit - 1);
        }
        this.setPermissions(permissions);
        return (this.getPermissions() & 1 << bit - 1) != 0x0;
    }
    
    public boolean canPrint() {
        return this.isPermissionBitOn(3);
    }
    
    public void setCanPrint(final boolean allowPrinting) {
        this.setPermissionBit(3, allowPrinting);
    }
    
    public boolean canModify() {
        return this.isPermissionBitOn(4);
    }
    
    public void setCanModify(final boolean allowModifications) {
        this.setPermissionBit(4, allowModifications);
    }
    
    public boolean canExtractContent() {
        return this.isPermissionBitOn(5);
    }
    
    public void setCanExtractContent(final boolean allowExtraction) {
        this.setPermissionBit(5, allowExtraction);
    }
    
    public boolean canModifyAnnotations() {
        return this.isPermissionBitOn(6);
    }
    
    public void setCanModifyAnnotations(final boolean allowAnnotationModification) {
        this.setPermissionBit(6, allowAnnotationModification);
    }
    
    public boolean canFillInForm() {
        return this.isPermissionBitOn(9);
    }
    
    public void setCanFillInForm(final boolean allowFillingInForm) {
        this.setPermissionBit(9, allowFillingInForm);
    }
    
    public boolean canExtractForAccessibility() {
        return this.isPermissionBitOn(10);
    }
    
    public void setCanExtractForAccessibility(final boolean allowExtraction) {
        this.setPermissionBit(10, allowExtraction);
    }
    
    public boolean canAssembleDocument() {
        return this.isPermissionBitOn(11);
    }
    
    public void setCanAssembleDocument(final boolean allowAssembly) {
        this.setPermissionBit(11, allowAssembly);
    }
    
    public boolean canPrintDegraded() {
        return this.isPermissionBitOn(12);
    }
    
    public void setCanPrintDegraded(final boolean allowAssembly) {
        this.setPermissionBit(12, allowAssembly);
    }
}
