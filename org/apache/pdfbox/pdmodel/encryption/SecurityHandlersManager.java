// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.encryption;

import java.lang.reflect.Constructor;
import java.security.Provider;
import java.security.Security;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import java.util.Hashtable;

public class SecurityHandlersManager
{
    private static SecurityHandlersManager instance;
    private Hashtable handlerNames;
    private Hashtable handlerPolicyClasses;
    
    private SecurityHandlersManager() {
        this.handlerNames = null;
        this.handlerPolicyClasses = null;
        this.handlerNames = new Hashtable();
        this.handlerPolicyClasses = new Hashtable();
        try {
            this.registerHandler("Standard", StandardSecurityHandler.class, StandardProtectionPolicy.class);
            this.registerHandler("Adobe.PubSec", PublicKeySecurityHandler.class, PublicKeyProtectionPolicy.class);
        }
        catch (Exception e) {
            System.err.println("SecurityHandlersManager strange error with builtin handlers: " + e.getMessage());
            System.exit(1);
        }
    }
    
    public void registerHandler(final String filterName, final Class securityHandlerClass, final Class protectionPolicyClass) throws BadSecurityHandlerException {
        if (this.handlerNames.contains(securityHandlerClass) || this.handlerPolicyClasses.contains(securityHandlerClass)) {
            throw new BadSecurityHandlerException("the following security handler was already registered: " + securityHandlerClass.getName());
        }
        if (SecurityHandler.class.isAssignableFrom(securityHandlerClass)) {
            try {
                if (this.handlerNames.containsKey(filterName)) {
                    throw new BadSecurityHandlerException("a security handler was already registered for the filter name " + filterName);
                }
                if (this.handlerPolicyClasses.containsKey(protectionPolicyClass)) {
                    throw new BadSecurityHandlerException("a security handler was already registered for the policy class " + protectionPolicyClass.getName());
                }
                this.handlerNames.put(filterName, securityHandlerClass);
                this.handlerPolicyClasses.put(protectionPolicyClass, securityHandlerClass);
                return;
            }
            catch (Exception e) {
                throw new BadSecurityHandlerException(e);
            }
            throw new BadSecurityHandlerException("The class is not a super class of SecurityHandler");
        }
        throw new BadSecurityHandlerException("The class is not a super class of SecurityHandler");
    }
    
    public static SecurityHandlersManager getInstance() {
        if (SecurityHandlersManager.instance == null) {
            SecurityHandlersManager.instance = new SecurityHandlersManager();
            Security.addProvider(new BouncyCastleProvider());
        }
        return SecurityHandlersManager.instance;
    }
    
    public SecurityHandler getSecurityHandler(final ProtectionPolicy policy) throws BadSecurityHandlerException {
        final Object found = this.handlerPolicyClasses.get(policy.getClass());
        if (found == null) {
            throw new BadSecurityHandlerException("Cannot find an appropriate security handler for " + policy.getClass().getName());
        }
        final Class handlerclass = (Class)found;
        final Class[] argsClasses = { policy.getClass() };
        final Object[] args = { policy };
        try {
            final Constructor c = handlerclass.getDeclaredConstructor((Class[])argsClasses);
            final SecurityHandler handler = c.newInstance(args);
            return handler;
        }
        catch (Exception e) {
            e.printStackTrace();
            throw new BadSecurityHandlerException("problem while trying to instanciate the security handler " + handlerclass.getName() + ": " + e.getMessage());
        }
    }
    
    public SecurityHandler getSecurityHandler(final String filterName) throws BadSecurityHandlerException {
        final Object found = this.handlerNames.get(filterName);
        if (found == null) {
            throw new BadSecurityHandlerException("Cannot find an appropriate security handler for " + filterName);
        }
        final Class handlerclass = (Class)found;
        final Class[] argsClasses = new Class[0];
        final Object[] args = new Object[0];
        try {
            final Constructor c = handlerclass.getDeclaredConstructor((Class[])argsClasses);
            final SecurityHandler handler = c.newInstance(args);
            return handler;
        }
        catch (Exception e) {
            e.printStackTrace();
            throw new BadSecurityHandlerException("problem while trying to instanciate the security handler " + handlerclass.getName() + ": " + e.getMessage());
        }
    }
}
