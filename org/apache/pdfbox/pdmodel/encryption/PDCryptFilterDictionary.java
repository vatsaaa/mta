// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.encryption;

import java.io.IOException;
import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.cos.COSDictionary;

public class PDCryptFilterDictionary
{
    protected COSDictionary cryptFilterDictionary;
    
    public PDCryptFilterDictionary() {
        this.cryptFilterDictionary = null;
        this.cryptFilterDictionary = new COSDictionary();
    }
    
    public PDCryptFilterDictionary(final COSDictionary d) {
        this.cryptFilterDictionary = null;
        this.cryptFilterDictionary = d;
    }
    
    public COSDictionary getCOSDictionary() {
        return this.cryptFilterDictionary;
    }
    
    public void setLength(final int length) {
        this.cryptFilterDictionary.setInt(COSName.LENGTH, length);
    }
    
    public int getLength() {
        return this.cryptFilterDictionary.getInt(COSName.LENGTH, 40);
    }
    
    public void setCryptFilterMethod(final COSName cfm) throws IOException {
        this.cryptFilterDictionary.setItem(COSName.CFM, cfm);
    }
    
    public COSName getCryptFilterMethod() throws IOException {
        return (COSName)this.cryptFilterDictionary.getDictionaryObject(COSName.CFM);
    }
}
