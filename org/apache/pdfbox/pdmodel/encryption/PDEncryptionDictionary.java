// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.encryption;

import org.apache.pdfbox.cos.COSArray;
import org.apache.pdfbox.cos.COSBoolean;
import java.io.IOException;
import org.apache.pdfbox.cos.COSString;
import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.cos.COSDictionary;

public class PDEncryptionDictionary
{
    public static final int VERSION0_UNDOCUMENTED_UNSUPPORTED = 0;
    public static final int VERSION1_40_BIT_ALGORITHM = 1;
    public static final int VERSION2_VARIABLE_LENGTH_ALGORITHM = 2;
    public static final int VERSION3_UNPUBLISHED_ALGORITHM = 3;
    public static final int VERSION4_SECURITY_HANDLER = 4;
    public static final String DEFAULT_NAME = "Standard";
    public static final int DEFAULT_LENGTH = 40;
    public static final int DEFAULT_VERSION = 0;
    protected COSDictionary encryptionDictionary;
    
    public PDEncryptionDictionary() {
        this.encryptionDictionary = null;
        this.encryptionDictionary = new COSDictionary();
    }
    
    public PDEncryptionDictionary(final COSDictionary d) {
        this.encryptionDictionary = null;
        this.encryptionDictionary = d;
    }
    
    public COSDictionary getCOSDictionary() {
        return this.encryptionDictionary;
    }
    
    public void setFilter(final String filter) {
        this.encryptionDictionary.setItem(COSName.FILTER, COSName.getPDFName(filter));
    }
    
    public String getFilter() {
        return this.encryptionDictionary.getNameAsString(COSName.FILTER);
    }
    
    public String getSubFilter() {
        return this.encryptionDictionary.getNameAsString(COSName.SUB_FILTER);
    }
    
    public void setSubFilter(final String subfilter) {
        this.encryptionDictionary.setName(COSName.SUB_FILTER, subfilter);
    }
    
    public void setVersion(final int version) {
        this.encryptionDictionary.setInt(COSName.V, version);
    }
    
    public int getVersion() {
        return this.encryptionDictionary.getInt(COSName.V, 0);
    }
    
    public void setLength(final int length) {
        this.encryptionDictionary.setInt(COSName.LENGTH, length);
    }
    
    public int getLength() {
        return this.encryptionDictionary.getInt(COSName.LENGTH, 40);
    }
    
    public void setRevision(final int revision) {
        this.encryptionDictionary.setInt(COSName.R, revision);
    }
    
    public int getRevision() {
        return this.encryptionDictionary.getInt(COSName.R, 0);
    }
    
    public void setOwnerKey(final byte[] o) throws IOException {
        final COSString owner = new COSString();
        owner.append(o);
        this.encryptionDictionary.setItem(COSName.O, owner);
    }
    
    public byte[] getOwnerKey() throws IOException {
        byte[] o = null;
        final COSString owner = (COSString)this.encryptionDictionary.getDictionaryObject(COSName.O);
        if (owner != null) {
            o = owner.getBytes();
        }
        return o;
    }
    
    public void setUserKey(final byte[] u) throws IOException {
        final COSString user = new COSString();
        user.append(u);
        this.encryptionDictionary.setItem(COSName.U, user);
    }
    
    public byte[] getUserKey() throws IOException {
        byte[] u = null;
        final COSString user = (COSString)this.encryptionDictionary.getDictionaryObject(COSName.U);
        if (user != null) {
            u = user.getBytes();
        }
        return u;
    }
    
    public void setPermissions(final int permissions) {
        this.encryptionDictionary.setInt(COSName.P, permissions);
    }
    
    public int getPermissions() {
        return this.encryptionDictionary.getInt(COSName.P, 0);
    }
    
    public boolean isEncryptMetaData() {
        boolean encryptMetaData = true;
        final COSBase value = this.encryptionDictionary.getDictionaryObject(COSName.ENCRYPT_META_DATA);
        if (value instanceof COSBoolean) {
            encryptMetaData = ((COSBoolean)value).getValue();
        }
        return encryptMetaData;
    }
    
    public void setRecipients(final byte[][] recipients) throws IOException {
        final COSArray array = new COSArray();
        for (int i = 0; i < recipients.length; ++i) {
            final COSString recip = new COSString();
            recip.append(recipients[i]);
            recip.setForceLiteralForm(true);
            array.add(recip);
        }
        this.encryptionDictionary.setItem(COSName.RECIPIENTS, array);
    }
    
    public int getRecipientsLength() {
        final COSArray array = (COSArray)this.encryptionDictionary.getItem(COSName.RECIPIENTS);
        return array.size();
    }
    
    public COSString getRecipientStringAt(final int i) {
        final COSArray array = (COSArray)this.encryptionDictionary.getItem(COSName.RECIPIENTS);
        return (COSString)array.get(i);
    }
    
    public PDCryptFilterDictionary getStdCryptFilterDictionary() {
        return this.getCryptFilterDictionary(COSName.STD_CF);
    }
    
    public PDCryptFilterDictionary getCryptFilterDictionary(final COSName cryptFilterName) {
        final COSDictionary cryptFilterDictionary = (COSDictionary)this.encryptionDictionary.getDictionaryObject(COSName.CF);
        if (cryptFilterDictionary != null) {
            final COSDictionary stdCryptFilterDictionary = (COSDictionary)cryptFilterDictionary.getDictionaryObject(cryptFilterName);
            if (stdCryptFilterDictionary != null) {
                return new PDCryptFilterDictionary(stdCryptFilterDictionary);
            }
        }
        return null;
    }
    
    public COSName getStreamFilterName() {
        COSName stmF = (COSName)this.encryptionDictionary.getDictionaryObject(COSName.STM_F);
        if (stmF == null) {
            stmF = COSName.IDENTITY;
        }
        return stmF;
    }
    
    public COSName getStringFilterName() {
        COSName strF = (COSName)this.encryptionDictionary.getDictionaryObject(COSName.STR_F);
        if (strF == null) {
            strF = COSName.IDENTITY;
        }
        return strF;
    }
}
