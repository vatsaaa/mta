// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.encryption;

import java.util.Collections;
import java.util.HashMap;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.io.IOException;
import org.apache.pdfbox.cos.COSDictionary;
import org.apache.pdfbox.cos.COSName;
import java.util.Map;

public class PDEncryptionManager
{
    private static Map handlerMap;
    
    private PDEncryptionManager() {
    }
    
    public static void registerSecurityHandler(final String filterName, final Class handlerClass) {
        PDEncryptionManager.handlerMap.put(COSName.getPDFName(filterName), handlerClass);
    }
    
    public static PDEncryptionDictionary getEncryptionDictionary(final COSDictionary dictionary) throws IOException {
        Object retval = null;
        if (dictionary != null) {
            final COSName filter = (COSName)dictionary.getDictionaryObject(COSName.FILTER);
            final Class handlerClass = PDEncryptionManager.handlerMap.get(filter);
            if (handlerClass == null) {
                throw new IOException("No handler for security handler '" + filter.getName() + "'");
            }
            try {
                final Constructor ctor = handlerClass.getConstructor(COSDictionary.class);
                retval = ctor.newInstance(dictionary);
            }
            catch (NoSuchMethodException e) {
                throw new IOException(e.getMessage());
            }
            catch (InstantiationException e2) {
                throw new IOException(e2.getMessage());
            }
            catch (IllegalAccessException e3) {
                throw new IOException(e3.getMessage());
            }
            catch (InvocationTargetException e4) {
                throw new IOException(e4.getMessage());
            }
        }
        return (PDEncryptionDictionary)retval;
    }
    
    static {
        PDEncryptionManager.handlerMap = Collections.synchronizedMap(new HashMap<Object, Object>());
        registerSecurityHandler("Standard", PDStandardEncryption.class);
    }
}
