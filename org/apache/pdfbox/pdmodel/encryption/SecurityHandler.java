// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.encryption;

import java.io.ByteArrayOutputStream;
import java.io.ByteArrayInputStream;
import java.util.Map;
import org.apache.pdfbox.cos.COSStream;
import org.apache.pdfbox.cos.COSString;
import javax.crypto.SecretKey;
import javax.crypto.NoSuchPaddingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import org.apache.pdfbox.exceptions.WrappedIOException;
import javax.crypto.CipherInputStream;
import java.security.spec.AlgorithmParameterSpec;
import java.security.Key;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.crypto.Cipher;
import java.security.NoSuchAlgorithmException;
import java.security.MessageDigest;
import java.io.OutputStream;
import java.io.InputStream;
import org.apache.pdfbox.cos.COSName;
import java.util.Iterator;
import java.util.List;
import org.apache.pdfbox.cos.COSObject;
import org.apache.pdfbox.cos.COSArray;
import java.io.IOException;
import org.apache.pdfbox.exceptions.CryptographyException;
import java.util.HashSet;
import org.apache.pdfbox.cos.COSDictionary;
import org.apache.pdfbox.cos.COSBase;
import java.util.Set;
import org.apache.pdfbox.encryption.ARCFour;
import org.apache.pdfbox.pdmodel.PDDocument;

public abstract class SecurityHandler
{
    private static final int DEFAULT_KEY_LENGTH = 40;
    private static final byte[] AES_SALT;
    protected int version;
    protected int keyLength;
    protected byte[] encryptionKey;
    protected PDDocument document;
    protected ARCFour rc4;
    private Set<COSBase> objects;
    private Set<COSDictionary> potentialSignatures;
    private boolean aes;
    protected AccessPermission currentAccessPermission;
    
    public SecurityHandler() {
        this.keyLength = 40;
        this.rc4 = new ARCFour();
        this.objects = new HashSet<COSBase>();
        this.potentialSignatures = new HashSet<COSDictionary>();
        this.currentAccessPermission = null;
    }
    
    public abstract void prepareDocumentForEncryption(final PDDocument p0) throws CryptographyException, IOException;
    
    public abstract void prepareForDecryption(final PDEncryptionDictionary p0, final COSArray p1, final DecryptionMaterial p2) throws CryptographyException, IOException;
    
    public abstract void decryptDocument(final PDDocument p0, final DecryptionMaterial p1) throws CryptographyException, IOException;
    
    protected void proceedDecryption() throws IOException, CryptographyException {
        final COSDictionary trailer = this.document.getDocument().getTrailer();
        final COSArray fields = (COSArray)trailer.getObjectFromPath("Root/AcroForm/Fields");
        if (fields != null) {
            for (int i = 0; i < fields.size(); ++i) {
                final COSDictionary field = (COSDictionary)fields.getObject(i);
                if (field == null) {
                    throw new IOException("Could not decypt document, object not found.");
                }
                this.addDictionaryAndSubDictionary(this.potentialSignatures, field);
            }
        }
        final List<COSObject> allObjects = this.document.getDocument().getObjects();
        final Iterator<COSObject> objectIter = allObjects.iterator();
        while (objectIter.hasNext()) {
            this.decryptObject(objectIter.next());
        }
        this.document.setEncryptionDictionary(null);
    }
    
    private void addDictionaryAndSubDictionary(final Set<COSDictionary> set, final COSDictionary dic) {
        if (dic != null) {
            set.add(dic);
            final COSArray kids = (COSArray)dic.getDictionaryObject(COSName.KIDS);
            for (int i = 0; kids != null && i < kids.size(); ++i) {
                this.addDictionaryAndSubDictionary(set, (COSDictionary)kids.getObject(i));
            }
            final COSBase value = dic.getDictionaryObject(COSName.V);
            if (value instanceof COSDictionary) {
                this.addDictionaryAndSubDictionary(set, (COSDictionary)value);
            }
        }
    }
    
    @Deprecated
    public void encryptData(final long objectNumber, final long genNumber, final InputStream data, final OutputStream output) throws CryptographyException, IOException {
        this.encryptData(objectNumber, genNumber, data, output, false);
    }
    
    public void encryptData(final long objectNumber, final long genNumber, final InputStream data, final OutputStream output, final boolean decrypt) throws CryptographyException, IOException {
        if (this.aes && !decrypt) {
            throw new IllegalArgumentException("AES encryption is not yet implemented.");
        }
        final byte[] newKey = new byte[this.encryptionKey.length + 5];
        System.arraycopy(this.encryptionKey, 0, newKey, 0, this.encryptionKey.length);
        newKey[newKey.length - 5] = (byte)(objectNumber & 0xFFL);
        newKey[newKey.length - 4] = (byte)(objectNumber >> 8 & 0xFFL);
        newKey[newKey.length - 3] = (byte)(objectNumber >> 16 & 0xFFL);
        newKey[newKey.length - 2] = (byte)(genNumber & 0xFFL);
        newKey[newKey.length - 1] = (byte)(genNumber >> 8 & 0xFFL);
        byte[] digestedKey = null;
        try {
            final MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(newKey);
            if (this.aes) {
                md.update(SecurityHandler.AES_SALT);
            }
            digestedKey = md.digest();
        }
        catch (NoSuchAlgorithmException e) {
            throw new CryptographyException(e);
        }
        final int length = Math.min(newKey.length, 16);
        final byte[] finalKey = new byte[length];
        System.arraycopy(digestedKey, 0, finalKey, 0, length);
        if (this.aes) {
            final byte[] iv = new byte[16];
            data.read(iv);
            try {
                final Cipher decryptCipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
                final SecretKey aesKey = new SecretKeySpec(finalKey, "AES");
                final IvParameterSpec ips = new IvParameterSpec(iv);
                decryptCipher.init(decrypt ? 2 : 1, aesKey, ips);
                final CipherInputStream cipherStream = new CipherInputStream(data, decryptCipher);
                try {
                    final byte[] buffer = new byte[4096];
                    long count = 0L;
                    int n = 0;
                    while (-1 != (n = cipherStream.read(buffer))) {
                        output.write(buffer, 0, n);
                        count += n;
                    }
                }
                finally {
                    cipherStream.close();
                }
            }
            catch (InvalidKeyException e2) {
                throw new WrappedIOException(e2);
            }
            catch (InvalidAlgorithmParameterException e3) {
                throw new WrappedIOException(e3);
            }
            catch (NoSuchAlgorithmException e4) {
                throw new WrappedIOException(e4);
            }
            catch (NoSuchPaddingException e5) {
                throw new WrappedIOException(e5);
            }
        }
        else {
            this.rc4.setKey(finalKey);
            this.rc4.write(data, output);
        }
        output.flush();
    }
    
    private void decryptObject(final COSObject object) throws CryptographyException, IOException {
        final long objNum = object.getObjectNumber().intValue();
        final long genNum = object.getGenerationNumber().intValue();
        final COSBase base = object.getObject();
        this.decrypt(base, objNum, genNum);
    }
    
    private void decrypt(final COSBase obj, final long objNum, final long genNum) throws CryptographyException, IOException {
        if (!this.objects.contains(obj)) {
            this.objects.add(obj);
            if (obj instanceof COSString) {
                this.decryptString((COSString)obj, objNum, genNum);
            }
            else if (obj instanceof COSStream) {
                this.decryptStream((COSStream)obj, objNum, genNum);
            }
            else if (obj instanceof COSDictionary) {
                this.decryptDictionary((COSDictionary)obj, objNum, genNum);
            }
            else if (obj instanceof COSArray) {
                this.decryptArray((COSArray)obj, objNum, genNum);
            }
        }
    }
    
    public void decryptStream(final COSStream stream, final long objNum, final long genNum) throws CryptographyException, IOException {
        this.decryptDictionary(stream, objNum, genNum);
        final InputStream encryptedStream = stream.getFilteredStream();
        this.encryptData(objNum, genNum, encryptedStream, stream.createFilteredStream(), true);
    }
    
    public void encryptStream(final COSStream stream, final long objNum, final long genNum) throws CryptographyException, IOException {
        final InputStream encryptedStream = stream.getFilteredStream();
        this.encryptData(objNum, genNum, encryptedStream, stream.createFilteredStream(), false);
    }
    
    private void decryptDictionary(final COSDictionary dictionary, final long objNum, final long genNum) throws CryptographyException, IOException {
        for (final Map.Entry<COSName, COSBase> entry : dictionary.entrySet()) {
            if (!entry.getKey().getName().equals("Contents") || !(entry.getValue() instanceof COSString) || !this.potentialSignatures.contains(dictionary)) {
                this.decrypt(entry.getValue(), objNum, genNum);
            }
        }
    }
    
    public void decryptString(final COSString string, final long objNum, final long genNum) throws CryptographyException, IOException {
        final ByteArrayInputStream data = new ByteArrayInputStream(string.getBytes());
        final ByteArrayOutputStream buffer = new ByteArrayOutputStream();
        this.encryptData(objNum, genNum, data, buffer, true);
        string.reset();
        string.append(buffer.toByteArray());
    }
    
    private void decryptArray(final COSArray array, final long objNum, final long genNum) throws CryptographyException, IOException {
        for (int i = 0; i < array.size(); ++i) {
            this.decrypt(array.get(i), objNum, genNum);
        }
    }
    
    public int getKeyLength() {
        return this.keyLength;
    }
    
    public void setKeyLength(final int keyLen) {
        this.keyLength = keyLen;
    }
    
    public AccessPermission getCurrentAccessPermission() {
        return this.currentAccessPermission;
    }
    
    public boolean isAES() {
        return this.aes;
    }
    
    public void setAES(final boolean aes) {
        this.aes = aes;
    }
    
    static {
        AES_SALT = new byte[] { 115, 65, 108, 84 };
    }
}
