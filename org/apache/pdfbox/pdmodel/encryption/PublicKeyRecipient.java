// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.encryption;

import java.security.cert.X509Certificate;

public class PublicKeyRecipient
{
    private X509Certificate x509;
    private AccessPermission permission;
    
    public X509Certificate getX509() {
        return this.x509;
    }
    
    public void setX509(final X509Certificate aX509) {
        this.x509 = aX509;
    }
    
    public AccessPermission getPermission() {
        return this.permission;
    }
    
    public void setPermission(final AccessPermission permissions) {
        this.permission = permissions;
    }
}
