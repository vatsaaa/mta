// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel;

import java.util.Iterator;
import java.util.ArrayList;
import java.util.List;
import java.io.File;
import org.apache.pdfbox.cos.COSDocument;
import java.io.IOException;
import java.util.HashMap;
import org.apache.pdfbox.pdfparser.ConformingPDFParser;
import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.persistence.util.COSObjectKey;
import java.util.Map;

public class ConformingPDDocument extends PDDocument
{
    private final Map<COSObjectKey, COSBase> objectPool;
    private ConformingPDFParser parser;
    
    public ConformingPDDocument() throws IOException {
        this.objectPool = new HashMap<COSObjectKey, COSBase>();
        this.parser = null;
    }
    
    public ConformingPDDocument(final COSDocument doc) throws IOException {
        super(doc);
        this.objectPool = new HashMap<COSObjectKey, COSBase>();
        this.parser = null;
    }
    
    public static PDDocument load(final File input) throws IOException {
        final ConformingPDFParser parser = new ConformingPDFParser(input);
        parser.parse();
        return parser.getPDDocument();
    }
    
    public COSBase getObjectFromPool(final COSObjectKey key) throws IOException {
        return this.objectPool.get(key);
    }
    
    public List<COSObjectKey> getObjectKeysFromPool() throws IOException {
        final List<COSObjectKey> keys = new ArrayList<COSObjectKey>();
        for (final COSObjectKey key : this.objectPool.keySet()) {
            keys.add(key);
        }
        return keys;
    }
    
    public COSBase getObjectFromPool(final long number, final long generation) throws IOException {
        return this.objectPool.get(new COSObjectKey(number, generation));
    }
    
    public void putObjectInPool(final COSBase object, final long number, final long generation) {
        this.objectPool.put(new COSObjectKey(number, generation), object);
    }
    
    public ConformingPDFParser getParser() {
        return this.parser;
    }
    
    public void setParser(final ConformingPDFParser parser) {
        this.parser = parser;
    }
}
