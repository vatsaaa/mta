// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.markedcontent;

import org.apache.pdfbox.pdmodel.graphics.optionalcontent.PDOptionalContentGroup;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSDictionary;
import org.apache.pdfbox.pdmodel.common.COSObjectable;

public class PDPropertyList implements COSObjectable
{
    private COSDictionary props;
    
    public PDPropertyList() {
        this.props = new COSDictionary();
    }
    
    public PDPropertyList(final COSDictionary dict) {
        this.props = dict;
    }
    
    public COSBase getCOSObject() {
        return this.props;
    }
    
    public PDOptionalContentGroup getOptionalContentGroup(final COSName name) {
        final COSDictionary dict = (COSDictionary)this.props.getDictionaryObject(name);
        if (dict != null && COSName.OCG.equals(dict.getItem(COSName.TYPE))) {
            return new PDOptionalContentGroup(dict);
        }
        return null;
    }
    
    public void putMapping(final COSName name, final PDOptionalContentGroup ocg) {
        this.putMapping(name, (COSDictionary)ocg.getCOSObject());
    }
    
    private void putMapping(final COSName name, final COSDictionary dict) {
        this.props.setItem(name, dict);
    }
}
