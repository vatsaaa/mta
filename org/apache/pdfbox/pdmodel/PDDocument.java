// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel;

import org.apache.pdfbox.pdmodel.encryption.SecurityHandlersManager;
import java.awt.print.Printable;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.awt.print.PageFormat;
import org.apache.pdfbox.pdfwriter.COSWriter;
import org.apache.pdfbox.exceptions.COSVisitorException;
import java.io.FileOutputStream;
import org.apache.pdfbox.pdfparser.NonSequentialPDFParser;
import org.apache.pdfbox.pdfparser.PDFParser;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import org.apache.pdfbox.io.RandomAccess;
import java.net.URL;
import org.apache.pdfbox.pdmodel.encryption.ProtectionPolicy;
import org.apache.pdfbox.pdmodel.encryption.StandardProtectionPolicy;
import org.apache.pdfbox.pdmodel.encryption.AccessPermission;
import org.apache.pdfbox.exceptions.InvalidPasswordException;
import org.apache.pdfbox.pdmodel.encryption.BadSecurityHandlerException;
import org.apache.pdfbox.pdmodel.encryption.DecryptionMaterial;
import org.apache.pdfbox.pdmodel.encryption.StandardDecryptionMaterial;
import org.apache.pdfbox.exceptions.CryptographyException;
import java.io.OutputStream;
import java.io.InputStream;
import org.apache.pdfbox.pdmodel.common.PDStream;
import java.util.Iterator;
import org.apache.pdfbox.pdmodel.interactive.annotation.PDAnnotation;
import org.apache.pdfbox.pdmodel.common.COSObjectable;
import org.apache.pdfbox.pdmodel.interactive.annotation.PDAppearanceStream;
import org.apache.pdfbox.cos.COSStream;
import org.apache.pdfbox.pdmodel.interactive.annotation.PDAppearanceDictionary;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.interactive.form.PDSignatureField;
import org.apache.pdfbox.pdmodel.interactive.form.PDAcroForm;
import java.util.ArrayList;
import org.apache.pdfbox.exceptions.SignatureException;
import org.apache.pdfbox.pdmodel.interactive.digitalsignature.SignatureOptions;
import org.apache.pdfbox.pdmodel.interactive.digitalsignature.SignatureInterface;
import org.apache.pdfbox.pdmodel.interactive.digitalsignature.PDSignature;
import org.apache.pdfbox.cos.COSObject;
import org.apache.pdfbox.pdmodel.common.COSArrayList;
import java.util.List;
import java.util.HashMap;
import java.io.IOException;
import org.apache.pdfbox.cos.COSInteger;
import org.apache.pdfbox.cos.COSArray;
import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.cos.COSDictionary;
import java.util.Map;
import org.apache.pdfbox.pdmodel.encryption.SecurityHandler;
import org.apache.pdfbox.pdmodel.encryption.PDEncryptionDictionary;
import org.apache.pdfbox.cos.COSDocument;
import java.awt.print.Pageable;

public class PDDocument implements Pageable
{
    private COSDocument document;
    private PDDocumentInformation documentInformation;
    private PDDocumentCatalog documentCatalog;
    private PDEncryptionDictionary encParameters;
    private SecurityHandler securityHandler;
    private Map<String, Integer> pageMap;
    private boolean allSecurityToBeRemoved;
    
    public PDDocument() throws IOException {
        this.encParameters = null;
        this.securityHandler = null;
        this.pageMap = null;
        this.allSecurityToBeRemoved = false;
        this.document = new COSDocument();
        final COSDictionary trailer = new COSDictionary();
        this.document.setTrailer(trailer);
        final COSDictionary rootDictionary = new COSDictionary();
        trailer.setItem(COSName.ROOT, rootDictionary);
        rootDictionary.setItem(COSName.TYPE, COSName.CATALOG);
        rootDictionary.setItem(COSName.VERSION, COSName.getPDFName("1.4"));
        final COSDictionary pages = new COSDictionary();
        rootDictionary.setItem(COSName.PAGES, pages);
        pages.setItem(COSName.TYPE, COSName.PAGES);
        final COSArray kidsArray = new COSArray();
        pages.setItem(COSName.KIDS, kidsArray);
        pages.setItem(COSName.COUNT, COSInteger.ZERO);
    }
    
    private void generatePageMap() {
        this.pageMap = new HashMap<String, Integer>();
        this.processListOfPageReferences(this.getDocumentCatalog().getPages().getKids());
    }
    
    private void processListOfPageReferences(final List<Object> pageNodes) {
        for (int numberOfNodes = pageNodes.size(), i = 0; i < numberOfNodes; ++i) {
            final Object pageOrArray = pageNodes.get(i);
            if (pageOrArray instanceof PDPage) {
                final COSArray pageArray = ((COSArrayList)((PDPage)pageOrArray).getParent().getKids()).toList();
                this.parseCatalogObject((COSObject)pageArray.get(i));
            }
            else if (pageOrArray instanceof PDPageNode) {
                this.processListOfPageReferences(((PDPageNode)pageOrArray).getKids());
            }
        }
    }
    
    private void parseCatalogObject(final COSObject thePageOrArrayObject) {
        final COSBase arrayCountBase = thePageOrArrayObject.getItem(COSName.COUNT);
        int arrayCount = -1;
        if (arrayCountBase instanceof COSInteger) {
            arrayCount = ((COSInteger)arrayCountBase).intValue();
        }
        final COSBase kidsBase = thePageOrArrayObject.getItem(COSName.KIDS);
        int kidsCount = -1;
        if (kidsBase instanceof COSArray) {
            kidsCount = ((COSArray)kidsBase).size();
        }
        if (arrayCount == -1 || kidsCount == -1) {
            final String objStr = String.valueOf(thePageOrArrayObject.getObjectNumber().intValue());
            final String genStr = String.valueOf(thePageOrArrayObject.getGenerationNumber().intValue());
            this.getPageMap().put(objStr + "," + genStr, new Integer(this.getPageMap().size() + 1));
        }
        else if (arrayCount == kidsCount) {
            final COSArray kidsArray = (COSArray)kidsBase;
            for (int i = 0; i < kidsArray.size(); ++i) {
                final COSObject thisObject = (COSObject)kidsArray.get(i);
                final String objStr2 = String.valueOf(thisObject.getObjectNumber().intValue());
                final String genStr2 = String.valueOf(thisObject.getGenerationNumber().intValue());
                this.getPageMap().put(objStr2 + "," + genStr2, new Integer(this.getPageMap().size() + 1));
            }
        }
        else {
            COSArray list = null;
            if (kidsBase instanceof COSArray) {
                list = (COSArray)kidsBase;
            }
            if (list != null) {
                for (int arrayCounter = 0; arrayCounter < list.size(); ++arrayCounter) {
                    this.parseCatalogObject((COSObject)list.get(arrayCounter));
                }
            }
        }
    }
    
    public final Map<String, Integer> getPageMap() {
        if (this.pageMap == null) {
            this.generatePageMap();
        }
        return this.pageMap;
    }
    
    public void addPage(final PDPage page) {
        final PDPageNode rootPages = this.getDocumentCatalog().getPages();
        rootPages.getKids().add(page);
        page.setParent(rootPages);
        rootPages.updateCount();
    }
    
    public void addSignature(final PDSignature sigObject, final SignatureInterface signatureInterface) throws IOException, SignatureException {
        final SignatureOptions defaultOptions = new SignatureOptions();
        defaultOptions.setPage(1);
        this.addSignature(sigObject, signatureInterface, defaultOptions);
    }
    
    public void addSignature(final PDSignature sigObject, final SignatureInterface signatureInterface, final SignatureOptions options) throws IOException, SignatureException {
        final int preferedSignatureSize = options.getPreferedSignatureSize();
        if (preferedSignatureSize > 0) {
            sigObject.setContents(new byte[preferedSignatureSize * 2 + 2]);
        }
        else {
            sigObject.setContents(new byte[18946]);
        }
        sigObject.setByteRange(new int[] { 0, 1000000000, 1000000000, 1000000000 });
        this.getDocument().setSignatureInterface(signatureInterface);
        final PDDocumentCatalog root = this.getDocumentCatalog();
        final PDPageNode rootPages = root.getPages();
        final List<PDPage> kids = new ArrayList<PDPage>();
        rootPages.getAllKids(kids);
        final int size = (int)rootPages.getCount();
        PDPage page = null;
        if (size == 0) {
            throw new SignatureException(5, "The PDF file has no pages");
        }
        if (options.getPage() > size) {
            page = kids.get(size - 1);
        }
        else if (options.getPage() <= 0) {
            page = kids.get(0);
        }
        else {
            page = kids.get(options.getPage() - 1);
        }
        PDAcroForm acroForm = root.getAcroForm();
        root.getCOSObject().setNeedToBeUpdate(true);
        if (acroForm == null) {
            acroForm = new PDAcroForm(this);
            root.setAcroForm(acroForm);
        }
        else {
            acroForm.getCOSObject().setNeedToBeUpdate(true);
        }
        final PDSignatureField signatureField = new PDSignatureField(acroForm);
        signatureField.setSignature(sigObject);
        signatureField.getWidget().setPage(page);
        final List acroFormFields = acroForm.getFields();
        final COSDictionary acroFormDict = acroForm.getDictionary();
        acroFormDict.setDirect(true);
        acroFormDict.setInt(COSName.SIG_FLAGS, 3);
        acroFormFields.add(signatureField);
        final COSDocument visualSignature = options.getVisualSignature();
        if (visualSignature == null) {
            signatureField.getWidget().setRectangle(new PDRectangle());
            acroFormDict.setItem(COSName.DR, null);
            final PDAppearanceDictionary ap = new PDAppearanceDictionary();
            final COSStream apsStream = new COSStream(this.getDocument().getScratchFile());
            apsStream.createUnfilteredStream();
            final PDAppearanceStream aps = new PDAppearanceStream(apsStream);
            final COSDictionary cosObject = (COSDictionary)aps.getCOSObject();
            cosObject.setItem(COSName.SUBTYPE, COSName.FORM);
            cosObject.setItem(COSName.BBOX, new PDRectangle());
            ap.setNormalAppearance(aps);
            ap.getDictionary().setDirect(true);
            signatureField.getWidget().setAppearance(ap);
        }
        else {
            final List<COSObject> cosObjects = visualSignature.getObjects();
            boolean annotNotFound = true;
            boolean sigFieldNotFound = true;
            for (final COSObject cosObject2 : cosObjects) {
                final COSBase base = cosObject2.getObject();
                if (base != null && base instanceof COSDictionary) {
                    final COSBase ft = ((COSDictionary)base).getItem(COSName.FT);
                    final COSBase type = ((COSDictionary)base).getItem(COSName.TYPE);
                    final COSBase apDict = ((COSDictionary)base).getItem(COSName.AP);
                    if (annotNotFound && COSName.ANNOT.equals(type)) {
                        final COSDictionary cosBaseDict = (COSDictionary)base;
                        final COSArray rectAry = (COSArray)cosBaseDict.getItem(COSName.RECT);
                        final PDRectangle rect = new PDRectangle(rectAry);
                        signatureField.getWidget().setRectangle(rect);
                        annotNotFound = false;
                    }
                    if (!sigFieldNotFound || !COSName.SIG.equals(ft) || apDict == null) {
                        continue;
                    }
                    final COSDictionary cosBaseDict = (COSDictionary)base;
                    final PDAppearanceDictionary ap2 = new PDAppearanceDictionary((COSDictionary)cosBaseDict.getItem(COSName.AP));
                    ap2.getDictionary().setDirect(true);
                    signatureField.getWidget().setAppearance(ap2);
                    final COSBase dr = cosBaseDict.getItem(COSName.DR);
                    dr.setDirect(true);
                    dr.setNeedToBeUpdate(true);
                    acroFormDict.setItem(COSName.DR, dr);
                    sigFieldNotFound = false;
                }
            }
            if (annotNotFound || sigFieldNotFound) {
                throw new SignatureException(6, "Could not read all needed objects from template");
            }
        }
        List annotations = page.getAnnotations();
        if (annotations == null) {
            annotations = new COSArrayList();
            page.setAnnotations(annotations);
        }
        if (!(annotations instanceof COSArrayList) || !(acroFormFields instanceof COSArrayList) || !((COSArrayList)annotations).toList().equals(((COSArrayList)acroFormFields).toList())) {
            annotations.add(signatureField.getWidget());
        }
        page.getCOSObject().setNeedToBeUpdate(true);
    }
    
    public boolean removePage(final PDPage page) {
        final PDPageNode parent = page.getParent();
        final boolean retval = parent.getKids().remove(page);
        if (retval) {
            this.getDocumentCatalog().getPages().updateCount();
        }
        return retval;
    }
    
    public boolean removePage(final int pageNumber) {
        boolean removed = false;
        final List allPages = this.getDocumentCatalog().getAllPages();
        if (allPages.size() > pageNumber) {
            final PDPage page = allPages.get(pageNumber);
            removed = this.removePage(page);
        }
        return removed;
    }
    
    public PDPage importPage(final PDPage page) throws IOException {
        final PDPage importedPage = new PDPage(new COSDictionary(page.getCOSDictionary()));
        InputStream is = null;
        OutputStream os = null;
        try {
            final PDStream src = page.getContents();
            if (src != null) {
                final PDStream dest = new PDStream(new COSStream(src.getStream(), this.document.getScratchFile()));
                importedPage.setContents(dest);
                os = dest.createOutputStream();
                final byte[] buf = new byte[10240];
                int amountRead = 0;
                is = src.createInputStream();
                while ((amountRead = is.read(buf, 0, 10240)) > -1) {
                    os.write(buf, 0, amountRead);
                }
            }
            this.addPage(importedPage);
        }
        finally {
            if (is != null) {
                is.close();
            }
            if (os != null) {
                os.close();
            }
        }
        return importedPage;
    }
    
    public PDDocument(final COSDocument doc) {
        this.encParameters = null;
        this.securityHandler = null;
        this.pageMap = null;
        this.allSecurityToBeRemoved = false;
        this.document = doc;
    }
    
    public COSDocument getDocument() {
        return this.document;
    }
    
    public PDDocumentInformation getDocumentInformation() {
        if (this.documentInformation == null) {
            final COSDictionary trailer = this.document.getTrailer();
            COSDictionary infoDic = (COSDictionary)trailer.getDictionaryObject(COSName.INFO);
            if (infoDic == null) {
                infoDic = new COSDictionary();
                trailer.setItem(COSName.INFO, infoDic);
            }
            this.documentInformation = new PDDocumentInformation(infoDic);
        }
        return this.documentInformation;
    }
    
    public void setDocumentInformation(final PDDocumentInformation info) {
        this.documentInformation = info;
        this.document.getTrailer().setItem(COSName.INFO, info.getDictionary());
    }
    
    public PDDocumentCatalog getDocumentCatalog() {
        if (this.documentCatalog == null) {
            final COSDictionary trailer = this.document.getTrailer();
            final COSBase dictionary = trailer.getDictionaryObject(COSName.ROOT);
            if (dictionary instanceof COSDictionary) {
                this.documentCatalog = new PDDocumentCatalog(this, (COSDictionary)dictionary);
            }
            else {
                this.documentCatalog = new PDDocumentCatalog(this);
            }
        }
        return this.documentCatalog;
    }
    
    public boolean isEncrypted() {
        return this.document.isEncrypted();
    }
    
    public PDEncryptionDictionary getEncryptionDictionary() throws IOException {
        if (this.encParameters == null && this.isEncrypted()) {
            this.encParameters = new PDEncryptionDictionary(this.document.getEncryptionDictionary());
        }
        return this.encParameters;
    }
    
    public void setEncryptionDictionary(final PDEncryptionDictionary encDictionary) throws IOException {
        this.encParameters = encDictionary;
    }
    
    public PDSignature getSignatureDictionary() throws IOException {
        final COSDictionary signatureDictionary = this.document.getLastSignatureDictionary();
        if (signatureDictionary != null) {
            return new PDSignature(signatureDictionary);
        }
        return null;
    }
    
    @Deprecated
    public boolean isUserPassword(final String password) throws IOException, CryptographyException {
        return false;
    }
    
    @Deprecated
    public boolean isOwnerPassword(final String password) throws IOException, CryptographyException {
        return false;
    }
    
    public void decrypt(final String password) throws CryptographyException, IOException, InvalidPasswordException {
        try {
            final StandardDecryptionMaterial m = new StandardDecryptionMaterial(password);
            this.openProtection(m);
            this.document.dereferenceObjectStreams();
        }
        catch (BadSecurityHandlerException e) {
            throw new CryptographyException(e);
        }
    }
    
    @Deprecated
    public boolean wasDecryptedWithOwnerPassword() {
        return false;
    }
    
    public void encrypt(final String ownerPassword, final String userPassword) throws CryptographyException, IOException {
        try {
            final StandardProtectionPolicy policy = new StandardProtectionPolicy(ownerPassword, userPassword, new AccessPermission());
            this.protect(policy);
        }
        catch (BadSecurityHandlerException e) {
            throw new CryptographyException(e);
        }
    }
    
    @Deprecated
    public String getOwnerPasswordForEncryption() {
        return null;
    }
    
    @Deprecated
    public String getUserPasswordForEncryption() {
        return null;
    }
    
    @Deprecated
    public boolean willEncryptWhenSaving() {
        return false;
    }
    
    @Deprecated
    public void clearWillEncryptWhenSaving() {
    }
    
    public static PDDocument load(final URL url) throws IOException {
        return load(url.openStream());
    }
    
    public static PDDocument load(final URL url, final boolean force) throws IOException {
        return load(url.openStream(), force);
    }
    
    public static PDDocument load(final URL url, final RandomAccess scratchFile) throws IOException {
        return load(url.openStream(), scratchFile);
    }
    
    public static PDDocument load(final String filename) throws IOException {
        return load(new FileInputStream(filename));
    }
    
    public static PDDocument load(final String filename, final boolean force) throws IOException {
        return load(new FileInputStream(filename), force);
    }
    
    public static PDDocument load(final String filename, final RandomAccess scratchFile) throws IOException {
        return load(new FileInputStream(filename), scratchFile);
    }
    
    public static PDDocument load(final File file) throws IOException {
        return load(new FileInputStream(file));
    }
    
    public static PDDocument load(final File file, final RandomAccess scratchFile) throws IOException {
        return load(new FileInputStream(file), scratchFile);
    }
    
    public static PDDocument load(final InputStream input) throws IOException {
        return load(input, null);
    }
    
    public static PDDocument load(final InputStream input, final boolean force) throws IOException {
        return load(input, null, force);
    }
    
    public static PDDocument load(final InputStream input, final RandomAccess scratchFile) throws IOException {
        final PDFParser parser = new PDFParser(new BufferedInputStream(input), scratchFile);
        parser.parse();
        return parser.getPDDocument();
    }
    
    public static PDDocument load(final InputStream input, final RandomAccess scratchFile, final boolean force) throws IOException {
        final PDFParser parser = new PDFParser(new BufferedInputStream(input), scratchFile, force);
        parser.parse();
        return parser.getPDDocument();
    }
    
    public static PDDocument loadNonSeq(final File file, final RandomAccess scratchFile) throws IOException {
        return loadNonSeq(file, scratchFile, "");
    }
    
    public static PDDocument loadNonSeq(final File file, final RandomAccess scratchFile, final String password) throws IOException {
        final NonSequentialPDFParser parser = new NonSequentialPDFParser(file, scratchFile, password);
        parser.parse();
        return parser.getPDDocument();
    }
    
    public void save(final String fileName) throws IOException, COSVisitorException {
        this.save(new FileOutputStream(fileName));
    }
    
    public void save(final OutputStream output) throws IOException, COSVisitorException {
        this.getDocumentCatalog().getPages().updateCount();
        COSWriter writer = null;
        try {
            writer = new COSWriter(output);
            writer.write(this);
            writer.close();
        }
        finally {
            if (writer != null) {
                writer.close();
            }
        }
    }
    
    public void saveIncremental(final String fileName) throws IOException, COSVisitorException {
        this.saveIncremental(new FileInputStream(fileName), new FileOutputStream(fileName, true));
    }
    
    public void saveIncremental(final FileInputStream input, final OutputStream output) throws IOException, COSVisitorException {
        this.getDocumentCatalog().getPages().updateCount();
        COSWriter writer = null;
        try {
            output.write("\r\n".getBytes());
            writer = new COSWriter(output, input);
            writer.write(this);
            writer.close();
        }
        finally {
            if (writer != null) {
                writer.close();
            }
        }
    }
    
    @Deprecated
    public int getPageCount() {
        return this.getNumberOfPages();
    }
    
    public int getNumberOfPages() {
        final PDDocumentCatalog cat = this.getDocumentCatalog();
        return (int)cat.getPages().getCount();
    }
    
    @Deprecated
    public PageFormat getPageFormat(final int pageIndex) {
        try {
            final PrinterJob printerJob = PrinterJob.getPrinterJob();
            return new PDPageable(this, printerJob).getPageFormat(pageIndex);
        }
        catch (PrinterException e) {
            throw new RuntimeException(e);
        }
    }
    
    public Printable getPrintable(final int pageIndex) {
        return this.getDocumentCatalog().getAllPages().get(pageIndex);
    }
    
    public void print(final PrinterJob printJob) throws PrinterException {
        this.print(printJob, false);
    }
    
    public void print() throws PrinterException {
        this.print(PrinterJob.getPrinterJob());
    }
    
    public void silentPrint() throws PrinterException {
        this.silentPrint(PrinterJob.getPrinterJob());
    }
    
    public void silentPrint(final PrinterJob printJob) throws PrinterException {
        this.print(printJob, true);
    }
    
    private void print(final PrinterJob job, final boolean silent) throws PrinterException {
        if (job == null) {
            throw new PrinterException("The given printer job is null.");
        }
        job.setPageable(new PDPageable(this, job));
        if (silent || job.printDialog()) {
            job.print();
        }
    }
    
    public void close() throws IOException {
        this.document.close();
    }
    
    public void protect(final ProtectionPolicy pp) throws BadSecurityHandlerException {
        final SecurityHandler handler = SecurityHandlersManager.getInstance().getSecurityHandler(pp);
        this.securityHandler = handler;
    }
    
    public void openProtection(final DecryptionMaterial pm) throws BadSecurityHandlerException, IOException, CryptographyException {
        final PDEncryptionDictionary dict = this.getEncryptionDictionary();
        if (dict.getFilter() != null) {
            (this.securityHandler = SecurityHandlersManager.getInstance().getSecurityHandler(dict.getFilter())).decryptDocument(this, pm);
            this.document.dereferenceObjectStreams();
            this.document.setEncryptionDictionary(null);
            return;
        }
        throw new RuntimeException("This document does not need to be decrypted");
    }
    
    public AccessPermission getCurrentAccessPermission() {
        if (this.securityHandler == null) {
            return AccessPermission.getOwnerAccessPermission();
        }
        return this.securityHandler.getCurrentAccessPermission();
    }
    
    public SecurityHandler getSecurityHandler() {
        return this.securityHandler;
    }
    
    public boolean setSecurityHandler(final SecurityHandler _sHandler) {
        if (this.securityHandler == null) {
            this.securityHandler = _sHandler;
            return true;
        }
        return false;
    }
    
    public boolean isAllSecurityToBeRemoved() {
        return this.allSecurityToBeRemoved;
    }
    
    public void setAllSecurityToBeRemoved(final boolean allSecurityToBeRemoved) {
        this.allSecurityToBeRemoved = allSecurityToBeRemoved;
    }
}
