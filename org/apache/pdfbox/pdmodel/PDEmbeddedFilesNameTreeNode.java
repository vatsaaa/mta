// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel;

import java.io.IOException;
import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSDictionary;
import org.apache.pdfbox.pdmodel.common.filespecification.PDComplexFileSpecification;
import org.apache.pdfbox.pdmodel.common.PDNameTreeNode;

public class PDEmbeddedFilesNameTreeNode extends PDNameTreeNode
{
    public PDEmbeddedFilesNameTreeNode() {
        super(PDComplexFileSpecification.class);
    }
    
    public PDEmbeddedFilesNameTreeNode(final COSDictionary dic) {
        super(dic, PDComplexFileSpecification.class);
    }
    
    @Override
    protected Object convertCOSToPD(final COSBase base) throws IOException {
        final COSBase destination = base;
        return new PDComplexFileSpecification((COSDictionary)destination);
    }
    
    @Override
    protected PDNameTreeNode createChildNode(final COSDictionary dic) {
        return new PDEmbeddedFilesNameTreeNode(dic);
    }
}
