// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.font;

import java.io.IOException;
import java.awt.Font;
import org.apache.pdfbox.cos.COSDictionary;
import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSName;

public class PDCIDFontType0Font extends PDCIDFont
{
    public PDCIDFontType0Font() {
        this.font.setItem(COSName.SUBTYPE, COSName.CID_FONT_TYPE0);
    }
    
    public PDCIDFontType0Font(final COSDictionary fontDictionary) {
        super(fontDictionary);
    }
    
    @Override
    public Font getawtFont() throws IOException {
        final PDFontDescriptor fd = this.getFontDescriptor();
        Font awtFont = FontManager.getAwtFont(fd.getFontName());
        if (awtFont == null && fd instanceof PDFontDescriptorDictionary) {
            final PDFontDescriptorDictionary fdd = (PDFontDescriptorDictionary)fd;
            if (fdd.getFontFile3() != null) {
                awtFont = new PDType1CFont(this.font).getawtFont();
            }
        }
        return awtFont;
    }
}
