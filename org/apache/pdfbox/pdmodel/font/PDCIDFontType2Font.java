// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.font;

import org.apache.commons.logging.LogFactory;
import org.apache.pdfbox.io.IOUtils;
import org.apache.pdfbox.cos.COSStream;
import java.io.IOException;
import org.apache.pdfbox.pdmodel.common.PDStream;
import java.awt.FontFormatException;
import java.awt.Font;
import org.apache.pdfbox.cos.COSDictionary;
import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSName;
import org.apache.commons.logging.Log;

public class PDCIDFontType2Font extends PDCIDFont
{
    private static final Log LOG;
    private Boolean hasCIDToGIDMap;
    private int[] cid2gid;
    
    public PDCIDFontType2Font() {
        this.hasCIDToGIDMap = null;
        this.cid2gid = null;
        this.font.setItem(COSName.SUBTYPE, COSName.CID_FONT_TYPE2);
    }
    
    public PDCIDFontType2Font(final COSDictionary fontDictionary) {
        super(fontDictionary);
        this.hasCIDToGIDMap = null;
        this.cid2gid = null;
    }
    
    @Override
    public Font getawtFont() throws IOException {
        Font awtFont = null;
        final PDFontDescriptorDictionary fd = (PDFontDescriptorDictionary)this.getFontDescriptor();
        final PDStream ff2Stream = fd.getFontFile2();
        if (ff2Stream != null) {
            try {
                awtFont = Font.createFont(0, ff2Stream.createInputStream());
            }
            catch (FontFormatException f) {
                PDCIDFontType2Font.LOG.info("Can't read the embedded font " + fd.getFontName());
            }
            if (awtFont == null) {
                awtFont = FontManager.getAwtFont(fd.getFontName());
                if (awtFont != null) {
                    PDCIDFontType2Font.LOG.info("Using font " + awtFont.getName() + " instead");
                }
                this.setIsFontSubstituted(true);
            }
        }
        return awtFont;
    }
    
    private void readCIDToGIDMapping() {
        final COSBase map = this.font.getDictionaryObject(COSName.CID_TO_GID_MAP);
        if (map instanceof COSStream) {
            final COSStream stream = (COSStream)map;
            try {
                final byte[] mapAsBytes = IOUtils.toByteArray(stream.getUnfilteredStream());
                final int numberOfInts = mapAsBytes.length / 2;
                this.cid2gid = new int[numberOfInts];
                final int index = 0;
                for (int offset = 0; offset < numberOfInts; ++offset) {
                    this.cid2gid[index] = this.getCodeFromArray(mapAsBytes, offset, 2);
                }
            }
            catch (IOException exception) {
                PDCIDFontType2Font.LOG.error("Can't read the CIDToGIDMap", exception);
            }
        }
    }
    
    public boolean hasCIDToGIDMap() {
        if (this.hasCIDToGIDMap == null) {
            final COSBase map = this.font.getDictionaryObject(COSName.CID_TO_GID_MAP);
            if (map != null && map instanceof COSStream) {
                this.hasCIDToGIDMap = Boolean.TRUE;
            }
            else {
                this.hasCIDToGIDMap = Boolean.FALSE;
            }
        }
        return this.hasCIDToGIDMap;
    }
    
    public int mapCIDToGID(final int cid) {
        if (!this.hasCIDToGIDMap()) {
            return cid;
        }
        if (this.cid2gid == null) {
            this.readCIDToGIDMapping();
        }
        if (this.cid2gid != null && cid < this.cid2gid.length) {
            return this.cid2gid[cid];
        }
        return -1;
    }
    
    static {
        LOG = LogFactory.getLog(PDCIDFontType2Font.class);
    }
}
