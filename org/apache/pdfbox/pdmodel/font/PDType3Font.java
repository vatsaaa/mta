// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.font;

import org.apache.pdfbox.pdmodel.common.COSObjectable;
import org.apache.pdfbox.pdmodel.common.PDMatrix;
import java.awt.image.ImageObserver;
import java.awt.geom.AffineTransform;
import java.awt.Graphics;
import java.io.IOException;
import org.apache.pdfbox.cos.COSStream;
import org.apache.pdfbox.cos.COSDictionary;
import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSName;
import java.util.HashMap;
import java.awt.Image;
import java.util.Map;

public class PDType3Font extends PDSimpleFont
{
    private Map<Character, Image> images;
    
    public PDType3Font() {
        this.images = new HashMap<Character, Image>();
        this.font.setItem(COSName.SUBTYPE, COSName.TYPE3);
    }
    
    public PDType3Font(final COSDictionary fontDictionary) {
        super(fontDictionary);
        this.images = new HashMap<Character, Image>();
    }
    
    private Image createImageIfNecessary(final char character) throws IOException {
        final Character c = new Character(character);
        Image retval = this.images.get(c);
        if (retval == null) {
            final COSDictionary charProcs = (COSDictionary)this.font.getDictionaryObject(COSName.CHAR_PROCS);
            final COSStream stream = (COSStream)charProcs.getDictionaryObject(COSName.getPDFName("" + character));
            if (stream != null) {
                final Type3StreamParser parser = new Type3StreamParser();
                retval = parser.createImage(stream);
                this.images.put(c, retval);
            }
        }
        return retval;
    }
    
    @Override
    public void drawString(final String string, final int[] codePoints, final Graphics g, final float fontSize, final AffineTransform at, float x, final float y) throws IOException {
        for (int i = 0; i < string.length(); ++i) {
            final char c = string.charAt(i);
            Image image = this.createImageIfNecessary(c);
            if (image != null) {
                final int newWidth = (int)(0.12 * image.getWidth(null));
                final int newHeight = (int)(0.12 * image.getHeight(null));
                if (newWidth > 0 && newHeight > 0) {
                    image = image.getScaledInstance(newWidth, newHeight, 4);
                    g.drawImage(image, (int)x, (int)y, null);
                    x += newWidth;
                }
            }
        }
    }
    
    public void setFontMatrix(final PDMatrix matrix) {
        this.font.setItem(COSName.FONT_MATRIX, matrix);
    }
}
