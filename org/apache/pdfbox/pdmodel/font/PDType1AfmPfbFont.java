// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.font;

import org.apache.pdfbox.cos.COSDictionary;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSInteger;
import org.apache.pdfbox.cos.COSArray;
import org.apache.pdfbox.encoding.DictionaryEncoding;
import java.util.Iterator;
import java.util.List;
import org.apache.fontbox.afm.CharMetric;
import java.util.ArrayList;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.encoding.Encoding;
import org.apache.pdfbox.encoding.AFMEncoding;
import org.apache.fontbox.afm.AFMParser;
import org.apache.pdfbox.pdmodel.common.PDStream;
import org.apache.fontbox.pfb.PfbParser;
import java.io.IOException;
import java.io.InputStream;
import java.io.BufferedInputStream;
import java.io.FileInputStream;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.fontbox.afm.FontMetric;

public class PDType1AfmPfbFont extends PDType1Font
{
    private static final int BUFFERSIZE = 65535;
    private FontMetric metric;
    
    public PDType1AfmPfbFont(final PDDocument doc, final String afmname) throws IOException {
        final InputStream afmin = new BufferedInputStream(new FileInputStream(afmname), 65535);
        final String pfbname = afmname.replaceAll(".AFM", "").replaceAll(".afm", "") + ".pfb";
        final InputStream pfbin = new BufferedInputStream(new FileInputStream(pfbname), 65535);
        this.load(doc, afmin, pfbin);
    }
    
    public PDType1AfmPfbFont(final PDDocument doc, final InputStream afm, final InputStream pfb) throws IOException {
        this.load(doc, afm, pfb);
    }
    
    private void load(final PDDocument doc, final InputStream afm, final InputStream pfb) throws IOException {
        final PDFontDescriptorDictionary fd = new PDFontDescriptorDictionary();
        this.setFontDescriptor(fd);
        final PfbParser pfbparser = new PfbParser(pfb);
        pfb.close();
        final PDStream fontStream = new PDStream(doc, pfbparser.getInputStream(), false);
        fontStream.getStream().setInt("Length", pfbparser.size());
        for (int i = 0; i < pfbparser.getLengths().length; ++i) {
            fontStream.getStream().setInt("Length" + (i + 1), pfbparser.getLengths()[i]);
        }
        fontStream.addCompression();
        fd.setFontFile(fontStream);
        final AFMParser parser = new AFMParser(afm);
        parser.parse();
        this.metric = parser.getResult();
        this.setFontEncoding(this.afmToDictionary(new AFMEncoding(this.metric)));
        this.setBaseFont(this.metric.getFontName());
        fd.setFontName(this.metric.getFontName());
        fd.setFontFamily(this.metric.getFamilyName());
        fd.setNonSymbolic(true);
        fd.setFontBoundingBox(new PDRectangle(this.metric.getFontBBox()));
        fd.setItalicAngle(this.metric.getItalicAngle());
        fd.setAscent(this.metric.getAscender());
        fd.setDescent(this.metric.getDescender());
        fd.setCapHeight(this.metric.getCapHeight());
        fd.setXHeight(this.metric.getXHeight());
        fd.setAverageWidth(this.metric.getAverageCharacterWidth());
        fd.setCharacterSet(this.metric.getCharacterSet());
        int firstchar = 255;
        int lastchar = 0;
        final List<CharMetric> listmetric = this.metric.getCharMetrics();
        final Encoding encoding = this.getFontEncoding();
        final int maxWidths = 256;
        final List<Float> widths = new ArrayList<Float>(maxWidths);
        final float zero = 250.0f;
        final Iterator<CharMetric> iter = listmetric.iterator();
        for (int j = 0; j < maxWidths; ++j) {
            widths.add(zero);
        }
        while (iter.hasNext()) {
            final CharMetric m = iter.next();
            final int n = m.getCharacterCode();
            if (n > 0) {
                firstchar = Math.min(firstchar, n);
                lastchar = Math.max(lastchar, n);
                if (m.getWx() <= 0.0f) {
                    continue;
                }
                final float width = m.getWx();
                widths.set(n, new Float(width));
                if (!m.getName().equals("germandbls") || n == 223) {
                    continue;
                }
                widths.set(223, new Float(width));
            }
            else if (m.getName().equals("adieresis")) {
                widths.set(228, widths.get(encoding.getCode("a")));
            }
            else if (m.getName().equals("odieresis")) {
                widths.set(246, widths.get(encoding.getCode("o")));
            }
            else if (m.getName().equals("udieresis")) {
                widths.set(252, widths.get(encoding.getCode("u")));
            }
            else if (m.getName().equals("Adieresis")) {
                widths.set(196, widths.get(encoding.getCode("A")));
            }
            else if (m.getName().equals("Odieresis")) {
                widths.set(214, widths.get(encoding.getCode("O")));
            }
            else {
                if (!m.getName().equals("Udieresis")) {
                    continue;
                }
                widths.set(220, widths.get(encoding.getCode("U")));
            }
        }
        this.setFirstChar(0);
        this.setLastChar(255);
        this.setWidths(widths);
    }
    
    private DictionaryEncoding afmToDictionary(final AFMEncoding encoding) throws IOException {
        final COSArray array = new COSArray();
        array.add(COSInteger.ZERO);
        for (int i = 0; i < 256; ++i) {
            array.add(COSName.getPDFName(encoding.getName(i)));
        }
        array.set(224, COSName.getPDFName("germandbls"));
        array.set(229, COSName.getPDFName("adieresis"));
        array.set(247, COSName.getPDFName("odieresis"));
        array.set(253, COSName.getPDFName("udieresis"));
        array.set(197, COSName.getPDFName("Adieresis"));
        array.set(215, COSName.getPDFName("Odieresis"));
        array.set(221, COSName.getPDFName("Udieresis"));
        final COSDictionary dictionary = new COSDictionary();
        dictionary.setItem(COSName.NAME, COSName.ENCODING);
        dictionary.setItem(COSName.DIFFERENCES, array);
        dictionary.setItem(COSName.BASE_ENCODING, COSName.STANDARD_ENCODING);
        return new DictionaryEncoding(dictionary);
    }
}
