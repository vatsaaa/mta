// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.font;

import java.io.IOException;
import org.apache.fontbox.util.BoundingBox;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.fontbox.afm.FontMetric;

public class PDFontDescriptorAFM extends PDFontDescriptor
{
    private FontMetric afm;
    
    public PDFontDescriptorAFM(final FontMetric afmFile) {
        this.afm = afmFile;
    }
    
    @Override
    public String getFontName() {
        return this.afm.getFontName();
    }
    
    @Override
    public void setFontName(final String fontName) {
        throw new UnsupportedOperationException("The AFM Font descriptor is immutable");
    }
    
    @Override
    public String getFontFamily() {
        return this.afm.getFamilyName();
    }
    
    @Override
    public void setFontFamily(final String fontFamily) {
        throw new UnsupportedOperationException("The AFM Font descriptor is immutable");
    }
    
    @Override
    public float getFontWeight() {
        final String weight = this.afm.getWeight();
        float retval = 500.0f;
        if (weight != null && weight.equalsIgnoreCase("bold")) {
            retval = 900.0f;
        }
        else if (weight != null && weight.equalsIgnoreCase("light")) {
            retval = 100.0f;
        }
        return retval;
    }
    
    @Override
    public void setFontWeight(final float fontWeight) {
        throw new UnsupportedOperationException("The AFM Font descriptor is immutable");
    }
    
    @Override
    public String getFontStretch() {
        return null;
    }
    
    @Override
    public void setFontStretch(final String fontStretch) {
        throw new UnsupportedOperationException("The AFM Font descriptor is immutable");
    }
    
    @Override
    public int getFlags() {
        return this.afm.isFixedPitch() ? 1 : 0;
    }
    
    @Override
    public void setFlags(final int flags) {
        throw new UnsupportedOperationException("The AFM Font descriptor is immutable");
    }
    
    @Override
    public PDRectangle getFontBoundingBox() {
        final BoundingBox box = this.afm.getFontBBox();
        PDRectangle retval = null;
        if (box != null) {
            retval = new PDRectangle(box);
        }
        return retval;
    }
    
    @Override
    public void setFontBoundingBox(final PDRectangle rect) {
        throw new UnsupportedOperationException("The AFM Font descriptor is immutable");
    }
    
    @Override
    public float getItalicAngle() {
        return this.afm.getItalicAngle();
    }
    
    @Override
    public void setItalicAngle(final float angle) {
        throw new UnsupportedOperationException("The AFM Font descriptor is immutable");
    }
    
    @Override
    public float getAscent() {
        return this.afm.getAscender();
    }
    
    @Override
    public void setAscent(final float ascent) {
        throw new UnsupportedOperationException("The AFM Font descriptor is immutable");
    }
    
    @Override
    public float getDescent() {
        return this.afm.getDescender();
    }
    
    @Override
    public void setDescent(final float descent) {
        throw new UnsupportedOperationException("The AFM Font descriptor is immutable");
    }
    
    @Override
    public float getLeading() {
        return 0.0f;
    }
    
    @Override
    public void setLeading(final float leading) {
        throw new UnsupportedOperationException("The AFM Font descriptor is immutable");
    }
    
    @Override
    public float getCapHeight() {
        return this.afm.getCapHeight();
    }
    
    @Override
    public void setCapHeight(final float capHeight) {
        throw new UnsupportedOperationException("The AFM Font descriptor is immutable");
    }
    
    @Override
    public float getXHeight() {
        return this.afm.getXHeight();
    }
    
    @Override
    public void setXHeight(final float xHeight) {
        throw new UnsupportedOperationException("The AFM Font descriptor is immutable");
    }
    
    @Override
    public float getStemV() {
        return 0.0f;
    }
    
    @Override
    public void setStemV(final float stemV) {
        throw new UnsupportedOperationException("The AFM Font descriptor is immutable");
    }
    
    @Override
    public float getStemH() {
        return 0.0f;
    }
    
    @Override
    public void setStemH(final float stemH) {
        throw new UnsupportedOperationException("The AFM Font descriptor is immutable");
    }
    
    @Override
    public float getAverageWidth() throws IOException {
        return this.afm.getAverageCharacterWidth();
    }
    
    @Override
    public void setAverageWidth(final float averageWidth) {
        throw new UnsupportedOperationException("The AFM Font descriptor is immutable");
    }
    
    @Override
    public float getMaxWidth() {
        return 0.0f;
    }
    
    @Override
    public void setMaxWidth(final float maxWidth) {
        throw new UnsupportedOperationException("The AFM Font descriptor is immutable");
    }
    
    @Override
    public float getMissingWidth() {
        return 0.0f;
    }
    
    @Override
    public void setMissingWidth(final float missingWidth) {
        throw new UnsupportedOperationException("The AFM Font descriptor is immutable");
    }
    
    @Override
    public String getCharSet() {
        return this.afm.getCharacterSet();
    }
    
    @Override
    public void setCharacterSet(final String charSet) {
        throw new UnsupportedOperationException("The AFM Font descriptor is immutable");
    }
}
