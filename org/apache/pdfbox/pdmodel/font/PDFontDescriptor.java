// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.font;

import java.io.IOException;
import org.apache.pdfbox.pdmodel.common.PDRectangle;

public abstract class PDFontDescriptor
{
    private static final int FLAG_FIXED_PITCH = 1;
    private static final int FLAG_SERIF = 2;
    private static final int FLAG_SYMBOLIC = 4;
    private static final int FLAG_SCRIPT = 8;
    private static final int FLAG_NON_SYMBOLIC = 32;
    private static final int FLAG_ITALIC = 64;
    private static final int FLAG_ALL_CAP = 65536;
    private static final int FLAG_SMALL_CAP = 131072;
    private static final int FLAG_FORCE_BOLD = 262144;
    
    public abstract String getFontName();
    
    public abstract void setFontName(final String p0);
    
    public abstract String getFontFamily();
    
    public abstract void setFontFamily(final String p0);
    
    public abstract String getFontStretch();
    
    public abstract void setFontStretch(final String p0);
    
    public abstract float getFontWeight();
    
    public abstract void setFontWeight(final float p0);
    
    public abstract int getFlags();
    
    public abstract void setFlags(final int p0);
    
    public boolean isFixedPitch() {
        return this.isFlagBitOn(1);
    }
    
    public void setFixedPitch(final boolean flag) {
        this.setFlagBit(1, flag);
    }
    
    public boolean isSerif() {
        return this.isFlagBitOn(2);
    }
    
    public void setSerif(final boolean flag) {
        this.setFlagBit(2, flag);
    }
    
    public boolean isSymbolic() {
        return this.isFlagBitOn(4);
    }
    
    public void setSymbolic(final boolean flag) {
        this.setFlagBit(4, flag);
    }
    
    public boolean isScript() {
        return this.isFlagBitOn(8);
    }
    
    public void setScript(final boolean flag) {
        this.setFlagBit(8, flag);
    }
    
    public boolean isNonSymbolic() {
        return this.isFlagBitOn(32);
    }
    
    public void setNonSymbolic(final boolean flag) {
        this.setFlagBit(32, flag);
    }
    
    public boolean isItalic() {
        return this.isFlagBitOn(64);
    }
    
    public void setItalic(final boolean flag) {
        this.setFlagBit(64, flag);
    }
    
    public boolean isAllCap() {
        return this.isFlagBitOn(65536);
    }
    
    public void setAllCap(final boolean flag) {
        this.setFlagBit(65536, flag);
    }
    
    public boolean isSmallCap() {
        return this.isFlagBitOn(131072);
    }
    
    public void setSmallCap(final boolean flag) {
        this.setFlagBit(131072, flag);
    }
    
    public boolean isForceBold() {
        return this.isFlagBitOn(262144);
    }
    
    public void setForceBold(final boolean flag) {
        this.setFlagBit(262144, flag);
    }
    
    private boolean isFlagBitOn(final int bit) {
        return (this.getFlags() & bit) != 0x0;
    }
    
    private void setFlagBit(final int bit, final boolean value) {
        int flags = this.getFlags();
        if (value) {
            flags |= bit;
        }
        else {
            flags &= (-1 ^ bit);
        }
        this.setFlags(flags);
    }
    
    public abstract PDRectangle getFontBoundingBox();
    
    public abstract void setFontBoundingBox(final PDRectangle p0);
    
    public abstract float getItalicAngle();
    
    public abstract void setItalicAngle(final float p0);
    
    public abstract float getAscent();
    
    public abstract void setAscent(final float p0);
    
    public abstract float getDescent();
    
    public abstract void setDescent(final float p0);
    
    public abstract float getLeading();
    
    public abstract void setLeading(final float p0);
    
    public abstract float getCapHeight();
    
    public abstract void setCapHeight(final float p0);
    
    public abstract float getXHeight();
    
    public abstract void setXHeight(final float p0);
    
    public abstract float getStemV();
    
    public abstract void setStemV(final float p0);
    
    public abstract float getStemH();
    
    public abstract void setStemH(final float p0);
    
    public abstract float getAverageWidth() throws IOException;
    
    public abstract void setAverageWidth(final float p0);
    
    public abstract float getMaxWidth();
    
    public abstract void setMaxWidth(final float p0);
    
    public abstract String getCharSet();
    
    public abstract void setCharacterSet(final String p0);
    
    public abstract float getMissingWidth();
    
    public abstract void setMissingWidth(final float p0);
}
