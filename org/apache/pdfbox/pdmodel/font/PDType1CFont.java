// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.font;

import org.apache.commons.logging.LogFactory;
import java.awt.FontFormatException;
import org.apache.pdfbox.exceptions.WrappedIOException;
import org.apache.fontbox.cff.Type1FontFormatter;
import org.apache.fontbox.util.BoundingBox;
import org.apache.fontbox.afm.AFMParser;
import java.io.ByteArrayInputStream;
import org.apache.fontbox.cff.AFMFormatter;
import org.apache.pdfbox.cos.COSNumber;
import org.apache.pdfbox.encoding.EncodingManager;
import org.apache.pdfbox.cos.COSName;
import java.io.InputStream;
import org.apache.pdfbox.pdmodel.common.PDStream;
import java.io.ByteArrayOutputStream;
import java.lang.reflect.Field;
import java.util.Set;
import org.apache.fontbox.cff.charset.CFFCharset;
import org.apache.fontbox.cff.encoding.CFFEncoding;
import org.apache.pdfbox.encoding.Encoding;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedHashMap;
import org.apache.fontbox.cff.CFFParser;
import java.util.Iterator;
import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSFloat;
import org.apache.pdfbox.cos.COSArray;
import java.util.List;
import org.apache.pdfbox.pdmodel.common.PDMatrix;
import java.util.Arrays;
import java.io.IOException;
import java.util.HashMap;
import org.apache.pdfbox.cos.COSDictionary;
import org.apache.commons.logging.Log;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import java.awt.Font;
import org.apache.fontbox.afm.FontMetric;
import java.util.Map;
import org.apache.fontbox.cff.CFFFont;

public class PDType1CFont extends PDSimpleFont
{
    private CFFFont cffFont;
    private Map<Integer, String> codeToName;
    private Map<Integer, String> codeToCharacter;
    private Map<String, Integer> characterToCode;
    private FontMetric fontMetric;
    private Font awtFont;
    private Map<String, Float> glyphWidths;
    private Map<String, Float> glyphHeights;
    private Float avgWidth;
    private PDRectangle fontBBox;
    private static final Log log;
    private static final byte[] SPACE_BYTES;
    private COSDictionary fontDict;
    
    public PDType1CFont(final COSDictionary fontDictionary) throws IOException {
        super(fontDictionary);
        this.cffFont = null;
        this.codeToName = new HashMap<Integer, String>();
        this.codeToCharacter = new HashMap<Integer, String>();
        this.characterToCode = new HashMap<String, Integer>();
        this.fontMetric = null;
        this.awtFont = null;
        this.glyphWidths = new HashMap<String, Float>();
        this.glyphHeights = new HashMap<String, Float>();
        this.avgWidth = null;
        this.fontBBox = null;
        this.fontDict = null;
        this.fontDict = fontDictionary;
        this.load();
    }
    
    @Override
    public String encode(final byte[] bytes, final int offset, final int length) throws IOException {
        final String character = this.getCharacter(bytes, offset, length);
        if (character == null) {
            PDType1CFont.log.debug("No character for code " + (bytes[offset] & 0xFF) + " in " + this.cffFont.getName());
            return null;
        }
        return character;
    }
    
    @Override
    public int encodeToCID(final byte[] bytes, final int offset, final int length) {
        if (length > 2) {
            return -1;
        }
        int code = bytes[offset] & 0xFF;
        if (length == 2) {
            code = (code * 256 + bytes[offset + 1] & 0xFF);
        }
        return code;
    }
    
    private String getCharacter(final byte[] bytes, final int offset, final int length) {
        final int code = this.encodeToCID(bytes, offset, length);
        if (code == -1) {
            return null;
        }
        return this.codeToCharacter.get(code);
    }
    
    @Override
    public float getFontWidth(final byte[] bytes, final int offset, final int length) throws IOException {
        final String name = this.getName(bytes, offset, length);
        if (name == null && !Arrays.equals(PDType1CFont.SPACE_BYTES, bytes)) {
            PDType1CFont.log.debug("No name for code " + (bytes[offset] & 0xFF) + " in " + this.cffFont.getName());
            return 0.0f;
        }
        Float width = this.glyphWidths.get(name);
        if (width == null) {
            width = this.getFontMetric().getCharacterWidth(name);
            this.glyphWidths.put(name, width);
        }
        return width;
    }
    
    @Override
    public float getFontHeight(final byte[] bytes, final int offset, final int length) throws IOException {
        final String name = this.getName(bytes, offset, length);
        if (name == null) {
            PDType1CFont.log.debug("No name for code " + (bytes[offset] & 0xFF) + " in " + this.cffFont.getName());
            return 0.0f;
        }
        Float height = this.glyphHeights.get(name);
        if (height == null) {
            height = this.getFontMetric().getCharacterHeight(name);
            this.glyphHeights.put(name, height);
        }
        return height;
    }
    
    private String getName(final byte[] bytes, final int offset, final int length) {
        if (length > 2) {
            return null;
        }
        int code = bytes[offset] & 0xFF;
        if (length == 2) {
            code = (code * 256 + bytes[offset + 1] & 0xFF);
        }
        return this.codeToName.get(code);
    }
    
    @Override
    public float getStringWidth(final String string) throws IOException {
        float width = 0.0f;
        for (int i = 0; i < string.length(); ++i) {
            final String character = string.substring(i, i + 1);
            final Integer code = this.getCode(character);
            if (code == null) {
                PDType1CFont.log.debug("No code for character " + character);
                return 0.0f;
            }
            width += this.getFontWidth(new byte[] { (byte)(int)code }, 0, 1);
        }
        return width;
    }
    
    private Integer getCode(final String character) {
        return this.characterToCode.get(character);
    }
    
    @Override
    public float getAverageFontWidth() throws IOException {
        if (this.avgWidth == null) {
            this.avgWidth = this.getFontMetric().getAverageCharacterWidth();
        }
        return this.avgWidth;
    }
    
    @Override
    public PDRectangle getFontBoundingBox() throws IOException {
        if (this.fontBBox == null) {
            this.fontBBox = new PDRectangle(this.getFontMetric().getFontBBox());
        }
        return this.fontBBox;
    }
    
    @Override
    public PDMatrix getFontMatrix() {
        if (this.fontMatrix == null) {
            final List<Number> numbers = (List<Number>)this.cffFont.getProperty("FontMatrix");
            if (numbers != null && numbers.size() == 6) {
                final COSArray array = new COSArray();
                for (final Number number : numbers) {
                    array.add(new COSFloat(number.floatValue()));
                }
                this.fontMatrix = new PDMatrix(array);
            }
            else {
                super.getFontMatrix();
            }
        }
        return this.fontMatrix;
    }
    
    @Override
    public Font getawtFont() throws IOException {
        if (this.awtFont == null) {
            this.awtFont = prepareAwtFont(this.cffFont);
        }
        return this.awtFont;
    }
    
    private FontMetric getFontMetric() {
        if (this.fontMetric == null) {
            try {
                this.fontMetric = this.prepareFontMetric(this.cffFont);
            }
            catch (IOException exception) {
                PDType1CFont.log.error("An error occured while extracting the font metrics!", exception);
            }
        }
        return this.fontMetric;
    }
    
    private void load() throws IOException {
        final byte[] cffBytes = this.loadBytes();
        final CFFParser cffParser = new CFFParser();
        final List<CFFFont> fonts = cffParser.parse(cffBytes);
        this.cffFont = fonts.get(0);
        final CFFEncoding encoding = this.cffFont.getEncoding();
        final PDFEncoding pdfEncoding = new PDFEncoding(encoding);
        final CFFCharset charset = this.cffFont.getCharset();
        final PDFCharset pdfCharset = new PDFCharset(charset);
        final Map<String, byte[]> charStringsDict = this.cffFont.getCharStringsDict();
        final Map<String, byte[]> pdfCharStringsDict = new LinkedHashMap<String, byte[]>();
        pdfCharStringsDict.put(".notdef", charStringsDict.get(".notdef"));
        final Map<Integer, String> codeToNameMap = new LinkedHashMap<Integer, String>();
        final Collection<CFFFont.Mapping> mappings = this.cffFont.getMappings();
        for (final CFFFont.Mapping mapping : mappings) {
            final Integer code = mapping.getCode();
            final String name = mapping.getName();
            codeToNameMap.put(code, name);
        }
        final Set<String> knownNames = new HashSet<String>(codeToNameMap.values());
        final Map<Integer, String> codeToNameOverride = this.loadOverride();
        for (final Map.Entry<Integer, String> entry : codeToNameOverride.entrySet()) {
            final Integer code2 = entry.getKey();
            final String name2 = entry.getValue();
            if (knownNames.contains(name2)) {
                codeToNameMap.put(code2, name2);
            }
        }
        Map nameToCharacter;
        try {
            final Field nameToCharacterField = Encoding.class.getDeclaredField("NAME_TO_CHARACTER");
            nameToCharacterField.setAccessible(true);
            nameToCharacter = (Map)nameToCharacterField.get(null);
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }
        for (final Map.Entry<Integer, String> entry2 : codeToNameMap.entrySet()) {
            final Integer code3 = entry2.getKey();
            final String name3 = entry2.getValue();
            String uniName = "uni";
            String character = nameToCharacter.get(name3);
            if (character != null) {
                for (int j = 0; j < character.length(); ++j) {
                    uniName += hexString(character.charAt(j), 4);
                }
            }
            else {
                uniName += hexString(code3, 4);
                character = String.valueOf((char)(int)code3);
            }
            pdfEncoding.register(code3, code3);
            pdfCharset.register(code3, uniName);
            this.codeToName.put(code3, uniName);
            this.codeToCharacter.put(code3, character);
            this.characterToCode.put(character, code3);
            pdfCharStringsDict.put(uniName, charStringsDict.get(name3));
        }
        this.cffFont.setEncoding(pdfEncoding);
        this.cffFont.setCharset(pdfCharset);
        charStringsDict.clear();
        charStringsDict.putAll(pdfCharStringsDict);
        final Number defaultWidthX = (Number)this.cffFont.getProperty("defaultWidthX");
        this.glyphWidths.put(null, defaultWidthX.floatValue());
    }
    
    private byte[] loadBytes() throws IOException {
        final PDFontDescriptor fd = this.getFontDescriptor();
        if (fd != null && fd instanceof PDFontDescriptorDictionary) {
            final PDStream ff3Stream = ((PDFontDescriptorDictionary)fd).getFontFile3();
            if (ff3Stream != null) {
                final ByteArrayOutputStream os = new ByteArrayOutputStream();
                final InputStream is = ff3Stream.createInputStream();
                try {
                    final byte[] buf = new byte[512];
                    while (true) {
                        final int count = is.read(buf);
                        if (count < 0) {
                            break;
                        }
                        os.write(buf, 0, count);
                    }
                }
                finally {
                    is.close();
                }
                return os.toByteArray();
            }
        }
        throw new IOException();
    }
    
    private Map<Integer, String> loadOverride() throws IOException {
        final Map<Integer, String> result = new LinkedHashMap<Integer, String>();
        final COSBase encoding = this.fontDict.getDictionaryObject(COSName.ENCODING);
        if (encoding instanceof COSName) {
            final COSName name = (COSName)encoding;
            result.putAll(this.loadEncoding(name));
        }
        else if (encoding instanceof COSDictionary) {
            final COSDictionary encodingDic = (COSDictionary)encoding;
            final COSName baseName = (COSName)encodingDic.getDictionaryObject(COSName.BASE_ENCODING);
            if (baseName != null) {
                result.putAll(this.loadEncoding(baseName));
            }
            final COSArray differences = (COSArray)encodingDic.getDictionaryObject(COSName.DIFFERENCES);
            if (differences != null) {
                result.putAll(this.loadDifferences(differences));
            }
        }
        return result;
    }
    
    private Map<Integer, String> loadEncoding(final COSName name) throws IOException {
        final Map<Integer, String> result = new LinkedHashMap<Integer, String>();
        final Encoding encoding = EncodingManager.INSTANCE.getEncoding(name);
        for (final Map.Entry<Integer, String> entry : encoding.getCodeToNameMap().entrySet()) {
            result.put(entry.getKey(), entry.getValue());
        }
        return result;
    }
    
    private Map<Integer, String> loadDifferences(final COSArray differences) {
        final Map<Integer, String> result = new LinkedHashMap<Integer, String>();
        Integer code = null;
        for (int i = 0; i < differences.size(); ++i) {
            final COSBase element = differences.get(i);
            if (element instanceof COSNumber) {
                final COSNumber number = (COSNumber)element;
                code = number.intValue();
            }
            else if (element instanceof COSName) {
                final COSName name = (COSName)element;
                result.put(code, name.getName());
                ++code;
            }
        }
        return result;
    }
    
    private static String hexString(final int code, final int length) {
        String string;
        for (string = Integer.toHexString(code); string.length() < length; string = "0" + string) {}
        return string;
    }
    
    private FontMetric prepareFontMetric(final CFFFont font) throws IOException {
        final byte[] afmBytes = AFMFormatter.format(font);
        final InputStream is = new ByteArrayInputStream(afmBytes);
        try {
            final AFMParser afmParser = new AFMParser(is);
            afmParser.parse();
            final FontMetric result = afmParser.getResult();
            final BoundingBox bounds = result.getFontBBox();
            final List<Integer> numbers = Arrays.asList((int)bounds.getLowerLeftX(), (int)bounds.getLowerLeftY(), (int)bounds.getUpperRightX(), (int)bounds.getUpperRightY());
            font.addValueToTopDict("FontBBox", numbers);
            return result;
        }
        finally {
            is.close();
        }
    }
    
    private static Font prepareAwtFont(final CFFFont font) throws IOException {
        final byte[] type1Bytes = Type1FontFormatter.format(font);
        final InputStream is = new ByteArrayInputStream(type1Bytes);
        try {
            return Font.createFont(1, is);
        }
        catch (FontFormatException ffe) {
            throw new WrappedIOException(ffe);
        }
        finally {
            is.close();
        }
    }
    
    static {
        log = LogFactory.getLog(PDType1CFont.class);
        SPACE_BYTES = new byte[] { 32 };
    }
    
    private static class PDFEncoding extends CFFEncoding
    {
        private PDFEncoding(final CFFEncoding parent) {
            final Iterator<Entry> parentEntries = parent.getEntries().iterator();
            while (parentEntries.hasNext()) {
                this.addEntry(parentEntries.next());
            }
        }
        
        @Override
        public boolean isFontSpecific() {
            return true;
        }
    }
    
    private static class PDFCharset extends CFFCharset
    {
        private PDFCharset(final CFFCharset parent) {
            final Iterator<Entry> parentEntries = parent.getEntries().iterator();
            while (parentEntries.hasNext()) {
                this.addEntry(parentEntries.next());
            }
        }
        
        @Override
        public boolean isFontSpecific() {
            return true;
        }
    }
}
