// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.font;

import org.apache.pdfbox.cos.COSStream;
import org.apache.pdfbox.pdmodel.common.PDStream;
import org.apache.pdfbox.cos.COSArray;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.cos.COSString;
import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.cos.COSDictionary;
import org.apache.pdfbox.pdmodel.common.COSObjectable;

public class PDFontDescriptorDictionary extends PDFontDescriptor implements COSObjectable
{
    private COSDictionary dic;
    private float xHeight;
    private float capHeight;
    private int flags;
    
    public PDFontDescriptorDictionary() {
        this.xHeight = Float.NEGATIVE_INFINITY;
        this.capHeight = Float.NEGATIVE_INFINITY;
        this.flags = -1;
        (this.dic = new COSDictionary()).setItem(COSName.TYPE, COSName.FONT_DESC);
    }
    
    public PDFontDescriptorDictionary(final COSDictionary desc) {
        this.xHeight = Float.NEGATIVE_INFINITY;
        this.capHeight = Float.NEGATIVE_INFINITY;
        this.flags = -1;
        this.dic = desc;
    }
    
    public COSDictionary getCOSDictionary() {
        return this.dic;
    }
    
    public COSBase getCOSObject() {
        return this.dic;
    }
    
    @Override
    public String getFontName() {
        String retval = null;
        final COSName name = (COSName)this.dic.getDictionaryObject(COSName.FONT_NAME);
        if (name != null) {
            retval = name.getName();
        }
        return retval;
    }
    
    @Override
    public void setFontName(final String fontName) {
        COSName name = null;
        if (fontName != null) {
            name = COSName.getPDFName(fontName);
        }
        this.dic.setItem(COSName.FONT_NAME, name);
    }
    
    @Override
    public String getFontFamily() {
        String retval = null;
        final COSString name = (COSString)this.dic.getDictionaryObject(COSName.FONT_FAMILY);
        if (name != null) {
            retval = name.getString();
        }
        return retval;
    }
    
    @Override
    public void setFontFamily(final String fontFamily) {
        COSString name = null;
        if (fontFamily != null) {
            name = new COSString(fontFamily);
        }
        this.dic.setItem(COSName.FONT_FAMILY, name);
    }
    
    @Override
    public float getFontWeight() {
        return this.dic.getFloat(COSName.FONT_WEIGHT, 0.0f);
    }
    
    @Override
    public void setFontWeight(final float fontWeight) {
        this.dic.setFloat(COSName.FONT_WEIGHT, fontWeight);
    }
    
    @Override
    public String getFontStretch() {
        String retval = null;
        final COSName name = (COSName)this.dic.getDictionaryObject(COSName.FONT_STRETCH);
        if (name != null) {
            retval = name.getName();
        }
        return retval;
    }
    
    @Override
    public void setFontStretch(final String fontStretch) {
        COSName name = null;
        if (fontStretch != null) {
            name = COSName.getPDFName(fontStretch);
        }
        this.dic.setItem(COSName.FONT_STRETCH, name);
    }
    
    @Override
    public int getFlags() {
        if (this.flags == -1) {
            this.flags = this.dic.getInt(COSName.FLAGS, 0);
        }
        return this.flags;
    }
    
    @Override
    public void setFlags(final int flags) {
        this.dic.setInt(COSName.FLAGS, flags);
        this.flags = flags;
    }
    
    @Override
    public PDRectangle getFontBoundingBox() {
        final COSArray rect = (COSArray)this.dic.getDictionaryObject(COSName.FONT_BBOX);
        PDRectangle retval = null;
        if (rect != null) {
            retval = new PDRectangle(rect);
        }
        return retval;
    }
    
    @Override
    public void setFontBoundingBox(final PDRectangle rect) {
        COSArray array = null;
        if (rect != null) {
            array = rect.getCOSArray();
        }
        this.dic.setItem(COSName.FONT_BBOX, array);
    }
    
    @Override
    public float getItalicAngle() {
        return this.dic.getFloat(COSName.ITALIC_ANGLE, 0.0f);
    }
    
    @Override
    public void setItalicAngle(final float angle) {
        this.dic.setFloat(COSName.ITALIC_ANGLE, angle);
    }
    
    @Override
    public float getAscent() {
        return this.dic.getFloat(COSName.ASCENT, 0.0f);
    }
    
    @Override
    public void setAscent(final float ascent) {
        this.dic.setFloat(COSName.ASCENT, ascent);
    }
    
    @Override
    public float getDescent() {
        return this.dic.getFloat(COSName.DESCENT, 0.0f);
    }
    
    @Override
    public void setDescent(final float descent) {
        this.dic.setFloat(COSName.DESCENT, descent);
    }
    
    @Override
    public float getLeading() {
        return this.dic.getFloat(COSName.LEADING, 0.0f);
    }
    
    @Override
    public void setLeading(final float leading) {
        this.dic.setFloat(COSName.LEADING, leading);
    }
    
    @Override
    public float getCapHeight() {
        if (this.capHeight == Float.NEGATIVE_INFINITY) {
            this.capHeight = Math.abs(this.dic.getFloat(COSName.CAP_HEIGHT, 0.0f));
        }
        return this.capHeight;
    }
    
    @Override
    public void setCapHeight(final float capHeight) {
        this.dic.setFloat(COSName.CAP_HEIGHT, capHeight);
        this.capHeight = capHeight;
    }
    
    @Override
    public float getXHeight() {
        if (this.xHeight == Float.NEGATIVE_INFINITY) {
            this.xHeight = Math.abs(this.dic.getFloat(COSName.XHEIGHT, 0.0f));
        }
        return this.xHeight;
    }
    
    @Override
    public void setXHeight(final float xHeight) {
        this.dic.setFloat(COSName.XHEIGHT, xHeight);
        this.xHeight = xHeight;
    }
    
    @Override
    public float getStemV() {
        return this.dic.getFloat(COSName.STEM_V, 0.0f);
    }
    
    @Override
    public void setStemV(final float stemV) {
        this.dic.setFloat(COSName.STEM_V, stemV);
    }
    
    @Override
    public float getStemH() {
        return this.dic.getFloat(COSName.STEM_H, 0.0f);
    }
    
    @Override
    public void setStemH(final float stemH) {
        this.dic.setFloat(COSName.STEM_H, stemH);
    }
    
    @Override
    public float getAverageWidth() {
        return this.dic.getFloat(COSName.AVG_WIDTH, 0.0f);
    }
    
    @Override
    public void setAverageWidth(final float averageWidth) {
        this.dic.setFloat(COSName.AVG_WIDTH, averageWidth);
    }
    
    @Override
    public float getMaxWidth() {
        return this.dic.getFloat(COSName.MAX_WIDTH, 0.0f);
    }
    
    @Override
    public void setMaxWidth(final float maxWidth) {
        this.dic.setFloat(COSName.MAX_WIDTH, maxWidth);
    }
    
    @Override
    public float getMissingWidth() {
        return this.dic.getFloat(COSName.MISSING_WIDTH, 0.0f);
    }
    
    @Override
    public void setMissingWidth(final float missingWidth) {
        this.dic.setFloat(COSName.MISSING_WIDTH, missingWidth);
    }
    
    @Override
    public String getCharSet() {
        String retval = null;
        final COSString name = (COSString)this.dic.getDictionaryObject(COSName.CHAR_SET);
        if (name != null) {
            retval = name.getString();
        }
        return retval;
    }
    
    @Override
    public void setCharacterSet(final String charSet) {
        COSString name = null;
        if (charSet != null) {
            name = new COSString(charSet);
        }
        this.dic.setItem(COSName.CHAR_SET, name);
    }
    
    public PDStream getFontFile() {
        PDStream retval = null;
        final COSStream stream = (COSStream)this.dic.getDictionaryObject(COSName.FONT_FILE);
        if (stream != null) {
            retval = new PDStream(stream);
        }
        return retval;
    }
    
    public void setFontFile(final PDStream type1Stream) {
        this.dic.setItem(COSName.FONT_FILE, type1Stream);
    }
    
    public PDStream getFontFile2() {
        PDStream retval = null;
        final COSStream stream = (COSStream)this.dic.getDictionaryObject(COSName.FONT_FILE2);
        if (stream != null) {
            retval = new PDStream(stream);
        }
        return retval;
    }
    
    public void setFontFile2(final PDStream ttfStream) {
        this.dic.setItem(COSName.FONT_FILE2, ttfStream);
    }
    
    public PDStream getFontFile3() {
        PDStream retval = null;
        final COSStream stream = (COSStream)this.dic.getDictionaryObject(COSName.FONT_FILE3);
        if (stream != null) {
            retval = new PDStream(stream);
        }
        return retval;
    }
    
    public void setFontFile3(final PDStream stream) {
        this.dic.setItem(COSName.FONT_FILE3, stream);
    }
}
