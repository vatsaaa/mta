// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.font;

import org.apache.pdfbox.cos.COSDictionary;
import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSName;

public class PDMMType1Font extends PDSimpleFont
{
    public PDMMType1Font() {
        this.font.setItem(COSName.SUBTYPE, COSName.MM_TYPE1);
    }
    
    public PDMMType1Font(final COSDictionary fontDictionary) {
        super(fontDictionary);
    }
}
