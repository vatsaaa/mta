// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.font;

import java.util.HashMap;
import org.apache.commons.logging.LogFactory;
import org.apache.pdfbox.util.ResourceLoader;
import java.awt.FontFormatException;
import org.apache.fontbox.ttf.HorizontalMetricsTable;
import org.apache.fontbox.ttf.CMAPEncodingEntry;
import org.apache.fontbox.ttf.CMAPTable;
import org.apache.fontbox.ttf.PostScriptTable;
import org.apache.fontbox.ttf.GlyphData;
import org.apache.fontbox.ttf.GlyphTable;
import org.apache.fontbox.ttf.HorizontalHeaderTable;
import org.apache.fontbox.ttf.HeaderTable;
import org.apache.fontbox.ttf.OS2WindowsMetricsTable;
import java.util.List;
import org.apache.fontbox.ttf.NamingTable;
import java.util.ArrayList;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.fontbox.ttf.NameRecord;
import org.apache.fontbox.ttf.TTFParser;
import org.apache.pdfbox.encoding.Encoding;
import org.apache.pdfbox.encoding.WinAnsiEncoding;
import org.apache.pdfbox.pdmodel.common.PDStream;
import java.io.InputStream;
import java.io.FileInputStream;
import java.io.File;
import org.apache.pdfbox.pdmodel.PDDocument;
import java.io.IOException;
import org.apache.pdfbox.cos.COSDictionary;
import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSName;
import org.apache.fontbox.ttf.TrueTypeFont;
import java.util.Map;
import java.util.Properties;
import java.awt.Font;
import org.apache.commons.logging.Log;

public class PDTrueTypeFont extends PDSimpleFont
{
    private static final Log log;
    public static final String UNKNOWN_FONT = "UNKNOWN_FONT";
    private Font awtFont;
    private static Properties externalFonts;
    private static Map<String, TrueTypeFont> loadedExternalFonts;
    
    public PDTrueTypeFont() {
        this.awtFont = null;
        this.font.setItem(COSName.SUBTYPE, COSName.TRUE_TYPE);
    }
    
    public PDTrueTypeFont(final COSDictionary fontDictionary) throws IOException {
        super(fontDictionary);
        this.awtFont = null;
        this.ensureFontDescriptor();
    }
    
    public static PDTrueTypeFont loadTTF(final PDDocument doc, final String file) throws IOException {
        return loadTTF(doc, new File(file));
    }
    
    public static PDTrueTypeFont loadTTF(final PDDocument doc, final File file) throws IOException {
        return loadTTF(doc, new FileInputStream(file));
    }
    
    public static PDTrueTypeFont loadTTF(final PDDocument doc, InputStream stream) throws IOException {
        final PDTrueTypeFont retval = new PDTrueTypeFont();
        final PDFontDescriptorDictionary fd = new PDFontDescriptorDictionary();
        retval.setFontDescriptor(fd);
        final PDStream fontStream = new PDStream(doc, stream, false);
        fontStream.getStream().setInt(COSName.LENGTH1, fontStream.getByteArray().length);
        fontStream.addCompression();
        fd.setFontFile2(fontStream);
        stream = fontStream.createInputStream();
        try {
            retval.loadDescriptorDictionary(fd, stream);
        }
        finally {
            stream.close();
        }
        retval.setFontEncoding(new WinAnsiEncoding());
        retval.setEncoding(COSName.WIN_ANSI_ENCODING);
        return retval;
    }
    
    private void ensureFontDescriptor() throws IOException {
        if (this.getFontDescriptor() == null) {
            final PDFontDescriptorDictionary fdd = new PDFontDescriptorDictionary();
            this.setFontDescriptor(fdd);
            final InputStream ttfData = this.getExternalTTFData();
            if (ttfData != null) {
                try {
                    this.loadDescriptorDictionary(fdd, ttfData);
                }
                finally {
                    ttfData.close();
                }
            }
        }
    }
    
    private void loadDescriptorDictionary(final PDFontDescriptorDictionary fd, final InputStream ttfData) throws IOException {
        TrueTypeFont ttf = null;
        try {
            final TTFParser parser = new TTFParser();
            ttf = parser.parseTTF(ttfData);
            final NamingTable naming = ttf.getNaming();
            final List<NameRecord> records = naming.getNameRecords();
            for (int i = 0; i < records.size(); ++i) {
                final NameRecord nr = records.get(i);
                if (nr.getNameId() == 6) {
                    this.setBaseFont(nr.getString());
                    fd.setFontName(nr.getString());
                }
                else if (nr.getNameId() == 1) {
                    fd.setFontFamily(nr.getString());
                }
            }
            final OS2WindowsMetricsTable os2 = ttf.getOS2Windows();
            boolean isSymbolic = false;
            switch (os2.getFamilyClass()) {
                case 12: {
                    isSymbolic = true;
                    break;
                }
                case 10: {
                    fd.setScript(true);
                    break;
                }
                case 1:
                case 3:
                case 4:
                case 5:
                case 7: {
                    fd.setSerif(true);
                    break;
                }
            }
            switch (os2.getWidthClass()) {
                case 1: {
                    fd.setFontStretch("UltraCondensed");
                    break;
                }
                case 2: {
                    fd.setFontStretch("ExtraCondensed");
                    break;
                }
                case 3: {
                    fd.setFontStretch("Condensed");
                    break;
                }
                case 4: {
                    fd.setFontStretch("SemiCondensed");
                    break;
                }
                case 5: {
                    fd.setFontStretch("Normal");
                    break;
                }
                case 6: {
                    fd.setFontStretch("SemiExpanded");
                    break;
                }
                case 7: {
                    fd.setFontStretch("Expanded");
                    break;
                }
                case 8: {
                    fd.setFontStretch("ExtraExpanded");
                    break;
                }
                case 9: {
                    fd.setFontStretch("UltraExpanded");
                    break;
                }
            }
            fd.setFontWeight((float)os2.getWeightClass());
            fd.setSymbolic(isSymbolic);
            fd.setNonSymbolic(!isSymbolic);
            final HeaderTable header = ttf.getHeader();
            final PDRectangle rect = new PDRectangle();
            final float scaling = 1000.0f / header.getUnitsPerEm();
            rect.setLowerLeftX(header.getXMin() * 1000.0f / header.getUnitsPerEm());
            rect.setLowerLeftY(header.getYMin() * 1000.0f / header.getUnitsPerEm());
            rect.setUpperRightX(header.getXMax() * 1000.0f / header.getUnitsPerEm());
            rect.setUpperRightY(header.getYMax() * 1000.0f / header.getUnitsPerEm());
            fd.setFontBoundingBox(rect);
            final HorizontalHeaderTable hHeader = ttf.getHorizontalHeader();
            fd.setAscent(hHeader.getAscender() * 1000.0f / header.getUnitsPerEm());
            fd.setDescent(hHeader.getDescender() * 1000.0f / header.getUnitsPerEm());
            final GlyphTable glyphTable = ttf.getGlyph();
            final GlyphData[] glyphs = glyphTable.getGlyphs();
            final PostScriptTable ps = ttf.getPostScript();
            fd.setFixedPitch(ps.getIsFixedPitch() > 0L);
            fd.setItalicAngle(ps.getItalicAngle());
            final String[] names = ps.getGlyphNames();
            if (names != null) {
                for (int j = 0; j < names.length; ++j) {
                    if (names[j].equals("H")) {
                        fd.setCapHeight(glyphs[j].getBoundingBox().getUpperRightY() * 1000.0f / header.getUnitsPerEm());
                    }
                    if (names[j].equals("x")) {
                        fd.setXHeight(glyphs[j].getBoundingBox().getUpperRightY() * 1000.0f / header.getUnitsPerEm());
                    }
                }
            }
            fd.setStemV(fd.getFontBoundingBox().getWidth() * 0.13f);
            final CMAPTable cmapTable = ttf.getCMAP();
            final CMAPEncodingEntry[] cmaps = cmapTable.getCmaps();
            int[] glyphToCCode = null;
            for (int k = 0; k < cmaps.length; ++k) {
                if (cmaps[k].getPlatformId() == 3) {
                    final int platformEncoding = cmaps[k].getPlatformEncodingId();
                    if ((isSymbolic && 0 == platformEncoding) || 1 == platformEncoding) {
                        glyphToCCode = cmaps[k].getGlyphIdToCharacterCode();
                        break;
                    }
                }
            }
            final int firstChar = os2.getFirstCharIndex();
            final int maxWidths = glyphToCCode.length;
            final HorizontalMetricsTable hMet = ttf.getHorizontalMetrics();
            final int[] widthValues = hMet.getAdvanceWidth();
            final List<Float> widths = new ArrayList<Float>(maxWidths);
            final float zero = 250.0f;
            for (int l = 0; l < maxWidths; ++l) {
                widths.add(zero);
            }
            for (int l = 0; l < maxWidths; ++l) {
                if (glyphToCCode[l] - firstChar < widths.size() && glyphToCCode[l] - firstChar >= 0 && widths.get(glyphToCCode[l] - firstChar) == zero) {
                    widths.set(glyphToCCode[l] - firstChar, widthValues[l] * scaling);
                }
            }
            this.setWidths(widths);
            this.setFirstChar(isSymbolic ? 0 : firstChar);
            this.setLastChar(isSymbolic ? widths.size() : (firstChar + widths.size() - 1));
        }
        finally {
            if (ttf != null) {
                ttf.close();
            }
        }
    }
    
    @Override
    public Font getawtFont() throws IOException {
        final PDFontDescriptorDictionary fd = (PDFontDescriptorDictionary)this.getFontDescriptor();
        if (this.awtFont == null) {
            final PDStream ff2Stream = fd.getFontFile2();
            if (ff2Stream != null) {
                try {
                    this.awtFont = Font.createFont(0, ff2Stream.createInputStream());
                }
                catch (FontFormatException f) {
                    PDTrueTypeFont.log.info("Can't read the embedded font " + fd.getFontName());
                }
                if (this.awtFont == null) {
                    this.awtFont = FontManager.getAwtFont(fd.getFontName());
                    if (this.awtFont != null) {
                        PDTrueTypeFont.log.info("Using font " + this.awtFont.getName() + " instead");
                    }
                    this.setIsFontSubstituted(true);
                }
            }
            else {
                this.awtFont = FontManager.getAwtFont(fd.getFontName());
                if (this.awtFont == null) {
                    PDTrueTypeFont.log.info("Can't find the specified font " + fd.getFontName());
                    final TrueTypeFont ttf = this.getExternalFontFile2(fd);
                    if (ttf != null) {
                        try {
                            this.awtFont = Font.createFont(0, ttf.getOriginalData());
                        }
                        catch (FontFormatException f2) {
                            PDTrueTypeFont.log.info("Can't read the external fontfile " + fd.getFontName());
                        }
                    }
                }
            }
            if (this.awtFont == null) {
                this.awtFont = FontManager.getStandardFont();
                PDTrueTypeFont.log.info("Using font " + this.awtFont.getName() + " instead");
                this.setIsFontSubstituted(true);
            }
        }
        return this.awtFont;
    }
    
    private InputStream getExternalTTFData() throws IOException {
        String ttfResource = PDTrueTypeFont.externalFonts.getProperty("UNKNOWN_FONT");
        final String baseFont = this.getBaseFont();
        if (baseFont != null && PDTrueTypeFont.externalFonts.containsKey(baseFont)) {
            ttfResource = PDTrueTypeFont.externalFonts.getProperty(baseFont);
        }
        return (ttfResource != null) ? ResourceLoader.loadResource(ttfResource) : null;
    }
    
    private TrueTypeFont getExternalFontFile2(final PDFontDescriptorDictionary fd) throws IOException {
        TrueTypeFont retval = null;
        if (fd != null) {
            final String baseFont = this.getBaseFont();
            String fontResource = PDTrueTypeFont.externalFonts.getProperty("UNKNOWN_FONT");
            if (baseFont != null && PDTrueTypeFont.externalFonts.containsKey(baseFont)) {
                fontResource = PDTrueTypeFont.externalFonts.getProperty(baseFont);
            }
            if (fontResource != null) {
                retval = PDTrueTypeFont.loadedExternalFonts.get(baseFont);
                if (retval == null) {
                    final TTFParser ttfParser = new TTFParser();
                    final InputStream fontStream = ResourceLoader.loadResource(fontResource);
                    if (fontStream == null) {
                        throw new IOException("Error missing font resource '" + PDTrueTypeFont.externalFonts.get(baseFont) + "'");
                    }
                    retval = ttfParser.parseTTF(fontStream);
                    PDTrueTypeFont.loadedExternalFonts.put(baseFont, retval);
                }
            }
        }
        return retval;
    }
    
    static {
        log = LogFactory.getLog(PDTrueTypeFont.class);
        PDTrueTypeFont.externalFonts = new Properties();
        PDTrueTypeFont.loadedExternalFonts = new HashMap<String, TrueTypeFont>();
        try {
            ResourceLoader.loadProperties("org/apache/pdfbox/resources/PDFBox_External_Fonts.properties", PDTrueTypeFont.externalFonts);
        }
        catch (IOException io) {
            throw new RuntimeException("Error loading font resources", io);
        }
    }
}
