// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.font;

import org.apache.commons.logging.LogFactory;
import org.apache.pdfbox.cos.COSName;
import java.io.IOException;
import java.util.Map;
import org.apache.pdfbox.cos.COSDictionary;
import org.apache.commons.logging.Log;

public class PDFontFactory
{
    private static final Log LOG;
    
    private PDFontFactory() {
    }
    
    @Deprecated
    public static PDFont createFont(final COSDictionary dic, final Map fontCache) throws IOException {
        return createFont(dic);
    }
    
    public static PDFont createFont(final COSDictionary dic) throws IOException {
        PDFont retval = null;
        final COSName type = (COSName)dic.getDictionaryObject(COSName.TYPE);
        if (!type.equals(COSName.FONT)) {
            throw new IOException("Cannot create font if /Type is not /Font.  Actual=" + type);
        }
        final COSName subType = (COSName)dic.getDictionaryObject(COSName.SUBTYPE);
        if (subType.equals(COSName.TYPE1)) {
            retval = new PDType1Font(dic);
        }
        else if (subType.equals(COSName.MM_TYPE1)) {
            retval = new PDMMType1Font(dic);
        }
        else if (subType.equals(COSName.TRUE_TYPE)) {
            retval = new PDTrueTypeFont(dic);
        }
        else if (subType.equals(COSName.TYPE3)) {
            retval = new PDType3Font(dic);
        }
        else if (subType.equals(COSName.TYPE0)) {
            retval = new PDType0Font(dic);
        }
        else if (subType.equals(COSName.CID_FONT_TYPE0)) {
            retval = new PDCIDFontType0Font(dic);
        }
        else if (subType.equals(COSName.CID_FONT_TYPE2)) {
            retval = new PDCIDFontType2Font(dic);
        }
        else {
            PDFontFactory.LOG.warn("Substituting TrueType for unknown font subtype=" + dic.getDictionaryObject(COSName.SUBTYPE).toString());
            retval = new PDTrueTypeFont(dic);
        }
        return retval;
    }
    
    static {
        LOG = LogFactory.getLog(PDFontFactory.class);
    }
}
