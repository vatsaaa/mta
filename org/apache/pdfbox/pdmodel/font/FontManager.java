// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.font;

import java.io.IOException;
import org.apache.pdfbox.util.ResourceLoader;
import java.util.Enumeration;
import java.awt.GraphicsEnvironment;
import java.awt.Font;
import java.util.Properties;
import java.util.HashMap;

public class FontManager
{
    private static HashMap envFonts;
    private static String standardFont;
    private static Properties fontMapping;
    
    private FontManager() {
    }
    
    public static Font getStandardFont() {
        if (FontManager.standardFont != null) {
            return getAwtFont(FontManager.standardFont);
        }
        return null;
    }
    
    public static Font getAwtFont(final String font) {
        final String fontname = normalizeFontname(font);
        if (FontManager.envFonts.containsKey(fontname)) {
            return FontManager.envFonts.get(fontname);
        }
        return null;
    }
    
    private static void loadFonts() {
        for (final Font font : GraphicsEnvironment.getLocalGraphicsEnvironment().getAllFonts()) {
            final String family = normalizeFontname(font.getFamily());
            final String psname = normalizeFontname(font.getPSName());
            if (isBoldItalic(font)) {
                FontManager.envFonts.put(family + "bolditalic", font);
            }
            else if (isBold(font)) {
                FontManager.envFonts.put(family + "bold", font);
            }
            else if (isItalic(font)) {
                FontManager.envFonts.put(family + "italic", font);
            }
            else {
                FontManager.envFonts.put(family, font);
            }
            if (!family.equals(psname)) {
                FontManager.envFonts.put(normalizeFontname(font.getPSName()), font);
            }
        }
    }
    
    private static void setStandardFont() {
        if (FontManager.envFonts.containsKey("arial")) {
            FontManager.standardFont = "arial";
        }
        else if (FontManager.envFonts.containsKey("timesnewroman")) {
            FontManager.standardFont = "timesnewroman";
        }
    }
    
    private static String normalizeFontname(final String fontname) {
        String normalizedFontname = fontname.toLowerCase().replaceAll(" ", "").replaceAll(",", "").replaceAll("-", "");
        if (normalizedFontname.indexOf("+") > -1) {
            normalizedFontname = normalizedFontname.substring(normalizedFontname.indexOf("+") + 1);
        }
        final boolean isBold = normalizedFontname.indexOf("bold") > -1;
        final boolean isItalic = normalizedFontname.indexOf("italic") > -1 || normalizedFontname.indexOf("oblique") > -1;
        normalizedFontname = normalizedFontname.toLowerCase().replaceAll("bold", "").replaceAll("italic", "").replaceAll("oblique", "");
        if (isBold) {
            normalizedFontname += "bold";
        }
        if (isItalic) {
            normalizedFontname += "italic";
        }
        return normalizedFontname;
    }
    
    private static boolean addFontMapping(final String font, final String mappedName) {
        final String fontname = normalizeFontname(font);
        if (FontManager.envFonts.containsKey(fontname)) {
            return false;
        }
        final String mappedFontname = normalizeFontname(mappedName);
        if (!FontManager.envFonts.containsKey(mappedFontname)) {
            return false;
        }
        FontManager.envFonts.put(fontname, FontManager.envFonts.get(mappedFontname));
        return true;
    }
    
    private static void loadFontMapping() {
        for (boolean addedMapping = true; addedMapping; addedMapping = false) {
            int counter = 0;
            final Enumeration keys = FontManager.fontMapping.keys();
            while (keys.hasMoreElements()) {
                final String key = keys.nextElement();
                if (addFontMapping(key, (String)FontManager.fontMapping.get(key))) {
                    ++counter;
                }
            }
            if (counter == 0) {}
        }
    }
    
    private static void loadBasefontMapping() {
        addFontMapping("Times-Roman", "TimesNewRoman");
        addFontMapping("Times-Bold", "TimesNewRoman,Bold");
        addFontMapping("Times-Italic", "TimesNewRoman,Italic");
        addFontMapping("Times-BoldItalic", "TimesNewRoman,Bold,Italic");
        addFontMapping("Helvetica-Oblique", "Helvetica,Italic");
        addFontMapping("Helvetica-BoldOblique", "Helvetica,Bold,Italic");
        addFontMapping("Courier-Oblique", "Courier,Italic");
        addFontMapping("Courier-BoldOblique", "Courier,Bold,Italic");
    }
    
    private static boolean isBoldItalic(final Font font) {
        return isBold(font) && isItalic(font);
    }
    
    private static boolean isBold(final Font font) {
        final String name = font.getName().toLowerCase();
        if (name.indexOf("bold") > -1) {
            return true;
        }
        final String psname = font.getPSName().toLowerCase();
        return psname.indexOf("bold") > -1;
    }
    
    private static boolean isItalic(final Font font) {
        final String name = font.getName().toLowerCase();
        if (name.indexOf("italic") > -1 || name.indexOf("oblique") > -1) {
            return true;
        }
        final String psname = font.getPSName().toLowerCase();
        return psname.indexOf("italic") > -1 || psname.indexOf("oblique") > -1;
    }
    
    static {
        FontManager.envFonts = new HashMap();
        FontManager.standardFont = null;
        FontManager.fontMapping = new Properties();
        try {
            ResourceLoader.loadProperties("org/apache/pdfbox/resources/FontMapping.properties", FontManager.fontMapping);
        }
        catch (IOException io) {
            io.printStackTrace();
            throw new RuntimeException("Error loading font mapping");
        }
        loadFonts();
        loadFontMapping();
        loadBasefontMapping();
        setStandardFont();
    }
}
