// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.font;

import org.apache.commons.logging.LogFactory;
import org.apache.pdfbox.util.ResourceLoader;
import org.apache.fontbox.cmap.CMap;
import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSArray;
import java.util.HashMap;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.cos.COSNumber;
import java.io.IOException;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.cos.COSDictionary;
import java.util.Map;
import org.apache.commons.logging.Log;

public abstract class PDCIDFont extends PDSimpleFont
{
    private static final Log log;
    private Map<Integer, Float> widthCache;
    private long defaultWidth;
    
    public PDCIDFont() {
        this.widthCache = null;
        this.defaultWidth = 0L;
    }
    
    public PDCIDFont(final COSDictionary fontDictionary) {
        super(fontDictionary);
        this.widthCache = null;
        this.defaultWidth = 0L;
        this.extractWidths();
    }
    
    @Override
    public PDRectangle getFontBoundingBox() throws IOException {
        throw new RuntimeException("getFontBoundingBox(): Not yet implemented");
    }
    
    public long getDefaultWidth() {
        if (this.defaultWidth == 0L) {
            final COSNumber number = (COSNumber)this.font.getDictionaryObject(COSName.DW);
            if (number != null) {
                this.defaultWidth = number.intValue();
            }
            else {
                this.defaultWidth = 1000L;
            }
        }
        return this.defaultWidth;
    }
    
    public void setDefaultWidth(final long dw) {
        this.defaultWidth = dw;
        this.font.setLong(COSName.DW, dw);
    }
    
    @Override
    public float getFontWidth(final byte[] c, final int offset, final int length) throws IOException {
        float retval = (float)this.getDefaultWidth();
        final int code = this.getCodeFromArray(c, offset, length);
        final Float widthFloat = this.widthCache.get(code);
        if (widthFloat != null) {
            retval = widthFloat;
        }
        return retval;
    }
    
    private void extractWidths() {
        if (this.widthCache == null) {
            this.widthCache = new HashMap<Integer, Float>();
            final COSArray widths = (COSArray)this.font.getDictionaryObject(COSName.W);
            if (widths != null) {
                final int size = widths.size();
                int counter = 0;
                while (counter < size) {
                    final COSNumber firstCode = (COSNumber)widths.getObject(counter++);
                    final COSBase next = widths.getObject(counter++);
                    if (next instanceof COSArray) {
                        final COSArray array = (COSArray)next;
                        final int startRange = firstCode.intValue();
                        for (int arraySize = array.size(), i = 0; i < arraySize; ++i) {
                            final COSNumber width = (COSNumber)array.get(i);
                            this.widthCache.put(startRange + i, width.floatValue());
                        }
                    }
                    else {
                        final COSNumber secondCode = (COSNumber)next;
                        final COSNumber rangeWidth = (COSNumber)widths.getObject(counter++);
                        final int startRange2 = firstCode.intValue();
                        final int endRange = secondCode.intValue();
                        final float width2 = rangeWidth.floatValue();
                        for (int j = startRange2; j <= endRange; ++j) {
                            this.widthCache.put(j, width2);
                        }
                    }
                }
            }
        }
    }
    
    @Override
    public float getFontHeight(final byte[] c, final int offset, final int length) throws IOException {
        float retval = 0.0f;
        final PDFontDescriptor desc = this.getFontDescriptor();
        final float xHeight = desc.getXHeight();
        final float capHeight = desc.getCapHeight();
        if (xHeight != 0.0f && capHeight != 0.0f) {
            retval = (xHeight + capHeight) / 2.0f;
        }
        else if (xHeight != 0.0f) {
            retval = xHeight;
        }
        else if (capHeight != 0.0f) {
            retval = capHeight;
        }
        else {
            retval = 0.0f;
        }
        if (retval == 0.0f) {
            retval = desc.getAscent();
        }
        return retval;
    }
    
    @Override
    public float getAverageFontWidth() throws IOException {
        float totalWidths = 0.0f;
        float characterCount = 0.0f;
        final float defaultWidth = (float)this.getDefaultWidth();
        final COSArray widths = (COSArray)this.font.getDictionaryObject(COSName.W);
        if (widths != null) {
            for (int i = 0; i < widths.size(); ++i) {
                final COSNumber firstCode = (COSNumber)widths.getObject(i++);
                final COSBase next = widths.getObject(i);
                if (next instanceof COSArray) {
                    final COSArray array = (COSArray)next;
                    for (int j = 0; j < array.size(); ++j) {
                        final COSNumber width = (COSNumber)array.get(j);
                        totalWidths += width.floatValue();
                        ++characterCount;
                    }
                }
                else {
                    ++i;
                    final COSNumber rangeWidth = (COSNumber)widths.getObject(i);
                    if (rangeWidth.floatValue() > 0.0f) {
                        totalWidths += rangeWidth.floatValue();
                        ++characterCount;
                    }
                }
            }
        }
        float average = totalWidths / characterCount;
        if (average <= 0.0f) {
            average = defaultWidth;
        }
        return average;
    }
    
    @Override
    public float getFontWidth(final int charCode) {
        float width = -1.0f;
        if (this.widthCache.containsKey(charCode)) {
            width = this.widthCache.get(charCode);
        }
        return width;
    }
    
    private String getCIDSystemInfo() {
        String cidSystemInfo = null;
        final COSDictionary cidsysteminfo = (COSDictionary)this.font.getDictionaryObject(COSName.CIDSYSTEMINFO);
        if (cidsysteminfo != null) {
            final String ordering = cidsysteminfo.getString(COSName.ORDERING);
            final String registry = cidsysteminfo.getString(COSName.REGISTRY);
            final int supplement = cidsysteminfo.getInt(COSName.SUPPLEMENT);
            cidSystemInfo = registry + "-" + ordering + "-" + supplement;
        }
        return cidSystemInfo;
    }
    
    @Override
    protected void determineEncoding() {
        String cidSystemInfo = this.getCIDSystemInfo();
        if (cidSystemInfo != null) {
            if (cidSystemInfo.contains("Identity")) {
                cidSystemInfo = "Identity-H";
            }
            else if (cidSystemInfo.startsWith("Adobe-UCS-")) {
                cidSystemInfo = "Adobe-Identity-UCS";
            }
            else {
                cidSystemInfo = cidSystemInfo.substring(0, cidSystemInfo.lastIndexOf("-")) + "-UCS2";
            }
            this.cmap = PDCIDFont.cmapObjects.get(cidSystemInfo);
            if (this.cmap == null) {
                final String resourceName = "org/apache/pdfbox/resources/cmap/" + cidSystemInfo;
                try {
                    this.cmap = this.parseCmap("org/apache/pdfbox/resources/cmap/", ResourceLoader.loadResource(resourceName));
                    if (this.cmap == null) {
                        PDCIDFont.log.error("Error: Could not parse predefined CMAP file for '" + cidSystemInfo + "'");
                    }
                }
                catch (IOException exception) {
                    PDCIDFont.log.error("Error: Could not find predefined CMAP file for '" + cidSystemInfo + "'");
                }
            }
        }
        else {
            super.determineEncoding();
        }
    }
    
    @Override
    public String encode(final byte[] c, final int offset, final int length) throws IOException {
        String result = null;
        if (this.cmap != null) {
            result = this.cmapEncoding(this.getCodeFromArray(c, offset, length), length, true, this.cmap);
        }
        else {
            result = super.encode(c, offset, length);
        }
        return result;
    }
    
    static {
        log = LogFactory.getLog(PDCIDFont.class);
    }
}
