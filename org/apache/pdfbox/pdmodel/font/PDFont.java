// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.font;

import java.io.UnsupportedEncodingException;
import java.util.Collections;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.cos.COSInteger;
import org.apache.pdfbox.cos.COSFloat;
import org.apache.pdfbox.pdmodel.common.COSArrayList;
import org.apache.pdfbox.cos.COSArray;
import org.apache.fontbox.cmap.CMapParser;
import org.apache.pdfbox.cos.COSString;
import java.awt.geom.AffineTransform;
import java.awt.Graphics;
import java.io.IOException;
import org.apache.pdfbox.cos.COSName;
import java.io.InputStream;
import org.apache.fontbox.afm.AFMParser;
import org.apache.pdfbox.util.ResourceLoader;
import java.util.HashMap;
import org.apache.pdfbox.cos.COSBase;
import org.apache.fontbox.afm.FontMetric;
import java.util.List;
import java.util.Map;
import org.apache.fontbox.cmap.CMap;
import org.apache.pdfbox.pdmodel.common.PDMatrix;
import org.apache.pdfbox.encoding.Encoding;
import org.apache.pdfbox.cos.COSDictionary;
import org.apache.pdfbox.pdmodel.common.COSObjectable;

public abstract class PDFont implements COSObjectable
{
    protected COSDictionary font;
    private Encoding fontEncoding;
    private PDFontDescriptor fontDescriptor;
    protected PDMatrix fontMatrix;
    protected CMap cmap;
    protected CMap toUnicodeCmap;
    private boolean hasToUnicode;
    protected static Map<String, CMap> cmapObjects;
    private List<Float> widths;
    private static final Map<String, FontMetric> afmObjects;
    protected static final String resourceRootCMAP = "org/apache/pdfbox/resources/cmap/";
    private static final String resourceRootAFM = "org/apache/pdfbox/resources/afm/";
    private FontMetric afm;
    private COSBase encoding;
    private static final String[] SINGLE_CHAR_STRING;
    private static final String[][] DOUBLE_CHAR_STRING;
    private String subtype;
    private boolean type1Font;
    private boolean trueTypeFont;
    private boolean typeFont;
    private boolean type0Font;
    
    private static Map<String, FontMetric> getAdobeFontMetrics() {
        final Map<String, FontMetric> metrics = new HashMap<String, FontMetric>();
        addAdobeFontMetric(metrics, "Courier-Bold");
        addAdobeFontMetric(metrics, "Courier-BoldOblique");
        addAdobeFontMetric(metrics, "Courier");
        addAdobeFontMetric(metrics, "Courier-Oblique");
        addAdobeFontMetric(metrics, "Helvetica");
        addAdobeFontMetric(metrics, "Helvetica-Bold");
        addAdobeFontMetric(metrics, "Helvetica-BoldOblique");
        addAdobeFontMetric(metrics, "Helvetica-Oblique");
        addAdobeFontMetric(metrics, "Symbol");
        addAdobeFontMetric(metrics, "Times-Bold");
        addAdobeFontMetric(metrics, "Times-BoldItalic");
        addAdobeFontMetric(metrics, "Times-Italic");
        addAdobeFontMetric(metrics, "Times-Roman");
        addAdobeFontMetric(metrics, "ZapfDingbats");
        return metrics;
    }
    
    private static void addAdobeFontMetric(final Map<String, FontMetric> metrics, final String name) {
        try {
            final String resource = "org/apache/pdfbox/resources/afm/" + name + ".afm";
            final InputStream afmStream = ResourceLoader.loadResource(resource);
            if (afmStream != null) {
                try {
                    final AFMParser parser = new AFMParser(afmStream);
                    parser.parse();
                    metrics.put(name, parser.getResult());
                }
                finally {
                    afmStream.close();
                }
            }
        }
        catch (Exception ex) {}
    }
    
    public static void clearResources() {
        PDFont.cmapObjects.clear();
    }
    
    public PDFont() {
        this.fontEncoding = null;
        this.fontDescriptor = null;
        this.fontMatrix = null;
        this.cmap = null;
        this.toUnicodeCmap = null;
        this.hasToUnicode = false;
        this.widths = null;
        this.afm = null;
        this.encoding = null;
        this.subtype = null;
        (this.font = new COSDictionary()).setItem(COSName.TYPE, COSName.FONT);
    }
    
    public PDFont(final COSDictionary fontDictionary) {
        this.fontEncoding = null;
        this.fontDescriptor = null;
        this.fontMatrix = null;
        this.cmap = null;
        this.toUnicodeCmap = null;
        this.hasToUnicode = false;
        this.widths = null;
        this.afm = null;
        this.encoding = null;
        this.subtype = null;
        this.font = fontDictionary;
        this.determineEncoding();
    }
    
    public PDFontDescriptor getFontDescriptor() {
        if (this.fontDescriptor == null) {
            final COSDictionary fd = (COSDictionary)this.font.getDictionaryObject(COSName.FONT_DESC);
            if (fd != null) {
                this.fontDescriptor = new PDFontDescriptorDictionary(fd);
            }
            else {
                this.getAFM();
                if (this.afm != null) {
                    this.fontDescriptor = new PDFontDescriptorAFM(this.afm);
                }
            }
        }
        return this.fontDescriptor;
    }
    
    public void setFontDescriptor(final PDFontDescriptorDictionary fdDictionary) {
        COSDictionary dic = null;
        if (fdDictionary != null) {
            dic = fdDictionary.getCOSDictionary();
        }
        this.font.setItem(COSName.FONT_DESC, dic);
        this.fontDescriptor = fdDictionary;
    }
    
    protected abstract void determineEncoding();
    
    public COSBase getCOSObject() {
        return this.font;
    }
    
    public abstract float getFontWidth(final byte[] p0, final int p1, final int p2) throws IOException;
    
    public abstract float getFontHeight(final byte[] p0, final int p1, final int p2) throws IOException;
    
    public float getStringWidth(final String string) throws IOException {
        final byte[] data = string.getBytes("ISO-8859-1");
        float totalWidth = 0.0f;
        for (int i = 0; i < data.length; ++i) {
            totalWidth += this.getFontWidth(data, i, 1);
        }
        return totalWidth;
    }
    
    public abstract float getAverageFontWidth() throws IOException;
    
    @Deprecated
    public void drawString(final String string, final Graphics g, final float fontSize, final AffineTransform at, final float x, final float y) throws IOException {
        this.drawString(string, null, g, fontSize, at, x, y);
    }
    
    public abstract void drawString(final String p0, final int[] p1, final Graphics p2, final float p3, final AffineTransform p4, final float p5, final float p6) throws IOException;
    
    public int getCodeFromArray(final byte[] data, final int offset, final int length) {
        int code = 0;
        for (int i = 0; i < length; ++i) {
            code <<= 8;
            code |= (data[offset + i] + 256) % 256;
        }
        return code;
    }
    
    protected float getFontWidthFromAFMFile(final int code) throws IOException {
        float retval = 0.0f;
        final FontMetric metric = this.getAFM();
        if (metric != null) {
            final String characterName = this.fontEncoding.getName(code);
            retval = metric.getCharacterWidth(characterName);
        }
        return retval;
    }
    
    protected float getAverageFontWidthFromAFMFile() throws IOException {
        float retval = 0.0f;
        final FontMetric metric = this.getAFM();
        if (metric != null) {
            retval = metric.getAverageCharacterWidth();
        }
        return retval;
    }
    
    protected FontMetric getAFM() {
        if (this.isType1Font() && this.afm == null) {
            final COSBase baseFont = this.font.getDictionaryObject(COSName.BASE_FONT);
            String name = null;
            if (baseFont instanceof COSName) {
                name = ((COSName)baseFont).getName();
                if (name.indexOf("+") > -1) {
                    name = name.substring(name.indexOf("+") + 1);
                }
            }
            else if (baseFont instanceof COSString) {
                final COSString string = (COSString)baseFont;
                name = string.getString();
            }
            if (name != null) {
                this.afm = PDFont.afmObjects.get(name);
            }
        }
        return this.afm;
    }
    
    protected COSBase getEncoding() {
        if (this.encoding == null) {
            this.encoding = this.font.getDictionaryObject(COSName.ENCODING);
        }
        return this.encoding;
    }
    
    protected void setEncoding(final COSBase encodingValue) {
        this.font.setItem(COSName.ENCODING, encodingValue);
        this.encoding = encodingValue;
    }
    
    protected String cmapEncoding(final int code, final int length, final boolean isCIDFont, final CMap sourceCmap) throws IOException {
        String retval = null;
        if (sourceCmap != null) {
            retval = sourceCmap.lookup(code, length);
            if (retval == null && isCIDFont) {
                retval = sourceCmap.lookupCID(code);
            }
        }
        return retval;
    }
    
    public String encode(final byte[] c, final int offset, final int length) throws IOException {
        String retval = null;
        final int code = this.getCodeFromArray(c, offset, length);
        if (this.toUnicodeCmap != null) {
            retval = this.cmapEncoding(code, length, false, this.toUnicodeCmap);
        }
        if (retval == null && this.cmap != null) {
            retval = this.cmapEncoding(code, length, false, this.cmap);
        }
        if (retval == null) {
            if (this.fontEncoding != null) {
                retval = this.fontEncoding.getCharacter(code);
            }
            if (retval == null && (this.cmap == null || length == 2)) {
                retval = getStringFromArray(c, offset, length);
            }
        }
        return retval;
    }
    
    public int encodeToCID(final byte[] c, final int offset, final int length) throws IOException {
        int code = -1;
        if (this.encode(c, offset, length) != null) {
            code = this.getCodeFromArray(c, offset, length);
        }
        return code;
    }
    
    private static String getStringFromArray(final byte[] c, final int offset, final int length) throws IOException {
        String retval = null;
        if (length == 1) {
            retval = PDFont.SINGLE_CHAR_STRING[(c[offset] + 256) % 256];
        }
        else {
            if (length != 2) {
                throw new IOException("Error:Unknown character length:" + length);
            }
            retval = PDFont.DOUBLE_CHAR_STRING[(c[offset] + 256) % 256][(c[offset + 1] + 256) % 256];
        }
        return retval;
    }
    
    protected CMap parseCmap(final String cmapRoot, final InputStream cmapStream) {
        CMap targetCmap = null;
        if (cmapStream != null) {
            final CMapParser parser = new CMapParser();
            try {
                targetCmap = parser.parse(cmapRoot, cmapStream);
                if (cmapRoot != null) {
                    PDFont.cmapObjects.put(targetCmap.getName(), targetCmap);
                }
            }
            catch (IOException ex) {}
        }
        return targetCmap;
    }
    
    public void setFontEncoding(final Encoding enc) {
        this.fontEncoding = enc;
    }
    
    public Encoding getFontEncoding() {
        return this.fontEncoding;
    }
    
    public String getType() {
        return this.font.getNameAsString(COSName.TYPE);
    }
    
    public String getSubType() {
        if (this.subtype == null) {
            this.subtype = this.font.getNameAsString(COSName.SUBTYPE);
            this.type1Font = "Type1".equals(this.subtype);
            this.trueTypeFont = "TrueType".equals(this.subtype);
            this.type0Font = "Type0".equals(this.subtype);
            this.typeFont = (this.type1Font || "Type0".equals(this.subtype) || this.trueTypeFont);
        }
        return this.subtype;
    }
    
    protected boolean isType1Font() {
        this.getSubType();
        return this.type1Font;
    }
    
    protected boolean isType0Font() {
        this.getSubType();
        return this.type0Font;
    }
    
    private boolean isTrueTypeFont() {
        this.getSubType();
        return this.trueTypeFont;
    }
    
    private boolean isTypeFont() {
        this.getSubType();
        return this.typeFont;
    }
    
    public String getBaseFont() {
        return this.font.getNameAsString(COSName.BASE_FONT);
    }
    
    public void setBaseFont(final String baseFont) {
        this.font.setName(COSName.BASE_FONT, baseFont);
    }
    
    public int getFirstChar() {
        return this.font.getInt(COSName.FIRST_CHAR, -1);
    }
    
    public void setFirstChar(final int firstChar) {
        this.font.setInt(COSName.FIRST_CHAR, firstChar);
    }
    
    public int getLastChar() {
        return this.font.getInt(COSName.LAST_CHAR, -1);
    }
    
    public void setLastChar(final int lastChar) {
        this.font.setInt(COSName.LAST_CHAR, lastChar);
    }
    
    public List<Float> getWidths() {
        if (this.widths == null) {
            final COSArray array = (COSArray)this.font.getDictionaryObject(COSName.WIDTHS);
            if (array != null) {
                this.widths = (List<Float>)COSArrayList.convertFloatCOSArrayToList(array);
            }
        }
        return this.widths;
    }
    
    public void setWidths(final List<Float> widthsList) {
        this.widths = widthsList;
        this.font.setItem(COSName.WIDTHS, COSArrayList.converterToCOSArray(this.widths));
    }
    
    public PDMatrix getFontMatrix() {
        if (this.fontMatrix == null) {
            COSArray array = (COSArray)this.font.getDictionaryObject(COSName.FONT_MATRIX);
            if (array == null) {
                array = new COSArray();
                array.add(new COSFloat(0.001f));
                array.add(COSInteger.ZERO);
                array.add(COSInteger.ZERO);
                array.add(new COSFloat(0.001f));
                array.add(COSInteger.ZERO);
                array.add(COSInteger.ZERO);
            }
            this.fontMatrix = new PDMatrix(array);
        }
        return this.fontMatrix;
    }
    
    public abstract PDRectangle getFontBoundingBox() throws IOException;
    
    @Override
    public boolean equals(final Object other) {
        return other instanceof PDFont && ((PDFont)other).getCOSObject() == this.getCOSObject();
    }
    
    @Override
    public int hashCode() {
        return this.getCOSObject().hashCode();
    }
    
    public float getFontWidth(final int charCode) {
        float width = -1.0f;
        final int firstChar = this.getFirstChar();
        final int lastChar = this.getLastChar();
        if (charCode >= firstChar && charCode <= lastChar) {
            this.getWidths();
            if (this.widths != null) {
                width = this.widths.get(charCode - firstChar);
            }
        }
        else {
            final PDFontDescriptor fd = this.getFontDescriptor();
            if (fd instanceof PDFontDescriptorDictionary) {
                width = fd.getMissingWidth();
            }
        }
        return width;
    }
    
    protected boolean hasToUnicode() {
        return this.hasToUnicode;
    }
    
    protected void setHasToUnicode(final boolean hasToUnicodeValue) {
        this.hasToUnicode = hasToUnicodeValue;
    }
    
    static {
        PDFont.cmapObjects = Collections.synchronizedMap(new HashMap<String, CMap>());
        afmObjects = Collections.unmodifiableMap((Map<? extends String, ? extends FontMetric>)getAdobeFontMetrics());
        SINGLE_CHAR_STRING = new String[256];
        DOUBLE_CHAR_STRING = new String[256][256];
        for (int i = 0; i < 256; ++i) {
            try {
                PDFont.SINGLE_CHAR_STRING[i] = new String(new byte[] { (byte)i }, "ISO-8859-1");
            }
            catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            for (int j = 0; j < 256; ++j) {
                try {
                    PDFont.DOUBLE_CHAR_STRING[i][j] = new String(new byte[] { (byte)i, (byte)j }, "UTF-16BE");
                }
                catch (UnsupportedEncodingException e2) {
                    e2.printStackTrace();
                }
            }
        }
    }
}
