// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.font;

import org.apache.commons.logging.LogFactory;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import java.io.IOException;
import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSName;
import java.awt.Font;
import org.apache.pdfbox.cos.COSDictionary;
import org.apache.pdfbox.cos.COSArray;
import org.apache.commons.logging.Log;

public class PDType0Font extends PDSimpleFont
{
    private static final Log LOG;
    private COSArray descendantFontArray;
    private PDFont descendantFont;
    private COSDictionary descendantFontDictionary;
    private Font awtFont;
    
    public PDType0Font() {
        this.font.setItem(COSName.SUBTYPE, COSName.TYPE0);
    }
    
    public PDType0Font(final COSDictionary fontDictionary) {
        super(fontDictionary);
        this.descendantFontDictionary = (COSDictionary)this.getDescendantFonts().getObject(0);
        if (this.descendantFontDictionary != null) {
            try {
                this.descendantFont = PDFontFactory.createFont(this.descendantFontDictionary);
            }
            catch (IOException exception) {
                PDType0Font.LOG.error("Error while creating the descendant font!");
            }
        }
    }
    
    @Override
    public Font getawtFont() throws IOException {
        if (this.awtFont == null) {
            if (this.descendantFont != null) {
                this.awtFont = ((PDSimpleFont)this.descendantFont).getawtFont();
            }
            if (this.awtFont == null) {
                this.awtFont = FontManager.getStandardFont();
                PDType0Font.LOG.info("Using font " + this.awtFont.getName() + " instead of " + this.descendantFont.getFontDescriptor().getFontName());
                this.setIsFontSubstituted(true);
            }
        }
        return this.awtFont;
    }
    
    @Override
    public PDRectangle getFontBoundingBox() throws IOException {
        throw new RuntimeException("Not yet implemented");
    }
    
    @Override
    public float getFontWidth(final byte[] c, final int offset, final int length) throws IOException {
        return this.descendantFont.getFontWidth(c, offset, length);
    }
    
    @Override
    public float getFontHeight(final byte[] c, final int offset, final int length) throws IOException {
        return this.descendantFont.getFontHeight(c, offset, length);
    }
    
    @Override
    public float getAverageFontWidth() throws IOException {
        return this.descendantFont.getAverageFontWidth();
    }
    
    private COSArray getDescendantFonts() {
        if (this.descendantFontArray == null) {
            this.descendantFontArray = (COSArray)this.font.getDictionaryObject(COSName.DESCENDANT_FONTS);
        }
        return this.descendantFontArray;
    }
    
    @Override
    public float getFontWidth(final int charCode) {
        return this.descendantFont.getFontWidth(charCode);
    }
    
    @Override
    public String encode(final byte[] c, final int offset, final int length) throws IOException {
        String retval = null;
        if (this.hasToUnicode()) {
            retval = super.encode(c, offset, length);
        }
        if (retval == null) {
            final int result = this.cmap.lookupCID(c, offset, length);
            if (result != -1) {
                retval = this.descendantFont.cmapEncoding(result, 2, true, this.cmap);
            }
        }
        return retval;
    }
    
    protected PDFont getDescendantFont() {
        return this.descendantFont;
    }
    
    static {
        LOG = LogFactory.getLog(PDType0Font.class);
    }
}
