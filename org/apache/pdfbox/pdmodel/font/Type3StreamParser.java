// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.font;

import org.apache.pdfbox.util.ImageParameters;
import org.apache.pdfbox.cos.COSNumber;
import org.apache.pdfbox.cos.COSBase;
import java.util.List;
import org.apache.pdfbox.util.PDFOperator;
import java.io.IOException;
import org.apache.pdfbox.pdmodel.PDResources;
import org.apache.pdfbox.pdmodel.PDPage;
import java.awt.Image;
import org.apache.pdfbox.cos.COSStream;
import org.apache.fontbox.util.BoundingBox;
import org.apache.pdfbox.pdmodel.graphics.xobject.PDInlinedImage;
import org.apache.pdfbox.util.PDFStreamEngine;

public class Type3StreamParser extends PDFStreamEngine
{
    private PDInlinedImage image;
    private BoundingBox box;
    
    public Type3StreamParser() {
        this.image = null;
        this.box = null;
    }
    
    public Image createImage(final COSStream type3Stream) throws IOException {
        this.processStream(null, null, type3Stream);
        return this.image.createImage();
    }
    
    @Override
    protected void processOperator(final PDFOperator operator, final List arguments) throws IOException {
        super.processOperator(operator, arguments);
        final String operation = operator.getOperation();
        if (operation.equals("BI")) {
            final ImageParameters params = operator.getImageParameters();
            (this.image = new PDInlinedImage()).setImageParameters(params);
            this.image.setImageData(operator.getImageData());
        }
        if (!operation.equals("d0")) {
            if (operation.equals("d1")) {
                final COSNumber llx = arguments.get(2);
                final COSNumber lly = arguments.get(3);
                final COSNumber urx = arguments.get(4);
                final COSNumber ury = arguments.get(5);
                (this.box = new BoundingBox()).setLowerLeftX(llx.floatValue());
                this.box.setLowerLeftY(lly.floatValue());
                this.box.setUpperRightX(urx.floatValue());
                this.box.setUpperRightY(ury.floatValue());
            }
        }
    }
}
