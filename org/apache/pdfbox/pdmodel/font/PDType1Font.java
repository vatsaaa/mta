// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.font;

import java.util.HashMap;
import org.apache.commons.logging.LogFactory;
import org.apache.pdfbox.pdmodel.common.PDMatrix;
import org.apache.pdfbox.cos.COSFloat;
import org.apache.pdfbox.cos.COSArray;
import org.apache.pdfbox.encoding.EncodingManager;
import org.apache.pdfbox.encoding.Type1Encoding;
import java.util.StringTokenizer;
import java.io.Reader;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import org.apache.fontbox.afm.FontMetric;
import org.apache.pdfbox.encoding.AFMEncoding;
import java.awt.FontFormatException;
import org.apache.pdfbox.encoding.Encoding;
import org.apache.pdfbox.encoding.WinAnsiEncoding;
import org.apache.pdfbox.pdmodel.common.PDStream;
import java.io.IOException;
import org.apache.pdfbox.cos.COSDictionary;
import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSName;
import java.awt.Font;
import java.util.Map;
import org.apache.commons.logging.Log;

public class PDType1Font extends PDSimpleFont
{
    private static final Log log;
    private PDType1CFont type1CFont;
    public static final PDType1Font TIMES_ROMAN;
    public static final PDType1Font TIMES_BOLD;
    public static final PDType1Font TIMES_ITALIC;
    public static final PDType1Font TIMES_BOLD_ITALIC;
    public static final PDType1Font HELVETICA;
    public static final PDType1Font HELVETICA_BOLD;
    public static final PDType1Font HELVETICA_OBLIQUE;
    public static final PDType1Font HELVETICA_BOLD_OBLIQUE;
    public static final PDType1Font COURIER;
    public static final PDType1Font COURIER_BOLD;
    public static final PDType1Font COURIER_OBLIQUE;
    public static final PDType1Font COURIER_BOLD_OBLIQUE;
    public static final PDType1Font SYMBOL;
    public static final PDType1Font ZAPF_DINGBATS;
    private static final Map<String, PDType1Font> STANDARD_14;
    private Font awtFont;
    
    public PDType1Font() {
        this.type1CFont = null;
        this.awtFont = null;
        this.font.setItem(COSName.SUBTYPE, COSName.TYPE1);
    }
    
    public PDType1Font(final COSDictionary fontDictionary) {
        super(fontDictionary);
        this.type1CFont = null;
        this.awtFont = null;
        final PDFontDescriptor fd = this.getFontDescriptor();
        if (fd != null && fd instanceof PDFontDescriptorDictionary) {
            final PDStream fontFile3 = ((PDFontDescriptorDictionary)fd).getFontFile3();
            if (fontFile3 != null) {
                try {
                    this.type1CFont = new PDType1CFont(super.font);
                }
                catch (IOException exception) {
                    PDType1Font.log.info("Can't read the embedded type1C font " + fd.getFontName());
                }
            }
        }
    }
    
    public PDType1Font(final String baseFont) {
        this();
        this.setBaseFont(baseFont);
        this.setFontEncoding(new WinAnsiEncoding());
        this.setEncoding(COSName.WIN_ANSI_ENCODING);
    }
    
    public static PDType1Font getStandardFont(final String name) {
        return PDType1Font.STANDARD_14.get(name);
    }
    
    public static String[] getStandard14Names() {
        return PDType1Font.STANDARD_14.keySet().toArray(new String[14]);
    }
    
    @Override
    public Font getawtFont() throws IOException {
        if (this.awtFont == null) {
            if (this.type1CFont != null) {
                this.awtFont = this.type1CFont.getawtFont();
            }
            else {
                final String baseFont = this.getBaseFont();
                final PDFontDescriptor fd = this.getFontDescriptor();
                if (fd != null && fd instanceof PDFontDescriptorDictionary) {
                    final PDFontDescriptorDictionary fdDictionary = (PDFontDescriptorDictionary)fd;
                    if (fdDictionary.getFontFile() != null) {
                        try {
                            this.awtFont = Font.createFont(1, fdDictionary.getFontFile().createInputStream());
                        }
                        catch (FontFormatException e) {
                            PDType1Font.log.info("Can't read the embedded type1 font " + fd.getFontName());
                        }
                    }
                    if (this.awtFont == null) {
                        this.awtFont = FontManager.getAwtFont(fd.getFontName());
                        if (this.awtFont == null) {
                            PDType1Font.log.info("Can't find the specified font " + fd.getFontName());
                        }
                    }
                }
                else {
                    this.awtFont = FontManager.getAwtFont(baseFont);
                    if (this.awtFont == null) {
                        PDType1Font.log.info("Can't find the specified basefont " + baseFont);
                    }
                }
            }
            if (this.awtFont == null) {
                this.awtFont = FontManager.getStandardFont();
                PDType1Font.log.info("Using font " + this.awtFont.getName() + " instead");
            }
        }
        return this.awtFont;
    }
    
    @Override
    protected void determineEncoding() {
        super.determineEncoding();
        Encoding fontEncoding = this.getFontEncoding();
        if (fontEncoding == null) {
            final FontMetric metric = this.getAFM();
            if (metric != null) {
                fontEncoding = new AFMEncoding(metric);
            }
            this.setFontEncoding(fontEncoding);
        }
        this.getEncodingFromFont(this.getFontEncoding() == null);
    }
    
    private void getEncodingFromFont(final boolean extractEncoding) {
        final PDFontDescriptor fontDescriptor = this.getFontDescriptor();
        if (fontDescriptor != null && fontDescriptor instanceof PDFontDescriptorDictionary) {
            final PDStream fontFile = ((PDFontDescriptorDictionary)fontDescriptor).getFontFile();
            if (fontFile != null) {
                BufferedReader in = null;
                try {
                    in = new BufferedReader(new InputStreamReader(fontFile.createInputStream()));
                    String line = "";
                    Type1Encoding encoding = null;
                    while ((line = in.readLine()) != null) {
                        if (extractEncoding) {
                            if (line.startsWith("currentdict end")) {
                                if (encoding != null) {
                                    this.setFontEncoding(encoding);
                                    break;
                                }
                                break;
                            }
                            else if (line.startsWith("/Encoding")) {
                                if (line.contains("array")) {
                                    final StringTokenizer st = new StringTokenizer(line);
                                    st.nextElement();
                                    final int arraySize = Integer.parseInt(st.nextToken());
                                    encoding = new Type1Encoding(arraySize);
                                }
                                else if (this.getFontEncoding() == null) {
                                    final StringTokenizer st = new StringTokenizer(line);
                                    st.nextElement();
                                    final String type1Encoding = st.nextToken();
                                    this.setFontEncoding(EncodingManager.INSTANCE.getEncoding(COSName.getPDFName(type1Encoding)));
                                    break;
                                }
                            }
                            else if (line.startsWith("dup")) {
                                final StringTokenizer st = new StringTokenizer(line.replaceAll("/", " /"));
                                st.nextElement();
                                final int index = Integer.parseInt(st.nextToken());
                                final String name = st.nextToken();
                                if (encoding == null) {
                                    PDType1Font.log.warn("Unable to get character encoding.  Encoding defintion found without /Encoding line.");
                                }
                                else {
                                    encoding.addCharacterEncoding(index, name.replace("/", ""));
                                }
                            }
                        }
                        if (line.startsWith("/FontMatrix")) {
                            final String matrixValues = line.substring(line.indexOf("[") + 1, line.lastIndexOf("]"));
                            final StringTokenizer st2 = new StringTokenizer(matrixValues);
                            final COSArray array = new COSArray();
                            if (st2.countTokens() < 6) {
                                continue;
                            }
                            try {
                                for (int i = 0; i < 6; ++i) {
                                    final COSFloat floatValue = new COSFloat(Float.parseFloat(st2.nextToken()));
                                    array.add(floatValue);
                                }
                                this.fontMatrix = new PDMatrix(array);
                            }
                            catch (NumberFormatException exception) {
                                PDType1Font.log.error("Can't read the fontmatrix from embedded font file!");
                            }
                        }
                    }
                }
                catch (IOException exception2) {
                    PDType1Font.log.error("Error: Could not extract the encoding from the embedded type1 font.");
                    if (in != null) {
                        try {
                            in.close();
                        }
                        catch (IOException exception2) {
                            PDType1Font.log.error("An error occurs while closing the stream used to read the embedded type1 font.");
                        }
                    }
                }
                finally {
                    if (in != null) {
                        try {
                            in.close();
                        }
                        catch (IOException exception3) {
                            PDType1Font.log.error("An error occurs while closing the stream used to read the embedded type1 font.");
                        }
                    }
                }
            }
        }
    }
    
    @Override
    public String encode(final byte[] c, final int offset, final int length) throws IOException {
        if (this.type1CFont != null && this.getFontEncoding() == null) {
            return this.type1CFont.encode(c, offset, length);
        }
        return super.encode(c, offset, length);
    }
    
    @Override
    public int encodeToCID(final byte[] c, final int offset, final int length) throws IOException {
        if (this.type1CFont != null && this.getFontEncoding() == null) {
            return this.type1CFont.encodeToCID(c, offset, length);
        }
        return super.encodeToCID(c, offset, length);
    }
    
    @Override
    public PDMatrix getFontMatrix() {
        if (this.type1CFont != null) {
            return this.type1CFont.getFontMatrix();
        }
        return super.getFontMatrix();
    }
    
    static {
        log = LogFactory.getLog(PDType1Font.class);
        TIMES_ROMAN = new PDType1Font("Times-Roman");
        TIMES_BOLD = new PDType1Font("Times-Bold");
        TIMES_ITALIC = new PDType1Font("Times-Italic");
        TIMES_BOLD_ITALIC = new PDType1Font("Times-BoldItalic");
        HELVETICA = new PDType1Font("Helvetica");
        HELVETICA_BOLD = new PDType1Font("Helvetica-Bold");
        HELVETICA_OBLIQUE = new PDType1Font("Helvetica-Oblique");
        HELVETICA_BOLD_OBLIQUE = new PDType1Font("Helvetica-BoldOblique");
        COURIER = new PDType1Font("Courier");
        COURIER_BOLD = new PDType1Font("Courier-Bold");
        COURIER_OBLIQUE = new PDType1Font("Courier-Oblique");
        COURIER_BOLD_OBLIQUE = new PDType1Font("Courier-BoldOblique");
        SYMBOL = new PDType1Font("Symbol");
        ZAPF_DINGBATS = new PDType1Font("ZapfDingbats");
        (STANDARD_14 = new HashMap<String, PDType1Font>()).put(PDType1Font.TIMES_ROMAN.getBaseFont(), PDType1Font.TIMES_ROMAN);
        PDType1Font.STANDARD_14.put(PDType1Font.TIMES_BOLD.getBaseFont(), PDType1Font.TIMES_BOLD);
        PDType1Font.STANDARD_14.put(PDType1Font.TIMES_ITALIC.getBaseFont(), PDType1Font.TIMES_ITALIC);
        PDType1Font.STANDARD_14.put(PDType1Font.TIMES_BOLD_ITALIC.getBaseFont(), PDType1Font.TIMES_BOLD_ITALIC);
        PDType1Font.STANDARD_14.put(PDType1Font.HELVETICA.getBaseFont(), PDType1Font.HELVETICA);
        PDType1Font.STANDARD_14.put(PDType1Font.HELVETICA_BOLD.getBaseFont(), PDType1Font.HELVETICA_BOLD);
        PDType1Font.STANDARD_14.put(PDType1Font.HELVETICA_OBLIQUE.getBaseFont(), PDType1Font.HELVETICA_OBLIQUE);
        PDType1Font.STANDARD_14.put(PDType1Font.HELVETICA_BOLD_OBLIQUE.getBaseFont(), PDType1Font.HELVETICA_BOLD_OBLIQUE);
        PDType1Font.STANDARD_14.put(PDType1Font.COURIER.getBaseFont(), PDType1Font.COURIER);
        PDType1Font.STANDARD_14.put(PDType1Font.COURIER_BOLD.getBaseFont(), PDType1Font.COURIER_BOLD);
        PDType1Font.STANDARD_14.put(PDType1Font.COURIER_OBLIQUE.getBaseFont(), PDType1Font.COURIER_OBLIQUE);
        PDType1Font.STANDARD_14.put(PDType1Font.COURIER_BOLD_OBLIQUE.getBaseFont(), PDType1Font.COURIER_BOLD_OBLIQUE);
        PDType1Font.STANDARD_14.put(PDType1Font.SYMBOL.getBaseFont(), PDType1Font.SYMBOL);
        PDType1Font.STANDARD_14.put(PDType1Font.ZAPF_DINGBATS.getBaseFont(), PDType1Font.ZAPF_DINGBATS);
    }
}
