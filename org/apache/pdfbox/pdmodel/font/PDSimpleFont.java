// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.font;

import org.apache.commons.logging.LogFactory;
import org.apache.pdfbox.util.ResourceLoader;
import org.apache.pdfbox.encoding.DictionaryEncoding;
import org.apache.pdfbox.cos.COSStream;
import org.apache.pdfbox.encoding.EncodingManager;
import org.apache.fontbox.cmap.CMap;
import java.awt.geom.NoninvertibleTransformException;
import java.awt.geom.Point2D;
import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSNumber;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.cos.COSArray;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.encoding.Encoding;
import org.apache.fontbox.afm.FontMetric;
import java.awt.font.GlyphVector;
import java.awt.RenderingHints;
import java.awt.Graphics2D;
import java.awt.font.FontRenderContext;
import java.awt.geom.AffineTransform;
import java.awt.Graphics;
import java.io.IOException;
import java.awt.Font;
import org.apache.pdfbox.cos.COSDictionary;
import org.apache.commons.logging.Log;
import java.util.HashMap;

public abstract class PDSimpleFont extends PDFont
{
    private final HashMap<Integer, Float> mFontSizes;
    private float avgFontWidth;
    private float avgFontHeight;
    private static final Log LOG;
    private boolean isFontSubstituted;
    
    public PDSimpleFont() {
        this.mFontSizes = new HashMap<Integer, Float>(128);
        this.avgFontWidth = 0.0f;
        this.avgFontHeight = 0.0f;
        this.isFontSubstituted = false;
    }
    
    public PDSimpleFont(final COSDictionary fontDictionary) {
        super(fontDictionary);
        this.mFontSizes = new HashMap<Integer, Float>(128);
        this.avgFontWidth = 0.0f;
        this.avgFontHeight = 0.0f;
        this.isFontSubstituted = false;
    }
    
    public Font getawtFont() throws IOException {
        PDSimpleFont.LOG.error("Not yet implemented:" + this.getClass().getName());
        return null;
    }
    
    @Override
    public void drawString(final String string, final int[] codePoints, final Graphics g, final float fontSize, final AffineTransform at, final float x, final float y) throws IOException {
        Font awtFont = this.getawtFont();
        final FontRenderContext frc = new FontRenderContext(new AffineTransform(), true, true);
        GlyphVector glyphs = null;
        final boolean useCodepoints = codePoints != null && this.isType0Font();
        final PDFont descendantFont = useCodepoints ? ((PDType0Font)this).getDescendantFont() : null;
        if (useCodepoints && !descendantFont.getFontDescriptor().isSymbolic()) {
            PDCIDFontType2Font cid2Font = null;
            if (descendantFont instanceof PDCIDFontType2Font) {
                cid2Font = (PDCIDFontType2Font)descendantFont;
            }
            if ((cid2Font != null && cid2Font.hasCIDToGIDMap()) || this.isFontSubstituted) {
                glyphs = awtFont.createGlyphVector(frc, string);
            }
            else {
                glyphs = awtFont.createGlyphVector(frc, codePoints);
            }
        }
        else {
            if (!this.isType1Font() && awtFont.canDisplayUpTo(string) != -1) {
                PDSimpleFont.LOG.warn("Changing font on <" + string + "> from <" + awtFont.getName() + "> to the default font");
                awtFont = Font.decode(null).deriveFont(1.0f);
            }
            glyphs = awtFont.createGlyphVector(frc, string);
        }
        final Graphics2D g2d = (Graphics2D)g;
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        this.writeFont(g2d, at, x, y, glyphs);
    }
    
    @Override
    public float getFontHeight(final byte[] c, final int offset, final int length) throws IOException {
        if (this.avgFontHeight > 0.0f) {
            return this.avgFontHeight;
        }
        float retval = 0.0f;
        final FontMetric metric = this.getAFM();
        if (metric != null) {
            final int code = this.getCodeFromArray(c, offset, length);
            final Encoding encoding = this.getFontEncoding();
            final String characterName = encoding.getName(code);
            retval = metric.getCharacterHeight(characterName);
        }
        else {
            final PDFontDescriptor desc = this.getFontDescriptor();
            if (desc != null) {
                final PDRectangle fontBBox = desc.getFontBoundingBox();
                if (fontBBox != null) {
                    retval = fontBBox.getHeight() / 2.0f;
                }
                if (retval == 0.0f) {
                    retval = desc.getCapHeight();
                }
                if (retval == 0.0f) {
                    retval = desc.getAscent();
                }
                if (retval == 0.0f) {
                    retval = desc.getXHeight();
                    if (retval > 0.0f) {
                        retval -= desc.getDescent();
                    }
                }
                this.avgFontHeight = retval;
            }
        }
        return retval;
    }
    
    @Override
    public float getFontWidth(final byte[] c, final int offset, final int length) throws IOException {
        final int code = this.getCodeFromArray(c, offset, length);
        Float fontWidth = this.mFontSizes.get(code);
        if (fontWidth == null) {
            fontWidth = this.getFontWidth(code);
            if (fontWidth <= 0.0f) {
                fontWidth = this.getFontWidthFromAFMFile(code);
            }
            this.mFontSizes.put(code, fontWidth);
        }
        return fontWidth;
    }
    
    @Override
    public float getAverageFontWidth() throws IOException {
        float average = 0.0f;
        if (this.avgFontWidth != 0.0f) {
            average = this.avgFontWidth;
        }
        else {
            float totalWidth = 0.0f;
            float characterCount = 0.0f;
            final COSArray widths = (COSArray)this.font.getDictionaryObject(COSName.WIDTHS);
            if (widths != null) {
                for (int i = 0; i < widths.size(); ++i) {
                    final COSNumber fontWidth = (COSNumber)widths.getObject(i);
                    if (fontWidth.floatValue() > 0.0f) {
                        totalWidth += fontWidth.floatValue();
                        ++characterCount;
                    }
                }
            }
            if (totalWidth > 0.0f) {
                average = totalWidth / characterCount;
            }
            else {
                average = this.getAverageFontWidthFromAFMFile();
            }
            this.avgFontWidth = average;
        }
        return average;
    }
    
    public COSBase getToUnicode() {
        return this.font.getDictionaryObject(COSName.TO_UNICODE);
    }
    
    public void setToUnicode(final COSBase unicode) {
        this.font.setItem(COSName.TO_UNICODE, unicode);
    }
    
    @Override
    public PDRectangle getFontBoundingBox() throws IOException {
        return this.getFontDescriptor().getFontBoundingBox();
    }
    
    protected void writeFont(final Graphics2D g2d, final AffineTransform at, final float x, final float y, final GlyphVector glyphs) {
        if (!at.isIdentity()) {
            try {
                final AffineTransform atInv = at.createInverse();
                g2d.transform(at);
                final Point2D.Float newXy = new Point2D.Float(x, y);
                atInv.transform(new Point2D.Float(x, y), newXy);
                g2d.drawGlyphVector(glyphs, (float)newXy.getX(), (float)newXy.getY());
                g2d.transform(atInv);
            }
            catch (NoninvertibleTransformException e) {
                PDSimpleFont.LOG.error("Error in " + this.getClass().getName() + ".writeFont", e);
            }
        }
        else {
            g2d.drawGlyphVector(glyphs, x, y);
        }
    }
    
    @Override
    protected void determineEncoding() {
        String cmapName = null;
        COSName encodingName = null;
        final COSBase encoding = this.getEncoding();
        Encoding fontEncoding = null;
        if (encoding != null) {
            if (encoding instanceof COSName) {
                if (this.cmap == null) {
                    encodingName = (COSName)encoding;
                    this.cmap = PDSimpleFont.cmapObjects.get(encodingName.getName());
                    if (this.cmap == null) {
                        cmapName = encodingName.getName();
                    }
                }
                if (this.cmap == null && cmapName != null) {
                    try {
                        fontEncoding = EncodingManager.INSTANCE.getEncoding(encodingName);
                    }
                    catch (IOException exception) {
                        PDSimpleFont.LOG.debug("Debug: Could not find encoding for " + encodingName);
                    }
                }
            }
            else if (encoding instanceof COSStream) {
                if (this.cmap == null) {
                    final COSStream encodingStream = (COSStream)encoding;
                    try {
                        this.cmap = this.parseCmap(null, encodingStream.getUnfilteredStream());
                    }
                    catch (IOException exception2) {
                        PDSimpleFont.LOG.error("Error: Could not parse the embedded CMAP");
                    }
                }
            }
            else if (encoding instanceof COSDictionary) {
                try {
                    fontEncoding = new DictionaryEncoding((COSDictionary)encoding);
                }
                catch (IOException exception) {
                    PDSimpleFont.LOG.error("Error: Could not create the DictionaryEncoding");
                }
            }
        }
        this.setFontEncoding(fontEncoding);
        this.extractToUnicodeEncoding();
        if (this.cmap == null && cmapName != null) {
            final String resourceName = "org/apache/pdfbox/resources/cmap/" + cmapName;
            try {
                this.cmap = this.parseCmap("org/apache/pdfbox/resources/cmap/", ResourceLoader.loadResource(resourceName));
                if (this.cmap == null && encodingName == null) {
                    PDSimpleFont.LOG.error("Error: Could not parse predefined CMAP file for '" + cmapName + "'");
                }
            }
            catch (IOException exception2) {
                PDSimpleFont.LOG.error("Error: Could not find predefined CMAP file for '" + cmapName + "'");
            }
        }
    }
    
    private void extractToUnicodeEncoding() {
        COSName encodingName = null;
        String cmapName = null;
        final COSBase toUnicode = this.getToUnicode();
        if (toUnicode != null) {
            this.setHasToUnicode(true);
            if (toUnicode instanceof COSStream) {
                try {
                    this.toUnicodeCmap = this.parseCmap(null, ((COSStream)toUnicode).getUnfilteredStream());
                }
                catch (IOException exception) {
                    PDSimpleFont.LOG.error("Error: Could not load embedded ToUnicode CMap");
                }
            }
            else if (toUnicode instanceof COSName) {
                encodingName = (COSName)toUnicode;
                this.toUnicodeCmap = PDSimpleFont.cmapObjects.get(encodingName.getName());
                if (this.toUnicodeCmap == null) {
                    cmapName = encodingName.getName();
                    final String resourceName = "org/apache/pdfbox/resources/cmap/" + cmapName;
                    try {
                        this.toUnicodeCmap = this.parseCmap("org/apache/pdfbox/resources/cmap/", ResourceLoader.loadResource(resourceName));
                    }
                    catch (IOException exception2) {
                        PDSimpleFont.LOG.error("Error: Could not find predefined ToUnicode CMap file for '" + cmapName + "'");
                    }
                    if (this.toUnicodeCmap == null) {
                        PDSimpleFont.LOG.error("Error: Could not parse predefined ToUnicode CMap file for '" + cmapName + "'");
                    }
                }
            }
        }
    }
    
    protected boolean isFontSubstituted() {
        return this.isFontSubstituted;
    }
    
    protected void setIsFontSubstituted(final boolean isSubstituted) {
        this.isFontSubstituted = isSubstituted;
    }
    
    static {
        LOG = LogFactory.getLog(PDSimpleFont.class);
    }
}
