// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.edit;

import org.apache.commons.logging.LogFactory;
import java.awt.color.ColorSpace;
import java.awt.Color;
import org.apache.pdfbox.pdmodel.graphics.color.PDICCBased;
import org.apache.pdfbox.pdmodel.graphics.color.PDDeviceN;
import org.apache.pdfbox.pdmodel.graphics.color.PDPattern;
import org.apache.pdfbox.pdmodel.graphics.color.PDSeparation;
import org.apache.pdfbox.pdmodel.common.COSObjectable;
import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSDictionary;
import org.apache.pdfbox.pdmodel.graphics.color.PDDeviceCMYK;
import org.apache.pdfbox.pdmodel.graphics.color.PDDeviceRGB;
import java.io.ByteArrayOutputStream;
import org.apache.pdfbox.cos.COSString;
import java.awt.geom.AffineTransform;
import org.apache.pdfbox.pdmodel.graphics.xobject.PDXObject;
import org.apache.pdfbox.pdmodel.graphics.xobject.PDXObjectImage;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.cos.COSStream;
import java.util.List;
import org.apache.pdfbox.cos.COSName;
import java.util.ArrayList;
import org.apache.pdfbox.cos.COSArray;
import org.apache.pdfbox.pdmodel.common.COSStreamArray;
import org.apache.pdfbox.pdmodel.common.PDStream;
import java.util.Locale;
import org.apache.pdfbox.pdmodel.graphics.color.PDDeviceGray;
import java.io.IOException;
import org.apache.pdfbox.pdmodel.PDDocument;
import java.text.NumberFormat;
import org.apache.pdfbox.pdmodel.graphics.color.PDColorSpace;
import org.apache.pdfbox.pdmodel.PDResources;
import java.io.OutputStream;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.commons.logging.Log;

public class PDPageContentStream
{
    private static final Log LOG;
    private PDPage page;
    private OutputStream output;
    private boolean inTextMode;
    private PDResources resources;
    private PDColorSpace currentStrokingColorSpace;
    private PDColorSpace currentNonStrokingColorSpace;
    private float[] colorComponents;
    private NumberFormat formatDecimal;
    private static final String BEGIN_TEXT = "BT\n";
    private static final String END_TEXT = "ET\n";
    private static final String SET_FONT = "Tf\n";
    private static final String MOVE_TEXT_POSITION = "Td\n";
    private static final String SET_TEXT_MATRIX = "Tm\n";
    private static final String SHOW_TEXT = "Tj\n";
    private static final String SAVE_GRAPHICS_STATE = "q\n";
    private static final String RESTORE_GRAPHICS_STATE = "Q\n";
    private static final String CONCATENATE_MATRIX = "cm\n";
    private static final String XOBJECT_DO = "Do\n";
    private static final String RG_STROKING = "RG\n";
    private static final String RG_NON_STROKING = "rg\n";
    private static final String K_STROKING = "K\n";
    private static final String K_NON_STROKING = "k\n";
    private static final String G_STROKING = "G\n";
    private static final String G_NON_STROKING = "g\n";
    private static final String RECTANGLE = "re\n";
    private static final String FILL_NON_ZERO = "f\n";
    private static final String FILL_EVEN_ODD = "f*\n";
    private static final String LINE_TO = "l\n";
    private static final String MOVE_TO = "m\n";
    private static final String CLOSE_STROKE = "s\n";
    private static final String STROKE = "S\n";
    private static final String LINE_WIDTH = "w\n";
    private static final String LINE_JOIN_STYLE = "j\n";
    private static final String LINE_CAP_STYLE = "J\n";
    private static final String LINE_DASH_PATTERN = "d\n";
    private static final String CLOSE_SUBPATH = "h\n";
    private static final String CLIP_PATH_NON_ZERO = "W\n";
    private static final String CLIP_PATH_EVEN_ODD = "W*\n";
    private static final String NOP = "n\n";
    private static final String BEZIER_312 = "c\n";
    private static final String BEZIER_32 = "v\n";
    private static final String BEZIER_313 = "y\n";
    private static final String BMC = "BMC\n";
    private static final String BDC = "BDC\n";
    private static final String EMC = "EMC\n";
    private static final String SET_STROKING_COLORSPACE = "CS\n";
    private static final String SET_NON_STROKING_COLORSPACE = "cs\n";
    private static final String SET_STROKING_COLOR_SIMPLE = "SC\n";
    private static final String SET_STROKING_COLOR_COMPLEX = "SCN\n";
    private static final String SET_NON_STROKING_COLOR_SIMPLE = "sc\n";
    private static final String SET_NON_STROKING_COLOR_COMPLEX = "scn\n";
    private static final int SPACE = 32;
    
    public PDPageContentStream(final PDDocument document, final PDPage sourcePage) throws IOException {
        this(document, sourcePage, false, true);
    }
    
    public PDPageContentStream(final PDDocument document, final PDPage sourcePage, final boolean appendContent, final boolean compress) throws IOException {
        this(document, sourcePage, appendContent, compress, false);
    }
    
    public PDPageContentStream(final PDDocument document, final PDPage sourcePage, final boolean appendContent, final boolean compress, final boolean resetContext) throws IOException {
        this.inTextMode = false;
        this.currentStrokingColorSpace = new PDDeviceGray();
        this.currentNonStrokingColorSpace = new PDDeviceGray();
        this.colorComponents = new float[4];
        this.formatDecimal = NumberFormat.getNumberInstance(Locale.US);
        this.page = sourcePage;
        this.resources = this.page.getResources();
        if (this.resources == null) {
            this.resources = new PDResources();
            this.page.setResources(this.resources);
        }
        PDStream contents = sourcePage.getContents();
        final boolean hasContent = contents != null;
        if (appendContent && hasContent) {
            final PDStream contentsToAppend = new PDStream(document);
            COSStreamArray compoundStream = null;
            if (contents.getStream() instanceof COSStreamArray) {
                compoundStream = (COSStreamArray)contents.getStream();
                compoundStream.appendStream(contentsToAppend.getStream());
            }
            else {
                final COSArray newArray = new COSArray();
                newArray.add(contents.getCOSObject());
                newArray.add(contentsToAppend.getCOSObject());
                compoundStream = new COSStreamArray(newArray);
            }
            if (compress) {
                final List<COSName> filters = new ArrayList<COSName>();
                filters.add(COSName.FLATE_DECODE);
                contentsToAppend.setFilters(filters);
            }
            if (resetContext) {
                final PDStream saveGraphics = new PDStream(document);
                this.output = saveGraphics.createOutputStream();
                this.saveGraphicsState();
                this.close();
                if (compress) {
                    final List<COSName> filters2 = new ArrayList<COSName>();
                    filters2.add(COSName.FLATE_DECODE);
                    saveGraphics.setFilters(filters2);
                }
                compoundStream.insertCOSStream(saveGraphics);
            }
            sourcePage.setContents(new PDStream(compoundStream));
            this.output = contentsToAppend.createOutputStream();
            if (resetContext) {
                this.restoreGraphicsState();
            }
        }
        else {
            if (hasContent) {
                PDPageContentStream.LOG.warn("You are overwriting an existing content, you should use the append mode");
            }
            contents = new PDStream(document);
            if (compress) {
                final List<COSName> filters3 = new ArrayList<COSName>();
                filters3.add(COSName.FLATE_DECODE);
                contents.setFilters(filters3);
            }
            sourcePage.setContents(contents);
            this.output = contents.createOutputStream();
        }
        this.formatDecimal.setMaximumFractionDigits(10);
        this.formatDecimal.setGroupingUsed(false);
    }
    
    public void beginText() throws IOException {
        if (this.inTextMode) {
            throw new IOException("Error: Nested beginText() calls are not allowed.");
        }
        this.appendRawCommands("BT\n");
        this.inTextMode = true;
    }
    
    public void endText() throws IOException {
        if (!this.inTextMode) {
            throw new IOException("Error: You must call beginText() before calling endText.");
        }
        this.appendRawCommands("ET\n");
        this.inTextMode = false;
    }
    
    public void setFont(final PDFont font, final float fontSize) throws IOException {
        final String fontMapping = this.resources.addFont(font);
        this.appendRawCommands("/");
        this.appendRawCommands(fontMapping);
        this.appendRawCommands(32);
        this.appendRawCommands(this.formatDecimal.format(fontSize));
        this.appendRawCommands(32);
        this.appendRawCommands("Tf\n");
    }
    
    public void drawImage(final PDXObjectImage image, final float x, final float y) throws IOException {
        this.drawXObject(image, x, y, (float)image.getWidth(), (float)image.getHeight());
    }
    
    public void drawXObject(final PDXObject xobject, final float x, final float y, final float width, final float height) throws IOException {
        final AffineTransform transform = new AffineTransform(width, 0.0f, 0.0f, height, x, y);
        this.drawXObject(xobject, transform);
    }
    
    public void drawXObject(final PDXObject xobject, final AffineTransform transform) throws IOException {
        String xObjectPrefix = null;
        if (xobject instanceof PDXObjectImage) {
            xObjectPrefix = "Im";
        }
        else {
            xObjectPrefix = "Form";
        }
        final String objMapping = this.resources.addXObject(xobject, xObjectPrefix);
        this.saveGraphicsState();
        this.appendRawCommands(32);
        this.concatenate2CTM(transform);
        this.appendRawCommands(32);
        this.appendRawCommands("/");
        this.appendRawCommands(objMapping);
        this.appendRawCommands(32);
        this.appendRawCommands("Do\n");
        this.restoreGraphicsState();
    }
    
    public void moveTextPositionByAmount(final float x, final float y) throws IOException {
        if (!this.inTextMode) {
            throw new IOException("Error: must call beginText() before moveTextPositionByAmount");
        }
        this.appendRawCommands(this.formatDecimal.format(x));
        this.appendRawCommands(32);
        this.appendRawCommands(this.formatDecimal.format(y));
        this.appendRawCommands(32);
        this.appendRawCommands("Td\n");
    }
    
    public void setTextMatrix(final double a, final double b, final double c, final double d, final double e, final double f) throws IOException {
        if (!this.inTextMode) {
            throw new IOException("Error: must call beginText() before setTextMatrix");
        }
        this.appendRawCommands(this.formatDecimal.format(a));
        this.appendRawCommands(32);
        this.appendRawCommands(this.formatDecimal.format(b));
        this.appendRawCommands(32);
        this.appendRawCommands(this.formatDecimal.format(c));
        this.appendRawCommands(32);
        this.appendRawCommands(this.formatDecimal.format(d));
        this.appendRawCommands(32);
        this.appendRawCommands(this.formatDecimal.format(e));
        this.appendRawCommands(32);
        this.appendRawCommands(this.formatDecimal.format(f));
        this.appendRawCommands(32);
        this.appendRawCommands("Tm\n");
    }
    
    public void setTextMatrix(final AffineTransform matrix) throws IOException {
        this.appendMatrix(matrix);
        this.appendRawCommands("Tm\n");
    }
    
    public void setTextScaling(final double sx, final double sy, final double tx, final double ty) throws IOException {
        this.setTextMatrix(sx, 0.0, 0.0, sy, tx, ty);
    }
    
    public void setTextTranslation(final double tx, final double ty) throws IOException {
        this.setTextMatrix(1.0, 0.0, 0.0, 1.0, tx, ty);
    }
    
    public void setTextRotation(final double angle, final double tx, final double ty) throws IOException {
        final double angleCos = Math.cos(angle);
        final double angleSin = Math.sin(angle);
        this.setTextMatrix(angleCos, angleSin, -angleSin, angleCos, tx, ty);
    }
    
    public void concatenate2CTM(final double a, final double b, final double c, final double d, final double e, final double f) throws IOException {
        this.appendRawCommands(this.formatDecimal.format(a));
        this.appendRawCommands(32);
        this.appendRawCommands(this.formatDecimal.format(b));
        this.appendRawCommands(32);
        this.appendRawCommands(this.formatDecimal.format(c));
        this.appendRawCommands(32);
        this.appendRawCommands(this.formatDecimal.format(d));
        this.appendRawCommands(32);
        this.appendRawCommands(this.formatDecimal.format(e));
        this.appendRawCommands(32);
        this.appendRawCommands(this.formatDecimal.format(f));
        this.appendRawCommands(32);
        this.appendRawCommands("cm\n");
    }
    
    public void concatenate2CTM(final AffineTransform at) throws IOException {
        this.appendMatrix(at);
        this.appendRawCommands("cm\n");
    }
    
    public void drawString(final String text) throws IOException {
        if (!this.inTextMode) {
            throw new IOException("Error: must call beginText() before drawString");
        }
        final COSString string = new COSString(text);
        final ByteArrayOutputStream buffer = new ByteArrayOutputStream();
        string.writePDF(buffer);
        this.appendRawCommands(new String(buffer.toByteArray(), "ISO-8859-1"));
        this.appendRawCommands(32);
        this.appendRawCommands("Tj\n");
    }
    
    public void setStrokingColorSpace(final PDColorSpace colorSpace) throws IOException {
        this.writeColorSpace(this.currentStrokingColorSpace = colorSpace);
        this.appendRawCommands("CS\n");
    }
    
    public void setNonStrokingColorSpace(final PDColorSpace colorSpace) throws IOException {
        this.writeColorSpace(this.currentNonStrokingColorSpace = colorSpace);
        this.appendRawCommands("cs\n");
    }
    
    private void writeColorSpace(final PDColorSpace colorSpace) throws IOException {
        COSName key = null;
        if (colorSpace instanceof PDDeviceGray || colorSpace instanceof PDDeviceRGB || colorSpace instanceof PDDeviceCMYK) {
            key = COSName.getPDFName(colorSpace.getName());
        }
        else {
            COSDictionary colorSpaces = (COSDictionary)this.resources.getCOSDictionary().getDictionaryObject(COSName.COLORSPACE);
            if (colorSpaces == null) {
                colorSpaces = new COSDictionary();
                this.resources.getCOSDictionary().setItem(COSName.COLORSPACE, colorSpaces);
            }
            key = colorSpaces.getKeyForValue(colorSpace.getCOSObject());
            if (key == null) {
                int counter;
                String csName;
                for (counter = 0, csName = "CS"; colorSpaces.containsValue(csName + counter); ++counter) {}
                key = COSName.getPDFName(csName + counter);
                colorSpaces.setItem(key, colorSpace);
            }
        }
        key.writePDF(this.output);
        this.appendRawCommands(32);
    }
    
    public void setStrokingColor(final float[] components) throws IOException {
        for (int i = 0; i < components.length; ++i) {
            this.appendRawCommands(this.formatDecimal.format(components[i]));
            this.appendRawCommands(32);
        }
        if (this.currentStrokingColorSpace instanceof PDSeparation || this.currentStrokingColorSpace instanceof PDPattern || this.currentStrokingColorSpace instanceof PDDeviceN || this.currentStrokingColorSpace instanceof PDICCBased) {
            this.appendRawCommands("SCN\n");
        }
        else {
            this.appendRawCommands("SC\n");
        }
    }
    
    public void setStrokingColor(final Color color) throws IOException {
        final ColorSpace colorSpace = color.getColorSpace();
        if (colorSpace.getType() == 5) {
            this.setStrokingColor(color.getRed(), color.getGreen(), color.getBlue());
        }
        else if (colorSpace.getType() == 6) {
            color.getColorComponents(this.colorComponents);
            this.setStrokingColor(this.colorComponents[0]);
        }
        else {
            if (colorSpace.getType() != 9) {
                throw new IOException("Error: unknown colorspace:" + colorSpace);
            }
            color.getColorComponents(this.colorComponents);
            this.setStrokingColor(this.colorComponents[0], this.colorComponents[2], this.colorComponents[2], this.colorComponents[3]);
        }
    }
    
    public void setNonStrokingColor(final Color color) throws IOException {
        final ColorSpace colorSpace = color.getColorSpace();
        if (colorSpace.getType() == 5) {
            this.setNonStrokingColor(color.getRed(), color.getGreen(), color.getBlue());
        }
        else if (colorSpace.getType() == 6) {
            color.getColorComponents(this.colorComponents);
            this.setNonStrokingColor(this.colorComponents[0]);
        }
        else {
            if (colorSpace.getType() != 9) {
                throw new IOException("Error: unknown colorspace:" + colorSpace);
            }
            color.getColorComponents(this.colorComponents);
            this.setNonStrokingColor(this.colorComponents[0], this.colorComponents[2], this.colorComponents[2], this.colorComponents[3]);
        }
    }
    
    public void setStrokingColor(final int r, final int g, final int b) throws IOException {
        this.appendRawCommands(this.formatDecimal.format(r / 255.0));
        this.appendRawCommands(32);
        this.appendRawCommands(this.formatDecimal.format(g / 255.0));
        this.appendRawCommands(32);
        this.appendRawCommands(this.formatDecimal.format(b / 255.0));
        this.appendRawCommands(32);
        this.appendRawCommands("RG\n");
    }
    
    public void setStrokingColor(final int c, final int m, final int y, final int k) throws IOException {
        this.appendRawCommands(this.formatDecimal.format(c / 255.0));
        this.appendRawCommands(32);
        this.appendRawCommands(this.formatDecimal.format(m / 255.0));
        this.appendRawCommands(32);
        this.appendRawCommands(this.formatDecimal.format(y / 255.0));
        this.appendRawCommands(32);
        this.appendRawCommands(this.formatDecimal.format(k / 255.0));
        this.appendRawCommands(32);
        this.appendRawCommands("K\n");
    }
    
    public void setStrokingColor(final double c, final double m, final double y, final double k) throws IOException {
        this.appendRawCommands(this.formatDecimal.format(c));
        this.appendRawCommands(32);
        this.appendRawCommands(this.formatDecimal.format(m));
        this.appendRawCommands(32);
        this.appendRawCommands(this.formatDecimal.format(y));
        this.appendRawCommands(32);
        this.appendRawCommands(this.formatDecimal.format(k));
        this.appendRawCommands(32);
        this.appendRawCommands("K\n");
    }
    
    public void setStrokingColor(final int g) throws IOException {
        this.appendRawCommands(this.formatDecimal.format(g / 255.0));
        this.appendRawCommands(32);
        this.appendRawCommands("G\n");
    }
    
    public void setStrokingColor(final double g) throws IOException {
        this.appendRawCommands(this.formatDecimal.format(g));
        this.appendRawCommands(32);
        this.appendRawCommands("G\n");
    }
    
    public void setNonStrokingColor(final float[] components) throws IOException {
        for (int i = 0; i < components.length; ++i) {
            this.appendRawCommands(this.formatDecimal.format(components[i]));
            this.appendRawCommands(32);
        }
        if (this.currentNonStrokingColorSpace instanceof PDSeparation || this.currentNonStrokingColorSpace instanceof PDPattern || this.currentNonStrokingColorSpace instanceof PDDeviceN || this.currentNonStrokingColorSpace instanceof PDICCBased) {
            this.appendRawCommands("scn\n");
        }
        else {
            this.appendRawCommands("sc\n");
        }
    }
    
    public void setNonStrokingColor(final int r, final int g, final int b) throws IOException {
        this.appendRawCommands(this.formatDecimal.format(r / 255.0));
        this.appendRawCommands(32);
        this.appendRawCommands(this.formatDecimal.format(g / 255.0));
        this.appendRawCommands(32);
        this.appendRawCommands(this.formatDecimal.format(b / 255.0));
        this.appendRawCommands(32);
        this.appendRawCommands("rg\n");
    }
    
    public void setNonStrokingColor(final int c, final int m, final int y, final int k) throws IOException {
        this.appendRawCommands(this.formatDecimal.format(c / 255.0));
        this.appendRawCommands(32);
        this.appendRawCommands(this.formatDecimal.format(m / 255.0));
        this.appendRawCommands(32);
        this.appendRawCommands(this.formatDecimal.format(y / 255.0));
        this.appendRawCommands(32);
        this.appendRawCommands(this.formatDecimal.format(k / 255.0));
        this.appendRawCommands(32);
        this.appendRawCommands("k\n");
    }
    
    public void setNonStrokingColor(final double c, final double m, final double y, final double k) throws IOException {
        this.appendRawCommands(this.formatDecimal.format(c));
        this.appendRawCommands(32);
        this.appendRawCommands(this.formatDecimal.format(m));
        this.appendRawCommands(32);
        this.appendRawCommands(this.formatDecimal.format(y));
        this.appendRawCommands(32);
        this.appendRawCommands(this.formatDecimal.format(k));
        this.appendRawCommands(32);
        this.appendRawCommands("k\n");
    }
    
    public void setNonStrokingColor(final int g) throws IOException {
        this.appendRawCommands(this.formatDecimal.format(g / 255.0));
        this.appendRawCommands(32);
        this.appendRawCommands("g\n");
    }
    
    public void setNonStrokingColor(final double g) throws IOException {
        this.appendRawCommands(this.formatDecimal.format(g));
        this.appendRawCommands(32);
        this.appendRawCommands("g\n");
    }
    
    public void addRect(final float x, final float y, final float width, final float height) throws IOException {
        this.appendRawCommands(this.formatDecimal.format(x));
        this.appendRawCommands(32);
        this.appendRawCommands(this.formatDecimal.format(y));
        this.appendRawCommands(32);
        this.appendRawCommands(this.formatDecimal.format(width));
        this.appendRawCommands(32);
        this.appendRawCommands(this.formatDecimal.format(height));
        this.appendRawCommands(32);
        this.appendRawCommands("re\n");
    }
    
    public void fillRect(final float x, final float y, final float width, final float height) throws IOException {
        this.addRect(x, y, width, height);
        this.fill(1);
    }
    
    public void addBezier312(final float x1, final float y1, final float x2, final float y2, final float x3, final float y3) throws IOException {
        this.appendRawCommands(this.formatDecimal.format(x1));
        this.appendRawCommands(32);
        this.appendRawCommands(this.formatDecimal.format(y1));
        this.appendRawCommands(32);
        this.appendRawCommands(this.formatDecimal.format(x2));
        this.appendRawCommands(32);
        this.appendRawCommands(this.formatDecimal.format(y2));
        this.appendRawCommands(32);
        this.appendRawCommands(this.formatDecimal.format(x3));
        this.appendRawCommands(32);
        this.appendRawCommands(this.formatDecimal.format(y3));
        this.appendRawCommands(32);
        this.appendRawCommands("c\n");
    }
    
    public void addBezier32(final float x2, final float y2, final float x3, final float y3) throws IOException {
        this.appendRawCommands(this.formatDecimal.format(x2));
        this.appendRawCommands(32);
        this.appendRawCommands(this.formatDecimal.format(y2));
        this.appendRawCommands(32);
        this.appendRawCommands(this.formatDecimal.format(x3));
        this.appendRawCommands(32);
        this.appendRawCommands(this.formatDecimal.format(y3));
        this.appendRawCommands(32);
        this.appendRawCommands("v\n");
    }
    
    public void addBezier31(final float x1, final float y1, final float x3, final float y3) throws IOException {
        this.appendRawCommands(this.formatDecimal.format(x1));
        this.appendRawCommands(32);
        this.appendRawCommands(this.formatDecimal.format(y1));
        this.appendRawCommands(32);
        this.appendRawCommands(this.formatDecimal.format(x3));
        this.appendRawCommands(32);
        this.appendRawCommands(this.formatDecimal.format(y3));
        this.appendRawCommands(32);
        this.appendRawCommands("y\n");
    }
    
    public void moveTo(final float x, final float y) throws IOException {
        this.appendRawCommands(this.formatDecimal.format(x));
        this.appendRawCommands(32);
        this.appendRawCommands(this.formatDecimal.format(y));
        this.appendRawCommands(32);
        this.appendRawCommands("m\n");
    }
    
    public void lineTo(final float x, final float y) throws IOException {
        this.appendRawCommands(this.formatDecimal.format(x));
        this.appendRawCommands(32);
        this.appendRawCommands(this.formatDecimal.format(y));
        this.appendRawCommands(32);
        this.appendRawCommands("l\n");
    }
    
    public void addLine(final float xStart, final float yStart, final float xEnd, final float yEnd) throws IOException {
        this.moveTo(xStart, yStart);
        this.lineTo(xEnd, yEnd);
    }
    
    public void drawLine(final float xStart, final float yStart, final float xEnd, final float yEnd) throws IOException {
        this.addLine(xStart, yStart, xEnd, yEnd);
        this.stroke();
    }
    
    public void addPolygon(final float[] x, final float[] y) throws IOException {
        if (x.length != y.length) {
            throw new IOException("Error: some points are missing coordinate");
        }
        for (int i = 0; i < x.length; ++i) {
            if (i == 0) {
                this.moveTo(x[i], y[i]);
            }
            else {
                this.lineTo(x[i], y[i]);
            }
        }
        this.closeSubPath();
    }
    
    public void drawPolygon(final float[] x, final float[] y) throws IOException {
        this.addPolygon(x, y);
        this.stroke();
    }
    
    public void fillPolygon(final float[] x, final float[] y) throws IOException {
        this.addPolygon(x, y);
        this.fill(1);
    }
    
    public void stroke() throws IOException {
        this.appendRawCommands("S\n");
    }
    
    public void closeAndStroke() throws IOException {
        this.appendRawCommands("s\n");
    }
    
    public void fill(final int windingRule) throws IOException {
        if (windingRule == 1) {
            this.appendRawCommands("f\n");
        }
        else {
            if (windingRule != 0) {
                throw new IOException("Error: unknown value for winding rule");
            }
            this.appendRawCommands("f*\n");
        }
    }
    
    public void closeSubPath() throws IOException {
        this.appendRawCommands("h\n");
    }
    
    public void clipPath(final int windingRule) throws IOException {
        if (windingRule == 1) {
            this.appendRawCommands("W\n");
            this.appendRawCommands("n\n");
        }
        else {
            if (windingRule != 0) {
                throw new IOException("Error: unknown value for winding rule");
            }
            this.appendRawCommands("W*\n");
            this.appendRawCommands("n\n");
        }
    }
    
    public void setLineWidth(final float lineWidth) throws IOException {
        this.appendRawCommands(this.formatDecimal.format(lineWidth));
        this.appendRawCommands(32);
        this.appendRawCommands("w\n");
    }
    
    public void setLineJoinStyle(final int lineJoinStyle) throws IOException {
        if (lineJoinStyle >= 0 && lineJoinStyle <= 2) {
            this.appendRawCommands(Integer.toString(lineJoinStyle));
            this.appendRawCommands(32);
            this.appendRawCommands("j\n");
            return;
        }
        throw new IOException("Error: unknown value for line join style");
    }
    
    public void setLineCapStyle(final int lineCapStyle) throws IOException {
        if (lineCapStyle >= 0 && lineCapStyle <= 2) {
            this.appendRawCommands(Integer.toString(lineCapStyle));
            this.appendRawCommands(32);
            this.appendRawCommands("J\n");
            return;
        }
        throw new IOException("Error: unknown value for line cap style");
    }
    
    public void setLineDashPattern(final float[] pattern, final float phase) throws IOException {
        this.appendRawCommands("[");
        for (final float value : pattern) {
            this.appendRawCommands(this.formatDecimal.format(value));
            this.appendRawCommands(32);
        }
        this.appendRawCommands("] ");
        this.appendRawCommands(this.formatDecimal.format(phase));
        this.appendRawCommands(32);
        this.appendRawCommands("d\n");
    }
    
    public void beginMarkedContentSequence(final COSName tag) throws IOException {
        this.appendCOSName(tag);
        this.appendRawCommands(32);
        this.appendRawCommands("BMC\n");
    }
    
    public void beginMarkedContentSequence(final COSName tag, final COSName propsName) throws IOException {
        this.appendCOSName(tag);
        this.appendRawCommands(32);
        this.appendCOSName(propsName);
        this.appendRawCommands(32);
        this.appendRawCommands("BDC\n");
    }
    
    public void endMarkedContentSequence() throws IOException {
        this.appendRawCommands("EMC\n");
    }
    
    public void saveGraphicsState() throws IOException {
        this.appendRawCommands("q\n");
    }
    
    public void restoreGraphicsState() throws IOException {
        this.appendRawCommands("Q\n");
    }
    
    public void appendRawCommands(final String commands) throws IOException {
        this.appendRawCommands(commands.getBytes("ISO-8859-1"));
    }
    
    public void appendRawCommands(final byte[] commands) throws IOException {
        this.output.write(commands);
    }
    
    public void appendRawCommands(final int data) throws IOException {
        this.output.write(data);
    }
    
    public void appendCOSName(final COSName name) throws IOException {
        name.writePDF(this.output);
    }
    
    private void appendMatrix(final AffineTransform transform) throws IOException {
        final double[] values = new double[6];
        transform.getMatrix(values);
        for (final double v : values) {
            this.appendRawCommands(this.formatDecimal.format(v));
            this.appendRawCommands(32);
        }
    }
    
    public void close() throws IOException {
        this.output.close();
    }
    
    static {
        LOG = LogFactory.getLog(PDPageContentStream.class);
    }
}
