// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel;

import java.io.IOException;
import org.apache.pdfbox.cos.COSStream;
import org.apache.pdfbox.cos.COSString;
import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSDictionary;
import org.apache.pdfbox.pdmodel.common.PDTextStream;
import org.apache.pdfbox.pdmodel.common.PDNameTreeNode;

public class PDJavascriptNameTreeNode extends PDNameTreeNode
{
    public PDJavascriptNameTreeNode() {
        super(PDTextStream.class);
    }
    
    public PDJavascriptNameTreeNode(final COSDictionary dic) {
        super(dic, PDTextStream.class);
    }
    
    @Override
    protected Object convertCOSToPD(final COSBase base) throws IOException {
        PDTextStream stream = null;
        if (base instanceof COSString) {
            stream = new PDTextStream((COSString)base);
        }
        else {
            if (!(base instanceof COSStream)) {
                throw new IOException("Error creating Javascript object, expected either COSString or COSStream and not " + base);
            }
            stream = new PDTextStream((COSStream)base);
        }
        return stream;
    }
    
    @Override
    protected PDNameTreeNode createChildNode(final COSDictionary dic) {
        return new PDJavascriptNameTreeNode(dic);
    }
}
