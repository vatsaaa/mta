// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.interactive.viewerpreferences;

import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSDictionary;
import org.apache.pdfbox.pdmodel.common.COSObjectable;

public class PDViewerPreferences implements COSObjectable
{
    @Deprecated
    public static final String NON_FULL_SCREEN_PAGE_MODE_USE_NONE = "UseNone";
    @Deprecated
    public static final String NON_FULL_SCREEN_PAGE_MODE_USE_OUTLINES = "UseOutlines";
    @Deprecated
    public static final String NON_FULL_SCREEN_PAGE_MODE_USE_THUMBS = "UseThumbs";
    @Deprecated
    public static final String NON_FULL_SCREEN_PAGE_MODE_USE_OPTIONAL_CONTENT = "UseOC";
    @Deprecated
    public static final String READING_DIRECTION_L2R = "L2R";
    @Deprecated
    public static final String READING_DIRECTION_R2L = "R2L";
    @Deprecated
    public static final String BOUNDARY_MEDIA_BOX = "MediaBox";
    @Deprecated
    public static final String BOUNDARY_CROP_BOX = "CropBox";
    @Deprecated
    public static final String BOUNDARY_BLEED_BOX = "BleedBox";
    @Deprecated
    public static final String BOUNDARY_TRIM_BOX = "TrimBox";
    @Deprecated
    public static final String BOUNDARY_ART_BOX = "ArtBox";
    private COSDictionary prefs;
    
    public PDViewerPreferences(final COSDictionary dic) {
        this.prefs = dic;
    }
    
    public COSDictionary getDictionary() {
        return this.prefs;
    }
    
    public COSBase getCOSObject() {
        return this.prefs;
    }
    
    public boolean hideToolbar() {
        return this.prefs.getBoolean(COSName.HIDE_TOOLBAR, false);
    }
    
    public void setHideToolbar(final boolean value) {
        this.prefs.setBoolean(COSName.HIDE_TOOLBAR, value);
    }
    
    public boolean hideMenubar() {
        return this.prefs.getBoolean(COSName.HIDE_MENUBAR, false);
    }
    
    public void setHideMenubar(final boolean value) {
        this.prefs.setBoolean(COSName.HIDE_MENUBAR, value);
    }
    
    public boolean hideWindowUI() {
        return this.prefs.getBoolean(COSName.HIDE_WINDOWUI, false);
    }
    
    public void setHideWindowUI(final boolean value) {
        this.prefs.setBoolean(COSName.HIDE_WINDOWUI, value);
    }
    
    public boolean fitWindow() {
        return this.prefs.getBoolean(COSName.FIT_WINDOW, false);
    }
    
    public void setFitWindow(final boolean value) {
        this.prefs.setBoolean(COSName.FIT_WINDOW, value);
    }
    
    public boolean centerWindow() {
        return this.prefs.getBoolean(COSName.CENTER_WINDOW, false);
    }
    
    public void setCenterWindow(final boolean value) {
        this.prefs.setBoolean(COSName.CENTER_WINDOW, value);
    }
    
    public boolean displayDocTitle() {
        return this.prefs.getBoolean(COSName.DISPLAY_DOC_TITLE, false);
    }
    
    public void setDisplayDocTitle(final boolean value) {
        this.prefs.setBoolean(COSName.DISPLAY_DOC_TITLE, value);
    }
    
    public String getNonFullScreenPageMode() {
        return this.prefs.getNameAsString(COSName.NON_FULL_SCREEN_PAGE_MODE, NON_FULL_SCREEN_PAGE_MODE.UseNone.toString());
    }
    
    public void setNonFullScreenPageMode(final NON_FULL_SCREEN_PAGE_MODE value) {
        this.prefs.setName(COSName.NON_FULL_SCREEN_PAGE_MODE, value.toString());
    }
    
    @Deprecated
    public void setNonFullScreenPageMode(final String value) {
        this.prefs.setName(COSName.NON_FULL_SCREEN_PAGE_MODE, value);
    }
    
    public String getReadingDirection() {
        return this.prefs.getNameAsString(COSName.DIRECTION, READING_DIRECTION.L2R.toString());
    }
    
    public void setReadingDirection(final READING_DIRECTION value) {
        this.prefs.setName(COSName.DIRECTION, value.toString());
    }
    
    @Deprecated
    public void setReadingDirection(final String value) {
        this.prefs.setName(COSName.DIRECTION, value.toString());
    }
    
    public String getViewArea() {
        return this.prefs.getNameAsString(COSName.VIEW_AREA, BOUNDARY.CropBox.toString());
    }
    
    @Deprecated
    public void setViewArea(final String value) {
        this.prefs.setName(COSName.VIEW_AREA, value);
    }
    
    public void setViewArea(final BOUNDARY value) {
        this.prefs.setName(COSName.VIEW_AREA, value.toString());
    }
    
    public String getViewClip() {
        return this.prefs.getNameAsString(COSName.VIEW_CLIP, BOUNDARY.CropBox.toString());
    }
    
    public void setViewClip(final BOUNDARY value) {
        this.prefs.setName(COSName.VIEW_CLIP, value.toString());
    }
    
    @Deprecated
    public void setViewClip(final String value) {
        this.prefs.setName(COSName.VIEW_CLIP, value);
    }
    
    public String getPrintArea() {
        return this.prefs.getNameAsString(COSName.PRINT_AREA, BOUNDARY.CropBox.toString());
    }
    
    @Deprecated
    public void setPrintArea(final String value) {
        this.prefs.setName(COSName.PRINT_AREA, value);
    }
    
    public void setPrintArea(final BOUNDARY value) {
        this.prefs.setName(COSName.PRINT_AREA, value.toString());
    }
    
    public String getPrintClip() {
        return this.prefs.getNameAsString(COSName.PRINT_CLIP, BOUNDARY.CropBox.toString());
    }
    
    @Deprecated
    public void setPrintClip(final String value) {
        this.prefs.setName(COSName.PRINT_CLIP, value);
    }
    
    public void setPrintClip(final BOUNDARY value) {
        this.prefs.setName(COSName.PRINT_CLIP, value.toString());
    }
    
    public String getDuplex() {
        return this.prefs.getNameAsString(COSName.DUPLEX);
    }
    
    public void setDuplex(final DUPLEX value) {
        this.prefs.setName(COSName.DUPLEX, value.toString());
    }
    
    public String getPrintScaling() {
        return this.prefs.getNameAsString(COSName.PRINT_SCALING, PRINT_SCALING.AppDefault.toString());
    }
    
    public void setPrintScaling(final PRINT_SCALING value) {
        this.prefs.setName(COSName.PRINT_SCALING, value.toString());
    }
    
    public enum NON_FULL_SCREEN_PAGE_MODE
    {
        UseNone, 
        UseOutlines, 
        UseThumbs, 
        UseOC;
    }
    
    public enum READING_DIRECTION
    {
        L2R, 
        R2L;
    }
    
    public enum BOUNDARY
    {
        MediaBox, 
        CropBox, 
        BleedBox, 
        TrimBox, 
        ArtBox;
    }
    
    public enum DUPLEX
    {
        Simplex, 
        DuplexFlipShortEdge, 
        DuplexFlipLongEdge;
    }
    
    public enum PRINT_SCALING
    {
        None, 
        AppDefault;
    }
}
