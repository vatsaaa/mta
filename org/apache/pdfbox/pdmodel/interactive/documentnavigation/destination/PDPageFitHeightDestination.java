// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.interactive.documentnavigation.destination;

import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSArray;

public class PDPageFitHeightDestination extends PDPageDestination
{
    protected static final String TYPE = "FitV";
    protected static final String TYPE_BOUNDED = "FitBV";
    
    public PDPageFitHeightDestination() {
        this.array.growToSize(3);
        this.array.setName(1, "FitV");
    }
    
    public PDPageFitHeightDestination(final COSArray arr) {
        super(arr);
    }
    
    public int getLeft() {
        return this.array.getInt(2);
    }
    
    public void setLeft(final int x) {
        this.array.growToSize(3);
        if (x == -1) {
            this.array.set(2, null);
        }
        else {
            this.array.setInt(2, x);
        }
    }
    
    public boolean fitBoundingBox() {
        return "FitBV".equals(this.array.getName(1));
    }
    
    public void setFitBoundingBox(final boolean fitBoundingBox) {
        this.array.growToSize(2);
        if (fitBoundingBox) {
            this.array.setName(1, "FitBV");
        }
        else {
            this.array.setName(1, "FitV");
        }
    }
}
