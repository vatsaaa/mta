// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.interactive.documentnavigation.destination;

import org.apache.pdfbox.cos.COSArray;

public class PDPageFitDestination extends PDPageDestination
{
    protected static final String TYPE = "Fit";
    protected static final String TYPE_BOUNDED = "FitB";
    
    public PDPageFitDestination() {
        this.array.growToSize(2);
        this.array.setName(1, "Fit");
    }
    
    public PDPageFitDestination(final COSArray arr) {
        super(arr);
    }
    
    public boolean fitBoundingBox() {
        return "FitB".equals(this.array.getName(1));
    }
    
    public void setFitBoundingBox(final boolean fitBoundingBox) {
        this.array.growToSize(2);
        if (fitBoundingBox) {
            this.array.setName(1, "FitB");
        }
        else {
            this.array.setName(1, "Fit");
        }
    }
}
