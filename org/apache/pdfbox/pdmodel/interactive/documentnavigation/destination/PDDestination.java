// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.interactive.documentnavigation.destination;

import org.apache.pdfbox.cos.COSString;
import java.io.IOException;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.cos.COSArray;
import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.pdmodel.common.PDDestinationOrAction;

public abstract class PDDestination implements PDDestinationOrAction
{
    public static PDDestination create(final COSBase base) throws IOException {
        PDDestination retval = null;
        if (base != null) {
            if (base instanceof COSArray && ((COSArray)base).size() > 0) {
                final COSArray array = (COSArray)base;
                final COSName type = (COSName)array.getObject(1);
                final String typeString = type.getName();
                if (typeString.equals("Fit") || typeString.equals("FitB")) {
                    retval = new PDPageFitDestination(array);
                }
                else if (typeString.equals("FitV") || typeString.equals("FitBV")) {
                    retval = new PDPageFitHeightDestination(array);
                }
                else if (typeString.equals("FitR")) {
                    retval = new PDPageFitRectangleDestination(array);
                }
                else if (typeString.equals("FitH") || typeString.equals("FitBH")) {
                    retval = new PDPageFitWidthDestination(array);
                }
                else {
                    if (!typeString.equals("XYZ")) {
                        throw new IOException("Unknown destination type:" + type);
                    }
                    retval = new PDPageXYZDestination(array);
                }
            }
            else if (base instanceof COSString) {
                retval = new PDNamedDestination((COSString)base);
            }
            else {
                if (!(base instanceof COSName)) {
                    throw new IOException("Error: can't convert to Destination " + base);
                }
                retval = new PDNamedDestination((COSName)base);
            }
        }
        return retval;
    }
    
    @Override
    public String toString() {
        return super.toString();
    }
}
