// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.interactive.documentnavigation.destination;

import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSArray;

public class PDPageFitWidthDestination extends PDPageDestination
{
    protected static final String TYPE = "FitH";
    protected static final String TYPE_BOUNDED = "FitBH";
    
    public PDPageFitWidthDestination() {
        this.array.growToSize(3);
        this.array.setName(1, "FitH");
    }
    
    public PDPageFitWidthDestination(final COSArray arr) {
        super(arr);
    }
    
    public int getTop() {
        return this.array.getInt(2);
    }
    
    public void setTop(final int y) {
        this.array.growToSize(3);
        if (y == -1) {
            this.array.set(2, null);
        }
        else {
            this.array.setInt(2, y);
        }
    }
    
    public boolean fitBoundingBox() {
        return "FitBH".equals(this.array.getName(1));
    }
    
    public void setFitBoundingBox(final boolean fitBoundingBox) {
        this.array.growToSize(2);
        if (fitBoundingBox) {
            this.array.setName(1, "FitBH");
        }
        else {
            this.array.setName(1, "FitH");
        }
    }
}
