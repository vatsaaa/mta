// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.interactive.documentnavigation.destination;

import java.io.IOException;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.cos.COSString;
import org.apache.pdfbox.cos.COSBase;

public class PDNamedDestination extends PDDestination
{
    private COSBase namedDestination;
    
    public PDNamedDestination(final COSString dest) {
        this.namedDestination = dest;
    }
    
    public PDNamedDestination(final COSName dest) {
        this.namedDestination = dest;
    }
    
    public PDNamedDestination() {
    }
    
    public PDNamedDestination(final String dest) {
        this.namedDestination = new COSString(dest);
    }
    
    public COSBase getCOSObject() {
        return this.namedDestination;
    }
    
    public String getNamedDestination() {
        String retval = null;
        if (this.namedDestination instanceof COSString) {
            retval = ((COSString)this.namedDestination).getString();
        }
        else if (this.namedDestination instanceof COSName) {
            retval = ((COSName)this.namedDestination).getName();
        }
        return retval;
    }
    
    public void setNamedDestination(final String dest) throws IOException {
        if (this.namedDestination instanceof COSString) {
            final COSString string = (COSString)this.namedDestination;
            string.reset();
            string.append(dest.getBytes("ISO-8859-1"));
        }
        else if (dest == null) {
            this.namedDestination = null;
        }
        else {
            this.namedDestination = new COSString(dest);
        }
    }
}
