// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.interactive.documentnavigation.destination;

import java.util.List;
import java.util.ArrayList;
import org.apache.pdfbox.pdmodel.PDPageNode;
import org.apache.pdfbox.cos.COSNumber;
import org.apache.pdfbox.pdmodel.common.COSObjectable;
import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSDictionary;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.cos.COSArray;

public abstract class PDPageDestination extends PDDestination
{
    protected COSArray array;
    
    protected PDPageDestination() {
        this.array = new COSArray();
    }
    
    protected PDPageDestination(final COSArray arr) {
        this.array = arr;
    }
    
    public PDPage getPage() {
        PDPage retval = null;
        if (this.array.size() > 0) {
            final COSBase page = this.array.getObject(0);
            if (page instanceof COSDictionary) {
                retval = new PDPage((COSDictionary)page);
            }
        }
        return retval;
    }
    
    public void setPage(final PDPage page) {
        this.array.set(0, page);
    }
    
    public int getPageNumber() {
        int retval = -1;
        if (this.array.size() > 0) {
            final COSBase page = this.array.getObject(0);
            if (page instanceof COSNumber) {
                retval = ((COSNumber)page).intValue();
            }
        }
        return retval;
    }
    
    public int findPageNumber() {
        int retval = -1;
        if (this.array.size() > 0) {
            final COSBase page = this.array.getObject(0);
            if (page instanceof COSNumber) {
                retval = ((COSNumber)page).intValue();
            }
            else if (page instanceof COSDictionary) {
                COSBase parent;
                for (parent = page; ((COSDictionary)parent).getDictionaryObject("Parent", "P") != null; parent = ((COSDictionary)parent).getDictionaryObject("Parent", "P")) {}
                final PDPageNode pages = new PDPageNode((COSDictionary)parent);
                final List<PDPage> allPages = new ArrayList<PDPage>();
                pages.getAllKids(allPages);
                retval = allPages.indexOf(new PDPage((COSDictionary)page)) + 1;
            }
        }
        return retval;
    }
    
    public void setPageNumber(final int pageNumber) {
        this.array.set(0, pageNumber);
    }
    
    public COSBase getCOSObject() {
        return this.array;
    }
    
    public COSArray getCOSArray() {
        return this.array;
    }
}
