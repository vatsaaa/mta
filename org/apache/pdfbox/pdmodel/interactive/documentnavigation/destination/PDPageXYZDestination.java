// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.interactive.documentnavigation.destination;

import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSArray;

public class PDPageXYZDestination extends PDPageDestination
{
    protected static final String TYPE = "XYZ";
    
    public PDPageXYZDestination() {
        this.array.growToSize(5);
        this.array.setName(1, "XYZ");
    }
    
    public PDPageXYZDestination(final COSArray arr) {
        super(arr);
    }
    
    public int getLeft() {
        return this.array.getInt(2);
    }
    
    public void setLeft(final int x) {
        this.array.growToSize(3);
        if (x == -1) {
            this.array.set(2, null);
        }
        else {
            this.array.setInt(2, x);
        }
    }
    
    public int getTop() {
        return this.array.getInt(3);
    }
    
    public void setTop(final int y) {
        this.array.growToSize(4);
        if (y == -1) {
            this.array.set(3, null);
        }
        else {
            this.array.setInt(3, y);
        }
    }
    
    public int getZoom() {
        return this.array.getInt(4);
    }
    
    public void setZoom(final int zoom) {
        this.array.growToSize(5);
        if (zoom == -1) {
            this.array.set(4, null);
        }
        else {
            this.array.setInt(4, zoom);
        }
    }
}
