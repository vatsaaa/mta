// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.interactive.documentnavigation.outline;

import org.apache.pdfbox.util.BitFlagHelper;
import java.awt.Color;
import org.apache.pdfbox.pdmodel.graphics.color.PDColorSpace;
import org.apache.pdfbox.pdmodel.graphics.color.PDDeviceRGB;
import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSFloat;
import org.apache.pdfbox.cos.COSArray;
import org.apache.pdfbox.pdmodel.graphics.color.PDColorState;
import org.apache.pdfbox.pdmodel.documentinterchange.logicalstructure.PDStructureElement;
import org.apache.pdfbox.pdmodel.interactive.action.PDActionFactory;
import java.util.List;
import org.apache.pdfbox.pdmodel.PDDestinationNameTreeNode;
import org.apache.pdfbox.pdmodel.PDDocumentNameDictionary;
import org.apache.pdfbox.pdmodel.interactive.action.type.PDAction;
import org.apache.pdfbox.pdmodel.interactive.documentnavigation.destination.PDPageDestination;
import org.apache.pdfbox.pdmodel.interactive.documentnavigation.destination.PDNamedDestination;
import org.apache.pdfbox.exceptions.OutlineNotLocalException;
import org.apache.pdfbox.pdmodel.interactive.action.type.PDActionGoTo;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.interactive.documentnavigation.destination.PDPageXYZDestination;
import org.apache.pdfbox.pdmodel.PDPage;
import java.io.IOException;
import org.apache.pdfbox.pdmodel.interactive.documentnavigation.destination.PDDestination;
import org.apache.pdfbox.pdmodel.common.COSObjectable;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.cos.COSDictionary;

public class PDOutlineItem extends PDOutlineNode
{
    private static final int ITALIC_FLAG = 1;
    private static final int BOLD_FLAG = 2;
    
    public PDOutlineItem() {
    }
    
    public PDOutlineItem(final COSDictionary dic) {
        super(dic);
    }
    
    public void insertSiblingAfter(final PDOutlineItem item) {
        item.setParent(this.getParent());
        final PDOutlineItem next = this.getNextSibling();
        this.setNextSibling(item);
        item.setPreviousSibling(this);
        if (next != null) {
            item.setNextSibling(next);
            next.setPreviousSibling(item);
        }
        this.updateParentOpenCount(1);
    }
    
    public PDOutlineNode getParent() {
        return super.getParent();
    }
    
    public PDOutlineItem getPreviousSibling() {
        PDOutlineItem last = null;
        final COSDictionary lastDic = (COSDictionary)this.node.getDictionaryObject(COSName.PREV);
        if (lastDic != null) {
            last = new PDOutlineItem(lastDic);
        }
        return last;
    }
    
    protected void setPreviousSibling(final PDOutlineNode outlineNode) {
        this.node.setItem(COSName.PREV, outlineNode);
    }
    
    public PDOutlineItem getNextSibling() {
        PDOutlineItem last = null;
        final COSDictionary lastDic = (COSDictionary)this.node.getDictionaryObject(COSName.NEXT);
        if (lastDic != null) {
            last = new PDOutlineItem(lastDic);
        }
        return last;
    }
    
    protected void setNextSibling(final PDOutlineNode outlineNode) {
        this.node.setItem(COSName.NEXT, outlineNode);
    }
    
    public String getTitle() {
        return this.node.getString(COSName.TITLE);
    }
    
    public void setTitle(final String title) {
        this.node.setString(COSName.TITLE, title);
    }
    
    public PDDestination getDestination() throws IOException {
        return PDDestination.create(this.node.getDictionaryObject(COSName.DEST));
    }
    
    public void setDestination(final PDDestination dest) {
        this.node.setItem(COSName.DEST, dest);
    }
    
    public void setDestination(final PDPage page) {
        PDPageXYZDestination dest = null;
        if (page != null) {
            dest = new PDPageXYZDestination();
            dest.setPage(page);
        }
        this.setDestination(dest);
    }
    
    public PDPage findDestinationPage(final PDDocument doc) throws IOException {
        PDPage page = null;
        PDDestination rawDest = this.getDestination();
        if (rawDest == null) {
            final PDAction outlineAction = this.getAction();
            if (outlineAction instanceof PDActionGoTo) {
                rawDest = ((PDActionGoTo)outlineAction).getDestination();
            }
            else if (outlineAction != null) {
                throw new OutlineNotLocalException("Error: Outline does not reference a local page.");
            }
        }
        PDPageDestination pageDest = null;
        if (rawDest instanceof PDNamedDestination) {
            final PDNamedDestination namedDest = (PDNamedDestination)rawDest;
            final PDDocumentNameDictionary namesDict = doc.getDocumentCatalog().getNames();
            if (namesDict != null) {
                final PDDestinationNameTreeNode destsTree = namesDict.getDests();
                if (destsTree != null) {
                    pageDest = (PDPageDestination)destsTree.getValue(namedDest.getNamedDestination());
                }
            }
        }
        else if (rawDest instanceof PDPageDestination) {
            pageDest = (PDPageDestination)rawDest;
        }
        else if (rawDest != null) {
            throw new IOException("Error: Unknown destination type " + rawDest);
        }
        if (pageDest != null) {
            page = pageDest.getPage();
            if (page == null) {
                final int pageNumber = pageDest.getPageNumber();
                if (pageNumber != -1) {
                    final List allPages = doc.getDocumentCatalog().getAllPages();
                    page = allPages.get(pageNumber);
                }
            }
        }
        return page;
    }
    
    public PDAction getAction() {
        return PDActionFactory.createAction((COSDictionary)this.node.getDictionaryObject(COSName.A));
    }
    
    public void setAction(final PDAction action) {
        this.node.setItem(COSName.A, action);
    }
    
    public PDStructureElement getStructureElement() {
        PDStructureElement se = null;
        final COSDictionary dic = (COSDictionary)this.node.getDictionaryObject(COSName.SE);
        if (dic != null) {
            se = new PDStructureElement(dic);
        }
        return se;
    }
    
    public void setStructuredElement(final PDStructureElement structureElement) {
        this.node.setItem(COSName.SE, structureElement);
    }
    
    public PDColorState getTextColor() {
        PDColorState retval = null;
        COSArray csValues = (COSArray)this.node.getDictionaryObject(COSName.C);
        if (csValues == null) {
            csValues = new COSArray();
            csValues.growToSize(3, new COSFloat(0.0f));
            this.node.setItem(COSName.C, csValues);
        }
        retval = new PDColorState(csValues);
        retval.setColorSpace(PDDeviceRGB.INSTANCE);
        return retval;
    }
    
    public void setTextColor(final PDColorState textColor) {
        this.node.setItem(COSName.C, textColor.getCOSColorSpaceValue());
    }
    
    public void setTextColor(final Color textColor) {
        final COSArray array = new COSArray();
        array.add(new COSFloat(textColor.getRed() / 255.0f));
        array.add(new COSFloat(textColor.getGreen() / 255.0f));
        array.add(new COSFloat(textColor.getBlue() / 255.0f));
        this.node.setItem(COSName.C, array);
    }
    
    public boolean isItalic() {
        return BitFlagHelper.getFlag(this.node, COSName.F, 1);
    }
    
    public void setItalic(final boolean italic) {
        BitFlagHelper.setFlag(this.node, COSName.F, 1, italic);
    }
    
    public boolean isBold() {
        return BitFlagHelper.getFlag(this.node, COSName.F, 2);
    }
    
    public void setBold(final boolean bold) {
        BitFlagHelper.setFlag(this.node, COSName.F, 2, bold);
    }
}
