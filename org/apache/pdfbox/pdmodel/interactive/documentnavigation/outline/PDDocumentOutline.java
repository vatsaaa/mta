// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.interactive.documentnavigation.outline;

import org.apache.pdfbox.cos.COSDictionary;

public class PDDocumentOutline extends PDOutlineNode
{
    public PDDocumentOutline() {
        this.node.setName("Type", "Outlines");
    }
    
    public PDDocumentOutline(final COSDictionary dic) {
        super(dic);
    }
}
