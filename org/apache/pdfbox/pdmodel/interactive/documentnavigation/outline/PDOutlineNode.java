// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.interactive.documentnavigation.outline;

import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSDictionary;
import org.apache.pdfbox.pdmodel.common.COSObjectable;

public class PDOutlineNode implements COSObjectable
{
    protected COSDictionary node;
    
    public PDOutlineNode() {
        this.node = new COSDictionary();
    }
    
    public PDOutlineNode(final COSDictionary dict) {
        this.node = dict;
    }
    
    public COSBase getCOSObject() {
        return this.node;
    }
    
    public COSDictionary getCOSDictionary() {
        return this.node;
    }
    
    protected PDOutlineNode getParent() {
        PDOutlineNode retval = null;
        final COSDictionary parent = (COSDictionary)this.node.getDictionaryObject("Parent", "P");
        if (parent != null) {
            if (parent.getDictionaryObject("Parent", "P") == null) {
                retval = new PDDocumentOutline(parent);
            }
            else {
                retval = new PDOutlineItem(parent);
            }
        }
        return retval;
    }
    
    protected void setParent(final PDOutlineNode parent) {
        this.node.setItem("Parent", parent);
    }
    
    public void appendChild(final PDOutlineItem outlineNode) {
        outlineNode.setParent(this);
        if (this.getFirstChild() == null) {
            final int currentOpenCount = this.getOpenCount();
            this.setFirstChild(outlineNode);
            int numberOfOpenNodesWeAreAdding = 1;
            if (outlineNode.isNodeOpen()) {
                numberOfOpenNodesWeAreAdding += outlineNode.getOpenCount();
            }
            if (this.isNodeOpen()) {
                this.setOpenCount(currentOpenCount + numberOfOpenNodesWeAreAdding);
            }
            else {
                this.setOpenCount(currentOpenCount - numberOfOpenNodesWeAreAdding);
            }
            this.updateParentOpenCount(numberOfOpenNodesWeAreAdding);
        }
        else {
            final PDOutlineItem previousLastChild = this.getLastChild();
            previousLastChild.insertSiblingAfter(outlineNode);
        }
        PDOutlineItem lastNode;
        for (lastNode = outlineNode; lastNode.getNextSibling() != null; lastNode = lastNode.getNextSibling()) {}
        this.setLastChild(lastNode);
    }
    
    public PDOutlineItem getFirstChild() {
        PDOutlineItem last = null;
        final COSDictionary lastDic = (COSDictionary)this.node.getDictionaryObject("First");
        if (lastDic != null) {
            last = new PDOutlineItem(lastDic);
        }
        return last;
    }
    
    protected void setFirstChild(final PDOutlineNode outlineNode) {
        this.node.setItem("First", outlineNode);
    }
    
    public PDOutlineItem getLastChild() {
        PDOutlineItem last = null;
        final COSDictionary lastDic = (COSDictionary)this.node.getDictionaryObject("Last");
        if (lastDic != null) {
            last = new PDOutlineItem(lastDic);
        }
        return last;
    }
    
    protected void setLastChild(final PDOutlineNode outlineNode) {
        this.node.setItem("Last", outlineNode);
    }
    
    public int getOpenCount() {
        return this.node.getInt("Count", 0);
    }
    
    protected void setOpenCount(final int openCount) {
        this.node.setInt("Count", openCount);
    }
    
    public void openNode() {
        if (!this.isNodeOpen()) {
            int openChildrenCount = 0;
            for (PDOutlineItem currentChild = this.getFirstChild(); currentChild != null; currentChild = currentChild.getNextSibling()) {
                ++openChildrenCount;
                if (currentChild.isNodeOpen()) {
                    openChildrenCount += currentChild.getOpenCount();
                }
            }
            this.setOpenCount(openChildrenCount);
            this.updateParentOpenCount(openChildrenCount);
        }
    }
    
    public void closeNode() {
        if (this.isNodeOpen()) {
            final int openCount = this.getOpenCount();
            this.updateParentOpenCount(-openCount);
            this.setOpenCount(-openCount);
        }
    }
    
    public boolean isNodeOpen() {
        return this.getOpenCount() > 0;
    }
    
    protected void updateParentOpenCount(final int amount) {
        final PDOutlineNode parent = this.getParent();
        if (parent != null) {
            int currentCount = parent.getOpenCount();
            final boolean negative = currentCount < 0 || parent.getCOSDictionary().getDictionaryObject("Count") == null;
            currentCount = Math.abs(currentCount);
            currentCount += amount;
            if (negative) {
                currentCount = -currentCount;
            }
            parent.setOpenCount(currentCount);
            if (!negative) {
                parent.updateParentOpenCount(amount);
            }
        }
    }
}
