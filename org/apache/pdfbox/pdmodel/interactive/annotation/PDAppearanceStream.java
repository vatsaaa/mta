// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.interactive.annotation;

import org.apache.pdfbox.cos.COSDictionary;
import org.apache.pdfbox.pdmodel.PDResources;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.cos.COSArray;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSStream;
import org.apache.pdfbox.pdmodel.common.COSObjectable;

public class PDAppearanceStream implements COSObjectable
{
    private COSStream stream;
    
    public PDAppearanceStream(final COSStream s) {
        this.stream = null;
        this.stream = s;
    }
    
    public COSStream getStream() {
        return this.stream;
    }
    
    public COSBase getCOSObject() {
        return this.stream;
    }
    
    public PDRectangle getBoundingBox() {
        PDRectangle box = null;
        final COSArray bbox = (COSArray)this.stream.getDictionaryObject(COSName.getPDFName("BBox"));
        if (bbox != null) {
            box = new PDRectangle(bbox);
        }
        return box;
    }
    
    public void setBoundingBox(final PDRectangle rectangle) {
        COSArray array = null;
        if (rectangle != null) {
            array = rectangle.getCOSArray();
        }
        this.stream.setItem(COSName.getPDFName("BBox"), array);
    }
    
    public PDResources getResources() {
        PDResources retval = null;
        final COSDictionary dict = (COSDictionary)this.stream.getDictionaryObject(COSName.RESOURCES);
        if (dict != null) {
            retval = new PDResources(dict);
        }
        return retval;
    }
    
    public void setResources(final PDResources resources) {
        COSDictionary dict = null;
        if (resources != null) {
            dict = resources.getCOSDictionary();
        }
        this.stream.setItem(COSName.RESOURCES, dict);
    }
}
