// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.interactive.annotation;

import org.apache.commons.logging.LogFactory;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.graphics.color.PDGamma;
import org.apache.pdfbox.util.BitFlagHelper;
import org.apache.pdfbox.cos.COSArray;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import java.io.IOException;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSDictionary;
import org.apache.commons.logging.Log;
import org.apache.pdfbox.pdmodel.common.COSObjectable;

public abstract class PDAnnotation implements COSObjectable
{
    private static final Log log;
    public static final int FLAG_INVISIBLE = 1;
    public static final int FLAG_HIDDEN = 2;
    public static final int FLAG_PRINTED = 4;
    public static final int FLAG_NO_ZOOM = 8;
    public static final int FLAG_NO_ROTATE = 16;
    public static final int FLAG_NO_VIEW = 32;
    public static final int FLAG_READ_ONLY = 64;
    public static final int FLAG_LOCKED = 128;
    public static final int FLAG_TOGGLE_NO_VIEW = 256;
    private COSDictionary dictionary;
    
    public static PDAnnotation createAnnotation(final COSBase base) throws IOException {
        PDAnnotation annot = null;
        if (base instanceof COSDictionary) {
            final COSDictionary annotDic = (COSDictionary)base;
            final String subtype = annotDic.getNameAsString(COSName.SUBTYPE);
            if ("FileAttachment".equals(subtype)) {
                annot = new PDAnnotationFileAttachment(annotDic);
            }
            else if ("Line".equals(subtype)) {
                annot = new PDAnnotationLine(annotDic);
            }
            else if ("Link".equals(subtype)) {
                annot = new PDAnnotationLink(annotDic);
            }
            else if ("Popup".equals(subtype)) {
                annot = new PDAnnotationPopup(annotDic);
            }
            else if ("Stamp".equals(subtype)) {
                annot = new PDAnnotationRubberStamp(annotDic);
            }
            else if ("Square".equals(subtype) || "Circle".equals(subtype)) {
                annot = new PDAnnotationSquareCircle(annotDic);
            }
            else if ("Text".equals(subtype)) {
                annot = new PDAnnotationText(annotDic);
            }
            else if ("Highlight".equals(subtype) || "Underline".equals(subtype) || "Squiggly".equals(subtype) || "StrikeOut".equals(subtype)) {
                annot = new PDAnnotationTextMarkup(annotDic);
            }
            else if ("Link".equals(subtype)) {
                annot = new PDAnnotationLink(annotDic);
            }
            else if ("Widget".equals(subtype)) {
                annot = new PDAnnotationWidget(annotDic);
            }
            else if ("FreeText".equals(subtype) || "Polygon".equals(subtype) || "PolyLine".equals(subtype) || "Caret".equals(subtype) || "Ink".equals(subtype) || "Sound".equals(subtype)) {
                annot = new PDAnnotationMarkup(annotDic);
            }
            else {
                annot = new PDAnnotationUnknown(annotDic);
                PDAnnotation.log.debug("Unknown or unsupported annotation subtype " + subtype);
            }
            return annot;
        }
        throw new IOException("Error: Unknown annotation type " + base);
    }
    
    public PDAnnotation() {
        (this.dictionary = new COSDictionary()).setItem(COSName.TYPE, COSName.ANNOT);
    }
    
    public PDAnnotation(final COSDictionary dict) {
        this.dictionary = dict;
    }
    
    public COSDictionary getDictionary() {
        return this.dictionary;
    }
    
    public PDRectangle getRectangle() {
        final COSArray rectArray = (COSArray)this.dictionary.getDictionaryObject(COSName.RECT);
        PDRectangle rectangle = null;
        if (rectArray != null) {
            rectangle = new PDRectangle(rectArray);
        }
        return rectangle;
    }
    
    public void setRectangle(final PDRectangle rectangle) {
        this.dictionary.setItem(COSName.RECT, rectangle.getCOSArray());
    }
    
    public int getAnnotationFlags() {
        return this.getDictionary().getInt(COSName.F, 0);
    }
    
    public void setAnnotationFlags(final int flags) {
        this.getDictionary().setInt(COSName.F, flags);
    }
    
    public COSBase getCOSObject() {
        return this.getDictionary();
    }
    
    public String getAppearanceStream() {
        String retval = null;
        final COSName name = (COSName)this.getDictionary().getDictionaryObject(COSName.AS);
        if (name != null) {
            retval = name.getName();
        }
        return retval;
    }
    
    public void setAppearanceStream(final String as) {
        if (as == null) {
            this.getDictionary().removeItem(COSName.AS);
        }
        else {
            this.getDictionary().setItem(COSName.AS, COSName.getPDFName(as));
        }
    }
    
    public PDAppearanceDictionary getAppearance() {
        PDAppearanceDictionary ap = null;
        final COSDictionary apDic = (COSDictionary)this.dictionary.getDictionaryObject(COSName.AP);
        if (apDic != null) {
            ap = new PDAppearanceDictionary(apDic);
        }
        return ap;
    }
    
    public void setAppearance(final PDAppearanceDictionary appearance) {
        COSDictionary ap = null;
        if (appearance != null) {
            ap = appearance.getDictionary();
        }
        this.dictionary.setItem(COSName.AP, ap);
    }
    
    public boolean isInvisible() {
        return BitFlagHelper.getFlag(this.getDictionary(), COSName.F, 1);
    }
    
    public void setInvisible(final boolean invisible) {
        BitFlagHelper.setFlag(this.getDictionary(), COSName.F, 1, invisible);
    }
    
    public boolean isHidden() {
        return BitFlagHelper.getFlag(this.getDictionary(), COSName.F, 2);
    }
    
    public void setHidden(final boolean hidden) {
        BitFlagHelper.setFlag(this.getDictionary(), COSName.F, 2, hidden);
    }
    
    public boolean isPrinted() {
        return BitFlagHelper.getFlag(this.getDictionary(), COSName.F, 4);
    }
    
    public void setPrinted(final boolean printed) {
        BitFlagHelper.setFlag(this.getDictionary(), COSName.F, 4, printed);
    }
    
    public boolean isNoZoom() {
        return BitFlagHelper.getFlag(this.getDictionary(), COSName.F, 8);
    }
    
    public void setNoZoom(final boolean noZoom) {
        BitFlagHelper.setFlag(this.getDictionary(), COSName.F, 8, noZoom);
    }
    
    public boolean isNoRotate() {
        return BitFlagHelper.getFlag(this.getDictionary(), COSName.F, 16);
    }
    
    public void setNoRotate(final boolean noRotate) {
        BitFlagHelper.setFlag(this.getDictionary(), COSName.F, 16, noRotate);
    }
    
    public boolean isNoView() {
        return BitFlagHelper.getFlag(this.getDictionary(), COSName.F, 32);
    }
    
    public void setNoView(final boolean noView) {
        BitFlagHelper.setFlag(this.getDictionary(), COSName.F, 32, noView);
    }
    
    public boolean isReadOnly() {
        return BitFlagHelper.getFlag(this.getDictionary(), COSName.F, 64);
    }
    
    public void setReadOnly(final boolean readOnly) {
        BitFlagHelper.setFlag(this.getDictionary(), COSName.F, 64, readOnly);
    }
    
    public boolean isLocked() {
        return BitFlagHelper.getFlag(this.getDictionary(), COSName.F, 128);
    }
    
    public void setLocked(final boolean locked) {
        BitFlagHelper.setFlag(this.getDictionary(), COSName.F, 128, locked);
    }
    
    public boolean isToggleNoView() {
        return BitFlagHelper.getFlag(this.getDictionary(), COSName.F, 256);
    }
    
    public void setToggleNoView(final boolean toggleNoView) {
        BitFlagHelper.setFlag(this.getDictionary(), COSName.F, 256, toggleNoView);
    }
    
    public String getContents() {
        return this.dictionary.getString(COSName.CONTENTS);
    }
    
    public void setContents(final String value) {
        this.dictionary.setString(COSName.CONTENTS, value);
    }
    
    public String getModifiedDate() {
        return this.getDictionary().getString(COSName.M);
    }
    
    public void setModifiedDate(final String m) {
        this.getDictionary().setString(COSName.M, m);
    }
    
    public String getAnnotationName() {
        return this.getDictionary().getString(COSName.NM);
    }
    
    public void setAnnotationName(final String nm) {
        this.getDictionary().setString(COSName.NM, nm);
    }
    
    public void setColour(final PDGamma c) {
        this.getDictionary().setItem(COSName.C, c);
    }
    
    public PDGamma getColour() {
        final COSArray c = (COSArray)this.getDictionary().getItem(COSName.C);
        if (c != null) {
            return new PDGamma(c);
        }
        return null;
    }
    
    public String getSubtype() {
        return this.getDictionary().getNameAsString(COSName.SUBTYPE);
    }
    
    public void setPage(final PDPage page) {
        this.getDictionary().setItem(COSName.P, page);
    }
    
    public PDPage getPage() {
        final COSDictionary p = (COSDictionary)this.getDictionary().getDictionaryObject(COSName.P);
        if (p != null) {
            return new PDPage(p);
        }
        return null;
    }
    
    static {
        log = LogFactory.getLog(PDAnnotation.class);
    }
}
