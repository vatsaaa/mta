// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.interactive.annotation;

import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.cos.COSDictionary;
import org.apache.pdfbox.pdmodel.common.COSObjectable;

public class PDExternalDataDictionary implements COSObjectable
{
    private COSDictionary dataDictionary;
    
    public PDExternalDataDictionary() {
        (this.dataDictionary = new COSDictionary()).setName(COSName.TYPE, "ExData");
    }
    
    public PDExternalDataDictionary(final COSDictionary dictionary) {
        this.dataDictionary = dictionary;
    }
    
    public COSBase getCOSObject() {
        return this.dataDictionary;
    }
    
    public COSDictionary getDictionary() {
        return this.dataDictionary;
    }
    
    public String getType() {
        return this.getDictionary().getNameAsString(COSName.TYPE, "ExData");
    }
    
    public String getSubtype() {
        return this.getDictionary().getNameAsString(COSName.SUBTYPE);
    }
    
    public void setSubtype(final String subtype) {
        this.getDictionary().setName(COSName.SUBTYPE, subtype);
    }
}
