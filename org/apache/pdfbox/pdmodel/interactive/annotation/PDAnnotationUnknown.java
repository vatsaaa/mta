// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.interactive.annotation;

import org.apache.pdfbox.cos.COSDictionary;

public class PDAnnotationUnknown extends PDAnnotation
{
    public PDAnnotationUnknown(final COSDictionary dic) {
        super(dic);
    }
}
