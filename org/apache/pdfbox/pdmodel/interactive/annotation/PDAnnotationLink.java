// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.interactive.annotation;

import org.apache.pdfbox.cos.COSArray;
import org.apache.pdfbox.pdmodel.interactive.action.type.PDActionURI;
import java.io.IOException;
import org.apache.pdfbox.pdmodel.interactive.documentnavigation.destination.PDDestination;
import org.apache.pdfbox.pdmodel.common.COSObjectable;
import org.apache.pdfbox.pdmodel.interactive.action.PDActionFactory;
import org.apache.pdfbox.pdmodel.interactive.action.type.PDAction;
import org.apache.pdfbox.cos.COSDictionary;
import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSName;

public class PDAnnotationLink extends PDAnnotation
{
    public static final String HIGHLIGHT_MODE_NONE = "N";
    public static final String HIGHLIGHT_MODE_INVERT = "I";
    public static final String HIGHLIGHT_MODE_OUTLINE = "O";
    public static final String HIGHLIGHT_MODE_PUSH = "P";
    public static final String SUB_TYPE = "Link";
    
    public PDAnnotationLink() {
        this.getDictionary().setItem(COSName.SUBTYPE, COSName.getPDFName("Link"));
    }
    
    public PDAnnotationLink(final COSDictionary field) {
        super(field);
    }
    
    public PDAction getAction() {
        final COSDictionary action = (COSDictionary)this.getDictionary().getDictionaryObject(COSName.A);
        return PDActionFactory.createAction(action);
    }
    
    public void setAction(final PDAction action) {
        this.getDictionary().setItem(COSName.A, action);
    }
    
    public void setBorderStyle(final PDBorderStyleDictionary bs) {
        this.getDictionary().setItem("BS", bs);
    }
    
    public PDBorderStyleDictionary getBorderStyle() {
        final COSDictionary bs = (COSDictionary)this.getDictionary().getItem(COSName.getPDFName("BS"));
        if (bs != null) {
            return new PDBorderStyleDictionary(bs);
        }
        return null;
    }
    
    public PDDestination getDestination() throws IOException {
        final COSBase base = this.getDictionary().getDictionaryObject(COSName.DEST);
        final PDDestination retval = PDDestination.create(base);
        return retval;
    }
    
    public void setDestination(final PDDestination dest) {
        this.getDictionary().setItem(COSName.DEST, dest);
    }
    
    public String getHighlightMode() {
        return this.getDictionary().getNameAsString(COSName.H, "I");
    }
    
    public void setHighlightMode(final String mode) {
        this.getDictionary().setName(COSName.H, mode);
    }
    
    public void setPreviousURI(final PDActionURI pa) {
        this.getDictionary().setItem("PA", pa);
    }
    
    public PDActionURI getPreviousURI() {
        final COSDictionary pa = (COSDictionary)this.getDictionary().getDictionaryObject("PA");
        if (pa != null) {
            return new PDActionURI(pa);
        }
        return null;
    }
    
    public void setQuadPoints(final float[] quadPoints) {
        final COSArray newQuadPoints = new COSArray();
        newQuadPoints.setFloatArray(quadPoints);
        this.getDictionary().setItem("QuadPoints", newQuadPoints);
    }
    
    public float[] getQuadPoints() {
        final COSArray quadPoints = (COSArray)this.getDictionary().getDictionaryObject("QuadPoints");
        if (quadPoints != null) {
            return quadPoints.toFloatArray();
        }
        return null;
    }
}
