// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.interactive.annotation;

import java.io.IOException;
import java.util.Calendar;
import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.pdmodel.common.PDTextStream;
import org.apache.pdfbox.pdmodel.common.COSObjectable;
import org.apache.pdfbox.cos.COSDictionary;

public class PDAnnotationMarkup extends PDAnnotation
{
    public static final String SUB_TYPE_FREETEXT = "FreeText";
    public static final String SUB_TYPE_POLYGON = "Polygon";
    public static final String SUB_TYPE_POLYLINE = "PolyLine";
    public static final String SUB_TYPE_CARET = "Caret";
    public static final String SUB_TYPE_INK = "Ink";
    public static final String SUB_TYPE_SOUND = "Sound";
    public static final String RT_REPLY = "R";
    public static final String RT_GROUP = "Group";
    
    public PDAnnotationMarkup() {
    }
    
    public PDAnnotationMarkup(final COSDictionary dict) {
        super(dict);
    }
    
    public String getTitlePopup() {
        return this.getDictionary().getString("T");
    }
    
    public void setTitlePopup(final String t) {
        this.getDictionary().setString("T", t);
    }
    
    public PDAnnotationPopup getPopup() {
        final COSDictionary popup = (COSDictionary)this.getDictionary().getDictionaryObject("Popup");
        if (popup != null) {
            return new PDAnnotationPopup(popup);
        }
        return null;
    }
    
    public void setPopup(final PDAnnotationPopup popup) {
        this.getDictionary().setItem("Popup", popup);
    }
    
    public float getConstantOpacity() {
        return this.getDictionary().getFloat("CA", 1.0f);
    }
    
    public void setConstantOpacity(final float ca) {
        this.getDictionary().setFloat("CA", ca);
    }
    
    public PDTextStream getRichContents() {
        final COSBase rc = this.getDictionary().getDictionaryObject("RC");
        if (rc != null) {
            return PDTextStream.createTextStream(rc);
        }
        return null;
    }
    
    public void setRichContents(final PDTextStream rc) {
        this.getDictionary().setItem("RC", rc);
    }
    
    public Calendar getCreationDate() throws IOException {
        return this.getDictionary().getDate("CreationDate");
    }
    
    public void setCreationDate(final Calendar creationDate) {
        this.getDictionary().setDate("CreationDate", creationDate);
    }
    
    public PDAnnotation getInReplyTo() throws IOException {
        final COSBase irt = this.getDictionary().getDictionaryObject("IRT");
        return PDAnnotation.createAnnotation(irt);
    }
    
    public void setInReplyTo(final PDAnnotation irt) {
        this.getDictionary().setItem("IRT", irt);
    }
    
    public String getSubject() {
        return this.getDictionary().getString("Subj");
    }
    
    public void setSubject(final String subj) {
        this.getDictionary().setString("Subj", subj);
    }
    
    public String getReplyType() {
        return this.getDictionary().getNameAsString("RT", "R");
    }
    
    public void setReplyType(final String rt) {
        this.getDictionary().setName("RT", rt);
    }
    
    public String getIntent() {
        return this.getDictionary().getNameAsString("IT");
    }
    
    public void setIntent(final String it) {
        this.getDictionary().setName("IT", it);
    }
    
    public PDExternalDataDictionary getExternalData() {
        final COSBase exData = this.getDictionary().getDictionaryObject("ExData");
        if (exData instanceof COSDictionary) {
            return new PDExternalDataDictionary((COSDictionary)exData);
        }
        return null;
    }
    
    public void setExternalData(final PDExternalDataDictionary externalData) {
        this.getDictionary().setItem("ExData", externalData);
    }
}
