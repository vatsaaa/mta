// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.interactive.annotation;

import org.apache.pdfbox.cos.COSFloat;
import org.apache.pdfbox.pdmodel.common.COSObjectable;
import org.apache.pdfbox.pdmodel.graphics.color.PDGamma;
import org.apache.pdfbox.cos.COSArray;
import org.apache.pdfbox.cos.COSDictionary;
import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSName;

public class PDAnnotationLine extends PDAnnotationMarkup
{
    public static final String IT_LINE_ARROW = "LineArrow";
    public static final String IT_LINE_DIMENSION = "LineDimension";
    public static final String LE_SQUARE = "Square";
    public static final String LE_CIRCLE = "Circle";
    public static final String LE_DIAMOND = "Diamond";
    public static final String LE_OPEN_ARROW = "OpenArrow";
    public static final String LE_CLOSED_ARROW = "ClosedArrow";
    public static final String LE_NONE = "None";
    public static final String LE_BUTT = "Butt";
    public static final String LE_R_OPEN_ARROW = "ROpenArrow";
    public static final String LE_R_CLOSED_ARROW = "RClosedArrow";
    public static final String LE_SLASH = "Slash";
    public static final String SUB_TYPE = "Line";
    
    public PDAnnotationLine() {
        this.getDictionary().setItem(COSName.SUBTYPE, COSName.getPDFName("Line"));
        this.setLine(new float[] { 0.0f, 0.0f, 0.0f, 0.0f });
    }
    
    public PDAnnotationLine(final COSDictionary field) {
        super(field);
    }
    
    public void setLine(final float[] l) {
        final COSArray newL = new COSArray();
        newL.setFloatArray(l);
        this.getDictionary().setItem("L", newL);
    }
    
    public float[] getLine() {
        final COSArray l = (COSArray)this.getDictionary().getDictionaryObject("L");
        return l.toFloatArray();
    }
    
    public void setStartPointEndingStyle(String style) {
        if (style == null) {
            style = "None";
        }
        COSArray array = (COSArray)this.getDictionary().getDictionaryObject("LE");
        if (array == null) {
            array = new COSArray();
            array.add(COSName.getPDFName(style));
            array.add(COSName.getPDFName("None"));
            this.getDictionary().setItem("LE", array);
        }
        else {
            array.setName(0, style);
        }
    }
    
    public String getStartPointEndingStyle() {
        String retval = "None";
        final COSArray array = (COSArray)this.getDictionary().getDictionaryObject("LE");
        if (array != null) {
            retval = array.getName(0);
        }
        return retval;
    }
    
    public void setEndPointEndingStyle(String style) {
        if (style == null) {
            style = "None";
        }
        COSArray array = (COSArray)this.getDictionary().getDictionaryObject("LE");
        if (array == null) {
            array = new COSArray();
            array.add(COSName.getPDFName("None"));
            array.add(COSName.getPDFName(style));
            this.getDictionary().setItem("LE", array);
        }
        else {
            array.setName(1, style);
        }
    }
    
    public String getEndPointEndingStyle() {
        String retval = "None";
        final COSArray array = (COSArray)this.getDictionary().getDictionaryObject("LE");
        if (array != null) {
            retval = array.getName(1);
        }
        return retval;
    }
    
    public void setInteriorColour(final PDGamma ic) {
        this.getDictionary().setItem("IC", ic);
    }
    
    public PDGamma getInteriorColour() {
        final COSArray ic = (COSArray)this.getDictionary().getDictionaryObject("IC");
        if (ic != null) {
            return new PDGamma(ic);
        }
        return null;
    }
    
    public void setCaption(final boolean cap) {
        this.getDictionary().setBoolean("Cap", cap);
    }
    
    public boolean getCaption() {
        return this.getDictionary().getBoolean("Cap", false);
    }
    
    public void setBorderStyle(final PDBorderStyleDictionary bs) {
        this.getDictionary().setItem("BS", bs);
    }
    
    public PDBorderStyleDictionary getBorderStyle() {
        final COSDictionary bs = (COSDictionary)this.getDictionary().getItem(COSName.getPDFName("BS"));
        if (bs != null) {
            return new PDBorderStyleDictionary(bs);
        }
        return null;
    }
    
    public float getLeaderLineLength() {
        return this.getDictionary().getFloat("LL");
    }
    
    public void setLeaderLineLength(final float leaderLineLength) {
        this.getDictionary().setFloat("LL", leaderLineLength);
    }
    
    public float getLeaderLineExtensionLength() {
        return this.getDictionary().getFloat("LLE");
    }
    
    public void setLeaderLineExtensionLength(final float leaderLineExtensionLength) {
        this.getDictionary().setFloat("LLE", leaderLineExtensionLength);
    }
    
    public float getLeaderLineOffsetLength() {
        return this.getDictionary().getFloat("LLO");
    }
    
    public void setLeaderLineOffsetLength(final float leaderLineOffsetLength) {
        this.getDictionary().setFloat("LLO", leaderLineOffsetLength);
    }
    
    public String getCaptionPositioning() {
        return this.getDictionary().getString("CP");
    }
    
    public void setCaptionPositioning(final String captionPositioning) {
        this.getDictionary().setString("CP", captionPositioning);
    }
    
    public void setCaptionHorizontalOffset(final float offset) {
        COSArray array = (COSArray)this.getDictionary().getDictionaryObject("CO");
        if (array == null) {
            array = new COSArray();
            array.setFloatArray(new float[] { offset, 0.0f });
            this.getDictionary().setItem("CO", array);
        }
        else {
            array.set(0, new COSFloat(offset));
        }
    }
    
    public float getCaptionHorizontalOffset() {
        float retval = 0.0f;
        final COSArray array = (COSArray)this.getDictionary().getDictionaryObject("CO");
        if (array != null) {
            retval = array.toFloatArray()[0];
        }
        return retval;
    }
    
    public void setCaptionVerticalOffset(final float offset) {
        COSArray array = (COSArray)this.getDictionary().getDictionaryObject("CO");
        if (array == null) {
            array = new COSArray();
            array.setFloatArray(new float[] { 0.0f, offset });
            this.getDictionary().setItem("CO", array);
        }
        else {
            array.set(1, new COSFloat(offset));
        }
    }
    
    public float getCaptionVerticalOffset() {
        float retval = 0.0f;
        final COSArray array = (COSArray)this.getDictionary().getDictionaryObject("CO");
        if (array != null) {
            retval = array.toFloatArray()[1];
        }
        return retval;
    }
}
