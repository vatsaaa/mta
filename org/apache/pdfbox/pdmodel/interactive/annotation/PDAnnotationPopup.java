// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.interactive.annotation;

import java.io.IOException;
import org.apache.pdfbox.cos.COSDictionary;
import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSName;

public class PDAnnotationPopup extends PDAnnotation
{
    public static final String SUB_TYPE = "Popup";
    
    public PDAnnotationPopup() {
        this.getDictionary().setItem(COSName.SUBTYPE, COSName.getPDFName("Popup"));
    }
    
    public PDAnnotationPopup(final COSDictionary field) {
        super(field);
    }
    
    public void setOpen(final boolean open) {
        this.getDictionary().setBoolean("Open", open);
    }
    
    public boolean getOpen() {
        return this.getDictionary().getBoolean("Open", false);
    }
    
    public void setParent(final PDAnnotationMarkup annot) {
        this.getDictionary().setItem(COSName.PARENT, annot.getDictionary());
    }
    
    public PDAnnotationMarkup getParent() {
        PDAnnotationMarkup am = null;
        try {
            am = (PDAnnotationMarkup)PDAnnotation.createAnnotation(this.getDictionary().getDictionaryObject("Parent", "P"));
        }
        catch (IOException ex) {}
        return am;
    }
}
