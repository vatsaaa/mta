// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.interactive.annotation;

import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.cos.COSArray;
import org.apache.pdfbox.pdmodel.common.COSObjectable;
import org.apache.pdfbox.pdmodel.graphics.color.PDGamma;
import org.apache.pdfbox.cos.COSDictionary;

public class PDAnnotationSquareCircle extends PDAnnotationMarkup
{
    public static final String SUB_TYPE_SQUARE = "Square";
    public static final String SUB_TYPE_CIRCLE = "Circle";
    
    public PDAnnotationSquareCircle(final String subType) {
        this.setSubtype(subType);
    }
    
    public PDAnnotationSquareCircle(final COSDictionary field) {
        super(field);
    }
    
    public void setInteriorColour(final PDGamma ic) {
        this.getDictionary().setItem("IC", ic);
    }
    
    public PDGamma getInteriorColour() {
        final COSArray ic = (COSArray)this.getDictionary().getItem(COSName.getPDFName("IC"));
        if (ic != null) {
            return new PDGamma(ic);
        }
        return null;
    }
    
    public void setBorderEffect(final PDBorderEffectDictionary be) {
        this.getDictionary().setItem("BE", be);
    }
    
    public PDBorderEffectDictionary getBorderEffect() {
        final COSDictionary be = (COSDictionary)this.getDictionary().getDictionaryObject("BE");
        if (be != null) {
            return new PDBorderEffectDictionary(be);
        }
        return null;
    }
    
    public void setRectDifference(final PDRectangle rd) {
        this.getDictionary().setItem("RD", rd);
    }
    
    public PDRectangle getRectDifference() {
        final COSArray rd = (COSArray)this.getDictionary().getDictionaryObject("RD");
        if (rd != null) {
            return new PDRectangle(rd);
        }
        return null;
    }
    
    public void setSubtype(final String subType) {
        this.getDictionary().setName(COSName.SUBTYPE, subType);
    }
    
    @Override
    public String getSubtype() {
        return this.getDictionary().getNameAsString(COSName.SUBTYPE);
    }
    
    public void setBorderStyle(final PDBorderStyleDictionary bs) {
        this.getDictionary().setItem("BS", bs);
    }
    
    public PDBorderStyleDictionary getBorderStyle() {
        final COSDictionary bs = (COSDictionary)this.getDictionary().getItem(COSName.getPDFName("BS"));
        if (bs != null) {
            return new PDBorderStyleDictionary(bs);
        }
        return null;
    }
}
