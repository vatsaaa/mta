// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.interactive.annotation;

import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSDictionary;
import org.apache.pdfbox.pdmodel.common.COSObjectable;

public class PDBorderEffectDictionary implements COSObjectable
{
    public static final String STYLE_SOLID = "S";
    public static final String STYLE_CLOUDY = "C";
    private COSDictionary dictionary;
    
    public PDBorderEffectDictionary() {
        this.dictionary = new COSDictionary();
    }
    
    public PDBorderEffectDictionary(final COSDictionary dict) {
        this.dictionary = dict;
    }
    
    public COSDictionary getDictionary() {
        return this.dictionary;
    }
    
    public COSBase getCOSObject() {
        return this.dictionary;
    }
    
    public void setIntensity(final float i) {
        this.getDictionary().setFloat("I", i);
    }
    
    public float getIntensity() {
        return this.getDictionary().getFloat("I", 0.0f);
    }
    
    public void setStyle(final String s) {
        this.getDictionary().setName("S", s);
    }
    
    public String getStyle() {
        return this.getDictionary().getNameAsString("S", "S");
    }
}
