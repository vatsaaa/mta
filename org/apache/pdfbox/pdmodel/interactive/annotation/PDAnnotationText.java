// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.interactive.annotation;

import org.apache.pdfbox.cos.COSDictionary;
import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSName;

public class PDAnnotationText extends PDAnnotationMarkup
{
    public static final String NAME_COMMENT = "Comment";
    public static final String NAME_KEY = "Key";
    public static final String NAME_NOTE = "Note";
    public static final String NAME_HELP = "Help";
    public static final String NAME_NEW_PARAGRAPH = "NewParagraph";
    public static final String NAME_PARAGRAPH = "Paragraph";
    public static final String NAME_INSERT = "Insert";
    public static final String SUB_TYPE = "Text";
    
    public PDAnnotationText() {
        this.getDictionary().setItem(COSName.SUBTYPE, COSName.getPDFName("Text"));
    }
    
    public PDAnnotationText(final COSDictionary field) {
        super(field);
    }
    
    public void setOpen(final boolean open) {
        this.getDictionary().setBoolean(COSName.getPDFName("Open"), open);
    }
    
    public boolean getOpen() {
        return this.getDictionary().getBoolean(COSName.getPDFName("Open"), false);
    }
    
    public void setName(final String name) {
        this.getDictionary().setName(COSName.NAME, name);
    }
    
    public String getName() {
        return this.getDictionary().getNameAsString(COSName.NAME, "Note");
    }
    
    public String getState() {
        return this.getDictionary().getString("State");
    }
    
    public void setState(final String state) {
        this.getDictionary().setString("State", state);
    }
    
    public String getStateModel() {
        return this.getDictionary().getString("StateModel");
    }
    
    public void setStateModel(final String stateModel) {
        this.getDictionary().setString("StateModel", stateModel);
    }
}
