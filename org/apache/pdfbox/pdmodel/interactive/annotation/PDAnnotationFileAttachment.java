// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.interactive.annotation;

import org.apache.pdfbox.pdmodel.common.COSObjectable;
import java.io.IOException;
import org.apache.pdfbox.pdmodel.common.filespecification.PDFileSpecification;
import org.apache.pdfbox.cos.COSDictionary;
import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSName;

public class PDAnnotationFileAttachment extends PDAnnotationMarkup
{
    public static final String ATTACHMENT_NAME_PUSH_PIN = "PushPin";
    public static final String ATTACHMENT_NAME_GRAPH = "Graph";
    public static final String ATTACHMENT_NAME_PAPERCLIP = "Paperclip";
    public static final String ATTACHMENT_NAME_TAG = "Tag";
    public static final String SUB_TYPE = "FileAttachment";
    
    public PDAnnotationFileAttachment() {
        this.getDictionary().setItem(COSName.SUBTYPE, COSName.getPDFName("FileAttachment"));
    }
    
    public PDAnnotationFileAttachment(final COSDictionary field) {
        super(field);
    }
    
    public PDFileSpecification getFile() throws IOException {
        return PDFileSpecification.createFS(this.getDictionary().getDictionaryObject("FS"));
    }
    
    public void setFile(final PDFileSpecification file) {
        this.getDictionary().setItem("FS", file);
    }
    
    public String getAttachmentName() {
        return this.getDictionary().getNameAsString("Name", "PushPin");
    }
    
    public void setAttachementName(final String name) {
        this.getDictionary().setName("Name", name);
    }
}
