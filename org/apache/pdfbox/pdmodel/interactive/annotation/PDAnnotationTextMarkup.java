// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.interactive.annotation;

import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSArray;
import org.apache.pdfbox.cos.COSDictionary;

public class PDAnnotationTextMarkup extends PDAnnotationMarkup
{
    public static final String SUB_TYPE_HIGHLIGHT = "Highlight";
    public static final String SUB_TYPE_UNDERLINE = "Underline";
    public static final String SUB_TYPE_SQUIGGLY = "Squiggly";
    public static final String SUB_TYPE_STRIKEOUT = "StrikeOut";
    
    private PDAnnotationTextMarkup() {
    }
    
    public PDAnnotationTextMarkup(final String subType) {
        this.setSubtype(subType);
        this.setQuadPoints(new float[0]);
    }
    
    public PDAnnotationTextMarkup(final COSDictionary field) {
        super(field);
    }
    
    public void setQuadPoints(final float[] quadPoints) {
        final COSArray newQuadPoints = new COSArray();
        newQuadPoints.setFloatArray(quadPoints);
        this.getDictionary().setItem("QuadPoints", newQuadPoints);
    }
    
    public float[] getQuadPoints() {
        final COSArray quadPoints = (COSArray)this.getDictionary().getDictionaryObject("QuadPoints");
        if (quadPoints != null) {
            return quadPoints.toFloatArray();
        }
        return null;
    }
    
    public void setSubtype(final String subType) {
        this.getDictionary().setName(COSName.SUBTYPE, subType);
    }
    
    @Override
    public String getSubtype() {
        return this.getDictionary().getNameAsString(COSName.SUBTYPE);
    }
}
