// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.interactive.annotation;

import java.util.Iterator;
import org.apache.pdfbox.pdmodel.common.COSDictionaryMap;
import java.util.HashMap;
import org.apache.pdfbox.cos.COSStream;
import java.util.Map;
import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.cos.COSDictionary;
import org.apache.pdfbox.pdmodel.common.COSObjectable;

public class PDAppearanceDictionary implements COSObjectable
{
    private COSDictionary dictionary;
    
    public PDAppearanceDictionary() {
        (this.dictionary = new COSDictionary()).setItem(COSName.N, new COSDictionary());
    }
    
    public PDAppearanceDictionary(final COSDictionary dict) {
        this.dictionary = dict;
    }
    
    public COSDictionary getDictionary() {
        return this.dictionary;
    }
    
    public COSBase getCOSObject() {
        return this.dictionary;
    }
    
    public Map<String, PDAppearanceStream> getNormalAppearance() {
        COSBase ap = this.dictionary.getDictionaryObject(COSName.N);
        if (ap == null) {
            return null;
        }
        if (ap instanceof COSStream) {
            final COSStream aux = (COSStream)ap;
            ap = new COSDictionary();
            ((COSDictionary)ap).setItem(COSName.getPDFName("default"), aux);
        }
        final COSDictionary map = (COSDictionary)ap;
        final Map<String, PDAppearanceStream> actuals = new HashMap<String, PDAppearanceStream>();
        final Map retval = new COSDictionaryMap(actuals, map);
        for (final COSName asName : map.keySet()) {
            final COSStream as = (COSStream)map.getDictionaryObject(asName);
            actuals.put(asName.getName(), new PDAppearanceStream(as));
        }
        return (Map<String, PDAppearanceStream>)retval;
    }
    
    public void setNormalAppearance(final Map<String, PDAppearanceStream> appearanceMap) {
        this.dictionary.setItem(COSName.N, COSDictionaryMap.convert(appearanceMap));
    }
    
    public void setNormalAppearance(final PDAppearanceStream ap) {
        this.dictionary.setItem(COSName.N, ap.getStream());
    }
    
    public Map<String, PDAppearanceStream> getRolloverAppearance() {
        Map<String, PDAppearanceStream> retval = null;
        COSBase ap = this.dictionary.getDictionaryObject(COSName.R);
        if (ap == null) {
            retval = this.getNormalAppearance();
        }
        else {
            if (ap instanceof COSStream) {
                final COSStream aux = (COSStream)ap;
                ap = new COSDictionary();
                ((COSDictionary)ap).setItem(COSName.getPDFName("default"), aux);
            }
            final COSDictionary map = (COSDictionary)ap;
            final Map<String, PDAppearanceStream> actuals = new HashMap<String, PDAppearanceStream>();
            retval = (Map<String, PDAppearanceStream>)new COSDictionaryMap(actuals, map);
            for (final COSName asName : map.keySet()) {
                final COSStream as = (COSStream)map.getDictionaryObject(asName);
                actuals.put(asName.getName(), new PDAppearanceStream(as));
            }
        }
        return retval;
    }
    
    public void setRolloverAppearance(final Map<String, PDAppearanceStream> appearanceMap) {
        this.dictionary.setItem(COSName.R, COSDictionaryMap.convert(appearanceMap));
    }
    
    public void setRolloverAppearance(final PDAppearanceStream ap) {
        this.dictionary.setItem(COSName.R, ap.getStream());
    }
    
    public Map<String, PDAppearanceStream> getDownAppearance() {
        Map<String, PDAppearanceStream> retval = null;
        COSBase ap = this.dictionary.getDictionaryObject(COSName.D);
        if (ap == null) {
            retval = this.getNormalAppearance();
        }
        else {
            if (ap instanceof COSStream) {
                final COSStream aux = (COSStream)ap;
                ap = new COSDictionary();
                ((COSDictionary)ap).setItem(COSName.getPDFName("default"), aux);
            }
            final COSDictionary map = (COSDictionary)ap;
            final Map<String, PDAppearanceStream> actuals = new HashMap<String, PDAppearanceStream>();
            retval = (Map<String, PDAppearanceStream>)new COSDictionaryMap(actuals, map);
            for (final COSName asName : map.keySet()) {
                final COSStream as = (COSStream)map.getDictionaryObject(asName);
                actuals.put(asName.getName(), new PDAppearanceStream(as));
            }
        }
        return retval;
    }
    
    public void setDownAppearance(final Map<String, PDAppearanceStream> appearanceMap) {
        this.dictionary.setItem(COSName.D, COSDictionaryMap.convert(appearanceMap));
    }
    
    public void setDownAppearance(final PDAppearanceStream ap) {
        this.dictionary.setItem(COSName.D, ap.getStream());
    }
}
