// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.interactive.annotation;

import org.apache.pdfbox.pdmodel.interactive.action.PDAnnotationAdditionalActions;
import org.apache.pdfbox.pdmodel.interactive.action.PDActionFactory;
import org.apache.pdfbox.pdmodel.interactive.action.type.PDAction;
import org.apache.pdfbox.pdmodel.common.COSObjectable;
import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSDictionary;
import org.apache.pdfbox.cos.COSName;

public class PDAnnotationWidget extends PDAnnotation
{
    public static final String SUB_TYPE = "Widget";
    
    public PDAnnotationWidget() {
        this.getDictionary().setName(COSName.SUBTYPE, "Widget");
    }
    
    public PDAnnotationWidget(final COSDictionary field) {
        super(field);
    }
    
    public String getHighlightingMode() {
        return this.getDictionary().getNameAsString(COSName.H, "I");
    }
    
    public void setHighlightingMode(final String highlightingMode) {
        if (highlightingMode == null || "N".equals(highlightingMode) || "I".equals(highlightingMode) || "O".equals(highlightingMode) || "P".equals(highlightingMode) || "T".equals(highlightingMode)) {
            this.getDictionary().setName(COSName.H, highlightingMode);
            return;
        }
        throw new IllegalArgumentException("Valid values for highlighting mode are 'N', 'N', 'O', 'P' or 'T'");
    }
    
    public PDAppearanceCharacteristicsDictionary getAppearanceCharacteristics() {
        final COSBase mk = this.getDictionary().getDictionaryObject(COSName.getPDFName("MK"));
        if (mk instanceof COSDictionary) {
            return new PDAppearanceCharacteristicsDictionary((COSDictionary)mk);
        }
        return null;
    }
    
    public void setAppearanceCharacteristics(final PDAppearanceCharacteristicsDictionary appearanceCharacteristics) {
        this.getDictionary().setItem("MK", appearanceCharacteristics);
    }
    
    public PDAction getAction() {
        final COSDictionary action = (COSDictionary)this.getDictionary().getDictionaryObject(COSName.A);
        return PDActionFactory.createAction(action);
    }
    
    public void setAction(final PDAction action) {
        this.getDictionary().setItem(COSName.A, action);
    }
    
    public PDAnnotationAdditionalActions getActions() {
        final COSDictionary aa = (COSDictionary)this.getDictionary().getDictionaryObject("AA");
        PDAnnotationAdditionalActions retval = null;
        if (aa != null) {
            retval = new PDAnnotationAdditionalActions(aa);
        }
        return retval;
    }
    
    public void setActions(final PDAnnotationAdditionalActions actions) {
        this.getDictionary().setItem("AA", actions);
    }
    
    public void setBorderStyle(final PDBorderStyleDictionary bs) {
        this.getDictionary().setItem("BS", bs);
    }
    
    public PDBorderStyleDictionary getBorderStyle() {
        final COSDictionary bs = (COSDictionary)this.getDictionary().getItem(COSName.getPDFName("BS"));
        if (bs != null) {
            return new PDBorderStyleDictionary(bs);
        }
        return null;
    }
}
