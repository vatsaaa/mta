// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.interactive.annotation;

import org.apache.pdfbox.cos.COSStream;
import org.apache.pdfbox.pdmodel.graphics.xobject.PDXObjectForm;
import org.apache.pdfbox.cos.COSArray;
import org.apache.pdfbox.pdmodel.graphics.color.PDGamma;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSDictionary;
import org.apache.pdfbox.pdmodel.common.COSObjectable;

public class PDAppearanceCharacteristicsDictionary implements COSObjectable
{
    private COSDictionary dictionary;
    
    public PDAppearanceCharacteristicsDictionary(final COSDictionary dict) {
        this.dictionary = dict;
    }
    
    public COSDictionary getDictionary() {
        return this.dictionary;
    }
    
    public COSBase getCOSObject() {
        return this.dictionary;
    }
    
    public int getRotation() {
        return this.getDictionary().getInt(COSName.R, 0);
    }
    
    public void setRotation(final int rotation) {
        this.getDictionary().setInt(COSName.R, rotation);
    }
    
    public PDGamma getBorderColour() {
        final COSBase c = this.getDictionary().getItem(COSName.getPDFName("BC"));
        if (c instanceof COSArray) {
            return new PDGamma((COSArray)c);
        }
        return null;
    }
    
    public void setBorderColour(final PDGamma c) {
        this.getDictionary().setItem("BC", c);
    }
    
    public PDGamma getBackground() {
        final COSBase c = this.getDictionary().getItem(COSName.getPDFName("BG"));
        if (c instanceof COSArray) {
            return new PDGamma((COSArray)c);
        }
        return null;
    }
    
    public void setBackground(final PDGamma c) {
        this.getDictionary().setItem("BG", c);
    }
    
    public String getNormalCaption() {
        return this.getDictionary().getString("CA");
    }
    
    public void setNormalCaption(final String caption) {
        this.getDictionary().setString("CA", caption);
    }
    
    public String getRolloverCaption() {
        return this.getDictionary().getString("RC");
    }
    
    public void setRolloverCaption(final String caption) {
        this.getDictionary().setString("RC", caption);
    }
    
    public String getAlternateCaption() {
        return this.getDictionary().getString("AC");
    }
    
    public void setAlternateCaption(final String caption) {
        this.getDictionary().setString("AC", caption);
    }
    
    public PDXObjectForm getNormalIcon() {
        final COSBase i = this.getDictionary().getDictionaryObject("I");
        if (i instanceof COSStream) {
            return new PDXObjectForm((COSStream)i);
        }
        return null;
    }
    
    public PDXObjectForm getRolloverIcon() {
        final COSBase i = this.getDictionary().getDictionaryObject("RI");
        if (i instanceof COSStream) {
            return new PDXObjectForm((COSStream)i);
        }
        return null;
    }
    
    public PDXObjectForm getAlternateIcon() {
        final COSBase i = this.getDictionary().getDictionaryObject("IX");
        if (i instanceof COSStream) {
            return new PDXObjectForm((COSStream)i);
        }
        return null;
    }
}
