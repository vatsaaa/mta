// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.interactive.annotation;

import org.apache.pdfbox.cos.COSInteger;
import org.apache.pdfbox.cos.COSArray;
import org.apache.pdfbox.pdmodel.graphics.PDLineDashPattern;
import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSDictionary;
import org.apache.pdfbox.pdmodel.common.COSObjectable;

public class PDBorderStyleDictionary implements COSObjectable
{
    public static final String STYLE_SOLID = "S";
    public static final String STYLE_DASHED = "D";
    public static final String STYLE_BEVELED = "B";
    public static final String STYLE_INSET = "I";
    public static final String STYLE_UNDERLINE = "U";
    private COSDictionary dictionary;
    
    public PDBorderStyleDictionary() {
        this.dictionary = new COSDictionary();
    }
    
    public PDBorderStyleDictionary(final COSDictionary dict) {
        this.dictionary = dict;
    }
    
    public COSDictionary getDictionary() {
        return this.dictionary;
    }
    
    public COSBase getCOSObject() {
        return this.dictionary;
    }
    
    public void setWidth(final float w) {
        this.getDictionary().setFloat("W", w);
    }
    
    public float getWidth() {
        return this.getDictionary().getFloat("W", 1.0f);
    }
    
    public void setStyle(final String s) {
        this.getDictionary().setName("S", s);
    }
    
    public String getStyle() {
        return this.getDictionary().getNameAsString("S", "S");
    }
    
    public void setDashStyle(final PDLineDashPattern d) {
        COSArray array = null;
        if (d != null) {
            array = d.getCOSDashPattern();
        }
        this.getDictionary().setItem("D", array);
    }
    
    public PDLineDashPattern getDashStyle() {
        COSArray d = (COSArray)this.getDictionary().getDictionaryObject("D");
        if (d == null) {
            d = new COSArray();
            d.add(COSInteger.THREE);
            this.getDictionary().setItem("D", d);
        }
        return new PDLineDashPattern(d, 0);
    }
}
