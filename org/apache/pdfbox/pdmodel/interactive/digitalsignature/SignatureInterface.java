// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.interactive.digitalsignature;

import java.io.IOException;
import org.apache.pdfbox.exceptions.SignatureException;
import java.io.InputStream;

public interface SignatureInterface
{
    byte[] sign(final InputStream p0) throws SignatureException, IOException;
}
