// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.interactive.digitalsignature;

import java.io.IOException;
import org.apache.pdfbox.pdfparser.VisualSignatureParser;
import java.io.InputStream;
import org.apache.pdfbox.cos.COSDocument;

public class SignatureOptions
{
    private COSDocument visualSignature;
    private int preferedSignatureSize;
    private int pageNo;
    
    public void setPage(final int pageNo) {
        this.pageNo = pageNo;
    }
    
    public int getPage() {
        return this.pageNo;
    }
    
    public void setVisualSignature(final InputStream is) throws IOException {
        final VisualSignatureParser visParser = new VisualSignatureParser(is);
        visParser.parse();
        this.visualSignature = visParser.getDocument();
    }
    
    public COSDocument getVisualSignature() {
        return this.visualSignature;
    }
    
    public int getPreferedSignatureSize() {
        return this.preferedSignatureSize;
    }
    
    public void setPreferedSignatureSize(final int size) {
        if (size > 0) {
            this.preferedSignatureSize = size;
        }
    }
}
