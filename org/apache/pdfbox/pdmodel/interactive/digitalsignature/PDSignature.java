// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.interactive.digitalsignature;

import org.apache.pdfbox.cos.COSString;
import java.io.ByteArrayOutputStream;
import org.apache.pdfbox.pdfwriter.COSFilterInputStream;
import java.io.InputStream;
import org.apache.pdfbox.cos.COSInteger;
import org.apache.pdfbox.cos.COSArray;
import java.io.IOException;
import java.util.Calendar;
import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.cos.COSDictionary;
import org.apache.pdfbox.pdmodel.common.COSObjectable;

public class PDSignature implements COSObjectable
{
    private COSDictionary dictionary;
    public static final COSName FILTER_ADOBE_PPKLITE;
    public static final COSName FILTER_ENTRUST_PPKEF;
    public static final COSName FILTER_CICI_SIGNIT;
    public static final COSName FILTER_VERISIGN_PPKVS;
    public static final COSName SUBFILTER_ADBE_X509_RSA_SHA1;
    public static final COSName SUBFILTER_ADBE_PKCS7_DETACHED;
    public static final COSName SUBFILTER_ETSI_CADES_DETACHED;
    public static final COSName SUBFILTER_ADBE_PKCS7_SHA1;
    
    public PDSignature() {
        (this.dictionary = new COSDictionary()).setItem(COSName.TYPE, COSName.SIG);
    }
    
    public PDSignature(final COSDictionary dict) {
        this.dictionary = dict;
    }
    
    public COSBase getCOSObject() {
        return this.getDictionary();
    }
    
    public COSDictionary getDictionary() {
        return this.dictionary;
    }
    
    public void setFilter(final COSName filter) {
        this.dictionary.setItem(COSName.FILTER, filter);
    }
    
    public void setSubFilter(final COSName subfilter) {
        this.dictionary.setItem(COSName.SUBFILTER, subfilter);
    }
    
    public void setName(final String name) {
        this.dictionary.setString(COSName.NAME, name);
    }
    
    public void setLocation(final String location) {
        this.dictionary.setString(COSName.LOCATION, location);
    }
    
    public void setReason(final String reason) {
        this.dictionary.setString(COSName.REASON, reason);
    }
    
    public void setSignDate(final Calendar cal) {
        this.dictionary.setDate("M", cal);
    }
    
    public String getFilter() {
        return ((COSName)this.dictionary.getItem(COSName.FILTER)).getName();
    }
    
    public String getSubFilter() {
        return ((COSName)this.dictionary.getItem(COSName.SUBFILTER)).getName();
    }
    
    public String getName() {
        return this.dictionary.getString(COSName.NAME);
    }
    
    public String getLocation() {
        return this.dictionary.getString(COSName.LOCATION);
    }
    
    public String getReason() {
        return this.dictionary.getString(COSName.REASON);
    }
    
    public Calendar getSignDate() {
        try {
            return this.dictionary.getDate("M");
        }
        catch (IOException e) {
            return null;
        }
    }
    
    public void setByteRange(final int[] range) {
        if (range.length != 4) {
            return;
        }
        final COSArray ary = new COSArray();
        for (final int i : range) {
            ary.add(COSInteger.get(i));
        }
        this.dictionary.setItem("ByteRange", ary);
    }
    
    public int[] getByteRange() {
        final COSArray byteRange = (COSArray)this.dictionary.getDictionaryObject("ByteRange");
        final int[] ary = new int[byteRange.size()];
        for (int i = 0; i < ary.length; ++i) {
            ary[i] = byteRange.getInt(i);
        }
        return ary;
    }
    
    public byte[] getContents(final InputStream pdfFile) throws IOException {
        final int[] byteRange = this.getByteRange();
        final int begin = byteRange[0] + byteRange[1] + 1;
        final int end = byteRange[2] - begin;
        return this.getContents(new COSFilterInputStream(pdfFile, new int[] { begin, end }));
    }
    
    public byte[] getContents(final byte[] pdfFile) throws IOException {
        final int[] byteRange = this.getByteRange();
        final int begin = byteRange[0] + byteRange[1] + 1;
        final int end = byteRange[2] - begin;
        return this.getContents(new COSFilterInputStream(pdfFile, new int[] { begin, end }));
    }
    
    private byte[] getContents(final COSFilterInputStream fis) throws IOException {
        final ByteArrayOutputStream byteOS = new ByteArrayOutputStream(1024);
        final byte[] buffer = new byte[1024];
        int c;
        while ((c = fis.read(buffer)) != -1) {
            if (buffer[0] == 60 || buffer[0] == 40) {
                byteOS.write(buffer, 1, c);
            }
            else if (buffer[c - 1] == 62 || buffer[c - 1] == 41) {
                byteOS.write(buffer, 0, c - 1);
            }
            else {
                byteOS.write(buffer, 0, c);
            }
        }
        fis.close();
        return COSString.createFromHexString(byteOS.toString()).getBytes();
    }
    
    public void setContents(final byte[] bytes) {
        final COSString string = new COSString(bytes);
        string.setForceHexForm(true);
        this.dictionary.setItem("Contents", string);
    }
    
    public byte[] getSignedContent(final InputStream pdfFile) throws IOException {
        COSFilterInputStream fis = null;
        try {
            fis = new COSFilterInputStream(pdfFile, this.getByteRange());
            return fis.toByteArray();
        }
        finally {
            if (fis != null) {
                fis.close();
            }
        }
    }
    
    public byte[] getSignedContent(final byte[] pdfFile) throws IOException {
        COSFilterInputStream fis = null;
        try {
            fis = new COSFilterInputStream(pdfFile, this.getByteRange());
            return fis.toByteArray();
        }
        finally {
            if (fis != null) {
                fis.close();
            }
        }
    }
    
    static {
        FILTER_ADOBE_PPKLITE = COSName.ADOBE_PPKLITE;
        FILTER_ENTRUST_PPKEF = COSName.ENTRUST_PPKEF;
        FILTER_CICI_SIGNIT = COSName.CICI_SIGNIT;
        FILTER_VERISIGN_PPKVS = COSName.VERISIGN_PPKVS;
        SUBFILTER_ADBE_X509_RSA_SHA1 = COSName.ADBE_X509_RSA_SHA1;
        SUBFILTER_ADBE_PKCS7_DETACHED = COSName.ADBE_PKCS7_DETACHED;
        SUBFILTER_ETSI_CADES_DETACHED = COSName.getPDFName("ETSI.CAdES.detached");
        SUBFILTER_ADBE_PKCS7_SHA1 = COSName.ADBE_PKCS7_SHA1;
    }
}
