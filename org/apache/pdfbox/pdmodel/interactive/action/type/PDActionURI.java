// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.interactive.action.type;

import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSDictionary;

public class PDActionURI extends PDAction
{
    public static final String SUB_TYPE = "URI";
    
    public PDActionURI() {
        this.action = new COSDictionary();
        this.setSubType("URI");
    }
    
    public PDActionURI(final COSDictionary a) {
        super(a);
    }
    
    @Override
    public COSBase getCOSObject() {
        return this.action;
    }
    
    @Override
    public COSDictionary getCOSDictionary() {
        return this.action;
    }
    
    public String getS() {
        return this.action.getNameAsString("S");
    }
    
    public void setS(final String s) {
        this.action.setName("S", s);
    }
    
    public String getURI() {
        return this.action.getString("URI");
    }
    
    public void setURI(final String uri) {
        this.action.setString("URI", uri);
    }
    
    public boolean shouldTrackMousePosition() {
        return this.action.getBoolean("IsMap", false);
    }
    
    public void setTrackMousePosition(final boolean value) {
        this.action.setBoolean("IsMap", value);
    }
    
    @Deprecated
    public String getBase() {
        return this.action.getString("Base");
    }
    
    @Deprecated
    public void setBase(final String base) {
        this.action.setString("Base", base);
    }
}
