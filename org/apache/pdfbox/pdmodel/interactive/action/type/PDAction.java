// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.interactive.action.type;

import java.util.ArrayList;
import org.apache.pdfbox.cos.COSArray;
import org.apache.pdfbox.pdmodel.common.COSArrayList;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.pdmodel.interactive.action.PDActionFactory;
import java.util.List;
import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSDictionary;
import org.apache.pdfbox.pdmodel.common.PDDestinationOrAction;

public abstract class PDAction implements PDDestinationOrAction
{
    public static final String TYPE = "Action";
    protected COSDictionary action;
    
    public PDAction() {
        this.action = new COSDictionary();
        this.setType("Action");
    }
    
    public PDAction(final COSDictionary a) {
        this.action = a;
    }
    
    public COSBase getCOSObject() {
        return this.action;
    }
    
    public COSDictionary getCOSDictionary() {
        return this.action;
    }
    
    public String getType() {
        return this.action.getNameAsString("Type");
    }
    
    public void setType(final String type) {
        this.action.setName("Type", type);
    }
    
    public String getSubType() {
        return this.action.getNameAsString("S");
    }
    
    public void setSubType(final String s) {
        this.action.setName("S", s);
    }
    
    public List getNext() {
        List retval = null;
        final COSBase next = this.action.getDictionaryObject("Next");
        if (next instanceof COSDictionary) {
            final PDAction pdAction = PDActionFactory.createAction((COSDictionary)next);
            retval = new COSArrayList(pdAction, next, this.action, COSName.getPDFName("Next"));
        }
        else if (next instanceof COSArray) {
            final COSArray array = (COSArray)next;
            final List actions = new ArrayList();
            for (int i = 0; i < array.size(); ++i) {
                actions.add(PDActionFactory.createAction((COSDictionary)array.getObject(i)));
            }
            retval = new COSArrayList(actions, array);
        }
        return retval;
    }
    
    public void setNext(final List next) {
        this.action.setItem("Next", COSArrayList.converterToCOSArray(next));
    }
}
