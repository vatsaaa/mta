// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.interactive.action.type;

import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSDictionary;
import org.apache.pdfbox.pdmodel.common.COSObjectable;

public class PDURIDictionary implements COSObjectable
{
    private COSDictionary uriDictionary;
    
    public PDURIDictionary() {
        this.uriDictionary = new COSDictionary();
    }
    
    public PDURIDictionary(final COSDictionary dictionary) {
        this.uriDictionary = dictionary;
    }
    
    public COSBase getCOSObject() {
        return this.uriDictionary;
    }
    
    public COSDictionary getDictionary() {
        return this.uriDictionary;
    }
    
    public String getBase() {
        return this.getDictionary().getString("Base");
    }
    
    public void setBase(final String base) {
        this.getDictionary().setString("Base", base);
    }
}
