// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.interactive.action.type;

import org.apache.pdfbox.pdmodel.common.COSObjectable;
import java.io.IOException;
import org.apache.pdfbox.pdmodel.common.filespecification.PDFileSpecification;
import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSDictionary;

public class PDActionRemoteGoTo extends PDAction
{
    public static final String SUB_TYPE = "GoToR";
    
    public PDActionRemoteGoTo() {
        this.action = new COSDictionary();
        this.setSubType("GoToR");
    }
    
    public PDActionRemoteGoTo(final COSDictionary a) {
        super(a);
    }
    
    @Override
    public COSBase getCOSObject() {
        return this.action;
    }
    
    @Override
    public COSDictionary getCOSDictionary() {
        return this.action;
    }
    
    public String getS() {
        return this.action.getNameAsString("S");
    }
    
    public void setS(final String s) {
        this.action.setName("S", s);
    }
    
    public PDFileSpecification getFile() throws IOException {
        return PDFileSpecification.createFS(this.action.getDictionaryObject("F"));
    }
    
    public void setFile(final PDFileSpecification fs) {
        this.action.setItem("F", fs);
    }
    
    public COSBase getD() {
        return this.action.getDictionaryObject("D");
    }
    
    public void setD(final COSBase d) {
        this.action.setItem("D", d);
    }
    
    public boolean shouldOpenInNewWindow() {
        return this.action.getBoolean("NewWindow", true);
    }
    
    public void setOpenInNewWindow(final boolean value) {
        this.action.setBoolean("NewWindow", value);
    }
}
