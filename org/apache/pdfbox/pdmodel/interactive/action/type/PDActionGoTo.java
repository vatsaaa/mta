// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.interactive.action.type;

import org.apache.pdfbox.pdmodel.common.COSObjectable;
import java.io.IOException;
import org.apache.pdfbox.pdmodel.interactive.documentnavigation.destination.PDDestination;
import org.apache.pdfbox.cos.COSDictionary;

public class PDActionGoTo extends PDAction
{
    public static final String SUB_TYPE = "GoTo";
    
    public PDActionGoTo() {
        this.setSubType("GoTo");
    }
    
    public PDActionGoTo(final COSDictionary a) {
        super(a);
    }
    
    public PDDestination getDestination() throws IOException {
        return PDDestination.create(this.getCOSDictionary().getDictionaryObject("D"));
    }
    
    public void setDestination(final PDDestination d) {
        this.getCOSDictionary().setItem("D", d);
    }
}
