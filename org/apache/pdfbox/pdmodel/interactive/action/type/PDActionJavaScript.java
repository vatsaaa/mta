// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.interactive.action.type;

import org.apache.pdfbox.pdmodel.common.COSObjectable;
import org.apache.pdfbox.pdmodel.common.PDTextStream;
import org.apache.pdfbox.cos.COSDictionary;

public class PDActionJavaScript extends PDAction
{
    public static final String SUB_TYPE = "JavaScript";
    
    public PDActionJavaScript() {
        this.setSubType("JavaScript");
    }
    
    public PDActionJavaScript(final String js) {
        this();
        this.setAction(js);
    }
    
    public PDActionJavaScript(final COSDictionary a) {
        super(a);
    }
    
    public void setAction(final PDTextStream sAction) {
        this.action.setItem("JS", sAction);
    }
    
    public void setAction(final String sAction) {
        this.action.setString("JS", sAction);
    }
    
    public PDTextStream getAction() {
        return PDTextStream.createTextStream(this.action.getDictionaryObject("JS"));
    }
}
