// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.interactive.action.type;

import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSDictionary;
import org.apache.pdfbox.pdmodel.common.COSObjectable;

public class PDWindowsLaunchParams implements COSObjectable
{
    public static final String OPERATION_OPEN = "open";
    public static final String OPERATION_PRINT = "print";
    protected COSDictionary params;
    
    public PDWindowsLaunchParams() {
        this.params = new COSDictionary();
    }
    
    public PDWindowsLaunchParams(final COSDictionary p) {
        this.params = p;
    }
    
    public COSBase getCOSObject() {
        return this.params;
    }
    
    public COSDictionary getCOSDictionary() {
        return this.params;
    }
    
    public String getFilename() {
        return this.params.getString("F");
    }
    
    public void setFilename(final String file) {
        this.params.setString("F", file);
    }
    
    public String getDirectory() {
        return this.params.getString("D");
    }
    
    public void setDirectory(final String dir) {
        this.params.setString("D", dir);
    }
    
    public String getOperation() {
        return this.params.getString("O", "open");
    }
    
    public void setOperation(final String op) {
        this.params.setString("D", op);
    }
    
    public String getExecuteParam() {
        return this.params.getString("P");
    }
    
    public void setExecuteParam(final String param) {
        this.params.setString("P", param);
    }
}
