// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.interactive.action.type;

import org.apache.pdfbox.pdmodel.common.COSObjectable;
import java.io.IOException;
import org.apache.pdfbox.pdmodel.common.filespecification.PDFileSpecification;
import org.apache.pdfbox.cos.COSDictionary;

public class PDActionLaunch extends PDAction
{
    public static final String SUB_TYPE = "Launch";
    
    public PDActionLaunch() {
        this.setSubType("Launch");
    }
    
    public PDActionLaunch(final COSDictionary a) {
        super(a);
    }
    
    public PDFileSpecification getFile() throws IOException {
        return PDFileSpecification.createFS(this.getCOSDictionary().getDictionaryObject("F"));
    }
    
    public void setFile(final PDFileSpecification fs) {
        this.getCOSDictionary().setItem("F", fs);
    }
    
    public PDWindowsLaunchParams getWinLaunchParams() {
        final COSDictionary win = (COSDictionary)this.action.getDictionaryObject("Win");
        PDWindowsLaunchParams retval = null;
        if (win != null) {
            retval = new PDWindowsLaunchParams(win);
        }
        return retval;
    }
    
    public void setWinLaunchParams(final PDWindowsLaunchParams win) {
        this.action.setItem("Win", win);
    }
    
    public String getF() {
        return this.action.getString("F");
    }
    
    public void setF(final String f) {
        this.action.setString("F", f);
    }
    
    public String getD() {
        return this.action.getString("D");
    }
    
    public void setD(final String d) {
        this.action.setString("D", d);
    }
    
    public String getO() {
        return this.action.getString("O");
    }
    
    public void setO(final String o) {
        this.action.setString("O", o);
    }
    
    public String getP() {
        return this.action.getString("P");
    }
    
    public void setP(final String p) {
        this.action.setString("P", p);
    }
    
    public boolean shouldOpenInNewWindow() {
        return this.action.getBoolean("NewWindow", true);
    }
    
    public void setOpenInNewWindow(final boolean value) {
        this.action.setBoolean("NewWindow", value);
    }
}
