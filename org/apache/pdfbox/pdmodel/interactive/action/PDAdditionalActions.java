// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.interactive.action;

import org.apache.pdfbox.pdmodel.interactive.action.type.PDAction;
import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSDictionary;
import org.apache.pdfbox.pdmodel.common.COSObjectable;

public class PDAdditionalActions implements COSObjectable
{
    private COSDictionary actions;
    
    public PDAdditionalActions() {
        this.actions = new COSDictionary();
    }
    
    public PDAdditionalActions(final COSDictionary a) {
        this.actions = a;
    }
    
    public COSBase getCOSObject() {
        return this.actions;
    }
    
    public COSDictionary getCOSDictionary() {
        return this.actions;
    }
    
    public PDAction getF() {
        return PDActionFactory.createAction((COSDictionary)this.actions.getDictionaryObject("F"));
    }
    
    public void setF(final PDAction action) {
        this.actions.setItem("F", action);
    }
}
