// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.interactive.action;

import org.apache.pdfbox.pdmodel.interactive.action.type.PDActionURI;
import org.apache.pdfbox.pdmodel.interactive.action.type.PDActionRemoteGoTo;
import org.apache.pdfbox.pdmodel.interactive.action.type.PDActionLaunch;
import org.apache.pdfbox.pdmodel.interactive.action.type.PDActionGoTo;
import org.apache.pdfbox.pdmodel.interactive.action.type.PDActionJavaScript;
import org.apache.pdfbox.pdmodel.interactive.action.type.PDAction;
import org.apache.pdfbox.cos.COSDictionary;

public class PDActionFactory
{
    private PDActionFactory() {
    }
    
    public static PDAction createAction(final COSDictionary action) {
        PDAction retval = null;
        if (action != null) {
            final String type = action.getNameAsString("S");
            if ("JavaScript".equals(type)) {
                retval = new PDActionJavaScript(action);
            }
            else if ("GoTo".equals(type)) {
                retval = new PDActionGoTo(action);
            }
            else if ("Launch".equals(type)) {
                retval = new PDActionLaunch(action);
            }
            else if ("GoToR".equals(type)) {
                retval = new PDActionRemoteGoTo(action);
            }
            else if ("URI".equals(type)) {
                retval = new PDActionURI(action);
            }
        }
        return retval;
    }
}
