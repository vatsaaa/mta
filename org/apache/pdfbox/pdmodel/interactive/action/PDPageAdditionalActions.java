// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.interactive.action;

import org.apache.pdfbox.pdmodel.interactive.action.type.PDAction;
import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSDictionary;
import org.apache.pdfbox.pdmodel.common.COSObjectable;

public class PDPageAdditionalActions implements COSObjectable
{
    private COSDictionary actions;
    
    public PDPageAdditionalActions() {
        this.actions = new COSDictionary();
    }
    
    public PDPageAdditionalActions(final COSDictionary a) {
        this.actions = a;
    }
    
    public COSBase getCOSObject() {
        return this.actions;
    }
    
    public COSDictionary getCOSDictionary() {
        return this.actions;
    }
    
    public PDAction getO() {
        final COSDictionary o = (COSDictionary)this.actions.getDictionaryObject("O");
        PDAction retval = null;
        if (o != null) {
            retval = PDActionFactory.createAction(o);
        }
        return retval;
    }
    
    public void setO(final PDAction o) {
        this.actions.setItem("O", o);
    }
    
    public PDAction getC() {
        final COSDictionary c = (COSDictionary)this.actions.getDictionaryObject("C");
        PDAction retval = null;
        if (c != null) {
            retval = PDActionFactory.createAction(c);
        }
        return retval;
    }
    
    public void setC(final PDAction c) {
        this.actions.setItem("C", c);
    }
}
