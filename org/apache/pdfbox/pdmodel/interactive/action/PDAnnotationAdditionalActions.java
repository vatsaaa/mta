// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.interactive.action;

import org.apache.pdfbox.pdmodel.interactive.action.type.PDAction;
import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSDictionary;
import org.apache.pdfbox.pdmodel.common.COSObjectable;

public class PDAnnotationAdditionalActions implements COSObjectable
{
    private COSDictionary actions;
    
    public PDAnnotationAdditionalActions() {
        this.actions = new COSDictionary();
    }
    
    public PDAnnotationAdditionalActions(final COSDictionary a) {
        this.actions = a;
    }
    
    public COSBase getCOSObject() {
        return this.actions;
    }
    
    public COSDictionary getCOSDictionary() {
        return this.actions;
    }
    
    public PDAction getE() {
        final COSDictionary e = (COSDictionary)this.actions.getDictionaryObject("E");
        PDAction retval = null;
        if (e != null) {
            retval = PDActionFactory.createAction(e);
        }
        return retval;
    }
    
    public void setE(final PDAction e) {
        this.actions.setItem("E", e);
    }
    
    public PDAction getX() {
        final COSDictionary x = (COSDictionary)this.actions.getDictionaryObject("X");
        PDAction retval = null;
        if (x != null) {
            retval = PDActionFactory.createAction(x);
        }
        return retval;
    }
    
    public void setX(final PDAction x) {
        this.actions.setItem("X", x);
    }
    
    public PDAction getD() {
        final COSDictionary d = (COSDictionary)this.actions.getDictionaryObject("D");
        PDAction retval = null;
        if (d != null) {
            retval = PDActionFactory.createAction(d);
        }
        return retval;
    }
    
    public void setD(final PDAction d) {
        this.actions.setItem("D", d);
    }
    
    public PDAction getU() {
        final COSDictionary u = (COSDictionary)this.actions.getDictionaryObject("U");
        PDAction retval = null;
        if (u != null) {
            retval = PDActionFactory.createAction(u);
        }
        return retval;
    }
    
    public void setU(final PDAction u) {
        this.actions.setItem("U", u);
    }
    
    public PDAction getFo() {
        final COSDictionary fo = (COSDictionary)this.actions.getDictionaryObject("Fo");
        PDAction retval = null;
        if (fo != null) {
            retval = PDActionFactory.createAction(fo);
        }
        return retval;
    }
    
    public void setFo(final PDAction fo) {
        this.actions.setItem("Fo", fo);
    }
    
    public PDAction getBl() {
        final COSDictionary bl = (COSDictionary)this.actions.getDictionaryObject("Bl");
        PDAction retval = null;
        if (bl != null) {
            retval = PDActionFactory.createAction(bl);
        }
        return retval;
    }
    
    public void setBl(final PDAction bl) {
        this.actions.setItem("Bl", bl);
    }
    
    public PDAction getPO() {
        final COSDictionary po = (COSDictionary)this.actions.getDictionaryObject("PO");
        PDAction retval = null;
        if (po != null) {
            retval = PDActionFactory.createAction(po);
        }
        return retval;
    }
    
    public void setPO(final PDAction po) {
        this.actions.setItem("PO", po);
    }
    
    public PDAction getPC() {
        final COSDictionary pc = (COSDictionary)this.actions.getDictionaryObject("PC");
        PDAction retval = null;
        if (pc != null) {
            retval = PDActionFactory.createAction(pc);
        }
        return retval;
    }
    
    public void setPC(final PDAction pc) {
        this.actions.setItem("PC", pc);
    }
    
    public PDAction getPV() {
        final COSDictionary pv = (COSDictionary)this.actions.getDictionaryObject("PV");
        PDAction retval = null;
        if (pv != null) {
            retval = PDActionFactory.createAction(pv);
        }
        return retval;
    }
    
    public void setPV(final PDAction pv) {
        this.actions.setItem("PV", pv);
    }
    
    public PDAction getPI() {
        final COSDictionary pi = (COSDictionary)this.actions.getDictionaryObject("PI");
        PDAction retval = null;
        if (pi != null) {
            retval = PDActionFactory.createAction(pi);
        }
        return retval;
    }
    
    public void setPI(final PDAction pi) {
        this.actions.setItem("PI", pi);
    }
}
