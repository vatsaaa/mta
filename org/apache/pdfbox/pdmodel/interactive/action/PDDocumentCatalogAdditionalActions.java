// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.interactive.action;

import org.apache.pdfbox.pdmodel.interactive.action.type.PDAction;
import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSDictionary;
import org.apache.pdfbox.pdmodel.common.COSObjectable;

public class PDDocumentCatalogAdditionalActions implements COSObjectable
{
    private COSDictionary actions;
    
    public PDDocumentCatalogAdditionalActions() {
        this.actions = new COSDictionary();
    }
    
    public PDDocumentCatalogAdditionalActions(final COSDictionary a) {
        this.actions = a;
    }
    
    public COSBase getCOSObject() {
        return this.actions;
    }
    
    public COSDictionary getCOSDictionary() {
        return this.actions;
    }
    
    public PDAction getWC() {
        final COSDictionary wc = (COSDictionary)this.actions.getDictionaryObject("WC");
        PDAction retval = null;
        if (wc != null) {
            retval = PDActionFactory.createAction(wc);
        }
        return retval;
    }
    
    public void setWC(final PDAction wc) {
        this.actions.setItem("WC", wc);
    }
    
    public PDAction getWS() {
        final COSDictionary ws = (COSDictionary)this.actions.getDictionaryObject("WS");
        PDAction retval = null;
        if (ws != null) {
            retval = PDActionFactory.createAction(ws);
        }
        return retval;
    }
    
    public void setWS(final PDAction ws) {
        this.actions.setItem("WS", ws);
    }
    
    public PDAction getDS() {
        final COSDictionary ds = (COSDictionary)this.actions.getDictionaryObject("DS");
        PDAction retval = null;
        if (ds != null) {
            retval = PDActionFactory.createAction(ds);
        }
        return retval;
    }
    
    public void setDS(final PDAction ds) {
        this.actions.setItem("DS", ds);
    }
    
    public PDAction getWP() {
        final COSDictionary wp = (COSDictionary)this.actions.getDictionaryObject("WP");
        PDAction retval = null;
        if (wp != null) {
            retval = PDActionFactory.createAction(wp);
        }
        return retval;
    }
    
    public void setWP(final PDAction wp) {
        this.actions.setItem("WP", wp);
    }
    
    public PDAction getDP() {
        final COSDictionary dp = (COSDictionary)this.actions.getDictionaryObject("DP");
        PDAction retval = null;
        if (dp != null) {
            retval = PDActionFactory.createAction(dp);
        }
        return retval;
    }
    
    public void setDP(final PDAction dp) {
        this.actions.setItem("DP", dp);
    }
}
