// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.interactive.action;

import org.apache.pdfbox.pdmodel.interactive.action.type.PDAction;
import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSDictionary;
import org.apache.pdfbox.pdmodel.common.COSObjectable;

public class PDFormFieldAdditionalActions implements COSObjectable
{
    private COSDictionary actions;
    
    public PDFormFieldAdditionalActions() {
        this.actions = new COSDictionary();
    }
    
    public PDFormFieldAdditionalActions(final COSDictionary a) {
        this.actions = a;
    }
    
    public COSBase getCOSObject() {
        return this.actions;
    }
    
    public COSDictionary getCOSDictionary() {
        return this.actions;
    }
    
    public PDAction getK() {
        final COSDictionary k = (COSDictionary)this.actions.getDictionaryObject("K");
        PDAction retval = null;
        if (k != null) {
            retval = PDActionFactory.createAction(k);
        }
        return retval;
    }
    
    public void setK(final PDAction k) {
        this.actions.setItem("K", k);
    }
    
    public PDAction getF() {
        final COSDictionary f = (COSDictionary)this.actions.getDictionaryObject("F");
        PDAction retval = null;
        if (f != null) {
            retval = PDActionFactory.createAction(f);
        }
        return retval;
    }
    
    public void setF(final PDAction f) {
        this.actions.setItem("F", f);
    }
    
    public PDAction getV() {
        final COSDictionary v = (COSDictionary)this.actions.getDictionaryObject("V");
        PDAction retval = null;
        if (v != null) {
            retval = PDActionFactory.createAction(v);
        }
        return retval;
    }
    
    public void setV(final PDAction v) {
        this.actions.setItem("V", v);
    }
    
    public PDAction getC() {
        final COSDictionary c = (COSDictionary)this.actions.getDictionaryObject("C");
        PDAction retval = null;
        if (c != null) {
            retval = PDActionFactory.createAction(c);
        }
        return retval;
    }
    
    public void setC(final PDAction c) {
        this.actions.setItem("C", c);
    }
}
