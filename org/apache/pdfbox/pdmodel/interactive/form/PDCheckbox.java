// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.interactive.form;

import java.io.IOException;
import java.util.Iterator;
import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSDictionary;
import org.apache.pdfbox.cos.COSName;

public class PDCheckbox extends PDChoiceButton
{
    private static final COSName KEY;
    private static final COSName OFF_VALUE;
    private COSName value;
    
    public PDCheckbox(final PDAcroForm theAcroForm, final COSDictionary field) {
        super(theAcroForm, field);
        final COSDictionary ap = (COSDictionary)field.getDictionaryObject(COSName.getPDFName("AP"));
        if (ap != null) {
            final COSBase n = ap.getDictionaryObject(COSName.getPDFName("N"));
            if (n instanceof COSDictionary) {
                for (final COSName name : ((COSDictionary)n).keySet()) {
                    if (!name.equals(PDCheckbox.OFF_VALUE)) {
                        this.value = name;
                    }
                }
            }
        }
        else {
            this.value = (COSName)this.getDictionary().getDictionaryObject("V");
        }
    }
    
    public boolean isChecked() {
        boolean retval = false;
        final String onValue = this.getOnValue();
        final COSName radioValue = (COSName)this.getDictionary().getDictionaryObject(PDCheckbox.KEY);
        if (radioValue != null && this.value != null && radioValue.getName().equals(onValue)) {
            retval = true;
        }
        return retval;
    }
    
    public void check() {
        this.getDictionary().setItem(PDCheckbox.KEY, this.value);
    }
    
    public void unCheck() {
        this.getDictionary().setItem(PDCheckbox.KEY, PDCheckbox.OFF_VALUE);
    }
    
    @Override
    public void setValue(final String newValue) {
        this.getDictionary().setName("V", newValue);
        if (newValue == null) {
            this.getDictionary().setItem(PDCheckbox.KEY, PDCheckbox.OFF_VALUE);
        }
        else {
            this.getDictionary().setName(PDCheckbox.KEY, newValue);
        }
    }
    
    public String getOffValue() {
        return PDCheckbox.OFF_VALUE.getName();
    }
    
    public String getOnValue() {
        String retval = null;
        final COSDictionary ap = (COSDictionary)this.getDictionary().getDictionaryObject(COSName.getPDFName("AP"));
        final COSBase n = ap.getDictionaryObject(COSName.getPDFName("N"));
        if (n instanceof COSDictionary) {
            for (final COSName key : ((COSDictionary)n).keySet()) {
                if (!key.equals(PDCheckbox.OFF_VALUE)) {
                    retval = key.getName();
                }
            }
        }
        return retval;
    }
    
    @Override
    public String getValue() throws IOException {
        return this.getDictionary().getNameAsString("V");
    }
    
    static {
        KEY = COSName.getPDFName("AS");
        OFF_VALUE = COSName.getPDFName("Off");
    }
}
