// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.interactive.form;

import org.apache.pdfbox.pdmodel.font.PDFontDescriptor;
import org.apache.pdfbox.pdmodel.font.PDSimpleFont;
import org.apache.pdfbox.pdmodel.PDResources;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.io.RandomAccess;
import java.io.PrintWriter;
import org.apache.pdfbox.pdmodel.font.PDFont;
import java.util.Map;
import org.apache.pdfbox.pdmodel.interactive.action.PDFormFieldAdditionalActions;
import java.util.Iterator;
import org.apache.pdfbox.cos.COSFloat;
import java.util.Collection;
import java.io.OutputStream;
import org.apache.pdfbox.pdfwriter.ContentStreamWriter;
import java.io.ByteArrayOutputStream;
import org.apache.pdfbox.pdmodel.interactive.annotation.PDAppearanceDictionary;
import org.apache.pdfbox.pdmodel.interactive.annotation.PDAnnotationWidget;
import org.apache.pdfbox.util.PDFOperator;
import org.apache.pdfbox.cos.COSStream;
import java.io.InputStream;
import org.apache.pdfbox.pdfparser.PDFStreamParser;
import java.io.ByteArrayInputStream;
import org.apache.pdfbox.pdmodel.interactive.annotation.PDAppearanceStream;
import org.apache.pdfbox.cos.COSNumber;
import org.apache.pdfbox.cos.COSDictionary;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.cos.COSArray;
import java.io.IOException;
import java.util.ArrayList;
import org.apache.pdfbox.pdmodel.common.COSObjectable;
import java.util.List;
import org.apache.pdfbox.cos.COSString;

public class PDAppearance
{
    private PDVariableText parent;
    private String value;
    private COSString defaultAppearance;
    private PDAcroForm acroForm;
    private List<COSObjectable> widgets;
    
    public PDAppearance(final PDAcroForm theAcroForm, final PDVariableText field) throws IOException {
        this.widgets = new ArrayList<COSObjectable>();
        this.acroForm = theAcroForm;
        this.parent = field;
        this.widgets = field.getKids();
        if (this.widgets == null) {
            (this.widgets = new ArrayList<COSObjectable>()).add(field.getWidget());
        }
        this.defaultAppearance = this.getDefaultAppearance();
    }
    
    private COSString getDefaultAppearance() {
        COSString dap = this.parent.getDefaultAppearance();
        if (dap == null) {
            final COSArray kids = (COSArray)this.parent.getDictionary().getDictionaryObject(COSName.KIDS);
            if (kids != null && kids.size() > 0) {
                final COSDictionary firstKid = (COSDictionary)kids.getObject(0);
                dap = (COSString)firstKid.getDictionaryObject(COSName.DA);
            }
            if (dap == null) {
                dap = (COSString)this.acroForm.getDictionary().getDictionaryObject(COSName.DA);
            }
        }
        return dap;
    }
    
    private int getQ() {
        int q = this.parent.getQ();
        if (this.parent.getDictionary().getDictionaryObject(COSName.Q) == null) {
            final COSArray kids = (COSArray)this.parent.getDictionary().getDictionaryObject(COSName.KIDS);
            if (kids != null && kids.size() > 0) {
                final COSDictionary firstKid = (COSDictionary)kids.getObject(0);
                final COSNumber qNum = (COSNumber)firstKid.getDictionaryObject(COSName.Q);
                if (qNum != null) {
                    q = qNum.intValue();
                }
            }
        }
        return q;
    }
    
    private List getStreamTokens(final PDAppearanceStream appearanceStream) throws IOException {
        List tokens = null;
        if (appearanceStream != null) {
            tokens = this.getStreamTokens(appearanceStream.getStream());
        }
        return tokens;
    }
    
    private List getStreamTokens(final COSString string) throws IOException {
        List tokens = null;
        if (string != null) {
            final ByteArrayInputStream stream = new ByteArrayInputStream(string.getBytes());
            final PDFStreamParser parser = new PDFStreamParser(stream, this.acroForm.getDocument().getDocument().getScratchFile());
            parser.parse();
            tokens = parser.getTokens();
        }
        return tokens;
    }
    
    private List getStreamTokens(final COSStream stream) throws IOException {
        List tokens = null;
        if (stream != null) {
            final PDFStreamParser parser = new PDFStreamParser(stream);
            parser.parse();
            tokens = parser.getTokens();
        }
        return tokens;
    }
    
    private boolean containsMarkedContent(final List stream) {
        return stream.contains(PDFOperator.getOperator("BMC"));
    }
    
    public void setAppearanceValue(String apValue) throws IOException {
        if (this.parent.isMultiline() && apValue.indexOf(10) != -1) {
            apValue = this.convertToMultiLine(apValue);
        }
        this.value = apValue;
        for (final COSObjectable next : this.widgets) {
            PDField field = null;
            PDAnnotationWidget widget = null;
            if (next instanceof PDField) {
                field = (PDField)next;
                widget = field.getWidget();
            }
            else {
                widget = (PDAnnotationWidget)next;
            }
            PDFormFieldAdditionalActions actions = null;
            if (field != null) {
                actions = field.getActions();
            }
            if (actions != null && actions.getF() != null && widget.getDictionary().getDictionaryObject(COSName.AP) == null) {
                continue;
            }
            PDAppearanceDictionary appearance = widget.getAppearance();
            if (appearance == null) {
                appearance = new PDAppearanceDictionary();
                widget.setAppearance(appearance);
            }
            final Map normalAppearance = appearance.getNormalAppearance();
            PDAppearanceStream appearanceStream = normalAppearance.get("default");
            if (appearanceStream == null) {
                final COSStream cosStream = new COSStream(this.acroForm.getDocument().getDocument().getScratchFile());
                appearanceStream = new PDAppearanceStream(cosStream);
                appearanceStream.setBoundingBox(widget.getRectangle().createRetranslatedRectangle());
                appearance.setNormalAppearance(appearanceStream);
            }
            final List tokens = this.getStreamTokens(appearanceStream);
            final List daTokens = this.getStreamTokens(this.getDefaultAppearance());
            final PDFont pdFont = this.getFontAndUpdateResources(tokens, appearanceStream);
            if (!this.containsMarkedContent(tokens)) {
                final ByteArrayOutputStream output = new ByteArrayOutputStream();
                final ContentStreamWriter writer = new ContentStreamWriter(output);
                writer.writeTokens(tokens);
                output.write(" /Tx BMC\n".getBytes("ISO-8859-1"));
                this.insertGeneratedAppearance(widget, output, pdFont, tokens, appearanceStream);
                output.write(" EMC".getBytes("ISO-8859-1"));
                this.writeToStream(output.toByteArray(), appearanceStream);
            }
            else {
                if (tokens == null) {
                    continue;
                }
                if (daTokens != null) {
                    final int bmcIndex = tokens.indexOf(PDFOperator.getOperator("BMC"));
                    final int emcIndex = tokens.indexOf(PDFOperator.getOperator("EMC"));
                    if (bmcIndex != -1 && emcIndex != -1 && emcIndex == bmcIndex + 1) {
                        tokens.addAll(emcIndex, daTokens);
                    }
                }
                final ByteArrayOutputStream output = new ByteArrayOutputStream();
                final ContentStreamWriter writer = new ContentStreamWriter(output);
                final float fontSize = this.calculateFontSize(pdFont, appearanceStream.getBoundingBox(), tokens, null);
                boolean foundString = false;
                for (int i = 0; i < tokens.size(); ++i) {
                    if (tokens.get(i) instanceof COSString) {
                        foundString = true;
                        final COSString drawnString = tokens.get(i);
                        drawnString.reset();
                        drawnString.append(apValue.getBytes("ISO-8859-1"));
                    }
                }
                final int setFontIndex = tokens.indexOf(PDFOperator.getOperator("Tf"));
                tokens.set(setFontIndex - 1, new COSFloat(fontSize));
                if (foundString) {
                    writer.writeTokens(tokens);
                }
                else {
                    final int bmcIndex2 = tokens.indexOf(PDFOperator.getOperator("BMC"));
                    final int emcIndex2 = tokens.indexOf(PDFOperator.getOperator("EMC"));
                    if (bmcIndex2 != -1) {
                        writer.writeTokens(tokens, 0, bmcIndex2 + 1);
                    }
                    else {
                        writer.writeTokens(tokens);
                    }
                    output.write("\n".getBytes("ISO-8859-1"));
                    this.insertGeneratedAppearance(widget, output, pdFont, tokens, appearanceStream);
                    if (emcIndex2 != -1) {
                        writer.writeTokens(tokens, emcIndex2, tokens.size());
                    }
                }
                this.writeToStream(output.toByteArray(), appearanceStream);
            }
        }
    }
    
    private void insertGeneratedAppearance(final PDAnnotationWidget fieldWidget, final OutputStream output, final PDFont pdFont, final List tokens, final PDAppearanceStream appearanceStream) throws IOException {
        final PrintWriter printWriter = new PrintWriter(output, true);
        float fontSize = 0.0f;
        PDRectangle boundingBox = null;
        boundingBox = appearanceStream.getBoundingBox();
        if (boundingBox == null) {
            boundingBox = fieldWidget.getRectangle().createRetranslatedRectangle();
        }
        printWriter.println("BT");
        if (this.defaultAppearance != null) {
            final String daString = this.defaultAppearance.getString();
            final PDFStreamParser daParser = new PDFStreamParser(new ByteArrayInputStream(daString.getBytes("ISO-8859-1")), null);
            daParser.parse();
            final List<Object> daTokens = daParser.getTokens();
            fontSize = this.calculateFontSize(pdFont, boundingBox, tokens, daTokens);
            final int fontIndex = daTokens.indexOf(PDFOperator.getOperator("Tf"));
            if (fontIndex != -1) {
                daTokens.set(fontIndex - 1, new COSFloat(fontSize));
            }
            final ContentStreamWriter daWriter = new ContentStreamWriter(output);
            daWriter.writeTokens(daTokens);
        }
        printWriter.println(this.getTextPosition(boundingBox, pdFont, fontSize, tokens));
        final int q = this.getQ();
        if (q != 0) {
            if (q != 1 && q != 2) {
                throw new IOException("Error: Unknown justification value:" + q);
            }
            final float fieldWidth = boundingBox.getWidth();
            final float stringWidth = pdFont.getStringWidth(this.value) / 1000.0f * fontSize;
            float adjustAmount = fieldWidth - stringWidth - 4.0f;
            if (q == 1) {
                adjustAmount /= 2.0f;
            }
            printWriter.println(adjustAmount + " 0 Td");
        }
        printWriter.println("(" + this.value + ") Tj");
        printWriter.println("ET");
        printWriter.flush();
    }
    
    private PDFont getFontAndUpdateResources(List tokens, final PDAppearanceStream appearanceStream) throws IOException {
        PDFont retval = null;
        PDResources streamResources = appearanceStream.getResources();
        final PDResources formResources = this.acroForm.getDefaultResources();
        if (formResources != null) {
            if (streamResources == null) {
                streamResources = new PDResources();
                appearanceStream.setResources(streamResources);
            }
            final COSString da = this.getDefaultAppearance();
            if (da != null) {
                final String data = da.getString();
                final PDFStreamParser streamParser = new PDFStreamParser(new ByteArrayInputStream(data.getBytes("ISO-8859-1")), null);
                streamParser.parse();
                tokens = streamParser.getTokens();
            }
            final int setFontIndex = tokens.indexOf(PDFOperator.getOperator("Tf"));
            final COSName cosFontName = (COSName)tokens.get(setFontIndex - 2);
            final String fontName = cosFontName.getName();
            retval = streamResources.getFonts().get(fontName);
            if (retval == null) {
                retval = formResources.getFonts().get(fontName);
                streamResources.getFonts().put(fontName, retval);
            }
        }
        return retval;
    }
    
    private String convertToMultiLine(final String line) {
        int currIdx = 0;
        int lastIdx = 0;
        final StringBuffer result = new StringBuffer(line.length() + 64);
        while ((currIdx = line.indexOf(10, lastIdx)) > -1) {
            result.append(line.substring(lastIdx, currIdx));
            result.append(" ) Tj\n0 -13 Td\n(");
            lastIdx = currIdx + 1;
        }
        result.append(line.substring(lastIdx));
        return result.toString();
    }
    
    private void writeToStream(final byte[] data, final PDAppearanceStream appearanceStream) throws IOException {
        final OutputStream out = appearanceStream.getStream().createUnfilteredStream();
        out.write(data);
        out.flush();
    }
    
    private float getLineWidth(final List tokens) {
        float retval = 1.0f;
        if (tokens != null) {
            final int btIndex = tokens.indexOf(PDFOperator.getOperator("BT"));
            final int wIndex = tokens.indexOf(PDFOperator.getOperator("w"));
            if (wIndex > 0 && wIndex < btIndex) {
                retval = tokens.get(wIndex - 1).floatValue();
            }
        }
        return retval;
    }
    
    private PDRectangle getSmallestDrawnRectangle(final PDRectangle boundingBox, final List tokens) {
        PDRectangle smallest = boundingBox;
        for (int i = 0; i < tokens.size(); ++i) {
            final Object next = tokens.get(i);
            if (next == PDFOperator.getOperator("re")) {
                final COSNumber x = tokens.get(i - 4);
                final COSNumber y = tokens.get(i - 3);
                final COSNumber width = tokens.get(i - 2);
                final COSNumber height = tokens.get(i - 1);
                final PDRectangle potentialSmallest = new PDRectangle();
                potentialSmallest.setLowerLeftX(x.floatValue());
                potentialSmallest.setLowerLeftY(y.floatValue());
                potentialSmallest.setUpperRightX(x.floatValue() + width.floatValue());
                potentialSmallest.setUpperRightY(y.floatValue() + height.floatValue());
                if (smallest == null || smallest.getLowerLeftX() < potentialSmallest.getLowerLeftX() || smallest.getUpperRightY() > potentialSmallest.getUpperRightY()) {
                    smallest = potentialSmallest;
                }
            }
        }
        return smallest;
    }
    
    private float calculateFontSize(final PDFont pdFont, final PDRectangle boundingBox, final List tokens, final List daTokens) throws IOException {
        float fontSize = 0.0f;
        if (daTokens != null) {
            final int fontIndex = daTokens.indexOf(PDFOperator.getOperator("Tf"));
            if (fontIndex != -1) {
                fontSize = daTokens.get(fontIndex - 1).floatValue();
            }
        }
        float widthBasedFontSize = Float.MAX_VALUE;
        if (this.parent.doNotScroll()) {
            final float widthAtFontSize1 = pdFont.getStringWidth(this.value) / 1000.0f;
            final float availableWidth = this.getAvailableWidth(boundingBox, this.getLineWidth(tokens));
            widthBasedFontSize = availableWidth / widthAtFontSize1;
        }
        else if (fontSize == 0.0f) {
            final float lineWidth = this.getLineWidth(tokens);
            final float stringWidth = pdFont.getStringWidth(this.value);
            float height = 0.0f;
            if (pdFont instanceof PDSimpleFont) {
                height = ((PDSimpleFont)pdFont).getFontDescriptor().getFontBoundingBox().getHeight();
            }
            else {
                height = pdFont.getAverageFontWidth();
            }
            height /= 1000.0f;
            final float availHeight = this.getAvailableHeight(boundingBox, lineWidth);
            fontSize = Math.min(availHeight / height, widthBasedFontSize);
        }
        return fontSize;
    }
    
    private String getTextPosition(final PDRectangle boundingBox, final PDFont pdFont, final float fontSize, final List tokens) throws IOException {
        final float lineWidth = this.getLineWidth(tokens);
        float pos = 0.0f;
        if (this.parent.isMultiline()) {
            final int rows = (int)(this.getAvailableHeight(boundingBox, lineWidth) / (int)fontSize);
            pos = rows * fontSize - fontSize;
        }
        else {
            if (!(pdFont instanceof PDSimpleFont)) {
                throw new IOException("Error: Don't know how to calculate the position for non-simple fonts");
            }
            final PDFontDescriptor fd = ((PDSimpleFont)pdFont).getFontDescriptor();
            final float bBoxHeight = boundingBox.getHeight();
            float fontHeight = fd.getFontBoundingBox().getHeight() + 2.0f * fd.getDescent();
            fontHeight = fontHeight / 1000.0f * fontSize;
            pos = (bBoxHeight - fontHeight) / 2.0f;
        }
        final PDRectangle innerBox = this.getSmallestDrawnRectangle(boundingBox, tokens);
        final float xInset = 2.0f + 2.0f * (boundingBox.getWidth() - innerBox.getWidth());
        return Math.round(xInset) + " " + pos + " Td";
    }
    
    private float getAvailableWidth(final PDRectangle boundingBox, final float lineWidth) {
        return boundingBox.getWidth() - 2.0f * lineWidth;
    }
    
    private float getAvailableHeight(final PDRectangle boundingBox, final float lineWidth) {
        return boundingBox.getHeight() - 2.0f * lineWidth;
    }
}
