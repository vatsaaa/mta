// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.interactive.form;

import org.apache.pdfbox.pdmodel.PDResources;
import org.apache.pdfbox.cos.COSString;
import java.util.HashMap;
import org.apache.pdfbox.pdmodel.common.COSArrayList;
import java.util.Iterator;
import org.apache.pdfbox.pdmodel.fdf.FDFCatalog;
import java.util.ArrayList;
import org.apache.pdfbox.pdmodel.fdf.FDFDictionary;
import java.io.IOException;
import java.util.List;
import org.apache.pdfbox.pdmodel.fdf.FDFField;
import org.apache.pdfbox.pdmodel.fdf.FDFDocument;
import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.cos.COSArray;
import java.util.Map;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.cos.COSDictionary;
import org.apache.pdfbox.pdmodel.common.COSObjectable;

public class PDAcroForm implements COSObjectable
{
    private COSDictionary acroForm;
    private PDDocument document;
    private Map fieldCache;
    
    public PDAcroForm(final PDDocument doc) {
        this.document = doc;
        this.acroForm = new COSDictionary();
        final COSArray fields = new COSArray();
        this.acroForm.setItem(COSName.getPDFName("Fields"), fields);
    }
    
    public PDAcroForm(final PDDocument doc, final COSDictionary form) {
        this.document = doc;
        this.acroForm = form;
    }
    
    public PDDocument getDocument() {
        return this.document;
    }
    
    public COSDictionary getDictionary() {
        return this.acroForm;
    }
    
    public void importFDF(final FDFDocument fdf) throws IOException {
        final List fields = fdf.getCatalog().getFDF().getFields();
        if (fields != null) {
            for (int i = 0; i < fields.size(); ++i) {
                final FDFField fdfField = fields.get(i);
                final PDField docField = this.getField(fdfField.getPartialFieldName());
                if (docField != null) {
                    docField.importFDF(fdfField);
                }
            }
        }
    }
    
    public FDFDocument exportFDF() throws IOException {
        final FDFDocument fdf = new FDFDocument();
        final FDFCatalog catalog = fdf.getCatalog();
        final FDFDictionary fdfDict = new FDFDictionary();
        catalog.setFDF(fdfDict);
        final List fdfFields = new ArrayList();
        final List fields = this.getFields();
        for (final PDField docField : fields) {
            this.addFieldAndChildren(docField, fdfFields);
        }
        fdfDict.setID(this.document.getDocument().getDocumentID());
        if (fdfFields.size() > 0) {
            fdfDict.setFields(fdfFields);
        }
        return fdf;
    }
    
    private void addFieldAndChildren(final PDField docField, final List fdfFields) throws IOException {
        final Object fieldValue = docField.getValue();
        final FDFField fdfField = new FDFField();
        fdfField.setPartialFieldName(docField.getPartialName());
        fdfField.setValue(fieldValue);
        final List kids = docField.getKids();
        final List childFDFFields = new ArrayList();
        if (kids != null) {
            for (int i = 0; i < kids.size(); ++i) {
                this.addFieldAndChildren(kids.get(i), childFDFFields);
            }
            if (childFDFFields.size() > 0) {
                fdfField.setKids(childFDFFields);
            }
        }
        if (fieldValue != null || childFDFFields.size() > 0) {
            fdfFields.add(fdfField);
        }
    }
    
    public List getFields() throws IOException {
        List retval = null;
        final COSArray fields = (COSArray)this.acroForm.getDictionaryObject(COSName.getPDFName("Fields"));
        if (fields != null) {
            final List actuals = new ArrayList();
            for (int i = 0; i < fields.size(); ++i) {
                final COSDictionary element = (COSDictionary)fields.getObject(i);
                if (element != null) {
                    final PDField field = PDFieldFactory.createField(this, element);
                    if (field != null) {
                        actuals.add(field);
                    }
                }
            }
            retval = new COSArrayList(actuals, fields);
        }
        return retval;
    }
    
    public void setFields(final List fields) {
        this.acroForm.setItem("Fields", COSArrayList.converterToCOSArray(fields));
    }
    
    public void setCacheFields(final boolean cache) throws IOException {
        if (cache) {
            this.fieldCache = new HashMap();
            final List fields = this.getFields();
            for (final PDField next : fields) {
                this.fieldCache.put(next.getFullyQualifiedName(), next);
            }
        }
        else {
            this.fieldCache = null;
        }
    }
    
    public boolean isCachingFields() {
        return this.fieldCache != null;
    }
    
    public PDField getField(final String name) throws IOException {
        PDField retval = null;
        if (this.fieldCache != null) {
            retval = this.fieldCache.get(name);
        }
        else {
            final String[] nameSubSection = name.split("\\.");
            final COSArray fields = (COSArray)this.acroForm.getDictionaryObject(COSName.getPDFName("Fields"));
            for (int i = 0; i < fields.size() && retval == null; ++i) {
                final COSDictionary element = (COSDictionary)fields.getObject(i);
                if (element != null) {
                    final COSString fieldName = (COSString)element.getDictionaryObject(COSName.getPDFName("T"));
                    if (fieldName.getString().equals(name) || fieldName.getString().equals(nameSubSection[0])) {
                        final PDField root = PDFieldFactory.createField(this, element);
                        if (nameSubSection.length > 1) {
                            final PDField kid = root.findKid(nameSubSection, 1);
                            if (kid != null) {
                                retval = kid;
                            }
                            else {
                                retval = root;
                            }
                        }
                        else {
                            retval = root;
                        }
                    }
                }
            }
        }
        return retval;
    }
    
    public PDResources getDefaultResources() {
        PDResources retval = null;
        final COSDictionary dr = (COSDictionary)this.acroForm.getDictionaryObject(COSName.getPDFName("DR"));
        if (dr != null) {
            retval = new PDResources(dr);
        }
        return retval;
    }
    
    public void setDefaultResources(final PDResources dr) {
        COSDictionary drDict = null;
        if (dr != null) {
            drDict = dr.getCOSDictionary();
        }
        this.acroForm.setItem(COSName.getPDFName("DR"), drDict);
    }
    
    public COSBase getCOSObject() {
        return this.acroForm;
    }
    
    public PDXFA getXFA() {
        PDXFA xfa = null;
        final COSBase base = this.acroForm.getDictionaryObject("XFA");
        if (base != null) {
            xfa = new PDXFA(base);
        }
        return xfa;
    }
    
    public void setXFA(final PDXFA xfa) {
        this.acroForm.setItem("XFA", xfa);
    }
}
