// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.interactive.form;

import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.pdmodel.common.COSObjectable;
import org.apache.pdfbox.pdmodel.interactive.digitalsignature.PDSignature;
import java.util.Iterator;
import java.util.Set;
import java.util.List;
import java.util.HashSet;
import java.io.IOException;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.cos.COSDictionary;

public class PDSignatureField extends PDField
{
    public PDSignatureField(final PDAcroForm theAcroForm, final COSDictionary field) throws IOException {
        super(theAcroForm, field);
        this.getDictionary().setName(COSName.TYPE, "Annot");
        this.getDictionary().setName(COSName.SUBTYPE, "Widget");
    }
    
    public PDSignatureField(final PDAcroForm theAcroForm) throws IOException {
        super(theAcroForm);
        this.getDictionary().setName("FT", "Sig");
        this.getWidget().setLocked(true);
        this.getWidget().setPrinted(true);
        this.setPartialName(this.generatePartialName());
        this.getDictionary().setName(COSName.TYPE, "Annot");
        this.getDictionary().setName(COSName.SUBTYPE, "Widget");
    }
    
    private String generatePartialName() throws IOException {
        final PDAcroForm acroForm = this.getAcroForm();
        final List fields = acroForm.getFields();
        final String fieldName = "Signature";
        int i = 1;
        final Set<String> sigNames = new HashSet<String>();
        for (final Object object : fields) {
            if (object instanceof PDSignatureField) {
                sigNames.add(((PDSignatureField)object).getPartialName());
            }
        }
        while (sigNames.contains(fieldName + i)) {
            ++i;
        }
        return fieldName + i;
    }
    
    @Deprecated
    @Override
    public void setValue(final String value) throws IOException {
        throw new RuntimeException("Can't set signature as String, use setSignature(PDSignature) instead");
    }
    
    @Deprecated
    @Override
    public String getValue() throws IOException {
        throw new RuntimeException("Can't get signature as String, use getSignature() instead.");
    }
    
    @Override
    public String toString() {
        return "PDSignature";
    }
    
    public void setSignature(final PDSignature value) {
        this.getDictionary().setItem("V", value);
    }
    
    public PDSignature getSignature() {
        final COSBase dictionary = this.getDictionary().getDictionaryObject(COSName.V);
        if (dictionary == null) {
            return null;
        }
        return new PDSignature((COSDictionary)dictionary);
    }
}
