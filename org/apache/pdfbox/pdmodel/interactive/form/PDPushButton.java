// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.interactive.form;

import java.io.IOException;
import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.cos.COSString;
import org.apache.pdfbox.cos.COSDictionary;

public class PDPushButton extends PDField
{
    public PDPushButton(final PDAcroForm theAcroForm, final COSDictionary field) {
        super(theAcroForm, field);
    }
    
    @Override
    public void setValue(final String value) throws IOException {
        final COSString fieldValue = new COSString(value);
        this.getDictionary().setItem(COSName.getPDFName("V"), fieldValue);
        this.getDictionary().setItem(COSName.getPDFName("DV"), fieldValue);
    }
    
    @Override
    public String getValue() throws IOException {
        return this.getDictionary().getString("V");
    }
}
