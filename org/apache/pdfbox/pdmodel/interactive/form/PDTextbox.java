// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.interactive.form;

import org.apache.pdfbox.cos.COSDictionary;

public class PDTextbox extends PDVariableText
{
    public PDTextbox(final PDAcroForm theAcroForm) {
        super(theAcroForm);
    }
    
    public PDTextbox(final PDAcroForm theAcroForm, final COSDictionary field) {
        super(theAcroForm, field);
    }
}
