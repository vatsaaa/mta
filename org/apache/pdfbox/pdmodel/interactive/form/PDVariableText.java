// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.interactive.form;

import org.apache.pdfbox.cos.COSNumber;
import org.apache.pdfbox.util.BitFlagHelper;
import java.io.IOException;
import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.cos.COSDictionary;
import org.apache.pdfbox.cos.COSString;

public abstract class PDVariableText extends PDField
{
    public static final int FLAG_MULTILINE = 4096;
    public static final int FLAG_PASSWORD = 8192;
    public static final int FLAG_FILE_SELECT = 1048576;
    public static final int FLAG_DO_NOT_SPELL_CHECK = 4194304;
    public static final int FLAG_DO_NOT_SCROLL = 8388608;
    public static final int FLAG_COMB = 16777216;
    public static final int FLAG_RICH_TEXT = 33554432;
    private COSString da;
    private PDAppearance appearance;
    public static final int QUADDING_LEFT = 0;
    public static final int QUADDING_CENTERED = 1;
    public static final int QUADDING_RIGHT = 2;
    
    public PDVariableText(final PDAcroForm theAcroForm) {
        super(theAcroForm);
    }
    
    public PDVariableText(final PDAcroForm theAcroForm, final COSDictionary field) {
        super(theAcroForm, field);
        this.da = (COSString)field.getDictionaryObject(COSName.DA);
    }
    
    @Override
    public void setValue(final String value) throws IOException {
        final COSString fieldValue = new COSString(value);
        this.getDictionary().setItem(COSName.V, fieldValue);
        if (this.appearance == null) {
            this.appearance = new PDAppearance(this.getAcroForm(), this);
        }
        this.appearance.setAppearanceValue(value);
    }
    
    @Override
    public String getValue() throws IOException {
        return this.getDictionary().getString(COSName.V);
    }
    
    public boolean isMultiline() {
        return BitFlagHelper.getFlag(this.getDictionary(), COSName.FF, 4096);
    }
    
    public void setMultiline(final boolean multiline) {
        BitFlagHelper.setFlag(this.getDictionary(), COSName.FF, 4096, multiline);
    }
    
    public boolean isPassword() {
        return BitFlagHelper.getFlag(this.getDictionary(), COSName.FF, 8192);
    }
    
    public void setPassword(final boolean password) {
        BitFlagHelper.setFlag(this.getDictionary(), COSName.FF, 8192, password);
    }
    
    public boolean isFileSelect() {
        return BitFlagHelper.getFlag(this.getDictionary(), COSName.FF, 1048576);
    }
    
    public void setFileSelect(final boolean fileSelect) {
        BitFlagHelper.setFlag(this.getDictionary(), COSName.FF, 1048576, fileSelect);
    }
    
    public boolean doNotSpellCheck() {
        return BitFlagHelper.getFlag(this.getDictionary(), COSName.FF, 4194304);
    }
    
    public void setDoNotSpellCheck(final boolean doNotSpellCheck) {
        BitFlagHelper.setFlag(this.getDictionary(), COSName.FF, 4194304, doNotSpellCheck);
    }
    
    public boolean doNotScroll() {
        return BitFlagHelper.getFlag(this.getDictionary(), COSName.FF, 8388608);
    }
    
    public void setDoNotScroll(final boolean doNotScroll) {
        BitFlagHelper.setFlag(this.getDictionary(), COSName.FF, 8388608, doNotScroll);
    }
    
    public boolean shouldComb() {
        return BitFlagHelper.getFlag(this.getDictionary(), COSName.FF, 16777216);
    }
    
    public void setComb(final boolean comb) {
        BitFlagHelper.setFlag(this.getDictionary(), COSName.FF, 16777216, comb);
    }
    
    public boolean isRichText() {
        return BitFlagHelper.getFlag(this.getDictionary(), COSName.FF, 33554432);
    }
    
    public void setRichText(final boolean richText) {
        BitFlagHelper.setFlag(this.getDictionary(), COSName.FF, 33554432, richText);
    }
    
    protected COSString getDefaultAppearance() {
        return this.da;
    }
    
    public int getQ() {
        int retval = 0;
        final COSNumber number = (COSNumber)this.getDictionary().getDictionaryObject(COSName.Q);
        if (number != null) {
            retval = number.intValue();
        }
        return retval;
    }
    
    public void setQ(final int q) {
        this.getDictionary().setInt(COSName.Q, q);
    }
}
