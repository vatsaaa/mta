// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.interactive.form;

import org.apache.pdfbox.pdmodel.common.COSArrayList;
import java.util.ArrayList;
import org.apache.pdfbox.cos.COSArray;
import java.io.IOException;
import java.util.List;
import org.apache.pdfbox.util.BitFlagHelper;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.cos.COSDictionary;

public class PDRadioCollection extends PDChoiceButton
{
    public static final int FLAG_RADIOS_IN_UNISON = 33554432;
    
    public PDRadioCollection(final PDAcroForm theAcroForm, final COSDictionary field) {
        super(theAcroForm, field);
    }
    
    public void setRadiosInUnison(final boolean radiosInUnison) {
        BitFlagHelper.setFlag(this.getDictionary(), COSName.FF, 33554432, radiosInUnison);
    }
    
    public boolean isRadiosInUnison() {
        return BitFlagHelper.getFlag(this.getDictionary(), COSName.FF, 33554432);
    }
    
    @Override
    public void setValue(final String value) throws IOException {
        this.getDictionary().setString(COSName.V, value);
        final List kids = this.getKids();
        for (int i = 0; i < kids.size(); ++i) {
            final PDField field = kids.get(i);
            if (field instanceof PDCheckbox) {
                final PDCheckbox btn = (PDCheckbox)field;
                if (btn.getOnValue().equals(value)) {
                    btn.check();
                }
                else {
                    btn.unCheck();
                }
            }
        }
    }
    
    @Override
    public String getValue() throws IOException {
        String retval = null;
        final List kids = this.getKids();
        for (int i = 0; i < kids.size(); ++i) {
            final PDField kid = kids.get(i);
            if (kid instanceof PDCheckbox) {
                final PDCheckbox btn = (PDCheckbox)kid;
                if (btn.isChecked()) {
                    retval = btn.getOnValue();
                }
            }
        }
        if (retval == null) {
            retval = this.getDictionary().getNameAsString(COSName.V);
        }
        return retval;
    }
    
    @Override
    public List getKids() throws IOException {
        List retval = null;
        final COSArray kids = (COSArray)this.getDictionary().getDictionaryObject(COSName.KIDS);
        if (kids != null) {
            final List kidsList = new ArrayList();
            for (int i = 0; i < kids.size(); ++i) {
                kidsList.add(PDFieldFactory.createField(this.getAcroForm(), (COSDictionary)kids.getObject(i)));
            }
            retval = new COSArrayList(kidsList, kids);
        }
        return retval;
    }
}
