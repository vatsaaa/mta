// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.interactive.form;

import java.io.IOException;
import org.apache.pdfbox.cos.COSDictionary;

public class PDSignature extends PDField
{
    public PDSignature(final PDAcroForm theAcroForm, final COSDictionary field) {
        super(theAcroForm, field);
    }
    
    @Override
    public void setValue(final String value) throws IOException {
        throw new RuntimeException("Not yet implemented");
    }
    
    @Override
    public String getValue() throws IOException {
        throw new RuntimeException("Not yet implemented");
    }
    
    @Override
    public String toString() {
        return "PDSignature";
    }
}
