// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.interactive.form;

import java.io.IOException;
import org.apache.pdfbox.cos.COSDictionary;

public class PDUnknownField extends PDField
{
    public PDUnknownField(final PDAcroForm theAcroForm, final COSDictionary field) {
        super(theAcroForm, field);
    }
    
    @Override
    public void setValue(final String value) throws IOException {
    }
    
    @Override
    public String getValue() throws IOException {
        return null;
    }
}
