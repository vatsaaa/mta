// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.interactive.form;

import org.apache.pdfbox.cos.COSInteger;
import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.cos.COSString;
import java.io.IOException;
import org.apache.pdfbox.cos.COSArray;
import org.apache.pdfbox.cos.COSDictionary;

public class PDChoiceField extends PDVariableText
{
    public PDChoiceField(final PDAcroForm theAcroForm, final COSDictionary field) {
        super(theAcroForm, field);
    }
    
    @Override
    public void setValue(final String optionValue) throws IOException {
        int indexSelected = -1;
        final COSArray options = (COSArray)this.getDictionary().getDictionaryObject("Opt");
        if (options.size() == 0) {
            throw new IOException("Error: You cannot set a value for a choice field if there are no options.");
        }
        for (int i = 0; i < options.size() && indexSelected == -1; ++i) {
            final COSBase option = options.getObject(i);
            if (option instanceof COSArray) {
                final COSArray keyValuePair = (COSArray)option;
                final COSString key = (COSString)keyValuePair.getObject(0);
                final COSString value = (COSString)keyValuePair.getObject(1);
                if (optionValue.equals(key.getString()) || optionValue.equals(value.getString())) {
                    super.setValue(value.getString());
                    this.getDictionary().setItem(COSName.getPDFName("V"), key);
                    indexSelected = i;
                }
            }
            else {
                final COSString value2 = (COSString)option;
                if (optionValue.equals(value2.getString())) {
                    super.setValue(optionValue);
                    indexSelected = i;
                }
            }
        }
        if (indexSelected == -1) {
            throw new IOException("Error: '" + optionValue + "' was not an available option.");
        }
        final COSArray indexArray = (COSArray)this.getDictionary().getDictionaryObject("I");
        if (indexArray != null) {
            indexArray.clear();
            indexArray.add(COSInteger.get(indexSelected));
        }
    }
}
