// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.interactive.form;

import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.pdmodel.common.COSObjectable;

public class PDXFA implements COSObjectable
{
    private COSBase xfa;
    
    public PDXFA(final COSBase xfaBase) {
        this.xfa = xfaBase;
    }
    
    public COSBase getCOSObject() {
        return this.xfa;
    }
}
