// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.interactive.form;

import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.pdmodel.common.COSArrayList;
import org.apache.pdfbox.cos.COSString;
import java.util.ArrayList;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.cos.COSArray;
import java.util.List;
import org.apache.pdfbox.cos.COSDictionary;

public abstract class PDChoiceButton extends PDField
{
    public PDChoiceButton(final PDAcroForm theAcroForm, final COSDictionary field) {
        super(theAcroForm, field);
    }
    
    public List getOptions() {
        List retval = null;
        final COSArray array = (COSArray)this.getDictionary().getDictionaryObject(COSName.getPDFName("Opt"));
        if (array != null) {
            final List strings = new ArrayList();
            for (int i = 0; i < array.size(); ++i) {
                strings.add(((COSString)array.getObject(i)).getString());
            }
            retval = new COSArrayList(strings, array);
        }
        return retval;
    }
    
    public void setOptions(final List options) {
        this.getDictionary().setItem(COSName.getPDFName("Opt"), COSArrayList.converterToCOSArray(options));
    }
}
