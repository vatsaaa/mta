// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.interactive.form;

import org.apache.pdfbox.pdmodel.interactive.action.PDFormFieldAdditionalActions;
import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.pdmodel.common.COSArrayList;
import java.util.ArrayList;
import org.apache.pdfbox.cos.COSArray;
import java.util.List;
import org.apache.pdfbox.pdmodel.interactive.annotation.PDAnnotationWidget;
import org.apache.pdfbox.pdmodel.common.PDTextStream;
import org.apache.pdfbox.pdmodel.fdf.FDFField;
import org.apache.pdfbox.cos.COSInteger;
import org.apache.pdfbox.util.BitFlagHelper;
import java.io.IOException;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.cos.COSDictionary;
import org.apache.pdfbox.pdmodel.common.COSObjectable;

public abstract class PDField implements COSObjectable
{
    public static final int FLAG_READ_ONLY = 1;
    public static final int FLAG_REQUIRED = 2;
    public static final int FLAG_NO_EXPORT = 4;
    private PDAcroForm acroForm;
    private COSDictionary dictionary;
    
    public PDField(final PDAcroForm theAcroForm) {
        this.acroForm = theAcroForm;
        this.dictionary = new COSDictionary();
    }
    
    public PDField(final PDAcroForm theAcroForm, final COSDictionary field) {
        this.acroForm = theAcroForm;
        this.dictionary = field;
    }
    
    public String getPartialName() {
        return this.getDictionary().getString(COSName.T);
    }
    
    public void setPartialName(final String name) {
        this.getDictionary().setString(COSName.T, name);
    }
    
    public String getFullyQualifiedName() throws IOException {
        final PDField parent = this.getParent();
        String parentName = null;
        if (parent != null) {
            parentName = parent.getFullyQualifiedName();
        }
        String finalName = this.getPartialName();
        if (parentName != null) {
            finalName = parentName + "." + finalName;
        }
        return finalName;
    }
    
    public String getAlternateFieldName() {
        return this.getDictionary().getString(COSName.TU);
    }
    
    public void setAlternateFieldName(final String alternateFieldName) {
        this.getDictionary().setString(COSName.TU, alternateFieldName);
    }
    
    public String getFieldType() {
        return this.getDictionary().getNameAsString(COSName.FT);
    }
    
    public String findFieldType() {
        return this.findFieldType(this.getDictionary());
    }
    
    private String findFieldType(final COSDictionary dic) {
        String retval = dic.getNameAsString(COSName.FT);
        if (retval == null) {
            final COSDictionary parent = (COSDictionary)dic.getDictionaryObject(COSName.PARENT, COSName.P);
            if (parent != null) {
                retval = this.findFieldType(parent);
            }
        }
        return retval;
    }
    
    public abstract void setValue(final String p0) throws IOException;
    
    public abstract String getValue() throws IOException;
    
    public void setReadonly(final boolean readonly) {
        BitFlagHelper.setFlag(this.getDictionary(), COSName.FF, 1, readonly);
    }
    
    public boolean isReadonly() {
        return BitFlagHelper.getFlag(this.getDictionary(), COSName.FF, 1);
    }
    
    public void setRequired(final boolean required) {
        BitFlagHelper.setFlag(this.getDictionary(), COSName.FF, 2, required);
    }
    
    public boolean isRequired() {
        return BitFlagHelper.getFlag(this.getDictionary(), COSName.FF, 2);
    }
    
    public void setNoExport(final boolean noExport) {
        BitFlagHelper.setFlag(this.getDictionary(), COSName.FF, 4, noExport);
    }
    
    public boolean isNoExport() {
        return BitFlagHelper.getFlag(this.getDictionary(), COSName.FF, 4);
    }
    
    public int getFieldFlags() {
        int retval = 0;
        final COSInteger ff = (COSInteger)this.getDictionary().getDictionaryObject(COSName.FF);
        if (ff != null) {
            retval = ff.intValue();
        }
        return retval;
    }
    
    public void setFieldFlags(final int flags) {
        this.getDictionary().setInt(COSName.FF, flags);
    }
    
    public void importFDF(final FDFField fdfField) throws IOException {
        final Object fieldValue = fdfField.getValue();
        int fieldFlags = this.getFieldFlags();
        if (fieldValue != null) {
            if (fieldValue instanceof String) {
                this.setValue((String)fieldValue);
            }
            else {
                if (!(fieldValue instanceof PDTextStream)) {
                    throw new IOException("Unknown field type:" + fieldValue.getClass().getName());
                }
                this.setValue(((PDTextStream)fieldValue).getAsString());
            }
        }
        final Integer ff = fdfField.getFieldFlags();
        if (ff != null) {
            this.setFieldFlags(ff);
        }
        else {
            final Integer setFf = fdfField.getSetFieldFlags();
            if (setFf != null) {
                final int setFfInt = setFf;
                fieldFlags |= setFfInt;
                this.setFieldFlags(fieldFlags);
            }
            final Integer clrFf = fdfField.getClearFieldFlags();
            if (clrFf != null) {
                int clrFfValue = clrFf;
                clrFfValue ^= -1;
                fieldFlags &= clrFfValue;
                this.setFieldFlags(fieldFlags);
            }
        }
        final PDAnnotationWidget widget = this.getWidget();
        if (widget != null) {
            int annotFlags = widget.getAnnotationFlags();
            final Integer f = fdfField.getWidgetFieldFlags();
            if (f != null && widget != null) {
                widget.setAnnotationFlags(f);
            }
            else {
                final Integer setF = fdfField.getSetWidgetFieldFlags();
                if (setF != null) {
                    annotFlags |= setF;
                    widget.setAnnotationFlags(annotFlags);
                }
                final Integer clrF = fdfField.getClearWidgetFieldFlags();
                if (clrF != null) {
                    int clrFValue = clrF;
                    clrFValue = (int)((long)clrFValue ^ 0xFFFFFFFFL);
                    annotFlags &= clrFValue;
                    widget.setAnnotationFlags(annotFlags);
                }
            }
        }
        final List<FDFField> fdfKids = fdfField.getKids();
        final List<COSObjectable> pdKids = this.getKids();
        for (int i = 0; fdfKids != null && i < fdfKids.size(); ++i) {
            final FDFField fdfChild = fdfKids.get(i);
            final String fdfName = fdfChild.getPartialFieldName();
            for (int j = 0; j < pdKids.size(); ++j) {
                final Object pdChildObj = pdKids.get(j);
                if (pdChildObj instanceof PDField) {
                    final PDField pdChild = (PDField)pdChildObj;
                    if (fdfName != null && fdfName.equals(pdChild.getPartialName())) {
                        pdChild.importFDF(fdfChild);
                    }
                }
            }
        }
    }
    
    public PDAnnotationWidget getWidget() throws IOException {
        PDAnnotationWidget retval = null;
        final List<COSObjectable> kids = this.getKids();
        if (kids == null) {
            retval = new PDAnnotationWidget(this.getDictionary());
        }
        else if (kids.size() > 0) {
            final Object firstKid = kids.get(0);
            if (firstKid instanceof PDAnnotationWidget) {
                retval = (PDAnnotationWidget)firstKid;
            }
            else {
                retval = ((PDField)firstKid).getWidget();
            }
        }
        else {
            retval = null;
        }
        return retval;
    }
    
    public PDField getParent() throws IOException {
        PDField parent = null;
        final COSDictionary parentDic = (COSDictionary)this.getDictionary().getDictionaryObject(COSName.PARENT, COSName.P);
        if (parentDic != null) {
            parent = PDFieldFactory.createField(this.getAcroForm(), parentDic);
        }
        return parent;
    }
    
    public void setParent(final PDField parent) {
        this.getDictionary().setItem("Parent", parent);
    }
    
    public PDField findKid(final String[] name, final int nameIndex) throws IOException {
        PDField retval = null;
        final COSArray kids = (COSArray)this.getDictionary().getDictionaryObject(COSName.KIDS);
        if (kids != null) {
            for (int i = 0; retval == null && i < kids.size(); ++i) {
                final COSDictionary kidDictionary = (COSDictionary)kids.getObject(i);
                if (name[nameIndex].equals(kidDictionary.getString("T"))) {
                    retval = PDFieldFactory.createField(this.acroForm, kidDictionary);
                    if (name.length > nameIndex + 1) {
                        retval = retval.findKid(name, nameIndex + 1);
                    }
                }
            }
        }
        return retval;
    }
    
    public List<COSObjectable> getKids() throws IOException {
        List<COSObjectable> retval = null;
        final COSArray kids = (COSArray)this.getDictionary().getDictionaryObject(COSName.KIDS);
        if (kids != null) {
            final List<COSObjectable> kidsList = new ArrayList<COSObjectable>();
            for (int i = 0; i < kids.size(); ++i) {
                final COSDictionary kidDictionary = (COSDictionary)kids.getObject(i);
                final COSDictionary parent = (COSDictionary)kidDictionary.getDictionaryObject(COSName.PARENT, COSName.P);
                if (kidDictionary.getDictionaryObject(COSName.FT) != null || (parent != null && parent.getDictionaryObject(COSName.FT) != null)) {
                    kidsList.add(PDFieldFactory.createField(this.acroForm, kidDictionary));
                }
                else if ("Widget".equals(kidDictionary.getNameAsString(COSName.SUBTYPE))) {
                    kidsList.add(new PDAnnotationWidget(kidDictionary));
                }
                else {
                    kidsList.add(PDFieldFactory.createField(this.acroForm, kidDictionary));
                }
            }
            retval = (List<COSObjectable>)new COSArrayList(kidsList, kids);
        }
        return retval;
    }
    
    public void setKids(final List<COSObjectable> kids) {
        final COSArray kidsArray = COSArrayList.converterToCOSArray(kids);
        this.getDictionary().setItem(COSName.KIDS, kidsArray);
    }
    
    @Override
    public String toString() {
        return "" + this.getDictionary().getDictionaryObject(COSName.V);
    }
    
    public PDAcroForm getAcroForm() {
        return this.acroForm;
    }
    
    public void setAcroForm(final PDAcroForm value) {
        this.acroForm = value;
    }
    
    public COSDictionary getDictionary() {
        return this.dictionary;
    }
    
    public COSBase getCOSObject() {
        return this.dictionary;
    }
    
    public PDFormFieldAdditionalActions getActions() {
        final COSDictionary aa = (COSDictionary)this.dictionary.getDictionaryObject(COSName.AA);
        PDFormFieldAdditionalActions retval = null;
        if (aa != null) {
            retval = new PDFormFieldAdditionalActions(aa);
        }
        return retval;
    }
    
    public void setActions(final PDFormFieldAdditionalActions actions) {
        this.dictionary.setItem(COSName.AA, actions);
    }
}
