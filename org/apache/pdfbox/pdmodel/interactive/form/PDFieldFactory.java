// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.interactive.form;

import java.util.List;
import org.apache.pdfbox.pdmodel.interactive.annotation.PDAnnotationWidget;
import java.io.IOException;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.cos.COSArray;
import org.apache.pdfbox.cos.COSDictionary;

public class PDFieldFactory
{
    private static final int RADIO_BITMASK = 32768;
    private static final int PUSHBUTTON_BITMASK = 65536;
    private static final int RADIOS_IN_UNISON_BITMASK = 33554432;
    private static final String FIELD_TYPE_BTN = "Btn";
    private static final String FIELD_TYPE_TX = "Tx";
    private static final String FIELD_TYPE_CH = "Ch";
    private static final String FIELD_TYPE_SIG = "Sig";
    
    private PDFieldFactory() {
    }
    
    public static PDField createField(final PDAcroForm acroForm, final COSDictionary field) throws IOException {
        PDField pdField = new PDUnknownField(acroForm, field);
        if (isButton(pdField)) {
            final int flags = pdField.getFieldFlags();
            final COSArray kids = (COSArray)field.getDictionaryObject(COSName.getPDFName("Kids"));
            if (kids != null || isRadio(flags)) {
                pdField = new PDRadioCollection(acroForm, field);
            }
            else if (isPushButton(flags)) {
                pdField = new PDPushButton(acroForm, field);
            }
            else {
                pdField = new PDCheckbox(acroForm, field);
            }
        }
        else if (isChoiceField(pdField)) {
            pdField = new PDChoiceField(acroForm, field);
        }
        else if (isTextbox(pdField)) {
            pdField = new PDTextbox(acroForm, field);
        }
        else if (isSignature(pdField)) {
            pdField = new PDSignatureField(acroForm, field);
        }
        return pdField;
    }
    
    private static boolean isRadio(final int flags) {
        return (flags & 0x8000) > 0;
    }
    
    private static boolean isPushButton(final int flags) {
        return (flags & 0x10000) > 0;
    }
    
    private static boolean isChoiceField(final PDField field) throws IOException {
        return "Ch".equals(field.findFieldType());
    }
    
    private static boolean isButton(final PDField field) throws IOException {
        final String ft = field.findFieldType();
        boolean retval = "Btn".equals(ft);
        final List kids = field.getKids();
        if (ft == null && kids != null && kids.size() > 0) {
            final Object obj = kids.get(0);
            COSDictionary kidDict = null;
            if (obj instanceof PDField) {
                kidDict = ((PDField)obj).getDictionary();
            }
            else {
                if (!(obj instanceof PDAnnotationWidget)) {
                    throw new IOException("Error:Unexpected type of kids field:" + obj);
                }
                kidDict = ((PDAnnotationWidget)obj).getDictionary();
            }
            retval = isButton(new PDUnknownField(field.getAcroForm(), kidDict));
        }
        return retval;
    }
    
    private static boolean isSignature(final PDField field) throws IOException {
        return "Sig".equals(field.findFieldType());
    }
    
    private static boolean isTextbox(final PDField field) throws IOException {
        return "Tx".equals(field.findFieldType());
    }
}
