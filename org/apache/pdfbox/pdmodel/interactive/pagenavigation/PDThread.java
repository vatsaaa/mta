// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.interactive.pagenavigation;

import org.apache.pdfbox.pdmodel.PDDocumentInformation;
import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSDictionary;
import org.apache.pdfbox.pdmodel.common.COSObjectable;

public class PDThread implements COSObjectable
{
    private COSDictionary thread;
    
    public PDThread(final COSDictionary t) {
        this.thread = t;
    }
    
    public PDThread() {
        (this.thread = new COSDictionary()).setName("Type", "Thread");
    }
    
    public COSDictionary getDictionary() {
        return this.thread;
    }
    
    public COSBase getCOSObject() {
        return this.thread;
    }
    
    public PDDocumentInformation getThreadInfo() {
        PDDocumentInformation retval = null;
        final COSDictionary info = (COSDictionary)this.thread.getDictionaryObject("I");
        if (info != null) {
            retval = new PDDocumentInformation(info);
        }
        return retval;
    }
    
    public void setThreadInfo(final PDDocumentInformation info) {
        this.thread.setItem("I", info);
    }
    
    public PDThreadBead getFirstBead() {
        PDThreadBead retval = null;
        final COSDictionary bead = (COSDictionary)this.thread.getDictionaryObject("F");
        if (bead != null) {
            retval = new PDThreadBead(bead);
        }
        return retval;
    }
    
    public void setFirstBead(final PDThreadBead bead) {
        if (bead != null) {
            bead.setThread(this);
        }
        this.thread.setItem("F", bead);
    }
}
