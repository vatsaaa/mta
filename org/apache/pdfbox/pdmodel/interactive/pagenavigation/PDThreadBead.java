// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.interactive.pagenavigation;

import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.cos.COSArray;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSDictionary;
import org.apache.pdfbox.pdmodel.common.COSObjectable;

public class PDThreadBead implements COSObjectable
{
    private COSDictionary bead;
    
    public PDThreadBead(final COSDictionary b) {
        this.bead = b;
    }
    
    public PDThreadBead() {
        (this.bead = new COSDictionary()).setName("Type", "Bead");
        this.setNextBead(this);
        this.setPreviousBead(this);
    }
    
    public COSDictionary getDictionary() {
        return this.bead;
    }
    
    public COSBase getCOSObject() {
        return this.bead;
    }
    
    public PDThread getThread() {
        PDThread retval = null;
        final COSDictionary dic = (COSDictionary)this.bead.getDictionaryObject("T");
        if (dic != null) {
            retval = new PDThread(dic);
        }
        return retval;
    }
    
    public void setThread(final PDThread thread) {
        this.bead.setItem("T", thread);
    }
    
    public PDThreadBead getNextBead() {
        return new PDThreadBead((COSDictionary)this.bead.getDictionaryObject("N"));
    }
    
    protected void setNextBead(final PDThreadBead next) {
        this.bead.setItem("N", next);
    }
    
    public PDThreadBead getPreviousBead() {
        return new PDThreadBead((COSDictionary)this.bead.getDictionaryObject("V"));
    }
    
    protected void setPreviousBead(final PDThreadBead previous) {
        this.bead.setItem("V", previous);
    }
    
    public void appendBead(final PDThreadBead append) {
        final PDThreadBead nextBead = this.getNextBead();
        nextBead.setPreviousBead(append);
        append.setNextBead(nextBead);
        this.setNextBead(append);
        append.setPreviousBead(this);
    }
    
    public PDPage getPage() {
        PDPage page = null;
        final COSDictionary dic = (COSDictionary)this.bead.getDictionaryObject("P");
        if (dic != null) {
            page = new PDPage(dic);
        }
        return page;
    }
    
    public void setPage(final PDPage page) {
        this.bead.setItem("P", page);
    }
    
    public PDRectangle getRectangle() {
        PDRectangle rect = null;
        final COSArray array = (COSArray)this.bead.getDictionaryObject(COSName.R);
        if (array != null) {
            rect = new PDRectangle(array);
        }
        return rect;
    }
    
    public void setRectangle(final PDRectangle rect) {
        this.bead.setItem(COSName.R, rect);
    }
}
