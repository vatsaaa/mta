// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.interactive.measurement;

import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.cos.COSDictionary;
import org.apache.pdfbox.pdmodel.common.COSObjectable;

public class PDNumberFormatDictionary implements COSObjectable
{
    public static final String TYPE = "NumberFormat";
    public static final String LABEL_SUFFIX_TO_VALUE = "S";
    public static final String LABEL_PREFIX_TO_VALUE = "P";
    public static final String FRACTIONAL_DISPLAY_DECIMAL = "D";
    public static final String FRACTIONAL_DISPLAY_FRACTION = "F";
    public static final String FRACTIONAL_DISPLAY_ROUND = "R";
    public static final String FRACTIONAL_DISPLAY_TRUNCATE = "T";
    private COSDictionary numberFormatDictionary;
    
    public PDNumberFormatDictionary() {
        (this.numberFormatDictionary = new COSDictionary()).setName(COSName.TYPE, "NumberFormat");
    }
    
    public PDNumberFormatDictionary(final COSDictionary dictionary) {
        this.numberFormatDictionary = dictionary;
    }
    
    public COSBase getCOSObject() {
        return this.numberFormatDictionary;
    }
    
    public COSDictionary getDictionary() {
        return this.numberFormatDictionary;
    }
    
    public String getType() {
        return "NumberFormat";
    }
    
    public String getUnits() {
        return this.getDictionary().getString("U");
    }
    
    public void setUnits(final String units) {
        this.getDictionary().setString("U", units);
    }
    
    public float getConversionFactor() {
        return this.getDictionary().getFloat("C");
    }
    
    public void setConversionFactor(final float conversionFactor) {
        this.getDictionary().setFloat("C", conversionFactor);
    }
    
    public String getFractionalDisplay() {
        return this.getDictionary().getString("F", "D");
    }
    
    public void setFractionalDisplay(final String fractionalDisplay) {
        if (fractionalDisplay == null || "D".equals(fractionalDisplay) || "F".equals(fractionalDisplay) || "R".equals(fractionalDisplay) || "T".equals(fractionalDisplay)) {
            this.getDictionary().setString("F", fractionalDisplay);
            return;
        }
        throw new IllegalArgumentException("Value must be \"D\", \"F\", \"R\", or \"T\", (or null).");
    }
    
    public int getDenominator() {
        return this.getDictionary().getInt("D");
    }
    
    public void setDenominator(final int denominator) {
        this.getDictionary().setInt("D", denominator);
    }
    
    public boolean isFD() {
        return this.getDictionary().getBoolean("FD", false);
    }
    
    public void setFD(final boolean fd) {
        this.getDictionary().setBoolean("FD", fd);
    }
    
    public String getThousandsSeparator() {
        return this.getDictionary().getString("RT", ",");
    }
    
    public void setThousandsSeparator(final String thousandsSeparator) {
        this.getDictionary().setString("RT", thousandsSeparator);
    }
    
    public String getDecimalSeparator() {
        return this.getDictionary().getString("RD", ".");
    }
    
    public void setDecimalSeparator(final String decimalSeparator) {
        this.getDictionary().setString("RD", decimalSeparator);
    }
    
    public String getLabelPrefixString() {
        return this.getDictionary().getString("PS", " ");
    }
    
    public void setLabelPrefixString(final String labelPrefixString) {
        this.getDictionary().setString("PS", labelPrefixString);
    }
    
    public String getLabelSuffixString() {
        return this.getDictionary().getString("SS", " ");
    }
    
    public void setLabelSuffixString(final String labelSuffixString) {
        this.getDictionary().setString("SS", labelSuffixString);
    }
    
    public String getLabelPositionToValue() {
        return this.getDictionary().getString("O", "S");
    }
    
    public void setLabelPositionToValue(final String labelPositionToValue) {
        if (labelPositionToValue == null || "P".equals(labelPositionToValue) || "S".equals(labelPositionToValue)) {
            this.getDictionary().setString("O", labelPositionToValue);
            return;
        }
        throw new IllegalArgumentException("Value must be \"S\", or \"P\" (or null).");
    }
}
