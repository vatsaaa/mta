// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.interactive.measurement;

import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.cos.COSArray;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSDictionary;
import org.apache.pdfbox.pdmodel.common.COSObjectable;

public class PDViewportDictionary implements COSObjectable
{
    public static final String TYPE = "Viewport";
    private COSDictionary viewportDictionary;
    
    public PDViewportDictionary() {
        this.viewportDictionary = new COSDictionary();
    }
    
    public PDViewportDictionary(final COSDictionary dictionary) {
        this.viewportDictionary = dictionary;
    }
    
    public COSBase getCOSObject() {
        return this.viewportDictionary;
    }
    
    public COSDictionary getDictionary() {
        return this.viewportDictionary;
    }
    
    public String getType() {
        return "Viewport";
    }
    
    public PDRectangle getBBox() {
        final COSArray bbox = (COSArray)this.getDictionary().getDictionaryObject("BBox");
        if (bbox != null) {
            return new PDRectangle(bbox);
        }
        return null;
    }
    
    public void setBBox(final PDRectangle rectangle) {
        this.getDictionary().setItem("BBox", rectangle);
    }
    
    public String getName() {
        return this.getDictionary().getNameAsString(COSName.NAME);
    }
    
    public void setName(final String name) {
        this.getDictionary().setName(COSName.NAME, name);
    }
    
    public PDMeasureDictionary getMeasure() {
        final COSDictionary measure = (COSDictionary)this.getDictionary().getDictionaryObject("Measure");
        if (measure != null) {
            return new PDMeasureDictionary(measure);
        }
        return null;
    }
    
    public void setMeasure(final PDMeasureDictionary measure) {
        this.getDictionary().setItem("Measure", measure);
    }
}
