// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.interactive.measurement;

import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.cos.COSDictionary;
import org.apache.pdfbox.pdmodel.common.COSObjectable;

public class PDMeasureDictionary implements COSObjectable
{
    public static final String TYPE = "Measure";
    private COSDictionary measureDictionary;
    
    protected PDMeasureDictionary() {
        this.measureDictionary = new COSDictionary();
        this.getDictionary().setName(COSName.TYPE, "Measure");
    }
    
    public PDMeasureDictionary(final COSDictionary dictionary) {
        this.measureDictionary = dictionary;
    }
    
    public COSBase getCOSObject() {
        return this.measureDictionary;
    }
    
    public COSDictionary getDictionary() {
        return this.measureDictionary;
    }
    
    public String getType() {
        return "Measure";
    }
    
    public String getSubtype() {
        return this.getDictionary().getNameAsString(COSName.SUBTYPE, "RL");
    }
    
    protected void setSubtype(final String subtype) {
        this.getDictionary().setName(COSName.SUBTYPE, subtype);
    }
}
