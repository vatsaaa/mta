// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.interactive.measurement;

import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.pdmodel.common.COSObjectable;
import org.apache.pdfbox.cos.COSArray;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.cos.COSDictionary;

public class PDRectlinearMeasureDictionary extends PDMeasureDictionary
{
    public static final String SUBTYPE = "RL";
    
    public PDRectlinearMeasureDictionary() {
        this.setSubtype("RL");
    }
    
    public PDRectlinearMeasureDictionary(final COSDictionary dictionary) {
        super(dictionary);
    }
    
    public String getScaleRatio() {
        return this.getDictionary().getString(COSName.R);
    }
    
    public void setScaleRatio(final String scaleRatio) {
        this.getDictionary().setString(COSName.R, scaleRatio);
    }
    
    public PDNumberFormatDictionary[] getChangeXs() {
        final COSArray x = (COSArray)this.getDictionary().getDictionaryObject("X");
        if (x != null) {
            final PDNumberFormatDictionary[] retval = new PDNumberFormatDictionary[x.size()];
            for (int i = 0; i < x.size(); ++i) {
                final COSDictionary dic = (COSDictionary)x.get(i);
                retval[i] = new PDNumberFormatDictionary(dic);
            }
            return retval;
        }
        return null;
    }
    
    public void setChangeXs(final PDNumberFormatDictionary[] changeXs) {
        final COSArray array = new COSArray();
        for (int i = 0; i < changeXs.length; ++i) {
            array.add(changeXs[i]);
        }
        this.getDictionary().setItem("X", array);
    }
    
    public PDNumberFormatDictionary[] getChangeYs() {
        final COSArray y = (COSArray)this.getDictionary().getDictionaryObject("Y");
        if (y != null) {
            final PDNumberFormatDictionary[] retval = new PDNumberFormatDictionary[y.size()];
            for (int i = 0; i < y.size(); ++i) {
                final COSDictionary dic = (COSDictionary)y.get(i);
                retval[i] = new PDNumberFormatDictionary(dic);
            }
            return retval;
        }
        return null;
    }
    
    public void setChangeYs(final PDNumberFormatDictionary[] changeYs) {
        final COSArray array = new COSArray();
        for (int i = 0; i < changeYs.length; ++i) {
            array.add(changeYs[i]);
        }
        this.getDictionary().setItem("Y", array);
    }
    
    public PDNumberFormatDictionary[] getDistances() {
        final COSArray d = (COSArray)this.getDictionary().getDictionaryObject("D");
        if (d != null) {
            final PDNumberFormatDictionary[] retval = new PDNumberFormatDictionary[d.size()];
            for (int i = 0; i < d.size(); ++i) {
                final COSDictionary dic = (COSDictionary)d.get(i);
                retval[i] = new PDNumberFormatDictionary(dic);
            }
            return retval;
        }
        return null;
    }
    
    public void setDistances(final PDNumberFormatDictionary[] distances) {
        final COSArray array = new COSArray();
        for (int i = 0; i < distances.length; ++i) {
            array.add(distances[i]);
        }
        this.getDictionary().setItem("D", array);
    }
    
    public PDNumberFormatDictionary[] getAreas() {
        final COSArray a = (COSArray)this.getDictionary().getDictionaryObject(COSName.A);
        if (a != null) {
            final PDNumberFormatDictionary[] retval = new PDNumberFormatDictionary[a.size()];
            for (int i = 0; i < a.size(); ++i) {
                final COSDictionary dic = (COSDictionary)a.get(i);
                retval[i] = new PDNumberFormatDictionary(dic);
            }
            return retval;
        }
        return null;
    }
    
    public void setAreas(final PDNumberFormatDictionary[] areas) {
        final COSArray array = new COSArray();
        for (int i = 0; i < areas.length; ++i) {
            array.add(areas[i]);
        }
        this.getDictionary().setItem(COSName.A, array);
    }
    
    public PDNumberFormatDictionary[] getAngles() {
        final COSArray t = (COSArray)this.getDictionary().getDictionaryObject("T");
        if (t != null) {
            final PDNumberFormatDictionary[] retval = new PDNumberFormatDictionary[t.size()];
            for (int i = 0; i < t.size(); ++i) {
                final COSDictionary dic = (COSDictionary)t.get(i);
                retval[i] = new PDNumberFormatDictionary(dic);
            }
            return retval;
        }
        return null;
    }
    
    public void setAngles(final PDNumberFormatDictionary[] angles) {
        final COSArray array = new COSArray();
        for (int i = 0; i < angles.length; ++i) {
            array.add(angles[i]);
        }
        this.getDictionary().setItem("T", array);
    }
    
    public PDNumberFormatDictionary[] getLineSloaps() {
        final COSArray s = (COSArray)this.getDictionary().getDictionaryObject("S");
        if (s != null) {
            final PDNumberFormatDictionary[] retval = new PDNumberFormatDictionary[s.size()];
            for (int i = 0; i < s.size(); ++i) {
                final COSDictionary dic = (COSDictionary)s.get(i);
                retval[i] = new PDNumberFormatDictionary(dic);
            }
            return retval;
        }
        return null;
    }
    
    public void setLineSloaps(final PDNumberFormatDictionary[] lineSloaps) {
        final COSArray array = new COSArray();
        for (int i = 0; i < lineSloaps.length; ++i) {
            array.add(lineSloaps[i]);
        }
        this.getDictionary().setItem("S", array);
    }
    
    public float[] getCoordSystemOrigin() {
        final COSArray o = (COSArray)this.getDictionary().getDictionaryObject("O");
        if (o != null) {
            return o.toFloatArray();
        }
        return null;
    }
    
    public void setCoordSystemOrigin(final float[] coordSystemOrigin) {
        final COSArray array = new COSArray();
        array.setFloatArray(coordSystemOrigin);
        this.getDictionary().setItem("O", array);
    }
    
    public float getCYX() {
        return this.getDictionary().getFloat("CYX");
    }
    
    public void setCYX(final float cyx) {
        this.getDictionary().setFloat("CYX", cyx);
    }
}
