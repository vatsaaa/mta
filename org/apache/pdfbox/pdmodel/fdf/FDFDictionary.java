// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.fdf;

import org.apache.pdfbox.cos.COSStream;
import org.apache.pdfbox.pdmodel.common.COSArrayList;
import org.apache.pdfbox.cos.COSName;
import java.io.Writer;
import java.util.List;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import java.io.IOException;
import java.util.ArrayList;
import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSString;
import org.apache.pdfbox.cos.COSArray;
import org.apache.pdfbox.pdmodel.common.filespecification.PDFileSpecification;
import org.apache.pdfbox.pdmodel.common.filespecification.PDSimpleFileSpecification;
import org.w3c.dom.Element;
import org.apache.pdfbox.cos.COSDictionary;
import org.apache.pdfbox.pdmodel.common.COSObjectable;

public class FDFDictionary implements COSObjectable
{
    private COSDictionary fdf;
    
    public FDFDictionary() {
        this.fdf = new COSDictionary();
    }
    
    public FDFDictionary(final COSDictionary fdfDictionary) {
        this.fdf = fdfDictionary;
    }
    
    public FDFDictionary(final Element fdfXML) throws IOException {
        this();
        final NodeList nodeList = fdfXML.getChildNodes();
        for (int i = 0; i < nodeList.getLength(); ++i) {
            final Node node = nodeList.item(i);
            if (node instanceof Element) {
                final Element child = (Element)node;
                if (child.getTagName().equals("f")) {
                    final PDSimpleFileSpecification fs = new PDSimpleFileSpecification();
                    fs.setFile(child.getAttribute("href"));
                    this.setFile(fs);
                }
                else if (child.getTagName().equals("ids")) {
                    final COSArray ids = new COSArray();
                    final String original = child.getAttribute("original");
                    final String modified = child.getAttribute("modified");
                    ids.add(COSString.createFromHexString(original));
                    ids.add(COSString.createFromHexString(modified));
                    this.setID(ids);
                }
                else if (child.getTagName().equals("fields")) {
                    final NodeList fields = child.getChildNodes();
                    final List fieldList = new ArrayList();
                    for (int f = 0; f < fields.getLength(); ++f) {
                        fieldList.add(new FDFField((Element)fields.item(f)));
                        final Node currentNode = fields.item(f);
                        if (currentNode instanceof Element && ((Element)currentNode).getTagName().equals("field")) {
                            fieldList.add(new FDFField((Element)fields.item(f)));
                        }
                    }
                    this.setFields(fieldList);
                }
                else if (child.getTagName().equals("annots")) {
                    final NodeList annots = child.getChildNodes();
                    final List annotList = new ArrayList();
                    for (int j = 0; j < annots.getLength(); ++j) {
                        final Node annotNode = annots.item(i);
                        if (annotNode instanceof Element) {
                            final Element annot = (Element)annotNode;
                            if (!annot.getNodeName().equals("text")) {
                                throw new IOException("Error: Unknown annotation type '" + annot.getNodeName());
                            }
                            annotList.add(new FDFAnnotationText(annot));
                        }
                    }
                    this.setAnnotations(annotList);
                }
            }
        }
    }
    
    public void writeXML(final Writer output) throws IOException {
        final PDFileSpecification fs = this.getFile();
        if (fs != null) {
            output.write("<f href=\"" + fs.getFile() + "\" />\n");
        }
        final COSArray ids = this.getID();
        if (ids != null) {
            final COSString original = (COSString)ids.getObject(0);
            final COSString modified = (COSString)ids.getObject(1);
            output.write("<ids original=\"" + original.getHexString() + "\" ");
            output.write("modified=\"" + modified.getHexString() + "\" />\n");
        }
        final List fields = this.getFields();
        if (fields != null && fields.size() > 0) {
            output.write("<fields>\n");
            for (int i = 0; i < fields.size(); ++i) {
                fields.get(i).writeXML(output);
            }
            output.write("</fields>\n");
        }
    }
    
    public COSBase getCOSObject() {
        return this.fdf;
    }
    
    public COSDictionary getCOSDictionary() {
        return this.fdf;
    }
    
    public PDFileSpecification getFile() throws IOException {
        return PDFileSpecification.createFS(this.fdf.getDictionaryObject(COSName.F));
    }
    
    public void setFile(final PDFileSpecification fs) {
        this.fdf.setItem(COSName.F, fs);
    }
    
    public COSArray getID() {
        return (COSArray)this.fdf.getDictionaryObject(COSName.ID);
    }
    
    public void setID(final COSArray id) {
        this.fdf.setItem(COSName.ID, id);
    }
    
    public List getFields() {
        List retval = null;
        final COSArray fieldArray = (COSArray)this.fdf.getDictionaryObject(COSName.FIELDS);
        if (fieldArray != null) {
            final List<FDFField> fields = new ArrayList<FDFField>();
            for (int i = 0; i < fieldArray.size(); ++i) {
                fields.add(new FDFField((COSDictionary)fieldArray.getObject(i)));
            }
            retval = new COSArrayList(fields, fieldArray);
        }
        return retval;
    }
    
    public void setFields(final List fields) {
        this.fdf.setItem(COSName.FIELDS, COSArrayList.converterToCOSArray(fields));
    }
    
    public String getStatus() {
        return this.fdf.getString(COSName.STATUS);
    }
    
    public void setStatus(final String status) {
        this.fdf.setString(COSName.STATUS, status);
    }
    
    public List getPages() {
        List retval = null;
        final COSArray pageArray = (COSArray)this.fdf.getDictionaryObject(COSName.PAGES);
        if (pageArray != null) {
            final List<FDFPage> pages = new ArrayList<FDFPage>();
            for (int i = 0; i < pageArray.size(); ++i) {
                pages.add(new FDFPage((COSDictionary)pageArray.get(i)));
            }
            retval = new COSArrayList(pages, pageArray);
        }
        return retval;
    }
    
    public void setPages(final List pages) {
        this.fdf.setItem(COSName.PAGES, COSArrayList.converterToCOSArray(pages));
    }
    
    public String getEncoding() {
        String encoding = this.fdf.getNameAsString(COSName.ENCODING);
        if (encoding == null) {
            encoding = "PDFDocEncoding";
        }
        return encoding;
    }
    
    public void setEncoding(final String encoding) {
        this.fdf.setName(COSName.ENCODING, encoding);
    }
    
    public List getAnnotations() throws IOException {
        List retval = null;
        final COSArray annotArray = (COSArray)this.fdf.getDictionaryObject(COSName.ANNOTS);
        if (annotArray != null) {
            final List<FDFAnnotation> annots = new ArrayList<FDFAnnotation>();
            for (int i = 0; i < annotArray.size(); ++i) {
                annots.add(FDFAnnotation.create((COSDictionary)annotArray.getObject(i)));
            }
            retval = new COSArrayList(annots, annotArray);
        }
        return retval;
    }
    
    public void setAnnotations(final List annots) {
        this.fdf.setItem(COSName.ANNOTS, COSArrayList.converterToCOSArray(annots));
    }
    
    public COSStream getDifferences() {
        return (COSStream)this.fdf.getDictionaryObject(COSName.DIFFERENCES);
    }
    
    public void setDifferences(final COSStream diff) {
        this.fdf.setItem(COSName.DIFFERENCES, diff);
    }
    
    public String getTarget() {
        return this.fdf.getString(COSName.TARGET);
    }
    
    public void setTarget(final String target) {
        this.fdf.setString(COSName.TARGET, target);
    }
    
    public List getEmbeddedFDFs() throws IOException {
        List retval = null;
        final COSArray embeddedArray = (COSArray)this.fdf.getDictionaryObject(COSName.EMBEDDED_FDFS);
        if (embeddedArray != null) {
            final List<PDFileSpecification> embedded = new ArrayList<PDFileSpecification>();
            for (int i = 0; i < embeddedArray.size(); ++i) {
                embedded.add(PDFileSpecification.createFS(embeddedArray.get(i)));
            }
            retval = new COSArrayList(embedded, embeddedArray);
        }
        return retval;
    }
    
    public void setEmbeddedFDFs(final List embedded) {
        this.fdf.setItem(COSName.EMBEDDED_FDFS, COSArrayList.converterToCOSArray(embedded));
    }
    
    public FDFJavaScript getJavaScript() {
        FDFJavaScript fs = null;
        final COSDictionary dic = (COSDictionary)this.fdf.getDictionaryObject(COSName.JAVA_SCRIPT);
        if (dic != null) {
            fs = new FDFJavaScript(dic);
        }
        return fs;
    }
    
    public void setJavaScript(final FDFJavaScript js) {
        this.fdf.setItem(COSName.JAVA_SCRIPT, js);
    }
}
