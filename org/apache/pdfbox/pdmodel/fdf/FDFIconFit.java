// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.fdf;

import org.apache.pdfbox.cos.COSArray;
import org.apache.pdfbox.pdmodel.common.PDRange;
import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSDictionary;
import org.apache.pdfbox.pdmodel.common.COSObjectable;

public class FDFIconFit implements COSObjectable
{
    private COSDictionary fit;
    public static final String SCALE_OPTION_ALWAYS = "A";
    public static final String SCALE_OPTION_ONLY_WHEN_ICON_IS_BIGGER = "B";
    public static final String SCALE_OPTION_ONLY_WHEN_ICON_IS_SMALLER = "S";
    public static final String SCALE_OPTION_NEVER = "N";
    public static final String SCALE_TYPE_ANAMORPHIC = "A";
    public static final String SCALE_TYPE_PROPORTIONAL = "P";
    
    public FDFIconFit() {
        this.fit = new COSDictionary();
    }
    
    public FDFIconFit(final COSDictionary f) {
        this.fit = f;
    }
    
    public COSBase getCOSObject() {
        return this.fit;
    }
    
    public COSDictionary getCOSDictionary() {
        return this.fit;
    }
    
    public String getScaleOption() {
        String retval = this.fit.getNameAsString("SW");
        if (retval == null) {
            retval = "A";
        }
        return retval;
    }
    
    public void setScaleOption(final String option) {
        this.fit.setName("SW", option);
    }
    
    public String getScaleType() {
        String retval = this.fit.getNameAsString("S");
        if (retval == null) {
            retval = "P";
        }
        return retval;
    }
    
    public void setScaleType(final String scale) {
        this.fit.setName("S", scale);
    }
    
    public PDRange getFractionalSpaceToAllocate() {
        PDRange retval = null;
        final COSArray array = (COSArray)this.fit.getDictionaryObject("A");
        if (array == null) {
            retval = new PDRange();
            retval.setMin(0.5f);
            retval.setMax(0.5f);
            this.setFractionalSpaceToAllocate(retval);
        }
        else {
            retval = new PDRange(array);
        }
        return retval;
    }
    
    public void setFractionalSpaceToAllocate(final PDRange space) {
        this.fit.setItem("A", space);
    }
    
    public boolean shouldScaleToFitAnnotation() {
        return this.fit.getBoolean("FB", false);
    }
    
    public void setScaleToFitAnnotation(final boolean value) {
        this.fit.setBoolean("FB", value);
    }
}
