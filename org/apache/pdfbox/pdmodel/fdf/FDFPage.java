// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.fdf;

import org.apache.pdfbox.pdmodel.common.COSArrayList;
import java.util.ArrayList;
import org.apache.pdfbox.cos.COSArray;
import java.util.List;
import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSDictionary;
import org.apache.pdfbox.pdmodel.common.COSObjectable;

public class FDFPage implements COSObjectable
{
    private COSDictionary page;
    
    public FDFPage() {
        this.page = new COSDictionary();
    }
    
    public FDFPage(final COSDictionary p) {
        this.page = p;
    }
    
    public COSBase getCOSObject() {
        return this.page;
    }
    
    public COSDictionary getCOSDictionary() {
        return this.page;
    }
    
    public List getTemplates() {
        List retval = null;
        final COSArray array = (COSArray)this.page.getDictionaryObject("Templates");
        if (array != null) {
            final List objects = new ArrayList();
            for (int i = 0; i < array.size(); ++i) {
                objects.add(new FDFTemplate((COSDictionary)array.getObject(i)));
            }
            retval = new COSArrayList(objects, array);
        }
        return retval;
    }
    
    public void setTemplates(final List templates) {
        this.page.setItem("Templates", COSArrayList.converterToCOSArray(templates));
    }
    
    public FDFPageInfo getPageInfo() {
        FDFPageInfo retval = null;
        final COSDictionary dict = (COSDictionary)this.page.getDictionaryObject("Info");
        if (dict != null) {
            retval = new FDFPageInfo(dict);
        }
        return retval;
    }
    
    public void setPageInfo(final FDFPageInfo info) {
        this.page.setItem("Info", info);
    }
}
