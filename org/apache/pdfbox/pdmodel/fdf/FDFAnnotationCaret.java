// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.fdf;

import java.io.IOException;
import org.w3c.dom.Element;
import org.apache.pdfbox.cos.COSDictionary;
import org.apache.pdfbox.cos.COSName;

public class FDFAnnotationCaret extends FDFAnnotation
{
    public static final String SUBTYPE = "Caret";
    
    public FDFAnnotationCaret() {
        this.annot.setName(COSName.SUBTYPE, "Caret");
    }
    
    public FDFAnnotationCaret(final COSDictionary a) {
        super(a);
    }
    
    public FDFAnnotationCaret(final Element element) throws IOException {
        super(element);
        this.annot.setName(COSName.SUBTYPE, "Caret");
    }
}
