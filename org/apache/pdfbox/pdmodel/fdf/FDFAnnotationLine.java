// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.fdf;

import java.io.IOException;
import org.w3c.dom.Element;
import org.apache.pdfbox.cos.COSDictionary;
import org.apache.pdfbox.cos.COSName;

public class FDFAnnotationLine extends FDFAnnotation
{
    public static final String SUBTYPE = "Line";
    
    public FDFAnnotationLine() {
        this.annot.setName(COSName.SUBTYPE, "Line");
    }
    
    public FDFAnnotationLine(final COSDictionary a) {
        super(a);
    }
    
    public FDFAnnotationLine(final Element element) throws IOException {
        super(element);
        this.annot.setName(COSName.SUBTYPE, "Line");
    }
}
