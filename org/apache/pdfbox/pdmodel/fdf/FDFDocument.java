// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.fdf;

import java.io.BufferedWriter;
import java.io.FileWriter;
import org.apache.pdfbox.pdfwriter.COSWriter;
import org.apache.pdfbox.exceptions.COSVisitorException;
import java.io.OutputStream;
import java.io.FileOutputStream;
import org.apache.pdfbox.util.XMLUtil;
import org.apache.pdfbox.pdfparser.PDFParser;
import java.io.File;
import java.io.InputStream;
import java.io.BufferedInputStream;
import java.io.FileInputStream;
import org.apache.pdfbox.pdmodel.common.COSObjectable;
import org.apache.pdfbox.cos.COSName;
import java.io.Writer;
import org.w3c.dom.Element;
import org.w3c.dom.Document;
import java.io.IOException;
import org.apache.pdfbox.cos.COSDictionary;
import org.apache.pdfbox.cos.COSDocument;

public class FDFDocument
{
    private COSDocument document;
    
    public FDFDocument() throws IOException {
        (this.document = new COSDocument()).setHeaderString("%FDF-1.2");
        this.document.setTrailer(new COSDictionary());
        final FDFCatalog catalog = new FDFCatalog();
        this.setCatalog(catalog);
    }
    
    public FDFDocument(final COSDocument doc) {
        this.document = doc;
    }
    
    public FDFDocument(final Document doc) throws IOException {
        this();
        final Element xfdf = doc.getDocumentElement();
        if (!xfdf.getNodeName().equals("xfdf")) {
            throw new IOException("Error while importing xfdf document, root should be 'xfdf' and not '" + xfdf.getNodeName() + "'");
        }
        final FDFCatalog cat = new FDFCatalog(xfdf);
        this.setCatalog(cat);
    }
    
    public void writeXML(final Writer output) throws IOException {
        output.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
        output.write("<xfdf xmlns=\"http://ns.adobe.com/xfdf/\" xml:space=\"preserve\">\n");
        this.getCatalog().writeXML(output);
        output.write("</xfdf>\n");
    }
    
    public COSDocument getDocument() {
        return this.document;
    }
    
    public FDFCatalog getCatalog() {
        FDFCatalog retval = null;
        final COSDictionary trailer = this.document.getTrailer();
        final COSDictionary root = (COSDictionary)trailer.getDictionaryObject(COSName.ROOT);
        if (root == null) {
            retval = new FDFCatalog();
            this.setCatalog(retval);
        }
        else {
            retval = new FDFCatalog(root);
        }
        return retval;
    }
    
    public void setCatalog(final FDFCatalog cat) {
        final COSDictionary trailer = this.document.getTrailer();
        trailer.setItem(COSName.ROOT, cat);
    }
    
    public static FDFDocument load(final String filename) throws IOException {
        return load(new BufferedInputStream(new FileInputStream(filename)));
    }
    
    public static FDFDocument load(final File file) throws IOException {
        return load(new BufferedInputStream(new FileInputStream(file)));
    }
    
    public static FDFDocument load(final InputStream input) throws IOException {
        final PDFParser parser = new PDFParser(input);
        parser.parse();
        return parser.getFDFDocument();
    }
    
    public static FDFDocument loadXFDF(final String filename) throws IOException {
        return loadXFDF(new BufferedInputStream(new FileInputStream(filename)));
    }
    
    public static FDFDocument loadXFDF(final File file) throws IOException {
        return loadXFDF(new BufferedInputStream(new FileInputStream(file)));
    }
    
    public static FDFDocument loadXFDF(final InputStream input) throws IOException {
        final Document doc = XMLUtil.parse(input);
        return new FDFDocument(doc);
    }
    
    public void save(final File fileName) throws IOException, COSVisitorException {
        this.save(new FileOutputStream(fileName));
    }
    
    public void save(final String fileName) throws IOException, COSVisitorException {
        this.save(new FileOutputStream(fileName));
    }
    
    public void save(final OutputStream output) throws IOException, COSVisitorException {
        COSWriter writer = null;
        try {
            writer = new COSWriter(output);
            writer.write(this.document);
            writer.close();
        }
        finally {
            if (writer != null) {
                writer.close();
            }
        }
    }
    
    public void saveXFDF(final File fileName) throws IOException, COSVisitorException {
        this.saveXFDF(new BufferedWriter(new FileWriter(fileName)));
    }
    
    public void saveXFDF(final String fileName) throws IOException, COSVisitorException {
        this.saveXFDF(new BufferedWriter(new FileWriter(fileName)));
    }
    
    public void saveXFDF(final Writer output) throws IOException, COSVisitorException {
        try {
            this.writeXML(output);
        }
        finally {
            if (output != null) {
                output.close();
            }
        }
    }
    
    public void close() throws IOException {
        this.document.close();
    }
}
