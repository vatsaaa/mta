// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.fdf;

import org.apache.pdfbox.pdmodel.common.COSArrayList;
import java.util.ArrayList;
import org.apache.pdfbox.cos.COSArray;
import java.util.List;
import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSDictionary;
import org.apache.pdfbox.pdmodel.common.COSObjectable;

public class FDFTemplate implements COSObjectable
{
    private COSDictionary template;
    
    public FDFTemplate() {
        this.template = new COSDictionary();
    }
    
    public FDFTemplate(final COSDictionary t) {
        this.template = t;
    }
    
    public COSBase getCOSObject() {
        return this.template;
    }
    
    public COSDictionary getCOSDictionary() {
        return this.template;
    }
    
    public FDFNamedPageReference getTemplateReference() {
        FDFNamedPageReference retval = null;
        final COSDictionary dict = (COSDictionary)this.template.getDictionaryObject("TRef");
        if (dict != null) {
            retval = new FDFNamedPageReference(dict);
        }
        return retval;
    }
    
    public void setTemplateReference(final FDFNamedPageReference tRef) {
        this.template.setItem("TRef", tRef);
    }
    
    public List getFields() {
        List retval = null;
        final COSArray array = (COSArray)this.template.getDictionaryObject("Fields");
        if (array != null) {
            final List fields = new ArrayList();
            for (int i = 0; i < array.size(); ++i) {
                fields.add(new FDFField((COSDictionary)array.getObject(i)));
            }
            retval = new COSArrayList(fields, array);
        }
        return retval;
    }
    
    public void setFields(final List fields) {
        this.template.setItem("Fields", COSArrayList.converterToCOSArray(fields));
    }
    
    public boolean shouldRename() {
        return this.template.getBoolean("Rename", false);
    }
    
    public void setRename(final boolean value) {
        this.template.setBoolean("Rename", value);
    }
}
