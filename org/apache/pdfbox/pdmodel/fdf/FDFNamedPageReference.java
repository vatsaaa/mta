// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.fdf;

import java.io.IOException;
import org.apache.pdfbox.pdmodel.common.filespecification.PDFileSpecification;
import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSDictionary;
import org.apache.pdfbox.pdmodel.common.COSObjectable;

public class FDFNamedPageReference implements COSObjectable
{
    private COSDictionary ref;
    
    public FDFNamedPageReference() {
        this.ref = new COSDictionary();
    }
    
    public FDFNamedPageReference(final COSDictionary r) {
        this.ref = r;
    }
    
    public COSBase getCOSObject() {
        return this.ref;
    }
    
    public COSDictionary getCOSDictionary() {
        return this.ref;
    }
    
    public String getName() {
        return this.ref.getString("Name");
    }
    
    public void setName(final String name) {
        this.ref.setString("Name", name);
    }
    
    public PDFileSpecification getFileSpecification() throws IOException {
        return PDFileSpecification.createFS(this.ref.getDictionaryObject("F"));
    }
    
    public void setFileSpecification(final PDFileSpecification fs) {
        this.ref.setItem("F", fs);
    }
}
