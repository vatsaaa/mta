// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.fdf;

import java.io.IOException;
import org.w3c.dom.Element;
import org.apache.pdfbox.cos.COSDictionary;
import org.apache.pdfbox.cos.COSName;

public class FDFAnnotationStamp extends FDFAnnotation
{
    public static final String SUBTYPE = "Stamp";
    
    public FDFAnnotationStamp() {
        this.annot.setName(COSName.SUBTYPE, "Stamp");
    }
    
    public FDFAnnotationStamp(final COSDictionary a) {
        super(a);
    }
    
    public FDFAnnotationStamp(final Element element) throws IOException {
        super(element);
        this.annot.setName(COSName.SUBTYPE, "Stamp");
    }
}
