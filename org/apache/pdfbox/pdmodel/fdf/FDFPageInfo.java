// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.fdf;

import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSDictionary;
import org.apache.pdfbox.pdmodel.common.COSObjectable;

public class FDFPageInfo implements COSObjectable
{
    private COSDictionary pageInfo;
    
    public FDFPageInfo() {
        this.pageInfo = new COSDictionary();
    }
    
    public FDFPageInfo(final COSDictionary p) {
        this.pageInfo = p;
    }
    
    public COSBase getCOSObject() {
        return this.pageInfo;
    }
    
    public COSDictionary getCOSDictionary() {
        return this.pageInfo;
    }
}
