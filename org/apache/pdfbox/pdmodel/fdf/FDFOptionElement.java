// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.fdf;

import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSString;
import org.apache.pdfbox.cos.COSArray;
import org.apache.pdfbox.pdmodel.common.COSObjectable;

public class FDFOptionElement implements COSObjectable
{
    private COSArray option;
    
    public FDFOptionElement() {
        (this.option = new COSArray()).add(new COSString(""));
        this.option.add(new COSString(""));
    }
    
    public FDFOptionElement(final COSArray o) {
        this.option = o;
    }
    
    public COSBase getCOSObject() {
        return this.option;
    }
    
    public COSArray getCOSArray() {
        return this.option;
    }
    
    public String getOption() {
        return ((COSString)this.option.getObject(0)).getString();
    }
    
    public void setOption(final String opt) {
        this.option.set(0, new COSString(opt));
    }
    
    public String getDefaultAppearanceString() {
        return ((COSString)this.option.getObject(1)).getString();
    }
    
    public void setDefaultAppearanceString(final String da) {
        this.option.set(1, new COSString(da));
    }
}
