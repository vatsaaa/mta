// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.fdf;

import java.util.Calendar;
import org.apache.pdfbox.util.BitFlagHelper;
import org.apache.pdfbox.cos.COSNumber;
import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSName;
import java.io.IOException;
import org.apache.pdfbox.util.DateConverter;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.cos.COSArray;
import java.awt.Color;
import org.w3c.dom.Element;
import org.apache.pdfbox.cos.COSDictionary;
import org.apache.pdfbox.pdmodel.common.COSObjectable;

public abstract class FDFAnnotation implements COSObjectable
{
    protected COSDictionary annot;
    
    public FDFAnnotation() {
        (this.annot = new COSDictionary()).setName("Type", "Annot");
    }
    
    public FDFAnnotation(final COSDictionary a) {
        this.annot = a;
    }
    
    public FDFAnnotation(final Element element) throws IOException {
        this();
        final String page = element.getAttribute("page");
        if (page != null) {
            this.setPage(Integer.parseInt(page));
        }
        final String color = element.getAttribute("color");
        if (color != null && color.length() == 7 && color.charAt(0) == '#') {
            final int colorValue = Integer.parseInt(color.substring(1, 7), 16);
            this.setColor(new Color(colorValue));
        }
        this.setDate(element.getAttribute("date"));
        final String flags = element.getAttribute("flags");
        if (flags != null) {
            final String[] flagTokens = flags.split(",");
            for (int i = 0; i < flagTokens.length; ++i) {
                if (flagTokens[i].equals("invisible")) {
                    this.setInvisible(true);
                }
                else if (flagTokens[i].equals("hidden")) {
                    this.setHidden(true);
                }
                else if (flagTokens[i].equals("print")) {
                    this.setPrinted(true);
                }
                else if (flagTokens[i].equals("nozoom")) {
                    this.setNoZoom(true);
                }
                else if (flagTokens[i].equals("norotate")) {
                    this.setNoRotate(true);
                }
                else if (flagTokens[i].equals("noview")) {
                    this.setNoView(true);
                }
                else if (flagTokens[i].equals("readonly")) {
                    this.setReadOnly(true);
                }
                else if (flagTokens[i].equals("locked")) {
                    this.setLocked(true);
                }
                else if (flagTokens[i].equals("togglenoview")) {
                    this.setToggleNoView(true);
                }
            }
        }
        this.setName(element.getAttribute("name"));
        final String rect = element.getAttribute("rect");
        if (rect != null) {
            final String[] rectValues = rect.split(",");
            final float[] values = new float[rectValues.length];
            for (int j = 0; j < rectValues.length; ++j) {
                values[j] = Float.parseFloat(rectValues[j]);
            }
            final COSArray array = new COSArray();
            array.setFloatArray(values);
            this.setRectangle(new PDRectangle(array));
        }
        this.setName(element.getAttribute("title"));
        this.setCreationDate(DateConverter.toCalendar(element.getAttribute("creationdate")));
        final String opac = element.getAttribute("opacity");
        if (opac != null) {
            this.setOpacity(Float.parseFloat(opac));
        }
        this.setSubject(element.getAttribute("subject"));
    }
    
    public static FDFAnnotation create(final COSDictionary fdfDic) throws IOException {
        FDFAnnotation retval = null;
        if (fdfDic != null) {
            if (!"Text".equals(fdfDic.getNameAsString(COSName.SUBTYPE))) {
                throw new IOException("Unknown annotation type '" + fdfDic.getNameAsString(COSName.SUBTYPE) + "'");
            }
            retval = new FDFAnnotationText(fdfDic);
        }
        return retval;
    }
    
    public COSBase getCOSObject() {
        return this.annot;
    }
    
    public COSDictionary getCOSDictionary() {
        return this.annot;
    }
    
    public Integer getPage() {
        Integer retval = null;
        final COSNumber page = (COSNumber)this.annot.getDictionaryObject("Page");
        if (page != null) {
            retval = new Integer(page.intValue());
        }
        return retval;
    }
    
    public void setPage(final int page) {
        this.annot.setInt("Page", page);
    }
    
    public Color getColor() {
        Color retval = null;
        final COSArray array = (COSArray)this.annot.getDictionaryObject("color");
        if (array != null) {
            final float[] rgb = array.toFloatArray();
            if (rgb.length >= 3) {
                retval = new Color(rgb[0], rgb[1], rgb[2]);
            }
        }
        return retval;
    }
    
    public void setColor(final Color c) {
        COSArray color = null;
        if (c != null) {
            final float[] colors = c.getRGBColorComponents(null);
            color = new COSArray();
            color.setFloatArray(colors);
        }
        this.annot.setItem("color", color);
    }
    
    public String getDate() {
        return this.annot.getString("date");
    }
    
    public void setDate(final String date) {
        this.annot.setString("date", date);
    }
    
    public boolean isInvisible() {
        return BitFlagHelper.getFlag(this.annot, COSName.F, 1);
    }
    
    public void setInvisible(final boolean invisible) {
        BitFlagHelper.setFlag(this.annot, COSName.F, 1, invisible);
    }
    
    public boolean isHidden() {
        return BitFlagHelper.getFlag(this.annot, COSName.F, 2);
    }
    
    public void setHidden(final boolean hidden) {
        BitFlagHelper.setFlag(this.annot, COSName.F, 2, hidden);
    }
    
    public boolean isPrinted() {
        return BitFlagHelper.getFlag(this.annot, COSName.F, 4);
    }
    
    public void setPrinted(final boolean printed) {
        BitFlagHelper.setFlag(this.annot, COSName.F, 4, printed);
    }
    
    public boolean isNoZoom() {
        return BitFlagHelper.getFlag(this.annot, COSName.F, 8);
    }
    
    public void setNoZoom(final boolean noZoom) {
        BitFlagHelper.setFlag(this.annot, COSName.F, 8, noZoom);
    }
    
    public boolean isNoRotate() {
        return BitFlagHelper.getFlag(this.annot, COSName.F, 16);
    }
    
    public void setNoRotate(final boolean noRotate) {
        BitFlagHelper.setFlag(this.annot, COSName.F, 16, noRotate);
    }
    
    public boolean isNoView() {
        return BitFlagHelper.getFlag(this.annot, COSName.F, 32);
    }
    
    public void setNoView(final boolean noView) {
        BitFlagHelper.setFlag(this.annot, COSName.F, 32, noView);
    }
    
    public boolean isReadOnly() {
        return BitFlagHelper.getFlag(this.annot, COSName.F, 64);
    }
    
    public void setReadOnly(final boolean readOnly) {
        BitFlagHelper.setFlag(this.annot, COSName.F, 64, readOnly);
    }
    
    public boolean isLocked() {
        return BitFlagHelper.getFlag(this.annot, COSName.F, 128);
    }
    
    public void setLocked(final boolean locked) {
        BitFlagHelper.setFlag(this.annot, COSName.F, 128, locked);
    }
    
    public boolean isToggleNoView() {
        return BitFlagHelper.getFlag(this.annot, COSName.F, 256);
    }
    
    public void setToggleNoView(final boolean toggleNoView) {
        BitFlagHelper.setFlag(this.annot, COSName.F, 256, toggleNoView);
    }
    
    public void setName(final String name) {
        this.annot.setString(COSName.NM, name);
    }
    
    public String getName() {
        return this.annot.getString(COSName.NM);
    }
    
    public void setRectangle(final PDRectangle rectangle) {
        this.annot.setItem(COSName.RECT, rectangle);
    }
    
    public PDRectangle getRectangle() {
        PDRectangle retval = null;
        final COSArray rectArray = (COSArray)this.annot.getDictionaryObject(COSName.RECT);
        if (rectArray != null) {
            retval = new PDRectangle(rectArray);
        }
        return retval;
    }
    
    public void setTitle(final String title) {
        this.annot.setString(COSName.T, title);
    }
    
    public String getTitle() {
        return this.annot.getString(COSName.T);
    }
    
    public Calendar getCreationDate() throws IOException {
        return this.annot.getDate(COSName.CREATION_DATE);
    }
    
    public void setCreationDate(final Calendar date) {
        this.annot.setDate(COSName.CREATION_DATE, date);
    }
    
    public void setOpacity(final float opacity) {
        this.annot.setFloat(COSName.CA, opacity);
    }
    
    public float getOpacity() {
        return this.annot.getFloat(COSName.CA, 1.0f);
    }
    
    public void setSubject(final String subject) {
        this.annot.setString(COSName.SUBJ, subject);
    }
    
    public String getSubject() {
        return this.annot.getString(COSName.SUBJ);
    }
}
