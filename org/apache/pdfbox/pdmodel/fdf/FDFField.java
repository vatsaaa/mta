// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.fdf;

import org.apache.pdfbox.pdmodel.interactive.action.PDAdditionalActions;
import org.apache.pdfbox.pdmodel.interactive.action.PDActionFactory;
import org.apache.pdfbox.pdmodel.interactive.action.type.PDAction;
import org.apache.pdfbox.pdmodel.interactive.annotation.PDAppearanceDictionary;
import org.apache.pdfbox.cos.COSInteger;
import org.apache.pdfbox.cos.COSNumber;
import org.apache.pdfbox.cos.COSStream;
import org.apache.pdfbox.cos.COSString;
import org.apache.pdfbox.pdmodel.common.COSArrayList;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.cos.COSArray;
import org.apache.pdfbox.cos.COSBase;
import java.io.Writer;
import java.io.IOException;
import org.w3c.dom.Node;
import java.util.List;
import org.w3c.dom.NodeList;
import org.apache.pdfbox.pdmodel.common.PDTextStream;
import org.apache.pdfbox.util.XMLUtil;
import java.util.ArrayList;
import org.w3c.dom.Element;
import org.apache.pdfbox.cos.COSDictionary;
import org.apache.pdfbox.pdmodel.common.COSObjectable;

public class FDFField implements COSObjectable
{
    private COSDictionary field;
    
    public FDFField() {
        this.field = new COSDictionary();
    }
    
    public FDFField(final COSDictionary f) {
        this.field = f;
    }
    
    public FDFField(final Element fieldXML) throws IOException {
        this();
        this.setPartialFieldName(fieldXML.getAttribute("name"));
        final NodeList nodeList = fieldXML.getChildNodes();
        final List<FDFField> kids = new ArrayList<FDFField>();
        for (int i = 0; i < nodeList.getLength(); ++i) {
            final Node node = nodeList.item(i);
            if (node instanceof Element) {
                final Element child = (Element)node;
                if (child.getTagName().equals("value")) {
                    this.setValue(XMLUtil.getNodeValue(child));
                }
                else if (child.getTagName().equals("value-richtext")) {
                    this.setRichText(new PDTextStream(XMLUtil.getNodeValue(child)));
                }
                else if (child.getTagName().equals("field")) {
                    kids.add(new FDFField(child));
                }
            }
        }
        if (kids.size() > 0) {
            this.setKids(kids);
        }
    }
    
    public void writeXML(final Writer output) throws IOException {
        output.write("<field name=\"" + this.getPartialFieldName() + "\">\n");
        final Object value = this.getValue();
        if (value != null) {
            output.write("<value>" + value + "</value>\n");
        }
        final PDTextStream rt = this.getRichText();
        if (rt != null) {
            output.write("<value-richtext>" + rt.getAsString() + "</value-richtext>\n");
        }
        final List<FDFField> kids = this.getKids();
        if (kids != null) {
            for (int i = 0; i < kids.size(); ++i) {
                kids.get(i).writeXML(output);
            }
        }
        output.write("</field>\n");
    }
    
    public COSBase getCOSObject() {
        return this.field;
    }
    
    public COSDictionary getCOSDictionary() {
        return this.field;
    }
    
    public List<FDFField> getKids() {
        final COSArray kids = (COSArray)this.field.getDictionaryObject(COSName.KIDS);
        List<FDFField> retval = null;
        if (kids != null) {
            final List<FDFField> actuals = new ArrayList<FDFField>();
            for (int i = 0; i < kids.size(); ++i) {
                actuals.add(new FDFField((COSDictionary)kids.getObject(i)));
            }
            retval = (List<FDFField>)new COSArrayList(actuals, kids);
        }
        return retval;
    }
    
    public void setKids(final List<FDFField> kids) {
        this.field.setItem(COSName.KIDS, COSArrayList.converterToCOSArray(kids));
    }
    
    public String getPartialFieldName() {
        return this.field.getString(COSName.T);
    }
    
    public void setPartialFieldName(final String partial) {
        this.field.setString(COSName.T, partial);
    }
    
    public Object getValue() throws IOException {
        Object retval = null;
        final COSBase value = this.field.getDictionaryObject(COSName.V);
        if (value instanceof COSName) {
            retval = ((COSName)value).getName();
        }
        else if (value instanceof COSArray) {
            retval = COSArrayList.convertCOSStringCOSArrayToList((COSArray)value);
        }
        else if (value instanceof COSString || value instanceof COSStream) {
            retval = PDTextStream.createTextStream(value);
        }
        else if (value != null) {
            throw new IOException("Error:Unknown type for field import" + value);
        }
        return retval;
    }
    
    public void setValue(final Object value) throws IOException {
        COSBase cos = null;
        if (value instanceof List) {
            cos = COSArrayList.convertStringListToCOSStringCOSArray((List)value);
        }
        else if (value instanceof String) {
            cos = COSName.getPDFName((String)value);
        }
        else if (value instanceof COSObjectable) {
            cos = ((COSObjectable)value).getCOSObject();
        }
        else if (value != null) {
            throw new IOException("Error:Unknown type for field import" + value);
        }
        this.field.setItem(COSName.V, cos);
    }
    
    public Integer getFieldFlags() {
        Integer retval = null;
        final COSNumber ff = (COSNumber)this.field.getDictionaryObject(COSName.FF);
        if (ff != null) {
            retval = new Integer(ff.intValue());
        }
        return retval;
    }
    
    public void setFieldFlags(final Integer ff) {
        COSInteger value = null;
        if (ff != null) {
            value = COSInteger.get(ff);
        }
        this.field.setItem(COSName.FF, value);
    }
    
    public void setFieldFlags(final int ff) {
        this.field.setInt(COSName.FF, ff);
    }
    
    public Integer getSetFieldFlags() {
        Integer retval = null;
        final COSNumber ff = (COSNumber)this.field.getDictionaryObject(COSName.SET_FF);
        if (ff != null) {
            retval = new Integer(ff.intValue());
        }
        return retval;
    }
    
    public void setSetFieldFlags(final Integer ff) {
        COSInteger value = null;
        if (ff != null) {
            value = COSInteger.get(ff);
        }
        this.field.setItem(COSName.SET_FF, value);
    }
    
    public void setSetFieldFlags(final int ff) {
        this.field.setInt(COSName.SET_FF, ff);
    }
    
    public Integer getClearFieldFlags() {
        Integer retval = null;
        final COSNumber ff = (COSNumber)this.field.getDictionaryObject(COSName.CLR_FF);
        if (ff != null) {
            retval = new Integer(ff.intValue());
        }
        return retval;
    }
    
    public void setClearFieldFlags(final Integer ff) {
        COSInteger value = null;
        if (ff != null) {
            value = COSInteger.get(ff);
        }
        this.field.setItem(COSName.CLR_FF, value);
    }
    
    public void setClearFieldFlags(final int ff) {
        this.field.setInt(COSName.CLR_FF, ff);
    }
    
    public Integer getWidgetFieldFlags() {
        Integer retval = null;
        final COSNumber f = (COSNumber)this.field.getDictionaryObject("F");
        if (f != null) {
            retval = new Integer(f.intValue());
        }
        return retval;
    }
    
    public void setWidgetFieldFlags(final Integer f) {
        COSInteger value = null;
        if (f != null) {
            value = COSInteger.get(f);
        }
        this.field.setItem(COSName.F, value);
    }
    
    public void setWidgetFieldFlags(final int f) {
        this.field.setInt(COSName.F, f);
    }
    
    public Integer getSetWidgetFieldFlags() {
        Integer retval = null;
        final COSNumber ff = (COSNumber)this.field.getDictionaryObject(COSName.SET_F);
        if (ff != null) {
            retval = new Integer(ff.intValue());
        }
        return retval;
    }
    
    public void setSetWidgetFieldFlags(final Integer ff) {
        COSInteger value = null;
        if (ff != null) {
            value = COSInteger.get(ff);
        }
        this.field.setItem(COSName.SET_F, value);
    }
    
    public void setSetWidgetFieldFlags(final int ff) {
        this.field.setInt(COSName.SET_F, ff);
    }
    
    public Integer getClearWidgetFieldFlags() {
        Integer retval = null;
        final COSNumber ff = (COSNumber)this.field.getDictionaryObject(COSName.CLR_F);
        if (ff != null) {
            retval = new Integer(ff.intValue());
        }
        return retval;
    }
    
    public void setClearWidgetFieldFlags(final Integer ff) {
        COSInteger value = null;
        if (ff != null) {
            value = COSInteger.get(ff);
        }
        this.field.setItem(COSName.CLR_F, value);
    }
    
    public void setClearWidgetFieldFlags(final int ff) {
        this.field.setInt(COSName.CLR_F, ff);
    }
    
    public PDAppearanceDictionary getAppearanceDictionary() {
        PDAppearanceDictionary retval = null;
        final COSDictionary dict = (COSDictionary)this.field.getDictionaryObject(COSName.AP);
        if (dict != null) {
            retval = new PDAppearanceDictionary(dict);
        }
        return retval;
    }
    
    public void setAppearanceDictionary(final PDAppearanceDictionary ap) {
        this.field.setItem(COSName.AP, ap);
    }
    
    public FDFNamedPageReference getAppearanceStreamReference() {
        FDFNamedPageReference retval = null;
        final COSDictionary ref = (COSDictionary)this.field.getDictionaryObject(COSName.AP_REF);
        if (ref != null) {
            retval = new FDFNamedPageReference(ref);
        }
        return retval;
    }
    
    public void setAppearanceStreamReference(final FDFNamedPageReference ref) {
        this.field.setItem(COSName.AP_REF, ref);
    }
    
    public FDFIconFit getIconFit() {
        FDFIconFit retval = null;
        final COSDictionary dic = (COSDictionary)this.field.getDictionaryObject("IF");
        if (dic != null) {
            retval = new FDFIconFit(dic);
        }
        return retval;
    }
    
    public void setIconFit(final FDFIconFit fit) {
        this.field.setItem("IF", fit);
    }
    
    public List getOptions() {
        List retval = null;
        final COSArray array = (COSArray)this.field.getDictionaryObject(COSName.OPT);
        if (array != null) {
            final List objects = new ArrayList();
            for (int i = 0; i < array.size(); ++i) {
                final COSBase next = array.getObject(i);
                if (next instanceof COSString) {
                    objects.add(((COSString)next).getString());
                }
                else {
                    final COSArray value = (COSArray)next;
                    objects.add(new FDFOptionElement(value));
                }
            }
            retval = new COSArrayList(objects, array);
        }
        return retval;
    }
    
    public void setOptions(final List options) {
        final COSArray value = COSArrayList.converterToCOSArray(options);
        this.field.setItem(COSName.OPT, value);
    }
    
    public PDAction getAction() {
        return PDActionFactory.createAction((COSDictionary)this.field.getDictionaryObject(COSName.A));
    }
    
    public void setAction(final PDAction a) {
        this.field.setItem(COSName.A, a);
    }
    
    public PDAdditionalActions getAdditionalActions() {
        PDAdditionalActions retval = null;
        final COSDictionary dict = (COSDictionary)this.field.getDictionaryObject(COSName.AA);
        if (dict != null) {
            retval = new PDAdditionalActions(dict);
        }
        return retval;
    }
    
    public void setAdditionalActions(final PDAdditionalActions aa) {
        this.field.setItem(COSName.AA, aa);
    }
    
    public PDTextStream getRichText() {
        final COSBase rv = this.field.getDictionaryObject(COSName.RV);
        return PDTextStream.createTextStream(rv);
    }
    
    public void setRichText(final PDTextStream rv) {
        this.field.setItem(COSName.RV, rv);
    }
}
