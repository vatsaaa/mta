// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.fdf;

import java.io.IOException;
import org.w3c.dom.Element;
import org.apache.pdfbox.cos.COSDictionary;
import org.apache.pdfbox.cos.COSName;

public class FDFAnnotationPolygon extends FDFAnnotation
{
    public static final String SUBTYPE = "Polygon";
    
    public FDFAnnotationPolygon() {
        this.annot.setName(COSName.SUBTYPE, "Polygon");
    }
    
    public FDFAnnotationPolygon(final COSDictionary a) {
        super(a);
    }
    
    public FDFAnnotationPolygon(final Element element) throws IOException {
        super(element);
        this.annot.setName(COSName.SUBTYPE, "Polygon");
    }
}
