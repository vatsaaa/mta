// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.fdf;

import org.apache.pdfbox.pdmodel.common.COSArrayList;
import org.apache.pdfbox.pdmodel.common.PDNamedTextStream;
import org.apache.pdfbox.cos.COSName;
import java.util.ArrayList;
import org.apache.pdfbox.cos.COSArray;
import java.util.List;
import org.apache.pdfbox.pdmodel.common.PDTextStream;
import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSDictionary;
import org.apache.pdfbox.pdmodel.common.COSObjectable;

public class FDFJavaScript implements COSObjectable
{
    private COSDictionary js;
    
    public FDFJavaScript() {
        this.js = new COSDictionary();
    }
    
    public FDFJavaScript(final COSDictionary javaScript) {
        this.js = javaScript;
    }
    
    public COSBase getCOSObject() {
        return this.js;
    }
    
    public COSDictionary getCOSDictionary() {
        return this.js;
    }
    
    public PDTextStream getBefore() {
        return PDTextStream.createTextStream(this.js.getDictionaryObject("Before"));
    }
    
    public void setBefore(final PDTextStream before) {
        this.js.setItem("Before", before);
    }
    
    public PDTextStream getAfter() {
        return PDTextStream.createTextStream(this.js.getDictionaryObject("After"));
    }
    
    public void setAfter(final PDTextStream after) {
        this.js.setItem("After", after);
    }
    
    public List getNamedJavaScripts() {
        COSArray array = (COSArray)this.js.getDictionaryObject("Doc");
        final List namedStreams = new ArrayList();
        if (array == null) {
            array = new COSArray();
            this.js.setItem("Doc", array);
        }
        for (int i = 0; i < array.size(); ++i) {
            final COSName name = (COSName)array.get(i);
            ++i;
            final COSBase stream = array.get(i);
            final PDNamedTextStream namedStream = new PDNamedTextStream(name, stream);
            namedStreams.add(namedStream);
        }
        return new COSArrayList(namedStreams, array);
    }
    
    public void setNamedJavaScripts(final List namedStreams) {
        final COSArray array = COSArrayList.converterToCOSArray(namedStreams);
        this.js.setItem("Doc", array);
    }
}
