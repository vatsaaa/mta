// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.fdf;

import org.apache.pdfbox.pdmodel.interactive.digitalsignature.PDSignature;
import org.apache.pdfbox.cos.COSBase;
import java.io.Writer;
import java.io.IOException;
import org.w3c.dom.Element;
import org.apache.pdfbox.cos.COSDictionary;
import org.apache.pdfbox.pdmodel.common.COSObjectable;

public class FDFCatalog implements COSObjectable
{
    private COSDictionary catalog;
    
    public FDFCatalog() {
        this.catalog = new COSDictionary();
    }
    
    public FDFCatalog(final COSDictionary cat) {
        this.catalog = cat;
    }
    
    public FDFCatalog(final Element element) throws IOException {
        this();
        final FDFDictionary fdfDict = new FDFDictionary(element);
        this.setFDF(fdfDict);
    }
    
    public void writeXML(final Writer output) throws IOException {
        final FDFDictionary fdf = this.getFDF();
        fdf.writeXML(output);
    }
    
    public COSBase getCOSObject() {
        return this.catalog;
    }
    
    public COSDictionary getCOSDictionary() {
        return this.catalog;
    }
    
    public String getVersion() {
        return this.catalog.getNameAsString("Version");
    }
    
    public void setVersion(final String version) {
        this.catalog.setName("Version", version);
    }
    
    public FDFDictionary getFDF() {
        final COSDictionary fdf = (COSDictionary)this.catalog.getDictionaryObject("FDF");
        FDFDictionary retval = null;
        if (fdf != null) {
            retval = new FDFDictionary(fdf);
        }
        else {
            retval = new FDFDictionary();
            this.setFDF(retval);
        }
        return retval;
    }
    
    public void setFDF(final FDFDictionary fdf) {
        this.catalog.setItem("FDF", fdf);
    }
    
    public PDSignature getSignature() {
        PDSignature signature = null;
        final COSDictionary sig = (COSDictionary)this.catalog.getDictionaryObject("Sig");
        if (sig != null) {
            signature = new PDSignature(sig);
        }
        return signature;
    }
    
    public void setSignature(final PDSignature sig) {
        this.catalog.setItem("Sig", sig);
    }
}
