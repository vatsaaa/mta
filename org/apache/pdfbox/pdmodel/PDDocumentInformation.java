// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel;

import java.util.Iterator;
import java.util.TreeSet;
import java.util.Set;
import java.io.IOException;
import java.util.Calendar;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSDictionary;
import org.apache.pdfbox.pdmodel.common.COSObjectable;

public class PDDocumentInformation implements COSObjectable
{
    private COSDictionary info;
    
    public PDDocumentInformation() {
        this.info = new COSDictionary();
    }
    
    public PDDocumentInformation(final COSDictionary dic) {
        this.info = dic;
    }
    
    public COSDictionary getDictionary() {
        return this.info;
    }
    
    public COSBase getCOSObject() {
        return this.info;
    }
    
    public String getTitle() {
        return this.info.getString(COSName.TITLE);
    }
    
    public void setTitle(final String title) {
        this.info.setString(COSName.TITLE, title);
    }
    
    public String getAuthor() {
        return this.info.getString(COSName.AUTHOR);
    }
    
    public void setAuthor(final String author) {
        this.info.setString(COSName.AUTHOR, author);
    }
    
    public String getSubject() {
        return this.info.getString(COSName.SUBJECT);
    }
    
    public void setSubject(final String subject) {
        this.info.setString(COSName.SUBJECT, subject);
    }
    
    public String getKeywords() {
        return this.info.getString(COSName.KEYWORDS);
    }
    
    public void setKeywords(final String keywords) {
        this.info.setString(COSName.KEYWORDS, keywords);
    }
    
    public String getCreator() {
        return this.info.getString(COSName.CREATOR);
    }
    
    public void setCreator(final String creator) {
        this.info.setString(COSName.CREATOR, creator);
    }
    
    public String getProducer() {
        return this.info.getString(COSName.PRODUCER);
    }
    
    public void setProducer(final String producer) {
        this.info.setString(COSName.PRODUCER, producer);
    }
    
    public Calendar getCreationDate() throws IOException {
        return this.info.getDate(COSName.CREATION_DATE);
    }
    
    public void setCreationDate(final Calendar date) {
        this.info.setDate(COSName.CREATION_DATE, date);
    }
    
    public Calendar getModificationDate() throws IOException {
        return this.info.getDate(COSName.MOD_DATE);
    }
    
    public void setModificationDate(final Calendar date) {
        this.info.setDate(COSName.MOD_DATE, date);
    }
    
    public String getTrapped() {
        return this.info.getNameAsString(COSName.TRAPPED);
    }
    
    public Set<String> getMetadataKeys() {
        final Set<String> keys = new TreeSet<String>();
        for (final COSName key : this.info.keySet()) {
            keys.add(key.getName());
        }
        return keys;
    }
    
    public String getCustomMetadataValue(final String fieldName) {
        return this.info.getString(fieldName);
    }
    
    public void setCustomMetadataValue(final String fieldName, final String fieldValue) {
        this.info.setString(fieldName, fieldValue);
    }
    
    public void setTrapped(final String value) {
        if (value != null && !value.equals("True") && !value.equals("False") && !value.equals("Unknown")) {
            throw new RuntimeException("Valid values for trapped are 'True', 'False', or 'Unknown'");
        }
        this.info.setName(COSName.TRAPPED, value);
    }
}
