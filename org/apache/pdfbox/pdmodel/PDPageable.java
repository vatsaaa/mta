// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel;

import java.awt.Graphics;
import javax.print.PrintService;
import java.awt.print.Paper;
import java.awt.Dimension;
import javax.print.attribute.Attribute;
import javax.print.attribute.standard.OrientationRequested;
import java.awt.print.PageFormat;
import java.awt.print.PrinterException;
import java.util.ArrayList;
import java.awt.print.PrinterJob;
import java.util.List;
import java.awt.print.Printable;
import java.awt.print.Pageable;

public class PDPageable implements Pageable, Printable
{
    private final List<PDPage> pages;
    private final PrinterJob job;
    
    public PDPageable(final PDDocument document, final PrinterJob job) throws IllegalArgumentException, PrinterException {
        this.pages = new ArrayList<PDPage>();
        if (document == null || job == null) {
            throw new IllegalArgumentException("PDPageable(" + document + ", " + job + ")");
        }
        if (!document.getCurrentAccessPermission().canPrint()) {
            throw new PrinterException("You do not have permission to print this document");
        }
        document.getDocumentCatalog().getPages().getAllKids(this.pages);
        this.job = job;
    }
    
    public PDPageable(final PDDocument document) throws IllegalArgumentException, PrinterException {
        this(document, PrinterJob.getPrinterJob());
    }
    
    public PrinterJob getPrinterJob() {
        return this.job;
    }
    
    public int getNumberOfPages() {
        return this.pages.size();
    }
    
    public PageFormat getPageFormat(final int i) throws IndexOutOfBoundsException {
        final PageFormat format = this.job.defaultPage();
        final PDPage page = this.pages.get(i);
        final Dimension media = page.findMediaBox().createDimension();
        final Dimension crop = page.findCropBox().createDimension();
        double diffWidth = 0.0;
        double diffHeight = 0.0;
        if (!media.equals(crop)) {
            diffWidth = (media.getWidth() - crop.getWidth()) / 2.0;
            diffHeight = (media.getHeight() - crop.getHeight()) / 2.0;
        }
        final Paper paper = format.getPaper();
        final PrintService service = this.job.getPrintService();
        final Class<OrientationRequested> orientation = OrientationRequested.class;
        if (service != null && service.getDefaultAttributeValue(orientation) == OrientationRequested.LANDSCAPE) {
            format.setOrientation(0);
            paper.setImageableArea(diffHeight, diffWidth, crop.getHeight(), crop.getWidth());
            paper.setSize(media.getHeight(), media.getWidth());
        }
        else {
            format.setOrientation(1);
            paper.setImageableArea(diffWidth, diffHeight, crop.getWidth(), crop.getHeight());
            paper.setSize(media.getWidth(), media.getHeight());
        }
        format.setPaper(paper);
        return format;
    }
    
    public Printable getPrintable(final int i) throws IndexOutOfBoundsException {
        return this.pages.get(i);
    }
    
    public int print(final Graphics graphics, final PageFormat format, final int i) throws PrinterException {
        if (0 <= i && i < this.pages.size()) {
            return this.pages.get(i).print(graphics, format, i);
        }
        return 1;
    }
}
