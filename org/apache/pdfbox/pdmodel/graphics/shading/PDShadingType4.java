// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.graphics.shading;

import org.apache.pdfbox.pdmodel.common.PDRange;
import org.apache.pdfbox.cos.COSBase;
import java.io.IOException;
import org.apache.pdfbox.pdmodel.common.COSObjectable;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.cos.COSDictionary;
import org.apache.pdfbox.cos.COSArray;
import org.apache.pdfbox.pdmodel.common.function.PDFunction;

public class PDShadingType4 extends PDShadingResources
{
    private PDFunction function;
    private COSArray decode;
    
    public PDShadingType4(final COSDictionary shadingDictionary) {
        super(shadingDictionary);
        this.function = null;
        this.decode = null;
    }
    
    @Override
    public int getShadingType() {
        return 4;
    }
    
    public void setFunction(final PDFunction newFunction) {
        this.function = newFunction;
        this.getCOSDictionary().setItem(COSName.FUNCTION, newFunction);
    }
    
    public PDFunction getFunction() throws IOException {
        if (this.function == null) {
            this.function = PDFunction.create(this.getCOSDictionary().getDictionaryObject(COSName.FUNCTION));
        }
        return this.function;
    }
    
    public int getBitsPerComponent() {
        return this.getCOSDictionary().getInt(COSName.BITS_PER_COMPONENT, -1);
    }
    
    public void setBitsPerComponent(final int bpc) {
        this.getCOSDictionary().setInt(COSName.BITS_PER_COMPONENT, bpc);
    }
    
    public int getBitsPerCoordinate() {
        return this.getCOSDictionary().getInt(COSName.BITS_PER_COORDINATE, -1);
    }
    
    public void setBitsPerCoordinate(final int bpc) {
        this.getCOSDictionary().setInt(COSName.BITS_PER_COORDINATE, bpc);
    }
    
    public int getBitsPerFlag() {
        return this.getCOSDictionary().getInt(COSName.BITS_PER_FLAG, -1);
    }
    
    public void setBitsPerFlag(final int bpf) {
        this.getCOSDictionary().setInt(COSName.BITS_PER_FLAG, bpf);
    }
    
    private COSArray getDecodeValues() {
        if (this.decode == null) {
            this.decode = (COSArray)this.getCOSDictionary().getDictionaryObject(COSName.DECODE);
        }
        return this.decode;
    }
    
    public void setDecodeValues(final COSArray decodeValues) {
        this.decode = decodeValues;
        this.getCOSDictionary().setItem(COSName.DECODE, decodeValues);
    }
    
    public PDRange getDecodeForParameter(final int paramNum) {
        PDRange retval = null;
        final COSArray decodeValues = this.getDecodeValues();
        if (decodeValues != null && decodeValues.size() >= paramNum * 2 + 1) {
            retval = new PDRange(decodeValues, paramNum);
        }
        return retval;
    }
}
