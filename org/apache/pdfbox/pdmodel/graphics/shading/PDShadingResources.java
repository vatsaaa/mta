// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.graphics.shading;

import java.io.IOException;
import org.apache.pdfbox.pdmodel.graphics.color.PDColorSpaceFactory;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.pdmodel.graphics.color.PDColorSpace;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.cos.COSArray;
import org.apache.pdfbox.cos.COSDictionary;
import org.apache.pdfbox.pdmodel.common.COSObjectable;

public abstract class PDShadingResources implements COSObjectable
{
    private COSDictionary dictionary;
    private COSArray background;
    private PDRectangle bBox;
    private PDColorSpace colorspace;
    public static final int SHADING_TYPE1 = 1;
    public static final int SHADING_TYPE2 = 2;
    public static final int SHADING_TYPE3 = 3;
    public static final int SHADING_TYPE4 = 4;
    public static final int SHADING_TYPE5 = 5;
    public static final int SHADING_TYPE6 = 6;
    public static final int SHADING_TYPE7 = 7;
    
    public PDShadingResources() {
        this.background = null;
        this.bBox = null;
        this.colorspace = null;
        this.dictionary = new COSDictionary();
    }
    
    public PDShadingResources(final COSDictionary shadingDictionary) {
        this.background = null;
        this.bBox = null;
        this.colorspace = null;
        this.dictionary = shadingDictionary;
    }
    
    public COSDictionary getCOSDictionary() {
        return this.dictionary;
    }
    
    public COSBase getCOSObject() {
        return this.dictionary;
    }
    
    public String getType() {
        return COSName.SHADING.getName();
    }
    
    public void setShadingType(final int shadingType) {
        this.dictionary.setInt(COSName.SHADING_TYPE, shadingType);
    }
    
    public abstract int getShadingType();
    
    public void setBackground(final COSArray newBackground) {
        this.background = newBackground;
        this.dictionary.setItem(COSName.BACKGROUND, newBackground);
    }
    
    public COSArray getBackground() {
        if (this.background == null) {
            this.background = (COSArray)this.dictionary.getDictionaryObject(COSName.BACKGROUND);
        }
        return this.background;
    }
    
    public PDRectangle getBBox() {
        if (this.bBox == null) {
            final COSArray array = (COSArray)this.dictionary.getDictionaryObject(COSName.BBOX);
            if (array != null) {
                this.bBox = new PDRectangle(array);
            }
        }
        return this.bBox;
    }
    
    public void setBBox(final PDRectangle newBBox) {
        this.bBox = newBBox;
        if (this.bBox == null) {
            this.dictionary.removeItem(COSName.BBOX);
        }
        else {
            this.dictionary.setItem(COSName.BBOX, this.bBox.getCOSArray());
        }
    }
    
    public void setAntiAlias(final boolean antiAlias) {
        this.dictionary.setBoolean(COSName.ANTI_ALIAS, antiAlias);
    }
    
    public boolean getAntiAlias() {
        return this.dictionary.getBoolean(COSName.ANTI_ALIAS, false);
    }
    
    public PDColorSpace getColorSpace() throws IOException {
        if (this.colorspace == null) {
            final COSBase colorSpaceDictionary = this.dictionary.getDictionaryObject(COSName.CS, COSName.COLORSPACE);
            this.colorspace = PDColorSpaceFactory.createColorSpace(colorSpaceDictionary);
        }
        return this.colorspace;
    }
    
    public void setColorSpace(final PDColorSpace newColorspace) {
        this.colorspace = newColorspace;
        if (newColorspace != null) {
            this.dictionary.setItem(COSName.COLORSPACE, newColorspace.getCOSObject());
        }
        else {
            this.dictionary.removeItem(COSName.COLORSPACE);
        }
    }
    
    public static PDShadingResources create(final COSDictionary resourceDictionary) throws IOException {
        PDShadingResources shading = null;
        final int shadingType = resourceDictionary.getInt(COSName.SHADING_TYPE, 0);
        switch (shadingType) {
            case 1: {
                shading = new PDShadingType1(resourceDictionary);
                break;
            }
            case 2: {
                shading = new PDShadingType2(resourceDictionary);
                break;
            }
            case 3: {
                shading = new PDShadingType3(resourceDictionary);
                break;
            }
            case 4: {
                shading = new PDShadingType4(resourceDictionary);
                break;
            }
            case 5: {
                shading = new PDShadingType5(resourceDictionary);
                break;
            }
            case 6: {
                shading = new PDShadingType6(resourceDictionary);
                break;
            }
            case 7: {
                shading = new PDShadingType7(resourceDictionary);
                break;
            }
            default: {
                throw new IOException("Error: Unknown shading type " + shadingType);
            }
        }
        return shading;
    }
}
