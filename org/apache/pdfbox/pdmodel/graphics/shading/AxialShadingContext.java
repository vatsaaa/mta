// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.graphics.shading;

import org.apache.commons.logging.LogFactory;
import java.awt.image.WritableRaster;
import java.awt.image.Raster;
import org.apache.pdfbox.cos.COSArray;
import org.apache.pdfbox.pdmodel.graphics.color.PDColorSpace;
import org.apache.pdfbox.cos.COSBoolean;
import java.io.IOException;
import org.apache.pdfbox.pdmodel.graphics.color.PDDeviceRGB;
import org.apache.pdfbox.util.Matrix;
import java.awt.geom.AffineTransform;
import org.apache.commons.logging.Log;
import java.awt.color.ColorSpace;
import org.apache.pdfbox.pdmodel.common.function.PDFunction;
import java.awt.image.ColorModel;
import java.awt.PaintContext;

public class AxialShadingContext implements PaintContext
{
    private ColorModel colorModel;
    private PDFunction function;
    private ColorSpace shadingColorSpace;
    private float[] coords;
    private float[] domain;
    private boolean[] extend;
    private double x1x0;
    private double y1y0;
    private float d1d0;
    private double denom;
    private static final Log LOG;
    
    public AxialShadingContext(final PDShadingType2 shadingType2, final ColorModel colorModelValue, final AffineTransform xform, final Matrix ctm, final int pageHeight) {
        this.coords = shadingType2.getCoords().toFloatArray();
        if (ctm != null) {
            final float[] coordsTemp = new float[this.coords.length];
            ctm.createAffineTransform().transform(this.coords, 0, coordsTemp, 0, 2);
            coordsTemp[1] = pageHeight - coordsTemp[1];
            coordsTemp[3] = pageHeight - coordsTemp[3];
            xform.transform(coordsTemp, 0, this.coords, 0, 2);
        }
        else {
            final float translateY = (float)xform.getTranslateY();
            this.coords[1] = pageHeight + translateY - this.coords[1];
            this.coords[3] = pageHeight + translateY - this.coords[3];
        }
        try {
            final PDColorSpace cs = shadingType2.getColorSpace();
            if (!(cs instanceof PDDeviceRGB)) {
                this.shadingColorSpace = cs.getJavaColorSpace();
            }
        }
        catch (IOException exception) {
            AxialShadingContext.LOG.error("error while creating colorSpace", exception);
        }
        if (colorModelValue != null) {
            this.colorModel = colorModelValue;
        }
        else {
            try {
                this.colorModel = shadingType2.getColorSpace().createColorModel(8);
            }
            catch (IOException exception) {
                AxialShadingContext.LOG.error("error while creating colorModel", exception);
            }
        }
        try {
            this.function = shadingType2.getFunction();
        }
        catch (IOException exception) {
            AxialShadingContext.LOG.error("error while creating a function", exception);
        }
        if (shadingType2.getDomain() != null) {
            this.domain = shadingType2.getDomain().toFloatArray();
        }
        else {
            this.domain = new float[] { 0.0f, 1.0f };
        }
        final COSArray extendValues = shadingType2.getExtend();
        if (shadingType2.getExtend() != null) {
            (this.extend = new boolean[2])[0] = ((COSBoolean)extendValues.get(0)).getValue();
            this.extend[1] = ((COSBoolean)extendValues.get(1)).getValue();
        }
        else {
            this.extend = new boolean[] { false, false };
        }
        this.x1x0 = this.coords[2] - this.coords[0];
        this.y1y0 = this.coords[3] - this.coords[1];
        this.d1d0 = this.domain[1] - this.domain[0];
        this.denom = Math.pow(this.x1x0, 2.0) + Math.pow(this.y1y0, 2.0);
    }
    
    public void dispose() {
        this.colorModel = null;
        this.function = null;
    }
    
    public ColorModel getColorModel() {
        return this.colorModel;
    }
    
    public Raster getRaster(final int x, final int y, final int w, final int h) {
        final WritableRaster raster = this.getColorModel().createCompatibleWritableRaster(w, h);
        final float[] input = { 0.0f };
        final int[] data = new int[w * h * 3];
        for (int j = 0; j < h; ++j) {
            for (int i = 0; i < w; ++i) {
                double inputValue = this.x1x0 * (x + i - this.coords[0]);
                inputValue += this.y1y0 * (y + j - this.coords[1]);
                inputValue /= this.denom;
                if (inputValue < this.domain[0]) {
                    if (!this.extend[0]) {
                        continue;
                    }
                    inputValue = this.domain[0];
                }
                else if (inputValue > this.domain[1]) {
                    if (!this.extend[1]) {
                        continue;
                    }
                    inputValue = this.domain[1];
                }
                input[0] = (float)(this.domain[0] + this.d1d0 * inputValue);
                float[] values = null;
                try {
                    values = this.function.eval(input);
                }
                catch (IOException exception) {
                    AxialShadingContext.LOG.error("error while processing a function", exception);
                }
                final int index = (j * w + i) * 3;
                if (this.shadingColorSpace != null) {
                    values = this.shadingColorSpace.toRGB(values);
                }
                data[index] = (int)(values[0] * 255.0f);
                data[index + 1] = (int)(values[1] * 255.0f);
                data[index + 2] = (int)(values[2] * 255.0f);
            }
        }
        raster.setPixels(0, 0, w, h, data);
        return raster;
    }
    
    static {
        LOG = LogFactory.getLog(AxialShadingContext.class);
    }
}
