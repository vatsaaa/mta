// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.graphics.shading;

import org.apache.commons.logging.LogFactory;
import java.awt.image.WritableRaster;
import java.awt.image.Raster;
import org.apache.pdfbox.cos.COSArray;
import org.apache.pdfbox.pdmodel.graphics.color.PDColorSpace;
import org.apache.pdfbox.cos.COSBoolean;
import java.io.IOException;
import org.apache.pdfbox.pdmodel.graphics.color.PDDeviceRGB;
import org.apache.pdfbox.util.Matrix;
import java.awt.geom.AffineTransform;
import org.apache.commons.logging.Log;
import java.awt.color.ColorSpace;
import org.apache.pdfbox.pdmodel.common.function.PDFunction;
import java.awt.image.ColorModel;
import java.awt.PaintContext;

public class RadialShadingContext implements PaintContext
{
    private ColorModel colorModel;
    private PDFunction function;
    private ColorSpace shadingColorSpace;
    private float[] coords;
    private float[] domain;
    private boolean[] extend;
    private double x1x0;
    private double y1y0;
    private double r1r0;
    private double x1x0pow2;
    private double y1y0pow2;
    private double r0pow2;
    private float d1d0;
    private double denom;
    private static final Log LOG;
    
    public RadialShadingContext(final PDShadingType3 shadingType3, final ColorModel colorModelValue, final AffineTransform xform, final Matrix ctm, final int pageHeight) {
        this.coords = shadingType3.getCoords().toFloatArray();
        if (ctm != null) {
            final float[] coordsTemp = new float[this.coords.length];
            ctm.createAffineTransform().transform(this.coords, 0, coordsTemp, 0, 1);
            ctm.createAffineTransform().transform(this.coords, 3, coordsTemp, 3, 1);
            coordsTemp[1] = pageHeight - coordsTemp[1];
            coordsTemp[4] = pageHeight - coordsTemp[4];
            xform.transform(coordsTemp, 0, this.coords, 0, 1);
            xform.transform(coordsTemp, 3, this.coords, 3, 1);
        }
        else {
            final float translateY = (float)xform.getTranslateY();
            this.coords[1] = pageHeight + translateY - this.coords[1];
            this.coords[4] = pageHeight + translateY - this.coords[4];
        }
        try {
            final PDColorSpace cs = shadingType3.getColorSpace();
            if (!(cs instanceof PDDeviceRGB)) {
                this.shadingColorSpace = cs.getJavaColorSpace();
            }
        }
        catch (IOException exception) {
            RadialShadingContext.LOG.error("error while creating colorSpace", exception);
        }
        if (colorModelValue != null) {
            this.colorModel = colorModelValue;
        }
        else {
            try {
                this.colorModel = shadingType3.getColorSpace().createColorModel(8);
            }
            catch (IOException exception) {
                RadialShadingContext.LOG.error("error while creating colorModel", exception);
            }
        }
        try {
            this.function = shadingType3.getFunction();
        }
        catch (IOException exception) {
            RadialShadingContext.LOG.error("error while creating a function", exception);
        }
        if (shadingType3.getDomain() != null) {
            this.domain = shadingType3.getDomain().toFloatArray();
        }
        else {
            this.domain = new float[] { 0.0f, 1.0f };
        }
        final COSArray extendValues = shadingType3.getExtend();
        if (shadingType3.getExtend() != null) {
            (this.extend = new boolean[2])[0] = ((COSBoolean)extendValues.get(0)).getValue();
            this.extend[1] = ((COSBoolean)extendValues.get(1)).getValue();
        }
        else {
            this.extend = new boolean[] { false, false };
        }
        this.x1x0 = this.coords[3] - this.coords[0];
        this.y1y0 = this.coords[4] - this.coords[1];
        this.r1r0 = this.coords[5] - this.coords[2];
        this.x1x0pow2 = Math.pow(this.x1x0, 2.0);
        this.y1y0pow2 = Math.pow(this.y1y0, 2.0);
        this.r0pow2 = Math.pow(this.coords[2], 2.0);
        this.denom = this.x1x0pow2 + this.y1y0pow2 - Math.pow(this.r1r0, 2.0);
        this.d1d0 = this.domain[1] - this.domain[0];
    }
    
    public void dispose() {
        this.colorModel = null;
        this.function = null;
    }
    
    public ColorModel getColorModel() {
        return this.colorModel;
    }
    
    public Raster getRaster(final int x, final int y, final int w, final int h) {
        final WritableRaster raster = this.getColorModel().createCompatibleWritableRaster(w, h);
        final float[] input = { 0.0f };
        final int[] data = new int[w * h * 3];
        for (int j = 0; j < h; ++j) {
            for (int i = 0; i < w; ++i) {
                final float[] inputValues = this.calculateInputValues(x + i, y + j);
                float inputValue;
                if (inputValues[0] >= this.domain[0] && inputValues[0] <= this.domain[1]) {
                    if (inputValues[1] >= this.domain[0] && inputValues[1] <= this.domain[1]) {
                        inputValue = Math.max(inputValues[0], inputValues[1]);
                    }
                    else {
                        inputValue = inputValues[0];
                    }
                }
                else if (inputValues[1] >= this.domain[0] && inputValues[1] <= this.domain[1]) {
                    inputValue = inputValues[1];
                }
                else {
                    inputValue = inputValues[0];
                }
                if (inputValue < this.domain[0]) {
                    if (!this.extend[0]) {
                        continue;
                    }
                    inputValue = this.domain[0];
                }
                else if (inputValue > this.domain[1]) {
                    if (!this.extend[1]) {
                        continue;
                    }
                    inputValue = this.domain[1];
                }
                input[0] = this.domain[0] + this.d1d0 * inputValue;
                float[] values = null;
                try {
                    values = this.function.eval(input);
                }
                catch (IOException exception) {
                    RadialShadingContext.LOG.error("error while processing a function", exception);
                }
                final int index = (j * w + i) * 3;
                if (this.shadingColorSpace != null) {
                    values = this.shadingColorSpace.toRGB(values);
                }
                data[index] = (int)(values[0] * 255.0f);
                data[index + 1] = (int)(values[1] * 255.0f);
                data[index + 2] = (int)(values[2] * 255.0f);
            }
        }
        raster.setPixels(0, 0, w, h, data);
        return raster;
    }
    
    private float[] calculateInputValues(final int x, final int y) {
        final float[] values = new float[2];
        final double p = -0.25 * ((x - this.coords[0]) * this.x1x0 + (y - this.coords[1]) * this.y1y0 - this.r1r0) / this.denom;
        final double q = (Math.pow(x - this.coords[0], 2.0) + Math.pow(y - this.coords[1], 2.0) - this.r0pow2) / this.denom;
        final double root = Math.sqrt(Math.pow(p, 2.0) - q);
        values[0] = (float)(-1.0 * p + root);
        values[1] = (float)(-1.0 * p - root);
        return values;
    }
    
    static {
        LOG = LogFactory.getLog(AxialShadingContext.class);
    }
}
