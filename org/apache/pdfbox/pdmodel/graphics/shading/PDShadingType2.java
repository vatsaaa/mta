// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.graphics.shading;

import java.io.IOException;
import org.apache.pdfbox.pdmodel.common.COSObjectable;
import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.cos.COSDictionary;
import org.apache.pdfbox.pdmodel.common.function.PDFunction;
import org.apache.pdfbox.cos.COSArray;

public class PDShadingType2 extends PDShadingResources
{
    private COSArray coords;
    private COSArray domain;
    private COSArray extend;
    private PDFunction function;
    
    public PDShadingType2(final COSDictionary shadingDictionary) {
        super(shadingDictionary);
        this.coords = null;
        this.domain = null;
        this.extend = null;
        this.function = null;
    }
    
    @Override
    public int getShadingType() {
        return 2;
    }
    
    public COSArray getExtend() {
        if (this.extend == null) {
            this.extend = (COSArray)this.getCOSDictionary().getDictionaryObject(COSName.EXTEND);
        }
        return this.extend;
    }
    
    public void setExtend(final COSArray newExtend) {
        this.extend = newExtend;
        if (newExtend == null) {
            this.getCOSDictionary().removeItem(COSName.EXTEND);
        }
        else {
            this.getCOSDictionary().setItem(COSName.EXTEND, newExtend);
        }
    }
    
    public COSArray getDomain() {
        if (this.domain == null) {
            this.domain = (COSArray)this.getCOSDictionary().getDictionaryObject(COSName.DOMAIN);
        }
        return this.domain;
    }
    
    public void setDomain(final COSArray newDomain) {
        this.domain = newDomain;
        if (newDomain == null) {
            this.getCOSDictionary().removeItem(COSName.DOMAIN);
        }
        else {
            this.getCOSDictionary().setItem(COSName.DOMAIN, newDomain);
        }
    }
    
    public COSArray getCoords() {
        if (this.coords == null) {
            this.coords = (COSArray)this.getCOSDictionary().getDictionaryObject(COSName.COORDS);
        }
        return this.coords;
    }
    
    public void setCoords(final COSArray newCoords) {
        this.coords = newCoords;
        if (newCoords == null) {
            this.getCOSDictionary().removeItem(COSName.COORDS);
        }
        else {
            this.getCOSDictionary().setItem(COSName.COORDS, newCoords);
        }
    }
    
    public void setFunction(final PDFunction newFunction) {
        this.function = newFunction;
        if (newFunction == null) {
            this.getCOSDictionary().removeItem(COSName.FUNCTION);
        }
        else {
            this.getCOSDictionary().setItem(COSName.FUNCTION, newFunction);
        }
    }
    
    public PDFunction getFunction() throws IOException {
        if (this.function == null) {
            this.function = PDFunction.create(this.getCOSDictionary().getDictionaryObject(COSName.FUNCTION));
        }
        return this.function;
    }
}
