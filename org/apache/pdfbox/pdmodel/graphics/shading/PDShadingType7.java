// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.graphics.shading;

import org.apache.pdfbox.cos.COSDictionary;

public class PDShadingType7 extends PDShadingType4
{
    public PDShadingType7(final COSDictionary shadingDictionary) {
        super(shadingDictionary);
    }
    
    @Override
    public int getShadingType() {
        return 6;
    }
}
