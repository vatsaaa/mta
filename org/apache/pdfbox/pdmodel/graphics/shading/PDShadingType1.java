// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.graphics.shading;

import java.io.IOException;
import org.apache.pdfbox.pdmodel.common.COSObjectable;
import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSFloat;
import java.awt.geom.AffineTransform;
import org.apache.pdfbox.cos.COSNumber;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.util.Matrix;
import org.apache.pdfbox.cos.COSDictionary;
import org.apache.pdfbox.pdmodel.common.function.PDFunction;
import org.apache.pdfbox.cos.COSArray;

public class PDShadingType1 extends PDShadingResources
{
    private COSArray domain;
    private PDFunction function;
    
    public PDShadingType1(final COSDictionary shadingDictionary) {
        super(shadingDictionary);
        this.domain = null;
        this.function = null;
    }
    
    @Override
    public int getShadingType() {
        return 1;
    }
    
    public Matrix getMatrix() {
        Matrix retval = null;
        final COSArray array = (COSArray)this.getCOSDictionary().getDictionaryObject(COSName.MATRIX);
        if (array != null) {
            retval = new Matrix();
            retval.setValue(0, 0, ((COSNumber)array.get(0)).floatValue());
            retval.setValue(0, 1, ((COSNumber)array.get(1)).floatValue());
            retval.setValue(1, 0, ((COSNumber)array.get(2)).floatValue());
            retval.setValue(1, 1, ((COSNumber)array.get(3)).floatValue());
            retval.setValue(2, 0, ((COSNumber)array.get(4)).floatValue());
            retval.setValue(2, 1, ((COSNumber)array.get(5)).floatValue());
        }
        return retval;
    }
    
    public void setMatrix(final AffineTransform transform) {
        final COSArray matrix = new COSArray();
        final double[] values = new double[6];
        transform.getMatrix(values);
        for (final double v : values) {
            matrix.add(new COSFloat((float)v));
        }
        this.getCOSDictionary().setItem(COSName.MATRIX, matrix);
    }
    
    public COSArray getDomain() {
        if (this.domain == null) {
            this.domain = (COSArray)this.getCOSDictionary().getDictionaryObject(COSName.DOMAIN);
        }
        return this.domain;
    }
    
    public void setDomain(final COSArray newDomain) {
        this.domain = newDomain;
        this.getCOSDictionary().setItem(COSName.DOMAIN, newDomain);
    }
    
    public void setFunction(final PDFunction newFunction) {
        this.function = newFunction;
        this.getCOSDictionary().setItem(COSName.FUNCTION, newFunction);
    }
    
    public PDFunction getFunction() throws IOException {
        if (this.function == null) {
            this.function = PDFunction.create(this.getCOSDictionary().getDictionaryObject(COSName.FUNCTION));
        }
        return this.function;
    }
}
