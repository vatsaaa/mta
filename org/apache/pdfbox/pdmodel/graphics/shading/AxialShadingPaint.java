// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.graphics.shading;

import java.awt.PaintContext;
import java.awt.RenderingHints;
import java.awt.geom.AffineTransform;
import java.awt.geom.Rectangle2D;
import java.awt.Rectangle;
import java.awt.image.ColorModel;
import org.apache.pdfbox.util.Matrix;
import java.awt.Paint;

public class AxialShadingPaint implements Paint
{
    private PDShadingType2 shading;
    private Matrix currentTransformationMatrix;
    private int pageHeight;
    
    public AxialShadingPaint(final PDShadingType2 shadingType2, final Matrix ctm, final int pageHeightValue) {
        this.shading = shadingType2;
        this.currentTransformationMatrix = ctm;
        this.pageHeight = pageHeightValue;
    }
    
    public int getTransparency() {
        return 0;
    }
    
    public PaintContext createContext(final ColorModel cm, final Rectangle deviceBounds, final Rectangle2D userBounds, final AffineTransform xform, final RenderingHints hints) {
        return new AxialShadingContext(this.shading, cm, xform, this.currentTransformationMatrix, this.pageHeight);
    }
}
