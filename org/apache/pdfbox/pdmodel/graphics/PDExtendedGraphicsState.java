// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.graphics;

import org.apache.pdfbox.cos.COSFloat;
import org.apache.pdfbox.cos.COSNumber;
import org.apache.pdfbox.cos.COSArray;
import java.io.IOException;
import java.util.Iterator;
import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.cos.COSDictionary;
import org.apache.pdfbox.pdmodel.common.COSObjectable;

public class PDExtendedGraphicsState implements COSObjectable
{
    public static final String RENDERING_INTENT_ABSOLUTE_COLORIMETRIC = "AbsoluteColorimetric";
    public static final String RENDERING_INTENT_RELATIVE_COLORIMETRIC = "RelativeColorimetric";
    public static final String RENDERING_INTENT_SATURATION = "Saturation";
    public static final String RENDERING_INTENT_PERCEPTUAL = "Perceptual";
    private COSDictionary graphicsState;
    
    public PDExtendedGraphicsState() {
        (this.graphicsState = new COSDictionary()).setItem(COSName.TYPE, COSName.EXT_G_STATE);
    }
    
    public PDExtendedGraphicsState(final COSDictionary dictionary) {
        this.graphicsState = dictionary;
    }
    
    public void copyIntoGraphicsState(final PDGraphicsState gs) throws IOException {
        for (final COSName key : this.graphicsState.keySet()) {
            if (key.equals(COSName.LW)) {
                gs.setLineWidth(this.getLineWidth());
            }
            else if (key.equals(COSName.LC)) {
                gs.setLineCap(this.getLineCapStyle());
            }
            else if (key.equals(COSName.LJ)) {
                gs.setLineJoin(this.getLineJoinStyle());
            }
            else if (key.equals(COSName.ML)) {
                gs.setMiterLimit(this.getMiterLimit());
            }
            else if (key.equals(COSName.D)) {
                gs.setLineDashPattern(this.getLineDashPattern());
            }
            else if (key.equals(COSName.RI)) {
                gs.setRenderingIntent(this.getRenderingIntent());
            }
            else if (key.equals(COSName.OPM)) {
                gs.setOverprintMode(this.getOverprintMode());
            }
            else if (key.equals(COSName.FONT)) {
                final PDFontSetting setting = this.getFontSetting();
                gs.getTextState().setFont(setting.getFont());
                gs.getTextState().setFontSize(setting.getFontSize());
            }
            else if (key.equals(COSName.FL)) {
                gs.setFlatness(this.getFlatnessTolerance());
            }
            else if (key.equals(COSName.SM)) {
                gs.setSmoothness(this.getSmoothnessTolerance());
            }
            else if (key.equals(COSName.SA)) {
                gs.setStrokeAdjustment(this.getAutomaticStrokeAdjustment());
            }
            else if (key.equals(COSName.CA)) {
                gs.setAlphaConstants(this.getStrokingAlpaConstant());
            }
            else if (key.equals(COSName.CA_NS)) {
                gs.setNonStrokeAlphaConstants(this.getNonStrokingAlpaConstant());
            }
            else if (key.equals(COSName.AIS)) {
                gs.setAlphaSource(this.getAlphaSourceFlag());
            }
            else {
                if (!key.equals(COSName.TK)) {
                    continue;
                }
                gs.getTextState().setKnockoutFlag(this.getTextKnockoutFlag());
            }
        }
    }
    
    public COSDictionary getCOSDictionary() {
        return this.graphicsState;
    }
    
    public COSBase getCOSObject() {
        return this.graphicsState;
    }
    
    public Float getLineWidth() {
        return this.getFloatItem(COSName.LW);
    }
    
    public void setLineWidth(final Float width) {
        this.setFloatItem(COSName.LW, width);
    }
    
    public int getLineCapStyle() {
        return this.graphicsState.getInt(COSName.LC);
    }
    
    public void setLineCapStyle(final int style) {
        this.graphicsState.setInt(COSName.LC, style);
    }
    
    public int getLineJoinStyle() {
        return this.graphicsState.getInt(COSName.LJ);
    }
    
    public void setLineJoinStyle(final int style) {
        this.graphicsState.setInt(COSName.LJ, style);
    }
    
    public Float getMiterLimit() {
        return this.getFloatItem(COSName.ML);
    }
    
    public void setMiterLimit(final Float miterLimit) {
        this.setFloatItem(COSName.ML, miterLimit);
    }
    
    public PDLineDashPattern getLineDashPattern() {
        PDLineDashPattern retval = null;
        final COSArray dp = (COSArray)this.graphicsState.getDictionaryObject(COSName.D);
        if (dp != null) {
            retval = new PDLineDashPattern(dp);
        }
        return retval;
    }
    
    public void setLineDashPattern(final PDLineDashPattern dashPattern) {
        this.graphicsState.setItem(COSName.D, dashPattern.getCOSObject());
    }
    
    public String getRenderingIntent() {
        return this.graphicsState.getNameAsString("RI");
    }
    
    public void setRenderingIntent(final String ri) {
        this.graphicsState.setName("RI", ri);
    }
    
    public boolean getStrokingOverprintControl() {
        return this.graphicsState.getBoolean(COSName.OP, false);
    }
    
    public void setStrokingOverprintControl(final boolean op) {
        this.graphicsState.setBoolean(COSName.OP, op);
    }
    
    public boolean getNonStrokingOverprintControl() {
        return this.graphicsState.getBoolean(COSName.OP_NS, this.getStrokingOverprintControl());
    }
    
    public void setNonStrokingOverprintControl(final boolean op) {
        this.graphicsState.setBoolean(COSName.OP_NS, op);
    }
    
    public Float getOverprintMode() {
        return this.getFloatItem(COSName.OPM);
    }
    
    public void setOverprintMode(final Float overprintMode) {
        this.setFloatItem(COSName.OPM, overprintMode);
    }
    
    public PDFontSetting getFontSetting() {
        PDFontSetting setting = null;
        final COSArray font = (COSArray)this.graphicsState.getDictionaryObject(COSName.FONT);
        if (font != null) {
            setting = new PDFontSetting(font);
        }
        return setting;
    }
    
    public void setFontSetting(final PDFontSetting fs) {
        this.graphicsState.setItem(COSName.FONT, fs);
    }
    
    public Float getFlatnessTolerance() {
        return this.getFloatItem(COSName.FL);
    }
    
    public void setFlatnessTolerance(final Float flatness) {
        this.setFloatItem(COSName.FL, flatness);
    }
    
    public Float getSmoothnessTolerance() {
        return this.getFloatItem(COSName.SM);
    }
    
    public void setSmoothnessTolerance(final Float smoothness) {
        this.setFloatItem(COSName.SM, smoothness);
    }
    
    public boolean getAutomaticStrokeAdjustment() {
        return this.graphicsState.getBoolean(COSName.SA, false);
    }
    
    public void setAutomaticStrokeAdjustment(final boolean sa) {
        this.graphicsState.setBoolean(COSName.SA, sa);
    }
    
    public Float getStrokingAlpaConstant() {
        return this.getFloatItem(COSName.CA);
    }
    
    public void setStrokingAlphaConstant(final Float alpha) {
        this.setFloatItem(COSName.CA, alpha);
    }
    
    public Float getNonStrokingAlpaConstant() {
        return this.getFloatItem(COSName.CA_NS);
    }
    
    public void setNonStrokingAlphaConstant(final Float alpha) {
        this.setFloatItem(COSName.CA_NS, alpha);
    }
    
    public boolean getAlphaSourceFlag() {
        return this.graphicsState.getBoolean(COSName.AIS, false);
    }
    
    public void setAlphaSourceFlag(final boolean alpha) {
        this.graphicsState.setBoolean(COSName.AIS, alpha);
    }
    
    public boolean getTextKnockoutFlag() {
        return this.graphicsState.getBoolean(COSName.TK, true);
    }
    
    public void setTextKnockoutFlag(final boolean tk) {
        this.graphicsState.setBoolean(COSName.TK, tk);
    }
    
    private Float getFloatItem(final COSName key) {
        Float retval = null;
        final COSNumber value = (COSNumber)this.graphicsState.getDictionaryObject(key);
        if (value != null) {
            retval = new Float(value.floatValue());
        }
        return retval;
    }
    
    private void setFloatItem(final COSName key, final Float value) {
        if (value == null) {
            this.graphicsState.removeItem(key);
        }
        else {
            this.graphicsState.setItem(key, new COSFloat(value));
        }
    }
}
