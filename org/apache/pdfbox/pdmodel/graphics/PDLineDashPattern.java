// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.graphics;

import org.apache.pdfbox.pdmodel.common.COSArrayList;
import java.util.List;
import org.apache.pdfbox.cos.COSNumber;
import org.apache.pdfbox.cos.COSInteger;
import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSArray;
import org.apache.pdfbox.pdmodel.common.COSObjectable;

public class PDLineDashPattern implements COSObjectable, Cloneable
{
    private COSArray lineDashPattern;
    
    public PDLineDashPattern() {
        this.lineDashPattern = null;
        (this.lineDashPattern = new COSArray()).add(new COSArray());
        this.lineDashPattern.add(COSInteger.ZERO);
    }
    
    public PDLineDashPattern(final COSArray ldp) {
        this.lineDashPattern = null;
        this.lineDashPattern = ldp;
    }
    
    public PDLineDashPattern(final COSArray ldp, final int phase) {
        this.lineDashPattern = null;
        (this.lineDashPattern = new COSArray()).add(ldp);
        this.lineDashPattern.add(COSInteger.get(phase));
    }
    
    public Object clone() {
        PDLineDashPattern pattern = null;
        try {
            pattern = (PDLineDashPattern)super.clone();
            pattern.setDashPattern(this.getDashPattern());
            pattern.setPhaseStart(this.getPhaseStart());
        }
        catch (CloneNotSupportedException exception) {
            exception.printStackTrace();
        }
        return pattern;
    }
    
    public COSBase getCOSObject() {
        return this.lineDashPattern;
    }
    
    public int getPhaseStart() {
        final COSNumber phase = (COSNumber)this.lineDashPattern.get(1);
        return phase.intValue();
    }
    
    public void setPhaseStart(final int phase) {
        this.lineDashPattern.set(1, phase);
    }
    
    public List getDashPattern() {
        final COSArray dashPatterns = (COSArray)this.lineDashPattern.get(0);
        return COSArrayList.convertIntegerCOSArrayToList(dashPatterns);
    }
    
    public COSArray getCOSDashPattern() {
        return (COSArray)this.lineDashPattern.get(0);
    }
    
    public void setDashPattern(final List dashPattern) {
        this.lineDashPattern.set(0, COSArrayList.converterToCOSArray(dashPattern));
    }
    
    public boolean isDashPatternEmpty() {
        final float[] dashPattern = this.getCOSDashPattern().toFloatArray();
        boolean dashPatternEmpty = true;
        if (dashPattern != null) {
            for (int arraySize = dashPattern.length, i = 0; i < arraySize; ++i) {
                if (dashPattern[i] > 0.0f) {
                    dashPatternEmpty = false;
                    break;
                }
            }
        }
        return dashPatternEmpty;
    }
}
