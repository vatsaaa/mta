// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.graphics.optionalcontent;

import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.cos.COSDictionary;
import org.apache.pdfbox.pdmodel.common.COSObjectable;

public class PDOptionalContentGroup implements COSObjectable
{
    private COSDictionary ocg;
    
    public PDOptionalContentGroup(final String name) {
        (this.ocg = new COSDictionary()).setItem(COSName.TYPE, COSName.OCG);
        this.setName(name);
    }
    
    public PDOptionalContentGroup(final COSDictionary dict) {
        if (!dict.getItem(COSName.TYPE).equals(COSName.OCG)) {
            throw new IllegalArgumentException("Provided dictionary is not of type '" + COSName.OCG + "'");
        }
        this.ocg = dict;
    }
    
    public COSBase getCOSObject() {
        return this.ocg;
    }
    
    public String getName() {
        return this.ocg.getString(COSName.NAME);
    }
    
    public void setName(final String name) {
        this.ocg.setString(COSName.NAME, name);
    }
    
    @Override
    public String toString() {
        return super.toString() + " (" + this.getName() + ")";
    }
}
