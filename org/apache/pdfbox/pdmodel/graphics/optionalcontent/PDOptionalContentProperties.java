// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.graphics.optionalcontent;

import org.apache.pdfbox.cos.COSObject;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSArray;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.cos.COSDictionary;
import org.apache.pdfbox.pdmodel.common.COSObjectable;

public class PDOptionalContentProperties implements COSObjectable
{
    private COSDictionary dict;
    
    public PDOptionalContentProperties() {
        (this.dict = new COSDictionary()).setItem(COSName.OCGS, new COSArray());
        this.dict.setItem(COSName.D, new COSDictionary());
    }
    
    public PDOptionalContentProperties(final COSDictionary props) {
        this.dict = props;
    }
    
    public COSBase getCOSObject() {
        return this.dict;
    }
    
    private COSArray getOCGs() {
        COSArray ocgs = (COSArray)this.dict.getItem(COSName.OCGS);
        if (ocgs == null) {
            ocgs = new COSArray();
            this.dict.setItem(COSName.OCGS, ocgs);
        }
        return ocgs;
    }
    
    private COSDictionary getD() {
        COSDictionary d = (COSDictionary)this.dict.getDictionaryObject(COSName.D);
        if (d == null) {
            d = new COSDictionary();
            this.dict.setItem(COSName.D, d);
        }
        return d;
    }
    
    public PDOptionalContentGroup getGroup(final String name) {
        final COSArray ocgs = this.getOCGs();
        for (final COSBase o : ocgs) {
            final COSDictionary ocg = this.toDictionary(o);
            final String groupName = ocg.getString(COSName.NAME);
            if (groupName.equals(name)) {
                return new PDOptionalContentGroup(ocg);
            }
        }
        return null;
    }
    
    public void addGroup(final PDOptionalContentGroup ocg) {
        final COSArray ocgs = this.getOCGs();
        ocgs.add(ocg.getCOSObject());
        COSArray order = (COSArray)this.getD().getDictionaryObject(COSName.ORDER);
        if (order == null) {
            order = new COSArray();
            this.getD().setItem(COSName.ORDER, order);
        }
        order.add(ocg);
    }
    
    public Collection<PDOptionalContentGroup> getOptionalContentGroups() {
        final Collection<PDOptionalContentGroup> coll = new ArrayList<PDOptionalContentGroup>();
        final COSArray ocgs = this.getOCGs();
        for (final COSBase base : ocgs) {
            final COSObject obj = (COSObject)base;
            coll.add(new PDOptionalContentGroup((COSDictionary)obj.getObject()));
        }
        return coll;
    }
    
    public BaseState getBaseState() {
        final COSDictionary d = this.getD();
        final COSName name = (COSName)d.getItem(COSName.BASE_STATE);
        return BaseState.valueOf(name);
    }
    
    public void setBaseState(final BaseState state) {
        final COSDictionary d = this.getD();
        d.setItem(COSName.BASE_STATE, state.getName());
    }
    
    public String[] getGroupNames() {
        final COSArray ocgs = (COSArray)this.dict.getDictionaryObject(COSName.OCGS);
        final int size = ocgs.size();
        final String[] groups = new String[size];
        for (int i = 0; i < size; ++i) {
            final COSBase obj = ocgs.get(i);
            final COSDictionary ocg = this.toDictionary(obj);
            groups[i] = ocg.getString(COSName.NAME);
        }
        return groups;
    }
    
    public boolean hasGroup(final String groupName) {
        final String[] arr$;
        final String[] layers = arr$ = this.getGroupNames();
        for (final String layer : arr$) {
            if (layer.equals(groupName)) {
                return true;
            }
        }
        return false;
    }
    
    public boolean isGroupEnabled(final String groupName) {
        final COSDictionary d = this.getD();
        final COSArray on = (COSArray)d.getDictionaryObject(COSName.ON);
        if (on != null) {
            for (final COSBase o : on) {
                final COSDictionary group = this.toDictionary(o);
                final String name = group.getString(COSName.NAME);
                if (name.equals(groupName)) {
                    return true;
                }
            }
        }
        final COSArray off = (COSArray)d.getDictionaryObject(COSName.OFF);
        if (off != null) {
            for (final COSBase o2 : off) {
                final COSDictionary group2 = this.toDictionary(o2);
                final String name2 = group2.getString(COSName.NAME);
                if (name2.equals(groupName)) {
                    return false;
                }
            }
        }
        final BaseState baseState = this.getBaseState();
        final boolean enabled = !baseState.equals(BaseState.OFF);
        return enabled;
    }
    
    private COSDictionary toDictionary(final COSBase o) {
        if (o instanceof COSObject) {
            return (COSDictionary)((COSObject)o).getObject();
        }
        return (COSDictionary)o;
    }
    
    public boolean setGroupEnabled(final String groupName, final boolean enable) {
        final COSDictionary d = this.getD();
        COSArray on = (COSArray)d.getDictionaryObject(COSName.ON);
        if (on == null) {
            on = new COSArray();
            d.setItem(COSName.ON, on);
        }
        COSArray off = (COSArray)d.getDictionaryObject(COSName.OFF);
        if (off == null) {
            off = new COSArray();
            d.setItem(COSName.OFF, off);
        }
        boolean found = false;
        for (final COSBase o : on) {
            final COSDictionary group = this.toDictionary(o);
            final String name = group.getString(COSName.NAME);
            if (!enable && name.equals(groupName)) {
                on.remove(group);
                off.add(group);
                found = true;
                break;
            }
        }
        for (final COSBase o : off) {
            final COSDictionary group = this.toDictionary(o);
            final String name = group.getString(COSName.NAME);
            if (enable && name.equals(groupName)) {
                off.remove(group);
                on.add(group);
                found = true;
                break;
            }
        }
        if (!found) {
            final PDOptionalContentGroup ocg = this.getGroup(groupName);
            if (enable) {
                on.add(ocg.getCOSObject());
            }
            else {
                off.add(ocg.getCOSObject());
            }
        }
        return found;
    }
    
    public enum BaseState
    {
        ON(COSName.ON), 
        OFF(COSName.OFF), 
        UNCHANGED(COSName.UNCHANGED);
        
        private COSName name;
        
        private BaseState(final COSName value) {
            this.name = value;
        }
        
        public COSName getName() {
            return this.name;
        }
        
        public static BaseState valueOf(final COSName state) {
            if (state == null) {
                return BaseState.ON;
            }
            return valueOf(state.getName().toUpperCase());
        }
    }
}
