// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.graphics;

import org.apache.pdfbox.cos.COSBoolean;
import org.apache.pdfbox.cos.COSFloat;
import java.io.IOException;
import org.apache.pdfbox.pdmodel.graphics.color.PDColorSpaceFactory;
import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.pdmodel.graphics.color.PDColorSpace;
import org.apache.pdfbox.pdmodel.common.function.PDFunction;
import org.apache.pdfbox.cos.COSArray;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.cos.COSDictionary;
import org.apache.pdfbox.pdmodel.common.COSObjectable;

public class PDShading implements COSObjectable
{
    private COSDictionary DictShading;
    private COSName shadingname;
    private COSArray domain;
    private COSArray extend;
    private PDFunction function;
    private PDColorSpace colorspace;
    public static final String NAME = "Shading";
    
    public PDShading() {
        this.domain = null;
        this.extend = null;
        this.function = null;
        this.colorspace = null;
        this.DictShading = new COSDictionary();
    }
    
    public PDShading(final COSName name, final COSDictionary shading) {
        this.domain = null;
        this.extend = null;
        this.function = null;
        this.colorspace = null;
        this.DictShading = shading;
        this.shadingname = name;
    }
    
    public String getName() {
        return "Shading";
    }
    
    public COSBase getCOSObject() {
        return COSName.SHADING;
    }
    
    public COSName getShadingName() {
        return this.shadingname;
    }
    
    public int getShadingType() {
        return this.DictShading.getInt(COSName.SHADING_TYPE);
    }
    
    public PDColorSpace getColorSpace() throws IOException {
        if (this.colorspace == null) {
            this.colorspace = PDColorSpaceFactory.createColorSpace(this.DictShading.getDictionaryObject(COSName.COLORSPACE));
        }
        return this.colorspace;
    }
    
    public boolean getAntiAlias() {
        return this.DictShading.getBoolean(COSName.ANTI_ALIAS, false);
    }
    
    public COSArray getCoords() {
        return (COSArray)this.DictShading.getDictionaryObject(COSName.COORDS);
    }
    
    public PDFunction getFunction() throws IOException {
        if (this.function == null) {
            this.function = PDFunction.create(this.DictShading.getDictionaryObject(COSName.FUNCTION));
        }
        return this.function;
    }
    
    public COSArray getDomain() {
        if (this.domain == null) {
            this.domain = (COSArray)this.DictShading.getDictionaryObject(COSName.DOMAIN);
            if (this.domain == null) {
                (this.domain = new COSArray()).add(new COSFloat(0.0f));
                this.domain.add(new COSFloat(1.0f));
            }
        }
        return this.domain;
    }
    
    public COSArray getExtend() {
        if (this.extend == null) {
            this.extend = (COSArray)this.DictShading.getDictionaryObject(COSName.EXTEND);
            if (this.extend == null) {
                (this.extend = new COSArray()).add(COSBoolean.FALSE);
                this.extend.add(COSBoolean.FALSE);
            }
        }
        return this.extend;
    }
    
    @Override
    public String toString() {
        String sColorSpace;
        try {
            sColorSpace = this.getColorSpace().toString();
        }
        catch (IOException e) {
            sColorSpace = "Failure retrieving ColorSpace: " + e.toString();
        }
        String sFunction;
        try {
            sFunction = this.getFunction().toString();
        }
        catch (IOException e) {
            sFunction = "n/a";
        }
        final String s = "Shading " + this.shadingname + "\n" + "\tShadingType: " + this.getShadingType() + "\n" + "\tColorSpace: " + sColorSpace + "\n" + "\tAntiAlias: " + this.getAntiAlias() + "\n" + "\tCoords: " + ((this.getCoords() != null) ? this.getCoords().toString() : "") + "\n" + "\tDomain: " + this.getDomain().toString() + "\n" + "\tFunction: " + sFunction + "\n" + "\tExtend: " + this.getExtend().toString() + "\n" + "\tRaw Value:\n" + this.DictShading.toString();
        return s;
    }
}
