// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.graphics.pattern;

import java.io.IOException;
import java.awt.Paint;
import org.apache.pdfbox.cos.COSFloat;
import java.awt.geom.AffineTransform;
import org.apache.pdfbox.cos.COSNumber;
import org.apache.pdfbox.util.Matrix;
import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSArray;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.common.COSObjectable;
import org.apache.pdfbox.pdmodel.PDResources;
import org.apache.pdfbox.cos.COSDictionary;
import org.apache.pdfbox.cos.COSName;

public class PDTilingPatternResources extends PDPatternResources
{
    public PDTilingPatternResources() {
        this.getCOSDictionary().setInt(COSName.PATTERN_TYPE, 1);
    }
    
    public PDTilingPatternResources(final COSDictionary resourceDictionary) {
        super(resourceDictionary);
    }
    
    @Override
    public int getPatternType() {
        return 1;
    }
    
    @Override
    public void setLength(final int length) {
        this.getCOSDictionary().setInt(COSName.LENGTH, length);
    }
    
    @Override
    public int getLength() {
        return this.getCOSDictionary().getInt(COSName.LENGTH, 0);
    }
    
    @Override
    public void setPaintType(final int paintType) {
        this.getCOSDictionary().setInt(COSName.PAINT_TYPE, paintType);
    }
    
    public int getPaintType() {
        return this.getCOSDictionary().getInt(COSName.PAINT_TYPE, 0);
    }
    
    public void setTilingType(final int tilingType) {
        this.getCOSDictionary().setInt(COSName.TILING_TYPE, tilingType);
    }
    
    public int getTilingType() {
        return this.getCOSDictionary().getInt(COSName.TILING_TYPE, 0);
    }
    
    public void setXStep(final int xStep) {
        this.getCOSDictionary().setInt(COSName.X_STEP, xStep);
    }
    
    public int getXStep() {
        return this.getCOSDictionary().getInt(COSName.X_STEP, 0);
    }
    
    public void setYStep(final int yStep) {
        this.getCOSDictionary().setInt(COSName.Y_STEP, yStep);
    }
    
    public int getYStep() {
        return this.getCOSDictionary().getInt(COSName.Y_STEP, 0);
    }
    
    public PDResources getResources() {
        PDResources retval = null;
        final COSDictionary resources = (COSDictionary)this.getCOSDictionary().getDictionaryObject(COSName.RESOURCES);
        if (resources != null) {
            retval = new PDResources(resources);
        }
        return retval;
    }
    
    public void setResources(final PDResources resources) {
        if (resources != null) {
            this.getCOSDictionary().setItem(COSName.RESOURCES, resources);
        }
        else {
            this.getCOSDictionary().removeItem(COSName.RESOURCES);
        }
    }
    
    public PDRectangle getBBox() {
        PDRectangle retval = null;
        final COSArray array = (COSArray)this.getCOSDictionary().getDictionaryObject(COSName.BBOX);
        if (array != null) {
            retval = new PDRectangle(array);
        }
        return retval;
    }
    
    public void setBBox(final PDRectangle bbox) {
        if (bbox == null) {
            this.getCOSDictionary().removeItem(COSName.BBOX);
        }
        else {
            this.getCOSDictionary().setItem(COSName.BBOX, bbox.getCOSArray());
        }
    }
    
    public Matrix getMatrix() {
        Matrix retval = null;
        final COSArray array = (COSArray)this.getCOSDictionary().getDictionaryObject(COSName.MATRIX);
        if (array != null) {
            retval = new Matrix();
            retval.setValue(0, 0, ((COSNumber)array.get(0)).floatValue());
            retval.setValue(0, 1, ((COSNumber)array.get(1)).floatValue());
            retval.setValue(1, 0, ((COSNumber)array.get(2)).floatValue());
            retval.setValue(1, 1, ((COSNumber)array.get(3)).floatValue());
            retval.setValue(2, 0, ((COSNumber)array.get(4)).floatValue());
            retval.setValue(2, 1, ((COSNumber)array.get(5)).floatValue());
        }
        return retval;
    }
    
    public void setMatrix(final AffineTransform transform) {
        final COSArray matrix = new COSArray();
        final double[] values = new double[6];
        transform.getMatrix(values);
        for (final double v : values) {
            matrix.add(new COSFloat((float)v));
        }
        this.getCOSDictionary().setItem(COSName.MATRIX, matrix);
    }
    
    @Override
    public Paint getPaint(final int pageHeight) throws IOException {
        return null;
    }
}
