// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.graphics.pattern;

import java.awt.Paint;
import java.io.IOException;
import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.cos.COSDictionary;
import org.apache.pdfbox.pdmodel.common.COSObjectable;

public abstract class PDPatternResources implements COSObjectable
{
    private COSDictionary patternDictionary;
    public static final int TILING_PATTERN = 1;
    public static final int SHADING_PATTERN = 2;
    
    public PDPatternResources() {
        (this.patternDictionary = new COSDictionary()).setName(COSName.TYPE, COSName.PATTERN.getName());
    }
    
    public PDPatternResources(final COSDictionary resourceDictionary) {
        this.patternDictionary = resourceDictionary;
    }
    
    public COSDictionary getCOSDictionary() {
        return this.patternDictionary;
    }
    
    public COSBase getCOSObject() {
        return this.patternDictionary;
    }
    
    public void setFilter(final String filter) {
        this.patternDictionary.setItem(COSName.FILTER, COSName.getPDFName(filter));
    }
    
    public String getFilter() {
        return this.patternDictionary.getNameAsString(COSName.FILTER);
    }
    
    public void setLength(final int length) {
        this.patternDictionary.setInt(COSName.LENGTH, length);
    }
    
    public int getLength() {
        return this.patternDictionary.getInt(COSName.LENGTH, 0);
    }
    
    public void setPaintType(final int paintType) {
        this.patternDictionary.setInt(COSName.PAINT_TYPE, paintType);
    }
    
    public String getType() {
        return COSName.PATTERN.getName();
    }
    
    public void setPatternType(final int patternType) {
        this.patternDictionary.setInt(COSName.PATTERN_TYPE, patternType);
    }
    
    public abstract int getPatternType();
    
    public static PDPatternResources create(final COSDictionary resourceDictionary) throws IOException {
        PDPatternResources pattern = null;
        final int patternType = resourceDictionary.getInt(COSName.PATTERN_TYPE, 0);
        switch (patternType) {
            case 1: {
                pattern = new PDTilingPatternResources(resourceDictionary);
                break;
            }
            case 2: {
                pattern = new PDShadingPatternResources(resourceDictionary);
                break;
            }
            default: {
                throw new IOException("Error: Unknown pattern type " + patternType);
            }
        }
        return pattern;
    }
    
    public abstract Paint getPaint(final int p0) throws IOException;
}
