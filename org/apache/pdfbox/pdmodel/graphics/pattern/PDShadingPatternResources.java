// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.graphics.pattern;

import org.apache.commons.logging.LogFactory;
import org.apache.pdfbox.pdmodel.graphics.shading.RadialShadingPaint;
import org.apache.pdfbox.pdmodel.graphics.shading.PDShadingType3;
import org.apache.pdfbox.pdmodel.graphics.shading.AxialShadingPaint;
import org.apache.pdfbox.pdmodel.graphics.shading.PDShadingType2;
import java.awt.Paint;
import java.io.IOException;
import org.apache.pdfbox.pdmodel.common.COSObjectable;
import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSFloat;
import java.awt.geom.AffineTransform;
import org.apache.pdfbox.cos.COSNumber;
import org.apache.pdfbox.util.Matrix;
import org.apache.pdfbox.cos.COSDictionary;
import org.apache.pdfbox.cos.COSName;
import org.apache.commons.logging.Log;
import org.apache.pdfbox.cos.COSArray;
import org.apache.pdfbox.pdmodel.graphics.shading.PDShadingResources;
import org.apache.pdfbox.pdmodel.graphics.PDExtendedGraphicsState;

public class PDShadingPatternResources extends PDPatternResources
{
    private PDExtendedGraphicsState extendedGraphicsState;
    private PDShadingResources shading;
    private COSArray matrix;
    private static final Log LOG;
    
    public PDShadingPatternResources() {
        this.matrix = null;
        this.getCOSDictionary().setInt(COSName.PATTERN_TYPE, 2);
    }
    
    public PDShadingPatternResources(final COSDictionary resourceDictionary) {
        super(resourceDictionary);
        this.matrix = null;
    }
    
    @Override
    public int getPatternType() {
        return 2;
    }
    
    public Matrix getMatrix() {
        Matrix returnMatrix = null;
        if (this.matrix == null) {
            this.matrix = (COSArray)this.getCOSDictionary().getDictionaryObject(COSName.MATRIX);
        }
        if (this.matrix != null) {
            returnMatrix = new Matrix();
            returnMatrix.setValue(0, 0, ((COSNumber)this.matrix.get(0)).floatValue());
            returnMatrix.setValue(0, 1, ((COSNumber)this.matrix.get(1)).floatValue());
            returnMatrix.setValue(1, 0, ((COSNumber)this.matrix.get(2)).floatValue());
            returnMatrix.setValue(1, 1, ((COSNumber)this.matrix.get(3)).floatValue());
            returnMatrix.setValue(2, 0, ((COSNumber)this.matrix.get(4)).floatValue());
            returnMatrix.setValue(2, 1, ((COSNumber)this.matrix.get(5)).floatValue());
        }
        return returnMatrix;
    }
    
    public void setMatrix(final AffineTransform transform) {
        this.matrix = new COSArray();
        final double[] values = new double[6];
        transform.getMatrix(values);
        for (final double v : values) {
            this.matrix.add(new COSFloat((float)v));
        }
        this.getCOSDictionary().setItem(COSName.MATRIX, this.matrix);
    }
    
    public PDExtendedGraphicsState getExtendedGraphicsState() {
        if (this.extendedGraphicsState == null) {
            final COSDictionary dictionary = (COSDictionary)this.getCOSDictionary().getDictionaryObject(COSName.EXT_G_STATE);
            if (dictionary != null) {
                this.extendedGraphicsState = new PDExtendedGraphicsState(dictionary);
            }
        }
        return this.extendedGraphicsState;
    }
    
    public void setExtendedGraphicsState(final PDExtendedGraphicsState extendedGraphicsState) {
        this.extendedGraphicsState = extendedGraphicsState;
        if (extendedGraphicsState != null) {
            this.getCOSDictionary().setItem(COSName.EXT_G_STATE, extendedGraphicsState);
        }
        else {
            this.getCOSDictionary().removeItem(COSName.EXT_G_STATE);
        }
    }
    
    public PDShadingResources getShading() throws IOException {
        if (this.shading == null) {
            final COSDictionary dictionary = (COSDictionary)this.getCOSDictionary().getDictionaryObject(COSName.SHADING);
            if (dictionary != null) {
                this.shading = PDShadingResources.create(dictionary);
            }
        }
        return this.shading;
    }
    
    public void setShading(final PDShadingResources shadingResources) {
        this.shading = shadingResources;
        if (shadingResources != null) {
            this.getCOSDictionary().setItem(COSName.SHADING, shadingResources);
        }
        else {
            this.getCOSDictionary().removeItem(COSName.SHADING);
        }
    }
    
    @Override
    public Paint getPaint(final int pageHeight) throws IOException {
        Paint paint = null;
        final PDShadingResources shadingResources = this.getShading();
        final int shadingType = (shadingResources != null) ? shadingResources.getShadingType() : 0;
        switch (shadingType) {
            case 2: {
                paint = new AxialShadingPaint((PDShadingType2)this.getShading(), null, pageHeight);
                break;
            }
            case 3: {
                paint = new RadialShadingPaint((PDShadingType3)this.getShading(), null, pageHeight);
                break;
            }
            case 1:
            case 4:
            case 5:
            case 6:
            case 7: {
                PDShadingPatternResources.LOG.debug("Error: Unsupported shading type " + shadingType);
                break;
            }
            default: {
                throw new IOException("Error: Unknown shading type " + shadingType);
            }
        }
        return paint;
    }
    
    static {
        LOG = LogFactory.getLog(PDShadingPatternResources.class);
    }
}
