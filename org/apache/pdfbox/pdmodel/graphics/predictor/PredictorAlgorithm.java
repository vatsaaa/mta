// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.graphics.predictor;

import java.util.Random;

public abstract class PredictorAlgorithm
{
    private int width;
    private int height;
    private int bpp;
    
    public void checkBufsiz(final byte[] src, final byte[] dest) {
        if (src.length != dest.length) {
            throw new IllegalArgumentException("src.length != dest.length");
        }
        if (src.length != this.getWidth() * this.getHeight() * this.getBpp()) {
            throw new IllegalArgumentException("src.length != width * height * bpp");
        }
    }
    
    public abstract void encodeLine(final byte[] p0, final byte[] p1, final int p2, final int p3, final int p4, final int p5);
    
    public abstract void decodeLine(final byte[] p0, final byte[] p1, final int p2, final int p3, final int p4, final int p5);
    
    public static void main(final String[] args) {
        final Random rnd = new Random();
        final int width = 5;
        final int height = 5;
        final int bpp = 3;
        final byte[] raw = new byte[width * height * bpp];
        rnd.nextBytes(raw);
        System.out.println("raw:   ");
        dump(raw);
        for (int i = 10; i < 15; ++i) {
            final byte[] decoded = new byte[width * height * bpp];
            final byte[] encoded = new byte[width * height * bpp];
            final PredictorAlgorithm filter = getFilter(i);
            filter.setWidth(width);
            filter.setHeight(height);
            filter.setBpp(bpp);
            filter.encode(raw, encoded);
            filter.decode(encoded, decoded);
            System.out.println(filter.getClass().getName());
            dump(decoded);
        }
    }
    
    public int leftPixel(final byte[] buf, final int offset, final int dy, final int x) {
        return (x >= this.getBpp()) ? buf[offset + x - this.getBpp()] : 0;
    }
    
    public int abovePixel(final byte[] buf, final int offset, final int dy, final int x) {
        return (offset >= dy) ? buf[offset + x - dy] : 0;
    }
    
    public int aboveLeftPixel(final byte[] buf, final int offset, final int dy, final int x) {
        return (offset >= dy && x >= this.getBpp()) ? buf[offset + x - dy - this.getBpp()] : 0;
    }
    
    private static void dump(final byte[] raw) {
        for (int i = 0; i < raw.length; ++i) {
            System.out.print(raw[i] + " ");
        }
        System.out.println();
    }
    
    public int getBpp() {
        return this.bpp;
    }
    
    public void setBpp(final int newBpp) {
        this.bpp = newBpp;
    }
    
    public int getHeight() {
        return this.height;
    }
    
    public void setHeight(final int newHeight) {
        this.height = newHeight;
    }
    
    public int getWidth() {
        return this.width;
    }
    
    public void setWidth(final int newWidth) {
        this.width = newWidth;
    }
    
    public void encode(final byte[] src, final byte[] dest) {
        this.checkBufsiz(dest, src);
        final int dy = this.getWidth() * this.getBpp();
        for (int y = 0; y < this.height; ++y) {
            final int yoffset = y * dy;
            this.encodeLine(src, dest, dy, yoffset, dy, yoffset);
        }
    }
    
    public void decode(final byte[] src, final byte[] dest) {
        this.checkBufsiz(src, dest);
        final int dy = this.width * this.bpp;
        for (int y = 0; y < this.height; ++y) {
            final int yoffset = y * dy;
            this.decodeLine(src, dest, dy, yoffset, dy, yoffset);
        }
    }
    
    public static PredictorAlgorithm getFilter(final int predictor) {
        PredictorAlgorithm filter = null;
        switch (predictor) {
            case 10: {
                filter = new None();
                break;
            }
            case 11: {
                filter = new Sub();
                break;
            }
            case 12: {
                filter = new Up();
                break;
            }
            case 13: {
                filter = new Average();
                break;
            }
            case 14: {
                filter = new Paeth();
                break;
            }
            case 15: {
                filter = new Optimum();
                break;
            }
            default: {
                filter = new None();
                break;
            }
        }
        return filter;
    }
}
