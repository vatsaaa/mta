// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.graphics.predictor;

public class Average extends PredictorAlgorithm
{
    @Override
    public void encodeLine(final byte[] src, final byte[] dest, final int srcDy, final int srcOffset, final int destDy, final int destOffset) {
        for (int bpl = this.getWidth() * this.getBpp(), x = 0; x < bpl; ++x) {
            dest[x + destOffset] = (byte)(src[x + srcOffset] - (this.leftPixel(src, srcOffset, srcDy, x) + this.abovePixel(src, srcOffset, srcDy, x) >>> 2));
        }
    }
    
    @Override
    public void decodeLine(final byte[] src, final byte[] dest, final int srcDy, final int srcOffset, final int destDy, final int destOffset) {
        for (int bpl = this.getWidth() * this.getBpp(), x = 0; x < bpl; ++x) {
            dest[x + destOffset] = (byte)(src[x + srcOffset] + (this.leftPixel(dest, destOffset, destDy, x) + this.abovePixel(dest, destOffset, destDy, x) >>> 2));
        }
    }
}
