// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.graphics.predictor;

public class Up extends PredictorAlgorithm
{
    @Override
    public void encodeLine(final byte[] src, final byte[] dest, final int srcDy, final int srcOffset, final int destDy, final int destOffset) {
        final int bpl = this.getWidth() * this.getBpp();
        if (srcOffset - srcDy < 0) {
            if (0 < this.getHeight()) {
                for (int x = 0; x < bpl; ++x) {
                    dest[destOffset + x] = src[srcOffset + x];
                }
            }
        }
        else {
            for (int x = 0; x < bpl; ++x) {
                dest[destOffset + x] = (byte)(src[srcOffset + x] - src[srcOffset + x - srcDy]);
            }
        }
    }
    
    @Override
    public void decodeLine(final byte[] src, final byte[] dest, final int srcDy, final int srcOffset, final int destDy, final int destOffset) {
        final int bpl = this.getWidth() * this.getBpp();
        if (destOffset - destDy < 0) {
            if (0 < this.getHeight()) {
                for (int x = 0; x < bpl; ++x) {
                    dest[destOffset + x] = src[srcOffset + x];
                }
            }
        }
        else {
            for (int x = 0; x < bpl; ++x) {
                dest[destOffset + x] = (byte)(src[srcOffset + x] + dest[destOffset + x - destDy]);
            }
        }
    }
}
