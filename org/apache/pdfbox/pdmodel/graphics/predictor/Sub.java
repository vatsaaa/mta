// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.graphics.predictor;

public class Sub extends PredictorAlgorithm
{
    @Override
    public void encodeLine(final byte[] src, final byte[] dest, final int srcDy, final int srcOffset, final int destDy, final int destOffset) {
        final int bpl = this.getWidth() * this.getBpp();
        final int bpp = this.getBpp();
        for (int x = 0; x < bpl && x < bpp; ++x) {
            dest[x + destOffset] = src[x + srcOffset];
        }
        for (int x = this.getBpp(); x < bpl; ++x) {
            dest[x + destOffset] = (byte)(src[x + srcOffset] - src[x + srcOffset - bpp]);
        }
    }
    
    @Override
    public void decodeLine(final byte[] src, final byte[] dest, final int srcDy, final int srcOffset, final int destDy, final int destOffset) {
        final int bpl = this.getWidth() * this.getBpp();
        final int bpp = this.getBpp();
        for (int x = 0; x < bpl && x < bpp; ++x) {
            dest[x + destOffset] = src[x + srcOffset];
        }
        for (int x = this.getBpp(); x < bpl; ++x) {
            dest[x + destOffset] = (byte)(src[x + srcOffset] + dest[x + destOffset - bpp]);
        }
    }
}
