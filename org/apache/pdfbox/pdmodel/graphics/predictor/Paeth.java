// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.graphics.predictor;

public class Paeth extends PredictorAlgorithm
{
    public int paethPredictor(final int a, final int b, final int c) {
        final int p = a + b - c;
        final int pa = Math.abs(p - a);
        final int pb = Math.abs(p - b);
        final int pc = Math.abs(p - c);
        if (pa <= pb && pa <= pc) {
            return a;
        }
        if (pb <= pc) {
            return b;
        }
        return c;
    }
    
    @Override
    public void encodeLine(final byte[] src, final byte[] dest, final int srcDy, final int srcOffset, final int destDy, final int destOffset) {
        for (int bpl = this.getWidth() * this.getBpp(), x = 0; x < bpl; ++x) {
            dest[x + destOffset] = (byte)(src[x + srcOffset] - this.paethPredictor(this.leftPixel(src, srcOffset, srcDy, x), this.abovePixel(src, srcOffset, srcDy, x), this.aboveLeftPixel(src, srcOffset, srcDy, x)));
        }
    }
    
    @Override
    public void decodeLine(final byte[] src, final byte[] dest, final int srcDy, final int srcOffset, final int destDy, final int destOffset) {
        for (int bpl = this.getWidth() * this.getBpp(), x = 0; x < bpl; ++x) {
            dest[x + destOffset] = (byte)(src[x + srcOffset] + this.paethPredictor(this.leftPixel(dest, destOffset, destDy, x), this.abovePixel(dest, destOffset, destDy, x), this.aboveLeftPixel(dest, destOffset, destDy, x)));
        }
    }
}
