// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.graphics.predictor;

public class None extends PredictorAlgorithm
{
    @Override
    public void encode(final byte[] src, final byte[] dest) {
        this.checkBufsiz(dest, src);
        System.arraycopy(src, 0, dest, 0, src.length);
    }
    
    @Override
    public void decode(final byte[] src, final byte[] dest) {
        System.arraycopy(src, 0, dest, 0, src.length);
    }
    
    @Override
    public void encodeLine(final byte[] src, final byte[] dest, final int srcDy, final int srcOffset, final int destDy, final int destOffset) {
        for (int bpl = this.getWidth() * this.getBpp(), x = 0; x < bpl; ++x) {
            dest[destOffset + x] = src[srcOffset + x];
        }
    }
    
    @Override
    public void decodeLine(final byte[] src, final byte[] dest, final int srcDy, final int srcOffset, final int destDy, final int destOffset) {
        for (int bpl = this.getWidth() * this.getBpp(), x = 0; x < bpl; ++x) {
            dest[destOffset + x] = src[srcOffset + x];
        }
    }
}
