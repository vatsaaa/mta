// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.graphics.predictor;

public class Optimum extends PredictorAlgorithm
{
    PredictorAlgorithm[] filter;
    
    public Optimum() {
        this.filter = new PredictorAlgorithm[] { new None(), new Sub(), new Up(), new Average(), new Paeth() };
    }
    
    @Override
    public void checkBufsiz(final byte[] filtered, final byte[] raw) {
        if (filtered.length != (this.getWidth() * this.getBpp() + 1) * this.getHeight()) {
            throw new IllegalArgumentException("filtered.length != (width*bpp + 1) * height, " + filtered.length + " " + (this.getWidth() * this.getBpp() + 1) * this.getHeight() + "w,h,bpp=" + this.getWidth() + "," + this.getHeight() + "," + this.getBpp());
        }
        if (raw.length != this.getWidth() * this.getHeight() * this.getBpp()) {
            throw new IllegalArgumentException("raw.length != width * height * bpp, raw.length=" + raw.length + " w,h,bpp=" + this.getWidth() + "," + this.getHeight() + "," + this.getBpp());
        }
    }
    
    @Override
    public void encodeLine(final byte[] src, final byte[] dest, final int srcDy, final int srcOffset, final int destDy, final int destOffset) {
        throw new UnsupportedOperationException("encodeLine");
    }
    
    @Override
    public void decodeLine(final byte[] src, final byte[] dest, final int srcDy, final int srcOffset, final int destDy, final int destOffset) {
        throw new UnsupportedOperationException("decodeLine");
    }
    
    @Override
    public void encode(final byte[] src, final byte[] dest) {
        this.checkBufsiz(dest, src);
        throw new UnsupportedOperationException("encode");
    }
    
    @Override
    public void setBpp(final int bpp) {
        super.setBpp(bpp);
        for (int i = 0; i < this.filter.length; ++i) {
            this.filter[i].setBpp(bpp);
        }
    }
    
    @Override
    public void setHeight(final int height) {
        super.setHeight(height);
        for (int i = 0; i < this.filter.length; ++i) {
            this.filter[i].setHeight(height);
        }
    }
    
    @Override
    public void setWidth(final int width) {
        super.setWidth(width);
        for (int i = 0; i < this.filter.length; ++i) {
            this.filter[i].setWidth(width);
        }
    }
    
    @Override
    public void decode(final byte[] src, final byte[] dest) {
        this.checkBufsiz(src, dest);
        final int bpl = this.getWidth() * this.getBpp();
        final int srcDy = bpl + 1;
        for (int y = 0; y < this.getHeight(); ++y) {
            final PredictorAlgorithm f = this.filter[src[y * srcDy]];
            final int srcOffset = y * srcDy + 1;
            f.decodeLine(src, dest, srcDy, srcOffset, bpl, y * bpl);
        }
    }
}
