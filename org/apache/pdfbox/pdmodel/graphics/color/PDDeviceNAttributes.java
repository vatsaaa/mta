// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.graphics.color;

import java.io.IOException;
import java.util.Iterator;
import org.apache.pdfbox.pdmodel.common.COSDictionaryMap;
import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSName;
import java.util.HashMap;
import java.util.Map;
import org.apache.pdfbox.cos.COSDictionary;

public class PDDeviceNAttributes
{
    private COSDictionary dictionary;
    
    public PDDeviceNAttributes() {
        this.dictionary = new COSDictionary();
    }
    
    public PDDeviceNAttributes(final COSDictionary attributes) {
        this.dictionary = attributes;
    }
    
    public COSDictionary getCOSDictionary() {
        return this.dictionary;
    }
    
    public Map getColorants() throws IOException {
        final Map actuals = new HashMap();
        COSDictionary colorants = (COSDictionary)this.dictionary.getDictionaryObject(COSName.COLORANTS);
        if (colorants == null) {
            colorants = new COSDictionary();
            this.dictionary.setItem(COSName.COLORANTS, colorants);
        }
        for (final COSName name : colorants.keySet()) {
            final COSBase value = colorants.getDictionaryObject(name);
            actuals.put(name.getName(), PDColorSpaceFactory.createColorSpace(value));
        }
        return new COSDictionaryMap(actuals, colorants);
    }
    
    public void setColorants(final Map colorants) {
        COSDictionary colorantDict = null;
        if (colorants != null) {
            colorantDict = COSDictionaryMap.convert(colorants);
        }
        this.dictionary.setItem(COSName.COLORANTS, colorantDict);
    }
}
