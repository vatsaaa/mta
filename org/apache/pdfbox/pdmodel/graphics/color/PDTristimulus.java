// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.graphics.color;

import org.apache.pdfbox.cos.COSNumber;
import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSFloat;
import org.apache.pdfbox.cos.COSArray;
import org.apache.pdfbox.pdmodel.common.COSObjectable;

public class PDTristimulus implements COSObjectable
{
    private COSArray values;
    
    public PDTristimulus() {
        this.values = null;
        (this.values = new COSArray()).add(new COSFloat(0.0f));
        this.values.add(new COSFloat(0.0f));
        this.values.add(new COSFloat(0.0f));
    }
    
    public PDTristimulus(final COSArray array) {
        this.values = null;
        this.values = array;
    }
    
    public PDTristimulus(final float[] array) {
        this.values = null;
        this.values = new COSArray();
        for (int i = 0; i < array.length && i < 3; ++i) {
            this.values.add(new COSFloat(array[i]));
        }
    }
    
    public COSBase getCOSObject() {
        return this.values;
    }
    
    public float getX() {
        return ((COSNumber)this.values.get(0)).floatValue();
    }
    
    public void setX(final float x) {
        this.values.set(0, new COSFloat(x));
    }
    
    public float getY() {
        return ((COSNumber)this.values.get(1)).floatValue();
    }
    
    public void setY(final float y) {
        this.values.set(1, new COSFloat(y));
    }
    
    public float getZ() {
        return ((COSNumber)this.values.get(2)).floatValue();
    }
    
    public void setZ(final float z) {
        this.values.set(2, new COSFloat(z));
    }
}
