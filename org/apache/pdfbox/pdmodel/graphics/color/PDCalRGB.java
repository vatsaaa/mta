// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.graphics.color;

import org.apache.pdfbox.pdmodel.common.PDMatrix;
import org.apache.pdfbox.cos.COSFloat;
import java.awt.image.ComponentColorModel;
import java.awt.image.ColorModel;
import java.awt.color.ColorSpace;
import java.io.IOException;
import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.cos.COSDictionary;
import org.apache.pdfbox.cos.COSArray;

public class PDCalRGB extends PDColorSpace
{
    public static final String NAME = "CalRGB";
    private COSArray array;
    private COSDictionary dictionary;
    
    public PDCalRGB() {
        this.array = new COSArray();
        this.dictionary = new COSDictionary();
        this.array.add(COSName.CALRGB);
        this.array.add(this.dictionary);
    }
    
    public PDCalRGB(final COSArray rgb) {
        this.array = rgb;
        this.dictionary = (COSDictionary)this.array.getObject(1);
    }
    
    @Override
    public int getNumberOfComponents() throws IOException {
        return 3;
    }
    
    @Override
    public String getName() {
        return "CalRGB";
    }
    
    @Override
    protected ColorSpace createColorSpace() {
        return new ColorSpaceCalRGB(this.getGamma(), this.getWhitepoint(), this.getBlackPoint(), this.getLinearInterpretation());
    }
    
    @Override
    public ColorModel createColorModel(final int bpc) throws IOException {
        final int[] nBits = { bpc, bpc, bpc };
        return new ComponentColorModel(this.getJavaColorSpace(), nBits, false, false, 1, 0);
    }
    
    @Override
    public COSBase getCOSObject() {
        return this.array;
    }
    
    public PDTristimulus getWhitepoint() {
        COSArray wp = (COSArray)this.dictionary.getDictionaryObject(COSName.WHITE_POINT);
        if (wp == null) {
            wp = new COSArray();
            wp.add(new COSFloat(1.0f));
            wp.add(new COSFloat(1.0f));
            wp.add(new COSFloat(1.0f));
            this.dictionary.setItem(COSName.WHITE_POINT, wp);
        }
        return new PDTristimulus(wp);
    }
    
    public void setWhitepoint(final PDTristimulus wp) {
        final COSBase wpArray = wp.getCOSObject();
        if (wpArray != null) {
            this.dictionary.setItem(COSName.WHITE_POINT, wpArray);
        }
    }
    
    public PDTristimulus getBlackPoint() {
        COSArray bp = (COSArray)this.dictionary.getDictionaryObject(COSName.BLACK_POINT);
        if (bp == null) {
            bp = new COSArray();
            bp.add(new COSFloat(0.0f));
            bp.add(new COSFloat(0.0f));
            bp.add(new COSFloat(0.0f));
            this.dictionary.setItem(COSName.BLACK_POINT, bp);
        }
        return new PDTristimulus(bp);
    }
    
    public void setBlackPoint(final PDTristimulus bp) {
        COSBase bpArray = null;
        if (bp != null) {
            bpArray = bp.getCOSObject();
        }
        this.dictionary.setItem(COSName.BLACK_POINT, bpArray);
    }
    
    public PDGamma getGamma() {
        COSArray gamma = (COSArray)this.dictionary.getDictionaryObject(COSName.GAMMA);
        if (gamma == null) {
            gamma = new COSArray();
            gamma.add(new COSFloat(1.0f));
            gamma.add(new COSFloat(1.0f));
            gamma.add(new COSFloat(1.0f));
            this.dictionary.setItem(COSName.GAMMA, gamma);
        }
        return new PDGamma(gamma);
    }
    
    public void setGamma(final PDGamma value) {
        COSArray gamma = null;
        if (value != null) {
            gamma = value.getCOSArray();
        }
        this.dictionary.setItem(COSName.GAMMA, gamma);
    }
    
    public PDMatrix getLinearInterpretation() {
        PDMatrix retval = null;
        final COSArray matrix = (COSArray)this.dictionary.getDictionaryObject(COSName.MATRIX);
        if (matrix == null) {
            retval = new PDMatrix();
            this.setLinearInterpretation(retval);
        }
        else {
            retval = new PDMatrix(matrix);
        }
        return retval;
    }
    
    public void setLinearInterpretation(final PDMatrix matrix) {
        COSArray matrixArray = null;
        if (matrix != null) {
            matrixArray = matrix.getCOSArray();
        }
        this.dictionary.setItem(COSName.MATRIX, matrixArray);
    }
}
