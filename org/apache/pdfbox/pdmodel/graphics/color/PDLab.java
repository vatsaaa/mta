// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.graphics.color;

import org.apache.pdfbox.pdmodel.common.PDRange;
import org.apache.pdfbox.cos.COSFloat;
import java.awt.image.ColorModel;
import java.io.IOException;
import java.awt.color.ColorSpace;
import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.cos.COSDictionary;
import org.apache.pdfbox.cos.COSArray;

public class PDLab extends PDColorSpace
{
    public static final String NAME = "Lab";
    private COSArray array;
    private COSDictionary dictionary;
    
    public PDLab() {
        this.array = new COSArray();
        this.dictionary = new COSDictionary();
        this.array.add(COSName.LAB);
        this.array.add(this.dictionary);
    }
    
    public PDLab(final COSArray lab) {
        this.array = lab;
        this.dictionary = (COSDictionary)this.array.getObject(1);
    }
    
    @Override
    public String getName() {
        return "Lab";
    }
    
    @Override
    public COSBase getCOSObject() {
        return this.array;
    }
    
    @Override
    protected ColorSpace createColorSpace() throws IOException {
        throw new IOException("Not implemented");
    }
    
    @Override
    public ColorModel createColorModel(final int bpc) throws IOException {
        throw new IOException("Not implemented");
    }
    
    @Override
    public int getNumberOfComponents() throws IOException {
        return 3;
    }
    
    public PDTristimulus getWhitepoint() {
        COSArray wp = (COSArray)this.dictionary.getDictionaryObject(COSName.WHITE_POINT);
        if (wp == null) {
            wp = new COSArray();
            wp.add(new COSFloat(1.0f));
            wp.add(new COSFloat(1.0f));
            wp.add(new COSFloat(1.0f));
            this.dictionary.setItem(COSName.WHITE_POINT, wp);
        }
        return new PDTristimulus(wp);
    }
    
    public void setWhitepoint(final PDTristimulus wp) {
        final COSBase wpArray = wp.getCOSObject();
        if (wpArray != null) {
            this.dictionary.setItem(COSName.WHITE_POINT, wpArray);
        }
    }
    
    public PDTristimulus getBlackPoint() {
        COSArray bp = (COSArray)this.dictionary.getDictionaryObject(COSName.BLACK_POINT);
        if (bp == null) {
            bp = new COSArray();
            bp.add(new COSFloat(0.0f));
            bp.add(new COSFloat(0.0f));
            bp.add(new COSFloat(0.0f));
            this.dictionary.setItem(COSName.BLACK_POINT, bp);
        }
        return new PDTristimulus(bp);
    }
    
    public void setBlackPoint(final PDTristimulus bp) {
        COSBase bpArray = null;
        if (bp != null) {
            bpArray = bp.getCOSObject();
        }
        this.dictionary.setItem(COSName.BLACK_POINT, bpArray);
    }
    
    private COSArray getRangeArray() {
        COSArray range = (COSArray)this.dictionary.getDictionaryObject(COSName.RANGE);
        if (range == null) {
            range = new COSArray();
            this.dictionary.setItem(COSName.RANGE, this.array);
            range.add(new COSFloat(-100.0f));
            range.add(new COSFloat(100.0f));
            range.add(new COSFloat(-100.0f));
            range.add(new COSFloat(100.0f));
        }
        return range;
    }
    
    public PDRange getARange() {
        final COSArray range = this.getRangeArray();
        return new PDRange(range, 0);
    }
    
    public void setARange(final PDRange range) {
        COSArray rangeArray = null;
        if (range == null) {
            rangeArray = this.getRangeArray();
            rangeArray.set(0, new COSFloat(-100.0f));
            rangeArray.set(1, new COSFloat(100.0f));
        }
        else {
            rangeArray = range.getCOSArray();
        }
        this.dictionary.setItem(COSName.RANGE, rangeArray);
    }
    
    public PDRange getBRange() {
        final COSArray range = this.getRangeArray();
        return new PDRange(range, 1);
    }
    
    public void setBRange(final PDRange range) {
        COSArray rangeArray = null;
        if (range == null) {
            rangeArray = this.getRangeArray();
            rangeArray.set(2, new COSFloat(-100.0f));
            rangeArray.set(3, new COSFloat(100.0f));
        }
        else {
            rangeArray = range.getCOSArray();
        }
        this.dictionary.setItem(COSName.RANGE, rangeArray);
    }
}
