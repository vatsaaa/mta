// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.graphics.color;

import java.awt.color.ColorSpace;

public class ColorSpaceCMYK extends ColorSpace
{
    private static final long serialVersionUID = -6362864473145799405L;
    
    public ColorSpaceCMYK() {
        super(9, 4);
    }
    
    private float[] fromRGBtoCIEXYZ(final float[] rgbvalue) {
        final ColorSpace colorspaceRGB = ColorSpace.getInstance(1000);
        return colorspaceRGB.toCIEXYZ(rgbvalue);
    }
    
    private float[] fromCIEXYZtoRGB(final float[] xyzvalue) {
        final ColorSpace colorspaceXYZ = ColorSpace.getInstance(1001);
        return colorspaceXYZ.toRGB(xyzvalue);
    }
    
    @Override
    public float[] fromCIEXYZ(final float[] colorvalue) {
        if (colorvalue != null && colorvalue.length == 3) {
            return this.fromRGB(this.fromCIEXYZtoRGB(colorvalue));
        }
        return null;
    }
    
    @Override
    public float[] fromRGB(final float[] rgbvalue) {
        if (rgbvalue != null && rgbvalue.length == 3) {
            final float c = 1.0f - rgbvalue[0];
            final float m = 1.0f - rgbvalue[1];
            final float y = 1.0f - rgbvalue[2];
            float varK = 1.0f;
            final float[] cmyk = new float[4];
            if (c < varK) {
                varK = c;
            }
            if (m < varK) {
                varK = m;
            }
            if (y < varK) {
                varK = y;
            }
            if (varK == 1.0f) {
                final float[] array = cmyk;
                final int n = 0;
                final float[] array2 = cmyk;
                final int n2 = 1;
                final float[] array3 = cmyk;
                final int n3 = 2;
                final float n4 = 0.0f;
                array3[n3] = n4;
                array[n] = (array2[n2] = n4);
            }
            else {
                cmyk[0] = (c - varK) / (1.0f - varK);
                cmyk[1] = (m - varK) / (1.0f - varK);
                cmyk[2] = (y - varK) / (1.0f - varK);
            }
            cmyk[3] = varK;
            return cmyk;
        }
        return null;
    }
    
    @Override
    public float[] toCIEXYZ(final float[] colorvalue) {
        if (colorvalue != null && colorvalue.length == 4) {
            return this.fromRGBtoCIEXYZ(this.toRGB(colorvalue));
        }
        return null;
    }
    
    @Override
    public float[] toRGB(final float[] colorvalue) {
        if (colorvalue != null && colorvalue.length == 4) {
            final float k = colorvalue[3];
            final float c = colorvalue[0] * (1.0f - k) + k;
            final float m = colorvalue[1] * (1.0f - k) + k;
            final float y = colorvalue[2] * (1.0f - k) + k;
            return new float[] { 1.0f - c, 1.0f - m, 1.0f - y };
        }
        return null;
    }
}
