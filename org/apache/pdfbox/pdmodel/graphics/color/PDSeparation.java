// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.graphics.color;

import org.apache.commons.logging.LogFactory;
import org.apache.pdfbox.pdmodel.common.COSObjectable;
import org.apache.pdfbox.pdmodel.common.function.PDFunction;
import java.awt.image.ColorModel;
import java.awt.color.ColorSpace;
import java.io.IOException;
import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.cos.COSArray;
import org.apache.commons.logging.Log;

public class PDSeparation extends PDColorSpace
{
    private static final Log log;
    public static final String NAME = "Separation";
    
    public PDSeparation() {
        (this.array = new COSArray()).add(COSName.SEPARATION);
        this.array.add(COSName.getPDFName(""));
    }
    
    public PDSeparation(final COSArray separation) {
        this.array = separation;
    }
    
    @Override
    public String getName() {
        return "Separation";
    }
    
    @Override
    public int getNumberOfComponents() throws IOException {
        return this.getAlternateColorSpace().getNumberOfComponents();
    }
    
    @Override
    protected ColorSpace createColorSpace() throws IOException {
        try {
            final PDColorSpace alt = this.getAlternateColorSpace();
            return alt.getJavaColorSpace();
        }
        catch (IOException ioexception) {
            PDSeparation.log.error(ioexception, ioexception);
            throw ioexception;
        }
        catch (Exception exception) {
            PDSeparation.log.error(exception, exception);
            throw new IOException("Failed to Create ColorSpace");
        }
    }
    
    @Override
    public ColorModel createColorModel(final int bpc) throws IOException {
        PDSeparation.log.info("About to create ColorModel for " + this.getAlternateColorSpace().toString());
        return this.getAlternateColorSpace().createColorModel(bpc);
    }
    
    public String getColorantName() {
        final COSName name = (COSName)this.array.getObject(1);
        return name.getName();
    }
    
    public void setColorantName(final String name) {
        this.array.set(1, COSName.getPDFName(name));
    }
    
    public PDColorSpace getAlternateColorSpace() throws IOException {
        final COSBase alternate = this.array.getObject(2);
        final PDColorSpace cs = PDColorSpaceFactory.createColorSpace(alternate);
        return cs;
    }
    
    public void setAlternateColorSpace(final PDColorSpace cs) {
        COSBase space = null;
        if (cs != null) {
            space = cs.getCOSObject();
        }
        this.array.set(2, space);
    }
    
    public PDFunction getTintTransform() throws IOException {
        return PDFunction.create(this.array.getObject(3));
    }
    
    public void setTintTransform(final PDFunction tint) {
        this.array.set(3, tint);
    }
    
    public COSArray calculateColorValues(final COSBase tintValue) throws IOException {
        final PDFunction tintTransform = this.getTintTransform();
        final COSArray tint = new COSArray();
        tint.add(tintValue);
        return tintTransform.eval(tint);
    }
    
    static {
        log = LogFactory.getLog(PDSeparation.class);
    }
}
