// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.graphics.color;

import org.apache.pdfbox.cos.COSFloat;
import org.apache.pdfbox.cos.COSNumber;
import java.awt.image.ColorModel;
import java.awt.color.ColorSpace;
import java.io.IOException;
import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.cos.COSDictionary;
import org.apache.pdfbox.cos.COSArray;

public class PDCalGray extends PDColorSpace
{
    public static final String NAME = "CalGray";
    private COSArray array;
    private COSDictionary dictionary;
    
    public PDCalGray() {
        this.array = new COSArray();
        this.dictionary = new COSDictionary();
        this.array.add(COSName.CALGRAY);
        this.array.add(this.dictionary);
    }
    
    public PDCalGray(final COSArray gray) {
        this.array = gray;
        this.dictionary = (COSDictionary)this.array.getObject(1);
    }
    
    @Override
    public int getNumberOfComponents() throws IOException {
        return 1;
    }
    
    @Override
    public String getName() {
        return "CalGray";
    }
    
    @Override
    protected ColorSpace createColorSpace() throws IOException {
        throw new IOException("Not implemented");
    }
    
    @Override
    public ColorModel createColorModel(final int bpc) throws IOException {
        throw new IOException("Not implemented");
    }
    
    @Override
    public COSBase getCOSObject() {
        return this.array;
    }
    
    public float getGamma() {
        float retval = 1.0f;
        final COSNumber gamma = (COSNumber)this.dictionary.getDictionaryObject(COSName.GAMMA);
        if (gamma != null) {
            retval = gamma.floatValue();
        }
        return retval;
    }
    
    public void setGamma(final float value) {
        this.dictionary.setItem(COSName.GAMMA, new COSFloat(value));
    }
    
    public PDTristimulus getWhitepoint() {
        COSArray wp = (COSArray)this.dictionary.getDictionaryObject(COSName.WHITE_POINT);
        if (wp == null) {
            wp = new COSArray();
            wp.add(new COSFloat(1.0f));
            wp.add(new COSFloat(1.0f));
            wp.add(new COSFloat(1.0f));
            this.dictionary.setItem(COSName.WHITE_POINT, wp);
        }
        return new PDTristimulus(wp);
    }
    
    public void setWhitepoint(final PDTristimulus wp) {
        final COSBase wpArray = wp.getCOSObject();
        if (wpArray != null) {
            this.dictionary.setItem(COSName.WHITE_POINT, wpArray);
        }
    }
    
    public PDTristimulus getBlackPoint() {
        COSArray bp = (COSArray)this.dictionary.getDictionaryObject(COSName.BLACK_POINT);
        if (bp == null) {
            bp = new COSArray();
            bp.add(new COSFloat(0.0f));
            bp.add(new COSFloat(0.0f));
            bp.add(new COSFloat(0.0f));
            this.dictionary.setItem(COSName.BLACK_POINT, bp);
        }
        return new PDTristimulus(bp);
    }
    
    public void setBlackPoint(final PDTristimulus bp) {
        COSBase bpArray = null;
        if (bp != null) {
            bpArray = bp.getCOSObject();
        }
        this.dictionary.setItem(COSName.BLACK_POINT, bpArray);
    }
}
