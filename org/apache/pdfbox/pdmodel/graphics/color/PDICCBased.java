// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.graphics.color;

import org.apache.commons.logging.LogFactory;
import org.apache.pdfbox.pdmodel.common.PDRange;
import org.apache.pdfbox.cos.COSFloat;
import org.apache.pdfbox.pdmodel.common.COSArrayList;
import java.util.ArrayList;
import java.awt.image.ComponentColorModel;
import java.awt.image.ColorModel;
import java.io.IOException;
import java.util.List;
import java.io.InputStream;
import java.awt.Color;
import java.awt.color.ICC_ColorSpace;
import java.awt.color.ICC_Profile;
import java.awt.color.ColorSpace;
import org.apache.pdfbox.cos.COSStream;
import org.apache.pdfbox.pdmodel.common.COSObjectable;
import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.cos.COSArray;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.common.PDStream;
import org.apache.commons.logging.Log;

public class PDICCBased extends PDColorSpace
{
    private static final Log LOG;
    public static final String NAME = "ICCBased";
    private PDStream stream;
    private int numberOfComponents;
    
    public PDICCBased(final PDDocument doc) {
        this.numberOfComponents = -1;
        (this.array = new COSArray()).add(COSName.ICCBASED);
        this.array.add(new PDStream(doc));
    }
    
    public PDICCBased(final COSArray iccArray) {
        this.numberOfComponents = -1;
        this.array = iccArray;
        this.stream = new PDStream((COSStream)iccArray.getObject(1));
    }
    
    @Override
    public String getName() {
        return "ICCBased";
    }
    
    @Override
    public COSBase getCOSObject() {
        return this.array;
    }
    
    public PDStream getPDStream() {
        return this.stream;
    }
    
    @Override
    protected ColorSpace createColorSpace() throws IOException {
        InputStream profile = null;
        ColorSpace cSpace = null;
        try {
            profile = this.stream.createInputStream();
            final ICC_Profile iccProfile = ICC_Profile.getInstance(profile);
            cSpace = new ICC_ColorSpace(iccProfile);
            final float[] components = new float[this.numberOfComponents];
            new Color(cSpace, components, 1.0f);
        }
        catch (RuntimeException e) {
            PDICCBased.LOG.debug("Can't read ICC-profile, using alternate colorspace instead");
            final List alternateCSList = this.getAlternateColorSpaces();
            final PDColorSpace alternate = alternateCSList.get(0);
            cSpace = alternate.getJavaColorSpace();
        }
        finally {
            if (profile != null) {
                profile.close();
            }
        }
        return cSpace;
    }
    
    @Override
    public ColorModel createColorModel(final int bpc) throws IOException {
        final int numOfComponents = this.getNumberOfComponents();
        int[] nbBits = null;
        switch (numOfComponents) {
            case 1: {
                nbBits = new int[] { bpc };
                break;
            }
            case 3: {
                nbBits = new int[] { bpc, bpc, bpc };
                break;
            }
            case 4: {
                nbBits = new int[] { bpc, bpc, bpc, bpc };
                break;
            }
            default: {
                throw new IOException("Unknown colorspace number of components:" + numOfComponents);
            }
        }
        final ComponentColorModel componentColorModel = new ComponentColorModel(this.getJavaColorSpace(), nbBits, false, false, 1, 0);
        return componentColorModel;
    }
    
    @Override
    public int getNumberOfComponents() throws IOException {
        if (this.numberOfComponents < 0) {
            this.numberOfComponents = this.stream.getStream().getInt(COSName.N);
        }
        return this.numberOfComponents;
    }
    
    public void setNumberOfComponents(final int n) {
        this.numberOfComponents = n;
        this.stream.getStream().setInt(COSName.N, n);
    }
    
    public List getAlternateColorSpaces() throws IOException {
        final COSBase alternate = this.stream.getStream().getDictionaryObject(COSName.ALTERNATE);
        COSArray alternateArray = null;
        if (alternate == null) {
            alternateArray = new COSArray();
            final int numComponents = this.getNumberOfComponents();
            COSName csName = null;
            if (numComponents == 1) {
                csName = COSName.DEVICEGRAY;
            }
            else if (numComponents == 3) {
                csName = COSName.DEVICERGB;
            }
            else {
                if (numComponents != 4) {
                    throw new IOException("Unknown colorspace number of components:" + numComponents);
                }
                csName = COSName.DEVICECMYK;
            }
            alternateArray.add(csName);
        }
        else if (alternate instanceof COSArray) {
            alternateArray = (COSArray)alternate;
        }
        else {
            if (!(alternate instanceof COSName)) {
                throw new IOException("Error: expected COSArray or COSName and not " + alternate.getClass().getName());
            }
            alternateArray = new COSArray();
            alternateArray.add(alternate);
        }
        final List retval = new ArrayList();
        for (int i = 0; i < alternateArray.size(); ++i) {
            retval.add(PDColorSpaceFactory.createColorSpace(alternateArray.get(i)));
        }
        return new COSArrayList(retval, alternateArray);
    }
    
    public void setAlternateColorSpaces(final List list) {
        COSArray altArray = null;
        if (list != null) {
            altArray = COSArrayList.converterToCOSArray(list);
        }
        this.stream.getStream().setItem(COSName.ALTERNATE, altArray);
    }
    
    private COSArray getRangeArray(final int n) {
        COSArray rangeArray = (COSArray)this.stream.getStream().getDictionaryObject(COSName.RANGE);
        if (rangeArray == null) {
            rangeArray = new COSArray();
            this.stream.getStream().setItem(COSName.RANGE, rangeArray);
            while (rangeArray.size() < n * 2) {
                rangeArray.add(new COSFloat(-100.0f));
                rangeArray.add(new COSFloat(100.0f));
            }
        }
        return rangeArray;
    }
    
    public PDRange getRangeForComponent(final int n) {
        final COSArray rangeArray = this.getRangeArray(n);
        return new PDRange(rangeArray, n);
    }
    
    public void setRangeForComponent(final PDRange range, final int n) {
        final COSArray rangeArray = this.getRangeArray(n);
        rangeArray.set(n * 2, new COSFloat(range.getMin()));
        rangeArray.set(n * 2 + 1, new COSFloat(range.getMax()));
    }
    
    public COSStream getMetadata() {
        return (COSStream)this.stream.getStream().getDictionaryObject(COSName.METADATA);
    }
    
    public void setMetadata(final COSStream metadata) {
        this.stream.getStream().setItem(COSName.METADATA, metadata);
    }
    
    @Override
    public String toString() {
        String retVal = super.toString() + "\n\t Number of Components: ";
        try {
            retVal += this.getNumberOfComponents();
        }
        catch (IOException exception) {
            retVal += exception.toString();
        }
        return retVal;
    }
    
    static {
        LOG = LogFactory.getLog(PDICCBased.class);
    }
}
