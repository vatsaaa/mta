// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.graphics.color;

import java.io.InputStream;
import java.io.ByteArrayOutputStream;
import org.apache.pdfbox.cos.COSStream;
import org.apache.pdfbox.cos.COSString;
import org.apache.pdfbox.cos.COSNumber;
import java.awt.image.IndexColorModel;
import java.awt.image.ColorModel;
import java.awt.color.ColorSpace;
import java.io.IOException;
import org.apache.pdfbox.cos.COSNull;
import org.apache.pdfbox.cos.COSInteger;
import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.cos.COSArray;

public class PDIndexed extends PDColorSpace
{
    public static final String NAME = "Indexed";
    public static final String ABBREVIATED_NAME = "I";
    private COSArray array;
    private byte[] lookupData;
    
    public PDIndexed() {
        (this.array = new COSArray()).add(COSName.INDEXED);
        this.array.add(COSName.DEVICERGB);
        this.array.add(COSInteger.get(255L));
        this.array.add(COSNull.NULL);
    }
    
    public PDIndexed(final COSArray indexedArray) {
        this.array = indexedArray;
    }
    
    @Override
    public int getNumberOfComponents() throws IOException {
        return this.getBaseColorSpace().getNumberOfComponents();
    }
    
    @Override
    public String getName() {
        return "Indexed";
    }
    
    @Override
    protected ColorSpace createColorSpace() throws IOException {
        throw new IOException("Not implemented");
    }
    
    @Override
    public ColorModel createColorModel(final int bpc) throws IOException {
        final int size = this.getHighValue();
        final byte[] index = this.getLookupData();
        final PDColorSpace baseColorSpace = this.getBaseColorSpace();
        ColorModel cm = null;
        if (baseColorSpace instanceof PDDeviceRGB) {
            cm = new IndexColorModel(bpc, size + 1, index, 0, false);
        }
        else {
            final ColorModel baseColorModel = baseColorSpace.createColorModel(bpc);
            if (baseColorModel.getTransferType() != 0) {
                throw new IOException("Not implemented");
            }
            final byte[] r = new byte[size + 1];
            final byte[] g = new byte[size + 1];
            final byte[] b = new byte[size + 1];
            final byte[] a = (byte[])(baseColorModel.hasAlpha() ? new byte[size + 1] : null);
            final byte[] inData = new byte[baseColorModel.getNumComponents()];
            for (int i = 0; i <= size; ++i) {
                System.arraycopy(index, i * inData.length, inData, 0, inData.length);
                r[i] = (byte)baseColorModel.getRed(inData);
                g[i] = (byte)baseColorModel.getGreen(inData);
                b[i] = (byte)baseColorModel.getBlue(inData);
                if (a != null) {
                    a[i] = (byte)baseColorModel.getAlpha(inData);
                }
            }
            cm = ((a == null) ? new IndexColorModel(bpc, size + 1, r, g, b) : new IndexColorModel(bpc, size + 1, r, g, b, a));
        }
        return cm;
    }
    
    public PDColorSpace getBaseColorSpace() throws IOException {
        final COSBase base = this.array.getObject(1);
        return PDColorSpaceFactory.createColorSpace(base);
    }
    
    public void setBaseColorSpace(final PDColorSpace base) {
        this.array.set(1, base.getCOSObject());
    }
    
    public int getHighValue() {
        return ((COSNumber)this.array.getObject(2)).intValue();
    }
    
    public void setHighValue(final int high) {
        this.array.set(2, high);
    }
    
    public int lookupColor(final int lookupIndex, final int componentNumber) throws IOException {
        final PDColorSpace baseColor = this.getBaseColorSpace();
        final byte[] data = this.getLookupData();
        final int numberOfComponents = baseColor.getNumberOfComponents();
        return (data[lookupIndex * numberOfComponents + componentNumber] + 256) % 256;
    }
    
    public byte[] getLookupData() throws IOException {
        if (this.lookupData == null) {
            final COSBase lookupTable = this.array.getObject(3);
            if (lookupTable instanceof COSString) {
                this.lookupData = ((COSString)lookupTable).getBytes();
            }
            else if (lookupTable instanceof COSStream) {
                final COSStream lookupStream = (COSStream)lookupTable;
                final InputStream input = lookupStream.getUnfilteredStream();
                final ByteArrayOutputStream output = new ByteArrayOutputStream(1024);
                final byte[] buffer = new byte[1024];
                int amountRead;
                while ((amountRead = input.read(buffer, 0, buffer.length)) != -1) {
                    output.write(buffer, 0, amountRead);
                }
                this.lookupData = output.toByteArray();
            }
            else {
                if (lookupTable != null) {
                    throw new IOException("Error: Unknown type for lookup table " + lookupTable);
                }
                this.lookupData = new byte[0];
            }
        }
        return this.lookupData;
    }
    
    public void setLookupColor(final int lookupIndex, final int componentNumber, final int color) throws IOException {
        final PDColorSpace baseColor = this.getBaseColorSpace();
        final int numberOfComponents = baseColor.getNumberOfComponents();
        final byte[] data = this.getLookupData();
        data[lookupIndex * numberOfComponents + componentNumber] = (byte)color;
        final COSString string = new COSString(data);
        this.array.set(3, string);
    }
}
