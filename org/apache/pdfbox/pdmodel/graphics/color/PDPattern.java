// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.graphics.color;

import java.awt.image.ColorModel;
import java.awt.color.ColorSpace;
import java.io.IOException;
import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.cos.COSArray;

public class PDPattern extends PDColorSpace
{
    private COSArray array;
    public static final String NAME = "Pattern";
    
    public PDPattern() {
        (this.array = new COSArray()).add(COSName.PATTERN);
    }
    
    public PDPattern(final COSArray pattern) {
        this.array = pattern;
    }
    
    @Override
    public String getName() {
        return "Pattern";
    }
    
    @Override
    public int getNumberOfComponents() throws IOException {
        return -1;
    }
    
    @Override
    protected ColorSpace createColorSpace() throws IOException {
        throw new IOException("Not implemented");
    }
    
    @Override
    public ColorModel createColorModel(final int bpc) throws IOException {
        throw new IOException("Not implemented");
    }
}
