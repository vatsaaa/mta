// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.graphics.color;

import org.apache.pdfbox.pdmodel.common.PDMatrix;
import java.awt.color.ColorSpace;

public class ColorSpaceCalRGB extends ColorSpace
{
    private PDGamma gamma;
    private PDTristimulus whitepoint;
    private PDTristimulus blackpoint;
    private PDMatrix matrix;
    private static final long serialVersionUID = -6362864473145799405L;
    
    public ColorSpaceCalRGB() {
        super(13, 3);
        this.gamma = null;
        this.whitepoint = null;
        this.blackpoint = null;
        this.matrix = null;
    }
    
    public ColorSpaceCalRGB(final PDGamma gammaValue, final PDTristimulus whitept, final PDTristimulus blackpt, final PDMatrix linearMatrix) {
        this();
        this.gamma = gammaValue;
        this.whitepoint = whitept;
        this.blackpoint = blackpt;
        this.matrix = linearMatrix;
    }
    
    private float[] fromRGBtoCIEXYZ(final float[] rgbvalue) {
        final ColorSpace colorspaceRGB = ColorSpace.getInstance(1000);
        return colorspaceRGB.toCIEXYZ(rgbvalue);
    }
    
    private float[] fromCIEXYZtoRGB(final float[] xyzvalue) {
        final ColorSpace colorspaceXYZ = ColorSpace.getInstance(1001);
        return colorspaceXYZ.toRGB(xyzvalue);
    }
    
    @Override
    public float[] fromCIEXYZ(final float[] colorvalue) {
        if (colorvalue != null && colorvalue.length == 3) {
            return this.fromCIEXYZtoRGB(colorvalue);
        }
        return null;
    }
    
    @Override
    public float[] fromRGB(final float[] rgbvalue) {
        if (rgbvalue != null && rgbvalue.length == 3) {
            return rgbvalue;
        }
        return null;
    }
    
    @Override
    public float[] toCIEXYZ(final float[] colorvalue) {
        if (colorvalue != null && colorvalue.length == 4) {
            return this.fromRGBtoCIEXYZ(this.toRGB(colorvalue));
        }
        return null;
    }
    
    @Override
    public float[] toRGB(final float[] colorvalue) {
        if (colorvalue != null && colorvalue.length == 3) {
            return colorvalue;
        }
        return null;
    }
}
