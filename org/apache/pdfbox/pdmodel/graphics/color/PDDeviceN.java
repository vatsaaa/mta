// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.graphics.color;

import org.apache.commons.logging.LogFactory;
import java.util.Collection;
import org.apache.pdfbox.cos.COSNull;
import org.apache.pdfbox.pdmodel.common.COSObjectable;
import org.apache.pdfbox.pdmodel.common.function.PDFunction;
import org.apache.pdfbox.pdmodel.common.COSArrayList;
import java.util.List;
import java.awt.image.ColorModel;
import java.awt.color.ColorSpace;
import java.io.IOException;
import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.cos.COSArray;
import org.apache.commons.logging.Log;

public class PDDeviceN extends PDColorSpace
{
    private static final Log LOG;
    private static final int COLORANT_NAMES = 1;
    private static final int ALTERNATE_CS = 2;
    private static final int TINT_TRANSFORM = 3;
    private static final int DEVICEN_ATTRIBUTES = 4;
    public static final String NAME = "DeviceN";
    private COSArray array;
    
    public PDDeviceN() {
        (this.array = new COSArray()).add(COSName.DEVICEN);
        this.array.add(COSName.getPDFName(""));
    }
    
    public PDDeviceN(final COSArray separation) {
        this.array = separation;
    }
    
    @Override
    public String getName() {
        return "DeviceN";
    }
    
    @Override
    public int getNumberOfComponents() throws IOException {
        return this.getColorantNames().size();
    }
    
    @Override
    protected ColorSpace createColorSpace() throws IOException {
        try {
            final PDColorSpace alt = this.getAlternateColorSpace();
            return alt.getJavaColorSpace();
        }
        catch (IOException ioexception) {
            PDDeviceN.LOG.error(ioexception, ioexception);
            throw ioexception;
        }
        catch (Exception exception) {
            PDDeviceN.LOG.error(exception, exception);
            throw new IOException("Failed to Create ColorSpace");
        }
    }
    
    @Override
    public ColorModel createColorModel(final int bpc) throws IOException {
        PDDeviceN.LOG.info("About to create ColorModel for " + this.getAlternateColorSpace().toString());
        return this.getAlternateColorSpace().createColorModel(bpc);
    }
    
    public List<COSBase> getColorantNames() {
        final COSArray names = (COSArray)this.array.getObject(1);
        return (List<COSBase>)COSArrayList.convertCOSNameCOSArrayToList(names);
    }
    
    public void setColorantNames(final List<COSBase> names) {
        final COSArray namesArray = COSArrayList.convertStringListToCOSNameCOSArray(names);
        this.array.set(1, namesArray);
    }
    
    public PDColorSpace getAlternateColorSpace() throws IOException {
        final COSBase alternate = this.array.getObject(2);
        return PDColorSpaceFactory.createColorSpace(alternate);
    }
    
    public void setAlternateColorSpace(final PDColorSpace cs) {
        COSBase space = null;
        if (cs != null) {
            space = cs.getCOSObject();
        }
        this.array.set(2, space);
    }
    
    public PDFunction getTintTransform() throws IOException {
        return PDFunction.create(this.array.getObject(3));
    }
    
    public void setTintTransform(final PDFunction tint) {
        this.array.set(3, tint);
    }
    
    public PDDeviceNAttributes getAttributes() {
        PDDeviceNAttributes retval = null;
        if (this.array.size() <= 4) {
            retval = new PDDeviceNAttributes();
            this.setAttributes(retval);
        }
        return retval;
    }
    
    public void setAttributes(final PDDeviceNAttributes attributes) {
        if (attributes == null) {
            this.array.remove(4);
        }
        else {
            while (this.array.size() <= 4) {
                this.array.add(COSNull.NULL);
            }
            this.array.set(4, attributes.getCOSDictionary());
        }
    }
    
    public COSArray calculateColorValues(final List<COSBase> tintValues) throws IOException {
        final PDFunction tintTransform = this.getTintTransform();
        final COSArray tint = new COSArray();
        tint.addAll(tintValues);
        return tintTransform.eval(tint);
    }
    
    static {
        LOG = LogFactory.getLog(PDDeviceN.class);
    }
}
