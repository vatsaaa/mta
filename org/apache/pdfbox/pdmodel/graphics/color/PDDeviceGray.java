// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.graphics.color;

import java.awt.image.ComponentColorModel;
import java.awt.image.ColorModel;
import java.awt.color.ColorSpace;
import java.io.IOException;

public class PDDeviceGray extends PDColorSpace
{
    public static final String NAME = "DeviceGray";
    public static final String ABBREVIATED_NAME = "G";
    
    @Override
    public String getName() {
        return "DeviceGray";
    }
    
    @Override
    public int getNumberOfComponents() throws IOException {
        return 1;
    }
    
    @Override
    protected ColorSpace createColorSpace() {
        return ColorSpace.getInstance(1003);
    }
    
    @Override
    public ColorModel createColorModel(final int bpc) throws IOException {
        final ColorSpace cs = ColorSpace.getInstance(1003);
        final int[] nBits = { bpc };
        final ColorModel colorModel = new ComponentColorModel(cs, nBits, false, false, 1, 0);
        return colorModel;
    }
}
