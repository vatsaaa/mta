// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.graphics.color;

import org.apache.commons.logging.LogFactory;
import java.awt.color.ColorSpace;
import java.util.Arrays;
import java.awt.color.ICC_ColorSpace;
import java.io.IOException;
import java.awt.Paint;
import org.apache.pdfbox.pdmodel.graphics.pattern.PDPatternResources;
import org.apache.pdfbox.cos.COSArray;
import java.awt.Color;
import org.apache.commons.logging.Log;

public class PDColorState implements Cloneable
{
    private static final Log LOG;
    private static volatile Color iccOverrideColor;
    private PDColorSpace colorSpace;
    private COSArray colorSpaceValue;
    private PDPatternResources pattern;
    private Color color;
    private Paint paint;
    
    public static void setIccOverrideColor(final Color color) {
        PDColorState.iccOverrideColor = color;
    }
    
    public PDColorState() {
        this.colorSpace = new PDDeviceGray();
        this.colorSpaceValue = new COSArray();
        this.pattern = null;
        this.color = null;
        this.paint = null;
        this.setColorSpaceValue(new float[] { 0.0f });
    }
    
    public Object clone() {
        final PDColorState retval = new PDColorState();
        retval.colorSpace = this.colorSpace;
        retval.colorSpaceValue.clear();
        retval.colorSpaceValue.addAll(this.colorSpaceValue);
        retval.setPattern(this.getPattern());
        return retval;
    }
    
    public Color getJavaColor() throws IOException {
        if (this.color == null && this.colorSpaceValue.size() > 0) {
            this.color = this.createColor();
        }
        return this.color;
    }
    
    public Paint getPaint(final int pageHeight) throws IOException {
        if (this.paint == null && this.pattern != null) {
            this.paint = this.pattern.getPaint(pageHeight);
        }
        return this.paint;
    }
    
    private Color createColor() throws IOException {
        final float[] components = this.colorSpaceValue.toFloatArray();
        try {
            if (this.colorSpace.getName().equals("DeviceRGB") && components.length == 3) {
                return new Color(components[0], components[1], components[2]);
            }
            if (components.length == 1) {
                if (this.colorSpace.getName().equals("Separation")) {
                    return new Color((int)components[0]);
                }
                if (this.colorSpace.getName().equals("DeviceGray")) {
                    return new Color(components[0], components[0], components[0]);
                }
            }
            final Color override = PDColorState.iccOverrideColor;
            final ColorSpace cs = this.colorSpace.getJavaColorSpace();
            if (cs instanceof ICC_ColorSpace && override != null) {
                PDColorState.LOG.warn("Using an ICC override color to avoid a potential JVM crash (see PDFBOX-511)");
                return override;
            }
            return new Color(cs, components, 1.0f);
        }
        catch (Exception e3) {
            String sMsg = "Unable to create the color instance " + Arrays.toString(components) + " in color space " + this.colorSpace + "; guessing color ... ";
            Color cGuess = null;
            try {
                switch (components.length) {
                    case 1: {
                        cGuess = new Color((int)components[0]);
                        sMsg += "\nInterpretating as single-integer RGB";
                        break;
                    }
                    case 3: {
                        cGuess = new Color(components[0], components[1], components[2]);
                        sMsg += "\nInterpretating as RGB";
                        break;
                    }
                    case 4: {
                        final float k = components[3];
                        float r = components[0] * (1.0f - k) + k;
                        float g = components[1] * (1.0f - k) + k;
                        float b = components[2] * (1.0f - k) + k;
                        r = 1.0f - r;
                        g = 1.0f - g;
                        b = 1.0f - b;
                        cGuess = new Color(r, g, b);
                        sMsg += "\nInterpretating as CMYK";
                        break;
                    }
                    default: {
                        sMsg = sMsg + "\nUnable to guess using " + components.length + " components; using black instead";
                        cGuess = Color.BLACK;
                        break;
                    }
                }
            }
            catch (Exception e2) {
                sMsg += "\nColor interpolation failed; using black instead\n";
                sMsg += e2.toString();
                cGuess = Color.BLACK;
            }
            PDColorState.LOG.warn(sMsg, e3);
            return cGuess;
        }
    }
    
    public PDColorState(final COSArray csValues) {
        this.colorSpace = new PDDeviceGray();
        this.colorSpaceValue = new COSArray();
        this.pattern = null;
        this.color = null;
        this.paint = null;
        this.colorSpaceValue = csValues;
    }
    
    public PDColorSpace getColorSpace() {
        return this.colorSpace;
    }
    
    public void setColorSpace(final PDColorSpace value) {
        this.colorSpace = value;
        this.color = null;
        this.pattern = null;
    }
    
    public float[] getColorSpaceValue() {
        return this.colorSpaceValue.toFloatArray();
    }
    
    public COSArray getCOSColorSpaceValue() {
        return this.colorSpaceValue;
    }
    
    public void setColorSpaceValue(final float[] value) {
        this.colorSpaceValue.setFloatArray(value);
        this.color = null;
        this.pattern = null;
    }
    
    public PDPatternResources getPattern() {
        return this.pattern;
    }
    
    public void setPattern(final PDPatternResources patternValue) {
        this.pattern = patternValue;
        this.color = null;
    }
    
    static {
        LOG = LogFactory.getLog(PDColorState.class);
        PDColorState.iccOverrideColor = Color.getColor("org.apache.pdfbox.ICC_override_color");
    }
}
