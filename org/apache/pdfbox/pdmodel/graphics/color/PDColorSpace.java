// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.graphics.color;

import java.awt.image.ColorModel;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.cos.COSBase;
import java.io.IOException;
import java.awt.color.ColorSpace;
import org.apache.pdfbox.cos.COSArray;
import org.apache.pdfbox.pdmodel.common.COSObjectable;

public abstract class PDColorSpace implements COSObjectable
{
    protected COSArray array;
    private ColorSpace colorSpace;
    
    public PDColorSpace() {
        this.colorSpace = null;
    }
    
    public abstract String getName();
    
    public abstract int getNumberOfComponents() throws IOException;
    
    public COSBase getCOSObject() {
        return COSName.getPDFName(this.getName());
    }
    
    public ColorSpace getJavaColorSpace() throws IOException {
        if (this.colorSpace == null) {
            this.colorSpace = this.createColorSpace();
        }
        return this.colorSpace;
    }
    
    protected abstract ColorSpace createColorSpace() throws IOException;
    
    public abstract ColorModel createColorModel(final int p0) throws IOException;
    
    @Override
    public String toString() {
        return this.getName() + "{ " + ((this.array == null) ? "" : this.array.toString()) + " }";
    }
}
