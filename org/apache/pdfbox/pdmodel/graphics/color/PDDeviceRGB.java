// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.graphics.color;

import java.awt.image.ComponentColorModel;
import java.awt.image.ColorModel;
import java.awt.color.ColorSpace;
import java.io.IOException;

public class PDDeviceRGB extends PDColorSpace
{
    public static final String NAME = "DeviceRGB";
    public static final String ABBREVIATED_NAME = "RGB";
    public static final PDDeviceRGB INSTANCE;
    
    private PDDeviceRGB() {
    }
    
    @Override
    public String getName() {
        return "DeviceRGB";
    }
    
    @Override
    public int getNumberOfComponents() throws IOException {
        return 3;
    }
    
    @Override
    protected ColorSpace createColorSpace() {
        return ColorSpace.getInstance(1000);
    }
    
    @Override
    public ColorModel createColorModel(final int bpc) throws IOException {
        final int[] nbBits = { bpc, bpc, bpc };
        final ComponentColorModel componentColorModel = new ComponentColorModel(this.getJavaColorSpace(), nbBits, false, false, 1, 0);
        return componentColorModel;
    }
    
    static {
        INSTANCE = new PDDeviceRGB();
    }
}
