// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.graphics.color;

import java.awt.image.ComponentColorModel;
import java.awt.image.ColorModel;
import java.awt.color.ColorSpace;
import java.io.IOException;

public class PDDeviceCMYK extends PDColorSpace
{
    public static final PDDeviceCMYK INSTANCE;
    public static final String NAME = "DeviceCMYK";
    public static final String ABBREVIATED_NAME = "CMYK";
    
    private PDDeviceCMYK() {
    }
    
    @Override
    public String getName() {
        return "DeviceCMYK";
    }
    
    @Override
    public int getNumberOfComponents() throws IOException {
        return 4;
    }
    
    @Override
    protected ColorSpace createColorSpace() {
        return new ColorSpaceCMYK();
    }
    
    @Override
    public ColorModel createColorModel(final int bpc) throws IOException {
        final int[] nbBits = { bpc, bpc, bpc, bpc };
        final ComponentColorModel componentColorModel = new ComponentColorModel(this.getJavaColorSpace(), nbBits, false, false, 1, 0);
        return componentColorModel;
    }
    
    static {
        INSTANCE = new PDDeviceCMYK();
    }
}
