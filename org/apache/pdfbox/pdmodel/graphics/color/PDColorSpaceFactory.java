// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.graphics.color;

import java.io.OutputStream;
import org.apache.pdfbox.pdmodel.common.PDStream;
import org.apache.pdfbox.cos.COSFloat;
import java.awt.color.ICC_ColorSpace;
import java.awt.color.ColorSpace;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.cos.COSArray;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.pdmodel.graphics.pattern.PDPatternResources;
import java.io.IOException;
import java.util.Map;
import org.apache.pdfbox.cos.COSBase;

public final class PDColorSpaceFactory
{
    private PDColorSpaceFactory() {
    }
    
    public static PDColorSpace createColorSpace(final COSBase colorSpace) throws IOException {
        return createColorSpace(colorSpace, null);
    }
    
    public static PDColorSpace createColorSpace(final COSBase colorSpace, final Map<String, PDColorSpace> colorSpaces) throws IOException {
        return createColorSpace(colorSpace, colorSpaces, null);
    }
    
    public static PDColorSpace createColorSpace(final COSBase colorSpace, final Map<String, PDColorSpace> colorSpaces, final Map<String, PDPatternResources> patterns) throws IOException {
        PDColorSpace retval = null;
        if (colorSpace instanceof COSName) {
            retval = createColorSpace(((COSName)colorSpace).getName(), colorSpaces);
        }
        else {
            if (!(colorSpace instanceof COSArray)) {
                throw new IOException("Unknown colorspace type:" + colorSpace);
            }
            final COSArray array = (COSArray)colorSpace;
            final String name = ((COSName)array.getObject(0)).getName();
            if (name.equals("CalGray")) {
                retval = new PDCalGray(array);
            }
            else if (name.equals("DeviceRGB")) {
                retval = PDDeviceRGB.INSTANCE;
            }
            else if (name.equals("DeviceGray")) {
                retval = new PDDeviceGray();
            }
            else if (name.equals("DeviceCMYK")) {
                retval = PDDeviceCMYK.INSTANCE;
            }
            else if (name.equals("CalRGB")) {
                retval = new PDCalRGB(array);
            }
            else if (name.equals("DeviceN")) {
                retval = new PDDeviceN(array);
            }
            else if (name.equals("Indexed") || name.equals("I")) {
                retval = new PDIndexed(array);
            }
            else if (name.equals("Lab")) {
                retval = new PDLab(array);
            }
            else if (name.equals("Separation")) {
                retval = new PDSeparation(array);
            }
            else if (name.equals("ICCBased")) {
                retval = new PDICCBased(array);
            }
            else {
                if (!name.equals("Pattern")) {
                    throw new IOException("Unknown colorspace array type:" + name);
                }
                retval = new PDPattern(array);
            }
        }
        return retval;
    }
    
    public static PDColorSpace createColorSpace(final String colorSpaceName) throws IOException {
        return createColorSpace(colorSpaceName, null);
    }
    
    public static PDColorSpace createColorSpace(final String colorSpaceName, final Map<String, PDColorSpace> colorSpaces) throws IOException {
        PDColorSpace cs = null;
        if (colorSpaceName.equals("DeviceCMYK") || colorSpaceName.equals("CMYK")) {
            cs = PDDeviceCMYK.INSTANCE;
        }
        else if (colorSpaceName.equals("DeviceRGB") || colorSpaceName.equals("RGB")) {
            cs = PDDeviceRGB.INSTANCE;
        }
        else if (colorSpaceName.equals("DeviceGray") || colorSpaceName.equals("G")) {
            cs = new PDDeviceGray();
        }
        else if (colorSpaces != null && colorSpaces.get(colorSpaceName) != null) {
            cs = colorSpaces.get(colorSpaceName);
        }
        else if (colorSpaceName.equals("Lab")) {
            cs = new PDLab();
        }
        else {
            if (!colorSpaceName.equals("Pattern")) {
                throw new IOException("Error: Unknown colorspace '" + colorSpaceName + "'");
            }
            cs = new PDPattern();
        }
        return cs;
    }
    
    public static PDColorSpace createColorSpace(final PDDocument doc, final ColorSpace cs) throws IOException {
        PDColorSpace retval = null;
        if (cs.isCS_sRGB()) {
            retval = PDDeviceRGB.INSTANCE;
        }
        else {
            if (!(cs instanceof ICC_ColorSpace)) {
                throw new IOException("Not yet implemented:" + cs);
            }
            final ICC_ColorSpace ics = (ICC_ColorSpace)cs;
            final PDICCBased pdCS = (PDICCBased)(retval = new PDICCBased(doc));
            final COSArray ranges = new COSArray();
            for (int i = 0; i < cs.getNumComponents(); ++i) {
                ranges.add(new COSFloat(ics.getMinValue(i)));
                ranges.add(new COSFloat(ics.getMaxValue(i)));
            }
            final PDStream iccData = pdCS.getPDStream();
            OutputStream output = null;
            try {
                output = iccData.createOutputStream();
                output.write(ics.getProfile().getData());
            }
            finally {
                if (output != null) {
                    output.close();
                }
            }
            pdCS.setNumberOfComponents(cs.getNumComponents());
        }
        return retval;
    }
}
