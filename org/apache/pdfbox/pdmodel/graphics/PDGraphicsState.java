// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.graphics;

import java.awt.AlphaComposite;
import java.awt.Composite;
import java.awt.Shape;
import java.awt.Rectangle;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import java.awt.geom.GeneralPath;
import org.apache.pdfbox.pdmodel.text.PDTextState;
import org.apache.pdfbox.pdmodel.graphics.color.PDColorState;
import org.apache.pdfbox.util.Matrix;

public class PDGraphicsState implements Cloneable
{
    private Matrix currentTransformationMatrix;
    private PDColorState strokingColor;
    private PDColorState nonStrokingColor;
    private PDTextState textState;
    private double lineWidth;
    private int lineCap;
    private int lineJoin;
    private double miterLimit;
    private PDLineDashPattern lineDashPattern;
    private String renderingIntent;
    private boolean strokeAdjustment;
    private double alphaConstants;
    private double nonStrokingAlphaConstants;
    private boolean alphaSource;
    private boolean overprint;
    private double overprintMode;
    private double flatness;
    private double smoothness;
    private GeneralPath currentClippingPath;
    
    public PDGraphicsState() {
        this.currentTransformationMatrix = new Matrix();
        this.strokingColor = new PDColorState();
        this.nonStrokingColor = new PDColorState();
        this.textState = new PDTextState();
        this.lineWidth = 0.0;
        this.lineCap = 0;
        this.lineJoin = 0;
        this.miterLimit = 0.0;
        this.strokeAdjustment = false;
        this.alphaConstants = 1.0;
        this.nonStrokingAlphaConstants = 1.0;
        this.alphaSource = false;
        this.overprint = false;
        this.overprintMode = 0.0;
        this.flatness = 1.0;
        this.smoothness = 0.0;
    }
    
    public PDGraphicsState(final PDRectangle page) {
        this.currentTransformationMatrix = new Matrix();
        this.strokingColor = new PDColorState();
        this.nonStrokingColor = new PDColorState();
        this.textState = new PDTextState();
        this.lineWidth = 0.0;
        this.lineCap = 0;
        this.lineJoin = 0;
        this.miterLimit = 0.0;
        this.strokeAdjustment = false;
        this.alphaConstants = 1.0;
        this.nonStrokingAlphaConstants = 1.0;
        this.alphaSource = false;
        this.overprint = false;
        this.overprintMode = 0.0;
        this.flatness = 1.0;
        this.smoothness = 0.0;
        this.currentClippingPath = new GeneralPath(new Rectangle(page.createDimension()));
        if (page.getLowerLeftX() != 0.0f || page.getLowerLeftY() != 0.0f) {
            this.currentTransformationMatrix = this.currentTransformationMatrix.multiply(Matrix.getTranslatingInstance(-page.getLowerLeftX(), -page.getLowerLeftY()));
        }
    }
    
    public Matrix getCurrentTransformationMatrix() {
        return this.currentTransformationMatrix;
    }
    
    public void setCurrentTransformationMatrix(final Matrix value) {
        this.currentTransformationMatrix = value;
    }
    
    public double getLineWidth() {
        return this.lineWidth;
    }
    
    public void setLineWidth(final double value) {
        this.lineWidth = value;
    }
    
    public int getLineCap() {
        return this.lineCap;
    }
    
    public void setLineCap(final int value) {
        this.lineCap = value;
    }
    
    public int getLineJoin() {
        return this.lineJoin;
    }
    
    public void setLineJoin(final int value) {
        this.lineJoin = value;
    }
    
    public double getMiterLimit() {
        return this.miterLimit;
    }
    
    public void setMiterLimit(final double value) {
        this.miterLimit = value;
    }
    
    public boolean isStrokeAdjustment() {
        return this.strokeAdjustment;
    }
    
    public void setStrokeAdjustment(final boolean value) {
        this.strokeAdjustment = value;
    }
    
    public double getAlphaConstants() {
        return this.alphaConstants;
    }
    
    public void setAlphaConstants(final double value) {
        this.alphaConstants = value;
    }
    
    public double getNonStrokeAlphaConstants() {
        return this.nonStrokingAlphaConstants;
    }
    
    public void setNonStrokeAlphaConstants(final double value) {
        this.nonStrokingAlphaConstants = value;
    }
    
    public boolean isAlphaSource() {
        return this.alphaSource;
    }
    
    public void setAlphaSource(final boolean value) {
        this.alphaSource = value;
    }
    
    public boolean isOverprint() {
        return this.overprint;
    }
    
    public void setOverprint(final boolean value) {
        this.overprint = value;
    }
    
    public double getOverprintMode() {
        return this.overprintMode;
    }
    
    public void setOverprintMode(final double value) {
        this.overprintMode = value;
    }
    
    public double getFlatness() {
        return this.flatness;
    }
    
    public void setFlatness(final double value) {
        this.flatness = value;
    }
    
    public double getSmoothness() {
        return this.smoothness;
    }
    
    public void setSmoothness(final double value) {
        this.smoothness = value;
    }
    
    public PDTextState getTextState() {
        return this.textState;
    }
    
    public void setTextState(final PDTextState value) {
        this.textState = value;
    }
    
    public PDLineDashPattern getLineDashPattern() {
        return this.lineDashPattern;
    }
    
    public void setLineDashPattern(final PDLineDashPattern value) {
        this.lineDashPattern = value;
    }
    
    public String getRenderingIntent() {
        return this.renderingIntent;
    }
    
    public void setRenderingIntent(final String value) {
        this.renderingIntent = value;
    }
    
    public Object clone() {
        PDGraphicsState clone = null;
        try {
            clone = (PDGraphicsState)super.clone();
            clone.setTextState((PDTextState)this.textState.clone());
            clone.setCurrentTransformationMatrix(this.currentTransformationMatrix.copy());
            clone.strokingColor = (PDColorState)this.strokingColor.clone();
            clone.nonStrokingColor = (PDColorState)this.nonStrokingColor.clone();
            if (this.lineDashPattern != null) {
                clone.setLineDashPattern((PDLineDashPattern)this.lineDashPattern.clone());
            }
            if (this.currentClippingPath != null) {
                clone.setCurrentClippingPath((Shape)this.currentClippingPath.clone());
            }
        }
        catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        return clone;
    }
    
    public PDColorState getStrokingColor() {
        return this.strokingColor;
    }
    
    public PDColorState getNonStrokingColor() {
        return this.nonStrokingColor;
    }
    
    public void setCurrentClippingPath(final Shape pCurrentClippingPath) {
        if (pCurrentClippingPath != null) {
            if (pCurrentClippingPath instanceof GeneralPath) {
                this.currentClippingPath = (GeneralPath)pCurrentClippingPath;
            }
            else {
                (this.currentClippingPath = new GeneralPath()).append(pCurrentClippingPath, false);
            }
        }
        else {
            this.currentClippingPath = null;
        }
    }
    
    public Shape getCurrentClippingPath() {
        return this.currentClippingPath;
    }
    
    public Composite getStrokeJavaComposite() {
        return AlphaComposite.getInstance(3, (float)this.alphaConstants);
    }
    
    public Composite getNonStrokeJavaComposite() {
        return AlphaComposite.getInstance(3, (float)this.nonStrokingAlphaConstants);
    }
}
