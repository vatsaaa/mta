// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.graphics.xobject;

import org.apache.commons.logging.LogFactory;
import org.apache.pdfbox.util.ImageIOUtil;
import org.apache.pdfbox.pdmodel.common.function.PDFunction;
import java.awt.image.BufferedImageOp;
import java.awt.Color;
import java.awt.image.DataBufferByte;
import org.apache.pdfbox.pdmodel.graphics.color.PDICCBased;
import org.apache.pdfbox.pdmodel.graphics.color.PDSeparation;
import java.awt.image.IndexColorModel;
import org.apache.pdfbox.cos.COSArray;
import org.apache.pdfbox.pdmodel.graphics.color.PDIndexed;
import org.apache.pdfbox.cos.COSDictionary;
import java.io.OutputStream;
import java.awt.Graphics2D;
import java.awt.image.ColorModel;
import java.awt.image.WritableRaster;
import org.apache.pdfbox.pdmodel.common.COSObjectable;
import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.pdmodel.graphics.color.PDDeviceGray;
import org.apache.pdfbox.pdmodel.graphics.color.PDColorSpace;
import org.apache.pdfbox.pdmodel.graphics.color.PDDeviceRGB;
import java.awt.image.ImageObserver;
import java.awt.Image;
import java.awt.Composite;
import java.awt.AlphaComposite;
import java.util.Hashtable;
import java.awt.image.ComponentColorModel;
import java.awt.color.ColorSpace;
import java.io.IOException;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.common.PDStream;
import java.awt.image.BufferedImage;
import org.apache.commons.logging.Log;

public class PDPixelMap extends PDXObjectImage
{
    private static final Log LOG;
    private BufferedImage image;
    private static final String PNG = "png";
    
    public PDPixelMap(final PDStream pdStream) {
        super(pdStream, "png");
        this.image = null;
    }
    
    public PDPixelMap(final PDDocument doc, final BufferedImage bi) throws IOException {
        super(doc, "png");
        this.image = null;
        this.createImageStream(doc, bi);
    }
    
    private void createImageStream(final PDDocument doc, final BufferedImage bi) throws IOException {
        BufferedImage alphaImage = null;
        BufferedImage rgbImage = null;
        final int width = bi.getWidth();
        final int height = bi.getHeight();
        if (bi.getColorModel().hasAlpha()) {
            final WritableRaster alphaRaster = bi.getAlphaRaster();
            final ColorModel cm = new ComponentColorModel(ColorSpace.getInstance(1003), false, false, 1, 0);
            alphaImage = new BufferedImage(cm, alphaRaster, false, null);
            rgbImage = new BufferedImage(width, height, 5);
            final Graphics2D g = rgbImage.createGraphics();
            g.setComposite(AlphaComposite.Src);
            g.drawImage(bi, 0, 0, null);
        }
        else {
            rgbImage = bi;
        }
        OutputStream os = null;
        try {
            final int numberOfComponents = rgbImage.getColorModel().getNumComponents();
            if (numberOfComponents == 3) {
                this.setColorSpace(PDDeviceRGB.INSTANCE);
            }
            else {
                if (numberOfComponents != 1) {
                    throw new IllegalStateException();
                }
                this.setColorSpace(new PDDeviceGray());
            }
            final byte[] outData = new byte[width * height * numberOfComponents];
            rgbImage.getData().getDataElements(0, 0, width, height, outData);
            this.getPDStream().addCompression();
            os = this.getCOSStream().createUnfilteredStream();
            os.write(outData);
            final COSDictionary dic = this.getCOSStream();
            dic.setItem(COSName.FILTER, COSName.FLATE_DECODE);
            dic.setItem(COSName.SUBTYPE, COSName.IMAGE);
            dic.setItem(COSName.TYPE, COSName.XOBJECT);
            if (alphaImage != null) {
                final PDPixelMap smask = new PDPixelMap(doc, alphaImage);
                dic.setItem(COSName.SMASK, smask);
            }
            this.setBitsPerComponent(8);
            this.setHeight(height);
            this.setWidth(width);
        }
        finally {
            os.close();
        }
    }
    
    @Override
    public BufferedImage getRGBImage() throws IOException {
        if (this.image != null) {
            return this.image;
        }
        try {
            final int width = this.getWidth();
            final int height = this.getHeight();
            final int bpc = this.getBitsPerComponent();
            byte[] array = this.getPDStream().getByteArray();
            if (array.length == 0) {
                PDPixelMap.LOG.error("Something went wrong ... the pixelmap doesn't contain any data.");
                return null;
            }
            final PDColorSpace colorspace = this.getColorSpace();
            if (colorspace == null) {
                PDPixelMap.LOG.error("getColorSpace() returned NULL.  Predictor = " + this.getPredictor());
                return null;
            }
            ColorModel cm = null;
            if (colorspace instanceof PDIndexed) {
                final PDIndexed csIndexed = (PDIndexed)colorspace;
                final ColorModel baseColorModel = csIndexed.getBaseColorSpace().createColorModel(8);
                final int numberOfColorValues = 1 << bpc;
                final int highValue = csIndexed.getHighValue();
                final int size = Math.min(numberOfColorValues - 1, highValue);
                final byte[] index = csIndexed.getLookupData();
                final boolean hasAlpha = baseColorModel.hasAlpha();
                final COSBase maskArray = this.getMask();
                if (baseColorModel.getTransferType() != 0) {
                    throw new IOException("Not implemented");
                }
                final int numberOfComponents = 3 + (hasAlpha ? 1 : 0);
                final int buffersize = (size + 1) * numberOfComponents;
                final byte[] colorValues = new byte[buffersize];
                final byte[] inData = new byte[baseColorModel.getNumComponents()];
                int bufferIndex = 0;
                for (int i = 0; i <= size; ++i) {
                    System.arraycopy(index, i * inData.length, inData, 0, inData.length);
                    colorValues[bufferIndex] = (byte)baseColorModel.getRed(inData);
                    colorValues[bufferIndex + 1] = (byte)baseColorModel.getGreen(inData);
                    colorValues[bufferIndex + 2] = (byte)baseColorModel.getBlue(inData);
                    if (hasAlpha) {
                        colorValues[bufferIndex + 3] = (byte)baseColorModel.getAlpha(inData);
                    }
                    bufferIndex += numberOfComponents;
                }
                if (maskArray != null && maskArray instanceof COSArray) {
                    cm = new IndexColorModel(bpc, size + 1, colorValues, 0, hasAlpha, ((COSArray)maskArray).getInt(0));
                }
                else {
                    cm = new IndexColorModel(bpc, size + 1, colorValues, 0, hasAlpha);
                }
            }
            else if (colorspace instanceof PDSeparation) {
                final PDSeparation csSeparation = (PDSeparation)colorspace;
                final int numberOfComponents2 = csSeparation.getAlternateColorSpace().getNumberOfComponents();
                final PDFunction tintTransformFunc = csSeparation.getTintTransform();
                final COSArray decode = this.getDecode();
                final boolean invert = decode != null && decode.getInt(0) == 1;
                final int maxValue = (int)Math.pow(2.0, bpc) - 1;
                final byte[] mappedData = new byte[width * height * numberOfComponents2];
                final int rowLength = width * numberOfComponents2;
                final float[] input = { 0.0f };
                for (int j = 0; j < height; ++j) {
                    final int rowOffset = j * rowLength;
                    for (int k = 0; k < width; ++k) {
                        final int value = (array[j * width + k] + 256) % 256;
                        if (invert) {
                            input[0] = (float)(1 - value / maxValue);
                        }
                        else {
                            input[0] = (float)(value / maxValue);
                        }
                        final float[] mappedColor = tintTransformFunc.eval(input);
                        final int columnOffset = k * numberOfComponents2;
                        for (int l = 0; l < numberOfComponents2; ++l) {
                            final float mappedValue = mappedColor[l];
                            mappedData[rowOffset + columnOffset + l] = (byte)(mappedValue * maxValue);
                        }
                    }
                }
                array = mappedData;
                cm = colorspace.createColorModel(bpc);
            }
            else if (bpc == 1) {
                byte[] map = null;
                if (colorspace instanceof PDDeviceGray) {
                    final COSArray decode2 = this.getDecode();
                    if (decode2 != null && decode2.getInt(0) == 1) {
                        map = new byte[] { -1 };
                    }
                    else {
                        map = new byte[] { 0, -1 };
                    }
                }
                else if (colorspace instanceof PDICCBased) {
                    if (((PDICCBased)colorspace).getNumberOfComponents() == 1) {
                        map = new byte[] { -1 };
                    }
                    else {
                        map = new byte[] { 0, -1 };
                    }
                }
                else {
                    map = new byte[] { 0, -1 };
                }
                cm = new IndexColorModel(bpc, map.length, map, map, map, 1);
            }
            else if (colorspace instanceof PDICCBased) {
                if (((PDICCBased)colorspace).getNumberOfComponents() == 1) {
                    final byte[] map = { -1 };
                    cm = new IndexColorModel(bpc, 1, map, map, map, 1);
                }
                else {
                    cm = colorspace.createColorModel(bpc);
                }
            }
            else {
                cm = colorspace.createColorModel(bpc);
            }
            PDPixelMap.LOG.debug("ColorModel: " + cm.toString());
            final WritableRaster raster = cm.createCompatibleWritableRaster(width, height);
            final DataBufferByte buffer = (DataBufferByte)raster.getDataBuffer();
            final byte[] bufferData = buffer.getData();
            System.arraycopy(array, 0, bufferData, 0, (array.length < bufferData.length) ? array.length : bufferData.length);
            this.image = new BufferedImage(cm, raster, false, null);
            final PDXObjectImage smask = this.getSMaskImage();
            if (smask != null) {
                final BufferedImage smaskBI = smask.getRGBImage();
                final COSArray decodeArray = smask.getDecode();
                final CompositeImage compositeImage = new CompositeImage(this.image, smaskBI);
                final BufferedImage rgbImage = compositeImage.createMaskedImage(decodeArray);
                return rgbImage;
            }
            if (this.getImageMask()) {
                final BufferedImage stencilMask = new BufferedImage(width, height, 2);
                final Graphics2D graphics = (Graphics2D)stencilMask.getGraphics();
                if (this.getStencilColor() != null) {
                    graphics.setColor(this.getStencilColor().getJavaColor());
                }
                else {
                    PDPixelMap.LOG.debug("no stencil color for PixelMap found, using Color.BLACK instead.");
                    graphics.setColor(Color.BLACK);
                }
                graphics.fillRect(0, 0, width, height);
                graphics.setComposite(AlphaComposite.DstIn);
                graphics.drawImage(this.image, null, 0, 0);
                return stencilMask;
            }
            return this.image;
        }
        catch (Exception exception) {
            PDPixelMap.LOG.error(exception, exception);
            return null;
        }
    }
    
    @Override
    public void write2OutputStream(final OutputStream out) throws IOException {
        this.getRGBImage();
        if (this.image != null) {
            ImageIOUtil.writeImage(this.image, "png", out);
        }
    }
    
    public COSDictionary getDecodeParams() {
        final COSBase decodeParms = this.getCOSStream().getDictionaryObject(COSName.DECODE_PARMS);
        if (decodeParms == null) {
            return null;
        }
        if (decodeParms instanceof COSDictionary) {
            return (COSDictionary)decodeParms;
        }
        if (decodeParms instanceof COSArray) {
            return null;
        }
        return null;
    }
    
    public int getPredictor() {
        final COSDictionary decodeParms = this.getDecodeParams();
        if (decodeParms != null) {
            final int i = decodeParms.getInt(COSName.PREDICTOR);
            if (i != -1) {
                return i;
            }
        }
        return 1;
    }
    
    static {
        LOG = LogFactory.getLog(PDPixelMap.class);
    }
}
