// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.graphics.xobject;

import org.apache.commons.logging.LogFactory;
import org.apache.pdfbox.pdmodel.common.PDMetadata;
import java.io.IOException;
import java.util.List;
import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.cos.COSStream;
import org.apache.pdfbox.pdmodel.common.PDStream;
import org.apache.commons.logging.Log;
import org.apache.pdfbox.pdmodel.common.COSObjectable;

public abstract class PDXObject implements COSObjectable
{
    private static final Log log;
    private PDStream xobject;
    
    public PDXObject(final COSStream xobj) {
        this.xobject = new PDStream(xobj);
        this.xobject.getStream().setName(COSName.TYPE, "XObject");
    }
    
    public PDXObject(final PDStream xobj) {
        this.xobject = xobj;
        this.xobject.getStream().setName(COSName.TYPE, "XObject");
    }
    
    public PDXObject(final PDDocument doc) {
        this.xobject = new PDStream(doc);
        this.xobject.getStream().setName(COSName.TYPE, "XObject");
    }
    
    public COSBase getCOSObject() {
        return this.xobject.getCOSObject();
    }
    
    public COSStream getCOSStream() {
        return this.xobject.getStream();
    }
    
    public PDStream getPDStream() {
        return this.xobject;
    }
    
    public static PDXObject createXObject(final COSBase xobject) throws IOException {
        PDXObject retval = null;
        if (xobject == null) {
            retval = null;
        }
        else if (xobject instanceof COSStream) {
            final COSStream xstream = (COSStream)xobject;
            final String subtype = xstream.getNameAsString(COSName.SUBTYPE);
            if (subtype.equals("Image")) {
                final PDStream image = new PDStream(xstream);
                final List filters = image.getFilters();
                if (filters != null && filters.contains(COSName.DCT_DECODE.getName())) {
                    return new PDJpeg(image);
                }
                if (filters != null && filters.contains(COSName.CCITTFAX_DECODE.getName())) {
                    return new PDCcitt(image);
                }
                if (filters != null && filters.contains(COSName.JPX_DECODE.getName())) {
                    return new PDPixelMap(image);
                }
                retval = new PDPixelMap(image);
            }
            else if (subtype.equals("Form")) {
                retval = new PDXObjectForm(xstream);
            }
            else {
                PDXObject.log.warn("Skipping unknown XObject subtype '" + subtype + "'");
            }
        }
        return retval;
    }
    
    public PDMetadata getMetadata() {
        PDMetadata retval = null;
        final COSStream mdStream = (COSStream)this.xobject.getStream().getDictionaryObject(COSName.METADATA);
        if (mdStream != null) {
            retval = new PDMetadata(mdStream);
        }
        return retval;
    }
    
    public void setMetadata(final PDMetadata meta) {
        this.xobject.getStream().setItem(COSName.METADATA, meta);
    }
    
    static {
        log = LogFactory.getLog(PDXObject.class);
    }
}
