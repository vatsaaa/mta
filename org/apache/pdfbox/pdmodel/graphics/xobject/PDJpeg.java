// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.graphics.xobject;

import java.util.ArrayList;
import org.apache.pdfbox.pdmodel.common.function.PDFunction;
import java.awt.image.DataBufferByte;
import java.util.Iterator;
import javax.imageio.stream.ImageInputStream;
import javax.imageio.ImageReader;
import java.awt.image.Raster;
import org.apache.pdfbox.cos.COSArray;
import javax.imageio.IIOException;
import javax.imageio.ImageIO;
import java.io.ByteArrayInputStream;
import org.apache.pdfbox.pdmodel.graphics.color.PDDeviceN;
import org.apache.pdfbox.pdmodel.graphics.color.PDSeparation;
import org.apache.pdfbox.pdmodel.graphics.color.PDICCBased;
import org.apache.pdfbox.pdmodel.graphics.color.PDDeviceCMYK;
import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.awt.Graphics2D;
import java.awt.image.ColorModel;
import java.awt.image.WritableRaster;
import org.apache.pdfbox.pdmodel.graphics.color.PDDeviceGray;
import org.apache.pdfbox.pdmodel.common.COSObjectable;
import org.apache.pdfbox.util.ImageIOUtil;
import java.awt.image.ImageObserver;
import java.awt.Image;
import java.awt.Composite;
import java.awt.AlphaComposite;
import java.util.Hashtable;
import java.awt.image.ComponentColorModel;
import java.awt.color.ColorSpace;
import java.io.IOException;
import org.apache.pdfbox.cos.COSDictionary;
import org.apache.pdfbox.pdmodel.graphics.color.PDColorSpace;
import org.apache.pdfbox.pdmodel.graphics.color.PDDeviceRGB;
import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSName;
import java.io.InputStream;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.common.PDStream;
import java.util.List;
import java.awt.image.BufferedImage;

public class PDJpeg extends PDXObjectImage
{
    private BufferedImage image;
    private static final String JPG = "jpg";
    private static final List<String> DCT_FILTERS;
    private static final float DEFAULT_COMPRESSION_LEVEL = 0.75f;
    
    public PDJpeg(final PDStream jpeg) {
        super(jpeg, "jpg");
        this.image = null;
    }
    
    public PDJpeg(final PDDocument doc, final InputStream is) throws IOException {
        super(new PDStream(doc, is, true), "jpg");
        this.image = null;
        final COSDictionary dic = this.getCOSStream();
        dic.setItem(COSName.FILTER, COSName.DCT_DECODE);
        dic.setItem(COSName.SUBTYPE, COSName.IMAGE);
        dic.setItem(COSName.TYPE, COSName.XOBJECT);
        this.getRGBImage();
        if (this.image != null) {
            this.setBitsPerComponent(8);
            this.setColorSpace(PDDeviceRGB.INSTANCE);
            this.setHeight(this.image.getHeight());
            this.setWidth(this.image.getWidth());
        }
    }
    
    public PDJpeg(final PDDocument doc, final BufferedImage bi) throws IOException {
        super(new PDStream(doc), "jpg");
        this.image = null;
        this.createImageStream(doc, bi, 0.75f);
    }
    
    public PDJpeg(final PDDocument doc, final BufferedImage bi, final float compressionQuality) throws IOException {
        super(new PDStream(doc), "jpg");
        this.image = null;
        this.createImageStream(doc, bi, compressionQuality);
    }
    
    private void createImageStream(final PDDocument doc, BufferedImage bi, final float compressionQuality) throws IOException {
        BufferedImage alpha = null;
        if (bi.getColorModel().hasAlpha()) {
            final WritableRaster alphaRaster = bi.getAlphaRaster();
            final ColorModel cm = new ComponentColorModel(ColorSpace.getInstance(1003), false, false, 1, 0);
            alpha = new BufferedImage(cm, alphaRaster, false, null);
            this.image = new BufferedImage(bi.getWidth(), bi.getHeight(), 1);
            final Graphics2D g = this.image.createGraphics();
            g.setComposite(AlphaComposite.Src);
            g.drawImage(bi, 0, 0, null);
            bi = this.image;
        }
        final OutputStream os = this.getCOSStream().createFilteredStream();
        try {
            ImageIOUtil.writeImage(bi, "jpg", os);
            final COSDictionary dic = this.getCOSStream();
            dic.setItem(COSName.FILTER, COSName.DCT_DECODE);
            dic.setItem(COSName.SUBTYPE, COSName.IMAGE);
            dic.setItem(COSName.TYPE, COSName.XOBJECT);
            PDXObjectImage alphaPdImage = null;
            if (alpha != null) {
                alphaPdImage = new PDJpeg(doc, alpha, compressionQuality);
                dic.setItem(COSName.SMASK, alphaPdImage);
            }
            this.setBitsPerComponent(8);
            if (bi.getColorModel().getNumComponents() == 3) {
                this.setColorSpace(PDDeviceRGB.INSTANCE);
            }
            else {
                if (bi.getColorModel().getNumComponents() != 1) {
                    throw new IllegalStateException();
                }
                this.setColorSpace(new PDDeviceGray());
            }
            this.setHeight(bi.getHeight());
            this.setWidth(bi.getWidth());
        }
        finally {
            os.close();
        }
    }
    
    @Override
    public BufferedImage getRGBImage() throws IOException {
        if (this.image != null) {
            return this.image;
        }
        BufferedImage bi = null;
        boolean readError = false;
        final ByteArrayOutputStream os = new ByteArrayOutputStream();
        this.removeAllFiltersButDCT(os);
        os.close();
        final byte[] img = os.toByteArray();
        final PDColorSpace cs = this.getColorSpace();
        try {
            if (cs instanceof PDDeviceCMYK || (cs instanceof PDICCBased && cs.getNumberOfComponents() == 4)) {
                bi = this.convertCMYK2RGB(this.readImage(img), cs);
            }
            else if (cs instanceof PDSeparation) {
                bi = this.processTintTransformation(this.readImage(img), ((PDSeparation)cs).getTintTransform(), cs.getJavaColorSpace());
            }
            else if (cs instanceof PDDeviceN) {
                bi = this.processTintTransformation(this.readImage(img), ((PDDeviceN)cs).getTintTransform(), cs.getJavaColorSpace());
            }
            else {
                final ByteArrayInputStream bai = new ByteArrayInputStream(img);
                bi = ImageIO.read(bai);
            }
        }
        catch (IIOException exception) {
            readError = true;
        }
        if (bi == null && readError) {
            final byte[] newImage = this.replaceHeader(img);
            final ByteArrayInputStream bai2 = new ByteArrayInputStream(newImage);
            bi = ImageIO.read(bai2);
        }
        final PDXObjectImage smask = this.getSMaskImage();
        if (smask != null) {
            final BufferedImage smaskBI = smask.getRGBImage();
            final COSArray decodeArray = smask.getDecode();
            final CompositeImage compositeImage = new CompositeImage(bi, smaskBI);
            final BufferedImage rgbImage = compositeImage.createMaskedImage(decodeArray);
            this.image = rgbImage;
        }
        else {
            this.image = bi;
        }
        return this.image;
    }
    
    @Override
    public void write2OutputStream(final OutputStream out) throws IOException {
        this.getRGBImage();
        if (this.image != null) {
            ImageIOUtil.writeImage(this.image, "jpg", out);
        }
    }
    
    private void removeAllFiltersButDCT(final OutputStream out) throws IOException {
        final InputStream data = this.getPDStream().getPartiallyFilteredStream(PDJpeg.DCT_FILTERS);
        final byte[] buf = new byte[1024];
        int amountRead = -1;
        while ((amountRead = data.read(buf)) != -1) {
            out.write(buf, 0, amountRead);
        }
    }
    
    private int getHeaderEndPos(final byte[] imageAsBytes) {
        for (int i = 0; i < imageAsBytes.length; ++i) {
            final byte b = imageAsBytes[i];
            if (b == -37) {
                return i - 2;
            }
        }
        return 0;
    }
    
    private byte[] replaceHeader(final byte[] imageAsBytes) {
        final int pos = this.getHeaderEndPos(imageAsBytes);
        final byte[] header = { -1, -40, -1, -32, 0, 16, 74, 70, 73, 70, 0, 1, 1, 1, 0, 96, 0, 96, 0, 0 };
        final byte[] newImage = new byte[imageAsBytes.length - pos + header.length - 1];
        System.arraycopy(header, 0, newImage, 0, header.length);
        System.arraycopy(imageAsBytes, pos + 1, newImage, header.length, imageAsBytes.length - pos - 1);
        return newImage;
    }
    
    private Raster readImage(final byte[] bytes) throws IOException {
        final ImageInputStream input = ImageIO.createImageInputStream(new ByteArrayInputStream(bytes));
        final Iterator<ImageReader> readers = ImageIO.getImageReaders(input);
        if (readers == null || !readers.hasNext()) {
            throw new RuntimeException("No ImageReaders found");
        }
        final ImageReader reader = readers.next();
        reader.setInput(input);
        final Raster raster = reader.readRaster(0, reader.getDefaultReadParam());
        if (input != null) {
            input.close();
        }
        reader.dispose();
        return raster;
    }
    
    private BufferedImage convertCMYK2RGB(final Raster raster, final PDColorSpace colorspace) throws IOException {
        final ColorSpace cs = colorspace.getJavaColorSpace();
        final int width = raster.getWidth();
        final int height = raster.getHeight();
        final byte[] rgb = new byte[width * height * 3];
        int rgbIndex = 0;
        for (int i = 0; i < height; ++i) {
            for (int j = 0; j < width; ++j) {
                final float[] srcColorValues = raster.getPixel(j, i, (float[])null);
                for (int k = 0; k < 4; ++k) {
                    final float[] array = srcColorValues;
                    final int n = k;
                    array[n] /= 255.0f;
                }
                final float[] rgbValues = cs.toRGB(srcColorValues);
                for (int l = 0; l < 3; ++l) {
                    rgb[rgbIndex + l] = (byte)(rgbValues[l] * 255.0f);
                }
                rgbIndex += 3;
            }
        }
        final ColorModel cm = new ComponentColorModel(ColorSpace.getInstance(1000), false, false, 1, 0);
        final WritableRaster writeableRaster = cm.createCompatibleWritableRaster(width, height);
        final DataBufferByte buffer = (DataBufferByte)writeableRaster.getDataBuffer();
        final byte[] bufferData = buffer.getData();
        System.arraycopy(rgb, 0, bufferData, 0, rgb.length);
        return new BufferedImage(cm, writeableRaster, true, null);
    }
    
    private BufferedImage processTintTransformation(final Raster raster, final PDFunction function, final ColorSpace colorspace) throws IOException {
        final int numberOfInputValues = function.getNumberOfInputParameters();
        final int numberOfOutputValues = function.getNumberOfOutputParameters();
        final int width = raster.getWidth();
        final int height = raster.getHeight();
        final byte[] sourceBuffer = new byte[width * height * numberOfOutputValues];
        int bufferIndex = 0;
        for (int i = 0; i < height; ++i) {
            for (int j = 0; j < width; ++j) {
                final float[] srcColorValues = raster.getPixel(j, i, (float[])null);
                for (int k = 0; k < numberOfInputValues; ++k) {
                    final float[] array = srcColorValues;
                    final int n = k;
                    array[n] /= 255.0f;
                }
                final float[] convertedValues = function.eval(srcColorValues);
                for (int l = 0; l < numberOfOutputValues; ++l) {
                    sourceBuffer[bufferIndex + l] = (byte)(convertedValues[l] * 255.0f);
                }
                bufferIndex += numberOfOutputValues;
            }
        }
        final ColorModel cm = new ComponentColorModel(colorspace, false, false, 1, 0);
        final WritableRaster writeableRaster = cm.createCompatibleWritableRaster(width, height);
        final DataBufferByte buffer = (DataBufferByte)writeableRaster.getDataBuffer();
        final byte[] bufferData = buffer.getData();
        System.arraycopy(sourceBuffer, 0, bufferData, 0, sourceBuffer.length);
        return new BufferedImage(cm, writeableRaster, true, null);
    }
    
    static {
        (DCT_FILTERS = new ArrayList<String>()).add(COSName.DCT_DECODE.getName());
        PDJpeg.DCT_FILTERS.add(COSName.DCT_DECODE_ABBREVIATION.getName());
    }
}
