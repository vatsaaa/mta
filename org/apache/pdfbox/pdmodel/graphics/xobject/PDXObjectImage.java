// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.graphics.xobject;

import org.apache.commons.logging.LogFactory;
import org.apache.pdfbox.cos.COSArray;
import org.apache.pdfbox.pdmodel.graphics.color.PDDeviceGray;
import org.apache.pdfbox.pdmodel.graphics.color.PDColorSpaceFactory;
import org.apache.pdfbox.pdmodel.graphics.color.PDColorSpace;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSStream;
import java.io.IOException;
import java.awt.image.BufferedImage;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.common.PDStream;
import org.apache.pdfbox.pdmodel.graphics.color.PDColorState;
import org.apache.commons.logging.Log;

public abstract class PDXObjectImage extends PDXObject
{
    private static final Log LOG;
    public static final String SUB_TYPE = "Image";
    private String suffix;
    private PDColorState stencilColor;
    
    public PDXObjectImage(final PDStream imageStream, final String fileSuffix) {
        super(imageStream);
        this.suffix = fileSuffix;
    }
    
    public PDXObjectImage(final PDDocument doc, final String fileSuffix) {
        super(doc);
        this.getCOSStream().setName(COSName.SUBTYPE, "Image");
        this.suffix = fileSuffix;
    }
    
    public abstract BufferedImage getRGBImage() throws IOException;
    
    public PDXObjectImage getSMaskImage() throws IOException {
        final COSStream cosStream = this.getPDStream().getStream();
        final COSBase smask = cosStream.getDictionaryObject(COSName.SMASK);
        if (smask == null) {
            return null;
        }
        return (PDXObjectImage)PDXObject.createXObject(smask);
    }
    
    public abstract void write2OutputStream(final OutputStream p0) throws IOException;
    
    public void write2file(final String filename) throws IOException {
        FileOutputStream out = null;
        try {
            out = new FileOutputStream(filename + "." + this.suffix);
            this.write2OutputStream(out);
            out.flush();
        }
        finally {
            if (out != null) {
                out.close();
            }
        }
    }
    
    public void write2file(final File file) throws IOException {
        FileOutputStream out = null;
        try {
            out = new FileOutputStream(file);
            this.write2OutputStream(out);
            out.flush();
        }
        finally {
            if (out != null) {
                out.close();
            }
        }
    }
    
    public int getHeight() {
        return this.getCOSStream().getInt(COSName.HEIGHT, -1);
    }
    
    public void setHeight(final int height) {
        this.getCOSStream().setInt(COSName.HEIGHT, height);
    }
    
    public int getWidth() {
        return this.getCOSStream().getInt(COSName.WIDTH, -1);
    }
    
    public void setWidth(final int width) {
        this.getCOSStream().setInt(COSName.WIDTH, width);
    }
    
    public int getBitsPerComponent() {
        return this.getCOSStream().getInt(COSName.BITS_PER_COMPONENT, COSName.BPC, -1);
    }
    
    public void setBitsPerComponent(final int bpc) {
        this.getCOSStream().setInt(COSName.BITS_PER_COMPONENT, bpc);
    }
    
    public PDColorSpace getColorSpace() throws IOException {
        final COSBase cs = this.getCOSStream().getDictionaryObject(COSName.COLORSPACE, COSName.CS);
        PDColorSpace retval = null;
        if (cs != null) {
            retval = PDColorSpaceFactory.createColorSpace(cs);
            if (retval == null) {
                PDXObjectImage.LOG.info("About to return NULL from createColorSpace branch");
            }
        }
        else {
            final COSBase filter = this.getCOSStream().getDictionaryObject(COSName.FILTER);
            if (COSName.CCITTFAX_DECODE.equals(filter) || COSName.CCITTFAX_DECODE_ABBREVIATION.equals(filter)) {
                retval = new PDDeviceGray();
            }
            else if (COSName.JBIG2_DECODE.equals(filter)) {
                retval = new PDDeviceGray();
            }
            else if (this.getImageMask()) {
                retval = new PDDeviceGray();
            }
            else {
                PDXObjectImage.LOG.info("About to return NULL from unhandled branch. filter = " + filter);
            }
        }
        return retval;
    }
    
    public void setColorSpace(final PDColorSpace cs) {
        COSBase base = null;
        if (cs != null) {
            base = cs.getCOSObject();
        }
        this.getCOSStream().setItem(COSName.COLORSPACE, base);
    }
    
    public String getSuffix() {
        return this.suffix;
    }
    
    public boolean getImageMask() {
        return this.getCOSStream().getBoolean(COSName.IMAGE_MASK, false);
    }
    
    public void setStencilColor(final PDColorState stencilColorValue) {
        this.stencilColor = stencilColorValue;
    }
    
    public PDColorState getStencilColor() {
        return this.stencilColor;
    }
    
    public COSArray getDecode() {
        final COSBase decode = this.getCOSStream().getDictionaryObject(COSName.DECODE);
        if (decode != null && decode instanceof COSArray) {
            return (COSArray)decode;
        }
        return null;
    }
    
    public COSBase getMask() {
        final COSBase mask = this.getCOSStream().getDictionaryObject(COSName.MASK);
        if (mask != null) {
            return mask;
        }
        return null;
    }
    
    static {
        LOG = LogFactory.getLog(PDXObjectImage.class);
    }
}
