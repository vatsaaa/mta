// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.graphics.xobject;

import java.awt.image.DataBuffer;
import java.awt.image.WritableRaster;
import org.apache.pdfbox.filter.Filter;
import java.util.List;
import java.awt.image.ColorModel;
import org.apache.pdfbox.pdmodel.graphics.color.PDColorSpace;
import java.awt.image.Raster;
import java.util.Hashtable;
import java.awt.image.DataBufferInt;
import java.awt.image.DataBufferByte;
import java.io.OutputStream;
import java.io.InputStream;
import org.apache.pdfbox.filter.FilterManager;
import java.io.ByteArrayOutputStream;
import java.io.ByteArrayInputStream;
import java.awt.image.IndexColorModel;
import java.io.IOException;
import java.util.Map;
import java.awt.image.BufferedImage;
import org.apache.pdfbox.util.ImageParameters;

public class PDInlinedImage
{
    private ImageParameters params;
    private byte[] imageData;
    
    public ImageParameters getImageParameters() {
        return this.params;
    }
    
    public void setImageParameters(final ImageParameters imageParams) {
        this.params = imageParams;
    }
    
    public byte[] getImageData() {
        return this.imageData;
    }
    
    public void setImageData(final byte[] value) {
        this.imageData = value;
    }
    
    public BufferedImage createImage() throws IOException {
        return this.createImage(null);
    }
    
    public BufferedImage createImage(final Map colorSpaces) throws IOException {
        final PDColorSpace pcs = this.params.getColorSpace(colorSpaces);
        ColorModel colorModel = null;
        if (pcs != null) {
            colorModel = pcs.createColorModel(this.params.getBitsPerComponent());
        }
        else {
            final byte[] transparentColors = { -1, -1 };
            final byte[] colors = { 0, -1 };
            colorModel = new IndexColorModel(1, 2, colors, colors, colors, transparentColors);
        }
        final List filters = this.params.getFilters();
        byte[] finalData = null;
        if (filters == null) {
            finalData = this.getImageData();
        }
        else {
            ByteArrayInputStream in = new ByteArrayInputStream(this.getImageData());
            final ByteArrayOutputStream out = new ByteArrayOutputStream(this.getImageData().length);
            final FilterManager filterManager = new FilterManager();
            for (int i = 0; i < filters.size(); ++i) {
                out.reset();
                final Filter filter = filterManager.getFilter(filters.get(i));
                filter.decode(in, out, this.params.getDictionary(), i);
                in = new ByteArrayInputStream(out.toByteArray());
            }
            finalData = out.toByteArray();
        }
        final WritableRaster raster = colorModel.createCompatibleWritableRaster(this.params.getWidth(), this.params.getHeight());
        final DataBuffer rasterBuffer = raster.getDataBuffer();
        if (rasterBuffer instanceof DataBufferByte) {
            final DataBufferByte byteBuffer = (DataBufferByte)rasterBuffer;
            final byte[] data = byteBuffer.getData();
            System.arraycopy(finalData, 0, data, 0, data.length);
        }
        else if (rasterBuffer instanceof DataBufferInt) {
            final DataBufferInt byteBuffer2 = (DataBufferInt)rasterBuffer;
            final int[] data2 = byteBuffer2.getData();
            for (int j = 0; j < finalData.length; ++j) {
                data2[j] = (finalData[j] + 256) % 256;
            }
        }
        final BufferedImage image = new BufferedImage(colorModel, raster, false, null);
        image.setData(raster);
        return image;
    }
}
