// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.graphics.xobject;

import org.apache.pdfbox.cos.COSFloat;
import java.awt.geom.AffineTransform;
import org.apache.pdfbox.cos.COSNumber;
import org.apache.pdfbox.util.Matrix;
import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSArray;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.common.COSObjectable;
import org.apache.pdfbox.cos.COSDictionary;
import org.apache.pdfbox.pdmodel.PDResources;
import org.apache.pdfbox.cos.COSStream;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.pdmodel.common.PDStream;

public class PDXObjectForm extends PDXObject
{
    public static final String SUB_TYPE = "Form";
    
    public PDXObjectForm(final PDStream formStream) {
        super(formStream);
        this.getCOSStream().setName(COSName.SUBTYPE, "Form");
    }
    
    public PDXObjectForm(final COSStream formStream) {
        super(formStream);
        this.getCOSStream().setName(COSName.SUBTYPE, "Form");
    }
    
    public int getFormType() {
        return this.getCOSStream().getInt("FormType", 1);
    }
    
    public void setFormType(final int formType) {
        this.getCOSStream().setInt("FormType", formType);
    }
    
    public PDResources getResources() {
        PDResources retval = null;
        final COSDictionary resources = (COSDictionary)this.getCOSStream().getDictionaryObject(COSName.RESOURCES);
        if (resources != null) {
            retval = new PDResources(resources);
        }
        return retval;
    }
    
    public void setResources(final PDResources resources) {
        this.getCOSStream().setItem(COSName.RESOURCES, resources);
    }
    
    public PDRectangle getBBox() {
        PDRectangle retval = null;
        final COSArray array = (COSArray)this.getCOSStream().getDictionaryObject(COSName.BBOX);
        if (array != null) {
            retval = new PDRectangle(array);
        }
        return retval;
    }
    
    public void setBBox(final PDRectangle bbox) {
        if (bbox == null) {
            this.getCOSStream().removeItem(COSName.BBOX);
        }
        else {
            this.getCOSStream().setItem(COSName.BBOX, bbox.getCOSArray());
        }
    }
    
    public Matrix getMatrix() {
        Matrix retval = null;
        final COSArray array = (COSArray)this.getCOSStream().getDictionaryObject(COSName.MATRIX);
        if (array != null) {
            retval = new Matrix();
            retval.setValue(0, 0, ((COSNumber)array.get(0)).floatValue());
            retval.setValue(0, 1, ((COSNumber)array.get(1)).floatValue());
            retval.setValue(1, 0, ((COSNumber)array.get(2)).floatValue());
            retval.setValue(1, 1, ((COSNumber)array.get(3)).floatValue());
            retval.setValue(2, 0, ((COSNumber)array.get(4)).floatValue());
            retval.setValue(2, 1, ((COSNumber)array.get(5)).floatValue());
        }
        return retval;
    }
    
    public void setMatrix(final AffineTransform transform) {
        final COSArray matrix = new COSArray();
        final double[] values = new double[6];
        transform.getMatrix(values);
        for (final double v : values) {
            matrix.add(new COSFloat((float)v));
        }
        this.getCOSStream().setItem(COSName.MATRIX, matrix);
    }
}
