// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.graphics.xobject;

import java.io.IOException;
import org.apache.pdfbox.cos.COSArray;
import java.awt.image.BufferedImage;

public class CompositeImage
{
    private BufferedImage baseImage;
    private BufferedImage smaskImage;
    
    public CompositeImage(final BufferedImage baseImage, final BufferedImage smaskImage) {
        this.baseImage = baseImage;
        this.smaskImage = smaskImage;
    }
    
    public BufferedImage createMaskedImage(final COSArray decodeArray) throws IOException {
        boolean isOpaque = false;
        if (decodeArray != null) {
            isOpaque = (decodeArray.getInt(0) > decodeArray.getInt(1));
        }
        final int baseImageWidth = this.baseImage.getWidth();
        final int baseImageHeight = this.baseImage.getHeight();
        final BufferedImage result = new BufferedImage(baseImageWidth, baseImageHeight, 2);
        for (int x = 0; x < baseImageWidth; ++x) {
            for (int y = 0; y < baseImageHeight; ++y) {
                final int rgb = this.baseImage.getRGB(x, y);
                int alpha = this.smaskImage.getRGB(x, y);
                final int rgbOnly = 0xFFFFFF & rgb;
                if (isOpaque) {
                    alpha ^= -1;
                }
                final int alphaOnly = alpha << 24;
                result.setRGB(x, y, rgbOnly | alphaOnly);
            }
        }
        return result;
    }
}
