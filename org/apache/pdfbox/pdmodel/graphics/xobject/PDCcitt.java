// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.graphics.xobject;

import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.ArrayList;
import java.io.InputStream;
import java.io.OutputStream;
import java.awt.image.WritableRaster;
import org.apache.pdfbox.cos.COSStream;
import org.apache.pdfbox.io.IOUtils;
import java.awt.image.DataBufferByte;
import org.apache.pdfbox.cos.COSArray;
import java.awt.image.BufferedImage;
import java.io.IOException;
import org.apache.pdfbox.pdmodel.graphics.color.PDColorSpace;
import org.apache.pdfbox.pdmodel.graphics.color.PDDeviceGray;
import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.cos.COSDictionary;
import org.apache.pdfbox.io.RandomAccess;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.common.PDStream;
import java.util.List;

public class PDCcitt extends PDXObjectImage
{
    private static final List<String> FAX_FILTERS;
    
    public PDCcitt(final PDStream ccitt) {
        super(ccitt, "tiff");
    }
    
    public PDCcitt(final PDDocument doc, final RandomAccess raf) throws IOException {
        super(new PDStream(doc), "tiff");
        final COSDictionary decodeParms = new COSDictionary();
        final COSDictionary dic = this.getCOSStream();
        this.extractFromTiff(raf, this.getCOSStream().createFilteredStream(), decodeParms);
        dic.setItem(COSName.FILTER, COSName.CCITTFAX_DECODE);
        dic.setItem(COSName.SUBTYPE, COSName.IMAGE);
        dic.setItem(COSName.TYPE, COSName.XOBJECT);
        dic.setItem(COSName.DECODE_PARMS, decodeParms);
        this.setBitsPerComponent(1);
        this.setColorSpace(new PDDeviceGray());
        this.setWidth(decodeParms.getInt(COSName.COLUMNS));
        this.setHeight(decodeParms.getInt(COSName.ROWS));
    }
    
    @Override
    public BufferedImage getRGBImage() throws IOException {
        final COSStream stream = this.getCOSStream();
        final COSBase decodeP = stream.getDictionaryObject(COSName.DECODE_PARMS);
        COSDictionary decodeParms = null;
        if (decodeP instanceof COSDictionary) {
            decodeParms = (COSDictionary)decodeP;
        }
        else if (decodeP instanceof COSArray) {
            int index = 0;
            final COSBase filters = stream.getFilters();
            if (filters instanceof COSArray) {
                for (COSArray filterArray = (COSArray)filters; index < filterArray.size(); ++index) {
                    final COSName filtername = (COSName)filterArray.get(index);
                    if (COSName.CCITTFAX_DECODE.equals(filtername)) {
                        break;
                    }
                }
            }
            decodeParms = (COSDictionary)((COSArray)decodeP).get(index);
        }
        final int cols = decodeParms.getInt(COSName.COLUMNS, 1728);
        int rows = decodeParms.getInt(COSName.ROWS, 0);
        final int height = stream.getInt(COSName.HEIGHT, 0);
        if (rows > 0 && height > 0) {
            rows = Math.min(rows, height);
        }
        else {
            rows = Math.max(rows, height);
        }
        final boolean blackIsOne = decodeParms.getBoolean(COSName.BLACK_IS_1, false);
        final BufferedImage image = new BufferedImage(cols, rows, 12);
        final WritableRaster raster = image.getRaster();
        final DataBufferByte buffer = (DataBufferByte)raster.getDataBuffer();
        final byte[] bufferData = buffer.getData();
        IOUtils.populateBuffer(stream.getUnfilteredStream(), bufferData);
        if (!blackIsOne) {
            this.invertBitmap(bufferData);
        }
        return image;
    }
    
    private void invertBitmap(final byte[] bufferData) {
        for (int i = 0, c = bufferData.length; i < c; ++i) {
            bufferData[i] = (byte)(~bufferData[i] & 0xFF);
        }
    }
    
    @Override
    public void write2OutputStream(final OutputStream out) throws IOException {
        final InputStream data = new TiffWrapper(this.getPDStream().getPartiallyFilteredStream(PDCcitt.FAX_FILTERS), (COSDictionary)this.getCOSStream());
        IOUtils.copy(data, out);
    }
    
    private void extractFromTiff(final RandomAccess raf, final OutputStream os, final COSDictionary parms) throws IOException {
        try {
            raf.seek(0L);
            final char endianess = (char)raf.read();
            if ((char)raf.read() != endianess) {
                throw new IOException("Not a valid tiff file");
            }
            if (endianess != 'M' && endianess != 'I') {
                throw new IOException("Not a valid tiff file");
            }
            final int magicNumber = this.readshort(endianess, raf);
            if (magicNumber != 42) {
                throw new IOException("Not a valid tiff file");
            }
            raf.seek(this.readlong(endianess, raf));
            final int numtags = this.readshort(endianess, raf);
            if (numtags > 50) {
                throw new IOException("Not a valid tiff file");
            }
            int k = -1000;
            int dataoffset = 0;
            int datalength = 0;
            for (int i = 0; i < numtags; ++i) {
                final int tag = this.readshort(endianess, raf);
                final int type = this.readshort(endianess, raf);
                final int count = this.readlong(endianess, raf);
                int val = this.readlong(endianess, raf);
                if (endianess == 'M') {
                    switch (type) {
                        case 1: {
                            val >>= 24;
                            break;
                        }
                        case 3: {
                            val >>= 16;
                        }
                    }
                }
                switch (tag) {
                    case 256: {
                        parms.setInt(COSName.COLUMNS, val);
                        break;
                    }
                    case 257: {
                        parms.setInt(COSName.ROWS, val);
                        break;
                    }
                    case 259: {
                        if (val == 4) {
                            k = -1;
                        }
                        if (val == 3) {
                            k = 0;
                            break;
                        }
                        break;
                    }
                    case 262: {
                        if (val == 1) {
                            parms.setBoolean(COSName.BLACK_IS_1, true);
                            break;
                        }
                        break;
                    }
                    case 273: {
                        if (count == 1) {
                            dataoffset = val;
                            break;
                        }
                        break;
                    }
                    case 279: {
                        if (count == 1) {
                            datalength = val;
                            break;
                        }
                        break;
                    }
                    case 292: {
                        if (val == 1) {
                            k = 50;
                            break;
                        }
                        break;
                    }
                    case 324: {
                        if (count == 1) {
                            dataoffset = val;
                            break;
                        }
                        break;
                    }
                    case 325: {
                        if (count == 1) {
                            datalength = val;
                            break;
                        }
                        break;
                    }
                }
            }
            if (k == -1000) {
                throw new IOException("First image in tiff is not CCITT T4 or T6 compressed");
            }
            if (dataoffset == 0) {
                throw new IOException("First image in tiff is not a single tile/strip");
            }
            parms.setInt(COSName.K, k);
            raf.seek(dataoffset);
            final byte[] buf = new byte[8192];
            int amountRead = -1;
            while ((amountRead = raf.read(buf, 0, Math.min(8192, datalength))) > 0) {
                datalength -= amountRead;
                os.write(buf, 0, amountRead);
            }
        }
        finally {
            os.close();
        }
    }
    
    private int readshort(final char endianess, final RandomAccess raf) throws IOException {
        if (endianess == 'I') {
            return raf.read() | raf.read() << 8;
        }
        return raf.read() << 8 | raf.read();
    }
    
    private int readlong(final char endianess, final RandomAccess raf) throws IOException {
        if (endianess == 'I') {
            return raf.read() | raf.read() << 8 | raf.read() << 16 | raf.read() << 24;
        }
        return raf.read() << 24 | raf.read() << 16 | raf.read() << 8 | raf.read();
    }
    
    static {
        (FAX_FILTERS = new ArrayList<String>()).add(COSName.CCITTFAX_DECODE.getName());
        PDCcitt.FAX_FILTERS.add(COSName.CCITTFAX_DECODE_ABBREVIATION.getName());
    }
    
    private class TiffWrapper extends InputStream
    {
        private int currentOffset;
        private byte[] tiffheader;
        private InputStream datastream;
        private final byte[] basicHeader;
        private int additionalOffset;
        
        private TiffWrapper(final InputStream rawstream, final COSDictionary options) {
            this.basicHeader = new byte[] { 73, 73, 42, 0, 8, 0, 0, 0, 0, 0 };
            this.buildHeader(options);
            this.currentOffset = 0;
            this.datastream = rawstream;
        }
        
        @Override
        public boolean markSupported() {
            return false;
        }
        
        @Override
        public void reset() throws IOException {
            throw new IOException("reset not supported");
        }
        
        @Override
        public int read() throws IOException {
            if (this.currentOffset < this.tiffheader.length) {
                return this.tiffheader[this.currentOffset++];
            }
            return this.datastream.read();
        }
        
        @Override
        public int read(final byte[] data) throws IOException {
            if (this.currentOffset < this.tiffheader.length) {
                final int length = Math.min(this.tiffheader.length - this.currentOffset, data.length);
                if (length > 0) {
                    System.arraycopy(this.tiffheader, this.currentOffset, data, 0, length);
                }
                this.currentOffset += length;
                return length;
            }
            return this.datastream.read(data);
        }
        
        @Override
        public int read(final byte[] data, final int off, final int len) throws IOException {
            if (this.currentOffset < this.tiffheader.length) {
                final int length = Math.min(this.tiffheader.length - this.currentOffset, len);
                if (length > 0) {
                    System.arraycopy(this.tiffheader, this.currentOffset, data, off, length);
                }
                this.currentOffset += length;
                return length;
            }
            return this.datastream.read(data, off, len);
        }
        
        @Override
        public long skip(final long n) throws IOException {
            if (this.currentOffset < this.tiffheader.length) {
                final long length = Math.min(this.tiffheader.length - this.currentOffset, n);
                this.currentOffset += (int)length;
                return length;
            }
            return this.datastream.skip(n);
        }
        
        private void buildHeader(final COSDictionary options) {
            final int numOfTags = 10;
            final int maxAdditionalData = 24;
            final int ifdSize = 134;
            Arrays.fill(this.tiffheader = new byte[ifdSize + 24], (byte)0);
            System.arraycopy(this.basicHeader, 0, this.tiffheader, 0, this.basicHeader.length);
            this.additionalOffset = ifdSize;
            short cols = 1728;
            short rows = 0;
            short blackis1 = 0;
            short comptype = 3;
            long t4options = 0L;
            final COSArray decode = PDCcitt.this.getDecode();
            if (decode != null && decode.getInt(0) == 1) {
                blackis1 = 1;
            }
            final COSBase dicOrArrayParms = options.getDictionaryObject(COSName.DECODE_PARMS);
            COSDictionary decodeParms = null;
            if (dicOrArrayParms instanceof COSDictionary) {
                decodeParms = (COSDictionary)dicOrArrayParms;
            }
            else {
                final COSArray parmsArray = (COSArray)dicOrArrayParms;
                if (parmsArray.size() == 1) {
                    decodeParms = (COSDictionary)parmsArray.getObject(0);
                }
                else {
                    for (int i = 0; i < parmsArray.size() && decodeParms == null; ++i) {
                        final COSDictionary dic = (COSDictionary)parmsArray.getObject(i);
                        if (dic != null && (dic.getDictionaryObject(COSName.COLUMNS) != null || dic.getDictionaryObject(COSName.ROWS) != null)) {
                            decodeParms = dic;
                        }
                    }
                }
            }
            if (decodeParms != null) {
                cols = (short)decodeParms.getInt(COSName.COLUMNS, cols);
                rows = (short)decodeParms.getInt(COSName.ROWS, rows);
                if (decodeParms.getBoolean(COSName.BLACK_IS_1, false)) {
                    blackis1 = 1;
                }
                final int k = decodeParms.getInt(COSName.K, 0);
                if (k < 0) {
                    comptype = 4;
                }
                if (k > 0) {
                    comptype = 3;
                    t4options = 1L;
                }
            }
            if (rows == 0) {
                rows = (short)options.getInt(COSName.HEIGHT, rows);
            }
            this.addTag(256, cols);
            this.addTag(257, rows);
            this.addTag(259, comptype);
            this.addTag(262, blackis1);
            this.addTag(273, this.tiffheader.length);
            this.addTag(279, options.getInt(COSName.LENGTH));
            this.addTag(282, 300L, 1L);
            this.addTag(283, 300L, 1L);
            if (comptype == 3) {
                this.addTag(292, t4options);
            }
            this.addTag(305, "PDFBOX");
        }
        
        private void addTag(final int tag, final long value) {
            final byte[] tiffheader = this.tiffheader;
            final int n = 8;
            final byte b = (byte)(tiffheader[n] + 1);
            tiffheader[n] = b;
            final int count = b;
            final int offset = (count - 1) * 12 + 10;
            this.tiffheader[offset] = (byte)(tag & 0xFF);
            this.tiffheader[offset + 1] = (byte)(tag >> 8 & 0xFF);
            this.tiffheader[offset + 2] = 4;
            this.tiffheader[offset + 4] = 1;
            this.tiffheader[offset + 8] = (byte)(value & 0xFFL);
            this.tiffheader[offset + 9] = (byte)(value >> 8 & 0xFFL);
            this.tiffheader[offset + 10] = (byte)(value >> 16 & 0xFFL);
            this.tiffheader[offset + 11] = (byte)(value >> 24 & 0xFFL);
        }
        
        private void addTag(final int tag, final short value) {
            final byte[] tiffheader = this.tiffheader;
            final int n = 8;
            final byte b = (byte)(tiffheader[n] + 1);
            tiffheader[n] = b;
            final int count = b;
            final int offset = (count - 1) * 12 + 10;
            this.tiffheader[offset] = (byte)(tag & 0xFF);
            this.tiffheader[offset + 1] = (byte)(tag >> 8 & 0xFF);
            this.tiffheader[offset + 2] = 3;
            this.tiffheader[offset + 4] = 1;
            this.tiffheader[offset + 8] = (byte)(value & 0xFF);
            this.tiffheader[offset + 9] = (byte)(value >> 8 & 0xFF);
        }
        
        private void addTag(final int tag, final String value) {
            final byte[] tiffheader = this.tiffheader;
            final int n = 8;
            final byte b = (byte)(tiffheader[n] + 1);
            tiffheader[n] = b;
            final int count = b;
            final int offset = (count - 1) * 12 + 10;
            this.tiffheader[offset] = (byte)(tag & 0xFF);
            this.tiffheader[offset + 1] = (byte)(tag >> 8 & 0xFF);
            this.tiffheader[offset + 2] = 2;
            final int len = value.length() + 1;
            this.tiffheader[offset + 4] = (byte)(len & 0xFF);
            this.tiffheader[offset + 8] = (byte)(this.additionalOffset & 0xFF);
            this.tiffheader[offset + 9] = (byte)(this.additionalOffset >> 8 & 0xFF);
            this.tiffheader[offset + 10] = (byte)(this.additionalOffset >> 16 & 0xFF);
            this.tiffheader[offset + 11] = (byte)(this.additionalOffset >> 24 & 0xFF);
            try {
                System.arraycopy(value.getBytes("US-ASCII"), 0, this.tiffheader, this.additionalOffset, value.length());
            }
            catch (UnsupportedEncodingException e) {
                throw new RuntimeException("Incompatible VM without US-ASCII encoding", e);
            }
            this.additionalOffset += len;
        }
        
        private void addTag(final int tag, final long numerator, final long denominator) {
            final byte[] tiffheader = this.tiffheader;
            final int n = 8;
            final byte b = (byte)(tiffheader[n] + 1);
            tiffheader[n] = b;
            final int count = b;
            final int offset = (count - 1) * 12 + 10;
            this.tiffheader[offset] = (byte)(tag & 0xFF);
            this.tiffheader[offset + 1] = (byte)(tag >> 8 & 0xFF);
            this.tiffheader[offset + 2] = 5;
            this.tiffheader[offset + 4] = 1;
            this.tiffheader[offset + 8] = (byte)(this.additionalOffset & 0xFF);
            this.tiffheader[offset + 9] = (byte)(this.additionalOffset >> 8 & 0xFF);
            this.tiffheader[offset + 10] = (byte)(this.additionalOffset >> 16 & 0xFF);
            this.tiffheader[offset + 11] = (byte)(this.additionalOffset >> 24 & 0xFF);
            this.tiffheader[this.additionalOffset++] = (byte)(numerator & 0xFFL);
            this.tiffheader[this.additionalOffset++] = (byte)(numerator >> 8 & 0xFFL);
            this.tiffheader[this.additionalOffset++] = (byte)(numerator >> 16 & 0xFFL);
            this.tiffheader[this.additionalOffset++] = (byte)(numerator >> 24 & 0xFFL);
            this.tiffheader[this.additionalOffset++] = (byte)(denominator & 0xFFL);
            this.tiffheader[this.additionalOffset++] = (byte)(denominator >> 8 & 0xFFL);
            this.tiffheader[this.additionalOffset++] = (byte)(denominator >> 16 & 0xFFL);
            this.tiffheader[this.additionalOffset++] = (byte)(denominator >> 24 & 0xFFL);
        }
    }
}
