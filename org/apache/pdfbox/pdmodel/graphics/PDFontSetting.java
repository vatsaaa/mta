// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.graphics;

import org.apache.pdfbox.cos.COSNumber;
import java.io.IOException;
import org.apache.pdfbox.pdmodel.font.PDFontFactory;
import org.apache.pdfbox.cos.COSDictionary;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.cos.COSFloat;
import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSArray;
import org.apache.pdfbox.pdmodel.common.COSObjectable;

public class PDFontSetting implements COSObjectable
{
    private COSArray fontSetting;
    
    public PDFontSetting() {
        this.fontSetting = null;
        (this.fontSetting = new COSArray()).add(null);
        this.fontSetting.add(new COSFloat(1.0f));
    }
    
    public PDFontSetting(final COSArray fs) {
        this.fontSetting = null;
        this.fontSetting = fs;
    }
    
    public COSBase getCOSObject() {
        return this.fontSetting;
    }
    
    public PDFont getFont() throws IOException {
        PDFont retval = null;
        final COSBase font = this.fontSetting.get(0);
        if (font instanceof COSDictionary) {
            retval = PDFontFactory.createFont((COSDictionary)font);
        }
        return retval;
    }
    
    public void setFont(final PDFont font) {
        this.fontSetting.set(0, font);
    }
    
    public float getFontSize() {
        final COSNumber size = (COSNumber)this.fontSetting.get(1);
        return size.floatValue();
    }
    
    public void setFontSize(final float size) {
        this.fontSetting.set(1, new COSFloat(size));
    }
}
