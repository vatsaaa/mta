// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.documentinterchange.taggedpdf;

import org.apache.pdfbox.pdmodel.common.COSObjectable;
import org.apache.pdfbox.pdmodel.graphics.color.PDGamma;
import org.apache.pdfbox.cos.COSFloat;
import org.apache.pdfbox.cos.COSNumber;
import org.apache.pdfbox.cos.COSString;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.cos.COSArray;
import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSDictionary;
import org.apache.pdfbox.pdmodel.documentinterchange.logicalstructure.PDAttributeObject;

public abstract class PDStandardAttributeObject extends PDAttributeObject
{
    protected static final float UNSPECIFIED = -1.0f;
    
    public PDStandardAttributeObject() {
    }
    
    public PDStandardAttributeObject(final COSDictionary dictionary) {
        super(dictionary);
    }
    
    public boolean isSpecified(final String name) {
        return this.getCOSDictionary().getDictionaryObject(name) != null;
    }
    
    protected String getString(final String name) {
        return this.getCOSDictionary().getString(name);
    }
    
    protected void setString(final String name, final String value) {
        final COSBase oldBase = this.getCOSDictionary().getDictionaryObject(name);
        this.getCOSDictionary().setString(name, value);
        final COSBase newBase = this.getCOSDictionary().getDictionaryObject(name);
        this.potentiallyNotifyChanged(oldBase, newBase);
    }
    
    protected String[] getArrayOfString(final String name) {
        final COSBase v = this.getCOSDictionary().getDictionaryObject(name);
        if (v instanceof COSArray) {
            final COSArray array = (COSArray)v;
            final String[] strings = new String[array.size()];
            for (int i = 0; i < array.size(); ++i) {
                strings[i] = ((COSName)array.getObject(i)).getName();
            }
            return strings;
        }
        return null;
    }
    
    protected void setArrayOfString(final String name, final String[] values) {
        final COSBase oldBase = this.getCOSDictionary().getDictionaryObject(name);
        final COSArray array = new COSArray();
        for (int i = 0; i < values.length; ++i) {
            array.add(new COSString(values[i]));
        }
        this.getCOSDictionary().setItem(name, array);
        final COSBase newBase = this.getCOSDictionary().getDictionaryObject(name);
        this.potentiallyNotifyChanged(oldBase, newBase);
    }
    
    protected String getName(final String name) {
        return this.getCOSDictionary().getNameAsString(name);
    }
    
    protected String getName(final String name, final String defaultValue) {
        return this.getCOSDictionary().getNameAsString(name, defaultValue);
    }
    
    protected Object getNameOrArrayOfName(final String name, final String defaultValue) {
        final COSBase v = this.getCOSDictionary().getDictionaryObject(name);
        if (v instanceof COSArray) {
            final COSArray array = (COSArray)v;
            final String[] names = new String[array.size()];
            for (int i = 0; i < array.size(); ++i) {
                final COSBase item = array.getObject(i);
                if (item instanceof COSName) {
                    names[i] = ((COSName)item).getName();
                }
            }
            return names;
        }
        if (v instanceof COSName) {
            return ((COSName)v).getName();
        }
        return defaultValue;
    }
    
    protected void setName(final String name, final String value) {
        final COSBase oldBase = this.getCOSDictionary().getDictionaryObject(name);
        this.getCOSDictionary().setName(name, value);
        final COSBase newBase = this.getCOSDictionary().getDictionaryObject(name);
        this.potentiallyNotifyChanged(oldBase, newBase);
    }
    
    protected void setArrayOfName(final String name, final String[] values) {
        final COSBase oldBase = this.getCOSDictionary().getDictionaryObject(name);
        final COSArray array = new COSArray();
        for (int i = 0; i < values.length; ++i) {
            array.add(COSName.getPDFName(values[i]));
        }
        this.getCOSDictionary().setItem(name, array);
        final COSBase newBase = this.getCOSDictionary().getDictionaryObject(name);
        this.potentiallyNotifyChanged(oldBase, newBase);
    }
    
    protected Object getNumberOrName(final String name, final String defaultValue) {
        final COSBase value = this.getCOSDictionary().getDictionaryObject(name);
        if (value instanceof COSNumber) {
            return ((COSNumber)value).floatValue();
        }
        if (value instanceof COSName) {
            return ((COSName)value).getName();
        }
        return defaultValue;
    }
    
    protected int getInteger(final String name, final int defaultValue) {
        return this.getCOSDictionary().getInt(name, defaultValue);
    }
    
    protected void setInteger(final String name, final int value) {
        final COSBase oldBase = this.getCOSDictionary().getDictionaryObject(name);
        this.getCOSDictionary().setInt(name, value);
        final COSBase newBase = this.getCOSDictionary().getDictionaryObject(name);
        this.potentiallyNotifyChanged(oldBase, newBase);
    }
    
    protected float getNumber(final String name, final float defaultValue) {
        return this.getCOSDictionary().getFloat(name, defaultValue);
    }
    
    protected float getNumber(final String name) {
        return this.getCOSDictionary().getFloat(name);
    }
    
    protected Object getNumberOrArrayOfNumber(final String name, final float defaultValue) {
        final COSBase v = this.getCOSDictionary().getDictionaryObject(name);
        if (v instanceof COSArray) {
            final COSArray array = (COSArray)v;
            final float[] values = new float[array.size()];
            for (int i = 0; i < array.size(); ++i) {
                final COSBase item = array.getObject(i);
                if (item instanceof COSNumber) {
                    values[i] = ((COSNumber)item).floatValue();
                }
            }
            return values;
        }
        if (v instanceof COSNumber) {
            return ((COSNumber)v).floatValue();
        }
        if (defaultValue == -1.0f) {
            return null;
        }
        return defaultValue;
    }
    
    protected void setNumber(final String name, final float value) {
        final COSBase oldBase = this.getCOSDictionary().getDictionaryObject(name);
        this.getCOSDictionary().setFloat(name, value);
        final COSBase newBase = this.getCOSDictionary().getDictionaryObject(name);
        this.potentiallyNotifyChanged(oldBase, newBase);
    }
    
    protected void setNumber(final String name, final int value) {
        final COSBase oldBase = this.getCOSDictionary().getDictionaryObject(name);
        this.getCOSDictionary().setInt(name, value);
        final COSBase newBase = this.getCOSDictionary().getDictionaryObject(name);
        this.potentiallyNotifyChanged(oldBase, newBase);
    }
    
    protected void setArrayOfNumber(final String name, final float[] values) {
        final COSArray array = new COSArray();
        for (int i = 0; i < values.length; ++i) {
            array.add(new COSFloat(values[i]));
        }
        final COSBase oldBase = this.getCOSDictionary().getDictionaryObject(name);
        this.getCOSDictionary().setItem(name, array);
        final COSBase newBase = this.getCOSDictionary().getDictionaryObject(name);
        this.potentiallyNotifyChanged(oldBase, newBase);
    }
    
    protected PDGamma getColor(final String name) {
        final COSArray c = (COSArray)this.getCOSDictionary().getDictionaryObject(name);
        if (c != null) {
            return new PDGamma(c);
        }
        return null;
    }
    
    protected Object getColorOrFourColors(final String name) {
        final COSArray array = (COSArray)this.getCOSDictionary().getDictionaryObject(name);
        if (array == null) {
            return null;
        }
        if (array.size() == 3) {
            return new PDGamma(array);
        }
        if (array.size() == 4) {
            return new PDFourColours(array);
        }
        return null;
    }
    
    protected void setColor(final String name, final PDGamma value) {
        final COSBase oldValue = this.getCOSDictionary().getDictionaryObject(name);
        this.getCOSDictionary().setItem(name, value);
        final COSBase newValue = (value == null) ? null : value.getCOSObject();
        this.potentiallyNotifyChanged(oldValue, newValue);
    }
    
    protected void setFourColors(final String name, final PDFourColours value) {
        final COSBase oldValue = this.getCOSDictionary().getDictionaryObject(name);
        this.getCOSDictionary().setItem(name, value);
        final COSBase newValue = (value == null) ? null : value.getCOSObject();
        this.potentiallyNotifyChanged(oldValue, newValue);
    }
}
