// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.documentinterchange.taggedpdf;

import org.apache.pdfbox.pdmodel.documentinterchange.logicalstructure.PDAttributeObject;
import org.apache.pdfbox.cos.COSDictionary;

public class PDTableAttributeObject extends PDStandardAttributeObject
{
    public static final String OWNER_TABLE = "Table";
    protected static final String ROW_SPAN = "RowSpan";
    protected static final String COL_SPAN = "ColSpan";
    protected static final String HEADERS = "Headers";
    protected static final String SCOPE = "Scope";
    protected static final String SUMMARY = "Summary";
    public static final String SCOPE_BOTH = "Both";
    public static final String SCOPE_COLUMN = "Column";
    public static final String SCOPE_ROW = "Row";
    
    public PDTableAttributeObject() {
        this.setOwner("Table");
    }
    
    public PDTableAttributeObject(final COSDictionary dictionary) {
        super(dictionary);
    }
    
    public int getRowSpan() {
        return this.getInteger("RowSpan", 1);
    }
    
    public void setRowSpan(final int rowSpan) {
        this.setInteger("RowSpan", rowSpan);
    }
    
    public int getColSpan() {
        return this.getInteger("ColSpan", 1);
    }
    
    public void setColSpan(final int colSpan) {
        this.setInteger("ColSpan", colSpan);
    }
    
    public String[] getHeaders() {
        return this.getArrayOfString("Headers");
    }
    
    public void setHeaders(final String[] headers) {
        this.setArrayOfString("Headers", headers);
    }
    
    public String getScope() {
        return this.getName("Scope");
    }
    
    public void setScope(final String scope) {
        this.setName("Scope", scope);
    }
    
    public String getSummary() {
        return this.getString("Summary");
    }
    
    public void setSummary(final String summary) {
        this.setString("Summary", summary);
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder().append(super.toString());
        if (this.isSpecified("RowSpan")) {
            sb.append(", RowSpan=").append(String.valueOf(this.getRowSpan()));
        }
        if (this.isSpecified("ColSpan")) {
            sb.append(", ColSpan=").append(String.valueOf(this.getColSpan()));
        }
        if (this.isSpecified("Headers")) {
            sb.append(", Headers=").append(PDAttributeObject.arrayToString(this.getHeaders()));
        }
        if (this.isSpecified("Scope")) {
            sb.append(", Scope=").append(this.getScope());
        }
        if (this.isSpecified("Summary")) {
            sb.append(", Summary=").append(this.getSummary());
        }
        return sb.toString();
    }
}
