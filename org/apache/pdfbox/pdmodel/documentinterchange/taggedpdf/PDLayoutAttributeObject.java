// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.documentinterchange.taggedpdf;

import org.apache.pdfbox.pdmodel.documentinterchange.logicalstructure.PDAttributeObject;
import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.pdmodel.common.COSObjectable;
import org.apache.pdfbox.cos.COSArray;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.graphics.color.PDGamma;
import org.apache.pdfbox.cos.COSDictionary;

public class PDLayoutAttributeObject extends PDStandardAttributeObject
{
    public static final String OWNER_LAYOUT = "Layout";
    private static final String PLACEMENT = "Placement";
    private static final String WRITING_MODE = "WritingMode";
    private static final String BACKGROUND_COLOR = "BackgroundColor";
    private static final String BORDER_COLOR = "BorderColor";
    private static final String BORDER_STYLE = "BorderStyle";
    private static final String BORDER_THICKNESS = "BorderThickness";
    private static final String PADDING = "Padding";
    private static final String COLOR = "Color";
    private static final String SPACE_BEFORE = "SpaceBefore";
    private static final String SPACE_AFTER = "SpaceAfter";
    private static final String START_INDENT = "StartIndent";
    private static final String END_INDENT = "EndIndent";
    private static final String TEXT_INDENT = "TextIndent";
    private static final String TEXT_ALIGN = "TextAlign";
    private static final String BBOX = "BBox";
    private static final String WIDTH = "Width";
    private static final String HEIGHT = "Height";
    private static final String BLOCK_ALIGN = "BlockAlign";
    private static final String INLINE_ALIGN = "InlineAlign";
    private static final String T_BORDER_STYLE = "TBorderStyle";
    private static final String T_PADDING = "TPadding";
    private static final String BASELINE_SHIFT = "BaselineShift";
    private static final String LINE_HEIGHT = "LineHeight";
    private static final String TEXT_DECORATION_COLOR = "TextDecorationColor";
    private static final String TEXT_DECORATION_THICKNESS = "TextDecorationThickness";
    private static final String TEXT_DECORATION_TYPE = "TextDecorationType";
    private static final String RUBY_ALIGN = "RubyAlign";
    private static final String RUBY_POSITION = "RubyPosition";
    private static final String GLYPH_ORIENTATION_VERTICAL = "GlyphOrientationVertical";
    private static final String COLUMN_COUNT = "ColumnCount";
    private static final String COLUMN_GAP = "ColumnGap";
    private static final String COLUMN_WIDTHS = "ColumnWidths";
    public static final String PLACEMENT_BLOCK = "Block";
    public static final String PLACEMENT_INLINE = "Inline";
    public static final String PLACEMENT_BEFORE = "Before";
    public static final String PLACEMENT_START = "Start";
    public static final String PLACEMENT_END = "End";
    public static final String WRITING_MODE_LRTB = "LrTb";
    public static final String WRITING_MODE_RLTB = "RlTb";
    public static final String WRITING_MODE_TBRL = "TbRl";
    public static final String BORDER_STYLE_NONE = "None";
    public static final String BORDER_STYLE_HIDDEN = "Hidden";
    public static final String BORDER_STYLE_DOTTED = "Dotted";
    public static final String BORDER_STYLE_DASHED = "Dashed";
    public static final String BORDER_STYLE_SOLID = "Solid";
    public static final String BORDER_STYLE_DOUBLE = "Double";
    public static final String BORDER_STYLE_GROOVE = "Groove";
    public static final String BORDER_STYLE_RIDGE = "Ridge";
    public static final String BORDER_STYLE_INSET = "Inset";
    public static final String BORDER_STYLE_OUTSET = "Outset";
    public static final String TEXT_ALIGN_START = "Start";
    public static final String TEXT_ALIGN_CENTER = "Center";
    public static final String TEXT_ALIGN_END = "End";
    public static final String TEXT_ALIGN_JUSTIFY = "Justify";
    public static final String WIDTH_AUTO = "Auto";
    public static final String HEIGHT_AUTO = "Auto";
    public static final String BLOCK_ALIGN_BEFORE = "Before";
    public static final String BLOCK_ALIGN_MIDDLE = "Middle";
    public static final String BLOCK_ALIGN_AFTER = "After";
    public static final String BLOCK_ALIGN_JUSTIFY = "Justify";
    public static final String INLINE_ALIGN_START = "Start";
    public static final String INLINE_ALIGN_CENTER = "Center";
    public static final String INLINE_ALIGN_END = "End";
    public static final String LINE_HEIGHT_NORMAL = "Normal";
    public static final String LINE_HEIGHT_AUTO = "Auto";
    public static final String TEXT_DECORATION_TYPE_NONE = "None";
    public static final String TEXT_DECORATION_TYPE_UNDERLINE = "Underline";
    public static final String TEXT_DECORATION_TYPE_OVERLINE = "Overline";
    public static final String TEXT_DECORATION_TYPE_LINE_THROUGH = "LineThrough";
    public static final String RUBY_ALIGN_START = "Start";
    public static final String RUBY_ALIGN_CENTER = "Center";
    public static final String RUBY_ALIGN_END = "End";
    public static final String RUBY_ALIGN_JUSTIFY = "Justify";
    public static final String RUBY_ALIGN_DISTRIBUTE = "Distribute";
    public static final String RUBY_POSITION_BEFORE = "Before";
    public static final String RUBY_POSITION_AFTER = "After";
    public static final String RUBY_POSITION_WARICHU = "Warichu";
    public static final String RUBY_POSITION_INLINE = "Inline";
    public static final String GLYPH_ORIENTATION_VERTICAL_AUTO = "Auto";
    public static final String GLYPH_ORIENTATION_VERTICAL_MINUS_180_DEGREES = "-180";
    public static final String GLYPH_ORIENTATION_VERTICAL_MINUS_90_DEGREES = "-90";
    public static final String GLYPH_ORIENTATION_VERTICAL_ZERO_DEGREES = "0";
    public static final String GLYPH_ORIENTATION_VERTICAL_90_DEGREES = "90";
    public static final String GLYPH_ORIENTATION_VERTICAL_180_DEGREES = "180";
    public static final String GLYPH_ORIENTATION_VERTICAL_270_DEGREES = "270";
    public static final String GLYPH_ORIENTATION_VERTICAL_360_DEGREES = "360";
    
    public PDLayoutAttributeObject() {
        this.setOwner("Layout");
    }
    
    public PDLayoutAttributeObject(final COSDictionary dictionary) {
        super(dictionary);
    }
    
    public String getPlacement() {
        return this.getName("Placement", "Inline");
    }
    
    public void setPlacement(final String placement) {
        this.setName("Placement", placement);
    }
    
    public String getWritingMode() {
        return this.getName("WritingMode", "LrTb");
    }
    
    public void setWritingMode(final String writingMode) {
        this.setName("WritingMode", writingMode);
    }
    
    public PDGamma getBackgroundColor() {
        return this.getColor("BackgroundColor");
    }
    
    public void setBackgroundColor(final PDGamma backgroundColor) {
        this.setColor("BackgroundColor", backgroundColor);
    }
    
    public Object getBorderColors() {
        return this.getColorOrFourColors("BorderColor");
    }
    
    public void setAllBorderColors(final PDGamma borderColor) {
        this.setColor("BorderColor", borderColor);
    }
    
    public void setBorderColors(final PDFourColours borderColors) {
        this.setFourColors("BorderColor", borderColors);
    }
    
    public Object getBorderStyle() {
        return this.getNameOrArrayOfName("BorderStyle", "None");
    }
    
    public void setAllBorderStyles(final String borderStyle) {
        this.setName("BorderStyle", borderStyle);
    }
    
    public void setBorderStyles(final String[] borderStyles) {
        this.setArrayOfName("BorderStyle", borderStyles);
    }
    
    public Object getBorderThickness() {
        return this.getNumberOrArrayOfNumber("BorderThickness", -1.0f);
    }
    
    public void setAllBorderThicknesses(final float borderThickness) {
        this.setNumber("BorderThickness", borderThickness);
    }
    
    public void setAllBorderThicknesses(final int borderThickness) {
        this.setNumber("BorderThickness", borderThickness);
    }
    
    public void setBorderThicknesses(final float[] borderThicknesses) {
        this.setArrayOfNumber("BorderThickness", borderThicknesses);
    }
    
    public Object getPadding() {
        return this.getNumberOrArrayOfNumber("Padding", 0.0f);
    }
    
    public void setAllPaddings(final float padding) {
        this.setNumber("Padding", padding);
    }
    
    public void setAllPaddings(final int padding) {
        this.setNumber("Padding", padding);
    }
    
    public void setPaddings(final float[] paddings) {
        this.setArrayOfNumber("Padding", paddings);
    }
    
    public PDGamma getColor() {
        return this.getColor("Color");
    }
    
    public void setColor(final PDGamma color) {
        this.setColor("Color", color);
    }
    
    public float getSpaceBefore() {
        return this.getNumber("SpaceBefore", 0.0f);
    }
    
    public void setSpaceBefore(final float spaceBefore) {
        this.setNumber("SpaceBefore", spaceBefore);
    }
    
    public void setSpaceBefore(final int spaceBefore) {
        this.setNumber("SpaceBefore", spaceBefore);
    }
    
    public float getSpaceAfter() {
        return this.getNumber("SpaceAfter", 0.0f);
    }
    
    public void setSpaceAfter(final float spaceAfter) {
        this.setNumber("SpaceAfter", spaceAfter);
    }
    
    public void setSpaceAfter(final int spaceAfter) {
        this.setNumber("SpaceAfter", spaceAfter);
    }
    
    public float getStartIndent() {
        return this.getNumber("StartIndent", 0.0f);
    }
    
    public void setStartIndent(final float startIndent) {
        this.setNumber("StartIndent", startIndent);
    }
    
    public void setStartIndent(final int startIndent) {
        this.setNumber("StartIndent", startIndent);
    }
    
    public float getEndIndent() {
        return this.getNumber("EndIndent", 0.0f);
    }
    
    public void setEndIndent(final float endIndent) {
        this.setNumber("EndIndent", endIndent);
    }
    
    public void setEndIndent(final int endIndent) {
        this.setNumber("EndIndent", endIndent);
    }
    
    public float getTextIndent() {
        return this.getNumber("TextIndent", 0.0f);
    }
    
    public void setTextIndent(final float textIndent) {
        this.setNumber("TextIndent", textIndent);
    }
    
    public void setTextIndent(final int textIndent) {
        this.setNumber("TextIndent", textIndent);
    }
    
    public String getTextAlign() {
        return this.getName("TextAlign", "Start");
    }
    
    public void setTextAlign(final String textIndent) {
        this.setName("TextAlign", textIndent);
    }
    
    public PDRectangle getBBox() {
        final COSArray array = (COSArray)this.getCOSDictionary().getDictionaryObject("BBox");
        if (array != null) {
            return new PDRectangle(array);
        }
        return null;
    }
    
    public void setBBox(final PDRectangle bbox) {
        final String name = "BBox";
        final COSBase oldValue = this.getCOSDictionary().getDictionaryObject(name);
        this.getCOSDictionary().setItem(name, bbox);
        final COSBase newValue = (bbox == null) ? null : bbox.getCOSObject();
        this.potentiallyNotifyChanged(oldValue, newValue);
    }
    
    public Object getWidth() {
        return this.getNumberOrName("Width", "Auto");
    }
    
    public void setWidthAuto() {
        this.setName("Width", "Auto");
    }
    
    public void setWidth(final float width) {
        this.setNumber("Width", width);
    }
    
    public void setWidth(final int width) {
        this.setNumber("Width", width);
    }
    
    public Object getHeight() {
        return this.getNumberOrName("Height", "Auto");
    }
    
    public void setHeightAuto() {
        this.setName("Height", "Auto");
    }
    
    public void setHeight(final float height) {
        this.setNumber("Height", height);
    }
    
    public void setHeight(final int height) {
        this.setNumber("Height", height);
    }
    
    public String getBlockAlign() {
        return this.getName("BlockAlign", "Before");
    }
    
    public void setBlockAlign(final String blockAlign) {
        this.setName("BlockAlign", blockAlign);
    }
    
    public String getInlineAlign() {
        return this.getName("InlineAlign", "Start");
    }
    
    public void setInlineAlign(final String inlineAlign) {
        this.setName("InlineAlign", inlineAlign);
    }
    
    public Object getTBorderStyle() {
        return this.getNameOrArrayOfName("TBorderStyle", "None");
    }
    
    public void setAllTBorderStyles(final String tBorderStyle) {
        this.setName("TBorderStyle", tBorderStyle);
    }
    
    public void setTBorderStyles(final String[] tBorderStyles) {
        this.setArrayOfName("TBorderStyle", tBorderStyles);
    }
    
    public Object getTPadding() {
        return this.getNumberOrArrayOfNumber("TPadding", 0.0f);
    }
    
    public void setAllTPaddings(final float tPadding) {
        this.setNumber("TPadding", tPadding);
    }
    
    public void setAllTPaddings(final int tPadding) {
        this.setNumber("TPadding", tPadding);
    }
    
    public void setTPaddings(final float[] tPaddings) {
        this.setArrayOfNumber("TPadding", tPaddings);
    }
    
    public float getBaselineShift() {
        return this.getNumber("BaselineShift", 0.0f);
    }
    
    public void setBaselineShift(final float baselineShift) {
        this.setNumber("BaselineShift", baselineShift);
    }
    
    public void setBaselineShift(final int baselineShift) {
        this.setNumber("BaselineShift", baselineShift);
    }
    
    public Object getLineHeight() {
        return this.getNumberOrName("LineHeight", "Normal");
    }
    
    public void setLineHeightNormal() {
        this.setName("LineHeight", "Normal");
    }
    
    public void setLineHeightAuto() {
        this.setName("LineHeight", "Auto");
    }
    
    public void setLineHeight(final float lineHeight) {
        this.setNumber("LineHeight", lineHeight);
    }
    
    public void setLineHeight(final int lineHeight) {
        this.setNumber("LineHeight", lineHeight);
    }
    
    public PDGamma getTextDecorationColor() {
        return this.getColor("TextDecorationColor");
    }
    
    public void setTextDecorationColor(final PDGamma textDecorationColor) {
        this.setColor("TextDecorationColor", textDecorationColor);
    }
    
    public float getTextDecorationThickness() {
        return this.getNumber("TextDecorationThickness");
    }
    
    public void setTextDecorationThickness(final float textDecorationThickness) {
        this.setNumber("TextDecorationThickness", textDecorationThickness);
    }
    
    public void setTextDecorationThickness(final int textDecorationThickness) {
        this.setNumber("TextDecorationThickness", textDecorationThickness);
    }
    
    public String getTextDecorationType() {
        return this.getName("TextDecorationType", "None");
    }
    
    public void setTextDecorationType(final String textDecorationType) {
        this.setName("TextDecorationType", textDecorationType);
    }
    
    public String getRubyAlign() {
        return this.getName("RubyAlign", "Distribute");
    }
    
    public void setRubyAlign(final String rubyAlign) {
        this.setName("RubyAlign", rubyAlign);
    }
    
    public String getRubyPosition() {
        return this.getName("RubyPosition", "Before");
    }
    
    public void setRubyPosition(final String rubyPosition) {
        this.setName("RubyPosition", rubyPosition);
    }
    
    public String getGlyphOrientationVertical() {
        return this.getName("GlyphOrientationVertical", "Auto");
    }
    
    public void setGlyphOrientationVertical(final String glyphOrientationVertical) {
        this.setName("GlyphOrientationVertical", glyphOrientationVertical);
    }
    
    public int getColumnCount() {
        return this.getInteger("ColumnCount", 1);
    }
    
    public void setColumnCount(final int columnCount) {
        this.setInteger("ColumnCount", columnCount);
    }
    
    public Object getColumnGap() {
        return this.getNumberOrArrayOfNumber("ColumnGap", -1.0f);
    }
    
    public void setColumnGap(final float columnGap) {
        this.setNumber("ColumnGap", columnGap);
    }
    
    public void setColumnGap(final int columnGap) {
        this.setNumber("ColumnGap", columnGap);
    }
    
    public void setColumnGaps(final float[] columnGaps) {
        this.setArrayOfNumber("ColumnGap", columnGaps);
    }
    
    public Object getColumnWidths() {
        return this.getNumberOrArrayOfNumber("ColumnWidths", -1.0f);
    }
    
    public void setAllColumnWidths(final float columnWidth) {
        this.setNumber("ColumnWidths", columnWidth);
    }
    
    public void setAllColumnWidths(final int columnWidth) {
        this.setNumber("ColumnWidths", columnWidth);
    }
    
    public void setColumnWidths(final float[] columnWidths) {
        this.setArrayOfNumber("ColumnWidths", columnWidths);
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder().append(super.toString());
        if (this.isSpecified("Placement")) {
            sb.append(", Placement=").append(this.getPlacement());
        }
        if (this.isSpecified("WritingMode")) {
            sb.append(", WritingMode=").append(this.getWritingMode());
        }
        if (this.isSpecified("BackgroundColor")) {
            sb.append(", BackgroundColor=").append(this.getBackgroundColor());
        }
        if (this.isSpecified("BorderColor")) {
            sb.append(", BorderColor=").append(this.getBorderColors());
        }
        if (this.isSpecified("BorderStyle")) {
            final Object borderStyle = this.getBorderStyle();
            sb.append(", BorderStyle=");
            if (borderStyle instanceof String[]) {
                sb.append(PDAttributeObject.arrayToString((Object[])borderStyle));
            }
            else {
                sb.append(borderStyle);
            }
        }
        if (this.isSpecified("BorderThickness")) {
            final Object borderThickness = this.getBorderThickness();
            sb.append(", BorderThickness=");
            if (borderThickness instanceof float[]) {
                sb.append(PDAttributeObject.arrayToString((float[])borderThickness));
            }
            else {
                sb.append(String.valueOf(borderThickness));
            }
        }
        if (this.isSpecified("Padding")) {
            final Object padding = this.getPadding();
            sb.append(", Padding=");
            if (padding instanceof float[]) {
                sb.append(PDAttributeObject.arrayToString((float[])padding));
            }
            else {
                sb.append(String.valueOf(padding));
            }
        }
        if (this.isSpecified("Color")) {
            sb.append(", Color=").append(this.getColor());
        }
        if (this.isSpecified("SpaceBefore")) {
            sb.append(", SpaceBefore=").append(String.valueOf(this.getSpaceBefore()));
        }
        if (this.isSpecified("SpaceAfter")) {
            sb.append(", SpaceAfter=").append(String.valueOf(this.getSpaceAfter()));
        }
        if (this.isSpecified("StartIndent")) {
            sb.append(", StartIndent=").append(String.valueOf(this.getStartIndent()));
        }
        if (this.isSpecified("EndIndent")) {
            sb.append(", EndIndent=").append(String.valueOf(this.getEndIndent()));
        }
        if (this.isSpecified("TextIndent")) {
            sb.append(", TextIndent=").append(String.valueOf(this.getTextIndent()));
        }
        if (this.isSpecified("TextAlign")) {
            sb.append(", TextAlign=").append(this.getTextAlign());
        }
        if (this.isSpecified("BBox")) {
            sb.append(", BBox=").append(this.getBBox());
        }
        if (this.isSpecified("Width")) {
            final Object width = this.getWidth();
            sb.append(", Width=");
            if (width instanceof Float) {
                sb.append(String.valueOf(width));
            }
            else {
                sb.append(width);
            }
        }
        if (this.isSpecified("Height")) {
            final Object height = this.getHeight();
            sb.append(", Height=");
            if (height instanceof Float) {
                sb.append(String.valueOf(height));
            }
            else {
                sb.append(height);
            }
        }
        if (this.isSpecified("BlockAlign")) {
            sb.append(", BlockAlign=").append(this.getBlockAlign());
        }
        if (this.isSpecified("InlineAlign")) {
            sb.append(", InlineAlign=").append(this.getInlineAlign());
        }
        if (this.isSpecified("TBorderStyle")) {
            final Object tBorderStyle = this.getTBorderStyle();
            sb.append(", TBorderStyle=");
            if (tBorderStyle instanceof String[]) {
                sb.append(PDAttributeObject.arrayToString((Object[])tBorderStyle));
            }
            else {
                sb.append(tBorderStyle);
            }
        }
        if (this.isSpecified("TPadding")) {
            final Object tPadding = this.getTPadding();
            sb.append(", TPadding=");
            if (tPadding instanceof float[]) {
                sb.append(PDAttributeObject.arrayToString((float[])tPadding));
            }
            else {
                sb.append(String.valueOf(tPadding));
            }
        }
        if (this.isSpecified("BaselineShift")) {
            sb.append(", BaselineShift=").append(String.valueOf(this.getBaselineShift()));
        }
        if (this.isSpecified("LineHeight")) {
            final Object lineHeight = this.getLineHeight();
            sb.append(", LineHeight=");
            if (lineHeight instanceof Float) {
                sb.append(String.valueOf(lineHeight));
            }
            else {
                sb.append(lineHeight);
            }
        }
        if (this.isSpecified("TextDecorationColor")) {
            sb.append(", TextDecorationColor=").append(this.getTextDecorationColor());
        }
        if (this.isSpecified("TextDecorationThickness")) {
            sb.append(", TextDecorationThickness=").append(String.valueOf(this.getTextDecorationThickness()));
        }
        if (this.isSpecified("TextDecorationType")) {
            sb.append(", TextDecorationType=").append(this.getTextDecorationType());
        }
        if (this.isSpecified("RubyAlign")) {
            sb.append(", RubyAlign=").append(this.getRubyAlign());
        }
        if (this.isSpecified("RubyPosition")) {
            sb.append(", RubyPosition=").append(this.getRubyPosition());
        }
        if (this.isSpecified("GlyphOrientationVertical")) {
            sb.append(", GlyphOrientationVertical=").append(this.getGlyphOrientationVertical());
        }
        if (this.isSpecified("ColumnCount")) {
            sb.append(", ColumnCount=").append(String.valueOf(this.getColumnCount()));
        }
        if (this.isSpecified("ColumnGap")) {
            final Object columnGap = this.getColumnGap();
            sb.append(", ColumnGap=");
            if (columnGap instanceof float[]) {
                sb.append(PDAttributeObject.arrayToString((float[])columnGap));
            }
            else {
                sb.append(String.valueOf(columnGap));
            }
        }
        if (this.isSpecified("ColumnWidths")) {
            final Object columnWidth = this.getColumnWidths();
            sb.append(", ColumnWidths=");
            if (columnWidth instanceof float[]) {
                sb.append(PDAttributeObject.arrayToString((float[])columnWidth));
            }
            else {
                sb.append(String.valueOf(columnWidth));
            }
        }
        return sb.toString();
    }
}
