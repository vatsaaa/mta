// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.documentinterchange.taggedpdf;

import org.apache.pdfbox.pdmodel.documentinterchange.logicalstructure.PDAttributeObject;
import org.apache.pdfbox.cos.COSDictionary;

public class PDExportFormatAttributeObject extends PDLayoutAttributeObject
{
    public static final String OWNER_XML_1_00 = "XML-1.00";
    public static final String OWNER_HTML_3_20 = "HTML-3.2";
    public static final String OWNER_HTML_4_01 = "HTML-4.01";
    public static final String OWNER_OEB_1_00 = "OEB-1.00";
    public static final String OWNER_RTF_1_05 = "RTF-1.05";
    public static final String OWNER_CSS_1_00 = "CSS-1.00";
    public static final String OWNER_CSS_2_00 = "CSS-2.00";
    
    public PDExportFormatAttributeObject(final String owner) {
        this.setOwner(owner);
    }
    
    public PDExportFormatAttributeObject(final COSDictionary dictionary) {
        super(dictionary);
    }
    
    public String getListNumbering() {
        return this.getName("ListNumbering", "None");
    }
    
    public void setListNumbering(final String listNumbering) {
        this.setName("ListNumbering", listNumbering);
    }
    
    public int getRowSpan() {
        return this.getInteger("RowSpan", 1);
    }
    
    public void setRowSpan(final int rowSpan) {
        this.setInteger("RowSpan", rowSpan);
    }
    
    public int getColSpan() {
        return this.getInteger("ColSpan", 1);
    }
    
    public void setColSpan(final int colSpan) {
        this.setInteger("ColSpan", colSpan);
    }
    
    public String[] getHeaders() {
        return this.getArrayOfString("Headers");
    }
    
    public void setHeaders(final String[] headers) {
        this.setArrayOfString("Headers", headers);
    }
    
    public String getScope() {
        return this.getName("Scope");
    }
    
    public void setScope(final String scope) {
        this.setName("Scope", scope);
    }
    
    public String getSummary() {
        return this.getString("Summary");
    }
    
    public void setSummary(final String summary) {
        this.setString("Summary", summary);
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder().append(super.toString());
        if (this.isSpecified("ListNumbering")) {
            sb.append(", ListNumbering=").append(this.getListNumbering());
        }
        if (this.isSpecified("RowSpan")) {
            sb.append(", RowSpan=").append(String.valueOf(this.getRowSpan()));
        }
        if (this.isSpecified("ColSpan")) {
            sb.append(", ColSpan=").append(String.valueOf(this.getColSpan()));
        }
        if (this.isSpecified("Headers")) {
            sb.append(", Headers=").append(PDAttributeObject.arrayToString(this.getHeaders()));
        }
        if (this.isSpecified("Scope")) {
            sb.append(", Scope=").append(this.getScope());
        }
        if (this.isSpecified("Summary")) {
            sb.append(", Summary=").append(this.getSummary());
        }
        return sb.toString();
    }
}
