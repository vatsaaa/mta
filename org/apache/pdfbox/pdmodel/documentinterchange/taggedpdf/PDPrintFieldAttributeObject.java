// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.documentinterchange.taggedpdf;

import org.apache.pdfbox.cos.COSDictionary;

public class PDPrintFieldAttributeObject extends PDStandardAttributeObject
{
    public static final String OWNER_PRINT_FIELD = "PrintField";
    private static final String ROLE = "Role";
    private static final String CHECKED = "checked";
    private static final String DESC = "Desc";
    public static final String ROLE_RB = "rb";
    public static final String ROLE_CB = "cb";
    public static final String ROLE_PB = "pb";
    public static final String ROLE_TV = "tv";
    public static final String CHECKED_STATE_ON = "on";
    public static final String CHECKED_STATE_OFF = "off";
    public static final String CHECKED_STATE_NEUTRAL = "neutral";
    
    public PDPrintFieldAttributeObject() {
        this.setOwner("PrintField");
    }
    
    public PDPrintFieldAttributeObject(final COSDictionary dictionary) {
        super(dictionary);
    }
    
    public String getRole() {
        return this.getName("Role");
    }
    
    public void setRole(final String role) {
        this.setName("Role", role);
    }
    
    public String getCheckedState() {
        return this.getName("checked", "off");
    }
    
    public void setCheckedState(final String checkedState) {
        this.setName("checked", checkedState);
    }
    
    public String getAlternateName() {
        return this.getString("Desc");
    }
    
    public void setAlternateName(final String alternateName) {
        this.setString("Desc", alternateName);
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder().append(super.toString());
        if (this.isSpecified("Role")) {
            sb.append(", Role=").append(this.getRole());
        }
        if (this.isSpecified("checked")) {
            sb.append(", Checked=").append(this.getCheckedState());
        }
        if (this.isSpecified("Desc")) {
            sb.append(", Desc=").append(this.getAlternateName());
        }
        return sb.toString();
    }
}
