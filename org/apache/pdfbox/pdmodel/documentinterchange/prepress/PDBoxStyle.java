// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.documentinterchange.prepress;

import org.apache.pdfbox.pdmodel.graphics.PDLineDashPattern;
import org.apache.pdfbox.pdmodel.graphics.color.PDColorSpace;
import org.apache.pdfbox.pdmodel.graphics.color.PDDeviceRGB;
import org.apache.pdfbox.cos.COSInteger;
import org.apache.pdfbox.cos.COSArray;
import org.apache.pdfbox.pdmodel.graphics.color.PDColorState;
import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSDictionary;
import org.apache.pdfbox.pdmodel.common.COSObjectable;

public class PDBoxStyle implements COSObjectable
{
    public static final String GUIDELINE_STYLE_SOLID = "S";
    public static final String GUIDELINE_STYLE_DASHED = "D";
    private COSDictionary dictionary;
    
    public PDBoxStyle() {
        this.dictionary = new COSDictionary();
    }
    
    public PDBoxStyle(final COSDictionary dic) {
        this.dictionary = dic;
    }
    
    public COSBase getCOSObject() {
        return this.dictionary;
    }
    
    public COSDictionary getDictionary() {
        return this.dictionary;
    }
    
    public PDColorState getGuidelineColor() {
        COSArray colorValues = (COSArray)this.dictionary.getDictionaryObject("C");
        if (colorValues == null) {
            colorValues = new COSArray();
            colorValues.add(COSInteger.ZERO);
            colorValues.add(COSInteger.ZERO);
            colorValues.add(COSInteger.ZERO);
            this.dictionary.setItem("C", colorValues);
        }
        final PDColorState instance = new PDColorState(colorValues);
        instance.setColorSpace(PDDeviceRGB.INSTANCE);
        return instance;
    }
    
    public void setGuideLineColor(final PDColorState color) {
        COSArray values = null;
        if (color != null) {
            values = color.getCOSColorSpaceValue();
        }
        this.dictionary.setItem("C", values);
    }
    
    public float getGuidelineWidth() {
        return this.dictionary.getFloat("W", 1.0f);
    }
    
    public void setGuidelineWidth(final float width) {
        this.dictionary.setFloat("W", width);
    }
    
    public String getGuidelineStyle() {
        return this.dictionary.getNameAsString("S", "S");
    }
    
    public void setGuidelineStyle(final String style) {
        this.dictionary.setName("S", style);
    }
    
    public PDLineDashPattern getLineDashPattern() {
        PDLineDashPattern pattern = null;
        COSArray d = (COSArray)this.dictionary.getDictionaryObject("D");
        if (d == null) {
            d = new COSArray();
            d.add(COSInteger.THREE);
            this.dictionary.setItem("D", d);
        }
        final COSArray lineArray = new COSArray();
        lineArray.add(d);
        lineArray.add(COSInteger.ZERO);
        pattern = new PDLineDashPattern(lineArray);
        return pattern;
    }
    
    public void setLineDashPattern(final PDLineDashPattern pattern) {
        COSArray array = null;
        if (pattern != null) {
            array = pattern.getCOSDashPattern();
        }
        this.dictionary.setItem("D", array);
    }
}
