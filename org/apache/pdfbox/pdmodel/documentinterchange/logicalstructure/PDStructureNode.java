// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.documentinterchange.logicalstructure;

import org.apache.pdfbox.cos.COSObject;
import org.apache.pdfbox.cos.COSInteger;
import org.apache.pdfbox.pdmodel.common.COSArrayList;
import java.util.Iterator;
import org.apache.pdfbox.cos.COSArray;
import java.util.ArrayList;
import java.util.List;
import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.cos.COSDictionary;
import org.apache.pdfbox.pdmodel.common.COSObjectable;

public abstract class PDStructureNode implements COSObjectable
{
    private COSDictionary dictionary;
    
    public static PDStructureNode create(final COSDictionary node) {
        final String type = node.getNameAsString(COSName.TYPE);
        if ("StructTreeRoot".equals(type)) {
            return new PDStructureTreeRoot(node);
        }
        if (type == null || "StructElem".equals(type)) {
            return new PDStructureElement(node);
        }
        throw new IllegalArgumentException("Dictionary must not include a Type entry with a value that is neither StructTreeRoot nor StructElem.");
    }
    
    protected COSDictionary getCOSDictionary() {
        return this.dictionary;
    }
    
    protected PDStructureNode(final String type) {
        (this.dictionary = new COSDictionary()).setName(COSName.TYPE, type);
    }
    
    protected PDStructureNode(final COSDictionary dictionary) {
        this.dictionary = dictionary;
    }
    
    public COSBase getCOSObject() {
        return this.dictionary;
    }
    
    public String getType() {
        return this.getCOSDictionary().getNameAsString(COSName.TYPE);
    }
    
    public List<Object> getKids() {
        final List<Object> kidObjects = new ArrayList<Object>();
        final COSBase k = this.getCOSDictionary().getDictionaryObject(COSName.K);
        if (k instanceof COSArray) {
            for (final COSBase kid : (COSArray)k) {
                final Object kidObject = this.createObject(kid);
                if (kidObject != null) {
                    kidObjects.add(kidObject);
                }
            }
        }
        else {
            final Object kidObject2 = this.createObject(k);
            if (kidObject2 != null) {
                kidObjects.add(kidObject2);
            }
        }
        return kidObjects;
    }
    
    public void setKids(final List<Object> kids) {
        this.getCOSDictionary().setItem(COSName.K, COSArrayList.converterToCOSArray(kids));
    }
    
    public void appendKid(final PDStructureElement structureElement) {
        this.appendObjectableKid(structureElement);
        structureElement.setParent(this);
    }
    
    protected void appendObjectableKid(final COSObjectable objectable) {
        if (objectable == null) {
            return;
        }
        this.appendKid(objectable.getCOSObject());
    }
    
    protected void appendKid(final COSBase object) {
        if (object == null) {
            return;
        }
        final COSBase k = this.getCOSDictionary().getDictionaryObject(COSName.K);
        if (k == null) {
            this.getCOSDictionary().setItem(COSName.K, object);
        }
        else if (k instanceof COSArray) {
            final COSArray array = (COSArray)k;
            array.add(object);
        }
        else {
            final COSArray array = new COSArray();
            array.add(k);
            array.add(object);
            this.getCOSDictionary().setItem(COSName.K, array);
        }
    }
    
    public void insertBefore(final PDStructureElement newKid, final Object refKid) {
        this.insertObjectableBefore(newKid, refKid);
    }
    
    protected void insertObjectableBefore(final COSObjectable newKid, final Object refKid) {
        if (newKid == null) {
            return;
        }
        this.insertBefore(newKid.getCOSObject(), refKid);
    }
    
    protected void insertBefore(final COSBase newKid, final Object refKid) {
        if (newKid == null || refKid == null) {
            return;
        }
        final COSBase k = this.getCOSDictionary().getDictionaryObject(COSName.K);
        if (k == null) {
            return;
        }
        COSBase refKidBase = null;
        if (refKid instanceof COSObjectable) {
            refKidBase = ((COSObjectable)refKid).getCOSObject();
        }
        else if (refKid instanceof COSInteger) {
            refKidBase = (COSInteger)refKid;
        }
        if (k instanceof COSArray) {
            final COSArray array = (COSArray)k;
            final int refIndex = array.indexOfObject(refKidBase);
            array.add(refIndex, newKid.getCOSObject());
        }
        else {
            boolean onlyKid = k.equals(refKidBase);
            if (!onlyKid && k instanceof COSObject) {
                final COSBase kObj = ((COSObject)k).getObject();
                onlyKid = kObj.equals(refKidBase);
            }
            if (onlyKid) {
                final COSArray array2 = new COSArray();
                array2.add(newKid);
                array2.add(refKidBase);
                this.getCOSDictionary().setItem(COSName.K, array2);
            }
        }
    }
    
    public boolean removeKid(final PDStructureElement structureElement) {
        final boolean removed = this.removeObjectableKid(structureElement);
        if (removed) {
            structureElement.setParent(null);
        }
        return removed;
    }
    
    protected boolean removeObjectableKid(final COSObjectable objectable) {
        return objectable != null && this.removeKid(objectable.getCOSObject());
    }
    
    protected boolean removeKid(final COSBase object) {
        if (object == null) {
            return false;
        }
        final COSBase k = this.getCOSDictionary().getDictionaryObject(COSName.K);
        if (k == null) {
            return false;
        }
        if (k instanceof COSArray) {
            final COSArray array = (COSArray)k;
            final boolean removed = array.removeObject(object);
            if (array.size() == 1) {
                this.getCOSDictionary().setItem(COSName.K, array.getObject(0));
            }
            return removed;
        }
        boolean onlyKid = k.equals(object);
        if (!onlyKid && k instanceof COSObject) {
            final COSBase kObj = ((COSObject)k).getObject();
            onlyKid = kObj.equals(object);
        }
        if (onlyKid) {
            this.getCOSDictionary().setItem(COSName.K, null);
            return true;
        }
        return false;
    }
    
    protected Object createObject(final COSBase kid) {
        COSDictionary kidDic = null;
        if (kid instanceof COSDictionary) {
            kidDic = (COSDictionary)kid;
        }
        else if (kid instanceof COSObject) {
            final COSBase base = ((COSObject)kid).getObject();
            if (base instanceof COSDictionary) {
                kidDic = (COSDictionary)base;
            }
        }
        if (kidDic != null) {
            final String type = kidDic.getNameAsString(COSName.TYPE);
            if (type == null || "StructElem".equals(type)) {
                return new PDStructureElement(kidDic);
            }
            if ("OBJR".equals(type)) {
                return new PDObjectReference(kidDic);
            }
            if ("MCR".equals(type)) {
                return new PDMarkedContentReference(kidDic);
            }
        }
        else if (kid instanceof COSInteger) {
            final COSInteger mcid = (COSInteger)kid;
            return mcid.intValue();
        }
        return null;
    }
}
