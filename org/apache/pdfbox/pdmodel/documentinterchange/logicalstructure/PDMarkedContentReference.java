// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.documentinterchange.logicalstructure;

import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.cos.COSDictionary;
import org.apache.pdfbox.pdmodel.common.COSObjectable;

public class PDMarkedContentReference implements COSObjectable
{
    public static final String TYPE = "MCR";
    private COSDictionary dictionary;
    
    protected COSDictionary getCOSDictionary() {
        return this.dictionary;
    }
    
    public PDMarkedContentReference() {
        (this.dictionary = new COSDictionary()).setName(COSName.TYPE, "MCR");
    }
    
    public PDMarkedContentReference(final COSDictionary dictionary) {
        this.dictionary = dictionary;
    }
    
    public COSBase getCOSObject() {
        return this.dictionary;
    }
    
    public PDPage getPage() {
        final COSDictionary pg = (COSDictionary)this.getCOSDictionary().getDictionaryObject(COSName.PG);
        if (pg != null) {
            return new PDPage(pg);
        }
        return null;
    }
    
    public void setPage(final PDPage page) {
        this.getCOSDictionary().setItem(COSName.PG, page);
    }
    
    public int getMCID() {
        return this.getCOSDictionary().getInt(COSName.MCID);
    }
    
    public void setMCID(final int mcid) {
        this.getCOSDictionary().setInt(COSName.MCID, mcid);
    }
    
    @Override
    public String toString() {
        return "mcid=" + this.getMCID();
    }
}
