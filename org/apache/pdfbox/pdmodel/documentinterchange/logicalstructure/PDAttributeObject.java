// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.documentinterchange.logicalstructure;

import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.pdmodel.documentinterchange.taggedpdf.PDExportFormatAttributeObject;
import org.apache.pdfbox.pdmodel.documentinterchange.taggedpdf.PDLayoutAttributeObject;
import org.apache.pdfbox.pdmodel.documentinterchange.taggedpdf.PDTableAttributeObject;
import org.apache.pdfbox.pdmodel.documentinterchange.taggedpdf.PDPrintFieldAttributeObject;
import org.apache.pdfbox.pdmodel.documentinterchange.taggedpdf.PDListAttributeObject;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.cos.COSDictionary;
import org.apache.pdfbox.pdmodel.common.PDDictionaryWrapper;

public abstract class PDAttributeObject extends PDDictionaryWrapper
{
    private PDStructureElement structureElement;
    
    public static PDAttributeObject create(final COSDictionary dictionary) {
        final String owner = dictionary.getNameAsString(COSName.O);
        if ("UserProperties".equals(owner)) {
            return new PDUserAttributeObject(dictionary);
        }
        if ("List".equals(owner)) {
            return new PDListAttributeObject(dictionary);
        }
        if ("PrintField".equals(owner)) {
            return new PDPrintFieldAttributeObject(dictionary);
        }
        if ("Table".equals(owner)) {
            return new PDTableAttributeObject(dictionary);
        }
        if ("Layout".equals(owner)) {
            return new PDLayoutAttributeObject(dictionary);
        }
        if ("XML-1.00".equals(owner) || "HTML-3.2".equals(owner) || "HTML-4.01".equals(owner) || "OEB-1.00".equals(owner) || "RTF-1.05".equals(owner) || "CSS-1.00".equals(owner) || "CSS-2.00".equals(owner)) {
            return new PDExportFormatAttributeObject(dictionary);
        }
        return new PDDefaultAttributeObject(dictionary);
    }
    
    private PDStructureElement getStructureElement() {
        return this.structureElement;
    }
    
    protected void setStructureElement(final PDStructureElement structureElement) {
        this.structureElement = structureElement;
    }
    
    public PDAttributeObject() {
    }
    
    public PDAttributeObject(final COSDictionary dictionary) {
        super(dictionary);
    }
    
    public String getOwner() {
        return this.getCOSDictionary().getNameAsString(COSName.O);
    }
    
    protected void setOwner(final String owner) {
        this.getCOSDictionary().setName(COSName.O, owner);
    }
    
    public boolean isEmpty() {
        return this.getCOSDictionary().size() == 1 && this.getOwner() != null;
    }
    
    protected void potentiallyNotifyChanged(final COSBase oldBase, final COSBase newBase) {
        if (this.isValueChanged(oldBase, newBase)) {
            this.notifyChanged();
        }
    }
    
    private boolean isValueChanged(final COSBase oldValue, final COSBase newValue) {
        if (oldValue == null) {
            return newValue != null;
        }
        return !oldValue.equals(newValue);
    }
    
    protected void notifyChanged() {
        if (this.getStructureElement() != null) {
            this.getStructureElement().attributeChanged(this);
        }
    }
    
    @Override
    public String toString() {
        return "O=" + this.getOwner();
    }
    
    protected static String arrayToString(final Object[] array) {
        final StringBuilder sb = new StringBuilder("[");
        for (int i = 0; i < array.length; ++i) {
            if (i > 0) {
                sb.append(", ");
            }
            sb.append(array[i]);
        }
        return sb.append(']').toString();
    }
    
    protected static String arrayToString(final float[] array) {
        final StringBuilder sb = new StringBuilder("[");
        for (int i = 0; i < array.length; ++i) {
            if (i > 0) {
                sb.append(", ");
            }
            sb.append(array[i]);
        }
        return sb.append(']').toString();
    }
}
