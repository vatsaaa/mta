// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.documentinterchange.logicalstructure;

import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSDictionary;
import org.apache.pdfbox.pdmodel.common.COSObjectable;

public class PDMarkInfo implements COSObjectable
{
    private COSDictionary dictionary;
    
    public PDMarkInfo() {
        this.dictionary = new COSDictionary();
    }
    
    public PDMarkInfo(final COSDictionary dic) {
        this.dictionary = dic;
    }
    
    public COSBase getCOSObject() {
        return this.dictionary;
    }
    
    public COSDictionary getDictionary() {
        return this.dictionary;
    }
    
    public boolean isMarked() {
        return this.dictionary.getBoolean("Marked", false);
    }
    
    public void setMarked(final boolean value) {
        this.dictionary.setBoolean("Marked", value);
    }
    
    public boolean usesUserProperties() {
        return this.dictionary.getBoolean("UserProperties", false);
    }
    
    public void setUserProperties(final boolean userProps) {
        this.dictionary.setBoolean("UserProperties", userProps);
    }
    
    public boolean isSuspect() {
        return this.dictionary.getBoolean("Suspects", false);
    }
    
    public void setSuspect(final boolean suspect) {
        this.dictionary.setBoolean("Suspects", false);
    }
}
