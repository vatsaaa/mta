// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.documentinterchange.logicalstructure;

import java.io.IOException;
import org.apache.pdfbox.pdmodel.graphics.xobject.PDXObject;
import org.apache.pdfbox.pdmodel.interactive.annotation.PDAnnotation;
import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.cos.COSDictionary;
import org.apache.pdfbox.pdmodel.common.COSObjectable;

public class PDObjectReference implements COSObjectable
{
    public static final String TYPE = "OBJR";
    private COSDictionary dictionary;
    
    protected COSDictionary getCOSDictionary() {
        return this.dictionary;
    }
    
    public PDObjectReference() {
        (this.dictionary = new COSDictionary()).setName(COSName.TYPE, "OBJR");
    }
    
    public PDObjectReference(final COSDictionary dictionary) {
        this.dictionary = dictionary;
    }
    
    public COSBase getCOSObject() {
        return this.dictionary;
    }
    
    public COSObjectable getReferencedObject() {
        final COSBase obj = this.getCOSDictionary().getDictionaryObject(COSName.OBJ);
        try {
            return PDAnnotation.createAnnotation(obj);
        }
        catch (IOException e) {
            try {
                return PDXObject.createXObject(obj);
            }
            catch (IOException e2) {
                return null;
            }
        }
    }
    
    public void setReferencedObject(final PDAnnotation annotation) {
        this.getCOSDictionary().setItem(COSName.OBJ, annotation);
    }
    
    public void setReferencedObject(final PDXObject xobject) {
        this.getCOSDictionary().setItem(COSName.OBJ, xobject);
    }
}
