// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.documentinterchange.logicalstructure;

import java.util.Iterator;
import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSName;
import java.util.Map;
import java.util.ArrayList;
import java.util.List;
import org.apache.pdfbox.cos.COSDictionary;

public class PDDefaultAttributeObject extends PDAttributeObject
{
    public PDDefaultAttributeObject() {
    }
    
    public PDDefaultAttributeObject(final COSDictionary dictionary) {
        super(dictionary);
    }
    
    public List<String> getAttributeNames() {
        final List<String> attrNames = new ArrayList<String>();
        for (final Map.Entry<COSName, COSBase> entry : this.getCOSDictionary().entrySet()) {
            final COSName key = entry.getKey();
            if (!COSName.O.equals(key)) {
                attrNames.add(key.getName());
            }
        }
        return attrNames;
    }
    
    public COSBase getAttributeValue(final String attrName) {
        return this.getCOSDictionary().getDictionaryObject(attrName);
    }
    
    protected COSBase getAttributeValue(final String attrName, final COSBase defaultValue) {
        final COSBase value = this.getCOSDictionary().getDictionaryObject(attrName);
        if (value == null) {
            return defaultValue;
        }
        return value;
    }
    
    public void setAttribute(final String attrName, final COSBase attrValue) {
        final COSBase old = this.getAttributeValue(attrName);
        this.getCOSDictionary().setItem(COSName.getPDFName(attrName), attrValue);
        this.potentiallyNotifyChanged(old, attrValue);
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder().append(super.toString()).append(", attributes={");
        final Iterator<String> it = this.getAttributeNames().iterator();
        while (it.hasNext()) {
            final String name = it.next();
            sb.append(name).append('=').append(this.getAttributeValue(name));
            if (it.hasNext()) {
                sb.append(", ");
            }
        }
        return sb.append('}').toString();
    }
}
