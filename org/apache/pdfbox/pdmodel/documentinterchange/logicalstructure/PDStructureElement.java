// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.documentinterchange.logicalstructure;

import java.util.Map;
import org.apache.pdfbox.pdmodel.documentinterchange.markedcontent.PDMarkedContent;
import org.apache.pdfbox.cos.COSObject;
import java.util.Iterator;
import org.apache.pdfbox.cos.COSInteger;
import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSArray;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.common.COSObjectable;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.cos.COSDictionary;

public class PDStructureElement extends PDStructureNode
{
    public static final String TYPE = "StructElem";
    
    public PDStructureElement(final String structureType, final PDStructureNode parent) {
        super("StructElem");
        this.setStructureType(structureType);
        this.setParent(parent);
    }
    
    public PDStructureElement(final COSDictionary dic) {
        super(dic);
    }
    
    public String getStructureType() {
        return this.getCOSDictionary().getNameAsString(COSName.S);
    }
    
    public void setStructureType(final String structureType) {
        this.getCOSDictionary().setName(COSName.S, structureType);
    }
    
    public PDStructureNode getParent() {
        final COSDictionary p = (COSDictionary)this.getCOSDictionary().getDictionaryObject(COSName.P);
        if (p == null) {
            return null;
        }
        return PDStructureNode.create(p);
    }
    
    public void setParent(final PDStructureNode structureNode) {
        this.getCOSDictionary().setItem(COSName.P, structureNode);
    }
    
    public String getElementIdentifier() {
        return this.getCOSDictionary().getString(COSName.ID);
    }
    
    public void setElementIdentifier(final String id) {
        this.getCOSDictionary().setString(COSName.ID, id);
    }
    
    public PDPage getPage() {
        final COSDictionary pageDic = (COSDictionary)this.getCOSDictionary().getDictionaryObject(COSName.PG);
        if (pageDic == null) {
            return null;
        }
        return new PDPage(pageDic);
    }
    
    public void setPage(final PDPage page) {
        this.getCOSDictionary().setItem(COSName.PG, page);
    }
    
    public Revisions<PDAttributeObject> getAttributes() {
        final Revisions<PDAttributeObject> attributes = new Revisions<PDAttributeObject>();
        final COSBase a = this.getCOSDictionary().getDictionaryObject(COSName.A);
        if (a instanceof COSArray) {
            final COSArray aa = (COSArray)a;
            final Iterator<COSBase> it = aa.iterator();
            PDAttributeObject ao = null;
            while (it.hasNext()) {
                final COSBase item = it.next();
                if (item instanceof COSDictionary) {
                    ao = PDAttributeObject.create((COSDictionary)item);
                    ao.setStructureElement(this);
                    attributes.addObject(ao, 0);
                }
                else {
                    if (!(item instanceof COSInteger)) {
                        continue;
                    }
                    attributes.setRevisionNumber(ao, ((COSInteger)item).intValue());
                }
            }
        }
        if (a instanceof COSDictionary) {
            final PDAttributeObject ao2 = PDAttributeObject.create((COSDictionary)a);
            ao2.setStructureElement(this);
            attributes.addObject(ao2, 0);
        }
        return attributes;
    }
    
    public void setAttributes(final Revisions<PDAttributeObject> attributes) {
        final COSName key = COSName.A;
        if (attributes.size() == 1 && attributes.getRevisionNumber(0) == 0) {
            final PDAttributeObject attributeObject = attributes.getObject(0);
            attributeObject.setStructureElement(this);
            this.getCOSDictionary().setItem(key, attributeObject);
            return;
        }
        final COSArray array = new COSArray();
        for (int i = 0; i < attributes.size(); ++i) {
            final PDAttributeObject attributeObject2 = attributes.getObject(i);
            attributeObject2.setStructureElement(this);
            final int revisionNumber = attributes.getRevisionNumber(i);
            if (revisionNumber < 0) {}
            array.add(attributeObject2);
            array.add(COSInteger.get(revisionNumber));
        }
        this.getCOSDictionary().setItem(key, array);
    }
    
    public void addAttribute(final PDAttributeObject attributeObject) {
        final COSName key = COSName.A;
        attributeObject.setStructureElement(this);
        final COSBase a = this.getCOSDictionary().getDictionaryObject(key);
        COSArray array = null;
        if (a instanceof COSArray) {
            array = (COSArray)a;
        }
        else {
            array = new COSArray();
            if (a != null) {
                array.add(a);
                array.add(COSInteger.get(0L));
            }
        }
        this.getCOSDictionary().setItem(key, array);
        array.add(attributeObject);
        array.add(COSInteger.get(this.getRevisionNumber()));
    }
    
    public void removeAttribute(final PDAttributeObject attributeObject) {
        final COSName key = COSName.A;
        final COSBase a = this.getCOSDictionary().getDictionaryObject(key);
        if (a instanceof COSArray) {
            final COSArray array = (COSArray)a;
            array.remove(attributeObject.getCOSObject());
            if (array.size() == 2 && array.getInt(1) == 0) {
                this.getCOSDictionary().setItem(key, array.getObject(0));
            }
        }
        else {
            COSBase directA = a;
            if (a instanceof COSObject) {
                directA = ((COSObject)a).getObject();
            }
            if (attributeObject.getCOSObject().equals(directA)) {
                this.getCOSDictionary().setItem(key, null);
            }
        }
        attributeObject.setStructureElement(null);
    }
    
    public void attributeChanged(final PDAttributeObject attributeObject) {
        final COSName key = COSName.A;
        final COSBase a = this.getCOSDictionary().getDictionaryObject(key);
        if (a instanceof COSArray) {
            final COSArray array = (COSArray)a;
            for (int i = 0; i < array.size(); ++i) {
                final COSBase entry = array.getObject(i);
                if (entry.equals(attributeObject.getCOSObject())) {
                    final COSBase next = array.get(i + 1);
                    if (next instanceof COSInteger) {
                        array.set(i + 1, COSInteger.get(this.getRevisionNumber()));
                    }
                }
            }
        }
        else {
            final COSArray array = new COSArray();
            array.add(a);
            array.add(COSInteger.get(this.getRevisionNumber()));
            this.getCOSDictionary().setItem(key, array);
        }
    }
    
    public Revisions<String> getClassNames() {
        final COSName key = COSName.C;
        final Revisions<String> classNames = new Revisions<String>();
        final COSBase c = this.getCOSDictionary().getDictionaryObject(key);
        if (c instanceof COSName) {
            classNames.addObject(((COSName)c).getName(), 0);
        }
        if (c instanceof COSArray) {
            final COSArray array = (COSArray)c;
            final Iterator<COSBase> it = array.iterator();
            String className = null;
            while (it.hasNext()) {
                final COSBase item = it.next();
                if (item instanceof COSName) {
                    className = ((COSName)item).getName();
                    classNames.addObject(className, 0);
                }
                else {
                    if (!(item instanceof COSInteger)) {
                        continue;
                    }
                    classNames.setRevisionNumber(className, ((COSInteger)item).intValue());
                }
            }
        }
        return classNames;
    }
    
    public void setClassNames(final Revisions<String> classNames) {
        if (classNames == null) {
            return;
        }
        final COSName key = COSName.C;
        if (classNames.size() == 1 && classNames.getRevisionNumber(0) == 0) {
            final String className = classNames.getObject(0);
            this.getCOSDictionary().setName(key, className);
            return;
        }
        final COSArray array = new COSArray();
        for (int i = 0; i < classNames.size(); ++i) {
            final String className2 = classNames.getObject(i);
            final int revisionNumber = classNames.getRevisionNumber(i);
            if (revisionNumber < 0) {}
            array.add(COSName.getPDFName(className2));
            array.add(COSInteger.get(revisionNumber));
        }
        this.getCOSDictionary().setItem(key, array);
    }
    
    public void addClassName(final String className) {
        if (className == null) {
            return;
        }
        final COSName key = COSName.C;
        final COSBase c = this.getCOSDictionary().getDictionaryObject(key);
        COSArray array = null;
        if (c instanceof COSArray) {
            array = (COSArray)c;
        }
        else {
            array = new COSArray();
            if (c != null) {
                array.add(c);
                array.add(COSInteger.get(0L));
            }
        }
        this.getCOSDictionary().setItem(key, array);
        array.add(COSName.getPDFName(className));
        array.add(COSInteger.get(this.getRevisionNumber()));
    }
    
    public void removeClassName(final String className) {
        if (className == null) {
            return;
        }
        final COSName key = COSName.C;
        final COSBase c = this.getCOSDictionary().getDictionaryObject(key);
        final COSName name = COSName.getPDFName(className);
        if (c instanceof COSArray) {
            final COSArray array = (COSArray)c;
            array.remove(name);
            if (array.size() == 2 && array.getInt(1) == 0) {
                this.getCOSDictionary().setItem(key, array.getObject(0));
            }
        }
        else {
            COSBase directC = c;
            if (c instanceof COSObject) {
                directC = ((COSObject)c).getObject();
            }
            if (name.equals(directC)) {
                this.getCOSDictionary().setItem(key, null);
            }
        }
    }
    
    public int getRevisionNumber() {
        return this.getCOSDictionary().getInt(COSName.R, 0);
    }
    
    public void setRevisionNumber(final int revisionNumber) {
        if (revisionNumber < 0) {}
        this.getCOSDictionary().setInt(COSName.R, revisionNumber);
    }
    
    public void incrementRevisionNumber() {
        this.setRevisionNumber(this.getRevisionNumber() + 1);
    }
    
    public String getTitle() {
        return this.getCOSDictionary().getString(COSName.T);
    }
    
    public void setTitle(final String title) {
        this.getCOSDictionary().setString(COSName.T, title);
    }
    
    public String getLanguage() {
        return this.getCOSDictionary().getString(COSName.LANG);
    }
    
    public void setLanguage(final String language) {
        this.getCOSDictionary().setString(COSName.LANG, language);
    }
    
    public String getAlternateDescription() {
        return this.getCOSDictionary().getString(COSName.ALT);
    }
    
    public void setAlternateDescription(final String alternateDescription) {
        this.getCOSDictionary().setString(COSName.ALT, alternateDescription);
    }
    
    public String getExpandedForm() {
        return this.getCOSDictionary().getString(COSName.E);
    }
    
    public void setExpandedForm(final String expandedForm) {
        this.getCOSDictionary().setString(COSName.E, expandedForm);
    }
    
    public String getActualText() {
        return this.getCOSDictionary().getString(COSName.ACTUAL_TEXT);
    }
    
    public void setActualText(final String actualText) {
        this.getCOSDictionary().setString(COSName.ACTUAL_TEXT, actualText);
    }
    
    public String getStandardStructureType() {
        String type = this.getStructureType();
        while (true) {
            final String mappedType = this.getRoleMap().get(type);
            if (mappedType == null || type.equals(mappedType)) {
                break;
            }
            type = mappedType;
        }
        return type;
    }
    
    public void appendKid(final PDMarkedContent markedContent) {
        if (markedContent == null) {
            return;
        }
        this.appendKid(COSInteger.get(markedContent.getMCID()));
    }
    
    public void appendKid(final PDMarkedContentReference markedContentReference) {
        this.appendObjectableKid(markedContentReference);
    }
    
    public void appendKid(final PDObjectReference objectReference) {
        this.appendObjectableKid(objectReference);
    }
    
    public void insertBefore(final COSInteger markedContentIdentifier, final Object refKid) {
        this.insertBefore(markedContentIdentifier, refKid);
    }
    
    public void insertBefore(final PDMarkedContentReference markedContentReference, final Object refKid) {
        this.insertObjectableBefore(markedContentReference, refKid);
    }
    
    public void insertBefore(final PDObjectReference objectReference, final Object refKid) {
        this.insertObjectableBefore(objectReference, refKid);
    }
    
    public void removeKid(final COSInteger markedContentIdentifier) {
        this.removeKid(markedContentIdentifier);
    }
    
    public void removeKid(final PDMarkedContentReference markedContentReference) {
        this.removeObjectableKid(markedContentReference);
    }
    
    public void removeKid(final PDObjectReference objectReference) {
        this.removeObjectableKid(objectReference);
    }
    
    private PDStructureTreeRoot getStructureTreeRoot() {
        PDStructureNode parent;
        for (parent = this.getParent(); parent instanceof PDStructureElement; parent = ((PDStructureElement)parent).getParent()) {}
        if (parent instanceof PDStructureTreeRoot) {
            return (PDStructureTreeRoot)parent;
        }
        return null;
    }
    
    private Map<String, String> getRoleMap() {
        final PDStructureTreeRoot root = this.getStructureTreeRoot();
        if (root != null) {
            return root.getRoleMap();
        }
        return null;
    }
}
