// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.documentinterchange.logicalstructure;

import java.util.Iterator;
import org.apache.pdfbox.cos.COSBase;
import java.util.Hashtable;
import java.io.IOException;
import org.apache.pdfbox.pdmodel.common.COSDictionaryMap;
import java.util.Map;
import org.apache.pdfbox.pdmodel.common.COSObjectable;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.pdmodel.common.PDNameTreeNode;
import org.apache.pdfbox.cos.COSDictionary;

public class PDStructureTreeRoot extends PDStructureNode
{
    public static final String TYPE = "StructTreeRoot";
    
    public PDStructureTreeRoot() {
        super("StructTreeRoot");
    }
    
    public PDStructureTreeRoot(final COSDictionary dic) {
        super(dic);
    }
    
    public PDNameTreeNode getIDTree() {
        final COSDictionary idTreeDic = (COSDictionary)this.getCOSDictionary().getDictionaryObject(COSName.ID_TREE);
        if (idTreeDic != null) {
            return new PDNameTreeNode(idTreeDic, PDStructureElement.class);
        }
        return null;
    }
    
    public void setIDTree(final PDNameTreeNode idTree) {
        this.getCOSDictionary().setItem(COSName.ID_TREE, idTree);
    }
    
    public int getParentTreeNextKey() {
        return this.getCOSDictionary().getInt(COSName.PARENT_TREE_NEXT_KEY);
    }
    
    public Map<String, String> getRoleMap() {
        final COSBase rm = this.getCOSDictionary().getDictionaryObject(COSName.ROLE_MAP);
        if (rm instanceof COSDictionary) {
            try {
                return (Map<String, String>)COSDictionaryMap.convertBasicTypesToMap((COSDictionary)rm);
            }
            catch (IOException e) {
                e.printStackTrace();
            }
        }
        return new Hashtable<String, String>();
    }
    
    public void setRoleMap(final Map<String, String> roleMap) {
        final COSDictionary rmDic = new COSDictionary();
        for (final String key : roleMap.keySet()) {
            rmDic.setName(key, roleMap.get(key));
        }
        this.getCOSDictionary().setItem(COSName.ROLE_MAP, rmDic);
    }
}
