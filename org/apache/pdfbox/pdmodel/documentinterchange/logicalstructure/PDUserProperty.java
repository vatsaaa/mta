// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.documentinterchange.logicalstructure;

import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.cos.COSDictionary;
import org.apache.pdfbox.pdmodel.common.PDDictionaryWrapper;

public class PDUserProperty extends PDDictionaryWrapper
{
    private final PDUserAttributeObject userAttributeObject;
    
    public PDUserProperty(final PDUserAttributeObject userAttributeObject) {
        this.userAttributeObject = userAttributeObject;
    }
    
    public PDUserProperty(final COSDictionary dictionary, final PDUserAttributeObject userAttributeObject) {
        super(dictionary);
        this.userAttributeObject = userAttributeObject;
    }
    
    public String getName() {
        return this.getCOSDictionary().getNameAsString(COSName.N);
    }
    
    public void setName(final String name) {
        this.potentiallyNotifyChanged(this.getName(), name);
        this.getCOSDictionary().setName(COSName.N, name);
    }
    
    public COSBase getValue() {
        return this.getCOSDictionary().getDictionaryObject(COSName.V);
    }
    
    public void setValue(final COSBase value) {
        this.potentiallyNotifyChanged(this.getValue(), value);
        this.getCOSDictionary().setItem(COSName.V, value);
    }
    
    public String getFormattedValue() {
        return this.getCOSDictionary().getString(COSName.F);
    }
    
    public void setFormattedValue(final String formattedValue) {
        this.potentiallyNotifyChanged(this.getFormattedValue(), formattedValue);
        this.getCOSDictionary().setString(COSName.F, formattedValue);
    }
    
    public boolean isHidden() {
        return this.getCOSDictionary().getBoolean(COSName.H, false);
    }
    
    public void setHidden(final boolean hidden) {
        this.potentiallyNotifyChanged(this.isHidden(), hidden);
        this.getCOSDictionary().setBoolean(COSName.H, hidden);
    }
    
    @Override
    public String toString() {
        return "Name=" + this.getName() + ", Value=" + this.getValue() + ", FormattedValue=" + this.getFormattedValue() + ", Hidden=" + this.isHidden();
    }
    
    private void potentiallyNotifyChanged(final Object oldEntry, final Object newEntry) {
        if (this.isEntryChanged(oldEntry, newEntry)) {
            this.userAttributeObject.userPropertyChanged(this);
        }
    }
    
    private boolean isEntryChanged(final Object oldEntry, final Object newEntry) {
        if (oldEntry == null) {
            return newEntry != null;
        }
        return !oldEntry.equals(newEntry);
    }
}
