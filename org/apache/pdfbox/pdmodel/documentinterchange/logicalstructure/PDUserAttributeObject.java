// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.documentinterchange.logicalstructure;

import java.util.Iterator;
import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.pdmodel.common.COSObjectable;
import java.util.ArrayList;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.cos.COSArray;
import java.util.List;
import org.apache.pdfbox.cos.COSDictionary;

public class PDUserAttributeObject extends PDAttributeObject
{
    public static final String OWNER_USER_PROPERTIES = "UserProperties";
    
    public PDUserAttributeObject() {
        this.setOwner("UserProperties");
    }
    
    public PDUserAttributeObject(final COSDictionary dictionary) {
        super(dictionary);
    }
    
    public List<PDUserProperty> getOwnerUserProperties() {
        final COSArray p = (COSArray)this.getCOSDictionary().getDictionaryObject(COSName.P);
        final List<PDUserProperty> properties = new ArrayList<PDUserProperty>(p.size());
        for (int i = 0; i < p.size(); ++i) {
            properties.add(new PDUserProperty((COSDictionary)p.getObject(i), this));
        }
        return properties;
    }
    
    public void setUserProperties(final List<PDUserProperty> userProperties) {
        final COSArray p = new COSArray();
        for (final PDUserProperty userProperty : userProperties) {
            p.add(userProperty);
        }
        this.getCOSDictionary().setItem(COSName.P, p);
    }
    
    public void addUserProperty(final PDUserProperty userProperty) {
        final COSArray p = (COSArray)this.getCOSDictionary().getDictionaryObject(COSName.P);
        p.add(userProperty);
        this.notifyChanged();
    }
    
    public void removeUserProperty(final PDUserProperty userProperty) {
        if (userProperty == null) {
            return;
        }
        final COSArray p = (COSArray)this.getCOSDictionary().getDictionaryObject(COSName.P);
        p.remove(userProperty.getCOSObject());
        this.notifyChanged();
    }
    
    public void userPropertyChanged(final PDUserProperty userProperty) {
    }
    
    @Override
    public String toString() {
        return super.toString() + ", userProperties=" + this.getOwnerUserProperties();
    }
}
