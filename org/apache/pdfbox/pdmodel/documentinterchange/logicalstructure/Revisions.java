// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.documentinterchange.logicalstructure;

import java.util.ArrayList;
import java.util.List;

public class Revisions<T>
{
    private List<T> objects;
    private List<Integer> revisionNumbers;
    
    private List<T> getObjects() {
        if (this.objects == null) {
            this.objects = new ArrayList<T>();
        }
        return this.objects;
    }
    
    private List<Integer> getRevisionNumbers() {
        if (this.revisionNumbers == null) {
            this.revisionNumbers = new ArrayList<Integer>();
        }
        return this.revisionNumbers;
    }
    
    public T getObject(final int index) throws IndexOutOfBoundsException {
        return this.getObjects().get(index);
    }
    
    public int getRevisionNumber(final int index) throws IndexOutOfBoundsException {
        return this.getRevisionNumbers().get(index);
    }
    
    public void addObject(final T object, final int revisionNumber) {
        this.getObjects().add(object);
        this.getRevisionNumbers().add(revisionNumber);
    }
    
    protected void setRevisionNumber(final T object, final int revisionNumber) {
        final int index = this.getObjects().indexOf(object);
        if (index > -1) {
            this.getRevisionNumbers().set(index, revisionNumber);
        }
    }
    
    public int size() {
        return this.getObjects().size();
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        for (int i = 0; i < this.getObjects().size(); ++i) {
            if (i > 0) {
                sb.append("; ");
            }
            sb.append("object=").append(this.getObjects().get(i)).append(", revisionNumber=").append(this.getRevisionNumber(i));
        }
        return sb.toString();
    }
}
