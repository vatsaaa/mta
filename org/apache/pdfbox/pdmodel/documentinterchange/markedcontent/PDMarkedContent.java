// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.documentinterchange.markedcontent;

import org.apache.pdfbox.pdmodel.graphics.xobject.PDXObject;
import org.apache.pdfbox.util.TextPosition;
import java.util.ArrayList;
import org.apache.pdfbox.pdmodel.documentinterchange.taggedpdf.PDArtifactMarkedContent;
import org.apache.pdfbox.cos.COSName;
import java.util.List;
import org.apache.pdfbox.cos.COSDictionary;

public class PDMarkedContent
{
    private String tag;
    private COSDictionary properties;
    private List<Object> contents;
    
    public static PDMarkedContent create(final COSName tag, final COSDictionary properties) {
        if (COSName.ARTIFACT.equals(tag)) {
            new PDArtifactMarkedContent(properties);
        }
        return new PDMarkedContent(tag, properties);
    }
    
    public PDMarkedContent(final COSName tag, final COSDictionary properties) {
        this.tag = ((tag == null) ? null : tag.getName());
        this.properties = properties;
        this.contents = new ArrayList<Object>();
    }
    
    public String getTag() {
        return this.tag;
    }
    
    public COSDictionary getProperties() {
        return this.properties;
    }
    
    public int getMCID() {
        return (this.getProperties() == null) ? null : Integer.valueOf(this.getProperties().getInt(COSName.MCID));
    }
    
    public String getLanguage() {
        return (this.getProperties() == null) ? null : this.getProperties().getNameAsString(COSName.LANG);
    }
    
    public String getActualText() {
        return (this.getProperties() == null) ? null : this.getProperties().getString(COSName.ACTUAL_TEXT);
    }
    
    public String getAlternateDescription() {
        return (this.getProperties() == null) ? null : this.getProperties().getString(COSName.ALT);
    }
    
    public String getExpandedForm() {
        return (this.getProperties() == null) ? null : this.getProperties().getString(COSName.E);
    }
    
    public List<Object> getContents() {
        return this.contents;
    }
    
    public void addText(final TextPosition text) {
        this.getContents().add(text);
    }
    
    public void addMarkedContent(final PDMarkedContent markedContent) {
        this.getContents().add(markedContent);
    }
    
    public void addXObject(final PDXObject xobject) {
        this.getContents().add(xobject);
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("tag=").append(this.tag).append(", properties=").append(this.properties);
        sb.append(", contents=").append(this.contents);
        return sb.toString();
    }
}
