// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.common;

import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSName;

public class PDNamedTextStream implements DualCOSObjectable
{
    private COSName streamName;
    private PDTextStream stream;
    
    public PDNamedTextStream() {
    }
    
    public PDNamedTextStream(final COSName name, final COSBase str) {
        this.streamName = name;
        this.stream = PDTextStream.createTextStream(str);
    }
    
    public String getName() {
        String name = null;
        if (this.streamName != null) {
            name = this.streamName.getName();
        }
        return name;
    }
    
    public void setName(final String name) {
        this.streamName = COSName.getPDFName(name);
    }
    
    public PDTextStream getStream() {
        return this.stream;
    }
    
    public void setStream(final PDTextStream str) {
        this.stream = str;
    }
    
    public COSBase getFirstCOSObject() {
        return this.streamName;
    }
    
    public COSBase getSecondCOSObject() {
        COSBase retval = null;
        if (this.stream != null) {
            retval = this.stream.getCOSObject();
        }
        return retval;
    }
}
