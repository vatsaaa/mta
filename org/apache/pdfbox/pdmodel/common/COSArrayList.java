// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.common;

import java.util.ListIterator;
import org.apache.pdfbox.cos.COSNull;
import org.apache.pdfbox.cos.COSFloat;
import org.apache.pdfbox.cos.COSInteger;
import org.apache.pdfbox.cos.COSNumber;
import java.util.Collection;
import org.apache.pdfbox.cos.COSString;
import java.util.Iterator;
import org.apache.pdfbox.cos.COSBase;
import java.util.ArrayList;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.cos.COSDictionary;
import org.apache.pdfbox.cos.COSArray;
import java.util.List;

public class COSArrayList implements List
{
    private COSArray array;
    private List actual;
    private COSDictionary parentDict;
    private COSName dictKey;
    
    public COSArrayList() {
        this.array = new COSArray();
        this.actual = new ArrayList();
    }
    
    public COSArrayList(final List actualList, final COSArray cosArray) {
        this.actual = actualList;
        this.array = cosArray;
    }
    
    public COSArrayList(final Object actualObject, final COSBase item, final COSDictionary dictionary, final COSName dictionaryKey) {
        (this.array = new COSArray()).add(item);
        (this.actual = new ArrayList()).add(actualObject);
        this.parentDict = dictionary;
        this.dictKey = dictionaryKey;
    }
    
    @Deprecated
    public COSArrayList(final Object actualObject, final COSBase item, final COSDictionary dictionary, final String dictionaryKey) {
        this(actualObject, item, dictionary, COSName.getPDFName(dictionaryKey));
    }
    
    public int size() {
        return this.actual.size();
    }
    
    public boolean isEmpty() {
        return this.actual.isEmpty();
    }
    
    public boolean contains(final Object o) {
        return this.actual.contains(o);
    }
    
    public Iterator iterator() {
        return this.actual.iterator();
    }
    
    public Object[] toArray() {
        return this.actual.toArray();
    }
    
    public Object[] toArray(final Object[] a) {
        return this.actual.toArray(a);
    }
    
    public boolean add(final Object o) {
        if (this.parentDict != null) {
            this.parentDict.setItem(this.dictKey, this.array);
            this.parentDict = null;
        }
        if (o instanceof String) {
            this.array.add(new COSString((String)o));
        }
        else if (o instanceof DualCOSObjectable) {
            final DualCOSObjectable dual = (DualCOSObjectable)o;
            this.array.add(dual.getFirstCOSObject());
            this.array.add(dual.getSecondCOSObject());
        }
        else if (this.array != null) {
            this.array.add(((COSObjectable)o).getCOSObject());
        }
        return this.actual.add(o);
    }
    
    public boolean remove(final Object o) {
        boolean retval = true;
        final int index = this.actual.indexOf(o);
        if (index >= 0) {
            this.actual.remove(index);
            this.array.remove(index);
        }
        else {
            retval = false;
        }
        return retval;
    }
    
    public boolean containsAll(final Collection c) {
        return this.actual.containsAll(c);
    }
    
    public boolean addAll(final Collection c) {
        if (this.parentDict != null && c.size() > 0) {
            this.parentDict.setItem(this.dictKey, this.array);
            this.parentDict = null;
        }
        this.array.addAll(this.toCOSObjectList(c));
        return this.actual.addAll(c);
    }
    
    public boolean addAll(final int index, final Collection c) {
        if (this.parentDict != null && c.size() > 0) {
            this.parentDict.setItem(this.dictKey, this.array);
            this.parentDict = null;
        }
        if (c.size() > 0 && c.toArray()[0] instanceof DualCOSObjectable) {
            this.array.addAll(index * 2, this.toCOSObjectList(c));
        }
        else {
            this.array.addAll(index, this.toCOSObjectList(c));
        }
        return this.actual.addAll(index, c);
    }
    
    public static List convertIntegerCOSArrayToList(final COSArray intArray) {
        final List numbers = new ArrayList();
        for (int i = 0; i < intArray.size(); ++i) {
            numbers.add(new Integer(((COSNumber)intArray.get(i)).intValue()));
        }
        return new COSArrayList(numbers, intArray);
    }
    
    public static List convertFloatCOSArrayToList(final COSArray floatArray) {
        List retval = null;
        if (floatArray != null) {
            final List numbers = new ArrayList();
            for (int i = 0; i < floatArray.size(); ++i) {
                numbers.add(new Float(((COSNumber)floatArray.get(i)).floatValue()));
            }
            retval = new COSArrayList(numbers, floatArray);
        }
        return retval;
    }
    
    public static List convertCOSNameCOSArrayToList(final COSArray nameArray) {
        List retval = null;
        if (nameArray != null) {
            final List names = new ArrayList();
            for (int i = 0; i < nameArray.size(); ++i) {
                names.add(((COSName)nameArray.getObject(i)).getName());
            }
            retval = new COSArrayList(names, nameArray);
        }
        return retval;
    }
    
    public static List convertCOSStringCOSArrayToList(final COSArray stringArray) {
        List retval = null;
        if (stringArray != null) {
            final List string = new ArrayList();
            for (int i = 0; i < stringArray.size(); ++i) {
                string.add(((COSString)stringArray.getObject(i)).getString());
            }
            retval = new COSArrayList(string, stringArray);
        }
        return retval;
    }
    
    public static COSArray convertStringListToCOSNameCOSArray(final List strings) {
        final COSArray retval = new COSArray();
        for (int i = 0; i < strings.size(); ++i) {
            final Object next = strings.get(i);
            if (next instanceof COSName) {
                retval.add((COSBase)next);
            }
            else {
                retval.add(COSName.getPDFName((String)next));
            }
        }
        return retval;
    }
    
    public static COSArray convertStringListToCOSStringCOSArray(final List strings) {
        final COSArray retval = new COSArray();
        for (int i = 0; i < strings.size(); ++i) {
            retval.add(new COSString(strings.get(i)));
        }
        return retval;
    }
    
    public static COSArray converterToCOSArray(final List cosObjectableList) {
        COSArray array = null;
        if (cosObjectableList != null) {
            if (cosObjectableList instanceof COSArrayList) {
                array = ((COSArrayList)cosObjectableList).array;
            }
            else {
                array = new COSArray();
                for (final Object next : cosObjectableList) {
                    if (next instanceof String) {
                        array.add(new COSString((String)next));
                    }
                    else if (next instanceof Integer || next instanceof Long) {
                        array.add(COSInteger.get(((Number)next).longValue()));
                    }
                    else if (next instanceof Float || next instanceof Double) {
                        array.add(new COSFloat(((Number)next).floatValue()));
                    }
                    else if (next instanceof COSObjectable) {
                        final COSObjectable object = (COSObjectable)next;
                        array.add(object.getCOSObject());
                    }
                    else if (next instanceof DualCOSObjectable) {
                        final DualCOSObjectable object2 = (DualCOSObjectable)next;
                        array.add(object2.getFirstCOSObject());
                        array.add(object2.getSecondCOSObject());
                    }
                    else {
                        if (next != null) {
                            throw new RuntimeException("Error: Don't know how to convert type to COSBase '" + next.getClass().getName() + "'");
                        }
                        array.add(COSNull.NULL);
                    }
                }
            }
        }
        return array;
    }
    
    private List toCOSObjectList(final Collection list) {
        final List cosObjects = new ArrayList();
        for (final Object next : list) {
            if (next instanceof String) {
                cosObjects.add(new COSString((String)next));
            }
            else if (next instanceof DualCOSObjectable) {
                final DualCOSObjectable object = (DualCOSObjectable)next;
                this.array.add(object.getFirstCOSObject());
                this.array.add(object.getSecondCOSObject());
            }
            else {
                final COSObjectable cos = (COSObjectable)next;
                cosObjects.add(cos.getCOSObject());
            }
        }
        return cosObjects;
    }
    
    public boolean removeAll(final Collection c) {
        this.array.removeAll(this.toCOSObjectList(c));
        return this.actual.removeAll(c);
    }
    
    public boolean retainAll(final Collection c) {
        this.array.retainAll(this.toCOSObjectList(c));
        return this.actual.retainAll(c);
    }
    
    public void clear() {
        if (this.parentDict != null) {
            this.parentDict.setItem(this.dictKey, null);
        }
        this.actual.clear();
        this.array.clear();
    }
    
    @Override
    public boolean equals(final Object o) {
        return this.actual.equals(o);
    }
    
    @Override
    public int hashCode() {
        return this.actual.hashCode();
    }
    
    public Object get(final int index) {
        return this.actual.get(index);
    }
    
    public Object set(final int index, final Object element) {
        if (element instanceof String) {
            final COSString item = new COSString((String)element);
            if (this.parentDict != null && index == 0) {
                this.parentDict.setItem(this.dictKey, item);
            }
            this.array.set(index, item);
        }
        else if (element instanceof DualCOSObjectable) {
            final DualCOSObjectable dual = (DualCOSObjectable)element;
            this.array.set(index * 2, dual.getFirstCOSObject());
            this.array.set(index * 2 + 1, dual.getSecondCOSObject());
        }
        else {
            if (this.parentDict != null && index == 0) {
                this.parentDict.setItem(this.dictKey, ((COSObjectable)element).getCOSObject());
            }
            this.array.set(index, ((COSObjectable)element).getCOSObject());
        }
        return this.actual.set(index, element);
    }
    
    public void add(final int index, final Object element) {
        if (this.parentDict != null) {
            this.parentDict.setItem(this.dictKey, this.array);
            this.parentDict = null;
        }
        this.actual.add(index, element);
        if (element instanceof String) {
            this.array.add(index, new COSString((String)element));
        }
        else if (element instanceof DualCOSObjectable) {
            final DualCOSObjectable dual = (DualCOSObjectable)element;
            this.array.add(index * 2, dual.getFirstCOSObject());
            this.array.add(index * 2 + 1, dual.getSecondCOSObject());
        }
        else {
            this.array.add(index, ((COSObjectable)element).getCOSObject());
        }
    }
    
    public Object remove(final int index) {
        if (this.array.size() > index && this.array.get(index) instanceof DualCOSObjectable) {
            this.array.remove(index);
            this.array.remove(index);
        }
        else {
            this.array.remove(index);
        }
        return this.actual.remove(index);
    }
    
    public int indexOf(final Object o) {
        return this.actual.indexOf(o);
    }
    
    public int lastIndexOf(final Object o) {
        return this.actual.indexOf(o);
    }
    
    public ListIterator listIterator() {
        return this.actual.listIterator();
    }
    
    public ListIterator listIterator(final int index) {
        return this.actual.listIterator(index);
    }
    
    public List subList(final int fromIndex, final int toIndex) {
        return this.actual.subList(fromIndex, toIndex);
    }
    
    @Override
    public String toString() {
        return "COSArrayList{" + this.array.toString() + "}";
    }
    
    public COSArray toList() {
        return this.array;
    }
}
