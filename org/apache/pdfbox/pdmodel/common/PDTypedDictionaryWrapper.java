// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.common;

import org.apache.pdfbox.cos.COSDictionary;
import org.apache.pdfbox.cos.COSName;

public class PDTypedDictionaryWrapper extends PDDictionaryWrapper
{
    public PDTypedDictionaryWrapper(final String type) {
        this.getCOSDictionary().setName(COSName.TYPE, type);
    }
    
    public PDTypedDictionaryWrapper(final COSDictionary dictionary) {
        super(dictionary);
    }
    
    public String getType() {
        return this.getCOSDictionary().getNameAsString(COSName.TYPE);
    }
}
