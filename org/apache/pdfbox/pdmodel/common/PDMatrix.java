// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.common;

import org.apache.pdfbox.cos.COSNumber;
import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSFloat;
import org.apache.pdfbox.cos.COSArray;

public class PDMatrix implements COSObjectable
{
    private COSArray matrix;
    private int numberOfRowElements;
    
    public PDMatrix() {
        this.numberOfRowElements = 3;
        (this.matrix = new COSArray()).add(new COSFloat(1.0f));
        this.matrix.add(new COSFloat(0.0f));
        this.matrix.add(new COSFloat(0.0f));
        this.matrix.add(new COSFloat(0.0f));
        this.matrix.add(new COSFloat(1.0f));
        this.matrix.add(new COSFloat(0.0f));
        this.matrix.add(new COSFloat(0.0f));
        this.matrix.add(new COSFloat(0.0f));
        this.matrix.add(new COSFloat(1.0f));
    }
    
    public PDMatrix(final COSArray array) {
        this.numberOfRowElements = 3;
        if (array.size() == 6) {
            this.numberOfRowElements = 2;
        }
        this.matrix = array;
    }
    
    public COSArray getCOSArray() {
        return this.matrix;
    }
    
    public COSBase getCOSObject() {
        return this.matrix;
    }
    
    public float getValue(final int row, final int column) {
        return ((COSNumber)this.matrix.get(row * this.numberOfRowElements + column)).floatValue();
    }
    
    public void setValue(final int row, final int column, final float value) {
        this.matrix.set(row * this.numberOfRowElements + column, new COSFloat(value));
    }
}
