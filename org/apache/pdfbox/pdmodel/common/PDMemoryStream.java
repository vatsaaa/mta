// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.common;

import org.apache.pdfbox.pdmodel.common.filespecification.PDFileSpecification;
import org.apache.pdfbox.cos.COSStream;
import java.util.List;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.IOException;
import java.io.OutputStream;
import org.apache.pdfbox.cos.COSBase;

public class PDMemoryStream extends PDStream
{
    private byte[] data;
    
    public PDMemoryStream(final byte[] buffer) {
        this.data = buffer;
    }
    
    @Override
    public void addCompression() {
    }
    
    @Override
    public COSBase getCOSObject() {
        throw new UnsupportedOperationException("not supported for memory stream");
    }
    
    @Override
    public OutputStream createOutputStream() throws IOException {
        throw new UnsupportedOperationException("not supported for memory stream");
    }
    
    @Override
    public InputStream createInputStream() throws IOException {
        return new ByteArrayInputStream(this.data);
    }
    
    @Override
    public InputStream getPartiallyFilteredStream(final List stopFilters) throws IOException {
        return this.createInputStream();
    }
    
    @Override
    public COSStream getStream() {
        throw new UnsupportedOperationException("not supported for memory stream");
    }
    
    @Override
    public int getLength() {
        return this.data.length;
    }
    
    @Override
    public List getFilters() {
        return null;
    }
    
    @Override
    public void setFilters(final List filters) {
        throw new UnsupportedOperationException("not supported for memory stream");
    }
    
    public List getDecodeParams() throws IOException {
        return null;
    }
    
    public void setDecodeParams(final List decodeParams) {
    }
    
    @Override
    public PDFileSpecification getFile() {
        return null;
    }
    
    @Override
    public void setFile(final PDFileSpecification f) {
    }
    
    @Override
    public List getFileFilters() {
        return null;
    }
    
    @Override
    public void setFileFilters(final List filters) {
    }
    
    @Override
    public List getFileDecodeParams() throws IOException {
        return null;
    }
    
    @Override
    public void setFileDecodeParams(final List decodeParams) {
    }
    
    @Override
    public byte[] getByteArray() throws IOException {
        return this.data;
    }
    
    @Override
    public PDMetadata getMetadata() {
        return null;
    }
    
    @Override
    public void setMetadata(final PDMetadata meta) {
    }
}
