// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.common.filespecification;

import java.io.IOException;
import org.apache.pdfbox.cos.COSDictionary;
import org.apache.pdfbox.cos.COSString;
import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.pdmodel.common.COSObjectable;

public abstract class PDFileSpecification implements COSObjectable
{
    public static PDFileSpecification createFS(final COSBase base) throws IOException {
        PDFileSpecification retval = null;
        if (base != null) {
            if (base instanceof COSString) {
                retval = new PDSimpleFileSpecification((COSString)base);
            }
            else {
                if (!(base instanceof COSDictionary)) {
                    throw new IOException("Error: Unknown file specification " + base);
                }
                retval = new PDComplexFileSpecification((COSDictionary)base);
            }
        }
        return retval;
    }
    
    public abstract String getFile();
    
    public abstract void setFile(final String p0);
}
