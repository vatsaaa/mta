// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.common.filespecification;

import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSString;

public class PDSimpleFileSpecification extends PDFileSpecification
{
    private COSString file;
    
    public PDSimpleFileSpecification() {
        this.file = new COSString("");
    }
    
    public PDSimpleFileSpecification(final COSString fileName) {
        this.file = fileName;
    }
    
    @Override
    public String getFile() {
        return this.file.getString();
    }
    
    @Override
    public void setFile(final String fileName) {
        this.file = new COSString(fileName);
    }
    
    public COSBase getCOSObject() {
        return this.file;
    }
}
