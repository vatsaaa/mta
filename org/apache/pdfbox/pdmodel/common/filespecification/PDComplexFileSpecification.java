// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.common.filespecification;

import org.apache.pdfbox.pdmodel.common.COSObjectable;
import org.apache.pdfbox.cos.COSStream;
import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSDictionary;

public class PDComplexFileSpecification extends PDFileSpecification
{
    private COSDictionary fs;
    
    public PDComplexFileSpecification() {
        (this.fs = new COSDictionary()).setName("Type", "Filespec");
    }
    
    public PDComplexFileSpecification(final COSDictionary dict) {
        this.fs = dict;
    }
    
    public COSBase getCOSObject() {
        return this.fs;
    }
    
    public COSDictionary getCOSDictionary() {
        return this.fs;
    }
    
    @Override
    public String getFile() {
        return this.fs.getString("F");
    }
    
    @Override
    public void setFile(final String file) {
        this.fs.setString("F", file);
    }
    
    public String getFileDos() {
        return this.fs.getString("DOS");
    }
    
    public void setFileDos(final String file) {
        this.fs.setString("DOS", file);
    }
    
    public String getFileMac() {
        return this.fs.getString("Mac");
    }
    
    public void setFileMac(final String file) {
        this.fs.setString("Mac", file);
    }
    
    public String getFileUnix() {
        return this.fs.getString("Unix");
    }
    
    public void setFileUnix(final String file) {
        this.fs.setString("Unix", file);
    }
    
    public void setVolatile(final boolean fileIsVolatile) {
        this.fs.setBoolean("V", fileIsVolatile);
    }
    
    public boolean isVolatile() {
        return this.fs.getBoolean("V", false);
    }
    
    public PDEmbeddedFile getEmbeddedFile() {
        PDEmbeddedFile file = null;
        final COSStream stream = (COSStream)this.fs.getObjectFromPath("EF/F");
        if (stream != null) {
            file = new PDEmbeddedFile(stream);
        }
        return file;
    }
    
    public void setEmbeddedFile(final PDEmbeddedFile file) {
        COSDictionary ef = (COSDictionary)this.fs.getDictionaryObject("EF");
        if (ef == null && file != null) {
            ef = new COSDictionary();
            this.fs.setItem("EF", ef);
        }
        if (ef != null) {
            ef.setItem("F", file);
        }
    }
    
    public PDEmbeddedFile getEmbeddedFileDos() {
        PDEmbeddedFile file = null;
        final COSStream stream = (COSStream)this.fs.getObjectFromPath("EF/DOS");
        if (stream != null) {
            file = new PDEmbeddedFile(stream);
        }
        return file;
    }
    
    public void setEmbeddedFileDos(final PDEmbeddedFile file) {
        COSDictionary ef = (COSDictionary)this.fs.getDictionaryObject("DOS");
        if (ef == null && file != null) {
            ef = new COSDictionary();
            this.fs.setItem("EF", ef);
        }
        if (ef != null) {
            ef.setItem("DOS", file);
        }
    }
    
    public PDEmbeddedFile getEmbeddedFileMac() {
        PDEmbeddedFile file = null;
        final COSStream stream = (COSStream)this.fs.getObjectFromPath("EF/Mac");
        if (stream != null) {
            file = new PDEmbeddedFile(stream);
        }
        return file;
    }
    
    public void setEmbeddedFileMac(final PDEmbeddedFile file) {
        COSDictionary ef = (COSDictionary)this.fs.getDictionaryObject("Mac");
        if (ef == null && file != null) {
            ef = new COSDictionary();
            this.fs.setItem("EF", ef);
        }
        if (ef != null) {
            ef.setItem("Mac", file);
        }
    }
    
    public PDEmbeddedFile getEmbeddedFileUnix() {
        PDEmbeddedFile file = null;
        final COSStream stream = (COSStream)this.fs.getObjectFromPath("EF/Unix");
        if (stream != null) {
            file = new PDEmbeddedFile(stream);
        }
        return file;
    }
    
    public void setEmbeddedFileUnix(final PDEmbeddedFile file) {
        COSDictionary ef = (COSDictionary)this.fs.getDictionaryObject("Unix");
        if (ef == null && file != null) {
            ef = new COSDictionary();
            this.fs.setItem("EF", ef);
        }
        if (ef != null) {
            ef.setItem("Unix", file);
        }
    }
}
