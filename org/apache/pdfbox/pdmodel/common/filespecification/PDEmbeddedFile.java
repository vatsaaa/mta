// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.common.filespecification;

import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSDictionary;
import java.util.Calendar;
import java.io.IOException;
import java.io.InputStream;
import org.apache.pdfbox.cos.COSStream;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.common.PDStream;

public class PDEmbeddedFile extends PDStream
{
    public PDEmbeddedFile(final PDDocument document) {
        super(document);
        this.getStream().setName("Type", "EmbeddedFile");
    }
    
    public PDEmbeddedFile(final COSStream str) {
        super(str);
    }
    
    public PDEmbeddedFile(final PDDocument doc, final InputStream str) throws IOException {
        super(doc, str);
        this.getStream().setName("Type", "EmbeddedFile");
    }
    
    public PDEmbeddedFile(final PDDocument doc, final InputStream str, final boolean filtered) throws IOException {
        super(doc, str, filtered);
        this.getStream().setName("Type", "EmbeddedFile");
    }
    
    public void setSubtype(final String mimeType) {
        this.getStream().setName("Subtype", mimeType);
    }
    
    public String getSubtype() {
        return this.getStream().getNameAsString("Subtype");
    }
    
    public int getSize() {
        return this.getStream().getEmbeddedInt("Params", "Size");
    }
    
    public void setSize(final int size) {
        this.getStream().setEmbeddedInt("Params", "Size", size);
    }
    
    public Calendar getCreationDate() throws IOException {
        return this.getStream().getEmbeddedDate("Params", "CreationDate");
    }
    
    public void setCreationDate(final Calendar creation) {
        this.getStream().setEmbeddedDate("Params", "CreationDate", creation);
    }
    
    public Calendar getModDate() throws IOException {
        return this.getStream().getEmbeddedDate("Params", "ModDate");
    }
    
    public void setModDate(final Calendar mod) {
        this.getStream().setEmbeddedDate("Params", "ModDate", mod);
    }
    
    public String getCheckSum() {
        return this.getStream().getEmbeddedString("Params", "CheckSum");
    }
    
    public void setCheckSum(final String checksum) {
        this.getStream().setEmbeddedString("Params", "CheckSum", checksum);
    }
    
    public String getMacSubtype() {
        String retval = null;
        final COSDictionary params = (COSDictionary)this.getStream().getDictionaryObject("Params");
        if (params != null) {
            retval = params.getEmbeddedString("Mac", "Subtype");
        }
        return retval;
    }
    
    public void setMacSubtype(final String macSubtype) {
        COSDictionary params = (COSDictionary)this.getStream().getDictionaryObject("Params");
        if (params == null && macSubtype != null) {
            params = new COSDictionary();
            this.getStream().setItem("Params", params);
        }
        if (params != null) {
            params.setEmbeddedString("Mac", "Subtype", macSubtype);
        }
    }
    
    public String getMacCreator() {
        String retval = null;
        final COSDictionary params = (COSDictionary)this.getStream().getDictionaryObject("Params");
        if (params != null) {
            retval = params.getEmbeddedString("Mac", "Creator");
        }
        return retval;
    }
    
    public void setMacCreator(final String macCreator) {
        COSDictionary params = (COSDictionary)this.getStream().getDictionaryObject("Params");
        if (params == null && macCreator != null) {
            params = new COSDictionary();
            this.getStream().setItem("Params", params);
        }
        if (params != null) {
            params.setEmbeddedString("Mac", "Creator", macCreator);
        }
    }
    
    public String getMacResFork() {
        String retval = null;
        final COSDictionary params = (COSDictionary)this.getStream().getDictionaryObject("Params");
        if (params != null) {
            retval = params.getEmbeddedString("Mac", "ResFork");
        }
        return retval;
    }
    
    public void setMacResFork(final String macResFork) {
        COSDictionary params = (COSDictionary)this.getStream().getDictionaryObject("Params");
        if (params == null && macResFork != null) {
            params = new COSDictionary();
            this.getStream().setItem("Params", params);
        }
        if (params != null) {
            params.setEmbeddedString("Mac", "ResFork", macResFork);
        }
    }
}
