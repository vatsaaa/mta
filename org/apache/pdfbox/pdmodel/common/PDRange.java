// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.common;

import org.apache.pdfbox.cos.COSNumber;
import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSFloat;
import org.apache.pdfbox.cos.COSArray;

public class PDRange implements COSObjectable
{
    private COSArray rangeArray;
    private int startingIndex;
    
    public PDRange() {
        (this.rangeArray = new COSArray()).add(new COSFloat(0.0f));
        this.rangeArray.add(new COSFloat(1.0f));
        this.startingIndex = 0;
    }
    
    public PDRange(final COSArray range) {
        this.rangeArray = range;
    }
    
    public PDRange(final COSArray range, final int index) {
        this.rangeArray = range;
        this.startingIndex = index;
    }
    
    public COSBase getCOSObject() {
        return this.rangeArray;
    }
    
    public COSArray getCOSArray() {
        return this.rangeArray;
    }
    
    public float getMin() {
        final COSNumber min = (COSNumber)this.rangeArray.getObject(this.startingIndex * 2);
        return min.floatValue();
    }
    
    public void setMin(final float min) {
        this.rangeArray.set(this.startingIndex * 2, new COSFloat(min));
    }
    
    public float getMax() {
        final COSNumber max = (COSNumber)this.rangeArray.getObject(this.startingIndex * 2 + 1);
        return max.floatValue();
    }
    
    public void setMax(final float max) {
        this.rangeArray.set(this.startingIndex * 2 + 1, new COSFloat(max));
    }
}
