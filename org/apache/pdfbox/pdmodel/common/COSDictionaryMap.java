// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.common;

import org.apache.pdfbox.cos.COSBase;
import java.io.IOException;
import org.apache.pdfbox.cos.COSBoolean;
import org.apache.pdfbox.cos.COSFloat;
import org.apache.pdfbox.cos.COSInteger;
import org.apache.pdfbox.cos.COSString;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Collections;
import java.util.Collection;
import java.util.Set;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.cos.COSDictionary;
import java.util.Map;

public class COSDictionaryMap implements Map
{
    private COSDictionary map;
    private Map actuals;
    
    public COSDictionaryMap(final Map actualsMap, final COSDictionary dicMap) {
        this.actuals = actualsMap;
        this.map = dicMap;
    }
    
    public int size() {
        return this.map.size();
    }
    
    public boolean isEmpty() {
        return this.size() == 0;
    }
    
    public boolean containsKey(final Object key) {
        return this.map.keySet().contains(key);
    }
    
    public boolean containsValue(final Object value) {
        return this.actuals.containsValue(value);
    }
    
    public Object get(final Object key) {
        return this.actuals.get(key);
    }
    
    public Object put(final Object key, final Object value) {
        final COSObjectable object = (COSObjectable)value;
        this.map.setItem(COSName.getPDFName((String)key), object.getCOSObject());
        return this.actuals.put(key, value);
    }
    
    public Object remove(final Object key) {
        this.map.removeItem(COSName.getPDFName((String)key));
        return this.actuals.remove(key);
    }
    
    public void putAll(final Map t) {
        throw new RuntimeException("Not yet implemented");
    }
    
    public void clear() {
        this.map.clear();
        this.actuals.clear();
    }
    
    public Set keySet() {
        return this.actuals.keySet();
    }
    
    public Collection values() {
        return this.actuals.values();
    }
    
    public Set entrySet() {
        return Collections.unmodifiableSet((Set<?>)this.actuals.entrySet());
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean retval = false;
        if (o instanceof COSDictionaryMap) {
            final COSDictionaryMap other = (COSDictionaryMap)o;
            retval = other.map.equals(this.map);
        }
        return retval;
    }
    
    @Override
    public String toString() {
        return this.actuals.toString();
    }
    
    @Override
    public int hashCode() {
        return this.map.hashCode();
    }
    
    public static COSDictionary convert(final Map someMap) {
        final Iterator iter = someMap.keySet().iterator();
        final COSDictionary dic = new COSDictionary();
        while (iter.hasNext()) {
            final String name = iter.next();
            final COSObjectable object = someMap.get(name);
            dic.setItem(COSName.getPDFName(name), object.getCOSObject());
        }
        return dic;
    }
    
    public static COSDictionaryMap convertBasicTypesToMap(final COSDictionary map) throws IOException {
        COSDictionaryMap retval = null;
        if (map != null) {
            final Map actualMap = new HashMap();
            for (final COSName key : map.keySet()) {
                final COSBase cosObj = map.getDictionaryObject(key);
                Object actualObject = null;
                if (cosObj instanceof COSString) {
                    actualObject = ((COSString)cosObj).getString();
                }
                else if (cosObj instanceof COSInteger) {
                    actualObject = new Integer(((COSInteger)cosObj).intValue());
                }
                else if (cosObj instanceof COSName) {
                    actualObject = ((COSName)cosObj).getName();
                }
                else if (cosObj instanceof COSFloat) {
                    actualObject = new Float(((COSFloat)cosObj).floatValue());
                }
                else {
                    if (!(cosObj instanceof COSBoolean)) {
                        throw new IOException("Error:unknown type of object to convert:" + cosObj);
                    }
                    actualObject = (((COSBoolean)cosObj).getValue() ? Boolean.TRUE : Boolean.FALSE);
                }
                actualMap.put(key.getName(), actualObject);
            }
            retval = new COSDictionaryMap(actualMap, map);
        }
        return retval;
    }
}
