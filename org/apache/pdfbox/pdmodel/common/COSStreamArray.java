// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.common;

import java.io.OutputStream;
import org.apache.pdfbox.exceptions.COSVisitorException;
import org.apache.pdfbox.cos.ICOSVisitor;
import java.util.Enumeration;
import java.io.SequenceInputStream;
import java.io.ByteArrayInputStream;
import java.util.Vector;
import java.io.InputStream;
import java.io.IOException;
import java.util.ArrayList;
import org.apache.pdfbox.pdfparser.PDFStreamParser;
import java.util.List;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.io.RandomAccess;
import org.apache.pdfbox.cos.COSDictionary;
import org.apache.pdfbox.cos.COSArray;
import org.apache.pdfbox.cos.COSStream;

public class COSStreamArray extends COSStream
{
    private COSArray streams;
    private COSStream firstStream;
    
    public COSStreamArray(final COSArray array) {
        super(new COSDictionary(), null);
        this.streams = array;
        if (array.size() > 0) {
            this.firstStream = (COSStream)array.getObject(0);
        }
    }
    
    public COSBase get(final int index) {
        return this.streams.get(index);
    }
    
    public int getStreamCount() {
        return this.streams.size();
    }
    
    @Override
    public RandomAccess getScratchFile() {
        return this.firstStream.getScratchFile();
    }
    
    @Override
    public COSBase getItem(final COSName key) {
        return this.firstStream.getItem(key);
    }
    
    @Override
    public COSBase getDictionaryObject(final COSName key) {
        return this.firstStream.getDictionaryObject(key);
    }
    
    @Override
    public String toString() {
        return "COSStream{}";
    }
    
    @Override
    public List getStreamTokens() throws IOException {
        List retval = null;
        if (this.streams.size() > 0) {
            final PDFStreamParser parser = new PDFStreamParser(this);
            parser.parse();
            retval = parser.getTokens();
        }
        else {
            retval = new ArrayList();
        }
        return retval;
    }
    
    public COSDictionary getDictionary() {
        return this.firstStream;
    }
    
    @Override
    public InputStream getFilteredStream() throws IOException {
        throw new IOException("Error: Not allowed to get filtered stream from array of streams.");
    }
    
    @Override
    public InputStream getUnfilteredStream() throws IOException {
        final Vector<InputStream> inputStreams = new Vector<InputStream>();
        final byte[] inbetweenStreamBytes = "\n".getBytes("ISO-8859-1");
        for (int i = 0; i < this.streams.size(); ++i) {
            final COSStream stream = (COSStream)this.streams.getObject(i);
            inputStreams.add(stream.getUnfilteredStream());
            inputStreams.add(new ByteArrayInputStream(inbetweenStreamBytes));
        }
        return new SequenceInputStream(inputStreams.elements());
    }
    
    @Override
    public Object accept(final ICOSVisitor visitor) throws COSVisitorException {
        return this.streams.accept(visitor);
    }
    
    @Override
    public COSBase getFilters() {
        return this.firstStream.getFilters();
    }
    
    @Override
    public OutputStream createFilteredStream() throws IOException {
        return this.firstStream.createFilteredStream();
    }
    
    @Override
    public OutputStream createFilteredStream(final COSBase expectedLength) throws IOException {
        return this.firstStream.createFilteredStream(expectedLength);
    }
    
    @Override
    public void setFilters(final COSBase filters) throws IOException {
        this.firstStream.setFilters(filters);
    }
    
    @Override
    public OutputStream createUnfilteredStream() throws IOException {
        return this.firstStream.createUnfilteredStream();
    }
    
    public void appendStream(final COSStream streamToAppend) {
        this.streams.add(streamToAppend);
    }
    
    public void insertCOSStream(final PDStream streamToBeInserted) {
        final COSArray tmp = new COSArray();
        tmp.add(streamToBeInserted);
        tmp.addAll(this.streams);
        this.streams.clear();
        this.streams = tmp;
    }
}
