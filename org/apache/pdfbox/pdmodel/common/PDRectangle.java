// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.common;

import java.awt.Dimension;
import org.apache.pdfbox.cos.COSNumber;
import org.apache.fontbox.util.BoundingBox;
import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSFloat;
import org.apache.pdfbox.cos.COSArray;

public class PDRectangle implements COSObjectable
{
    private COSArray rectArray;
    
    public PDRectangle() {
        (this.rectArray = new COSArray()).add(new COSFloat(0.0f));
        this.rectArray.add(new COSFloat(0.0f));
        this.rectArray.add(new COSFloat(0.0f));
        this.rectArray.add(new COSFloat(0.0f));
    }
    
    public PDRectangle(final float width, final float height) {
        (this.rectArray = new COSArray()).add(new COSFloat(0.0f));
        this.rectArray.add(new COSFloat(0.0f));
        this.rectArray.add(new COSFloat(width));
        this.rectArray.add(new COSFloat(height));
    }
    
    public PDRectangle(final BoundingBox box) {
        (this.rectArray = new COSArray()).add(new COSFloat(box.getLowerLeftX()));
        this.rectArray.add(new COSFloat(box.getLowerLeftY()));
        this.rectArray.add(new COSFloat(box.getUpperRightX()));
        this.rectArray.add(new COSFloat(box.getUpperRightY()));
    }
    
    public PDRectangle(final COSArray array) {
        this.rectArray = array;
    }
    
    public boolean contains(final float x, final float y) {
        final float llx = this.getLowerLeftX();
        final float urx = this.getUpperRightX();
        final float lly = this.getLowerLeftY();
        final float ury = this.getUpperRightY();
        return x >= llx && x <= urx && y >= lly && y <= ury;
    }
    
    public PDRectangle createRetranslatedRectangle() {
        final PDRectangle retval = new PDRectangle();
        retval.setUpperRightX(this.getWidth());
        retval.setUpperRightY(this.getHeight());
        return retval;
    }
    
    public COSArray getCOSArray() {
        return this.rectArray;
    }
    
    public float getLowerLeftX() {
        return ((COSNumber)this.rectArray.get(0)).floatValue();
    }
    
    public void setLowerLeftX(final float value) {
        this.rectArray.set(0, new COSFloat(value));
    }
    
    public float getLowerLeftY() {
        return ((COSNumber)this.rectArray.get(1)).floatValue();
    }
    
    public void setLowerLeftY(final float value) {
        this.rectArray.set(1, new COSFloat(value));
    }
    
    public float getUpperRightX() {
        return ((COSNumber)this.rectArray.get(2)).floatValue();
    }
    
    public void setUpperRightX(final float value) {
        this.rectArray.set(2, new COSFloat(value));
    }
    
    public float getUpperRightY() {
        return ((COSNumber)this.rectArray.get(3)).floatValue();
    }
    
    public void setUpperRightY(final float value) {
        this.rectArray.set(3, new COSFloat(value));
    }
    
    public float getWidth() {
        return this.getUpperRightX() - this.getLowerLeftX();
    }
    
    public float getHeight() {
        return this.getUpperRightY() - this.getLowerLeftY();
    }
    
    public Dimension createDimension() {
        return new Dimension((int)this.getWidth(), (int)this.getHeight());
    }
    
    public void move(final float horizontalAmount, final float verticalAmount) {
        this.setUpperRightX(this.getUpperRightX() + horizontalAmount);
        this.setLowerLeftX(this.getLowerLeftX() + horizontalAmount);
        this.setUpperRightY(this.getUpperRightY() + verticalAmount);
        this.setLowerLeftY(this.getLowerLeftY() + verticalAmount);
    }
    
    public COSBase getCOSObject() {
        return this.rectArray;
    }
    
    @Override
    public String toString() {
        return "[" + this.getLowerLeftX() + "," + this.getLowerLeftY() + "," + this.getUpperRightX() + "," + this.getUpperRightY() + "]";
    }
}
