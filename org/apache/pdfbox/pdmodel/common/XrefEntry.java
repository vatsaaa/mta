// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.common;

public class XrefEntry
{
    private int objectNumber;
    private int byteOffset;
    private int generation;
    private boolean inUse;
    
    public XrefEntry() {
        this.objectNumber = 0;
        this.byteOffset = 0;
        this.generation = 0;
        this.inUse = true;
    }
    
    public XrefEntry(final int objectNumber, final int byteOffset, final int generation, final String inUse) {
        this.objectNumber = 0;
        this.byteOffset = 0;
        this.generation = 0;
        this.inUse = true;
        this.objectNumber = objectNumber;
        this.byteOffset = byteOffset;
        this.generation = generation;
        this.inUse = "n".equals(inUse);
    }
    
    public int getByteOffset() {
        return this.byteOffset;
    }
}
