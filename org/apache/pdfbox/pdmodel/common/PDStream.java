// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.common;

import org.apache.pdfbox.cos.COSNull;
import org.apache.pdfbox.pdmodel.common.filespecification.PDFileSpecification;
import java.util.Map;
import org.apache.pdfbox.filter.Filter;
import org.apache.pdfbox.filter.FilterManager;
import java.io.ByteArrayInputStream;
import org.apache.pdfbox.cos.COSDictionary;
import java.io.ByteArrayOutputStream;
import org.apache.pdfbox.cos.COSArray;
import org.apache.pdfbox.cos.COSBase;
import java.util.List;
import org.apache.pdfbox.cos.COSName;
import java.util.ArrayList;
import java.io.OutputStream;
import java.io.IOException;
import java.io.InputStream;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.cos.COSStream;

public class PDStream implements COSObjectable
{
    private COSStream stream;
    
    protected PDStream() {
    }
    
    public PDStream(final PDDocument document) {
        this.stream = new COSStream(document.getDocument().getScratchFile());
    }
    
    public PDStream(final COSStream str) {
        this.stream = str;
    }
    
    public PDStream(final PDDocument doc, final InputStream str) throws IOException {
        this(doc, str, false);
    }
    
    public PDStream(final PDDocument doc, final InputStream str, final boolean filtered) throws IOException {
        OutputStream output = null;
        try {
            this.stream = new COSStream(doc.getDocument().getScratchFile());
            if (filtered) {
                output = this.stream.createFilteredStream();
            }
            else {
                output = this.stream.createUnfilteredStream();
            }
            final byte[] buffer = new byte[1024];
            int amountRead = -1;
            while ((amountRead = str.read(buffer)) != -1) {
                output.write(buffer, 0, amountRead);
            }
        }
        finally {
            if (output != null) {
                output.close();
            }
            if (str != null) {
                str.close();
            }
        }
    }
    
    public void addCompression() {
        List filters = this.getFilters();
        if (filters == null) {
            filters = new ArrayList();
            filters.add(COSName.FLATE_DECODE);
            this.setFilters(filters);
        }
    }
    
    public static PDStream createFromCOS(final COSBase base) throws IOException {
        PDStream retval = null;
        if (base instanceof COSStream) {
            retval = new PDStream((COSStream)base);
        }
        else if (base instanceof COSArray) {
            if (((COSArray)base).size() > 0) {
                retval = new PDStream(new COSStreamArray((COSArray)base));
            }
        }
        else if (base != null) {
            throw new IOException("Contents are unknown type:" + base.getClass().getName());
        }
        return retval;
    }
    
    public COSBase getCOSObject() {
        return this.stream;
    }
    
    public OutputStream createOutputStream() throws IOException {
        return this.stream.createUnfilteredStream();
    }
    
    public InputStream createInputStream() throws IOException {
        return this.stream.getUnfilteredStream();
    }
    
    public InputStream getPartiallyFilteredStream(final List stopFilters) throws IOException {
        final FilterManager manager = this.stream.getFilterManager();
        InputStream is = this.stream.getFilteredStream();
        final ByteArrayOutputStream os = new ByteArrayOutputStream();
        final List filters = this.getFilters();
        String nextFilter = null;
        boolean done = false;
        for (int i = 0; i < filters.size() && !done; ++i) {
            os.reset();
            nextFilter = filters.get(i);
            if (stopFilters.contains(nextFilter)) {
                done = true;
            }
            else {
                final Filter filter = manager.getFilter(COSName.getPDFName(nextFilter));
                filter.decode(is, os, this.stream, i);
                is = new ByteArrayInputStream(os.toByteArray());
            }
        }
        return is;
    }
    
    public COSStream getStream() {
        return this.stream;
    }
    
    public int getLength() {
        return this.stream.getInt("Length", 0);
    }
    
    public List getFilters() {
        List retval = null;
        final COSBase filters = this.stream.getFilters();
        if (filters instanceof COSName) {
            final COSName name = (COSName)filters;
            retval = new COSArrayList(name.getName(), name, this.stream, COSName.FILTER);
        }
        else if (filters instanceof COSArray) {
            retval = COSArrayList.convertCOSNameCOSArrayToList((COSArray)filters);
        }
        return retval;
    }
    
    public void setFilters(final List filters) {
        final COSBase obj = COSArrayList.convertStringListToCOSNameCOSArray(filters);
        this.stream.setItem(COSName.FILTER, obj);
    }
    
    public List getDecodeParms() throws IOException {
        List retval = null;
        COSBase dp = this.stream.getDictionaryObject(COSName.DECODE_PARMS);
        if (dp == null) {
            dp = this.stream.getDictionaryObject(COSName.DP);
        }
        if (dp instanceof COSDictionary) {
            final Map map = COSDictionaryMap.convertBasicTypesToMap((COSDictionary)dp);
            retval = new COSArrayList(map, dp, this.stream, COSName.DECODE_PARMS);
        }
        else if (dp instanceof COSArray) {
            final COSArray array = (COSArray)dp;
            final List actuals = new ArrayList();
            for (int i = 0; i < array.size(); ++i) {
                actuals.add(COSDictionaryMap.convertBasicTypesToMap((COSDictionary)array.getObject(i)));
            }
            retval = new COSArrayList(actuals, array);
        }
        return retval;
    }
    
    public void setDecodeParms(final List decodeParams) {
        this.stream.setItem(COSName.DECODE_PARMS, COSArrayList.converterToCOSArray(decodeParams));
    }
    
    public PDFileSpecification getFile() throws IOException {
        final COSBase f = this.stream.getDictionaryObject(COSName.F);
        final PDFileSpecification retval = PDFileSpecification.createFS(f);
        return retval;
    }
    
    public void setFile(final PDFileSpecification f) {
        this.stream.setItem(COSName.F, f);
    }
    
    public List getFileFilters() {
        List retval = null;
        final COSBase filters = this.stream.getDictionaryObject(COSName.F_FILTER);
        if (filters instanceof COSName) {
            final COSName name = (COSName)filters;
            retval = new COSArrayList(name.getName(), name, this.stream, COSName.F_FILTER);
        }
        else if (filters instanceof COSArray) {
            retval = COSArrayList.convertCOSNameCOSArrayToList((COSArray)filters);
        }
        return retval;
    }
    
    public void setFileFilters(final List filters) {
        final COSBase obj = COSArrayList.convertStringListToCOSNameCOSArray(filters);
        this.stream.setItem(COSName.F_FILTER, obj);
    }
    
    public List getFileDecodeParams() throws IOException {
        List retval = null;
        final COSBase dp = this.stream.getDictionaryObject(COSName.F_DECODE_PARMS);
        if (dp instanceof COSDictionary) {
            final Map map = COSDictionaryMap.convertBasicTypesToMap((COSDictionary)dp);
            retval = new COSArrayList(map, dp, this.stream, COSName.F_DECODE_PARMS);
        }
        else if (dp instanceof COSArray) {
            final COSArray array = (COSArray)dp;
            final List actuals = new ArrayList();
            for (int i = 0; i < array.size(); ++i) {
                actuals.add(COSDictionaryMap.convertBasicTypesToMap((COSDictionary)array.getObject(i)));
            }
            retval = new COSArrayList(actuals, array);
        }
        return retval;
    }
    
    public void setFileDecodeParams(final List decodeParams) {
        this.stream.setItem("FDecodeParams", COSArrayList.converterToCOSArray(decodeParams));
    }
    
    public byte[] getByteArray() throws IOException {
        final ByteArrayOutputStream output = new ByteArrayOutputStream();
        final byte[] buf = new byte[1024];
        InputStream is = null;
        try {
            is = this.createInputStream();
            int amountRead = -1;
            while ((amountRead = is.read(buf)) != -1) {
                output.write(buf, 0, amountRead);
            }
        }
        finally {
            if (is != null) {
                is.close();
            }
        }
        return output.toByteArray();
    }
    
    public String getInputStreamAsString() throws IOException {
        final byte[] bStream = this.getByteArray();
        return new String(bStream, "ISO-8859-1");
    }
    
    public PDMetadata getMetadata() {
        PDMetadata retval = null;
        final COSBase mdStream = this.stream.getDictionaryObject(COSName.METADATA);
        if (mdStream != null) {
            if (mdStream instanceof COSStream) {
                retval = new PDMetadata((COSStream)mdStream);
            }
            else if (!(mdStream instanceof COSNull)) {
                throw new IllegalStateException("Expected a COSStream but was a " + mdStream.getClass().getSimpleName());
            }
        }
        return retval;
    }
    
    public void setMetadata(final PDMetadata meta) {
        this.stream.setItem(COSName.METADATA, meta);
    }
    
    public int getDecodedStreamLength() {
        return this.stream.getInt(COSName.DL);
    }
    
    public void setDecodedStreamLength(final int decodedStreamLength) {
        this.stream.setInt(COSName.DL, decodedStreamLength);
    }
}
