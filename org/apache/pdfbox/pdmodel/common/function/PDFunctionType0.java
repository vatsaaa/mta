// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.common.function;

import org.apache.commons.logging.LogFactory;
import org.apache.pdfbox.pdmodel.common.PDRange;
import org.apache.pdfbox.cos.COSInteger;
import java.io.IOException;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSArray;
import org.apache.commons.logging.Log;

public class PDFunctionType0 extends PDFunction
{
    private static final Log log;
    private COSArray encode;
    private COSArray decode;
    private COSArray size;
    private int[][] samples;
    
    public PDFunctionType0(final COSBase function) {
        super(function);
        this.encode = null;
        this.decode = null;
        this.size = null;
        this.samples = null;
    }
    
    @Override
    public int getFunctionType() {
        return 0;
    }
    
    public COSArray getSize() {
        if (this.size == null) {
            this.size = (COSArray)this.getDictionary().getDictionaryObject(COSName.SIZE);
        }
        return this.size;
    }
    
    public int[][] getSamples() {
        if (this.samples == null) {
            int arraySize = 1;
            final int numberOfInputValues = this.getNumberOfInputParameters();
            final int numberOfOutputValues = this.getNumberOfOutputParameters();
            final COSArray sizes = this.getSize();
            for (int i = 0; i < numberOfInputValues; ++i) {
                arraySize *= sizes.getInt(i);
            }
            this.samples = new int[arraySize][this.getNumberOfOutputParameters()];
            final int bitsPerSample = this.getBitsPerSample();
            int index = 0;
            int arrayIndex = 0;
            try {
                final byte[] samplesArray = this.getPDStream().getByteArray();
                for (int j = 0; j < numberOfInputValues; ++j) {
                    for (int sizeInputValues = sizes.getInt(j), k = 0; k < sizeInputValues; ++k) {
                        int bitsLeft = 0;
                        int bitsToRead = bitsPerSample;
                        int currentValue = 0;
                        for (int l = 0; l < numberOfOutputValues; ++l) {
                            if (bitsLeft == 0) {
                                currentValue = (samplesArray[arrayIndex++] + 256) % 256;
                                bitsLeft = 8;
                            }
                            int value = 0;
                            while (bitsToRead > 0) {
                                final int bits = Math.min(bitsToRead, bitsLeft);
                                value <<= bits;
                                final int valueToAdd = currentValue >> 8 - bits;
                                value |= valueToAdd;
                                bitsToRead -= bits;
                                bitsLeft -= bits;
                                if (bitsLeft == 0 && bitsToRead > 0) {
                                    currentValue = (samplesArray[arrayIndex++] + 256) % 256;
                                    bitsLeft = 8;
                                }
                            }
                            this.samples[index][l] = value;
                            bitsToRead = bitsPerSample;
                        }
                        ++index;
                    }
                }
            }
            catch (IOException exception) {
                PDFunctionType0.log.error("IOException while reading the sample values of this function.");
            }
        }
        return this.samples;
    }
    
    public int getBitsPerSample() {
        return this.getDictionary().getInt(COSName.BITS_PER_SAMPLE);
    }
    
    public void setBitsPerSample(final int bps) {
        this.getDictionary().setInt(COSName.BITS_PER_SAMPLE, bps);
    }
    
    private COSArray getEncodeValues() {
        if (this.encode == null) {
            this.encode = (COSArray)this.getDictionary().getDictionaryObject(COSName.ENCODE);
            if (this.encode == null) {
                this.encode = new COSArray();
                final COSArray sizeValues = this.getSize();
                for (int sizeValuesSize = sizeValues.size(), i = 0; i < sizeValuesSize; ++i) {
                    this.encode.add(COSInteger.ZERO);
                    this.encode.add(COSInteger.get(sizeValues.getInt(i) - 1));
                }
            }
        }
        return this.encode;
    }
    
    private COSArray getDecodeValues() {
        if (this.decode == null) {
            this.decode = (COSArray)this.getDictionary().getDictionaryObject(COSName.DECODE);
            if (this.decode == null) {
                this.decode = this.getRangeValues();
            }
        }
        return this.decode;
    }
    
    public PDRange getEncodeForParameter(final int paramNum) {
        PDRange retval = null;
        final COSArray encodeValues = this.getEncodeValues();
        if (encodeValues != null && encodeValues.size() >= paramNum * 2 + 1) {
            retval = new PDRange(encodeValues, paramNum);
        }
        return retval;
    }
    
    public void setEncodeValues(final COSArray encodeValues) {
        this.encode = encodeValues;
        this.getDictionary().setItem(COSName.ENCODE, encodeValues);
    }
    
    public PDRange getDecodeForParameter(final int paramNum) {
        PDRange retval = null;
        final COSArray decodeValues = this.getDecodeValues();
        if (decodeValues != null && decodeValues.size() >= paramNum * 2 + 1) {
            retval = new PDRange(decodeValues, paramNum);
        }
        return retval;
    }
    
    public void setDecodeValues(final COSArray decodeValues) {
        this.decode = decodeValues;
        this.getDictionary().setItem(COSName.DECODE, decodeValues);
    }
    
    @Override
    public float[] eval(final float[] input) throws IOException {
        final float[] sizeValues = this.getSize().toFloatArray();
        final int bitsPerSample = this.getBitsPerSample();
        final int numberOfInputValues = input.length;
        final int numberOfOutputValues = this.getNumberOfOutputParameters();
        final int[] intInputValuesPrevious = new int[numberOfInputValues];
        final int[] intInputValuesNext = new int[numberOfInputValues];
        for (int i = 0; i < numberOfInputValues; ++i) {
            final PDRange domain = this.getDomainForInput(i);
            final PDRange encode = this.getEncodeForParameter(i);
            input[i] = this.clipToRange(input[i], domain.getMin(), domain.getMax());
            input[i] = this.interpolate(input[i], domain.getMin(), domain.getMax(), encode.getMin(), encode.getMax());
            input[i] = this.clipToRange(input[i], 0.0f, sizeValues[i] - 1.0f);
            intInputValuesPrevious[i] = (int)Math.floor(input[i]);
            intInputValuesNext[i] = (int)Math.ceil(input[i]);
        }
        float[] outputValuesPrevious = null;
        float[] outputValuesNext = null;
        outputValuesPrevious = this.getSample(intInputValuesPrevious);
        outputValuesNext = this.getSample(intInputValuesNext);
        final float[] outputValues = new float[numberOfOutputValues];
        for (int j = 0; j < numberOfOutputValues; ++j) {
            final PDRange range = this.getRangeForOutput(j);
            final PDRange decode = this.getDecodeForParameter(j);
            outputValues[j] = (outputValuesPrevious[j] + outputValuesNext[j]) / 2.0f;
            outputValues[j] = this.interpolate(outputValues[j], 0.0f, (float)Math.pow(2.0, bitsPerSample), decode.getMin(), decode.getMax());
            outputValues[j] = this.clipToRange(outputValues[j], range.getMin(), range.getMax());
        }
        return outputValues;
    }
    
    private float[] getSample(final int[] inputValues) {
        final int[][] sampleValues = this.getSamples();
        final COSArray sizes = this.getSize();
        final int numberOfInputValues = this.getNumberOfInputParameters();
        int index = 0;
        int previousSize = 1;
        for (int i = 0; i < numberOfInputValues; ++i) {
            index += inputValues[i];
            previousSize *= sizes.getInt(i);
        }
        final int numberOfOutputValues = this.getNumberOfOutputParameters();
        final float[] result = new float[numberOfOutputValues];
        for (int j = 0; j < numberOfOutputValues; ++j) {
            result[j] = (float)sampleValues[index][j];
        }
        return result;
    }
    
    static {
        log = LogFactory.getLog(PDFunctionType0.class);
    }
}
