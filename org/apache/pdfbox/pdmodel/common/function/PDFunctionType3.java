// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.common.function;

import org.apache.pdfbox.cos.COSName;
import java.io.IOException;
import org.apache.pdfbox.pdmodel.common.PDRange;
import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSArray;

public class PDFunctionType3 extends PDFunction
{
    private COSArray functions;
    private COSArray encode;
    private COSArray bounds;
    
    public PDFunctionType3(final COSBase functionStream) {
        super(functionStream);
        this.functions = null;
        this.encode = null;
        this.bounds = null;
    }
    
    @Override
    public int getFunctionType() {
        return 3;
    }
    
    @Override
    public float[] eval(final float[] input) throws IOException {
        PDFunction function = null;
        float x = input[0];
        final PDRange domain = this.getDomainForInput(0);
        x = this.clipToRange(x, domain.getMin(), domain.getMax());
        final COSArray functionsArray = this.getFunctions();
        final int numberOfFunctions = functionsArray.size();
        if (numberOfFunctions == 1) {
            function = PDFunction.create(functionsArray.get(0));
            final PDRange encRange = this.getEncodeForParameter(0);
            x = this.interpolate(x, domain.getMin(), domain.getMax(), encRange.getMin(), encRange.getMax());
        }
        else {
            final float[] boundsValues = this.getBounds().toFloatArray();
            final int boundsSize = boundsValues.length;
            final float[] partitionValues = new float[boundsSize + 2];
            final int partitionValuesSize = partitionValues.length;
            partitionValues[0] = domain.getMin();
            partitionValues[partitionValuesSize - 1] = domain.getMax();
            System.arraycopy(boundsValues, 0, partitionValues, 1, boundsSize);
            for (int i = 0; i < partitionValuesSize - 1; ++i) {
                if (x >= partitionValues[i] && (x < partitionValues[i + 1] || (i == partitionValuesSize - 2 && x == partitionValues[i + 1]))) {
                    function = PDFunction.create(functionsArray.get(i));
                    final PDRange encRange2 = this.getEncodeForParameter(i);
                    x = this.interpolate(x, partitionValues[i], partitionValues[i + 1], encRange2.getMin(), encRange2.getMax());
                    break;
                }
            }
        }
        final float[] functionValues = { x };
        final float[] functionResult = function.eval(functionValues);
        return this.clipToRange(functionResult);
    }
    
    public COSArray getFunctions() {
        if (this.functions == null) {
            this.functions = (COSArray)this.getDictionary().getDictionaryObject(COSName.FUNCTIONS);
        }
        return this.functions;
    }
    
    public COSArray getBounds() {
        if (this.bounds == null) {
            this.bounds = (COSArray)this.getDictionary().getDictionaryObject(COSName.BOUNDS);
        }
        return this.bounds;
    }
    
    public COSArray getEncode() {
        if (this.encode == null) {
            this.encode = (COSArray)this.getDictionary().getDictionaryObject(COSName.ENCODE);
        }
        return this.encode;
    }
    
    private PDRange getEncodeForParameter(final int n) {
        final COSArray encodeValues = this.getEncode();
        return new PDRange(encodeValues, n);
    }
}
