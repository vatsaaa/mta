// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.common.function;

import org.apache.pdfbox.pdmodel.common.PDRange;
import java.io.IOException;
import org.apache.pdfbox.cos.COSObject;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.cos.COSStream;
import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSArray;
import org.apache.pdfbox.cos.COSDictionary;
import org.apache.pdfbox.pdmodel.common.PDStream;
import org.apache.pdfbox.pdmodel.common.COSObjectable;

public abstract class PDFunction implements COSObjectable
{
    private PDStream functionStream;
    private COSDictionary functionDictionary;
    private COSArray domain;
    private COSArray range;
    
    public PDFunction(final COSBase function) {
        this.functionStream = null;
        this.functionDictionary = null;
        this.domain = null;
        this.range = null;
        if (function instanceof COSStream) {
            this.functionStream = new PDStream((COSStream)function);
            this.functionStream.getStream().setName(COSName.TYPE, "Function");
        }
        else if (function instanceof COSDictionary) {
            this.functionDictionary = (COSDictionary)function;
        }
    }
    
    public abstract int getFunctionType();
    
    public COSBase getCOSObject() {
        if (this.functionStream != null) {
            return this.functionStream.getCOSObject();
        }
        return this.functionDictionary;
    }
    
    public COSDictionary getDictionary() {
        if (this.functionStream != null) {
            return this.functionStream.getStream();
        }
        return this.functionDictionary;
    }
    
    protected PDStream getPDStream() {
        return this.functionStream;
    }
    
    public static PDFunction create(COSBase function) throws IOException {
        PDFunction retval = null;
        if (function instanceof COSObject) {
            function = ((COSObject)function).getObject();
        }
        final COSDictionary functionDictionary = (COSDictionary)function;
        final int functionType = functionDictionary.getInt(COSName.FUNCTION_TYPE);
        if (functionType == 0) {
            retval = new PDFunctionType0(functionDictionary);
        }
        else if (functionType == 2) {
            retval = new PDFunctionType2(functionDictionary);
        }
        else if (functionType == 3) {
            retval = new PDFunctionType3(functionDictionary);
        }
        else {
            if (functionType != 4) {
                throw new IOException("Error: Unknown function type " + functionType);
            }
            retval = new PDFunctionType4(functionDictionary);
        }
        return retval;
    }
    
    public int getNumberOfOutputParameters() {
        final COSArray rangeValues = this.getRangeValues();
        return rangeValues.size() / 2;
    }
    
    public PDRange getRangeForOutput(final int n) {
        final COSArray rangeValues = this.getRangeValues();
        return new PDRange(rangeValues, n);
    }
    
    public void setRangeValues(final COSArray rangeValues) {
        this.range = rangeValues;
        this.getDictionary().setItem(COSName.RANGE, rangeValues);
    }
    
    public int getNumberOfInputParameters() {
        final COSArray array = this.getDomainValues();
        return array.size() / 2;
    }
    
    public PDRange getDomainForInput(final int n) {
        final COSArray domainValues = this.getDomainValues();
        return new PDRange(domainValues, n);
    }
    
    public void setDomainValues(final COSArray domainValues) {
        this.domain = domainValues;
        this.getDictionary().setItem(COSName.DOMAIN, domainValues);
    }
    
    public COSArray eval(final COSArray input) throws IOException {
        final float[] outputValues = this.eval(input.toFloatArray());
        final COSArray array = new COSArray();
        array.setFloatArray(outputValues);
        return array;
    }
    
    public abstract float[] eval(final float[] p0) throws IOException;
    
    protected COSArray getRangeValues() {
        if (this.range == null) {
            this.range = (COSArray)this.getDictionary().getDictionaryObject(COSName.RANGE);
        }
        return this.range;
    }
    
    private COSArray getDomainValues() {
        if (this.domain == null) {
            this.domain = (COSArray)this.getDictionary().getDictionaryObject(COSName.DOMAIN);
        }
        return this.domain;
    }
    
    protected float[] clipToRange(final float[] inputValues) {
        final COSArray rangesArray = this.getRangeValues();
        float[] result = null;
        if (rangesArray != null) {
            final float[] rangeValues = rangesArray.toFloatArray();
            final int numberOfRanges = rangeValues.length / 2;
            result = new float[numberOfRanges];
            for (int i = 0; i < numberOfRanges; ++i) {
                result[i] = this.clipToRange(inputValues[i], rangeValues[2 * i], rangeValues[2 * i + 1]);
            }
        }
        else {
            result = inputValues;
        }
        return result;
    }
    
    protected float clipToRange(final float x, final float rangeMin, final float rangeMax) {
        return Math.min(Math.max(x, rangeMin), rangeMax);
    }
    
    protected float interpolate(final float x, final float xRangeMin, final float xRangeMax, final float yRangeMin, final float yRangeMax) {
        return yRangeMin + (x - xRangeMin) * (yRangeMax - yRangeMin) / (xRangeMax - xRangeMin);
    }
}
