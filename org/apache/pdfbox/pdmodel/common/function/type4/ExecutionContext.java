// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.common.function.type4;

import java.util.Stack;

public class ExecutionContext
{
    private Operators operators;
    private Stack<Object> stack;
    
    public ExecutionContext(final Operators operatorSet) {
        this.stack = new Stack<Object>();
        this.operators = operatorSet;
    }
    
    public Stack<Object> getStack() {
        return this.stack;
    }
    
    public Operators getOperators() {
        return this.operators;
    }
    
    public Number popNumber() {
        return this.stack.pop();
    }
    
    public int popInt() {
        return this.stack.pop();
    }
    
    public float popReal() {
        return this.stack.pop().floatValue();
    }
}
