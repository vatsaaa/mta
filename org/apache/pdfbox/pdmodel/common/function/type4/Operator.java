// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.common.function.type4;

public interface Operator
{
    void execute(final ExecutionContext p0);
}
