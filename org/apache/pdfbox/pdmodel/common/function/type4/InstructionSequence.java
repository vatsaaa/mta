// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.common.function.type4;

import java.util.Iterator;
import java.util.Stack;
import java.util.ArrayList;
import java.util.List;

public class InstructionSequence
{
    private List<Object> instructions;
    
    public InstructionSequence() {
        this.instructions = new ArrayList<Object>();
    }
    
    public void addName(final String name) {
        this.instructions.add(name);
    }
    
    public void addInteger(final int value) {
        this.instructions.add(value);
    }
    
    public void addReal(final float value) {
        this.instructions.add(value);
    }
    
    public void addBoolean(final boolean value) {
        this.instructions.add(value);
    }
    
    public void addProc(final InstructionSequence child) {
        this.instructions.add(child);
    }
    
    public void execute(final ExecutionContext context) {
        final Stack<Object> stack = context.getStack();
        for (final Object o : this.instructions) {
            if (o instanceof String) {
                final String name = (String)o;
                final Operator cmd = context.getOperators().getOperator(name);
                if (cmd == null) {
                    throw new UnsupportedOperationException("Unknown operator or name: " + name);
                }
                cmd.execute(context);
            }
            else {
                stack.push(o);
            }
        }
        while (!stack.isEmpty() && stack.peek() instanceof InstructionSequence) {
            final InstructionSequence nested = stack.pop();
            nested.execute(context);
        }
    }
}
