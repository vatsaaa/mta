// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.common.function.type4;

import java.util.Stack;

class ArithmeticOperators
{
    static class Abs implements Operator
    {
        public void execute(final ExecutionContext context) {
            final Number num = context.popNumber();
            if (num instanceof Integer) {
                context.getStack().push(Math.abs(num.intValue()));
            }
            else {
                context.getStack().push(Math.abs(num.floatValue()));
            }
        }
    }
    
    static class Add implements Operator
    {
        public void execute(final ExecutionContext context) {
            final Number num2 = context.popNumber();
            final Number num3 = context.popNumber();
            if (num3 instanceof Integer && num2 instanceof Integer) {
                final long sum = num3.longValue() + num2.longValue();
                if (sum < -2147483648L || sum > 2147483647L) {
                    context.getStack().push(sum);
                }
                else {
                    context.getStack().push((int)sum);
                }
            }
            else {
                final float sum2 = num3.floatValue() + num2.floatValue();
                context.getStack().push(sum2);
            }
        }
    }
    
    static class Atan implements Operator
    {
        public void execute(final ExecutionContext context) {
            final float den = context.popReal();
            final float num = context.popReal();
            float atan = (float)Math.atan2(num, den);
            atan = (float)Math.toDegrees(atan) % 360.0f;
            if (atan < 0.0f) {
                atan += 360.0f;
            }
            context.getStack().push(atan);
        }
    }
    
    static class Ceiling implements Operator
    {
        public void execute(final ExecutionContext context) {
            final Number num = context.popNumber();
            if (num instanceof Integer) {
                context.getStack().push(num);
            }
            else {
                context.getStack().push((float)Math.ceil(num.doubleValue()));
            }
        }
    }
    
    static class Cos implements Operator
    {
        public void execute(final ExecutionContext context) {
            final float angle = context.popReal();
            final float cos = (float)Math.cos(Math.toRadians(angle));
            context.getStack().push(cos);
        }
    }
    
    static class Cvi implements Operator
    {
        public void execute(final ExecutionContext context) {
            final Number num = context.popNumber();
            context.getStack().push(num.intValue());
        }
    }
    
    static class Cvr implements Operator
    {
        public void execute(final ExecutionContext context) {
            final Number num = context.popNumber();
            context.getStack().push(num.floatValue());
        }
    }
    
    static class Div implements Operator
    {
        public void execute(final ExecutionContext context) {
            final Number num2 = context.popNumber();
            final Number num3 = context.popNumber();
            context.getStack().push(num3.floatValue() / num2.floatValue());
        }
    }
    
    static class Exp implements Operator
    {
        public void execute(final ExecutionContext context) {
            final Number exp = context.popNumber();
            final Number base = context.popNumber();
            final double value = Math.pow(base.doubleValue(), exp.doubleValue());
            context.getStack().push((float)value);
        }
    }
    
    static class Floor implements Operator
    {
        public void execute(final ExecutionContext context) {
            final Number num = context.popNumber();
            if (num instanceof Integer) {
                context.getStack().push(num);
            }
            else {
                context.getStack().push((float)Math.floor(num.doubleValue()));
            }
        }
    }
    
    static class IDiv implements Operator
    {
        public void execute(final ExecutionContext context) {
            final int num2 = context.popInt();
            final int num3 = context.popInt();
            context.getStack().push(num3 / num2);
        }
    }
    
    static class Ln implements Operator
    {
        public void execute(final ExecutionContext context) {
            final Number num = context.popNumber();
            context.getStack().push((float)Math.log(num.doubleValue()));
        }
    }
    
    static class Log implements Operator
    {
        public void execute(final ExecutionContext context) {
            final Number num = context.popNumber();
            context.getStack().push((float)Math.log10(num.doubleValue()));
        }
    }
    
    static class Mod implements Operator
    {
        public void execute(final ExecutionContext context) {
            final int int2 = context.popInt();
            final int int3 = context.popInt();
            context.getStack().push(int3 % int2);
        }
    }
    
    static class Mul implements Operator
    {
        public void execute(final ExecutionContext context) {
            final Number num2 = context.popNumber();
            final Number num3 = context.popNumber();
            if (num3 instanceof Integer && num2 instanceof Integer) {
                final long result = num3.longValue() * num2.longValue();
                if (result >= -2147483648L && result <= 2147483647L) {
                    context.getStack().push((int)result);
                }
                else {
                    context.getStack().push(result);
                }
            }
            else {
                final double result2 = num3.doubleValue() * num2.doubleValue();
                context.getStack().push((float)result2);
            }
        }
    }
    
    static class Neg implements Operator
    {
        public void execute(final ExecutionContext context) {
            final Number num = context.popNumber();
            if (num instanceof Integer) {
                final int v = num.intValue();
                if (v == Integer.MIN_VALUE) {
                    context.getStack().push(-num.floatValue());
                }
                else {
                    context.getStack().push(-num.intValue());
                }
            }
            else {
                context.getStack().push(-num.floatValue());
            }
        }
    }
    
    static class Round implements Operator
    {
        public void execute(final ExecutionContext context) {
            final Number num = context.popNumber();
            if (num instanceof Integer) {
                context.getStack().push(num.intValue());
            }
            else {
                context.getStack().push(Math.round(num.doubleValue()));
            }
        }
    }
    
    static class Sin implements Operator
    {
        public void execute(final ExecutionContext context) {
            final float angle = context.popReal();
            final float sin = (float)Math.sin(Math.toRadians(angle));
            context.getStack().push(sin);
        }
    }
    
    static class Sqrt implements Operator
    {
        public void execute(final ExecutionContext context) {
            final float num = context.popReal();
            if (num < 0.0f) {
                throw new IllegalArgumentException("argument must be nonnegative");
            }
            context.getStack().push((float)Math.sqrt(num));
        }
    }
    
    static class Sub implements Operator
    {
        public void execute(final ExecutionContext context) {
            final Stack<Object> stack = context.getStack();
            final Number num2 = context.popNumber();
            final Number num3 = context.popNumber();
            if (num3 instanceof Integer && num2 instanceof Integer) {
                final long result = num3.longValue() - num2.longValue();
                if (result < -2147483648L || result > 2147483647L) {
                    stack.push(result);
                }
                else {
                    stack.push((int)result);
                }
            }
            else {
                final float result2 = num3.floatValue() - num2.floatValue();
                stack.push(result2);
            }
        }
    }
    
    static class Truncate implements Operator
    {
        public void execute(final ExecutionContext context) {
            final Number num = context.popNumber();
            if (num instanceof Integer) {
                context.getStack().push(num.intValue());
            }
            else {
                context.getStack().push((int)num.floatValue());
            }
        }
    }
}
