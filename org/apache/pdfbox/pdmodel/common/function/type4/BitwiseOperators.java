// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.common.function.type4;

import java.util.Stack;

class BitwiseOperators
{
    private abstract static class AbstractLogicalOperator implements Operator
    {
        public void execute(final ExecutionContext context) {
            final Stack<Object> stack = context.getStack();
            final Object op2 = stack.pop();
            final Object op3 = stack.pop();
            if (op3 instanceof Boolean && op2 instanceof Boolean) {
                final boolean bool1 = (boolean)op3;
                final boolean bool2 = (boolean)op2;
                final boolean result = this.applyForBoolean(bool1, bool2);
                stack.push(result);
            }
            else {
                if (!(op3 instanceof Integer) || !(op2 instanceof Integer)) {
                    throw new ClassCastException("Operands must be bool/bool or int/int");
                }
                final int int1 = (int)op3;
                final int int2 = (int)op2;
                final int result2 = this.applyforInteger(int1, int2);
                stack.push(result2);
            }
        }
        
        protected abstract boolean applyForBoolean(final boolean p0, final boolean p1);
        
        protected abstract int applyforInteger(final int p0, final int p1);
    }
    
    static class And extends AbstractLogicalOperator
    {
        @Override
        protected boolean applyForBoolean(final boolean bool1, final boolean bool2) {
            return bool1 & bool2;
        }
        
        @Override
        protected int applyforInteger(final int int1, final int int2) {
            return int1 & int2;
        }
    }
    
    static class Bitshift implements Operator
    {
        public void execute(final ExecutionContext context) {
            final Stack<Object> stack = context.getStack();
            final int shift = stack.pop();
            final int int1 = stack.pop();
            if (shift < 0) {
                final int result = int1 >> Math.abs(shift);
                stack.push(result);
            }
            else {
                final int result = int1 << shift;
                stack.push(result);
            }
        }
    }
    
    static class False implements Operator
    {
        public void execute(final ExecutionContext context) {
            final Stack<Object> stack = context.getStack();
            stack.push(Boolean.FALSE);
        }
    }
    
    static class Not implements Operator
    {
        public void execute(final ExecutionContext context) {
            final Stack<Object> stack = context.getStack();
            final Object op1 = stack.pop();
            if (op1 instanceof Boolean) {
                final boolean bool1 = (boolean)op1;
                final boolean result = !bool1;
                stack.push(result);
            }
            else {
                if (!(op1 instanceof Integer)) {
                    throw new ClassCastException("Operand must be bool or int");
                }
                final int int1 = (int)op1;
                final int result2 = -int1;
                stack.push(result2);
            }
        }
    }
    
    static class Or extends AbstractLogicalOperator
    {
        @Override
        protected boolean applyForBoolean(final boolean bool1, final boolean bool2) {
            return bool1 | bool2;
        }
        
        @Override
        protected int applyforInteger(final int int1, final int int2) {
            return int1 | int2;
        }
    }
    
    static class True implements Operator
    {
        public void execute(final ExecutionContext context) {
            final Stack<Object> stack = context.getStack();
            stack.push(Boolean.TRUE);
        }
    }
    
    static class Xor extends AbstractLogicalOperator
    {
        @Override
        protected boolean applyForBoolean(final boolean bool1, final boolean bool2) {
            return bool1 ^ bool2;
        }
        
        @Override
        protected int applyforInteger(final int int1, final int int2) {
            return int1 ^ int2;
        }
    }
}
