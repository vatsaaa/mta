// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.common.function.type4;

import java.util.Stack;

class ConditionalOperators
{
    static class If implements Operator
    {
        public void execute(final ExecutionContext context) {
            final Stack<Object> stack = context.getStack();
            final InstructionSequence proc = stack.pop();
            final Boolean condition = stack.pop();
            if (condition) {
                proc.execute(context);
            }
        }
    }
    
    static class IfElse implements Operator
    {
        public void execute(final ExecutionContext context) {
            final Stack<Object> stack = context.getStack();
            final InstructionSequence proc2 = stack.pop();
            final InstructionSequence proc3 = stack.pop();
            final Boolean condition = stack.pop();
            if (condition) {
                proc3.execute(context);
            }
            else {
                proc2.execute(context);
            }
        }
    }
}
