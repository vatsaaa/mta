// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.common.function.type4;

import java.util.HashMap;
import java.util.Map;

public class Operators
{
    private static final Operator ABS;
    private static final Operator ADD;
    private static final Operator ATAN;
    private static final Operator CEILING;
    private static final Operator COS;
    private static final Operator CVI;
    private static final Operator CVR;
    private static final Operator DIV;
    private static final Operator EXP;
    private static final Operator FLOOR;
    private static final Operator IDIV;
    private static final Operator LN;
    private static final Operator LOG;
    private static final Operator MOD;
    private static final Operator MUL;
    private static final Operator NEG;
    private static final Operator ROUND;
    private static final Operator SIN;
    private static final Operator SQRT;
    private static final Operator SUB;
    private static final Operator TRUNCATE;
    private static final Operator AND;
    private static final Operator BITSHIFT;
    private static final Operator EQ;
    private static final Operator FALSE;
    private static final Operator GE;
    private static final Operator GT;
    private static final Operator LE;
    private static final Operator LT;
    private static final Operator NE;
    private static final Operator NOT;
    private static final Operator OR;
    private static final Operator TRUE;
    private static final Operator XOR;
    private static final Operator IF;
    private static final Operator IFELSE;
    private static final Operator COPY;
    private static final Operator DUP;
    private static final Operator EXCH;
    private static final Operator INDEX;
    private static final Operator POP;
    private static final Operator ROLL;
    private Map<String, Operator> operators;
    
    public Operators() {
        (this.operators = new HashMap<String, Operator>()).put("add", Operators.ADD);
        this.operators.put("abs", Operators.ABS);
        this.operators.put("atan", Operators.ATAN);
        this.operators.put("ceiling", Operators.CEILING);
        this.operators.put("cos", Operators.COS);
        this.operators.put("cvi", Operators.CVI);
        this.operators.put("cvr", Operators.CVR);
        this.operators.put("div", Operators.DIV);
        this.operators.put("exp", Operators.EXP);
        this.operators.put("floor", Operators.FLOOR);
        this.operators.put("idiv", Operators.IDIV);
        this.operators.put("ln", Operators.LN);
        this.operators.put("log", Operators.LOG);
        this.operators.put("mod", Operators.MOD);
        this.operators.put("mul", Operators.MUL);
        this.operators.put("neg", Operators.NEG);
        this.operators.put("round", Operators.ROUND);
        this.operators.put("sin", Operators.SIN);
        this.operators.put("sqrt", Operators.SQRT);
        this.operators.put("sub", Operators.SUB);
        this.operators.put("truncate", Operators.TRUNCATE);
        this.operators.put("and", Operators.AND);
        this.operators.put("bitshift", Operators.BITSHIFT);
        this.operators.put("eq", Operators.EQ);
        this.operators.put("false", Operators.FALSE);
        this.operators.put("ge", Operators.GE);
        this.operators.put("gt", Operators.GT);
        this.operators.put("le", Operators.LE);
        this.operators.put("lt", Operators.LT);
        this.operators.put("ne", Operators.NE);
        this.operators.put("not", Operators.NOT);
        this.operators.put("or", Operators.OR);
        this.operators.put("true", Operators.TRUE);
        this.operators.put("xor", Operators.XOR);
        this.operators.put("if", Operators.IF);
        this.operators.put("ifelse", Operators.IFELSE);
        this.operators.put("copy", Operators.COPY);
        this.operators.put("dup", Operators.DUP);
        this.operators.put("exch", Operators.EXCH);
        this.operators.put("index", Operators.INDEX);
        this.operators.put("pop", Operators.POP);
        this.operators.put("roll", Operators.ROLL);
    }
    
    public Operator getOperator(final String operatorName) {
        return this.operators.get(operatorName);
    }
    
    static {
        ABS = new ArithmeticOperators.Abs();
        ADD = new ArithmeticOperators.Add();
        ATAN = new ArithmeticOperators.Atan();
        CEILING = new ArithmeticOperators.Ceiling();
        COS = new ArithmeticOperators.Cos();
        CVI = new ArithmeticOperators.Cvi();
        CVR = new ArithmeticOperators.Cvr();
        DIV = new ArithmeticOperators.Div();
        EXP = new ArithmeticOperators.Exp();
        FLOOR = new ArithmeticOperators.Floor();
        IDIV = new ArithmeticOperators.IDiv();
        LN = new ArithmeticOperators.Ln();
        LOG = new ArithmeticOperators.Log();
        MOD = new ArithmeticOperators.Mod();
        MUL = new ArithmeticOperators.Mul();
        NEG = new ArithmeticOperators.Neg();
        ROUND = new ArithmeticOperators.Round();
        SIN = new ArithmeticOperators.Sin();
        SQRT = new ArithmeticOperators.Sqrt();
        SUB = new ArithmeticOperators.Sub();
        TRUNCATE = new ArithmeticOperators.Truncate();
        AND = new BitwiseOperators.And();
        BITSHIFT = new BitwiseOperators.Bitshift();
        EQ = new RelationalOperators.Eq();
        FALSE = new BitwiseOperators.False();
        GE = new RelationalOperators.Ge();
        GT = new RelationalOperators.Gt();
        LE = new RelationalOperators.Le();
        LT = new RelationalOperators.Lt();
        NE = new RelationalOperators.Ne();
        NOT = new BitwiseOperators.Not();
        OR = new BitwiseOperators.Or();
        TRUE = new BitwiseOperators.True();
        XOR = new BitwiseOperators.Xor();
        IF = new ConditionalOperators.If();
        IFELSE = new ConditionalOperators.IfElse();
        COPY = new StackOperators.Copy();
        DUP = new StackOperators.Dup();
        EXCH = new StackOperators.Exch();
        INDEX = new StackOperators.Index();
        POP = new StackOperators.Pop();
        ROLL = new StackOperators.Roll();
    }
}
