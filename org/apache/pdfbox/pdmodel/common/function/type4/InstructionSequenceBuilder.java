// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.common.function.type4;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.Stack;

public class InstructionSequenceBuilder extends Parser.AbstractSyntaxHandler
{
    private InstructionSequence mainSequence;
    private Stack<InstructionSequence> seqStack;
    private static final Pattern INTEGER_PATTERN;
    private static final Pattern REAL_PATTERN;
    
    private InstructionSequenceBuilder() {
        this.mainSequence = new InstructionSequence();
        (this.seqStack = new Stack<InstructionSequence>()).push(this.mainSequence);
    }
    
    public InstructionSequence getInstructionSequence() {
        return this.mainSequence;
    }
    
    public static InstructionSequence parse(final CharSequence text) {
        final InstructionSequenceBuilder builder = new InstructionSequenceBuilder();
        Parser.parse(text, builder);
        return builder.getInstructionSequence();
    }
    
    private InstructionSequence getCurrentSequence() {
        return this.seqStack.peek();
    }
    
    public void token(final CharSequence text) {
        final String token = text.toString();
        this.token(token);
    }
    
    private void token(final String token) {
        if ("{".equals(token)) {
            final InstructionSequence child = new InstructionSequence();
            this.getCurrentSequence().addProc(child);
            this.seqStack.push(child);
        }
        else if ("}".equals(token)) {
            this.seqStack.pop();
        }
        else {
            Matcher m = InstructionSequenceBuilder.INTEGER_PATTERN.matcher(token);
            if (m.matches()) {
                this.getCurrentSequence().addInteger(parseInt(token.toString()));
                return;
            }
            m = InstructionSequenceBuilder.REAL_PATTERN.matcher(token);
            if (m.matches()) {
                this.getCurrentSequence().addReal(parseReal(token));
                return;
            }
            this.getCurrentSequence().addName(token.toString());
        }
    }
    
    public static int parseInt(String token) {
        if (token.startsWith("+")) {
            token = token.substring(1);
        }
        return Integer.parseInt(token);
    }
    
    public static float parseReal(final String token) {
        return Float.parseFloat(token);
    }
    
    static {
        INTEGER_PATTERN = Pattern.compile("[\\+\\-]?\\d+");
        REAL_PATTERN = Pattern.compile("[\\-]?\\d*\\.\\d*([Ee]\\-?\\d+)?");
    }
}
