// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.common.function.type4;

import java.util.LinkedList;
import java.util.List;
import java.util.Stack;
import java.util.Collection;
import java.util.ArrayList;

class StackOperators
{
    static class Copy implements Operator
    {
        public void execute(final ExecutionContext context) {
            final Stack<Object> stack = context.getStack();
            final int n = stack.pop().intValue();
            if (n > 0) {
                final int size = stack.size();
                final List<Object> copy = new ArrayList<Object>(stack.subList(size - n, size));
                stack.addAll(copy);
            }
        }
    }
    
    static class Dup implements Operator
    {
        public void execute(final ExecutionContext context) {
            final Stack<Object> stack = context.getStack();
            stack.push(stack.peek());
        }
    }
    
    static class Exch implements Operator
    {
        public void execute(final ExecutionContext context) {
            final Stack<Object> stack = context.getStack();
            final Object any2 = stack.pop();
            final Object any3 = stack.pop();
            stack.push(any2);
            stack.push(any3);
        }
    }
    
    static class Index implements Operator
    {
        public void execute(final ExecutionContext context) {
            final Stack<Object> stack = context.getStack();
            final int n = stack.pop().intValue();
            if (n < 0) {
                throw new IllegalArgumentException("rangecheck: " + n);
            }
            final int size = stack.size();
            stack.push(stack.get(size - n - 1));
        }
    }
    
    static class Pop implements Operator
    {
        public void execute(final ExecutionContext context) {
            final Stack<Object> stack = context.getStack();
            stack.pop();
        }
    }
    
    static class Roll implements Operator
    {
        public void execute(final ExecutionContext context) {
            final Stack<Object> stack = context.getStack();
            final int j = stack.pop().intValue();
            final int n = stack.pop().intValue();
            if (j == 0) {
                return;
            }
            if (n < 0) {
                throw new IllegalArgumentException("rangecheck: " + n);
            }
            final LinkedList<Object> rolled = new LinkedList<Object>();
            final LinkedList<Object> moved = new LinkedList<Object>();
            if (j < 0) {
                for (int n2 = n + j, i = 0; i < n2; ++i) {
                    moved.addFirst(stack.pop());
                }
                for (int i = j; i < 0; ++i) {
                    rolled.addFirst(stack.pop());
                }
                stack.addAll(moved);
                stack.addAll(rolled);
            }
            else {
                final int n2 = n - j;
                for (int i = j; i > 0; --i) {
                    rolled.addFirst(stack.pop());
                }
                for (int i = 0; i < n2; ++i) {
                    moved.addFirst(stack.pop());
                }
                stack.addAll(rolled);
                stack.addAll(moved);
            }
        }
    }
}
