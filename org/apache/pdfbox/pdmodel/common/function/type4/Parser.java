// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.common.function.type4;

public class Parser
{
    private Parser() {
    }
    
    public static void parse(final CharSequence input, final SyntaxHandler handler) {
        final Tokenizer tokenizer = new Tokenizer(input, handler);
        tokenizer.tokenize();
    }
    
    private enum State
    {
        NEWLINE, 
        WHITESPACE, 
        COMMENT, 
        TOKEN;
    }
    
    public abstract static class AbstractSyntaxHandler implements SyntaxHandler
    {
        public void comment(final CharSequence text) {
        }
        
        public void newLine(final CharSequence text) {
        }
        
        public void whitespace(final CharSequence text) {
        }
    }
    
    private static class Tokenizer
    {
        private static final char NUL = '\0';
        private static final char EOT = '\u0004';
        private static final char TAB = '\t';
        private static final char FF = '\f';
        private static final char CR = '\r';
        private static final char LF = '\n';
        private static final char SPACE = ' ';
        private CharSequence input;
        private int index;
        private SyntaxHandler handler;
        private State state;
        private StringBuilder buffer;
        
        private Tokenizer(final CharSequence text, final SyntaxHandler syntaxHandler) {
            this.state = State.WHITESPACE;
            this.buffer = new StringBuilder();
            this.input = text;
            this.handler = syntaxHandler;
        }
        
        private boolean hasMore() {
            return this.index < this.input.length();
        }
        
        private char currentChar() {
            return this.input.charAt(this.index);
        }
        
        private char nextChar() {
            ++this.index;
            if (!this.hasMore()) {
                return '\u0004';
            }
            return this.currentChar();
        }
        
        private char peek() {
            if (this.index < this.input.length() - 1) {
                return this.input.charAt(this.index + 1);
            }
            return '\u0004';
        }
        
        private State nextState() {
            final char ch = this.currentChar();
            switch (ch) {
                case '\n':
                case '\f':
                case '\r': {
                    this.state = State.NEWLINE;
                    break;
                }
                case '\0':
                case '\t':
                case ' ': {
                    this.state = State.WHITESPACE;
                    break;
                }
                case '%': {
                    this.state = State.COMMENT;
                    break;
                }
                default: {
                    this.state = State.TOKEN;
                    break;
                }
            }
            return this.state;
        }
        
        private void tokenize() {
            while (this.hasMore()) {
                this.buffer.setLength(0);
                this.nextState();
                switch (this.state) {
                    case NEWLINE: {
                        this.scanNewLine();
                        continue;
                    }
                    case WHITESPACE: {
                        this.scanWhitespace();
                        continue;
                    }
                    case COMMENT: {
                        this.scanComment();
                        continue;
                    }
                    default: {
                        this.scanToken();
                        continue;
                    }
                }
            }
        }
        
        private void scanNewLine() {
            assert this.state == State.NEWLINE;
            final char ch = this.currentChar();
            this.buffer.append(ch);
            if (ch == '\r' && this.peek() == '\n') {
                this.buffer.append(this.nextChar());
            }
            this.handler.newLine(this.buffer);
            this.nextChar();
        }
        
        private void scanWhitespace() {
            assert this.state == State.WHITESPACE;
            this.buffer.append(this.currentChar());
        Label_0102:
            while (this.hasMore()) {
                final char ch = this.nextChar();
                switch (ch) {
                    case '\0':
                    case '\t':
                    case ' ': {
                        this.buffer.append(ch);
                        continue;
                    }
                    default: {
                        break Label_0102;
                    }
                }
            }
            this.handler.whitespace(this.buffer);
        }
        
        private void scanComment() {
            assert this.state == State.COMMENT;
            this.buffer.append(this.currentChar());
        Label_0095:
            while (this.hasMore()) {
                final char ch = this.nextChar();
                switch (ch) {
                    case '\n':
                    case '\f':
                    case '\r': {
                        break Label_0095;
                    }
                    default: {
                        this.buffer.append(ch);
                        continue;
                    }
                }
            }
            this.handler.comment(this.buffer);
        }
        
        private void scanToken() {
            assert this.state == State.TOKEN;
            char ch = this.currentChar();
            this.buffer.append(ch);
            switch (ch) {
                case '{':
                case '}': {
                    this.handler.token(this.buffer);
                    this.nextChar();
                }
                default: {
                Label_0195:
                    while (this.hasMore()) {
                        ch = this.nextChar();
                        switch (ch) {
                            case '\0':
                            case '\u0004':
                            case '\t':
                            case '\n':
                            case '\f':
                            case '\r':
                            case ' ':
                            case '{':
                            case '}': {
                                break Label_0195;
                            }
                            default: {
                                this.buffer.append(ch);
                                continue;
                            }
                        }
                    }
                    this.handler.token(this.buffer);
                }
            }
        }
    }
    
    public interface SyntaxHandler
    {
        void newLine(final CharSequence p0);
        
        void whitespace(final CharSequence p0);
        
        void token(final CharSequence p0);
        
        void comment(final CharSequence p0);
    }
}
