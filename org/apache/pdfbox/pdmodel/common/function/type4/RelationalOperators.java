// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.common.function.type4;

import java.util.Stack;

class RelationalOperators
{
    static class Eq implements Operator
    {
        public void execute(final ExecutionContext context) {
            final Stack<Object> stack = context.getStack();
            final Object op2 = stack.pop();
            final Object op3 = stack.pop();
            final boolean result = this.isEqual(op3, op2);
            stack.push(result);
        }
        
        protected boolean isEqual(final Object op1, final Object op2) {
            boolean result = false;
            if (op1 instanceof Number && op2 instanceof Number) {
                final Number num1 = (Number)op1;
                final Number num2 = (Number)op2;
                result = (num1.floatValue() == num2.floatValue());
            }
            else {
                result = op1.equals(op2);
            }
            return result;
        }
    }
    
    private abstract static class AbstractNumberComparisonOperator implements Operator
    {
        public void execute(final ExecutionContext context) {
            final Stack<Object> stack = context.getStack();
            final Object op2 = stack.pop();
            final Object op3 = stack.pop();
            final Number num1 = (Number)op3;
            final Number num2 = (Number)op2;
            final boolean result = this.compare(num1, num2);
            stack.push(result);
        }
        
        protected abstract boolean compare(final Number p0, final Number p1);
    }
    
    static class Ge extends AbstractNumberComparisonOperator
    {
        @Override
        protected boolean compare(final Number num1, final Number num2) {
            return num1.floatValue() >= num2.floatValue();
        }
    }
    
    static class Gt extends AbstractNumberComparisonOperator
    {
        @Override
        protected boolean compare(final Number num1, final Number num2) {
            return num1.floatValue() > num2.floatValue();
        }
    }
    
    static class Le extends AbstractNumberComparisonOperator
    {
        @Override
        protected boolean compare(final Number num1, final Number num2) {
            return num1.floatValue() <= num2.floatValue();
        }
    }
    
    static class Lt extends AbstractNumberComparisonOperator
    {
        @Override
        protected boolean compare(final Number num1, final Number num2) {
            return num1.floatValue() < num2.floatValue();
        }
    }
    
    static class Ne extends Eq
    {
        @Override
        protected boolean isEqual(final Object op1, final Object op2) {
            final boolean result = super.isEqual(op1, op2);
            return !result;
        }
    }
}
