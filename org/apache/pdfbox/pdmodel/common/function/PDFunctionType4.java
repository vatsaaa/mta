// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.common.function;

import org.apache.pdfbox.pdmodel.common.PDRange;
import org.apache.pdfbox.pdmodel.common.function.type4.ExecutionContext;
import java.io.IOException;
import org.apache.pdfbox.pdmodel.common.function.type4.InstructionSequenceBuilder;
import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.pdmodel.common.function.type4.InstructionSequence;
import org.apache.pdfbox.pdmodel.common.function.type4.Operators;

public class PDFunctionType4 extends PDFunction
{
    private static final Operators OPERATORS;
    private final InstructionSequence instructions;
    
    public PDFunctionType4(final COSBase functionStream) throws IOException {
        super(functionStream);
        this.instructions = InstructionSequenceBuilder.parse(this.getPDStream().getInputStreamAsString());
    }
    
    @Override
    public int getFunctionType() {
        return 4;
    }
    
    @Override
    public float[] eval(final float[] input) throws IOException {
        final int numberOfInputValues = input.length;
        final ExecutionContext context = new ExecutionContext(PDFunctionType4.OPERATORS);
        for (int i = numberOfInputValues - 1; i >= 0; --i) {
            final PDRange domain = this.getDomainForInput(i);
            final float value = this.clipToRange(input[i], domain.getMin(), domain.getMax());
            context.getStack().push(value);
        }
        this.instructions.execute(context);
        final int numberOfOutputValues = this.getNumberOfOutputParameters();
        final int numberOfActualOutputValues = context.getStack().size();
        if (numberOfActualOutputValues < numberOfOutputValues) {
            throw new IllegalStateException("The type 4 function returned " + numberOfActualOutputValues + " values but the Range entry indicates that " + numberOfOutputValues + " values be returned.");
        }
        final float[] outputValues = new float[numberOfOutputValues];
        for (int j = numberOfOutputValues - 1; j >= 0; --j) {
            final PDRange range = this.getRangeForOutput(j);
            outputValues[j] = context.popReal();
            outputValues[j] = this.clipToRange(outputValues[j], range.getMin(), range.getMax());
        }
        return outputValues;
    }
    
    static {
        OPERATORS = new Operators();
    }
}
