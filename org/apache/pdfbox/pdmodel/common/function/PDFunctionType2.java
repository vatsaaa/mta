// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.common.function;

import org.apache.pdfbox.cos.COSFloat;
import org.apache.pdfbox.cos.COSName;
import java.io.IOException;
import org.apache.pdfbox.cos.COSNumber;
import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSArray;

public class PDFunctionType2 extends PDFunction
{
    private COSArray C0;
    private COSArray C1;
    
    public PDFunctionType2(final COSBase function) {
        super(function);
    }
    
    @Override
    public int getFunctionType() {
        return 2;
    }
    
    @Override
    public float[] eval(final float[] input) throws IOException {
        final double inputValue = input[0];
        final double exponent = this.getN();
        final COSArray c0 = this.getC0();
        final COSArray c2 = this.getC1();
        final int c0Size = c0.size();
        final float[] functionResult = new float[c0Size];
        for (int j = 0; j < c0Size; ++j) {
            functionResult[j] = ((COSNumber)c0.get(j)).floatValue() + (float)Math.pow(inputValue, exponent) * (((COSNumber)c2.get(j)).floatValue() - ((COSNumber)c0.get(j)).floatValue());
        }
        return this.clipToRange(functionResult);
    }
    
    public COSArray getC0() {
        if (this.C0 == null) {
            this.C0 = (COSArray)this.getDictionary().getDictionaryObject(COSName.C0);
            if (this.C0 == null) {
                (this.C0 = new COSArray()).add(new COSFloat(0.0f));
            }
        }
        return this.C0;
    }
    
    public COSArray getC1() {
        if (this.C1 == null) {
            this.C1 = (COSArray)this.getDictionary().getDictionaryObject(COSName.C1);
            if (this.C1 == null) {
                (this.C1 = new COSArray()).add(new COSFloat(1.0f));
            }
        }
        return this.C1;
    }
    
    public float getN() {
        return this.getDictionary().getFloat(COSName.N);
    }
}
