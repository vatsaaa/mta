// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.common;

import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.cos.COSDictionary;

public class PDPageLabelRange implements COSObjectable
{
    private COSDictionary root;
    private static final COSName KEY_START;
    private static final COSName KEY_PREFIX;
    private static final COSName KEY_STYLE;
    public static final String STYLE_DECIMAL = "D";
    public static final String STYLE_ROMAN_UPPER = "R";
    public static final String STYLE_ROMAN_LOWER = "r";
    public static final String STYLE_LETTERS_UPPER = "A";
    public static final String STYLE_LETTERS_LOWER = "a";
    
    public PDPageLabelRange() {
        this(new COSDictionary());
    }
    
    public PDPageLabelRange(final COSDictionary dict) {
        this.root = dict;
    }
    
    public COSDictionary getCOSDictionary() {
        return this.root;
    }
    
    public COSBase getCOSObject() {
        return this.root;
    }
    
    public String getStyle() {
        return this.root.getNameAsString(PDPageLabelRange.KEY_STYLE);
    }
    
    public void setStyle(final String style) {
        if (style != null) {
            this.root.setName(PDPageLabelRange.KEY_STYLE, style);
        }
        else {
            this.root.removeItem(PDPageLabelRange.KEY_STYLE);
        }
    }
    
    public int getStart() {
        return this.root.getInt(PDPageLabelRange.KEY_START, 1);
    }
    
    public void setStart(final int start) {
        if (start <= 0) {
            throw new IllegalArgumentException("The page numbering start value must be a positive integer");
        }
        this.root.setInt(PDPageLabelRange.KEY_START, start);
    }
    
    public String getPrefix() {
        return this.root.getString(PDPageLabelRange.KEY_PREFIX);
    }
    
    public void setPrefix(final String prefix) {
        if (prefix != null) {
            this.root.setString(PDPageLabelRange.KEY_PREFIX, prefix);
        }
        else {
            this.root.removeItem(PDPageLabelRange.KEY_PREFIX);
        }
    }
    
    static {
        KEY_START = COSName.getPDFName("St");
        KEY_PREFIX = COSName.P;
        KEY_STYLE = COSName.getPDFName("S");
    }
}
