// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.common;

import java.util.Collection;
import java.lang.reflect.Constructor;
import java.util.Collections;
import org.apache.pdfbox.cos.COSInteger;
import java.util.HashMap;
import java.io.IOException;
import java.util.Map;
import java.util.ArrayList;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.cos.COSArray;
import java.util.List;
import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSDictionary;

public class PDNumberTreeNode implements COSObjectable
{
    private COSDictionary node;
    private Class<?> valueType;
    
    public PDNumberTreeNode(final Class<?> valueClass) {
        this.valueType = null;
        this.node = new COSDictionary();
        this.valueType = valueClass;
    }
    
    public PDNumberTreeNode(final COSDictionary dict, final Class<?> valueClass) {
        this.valueType = null;
        this.node = dict;
        this.valueType = valueClass;
    }
    
    public COSBase getCOSObject() {
        return this.node;
    }
    
    public COSDictionary getCOSDictionary() {
        return this.node;
    }
    
    public List getKids() {
        List retval = null;
        final COSArray kids = (COSArray)this.node.getDictionaryObject(COSName.KIDS);
        if (kids != null) {
            final List pdObjects = new ArrayList();
            for (int i = 0; i < kids.size(); ++i) {
                pdObjects.add(this.createChildNode((COSDictionary)kids.getObject(i)));
            }
            retval = new COSArrayList(pdObjects, kids);
        }
        return retval;
    }
    
    public void setKids(final List kids) {
        this.node.setItem(COSName.KIDS, COSArrayList.converterToCOSArray(kids));
    }
    
    public Object getValue(final Integer index) throws IOException {
        Object retval = null;
        final Map<Integer, Object> names = (Map<Integer, Object>)this.getNumbers();
        if (names != null) {
            retval = names.get(index);
        }
        else {
            final List kids = this.getKids();
            for (int i = 0; i < kids.size() && retval == null; ++i) {
                final PDNumberTreeNode childNode = kids.get(i);
                if (childNode.getLowerLimit().compareTo(index) <= 0 && childNode.getUpperLimit().compareTo(index) >= 0) {
                    retval = childNode.getValue(index);
                }
            }
        }
        return retval;
    }
    
    public Map getNumbers() throws IOException {
        Map<Integer, Object> indices = null;
        final COSArray namesArray = (COSArray)this.node.getDictionaryObject(COSName.NUMS);
        if (namesArray != null) {
            indices = new HashMap<Integer, Object>();
            for (int i = 0; i < namesArray.size(); i += 2) {
                final COSInteger key = (COSInteger)namesArray.getObject(i);
                final COSBase cosValue = namesArray.getObject(i + 1);
                final Object pdValue = this.convertCOSToPD(cosValue);
                indices.put(key.intValue(), pdValue);
            }
            indices = Collections.unmodifiableMap((Map<? extends Integer, ?>)indices);
        }
        return indices;
    }
    
    protected Object convertCOSToPD(final COSBase base) throws IOException {
        Object retval = null;
        try {
            final Constructor<?> ctor = this.valueType.getConstructor(base.getClass());
            retval = ctor.newInstance(base);
        }
        catch (Throwable t) {
            throw new IOException("Error while trying to create value in number tree:" + t.getMessage());
        }
        return retval;
    }
    
    protected PDNumberTreeNode createChildNode(final COSDictionary dic) {
        return new PDNumberTreeNode(dic, this.valueType);
    }
    
    public void setNumbers(final Map<Integer, Object> numbers) {
        if (numbers == null) {
            this.node.setItem(COSName.NUMS, (COSObjectable)null);
            this.node.setItem(COSName.LIMITS, (COSObjectable)null);
        }
        else {
            final List<Integer> keys = new ArrayList<Integer>(numbers.keySet());
            Collections.sort(keys);
            final COSArray array = new COSArray();
            for (int i = 0; i < keys.size(); ++i) {
                final Integer key = keys.get(i);
                array.add(COSInteger.get(key));
                final COSObjectable obj = numbers.get(key);
                array.add(obj);
            }
            Integer lower = null;
            Integer upper = null;
            if (keys.size() > 0) {
                lower = keys.get(0);
                upper = keys.get(keys.size() - 1);
            }
            this.setUpperLimit(upper);
            this.setLowerLimit(lower);
            this.node.setItem(COSName.NUMS, array);
        }
    }
    
    public Integer getUpperLimit() {
        Integer retval = null;
        final COSArray arr = (COSArray)this.node.getDictionaryObject(COSName.LIMITS);
        if (arr != null) {
            retval = arr.getInt(1);
        }
        return retval;
    }
    
    private void setUpperLimit(final Integer upper) {
        COSArray arr = (COSArray)this.node.getDictionaryObject(COSName.LIMITS);
        if (arr == null) {
            arr = new COSArray();
            arr.add(null);
            arr.add(null);
        }
        arr.setInt(1, upper);
    }
    
    public Integer getLowerLimit() {
        Integer retval = null;
        final COSArray arr = (COSArray)this.node.getDictionaryObject(COSName.LIMITS);
        if (arr != null) {
            retval = arr.getInt(0);
        }
        return retval;
    }
    
    private void setLowerLimit(final Integer lower) {
        COSArray arr = (COSArray)this.node.getDictionaryObject(COSName.LIMITS);
        if (arr == null) {
            arr = new COSArray();
            arr.add(null);
            arr.add(null);
        }
        arr.setInt(0, lower);
    }
}
