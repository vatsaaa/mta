// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.common;

import org.apache.commons.logging.LogFactory;
import java.util.Iterator;
import java.util.Collection;
import java.lang.reflect.Constructor;
import java.util.Collections;
import org.apache.pdfbox.cos.COSString;
import java.util.LinkedHashMap;
import java.io.IOException;
import java.util.Map;
import java.util.ArrayList;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.cos.COSArray;
import java.util.List;
import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSDictionary;
import org.apache.commons.logging.Log;

public class PDNameTreeNode implements COSObjectable
{
    private static final Log log;
    private COSDictionary node;
    private Class valueType;
    
    public PDNameTreeNode(final Class valueClass) {
        this.valueType = null;
        this.node = new COSDictionary();
        this.valueType = valueClass;
    }
    
    public PDNameTreeNode(final COSDictionary dict, final Class valueClass) {
        this.valueType = null;
        this.node = dict;
        this.valueType = valueClass;
    }
    
    public COSBase getCOSObject() {
        return this.node;
    }
    
    public COSDictionary getCOSDictionary() {
        return this.node;
    }
    
    public List getKids() {
        List retval = null;
        final COSArray kids = (COSArray)this.node.getDictionaryObject(COSName.KIDS);
        if (kids != null) {
            final List pdObjects = new ArrayList();
            for (int i = 0; i < kids.size(); ++i) {
                pdObjects.add(this.createChildNode((COSDictionary)kids.getObject(i)));
            }
            retval = new COSArrayList(pdObjects, kids);
        }
        return retval;
    }
    
    public void setKids(final List kids) {
        if (kids != null && kids.size() > 0) {
            final PDNameTreeNode firstKid = kids.get(0);
            final PDNameTreeNode lastKid = kids.get(kids.size() - 1);
            final String lowerLimit = firstKid.getLowerLimit();
            this.setLowerLimit(lowerLimit);
            final String upperLimit = lastKid.getUpperLimit();
            this.setUpperLimit(upperLimit);
        }
        this.node.setItem("Kids", COSArrayList.converterToCOSArray(kids));
    }
    
    public Object getValue(final String name) throws IOException {
        Object retval = null;
        final Map names = this.getNames();
        if (names != null) {
            retval = names.get(name);
        }
        else {
            final List kids = this.getKids();
            if (kids != null) {
                for (int i = 0; i < kids.size() && retval == null; ++i) {
                    final PDNameTreeNode childNode = kids.get(i);
                    if (childNode.getLowerLimit().compareTo(name) <= 0 && childNode.getUpperLimit().compareTo(name) >= 0) {
                        retval = childNode.getValue(name);
                    }
                }
            }
            else {
                PDNameTreeNode.log.warn("NameTreeNode does not have \"names\" nor \"kids\" objects.");
            }
        }
        return retval;
    }
    
    public Map<String, Object> getNames() throws IOException {
        final COSArray namesArray = (COSArray)this.node.getDictionaryObject(COSName.NAMES);
        if (namesArray != null) {
            final Map<String, Object> names = new LinkedHashMap<String, Object>();
            for (int i = 0; i < namesArray.size(); i += 2) {
                final COSString key = (COSString)namesArray.getObject(i);
                final COSBase cosValue = namesArray.getObject(i + 1);
                names.put(key.getString(), this.convertCOSToPD(cosValue));
            }
            return Collections.unmodifiableMap((Map<? extends String, ?>)names);
        }
        return null;
    }
    
    protected Object convertCOSToPD(final COSBase base) throws IOException {
        Object retval = null;
        try {
            final Constructor ctor = this.valueType.getConstructor(base.getClass());
            retval = ctor.newInstance(base);
        }
        catch (Throwable t) {
            throw new IOException("Error while trying to create value in named tree:" + t.getMessage());
        }
        return retval;
    }
    
    protected PDNameTreeNode createChildNode(final COSDictionary dic) {
        return new PDNameTreeNode(dic, this.valueType);
    }
    
    public void setNames(final Map<String, ? extends COSObjectable> names) {
        if (names == null) {
            this.node.setItem("Names", (COSObjectable)null);
            this.node.setItem(COSName.LIMITS, (COSObjectable)null);
        }
        else {
            final COSArray array = new COSArray();
            final List<String> keys = new ArrayList<String>(names.keySet());
            Collections.sort(keys);
            for (final String key : keys) {
                array.add(new COSString(key));
                array.add((COSObjectable)names.get(key));
            }
            this.setLowerLimit(keys.get(0));
            this.setUpperLimit(keys.get(keys.size() - 1));
            this.node.setItem("Names", array);
        }
    }
    
    public String getUpperLimit() {
        String retval = null;
        final COSArray arr = (COSArray)this.node.getDictionaryObject(COSName.LIMITS);
        if (arr != null) {
            retval = arr.getString(1);
        }
        return retval;
    }
    
    private void setUpperLimit(final String upper) {
        COSArray arr = (COSArray)this.node.getDictionaryObject(COSName.LIMITS);
        if (arr == null) {
            arr = new COSArray();
            arr.add(null);
            arr.add(null);
            this.node.setItem(COSName.LIMITS, arr);
        }
        arr.setString(1, upper);
    }
    
    public String getLowerLimit() {
        String retval = null;
        final COSArray arr = (COSArray)this.node.getDictionaryObject(COSName.LIMITS);
        if (arr != null) {
            retval = arr.getString(0);
        }
        return retval;
    }
    
    private void setLowerLimit(final String lower) {
        COSArray arr = (COSArray)this.node.getDictionaryObject(COSName.LIMITS);
        if (arr == null) {
            arr = new COSArray();
            arr.add(null);
            arr.add(null);
            this.node.setItem(COSName.LIMITS, arr);
        }
        arr.setString(0, lower);
    }
    
    static {
        log = LogFactory.getLog(PDNameTreeNode.class);
    }
}
