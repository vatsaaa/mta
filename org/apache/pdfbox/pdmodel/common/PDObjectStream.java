// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.common;

import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.cos.COSStream;

public class PDObjectStream extends PDStream
{
    public PDObjectStream(final COSStream str) {
        super(str);
    }
    
    public static PDObjectStream createStream(final PDDocument document) {
        final COSStream cosStream = new COSStream(document.getDocument().getScratchFile());
        final PDObjectStream strm = new PDObjectStream(cosStream);
        strm.getStream().setName("Type", "ObjStm");
        return strm;
    }
    
    public String getType() {
        return this.getStream().getNameAsString("Type");
    }
    
    public int getNumberOfObjects() {
        return this.getStream().getInt("N", 0);
    }
    
    public void setNumberOfObjects(final int n) {
        this.getStream().setInt("N", n);
    }
    
    public int getFirstByteOffset() {
        return this.getStream().getInt("First", 0);
    }
    
    public void setFirstByteOffset(final int n) {
        this.getStream().setInt("First", n);
    }
    
    public PDObjectStream getExtends() {
        PDObjectStream retval = null;
        final COSStream stream = (COSStream)this.getStream().getDictionaryObject(COSName.EXTENDS);
        if (stream != null) {
            retval = new PDObjectStream(stream);
        }
        return retval;
    }
    
    public void setExtends(final PDObjectStream stream) {
        this.getStream().setItem(COSName.EXTENDS, stream);
    }
}
