// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.common;

import javax.xml.transform.TransformerException;
import org.apache.jempbox.xmp.XMPMetadata;
import org.apache.pdfbox.cos.COSStream;
import java.io.IOException;
import java.io.InputStream;
import org.apache.pdfbox.pdmodel.PDDocument;

public class PDMetadata extends PDStream
{
    public PDMetadata(final PDDocument document) {
        super(document);
        this.getStream().setName("Type", "Metadata");
        this.getStream().setName("Subtype", "XML");
    }
    
    public PDMetadata(final PDDocument doc, final InputStream str, final boolean filtered) throws IOException {
        super(doc, str, filtered);
        this.getStream().setName("Type", "Metadata");
        this.getStream().setName("Subtype", "XML");
    }
    
    public PDMetadata(final COSStream str) {
        super(str);
    }
    
    public XMPMetadata exportXMPMetadata() throws IOException {
        return XMPMetadata.load(this.createInputStream());
    }
    
    public void importXMPMetadata(final XMPMetadata xmp) throws IOException, TransformerException {
        xmp.save(this.createOutputStream());
    }
}
