// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.common;

import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSDictionary;

public class PDDictionaryWrapper implements COSObjectable
{
    private final COSDictionary dictionary;
    
    public PDDictionaryWrapper() {
        this.dictionary = new COSDictionary();
    }
    
    public PDDictionaryWrapper(final COSDictionary dictionary) {
        this.dictionary = dictionary;
    }
    
    public COSBase getCOSObject() {
        return this.dictionary;
    }
    
    protected COSDictionary getCOSDictionary() {
        return this.dictionary;
    }
    
    @Override
    public boolean equals(final Object obj) {
        return this == obj || (obj instanceof PDDictionaryWrapper && this.dictionary.equals(((PDDictionaryWrapper)obj).dictionary));
    }
    
    @Override
    public int hashCode() {
        return this.dictionary.hashCode();
    }
}
