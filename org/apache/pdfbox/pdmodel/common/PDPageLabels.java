// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.common;

import java.util.NoSuchElementException;
import java.util.HashMap;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.cos.COSInteger;
import org.apache.pdfbox.cos.COSArray;
import org.apache.pdfbox.cos.COSBase;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.io.IOException;
import org.apache.pdfbox.cos.COSDictionary;
import java.util.TreeMap;
import org.apache.pdfbox.pdmodel.PDDocument;
import java.util.SortedMap;

public class PDPageLabels implements COSObjectable
{
    private SortedMap<Integer, PDPageLabelRange> labels;
    private PDDocument doc;
    
    public PDPageLabels(final PDDocument document) {
        this.labels = new TreeMap<Integer, PDPageLabelRange>();
        this.doc = document;
        final PDPageLabelRange defaultRange = new PDPageLabelRange();
        defaultRange.setStyle("D");
        this.labels.put(0, defaultRange);
    }
    
    public PDPageLabels(final PDDocument document, final COSDictionary dict) throws IOException {
        this(document);
        if (dict == null) {
            return;
        }
        final PDNumberTreeNode root = new PDNumberTreeNode(dict, COSDictionary.class);
        this.findLabels(root);
    }
    
    private void findLabels(final PDNumberTreeNode node) throws IOException {
        if (node.getKids() != null) {
            final List<PDNumberTreeNode> kids = (List<PDNumberTreeNode>)node.getKids();
            for (final PDNumberTreeNode kid : kids) {
                this.findLabels(kid);
            }
        }
        else if (node.getNumbers() != null) {
            final Map<Integer, COSDictionary> numbers = (Map<Integer, COSDictionary>)node.getNumbers();
            for (final Map.Entry<Integer, COSDictionary> i : numbers.entrySet()) {
                if (i.getKey() >= 0) {
                    this.labels.put(i.getKey(), new PDPageLabelRange(i.getValue()));
                }
            }
        }
    }
    
    public int getPageRangeCount() {
        return this.labels.size();
    }
    
    public PDPageLabelRange getPageLabelRange(final int startPage) {
        return this.labels.get(startPage);
    }
    
    public void setLabelItem(final int startPage, final PDPageLabelRange item) {
        this.labels.put(startPage, item);
    }
    
    public COSBase getCOSObject() {
        final COSDictionary dict = new COSDictionary();
        final COSArray arr = new COSArray();
        for (final Map.Entry<Integer, PDPageLabelRange> i : this.labels.entrySet()) {
            arr.add(COSInteger.get(i.getKey()));
            arr.add(i.getValue());
        }
        dict.setItem(COSName.NUMS, arr);
        return dict;
    }
    
    public Map<String, Integer> getPageIndicesByLabels() {
        final Map<String, Integer> labelMap = new HashMap<String, Integer>(this.doc.getNumberOfPages());
        this.computeLabels(new LabelHandler() {
            public void newLabel(final int pageIndex, final String label) {
                labelMap.put(label, pageIndex);
            }
        });
        return labelMap;
    }
    
    public String[] getLabelsByPageIndices() {
        final String[] map = new String[this.doc.getNumberOfPages()];
        this.computeLabels(new LabelHandler() {
            public void newLabel(final int pageIndex, final String label) {
                if (pageIndex < PDPageLabels.this.doc.getNumberOfPages()) {
                    map[pageIndex] = label;
                }
            }
        });
        return map;
    }
    
    private void computeLabels(final LabelHandler handler) {
        final Iterator<Map.Entry<Integer, PDPageLabelRange>> iterator = this.labels.entrySet().iterator();
        if (!iterator.hasNext()) {
            return;
        }
        int pageIndex = 0;
        Map.Entry<Integer, PDPageLabelRange> lastEntry = iterator.next();
        while (iterator.hasNext()) {
            final Map.Entry<Integer, PDPageLabelRange> entry = iterator.next();
            final int numPages = entry.getKey() - lastEntry.getKey();
            final LabelGenerator gen = new LabelGenerator(lastEntry.getValue(), numPages);
            while (gen.hasNext()) {
                handler.newLabel(pageIndex, gen.next());
                ++pageIndex;
            }
            lastEntry = entry;
        }
        final LabelGenerator gen2 = new LabelGenerator(lastEntry.getValue(), this.doc.getNumberOfPages() - lastEntry.getKey());
        while (gen2.hasNext()) {
            handler.newLabel(pageIndex, gen2.next());
            ++pageIndex;
        }
    }
    
    private static class LabelGenerator implements Iterator<String>
    {
        private PDPageLabelRange labelInfo;
        private int numPages;
        private int currentPage;
        private static final String[][] ROMANS;
        
        public LabelGenerator(final PDPageLabelRange label, final int pages) {
            this.labelInfo = label;
            this.numPages = pages;
            this.currentPage = 0;
        }
        
        public boolean hasNext() {
            return this.currentPage < this.numPages;
        }
        
        public String next() {
            if (!this.hasNext()) {
                throw new NoSuchElementException();
            }
            final StringBuilder buf = new StringBuilder();
            if (this.labelInfo.getPrefix() != null) {
                String label;
                for (label = this.labelInfo.getPrefix(); label.lastIndexOf(0) != -1; label = label.substring(0, label.length() - 1)) {}
                buf.append(label);
            }
            if (this.labelInfo.getStyle() != null) {
                buf.append(this.getNumber(this.labelInfo.getStart() + this.currentPage, this.labelInfo.getStyle()));
            }
            ++this.currentPage;
            return buf.toString();
        }
        
        private String getNumber(final int pageIndex, final String style) {
            if ("D".equals(style)) {
                return Integer.toString(pageIndex);
            }
            if ("a".equals(style)) {
                return makeLetterLabel(pageIndex);
            }
            if ("A".equals(style)) {
                return makeLetterLabel(pageIndex).toUpperCase();
            }
            if ("r".equals(style)) {
                return makeRomanLabel(pageIndex);
            }
            if ("R".equals(style)) {
                return makeRomanLabel(pageIndex).toUpperCase();
            }
            return Integer.toString(pageIndex);
        }
        
        private static String makeRomanLabel(int pageIndex) {
            final StringBuilder buf = new StringBuilder();
            for (int power = 0; power < 3 && pageIndex > 0; pageIndex /= 10, ++power) {
                buf.insert(0, LabelGenerator.ROMANS[power][pageIndex % 10]);
            }
            for (int i = 0; i < pageIndex; ++i) {
                buf.insert(0, 'm');
            }
            return buf.toString();
        }
        
        private static String makeLetterLabel(final int num) {
            final StringBuilder buf = new StringBuilder();
            final int numLetters = num / 26 + Integer.signum(num % 26);
            final int letter = num % 26 + 26 * (1 - Integer.signum(num % 26)) + 64;
            for (int i = 0; i < numLetters; ++i) {
                buf.appendCodePoint(letter);
            }
            return buf.toString();
        }
        
        public void remove() {
            throw new UnsupportedOperationException();
        }
        
        static {
            ROMANS = new String[][] { { "", "i", "ii", "iii", "iv", "v", "vi", "vii", "viii", "ix" }, { "", "x", "xx", "xxx", "xl", "l", "lx", "lxx", "lxxx", "xc" }, { "", "c", "cc", "ccc", "cd", "d", "dc", "dcc", "dccc", "cm" } };
        }
    }
    
    private interface LabelHandler
    {
        void newLabel(final int p0, final String p1);
    }
}
