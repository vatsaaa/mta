// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.common;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ByteArrayOutputStream;
import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSStream;
import org.apache.pdfbox.cos.COSString;

public class PDTextStream implements COSObjectable
{
    private COSString string;
    private COSStream stream;
    
    public PDTextStream(final COSString str) {
        this.string = str;
    }
    
    public PDTextStream(final String str) {
        this.string = new COSString(str);
    }
    
    public PDTextStream(final COSStream str) {
        this.stream = str;
    }
    
    public static PDTextStream createTextStream(final COSBase base) {
        PDTextStream retval = null;
        if (base instanceof COSString) {
            retval = new PDTextStream((COSString)base);
        }
        else if (base instanceof COSStream) {
            retval = new PDTextStream((COSStream)base);
        }
        return retval;
    }
    
    public COSBase getCOSObject() {
        COSBase retval = null;
        if (this.string == null) {
            retval = this.stream;
        }
        else {
            retval = this.string;
        }
        return retval;
    }
    
    public String getAsString() throws IOException {
        String retval = null;
        if (this.string != null) {
            retval = this.string.getString();
        }
        else {
            final ByteArrayOutputStream out = new ByteArrayOutputStream();
            final byte[] buffer = new byte[1024];
            int amountRead = -1;
            final InputStream is = this.stream.getUnfilteredStream();
            while ((amountRead = is.read(buffer)) != -1) {
                out.write(buffer, 0, amountRead);
            }
            retval = new String(out.toByteArray(), "ISO-8859-1");
        }
        return retval;
    }
    
    public InputStream getAsStream() throws IOException {
        InputStream retval = null;
        if (this.string != null) {
            retval = new ByteArrayInputStream(this.string.getBytes());
        }
        else {
            retval = this.stream.getUnfilteredStream();
        }
        return retval;
    }
}
