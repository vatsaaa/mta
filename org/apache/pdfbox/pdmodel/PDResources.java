// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel;

import org.apache.commons.logging.LogFactory;
import org.apache.pdfbox.util.MapUtil;
import org.apache.pdfbox.pdmodel.markedcontent.PDPropertyList;
import org.apache.pdfbox.pdmodel.graphics.color.PDColorSpaceFactory;
import org.apache.pdfbox.pdmodel.common.COSDictionaryMap;
import java.util.Iterator;
import org.apache.pdfbox.pdmodel.font.PDFontFactory;
import org.apache.pdfbox.cos.COSName;
import java.io.IOException;
import org.apache.pdfbox.cos.COSBase;
import org.apache.commons.logging.Log;
import org.apache.pdfbox.pdmodel.graphics.shading.PDShadingResources;
import org.apache.pdfbox.pdmodel.graphics.pattern.PDPatternResources;
import org.apache.pdfbox.pdmodel.graphics.PDExtendedGraphicsState;
import org.apache.pdfbox.pdmodel.graphics.xobject.PDXObjectImage;
import java.util.HashMap;
import org.apache.pdfbox.pdmodel.graphics.xobject.PDXObject;
import org.apache.pdfbox.pdmodel.graphics.color.PDColorSpace;
import org.apache.pdfbox.pdmodel.font.PDFont;
import java.util.Map;
import org.apache.pdfbox.cos.COSDictionary;
import org.apache.pdfbox.pdmodel.common.COSObjectable;

public class PDResources implements COSObjectable
{
    private COSDictionary resources;
    private Map<String, PDFont> fonts;
    private Map<PDFont, String> fontMappings;
    private Map<String, PDColorSpace> colorspaces;
    private Map<String, PDXObject> xobjects;
    private Map<PDXObject, String> xobjectMappings;
    private HashMap<String, PDXObjectImage> images;
    private Map<String, PDExtendedGraphicsState> graphicsStates;
    private Map<String, PDPatternResources> patterns;
    private Map<String, PDShadingResources> shadings;
    private static final Log LOG;
    
    public PDResources() {
        this.fonts = null;
        this.fontMappings = null;
        this.colorspaces = null;
        this.xobjects = null;
        this.xobjectMappings = null;
        this.images = null;
        this.graphicsStates = null;
        this.patterns = null;
        this.shadings = null;
        this.resources = new COSDictionary();
    }
    
    public PDResources(final COSDictionary resourceDictionary) {
        this.fonts = null;
        this.fontMappings = null;
        this.colorspaces = null;
        this.xobjects = null;
        this.xobjectMappings = null;
        this.images = null;
        this.graphicsStates = null;
        this.patterns = null;
        this.shadings = null;
        this.resources = resourceDictionary;
    }
    
    public COSDictionary getCOSDictionary() {
        return this.resources;
    }
    
    public COSBase getCOSObject() {
        return this.resources;
    }
    
    public void clear() {
        if (this.fonts != null) {
            this.fonts.clear();
        }
        if (this.colorspaces != null) {
            this.colorspaces.clear();
        }
        if (this.xobjects != null) {
            this.xobjects.clear();
        }
        if (this.images != null) {
            this.images.clear();
        }
        if (this.graphicsStates != null) {
            this.graphicsStates.clear();
        }
        if (this.patterns != null) {
            this.patterns.clear();
        }
        if (this.shadings != null) {
            this.shadings.clear();
        }
        this.resources = null;
    }
    
    @Deprecated
    public Map<String, PDFont> getFonts(final Map<String, PDFont> fontCache) throws IOException {
        return this.getFonts();
    }
    
    public Map<String, PDFont> getFonts() {
        if (this.fonts == null) {
            this.fonts = new HashMap<String, PDFont>();
            COSDictionary fontsDictionary = (COSDictionary)this.resources.getDictionaryObject(COSName.FONT);
            if (fontsDictionary == null) {
                fontsDictionary = new COSDictionary();
                this.resources.setItem(COSName.FONT, fontsDictionary);
            }
            else {
                for (final COSName fontName : fontsDictionary.keySet()) {
                    final COSBase font = fontsDictionary.getDictionaryObject(fontName);
                    if (font instanceof COSDictionary) {
                        PDFont newFont = null;
                        try {
                            newFont = PDFontFactory.createFont((COSDictionary)font);
                        }
                        catch (IOException exception) {
                            PDResources.LOG.error("error while creating a font", exception);
                        }
                        if (newFont == null) {
                            continue;
                        }
                        this.fonts.put(fontName.getName(), newFont);
                    }
                }
            }
        }
        return this.fonts;
    }
    
    public Map<String, PDXObject> getXObjects() {
        if (this.xobjects == null) {
            this.xobjects = new HashMap<String, PDXObject>();
            COSDictionary xobjectsDictionary = (COSDictionary)this.resources.getDictionaryObject(COSName.XOBJECT);
            if (xobjectsDictionary == null) {
                xobjectsDictionary = new COSDictionary();
                this.resources.setItem(COSName.XOBJECT, xobjectsDictionary);
            }
            else {
                this.xobjects = new HashMap<String, PDXObject>();
                for (final COSName objName : xobjectsDictionary.keySet()) {
                    PDXObject xobject = null;
                    try {
                        xobject = PDXObject.createXObject(xobjectsDictionary.getDictionaryObject(objName));
                    }
                    catch (IOException exception) {
                        PDResources.LOG.error("error while creating a xobject", exception);
                    }
                    if (xobject != null) {
                        this.xobjects.put(objName.getName(), xobject);
                    }
                }
            }
        }
        return this.xobjects;
    }
    
    public Map<String, PDXObjectImage> getImages() throws IOException {
        if (this.images == null) {
            final Map<String, PDXObject> allXObjects = this.getXObjects();
            this.images = new HashMap<String, PDXObjectImage>();
            for (final Map.Entry<String, PDXObject> entry : allXObjects.entrySet()) {
                final PDXObject xobject = entry.getValue();
                if (xobject instanceof PDXObjectImage) {
                    this.images.put(entry.getKey(), (PDXObjectImage)xobject);
                }
            }
        }
        return this.images;
    }
    
    public void setFonts(final Map<String, PDFont> fontsValue) {
        this.fonts = fontsValue;
        this.resources.setItem(COSName.FONT, COSDictionaryMap.convert(fontsValue));
    }
    
    public void setXObjects(final Map<String, PDXObject> xobjectsValue) {
        this.xobjects = xobjectsValue;
        this.resources.setItem(COSName.XOBJECT, COSDictionaryMap.convert(xobjectsValue));
    }
    
    public Map<String, PDColorSpace> getColorSpaces() {
        if (this.colorspaces == null) {
            final COSDictionary csDictionary = (COSDictionary)this.resources.getDictionaryObject(COSName.COLORSPACE);
            if (csDictionary != null) {
                this.colorspaces = new HashMap<String, PDColorSpace>();
                for (final COSName csName : csDictionary.keySet()) {
                    final COSBase cs = csDictionary.getDictionaryObject(csName);
                    PDColorSpace colorspace = null;
                    try {
                        colorspace = PDColorSpaceFactory.createColorSpace(cs);
                    }
                    catch (IOException exception) {
                        PDResources.LOG.error("error while creating a colorspace", exception);
                    }
                    if (colorspace != null) {
                        this.colorspaces.put(csName.getName(), colorspace);
                    }
                }
            }
        }
        return this.colorspaces;
    }
    
    public void setColorSpaces(final Map<String, PDColorSpace> csValue) {
        this.colorspaces = csValue;
        this.resources.setItem(COSName.COLORSPACE, COSDictionaryMap.convert(csValue));
    }
    
    public Map<String, PDExtendedGraphicsState> getGraphicsStates() {
        if (this.graphicsStates == null) {
            final COSDictionary states = (COSDictionary)this.resources.getDictionaryObject(COSName.EXT_G_STATE);
            if (states != null) {
                this.graphicsStates = new HashMap<String, PDExtendedGraphicsState>();
                for (final COSName name : states.keySet()) {
                    final COSDictionary dictionary = (COSDictionary)states.getDictionaryObject(name);
                    this.graphicsStates.put(name.getName(), new PDExtendedGraphicsState(dictionary));
                }
            }
        }
        return this.graphicsStates;
    }
    
    public void setGraphicsStates(final Map<String, PDExtendedGraphicsState> states) {
        this.graphicsStates = states;
        final Iterator<String> iter = states.keySet().iterator();
        final COSDictionary dic = new COSDictionary();
        while (iter.hasNext()) {
            final String name = iter.next();
            final PDExtendedGraphicsState state = states.get(name);
            dic.setItem(COSName.getPDFName(name), state.getCOSObject());
        }
        this.resources.setItem(COSName.EXT_G_STATE, dic);
    }
    
    public PDPropertyList getProperties() {
        PDPropertyList retval = null;
        final COSDictionary props = (COSDictionary)this.resources.getDictionaryObject(COSName.PROPERTIES);
        if (props != null) {
            retval = new PDPropertyList(props);
        }
        return retval;
    }
    
    public void setProperties(final PDPropertyList props) {
        this.resources.setItem(COSName.PROPERTIES, props.getCOSObject());
    }
    
    public Map<String, PDPatternResources> getPatterns() throws IOException {
        if (this.patterns == null) {
            final COSDictionary patternsDictionary = (COSDictionary)this.resources.getDictionaryObject(COSName.PATTERN);
            if (patternsDictionary != null) {
                this.patterns = new HashMap<String, PDPatternResources>();
                for (final COSName name : patternsDictionary.keySet()) {
                    final COSDictionary dictionary = (COSDictionary)patternsDictionary.getDictionaryObject(name);
                    this.patterns.put(name.getName(), PDPatternResources.create(dictionary));
                }
            }
        }
        return this.patterns;
    }
    
    public void setPatterns(final Map<String, PDPatternResources> patternsValue) {
        this.patterns = patternsValue;
        final Iterator<String> iter = patternsValue.keySet().iterator();
        final COSDictionary dic = new COSDictionary();
        while (iter.hasNext()) {
            final String name = iter.next();
            final PDPatternResources pattern = patternsValue.get(name);
            dic.setItem(COSName.getPDFName(name), pattern.getCOSObject());
        }
        this.resources.setItem(COSName.PATTERN, dic);
    }
    
    public Map<String, PDShadingResources> getShadings() throws IOException {
        if (this.shadings == null) {
            final COSDictionary shadingsDictionary = (COSDictionary)this.resources.getDictionaryObject(COSName.SHADING);
            if (shadingsDictionary != null) {
                this.shadings = new HashMap<String, PDShadingResources>();
                for (final COSName name : shadingsDictionary.keySet()) {
                    final COSDictionary dictionary = (COSDictionary)shadingsDictionary.getDictionaryObject(name);
                    this.shadings.put(name.getName(), PDShadingResources.create(dictionary));
                }
            }
        }
        return this.shadings;
    }
    
    public void setShadings(final Map<String, PDShadingResources> shadingsValue) {
        this.shadings = shadingsValue;
        final Iterator<String> iter = shadingsValue.keySet().iterator();
        final COSDictionary dic = new COSDictionary();
        while (iter.hasNext()) {
            final String name = iter.next();
            final PDShadingResources shading = shadingsValue.get(name);
            dic.setItem(COSName.getPDFName(name), shading.getCOSObject());
        }
        this.resources.setItem(COSName.SHADING, dic);
    }
    
    public String addFont(final PDFont font) {
        if (this.fonts == null) {
            this.fonts = this.getFonts();
            this.fontMappings = this.reverseMap(this.fonts, PDFont.class);
            this.setFonts(this.fonts);
        }
        String fontMapping = this.fontMappings.get(font);
        if (fontMapping == null) {
            fontMapping = MapUtil.getNextUniqueKey(this.fonts, "F");
            this.fontMappings.put(font, fontMapping);
            this.fonts.put(fontMapping, font);
            this.addFontToDictionary(font, fontMapping);
        }
        return fontMapping;
    }
    
    private void addFontToDictionary(final PDFont font, final String fontName) {
        final COSDictionary fontsDictionary = (COSDictionary)this.resources.getDictionaryObject(COSName.FONT);
        fontsDictionary.setItem(fontName, font);
    }
    
    public String addXObject(final PDXObject xobject, final String prefix) {
        if (this.xobjects == null) {
            this.xobjects = new HashMap<String, PDXObject>();
            this.xobjectMappings = this.reverseMap(this.xobjects, PDXObject.class);
            this.setXObjects(this.xobjects);
        }
        String objMapping = this.xobjectMappings.get(xobject);
        if (objMapping == null) {
            objMapping = MapUtil.getNextUniqueKey(this.xobjects, prefix);
            this.xobjectMappings.put(xobject, objMapping);
            this.xobjects.put(objMapping, xobject);
            this.addXObjectToDictionary(xobject, objMapping);
        }
        return objMapping;
    }
    
    private void addXObjectToDictionary(final PDXObject xobject, final String xobjectName) {
        final COSDictionary fontsDictionary = (COSDictionary)this.resources.getDictionaryObject(COSName.XOBJECT);
        fontsDictionary.setItem(xobjectName, xobject);
    }
    
    private <T> Map<T, String> reverseMap(final Map<String, T> map, final Class<T> keyClass) {
        final Map<T, String> reversed = new HashMap<T, String>();
        for (final Map.Entry<String, T> entry : map.entrySet()) {
            reversed.put(keyClass.cast(entry.getValue()), entry.getKey());
        }
        return reversed;
    }
    
    static {
        LOG = LogFactory.getLog(PDResources.class);
    }
}
