// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel;

import org.apache.commons.logging.LogFactory;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.common.COSArrayList;
import java.util.ArrayList;
import org.apache.pdfbox.cos.COSNumber;
import java.util.Iterator;
import java.util.List;
import org.apache.pdfbox.cos.COSInteger;
import org.apache.pdfbox.cos.COSArray;
import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSName;
import org.apache.commons.logging.Log;
import org.apache.pdfbox.cos.COSDictionary;
import org.apache.pdfbox.pdmodel.common.COSObjectable;

public class PDPageNode implements COSObjectable
{
    private COSDictionary page;
    private static final Log log;
    
    public PDPageNode() {
        (this.page = new COSDictionary()).setItem(COSName.TYPE, COSName.PAGES);
        this.page.setItem(COSName.KIDS, new COSArray());
        this.page.setItem(COSName.COUNT, COSInteger.ZERO);
    }
    
    public PDPageNode(final COSDictionary pages) {
        this.page = pages;
    }
    
    public long updateCount() {
        long totalCount = 0L;
        final List kids = this.getKids();
        for (final Object next : kids) {
            if (next instanceof PDPage) {
                ++totalCount;
            }
            else {
                final PDPageNode node = (PDPageNode)next;
                totalCount += node.updateCount();
            }
        }
        this.page.setLong(COSName.COUNT, totalCount);
        return totalCount;
    }
    
    public long getCount() {
        if (this.page == null) {
            return 0L;
        }
        final COSBase num = this.page.getDictionaryObject(COSName.COUNT);
        if (num == null) {
            return 0L;
        }
        return ((COSNumber)num).intValue();
    }
    
    public COSDictionary getDictionary() {
        return this.page;
    }
    
    public PDPageNode getParent() {
        PDPageNode parent = null;
        final COSDictionary parentDic = (COSDictionary)this.page.getDictionaryObject(COSName.PARENT, COSName.P);
        if (parentDic != null) {
            parent = new PDPageNode(parentDic);
        }
        return parent;
    }
    
    public void setParent(final PDPageNode parent) {
        this.page.setItem(COSName.PARENT, parent.getDictionary());
    }
    
    public COSBase getCOSObject() {
        return this.page;
    }
    
    public List getKids() {
        final List actuals = new ArrayList();
        final COSArray kids = getAllKids(actuals, this.page, false);
        return new COSArrayList(actuals, kids);
    }
    
    public void getAllKids(final List result) {
        getAllKids(result, this.page, true);
    }
    
    private static COSArray getAllKids(final List result, final COSDictionary page, final boolean recurse) {
        if (page == null) {
            return null;
        }
        final COSArray kids = (COSArray)page.getDictionaryObject(COSName.KIDS);
        if (kids == null) {
            PDPageNode.log.error("No Kids found in getAllKids(). Probably a malformed pdf.");
            return null;
        }
        for (int i = 0; i < kids.size(); ++i) {
            final COSBase obj = kids.getObject(i);
            if (obj instanceof COSDictionary) {
                final COSDictionary kid = (COSDictionary)obj;
                if (COSName.PAGE.equals(kid.getDictionaryObject(COSName.TYPE))) {
                    result.add(new PDPage(kid));
                }
                else if (recurse) {
                    getAllKids(result, kid, recurse);
                }
                else {
                    result.add(new PDPageNode(kid));
                }
            }
        }
        return kids;
    }
    
    public PDResources getResources() {
        PDResources retval = null;
        final COSDictionary resources = (COSDictionary)this.page.getDictionaryObject(COSName.RESOURCES);
        if (resources != null) {
            retval = new PDResources(resources);
        }
        return retval;
    }
    
    public PDResources findResources() {
        PDResources retval = this.getResources();
        final PDPageNode parent = this.getParent();
        if (retval == null && parent != null) {
            retval = parent.findResources();
        }
        return retval;
    }
    
    public void setResources(final PDResources resources) {
        if (resources == null) {
            this.page.removeItem(COSName.RESOURCES);
        }
        else {
            this.page.setItem(COSName.RESOURCES, resources.getCOSDictionary());
        }
    }
    
    public PDRectangle getMediaBox() {
        PDRectangle retval = null;
        final COSArray array = (COSArray)this.page.getDictionaryObject(COSName.MEDIA_BOX);
        if (array != null) {
            retval = new PDRectangle(array);
        }
        return retval;
    }
    
    public PDRectangle findMediaBox() {
        PDRectangle retval = this.getMediaBox();
        final PDPageNode parent = this.getParent();
        if (retval == null && parent != null) {
            retval = parent.findMediaBox();
        }
        return retval;
    }
    
    public void setMediaBox(final PDRectangle mediaBox) {
        if (mediaBox == null) {
            this.page.removeItem(COSName.MEDIA_BOX);
        }
        else {
            this.page.setItem(COSName.MEDIA_BOX, mediaBox.getCOSArray());
        }
    }
    
    public PDRectangle getCropBox() {
        PDRectangle retval = null;
        final COSArray array = (COSArray)this.page.getDictionaryObject(COSName.CROP_BOX);
        if (array != null) {
            retval = new PDRectangle(array);
        }
        return retval;
    }
    
    public PDRectangle findCropBox() {
        PDRectangle retval = this.getCropBox();
        final PDPageNode parent = this.getParent();
        if (retval == null && parent != null) {
            retval = this.findParentCropBox(parent);
        }
        if (retval == null) {
            retval = this.findMediaBox();
        }
        return retval;
    }
    
    private PDRectangle findParentCropBox(final PDPageNode node) {
        PDRectangle rect = node.getCropBox();
        final PDPageNode parent = node.getParent();
        if (rect == null && parent != null) {
            rect = this.findParentCropBox(node);
        }
        return rect;
    }
    
    public void setCropBox(final PDRectangle cropBox) {
        if (cropBox == null) {
            this.page.removeItem(COSName.CROP_BOX);
        }
        else {
            this.page.setItem(COSName.CROP_BOX, cropBox.getCOSArray());
        }
    }
    
    public Integer getRotation() {
        Integer retval = null;
        final COSNumber value = (COSNumber)this.page.getDictionaryObject(COSName.ROTATE);
        if (value != null) {
            retval = new Integer(value.intValue());
        }
        return retval;
    }
    
    public int findRotation() {
        int retval = 0;
        final Integer rotation = this.getRotation();
        if (rotation != null) {
            retval = rotation;
        }
        else {
            final PDPageNode parent = this.getParent();
            if (parent != null) {
                retval = parent.findRotation();
            }
        }
        return retval;
    }
    
    public void setRotation(final int rotation) {
        this.page.setInt(COSName.ROTATE, rotation);
    }
    
    static {
        log = LogFactory.getLog(PDPageNode.class);
    }
}
