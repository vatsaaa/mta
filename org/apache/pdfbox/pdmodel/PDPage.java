// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel;

import java.awt.print.PrinterException;
import java.awt.print.PrinterIOException;
import java.awt.print.PageFormat;
import org.apache.pdfbox.pdmodel.interactive.annotation.PDAnnotation;
import org.apache.pdfbox.pdmodel.interactive.action.PDPageAdditionalActions;
import java.awt.Graphics;
import org.apache.pdfbox.pdfviewer.PageDrawer;
import java.awt.Graphics2D;
import java.awt.Dimension;
import java.awt.image.BufferedImage;
import org.apache.pdfbox.cos.COSStream;
import org.apache.pdfbox.pdmodel.common.PDMetadata;
import org.apache.pdfbox.pdmodel.common.COSArrayList;
import org.apache.pdfbox.pdmodel.interactive.pagenavigation.PDThreadBead;
import java.util.ArrayList;
import java.util.List;
import org.apache.pdfbox.pdmodel.common.PDStream;
import org.apache.pdfbox.cos.COSNumber;
import org.apache.pdfbox.cos.COSArray;
import java.io.IOException;
import java.util.Calendar;
import java.util.GregorianCalendar;
import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.cos.COSDictionary;
import java.awt.Color;
import java.awt.print.Printable;
import org.apache.pdfbox.pdmodel.common.COSObjectable;

public class PDPage implements COSObjectable, Printable
{
    private static final int DEFAULT_USER_SPACE_UNIT_DPI = 72;
    private static final float MM_TO_UNITS = 2.8346457f;
    private static final Color TRANSPARENT_WHITE;
    private COSDictionary page;
    public static final PDRectangle PAGE_SIZE_LETTER;
    public static final PDRectangle PAGE_SIZE_A0;
    public static final PDRectangle PAGE_SIZE_A1;
    public static final PDRectangle PAGE_SIZE_A2;
    public static final PDRectangle PAGE_SIZE_A3;
    public static final PDRectangle PAGE_SIZE_A4;
    public static final PDRectangle PAGE_SIZE_A5;
    public static final PDRectangle PAGE_SIZE_A6;
    private PDPageNode parent;
    private PDRectangle mediaBox;
    
    public PDPage() {
        this.parent = null;
        this.mediaBox = null;
        (this.page = new COSDictionary()).setItem(COSName.TYPE, COSName.PAGE);
        this.setMediaBox(PDPage.PAGE_SIZE_LETTER);
    }
    
    public PDPage(final PDRectangle size) {
        this.parent = null;
        this.mediaBox = null;
        (this.page = new COSDictionary()).setItem(COSName.TYPE, COSName.PAGE);
        this.setMediaBox(size);
    }
    
    public PDPage(final COSDictionary pageDic) {
        this.parent = null;
        this.mediaBox = null;
        this.page = pageDic;
    }
    
    public COSBase getCOSObject() {
        return this.page;
    }
    
    public COSDictionary getCOSDictionary() {
        return this.page;
    }
    
    public PDPageNode getParent() {
        if (this.parent == null) {
            final COSDictionary parentDic = (COSDictionary)this.page.getDictionaryObject(COSName.PARENT, COSName.P);
            if (parentDic != null) {
                this.parent = new PDPageNode(parentDic);
            }
        }
        return this.parent;
    }
    
    public void setParent(final PDPageNode parentNode) {
        this.parent = parentNode;
        this.page.setItem(COSName.PARENT, this.parent.getDictionary());
    }
    
    public void updateLastModified() {
        this.page.setDate(COSName.LAST_MODIFIED, new GregorianCalendar());
    }
    
    public Calendar getLastModified() throws IOException {
        return this.page.getDate(COSName.LAST_MODIFIED);
    }
    
    public PDResources getResources() {
        PDResources retval = null;
        final COSDictionary resources = (COSDictionary)this.page.getDictionaryObject(COSName.RESOURCES);
        if (resources != null) {
            retval = new PDResources(resources);
        }
        return retval;
    }
    
    public PDResources findResources() {
        PDResources retval = this.getResources();
        final PDPageNode parentNode = this.getParent();
        if (retval == null && this.parent != null) {
            retval = parentNode.findResources();
        }
        return retval;
    }
    
    public void setResources(final PDResources resources) {
        this.page.setItem(COSName.RESOURCES, resources);
    }
    
    public PDRectangle getMediaBox() {
        if (this.mediaBox == null) {
            final COSArray array = (COSArray)this.page.getDictionaryObject(COSName.MEDIA_BOX);
            if (array != null) {
                this.mediaBox = new PDRectangle(array);
            }
        }
        return this.mediaBox;
    }
    
    public PDRectangle findMediaBox() {
        PDRectangle retval = this.getMediaBox();
        if (retval == null && this.getParent() != null) {
            retval = this.getParent().findMediaBox();
        }
        return retval;
    }
    
    public void setMediaBox(final PDRectangle mediaBoxValue) {
        this.mediaBox = mediaBoxValue;
        if (mediaBoxValue == null) {
            this.page.removeItem(COSName.MEDIA_BOX);
        }
        else {
            this.page.setItem(COSName.MEDIA_BOX, mediaBoxValue.getCOSArray());
        }
    }
    
    public PDRectangle getCropBox() {
        PDRectangle retval = null;
        final COSArray array = (COSArray)this.page.getDictionaryObject(COSName.CROP_BOX);
        if (array != null) {
            retval = new PDRectangle(array);
        }
        return retval;
    }
    
    public PDRectangle findCropBox() {
        PDRectangle retval = this.getCropBox();
        final PDPageNode parentNode = this.getParent();
        if (retval == null && parentNode != null) {
            retval = this.findParentCropBox(parentNode);
        }
        if (retval == null) {
            retval = this.findMediaBox();
        }
        return retval;
    }
    
    private PDRectangle findParentCropBox(final PDPageNode node) {
        PDRectangle rect = node.getCropBox();
        final PDPageNode parentNode = node.getParent();
        if (rect == null && parentNode != null) {
            rect = this.findParentCropBox(parentNode);
        }
        return rect;
    }
    
    public void setCropBox(final PDRectangle cropBox) {
        if (cropBox == null) {
            this.page.removeItem(COSName.CROP_BOX);
        }
        else {
            this.page.setItem(COSName.CROP_BOX, cropBox.getCOSArray());
        }
    }
    
    public PDRectangle getBleedBox() {
        PDRectangle retval = null;
        final COSArray array = (COSArray)this.page.getDictionaryObject(COSName.BLEED_BOX);
        if (array != null) {
            retval = new PDRectangle(array);
        }
        else {
            retval = this.findCropBox();
        }
        return retval;
    }
    
    public void setBleedBox(final PDRectangle bleedBox) {
        if (bleedBox == null) {
            this.page.removeItem(COSName.BLEED_BOX);
        }
        else {
            this.page.setItem(COSName.BLEED_BOX, bleedBox.getCOSArray());
        }
    }
    
    public PDRectangle getTrimBox() {
        PDRectangle retval = null;
        final COSArray array = (COSArray)this.page.getDictionaryObject(COSName.TRIM_BOX);
        if (array != null) {
            retval = new PDRectangle(array);
        }
        else {
            retval = this.findCropBox();
        }
        return retval;
    }
    
    public void setTrimBox(final PDRectangle trimBox) {
        if (trimBox == null) {
            this.page.removeItem(COSName.TRIM_BOX);
        }
        else {
            this.page.setItem(COSName.TRIM_BOX, trimBox.getCOSArray());
        }
    }
    
    public PDRectangle getArtBox() {
        PDRectangle retval = null;
        final COSArray array = (COSArray)this.page.getDictionaryObject(COSName.ART_BOX);
        if (array != null) {
            retval = new PDRectangle(array);
        }
        else {
            retval = this.findCropBox();
        }
        return retval;
    }
    
    public void setArtBox(final PDRectangle artBox) {
        if (artBox == null) {
            this.page.removeItem(COSName.ART_BOX);
        }
        else {
            this.page.setItem(COSName.ART_BOX, artBox.getCOSArray());
        }
    }
    
    public Integer getRotation() {
        Integer retval = null;
        final COSNumber value = (COSNumber)this.page.getDictionaryObject(COSName.ROTATE);
        if (value != null) {
            retval = new Integer(value.intValue());
        }
        return retval;
    }
    
    public int findRotation() {
        int retval = 0;
        final Integer rotation = this.getRotation();
        if (rotation != null) {
            retval = rotation;
        }
        else {
            final PDPageNode parentNode = this.getParent();
            if (parentNode != null) {
                retval = parentNode.findRotation();
            }
        }
        return retval;
    }
    
    public void setRotation(final int rotation) {
        this.page.setInt(COSName.ROTATE, rotation);
    }
    
    public PDStream getContents() throws IOException {
        return PDStream.createFromCOS(this.page.getDictionaryObject(COSName.CONTENTS));
    }
    
    public void setContents(final PDStream contents) {
        this.page.setItem(COSName.CONTENTS, contents);
    }
    
    public List getThreadBeads() {
        COSArray beads = (COSArray)this.page.getDictionaryObject(COSName.B);
        if (beads == null) {
            beads = new COSArray();
        }
        final List<PDThreadBead> pdObjects = new ArrayList<PDThreadBead>();
        for (int i = 0; i < beads.size(); ++i) {
            final COSDictionary beadDic = (COSDictionary)beads.getObject(i);
            PDThreadBead bead = null;
            if (beadDic != null) {
                bead = new PDThreadBead(beadDic);
            }
            pdObjects.add(bead);
        }
        return new COSArrayList(pdObjects, beads);
    }
    
    public void setThreadBeads(final List<PDThreadBead> beads) {
        this.page.setItem(COSName.B, COSArrayList.converterToCOSArray(beads));
    }
    
    public PDMetadata getMetadata() {
        PDMetadata retval = null;
        final COSStream stream = (COSStream)this.page.getDictionaryObject(COSName.METADATA);
        if (stream != null) {
            retval = new PDMetadata(stream);
        }
        return retval;
    }
    
    public void setMetadata(final PDMetadata meta) {
        this.page.setItem(COSName.METADATA, meta);
    }
    
    public BufferedImage convertToImage() throws IOException {
        return this.convertToImage(8, 144);
    }
    
    public BufferedImage convertToImage(final int imageType, final int resolution) throws IOException {
        final PDRectangle cropBox = this.findCropBox();
        final float widthPt = cropBox.getWidth();
        final float heightPt = cropBox.getHeight();
        final float scaling = resolution / 72.0f;
        final int widthPx = Math.round(widthPt * scaling);
        final int heightPx = Math.round(heightPt * scaling);
        final Dimension pageDimension = new Dimension((int)widthPt, (int)heightPt);
        BufferedImage retval = null;
        final float rotation = (float)Math.toRadians(this.findRotation());
        if (rotation != 0.0f) {
            retval = new BufferedImage(heightPx, widthPx, imageType);
        }
        else {
            retval = new BufferedImage(widthPx, heightPx, imageType);
        }
        final Graphics2D graphics = (Graphics2D)retval.getGraphics();
        graphics.setBackground(PDPage.TRANSPARENT_WHITE);
        graphics.clearRect(0, 0, retval.getWidth(), retval.getHeight());
        if (rotation != 0.0f) {
            graphics.translate(retval.getWidth(), 0.0);
            graphics.rotate(rotation);
        }
        graphics.scale(scaling, scaling);
        final PageDrawer drawer = new PageDrawer();
        drawer.drawPage(graphics, this, pageDimension);
        return retval;
    }
    
    public PDPageAdditionalActions getActions() {
        COSDictionary addAct = (COSDictionary)this.page.getDictionaryObject(COSName.AA);
        if (addAct == null) {
            addAct = new COSDictionary();
            this.page.setItem(COSName.AA, addAct);
        }
        return new PDPageAdditionalActions(addAct);
    }
    
    public void setActions(final PDPageAdditionalActions actions) {
        this.page.setItem(COSName.AA, actions);
    }
    
    public List getAnnotations() throws IOException {
        COSArrayList retval = null;
        COSArray annots = (COSArray)this.page.getDictionaryObject(COSName.ANNOTS);
        if (annots == null) {
            annots = new COSArray();
            this.page.setItem(COSName.ANNOTS, annots);
            retval = new COSArrayList(new ArrayList(), annots);
        }
        else {
            final List<PDAnnotation> actuals = new ArrayList<PDAnnotation>();
            for (int i = 0; i < annots.size(); ++i) {
                final COSBase item = annots.getObject(i);
                actuals.add(PDAnnotation.createAnnotation(item));
            }
            retval = new COSArrayList(actuals, annots);
        }
        return retval;
    }
    
    public void setAnnotations(final List<PDAnnotation> annots) {
        this.page.setItem(COSName.ANNOTS, COSArrayList.converterToCOSArray(annots));
    }
    
    @Deprecated
    public int print(final Graphics graphics, final PageFormat pageFormat, final int pageIndex) throws PrinterException {
        try {
            final PageDrawer drawer = new PageDrawer();
            final PDRectangle cropBox = this.findCropBox();
            drawer.drawPage(graphics, this, cropBox.createDimension());
            return 0;
        }
        catch (IOException io) {
            throw new PrinterIOException(io);
        }
    }
    
    @Override
    public boolean equals(final Object other) {
        return other instanceof PDPage && ((PDPage)other).getCOSObject() == this.getCOSObject();
    }
    
    @Override
    public int hashCode() {
        return this.getCOSDictionary().hashCode();
    }
    
    static {
        TRANSPARENT_WHITE = new Color(255, 255, 255, 0);
        PAGE_SIZE_LETTER = new PDRectangle(612.0f, 792.0f);
        PAGE_SIZE_A0 = new PDRectangle(2383.937f, 3370.3938f);
        PAGE_SIZE_A1 = new PDRectangle(1683.7795f, 2383.937f);
        PAGE_SIZE_A2 = new PDRectangle(1190.5513f, 1683.7795f);
        PAGE_SIZE_A3 = new PDRectangle(841.8898f, 1190.5513f);
        PAGE_SIZE_A4 = new PDRectangle(595.27563f, 841.8898f);
        PAGE_SIZE_A5 = new PDRectangle(419.52756f, 595.27563f);
        PAGE_SIZE_A6 = new PDRectangle(297.63782f, 419.52756f);
    }
}
