// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel;

import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSDictionary;
import org.apache.pdfbox.pdmodel.common.COSObjectable;

public class PDDocumentNameDictionary implements COSObjectable
{
    private COSDictionary nameDictionary;
    private PDDocumentCatalog catalog;
    
    public PDDocumentNameDictionary(final PDDocumentCatalog cat) {
        this.nameDictionary = new COSDictionary();
        this.catalog = cat;
    }
    
    public PDDocumentNameDictionary(final PDDocumentCatalog cat, final COSDictionary names) {
        this.catalog = cat;
        this.nameDictionary = names;
    }
    
    public COSBase getCOSObject() {
        return this.nameDictionary;
    }
    
    public COSDictionary getCOSDictionary() {
        return this.nameDictionary;
    }
    
    public PDDestinationNameTreeNode getDests() {
        PDDestinationNameTreeNode dests = null;
        COSDictionary dic = (COSDictionary)this.nameDictionary.getDictionaryObject(COSName.DESTS);
        if (dic == null) {
            dic = (COSDictionary)this.catalog.getCOSDictionary().getDictionaryObject(COSName.DESTS);
        }
        if (dic != null) {
            dests = new PDDestinationNameTreeNode(dic);
        }
        return dests;
    }
    
    public void setDests(final PDDestinationNameTreeNode dests) {
        this.nameDictionary.setItem(COSName.DESTS, dests);
        this.catalog.getCOSDictionary().setItem(COSName.DESTS, (COSObjectable)null);
    }
    
    public PDEmbeddedFilesNameTreeNode getEmbeddedFiles() {
        PDEmbeddedFilesNameTreeNode retval = null;
        final COSDictionary dic = (COSDictionary)this.nameDictionary.getDictionaryObject(COSName.EMBEDDED_FILES);
        if (dic != null) {
            retval = new PDEmbeddedFilesNameTreeNode(dic);
        }
        return retval;
    }
    
    public void setEmbeddedFiles(final PDEmbeddedFilesNameTreeNode ef) {
        this.nameDictionary.setItem(COSName.EMBEDDED_FILES, ef);
    }
    
    public PDJavascriptNameTreeNode getJavaScript() {
        PDJavascriptNameTreeNode retval = null;
        final COSDictionary dic = (COSDictionary)this.nameDictionary.getDictionaryObject(COSName.JAVA_SCRIPT);
        if (dic != null) {
            retval = new PDJavascriptNameTreeNode(dic);
        }
        return retval;
    }
    
    public void setJavascript(final PDJavascriptNameTreeNode js) {
        this.nameDictionary.setItem(COSName.JAVA_SCRIPT, js);
    }
}
