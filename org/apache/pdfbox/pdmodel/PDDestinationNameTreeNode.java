// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel;

import java.io.IOException;
import org.apache.pdfbox.pdmodel.interactive.documentnavigation.destination.PDDestination;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSDictionary;
import org.apache.pdfbox.pdmodel.interactive.documentnavigation.destination.PDPageDestination;
import org.apache.pdfbox.pdmodel.common.PDNameTreeNode;

public class PDDestinationNameTreeNode extends PDNameTreeNode
{
    public PDDestinationNameTreeNode() {
        super(PDPageDestination.class);
    }
    
    public PDDestinationNameTreeNode(final COSDictionary dic) {
        super(dic, PDPageDestination.class);
    }
    
    @Override
    protected Object convertCOSToPD(final COSBase base) throws IOException {
        COSBase destination = base;
        if (base instanceof COSDictionary) {
            destination = ((COSDictionary)base).getDictionaryObject(COSName.D);
        }
        return PDDestination.create(destination);
    }
    
    @Override
    protected PDNameTreeNode createChildNode(final COSDictionary dic) {
        return new PDDestinationNameTreeNode(dic);
    }
}
