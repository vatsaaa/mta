// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdmodel.text;

import org.apache.pdfbox.pdmodel.font.PDFont;

public class PDTextState implements Cloneable
{
    public static final int RENDERING_MODE_FILL_TEXT = 0;
    public static final int RENDERING_MODE_STROKE_TEXT = 1;
    public static final int RENDERING_MODE_FILL_THEN_STROKE_TEXT = 2;
    public static final int RENDERING_MODE_NEITHER_FILL_NOR_STROKE_TEXT = 3;
    public static final int RENDERING_MODE_FILL_TEXT_AND_ADD_TO_PATH_FOR_CLIPPING = 4;
    public static final int RENDERING_MODE_STROKE_TEXT_AND_ADD_TO_PATH_FOR_CLIPPING = 5;
    public static final int RENDERING_MODE_FILL_THEN_STROKE_TEXT_AND_ADD_TO_PATH_FOR_CLIPPING = 6;
    public static final int RENDERING_MODE_ADD_TEXT_TO_PATH_FOR_CLIPPING = 7;
    private float characterSpacing;
    private float wordSpacing;
    private float horizontalScaling;
    private float leading;
    private PDFont font;
    private float fontSize;
    private int renderingMode;
    private float rise;
    private boolean knockout;
    
    public PDTextState() {
        this.characterSpacing = 0.0f;
        this.wordSpacing = 0.0f;
        this.horizontalScaling = 100.0f;
        this.leading = 0.0f;
        this.renderingMode = 0;
        this.rise = 0.0f;
        this.knockout = true;
    }
    
    public float getCharacterSpacing() {
        return this.characterSpacing;
    }
    
    public void setCharacterSpacing(final float value) {
        this.characterSpacing = value;
    }
    
    public float getWordSpacing() {
        return this.wordSpacing;
    }
    
    public void setWordSpacing(final float value) {
        this.wordSpacing = value;
    }
    
    public float getHorizontalScalingPercent() {
        return this.horizontalScaling;
    }
    
    public void setHorizontalScalingPercent(final float value) {
        this.horizontalScaling = value;
    }
    
    public float getLeading() {
        return this.leading;
    }
    
    public void setLeading(final float value) {
        this.leading = value;
    }
    
    public PDFont getFont() {
        return this.font;
    }
    
    public void setFont(final PDFont value) {
        this.font = value;
    }
    
    public float getFontSize() {
        return this.fontSize;
    }
    
    public void setFontSize(final float value) {
        this.fontSize = value;
    }
    
    public int getRenderingMode() {
        return this.renderingMode;
    }
    
    public void setRenderingMode(final int value) {
        this.renderingMode = value;
    }
    
    public float getRise() {
        return this.rise;
    }
    
    public void setRise(final float value) {
        this.rise = value;
    }
    
    public boolean getKnockoutFlag() {
        return this.knockout;
    }
    
    public void setKnockoutFlag(final boolean value) {
        this.knockout = value;
    }
    
    public Object clone() {
        try {
            return super.clone();
        }
        catch (CloneNotSupportedException ignore) {
            return null;
        }
    }
}
