// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox;

import java.io.ByteArrayOutputStream;
import java.util.Map;
import java.util.Iterator;
import org.apache.pdfbox.cos.COSArray;
import org.apache.pdfbox.cos.COSBase;
import java.util.TreeMap;
import org.apache.pdfbox.pdmodel.PDResources;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDDocumentCatalog;
import org.apache.pdfbox.cos.COSDictionary;
import java.io.InputStream;
import org.apache.pdfbox.pdfparser.PDFParser;
import java.io.FileInputStream;
import java.io.OutputStream;
import org.apache.pdfbox.pdfwriter.COSWriter;
import java.io.FileOutputStream;
import org.apache.pdfbox.exceptions.COSVisitorException;
import java.io.IOException;
import java.util.ArrayList;
import org.apache.pdfbox.cos.COSStream;
import org.apache.pdfbox.pdmodel.PDDocument;
import java.util.List;
import org.apache.pdfbox.cos.COSName;

public class Overlay
{
    @Deprecated
    public static final COSName XOBJECT;
    @Deprecated
    public static final COSName PROC_SET;
    @Deprecated
    public static final COSName EXT_G_STATE;
    private List layoutPages;
    private PDDocument pdfOverlay;
    private PDDocument pdfDocument;
    private int pageCount;
    private COSStream saveGraphicsStateStream;
    private COSStream restoreGraphicsStateStream;
    
    public Overlay() {
        this.layoutPages = new ArrayList(10);
        this.pageCount = 0;
    }
    
    public static void main(final String[] args) throws IOException, COSVisitorException {
        if (args.length != 3) {
            usage();
            System.exit(1);
        }
        else {
            PDDocument overlay = null;
            PDDocument pdf = null;
            try {
                overlay = getDocument(args[0]);
                pdf = getDocument(args[1]);
                final Overlay overlayer = new Overlay();
                overlayer.overlay(overlay, pdf);
                writeDocument(pdf, args[2]);
            }
            finally {
                if (overlay != null) {
                    overlay.close();
                }
                if (pdf != null) {
                    pdf.close();
                }
            }
        }
    }
    
    private static void writeDocument(final PDDocument pdf, final String filename) throws IOException, COSVisitorException {
        FileOutputStream output = null;
        COSWriter writer = null;
        try {
            output = new FileOutputStream(filename);
            writer = new COSWriter(output);
            writer.write(pdf);
        }
        finally {
            if (writer != null) {
                writer.close();
            }
            if (output != null) {
                output.close();
            }
        }
    }
    
    private static PDDocument getDocument(final String filename) throws IOException {
        FileInputStream input = null;
        PDFParser parser = null;
        PDDocument result = null;
        try {
            input = new FileInputStream(filename);
            parser = new PDFParser(input);
            parser.parse();
            result = parser.getPDDocument();
        }
        finally {
            if (input != null) {
                input.close();
            }
        }
        return result;
    }
    
    private static void usage() {
        System.err.println("usage: java -jar pdfbox-app-x.y.z.jar Overlay <overlay.pdf> <document.pdf> <result.pdf>");
    }
    
    public PDDocument overlay(final PDDocument overlay, final PDDocument destination) throws IOException {
        this.pdfOverlay = overlay;
        this.pdfDocument = destination;
        final PDDocumentCatalog overlayCatalog = this.pdfOverlay.getDocumentCatalog();
        this.collectLayoutPages(overlayCatalog.getAllPages());
        final COSDictionary saveGraphicsStateDic = new COSDictionary();
        this.saveGraphicsStateStream = new COSStream(saveGraphicsStateDic, this.pdfDocument.getDocument().getScratchFile());
        final OutputStream saveStream = this.saveGraphicsStateStream.createUnfilteredStream();
        saveStream.write(" q\n".getBytes("ISO-8859-1"));
        saveStream.flush();
        this.restoreGraphicsStateStream = new COSStream(saveGraphicsStateDic, this.pdfDocument.getDocument().getScratchFile());
        final OutputStream restoreStream = this.restoreGraphicsStateStream.createUnfilteredStream();
        restoreStream.write(" Q\n".getBytes("ISO-8859-1"));
        restoreStream.flush();
        final PDDocumentCatalog pdfCatalog = this.pdfDocument.getDocumentCatalog();
        this.processPages(pdfCatalog.getAllPages());
        return this.pdfDocument;
    }
    
    private void collectLayoutPages(final List pages) throws IOException {
        for (final PDPage page : pages) {
            final COSBase contents = page.getCOSDictionary().getDictionaryObject(COSName.CONTENTS);
            PDResources resources = page.findResources();
            if (resources == null) {
                resources = new PDResources();
                page.setResources(resources);
            }
            final COSDictionary res = resources.getCOSDictionary();
            if (contents instanceof COSStream) {
                COSStream stream = (COSStream)contents;
                final Map objectNameMap = new TreeMap();
                stream = this.makeUniqObjectNames(objectNameMap, stream);
                this.layoutPages.add(new LayoutPage(stream, res, objectNameMap));
            }
            else {
                if (contents instanceof COSArray) {
                    throw new UnsupportedOperationException("Layout pages with COSArray currently not supported.");
                }
                throw new IOException("Contents are unknown type:" + contents.getClass().getName());
            }
        }
    }
    
    private COSStream makeUniqObjectNames(final Map objectNameMap, final COSStream stream) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream(10240);
        byte[] buf = new byte[10240];
        final InputStream is = stream.getUnfilteredStream();
        int read;
        while ((read = is.read(buf)) > -1) {
            baos.write(buf, 0, read);
        }
        buf = baos.toByteArray();
        baos = new ByteArrayOutputStream(buf.length + 100);
        final StringBuffer sbObjectName = new StringBuffer(10);
        boolean bInObjectIdent = false;
        boolean bInText = false;
        boolean bInEscape = false;
        for (int i = 0; i < buf.length; ++i) {
            final byte b = buf[i];
            if (!bInEscape) {
                if (!bInText && b == 40) {
                    bInText = true;
                }
                if (bInText && b == 41) {
                    bInText = false;
                }
                if (b == 92) {
                    bInEscape = true;
                }
                if (!bInText && !bInEscape) {
                    if (b == 47) {
                        bInObjectIdent = true;
                    }
                    else if (bInObjectIdent && Character.isWhitespace((char)b)) {
                        bInObjectIdent = false;
                        final String objectName = sbObjectName.toString().substring(1);
                        final String newObjectName = objectName + "overlay";
                        baos.write(47);
                        baos.write(newObjectName.getBytes("ISO-8859-1"));
                        objectNameMap.put(objectName, COSName.getPDFName(newObjectName));
                        sbObjectName.delete(0, sbObjectName.length());
                    }
                }
                if (bInObjectIdent) {
                    sbObjectName.append((char)b);
                    continue;
                }
            }
            else {
                bInEscape = false;
            }
            baos.write(b);
        }
        final COSDictionary streamDict = new COSDictionary();
        streamDict.setInt(COSName.LENGTH, baos.size());
        final COSStream output = new COSStream(streamDict, this.pdfDocument.getDocument().getScratchFile());
        output.setFilters(stream.getFilters());
        final OutputStream os = output.createUnfilteredStream();
        baos.writeTo(os);
        os.close();
        return output;
    }
    
    private void processPages(final List pages) throws IOException {
        for (final PDPage page : pages) {
            final COSDictionary pageDictionary = page.getCOSDictionary();
            final COSBase contents = pageDictionary.getDictionaryObject(COSName.CONTENTS);
            if (contents instanceof COSStream) {
                final COSStream contentsStream = (COSStream)contents;
                final COSArray array = new COSArray();
                array.add(contentsStream);
                this.mergePage(array, page);
                pageDictionary.setItem(COSName.CONTENTS, array);
            }
            else {
                if (!(contents instanceof COSArray)) {
                    throw new IOException("Contents are unknown type:" + contents.getClass().getName());
                }
                final COSArray contentsArray = (COSArray)contents;
                this.mergePage(contentsArray, page);
            }
            ++this.pageCount;
        }
    }
    
    private void mergePage(final COSArray array, final PDPage page) {
        final int layoutPageNum = this.pageCount % this.layoutPages.size();
        final LayoutPage layoutPage = this.layoutPages.get(layoutPageNum);
        PDResources resources = page.findResources();
        if (resources == null) {
            resources = new PDResources();
            page.setResources(resources);
        }
        final COSDictionary docResDict = resources.getCOSDictionary();
        final COSDictionary layoutResDict = layoutPage.res;
        this.mergeArray(COSName.PROC_SET, docResDict, layoutResDict);
        this.mergeDictionary(COSName.FONT, docResDict, layoutResDict, layoutPage.objectNameMap);
        this.mergeDictionary(COSName.XOBJECT, docResDict, layoutResDict, layoutPage.objectNameMap);
        this.mergeDictionary(COSName.EXT_G_STATE, docResDict, layoutResDict, layoutPage.objectNameMap);
        array.add(0, this.saveGraphicsStateStream);
        array.add(this.restoreGraphicsStateStream);
        array.add(layoutPage.contents);
    }
    
    private void mergeDictionary(final COSName name, final COSDictionary dest, final COSDictionary source, final Map objectNameMap) {
        COSDictionary destDict = (COSDictionary)dest.getDictionaryObject(name);
        final COSDictionary sourceDict = (COSDictionary)source.getDictionaryObject(name);
        if (destDict == null) {
            destDict = new COSDictionary();
            dest.setItem(name, destDict);
        }
        if (sourceDict != null) {
            for (final Map.Entry<COSName, COSBase> entry : sourceDict.entrySet()) {
                final COSName mappedKey = objectNameMap.get(entry.getKey().getName());
                if (mappedKey != null) {
                    destDict.setItem(mappedKey, entry.getValue());
                }
            }
        }
    }
    
    private void mergeArray(final COSName name, final COSDictionary dest, final COSDictionary source) {
        COSArray destDict = (COSArray)dest.getDictionaryObject(name);
        final COSArray sourceDict = (COSArray)source.getDictionaryObject(name);
        if (destDict == null) {
            destDict = new COSArray();
            dest.setItem(name, destDict);
        }
        for (int sourceDictIdx = 0; sourceDict != null && sourceDictIdx < sourceDict.size(); ++sourceDictIdx) {
            final COSBase key = sourceDict.get(sourceDictIdx);
            if (key instanceof COSName) {
                final COSName keyname = (COSName)key;
                boolean bFound = false;
                for (int destDictIdx = 0; destDictIdx < destDict.size(); ++destDictIdx) {
                    final COSBase destkey = destDict.get(destDictIdx);
                    if (destkey instanceof COSName) {
                        final COSName destkeyname = (COSName)destkey;
                        if (destkeyname.equals(keyname)) {
                            bFound = true;
                            break;
                        }
                    }
                }
                if (!bFound) {
                    destDict.add(keyname);
                }
            }
        }
    }
    
    static {
        XOBJECT = COSName.XOBJECT;
        PROC_SET = COSName.PROC_SET;
        EXT_G_STATE = COSName.EXT_G_STATE;
    }
    
    private static class LayoutPage
    {
        private final COSBase contents;
        private final COSDictionary res;
        private final Map objectNameMap;
        
        public LayoutPage(final COSBase contentsValue, final COSDictionary resValue, final Map objectNameMapValue) {
            this.contents = contentsValue;
            this.res = resValue;
            this.objectNameMap = objectNameMapValue;
        }
    }
}
