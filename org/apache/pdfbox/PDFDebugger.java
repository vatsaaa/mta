// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox;

import org.apache.pdfbox.exceptions.CryptographyException;
import org.apache.pdfbox.exceptions.InvalidPasswordException;
import org.apache.pdfbox.io.RandomAccess;
import org.apache.pdfbox.pdfviewer.PDFTreeModel;
import javax.swing.UIManager;
import java.io.InputStream;
import org.apache.pdfbox.pdfviewer.ArrayEntry;
import org.apache.pdfbox.pdfviewer.MapEntry;
import java.io.IOException;
import java.io.ByteArrayOutputStream;
import org.apache.pdfbox.cos.COSStream;
import org.apache.pdfbox.cos.COSString;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.cos.COSInteger;
import org.apache.pdfbox.cos.COSNull;
import org.apache.pdfbox.cos.COSFloat;
import org.apache.pdfbox.cos.COSBoolean;
import javax.swing.tree.TreePath;
import javax.swing.filechooser.FileFilter;
import org.apache.pdfbox.util.ExtensionFileFilter;
import javax.swing.JFileChooser;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Component;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import java.awt.Dimension;
import javax.swing.border.Border;
import javax.swing.border.BevelBorder;
import java.awt.event.WindowListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowAdapter;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreeCellRenderer;
import org.apache.pdfbox.pdfviewer.PDFTreeCellRenderer;
import javax.swing.JPanel;
import javax.swing.JMenuBar;
import javax.swing.JTree;
import javax.swing.JTextPane;
import javax.swing.JSplitPane;
import javax.swing.JScrollPane;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import org.apache.pdfbox.pdmodel.PDDocument;
import java.io.File;
import javax.swing.JFrame;

public class PDFDebugger extends JFrame
{
    private File currentDir;
    private PDDocument document;
    private static final String NONSEQ = "-nonSeq";
    private static final String PASSWORD = "-password";
    private static boolean useNonSeqParser;
    private JMenuItem aboutMenuItem;
    private JMenuItem contentsMenuItem;
    private JMenuItem copyMenuItem;
    private JMenuItem cutMenuItem;
    private JMenuItem deleteMenuItem;
    private JMenu editMenu;
    private JMenuItem exitMenuItem;
    private JMenu fileMenu;
    private JMenu helpMenu;
    private JScrollPane jScrollPane1;
    private JScrollPane jScrollPane2;
    private JSplitPane jSplitPane1;
    private JTextPane jTextPane1;
    private JTree jTree1;
    private JMenuBar menuBar;
    private JMenuItem openMenuItem;
    private JMenuItem pasteMenuItem;
    private JMenuItem saveAsMenuItem;
    private JMenuItem saveMenuItem;
    private JPanel documentPanel;
    
    public PDFDebugger() {
        this.currentDir = new File(".");
        this.document = null;
        this.documentPanel = new JPanel();
        this.initComponents();
    }
    
    private void initComponents() {
        this.jSplitPane1 = new JSplitPane();
        this.jScrollPane1 = new JScrollPane();
        this.jTree1 = new JTree();
        this.jScrollPane2 = new JScrollPane();
        this.jTextPane1 = new JTextPane();
        this.menuBar = new JMenuBar();
        this.fileMenu = new JMenu();
        this.openMenuItem = new JMenuItem();
        this.saveMenuItem = new JMenuItem();
        this.saveAsMenuItem = new JMenuItem();
        this.exitMenuItem = new JMenuItem();
        this.editMenu = new JMenu();
        this.cutMenuItem = new JMenuItem();
        this.copyMenuItem = new JMenuItem();
        this.pasteMenuItem = new JMenuItem();
        this.deleteMenuItem = new JMenuItem();
        this.helpMenu = new JMenu();
        this.contentsMenuItem = new JMenuItem();
        this.aboutMenuItem = new JMenuItem();
        this.jTree1.setCellRenderer(new PDFTreeCellRenderer());
        this.jTree1.setModel(null);
        this.setTitle("PDFBox - PDF Viewer");
        this.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(final WindowEvent evt) {
                PDFDebugger.this.exitForm(evt);
            }
        });
        this.jScrollPane1.setBorder(new BevelBorder(0));
        this.jScrollPane1.setPreferredSize(new Dimension(300, 500));
        this.jTree1.addTreeSelectionListener(new TreeSelectionListener() {
            public void valueChanged(final TreeSelectionEvent evt) {
                PDFDebugger.this.jTree1ValueChanged(evt);
            }
        });
        this.jScrollPane1.setViewportView(this.jTree1);
        this.jSplitPane1.setRightComponent(this.jScrollPane2);
        this.jScrollPane2.setPreferredSize(new Dimension(300, 500));
        this.jScrollPane2.setViewportView(this.jTextPane1);
        this.jSplitPane1.setLeftComponent(this.jScrollPane1);
        final JScrollPane documentScroller = new JScrollPane();
        documentScroller.setViewportView(this.documentPanel);
        this.getContentPane().add(this.jSplitPane1, "Center");
        this.fileMenu.setText("File");
        this.openMenuItem.setText("Open");
        this.openMenuItem.setToolTipText("Open PDF file");
        this.openMenuItem.addActionListener(new ActionListener() {
            public void actionPerformed(final ActionEvent evt) {
                PDFDebugger.this.openMenuItemActionPerformed(evt);
            }
        });
        this.fileMenu.add(this.openMenuItem);
        this.saveMenuItem.setText("Save");
        this.saveAsMenuItem.setText("Save As ...");
        this.exitMenuItem.setText("Exit");
        this.exitMenuItem.addActionListener(new ActionListener() {
            public void actionPerformed(final ActionEvent evt) {
                PDFDebugger.this.exitMenuItemActionPerformed(evt);
            }
        });
        this.fileMenu.add(this.exitMenuItem);
        this.menuBar.add(this.fileMenu);
        this.editMenu.setText("Edit");
        this.cutMenuItem.setText("Cut");
        this.editMenu.add(this.cutMenuItem);
        this.copyMenuItem.setText("Copy");
        this.editMenu.add(this.copyMenuItem);
        this.pasteMenuItem.setText("Paste");
        this.editMenu.add(this.pasteMenuItem);
        this.deleteMenuItem.setText("Delete");
        this.editMenu.add(this.deleteMenuItem);
        this.helpMenu.setText("Help");
        this.contentsMenuItem.setText("Contents");
        this.helpMenu.add(this.contentsMenuItem);
        this.aboutMenuItem.setText("About");
        this.helpMenu.add(this.aboutMenuItem);
        this.setJMenuBar(this.menuBar);
        final Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        this.setBounds((screenSize.width - 700) / 2, (screenSize.height - 600) / 2, 700, 600);
    }
    
    private void openMenuItemActionPerformed(final ActionEvent evt) {
        final JFileChooser chooser = new JFileChooser();
        chooser.setCurrentDirectory(this.currentDir);
        final ExtensionFileFilter pdfFilter = new ExtensionFileFilter(new String[] { "pdf", "PDF" }, "PDF Files");
        chooser.setFileFilter(pdfFilter);
        final int result = chooser.showOpenDialog(this);
        if (result == 0) {
            final String name = chooser.getSelectedFile().getPath();
            this.currentDir = new File(name).getParentFile();
            try {
                this.readPDFFile(name, "");
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    
    private void jTree1ValueChanged(final TreeSelectionEvent evt) {
        final TreePath path = this.jTree1.getSelectionPath();
        if (path != null) {
            try {
                final Object selectedNode = path.getLastPathComponent();
                final String data = this.convertToString(selectedNode);
                if (data != null) {
                    this.jTextPane1.setText(data);
                }
                else {
                    this.jTextPane1.setText("");
                }
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    
    private String convertToString(final Object selectedNode) {
        String data = null;
        if (selectedNode instanceof COSBoolean) {
            data = "" + ((COSBoolean)selectedNode).getValue();
        }
        else if (selectedNode instanceof COSFloat) {
            data = "" + ((COSFloat)selectedNode).floatValue();
        }
        else if (selectedNode instanceof COSNull) {
            data = "null";
        }
        else if (selectedNode instanceof COSInteger) {
            data = "" + ((COSInteger)selectedNode).intValue();
        }
        else if (selectedNode instanceof COSName) {
            data = "" + ((COSName)selectedNode).getName();
        }
        else if (selectedNode instanceof COSString) {
            data = "" + ((COSString)selectedNode).getString();
        }
        else if (selectedNode instanceof COSStream) {
            try {
                final COSStream stream = (COSStream)selectedNode;
                final InputStream ioStream = stream.getUnfilteredStream();
                final ByteArrayOutputStream byteArray = new ByteArrayOutputStream();
                final byte[] buffer = new byte[1024];
                int amountRead = 0;
                while ((amountRead = ioStream.read(buffer, 0, buffer.length)) != -1) {
                    byteArray.write(buffer, 0, amountRead);
                }
                data = byteArray.toString();
            }
            catch (IOException e) {
                e.printStackTrace();
            }
        }
        else if (selectedNode instanceof MapEntry) {
            data = this.convertToString(((MapEntry)selectedNode).getValue());
        }
        else if (selectedNode instanceof ArrayEntry) {
            data = this.convertToString(((ArrayEntry)selectedNode).getValue());
        }
        return data;
    }
    
    private void exitMenuItemActionPerformed(final ActionEvent evt) {
        if (this.document != null) {
            try {
                this.document.close();
            }
            catch (IOException io) {
                io.printStackTrace();
            }
        }
        System.exit(0);
    }
    
    private void exitForm(final WindowEvent evt) {
        if (this.document != null) {
            try {
                this.document.close();
            }
            catch (IOException io) {
                io.printStackTrace();
            }
        }
        System.exit(0);
    }
    
    public static void main(final String[] args) throws Exception {
        UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        final PDFDebugger viewer = new PDFDebugger();
        String filename = null;
        String password = "";
        for (int i = 0; i < args.length; ++i) {
            if (args[i].equals("-password")) {
                if (++i >= args.length) {
                    usage();
                }
                password = args[i];
            }
            if (args[i].equals("-nonSeq")) {
                PDFDebugger.useNonSeqParser = true;
            }
            else {
                filename = args[i];
            }
        }
        if (filename != null) {
            viewer.readPDFFile(filename, password);
        }
        viewer.setVisible(true);
    }
    
    private void readPDFFile(final String file, final String password) throws Exception {
        if (this.document != null) {
            this.document.close();
        }
        final File f = new File(file);
        this.parseDocument(f, password);
        final TreeModel model = new PDFTreeModel(this.document);
        this.jTree1.setModel(model);
        this.setTitle("PDFBox - " + f.getAbsolutePath());
    }
    
    private void parseDocument(final File file, final String password) throws IOException {
        if (PDFDebugger.useNonSeqParser) {
            this.document = PDDocument.loadNonSeq(file, null, password);
        }
        else {
            this.document = PDDocument.load(file);
            if (this.document.isEncrypted()) {
                try {
                    this.document.decrypt(password);
                }
                catch (InvalidPasswordException e2) {
                    System.err.println("Error: The document is encrypted.");
                }
                catch (CryptographyException e) {
                    e.printStackTrace();
                }
            }
        }
    }
    
    private static void usage() {
        System.err.println("usage: java -jar pdfbox-app-x.y.z.jar PDFDebugger [OPTIONS] <input-file>\n  -password <password>      Password to decrypt the document\n  -nonSeq                   Enables the new non-sequential parser\n  <input-file>              The PDF document to be loaded\n");
    }
    
    static {
        PDFDebugger.useNonSeqParser = false;
    }
}
