// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdfparser;

import org.apache.commons.logging.LogFactory;
import org.apache.pdfbox.cos.COSNull;
import org.apache.pdfbox.cos.COSBoolean;
import org.apache.pdfbox.cos.COSObject;
import org.apache.pdfbox.cos.COSArray;
import org.apache.pdfbox.cos.COSString;
import java.io.OutputStream;
import org.apache.pdfbox.cos.COSNumber;
import org.apache.pdfbox.cos.COSStream;
import org.apache.pdfbox.io.RandomAccess;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.cos.COSDictionary;
import org.apache.pdfbox.persistence.util.COSObjectKey;
import org.apache.pdfbox.cos.COSInteger;
import org.apache.pdfbox.cos.COSBase;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.BufferedInputStream;
import java.io.InputStream;
import org.apache.pdfbox.cos.COSDocument;
import org.apache.pdfbox.io.PushBackInputStream;
import org.apache.commons.logging.Log;

public abstract class BaseParser
{
    private static final Log LOG;
    private static final int E = 101;
    private static final int N = 110;
    private static final int D = 100;
    private static final int S = 115;
    private static final int T = 116;
    private static final int R = 114;
    private static final int A = 97;
    private static final int M = 109;
    private static final int O = 111;
    private static final int B = 98;
    private static final int J = 106;
    private final int strmBufLen = 2048;
    private final byte[] strmBuf;
    public static final byte[] ENDSTREAM;
    public static final byte[] ENDOBJ;
    public static final String DEF = "def";
    private static final String ENDOBJ_STRING = "endobj";
    private static final String ENDSTREAM_STRING = "endstream";
    private static final String STREAM_STRING = "stream";
    private static final String TRUE = "true";
    private static final String FALSE = "false";
    private static final String NULL = "null";
    protected static final boolean FORCE_PARSING;
    protected PushBackInputStream pdfSource;
    protected COSDocument document;
    protected final boolean forceParsing;
    
    public BaseParser() {
        this.strmBuf = new byte[2048];
        this.forceParsing = BaseParser.FORCE_PARSING;
    }
    
    public BaseParser(final InputStream input, final boolean forceParsingValue) throws IOException {
        this.strmBuf = new byte[2048];
        this.pdfSource = new PushBackInputStream(new BufferedInputStream(input, 16384), 4096);
        this.forceParsing = forceParsingValue;
    }
    
    public BaseParser(final InputStream input) throws IOException {
        this(input, BaseParser.FORCE_PARSING);
    }
    
    protected BaseParser(final byte[] input) throws IOException {
        this(new ByteArrayInputStream(input));
    }
    
    public void setDocument(final COSDocument doc) {
        this.document = doc;
    }
    
    private static boolean isHexDigit(final char ch) {
        return (ch >= '0' && ch <= '9') || (ch >= 'a' && ch <= 'f') || (ch >= 'A' && ch <= 'F');
    }
    
    private COSBase parseCOSDictionaryValue() throws IOException {
        COSBase retval = null;
        final COSBase number = this.parseDirObject();
        this.skipSpaces();
        final char next = (char)this.pdfSource.peek();
        if (next >= '0' && next <= '9') {
            final COSBase generationNumber = this.parseDirObject();
            this.skipSpaces();
            final char r = (char)this.pdfSource.read();
            if (r != 'R') {
                throw new IOException("expected='R' actual='" + r + "' " + this.pdfSource);
            }
            final COSObjectKey key = new COSObjectKey(((COSInteger)number).intValue(), ((COSInteger)generationNumber).intValue());
            retval = this.document.getObjectFromPool(key);
        }
        else {
            retval = number;
        }
        return retval;
    }
    
    protected COSDictionary parseCOSDictionary() throws IOException {
        char c = (char)this.pdfSource.read();
        if (c != '<') {
            throw new IOException("expected='<' actual='" + c + "'");
        }
        c = (char)this.pdfSource.read();
        if (c != '<') {
            throw new IOException("expected='<' actual='" + c + "' " + this.pdfSource);
        }
        this.skipSpaces();
        final COSDictionary obj = new COSDictionary();
        boolean done = false;
        while (!done) {
            this.skipSpaces();
            c = (char)this.pdfSource.peek();
            if (c == '>') {
                done = true;
            }
            else if (c != '/') {
                BaseParser.LOG.warn("Invalid dictionary, found: '" + c + "' but expected: '/'");
                int read;
                for (read = this.pdfSource.read(); read != -1 && read != 47 && read != 62; read = this.pdfSource.read()) {
                    if (read == 101) {
                        read = this.pdfSource.read();
                        if (read == 110) {
                            read = this.pdfSource.read();
                            if (read == 100) {
                                read = this.pdfSource.read();
                                if (read == 115) {
                                    read = this.pdfSource.read();
                                    if (read == 116) {
                                        read = this.pdfSource.read();
                                        if (read == 114) {
                                            read = this.pdfSource.read();
                                            if (read == 101) {
                                                read = this.pdfSource.read();
                                                if (read == 97) {
                                                    read = this.pdfSource.read();
                                                    if (read == 109) {
                                                        return obj;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                else if (read == 111) {
                                    read = this.pdfSource.read();
                                    if (read == 98) {
                                        read = this.pdfSource.read();
                                        if (read == 106) {
                                            return obj;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                if (read == -1) {
                    return obj;
                }
                this.pdfSource.unread(read);
            }
            else {
                final COSName key = this.parseCOSName();
                final COSBase value = this.parseCOSDictionaryValue();
                this.skipSpaces();
                if ((char)this.pdfSource.peek() == 'd') {
                    final String potentialDEF = this.readString();
                    if (!potentialDEF.equals("def")) {
                        this.pdfSource.unread(potentialDEF.getBytes("ISO-8859-1"));
                    }
                    else {
                        this.skipSpaces();
                    }
                }
                if (value == null) {
                    BaseParser.LOG.warn("Bad Dictionary Declaration " + this.pdfSource);
                }
                else {
                    obj.setItem(key, value);
                }
            }
        }
        char ch = (char)this.pdfSource.read();
        if (ch != '>') {
            throw new IOException("expected='>' actual='" + ch + "'");
        }
        ch = (char)this.pdfSource.read();
        if (ch != '>') {
            throw new IOException("expected='>' actual='" + ch + "'");
        }
        return obj;
    }
    
    protected COSStream parseCOSStream(final COSDictionary dic, final RandomAccess file) throws IOException {
        final COSStream stream = new COSStream(dic, file);
        OutputStream out = null;
        try {
            final String streamString = this.readString();
            if (!streamString.equals("stream")) {
                throw new IOException("expected='stream' actual='" + streamString + "'");
            }
            int whitespace;
            for (whitespace = this.pdfSource.read(); whitespace == 32; whitespace = this.pdfSource.read()) {}
            if (whitespace == 13) {
                whitespace = this.pdfSource.read();
                if (whitespace != 10) {
                    this.pdfSource.unread(whitespace);
                }
            }
            else if (whitespace != 10) {
                this.pdfSource.unread(whitespace);
            }
            final COSBase streamLength = dic.getItem(COSName.LENGTH);
            out = stream.createFilteredStream(streamLength);
            int length = -1;
            if (streamLength instanceof COSNumber) {
                length = ((COSNumber)streamLength).intValue();
            }
            if (length == -1) {
                this.readUntilEndStream(out);
            }
            else {
                int readCount;
                for (int left = length; left > 0; left -= readCount) {
                    final int chunk = Math.min(left, 2048);
                    readCount = this.pdfSource.read(this.strmBuf, 0, chunk);
                    if (readCount == -1) {
                        break;
                    }
                    out.write(this.strmBuf, 0, readCount);
                }
            }
            this.skipSpaces();
            String endStream = this.readString();
            if (!endStream.equals("endstream")) {
                if (endStream.startsWith("endobj")) {
                    final byte[] endobjarray = endStream.getBytes("ISO-8859-1");
                    this.pdfSource.unread(endobjarray);
                }
                else if (endStream.startsWith("endstream")) {
                    final String extra = endStream.substring(9, endStream.length());
                    endStream = endStream.substring(0, 9);
                    final byte[] array = extra.getBytes("ISO-8859-1");
                    this.pdfSource.unread(array);
                }
                else {
                    this.readUntilEndStream(out);
                    endStream = this.readString();
                    if (!endStream.equals("endstream")) {
                        throw new IOException("expected='endstream' actual='" + endStream + "' " + this.pdfSource);
                    }
                }
            }
        }
        finally {
            if (out != null) {
                out.close();
            }
        }
        return stream;
    }
    
    private void readUntilEndStream(final OutputStream out) throws IOException {
        int charMatchCount = 0;
        byte[] keyw = BaseParser.ENDSTREAM;
        final int quickTestOffset = 5;
        int bufSize;
        while ((bufSize = this.pdfSource.read(this.strmBuf, charMatchCount, 2048 - charMatchCount)) > 0) {
            bufSize += charMatchCount;
            int bIdx = charMatchCount;
            final int maxQuicktestIdx = bufSize - 5;
            while (bIdx < bufSize) {
                Label_0191: {
                    final int quickTestIdx;
                    if (charMatchCount == 0 && (quickTestIdx = bIdx + 5) < maxQuicktestIdx) {
                        final byte ch = this.strmBuf[quickTestIdx];
                        if (ch > 116 || ch < 97) {
                            bIdx = quickTestIdx;
                            break Label_0191;
                        }
                    }
                    final byte ch = this.strmBuf[bIdx];
                    if (ch == keyw[charMatchCount]) {
                        if (++charMatchCount == keyw.length) {
                            ++bIdx;
                            break;
                        }
                    }
                    else if (charMatchCount == 3 && ch == BaseParser.ENDOBJ[charMatchCount]) {
                        keyw = BaseParser.ENDOBJ;
                        ++charMatchCount;
                    }
                    else {
                        charMatchCount = ((ch == 101) ? 1 : ((ch == 110 && charMatchCount == 7) ? 2 : 0));
                        keyw = BaseParser.ENDSTREAM;
                    }
                }
                ++bIdx;
            }
            final int contentBytes = Math.max(0, bIdx - charMatchCount);
            if (contentBytes > 0) {
                out.write(this.strmBuf, 0, contentBytes);
            }
            if (charMatchCount == keyw.length) {
                this.pdfSource.unread(this.strmBuf, contentBytes, bufSize - contentBytes);
                break;
            }
            System.arraycopy(keyw, 0, this.strmBuf, 0, charMatchCount);
        }
    }
    
    private int checkForMissingCloseParen(final int bracesParameter) throws IOException {
        int braces = bracesParameter;
        final byte[] nextThreeBytes = new byte[3];
        final int amountRead = this.pdfSource.read(nextThreeBytes);
        if (amountRead == 3 && ((nextThreeBytes[0] == 13 && nextThreeBytes[1] == 10 && nextThreeBytes[2] == 47) || (nextThreeBytes[0] == 13 && nextThreeBytes[1] == 47))) {
            braces = 0;
        }
        if (amountRead > 0) {
            this.pdfSource.unread(nextThreeBytes, 0, amountRead);
        }
        return braces;
    }
    
    protected COSString parseCOSString() throws IOException {
        final char nextChar = (char)this.pdfSource.read();
        final COSString retval = new COSString();
        if (nextChar == '(') {
            final char openBrace = '(';
            final char closeBrace = ')';
            int braces = 1;
            int c = this.pdfSource.read();
            while (braces > 0 && c != -1) {
                final char ch = (char)c;
                int nextc = -2;
                if (ch == closeBrace) {
                    --braces;
                    braces = this.checkForMissingCloseParen(braces);
                    if (braces != 0) {
                        retval.append(ch);
                    }
                }
                else if (ch == openBrace) {
                    ++braces;
                    retval.append(ch);
                }
                else if (ch == '\\') {
                    final char next = (char)this.pdfSource.read();
                    switch (next) {
                        case 'n': {
                            retval.append(10);
                            break;
                        }
                        case 'r': {
                            retval.append(13);
                            break;
                        }
                        case 't': {
                            retval.append(9);
                            break;
                        }
                        case 'b': {
                            retval.append(8);
                            break;
                        }
                        case 'f': {
                            retval.append(12);
                            break;
                        }
                        case ')': {
                            braces = this.checkForMissingCloseParen(braces);
                            if (braces != 0) {
                                retval.append(next);
                                break;
                            }
                            retval.append(92);
                            break;
                        }
                        case '(':
                        case '\\': {
                            retval.append(next);
                            break;
                        }
                        case '\n':
                        case '\r': {
                            for (c = this.pdfSource.read(); this.isEOL(c) && c != -1; c = this.pdfSource.read()) {}
                            nextc = c;
                            break;
                        }
                        case '0':
                        case '1':
                        case '2':
                        case '3':
                        case '4':
                        case '5':
                        case '6':
                        case '7': {
                            final StringBuffer octal = new StringBuffer();
                            octal.append(next);
                            c = this.pdfSource.read();
                            char digit = (char)c;
                            if (digit >= '0' && digit <= '7') {
                                octal.append(digit);
                                c = this.pdfSource.read();
                                digit = (char)c;
                                if (digit >= '0' && digit <= '7') {
                                    octal.append(digit);
                                }
                                else {
                                    nextc = c;
                                }
                            }
                            else {
                                nextc = c;
                            }
                            int character = 0;
                            try {
                                character = Integer.parseInt(octal.toString(), 8);
                            }
                            catch (NumberFormatException e) {
                                throw new IOException("Error: Expected octal character, actual='" + (Object)octal + "'");
                            }
                            retval.append(character);
                            break;
                        }
                        default: {
                            retval.append(92);
                            retval.append(next);
                            break;
                        }
                    }
                }
                else {
                    retval.append(ch);
                }
                if (nextc != -2) {
                    c = nextc;
                }
                else {
                    c = this.pdfSource.read();
                }
            }
            if (c != -1) {
                this.pdfSource.unread(c);
            }
            return retval;
        }
        if (nextChar == '<') {
            return this.parseCOSHexString();
        }
        throw new IOException("parseCOSString string should start with '(' or '<' and not '" + nextChar + "' " + this.pdfSource);
    }
    
    private final COSString parseCOSHexString() throws IOException {
        final StringBuilder sBuf = new StringBuilder();
        while (true) {
            final int c = this.pdfSource.read();
            if (isHexDigit((char)c)) {
                sBuf.append((char)c);
            }
            else {
                if (c == 62) {
                    return COSString.createFromHexString(sBuf.toString(), this.forceParsing);
                }
                if (c < 0) {
                    throw new IOException("Missing closing bracket for hex string. Reached EOS.");
                }
                if (c == 32 || c == 10 || c == 9 || c == 13 || c == 8) {
                    continue;
                }
                if (c == 12) {
                    continue;
                }
                throw new IOException("Not allowed character in hex string; char code: " + c);
            }
        }
    }
    
    protected COSArray parseCOSArray() throws IOException {
        final char ch = (char)this.pdfSource.read();
        if (ch != '[') {
            throw new IOException("expected='[' actual='" + ch + "'");
        }
        final COSArray po = new COSArray();
        COSBase pbo = null;
        this.skipSpaces();
        int i = 0;
        while ((i = this.pdfSource.peek()) > 0 && (char)i != ']') {
            pbo = this.parseDirObject();
            if (pbo instanceof COSObject) {
                if (po.get(po.size() - 1) instanceof COSInteger) {
                    final COSInteger genNumber = (COSInteger)po.remove(po.size() - 1);
                    if (po.get(po.size() - 1) instanceof COSInteger) {
                        final COSInteger number = (COSInteger)po.remove(po.size() - 1);
                        final COSObjectKey key = new COSObjectKey(number.intValue(), genNumber.intValue());
                        pbo = this.document.getObjectFromPool(key);
                    }
                    else {
                        pbo = null;
                    }
                }
                else {
                    pbo = null;
                }
            }
            if (pbo != null) {
                po.add(pbo);
            }
            else {
                BaseParser.LOG.warn("Corrupt object reference");
                final String isThisTheEnd = this.readString();
                this.pdfSource.unread(isThisTheEnd.getBytes("ISO-8859-1"));
                if ("endobj".equals(isThisTheEnd) || "endstream".equals(isThisTheEnd)) {
                    return po;
                }
            }
            this.skipSpaces();
        }
        this.pdfSource.read();
        this.skipSpaces();
        return po;
    }
    
    protected boolean isEndOfName(final char ch) {
        return ch == ' ' || ch == '\r' || ch == '\n' || ch == '\t' || ch == '>' || ch == '<' || ch == '[' || ch == '/' || ch == ']' || ch == ')' || ch == '(' || ch == -1;
    }
    
    protected COSName parseCOSName() throws IOException {
        COSName retval = null;
        int c = this.pdfSource.read();
        if ((char)c != '/') {
            throw new IOException("expected='/' actual='" + (char)c + "'-" + c + " " + this.pdfSource);
        }
        final StringBuilder buffer = new StringBuilder();
        c = this.pdfSource.read();
        while (c != -1) {
            final char ch = (char)c;
            if (ch == '#') {
                final char ch2 = (char)this.pdfSource.read();
                final char ch3 = (char)this.pdfSource.read();
                if (isHexDigit(ch2) && isHexDigit(ch3)) {
                    final String hex = "" + ch2 + ch3;
                    try {
                        buffer.append((char)Integer.parseInt(hex, 16));
                    }
                    catch (NumberFormatException e) {
                        throw new IOException("Error: expected hex number, actual='" + hex + "'");
                    }
                    c = this.pdfSource.read();
                }
                else {
                    this.pdfSource.unread(ch3);
                    c = ch2;
                    buffer.append(ch);
                }
            }
            else {
                if (this.isEndOfName(ch)) {
                    break;
                }
                buffer.append(ch);
                c = this.pdfSource.read();
            }
        }
        if (c != -1) {
            this.pdfSource.unread(c);
        }
        retval = COSName.getPDFName(buffer.toString());
        return retval;
    }
    
    protected COSBoolean parseBoolean() throws IOException {
        COSBoolean retval = null;
        final char c = (char)this.pdfSource.peek();
        if (c == 't') {
            final String trueString = new String(this.pdfSource.readFully(4), "ISO-8859-1");
            if (!trueString.equals("true")) {
                throw new IOException("Error parsing boolean: expected='true' actual='" + trueString + "'");
            }
            retval = COSBoolean.TRUE;
        }
        else {
            if (c != 'f') {
                throw new IOException("Error parsing boolean expected='t or f' actual='" + c + "'");
            }
            final String falseString = new String(this.pdfSource.readFully(5), "ISO-8859-1");
            if (!falseString.equals("false")) {
                throw new IOException("Error parsing boolean: expected='true' actual='" + falseString + "'");
            }
            retval = COSBoolean.FALSE;
        }
        return retval;
    }
    
    protected COSBase parseDirObject() throws IOException {
        COSBase retval = null;
        this.skipSpaces();
        final int nextByte = this.pdfSource.peek();
        char c = (char)nextByte;
        switch (c) {
            case '<': {
                final int leftBracket = this.pdfSource.read();
                c = (char)this.pdfSource.peek();
                this.pdfSource.unread(leftBracket);
                if (c == '<') {
                    retval = this.parseCOSDictionary();
                    this.skipSpaces();
                    break;
                }
                retval = this.parseCOSString();
                break;
            }
            case '[': {
                retval = this.parseCOSArray();
                break;
            }
            case '(': {
                retval = this.parseCOSString();
                break;
            }
            case '/': {
                retval = this.parseCOSName();
                break;
            }
            case 'n': {
                final String nullString = this.readString();
                if (!nullString.equals("null")) {
                    throw new IOException("Expected='null' actual='" + nullString + "'");
                }
                retval = COSNull.NULL;
                break;
            }
            case 't': {
                final String trueString = new String(this.pdfSource.readFully(4), "ISO-8859-1");
                if (trueString.equals("true")) {
                    retval = COSBoolean.TRUE;
                    break;
                }
                throw new IOException("expected true actual='" + trueString + "' " + this.pdfSource);
            }
            case 'f': {
                final String falseString = new String(this.pdfSource.readFully(5), "ISO-8859-1");
                if (falseString.equals("false")) {
                    retval = COSBoolean.FALSE;
                    break;
                }
                throw new IOException("expected false actual='" + falseString + "' " + this.pdfSource);
            }
            case 'R': {
                this.pdfSource.read();
                retval = new COSObject(null);
                break;
            }
            case '\uffff': {
                return null;
            }
            default: {
                if (Character.isDigit(c) || c == '-' || c == '+' || c == '.') {
                    final StringBuilder buf = new StringBuilder();
                    int ic;
                    for (ic = this.pdfSource.read(), c = (char)ic; Character.isDigit(c) || c == '-' || c == '+' || c == '.' || c == 'E' || c == 'e'; c = (char)ic) {
                        buf.append(c);
                        ic = this.pdfSource.read();
                    }
                    if (ic != -1) {
                        this.pdfSource.unread(ic);
                    }
                    retval = COSNumber.get(buf.toString());
                    break;
                }
                final String badString = this.readString();
                if (badString == null || badString.length() == 0) {
                    final int peek = this.pdfSource.peek();
                    throw new IOException("Unknown dir object c='" + c + "' cInt=" + (int)c + " peek='" + (char)peek + "' peekInt=" + peek + " " + this.pdfSource.getOffset());
                }
                if ("endobj".equals(badString) || "endstream".equals(badString)) {
                    this.pdfSource.unread(badString.getBytes("ISO-8859-1"));
                    break;
                }
                break;
            }
        }
        return retval;
    }
    
    protected String readString() throws IOException {
        this.skipSpaces();
        final StringBuilder buffer = new StringBuilder();
        int c;
        for (c = this.pdfSource.read(); !this.isEndOfName((char)c) && !this.isClosing(c) && c != -1; c = this.pdfSource.read()) {
            buffer.append((char)c);
        }
        if (c != -1) {
            this.pdfSource.unread(c);
        }
        return buffer.toString();
    }
    
    protected String readExpectedString(final String theString) throws IOException {
        int c;
        for (c = this.pdfSource.read(); this.isWhitespace(c) && c != -1; c = this.pdfSource.read()) {}
        final StringBuilder buffer = new StringBuilder(theString.length());
        for (int charsRead = 0; !this.isEOL(c) && c != -1 && charsRead < theString.length(); ++charsRead, c = this.pdfSource.read()) {
            final char next = (char)c;
            buffer.append(next);
            if (theString.charAt(charsRead) != next) {
                this.pdfSource.unread(buffer.toString().getBytes("ISO-8859-1"));
                throw new IOException("Error: Expected to read '" + theString + "' instead started reading '" + buffer.toString() + "'");
            }
        }
        while (this.isEOL(c) && c != -1) {
            c = this.pdfSource.read();
        }
        if (c != -1) {
            this.pdfSource.unread(c);
        }
        return buffer.toString();
    }
    
    protected String readString(final int length) throws IOException {
        this.skipSpaces();
        int c;
        StringBuilder buffer;
        for (c = this.pdfSource.read(), buffer = new StringBuilder(length); !this.isWhitespace(c) && !this.isClosing(c) && c != -1 && buffer.length() < length && c != 91 && c != 60 && c != 40 && c != 47; c = this.pdfSource.read()) {
            buffer.append((char)c);
        }
        if (c != -1) {
            this.pdfSource.unread(c);
        }
        return buffer.toString();
    }
    
    protected boolean isClosing() throws IOException {
        return this.isClosing(this.pdfSource.peek());
    }
    
    protected boolean isClosing(final int c) {
        return c == 93;
    }
    
    protected String readLine() throws IOException {
        if (this.pdfSource.isEOF()) {
            throw new IOException("Error: End-of-File, expected line");
        }
        final StringBuilder buffer = new StringBuilder(11);
        int c;
        while ((c = this.pdfSource.read()) != -1 && !this.isEOL(c)) {
            buffer.append((char)c);
        }
        return buffer.toString();
    }
    
    protected boolean isEOL() throws IOException {
        return this.isEOL(this.pdfSource.peek());
    }
    
    protected boolean isEOL(final int c) {
        return c == 10 || c == 13;
    }
    
    protected boolean isWhitespace() throws IOException {
        return this.isWhitespace(this.pdfSource.peek());
    }
    
    protected boolean isWhitespace(final int c) {
        return c == 0 || c == 9 || c == 12 || c == 10 || c == 13 || c == 32;
    }
    
    protected void skipSpaces() throws IOException {
        int c;
        for (c = this.pdfSource.read(); c == 0 || c == 9 || c == 12 || c == 10 || c == 13 || c == 32 || c == 37; c = this.pdfSource.read()) {
            if (c == 37) {
                for (c = this.pdfSource.read(); !this.isEOL(c) && c != -1; c = this.pdfSource.read()) {}
            }
            else {}
        }
        if (c != -1) {
            this.pdfSource.unread(c);
        }
    }
    
    protected int readInt() throws IOException {
        this.skipSpaces();
        int retval = 0;
        int lastByte = 0;
        final StringBuffer intBuffer = new StringBuffer();
        while ((lastByte = this.pdfSource.read()) != 32 && lastByte != 10 && lastByte != 13 && lastByte != 60 && lastByte != 0 && lastByte != -1) {
            intBuffer.append((char)lastByte);
        }
        if (lastByte != -1) {
            this.pdfSource.unread(lastByte);
        }
        try {
            retval = Integer.parseInt(intBuffer.toString());
        }
        catch (NumberFormatException e) {
            this.pdfSource.unread(intBuffer.toString().getBytes("ISO-8859-1"));
            throw new IOException("Error: Expected an integer type, actual='" + (Object)intBuffer + "'");
        }
        return retval;
    }
    
    static {
        LOG = LogFactory.getLog(BaseParser.class);
        ENDSTREAM = new byte[] { 101, 110, 100, 115, 116, 114, 101, 97, 109 };
        ENDOBJ = new byte[] { 101, 110, 100, 111, 98, 106 };
        FORCE_PARSING = Boolean.getBoolean("org.apache.pdfbox.forceParsing");
    }
}
