// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdfparser;

import java.util.Set;
import org.apache.pdfbox.cos.COSInteger;
import org.apache.pdfbox.cos.COSDictionary;
import org.apache.pdfbox.cos.COSObject;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.cos.COSArray;
import org.apache.pdfbox.cos.COSUnread;
import org.apache.pdfbox.cos.COSString;
import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSFloat;
import org.apache.pdfbox.cos.COSNumber;
import java.util.ArrayList;
import org.apache.pdfbox.pdmodel.PDDocument;
import java.util.Iterator;
import org.apache.pdfbox.persistence.util.COSObjectKey;
import org.apache.pdfbox.cos.COSDocument;
import java.io.IOException;
import org.apache.pdfbox.io.RandomAccessFile;
import java.io.File;
import org.apache.pdfbox.pdmodel.ConformingPDDocument;
import org.apache.pdfbox.pdmodel.common.XrefEntry;
import java.util.List;
import org.apache.pdfbox.io.RandomAccess;

public class ConformingPDFParser extends BaseParser
{
    protected RandomAccess inputFile;
    List<XrefEntry> xrefEntries;
    private long currentOffset;
    private ConformingPDDocument doc;
    private boolean throwNonConformingException;
    private boolean recursivlyRead;
    
    public ConformingPDFParser(final File inputFile) throws IOException {
        this.doc = null;
        this.throwNonConformingException = true;
        this.recursivlyRead = true;
        this.inputFile = new RandomAccessFile(inputFile, "r");
    }
    
    public void parse() throws IOException {
        this.document = new COSDocument();
        this.doc = new ConformingPDDocument(this.document);
        this.currentOffset = this.inputFile.length() - 1L;
        final long xRefTableLocation = this.parseTrailerInformation();
        this.currentOffset = xRefTableLocation;
        this.parseXrefTable();
        final boolean oldValue = this.recursivlyRead;
        this.recursivlyRead = false;
        final List<COSObjectKey> keys = this.doc.getObjectKeysFromPool();
        for (final COSObjectKey key : keys) {
            this.getObject(key.getNumber(), key.getGeneration());
        }
        this.recursivlyRead = oldValue;
    }
    
    public COSDocument getDocument() throws IOException {
        if (this.document == null) {
            throw new IOException("You must call parse() before calling getDocument()");
        }
        return this.document;
    }
    
    public PDDocument getPDDocument() throws IOException {
        return this.doc;
    }
    
    private boolean parseXrefTable() throws IOException {
        final String currentLine = this.readLine();
        if (this.throwNonConformingException && !"xref".equals(currentLine)) {
            throw new AssertionError((Object)("xref table not found.\nExpected: xref\nFound: " + currentLine));
        }
        int objectNumber = this.readInt();
        final int entries = this.readInt();
        this.xrefEntries = new ArrayList<XrefEntry>(entries);
        for (int i = 0; i < entries; ++i) {
            this.xrefEntries.add(new XrefEntry(objectNumber++, this.readInt(), this.readInt(), this.readLine()));
        }
        return true;
    }
    
    protected long parseTrailerInformation() throws IOException, NumberFormatException {
        long xrefLocation = -1L;
        this.consumeWhitespaceBackwards();
        String currentLine = this.readLineBackwards();
        if (this.throwNonConformingException && !"%%EOF".equals(currentLine)) {
            throw new AssertionError((Object)("Invalid EOF marker.\nExpected: %%EOF\nFound: " + currentLine));
        }
        xrefLocation = this.readLongBackwards();
        currentLine = this.readLineBackwards();
        if (this.throwNonConformingException && !"startxref".equals(currentLine)) {
            throw new AssertionError((Object)("Invalid trailer.\nExpected: startxref\nFound: " + currentLine));
        }
        this.document.setTrailer(this.readDictionaryBackwards());
        this.consumeWhitespaceBackwards();
        currentLine = this.readLineBackwards();
        if (this.throwNonConformingException && !"trailer".equals(currentLine)) {
            throw new AssertionError((Object)("Invalid trailer.\nExpected: trailer\nFound: " + currentLine));
        }
        return xrefLocation;
    }
    
    protected byte readByteBackwards() throws IOException {
        this.inputFile.seek(this.currentOffset);
        final byte singleByte = (byte)this.inputFile.read();
        --this.currentOffset;
        return singleByte;
    }
    
    protected byte readByte() throws IOException {
        this.inputFile.seek(this.currentOffset);
        final byte singleByte = (byte)this.inputFile.read();
        ++this.currentOffset;
        return singleByte;
    }
    
    protected String readBackwardUntilWhitespace() throws IOException {
        final StringBuilder sb = new StringBuilder();
        for (byte singleByte = this.readByteBackwards(); !this.isWhitespace(singleByte); singleByte = this.readByteBackwards()) {
            sb.insert(0, (char)singleByte);
        }
        return sb.toString();
    }
    
    protected byte consumeWhitespaceBackwards() throws IOException {
        this.inputFile.seek(this.currentOffset);
        byte singleByte = (byte)this.inputFile.read();
        if (!this.isWhitespace(singleByte)) {
            return singleByte;
        }
        while (this.isWhitespace(singleByte)) {
            singleByte = this.readByteBackwards();
        }
        ++this.currentOffset;
        return singleByte;
    }
    
    protected byte consumeWhitespace() throws IOException {
        this.inputFile.seek(this.currentOffset);
        byte singleByte = (byte)this.inputFile.read();
        if (!this.isWhitespace(singleByte)) {
            return singleByte;
        }
        while (this.isWhitespace(singleByte)) {
            singleByte = this.readByte();
        }
        --this.currentOffset;
        return singleByte;
    }
    
    protected long readLongBackwards() throws IOException, NumberFormatException {
        final StringBuilder sb = new StringBuilder();
        this.consumeWhitespaceBackwards();
        for (byte singleByte = this.readByteBackwards(); !this.isWhitespace(singleByte); singleByte = this.readByteBackwards()) {
            sb.insert(0, (char)singleByte);
        }
        if (sb.length() == 0) {
            throw new AssertionError((Object)("Number not found.  Expected number at offset: " + this.currentOffset));
        }
        return Long.parseLong(sb.toString());
    }
    
    @Override
    protected int readInt() throws IOException {
        final StringBuilder sb = new StringBuilder();
        this.consumeWhitespace();
        for (byte singleByte = this.readByte(); !this.isWhitespace(singleByte); singleByte = this.readByte()) {
            sb.append((char)singleByte);
        }
        if (sb.length() == 0) {
            throw new AssertionError((Object)("Number not found.  Expected number at offset: " + this.currentOffset));
        }
        return Integer.parseInt(sb.toString());
    }
    
    protected COSNumber readNumber() throws IOException {
        final StringBuilder sb = new StringBuilder();
        this.consumeWhitespace();
        for (byte singleByte = this.readByte(); !this.isWhitespace(singleByte); singleByte = this.readByte()) {
            sb.append((char)singleByte);
        }
        if (sb.length() == 0) {
            throw new AssertionError((Object)("Number not found.  Expected number at offset: " + this.currentOffset));
        }
        return this.parseNumber(sb.toString());
    }
    
    protected COSNumber parseNumber(final String number) throws IOException {
        if (number.matches("^[0-9]+$")) {
            return COSNumber.get(number);
        }
        return new COSFloat(Float.parseFloat(number));
    }
    
    protected COSBase processCosObject(final String string) throws IOException {
        if (string != null && string.endsWith(">")) {
            return COSString.createFromHexString(string.replaceAll("^<", "").replaceAll(">$", ""));
        }
        return null;
    }
    
    protected COSBase readObjectBackwards() throws IOException {
        COSBase obj = null;
        this.consumeWhitespaceBackwards();
        String lastSection = this.readBackwardUntilWhitespace();
        if ("R".equals(lastSection)) {
            final long gen = this.readLongBackwards();
            final long number = this.readLongBackwards();
            this.doc.putObjectInPool(new COSUnread(), number, gen);
            obj = new COSUnread(number, gen, this);
        }
        else {
            if (">>".equals(lastSection)) {
                throw new RuntimeException("Not yet implemented");
            }
            if (lastSection != null && lastSection.endsWith("]")) {
                final COSArray array = new COSArray();
                for (lastSection = lastSection.replaceAll("]$", ""); !lastSection.startsWith("["); lastSection = this.readBackwardUntilWhitespace()) {
                    if (lastSection.matches("^\\s*<.*>\\s*$")) {
                        array.add(COSString.createFromHexString(lastSection.replaceAll("^\\s*<", "").replaceAll(">\\s*$", "")));
                    }
                }
                lastSection = lastSection.replaceAll("^\\[", "");
                if (lastSection.matches("^\\s*<.*>\\s*$")) {
                    array.add(COSString.createFromHexString(lastSection.replaceAll("^\\s*<", "").replaceAll(">\\s*$", "")));
                }
                obj = array;
            }
            else if (lastSection != null && lastSection.endsWith(">")) {
                obj = this.processCosObject(lastSection);
            }
            else {
                try {
                    Long.parseLong(lastSection);
                    obj = COSNumber.get(lastSection);
                }
                catch (NumberFormatException e) {
                    throw new RuntimeException("Not yet implemented");
                }
            }
        }
        return obj;
    }
    
    protected COSName readNameBackwards() throws IOException {
        String name = this.readBackwardUntilWhitespace();
        name = name.replaceAll("^/", "");
        return COSName.getPDFName(name);
    }
    
    public COSBase getObject(final long objectNumber, final long generation) throws IOException {
        final XrefEntry entry = this.xrefEntries.get((int)objectNumber);
        this.currentOffset = entry.getByteOffset();
        return this.readObject(objectNumber, generation);
    }
    
    public COSBase readObject(final long objectNumber, final long generation) throws IOException {
        if (this.document != null && this.recursivlyRead) {
            final COSBase obj = this.doc.getObjectFromPool(objectNumber, generation);
            if (obj != null) {
                return obj;
            }
        }
        final int actualObjectNumber = this.readInt();
        if (objectNumber != actualObjectNumber && this.throwNonConformingException) {
            throw new AssertionError((Object)("Object numer expected was " + objectNumber + " but actual was " + actualObjectNumber));
        }
        this.consumeWhitespace();
        final int actualGeneration = this.readInt();
        if (generation != actualGeneration && this.throwNonConformingException) {
            throw new AssertionError((Object)("Generation expected was " + generation + " but actual was " + actualGeneration));
        }
        this.consumeWhitespace();
        final String obj2 = this.readWord();
        if (!"obj".equals(obj2) && this.throwNonConformingException) {
            throw new AssertionError((Object)("Expected keyword 'obj' but found " + obj2));
        }
        this.doc.putObjectInPool(new COSObject(null), objectNumber, generation);
        final COSBase object = this.readObject();
        this.doc.putObjectInPool(object, objectNumber, generation);
        return object;
    }
    
    protected COSBase readObject() throws IOException {
        this.consumeWhitespace();
        String string = this.readWord();
        if (string.startsWith("<<")) {
            final COSDictionary dictionary = new COSDictionary();
            boolean atEndOfDictionary = false;
            string = string.replaceAll("^<<", "");
            if ("".equals(string) || string.matches("^\\w$")) {
                string = this.readWord().trim();
            }
            while (!atEndOfDictionary) {
                final COSName name = COSName.getPDFName(string);
                final COSBase object = this.readObject();
                dictionary.setItem(name, object);
                final byte singleByte = this.consumeWhitespace();
                if (singleByte == 62) {
                    this.readByte();
                    atEndOfDictionary = true;
                }
                if (!atEndOfDictionary) {
                    string = this.readWord().trim();
                }
            }
            return dictionary;
        }
        if (string.startsWith("/")) {
            final COSBase name2 = COSName.getPDFName(string);
            return name2;
        }
        if (string.startsWith("-")) {
            return this.parseNumber(string);
        }
        if (string.charAt(0) >= '0' && string.charAt(0) <= '9') {
            final long tempOffset = this.currentOffset;
            this.consumeWhitespace();
            String tempString = this.readWord();
            if (!tempString.matches("^[0-9]+$")) {
                this.currentOffset = tempOffset;
                return this.parseNumber(string);
            }
            tempString = this.readWord();
            if (!"R".equals(tempString)) {
                this.currentOffset = tempOffset;
                return this.parseNumber(string);
            }
            this.currentOffset = tempOffset;
            final int number = Integer.parseInt(string);
            final int gen = this.readInt();
            final String r = this.readWord();
            if (!"R".equals(r) && this.throwNonConformingException) {
                throw new AssertionError((Object)("Expected keyword 'R' but found " + r));
            }
            if (this.recursivlyRead) {
                final long tempLocation = this.currentOffset;
                this.currentOffset = this.xrefEntries.get(number).getByteOffset();
                final COSBase returnValue = this.readObject(number, gen);
                this.currentOffset = tempLocation;
                return returnValue;
            }
            final COSObject obj = new COSObject(new COSUnread());
            obj.setObjectNumber(COSInteger.get(number));
            obj.setGenerationNumber(COSInteger.get(gen));
            return obj;
        }
        else if (string.startsWith("]")) {
            if ("]".equals(string)) {
                return null;
            }
            final int oldLength = string.length();
            this.currentOffset -= oldLength;
            return null;
        }
        else {
            if (string.startsWith("[")) {
                final int oldLength = string.length();
                string = "[";
                this.currentOffset -= oldLength - string.length() + 1;
                final COSArray array = new COSArray();
                for (COSBase object2 = this.readObject(); object2 != null; object2 = this.readObject()) {
                    array.add(object2);
                }
                return array;
            }
            if (string.startsWith("(")) {
                final StringBuilder sb = new StringBuilder(string.substring(1));
                for (byte singleByte2 = this.readByte(); singleByte2 != 41; singleByte2 = this.readByte()) {
                    sb.append((char)singleByte2);
                }
                return new COSString(sb.toString());
            }
            throw new RuntimeException("Not yet implemented: " + string + " loation=" + this.currentOffset);
        }
    }
    
    @Override
    protected String readString() throws IOException {
        this.consumeWhitespace();
        final StringBuilder buffer = new StringBuilder();
        int c;
        for (c = this.pdfSource.read(); !this.isEndOfName((char)c) && !this.isClosing(c) && c != -1; c = this.pdfSource.read()) {
            buffer.append((char)c);
        }
        if (c != -1) {
            this.pdfSource.unread(c);
        }
        return buffer.toString();
    }
    
    protected COSDictionary readDictionaryBackwards() throws IOException {
        final COSDictionary dict = new COSDictionary();
        this.consumeWhitespaceBackwards();
        byte singleByte = this.readByteBackwards();
        if (this.throwNonConformingException && singleByte != 62) {
            throw new AssertionError((Object)"");
        }
        singleByte = this.readByteBackwards();
        if (this.throwNonConformingException && singleByte != 62) {
            throw new AssertionError((Object)"");
        }
        boolean atEndOfDictionary = false;
        singleByte = this.consumeWhitespaceBackwards();
        if (singleByte == 60) {
            this.inputFile.seek(this.currentOffset - 1L);
            atEndOfDictionary = ((byte)this.inputFile.read() == 60);
        }
        final COSDictionary backwardsDictionary = new COSDictionary();
        while (!atEndOfDictionary) {
            final COSBase object = this.readObjectBackwards();
            final COSName name = this.readNameBackwards();
            backwardsDictionary.setItem(name, object);
            singleByte = this.consumeWhitespaceBackwards();
            if (singleByte == 60) {
                this.inputFile.seek(this.currentOffset - 1L);
                atEndOfDictionary = ((byte)this.inputFile.read() == 60);
            }
        }
        final Set<COSName> backwardsKeys = backwardsDictionary.keySet();
        for (int i = backwardsKeys.size() - 1; i >= 0; --i) {
            dict.setItem((COSName)backwardsKeys.toArray()[i], backwardsDictionary.getItem((COSName)backwardsKeys.toArray()[i]));
        }
        this.readByteBackwards();
        this.readByteBackwards();
        return dict;
    }
    
    protected String readLineBackwards() throws IOException {
        final StringBuilder sb = new StringBuilder();
        boolean endOfObject = false;
        do {
            final byte singleByte = this.readByteBackwards();
            if (singleByte == 10) {
                this.inputFile.seek(this.currentOffset);
                if ((byte)this.inputFile.read() == 13) {
                    --this.currentOffset;
                }
                endOfObject = true;
            }
            else if (singleByte == 13) {
                endOfObject = true;
            }
            else {
                sb.insert(0, (char)singleByte);
            }
        } while (!endOfObject);
        return sb.toString();
    }
    
    @Override
    protected String readLine() throws IOException {
        final StringBuilder sb = new StringBuilder();
        boolean endOfLine = false;
        do {
            final byte singleByte = this.readByte();
            if (singleByte == 10) {
                this.inputFile.seek(this.currentOffset);
                if ((byte)this.inputFile.read() == 13) {
                    ++this.currentOffset;
                }
                endOfLine = true;
            }
            else if (singleByte == 13) {
                endOfLine = true;
            }
            else {
                sb.append((char)singleByte);
            }
        } while (!endOfLine);
        return sb.toString();
    }
    
    protected String readWord() throws IOException {
        final StringBuilder sb = new StringBuilder();
        boolean stop = true;
        do {
            final byte singleByte = this.readByte();
            stop = this.isWhitespace(singleByte);
            if (!stop && sb.length() > 0) {
                stop = (singleByte == 47 || singleByte == 91 || singleByte == 93 || (singleByte == 62 && !">".equals(sb.toString())));
                if (stop) {
                    --this.currentOffset;
                }
            }
            if (!stop) {
                sb.append((char)singleByte);
            }
        } while (!stop);
        return sb.toString();
    }
    
    public boolean isRecursivlyRead() {
        return this.recursivlyRead;
    }
    
    public void setRecursivlyRead(final boolean recursivlyRead) {
        this.recursivlyRead = recursivlyRead;
    }
}
