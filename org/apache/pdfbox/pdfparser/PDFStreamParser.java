// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdfparser;

import java.io.ByteArrayOutputStream;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.util.ImageParameters;
import org.apache.pdfbox.cos.COSDictionary;
import org.apache.pdfbox.cos.COSNumber;
import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSObject;
import org.apache.pdfbox.cos.COSBoolean;
import org.apache.pdfbox.cos.COSNull;
import java.util.NoSuchElementException;
import java.util.Iterator;
import org.apache.pdfbox.cos.COSStream;
import org.apache.pdfbox.pdmodel.common.PDStream;
import java.io.IOException;
import java.util.ArrayList;
import java.io.InputStream;
import org.apache.pdfbox.util.PDFOperator;
import org.apache.pdfbox.io.RandomAccess;
import java.util.List;

public class PDFStreamParser extends BaseParser
{
    private List<Object> streamObjects;
    private RandomAccess file;
    private PDFOperator lastBIToken;
    
    public PDFStreamParser(final InputStream stream, final RandomAccess raf, final boolean forceParsing) throws IOException {
        super(stream, forceParsing);
        this.streamObjects = new ArrayList<Object>(100);
        this.lastBIToken = null;
        this.file = raf;
    }
    
    public PDFStreamParser(final InputStream stream, final RandomAccess raf) throws IOException {
        this(stream, raf, PDFStreamParser.FORCE_PARSING);
    }
    
    public PDFStreamParser(final PDStream stream) throws IOException {
        this(stream.createInputStream(), stream.getStream().getScratchFile());
    }
    
    public PDFStreamParser(final COSStream stream, final boolean forceParsing) throws IOException {
        this(stream.getUnfilteredStream(), stream.getScratchFile(), forceParsing);
    }
    
    public PDFStreamParser(final COSStream stream) throws IOException {
        this(stream.getUnfilteredStream(), stream.getScratchFile());
    }
    
    public void parse() throws IOException {
        try {
            Object token = null;
            while ((token = this.parseNextToken()) != null) {
                this.streamObjects.add(token);
            }
        }
        finally {
            this.pdfSource.close();
        }
    }
    
    public List<Object> getTokens() {
        return this.streamObjects;
    }
    
    public void close() throws IOException {
        this.pdfSource.close();
    }
    
    public Iterator<Object> getTokenIterator() {
        return new Iterator<Object>() {
            private Object token;
            
            private void tryNext() {
                try {
                    if (this.token == null) {
                        this.token = PDFStreamParser.this.parseNextToken();
                    }
                }
                catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
            
            public boolean hasNext() {
                this.tryNext();
                return this.token != null;
            }
            
            public Object next() {
                this.tryNext();
                final Object tmp = this.token;
                if (tmp == null) {
                    throw new NoSuchElementException();
                }
                this.token = null;
                return tmp;
            }
            
            public void remove() {
                throw new UnsupportedOperationException();
            }
        };
    }
    
    private Object parseNextToken() throws IOException {
        Object retval = null;
        this.skipSpaces();
        final int nextByte = this.pdfSource.peek();
        if ((byte)nextByte == -1) {
            return null;
        }
        char c = (char)nextByte;
        switch (c) {
            case '<': {
                final int leftBracket = this.pdfSource.read();
                c = (char)this.pdfSource.peek();
                this.pdfSource.unread(leftBracket);
                if (c == '<') {
                    final COSDictionary pod = this.parseCOSDictionary();
                    this.skipSpaces();
                    if ((char)this.pdfSource.peek() == 's') {
                        retval = this.parseCOSStream(pod, this.file);
                    }
                    else {
                        retval = pod;
                    }
                    break;
                }
                retval = this.parseCOSString();
                break;
            }
            case '[': {
                retval = this.parseCOSArray();
                break;
            }
            case '(': {
                retval = this.parseCOSString();
                break;
            }
            case '/': {
                retval = this.parseCOSName();
                break;
            }
            case 'n': {
                final String nullString = this.readString();
                if (nullString.equals("null")) {
                    retval = COSNull.NULL;
                    break;
                }
                retval = PDFOperator.getOperator(nullString);
                break;
            }
            case 'f':
            case 't': {
                final String next = this.readString();
                if (next.equals("true")) {
                    retval = COSBoolean.TRUE;
                    break;
                }
                if (next.equals("false")) {
                    retval = COSBoolean.FALSE;
                    break;
                }
                retval = PDFOperator.getOperator(next);
                break;
            }
            case 'R': {
                final String line = this.readString();
                if (line.equals("R")) {
                    retval = new COSObject(null);
                    break;
                }
                retval = PDFOperator.getOperator(line);
                break;
            }
            case '+':
            case '-':
            case '.':
            case '0':
            case '1':
            case '2':
            case '3':
            case '4':
            case '5':
            case '6':
            case '7':
            case '8':
            case '9': {
                final StringBuffer buf = new StringBuffer();
                buf.append(c);
                this.pdfSource.read();
                for (boolean dotNotRead = c != '.'; Character.isDigit(c = (char)this.pdfSource.peek()) || (dotNotRead && c == '.'); dotNotRead = false) {
                    buf.append(c);
                    this.pdfSource.read();
                    if (dotNotRead && c == '.') {}
                }
                retval = COSNumber.get(buf.toString());
                break;
            }
            case 'B': {
                final String next = this.readString();
                retval = PDFOperator.getOperator(next);
                if (next.equals("BI")) {
                    this.lastBIToken = (PDFOperator)retval;
                    final COSDictionary imageParams = new COSDictionary();
                    this.lastBIToken.setImageParameters(new ImageParameters(imageParams));
                    Object nextToken = null;
                    while ((nextToken = this.parseNextToken()) instanceof COSName) {
                        final Object value = this.parseNextToken();
                        imageParams.setItem((COSName)nextToken, (COSBase)value);
                    }
                    final PDFOperator imageData = (PDFOperator)nextToken;
                    this.lastBIToken.setImageData(imageData.getImageData());
                    break;
                }
                break;
            }
            case 'I': {
                final String id = "" + (char)this.pdfSource.read() + (char)this.pdfSource.read();
                if (!id.equals("ID")) {
                    throw new IOException("Error: Expected operator 'ID' actual='" + id + "'");
                }
                final ByteArrayOutputStream imageData2 = new ByteArrayOutputStream();
                if (this.isWhitespace()) {
                    this.pdfSource.read();
                }
                for (int twoBytesAgo = 0, lastByte = this.pdfSource.read(), currentByte = this.pdfSource.read(), count = 0; (!this.isWhitespace(twoBytesAgo) || lastByte != 69 || currentByte != 73 || !this.isWhitespace()) && !this.pdfSource.isEOF(); twoBytesAgo = lastByte, lastByte = currentByte, currentByte = this.pdfSource.read(), ++count) {
                    imageData2.write(lastByte);
                }
                this.pdfSource.unread(73);
                this.pdfSource.unread(69);
                retval = PDFOperator.getOperator("ID");
                ((PDFOperator)retval).setImageData(imageData2.toByteArray());
                break;
            }
            case ']': {
                this.pdfSource.read();
                retval = COSNull.NULL;
                break;
            }
            default: {
                final String operator = this.readOperator();
                if (operator.trim().length() == 0) {
                    retval = null;
                    break;
                }
                retval = PDFOperator.getOperator(operator);
                break;
            }
        }
        return retval;
    }
    
    protected String readOperator() throws IOException {
        this.skipSpaces();
        final StringBuffer buffer = new StringBuffer(4);
        for (int nextChar = this.pdfSource.peek(); nextChar != -1 && !this.isWhitespace(nextChar) && !this.isClosing(nextChar) && nextChar != 91 && nextChar != 60 && nextChar != 40 && nextChar != 47 && (nextChar < 48 || nextChar > 57); nextChar = this.pdfSource.peek()) {
            final char currentChar = (char)this.pdfSource.read();
            nextChar = this.pdfSource.peek();
            buffer.append(currentChar);
            if (currentChar == 'd' && (nextChar == 48 || nextChar == 49)) {
                buffer.append((char)this.pdfSource.read());
            }
        }
        return buffer.toString();
    }
}
