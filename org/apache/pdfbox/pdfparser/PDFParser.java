// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdfparser;

import java.util.Iterator;
import org.apache.commons.logging.LogFactory;
import org.apache.pdfbox.cos.COSInteger;
import org.apache.pdfbox.cos.COSObject;
import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.persistence.util.COSObjectKey;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.cos.COSStream;
import org.apache.pdfbox.cos.COSDictionary;
import org.apache.pdfbox.pdmodel.fdf.FDFDocument;
import org.apache.pdfbox.pdmodel.PDDocument;
import java.util.regex.Pattern;
import org.apache.pdfbox.exceptions.WrappedIOException;
import org.apache.pdfbox.cos.COSDocument;
import java.util.ArrayList;
import java.io.IOException;
import java.io.InputStream;
import org.apache.pdfbox.io.RandomAccess;
import java.io.File;
import java.util.List;
import org.apache.commons.logging.Log;

public class PDFParser extends BaseParser
{
    private static final Log LOG;
    private static final int SPACE_BYTE = 32;
    private static final String PDF_HEADER = "%PDF-";
    private static final String FDF_HEADER = "%FDF-";
    private List<ConflictObj> conflictList;
    protected XrefTrailerResolver xrefTrailerResolver;
    private File tempDirectory;
    private RandomAccess raf;
    
    public PDFParser(final InputStream input) throws IOException {
        this(input, null, PDFParser.FORCE_PARSING);
    }
    
    public PDFParser(final InputStream input, final RandomAccess rafi) throws IOException {
        this(input, rafi, PDFParser.FORCE_PARSING);
    }
    
    public PDFParser(final InputStream input, final RandomAccess rafi, final boolean force) throws IOException {
        super(input, force);
        this.conflictList = new ArrayList<ConflictObj>();
        this.xrefTrailerResolver = new XrefTrailerResolver();
        this.tempDirectory = null;
        this.raf = null;
        this.raf = rafi;
    }
    
    public void setTempDirectory(final File tmpDir) {
        this.tempDirectory = tmpDir;
    }
    
    protected boolean isContinueOnError(final Exception e) {
        return this.forceParsing;
    }
    
    public void parse() throws IOException {
        try {
            if (this.raf == null) {
                if (this.tempDirectory != null) {
                    this.document = new COSDocument(this.tempDirectory);
                }
                else {
                    this.document = new COSDocument();
                }
            }
            else {
                this.document = new COSDocument(this.raf);
            }
            this.setDocument(this.document);
            this.parseHeader();
            this.skipToNextObj();
            boolean wasLastParsedObjectEOF = false;
            while (true) {
                while (!this.pdfSource.isEOF()) {
                    try {
                        wasLastParsedObjectEOF = this.parseObject();
                    }
                    catch (IOException e) {
                        if (wasLastParsedObjectEOF) {
                            break;
                        }
                        if (!this.isContinueOnError(e)) {
                            throw e;
                        }
                        PDFParser.LOG.warn("Parsing Error, Skipping Object", e);
                        this.skipToNextObj();
                    }
                    this.skipSpaces();
                    continue;
                    this.xrefTrailerResolver.setStartxref(this.document.getStartXref());
                    this.document.setTrailer(this.xrefTrailerResolver.getTrailer());
                    this.document.addXRefTable(this.xrefTrailerResolver.getXrefTable());
                    if (!this.document.isEncrypted()) {
                        this.document.dereferenceObjectStreams();
                    }
                    resolveConflicts(this.document, this.conflictList);
                    return;
                }
                continue;
            }
        }
        catch (Throwable t) {
            if (this.document != null) {
                this.document.close();
            }
            if (t instanceof IOException) {
                throw (IOException)t;
            }
            throw new WrappedIOException(t);
        }
        finally {
            this.pdfSource.close();
        }
    }
    
    private void skipToNextObj() throws IOException {
        final byte[] b = new byte[16];
        final Pattern p = Pattern.compile("\\d+\\s+\\d+\\s+obj.*", 32);
        while (!this.pdfSource.isEOF()) {
            final int l = this.pdfSource.read(b);
            if (l < 1) {
                break;
            }
            final String s = new String(b, "US-ASCII");
            if (s.startsWith("trailer") || s.startsWith("xref") || s.startsWith("startxref") || s.startsWith("stream") || p.matcher(s).matches()) {
                this.pdfSource.unread(b);
                break;
            }
            this.pdfSource.unread(b, 1, l - 1);
        }
    }
    
    private void parseHeader() throws IOException {
        String header = this.readLine();
        if (header.indexOf("%PDF-") == -1 && header.indexOf("%FDF-") == -1) {
            for (header = this.readLine(); header.indexOf("%PDF-") == -1 && header.indexOf("%FDF-") == -1; header = this.readLine()) {
                if (header.length() > 0 && Character.isDigit(header.charAt(0))) {
                    break;
                }
            }
        }
        if (header.indexOf("%PDF-") == -1 && header.indexOf("%FDF-") == -1) {
            throw new IOException("Error: Header doesn't contain versioninfo");
        }
        int headerStart = header.indexOf("%PDF-");
        if (headerStart == -1) {
            headerStart = header.indexOf("%FDF-");
        }
        if (headerStart > 0) {
            header = header.substring(headerStart, header.length());
        }
        if (header.startsWith("%PDF-")) {
            if (!header.matches("%PDF-\\d.\\d")) {
                final String headerGarbage = header.substring("%PDF-".length() + 3, header.length()) + "\n";
                header = header.substring(0, "%PDF-".length() + 3);
                this.pdfSource.unread(headerGarbage.getBytes("ISO-8859-1"));
            }
        }
        else if (!header.matches("%FDF-\\d.\\d")) {
            final String headerGarbage = header.substring("%FDF-".length() + 3, header.length()) + "\n";
            header = header.substring(0, "%FDF-".length() + 3);
            this.pdfSource.unread(headerGarbage.getBytes("ISO-8859-1"));
        }
        this.document.setHeaderString(header);
        try {
            if (header.startsWith("%PDF-")) {
                final float pdfVersion = Float.parseFloat(header.substring("%PDF-".length(), Math.min(header.length(), "%PDF-".length() + 3)));
                this.document.setVersion(pdfVersion);
            }
            else {
                final float pdfVersion = Float.parseFloat(header.substring("%FDF-".length(), Math.min(header.length(), "%FDF-".length() + 3)));
                this.document.setVersion(pdfVersion);
            }
        }
        catch (NumberFormatException e) {
            throw new IOException("Error getting pdf version:" + e);
        }
    }
    
    public COSDocument getDocument() throws IOException {
        if (this.document == null) {
            throw new IOException("You must call parse() before calling getDocument()");
        }
        return this.document;
    }
    
    public PDDocument getPDDocument() throws IOException {
        return new PDDocument(this.getDocument());
    }
    
    public FDFDocument getFDFDocument() throws IOException {
        return new FDFDocument(this.getDocument());
    }
    
    private boolean parseObject() throws IOException {
        long currentObjByteOffset = this.pdfSource.getOffset();
        boolean isEndOfFile = false;
        this.skipSpaces();
        char peekedChar;
        for (peekedChar = (char)this.pdfSource.peek(); peekedChar == 'e'; peekedChar = (char)this.pdfSource.peek()) {
            this.readString();
            this.skipSpaces();
            currentObjByteOffset = this.pdfSource.getOffset();
        }
        if (!this.pdfSource.isEOF()) {
            if (peekedChar == 'x') {
                this.parseXrefTable(currentObjByteOffset);
            }
            else if (peekedChar == 't' || peekedChar == 's') {
                if (peekedChar == 't') {
                    this.parseTrailer();
                    peekedChar = (char)this.pdfSource.peek();
                }
                if (peekedChar == 's') {
                    this.parseStartXref();
                    while (this.isWhitespace(this.pdfSource.peek()) && !this.pdfSource.isEOF()) {
                        this.pdfSource.read();
                    }
                    String eof = "";
                    if (!this.pdfSource.isEOF()) {
                        eof = this.readLine();
                    }
                    if (!"%%EOF".equals(eof)) {
                        if (eof.startsWith("%%EOF")) {
                            this.pdfSource.unread(32);
                            this.pdfSource.unread(eof.substring(5).getBytes("ISO-8859-1"));
                        }
                        else {
                            PDFParser.LOG.warn("expected='%%EOF' actual='" + eof + "'");
                            if (!this.pdfSource.isEOF()) {
                                this.pdfSource.unread(32);
                                this.pdfSource.unread(eof.getBytes("ISO-8859-1"));
                            }
                        }
                    }
                    isEndOfFile = true;
                }
            }
            else {
                int number = -1;
                int genNum = -1;
                String objectKey = null;
                boolean missingObjectNumber = false;
                try {
                    final char peeked = (char)this.pdfSource.peek();
                    if (peeked == '<') {
                        missingObjectNumber = true;
                    }
                    else {
                        number = this.readInt();
                    }
                }
                catch (IOException e) {
                    number = this.readInt();
                }
                if (!missingObjectNumber) {
                    this.skipSpaces();
                    genNum = this.readInt();
                    objectKey = this.readString(3);
                    if (!objectKey.equals("obj") && (!this.isContinueOnError(null) || !objectKey.equals("o"))) {
                        throw new IOException("expected='obj' actual='" + objectKey + "' " + this.pdfSource);
                    }
                }
                else {
                    number = -1;
                    genNum = -1;
                }
                this.skipSpaces();
                COSBase pb = this.parseDirObject();
                String endObjectKey = this.readString();
                if (endObjectKey.equals("stream")) {
                    this.pdfSource.unread(endObjectKey.getBytes("ISO-8859-1"));
                    this.pdfSource.unread(32);
                    if (!(pb instanceof COSDictionary)) {
                        throw new IOException("stream not preceded by dictionary");
                    }
                    pb = this.parseCOSStream((COSDictionary)pb, this.getDocument().getScratchFile());
                    final COSStream strmObj = (COSStream)pb;
                    final COSName objectType = (COSName)strmObj.getItem(COSName.TYPE);
                    if (objectType != null && objectType.equals(COSName.XREF)) {
                        this.parseXrefStream(strmObj, currentObjByteOffset);
                    }
                    this.skipSpaces();
                    endObjectKey = this.readLine();
                }
                final COSObjectKey key = new COSObjectKey(number, genNum);
                final COSObject pdfObject = this.document.getObjectFromPool(key);
                if (pdfObject.getObject() == null) {
                    pdfObject.setObject(pb);
                }
                else {
                    this.addObjectToConflicts(currentObjByteOffset, key, pb);
                }
                if (!endObjectKey.equals("endobj")) {
                    if (endObjectKey.startsWith("endobj")) {
                        this.pdfSource.unread(32);
                        this.pdfSource.unread(endObjectKey.substring(6).getBytes("ISO-8859-1"));
                    }
                    else if (endObjectKey.trim().endsWith("endobj")) {
                        PDFParser.LOG.warn("expected='endobj' actual='" + endObjectKey + "' ");
                    }
                    else if (!this.pdfSource.isEOF()) {
                        this.pdfSource.unread(32);
                        this.pdfSource.unread(endObjectKey.getBytes("ISO-8859-1"));
                    }
                }
                this.skipSpaces();
            }
        }
        return isEndOfFile;
    }
    
    private void addObjectToConflicts(final long offset, final COSObjectKey key, final COSBase pb) throws IOException {
        final COSObject obj = new COSObject(null);
        obj.setObjectNumber(COSInteger.get(key.getNumber()));
        obj.setGenerationNumber(COSInteger.get(key.getGeneration()));
        obj.setObject(pb);
        final ConflictObj conflictObj = new ConflictObj(offset, key, obj);
        this.conflictList.add(conflictObj);
    }
    
    protected boolean parseStartXref() throws IOException {
        if (this.pdfSource.peek() != 115) {
            return false;
        }
        final String startXRef = this.readString();
        if (!startXRef.trim().equals("startxref")) {
            return false;
        }
        this.skipSpaces();
        this.getDocument().setStartXref(this.readInt());
        return true;
    }
    
    protected boolean parseXrefTable(final long startByteOffset) throws IOException {
        if (this.pdfSource.peek() != 120) {
            return false;
        }
        final String xref = this.readString();
        if (!xref.trim().equals("xref")) {
            return false;
        }
        this.xrefTrailerResolver.nextXrefObj(startByteOffset);
        while (true) {
            int currObjID = this.readInt();
            final int count = this.readInt();
            this.skipSpaces();
            for (int i = 0; i < count && !this.pdfSource.isEOF(); ++i) {
                if (this.isEndOfName((char)this.pdfSource.peek())) {
                    break;
                }
                if (this.pdfSource.peek() == 116) {
                    break;
                }
                final String currentLine = this.readLine();
                final String[] splitString = currentLine.split(" ");
                if (splitString.length < 3) {
                    PDFParser.LOG.warn("invalid xref line: " + currentLine);
                    break;
                }
                Label_0281: {
                    if (splitString[splitString.length - 1].equals("n")) {
                        try {
                            final long currOffset = Long.parseLong(splitString[0]);
                            final int currGenID = Integer.parseInt(splitString[1]);
                            final COSObjectKey objKey = new COSObjectKey(currObjID, currGenID);
                            this.xrefTrailerResolver.setXRef(objKey, currOffset);
                            break Label_0281;
                        }
                        catch (NumberFormatException e) {
                            throw new IOException(e.getMessage());
                        }
                    }
                    if (!splitString[2].equals("f")) {
                        throw new IOException("Corrupt XRefTable Entry - ObjID:" + currObjID);
                    }
                }
                ++currObjID;
                this.skipSpaces();
            }
            this.skipSpaces();
            final char c = (char)this.pdfSource.peek();
            if (c >= '0' && c <= '9') {
                continue;
            }
            return true;
        }
    }
    
    protected boolean parseTrailer() throws IOException {
        if (this.pdfSource.peek() != 116) {
            return false;
        }
        final String nextLine = this.readLine();
        if (!nextLine.trim().equals("trailer")) {
            if (!nextLine.startsWith("trailer")) {
                return false;
            }
            final byte[] b = nextLine.getBytes("ISO-8859-1");
            final int len = "trailer".length();
            this.pdfSource.unread(10);
            this.pdfSource.unread(b, len, b.length - len);
        }
        this.skipSpaces();
        final COSDictionary parsedTrailer = this.parseCOSDictionary();
        this.xrefTrailerResolver.setTrailer(parsedTrailer);
        this.readVersionInTrailer(parsedTrailer);
        this.skipSpaces();
        return true;
    }
    
    private void readVersionInTrailer(final COSDictionary parsedTrailer) {
        final COSObject root = (COSObject)parsedTrailer.getItem(COSName.ROOT);
        if (root != null) {
            final COSName version = (COSName)root.getItem(COSName.VERSION);
            if (version != null) {
                final float trailerVersion = Float.valueOf(version.getName());
                if (trailerVersion > this.document.getVersion()) {
                    this.document.setVersion(trailerVersion);
                }
            }
        }
    }
    
    public void parseXrefStream(final COSStream stream, final long objByteOffset) throws IOException {
        this.xrefTrailerResolver.nextXrefObj(objByteOffset);
        this.xrefTrailerResolver.setTrailer(stream);
        final PDFXrefStreamParser parser = new PDFXrefStreamParser(stream, this.document, this.forceParsing, this.xrefTrailerResolver);
        parser.parse();
    }
    
    static {
        LOG = LogFactory.getLog(PDFParser.class);
    }
    
    private static class ConflictObj
    {
        private long offset;
        private COSObjectKey objectKey;
        private COSObject object;
        
        public ConflictObj(final long offsetValue, final COSObjectKey key, final COSObject pdfObject) {
            this.offset = offsetValue;
            this.objectKey = key;
            this.object = pdfObject;
        }
        
        @Override
        public String toString() {
            return "Object(" + this.offset + ", " + this.objectKey + ")";
        }
        
        private static void resolveConflicts(final COSDocument document, final List<ConflictObj> conflictList) throws IOException {
            for (final ConflictObj o : conflictList) {
                final Long offset = new Long(o.offset);
                if (document.getXrefTable().containsValue(offset)) {
                    final COSObject pdfObject = document.getObjectFromPool(o.objectKey);
                    pdfObject.setObject(o.object.getObject());
                }
            }
        }
    }
}
