// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdfparser;

import org.apache.commons.logging.LogFactory;
import java.util.HashSet;
import java.util.Set;
import java.util.Iterator;
import java.util.List;
import org.apache.pdfbox.cos.COSName;
import java.util.Collections;
import java.util.Collection;
import java.util.ArrayList;
import org.apache.pdfbox.cos.COSDictionary;
import org.apache.pdfbox.persistence.util.COSObjectKey;
import java.util.HashMap;
import org.apache.commons.logging.Log;
import java.util.Map;

public class XrefTrailerResolver
{
    private final Map<Long, XrefTrailerObj> bytePosToXrefMap;
    private XrefTrailerObj curXrefTrailerObj;
    private XrefTrailerObj resolvedXrefTrailer;
    private static final Log LOG;
    
    public XrefTrailerResolver() {
        this.bytePosToXrefMap = new HashMap<Long, XrefTrailerObj>();
        this.curXrefTrailerObj = null;
        this.resolvedXrefTrailer = null;
    }
    
    public void nextXrefObj(final long startBytePos) {
        this.bytePosToXrefMap.put(startBytePos, this.curXrefTrailerObj = new XrefTrailerObj());
    }
    
    public void setXRef(final COSObjectKey objKey, final long offset) {
        if (this.curXrefTrailerObj == null) {
            XrefTrailerResolver.LOG.warn("Cannot add XRef entry for '" + objKey.getNumber() + "' because XRef start was not signalled.");
            return;
        }
        this.curXrefTrailerObj.xrefTable.put(objKey, offset);
    }
    
    public void setTrailer(final COSDictionary trailer) {
        if (this.curXrefTrailerObj == null) {
            XrefTrailerResolver.LOG.warn("Cannot add trailer because XRef start was not signalled.");
            return;
        }
        this.curXrefTrailerObj.trailer = trailer;
    }
    
    public COSDictionary getCurrentTrailer() {
        return this.curXrefTrailerObj.trailer;
    }
    
    public void setStartxref(final long startxrefBytePosValue) {
        if (this.resolvedXrefTrailer != null) {
            XrefTrailerResolver.LOG.warn("Method must be called only ones with last startxref value.");
            return;
        }
        (this.resolvedXrefTrailer = new XrefTrailerObj()).trailer = new COSDictionary();
        XrefTrailerObj curObj = this.bytePosToXrefMap.get(startxrefBytePosValue);
        final List<Long> xrefSeqBytePos = new ArrayList<Long>();
        if (curObj == null) {
            XrefTrailerResolver.LOG.warn("Did not found XRef object at specified startxref position " + startxrefBytePosValue);
            xrefSeqBytePos.addAll(this.bytePosToXrefMap.keySet());
            Collections.sort(xrefSeqBytePos);
        }
        else {
            xrefSeqBytePos.add(startxrefBytePosValue);
            while (curObj.trailer != null) {
                final long prevBytePos = curObj.trailer.getLong(COSName.PREV, -1L);
                if (prevBytePos == -1L) {
                    break;
                }
                curObj = this.bytePosToXrefMap.get(prevBytePos);
                if (curObj == null) {
                    XrefTrailerResolver.LOG.warn("Did not found XRef object pointed to by 'Prev' key at position " + prevBytePos);
                    break;
                }
                xrefSeqBytePos.add(prevBytePos);
                if (xrefSeqBytePos.size() >= this.bytePosToXrefMap.size()) {
                    break;
                }
            }
            Collections.reverse(xrefSeqBytePos);
        }
        for (final Long bPos : xrefSeqBytePos) {
            curObj = this.bytePosToXrefMap.get(bPos);
            if (curObj.trailer != null) {
                this.resolvedXrefTrailer.trailer.addAll(curObj.trailer);
            }
            this.resolvedXrefTrailer.xrefTable.putAll(curObj.xrefTable);
        }
    }
    
    public COSDictionary getTrailer() {
        return (this.resolvedXrefTrailer == null) ? null : this.resolvedXrefTrailer.trailer;
    }
    
    public Map<COSObjectKey, Long> getXrefTable() {
        return (this.resolvedXrefTrailer == null) ? null : this.resolvedXrefTrailer.xrefTable;
    }
    
    public Set<Long> getContainedObjectNumbers(final int objstmObjNr) {
        if (this.resolvedXrefTrailer == null) {
            return null;
        }
        final Set<Long> refObjNrs = new HashSet<Long>();
        final int cmpVal = -objstmObjNr;
        for (final Map.Entry<COSObjectKey, Long> xrefEntry : this.resolvedXrefTrailer.xrefTable.entrySet()) {
            if (xrefEntry.getValue() == cmpVal) {
                refObjNrs.add(xrefEntry.getKey().getNumber());
            }
        }
        return refObjNrs;
    }
    
    static {
        LOG = LogFactory.getLog(XrefTrailerResolver.class);
    }
    
    private class XrefTrailerObj
    {
        private COSDictionary trailer;
        private final Map<COSObjectKey, Long> xrefTable;
        
        private XrefTrailerObj() {
            this.trailer = null;
            this.xrefTable = new HashMap<COSObjectKey, Long>();
        }
    }
}
