// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdfparser;

import java.util.Iterator;
import org.apache.pdfbox.persistence.util.COSObjectKey;
import java.util.ArrayList;
import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSInteger;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.cos.COSArray;
import java.io.IOException;
import org.apache.pdfbox.cos.COSDocument;
import org.apache.pdfbox.cos.COSStream;

public class PDFXrefStreamParser extends BaseParser
{
    private COSStream stream;
    private XrefTrailerResolver xrefTrailerResolver;
    
    public PDFXrefStreamParser(final COSStream strm, final COSDocument doc, final boolean forceParsing, final XrefTrailerResolver resolver) throws IOException {
        super(strm.getUnfilteredStream(), forceParsing);
        this.setDocument(doc);
        this.stream = strm;
        this.xrefTrailerResolver = resolver;
    }
    
    public void parse() throws IOException {
        try {
            final COSArray xrefFormat = (COSArray)this.stream.getDictionaryObject(COSName.W);
            COSArray indexArray = (COSArray)this.stream.getDictionaryObject(COSName.INDEX);
            if (indexArray == null) {
                indexArray = new COSArray();
                indexArray.add(COSInteger.ZERO);
                indexArray.add(this.stream.getDictionaryObject(COSName.SIZE));
            }
            final ArrayList<Integer> objNums = new ArrayList<Integer>();
            final Iterator<COSBase> indexIter = indexArray.iterator();
            while (indexIter.hasNext()) {
                final int objID = indexIter.next().intValue();
                for (int size = indexIter.next().intValue(), i = 0; i < size; ++i) {
                    objNums.add(new Integer(objID + i));
                }
            }
            final Iterator<Integer> objIter = objNums.iterator();
            final int w0 = xrefFormat.getInt(0);
            final int w2 = xrefFormat.getInt(1);
            final int w3 = xrefFormat.getInt(2);
            final int lineSize = w0 + w2 + w3;
            while (this.pdfSource.available() > 0 && objIter.hasNext()) {
                final byte[] currLine = new byte[lineSize];
                this.pdfSource.read(currLine);
                int type = 0;
                for (int j = 0; j < w0; ++j) {
                    type += (currLine[j] & 0xFF) << (w0 - j - 1) * 8;
                }
                final Integer objID2 = objIter.next();
                switch (type) {
                    case 0: {
                        continue;
                    }
                    case 1: {
                        int offset = 0;
                        for (int k = 0; k < w2; ++k) {
                            offset += (currLine[k + w0] & 0xFF) << (w2 - k - 1) * 8;
                        }
                        int genNum = 0;
                        for (int l = 0; l < w3; ++l) {
                            genNum += (currLine[l + w0 + w2] & 0xFF) << (w3 - l - 1) * 8;
                        }
                        final COSObjectKey objKey = new COSObjectKey(objID2, genNum);
                        this.xrefTrailerResolver.setXRef(objKey, offset);
                        continue;
                    }
                    case 2: {
                        int objstmObjNr = 0;
                        for (int m = 0; m < w2; ++m) {
                            objstmObjNr += (currLine[m + w0] & 0xFF) << (w2 - m - 1) * 8;
                        }
                        final COSObjectKey objKey = new COSObjectKey(objID2, 0L);
                        this.xrefTrailerResolver.setXRef(objKey, -objstmObjNr);
                        continue;
                    }
                }
            }
        }
        finally {
            this.pdfSource.close();
        }
    }
}
