// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdfparser;

import org.apache.commons.logging.LogFactory;
import org.apache.pdfbox.cos.COSObject;
import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.pdfwriter.COSWriter;
import org.apache.pdfbox.persistence.util.COSObjectKey;
import org.apache.pdfbox.cos.COSDictionary;
import java.util.regex.Pattern;
import org.apache.pdfbox.cos.COSDocument;
import java.io.IOException;
import java.io.InputStream;
import org.apache.commons.logging.Log;

public class VisualSignatureParser extends BaseParser
{
    private static final Log LOG;
    
    public VisualSignatureParser(final InputStream input) throws IOException {
        super(input);
    }
    
    public void parse() throws IOException {
        this.document = new COSDocument();
        this.skipToNextObj();
        boolean wasLastParsedObjectEOF = false;
        try {
            while (!wasLastParsedObjectEOF && !this.pdfSource.isEOF()) {
                try {
                    wasLastParsedObjectEOF = this.parseObject();
                }
                catch (IOException e) {
                    VisualSignatureParser.LOG.warn("Parsing Error, Skipping Object", e);
                    this.skipToNextObj();
                }
                this.skipSpaces();
            }
        }
        catch (IOException e) {
            if (!wasLastParsedObjectEOF) {
                throw e;
            }
        }
    }
    
    private void skipToNextObj() throws IOException {
        final byte[] b = new byte[16];
        final Pattern p = Pattern.compile("\\d+\\s+\\d+\\s+obj.*", 32);
        while (!this.pdfSource.isEOF()) {
            final int l = this.pdfSource.read(b);
            if (l < 1) {
                break;
            }
            final String s = new String(b, "US-ASCII");
            if (s.startsWith("trailer") || s.startsWith("xref") || s.startsWith("startxref") || s.startsWith("stream") || p.matcher(s).matches()) {
                this.pdfSource.unread(b);
                break;
            }
            this.pdfSource.unread(b, 1, l - 1);
        }
    }
    
    private boolean parseObject() throws IOException {
        boolean isEndOfFile = false;
        this.skipSpaces();
        char peekedChar;
        for (peekedChar = (char)this.pdfSource.peek(); peekedChar == 'e'; peekedChar = (char)this.pdfSource.peek()) {
            this.readString();
            this.skipSpaces();
        }
        if (!this.pdfSource.isEOF()) {
            if (peekedChar == 'x') {
                return true;
            }
            if (peekedChar == 't' || peekedChar == 's') {
                if (peekedChar == 't') {
                    return true;
                }
                if (peekedChar == 's') {
                    this.skipToNextObj();
                    final String eof = this.readExpectedString("%%EOF");
                    if (eof.indexOf("%%EOF") == -1 && !this.pdfSource.isEOF()) {
                        throw new IOException("expected='%%EOF' actual='" + eof + "' next=" + this.readString() + " next=" + this.readString());
                    }
                    isEndOfFile = true;
                }
            }
            else {
                int number = -1;
                int genNum = -1;
                String objectKey = null;
                boolean missingObjectNumber = false;
                try {
                    final char peeked = (char)this.pdfSource.peek();
                    if (peeked == '<') {
                        missingObjectNumber = true;
                    }
                    else {
                        number = this.readInt();
                    }
                }
                catch (IOException e) {
                    number = this.readInt();
                }
                if (!missingObjectNumber) {
                    this.skipSpaces();
                    genNum = this.readInt();
                    objectKey = this.readString(3);
                    if (!objectKey.equals("obj")) {
                        throw new IOException("expected='obj' actual='" + objectKey + "' " + this.pdfSource);
                    }
                }
                else {
                    number = -1;
                    genNum = -1;
                }
                this.skipSpaces();
                COSBase pb = this.parseDirObject();
                String endObjectKey = this.readString();
                if (endObjectKey.equals("stream")) {
                    this.pdfSource.unread(endObjectKey.getBytes());
                    this.pdfSource.unread(32);
                    if (!(pb instanceof COSDictionary)) {
                        throw new IOException("stream not preceded by dictionary");
                    }
                    pb = this.parseCOSStream((COSDictionary)pb, this.getDocument().getScratchFile());
                    endObjectKey = this.readString();
                }
                final COSObjectKey key = new COSObjectKey(number, genNum);
                final COSObject pdfObject = this.document.getObjectFromPool(key);
                pb.setNeedToBeUpdate(true);
                pdfObject.setObject(pb);
                if (!endObjectKey.equals("endobj")) {
                    if (endObjectKey.startsWith("endobj")) {
                        this.pdfSource.unread(endObjectKey.substring(6).getBytes());
                    }
                    else if (!this.pdfSource.isEOF()) {
                        try {
                            Float.parseFloat(endObjectKey);
                            this.pdfSource.unread(COSWriter.SPACE);
                            this.pdfSource.unread(endObjectKey.getBytes());
                        }
                        catch (NumberFormatException e2) {
                            final String secondEndObjectKey = this.readString();
                            if (!secondEndObjectKey.equals("endobj")) {
                                if (this.isClosing()) {
                                    this.pdfSource.read();
                                }
                                this.skipSpaces();
                                final String thirdPossibleEndObj = this.readString();
                                if (!thirdPossibleEndObj.equals("endobj")) {
                                    throw new IOException("expected='endobj' firstReadAttempt='" + endObjectKey + "' " + "secondReadAttempt='" + secondEndObjectKey + "' " + this.pdfSource);
                                }
                            }
                        }
                    }
                }
                this.skipSpaces();
            }
        }
        return isEndOfFile;
    }
    
    public COSDocument getDocument() throws IOException {
        if (this.document == null) {
            throw new IOException("You must call parse() before calling getDocument()");
        }
        return this.document;
    }
    
    static {
        LOG = LogFactory.getLog(PDFParser.class);
    }
}
