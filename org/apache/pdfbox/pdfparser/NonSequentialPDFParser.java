// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.pdfparser;

import org.apache.commons.logging.LogFactory;
import java.io.ByteArrayInputStream;
import java.io.OutputStream;
import org.apache.pdfbox.cos.COSNumber;
import java.util.Map;
import org.apache.pdfbox.cos.COSString;
import org.apache.pdfbox.exceptions.CryptographyException;
import org.apache.pdfbox.cos.COSNull;
import java.util.ArrayList;
import java.util.Collections;
import org.apache.pdfbox.persistence.util.COSObjectKey;
import java.util.HashSet;
import java.util.List;
import java.util.TreeMap;
import java.util.LinkedList;
import java.util.Set;
import java.util.Collection;
import java.util.Queue;
import java.util.Iterator;
import org.apache.pdfbox.cos.COSArray;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.cos.COSStream;
import org.apache.pdfbox.pdmodel.encryption.AccessPermission;
import org.apache.pdfbox.pdmodel.encryption.DecryptionMaterial;
import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.pdmodel.encryption.SecurityHandlersManager;
import org.apache.pdfbox.pdmodel.encryption.StandardDecryptionMaterial;
import org.apache.pdfbox.pdmodel.encryption.PublicKeyDecryptionMaterial;
import java.io.FileInputStream;
import java.security.KeyStore;
import org.apache.pdfbox.pdmodel.encryption.PDEncryptionDictionary;
import org.apache.pdfbox.cos.COSObject;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.io.PushBackInputStream;
import org.apache.pdfbox.cos.COSDocument;
import org.apache.pdfbox.io.RandomAccessBuffer;
import java.io.IOException;
import org.apache.pdfbox.io.RandomAccess;
import org.apache.pdfbox.cos.COSDictionary;
import org.apache.commons.logging.Log;
import org.apache.pdfbox.pdmodel.encryption.SecurityHandler;
import org.apache.pdfbox.io.RandomAccessBufferedFileInputStream;
import java.io.File;
import java.io.InputStream;

public class NonSequentialPDFParser extends PDFParser
{
    public static final String SYSPROP_PARSEMINIMAL = "org.apache.pdfbox.pdfparser.nonSequentialPDFParser.parseMinimal";
    public static final String SYSPROP_EOFLOOKUPRANGE = "org.apache.pdfbox.pdfparser.nonSequentialPDFParser.eofLookupRange";
    private static final InputStream EMPTY_INPUT_STREAM;
    private static final int DEFAULT_TRAIL_BYTECOUNT = 2048;
    private static final char[] EOF_MARKER;
    private static final char[] STARTXREF_MARKER;
    private static final char[] OBJ_MARKER;
    private final File pdfFile;
    private final RandomAccessBufferedFileInputStream raStream;
    private SecurityHandler securityHandler;
    private String keyStoreFilename;
    private String alias;
    private String password;
    private int readTrailBytes;
    private boolean parseMinimalCatalog;
    private boolean initialParseDone;
    private boolean allPagesParsed;
    private static final Log LOG;
    private COSDictionary pagesDictionary;
    private boolean inGetLength;
    private final int streamCopyBufLen = 8192;
    private final byte[] streamCopyBuf;
    
    public NonSequentialPDFParser(final String filename) throws IOException {
        this(new File(filename), null);
    }
    
    public NonSequentialPDFParser(final File file, final RandomAccess raBuf) throws IOException {
        this(file, raBuf, "");
    }
    
    public NonSequentialPDFParser(final File file, final RandomAccess raBuf, final String decryptionPassword) throws IOException {
        super(NonSequentialPDFParser.EMPTY_INPUT_STREAM, null, false);
        this.securityHandler = null;
        this.keyStoreFilename = null;
        this.alias = null;
        this.password = "";
        this.readTrailBytes = 2048;
        this.parseMinimalCatalog = "true".equals(System.getProperty("org.apache.pdfbox.pdfparser.nonSequentialPDFParser.parseMinimal"));
        this.initialParseDone = false;
        this.allPagesParsed = false;
        this.pagesDictionary = null;
        this.inGetLength = false;
        this.streamCopyBuf = new byte[8192];
        final String eofLookupRangeStr = System.getProperty("org.apache.pdfbox.pdfparser.nonSequentialPDFParser.eofLookupRange");
        if (eofLookupRangeStr != null) {
            try {
                this.setEOFLookupRange(Integer.parseInt(eofLookupRangeStr));
            }
            catch (NumberFormatException nfe) {
                NonSequentialPDFParser.LOG.warn("System property org.apache.pdfbox.pdfparser.nonSequentialPDFParser.eofLookupRange does not contain an integer value, but: '" + eofLookupRangeStr + "'");
            }
        }
        this.pdfFile = file;
        this.raStream = new RandomAccessBufferedFileInputStream(this.pdfFile);
        this.setDocument((raBuf == null) ? new COSDocument(new RandomAccessBuffer(), false) : new COSDocument(raBuf, false));
        this.pdfSource = new PushBackInputStream(this.raStream, 4096);
        this.password = decryptionPassword;
    }
    
    public void setEOFLookupRange(final int byteCount) {
        if (byteCount > 15) {
            this.readTrailBytes = byteCount;
        }
    }
    
    private void initialParse() throws IOException {
        final long startxrefOff = this.getStartxrefOffset();
        this.setPdfSource(startxrefOff);
        this.parseStartXref();
        long prev;
        final long xrefOffset = prev = this.document.getStartXref();
        while (prev > -1L) {
            this.setPdfSource(prev);
            if (this.pdfSource.peek() == 120) {
                this.parseXrefTable(prev);
                if (!this.parseTrailer()) {
                    throw new IOException("Expected trailer object at position: " + this.pdfSource.getOffset());
                }
                final COSDictionary trailer = this.xrefTrailerResolver.getCurrentTrailer();
                prev = trailer.getInt(COSName.PREV);
            }
            else {
                prev = this.parseXrefObjStream(prev);
            }
        }
        this.xrefTrailerResolver.setStartxref(xrefOffset);
        this.document.setTrailer(this.xrefTrailerResolver.getTrailer());
        final COSBase trailerEncryptItem = this.document.getTrailer().getItem(COSName.ENCRYPT);
        if (trailerEncryptItem != null) {
            if (trailerEncryptItem instanceof COSObject) {
                final COSObject trailerEncryptObj = (COSObject)trailerEncryptItem;
                this.parseObjectDynamically(trailerEncryptObj, true);
            }
            try {
                final PDEncryptionDictionary encParameters = new PDEncryptionDictionary(this.document.getEncryptionDictionary());
                DecryptionMaterial decryptionMaterial = null;
                if (this.keyStoreFilename != null) {
                    final KeyStore ks = KeyStore.getInstance("PKCS12");
                    ks.load(new FileInputStream(this.keyStoreFilename), this.password.toCharArray());
                    decryptionMaterial = new PublicKeyDecryptionMaterial(ks, this.alias, this.password);
                }
                else {
                    decryptionMaterial = new StandardDecryptionMaterial(this.password);
                }
                (this.securityHandler = SecurityHandlersManager.getInstance().getSecurityHandler(encParameters.getFilter())).prepareForDecryption(encParameters, this.document.getDocumentID(), decryptionMaterial);
                final AccessPermission permission = this.securityHandler.getCurrentAccessPermission();
                if (!permission.canExtractContent()) {
                    NonSequentialPDFParser.LOG.warn("PDF file '" + this.pdfFile.getPath() + "' does not allow extracting content.");
                }
            }
            catch (Exception e) {
                throw new IOException("Error (" + e.getClass().getSimpleName() + ") while creating security handler for decryption: " + e.getMessage());
            }
        }
        final COSObject root = (COSObject)this.xrefTrailerResolver.getTrailer().getItem(COSName.ROOT);
        if (root == null) {
            throw new IOException("Missing root object specification in trailer.");
        }
        this.parseObjectDynamically(root, false);
        if (!this.parseMinimalCatalog) {
            final COSObject catalogObj = this.document.getCatalog();
            if (catalogObj != null && catalogObj.getObject() instanceof COSDictionary) {
                this.parseDictObjects((COSDictionary)catalogObj.getObject(), (COSName[])null);
                this.allPagesParsed = true;
                this.document.setDecrypted();
            }
        }
        this.initialParseDone = true;
    }
    
    private long parseXrefObjStream(final long objByteOffset) throws IOException {
        this.readInt();
        this.readInt();
        this.readPattern(NonSequentialPDFParser.OBJ_MARKER);
        final COSDictionary dict = this.parseCOSDictionary();
        final COSStream xrefStream = this.parseCOSStream(dict, this.getDocument().getScratchFile());
        this.parseXrefStream(xrefStream, (int)objByteOffset);
        return dict.getLong(COSName.PREV);
    }
    
    private final long getPdfSourceOffset() {
        return this.pdfSource.getOffset();
    }
    
    private final void setPdfSource(final long fileOffset) throws IOException {
        this.pdfSource.seek(fileOffset);
    }
    
    private final void releasePdfSourceInputStream() throws IOException {
    }
    
    private final void closeFileStream() throws IOException {
        if (this.pdfSource != null) {
            this.pdfSource.close();
        }
    }
    
    private final long getStartxrefOffset() throws IOException {
        final long fileLen = this.pdfFile.length();
        FileInputStream fIn = null;
        byte[] buf;
        long skipBytes;
        try {
            fIn = new FileInputStream(this.pdfFile);
            final int trailByteCount = (fileLen < this.readTrailBytes) ? ((int)fileLen) : this.readTrailBytes;
            buf = new byte[trailByteCount];
            fIn.skip(skipBytes = fileLen - trailByteCount);
            int readBytes;
            for (int off = 0; off < trailByteCount; off += readBytes) {
                readBytes = fIn.read(buf, off, trailByteCount - off);
                if (readBytes < 1) {
                    throw new IOException("No more bytes to read for trailing buffer, but expected: " + (trailByteCount - off));
                }
            }
        }
        finally {
            if (fIn != null) {
                try {
                    fIn.close();
                }
                catch (IOException ex) {}
            }
        }
        int bufOff = this.lastIndexOf(NonSequentialPDFParser.EOF_MARKER, buf, buf.length);
        if (bufOff < 0) {
            throw new IOException("Missing end of file marker '" + new String(NonSequentialPDFParser.EOF_MARKER) + "'");
        }
        bufOff = this.lastIndexOf(NonSequentialPDFParser.STARTXREF_MARKER, buf, bufOff);
        if (bufOff < 0) {
            throw new IOException("Missing 'startxref' marker.");
        }
        return skipBytes + bufOff;
    }
    
    private final int lastIndexOf(final char[] pattern, final byte[] buf, final int endOff) {
        final int lastPatternChOff = pattern.length - 1;
        int bufOff = endOff;
        int patOff = lastPatternChOff;
        char lookupCh = pattern[patOff];
        while (--bufOff >= 0) {
            if (buf[bufOff] == lookupCh) {
                if (--patOff < 0) {
                    return bufOff;
                }
                lookupCh = pattern[patOff];
            }
            else {
                if (patOff >= lastPatternChOff) {
                    continue;
                }
                lookupCh = pattern[patOff = lastPatternChOff];
            }
        }
        return -1;
    }
    
    private final void readPattern(final char[] pattern) throws IOException {
        this.skipSpaces();
        for (final char c : pattern) {
            if (this.pdfSource.read() != c) {
                throw new IOException("Expected pattern '" + new String(pattern) + " but missed at character '" + c + "'");
            }
        }
        this.skipSpaces();
    }
    
    private COSDictionary getPagesObject() throws IOException {
        if (this.pagesDictionary != null) {
            return this.pagesDictionary;
        }
        final COSObject pages = (COSObject)this.document.getCatalog().getItem(COSName.PAGES);
        if (pages == null) {
            throw new IOException("Missing PAGES entry in document catalog.");
        }
        final COSBase object = this.parseObjectDynamically(pages, false);
        if (!(object instanceof COSDictionary)) {
            throw new IOException("PAGES not a dictionary object, but: " + object.getClass().getSimpleName());
        }
        return this.pagesDictionary = (COSDictionary)object;
    }
    
    @Override
    public void parse() throws IOException {
        boolean exceptionOccurred = true;
        try {
            if (!this.initialParseDone) {
                this.initialParse();
            }
            final int pageCount = this.getPageNumber();
            if (!this.allPagesParsed) {
                for (int pNr = 0; pNr < pageCount; ++pNr) {
                    this.getPage(pNr);
                }
                this.allPagesParsed = true;
                this.document.setDecrypted();
            }
            exceptionOccurred = false;
        }
        finally {
            try {
                this.closeFileStream();
            }
            catch (IOException ex) {}
            if (exceptionOccurred && this.document != null) {
                try {
                    this.document.close();
                }
                catch (IOException ex2) {}
            }
        }
    }
    
    public SecurityHandler getSecurityHandler() {
        return this.securityHandler;
    }
    
    @Override
    public PDDocument getPDDocument() throws IOException {
        final PDDocument pdDocument = super.getPDDocument();
        if (this.securityHandler != null) {
            pdDocument.setSecurityHandler(this.securityHandler);
        }
        return pdDocument;
    }
    
    public int getPageNumber() throws IOException {
        final int pageCount = this.getPagesObject().getInt(COSName.COUNT);
        if (pageCount < 0) {
            throw new IOException("No page number specified.");
        }
        return pageCount;
    }
    
    public PDPage getPage(final int pageNr) throws IOException {
        this.getPagesObject();
        final COSArray kids = (COSArray)this.pagesDictionary.getDictionaryObject(COSName.KIDS);
        if (kids == null) {
            throw new IOException("Missing 'Kids' entry in pages dictionary.");
        }
        final COSObject pageObj = this.getPageObject(pageNr, kids, 0);
        if (pageObj == null) {
            throw new IOException("Page " + pageNr + " not found.");
        }
        final COSDictionary pageDict = (COSDictionary)pageObj.getObject();
        if (this.parseMinimalCatalog && !this.allPagesParsed) {
            final COSDictionary resDict = (COSDictionary)pageDict.getDictionaryObject(COSName.RESOURCES);
            this.parseDictObjects(resDict, new COSName[0]);
        }
        return new PDPage(pageDict);
    }
    
    private COSObject getPageObject(final int num, final COSArray startKids, final int startPageCount) throws IOException {
        int curPageCount = startPageCount;
        for (final COSObject obj : startKids) {
            COSBase base = obj.getObject();
            if (base == null) {
                base = this.parseObjectDynamically(obj, false);
                obj.setObject(base);
            }
            final COSDictionary dic = (COSDictionary)base;
            final int count = dic.getInt(COSName.COUNT);
            if (count >= 0 && curPageCount + count <= num) {
                curPageCount += count;
            }
            else {
                final COSArray kids = (COSArray)dic.getDictionaryObject(COSName.KIDS);
                if (kids != null) {
                    final COSObject ans = this.getPageObject(num, kids, curPageCount);
                    if (ans != null) {
                        return ans;
                    }
                    continue;
                }
                else {
                    if (curPageCount == num) {
                        return obj;
                    }
                    ++curPageCount;
                }
            }
        }
        return null;
    }
    
    private final long getObjectId(final COSObject obj) {
        return obj.getObjectNumber().longValue() << 32 | obj.getGenerationNumber().longValue();
    }
    
    private final void addNewToList(final Queue<COSBase> toBeParsedList, final Collection<COSBase> newObjects, final Set<Long> addedObjects) {
        for (final COSBase newObject : newObjects) {
            if (newObject instanceof COSObject) {
                final long objId = this.getObjectId((COSObject)newObject);
                if (!addedObjects.add(objId)) {
                    continue;
                }
            }
            toBeParsedList.add(newObject);
        }
    }
    
    private final void addNewToList(final Queue<COSBase> toBeParsedList, final COSBase newObject, final Set<Long> addedObjects) {
        if (newObject instanceof COSObject) {
            final long objId = this.getObjectId((COSObject)newObject);
            if (!addedObjects.add(objId)) {
                return;
            }
        }
        toBeParsedList.add(newObject);
    }
    
    private void parseDictObjects(final COSDictionary dict, final COSName... excludeObjects) throws IOException {
        final Queue<COSBase> toBeParsedList = new LinkedList<COSBase>();
        final TreeMap<Long, List<COSObject>> objToBeParsed = new TreeMap<Long, List<COSObject>>();
        final Set<Long> parsedObjects = new HashSet<Long>();
        final Set<Long> addedObjects = new HashSet<Long>();
        if (excludeObjects != null) {
            for (final COSName objName : excludeObjects) {
                final COSBase baseObj = dict.getItem(objName);
                if (baseObj instanceof COSObject) {
                    parsedObjects.add(this.getObjectId((COSObject)baseObj));
                }
            }
        }
        this.addNewToList(toBeParsedList, dict.getValues(), addedObjects);
        while (!toBeParsedList.isEmpty() || !objToBeParsed.isEmpty()) {
            COSBase baseObj2;
            while ((baseObj2 = toBeParsedList.poll()) != null) {
                if (baseObj2 instanceof COSStream) {
                    this.addNewToList(toBeParsedList, ((COSStream)baseObj2).getValues(), addedObjects);
                }
                else if (baseObj2 instanceof COSDictionary) {
                    this.addNewToList(toBeParsedList, ((COSDictionary)baseObj2).getValues(), addedObjects);
                }
                else if (baseObj2 instanceof COSArray) {
                    final Iterator<COSBase> arrIter = ((COSArray)baseObj2).iterator();
                    while (arrIter.hasNext()) {
                        this.addNewToList(toBeParsedList, arrIter.next(), addedObjects);
                    }
                }
                else {
                    if (!(baseObj2 instanceof COSObject)) {
                        continue;
                    }
                    final COSObject obj = (COSObject)baseObj2;
                    final long objId = this.getObjectId(obj);
                    final COSObjectKey objKey = new COSObjectKey(obj.getObjectNumber().intValue(), obj.getGenerationNumber().intValue());
                    if (parsedObjects.contains(objId)) {
                        continue;
                    }
                    Long fileOffset = this.xrefTrailerResolver.getXrefTable().get(objKey);
                    if (fileOffset != null) {
                        if (fileOffset > 0L) {
                            objToBeParsed.put(fileOffset, Collections.singletonList(obj));
                        }
                        else {
                            fileOffset = this.xrefTrailerResolver.getXrefTable().get(new COSObjectKey(-fileOffset, 0L));
                            if (fileOffset == null || fileOffset <= 0L) {
                                throw new IOException("Invalid object stream xref object reference: " + fileOffset);
                            }
                            List<COSObject> stmObjects = objToBeParsed.get(fileOffset);
                            if (stmObjects == null) {
                                objToBeParsed.put(fileOffset, stmObjects = new ArrayList<COSObject>());
                            }
                            stmObjects.add(obj);
                        }
                    }
                    else {
                        final COSObject pdfObject = this.document.getObjectFromPool(objKey);
                        pdfObject.setObject(COSNull.NULL);
                    }
                }
            }
            if (objToBeParsed.isEmpty()) {
                break;
            }
            for (final COSObject obj2 : objToBeParsed.remove(objToBeParsed.firstKey())) {
                final COSBase parsedObj = this.parseObjectDynamically(obj2, false);
                obj2.setObject(parsedObj);
                this.addNewToList(toBeParsedList, parsedObj, addedObjects);
                parsedObjects.add(this.getObjectId(obj2));
            }
        }
    }
    
    private COSBase parseObjectDynamically(final COSObject obj, final boolean requireExistingNotCompressedObj) throws IOException {
        return this.parseObjectDynamically(obj.getObjectNumber().intValue(), obj.getGenerationNumber().intValue(), requireExistingNotCompressedObj);
    }
    
    private COSBase parseObjectDynamically(final int objNr, final int objGenNr, final boolean requireExistingNotCompressedObj) throws IOException {
        final COSObjectKey objKey = new COSObjectKey(objNr, objGenNr);
        final COSObject pdfObject = this.document.getObjectFromPool(objKey);
        if (pdfObject.getObject() == null) {
            final Long offsetOrObjstmObNr = this.xrefTrailerResolver.getXrefTable().get(objKey);
            if (requireExistingNotCompressedObj && (offsetOrObjstmObNr == null || offsetOrObjstmObNr <= 0L)) {
                throw new IOException("Object must be defined and must not be compressed object: " + objKey.getNumber() + ":" + objKey.getGeneration());
            }
            if (offsetOrObjstmObNr == null) {
                pdfObject.setObject(COSNull.NULL);
            }
            else if (offsetOrObjstmObNr > 0L) {
                this.setPdfSource(offsetOrObjstmObNr);
                final int readObjNr = this.readInt();
                final int readObjGen = this.readInt();
                this.readPattern(NonSequentialPDFParser.OBJ_MARKER);
                if (readObjNr != objKey.getNumber() || readObjGen != objKey.getGeneration()) {
                    throw new IOException("XREF for " + objKey.getNumber() + ":" + objKey.getGeneration() + " points to wrong object: " + readObjNr + ":" + readObjGen);
                }
                this.skipSpaces();
                COSBase pb = this.parseDirObject();
                String endObjectKey = this.readString();
                if (endObjectKey.equals("stream")) {
                    this.pdfSource.unread(endObjectKey.getBytes("ISO-8859-1"));
                    this.pdfSource.unread(32);
                    if (!(pb instanceof COSDictionary)) {
                        throw new IOException("Stream not preceded by dictionary (offset: " + offsetOrObjstmObNr + ").");
                    }
                    final COSStream stream = this.parseCOSStream((COSDictionary)pb, this.getDocument().getScratchFile());
                    if (this.securityHandler != null) {
                        try {
                            this.securityHandler.decryptStream(stream, objNr, objGenNr);
                        }
                        catch (CryptographyException ce) {
                            throw new IOException("Error decrypting stream object " + objNr + ": " + ce.getMessage());
                        }
                    }
                    pb = stream;
                    this.skipSpaces();
                    endObjectKey = this.readLine();
                    if (!endObjectKey.startsWith("endobj") && endObjectKey.startsWith("endstream")) {
                        endObjectKey = endObjectKey.substring(9).trim();
                        if (endObjectKey.length() == 0) {
                            endObjectKey = this.readLine();
                        }
                    }
                }
                else if (this.securityHandler != null) {
                    if (pb instanceof COSString) {
                        this.decrypt((COSString)pb, objNr, objGenNr);
                    }
                    else if (pb instanceof COSDictionary) {
                        for (final Map.Entry<COSName, COSBase> entry : ((COSDictionary)pb).entrySet()) {
                            if (entry.getValue() instanceof COSString) {
                                this.decrypt(entry.getValue(), objNr, objGenNr);
                            }
                        }
                    }
                    else if (pb instanceof COSArray) {
                        final COSArray array = (COSArray)pb;
                        for (int aIdx = 0, len = array.size(); aIdx < len; ++aIdx) {
                            if (array.get(aIdx) instanceof COSString) {
                                this.decrypt((COSString)array.get(aIdx), objNr, objGenNr);
                            }
                        }
                    }
                }
                pdfObject.setObject(pb);
                if (!endObjectKey.startsWith("endobj")) {
                    throw new IOException("Object (" + readObjNr + ":" + readObjGen + ") at offset " + offsetOrObjstmObNr + " does not end with 'endobj'.");
                }
                this.releasePdfSourceInputStream();
            }
            else {
                final int objstmObjNr = (Object)(-offsetOrObjstmObNr);
                final COSBase objstmBaseObj = this.parseObjectDynamically(objstmObjNr, 0, true);
                if (objstmBaseObj instanceof COSStream) {
                    final PDFObjectStreamParser parser = new PDFObjectStreamParser((COSStream)objstmBaseObj, this.document, this.forceParsing);
                    parser.parse();
                    final Set<Long> refObjNrs = this.xrefTrailerResolver.getContainedObjectNumbers(objstmObjNr);
                    for (final COSObject next : parser.getObjects()) {
                        final COSObjectKey stmObjKey = new COSObjectKey(next);
                        if (refObjNrs.contains(stmObjKey.getNumber())) {
                            final COSObject stmObj = this.document.getObjectFromPool(stmObjKey);
                            stmObj.setObject(next.getObject());
                        }
                    }
                }
            }
        }
        return pdfObject.getObject();
    }
    
    private final void decrypt(final COSString str, final long objNr, final long objGenNr) throws IOException {
        try {
            this.securityHandler.decryptString(str, objNr, objGenNr);
        }
        catch (CryptographyException ce) {
            throw new IOException("Error decrypting string: " + ce.getMessage());
        }
    }
    
    private COSNumber getLength(final COSBase lengthBaseObj) throws IOException {
        if (lengthBaseObj == null) {
            return null;
        }
        if (this.inGetLength) {
            throw new IOException("Loop while reading length from " + lengthBaseObj);
        }
        COSNumber retVal = null;
        try {
            this.inGetLength = true;
            if (lengthBaseObj instanceof COSNumber) {
                retVal = (COSNumber)lengthBaseObj;
            }
            else {
                if (!(lengthBaseObj instanceof COSObject)) {
                    throw new IOException("Wrong type of length object: " + lengthBaseObj.getClass().getSimpleName());
                }
                final COSObject lengthObj = (COSObject)lengthBaseObj;
                if (lengthObj.getObject() == null) {
                    final long curFileOffset = this.getPdfSourceOffset();
                    this.releasePdfSourceInputStream();
                    this.parseObjectDynamically(lengthObj, true);
                    this.setPdfSource(curFileOffset);
                    if (lengthObj.getObject() == null) {
                        throw new IOException("Length object content was not read.");
                    }
                }
                if (!(lengthObj.getObject() instanceof COSNumber)) {
                    throw new IOException("Wrong type of referenced length object " + lengthObj + ": " + lengthObj.getObject().getClass().getSimpleName());
                }
                retVal = (COSNumber)lengthObj.getObject();
            }
        }
        finally {
            this.inGetLength = false;
        }
        return retVal;
    }
    
    @Override
    protected COSStream parseCOSStream(final COSDictionary dic, final RandomAccess file) throws IOException {
        final COSStream stream = new COSStream(dic, file);
        OutputStream out = null;
        try {
            this.readString();
            int whitespace;
            for (whitespace = this.pdfSource.read(); whitespace == 32; whitespace = this.pdfSource.read()) {}
            if (whitespace == 13) {
                whitespace = this.pdfSource.read();
                if (whitespace != 10) {
                    this.pdfSource.unread(whitespace);
                }
            }
            else if (whitespace != 10) {
                this.pdfSource.unread(whitespace);
            }
            final COSNumber streamLengthObj = this.getLength(dic.getItem(COSName.LENGTH));
            if (streamLengthObj == null) {
                throw new IOException("Missing length for stream.");
            }
            out = stream.createFilteredStream(streamLengthObj);
            int readBytes;
            for (long remainBytes = streamLengthObj.longValue(); remainBytes > 0L; remainBytes -= readBytes) {
                readBytes = this.pdfSource.read(this.streamCopyBuf, 0, (remainBytes > 8192L) ? 8192 : ((int)remainBytes));
                if (readBytes <= 0) {
                    throw new IOException("No more bytes from stream but expected: " + remainBytes);
                }
                out.write(this.streamCopyBuf, 0, readBytes);
            }
            final String endStream = this.readString();
            if (!endStream.equals("endstream")) {
                throw new IOException("Error reading stream using length value. Expected='endstream' actual='" + endStream + "' ");
            }
        }
        finally {
            if (out != null) {
                out.close();
            }
        }
        return stream;
    }
    
    static {
        EMPTY_INPUT_STREAM = new ByteArrayInputStream(new byte[0]);
        EOF_MARKER = new char[] { '%', '%', 'E', 'O', 'F' };
        STARTXREF_MARKER = new char[] { 's', 't', 'a', 'r', 't', 'x', 'r', 'e', 'f' };
        OBJ_MARKER = new char[] { 'o', 'b', 'j' };
        LOG = LogFactory.getLog(NonSequentialPDFParser.class);
    }
}
