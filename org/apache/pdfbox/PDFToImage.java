// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox;

import java.util.List;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.PDPage;
import javax.imageio.ImageIO;
import org.apache.pdfbox.util.PDFImageWriter;
import org.apache.pdfbox.exceptions.InvalidPasswordException;
import org.apache.pdfbox.io.RandomAccess;
import org.apache.pdfbox.pdmodel.PDDocument;
import java.io.File;
import java.awt.HeadlessException;
import java.awt.Toolkit;

public class PDFToImage
{
    private static final String PASSWORD = "-password";
    private static final String START_PAGE = "-startPage";
    private static final String END_PAGE = "-endPage";
    private static final String IMAGE_FORMAT = "-imageType";
    private static final String OUTPUT_PREFIX = "-outputPrefix";
    private static final String COLOR = "-color";
    private static final String RESOLUTION = "-resolution";
    private static final String CROPBOX = "-cropbox";
    private static final String NONSEQ = "-nonSeq";
    
    private PDFToImage() {
    }
    
    public static void main(final String[] args) throws Exception {
        boolean useNonSeqParser = false;
        String password = "";
        String pdfFile = null;
        String outputPrefix = null;
        String imageFormat = "jpg";
        int startPage = 1;
        int endPage = Integer.MAX_VALUE;
        String color = "rgb";
        float cropBoxLowerLeftX = 0.0f;
        float cropBoxLowerLeftY = 0.0f;
        float cropBoxUpperRightX = 0.0f;
        float cropBoxUpperRightY = 0.0f;
        int resolution;
        try {
            resolution = Toolkit.getDefaultToolkit().getScreenResolution();
        }
        catch (HeadlessException e2) {
            resolution = 96;
        }
        for (int i = 0; i < args.length; ++i) {
            if (args[i].equals("-password")) {
                if (++i >= args.length) {
                    usage();
                }
                password = args[i];
            }
            else if (args[i].equals("-startPage")) {
                if (++i >= args.length) {
                    usage();
                }
                startPage = Integer.parseInt(args[i]);
            }
            else if (args[i].equals("-endPage")) {
                if (++i >= args.length) {
                    usage();
                }
                endPage = Integer.parseInt(args[i]);
            }
            else if (args[i].equals("-imageType")) {
                ++i;
                imageFormat = args[i];
            }
            else if (args[i].equals("-outputPrefix")) {
                ++i;
                outputPrefix = args[i];
            }
            else if (args[i].equals("-color")) {
                ++i;
                color = args[i];
            }
            else if (args[i].equals("-resolution")) {
                ++i;
                resolution = Integer.parseInt(args[i]);
            }
            else if (args[i].equals("-cropbox")) {
                ++i;
                cropBoxLowerLeftX = Float.valueOf(args[i]);
                ++i;
                cropBoxLowerLeftY = Float.valueOf(args[i]);
                ++i;
                cropBoxUpperRightX = Float.valueOf(args[i]);
                ++i;
                cropBoxUpperRightY = Float.valueOf(args[i]);
            }
            else if (args[i].equals("-nonSeq")) {
                useNonSeqParser = true;
            }
            else if (pdfFile == null) {
                pdfFile = args[i];
            }
        }
        if (pdfFile == null) {
            usage();
        }
        else {
            if (outputPrefix == null) {
                outputPrefix = pdfFile.substring(0, pdfFile.lastIndexOf(46));
            }
            PDDocument document = null;
            try {
                if (useNonSeqParser) {
                    document = PDDocument.loadNonSeq(new File(pdfFile), null, password);
                }
                else {
                    document = PDDocument.load(pdfFile);
                    if (document.isEncrypted()) {
                        try {
                            document.decrypt(password);
                        }
                        catch (InvalidPasswordException e3) {
                            if (args.length == 4) {
                                System.err.println("Error: The supplied password is incorrect.");
                                System.exit(2);
                            }
                            else {
                                System.err.println("Error: The document is encrypted.");
                                usage();
                            }
                        }
                    }
                }
                int imageType = 24;
                if ("bilevel".equalsIgnoreCase(color)) {
                    imageType = 12;
                }
                else if ("indexed".equalsIgnoreCase(color)) {
                    imageType = 13;
                }
                else if ("gray".equalsIgnoreCase(color)) {
                    imageType = 10;
                }
                else if ("rgb".equalsIgnoreCase(color)) {
                    imageType = 1;
                }
                else if ("rgba".equalsIgnoreCase(color)) {
                    imageType = 2;
                }
                else {
                    System.err.println("Error: the number of bits per pixel must be 1, 8 or 24.");
                    System.exit(2);
                }
                if (cropBoxLowerLeftX != 0.0f || cropBoxLowerLeftY != 0.0f || cropBoxUpperRightX != 0.0f || cropBoxUpperRightY != 0.0f) {
                    changeCropBoxes(document, cropBoxLowerLeftX, cropBoxLowerLeftY, cropBoxUpperRightX, cropBoxUpperRightY);
                }
                final PDFImageWriter imageWriter = new PDFImageWriter();
                final boolean success = imageWriter.writeImage(document, imageFormat, password, startPage, endPage, outputPrefix, imageType, resolution);
                if (!success) {
                    System.err.println("Error: no writer found for image format '" + imageFormat + "'");
                    System.exit(1);
                }
            }
            catch (Exception e) {
                System.err.println(e);
            }
            finally {
                if (document != null) {
                    document.close();
                }
            }
        }
    }
    
    private static void usage() {
        System.err.println("Usage: java -jar pdfbox-app-x.y.z.jar PDFToImage [OPTIONS] <PDF file>\n  -password  <password>          Password to decrypt document\n  -imageType <image type>        (" + getImageFormats() + ")\n" + "  -outputPrefix <output prefix>  Filename prefix for image files\n" + "  -startPage <number>            The first page to start extraction(1 based)\n" + "  -endPage <number>              The last page to extract(inclusive)\n" + "  -color <string>                The color depth (valid: bilevel, indexed, gray, rgb, rgba)\n" + "  -resolution <number>           The bitmap resolution in dpi\n" + "  -cropbox <number> <number> <number> <number> The page area to export\n" + "  -nonSeq                        Enables the new non-sequential parser\n" + "  <PDF file>                     The PDF document to use\n");
        System.exit(1);
    }
    
    private static String getImageFormats() {
        final StringBuffer retval = new StringBuffer();
        final String[] formats = ImageIO.getReaderFormatNames();
        for (int i = 0; i < formats.length; ++i) {
            retval.append(formats[i]);
            if (i + 1 < formats.length) {
                retval.append(",");
            }
        }
        return retval.toString();
    }
    
    private static void changeCropBoxes(final PDDocument document, final float a, final float b, final float c, final float d) {
        final List pages = document.getDocumentCatalog().getAllPages();
        for (int i = 0; i < pages.size(); ++i) {
            System.out.println("resizing page");
            final PDPage page = pages.get(i);
            final PDRectangle rectangle = new PDRectangle();
            rectangle.setLowerLeftX(a);
            rectangle.setLowerLeftY(b);
            rectangle.setUpperRightX(c);
            rectangle.setUpperRightY(d);
            page.setMediaBox(rectangle);
            page.setCropBox(rectangle);
        }
    }
}
