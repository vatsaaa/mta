// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.exceptions;

public class SignatureException extends Exception
{
    public static final int WRONG_PASSWORD = 1;
    public static final int UNSUPPORTED_OPERATION = 2;
    public static final int CERT_PATH_CHECK_INVALID = 3;
    public static final int NO_SUCH_ALGORITHM = 4;
    public static final int INVALID_PAGE_FOR_SIGNATURE = 5;
    public static final int VISUAL_SIGNATURE_INVALID = 6;
    private int no;
    
    public SignatureException(final String msg) {
        super(msg);
    }
    
    public SignatureException(final int errno, final String msg) {
        super(msg);
        this.no = errno;
    }
    
    public SignatureException(final Throwable e) {
        super(e);
    }
    
    public SignatureException(final int errno, final Throwable e) {
        super(e);
    }
    
    public int getErrNo() {
        return this.no;
    }
}
