// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.exceptions;

import java.io.PrintStream;

public class WrappedException extends Exception
{
    private Exception wrapped;
    
    public WrappedException(final Exception e) {
        this.wrapped = null;
        this.wrapped = e;
    }
    
    @Override
    public String getMessage() {
        return this.wrapped.getMessage();
    }
    
    @Override
    public void printStackTrace(final PrintStream s) {
        super.printStackTrace(s);
        this.wrapped.printStackTrace(s);
    }
}
