// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.exceptions;

public class InvalidPasswordException extends Exception
{
    public InvalidPasswordException(final String msg) {
        super(msg);
    }
}
