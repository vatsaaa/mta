// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.exceptions;

import java.io.IOException;

public class OutlineNotLocalException extends IOException
{
    public OutlineNotLocalException(final String msg) {
        super(msg);
    }
}
