// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.exceptions;

import java.io.IOException;

public class WrappedIOException extends IOException
{
    public WrappedIOException(final Throwable e) {
        this.initCause(e);
    }
    
    public WrappedIOException(final String message, final Throwable e) {
        super(message);
        this.initCause(e);
    }
}
