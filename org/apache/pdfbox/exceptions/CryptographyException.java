// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.exceptions;

public class CryptographyException extends Exception
{
    private Exception embedded;
    
    public CryptographyException(final String msg) {
        super(msg);
    }
    
    public CryptographyException(final Exception e) {
        super(e.getMessage());
        this.setEmbedded(e);
    }
    
    public Exception getEmbedded() {
        return this.embedded;
    }
    
    private void setEmbedded(final Exception e) {
        this.embedded = e;
    }
}
