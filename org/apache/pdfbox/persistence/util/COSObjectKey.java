// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox.persistence.util;

import org.apache.pdfbox.cos.COSObject;

public class COSObjectKey implements Comparable<COSObjectKey>
{
    private long number;
    private long generation;
    
    public COSObjectKey(final COSObject object) {
        this(object.getObjectNumber().longValue(), object.getGenerationNumber().longValue());
    }
    
    public COSObjectKey(final long num, final long gen) {
        this.setNumber(num);
        this.setGeneration(gen);
    }
    
    @Override
    public boolean equals(final Object obj) {
        return obj instanceof COSObjectKey && ((COSObjectKey)obj).getNumber() == this.getNumber() && ((COSObjectKey)obj).getGeneration() == this.getGeneration();
    }
    
    public long getGeneration() {
        return this.generation;
    }
    
    public long getNumber() {
        return this.number;
    }
    
    @Override
    public int hashCode() {
        return (int)(this.number + this.generation);
    }
    
    public void setGeneration(final long newGeneration) {
        this.generation = newGeneration;
    }
    
    public void setNumber(final long newNumber) {
        this.number = newNumber;
    }
    
    @Override
    public String toString() {
        return "" + this.getNumber() + " " + this.getGeneration() + " R";
    }
    
    public int compareTo(final COSObjectKey other) {
        if (this.getNumber() < other.getNumber()) {
            return -1;
        }
        if (this.getNumber() > other.getNumber()) {
            return 1;
        }
        if (this.getGeneration() < other.getGeneration()) {
            return -1;
        }
        if (this.getGeneration() > other.getGeneration()) {
            return 1;
        }
        return 0;
    }
}
