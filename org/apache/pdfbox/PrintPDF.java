// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.pdfbox;

import javax.print.PrintService;
import java.io.File;
import java.awt.print.PrinterJob;
import org.apache.pdfbox.pdmodel.PDDocument;

public class PrintPDF
{
    private static final String PASSWORD = "-password";
    private static final String SILENT = "-silentPrint";
    private static final String PRINTER_NAME = "-printerName";
    
    private PrintPDF() {
    }
    
    public static void main(final String[] args) throws Exception {
        String password = "";
        String pdfFile = null;
        boolean silentPrint = false;
        String printerName = null;
        for (int i = 0; i < args.length; ++i) {
            if (args[i].equals("-password")) {
                if (++i >= args.length) {
                    usage();
                }
                password = args[i];
            }
            else if (args[i].equals("-printerName")) {
                if (++i >= args.length) {
                    usage();
                }
                printerName = args[i];
            }
            else if (args[i].equals("-silentPrint")) {
                silentPrint = true;
            }
            else {
                pdfFile = args[i];
            }
        }
        if (pdfFile == null) {
            usage();
        }
        PDDocument document = null;
        try {
            document = PDDocument.load(pdfFile);
            if (document.isEncrypted()) {
                document.decrypt(password);
            }
            final PrinterJob printJob = PrinterJob.getPrinterJob();
            printJob.setJobName(new File(pdfFile).getName());
            if (printerName != null) {
                final PrintService[] printService = PrinterJob.lookupPrintServices();
                boolean printerFound = false;
                for (int j = 0; !printerFound && j < printService.length; ++j) {
                    if (printService[j].getName().indexOf(printerName) != -1) {
                        printJob.setPrintService(printService[j]);
                        printerFound = true;
                    }
                }
            }
            if (silentPrint) {
                document.silentPrint(printJob);
            }
            else {
                document.print(printJob);
            }
        }
        finally {
            if (document != null) {
                document.close();
            }
        }
    }
    
    private static void usage() {
        System.err.println("Usage: java -jar pdfbox-app-x.y.z.jar PrintPDF [OPTIONS] <PDF file>\n  -password  <password>        Password to decrypt document\n  -silentPrint                 Print without prompting for printer info\n");
        System.exit(1);
    }
}
