// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.xmlbeans;

import java.math.BigInteger;
import javax.xml.namespace.QName;

public interface SchemaField
{
    QName getName();
    
    boolean isAttribute();
    
    boolean isNillable();
    
    SchemaType getType();
    
    BigInteger getMinOccurs();
    
    BigInteger getMaxOccurs();
    
    String getDefaultText();
    
    XmlAnySimpleType getDefaultValue();
    
    boolean isDefault();
    
    boolean isFixed();
    
    Object getUserData();
}
