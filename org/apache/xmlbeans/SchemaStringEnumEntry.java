// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.xmlbeans;

public interface SchemaStringEnumEntry
{
    String getString();
    
    int getIntValue();
    
    String getEnumName();
}
