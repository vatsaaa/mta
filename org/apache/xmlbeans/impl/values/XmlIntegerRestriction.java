// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.xmlbeans.impl.values;

import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlInteger;

public class XmlIntegerRestriction extends JavaIntegerHolderEx implements XmlInteger
{
    public XmlIntegerRestriction(final SchemaType type, final boolean complex) {
        super(type, complex);
    }
}
