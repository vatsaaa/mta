// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.xmlbeans.impl.values;

import org.apache.xmlbeans.XmlObject;
import java.util.Arrays;
import org.apache.xmlbeans.impl.common.ValidationContext;
import org.apache.xmlbeans.impl.common.XMLChar;
import java.util.ArrayList;
import org.apache.xmlbeans.impl.common.PrefixResolver;
import org.apache.xmlbeans.impl.common.QNameHelper;
import org.apache.xmlbeans.SimpleValue;
import java.util.List;
import org.apache.xmlbeans.XmlSimpleList;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlAnySimpleType;

public class XmlListImpl extends XmlObjectBase implements XmlAnySimpleType
{
    private SchemaType _schemaType;
    private XmlSimpleList _value;
    private XmlSimpleList _jvalue;
    private static final String[] EMPTY_STRINGARRAY;
    
    public XmlListImpl(final SchemaType type, final boolean complex) {
        this._schemaType = type;
        this.initComplexType(complex, false);
    }
    
    public SchemaType schemaType() {
        return this._schemaType;
    }
    
    private static String nullAsEmpty(final String s) {
        if (s == null) {
            return "";
        }
        return s;
    }
    
    private static String compute_list_text(final List xList) {
        if (xList.size() == 0) {
            return "";
        }
        final StringBuffer sb = new StringBuffer();
        sb.append(nullAsEmpty(xList.get(0).getStringValue()));
        for (int i = 1; i < xList.size(); ++i) {
            sb.append(' ');
            sb.append(nullAsEmpty(xList.get(i).getStringValue()));
        }
        return sb.toString();
    }
    
    protected String compute_text(final NamespaceManager nsm) {
        return compute_list_text(this._value);
    }
    
    protected boolean is_defaultable_ws(final String v) {
        try {
            final XmlSimpleList savedValue = this._value;
            this.set_text(v);
            this._value = savedValue;
            return false;
        }
        catch (XmlValueOutOfRangeException e) {
            return true;
        }
    }
    
    protected void set_text(final String s) {
        if (this._validateOnSet() && !this._schemaType.matchPatternFacet(s)) {
            throw new XmlValueOutOfRangeException("cvc-datatype-valid.1.1", new Object[] { "list", s, QNameHelper.readable(this._schemaType) });
        }
        final SchemaType itemType = this._schemaType.getListItemType();
        final XmlSimpleList newval = lex(s, itemType, XmlListImpl._voorVc, this.has_store() ? this.get_store() : null);
        if (this._validateOnSet()) {
            validateValue(newval, this._schemaType, XmlListImpl._voorVc);
        }
        this._value = newval;
        this._jvalue = null;
    }
    
    public static String[] split_list(final String s) {
        if (s.length() == 0) {
            return XmlListImpl.EMPTY_STRINGARRAY;
        }
        final List result = new ArrayList();
        int i = 0;
        int start = 0;
        while (true) {
            if (i < s.length() && XMLChar.isSpace(s.charAt(i))) {
                ++i;
            }
            else {
                if (i >= s.length()) {
                    break;
                }
                start = i;
                while (i < s.length() && !XMLChar.isSpace(s.charAt(i))) {
                    ++i;
                }
                result.add(s.substring(start, i));
            }
        }
        return result.toArray(XmlListImpl.EMPTY_STRINGARRAY);
    }
    
    public static XmlSimpleList lex(final String s, final SchemaType itemType, final ValidationContext ctx, final PrefixResolver resolver) {
        final String[] parts = split_list(s);
        final XmlAnySimpleType[] newArray = new XmlAnySimpleType[parts.length];
        boolean pushed = false;
        if (resolver != null) {
            NamespaceContext.push(new NamespaceContext(resolver));
            pushed = true;
        }
        int i = 0;
        try {
            for (i = 0; i < parts.length; ++i) {
                try {
                    newArray[i] = itemType.newValue(parts[i]);
                }
                catch (XmlValueOutOfRangeException e) {
                    ctx.invalid("list", new Object[] { "item '" + parts[i] + "' is not a valid value of " + QNameHelper.readable(itemType) });
                }
            }
        }
        finally {
            if (pushed) {
                NamespaceContext.pop();
            }
        }
        return new XmlSimpleList(Arrays.asList(newArray));
    }
    
    protected void set_nil() {
        this._value = null;
    }
    
    public List xlistValue() {
        this.check_dated();
        return this._value;
    }
    
    public List listValue() {
        this.check_dated();
        if (this._value == null) {
            return null;
        }
        if (this._jvalue != null) {
            return this._jvalue;
        }
        final List javaResult = new ArrayList();
        for (int i = 0; i < this._value.size(); ++i) {
            javaResult.add(XmlObjectBase.java_value((XmlObject)this._value.get(i)));
        }
        return this._jvalue = new XmlSimpleList(javaResult);
    }
    
    private static boolean permits_inner_space(final XmlObject obj) {
        switch (((SimpleValue)obj).instanceType().getPrimitiveType().getBuiltinTypeCode()) {
            case 1:
            case 2:
            case 6:
            case 12: {
                return true;
            }
            default: {
                return false;
            }
        }
    }
    
    private static boolean contains_white_space(final String s) {
        return s.indexOf(32) >= 0 || s.indexOf(9) >= 0 || s.indexOf(10) >= 0 || s.indexOf(13) >= 0;
    }
    
    public void set_list(final List list) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: getfield        org/apache/xmlbeans/impl/values/XmlListImpl._schemaType:Lorg/apache/xmlbeans/SchemaType;
        //     4: invokeinterface org/apache/xmlbeans/SchemaType.getListItemType:()Lorg/apache/xmlbeans/SchemaType;
        //     9: astore_2        /* itemType */
        //    10: iconst_0       
        //    11: istore          pushed
        //    13: aload_0         /* this */
        //    14: invokevirtual   org/apache/xmlbeans/impl/values/XmlListImpl.has_store:()Z
        //    17: ifeq            37
        //    20: new             Lorg/apache/xmlbeans/impl/values/NamespaceContext;
        //    23: dup            
        //    24: aload_0         /* this */
        //    25: invokevirtual   org/apache/xmlbeans/impl/values/XmlListImpl.get_store:()Lorg/apache/xmlbeans/impl/values/TypeStore;
        //    28: invokespecial   org/apache/xmlbeans/impl/values/NamespaceContext.<init>:(Lorg/apache/xmlbeans/impl/values/TypeStore;)V
        //    31: invokestatic    org/apache/xmlbeans/impl/values/NamespaceContext.push:(Lorg/apache/xmlbeans/impl/values/NamespaceContext;)V
        //    34: iconst_1       
        //    35: istore          pushed
        //    37: aload_1         /* list */
        //    38: invokeinterface java/util/List.size:()I
        //    43: anewarray       Lorg/apache/xmlbeans/XmlAnySimpleType;
        //    46: astore          newval
        //    48: iconst_0       
        //    49: istore          i
        //    51: iload           i
        //    53: aload_1         /* list */
        //    54: invokeinterface java/util/List.size:()I
        //    59: if_icmpge       145
        //    62: aload_1         /* list */
        //    63: iload           i
        //    65: invokeinterface java/util/List.get:(I)Ljava/lang/Object;
        //    70: astore          entry
        //    72: aload           entry
        //    74: instanceof      Lorg/apache/xmlbeans/XmlObject;
        //    77: ifeq            126
        //    80: aload_1         /* list */
        //    81: iload           i
        //    83: invokeinterface java/util/List.get:(I)Ljava/lang/Object;
        //    88: checkcast       Lorg/apache/xmlbeans/XmlObject;
        //    91: invokestatic    org/apache/xmlbeans/impl/values/XmlListImpl.permits_inner_space:(Lorg/apache/xmlbeans/XmlObject;)Z
        //    94: ifeq            126
        //    97: aload_1         /* list */
        //    98: iload           i
        //   100: invokeinterface java/util/List.get:(I)Ljava/lang/Object;
        //   105: invokevirtual   java/lang/Object.toString:()Ljava/lang/String;
        //   108: astore          stringrep
        //   110: aload           stringrep
        //   112: invokestatic    org/apache/xmlbeans/impl/values/XmlListImpl.contains_white_space:(Ljava/lang/String;)Z
        //   115: ifeq            126
        //   118: new             Lorg/apache/xmlbeans/impl/values/XmlValueOutOfRangeException;
        //   121: dup            
        //   122: invokespecial   org/apache/xmlbeans/impl/values/XmlValueOutOfRangeException.<init>:()V
        //   125: athrow         
        //   126: aload           newval
        //   128: iload           i
        //   130: aload_2         /* itemType */
        //   131: aload           entry
        //   133: invokeinterface org/apache/xmlbeans/SchemaType.newValue:(Ljava/lang/Object;)Lorg/apache/xmlbeans/XmlAnySimpleType;
        //   138: aastore        
        //   139: iinc            i, 1
        //   142: goto            51
        //   145: new             Lorg/apache/xmlbeans/XmlSimpleList;
        //   148: dup            
        //   149: aload           newval
        //   151: invokestatic    java/util/Arrays.asList:([Ljava/lang/Object;)Ljava/util/List;
        //   154: invokespecial   org/apache/xmlbeans/XmlSimpleList.<init>:(Ljava/util/List;)V
        //   157: astore_3        /* xList */
        //   158: iload           pushed
        //   160: ifeq            182
        //   163: invokestatic    org/apache/xmlbeans/impl/values/NamespaceContext.pop:()V
        //   166: goto            182
        //   169: astore          9
        //   171: iload           pushed
        //   173: ifeq            179
        //   176: invokestatic    org/apache/xmlbeans/impl/values/NamespaceContext.pop:()V
        //   179: aload           9
        //   181: athrow         
        //   182: aload_0         /* this */
        //   183: invokevirtual   org/apache/xmlbeans/impl/values/XmlListImpl._validateOnSet:()Z
        //   186: ifeq            200
        //   189: aload_3        
        //   190: aload_0         /* this */
        //   191: getfield        org/apache/xmlbeans/impl/values/XmlListImpl._schemaType:Lorg/apache/xmlbeans/SchemaType;
        //   194: getstatic       org/apache/xmlbeans/impl/values/XmlListImpl._voorVc:Lorg/apache/xmlbeans/impl/common/ValidationContext;
        //   197: invokestatic    org/apache/xmlbeans/impl/values/XmlListImpl.validateValue:(Lorg/apache/xmlbeans/XmlSimpleList;Lorg/apache/xmlbeans/SchemaType;Lorg/apache/xmlbeans/impl/common/ValidationContext;)V
        //   200: aload_0         /* this */
        //   201: aload_3        
        //   202: putfield        org/apache/xmlbeans/impl/values/XmlListImpl._value:Lorg/apache/xmlbeans/XmlSimpleList;
        //   205: aload_0         /* this */
        //   206: aconst_null    
        //   207: putfield        org/apache/xmlbeans/impl/values/XmlListImpl._jvalue:Lorg/apache/xmlbeans/XmlSimpleList;
        //   210: return         
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type
        //  -----  -----  -----  -----  ----
        //  37     158    169    182    Any
        //  169    171    169    182    Any
        // 
        // The error that occurred was:
        // 
        // java.lang.NullPointerException
        //     at com.strobel.decompiler.ast.AstBuilder.convertLocalVariables(AstBuilder.java:2895)
        //     at com.strobel.decompiler.ast.AstBuilder.performStackAnalysis(AstBuilder.java:2445)
        //     at com.strobel.decompiler.ast.AstBuilder.build(AstBuilder.java:108)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:211)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:782)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:675)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:552)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:519)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:161)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:150)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:125)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:330)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:251)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:126)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public static void validateValue(final XmlSimpleList items, final SchemaType sType, final ValidationContext context) {
        final XmlObject[] enumvals = sType.getEnumerationValues();
        Label_0075: {
            if (enumvals != null) {
                for (int i = 0; i < enumvals.length; ++i) {
                    if (equal_xmlLists(items, ((XmlObjectBase)enumvals[i]).xlistValue())) {
                        break Label_0075;
                    }
                }
                context.invalid("cvc-enumeration-valid", new Object[] { "list", items, QNameHelper.readable(sType) });
            }
        }
        XmlObject o;
        int j;
        if ((o = sType.getFacet(0)) != null && (j = ((SimpleValue)o).getIntValue()) != items.size()) {
            context.invalid("cvc-length-valid.2", new Object[] { items, new Integer(items.size()), new Integer(j), QNameHelper.readable(sType) });
        }
        if ((o = sType.getFacet(1)) != null && (j = ((SimpleValue)o).getIntValue()) > items.size()) {
            context.invalid("cvc-minLength-valid.2", new Object[] { items, new Integer(items.size()), new Integer(j), QNameHelper.readable(sType) });
        }
        if ((o = sType.getFacet(2)) != null && (j = ((SimpleValue)o).getIntValue()) < items.size()) {
            context.invalid("cvc-maxLength-valid.2", new Object[] { items, new Integer(items.size()), new Integer(j), QNameHelper.readable(sType) });
        }
    }
    
    protected boolean equal_to(final XmlObject obj) {
        return equal_xmlLists(this._value, ((XmlObjectBase)obj).xlistValue());
    }
    
    private static boolean equal_xmlLists(final List a, final List b) {
        if (a.size() != b.size()) {
            return false;
        }
        for (int i = 0; i < a.size(); ++i) {
            if (!a.get(i).equals(b.get(i))) {
                return false;
            }
        }
        return true;
    }
    
    protected int value_hash_code() {
        if (this._value == null) {
            return 0;
        }
        int hash = this._value.size();
        int incr = this._value.size() / 9;
        if (incr < 1) {
            incr = 1;
        }
        int i;
        for (i = 0; i < this._value.size(); i += incr) {
            hash *= 19;
            hash += this._value.get(i).hashCode();
        }
        if (i < this._value.size()) {
            hash *= 19;
            hash += this._value.get(i).hashCode();
        }
        return hash;
    }
    
    protected void validate_simpleval(final String lexical, final ValidationContext ctx) {
        validateValue((XmlSimpleList)this.xlistValue(), this.schemaType(), ctx);
    }
    
    static {
        EMPTY_STRINGARRAY = new String[0];
    }
}
