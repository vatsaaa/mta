// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.xmlbeans.impl.values;

import org.apache.xmlbeans.XmlObject;
import org.apache.xmlbeans.SchemaType;

public abstract class JavaNotationHolderEx extends JavaNotationHolder
{
    private SchemaType _schemaType;
    
    public SchemaType schemaType() {
        return this._schemaType;
    }
    
    public JavaNotationHolderEx(final SchemaType type, final boolean complex) {
        this._schemaType = type;
        this.initComplexType(complex, false);
    }
    
    protected int get_wscanon_rule() {
        return this.schemaType().getWhiteSpaceRule();
    }
    
    protected void set_text(final String s) {
        if (this._validateOnSet()) {
            if (!check(s, this._schemaType)) {
                throw new XmlValueOutOfRangeException();
            }
            if (!this._schemaType.matchPatternFacet(s)) {
                throw new XmlValueOutOfRangeException();
            }
        }
        super.set_text(s);
    }
    
    protected void set_notation(final String v) {
        this.set_text(v);
    }
    
    private static boolean check(final String v, final SchemaType sType) {
        final XmlObject len = sType.getFacet(0);
        if (len != null) {
            final int m = ((XmlObjectBase)len).bigIntegerValue().intValue();
            if (v.length() == m) {
                return false;
            }
        }
        final XmlObject min = sType.getFacet(1);
        if (min != null) {
            final int i = ((XmlObjectBase)min).bigIntegerValue().intValue();
            if (v.length() < i) {
                return false;
            }
        }
        final XmlObject max = sType.getFacet(2);
        if (max != null) {
            final int j = ((XmlObjectBase)max).bigIntegerValue().intValue();
            if (v.length() > j) {
                return false;
            }
        }
        return true;
    }
}
