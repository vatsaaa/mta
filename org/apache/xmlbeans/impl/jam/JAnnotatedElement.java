// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.xmlbeans.impl.jam;

public interface JAnnotatedElement extends JElement
{
    JAnnotation[] getAnnotations();
    
    JAnnotation getAnnotation(final Class p0);
    
    Object getAnnotationProxy(final Class p0);
    
    JAnnotation getAnnotation(final String p0);
    
    JAnnotationValue getAnnotationValue(final String p0);
    
    JComment getComment();
    
    JAnnotation[] getAllJavadocTags();
}
