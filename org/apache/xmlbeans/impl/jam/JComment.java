// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.xmlbeans.impl.jam;

public interface JComment extends JElement
{
    String getText();
    
    JSourcePosition getSourcePosition();
}
