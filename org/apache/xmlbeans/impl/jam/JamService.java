// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.xmlbeans.impl.jam;

public interface JamService
{
    JamClassLoader getClassLoader();
    
    String[] getClassNames();
    
    JamClassIterator getClasses();
    
    JClass[] getAllClasses();
}
