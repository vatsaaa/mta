// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.xmlbeans.impl.jam.mutable;

import org.apache.xmlbeans.impl.jam.JMember;

public interface MMember extends MAnnotatedElement, JMember
{
    void setModifiers(final int p0);
}
