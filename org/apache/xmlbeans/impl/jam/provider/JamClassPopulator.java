// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.xmlbeans.impl.jam.provider;

import org.apache.xmlbeans.impl.jam.mutable.MClass;

public interface JamClassPopulator
{
    void populate(final MClass p0);
}
