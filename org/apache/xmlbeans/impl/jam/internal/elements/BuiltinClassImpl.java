// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.xmlbeans.impl.jam.internal.elements;

import org.apache.xmlbeans.impl.jam.mutable.MMethod;
import org.apache.xmlbeans.impl.jam.mutable.MConstructor;
import org.apache.xmlbeans.impl.jam.mutable.MField;
import org.apache.xmlbeans.impl.jam.JProperty;
import org.apache.xmlbeans.impl.jam.JPackage;
import org.apache.xmlbeans.impl.jam.JMethod;
import org.apache.xmlbeans.impl.jam.JConstructor;
import org.apache.xmlbeans.impl.jam.JField;
import org.apache.xmlbeans.impl.jam.JSourcePosition;
import org.apache.xmlbeans.impl.jam.JClass;
import org.apache.xmlbeans.impl.jam.visitor.JVisitor;
import org.apache.xmlbeans.impl.jam.visitor.MVisitor;
import org.apache.xmlbeans.impl.jam.mutable.MClass;

public abstract class BuiltinClassImpl extends AnnotatedElementImpl implements MClass
{
    protected BuiltinClassImpl(final ElementContext ctx) {
        super(ctx);
    }
    
    public void accept(final MVisitor visitor) {
        visitor.visit(this);
    }
    
    public void accept(final JVisitor visitor) {
        visitor.visit(this);
    }
    
    public String getQualifiedName() {
        return this.mSimpleName;
    }
    
    public String getFieldDescriptor() {
        return this.mSimpleName;
    }
    
    public int getModifiers() {
        return Object.class.getModifiers();
    }
    
    public boolean isPublic() {
        return true;
    }
    
    public boolean isPackagePrivate() {
        return false;
    }
    
    public boolean isProtected() {
        return false;
    }
    
    public boolean isPrivate() {
        return false;
    }
    
    public JSourcePosition getSourcePosition() {
        return null;
    }
    
    public JClass getContainingClass() {
        return null;
    }
    
    public JClass forName(final String fd) {
        return this.getClassLoader().loadClass(fd);
    }
    
    public JClass getArrayComponentType() {
        return null;
    }
    
    public int getArrayDimensions() {
        return 0;
    }
    
    public JClass getSuperclass() {
        return null;
    }
    
    public JClass[] getInterfaces() {
        return BuiltinClassImpl.NO_CLASS;
    }
    
    public JField[] getFields() {
        return BuiltinClassImpl.NO_FIELD;
    }
    
    public JField[] getDeclaredFields() {
        return BuiltinClassImpl.NO_FIELD;
    }
    
    public JConstructor[] getConstructors() {
        return BuiltinClassImpl.NO_CONSTRUCTOR;
    }
    
    public JMethod[] getMethods() {
        return BuiltinClassImpl.NO_METHOD;
    }
    
    public JMethod[] getDeclaredMethods() {
        return BuiltinClassImpl.NO_METHOD;
    }
    
    public JPackage getContainingPackage() {
        return null;
    }
    
    public boolean isInterface() {
        return false;
    }
    
    public boolean isArrayType() {
        return false;
    }
    
    public boolean isAnnotationType() {
        return false;
    }
    
    public boolean isPrimitiveType() {
        return false;
    }
    
    public boolean isBuiltinType() {
        return true;
    }
    
    public boolean isUnresolvedType() {
        return false;
    }
    
    public boolean isObjectType() {
        return false;
    }
    
    public boolean isVoidType() {
        return false;
    }
    
    public boolean isEnumType() {
        return false;
    }
    
    public Class getPrimitiveClass() {
        return null;
    }
    
    public boolean isAbstract() {
        return false;
    }
    
    public boolean isFinal() {
        return false;
    }
    
    public boolean isStatic() {
        return false;
    }
    
    public JClass[] getClasses() {
        return BuiltinClassImpl.NO_CLASS;
    }
    
    public JProperty[] getProperties() {
        return BuiltinClassImpl.NO_PROPERTY;
    }
    
    public JProperty[] getDeclaredProperties() {
        return BuiltinClassImpl.NO_PROPERTY;
    }
    
    public JPackage[] getImportedPackages() {
        return BuiltinClassImpl.NO_PACKAGE;
    }
    
    public JClass[] getImportedClasses() {
        return BuiltinClassImpl.NO_CLASS;
    }
    
    public MField[] getMutableFields() {
        return BuiltinClassImpl.NO_FIELD;
    }
    
    public MConstructor[] getMutableConstructors() {
        return BuiltinClassImpl.NO_CONSTRUCTOR;
    }
    
    public MMethod[] getMutableMethods() {
        return BuiltinClassImpl.NO_METHOD;
    }
    
    public void setSimpleName(final String s) {
        this.nocando();
    }
    
    public void setIsAnnotationType(final boolean b) {
        this.nocando();
    }
    
    public void setIsInterface(final boolean b) {
        this.nocando();
    }
    
    public void setIsUnresolvedType(final boolean b) {
        this.nocando();
    }
    
    public void setIsEnumType(final boolean b) {
        this.nocando();
    }
    
    public void setSuperclass(final String qualifiedClassName) {
        this.nocando();
    }
    
    public void setSuperclassUnqualified(final String unqualifiedClassName) {
        this.nocando();
    }
    
    public void setSuperclass(final JClass clazz) {
        this.nocando();
    }
    
    public void addInterface(final String className) {
        this.nocando();
    }
    
    public void addInterfaceUnqualified(final String unqualifiedClassName) {
        this.nocando();
    }
    
    public void addInterface(final JClass interf) {
        this.nocando();
    }
    
    public void removeInterface(final String className) {
        this.nocando();
    }
    
    public void removeInterface(final JClass interf) {
        this.nocando();
    }
    
    public MConstructor addNewConstructor() {
        this.nocando();
        return null;
    }
    
    public void removeConstructor(final MConstructor constr) {
        this.nocando();
    }
    
    public MField addNewField() {
        this.nocando();
        return null;
    }
    
    public void removeField(final MField field) {
        this.nocando();
    }
    
    public MMethod addNewMethod() {
        this.nocando();
        return null;
    }
    
    public void removeMethod(final MMethod method) {
        this.nocando();
    }
    
    public void setModifiers(final int modifiers) {
        this.nocando();
    }
    
    public MClass addNewInnerClass(final String named) {
        this.nocando();
        return null;
    }
    
    public void removeInnerClass(final MClass inner) {
        this.nocando();
    }
    
    public JProperty addNewProperty(final String name, final JMethod m, final JMethod x) {
        this.nocando();
        return null;
    }
    
    public void removeProperty(final JProperty prop) {
        this.nocando();
    }
    
    public JProperty addNewDeclaredProperty(final String name, final JMethod m, final JMethod x) {
        this.nocando();
        return null;
    }
    
    public void removeDeclaredProperty(final JProperty prop) {
        this.nocando();
    }
    
    public boolean equals(final Object o) {
        return o instanceof JClass && ((JClass)o).getFieldDescriptor().equals(this.getFieldDescriptor());
    }
    
    public int hashCode() {
        return this.getFieldDescriptor().hashCode();
    }
    
    protected void reallySetSimpleName(final String name) {
        super.setSimpleName(name);
    }
    
    private void nocando() {
        throw new UnsupportedOperationException("Cannot alter builtin types");
    }
}
