// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.xmlbeans.impl.jam.internal.elements;

import java.net.URI;
import org.apache.xmlbeans.impl.jam.mutable.MSourcePosition;

public final class SourcePositionImpl implements MSourcePosition
{
    private int mColumn;
    private int mLine;
    private URI mURI;
    
    SourcePositionImpl() {
        this.mColumn = -1;
        this.mLine = -1;
        this.mURI = null;
    }
    
    public void setColumn(final int col) {
        this.mColumn = col;
    }
    
    public void setLine(final int line) {
        this.mLine = line;
    }
    
    public void setSourceURI(final URI uri) {
        this.mURI = uri;
    }
    
    public int getColumn() {
        return this.mColumn;
    }
    
    public int getLine() {
        return this.mLine;
    }
    
    public URI getSourceURI() {
        return this.mURI;
    }
}
