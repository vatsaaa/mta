// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.xmlbeans.impl.jam.internal.elements;

import org.apache.xmlbeans.impl.jam.JClass;
import org.apache.xmlbeans.impl.jam.JPackage;

public final class UnresolvedClassImpl extends BuiltinClassImpl
{
    private String mPackageName;
    
    public UnresolvedClassImpl(final String packageName, final String simpleName, final ElementContext ctx) {
        super(ctx);
        if (packageName == null) {
            throw new IllegalArgumentException("null pkg");
        }
        this.mPackageName = packageName;
        this.reallySetSimpleName(simpleName);
    }
    
    public String getQualifiedName() {
        return ((this.mPackageName.length() > 0) ? (this.mPackageName + '.') : "") + this.mSimpleName;
    }
    
    public String getFieldDescriptor() {
        return this.getQualifiedName();
    }
    
    public JPackage getContainingPackage() {
        return null;
    }
    
    public boolean isAssignableFrom(final JClass c) {
        return false;
    }
    
    public boolean isUnresolvedType() {
        return true;
    }
}
