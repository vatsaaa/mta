// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.xmlbeans.impl.jam.internal.elements;

import org.apache.xmlbeans.impl.jam.JComment;
import org.apache.xmlbeans.impl.jam.visitor.JVisitor;
import org.apache.xmlbeans.impl.jam.visitor.MVisitor;
import org.apache.xmlbeans.impl.jam.mutable.MComment;

public final class CommentImpl extends ElementImpl implements MComment
{
    private String mText;
    
    CommentImpl(final ElementImpl parent) {
        super(parent);
        this.mText = null;
    }
    
    public void setText(final String text) {
        this.mText = text;
    }
    
    public String getText() {
        return (this.mText == null) ? "" : this.mText;
    }
    
    public void accept(final MVisitor visitor) {
        visitor.visit(this);
    }
    
    public void accept(final JVisitor visitor) {
        visitor.visit(this);
    }
    
    public String getQualifiedName() {
        return this.getParent().getQualifiedName() + ".{comment}";
    }
}
