// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.xmlbeans.impl.jam.internal.classrefs;

import org.apache.xmlbeans.impl.jam.JClass;

public interface JClassRef
{
    JClass getRefClass();
    
    String getQualifiedName();
}
