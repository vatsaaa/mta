// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.xmlbeans.impl.jam.internal.reflect;

import org.apache.xmlbeans.impl.jam.mutable.MParameter;
import java.lang.reflect.Field;
import org.apache.xmlbeans.impl.jam.mutable.MField;
import java.lang.reflect.Constructor;
import org.apache.xmlbeans.impl.jam.mutable.MConstructor;
import java.lang.reflect.Method;
import org.apache.xmlbeans.impl.jam.mutable.MMember;
import org.apache.xmlbeans.impl.jam.mutable.MClass;

public final class ReflectTigerDelegateImpl_150 extends ReflectTigerDelegate
{
    public void populateAnnotationTypeIfNecessary(final Class cd, final MClass clazz, final ReflectClassBuilder builder) {
    }
    
    public void extractAnnotations(final MMember dest, final Method src) {
    }
    
    public void extractAnnotations(final MConstructor dest, final Constructor src) {
    }
    
    public void extractAnnotations(final MField dest, final Field src) {
    }
    
    public void extractAnnotations(final MClass dest, final Class src) {
    }
    
    public void extractAnnotations(final MParameter dest, final Method src, final int paramNum) {
    }
    
    public void extractAnnotations(final MParameter dest, final Constructor src, final int paramNum) {
    }
    
    public boolean isEnum(final Class clazz) {
        return false;
    }
    
    public Constructor getEnclosingConstructor(final Class clazz) {
        return null;
    }
    
    public Method getEnclosingMethod(final Class clazz) {
        return null;
    }
}
