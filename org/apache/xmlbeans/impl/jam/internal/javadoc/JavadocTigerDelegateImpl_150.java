// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.xmlbeans.impl.jam.internal.javadoc;

import com.sun.javadoc.Parameter;
import com.sun.javadoc.ExecutableMemberDoc;
import com.sun.javadoc.ProgramElementDoc;
import org.apache.xmlbeans.impl.jam.mutable.MAnnotatedElement;
import org.apache.xmlbeans.impl.jam.mutable.MClass;
import com.sun.javadoc.ClassDoc;
import org.apache.xmlbeans.impl.jam.provider.JamLogger;
import org.apache.xmlbeans.impl.jam.internal.elements.ElementContext;

public final class JavadocTigerDelegateImpl_150 extends JavadocTigerDelegate
{
    public void init(final ElementContext ctx) {
    }
    
    public void init(final JamLogger logger) {
    }
    
    public void populateAnnotationTypeIfNecessary(final ClassDoc cd, final MClass clazz, final JavadocClassBuilder builder) {
    }
    
    public void extractAnnotations(final MAnnotatedElement dest, final ProgramElementDoc src) {
    }
    
    public void extractAnnotations(final MAnnotatedElement dest, final ExecutableMemberDoc method, final Parameter src) {
    }
    
    public boolean isEnum(final ClassDoc cd) {
        return false;
    }
}
