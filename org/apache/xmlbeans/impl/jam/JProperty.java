// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.xmlbeans.impl.jam;

public interface JProperty extends JAnnotatedElement
{
    JClass getType();
    
    JMethod getSetter();
    
    JMethod getGetter();
}
