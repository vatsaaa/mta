// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.xmlbeans.impl.xb.ltgfmt.impl;

import java.util.List;
import java.util.ArrayList;
import org.apache.xmlbeans.impl.xb.ltgfmt.TestCase;
import org.apache.xmlbeans.XmlObject;
import org.apache.xmlbeans.SchemaType;
import javax.xml.namespace.QName;
import org.apache.xmlbeans.impl.xb.ltgfmt.TestsDocument;
import org.apache.xmlbeans.impl.values.XmlComplexContentImpl;

public class TestsDocumentImpl extends XmlComplexContentImpl implements TestsDocument
{
    private static final QName TESTS$0;
    
    public TestsDocumentImpl(final SchemaType sType) {
        super(sType);
    }
    
    public Tests getTests() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            Tests target = null;
            target = (Tests)this.get_store().find_element_user(TestsDocumentImpl.TESTS$0, 0);
            if (target == null) {
                return null;
            }
            return target;
        }
    }
    
    public void setTests(final Tests tests) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            Tests target = null;
            target = (Tests)this.get_store().find_element_user(TestsDocumentImpl.TESTS$0, 0);
            if (target == null) {
                target = (Tests)this.get_store().add_element_user(TestsDocumentImpl.TESTS$0);
            }
            target.set(tests);
        }
    }
    
    public Tests addNewTests() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            Tests target = null;
            target = (Tests)this.get_store().add_element_user(TestsDocumentImpl.TESTS$0);
            return target;
        }
    }
    
    static {
        TESTS$0 = new QName("http://www.bea.com/2003/05/xmlbean/ltgfmt", "tests");
    }
    
    public static class TestsImpl extends XmlComplexContentImpl implements Tests
    {
        private static final QName TEST$0;
        
        public TestsImpl(final SchemaType sType) {
            super(sType);
        }
        
        public TestCase[] getTestArray() {
            synchronized (this.monitor()) {
                this.check_orphaned();
                final List targetList = new ArrayList();
                this.get_store().find_all_element_users(TestsImpl.TEST$0, targetList);
                final TestCase[] result = new TestCase[targetList.size()];
                targetList.toArray(result);
                return result;
            }
        }
        
        public TestCase getTestArray(final int i) {
            synchronized (this.monitor()) {
                this.check_orphaned();
                TestCase target = null;
                target = (TestCase)this.get_store().find_element_user(TestsImpl.TEST$0, i);
                if (target == null) {
                    throw new IndexOutOfBoundsException();
                }
                return target;
            }
        }
        
        public int sizeOfTestArray() {
            synchronized (this.monitor()) {
                this.check_orphaned();
                return this.get_store().count_elements(TestsImpl.TEST$0);
            }
        }
        
        public void setTestArray(final TestCase[] testArray) {
            synchronized (this.monitor()) {
                this.check_orphaned();
                this.arraySetterHelper(testArray, TestsImpl.TEST$0);
            }
        }
        
        public void setTestArray(final int i, final TestCase test) {
            synchronized (this.monitor()) {
                this.check_orphaned();
                TestCase target = null;
                target = (TestCase)this.get_store().find_element_user(TestsImpl.TEST$0, i);
                if (target == null) {
                    throw new IndexOutOfBoundsException();
                }
                target.set(test);
            }
        }
        
        public TestCase insertNewTest(final int i) {
            synchronized (this.monitor()) {
                this.check_orphaned();
                TestCase target = null;
                target = (TestCase)this.get_store().insert_element_user(TestsImpl.TEST$0, i);
                return target;
            }
        }
        
        public TestCase addNewTest() {
            synchronized (this.monitor()) {
                this.check_orphaned();
                TestCase target = null;
                target = (TestCase)this.get_store().add_element_user(TestsImpl.TEST$0);
                return target;
            }
        }
        
        public void removeTest(final int i) {
            synchronized (this.monitor()) {
                this.check_orphaned();
                this.get_store().remove_element(TestsImpl.TEST$0, i);
            }
        }
        
        static {
            TEST$0 = new QName("http://www.bea.com/2003/05/xmlbean/ltgfmt", "test");
        }
    }
}
