// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.xmlbeans.impl.xb.xmlconfig.impl;

import org.apache.xmlbeans.impl.xb.xmlconfig.Extensionconfig;
import org.apache.xmlbeans.impl.xb.xmlconfig.Qnameconfig;
import java.util.List;
import java.util.ArrayList;
import org.apache.xmlbeans.impl.xb.xmlconfig.Nsconfig;
import org.apache.xmlbeans.XmlObject;
import org.apache.xmlbeans.SchemaType;
import javax.xml.namespace.QName;
import org.apache.xmlbeans.impl.xb.xmlconfig.ConfigDocument;
import org.apache.xmlbeans.impl.values.XmlComplexContentImpl;

public class ConfigDocumentImpl extends XmlComplexContentImpl implements ConfigDocument
{
    private static final QName CONFIG$0;
    
    public ConfigDocumentImpl(final SchemaType sType) {
        super(sType);
    }
    
    public Config getConfig() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            Config target = null;
            target = (Config)this.get_store().find_element_user(ConfigDocumentImpl.CONFIG$0, 0);
            if (target == null) {
                return null;
            }
            return target;
        }
    }
    
    public void setConfig(final Config config) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            Config target = null;
            target = (Config)this.get_store().find_element_user(ConfigDocumentImpl.CONFIG$0, 0);
            if (target == null) {
                target = (Config)this.get_store().add_element_user(ConfigDocumentImpl.CONFIG$0);
            }
            target.set(config);
        }
    }
    
    public Config addNewConfig() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            Config target = null;
            target = (Config)this.get_store().add_element_user(ConfigDocumentImpl.CONFIG$0);
            return target;
        }
    }
    
    static {
        CONFIG$0 = new QName("http://xml.apache.org/xmlbeans/2004/02/xbean/config", "config");
    }
    
    public static class ConfigImpl extends XmlComplexContentImpl implements Config
    {
        private static final QName NAMESPACE$0;
        private static final QName QNAME$2;
        private static final QName EXTENSION$4;
        
        public ConfigImpl(final SchemaType sType) {
            super(sType);
        }
        
        public Nsconfig[] getNamespaceArray() {
            synchronized (this.monitor()) {
                this.check_orphaned();
                final List targetList = new ArrayList();
                this.get_store().find_all_element_users(ConfigImpl.NAMESPACE$0, targetList);
                final Nsconfig[] result = new Nsconfig[targetList.size()];
                targetList.toArray(result);
                return result;
            }
        }
        
        public Nsconfig getNamespaceArray(final int i) {
            synchronized (this.monitor()) {
                this.check_orphaned();
                Nsconfig target = null;
                target = (Nsconfig)this.get_store().find_element_user(ConfigImpl.NAMESPACE$0, i);
                if (target == null) {
                    throw new IndexOutOfBoundsException();
                }
                return target;
            }
        }
        
        public int sizeOfNamespaceArray() {
            synchronized (this.monitor()) {
                this.check_orphaned();
                return this.get_store().count_elements(ConfigImpl.NAMESPACE$0);
            }
        }
        
        public void setNamespaceArray(final Nsconfig[] namespaceArray) {
            synchronized (this.monitor()) {
                this.check_orphaned();
                this.arraySetterHelper(namespaceArray, ConfigImpl.NAMESPACE$0);
            }
        }
        
        public void setNamespaceArray(final int i, final Nsconfig namespace) {
            synchronized (this.monitor()) {
                this.check_orphaned();
                Nsconfig target = null;
                target = (Nsconfig)this.get_store().find_element_user(ConfigImpl.NAMESPACE$0, i);
                if (target == null) {
                    throw new IndexOutOfBoundsException();
                }
                target.set(namespace);
            }
        }
        
        public Nsconfig insertNewNamespace(final int i) {
            synchronized (this.monitor()) {
                this.check_orphaned();
                Nsconfig target = null;
                target = (Nsconfig)this.get_store().insert_element_user(ConfigImpl.NAMESPACE$0, i);
                return target;
            }
        }
        
        public Nsconfig addNewNamespace() {
            synchronized (this.monitor()) {
                this.check_orphaned();
                Nsconfig target = null;
                target = (Nsconfig)this.get_store().add_element_user(ConfigImpl.NAMESPACE$0);
                return target;
            }
        }
        
        public void removeNamespace(final int i) {
            synchronized (this.monitor()) {
                this.check_orphaned();
                this.get_store().remove_element(ConfigImpl.NAMESPACE$0, i);
            }
        }
        
        public Qnameconfig[] getQnameArray() {
            synchronized (this.monitor()) {
                this.check_orphaned();
                final List targetList = new ArrayList();
                this.get_store().find_all_element_users(ConfigImpl.QNAME$2, targetList);
                final Qnameconfig[] result = new Qnameconfig[targetList.size()];
                targetList.toArray(result);
                return result;
            }
        }
        
        public Qnameconfig getQnameArray(final int i) {
            synchronized (this.monitor()) {
                this.check_orphaned();
                Qnameconfig target = null;
                target = (Qnameconfig)this.get_store().find_element_user(ConfigImpl.QNAME$2, i);
                if (target == null) {
                    throw new IndexOutOfBoundsException();
                }
                return target;
            }
        }
        
        public int sizeOfQnameArray() {
            synchronized (this.monitor()) {
                this.check_orphaned();
                return this.get_store().count_elements(ConfigImpl.QNAME$2);
            }
        }
        
        public void setQnameArray(final Qnameconfig[] qnameArray) {
            synchronized (this.monitor()) {
                this.check_orphaned();
                this.arraySetterHelper(qnameArray, ConfigImpl.QNAME$2);
            }
        }
        
        public void setQnameArray(final int i, final Qnameconfig qname) {
            synchronized (this.monitor()) {
                this.check_orphaned();
                Qnameconfig target = null;
                target = (Qnameconfig)this.get_store().find_element_user(ConfigImpl.QNAME$2, i);
                if (target == null) {
                    throw new IndexOutOfBoundsException();
                }
                target.set(qname);
            }
        }
        
        public Qnameconfig insertNewQname(final int i) {
            synchronized (this.monitor()) {
                this.check_orphaned();
                Qnameconfig target = null;
                target = (Qnameconfig)this.get_store().insert_element_user(ConfigImpl.QNAME$2, i);
                return target;
            }
        }
        
        public Qnameconfig addNewQname() {
            synchronized (this.monitor()) {
                this.check_orphaned();
                Qnameconfig target = null;
                target = (Qnameconfig)this.get_store().add_element_user(ConfigImpl.QNAME$2);
                return target;
            }
        }
        
        public void removeQname(final int i) {
            synchronized (this.monitor()) {
                this.check_orphaned();
                this.get_store().remove_element(ConfigImpl.QNAME$2, i);
            }
        }
        
        public Extensionconfig[] getExtensionArray() {
            synchronized (this.monitor()) {
                this.check_orphaned();
                final List targetList = new ArrayList();
                this.get_store().find_all_element_users(ConfigImpl.EXTENSION$4, targetList);
                final Extensionconfig[] result = new Extensionconfig[targetList.size()];
                targetList.toArray(result);
                return result;
            }
        }
        
        public Extensionconfig getExtensionArray(final int i) {
            synchronized (this.monitor()) {
                this.check_orphaned();
                Extensionconfig target = null;
                target = (Extensionconfig)this.get_store().find_element_user(ConfigImpl.EXTENSION$4, i);
                if (target == null) {
                    throw new IndexOutOfBoundsException();
                }
                return target;
            }
        }
        
        public int sizeOfExtensionArray() {
            synchronized (this.monitor()) {
                this.check_orphaned();
                return this.get_store().count_elements(ConfigImpl.EXTENSION$4);
            }
        }
        
        public void setExtensionArray(final Extensionconfig[] extensionArray) {
            synchronized (this.monitor()) {
                this.check_orphaned();
                this.arraySetterHelper(extensionArray, ConfigImpl.EXTENSION$4);
            }
        }
        
        public void setExtensionArray(final int i, final Extensionconfig extension) {
            synchronized (this.monitor()) {
                this.check_orphaned();
                Extensionconfig target = null;
                target = (Extensionconfig)this.get_store().find_element_user(ConfigImpl.EXTENSION$4, i);
                if (target == null) {
                    throw new IndexOutOfBoundsException();
                }
                target.set(extension);
            }
        }
        
        public Extensionconfig insertNewExtension(final int i) {
            synchronized (this.monitor()) {
                this.check_orphaned();
                Extensionconfig target = null;
                target = (Extensionconfig)this.get_store().insert_element_user(ConfigImpl.EXTENSION$4, i);
                return target;
            }
        }
        
        public Extensionconfig addNewExtension() {
            synchronized (this.monitor()) {
                this.check_orphaned();
                Extensionconfig target = null;
                target = (Extensionconfig)this.get_store().add_element_user(ConfigImpl.EXTENSION$4);
                return target;
            }
        }
        
        public void removeExtension(final int i) {
            synchronized (this.monitor()) {
                this.check_orphaned();
                this.get_store().remove_element(ConfigImpl.EXTENSION$4, i);
            }
        }
        
        static {
            NAMESPACE$0 = new QName("http://xml.apache.org/xmlbeans/2004/02/xbean/config", "namespace");
            QNAME$2 = new QName("http://xml.apache.org/xmlbeans/2004/02/xbean/config", "qname");
            EXTENSION$4 = new QName("http://xml.apache.org/xmlbeans/2004/02/xbean/config", "extension");
        }
    }
}
