// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.xmlbeans.impl.xb.xmlconfig.impl;

import org.apache.xmlbeans.XmlString;
import org.apache.xmlbeans.XmlObject;
import org.apache.xmlbeans.XmlQName;
import org.apache.xmlbeans.SimpleValue;
import org.apache.xmlbeans.SchemaType;
import javax.xml.namespace.QName;
import org.apache.xmlbeans.impl.xb.xmlconfig.Qnameconfig;
import org.apache.xmlbeans.impl.values.XmlComplexContentImpl;

public class QnameconfigImpl extends XmlComplexContentImpl implements Qnameconfig
{
    private static final QName NAME$0;
    private static final QName JAVANAME$2;
    
    public QnameconfigImpl(final SchemaType sType) {
        super(sType);
    }
    
    public QName getName() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            SimpleValue target = null;
            target = (SimpleValue)this.get_store().find_attribute_user(QnameconfigImpl.NAME$0);
            if (target == null) {
                return null;
            }
            return target.getQNameValue();
        }
    }
    
    public XmlQName xgetName() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            XmlQName target = null;
            target = (XmlQName)this.get_store().find_attribute_user(QnameconfigImpl.NAME$0);
            return target;
        }
    }
    
    public boolean isSetName() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().find_attribute_user(QnameconfigImpl.NAME$0) != null;
        }
    }
    
    public void setName(final QName name) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            SimpleValue target = null;
            target = (SimpleValue)this.get_store().find_attribute_user(QnameconfigImpl.NAME$0);
            if (target == null) {
                target = (SimpleValue)this.get_store().add_attribute_user(QnameconfigImpl.NAME$0);
            }
            target.setQNameValue(name);
        }
    }
    
    public void xsetName(final XmlQName name) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            XmlQName target = null;
            target = (XmlQName)this.get_store().find_attribute_user(QnameconfigImpl.NAME$0);
            if (target == null) {
                target = (XmlQName)this.get_store().add_attribute_user(QnameconfigImpl.NAME$0);
            }
            target.set(name);
        }
    }
    
    public void unsetName() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_attribute(QnameconfigImpl.NAME$0);
        }
    }
    
    public String getJavaname() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            SimpleValue target = null;
            target = (SimpleValue)this.get_store().find_attribute_user(QnameconfigImpl.JAVANAME$2);
            if (target == null) {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    public XmlString xgetJavaname() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            XmlString target = null;
            target = (XmlString)this.get_store().find_attribute_user(QnameconfigImpl.JAVANAME$2);
            return target;
        }
    }
    
    public boolean isSetJavaname() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            return this.get_store().find_attribute_user(QnameconfigImpl.JAVANAME$2) != null;
        }
    }
    
    public void setJavaname(final String javaname) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            SimpleValue target = null;
            target = (SimpleValue)this.get_store().find_attribute_user(QnameconfigImpl.JAVANAME$2);
            if (target == null) {
                target = (SimpleValue)this.get_store().add_attribute_user(QnameconfigImpl.JAVANAME$2);
            }
            target.setStringValue(javaname);
        }
    }
    
    public void xsetJavaname(final XmlString javaname) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            XmlString target = null;
            target = (XmlString)this.get_store().find_attribute_user(QnameconfigImpl.JAVANAME$2);
            if (target == null) {
                target = (XmlString)this.get_store().add_attribute_user(QnameconfigImpl.JAVANAME$2);
            }
            target.set(javaname);
        }
    }
    
    public void unsetJavaname() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            this.get_store().remove_attribute(QnameconfigImpl.JAVANAME$2);
        }
    }
    
    static {
        NAME$0 = new QName("", "name");
        JAVANAME$2 = new QName("", "javaname");
    }
}
