// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.xmlbeans.impl.xb.xsdownload.impl;

import org.apache.xmlbeans.XmlToken;
import org.apache.xmlbeans.SimpleValue;
import java.util.List;
import java.util.ArrayList;
import org.apache.xmlbeans.impl.xb.xsdownload.DownloadedSchemaEntry;
import org.apache.xmlbeans.XmlObject;
import org.apache.xmlbeans.SchemaType;
import javax.xml.namespace.QName;
import org.apache.xmlbeans.impl.xb.xsdownload.DownloadedSchemasDocument;
import org.apache.xmlbeans.impl.values.XmlComplexContentImpl;

public class DownloadedSchemasDocumentImpl extends XmlComplexContentImpl implements DownloadedSchemasDocument
{
    private static final QName DOWNLOADEDSCHEMAS$0;
    
    public DownloadedSchemasDocumentImpl(final SchemaType sType) {
        super(sType);
    }
    
    public DownloadedSchemas getDownloadedSchemas() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            DownloadedSchemas target = null;
            target = (DownloadedSchemas)this.get_store().find_element_user(DownloadedSchemasDocumentImpl.DOWNLOADEDSCHEMAS$0, 0);
            if (target == null) {
                return null;
            }
            return target;
        }
    }
    
    public void setDownloadedSchemas(final DownloadedSchemas downloadedSchemas) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            DownloadedSchemas target = null;
            target = (DownloadedSchemas)this.get_store().find_element_user(DownloadedSchemasDocumentImpl.DOWNLOADEDSCHEMAS$0, 0);
            if (target == null) {
                target = (DownloadedSchemas)this.get_store().add_element_user(DownloadedSchemasDocumentImpl.DOWNLOADEDSCHEMAS$0);
            }
            target.set(downloadedSchemas);
        }
    }
    
    public DownloadedSchemas addNewDownloadedSchemas() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            DownloadedSchemas target = null;
            target = (DownloadedSchemas)this.get_store().add_element_user(DownloadedSchemasDocumentImpl.DOWNLOADEDSCHEMAS$0);
            return target;
        }
    }
    
    static {
        DOWNLOADEDSCHEMAS$0 = new QName("http://www.bea.com/2003/01/xmlbean/xsdownload", "downloaded-schemas");
    }
    
    public static class DownloadedSchemasImpl extends XmlComplexContentImpl implements DownloadedSchemas
    {
        private static final QName ENTRY$0;
        private static final QName DEFAULTDIRECTORY$2;
        
        public DownloadedSchemasImpl(final SchemaType sType) {
            super(sType);
        }
        
        public DownloadedSchemaEntry[] getEntryArray() {
            synchronized (this.monitor()) {
                this.check_orphaned();
                final List targetList = new ArrayList();
                this.get_store().find_all_element_users(DownloadedSchemasImpl.ENTRY$0, targetList);
                final DownloadedSchemaEntry[] result = new DownloadedSchemaEntry[targetList.size()];
                targetList.toArray(result);
                return result;
            }
        }
        
        public DownloadedSchemaEntry getEntryArray(final int i) {
            synchronized (this.monitor()) {
                this.check_orphaned();
                DownloadedSchemaEntry target = null;
                target = (DownloadedSchemaEntry)this.get_store().find_element_user(DownloadedSchemasImpl.ENTRY$0, i);
                if (target == null) {
                    throw new IndexOutOfBoundsException();
                }
                return target;
            }
        }
        
        public int sizeOfEntryArray() {
            synchronized (this.monitor()) {
                this.check_orphaned();
                return this.get_store().count_elements(DownloadedSchemasImpl.ENTRY$0);
            }
        }
        
        public void setEntryArray(final DownloadedSchemaEntry[] entryArray) {
            synchronized (this.monitor()) {
                this.check_orphaned();
                this.arraySetterHelper(entryArray, DownloadedSchemasImpl.ENTRY$0);
            }
        }
        
        public void setEntryArray(final int i, final DownloadedSchemaEntry entry) {
            synchronized (this.monitor()) {
                this.check_orphaned();
                DownloadedSchemaEntry target = null;
                target = (DownloadedSchemaEntry)this.get_store().find_element_user(DownloadedSchemasImpl.ENTRY$0, i);
                if (target == null) {
                    throw new IndexOutOfBoundsException();
                }
                target.set(entry);
            }
        }
        
        public DownloadedSchemaEntry insertNewEntry(final int i) {
            synchronized (this.monitor()) {
                this.check_orphaned();
                DownloadedSchemaEntry target = null;
                target = (DownloadedSchemaEntry)this.get_store().insert_element_user(DownloadedSchemasImpl.ENTRY$0, i);
                return target;
            }
        }
        
        public DownloadedSchemaEntry addNewEntry() {
            synchronized (this.monitor()) {
                this.check_orphaned();
                DownloadedSchemaEntry target = null;
                target = (DownloadedSchemaEntry)this.get_store().add_element_user(DownloadedSchemasImpl.ENTRY$0);
                return target;
            }
        }
        
        public void removeEntry(final int i) {
            synchronized (this.monitor()) {
                this.check_orphaned();
                this.get_store().remove_element(DownloadedSchemasImpl.ENTRY$0, i);
            }
        }
        
        public String getDefaultDirectory() {
            synchronized (this.monitor()) {
                this.check_orphaned();
                SimpleValue target = null;
                target = (SimpleValue)this.get_store().find_attribute_user(DownloadedSchemasImpl.DEFAULTDIRECTORY$2);
                if (target == null) {
                    return null;
                }
                return target.getStringValue();
            }
        }
        
        public XmlToken xgetDefaultDirectory() {
            synchronized (this.monitor()) {
                this.check_orphaned();
                XmlToken target = null;
                target = (XmlToken)this.get_store().find_attribute_user(DownloadedSchemasImpl.DEFAULTDIRECTORY$2);
                return target;
            }
        }
        
        public boolean isSetDefaultDirectory() {
            synchronized (this.monitor()) {
                this.check_orphaned();
                return this.get_store().find_attribute_user(DownloadedSchemasImpl.DEFAULTDIRECTORY$2) != null;
            }
        }
        
        public void setDefaultDirectory(final String defaultDirectory) {
            synchronized (this.monitor()) {
                this.check_orphaned();
                SimpleValue target = null;
                target = (SimpleValue)this.get_store().find_attribute_user(DownloadedSchemasImpl.DEFAULTDIRECTORY$2);
                if (target == null) {
                    target = (SimpleValue)this.get_store().add_attribute_user(DownloadedSchemasImpl.DEFAULTDIRECTORY$2);
                }
                target.setStringValue(defaultDirectory);
            }
        }
        
        public void xsetDefaultDirectory(final XmlToken defaultDirectory) {
            synchronized (this.monitor()) {
                this.check_orphaned();
                XmlToken target = null;
                target = (XmlToken)this.get_store().find_attribute_user(DownloadedSchemasImpl.DEFAULTDIRECTORY$2);
                if (target == null) {
                    target = (XmlToken)this.get_store().add_attribute_user(DownloadedSchemasImpl.DEFAULTDIRECTORY$2);
                }
                target.set(defaultDirectory);
            }
        }
        
        public void unsetDefaultDirectory() {
            synchronized (this.monitor()) {
                this.check_orphaned();
                this.get_store().remove_attribute(DownloadedSchemasImpl.DEFAULTDIRECTORY$2);
            }
        }
        
        static {
            ENTRY$0 = new QName("http://www.bea.com/2003/01/xmlbean/xsdownload", "entry");
            DEFAULTDIRECTORY$2 = new QName("", "defaultDirectory");
        }
    }
}
