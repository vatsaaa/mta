// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.xmlbeans.impl.xb.xsdschema.impl;

import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.impl.xb.xsdschema.LocalElement;

public class LocalElementImpl extends ElementImpl implements LocalElement
{
    public LocalElementImpl(final SchemaType sType) {
        super(sType);
    }
}
