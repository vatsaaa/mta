// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.xmlbeans.impl.xb.xsdschema.impl;

import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.impl.xb.xsdschema.ReducedDerivationControl;
import org.apache.xmlbeans.impl.values.JavaStringEnumerationHolderEx;

public class ReducedDerivationControlImpl extends JavaStringEnumerationHolderEx implements ReducedDerivationControl
{
    public ReducedDerivationControlImpl(final SchemaType sType) {
        super(sType, false);
    }
    
    protected ReducedDerivationControlImpl(final SchemaType sType, final boolean b) {
        super(sType, b);
    }
}
