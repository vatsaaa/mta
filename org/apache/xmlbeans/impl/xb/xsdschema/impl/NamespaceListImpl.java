// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.xmlbeans.impl.xb.xsdschema.impl;

import org.apache.xmlbeans.impl.xb.xsdschema.NamespaceList.Member2.Item;
import org.apache.xmlbeans.XmlAnyURI;
import org.apache.xmlbeans.impl.xb.xsdschema.NamespaceList.Member2;
import org.apache.xmlbeans.impl.values.XmlListImpl;
import org.apache.xmlbeans.impl.values.JavaStringEnumerationHolderEx;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.impl.xb.xsdschema.NamespaceList;
import org.apache.xmlbeans.impl.values.XmlUnionImpl;

public class NamespaceListImpl extends XmlUnionImpl implements NamespaceList, Member, Member2
{
    public NamespaceListImpl(final SchemaType sType) {
        super(sType, false);
    }
    
    protected NamespaceListImpl(final SchemaType sType, final boolean b) {
        super(sType, b);
    }
    
    public static class MemberImpl extends JavaStringEnumerationHolderEx implements Member
    {
        public MemberImpl(final SchemaType sType) {
            super(sType, false);
        }
        
        protected MemberImpl(final SchemaType sType, final boolean b) {
            super(sType, b);
        }
    }
    
    public static class MemberImpl2 extends XmlListImpl implements Member2
    {
        public MemberImpl2(final SchemaType sType) {
            super(sType, false);
        }
        
        protected MemberImpl2(final SchemaType sType, final boolean b) {
            super(sType, b);
        }
        
        public static class ItemImpl extends XmlUnionImpl implements Item, XmlAnyURI, Item.Member
        {
            public ItemImpl(final SchemaType sType) {
                super(sType, false);
            }
            
            protected ItemImpl(final SchemaType sType, final boolean b) {
                super(sType, b);
            }
            
            public static class MemberImpl extends JavaStringEnumerationHolderEx implements Item.Member
            {
                public MemberImpl(final SchemaType sType) {
                    super(sType, false);
                }
                
                protected MemberImpl(final SchemaType sType, final boolean b) {
                    super(sType, b);
                }
            }
        }
    }
}
