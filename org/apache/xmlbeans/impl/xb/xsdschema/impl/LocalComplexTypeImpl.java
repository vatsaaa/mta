// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.xmlbeans.impl.xb.xsdschema.impl;

import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.impl.xb.xsdschema.LocalComplexType;

public class LocalComplexTypeImpl extends ComplexTypeImpl implements LocalComplexType
{
    public LocalComplexTypeImpl(final SchemaType sType) {
        super(sType);
    }
}
