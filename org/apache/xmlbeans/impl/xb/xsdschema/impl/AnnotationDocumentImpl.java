// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.xmlbeans.impl.xb.xsdschema.impl;

import org.apache.xmlbeans.XmlID;
import org.apache.xmlbeans.SimpleValue;
import org.apache.xmlbeans.impl.xb.xsdschema.DocumentationDocument;
import java.util.List;
import java.util.ArrayList;
import org.apache.xmlbeans.impl.xb.xsdschema.AppinfoDocument;
import org.apache.xmlbeans.XmlObject;
import org.apache.xmlbeans.SchemaType;
import javax.xml.namespace.QName;
import org.apache.xmlbeans.impl.xb.xsdschema.AnnotationDocument;
import org.apache.xmlbeans.impl.values.XmlComplexContentImpl;

public class AnnotationDocumentImpl extends XmlComplexContentImpl implements AnnotationDocument
{
    private static final QName ANNOTATION$0;
    
    public AnnotationDocumentImpl(final SchemaType sType) {
        super(sType);
    }
    
    public Annotation getAnnotation() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            Annotation target = null;
            target = (Annotation)this.get_store().find_element_user(AnnotationDocumentImpl.ANNOTATION$0, 0);
            if (target == null) {
                return null;
            }
            return target;
        }
    }
    
    public void setAnnotation(final Annotation annotation) {
        synchronized (this.monitor()) {
            this.check_orphaned();
            Annotation target = null;
            target = (Annotation)this.get_store().find_element_user(AnnotationDocumentImpl.ANNOTATION$0, 0);
            if (target == null) {
                target = (Annotation)this.get_store().add_element_user(AnnotationDocumentImpl.ANNOTATION$0);
            }
            target.set(annotation);
        }
    }
    
    public Annotation addNewAnnotation() {
        synchronized (this.monitor()) {
            this.check_orphaned();
            Annotation target = null;
            target = (Annotation)this.get_store().add_element_user(AnnotationDocumentImpl.ANNOTATION$0);
            return target;
        }
    }
    
    static {
        ANNOTATION$0 = new QName("http://www.w3.org/2001/XMLSchema", "annotation");
    }
    
    public static class AnnotationImpl extends OpenAttrsImpl implements Annotation
    {
        private static final QName APPINFO$0;
        private static final QName DOCUMENTATION$2;
        private static final QName ID$4;
        
        public AnnotationImpl(final SchemaType sType) {
            super(sType);
        }
        
        public AppinfoDocument.Appinfo[] getAppinfoArray() {
            synchronized (this.monitor()) {
                this.check_orphaned();
                final List targetList = new ArrayList();
                this.get_store().find_all_element_users(AnnotationImpl.APPINFO$0, targetList);
                final AppinfoDocument.Appinfo[] result = new AppinfoDocument.Appinfo[targetList.size()];
                targetList.toArray(result);
                return result;
            }
        }
        
        public AppinfoDocument.Appinfo getAppinfoArray(final int i) {
            synchronized (this.monitor()) {
                this.check_orphaned();
                AppinfoDocument.Appinfo target = null;
                target = (AppinfoDocument.Appinfo)this.get_store().find_element_user(AnnotationImpl.APPINFO$0, i);
                if (target == null) {
                    throw new IndexOutOfBoundsException();
                }
                return target;
            }
        }
        
        public int sizeOfAppinfoArray() {
            synchronized (this.monitor()) {
                this.check_orphaned();
                return this.get_store().count_elements(AnnotationImpl.APPINFO$0);
            }
        }
        
        public void setAppinfoArray(final AppinfoDocument.Appinfo[] appinfoArray) {
            synchronized (this.monitor()) {
                this.check_orphaned();
                this.arraySetterHelper(appinfoArray, AnnotationImpl.APPINFO$0);
            }
        }
        
        public void setAppinfoArray(final int i, final AppinfoDocument.Appinfo appinfo) {
            synchronized (this.monitor()) {
                this.check_orphaned();
                AppinfoDocument.Appinfo target = null;
                target = (AppinfoDocument.Appinfo)this.get_store().find_element_user(AnnotationImpl.APPINFO$0, i);
                if (target == null) {
                    throw new IndexOutOfBoundsException();
                }
                target.set(appinfo);
            }
        }
        
        public AppinfoDocument.Appinfo insertNewAppinfo(final int i) {
            synchronized (this.monitor()) {
                this.check_orphaned();
                AppinfoDocument.Appinfo target = null;
                target = (AppinfoDocument.Appinfo)this.get_store().insert_element_user(AnnotationImpl.APPINFO$0, i);
                return target;
            }
        }
        
        public AppinfoDocument.Appinfo addNewAppinfo() {
            synchronized (this.monitor()) {
                this.check_orphaned();
                AppinfoDocument.Appinfo target = null;
                target = (AppinfoDocument.Appinfo)this.get_store().add_element_user(AnnotationImpl.APPINFO$0);
                return target;
            }
        }
        
        public void removeAppinfo(final int i) {
            synchronized (this.monitor()) {
                this.check_orphaned();
                this.get_store().remove_element(AnnotationImpl.APPINFO$0, i);
            }
        }
        
        public DocumentationDocument.Documentation[] getDocumentationArray() {
            synchronized (this.monitor()) {
                this.check_orphaned();
                final List targetList = new ArrayList();
                this.get_store().find_all_element_users(AnnotationImpl.DOCUMENTATION$2, targetList);
                final DocumentationDocument.Documentation[] result = new DocumentationDocument.Documentation[targetList.size()];
                targetList.toArray(result);
                return result;
            }
        }
        
        public DocumentationDocument.Documentation getDocumentationArray(final int i) {
            synchronized (this.monitor()) {
                this.check_orphaned();
                DocumentationDocument.Documentation target = null;
                target = (DocumentationDocument.Documentation)this.get_store().find_element_user(AnnotationImpl.DOCUMENTATION$2, i);
                if (target == null) {
                    throw new IndexOutOfBoundsException();
                }
                return target;
            }
        }
        
        public int sizeOfDocumentationArray() {
            synchronized (this.monitor()) {
                this.check_orphaned();
                return this.get_store().count_elements(AnnotationImpl.DOCUMENTATION$2);
            }
        }
        
        public void setDocumentationArray(final DocumentationDocument.Documentation[] documentationArray) {
            synchronized (this.monitor()) {
                this.check_orphaned();
                this.arraySetterHelper(documentationArray, AnnotationImpl.DOCUMENTATION$2);
            }
        }
        
        public void setDocumentationArray(final int i, final DocumentationDocument.Documentation documentation) {
            synchronized (this.monitor()) {
                this.check_orphaned();
                DocumentationDocument.Documentation target = null;
                target = (DocumentationDocument.Documentation)this.get_store().find_element_user(AnnotationImpl.DOCUMENTATION$2, i);
                if (target == null) {
                    throw new IndexOutOfBoundsException();
                }
                target.set(documentation);
            }
        }
        
        public DocumentationDocument.Documentation insertNewDocumentation(final int i) {
            synchronized (this.monitor()) {
                this.check_orphaned();
                DocumentationDocument.Documentation target = null;
                target = (DocumentationDocument.Documentation)this.get_store().insert_element_user(AnnotationImpl.DOCUMENTATION$2, i);
                return target;
            }
        }
        
        public DocumentationDocument.Documentation addNewDocumentation() {
            synchronized (this.monitor()) {
                this.check_orphaned();
                DocumentationDocument.Documentation target = null;
                target = (DocumentationDocument.Documentation)this.get_store().add_element_user(AnnotationImpl.DOCUMENTATION$2);
                return target;
            }
        }
        
        public void removeDocumentation(final int i) {
            synchronized (this.monitor()) {
                this.check_orphaned();
                this.get_store().remove_element(AnnotationImpl.DOCUMENTATION$2, i);
            }
        }
        
        public String getId() {
            synchronized (this.monitor()) {
                this.check_orphaned();
                SimpleValue target = null;
                target = (SimpleValue)this.get_store().find_attribute_user(AnnotationImpl.ID$4);
                if (target == null) {
                    return null;
                }
                return target.getStringValue();
            }
        }
        
        public XmlID xgetId() {
            synchronized (this.monitor()) {
                this.check_orphaned();
                XmlID target = null;
                target = (XmlID)this.get_store().find_attribute_user(AnnotationImpl.ID$4);
                return target;
            }
        }
        
        public boolean isSetId() {
            synchronized (this.monitor()) {
                this.check_orphaned();
                return this.get_store().find_attribute_user(AnnotationImpl.ID$4) != null;
            }
        }
        
        public void setId(final String id) {
            synchronized (this.monitor()) {
                this.check_orphaned();
                SimpleValue target = null;
                target = (SimpleValue)this.get_store().find_attribute_user(AnnotationImpl.ID$4);
                if (target == null) {
                    target = (SimpleValue)this.get_store().add_attribute_user(AnnotationImpl.ID$4);
                }
                target.setStringValue(id);
            }
        }
        
        public void xsetId(final XmlID id) {
            synchronized (this.monitor()) {
                this.check_orphaned();
                XmlID target = null;
                target = (XmlID)this.get_store().find_attribute_user(AnnotationImpl.ID$4);
                if (target == null) {
                    target = (XmlID)this.get_store().add_attribute_user(AnnotationImpl.ID$4);
                }
                target.set(id);
            }
        }
        
        public void unsetId() {
            synchronized (this.monitor()) {
                this.check_orphaned();
                this.get_store().remove_attribute(AnnotationImpl.ID$4);
            }
        }
        
        static {
            APPINFO$0 = new QName("http://www.w3.org/2001/XMLSchema", "appinfo");
            DOCUMENTATION$2 = new QName("http://www.w3.org/2001/XMLSchema", "documentation");
            ID$4 = new QName("", "id");
        }
    }
}
