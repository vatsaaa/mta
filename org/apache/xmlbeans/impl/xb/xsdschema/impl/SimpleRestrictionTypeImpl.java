// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.xmlbeans.impl.xb.xsdschema.impl;

import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.impl.xb.xsdschema.SimpleRestrictionType;

public class SimpleRestrictionTypeImpl extends RestrictionTypeImpl implements SimpleRestrictionType
{
    public SimpleRestrictionTypeImpl(final SchemaType sType) {
        super(sType);
    }
}
