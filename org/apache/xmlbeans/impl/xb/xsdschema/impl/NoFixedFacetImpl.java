// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.xmlbeans.impl.xb.xsdschema.impl;

import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.impl.xb.xsdschema.NoFixedFacet;

public class NoFixedFacetImpl extends FacetImpl implements NoFixedFacet
{
    public NoFixedFacetImpl(final SchemaType sType) {
        super(sType);
    }
}
