// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.xmlbeans.impl.xb.xsdschema.impl;

import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.impl.xb.xsdschema.SimpleExplicitGroup;

public class SimpleExplicitGroupImpl extends ExplicitGroupImpl implements SimpleExplicitGroup
{
    public SimpleExplicitGroupImpl(final SchemaType sType) {
        super(sType);
    }
}
