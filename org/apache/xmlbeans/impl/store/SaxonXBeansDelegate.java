// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.xmlbeans.impl.store;

import java.util.List;
import java.util.Map;
import java.lang.reflect.Constructor;

public final class SaxonXBeansDelegate
{
    protected static boolean _saxonAvailable;
    private static Constructor _constructor;
    private static Constructor _xqConstructor;
    
    private SaxonXBeansDelegate() {
    }
    
    static void init() {
        Class saxonXPathImpl = null;
        Class saxonXQueryImpl = null;
        try {
            saxonXPathImpl = Class.forName("org.apache.xmlbeans.impl.xpath.saxon.XBeansXPath");
            saxonXQueryImpl = Class.forName("org.apache.xmlbeans.impl.xquery.saxon.XBeansXQuery");
        }
        catch (ClassNotFoundException e2) {
            SaxonXBeansDelegate._saxonAvailable = false;
        }
        catch (NoClassDefFoundError e3) {
            SaxonXBeansDelegate._saxonAvailable = false;
        }
        if (SaxonXBeansDelegate._saxonAvailable) {
            try {
                SaxonXBeansDelegate._constructor = saxonXPathImpl.getConstructor(String.class, String.class, Map.class, String.class);
                SaxonXBeansDelegate._xqConstructor = saxonXQueryImpl.getConstructor(String.class, String.class, Integer.class);
            }
            catch (Exception e) {
                SaxonXBeansDelegate._saxonAvailable = false;
                throw new RuntimeException(e);
            }
        }
    }
    
    static SelectPathInterface createInstance(final String xpath, final String contextVar, final Map namespaceMap) {
        if (SaxonXBeansDelegate._saxonAvailable && SaxonXBeansDelegate._constructor == null) {
            init();
        }
        if (SaxonXBeansDelegate._constructor == null) {
            return null;
        }
        try {
            final Object defaultNS = namespaceMap.get("$xmlbeans!default_uri");
            if (defaultNS != null) {
                namespaceMap.remove("$xmlbeans!default_uri");
            }
            return SaxonXBeansDelegate._constructor.newInstance(xpath, contextVar, namespaceMap, (String)defaultNS);
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
    
    static QueryInterface createQueryInstance(final String query, final String contextVar, final int boundary) {
        if (SaxonXBeansDelegate._saxonAvailable && SaxonXBeansDelegate._xqConstructor == null) {
            init();
        }
        if (SaxonXBeansDelegate._xqConstructor == null) {
            return null;
        }
        try {
            return SaxonXBeansDelegate._xqConstructor.newInstance(query, contextVar, new Integer(boundary));
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
    
    static {
        SaxonXBeansDelegate._saxonAvailable = true;
    }
    
    public interface QueryInterface
    {
        List execQuery(final Object p0, final Map p1);
    }
    
    public interface SelectPathInterface
    {
        List selectPath(final Object p0);
    }
}
