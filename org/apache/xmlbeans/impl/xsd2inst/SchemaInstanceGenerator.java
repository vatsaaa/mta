// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.xmlbeans.impl.xsd2inst;

import org.apache.xmlbeans.SchemaType;
import java.util.Iterator;
import org.apache.xmlbeans.SchemaTypeSystem;
import java.util.List;
import java.io.File;
import java.util.Set;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.SchemaTypeLoader;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.XmlObject;
import org.apache.xmlbeans.XmlOptions;
import java.util.ArrayList;
import java.util.Collection;
import org.apache.xmlbeans.impl.tool.CommandLine;
import java.util.HashSet;

public class SchemaInstanceGenerator
{
    public static void printUsage() {
        System.out.println("Generates a document based on the given Schema file");
        System.out.println("having the given element as root.");
        System.out.println("The tool makes reasonable attempts to create a valid document,");
        System.out.println("but this is not always possible since, for example, ");
        System.out.println("there are schemas for which no valid instance document ");
        System.out.println("can be produced.");
        System.out.println("Usage: xsd2inst [flags] schema.xsd -name element_name");
        System.out.println("Flags:");
        System.out.println("    -name    the name of the root element");
        System.out.println("    -dl      enable network downloads for imports and includes");
        System.out.println("    -nopvr   disable particle valid (restriction) rule");
        System.out.println("    -noupa   diable unique particle attributeion rule");
        System.out.println("    -license prints license information");
    }
    
    public static void main(final String[] args) {
        final Set flags = new HashSet();
        final Set opts = new HashSet();
        flags.add("h");
        flags.add("help");
        flags.add("usage");
        flags.add("license");
        flags.add("version");
        flags.add("dl");
        flags.add("noupa");
        flags.add("nopvr");
        flags.add("partial");
        opts.add("name");
        final CommandLine cl = new CommandLine(args, flags, opts);
        if (cl.getOpt("h") != null || cl.getOpt("help") != null || cl.getOpt("usage") != null) {
            printUsage();
            return;
        }
        final String[] badOpts = cl.getBadOpts();
        if (badOpts.length > 0) {
            for (int i = 0; i < badOpts.length; ++i) {
                System.out.println("Unrecognized option: " + badOpts[i]);
            }
            printUsage();
            return;
        }
        if (cl.getOpt("license") != null) {
            CommandLine.printLicense();
            System.exit(0);
            return;
        }
        if (cl.getOpt("version") != null) {
            CommandLine.printVersion();
            System.exit(0);
            return;
        }
        final boolean dl = cl.getOpt("dl") != null;
        final boolean nopvr = cl.getOpt("nopvr") != null;
        final boolean noupa = cl.getOpt("noupa") != null;
        final File[] schemaFiles = cl.filesEndingWith(".xsd");
        final String rootName = cl.getOpt("name");
        if (rootName == null) {
            System.out.println("Required option \"-name\" must be present");
            return;
        }
        final List sdocs = new ArrayList();
        for (int j = 0; j < schemaFiles.length; ++j) {
            try {
                sdocs.add(XmlObject.Factory.parse(schemaFiles[j], new XmlOptions().setLoadLineNumbers().setLoadMessageDigest()));
            }
            catch (Exception e) {
                System.err.println("Can not load schema file: " + schemaFiles[j] + ": ");
                e.printStackTrace();
            }
        }
        final XmlObject[] schemas = sdocs.toArray(new XmlObject[sdocs.size()]);
        SchemaTypeSystem sts = null;
        if (schemas.length > 0) {
            final Collection errors = new ArrayList();
            final XmlOptions compileOptions = new XmlOptions();
            if (dl) {
                compileOptions.setCompileDownloadUrls();
            }
            if (nopvr) {
                compileOptions.setCompileNoPvrRule();
            }
            if (noupa) {
                compileOptions.setCompileNoUpaRule();
            }
            try {
                sts = XmlBeans.compileXsd(schemas, XmlBeans.getBuiltinTypeSystem(), compileOptions);
            }
            catch (Exception e2) {
                if (errors.isEmpty() || !(e2 instanceof XmlException)) {
                    e2.printStackTrace();
                }
                System.out.println("Schema compilation errors: ");
                final Iterator k = errors.iterator();
                while (k.hasNext()) {
                    System.out.println(k.next());
                }
            }
        }
        if (sts == null) {
            System.out.println("No Schemas to process.");
            return;
        }
        final SchemaType[] globalElems = sts.documentTypes();
        SchemaType elem = null;
        for (int l = 0; l < globalElems.length; ++l) {
            if (rootName.equals(globalElems[l].getDocumentElementName().getLocalPart())) {
                elem = globalElems[l];
                break;
            }
        }
        if (elem == null) {
            System.out.println("Could not find a global element with name \"" + rootName + "\"");
            return;
        }
        final String result = SampleXmlUtil.createSampleForType(elem);
        System.out.println(result);
    }
}
