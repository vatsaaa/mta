// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.xmlbeans.impl.piccolo.io;

import java.io.CharConversionException;

public interface CharsetDecoder
{
    int minBytesPerChar();
    
    int maxBytesPerChar();
    
    void decode(final byte[] p0, final int p1, final int p2, final char[] p3, final int p4, final int p5, final int[] p6) throws CharConversionException;
    
    CharsetDecoder newCharsetDecoder();
    
    void reset();
}
