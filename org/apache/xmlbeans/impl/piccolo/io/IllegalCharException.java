// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.xmlbeans.impl.piccolo.io;

import java.io.CharConversionException;

public class IllegalCharException extends CharConversionException
{
    protected int line;
    protected int column;
    
    public IllegalCharException(final String msg) {
        super(msg);
    }
}
