// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.xmlbeans.impl.piccolo.io;

import java.io.IOException;

public class FileFormatException extends IOException
{
    protected int line;
    protected int column;
    
    public FileFormatException() {
        this((String)null);
    }
    
    public FileFormatException(final String msg) {
        this(msg, -1, -1);
    }
    
    public FileFormatException(final String msg, final int line, final int column) {
        super(msg);
        this.line = line;
        this.column = column;
    }
    
    public int getLine() {
        return this.line;
    }
    
    public int getColumn() {
        return this.column;
    }
}
