// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.xmlbeans.impl.piccolo.util;

public class RecursionException extends Exception
{
    public RecursionException(final String msg) {
        super(msg);
    }
    
    public RecursionException() {
    }
}
