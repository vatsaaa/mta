// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.xmlbeans.impl.piccolo.util;

public class DuplicateKeyException extends Exception
{
    public DuplicateKeyException() {
    }
    
    public DuplicateKeyException(final String msg) {
        super(msg);
    }
}
