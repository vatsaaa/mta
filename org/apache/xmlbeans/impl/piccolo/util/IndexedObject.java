// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.xmlbeans.impl.piccolo.util;

public interface IndexedObject
{
    int getIndex();
    
    void setIndex(final int p0);
    
    Object getObject();
    
    void setObject(final Object p0);
}
