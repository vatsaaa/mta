// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.xmlbeans.impl.piccolo.util;

public final class CharStringConverter
{
    private static final float DEFAULT_LOAD = 0.7f;
    private float loadFactor;
    private int numEntries;
    private int maxEntries;
    private int hashmask;
    private char[][] keys;
    private String[] values;
    
    public CharStringConverter(final int initialCapacity, final float loadFactor) {
        this.numEntries = 0;
        if (initialCapacity < 0) {
            throw new IllegalArgumentException("Illegal initial capacity: " + initialCapacity);
        }
        if (loadFactor < 0.0f || loadFactor > 1.0f) {
            throw new IllegalArgumentException("Illegal load factor: " + loadFactor);
        }
        int desiredSize;
        int size;
        for (desiredSize = (int)(initialCapacity / loadFactor), size = 16; size < desiredSize; size <<= 1) {}
        this.hashmask = size - 1;
        this.maxEntries = (int)(size * loadFactor);
        this.keys = new char[size][];
        this.values = new String[size];
        this.loadFactor = loadFactor;
    }
    
    public CharStringConverter() {
        this(0, 0.7f);
    }
    
    public CharStringConverter(final int initialCapacity) {
        this(initialCapacity, 0.7f);
    }
    
    public int getCacheSize() {
        return this.numEntries;
    }
    
    public String convert(final char[] ch) {
        return this.convert(ch, 0, ch.length);
    }
    
    public String convert(final char[] ch, final int start, final int length) {
        if (this.numEntries >= this.maxEntries) {
            this.rehash();
        }
        int offset;
        char[] k;
        for (offset = (hashKey(ch, start, length) & this.hashmask), k = null; (k = this.keys[offset]) != null && !keysAreEqual(k, 0, k.length, ch, start, length); offset = (offset - 1 & this.hashmask)) {}
        if (k != null) {
            return this.values[offset];
        }
        k = new char[length];
        System.arraycopy(ch, start, k, 0, length);
        final String v = new String(k).intern();
        this.keys[offset] = k;
        this.values[offset] = v;
        ++this.numEntries;
        return v;
    }
    
    private void rehash() {
        final int newlength = this.keys.length << 1;
        final char[][] newkeys = new char[newlength][];
        final String[] newvalues = new String[newlength];
        final int newhashmask = newlength - 1;
        for (int i = 0; i < this.keys.length; ++i) {
            final char[] k = this.keys[i];
            final String v = this.values[i];
            if (k != null) {
                int newoffset = hashKey(k, 0, k.length) & newhashmask;
                for (char[] newk = null; (newk = newkeys[newoffset]) != null && !keysAreEqual(newk, 0, newk.length, k, 0, k.length); newoffset = (newoffset - 1 & newhashmask)) {}
                newkeys[newoffset] = k;
                newvalues[newoffset] = v;
            }
        }
        this.keys = newkeys;
        this.values = newvalues;
        this.maxEntries = (int)(newlength * this.loadFactor);
        this.hashmask = newhashmask;
    }
    
    public void clearCache() {
        for (int i = 0; i < this.keys.length; ++i) {
            this.keys[i] = null;
            this.values[i] = null;
        }
        this.numEntries = 0;
    }
    
    private static final boolean keysAreEqual(final char[] a, final int astart, final int alength, final char[] b, final int bstart, final int blength) {
        if (alength != blength) {
            return false;
        }
        for (int i = 0; i < alength; ++i) {
            if (a[astart + i] != b[bstart + i]) {
                return false;
            }
        }
        return true;
    }
    
    private static final int hashKey(final char[] ch, final int start, final int length) {
        int hash = 0;
        for (int i = 0; i < length; ++i) {
            hash = (hash << 5) + ch[start + i];
        }
        return hash;
    }
}
