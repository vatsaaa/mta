// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.xmlbeans.impl.piccolo.util;

import java.net.URL;
import java.util.Enumeration;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.io.BufferedReader;
import java.io.InputStreamReader;

public class FactoryServiceFinder
{
    static final String SERVICE = "META-INF/services/";
    
    public static String findService(final String name) throws IOException {
        final InputStream is = ClassLoader.getSystemClassLoader().getResourceAsStream("META-INF/services/" + name);
        final BufferedReader r = new BufferedReader(new InputStreamReader(is, "UTF-8"));
        return r.readLine();
    }
    
    public static Enumeration findServices(final String name) throws IOException {
        return new FactoryEnumeration(ClassLoader.getSystemClassLoader().getResources(name));
    }
    
    private static class FactoryEnumeration implements Enumeration
    {
        Enumeration enumValue;
        Object next;
        
        FactoryEnumeration(final Enumeration enumValue) {
            this.next = null;
            this.enumValue = enumValue;
            this.nextElement();
        }
        
        public boolean hasMoreElements() {
            return this.next != null;
        }
        
        public Object nextElement() {
            final Object current = this.next;
            while (true) {
                try {
                    if (this.enumValue.hasMoreElements()) {
                        final BufferedReader r = new BufferedReader(new InputStreamReader(this.enumValue.nextElement().openStream()));
                        this.next = r.readLine();
                    }
                    else {
                        this.next = null;
                    }
                }
                catch (IOException e) {
                    continue;
                }
                break;
            }
            return current;
        }
    }
}
