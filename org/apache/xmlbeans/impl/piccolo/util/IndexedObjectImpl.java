// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.xmlbeans.impl.piccolo.util;

public final class IndexedObjectImpl implements IndexedObject
{
    private int index;
    private Object object;
    
    public IndexedObjectImpl(final int index, final Object object) {
        this.index = index;
        this.object = object;
    }
    
    public final int getIndex() {
        return this.index;
    }
    
    public final void setIndex(final int index) {
        this.index = index;
    }
    
    public final Object getObject() {
        return this.object;
    }
    
    public final void setObject(final Object object) {
        this.object = object;
    }
    
    public final Object clone() {
        return new IndexedObjectImpl(this.index, this.object);
    }
    
    public final boolean equals(final Object o) {
        if (o instanceof IndexedObject) {
            final IndexedObject i = (IndexedObject)o;
            return this.index == i.getIndex() && this.object.equals(i.getObject());
        }
        return false;
    }
}
