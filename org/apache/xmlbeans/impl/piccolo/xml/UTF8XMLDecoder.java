// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.xmlbeans.impl.piccolo.xml;

import org.apache.xmlbeans.impl.piccolo.io.IllegalCharException;
import java.io.CharConversionException;
import org.apache.xmlbeans.impl.piccolo.io.CharsetDecoder;

public final class UTF8XMLDecoder implements XMLDecoder
{
    private boolean sawCR;
    
    public UTF8XMLDecoder() {
        this.sawCR = false;
    }
    
    public CharsetDecoder newCharsetDecoder() {
        return this.newXMLDecoder();
    }
    
    public XMLDecoder newXMLDecoder() {
        return new UTF8XMLDecoder();
    }
    
    public int minBytesPerChar() {
        return 1;
    }
    
    public int maxBytesPerChar() {
        return 3;
    }
    
    public void reset() {
        this.sawCR = false;
    }
    
    public void decode(final byte[] in_buf, final int in_off, final int in_len, final char[] out_buf, final int out_off, final int out_len, final int[] result) throws CharConversionException {
        int i;
        int o;
        for (o = (i = 0); i < in_len && o < out_len; ++i) {
            int c = in_buf[in_off + i];
            if ((c & 0x80) != 0x0) {
                if (++i >= in_len) {
                    result[0] = i - 1;
                    result[1] = o;
                    return;
                }
                final int c2 = in_buf[in_off + i];
                if ((c & 0xE0) == 0xC0) {
                    if ((c2 & 0x80) != 0x80) {
                        throw new CharConversionException("Malformed UTF-8 character: 0x" + Integer.toHexString(c & 0xFF) + " 0x" + Integer.toHexString(c2 & 0xFF));
                    }
                    c = ((c & 0x1F) << 6 | (c2 & 0x3F));
                    if ((c & 0x780) == 0x0) {
                        throw new CharConversionException("2-byte UTF-8 character is overlong: 0x" + Integer.toHexString(in_buf[in_off + i - 1] & 0xFF) + " 0x" + Integer.toHexString(c2 & 0xFF));
                    }
                }
                else if ((c & 0xF0) == 0xE0) {
                    if (++i >= in_len) {
                        result[0] = i - 2;
                        result[1] = o;
                        return;
                    }
                    final int c3 = in_buf[in_off + i];
                    if ((c2 & 0x80) != 0x80 || (c3 & 0x80) != 0x80) {
                        throw new CharConversionException("Malformed UTF-8 character: 0x" + Integer.toHexString(c & 0xFF) + " 0x" + Integer.toHexString(c2 & 0xFF) + " 0x" + Integer.toHexString(c3 & 0xFF));
                    }
                    c = ((c & 0xF) << 12 | (c2 & 0x3F) << 6 | (c3 & 0x3F));
                    if ((c & 0xF800) == 0x0) {
                        throw new CharConversionException("3-byte UTF-8 character is overlong: 0x" + Integer.toHexString(in_buf[in_off + i - 2] & 0xFF) + " 0x" + Integer.toHexString(c2 & 0xFF) + " 0x" + Integer.toHexString(c3 & 0xFF));
                    }
                }
                else {
                    if ((c & 0xF0) != 0xF0) {
                        throw new CharConversionException("Characters larger than 4 bytes are not supported: byte 0x" + Integer.toHexString(c & 0xFF) + " implies a length of more than 4 bytes");
                    }
                    if (i + 2 >= in_len) {
                        result[0] = i - 2;
                        result[1] = o;
                        return;
                    }
                    final int c3 = in_buf[in_off + ++i];
                    final int c4 = in_buf[in_off + ++i];
                    if ((c2 & 0x80) != 0x80 || (c3 & 0x80) != 0x80 || (c4 & 0x80) != 0x80) {
                        throw new CharConversionException("Malformed UTF-8 character: 0x" + Integer.toHexString(c & 0xFF) + " 0x" + Integer.toHexString(c2 & 0xFF) + " 0x" + Integer.toHexString(c3 & 0xFF) + " 0x" + Integer.toHexString(c4 & 0xFF));
                    }
                    c = ((c & 0x7) << 18 | (c2 & 0x3F) << 12 | (c3 & 0x3F) << 6 | (c4 & 0x3F));
                    if (c < 65536 || c > 1114111) {
                        throw new IllegalCharException("Illegal XML character: 0x" + Integer.toHexString(c));
                    }
                    c -= 65536;
                    out_buf[out_off + o++] = (char)(c >> 10 | 0xD800);
                    out_buf[out_off + o++] = (char)((c & 0x3FF) | 0xDC00);
                    this.sawCR = false;
                    continue;
                }
                if ((c >= 55296 && c < 57344) || c == 65534 || c == 65535) {
                    throw new IllegalCharException("Illegal XML character: 0x" + Integer.toHexString(c));
                }
            }
            if (c >= 32) {
                this.sawCR = false;
                out_buf[out_off + o++] = (char)c;
            }
            else {
                switch (c) {
                    case 10: {
                        if (this.sawCR) {
                            this.sawCR = false;
                            break;
                        }
                        out_buf[out_off + o++] = '\n';
                        break;
                    }
                    case 13: {
                        this.sawCR = true;
                        out_buf[out_off + o++] = '\n';
                        break;
                    }
                    case 9: {
                        out_buf[out_off + o++] = '\t';
                        break;
                    }
                    default: {
                        throw new IllegalCharException("Illegal XML character: 0x" + Integer.toHexString(c));
                    }
                }
            }
        }
        result[0] = i;
        result[1] = o;
    }
    
    public void decodeXMLDecl(final byte[] in_buf, final int in_off, final int in_len, final char[] out_buf, final int out_off, final int out_len, final int[] result) throws CharConversionException {
        int i = 0;
        int o = 0;
    Label_0193:
        for (o = (i = 0); i < in_len && o < out_len; ++i) {
            final int c = in_buf[in_off + i];
            if ((c & 0x80) != 0x0) {
                break;
            }
            if (c >= 32) {
                this.sawCR = false;
                out_buf[out_off + o++] = (char)c;
                if (c == 62) {
                    ++i;
                    break;
                }
            }
            else {
                switch (c) {
                    case 10: {
                        if (this.sawCR) {
                            this.sawCR = false;
                            break;
                        }
                        out_buf[out_off + o++] = '\n';
                        break;
                    }
                    case 13: {
                        this.sawCR = true;
                        out_buf[out_off + o++] = '\n';
                        break;
                    }
                    case 9: {
                        out_buf[out_off + o++] = '\t';
                        break;
                    }
                    default: {
                        break Label_0193;
                    }
                }
            }
        }
        result[0] = i;
        result[1] = o;
    }
}
