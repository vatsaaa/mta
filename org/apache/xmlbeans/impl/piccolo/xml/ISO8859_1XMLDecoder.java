// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.xmlbeans.impl.piccolo.xml;

import org.apache.xmlbeans.impl.piccolo.io.IllegalCharException;
import java.io.CharConversionException;
import org.apache.xmlbeans.impl.piccolo.io.CharsetDecoder;

public final class ISO8859_1XMLDecoder implements XMLDecoder
{
    private boolean sawCR;
    
    public ISO8859_1XMLDecoder() {
        this.sawCR = false;
    }
    
    public CharsetDecoder newCharsetDecoder() {
        return this.newXMLDecoder();
    }
    
    public XMLDecoder newXMLDecoder() {
        return new ISO8859_1XMLDecoder();
    }
    
    public int minBytesPerChar() {
        return 1;
    }
    
    public int maxBytesPerChar() {
        return 1;
    }
    
    public void reset() {
        this.sawCR = false;
    }
    
    public void decode(final byte[] in_buf, final int in_off, final int in_len, final char[] out_buf, final int out_off, final int out_len, final int[] result) throws CharConversionException {
        this.internalDecode(in_buf, in_off, in_len, out_buf, out_off, out_len, result, false);
    }
    
    public void decodeXMLDecl(final byte[] in_buf, final int in_off, final int in_len, final char[] out_buf, final int out_off, final int out_len, final int[] result) throws CharConversionException {
        this.internalDecode(in_buf, in_off, in_len, out_buf, out_off, out_len, result, true);
    }
    
    private void internalDecode(final byte[] in_buf, final int in_off, final int in_len, final char[] out_buf, final int out_off, final int out_len, final int[] result, final boolean decodeDecl) throws CharConversionException {
        int i = 0;
        int o = 0;
    Label_0209:
        for (o = (i = 0); i < in_len && o < out_len; ++i) {
            final char c = (char)(0xFF & in_buf[in_off + i]);
            if (c >= ' ') {
                this.sawCR = false;
                out_buf[out_off + o++] = c;
            }
            else {
                switch (c) {
                    case '\n': {
                        if (this.sawCR) {
                            this.sawCR = false;
                            break;
                        }
                        out_buf[out_off + o++] = '\n';
                        break;
                    }
                    case '\r': {
                        this.sawCR = true;
                        out_buf[out_off + o++] = '\n';
                        break;
                    }
                    case '\t': {
                        out_buf[out_off + o++] = '\t';
                        break;
                    }
                    default: {
                        if (decodeDecl) {
                            break Label_0209;
                        }
                        throw new IllegalCharException("Illegal XML character: 0x" + Integer.toHexString(c));
                    }
                }
            }
        }
        result[0] = i;
        result[1] = o;
    }
}
