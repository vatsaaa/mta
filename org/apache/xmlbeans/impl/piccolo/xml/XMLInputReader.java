// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.xmlbeans.impl.piccolo.xml;

import java.io.IOException;
import java.io.Reader;

public abstract class XMLInputReader extends Reader
{
    private String xmlVersion;
    private boolean xmlStandaloneDeclared;
    private boolean xmlStandalone;
    private String xmlDeclaredEncoding;
    private XMLDeclParser parser;
    
    public XMLInputReader() {
        this.xmlVersion = null;
        this.xmlStandaloneDeclared = false;
        this.xmlStandalone = false;
        this.xmlDeclaredEncoding = null;
        this.parser = new XMLDeclParser();
    }
    
    protected void resetInput() {
        final String s = null;
        this.xmlDeclaredEncoding = s;
        this.xmlVersion = s;
        final boolean b = false;
        this.xmlStandalone = b;
        this.xmlStandaloneDeclared = b;
    }
    
    protected int parseXMLDeclaration(final char[] cbuf, final int offset, final int length) throws IOException {
        this.parser.reset(cbuf, offset, length);
        final int parse = this.parser.parse();
        final XMLDeclParser parser = this.parser;
        if (parse == 1) {
            this.xmlVersion = this.parser.getXMLVersion();
            this.xmlStandalone = this.parser.isXMLStandalone();
            this.xmlStandaloneDeclared = this.parser.isXMLStandaloneDeclared();
            this.xmlDeclaredEncoding = this.parser.getXMLEncoding();
            return this.parser.getCharsRead();
        }
        return 0;
    }
    
    public String getXMLVersion() {
        return this.xmlVersion;
    }
    
    public boolean isXMLStandalone() {
        return this.xmlStandalone;
    }
    
    public boolean isXMLStandaloneDeclared() {
        return this.xmlStandaloneDeclared;
    }
    
    public String getXMLDeclaredEncoding() {
        return this.xmlDeclaredEncoding;
    }
}
