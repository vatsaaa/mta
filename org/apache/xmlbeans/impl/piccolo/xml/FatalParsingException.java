// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.xmlbeans.impl.piccolo.xml;

import org.xml.sax.SAXException;

class FatalParsingException extends SAXException
{
    FatalParsingException(final String msg) {
        super(msg);
    }
    
    FatalParsingException(final String msg, final Exception ex) {
        super(msg, ex);
    }
}
