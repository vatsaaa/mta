// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.xmlbeans.impl.piccolo.xml;

import java.net.MalformedURLException;
import java.io.File;
import org.apache.xmlbeans.impl.piccolo.util.RecursionException;
import java.io.InputStream;
import java.io.Reader;
import java.io.IOException;
import org.xml.sax.InputSource;
import java.net.URL;

public class DocumentEntity implements Entity
{
    private boolean isOpen;
    private URL url;
    private String sysID;
    private InputSource source;
    private static URL defaultContext;
    private boolean isStandalone;
    private XMLStreamReader streamReader;
    private XMLReaderReader readerReader;
    private XMLInputReader activeReader;
    
    public DocumentEntity() {
        this.isOpen = false;
        this.url = null;
        this.sysID = null;
        this.source = null;
        this.isStandalone = false;
        this.streamReader = null;
        this.readerReader = null;
        this.activeReader = null;
    }
    
    public DocumentEntity(final String sysID) throws IOException {
        this.isOpen = false;
        this.url = null;
        this.sysID = null;
        this.source = null;
        this.isStandalone = false;
        this.streamReader = null;
        this.readerReader = null;
        this.activeReader = null;
        this.reset(sysID);
    }
    
    public DocumentEntity(final InputSource source) throws IOException {
        this.isOpen = false;
        this.url = null;
        this.sysID = null;
        this.source = null;
        this.isStandalone = false;
        this.streamReader = null;
        this.readerReader = null;
        this.activeReader = null;
        this.reset(source);
    }
    
    public boolean isOpen() {
        return this.isOpen;
    }
    
    public void open() throws IOException, RecursionException {
        String encoding = null;
        if (this.source != null) {
            final Reader sourceReader = this.source.getCharacterStream();
            if (sourceReader != null) {
                if (this.readerReader == null) {
                    this.readerReader = new XMLReaderReader();
                }
                this.readerReader.reset(sourceReader, true);
                this.isStandalone = this.readerReader.isXMLStandalone();
                this.activeReader = this.readerReader;
                this.isOpen = true;
                return;
            }
            final InputStream in = this.source.getByteStream();
            if (in != null) {
                if (this.streamReader == null) {
                    this.streamReader = new XMLStreamReader();
                }
                this.streamReader.reset(in, this.source.getEncoding(), true);
                this.isOpen = true;
                this.isStandalone = this.streamReader.isXMLStandalone();
                this.activeReader = this.streamReader;
                return;
            }
            this.url = new URL(DocumentEntity.defaultContext, this.source.getSystemId());
            this.sysID = this.url.toString();
            encoding = this.source.getEncoding();
        }
        if (this.streamReader == null) {
            this.streamReader = new XMLStreamReader();
        }
        this.streamReader.reset(this.url.openStream(), encoding, true);
        this.isStandalone = this.streamReader.isXMLStandalone();
        this.activeReader = this.streamReader;
        this.isOpen = true;
    }
    
    public String getDeclaredEncoding() {
        return this.activeReader.getXMLDeclaredEncoding();
    }
    
    public boolean isStandaloneDeclared() {
        return this.activeReader.isXMLStandaloneDeclared();
    }
    
    public String getXMLVersion() {
        return this.activeReader.getXMLVersion();
    }
    
    public void reset(final String sysID) throws IOException {
        this.close();
        this.isStandalone = false;
        this.source = null;
        try {
            this.url = new URL(DocumentEntity.defaultContext, sysID);
        }
        catch (MalformedURLException e) {
            this.url = new File(sysID).toURL();
        }
        this.sysID = this.url.toString();
    }
    
    public void reset(final InputSource source) throws IOException {
        this.close();
        this.isStandalone = false;
        this.source = source;
        this.sysID = source.getSystemId();
        if (this.sysID != null) {
            try {
                this.url = new URL(DocumentEntity.defaultContext, this.sysID);
            }
            catch (MalformedURLException e) {
                this.url = new File(this.sysID).toURL();
            }
            this.sysID = this.url.toString();
        }
    }
    
    public void close() throws IOException {
        if (!this.isOpen) {
            return;
        }
        this.source = null;
        this.activeReader.close();
        this.activeReader = null;
        this.isOpen = false;
    }
    
    public String getPublicID() {
        return null;
    }
    
    public String getSystemID() {
        return this.sysID;
    }
    
    public boolean isStandalone() {
        return this.isStandalone;
    }
    
    public void setStandalone(final boolean standalone) {
        this.isStandalone = standalone;
    }
    
    public boolean isInternal() {
        return false;
    }
    
    public boolean isParsed() {
        return true;
    }
    
    public Reader getReader() {
        return this.activeReader;
    }
    
    public String stringValue() {
        throw new UnsupportedOperationException();
    }
    
    public char[] charArrayValue() {
        throw new UnsupportedOperationException();
    }
    
    static {
        try {
            DocumentEntity.defaultContext = new URL("file", null, ".");
        }
        catch (IOException e) {
            DocumentEntity.defaultContext = null;
        }
    }
}
