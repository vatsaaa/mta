// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.xmlbeans.impl.piccolo.xml;

import org.apache.xmlbeans.impl.piccolo.util.IntStack;
import org.apache.xmlbeans.impl.piccolo.util.StringStack;

public class FastNamespaceSupport
{
    public static final String XMLNS = "http://www.w3.org/XML/1998/namespace";
    private String[] prefixes;
    private String[] uris;
    private int prefixPos;
    private String defaultURI;
    private StringStack defaultURIs;
    private int prefixCount;
    private IntStack contextPrefixCounts;
    private int defaultURIContexts;
    private IntStack defaultURIContextCounts;
    
    public FastNamespaceSupport() {
        this.prefixes = new String[20];
        this.uris = new String[20];
        this.defaultURIs = new StringStack(20);
        this.contextPrefixCounts = new IntStack(20);
        this.defaultURIContextCounts = new IntStack(20);
        this.reset();
    }
    
    public void reset() {
        this.defaultURIs.clear();
        this.contextPrefixCounts.clear();
        this.defaultURIContextCounts.clear();
        this.prefixPos = -1;
        this.defaultURI = "";
        this.prefixCount = 0;
        this.defaultURIContexts = 0;
    }
    
    public void pushContext() {
        ++this.defaultURIContexts;
        this.contextPrefixCounts.push(this.prefixCount);
        this.prefixCount = 0;
    }
    
    public void popContext() {
        if (this.defaultURIContexts <= 0) {
            this.defaultURIContexts = this.defaultURIContextCounts.pop();
            this.defaultURI = this.defaultURIs.pop();
        }
        else {
            --this.defaultURIContexts;
        }
        this.prefixPos -= this.prefixCount;
        this.prefixCount = this.contextPrefixCounts.pop();
    }
    
    public void declarePrefix(final String prefix, final String uri) {
        if (prefix.length() == 0) {
            --this.defaultURIContexts;
            this.defaultURIContextCounts.push(this.defaultURIContexts);
            this.defaultURIs.push(this.defaultURI);
            this.defaultURIContexts = 0;
            this.defaultURI = uri;
        }
        else {
            for (int i = 0; i < this.prefixCount; ++i) {
                if (prefix == this.prefixes[this.prefixPos - i]) {
                    this.uris[this.prefixPos - i] = uri;
                    return;
                }
            }
            ++this.prefixPos;
            ++this.prefixCount;
            if (this.prefixPos >= this.prefixes.length) {
                final int oldLength = this.prefixes.length;
                final int newLength = oldLength * 2;
                final String[] newPrefixes = new String[newLength];
                final String[] newURIs = new String[newLength];
                System.arraycopy(this.prefixes, 0, newPrefixes, 0, oldLength);
                System.arraycopy(this.uris, 0, newURIs, 0, oldLength);
                this.prefixes = newPrefixes;
                this.uris = newURIs;
            }
            this.prefixes[this.prefixPos] = prefix;
            this.uris[this.prefixPos] = uri;
        }
    }
    
    public String[] processName(final String qName, final String[] parts, final boolean isAttribute) {
        final int colon = qName.indexOf(58);
        parts[2] = qName;
        if (colon < 0) {
            parts[1] = qName;
            if (isAttribute) {
                parts[0] = "";
            }
            else {
                parts[0] = this.defaultURI;
            }
            return parts;
        }
        final String prefix = qName.substring(0, colon);
        parts[1] = qName.substring(colon + 1);
        if ((parts[0] = this.getURI(prefix)) == "") {
            return null;
        }
        return parts;
    }
    
    public String getDefaultURI() {
        return this.defaultURI;
    }
    
    public String getURI(final String prefix) {
        if (prefix == null || prefix.length() == 0) {
            return this.defaultURI;
        }
        if (prefix == "xml") {
            return "http://www.w3.org/XML/1998/namespace";
        }
        for (int i = this.prefixPos; i >= 0; --i) {
            if (prefix == this.prefixes[i]) {
                return this.uris[i];
            }
        }
        return "";
    }
    
    public int getContextSize() {
        return this.prefixCount + ((this.defaultURIContexts == 0 && this.defaultURI != "") ? 1 : 0);
    }
    
    public String getContextPrefix(final int index) {
        if (index == this.prefixCount && this.defaultURIContexts == 0 && this.defaultURI != "") {
            return "";
        }
        return this.prefixes[this.prefixPos - index];
    }
    
    public String getContextURI(final int index) {
        if (index == this.prefixCount && this.defaultURIContexts == 0 && this.defaultURI != "") {
            return this.defaultURI;
        }
        return this.uris[this.prefixPos - index];
    }
}
