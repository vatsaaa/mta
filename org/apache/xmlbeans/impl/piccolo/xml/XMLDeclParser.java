// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.xmlbeans.impl.piccolo.xml;

import org.apache.xmlbeans.impl.piccolo.io.FileFormatException;
import java.io.InputStreamReader;
import java.io.InputStream;
import java.io.IOException;
import java.io.Reader;

final class XMLDeclParser
{
    public static final int YYEOF = -1;
    private static final int YY_BUFFERSIZE = 0;
    public static final int GOT_ENCODING = 5;
    public static final int ENCODING = 4;
    public static final int XML_DECL = 1;
    public static final int YYINITIAL = 0;
    public static final int STANDALONE = 6;
    public static final int GOT_VERSION = 3;
    public static final int VERSION = 2;
    public static final int GOT_STANDALONE = 7;
    private static final String yycmap_packed = "\t\u0000\u0001\u0001\u0001\u0001\u0002\u0000\u0001\u0001\u0012\u0000\u0001\u0001\u0001\u0000\u0001\u0012\u0004\u0000\u0001\u0016\u0005\u0000\u0001 \u0001\u0014\u0001\u0000\u0001\u0015\u0001\u0013\u0003\u0005\u0001(\u0001&\u0001\u0005\u0001!\u0001)\u0001\u0003\u0001\u0000\u0001\u0006\u0001\u0002\u0001\u001c\u0001\u0007\u0001\u0000\u0001#\u0001\u0004\u0001$\u0002\u0004\u0001\u001f\u0002\u0004\u0001%\u0005\u0004\u0001'\u0003\u0004\u0001\"\u0001\u001e\u0001\u001d\u0005\u0004\u0004\u0000\u0001\u0005\u0001\u0000\u0001\u001b\u0001\u0004\u0001\u0017\u0001\u0018\u0001\f\u0001\u0004\u0001\u0019\u0001\u0004\u0001\u000f\u0002\u0004\u0001\n\u0001\t\u0001\u0011\u0001\u0010\u0002\u0004\u0001\r\u0001\u000e\u0001\u001a\u0001\u0004\u0001\u000b\u0001\u0004\u0001\b\u0001*\u0001\u0004\uff85\u0000";
    private static final char[] yycmap;
    private static final int[] yy_rowMap;
    private static final String yy_packed0 = "\u0006\t\u0001\n$\t\u0001\u000b\u0001\f\u0005\u000b\u0001\r5\u000b\u0001\u000e\u0003\u000b\u0001\u000f\u0015\u000b\u0001\u0010\u0005\u000b\u0001\r5\u000b\u0001\u0011\u0003\u000b\u0001\u0012\u0015\u000b\u0001\u0013\u0005\u000b\u0001\r5\u000b\u0001\u0014\u0003\u000b\u0001\u0015\u0015\u000b\u0001\u0016\u0005\u000b\u0001\r#\u000b2\u0000\u0001\u0017$\u0000\u0001\u0018\u0005\u0000\u0001\u0019\u0003\u0000\u0001\u001a\u0001\u001b\u0001\u0000\u0001\u001c8\u0000\u0001\u001d\u0011\u0000\u0003\u001e\u0002\u0000\n\u001e\u0001\u0000\u0001\u001f\u0002\u001e\u0001\u0000\u0005\u001e\u0001\u0000\u000e\u001e\u0003\u0000\u0003 \u0002\u0000\n \u0001\u0000\u0001!\u0002 \u0001\u0000\u0005 \u0001\u0000\u000e \u0001\u0000\u0001\"\u0005\u0000\u0001\u0019\u0004\u0000\u0001\u001b\u0001\u0000\u0001\u001c \u0000\u0001#\u0003\u0000\n#\u0005\u0000\u0005#\u0001\u0000\u0001$\u0002#\u0002\u0000\u0001#\u0001%\u0001#\u0001&\u0001\u0000\u0001#\u0002\u0000\u0001#\u0004\u0000\u0001'\u0003\u0000\n'\u0005\u0000\u0005'\u0001\u0000\u0001(\u0002'\u0002\u0000\u0001'\u0001)\u0001'\u0001*\u0001\u0000\u0001'\u0002\u0000\u0001'\u0001\u0000\u0001+\u0005\u0000\u0001\u0019\u0006\u0000\u0001\u001c-\u0000\u0001,\u0018\u0000\u0001-\u0011\u0000\u0001.\u0018\u0000\u0001/\u0001\u0000\u00010\u0005\u0000\u0001\u0019+\u0000\u00011.\u0000\u00012/\u0000\u000133\u0000\u00014\u0013\u0000\u0003\u001e\u0002\u0000\n\u001e\u00015\u0003\u001e\u0001\u0000\u0005\u001e\u0001\u0000\u000e\u001e\u0003\u0000\u0003\u001e\u0002\u0000\n\u001e\u00015\u0001\u001e\u00016\u0001\u001e\u0001\u0000\u0005\u001e\u0001\u0000\u000e\u001e\u0003\u0000\u0003 \u0002\u0000\n \u0001\u0000\u0003 \u00015\u0005 \u0001\u0000\u000e \u0003\u0000\u0003 \u0002\u0000\n \u0001\u0000\u0001 \u00017\u0001 \u00015\u0005 \u0001\u0000\u000e \u0004\u0000\u0002#\u0002\u0000\n#\u00018\u0003#\u0001\u0000\u0005#\u0001\u0000\u000e#\u0004\u0000\u0002#\u0002\u0000\n#\u00018\u0003#\u0001\u0000\u0005#\u0001\u0000\u0001#\u00019\u0003#\u0001:\b#\u0004\u0000\u0002#\u0002\u0000\n#\u00018\u0003#\u0001\u0000\u0005#\u0001\u0000\u0005#\u0001;\b#\u0004\u0000\u0002#\u0002\u0000\n#\u00018\u0003#\u0001\u0000\u0005#\u0001\u0000\u0005#\u0001<\b#\u0004\u0000\u0002'\u0002\u0000\n'\u0001\u0000\u0003'\u00018\u0005'\u0001\u0000\u000e'\u0004\u0000\u0002'\u0002\u0000\n'\u0001\u0000\u0003'\u00018\u0005'\u0001\u0000\u0001'\u0001=\u0003'\u0001>\b'\u0004\u0000\u0002'\u0002\u0000\n'\u0001\u0000\u0003'\u00018\u0005'\u0001\u0000\u0005'\u0001?\b'\u0004\u0000\u0002'\u0002\u0000\n'\u0001\u0000\u0003'\u00018\u0005'\u0001\u0000\u0005'\u0001@\b'\u0010\u0000\u0001A&\u0000\u0001B.\u0000\u0001C&\u0000\u0001D'\u0000\u0001E.\u0000\u0001F4\u0000\u0001G.\u0000\u0001H\u0012\u0000\u0003\u001e\u0002\u0000\n\u001e\u00015\u0002\u001e\u0001I\u0001\u0000\u0005\u001e\u0001\u0000\u000e\u001e\u0003\u0000\u0003 \u0002\u0000\n \u0001\u0000\u0002 \u0001J\u00015\u0005 \u0001\u0000\u000e \u0004\u0000\u0002#\u0002\u0000\n#\u00018\u0003#\u0001\u0000\u0005#\u0001\u0000\u0002#\u0001K\u000b#\u0004\u0000\u0002#\u0002\u0000\n#\u00018\u0003#\u0001\u0000\u0005#\u0001\u0000\u0003#\u0001L\n#\u0004\u0000\u0002#\u0002\u0000\n#\u00018\u0003#\u0001\u0000\u0005#\u0001\u0000\u0007#\u0001M\u0006#\u0004\u0000\u0002#\u0002\u0000\n#\u00018\u0003#\u0001\u0000\u0005#\u0001\u0000\n#\u0001N\u0003#\u0004\u0000\u0002'\u0002\u0000\n'\u0001\u0000\u0003'\u00018\u0005'\u0001\u0000\u0002'\u0001O\u000b'\u0004\u0000\u0002'\u0002\u0000\n'\u0001\u0000\u0003'\u00018\u0005'\u0001\u0000\u0003'\u0001P\n'\u0004\u0000\u0002'\u0002\u0000\n'\u0001\u0000\u0003'\u00018\u0005'\u0001\u0000\u0007'\u0001Q\u0006'\u0004\u0000\u0002'\u0002\u0000\n'\u0001\u0000\u0003'\u00018\u0005'\u0001\u0000\n'\u0001R\u0003'\u0012\u0000\u0001S&\u0000\u0001T2\u0000\u0001S\"\u0000\u0001U&\u0000\u0001V.\u0000\u0001W,\u0000\u0001X+\u0000\u0001Y\u001c\u0000\u0003\u001e\u0002\u0000\n\u001e\u0001Z\u0003\u001e\u0001\u0000\u0005\u001e\u0001\u0000\u000e\u001e\u0003\u0000\u0003 \u0002\u0000\n \u0001\u0000\u0003 \u0001Z\u0005 \u0001\u0000\u000e \u0004\u0000\u0002#\u0002\u0000\n#\u00018\u0001[\u0002#\u0001\u0000\u0005#\u0001\u0000\u0003#\u0001\\\u0001]\t#\u0004\u0000\u0002#\u0002\u0000\n#\u00018\u0003#\u0001\u0000\u0005#\u0001\u0000\u0006#\u0001%\u0007#\u0004\u0000\u0002#\u0002\u0000\n#\u00018\u0003#\u0001\u0000\u0005#\u0001\u0000\b#\u0001^\u0005#\u0004\u0000\u0002#\u0002\u0000\n#\u00018\u0003#\u0001\u0000\u0005#\u0001\u0000\u0003#\u0001_\n#\u0004\u0000\u0002'\u0002\u0000\n'\u0001\u0000\u0001`\u0002'\u00018\u0005'\u0001\u0000\u0003'\u0001a\u0001b\t'\u0004\u0000\u0002'\u0002\u0000\n'\u0001\u0000\u0003'\u00018\u0005'\u0001\u0000\u0006'\u0001)\u0007'\u0004\u0000\u0002'\u0002\u0000\n'\u0001\u0000\u0003'\u00018\u0005'\u0001\u0000\b'\u0001c\u0005'\u0004\u0000\u0002'\u0002\u0000\n'\u0001\u0000\u0003'\u00018\u0005'\u0001\u0000\u0003'\u0001d\n'\u0012\u0000\u0001e.\u0000\u0001e\u0015\u0000\u0001f8\u0000\u0001g3\u0000\u0001h*\u0000\u0001i\u0016\u0000\u0002#\u0002\u0000\n#\u00018\u0003#\u0001\u0000\u0005#\u0001\u0000\t#\u0001j\u0004#\u0004\u0000\u0002#\u0002\u0000\n#\u00018\u0001[\u0002#\u0001\u0000\u0005#\u0001\u0000\u0004#\u0001]\t#\u0004\u0000\u0002#\u0002\u0000\n#\u0001k\u0003#\u0001\u0000\u0005#\u0001\u0000\u000e#\u0004\u0000\u0002#\u0002\u0000\n#\u00018\u0003#\u0001\u0000\u0005#\u0001\u0000\b#\u0001l\u0005#\u0004\u0000\u0002#\u0002\u0000\n#\u00018\u0003#\u0001\u0000\u0005#\u0001\u0000\u0004#\u0001m\t#\u0004\u0000\u0002'\u0002\u0000\n'\u0001\u0000\u0003'\u00018\u0005'\u0001\u0000\t'\u0001n\u0004'\u0004\u0000\u0002'\u0002\u0000\n'\u0001\u0000\u0001`\u0002'\u00018\u0005'\u0001\u0000\u0004'\u0001b\t'\u0004\u0000\u0002'\u0002\u0000\n'\u0001\u0000\u0003'\u0001k\u0005'\u0001\u0000\u000e'\u0004\u0000\u0002'\u0002\u0000\n'\u0001\u0000\u0003'\u00018\u0005'\u0001\u0000\b'\u0001o\u0005'\u0004\u0000\u0002'\u0002\u0000\n'\u0001\u0000\u0003'\u00018\u0005'\u0001\u0000\u0004'\u0001p\t'\u0010\u0000\u0001q)\u0000\u0001r6\u0000\u0001s\u0013\u0000\u0002#\u0002\u0000\n#\u0001t\u0003#\u0001\u0000\u0005#\u0001\u0000\u000e#\u0004\u0000\u0002#\u0002\u0000\n#\u0001u\u0003#\u0001\u0000\u0005#\u0001\u0000\u000e#\u0004\u0000\u0002#\u0002\u0000\n#\u00018\u0003#\u0001\u0000\u0005#\u0001\u0000\u0004#\u0001v\t#\u0004\u0000\u0002'\u0002\u0000\n'\u0001\u0000\u0003'\u0001t\u0005'\u0001\u0000\u000e'\u0004\u0000\u0002'\u0002\u0000\n'\u0001\u0000\u0003'\u0001u\u0005'\u0001\u0000\u000e'\u0004\u0000\u0002'\u0002\u0000\n'\u0001\u0000\u0003'\u00018\u0005'\u0001\u0000\u0004'\u0001w\t'\u0011\u0000\u0001x*\u0000\u0001y#\u0000\u0001z$\u0000\u0002#\u0002\u0000\n#\u00018\u0003#\u0001\u0000\u0005#\u0001\u0000\u000b#\u0001{\u0002#\u0004\u0000\u0002'\u0002\u0000\n'\u0001\u0000\u0003'\u00018\u0005'\u0001\u0000\u000b'\u0001|\u0002'\u0001\u0000\u0001x\u0001}A\u0000\u0001~!\u0000\u0001\u007f\u001e\u0000\u0002#\u0002\u0000\n#\u00018\u0003#\u0001\u0000\u0005#\u0001\u0000\f#\u0001\u0080\u0001#\u0004\u0000\u0002'\u0002\u0000\n'\u0001\u0000\u0003'\u00018\u0005'\u0001\u0000\f'\u0001\u0081\u0001'\u0001\u0000\u0001}*\u0000\u0001~\u0001\u00829\u0000\u0001\u0083\u001d\u0000\u0002#\u0002\u0000\n#\u00018\u0003#\u0001\u0000\u0005#\u0001\u0000\u0003#\u0001\u0084\n#\u0004\u0000\u0002'\u0002\u0000\n'\u0001\u0000\u0003'\u00018\u0005'\u0001\u0000\u0003'\u0001\u0085\n'\u0001\u0000\u0001\u00825\u0000\u0001\u0086\"\u0000\u0002#\u0002\u0000\n#\u00018\u0001\u0087\u0002#\u0001\u0000\u0005#\u0001\u0000\u000e#\u0004\u0000\u0002'\u0002\u0000\n'\u0001\u0000\u0001\u0088\u0002'\u00018\u0005'\u0001\u0000\u000e'\u0001\u0000\u0001\u0086\u0001\u0089,\u0000\u0002#\u0002\u0000\n#\u0001\u008a\u0003#\u0001\u0000\u0005#\u0001\u0000\u000e#\u0004\u0000\u0002'\u0002\u0000\n'\u0001\u0000\u0003'\u0001\u008a\u0005'\u0001\u0000\u000e'\u0001\u0000\u0001\u0089)\u0000";
    private static final int[] yytrans;
    private static final int YY_UNKNOWN_ERROR = 0;
    private static final int YY_ILLEGAL_STATE = 1;
    private static final int YY_NO_MATCH = 2;
    private static final int YY_PUSHBACK_2BIG = 3;
    private static final int YY_SKIP_2BIG = 4;
    private static final String[] YY_ERROR_MSG;
    private static final byte[] YY_ATTRIBUTE;
    private Reader yy_reader;
    private int yy_state;
    private int yy_lexical_state;
    private char[] yy_buffer;
    private char[] yy_saved_buffer;
    private int yy_markedPos;
    private int yy_pushbackPos;
    private int yy_currentPos;
    private int yy_startRead;
    private int yy_endRead;
    private int yyline;
    private int yychar;
    private int yycolumn;
    private boolean yy_atBOL;
    private boolean yy_atEOF;
    public static final int SUCCESS = 1;
    public static final int NO_DECLARATION = -1;
    private String xmlVersion;
    private String xmlEncoding;
    private boolean xmlStandalone;
    private boolean xmlStandaloneDeclared;
    private int yy_currentPos_l;
    private int yy_startRead_l;
    private int yy_markedPos_l;
    private int yy_endRead_l;
    private char[] yy_buffer_l;
    private char[] yycmap_l;
    private boolean yy_sawCR;
    private boolean yy_prev_sawCR;
    private int yyline_next;
    private int yycolumn_next;
    
    public XMLDeclParser(final char[] buf, final int off, final int len) throws IOException {
        this.yy_lexical_state = 0;
        this.yy_buffer = new char[0];
        this.yy_saved_buffer = null;
        this.yy_atBOL = true;
        this.xmlVersion = null;
        this.xmlEncoding = null;
        this.xmlStandalone = false;
        this.xmlStandaloneDeclared = false;
        this.yy_sawCR = false;
        this.yy_prev_sawCR = false;
        this.yyline_next = 0;
        this.yycolumn_next = 0;
        this.yyreset(buf, off, len);
    }
    
    public XMLDeclParser() {
        this.yy_lexical_state = 0;
        this.yy_buffer = new char[0];
        this.yy_saved_buffer = null;
        this.yy_atBOL = true;
        this.xmlVersion = null;
        this.xmlEncoding = null;
        this.xmlStandalone = false;
        this.xmlStandaloneDeclared = false;
        this.yy_sawCR = false;
        this.yy_prev_sawCR = false;
        this.yyline_next = 0;
        this.yycolumn_next = 0;
    }
    
    public void reset(final char[] buf, final int off, final int len) throws IOException {
        final String s = null;
        this.xmlEncoding = s;
        this.xmlVersion = s;
        final boolean b = false;
        this.xmlStandalone = b;
        this.xmlStandaloneDeclared = b;
        this.yyreset(buf, off, len);
    }
    
    public String getXMLVersion() {
        return this.xmlVersion;
    }
    
    public String getXMLEncoding() {
        return this.xmlEncoding;
    }
    
    public boolean isXMLStandaloneDeclared() {
        return this.xmlStandaloneDeclared;
    }
    
    public boolean isXMLStandalone() {
        return this.xmlStandalone;
    }
    
    public int getCharsRead() {
        return this.yychar + this.yylength();
    }
    
    XMLDeclParser(final Reader in) {
        this.yy_lexical_state = 0;
        this.yy_buffer = new char[0];
        this.yy_saved_buffer = null;
        this.yy_atBOL = true;
        this.xmlVersion = null;
        this.xmlEncoding = null;
        this.xmlStandalone = false;
        this.xmlStandaloneDeclared = false;
        this.yy_sawCR = false;
        this.yy_prev_sawCR = false;
        this.yyline_next = 0;
        this.yycolumn_next = 0;
        this.yy_reader = in;
    }
    
    XMLDeclParser(final InputStream in) {
        this(new InputStreamReader(in));
    }
    
    private static int[] yy_unpack() {
        final int[] trans = new int[5203];
        int offset = 0;
        offset = yy_unpack("\u0006\t\u0001\n$\t\u0001\u000b\u0001\f\u0005\u000b\u0001\r5\u000b\u0001\u000e\u0003\u000b\u0001\u000f\u0015\u000b\u0001\u0010\u0005\u000b\u0001\r5\u000b\u0001\u0011\u0003\u000b\u0001\u0012\u0015\u000b\u0001\u0013\u0005\u000b\u0001\r5\u000b\u0001\u0014\u0003\u000b\u0001\u0015\u0015\u000b\u0001\u0016\u0005\u000b\u0001\r#\u000b2\u0000\u0001\u0017$\u0000\u0001\u0018\u0005\u0000\u0001\u0019\u0003\u0000\u0001\u001a\u0001\u001b\u0001\u0000\u0001\u001c8\u0000\u0001\u001d\u0011\u0000\u0003\u001e\u0002\u0000\n\u001e\u0001\u0000\u0001\u001f\u0002\u001e\u0001\u0000\u0005\u001e\u0001\u0000\u000e\u001e\u0003\u0000\u0003 \u0002\u0000\n \u0001\u0000\u0001!\u0002 \u0001\u0000\u0005 \u0001\u0000\u000e \u0001\u0000\u0001\"\u0005\u0000\u0001\u0019\u0004\u0000\u0001\u001b\u0001\u0000\u0001\u001c \u0000\u0001#\u0003\u0000\n#\u0005\u0000\u0005#\u0001\u0000\u0001$\u0002#\u0002\u0000\u0001#\u0001%\u0001#\u0001&\u0001\u0000\u0001#\u0002\u0000\u0001#\u0004\u0000\u0001'\u0003\u0000\n'\u0005\u0000\u0005'\u0001\u0000\u0001(\u0002'\u0002\u0000\u0001'\u0001)\u0001'\u0001*\u0001\u0000\u0001'\u0002\u0000\u0001'\u0001\u0000\u0001+\u0005\u0000\u0001\u0019\u0006\u0000\u0001\u001c-\u0000\u0001,\u0018\u0000\u0001-\u0011\u0000\u0001.\u0018\u0000\u0001/\u0001\u0000\u00010\u0005\u0000\u0001\u0019+\u0000\u00011.\u0000\u00012/\u0000\u000133\u0000\u00014\u0013\u0000\u0003\u001e\u0002\u0000\n\u001e\u00015\u0003\u001e\u0001\u0000\u0005\u001e\u0001\u0000\u000e\u001e\u0003\u0000\u0003\u001e\u0002\u0000\n\u001e\u00015\u0001\u001e\u00016\u0001\u001e\u0001\u0000\u0005\u001e\u0001\u0000\u000e\u001e\u0003\u0000\u0003 \u0002\u0000\n \u0001\u0000\u0003 \u00015\u0005 \u0001\u0000\u000e \u0003\u0000\u0003 \u0002\u0000\n \u0001\u0000\u0001 \u00017\u0001 \u00015\u0005 \u0001\u0000\u000e \u0004\u0000\u0002#\u0002\u0000\n#\u00018\u0003#\u0001\u0000\u0005#\u0001\u0000\u000e#\u0004\u0000\u0002#\u0002\u0000\n#\u00018\u0003#\u0001\u0000\u0005#\u0001\u0000\u0001#\u00019\u0003#\u0001:\b#\u0004\u0000\u0002#\u0002\u0000\n#\u00018\u0003#\u0001\u0000\u0005#\u0001\u0000\u0005#\u0001;\b#\u0004\u0000\u0002#\u0002\u0000\n#\u00018\u0003#\u0001\u0000\u0005#\u0001\u0000\u0005#\u0001<\b#\u0004\u0000\u0002'\u0002\u0000\n'\u0001\u0000\u0003'\u00018\u0005'\u0001\u0000\u000e'\u0004\u0000\u0002'\u0002\u0000\n'\u0001\u0000\u0003'\u00018\u0005'\u0001\u0000\u0001'\u0001=\u0003'\u0001>\b'\u0004\u0000\u0002'\u0002\u0000\n'\u0001\u0000\u0003'\u00018\u0005'\u0001\u0000\u0005'\u0001?\b'\u0004\u0000\u0002'\u0002\u0000\n'\u0001\u0000\u0003'\u00018\u0005'\u0001\u0000\u0005'\u0001@\b'\u0010\u0000\u0001A&\u0000\u0001B.\u0000\u0001C&\u0000\u0001D'\u0000\u0001E.\u0000\u0001F4\u0000\u0001G.\u0000\u0001H\u0012\u0000\u0003\u001e\u0002\u0000\n\u001e\u00015\u0002\u001e\u0001I\u0001\u0000\u0005\u001e\u0001\u0000\u000e\u001e\u0003\u0000\u0003 \u0002\u0000\n \u0001\u0000\u0002 \u0001J\u00015\u0005 \u0001\u0000\u000e \u0004\u0000\u0002#\u0002\u0000\n#\u00018\u0003#\u0001\u0000\u0005#\u0001\u0000\u0002#\u0001K\u000b#\u0004\u0000\u0002#\u0002\u0000\n#\u00018\u0003#\u0001\u0000\u0005#\u0001\u0000\u0003#\u0001L\n#\u0004\u0000\u0002#\u0002\u0000\n#\u00018\u0003#\u0001\u0000\u0005#\u0001\u0000\u0007#\u0001M\u0006#\u0004\u0000\u0002#\u0002\u0000\n#\u00018\u0003#\u0001\u0000\u0005#\u0001\u0000\n#\u0001N\u0003#\u0004\u0000\u0002'\u0002\u0000\n'\u0001\u0000\u0003'\u00018\u0005'\u0001\u0000\u0002'\u0001O\u000b'\u0004\u0000\u0002'\u0002\u0000\n'\u0001\u0000\u0003'\u00018\u0005'\u0001\u0000\u0003'\u0001P\n'\u0004\u0000\u0002'\u0002\u0000\n'\u0001\u0000\u0003'\u00018\u0005'\u0001\u0000\u0007'\u0001Q\u0006'\u0004\u0000\u0002'\u0002\u0000\n'\u0001\u0000\u0003'\u00018\u0005'\u0001\u0000\n'\u0001R\u0003'\u0012\u0000\u0001S&\u0000\u0001T2\u0000\u0001S\"\u0000\u0001U&\u0000\u0001V.\u0000\u0001W,\u0000\u0001X+\u0000\u0001Y\u001c\u0000\u0003\u001e\u0002\u0000\n\u001e\u0001Z\u0003\u001e\u0001\u0000\u0005\u001e\u0001\u0000\u000e\u001e\u0003\u0000\u0003 \u0002\u0000\n \u0001\u0000\u0003 \u0001Z\u0005 \u0001\u0000\u000e \u0004\u0000\u0002#\u0002\u0000\n#\u00018\u0001[\u0002#\u0001\u0000\u0005#\u0001\u0000\u0003#\u0001\\\u0001]\t#\u0004\u0000\u0002#\u0002\u0000\n#\u00018\u0003#\u0001\u0000\u0005#\u0001\u0000\u0006#\u0001%\u0007#\u0004\u0000\u0002#\u0002\u0000\n#\u00018\u0003#\u0001\u0000\u0005#\u0001\u0000\b#\u0001^\u0005#\u0004\u0000\u0002#\u0002\u0000\n#\u00018\u0003#\u0001\u0000\u0005#\u0001\u0000\u0003#\u0001_\n#\u0004\u0000\u0002'\u0002\u0000\n'\u0001\u0000\u0001`\u0002'\u00018\u0005'\u0001\u0000\u0003'\u0001a\u0001b\t'\u0004\u0000\u0002'\u0002\u0000\n'\u0001\u0000\u0003'\u00018\u0005'\u0001\u0000\u0006'\u0001)\u0007'\u0004\u0000\u0002'\u0002\u0000\n'\u0001\u0000\u0003'\u00018\u0005'\u0001\u0000\b'\u0001c\u0005'\u0004\u0000\u0002'\u0002\u0000\n'\u0001\u0000\u0003'\u00018\u0005'\u0001\u0000\u0003'\u0001d\n'\u0012\u0000\u0001e.\u0000\u0001e\u0015\u0000\u0001f8\u0000\u0001g3\u0000\u0001h*\u0000\u0001i\u0016\u0000\u0002#\u0002\u0000\n#\u00018\u0003#\u0001\u0000\u0005#\u0001\u0000\t#\u0001j\u0004#\u0004\u0000\u0002#\u0002\u0000\n#\u00018\u0001[\u0002#\u0001\u0000\u0005#\u0001\u0000\u0004#\u0001]\t#\u0004\u0000\u0002#\u0002\u0000\n#\u0001k\u0003#\u0001\u0000\u0005#\u0001\u0000\u000e#\u0004\u0000\u0002#\u0002\u0000\n#\u00018\u0003#\u0001\u0000\u0005#\u0001\u0000\b#\u0001l\u0005#\u0004\u0000\u0002#\u0002\u0000\n#\u00018\u0003#\u0001\u0000\u0005#\u0001\u0000\u0004#\u0001m\t#\u0004\u0000\u0002'\u0002\u0000\n'\u0001\u0000\u0003'\u00018\u0005'\u0001\u0000\t'\u0001n\u0004'\u0004\u0000\u0002'\u0002\u0000\n'\u0001\u0000\u0001`\u0002'\u00018\u0005'\u0001\u0000\u0004'\u0001b\t'\u0004\u0000\u0002'\u0002\u0000\n'\u0001\u0000\u0003'\u0001k\u0005'\u0001\u0000\u000e'\u0004\u0000\u0002'\u0002\u0000\n'\u0001\u0000\u0003'\u00018\u0005'\u0001\u0000\b'\u0001o\u0005'\u0004\u0000\u0002'\u0002\u0000\n'\u0001\u0000\u0003'\u00018\u0005'\u0001\u0000\u0004'\u0001p\t'\u0010\u0000\u0001q)\u0000\u0001r6\u0000\u0001s\u0013\u0000\u0002#\u0002\u0000\n#\u0001t\u0003#\u0001\u0000\u0005#\u0001\u0000\u000e#\u0004\u0000\u0002#\u0002\u0000\n#\u0001u\u0003#\u0001\u0000\u0005#\u0001\u0000\u000e#\u0004\u0000\u0002#\u0002\u0000\n#\u00018\u0003#\u0001\u0000\u0005#\u0001\u0000\u0004#\u0001v\t#\u0004\u0000\u0002'\u0002\u0000\n'\u0001\u0000\u0003'\u0001t\u0005'\u0001\u0000\u000e'\u0004\u0000\u0002'\u0002\u0000\n'\u0001\u0000\u0003'\u0001u\u0005'\u0001\u0000\u000e'\u0004\u0000\u0002'\u0002\u0000\n'\u0001\u0000\u0003'\u00018\u0005'\u0001\u0000\u0004'\u0001w\t'\u0011\u0000\u0001x*\u0000\u0001y#\u0000\u0001z$\u0000\u0002#\u0002\u0000\n#\u00018\u0003#\u0001\u0000\u0005#\u0001\u0000\u000b#\u0001{\u0002#\u0004\u0000\u0002'\u0002\u0000\n'\u0001\u0000\u0003'\u00018\u0005'\u0001\u0000\u000b'\u0001|\u0002'\u0001\u0000\u0001x\u0001}A\u0000\u0001~!\u0000\u0001\u007f\u001e\u0000\u0002#\u0002\u0000\n#\u00018\u0003#\u0001\u0000\u0005#\u0001\u0000\f#\u0001\u0080\u0001#\u0004\u0000\u0002'\u0002\u0000\n'\u0001\u0000\u0003'\u00018\u0005'\u0001\u0000\f'\u0001\u0081\u0001'\u0001\u0000\u0001}*\u0000\u0001~\u0001\u00829\u0000\u0001\u0083\u001d\u0000\u0002#\u0002\u0000\n#\u00018\u0003#\u0001\u0000\u0005#\u0001\u0000\u0003#\u0001\u0084\n#\u0004\u0000\u0002'\u0002\u0000\n'\u0001\u0000\u0003'\u00018\u0005'\u0001\u0000\u0003'\u0001\u0085\n'\u0001\u0000\u0001\u00825\u0000\u0001\u0086\"\u0000\u0002#\u0002\u0000\n#\u00018\u0001\u0087\u0002#\u0001\u0000\u0005#\u0001\u0000\u000e#\u0004\u0000\u0002'\u0002\u0000\n'\u0001\u0000\u0001\u0088\u0002'\u00018\u0005'\u0001\u0000\u000e'\u0001\u0000\u0001\u0086\u0001\u0089,\u0000\u0002#\u0002\u0000\n#\u0001\u008a\u0003#\u0001\u0000\u0005#\u0001\u0000\u000e#\u0004\u0000\u0002'\u0002\u0000\n'\u0001\u0000\u0003'\u0001\u008a\u0005'\u0001\u0000\u000e'\u0001\u0000\u0001\u0089)\u0000", offset, trans);
        return trans;
    }
    
    private static int yy_unpack(final String packed, final int offset, final int[] trans) {
        int i = 0;
        int j = offset;
        final int l = packed.length();
        while (i < l) {
            int count = packed.charAt(i++);
            int value = packed.charAt(i++);
            --value;
            do {
                trans[j++] = value;
            } while (--count > 0);
        }
        return j;
    }
    
    private static char[] yy_unpack_cmap(final String packed) {
        final char[] map = new char[65536];
        int i = 0;
        int j = 0;
        while (i < 144) {
            int count = packed.charAt(i++);
            final char value = packed.charAt(i++);
            do {
                map[j++] = value;
            } while (--count > 0);
        }
        return map;
    }
    
    private boolean yy_refill() throws IOException {
        if (this.yy_startRead > 0) {
            System.arraycopy(this.yy_buffer, this.yy_startRead, this.yy_buffer, 0, this.yy_endRead - this.yy_startRead);
            this.yy_endRead -= this.yy_startRead;
            this.yy_currentPos -= this.yy_startRead;
            this.yy_markedPos -= this.yy_startRead;
            this.yy_pushbackPos -= this.yy_startRead;
            this.yy_startRead = 0;
        }
        if (this.yy_markedPos >= this.yy_buffer.length || this.yy_currentPos >= this.yy_buffer.length) {
            final char[] newBuffer = new char[this.yy_buffer.length * 2];
            System.arraycopy(this.yy_buffer, 0, newBuffer, 0, this.yy_buffer.length);
            this.yy_buffer = newBuffer;
        }
        final int numRead = this.yy_reader.read(this.yy_buffer, this.yy_endRead, this.yy_buffer.length - this.yy_endRead);
        if (numRead < 0) {
            return true;
        }
        this.yy_endRead += numRead;
        return false;
    }
    
    public final void yyclose() throws IOException {
        this.yy_atEOF = true;
        this.yy_endRead = this.yy_startRead;
        if (this.yy_reader != null) {
            this.yy_reader.close();
        }
    }
    
    public final void yyreset(final Reader reader) throws IOException {
        this.yyclose();
        if (this.yy_saved_buffer != null) {
            this.yy_buffer = this.yy_saved_buffer;
            this.yy_saved_buffer = null;
        }
        this.yy_reader = reader;
        this.yy_atBOL = true;
        this.yy_atEOF = false;
        final int n = 0;
        this.yy_startRead = n;
        this.yy_endRead = n;
        final int yy_currentPos = 0;
        this.yy_pushbackPos = yy_currentPos;
        this.yy_markedPos = yy_currentPos;
        this.yy_currentPos = yy_currentPos;
        final int yyline = 0;
        this.yycolumn = yyline;
        this.yychar = yyline;
        this.yyline = yyline;
        final int n2 = 0;
        this.yy_lexical_state = n2;
        this.yy_state = n2;
        this.yy_sawCR = false;
        final int n3 = 0;
        this.yycolumn_next = n3;
        this.yyline_next = n3;
    }
    
    public final void yyreset(final char[] buffer, final int off, final int len) throws IOException {
        this.yyclose();
        if (this.yy_saved_buffer == null) {
            this.yy_saved_buffer = this.yy_buffer;
        }
        this.yy_buffer = buffer;
        this.yy_reader = null;
        this.yy_atBOL = true;
        this.yy_atEOF = true;
        this.yy_startRead = off;
        this.yy_pushbackPos = off;
        this.yy_markedPos = off;
        this.yy_currentPos = off;
        this.yy_endRead = off + len;
        final int yyline = 0;
        this.yycolumn = yyline;
        this.yychar = yyline;
        this.yyline = yyline;
        final int n = 0;
        this.yy_lexical_state = n;
        this.yy_state = n;
        this.yy_sawCR = false;
        final int n2 = 0;
        this.yycolumn_next = n2;
        this.yyline_next = n2;
        this.yy_endRead_l = this.yy_endRead;
        this.yy_buffer_l = this.yy_buffer;
    }
    
    public final int yystate() {
        return this.yy_lexical_state;
    }
    
    public final void yybegin(final int newState) {
        this.yy_lexical_state = newState;
    }
    
    public final String yytext() {
        return new String(this.yy_buffer, this.yy_startRead, this.yy_markedPos - this.yy_startRead);
    }
    
    public final String yytext(final int offset, final int length) {
        return new String(this.yy_buffer, this.yy_startRead + offset, length);
    }
    
    public final void yynextAction() {
        this.yyline = this.yyline_next;
        this.yycolumn = this.yycolumn_next;
        final int yy_markedPos = this.yy_markedPos;
        this.yy_startRead = yy_markedPos;
        this.yy_currentPos = yy_markedPos;
    }
    
    public final int yynextChar() throws IOException {
        int yy_input;
        if (this.yy_markedPos < this.yy_endRead) {
            yy_input = this.yy_buffer[this.yy_markedPos++];
        }
        else {
            if (this.yy_atEOF) {
                return -1;
            }
            final boolean eof = this.yy_refill();
            this.yy_buffer_l = this.yy_buffer;
            this.yy_endRead_l = this.yy_endRead;
            if (eof) {
                return -1;
            }
            yy_input = this.yy_buffer[this.yy_markedPos++];
        }
        this.yy_doCount(yy_input);
        return yy_input;
    }
    
    public final int yynextBufferChar() throws IOException {
        final int yy_input = this.yy_buffer[this.yy_markedPos++];
        this.yy_doCount(yy_input);
        return yy_input;
    }
    
    private final int yy_doCount(final int yy_input) {
        switch (yy_input) {
            case 13: {
                ++this.yyline_next;
                this.yycolumn_next = 0;
                this.yy_sawCR = true;
                break;
            }
            case 10: {
                if (this.yy_sawCR) {
                    this.yy_sawCR = false;
                    break;
                }
                ++this.yyline_next;
                this.yycolumn_next = 0;
                break;
            }
            default: {
                this.yy_sawCR = false;
                ++this.yycolumn_next;
                break;
            }
        }
        return yy_input;
    }
    
    public final char yycharat(final int pos) {
        return this.yy_buffer[this.yy_startRead + pos];
    }
    
    public final int yybufferLeft() {
        return this.yy_endRead - this.yy_markedPos;
    }
    
    public final void yyskip(final int n) {
        this.yy_markedPos += n;
        this.yy_markedPos_l = this.yy_markedPos;
        if (this.yy_markedPos > this.yy_endRead) {
            this.yy_ScanError(4);
        }
    }
    
    public final int yylength() {
        return this.yy_markedPos - this.yy_startRead;
    }
    
    private void yy_ScanError(final int errorCode) {
        String message;
        try {
            message = XMLDeclParser.YY_ERROR_MSG[errorCode];
        }
        catch (ArrayIndexOutOfBoundsException e) {
            message = XMLDeclParser.YY_ERROR_MSG[0];
        }
        throw new Error(message);
    }
    
    private void yypushback(final int number) {
        if (number > this.yylength()) {
            this.yy_ScanError(3);
        }
        this.yy_markedPos -= number;
        this.yyline_next = this.yyline;
        this.yycolumn_next = this.yycolumn;
        this.yy_sawCR = this.yy_prev_sawCR;
        for (int pos = this.yy_startRead; pos < this.yy_markedPos; ++pos) {
            this.yy_doCount(this.yy_buffer[pos]);
        }
    }
    
    public int parse() throws IOException, FileFormatException {
        this.yy_endRead_l = this.yy_endRead;
        this.yy_buffer_l = this.yy_buffer;
        this.yycmap_l = XMLDeclParser.yycmap;
        final int[] yytrans_l = XMLDeclParser.yytrans;
        final int[] yy_rowMap_l = XMLDeclParser.yy_rowMap;
        final byte[] yy_attr_l = XMLDeclParser.YY_ATTRIBUTE;
        final int yy_pushbackPos = -1;
        this.yy_pushbackPos = yy_pushbackPos;
        int yy_pushbackPos_l = yy_pushbackPos;
        while (true) {
            this.yy_markedPos_l = this.yy_markedPos;
            this.yychar += this.yy_markedPos_l - this.yy_startRead;
            int yy_action = -1;
            final int yy_markedPos_l = this.yy_markedPos_l;
            this.yy_startRead = yy_markedPos_l;
            this.yy_currentPos = yy_markedPos_l;
            this.yy_currentPos_l = yy_markedPos_l;
            this.yy_startRead_l = yy_markedPos_l;
            this.yy_state = this.yy_lexical_state;
            boolean yy_was_pushback = false;
            int yy_input;
            while (true) {
                if (this.yy_currentPos_l < this.yy_endRead_l) {
                    yy_input = this.yy_buffer_l[this.yy_currentPos_l++];
                }
                else {
                    if (this.yy_atEOF) {
                        yy_input = -1;
                        break;
                    }
                    this.yy_currentPos = this.yy_currentPos_l;
                    this.yy_markedPos = this.yy_markedPos_l;
                    this.yy_pushbackPos = yy_pushbackPos_l;
                    final boolean eof = this.yy_refill();
                    this.yy_currentPos_l = this.yy_currentPos;
                    this.yy_markedPos_l = this.yy_markedPos;
                    this.yy_buffer_l = this.yy_buffer;
                    this.yy_endRead_l = this.yy_endRead;
                    yy_pushbackPos_l = this.yy_pushbackPos;
                    if (eof) {
                        yy_input = -1;
                        break;
                    }
                    yy_input = this.yy_buffer_l[this.yy_currentPos_l++];
                }
                final int yy_next = yytrans_l[yy_rowMap_l[this.yy_state] + this.yycmap_l[yy_input]];
                if (yy_next == -1) {
                    break;
                }
                this.yy_state = yy_next;
                final int yy_attributes = yy_attr_l[this.yy_state];
                if ((yy_attributes & 0x2) == 0x2) {
                    yy_pushbackPos_l = this.yy_currentPos_l;
                }
                if ((yy_attributes & 0x1) != 0x1) {
                    continue;
                }
                yy_was_pushback = ((yy_attributes & 0x4) == 0x4);
                yy_action = this.yy_state;
                this.yy_markedPos_l = this.yy_currentPos_l;
                if ((yy_attributes & 0x8) == 0x8) {
                    break;
                }
            }
            this.yy_markedPos = this.yy_markedPos_l;
            if (yy_was_pushback) {
                this.yy_markedPos = yy_pushbackPos_l;
            }
            switch (yy_action) {
                case 115: {
                    this.xmlEncoding = "UTF-16";
                    this.yybegin(5);
                }
                case 139: {
                    continue;
                }
                case 106: {
                    this.xmlEncoding = "UTF-8";
                    this.yybegin(5);
                }
                case 140: {
                    continue;
                }
                case 89: {
                    this.xmlVersion = "1.0";
                    this.yybegin(3);
                }
                case 141: {
                    continue;
                }
                case 101: {
                    this.yybegin(1);
                }
                case 142: {
                    continue;
                }
                case 10:
                case 11:
                case 12:
                case 13:
                case 14:
                case 15:
                case 16:
                case 17:
                case 18:
                case 19:
                case 20:
                case 21: {
                    throw new FileFormatException("XML Declaration not well-formed", -1, -1);
                }
                case 143: {
                    continue;
                }
                case 82: {
                    this.xmlStandalone = false;
                    this.yybegin(7);
                }
                case 144: {
                    continue;
                }
                case 100: {
                    this.xmlStandalone = true;
                    this.yybegin(7);
                }
                case 145: {
                    continue;
                }
                case 28: {
                    return 1;
                }
                case 146: {
                    continue;
                }
                case 8:
                case 9: {
                    return -1;
                }
                case 147: {
                    continue;
                }
                case 124: {
                    this.yybegin(2);
                }
                case 148: {
                    continue;
                }
                case 52: {
                    this.xmlVersion = this.yytext(1, this.yylength() - 2);
                    this.yybegin(3);
                }
                case 149: {
                    continue;
                }
                case 55: {
                    this.xmlEncoding = this.yytext(1, this.yylength() - 2);
                    this.yybegin(5);
                }
                case 150: {
                    continue;
                }
                case 129: {
                    this.yybegin(4);
                }
                case 151: {
                    continue;
                }
                case 137: {
                    this.xmlEncoding = "ISO-8859-1";
                    this.yybegin(5);
                }
                case 152: {
                    continue;
                }
                case 136: {
                    this.xmlStandaloneDeclared = true;
                    this.yybegin(6);
                }
                case 153: {
                    continue;
                }
                case 116: {
                    this.xmlEncoding = "US-ASCII";
                    this.yybegin(5);
                }
                case 154: {
                    continue;
                }
                default: {
                    if (yy_input == -1 && this.yy_startRead == this.yy_currentPos) {
                        this.yy_atEOF = true;
                        return -1;
                    }
                    this.yy_ScanError(2);
                    continue;
                }
            }
        }
    }
    
    static {
        yycmap = yy_unpack_cmap("\t\u0000\u0001\u0001\u0001\u0001\u0002\u0000\u0001\u0001\u0012\u0000\u0001\u0001\u0001\u0000\u0001\u0012\u0004\u0000\u0001\u0016\u0005\u0000\u0001 \u0001\u0014\u0001\u0000\u0001\u0015\u0001\u0013\u0003\u0005\u0001(\u0001&\u0001\u0005\u0001!\u0001)\u0001\u0003\u0001\u0000\u0001\u0006\u0001\u0002\u0001\u001c\u0001\u0007\u0001\u0000\u0001#\u0001\u0004\u0001$\u0002\u0004\u0001\u001f\u0002\u0004\u0001%\u0005\u0004\u0001'\u0003\u0004\u0001\"\u0001\u001e\u0001\u001d\u0005\u0004\u0004\u0000\u0001\u0005\u0001\u0000\u0001\u001b\u0001\u0004\u0001\u0017\u0001\u0018\u0001\f\u0001\u0004\u0001\u0019\u0001\u0004\u0001\u000f\u0002\u0004\u0001\n\u0001\t\u0001\u0011\u0001\u0010\u0002\u0004\u0001\r\u0001\u000e\u0001\u001a\u0001\u0004\u0001\u000b\u0001\u0004\u0001\b\u0001*\u0001\u0004\uff85\u0000");
        yy_rowMap = new int[] { 0, 43, 86, 129, 172, 215, 258, 301, 344, 387, 344, 430, 473, 516, 559, 602, 645, 688, 731, 774, 817, 860, 903, 430, 473, 946, 989, 1032, 344, 1075, 1118, 1161, 1204, 602, 1247, 1290, 1333, 1376, 1419, 1462, 1505, 1548, 731, 1591, 1634, 1677, 1720, 860, 1763, 1806, 1849, 1892, 344, 1935, 1978, 344, 2021, 2064, 2107, 2150, 2193, 2236, 2279, 2322, 2365, 2408, 2451, 2494, 2537, 2580, 2623, 2666, 2709, 2752, 2795, 2838, 2881, 2924, 2967, 3010, 3053, 3096, 344, 3139, 3182, 3225, 3268, 3311, 3354, 344, 3397, 3440, 3483, 3526, 3569, 3612, 3655, 3698, 3741, 3784, 344, 3225, 3827, 3870, 3913, 3956, 344, 3999, 4042, 4085, 4128, 4171, 4214, 4257, 4300, 344, 344, 4343, 4386, 4429, 4472, 4515, 4558, 4601, 4644, 4687, 4730, 4773, 4816, 4859, 4902, 4945, 4988, 5031, 5074, 5117, 5160, 344 };
        yytrans = yy_unpack();
        YY_ERROR_MSG = new String[] { "Unkown internal scanner error", "Internal error: unknown state", "Error: could not match input", "Error: pushback value was too large", "Error: skip value was too large" };
        YY_ATTRIBUTE = new byte[] { 0, 0, 0, 0, 0, 0, 0, 0, 9, 1, 9, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 9, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 9, 0, 0, 9, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 9, 0, 0, 2, 0, 0, 0, 9, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 9, 5, 0, 0, 0, 0, 9, 0, 0, 0, 0, 0, 0, 0, 0, 9, 9, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 9 };
    }
}
