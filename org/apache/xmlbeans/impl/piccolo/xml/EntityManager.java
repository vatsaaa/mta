// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.xmlbeans.impl.piccolo.xml;

import java.io.CharArrayReader;
import java.io.IOException;
import org.xml.sax.SAXException;
import java.io.InputStream;
import java.io.Reader;
import org.xml.sax.InputSource;
import org.apache.xmlbeans.impl.piccolo.util.RecursionException;
import java.net.URL;
import java.net.MalformedURLException;
import java.util.HashMap;
import java.util.Map;
import org.xml.sax.EntityResolver;

public class EntityManager
{
    public static final int GENERAL = 0;
    public static final int PARAMETER = 1;
    private EntityResolver resolver;
    private Map[] entityMaps;
    private Map entitiesByURI;
    
    public EntityManager() {
        this(null);
    }
    
    public EntityManager(final EntityResolver resolver) {
        this.entityMaps = new HashMap[] { new HashMap(), new HashMap() };
        this.entitiesByURI = new HashMap();
        this.setResolver(resolver);
    }
    
    public void setResolver(final EntityResolver resolver) {
        this.resolver = resolver;
    }
    
    public EntityResolver getResolver() {
        return this.resolver;
    }
    
    public boolean putInternal(final String name, final String value, final int type) {
        if (this.entityMaps[type].get(name) == null) {
            this.entityMaps[type].put(name, new Entry(value));
            return true;
        }
        return false;
    }
    
    public boolean putExternal(final Entity context, final String name, final String pubID, String sysID, final int type) throws MalformedURLException {
        if (this.entityMaps[type].get(name) == null) {
            sysID = resolveSystemID(context.getSystemID(), sysID);
            final Entry e = new Entry(pubID, sysID);
            this.entityMaps[type].put(name, e);
            if (pubID != null && pubID.length() > 0) {
                this.entitiesByURI.put(pubID, e);
            }
            this.entitiesByURI.put(sysID, e);
            return true;
        }
        return false;
    }
    
    public boolean putUnparsed(final Entity context, final String name, final String pubID, final String sysID, final String ndata, final int type) throws MalformedURLException {
        if (this.entityMaps[type].get(name) == null) {
            this.entityMaps[type].put(name, new Entry(pubID, sysID, ndata));
            return true;
        }
        return false;
    }
    
    public void clear() {
        this.entityMaps[0].clear();
        this.entityMaps[1].clear();
        this.entitiesByURI.clear();
    }
    
    public Entity getByName(final String name, final int type) {
        return this.entityMaps[type].get(name);
    }
    
    public Entity getByID(final Entity context, final String pubID, String sysID) throws MalformedURLException {
        Entity result = null;
        sysID = resolveSystemID(context.getSystemID(), sysID);
        result = this.entitiesByURI.get(sysID);
        if (result != null) {
            return result;
        }
        if (pubID != null && pubID.length() > 0) {
            result = this.entitiesByURI.get(pubID);
            if (result != null) {
                return result;
            }
        }
        result = new Entry(pubID, sysID);
        if (pubID != null && pubID.length() > 0) {
            this.entitiesByURI.put(pubID, result);
        }
        this.entitiesByURI.put(sysID, result);
        return result;
    }
    
    public static String resolveSystemID(final String contextSysID, final String sysID) throws MalformedURLException {
        URL url;
        if (contextSysID != null) {
            url = new URL(new URL(contextSysID), sysID);
        }
        else {
            url = new URL(sysID);
        }
        return url.toString();
    }
    
    private final class Entry implements Entity
    {
        boolean isOpen;
        char[] value;
        String pubID;
        String sysID;
        String ndata;
        XMLInputReader reader;
        boolean isStandalone;
        
        Entry(final String value) {
            this.isOpen = false;
            this.reader = null;
            this.isStandalone = false;
            final String pubID = null;
            this.ndata = pubID;
            this.sysID = pubID;
            this.pubID = pubID;
            this.value = value.toCharArray();
        }
        
        Entry(final EntityManager this$0, final String pubID, final String sysID) {
            this(this$0, pubID, sysID, null);
        }
        
        Entry(final String pubID, final String sysID, final String ndata) {
            this.isOpen = false;
            this.reader = null;
            this.isStandalone = false;
            this.pubID = pubID;
            this.sysID = sysID;
            this.ndata = ndata;
        }
        
        public void open() throws RecursionException, SAXException, IOException {
            if (this.ndata != null) {
                throw new FatalParsingException("Cannot reference entity; unknown NDATA type '" + this.ndata + "'");
            }
            if (this.isOpen) {
                throw new RecursionException();
            }
            if (!this.isInternal()) {
                if (EntityManager.this.resolver == null) {
                    this.reader = new XMLStreamReader(new URL(this.sysID).openStream(), true);
                }
                else {
                    final InputSource source = EntityManager.this.resolver.resolveEntity(this.pubID, this.sysID);
                    if (source == null) {
                        this.reader = new XMLStreamReader(new URL(this.sysID).openStream(), true);
                    }
                    else {
                        final Reader r = source.getCharacterStream();
                        if (r != null) {
                            this.reader = new XMLReaderReader(r, true);
                        }
                        else {
                            final InputStream in = source.getByteStream();
                            if (in != null) {
                                this.reader = new XMLStreamReader(in, source.getEncoding(), true);
                            }
                            else {
                                this.reader = new XMLStreamReader(new URL(source.getSystemId()).openStream(), source.getEncoding(), true);
                            }
                        }
                    }
                }
                this.isStandalone = this.reader.isXMLStandalone();
            }
            this.isOpen = true;
        }
        
        public boolean isOpen() {
            return this.isOpen;
        }
        
        public void close() {
            this.isOpen = false;
            this.reader = null;
        }
        
        public String getSystemID() {
            return this.sysID;
        }
        
        public String getPublicID() {
            return this.pubID;
        }
        
        public boolean isStandalone() {
            return this.isStandalone;
        }
        
        public void setStandalone(final boolean standalone) {
            this.isStandalone = standalone;
        }
        
        public boolean isInternal() {
            return this.sysID == null;
        }
        
        public boolean isParsed() {
            return this.ndata == null;
        }
        
        public String getDeclaredEncoding() {
            if (this.reader != null) {
                return this.reader.getXMLDeclaredEncoding();
            }
            return null;
        }
        
        public boolean isStandaloneDeclared() {
            return this.reader != null && this.reader.isXMLStandaloneDeclared();
        }
        
        public String getXMLVersion() {
            if (this.reader != null) {
                return this.reader.getXMLVersion();
            }
            return null;
        }
        
        public Reader getReader() {
            if (this.isInternal()) {
                return new CharArrayReader(this.value);
            }
            return this.reader;
        }
        
        public String stringValue() {
            return new String(this.value);
        }
        
        public char[] charArrayValue() {
            return this.value;
        }
    }
}
