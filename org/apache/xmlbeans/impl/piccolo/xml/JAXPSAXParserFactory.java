// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.xmlbeans.impl.piccolo.xml;

import org.xml.sax.XMLReader;
import org.xml.sax.Parser;
import java.util.Iterator;
import java.util.Enumeration;
import org.apache.xmlbeans.impl.piccolo.util.FactoryServiceFinder;
import java.io.InputStream;
import java.io.FileInputStream;
import java.util.Properties;
import java.io.File;
import org.xml.sax.SAXException;
import javax.xml.parsers.SAXParser;
import org.xml.sax.SAXNotSupportedException;
import org.xml.sax.SAXNotRecognizedException;
import java.util.HashMap;
import javax.xml.parsers.ParserConfigurationException;
import java.util.Map;
import javax.xml.parsers.SAXParserFactory;

public class JAXPSAXParserFactory extends SAXParserFactory
{
    private Map featureMap;
    private static Boolean TRUE;
    private static Boolean FALSE;
    private Piccolo nvParser;
    private SAXParserFactory validatingFactory;
    private static final String VALIDATING_PROPERTY = "org.apache.xmlbeans.impl.piccolo.xml.ValidatingSAXParserFactory";
    private static Class validatingFactoryClass;
    private ParserConfigurationException pendingValidatingException;
    private ParserConfigurationException pendingNonvalidatingException;
    private boolean validating;
    private boolean namespaceAware;
    
    public static SAXParserFactory newInstance() {
        return new JAXPSAXParserFactory();
    }
    
    public JAXPSAXParserFactory() {
        this.featureMap = new HashMap();
        this.nvParser = new Piccolo();
        this.pendingValidatingException = null;
        this.pendingNonvalidatingException = null;
        this.validating = false;
        this.namespaceAware = false;
        try {
            if (JAXPSAXParserFactory.validatingFactoryClass != null) {
                (this.validatingFactory = JAXPSAXParserFactory.validatingFactoryClass.newInstance()).setNamespaceAware(false);
                this.validatingFactory.setValidating(true);
            }
        }
        catch (Exception e) {
            this.validatingFactory = null;
        }
        this.setNamespaceAware(false);
    }
    
    public boolean getFeature(final String name) throws ParserConfigurationException, SAXNotRecognizedException, SAXNotSupportedException {
        if (this.validating && this.validatingFactory != null) {
            return this.validatingFactory.getFeature(name);
        }
        return this.nvParser.getFeature(name);
    }
    
    public SAXParser newSAXParser() throws ParserConfigurationException, SAXException {
        if (this.validating) {
            if (this.validatingFactory == null) {
                throw new ParserConfigurationException("XML document validation is not supported");
            }
            if (this.pendingValidatingException != null) {
                throw this.pendingValidatingException;
            }
            return this.validatingFactory.newSAXParser();
        }
        else {
            if (this.pendingNonvalidatingException != null) {
                throw this.pendingNonvalidatingException;
            }
            return new JAXPSAXParser(new Piccolo(this.nvParser));
        }
    }
    
    public void setFeature(final String name, final boolean enabled) throws ParserConfigurationException, SAXNotRecognizedException, SAXNotSupportedException {
        this.featureMap.put(name, enabled ? JAXPSAXParserFactory.TRUE : JAXPSAXParserFactory.FALSE);
        if (this.validatingFactory != null) {
            if (this.pendingValidatingException != null) {
                this.reconfigureValidating();
            }
            else {
                try {
                    this.validatingFactory.setFeature(name, enabled);
                }
                catch (ParserConfigurationException e) {
                    this.pendingValidatingException = e;
                }
            }
        }
        if (this.pendingNonvalidatingException != null) {
            this.reconfigureNonvalidating();
        }
        if (this.validating && this.pendingValidatingException != null) {
            throw this.pendingValidatingException;
        }
        if (!this.validating && this.pendingNonvalidatingException != null) {
            throw this.pendingNonvalidatingException;
        }
    }
    
    public void setNamespaceAware(final boolean awareness) {
        super.setNamespaceAware(awareness);
        this.namespaceAware = awareness;
        try {
            this.nvParser.setFeature("http://xml.org/sax/features/namespaces", awareness);
            this.nvParser.setFeature("http://xml.org/sax/features/namespace-prefixes", !awareness);
        }
        catch (SAXNotSupportedException e) {
            this.pendingNonvalidatingException = new ParserConfigurationException("Error setting namespace feature: " + e.toString());
        }
        catch (SAXNotRecognizedException e2) {
            this.pendingNonvalidatingException = new ParserConfigurationException("Error setting namespace feature: " + e2.toString());
        }
        if (this.validatingFactory != null) {
            this.validatingFactory.setNamespaceAware(awareness);
        }
    }
    
    public void setValidating(final boolean value) {
        super.setValidating(value);
        this.validating = value;
    }
    
    private static Class findValidatingFactory() {
        try {
            final String validatingClassName = System.getProperty("org.apache.xmlbeans.impl.piccolo.xml.ValidatingSAXParserFactory");
            if (validatingClassName != null) {
                return Class.forName(validatingClassName);
            }
        }
        catch (Exception ex) {}
        try {
            final String javah = System.getProperty("java.home");
            final String configFile = javah + File.separator + "lib" + File.separator + "jaxp.properties";
            final File f = new File(configFile);
            if (f.exists()) {
                final Properties props = new Properties();
                props.load(new FileInputStream(f));
                final String validatingClassName2 = props.getProperty("org.apache.xmlbeans.impl.piccolo.xml.ValidatingSAXParserFactory");
                if (validatingClassName2 != null) {
                    return Class.forName(validatingClassName2);
                }
            }
        }
        catch (Exception ex2) {}
        try {
            final Enumeration enumValue = FactoryServiceFinder.findServices("javax.xml.parsers.SAXParserFactory");
            while (enumValue.hasMoreElements()) {
                try {
                    final String factory = enumValue.nextElement();
                    if (!factory.equals("org.apache.xmlbeans.impl.piccolo.xml.Piccolo")) {
                        return Class.forName(factory);
                    }
                    continue;
                }
                catch (ClassNotFoundException e) {}
            }
        }
        catch (Exception ex3) {}
        try {
            return Class.forName("org.apache.crimson.jaxp.SAXParserFactoryImpl");
        }
        catch (ClassNotFoundException e2) {
            return null;
        }
    }
    
    private void reconfigureValidating() {
        if (this.validatingFactory == null) {
            return;
        }
        try {
            for (final Map.Entry entry : this.featureMap.entrySet()) {
                this.validatingFactory.setFeature(entry.getKey(), entry.getValue());
            }
        }
        catch (ParserConfigurationException e) {
            this.pendingValidatingException = e;
        }
        catch (SAXNotRecognizedException e2) {
            this.pendingValidatingException = new ParserConfigurationException(e2.toString());
        }
        catch (SAXNotSupportedException e3) {
            this.pendingValidatingException = new ParserConfigurationException(e3.toString());
        }
    }
    
    private void reconfigureNonvalidating() {
        try {
            for (final Map.Entry entry : this.featureMap.entrySet()) {
                this.nvParser.setFeature(entry.getKey(), entry.getValue());
            }
        }
        catch (SAXNotRecognizedException e) {
            this.pendingNonvalidatingException = new ParserConfigurationException(e.toString());
        }
        catch (SAXNotSupportedException e2) {
            this.pendingNonvalidatingException = new ParserConfigurationException(e2.toString());
        }
    }
    
    static {
        JAXPSAXParserFactory.TRUE = new Boolean(true);
        JAXPSAXParserFactory.FALSE = new Boolean(false);
        JAXPSAXParserFactory.validatingFactoryClass = findValidatingFactory();
    }
    
    static class JAXPSAXParser extends SAXParser
    {
        Piccolo parser;
        
        JAXPSAXParser(final Piccolo parser) {
            this.parser = parser;
        }
        
        public Parser getParser() {
            return this.parser;
        }
        
        public Object getProperty(final String name) throws SAXNotRecognizedException, SAXNotSupportedException {
            return this.parser.getProperty(name);
        }
        
        public XMLReader getXMLReader() {
            return this.parser;
        }
        
        public boolean isNamespaceAware() {
            return this.parser.fNamespaces;
        }
        
        public boolean isValidating() {
            return false;
        }
        
        public void setProperty(final String name, final Object value) throws SAXNotRecognizedException, SAXNotSupportedException {
            this.parser.setProperty(name, value);
        }
    }
}
