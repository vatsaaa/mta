// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.xmlbeans.impl.piccolo.xml;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;

public class XMLDecoderFactory
{
    private static HashMap decoders;
    
    public static XMLDecoder createDecoder(final String encoding) throws UnsupportedEncodingException {
        final XMLDecoder d = XMLDecoderFactory.decoders.get(encoding.toUpperCase());
        if (d != null) {
            return d.newXMLDecoder();
        }
        throw new UnsupportedEncodingException("Encoding '" + encoding + "' not supported");
    }
    
    static {
        XMLDecoderFactory.decoders = new HashMap();
        final UTF8XMLDecoder utf8 = new UTF8XMLDecoder();
        final ASCIIXMLDecoder ascii = new ASCIIXMLDecoder();
        final ISO8859_1XMLDecoder iso8859 = new ISO8859_1XMLDecoder();
        final UnicodeBigXMLDecoder utf16be = new UnicodeBigXMLDecoder();
        final UnicodeLittleXMLDecoder utf16le = new UnicodeLittleXMLDecoder();
        XMLDecoderFactory.decoders.put("UTF-8", utf8);
        XMLDecoderFactory.decoders.put("UTF8", utf8);
        XMLDecoderFactory.decoders.put("US-ASCII", ascii);
        XMLDecoderFactory.decoders.put("ASCII", ascii);
        XMLDecoderFactory.decoders.put("ISO-8859-1", iso8859);
        XMLDecoderFactory.decoders.put("ISO8859_1", iso8859);
        XMLDecoderFactory.decoders.put("UTF-16LE", utf16le);
        XMLDecoderFactory.decoders.put("UNICODELITTLE", utf16le);
        XMLDecoderFactory.decoders.put("UNICODELITTLEUNMARKED", utf16le);
        XMLDecoderFactory.decoders.put("UTF-16BE", utf16be);
        XMLDecoderFactory.decoders.put("UTF-16", utf16be);
        XMLDecoderFactory.decoders.put("UNICODEBIG", utf16be);
        XMLDecoderFactory.decoders.put("UNICODEBIGUNMARKED", utf16be);
    }
}
