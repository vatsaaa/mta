// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.xmlbeans.impl.piccolo.xml;

import java.io.CharConversionException;
import org.apache.xmlbeans.impl.piccolo.io.IllegalCharException;
import org.apache.xmlbeans.impl.piccolo.io.CharsetDecoder;

public final class UnicodeBigXMLDecoder implements XMLDecoder
{
    private boolean sawCR;
    
    public UnicodeBigXMLDecoder() {
        this.sawCR = false;
    }
    
    public CharsetDecoder newCharsetDecoder() {
        return this.newXMLDecoder();
    }
    
    public XMLDecoder newXMLDecoder() {
        return new UnicodeBigXMLDecoder();
    }
    
    public int minBytesPerChar() {
        return 2;
    }
    
    public int maxBytesPerChar() {
        return 2;
    }
    
    public void reset() {
        this.sawCR = false;
    }
    
    public void decode(final byte[] in_buf, final int in_off, final int in_len, final char[] out_buf, final int out_off, final int out_len, final int[] result) throws CharConversionException {
        int i;
        int o;
        for (o = (i = 0); i + 1 < in_len && o < out_len; i += 2) {
            final char c = (char)((0xFF & in_buf[in_off + i]) << 8 | (0xFF & in_buf[in_off + i + 1]));
            if (c >= ' ') {
                if (c > '\ud7ff' && (c < '\ue000' || c > '\ufffd') && (c < 65536 || c > 1114111)) {
                    throw new IllegalCharException("Illegal XML Character: 0x" + Integer.toHexString(c));
                }
                this.sawCR = false;
                out_buf[out_off + o++] = c;
            }
            else {
                switch (c) {
                    case '\n': {
                        if (this.sawCR) {
                            this.sawCR = false;
                            break;
                        }
                        out_buf[out_off + o++] = '\n';
                        break;
                    }
                    case '\r': {
                        this.sawCR = true;
                        out_buf[out_off + o++] = '\n';
                        break;
                    }
                    case '\t': {
                        out_buf[out_off + o++] = '\t';
                        break;
                    }
                    default: {
                        throw new IllegalCharException("Illegal XML character: 0x" + Integer.toHexString(c));
                    }
                }
            }
        }
        result[0] = i;
        result[1] = o;
    }
    
    public void decodeXMLDecl(final byte[] in_buf, final int in_off, final int in_len, final char[] out_buf, final int out_off, final int out_len, final int[] result) throws CharConversionException {
        int i = 0;
        int o = 0;
    Label_0237:
        for (o = (i = 0); i + 1 < in_len && o < out_len; i += 2) {
            final char c = (char)((0xFF & in_buf[in_off + i]) << 8 | (0xFF & in_buf[in_off + i + 1]));
            if (c >= ' ') {
                if (c > '\ud7ff' && (c < '\ue000' || c > '\ufffd') && (c < 65536 || c > 1114111)) {
                    break;
                }
                this.sawCR = false;
                if ((out_buf[out_off + o++] = c) == '>') {
                    i += 2;
                    break;
                }
            }
            else {
                switch (c) {
                    case '\n': {
                        if (this.sawCR) {
                            this.sawCR = false;
                            break;
                        }
                        out_buf[out_off + o++] = '\n';
                        break;
                    }
                    case '\r': {
                        this.sawCR = true;
                        out_buf[out_off + o++] = '\n';
                        break;
                    }
                    case '\t': {
                        out_buf[out_off + o++] = '\t';
                        break;
                    }
                    default: {
                        break Label_0237;
                    }
                }
            }
        }
        result[0] = i;
        result[1] = o;
    }
}
