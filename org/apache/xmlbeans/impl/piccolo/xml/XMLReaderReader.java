// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.xmlbeans.impl.piccolo.xml;

import org.apache.xmlbeans.impl.piccolo.io.IllegalCharException;
import java.io.IOException;
import java.io.Reader;

public final class XMLReaderReader extends XMLInputReader
{
    private static final int BUFFER_SIZE = 8192;
    private Reader in;
    private boolean rewindDeclaration;
    private char[] cbuf;
    private int cbufPos;
    private int cbufEnd;
    private boolean eofReached;
    private boolean sawCR;
    private char[] oneCharBuf;
    
    public XMLReaderReader() {
        this.cbuf = new char[8192];
        this.cbufPos = 0;
        this.cbufEnd = 0;
        this.eofReached = false;
        this.sawCR = false;
        this.oneCharBuf = new char[1];
    }
    
    public XMLReaderReader(final Reader in) throws IOException {
        this(in, true);
    }
    
    public XMLReaderReader(final Reader in, final boolean rewindDeclaration) throws IOException {
        this.cbuf = new char[8192];
        this.cbufPos = 0;
        this.cbufEnd = 0;
        this.eofReached = false;
        this.sawCR = false;
        this.oneCharBuf = new char[1];
        this.reset(in, rewindDeclaration);
    }
    
    public void reset(final Reader in, final boolean rewindDeclaration) throws IOException {
        super.resetInput();
        this.in = in;
        this.rewindDeclaration = rewindDeclaration;
        final int n = 0;
        this.cbufEnd = n;
        this.cbufPos = n;
        this.sawCR = false;
        this.eofReached = false;
        this.fillCharBuffer();
        this.processXMLDecl();
    }
    
    public void close() throws IOException {
        this.eofReached = true;
        final int n = 0;
        this.cbufEnd = n;
        this.cbufPos = n;
        if (this.in != null) {
            this.in.close();
        }
    }
    
    public void mark(final int readAheadLimit) throws IOException {
        throw new UnsupportedOperationException("mark() not supported");
    }
    
    public boolean markSupported() {
        return false;
    }
    
    public int read() throws IOException {
        final int n = this.read(this.oneCharBuf, 0, 1);
        if (n <= 0) {
            return n;
        }
        return this.oneCharBuf[0];
    }
    
    public int read(final char[] destbuf) throws IOException {
        return this.read(destbuf, 0, destbuf.length);
    }
    
    public int read(final char[] destbuf, final int off, final int len) throws IOException {
        int charsRead = 0;
        while (charsRead < len) {
            if (this.cbufPos < this.cbufEnd) {
                final char c = this.cbuf[this.cbufPos++];
                if (c >= ' ') {
                    if (c > '\ud7ff' && (c < '\ue000' || c > '\ufffd') && (c < 65536 || c > 1114111)) {
                        throw new IllegalCharException("Illegal XML Character: 0x" + Integer.toHexString(c));
                    }
                    this.sawCR = false;
                    destbuf[off + charsRead++] = c;
                }
                else {
                    switch (c) {
                        case '\n': {
                            if (this.sawCR) {
                                this.sawCR = false;
                                continue;
                            }
                            destbuf[off + charsRead++] = '\n';
                            continue;
                        }
                        case '\r': {
                            this.sawCR = true;
                            destbuf[off + charsRead++] = '\n';
                            continue;
                        }
                        case '\t': {
                            destbuf[off + charsRead++] = '\t';
                            continue;
                        }
                        default: {
                            throw new IllegalCharException("Illegal XML character: 0x" + Integer.toHexString(c));
                        }
                    }
                }
            }
            else {
                if (this.eofReached) {
                    break;
                }
                if (charsRead != 0 && !this.in.ready()) {
                    break;
                }
                this.fillCharBuffer();
            }
        }
        return (charsRead == 0 && this.eofReached) ? -1 : charsRead;
    }
    
    public boolean ready() throws IOException {
        return this.cbufEnd - this.cbufPos > 0 || this.in.ready();
    }
    
    public void reset() throws IOException {
        super.resetInput();
        this.in.reset();
        final int n = 0;
        this.cbufEnd = n;
        this.cbufPos = n;
        this.sawCR = false;
        this.eofReached = false;
    }
    
    public long skip(final long n) throws IOException {
        int charsRead = 0;
        while (charsRead < n) {
            if (this.cbufPos < this.cbufEnd) {
                final char c = this.cbuf[this.cbufPos++];
                if (c >= ' ') {
                    if (c > '\ud7ff' && (c < '\ue000' || c > '\ufffd') && (c < 65536 || c > 1114111)) {
                        throw new IllegalCharException("Illegal XML Character: 0x" + Integer.toHexString(c));
                    }
                    this.sawCR = false;
                    ++charsRead;
                }
                else {
                    switch (c) {
                        case '\n': {
                            if (this.sawCR) {
                                this.sawCR = false;
                                continue;
                            }
                            ++charsRead;
                            continue;
                        }
                        case '\r': {
                            this.sawCR = true;
                            ++charsRead;
                            continue;
                        }
                        case '\t': {
                            ++charsRead;
                            continue;
                        }
                        default: {
                            throw new IllegalCharException("Illegal XML character: 0x" + Integer.toHexString(c));
                        }
                    }
                }
            }
            else {
                if (this.eofReached) {
                    break;
                }
                this.fillCharBuffer();
            }
        }
        return (charsRead == 0 && this.eofReached) ? -1 : charsRead;
    }
    
    private void fillCharBuffer() throws IOException {
        this.cbufPos = 0;
        this.cbufEnd = this.in.read(this.cbuf, 0, 8192);
        if (this.cbufEnd <= 0) {
            this.eofReached = true;
        }
    }
    
    private void processXMLDecl() throws IOException {
        final int numCharsParsed = this.parseXMLDeclaration(this.cbuf, 0, this.cbufEnd);
        if (numCharsParsed > 0 && !this.rewindDeclaration) {
            this.cbufPos += numCharsParsed;
        }
    }
}
