// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.xmlbeans.impl.piccolo.xml;

import java.io.CharConversionException;
import org.apache.xmlbeans.impl.piccolo.io.CharsetDecoder;

public interface XMLDecoder extends CharsetDecoder
{
    void decodeXMLDecl(final byte[] p0, final int p1, final int p2, final char[] p3, final int p4, final int p5, final int[] p6) throws CharConversionException;
    
    XMLDecoder newXMLDecoder();
}
