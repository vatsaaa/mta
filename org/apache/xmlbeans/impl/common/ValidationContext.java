// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.xmlbeans.impl.common;

public interface ValidationContext
{
    void invalid(final String p0);
    
    void invalid(final String p0, final Object[] p1);
}
