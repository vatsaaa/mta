// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.xmlbeans.impl.common;

public interface XmlLocale
{
    boolean sync();
    
    boolean noSync();
    
    void enter();
    
    void exit();
}
