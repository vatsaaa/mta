// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.xmlbeans.impl.common;

public interface PrefixResolver
{
    String getNamespaceForPrefix(final String p0);
}
