// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.xmlbeans.impl.soap;

public class MimeHeader
{
    private String name;
    private String value;
    
    public MimeHeader(final String name, final String value) {
        this.name = name;
        this.value = value;
    }
    
    public String getName() {
        return this.name;
    }
    
    public String getValue() {
        return this.value;
    }
}
