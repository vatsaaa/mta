// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.xmlbeans.xml.stream;

public interface BufferedXMLInputStream extends XMLInputStream
{
    void mark() throws XMLStreamException;
    
    void reset() throws XMLStreamException;
}
