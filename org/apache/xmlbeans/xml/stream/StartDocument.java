// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.xmlbeans.xml.stream;

public interface StartDocument extends XMLEvent
{
    String getSystemId();
    
    String getCharacterEncodingScheme();
    
    boolean isStandalone();
    
    String getVersion();
}
