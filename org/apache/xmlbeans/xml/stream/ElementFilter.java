// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.xmlbeans.xml.stream;

public interface ElementFilter
{
    boolean accept(final XMLEvent p0);
}
