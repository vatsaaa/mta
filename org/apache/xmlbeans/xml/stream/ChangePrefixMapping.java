// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.xmlbeans.xml.stream;

public interface ChangePrefixMapping extends XMLEvent
{
    String getOldNamespaceUri();
    
    String getNewNamespaceUri();
    
    String getPrefix();
}
