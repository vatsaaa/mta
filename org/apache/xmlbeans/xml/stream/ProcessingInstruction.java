// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.xmlbeans.xml.stream;

public interface ProcessingInstruction extends XMLEvent
{
    String getTarget();
    
    String getData();
}
