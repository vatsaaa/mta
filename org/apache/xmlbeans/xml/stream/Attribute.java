// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.xmlbeans.xml.stream;

public interface Attribute
{
    XMLName getName();
    
    String getValue();
    
    String getType();
    
    XMLName getSchemaType();
}
