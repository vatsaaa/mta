// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.xmlbeans.xml.stream;

public interface CharacterData extends XMLEvent
{
    String getContent();
    
    boolean hasContent();
}
