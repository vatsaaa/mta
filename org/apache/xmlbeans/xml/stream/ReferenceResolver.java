// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.xmlbeans.xml.stream;

public interface ReferenceResolver
{
    XMLInputStream resolve(final String p0) throws XMLStreamException;
    
    String getId(final String p0);
}
