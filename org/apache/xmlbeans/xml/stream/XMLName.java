// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.xmlbeans.xml.stream;

public interface XMLName
{
    String getNamespaceUri();
    
    String getLocalName();
    
    String getPrefix();
    
    String getQualifiedName();
}
