// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.xmlbeans;

import java.util.List;
import javax.xml.namespace.QName;
import java.util.Date;
import java.util.Calendar;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.io.Writer;
import java.io.OutputStream;
import java.io.IOException;
import java.io.File;
import org.xml.sax.SAXException;
import org.xml.sax.ext.LexicalHandler;
import org.xml.sax.ContentHandler;
import org.w3c.dom.Node;
import java.io.Reader;
import java.io.InputStream;
import javax.xml.stream.XMLStreamReader;
import org.apache.xmlbeans.xml.stream.XMLInputStream;

public abstract class FilterXmlObject implements XmlObject, SimpleValue, DelegateXmlObject
{
    public SchemaType schemaType() {
        return this.underlyingXmlObject().schemaType();
    }
    
    public boolean validate() {
        return this.underlyingXmlObject().validate();
    }
    
    public boolean validate(final XmlOptions options) {
        return this.underlyingXmlObject().validate(options);
    }
    
    public XmlObject[] selectPath(final String path) {
        return this.underlyingXmlObject().selectPath(path);
    }
    
    public XmlObject[] selectPath(final String path, final XmlOptions options) {
        return this.underlyingXmlObject().selectPath(path, options);
    }
    
    public XmlObject[] execQuery(final String query) {
        return this.underlyingXmlObject().execQuery(query);
    }
    
    public XmlObject[] execQuery(final String query, final XmlOptions options) {
        return this.underlyingXmlObject().execQuery(query, options);
    }
    
    public XmlObject changeType(final SchemaType newType) {
        return this.underlyingXmlObject().changeType(newType);
    }
    
    public boolean isNil() {
        return this.underlyingXmlObject().isNil();
    }
    
    public void setNil() {
        this.underlyingXmlObject().setNil();
    }
    
    public boolean isImmutable() {
        return this.underlyingXmlObject().isImmutable();
    }
    
    public XmlObject set(final XmlObject srcObj) {
        return this.underlyingXmlObject().set(srcObj);
    }
    
    public XmlObject copy() {
        return this.underlyingXmlObject().copy();
    }
    
    public boolean valueEquals(final XmlObject obj) {
        return this.underlyingXmlObject().valueEquals(obj);
    }
    
    public int valueHashCode() {
        return this.underlyingXmlObject().valueHashCode();
    }
    
    public int compareTo(final Object obj) {
        return this.underlyingXmlObject().compareTo(obj);
    }
    
    public int compareValue(final XmlObject obj) {
        return this.underlyingXmlObject().compareValue(obj);
    }
    
    public Object monitor() {
        return this.underlyingXmlObject().monitor();
    }
    
    public XmlDocumentProperties documentProperties() {
        return this.underlyingXmlObject().documentProperties();
    }
    
    public XmlCursor newCursor() {
        return this.underlyingXmlObject().newCursor();
    }
    
    public XMLInputStream newXMLInputStream() {
        return this.underlyingXmlObject().newXMLInputStream();
    }
    
    public XMLStreamReader newXMLStreamReader() {
        return this.underlyingXmlObject().newXMLStreamReader();
    }
    
    public String xmlText() {
        return this.underlyingXmlObject().xmlText();
    }
    
    public InputStream newInputStream() {
        return this.underlyingXmlObject().newInputStream();
    }
    
    public Reader newReader() {
        return this.underlyingXmlObject().newReader();
    }
    
    public Node newDomNode() {
        return this.underlyingXmlObject().newDomNode();
    }
    
    public Node getDomNode() {
        return this.underlyingXmlObject().getDomNode();
    }
    
    public void save(final ContentHandler ch, final LexicalHandler lh) throws SAXException {
        this.underlyingXmlObject().save(ch, lh);
    }
    
    public void save(final File file) throws IOException {
        this.underlyingXmlObject().save(file);
    }
    
    public void save(final OutputStream os) throws IOException {
        this.underlyingXmlObject().save(os);
    }
    
    public void save(final Writer w) throws IOException {
        this.underlyingXmlObject().save(w);
    }
    
    public XMLInputStream newXMLInputStream(final XmlOptions options) {
        return this.underlyingXmlObject().newXMLInputStream(options);
    }
    
    public XMLStreamReader newXMLStreamReader(final XmlOptions options) {
        return this.underlyingXmlObject().newXMLStreamReader(options);
    }
    
    public String xmlText(final XmlOptions options) {
        return this.underlyingXmlObject().xmlText(options);
    }
    
    public InputStream newInputStream(final XmlOptions options) {
        return this.underlyingXmlObject().newInputStream(options);
    }
    
    public Reader newReader(final XmlOptions options) {
        return this.underlyingXmlObject().newReader(options);
    }
    
    public Node newDomNode(final XmlOptions options) {
        return this.underlyingXmlObject().newDomNode(options);
    }
    
    public void save(final ContentHandler ch, final LexicalHandler lh, final XmlOptions options) throws SAXException {
        this.underlyingXmlObject().save(ch, lh, options);
    }
    
    public void save(final File file, final XmlOptions options) throws IOException {
        this.underlyingXmlObject().save(file, options);
    }
    
    public void save(final OutputStream os, final XmlOptions options) throws IOException {
        this.underlyingXmlObject().save(os, options);
    }
    
    public void save(final Writer w, final XmlOptions options) throws IOException {
        this.underlyingXmlObject().save(w, options);
    }
    
    public SchemaType instanceType() {
        return ((SimpleValue)this.underlyingXmlObject()).instanceType();
    }
    
    public String stringValue() {
        return ((SimpleValue)this.underlyingXmlObject()).stringValue();
    }
    
    public boolean booleanValue() {
        return ((SimpleValue)this.underlyingXmlObject()).booleanValue();
    }
    
    public byte byteValue() {
        return ((SimpleValue)this.underlyingXmlObject()).byteValue();
    }
    
    public short shortValue() {
        return ((SimpleValue)this.underlyingXmlObject()).shortValue();
    }
    
    public int intValue() {
        return ((SimpleValue)this.underlyingXmlObject()).intValue();
    }
    
    public long longValue() {
        return ((SimpleValue)this.underlyingXmlObject()).longValue();
    }
    
    public BigInteger bigIntegerValue() {
        return ((SimpleValue)this.underlyingXmlObject()).bigIntegerValue();
    }
    
    public BigDecimal bigDecimalValue() {
        return ((SimpleValue)this.underlyingXmlObject()).bigDecimalValue();
    }
    
    public float floatValue() {
        return ((SimpleValue)this.underlyingXmlObject()).floatValue();
    }
    
    public double doubleValue() {
        return ((SimpleValue)this.underlyingXmlObject()).doubleValue();
    }
    
    public byte[] byteArrayValue() {
        return ((SimpleValue)this.underlyingXmlObject()).byteArrayValue();
    }
    
    public StringEnumAbstractBase enumValue() {
        return ((SimpleValue)this.underlyingXmlObject()).enumValue();
    }
    
    public Calendar calendarValue() {
        return ((SimpleValue)this.underlyingXmlObject()).calendarValue();
    }
    
    public Date dateValue() {
        return ((SimpleValue)this.underlyingXmlObject()).dateValue();
    }
    
    public GDate gDateValue() {
        return ((SimpleValue)this.underlyingXmlObject()).gDateValue();
    }
    
    public GDuration gDurationValue() {
        return ((SimpleValue)this.underlyingXmlObject()).gDurationValue();
    }
    
    public QName qNameValue() {
        return ((SimpleValue)this.underlyingXmlObject()).qNameValue();
    }
    
    public List listValue() {
        return ((SimpleValue)this.underlyingXmlObject()).listValue();
    }
    
    public List xlistValue() {
        return ((SimpleValue)this.underlyingXmlObject()).xlistValue();
    }
    
    public Object objectValue() {
        return ((SimpleValue)this.underlyingXmlObject()).objectValue();
    }
    
    public void set(final String obj) {
        ((SimpleValue)this.underlyingXmlObject()).set(obj);
    }
    
    public void set(final boolean v) {
        ((SimpleValue)this.underlyingXmlObject()).set(v);
    }
    
    public void set(final byte v) {
        ((SimpleValue)this.underlyingXmlObject()).set(v);
    }
    
    public void set(final short v) {
        ((SimpleValue)this.underlyingXmlObject()).set(v);
    }
    
    public void set(final int v) {
        ((SimpleValue)this.underlyingXmlObject()).set(v);
    }
    
    public void set(final long v) {
        ((SimpleValue)this.underlyingXmlObject()).set(v);
    }
    
    public void set(final BigInteger obj) {
        ((SimpleValue)this.underlyingXmlObject()).set(obj);
    }
    
    public void set(final BigDecimal obj) {
        ((SimpleValue)this.underlyingXmlObject()).set(obj);
    }
    
    public void set(final float v) {
        ((SimpleValue)this.underlyingXmlObject()).set(v);
    }
    
    public void set(final double v) {
        ((SimpleValue)this.underlyingXmlObject()).set(v);
    }
    
    public void set(final byte[] obj) {
        ((SimpleValue)this.underlyingXmlObject()).set(obj);
    }
    
    public void set(final StringEnumAbstractBase obj) {
        ((SimpleValue)this.underlyingXmlObject()).set(obj);
    }
    
    public void set(final Calendar obj) {
        ((SimpleValue)this.underlyingXmlObject()).set(obj);
    }
    
    public void set(final Date obj) {
        ((SimpleValue)this.underlyingXmlObject()).set(obj);
    }
    
    public void set(final GDateSpecification obj) {
        ((SimpleValue)this.underlyingXmlObject()).set(obj);
    }
    
    public void set(final GDurationSpecification obj) {
        ((SimpleValue)this.underlyingXmlObject()).set(obj);
    }
    
    public void set(final QName obj) {
        ((SimpleValue)this.underlyingXmlObject()).set(obj);
    }
    
    public void set(final List obj) {
        ((SimpleValue)this.underlyingXmlObject()).set(obj);
    }
    
    public String getStringValue() {
        return ((SimpleValue)this.underlyingXmlObject()).getStringValue();
    }
    
    public boolean getBooleanValue() {
        return ((SimpleValue)this.underlyingXmlObject()).getBooleanValue();
    }
    
    public byte getByteValue() {
        return ((SimpleValue)this.underlyingXmlObject()).getByteValue();
    }
    
    public short getShortValue() {
        return ((SimpleValue)this.underlyingXmlObject()).getShortValue();
    }
    
    public int getIntValue() {
        return ((SimpleValue)this.underlyingXmlObject()).getIntValue();
    }
    
    public long getLongValue() {
        return ((SimpleValue)this.underlyingXmlObject()).getLongValue();
    }
    
    public BigInteger getBigIntegerValue() {
        return ((SimpleValue)this.underlyingXmlObject()).getBigIntegerValue();
    }
    
    public BigDecimal getBigDecimalValue() {
        return ((SimpleValue)this.underlyingXmlObject()).getBigDecimalValue();
    }
    
    public float getFloatValue() {
        return ((SimpleValue)this.underlyingXmlObject()).getFloatValue();
    }
    
    public double getDoubleValue() {
        return ((SimpleValue)this.underlyingXmlObject()).getDoubleValue();
    }
    
    public byte[] getByteArrayValue() {
        return ((SimpleValue)this.underlyingXmlObject()).getByteArrayValue();
    }
    
    public StringEnumAbstractBase getEnumValue() {
        return ((SimpleValue)this.underlyingXmlObject()).getEnumValue();
    }
    
    public Calendar getCalendarValue() {
        return ((SimpleValue)this.underlyingXmlObject()).getCalendarValue();
    }
    
    public Date getDateValue() {
        return ((SimpleValue)this.underlyingXmlObject()).getDateValue();
    }
    
    public GDate getGDateValue() {
        return ((SimpleValue)this.underlyingXmlObject()).getGDateValue();
    }
    
    public GDuration getGDurationValue() {
        return ((SimpleValue)this.underlyingXmlObject()).getGDurationValue();
    }
    
    public QName getQNameValue() {
        return ((SimpleValue)this.underlyingXmlObject()).getQNameValue();
    }
    
    public List getListValue() {
        return ((SimpleValue)this.underlyingXmlObject()).getListValue();
    }
    
    public List xgetListValue() {
        return ((SimpleValue)this.underlyingXmlObject()).xgetListValue();
    }
    
    public Object getObjectValue() {
        return ((SimpleValue)this.underlyingXmlObject()).getObjectValue();
    }
    
    public void setStringValue(final String obj) {
        ((SimpleValue)this.underlyingXmlObject()).setStringValue(obj);
    }
    
    public void setBooleanValue(final boolean v) {
        ((SimpleValue)this.underlyingXmlObject()).setBooleanValue(v);
    }
    
    public void setByteValue(final byte v) {
        ((SimpleValue)this.underlyingXmlObject()).setByteValue(v);
    }
    
    public void setShortValue(final short v) {
        ((SimpleValue)this.underlyingXmlObject()).setShortValue(v);
    }
    
    public void setIntValue(final int v) {
        ((SimpleValue)this.underlyingXmlObject()).setIntValue(v);
    }
    
    public void setLongValue(final long v) {
        ((SimpleValue)this.underlyingXmlObject()).setLongValue(v);
    }
    
    public void setBigIntegerValue(final BigInteger obj) {
        ((SimpleValue)this.underlyingXmlObject()).setBigIntegerValue(obj);
    }
    
    public void setBigDecimalValue(final BigDecimal obj) {
        ((SimpleValue)this.underlyingXmlObject()).setBigDecimalValue(obj);
    }
    
    public void setFloatValue(final float v) {
        ((SimpleValue)this.underlyingXmlObject()).setFloatValue(v);
    }
    
    public void setDoubleValue(final double v) {
        ((SimpleValue)this.underlyingXmlObject()).setDoubleValue(v);
    }
    
    public void setByteArrayValue(final byte[] obj) {
        ((SimpleValue)this.underlyingXmlObject()).setByteArrayValue(obj);
    }
    
    public void setEnumValue(final StringEnumAbstractBase obj) {
        ((SimpleValue)this.underlyingXmlObject()).setEnumValue(obj);
    }
    
    public void setCalendarValue(final Calendar obj) {
        ((SimpleValue)this.underlyingXmlObject()).setCalendarValue(obj);
    }
    
    public void setDateValue(final Date obj) {
        ((SimpleValue)this.underlyingXmlObject()).setDateValue(obj);
    }
    
    public void setGDateValue(final GDate obj) {
        ((SimpleValue)this.underlyingXmlObject()).setGDateValue(obj);
    }
    
    public void setGDurationValue(final GDuration obj) {
        ((SimpleValue)this.underlyingXmlObject()).setGDurationValue(obj);
    }
    
    public void setQNameValue(final QName obj) {
        ((SimpleValue)this.underlyingXmlObject()).setQNameValue(obj);
    }
    
    public void setListValue(final List obj) {
        ((SimpleValue)this.underlyingXmlObject()).setListValue(obj);
    }
    
    public void setObjectValue(final Object obj) {
        ((SimpleValue)this.underlyingXmlObject()).setObjectValue(obj);
    }
    
    public void objectSet(final Object obj) {
        ((SimpleValue)this.underlyingXmlObject()).objectSet(obj);
    }
    
    public XmlObject[] selectChildren(final QName elementName) {
        return this.underlyingXmlObject().selectChildren(elementName);
    }
    
    public XmlObject[] selectChildren(final String elementUri, final String elementLocalName) {
        return this.underlyingXmlObject().selectChildren(elementUri, elementLocalName);
    }
    
    public XmlObject[] selectChildren(final QNameSet elementNameSet) {
        return this.underlyingXmlObject().selectChildren(elementNameSet);
    }
    
    public XmlObject selectAttribute(final QName attributeName) {
        return this.underlyingXmlObject().selectAttribute(attributeName);
    }
    
    public XmlObject selectAttribute(final String attributeUri, final String attributeLocalName) {
        return this.underlyingXmlObject().selectAttribute(attributeUri, attributeLocalName);
    }
    
    public XmlObject[] selectAttributes(final QNameSet attributeNameSet) {
        return this.underlyingXmlObject().selectAttributes(attributeNameSet);
    }
}
