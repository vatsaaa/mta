// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.xmlbeans;

import org.apache.xmlbeans.xml.stream.XMLStreamException;

public class XMLStreamValidationException extends XMLStreamException
{
    private XmlError _xmlError;
    
    public XMLStreamValidationException(final XmlError xmlError) {
        super(xmlError.toString());
        this._xmlError = xmlError;
    }
    
    public XmlError getXmlError() {
        return this._xmlError;
    }
}
