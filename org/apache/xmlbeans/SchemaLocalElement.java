// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.xmlbeans;

public interface SchemaLocalElement extends SchemaField, SchemaAnnotated
{
    boolean blockExtension();
    
    boolean blockRestriction();
    
    boolean blockSubstitution();
    
    boolean isAbstract();
    
    SchemaIdentityConstraint[] getIdentityConstraints();
}
