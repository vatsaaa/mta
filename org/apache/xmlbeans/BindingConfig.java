// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.xmlbeans;

import javax.xml.namespace.QName;

public class BindingConfig
{
    private static final InterfaceExtension[] EMPTY_INTERFACE_EXT_ARRAY;
    private static final PrePostExtension[] EMPTY_PREPOST_EXT_ARRAY;
    
    public String lookupPackageForNamespace(final String uri) {
        return null;
    }
    
    public String lookupPrefixForNamespace(final String uri) {
        return null;
    }
    
    public String lookupSuffixForNamespace(final String uri) {
        return null;
    }
    
    public String lookupJavanameForQName(final QName qname) {
        return null;
    }
    
    public InterfaceExtension[] getInterfaceExtensions() {
        return BindingConfig.EMPTY_INTERFACE_EXT_ARRAY;
    }
    
    public InterfaceExtension[] getInterfaceExtensions(final String fullJavaName) {
        return BindingConfig.EMPTY_INTERFACE_EXT_ARRAY;
    }
    
    public PrePostExtension[] getPrePostExtensions() {
        return BindingConfig.EMPTY_PREPOST_EXT_ARRAY;
    }
    
    public PrePostExtension getPrePostExtension(final String fullJavaName) {
        return null;
    }
    
    static {
        EMPTY_INTERFACE_EXT_ARRAY = new InterfaceExtension[0];
        EMPTY_PREPOST_EXT_ARRAY = new PrePostExtension[0];
    }
}
