// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.xmlbeans;

import java.io.InputStream;

public interface ResourceLoader
{
    InputStream getResourceAsStream(final String p0);
    
    void close();
}
