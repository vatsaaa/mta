// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.xmlbeans;

import java.util.List;
import javax.xml.namespace.QName;
import java.util.Date;
import java.util.Calendar;
import java.math.BigDecimal;
import java.math.BigInteger;

public interface SimpleValue extends XmlObject
{
    SchemaType instanceType();
    
    String getStringValue();
    
    boolean getBooleanValue();
    
    byte getByteValue();
    
    short getShortValue();
    
    int getIntValue();
    
    long getLongValue();
    
    BigInteger getBigIntegerValue();
    
    BigDecimal getBigDecimalValue();
    
    float getFloatValue();
    
    double getDoubleValue();
    
    byte[] getByteArrayValue();
    
    StringEnumAbstractBase getEnumValue();
    
    Calendar getCalendarValue();
    
    Date getDateValue();
    
    GDate getGDateValue();
    
    GDuration getGDurationValue();
    
    QName getQNameValue();
    
    List getListValue();
    
    List xgetListValue();
    
    Object getObjectValue();
    
    void setStringValue(final String p0);
    
    void setBooleanValue(final boolean p0);
    
    void setByteValue(final byte p0);
    
    void setShortValue(final short p0);
    
    void setIntValue(final int p0);
    
    void setLongValue(final long p0);
    
    void setBigIntegerValue(final BigInteger p0);
    
    void setBigDecimalValue(final BigDecimal p0);
    
    void setFloatValue(final float p0);
    
    void setDoubleValue(final double p0);
    
    void setByteArrayValue(final byte[] p0);
    
    void setEnumValue(final StringEnumAbstractBase p0);
    
    void setCalendarValue(final Calendar p0);
    
    void setDateValue(final Date p0);
    
    void setGDateValue(final GDate p0);
    
    void setGDurationValue(final GDuration p0);
    
    void setQNameValue(final QName p0);
    
    void setListValue(final List p0);
    
    void setObjectValue(final Object p0);
    
    String stringValue();
    
    boolean booleanValue();
    
    byte byteValue();
    
    short shortValue();
    
    int intValue();
    
    long longValue();
    
    BigInteger bigIntegerValue();
    
    BigDecimal bigDecimalValue();
    
    float floatValue();
    
    double doubleValue();
    
    byte[] byteArrayValue();
    
    StringEnumAbstractBase enumValue();
    
    Calendar calendarValue();
    
    Date dateValue();
    
    GDate gDateValue();
    
    GDuration gDurationValue();
    
    QName qNameValue();
    
    List listValue();
    
    List xlistValue();
    
    Object objectValue();
    
    void set(final String p0);
    
    void set(final boolean p0);
    
    void set(final byte p0);
    
    void set(final short p0);
    
    void set(final int p0);
    
    void set(final long p0);
    
    void set(final BigInteger p0);
    
    void set(final BigDecimal p0);
    
    void set(final float p0);
    
    void set(final double p0);
    
    void set(final byte[] p0);
    
    void set(final StringEnumAbstractBase p0);
    
    void set(final Calendar p0);
    
    void set(final Date p0);
    
    void set(final GDateSpecification p0);
    
    void set(final GDurationSpecification p0);
    
    void set(final QName p0);
    
    void set(final List p0);
    
    void objectSet(final Object p0);
}
