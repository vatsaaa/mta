// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.xmlbeans;

import java.util.ListIterator;
import java.util.Iterator;
import java.util.Collection;
import java.io.Serializable;
import java.util.List;

public class XmlSimpleList implements List, Serializable
{
    private static final long serialVersionUID = 1L;
    private List underlying;
    
    public XmlSimpleList(final List list) {
        this.underlying = list;
    }
    
    public int size() {
        return this.underlying.size();
    }
    
    public boolean isEmpty() {
        return this.underlying.isEmpty();
    }
    
    public boolean contains(final Object o) {
        return this.underlying.contains(o);
    }
    
    public boolean containsAll(final Collection coll) {
        return this.underlying.containsAll(coll);
    }
    
    public Object[] toArray() {
        return this.underlying.toArray();
    }
    
    public Object[] toArray(final Object[] a) {
        return this.underlying.toArray(a);
    }
    
    public boolean add(final Object o) {
        throw new UnsupportedOperationException();
    }
    
    public boolean addAll(final Collection coll) {
        throw new UnsupportedOperationException();
    }
    
    public boolean remove(final Object o) {
        throw new UnsupportedOperationException();
    }
    
    public boolean removeAll(final Collection coll) {
        throw new UnsupportedOperationException();
    }
    
    public boolean retainAll(final Collection coll) {
        throw new UnsupportedOperationException();
    }
    
    public void clear() {
        throw new UnsupportedOperationException();
    }
    
    public Object get(final int index) {
        return this.underlying.get(index);
    }
    
    public Object set(final int index, final Object element) {
        throw new UnsupportedOperationException();
    }
    
    public void add(final int index, final Object element) {
        throw new UnsupportedOperationException();
    }
    
    public Object remove(final int index) {
        throw new UnsupportedOperationException();
    }
    
    public int indexOf(final Object o) {
        return this.underlying.indexOf(o);
    }
    
    public int lastIndexOf(final Object o) {
        return this.underlying.lastIndexOf(o);
    }
    
    public boolean addAll(final int index, final Collection c) {
        throw new UnsupportedOperationException();
    }
    
    public List subList(final int from, final int to) {
        return new XmlSimpleList(this.underlying.subList(from, to));
    }
    
    public Iterator iterator() {
        return new Iterator() {
            Iterator i = XmlSimpleList.this.underlying.iterator();
            
            public boolean hasNext() {
                return this.i.hasNext();
            }
            
            public Object next() {
                return this.i.next();
            }
            
            public void remove() {
                throw new UnsupportedOperationException();
            }
        };
    }
    
    public ListIterator listIterator() {
        return this.listIterator(0);
    }
    
    public ListIterator listIterator(final int index) {
        return new ListIterator() {
            ListIterator i = XmlSimpleList.this.underlying.listIterator(index);
            
            public boolean hasNext() {
                return this.i.hasNext();
            }
            
            public Object next() {
                return this.i.next();
            }
            
            public boolean hasPrevious() {
                return this.i.hasPrevious();
            }
            
            public Object previous() {
                return this.i.previous();
            }
            
            public int nextIndex() {
                return this.i.nextIndex();
            }
            
            public int previousIndex() {
                return this.i.previousIndex();
            }
            
            public void remove() {
                throw new UnsupportedOperationException();
            }
            
            public void set(final Object o) {
                throw new UnsupportedOperationException();
            }
            
            public void add(final Object o) {
                throw new UnsupportedOperationException();
            }
        };
    }
    
    private String stringValue(final Object o) {
        if (o instanceof SimpleValue) {
            return ((SimpleValue)o).stringValue();
        }
        return o.toString();
    }
    
    public String toString() {
        final int size = this.underlying.size();
        if (size == 0) {
            return "";
        }
        final String first = this.stringValue(this.underlying.get(0));
        if (size == 1) {
            return first;
        }
        final StringBuffer result = new StringBuffer(first);
        for (int i = 1; i < size; ++i) {
            result.append(' ');
            result.append(this.stringValue(this.underlying.get(i)));
        }
        return result.toString();
    }
    
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof XmlSimpleList)) {
            return false;
        }
        final XmlSimpleList xmlSimpleList = (XmlSimpleList)o;
        final List underlying2 = xmlSimpleList.underlying;
        final int size = this.underlying.size();
        if (size != underlying2.size()) {
            return false;
        }
        for (int i = 0; i < size; ++i) {
            final Object item = this.underlying.get(i);
            final Object item2 = underlying2.get(i);
            if (item == null) {
                if (item2 != null) {
                    return false;
                }
            }
            else if (!item.equals(item2)) {
                return false;
            }
        }
        return true;
    }
    
    public int hashCode() {
        final int size = this.underlying.size();
        int hash = 0;
        for (int i = 0; i < size; ++i) {
            final Object item = this.underlying.get(i);
            hash *= 19;
            hash += item.hashCode();
        }
        return hash;
    }
}
