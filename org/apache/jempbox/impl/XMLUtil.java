// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.jempbox.impl;

import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.io.StringWriter;
import java.io.OutputStream;
import javax.xml.transform.TransformerException;
import javax.xml.transform.Result;
import javax.xml.transform.Transformer;
import javax.xml.transform.Source;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import javax.xml.transform.TransformerFactory;
import org.apache.jempbox.xmp.Elementable;
import org.w3c.dom.Text;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Element;
import org.xml.sax.InputSource;
import javax.xml.parsers.DocumentBuilder;
import java.io.IOException;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import java.io.InputStream;

public class XMLUtil
{
    private XMLUtil() {
    }
    
    public static Document parse(final InputStream is) throws IOException {
        try {
            final DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
            final DocumentBuilder builder = builderFactory.newDocumentBuilder();
            return builder.parse(is);
        }
        catch (Exception e) {
            final IOException thrown = new IOException(e.getMessage());
            throw thrown;
        }
    }
    
    public static Document parse(final InputSource is) throws IOException {
        try {
            final DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
            final DocumentBuilder builder = builderFactory.newDocumentBuilder();
            return builder.parse(is);
        }
        catch (Exception e) {
            final IOException thrown = new IOException(e.getMessage());
            throw thrown;
        }
    }
    
    public static Document parse(final String fileName) throws IOException {
        try {
            final DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
            final DocumentBuilder builder = builderFactory.newDocumentBuilder();
            return builder.parse(fileName);
        }
        catch (Exception e) {
            final IOException thrown = new IOException(e.getMessage());
            throw thrown;
        }
    }
    
    public static Document newDocument() throws IOException {
        try {
            final DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
            final DocumentBuilder builder = builderFactory.newDocumentBuilder();
            return builder.newDocument();
        }
        catch (Exception e) {
            final IOException thrown = new IOException(e.getMessage());
            throw thrown;
        }
    }
    
    public static Element getElement(final Element parent, final String elementName) {
        Element retval = null;
        final NodeList children = parent.getElementsByTagName(elementName);
        if (children.getLength() > 0) {
            retval = (Element)children.item(0);
        }
        return retval;
    }
    
    public static Integer getIntValue(final Element parent, final String nodeName) {
        final String intVal = getStringValue(getElement(parent, nodeName));
        Integer retval = null;
        if (intVal != null) {
            retval = new Integer(intVal);
        }
        return retval;
    }
    
    public static void setIntValue(final Element parent, final String nodeName, final Integer intValue) {
        Element currentValue = getElement(parent, nodeName);
        if (intValue == null) {
            if (currentValue != null) {
                parent.removeChild(currentValue);
            }
        }
        else {
            if (currentValue == null) {
                currentValue = parent.getOwnerDocument().createElement(nodeName);
                parent.appendChild(currentValue);
            }
            setStringValue(currentValue, intValue.toString());
        }
    }
    
    public static String getStringValue(final Element parent, final String nodeName) {
        return getStringValue(getElement(parent, nodeName));
    }
    
    public static void setStringValue(final Element parent, final String nodeName, final String nodeValue) {
        Element currentValue = getElement(parent, nodeName);
        if (nodeValue == null) {
            if (currentValue != null) {
                parent.removeChild(currentValue);
            }
        }
        else {
            if (currentValue == null) {
                currentValue = parent.getOwnerDocument().createElement(nodeName);
                parent.appendChild(currentValue);
            }
            setStringValue(currentValue, nodeValue);
        }
    }
    
    public static String getStringValue(final Element node) {
        String retval = "";
        final NodeList children = node.getChildNodes();
        for (int i = 0; i < children.getLength(); ++i) {
            final Node next = children.item(i);
            if (next instanceof Text) {
                retval = next.getNodeValue();
            }
        }
        return retval;
    }
    
    public static void setStringValue(final Element node, final String value) {
        final NodeList children = node.getChildNodes();
        for (int i = 0; i < children.getLength(); ++i) {
            final Node next = children.item(i);
            if (next instanceof Text) {
                node.removeChild(next);
            }
        }
        node.appendChild(node.getOwnerDocument().createTextNode(value));
    }
    
    public static void setElementableValue(final Element parent, final String name, final Elementable node) {
        final NodeList nodes = parent.getElementsByTagName(name);
        if (node == null) {
            for (int i = 0; i < nodes.getLength(); ++i) {
                parent.removeChild(nodes.item(i));
            }
        }
        else if (nodes.getLength() == 0) {
            if (parent.hasChildNodes()) {
                final Node firstChild = parent.getChildNodes().item(0);
                parent.insertBefore(node.getElement(), firstChild);
            }
            else {
                parent.appendChild(node.getElement());
            }
        }
        else {
            final Node oldNode = nodes.item(0);
            parent.replaceChild(node.getElement(), oldNode);
        }
    }
    
    public static void save(final Document doc, final String file, final String encoding) throws TransformerException {
        try {
            final Transformer transformer = TransformerFactory.newInstance().newTransformer();
            transformer.setOutputProperty("indent", "yes");
            transformer.setOutputProperty("encoding", encoding);
            transformer.setOutputProperty("omit-xml-declaration", "yes");
            final Result result = new StreamResult(new File(file));
            final DOMSource source = new DOMSource(doc);
            transformer.transform(source, result);
        }
        finally {}
    }
    
    public static void save(final Node doc, final OutputStream outStream, final String encoding) throws TransformerException {
        try {
            final Transformer transformer = TransformerFactory.newInstance().newTransformer();
            transformer.setOutputProperty("indent", "yes");
            transformer.setOutputProperty("encoding", encoding);
            transformer.setOutputProperty("omit-xml-declaration", "yes");
            final Result result = new StreamResult(outStream);
            final DOMSource source = new DOMSource(doc);
            transformer.transform(source, result);
        }
        finally {}
    }
    
    public static byte[] asByteArray(final Document doc, final String encoding) throws TransformerException {
        final Transformer transformer = TransformerFactory.newInstance().newTransformer();
        transformer.setOutputProperty("indent", "yes");
        transformer.setOutputProperty("encoding", encoding);
        transformer.setOutputProperty("omit-xml-declaration", "yes");
        final StringWriter writer = new StringWriter();
        final Result result = new StreamResult(writer);
        final DOMSource source = new DOMSource(doc);
        transformer.transform(source, result);
        try {
            return writer.getBuffer().toString().getBytes(encoding);
        }
        catch (UnsupportedEncodingException e) {
            throw new TransformerException("Unsupported Encoding", e);
        }
    }
}
