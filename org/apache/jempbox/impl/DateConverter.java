// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.jempbox.impl;

import java.util.Date;
import java.text.ParseException;
import java.util.TimeZone;
import java.util.GregorianCalendar;
import java.util.SimpleTimeZone;
import java.io.IOException;
import java.util.Calendar;
import java.text.SimpleDateFormat;

public class DateConverter
{
    private static final SimpleDateFormat[] POTENTIAL_FORMATS;
    
    private DateConverter() {
    }
    
    public static Calendar toCalendar(String date) throws IOException {
        Calendar retval = null;
        if (date != null && date.trim().length() > 0) {
            int year = 0;
            int month = 1;
            int day = 1;
            int hour = 0;
            int minute = 0;
            int second = 0;
            try {
                SimpleTimeZone zone = null;
                if (date.startsWith("D:")) {
                    date = date.substring(2, date.length());
                }
                date = date.replaceAll("[-:T]", "");
                if (date.length() < 4) {
                    throw new IOException("Error: Invalid date format '" + date + "'");
                }
                year = Integer.parseInt(date.substring(0, 4));
                if (date.length() >= 6) {
                    month = Integer.parseInt(date.substring(4, 6));
                }
                if (date.length() >= 8) {
                    day = Integer.parseInt(date.substring(6, 8));
                }
                if (date.length() >= 10) {
                    hour = Integer.parseInt(date.substring(8, 10));
                }
                if (date.length() >= 12) {
                    minute = Integer.parseInt(date.substring(10, 12));
                }
                if (date.length() >= 14) {
                    second = Integer.parseInt(date.substring(12, 14));
                }
                if (date.length() >= 15) {
                    final char sign = date.charAt(14);
                    if (sign == 'Z') {
                        zone = new SimpleTimeZone(0, "Unknown");
                    }
                    else {
                        int hours = 0;
                        int minutes = 0;
                        if (date.length() >= 17) {
                            if (sign == '+') {
                                hours = Integer.parseInt(date.substring(15, 17));
                            }
                            else {
                                hours = -Integer.parseInt(date.substring(14, 16));
                            }
                        }
                        if (sign == '+') {
                            if (date.length() >= 19) {
                                minutes = Integer.parseInt(date.substring(17, 19));
                            }
                        }
                        else if (date.length() >= 18) {
                            minutes = Integer.parseInt(date.substring(16, 18));
                        }
                        zone = new SimpleTimeZone(hours * 60 * 60 * 1000 + minutes * 60 * 1000, "Unknown");
                    }
                }
                if (zone == null) {
                    retval = new GregorianCalendar();
                }
                else {
                    retval = new GregorianCalendar(zone);
                }
                retval.clear();
                retval.set(year, month - 1, day, hour, minute, second);
            }
            catch (NumberFormatException e) {
                if (date.substring(date.length() - 3, date.length() - 2).equals(":") && (date.substring(date.length() - 6, date.length() - 5).equals("+") || date.substring(date.length() - 6, date.length() - 5).equals("-"))) {
                    date = date.substring(0, date.length() - 3) + date.substring(date.length() - 2);
                }
                for (int i = 0; retval == null && i < DateConverter.POTENTIAL_FORMATS.length; ++i) {
                    try {
                        final Date utilDate = DateConverter.POTENTIAL_FORMATS[i].parse(date);
                        retval = new GregorianCalendar();
                        retval.setTime(utilDate);
                    }
                    catch (ParseException ex) {}
                }
                if (retval == null) {
                    throw new IOException("Error converting date:" + date);
                }
            }
        }
        return retval;
    }
    
    private static final void zeroAppend(final StringBuffer out, final int number) {
        if (number < 10) {
            out.append("0");
        }
        out.append(number);
    }
    
    public static String toISO8601(final Calendar cal) {
        final StringBuffer retval = new StringBuffer();
        retval.append(cal.get(1));
        retval.append("-");
        zeroAppend(retval, cal.get(2) + 1);
        retval.append("-");
        zeroAppend(retval, cal.get(5));
        retval.append("T");
        zeroAppend(retval, cal.get(11));
        retval.append(":");
        zeroAppend(retval, cal.get(12));
        retval.append(":");
        zeroAppend(retval, cal.get(13));
        int timeZone = cal.get(15) + cal.get(16);
        if (timeZone < 0) {
            retval.append("-");
        }
        else {
            retval.append("+");
        }
        timeZone = Math.abs(timeZone);
        final int hours = timeZone / 1000 / 60 / 60;
        final int minutes = (timeZone - hours * 1000 * 60 * 60) / 1000 / 1000;
        if (hours < 10) {
            retval.append("0");
        }
        retval.append(Integer.toString(hours));
        retval.append(":");
        if (minutes < 10) {
            retval.append("0");
        }
        retval.append(Integer.toString(minutes));
        return retval.toString();
    }
    
    static {
        POTENTIAL_FORMATS = new SimpleDateFormat[] { new SimpleDateFormat("EEEE, dd MMM yyyy hh:mm:ss a"), new SimpleDateFormat("EEEE, MMM dd, yyyy hh:mm:ss a"), new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'"), new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssz"), new SimpleDateFormat("MM/dd/yyyy hh:mm:ss"), new SimpleDateFormat("MM/dd/yyyy"), new SimpleDateFormat("EEEE, MMM dd, yyyy"), new SimpleDateFormat("EEEE MMM dd, yyyy HH:mm:ss"), new SimpleDateFormat("EEEE MMM dd HH:mm:ss z yyyy"), new SimpleDateFormat("EEEE, MMM dd, yyyy 'at' hh:mma") };
    }
}
