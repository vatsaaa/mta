// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.jempbox.xmp;

import java.io.IOException;
import java.util.Calendar;
import java.util.List;
import org.w3c.dom.Element;

public class XMPSchemaBasic extends XMPSchema
{
    public static final String NAMESPACE = "http://ns.adobe.com/xap/1.0/";
    
    public XMPSchemaBasic(final XMPMetadata parent) {
        super(parent, "xmp", "http://ns.adobe.com/xap/1.0/");
        this.schema.setAttributeNS("http://www.w3.org/2000/xmlns/", "xmlns:xapGImg", "http://ns.adobe.com/xap/1.0/g/img/");
    }
    
    public XMPSchemaBasic(final Element element, final String prefix) {
        super(element, prefix);
        if (this.schema.getAttribute("xmlns:xapGImg") == null) {
            this.schema.setAttributeNS("http://www.w3.org/2000/xmlns/", "xmlns:xapGImg", "http://ns.adobe.com/xap/1.0/g/img/");
        }
    }
    
    public void removeAdvisory(final String advisory) {
        this.removeBagValue(this.prefix + ":Advisory", advisory);
    }
    
    public void addAdvisory(final String advisory) {
        this.addBagValue(this.prefix + ":Advisory", advisory);
    }
    
    public List<String> getAdvisories() {
        return this.getBagList(this.prefix + ":Advisory");
    }
    
    public void setBaseURL(final String url) {
        this.setTextProperty(this.prefix + ":BaseURL", url);
    }
    
    public String getBaseURL() {
        return this.getTextProperty(this.prefix + ":BaseURL");
    }
    
    public void setCreateDate(final Calendar date) {
        this.setDateProperty(this.prefix + ":CreateDate", date);
    }
    
    public Calendar getCreateDate() throws IOException {
        return this.getDateProperty(this.prefix + ":CreateDate");
    }
    
    public void setCreatorTool(final String creator) {
        this.setTextProperty(this.prefix + ":CreatorTool", creator);
    }
    
    public String getCreatorTool() {
        return this.getTextProperty(this.prefix + ":CreatorTool");
    }
    
    public void removeIdentifier(final String id) {
        this.removeBagValue(this.prefix + ":Identifier", id);
    }
    
    public void addIdentifier(final String id) {
        this.addBagValue(this.prefix + ":Identifier", id);
    }
    
    public List<String> getIdentifiers() {
        return this.getBagList(this.prefix + ":Identifier");
    }
    
    public void setLabel(final String label) {
        this.setTextProperty(this.prefix + ":Label", label);
    }
    
    public String getLabel() {
        return this.getTextProperty(this.prefix + "p:Label");
    }
    
    public void setTitle(final String title) {
        this.setTextProperty(this.prefix + ":Title", title);
    }
    
    public String getTitle() {
        return this.getTextProperty(this.prefix + ":Title");
    }
    
    public void setMetadataDate(final Calendar date) {
        this.setDateProperty(this.prefix + ":MetadataDate", date);
    }
    
    public Calendar getMetadataDate() throws IOException {
        return this.getDateProperty(this.prefix + ":MetadataDate");
    }
    
    public void setModifyDate(final Calendar date) {
        this.setDateProperty(this.prefix + ":ModifyDate", date);
    }
    
    public Calendar getModifyDate() throws IOException {
        return this.getDateProperty(this.prefix + ":ModifyDate");
    }
    
    public void setNickname(final String nickname) {
        this.setTextProperty(this.prefix + ":Nickname", nickname);
    }
    
    public String getNickname() {
        return this.getTextProperty(this.prefix + ":Nickname");
    }
    
    public Integer getRating() {
        return this.getIntegerProperty(this.prefix + ":Rating");
    }
    
    public void setRating(final Integer rating) {
        this.setIntegerProperty(this.prefix + ":Rating", rating);
    }
    
    public void setThumbnail(final Thumbnail thumbnail) {
        this.setThumbnailProperty(this.prefix + ":Thumbnails", null, thumbnail);
    }
    
    public Thumbnail getThumbnail() {
        return this.getThumbnailProperty(this.prefix + ":Thumbnails", null);
    }
    
    public void setThumbnail(final String language, final Thumbnail thumbnail) {
        this.setThumbnailProperty(this.prefix + ":Thumbnails", language, thumbnail);
    }
    
    public Thumbnail getThumbnail(final String language) {
        return this.getThumbnailProperty(this.prefix + ":Thumbnails", language);
    }
    
    public List<String> getThumbnailLanguages() {
        return this.getLanguagePropertyLanguages(this.prefix + ":Thumbnails");
    }
}
