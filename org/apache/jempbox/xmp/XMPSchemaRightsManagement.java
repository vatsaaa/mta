// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.jempbox.xmp;

import java.util.List;
import org.w3c.dom.Element;

public class XMPSchemaRightsManagement extends XMPSchema
{
    public static final String NAMESPACE = "http://ns.adobe.com/xap/1.0/rights/";
    
    public XMPSchemaRightsManagement(final XMPMetadata parent) {
        super(parent, "xmpRights", "http://ns.adobe.com/xap/1.0/rights/");
    }
    
    public XMPSchemaRightsManagement(final Element element, final String prefix) {
        super(element, prefix);
    }
    
    public void setCertificateURL(final String certificate) {
        this.setTextProperty("xmpRights:Certificate", certificate);
    }
    
    public String getCertificateURL() {
        return this.getTextProperty(this.prefix + ":Certificate");
    }
    
    public void setMarked(final Boolean marked) {
        this.setBooleanProperty(this.prefix + ":Marked", marked);
    }
    
    public Boolean getMarked() {
        final Boolean b = this.getBooleanProperty(this.prefix + ":Marked");
        return (b != null) ? b : Boolean.FALSE;
    }
    
    public void removeOwner(final String owner) {
        this.removeBagValue(this.prefix + ":Owner", owner);
    }
    
    public void addOwner(final String owner) {
        this.addBagValue(this.prefix + ":Owner", owner);
    }
    
    public List<String> getOwners() {
        return this.getBagList(this.prefix + ":Owner");
    }
    
    public void setUsageTerms(final String terms) {
        this.setLanguageProperty(this.prefix + ":UsageTerms", null, terms);
    }
    
    public String getUsageTerms() {
        return this.getLanguageProperty(this.prefix + ":UsageTerms", null);
    }
    
    public void setDescription(final String language, final String terms) {
        this.setLanguageProperty(this.prefix + ":UsageTerms", language, terms);
    }
    
    public String getUsageTerms(final String language) {
        return this.getLanguageProperty(this.prefix + ":UsageTerms", language);
    }
    
    public List<String> getUsageTermsLanguages() {
        return this.getLanguagePropertyLanguages(this.prefix + ":UsageTerms");
    }
    
    public void setWebStatement(final String webStatement) {
        this.setTextProperty(this.prefix + ":WebStatement", webStatement);
    }
    
    public String getWebStatement() {
        return this.getTextProperty(this.prefix + ":WebStatement");
    }
    
    public void setCopyright(final String copyright) {
        this.setTextProperty(this.prefix + ":Copyright", copyright);
    }
    
    public String getCopyright() {
        return this.getTextProperty(this.prefix + ":Copyright");
    }
}
