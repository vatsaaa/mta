// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.jempbox.xmp;

import java.util.Iterator;
import java.util.LinkedList;
import java.lang.reflect.Constructor;
import org.w3c.dom.NamedNodeMap;
import java.util.ArrayList;
import java.util.List;
import java.io.InputStream;
import org.xml.sax.InputSource;
import org.w3c.dom.NodeList;
import javax.xml.transform.TransformerException;
import java.io.OutputStream;
import java.io.IOException;
import org.w3c.dom.Element;
import org.w3c.dom.ProcessingInstruction;
import org.w3c.dom.Node;
import org.apache.jempbox.impl.XMLUtil;
import java.util.HashMap;
import java.util.Map;
import org.w3c.dom.Document;

public class XMPMetadata
{
    public static final String ENCODING_UTF8 = "UTF-8";
    public static final String ENCODING_UTF16BE = "UTF-16BE";
    public static final String ENCODING_UTF16LE = "UTF-16LE";
    protected Document xmpDocument;
    protected String encoding;
    protected Map<String, Class<?>> nsMappings;
    
    public XMPMetadata() throws IOException {
        this.encoding = "UTF-8";
        this.nsMappings = new HashMap<String, Class<?>>();
        this.xmpDocument = XMLUtil.newDocument();
        final ProcessingInstruction beginXPacket = this.xmpDocument.createProcessingInstruction("xpacket", "begin=\"\ufeff\" id=\"W5M0MpCehiHzreSzNTczkc9d\"");
        this.xmpDocument.appendChild(beginXPacket);
        final Element xmpMeta = this.xmpDocument.createElementNS("adobe:ns:meta/", "x:xmpmeta");
        xmpMeta.setAttributeNS("http://www.w3.org/2000/xmlns/", "xmlns:x", "adobe:ns:meta/");
        this.xmpDocument.appendChild(xmpMeta);
        final Element rdf = this.xmpDocument.createElement("rdf:RDF");
        rdf.setAttributeNS("http://www.w3.org/2000/xmlns/", "xmlns:rdf", "http://www.w3.org/1999/02/22-rdf-syntax-ns#");
        xmpMeta.appendChild(rdf);
        final ProcessingInstruction endXPacket = this.xmpDocument.createProcessingInstruction("xpacket", "end=\"w\"");
        this.xmpDocument.appendChild(endXPacket);
        this.init();
    }
    
    public XMPMetadata(final Document doc) {
        this.encoding = "UTF-8";
        this.nsMappings = new HashMap<String, Class<?>>();
        this.xmpDocument = doc;
        this.init();
    }
    
    private void init() {
        this.nsMappings.put("http://ns.adobe.com/pdf/1.3/", XMPSchemaPDF.class);
        this.nsMappings.put("http://ns.adobe.com/xap/1.0/", XMPSchemaBasic.class);
        this.nsMappings.put("http://purl.org/dc/elements/1.1/", XMPSchemaDublinCore.class);
        this.nsMappings.put("http://ns.adobe.com/xap/1.0/mm/", XMPSchemaMediaManagement.class);
        this.nsMappings.put("http://ns.adobe.com/xap/1.0/rights/", XMPSchemaRightsManagement.class);
        this.nsMappings.put("http://ns.adobe.com/xap/1.0/bj/", XMPSchemaBasicJobTicket.class);
        this.nsMappings.put("http://ns.adobe.com/xmp/1.0/DynamicMedia/", XMPSchemaDynamicMedia.class);
        this.nsMappings.put("http://ns.adobe.com/xap/1.0/t/pg/", XMPSchemaPagedText.class);
        this.nsMappings.put("http://iptc.org/std/Iptc4xmpCore/1.0/xmlns/", XMPSchemaIptc4xmpCore.class);
        this.nsMappings.put("http://ns.adobe.com/photoshop/1.0/", XMPSchemaPhotoshop.class);
    }
    
    public void addXMLNSMapping(final String namespace, final Class<?> xmpSchema) {
        if (!XMPSchema.class.isAssignableFrom(xmpSchema)) {
            throw new IllegalArgumentException("Only XMPSchemas can be mapped to.");
        }
        this.nsMappings.put(namespace, xmpSchema);
    }
    
    public XMPSchemaPDF getPDFSchema() throws IOException {
        return (XMPSchemaPDF)this.getSchemaByClass(XMPSchemaPDF.class);
    }
    
    public XMPSchemaBasic getBasicSchema() throws IOException {
        return (XMPSchemaBasic)this.getSchemaByClass(XMPSchemaBasic.class);
    }
    
    public XMPSchemaDublinCore getDublinCoreSchema() throws IOException {
        return (XMPSchemaDublinCore)this.getSchemaByClass(XMPSchemaDublinCore.class);
    }
    
    public XMPSchemaMediaManagement getMediaManagementSchema() throws IOException {
        return (XMPSchemaMediaManagement)this.getSchemaByClass(XMPSchemaMediaManagement.class);
    }
    
    public XMPSchemaRightsManagement getRightsManagementSchema() throws IOException {
        return (XMPSchemaRightsManagement)this.getSchemaByClass(XMPSchemaRightsManagement.class);
    }
    
    public XMPSchemaBasicJobTicket getBasicJobTicketSchema() throws IOException {
        return (XMPSchemaBasicJobTicket)this.getSchemaByClass(XMPSchemaBasicJobTicket.class);
    }
    
    public XMPSchemaDynamicMedia getDynamicMediaSchema() throws IOException {
        return (XMPSchemaDynamicMedia)this.getSchemaByClass(XMPSchemaDynamicMedia.class);
    }
    
    public XMPSchemaPagedText getPagedTextSchema() throws IOException {
        return (XMPSchemaPagedText)this.getSchemaByClass(XMPSchemaPagedText.class);
    }
    
    public XMPSchemaMediaManagement addMediaManagementSchema() {
        final XMPSchemaMediaManagement schema = new XMPSchemaMediaManagement(this);
        return (XMPSchemaMediaManagement)this.basicAddSchema(schema);
    }
    
    public XMPSchemaRightsManagement addRightsManagementSchema() {
        final XMPSchemaRightsManagement schema = new XMPSchemaRightsManagement(this);
        return (XMPSchemaRightsManagement)this.basicAddSchema(schema);
    }
    
    public XMPSchemaBasicJobTicket addBasicJobTicketSchema() {
        final XMPSchemaBasicJobTicket schema = new XMPSchemaBasicJobTicket(this);
        return (XMPSchemaBasicJobTicket)this.basicAddSchema(schema);
    }
    
    public XMPSchemaDynamicMedia addDynamicMediaSchema() {
        final XMPSchemaDynamicMedia schema = new XMPSchemaDynamicMedia(this);
        return (XMPSchemaDynamicMedia)this.basicAddSchema(schema);
    }
    
    public XMPSchemaPagedText addPagedTextSchema() {
        final XMPSchemaPagedText schema = new XMPSchemaPagedText(this);
        return (XMPSchemaPagedText)this.basicAddSchema(schema);
    }
    
    public void addSchema(final XMPSchema schema) {
        final Element rdf = this.getRDFElement();
        rdf.appendChild(schema.getElement());
    }
    
    public void save(final String file) throws Exception {
        XMLUtil.save(this.xmpDocument, file, this.encoding);
    }
    
    public void save(final OutputStream outStream) throws TransformerException {
        XMLUtil.save(this.xmpDocument, outStream, this.encoding);
    }
    
    public byte[] asByteArray() throws Exception {
        return XMLUtil.asByteArray(this.xmpDocument, this.encoding);
    }
    
    public Document getXMPDocument() {
        return this.xmpDocument;
    }
    
    protected XMPSchema basicAddSchema(final XMPSchema schema) {
        final Element rdf = this.getRDFElement();
        rdf.appendChild(schema.getElement());
        return schema;
    }
    
    public XMPSchemaPDF addPDFSchema() {
        final XMPSchemaPDF schema = new XMPSchemaPDF(this);
        return (XMPSchemaPDF)this.basicAddSchema(schema);
    }
    
    public XMPSchemaDublinCore addDublinCoreSchema() {
        final XMPSchemaDublinCore schema = new XMPSchemaDublinCore(this);
        return (XMPSchemaDublinCore)this.basicAddSchema(schema);
    }
    
    public XMPSchemaBasic addBasicSchema() {
        final XMPSchemaBasic schema = new XMPSchemaBasic(this);
        return (XMPSchemaBasic)this.basicAddSchema(schema);
    }
    
    public XMPSchemaIptc4xmpCore addIptc4xmpCoreSchema() {
        final XMPSchemaIptc4xmpCore schema = new XMPSchemaIptc4xmpCore(this);
        return (XMPSchemaIptc4xmpCore)this.basicAddSchema(schema);
    }
    
    public XMPSchemaPhotoshop addPhotoshopSchema() {
        final XMPSchemaPhotoshop schema = new XMPSchemaPhotoshop(this);
        return (XMPSchemaPhotoshop)this.basicAddSchema(schema);
    }
    
    public void setEncoding(final String xmlEncoding) {
        this.encoding = xmlEncoding;
    }
    
    public String getEncoding() {
        return this.encoding;
    }
    
    private Element getRDFElement() {
        Element rdf = null;
        final NodeList nodes = this.xmpDocument.getElementsByTagName("rdf:RDF");
        if (nodes.getLength() > 0) {
            rdf = (Element)nodes.item(0);
        }
        return rdf;
    }
    
    public static XMPMetadata load(final String file) throws IOException {
        return new XMPMetadata(XMLUtil.parse(file));
    }
    
    public static XMPMetadata load(final InputSource is) throws IOException {
        return new XMPMetadata(XMLUtil.parse(is));
    }
    
    public static XMPMetadata load(final InputStream is) throws IOException {
        return new XMPMetadata(XMLUtil.parse(is));
    }
    
    public static void main(final String[] args) throws Exception {
        final XMPMetadata metadata = new XMPMetadata();
        final XMPSchemaPDF pdf = metadata.addPDFSchema();
        pdf.setAbout("uuid:b8659d3a-369e-11d9-b951-000393c97fd8");
        pdf.setKeywords("ben,bob,pdf");
        pdf.setPDFVersion("1.3");
        pdf.setProducer("Acrobat Distiller 6.0.1 for Macintosh");
        final XMPSchemaDublinCore dc = metadata.addDublinCoreSchema();
        dc.addContributor("Ben Litchfield");
        dc.addContributor("Solar Eclipse");
        dc.addContributor("Some Other Guy");
        final XMPSchemaBasic basic = metadata.addBasicSchema();
        final Thumbnail t = new Thumbnail(metadata);
        t.setFormat("JPEG");
        t.setImage("IMAGE_DATA");
        t.setHeight(new Integer(100));
        t.setWidth(new Integer(200));
        basic.setThumbnail(t);
        basic.setBaseURL("http://www.pdfbox.org/");
        final List<XMPSchema> schemas = metadata.getSchemas();
        System.out.println("schemas=" + schemas);
        metadata.save("test.xmp");
    }
    
    public List<XMPSchema> getSchemas() throws IOException {
        final NodeList schemaList = this.xmpDocument.getElementsByTagName("rdf:Description");
        final List<XMPSchema> retval = new ArrayList<XMPSchema>(schemaList.getLength());
        for (int i = 0; i < schemaList.getLength(); ++i) {
            final Element schema = (Element)schemaList.item(i);
            boolean found = false;
            final NamedNodeMap attributes = schema.getAttributes();
            for (int j = 0; j < attributes.getLength(); ++j) {
                final Node attribute = attributes.item(j);
                final String name = attribute.getNodeName();
                final String value = attribute.getNodeValue();
                if (name.startsWith("xmlns:") && this.nsMappings.containsKey(value)) {
                    final Class<?> schemaClass = this.nsMappings.get(value);
                    try {
                        final Constructor<?> ctor = schemaClass.getConstructor(Element.class, String.class);
                        retval.add((XMPSchema)ctor.newInstance(schema, name.substring(6)));
                        found = true;
                    }
                    catch (NoSuchMethodException e2) {
                        throw new IOException("Error: Class " + schemaClass.getName() + " must have a constructor with the signature of " + schemaClass.getName() + "( org.w3c.dom.Element, java.lang.String )");
                    }
                    catch (Exception e) {
                        e.printStackTrace();
                        throw new IOException(e.getMessage());
                    }
                }
            }
            if (!found) {
                retval.add(new XMPSchema(schema, null));
            }
        }
        return retval;
    }
    
    public List<XMPSchema> getSchemasByNamespaceURI(final String namespaceURI) throws IOException {
        final List<XMPSchema> l = this.getSchemas();
        final List<XMPSchema> result = new LinkedList<XMPSchema>();
        final Class<?> schemaClass = this.nsMappings.get(namespaceURI);
        if (schemaClass == null) {
            return result;
        }
        for (final XMPSchema schema : l) {
            if (schemaClass.isAssignableFrom(schema.getClass())) {
                result.add(schema);
            }
        }
        return result;
    }
    
    public boolean hasUnknownSchema() throws IOException {
        final NodeList schemaList = this.xmpDocument.getElementsByTagName("rdf:Description");
        for (int i = 0; i < schemaList.getLength(); ++i) {
            final Element schema = (Element)schemaList.item(i);
            final NamedNodeMap attributes = schema.getAttributes();
            for (int j = 0; j < attributes.getLength(); ++j) {
                final Node attribute = attributes.item(j);
                final String name = attribute.getNodeName();
                final String value = attribute.getNodeValue();
                if (name.startsWith("xmlns:") && !this.nsMappings.containsKey(value) && !value.equals("http://ns.adobe.com/xap/1.0/sType/ResourceEvent#")) {
                    return true;
                }
            }
        }
        return false;
    }
    
    public XMPSchema getSchemaByClass(final Class<?> targetSchema) throws IOException {
        for (final XMPSchema element : this.getSchemas()) {
            if (element.getClass().getName().equals(targetSchema.getName())) {
                return element;
            }
        }
        return null;
    }
    
    public void merge(final XMPMetadata metadata) throws IOException {
        final List<XMPSchema> schemas2 = metadata.getSchemas();
        for (final XMPSchema schema2 : schemas2) {
            final XMPSchema schema3 = this.getSchemaByClass(schema2.getClass());
            if (schema3 == null) {
                final Element rdf = this.getRDFElement();
                rdf.appendChild(this.xmpDocument.importNode(schema2.getElement(), true));
            }
            else {
                schema3.merge(schema2);
            }
        }
    }
}
