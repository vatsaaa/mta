// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.jempbox.xmp;

import org.apache.jempbox.impl.XMLUtil;
import org.w3c.dom.Element;

public class ResourceRef implements Elementable
{
    protected Element parent;
    
    public ResourceRef(final Element parentElement) {
        this.parent = null;
        this.parent = parentElement;
        if (!this.parent.hasAttribute("xmlns:stRef")) {
            this.parent.setAttributeNS("http://ns.adobe.com/xap/1.0/sType/ResourceRef#", "xmlns:stRef", "http://ns.adobe.com/xap/1.0/sType/ResourceRef#");
        }
    }
    
    public Element getElement() {
        return this.parent;
    }
    
    public String getInstanceID() {
        return XMLUtil.getStringValue(this.parent, "stRef:instanceID");
    }
    
    public void setInstanceID(final String id) {
        XMLUtil.setStringValue(this.parent, "stRef:instanceID", id);
    }
    
    public String getDocumentID() {
        return XMLUtil.getStringValue(this.parent, "stRef:documentID");
    }
    
    public void setDocumentID(final String id) {
        XMLUtil.setStringValue(this.parent, "stRef:documentID", id);
    }
    
    public String getVersionID() {
        return XMLUtil.getStringValue(this.parent, "stRef:versionID");
    }
    
    public void setVersionID(final String id) {
        XMLUtil.setStringValue(this.parent, "stRef:veresionID", id);
    }
    
    public String getRenditionClass() {
        return XMLUtil.getStringValue(this.parent, "stRef:renditionClass");
    }
    
    public void setRenditionClass(final String renditionClass) {
        XMLUtil.setStringValue(this.parent, "stRef:renditionClass", renditionClass);
    }
    
    public String getRenditionParams() {
        return XMLUtil.getStringValue(this.parent, "stRef:renditionParams");
    }
    
    public void setRenditionParams(final String params) {
        XMLUtil.setStringValue(this.parent, "stRef:renditionParams", params);
    }
    
    public String getManager() {
        return XMLUtil.getStringValue(this.parent, "stRef:manager");
    }
    
    public void setMangager(final String manager) {
        XMLUtil.setStringValue(this.parent, "stRef:manager", manager);
    }
    
    public String getManagerVariant() {
        return XMLUtil.getStringValue(this.parent, "stRef:managerVariant");
    }
    
    public void setMangagerVariant(final String managerVariant) {
        XMLUtil.setStringValue(this.parent, "stRef:managerVariant", managerVariant);
    }
    
    public String getManagerTo() {
        return XMLUtil.getStringValue(this.parent, "stRef:managerTo");
    }
    
    public void setMangagerTo(final String managerTo) {
        XMLUtil.setStringValue(this.parent, "stRef:managerTo", managerTo);
    }
    
    public String getManagerUI() {
        return XMLUtil.getStringValue(this.parent, "stRef:managerUI");
    }
    
    public void setMangagerUI(final String managerUI) {
        XMLUtil.setStringValue(this.parent, "stRef:managerUI", managerUI);
    }
}
