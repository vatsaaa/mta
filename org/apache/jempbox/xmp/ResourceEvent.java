// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.jempbox.xmp;

import java.io.IOException;
import org.apache.jempbox.impl.DateConverter;
import java.util.Calendar;
import org.apache.jempbox.impl.XMLUtil;
import org.w3c.dom.Element;

public class ResourceEvent implements Elementable
{
    public static final String NAMESPACE = "http://ns.adobe.com/xap/1.0/sType/ResourceEvent#";
    public static final String ACTION_CONVERTED = "converted";
    public static final String ACTION_COPIED = "copied";
    public static final String ACTION_CREATED = "created";
    public static final String ACTION_CROPPED = "cropped";
    public static final String ACTION_EDITED = "edited";
    public static final String ACTION_FILTERED = "filtered";
    public static final String ACTION_FORMATTED = "formatted";
    public static final String ACTION_VERSION_UPDATED = "version_updated";
    public static final String ACTION_PRINTED = "printed";
    public static final String ACTION_PUBLISHED = "published";
    public static final String ACTION_MANAGED = "managed";
    public static final String ACTION_PRODUCED = "produced";
    public static final String ACTION_RESIZED = "resized";
    protected Element parent;
    
    public ResourceEvent(final Element parentElement) {
        this.parent = null;
        this.parent = parentElement;
        if (!this.parent.hasAttribute("xmlns:stEvt")) {
            this.parent.setAttributeNS("http://www.w3.org/2000/xmlns/", "xmlns:stEvt", "http://ns.adobe.com/xap/1.0/sType/ResourceEvent#");
        }
    }
    
    public ResourceEvent(final XMPSchema schema) {
        this.parent = null;
        (this.parent = schema.getElement().getOwnerDocument().createElement("rdf:li")).setAttributeNS("http://www.w3.org/2000/xmlns/", "xmlns:stEvt", "http://ns.adobe.com/xap/1.0/sType/ResourceEvent#");
    }
    
    public Element getElement() {
        return this.parent;
    }
    
    public String getAction() {
        return XMLUtil.getStringValue(this.parent, "stEvt:action");
    }
    
    public void setAction(final String action) {
        XMLUtil.setStringValue(this.parent, "stEvt:action", action);
    }
    
    public String getInstanceID() {
        return XMLUtil.getStringValue(this.parent, "stEvt:instanceID");
    }
    
    public void setInstanceID(final String id) {
        XMLUtil.setStringValue(this.parent, "stEvt:instanceID", id);
    }
    
    public String getParameters() {
        return XMLUtil.getStringValue(this.parent, "stEvt:parameters");
    }
    
    public void setParameters(final String param) {
        XMLUtil.setStringValue(this.parent, "stEvt:parameters", param);
    }
    
    public String getSoftwareAgent() {
        return XMLUtil.getStringValue(this.parent, "stEvt:softwareAgent");
    }
    
    public void setSoftwareAgent(final String software) {
        XMLUtil.setStringValue(this.parent, "stEvt:softwareAgent", software);
    }
    
    public Calendar getWhen() throws IOException {
        return DateConverter.toCalendar(XMLUtil.getStringValue(this.parent, "stEvt:when"));
    }
    
    public void setWhen(final Calendar when) {
        XMLUtil.setStringValue(this.parent, "stEvt:when", DateConverter.toISO8601(when));
    }
    
    public String getManager() {
        return XMLUtil.getStringValue(this.parent, "stRef:manager");
    }
    
    public void setMangager(final String manager) {
        XMLUtil.setStringValue(this.parent, "stRef:manager", manager);
    }
    
    public String getManagerVariant() {
        return XMLUtil.getStringValue(this.parent, "stRef:managerVariant");
    }
    
    public void setMangagerVariant(final String managerVariant) {
        XMLUtil.setStringValue(this.parent, "stRef:managerVariant", managerVariant);
    }
    
    public String getManagerTo() {
        return XMLUtil.getStringValue(this.parent, "stRef:managerTo");
    }
    
    public void setMangagerTo(final String managerTo) {
        XMLUtil.setStringValue(this.parent, "stRef:managerTo", managerTo);
    }
    
    public String getManagerUI() {
        return XMLUtil.getStringValue(this.parent, "stRef:managerUI");
    }
    
    public void setMangagerUI(final String managerUI) {
        XMLUtil.setStringValue(this.parent, "stRef:managerUI", managerUI);
    }
}
