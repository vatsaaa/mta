// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.jempbox.xmp;

import org.w3c.dom.Element;

public class XMPSchemaPagedText extends XMPSchema
{
    public static final String NAMESPACE = "http://ns.adobe.com/xap/1.0/t/pg/";
    
    public XMPSchemaPagedText(final XMPMetadata parent) {
        super(parent, "xmpTPg", "http://ns.adobe.com/xap/1.0/t/pg/");
    }
    
    public XMPSchemaPagedText(final Element element, final String prefix) {
        super(element, prefix);
    }
}
