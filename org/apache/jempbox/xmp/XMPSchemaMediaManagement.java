// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.jempbox.xmp;

import java.util.List;
import org.apache.jempbox.impl.XMLUtil;
import org.w3c.dom.NodeList;
import org.w3c.dom.Element;

public class XMPSchemaMediaManagement extends XMPSchema
{
    public static final String NAMESPACE = "http://ns.adobe.com/xap/1.0/mm/";
    
    public XMPSchemaMediaManagement(final XMPMetadata parent) {
        super(parent, "xmpMM", "http://ns.adobe.com/xap/1.0/mm/");
    }
    
    public XMPSchemaMediaManagement(final Element element, final String prefix) {
        super(element, prefix);
    }
    
    public ResourceRef getDerivedFrom() {
        ResourceRef retval = null;
        final NodeList nodes = this.schema.getElementsByTagName(this.prefix + ":DerivedFrom");
        if (nodes.getLength() > 0) {
            final Element derived = (Element)nodes.item(0);
            retval = new ResourceRef(derived);
        }
        else {
            final NodeList deprecatedNodes = this.schema.getElementsByTagName("xmpMM:RenditionOf");
            if (deprecatedNodes.getLength() > 0) {
                final Element derived2 = (Element)deprecatedNodes.item(0);
                retval = new ResourceRef(derived2);
            }
        }
        return retval;
    }
    
    public ResourceRef createDerivedFrom() {
        final Element node = this.schema.getOwnerDocument().createElement(this.prefix + ":DerivedFrom");
        final ResourceRef ref = new ResourceRef(node);
        return ref;
    }
    
    public void setDerivedFrom(final ResourceRef resource) {
        XMLUtil.setElementableValue(this.schema, this.prefix + ":DerivedFrom", resource);
    }
    
    public void setDocumentID(final String id) {
        this.setTextProperty(this.prefix + ":DocumentID", id);
    }
    
    public String getDocumentID() {
        return this.getTextProperty(this.prefix + ":DocumentID");
    }
    
    public void setVersionID(final String id) {
        this.setTextProperty(this.prefix + ":VersionID", id);
    }
    
    public String getVersionID() {
        return this.getTextProperty(this.prefix + ":VersionID");
    }
    
    public List<ResourceEvent> getHistory() {
        return this.getEventSequenceList(this.prefix + ":History");
    }
    
    public void removeHistory(final ResourceEvent event) {
        this.removeSequenceValue(this.prefix + ":History", event);
    }
    
    public void addHistory(final ResourceEvent event) {
        this.addSequenceValue(this.prefix + ":History", event);
    }
    
    public ResourceRef getManagedFrom() {
        ResourceRef retval = null;
        final NodeList nodes = this.schema.getElementsByTagName(this.prefix + ":ManagedFrom");
        if (nodes.getLength() > 0) {
            final Element derived = (Element)nodes.item(0);
            retval = new ResourceRef(derived);
        }
        return retval;
    }
    
    public ResourceRef createManagedFrom() {
        final Element node = this.schema.getOwnerDocument().createElement(this.prefix + ":ManagedFrom");
        final ResourceRef ref = new ResourceRef(node);
        return ref;
    }
    
    public void setManagedFrom(final ResourceRef resource) {
        XMLUtil.setElementableValue(this.schema, this.prefix + ":ManagedFrom", resource);
    }
    
    public void setManager(final String manager) {
        this.setTextProperty(this.prefix + ":Manager", manager);
    }
    
    public String getManager() {
        return this.getTextProperty(this.prefix + ":Manager");
    }
    
    public void setManageTo(final String uri) {
        this.setTextProperty(this.prefix + ":ManageTo", uri);
    }
    
    public String getManageTo() {
        return this.getTextProperty(this.prefix + ":ManageTo");
    }
    
    public void setManageUI(final String uri) {
        this.setTextProperty(this.prefix + ":ManageUI", uri);
    }
    
    public String getManageUI() {
        return this.getTextProperty(this.prefix + ":ManageUI");
    }
}
