// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.jempbox.xmp;

import java.io.IOException;
import java.util.Calendar;
import java.util.List;
import org.w3c.dom.Element;

public class XMPSchemaDublinCore extends XMPSchema
{
    public static final String NAMESPACE = "http://purl.org/dc/elements/1.1/";
    
    public XMPSchemaDublinCore(final XMPMetadata parent) {
        super(parent, "dc", "http://purl.org/dc/elements/1.1/");
    }
    
    public XMPSchemaDublinCore(final Element element, final String prefix) {
        super(element, prefix);
    }
    
    public void removeContributor(final String contributor) {
        this.removeBagValue(this.prefix + ":contributor", contributor);
    }
    
    public void addContributor(final String contributor) {
        this.addBagValue(this.prefix + ":contributor", contributor);
    }
    
    public List<String> getContributors() {
        return this.getBagList(this.prefix + ":contributor");
    }
    
    public void setCoverage(final String coverage) {
        this.setTextProperty(this.prefix + ":coverage", coverage);
    }
    
    public String getCoverage() {
        return this.getTextProperty(this.prefix + ":coverage");
    }
    
    public void removeCreator(final String creator) {
        this.removeSequenceValue(this.prefix + ":creator", creator);
    }
    
    public void addCreator(final String creator) {
        this.addSequenceValue(this.prefix + ":creator", creator);
    }
    
    public List<String> getCreators() {
        return this.getSequenceList(this.prefix + ":creator");
    }
    
    public void removeDate(final Calendar date) {
        this.removeSequenceDateValue(this.prefix + ":date", date);
    }
    
    public void addDate(final Calendar date) {
        this.addSequenceDateValue(this.prefix + ":date", date);
    }
    
    public List<Calendar> getDates() throws IOException {
        return this.getSequenceDateList(this.prefix + ":date");
    }
    
    public void setDescription(final String description) {
        this.setLanguageProperty(this.prefix + ":description", null, description);
    }
    
    public String getDescription() {
        return this.getLanguageProperty(this.prefix + ":description", null);
    }
    
    public void setDescription(final String language, final String description) {
        this.setLanguageProperty(this.prefix + ":description", language, description);
    }
    
    public String getDescription(final String language) {
        return this.getLanguageProperty(this.prefix + ":description", language);
    }
    
    public List<String> getDescriptionLanguages() {
        return this.getLanguagePropertyLanguages(this.prefix + ":description");
    }
    
    public void setFormat(final String format) {
        this.setTextProperty(this.prefix + ":format", format);
    }
    
    public String getFormat() {
        return this.getTextProperty(this.prefix + ":format");
    }
    
    public void setIdentifier(final String id) {
        this.setTextProperty(this.prefix + ":identifier", id);
    }
    
    public String getIdentifier() {
        return this.getTextProperty(this.prefix + ":identifier");
    }
    
    public void removeLanguage(final String language) {
        this.removeBagValue(this.prefix + ":language", language);
    }
    
    public void addLanguage(final String language) {
        this.addBagValue(this.prefix + ":language", language);
    }
    
    public List<String> getLanguages() {
        return this.getBagList(this.prefix + ":language");
    }
    
    public void removePublisher(final String publisher) {
        this.removeBagValue(this.prefix + ":publisher", publisher);
    }
    
    public void addPublisher(final String publisher) {
        this.addBagValue(this.prefix + ":publisher", publisher);
    }
    
    public List<String> getPublishers() {
        return this.getBagList(this.prefix + ":publisher");
    }
    
    public void removeRelation(final String relation) {
        this.removeBagValue(this.prefix + ":relation", relation);
    }
    
    public void addRelation(final String relation) {
        this.addBagValue(this.prefix + ":relation", relation);
    }
    
    public List<String> getRelationships() {
        return this.getBagList(this.prefix + ":relation");
    }
    
    public void setRights(final String rights) {
        this.setLanguageProperty(this.prefix + ":rights", null, rights);
    }
    
    public String getRights() {
        return this.getLanguageProperty(this.prefix + ":rights", null);
    }
    
    public void setRights(final String language, final String rights) {
        this.setLanguageProperty(this.prefix + ":rights", language, rights);
    }
    
    public String getRights(final String language) {
        return this.getLanguageProperty(this.prefix + ":rights", language);
    }
    
    public List<String> getRightsLanguages() {
        return this.getLanguagePropertyLanguages(this.prefix + ":rights");
    }
    
    public void setSource(final String id) {
        this.setTextProperty(this.prefix + ":source", id);
    }
    
    public String getSource() {
        return this.getTextProperty(this.prefix + ":source");
    }
    
    public void removeSubject(final String subject) {
        this.removeBagValue(this.prefix + ":subject", subject);
    }
    
    public void addSubject(final String subject) {
        this.addBagValue(this.prefix + ":subject", subject);
    }
    
    public List<String> getSubjects() {
        return this.getBagList(this.prefix + ":subject");
    }
    
    public void setTitle(final String title) {
        this.setLanguageProperty(this.prefix + ":title", null, title);
    }
    
    public String getTitle() {
        return this.getLanguageProperty(this.prefix + ":title", null);
    }
    
    public void setTitle(final String language, final String title) {
        this.setLanguageProperty(this.prefix + ":title", language, title);
    }
    
    public String getTitle(final String language) {
        return this.getLanguageProperty(this.prefix + ":title", language);
    }
    
    public List<String> getTitleLanguages() {
        return this.getLanguagePropertyLanguages(this.prefix + ":title");
    }
    
    public void addType(final String type) {
        this.addBagValue(this.prefix + ":type", type);
    }
    
    public List<String> getTypes() {
        return this.getBagList(this.prefix + ":type");
    }
}
