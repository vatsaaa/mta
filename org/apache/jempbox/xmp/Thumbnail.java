// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.jempbox.xmp;

import org.apache.jempbox.impl.XMLUtil;
import org.w3c.dom.Element;

public class Thumbnail
{
    public static final String FORMAT_JPEG = "JPEG";
    protected Element parent;
    
    public Thumbnail(final XMPMetadata metadata) {
        this(metadata.xmpDocument.createElement("rdf:li"));
    }
    
    public Thumbnail(final Element parentElement) {
        this.parent = null;
        (this.parent = parentElement).setAttributeNS("http://www.w3.org/2000/xmlns/", "xmlns:xapGImg", "http://ns.adobe.com/xap/1.0/g/img/");
    }
    
    public Element getElement() {
        return this.parent;
    }
    
    public Integer getHeight() {
        return XMLUtil.getIntValue(this.parent, "xapGImg:height");
    }
    
    public void setHeight(final Integer height) {
        XMLUtil.setIntValue(this.parent, "xapGImg:height", height);
    }
    
    public Integer getWidth() {
        return XMLUtil.getIntValue(this.parent, "xapGImg:width");
    }
    
    public void setWidth(final Integer width) {
        XMLUtil.setIntValue(this.parent, "xapGImg:width", width);
    }
    
    public void setFormat(final String format) {
        XMLUtil.setStringValue(this.parent, "xapGImg:format", format);
    }
    
    public String getFormat() {
        return XMLUtil.getStringValue(this.parent, "xapGImg:format");
    }
    
    public void setImage(final String image) {
        XMLUtil.setStringValue(this.parent, "xapGImg:image", image);
    }
    
    public String getImage() {
        return XMLUtil.getStringValue(this.parent, "xapGImg:image");
    }
}
