// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.jempbox.xmp.pdfa;

import org.w3c.dom.Element;
import org.apache.jempbox.xmp.XMPMetadata;
import org.apache.jempbox.xmp.XMPSchema;

public class XMPSchemaPDFAId extends XMPSchema
{
    public static final String NAMESPACE = "http://www.aiim.org/pdfa/ns/id/";
    
    public XMPSchemaPDFAId(final XMPMetadata parent) {
        super(parent, "pdfaid", "http://www.aiim.org/pdfa/ns/id/");
    }
    
    public XMPSchemaPDFAId(final Element element, final String prefix) {
        super(element, prefix);
    }
    
    public Integer getPart() {
        return this.getIntegerProperty(this.prefix + ":part");
    }
    
    public void setPart(final Integer part) {
        this.setIntegerProperty(this.prefix + ":part", part);
    }
    
    public void setAmd(final String amd) {
        this.setTextProperty(this.prefix + ":amd", amd);
    }
    
    public String getAmd() {
        return this.getTextProperty(this.prefix + ":amd");
    }
    
    public void setConformance(final String conformance) {
        this.setTextProperty(this.prefix + ":conformance", conformance);
    }
    
    public String getConformance() {
        return this.getTextProperty(this.prefix + ":conformance");
    }
}
