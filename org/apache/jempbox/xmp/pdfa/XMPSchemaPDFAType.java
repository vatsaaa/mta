// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.jempbox.xmp.pdfa;

import org.w3c.dom.Element;
import org.apache.jempbox.xmp.XMPMetadata;
import org.apache.jempbox.xmp.XMPSchema;

public class XMPSchemaPDFAType extends XMPSchema
{
    public static final String NAMESPACE = "http://www.aiim.org/pdfa/ns/type";
    
    public XMPSchemaPDFAType(final XMPMetadata parent) {
        super(parent, "pdfaType", "http://www.aiim.org/pdfa/ns/type");
    }
    
    public XMPSchemaPDFAType(final Element element, final String prefix) {
        super(element, prefix);
    }
}
