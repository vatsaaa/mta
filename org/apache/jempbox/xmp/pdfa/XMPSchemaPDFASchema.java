// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.jempbox.xmp.pdfa;

import org.w3c.dom.Element;
import org.apache.jempbox.xmp.XMPMetadata;
import org.apache.jempbox.xmp.XMPSchema;

public class XMPSchemaPDFASchema extends XMPSchema
{
    public static final String NAMESPACE = "http://www.aiim.org/pdfa/ns/schema";
    
    public XMPSchemaPDFASchema(final XMPMetadata parent) {
        super(parent, "pdfaSchema", "http://www.aiim.org/pdfa/ns/schema");
    }
    
    public XMPSchemaPDFASchema(final Element element, final String prefix) {
        super(element, prefix);
    }
    
    public void setSchema(final String schema) {
        this.setTextProperty("pdfaSchema:schema", schema);
    }
    
    public String getSchema() {
        return this.getTextProperty("pdfaSchema:schema");
    }
    
    public void setPrefix(final String prefix) {
        this.setTextProperty("pdfaSchema:prefix", prefix);
    }
    
    public String getPrefix() {
        return this.getTextProperty("pdfaSchema:prefix");
    }
}
