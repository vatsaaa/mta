// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.jempbox.xmp.pdfa;

import org.apache.jempbox.xmp.XMPSchema;
import org.apache.jempbox.impl.XMLUtil;
import org.xml.sax.InputSource;
import org.w3c.dom.Document;
import java.io.IOException;
import org.apache.jempbox.xmp.XMPMetadata;

public class XMPMetadataPDFA extends XMPMetadata
{
    public XMPMetadataPDFA() throws IOException {
        this.init();
    }
    
    public XMPMetadataPDFA(final Document doc) {
        super(doc);
        this.init();
    }
    
    private void init() {
        this.nsMappings.put("http://www.aiim.org/pdfa/ns/field", XMPSchemaPDFAField.class);
        this.nsMappings.put("http://www.aiim.org/pdfa/ns/id/", XMPSchemaPDFAId.class);
        this.nsMappings.put("http://www.aiim.org/pdfa/ns/property", XMPSchemaPDFAProperty.class);
        this.nsMappings.put("http://www.aiim.org/pdfa/ns/schema", XMPSchemaPDFASchema.class);
        this.nsMappings.put("http://www.aiim.org/pdfa/ns/type", XMPSchemaPDFAType.class);
    }
    
    public static XMPMetadata load(final InputSource is) throws IOException {
        return new XMPMetadataPDFA(XMLUtil.parse(is));
    }
    
    public XMPSchemaPDFAField getPDFAFieldSchema() throws IOException {
        return (XMPSchemaPDFAField)this.getSchemaByClass(XMPSchemaPDFAField.class);
    }
    
    public XMPSchemaPDFAField addPDFAFieldSchema() {
        final XMPSchemaPDFAField schema = new XMPSchemaPDFAField(this);
        return (XMPSchemaPDFAField)this.basicAddSchema(schema);
    }
    
    public XMPSchemaPDFAId getPDFAIdSchema() throws IOException {
        return (XMPSchemaPDFAId)this.getSchemaByClass(XMPSchemaPDFAId.class);
    }
    
    public XMPSchemaPDFAId addPDFAIdSchema() {
        final XMPSchemaPDFAId schema = new XMPSchemaPDFAId(this);
        return (XMPSchemaPDFAId)this.basicAddSchema(schema);
    }
    
    public XMPSchemaPDFAProperty getPDFAPropertySchema() throws IOException {
        return (XMPSchemaPDFAProperty)this.getSchemaByClass(XMPSchemaPDFAProperty.class);
    }
    
    public XMPSchemaPDFAProperty addPDFAPropertySchema() {
        final XMPSchemaPDFAProperty schema = new XMPSchemaPDFAProperty(this);
        return (XMPSchemaPDFAProperty)this.basicAddSchema(schema);
    }
    
    public XMPSchemaPDFASchema getPDFASchema() throws IOException {
        return (XMPSchemaPDFASchema)this.getSchemaByClass(XMPSchemaPDFASchema.class);
    }
    
    public XMPSchemaPDFASchema addPDFASchema() {
        final XMPSchemaPDFASchema schema = new XMPSchemaPDFASchema(this);
        return (XMPSchemaPDFASchema)this.basicAddSchema(schema);
    }
    
    public XMPSchemaPDFAType getPDFATypeSchema() throws IOException {
        return (XMPSchemaPDFAType)this.getSchemaByClass(XMPSchemaPDFAType.class);
    }
    
    public XMPSchemaPDFAType addPDFATypeSchema() {
        final XMPSchemaPDFAType schema = new XMPSchemaPDFAType(this);
        return (XMPSchemaPDFAType)this.basicAddSchema(schema);
    }
}
