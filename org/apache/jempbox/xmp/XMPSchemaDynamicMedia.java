// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.jempbox.xmp;

import org.w3c.dom.Element;

public class XMPSchemaDynamicMedia extends XMPSchema
{
    public static final String NAMESPACE = "http://ns.adobe.com/xmp/1.0/DynamicMedia/";
    
    public XMPSchemaDynamicMedia(final XMPMetadata parent) {
        super(parent, "xmpDM", "http://ns.adobe.com/xmp/1.0/DynamicMedia/");
    }
    
    public XMPSchemaDynamicMedia(final Element element, final String prefix) {
        super(element, prefix);
    }
}
