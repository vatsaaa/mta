// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.jempbox.xmp;

import java.util.List;
import org.w3c.dom.Element;

public class XMPSchemaPhotoshop extends XMPSchema
{
    public static final String NAMESPACE = "http://ns.adobe.com/photoshop/1.0/";
    
    public XMPSchemaPhotoshop(final XMPMetadata parent) {
        super(parent, "photoshop", "http://ns.adobe.com/photoshop/1.0/");
    }
    
    public XMPSchemaPhotoshop(final Element element, final String aPrefix) {
        super(element, aPrefix);
    }
    
    public void setAuthorsPosition(final String s) {
        this.setTextProperty(this.prefix + ":AuthorsPosition", s);
    }
    
    public String getAuthorsPosition() {
        return this.getTextProperty(this.prefix + ":AuthorsPosition");
    }
    
    public void setCaptionWriter(final String s) {
        this.setTextProperty(this.prefix + ":CaptionWriter", s);
    }
    
    public String getCaptionWriter() {
        return this.getTextProperty(this.prefix + ":CaptionWriter");
    }
    
    public void setCategory(final String s) {
        if (s != null && s.length() > 3) {
            throw new RuntimeException("Error: photoshop:Category is limited to three characters value='" + s + "'");
        }
        this.setTextProperty(this.prefix + ":Category", s);
    }
    
    public String getCategory() {
        return this.getTextProperty(this.prefix + ":Category");
    }
    
    public void setCity(final String s) {
        this.setTextProperty(this.prefix + ":City", s);
    }
    
    public String getCity() {
        return this.getTextProperty(this.prefix + ":City");
    }
    
    public void setCountry(final String s) {
        this.setTextProperty(this.prefix + ":Country", s);
    }
    
    public String getCountry() {
        return this.getTextProperty(this.prefix + ":Country");
    }
    
    public void setCredit(final String s) {
        this.setTextProperty(this.prefix + ":Credit", s);
    }
    
    public String getCredit() {
        return this.getTextProperty(this.prefix + ":Credit");
    }
    
    public void setDateCreated(final String s) {
        this.setTextProperty(this.prefix + ":DateCreated", s);
    }
    
    public String getDateCreated() {
        return this.getTextProperty(this.prefix + ":DateCreated");
    }
    
    public void setHeadline(final String s) {
        this.setTextProperty(this.prefix + ":Headline", s);
    }
    
    public String getHeadline() {
        return this.getTextProperty(this.prefix + ":Headline");
    }
    
    public void setInstructions(final String s) {
        this.setTextProperty(this.prefix + ":Instructions", s);
    }
    
    public String getInstructions() {
        return this.getTextProperty(this.prefix + ":Instructions");
    }
    
    public void setSource(final String s) {
        this.setTextProperty(this.prefix + ":Source", s);
    }
    
    public String getSource() {
        return this.getTextProperty(this.prefix + ":Source");
    }
    
    public void setState(final String s) {
        this.setTextProperty(this.prefix + ":State", s);
    }
    
    public String getState() {
        return this.getTextProperty(this.prefix + ":State");
    }
    
    public void addSupplementalCategory(final String s) {
        this.addBagValue(this.prefix + ":SupplementalCategories", s);
    }
    
    public List<String> getSupplementalCategories() {
        return this.getBagList(this.prefix + ":SupplementalCategories");
    }
    
    public void removeSupplementalCategory(final String s) {
        this.removeBagValue(this.prefix + ":SupplementalCategories", s);
    }
    
    public void setTransmissionReference(final String s) {
        this.setTextProperty(this.prefix + ":TransmissionReference", s);
    }
    
    public String getTransmissionReference() {
        return this.getTextProperty(this.prefix + ":TransmissionReference");
    }
    
    public void setUrgency(final Integer s) {
        if (s != null && (s < 1 || s > 8)) {
            throw new RuntimeException("Error: photoshop:Urgency must be between 1 and 8.  value=" + s);
        }
        this.setIntegerProperty(this.prefix + ":Urgency", s);
    }
    
    public Integer getUrgency() {
        return this.getIntegerProperty(this.prefix + ":Urgency");
    }
}
