// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.jempbox.xmp;

import java.util.Iterator;
import org.w3c.dom.NamedNodeMap;
import java.util.Collections;
import java.util.ArrayList;
import java.util.List;
import java.io.IOException;
import org.apache.jempbox.impl.DateConverter;
import java.util.Calendar;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.apache.jempbox.impl.XMLUtil;
import org.w3c.dom.Element;

public class XMPSchema
{
    public static final String NS_NAMESPACE = "http://www.w3.org/2000/xmlns/";
    protected String prefix;
    protected Element schema;
    
    public XMPSchema(final XMPMetadata parent, final String namespaceName, final String namespaceURI) {
        this.schema = null;
        this.schema = parent.xmpDocument.createElementNS("http://www.w3.org/1999/02/22-rdf-syntax-ns#", "rdf:Description");
        this.prefix = namespaceName;
        this.schema.setAttributeNS("http://www.w3.org/2000/xmlns/", "xmlns:" + namespaceName, namespaceURI);
    }
    
    public XMPSchema(final Element element, final String aPrefix) {
        this.schema = null;
        this.schema = element;
        if (aPrefix != null) {
            this.prefix = aPrefix;
        }
        else {
            this.prefix = "";
        }
    }
    
    public Element getElement() {
        return this.schema;
    }
    
    public String getAbout() {
        return this.getTextProperty("rdf:about");
    }
    
    public void setAbout(final String about) {
        if (about == null) {
            this.schema.removeAttribute("rdf:about");
        }
        else {
            this.schema.setAttribute("rdf:about", about);
        }
    }
    
    public void setTextProperty(final String propertyName, final String propertyValue) {
        if (propertyValue == null) {
            this.schema.removeAttribute(propertyName);
            final NodeList keywordList = this.schema.getElementsByTagName(propertyName);
            for (int i = 0; i < keywordList.getLength(); ++i) {
                this.schema.removeChild(keywordList.item(i));
            }
        }
        else if (this.schema.hasAttribute(propertyName)) {
            this.schema.setAttribute(propertyName, propertyValue);
        }
        else if (this.schema.hasChildNodes()) {
            final NodeList nodeList = this.schema.getElementsByTagName(propertyName);
            if (nodeList.getLength() > 0) {
                final Element node = (Element)nodeList.item(0);
                node.setNodeValue(propertyValue);
            }
            else {
                final Element textNode = this.schema.getOwnerDocument().createElement(propertyName);
                XMLUtil.setStringValue(textNode, propertyValue);
                this.schema.appendChild(textNode);
            }
        }
        else {
            this.schema.setAttribute(propertyName, propertyValue);
        }
    }
    
    public String getTextProperty(final String propertyName) {
        if (this.schema.hasAttribute(propertyName)) {
            return this.schema.getAttribute(propertyName);
        }
        final NodeList nodes = this.schema.getElementsByTagName(propertyName);
        if (nodes.getLength() > 0) {
            final Element node = (Element)nodes.item(0);
            return XMLUtil.getStringValue(node);
        }
        return null;
    }
    
    public Calendar getDateProperty(final String propertyName) throws IOException {
        return DateConverter.toCalendar(this.getTextProperty(propertyName));
    }
    
    public void setDateProperty(final String propertyName, final Calendar date) {
        this.setTextProperty(propertyName, DateConverter.toISO8601(date));
    }
    
    public Boolean getBooleanProperty(final String propertyName) {
        Boolean value = null;
        final String stringValue = this.getTextProperty(propertyName);
        if (stringValue != null) {
            value = (stringValue.equals("True") ? Boolean.TRUE : Boolean.FALSE);
        }
        return value;
    }
    
    public void setBooleanProperty(final String propertyName, final Boolean bool) {
        String value = null;
        if (bool != null) {
            value = (bool ? "True" : "False");
        }
        this.setTextProperty(propertyName, value);
    }
    
    public Integer getIntegerProperty(final String propertyName) {
        Integer retval = null;
        final String intProperty = this.getTextProperty(propertyName);
        if (intProperty != null && intProperty.length() > 0) {
            retval = new Integer(intProperty);
        }
        return retval;
    }
    
    public void setIntegerProperty(final String propertyName, final Integer intValue) {
        String textValue = null;
        if (intValue != null) {
            textValue = intValue.toString();
        }
        this.setTextProperty(propertyName, textValue);
    }
    
    public void removeBagValue(final String bagName, final String bagValue) {
        Element bagElement = null;
        final NodeList nodes = this.schema.getElementsByTagName(bagName);
        if (nodes.getLength() > 0) {
            final Element contElement = (Element)nodes.item(0);
            final NodeList bagList = contElement.getElementsByTagName("rdf:Bag");
            if (bagList.getLength() > 0) {
                bagElement = (Element)bagList.item(0);
                final NodeList items = bagElement.getElementsByTagName("rdf:li");
                for (int i = items.getLength() - 1; i >= 0; --i) {
                    final Element li = (Element)items.item(i);
                    final String value = XMLUtil.getStringValue(li);
                    if (value.equals(bagValue)) {
                        bagElement.removeChild(li);
                    }
                }
            }
        }
    }
    
    public void addBagValue(final String bagName, final String bagValue) {
        Element bagElement = null;
        final NodeList nodes = this.schema.getElementsByTagName(bagName);
        if (nodes.getLength() > 0) {
            final Element contElement = (Element)nodes.item(0);
            final NodeList bagList = contElement.getElementsByTagName("rdf:Bag");
            if (bagList.getLength() > 0) {
                bagElement = (Element)bagList.item(0);
            }
        }
        else {
            final Element contElement = this.schema.getOwnerDocument().createElement(bagName);
            this.schema.appendChild(contElement);
            bagElement = this.schema.getOwnerDocument().createElement("rdf:Bag");
            contElement.appendChild(bagElement);
        }
        final Element liElement = this.schema.getOwnerDocument().createElement("rdf:li");
        XMLUtil.setStringValue(liElement, bagValue);
        bagElement.appendChild(liElement);
    }
    
    public List<String> getBagList(final String bagName) {
        List<String> retval = null;
        final NodeList nodes = this.schema.getElementsByTagName(bagName);
        if (nodes.getLength() > 0) {
            final Element contributor = (Element)nodes.item(0);
            final NodeList bagList = contributor.getElementsByTagName("rdf:Bag");
            if (bagList.getLength() > 0) {
                final Element bag = (Element)bagList.item(0);
                retval = new ArrayList<String>();
                final NodeList items = bag.getElementsByTagName("rdf:li");
                for (int i = 0; i < items.getLength(); ++i) {
                    final Element li = (Element)items.item(i);
                    retval.add(XMLUtil.getStringValue(li));
                }
                retval = Collections.unmodifiableList((List<? extends String>)retval);
            }
        }
        return retval;
    }
    
    public void removeSequenceValue(final String seqName, final String seqValue) {
        Element bagElement = null;
        final NodeList nodes = this.schema.getElementsByTagName(seqName);
        if (nodes.getLength() > 0) {
            final Element contElement = (Element)nodes.item(0);
            final NodeList bagList = contElement.getElementsByTagName("rdf:Seq");
            if (bagList.getLength() > 0) {
                bagElement = (Element)bagList.item(0);
                final NodeList items = bagElement.getElementsByTagName("rdf:li");
                for (int i = items.getLength() - 1; i >= 0; --i) {
                    final Element li = (Element)items.item(i);
                    final String value = XMLUtil.getStringValue(li);
                    if (value.equals(seqValue)) {
                        bagElement.removeChild(li);
                    }
                }
            }
        }
    }
    
    public void removeSequenceValue(final String seqName, final Elementable seqValue) {
        Element bagElement = null;
        final NodeList nodes = this.schema.getElementsByTagName(seqName);
        if (nodes.getLength() > 0) {
            final Element contElement = (Element)nodes.item(0);
            final NodeList bagList = contElement.getElementsByTagName("rdf:Seq");
            if (bagList.getLength() > 0) {
                bagElement = (Element)bagList.item(0);
                final NodeList items = bagElement.getElementsByTagName("rdf:li");
                for (int i = 0; i < items.getLength(); ++i) {
                    final Element li = (Element)items.item(i);
                    if (li == seqValue.getElement()) {
                        bagElement.removeChild(li);
                    }
                }
            }
        }
    }
    
    public void addSequenceValue(final String seqName, final String seqValue) {
        Element bagElement = null;
        final NodeList nodes = this.schema.getElementsByTagName(seqName);
        if (nodes.getLength() > 0) {
            final Element contElement = (Element)nodes.item(0);
            final NodeList bagList = contElement.getElementsByTagName("rdf:Seq");
            if (bagList.getLength() > 0) {
                bagElement = (Element)bagList.item(0);
            }
            else {
                this.schema.removeChild(nodes.item(0));
            }
        }
        if (bagElement == null) {
            final Element contElement = this.schema.getOwnerDocument().createElement(seqName);
            this.schema.appendChild(contElement);
            bagElement = this.schema.getOwnerDocument().createElement("rdf:Seq");
            contElement.appendChild(bagElement);
        }
        final Element liElement = this.schema.getOwnerDocument().createElement("rdf:li");
        liElement.appendChild(this.schema.getOwnerDocument().createTextNode(seqValue));
        bagElement.appendChild(liElement);
    }
    
    public void addSequenceValue(final String seqName, final Elementable seqValue) {
        Element bagElement = null;
        final NodeList nodes = this.schema.getElementsByTagName(seqName);
        if (nodes.getLength() > 0) {
            final Element contElement = (Element)nodes.item(0);
            final NodeList bagList = contElement.getElementsByTagName("rdf:Seq");
            if (bagList.getLength() > 0) {
                bagElement = (Element)bagList.item(0);
            }
        }
        else {
            final Element contElement = this.schema.getOwnerDocument().createElement(seqName);
            this.schema.appendChild(contElement);
            bagElement = this.schema.getOwnerDocument().createElement("rdf:Seq");
            contElement.appendChild(bagElement);
        }
        bagElement.appendChild(seqValue.getElement());
    }
    
    public List<String> getSequenceList(final String seqName) {
        List<String> retval = null;
        final NodeList nodes = this.schema.getElementsByTagName(seqName);
        if (nodes.getLength() > 0) {
            final Element contributor = (Element)nodes.item(0);
            final NodeList bagList = contributor.getElementsByTagName("rdf:Seq");
            if (bagList.getLength() > 0) {
                final Element bag = (Element)bagList.item(0);
                retval = new ArrayList<String>();
                final NodeList items = bag.getElementsByTagName("rdf:li");
                for (int i = 0; i < items.getLength(); ++i) {
                    final Element li = (Element)items.item(i);
                    retval.add(XMLUtil.getStringValue(li));
                }
                retval = Collections.unmodifiableList((List<? extends String>)retval);
            }
        }
        return retval;
    }
    
    public List<ResourceEvent> getEventSequenceList(final String seqName) {
        List<ResourceEvent> retval = null;
        final NodeList nodes = this.schema.getElementsByTagName(seqName);
        if (nodes.getLength() > 0) {
            final Element contributor = (Element)nodes.item(0);
            final NodeList bagList = contributor.getElementsByTagName("rdf:Seq");
            if (bagList.getLength() > 0) {
                final Element bag = (Element)bagList.item(0);
                retval = new ArrayList<ResourceEvent>();
                final NodeList items = bag.getElementsByTagName("rdf:li");
                for (int i = 0; i < items.getLength(); ++i) {
                    final Element li = (Element)items.item(i);
                    retval.add(new ResourceEvent(li));
                }
                retval = Collections.unmodifiableList((List<? extends ResourceEvent>)retval);
            }
        }
        return retval;
    }
    
    public void removeSequenceDateValue(final String seqName, final Calendar date) {
        final String dateAsString = DateConverter.toISO8601(date);
        this.removeSequenceValue(seqName, dateAsString);
    }
    
    public void addSequenceDateValue(final String seqName, final Calendar date) {
        final String dateAsString = DateConverter.toISO8601(date);
        this.addSequenceValue(seqName, dateAsString);
    }
    
    public List<Calendar> getSequenceDateList(final String seqName) throws IOException {
        final List<String> strings = this.getSequenceList(seqName);
        List<Calendar> retval = null;
        if (strings != null) {
            retval = new ArrayList<Calendar>();
            for (int i = 0; i < strings.size(); ++i) {
                retval.add(DateConverter.toCalendar(strings.get(i)));
            }
        }
        return retval;
    }
    
    public void setLanguageProperty(final String propertyName, String language, final String value) {
        final NodeList nodes = this.schema.getElementsByTagName(propertyName);
        Element property = null;
        if (nodes.getLength() == 0) {
            if (value == null) {
                return;
            }
            property = this.schema.getOwnerDocument().createElement(propertyName);
            this.schema.appendChild(property);
        }
        else {
            property = (Element)nodes.item(0);
        }
        Element alt = null;
        final NodeList altList = property.getElementsByTagName("rdf:Alt");
        if (altList.getLength() == 0) {
            if (value == null) {
                return;
            }
            alt = this.schema.getOwnerDocument().createElement("rdf:Alt");
            property.appendChild(alt);
        }
        else {
            alt = (Element)altList.item(0);
        }
        final NodeList items = alt.getElementsByTagName("rdf:li");
        if (language == null) {
            language = "x-default";
        }
        boolean foundValue = false;
        for (int i = 0; i < items.getLength(); ++i) {
            final Element li = (Element)items.item(i);
            if (value == null) {
                alt.removeChild(li);
            }
            else if (language.equals(li.getAttribute("xml:lang"))) {
                foundValue = true;
                XMLUtil.setStringValue(li, value);
            }
        }
        if (value != null && !foundValue) {
            final Element li2 = this.schema.getOwnerDocument().createElement("rdf:li");
            li2.setAttribute("xml:lang", language);
            XMLUtil.setStringValue(li2, value);
            if (language.equals("x-default")) {
                alt.insertBefore(li2, alt.getFirstChild());
            }
            else {
                alt.appendChild(li2);
            }
        }
    }
    
    public String getLanguageProperty(final String propertyName, String language) {
        String retval = null;
        if (language == null) {
            language = "x-default";
        }
        final NodeList nodes = this.schema.getElementsByTagName(propertyName);
        if (nodes.getLength() > 0) {
            final Element property = (Element)nodes.item(0);
            final NodeList altList = property.getElementsByTagName("rdf:Alt");
            if (altList.getLength() > 0) {
                final Element alt = (Element)altList.item(0);
                final NodeList items = alt.getElementsByTagName("rdf:li");
                for (int i = 0; i < items.getLength() && retval == null; ++i) {
                    final Element li = (Element)items.item(i);
                    final String elementLanguage = li.getAttribute("xml:lang");
                    if (language.equals(elementLanguage)) {
                        retval = XMLUtil.getStringValue(li);
                    }
                }
            }
            else if (property.getChildNodes().getLength() == 1 && 3 == property.getFirstChild().getNodeType()) {
                retval = property.getFirstChild().getNodeValue();
            }
        }
        return retval;
    }
    
    public void setThumbnailProperty(final String propertyName, String language, final Thumbnail value) {
        final NodeList nodes = this.schema.getElementsByTagName(propertyName);
        Element property = null;
        if (nodes.getLength() == 0) {
            if (value == null) {
                return;
            }
            property = this.schema.getOwnerDocument().createElement(propertyName);
            this.schema.appendChild(property);
        }
        else {
            property = (Element)nodes.item(0);
        }
        Element alt = null;
        final NodeList altList = property.getElementsByTagName("rdf:Alt");
        if (altList.getLength() == 0) {
            if (value == null) {
                return;
            }
            alt = this.schema.getOwnerDocument().createElement("rdf:Alt");
            property.appendChild(alt);
        }
        else {
            alt = (Element)altList.item(0);
        }
        final NodeList items = alt.getElementsByTagName("rdf:li");
        if (language == null) {
            language = "x-default";
        }
        boolean foundValue = false;
        for (int i = 0; i < items.getLength(); ++i) {
            final Element li = (Element)items.item(i);
            if (value == null) {
                alt.removeChild(li);
            }
            else if (language.equals(li.getAttribute("xml:lang"))) {
                foundValue = true;
                alt.replaceChild(li, value.getElement());
            }
        }
        if (value != null && !foundValue) {
            final Element li2 = value.getElement();
            li2.setAttribute("xml:lang", language);
            if (language.equals("x-default")) {
                alt.insertBefore(li2, alt.getFirstChild());
            }
            else {
                alt.appendChild(li2);
            }
        }
    }
    
    public Thumbnail getThumbnailProperty(final String propertyName, String language) {
        Thumbnail retval = null;
        if (language == null) {
            language = "x-default";
        }
        final NodeList nodes = this.schema.getElementsByTagName(propertyName);
        if (nodes.getLength() > 0) {
            final Element property = (Element)nodes.item(0);
            final NodeList altList = property.getElementsByTagName("rdf:Alt");
            if (altList.getLength() > 0) {
                final Element alt = (Element)altList.item(0);
                final NodeList items = alt.getElementsByTagName("rdf:li");
                for (int i = 0; i < items.getLength() && retval == null; ++i) {
                    final Element li = (Element)items.item(i);
                    final String elementLanguage = li.getAttribute("xml:lang");
                    if (language.equals(elementLanguage)) {
                        retval = new Thumbnail(li);
                    }
                }
            }
        }
        return retval;
    }
    
    public List<String> getLanguagePropertyLanguages(final String propertyName) {
        final List<String> retval = new ArrayList<String>();
        final NodeList nodes = this.schema.getElementsByTagName(propertyName);
        if (nodes.getLength() > 0) {
            final Element property = (Element)nodes.item(0);
            final NodeList altList = property.getElementsByTagName("rdf:Alt");
            if (altList.getLength() > 0) {
                final Element alt = (Element)altList.item(0);
                final NodeList items = alt.getElementsByTagName("rdf:li");
                for (int i = 0; i < items.getLength(); ++i) {
                    final Element li = (Element)items.item(i);
                    final String elementLanguage = li.getAttribute("xml:lang");
                    if (elementLanguage == null) {
                        retval.add("x-default");
                    }
                    else {
                        retval.add(elementLanguage);
                    }
                }
            }
        }
        return retval;
    }
    
    public void merge(final XMPSchema xmpSchema) throws IOException {
        if (!xmpSchema.getClass().equals(this.getClass())) {
            throw new IOException("Can only merge schemas of the same type.");
        }
        final NamedNodeMap attributes = xmpSchema.getElement().getAttributes();
        for (int i = 0; i < attributes.getLength(); ++i) {
            final Node a = attributes.item(i);
            final String name = a.getNodeName();
            if (name.startsWith(this.prefix)) {
                final String newValue = xmpSchema.getTextProperty(name);
                this.setTextProperty(name, newValue);
            }
        }
        final NodeList nodes = xmpSchema.getElement().getChildNodes();
        for (int j = 0; j < nodes.getLength(); ++j) {
            final Node a2 = nodes.item(j);
            final String name2 = a2.getNodeName();
            if (name2.startsWith(this.prefix)) {
                if (a2 instanceof Element) {
                    final Element e = (Element)a2;
                    if (nodes.getLength() > 0) {
                        final NodeList seqList = e.getElementsByTagName("rdf:Seq");
                        if (seqList.getLength() > 0) {
                            final List<String> newList = xmpSchema.getSequenceList(name2);
                            final List<String> oldList = this.getSequenceList(name2);
                            for (final String object : newList) {
                                if (oldList == null || !oldList.contains(object)) {
                                    this.addSequenceValue(name2, object);
                                }
                            }
                            continue;
                        }
                        final NodeList bagList = e.getElementsByTagName("rdf:Bag");
                        if (bagList.getLength() > 0) {
                            final List<String> newList2 = xmpSchema.getBagList(name2);
                            final List<String> oldList2 = this.getBagList(name2);
                            for (final String object2 : newList2) {
                                if (oldList2 == null || !oldList2.contains(object2)) {
                                    this.addBagValue(name2, object2);
                                }
                            }
                            continue;
                        }
                    }
                }
                final String newValue2 = xmpSchema.getTextProperty(name2);
                this.setTextProperty(name2, newValue2);
            }
        }
    }
}
