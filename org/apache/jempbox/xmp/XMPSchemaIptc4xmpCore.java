// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.jempbox.xmp;

import java.util.List;
import org.w3c.dom.Element;

public class XMPSchemaIptc4xmpCore extends XMPSchema
{
    public static final String NAMESPACE = "http://iptc.org/std/Iptc4xmpCore/1.0/xmlns/";
    
    public XMPSchemaIptc4xmpCore(final XMPMetadata metadata) {
        super(metadata, "Iptc4xmpCore", "http://iptc.org/std/Iptc4xmpCore/1.0/xmlns/");
    }
    
    public XMPSchemaIptc4xmpCore(final Element element, final String aPrefix) {
        super(element, aPrefix);
    }
    
    public void setCiAdrCity(final String city) {
        this.setTextProperty(this.prefix + ":CiAdrCity", city);
    }
    
    public String getCiAdrCity() {
        return this.getTextProperty(this.prefix + ":CiAdrCity");
    }
    
    public void setCiAdrCtry(final String country) {
        this.setTextProperty(this.prefix + ":CiAdrCtry", country);
    }
    
    public String getCiAdrCtry() {
        return this.getTextProperty(this.prefix + ":CiAdrCtry");
    }
    
    public void setCiAdrExtadr(final String adr) {
        this.setTextProperty(this.prefix + ":CiAdrExtadr", adr);
    }
    
    public String getCiAdrExtadr() {
        return this.getTextProperty(this.prefix + ":CiAdrExtadr");
    }
    
    public void setCiAdrPcode(final String po) {
        this.setTextProperty(this.prefix + ":CiAdrPcode", po);
    }
    
    public String getCiAdrPcode() {
        return this.getTextProperty(this.prefix + ":CiAdrPcode");
    }
    
    public void setCiAdrRegion(final String state) {
        this.setTextProperty(this.prefix + ":CiAdrRegion", state);
    }
    
    public String getCiAdrRegion() {
        return this.getTextProperty(this.prefix + ":CiAdrRegion");
    }
    
    public void setCiEmailWork(final String email) {
        this.setTextProperty(this.prefix + ":CiEmailWork", email);
    }
    
    public String getCiEmailWork() {
        return this.getTextProperty(this.prefix + ":CiEmailWork");
    }
    
    public void setCiTelWork(final String tel) {
        this.setTextProperty(this.prefix + ":CiTelWork", tel);
    }
    
    public String getCiTelWork() {
        return this.getTextProperty(this.prefix + ":CiTelWork");
    }
    
    public void setCiUrlWork(final String url) {
        this.setTextProperty(this.prefix + ":CiUrlWork", url);
    }
    
    public String getCiUrlWork() {
        return this.getTextProperty(this.prefix + ":CiUrlWork");
    }
    
    public void setLocation(final String loc) {
        this.setTextProperty(this.prefix + ":Location", loc);
    }
    
    public String getLocation() {
        return this.getTextProperty(this.prefix + ":Location");
    }
    
    public void addScene(final String scene) {
        this.addBagValue(this.prefix + ":Scene", scene);
    }
    
    public List<String> getScenes() {
        return this.getBagList(this.prefix + ":Scene");
    }
    
    public void addSubjectCode(final String subject) {
        this.addBagValue(this.prefix + ":SubjectCode", subject);
    }
    
    public List<String> getSubjectCodes() {
        return this.getBagList(this.prefix + ":SubjectCode");
    }
    
    public void setIntellectualGenre(final String genre) {
        this.setTextProperty(this.prefix + ":IntellectualGenre", genre);
    }
    
    public String getIntellectualGenre() {
        return this.getTextProperty(this.prefix + ":IntellectualGenre");
    }
    
    public void setCountryCode(final String code) {
        this.setTextProperty(this.prefix + ":CountryCode", code);
    }
    
    public String getCountryCode() {
        return this.getTextProperty(this.prefix + ":CountryCode");
    }
}
