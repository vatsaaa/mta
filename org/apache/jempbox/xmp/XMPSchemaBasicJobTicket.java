// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.jempbox.xmp;

import org.w3c.dom.Element;

public class XMPSchemaBasicJobTicket extends XMPSchema
{
    public static final String NAMESPACE = "http://ns.adobe.com/xap/1.0/bj/";
    
    public XMPSchemaBasicJobTicket(final XMPMetadata parent) {
        super(parent, "xmpBJ", "http://ns.adobe.com/xap/1.0/bj/");
    }
    
    public XMPSchemaBasicJobTicket(final Element element, final String prefix) {
        super(element, prefix);
    }
}
