// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.jempbox.xmp;

import org.w3c.dom.Element;

public interface Elementable
{
    Element getElement();
}
