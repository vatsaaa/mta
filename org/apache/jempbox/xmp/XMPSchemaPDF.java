// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.jempbox.xmp;

import org.w3c.dom.Element;

public class XMPSchemaPDF extends XMPSchema
{
    public static final String NAMESPACE = "http://ns.adobe.com/pdf/1.3/";
    
    public XMPSchemaPDF(final XMPMetadata parent) {
        super(parent, "pdf", "http://ns.adobe.com/pdf/1.3/");
    }
    
    public XMPSchemaPDF(final Element element, final String prefix) {
        super(element, prefix);
    }
    
    public void setKeywords(final String keywords) {
        this.setTextProperty(this.prefix + ":Keywords", keywords);
    }
    
    public String getKeywords() {
        return this.getTextProperty(this.prefix + ":Keywords");
    }
    
    public void setPDFVersion(final String pdfVersion) {
        this.setTextProperty(this.prefix + ":PDFVersion", pdfVersion);
    }
    
    public String getPDFVersion() {
        return this.getTextProperty(this.prefix + ":PDFVersion");
    }
    
    public void setProducer(final String producer) {
        this.setTextProperty(this.prefix + ":Producer", producer);
    }
    
    public String getProducer() {
        return this.getTextProperty(this.prefix + ":Producer");
    }
}
