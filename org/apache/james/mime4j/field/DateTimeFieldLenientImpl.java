// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.james.mime4j.field;

import org.apache.james.mime4j.dom.field.ParsedField;
import java.util.Iterator;
import java.text.ParseException;
import java.util.TimeZone;
import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.ArrayList;
import org.apache.james.mime4j.codec.DecodeMonitor;
import java.util.Collection;
import org.apache.james.mime4j.stream.Field;
import org.apache.james.mime4j.dom.FieldParser;
import java.util.Date;
import java.util.List;
import org.apache.james.mime4j.dom.field.DateTimeField;

public class DateTimeFieldLenientImpl extends AbstractField implements DateTimeField
{
    private static final String[] DEFAULT_DATE_FORMATS;
    private final List<String> datePatterns;
    private boolean parsed;
    private Date date;
    public static final FieldParser<DateTimeField> PARSER;
    
    DateTimeFieldLenientImpl(final Field rawField, final Collection<String> dateParsers, final DecodeMonitor monitor) {
        super(rawField, monitor);
        this.parsed = false;
        this.datePatterns = new ArrayList<String>();
        if (dateParsers != null) {
            this.datePatterns.addAll(dateParsers);
        }
        else {
            for (final String pattern : DateTimeFieldLenientImpl.DEFAULT_DATE_FORMATS) {
                this.datePatterns.add(pattern);
            }
        }
    }
    
    public Date getDate() {
        if (!this.parsed) {
            this.parse();
        }
        return this.date;
    }
    
    private void parse() {
        this.parsed = true;
        this.date = null;
        final String body = this.getBody();
        for (final String datePattern : this.datePatterns) {
            try {
                final SimpleDateFormat parser = new SimpleDateFormat(datePattern, Locale.US);
                parser.setTimeZone(TimeZone.getTimeZone("GMT"));
                parser.setLenient(true);
                this.date = parser.parse(body);
            }
            catch (ParseException ignore) {
                continue;
            }
            break;
        }
    }
    
    public static FieldParser<DateTimeField> createParser(final Collection<String> dateParsers) {
        return new FieldParser<DateTimeField>() {
            public DateTimeField parse(final Field rawField, final DecodeMonitor monitor) {
                return new DateTimeFieldLenientImpl(rawField, dateParsers, monitor);
            }
        };
    }
    
    static {
        DEFAULT_DATE_FORMATS = new String[] { "EEE, dd MMM yyyy HH:mm:ss ZZZZ", "dd MMM yyyy HH:mm:ss ZZZZ" };
        PARSER = new FieldParser<DateTimeField>() {
            public DateTimeField parse(final Field rawField, final DecodeMonitor monitor) {
                return new DateTimeFieldLenientImpl(rawField, null, monitor);
            }
        };
    }
}
