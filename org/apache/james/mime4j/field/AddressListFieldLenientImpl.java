// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.james.mime4j.field;

import org.apache.james.mime4j.dom.field.ParsedField;
import org.apache.james.mime4j.util.ByteSequence;
import org.apache.james.mime4j.stream.RawField;
import org.apache.james.mime4j.field.address.LenientAddressBuilder;
import org.apache.james.mime4j.stream.ParserCursor;
import org.apache.james.mime4j.util.ContentUtil;
import java.util.Collections;
import org.apache.james.mime4j.codec.DecodeMonitor;
import org.apache.james.mime4j.stream.Field;
import org.apache.james.mime4j.dom.FieldParser;
import org.apache.james.mime4j.dom.address.AddressList;
import org.apache.james.mime4j.dom.field.AddressListField;

public class AddressListFieldLenientImpl extends AbstractField implements AddressListField
{
    private boolean parsed;
    private AddressList addressList;
    public static final FieldParser<AddressListField> PARSER;
    
    AddressListFieldLenientImpl(final Field rawField, final DecodeMonitor monitor) {
        super(rawField, monitor);
        this.parsed = false;
    }
    
    public AddressList getAddressList() {
        if (!this.parsed) {
            this.parse();
        }
        return this.addressList;
    }
    
    private void parse() {
        this.parsed = true;
        final RawField f = this.getRawField();
        ByteSequence buf = f.getRaw();
        int pos = f.getDelimiterIdx() + 1;
        if (buf == null) {
            final String body = f.getBody();
            if (body == null) {
                this.addressList = new AddressList(Collections.emptyList(), true);
                return;
            }
            buf = ContentUtil.encode(body);
            pos = 0;
        }
        final ParserCursor cursor = new ParserCursor(pos, buf.length());
        this.addressList = LenientAddressBuilder.DEFAULT.parseAddressList(buf, cursor);
    }
    
    static {
        PARSER = new FieldParser<AddressListField>() {
            public AddressListField parse(final Field rawField, final DecodeMonitor monitor) {
                return new AddressListFieldLenientImpl(rawField, monitor);
            }
        };
    }
}
