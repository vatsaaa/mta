// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.james.mime4j.field;

import org.apache.james.mime4j.dom.field.ParsedField;
import java.util.List;
import org.apache.james.mime4j.field.contenttype.parser.TokenMgrError;
import java.io.Reader;
import org.apache.james.mime4j.field.contenttype.parser.ContentTypeParser;
import java.io.StringReader;
import java.util.Collections;
import java.util.HashMap;
import org.apache.james.mime4j.codec.DecodeMonitor;
import org.apache.james.mime4j.stream.Field;
import org.apache.james.mime4j.dom.FieldParser;
import org.apache.james.mime4j.field.contenttype.parser.ParseException;
import java.util.Map;
import org.apache.james.mime4j.dom.field.ContentTypeField;

public class ContentTypeFieldImpl extends AbstractField implements ContentTypeField
{
    private boolean parsed;
    private String mimeType;
    private String mediaType;
    private String subType;
    private Map<String, String> parameters;
    private ParseException parseException;
    public static final FieldParser<ContentTypeField> PARSER;
    
    ContentTypeFieldImpl(final Field rawField, final DecodeMonitor monitor) {
        super(rawField, monitor);
        this.parsed = false;
        this.mimeType = null;
        this.mediaType = null;
        this.subType = null;
        this.parameters = new HashMap<String, String>();
    }
    
    @Override
    public ParseException getParseException() {
        if (!this.parsed) {
            this.parse();
        }
        return this.parseException;
    }
    
    public String getMimeType() {
        if (!this.parsed) {
            this.parse();
        }
        return this.mimeType;
    }
    
    public String getMediaType() {
        if (!this.parsed) {
            this.parse();
        }
        return this.mediaType;
    }
    
    public String getSubType() {
        if (!this.parsed) {
            this.parse();
        }
        return this.subType;
    }
    
    public String getParameter(final String name) {
        if (!this.parsed) {
            this.parse();
        }
        return this.parameters.get(name.toLowerCase());
    }
    
    public Map<String, String> getParameters() {
        if (!this.parsed) {
            this.parse();
        }
        return Collections.unmodifiableMap((Map<? extends String, ? extends String>)this.parameters);
    }
    
    public boolean isMimeType(final String mimeType) {
        if (!this.parsed) {
            this.parse();
        }
        return this.mimeType != null && this.mimeType.equalsIgnoreCase(mimeType);
    }
    
    public boolean isMultipart() {
        if (!this.parsed) {
            this.parse();
        }
        return this.mimeType != null && this.mimeType.startsWith("multipart/");
    }
    
    public String getBoundary() {
        return this.getParameter("boundary");
    }
    
    public String getCharset() {
        return this.getParameter("charset");
    }
    
    public static String getMimeType(final ContentTypeField child, final ContentTypeField parent) {
        if (child != null && child.getMimeType() != null && (!child.isMultipart() || child.getBoundary() != null)) {
            return child.getMimeType();
        }
        if (parent != null && parent.isMimeType("multipart/digest")) {
            return "message/rfc822";
        }
        return "text/plain";
    }
    
    public static String getCharset(final ContentTypeField f) {
        if (f != null) {
            final String charset = f.getCharset();
            if (charset != null && charset.length() > 0) {
                return charset;
            }
        }
        return "us-ascii";
    }
    
    private void parse() {
        final String body = this.getBody();
        final ContentTypeParser parser = new ContentTypeParser(new StringReader(body));
        try {
            parser.parseAll();
        }
        catch (ParseException e) {
            this.parseException = e;
        }
        catch (TokenMgrError e2) {
            this.parseException = new ParseException(e2.getMessage());
        }
        this.mediaType = parser.getType();
        this.subType = parser.getSubType();
        if (this.mediaType != null && this.subType != null) {
            this.mimeType = (this.mediaType + "/" + this.subType).toLowerCase();
            final List<String> paramNames = parser.getParamNames();
            final List<String> paramValues = parser.getParamValues();
            if (paramNames != null && paramValues != null) {
                for (int len = Math.min(paramNames.size(), paramValues.size()), i = 0; i < len; ++i) {
                    final String paramName = paramNames.get(i).toLowerCase();
                    final String paramValue = paramValues.get(i);
                    this.parameters.put(paramName, paramValue);
                }
            }
        }
        this.parsed = true;
    }
    
    static {
        PARSER = new FieldParser<ContentTypeField>() {
            public ContentTypeField parse(final Field rawField, final DecodeMonitor monitor) {
                return new ContentTypeFieldImpl(rawField, monitor);
            }
        };
    }
}
