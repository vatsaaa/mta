// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.james.mime4j.field;

import org.apache.james.mime4j.dom.field.ParsedField;
import org.apache.james.mime4j.util.ByteSequence;
import org.apache.james.mime4j.stream.RawField;
import org.apache.james.mime4j.util.CharsetUtil;
import java.util.BitSet;
import org.apache.james.mime4j.stream.ParserCursor;
import org.apache.james.mime4j.stream.RawFieldParser;
import org.apache.james.mime4j.util.ContentUtil;
import org.apache.james.mime4j.codec.DecodeMonitor;
import org.apache.james.mime4j.stream.Field;
import org.apache.james.mime4j.dom.FieldParser;
import org.apache.james.mime4j.dom.field.ContentLocationField;

public class ContentLocationFieldLenientImpl extends AbstractField implements ContentLocationField
{
    private boolean parsed;
    private String location;
    public static final FieldParser<ContentLocationField> PARSER;
    
    ContentLocationFieldLenientImpl(final Field rawField, final DecodeMonitor monitor) {
        super(rawField, monitor);
        this.parsed = false;
    }
    
    private void parse() {
        this.parsed = true;
        this.location = null;
        final RawField f = this.getRawField();
        ByteSequence buf = f.getRaw();
        int pos = f.getDelimiterIdx() + 1;
        if (buf == null) {
            final String body = f.getBody();
            if (body == null) {
                return;
            }
            buf = ContentUtil.encode(body);
            pos = 0;
        }
        final RawFieldParser parser = RawFieldParser.DEFAULT;
        final ParserCursor cursor = new ParserCursor(pos, buf.length());
        final String token = parser.parseValue(buf, cursor, null);
        final StringBuilder sb = new StringBuilder(token.length());
        for (int i = 0; i < token.length(); ++i) {
            final char ch = token.charAt(i);
            if (!CharsetUtil.isWhitespace(ch)) {
                sb.append(ch);
            }
        }
        this.location = sb.toString();
    }
    
    public String getLocation() {
        if (!this.parsed) {
            this.parse();
        }
        return this.location;
    }
    
    static {
        PARSER = new FieldParser<ContentLocationField>() {
            public ContentLocationField parse(final Field rawField, final DecodeMonitor monitor) {
                return new ContentLocationFieldLenientImpl(rawField, monitor);
            }
        };
    }
}
