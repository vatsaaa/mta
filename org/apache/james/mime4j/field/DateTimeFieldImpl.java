// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.james.mime4j.field;

import org.apache.james.mime4j.dom.field.ParsedField;
import org.apache.james.mime4j.field.datetime.parser.TokenMgrError;
import java.io.Reader;
import org.apache.james.mime4j.field.datetime.parser.DateTimeParser;
import java.io.StringReader;
import org.apache.james.mime4j.codec.DecodeMonitor;
import org.apache.james.mime4j.stream.Field;
import org.apache.james.mime4j.dom.FieldParser;
import org.apache.james.mime4j.field.datetime.parser.ParseException;
import java.util.Date;
import org.apache.james.mime4j.dom.field.DateTimeField;

public class DateTimeFieldImpl extends AbstractField implements DateTimeField
{
    private boolean parsed;
    private Date date;
    private ParseException parseException;
    public static final FieldParser<DateTimeField> PARSER;
    
    DateTimeFieldImpl(final Field rawField, final DecodeMonitor monitor) {
        super(rawField, monitor);
        this.parsed = false;
    }
    
    public Date getDate() {
        if (!this.parsed) {
            this.parse();
        }
        return this.date;
    }
    
    @Override
    public ParseException getParseException() {
        if (!this.parsed) {
            this.parse();
        }
        return this.parseException;
    }
    
    private void parse() {
        final String body = this.getBody();
        try {
            this.date = new DateTimeParser(new StringReader(body)).parseAll().getDate();
        }
        catch (ParseException e) {
            this.parseException = e;
        }
        catch (TokenMgrError e2) {
            this.parseException = new ParseException(e2.getMessage());
        }
        this.parsed = true;
    }
    
    static {
        PARSER = new FieldParser<DateTimeField>() {
            public DateTimeField parse(final Field rawField, final DecodeMonitor monitor) {
                return new DateTimeFieldImpl(rawField, monitor);
            }
        };
    }
}
