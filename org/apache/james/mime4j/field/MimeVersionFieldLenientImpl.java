// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.james.mime4j.field;

import org.apache.james.mime4j.dom.field.ParsedField;
import org.apache.james.mime4j.util.ByteSequence;
import org.apache.james.mime4j.stream.RawField;
import org.apache.james.mime4j.stream.ParserCursor;
import org.apache.james.mime4j.stream.RawFieldParser;
import org.apache.james.mime4j.util.ContentUtil;
import org.apache.james.mime4j.codec.DecodeMonitor;
import org.apache.james.mime4j.stream.Field;
import org.apache.james.mime4j.dom.FieldParser;
import java.util.BitSet;
import org.apache.james.mime4j.dom.field.MimeVersionField;

public class MimeVersionFieldLenientImpl extends AbstractField implements MimeVersionField
{
    private static final int FULL_STOP = 46;
    private static final BitSet DELIM;
    public static final int DEFAULT_MINOR_VERSION = 0;
    public static final int DEFAULT_MAJOR_VERSION = 1;
    private boolean parsed;
    private int major;
    private int minor;
    public static final FieldParser<MimeVersionField> PARSER;
    
    MimeVersionFieldLenientImpl(final Field rawField, final DecodeMonitor monitor) {
        super(rawField, monitor);
        this.parsed = false;
        this.major = 1;
        this.minor = 0;
    }
    
    private void parse() {
        this.parsed = true;
        this.major = 1;
        this.minor = 0;
        final RawField f = this.getRawField();
        ByteSequence buf = f.getRaw();
        int pos = f.getDelimiterIdx() + 1;
        if (buf == null) {
            final String body = f.getBody();
            if (body == null) {
                return;
            }
            buf = ContentUtil.encode(body);
            pos = 0;
        }
        final RawFieldParser parser = RawFieldParser.DEFAULT;
        final ParserCursor cursor = new ParserCursor(pos, buf.length());
        final String token1 = parser.parseValue(buf, cursor, MimeVersionFieldLenientImpl.DELIM);
        try {
            this.major = Integer.parseInt(token1);
            if (this.major < 0) {
                this.major = 0;
            }
        }
        catch (NumberFormatException ex) {}
        if (!cursor.atEnd() && buf.byteAt(cursor.getPos()) == 46) {
            cursor.updatePos(cursor.getPos() + 1);
        }
        final String token2 = parser.parseValue(buf, cursor, null);
        try {
            this.minor = Integer.parseInt(token2);
            if (this.minor < 0) {
                this.minor = 0;
            }
        }
        catch (NumberFormatException ex2) {}
    }
    
    public int getMinorVersion() {
        if (!this.parsed) {
            this.parse();
        }
        return this.minor;
    }
    
    public int getMajorVersion() {
        if (!this.parsed) {
            this.parse();
        }
        return this.major;
    }
    
    static {
        DELIM = RawFieldParser.INIT_BITSET(46);
        PARSER = new FieldParser<MimeVersionField>() {
            public MimeVersionField parse(final Field rawField, final DecodeMonitor monitor) {
                return new MimeVersionFieldLenientImpl(rawField, monitor);
            }
        };
    }
}
