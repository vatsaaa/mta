// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.james.mime4j.field;

import org.apache.james.mime4j.dom.field.ParsedField;
import java.io.Reader;
import org.apache.james.mime4j.field.structured.parser.StructuredFieldParser;
import java.io.StringReader;
import org.apache.james.mime4j.codec.DecodeMonitor;
import org.apache.james.mime4j.stream.Field;
import org.apache.james.mime4j.dom.FieldParser;
import org.apache.james.mime4j.field.structured.parser.ParseException;
import org.apache.james.mime4j.dom.field.ContentLocationField;

public class ContentLocationFieldImpl extends AbstractField implements ContentLocationField
{
    private boolean parsed;
    private String location;
    private ParseException parseException;
    public static final FieldParser<ContentLocationField> PARSER;
    
    ContentLocationFieldImpl(final Field rawField, final DecodeMonitor monitor) {
        super(rawField, monitor);
        this.parsed = false;
    }
    
    private void parse() {
        this.parsed = true;
        final String body = this.getBody();
        this.location = null;
        if (body != null) {
            final StringReader stringReader = new StringReader(body);
            final StructuredFieldParser parser = new StructuredFieldParser(stringReader);
            try {
                this.location = parser.parse().replaceAll("\\s", "");
            }
            catch (ParseException ex) {
                this.parseException = ex;
            }
        }
    }
    
    public String getLocation() {
        if (!this.parsed) {
            this.parse();
        }
        return this.location;
    }
    
    @Override
    public org.apache.james.mime4j.dom.field.ParseException getParseException() {
        return this.parseException;
    }
    
    static {
        PARSER = new FieldParser<ContentLocationField>() {
            public ContentLocationField parse(final Field rawField, final DecodeMonitor monitor) {
                return new ContentLocationFieldImpl(rawField, monitor);
            }
        };
    }
}
