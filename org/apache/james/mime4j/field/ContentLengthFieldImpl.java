// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.james.mime4j.field;

import org.apache.james.mime4j.dom.field.ParsedField;
import org.apache.james.mime4j.codec.DecodeMonitor;
import org.apache.james.mime4j.stream.Field;
import org.apache.james.mime4j.dom.FieldParser;
import org.apache.james.mime4j.dom.field.ContentLengthField;

public class ContentLengthFieldImpl extends AbstractField implements ContentLengthField
{
    private boolean parsed;
    private long contentLength;
    public static final FieldParser<ContentLengthField> PARSER;
    
    ContentLengthFieldImpl(final Field rawField, final DecodeMonitor monitor) {
        super(rawField, monitor);
        this.parsed = false;
    }
    
    private void parse() {
        this.parsed = true;
        this.contentLength = -1L;
        final String body = this.getBody();
        if (body != null) {
            try {
                this.contentLength = Long.parseLong(body);
                if (this.contentLength < 0L) {
                    this.contentLength = -1L;
                    if (this.monitor.isListening()) {
                        this.monitor.warn("Negative content length: " + body, "ignoring Content-Length header");
                    }
                }
            }
            catch (NumberFormatException e) {
                if (this.monitor.isListening()) {
                    this.monitor.warn("Invalid content length: " + body, "ignoring Content-Length header");
                }
            }
        }
    }
    
    public long getContentLength() {
        if (!this.parsed) {
            this.parse();
        }
        return this.contentLength;
    }
    
    static {
        PARSER = new FieldParser<ContentLengthField>() {
            public ContentLengthField parse(final Field rawField, final DecodeMonitor monitor) {
                return new ContentLengthFieldImpl(rawField, monitor);
            }
        };
    }
}
