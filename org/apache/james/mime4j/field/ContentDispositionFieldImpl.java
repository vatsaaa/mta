// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.james.mime4j.field;

import org.apache.james.mime4j.dom.field.ParsedField;
import java.util.List;
import java.util.Locale;
import org.apache.james.mime4j.field.contentdisposition.parser.ContentDispositionParser;
import org.apache.james.mime4j.field.contentdisposition.parser.TokenMgrError;
import java.io.Reader;
import org.apache.james.mime4j.field.datetime.parser.DateTimeParser;
import java.io.StringReader;
import java.util.Collections;
import java.util.HashMap;
import org.apache.james.mime4j.codec.DecodeMonitor;
import org.apache.james.mime4j.stream.Field;
import org.apache.james.mime4j.dom.FieldParser;
import java.util.Date;
import org.apache.james.mime4j.field.contentdisposition.parser.ParseException;
import java.util.Map;
import org.apache.james.mime4j.dom.field.ContentDispositionField;

public class ContentDispositionFieldImpl extends AbstractField implements ContentDispositionField
{
    private boolean parsed;
    private String dispositionType;
    private Map<String, String> parameters;
    private ParseException parseException;
    private boolean creationDateParsed;
    private Date creationDate;
    private boolean modificationDateParsed;
    private Date modificationDate;
    private boolean readDateParsed;
    private Date readDate;
    public static final FieldParser<ContentDispositionField> PARSER;
    
    ContentDispositionFieldImpl(final Field rawField, final DecodeMonitor monitor) {
        super(rawField, monitor);
        this.parsed = false;
        this.dispositionType = "";
        this.parameters = new HashMap<String, String>();
    }
    
    @Override
    public ParseException getParseException() {
        if (!this.parsed) {
            this.parse();
        }
        return this.parseException;
    }
    
    public String getDispositionType() {
        if (!this.parsed) {
            this.parse();
        }
        return this.dispositionType;
    }
    
    public String getParameter(final String name) {
        if (!this.parsed) {
            this.parse();
        }
        return this.parameters.get(name.toLowerCase());
    }
    
    public Map<String, String> getParameters() {
        if (!this.parsed) {
            this.parse();
        }
        return Collections.unmodifiableMap((Map<? extends String, ? extends String>)this.parameters);
    }
    
    public boolean isDispositionType(final String dispositionType) {
        if (!this.parsed) {
            this.parse();
        }
        return this.dispositionType.equalsIgnoreCase(dispositionType);
    }
    
    public boolean isInline() {
        if (!this.parsed) {
            this.parse();
        }
        return this.dispositionType.equals("inline");
    }
    
    public boolean isAttachment() {
        if (!this.parsed) {
            this.parse();
        }
        return this.dispositionType.equals("attachment");
    }
    
    public String getFilename() {
        return this.getParameter("filename");
    }
    
    public Date getCreationDate() {
        if (!this.creationDateParsed) {
            this.creationDate = this.parseDate("creation-date");
            this.creationDateParsed = true;
        }
        return this.creationDate;
    }
    
    public Date getModificationDate() {
        if (!this.modificationDateParsed) {
            this.modificationDate = this.parseDate("modification-date");
            this.modificationDateParsed = true;
        }
        return this.modificationDate;
    }
    
    public Date getReadDate() {
        if (!this.readDateParsed) {
            this.readDate = this.parseDate("read-date");
            this.readDateParsed = true;
        }
        return this.readDate;
    }
    
    public long getSize() {
        final String value = this.getParameter("size");
        if (value == null) {
            return -1L;
        }
        try {
            final long size = Long.parseLong(value);
            return (size < 0L) ? -1L : size;
        }
        catch (NumberFormatException e) {
            return -1L;
        }
    }
    
    private Date parseDate(final String paramName) {
        final String value = this.getParameter(paramName);
        if (value == null) {
            this.monitor.warn("Parsing " + paramName + " null", "returning null");
            return null;
        }
        try {
            return new DateTimeParser(new StringReader(value)).parseAll().getDate();
        }
        catch (org.apache.james.mime4j.field.datetime.parser.ParseException e) {
            if (this.monitor.isListening()) {
                this.monitor.warn(paramName + " parameter is invalid: " + value, paramName + " parameter is ignored");
            }
            return null;
        }
        catch (TokenMgrError e2) {
            this.monitor.warn(paramName + " parameter is invalid: " + value, paramName + "parameter is ignored");
            return null;
        }
    }
    
    private void parse() {
        final String body = this.getBody();
        final ContentDispositionParser parser = new ContentDispositionParser(new StringReader(body));
        try {
            parser.parseAll();
        }
        catch (ParseException e) {
            this.parseException = e;
        }
        catch (TokenMgrError e2) {
            this.parseException = new ParseException(e2.getMessage());
        }
        final String dispositionType = parser.getDispositionType();
        if (dispositionType != null) {
            this.dispositionType = dispositionType.toLowerCase(Locale.US);
            final List<String> paramNames = parser.getParamNames();
            final List<String> paramValues = parser.getParamValues();
            if (paramNames != null && paramValues != null) {
                for (int len = Math.min(paramNames.size(), paramValues.size()), i = 0; i < len; ++i) {
                    final String paramName = paramNames.get(i).toLowerCase(Locale.US);
                    final String paramValue = paramValues.get(i);
                    this.parameters.put(paramName, paramValue);
                }
            }
        }
        this.parsed = true;
    }
    
    static {
        PARSER = new FieldParser<ContentDispositionField>() {
            public ContentDispositionField parse(final Field rawField, final DecodeMonitor monitor) {
                return new ContentDispositionFieldImpl(rawField, monitor);
            }
        };
    }
}
