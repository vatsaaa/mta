// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.james.mime4j.field;

import org.apache.james.mime4j.dom.field.ParsedField;
import java.util.Collection;
import org.apache.james.mime4j.util.ByteSequence;
import org.apache.james.mime4j.stream.RawField;
import org.apache.james.mime4j.stream.ParserCursor;
import org.apache.james.mime4j.stream.RawFieldParser;
import org.apache.james.mime4j.util.ContentUtil;
import java.util.ArrayList;
import org.apache.james.mime4j.codec.DecodeMonitor;
import org.apache.james.mime4j.stream.Field;
import org.apache.james.mime4j.dom.FieldParser;
import java.util.List;
import java.util.BitSet;
import org.apache.james.mime4j.dom.field.ContentLanguageField;

public class ContentLanguageFieldLenientImpl extends AbstractField implements ContentLanguageField
{
    private static final int COMMA = 44;
    private static final BitSet DELIM;
    private boolean parsed;
    private List<String> languages;
    public static final FieldParser<ContentLanguageField> PARSER;
    
    ContentLanguageFieldLenientImpl(final Field rawField, final DecodeMonitor monitor) {
        super(rawField, monitor);
        this.parsed = false;
    }
    
    private void parse() {
        this.parsed = true;
        this.languages = new ArrayList<String>();
        final RawField f = this.getRawField();
        ByteSequence buf = f.getRaw();
        int pos = f.getDelimiterIdx() + 1;
        if (buf == null) {
            final String body = f.getBody();
            if (body == null) {
                return;
            }
            buf = ContentUtil.encode(body);
            pos = 0;
        }
        final RawFieldParser parser = RawFieldParser.DEFAULT;
        final ParserCursor cursor = new ParserCursor(pos, buf.length());
        while (true) {
            final String token = parser.parseToken(buf, cursor, ContentLanguageFieldLenientImpl.DELIM);
            if (token.length() > 0) {
                this.languages.add(token);
            }
            if (cursor.atEnd()) {
                break;
            }
            pos = cursor.getPos();
            if (buf.byteAt(pos) != 44) {
                continue;
            }
            cursor.updatePos(pos + 1);
        }
    }
    
    public List<String> getLanguages() {
        if (!this.parsed) {
            this.parse();
        }
        return new ArrayList<String>(this.languages);
    }
    
    static {
        DELIM = RawFieldParser.INIT_BITSET(44);
        PARSER = new FieldParser<ContentLanguageField>() {
            public ContentLanguageField parse(final Field rawField, final DecodeMonitor monitor) {
                return new ContentLanguageFieldLenientImpl(rawField, monitor);
            }
        };
    }
}
