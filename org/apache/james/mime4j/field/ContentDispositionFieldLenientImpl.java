// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.james.mime4j.field;

import org.apache.james.mime4j.dom.field.ParsedField;
import java.text.ParseException;
import java.util.TimeZone;
import java.text.SimpleDateFormat;
import java.util.Iterator;
import org.apache.james.mime4j.stream.RawBody;
import org.apache.james.mime4j.stream.RawField;
import org.apache.james.mime4j.stream.NameValuePair;
import java.util.Locale;
import org.apache.james.mime4j.stream.RawFieldParser;
import java.util.Collections;
import java.util.ArrayList;
import java.util.HashMap;
import org.apache.james.mime4j.codec.DecodeMonitor;
import java.util.Collection;
import org.apache.james.mime4j.stream.Field;
import org.apache.james.mime4j.dom.FieldParser;
import java.util.Date;
import java.util.Map;
import java.util.List;
import org.apache.james.mime4j.dom.field.ContentDispositionField;

public class ContentDispositionFieldLenientImpl extends AbstractField implements ContentDispositionField
{
    private static final String DEFAULT_DATE_FORMAT = "EEE, dd MMM yyyy hh:mm:ss ZZZZ";
    private final List<String> datePatterns;
    private boolean parsed;
    private String dispositionType;
    private Map<String, String> parameters;
    private boolean creationDateParsed;
    private Date creationDate;
    private boolean modificationDateParsed;
    private Date modificationDate;
    private boolean readDateParsed;
    private Date readDate;
    public static final FieldParser<ContentDispositionField> PARSER;
    
    ContentDispositionFieldLenientImpl(final Field rawField, final Collection<String> dateParsers, final DecodeMonitor monitor) {
        super(rawField, monitor);
        this.parsed = false;
        this.dispositionType = "";
        this.parameters = new HashMap<String, String>();
        this.datePatterns = new ArrayList<String>();
        if (dateParsers != null) {
            this.datePatterns.addAll(dateParsers);
        }
        else {
            this.datePatterns.add("EEE, dd MMM yyyy hh:mm:ss ZZZZ");
        }
    }
    
    public String getDispositionType() {
        if (!this.parsed) {
            this.parse();
        }
        return this.dispositionType;
    }
    
    public String getParameter(final String name) {
        if (!this.parsed) {
            this.parse();
        }
        return this.parameters.get(name.toLowerCase());
    }
    
    public Map<String, String> getParameters() {
        if (!this.parsed) {
            this.parse();
        }
        return Collections.unmodifiableMap((Map<? extends String, ? extends String>)this.parameters);
    }
    
    public boolean isDispositionType(final String dispositionType) {
        if (!this.parsed) {
            this.parse();
        }
        return this.dispositionType.equalsIgnoreCase(dispositionType);
    }
    
    public boolean isInline() {
        if (!this.parsed) {
            this.parse();
        }
        return this.dispositionType.equals("inline");
    }
    
    public boolean isAttachment() {
        if (!this.parsed) {
            this.parse();
        }
        return this.dispositionType.equals("attachment");
    }
    
    public String getFilename() {
        return this.getParameter("filename");
    }
    
    public Date getCreationDate() {
        if (!this.creationDateParsed) {
            this.creationDate = this.parseDate("creation-date");
            this.creationDateParsed = true;
        }
        return this.creationDate;
    }
    
    public Date getModificationDate() {
        if (!this.modificationDateParsed) {
            this.modificationDate = this.parseDate("modification-date");
            this.modificationDateParsed = true;
        }
        return this.modificationDate;
    }
    
    public Date getReadDate() {
        if (!this.readDateParsed) {
            this.readDate = this.parseDate("read-date");
            this.readDateParsed = true;
        }
        return this.readDate;
    }
    
    public long getSize() {
        final String value = this.getParameter("size");
        if (value == null) {
            return -1L;
        }
        try {
            final long size = Long.parseLong(value);
            return (size < 0L) ? -1L : size;
        }
        catch (NumberFormatException e) {
            return -1L;
        }
    }
    
    private void parse() {
        this.parsed = true;
        final RawField f = this.getRawField();
        final RawBody body = RawFieldParser.DEFAULT.parseRawBody(f);
        final String main = body.getValue();
        if (main != null) {
            this.dispositionType = main.toLowerCase(Locale.US);
        }
        else {
            this.dispositionType = null;
        }
        this.parameters.clear();
        for (final NameValuePair nmp : body.getParams()) {
            final String name = nmp.getName().toLowerCase(Locale.US);
            this.parameters.put(name, nmp.getValue());
        }
    }
    
    private Date parseDate(final String paramName) {
        final String value = this.getParameter(paramName);
        if (value == null) {
            return null;
        }
        for (final String datePattern : this.datePatterns) {
            try {
                final SimpleDateFormat parser = new SimpleDateFormat(datePattern, Locale.US);
                parser.setTimeZone(TimeZone.getTimeZone("GMT"));
                parser.setLenient(true);
                return parser.parse(value);
            }
            catch (ParseException ignore) {
                continue;
            }
            break;
        }
        if (this.monitor.isListening()) {
            this.monitor.warn(paramName + " parameter is invalid: " + value, paramName + " parameter is ignored");
        }
        return null;
    }
    
    static {
        PARSER = new FieldParser<ContentDispositionField>() {
            public ContentDispositionField parse(final Field rawField, final DecodeMonitor monitor) {
                return new ContentDispositionFieldLenientImpl(rawField, null, monitor);
            }
        };
    }
}
