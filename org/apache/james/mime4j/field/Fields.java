// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.james.mime4j.field;

import org.apache.james.mime4j.field.address.AddressFormatter;
import org.apache.james.mime4j.stream.Field;
import org.apache.james.mime4j.codec.DecodeMonitor;
import org.apache.james.mime4j.stream.RawField;
import org.apache.james.mime4j.dom.field.ParsedField;
import org.apache.james.mime4j.dom.FieldParser;
import org.apache.james.mime4j.dom.field.AddressListField;
import org.apache.james.mime4j.dom.address.Address;
import java.util.Arrays;
import java.util.Collections;
import org.apache.james.mime4j.dom.field.MailboxListField;
import org.apache.james.mime4j.dom.field.MailboxField;
import org.apache.james.mime4j.dom.address.Mailbox;
import org.apache.james.mime4j.dom.field.UnstructuredField;
import org.apache.james.mime4j.dom.field.DateTimeField;
import java.util.TimeZone;
import org.apache.james.mime4j.util.MimeUtil;
import java.util.HashMap;
import java.util.Date;
import org.apache.james.mime4j.dom.field.ContentDispositionField;
import org.apache.james.mime4j.dom.field.ContentTransferEncodingField;
import java.util.Iterator;
import org.apache.james.mime4j.codec.EncoderUtil;
import java.util.Map;
import org.apache.james.mime4j.dom.field.ContentTypeField;
import java.util.regex.Pattern;

public class Fields
{
    private static final Pattern FIELD_NAME_PATTERN;
    
    private Fields() {
    }
    
    public static ContentTypeField contentType(final String contentType) {
        return parse(ContentTypeFieldImpl.PARSER, "Content-Type", contentType);
    }
    
    public static ContentTypeField contentType(final String mimeType, final Map<String, String> parameters) {
        if (!isValidMimeType(mimeType)) {
            throw new IllegalArgumentException();
        }
        if (parameters == null || parameters.isEmpty()) {
            return parse(ContentTypeFieldImpl.PARSER, "Content-Type", mimeType);
        }
        final StringBuilder sb = new StringBuilder(mimeType);
        for (final Map.Entry<String, String> entry : parameters.entrySet()) {
            sb.append("; ");
            sb.append(EncoderUtil.encodeHeaderParameter(entry.getKey(), entry.getValue()));
        }
        final String contentType = sb.toString();
        return contentType(contentType);
    }
    
    public static ContentTransferEncodingField contentTransferEncoding(final String contentTransferEncoding) {
        return parse(ContentTransferEncodingFieldImpl.PARSER, "Content-Transfer-Encoding", contentTransferEncoding);
    }
    
    public static ContentDispositionField contentDisposition(final String contentDisposition) {
        return parse(ContentDispositionFieldImpl.PARSER, "Content-Disposition", contentDisposition);
    }
    
    public static ContentDispositionField contentDisposition(final String dispositionType, final Map<String, String> parameters) {
        if (!isValidDispositionType(dispositionType)) {
            throw new IllegalArgumentException();
        }
        if (parameters == null || parameters.isEmpty()) {
            return parse(ContentDispositionFieldImpl.PARSER, "Content-Disposition", dispositionType);
        }
        final StringBuilder sb = new StringBuilder(dispositionType);
        for (final Map.Entry<String, String> entry : parameters.entrySet()) {
            sb.append("; ");
            sb.append(EncoderUtil.encodeHeaderParameter(entry.getKey(), entry.getValue()));
        }
        final String contentDisposition = sb.toString();
        return contentDisposition(contentDisposition);
    }
    
    public static ContentDispositionField contentDisposition(final String dispositionType, final String filename) {
        return contentDisposition(dispositionType, filename, -1L, null, null, null);
    }
    
    public static ContentDispositionField contentDisposition(final String dispositionType, final String filename, final long size) {
        return contentDisposition(dispositionType, filename, size, null, null, null);
    }
    
    public static ContentDispositionField contentDisposition(final String dispositionType, final String filename, final long size, final Date creationDate, final Date modificationDate, final Date readDate) {
        final Map<String, String> parameters = new HashMap<String, String>();
        if (filename != null) {
            parameters.put("filename", filename);
        }
        if (size >= 0L) {
            parameters.put("size", Long.toString(size));
        }
        if (creationDate != null) {
            parameters.put("creation-date", MimeUtil.formatDate(creationDate, null));
        }
        if (modificationDate != null) {
            parameters.put("modification-date", MimeUtil.formatDate(modificationDate, null));
        }
        if (readDate != null) {
            parameters.put("read-date", MimeUtil.formatDate(readDate, null));
        }
        return contentDisposition(dispositionType, parameters);
    }
    
    public static DateTimeField date(final Date date) {
        return date0("Date", date, null);
    }
    
    public static DateTimeField date(final String fieldName, final Date date) {
        checkValidFieldName(fieldName);
        return date0(fieldName, date, null);
    }
    
    public static DateTimeField date(final String fieldName, final Date date, final TimeZone zone) {
        checkValidFieldName(fieldName);
        return date0(fieldName, date, zone);
    }
    
    public static UnstructuredField messageId(final String hostname) {
        final String fieldValue = MimeUtil.createUniqueMessageId(hostname);
        return parse(UnstructuredFieldImpl.PARSER, "Message-ID", fieldValue);
    }
    
    public static UnstructuredField subject(final String subject) {
        final int usedCharacters = "Subject".length() + 2;
        final String fieldValue = EncoderUtil.encodeIfNecessary(subject, EncoderUtil.Usage.TEXT_TOKEN, usedCharacters);
        return parse(UnstructuredFieldImpl.PARSER, "Subject", fieldValue);
    }
    
    public static MailboxField sender(final Mailbox mailbox) {
        return mailbox0("Sender", mailbox);
    }
    
    public static MailboxListField from(final Mailbox mailbox) {
        return mailboxList0("From", Collections.singleton(mailbox));
    }
    
    public static MailboxListField from(final Mailbox... mailboxes) {
        return mailboxList0("From", Arrays.asList(mailboxes));
    }
    
    public static MailboxListField from(final Iterable<Mailbox> mailboxes) {
        return mailboxList0("From", mailboxes);
    }
    
    public static AddressListField to(final Address address) {
        return addressList0("To", Collections.singleton(address));
    }
    
    public static AddressListField to(final Address... addresses) {
        return addressList0("To", Arrays.asList(addresses));
    }
    
    public static AddressListField to(final Iterable<Address> addresses) {
        return addressList0("To", addresses);
    }
    
    public static AddressListField cc(final Address address) {
        return addressList0("Cc", Collections.singleton(address));
    }
    
    public static AddressListField cc(final Address... addresses) {
        return addressList0("Cc", Arrays.asList(addresses));
    }
    
    public static AddressListField cc(final Iterable<Address> addresses) {
        return addressList0("Cc", addresses);
    }
    
    public static AddressListField bcc(final Address address) {
        return addressList0("Bcc", Collections.singleton(address));
    }
    
    public static AddressListField bcc(final Address... addresses) {
        return addressList0("Bcc", Arrays.asList(addresses));
    }
    
    public static AddressListField bcc(final Iterable<Address> addresses) {
        return addressList0("Bcc", addresses);
    }
    
    public static AddressListField replyTo(final Address address) {
        return addressList0("Reply-To", Collections.singleton(address));
    }
    
    public static AddressListField replyTo(final Address... addresses) {
        return addressList0("Reply-To", Arrays.asList(addresses));
    }
    
    public static AddressListField replyTo(final Iterable<Address> addresses) {
        return addressList0("Reply-To", addresses);
    }
    
    public static MailboxField mailbox(final String fieldName, final Mailbox mailbox) {
        checkValidFieldName(fieldName);
        return mailbox0(fieldName, mailbox);
    }
    
    public static MailboxListField mailboxList(final String fieldName, final Iterable<Mailbox> mailboxes) {
        checkValidFieldName(fieldName);
        return mailboxList0(fieldName, mailboxes);
    }
    
    public static AddressListField addressList(final String fieldName, final Iterable<? extends Address> addresses) {
        checkValidFieldName(fieldName);
        return addressList0(fieldName, addresses);
    }
    
    private static DateTimeField date0(final String fieldName, final Date date, final TimeZone zone) {
        final String formattedDate = MimeUtil.formatDate(date, zone);
        return parse(DateTimeFieldImpl.PARSER, fieldName, formattedDate);
    }
    
    private static MailboxField mailbox0(final String fieldName, final Mailbox mailbox) {
        final String fieldValue = encodeAddresses(Collections.singleton(mailbox));
        return parse(MailboxFieldImpl.PARSER, fieldName, fieldValue);
    }
    
    private static MailboxListField mailboxList0(final String fieldName, final Iterable<Mailbox> mailboxes) {
        final String fieldValue = encodeAddresses(mailboxes);
        return parse(MailboxListFieldImpl.PARSER, fieldName, fieldValue);
    }
    
    private static AddressListField addressList0(final String fieldName, final Iterable<? extends Address> addresses) {
        final String fieldValue = encodeAddresses(addresses);
        return parse(AddressListFieldImpl.PARSER, fieldName, fieldValue);
    }
    
    private static void checkValidFieldName(final String fieldName) {
        if (!Fields.FIELD_NAME_PATTERN.matcher(fieldName).matches()) {
            throw new IllegalArgumentException("Invalid field name");
        }
    }
    
    private static boolean isValidMimeType(final String mimeType) {
        if (mimeType == null) {
            return false;
        }
        final int idx = mimeType.indexOf(47);
        if (idx == -1) {
            return false;
        }
        final String type = mimeType.substring(0, idx);
        final String subType = mimeType.substring(idx + 1);
        return EncoderUtil.isToken(type) && EncoderUtil.isToken(subType);
    }
    
    private static boolean isValidDispositionType(final String dispositionType) {
        return dispositionType != null && EncoderUtil.isToken(dispositionType);
    }
    
    private static <F extends ParsedField> F parse(final FieldParser<F> parser, final String fieldName, final String fieldBody) {
        final RawField rawField = new RawField(fieldName, fieldBody);
        return parser.parse(rawField, DecodeMonitor.SILENT);
    }
    
    private static String encodeAddresses(final Iterable<? extends Address> addresses) {
        final StringBuilder sb = new StringBuilder();
        for (final Address address : addresses) {
            if (sb.length() > 0) {
                sb.append(", ");
            }
            AddressFormatter.DEFAULT.encode(sb, address);
        }
        return sb.toString();
    }
    
    static {
        FIELD_NAME_PATTERN = Pattern.compile("[\\x21-\\x39\\x3b-\\x7e]+");
    }
}
