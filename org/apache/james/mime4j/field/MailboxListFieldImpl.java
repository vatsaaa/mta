// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.james.mime4j.field;

import org.apache.james.mime4j.dom.field.ParsedField;
import org.apache.james.mime4j.field.address.AddressBuilder;
import org.apache.james.mime4j.codec.DecodeMonitor;
import org.apache.james.mime4j.stream.Field;
import org.apache.james.mime4j.dom.FieldParser;
import org.apache.james.mime4j.field.address.ParseException;
import org.apache.james.mime4j.dom.address.MailboxList;
import org.apache.james.mime4j.dom.field.MailboxListField;

public class MailboxListFieldImpl extends AbstractField implements MailboxListField
{
    private boolean parsed;
    private MailboxList mailboxList;
    private ParseException parseException;
    public static final FieldParser<MailboxListField> PARSER;
    
    MailboxListFieldImpl(final Field rawField, final DecodeMonitor monitor) {
        super(rawField, monitor);
        this.parsed = false;
    }
    
    public MailboxList getMailboxList() {
        if (!this.parsed) {
            this.parse();
        }
        return this.mailboxList;
    }
    
    @Override
    public ParseException getParseException() {
        if (!this.parsed) {
            this.parse();
        }
        return this.parseException;
    }
    
    private void parse() {
        final String body = this.getBody();
        try {
            this.mailboxList = AddressBuilder.DEFAULT.parseAddressList(body, this.monitor).flatten();
        }
        catch (ParseException e) {
            this.parseException = e;
        }
        this.parsed = true;
    }
    
    static {
        PARSER = new FieldParser<MailboxListField>() {
            public MailboxListField parse(final Field rawField, final DecodeMonitor monitor) {
                return new MailboxListFieldImpl(rawField, monitor);
            }
        };
    }
}
