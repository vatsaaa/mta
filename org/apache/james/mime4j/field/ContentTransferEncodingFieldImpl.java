// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.james.mime4j.field;

import org.apache.james.mime4j.dom.field.ParsedField;
import java.util.Locale;
import org.apache.james.mime4j.codec.DecodeMonitor;
import org.apache.james.mime4j.stream.Field;
import org.apache.james.mime4j.dom.FieldParser;
import org.apache.james.mime4j.dom.field.ContentTransferEncodingField;

public class ContentTransferEncodingFieldImpl extends AbstractField implements ContentTransferEncodingField
{
    private boolean parsed;
    private String encoding;
    public static final FieldParser<ContentTransferEncodingField> PARSER;
    
    ContentTransferEncodingFieldImpl(final Field rawField, final DecodeMonitor monitor) {
        super(rawField, monitor);
        this.parsed = false;
    }
    
    private void parse() {
        this.parsed = true;
        final String body = this.getBody();
        if (body != null) {
            this.encoding = body.trim().toLowerCase(Locale.US);
        }
        else {
            this.encoding = null;
        }
    }
    
    public String getEncoding() {
        if (!this.parsed) {
            this.parse();
        }
        return this.encoding;
    }
    
    public static String getEncoding(final ContentTransferEncodingField f) {
        if (f != null && f.getEncoding().length() != 0) {
            return f.getEncoding();
        }
        return "7bit";
    }
    
    static {
        PARSER = new FieldParser<ContentTransferEncodingField>() {
            public ContentTransferEncodingField parse(final Field rawField, final DecodeMonitor monitor) {
                return new ContentTransferEncodingFieldImpl(rawField, monitor);
            }
        };
    }
}
