// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.james.mime4j.field;

import org.apache.james.mime4j.dom.field.AddressListField;
import org.apache.james.mime4j.dom.field.MailboxField;
import org.apache.james.mime4j.dom.field.MailboxListField;
import org.apache.james.mime4j.dom.field.DateTimeField;
import org.apache.james.mime4j.stream.RawField;
import org.apache.james.mime4j.util.ContentUtil;
import org.apache.james.mime4j.MimeException;
import org.apache.james.mime4j.stream.Field;
import org.apache.james.mime4j.stream.RawFieldParser;
import org.apache.james.mime4j.codec.DecodeMonitor;
import org.apache.james.mime4j.util.ByteSequence;
import org.apache.james.mime4j.dom.field.ParsedField;
import org.apache.james.mime4j.dom.FieldParser;

public class DefaultFieldParser extends DelegatingFieldParser
{
    private static final FieldParser<ParsedField> PARSER;
    
    public static FieldParser<ParsedField> getParser() {
        return DefaultFieldParser.PARSER;
    }
    
    public static ParsedField parse(final ByteSequence raw, final DecodeMonitor monitor) throws MimeException {
        final Field rawField = RawFieldParser.DEFAULT.parseField(raw);
        return DefaultFieldParser.PARSER.parse(rawField, monitor);
    }
    
    public static ParsedField parse(final String rawStr, final DecodeMonitor monitor) throws MimeException {
        final ByteSequence raw = ContentUtil.encode(rawStr);
        final RawField rawField = RawFieldParser.DEFAULT.parseField(raw);
        return DefaultFieldParser.PARSER.parse(rawField, monitor);
    }
    
    public static ParsedField parse(final String rawStr) throws MimeException {
        return parse(rawStr, DecodeMonitor.SILENT);
    }
    
    public DefaultFieldParser() {
        super(UnstructuredFieldImpl.PARSER);
        this.setFieldParser("Content-Type", ContentTypeFieldImpl.PARSER);
        this.setFieldParser("Content-Length", ContentLengthFieldImpl.PARSER);
        this.setFieldParser("Content-Transfer-Encoding", ContentTransferEncodingFieldImpl.PARSER);
        this.setFieldParser("Content-Disposition", ContentDispositionFieldImpl.PARSER);
        this.setFieldParser("Content-ID", ContentIdFieldImpl.PARSER);
        this.setFieldParser("Content-MD5", ContentMD5FieldImpl.PARSER);
        this.setFieldParser("Content-Description", ContentDescriptionFieldImpl.PARSER);
        this.setFieldParser("Content-Language", ContentLanguageFieldImpl.PARSER);
        this.setFieldParser("Content-Location", ContentLocationFieldImpl.PARSER);
        this.setFieldParser("MIME-Version", MimeVersionFieldImpl.PARSER);
        final FieldParser<DateTimeField> dateTimeParser = DateTimeFieldImpl.PARSER;
        this.setFieldParser("Date", dateTimeParser);
        this.setFieldParser("Resent-Date", dateTimeParser);
        final FieldParser<MailboxListField> mailboxListParser = MailboxListFieldImpl.PARSER;
        this.setFieldParser("From", mailboxListParser);
        this.setFieldParser("Resent-From", mailboxListParser);
        final FieldParser<MailboxField> mailboxParser = MailboxFieldImpl.PARSER;
        this.setFieldParser("Sender", mailboxParser);
        this.setFieldParser("Resent-Sender", mailboxParser);
        final FieldParser<AddressListField> addressListParser = AddressListFieldImpl.PARSER;
        this.setFieldParser("To", addressListParser);
        this.setFieldParser("Resent-To", addressListParser);
        this.setFieldParser("Cc", addressListParser);
        this.setFieldParser("Resent-Cc", addressListParser);
        this.setFieldParser("Bcc", addressListParser);
        this.setFieldParser("Resent-Bcc", addressListParser);
        this.setFieldParser("Reply-To", addressListParser);
    }
    
    static {
        PARSER = new DefaultFieldParser();
    }
}
