// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.james.mime4j.field;

import org.apache.james.mime4j.dom.field.ParsedField;
import java.util.Iterator;
import org.apache.james.mime4j.stream.RawBody;
import org.apache.james.mime4j.stream.RawField;
import java.util.Locale;
import org.apache.james.mime4j.stream.NameValuePair;
import org.apache.james.mime4j.stream.RawFieldParser;
import java.util.Collections;
import java.util.HashMap;
import org.apache.james.mime4j.codec.DecodeMonitor;
import org.apache.james.mime4j.stream.Field;
import org.apache.james.mime4j.dom.FieldParser;
import java.util.Map;
import org.apache.james.mime4j.dom.field.ContentTypeField;

public class ContentTypeFieldLenientImpl extends AbstractField implements ContentTypeField
{
    private boolean parsed;
    private String mimeType;
    private String mediaType;
    private String subType;
    private Map<String, String> parameters;
    public static final FieldParser<ContentTypeField> PARSER;
    
    ContentTypeFieldLenientImpl(final Field rawField, final DecodeMonitor monitor) {
        super(rawField, monitor);
        this.parsed = false;
        this.mimeType = null;
        this.mediaType = null;
        this.subType = null;
        this.parameters = new HashMap<String, String>();
    }
    
    public String getMimeType() {
        if (!this.parsed) {
            this.parse();
        }
        return this.mimeType;
    }
    
    public String getMediaType() {
        if (!this.parsed) {
            this.parse();
        }
        return this.mediaType;
    }
    
    public String getSubType() {
        if (!this.parsed) {
            this.parse();
        }
        return this.subType;
    }
    
    public String getParameter(final String name) {
        if (!this.parsed) {
            this.parse();
        }
        return this.parameters.get(name.toLowerCase());
    }
    
    public Map<String, String> getParameters() {
        if (!this.parsed) {
            this.parse();
        }
        return Collections.unmodifiableMap((Map<? extends String, ? extends String>)this.parameters);
    }
    
    public boolean isMimeType(final String mimeType) {
        if (!this.parsed) {
            this.parse();
        }
        return this.mimeType != null && this.mimeType.equalsIgnoreCase(mimeType);
    }
    
    public boolean isMultipart() {
        if (!this.parsed) {
            this.parse();
        }
        return this.mimeType != null && this.mimeType.startsWith("multipart/");
    }
    
    public String getBoundary() {
        return this.getParameter("boundary");
    }
    
    public String getCharset() {
        return this.getParameter("charset");
    }
    
    private void parse() {
        this.parsed = true;
        final RawField f = this.getRawField();
        final RawBody body = RawFieldParser.DEFAULT.parseRawBody(f);
        String main = body.getValue();
        String type = null;
        String subtype = null;
        if (main != null) {
            main = main.toLowerCase().trim();
            final int index = main.indexOf(47);
            boolean valid = false;
            if (index != -1) {
                type = main.substring(0, index).trim();
                subtype = main.substring(index + 1).trim();
                if (type.length() > 0 && subtype.length() > 0) {
                    main = type + "/" + subtype;
                    valid = true;
                }
            }
            if (!valid) {
                if (this.monitor.isListening()) {
                    this.monitor.warn("Invalid Content-Type: " + body, "Content-Type value ignored");
                }
                main = null;
                type = null;
                subtype = null;
            }
        }
        this.mimeType = main;
        this.mediaType = type;
        this.subType = subtype;
        this.parameters.clear();
        for (final NameValuePair nmp : body.getParams()) {
            final String name = nmp.getName().toLowerCase(Locale.US);
            this.parameters.put(name, nmp.getValue());
        }
    }
    
    static {
        PARSER = new FieldParser<ContentTypeField>() {
            public ContentTypeField parse(final Field rawField, final DecodeMonitor monitor) {
                return new ContentTypeFieldLenientImpl(rawField, monitor);
            }
        };
    }
}
