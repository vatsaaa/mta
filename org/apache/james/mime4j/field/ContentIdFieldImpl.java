// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.james.mime4j.field;

import org.apache.james.mime4j.dom.field.ParsedField;
import org.apache.james.mime4j.codec.DecodeMonitor;
import org.apache.james.mime4j.stream.Field;
import org.apache.james.mime4j.dom.FieldParser;
import org.apache.james.mime4j.dom.field.ContentIdField;

public class ContentIdFieldImpl extends AbstractField implements ContentIdField
{
    private boolean parsed;
    private String id;
    public static final FieldParser<ContentIdField> PARSER;
    
    ContentIdFieldImpl(final Field rawField, final DecodeMonitor monitor) {
        super(rawField, monitor);
        this.parsed = false;
    }
    
    private void parse() {
        this.parsed = true;
        final String body = this.getBody();
        if (body != null) {
            this.id = body.trim();
        }
        else {
            this.id = null;
        }
    }
    
    public String getId() {
        if (!this.parsed) {
            this.parse();
        }
        return this.id;
    }
    
    static {
        PARSER = new FieldParser<ContentIdField>() {
            public ContentIdField parse(final Field rawField, final DecodeMonitor monitor) {
                return new ContentIdFieldImpl(rawField, monitor);
            }
        };
    }
}
