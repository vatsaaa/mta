// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.james.mime4j.field;

import org.apache.james.mime4j.dom.field.ParsedField;
import org.apache.james.mime4j.codec.DecodeMonitor;
import org.apache.james.mime4j.stream.Field;
import org.apache.james.mime4j.dom.FieldParser;
import org.apache.james.mime4j.dom.field.ContentDescriptionField;

public class ContentDescriptionFieldImpl extends AbstractField implements ContentDescriptionField
{
    private boolean parsed;
    private String description;
    public static final FieldParser<ContentDescriptionField> PARSER;
    
    ContentDescriptionFieldImpl(final Field rawField, final DecodeMonitor monitor) {
        super(rawField, monitor);
        this.parsed = false;
    }
    
    private void parse() {
        this.parsed = true;
        final String body = this.getBody();
        if (body != null) {
            this.description = body.trim();
        }
        else {
            this.description = null;
        }
    }
    
    public String getDescription() {
        if (!this.parsed) {
            this.parse();
        }
        return this.description;
    }
    
    static {
        PARSER = new FieldParser<ContentDescriptionField>() {
            public ContentDescriptionField parse(final Field rawField, final DecodeMonitor monitor) {
                return new ContentDescriptionFieldImpl(rawField, monitor);
            }
        };
    }
}
