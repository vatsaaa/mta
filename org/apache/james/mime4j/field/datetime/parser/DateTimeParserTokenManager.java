// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.james.mime4j.field.datetime.parser;

import java.io.IOException;
import java.io.PrintStream;

public class DateTimeParserTokenManager implements DateTimeParserConstants
{
    static int commentNest;
    public PrintStream debugStream;
    static final long[] jjbitVec0;
    static final int[] jjnextStates;
    public static final String[] jjstrLiteralImages;
    public static final String[] lexStateNames;
    public static final int[] jjnewLexState;
    static final long[] jjtoToken;
    static final long[] jjtoSkip;
    static final long[] jjtoSpecial;
    static final long[] jjtoMore;
    protected SimpleCharStream input_stream;
    private final int[] jjrounds;
    private final int[] jjstateSet;
    private final StringBuilder jjimage;
    private StringBuilder image;
    private int jjimageLen;
    private int lengthOfMatch;
    protected char curChar;
    int curLexState;
    int defaultLexState;
    int jjnewStateCnt;
    int jjround;
    int jjmatchedPos;
    int jjmatchedKind;
    
    public void setDebugStream(final PrintStream ds) {
        this.debugStream = ds;
    }
    
    private final int jjStopStringLiteralDfa_0(final int pos, final long active0) {
        switch (pos) {
            case 0: {
                if ((active0 & 0x7FE7CF7F0L) != 0x0L) {
                    this.jjmatchedKind = 35;
                    return -1;
                }
                return -1;
            }
            case 1: {
                if ((active0 & 0x7FE7CF7F0L) != 0x0L) {
                    if (this.jjmatchedPos == 0) {
                        this.jjmatchedKind = 35;
                        this.jjmatchedPos = 0;
                    }
                    return -1;
                }
                return -1;
            }
            default: {
                return -1;
            }
        }
    }
    
    private final int jjStartNfa_0(final int pos, final long active0) {
        return this.jjMoveNfa_0(this.jjStopStringLiteralDfa_0(pos, active0), pos + 1);
    }
    
    private int jjStopAtPos(final int pos, final int kind) {
        this.jjmatchedKind = kind;
        return (this.jjmatchedPos = pos) + 1;
    }
    
    private int jjMoveStringLiteralDfa0_0() {
        switch (this.curChar) {
            case '\n': {
                return this.jjStopAtPos(0, 2);
            }
            case '\r': {
                return this.jjStopAtPos(0, 1);
            }
            case '(': {
                return this.jjStopAtPos(0, 37);
            }
            case ',': {
                return this.jjStopAtPos(0, 3);
            }
            case ':': {
                return this.jjStopAtPos(0, 23);
            }
            case 'A': {
                return this.jjMoveStringLiteralDfa1_0(278528L);
            }
            case 'C': {
                return this.jjMoveStringLiteralDfa1_0(1610612736L);
            }
            case 'D': {
                return this.jjMoveStringLiteralDfa1_0(4194304L);
            }
            case 'E': {
                return this.jjMoveStringLiteralDfa1_0(402653184L);
            }
            case 'F': {
                return this.jjMoveStringLiteralDfa1_0(4352L);
            }
            case 'G': {
                return this.jjMoveStringLiteralDfa1_0(67108864L);
            }
            case 'J': {
                return this.jjMoveStringLiteralDfa1_0(198656L);
            }
            case 'M': {
                return this.jjMoveStringLiteralDfa1_0(6442491920L);
            }
            case 'N': {
                return this.jjMoveStringLiteralDfa1_0(2097152L);
            }
            case 'O': {
                return this.jjMoveStringLiteralDfa1_0(1048576L);
            }
            case 'P': {
                return this.jjMoveStringLiteralDfa1_0(25769803776L);
            }
            case 'S': {
                return this.jjMoveStringLiteralDfa1_0(525824L);
            }
            case 'T': {
                return this.jjMoveStringLiteralDfa1_0(160L);
            }
            case 'U': {
                return this.jjMoveStringLiteralDfa1_0(33554432L);
            }
            case 'W': {
                return this.jjMoveStringLiteralDfa1_0(64L);
            }
            default: {
                return this.jjMoveNfa_0(0, 0);
            }
        }
    }
    
    private int jjMoveStringLiteralDfa1_0(final long active0) {
        try {
            this.curChar = this.input_stream.readChar();
        }
        catch (IOException e) {
            this.jjStopStringLiteralDfa_0(0, active0);
            return 1;
        }
        switch (this.curChar) {
            case 'D': {
                return this.jjMoveStringLiteralDfa2_0(active0, 22817013760L);
            }
            case 'M': {
                return this.jjMoveStringLiteralDfa2_0(active0, 67108864L);
            }
            case 'S': {
                return this.jjMoveStringLiteralDfa2_0(active0, 11408506880L);
            }
            case 'T': {
                if ((active0 & 0x2000000L) != 0x0L) {
                    return this.jjStopAtPos(1, 25);
                }
                break;
            }
            case 'a': {
                return this.jjMoveStringLiteralDfa2_0(active0, 43520L);
            }
            case 'c': {
                return this.jjMoveStringLiteralDfa2_0(active0, 1048576L);
            }
            case 'e': {
                return this.jjMoveStringLiteralDfa2_0(active0, 4722752L);
            }
            case 'h': {
                return this.jjMoveStringLiteralDfa2_0(active0, 128L);
            }
            case 'o': {
                return this.jjMoveStringLiteralDfa2_0(active0, 2097168L);
            }
            case 'p': {
                return this.jjMoveStringLiteralDfa2_0(active0, 16384L);
            }
            case 'r': {
                return this.jjMoveStringLiteralDfa2_0(active0, 256L);
            }
            case 'u': {
                return this.jjMoveStringLiteralDfa2_0(active0, 459808L);
            }
        }
        return this.jjStartNfa_0(0, active0);
    }
    
    private int jjMoveStringLiteralDfa2_0(final long old0, long active0) {
        if ((active0 &= old0) == 0x0L) {
            return this.jjStartNfa_0(0, old0);
        }
        try {
            this.curChar = this.input_stream.readChar();
        }
        catch (IOException e) {
            this.jjStopStringLiteralDfa_0(1, active0);
            return 2;
        }
        switch (this.curChar) {
            case 'T': {
                if ((active0 & 0x4000000L) != 0x0L) {
                    return this.jjStopAtPos(2, 26);
                }
                if ((active0 & 0x8000000L) != 0x0L) {
                    return this.jjStopAtPos(2, 27);
                }
                if ((active0 & 0x10000000L) != 0x0L) {
                    return this.jjStopAtPos(2, 28);
                }
                if ((active0 & 0x20000000L) != 0x0L) {
                    return this.jjStopAtPos(2, 29);
                }
                if ((active0 & 0x40000000L) != 0x0L) {
                    return this.jjStopAtPos(2, 30);
                }
                if ((active0 & 0x80000000L) != 0x0L) {
                    return this.jjStopAtPos(2, 31);
                }
                if ((active0 & 0x100000000L) != 0x0L) {
                    return this.jjStopAtPos(2, 32);
                }
                if ((active0 & 0x200000000L) != 0x0L) {
                    return this.jjStopAtPos(2, 33);
                }
                if ((active0 & 0x400000000L) != 0x0L) {
                    return this.jjStopAtPos(2, 34);
                }
                break;
            }
            case 'b': {
                if ((active0 & 0x1000L) != 0x0L) {
                    return this.jjStopAtPos(2, 12);
                }
                break;
            }
            case 'c': {
                if ((active0 & 0x400000L) != 0x0L) {
                    return this.jjStopAtPos(2, 22);
                }
                break;
            }
            case 'd': {
                if ((active0 & 0x40L) != 0x0L) {
                    return this.jjStopAtPos(2, 6);
                }
                break;
            }
            case 'e': {
                if ((active0 & 0x20L) != 0x0L) {
                    return this.jjStopAtPos(2, 5);
                }
                break;
            }
            case 'g': {
                if ((active0 & 0x40000L) != 0x0L) {
                    return this.jjStopAtPos(2, 18);
                }
                break;
            }
            case 'i': {
                if ((active0 & 0x100L) != 0x0L) {
                    return this.jjStopAtPos(2, 8);
                }
                break;
            }
            case 'l': {
                if ((active0 & 0x20000L) != 0x0L) {
                    return this.jjStopAtPos(2, 17);
                }
                break;
            }
            case 'n': {
                if ((active0 & 0x10L) != 0x0L) {
                    return this.jjStopAtPos(2, 4);
                }
                if ((active0 & 0x400L) != 0x0L) {
                    return this.jjStopAtPos(2, 10);
                }
                if ((active0 & 0x800L) != 0x0L) {
                    return this.jjStopAtPos(2, 11);
                }
                if ((active0 & 0x10000L) != 0x0L) {
                    return this.jjStopAtPos(2, 16);
                }
                break;
            }
            case 'p': {
                if ((active0 & 0x80000L) != 0x0L) {
                    return this.jjStopAtPos(2, 19);
                }
                break;
            }
            case 'r': {
                if ((active0 & 0x2000L) != 0x0L) {
                    return this.jjStopAtPos(2, 13);
                }
                if ((active0 & 0x4000L) != 0x0L) {
                    return this.jjStopAtPos(2, 14);
                }
                break;
            }
            case 't': {
                if ((active0 & 0x200L) != 0x0L) {
                    return this.jjStopAtPos(2, 9);
                }
                if ((active0 & 0x100000L) != 0x0L) {
                    return this.jjStopAtPos(2, 20);
                }
                break;
            }
            case 'u': {
                if ((active0 & 0x80L) != 0x0L) {
                    return this.jjStopAtPos(2, 7);
                }
                break;
            }
            case 'v': {
                if ((active0 & 0x200000L) != 0x0L) {
                    return this.jjStopAtPos(2, 21);
                }
                break;
            }
            case 'y': {
                if ((active0 & 0x8000L) != 0x0L) {
                    return this.jjStopAtPos(2, 15);
                }
                break;
            }
        }
        return this.jjStartNfa_0(1, active0);
    }
    
    private int jjMoveNfa_0(final int startState, int curPos) {
        int startsAt = 0;
        this.jjnewStateCnt = 4;
        int i = 1;
        this.jjstateSet[0] = startState;
        int kind = Integer.MAX_VALUE;
        while (true) {
            if (++this.jjround == Integer.MAX_VALUE) {
                this.ReInitRounds();
            }
            if (this.curChar < '@') {
                final long l = 1L << this.curChar;
                do {
                    switch (this.jjstateSet[--i]) {
                        case 0: {
                            if ((0x3FF000000000000L & l) != 0x0L) {
                                if (kind > 46) {
                                    kind = 46;
                                }
                                this.jjCheckNAdd(3);
                                continue;
                            }
                            if ((0x100000200L & l) != 0x0L) {
                                if (kind > 36) {
                                    kind = 36;
                                }
                                this.jjCheckNAdd(2);
                                continue;
                            }
                            if ((0x280000000000L & l) != 0x0L && kind > 24) {
                                kind = 24;
                                continue;
                            }
                            continue;
                        }
                        case 2: {
                            if ((0x100000200L & l) == 0x0L) {
                                continue;
                            }
                            kind = 36;
                            this.jjCheckNAdd(2);
                            continue;
                        }
                        case 3: {
                            if ((0x3FF000000000000L & l) == 0x0L) {
                                continue;
                            }
                            kind = 46;
                            this.jjCheckNAdd(3);
                            continue;
                        }
                        default: {
                            continue;
                        }
                    }
                } while (i != startsAt);
            }
            else if (this.curChar < '\u0080') {
                final long l = 1L << (this.curChar & '?');
                do {
                    switch (this.jjstateSet[--i]) {
                        case 0: {
                            if ((0x7FFFBFE07FFFBFEL & l) != 0x0L) {
                                kind = 35;
                                continue;
                            }
                            continue;
                        }
                        default: {
                            continue;
                        }
                    }
                } while (i != startsAt);
            }
            else {
                final int i2 = (this.curChar & '\u00ff') >> 6;
                final long l2 = 1L << (this.curChar & '?');
                do {
                    final int n = this.jjstateSet[--i];
                } while (i != startsAt);
            }
            if (kind != Integer.MAX_VALUE) {
                this.jjmatchedKind = kind;
                this.jjmatchedPos = curPos;
                kind = Integer.MAX_VALUE;
            }
            ++curPos;
            final int n2 = i = this.jjnewStateCnt;
            final int n3 = 4;
            final int jjnewStateCnt = startsAt;
            this.jjnewStateCnt = jjnewStateCnt;
            if (n2 == (startsAt = n3 - jjnewStateCnt)) {
                break;
            }
            try {
                this.curChar = this.input_stream.readChar();
            }
            catch (IOException e) {
                return curPos;
            }
        }
        return curPos;
    }
    
    private final int jjStopStringLiteralDfa_1(final int pos, final long active0) {
        return -1;
    }
    
    private final int jjStartNfa_1(final int pos, final long active0) {
        return this.jjMoveNfa_1(this.jjStopStringLiteralDfa_1(pos, active0), pos + 1);
    }
    
    private int jjMoveStringLiteralDfa0_1() {
        switch (this.curChar) {
            case '(': {
                return this.jjStopAtPos(0, 40);
            }
            case ')': {
                return this.jjStopAtPos(0, 38);
            }
            default: {
                return this.jjMoveNfa_1(0, 0);
            }
        }
    }
    
    private int jjMoveNfa_1(final int startState, int curPos) {
        int startsAt = 0;
        this.jjnewStateCnt = 3;
        int i = 1;
        this.jjstateSet[0] = startState;
        int kind = Integer.MAX_VALUE;
        while (true) {
            if (++this.jjround == Integer.MAX_VALUE) {
                this.ReInitRounds();
            }
            if (this.curChar < '@') {
                final long l = 1L << this.curChar;
                do {
                    switch (this.jjstateSet[--i]) {
                        case 0: {
                            if (kind > 41) {
                                kind = 41;
                                continue;
                            }
                            continue;
                        }
                        case 1: {
                            if (kind > 39) {
                                kind = 39;
                                continue;
                            }
                            continue;
                        }
                        default: {
                            continue;
                        }
                    }
                } while (i != startsAt);
            }
            else if (this.curChar < '\u0080') {
                final long l = 1L << (this.curChar & '?');
                do {
                    switch (this.jjstateSet[--i]) {
                        case 0: {
                            if (kind > 41) {
                                kind = 41;
                            }
                            if (this.curChar == '\\') {
                                this.jjstateSet[this.jjnewStateCnt++] = 1;
                                continue;
                            }
                            continue;
                        }
                        case 1: {
                            if (kind > 39) {
                                kind = 39;
                                continue;
                            }
                            continue;
                        }
                        case 2: {
                            if (kind > 41) {
                                kind = 41;
                                continue;
                            }
                            continue;
                        }
                        default: {
                            continue;
                        }
                    }
                } while (i != startsAt);
            }
            else {
                final int i2 = (this.curChar & '\u00ff') >> 6;
                final long l2 = 1L << (this.curChar & '?');
                do {
                    switch (this.jjstateSet[--i]) {
                        case 0: {
                            if ((DateTimeParserTokenManager.jjbitVec0[i2] & l2) != 0x0L && kind > 41) {
                                kind = 41;
                                continue;
                            }
                            continue;
                        }
                        case 1: {
                            if ((DateTimeParserTokenManager.jjbitVec0[i2] & l2) != 0x0L && kind > 39) {
                                kind = 39;
                                continue;
                            }
                            continue;
                        }
                        default: {
                            continue;
                        }
                    }
                } while (i != startsAt);
            }
            if (kind != Integer.MAX_VALUE) {
                this.jjmatchedKind = kind;
                this.jjmatchedPos = curPos;
                kind = Integer.MAX_VALUE;
            }
            ++curPos;
            final int n = i = this.jjnewStateCnt;
            final int n2 = 3;
            final int jjnewStateCnt = startsAt;
            this.jjnewStateCnt = jjnewStateCnt;
            if (n == (startsAt = n2 - jjnewStateCnt)) {
                break;
            }
            try {
                this.curChar = this.input_stream.readChar();
            }
            catch (IOException e) {
                return curPos;
            }
        }
        return curPos;
    }
    
    private final int jjStopStringLiteralDfa_2(final int pos, final long active0) {
        return -1;
    }
    
    private final int jjStartNfa_2(final int pos, final long active0) {
        return this.jjMoveNfa_2(this.jjStopStringLiteralDfa_2(pos, active0), pos + 1);
    }
    
    private int jjMoveStringLiteralDfa0_2() {
        switch (this.curChar) {
            case '(': {
                return this.jjStopAtPos(0, 43);
            }
            case ')': {
                return this.jjStopAtPos(0, 44);
            }
            default: {
                return this.jjMoveNfa_2(0, 0);
            }
        }
    }
    
    private int jjMoveNfa_2(final int startState, int curPos) {
        int startsAt = 0;
        this.jjnewStateCnt = 3;
        int i = 1;
        this.jjstateSet[0] = startState;
        int kind = Integer.MAX_VALUE;
        while (true) {
            if (++this.jjround == Integer.MAX_VALUE) {
                this.ReInitRounds();
            }
            if (this.curChar < '@') {
                final long l = 1L << this.curChar;
                do {
                    switch (this.jjstateSet[--i]) {
                        case 0: {
                            if (kind > 45) {
                                kind = 45;
                                continue;
                            }
                            continue;
                        }
                        case 1: {
                            if (kind > 42) {
                                kind = 42;
                                continue;
                            }
                            continue;
                        }
                        default: {
                            continue;
                        }
                    }
                } while (i != startsAt);
            }
            else if (this.curChar < '\u0080') {
                final long l = 1L << (this.curChar & '?');
                do {
                    switch (this.jjstateSet[--i]) {
                        case 0: {
                            if (kind > 45) {
                                kind = 45;
                            }
                            if (this.curChar == '\\') {
                                this.jjstateSet[this.jjnewStateCnt++] = 1;
                                continue;
                            }
                            continue;
                        }
                        case 1: {
                            if (kind > 42) {
                                kind = 42;
                                continue;
                            }
                            continue;
                        }
                        case 2: {
                            if (kind > 45) {
                                kind = 45;
                                continue;
                            }
                            continue;
                        }
                        default: {
                            continue;
                        }
                    }
                } while (i != startsAt);
            }
            else {
                final int i2 = (this.curChar & '\u00ff') >> 6;
                final long l2 = 1L << (this.curChar & '?');
                do {
                    switch (this.jjstateSet[--i]) {
                        case 0: {
                            if ((DateTimeParserTokenManager.jjbitVec0[i2] & l2) != 0x0L && kind > 45) {
                                kind = 45;
                                continue;
                            }
                            continue;
                        }
                        case 1: {
                            if ((DateTimeParserTokenManager.jjbitVec0[i2] & l2) != 0x0L && kind > 42) {
                                kind = 42;
                                continue;
                            }
                            continue;
                        }
                        default: {
                            continue;
                        }
                    }
                } while (i != startsAt);
            }
            if (kind != Integer.MAX_VALUE) {
                this.jjmatchedKind = kind;
                this.jjmatchedPos = curPos;
                kind = Integer.MAX_VALUE;
            }
            ++curPos;
            final int n = i = this.jjnewStateCnt;
            final int n2 = 3;
            final int jjnewStateCnt = startsAt;
            this.jjnewStateCnt = jjnewStateCnt;
            if (n == (startsAt = n2 - jjnewStateCnt)) {
                break;
            }
            try {
                this.curChar = this.input_stream.readChar();
            }
            catch (IOException e) {
                return curPos;
            }
        }
        return curPos;
    }
    
    public DateTimeParserTokenManager(final SimpleCharStream stream) {
        this.debugStream = System.out;
        this.jjrounds = new int[4];
        this.jjstateSet = new int[8];
        this.jjimage = new StringBuilder();
        this.image = this.jjimage;
        this.curLexState = 0;
        this.defaultLexState = 0;
        this.input_stream = stream;
    }
    
    public DateTimeParserTokenManager(final SimpleCharStream stream, final int lexState) {
        this(stream);
        this.SwitchTo(lexState);
    }
    
    public void ReInit(final SimpleCharStream stream) {
        final int n = 0;
        this.jjnewStateCnt = n;
        this.jjmatchedPos = n;
        this.curLexState = this.defaultLexState;
        this.input_stream = stream;
        this.ReInitRounds();
    }
    
    private void ReInitRounds() {
        this.jjround = -2147483647;
        int i = 4;
        while (i-- > 0) {
            this.jjrounds[i] = Integer.MIN_VALUE;
        }
    }
    
    public void ReInit(final SimpleCharStream stream, final int lexState) {
        this.ReInit(stream);
        this.SwitchTo(lexState);
    }
    
    public void SwitchTo(final int lexState) {
        if (lexState >= 3 || lexState < 0) {
            throw new TokenMgrError("Error: Ignoring invalid lexical state : " + lexState + ". State unchanged.", 2);
        }
        this.curLexState = lexState;
    }
    
    protected Token jjFillToken() {
        final String im = DateTimeParserTokenManager.jjstrLiteralImages[this.jjmatchedKind];
        final String curTokenImage = (im == null) ? this.input_stream.GetImage() : im;
        final int beginLine = this.input_stream.getBeginLine();
        final int beginColumn = this.input_stream.getBeginColumn();
        final int endLine = this.input_stream.getEndLine();
        final int endColumn = this.input_stream.getEndColumn();
        final Token t = Token.newToken(this.jjmatchedKind, curTokenImage);
        t.beginLine = beginLine;
        t.endLine = endLine;
        t.beginColumn = beginColumn;
        t.endColumn = endColumn;
        return t;
    }
    
    public Token getNextToken() {
        Token specialToken = null;
        int curPos = 0;
    Label_0395:
        while (true) {
            try {
                this.curChar = this.input_stream.BeginToken();
            }
            catch (IOException e) {
                this.jjmatchedKind = 0;
                final Token matchedToken = this.jjFillToken();
                matchedToken.specialToken = specialToken;
                return matchedToken;
            }
            (this.image = this.jjimage).setLength(0);
            this.jjimageLen = 0;
            while (true) {
                switch (this.curLexState) {
                    case 0: {
                        this.jjmatchedKind = Integer.MAX_VALUE;
                        this.jjmatchedPos = 0;
                        curPos = this.jjMoveStringLiteralDfa0_0();
                        break;
                    }
                    case 1: {
                        this.jjmatchedKind = Integer.MAX_VALUE;
                        this.jjmatchedPos = 0;
                        curPos = this.jjMoveStringLiteralDfa0_1();
                        break;
                    }
                    case 2: {
                        this.jjmatchedKind = Integer.MAX_VALUE;
                        this.jjmatchedPos = 0;
                        curPos = this.jjMoveStringLiteralDfa0_2();
                        break;
                    }
                }
                if (this.jjmatchedKind == Integer.MAX_VALUE) {
                    break Label_0395;
                }
                if (this.jjmatchedPos + 1 < curPos) {
                    this.input_stream.backup(curPos - this.jjmatchedPos - 1);
                }
                if ((DateTimeParserTokenManager.jjtoToken[this.jjmatchedKind >> 6] & 1L << (this.jjmatchedKind & 0x3F)) != 0x0L) {
                    final Token matchedToken = this.jjFillToken();
                    matchedToken.specialToken = specialToken;
                    if (DateTimeParserTokenManager.jjnewLexState[this.jjmatchedKind] != -1) {
                        this.curLexState = DateTimeParserTokenManager.jjnewLexState[this.jjmatchedKind];
                    }
                    return matchedToken;
                }
                if ((DateTimeParserTokenManager.jjtoSkip[this.jjmatchedKind >> 6] & 1L << (this.jjmatchedKind & 0x3F)) == 0x0L) {
                    this.MoreLexicalActions();
                    if (DateTimeParserTokenManager.jjnewLexState[this.jjmatchedKind] != -1) {
                        this.curLexState = DateTimeParserTokenManager.jjnewLexState[this.jjmatchedKind];
                    }
                    curPos = 0;
                    this.jjmatchedKind = Integer.MAX_VALUE;
                    try {
                        this.curChar = this.input_stream.readChar();
                        continue;
                    }
                    catch (IOException ex) {}
                    break Label_0395;
                }
                if ((DateTimeParserTokenManager.jjtoSpecial[this.jjmatchedKind >> 6] & 1L << (this.jjmatchedKind & 0x3F)) != 0x0L) {
                    final Token matchedToken = this.jjFillToken();
                    if (specialToken == null) {
                        specialToken = matchedToken;
                    }
                    else {
                        matchedToken.specialToken = specialToken;
                        final Token token = specialToken;
                        final Token next = matchedToken;
                        token.next = next;
                        specialToken = next;
                    }
                }
                if (DateTimeParserTokenManager.jjnewLexState[this.jjmatchedKind] != -1) {
                    this.curLexState = DateTimeParserTokenManager.jjnewLexState[this.jjmatchedKind];
                    break;
                }
                break;
            }
        }
        int error_line = this.input_stream.getEndLine();
        int error_column = this.input_stream.getEndColumn();
        String error_after = null;
        boolean EOFSeen = false;
        try {
            this.input_stream.readChar();
            this.input_stream.backup(1);
        }
        catch (IOException e2) {
            EOFSeen = true;
            error_after = ((curPos <= 1) ? "" : this.input_stream.GetImage());
            if (this.curChar == '\n' || this.curChar == '\r') {
                ++error_line;
                error_column = 0;
            }
            else {
                ++error_column;
            }
        }
        if (!EOFSeen) {
            this.input_stream.backup(1);
            error_after = ((curPos <= 1) ? "" : this.input_stream.GetImage());
        }
        throw new TokenMgrError(EOFSeen, this.curLexState, error_line, error_column, error_after, this.curChar, 0);
    }
    
    void MoreLexicalActions() {
        final int jjimageLen = this.jjimageLen;
        final int lengthOfMatch = this.jjmatchedPos + 1;
        this.lengthOfMatch = lengthOfMatch;
        this.jjimageLen = jjimageLen + lengthOfMatch;
        switch (this.jjmatchedKind) {
            case 39: {
                this.image.append(this.input_stream.GetSuffix(this.jjimageLen));
                this.jjimageLen = 0;
                this.image.deleteCharAt(this.image.length() - 2);
                break;
            }
            case 40: {
                this.image.append(this.input_stream.GetSuffix(this.jjimageLen));
                this.jjimageLen = 0;
                DateTimeParserTokenManager.commentNest = 1;
                break;
            }
            case 42: {
                this.image.append(this.input_stream.GetSuffix(this.jjimageLen));
                this.jjimageLen = 0;
                this.image.deleteCharAt(this.image.length() - 2);
                break;
            }
            case 43: {
                this.image.append(this.input_stream.GetSuffix(this.jjimageLen));
                this.jjimageLen = 0;
                ++DateTimeParserTokenManager.commentNest;
                break;
            }
            case 44: {
                this.image.append(this.input_stream.GetSuffix(this.jjimageLen));
                this.jjimageLen = 0;
                --DateTimeParserTokenManager.commentNest;
                if (DateTimeParserTokenManager.commentNest == 0) {
                    this.SwitchTo(1);
                    break;
                }
                break;
            }
        }
    }
    
    private void jjCheckNAdd(final int state) {
        if (this.jjrounds[state] != this.jjround) {
            this.jjstateSet[this.jjnewStateCnt++] = state;
            this.jjrounds[state] = this.jjround;
        }
    }
    
    private void jjAddStates(int start, final int end) {
        do {
            this.jjstateSet[this.jjnewStateCnt++] = DateTimeParserTokenManager.jjnextStates[start];
        } while (start++ != end);
    }
    
    private void jjCheckNAddTwoStates(final int state1, final int state2) {
        this.jjCheckNAdd(state1);
        this.jjCheckNAdd(state2);
    }
    
    static {
        jjbitVec0 = new long[] { 0L, 0L, -1L, -1L };
        jjnextStates = new int[0];
        jjstrLiteralImages = new String[] { "", "\r", "\n", ",", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun", "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec", ":", null, "UT", "GMT", "EST", "EDT", "CST", "CDT", "MST", "MDT", "PST", "PDT", null, null, null, null, null, null, null, null, null, null, null, null, null, null };
        lexStateNames = new String[] { "DEFAULT", "INCOMMENT", "NESTED_COMMENT" };
        jjnewLexState = new int[] { -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 1, 0, -1, 2, -1, -1, -1, -1, -1, -1, -1, -1 };
        jjtoToken = new long[] { 70437463654399L };
        jjtoSkip = new long[] { 343597383680L };
        jjtoSpecial = new long[] { 68719476736L };
        jjtoMore = new long[] { 69956427317248L };
    }
}
