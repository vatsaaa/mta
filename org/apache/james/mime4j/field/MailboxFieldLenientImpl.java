// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.james.mime4j.field;

import org.apache.james.mime4j.dom.field.ParsedField;
import org.apache.james.mime4j.util.ByteSequence;
import org.apache.james.mime4j.stream.RawField;
import java.util.BitSet;
import org.apache.james.mime4j.field.address.LenientAddressBuilder;
import org.apache.james.mime4j.stream.ParserCursor;
import org.apache.james.mime4j.util.ContentUtil;
import org.apache.james.mime4j.codec.DecodeMonitor;
import org.apache.james.mime4j.stream.Field;
import org.apache.james.mime4j.dom.FieldParser;
import org.apache.james.mime4j.dom.address.Mailbox;
import org.apache.james.mime4j.dom.field.MailboxField;

public class MailboxFieldLenientImpl extends AbstractField implements MailboxField
{
    private boolean parsed;
    private Mailbox mailbox;
    public static final FieldParser<MailboxField> PARSER;
    
    MailboxFieldLenientImpl(final Field rawField, final DecodeMonitor monitor) {
        super(rawField, monitor);
        this.parsed = false;
    }
    
    public Mailbox getMailbox() {
        if (!this.parsed) {
            this.parse();
        }
        return this.mailbox;
    }
    
    private void parse() {
        this.parsed = true;
        final RawField f = this.getRawField();
        ByteSequence buf = f.getRaw();
        int pos = f.getDelimiterIdx() + 1;
        if (buf == null) {
            final String body = f.getBody();
            if (body == null) {
                return;
            }
            buf = ContentUtil.encode(body);
            pos = 0;
        }
        final ParserCursor cursor = new ParserCursor(pos, buf.length());
        this.mailbox = LenientAddressBuilder.DEFAULT.parseMailbox(buf, cursor, null);
    }
    
    static {
        PARSER = new FieldParser<MailboxField>() {
            public MailboxField parse(final Field rawField, final DecodeMonitor monitor) {
                return new MailboxFieldLenientImpl(rawField, monitor);
            }
        };
    }
}
