// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.james.mime4j.field.address;

public class ASTaddress_list extends SimpleNode
{
    public ASTaddress_list(final int id) {
        super(id);
    }
    
    public ASTaddress_list(final AddressListParser p, final int id) {
        super(p, id);
    }
    
    @Override
    public Object jjtAccept(final AddressListParserVisitor visitor, final Object data) {
        return visitor.visit(this, data);
    }
}
