// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.james.mime4j.field.address;

import java.util.Iterator;
import org.apache.james.mime4j.dom.address.DomainList;
import org.apache.james.mime4j.dom.address.MailboxList;
import org.apache.james.mime4j.dom.address.Mailbox;
import org.apache.james.mime4j.codec.DecoderUtil;
import org.apache.james.mime4j.dom.address.Group;
import java.util.List;
import org.apache.james.mime4j.dom.address.Address;
import java.util.ArrayList;
import org.apache.james.mime4j.dom.address.AddressList;
import org.apache.james.mime4j.codec.DecodeMonitor;

class Builder
{
    private static Builder singleton;
    
    public static Builder getInstance() {
        return Builder.singleton;
    }
    
    public AddressList buildAddressList(final ASTaddress_list node, final DecodeMonitor monitor) throws ParseException {
        final List<Address> list = new ArrayList<Address>();
        for (int i = 0; i < node.jjtGetNumChildren(); ++i) {
            final ASTaddress childNode = (ASTaddress)node.jjtGetChild(i);
            final Address address = this.buildAddress(childNode, monitor);
            list.add(address);
        }
        return new AddressList(list, true);
    }
    
    public Address buildAddress(final ASTaddress node, final DecodeMonitor monitor) throws ParseException {
        final ChildNodeIterator it = new ChildNodeIterator(node);
        final Node n = it.next();
        if (n instanceof ASTaddr_spec) {
            return this.buildAddrSpec((ASTaddr_spec)n);
        }
        if (n instanceof ASTangle_addr) {
            return this.buildAngleAddr((ASTangle_addr)n);
        }
        if (!(n instanceof ASTphrase)) {
            throw new ParseException();
        }
        String name = this.buildString((SimpleNode)n, false);
        final Node n2 = it.next();
        if (n2 instanceof ASTgroup_body) {
            return new Group(name, this.buildGroupBody((ASTgroup_body)n2, monitor));
        }
        if (n2 instanceof ASTangle_addr) {
            try {
                name = DecoderUtil.decodeEncodedWords(name, monitor);
            }
            catch (IllegalArgumentException e) {
                throw new ParseException(e.getMessage());
            }
            final Mailbox mb = this.buildAngleAddr((ASTangle_addr)n2);
            return new Mailbox(name, mb.getRoute(), mb.getLocalPart(), mb.getDomain());
        }
        throw new ParseException();
    }
    
    private MailboxList buildGroupBody(final ASTgroup_body node, final DecodeMonitor monitor) throws ParseException {
        final List<Mailbox> results = new ArrayList<Mailbox>();
        final ChildNodeIterator it = new ChildNodeIterator(node);
        while (it.hasNext()) {
            final Node n = it.next();
            if (!(n instanceof ASTmailbox)) {
                throw new ParseException();
            }
            results.add(this.buildMailbox((ASTmailbox)n, monitor));
        }
        return new MailboxList(results, true);
    }
    
    public Mailbox buildMailbox(final ASTmailbox node, final DecodeMonitor monitor) throws ParseException {
        final ChildNodeIterator it = new ChildNodeIterator(node);
        final Node n = it.next();
        if (n instanceof ASTaddr_spec) {
            return this.buildAddrSpec((ASTaddr_spec)n);
        }
        if (n instanceof ASTangle_addr) {
            return this.buildAngleAddr((ASTangle_addr)n);
        }
        if (n instanceof ASTname_addr) {
            return this.buildNameAddr((ASTname_addr)n, monitor);
        }
        throw new ParseException();
    }
    
    private Mailbox buildNameAddr(final ASTname_addr node, final DecodeMonitor monitor) throws ParseException {
        final ChildNodeIterator it = new ChildNodeIterator(node);
        Node n = it.next();
        if (!(n instanceof ASTphrase)) {
            throw new ParseException();
        }
        String name = this.buildString((SimpleNode)n, false);
        n = it.next();
        if (n instanceof ASTangle_addr) {
            try {
                name = DecoderUtil.decodeEncodedWords(name, monitor);
            }
            catch (IllegalArgumentException e) {
                throw new ParseException(e.getMessage());
            }
            final Mailbox mb = this.buildAngleAddr((ASTangle_addr)n);
            return new Mailbox(name, mb.getRoute(), mb.getLocalPart(), mb.getDomain());
        }
        throw new ParseException();
    }
    
    private Mailbox buildAngleAddr(final ASTangle_addr node) throws ParseException {
        final ChildNodeIterator it = new ChildNodeIterator(node);
        DomainList route = null;
        Node n = it.next();
        if (n instanceof ASTroute) {
            route = this.buildRoute((ASTroute)n);
            n = it.next();
        }
        else if (!(n instanceof ASTaddr_spec)) {
            throw new ParseException();
        }
        if (n instanceof ASTaddr_spec) {
            return this.buildAddrSpec(route, (ASTaddr_spec)n);
        }
        throw new ParseException();
    }
    
    private DomainList buildRoute(final ASTroute node) throws ParseException {
        final List<String> results = new ArrayList<String>(node.jjtGetNumChildren());
        final ChildNodeIterator it = new ChildNodeIterator(node);
        while (it.hasNext()) {
            final Node n = it.next();
            if (!(n instanceof ASTdomain)) {
                throw new ParseException();
            }
            results.add(this.buildString((SimpleNode)n, true));
        }
        return new DomainList(results, true);
    }
    
    private Mailbox buildAddrSpec(final ASTaddr_spec node) {
        return this.buildAddrSpec(null, node);
    }
    
    private Mailbox buildAddrSpec(final DomainList route, final ASTaddr_spec node) {
        final ChildNodeIterator it = new ChildNodeIterator(node);
        final String localPart = this.buildString((SimpleNode)it.next(), true);
        final String domain = this.buildString((SimpleNode)it.next(), true);
        return new Mailbox(route, localPart, domain);
    }
    
    private String buildString(final SimpleNode node, final boolean stripSpaces) {
        Token head = node.firstToken;
        final Token tail = node.lastToken;
        final StringBuilder out = new StringBuilder();
        while (head != tail) {
            out.append(head.image);
            head = head.next;
            if (!stripSpaces) {
                this.addSpecials(out, head.specialToken);
            }
        }
        out.append(tail.image);
        return out.toString();
    }
    
    private void addSpecials(final StringBuilder out, final Token specialToken) {
        if (specialToken != null) {
            this.addSpecials(out, specialToken.specialToken);
            out.append(specialToken.image);
        }
    }
    
    static {
        Builder.singleton = new Builder();
    }
    
    private static class ChildNodeIterator implements Iterator<Node>
    {
        private SimpleNode simpleNode;
        private int index;
        private int len;
        
        public ChildNodeIterator(final SimpleNode simpleNode) {
            this.simpleNode = simpleNode;
            this.len = simpleNode.jjtGetNumChildren();
            this.index = 0;
        }
        
        public void remove() {
            throw new UnsupportedOperationException();
        }
        
        public boolean hasNext() {
            return this.index < this.len;
        }
        
        public Node next() {
            return this.simpleNode.jjtGetChild(this.index++);
        }
    }
}
