// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.james.mime4j.field.address;

import java.util.Iterator;
import org.apache.james.mime4j.codec.EncoderUtil;
import org.apache.james.mime4j.dom.address.Group;
import org.apache.james.mime4j.dom.address.Mailbox;
import org.apache.james.mime4j.dom.address.Address;

public class AddressFormatter
{
    public static final AddressFormatter DEFAULT;
    
    protected AddressFormatter() {
    }
    
    public void format(final StringBuilder sb, final Address address, final boolean includeRoute) {
        if (address == null) {
            return;
        }
        if (address instanceof Mailbox) {
            this.format(sb, (Mailbox)address, includeRoute);
        }
        else {
            if (!(address instanceof Group)) {
                throw new IllegalArgumentException("Unsuppported Address class: " + address.getClass());
            }
            this.format(sb, (Group)address, includeRoute);
        }
    }
    
    public void encode(final StringBuilder sb, final Address address) {
        if (address == null) {
            return;
        }
        if (address instanceof Mailbox) {
            this.encode(sb, (Mailbox)address);
        }
        else {
            if (!(address instanceof Group)) {
                throw new IllegalArgumentException("Unsuppported Address class: " + address.getClass());
            }
            this.encode(sb, (Group)address);
        }
    }
    
    public void format(final StringBuilder sb, final Mailbox mailbox, boolean includeRoute) {
        if (sb == null) {
            throw new IllegalArgumentException("StringBuilder may not be null");
        }
        if (mailbox == null) {
            throw new IllegalArgumentException("Mailbox may not be null");
        }
        includeRoute &= (mailbox.getRoute() != null);
        final boolean includeAngleBrackets = mailbox.getName() != null || includeRoute;
        if (mailbox.getName() != null) {
            sb.append(mailbox.getName());
            sb.append(' ');
        }
        if (includeAngleBrackets) {
            sb.append('<');
        }
        if (includeRoute) {
            sb.append(mailbox.getRoute().toRouteString());
            sb.append(':');
        }
        sb.append(mailbox.getLocalPart());
        if (mailbox.getDomain() != null) {
            sb.append('@');
            sb.append(mailbox.getDomain());
        }
        if (includeAngleBrackets) {
            sb.append('>');
        }
    }
    
    public String format(final Mailbox mailbox, final boolean includeRoute) {
        final StringBuilder sb = new StringBuilder();
        this.format(sb, mailbox, includeRoute);
        return sb.toString();
    }
    
    public void encode(final StringBuilder sb, final Mailbox mailbox) {
        if (sb == null) {
            throw new IllegalArgumentException("StringBuilder may not be null");
        }
        if (mailbox == null) {
            throw new IllegalArgumentException("Mailbox may not be null");
        }
        if (mailbox.getName() != null) {
            sb.append(EncoderUtil.encodeAddressDisplayName(mailbox.getName()));
            sb.append(" <");
        }
        sb.append(EncoderUtil.encodeAddressLocalPart(mailbox.getLocalPart()));
        if (mailbox.getDomain() != null) {
            sb.append('@');
            sb.append(mailbox.getDomain());
        }
        if (mailbox.getName() != null) {
            sb.append('>');
        }
    }
    
    public String encode(final Mailbox mailbox) {
        final StringBuilder sb = new StringBuilder();
        this.encode(sb, mailbox);
        return sb.toString();
    }
    
    public void format(final StringBuilder sb, final Group group, final boolean includeRoute) {
        if (sb == null) {
            throw new IllegalArgumentException("StringBuilder may not be null");
        }
        if (group == null) {
            throw new IllegalArgumentException("Group may not be null");
        }
        sb.append(group.getName());
        sb.append(':');
        boolean first = true;
        for (final Mailbox mailbox : group.getMailboxes()) {
            if (first) {
                first = false;
            }
            else {
                sb.append(',');
            }
            sb.append(' ');
            this.format(sb, mailbox, includeRoute);
        }
        sb.append(";");
    }
    
    public String format(final Group group, final boolean includeRoute) {
        final StringBuilder sb = new StringBuilder();
        this.format(sb, group, includeRoute);
        return sb.toString();
    }
    
    public void encode(final StringBuilder sb, final Group group) {
        if (sb == null) {
            throw new IllegalArgumentException("StringBuilder may not be null");
        }
        if (group == null) {
            throw new IllegalArgumentException("Group may not be null");
        }
        sb.append(EncoderUtil.encodeAddressDisplayName(group.getName()));
        sb.append(':');
        boolean first = true;
        for (final Mailbox mailbox : group.getMailboxes()) {
            if (first) {
                first = false;
            }
            else {
                sb.append(',');
            }
            sb.append(' ');
            this.encode(sb, mailbox);
        }
        sb.append(';');
    }
    
    public String encode(final Group group) {
        final StringBuilder sb = new StringBuilder();
        this.encode(sb, group);
        return sb.toString();
    }
    
    static {
        DEFAULT = new AddressFormatter();
    }
}
