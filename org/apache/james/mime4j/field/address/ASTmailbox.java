// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.james.mime4j.field.address;

public class ASTmailbox extends SimpleNode
{
    public ASTmailbox(final int id) {
        super(id);
    }
    
    public ASTmailbox(final AddressListParser p, final int id) {
        super(p, id);
    }
    
    @Override
    public Object jjtAccept(final AddressListParserVisitor visitor, final Object data) {
        return visitor.visit(this, data);
    }
}
