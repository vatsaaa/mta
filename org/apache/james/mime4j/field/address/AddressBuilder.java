// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.james.mime4j.field.address;

import org.apache.james.mime4j.dom.address.Group;
import org.apache.james.mime4j.dom.address.Mailbox;
import org.apache.james.mime4j.dom.address.AddressList;
import java.io.Reader;
import java.io.StringReader;
import org.apache.james.mime4j.dom.address.Address;
import org.apache.james.mime4j.codec.DecodeMonitor;

public class AddressBuilder
{
    public static final AddressBuilder DEFAULT;
    
    protected AddressBuilder() {
    }
    
    public Address parseAddress(final String rawAddressString, final DecodeMonitor monitor) throws ParseException {
        final AddressListParser parser = new AddressListParser(new StringReader(rawAddressString));
        return Builder.getInstance().buildAddress(parser.parseAddress(), monitor);
    }
    
    public Address parseAddress(final String rawAddressString) throws ParseException {
        return this.parseAddress(rawAddressString, DecodeMonitor.STRICT);
    }
    
    public AddressList parseAddressList(final String rawAddressList, final DecodeMonitor monitor) throws ParseException {
        final AddressListParser parser = new AddressListParser(new StringReader(rawAddressList));
        return Builder.getInstance().buildAddressList(parser.parseAddressList(), monitor);
    }
    
    public AddressList parseAddressList(final String rawAddressList) throws ParseException {
        return this.parseAddressList(rawAddressList, DecodeMonitor.STRICT);
    }
    
    public Mailbox parseMailbox(final String rawMailboxString, final DecodeMonitor monitor) throws ParseException {
        final AddressListParser parser = new AddressListParser(new StringReader(rawMailboxString));
        return Builder.getInstance().buildMailbox(parser.parseMailbox(), monitor);
    }
    
    public Mailbox parseMailbox(final String rawMailboxString) throws ParseException {
        return this.parseMailbox(rawMailboxString, DecodeMonitor.STRICT);
    }
    
    public Group parseGroup(final String rawGroupString, final DecodeMonitor monitor) throws ParseException {
        final Address address = this.parseAddress(rawGroupString, monitor);
        if (!(address instanceof Group)) {
            throw new ParseException("Not a group address");
        }
        return (Group)address;
    }
    
    public Group parseGroup(final String rawGroupString) throws ParseException {
        return this.parseGroup(rawGroupString, DecodeMonitor.STRICT);
    }
    
    static {
        DEFAULT = new AddressBuilder();
    }
}
