// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.james.mime4j.field.address;

import org.apache.james.mime4j.dom.address.AddressList;
import org.apache.james.mime4j.dom.address.Address;
import java.util.Collection;
import java.util.Collections;
import org.apache.james.mime4j.dom.address.Group;
import org.apache.james.mime4j.util.ContentUtil;
import org.apache.james.mime4j.codec.DecoderUtil;
import org.apache.james.mime4j.dom.address.Mailbox;
import java.util.List;
import java.util.ArrayList;
import org.apache.james.mime4j.dom.address.DomainList;
import org.apache.james.mime4j.util.CharsetUtil;
import org.apache.james.mime4j.stream.ParserCursor;
import org.apache.james.mime4j.util.ByteSequence;
import org.apache.james.mime4j.stream.RawFieldParser;
import org.apache.james.mime4j.codec.DecodeMonitor;
import java.util.BitSet;

public class LenientAddressBuilder
{
    private static final int AT = 64;
    private static final int OPENING_BRACKET = 60;
    private static final int CLOSING_BRACKET = 62;
    private static final int COMMA = 44;
    private static final int COLON = 58;
    private static final int SEMICOLON = 59;
    private static final BitSet AT_AND_CLOSING_BRACKET;
    private static final BitSet CLOSING_BRACKET_ONLY;
    private static final BitSet COMMA_ONLY;
    private static final BitSet COLON_ONLY;
    private static final BitSet SEMICOLON_ONLY;
    public static final LenientAddressBuilder DEFAULT;
    private final DecodeMonitor monitor;
    private final RawFieldParser parser;
    
    protected LenientAddressBuilder(final DecodeMonitor monitor) {
        this.monitor = monitor;
        this.parser = new RawFieldParser();
    }
    
    String parseDomain(final ByteSequence buf, final ParserCursor cursor, final BitSet delimiters) {
        final StringBuilder dst = new StringBuilder();
        while (!cursor.atEnd()) {
            final char current = (char)(buf.byteAt(cursor.getPos()) & 0xFF);
            if (delimiters != null && delimiters.get(current)) {
                break;
            }
            if (CharsetUtil.isWhitespace(current)) {
                this.parser.skipWhiteSpace(buf, cursor);
            }
            else if (current == '(') {
                this.parser.skipComment(buf, cursor);
            }
            else {
                this.parser.copyContent(buf, cursor, delimiters, dst);
            }
        }
        return dst.toString();
    }
    
    DomainList parseRoute(final ByteSequence buf, final ParserCursor cursor, final BitSet delimiters) {
        final BitSet bitset = RawFieldParser.INIT_BITSET(44, 58);
        if (delimiters != null) {
            bitset.or(delimiters);
        }
        List<String> domains = null;
        while (true) {
            this.parser.skipAllWhiteSpace(buf, cursor);
            if (cursor.atEnd()) {
                break;
            }
            int pos = cursor.getPos();
            int current = (char)(buf.byteAt(pos) & 0xFF);
            if (current != 64) {
                break;
            }
            cursor.updatePos(pos + 1);
            final String s = this.parseDomain(buf, cursor, bitset);
            if (s != null && s.length() > 0) {
                if (domains == null) {
                    domains = new ArrayList<String>();
                }
                domains.add(s);
            }
            if (cursor.atEnd()) {
                break;
            }
            pos = cursor.getPos();
            current = (char)(buf.byteAt(pos) & 0xFF);
            if (current == 44) {
                cursor.updatePos(pos + 1);
            }
            else {
                if (current == 58) {
                    cursor.updatePos(pos + 1);
                    break;
                }
                break;
            }
        }
        return (domains != null) ? new DomainList(domains, true) : null;
    }
    
    private Mailbox createMailbox(final String name, final DomainList route, final String localPart, final String domain) {
        return new Mailbox((name != null) ? DecoderUtil.decodeEncodedWords(name, this.monitor) : null, route, localPart, domain);
    }
    
    Mailbox parseMailboxAddress(final String openingText, final ByteSequence buf, final ParserCursor cursor) {
        if (cursor.atEnd()) {
            return this.createMailbox(null, null, openingText, null);
        }
        int pos = cursor.getPos();
        char current = (char)(buf.byteAt(pos) & 0xFF);
        if (current != '<') {
            return this.createMailbox(null, null, openingText, null);
        }
        cursor.updatePos(pos + 1);
        final DomainList domainList = this.parseRoute(buf, cursor, LenientAddressBuilder.CLOSING_BRACKET_ONLY);
        final String localPart = this.parser.parseValue(buf, cursor, LenientAddressBuilder.AT_AND_CLOSING_BRACKET);
        if (cursor.atEnd()) {
            return this.createMailbox(openingText, domainList, localPart, null);
        }
        pos = cursor.getPos();
        current = (char)(buf.byteAt(pos) & 0xFF);
        if (current != '@') {
            return this.createMailbox(openingText, domainList, localPart, null);
        }
        cursor.updatePos(pos + 1);
        final String domain = this.parseDomain(buf, cursor, LenientAddressBuilder.CLOSING_BRACKET_ONLY);
        if (cursor.atEnd()) {
            return this.createMailbox(openingText, domainList, localPart, domain);
        }
        pos = cursor.getPos();
        current = (char)(buf.byteAt(pos) & 0xFF);
        if (current == '>') {
            cursor.updatePos(pos + 1);
            while (!cursor.atEnd()) {
                pos = cursor.getPos();
                current = (char)(buf.byteAt(pos) & 0xFF);
                if (CharsetUtil.isWhitespace(current)) {
                    this.parser.skipWhiteSpace(buf, cursor);
                }
                else {
                    if (current != '(') {
                        break;
                    }
                    this.parser.skipComment(buf, cursor);
                }
            }
            return this.createMailbox(openingText, domainList, localPart, domain);
        }
        return this.createMailbox(openingText, domainList, localPart, domain);
    }
    
    private Mailbox createMailbox(final String localPart) {
        if (localPart != null && localPart.length() > 0) {
            return new Mailbox(null, null, localPart, null);
        }
        return null;
    }
    
    public Mailbox parseMailbox(final ByteSequence buf, final ParserCursor cursor, final BitSet delimiters) {
        final BitSet bitset = RawFieldParser.INIT_BITSET(64, 60);
        if (delimiters != null) {
            bitset.or(delimiters);
        }
        final String openingText = this.parser.parseValue(buf, cursor, bitset);
        if (cursor.atEnd()) {
            return this.createMailbox(openingText);
        }
        final int pos = cursor.getPos();
        final char current = (char)(buf.byteAt(pos) & 0xFF);
        if (current == '<') {
            return this.parseMailboxAddress(openingText, buf, cursor);
        }
        if (current == '@') {
            cursor.updatePos(pos + 1);
            final String localPart = openingText;
            final String domain = this.parseDomain(buf, cursor, delimiters);
            return new Mailbox(null, null, localPart, domain);
        }
        return this.createMailbox(openingText);
    }
    
    public Mailbox parseMailbox(final String text) {
        final ByteSequence raw = ContentUtil.encode(text);
        final ParserCursor cursor = new ParserCursor(0, text.length());
        return this.parseMailbox(raw, cursor, null);
    }
    
    List<Mailbox> parseMailboxes(final ByteSequence buf, final ParserCursor cursor, final BitSet delimiters) {
        final BitSet bitset = RawFieldParser.INIT_BITSET(44);
        if (delimiters != null) {
            bitset.or(delimiters);
        }
        final List<Mailbox> mboxes = new ArrayList<Mailbox>();
        while (!cursor.atEnd()) {
            final int pos = cursor.getPos();
            final int current = (char)(buf.byteAt(pos) & 0xFF);
            if (delimiters != null && delimiters.get(current)) {
                break;
            }
            if (current == 44) {
                cursor.updatePos(pos + 1);
            }
            else {
                final Mailbox mbox = this.parseMailbox(buf, cursor, bitset);
                if (mbox == null) {
                    continue;
                }
                mboxes.add(mbox);
            }
        }
        return mboxes;
    }
    
    public Group parseGroup(final ByteSequence buf, final ParserCursor cursor) {
        final String name = this.parser.parseToken(buf, cursor, LenientAddressBuilder.COLON_ONLY);
        if (cursor.atEnd()) {
            return new Group(name, (Collection<Mailbox>)Collections.emptyList());
        }
        final int pos = cursor.getPos();
        final int current = (char)(buf.byteAt(pos) & 0xFF);
        if (current == 58) {
            cursor.updatePos(pos + 1);
        }
        final List<Mailbox> mboxes = this.parseMailboxes(buf, cursor, LenientAddressBuilder.SEMICOLON_ONLY);
        return new Group(name, mboxes);
    }
    
    public Group parseGroup(final String text) {
        final ByteSequence raw = ContentUtil.encode(text);
        final ParserCursor cursor = new ParserCursor(0, text.length());
        return this.parseGroup(raw, cursor);
    }
    
    public Address parseAddress(final ByteSequence buf, final ParserCursor cursor, final BitSet delimiters) {
        final BitSet bitset = RawFieldParser.INIT_BITSET(58, 64, 60);
        if (delimiters != null) {
            bitset.or(delimiters);
        }
        final String openingText = this.parser.parseValue(buf, cursor, bitset);
        if (cursor.atEnd()) {
            return this.createMailbox(openingText);
        }
        int pos = cursor.getPos();
        char current = (char)(buf.byteAt(pos) & 0xFF);
        if (current == '<') {
            return this.parseMailboxAddress(openingText, buf, cursor);
        }
        if (current == '@') {
            cursor.updatePos(pos + 1);
            final String localPart = openingText;
            final String domain = this.parseDomain(buf, cursor, delimiters);
            return new Mailbox(null, null, localPart, domain);
        }
        if (current == ':') {
            cursor.updatePos(pos + 1);
            final String name = openingText;
            final List<Mailbox> mboxes = this.parseMailboxes(buf, cursor, LenientAddressBuilder.SEMICOLON_ONLY);
            if (!cursor.atEnd()) {
                pos = cursor.getPos();
                current = (char)(buf.byteAt(pos) & 0xFF);
                if (current == ';') {
                    cursor.updatePos(pos + 1);
                }
            }
            return new Group(name, mboxes);
        }
        return this.createMailbox(openingText);
    }
    
    public Address parseAddress(final String text) {
        final ByteSequence raw = ContentUtil.encode(text);
        final ParserCursor cursor = new ParserCursor(0, text.length());
        return this.parseAddress(raw, cursor, null);
    }
    
    public AddressList parseAddressList(final ByteSequence buf, final ParserCursor cursor) {
        final List<Address> addresses = new ArrayList<Address>();
        while (!cursor.atEnd()) {
            final int pos = cursor.getPos();
            final int current = (char)(buf.byteAt(pos) & 0xFF);
            if (current == 44) {
                cursor.updatePos(pos + 1);
            }
            else {
                final Address address = this.parseAddress(buf, cursor, LenientAddressBuilder.COMMA_ONLY);
                if (address == null) {
                    continue;
                }
                addresses.add(address);
            }
        }
        return new AddressList(addresses, false);
    }
    
    public AddressList parseAddressList(final String text) {
        final ByteSequence raw = ContentUtil.encode(text);
        final ParserCursor cursor = new ParserCursor(0, text.length());
        return this.parseAddressList(raw, cursor);
    }
    
    static {
        AT_AND_CLOSING_BRACKET = RawFieldParser.INIT_BITSET(64, 62);
        CLOSING_BRACKET_ONLY = RawFieldParser.INIT_BITSET(62);
        COMMA_ONLY = RawFieldParser.INIT_BITSET(44);
        COLON_ONLY = RawFieldParser.INIT_BITSET(58);
        SEMICOLON_ONLY = RawFieldParser.INIT_BITSET(59);
        DEFAULT = new LenientAddressBuilder(DecodeMonitor.SILENT);
    }
}
