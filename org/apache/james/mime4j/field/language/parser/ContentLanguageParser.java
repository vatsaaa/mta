// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.james.mime4j.field.language.parser;

import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.io.InputStream;
import java.util.List;

public class ContentLanguageParser implements ContentLanguageParserConstants
{
    private List<String> languages;
    public ContentLanguageParserTokenManager token_source;
    SimpleCharStream jj_input_stream;
    public Token token;
    public Token jj_nt;
    private int jj_ntk;
    private int jj_gen;
    private final int[] jj_la1;
    private static int[] jj_la1_0;
    private List<int[]> jj_expentries;
    private int[] jj_expentry;
    private int jj_kind;
    
    public List<String> parse() throws ParseException {
        try {
            return this.doParse();
        }
        catch (TokenMgrError e) {
            throw new ParseException(e);
        }
    }
    
    private final List<String> doParse() throws ParseException {
        this.language();
        while (true) {
            switch ((this.jj_ntk == -1) ? this.jj_ntk() : this.jj_ntk) {
                case 1: {
                    this.jj_consume_token(1);
                    this.language();
                    continue;
                }
                default: {
                    this.jj_la1[0] = this.jj_gen;
                    return this.languages;
                }
            }
        }
    }
    
    public final String language() throws ParseException {
        final StringBuffer languageTag = new StringBuffer();
        Token token = this.jj_consume_token(18);
        languageTag.append(token.image);
        while (true) {
            switch ((this.jj_ntk == -1) ? this.jj_ntk() : this.jj_ntk) {
                case 2:
                case 19: {
                    switch ((this.jj_ntk == -1) ? this.jj_ntk() : this.jj_ntk) {
                        case 2: {
                            this.jj_consume_token(2);
                            token = this.jj_consume_token(18);
                            languageTag.append('-');
                            languageTag.append(token.image);
                            continue;
                        }
                        case 19: {
                            token = this.jj_consume_token(19);
                            languageTag.append('-');
                            languageTag.append(token.image);
                            continue;
                        }
                        default: {
                            this.jj_la1[2] = this.jj_gen;
                            this.jj_consume_token(-1);
                            throw new ParseException();
                        }
                    }
                    break;
                }
                default: {
                    this.jj_la1[1] = this.jj_gen;
                    final String result = languageTag.toString();
                    this.languages.add(result);
                    return result;
                }
            }
        }
    }
    
    private static void jj_la1_init_0() {
        ContentLanguageParser.jj_la1_0 = new int[] { 2, 524292, 524292 };
    }
    
    public ContentLanguageParser(final InputStream stream) {
        this(stream, null);
    }
    
    public ContentLanguageParser(final InputStream stream, final String encoding) {
        this.languages = new ArrayList<String>();
        this.jj_la1 = new int[3];
        this.jj_expentries = new ArrayList<int[]>();
        this.jj_kind = -1;
        try {
            this.jj_input_stream = new SimpleCharStream(stream, encoding, 1, 1);
        }
        catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
        this.token_source = new ContentLanguageParserTokenManager(this.jj_input_stream);
        this.token = new Token();
        this.jj_ntk = -1;
        this.jj_gen = 0;
        for (int i = 0; i < 3; ++i) {
            this.jj_la1[i] = -1;
        }
    }
    
    public void ReInit(final InputStream stream) {
        this.ReInit(stream, null);
    }
    
    public void ReInit(final InputStream stream, final String encoding) {
        try {
            this.jj_input_stream.ReInit(stream, encoding, 1, 1);
        }
        catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
        this.token_source.ReInit(this.jj_input_stream);
        this.token = new Token();
        this.jj_ntk = -1;
        this.jj_gen = 0;
        for (int i = 0; i < 3; ++i) {
            this.jj_la1[i] = -1;
        }
    }
    
    public ContentLanguageParser(final Reader stream) {
        this.languages = new ArrayList<String>();
        this.jj_la1 = new int[3];
        this.jj_expentries = new ArrayList<int[]>();
        this.jj_kind = -1;
        this.jj_input_stream = new SimpleCharStream(stream, 1, 1);
        this.token_source = new ContentLanguageParserTokenManager(this.jj_input_stream);
        this.token = new Token();
        this.jj_ntk = -1;
        this.jj_gen = 0;
        for (int i = 0; i < 3; ++i) {
            this.jj_la1[i] = -1;
        }
    }
    
    public void ReInit(final Reader stream) {
        this.jj_input_stream.ReInit(stream, 1, 1);
        this.token_source.ReInit(this.jj_input_stream);
        this.token = new Token();
        this.jj_ntk = -1;
        this.jj_gen = 0;
        for (int i = 0; i < 3; ++i) {
            this.jj_la1[i] = -1;
        }
    }
    
    public ContentLanguageParser(final ContentLanguageParserTokenManager tm) {
        this.languages = new ArrayList<String>();
        this.jj_la1 = new int[3];
        this.jj_expentries = new ArrayList<int[]>();
        this.jj_kind = -1;
        this.token_source = tm;
        this.token = new Token();
        this.jj_ntk = -1;
        this.jj_gen = 0;
        for (int i = 0; i < 3; ++i) {
            this.jj_la1[i] = -1;
        }
    }
    
    public void ReInit(final ContentLanguageParserTokenManager tm) {
        this.token_source = tm;
        this.token = new Token();
        this.jj_ntk = -1;
        this.jj_gen = 0;
        for (int i = 0; i < 3; ++i) {
            this.jj_la1[i] = -1;
        }
    }
    
    private Token jj_consume_token(final int kind) throws ParseException {
        final Token oldToken;
        if ((oldToken = this.token).next != null) {
            this.token = this.token.next;
        }
        else {
            final Token token = this.token;
            final Token nextToken = this.token_source.getNextToken();
            token.next = nextToken;
            this.token = nextToken;
        }
        this.jj_ntk = -1;
        if (this.token.kind == kind) {
            ++this.jj_gen;
            return this.token;
        }
        this.token = oldToken;
        this.jj_kind = kind;
        throw this.generateParseException();
    }
    
    public final Token getNextToken() {
        if (this.token.next != null) {
            this.token = this.token.next;
        }
        else {
            final Token token = this.token;
            final Token nextToken = this.token_source.getNextToken();
            token.next = nextToken;
            this.token = nextToken;
        }
        this.jj_ntk = -1;
        ++this.jj_gen;
        return this.token;
    }
    
    public final Token getToken(final int index) {
        Token t = this.token;
        for (int i = 0; i < index; ++i) {
            if (t.next != null) {
                t = t.next;
            }
            else {
                final Token token = t;
                final Token nextToken = this.token_source.getNextToken();
                token.next = nextToken;
                t = nextToken;
            }
        }
        return t;
    }
    
    private int jj_ntk() {
        final Token next = this.token.next;
        this.jj_nt = next;
        if (next == null) {
            final Token token = this.token;
            final Token nextToken = this.token_source.getNextToken();
            token.next = nextToken;
            return this.jj_ntk = nextToken.kind;
        }
        return this.jj_ntk = this.jj_nt.kind;
    }
    
    public ParseException generateParseException() {
        this.jj_expentries.clear();
        final boolean[] la1tokens = new boolean[23];
        if (this.jj_kind >= 0) {
            la1tokens[this.jj_kind] = true;
            this.jj_kind = -1;
        }
        for (int i = 0; i < 3; ++i) {
            if (this.jj_la1[i] == this.jj_gen) {
                for (int j = 0; j < 32; ++j) {
                    if ((ContentLanguageParser.jj_la1_0[i] & 1 << j) != 0x0) {
                        la1tokens[j] = true;
                    }
                }
            }
        }
        for (int i = 0; i < 23; ++i) {
            if (la1tokens[i]) {
                (this.jj_expentry = new int[1])[0] = i;
                this.jj_expentries.add(this.jj_expentry);
            }
        }
        final int[][] exptokseq = new int[this.jj_expentries.size()][];
        for (int k = 0; k < this.jj_expentries.size(); ++k) {
            exptokseq[k] = this.jj_expentries.get(k);
        }
        return new ParseException(this.token, exptokseq, ContentLanguageParser.tokenImage);
    }
    
    public final void enable_tracing() {
    }
    
    public final void disable_tracing() {
    }
    
    static {
        jj_la1_init_0();
    }
}
