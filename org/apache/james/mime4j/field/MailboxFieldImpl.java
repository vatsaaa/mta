// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.james.mime4j.field;

import org.apache.james.mime4j.dom.field.ParsedField;
import org.apache.james.mime4j.field.address.AddressBuilder;
import org.apache.james.mime4j.codec.DecodeMonitor;
import org.apache.james.mime4j.stream.Field;
import org.apache.james.mime4j.dom.FieldParser;
import org.apache.james.mime4j.field.address.ParseException;
import org.apache.james.mime4j.dom.address.Mailbox;
import org.apache.james.mime4j.dom.field.MailboxField;

public class MailboxFieldImpl extends AbstractField implements MailboxField
{
    private boolean parsed;
    private Mailbox mailbox;
    private ParseException parseException;
    public static final FieldParser<MailboxField> PARSER;
    
    MailboxFieldImpl(final Field rawField, final DecodeMonitor monitor) {
        super(rawField, monitor);
        this.parsed = false;
    }
    
    public Mailbox getMailbox() {
        if (!this.parsed) {
            this.parse();
        }
        return this.mailbox;
    }
    
    @Override
    public ParseException getParseException() {
        if (!this.parsed) {
            this.parse();
        }
        return this.parseException;
    }
    
    private void parse() {
        final String body = this.getBody();
        try {
            this.mailbox = AddressBuilder.DEFAULT.parseMailbox(body, this.monitor);
        }
        catch (ParseException e) {
            this.parseException = e;
        }
        this.parsed = true;
    }
    
    static {
        PARSER = new FieldParser<MailboxField>() {
            public MailboxField parse(final Field rawField, final DecodeMonitor monitor) {
                return new MailboxFieldImpl(rawField, monitor);
            }
        };
    }
}
