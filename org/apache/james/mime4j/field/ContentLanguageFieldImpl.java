// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.james.mime4j.field;

import org.apache.james.mime4j.dom.field.ParsedField;
import java.util.Collection;
import java.util.ArrayList;
import java.io.Reader;
import org.apache.james.mime4j.field.language.parser.ContentLanguageParser;
import java.io.StringReader;
import java.util.Collections;
import org.apache.james.mime4j.codec.DecodeMonitor;
import org.apache.james.mime4j.stream.Field;
import org.apache.james.mime4j.dom.FieldParser;
import org.apache.james.mime4j.field.language.parser.ParseException;
import java.util.List;
import org.apache.james.mime4j.dom.field.ContentLanguageField;

public class ContentLanguageFieldImpl extends AbstractField implements ContentLanguageField
{
    private boolean parsed;
    private List<String> languages;
    private ParseException parseException;
    public static final FieldParser<ContentLanguageField> PARSER;
    
    ContentLanguageFieldImpl(final Field rawField, final DecodeMonitor monitor) {
        super(rawField, monitor);
        this.parsed = false;
    }
    
    private void parse() {
        this.parsed = true;
        this.languages = Collections.emptyList();
        final String body = this.getBody();
        if (body != null) {
            final ContentLanguageParser parser = new ContentLanguageParser(new StringReader(body));
            try {
                this.languages = parser.parse();
            }
            catch (ParseException ex) {
                this.parseException = ex;
            }
        }
    }
    
    @Override
    public org.apache.james.mime4j.dom.field.ParseException getParseException() {
        return this.parseException;
    }
    
    public List<String> getLanguages() {
        if (!this.parsed) {
            this.parse();
        }
        return new ArrayList<String>(this.languages);
    }
    
    static {
        PARSER = new FieldParser<ContentLanguageField>() {
            public ContentLanguageField parse(final Field rawField, final DecodeMonitor monitor) {
                return new ContentLanguageFieldImpl(rawField, monitor);
            }
        };
    }
}
