// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.james.mime4j.field;

import org.apache.james.mime4j.dom.field.ParsedField;
import org.apache.james.mime4j.codec.DecodeMonitor;
import org.apache.james.mime4j.stream.Field;
import org.apache.james.mime4j.dom.FieldParser;
import org.apache.james.mime4j.dom.field.ContentMD5Field;

public class ContentMD5FieldImpl extends AbstractField implements ContentMD5Field
{
    private boolean parsed;
    private String md5raw;
    public static final FieldParser<ContentMD5Field> PARSER;
    
    ContentMD5FieldImpl(final Field rawField, final DecodeMonitor monitor) {
        super(rawField, monitor);
        this.parsed = false;
    }
    
    private void parse() {
        this.parsed = true;
        final String body = this.getBody();
        if (body != null) {
            this.md5raw = body.trim();
        }
        else {
            this.md5raw = null;
        }
    }
    
    public String getMD5Raw() {
        if (!this.parsed) {
            this.parse();
        }
        return this.md5raw;
    }
    
    static {
        PARSER = new FieldParser<ContentMD5Field>() {
            public ContentMD5Field parse(final Field rawField, final DecodeMonitor monitor) {
                return new ContentMD5FieldImpl(rawField, monitor);
            }
        };
    }
}
