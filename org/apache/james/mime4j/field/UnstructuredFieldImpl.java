// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.james.mime4j.field;

import org.apache.james.mime4j.dom.field.ParsedField;
import org.apache.james.mime4j.codec.DecoderUtil;
import org.apache.james.mime4j.codec.DecodeMonitor;
import org.apache.james.mime4j.stream.Field;
import org.apache.james.mime4j.dom.FieldParser;
import org.apache.james.mime4j.dom.field.UnstructuredField;

public class UnstructuredFieldImpl extends AbstractField implements UnstructuredField
{
    private boolean parsed;
    private String value;
    public static final FieldParser<UnstructuredField> PARSER;
    
    UnstructuredFieldImpl(final Field rawField, final DecodeMonitor monitor) {
        super(rawField, monitor);
        this.parsed = false;
    }
    
    public String getValue() {
        if (!this.parsed) {
            this.parse();
        }
        return this.value;
    }
    
    private void parse() {
        final String body = this.getBody();
        this.value = DecoderUtil.decodeEncodedWords(body, this.monitor);
        this.parsed = true;
    }
    
    static {
        PARSER = new FieldParser<UnstructuredField>() {
            public UnstructuredField parse(final Field rawField, final DecodeMonitor monitor) {
                return new UnstructuredFieldImpl(rawField, monitor);
            }
        };
    }
}
