// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.james.mime4j.field;

import org.apache.james.mime4j.codec.DecodeMonitor;
import org.apache.james.mime4j.stream.Field;
import java.util.HashMap;
import java.util.Map;
import org.apache.james.mime4j.dom.field.ParsedField;
import org.apache.james.mime4j.dom.FieldParser;

public class DelegatingFieldParser implements FieldParser<ParsedField>
{
    private final FieldParser<? extends ParsedField> defaultParser;
    private final Map<String, FieldParser<? extends ParsedField>> parsers;
    
    public DelegatingFieldParser(final FieldParser<? extends ParsedField> defaultParser) {
        this.defaultParser = defaultParser;
        this.parsers = new HashMap<String, FieldParser<? extends ParsedField>>();
    }
    
    public void setFieldParser(final String name, final FieldParser<? extends ParsedField> parser) {
        this.parsers.put(name.toLowerCase(), parser);
    }
    
    public FieldParser<? extends ParsedField> getParser(final String name) {
        final FieldParser<? extends ParsedField> field = this.parsers.get(name.toLowerCase());
        if (field == null) {
            return this.defaultParser;
        }
        return field;
    }
    
    public ParsedField parse(final Field rawField, final DecodeMonitor monitor) {
        final FieldParser<? extends ParsedField> parser = this.getParser(rawField.getName());
        return (ParsedField)parser.parse(rawField, monitor);
    }
}
