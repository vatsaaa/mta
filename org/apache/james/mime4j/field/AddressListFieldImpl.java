// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.james.mime4j.field;

import org.apache.james.mime4j.dom.field.ParsedField;
import org.apache.james.mime4j.field.address.AddressBuilder;
import org.apache.james.mime4j.codec.DecodeMonitor;
import org.apache.james.mime4j.stream.Field;
import org.apache.james.mime4j.dom.FieldParser;
import org.apache.james.mime4j.field.address.ParseException;
import org.apache.james.mime4j.dom.address.AddressList;
import org.apache.james.mime4j.dom.field.AddressListField;

public class AddressListFieldImpl extends AbstractField implements AddressListField
{
    private boolean parsed;
    private AddressList addressList;
    private ParseException parseException;
    public static final FieldParser<AddressListField> PARSER;
    
    AddressListFieldImpl(final Field rawField, final DecodeMonitor monitor) {
        super(rawField, monitor);
        this.parsed = false;
    }
    
    public AddressList getAddressList() {
        if (!this.parsed) {
            this.parse();
        }
        return this.addressList;
    }
    
    @Override
    public ParseException getParseException() {
        if (!this.parsed) {
            this.parse();
        }
        return this.parseException;
    }
    
    private void parse() {
        final String body = this.getBody();
        try {
            this.addressList = AddressBuilder.DEFAULT.parseAddressList(body, this.monitor);
        }
        catch (ParseException e) {
            this.parseException = e;
        }
        this.parsed = true;
    }
    
    static {
        PARSER = new FieldParser<AddressListField>() {
            public AddressListField parse(final Field rawField, final DecodeMonitor monitor) {
                return new AddressListFieldImpl(rawField, monitor);
            }
        };
    }
}
