// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.james.mime4j.field;

import org.apache.james.mime4j.dom.field.ParsedField;
import org.apache.james.mime4j.MimeException;
import java.io.Reader;
import org.apache.james.mime4j.field.mimeversion.parser.MimeVersionParser;
import java.io.StringReader;
import org.apache.james.mime4j.codec.DecodeMonitor;
import org.apache.james.mime4j.stream.Field;
import org.apache.james.mime4j.dom.FieldParser;
import org.apache.james.mime4j.field.mimeversion.parser.ParseException;
import org.apache.james.mime4j.dom.field.MimeVersionField;

public class MimeVersionFieldImpl extends AbstractField implements MimeVersionField
{
    public static final int DEFAULT_MINOR_VERSION = 0;
    public static final int DEFAULT_MAJOR_VERSION = 1;
    private boolean parsed;
    private int major;
    private int minor;
    private ParseException parsedException;
    public static final FieldParser<MimeVersionField> PARSER;
    
    MimeVersionFieldImpl(final Field rawField, final DecodeMonitor monitor) {
        super(rawField, monitor);
        this.parsed = false;
        this.major = 1;
        this.minor = 0;
    }
    
    private void parse() {
        this.parsed = true;
        this.major = 1;
        this.minor = 0;
        final String body = this.getBody();
        if (body != null) {
            final StringReader reader = new StringReader(body);
            final MimeVersionParser parser = new MimeVersionParser(reader);
            try {
                parser.parse();
                int v = parser.getMajorVersion();
                if (v != -1) {
                    this.major = v;
                }
                v = parser.getMinorVersion();
                if (v != -1) {
                    this.minor = v;
                }
            }
            catch (MimeException ex) {
                this.parsedException = new ParseException(ex);
            }
        }
    }
    
    public int getMinorVersion() {
        if (!this.parsed) {
            this.parse();
        }
        return this.minor;
    }
    
    public int getMajorVersion() {
        if (!this.parsed) {
            this.parse();
        }
        return this.major;
    }
    
    @Override
    public org.apache.james.mime4j.dom.field.ParseException getParseException() {
        return this.parsedException;
    }
    
    static {
        PARSER = new FieldParser<MimeVersionField>() {
            public MimeVersionField parse(final Field rawField, final DecodeMonitor monitor) {
                return new MimeVersionFieldImpl(rawField, monitor);
            }
        };
    }
}
