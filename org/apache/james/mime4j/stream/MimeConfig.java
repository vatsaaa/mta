// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.james.mime4j.stream;

public final class MimeConfig implements Cloneable
{
    private boolean strictParsing;
    private int maxLineLen;
    private int maxHeaderCount;
    private int maxHeaderLen;
    private long maxContentLen;
    private boolean countLineNumbers;
    private String headlessParsing;
    private boolean malformedHeaderStartsBody;
    
    public MimeConfig() {
        this.strictParsing = false;
        this.countLineNumbers = false;
        this.malformedHeaderStartsBody = false;
        this.maxLineLen = 1000;
        this.maxHeaderCount = 1000;
        this.maxHeaderLen = 10000;
        this.maxContentLen = -1L;
        this.headlessParsing = null;
    }
    
    public boolean isMalformedHeaderStartsBody() {
        return this.malformedHeaderStartsBody;
    }
    
    public void setMalformedHeaderStartsBody(final boolean malformedHeaderStartsBody) {
        this.malformedHeaderStartsBody = malformedHeaderStartsBody;
    }
    
    public boolean isStrictParsing() {
        return this.strictParsing;
    }
    
    public void setStrictParsing(final boolean strictParsing) {
        this.strictParsing = strictParsing;
    }
    
    public int getMaxLineLen() {
        return this.maxLineLen;
    }
    
    public void setMaxLineLen(final int maxLineLen) {
        this.maxLineLen = maxLineLen;
    }
    
    public int getMaxHeaderCount() {
        return this.maxHeaderCount;
    }
    
    public void setMaxHeaderCount(final int maxHeaderCount) {
        this.maxHeaderCount = maxHeaderCount;
    }
    
    public int getMaxHeaderLen() {
        return this.maxHeaderLen;
    }
    
    public void setMaxHeaderLen(final int maxHeaderLen) {
        this.maxHeaderLen = maxHeaderLen;
    }
    
    public long getMaxContentLen() {
        return this.maxContentLen;
    }
    
    public void setMaxContentLen(final long maxContentLen) {
        this.maxContentLen = maxContentLen;
    }
    
    public boolean isCountLineNumbers() {
        return this.countLineNumbers;
    }
    
    public void setCountLineNumbers(final boolean countLineNumbers) {
        this.countLineNumbers = countLineNumbers;
    }
    
    public String getHeadlessParsing() {
        return this.headlessParsing;
    }
    
    public void setHeadlessParsing(final String contentType) {
        this.headlessParsing = contentType;
    }
    
    public MimeConfig clone() {
        try {
            return (MimeConfig)super.clone();
        }
        catch (CloneNotSupportedException e) {
            throw new InternalError();
        }
    }
    
    @Override
    public String toString() {
        return "[strict parsing: " + this.strictParsing + ", max line length: " + this.maxLineLen + ", max header count: " + this.maxHeaderCount + ", max content length: " + this.maxContentLen + ", count line numbers: " + this.countLineNumbers + "]";
    }
}
