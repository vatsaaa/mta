// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.james.mime4j.stream;

import org.apache.james.mime4j.util.CharsetUtil;
import java.util.ArrayList;
import org.apache.james.mime4j.util.ContentUtil;
import java.util.List;
import org.apache.james.mime4j.MimeException;
import org.apache.james.mime4j.util.ByteSequence;
import java.util.BitSet;

public class RawFieldParser
{
    static final BitSet COLON;
    static final BitSet EQUAL_OR_SEMICOLON;
    static final BitSet SEMICOLON;
    public static final RawFieldParser DEFAULT;
    
    public static BitSet INIT_BITSET(final int... b) {
        final BitSet bitset = new BitSet(b.length);
        for (int i = 0; i < b.length; ++i) {
            bitset.set(b[i]);
        }
        return bitset;
    }
    
    public RawField parseField(final ByteSequence raw) throws MimeException {
        if (raw == null) {
            return null;
        }
        final ParserCursor cursor = new ParserCursor(0, raw.length());
        final String name = this.parseToken(raw, cursor, RawFieldParser.COLON);
        if (cursor.atEnd()) {
            throw new MimeException("Invalid MIME field: no name/value separator found: " + raw.toString());
        }
        return new RawField(raw, cursor.getPos(), name, null);
    }
    
    public RawBody parseRawBody(final RawField field) {
        ByteSequence buf = field.getRaw();
        int pos = field.getDelimiterIdx() + 1;
        if (buf == null) {
            final String body = field.getBody();
            if (body == null) {
                return new RawBody("", null);
            }
            buf = ContentUtil.encode(body);
            pos = 0;
        }
        final ParserCursor cursor = new ParserCursor(pos, buf.length());
        return this.parseRawBody(buf, cursor);
    }
    
    public RawBody parseRawBody(final ByteSequence buf, final ParserCursor cursor) {
        final String value = this.parseToken(buf, cursor, RawFieldParser.SEMICOLON);
        if (cursor.atEnd()) {
            return new RawBody(value, new ArrayList<NameValuePair>());
        }
        cursor.updatePos(cursor.getPos() + 1);
        final List<NameValuePair> params = this.parseParameters(buf, cursor);
        return new RawBody(value, params);
    }
    
    public List<NameValuePair> parseParameters(final ByteSequence buf, final ParserCursor cursor) {
        final List<NameValuePair> params = new ArrayList<NameValuePair>();
        this.skipWhiteSpace(buf, cursor);
        while (!cursor.atEnd()) {
            final NameValuePair param = this.parseParameter(buf, cursor);
            params.add(param);
        }
        return params;
    }
    
    public NameValuePair parseParameter(final ByteSequence buf, final ParserCursor cursor) {
        final String name = this.parseToken(buf, cursor, RawFieldParser.EQUAL_OR_SEMICOLON);
        if (cursor.atEnd()) {
            return new NameValuePair(name, null);
        }
        final int delim = buf.byteAt(cursor.getPos());
        cursor.updatePos(cursor.getPos() + 1);
        if (delim == 59) {
            return new NameValuePair(name, null);
        }
        final String value = this.parseValue(buf, cursor, RawFieldParser.SEMICOLON);
        if (!cursor.atEnd()) {
            cursor.updatePos(cursor.getPos() + 1);
        }
        return new NameValuePair(name, value);
    }
    
    public String parseToken(final ByteSequence buf, final ParserCursor cursor, final BitSet delimiters) {
        final StringBuilder dst = new StringBuilder();
        boolean whitespace = false;
        while (!cursor.atEnd()) {
            final char current = (char)(buf.byteAt(cursor.getPos()) & 0xFF);
            if (delimiters != null && delimiters.get(current)) {
                break;
            }
            if (CharsetUtil.isWhitespace(current)) {
                this.skipWhiteSpace(buf, cursor);
                whitespace = true;
            }
            else if (current == '(') {
                this.skipComment(buf, cursor);
            }
            else {
                if (dst.length() > 0 && whitespace) {
                    dst.append(' ');
                }
                this.copyContent(buf, cursor, delimiters, dst);
                whitespace = false;
            }
        }
        return dst.toString();
    }
    
    public String parseValue(final ByteSequence buf, final ParserCursor cursor, final BitSet delimiters) {
        final StringBuilder dst = new StringBuilder();
        boolean whitespace = false;
        while (!cursor.atEnd()) {
            final char current = (char)(buf.byteAt(cursor.getPos()) & 0xFF);
            if (delimiters != null && delimiters.get(current)) {
                break;
            }
            if (CharsetUtil.isWhitespace(current)) {
                this.skipWhiteSpace(buf, cursor);
                whitespace = true;
            }
            else if (current == '(') {
                this.skipComment(buf, cursor);
            }
            else if (current == '\"') {
                if (dst.length() > 0 && whitespace) {
                    dst.append(' ');
                }
                this.copyQuotedContent(buf, cursor, dst);
                whitespace = false;
            }
            else {
                if (dst.length() > 0 && whitespace) {
                    dst.append(' ');
                }
                this.copyContent(buf, cursor, delimiters, dst);
                whitespace = false;
            }
        }
        return dst.toString();
    }
    
    public void skipWhiteSpace(final ByteSequence buf, final ParserCursor cursor) {
        int pos = cursor.getPos();
        final int indexFrom = cursor.getPos();
        for (int indexTo = cursor.getUpperBound(), i = indexFrom; i < indexTo; ++i) {
            final char current = (char)(buf.byteAt(i) & 0xFF);
            if (!CharsetUtil.isWhitespace(current)) {
                break;
            }
            ++pos;
        }
        cursor.updatePos(pos);
    }
    
    public void skipComment(final ByteSequence buf, final ParserCursor cursor) {
        if (cursor.atEnd()) {
            return;
        }
        int pos = cursor.getPos();
        int indexFrom = cursor.getPos();
        final int indexTo = cursor.getUpperBound();
        char current = (char)(buf.byteAt(pos) & 0xFF);
        if (current != '(') {
            return;
        }
        ++pos;
        ++indexFrom;
        int level = 1;
        boolean escaped = false;
        for (int i = indexFrom; i < indexTo; ++i, ++pos) {
            current = (char)(buf.byteAt(i) & 0xFF);
            if (escaped) {
                escaped = false;
            }
            else if (current == '\\') {
                escaped = true;
            }
            else if (current == '(') {
                ++level;
            }
            else if (current == ')') {
                --level;
            }
            if (level <= 0) {
                ++pos;
                break;
            }
        }
        cursor.updatePos(pos);
    }
    
    public void skipAllWhiteSpace(final ByteSequence buf, final ParserCursor cursor) {
        while (!cursor.atEnd()) {
            final char current = (char)(buf.byteAt(cursor.getPos()) & 0xFF);
            if (CharsetUtil.isWhitespace(current)) {
                this.skipWhiteSpace(buf, cursor);
            }
            else {
                if (current != '(') {
                    break;
                }
                this.skipComment(buf, cursor);
            }
        }
    }
    
    public void copyContent(final ByteSequence buf, final ParserCursor cursor, final BitSet delimiters, final StringBuilder dst) {
        int pos = cursor.getPos();
        final int indexFrom = cursor.getPos();
        for (int indexTo = cursor.getUpperBound(), i = indexFrom; i < indexTo; ++i) {
            final char current = (char)(buf.byteAt(i) & 0xFF);
            if ((delimiters != null && delimiters.get(current)) || CharsetUtil.isWhitespace(current)) {
                break;
            }
            if (current == '(') {
                break;
            }
            ++pos;
            dst.append(current);
        }
        cursor.updatePos(pos);
    }
    
    public void copyQuotedContent(final ByteSequence buf, final ParserCursor cursor, final StringBuilder dst) {
        if (cursor.atEnd()) {
            return;
        }
        int pos = cursor.getPos();
        int indexFrom = cursor.getPos();
        final int indexTo = cursor.getUpperBound();
        char current = (char)(buf.byteAt(pos) & 0xFF);
        if (current != '\"') {
            return;
        }
        ++pos;
        ++indexFrom;
        boolean escaped = false;
        for (int i = indexFrom; i < indexTo; ++i, ++pos) {
            current = (char)(buf.byteAt(i) & 0xFF);
            if (escaped) {
                if (current != '\"' && current != '\\') {
                    dst.append('\\');
                }
                dst.append(current);
                escaped = false;
            }
            else {
                if (current == '\"') {
                    ++pos;
                    break;
                }
                if (current == '\\') {
                    escaped = true;
                }
                else if (current != '\r' && current != '\n') {
                    dst.append(current);
                }
            }
        }
        cursor.updatePos(pos);
    }
    
    static {
        COLON = INIT_BITSET(58);
        EQUAL_OR_SEMICOLON = INIT_BITSET(61, 59);
        SEMICOLON = INIT_BITSET(59);
        DEFAULT = new RawFieldParser();
    }
}
