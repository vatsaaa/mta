// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.james.mime4j.stream;

import org.apache.james.mime4j.util.MimeUtil;
import org.apache.james.mime4j.util.ContentUtil;
import org.apache.james.mime4j.util.CharsetUtil;
import org.apache.james.mime4j.util.ByteSequence;

public final class RawField implements Field
{
    private final ByteSequence raw;
    private final int delimiterIdx;
    private final String name;
    private final String body;
    
    RawField(final ByteSequence raw, final int delimiterIdx, final String name, final String body) {
        if (name == null) {
            throw new IllegalArgumentException("Field may not be null");
        }
        this.raw = raw;
        this.delimiterIdx = delimiterIdx;
        this.name = name.trim();
        this.body = body;
    }
    
    public RawField(final String name, final String body) {
        this(null, -1, name, body);
    }
    
    public ByteSequence getRaw() {
        return this.raw;
    }
    
    public String getName() {
        return this.name;
    }
    
    public String getBody() {
        if (this.body != null) {
            return this.body;
        }
        if (this.raw != null) {
            final int len = this.raw.length();
            int off = this.delimiterIdx + 1;
            if (len > off + 1 && CharsetUtil.isWhitespace((char)(this.raw.byteAt(off) & 0xFF))) {
                ++off;
            }
            return MimeUtil.unfold(ContentUtil.decode(this.raw, off, len - off));
        }
        return null;
    }
    
    public int getDelimiterIdx() {
        return this.delimiterIdx;
    }
    
    @Override
    public String toString() {
        if (this.raw != null) {
            return ContentUtil.decode(this.raw);
        }
        final StringBuilder buf = new StringBuilder();
        buf.append(this.name);
        buf.append(": ");
        if (this.body != null) {
            buf.append(this.body);
        }
        return buf.toString();
    }
}
