// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.james.mime4j.stream;

import org.apache.james.mime4j.util.ByteSequence;

public interface Field
{
    String getName();
    
    String getBody();
    
    ByteSequence getRaw();
}
