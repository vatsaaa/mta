// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.james.mime4j.stream;

public final class Event
{
    public static final Event MIME_BODY_PREMATURE_END;
    public static final Event HEADERS_PREMATURE_END;
    public static final Event INVALID_HEADER;
    public static final Event OBSOLETE_HEADER;
    private final String code;
    
    public Event(final String code) {
        if (code == null) {
            throw new IllegalArgumentException("Code may not be null");
        }
        this.code = code;
    }
    
    @Override
    public int hashCode() {
        return this.code.hashCode();
    }
    
    @Override
    public boolean equals(final Object obj) {
        if (obj == null) {
            return false;
        }
        if (this == obj) {
            return true;
        }
        if (obj instanceof Event) {
            final Event that = (Event)obj;
            return this.code.equals(that.code);
        }
        return false;
    }
    
    @Override
    public String toString() {
        return this.code;
    }
    
    static {
        MIME_BODY_PREMATURE_END = new Event("Body part ended prematurely. Boundary detected in header or EOF reached.");
        HEADERS_PREMATURE_END = new Event("Unexpected end of headers detected. Higher level boundary detected or EOF reached.");
        INVALID_HEADER = new Event("Invalid header encountered");
        OBSOLETE_HEADER = new Event("Obsolete header encountered");
    }
}
