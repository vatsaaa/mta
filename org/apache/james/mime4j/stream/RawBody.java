// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.james.mime4j.stream;

import java.util.Iterator;
import java.util.Collection;
import java.util.ArrayList;
import java.util.List;

public final class RawBody
{
    private final String value;
    private final List<NameValuePair> params;
    
    RawBody(final String value, final List<NameValuePair> params) {
        if (value == null) {
            throw new IllegalArgumentException("Field value not be null");
        }
        this.value = value;
        this.params = ((params != null) ? params : new ArrayList<NameValuePair>());
    }
    
    public String getValue() {
        return this.value;
    }
    
    public List<NameValuePair> getParams() {
        return new ArrayList<NameValuePair>(this.params);
    }
    
    @Override
    public String toString() {
        final StringBuilder buf = new StringBuilder();
        buf.append(this.value);
        buf.append("; ");
        for (final NameValuePair param : this.params) {
            buf.append("; ");
            buf.append(param);
        }
        return buf.toString();
    }
}
