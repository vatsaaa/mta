// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.james.mime4j.stream;

public enum RecursionMode
{
    M_RECURSE, 
    M_NO_RECURSE, 
    M_RAW, 
    M_FLAT;
}
