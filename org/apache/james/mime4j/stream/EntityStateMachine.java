// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.james.mime4j.stream;

import java.io.InputStream;
import org.apache.james.mime4j.MimeException;
import java.io.IOException;

public interface EntityStateMachine
{
    EntityState getState();
    
    void setRecursionMode(final RecursionMode p0);
    
    EntityStateMachine advance() throws IOException, MimeException;
    
    BodyDescriptor getBodyDescriptor() throws IllegalStateException;
    
    InputStream getContentStream() throws IllegalStateException;
    
    InputStream getDecodedContentStream() throws IllegalStateException;
    
    Field getField() throws IllegalStateException;
}
