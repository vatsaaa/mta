// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.james.mime4j.stream;

public interface BodyDescriptor extends ContentDescriptor
{
    String getBoundary();
}
