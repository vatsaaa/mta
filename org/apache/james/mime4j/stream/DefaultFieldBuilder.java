// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.james.mime4j.stream;

import org.apache.james.mime4j.MimeException;
import org.apache.james.mime4j.util.ByteSequence;
import org.apache.james.mime4j.io.MaxHeaderLengthLimitException;
import org.apache.james.mime4j.util.ByteArrayBuffer;
import java.util.BitSet;

public class DefaultFieldBuilder implements FieldBuilder
{
    private static final BitSet FIELD_CHARS;
    private final ByteArrayBuffer buf;
    private final int maxlen;
    
    public DefaultFieldBuilder(final int maxlen) {
        this.buf = new ByteArrayBuffer(1024);
        this.maxlen = maxlen;
    }
    
    public void reset() {
        this.buf.clear();
    }
    
    public void append(final ByteArrayBuffer line) throws MaxHeaderLengthLimitException {
        if (line == null) {
            return;
        }
        final int len = line.length();
        if (this.maxlen > 0 && this.buf.length() + len >= this.maxlen) {
            throw new MaxHeaderLengthLimitException("Maximum header length limit exceeded");
        }
        this.buf.append(line.buffer(), 0, line.length());
    }
    
    public RawField build() throws MimeException {
        int len = this.buf.length();
        if (len > 0) {
            if (this.buf.byteAt(len - 1) == 10) {
                --len;
            }
            if (this.buf.byteAt(len - 1) == 13) {
                --len;
            }
        }
        final ByteArrayBuffer copy = new ByteArrayBuffer(this.buf.buffer(), len, false);
        final RawField field = RawFieldParser.DEFAULT.parseField(copy);
        final String name = field.getName();
        for (int i = 0; i < name.length(); ++i) {
            final char ch = name.charAt(i);
            if (!DefaultFieldBuilder.FIELD_CHARS.get(ch)) {
                throw new MimeException("MIME field name contains illegal characters: " + field.getName());
            }
        }
        return field;
    }
    
    public ByteArrayBuffer getRaw() {
        return this.buf;
    }
    
    static {
        FIELD_CHARS = new BitSet();
        for (int i = 33; i <= 57; ++i) {
            DefaultFieldBuilder.FIELD_CHARS.set(i);
        }
        for (int i = 59; i <= 126; ++i) {
            DefaultFieldBuilder.FIELD_CHARS.set(i);
        }
    }
}
