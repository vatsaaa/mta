// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.james.mime4j.stream;

public interface ContentDescriptor
{
    String getMimeType();
    
    String getMediaType();
    
    String getSubType();
    
    String getCharset();
    
    String getTransferEncoding();
    
    long getContentLength();
}
