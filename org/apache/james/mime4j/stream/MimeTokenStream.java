// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.james.mime4j.stream;

import java.io.InputStreamReader;
import java.nio.charset.Charset;
import org.apache.james.mime4j.util.CharsetUtil;
import java.io.Reader;
import org.apache.james.mime4j.io.LineNumberSource;
import org.apache.james.mime4j.io.LineNumberInputStream;
import java.io.IOException;
import org.apache.james.mime4j.MimeException;
import java.io.InputStream;
import java.util.LinkedList;
import org.apache.james.mime4j.codec.DecodeMonitor;

public class MimeTokenStream
{
    private final MimeConfig config;
    private final DecodeMonitor monitor;
    private final FieldBuilder fieldBuilder;
    private final BodyDescriptorBuilder bodyDescBuilder;
    private final LinkedList<EntityStateMachine> entities;
    private EntityState state;
    private EntityStateMachine currentStateMachine;
    private RecursionMode recursionMode;
    private MimeEntity rootentity;
    
    public MimeTokenStream() {
        this(null);
    }
    
    public MimeTokenStream(final MimeConfig config) {
        this(config, null, null, null);
    }
    
    public MimeTokenStream(final MimeConfig config, final BodyDescriptorBuilder bodyDescBuilder) {
        this(config, null, null, bodyDescBuilder);
    }
    
    public MimeTokenStream(final MimeConfig config, final DecodeMonitor monitor, final BodyDescriptorBuilder bodyDescBuilder) {
        this(config, monitor, null, bodyDescBuilder);
    }
    
    public MimeTokenStream(final MimeConfig config, final DecodeMonitor monitor, final FieldBuilder fieldBuilder, final BodyDescriptorBuilder bodyDescBuilder) {
        this.entities = new LinkedList<EntityStateMachine>();
        this.state = EntityState.T_END_OF_STREAM;
        this.recursionMode = RecursionMode.M_RECURSE;
        this.config = ((config != null) ? config : new MimeConfig());
        this.fieldBuilder = ((fieldBuilder != null) ? fieldBuilder : new DefaultFieldBuilder(this.config.getMaxHeaderLen()));
        this.monitor = ((monitor != null) ? monitor : (this.config.isStrictParsing() ? DecodeMonitor.STRICT : DecodeMonitor.SILENT));
        this.bodyDescBuilder = ((bodyDescBuilder != null) ? bodyDescBuilder : new FallbackBodyDescriptorBuilder());
    }
    
    public void parse(final InputStream stream) {
        this.doParse(stream, EntityState.T_START_MESSAGE);
    }
    
    public Field parseHeadless(final InputStream stream, final String contentType) {
        if (contentType == null) {
            throw new IllegalArgumentException("Content type may not be null");
        }
        Field newContentType;
        try {
            final RawField rawContentType = new RawField("Content-Type", contentType);
            newContentType = this.bodyDescBuilder.addField(rawContentType);
            if (newContentType == null) {
                newContentType = rawContentType;
            }
        }
        catch (MimeException ex) {
            throw new IllegalArgumentException(ex.getMessage());
        }
        this.doParse(stream, EntityState.T_END_HEADER);
        try {
            this.next();
        }
        catch (IOException e) {
            throw new IllegalStateException(e);
        }
        catch (MimeException e2) {
            throw new IllegalStateException(e2);
        }
        return newContentType;
    }
    
    private void doParse(InputStream stream, final EntityState start) {
        LineNumberSource lineSource = null;
        if (this.config.isCountLineNumbers()) {
            lineSource = (LineNumberSource)(stream = new LineNumberInputStream(stream));
        }
        (this.rootentity = new MimeEntity(lineSource, stream, this.config, start, EntityState.T_END_MESSAGE, this.monitor, this.fieldBuilder, this.bodyDescBuilder)).setRecursionMode(this.recursionMode);
        this.currentStateMachine = this.rootentity;
        this.entities.clear();
        this.entities.add(this.currentStateMachine);
        this.state = this.currentStateMachine.getState();
    }
    
    public boolean isRaw() {
        return this.recursionMode == RecursionMode.M_RAW;
    }
    
    public RecursionMode getRecursionMode() {
        return this.recursionMode;
    }
    
    public void setRecursionMode(final RecursionMode mode) {
        this.recursionMode = mode;
        if (this.currentStateMachine != null) {
            this.currentStateMachine.setRecursionMode(mode);
        }
    }
    
    public void stop() {
        this.rootentity.stop();
    }
    
    public EntityState getState() {
        return this.state;
    }
    
    public InputStream getInputStream() {
        return this.currentStateMachine.getContentStream();
    }
    
    public InputStream getDecodedInputStream() {
        return this.currentStateMachine.getDecodedContentStream();
    }
    
    public Reader getReader() {
        final BodyDescriptor bodyDescriptor = this.getBodyDescriptor();
        final String mimeCharset = bodyDescriptor.getCharset();
        Charset charset;
        if (mimeCharset == null || "".equals(mimeCharset)) {
            charset = CharsetUtil.US_ASCII;
        }
        else {
            charset = Charset.forName(mimeCharset);
        }
        final InputStream instream = this.getDecodedInputStream();
        return new InputStreamReader(instream, charset);
    }
    
    public BodyDescriptor getBodyDescriptor() {
        return this.currentStateMachine.getBodyDescriptor();
    }
    
    public Field getField() {
        return this.currentStateMachine.getField();
    }
    
    public EntityState next() throws IOException, MimeException {
        if (this.state == EntityState.T_END_OF_STREAM || this.currentStateMachine == null) {
            throw new IllegalStateException("No more tokens are available.");
        }
        while (this.currentStateMachine != null) {
            final EntityStateMachine next = this.currentStateMachine.advance();
            if (next != null) {
                this.entities.add(next);
                this.currentStateMachine = next;
            }
            this.state = this.currentStateMachine.getState();
            if (this.state != EntityState.T_END_OF_STREAM) {
                return this.state;
            }
            this.entities.removeLast();
            if (this.entities.isEmpty()) {
                this.currentStateMachine = null;
            }
            else {
                (this.currentStateMachine = this.entities.getLast()).setRecursionMode(this.recursionMode);
            }
        }
        return this.state = EntityState.T_END_OF_STREAM;
    }
    
    public static final String stateToString(final EntityState state) {
        return MimeEntity.stateToString(state);
    }
    
    public MimeConfig getConfig() {
        return this.config;
    }
}
