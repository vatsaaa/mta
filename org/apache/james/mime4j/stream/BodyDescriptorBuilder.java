// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.james.mime4j.stream;

import org.apache.james.mime4j.MimeException;

public interface BodyDescriptorBuilder
{
    void reset();
    
    Field addField(final RawField p0) throws MimeException;
    
    BodyDescriptor build();
    
    BodyDescriptorBuilder newChild();
}
