// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.james.mime4j.io;

import org.apache.james.mime4j.MimeException;

public class MaxHeaderLimitException extends MimeException
{
    private static final long serialVersionUID = 2154269045186186769L;
    
    public MaxHeaderLimitException(final String message) {
        super(message);
    }
}
