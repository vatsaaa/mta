// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.james.mime4j.io;

import java.io.IOException;
import java.io.InputStream;

public class LimitedInputStream extends PositionInputStream
{
    private final long limit;
    
    public LimitedInputStream(final InputStream instream, final long limit) {
        super(instream);
        if (limit < 0L) {
            throw new IllegalArgumentException("Limit may not be negative");
        }
        this.limit = limit;
    }
    
    private void enforceLimit() throws IOException {
        if (this.position >= this.limit) {
            throw new IOException("Input stream limit exceeded");
        }
    }
    
    @Override
    public int read() throws IOException {
        this.enforceLimit();
        return super.read();
    }
    
    @Override
    public int read(final byte[] b, final int off, int len) throws IOException {
        this.enforceLimit();
        len = Math.min(len, this.getBytesLeft());
        return super.read(b, off, len);
    }
    
    @Override
    public long skip(long n) throws IOException {
        this.enforceLimit();
        n = Math.min(n, this.getBytesLeft());
        return super.skip(n);
    }
    
    private int getBytesLeft() {
        return (int)Math.min(2147483647L, this.limit - this.position);
    }
}
