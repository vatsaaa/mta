// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.james.mime4j.io;

import java.io.IOException;
import org.apache.james.mime4j.util.ByteArrayBuffer;
import java.io.InputStream;
import java.io.FilterInputStream;

public abstract class LineReaderInputStream extends FilterInputStream
{
    protected LineReaderInputStream(final InputStream in) {
        super(in);
    }
    
    public abstract int readLine(final ByteArrayBuffer p0) throws MaxLineLimitException, IOException;
    
    public abstract boolean unread(final ByteArrayBuffer p0);
}
