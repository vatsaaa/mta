// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.james.mime4j.io;

import java.io.IOException;
import java.io.InputStream;
import java.io.FilterInputStream;

public class PositionInputStream extends FilterInputStream
{
    protected long position;
    private long markedPosition;
    
    public PositionInputStream(final InputStream inputStream) {
        super(inputStream);
        this.position = 0L;
        this.markedPosition = 0L;
    }
    
    public long getPosition() {
        return this.position;
    }
    
    @Override
    public int available() throws IOException {
        return this.in.available();
    }
    
    @Override
    public int read() throws IOException {
        final int b = this.in.read();
        if (b != -1) {
            ++this.position;
        }
        return b;
    }
    
    @Override
    public void close() throws IOException {
        this.in.close();
    }
    
    @Override
    public void reset() throws IOException {
        this.in.reset();
        this.position = this.markedPosition;
    }
    
    @Override
    public boolean markSupported() {
        return this.in.markSupported();
    }
    
    @Override
    public void mark(final int readlimit) {
        this.in.mark(readlimit);
        this.markedPosition = this.position;
    }
    
    @Override
    public long skip(final long n) throws IOException {
        final long c = this.in.skip(n);
        if (c > 0L) {
            this.position += c;
        }
        return c;
    }
    
    @Override
    public int read(final byte[] b, final int off, final int len) throws IOException {
        final int c = this.in.read(b, off, len);
        if (c > 0) {
            this.position += c;
        }
        return c;
    }
}
