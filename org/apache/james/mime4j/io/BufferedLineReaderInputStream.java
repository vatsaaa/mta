// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.james.mime4j.io;

import org.apache.james.mime4j.util.ByteArrayBuffer;
import java.io.IOException;
import java.io.InputStream;

public class BufferedLineReaderInputStream extends LineReaderInputStream
{
    private boolean truncated;
    boolean tempBuffer;
    private byte[] origBuffer;
    private int origBufpos;
    private int origBuflen;
    private byte[] buffer;
    private int bufpos;
    private int buflen;
    private final int maxLineLen;
    
    public BufferedLineReaderInputStream(final InputStream instream, final int buffersize, final int maxLineLen) {
        super(instream);
        this.tempBuffer = false;
        if (instream == null) {
            throw new IllegalArgumentException("Input stream may not be null");
        }
        if (buffersize <= 0) {
            throw new IllegalArgumentException("Buffer size may not be negative or zero");
        }
        this.buffer = new byte[buffersize];
        this.bufpos = 0;
        this.buflen = 0;
        this.maxLineLen = maxLineLen;
        this.truncated = false;
    }
    
    public BufferedLineReaderInputStream(final InputStream instream, final int buffersize) {
        this(instream, buffersize, -1);
    }
    
    private void expand(final int newlen) {
        final byte[] newbuffer = new byte[newlen];
        final int len = this.bufferLen();
        if (len > 0) {
            System.arraycopy(this.buffer, this.bufpos, newbuffer, this.bufpos, len);
        }
        this.buffer = newbuffer;
    }
    
    public void ensureCapacity(final int len) {
        if (len > this.buffer.length) {
            this.expand(len);
        }
    }
    
    public int fillBuffer() throws IOException {
        if (this.tempBuffer) {
            if (this.bufpos != this.buflen) {
                throw new IllegalStateException("unread only works when a buffer is fully read before the next refill is asked!");
            }
            this.buffer = this.origBuffer;
            this.buflen = this.origBuflen;
            this.bufpos = this.origBufpos;
            this.tempBuffer = false;
            return this.bufferLen();
        }
        else {
            if (this.bufpos > 0) {
                final int len = this.bufferLen();
                if (len > 0) {
                    System.arraycopy(this.buffer, this.bufpos, this.buffer, 0, len);
                }
                this.bufpos = 0;
                this.buflen = len;
            }
            final int off = this.buflen;
            final int len2 = this.buffer.length - off;
            final int l = this.in.read(this.buffer, off, len2);
            if (l == -1) {
                return -1;
            }
            this.buflen = off + l;
            return l;
        }
    }
    
    private int bufferLen() {
        return this.buflen - this.bufpos;
    }
    
    public boolean hasBufferedData() {
        return this.bufferLen() > 0;
    }
    
    public void truncate() {
        this.clear();
        this.truncated = true;
    }
    
    protected boolean readAllowed() {
        return !this.truncated;
    }
    
    @Override
    public int read() throws IOException {
        if (!this.readAllowed()) {
            return -1;
        }
        int noRead = 0;
        while (!this.hasBufferedData()) {
            noRead = this.fillBuffer();
            if (noRead == -1) {
                return -1;
            }
        }
        return this.buffer[this.bufpos++] & 0xFF;
    }
    
    @Override
    public int read(final byte[] b, final int off, final int len) throws IOException {
        if (!this.readAllowed()) {
            return -1;
        }
        if (b == null) {
            return 0;
        }
        int noRead = 0;
        while (!this.hasBufferedData()) {
            noRead = this.fillBuffer();
            if (noRead == -1) {
                return -1;
            }
        }
        int chunk = this.bufferLen();
        if (chunk > len) {
            chunk = len;
        }
        System.arraycopy(this.buffer, this.bufpos, b, off, chunk);
        this.bufpos += chunk;
        return chunk;
    }
    
    @Override
    public int read(final byte[] b) throws IOException {
        if (!this.readAllowed()) {
            return -1;
        }
        if (b == null) {
            return 0;
        }
        return this.read(b, 0, b.length);
    }
    
    @Override
    public boolean markSupported() {
        return false;
    }
    
    @Override
    public int readLine(final ByteArrayBuffer dst) throws MaxLineLimitException, IOException {
        if (dst == null) {
            throw new IllegalArgumentException("Buffer may not be null");
        }
        if (!this.readAllowed()) {
            return -1;
        }
        int total = 0;
        boolean found = false;
        int bytesRead = 0;
        while (!found) {
            if (!this.hasBufferedData()) {
                bytesRead = this.fillBuffer();
                if (bytesRead == -1) {
                    break;
                }
            }
            final int i = this.indexOf((byte)10);
            int chunk;
            if (i != -1) {
                found = true;
                chunk = i + 1 - this.pos();
            }
            else {
                chunk = this.length();
            }
            if (chunk > 0) {
                dst.append(this.buf(), this.pos(), chunk);
                this.skip(chunk);
                total += chunk;
            }
            if (this.maxLineLen > 0 && dst.length() >= this.maxLineLen) {
                throw new MaxLineLimitException("Maximum line length limit exceeded");
            }
        }
        if (total == 0 && bytesRead == -1) {
            return -1;
        }
        return total;
    }
    
    public int indexOf(final byte[] pattern, final int off, final int len) {
        if (pattern == null) {
            throw new IllegalArgumentException("Pattern may not be null");
        }
        if (off < this.bufpos || len < 0 || off + len > this.buflen) {
            throw new IndexOutOfBoundsException("looking for " + off + "(" + len + ")" + " in " + this.bufpos + "/" + this.buflen);
        }
        if (len < pattern.length) {
            return -1;
        }
        final int[] shiftTable = new int[256];
        for (int i = 0; i < shiftTable.length; ++i) {
            shiftTable[i] = pattern.length + 1;
        }
        for (int i = 0; i < pattern.length; ++i) {
            final int x = pattern[i] & 0xFF;
            shiftTable[x] = pattern.length - i;
        }
        int x2;
        for (int j = 0; j <= len - pattern.length; j += shiftTable[x2]) {
            final int cur = off + j;
            boolean match = true;
            for (int k = 0; k < pattern.length; ++k) {
                if (this.buffer[cur + k] != pattern[k]) {
                    match = false;
                    break;
                }
            }
            if (match) {
                return cur;
            }
            final int pos = cur + pattern.length;
            if (pos >= this.buffer.length) {
                break;
            }
            x2 = (this.buffer[pos] & 0xFF);
        }
        return -1;
    }
    
    public int indexOf(final byte[] pattern) {
        return this.indexOf(pattern, this.bufpos, this.buflen - this.bufpos);
    }
    
    public int indexOf(final byte b, final int off, final int len) {
        if (off < this.bufpos || len < 0 || off + len > this.buflen) {
            throw new IndexOutOfBoundsException();
        }
        for (int i = off; i < off + len; ++i) {
            if (this.buffer[i] == b) {
                return i;
            }
        }
        return -1;
    }
    
    public int indexOf(final byte b) {
        return this.indexOf(b, this.bufpos, this.bufferLen());
    }
    
    public int byteAt(final int pos) {
        if (pos < this.bufpos || pos > this.buflen) {
            throw new IndexOutOfBoundsException("looking for " + pos + " in " + this.bufpos + "/" + this.buflen);
        }
        return this.buffer[pos] & 0xFF;
    }
    
    protected byte[] buf() {
        return this.buffer;
    }
    
    protected int pos() {
        return this.bufpos;
    }
    
    protected int limit() {
        return this.buflen;
    }
    
    protected int length() {
        return this.bufferLen();
    }
    
    public int capacity() {
        return this.buffer.length;
    }
    
    protected int skip(final int n) {
        final int chunk = Math.min(n, this.bufferLen());
        this.bufpos += chunk;
        return chunk;
    }
    
    private void clear() {
        this.bufpos = 0;
        this.buflen = 0;
    }
    
    @Override
    public String toString() {
        final StringBuilder buffer = new StringBuilder();
        buffer.append("[pos: ");
        buffer.append(this.bufpos);
        buffer.append("]");
        buffer.append("[limit: ");
        buffer.append(this.buflen);
        buffer.append("]");
        buffer.append("[");
        for (int i = this.bufpos; i < this.buflen; ++i) {
            buffer.append((char)this.buffer[i]);
        }
        buffer.append("]");
        if (this.tempBuffer) {
            buffer.append("-ORIG[pos: ");
            buffer.append(this.origBufpos);
            buffer.append("]");
            buffer.append("[limit: ");
            buffer.append(this.origBuflen);
            buffer.append("]");
            buffer.append("[");
            for (int i = this.origBufpos; i < this.origBuflen; ++i) {
                buffer.append((char)this.origBuffer[i]);
            }
            buffer.append("]");
        }
        return buffer.toString();
    }
    
    @Override
    public boolean unread(final ByteArrayBuffer buf) {
        if (this.tempBuffer) {
            return false;
        }
        this.origBuffer = this.buffer;
        this.origBuflen = this.buflen;
        this.origBufpos = this.bufpos;
        this.bufpos = 0;
        this.buflen = buf.length();
        this.buffer = buf.buffer();
        return this.tempBuffer = true;
    }
}
