// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.james.mime4j.io;

import java.io.IOException;

public class MaxLineLimitException extends IOException
{
    private static final long serialVersionUID = 1855987166990764426L;
    
    public MaxLineLimitException(final String message) {
        super(message);
    }
}
