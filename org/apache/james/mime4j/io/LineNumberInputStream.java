// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.james.mime4j.io;

import java.io.IOException;
import java.io.InputStream;
import java.io.FilterInputStream;

public class LineNumberInputStream extends FilterInputStream implements LineNumberSource
{
    private int lineNumber;
    
    public LineNumberInputStream(final InputStream is) {
        super(is);
        this.lineNumber = 1;
    }
    
    public int getLineNumber() {
        return this.lineNumber;
    }
    
    @Override
    public int read() throws IOException {
        final int b = this.in.read();
        if (b == 10) {
            ++this.lineNumber;
        }
        return b;
    }
    
    @Override
    public int read(final byte[] b, final int off, final int len) throws IOException {
        final int n = this.in.read(b, off, len);
        for (int i = off; i < off + n; ++i) {
            if (b[i] == 10) {
                ++this.lineNumber;
            }
        }
        return n;
    }
}
