// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.james.mime4j.io;

import org.apache.james.mime4j.util.ByteArrayBuffer;
import java.io.IOException;
import java.io.InputStream;

public class LineReaderInputStreamAdaptor extends LineReaderInputStream
{
    private final LineReaderInputStream bis;
    private final int maxLineLen;
    private boolean used;
    private boolean eof;
    
    public LineReaderInputStreamAdaptor(final InputStream is, final int maxLineLen) {
        super(is);
        this.used = false;
        this.eof = false;
        if (is instanceof LineReaderInputStream) {
            this.bis = (LineReaderInputStream)is;
        }
        else {
            this.bis = null;
        }
        this.maxLineLen = maxLineLen;
    }
    
    public LineReaderInputStreamAdaptor(final InputStream is) {
        this(is, -1);
    }
    
    @Override
    public int read() throws IOException {
        final int i = this.in.read();
        this.eof = (i == -1);
        this.used = true;
        return i;
    }
    
    @Override
    public int read(final byte[] b, final int off, final int len) throws IOException {
        final int i = this.in.read(b, off, len);
        this.eof = (i == -1);
        this.used = true;
        return i;
    }
    
    @Override
    public int readLine(final ByteArrayBuffer dst) throws MaxLineLimitException, IOException {
        int i;
        if (this.bis != null) {
            i = this.bis.readLine(dst);
        }
        else {
            i = this.doReadLine(dst);
        }
        this.eof = (i == -1);
        this.used = true;
        return i;
    }
    
    private int doReadLine(final ByteArrayBuffer dst) throws MaxLineLimitException, IOException {
        int total = 0;
        int ch;
        while ((ch = this.in.read()) != -1) {
            dst.append(ch);
            ++total;
            if (this.maxLineLen > 0 && dst.length() >= this.maxLineLen) {
                throw new MaxLineLimitException("Maximum line length limit exceeded");
            }
            if (ch == 10) {
                break;
            }
        }
        if (total == 0 && ch == -1) {
            return -1;
        }
        return total;
    }
    
    public boolean eof() {
        return this.eof;
    }
    
    public boolean isUsed() {
        return this.used;
    }
    
    @Override
    public String toString() {
        return "[LineReaderInputStreamAdaptor: " + this.bis + "]";
    }
    
    @Override
    public boolean unread(final ByteArrayBuffer buf) {
        return this.bis != null && this.bis.unread(buf);
    }
    
    @Override
    public long skip(long count) throws IOException {
        if (count <= 0L) {
            return 0L;
        }
        final int bufferSize = (count > 8192L) ? 8192 : ((int)count);
        final byte[] buffer = new byte[bufferSize];
        long result = 0L;
        while (count > 0L) {
            final int res = this.read(buffer);
            if (res == -1) {
                break;
            }
            result += res;
            count -= res;
        }
        return result;
    }
}
