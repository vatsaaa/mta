// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.james.mime4j.io;

import java.io.IOException;
import java.io.PushbackInputStream;
import java.io.InputStream;

public class EOLConvertingInputStream extends InputStream
{
    public static final int CONVERT_CR = 1;
    public static final int CONVERT_LF = 2;
    public static final int CONVERT_BOTH = 3;
    private PushbackInputStream in;
    private int previous;
    private int flags;
    
    public EOLConvertingInputStream(final InputStream in) {
        this(in, 3);
    }
    
    public EOLConvertingInputStream(final InputStream in, final int flags) {
        this.in = null;
        this.previous = 0;
        this.flags = 3;
        this.in = new PushbackInputStream(in, 2);
        this.flags = flags;
    }
    
    @Override
    public void close() throws IOException {
        this.in.close();
    }
    
    @Override
    public int read() throws IOException {
        int b = this.in.read();
        if (b == -1) {
            return -1;
        }
        if ((this.flags & 0x1) != 0x0 && b == 13) {
            final int c = this.in.read();
            if (c != -1) {
                this.in.unread(c);
            }
            if (c != 10) {
                this.in.unread(10);
            }
        }
        else if ((this.flags & 0x2) != 0x0 && b == 10 && this.previous != 13) {
            b = 13;
            this.in.unread(10);
        }
        return this.previous = b;
    }
}
