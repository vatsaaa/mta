// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.james.mime4j.io;

public interface LineNumberSource
{
    int getLineNumber();
}
