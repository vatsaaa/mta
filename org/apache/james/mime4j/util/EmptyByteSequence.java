// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.james.mime4j.util;

final class EmptyByteSequence implements ByteSequence
{
    private static final byte[] EMPTY_BYTES;
    
    public int length() {
        return 0;
    }
    
    public byte byteAt(final int index) {
        throw new IndexOutOfBoundsException();
    }
    
    public byte[] toByteArray() {
        return EmptyByteSequence.EMPTY_BYTES;
    }
    
    static {
        EMPTY_BYTES = new byte[0];
    }
}
