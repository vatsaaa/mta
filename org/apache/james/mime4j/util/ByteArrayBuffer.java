// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.james.mime4j.util;

public final class ByteArrayBuffer implements ByteSequence
{
    private byte[] buffer;
    private int len;
    
    public ByteArrayBuffer(final int capacity) {
        if (capacity < 0) {
            throw new IllegalArgumentException("Buffer capacity may not be negative");
        }
        this.buffer = new byte[capacity];
    }
    
    public ByteArrayBuffer(final byte[] bytes, final boolean dontCopy) {
        this(bytes, bytes.length, dontCopy);
    }
    
    public ByteArrayBuffer(final byte[] bytes, final int len, final boolean dontCopy) {
        if (bytes == null) {
            throw new IllegalArgumentException();
        }
        if (len < 0 || len > bytes.length) {
            throw new IllegalArgumentException();
        }
        if (dontCopy) {
            this.buffer = bytes;
        }
        else {
            System.arraycopy(bytes, 0, this.buffer = new byte[len], 0, len);
        }
        this.len = len;
    }
    
    private void expand(final int newlen) {
        final byte[] newbuffer = new byte[Math.max(this.buffer.length << 1, newlen)];
        System.arraycopy(this.buffer, 0, newbuffer, 0, this.len);
        this.buffer = newbuffer;
    }
    
    public void append(final byte[] b, final int off, final int len) {
        if (b == null) {
            return;
        }
        if (off < 0 || off > b.length || len < 0 || off + len < 0 || off + len > b.length) {
            throw new IndexOutOfBoundsException();
        }
        if (len == 0) {
            return;
        }
        final int newlen = this.len + len;
        if (newlen > this.buffer.length) {
            this.expand(newlen);
        }
        System.arraycopy(b, off, this.buffer, this.len, len);
        this.len = newlen;
    }
    
    public void append(final int b) {
        final int newlen = this.len + 1;
        if (newlen > this.buffer.length) {
            this.expand(newlen);
        }
        this.buffer[this.len] = (byte)b;
        this.len = newlen;
    }
    
    public void clear() {
        this.len = 0;
    }
    
    public byte[] toByteArray() {
        final byte[] b = new byte[this.len];
        if (this.len > 0) {
            System.arraycopy(this.buffer, 0, b, 0, this.len);
        }
        return b;
    }
    
    public byte byteAt(final int i) {
        if (i < 0 || i >= this.len) {
            throw new IndexOutOfBoundsException();
        }
        return this.buffer[i];
    }
    
    public int capacity() {
        return this.buffer.length;
    }
    
    public int length() {
        return this.len;
    }
    
    public byte[] buffer() {
        return this.buffer;
    }
    
    public int indexOf(final byte b) {
        return this.indexOf(b, 0, this.len);
    }
    
    public int indexOf(final byte b, int beginIndex, int endIndex) {
        if (beginIndex < 0) {
            beginIndex = 0;
        }
        if (endIndex > this.len) {
            endIndex = this.len;
        }
        if (beginIndex > endIndex) {
            return -1;
        }
        for (int i = beginIndex; i < endIndex; ++i) {
            if (this.buffer[i] == b) {
                return i;
            }
        }
        return -1;
    }
    
    public void setLength(final int len) {
        if (len < 0 || len > this.buffer.length) {
            throw new IndexOutOfBoundsException();
        }
        this.len = len;
    }
    
    public void remove(final int off, final int len) {
        if (off < 0 || off > this.len || len < 0 || off + len < 0 || off + len > this.len) {
            throw new IndexOutOfBoundsException();
        }
        if (len == 0) {
            return;
        }
        final int remaining = this.len - off - len;
        if (remaining > 0) {
            System.arraycopy(this.buffer, off + len, this.buffer, off, remaining);
        }
        this.len -= len;
    }
    
    public boolean isEmpty() {
        return this.len == 0;
    }
    
    public boolean isFull() {
        return this.len == this.buffer.length;
    }
    
    @Override
    public String toString() {
        return new String(this.toByteArray());
    }
}
