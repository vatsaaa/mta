// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.james.mime4j.util;

public interface ByteSequence
{
    public static final ByteSequence EMPTY = new EmptyByteSequence();
    
    int length();
    
    byte byteAt(final int p0);
    
    byte[] toByteArray();
}
