// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.james.mime4j.util;

import java.nio.charset.UnsupportedCharsetException;
import java.nio.charset.IllegalCharsetNameException;
import java.nio.charset.Charset;

public class CharsetUtil
{
    public static final String CRLF = "\r\n";
    public static final int CR = 13;
    public static final int LF = 10;
    public static final int SP = 32;
    public static final int HT = 9;
    public static final Charset US_ASCII;
    public static final Charset ISO_8859_1;
    public static final Charset UTF_8;
    public static final Charset DEFAULT_CHARSET;
    
    public static boolean isASCII(final char ch) {
        return ('\uff80' & ch) == 0x0;
    }
    
    public static boolean isASCII(final String s) {
        if (s == null) {
            throw new IllegalArgumentException("String may not be null");
        }
        for (int len = s.length(), i = 0; i < len; ++i) {
            if (!isASCII(s.charAt(i))) {
                return false;
            }
        }
        return true;
    }
    
    public static boolean isWhitespace(final char ch) {
        return ch == ' ' || ch == '\t' || ch == '\r' || ch == '\n';
    }
    
    public static boolean isWhitespace(final String s) {
        if (s == null) {
            throw new IllegalArgumentException("String may not be null");
        }
        for (int len = s.length(), i = 0; i < len; ++i) {
            if (!isWhitespace(s.charAt(i))) {
                return false;
            }
        }
        return true;
    }
    
    public static Charset lookup(final String name) {
        if (name == null) {
            return null;
        }
        try {
            return Charset.forName(name);
        }
        catch (IllegalCharsetNameException ex) {
            return null;
        }
        catch (UnsupportedCharsetException ex2) {
            return null;
        }
    }
    
    static {
        US_ASCII = Charset.forName("US-ASCII");
        ISO_8859_1 = Charset.forName("ISO-8859-1");
        UTF_8 = Charset.forName("UTF-8");
        DEFAULT_CHARSET = CharsetUtil.US_ASCII;
    }
}
