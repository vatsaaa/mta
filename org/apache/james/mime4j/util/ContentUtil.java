// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.james.mime4j.util;

import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;

public class ContentUtil
{
    private ContentUtil() {
    }
    
    public static ByteSequence encode(final String string) {
        if (string == null) {
            return null;
        }
        final ByteArrayBuffer buf = new ByteArrayBuffer(string.length());
        for (int i = 0; i < string.length(); ++i) {
            buf.append((byte)string.charAt(i));
        }
        return buf;
    }
    
    public static ByteSequence encode(Charset charset, final String string) {
        if (string == null) {
            return null;
        }
        if (charset == null) {
            charset = Charset.defaultCharset();
        }
        final ByteBuffer encoded = charset.encode(CharBuffer.wrap(string));
        final ByteArrayBuffer buf = new ByteArrayBuffer(encoded.remaining());
        buf.append(encoded.array(), encoded.position(), encoded.remaining());
        return buf;
    }
    
    public static String decode(final ByteSequence byteSequence) {
        if (byteSequence == null) {
            return null;
        }
        return decode(byteSequence, 0, byteSequence.length());
    }
    
    public static String decode(final Charset charset, final ByteSequence byteSequence) {
        return decode(charset, byteSequence, 0, byteSequence.length());
    }
    
    public static String decode(final ByteSequence byteSequence, final int offset, final int length) {
        if (byteSequence == null) {
            return null;
        }
        final StringBuilder buf = new StringBuilder(length);
        for (int i = offset; i < offset + length; ++i) {
            buf.append((char)(byteSequence.byteAt(i) & 0xFF));
        }
        return buf.toString();
    }
    
    public static String decode(Charset charset, final ByteSequence byteSequence, final int offset, final int length) {
        if (byteSequence == null) {
            return null;
        }
        if (charset == null) {
            charset = Charset.defaultCharset();
        }
        if (byteSequence instanceof ByteArrayBuffer) {
            final ByteArrayBuffer bab = (ByteArrayBuffer)byteSequence;
            return decode(charset, bab.buffer(), offset, length);
        }
        final byte[] bytes = byteSequence.toByteArray();
        return decode(charset, bytes, offset, length);
    }
    
    private static String decode(final Charset charset, final byte[] buffer, final int offset, final int length) {
        return charset.decode(ByteBuffer.wrap(buffer, offset, length)).toString();
    }
}
