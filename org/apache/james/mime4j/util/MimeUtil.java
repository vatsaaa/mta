// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.james.mime4j.util;

import java.text.FieldPosition;
import java.util.Locale;
import java.text.SimpleDateFormat;
import java.util.TimeZone;
import java.util.Date;
import java.text.DateFormat;
import java.util.Random;

public final class MimeUtil
{
    public static final String ENC_QUOTED_PRINTABLE = "quoted-printable";
    public static final String ENC_BINARY = "binary";
    public static final String ENC_BASE64 = "base64";
    public static final String ENC_8BIT = "8bit";
    public static final String ENC_7BIT = "7bit";
    private static final Random random;
    private static int counter;
    private static final ThreadLocal<DateFormat> RFC822_DATE_FORMAT;
    
    private MimeUtil() {
    }
    
    public static boolean isSameMimeType(final String pType1, final String pType2) {
        return pType1 != null && pType2 != null && pType1.equalsIgnoreCase(pType2);
    }
    
    public static boolean isMessage(final String pMimeType) {
        return pMimeType != null && pMimeType.equalsIgnoreCase("message/rfc822");
    }
    
    public static boolean isMultipart(final String pMimeType) {
        return pMimeType != null && pMimeType.toLowerCase().startsWith("multipart/");
    }
    
    public static boolean isBase64Encoding(final String pTransferEncoding) {
        return "base64".equalsIgnoreCase(pTransferEncoding);
    }
    
    public static boolean isQuotedPrintableEncoded(final String pTransferEncoding) {
        return "quoted-printable".equalsIgnoreCase(pTransferEncoding);
    }
    
    public static String createUniqueBoundary() {
        final StringBuilder sb = new StringBuilder();
        sb.append("-=Part.");
        sb.append(Integer.toHexString(nextCounterValue()));
        sb.append('.');
        sb.append(Long.toHexString(MimeUtil.random.nextLong()));
        sb.append('.');
        sb.append(Long.toHexString(System.currentTimeMillis()));
        sb.append('.');
        sb.append(Long.toHexString(MimeUtil.random.nextLong()));
        sb.append("=-");
        return sb.toString();
    }
    
    public static String createUniqueMessageId(final String hostName) {
        final StringBuilder sb = new StringBuilder("<Mime4j.");
        sb.append(Integer.toHexString(nextCounterValue()));
        sb.append('.');
        sb.append(Long.toHexString(MimeUtil.random.nextLong()));
        sb.append('.');
        sb.append(Long.toHexString(System.currentTimeMillis()));
        if (hostName != null) {
            sb.append('@');
            sb.append(hostName);
        }
        sb.append('>');
        return sb.toString();
    }
    
    public static String formatDate(final Date date, final TimeZone zone) {
        final DateFormat df = MimeUtil.RFC822_DATE_FORMAT.get();
        if (zone == null) {
            df.setTimeZone(TimeZone.getDefault());
        }
        else {
            df.setTimeZone(zone);
        }
        return df.format(date);
    }
    
    public static String fold(final String s, final int usedCharacters) {
        final int maxCharacters = 76;
        final int length = s.length();
        if (usedCharacters + length <= 76) {
            return s;
        }
        final StringBuilder sb = new StringBuilder();
        int lastLineBreak = -usedCharacters;
        int nextWspIdx;
        for (int wspIdx = indexOfWsp(s, 0); wspIdx != length; wspIdx = nextWspIdx) {
            nextWspIdx = indexOfWsp(s, wspIdx + 1);
            if (nextWspIdx - lastLineBreak > 76) {
                sb.append(s.substring(Math.max(0, lastLineBreak), wspIdx));
                sb.append("\r\n");
                lastLineBreak = wspIdx;
            }
        }
        sb.append(s.substring(Math.max(0, lastLineBreak)));
        return sb.toString();
    }
    
    public static String unfold(final String s) {
        for (int length = s.length(), idx = 0; idx < length; ++idx) {
            final char c = s.charAt(idx);
            if (c == '\r' || c == '\n') {
                return unfold0(s, idx);
            }
        }
        return s;
    }
    
    private static String unfold0(final String s, final int crlfIdx) {
        final int length = s.length();
        final StringBuilder sb = new StringBuilder(length);
        if (crlfIdx > 0) {
            sb.append(s.substring(0, crlfIdx));
        }
        for (int idx = crlfIdx + 1; idx < length; ++idx) {
            final char c = s.charAt(idx);
            if (c != '\r' && c != '\n') {
                sb.append(c);
            }
        }
        return sb.toString();
    }
    
    private static int indexOfWsp(final String s, final int fromIndex) {
        final int len = s.length();
        for (int index = fromIndex; index < len; ++index) {
            final char c = s.charAt(index);
            if (c == ' ' || c == '\t') {
                return index;
            }
        }
        return len;
    }
    
    private static synchronized int nextCounterValue() {
        return MimeUtil.counter++;
    }
    
    static {
        random = new Random();
        MimeUtil.counter = 0;
        RFC822_DATE_FORMAT = new ThreadLocal<DateFormat>() {
            @Override
            protected DateFormat initialValue() {
                return new Rfc822DateFormat();
            }
        };
    }
    
    private static final class Rfc822DateFormat extends SimpleDateFormat
    {
        private static final long serialVersionUID = 1L;
        
        public Rfc822DateFormat() {
            super("EEE, d MMM yyyy HH:mm:ss ", Locale.US);
        }
        
        @Override
        public StringBuffer format(final Date date, final StringBuffer toAppendTo, final FieldPosition pos) {
            final StringBuffer sb = super.format(date, toAppendTo, pos);
            final int zoneMillis = this.calendar.get(15);
            final int dstMillis = this.calendar.get(16);
            int minutes = (zoneMillis + dstMillis) / 1000 / 60;
            if (minutes < 0) {
                sb.append('-');
                minutes = -minutes;
            }
            else {
                sb.append('+');
            }
            sb.append(String.format("%02d%02d", minutes / 60, minutes % 60));
            return sb;
        }
    }
}
