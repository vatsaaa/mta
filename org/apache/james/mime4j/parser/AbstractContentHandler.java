// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.james.mime4j.parser;

import org.apache.james.mime4j.stream.Field;
import java.io.IOException;
import java.io.InputStream;
import org.apache.james.mime4j.stream.BodyDescriptor;
import org.apache.james.mime4j.MimeException;

public abstract class AbstractContentHandler implements ContentHandler
{
    public void endMultipart() throws MimeException {
    }
    
    public void startMultipart(final BodyDescriptor bd) throws MimeException {
    }
    
    public void body(final BodyDescriptor bd, final InputStream is) throws MimeException, IOException {
    }
    
    public void endBodyPart() throws MimeException {
    }
    
    public void endHeader() throws MimeException {
    }
    
    public void endMessage() throws MimeException {
    }
    
    public void epilogue(final InputStream is) throws MimeException, IOException {
    }
    
    public void field(final Field field) throws MimeException {
    }
    
    public void preamble(final InputStream is) throws MimeException, IOException {
    }
    
    public void startBodyPart() throws MimeException {
    }
    
    public void startHeader() throws MimeException {
    }
    
    public void startMessage() throws MimeException {
    }
    
    public void raw(final InputStream is) throws MimeException, IOException {
    }
}
