// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.james.mime4j.parser;

import org.apache.james.mime4j.stream.BodyDescriptor;
import java.io.IOException;
import java.io.InputStream;
import org.apache.james.mime4j.stream.Field;
import org.apache.james.mime4j.MimeException;

public interface ContentHandler
{
    void startMessage() throws MimeException;
    
    void endMessage() throws MimeException;
    
    void startBodyPart() throws MimeException;
    
    void endBodyPart() throws MimeException;
    
    void startHeader() throws MimeException;
    
    void field(final Field p0) throws MimeException;
    
    void endHeader() throws MimeException;
    
    void preamble(final InputStream p0) throws MimeException, IOException;
    
    void epilogue(final InputStream p0) throws MimeException, IOException;
    
    void startMultipart(final BodyDescriptor p0) throws MimeException;
    
    void endMultipart() throws MimeException;
    
    void body(final BodyDescriptor p0, final InputStream p1) throws MimeException, IOException;
    
    void raw(final InputStream p0) throws MimeException, IOException;
}
