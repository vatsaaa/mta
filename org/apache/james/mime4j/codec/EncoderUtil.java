// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.james.mime4j.codec;

import java.nio.ByteBuffer;
import org.apache.james.mime4j.util.CharsetUtil;
import java.nio.charset.Charset;
import java.util.Locale;
import java.util.BitSet;

public class EncoderUtil
{
    private static final byte[] BASE64_TABLE;
    private static final char BASE64_PAD = '=';
    private static final BitSet Q_REGULAR_CHARS;
    private static final BitSet Q_RESTRICTED_CHARS;
    private static final int MAX_USED_CHARACTERS = 50;
    private static final String ENC_WORD_PREFIX = "=?";
    private static final String ENC_WORD_SUFFIX = "?=";
    private static final int ENCODED_WORD_MAX_LENGTH = 75;
    private static final BitSet TOKEN_CHARS;
    private static final BitSet ATEXT_CHARS;
    
    private static BitSet initChars(final String specials) {
        final BitSet bs = new BitSet(128);
        for (char ch = '!'; ch < '\u007f'; ++ch) {
            if (specials.indexOf(ch) == -1) {
                bs.set(ch);
            }
        }
        return bs;
    }
    
    private EncoderUtil() {
    }
    
    public static String encodeAddressDisplayName(final String displayName) {
        if (isAtomPhrase(displayName)) {
            return displayName;
        }
        if (hasToBeEncoded(displayName, 0)) {
            return encodeEncodedWord(displayName, Usage.WORD_ENTITY);
        }
        return quote(displayName);
    }
    
    public static String encodeAddressLocalPart(final String localPart) {
        if (isDotAtomText(localPart)) {
            return localPart;
        }
        return quote(localPart);
    }
    
    public static String encodeHeaderParameter(String name, final String value) {
        name = name.toLowerCase(Locale.US);
        if (isToken(value)) {
            return name + "=" + value;
        }
        return name + "=" + quote(value);
    }
    
    public static String encodeIfNecessary(final String text, final Usage usage, final int usedCharacters) {
        if (hasToBeEncoded(text, usedCharacters)) {
            return encodeEncodedWord(text, usage, usedCharacters);
        }
        return text;
    }
    
    public static boolean hasToBeEncoded(final String text, final int usedCharacters) {
        if (text == null) {
            throw new IllegalArgumentException();
        }
        if (usedCharacters < 0 || usedCharacters > 50) {
            throw new IllegalArgumentException();
        }
        int nonWhiteSpaceCount = usedCharacters;
        for (int idx = 0; idx < text.length(); ++idx) {
            final char ch = text.charAt(idx);
            if (ch == '\t' || ch == ' ') {
                nonWhiteSpaceCount = 0;
            }
            else {
                if (++nonWhiteSpaceCount > 77) {
                    return true;
                }
                if (ch < ' ' || ch >= '\u007f') {
                    return true;
                }
            }
        }
        return false;
    }
    
    public static String encodeEncodedWord(final String text, final Usage usage) {
        return encodeEncodedWord(text, usage, 0, null, null);
    }
    
    public static String encodeEncodedWord(final String text, final Usage usage, final int usedCharacters) {
        return encodeEncodedWord(text, usage, usedCharacters, null, null);
    }
    
    public static String encodeEncodedWord(final String text, final Usage usage, final int usedCharacters, Charset charset, Encoding encoding) {
        if (text == null) {
            throw new IllegalArgumentException();
        }
        if (usedCharacters < 0 || usedCharacters > 50) {
            throw new IllegalArgumentException();
        }
        if (charset == null) {
            charset = determineCharset(text);
        }
        final byte[] bytes = encode(text, charset);
        if (encoding == null) {
            encoding = determineEncoding(bytes, usage);
        }
        if (encoding == Encoding.B) {
            final String prefix = "=?" + charset.name() + "?B?";
            return encodeB(prefix, text, usedCharacters, charset, bytes);
        }
        final String prefix = "=?" + charset.name() + "?Q?";
        return encodeQ(prefix, text, usage, usedCharacters, charset, bytes);
    }
    
    public static String encodeB(final byte[] bytes) {
        final StringBuilder sb = new StringBuilder();
        int idx;
        int end;
        for (idx = 0, end = bytes.length; idx < end - 2; idx += 3) {
            final int data = (bytes[idx] & 0xFF) << 16 | (bytes[idx + 1] & 0xFF) << 8 | (bytes[idx + 2] & 0xFF);
            sb.append((char)EncoderUtil.BASE64_TABLE[data >> 18 & 0x3F]);
            sb.append((char)EncoderUtil.BASE64_TABLE[data >> 12 & 0x3F]);
            sb.append((char)EncoderUtil.BASE64_TABLE[data >> 6 & 0x3F]);
            sb.append((char)EncoderUtil.BASE64_TABLE[data & 0x3F]);
        }
        if (idx == end - 2) {
            final int data = (bytes[idx] & 0xFF) << 16 | (bytes[idx + 1] & 0xFF) << 8;
            sb.append((char)EncoderUtil.BASE64_TABLE[data >> 18 & 0x3F]);
            sb.append((char)EncoderUtil.BASE64_TABLE[data >> 12 & 0x3F]);
            sb.append((char)EncoderUtil.BASE64_TABLE[data >> 6 & 0x3F]);
            sb.append('=');
        }
        else if (idx == end - 1) {
            final int data = (bytes[idx] & 0xFF) << 16;
            sb.append((char)EncoderUtil.BASE64_TABLE[data >> 18 & 0x3F]);
            sb.append((char)EncoderUtil.BASE64_TABLE[data >> 12 & 0x3F]);
            sb.append('=');
            sb.append('=');
        }
        return sb.toString();
    }
    
    public static String encodeQ(final byte[] bytes, final Usage usage) {
        final BitSet qChars = (usage == Usage.TEXT_TOKEN) ? EncoderUtil.Q_REGULAR_CHARS : EncoderUtil.Q_RESTRICTED_CHARS;
        final StringBuilder sb = new StringBuilder();
        for (int end = bytes.length, idx = 0; idx < end; ++idx) {
            final int v = bytes[idx] & 0xFF;
            if (v == 32) {
                sb.append('_');
            }
            else if (!qChars.get(v)) {
                sb.append('=');
                sb.append(hexDigit(v >>> 4));
                sb.append(hexDigit(v & 0xF));
            }
            else {
                sb.append((char)v);
            }
        }
        return sb.toString();
    }
    
    public static boolean isToken(final String str) {
        final int length = str.length();
        if (length == 0) {
            return false;
        }
        for (int idx = 0; idx < length; ++idx) {
            final char ch = str.charAt(idx);
            if (!EncoderUtil.TOKEN_CHARS.get(ch)) {
                return false;
            }
        }
        return true;
    }
    
    private static boolean isAtomPhrase(final String str) {
        boolean containsAText = false;
        for (int length = str.length(), idx = 0; idx < length; ++idx) {
            final char ch = str.charAt(idx);
            if (EncoderUtil.ATEXT_CHARS.get(ch)) {
                containsAText = true;
            }
            else if (!CharsetUtil.isWhitespace(ch)) {
                return false;
            }
        }
        return containsAText;
    }
    
    private static boolean isDotAtomText(final String str) {
        char prev = '.';
        final int length = str.length();
        if (length == 0) {
            return false;
        }
        for (int idx = 0; idx < length; ++idx) {
            final char ch = str.charAt(idx);
            if (ch == '.') {
                if (prev == '.' || idx == length - 1) {
                    return false;
                }
            }
            else if (!EncoderUtil.ATEXT_CHARS.get(ch)) {
                return false;
            }
            prev = ch;
        }
        return true;
    }
    
    private static String quote(final String str) {
        final String escaped = str.replaceAll("[\\\\\"]", "\\\\$0");
        return "\"" + escaped + "\"";
    }
    
    private static String encodeB(final String prefix, final String text, final int usedCharacters, final Charset charset, final byte[] bytes) {
        final int encodedLength = bEncodedLength(bytes);
        final int totalLength = prefix.length() + encodedLength + "?=".length();
        if (totalLength <= 75 - usedCharacters) {
            return prefix + encodeB(bytes) + "?=";
        }
        final String part1 = text.substring(0, text.length() / 2);
        final byte[] bytes2 = encode(part1, charset);
        final String word1 = encodeB(prefix, part1, usedCharacters, charset, bytes2);
        final String part2 = text.substring(text.length() / 2);
        final byte[] bytes3 = encode(part2, charset);
        final String word2 = encodeB(prefix, part2, 0, charset, bytes3);
        return word1 + " " + word2;
    }
    
    private static int bEncodedLength(final byte[] bytes) {
        return (bytes.length + 2) / 3 * 4;
    }
    
    private static String encodeQ(final String prefix, final String text, final Usage usage, final int usedCharacters, final Charset charset, final byte[] bytes) {
        final int encodedLength = qEncodedLength(bytes, usage);
        final int totalLength = prefix.length() + encodedLength + "?=".length();
        if (totalLength <= 75 - usedCharacters) {
            return prefix + encodeQ(bytes, usage) + "?=";
        }
        final String part1 = text.substring(0, text.length() / 2);
        final byte[] bytes2 = encode(part1, charset);
        final String word1 = encodeQ(prefix, part1, usage, usedCharacters, charset, bytes2);
        final String part2 = text.substring(text.length() / 2);
        final byte[] bytes3 = encode(part2, charset);
        final String word2 = encodeQ(prefix, part2, usage, 0, charset, bytes3);
        return word1 + " " + word2;
    }
    
    private static int qEncodedLength(final byte[] bytes, final Usage usage) {
        final BitSet qChars = (usage == Usage.TEXT_TOKEN) ? EncoderUtil.Q_REGULAR_CHARS : EncoderUtil.Q_RESTRICTED_CHARS;
        int count = 0;
        for (int idx = 0; idx < bytes.length; ++idx) {
            final int v = bytes[idx] & 0xFF;
            if (v == 32) {
                ++count;
            }
            else if (!qChars.get(v)) {
                count += 3;
            }
            else {
                ++count;
            }
        }
        return count;
    }
    
    private static byte[] encode(final String text, final Charset charset) {
        final ByteBuffer buffer = charset.encode(text);
        final byte[] bytes = new byte[buffer.limit()];
        buffer.get(bytes);
        return bytes;
    }
    
    private static Charset determineCharset(final String text) {
        boolean ascii = true;
        for (int len = text.length(), index = 0; index < len; ++index) {
            final char ch = text.charAt(index);
            if (ch > '\u00ff') {
                return CharsetUtil.UTF_8;
            }
            if (ch > '\u007f') {
                ascii = false;
            }
        }
        return ascii ? CharsetUtil.US_ASCII : CharsetUtil.ISO_8859_1;
    }
    
    private static Encoding determineEncoding(final byte[] bytes, final Usage usage) {
        if (bytes.length == 0) {
            return Encoding.Q;
        }
        final BitSet qChars = (usage == Usage.TEXT_TOKEN) ? EncoderUtil.Q_REGULAR_CHARS : EncoderUtil.Q_RESTRICTED_CHARS;
        int qEncoded = 0;
        for (int i = 0; i < bytes.length; ++i) {
            final int v = bytes[i] & 0xFF;
            if (v != 32 && !qChars.get(v)) {
                ++qEncoded;
            }
        }
        final int percentage = qEncoded * 100 / bytes.length;
        return (percentage > 30) ? Encoding.B : Encoding.Q;
    }
    
    private static char hexDigit(final int i) {
        return (i < 10) ? ((char)(i + 48)) : ((char)(i - 10 + 65));
    }
    
    static {
        BASE64_TABLE = Base64OutputStream.BASE64_TABLE;
        Q_REGULAR_CHARS = initChars("=_?");
        Q_RESTRICTED_CHARS = initChars("=_?\"#$%&'(),.:;<>@[\\]^`{|}~");
        TOKEN_CHARS = initChars("()<>@,;:\\\"/[]?=");
        ATEXT_CHARS = initChars("()<>@.,;:\\\"[]");
    }
    
    public enum Encoding
    {
        B, 
        Q;
    }
    
    public enum Usage
    {
        TEXT_TOKEN, 
        WORD_ENTITY;
    }
}
