// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.james.mime4j.codec;

import java.util.HashSet;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Set;
import java.io.FilterOutputStream;

public class Base64OutputStream extends FilterOutputStream
{
    private static final int DEFAULT_LINE_LENGTH = 76;
    private static final byte[] CRLF_SEPARATOR;
    static final byte[] BASE64_TABLE;
    private static final byte BASE64_PAD = 61;
    private static final Set<Byte> BASE64_CHARS;
    private static final int MASK_6BITS = 63;
    private static final int ENCODED_BUFFER_SIZE = 2048;
    private final byte[] singleByte;
    private final int lineLength;
    private final byte[] lineSeparator;
    private boolean closed;
    private final byte[] encoded;
    private int position;
    private int data;
    private int modulus;
    private int linePosition;
    
    public Base64OutputStream(final OutputStream out) {
        this(out, 76, Base64OutputStream.CRLF_SEPARATOR);
    }
    
    public Base64OutputStream(final OutputStream out, final int lineLength) {
        this(out, lineLength, Base64OutputStream.CRLF_SEPARATOR);
    }
    
    public Base64OutputStream(final OutputStream out, final int lineLength, final byte[] lineSeparator) {
        super(out);
        this.singleByte = new byte[1];
        this.closed = false;
        this.position = 0;
        this.data = 0;
        this.modulus = 0;
        this.linePosition = 0;
        if (out == null) {
            throw new IllegalArgumentException();
        }
        if (lineLength < 0) {
            throw new IllegalArgumentException();
        }
        this.checkLineSeparator(lineSeparator);
        this.lineLength = lineLength;
        System.arraycopy(lineSeparator, 0, this.lineSeparator = new byte[lineSeparator.length], 0, lineSeparator.length);
        this.encoded = new byte[2048];
    }
    
    @Override
    public final void write(final int b) throws IOException {
        if (this.closed) {
            throw new IOException("Base64OutputStream has been closed");
        }
        this.singleByte[0] = (byte)b;
        this.write0(this.singleByte, 0, 1);
    }
    
    @Override
    public final void write(final byte[] buffer) throws IOException {
        if (this.closed) {
            throw new IOException("Base64OutputStream has been closed");
        }
        if (buffer == null) {
            throw new NullPointerException();
        }
        if (buffer.length == 0) {
            return;
        }
        this.write0(buffer, 0, buffer.length);
    }
    
    @Override
    public final void write(final byte[] buffer, final int offset, final int length) throws IOException {
        if (this.closed) {
            throw new IOException("Base64OutputStream has been closed");
        }
        if (buffer == null) {
            throw new NullPointerException();
        }
        if (offset < 0 || length < 0 || offset + length > buffer.length) {
            throw new IndexOutOfBoundsException();
        }
        if (length == 0) {
            return;
        }
        this.write0(buffer, offset, offset + length);
    }
    
    @Override
    public void flush() throws IOException {
        if (this.closed) {
            throw new IOException("Base64OutputStream has been closed");
        }
        this.flush0();
    }
    
    @Override
    public void close() throws IOException {
        if (this.closed) {
            return;
        }
        this.closed = true;
        this.close0();
    }
    
    private void write0(final byte[] buffer, final int from, final int to) throws IOException {
        for (int i = from; i < to; ++i) {
            this.data = (this.data << 8 | (buffer[i] & 0xFF));
            if (++this.modulus == 3) {
                this.modulus = 0;
                if (this.lineLength > 0 && this.linePosition >= this.lineLength) {
                    this.linePosition = 0;
                    if (this.encoded.length - this.position < this.lineSeparator.length) {
                        this.flush0();
                    }
                    for (final byte ls : this.lineSeparator) {
                        this.encoded[this.position++] = ls;
                    }
                }
                if (this.encoded.length - this.position < 4) {
                    this.flush0();
                }
                this.encoded[this.position++] = Base64OutputStream.BASE64_TABLE[this.data >> 18 & 0x3F];
                this.encoded[this.position++] = Base64OutputStream.BASE64_TABLE[this.data >> 12 & 0x3F];
                this.encoded[this.position++] = Base64OutputStream.BASE64_TABLE[this.data >> 6 & 0x3F];
                this.encoded[this.position++] = Base64OutputStream.BASE64_TABLE[this.data & 0x3F];
                this.linePosition += 4;
            }
        }
    }
    
    private void flush0() throws IOException {
        if (this.position > 0) {
            this.out.write(this.encoded, 0, this.position);
            this.position = 0;
        }
    }
    
    private void close0() throws IOException {
        if (this.modulus != 0) {
            this.writePad();
        }
        if (this.lineLength > 0 && this.linePosition > 0) {
            this.writeLineSeparator();
        }
        this.flush0();
    }
    
    private void writePad() throws IOException {
        if (this.lineLength > 0 && this.linePosition >= this.lineLength) {
            this.writeLineSeparator();
        }
        if (this.encoded.length - this.position < 4) {
            this.flush0();
        }
        if (this.modulus == 1) {
            this.encoded[this.position++] = Base64OutputStream.BASE64_TABLE[this.data >> 2 & 0x3F];
            this.encoded[this.position++] = Base64OutputStream.BASE64_TABLE[this.data << 4 & 0x3F];
            this.encoded[this.position++] = 61;
            this.encoded[this.position++] = 61;
        }
        else {
            assert this.modulus == 2;
            this.encoded[this.position++] = Base64OutputStream.BASE64_TABLE[this.data >> 10 & 0x3F];
            this.encoded[this.position++] = Base64OutputStream.BASE64_TABLE[this.data >> 4 & 0x3F];
            this.encoded[this.position++] = Base64OutputStream.BASE64_TABLE[this.data << 2 & 0x3F];
            this.encoded[this.position++] = 61;
        }
        this.linePosition += 4;
    }
    
    private void writeLineSeparator() throws IOException {
        this.linePosition = 0;
        if (this.encoded.length - this.position < this.lineSeparator.length) {
            this.flush0();
        }
        for (final byte ls : this.lineSeparator) {
            this.encoded[this.position++] = ls;
        }
    }
    
    private void checkLineSeparator(final byte[] lineSeparator) {
        if (lineSeparator.length > 2048) {
            throw new IllegalArgumentException("line separator length exceeds 2048");
        }
        for (final byte b : lineSeparator) {
            if (Base64OutputStream.BASE64_CHARS.contains(b)) {
                throw new IllegalArgumentException("line separator must not contain base64 character '" + (char)(b & 0xFF) + "'");
            }
        }
    }
    
    static {
        CRLF_SEPARATOR = new byte[] { 13, 10 };
        BASE64_TABLE = new byte[] { 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 43, 47 };
        BASE64_CHARS = new HashSet<Byte>();
        for (final byte b : Base64OutputStream.BASE64_TABLE) {
            Base64OutputStream.BASE64_CHARS.add(b);
        }
        Base64OutputStream.BASE64_CHARS.add((Byte)61);
    }
}
