// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.james.mime4j.codec;

import java.io.IOException;
import org.apache.james.mime4j.util.ByteArrayBuffer;
import java.io.InputStream;

public class Base64InputStream extends InputStream
{
    private static final int ENCODED_BUFFER_SIZE = 1536;
    private static final int[] BASE64_DECODE;
    private static final byte BASE64_PAD = 61;
    private static final int EOF = -1;
    private final byte[] singleByte;
    private final InputStream in;
    private final byte[] encoded;
    private final ByteArrayBuffer decodedBuf;
    private int position;
    private int size;
    private boolean closed;
    private boolean eof;
    private final DecodeMonitor monitor;
    
    public Base64InputStream(final InputStream in, final DecodeMonitor monitor) {
        this(1536, in, monitor);
    }
    
    protected Base64InputStream(final int bufsize, final InputStream in, final DecodeMonitor monitor) {
        this.singleByte = new byte[1];
        this.position = 0;
        this.size = 0;
        this.closed = false;
        if (in == null) {
            throw new IllegalArgumentException();
        }
        this.encoded = new byte[bufsize];
        this.decodedBuf = new ByteArrayBuffer(512);
        this.in = in;
        this.monitor = monitor;
    }
    
    public Base64InputStream(final InputStream in) {
        this(in, false);
    }
    
    public Base64InputStream(final InputStream in, final boolean strict) {
        this(1536, in, strict ? DecodeMonitor.STRICT : DecodeMonitor.SILENT);
    }
    
    @Override
    public int read() throws IOException {
        if (this.closed) {
            throw new IOException("Stream has been closed");
        }
        while (true) {
            final int bytes = this.read0(this.singleByte, 0, 1);
            if (bytes == -1) {
                return -1;
            }
            if (bytes == 1) {
                return this.singleByte[0] & 0xFF;
            }
        }
    }
    
    @Override
    public int read(final byte[] buffer) throws IOException {
        if (this.closed) {
            throw new IOException("Stream has been closed");
        }
        if (buffer == null) {
            throw new NullPointerException();
        }
        if (buffer.length == 0) {
            return 0;
        }
        return this.read0(buffer, 0, buffer.length);
    }
    
    @Override
    public int read(final byte[] buffer, final int offset, final int length) throws IOException {
        if (this.closed) {
            throw new IOException("Stream has been closed");
        }
        if (buffer == null) {
            throw new NullPointerException();
        }
        if (offset < 0 || length < 0 || offset + length > buffer.length) {
            throw new IndexOutOfBoundsException();
        }
        if (length == 0) {
            return 0;
        }
        return this.read0(buffer, offset, length);
    }
    
    @Override
    public void close() throws IOException {
        if (this.closed) {
            return;
        }
        this.closed = true;
    }
    
    private int read0(final byte[] buffer, final int off, final int len) throws IOException {
        final int from = off;
        final int to = off + len;
        int index = off;
        if (this.decodedBuf.length() > 0) {
            final int chunk = Math.min(this.decodedBuf.length(), len);
            System.arraycopy(this.decodedBuf.buffer(), 0, buffer, index, chunk);
            this.decodedBuf.remove(0, chunk);
            index += chunk;
        }
        if (this.eof) {
            return (index == from) ? -1 : (index - from);
        }
        int data = 0;
        int sextets = 0;
        while (index < to) {
            while (this.position == this.size) {
                final int n = this.in.read(this.encoded, 0, this.encoded.length);
                if (n == -1) {
                    this.eof = true;
                    if (sextets != 0) {
                        this.handleUnexpectedEof(sextets);
                    }
                    return (index == from) ? -1 : (index - from);
                }
                if (n > 0) {
                    this.position = 0;
                    this.size = n;
                }
                else {
                    assert n == 0;
                    continue;
                }
            }
            while (this.position < this.size && index < to) {
                final int value = this.encoded[this.position++] & 0xFF;
                if (value == 61) {
                    index = this.decodePad(data, sextets, buffer, index, to);
                    return index - from;
                }
                final int decoded = Base64InputStream.BASE64_DECODE[value];
                if (decoded < 0) {
                    if (value != 13 && value != 10 && value != 32 && this.monitor.warn("Unexpected base64 byte: " + (byte)value, "ignoring.")) {
                        throw new IOException("Unexpected base64 byte");
                    }
                    continue;
                }
                else {
                    data = (data << 6 | decoded);
                    if (++sextets != 4) {
                        continue;
                    }
                    sextets = 0;
                    final byte b1 = (byte)(data >>> 16);
                    final byte b2 = (byte)(data >>> 8);
                    final byte b3 = (byte)data;
                    if (index < to - 2) {
                        buffer[index++] = b1;
                        buffer[index++] = b2;
                        buffer[index++] = b3;
                    }
                    else {
                        if (index < to - 1) {
                            buffer[index++] = b1;
                            buffer[index++] = b2;
                            this.decodedBuf.append(b3);
                        }
                        else if (index < to) {
                            buffer[index++] = b1;
                            this.decodedBuf.append(b2);
                            this.decodedBuf.append(b3);
                        }
                        else {
                            this.decodedBuf.append(b1);
                            this.decodedBuf.append(b2);
                            this.decodedBuf.append(b3);
                        }
                        assert index == to;
                        return to - from;
                    }
                }
            }
        }
        assert sextets == 0;
        assert index == to;
        return to - from;
    }
    
    private int decodePad(final int data, final int sextets, final byte[] buffer, int index, final int end) throws IOException {
        this.eof = true;
        if (sextets == 2) {
            final byte b = (byte)(data >>> 4);
            if (index < end) {
                buffer[index++] = b;
            }
            else {
                this.decodedBuf.append(b);
            }
        }
        else if (sextets == 3) {
            final byte b2 = (byte)(data >>> 10);
            final byte b3 = (byte)(data >>> 2 & 0xFF);
            if (index < end - 1) {
                buffer[index++] = b2;
                buffer[index++] = b3;
            }
            else if (index < end) {
                buffer[index++] = b2;
                this.decodedBuf.append(b3);
            }
            else {
                this.decodedBuf.append(b2);
                this.decodedBuf.append(b3);
            }
        }
        else {
            this.handleUnexpecedPad(sextets);
        }
        return index;
    }
    
    private void handleUnexpectedEof(final int sextets) throws IOException {
        if (this.monitor.warn("Unexpected end of BASE64 stream", "dropping " + sextets + " sextet(s)")) {
            throw new IOException("Unexpected end of BASE64 stream");
        }
    }
    
    private void handleUnexpecedPad(final int sextets) throws IOException {
        if (this.monitor.warn("Unexpected padding character", "dropping " + sextets + " sextet(s)")) {
            throw new IOException("Unexpected padding character");
        }
    }
    
    static {
        BASE64_DECODE = new int[256];
        for (int i = 0; i < 256; ++i) {
            Base64InputStream.BASE64_DECODE[i] = -1;
        }
        for (int i = 0; i < Base64OutputStream.BASE64_TABLE.length; ++i) {
            Base64InputStream.BASE64_DECODE[Base64OutputStream.BASE64_TABLE[i] & 0xFF] = i;
        }
    }
}
