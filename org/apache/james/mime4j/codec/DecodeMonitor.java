// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.james.mime4j.codec;

public class DecodeMonitor
{
    public static final DecodeMonitor STRICT;
    public static final DecodeMonitor SILENT;
    
    public boolean warn(final String error, final String dropDesc) {
        return false;
    }
    
    public boolean isListening() {
        return false;
    }
    
    static {
        STRICT = new DecodeMonitor() {
            @Override
            public boolean warn(final String error, final String dropDesc) {
                return true;
            }
            
            @Override
            public boolean isListening() {
                return true;
            }
        };
        SILENT = new DecodeMonitor();
    }
}
