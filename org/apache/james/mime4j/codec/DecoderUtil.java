// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.james.mime4j.codec;

import java.nio.charset.Charset;
import java.util.regex.Matcher;
import org.apache.james.mime4j.util.CharsetUtil;
import java.io.UnsupportedEncodingException;
import java.io.IOException;
import java.io.InputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.regex.Pattern;

public class DecoderUtil
{
    private static final Pattern PATTERN_ENCODED_WORD;
    
    private static byte[] decodeQuotedPrintable(final String s, final DecodeMonitor monitor) {
        final ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try {
            final byte[] bytes = s.getBytes("US-ASCII");
            final QuotedPrintableInputStream is = new QuotedPrintableInputStream(new ByteArrayInputStream(bytes), monitor);
            int b = 0;
            while ((b = is.read()) != -1) {
                baos.write(b);
            }
        }
        catch (IOException e) {
            throw new IllegalStateException(e);
        }
        return baos.toByteArray();
    }
    
    private static byte[] decodeBase64(final String s, final DecodeMonitor monitor) {
        final ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try {
            final byte[] bytes = s.getBytes("US-ASCII");
            final Base64InputStream is = new Base64InputStream(new ByteArrayInputStream(bytes), monitor);
            int b = 0;
            while ((b = is.read()) != -1) {
                baos.write(b);
            }
        }
        catch (IOException e) {
            throw new IllegalStateException(e);
        }
        return baos.toByteArray();
    }
    
    static String decodeB(final String encodedText, final String charset, final DecodeMonitor monitor) throws UnsupportedEncodingException {
        final byte[] decodedBytes = decodeBase64(encodedText, monitor);
        return new String(decodedBytes, charset);
    }
    
    static String decodeQ(String encodedText, final String charset, final DecodeMonitor monitor) throws UnsupportedEncodingException {
        encodedText = replaceUnderscores(encodedText);
        final byte[] decodedBytes = decodeQuotedPrintable(encodedText, monitor);
        return new String(decodedBytes, charset);
    }
    
    static String decodeEncodedWords(final String body) {
        return decodeEncodedWords(body, DecodeMonitor.SILENT);
    }
    
    public static String decodeEncodedWords(final String body, final DecodeMonitor monitor) throws IllegalArgumentException {
        int tailIndex = 0;
        boolean lastMatchValid = false;
        final StringBuilder sb = new StringBuilder();
        final Matcher matcher = DecoderUtil.PATTERN_ENCODED_WORD.matcher(body);
        while (matcher.find()) {
            final String separator = matcher.group(1);
            final String mimeCharset = matcher.group(2);
            final String encoding = matcher.group(3);
            final String encodedText = matcher.group(4);
            String decoded = null;
            decoded = tryDecodeEncodedWord(mimeCharset, encoding, encodedText, monitor);
            if (decoded == null) {
                sb.append(matcher.group(0));
            }
            else {
                if (!lastMatchValid || !CharsetUtil.isWhitespace(separator)) {
                    sb.append(separator);
                }
                sb.append(decoded);
            }
            tailIndex = matcher.end();
            lastMatchValid = (decoded != null);
        }
        if (tailIndex == 0) {
            return body;
        }
        sb.append(body.substring(tailIndex));
        return sb.toString();
    }
    
    private static String tryDecodeEncodedWord(final String mimeCharset, final String encoding, final String encodedText, final DecodeMonitor monitor) {
        final Charset charset = CharsetUtil.lookup(mimeCharset);
        if (charset == null) {
            monitor(monitor, mimeCharset, encoding, encodedText, "leaving word encoded", "Mime charser '", mimeCharset, "' doesn't have a corresponding Java charset");
            return null;
        }
        if (encodedText.length() == 0) {
            monitor(monitor, mimeCharset, encoding, encodedText, "leaving word encoded", "Missing encoded text in encoded word");
            return null;
        }
        try {
            if (encoding.equalsIgnoreCase("Q")) {
                return decodeQ(encodedText, charset.name(), monitor);
            }
            if (encoding.equalsIgnoreCase("B")) {
                return decodeB(encodedText, charset.name(), monitor);
            }
            monitor(monitor, mimeCharset, encoding, encodedText, "leaving word encoded", "Warning: Unknown encoding in encoded word");
            return null;
        }
        catch (UnsupportedEncodingException e) {
            monitor(monitor, mimeCharset, encoding, encodedText, "leaving word encoded", "Unsupported encoding (", e.getMessage(), ") in encoded word");
            return null;
        }
        catch (RuntimeException e2) {
            monitor(monitor, mimeCharset, encoding, encodedText, "leaving word encoded", "Could not decode (", e2.getMessage(), ") encoded word");
            return null;
        }
    }
    
    private static void monitor(final DecodeMonitor monitor, final String mimeCharset, final String encoding, final String encodedText, final String dropDesc, final String... strings) throws IllegalArgumentException {
        if (monitor.isListening()) {
            final String encodedWord = recombine(mimeCharset, encoding, encodedText);
            final StringBuilder text = new StringBuilder();
            for (final String str : strings) {
                text.append(str);
            }
            text.append(" (");
            text.append(encodedWord);
            text.append(")");
            final String exceptionDesc = text.toString();
            if (monitor.warn(exceptionDesc, dropDesc)) {
                throw new IllegalArgumentException(text.toString());
            }
        }
    }
    
    private static String recombine(final String mimeCharset, final String encoding, final String encodedText) {
        return "=?" + mimeCharset + "?" + encoding + "?" + encodedText + "?=";
    }
    
    private static String replaceUnderscores(final String str) {
        final StringBuilder sb = new StringBuilder(128);
        for (int i = 0; i < str.length(); ++i) {
            final char c = str.charAt(i);
            if (c == '_') {
                sb.append("=20");
            }
            else {
                sb.append(c);
            }
        }
        return sb.toString();
    }
    
    static {
        PATTERN_ENCODED_WORD = Pattern.compile("(.*?)=\\?(.+?)\\?(\\w)\\?(.+?)\\?=", 32);
    }
}
