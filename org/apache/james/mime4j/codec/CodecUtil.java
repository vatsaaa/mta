// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.james.mime4j.codec;

import java.io.IOException;
import java.io.OutputStream;
import java.io.InputStream;

public class CodecUtil
{
    static final int DEFAULT_ENCODING_BUFFER_SIZE = 1024;
    
    public static void copy(final InputStream in, final OutputStream out) throws IOException {
        final byte[] buffer = new byte[1024];
        int inputLength;
        while (-1 != (inputLength = in.read(buffer))) {
            out.write(buffer, 0, inputLength);
        }
    }
    
    public static void encodeQuotedPrintableBinary(final InputStream in, final OutputStream out) throws IOException {
        final QuotedPrintableOutputStream qpOut = new QuotedPrintableOutputStream(out, true);
        copy(in, qpOut);
        qpOut.close();
    }
    
    public static void encodeQuotedPrintable(final InputStream in, final OutputStream out) throws IOException {
        final QuotedPrintableOutputStream qpOut = new QuotedPrintableOutputStream(out, false);
        copy(in, qpOut);
        qpOut.close();
    }
    
    public static void encodeBase64(final InputStream in, final OutputStream out) throws IOException {
        final Base64OutputStream b64Out = new Base64OutputStream(out);
        copy(in, b64Out);
        b64Out.close();
    }
    
    public static OutputStream wrapQuotedPrintable(final OutputStream out, final boolean binary) throws IOException {
        return new QuotedPrintableOutputStream(out, binary);
    }
    
    public static OutputStream wrapBase64(final OutputStream out) throws IOException {
        return new Base64OutputStream(out);
    }
}
