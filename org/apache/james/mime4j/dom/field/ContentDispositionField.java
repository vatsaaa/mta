// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.james.mime4j.dom.field;

import java.util.Date;
import java.util.Map;

public interface ContentDispositionField extends ParsedField
{
    public static final String DISPOSITION_TYPE_INLINE = "inline";
    public static final String DISPOSITION_TYPE_ATTACHMENT = "attachment";
    public static final String PARAM_FILENAME = "filename";
    public static final String PARAM_CREATION_DATE = "creation-date";
    public static final String PARAM_MODIFICATION_DATE = "modification-date";
    public static final String PARAM_READ_DATE = "read-date";
    public static final String PARAM_SIZE = "size";
    
    String getDispositionType();
    
    String getParameter(final String p0);
    
    Map<String, String> getParameters();
    
    boolean isDispositionType(final String p0);
    
    boolean isInline();
    
    boolean isAttachment();
    
    String getFilename();
    
    Date getCreationDate();
    
    Date getModificationDate();
    
    Date getReadDate();
    
    long getSize();
}
