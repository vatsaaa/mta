// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.james.mime4j.dom.field;

import org.apache.james.mime4j.MimeException;

public class ParseException extends MimeException
{
    private static final long serialVersionUID = 1L;
    
    protected ParseException(final String message) {
        super(message);
    }
    
    protected ParseException(final Throwable cause) {
        super(cause);
    }
    
    protected ParseException(final String message, final Throwable cause) {
        super(message, cause);
    }
}
