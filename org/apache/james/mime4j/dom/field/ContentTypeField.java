// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.james.mime4j.dom.field;

import java.util.Map;

public interface ContentTypeField extends ParsedField
{
    public static final String TYPE_MULTIPART_PREFIX = "multipart/";
    public static final String TYPE_MULTIPART_DIGEST = "multipart/digest";
    public static final String TYPE_TEXT_PLAIN = "text/plain";
    public static final String TYPE_MESSAGE_RFC822 = "message/rfc822";
    public static final String PARAM_BOUNDARY = "boundary";
    public static final String PARAM_CHARSET = "charset";
    
    String getMimeType();
    
    String getMediaType();
    
    String getSubType();
    
    String getParameter(final String p0);
    
    Map<String, String> getParameters();
    
    boolean isMimeType(final String p0);
    
    boolean isMultipart();
    
    String getBoundary();
    
    String getCharset();
}
