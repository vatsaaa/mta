// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.james.mime4j.dom.field;

public interface ContentMD5Field extends ParsedField
{
    String getMD5Raw();
}
