// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.james.mime4j.dom.field;

public interface MimeVersionField extends ParsedField
{
    int getMinorVersion();
    
    int getMajorVersion();
}
