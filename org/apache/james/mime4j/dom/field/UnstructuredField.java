// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.james.mime4j.dom.field;

public interface UnstructuredField extends ParsedField
{
    String getValue();
}
