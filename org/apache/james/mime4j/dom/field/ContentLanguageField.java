// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.james.mime4j.dom.field;

import java.util.List;

public interface ContentLanguageField extends ParsedField
{
    List<String> getLanguages();
}
