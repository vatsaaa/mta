// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.james.mime4j.dom;

import java.io.InputStream;
import java.util.Enumeration;
import java.io.IOException;
import java.io.Reader;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;

class ServiceLoader
{
    private ServiceLoader() {
    }
    
    static <T> T load(final Class<T> spiClass) {
        final String spiResURI = "META-INF/services/" + spiClass.getName();
        final ClassLoader classLoader = spiClass.getClassLoader();
        try {
            final Enumeration<URL> resources = classLoader.getResources(spiResURI);
            while (resources.hasMoreElements()) {
                final URL resource = resources.nextElement();
                final InputStream instream = resource.openStream();
                try {
                    final BufferedReader reader = new BufferedReader(new InputStreamReader(instream));
                    String line;
                    while ((line = reader.readLine()) != null) {
                        line = line.trim();
                        final int cmtIdx = line.indexOf(35);
                        if (cmtIdx != -1) {
                            line = line.substring(0, cmtIdx);
                            line = line.trim();
                        }
                        if (line.length() == 0) {
                            continue;
                        }
                        final Class<?> implClass = classLoader.loadClass(line);
                        if (spiClass.isAssignableFrom(implClass)) {
                            final Object impl = implClass.newInstance();
                            return spiClass.cast(impl);
                        }
                    }
                    reader.close();
                }
                finally {
                    instream.close();
                }
            }
            return null;
        }
        catch (IOException ex) {
            throw new ServiceLoaderException(ex);
        }
        catch (ClassNotFoundException ex2) {
            throw new ServiceLoaderException("Unknown SPI class '" + spiClass.getName() + "'", ex2);
        }
        catch (IllegalAccessException ex4) {
            return null;
        }
        catch (InstantiationException ex3) {
            throw new ServiceLoaderException("SPI class '" + spiClass.getName() + "' cannot be instantiated", ex3);
        }
    }
}
