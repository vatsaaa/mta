// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.james.mime4j.dom;

import org.apache.james.mime4j.codec.DecodeMonitor;
import org.apache.james.mime4j.stream.Field;
import org.apache.james.mime4j.dom.field.ParsedField;

public interface FieldParser<T extends ParsedField>
{
    T parse(final Field p0, final DecodeMonitor p1);
}
