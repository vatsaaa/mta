// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.james.mime4j.dom.datetime;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.TimeZone;
import java.util.Date;

public class DateTime
{
    private final Date date;
    private final int year;
    private final int month;
    private final int day;
    private final int hour;
    private final int minute;
    private final int second;
    private final int timeZone;
    
    public DateTime(final String yearString, final int month, final int day, final int hour, final int minute, final int second, final int timeZone) {
        this.year = this.convertToYear(yearString);
        this.date = convertToDate(this.year, month, day, hour, minute, second, timeZone);
        this.month = month;
        this.day = day;
        this.hour = hour;
        this.minute = minute;
        this.second = second;
        this.timeZone = timeZone;
    }
    
    private int convertToYear(final String yearString) {
        final int year = Integer.parseInt(yearString);
        switch (yearString.length()) {
            case 1:
            case 2: {
                if (year >= 0 && year < 50) {
                    return 2000 + year;
                }
                return 1900 + year;
            }
            case 3: {
                return 1900 + year;
            }
            default: {
                return year;
            }
        }
    }
    
    public static Date convertToDate(final int year, final int month, final int day, final int hour, final int minute, final int second, final int timeZone) {
        final Calendar c = new GregorianCalendar(TimeZone.getTimeZone("GMT+0"));
        c.set(year, month - 1, day, hour, minute, second);
        c.set(14, 0);
        if (timeZone != Integer.MIN_VALUE) {
            final int minutes = timeZone / 100 * 60 + timeZone % 100;
            c.add(12, -1 * minutes);
        }
        return c.getTime();
    }
    
    public Date getDate() {
        return this.date;
    }
    
    public int getYear() {
        return this.year;
    }
    
    public int getMonth() {
        return this.month;
    }
    
    public int getDay() {
        return this.day;
    }
    
    public int getHour() {
        return this.hour;
    }
    
    public int getMinute() {
        return this.minute;
    }
    
    public int getSecond() {
        return this.second;
    }
    
    public int getTimeZone() {
        return this.timeZone;
    }
    
    public void print() {
        System.out.println(this.toString());
    }
    
    @Override
    public String toString() {
        return this.getYear() + " " + this.getMonth() + " " + this.getDay() + "; " + this.getHour() + " " + this.getMinute() + " " + this.getSecond() + " " + this.getTimeZone();
    }
    
    @Override
    public int hashCode() {
        final int PRIME = 31;
        int result = 1;
        result = 31 * result + ((this.date == null) ? 0 : this.date.hashCode());
        result = 31 * result + this.day;
        result = 31 * result + this.hour;
        result = 31 * result + this.minute;
        result = 31 * result + this.month;
        result = 31 * result + this.second;
        result = 31 * result + this.timeZone;
        result = 31 * result + this.year;
        return result;
    }
    
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        final DateTime other = (DateTime)obj;
        if (this.date == null) {
            if (other.date != null) {
                return false;
            }
        }
        else if (!this.date.equals(other.date)) {
            return false;
        }
        return this.day == other.day && this.hour == other.hour && this.minute == other.minute && this.month == other.month && this.second == other.second && this.timeZone == other.timeZone && this.year == other.year;
    }
}
