// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.james.mime4j.dom;

import java.util.List;

public interface Multipart extends Body
{
    String getSubType();
    
    int getCount();
    
    List<Entity> getBodyParts();
    
    void setBodyParts(final List<Entity> p0);
    
    void addBodyPart(final Entity p0);
    
    void addBodyPart(final Entity p0, final int p1);
    
    Entity removeBodyPart(final int p0);
    
    Entity replaceBodyPart(final Entity p0, final int p1);
    
    String getPreamble();
    
    void setPreamble(final String p0);
    
    String getEpilogue();
    
    void setEpilogue(final String p0);
}
