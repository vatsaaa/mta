// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.james.mime4j.dom;

public class ServiceLoaderException extends RuntimeException
{
    private static final long serialVersionUID = -2801857820835508778L;
    
    public ServiceLoaderException(final String message) {
        super(message);
    }
    
    public ServiceLoaderException(final Throwable cause) {
        super(cause);
    }
    
    public ServiceLoaderException(final String message, final Throwable cause) {
        super(message, cause);
    }
}
