// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.james.mime4j.dom;

import java.io.IOException;
import org.apache.james.mime4j.MimeException;
import java.io.InputStream;

public interface MessageBuilder
{
    Header newHeader();
    
    Header newHeader(final Header p0);
    
    Multipart newMultipart(final String p0);
    
    Multipart newMultipart(final Multipart p0);
    
    Message newMessage();
    
    Message newMessage(final Message p0);
    
    Header parseHeader(final InputStream p0) throws MimeException, IOException;
    
    Message parseMessage(final InputStream p0) throws MimeException, IOException;
}
