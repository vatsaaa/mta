// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.james.mime4j.dom.address;

import java.util.Collections;
import java.util.Locale;
import org.apache.james.mime4j.util.LangUtils;
import java.util.List;

public class Mailbox extends Address
{
    private static final long serialVersionUID = 1L;
    private static final DomainList EMPTY_ROUTE_LIST;
    private final String name;
    private final DomainList route;
    private final String localPart;
    private final String domain;
    
    public Mailbox(final String name, final DomainList route, final String localPart, final String domain) {
        if (localPart == null) {
            throw new IllegalArgumentException();
        }
        this.name = ((name == null || name.length() == 0) ? null : name);
        this.route = ((route == null) ? Mailbox.EMPTY_ROUTE_LIST : route);
        this.localPart = localPart;
        this.domain = ((domain == null || domain.length() == 0) ? null : domain);
    }
    
    Mailbox(final String name, final Mailbox baseMailbox) {
        this(name, baseMailbox.getRoute(), baseMailbox.getLocalPart(), baseMailbox.getDomain());
    }
    
    public Mailbox(final String localPart, final String domain) {
        this(null, null, localPart, domain);
    }
    
    public Mailbox(final DomainList route, final String localPart, final String domain) {
        this(null, route, localPart, domain);
    }
    
    public Mailbox(final String name, final String localPart, final String domain) {
        this(name, null, localPart, domain);
    }
    
    public String getName() {
        return this.name;
    }
    
    public DomainList getRoute() {
        return this.route;
    }
    
    public String getLocalPart() {
        return this.localPart;
    }
    
    public String getDomain() {
        return this.domain;
    }
    
    public String getAddress() {
        if (this.domain == null) {
            return this.localPart;
        }
        return this.localPart + '@' + this.domain;
    }
    
    @Override
    protected final void doAddMailboxesTo(final List<Mailbox> results) {
        results.add(this);
    }
    
    @Override
    public int hashCode() {
        int hash = 17;
        hash = LangUtils.hashCode(hash, this.localPart);
        hash = LangUtils.hashCode(hash, (this.domain != null) ? this.domain.toLowerCase(Locale.US) : null);
        return hash;
    }
    
    @Override
    public boolean equals(final Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof Mailbox)) {
            return false;
        }
        final Mailbox that = (Mailbox)obj;
        return LangUtils.equals(this.localPart, that.localPart) && LangUtils.equalsIgnoreCase(this.domain, that.domain);
    }
    
    @Override
    public String toString() {
        return this.getAddress();
    }
    
    static {
        EMPTY_ROUTE_LIST = new DomainList(Collections.emptyList(), true);
    }
}
