// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.james.mime4j.dom.address;

import java.util.Collections;
import java.util.Collection;
import java.util.ArrayList;
import java.util.List;
import java.io.Serializable;
import java.util.AbstractList;

public class MailboxList extends AbstractList<Mailbox> implements Serializable
{
    private static final long serialVersionUID = 1L;
    private final List<Mailbox> mailboxes;
    
    public MailboxList(final List<Mailbox> mailboxes, final boolean dontCopy) {
        if (mailboxes != null) {
            this.mailboxes = (dontCopy ? mailboxes : new ArrayList<Mailbox>(mailboxes));
        }
        else {
            this.mailboxes = Collections.emptyList();
        }
    }
    
    @Override
    public int size() {
        return this.mailboxes.size();
    }
    
    @Override
    public Mailbox get(final int index) {
        return this.mailboxes.get(index);
    }
}
