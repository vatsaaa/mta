// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.james.mime4j.dom.address;

import java.util.List;
import java.io.Serializable;

public abstract class Address implements Serializable
{
    private static final long serialVersionUID = 634090661990433426L;
    
    final void addMailboxesTo(final List<Mailbox> results) {
        this.doAddMailboxesTo(results);
    }
    
    protected abstract void doAddMailboxesTo(final List<Mailbox> p0);
}
