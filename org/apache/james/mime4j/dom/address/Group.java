// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.james.mime4j.dom.address;

import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Arrays;

public class Group extends Address
{
    private static final long serialVersionUID = 1L;
    private final String name;
    private final MailboxList mailboxList;
    
    public Group(final String name, final MailboxList mailboxes) {
        if (name == null) {
            throw new IllegalArgumentException();
        }
        if (mailboxes == null) {
            throw new IllegalArgumentException();
        }
        this.name = name;
        this.mailboxList = mailboxes;
    }
    
    public Group(final String name, final Mailbox... mailboxes) {
        this(name, new MailboxList(Arrays.asList(mailboxes), true));
    }
    
    public Group(final String name, final Collection<Mailbox> mailboxes) {
        this(name, new MailboxList(new ArrayList<Mailbox>(mailboxes), true));
    }
    
    public String getName() {
        return this.name;
    }
    
    public MailboxList getMailboxes() {
        return this.mailboxList;
    }
    
    @Override
    protected void doAddMailboxesTo(final List<Mailbox> results) {
        for (final Mailbox mailbox : this.mailboxList) {
            results.add(mailbox);
        }
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append(this.name);
        sb.append(':');
        boolean first = true;
        for (final Mailbox mailbox : this.mailboxList) {
            if (first) {
                first = false;
            }
            else {
                sb.append(',');
            }
            sb.append(' ');
            sb.append(mailbox);
        }
        sb.append(";");
        return sb.toString();
    }
}
