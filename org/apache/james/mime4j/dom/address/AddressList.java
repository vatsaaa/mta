// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.james.mime4j.dom.address;

import java.util.Iterator;
import java.util.Collections;
import java.util.Collection;
import java.util.ArrayList;
import java.util.List;
import java.io.Serializable;
import java.util.AbstractList;

public class AddressList extends AbstractList<Address> implements Serializable
{
    private static final long serialVersionUID = 1L;
    private final List<? extends Address> addresses;
    
    public AddressList(final List<? extends Address> addresses, final boolean dontCopy) {
        if (addresses != null) {
            this.addresses = (dontCopy ? addresses : new ArrayList<Address>(addresses));
        }
        else {
            this.addresses = Collections.emptyList();
        }
    }
    
    @Override
    public int size() {
        return this.addresses.size();
    }
    
    @Override
    public Address get(final int index) {
        return (Address)this.addresses.get(index);
    }
    
    public MailboxList flatten() {
        boolean groupDetected = false;
        for (final Address addr : this.addresses) {
            if (!(addr instanceof Mailbox)) {
                groupDetected = true;
                break;
            }
        }
        if (!groupDetected) {
            final List<Mailbox> mailboxes = (List<Mailbox>)this.addresses;
            return new MailboxList(mailboxes, true);
        }
        final List<Mailbox> results = new ArrayList<Mailbox>();
        for (final Address addr2 : this.addresses) {
            addr2.addMailboxesTo(results);
        }
        return new MailboxList(results, false);
    }
}
