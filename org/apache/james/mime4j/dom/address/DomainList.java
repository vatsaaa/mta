// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.james.mime4j.dom.address;

import java.util.Iterator;
import java.util.Collections;
import java.util.Collection;
import java.util.ArrayList;
import java.util.List;
import java.io.Serializable;
import java.util.AbstractList;

public class DomainList extends AbstractList<String> implements Serializable
{
    private static final long serialVersionUID = 1L;
    private final List<String> domains;
    
    public DomainList(final List<String> domains, final boolean dontCopy) {
        if (domains != null) {
            this.domains = (dontCopy ? domains : new ArrayList<String>(domains));
        }
        else {
            this.domains = Collections.emptyList();
        }
    }
    
    @Override
    public int size() {
        return this.domains.size();
    }
    
    @Override
    public String get(final int index) {
        return this.domains.get(index);
    }
    
    public String toRouteString() {
        final StringBuilder sb = new StringBuilder();
        for (final String domain : this.domains) {
            if (sb.length() > 0) {
                sb.append(',');
            }
            sb.append("@");
            sb.append(domain);
        }
        return sb.toString();
    }
    
    @Override
    public String toString() {
        return this.toRouteString();
    }
}
