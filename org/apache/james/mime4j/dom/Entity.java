// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.james.mime4j.dom;

public interface Entity extends Disposable
{
    Entity getParent();
    
    void setParent(final Entity p0);
    
    Header getHeader();
    
    void setHeader(final Header p0);
    
    Body getBody();
    
    void setBody(final Body p0);
    
    Body removeBody();
    
    boolean isMultipart();
    
    String getMimeType();
    
    String getCharset();
    
    String getContentTransferEncoding();
    
    String getDispositionType();
    
    String getFilename();
}
