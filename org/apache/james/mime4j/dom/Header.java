// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.james.mime4j.dom;

import java.util.Iterator;
import java.util.List;
import org.apache.james.mime4j.stream.Field;

public interface Header extends Iterable<Field>
{
    void addField(final Field p0);
    
    List<Field> getFields();
    
    Field getField(final String p0);
    
    List<Field> getFields(final String p0);
    
    Iterator<Field> iterator();
    
    int removeFields(final String p0);
    
    void setField(final Field p0);
}
