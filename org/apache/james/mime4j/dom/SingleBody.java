// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.james.mime4j.dom;

import java.io.OutputStream;
import java.io.IOException;
import java.io.InputStream;

public abstract class SingleBody implements Body
{
    private Entity parent;
    static final int DEFAULT_ENCODING_BUFFER_SIZE = 1024;
    
    protected SingleBody() {
        this.parent = null;
    }
    
    public Entity getParent() {
        return this.parent;
    }
    
    public void setParent(final Entity parent) {
        this.parent = parent;
    }
    
    public abstract InputStream getInputStream() throws IOException;
    
    public void writeTo(final OutputStream out) throws IOException {
        if (out == null) {
            throw new IllegalArgumentException();
        }
        final InputStream in = this.getInputStream();
        copy(in, out);
        in.close();
    }
    
    public SingleBody copy() {
        throw new UnsupportedOperationException();
    }
    
    public void dispose() {
    }
    
    private static void copy(final InputStream in, final OutputStream out) throws IOException {
        final byte[] buffer = new byte[1024];
        int inputLength;
        while (-1 != (inputLength = in.read(buffer))) {
            out.write(buffer, 0, inputLength);
        }
    }
}
