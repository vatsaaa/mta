// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.james.mime4j.dom;

import org.apache.james.mime4j.dom.address.Address;
import org.apache.james.mime4j.dom.address.AddressList;
import java.util.Collection;
import org.apache.james.mime4j.dom.address.MailboxList;
import org.apache.james.mime4j.dom.address.Mailbox;
import java.util.TimeZone;
import java.util.Date;

public interface Message extends Entity, Body
{
    String getMessageId();
    
    void createMessageId(final String p0);
    
    String getSubject();
    
    void setSubject(final String p0);
    
    Date getDate();
    
    void setDate(final Date p0);
    
    void setDate(final Date p0, final TimeZone p1);
    
    Mailbox getSender();
    
    void setSender(final Mailbox p0);
    
    MailboxList getFrom();
    
    void setFrom(final Mailbox p0);
    
    void setFrom(final Mailbox... p0);
    
    void setFrom(final Collection<Mailbox> p0);
    
    AddressList getTo();
    
    void setTo(final Address p0);
    
    void setTo(final Address... p0);
    
    void setTo(final Collection<? extends Address> p0);
    
    AddressList getCc();
    
    void setCc(final Address p0);
    
    void setCc(final Address... p0);
    
    void setCc(final Collection<? extends Address> p0);
    
    AddressList getBcc();
    
    void setBcc(final Address p0);
    
    void setBcc(final Address... p0);
    
    void setBcc(final Collection<? extends Address> p0);
    
    AddressList getReplyTo();
    
    void setReplyTo(final Address p0);
    
    void setReplyTo(final Address... p0);
    
    void setReplyTo(final Collection<? extends Address> p0);
}
