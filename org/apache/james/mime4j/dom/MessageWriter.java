// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.james.mime4j.dom;

import org.apache.james.mime4j.stream.Field;
import java.io.IOException;
import java.io.OutputStream;

public interface MessageWriter
{
    void writeMessage(final Message p0, final OutputStream p1) throws IOException;
    
    void writeBody(final Body p0, final OutputStream p1) throws IOException;
    
    void writeEntity(final Entity p0, final OutputStream p1) throws IOException;
    
    void writeMultipart(final Multipart p0, final OutputStream p1) throws IOException;
    
    void writeField(final Field p0, final OutputStream p1) throws IOException;
    
    void writeHeader(final Header p0, final OutputStream p1) throws IOException;
}
