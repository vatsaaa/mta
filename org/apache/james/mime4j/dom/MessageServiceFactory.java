// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.james.mime4j.dom;

import org.apache.james.mime4j.MimeException;

public abstract class MessageServiceFactory
{
    public static MessageServiceFactory newInstance() throws MimeException {
        return ServiceLoader.load(MessageServiceFactory.class);
    }
    
    public abstract MessageBuilder newMessageBuilder();
    
    public abstract MessageWriter newMessageWriter();
    
    public abstract void setAttribute(final String p0, final Object p1) throws IllegalArgumentException;
}
