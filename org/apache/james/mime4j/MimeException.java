// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.james.mime4j;

public class MimeException extends Exception
{
    private static final long serialVersionUID = 8352821278714188542L;
    
    public MimeException(final String message) {
        super(message);
    }
    
    public MimeException(final Throwable cause) {
        super(cause);
    }
    
    public MimeException(final String message, final Throwable cause) {
        super(message, cause);
    }
}
