// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.james.mime4j.message;

import java.util.Collections;
import java.util.Iterator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import org.apache.james.mime4j.stream.Field;
import java.util.List;
import org.apache.james.mime4j.dom.Header;

public abstract class AbstractHeader implements Header
{
    private List<Field> fields;
    private Map<String, List<Field>> fieldMap;
    
    public AbstractHeader() {
        this.fields = new LinkedList<Field>();
        this.fieldMap = new HashMap<String, List<Field>>();
    }
    
    public AbstractHeader(final Header other) {
        this.fields = new LinkedList<Field>();
        this.fieldMap = new HashMap<String, List<Field>>();
        for (final Field otherField : other.getFields()) {
            this.addField(otherField);
        }
    }
    
    public void addField(final Field field) {
        List<Field> values = this.fieldMap.get(field.getName().toLowerCase());
        if (values == null) {
            values = new LinkedList<Field>();
            this.fieldMap.put(field.getName().toLowerCase(), values);
        }
        values.add(field);
        this.fields.add(field);
    }
    
    public List<Field> getFields() {
        return Collections.unmodifiableList((List<? extends Field>)this.fields);
    }
    
    public Field getField(final String name) {
        final List<Field> l = this.fieldMap.get(name.toLowerCase());
        if (l != null && !l.isEmpty()) {
            return l.get(0);
        }
        return null;
    }
    
    public List<Field> getFields(final String name) {
        final String lowerCaseName = name.toLowerCase();
        final List<Field> l = this.fieldMap.get(lowerCaseName);
        List<Field> results;
        if (l == null || l.isEmpty()) {
            results = Collections.emptyList();
        }
        else {
            results = Collections.unmodifiableList((List<? extends Field>)l);
        }
        return results;
    }
    
    public Iterator<Field> iterator() {
        return Collections.unmodifiableList((List<? extends Field>)this.fields).iterator();
    }
    
    public int removeFields(final String name) {
        final String lowerCaseName = name.toLowerCase();
        final List<Field> removed = this.fieldMap.remove(lowerCaseName);
        if (removed == null || removed.isEmpty()) {
            return 0;
        }
        final Iterator<Field> iterator = this.fields.iterator();
        while (iterator.hasNext()) {
            final Field field = iterator.next();
            if (field.getName().equalsIgnoreCase(name)) {
                iterator.remove();
            }
        }
        return removed.size();
    }
    
    public void setField(final Field field) {
        final String lowerCaseName = field.getName().toLowerCase();
        final List<Field> l = this.fieldMap.get(lowerCaseName);
        if (l == null || l.isEmpty()) {
            this.addField(field);
            return;
        }
        l.clear();
        l.add(field);
        int firstOccurrence = -1;
        int index = 0;
        final Iterator<Field> iterator = this.fields.iterator();
        while (iterator.hasNext()) {
            final Field f = iterator.next();
            if (f.getName().equalsIgnoreCase(field.getName())) {
                iterator.remove();
                if (firstOccurrence == -1) {
                    firstOccurrence = index;
                }
            }
            ++index;
        }
        this.fields.add(firstOccurrence, field);
    }
    
    @Override
    public String toString() {
        final StringBuilder str = new StringBuilder(128);
        for (final Field field : this.fields) {
            str.append(field.toString());
            str.append("\r\n");
        }
        return str.toString();
    }
}
