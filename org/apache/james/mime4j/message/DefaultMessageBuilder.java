// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.james.mime4j.message;

import java.io.IOException;
import org.apache.james.mime4j.MimeIOException;
import org.apache.james.mime4j.parser.ContentHandler;
import org.apache.james.mime4j.MimeException;
import org.apache.james.mime4j.parser.AbstractContentHandler;
import org.apache.james.mime4j.parser.MimeStreamParser;
import org.apache.james.mime4j.field.LenientFieldParser;
import org.apache.james.mime4j.field.DefaultFieldParser;
import java.io.InputStream;
import org.apache.james.mime4j.dom.SingleBody;
import org.apache.james.mime4j.dom.Message;
import org.apache.james.mime4j.dom.Body;
import org.apache.james.mime4j.dom.Multipart;
import org.apache.james.mime4j.dom.Entity;
import java.util.Iterator;
import org.apache.james.mime4j.stream.Field;
import org.apache.james.mime4j.dom.Header;
import org.apache.james.mime4j.codec.DecodeMonitor;
import org.apache.james.mime4j.stream.BodyDescriptorBuilder;
import org.apache.james.mime4j.stream.MimeConfig;
import org.apache.james.mime4j.dom.field.ParsedField;
import org.apache.james.mime4j.dom.FieldParser;
import org.apache.james.mime4j.dom.MessageBuilder;

public class DefaultMessageBuilder implements MessageBuilder
{
    private FieldParser<? extends ParsedField> fieldParser;
    private BodyFactory bodyFactory;
    private MimeConfig config;
    private BodyDescriptorBuilder bodyDescBuilder;
    private boolean contentDecoding;
    private boolean flatMode;
    private DecodeMonitor monitor;
    
    public DefaultMessageBuilder() {
        this.fieldParser = null;
        this.bodyFactory = null;
        this.config = null;
        this.bodyDescBuilder = null;
        this.contentDecoding = true;
        this.flatMode = false;
        this.monitor = null;
    }
    
    public void setFieldParser(final FieldParser<? extends ParsedField> fieldParser) {
        this.fieldParser = fieldParser;
    }
    
    public void setBodyFactory(final BodyFactory bodyFactory) {
        this.bodyFactory = bodyFactory;
    }
    
    public void setMimeEntityConfig(final MimeConfig config) {
        this.config = config;
    }
    
    public void setBodyDescriptorBuilder(final BodyDescriptorBuilder bodyDescBuilder) {
        this.bodyDescBuilder = bodyDescBuilder;
    }
    
    public void setDecodeMonitor(final DecodeMonitor monitor) {
        this.monitor = monitor;
    }
    
    public void setContentDecoding(final boolean contentDecoding) {
        this.contentDecoding = contentDecoding;
    }
    
    public void setFlatMode(final boolean flatMode) {
        this.flatMode = flatMode;
    }
    
    public Header copy(final Header other) {
        final HeaderImpl copy = new HeaderImpl();
        for (final Field otherField : other.getFields()) {
            copy.addField(otherField);
        }
        return copy;
    }
    
    public BodyPart copy(final Entity other) {
        final BodyPart copy = new BodyPart();
        if (other.getHeader() != null) {
            copy.setHeader(this.copy(other.getHeader()));
        }
        if (other.getBody() != null) {
            copy.setBody(this.copy(other.getBody()));
        }
        return copy;
    }
    
    public Multipart copy(final Multipart other) {
        final MultipartImpl copy = new MultipartImpl(other.getSubType());
        for (final Entity otherBodyPart : other.getBodyParts()) {
            copy.addBodyPart(this.copy(otherBodyPart));
        }
        copy.setPreamble(other.getPreamble());
        copy.setEpilogue(other.getEpilogue());
        return copy;
    }
    
    public Body copy(final Body body) {
        if (body == null) {
            throw new IllegalArgumentException("Body is null");
        }
        if (body instanceof Message) {
            return this.copy((Message)body);
        }
        if (body instanceof Multipart) {
            return this.copy((Multipart)body);
        }
        if (body instanceof SingleBody) {
            return ((SingleBody)body).copy();
        }
        throw new IllegalArgumentException("Unsupported body class");
    }
    
    public Message copy(final Message other) {
        final MessageImpl copy = new MessageImpl();
        if (other.getHeader() != null) {
            copy.setHeader(this.copy(other.getHeader()));
        }
        if (other.getBody() != null) {
            copy.setBody(this.copy(other.getBody()));
        }
        return copy;
    }
    
    public Header newHeader() {
        return new HeaderImpl();
    }
    
    public Header newHeader(final Header source) {
        return this.copy(source);
    }
    
    public Multipart newMultipart(final String subType) {
        return new MultipartImpl(subType);
    }
    
    public Multipart newMultipart(final Multipart source) {
        return this.copy(source);
    }
    
    public Header parseHeader(final InputStream is) throws IOException, MimeIOException {
        final MimeConfig cfg = (this.config != null) ? this.config : new MimeConfig();
        final boolean strict = cfg.isStrictParsing();
        final DecodeMonitor mon = (this.monitor != null) ? this.monitor : (strict ? DecodeMonitor.STRICT : DecodeMonitor.SILENT);
        final FieldParser<? extends ParsedField> fp = (this.fieldParser != null) ? this.fieldParser : (strict ? DefaultFieldParser.getParser() : LenientFieldParser.getParser());
        final HeaderImpl header = new HeaderImpl();
        final MimeStreamParser parser = new MimeStreamParser();
        parser.setContentHandler(new AbstractContentHandler() {
            @Override
            public void endHeader() {
                parser.stop();
            }
            
            @Override
            public void field(final Field field) throws MimeException {
                ParsedField parsedField;
                if (field instanceof ParsedField) {
                    parsedField = (ParsedField)field;
                }
                else {
                    parsedField = fp.parse(field, mon);
                }
                header.addField(parsedField);
            }
        });
        try {
            parser.parse(is);
        }
        catch (MimeException ex) {
            throw new MimeIOException(ex);
        }
        return header;
    }
    
    public Message newMessage() {
        return new MessageImpl();
    }
    
    public Message newMessage(final Message source) {
        return this.copy(source);
    }
    
    public Message parseMessage(final InputStream is) throws IOException, MimeIOException {
        try {
            final MessageImpl message = new MessageImpl();
            final MimeConfig cfg = (this.config != null) ? this.config : new MimeConfig();
            final boolean strict = cfg.isStrictParsing();
            final DecodeMonitor mon = (this.monitor != null) ? this.monitor : (strict ? DecodeMonitor.STRICT : DecodeMonitor.SILENT);
            BodyDescriptorBuilder bodyDescBuilder;
            if (this.bodyDescBuilder != null) {
                bodyDescBuilder = this.bodyDescBuilder;
            }
            else {
                final DefaultBodyDescriptorBuilder defaultBodyDescriptorBuilder;
                bodyDescBuilder = defaultBodyDescriptorBuilder;
                defaultBodyDescriptorBuilder = new DefaultBodyDescriptorBuilder(null, (this.fieldParser != null) ? this.fieldParser : (strict ? DefaultFieldParser.getParser() : LenientFieldParser.getParser()), mon);
            }
            final BodyDescriptorBuilder bdb = bodyDescBuilder;
            final BodyFactory bf = (this.bodyFactory != null) ? this.bodyFactory : new BasicBodyFactory();
            final MimeStreamParser parser = new MimeStreamParser(cfg, mon, bdb);
            parser.setContentHandler(new EntityBuilder(message, bf));
            parser.setContentDecoding(this.contentDecoding);
            if (this.flatMode) {
                parser.setFlat();
            }
            else {
                parser.setRecurse();
            }
            parser.parse(is);
            return message;
        }
        catch (MimeException e) {
            throw new MimeIOException(e);
        }
    }
}
