// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.james.mime4j.message;

import org.apache.james.mime4j.dom.MessageWriter;
import org.apache.james.mime4j.dom.MessageBuilder;
import org.apache.james.mime4j.codec.DecodeMonitor;
import org.apache.james.mime4j.stream.BodyDescriptorBuilder;
import org.apache.james.mime4j.stream.MimeConfig;
import org.apache.james.mime4j.dom.MessageServiceFactory;

public class MessageServiceFactoryImpl extends MessageServiceFactory
{
    private BodyFactory bodyFactory;
    private MimeConfig mimeEntityConfig;
    private BodyDescriptorBuilder bodyDescriptorBuilder;
    private DecodeMonitor decodeMonitor;
    private Boolean flatMode;
    private Boolean contentDecoding;
    
    public MessageServiceFactoryImpl() {
        this.bodyFactory = null;
        this.mimeEntityConfig = null;
        this.bodyDescriptorBuilder = null;
        this.decodeMonitor = null;
        this.flatMode = null;
        this.contentDecoding = null;
    }
    
    @Override
    public MessageBuilder newMessageBuilder() {
        final DefaultMessageBuilder m = new DefaultMessageBuilder();
        if (this.bodyFactory != null) {
            m.setBodyFactory(this.bodyFactory);
        }
        if (this.mimeEntityConfig != null) {
            m.setMimeEntityConfig(this.mimeEntityConfig);
        }
        if (this.bodyDescriptorBuilder != null) {
            m.setBodyDescriptorBuilder(this.bodyDescriptorBuilder);
        }
        if (this.flatMode != null) {
            m.setFlatMode(this.flatMode);
        }
        if (this.contentDecoding != null) {
            m.setContentDecoding(this.contentDecoding);
        }
        if (this.decodeMonitor != null) {
            m.setDecodeMonitor(this.decodeMonitor);
        }
        return m;
    }
    
    @Override
    public MessageWriter newMessageWriter() {
        return new DefaultMessageWriter();
    }
    
    @Override
    public void setAttribute(final String name, final Object value) throws IllegalArgumentException {
        if ("BodyFactory".equals(name)) {
            if (value instanceof BodyFactory) {
                this.bodyFactory = (BodyFactory)value;
                return;
            }
            throw new IllegalArgumentException("Unsupported attribute value type for " + name + ", expected a BodyFactory");
        }
        else if ("MimeEntityConfig".equals(name)) {
            if (value instanceof MimeConfig) {
                this.mimeEntityConfig = (MimeConfig)value;
                return;
            }
            throw new IllegalArgumentException("Unsupported attribute value type for " + name + ", expected a MimeConfig");
        }
        else if ("MutableBodyDescriptorFactory".equals(name)) {
            if (value instanceof BodyDescriptorBuilder) {
                this.bodyDescriptorBuilder = (BodyDescriptorBuilder)value;
                return;
            }
            throw new IllegalArgumentException("Unsupported attribute value type for " + name + ", expected a MutableBodyDescriptorFactory");
        }
        else if ("DecodeMonitor".equals(name)) {
            if (value instanceof DecodeMonitor) {
                this.decodeMonitor = (DecodeMonitor)value;
                return;
            }
            throw new IllegalArgumentException("Unsupported attribute value type for " + name + ", expected a DecodeMonitor");
        }
        else if ("FlatMode".equals(name)) {
            if (value instanceof Boolean) {
                this.flatMode = (Boolean)value;
                return;
            }
            throw new IllegalArgumentException("Unsupported attribute value type for " + name + ", expected a Boolean");
        }
        else {
            if (!"ContentDecoding".equals(name)) {
                throw new IllegalArgumentException("Unsupported attribute: " + name);
            }
            if (value instanceof Boolean) {
                this.contentDecoding = (Boolean)value;
                return;
            }
            throw new IllegalArgumentException("Unsupported attribute value type for " + name + ", expected a Boolean");
        }
    }
}
