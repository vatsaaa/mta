// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.james.mime4j.message;

import org.apache.james.mime4j.util.CharsetUtil;
import java.nio.charset.UnsupportedCharsetException;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.io.ByteArrayOutputStream;
import org.apache.james.mime4j.dom.TextBody;
import java.io.IOException;
import org.apache.james.mime4j.dom.BinaryBody;
import java.io.InputStream;

public class BasicBodyFactory implements BodyFactory
{
    public BinaryBody binaryBody(final InputStream is) throws IOException {
        return new BasicBinaryBody(bufferContent(is));
    }
    
    public TextBody textBody(final InputStream is, final String mimeCharset) throws IOException {
        return new BasicTextBody(bufferContent(is), mimeCharset);
    }
    
    private static byte[] bufferContent(final InputStream is) throws IOException {
        if (is == null) {
            throw new IllegalArgumentException("Input stream may not be null");
        }
        final ByteArrayOutputStream buf = new ByteArrayOutputStream();
        final byte[] tmp = new byte[2048];
        int l;
        while ((l = is.read(tmp)) != -1) {
            buf.write(tmp, 0, l);
        }
        return buf.toByteArray();
    }
    
    public TextBody textBody(final String text, final String mimeCharset) throws UnsupportedEncodingException {
        if (text == null) {
            throw new IllegalArgumentException("Text may not be null");
        }
        final Charset charset = Charset.forName(mimeCharset);
        try {
            return new StringBody(text, charset);
        }
        catch (UnsupportedCharsetException ex) {
            throw new UnsupportedEncodingException(ex.getMessage());
        }
    }
    
    public TextBody textBody(final String text, final Charset charset) {
        if (text == null) {
            throw new IllegalArgumentException("Text may not be null");
        }
        return new StringBody(text, charset);
    }
    
    public TextBody textBody(final String text) {
        return this.textBody(text, CharsetUtil.DEFAULT_CHARSET);
    }
    
    public BinaryBody binaryBody(final byte[] buf) {
        return new BasicBinaryBody(buf);
    }
}
