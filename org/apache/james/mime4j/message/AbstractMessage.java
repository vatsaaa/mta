// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.james.mime4j.message;

import org.apache.james.mime4j.dom.field.AddressListField;
import java.util.Arrays;
import java.util.Collections;
import org.apache.james.mime4j.dom.field.MailboxListField;
import org.apache.james.mime4j.dom.field.MailboxField;
import org.apache.james.mime4j.dom.address.Address;
import org.apache.james.mime4j.dom.address.AddressList;
import java.util.Collection;
import org.apache.james.mime4j.dom.address.MailboxList;
import org.apache.james.mime4j.dom.address.Mailbox;
import java.util.TimeZone;
import org.apache.james.mime4j.dom.field.DateTimeField;
import java.util.Date;
import org.apache.james.mime4j.dom.field.UnstructuredField;
import org.apache.james.mime4j.dom.field.ParsedField;
import org.apache.james.mime4j.dom.Header;
import org.apache.james.mime4j.stream.Field;
import org.apache.james.mime4j.dom.Message;

public abstract class AbstractMessage extends AbstractEntity implements Message
{
    public String getMessageId() {
        final Field field = this.obtainField("Message-ID");
        if (field == null) {
            return null;
        }
        return field.getBody();
    }
    
    public void createMessageId(final String hostname) {
        final Header header = this.obtainHeader();
        header.setField(this.newMessageId(hostname));
    }
    
    protected abstract ParsedField newMessageId(final String p0);
    
    public String getSubject() {
        final UnstructuredField field = this.obtainField("Subject");
        if (field == null) {
            return null;
        }
        return field.getValue();
    }
    
    public void setSubject(final String subject) {
        final Header header = this.obtainHeader();
        if (subject == null) {
            header.removeFields("Subject");
        }
        else {
            header.setField(this.newSubject(subject));
        }
    }
    
    public Date getDate() {
        final DateTimeField dateField = this.obtainField("Date");
        if (dateField == null) {
            return null;
        }
        return dateField.getDate();
    }
    
    public void setDate(final Date date) {
        this.setDate(date, null);
    }
    
    public void setDate(final Date date, final TimeZone zone) {
        final Header header = this.obtainHeader();
        if (date == null) {
            header.removeFields("Date");
        }
        else {
            header.setField(this.newDate(date, zone));
        }
    }
    
    public Mailbox getSender() {
        return this.getMailbox("Sender");
    }
    
    public void setSender(final Mailbox sender) {
        this.setMailbox("Sender", sender);
    }
    
    public MailboxList getFrom() {
        return this.getMailboxList("From");
    }
    
    public void setFrom(final Mailbox from) {
        this.setMailboxList("From", from);
    }
    
    public void setFrom(final Mailbox... from) {
        this.setMailboxList("From", from);
    }
    
    public void setFrom(final Collection<Mailbox> from) {
        this.setMailboxList("From", from);
    }
    
    public AddressList getTo() {
        return this.getAddressList("To");
    }
    
    public void setTo(final Address to) {
        this.setAddressList("To", to);
    }
    
    public void setTo(final Address... to) {
        this.setAddressList("To", to);
    }
    
    public void setTo(final Collection<? extends Address> to) {
        this.setAddressList("To", to);
    }
    
    public AddressList getCc() {
        return this.getAddressList("Cc");
    }
    
    public void setCc(final Address cc) {
        this.setAddressList("Cc", cc);
    }
    
    public void setCc(final Address... cc) {
        this.setAddressList("Cc", cc);
    }
    
    public void setCc(final Collection<? extends Address> cc) {
        this.setAddressList("Cc", cc);
    }
    
    public AddressList getBcc() {
        return this.getAddressList("Bcc");
    }
    
    public void setBcc(final Address bcc) {
        this.setAddressList("Bcc", bcc);
    }
    
    public void setBcc(final Address... bcc) {
        this.setAddressList("Bcc", bcc);
    }
    
    public void setBcc(final Collection<? extends Address> bcc) {
        this.setAddressList("Bcc", bcc);
    }
    
    public AddressList getReplyTo() {
        return this.getAddressList("Reply-To");
    }
    
    public void setReplyTo(final Address replyTo) {
        this.setAddressList("Reply-To", replyTo);
    }
    
    public void setReplyTo(final Address... replyTo) {
        this.setAddressList("Reply-To", replyTo);
    }
    
    public void setReplyTo(final Collection<? extends Address> replyTo) {
        this.setAddressList("Reply-To", replyTo);
    }
    
    private Mailbox getMailbox(final String fieldName) {
        final MailboxField field = this.obtainField(fieldName);
        if (field == null) {
            return null;
        }
        return field.getMailbox();
    }
    
    private void setMailbox(final String fieldName, final Mailbox mailbox) {
        final Header header = this.obtainHeader();
        if (mailbox == null) {
            header.removeFields(fieldName);
        }
        else {
            header.setField(this.newMailbox(fieldName, mailbox));
        }
    }
    
    private MailboxList getMailboxList(final String fieldName) {
        final MailboxListField field = this.obtainField(fieldName);
        if (field == null) {
            return null;
        }
        return field.getMailboxList();
    }
    
    private void setMailboxList(final String fieldName, final Mailbox mailbox) {
        this.setMailboxList(fieldName, (mailbox == null) ? null : Collections.singleton(mailbox));
    }
    
    private void setMailboxList(final String fieldName, final Mailbox... mailboxes) {
        this.setMailboxList(fieldName, (mailboxes == null) ? null : Arrays.asList(mailboxes));
    }
    
    private void setMailboxList(final String fieldName, final Collection<Mailbox> mailboxes) {
        final Header header = this.obtainHeader();
        if (mailboxes == null || mailboxes.isEmpty()) {
            header.removeFields(fieldName);
        }
        else {
            header.setField(this.newMailboxList(fieldName, mailboxes));
        }
    }
    
    private AddressList getAddressList(final String fieldName) {
        final AddressListField field = this.obtainField(fieldName);
        if (field == null) {
            return null;
        }
        return field.getAddressList();
    }
    
    private void setAddressList(final String fieldName, final Address address) {
        this.setAddressList(fieldName, (address == null) ? null : Collections.singleton(address));
    }
    
    private void setAddressList(final String fieldName, final Address... addresses) {
        this.setAddressList(fieldName, (addresses == null) ? null : Arrays.asList(addresses));
    }
    
    private void setAddressList(final String fieldName, final Collection<? extends Address> addresses) {
        final Header header = this.obtainHeader();
        if (addresses == null || addresses.isEmpty()) {
            header.removeFields(fieldName);
        }
        else {
            header.setField(this.newAddressList(fieldName, addresses));
        }
    }
    
    protected abstract AddressListField newAddressList(final String p0, final Collection<? extends Address> p1);
    
    protected abstract UnstructuredField newSubject(final String p0);
    
    protected abstract DateTimeField newDate(final Date p0, final TimeZone p1);
    
    protected abstract MailboxField newMailbox(final String p0, final Mailbox p1);
    
    protected abstract MailboxListField newMailboxList(final String p0, final Collection<Mailbox> p1);
}
