// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.james.mime4j.message;

import org.apache.james.mime4j.dom.TextBody;
import java.io.IOException;
import org.apache.james.mime4j.dom.BinaryBody;
import java.io.InputStream;

public interface BodyFactory
{
    BinaryBody binaryBody(final InputStream p0) throws IOException;
    
    TextBody textBody(final InputStream p0, final String p1) throws IOException;
}
