// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.james.mime4j.message;

import org.apache.james.mime4j.util.ByteArrayBuffer;
import org.apache.james.mime4j.util.ByteSequence;
import java.io.IOException;
import java.io.InputStream;
import org.apache.james.mime4j.dom.Multipart;
import org.apache.james.mime4j.stream.BodyDescriptor;
import org.apache.james.mime4j.dom.Header;
import org.apache.james.mime4j.stream.Field;
import org.apache.james.mime4j.MimeException;
import org.apache.james.mime4j.dom.Message;
import org.apache.james.mime4j.dom.Body;
import java.util.Stack;
import org.apache.james.mime4j.dom.Entity;
import org.apache.james.mime4j.parser.ContentHandler;

class EntityBuilder implements ContentHandler
{
    private final Entity entity;
    private final BodyFactory bodyFactory;
    private final Stack<Object> stack;
    
    EntityBuilder(final Entity entity, final BodyFactory bodyFactory) {
        this.entity = entity;
        this.bodyFactory = bodyFactory;
        this.stack = new Stack<Object>();
    }
    
    private void expect(final Class<?> c) {
        if (!c.isInstance(this.stack.peek())) {
            throw new IllegalStateException("Internal stack error: Expected '" + c.getName() + "' found '" + this.stack.peek().getClass().getName() + "'");
        }
    }
    
    public void startMessage() throws MimeException {
        if (this.stack.isEmpty()) {
            this.stack.push(this.entity);
        }
        else {
            this.expect(Entity.class);
            final Message m = new MessageImpl();
            this.stack.peek().setBody(m);
            this.stack.push(m);
        }
    }
    
    public void endMessage() throws MimeException {
        this.expect(Message.class);
        this.stack.pop();
    }
    
    public void startHeader() throws MimeException {
        this.stack.push(new HeaderImpl());
    }
    
    public void field(final Field field) throws MimeException {
        this.expect(Header.class);
        this.stack.peek().addField(field);
    }
    
    public void endHeader() throws MimeException {
        this.expect(Header.class);
        final Header h = this.stack.pop();
        this.expect(Entity.class);
        this.stack.peek().setHeader(h);
    }
    
    public void startMultipart(final BodyDescriptor bd) throws MimeException {
        this.expect(Entity.class);
        final Entity e = this.stack.peek();
        final String subType = bd.getSubType();
        final Multipart multiPart = new MultipartImpl(subType);
        e.setBody(multiPart);
        this.stack.push(multiPart);
    }
    
    public void body(final BodyDescriptor bd, final InputStream is) throws MimeException, IOException {
        this.expect(Entity.class);
        Body body;
        if (bd.getMimeType().startsWith("text/")) {
            body = this.bodyFactory.textBody(is, bd.getCharset());
        }
        else {
            body = this.bodyFactory.binaryBody(is);
        }
        final Entity entity = this.stack.peek();
        entity.setBody(body);
    }
    
    public void endMultipart() throws MimeException {
        this.stack.pop();
    }
    
    public void startBodyPart() throws MimeException {
        this.expect(Multipart.class);
        final BodyPart bodyPart = new BodyPart();
        this.stack.peek().addBodyPart(bodyPart);
        this.stack.push(bodyPart);
    }
    
    public void endBodyPart() throws MimeException {
        this.expect(BodyPart.class);
        this.stack.pop();
    }
    
    public void epilogue(final InputStream is) throws MimeException, IOException {
        this.expect(MultipartImpl.class);
        final ByteSequence bytes = loadStream(is);
        this.stack.peek().setEpilogueRaw(bytes);
    }
    
    public void preamble(final InputStream is) throws MimeException, IOException {
        this.expect(MultipartImpl.class);
        final ByteSequence bytes = loadStream(is);
        this.stack.peek().setPreambleRaw(bytes);
    }
    
    public void raw(final InputStream is) throws MimeException, IOException {
        throw new UnsupportedOperationException("Not supported");
    }
    
    private static ByteSequence loadStream(final InputStream in) throws IOException {
        final ByteArrayBuffer bab = new ByteArrayBuffer(64);
        int b;
        while ((b = in.read()) != -1) {
            bab.append(b);
        }
        return bab;
    }
}
