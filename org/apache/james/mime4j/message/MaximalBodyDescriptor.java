// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.james.mime4j.message;

import java.util.Locale;
import org.apache.james.mime4j.dom.field.ContentMD5Field;
import org.apache.james.mime4j.dom.field.ContentLocationField;
import org.apache.james.mime4j.dom.field.ContentLanguageField;
import java.util.List;
import java.util.Date;
import org.apache.james.mime4j.dom.field.ContentDispositionField;
import org.apache.james.mime4j.dom.field.ContentIdField;
import org.apache.james.mime4j.dom.field.ContentDescriptionField;
import org.apache.james.mime4j.dom.field.MimeVersionField;
import org.apache.james.mime4j.dom.field.ContentLengthField;
import org.apache.james.mime4j.dom.field.ContentTransferEncodingField;
import org.apache.james.mime4j.dom.field.ContentTypeField;
import java.util.Collections;
import java.util.HashMap;
import org.apache.james.mime4j.dom.field.ParsedField;
import java.util.Map;
import org.apache.james.mime4j.stream.BodyDescriptor;

public class MaximalBodyDescriptor implements BodyDescriptor
{
    private static final String CONTENT_TYPE;
    private static final String CONTENT_LENGTH;
    private static final String CONTENT_TRANSFER_ENCODING;
    private static final String CONTENT_DISPOSITION;
    private static final String CONTENT_ID;
    private static final String CONTENT_MD5;
    private static final String CONTENT_DESCRIPTION;
    private static final String CONTENT_LANGUAGE;
    private static final String CONTENT_LOCATION;
    private static final String MIME_VERSION;
    private final String mediaType;
    private final String subType;
    private final String mimeType;
    private final String boundary;
    private final String charset;
    private final Map<String, ParsedField> fields;
    
    MaximalBodyDescriptor(final String mimeType, final String mediaType, final String subType, final String boundary, final String charset, final Map<String, ParsedField> fields) {
        this.mimeType = mimeType;
        this.mediaType = mediaType;
        this.subType = subType;
        this.boundary = boundary;
        this.charset = charset;
        this.fields = ((fields != null) ? new HashMap<String, ParsedField>(fields) : Collections.emptyMap());
    }
    
    public String getMimeType() {
        return this.mimeType;
    }
    
    public String getBoundary() {
        return this.boundary;
    }
    
    public String getCharset() {
        return this.charset;
    }
    
    public String getMediaType() {
        return this.mediaType;
    }
    
    public String getSubType() {
        return this.subType;
    }
    
    public Map<String, String> getContentTypeParameters() {
        final ContentTypeField contentTypeField = this.fields.get(MaximalBodyDescriptor.CONTENT_TYPE);
        return (contentTypeField != null) ? contentTypeField.getParameters() : Collections.emptyMap();
    }
    
    public String getTransferEncoding() {
        final ContentTransferEncodingField contentTransferEncodingField = this.fields.get(MaximalBodyDescriptor.CONTENT_TRANSFER_ENCODING);
        return (contentTransferEncodingField != null) ? contentTransferEncodingField.getEncoding() : "7bit";
    }
    
    public long getContentLength() {
        final ContentLengthField contentLengthField = this.fields.get(MaximalBodyDescriptor.CONTENT_LENGTH);
        return (contentLengthField != null) ? contentLengthField.getContentLength() : -1L;
    }
    
    public int getMimeMajorVersion() {
        final MimeVersionField mimeVersionField = this.fields.get(MaximalBodyDescriptor.MIME_VERSION);
        return (mimeVersionField != null) ? mimeVersionField.getMajorVersion() : 1;
    }
    
    public int getMimeMinorVersion() {
        final MimeVersionField mimeVersionField = this.fields.get(MaximalBodyDescriptor.MIME_VERSION);
        return (mimeVersionField != null) ? mimeVersionField.getMinorVersion() : 0;
    }
    
    public String getContentDescription() {
        final ContentDescriptionField contentDescriptionField = this.fields.get(MaximalBodyDescriptor.CONTENT_DESCRIPTION);
        return (contentDescriptionField != null) ? contentDescriptionField.getDescription() : null;
    }
    
    public String getContentId() {
        final ContentIdField contentIdField = this.fields.get(MaximalBodyDescriptor.CONTENT_ID);
        return (contentIdField != null) ? contentIdField.getId() : null;
    }
    
    public String getContentDispositionType() {
        final ContentDispositionField contentDispositionField = this.fields.get(MaximalBodyDescriptor.CONTENT_DISPOSITION);
        return (contentDispositionField != null) ? contentDispositionField.getDispositionType() : null;
    }
    
    public Map<String, String> getContentDispositionParameters() {
        final ContentDispositionField contentDispositionField = this.fields.get(MaximalBodyDescriptor.CONTENT_DISPOSITION);
        return (contentDispositionField != null) ? contentDispositionField.getParameters() : Collections.emptyMap();
    }
    
    public String getContentDispositionFilename() {
        final ContentDispositionField contentDispositionField = this.fields.get(MaximalBodyDescriptor.CONTENT_DISPOSITION);
        return (contentDispositionField != null) ? contentDispositionField.getFilename() : null;
    }
    
    public Date getContentDispositionModificationDate() {
        final ContentDispositionField contentDispositionField = this.fields.get(MaximalBodyDescriptor.CONTENT_DISPOSITION);
        return (contentDispositionField != null) ? contentDispositionField.getModificationDate() : null;
    }
    
    public Date getContentDispositionCreationDate() {
        final ContentDispositionField contentDispositionField = this.fields.get(MaximalBodyDescriptor.CONTENT_DISPOSITION);
        return (contentDispositionField != null) ? contentDispositionField.getCreationDate() : null;
    }
    
    public Date getContentDispositionReadDate() {
        final ContentDispositionField contentDispositionField = this.fields.get(MaximalBodyDescriptor.CONTENT_DISPOSITION);
        return (contentDispositionField != null) ? contentDispositionField.getReadDate() : null;
    }
    
    public long getContentDispositionSize() {
        final ContentDispositionField contentDispositionField = this.fields.get(MaximalBodyDescriptor.CONTENT_DISPOSITION);
        return (contentDispositionField != null) ? contentDispositionField.getSize() : -1L;
    }
    
    public List<String> getContentLanguage() {
        final ContentLanguageField contentLanguageField = this.fields.get(MaximalBodyDescriptor.CONTENT_LANGUAGE);
        return (contentLanguageField != null) ? contentLanguageField.getLanguages() : Collections.emptyList();
    }
    
    public String getContentLocation() {
        final ContentLocationField contentLocationField = this.fields.get(MaximalBodyDescriptor.CONTENT_LOCATION);
        return (contentLocationField != null) ? contentLocationField.getLocation() : null;
    }
    
    public String getContentMD5Raw() {
        final ContentMD5Field contentMD5Field = this.fields.get(MaximalBodyDescriptor.CONTENT_MD5);
        return (contentMD5Field != null) ? contentMD5Field.getMD5Raw() : null;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("[mimeType=");
        sb.append(this.mimeType);
        sb.append(", mediaType=");
        sb.append(this.mediaType);
        sb.append(", subType=");
        sb.append(this.subType);
        sb.append(", boundary=");
        sb.append(this.boundary);
        sb.append(", charset=");
        sb.append(this.charset);
        sb.append("]");
        return sb.toString();
    }
    
    static {
        CONTENT_TYPE = "Content-Type".toLowerCase(Locale.US);
        CONTENT_LENGTH = "Content-Length".toLowerCase(Locale.US);
        CONTENT_TRANSFER_ENCODING = "Content-Transfer-Encoding".toLowerCase(Locale.US);
        CONTENT_DISPOSITION = "Content-Disposition".toLowerCase(Locale.US);
        CONTENT_ID = "Content-ID".toLowerCase(Locale.US);
        CONTENT_MD5 = "Content-MD5".toLowerCase(Locale.US);
        CONTENT_DESCRIPTION = "Content-Description".toLowerCase(Locale.US);
        CONTENT_LANGUAGE = "Content-Language".toLowerCase(Locale.US);
        CONTENT_LOCATION = "Content-Location".toLowerCase(Locale.US);
        MIME_VERSION = "MIME-Version".toLowerCase(Locale.US);
    }
}
