// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.james.mime4j.message;

import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import org.apache.james.mime4j.dom.Entity;
import java.util.List;
import org.apache.james.mime4j.dom.Multipart;

public abstract class AbstractMultipart implements Multipart
{
    protected List<Entity> bodyParts;
    private Entity parent;
    private String subType;
    
    public AbstractMultipart(final String subType) {
        this.bodyParts = new LinkedList<Entity>();
        this.parent = null;
        this.subType = subType;
    }
    
    public String getSubType() {
        return this.subType;
    }
    
    public void setSubType(final String subType) {
        this.subType = subType;
    }
    
    public Entity getParent() {
        return this.parent;
    }
    
    public void setParent(final Entity parent) {
        this.parent = parent;
        for (final Entity bodyPart : this.bodyParts) {
            bodyPart.setParent(parent);
        }
    }
    
    public int getCount() {
        return this.bodyParts.size();
    }
    
    public List<Entity> getBodyParts() {
        return Collections.unmodifiableList((List<? extends Entity>)this.bodyParts);
    }
    
    public void setBodyParts(final List<Entity> bodyParts) {
        this.bodyParts = bodyParts;
        for (final Entity bodyPart : bodyParts) {
            bodyPart.setParent(this.parent);
        }
    }
    
    public void addBodyPart(final Entity bodyPart) {
        if (bodyPart == null) {
            throw new IllegalArgumentException();
        }
        this.bodyParts.add(bodyPart);
        bodyPart.setParent(this.parent);
    }
    
    public void addBodyPart(final Entity bodyPart, final int index) {
        if (bodyPart == null) {
            throw new IllegalArgumentException();
        }
        this.bodyParts.add(index, bodyPart);
        bodyPart.setParent(this.parent);
    }
    
    public Entity removeBodyPart(final int index) {
        final Entity bodyPart = this.bodyParts.remove(index);
        bodyPart.setParent(null);
        return bodyPart;
    }
    
    public Entity replaceBodyPart(final Entity bodyPart, final int index) {
        if (bodyPart == null) {
            throw new IllegalArgumentException();
        }
        final Entity replacedEntity = this.bodyParts.set(index, bodyPart);
        if (bodyPart == replacedEntity) {
            throw new IllegalArgumentException("Cannot replace body part with itself");
        }
        bodyPart.setParent(this.parent);
        replacedEntity.setParent(null);
        return replacedEntity;
    }
    
    public abstract String getPreamble();
    
    public abstract void setPreamble(final String p0);
    
    public abstract String getEpilogue();
    
    public abstract void setEpilogue(final String p0);
    
    public void dispose() {
        for (final Entity bodyPart : this.bodyParts) {
            bodyPart.dispose();
        }
    }
}
