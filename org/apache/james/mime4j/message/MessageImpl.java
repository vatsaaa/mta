// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.james.mime4j.message;

import org.apache.james.mime4j.dom.field.ParsedField;
import org.apache.james.mime4j.field.ContentTypeFieldImpl;
import org.apache.james.mime4j.field.ContentTransferEncodingFieldImpl;
import org.apache.james.mime4j.dom.field.ContentTransferEncodingField;
import org.apache.james.mime4j.dom.field.ContentTypeField;
import java.util.Map;
import org.apache.james.mime4j.dom.field.ContentDispositionField;
import org.apache.james.mime4j.dom.field.AddressListField;
import org.apache.james.mime4j.dom.address.Address;
import org.apache.james.mime4j.dom.field.MailboxListField;
import java.util.Collection;
import org.apache.james.mime4j.dom.field.MailboxField;
import org.apache.james.mime4j.dom.address.Mailbox;
import org.apache.james.mime4j.dom.field.DateTimeField;
import java.util.TimeZone;
import java.util.Date;
import org.apache.james.mime4j.field.Fields;
import org.apache.james.mime4j.dom.field.UnstructuredField;
import org.apache.james.mime4j.util.MimeUtil;
import org.apache.james.mime4j.dom.Header;
import org.apache.james.mime4j.stream.Field;
import org.apache.james.mime4j.codec.DecodeMonitor;
import org.apache.james.mime4j.field.MimeVersionFieldLenientImpl;
import org.apache.james.mime4j.stream.RawField;

public class MessageImpl extends AbstractMessage
{
    public MessageImpl() {
        final Header header = this.obtainHeader();
        final RawField rawField = new RawField("MIME-Version", "1.0");
        header.addField(MimeVersionFieldLenientImpl.PARSER.parse(rawField, DecodeMonitor.SILENT));
    }
    
    @Override
    protected String newUniqueBoundary() {
        return MimeUtil.createUniqueBoundary();
    }
    
    @Override
    protected UnstructuredField newMessageId(final String hostname) {
        return Fields.messageId(hostname);
    }
    
    @Override
    protected DateTimeField newDate(final Date date, final TimeZone zone) {
        return Fields.date("Date", date, zone);
    }
    
    @Override
    protected MailboxField newMailbox(final String fieldName, final Mailbox mailbox) {
        return Fields.mailbox(fieldName, mailbox);
    }
    
    @Override
    protected MailboxListField newMailboxList(final String fieldName, final Collection<Mailbox> mailboxes) {
        return Fields.mailboxList(fieldName, mailboxes);
    }
    
    @Override
    protected AddressListField newAddressList(final String fieldName, final Collection<? extends Address> addresses) {
        return Fields.addressList(fieldName, addresses);
    }
    
    @Override
    protected UnstructuredField newSubject(final String subject) {
        return Fields.subject(subject);
    }
    
    @Override
    protected ContentDispositionField newContentDisposition(final String dispositionType, final String filename, final long size, final Date creationDate, final Date modificationDate, final Date readDate) {
        return Fields.contentDisposition(dispositionType, filename, size, creationDate, modificationDate, readDate);
    }
    
    @Override
    protected ContentDispositionField newContentDisposition(final String dispositionType, final Map<String, String> parameters) {
        return Fields.contentDisposition(dispositionType, parameters);
    }
    
    @Override
    protected ContentTypeField newContentType(final String mimeType, final Map<String, String> parameters) {
        return Fields.contentType(mimeType, parameters);
    }
    
    @Override
    protected ContentTransferEncodingField newContentTransferEncoding(final String contentTransferEncoding) {
        return Fields.contentTransferEncoding(contentTransferEncoding);
    }
    
    @Override
    protected String calcTransferEncoding(final ContentTransferEncodingField f) {
        return ContentTransferEncodingFieldImpl.getEncoding(f);
    }
    
    @Override
    protected String calcMimeType(final ContentTypeField child, final ContentTypeField parent) {
        return ContentTypeFieldImpl.getMimeType(child, parent);
    }
    
    @Override
    protected String calcCharset(final ContentTypeField contentType) {
        return ContentTypeFieldImpl.getCharset(contentType);
    }
}
