// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.james.mime4j.message;

import org.apache.james.mime4j.dom.SingleBody;
import java.io.IOException;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import org.apache.james.mime4j.dom.BinaryBody;

class BasicBinaryBody extends BinaryBody
{
    private final byte[] content;
    
    BasicBinaryBody(final byte[] content) {
        this.content = content;
    }
    
    @Override
    public InputStream getInputStream() throws IOException {
        return new ByteArrayInputStream(this.content);
    }
    
    @Override
    public BasicBinaryBody copy() {
        return new BasicBinaryBody(this.content);
    }
}
