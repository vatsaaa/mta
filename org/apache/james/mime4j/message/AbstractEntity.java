// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.james.mime4j.message;

import org.apache.james.mime4j.dom.field.ParsedField;
import java.util.Date;
import org.apache.james.mime4j.dom.field.ContentDispositionField;
import org.apache.james.mime4j.dom.field.ContentTransferEncodingField;
import org.apache.james.mime4j.dom.field.ContentTypeField;
import org.apache.james.mime4j.stream.Field;
import org.apache.james.mime4j.dom.TextBody;
import java.util.HashMap;
import java.util.Collections;
import org.apache.james.mime4j.dom.Multipart;
import java.util.Map;
import org.apache.james.mime4j.dom.Message;
import org.apache.james.mime4j.dom.Body;
import org.apache.james.mime4j.dom.Header;
import org.apache.james.mime4j.dom.Entity;

public abstract class AbstractEntity implements Entity
{
    private Header header;
    private Body body;
    private Entity parent;
    
    protected AbstractEntity() {
        this.header = null;
        this.body = null;
        this.parent = null;
    }
    
    public Entity getParent() {
        return this.parent;
    }
    
    public void setParent(final Entity parent) {
        this.parent = parent;
    }
    
    public Header getHeader() {
        return this.header;
    }
    
    public void setHeader(final Header header) {
        this.header = header;
    }
    
    public Body getBody() {
        return this.body;
    }
    
    public void setBody(final Body body) {
        if (this.body != null) {
            throw new IllegalStateException("body already set");
        }
        (this.body = body).setParent(this);
    }
    
    public Body removeBody() {
        if (this.body == null) {
            return null;
        }
        final Body body = this.body;
        this.body = null;
        body.setParent(null);
        return body;
    }
    
    public void setMessage(final Message message) {
        this.setBody(message, "message/rfc822", null);
    }
    
    public void setMultipart(final Multipart multipart) {
        final String mimeType = "multipart/" + multipart.getSubType();
        final Map<String, String> parameters = Collections.singletonMap("boundary", this.newUniqueBoundary());
        this.setBody(multipart, mimeType, parameters);
    }
    
    public void setMultipart(final Multipart multipart, Map<String, String> parameters) {
        final String mimeType = "multipart/" + multipart.getSubType();
        if (!parameters.containsKey("boundary")) {
            parameters = new HashMap<String, String>(parameters);
            parameters.put("boundary", this.newUniqueBoundary());
        }
        this.setBody(multipart, mimeType, parameters);
    }
    
    public void setText(final TextBody textBody) {
        this.setText(textBody, "plain");
    }
    
    public void setText(final TextBody textBody, final String subtype) {
        final String mimeType = "text/" + subtype;
        Map<String, String> parameters = null;
        final String mimeCharset = textBody.getMimeCharset();
        if (mimeCharset != null && !mimeCharset.equalsIgnoreCase("us-ascii")) {
            parameters = Collections.singletonMap("charset", mimeCharset);
        }
        this.setBody(textBody, mimeType, parameters);
    }
    
    public void setBody(final Body body, final String mimeType) {
        this.setBody(body, mimeType, null);
    }
    
    public void setBody(final Body body, final String mimeType, final Map<String, String> parameters) {
        this.setBody(body);
        final Header header = this.obtainHeader();
        header.setField(this.newContentType(mimeType, parameters));
    }
    
    public String getMimeType() {
        final ContentTypeField child = this.getContentTypeField();
        final ContentTypeField parent = (this.getParent() != null) ? ((ContentTypeField)this.getParent().getHeader().getField("Content-Type")) : null;
        return this.calcMimeType(child, parent);
    }
    
    private ContentTypeField getContentTypeField() {
        return (ContentTypeField)this.getHeader().getField("Content-Type");
    }
    
    public String getCharset() {
        return this.calcCharset((ContentTypeField)this.getHeader().getField("Content-Type"));
    }
    
    public String getContentTransferEncoding() {
        final ContentTransferEncodingField f = (ContentTransferEncodingField)this.getHeader().getField("Content-Transfer-Encoding");
        return this.calcTransferEncoding(f);
    }
    
    public void setContentTransferEncoding(final String contentTransferEncoding) {
        final Header header = this.obtainHeader();
        header.setField(this.newContentTransferEncoding(contentTransferEncoding));
    }
    
    public String getDispositionType() {
        final ContentDispositionField field = this.obtainField("Content-Disposition");
        if (field == null) {
            return null;
        }
        return field.getDispositionType();
    }
    
    public void setContentDisposition(final String dispositionType) {
        final Header header = this.obtainHeader();
        header.setField(this.newContentDisposition(dispositionType, null, -1L, null, null, null));
    }
    
    public void setContentDisposition(final String dispositionType, final String filename) {
        final Header header = this.obtainHeader();
        header.setField(this.newContentDisposition(dispositionType, filename, -1L, null, null, null));
    }
    
    public void setContentDisposition(final String dispositionType, final String filename, final long size) {
        final Header header = this.obtainHeader();
        header.setField(this.newContentDisposition(dispositionType, filename, size, null, null, null));
    }
    
    public void setContentDisposition(final String dispositionType, final String filename, final long size, final Date creationDate, final Date modificationDate, final Date readDate) {
        final Header header = this.obtainHeader();
        header.setField(this.newContentDisposition(dispositionType, filename, size, creationDate, modificationDate, readDate));
    }
    
    public String getFilename() {
        final ContentDispositionField field = this.obtainField("Content-Disposition");
        if (field == null) {
            return null;
        }
        return field.getFilename();
    }
    
    public void setFilename(final String filename) {
        final Header header = this.obtainHeader();
        final ContentDispositionField field = (ContentDispositionField)header.getField("Content-Disposition");
        if (field == null) {
            if (filename != null) {
                header.setField(this.newContentDisposition("attachment", filename, -1L, null, null, null));
            }
        }
        else {
            final String dispositionType = field.getDispositionType();
            final Map<String, String> parameters = new HashMap<String, String>(field.getParameters());
            if (filename == null) {
                parameters.remove("filename");
            }
            else {
                parameters.put("filename", filename);
            }
            header.setField(this.newContentDisposition(dispositionType, parameters));
        }
    }
    
    public boolean isMimeType(final String type) {
        return this.getMimeType().equalsIgnoreCase(type);
    }
    
    public boolean isMultipart() {
        final ContentTypeField f = this.getContentTypeField();
        return f != null && f.getBoundary() != null && this.getMimeType().startsWith("multipart/");
    }
    
    public void dispose() {
        if (this.body != null) {
            this.body.dispose();
        }
    }
    
    Header obtainHeader() {
        if (this.header == null) {
            this.header = new HeaderImpl();
        }
        return this.header;
    }
    
     <F extends ParsedField> F obtainField(final String fieldName) {
        final Header header = this.getHeader();
        if (header == null) {
            return null;
        }
        final F field = (F)header.getField(fieldName);
        return field;
    }
    
    protected abstract String newUniqueBoundary();
    
    protected abstract ContentDispositionField newContentDisposition(final String p0, final String p1, final long p2, final Date p3, final Date p4, final Date p5);
    
    protected abstract ContentDispositionField newContentDisposition(final String p0, final Map<String, String> p1);
    
    protected abstract ContentTypeField newContentType(final String p0, final Map<String, String> p1);
    
    protected abstract ContentTransferEncodingField newContentTransferEncoding(final String p0);
    
    protected abstract String calcMimeType(final ContentTypeField p0, final ContentTypeField p1);
    
    protected abstract String calcTransferEncoding(final ContentTransferEncodingField p0);
    
    protected abstract String calcCharset(final ContentTypeField p0);
}
