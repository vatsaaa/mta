// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.james.mime4j.message;

import org.apache.james.mime4j.field.ContentTypeFieldImpl;
import org.apache.james.mime4j.field.ContentTransferEncodingFieldImpl;
import org.apache.james.mime4j.dom.field.ContentTransferEncodingField;
import org.apache.james.mime4j.dom.field.ContentTypeField;
import java.util.Map;
import org.apache.james.mime4j.field.Fields;
import org.apache.james.mime4j.dom.field.ContentDispositionField;
import java.util.Date;
import org.apache.james.mime4j.util.MimeUtil;

public class BodyPart extends AbstractEntity
{
    @Override
    protected String newUniqueBoundary() {
        return MimeUtil.createUniqueBoundary();
    }
    
    @Override
    protected ContentDispositionField newContentDisposition(final String dispositionType, final String filename, final long size, final Date creationDate, final Date modificationDate, final Date readDate) {
        return Fields.contentDisposition(dispositionType, filename, size, creationDate, modificationDate, readDate);
    }
    
    @Override
    protected ContentDispositionField newContentDisposition(final String dispositionType, final Map<String, String> parameters) {
        return Fields.contentDisposition(dispositionType, parameters);
    }
    
    @Override
    protected ContentTypeField newContentType(final String mimeType, final Map<String, String> parameters) {
        return Fields.contentType(mimeType, parameters);
    }
    
    @Override
    protected ContentTransferEncodingField newContentTransferEncoding(final String contentTransferEncoding) {
        return Fields.contentTransferEncoding(contentTransferEncoding);
    }
    
    @Override
    protected String calcTransferEncoding(final ContentTransferEncodingField f) {
        return ContentTransferEncodingFieldImpl.getEncoding(f);
    }
    
    @Override
    protected String calcMimeType(final ContentTypeField child, final ContentTypeField parent) {
        return ContentTypeFieldImpl.getMimeType(child, parent);
    }
    
    @Override
    protected String calcCharset(final ContentTypeField contentType) {
        return ContentTypeFieldImpl.getCharset(contentType);
    }
}
