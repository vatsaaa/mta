// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.james.mime4j.message;

import org.apache.james.mime4j.MimeException;
import org.apache.james.mime4j.stream.Field;
import org.apache.james.mime4j.field.LenientFieldParser;
import org.apache.james.mime4j.dom.Header;
import org.apache.james.mime4j.codec.DecodeMonitor;
import org.apache.james.mime4j.dom.field.ParsedField;
import org.apache.james.mime4j.dom.FieldParser;
import org.apache.james.mime4j.parser.AbstractContentHandler;

public abstract class SimpleContentHandler extends AbstractContentHandler
{
    private final FieldParser<? extends ParsedField> fieldParser;
    private final DecodeMonitor monitor;
    private Header currHeader;
    
    public SimpleContentHandler(final FieldParser<? extends ParsedField> fieldParser, final DecodeMonitor monitor) {
        this.fieldParser = ((fieldParser != null) ? fieldParser : LenientFieldParser.getParser());
        this.monitor = ((monitor != null) ? monitor : DecodeMonitor.SILENT);
    }
    
    public SimpleContentHandler() {
        this(null, null);
    }
    
    public abstract void headers(final Header p0);
    
    @Override
    public final void startHeader() {
        this.currHeader = new HeaderImpl();
    }
    
    @Override
    public final void field(final Field field) throws MimeException {
        ParsedField parsedField;
        if (field instanceof ParsedField) {
            parsedField = (ParsedField)field;
        }
        else {
            parsedField = (ParsedField)this.fieldParser.parse(field, this.monitor);
        }
        this.currHeader.addField(parsedField);
    }
    
    @Override
    public final void endHeader() {
        final Header tmp = this.currHeader;
        this.currHeader = null;
        this.headers(tmp);
    }
}
