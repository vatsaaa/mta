// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.james.mime4j.message;

import org.apache.james.mime4j.util.ContentUtil;
import org.apache.james.mime4j.util.ByteSequence;

public class MultipartImpl extends AbstractMultipart
{
    private ByteSequence preamble;
    private transient String preambleStrCache;
    private transient boolean preambleComputed;
    private ByteSequence epilogue;
    private transient String epilogueStrCache;
    private transient boolean epilogueComputed;
    
    public MultipartImpl(final String subType) {
        super(subType);
        this.preambleComputed = false;
        this.epilogueComputed = false;
        this.preamble = null;
        this.preambleStrCache = null;
        this.preambleComputed = true;
        this.epilogue = null;
        this.epilogueStrCache = null;
        this.epilogueComputed = true;
    }
    
    public ByteSequence getPreambleRaw() {
        return this.preamble;
    }
    
    public void setPreambleRaw(final ByteSequence preamble) {
        this.preamble = preamble;
        this.preambleStrCache = null;
        this.preambleComputed = false;
    }
    
    @Override
    public String getPreamble() {
        if (!this.preambleComputed) {
            this.preambleStrCache = ((this.preamble != null) ? ContentUtil.decode(this.preamble) : null);
            this.preambleComputed = true;
        }
        return this.preambleStrCache;
    }
    
    @Override
    public void setPreamble(final String preamble) {
        this.preamble = ((preamble != null) ? ContentUtil.encode(preamble) : null);
        this.preambleStrCache = preamble;
        this.preambleComputed = true;
    }
    
    public ByteSequence getEpilogueRaw() {
        return this.epilogue;
    }
    
    public void setEpilogueRaw(final ByteSequence epilogue) {
        this.epilogue = epilogue;
        this.epilogueStrCache = null;
        this.epilogueComputed = false;
    }
    
    @Override
    public String getEpilogue() {
        if (!this.epilogueComputed) {
            this.epilogueStrCache = ((this.epilogue != null) ? ContentUtil.decode(this.epilogue) : null);
            this.epilogueComputed = true;
        }
        return this.epilogueStrCache;
    }
    
    @Override
    public void setEpilogue(final String epilogue) {
        this.epilogue = ((epilogue != null) ? ContentUtil.encode(epilogue) : null);
        this.epilogueStrCache = epilogue;
        this.epilogueComputed = true;
    }
}
