// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.james.mime4j.message;

import org.apache.james.mime4j.dom.SingleBody;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import org.apache.james.mime4j.dom.TextBody;

class BasicTextBody extends TextBody
{
    private final byte[] content;
    private final String charset;
    
    BasicTextBody(final byte[] content, final String charset) {
        this.content = content;
        this.charset = charset;
    }
    
    @Override
    public String getMimeCharset() {
        return this.charset;
    }
    
    @Override
    public Reader getReader() throws IOException {
        return new InputStreamReader(this.getInputStream(), this.charset);
    }
    
    @Override
    public InputStream getInputStream() throws IOException {
        return new ByteArrayInputStream(this.content);
    }
    
    @Override
    public SingleBody copy() {
        return new BasicTextBody(this.content, this.charset);
    }
}
