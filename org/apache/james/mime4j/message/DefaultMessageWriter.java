// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.james.mime4j.message;

import org.apache.james.mime4j.util.ByteArrayBuffer;
import org.apache.james.mime4j.codec.CodecUtil;
import org.apache.james.mime4j.util.MimeUtil;
import org.apache.james.mime4j.stream.Field;
import java.util.Iterator;
import org.apache.james.mime4j.util.ByteSequence;
import org.apache.james.mime4j.dom.field.ContentTypeField;
import org.apache.james.mime4j.util.ContentUtil;
import org.apache.james.mime4j.dom.Header;
import org.apache.james.mime4j.dom.BinaryBody;
import java.io.IOException;
import org.apache.james.mime4j.dom.SingleBody;
import org.apache.james.mime4j.dom.Multipart;
import org.apache.james.mime4j.dom.Entity;
import org.apache.james.mime4j.dom.Message;
import java.io.OutputStream;
import org.apache.james.mime4j.dom.Body;
import org.apache.james.mime4j.dom.MessageWriter;

public class DefaultMessageWriter implements MessageWriter
{
    private static final byte[] CRLF;
    private static final byte[] DASHES;
    
    public void writeBody(final Body body, final OutputStream out) throws IOException {
        if (body instanceof Message) {
            this.writeEntity((Entity)body, out);
        }
        else if (body instanceof Multipart) {
            this.writeMultipart((Multipart)body, out);
        }
        else {
            if (!(body instanceof SingleBody)) {
                throw new IllegalArgumentException("Unsupported body class");
            }
            ((SingleBody)body).writeTo(out);
        }
    }
    
    public void writeEntity(final Entity entity, final OutputStream out) throws IOException {
        final Header header = entity.getHeader();
        if (header == null) {
            throw new IllegalArgumentException("Missing header");
        }
        this.writeHeader(header, out);
        final Body body = entity.getBody();
        if (body == null) {
            throw new IllegalArgumentException("Missing body");
        }
        final boolean binaryBody = body instanceof BinaryBody;
        final OutputStream encOut = this.encodeStream(out, entity.getContentTransferEncoding(), binaryBody);
        this.writeBody(body, encOut);
        if (encOut != out) {
            encOut.close();
        }
    }
    
    public void writeMessage(final Message message, final OutputStream out) throws IOException {
        this.writeEntity(message, out);
    }
    
    public void writeMultipart(final Multipart multipart, final OutputStream out) throws IOException {
        final ContentTypeField contentType = this.getContentType(multipart);
        final ByteSequence boundary = this.getBoundary(contentType);
        ByteSequence preamble;
        ByteSequence epilogue;
        if (multipart instanceof MultipartImpl) {
            preamble = ((MultipartImpl)multipart).getPreambleRaw();
            epilogue = ((MultipartImpl)multipart).getEpilogueRaw();
        }
        else {
            preamble = ((multipart.getPreamble() != null) ? ContentUtil.encode(multipart.getPreamble()) : null);
            epilogue = ((multipart.getEpilogue() != null) ? ContentUtil.encode(multipart.getEpilogue()) : null);
        }
        if (preamble != null) {
            this.writeBytes(preamble, out);
            out.write(DefaultMessageWriter.CRLF);
        }
        for (final Entity bodyPart : multipart.getBodyParts()) {
            out.write(DefaultMessageWriter.DASHES);
            this.writeBytes(boundary, out);
            out.write(DefaultMessageWriter.CRLF);
            this.writeEntity(bodyPart, out);
            out.write(DefaultMessageWriter.CRLF);
        }
        out.write(DefaultMessageWriter.DASHES);
        this.writeBytes(boundary, out);
        out.write(DefaultMessageWriter.DASHES);
        out.write(DefaultMessageWriter.CRLF);
        if (epilogue != null) {
            this.writeBytes(epilogue, out);
        }
    }
    
    public void writeField(final Field field, final OutputStream out) throws IOException {
        ByteSequence raw = field.getRaw();
        if (raw == null) {
            final StringBuilder buf = new StringBuilder();
            buf.append(field.getName());
            buf.append(": ");
            final String body = field.getBody();
            if (body != null) {
                buf.append(body);
            }
            raw = ContentUtil.encode(MimeUtil.fold(buf.toString(), 0));
        }
        this.writeBytes(raw, out);
        out.write(DefaultMessageWriter.CRLF);
    }
    
    public void writeHeader(final Header header, final OutputStream out) throws IOException {
        for (final Field field : header) {
            this.writeField(field, out);
        }
        out.write(DefaultMessageWriter.CRLF);
    }
    
    protected OutputStream encodeStream(final OutputStream out, final String encoding, final boolean binaryBody) throws IOException {
        if (MimeUtil.isBase64Encoding(encoding)) {
            return CodecUtil.wrapBase64(out);
        }
        if (MimeUtil.isQuotedPrintableEncoded(encoding)) {
            return CodecUtil.wrapQuotedPrintable(out, binaryBody);
        }
        return out;
    }
    
    private ContentTypeField getContentType(final Multipart multipart) {
        final Entity parent = multipart.getParent();
        if (parent == null) {
            throw new IllegalArgumentException("Missing parent entity in multipart");
        }
        final Header header = parent.getHeader();
        if (header == null) {
            throw new IllegalArgumentException("Missing header in parent entity");
        }
        final ContentTypeField contentType = (ContentTypeField)header.getField("Content-Type");
        if (contentType == null) {
            throw new IllegalArgumentException("Content-Type field not specified");
        }
        return contentType;
    }
    
    private ByteSequence getBoundary(final ContentTypeField contentType) {
        final String boundary = contentType.getBoundary();
        if (boundary == null) {
            throw new IllegalArgumentException("Multipart boundary not specified. Mime-Type: " + contentType.getMimeType() + ", Raw: " + contentType.toString());
        }
        return ContentUtil.encode(boundary);
    }
    
    private void writeBytes(final ByteSequence byteSequence, final OutputStream out) throws IOException {
        if (byteSequence instanceof ByteArrayBuffer) {
            final ByteArrayBuffer bab = (ByteArrayBuffer)byteSequence;
            out.write(bab.buffer(), 0, bab.length());
        }
        else {
            out.write(byteSequence.toByteArray());
        }
    }
    
    static {
        CRLF = new byte[] { 13, 10 };
        DASHES = new byte[] { 45, 45 };
    }
}
