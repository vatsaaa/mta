// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.james.mime4j.message;

import org.apache.james.mime4j.dom.SingleBody;
import java.io.InputStream;
import java.io.IOException;
import java.io.StringReader;
import java.io.Reader;
import java.nio.charset.Charset;
import org.apache.james.mime4j.dom.TextBody;

class StringBody extends TextBody
{
    private final String content;
    private final Charset charset;
    
    StringBody(final String content, final Charset charset) {
        this.content = content;
        this.charset = charset;
    }
    
    @Override
    public String getMimeCharset() {
        return this.charset.name();
    }
    
    @Override
    public Reader getReader() throws IOException {
        return new StringReader(this.content);
    }
    
    @Override
    public InputStream getInputStream() throws IOException {
        return new StringInputStream(this.content, this.charset, 2048);
    }
    
    @Override
    public SingleBody copy() {
        return new StringBody(this.content, this.charset);
    }
}
