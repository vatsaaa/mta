// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.log4j.lf5.viewer;

public class LogTableColumnFormatException extends Exception
{
    public LogTableColumnFormatException(final String message) {
        super(message);
    }
}
