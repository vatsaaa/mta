// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.metadata;

public interface Message
{
    public static final String MESSAGE_RECIPIENT_ADDRESS = "Message-Recipient-Address";
    public static final String MESSAGE_FROM = "Message-From";
    public static final String MESSAGE_TO = "Message-To";
    public static final String MESSAGE_CC = "Message-Cc";
    public static final String MESSAGE_BCC = "Message-Bcc";
}
