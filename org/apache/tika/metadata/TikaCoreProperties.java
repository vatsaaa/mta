// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.metadata;

public interface TikaCoreProperties
{
    public static final Property FORMAT = Property.composite(DublinCore.FORMAT, new Property[] { Property.internalText("format") });
    public static final Property IDENTIFIER = Property.composite(DublinCore.IDENTIFIER, new Property[] { Property.internalText("identifier") });
    public static final Property CONTRIBUTOR = Property.composite(DublinCore.CONTRIBUTOR, new Property[] { Property.internalText("contributor") });
    public static final Property COVERAGE = Property.composite(DublinCore.COVERAGE, new Property[] { Property.internalText("coverage") });
    public static final Property CREATOR = Property.composite(DublinCore.CREATOR, new Property[] { Office.AUTHOR, Property.internalTextBag("creator"), Property.internalTextBag("Author") });
    public static final Property MODIFIER = Office.LAST_AUTHOR;
    public static final Property CREATOR_TOOL = XMP.CREATOR_TOOL;
    public static final Property LANGUAGE = Property.composite(DublinCore.LANGUAGE, new Property[] { Property.internalText("language") });
    public static final Property PUBLISHER = Property.composite(DublinCore.PUBLISHER, new Property[] { Property.internalText("publisher") });
    public static final Property RELATION = Property.composite(DublinCore.RELATION, new Property[] { Property.internalText("relation") });
    public static final Property RIGHTS = Property.composite(DublinCore.RIGHTS, new Property[] { Property.internalText("rights") });
    public static final Property SOURCE = Property.composite(DublinCore.SOURCE, new Property[] { Property.internalText("source") });
    public static final Property TYPE = Property.composite(DublinCore.TYPE, new Property[] { Property.internalText("type") });
    public static final Property TITLE = Property.composite(DublinCore.TITLE, new Property[] { Property.internalText("title") });
    public static final Property DESCRIPTION = Property.composite(DublinCore.DESCRIPTION, new Property[] { Property.internalText("description") });
    public static final Property KEYWORDS = Property.composite(DublinCore.SUBJECT, new Property[] { Office.KEYWORDS, Property.internalTextBag("Keywords"), Property.internalTextBag("subject") });
    public static final Property CREATED = Property.composite(DublinCore.CREATED, new Property[] { Office.CREATION_DATE, MSOffice.CREATION_DATE, Metadata.DATE });
    public static final Property MODIFIED = Property.composite(DublinCore.MODIFIED, new Property[] { Office.SAVE_DATE, MSOffice.LAST_SAVED, Property.internalText("modified"), Property.internalText("Last-Modified") });
    public static final Property PRINT_DATE = Property.composite(Office.PRINT_DATE, new Property[] { MSOffice.LAST_PRINTED });
    public static final Property METADATA_DATE = XMP.METADATA_DATE;
    public static final Property LATITUDE = Geographic.LATITUDE;
    public static final Property LONGITUDE = Geographic.LONGITUDE;
    public static final Property ALTITUDE = Geographic.ALTITUDE;
    public static final Property RATING = XMP.RATING;
    public static final Property COMMENTS = Property.composite(OfficeOpenXMLExtended.COMMENTS, new Property[] { Property.internalTextBag("comment"), Property.internalTextBag("Comments") });
    @Deprecated
    public static final Property TRANSITION_KEYWORDS_TO_DC_SUBJECT = Property.composite(DublinCore.SUBJECT, new Property[] { Property.internalTextBag("Keywords") });
    @Deprecated
    public static final Property TRANSITION_SUBJECT_TO_DC_DESCRIPTION = Property.composite(DublinCore.DESCRIPTION, new Property[] { Property.internalText("subject") });
    @Deprecated
    public static final Property TRANSITION_SUBJECT_TO_DC_TITLE = Property.composite(DublinCore.TITLE, new Property[] { Property.internalText("subject") });
    @Deprecated
    public static final Property TRANSITION_SUBJECT_TO_OO_SUBJECT = Property.composite(OfficeOpenXMLCore.SUBJECT, new Property[] { Property.internalText("subject") });
}
