// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.metadata;

import java.util.Enumeration;
import java.util.Properties;
import java.util.HashMap;
import java.util.Calendar;
import java.text.ParseException;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.text.DateFormatSymbols;
import java.util.Locale;
import java.text.DateFormat;
import java.util.TimeZone;
import java.util.Map;
import java.io.Serializable;

public class Metadata implements CreativeCommons, Geographic, HttpHeaders, Message, MSOffice, ClimateForcast, TIFF, TikaMetadataKeys, TikaMimeKeys, Serializable
{
    private static final long serialVersionUID = 5623926545693153182L;
    private Map<String, String[]> metadata;
    public static final String NAMESPACE_PREFIX_DELIMITER = ":";
    @Deprecated
    public static final String FORMAT = "format";
    @Deprecated
    public static final String IDENTIFIER = "identifier";
    @Deprecated
    public static final String MODIFIED = "modified";
    @Deprecated
    public static final String CONTRIBUTOR = "contributor";
    @Deprecated
    public static final String COVERAGE = "coverage";
    @Deprecated
    public static final String CREATOR = "creator";
    @Deprecated
    public static final Property DATE;
    @Deprecated
    public static final String DESCRIPTION = "description";
    @Deprecated
    public static final String LANGUAGE = "language";
    @Deprecated
    public static final String PUBLISHER = "publisher";
    @Deprecated
    public static final String RELATION = "relation";
    @Deprecated
    public static final String RIGHTS = "rights";
    @Deprecated
    public static final String SOURCE = "source";
    @Deprecated
    public static final String SUBJECT = "subject";
    @Deprecated
    public static final String TITLE = "title";
    @Deprecated
    public static final String TYPE = "type";
    private static final TimeZone UTC;
    private static final TimeZone MIDDAY;
    private static final DateFormat[] iso8601InputFormats;
    
    private static DateFormat createDateFormat(final String format, final TimeZone timezone) {
        final SimpleDateFormat sdf = new SimpleDateFormat(format, new DateFormatSymbols(Locale.US));
        if (timezone != null) {
            sdf.setTimeZone(timezone);
        }
        return sdf;
    }
    
    private static synchronized Date parseDate(String date) {
        final int n = date.length();
        if (date.charAt(n - 3) == ':' && (date.charAt(n - 6) == '+' || date.charAt(n - 6) == '-')) {
            date = date.substring(0, n - 3) + date.substring(n - 2);
        }
        final DateFormat[] arr$ = Metadata.iso8601InputFormats;
        final int len$ = arr$.length;
        int i$ = 0;
        while (i$ < len$) {
            final DateFormat format = arr$[i$];
            try {
                return format.parse(date);
            }
            catch (ParseException ignore) {
                ++i$;
                continue;
            }
            break;
        }
        return null;
    }
    
    private static String formatDate(final Date date) {
        final Calendar calendar = Calendar.getInstance(Metadata.UTC, Locale.US);
        calendar.setTime(date);
        return String.format("%04d-%02d-%02dT%02d:%02d:%02dZ", calendar.get(1), calendar.get(2) + 1, calendar.get(5), calendar.get(11), calendar.get(12), calendar.get(13));
    }
    
    public Metadata() {
        this.metadata = null;
        this.metadata = new HashMap<String, String[]>();
    }
    
    public boolean isMultiValued(final Property property) {
        return this.metadata.get(property.getName()) != null && this.metadata.get(property.getName()).length > 1;
    }
    
    public boolean isMultiValued(final String name) {
        return this.metadata.get(name) != null && this.metadata.get(name).length > 1;
    }
    
    public String[] names() {
        return this.metadata.keySet().toArray(new String[this.metadata.keySet().size()]);
    }
    
    public String get(final String name) {
        final String[] values = this.metadata.get(name);
        if (values == null) {
            return null;
        }
        return values[0];
    }
    
    public String get(final Property property) {
        return this.get(property.getName());
    }
    
    public Integer getInt(final Property property) {
        if (property.getPrimaryProperty().getPropertyType() != Property.PropertyType.SIMPLE) {
            return null;
        }
        if (property.getPrimaryProperty().getValueType() != Property.ValueType.INTEGER) {
            return null;
        }
        final String v = this.get(property);
        if (v == null) {
            return null;
        }
        try {
            return Integer.valueOf(v);
        }
        catch (NumberFormatException e) {
            return null;
        }
    }
    
    public Date getDate(final Property property) {
        if (property.getPrimaryProperty().getPropertyType() != Property.PropertyType.SIMPLE) {
            return null;
        }
        if (property.getPrimaryProperty().getValueType() != Property.ValueType.DATE) {
            return null;
        }
        final String v = this.get(property);
        if (v != null) {
            return parseDate(v);
        }
        return null;
    }
    
    public String[] getValues(final Property property) {
        return this._getValues(property.getName());
    }
    
    public String[] getValues(final String name) {
        return this._getValues(name);
    }
    
    private String[] _getValues(final String name) {
        String[] values = this.metadata.get(name);
        if (values == null) {
            values = new String[0];
        }
        return values;
    }
    
    private String[] appendedValues(final String[] values, final String value) {
        final String[] newValues = new String[values.length + 1];
        System.arraycopy(values, 0, newValues, 0, values.length);
        newValues[newValues.length - 1] = value;
        return newValues;
    }
    
    public void add(final String name, final String value) {
        final String[] values = this.metadata.get(name);
        if (values == null) {
            this.set(name, value);
        }
        else {
            this.metadata.put(name, this.appendedValues(values, value));
        }
    }
    
    public void add(final Property property, final String value) {
        final String[] values = this.metadata.get(property.getName());
        if (values == null) {
            this.set(property, value);
        }
        else {
            if (!property.isMultiValuePermitted()) {
                throw new PropertyTypeException(property.getPropertyType());
            }
            this.set(property, this.appendedValues(values, value));
        }
    }
    
    public void setAll(final Properties properties) {
        final Enumeration<String> names = (Enumeration<String>)properties.propertyNames();
        while (names.hasMoreElements()) {
            final String name = names.nextElement();
            this.metadata.put(name, new String[] { properties.getProperty(name) });
        }
    }
    
    public void set(final String name, final String value) {
        this.metadata.put(name, new String[] { value });
    }
    
    public void set(final Property property, final String value) {
        if (property == null) {
            throw new NullPointerException("property must not be null");
        }
        if (property.getPropertyType() == Property.PropertyType.COMPOSITE) {
            this.set(property.getPrimaryProperty(), value);
            if (property.getSecondaryExtractProperties() != null) {
                for (final Property secondaryExtractProperty : property.getSecondaryExtractProperties()) {
                    this.set(secondaryExtractProperty, value);
                }
            }
        }
        else {
            this.set(property.getName(), value);
        }
    }
    
    public void set(final Property property, final String[] values) {
        if (property == null) {
            throw new NullPointerException("property must not be null");
        }
        if (property.getPropertyType() == Property.PropertyType.COMPOSITE) {
            this.set(property.getPrimaryProperty(), values);
            if (property.getSecondaryExtractProperties() != null) {
                for (final Property secondaryExtractProperty : property.getSecondaryExtractProperties()) {
                    this.set(secondaryExtractProperty, values);
                }
            }
        }
        else {
            this.metadata.put(property.getName(), values);
        }
    }
    
    public void set(final Property property, final int value) {
        if (property.getPrimaryProperty().getPropertyType() != Property.PropertyType.SIMPLE) {
            throw new PropertyTypeException(Property.PropertyType.SIMPLE, property.getPrimaryProperty().getPropertyType());
        }
        if (property.getPrimaryProperty().getValueType() != Property.ValueType.INTEGER) {
            throw new PropertyTypeException(Property.ValueType.INTEGER, property.getPrimaryProperty().getValueType());
        }
        this.set(property, Integer.toString(value));
    }
    
    public void set(final Property property, final double value) {
        if (property.getPrimaryProperty().getPropertyType() != Property.PropertyType.SIMPLE) {
            throw new PropertyTypeException(Property.PropertyType.SIMPLE, property.getPrimaryProperty().getPropertyType());
        }
        if (property.getPrimaryProperty().getValueType() != Property.ValueType.REAL && property.getPrimaryProperty().getValueType() != Property.ValueType.RATIONAL) {
            throw new PropertyTypeException(Property.ValueType.REAL, property.getPrimaryProperty().getValueType());
        }
        this.set(property, Double.toString(value));
    }
    
    public void set(final Property property, final Date date) {
        if (property.getPrimaryProperty().getPropertyType() != Property.PropertyType.SIMPLE) {
            throw new PropertyTypeException(Property.PropertyType.SIMPLE, property.getPrimaryProperty().getPropertyType());
        }
        if (property.getPrimaryProperty().getValueType() != Property.ValueType.DATE) {
            throw new PropertyTypeException(Property.ValueType.DATE, property.getPrimaryProperty().getValueType());
        }
        this.set(property, formatDate(date));
    }
    
    public void remove(final String name) {
        this.metadata.remove(name);
    }
    
    public int size() {
        return this.metadata.size();
    }
    
    @Override
    public boolean equals(final Object o) {
        if (o == null) {
            return false;
        }
        Metadata other = null;
        try {
            other = (Metadata)o;
        }
        catch (ClassCastException cce) {
            return false;
        }
        if (other.size() != this.size()) {
            return false;
        }
        final String[] names = this.names();
        for (int i = 0; i < names.length; ++i) {
            final String[] otherValues = other._getValues(names[i]);
            final String[] thisValues = this._getValues(names[i]);
            if (otherValues.length != thisValues.length) {
                return false;
            }
            for (int j = 0; j < otherValues.length; ++j) {
                if (!otherValues[j].equals(thisValues[j])) {
                    return false;
                }
            }
        }
        return true;
    }
    
    @Override
    public String toString() {
        final StringBuffer buf = new StringBuffer();
        final String[] names = this.names();
        for (int i = 0; i < names.length; ++i) {
            final String[] values = this._getValues(names[i]);
            for (int j = 0; j < values.length; ++j) {
                buf.append(names[i]).append("=").append(values[j]).append(" ");
            }
        }
        return buf.toString();
    }
    
    static {
        DATE = Property.internalDate("date");
        UTC = TimeZone.getTimeZone("UTC");
        MIDDAY = TimeZone.getTimeZone("GMT-12:00");
        iso8601InputFormats = new DateFormat[] { createDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Metadata.UTC), createDateFormat("yyyy-MM-dd'T'HH:mm:ssZ", null), createDateFormat("yyyy-MM-dd'T'HH:mm:ss", null), createDateFormat("yyyy-MM-dd' 'HH:mm:ss'Z'", Metadata.UTC), createDateFormat("yyyy-MM-dd' 'HH:mm:ssZ", null), createDateFormat("yyyy-MM-dd' 'HH:mm:ss", null), createDateFormat("yyyy-MM-dd", Metadata.MIDDAY), createDateFormat("yyyy:MM:dd", Metadata.MIDDAY) };
    }
}
