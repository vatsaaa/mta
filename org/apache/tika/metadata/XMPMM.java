// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.metadata;

public interface XMPMM
{
    public static final String NAMESPACE_URI = "http://ns.adobe.com/xap/1.0/mm/";
    public static final String PREFIX = "xmpMM";
    public static final String PREFIX_ = "xmpMM:";
    public static final Property DOCUMENTID = Property.externalText("xmpMM:DocumentID");
    public static final Property INSTANCEID = Property.externalText("xmpMM:InstanceID");
    public static final Property ORIGINAL_DOCUMENTID = Property.externalText("xmpMM:OriginalDocumentID");
    public static final Property RENDITION_CLASS = Property.externalOpenChoise("xmpMM:RenditionClass", "default", "draft", "low-res", "proof", "screen", "thumbnail");
    public static final Property RENDITION_PARAMS = Property.externalText("xmpMM:RenditionParams");
}
