// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.gui;

import java.net.URI;
import java.util.StringTokenizer;
import java.util.ArrayList;
import javax.swing.Icon;
import java.awt.datatransfer.Clipboard;
import java.awt.event.InputEvent;
import java.util.Iterator;
import java.net.URL;
import java.io.File;
import java.util.List;
import java.awt.datatransfer.Transferable;
import javax.swing.JComponent;
import java.awt.datatransfer.DataFlavor;
import javax.swing.TransferHandler;

class ParsingTransferHandler extends TransferHandler
{
    private static final long serialVersionUID = -557932290014044494L;
    private final TransferHandler delegate;
    private final TikaGUI tika;
    private static DataFlavor uriListFlavor;
    private static DataFlavor urlListFlavor;
    
    public ParsingTransferHandler(final TransferHandler delegate, final TikaGUI tika) {
        this.delegate = delegate;
        this.tika = tika;
    }
    
    @Override
    public boolean canImport(final JComponent component, final DataFlavor[] flavors) {
        for (final DataFlavor flavor : flavors) {
            if (flavor.equals(DataFlavor.javaFileListFlavor) || flavor.equals(ParsingTransferHandler.uriListFlavor) || flavor.equals(ParsingTransferHandler.urlListFlavor)) {
                return true;
            }
        }
        return false;
    }
    
    @Override
    public boolean importData(final JComponent component, final Transferable transferable) {
        try {
            if (transferable.isDataFlavorSupported(DataFlavor.javaFileListFlavor)) {
                this.importFiles((List<File>)transferable.getTransferData(DataFlavor.javaFileListFlavor));
            }
            else if (transferable.isDataFlavorSupported(ParsingTransferHandler.urlListFlavor)) {
                final Object data = transferable.getTransferData(ParsingTransferHandler.urlListFlavor);
                this.tika.openURL(new URL(data.toString()));
            }
            else if (transferable.isDataFlavorSupported(ParsingTransferHandler.uriListFlavor)) {
                this.importFiles(uriToFileList(transferable.getTransferData(ParsingTransferHandler.uriListFlavor)));
            }
            return true;
        }
        catch (Exception e) {
            return false;
        }
    }
    
    private void importFiles(final List<File> files) {
        for (final File file : files) {
            this.tika.openFile(file);
        }
    }
    
    @Override
    public void exportAsDrag(final JComponent arg0, final InputEvent arg1, final int arg2) {
        this.delegate.exportAsDrag(arg0, arg1, arg2);
    }
    
    @Override
    public void exportToClipboard(final JComponent arg0, final Clipboard arg1, final int arg2) throws IllegalStateException {
        this.delegate.exportToClipboard(arg0, arg1, arg2);
    }
    
    @Override
    public int getSourceActions(final JComponent arg0) {
        return this.delegate.getSourceActions(arg0);
    }
    
    @Override
    public Icon getVisualRepresentation(final Transferable arg0) {
        return this.delegate.getVisualRepresentation(arg0);
    }
    
    private static List<File> uriToFileList(final Object data) {
        final List<File> list = new ArrayList<File>();
        final StringTokenizer st = new StringTokenizer(data.toString(), "\r\n");
        while (st.hasMoreTokens()) {
            final String s = st.nextToken();
            if (s.startsWith("#")) {
                continue;
            }
            try {
                list.add(new File(new URI(s)));
            }
            catch (Exception ex) {}
        }
        return list;
    }
    
    static {
        try {
            ParsingTransferHandler.uriListFlavor = new DataFlavor("text/uri-list;class=java.lang.String");
            ParsingTransferHandler.urlListFlavor = new DataFlavor("text/plain;class=java.lang.String");
        }
        catch (ClassNotFoundException ex) {}
    }
}
