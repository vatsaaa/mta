// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.gui;

import org.apache.tika.exception.TikaException;
import java.io.OutputStream;
import java.io.FileOutputStream;
import org.apache.tika.mime.MediaType;
import java.util.Set;
import java.util.HashMap;
import java.util.Map;
import org.apache.tika.parser.AbstractParser;
import org.apache.tika.parser.html.BoilerpipeContentHandler;
import org.apache.tika.sax.BodyContentHandler;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.sax.TransformerHandler;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.AttributesImpl;
import org.xml.sax.Attributes;
import org.apache.tika.sax.ContentHandlerDecorator;
import javax.xml.transform.Result;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.sax.SAXTransformerFactory;
import org.apache.tika.io.IOUtils;
import javax.swing.event.HyperlinkEvent;
import javax.swing.JTextPane;
import java.io.IOException;
import javax.swing.TransferHandler;
import javax.swing.JScrollPane;
import java.awt.Frame;
import javax.swing.JDialog;
import java.awt.Color;
import java.io.PrintWriter;
import java.util.Arrays;
import javax.swing.ProgressMonitorInputStream;
import org.apache.tika.sax.TeeContentHandler;
import java.io.Writer;
import org.xml.sax.ContentHandler;
import java.io.StringWriter;
import java.io.InputStream;
import org.apache.tika.io.TikaInputStream;
import org.apache.tika.metadata.Metadata;
import java.io.File;
import java.awt.AWTEvent;
import java.awt.Window;
import java.awt.event.WindowEvent;
import java.awt.Toolkit;
import java.net.MalformedURLException;
import java.net.URL;
import javax.swing.Icon;
import javax.swing.JOptionPane;
import java.awt.event.ActionEvent;
import javax.swing.JMenuItem;
import javax.swing.Box;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import org.apache.tika.extractor.DocumentSelector;
import java.awt.Dimension;
import java.awt.Container;
import java.awt.Component;
import java.awt.LayoutManager;
import javax.swing.SwingUtilities;
import org.apache.tika.parser.AutoDetectParser;
import javax.swing.UIManager;
import javax.swing.JFileChooser;
import javax.swing.JEditorPane;
import javax.swing.JPanel;
import java.awt.CardLayout;
import org.apache.tika.parser.Parser;
import org.apache.tika.parser.ParseContext;
import javax.swing.event.HyperlinkListener;
import java.awt.event.ActionListener;
import javax.swing.JFrame;

public class TikaGUI extends JFrame implements ActionListener, HyperlinkListener
{
    private static final long serialVersionUID = 5883906936187059495L;
    private final ParseContext context;
    private final Parser parser;
    private final ImageSavingParser imageParser;
    private final CardLayout layout;
    private final JPanel cards;
    private final JEditorPane html;
    private final JEditorPane text;
    private final JEditorPane textMain;
    private final JEditorPane xml;
    private final JEditorPane metadata;
    private final JFileChooser chooser;
    
    public static void main(final String[] args) throws Exception {
        UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                new TikaGUI(new AutoDetectParser()).setVisible(true);
            }
        });
    }
    
    public TikaGUI(final Parser parser) {
        super("Apache Tika");
        this.layout = new CardLayout();
        this.chooser = new JFileChooser();
        this.setDefaultCloseOperation(3);
        this.addMenuBar();
        this.addWelcomeCard(this.cards = new JPanel(this.layout), "welcome");
        this.metadata = this.addCard(this.cards, "text/plain", "metadata");
        this.html = this.addCard(this.cards, "text/html", "html");
        this.text = this.addCard(this.cards, "text/plain", "text");
        this.textMain = this.addCard(this.cards, "text/plain", "main");
        this.xml = this.addCard(this.cards, "text/plain", "xhtml");
        this.add(this.cards);
        this.layout.show(this.cards, "welcome");
        this.setPreferredSize(new Dimension(640, 480));
        this.pack();
        this.context = new ParseContext();
        this.parser = parser;
        this.imageParser = new ImageSavingParser(parser);
        this.context.set((Class<ImageDocumentSelector>)DocumentSelector.class, new ImageDocumentSelector());
        this.context.set(Parser.class, this.imageParser);
    }
    
    private void addMenuBar() {
        final JMenuBar bar = new JMenuBar();
        final JMenu file = new JMenu("File");
        file.setMnemonic(70);
        this.addMenuItem(file, "Open...", "openfile", 79);
        this.addMenuItem(file, "Open URL...", "openurl", 85);
        file.addSeparator();
        this.addMenuItem(file, "Exit", "exit", 88);
        bar.add(file);
        final JMenu view = new JMenu("View");
        view.setMnemonic(86);
        this.addMenuItem(view, "Metadata", "metadata", 77);
        this.addMenuItem(view, "Formatted text", "html", 70);
        this.addMenuItem(view, "Plain text", "text", 80);
        this.addMenuItem(view, "Main content", "main", 67);
        this.addMenuItem(view, "Structured text", "xhtml", 83);
        bar.add(view);
        bar.add(Box.createHorizontalGlue());
        final JMenu help = new JMenu("Help");
        help.setMnemonic(72);
        this.addMenuItem(help, "About Tika", "about", 65);
        bar.add(help);
        this.setJMenuBar(bar);
    }
    
    private void addMenuItem(final JMenu menu, final String title, final String command, final int key) {
        final JMenuItem item = new JMenuItem(title, key);
        item.setActionCommand(command);
        item.addActionListener(this);
        menu.add(item);
    }
    
    public void actionPerformed(final ActionEvent e) {
        final String command = e.getActionCommand();
        if ("openfile".equals(command)) {
            final int rv = this.chooser.showOpenDialog(this);
            if (rv == 0) {
                this.openFile(this.chooser.getSelectedFile());
            }
        }
        else if ("openurl".equals(command)) {
            final Object rv2 = JOptionPane.showInputDialog(this, "Enter the URL of the resource to be parsed:", "Open URL", -1, null, null, "");
            if (rv2 != null && rv2.toString().length() > 0) {
                try {
                    this.openURL(new URL(rv2.toString().trim()));
                }
                catch (MalformedURLException exception) {
                    JOptionPane.showMessageDialog(this, "The given string is not a valid URL", "Invalid URL", 0);
                }
            }
        }
        else if ("html".equals(command)) {
            this.layout.show(this.cards, command);
        }
        else if ("text".equals(command)) {
            this.layout.show(this.cards, command);
        }
        else if ("main".equals(command)) {
            this.layout.show(this.cards, command);
        }
        else if ("xhtml".equals(command)) {
            this.layout.show(this.cards, command);
        }
        else if ("metadata".equals(command)) {
            this.layout.show(this.cards, command);
        }
        else if ("about".equals(command)) {
            this.textDialog("About Apache Tika", TikaGUI.class.getResource("about.html"));
        }
        else if ("exit".equals(command)) {
            Toolkit.getDefaultToolkit().getSystemEventQueue().postEvent(new WindowEvent(this, 201));
        }
    }
    
    public void openFile(final File file) {
        try {
            final Metadata metadata = new Metadata();
            final TikaInputStream stream = TikaInputStream.get(file, metadata);
            try {
                this.handleStream(stream, metadata);
            }
            finally {
                stream.close();
            }
        }
        catch (Throwable t) {
            this.handleError(file.getPath(), t);
        }
    }
    
    public void openURL(final URL url) {
        try {
            final Metadata metadata = new Metadata();
            final TikaInputStream stream = TikaInputStream.get(url, metadata);
            try {
                this.handleStream(stream, metadata);
            }
            finally {
                stream.close();
            }
        }
        catch (Throwable t) {
            this.handleError(url.toString(), t);
        }
    }
    
    private void handleStream(InputStream input, final Metadata md) throws Exception {
        final StringWriter htmlBuffer = new StringWriter();
        final StringWriter textBuffer = new StringWriter();
        final StringWriter textMainBuffer = new StringWriter();
        final StringWriter xmlBuffer = new StringWriter();
        final StringBuilder metadataBuffer = new StringBuilder();
        final ContentHandler handler = new TeeContentHandler(new ContentHandler[] { this.getHtmlHandler(htmlBuffer), this.getTextContentHandler(textBuffer), this.getTextMainContentHandler(textMainBuffer), this.getXmlContentHandler(xmlBuffer) });
        this.context.set((Class<ImageDocumentSelector>)DocumentSelector.class, new ImageDocumentSelector());
        input = new ProgressMonitorInputStream(this, "Parsing stream", input);
        this.parser.parse(input, handler, md, this.context);
        final String[] names = md.names();
        Arrays.sort(names);
        for (final String name : names) {
            metadataBuffer.append(name);
            metadataBuffer.append(": ");
            metadataBuffer.append(md.get(name));
            metadataBuffer.append("\n");
        }
        final String name2 = md.get("resourceName");
        if (name2 != null && name2.length() > 0) {
            this.setTitle("Apache Tika: " + name2);
        }
        else {
            this.setTitle("Apache Tika: unnamed document");
        }
        this.setText(this.metadata, metadataBuffer.toString());
        this.setText(this.xml, xmlBuffer.toString());
        this.setText(this.text, textBuffer.toString());
        this.setText(this.textMain, textMainBuffer.toString());
        this.setText(this.html, htmlBuffer.toString());
        this.layout.show(this.cards, "metadata");
    }
    
    private void handleError(final String name, final Throwable t) {
        final StringWriter writer = new StringWriter();
        writer.append("Apache Tika was unable to parse the document\n");
        writer.append("at " + name + ".\n\n");
        writer.append("The full exception stack trace is included below:\n\n");
        t.printStackTrace(new PrintWriter(writer));
        final JEditorPane editor = new JEditorPane("text/plain", writer.toString());
        editor.setEditable(false);
        editor.setBackground(Color.WHITE);
        editor.setCaretPosition(0);
        editor.setPreferredSize(new Dimension(600, 400));
        final JDialog dialog = new JDialog(this, "Apache Tika error");
        dialog.add(new JScrollPane(editor));
        dialog.pack();
        dialog.setVisible(true);
    }
    
    private void addWelcomeCard(final JPanel panel, final String name) {
        try {
            final JEditorPane editor = new JEditorPane(TikaGUI.class.getResource("welcome.html"));
            editor.setContentType("text/html");
            editor.setEditable(false);
            editor.setBackground(Color.WHITE);
            editor.setTransferHandler(new ParsingTransferHandler(editor.getTransferHandler(), this));
            panel.add(new JScrollPane(editor), name);
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    private JEditorPane addCard(final JPanel panel, final String type, final String name) {
        final JEditorPane editor = new JTextPane();
        editor.setBackground(Color.WHITE);
        editor.setContentType(type);
        editor.setTransferHandler(new ParsingTransferHandler(editor.getTransferHandler(), this));
        panel.add(new JScrollPane(editor), name);
        return editor;
    }
    
    private void textDialog(final String title, final URL resource) {
        try {
            final JDialog dialog = new JDialog(this, title);
            final JEditorPane editor = new JEditorPane(resource);
            editor.setContentType("text/html");
            editor.setEditable(false);
            editor.setBackground(Color.WHITE);
            editor.setPreferredSize(new Dimension(400, 250));
            editor.addHyperlinkListener(this);
            dialog.add(editor);
            dialog.pack();
            dialog.setVisible(true);
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    public void hyperlinkUpdate(final HyperlinkEvent e) {
        if (e.getEventType() == HyperlinkEvent.EventType.ACTIVATED) {
            try {
                final URL url = e.getURL();
                final InputStream stream = url.openStream();
                try {
                    final StringWriter writer = new StringWriter();
                    IOUtils.copy(stream, writer, "UTF-8");
                    final JEditorPane editor = new JEditorPane("text/plain", writer.toString());
                    editor.setEditable(false);
                    editor.setBackground(Color.WHITE);
                    editor.setCaretPosition(0);
                    editor.setPreferredSize(new Dimension(600, 400));
                    String name = url.toString();
                    name = name.substring(name.lastIndexOf(47) + 1);
                    final JDialog dialog = new JDialog(this, "Apache Tika: " + name);
                    dialog.add(new JScrollPane(editor));
                    dialog.pack();
                    dialog.setVisible(true);
                }
                finally {
                    stream.close();
                }
            }
            catch (IOException exception) {
                exception.printStackTrace();
            }
        }
    }
    
    private void setText(final JEditorPane editor, final String text) {
        editor.setText(text);
        editor.setCaretPosition(0);
    }
    
    private ContentHandler getHtmlHandler(final Writer writer) throws TransformerConfigurationException {
        final SAXTransformerFactory factory = (SAXTransformerFactory)TransformerFactory.newInstance();
        final TransformerHandler handler = factory.newTransformerHandler();
        handler.getTransformer().setOutputProperty("method", "html");
        handler.setResult(new StreamResult(writer));
        return new ContentHandlerDecorator(handler) {
            @Override
            public void startElement(String uri, final String localName, final String name, final Attributes atts) throws SAXException {
                if ("http://www.w3.org/1999/xhtml".equals(uri)) {
                    uri = null;
                }
                if (!"head".equals(localName)) {
                    if ("img".equals(localName)) {
                        AttributesImpl newAttrs;
                        if (atts instanceof AttributesImpl) {
                            newAttrs = (AttributesImpl)atts;
                        }
                        else {
                            newAttrs = new AttributesImpl(atts);
                        }
                        for (int i = 0; i < newAttrs.getLength(); ++i) {
                            if ("src".equals(newAttrs.getLocalName(i))) {
                                final String src = newAttrs.getValue(i);
                                if (src.startsWith("embedded:")) {
                                    final String filename = src.substring(src.indexOf(58) + 1);
                                    try {
                                        final File img = TikaGUI.this.imageParser.requestSave(filename);
                                        final String newSrc = img.toURI().toString();
                                        newAttrs.setValue(i, newSrc);
                                    }
                                    catch (IOException e) {
                                        System.err.println("Error creating temp image file " + filename);
                                    }
                                }
                            }
                        }
                        super.startElement(uri, localName, name, newAttrs);
                    }
                    else {
                        super.startElement(uri, localName, name, atts);
                    }
                }
            }
            
            @Override
            public void endElement(String uri, final String localName, final String name) throws SAXException {
                if ("http://www.w3.org/1999/xhtml".equals(uri)) {
                    uri = null;
                }
                if (!"head".equals(localName)) {
                    super.endElement(uri, localName, name);
                }
            }
            
            @Override
            public void startPrefixMapping(final String prefix, final String uri) {
            }
            
            @Override
            public void endPrefixMapping(final String prefix) {
            }
        };
    }
    
    private ContentHandler getTextContentHandler(final Writer writer) {
        return new BodyContentHandler(writer);
    }
    
    private ContentHandler getTextMainContentHandler(final Writer writer) {
        return new BoilerpipeContentHandler(writer);
    }
    
    private ContentHandler getXmlContentHandler(final Writer writer) throws TransformerConfigurationException {
        final SAXTransformerFactory factory = (SAXTransformerFactory)TransformerFactory.newInstance();
        final TransformerHandler handler = factory.newTransformerHandler();
        handler.getTransformer().setOutputProperty("method", "xml");
        handler.setResult(new StreamResult(writer));
        return handler;
    }
    
    private static class ImageDocumentSelector implements DocumentSelector
    {
        public boolean select(final Metadata metadata) {
            final String type = metadata.get("Content-Type");
            return type != null && type.startsWith("image/");
        }
    }
    
    private static class ImageSavingParser extends AbstractParser
    {
        private Map<String, File> wanted;
        private Parser downstreamParser;
        private File tmpDir;
        
        private ImageSavingParser(final Parser downstreamParser) {
            this.wanted = new HashMap<String, File>();
            this.downstreamParser = downstreamParser;
            try {
                final File t = File.createTempFile("tika", ".test");
                this.tmpDir = t.getParentFile();
            }
            catch (IOException ex) {}
        }
        
        public File requestSave(final String embeddedName) throws IOException {
            final String suffix = ".tika";
            final int splitAt = embeddedName.lastIndexOf(46);
            if (splitAt > 0) {
                embeddedName.substring(splitAt);
            }
            final File tmp = File.createTempFile("tika-embedded-", suffix);
            this.wanted.put(embeddedName, tmp);
            return tmp;
        }
        
        public Set<MediaType> getSupportedTypes(final ParseContext context) {
            return null;
        }
        
        public void parse(final InputStream stream, final ContentHandler handler, final Metadata metadata, final ParseContext context) throws IOException, SAXException, TikaException {
            final String name = metadata.get("resourceName");
            if (name != null && this.wanted.containsKey(name)) {
                final FileOutputStream out = new FileOutputStream(this.wanted.get(name));
                IOUtils.copy(stream, out);
                out.close();
            }
            else if (this.downstreamParser != null) {
                this.downstreamParser.parse(stream, handler, metadata, context);
            }
        }
    }
}
