// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.parser.audio;

import java.util.Collections;
import java.util.Collection;
import java.util.HashSet;
import java.util.Arrays;
import org.apache.tika.exception.TikaException;
import org.xml.sax.SAXException;
import java.io.IOException;
import javax.sound.midi.MidiMessage;
import javax.sound.midi.Patch;
import javax.sound.midi.Track;
import javax.sound.midi.Sequence;
import javax.sound.midi.InvalidMidiDataException;
import javax.sound.midi.MetaMessage;
import javax.sound.midi.MidiSystem;
import java.io.BufferedInputStream;
import org.apache.tika.sax.XHTMLContentHandler;
import org.apache.tika.metadata.Metadata;
import org.xml.sax.ContentHandler;
import java.io.InputStream;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.mime.MediaType;
import java.util.Set;
import org.apache.tika.parser.AbstractParser;

public class MidiParser extends AbstractParser
{
    private static final long serialVersionUID = 6343278584336189432L;
    private static final Set<MediaType> SUPPORTED_TYPES;
    
    public Set<MediaType> getSupportedTypes(final ParseContext context) {
        return MidiParser.SUPPORTED_TYPES;
    }
    
    public void parse(final InputStream stream, final ContentHandler handler, final Metadata metadata, final ParseContext context) throws IOException, SAXException, TikaException {
        metadata.set("Content-Type", "audio/midi");
        final XHTMLContentHandler xhtml = new XHTMLContentHandler(handler, metadata);
        xhtml.startDocument();
        final InputStream buffered = new BufferedInputStream(stream);
        try {
            final Sequence sequence = MidiSystem.getSequence(buffered);
            final Track[] tracks = sequence.getTracks();
            metadata.set("tracks", String.valueOf(tracks.length));
            final Patch[] patches = sequence.getPatchList();
            metadata.set("patches", String.valueOf(patches.length));
            final float type = sequence.getDivisionType();
            if (type == 0.0f) {
                metadata.set("divisionType", "PPQ");
            }
            else if (type == 24.0f) {
                metadata.set("divisionType", "SMPTE_24");
            }
            else if (type == 25.0f) {
                metadata.set("divisionType", "SMPTE_25");
            }
            else if (type == 30.0f) {
                metadata.set("divisionType", "SMPTE_30");
            }
            else if (type == 29.97f) {
                metadata.set("divisionType", "SMPTE_30DROP");
            }
            else if (type == 24.0f) {
                metadata.set("divisionType", String.valueOf(type));
            }
            for (final Track track : tracks) {
                xhtml.startElement("p");
                for (int i = 0; i < track.size(); ++i) {
                    final MidiMessage message = track.get(i).getMessage();
                    if (message instanceof MetaMessage) {
                        final MetaMessage meta = (MetaMessage)message;
                        if (meta.getType() >= 1 && meta.getType() <= 15) {
                            xhtml.characters(new String(meta.getData(), "ISO-8859-1"));
                        }
                    }
                }
                xhtml.endElement("p");
            }
        }
        catch (InvalidMidiDataException ex) {}
        xhtml.endDocument();
    }
    
    static {
        SUPPORTED_TYPES = Collections.unmodifiableSet((Set<? extends MediaType>)new HashSet<MediaType>(Arrays.asList(MediaType.application("x-midi"), MediaType.audio("midi"))));
    }
}
