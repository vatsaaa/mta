// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.parser.audio;

import java.util.Collections;
import java.util.Collection;
import java.util.HashSet;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Map;
import org.apache.tika.exception.TikaException;
import org.xml.sax.SAXException;
import java.io.IOException;
import javax.sound.sampled.AudioFormat;
import org.apache.tika.sax.XHTMLContentHandler;
import javax.sound.sampled.UnsupportedAudioFileException;
import org.apache.tika.metadata.XMPDM;
import javax.sound.sampled.AudioFileFormat;
import javax.sound.sampled.AudioSystem;
import java.io.BufferedInputStream;
import org.apache.tika.metadata.Metadata;
import org.xml.sax.ContentHandler;
import java.io.InputStream;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.mime.MediaType;
import java.util.Set;
import org.apache.tika.parser.AbstractParser;

public class AudioParser extends AbstractParser
{
    private static final long serialVersionUID = -6015684081240882695L;
    private static final Set<MediaType> SUPPORTED_TYPES;
    
    public Set<MediaType> getSupportedTypes(final ParseContext context) {
        return AudioParser.SUPPORTED_TYPES;
    }
    
    public void parse(InputStream stream, final ContentHandler handler, final Metadata metadata, final ParseContext context) throws IOException, SAXException, TikaException {
        if (!stream.markSupported()) {
            stream = new BufferedInputStream(stream);
        }
        try {
            final AudioFileFormat fileFormat = AudioSystem.getAudioFileFormat(stream);
            final AudioFileFormat.Type type = fileFormat.getType();
            if (type == AudioFileFormat.Type.AIFC || type == AudioFileFormat.Type.AIFF) {
                metadata.set("Content-Type", "audio/x-aiff");
            }
            else if (type == AudioFileFormat.Type.AU || type == AudioFileFormat.Type.SND) {
                metadata.set("Content-Type", "audio/basic");
            }
            else if (type == AudioFileFormat.Type.WAVE) {
                metadata.set("Content-Type", "audio/x-wav");
            }
            final AudioFormat audioFormat = fileFormat.getFormat();
            final int channels = audioFormat.getChannels();
            if (channels != -1) {
                metadata.set("channels", String.valueOf(channels));
            }
            final float rate = audioFormat.getSampleRate();
            if (rate != -1.0f) {
                metadata.set("samplerate", String.valueOf(rate));
                metadata.set(XMPDM.AUDIO_SAMPLE_RATE, Integer.toString((int)rate));
            }
            final int bits = audioFormat.getSampleSizeInBits();
            if (bits != -1) {
                metadata.set("bits", String.valueOf(bits));
                if (bits == 8) {
                    metadata.set(XMPDM.AUDIO_SAMPLE_TYPE, "8Int");
                }
                else if (bits == 16) {
                    metadata.set(XMPDM.AUDIO_SAMPLE_TYPE, "16Int");
                }
                else if (bits == 32) {
                    metadata.set(XMPDM.AUDIO_SAMPLE_TYPE, "32Int");
                }
            }
            metadata.set("encoding", audioFormat.getEncoding().toString());
            this.addMetadata(metadata, fileFormat.properties());
            this.addMetadata(metadata, audioFormat.properties());
        }
        catch (UnsupportedAudioFileException ex) {}
        final XHTMLContentHandler xhtml = new XHTMLContentHandler(handler, metadata);
        xhtml.startDocument();
        xhtml.endDocument();
    }
    
    private void addMetadata(final Metadata metadata, final Map<String, Object> properties) {
        if (properties != null) {
            for (final Map.Entry<String, Object> entry : properties.entrySet()) {
                final Object value = entry.getValue();
                if (value != null) {
                    metadata.set(entry.getKey(), value.toString());
                }
            }
        }
    }
    
    static {
        SUPPORTED_TYPES = Collections.unmodifiableSet((Set<? extends MediaType>)new HashSet<MediaType>(Arrays.asList(MediaType.audio("basic"), MediaType.audio("x-wav"), MediaType.audio("x-aiff"))));
    }
}
