// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.parser.prt;

import java.util.Collections;
import java.io.UnsupportedEncodingException;
import org.apache.tika.io.EndianUtils;
import org.apache.tika.exception.TikaException;
import org.xml.sax.SAXException;
import java.io.IOException;
import org.apache.tika.metadata.TikaCoreProperties;
import org.apache.poi.util.IOUtils;
import org.apache.tika.sax.XHTMLContentHandler;
import org.apache.tika.metadata.Metadata;
import org.xml.sax.ContentHandler;
import java.io.InputStream;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.mime.MediaType;
import java.util.Set;
import org.apache.tika.parser.AbstractParser;

public class PRTParser extends AbstractParser
{
    private static final long serialVersionUID = 4659638314375035178L;
    private static final Set<MediaType> SUPPORTED_TYPES;
    public static final String PRT_MIME_TYPE = "application/x-prt";
    private static final int MAX_SANE_TEXT_LENGTH = 2048;
    
    public Set<MediaType> getSupportedTypes(final ParseContext context) {
        return PRTParser.SUPPORTED_TYPES;
    }
    
    public void parse(final InputStream stream, final ContentHandler handler, final Metadata metadata, final ParseContext context) throws IOException, SAXException, TikaException {
        final XHTMLContentHandler xhtml = new XHTMLContentHandler(handler, metadata);
        final Last5 l5 = new Last5();
        final byte[] header = new byte[30];
        IOUtils.readFully(stream, header);
        final byte[] date = new byte[12];
        IOUtils.readFully(stream, date);
        final String dateStr = new String(date, "ASCII");
        if (dateStr.startsWith("19") || dateStr.startsWith("20")) {
            final String formattedDate = dateStr.substring(0, 4) + "-" + dateStr.substring(4, 6) + "-" + dateStr.substring(6, 8) + "T" + dateStr.substring(8, 10) + ":" + dateStr.substring(10, 12) + ":00";
            metadata.set(TikaCoreProperties.CREATED, formattedDate);
        }
        metadata.set("Content-Type", "application/x-prt");
        final byte[] desc = new byte[500];
        IOUtils.readFully(stream, desc);
        final String description = this.extractText(desc, true);
        if (description.length() > 0) {
            metadata.set(TikaCoreProperties.DESCRIPTION, description);
        }
        int read;
        while ((read = stream.read()) > -1) {
            if (read == 224 || read == 227 || read == 240) {
                final int nread = stream.read();
                if (nread != 63 && nread != 191) {
                    continue;
                }
                if (read == 227 && nread == 63) {
                    if (!l5.is33()) {
                        continue;
                    }
                    this.handleNoteText(stream, xhtml);
                }
                else {
                    if (!l5.is00()) {
                        continue;
                    }
                    this.handleViewName(read, nread, stream, xhtml, l5);
                }
            }
            else {
                l5.record(read);
            }
        }
    }
    
    private void handleNoteText(final InputStream stream, final XHTMLContentHandler xhtml) throws IOException, SAXException, TikaException {
        for (int i = 0; i < 10; ++i) {
            final int read = stream.read();
            if (read < 0 || read > 15) {
                return;
            }
        }
        final int read = stream.read();
        if (read != 31) {
            return;
        }
        final int length = EndianUtils.readUShortLE(stream);
        if (length <= 2048) {
            this.handleText(length, stream, xhtml);
        }
    }
    
    private void handleViewName(final int typeA, final int typeB, final InputStream stream, final XHTMLContentHandler xhtml, final Last5 l5) throws IOException, SAXException, TikaException {
        final int maybeLength = EndianUtils.readUShortLE(stream);
        if (maybeLength == 0) {
            for (int i = 0; i < 6; ++i) {
                final int read = stream.read();
                if (read < 0 || read > 15) {
                    return;
                }
            }
            final byte[] b2 = new byte[2];
            IOUtils.readFully(stream, b2);
            final int length = EndianUtils.getUShortLE(b2);
            if (length > 1 && length <= 2048) {
                this.handleText(length, stream, xhtml);
            }
            else {
                l5.record(b2[0]);
                l5.record(b2[1]);
            }
        }
        else if (maybeLength > 0 && maybeLength < 2048) {
            this.handleText(maybeLength, stream, xhtml);
        }
    }
    
    private void handleText(final int length, final InputStream stream, final XHTMLContentHandler xhtml) throws IOException, SAXException, TikaException {
        final byte[] str = new byte[length];
        IOUtils.readFully(stream, str);
        if (str[length - 1] != 0) {
            return;
        }
        final String text = this.extractText(str, false);
        xhtml.startElement("p");
        xhtml.characters(text);
        xhtml.endElement("p");
    }
    
    private String extractText(final byte[] data, final boolean trim) throws TikaException {
        int length = data.length - 1;
        if (trim) {
            for (int i = 0; i < data.length; ++i) {
                if (data[i] == 0) {
                    length = i;
                    break;
                }
            }
        }
        String text;
        try {
            text = new String(data, 0, length, "cp437");
        }
        catch (UnsupportedEncodingException e) {
            throw new TikaException("JVM Broken, core codepage CP437 missing!");
        }
        text = text.replace("\u03c6", "\u00d8");
        return text;
    }
    
    static {
        SUPPORTED_TYPES = Collections.singleton(MediaType.application("x-prt"));
    }
    
    private static class Last5
    {
        byte[] data;
        int pos;
        
        private Last5() {
            this.data = new byte[5];
            this.pos = 0;
        }
        
        private void record(final int b) {
            this.data[this.pos] = (byte)b;
            ++this.pos;
            if (this.pos >= this.data.length) {
                this.pos = 0;
            }
        }
        
        private byte[] get() {
            final byte[] ret = new byte[5];
            for (int i = 0; i < ret.length; ++i) {
                int p = this.pos - i;
                if (p < 0) {
                    p += ret.length;
                }
                ret[i] = this.data[p];
            }
            return ret;
        }
        
        private boolean is33() {
            final byte[] arr$;
            final byte[] last5 = arr$ = this.get();
            for (final byte b : arr$) {
                if (b != 51) {
                    return false;
                }
            }
            return true;
        }
        
        private boolean is00() {
            final byte[] arr$;
            final byte[] last5 = arr$ = this.get();
            for (final byte b : arr$) {
                if (b != 0) {
                    return false;
                }
            }
            return true;
        }
    }
}
