// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.parser;

import java.util.Iterator;
import org.apache.tika.mime.MediaType;
import java.util.Map;
import org.apache.tika.mime.MediaTypeRegistry;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import org.apache.tika.config.ServiceLoader;

public class DefaultParser extends CompositeParser
{
    private static final long serialVersionUID = 3612324825403757520L;
    private final transient ServiceLoader loader;
    
    private static List<Parser> getDefaultParsers(final ServiceLoader loader) {
        final List<Parser> parsers = loader.loadStaticServiceProviders(Parser.class);
        Collections.sort(parsers, new Comparator<Parser>() {
            public int compare(final Parser p1, final Parser p2) {
                final String n1 = p1.getClass().getName();
                final String n2 = p2.getClass().getName();
                final boolean t1 = n1.startsWith("org.apache.tika.");
                final boolean t2 = n2.startsWith("org.apache.tika.");
                if (t1 == t2) {
                    return n1.compareTo(n2);
                }
                if (t1) {
                    return -1;
                }
                return 1;
            }
        });
        return parsers;
    }
    
    public DefaultParser(final MediaTypeRegistry registry, final ServiceLoader loader) {
        super(registry, getDefaultParsers(loader));
        this.loader = loader;
    }
    
    public DefaultParser(final MediaTypeRegistry registry, final ClassLoader loader) {
        this(registry, new ServiceLoader(loader));
    }
    
    public DefaultParser(final ClassLoader loader) {
        this(MediaTypeRegistry.getDefaultRegistry(), new ServiceLoader(loader));
    }
    
    public DefaultParser(final MediaTypeRegistry registry) {
        this(registry, new ServiceLoader());
    }
    
    public DefaultParser() {
        this(MediaTypeRegistry.getDefaultRegistry());
    }
    
    @Override
    public Map<MediaType, Parser> getParsers(final ParseContext context) {
        final Map<MediaType, Parser> map = super.getParsers(context);
        if (this.loader != null) {
            final MediaTypeRegistry registry = this.getMediaTypeRegistry();
            for (final Parser parser : this.loader.loadDynamicServiceProviders(Parser.class)) {
                for (final MediaType type : parser.getSupportedTypes(context)) {
                    map.put(registry.normalize(type), parser);
                }
            }
        }
        return map;
    }
}
