// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.parser.pdf;

import java.util.Collections;
import org.apache.pdfbox.cos.COSString;
import org.apache.pdfbox.cos.COSArray;
import org.apache.pdfbox.cos.COSBase;
import org.apache.tika.metadata.Property;
import java.util.List;
import java.util.Calendar;
import org.apache.pdfbox.pdmodel.PDDocumentInformation;
import org.apache.pdfbox.cos.COSName;
import java.util.Arrays;
import org.apache.tika.metadata.TikaCoreProperties;
import org.apache.tika.metadata.PagedText;
import org.apache.pdfbox.pdmodel.common.filespecification.PDEmbeddedFile;
import java.util.Iterator;
import org.apache.pdfbox.pdmodel.PDEmbeddedFilesNameTreeNode;
import org.apache.pdfbox.pdmodel.PDDocumentNameDictionary;
import org.apache.pdfbox.pdmodel.PDDocumentCatalog;
import org.apache.tika.sax.EmbeddedContentHandler;
import org.apache.pdfbox.pdmodel.common.filespecification.PDComplexFileSpecification;
import java.util.Map;
import org.apache.tika.extractor.ParsingEmbeddedDocumentExtractor;
import org.apache.tika.extractor.EmbeddedDocumentExtractor;
import org.apache.tika.exception.TikaException;
import org.xml.sax.SAXException;
import java.io.IOException;
import org.apache.pdfbox.io.RandomAccess;
import org.apache.tika.parser.PasswordProvider;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.tika.io.CloseShieldInputStream;
import org.apache.pdfbox.io.RandomAccessFile;
import org.apache.tika.io.TikaInputStream;
import org.apache.tika.io.TemporaryResources;
import org.apache.tika.metadata.Metadata;
import org.xml.sax.ContentHandler;
import java.io.InputStream;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.mime.MediaType;
import java.util.Set;
import org.apache.tika.parser.AbstractParser;

public class PDFParser extends AbstractParser
{
    private static final long serialVersionUID = -752276948656079347L;
    private boolean enableAutoSpace;
    private boolean suppressDuplicateOverlappingText;
    private boolean extractAnnotationText;
    private boolean sortByPosition;
    @Deprecated
    public static final String PASSWORD = "org.apache.tika.parser.pdf.password";
    private static final Set<MediaType> SUPPORTED_TYPES;
    
    public PDFParser() {
        this.enableAutoSpace = true;
        this.extractAnnotationText = true;
        this.sortByPosition = false;
    }
    
    public Set<MediaType> getSupportedTypes(final ParseContext context) {
        return PDFParser.SUPPORTED_TYPES;
    }
    
    public void parse(final InputStream stream, final ContentHandler handler, final Metadata metadata, final ParseContext context) throws IOException, SAXException, TikaException {
        PDDocument pdfDocument = null;
        final TemporaryResources tmp = new TemporaryResources();
        try {
            final TikaInputStream tstream = TikaInputStream.cast(stream);
            if (tstream != null && tstream.hasFile()) {
                final RandomAccess scratchFile = new RandomAccessFile(tmp.createTemporaryFile(), "rw");
                pdfDocument = PDDocument.load(new CloseShieldInputStream(stream), scratchFile, true);
            }
            else {
                pdfDocument = PDDocument.load(new CloseShieldInputStream(stream), true);
            }
            if (pdfDocument.isEncrypted()) {
                String password = null;
                final PasswordProvider passwordProvider = context.get(PasswordProvider.class);
                if (passwordProvider != null) {
                    password = passwordProvider.getPassword(metadata);
                }
                if (password == null && metadata.get("org.apache.tika.parser.pdf.password") != null) {
                    password = metadata.get("org.apache.tika.parser.pdf.password");
                }
                if (password == null) {
                    password = "";
                }
                try {
                    pdfDocument.decrypt(password);
                }
                catch (Exception ex) {}
            }
            metadata.set("Content-Type", "application/pdf");
            this.extractMetadata(pdfDocument, metadata);
            PDF2XHTML.process(pdfDocument, handler, metadata, this.extractAnnotationText, this.enableAutoSpace, this.suppressDuplicateOverlappingText, this.sortByPosition);
            this.extractEmbeddedDocuments(context, pdfDocument, handler);
        }
        finally {
            if (pdfDocument != null) {
                pdfDocument.close();
            }
            tmp.dispose();
        }
    }
    
    private void extractEmbeddedDocuments(final ParseContext context, final PDDocument document, final ContentHandler handler) throws IOException, SAXException, TikaException {
        final PDDocumentCatalog catalog = document.getDocumentCatalog();
        final PDDocumentNameDictionary names = catalog.getNames();
        if (names != null) {
            final PDEmbeddedFilesNameTreeNode embeddedFiles = names.getEmbeddedFiles();
            if (embeddedFiles != null) {
                EmbeddedDocumentExtractor embeddedExtractor = context.get(EmbeddedDocumentExtractor.class);
                if (embeddedExtractor == null) {
                    embeddedExtractor = new ParsingEmbeddedDocumentExtractor(context);
                }
                final Map<String, Object> embeddedFileNames = embeddedFiles.getNames();
                if (embeddedFileNames != null) {
                    for (final Map.Entry<String, Object> ent : embeddedFileNames.entrySet()) {
                        final PDComplexFileSpecification spec = ent.getValue();
                        final PDEmbeddedFile file = spec.getEmbeddedFile();
                        final Metadata metadata = new Metadata();
                        metadata.set("resourceName", ent.getKey());
                        metadata.set("Content-Type", file.getSubtype());
                        metadata.set("Content-Length", Long.toString(file.getSize()));
                        if (embeddedExtractor.shouldParseEmbedded(metadata)) {
                            final TikaInputStream stream = TikaInputStream.get(file.createInputStream());
                            try {
                                embeddedExtractor.parseEmbedded(stream, new EmbeddedContentHandler(handler), metadata, false);
                            }
                            finally {
                                stream.close();
                            }
                        }
                    }
                }
            }
        }
    }
    
    private void extractMetadata(final PDDocument document, final Metadata metadata) throws TikaException {
        final PDDocumentInformation info = document.getDocumentInformation();
        metadata.set(PagedText.N_PAGES, document.getNumberOfPages());
        this.addMetadata(metadata, TikaCoreProperties.TITLE, info.getTitle());
        this.addMetadata(metadata, TikaCoreProperties.CREATOR, info.getAuthor());
        this.addMetadata(metadata, TikaCoreProperties.CREATOR_TOOL, info.getCreator());
        this.addMetadata(metadata, TikaCoreProperties.KEYWORDS, info.getKeywords());
        this.addMetadata(metadata, "producer", info.getProducer());
        this.addMetadata(metadata, TikaCoreProperties.TRANSITION_SUBJECT_TO_OO_SUBJECT, info.getSubject());
        this.addMetadata(metadata, "trapped", info.getTrapped());
        try {
            this.addMetadata(metadata, "created", info.getCreationDate());
            this.addMetadata(metadata, TikaCoreProperties.CREATED, info.getCreationDate());
        }
        catch (IOException ex) {}
        try {
            final Calendar modified = info.getModificationDate();
            this.addMetadata(metadata, Metadata.LAST_MODIFIED, modified);
            this.addMetadata(metadata, TikaCoreProperties.MODIFIED, modified);
        }
        catch (IOException ex2) {}
        final List<String> handledMetadata = Arrays.asList("Author", "Creator", "CreationDate", "ModDate", "Keywords", "Producer", "Subject", "Title", "Trapped");
        for (final COSName key : info.getDictionary().keySet()) {
            final String name = key.getName();
            if (!handledMetadata.contains(name)) {
                this.addMetadata(metadata, name, info.getDictionary().getDictionaryObject(key));
            }
        }
    }
    
    private void addMetadata(final Metadata metadata, final Property property, final String value) {
        if (value != null) {
            metadata.add(property, value);
        }
    }
    
    private void addMetadata(final Metadata metadata, final String name, final String value) {
        if (value != null) {
            metadata.add(name, value);
        }
    }
    
    private void addMetadata(final Metadata metadata, final String name, final Calendar value) {
        if (value != null) {
            metadata.set(name, value.getTime().toString());
        }
    }
    
    private void addMetadata(final Metadata metadata, final Property property, final Calendar value) {
        if (value != null) {
            metadata.set(property, value.getTime());
        }
    }
    
    private void addMetadata(final Metadata metadata, final String name, final COSBase value) {
        if (value instanceof COSArray) {
            for (final COSBase v : ((COSArray)value).toList()) {
                this.addMetadata(metadata, name, v);
            }
        }
        else if (value instanceof COSString) {
            this.addMetadata(metadata, name, ((COSString)value).getString());
        }
        else {
            this.addMetadata(metadata, name, value.toString());
        }
    }
    
    public void setEnableAutoSpace(final boolean v) {
        this.enableAutoSpace = v;
    }
    
    public boolean getEnableAutoSpace() {
        return this.enableAutoSpace;
    }
    
    public void setExtractAnnotationText(final boolean v) {
        this.extractAnnotationText = v;
    }
    
    public boolean getExtractAnnotationText() {
        return this.extractAnnotationText;
    }
    
    public void setSuppressDuplicateOverlappingText(final boolean v) {
        this.suppressDuplicateOverlappingText = v;
    }
    
    public boolean getSuppressDuplicateOverlappingText() {
        return this.suppressDuplicateOverlappingText;
    }
    
    public void setSortByPosition(final boolean v) {
        this.sortByPosition = v;
    }
    
    public boolean getSortByPosition() {
        return this.sortByPosition;
    }
    
    static {
        SUPPORTED_TYPES = Collections.singleton(MediaType.application("pdf"));
    }
}
