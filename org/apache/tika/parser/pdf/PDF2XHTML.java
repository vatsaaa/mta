// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.parser.pdf;

import org.apache.pdfbox.util.TextPosition;
import org.apache.pdfbox.pdmodel.interactive.action.type.PDAction;
import java.util.Iterator;
import org.apache.pdfbox.pdmodel.interactive.annotation.PDAnnotationMarkup;
import org.apache.pdfbox.pdmodel.interactive.annotation.PDAnnotation;
import org.apache.pdfbox.pdmodel.interactive.action.type.PDActionURI;
import org.apache.pdfbox.pdmodel.interactive.annotation.PDAnnotationLink;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.tika.io.IOExceptionWithCause;
import java.io.IOException;
import org.apache.tika.exception.TikaException;
import org.xml.sax.SAXException;
import java.io.Writer;
import org.apache.tika.metadata.Metadata;
import org.xml.sax.ContentHandler;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.tika.sax.XHTMLContentHandler;
import org.apache.pdfbox.util.PDFTextStripper;

class PDF2XHTML extends PDFTextStripper
{
    private boolean inParagraph;
    private final XHTMLContentHandler handler;
    private final boolean extractAnnotationText;
    
    public static void process(final PDDocument document, final ContentHandler handler, final Metadata metadata, final boolean extractAnnotationText, final boolean enableAutoSpace, final boolean suppressDuplicateOverlappingText, final boolean sortByPosition) throws SAXException, TikaException {
        try {
            new PDF2XHTML(handler, metadata, extractAnnotationText, enableAutoSpace, suppressDuplicateOverlappingText, sortByPosition).writeText(document, new Writer() {
                @Override
                public void write(final char[] cbuf, final int off, final int len) {
                }
                
                @Override
                public void flush() {
                }
                
                @Override
                public void close() {
                }
            });
        }
        catch (IOException e) {
            if (e.getCause() instanceof SAXException) {
                throw (SAXException)e.getCause();
            }
            throw new TikaException("Unable to extract PDF content", e);
        }
    }
    
    private PDF2XHTML(final ContentHandler handler, final Metadata metadata, final boolean extractAnnotationText, final boolean enableAutoSpace, final boolean suppressDuplicateOverlappingText, final boolean sortByPosition) throws IOException {
        this.inParagraph = false;
        this.handler = new XHTMLContentHandler(handler, metadata);
        this.extractAnnotationText = extractAnnotationText;
        this.setForceParsing(true);
        this.setSortByPosition(sortByPosition);
        if (enableAutoSpace) {
            this.setWordSeparator(" ");
        }
        else {
            this.setWordSeparator("");
        }
        this.setSuppressDuplicateOverlappingText(suppressDuplicateOverlappingText);
    }
    
    @Override
    protected void startDocument(final PDDocument pdf) throws IOException {
        try {
            this.handler.startDocument();
        }
        catch (SAXException e) {
            throw new IOExceptionWithCause("Unable to start a document", e);
        }
    }
    
    @Override
    protected void endDocument(final PDDocument pdf) throws IOException {
        try {
            this.handler.endDocument();
        }
        catch (SAXException e) {
            throw new IOExceptionWithCause("Unable to end a document", e);
        }
    }
    
    @Override
    protected void startPage(final PDPage page) throws IOException {
        try {
            this.handler.startElement("div", "class", "page");
        }
        catch (SAXException e) {
            throw new IOExceptionWithCause("Unable to start a page", e);
        }
        this.writeParagraphStart();
    }
    
    @Override
    protected void endPage(final PDPage page) throws IOException {
        try {
            this.writeParagraphEnd();
            if (this.extractAnnotationText) {
                for (final Object o : page.getAnnotations()) {
                    if (o instanceof PDAnnotationLink) {
                        final PDAnnotationLink annotationlink = (PDAnnotationLink)o;
                        if (annotationlink.getAction() != null) {
                            final PDAction action = annotationlink.getAction();
                            if (action instanceof PDActionURI) {
                                final PDActionURI uri = (PDActionURI)action;
                                final String link = uri.getURI();
                                if (link != null) {
                                    this.handler.startElement("div", "class", "annotation");
                                    this.handler.startElement("a", "href", link);
                                    this.handler.endElement("a");
                                    this.handler.endElement("div");
                                }
                            }
                        }
                    }
                    if (o instanceof PDAnnotation && "FreeText".equals(((PDAnnotation)o).getSubtype())) {
                        final PDAnnotationMarkup annot = (PDAnnotationMarkup)o;
                        final String title = annot.getTitlePopup();
                        final String subject = annot.getTitlePopup();
                        final String contents = annot.getContents();
                        if (title == null && subject == null && contents == null) {
                            continue;
                        }
                        this.handler.startElement("div", "class", "annotation");
                        if (title != null) {
                            this.handler.startElement("div", "class", "annotationTitle");
                            this.handler.characters(title);
                            this.handler.endElement("div");
                        }
                        if (subject != null) {
                            this.handler.startElement("div", "class", "annotationSubject");
                            this.handler.characters(subject);
                            this.handler.endElement("div");
                        }
                        if (contents != null) {
                            this.handler.startElement("div", "class", "annotationContents");
                            this.handler.characters(contents);
                            this.handler.endElement("div");
                        }
                        this.handler.endElement("div");
                    }
                }
            }
            this.handler.endElement("div");
        }
        catch (SAXException e) {
            throw new IOExceptionWithCause("Unable to end a page", e);
        }
    }
    
    @Override
    protected void writeParagraphStart() throws IOException {
        if (this.inParagraph) {
            this.writeParagraphEnd();
        }
        assert !this.inParagraph;
        this.inParagraph = true;
        try {
            this.handler.startElement("p");
        }
        catch (SAXException e) {
            throw new IOExceptionWithCause("Unable to start a paragraph", e);
        }
    }
    
    @Override
    protected void writeParagraphEnd() throws IOException {
        if (!this.inParagraph) {
            this.writeParagraphStart();
        }
        assert this.inParagraph;
        this.inParagraph = false;
        try {
            this.handler.endElement("p");
        }
        catch (SAXException e) {
            throw new IOExceptionWithCause("Unable to end a paragraph", e);
        }
    }
    
    @Override
    protected void writeString(final String text) throws IOException {
        try {
            this.handler.characters(text);
        }
        catch (SAXException e) {
            throw new IOExceptionWithCause("Unable to write a string: " + text, e);
        }
    }
    
    @Override
    protected void writeCharacters(final TextPosition text) throws IOException {
        try {
            this.handler.characters(text.getCharacter());
        }
        catch (SAXException e) {
            throw new IOExceptionWithCause("Unable to write a character: " + text.getCharacter(), e);
        }
    }
    
    @Override
    protected void writeWordSeparator() throws IOException {
        try {
            this.handler.characters(this.getWordSeparator());
        }
        catch (SAXException e) {
            throw new IOExceptionWithCause("Unable to write a space character", e);
        }
    }
    
    @Override
    protected void writeLineSeparator() throws IOException {
        try {
            this.handler.characters("\n");
        }
        catch (SAXException e) {
            throw new IOExceptionWithCause("Unable to write a newline character", e);
        }
    }
}
