// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.parser;

import org.apache.tika.exception.TikaException;
import org.xml.sax.SAXException;
import java.io.IOException;
import org.apache.tika.metadata.Metadata;
import org.xml.sax.ContentHandler;
import java.io.InputStream;
import org.apache.tika.mime.MediaType;
import java.util.Set;

public class ParserDecorator extends AbstractParser
{
    private static final long serialVersionUID = -3861669115439125268L;
    private final Parser parser;
    
    public static final Parser withTypes(final Parser parser, final Set<MediaType> types) {
        return new ParserDecorator(parser) {
            private static final long serialVersionUID = -7345051519565330731L;
            
            @Override
            public Set<MediaType> getSupportedTypes(final ParseContext context) {
                return types;
            }
        };
    }
    
    public ParserDecorator(final Parser parser) {
        this.parser = parser;
    }
    
    public Set<MediaType> getSupportedTypes(final ParseContext context) {
        return this.parser.getSupportedTypes(context);
    }
    
    public void parse(final InputStream stream, final ContentHandler handler, final Metadata metadata, final ParseContext context) throws IOException, SAXException, TikaException {
        this.parser.parse(stream, handler, metadata, context);
    }
    
    public Parser getWrappedParser() {
        return this.parser;
    }
}
