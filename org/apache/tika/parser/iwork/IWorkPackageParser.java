// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.parser.iwork;

import javax.xml.namespace.QName;
import org.apache.commons.compress.archivers.zip.UnsupportedZipFeatureException;
import org.apache.tika.detect.XmlRootExtractor;
import org.apache.commons.compress.archivers.zip.ZipFile;
import java.util.Collections;
import java.util.Collection;
import java.util.HashSet;
import java.util.Arrays;
import org.xml.sax.SAXException;
import java.io.IOException;
import org.apache.commons.compress.archivers.zip.ZipArchiveEntry;
import org.xml.sax.helpers.DefaultHandler;
import org.apache.tika.sax.OfflineContentHandler;
import org.apache.tika.io.CloseShieldInputStream;
import org.apache.tika.exception.TikaException;
import org.apache.tika.sax.XHTMLContentHandler;
import java.io.BufferedInputStream;
import org.apache.commons.compress.archivers.zip.ZipArchiveInputStream;
import org.apache.tika.metadata.Metadata;
import org.xml.sax.ContentHandler;
import java.io.InputStream;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.mime.MediaType;
import java.util.Set;
import org.apache.tika.parser.AbstractParser;

public class IWorkPackageParser extends AbstractParser
{
    private static final long serialVersionUID = -2160322853809682372L;
    public static final Set<String> IWORK_CONTENT_ENTRIES;
    public static final String IWORK_COMMON_ENTRY = "buildVersionHistory.plist";
    private static final Set<MediaType> supportedTypes;
    
    public Set<MediaType> getSupportedTypes(final ParseContext context) {
        return IWorkPackageParser.supportedTypes;
    }
    
    public void parse(final InputStream stream, final ContentHandler handler, final Metadata metadata, final ParseContext context) throws IOException, SAXException, TikaException {
        final ZipArchiveInputStream zip = new ZipArchiveInputStream(stream);
        for (ZipArchiveEntry entry = zip.getNextZipEntry(); entry != null; entry = zip.getNextZipEntry()) {
            if (!(!IWorkPackageParser.IWORK_CONTENT_ENTRIES.contains(entry.getName()))) {
                final InputStream entryStream = new BufferedInputStream(zip, 4096);
                entryStream.mark(4096);
                final IWORKDocumentType type = detectType(entryStream);
                entryStream.reset();
                if (type != null) {
                    final XHTMLContentHandler xhtml = new XHTMLContentHandler(handler, metadata);
                    ContentHandler contentHandler = null;
                    switch (type) {
                        case KEYNOTE: {
                            contentHandler = new KeynoteContentHandler(xhtml, metadata);
                            break;
                        }
                        case NUMBERS: {
                            contentHandler = new NumbersContentHandler(xhtml, metadata);
                            break;
                        }
                        case PAGES: {
                            contentHandler = new PagesContentHandler(xhtml, metadata);
                            break;
                        }
                        case ENCRYPTED: {
                            contentHandler = null;
                            break;
                        }
                        default: {
                            throw new TikaException("Unhandled iWorks file " + type);
                        }
                    }
                    metadata.add("Content-Type", type.getType().toString());
                    xhtml.startDocument();
                    if (contentHandler != null) {
                        context.getSAXParser().parse(new CloseShieldInputStream(entryStream), new OfflineContentHandler(contentHandler));
                    }
                    xhtml.endDocument();
                }
            }
        }
        zip.close();
    }
    
    static {
        IWORK_CONTENT_ENTRIES = Collections.unmodifiableSet((Set<? extends String>)new HashSet<String>(Arrays.asList("index.apxl", "index.xml", "presentation.apxl")));
        supportedTypes = Collections.unmodifiableSet((Set<? extends MediaType>)new HashSet<MediaType>(Arrays.asList(MediaType.application("vnd.apple.iwork"), IWORKDocumentType.KEYNOTE.getType(), IWORKDocumentType.NUMBERS.getType(), IWORKDocumentType.PAGES.getType())));
    }
    
    public enum IWORKDocumentType
    {
        KEYNOTE("http://developer.apple.com/namespaces/keynote2", "presentation", MediaType.application("vnd.apple.keynote")), 
        NUMBERS("http://developer.apple.com/namespaces/ls", "document", MediaType.application("vnd.apple.numbers")), 
        PAGES("http://developer.apple.com/namespaces/sl", "document", MediaType.application("vnd.apple.pages")), 
        ENCRYPTED((String)null, (String)null, MediaType.application("x-tika-iworks-protected"));
        
        private final String namespace;
        private final String part;
        private final MediaType type;
        
        private IWORKDocumentType(final String namespace, final String part, final MediaType type) {
            this.namespace = namespace;
            this.part = part;
            this.type = type;
        }
        
        public String getNamespace() {
            return this.namespace;
        }
        
        public String getPart() {
            return this.part;
        }
        
        public MediaType getType() {
            return this.type;
        }
        
        public static IWORKDocumentType detectType(final ZipArchiveEntry entry, final ZipFile zip) {
            try {
                if (entry == null) {
                    return null;
                }
                final InputStream stream = zip.getInputStream(entry);
                try {
                    return detectType(stream);
                }
                finally {
                    stream.close();
                }
            }
            catch (IOException e) {
                return null;
            }
        }
        
        public static IWORKDocumentType detectType(final ZipArchiveEntry entry, final ZipArchiveInputStream zip) {
            if (entry == null) {
                return null;
            }
            return detectType(zip);
        }
        
        private static IWORKDocumentType detectType(final InputStream stream) {
            final QName qname = new XmlRootExtractor().extractRootElement(stream);
            if (qname != null) {
                final String uri = qname.getNamespaceURI();
                final String local = qname.getLocalPart();
                for (final IWORKDocumentType type : values()) {
                    if (type.getNamespace().equals(uri) && type.getPart().equals(local)) {
                        return type;
                    }
                }
            }
            else {
                try {
                    stream.read();
                }
                catch (UnsupportedZipFeatureException e) {
                    return IWORKDocumentType.ENCRYPTED;
                }
                catch (Exception ex) {}
            }
            return null;
        }
    }
}
