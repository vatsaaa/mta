// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.parser.iwork;

import org.apache.tika.metadata.TikaCoreProperties;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import java.util.HashMap;
import java.util.Map;
import org.apache.tika.metadata.Property;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.sax.XHTMLContentHandler;
import org.xml.sax.helpers.DefaultHandler;

class NumbersContentHandler extends DefaultHandler
{
    private final XHTMLContentHandler xhtml;
    private final Metadata metadata;
    private boolean inSheet;
    private boolean inText;
    private boolean parseText;
    private boolean inMetadata;
    private Property metadataKey;
    private String metadataPropertyQName;
    private boolean inTable;
    private int numberOfSheets;
    private int numberOfColumns;
    private int currentColumn;
    private Map<String, String> menuItems;
    private String currentMenuItemId;
    
    NumbersContentHandler(final XHTMLContentHandler xhtml, final Metadata metadata) {
        this.inSheet = false;
        this.inText = false;
        this.parseText = false;
        this.inMetadata = false;
        this.inTable = false;
        this.numberOfSheets = 0;
        this.numberOfColumns = -1;
        this.currentColumn = 0;
        this.menuItems = new HashMap<String, String>();
        this.xhtml = xhtml;
        this.metadata = metadata;
    }
    
    @Override
    public void endDocument() throws SAXException {
        this.metadata.set(Metadata.PAGE_COUNT, String.valueOf(this.numberOfSheets));
    }
    
    @Override
    public void startElement(final String uri, final String localName, final String qName, final Attributes attributes) throws SAXException {
        if ("ls:workspace".equals(qName)) {
            this.inSheet = true;
            ++this.numberOfSheets;
            this.xhtml.startElement("div");
            final String sheetName = attributes.getValue("ls:workspace-name");
            this.metadata.add("sheetNames", sheetName);
        }
        if ("sf:text".equals(qName)) {
            this.inText = true;
            this.xhtml.startElement("p");
        }
        if ("sf:p".equals(qName)) {
            this.parseText = true;
        }
        if ("sf:metadata".equals(qName)) {
            this.inMetadata = true;
            return;
        }
        if (this.inMetadata && this.metadataKey == null) {
            this.metadataKey = this.resolveMetadataKey(localName);
            this.metadataPropertyQName = qName;
        }
        if (this.inMetadata && this.metadataKey != null && "sf:string".equals(qName)) {
            this.metadata.add(this.metadataKey, attributes.getValue("sfa:string"));
        }
        if (!this.inSheet) {
            return;
        }
        if ("sf:tabular-model".equals(qName)) {
            final String tableName = attributes.getValue("sf:name");
            this.xhtml.startElement("div");
            this.xhtml.characters(tableName);
            this.xhtml.endElement("div");
            this.inTable = true;
            this.xhtml.startElement("table");
            this.xhtml.startElement("tr");
            this.currentColumn = 0;
        }
        if ("sf:menu-choices".equals(qName)) {
            this.menuItems = new HashMap<String, String>();
        }
        if (this.inTable && "sf:grid".equals(qName)) {
            this.numberOfColumns = Integer.parseInt(attributes.getValue("sf:numcols"));
        }
        if (this.menuItems != null && "sf:t".equals(qName)) {
            this.currentMenuItemId = attributes.getValue("sfa:ID");
        }
        if (this.currentMenuItemId != null && "sf:ct".equals(qName)) {
            this.menuItems.put(this.currentMenuItemId, attributes.getValue("sfa:s"));
        }
        if (this.inTable && "sf:ct".equals(qName)) {
            if (this.currentColumn >= this.numberOfColumns) {
                this.currentColumn = 0;
                this.xhtml.endElement("tr");
                this.xhtml.startElement("tr");
            }
            this.xhtml.element("td", attributes.getValue("sfa:s"));
            ++this.currentColumn;
        }
        if (this.inTable && ("sf:n".equals(qName) || "sf:rn".equals(qName))) {
            if (this.currentColumn >= this.numberOfColumns) {
                this.currentColumn = 0;
                this.xhtml.endElement("tr");
                this.xhtml.startElement("tr");
            }
            this.xhtml.element("td", attributes.getValue("sf:v"));
            ++this.currentColumn;
        }
        if (this.inTable && "sf:proxied-cell-ref".equals(qName)) {
            if (this.currentColumn >= this.numberOfColumns) {
                this.currentColumn = 0;
                this.xhtml.endElement("tr");
                this.xhtml.startElement("tr");
            }
            this.xhtml.element("td", this.menuItems.get(attributes.getValue("sfa:IDREF")));
            ++this.currentColumn;
        }
    }
    
    @Override
    public void characters(final char[] ch, final int start, final int length) throws SAXException {
        if (this.parseText && length > 0) {
            this.xhtml.characters(ch, start, length);
        }
    }
    
    @Override
    public void endElement(final String uri, final String localName, final String qName) throws SAXException {
        if ("ls:workspace".equals(qName)) {
            this.inSheet = false;
            this.xhtml.endElement("div");
        }
        if ("sf:text".equals(qName)) {
            this.inText = false;
            this.xhtml.endElement("p");
        }
        if ("sf:p".equals(qName)) {
            this.parseText = false;
        }
        if ("sf:metadata".equals(qName)) {
            this.inMetadata = false;
        }
        if (this.inMetadata && qName.equals(this.metadataPropertyQName)) {
            this.metadataPropertyQName = null;
            this.metadataKey = null;
        }
        if (!this.inSheet) {
            return;
        }
        if ("sf:menu-choices".equals(qName)) {}
        if ("sf:tabular-model".equals(qName)) {
            this.inTable = false;
            this.xhtml.endElement("tr");
            this.xhtml.endElement("table");
        }
        if (this.currentMenuItemId != null && "sf:t".equals(qName)) {
            this.currentMenuItemId = null;
        }
    }
    
    private Property resolveMetadataKey(final String localName) {
        if ("authors".equals(localName)) {
            return TikaCoreProperties.CREATOR;
        }
        if ("title".equals(localName)) {
            return TikaCoreProperties.TITLE;
        }
        if ("comment".equals(localName)) {
            return TikaCoreProperties.COMMENTS;
        }
        return Property.internalText(localName);
    }
}
