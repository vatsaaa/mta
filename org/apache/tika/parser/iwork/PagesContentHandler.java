// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.parser.iwork;

import org.apache.tika.metadata.TikaCoreProperties;
import java.util.Iterator;
import org.apache.tika.metadata.Property;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.sax.XHTMLContentHandler;
import org.xml.sax.helpers.DefaultHandler;

class PagesContentHandler extends DefaultHandler
{
    private final XHTMLContentHandler xhtml;
    private final Metadata metadata;
    private DocumentPart inPart;
    private boolean ghostText;
    private static String alphabet;
    private boolean parseProperty;
    private int pageCount;
    private int slPageCount;
    private HeaderFooter headers;
    private HeaderFooter footers;
    private Footnotes footnotes;
    private Annotations annotations;
    private Map<String, List<List<String>>> tableData;
    private String activeTableId;
    private int numberOfColumns;
    private List<String> activeRow;
    private String metaDataLocalName;
    private String metaDataQName;
    
    PagesContentHandler(final XHTMLContentHandler xhtml, final Metadata metadata) {
        this.inPart = null;
        this.parseProperty = false;
        this.pageCount = 0;
        this.slPageCount = 0;
        this.headers = null;
        this.footers = null;
        this.footnotes = null;
        this.annotations = null;
        this.tableData = new HashMap<String, List<List<String>>>();
        this.numberOfColumns = 0;
        this.activeRow = new ArrayList<String>();
        this.xhtml = xhtml;
        this.metadata = metadata;
    }
    
    @Override
    public void endDocument() throws SAXException {
        this.metadata.set(Metadata.PAGE_COUNT, String.valueOf(this.pageCount));
        if (this.pageCount > 0) {
            this.doFooter();
            this.xhtml.endElement("div");
        }
    }
    
    @Override
    public void startElement(final String uri, final String localName, final String qName, final Attributes attributes) throws SAXException {
        if (this.parseProperty) {
            final String value = this.parsePrimitiveElementValue(qName, attributes);
            if (value != null) {
                final Object metaDataKey = this.resolveMetaDataKey(this.metaDataLocalName);
                if (metaDataKey instanceof Property) {
                    this.metadata.set((Property)metaDataKey, value);
                }
                else {
                    this.metadata.add((String)metaDataKey, value);
                }
            }
        }
        if ("sl:publication-info".equals(qName)) {
            this.inPart = DocumentPart.METADATA;
        }
        else if ("sf:metadata".equals(qName)) {
            this.inPart = DocumentPart.METADATA;
        }
        else if ("sf:page-start".equals(qName) || "sl:page-group".equals(qName)) {
            if (this.pageCount > 0) {
                this.doFooter();
                this.xhtml.endElement("div");
            }
            this.xhtml.startElement("div");
            if ("sl:page-group".equals(qName)) {
                ++this.slPageCount;
            }
            else {
                ++this.pageCount;
            }
            this.doHeader();
        }
        else if ("sf:p".equals(qName)) {
            if (this.pageCount + this.slPageCount > 0) {
                this.inPart = DocumentPart.PARSABLE_TEXT;
                this.xhtml.startElement("p");
            }
        }
        else if ("sf:attachment".equals(qName)) {
            final String kind = attributes.getValue("sf:kind");
            if ("tabular-attachment".equals(kind)) {
                this.activeTableId = attributes.getValue("sfa:ID");
                this.tableData.put(this.activeTableId, new ArrayList<List<String>>());
            }
        }
        else if ("sf:attachment-ref".equals(qName)) {
            final String idRef = attributes.getValue("sfa:IDREF");
            this.outputTable(idRef);
        }
        else if ("sf:headers".equals(qName)) {
            this.headers = new HeaderFooter(qName);
            this.inPart = DocumentPart.HEADERS;
        }
        else if ("sf:footers".equals(qName)) {
            this.footers = new HeaderFooter(qName);
            this.inPart = DocumentPart.FOOTERS;
        }
        else if ("sf:header".equals(qName)) {
            this.inPart = this.headers.identifyPart(attributes.getValue("sf:name"));
        }
        else if ("sf:footer".equals(qName)) {
            this.inPart = this.footers.identifyPart(attributes.getValue("sf:name"));
        }
        else if ("sf:page-number".equals(qName)) {
            if (this.inPart == DocumentPart.FOOTER_ODD || this.inPart == DocumentPart.FOOTER_FIRST || this.inPart == DocumentPart.FOOTER_EVEN) {
                this.footers.hasAutoPageNumber = true;
                this.footers.autoPageNumberFormat = attributes.getValue("sf:format");
            }
            else {
                this.headers.hasAutoPageNumber = true;
                this.headers.autoPageNumberFormat = attributes.getValue("sf:format");
            }
            this.xhtml.characters(Integer.toString(this.pageCount));
        }
        else if ("sf:footnotes".equals(qName)) {
            this.footnotes = new Footnotes();
            this.inPart = DocumentPart.FOOTNOTES;
        }
        else if ("sf:footnote-mark".equals(qName)) {
            this.footnotes.recordMark(attributes.getValue("sf:mark"));
        }
        else if ("sf:footnote".equals(qName) && this.inPart == DocumentPart.PARSABLE_TEXT) {
            final String footnoteMark = attributes.getValue("sf:autonumber");
            if (this.footnotes != null) {
                final String footnoteText = this.footnotes.footnotes.get(footnoteMark);
                if (footnoteText != null) {
                    this.xhtml.startElement("div", "style", "footnote");
                    this.xhtml.characters("Footnote:");
                    this.xhtml.characters(footnoteText);
                    this.xhtml.endElement("div");
                }
            }
        }
        else if ("sf:annotations".equals(qName)) {
            this.annotations = new Annotations();
            this.inPart = DocumentPart.ANNOTATIONS;
        }
        else if ("sf:annotation".equals(qName) && this.inPart == DocumentPart.ANNOTATIONS) {
            this.annotations.start(attributes.getValue("sf:target"));
        }
        else if ("sf:annotation-field".equals(qName) && this.inPart == DocumentPart.PARSABLE_TEXT) {
            this.xhtml.startElement("div", "style", "annotated");
            final String annotationText = this.annotations.annotations.get(attributes.getValue("sfa:ID"));
            if (annotationText != null) {
                this.xhtml.startElement("div", "style", "annotation");
                this.xhtml.characters(annotationText);
                this.xhtml.endElement("div");
            }
        }
        else if ("sf:ghost-text".equals(qName)) {
            this.ghostText = true;
        }
        if (this.activeTableId != null) {
            this.parseTableData(qName, attributes);
        }
        if (this.inPart == DocumentPart.METADATA) {
            this.metaDataLocalName = localName;
            this.metaDataQName = qName;
            this.parseProperty = true;
        }
    }
    
    @Override
    public void endElement(final String uri, final String localName, final String qName) throws SAXException {
        if (this.metaDataLocalName != null && this.metaDataLocalName.equals(localName)) {
            this.metaDataLocalName = null;
            this.parseProperty = false;
        }
        if ("sl:publication-info".equals(qName)) {
            this.inPart = null;
        }
        else if ("sf:metadata".equals(qName)) {
            this.inPart = null;
        }
        else if ("sf:p".equals(qName) && this.pageCount + this.slPageCount > 0) {
            this.inPart = null;
            this.xhtml.endElement("p");
        }
        else if ("sf:attachment".equals(qName)) {
            this.activeTableId = null;
        }
        else if ("sf:annotation".equals(qName) && this.inPart == DocumentPart.ANNOTATIONS) {
            this.annotations.end();
        }
        else if ("sf:annotation-field".equals(qName) && this.inPart == DocumentPart.PARSABLE_TEXT) {
            this.xhtml.endElement("div");
        }
        else if ("sf:ghost-text".equals(qName)) {
            this.ghostText = false;
        }
    }
    
    @Override
    public void characters(final char[] ch, final int start, final int length) throws SAXException {
        if (length > 0) {
            if (this.inPart == DocumentPart.PARSABLE_TEXT) {
                if (!this.ghostText) {
                    this.xhtml.characters(ch, start, length);
                }
            }
            else if (this.inPart != null) {
                final String str = new String(ch, start, length);
                if (this.inPart == DocumentPart.HEADER_FIRST) {
                    this.headers.defaultFirst = str;
                }
                if (this.inPart == DocumentPart.HEADER_EVEN) {
                    this.headers.defaultEven = str;
                }
                if (this.inPart == DocumentPart.HEADER_ODD) {
                    this.headers.defaultOdd = str;
                }
                if (this.inPart == DocumentPart.FOOTER_FIRST) {
                    this.footers.defaultFirst = str;
                }
                if (this.inPart == DocumentPart.FOOTER_EVEN) {
                    this.footers.defaultEven = str;
                }
                if (this.inPart == DocumentPart.FOOTER_ODD) {
                    this.footers.defaultOdd = str;
                }
                if (this.inPart == DocumentPart.FOOTNOTES) {
                    this.footnotes.text(str);
                }
                if (this.inPart == DocumentPart.ANNOTATIONS) {
                    this.annotations.text(str);
                }
            }
        }
    }
    
    private void parseTableData(final String qName, final Attributes attributes) {
        if ("sf:grid".equals(qName)) {
            final String numberOfColumns = attributes.getValue("sf:numcols");
            this.numberOfColumns = Integer.parseInt(numberOfColumns);
        }
        else if ("sf:ct".equals(qName)) {
            this.activeRow.add(attributes.getValue("sfa:s"));
            if (this.activeRow.size() >= 3) {
                this.tableData.get(this.activeTableId).add(this.activeRow);
                this.activeRow = new ArrayList<String>();
            }
        }
    }
    
    private void outputTable(final String idRef) throws SAXException {
        final List<List<String>> tableData = this.tableData.get(idRef);
        if (tableData != null) {
            this.xhtml.startElement("table");
            for (final List<String> row : tableData) {
                this.xhtml.startElement("tr");
                for (final String cell : row) {
                    this.xhtml.element("td", cell);
                }
                this.xhtml.endElement("tr");
            }
            this.xhtml.endElement("table");
        }
    }
    
    private Object resolveMetaDataKey(final String metaDataLocalName) {
        Object metaDataKey = metaDataLocalName;
        if ("sf:authors".equals(this.metaDataQName)) {
            metaDataKey = TikaCoreProperties.CREATOR;
        }
        else if ("sf:title".equals(this.metaDataQName)) {
            metaDataKey = TikaCoreProperties.TITLE;
        }
        else if ("sl:SLCreationDateProperty".equals(this.metaDataQName)) {
            metaDataKey = TikaCoreProperties.CREATED;
        }
        else if ("sl:SLLastModifiedDateProperty".equals(this.metaDataQName)) {
            metaDataKey = Metadata.LAST_MODIFIED;
        }
        else if ("sl:language".equals(this.metaDataQName)) {
            metaDataKey = TikaCoreProperties.LANGUAGE;
        }
        return metaDataKey;
    }
    
    private String parsePrimitiveElementValue(final String qName, final Attributes attributes) {
        if ("sl:string".equals(qName) || "sf:string".equals(qName)) {
            return attributes.getValue("sfa:string");
        }
        if ("sl:number".equals(qName)) {
            return attributes.getValue("sfa:number");
        }
        if ("sl:date".equals(qName)) {
            return attributes.getValue("sf:val");
        }
        return null;
    }
    
    private void doHeader() throws SAXException {
        if (this.headers != null) {
            this.headers.output("header");
        }
    }
    
    private void doFooter() throws SAXException {
        if (this.footers != null) {
            this.footers.output("footer");
        }
    }
    
    static {
        PagesContentHandler.alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    }
    
    private enum DocumentPart
    {
        METADATA, 
        PARSABLE_TEXT, 
        HEADERS, 
        HEADER_ODD, 
        HEADER_EVEN, 
        HEADER_FIRST, 
        FOOTERS, 
        FOOTER_ODD, 
        FOOTER_EVEN, 
        FOOTER_FIRST, 
        FOOTNOTES, 
        ANNOTATIONS;
    }
    
    private class HeaderFooter
    {
        private String type;
        private String defaultOdd;
        private String defaultEven;
        private String defaultFirst;
        private boolean hasAutoPageNumber;
        private String autoPageNumberFormat;
        
        private HeaderFooter(final String type) {
            this.type = type;
        }
        
        private DocumentPart identifyPart(final String name) {
            if ("SFWPDefaultOddHeaderIdentifier".equals(name)) {
                return DocumentPart.HEADER_ODD;
            }
            if ("SFWPDefaultEvenHeaderIdentifier".equals(name)) {
                return DocumentPart.HEADER_EVEN;
            }
            if ("SFWPDefaultFirstHeaderIdentifier".equals(name)) {
                return DocumentPart.HEADER_FIRST;
            }
            if ("SFWPDefaultOddFooterIdentifier".equals(name)) {
                return DocumentPart.FOOTER_ODD;
            }
            if ("SFWPDefaultEvenFooterIdentifier".equals(name)) {
                return DocumentPart.FOOTER_EVEN;
            }
            if ("SFWPDefaultFirstFooterIdentifier".equals(name)) {
                return DocumentPart.FOOTER_FIRST;
            }
            return null;
        }
        
        private void output(final String what) throws SAXException {
            String text = null;
            if (PagesContentHandler.this.pageCount == 1 && this.defaultFirst != null) {
                text = this.defaultFirst;
            }
            else if (PagesContentHandler.this.pageCount % 2 == 0 && this.defaultEven != null) {
                text = this.defaultEven;
            }
            else {
                text = this.defaultOdd;
            }
            if (text != null) {
                PagesContentHandler.this.xhtml.startElement("div", "class", "header");
                PagesContentHandler.this.xhtml.characters(text);
                if (this.hasAutoPageNumber) {
                    if (this.autoPageNumberFormat == null) {
                        PagesContentHandler.this.xhtml.characters("\t" + PagesContentHandler.this.pageCount);
                    }
                    else if (this.autoPageNumberFormat.equals("upper-roman")) {
                        PagesContentHandler.this.xhtml.characters("\t" + AutoPageNumberUtils.asRomanNumerals(PagesContentHandler.this.pageCount));
                    }
                    else if (this.autoPageNumberFormat.equals("lower-roman")) {
                        PagesContentHandler.this.xhtml.characters("\t" + AutoPageNumberUtils.asRomanNumeralsLower(PagesContentHandler.this.pageCount));
                    }
                    else if (this.autoPageNumberFormat.equals("upper-alpha")) {
                        PagesContentHandler.this.xhtml.characters("\t" + AutoPageNumberUtils.asAlphaNumeric(PagesContentHandler.this.pageCount));
                    }
                    else if (this.autoPageNumberFormat.equals("lower-alpha")) {
                        PagesContentHandler.this.xhtml.characters("\t" + AutoPageNumberUtils.asAlphaNumericLower(PagesContentHandler.this.pageCount));
                    }
                }
                PagesContentHandler.this.xhtml.endElement("div");
            }
        }
    }
    
    private static class Footnotes
    {
        Map<String, String> footnotes;
        String lastSeenMark;
        
        private Footnotes() {
            this.footnotes = new HashMap<String, String>();
            this.lastSeenMark = null;
        }
        
        private void recordMark(final String mark) {
            this.lastSeenMark = mark;
        }
        
        private void text(String text) {
            if (this.lastSeenMark != null) {
                if (this.footnotes.containsKey(this.lastSeenMark)) {
                    text = this.footnotes.get(this.lastSeenMark) + text;
                }
                this.footnotes.put(this.lastSeenMark, text);
            }
        }
    }
    
    private class Annotations
    {
        Map<String, String> annotations;
        String currentID;
        StringBuffer currentText;
        
        private Annotations() {
            this.annotations = new HashMap<String, String>();
            this.currentID = null;
            this.currentText = null;
        }
        
        private void start(final String id) {
            this.currentID = id;
            this.currentText = new StringBuffer();
        }
        
        private void text(final String text) {
            if (text != null && text.length() > 0 && this.currentText != null) {
                this.currentText.append(text);
            }
        }
        
        private void end() {
            if (this.currentText.length() > 0) {
                this.annotations.put(this.currentID, this.currentText.toString());
                this.currentID = null;
                this.currentText = null;
            }
        }
    }
}
