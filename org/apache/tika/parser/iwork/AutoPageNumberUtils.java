// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.parser.iwork;

class AutoPageNumberUtils
{
    private static final String[] ALPHABET;
    private static final int MAX = 26;
    
    public static String asAlphaNumeric(final int i) {
        final StringBuffer sbuff = new StringBuffer();
        int index = i % 26;
        int ratio = i / 26;
        if (index == 0) {
            --ratio;
            index = 26;
        }
        for (int j = 0; j <= ratio; ++j) {
            sbuff.append(AutoPageNumberUtils.ALPHABET[index - 1]);
        }
        return sbuff.toString();
    }
    
    public static String asAlphaNumericLower(final int i) {
        return asAlphaNumeric(i).toLowerCase();
    }
    
    public static String asRomanNumerals(int i) {
        if (i <= 0) {
            throw new NumberFormatException("Roman numerals are 1-3999 (" + i + ")");
        }
        if (i > 3999) {
            throw new NumberFormatException("Roman numerals are 1-3999 (" + i + ")");
        }
        final StringBuffer sbuff = new StringBuffer();
        for (i = i2r(sbuff, i, "M", 1000, "CM", 900, "D", 500, "CD", 400), i = i2r(sbuff, i, "C", 100, "XC", 90, "L", 50, "XL", 40), i = i2r(sbuff, i, "X", 10, "IX", 9, "V", 5, "IV", 4); i >= 1; --i) {
            sbuff.append("I");
        }
        return sbuff.toString();
    }
    
    public static String asRomanNumeralsLower(final int i) {
        return asRomanNumerals(i).toLowerCase();
    }
    
    private static int i2r(final StringBuffer sbuff, int i, final String tens, final int iTens, final String nines, final int iNines, final String fives, final int iFives, final String fours, final int iFours) {
        while (i >= iTens) {
            sbuff.append(tens);
            i -= iTens;
        }
        if (i >= iNines) {
            sbuff.append(nines);
            i -= iNines;
        }
        if (i >= iFives) {
            sbuff.append(fives);
            i -= iFives;
        }
        if (i >= iFours) {
            sbuff.append(fours);
            i -= iFours;
        }
        return i;
    }
    
    static {
        ALPHABET = new String[] { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" };
    }
}
