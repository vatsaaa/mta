// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.parser.iwork;

import org.apache.tika.metadata.TikaCoreProperties;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.sax.XHTMLContentHandler;
import org.xml.sax.helpers.DefaultHandler;

class KeynoteContentHandler extends DefaultHandler
{
    public static final String PRESENTATION_WIDTH = "slides-width";
    public static final String PRESENTATION_HEIGHT = "slides-height";
    private final XHTMLContentHandler xhtml;
    private final Metadata metadata;
    private boolean inSlide;
    private boolean inTheme;
    private boolean inTitle;
    private boolean inBody;
    private String tableId;
    private Integer numberOfColumns;
    private Integer currentColumn;
    private boolean inMetadata;
    private boolean inMetaDataTitle;
    private boolean inMetaDataAuthors;
    private boolean inParsableText;
    private int numberOfSlides;
    
    KeynoteContentHandler(final XHTMLContentHandler xhtml, final Metadata metadata) {
        this.inSlide = false;
        this.inTheme = false;
        this.inTitle = false;
        this.inBody = false;
        this.numberOfColumns = null;
        this.currentColumn = null;
        this.inMetadata = false;
        this.inMetaDataTitle = false;
        this.inMetaDataAuthors = false;
        this.inParsableText = false;
        this.numberOfSlides = 0;
        this.xhtml = xhtml;
        this.metadata = metadata;
    }
    
    @Override
    public void endDocument() throws SAXException {
        this.metadata.set(Metadata.SLIDE_COUNT, String.valueOf(this.numberOfSlides));
    }
    
    @Override
    public void startElement(final String uri, final String localName, final String qName, final Attributes attributes) throws SAXException {
        if ("key:theme".equals(qName)) {
            this.inTheme = true;
        }
        else if ("key:slide".equals(qName)) {
            this.inSlide = true;
            ++this.numberOfSlides;
            this.xhtml.startElement("div");
        }
        else if ("key:master-slide".equals(qName)) {
            this.inSlide = true;
            this.xhtml.startElement("div");
        }
        else if ("key:title-placeholder".equals(qName) && this.inSlide) {
            this.inTitle = true;
            this.xhtml.startElement("h1");
        }
        else if ("sf:sticky-note".equals(qName) && this.inSlide) {
            this.xhtml.startElement("p");
        }
        else if ("key:notes".equals(qName) && this.inSlide) {
            this.xhtml.startElement("p");
        }
        else if ("key:body-placeholder".equals(qName) && this.inSlide) {
            this.xhtml.startElement("p");
            this.inBody = true;
        }
        else if ("key:size".equals(qName) && !this.inTheme) {
            final String width = attributes.getValue("sfa:w");
            final String height = attributes.getValue("sfa:h");
            this.metadata.set("slides-width", width);
            this.metadata.set("slides-height", height);
        }
        else if ("sf:text-body".equals(qName)) {
            this.inParsableText = true;
        }
        else if ("key:metadata".equals(qName)) {
            this.inMetadata = true;
        }
        else if (this.inMetadata && "key:title".equals(qName)) {
            this.inMetaDataTitle = true;
        }
        else if (this.inMetadata && "key:authors".equals(qName)) {
            this.inMetaDataAuthors = true;
        }
        else if (this.inMetaDataTitle && "key:string".equals(qName)) {
            this.metadata.set(TikaCoreProperties.TITLE, attributes.getValue("sfa:string"));
        }
        else if (this.inMetaDataAuthors && "key:string".equals(qName)) {
            this.metadata.add(TikaCoreProperties.CREATOR, attributes.getValue("sfa:string"));
        }
        else if (this.inSlide && "sf:tabular-model".equals(qName)) {
            this.tableId = attributes.getValue("sfa:ID");
            this.xhtml.startElement("table");
        }
        else if (this.tableId != null && "sf:columns".equals(qName)) {
            this.numberOfColumns = Integer.parseInt(attributes.getValue("sf:count"));
            this.currentColumn = 0;
        }
        else if (this.tableId != null && "sf:ct".equals(qName)) {
            this.parseTableData(attributes.getValue("sfa:s"));
        }
        else if (this.tableId != null && "sf:n".equals(qName)) {
            this.parseTableData(attributes.getValue("sf:v"));
        }
        else if ("sf:p".equals(qName)) {
            this.xhtml.startElement("p");
        }
    }
    
    @Override
    public void endElement(final String uri, final String localName, final String qName) throws SAXException {
        if ("key:theme".equals(qName)) {
            this.inTheme = false;
        }
        else if ("key:slide".equals(qName)) {
            this.inSlide = false;
            this.xhtml.endElement("div");
        }
        else if ("key:master-slide".equals(qName)) {
            this.inSlide = false;
            this.xhtml.endElement("div");
        }
        else if ("key:title-placeholder".equals(qName) && this.inSlide) {
            this.inTitle = false;
            this.xhtml.endElement("h1");
        }
        else if ("sf:sticky-note".equals(qName) && this.inSlide) {
            this.xhtml.endElement("p");
        }
        else if ("key:notes".equals(qName) && this.inSlide) {
            this.xhtml.endElement("p");
        }
        else if ("key:body-placeholder".equals(qName) && this.inSlide) {
            this.xhtml.endElement("p");
            this.inBody = false;
        }
        else if ("sf:text-body".equals(qName)) {
            this.inParsableText = false;
        }
        else if ("key:metadata".equals(qName)) {
            this.inMetadata = false;
        }
        else if (this.inMetadata && "key:title".equals(qName)) {
            this.inMetaDataTitle = false;
        }
        else if (this.inMetadata && "key:authors".equals(qName)) {
            this.inMetaDataAuthors = false;
        }
        else if (this.inSlide && "sf:tabular-model".equals(qName)) {
            this.xhtml.endElement("table");
            this.tableId = null;
            this.numberOfColumns = null;
            this.currentColumn = null;
        }
        else if ("sf:p".equals(qName)) {
            this.xhtml.endElement("p");
        }
    }
    
    @Override
    public void characters(final char[] ch, final int start, final int length) throws SAXException {
        if (this.inParsableText && this.inSlide && length != 0) {
            this.xhtml.characters(ch, start, length);
        }
    }
    
    private void parseTableData(final String value) throws SAXException {
        if (this.currentColumn == 0) {
            this.xhtml.startElement("tr");
        }
        this.xhtml.element("td", value);
        if (this.currentColumn.equals(this.numberOfColumns)) {
            this.xhtml.endElement("tr");
        }
    }
}
