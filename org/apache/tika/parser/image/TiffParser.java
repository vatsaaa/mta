// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.parser.image;

import java.util.Collections;
import org.apache.tika.exception.TikaException;
import org.xml.sax.SAXException;
import org.apache.tika.sax.XHTMLContentHandler;
import org.apache.tika.parser.image.xmp.JempboxExtractor;
import java.io.IOException;
import java.io.FilterInputStream;
import org.apache.tika.metadata.Metadata;
import org.xml.sax.ContentHandler;
import java.io.InputStream;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.mime.MediaType;
import java.util.Set;
import org.apache.tika.parser.AbstractParser;

public class TiffParser extends AbstractParser
{
    private static final long serialVersionUID = -3941143576535464926L;
    private static final Set<MediaType> SUPPORTED_TYPES;
    
    public Set<MediaType> getSupportedTypes(final ParseContext context) {
        return TiffParser.SUPPORTED_TYPES;
    }
    
    public void parse(final InputStream stream, final ContentHandler handler, final Metadata metadata, final ParseContext context) throws IOException, SAXException, TikaException {
        stream.mark(Integer.MAX_VALUE);
        final FilterInputStream first = new FilterInputStream(stream) {
            @Override
            public void close() throws IOException {
            }
        };
        new ImageMetadataExtractor(metadata).parseTiff(first);
        stream.reset();
        new JempboxExtractor(metadata).parse(stream);
        final XHTMLContentHandler xhtml = new XHTMLContentHandler(handler, metadata);
        xhtml.startDocument();
        xhtml.endDocument();
    }
    
    static {
        SUPPORTED_TYPES = Collections.singleton(MediaType.image("tiff"));
    }
}
