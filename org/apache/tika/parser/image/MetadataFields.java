// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.parser.image;

import org.apache.tika.metadata.Metadata;
import org.apache.tika.metadata.TikaCoreProperties;
import java.lang.reflect.Field;
import org.apache.tika.metadata.Property;
import java.lang.reflect.Modifier;
import java.util.HashSet;

public abstract class MetadataFields
{
    private static HashSet<String> known;
    
    private static void setKnownForClass(final Class<?> clazz) {
        final Field[] arr$;
        final Field[] fields = arr$ = clazz.getFields();
        for (final Field f : arr$) {
            final int mod = f.getModifiers();
            if (Modifier.isPublic(mod) && Modifier.isStatic(mod) && Modifier.isFinal(mod)) {
                final Class<?> c = f.getType();
                if (String.class.equals(c)) {
                    try {
                        final String p = (String)f.get(null);
                        if (p != null) {
                            MetadataFields.known.add(p);
                        }
                    }
                    catch (IllegalArgumentException e) {
                        e.printStackTrace();
                    }
                    catch (IllegalAccessException e2) {
                        e2.printStackTrace();
                    }
                }
                if (Property.class.isAssignableFrom(c)) {
                    try {
                        final Property p2 = (Property)f.get(null);
                        if (p2 != null) {
                            MetadataFields.known.add(p2.getName());
                        }
                    }
                    catch (IllegalArgumentException e) {
                        e.printStackTrace();
                    }
                    catch (IllegalAccessException e2) {
                        e2.printStackTrace();
                    }
                }
            }
        }
    }
    
    public static boolean isMetadataField(final String name) {
        return MetadataFields.known.contains(name);
    }
    
    public static boolean isMetadataField(final Property property) {
        return MetadataFields.known.contains(property.getName());
    }
    
    static {
        MetadataFields.known = new HashSet<String>();
        setKnownForClass(TikaCoreProperties.class);
        setKnownForClass(Metadata.class);
    }
}
