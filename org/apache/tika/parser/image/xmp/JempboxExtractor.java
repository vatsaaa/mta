// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.parser.image.xmp;

import java.util.List;
import org.apache.tika.exception.TikaException;
import java.util.Iterator;
import org.apache.jempbox.xmp.XMPSchemaDublinCore;
import java.io.Reader;
import java.io.IOException;
import org.apache.tika.metadata.TikaCoreProperties;
import org.apache.jempbox.xmp.XMPMetadata;
import org.xml.sax.InputSource;
import java.io.InputStreamReader;
import java.io.ByteArrayInputStream;
import java.io.OutputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import org.apache.tika.metadata.Metadata;

public class JempboxExtractor
{
    private XMPPacketScanner scanner;
    private Metadata metadata;
    private static final String DEFAULT_XMP_CHARSET = "UTF-8";
    
    public JempboxExtractor(final Metadata metadata) {
        this.scanner = new XMPPacketScanner();
        this.metadata = metadata;
    }
    
    public void parse(final InputStream file) throws IOException, TikaException {
        final ByteArrayOutputStream xmpraw = new ByteArrayOutputStream();
        if (!this.scanner.parse(file, xmpraw)) {
            return;
        }
        final Reader decoded = new InputStreamReader(new ByteArrayInputStream(xmpraw.toByteArray()), "UTF-8");
        try {
            final XMPMetadata xmp = XMPMetadata.load(new InputSource(decoded));
            final XMPSchemaDublinCore dc = xmp.getDublinCoreSchema();
            if (dc != null) {
                if (dc.getTitle() != null) {
                    this.metadata.set(TikaCoreProperties.TITLE, dc.getTitle());
                }
                if (dc.getDescription() != null) {
                    this.metadata.set(TikaCoreProperties.DESCRIPTION, dc.getDescription());
                }
                if (dc.getCreators() != null && dc.getCreators().size() > 0) {
                    this.metadata.set(TikaCoreProperties.CREATOR, this.joinCreators(dc.getCreators()));
                }
                if (dc.getSubjects() != null && dc.getSubjects().size() > 0) {
                    final Iterator<String> keywords = dc.getSubjects().iterator();
                    while (keywords.hasNext()) {
                        this.metadata.add(TikaCoreProperties.KEYWORDS, keywords.next());
                    }
                }
            }
        }
        catch (IOException ex) {}
    }
    
    protected String joinCreators(final List<String> creators) {
        if (creators == null || creators.size() == 0) {
            return "";
        }
        if (creators.size() == 1) {
            return creators.get(0);
        }
        final StringBuffer c = new StringBuffer();
        for (final String s : creators) {
            c.append(", ").append(s);
        }
        return c.substring(2);
    }
}
