// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.parser.image.xmp;

import java.io.UnsupportedEncodingException;
import java.io.IOException;
import java.io.BufferedInputStream;
import java.io.OutputStream;
import java.io.InputStream;

public class XMPPacketScanner
{
    private static final byte[] PACKET_HEADER;
    private static final byte[] PACKET_HEADER_END;
    private static final byte[] PACKET_TRAILER;
    
    public boolean parse(InputStream in, final OutputStream xmlOut) throws IOException {
        if (!in.markSupported()) {
            in = new BufferedInputStream(in);
        }
        final boolean foundXMP = skipAfter(in, XMPPacketScanner.PACKET_HEADER);
        if (!foundXMP) {
            return false;
        }
        if (!skipAfter(in, XMPPacketScanner.PACKET_HEADER_END)) {
            throw new IOException("Invalid XMP packet header!");
        }
        if (!skipAfter(in, XMPPacketScanner.PACKET_TRAILER, xmlOut)) {
            throw new IOException("XMP packet not properly terminated!");
        }
        return true;
    }
    
    private static boolean skipAfter(final InputStream in, final byte[] match) throws IOException {
        return skipAfter(in, match, null);
    }
    
    private static boolean skipAfter(final InputStream in, final byte[] match, final OutputStream out) throws IOException {
        int found = 0;
        final int len = match.length;
        int b;
        while ((b = in.read()) >= 0) {
            if (b == match[found]) {
                if (++found == len) {
                    return true;
                }
                continue;
            }
            else {
                if (out != null) {
                    if (found > 0) {
                        out.write(match, 0, found);
                    }
                    out.write(b);
                }
                found = 0;
            }
        }
        return false;
    }
    
    static {
        try {
            PACKET_HEADER = "<?xpacket begin=".getBytes("US-ASCII");
            PACKET_HEADER_END = "?>".getBytes("US-ASCII");
            PACKET_TRAILER = "<?xpacket".getBytes("US-ASCII");
        }
        catch (UnsupportedEncodingException e) {
            throw new RuntimeException("Incompatible JVM! US-ASCII encoding not supported.");
        }
    }
}
