// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.parser.image;

import java.io.UnsupportedEncodingException;
import java.util.Collections;
import java.util.Collection;
import java.util.HashSet;
import java.util.Arrays;
import org.xml.sax.SAXException;
import java.io.IOException;
import org.apache.tika.sax.XHTMLContentHandler;
import org.apache.tika.metadata.TikaCoreProperties;
import org.apache.tika.metadata.TIFF;
import org.apache.tika.io.EndianUtils;
import org.apache.tika.exception.TikaException;
import org.apache.poi.util.IOUtils;
import org.apache.tika.metadata.Metadata;
import org.xml.sax.ContentHandler;
import java.io.InputStream;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.mime.MediaType;
import java.util.Set;
import org.apache.tika.parser.AbstractParser;

public class PSDParser extends AbstractParser
{
    private static final long serialVersionUID = 883387734607994914L;
    private static final Set<MediaType> SUPPORTED_TYPES;
    
    public Set<MediaType> getSupportedTypes(final ParseContext context) {
        return PSDParser.SUPPORTED_TYPES;
    }
    
    public void parse(final InputStream stream, final ContentHandler handler, final Metadata metadata, final ParseContext context) throws IOException, SAXException, TikaException {
        final byte[] signature = new byte[4];
        IOUtils.readFully(stream, signature);
        if (signature[0] != 56 || signature[1] != 66 || signature[2] != 80 || signature[3] != 83) {
            throw new TikaException("PSD/PSB magic signature invalid");
        }
        final int version = EndianUtils.readUShortBE(stream);
        if (version != 1 && version != 2) {
            throw new TikaException("Invalid PSD/PSB version " + version);
        }
        IOUtils.readFully(stream, new byte[6]);
        final int numChannels = EndianUtils.readUShortBE(stream);
        final int height = EndianUtils.readIntBE(stream);
        final int width = EndianUtils.readIntBE(stream);
        metadata.set(TIFF.IMAGE_LENGTH, height);
        metadata.set(TIFF.IMAGE_WIDTH, width);
        final int depth = EndianUtils.readUShortBE(stream);
        metadata.set(TIFF.BITS_PER_SAMPLE, Integer.toString(depth));
        final int colorMode = EndianUtils.readUShortBE(stream);
        final long colorModeSectionSize = EndianUtils.readIntBE(stream);
        stream.skip(colorModeSectionSize);
        final long imageResourcesSectionSize = EndianUtils.readIntBE(stream);
        long read = 0L;
        while (read < imageResourcesSectionSize) {
            final ResourceBlock rb = new ResourceBlock(stream);
            read += rb.totalLength;
            if (rb.id == 1008) {
                metadata.add(TikaCoreProperties.DESCRIPTION, rb.getDataAsString());
            }
            else {
                if (rb.id == 1058) {
                    continue;
                }
                if (rb.id == 1059) {
                    continue;
                }
                if (rb.id == 1060) {}
            }
        }
        final XHTMLContentHandler xhtml = new XHTMLContentHandler(handler, metadata);
        xhtml.startDocument();
        xhtml.endDocument();
    }
    
    static {
        SUPPORTED_TYPES = Collections.unmodifiableSet((Set<? extends MediaType>)new HashSet<MediaType>(Arrays.asList(MediaType.image("vnd.adobe.photoshop"))));
    }
    
    private static class ResourceBlock
    {
        private static final long SIGNATURE = 943868237L;
        private static final int ID_CAPTION = 1008;
        private static final int ID_URL = 1035;
        private static final int ID_EXIF_1 = 1058;
        private static final int ID_EXIF_3 = 1059;
        private static final int ID_XMP = 1060;
        private int id;
        private String name;
        private byte[] data;
        private int totalLength;
        
        private ResourceBlock(final InputStream stream) throws IOException, TikaException {
            final long sig = EndianUtils.readIntBE(stream);
            if (sig != 943868237L) {
                throw new TikaException("Invalid Image Resource Block Signature Found, got " + sig + " 0x" + Long.toHexString(sig) + " but the spec defines " + 943868237L);
            }
            this.id = EndianUtils.readUShortBE(stream);
            final StringBuffer nameB = new StringBuffer();
            int nameLen = 0;
            while (true) {
                final int v = stream.read();
                ++nameLen;
                if (v == 0) {
                    break;
                }
                nameB.append((char)v);
                this.name = nameB.toString();
            }
            if (nameLen % 2 == 1) {
                stream.read();
                ++nameLen;
            }
            final int dataLen = EndianUtils.readIntBE(stream);
            this.totalLength = 6 + nameLen + 4 + dataLen;
            IOUtils.readFully(stream, this.data = new byte[dataLen]);
        }
        
        private String getDataAsString() {
            try {
                return new String(this.data, 0, this.data.length - 1, "ASCII");
            }
            catch (UnsupportedEncodingException e) {
                throw new RuntimeException("Something is very broken in your JVM!");
            }
        }
    }
}
