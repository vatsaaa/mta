// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.parser.image;

import java.text.DecimalFormatSymbols;
import java.util.Locale;
import com.drew.metadata.exif.GpsDirectory;
import java.text.DecimalFormat;
import org.apache.tika.metadata.IPTC;
import com.drew.metadata.iptc.IptcDirectory;
import java.util.Date;
import com.drew.lang.Rational;
import java.text.SimpleDateFormat;
import org.apache.tika.metadata.TikaCoreProperties;
import com.drew.metadata.jpeg.JpegCommentDirectory;
import java.util.regex.Matcher;
import org.apache.tika.metadata.Property;
import com.drew.metadata.exif.ExifDirectory;
import com.drew.metadata.jpeg.JpegDirectory;
import java.util.regex.Pattern;
import com.drew.metadata.Tag;
import com.drew.metadata.Directory;
import java.util.Iterator;
import com.drew.metadata.MetadataException;
import com.drew.imaging.tiff.TiffProcessingException;
import com.drew.imaging.tiff.TiffMetadataReader;
import java.io.InputStream;
import java.lang.reflect.Constructor;
import org.xml.sax.SAXException;
import java.io.IOException;
import com.drew.imaging.jpeg.JpegProcessingException;
import org.apache.tika.exception.TikaException;
import com.drew.metadata.jpeg.JpegCommentReader;
import com.drew.metadata.jpeg.JpegReader;
import com.drew.metadata.iptc.IptcReader;
import com.drew.metadata.MetadataReader;
import com.drew.metadata.exif.ExifReader;
import com.drew.imaging.jpeg.JpegSegmentReader;
import java.io.File;
import org.apache.tika.metadata.Metadata;

public class ImageMetadataExtractor
{
    private final Metadata metadata;
    private DirectoryHandler[] handlers;
    
    public ImageMetadataExtractor(final Metadata metadata) {
        this(metadata, new DirectoryHandler[] { new CopyUnknownFieldsHandler(), new JpegCommentHandler(), new ExifHandler(), new DimensionsHandler(), new GeotagHandler(), new IptcHandler() });
    }
    
    public ImageMetadataExtractor(final Metadata metadata, final DirectoryHandler... handlers) {
        this.metadata = metadata;
        this.handlers = handlers;
    }
    
    public void parseJpeg(final File file) throws IOException, SAXException, TikaException {
        try {
            final JpegSegmentReader reader = new JpegSegmentReader(file);
            this.extractMetadataFromSegment(reader, (byte)(-31), ExifReader.class);
            this.extractMetadataFromSegment(reader, (byte)(-19), IptcReader.class);
            this.extractMetadataFromSegment(reader, (byte)(-64), JpegReader.class);
            this.extractMetadataFromSegment(reader, (byte)(-2), JpegCommentReader.class);
        }
        catch (JpegProcessingException e) {
            throw new TikaException("Can't read JPEG metadata", e);
        }
    }
    
    private void extractMetadataFromSegment(final JpegSegmentReader reader, final byte marker, final Class<? extends MetadataReader> klass) {
        try {
            final Constructor<? extends MetadataReader> constructor = klass.getConstructor(byte[].class);
            for (int n = reader.getSegmentCount(marker), i = 0; i < n; ++i) {
                final byte[] segment = reader.readSegment(marker, i);
                final com.drew.metadata.Metadata metadata = new com.drew.metadata.Metadata();
                ((MetadataReader)constructor.newInstance(segment)).extract(metadata);
                this.handle(metadata);
            }
        }
        catch (Exception ex) {}
    }
    
    protected void parseTiff(final InputStream stream) throws IOException, SAXException, TikaException {
        try {
            final com.drew.metadata.Metadata tiffMetadata = TiffMetadataReader.readMetadata(stream);
            this.handle(tiffMetadata);
        }
        catch (TiffProcessingException e) {
            throw new TikaException("Can't read TIFF metadata", e);
        }
        catch (MetadataException e2) {
            throw new TikaException("Can't read TIFF metadata", e2);
        }
    }
    
    protected void handle(final com.drew.metadata.Metadata metadataExtractor) throws MetadataException {
        this.handle(metadataExtractor.getDirectoryIterator());
    }
    
    protected void handle(final Iterator<Directory> directories) throws MetadataException {
        while (directories.hasNext()) {
            final Directory directory = directories.next();
            for (int i = 0; i < this.handlers.length; ++i) {
                if (this.handlers[i].supports(directory.getClass())) {
                    this.handlers[i].handle(directory, this.metadata);
                }
            }
        }
    }
    
    static class CopyAllFieldsHandler implements DirectoryHandler
    {
        public boolean supports(final Class<? extends Directory> directoryType) {
            return true;
        }
        
        public void handle(final Directory directory, final Metadata metadata) throws MetadataException {
            final Iterator<?> tags = (Iterator<?>)directory.getTagIterator();
            while (tags.hasNext()) {
                final Tag tag = (Tag)tags.next();
                metadata.set(tag.getTagName(), tag.getDescription());
            }
        }
    }
    
    static class CopyUnknownFieldsHandler implements DirectoryHandler
    {
        public boolean supports(final Class<? extends Directory> directoryType) {
            return true;
        }
        
        public void handle(final Directory directory, final Metadata metadata) throws MetadataException {
            final Iterator<?> tags = (Iterator<?>)directory.getTagIterator();
            while (tags.hasNext()) {
                final Tag tag = (Tag)tags.next();
                final String name = tag.getTagName();
                if (!MetadataFields.isMetadataField(name)) {
                    try {
                        String value = tag.getDescription().trim();
                        if (Boolean.TRUE.toString().equalsIgnoreCase(value)) {
                            value = Boolean.TRUE.toString();
                        }
                        else if (Boolean.FALSE.toString().equalsIgnoreCase(value)) {
                            value = Boolean.FALSE.toString();
                        }
                        metadata.set(name, value);
                    }
                    catch (MetadataException ex) {}
                }
            }
        }
    }
    
    static class DimensionsHandler implements DirectoryHandler
    {
        private final Pattern LEADING_NUMBERS;
        
        DimensionsHandler() {
            this.LEADING_NUMBERS = Pattern.compile("(\\d+)\\s*.*");
        }
        
        public boolean supports(final Class<? extends Directory> directoryType) {
            return directoryType == JpegDirectory.class || directoryType == ExifDirectory.class;
        }
        
        public void handle(final Directory directory, final Metadata metadata) throws MetadataException {
            this.set(directory, metadata, 256, Metadata.IMAGE_WIDTH);
            this.set(directory, metadata, 3, Metadata.IMAGE_WIDTH);
            this.set(directory, metadata, 257, Metadata.IMAGE_LENGTH);
            this.set(directory, metadata, 1, Metadata.IMAGE_LENGTH);
            this.set(directory, metadata, 0, Metadata.BITS_PER_SAMPLE);
            this.set(directory, metadata, 258, Metadata.BITS_PER_SAMPLE);
            this.set(directory, metadata, 277, Metadata.SAMPLES_PER_PIXEL);
        }
        
        private void set(final Directory directory, final Metadata metadata, final int extractTag, final Property metadataField) {
            if (directory.containsTag(extractTag)) {
                final Matcher m = this.LEADING_NUMBERS.matcher(directory.getString(extractTag));
                if (m.matches()) {
                    metadata.set(metadataField, m.group(1));
                }
            }
        }
    }
    
    static class JpegCommentHandler implements DirectoryHandler
    {
        public boolean supports(final Class<? extends Directory> directoryType) {
            return directoryType == JpegCommentDirectory.class;
        }
        
        public void handle(final Directory directory, final Metadata metadata) throws MetadataException {
            if (directory.containsTag(0)) {
                metadata.add(TikaCoreProperties.COMMENTS, directory.getString(0));
            }
        }
    }
    
    static class ExifHandler implements DirectoryHandler
    {
        private static final SimpleDateFormat DATE_UNSPECIFIED_TZ;
        
        public boolean supports(final Class<? extends Directory> directoryType) {
            return directoryType == ExifDirectory.class;
        }
        
        public void handle(final Directory directory, final Metadata metadata) {
            try {
                this.handleDateTags(directory, metadata);
                this.handlePhotoTags(directory, metadata);
                this.handleCommentTags(directory, metadata);
            }
            catch (MetadataException ex) {}
        }
        
        public void handleCommentTags(final Directory directory, final Metadata metadata) {
            if (metadata.get(TikaCoreProperties.DESCRIPTION) == null && directory.containsTag(270)) {
                metadata.set(TikaCoreProperties.DESCRIPTION, directory.getString(270));
            }
        }
        
        public void handlePhotoTags(final Directory directory, final Metadata metadata) {
            if (directory.containsTag(33434)) {
                final Object exposure = directory.getObject(33434);
                if (exposure instanceof Rational) {
                    metadata.set(Metadata.EXPOSURE_TIME, ((Rational)exposure).doubleValue());
                }
                else {
                    metadata.set(Metadata.EXPOSURE_TIME, directory.getString(33434));
                }
            }
            if (directory.containsTag(37385)) {
                String flash = "";
                try {
                    flash = directory.getDescription(37385);
                }
                catch (MetadataException ex) {}
                if (flash.indexOf("Flash fired") > -1) {
                    metadata.set(Metadata.FLASH_FIRED, Boolean.TRUE.toString());
                }
                else if (flash.indexOf("Flash did not fire") > -1) {
                    metadata.set(Metadata.FLASH_FIRED, Boolean.FALSE.toString());
                }
                else {
                    metadata.set(Metadata.FLASH_FIRED, flash);
                }
            }
            if (directory.containsTag(33437)) {
                final Object fnumber = directory.getObject(33437);
                if (fnumber instanceof Rational) {
                    metadata.set(Metadata.F_NUMBER, ((Rational)fnumber).doubleValue());
                }
                else {
                    metadata.set(Metadata.F_NUMBER, directory.getString(33437));
                }
            }
            if (directory.containsTag(37386)) {
                final Object length = directory.getObject(37386);
                if (length instanceof Rational) {
                    metadata.set(Metadata.FOCAL_LENGTH, ((Rational)length).doubleValue());
                }
                else {
                    metadata.set(Metadata.FOCAL_LENGTH, directory.getString(37386));
                }
            }
            if (directory.containsTag(34855)) {
                metadata.set(Metadata.ISO_SPEED_RATINGS, directory.getString(34855));
            }
            if (directory.containsTag(271)) {
                metadata.set(Metadata.EQUIPMENT_MAKE, directory.getString(271));
            }
            if (directory.containsTag(272)) {
                metadata.set(Metadata.EQUIPMENT_MODEL, directory.getString(272));
            }
            if (directory.containsTag(274)) {
                final Object length = directory.getObject(274);
                if (length instanceof Integer) {
                    metadata.set(Metadata.ORIENTATION, Integer.toString((int)length));
                }
                else {
                    metadata.set(Metadata.ORIENTATION, directory.getString(274));
                }
            }
            if (directory.containsTag(305)) {
                metadata.set(Metadata.SOFTWARE, directory.getString(305));
            }
            if (directory.containsTag(282)) {
                final Object resolution = directory.getObject(282);
                if (resolution instanceof Rational) {
                    metadata.set(Metadata.RESOLUTION_HORIZONTAL, ((Rational)resolution).doubleValue());
                }
                else {
                    metadata.set(Metadata.RESOLUTION_HORIZONTAL, directory.getString(282));
                }
            }
            if (directory.containsTag(283)) {
                final Object resolution = directory.getObject(283);
                if (resolution instanceof Rational) {
                    metadata.set(Metadata.RESOLUTION_VERTICAL, ((Rational)resolution).doubleValue());
                }
                else {
                    metadata.set(Metadata.RESOLUTION_VERTICAL, directory.getString(283));
                }
            }
            if (directory.containsTag(296)) {
                try {
                    metadata.set(Metadata.RESOLUTION_UNIT, directory.getDescription(296));
                }
                catch (MetadataException ex2) {}
            }
        }
        
        public void handleDateTags(final Directory directory, final Metadata metadata) throws MetadataException {
            Date original = null;
            if (directory.containsTag(36867)) {
                original = directory.getDate(36867);
                final String datetimeNoTimeZone = ExifHandler.DATE_UNSPECIFIED_TZ.format(original);
                metadata.set(TikaCoreProperties.CREATED, datetimeNoTimeZone);
                metadata.set(Metadata.ORIGINAL_DATE, datetimeNoTimeZone);
            }
            if (directory.containsTag(306)) {
                final Date datetime = directory.getDate(306);
                final String datetimeNoTimeZone2 = ExifHandler.DATE_UNSPECIFIED_TZ.format(datetime);
                metadata.set(TikaCoreProperties.MODIFIED, datetimeNoTimeZone2);
                if (original == null) {
                    metadata.set(TikaCoreProperties.CREATED, datetimeNoTimeZone2);
                }
            }
        }
        
        static {
            DATE_UNSPECIFIED_TZ = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        }
    }
    
    static class IptcHandler implements DirectoryHandler
    {
        public boolean supports(final Class<? extends Directory> directoryType) {
            return directoryType == IptcDirectory.class;
        }
        
        public void handle(final Directory directory, final Metadata metadata) throws MetadataException {
            if (directory.containsTag(537)) {
                final String[] arr$;
                final String[] keywords = arr$ = directory.getStringArray(537);
                for (final String k : arr$) {
                    metadata.add(TikaCoreProperties.KEYWORDS, k);
                }
            }
            if (directory.containsTag(617)) {
                metadata.set(TikaCoreProperties.TITLE, directory.getString(617));
            }
            else if (directory.containsTag(517)) {
                metadata.set(TikaCoreProperties.TITLE, directory.getString(517));
            }
            if (directory.containsTag(592)) {
                metadata.set(TikaCoreProperties.CREATOR, directory.getString(592));
                metadata.set(IPTC.CREATOR, directory.getString(592));
            }
            if (directory.containsTag(632)) {
                metadata.set(TikaCoreProperties.DESCRIPTION, directory.getString(632).replaceAll("\r\n?", "\n"));
            }
        }
    }
    
    static class GeotagHandler implements DirectoryHandler
    {
        private static final Pattern HOURS_MINUTES_SECONDS;
        private static final DecimalFormat LAT_LONG_FORMAT;
        
        public boolean supports(final Class<? extends Directory> directoryType) {
            return directoryType == GpsDirectory.class;
        }
        
        public void handle(final Directory directory, final Metadata metadata) throws MetadataException {
            final String lat = directory.getDescription(2);
            final String latNS = directory.getDescription(1);
            if (lat != null) {
                Double latitude = this.parseHMS(lat);
                if (latitude != null) {
                    if (latNS != null && latNS.equalsIgnoreCase("S") && latitude > 0.0) {
                        latitude *= -1.0;
                    }
                    metadata.set(TikaCoreProperties.LATITUDE, GeotagHandler.LAT_LONG_FORMAT.format(latitude));
                }
            }
            final String lng = directory.getDescription(4);
            final String lngEW = directory.getDescription(3);
            if (lng != null) {
                Double longitude = this.parseHMS(lng);
                if (longitude != null) {
                    if (lngEW != null && lngEW.equalsIgnoreCase("W") && longitude > 0.0) {
                        longitude *= -1.0;
                    }
                    metadata.set(TikaCoreProperties.LONGITUDE, GeotagHandler.LAT_LONG_FORMAT.format(longitude));
                }
            }
        }
        
        private Double parseHMS(final String hms) {
            final Matcher m = GeotagHandler.HOURS_MINUTES_SECONDS.matcher(hms);
            if (m.matches()) {
                final double value = Integer.parseInt(m.group(1)) + Integer.parseInt(m.group(2)) / 60.0 + Double.parseDouble(m.group(3)) / 60.0 / 60.0;
                return value;
            }
            return null;
        }
        
        static {
            HOURS_MINUTES_SECONDS = Pattern.compile("(-?\\d+)\"(\\d+)'(\\d+\\.?\\d*)");
            LAT_LONG_FORMAT = new DecimalFormat("##0.0####", new DecimalFormatSymbols(Locale.US));
        }
    }
    
    interface DirectoryHandler
    {
        boolean supports(final Class<? extends Directory> p0);
        
        void handle(final Directory p0, final Metadata p1) throws MetadataException;
    }
}
