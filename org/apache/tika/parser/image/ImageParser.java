// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.parser.image;

import java.util.Collections;
import java.util.Collection;
import java.util.HashSet;
import java.util.Arrays;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import javax.imageio.metadata.IIOMetadata;
import org.apache.tika.metadata.Property;
import org.xml.sax.SAXException;
import java.io.IOException;
import javax.imageio.stream.ImageInputStream;
import java.util.Iterator;
import org.apache.tika.sax.XHTMLContentHandler;
import javax.imageio.IIOException;
import org.apache.tika.exception.TikaException;
import org.apache.tika.metadata.TikaCoreProperties;
import org.apache.tika.io.CloseShieldInputStream;
import javax.imageio.ImageReader;
import javax.imageio.ImageIO;
import org.apache.tika.metadata.Metadata;
import org.xml.sax.ContentHandler;
import java.io.InputStream;
import org.apache.tika.parser.ParseContext;
import java.util.Set;
import org.apache.tika.mime.MediaType;
import org.apache.tika.parser.AbstractParser;

public class ImageParser extends AbstractParser
{
    private static final long serialVersionUID = 7852529269245520335L;
    private static final MediaType CANONICAL_BMP_TYPE;
    private static final MediaType JAVA_BMP_TYPE;
    private static final Set<MediaType> SUPPORTED_TYPES;
    
    public Set<MediaType> getSupportedTypes(final ParseContext context) {
        return ImageParser.SUPPORTED_TYPES;
    }
    
    public void parse(final InputStream stream, final ContentHandler handler, final Metadata metadata, final ParseContext context) throws IOException, SAXException, TikaException {
        String type = metadata.get("Content-Type");
        if (type != null) {
            if (ImageParser.CANONICAL_BMP_TYPE.toString().equals(type)) {
                type = ImageParser.JAVA_BMP_TYPE.toString();
            }
            try {
                final Iterator<ImageReader> iterator = ImageIO.getImageReadersByMIMEType(type);
                if (iterator.hasNext()) {
                    final ImageReader reader = iterator.next();
                    try {
                        final ImageInputStream imageStream = ImageIO.createImageInputStream(new CloseShieldInputStream(stream));
                        try {
                            reader.setInput(imageStream);
                            metadata.set(Metadata.IMAGE_WIDTH, Integer.toString(reader.getWidth(0)));
                            metadata.set(Metadata.IMAGE_LENGTH, Integer.toString(reader.getHeight(0)));
                            metadata.set("height", Integer.toString(reader.getHeight(0)));
                            metadata.set("width", Integer.toString(reader.getWidth(0)));
                            loadMetadata(reader.getImageMetadata(0), metadata);
                        }
                        finally {
                            imageStream.close();
                        }
                    }
                    finally {
                        reader.dispose();
                    }
                }
                setIfPresent(metadata, "CommentExtensions CommentExtension", TikaCoreProperties.COMMENTS);
                setIfPresent(metadata, "markerSequence com", TikaCoreProperties.COMMENTS);
                setIfPresent(metadata, "Data BitsPerSample", Metadata.BITS_PER_SAMPLE);
            }
            catch (IIOException e) {
                if (!e.getMessage().equals("Unexpected block type 0!") || !type.equals("image/gif")) {
                    throw new TikaException(type + " parse error", e);
                }
            }
        }
        final XHTMLContentHandler xhtml = new XHTMLContentHandler(handler, metadata);
        xhtml.startDocument();
        xhtml.endDocument();
    }
    
    private static void setIfPresent(final Metadata metadata, final String imageIOkey, final String tikaKey) {
        if (metadata.get(imageIOkey) != null) {
            metadata.set(tikaKey, metadata.get(imageIOkey));
        }
    }
    
    private static void setIfPresent(final Metadata metadata, final String imageIOkey, final Property tikaProp) {
        if (metadata.get(imageIOkey) != null) {
            String v = metadata.get(imageIOkey);
            if (v.endsWith(" ")) {
                v = v.substring(0, v.lastIndexOf(32));
            }
            metadata.set(tikaProp, v);
        }
    }
    
    private static void loadMetadata(final IIOMetadata imageMetadata, final Metadata metadata) {
        final String[] names = imageMetadata.getMetadataFormatNames();
        if (names == null) {
            return;
        }
        for (int length = names.length, i = 0; i < length; ++i) {
            loadNode(metadata, imageMetadata.getAsTree(names[i]), "", false);
        }
    }
    
    private static void loadNode(final Metadata metadata, final Node node, String parents, final boolean addThisNodeName) {
        if (addThisNodeName) {
            if (parents.length() > 0) {
                parents += " ";
            }
            parents += node.getNodeName();
        }
        final NamedNodeMap map = node.getAttributes();
        if (map != null) {
            final int length = map.getLength();
            if (length == 1) {
                metadata.add(parents, normalize(map.item(0).getNodeValue()));
            }
            else if (length > 1) {
                final StringBuilder value = new StringBuilder();
                for (int i = 0; i < length; ++i) {
                    if (i > 0) {
                        value.append(", ");
                    }
                    final Node attr = map.item(i);
                    value.append(attr.getNodeName());
                    value.append("=");
                    value.append(normalize(attr.getNodeValue()));
                }
                metadata.add(parents, value.toString());
            }
        }
        for (Node child = node.getFirstChild(); child != null; child = child.getNextSibling()) {
            loadNode(metadata, child, parents, true);
        }
    }
    
    private static String normalize(String value) {
        if (value != null) {
            value = value.trim();
        }
        else {
            value = "";
        }
        if (Boolean.TRUE.toString().equalsIgnoreCase(value)) {
            return Boolean.TRUE.toString();
        }
        if (Boolean.FALSE.toString().equalsIgnoreCase(value)) {
            return Boolean.FALSE.toString();
        }
        return value;
    }
    
    static {
        CANONICAL_BMP_TYPE = MediaType.image("x-ms-bmp");
        JAVA_BMP_TYPE = MediaType.image("bmp");
        SUPPORTED_TYPES = Collections.unmodifiableSet((Set<? extends MediaType>)new HashSet<MediaType>(Arrays.asList(ImageParser.CANONICAL_BMP_TYPE, ImageParser.JAVA_BMP_TYPE, MediaType.image("gif"), MediaType.image("png"), MediaType.image("vnd.wap.wbmp"), MediaType.image("x-icon"), MediaType.image("x-xcf"))));
    }
}
