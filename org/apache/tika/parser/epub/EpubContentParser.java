// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.parser.epub;

import org.xml.sax.SAXException;
import java.io.IOException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.ParserConfigurationException;
import org.apache.tika.exception.TikaException;
import org.xml.sax.helpers.DefaultHandler;
import org.apache.tika.sax.OfflineContentHandler;
import org.apache.tika.io.CloseShieldInputStream;
import org.xml.sax.SAXNotRecognizedException;
import javax.xml.parsers.SAXParserFactory;
import org.apache.tika.sax.XHTMLContentHandler;
import org.apache.tika.metadata.Metadata;
import org.xml.sax.ContentHandler;
import java.io.InputStream;
import java.util.Collections;
import org.apache.tika.mime.MediaType;
import java.util.Set;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.parser.AbstractParser;

public class EpubContentParser extends AbstractParser
{
    public Set<MediaType> getSupportedTypes(final ParseContext context) {
        return Collections.emptySet();
    }
    
    public void parse(final InputStream stream, final ContentHandler handler, final Metadata metadata, final ParseContext context) throws IOException, SAXException, TikaException {
        final XHTMLContentHandler xhtml = new XHTMLContentHandler(handler, metadata);
        try {
            final SAXParserFactory factory = SAXParserFactory.newInstance();
            factory.setValidating(false);
            factory.setNamespaceAware(true);
            try {
                factory.setFeature("http://javax.xml.XMLConstants/feature/secure-processing", true);
            }
            catch (SAXNotRecognizedException ex) {}
            final SAXParser parser = factory.newSAXParser();
            parser.parse(new CloseShieldInputStream(stream), new OfflineContentHandler(xhtml));
        }
        catch (ParserConfigurationException e) {
            throw new TikaException("XML parser configuration error", e);
        }
    }
}
