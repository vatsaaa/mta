// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.parser.epub;

import java.util.Collections;
import java.util.Collection;
import java.util.HashSet;
import java.util.Arrays;
import org.apache.tika.exception.TikaException;
import org.xml.sax.SAXException;
import java.io.IOException;
import java.util.zip.ZipEntry;
import org.xml.sax.helpers.DefaultHandler;
import org.apache.tika.io.IOUtils;
import java.util.zip.ZipInputStream;
import org.apache.tika.sax.EmbeddedContentHandler;
import org.apache.tika.sax.BodyContentHandler;
import org.apache.tika.sax.XHTMLContentHandler;
import org.apache.tika.metadata.Metadata;
import org.xml.sax.ContentHandler;
import java.io.InputStream;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.parser.xml.DcXMLParser;
import org.apache.tika.parser.Parser;
import org.apache.tika.mime.MediaType;
import java.util.Set;
import org.apache.tika.parser.AbstractParser;

public class EpubParser extends AbstractParser
{
    private static final long serialVersionUID = 215176772484050550L;
    private static final Set<MediaType> SUPPORTED_TYPES;
    private Parser meta;
    private Parser content;
    
    public EpubParser() {
        this.meta = new DcXMLParser();
        this.content = new EpubContentParser();
    }
    
    public Parser getMetaParser() {
        return this.meta;
    }
    
    public void setMetaParser(final Parser meta) {
        this.meta = meta;
    }
    
    public Parser getContentParser() {
        return this.content;
    }
    
    public void setContentParser(final Parser content) {
        this.content = content;
    }
    
    public Set<MediaType> getSupportedTypes(final ParseContext context) {
        return EpubParser.SUPPORTED_TYPES;
    }
    
    public void parse(final InputStream stream, final ContentHandler handler, final Metadata metadata, final ParseContext context) throws IOException, SAXException, TikaException {
        final XHTMLContentHandler xhtml = new XHTMLContentHandler(handler, metadata);
        xhtml.startDocument();
        final ContentHandler childHandler = new EmbeddedContentHandler(new BodyContentHandler(xhtml));
        final ZipInputStream zip = new ZipInputStream(stream);
        for (ZipEntry entry = zip.getNextEntry(); entry != null; entry = zip.getNextEntry()) {
            if (entry.getName().equals("mimetype")) {
                final String type = IOUtils.toString(zip, "UTF-8");
                metadata.set("Content-Type", type);
            }
            else if (entry.getName().equals("metadata.xml")) {
                this.meta.parse(zip, new DefaultHandler(), metadata, context);
            }
            else if (entry.getName().endsWith(".opf")) {
                this.meta.parse(zip, new DefaultHandler(), metadata, context);
            }
            else if (entry.getName().endsWith(".html") || entry.getName().endsWith(".xhtml")) {
                this.content.parse(zip, childHandler, metadata, context);
            }
        }
        xhtml.endDocument();
    }
    
    static {
        SUPPORTED_TYPES = Collections.unmodifiableSet((Set<? extends MediaType>)new HashSet<MediaType>(Arrays.asList(MediaType.application("epub+zip"), MediaType.application("x-ibooks+zip"))));
    }
}
