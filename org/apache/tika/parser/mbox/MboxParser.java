// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.parser.mbox;

import java.util.Collections;
import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.Date;
import java.util.regex.Matcher;
import java.text.ParseException;
import org.apache.tika.metadata.TikaCoreProperties;
import org.xml.sax.SAXException;
import java.io.IOException;
import org.apache.tika.sax.XHTMLContentHandler;
import java.io.Reader;
import java.io.BufferedReader;
import java.io.UnsupportedEncodingException;
import org.apache.tika.exception.TikaException;
import java.io.InputStreamReader;
import org.apache.tika.metadata.Metadata;
import org.xml.sax.ContentHandler;
import java.io.InputStream;
import org.apache.tika.parser.ParseContext;
import java.util.regex.Pattern;
import org.apache.tika.mime.MediaType;
import java.util.Set;
import org.apache.tika.parser.AbstractParser;

public class MboxParser extends AbstractParser
{
    private static final long serialVersionUID = -1762689436731160661L;
    private static final Set<MediaType> SUPPORTED_TYPES;
    public static final String MBOX_MIME_TYPE = "application/mbox";
    public static final String MBOX_RECORD_DIVIDER = "From ";
    private static final Pattern EMAIL_HEADER_PATTERN;
    private static final Pattern EMAIL_ADDRESS_PATTERN;
    private static final String EMAIL_HEADER_METADATA_PREFIX = "MboxParser-";
    private static final String EMAIL_FROMLINE_METADATA = "MboxParser-from";
    
    public Set<MediaType> getSupportedTypes(final ParseContext context) {
        return MboxParser.SUPPORTED_TYPES;
    }
    
    public void parse(final InputStream stream, final ContentHandler handler, final Metadata metadata, final ParseContext context) throws IOException, TikaException, SAXException {
        InputStreamReader isr;
        try {
            isr = new InputStreamReader(stream, "US-ASCII");
        }
        catch (UnsupportedEncodingException e) {
            throw new TikaException("US-ASCII is not supported!", e);
        }
        final BufferedReader reader = new BufferedReader(isr);
        metadata.set("Content-Type", "application/mbox");
        metadata.set("Content-Encoding", "us-ascii");
        final XHTMLContentHandler xhtml = new XHTMLContentHandler(handler, metadata);
        xhtml.startDocument();
        ParseStates parseState = ParseStates.START;
        String multiLine = null;
        boolean inQuote = false;
        int numEmails = 0;
        for (String curLine = reader.readLine(); curLine != null; curLine = reader.readLine()) {
            boolean newMessage = curLine.startsWith("From ");
            if (newMessage) {
                ++numEmails;
            }
            Label_0165: {
                switch (parseState) {
                    case START: {
                        if (newMessage) {
                            parseState = ParseStates.IN_HEADER;
                            newMessage = false;
                            break Label_0165;
                        }
                        break;
                    }
                    case IN_HEADER: {
                        if (newMessage) {
                            this.saveHeaderInMetadata(numEmails, metadata, multiLine);
                            multiLine = curLine;
                            break;
                        }
                        if (curLine.length() == 0) {
                            this.saveHeaderInMetadata(numEmails, metadata, multiLine);
                            parseState = ParseStates.IN_CONTENT;
                            xhtml.startElement("div", "class", "email-entry");
                            xhtml.startElement("p");
                            inQuote = false;
                            break;
                        }
                        if (curLine.startsWith(" ") || curLine.startsWith("\t")) {
                            multiLine = multiLine + " " + curLine.trim();
                            break;
                        }
                        this.saveHeaderInMetadata(numEmails, metadata, multiLine);
                        multiLine = curLine;
                        break;
                    }
                    case IN_CONTENT: {
                        if (newMessage) {
                            this.endMessage(xhtml, inQuote);
                            parseState = ParseStates.IN_HEADER;
                            multiLine = curLine;
                            break;
                        }
                        final boolean quoted = curLine.startsWith(">");
                        if (inQuote) {
                            if (!quoted) {
                                xhtml.endElement("q");
                                inQuote = false;
                            }
                        }
                        else if (quoted) {
                            xhtml.startElement("q");
                            inQuote = true;
                        }
                        xhtml.characters(curLine);
                        xhtml.element("br", "");
                        break;
                    }
                }
            }
        }
        if (parseState == ParseStates.IN_HEADER) {
            this.saveHeaderInMetadata(numEmails, metadata, multiLine);
        }
        else if (parseState == ParseStates.IN_CONTENT) {
            this.endMessage(xhtml, inQuote);
        }
        xhtml.endDocument();
    }
    
    private void endMessage(final XHTMLContentHandler xhtml, final boolean inQuote) throws SAXException {
        if (inQuote) {
            xhtml.endElement("q");
        }
        xhtml.endElement("p");
        xhtml.endElement("div");
    }
    
    private void saveHeaderInMetadata(final int numEmails, final Metadata metadata, final String curLine) {
        if (curLine == null || numEmails > 1) {
            return;
        }
        if (curLine.startsWith("From ")) {
            metadata.add("MboxParser-from", curLine.substring("From ".length()));
            return;
        }
        final Matcher headerMatcher = MboxParser.EMAIL_HEADER_PATTERN.matcher(curLine);
        if (!headerMatcher.matches()) {
            return;
        }
        final String headerTag = headerMatcher.group(1).toLowerCase();
        final String headerContent = headerMatcher.group(2);
        if (headerTag.equalsIgnoreCase("From")) {
            metadata.set(TikaCoreProperties.CREATOR, headerContent);
        }
        else if (headerTag.equalsIgnoreCase("To") || headerTag.equalsIgnoreCase("Cc") || headerTag.equalsIgnoreCase("Bcc")) {
            final Matcher address = MboxParser.EMAIL_ADDRESS_PATTERN.matcher(headerContent);
            if (address.find()) {
                metadata.add("Message-Recipient-Address", address.group(1));
            }
            else if (headerContent.indexOf(64) > -1) {
                metadata.add("Message-Recipient-Address", headerContent);
            }
            String property = "Message-To";
            if (headerTag.equalsIgnoreCase("Cc")) {
                property = "Message-Cc";
            }
            else if (headerTag.equalsIgnoreCase("Bcc")) {
                property = "Message-Bcc";
            }
            metadata.add(property, headerContent);
        }
        else if (headerTag.equalsIgnoreCase("Subject")) {
            metadata.add(TikaCoreProperties.TRANSITION_SUBJECT_TO_DC_TITLE, headerContent);
        }
        else if (headerTag.equalsIgnoreCase("Date")) {
            try {
                final Date date = parseDate(headerContent);
                metadata.set(TikaCoreProperties.CREATED, date);
            }
            catch (ParseException e) {}
        }
        else if (headerTag.equalsIgnoreCase("Message-Id")) {
            metadata.set(TikaCoreProperties.IDENTIFIER, headerContent);
        }
        else if (headerTag.equalsIgnoreCase("In-Reply-To")) {
            metadata.set(TikaCoreProperties.RELATION, headerContent);
        }
        else if (headerTag.equalsIgnoreCase("Content-Type")) {
            metadata.add("Content-Type", headerContent);
            metadata.set(TikaCoreProperties.FORMAT, headerContent);
        }
        else {
            metadata.add("MboxParser-" + headerTag, headerContent);
        }
    }
    
    public static Date parseDate(final String headerContent) throws ParseException {
        final SimpleDateFormat dateFormat = new SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss Z", Locale.US);
        return dateFormat.parse(headerContent);
    }
    
    static {
        SUPPORTED_TYPES = Collections.singleton(MediaType.application("mbox"));
        EMAIL_HEADER_PATTERN = Pattern.compile("([^ ]+):[ \t]*(.*)");
        EMAIL_ADDRESS_PATTERN = Pattern.compile("<(.*@.*)>");
    }
    
    private enum ParseStates
    {
        START, 
        IN_HEADER, 
        IN_CONTENT;
    }
}
