// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.parser.video;

import java.util.Collections;
import org.xml.sax.SAXException;
import java.util.Iterator;
import java.util.Map;
import java.io.ByteArrayInputStream;
import org.apache.tika.sax.XHTMLContentHandler;
import org.apache.tika.exception.TikaException;
import org.apache.tika.metadata.Metadata;
import org.xml.sax.ContentHandler;
import java.io.InputStream;
import java.util.HashMap;
import java.util.ArrayList;
import java.util.Date;
import java.io.IOException;
import java.io.DataInputStream;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.mime.MediaType;
import java.util.Set;
import org.apache.tika.parser.AbstractParser;

public class FLVParser extends AbstractParser
{
    private static final long serialVersionUID = -8718013155719197679L;
    private static int TYPE_METADATA;
    private static byte MASK_AUDIO;
    private static byte MASK_VIDEO;
    private static final Set<MediaType> SUPPORTED_TYPES;
    
    public Set<MediaType> getSupportedTypes(final ParseContext context) {
        return FLVParser.SUPPORTED_TYPES;
    }
    
    private long readUInt32(final DataInputStream input) throws IOException {
        return (long)input.readInt() & 0xFFFFFFFFL;
    }
    
    private int readUInt24(final DataInputStream input) throws IOException {
        int uint = input.read() << 16;
        uint += input.read() << 8;
        uint += input.read();
        return uint;
    }
    
    private Object readAMFData(final DataInputStream input, int type) throws IOException {
        if (type == -1) {
            type = input.readUnsignedByte();
        }
        switch (type) {
            case 0: {
                return input.readDouble();
            }
            case 1: {
                return input.readUnsignedByte() == 1;
            }
            case 2: {
                return this.readAMFString(input);
            }
            case 3: {
                return this.readAMFObject(input);
            }
            case 8: {
                return this.readAMFEcmaArray(input);
            }
            case 10: {
                return this.readAMFStrictArray(input);
            }
            case 11: {
                final Date date = new Date((long)input.readDouble());
                input.readShort();
                return date;
            }
            case 13: {
                return "UNDEFINED";
            }
            default: {
                return null;
            }
        }
    }
    
    private Object readAMFStrictArray(final DataInputStream input) throws IOException {
        final long count = this.readUInt32(input);
        final ArrayList<Object> list = new ArrayList<Object>();
        for (int i = 0; i < count; ++i) {
            list.add(this.readAMFData(input, -1));
        }
        return list;
    }
    
    private String readAMFString(final DataInputStream input) throws IOException {
        final int size = input.readUnsignedShort();
        final byte[] chars = new byte[size];
        input.readFully(chars);
        return new String(chars);
    }
    
    private Object readAMFObject(final DataInputStream input) throws IOException {
        final HashMap<String, Object> array = new HashMap<String, Object>();
        while (true) {
            final String key = this.readAMFString(input);
            final int dataType = input.read();
            if (dataType == 9) {
                break;
            }
            array.put(key, this.readAMFData(input, dataType));
        }
        return array;
    }
    
    private Object readAMFEcmaArray(final DataInputStream input) throws IOException {
        final long size = this.readUInt32(input);
        final HashMap<String, Object> array = new HashMap<String, Object>();
        for (int i = 0; i < size; ++i) {
            final String key = this.readAMFString(input);
            final int dataType = input.read();
            array.put(key, this.readAMFData(input, dataType));
        }
        return array;
    }
    
    private boolean checkSignature(final DataInputStream fis) throws IOException {
        return fis.read() == 70 && fis.read() == 76 && fis.read() == 86;
    }
    
    public void parse(final InputStream stream, final ContentHandler handler, final Metadata metadata, final ParseContext context) throws IOException, SAXException, TikaException {
        final DataInputStream datainput = new DataInputStream(stream);
        if (!this.checkSignature(datainput)) {
            throw new TikaException("FLV signature not detected");
        }
        final int version = datainput.readUnsignedByte();
        if (version != 1) {
            throw new TikaException("Unpexpected FLV version: " + version);
        }
        final int typeFlags = datainput.readUnsignedByte();
        final long len = this.readUInt32(datainput);
        if (len != 9L) {
            throw new TikaException("Unpexpected FLV header length: " + len);
        }
        long sizePrev = this.readUInt32(datainput);
        if (sizePrev != 0L) {
            throw new TikaException("Unpexpected FLV first previous block size: " + sizePrev);
        }
        metadata.set("Content-Type", "video/x-flv");
        metadata.set("hasVideo", Boolean.toString((typeFlags & FLVParser.MASK_VIDEO) != 0x0));
        metadata.set("hasAudio", Boolean.toString((typeFlags & FLVParser.MASK_AUDIO) != 0x0));
        final XHTMLContentHandler xhtml = new XHTMLContentHandler(handler, metadata);
        xhtml.startDocument();
        int datalen;
        do {
            final int type = datainput.read();
            if (type == -1) {
                break;
            }
            datalen = this.readUInt24(datainput);
            this.readUInt32(datainput);
            this.readUInt24(datainput);
            if (type == FLVParser.TYPE_METADATA) {
                final byte[] metaBytes = new byte[datalen];
                int r;
                for (int readCount = 0; readCount < datalen; readCount += r) {
                    r = stream.read(metaBytes, readCount, datalen - readCount);
                    if (r == -1) {
                        break;
                    }
                }
                final ByteArrayInputStream is = new ByteArrayInputStream(metaBytes);
                final DataInputStream dis = new DataInputStream(is);
                Object data = null;
                for (int i = 0; i < 2; ++i) {
                    data = this.readAMFData(dis, -1);
                }
                if (data instanceof Map) {
                    final Map<String, Object> extractedMetadata = (Map<String, Object>)data;
                    for (final Map.Entry<String, Object> entry : extractedMetadata.entrySet()) {
                        metadata.set(entry.getKey(), entry.getValue().toString());
                    }
                }
            }
            else {
                for (int j = 0; j < datalen; ++j) {
                    datainput.readByte();
                }
            }
            sizePrev = this.readUInt32(datainput);
        } while (sizePrev == datalen + 11);
        xhtml.endDocument();
    }
    
    static {
        FLVParser.TYPE_METADATA = 18;
        FLVParser.MASK_AUDIO = 1;
        FLVParser.MASK_VIDEO = 4;
        SUPPORTED_TYPES = Collections.singleton(MediaType.video("x-flv"));
    }
}
