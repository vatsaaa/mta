// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.parser;

import org.apache.tika.metadata.Metadata;

public interface PasswordProvider
{
    String getPassword(final Metadata p0);
}
