// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.parser.microsoft;

import org.xml.sax.SAXException;
import org.apache.poi.hsmf.datatypes.AttachmentChunks;
import org.apache.poi.hsmf.datatypes.Chunk;
import java.util.Date;
import org.apache.tika.parser.txt.CharsetMatch;
import org.apache.poi.poifs.filesystem.DirectoryEntry;
import org.apache.tika.io.TikaInputStream;
import org.apache.tika.parser.rtf.RTFParser;
import org.apache.poi.hmef.attribute.MAPIRtfAttribute;
import java.io.InputStream;
import org.apache.tika.sax.EmbeddedContentHandler;
import org.xml.sax.ContentHandler;
import org.apache.tika.sax.BodyContentHandler;
import java.io.ByteArrayInputStream;
import org.apache.tika.parser.html.HtmlParser;
import org.apache.poi.hsmf.datatypes.StringChunk;
import org.apache.poi.hsmf.datatypes.ByteChunk;
import org.apache.poi.hsmf.datatypes.MAPIProperty;
import java.text.ParseException;
import org.apache.tika.parser.mbox.MboxParser;
import org.apache.poi.hsmf.exceptions.ChunkNotFoundException;
import org.apache.tika.metadata.TikaCoreProperties;
import org.apache.tika.parser.txt.CharsetDetector;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.sax.XHTMLContentHandler;
import java.io.IOException;
import org.apache.poi.poifs.filesystem.DirectoryNode;
import org.apache.tika.exception.TikaException;
import org.apache.tika.parser.ParseContext;
import org.apache.poi.poifs.filesystem.NPOIFSFileSystem;
import org.apache.poi.hsmf.MAPIMessage;

public class OutlookExtractor extends AbstractPOIFSExtractor
{
    private final MAPIMessage msg;
    
    public OutlookExtractor(final NPOIFSFileSystem filesystem, final ParseContext context) throws TikaException {
        this(filesystem.getRoot(), context);
    }
    
    public OutlookExtractor(final DirectoryNode root, final ParseContext context) throws TikaException {
        super(context);
        try {
            this.msg = new MAPIMessage(root);
        }
        catch (IOException e) {
            throw new TikaException("Failed to parse Outlook message", e);
        }
    }
    
    public void parse(final XHTMLContentHandler xhtml, final Metadata metadata) throws TikaException, SAXException, IOException {
        try {
            this.msg.setReturnNullOnMissingChunk(true);
            if (this.msg.has7BitEncodingStrings()) {
                if (this.msg.getHeaders() != null) {
                    this.msg.guess7BitEncoding();
                }
                else {
                    final StringChunk text = this.msg.getMainChunks().textBodyChunk;
                    if (text != null) {
                        final CharsetDetector detector = new CharsetDetector();
                        detector.setText(text.getRawValue());
                        final CharsetMatch match = detector.detect();
                        if (match.getConfidence() > 35) {
                            this.msg.set7BitEncoding(match.getName());
                        }
                    }
                }
            }
            final String subject = this.msg.getSubject();
            final String from = this.msg.getDisplayFrom();
            metadata.set(TikaCoreProperties.CREATOR, from);
            metadata.set("Message-From", from);
            metadata.set("Message-To", this.msg.getDisplayTo());
            metadata.set("Message-Cc", this.msg.getDisplayCC());
            metadata.set("Message-Bcc", this.msg.getDisplayBCC());
            metadata.set(TikaCoreProperties.TITLE, subject);
            metadata.set(TikaCoreProperties.TRANSITION_SUBJECT_TO_DC_DESCRIPTION, this.msg.getConversationTopic());
            try {
                for (final String recipientAddress : this.msg.getRecipientEmailAddressList()) {
                    if (recipientAddress != null) {
                        metadata.add("Message-Recipient-Address", recipientAddress);
                    }
                }
            }
            catch (ChunkNotFoundException ex) {}
            if (this.msg.getMessageDate() != null) {
                metadata.set(TikaCoreProperties.CREATED, this.msg.getMessageDate().getTime());
                metadata.set(TikaCoreProperties.MODIFIED, this.msg.getMessageDate().getTime());
            }
            else {
                try {
                    final String[] headers = this.msg.getHeaders();
                    if (headers != null && headers.length > 0) {
                        for (final String header : headers) {
                            if (header.toLowerCase().startsWith("date:")) {
                                final String date = header.substring(header.indexOf(58) + 1).trim();
                                try {
                                    final Date d = MboxParser.parseDate(date);
                                    metadata.set(TikaCoreProperties.CREATED, d);
                                    metadata.set(TikaCoreProperties.MODIFIED, d);
                                }
                                catch (ParseException e2) {
                                    metadata.set(TikaCoreProperties.CREATED, date);
                                    metadata.set(TikaCoreProperties.MODIFIED, date);
                                }
                                break;
                            }
                        }
                    }
                }
                catch (ChunkNotFoundException ex2) {}
            }
            xhtml.element("h1", subject);
            xhtml.startElement("dl");
            if (from != null) {
                this.header(xhtml, "From", from);
            }
            this.header(xhtml, "To", this.msg.getDisplayTo());
            this.header(xhtml, "Cc", this.msg.getDisplayCC());
            this.header(xhtml, "Bcc", this.msg.getDisplayBCC());
            try {
                this.header(xhtml, "Recipients", this.msg.getRecipientEmailAddress());
            }
            catch (ChunkNotFoundException ex3) {}
            xhtml.endElement("dl");
            Chunk htmlChunk = null;
            Chunk rtfChunk = null;
            Chunk textChunk = null;
            for (final Chunk chunk : this.msg.getMainChunks().getAll()) {
                if (chunk.getChunkId() == MAPIProperty.BODY_HTML.id) {
                    htmlChunk = chunk;
                }
                if (chunk.getChunkId() == MAPIProperty.RTF_COMPRESSED.id) {
                    rtfChunk = chunk;
                }
                if (chunk.getChunkId() == MAPIProperty.BODY.id) {
                    textChunk = chunk;
                }
            }
            boolean doneBody = false;
            xhtml.startElement("div", "class", "message-body");
            if (htmlChunk != null) {
                byte[] data = null;
                if (htmlChunk instanceof ByteChunk) {
                    data = ((ByteChunk)htmlChunk).getValue();
                }
                else if (htmlChunk instanceof StringChunk) {
                    data = ((StringChunk)htmlChunk).getRawValue();
                }
                if (data != null) {
                    final HtmlParser htmlParser = new HtmlParser();
                    htmlParser.parse(new ByteArrayInputStream(data), new EmbeddedContentHandler(new BodyContentHandler(xhtml)), new Metadata(), new ParseContext());
                    doneBody = true;
                }
            }
            if (rtfChunk != null && !doneBody) {
                final ByteChunk chunk2 = (ByteChunk)rtfChunk;
                final MAPIRtfAttribute rtf = new MAPIRtfAttribute(MAPIProperty.RTF_COMPRESSED, 258, chunk2.getValue());
                final RTFParser rtfParser = new RTFParser();
                rtfParser.parse(new ByteArrayInputStream(rtf.getData()), new EmbeddedContentHandler(new BodyContentHandler(xhtml)), new Metadata(), new ParseContext());
                doneBody = true;
            }
            if (textChunk != null && !doneBody) {
                xhtml.element("p", ((StringChunk)textChunk).getValue());
            }
            xhtml.endElement("div");
            for (final AttachmentChunks attachment : this.msg.getAttachmentFiles()) {
                xhtml.startElement("div", "class", "attachment-entry");
                String filename = null;
                if (attachment.attachLongFileName != null) {
                    filename = attachment.attachLongFileName.getValue();
                }
                else if (attachment.attachFileName != null) {
                    filename = attachment.attachFileName.getValue();
                }
                if (filename != null && filename.length() > 0) {
                    xhtml.element("h1", filename);
                }
                if (attachment.attachData != null) {
                    this.handleEmbeddedResource(TikaInputStream.get(attachment.attachData.getValue()), filename, null, xhtml, true);
                }
                if (attachment.attachmentDirectory != null) {
                    this.handleEmbeddedOfficeDoc(attachment.attachmentDirectory.getDirectory(), xhtml);
                }
                xhtml.endElement("div");
            }
        }
        catch (ChunkNotFoundException e) {
            throw new TikaException("POI MAPIMessage broken - didn't return null on missing chunk", e);
        }
    }
    
    private void header(final XHTMLContentHandler xhtml, final String key, final String value) throws SAXException {
        if (value != null && value.length() > 0) {
            xhtml.element("dt", key);
            xhtml.element("dd", value);
        }
    }
}
