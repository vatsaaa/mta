// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.parser.microsoft;

import org.xml.sax.SAXException;
import org.apache.tika.sax.XHTMLContentHandler;
import java.text.NumberFormat;

public class NumberCell implements Cell
{
    private final double number;
    private final NumberFormat format;
    
    public NumberCell(final double number, final NumberFormat format) {
        this.number = number;
        this.format = format;
    }
    
    public void render(final XHTMLContentHandler handler) throws SAXException {
        handler.characters(this.format.format(this.number));
    }
    
    @Override
    public String toString() {
        return "Numeric Cell: " + this.format.format(this.number);
    }
}
