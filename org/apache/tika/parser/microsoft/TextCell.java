// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.parser.microsoft;

import org.xml.sax.SAXException;
import org.apache.tika.sax.XHTMLContentHandler;

public class TextCell implements Cell
{
    private final String text;
    
    public TextCell(final String text) {
        this.text = text;
    }
    
    public void render(final XHTMLContentHandler handler) throws SAXException {
        handler.characters(this.text);
    }
    
    @Override
    public String toString() {
        return "Text Cell: \"" + this.text + "\"";
    }
}
