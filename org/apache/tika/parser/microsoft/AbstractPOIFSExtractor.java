// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.parser.microsoft;

import org.apache.tika.mime.MimeType;
import org.apache.tika.mime.MediaType;
import org.apache.poi.poifs.filesystem.Entry;
import org.apache.tika.mime.MimeTypeException;
import org.apache.poi.poifs.filesystem.Ole10NativeException;
import org.apache.poi.poifs.filesystem.Ole10Native;
import org.apache.poi.poifs.filesystem.DirectoryNode;
import org.apache.tika.parser.pkg.ZipContainerDetector;
import org.apache.poi.poifs.filesystem.DocumentInputStream;
import org.apache.poi.poifs.filesystem.DocumentEntry;
import org.apache.poi.poifs.filesystem.DirectoryEntry;
import org.apache.tika.exception.TikaException;
import org.xml.sax.SAXException;
import java.io.IOException;
import org.xml.sax.ContentHandler;
import java.io.InputStream;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.sax.XHTMLContentHandler;
import org.apache.tika.io.TikaInputStream;
import org.apache.tika.extractor.ParsingEmbeddedDocumentExtractor;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.detect.Detector;
import org.apache.tika.mime.MimeTypes;
import org.apache.tika.config.TikaConfig;
import org.apache.tika.extractor.EmbeddedDocumentExtractor;

abstract class AbstractPOIFSExtractor
{
    private final EmbeddedDocumentExtractor extractor;
    private TikaConfig tikaConfig;
    private MimeTypes mimeTypes;
    private Detector detector;
    
    protected AbstractPOIFSExtractor(final ParseContext context) {
        final EmbeddedDocumentExtractor ex = context.get(EmbeddedDocumentExtractor.class);
        if (ex == null) {
            this.extractor = new ParsingEmbeddedDocumentExtractor(context);
        }
        else {
            this.extractor = ex;
        }
        this.tikaConfig = context.get(TikaConfig.class);
        this.mimeTypes = context.get(MimeTypes.class);
        this.detector = context.get(Detector.class);
    }
    
    protected TikaConfig getTikaConfig() {
        if (this.tikaConfig == null) {
            this.tikaConfig = TikaConfig.getDefaultConfig();
        }
        return this.tikaConfig;
    }
    
    protected Detector getDetector() {
        if (this.detector != null) {
            return this.detector;
        }
        return this.detector = this.getTikaConfig().getDetector();
    }
    
    protected MimeTypes getMimeTypes() {
        if (this.mimeTypes != null) {
            return this.mimeTypes;
        }
        return this.mimeTypes = this.getTikaConfig().getMimeRepository();
    }
    
    protected void handleEmbeddedResource(final TikaInputStream resource, final String filename, final String mediaType, final XHTMLContentHandler xhtml, final boolean outputHtml) throws IOException, SAXException, TikaException {
        try {
            final Metadata metadata = new Metadata();
            if (filename != null) {
                metadata.set("tika.mime.file", filename);
                metadata.set("resourceName", filename);
            }
            if (mediaType != null) {
                metadata.set("Content-Type", mediaType);
            }
            if (this.extractor.shouldParseEmbedded(metadata)) {
                this.extractor.parseEmbedded(resource, xhtml, metadata, outputHtml);
            }
        }
        finally {
            resource.close();
        }
    }
    
    protected void handleEmbeddedOfficeDoc(final DirectoryEntry dir, final XHTMLContentHandler xhtml) throws IOException, SAXException, TikaException {
        if (dir.hasEntry("Package")) {
            final Entry ooxml = dir.getEntry("Package");
            final TikaInputStream stream = TikaInputStream.get(new DocumentInputStream((DocumentEntry)ooxml));
            try {
                final ZipContainerDetector detector = new ZipContainerDetector();
                final MediaType type = detector.detect(stream, new Metadata());
                this.handleEmbeddedResource(stream, null, type.toString(), xhtml, true);
                return;
            }
            finally {
                stream.close();
            }
        }
        final Metadata metadata = new Metadata();
        final OfficeParser.POIFSDocumentType type2 = OfficeParser.POIFSDocumentType.detectType(dir);
        TikaInputStream embedded = null;
        try {
            Label_0408: {
                if (type2 == OfficeParser.POIFSDocumentType.OLE10_NATIVE) {
                    try {
                        final Ole10Native ole = Ole10Native.createFromEmbeddedOleObject((DirectoryNode)dir);
                        metadata.set("resourceName", dir.getName() + '/' + ole.getLabel());
                        final byte[] data = ole.getDataBuffer();
                        embedded = TikaInputStream.get(data);
                    }
                    catch (Ole10NativeException ex) {}
                }
                else {
                    if (type2 == OfficeParser.POIFSDocumentType.COMP_OBJ) {
                        try {
                            final DocumentEntry contentsEntry = (DocumentEntry)dir.getEntry("CONTENTS");
                            final DocumentInputStream inp = new DocumentInputStream(contentsEntry);
                            final byte[] contents = new byte[contentsEntry.getSize()];
                            inp.readFully(contents);
                            embedded = TikaInputStream.get(contents);
                            final MediaType mediaType = this.getDetector().detect(embedded, new Metadata());
                            String extension = type2.getExtension();
                            try {
                                final MimeType mimeType = this.getMimeTypes().forName(mediaType.toString());
                                extension = mimeType.getExtension();
                            }
                            catch (MimeTypeException ex2) {}
                            metadata.set("Content-Type", mediaType.getType().toString());
                            metadata.set("resourceName", dir.getName() + extension);
                            break Label_0408;
                        }
                        catch (Exception e) {
                            throw new TikaException("Invalid embedded resource", e);
                        }
                    }
                    metadata.set("Content-Type", type2.getType().toString());
                    metadata.set("resourceName", dir.getName() + '.' + type2.getExtension());
                }
            }
            if (this.extractor.shouldParseEmbedded(metadata)) {
                if (embedded == null) {
                    embedded = TikaInputStream.get(new byte[0]);
                    embedded.setOpenContainer(dir);
                }
                this.extractor.parseEmbedded(embedded, xhtml, metadata, true);
            }
        }
        finally {
            if (embedded != null) {
                embedded.close();
            }
        }
    }
}
