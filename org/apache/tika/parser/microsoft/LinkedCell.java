// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.parser.microsoft;

import org.xml.sax.SAXException;
import org.apache.tika.sax.XHTMLContentHandler;

public class LinkedCell extends CellDecorator
{
    private final String link;
    
    public LinkedCell(final Cell cell, final String link) {
        super(cell);
        assert link != null;
        this.link = link;
    }
    
    @Override
    public void render(final XHTMLContentHandler handler) throws SAXException {
        handler.startElement("a", "href", this.link);
        super.render(handler);
        handler.endElement("a");
    }
}
