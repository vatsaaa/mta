// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.parser.microsoft.ooxml;

import org.xml.sax.Locator;
import org.xml.sax.Attributes;
import org.apache.poi.xssf.usermodel.XSSFComment;
import org.apache.poi.xssf.model.CommentsTable;
import org.apache.tika.metadata.Metadata;
import org.apache.poi.openxml4j.opc.PackagePartName;
import org.apache.tika.exception.TikaException;
import org.apache.poi.openxml4j.opc.PackagingURIHelper;
import org.apache.poi.openxml4j.opc.TargetMode;
import org.apache.poi.openxml4j.opc.PackageRelationship;
import org.apache.poi.xssf.usermodel.XSSFRelation;
import org.xml.sax.XMLReader;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.ContentHandler;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.InputSource;
import org.apache.poi.ss.usermodel.HeaderFooter;
import org.apache.poi.hssf.extractor.ExcelExtractor;
import java.io.IOException;
import org.xml.sax.SAXException;
import java.util.Iterator;
import java.io.InputStream;
import org.apache.poi.xssf.model.StylesTable;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.xssf.eventusermodel.XSSFSheetXMLHandler;
import org.apache.poi.openxml4j.exceptions.OpenXML4JException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.xmlbeans.XmlException;
import org.apache.poi.xssf.eventusermodel.ReadOnlySharedStringsTable;
import org.apache.poi.xssf.eventusermodel.XSSFReader;
import org.apache.tika.sax.XHTMLContentHandler;
import java.util.ArrayList;
import org.apache.poi.POIXMLTextExtractor;
import java.util.Locale;
import org.apache.tika.parser.ParseContext;
import org.apache.poi.xssf.usermodel.helpers.HeaderFooterHelper;
import org.apache.poi.openxml4j.opc.PackagePart;
import java.util.List;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.xssf.extractor.XSSFEventBasedExcelExtractor;

public class XSSFExcelExtractorDecorator extends AbstractOOXMLExtractor
{
    private final XSSFEventBasedExcelExtractor extractor;
    private final DataFormatter formatter;
    private final List<PackagePart> sheetParts;
    private final List<Boolean> sheetProtected;
    private static HeaderFooterHelper hfHelper;
    
    public XSSFExcelExtractorDecorator(final ParseContext context, final XSSFEventBasedExcelExtractor extractor, final Locale locale) {
        super(context, extractor);
        this.sheetParts = new ArrayList<PackagePart>();
        this.sheetProtected = new ArrayList<Boolean>();
        (this.extractor = extractor).setFormulasNotResults(false);
        extractor.setLocale(locale);
        if (locale == null) {
            this.formatter = new DataFormatter();
        }
        else {
            this.formatter = new DataFormatter(locale);
        }
    }
    
    @Override
    protected void buildXHTML(final XHTMLContentHandler xhtml) throws SAXException, XmlException, IOException {
        final OPCPackage container = this.extractor.getPackage();
        StylesTable styles;
        XSSFReader.SheetIterator iter;
        ReadOnlySharedStringsTable strings;
        try {
            final XSSFReader xssfReader = new XSSFReader(container);
            styles = xssfReader.getStylesTable();
            iter = (XSSFReader.SheetIterator)xssfReader.getSheetsData();
            strings = new ReadOnlySharedStringsTable(container);
        }
        catch (InvalidFormatException e) {
            throw new XmlException(e);
        }
        catch (OpenXML4JException oe) {
            throw new XmlException(oe);
        }
        while (iter.hasNext()) {
            final InputStream stream = iter.next();
            this.sheetParts.add(iter.getSheetPart());
            final SheetTextAsHTML sheetExtractor = new SheetTextAsHTML(xhtml, iter.getSheetComments());
            xhtml.startElement("div");
            xhtml.element("h1", iter.getSheetName());
            xhtml.startElement("table");
            xhtml.startElement("tbody");
            this.processSheet(sheetExtractor, styles, strings, stream);
            xhtml.endElement("tbody");
            xhtml.endElement("table");
            for (final String header : sheetExtractor.headers) {
                this.extractHeaderFooter(header, xhtml);
            }
            for (final String footer : sheetExtractor.footers) {
                this.extractHeaderFooter(footer, xhtml);
            }
            xhtml.endElement("div");
        }
    }
    
    private void extractHeaderFooter(final String hf, final XHTMLContentHandler xhtml) throws SAXException {
        final String content = ExcelExtractor._extractHeaderFooter(new HeaderFooterFromString(hf));
        if (content.length() > 0) {
            xhtml.element("p", content);
        }
    }
    
    public void processSheet(final XSSFSheetXMLHandler.SheetContentsHandler sheetContentsExtractor, final StylesTable styles, final ReadOnlySharedStringsTable strings, final InputStream sheetInputStream) throws IOException, SAXException {
        final InputSource sheetSource = new InputSource(sheetInputStream);
        final SAXParserFactory saxFactory = SAXParserFactory.newInstance();
        try {
            final SAXParser saxParser = saxFactory.newSAXParser();
            final XMLReader sheetParser = saxParser.getXMLReader();
            final XSSFSheetInterestingPartsCapturer handler = new XSSFSheetInterestingPartsCapturer(new XSSFSheetXMLHandler(styles, strings, sheetContentsExtractor, this.formatter, false));
            sheetParser.setContentHandler(handler);
            sheetParser.parse(sheetSource);
            sheetInputStream.close();
            this.sheetProtected.add(handler.hasProtection);
        }
        catch (ParserConfigurationException e) {
            throw new RuntimeException("SAX parser appears to be broken - " + e.getMessage());
        }
    }
    
    @Override
    protected List<PackagePart> getMainDocumentParts() throws TikaException {
        final List<PackagePart> parts = new ArrayList<PackagePart>();
        for (final PackagePart part : this.sheetParts) {
            parts.add(part);
            try {
                for (final PackageRelationship rel : part.getRelationshipsByType(XSSFRelation.DRAWINGS.getRelation())) {
                    if (rel.getTargetMode() == TargetMode.INTERNAL) {
                        final PackagePartName relName = PackagingURIHelper.createPartName(rel.getTargetURI());
                        parts.add(rel.getPackage().getPart(relName));
                    }
                }
                for (final PackageRelationship rel : part.getRelationshipsByType(XSSFRelation.VML_DRAWINGS.getRelation())) {
                    if (rel.getTargetMode() == TargetMode.INTERNAL) {
                        final PackagePartName relName = PackagingURIHelper.createPartName(rel.getTargetURI());
                        parts.add(rel.getPackage().getPart(relName));
                    }
                }
            }
            catch (InvalidFormatException e) {
                throw new TikaException("Broken OOXML file", e);
            }
        }
        return parts;
    }
    
    @Override
    public MetadataExtractor getMetadataExtractor() {
        return new MetadataExtractor(this.extractor) {
            @Override
            public void extract(final Metadata metadata) throws TikaException {
                super.extract(metadata);
                metadata.set("protected", "false");
                for (final boolean prot : XSSFExcelExtractorDecorator.this.sheetProtected) {
                    if (prot) {
                        metadata.set("protected", "true");
                    }
                }
            }
        };
    }
    
    static {
        XSSFExcelExtractorDecorator.hfHelper = new HeaderFooterHelper();
    }
    
    protected static class SheetTextAsHTML implements XSSFSheetXMLHandler.SheetContentsHandler
    {
        private XHTMLContentHandler xhtml;
        private CommentsTable comments;
        private List<String> headers;
        private List<String> footers;
        
        protected SheetTextAsHTML(final XHTMLContentHandler xhtml, final CommentsTable comments) {
            this.xhtml = xhtml;
            this.comments = comments;
            this.headers = new ArrayList<String>();
            this.footers = new ArrayList<String>();
        }
        
        public void startRow(final int rowNum) {
            try {
                this.xhtml.startElement("tr");
            }
            catch (SAXException ex) {}
        }
        
        public void endRow() {
            try {
                this.xhtml.endElement("tr");
            }
            catch (SAXException ex) {}
        }
        
        public void cell(final String cellRef, final String formattedValue) {
            try {
                this.xhtml.startElement("td");
                this.xhtml.characters(formattedValue);
                if (this.comments != null) {
                    final XSSFComment comment = this.comments.findCellComment(cellRef);
                    if (comment != null) {
                        this.xhtml.startElement("br");
                        this.xhtml.endElement("br");
                        this.xhtml.characters(comment.getAuthor());
                        this.xhtml.characters(": ");
                        this.xhtml.characters(comment.getString().getString());
                    }
                }
                this.xhtml.endElement("td");
            }
            catch (SAXException ex) {}
        }
        
        public void headerFooter(final String text, final boolean isHeader, final String tagName) {
            if (isHeader) {
                this.headers.add(text);
            }
            else {
                this.footers.add(text);
            }
        }
    }
    
    protected static class HeaderFooterFromString implements HeaderFooter
    {
        private String text;
        
        protected HeaderFooterFromString(final String text) {
            this.text = text;
        }
        
        public String getCenter() {
            return XSSFExcelExtractorDecorator.hfHelper.getCenterSection(this.text);
        }
        
        public String getLeft() {
            return XSSFExcelExtractorDecorator.hfHelper.getLeftSection(this.text);
        }
        
        public String getRight() {
            return XSSFExcelExtractorDecorator.hfHelper.getRightSection(this.text);
        }
        
        public void setCenter(final String paramString) {
        }
        
        public void setLeft(final String paramString) {
        }
        
        public void setRight(final String paramString) {
        }
    }
    
    protected static class XSSFSheetInterestingPartsCapturer implements ContentHandler
    {
        private ContentHandler delegate;
        private boolean hasProtection;
        
        protected XSSFSheetInterestingPartsCapturer(final ContentHandler delegate) {
            this.hasProtection = false;
            this.delegate = delegate;
        }
        
        public void startElement(final String uri, final String localName, final String qName, final Attributes atts) throws SAXException {
            if ("sheetProtection".equals(qName)) {
                this.hasProtection = true;
            }
            this.delegate.startElement(uri, localName, qName, atts);
        }
        
        public void characters(final char[] ch, final int start, final int length) throws SAXException {
            this.delegate.characters(ch, start, length);
        }
        
        public void endDocument() throws SAXException {
            this.delegate.endDocument();
        }
        
        public void endElement(final String uri, final String localName, final String qName) throws SAXException {
            this.delegate.endElement(uri, localName, qName);
        }
        
        public void endPrefixMapping(final String prefix) throws SAXException {
            this.delegate.endPrefixMapping(prefix);
        }
        
        public void ignorableWhitespace(final char[] ch, final int start, final int length) throws SAXException {
            this.delegate.ignorableWhitespace(ch, start, length);
        }
        
        public void processingInstruction(final String target, final String data) throws SAXException {
            this.delegate.processingInstruction(target, data);
        }
        
        public void setDocumentLocator(final Locator locator) {
            this.delegate.setDocumentLocator(locator);
        }
        
        public void skippedEntity(final String name) throws SAXException {
            this.delegate.skippedEntity(name);
        }
        
        public void startDocument() throws SAXException {
            this.delegate.startDocument();
        }
        
        public void startPrefixMapping(final String prefix, final String uri) throws SAXException {
            this.delegate.startPrefixMapping(prefix, uri);
        }
    }
}
