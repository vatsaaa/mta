// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.parser.microsoft.ooxml;

import java.util.ArrayList;
import org.apache.poi.openxml4j.opc.PackagePart;
import java.util.List;
import org.xml.sax.SAXException;
import org.apache.tika.sax.XHTMLContentHandler;
import org.apache.poi.POIXMLTextExtractor;
import org.apache.tika.parser.ParseContext;

public class POIXMLTextExtractorDecorator extends AbstractOOXMLExtractor
{
    public POIXMLTextExtractorDecorator(final ParseContext context, final POIXMLTextExtractor extractor) {
        super(context, extractor);
    }
    
    @Override
    protected void buildXHTML(final XHTMLContentHandler xhtml) throws SAXException {
        xhtml.element("p", this.extractor.getText());
    }
    
    @Override
    protected List<PackagePart> getMainDocumentParts() {
        return new ArrayList<PackagePart>();
    }
}
