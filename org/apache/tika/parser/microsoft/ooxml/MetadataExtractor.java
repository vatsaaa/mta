// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.parser.microsoft.ooxml;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Iterator;
import org.apache.tika.metadata.Property;
import org.openxmlformats.schemas.officeDocument.x2006.customProperties.CTProperty;
import org.openxmlformats.schemas.officeDocument.x2006.extendedProperties.CTProperties;
import org.apache.tika.metadata.MSOffice;
import org.apache.tika.metadata.Office;
import org.apache.tika.metadata.PagedText;
import org.apache.tika.metadata.OfficeOpenXMLExtended;
import org.apache.poi.openxml4j.opc.internal.PackagePropertiesPart;
import org.apache.poi.openxml4j.util.Nullable;
import org.apache.tika.metadata.TikaCoreProperties;
import org.apache.tika.metadata.OfficeOpenXMLCore;
import org.apache.poi.POIXMLProperties;
import org.apache.tika.exception.TikaException;
import org.apache.poi.xssf.extractor.XSSFEventBasedExcelExtractor;
import org.apache.tika.metadata.Metadata;
import org.apache.poi.POIXMLTextExtractor;

public class MetadataExtractor
{
    private final POIXMLTextExtractor extractor;
    
    public MetadataExtractor(final POIXMLTextExtractor extractor) {
        this.extractor = extractor;
    }
    
    public void extract(final Metadata metadata) throws TikaException {
        if (this.extractor.getDocument() != null || (this.extractor instanceof XSSFEventBasedExcelExtractor && this.extractor.getPackage() != null)) {
            this.extractMetadata(this.extractor.getCoreProperties(), metadata);
            this.extractMetadata(this.extractor.getExtendedProperties(), metadata);
            this.extractMetadata(this.extractor.getCustomProperties(), metadata);
        }
    }
    
    private void extractMetadata(final POIXMLProperties.CoreProperties properties, final Metadata metadata) {
        final PackagePropertiesPart propsHolder = properties.getUnderlyingProperties();
        this.addProperty(metadata, OfficeOpenXMLCore.CATEGORY, propsHolder.getCategoryProperty());
        this.addProperty(metadata, OfficeOpenXMLCore.CONTENT_STATUS, propsHolder.getContentStatusProperty());
        this.addProperty(metadata, TikaCoreProperties.CREATED, propsHolder.getCreatedProperty());
        this.addProperty(metadata, TikaCoreProperties.CREATOR, propsHolder.getCreatorProperty());
        this.addProperty(metadata, TikaCoreProperties.DESCRIPTION, propsHolder.getDescriptionProperty());
        this.addProperty(metadata, TikaCoreProperties.IDENTIFIER, propsHolder.getIdentifierProperty());
        this.addProperty(metadata, TikaCoreProperties.KEYWORDS, propsHolder.getKeywordsProperty());
        this.addProperty(metadata, TikaCoreProperties.LANGUAGE, propsHolder.getLanguageProperty());
        this.addProperty(metadata, TikaCoreProperties.MODIFIER, propsHolder.getLastModifiedByProperty());
        this.addProperty(metadata, TikaCoreProperties.PRINT_DATE, propsHolder.getLastPrintedProperty());
        this.addProperty(metadata, Metadata.LAST_MODIFIED, propsHolder.getModifiedProperty());
        this.addProperty(metadata, TikaCoreProperties.MODIFIED, propsHolder.getModifiedProperty());
        this.addProperty(metadata, OfficeOpenXMLCore.REVISION, propsHolder.getRevisionProperty());
        this.addProperty(metadata, TikaCoreProperties.TRANSITION_SUBJECT_TO_OO_SUBJECT, propsHolder.getSubjectProperty());
        this.addProperty(metadata, TikaCoreProperties.TITLE, propsHolder.getTitleProperty());
        this.addProperty(metadata, OfficeOpenXMLCore.VERSION, propsHolder.getVersionProperty());
        this.addProperty(metadata, "Category", propsHolder.getCategoryProperty());
        this.addProperty(metadata, "Content-Status", propsHolder.getContentStatusProperty());
        this.addProperty(metadata, "Revision-Number", propsHolder.getRevisionProperty());
        this.addProperty(metadata, "Version", propsHolder.getVersionProperty());
    }
    
    private void extractMetadata(final POIXMLProperties.ExtendedProperties properties, final Metadata metadata) {
        final CTProperties propsHolder = properties.getUnderlyingProperties();
        this.addProperty(metadata, OfficeOpenXMLExtended.APPLICATION, propsHolder.getApplication());
        this.addProperty(metadata, OfficeOpenXMLExtended.APP_VERSION, propsHolder.getAppVersion());
        this.addProperty(metadata, TikaCoreProperties.PUBLISHER, propsHolder.getCompany());
        this.addProperty(metadata, OfficeOpenXMLExtended.COMPANY, propsHolder.getCompany());
        this.addProperty(metadata, OfficeOpenXMLExtended.MANAGER, propsHolder.getManager());
        this.addProperty(metadata, OfficeOpenXMLExtended.NOTES, propsHolder.getNotes());
        this.addProperty(metadata, OfficeOpenXMLExtended.PRESENTATION_FORMAT, propsHolder.getPresentationFormat());
        this.addProperty(metadata, OfficeOpenXMLExtended.TEMPLATE, propsHolder.getTemplate());
        this.addProperty(metadata, OfficeOpenXMLExtended.TOTAL_TIME, propsHolder.getTotalTime());
        if (propsHolder.getPages() > 0) {
            metadata.set(PagedText.N_PAGES, propsHolder.getPages());
        }
        else if (propsHolder.getSlides() > 0) {
            metadata.set(PagedText.N_PAGES, propsHolder.getSlides());
        }
        this.addProperty(metadata, Office.PAGE_COUNT, propsHolder.getPages());
        this.addProperty(metadata, Office.SLIDE_COUNT, propsHolder.getSlides());
        this.addProperty(metadata, Office.PARAGRAPH_COUNT, propsHolder.getParagraphs());
        this.addProperty(metadata, Office.LINE_COUNT, propsHolder.getLines());
        this.addProperty(metadata, Office.WORD_COUNT, propsHolder.getWords());
        this.addProperty(metadata, Office.CHARACTER_COUNT, propsHolder.getCharacters());
        this.addProperty(metadata, Office.CHARACTER_COUNT_WITH_SPACES, propsHolder.getCharactersWithSpaces());
        this.addProperty(metadata, "Application-Name", propsHolder.getApplication());
        this.addProperty(metadata, "Application-Version", propsHolder.getAppVersion());
        this.addProperty(metadata, "Manager", propsHolder.getManager());
        this.addProperty(metadata, "Notes", propsHolder.getNotes());
        this.addProperty(metadata, "Presentation-Format", propsHolder.getPresentationFormat());
        this.addProperty(metadata, "Template", propsHolder.getTemplate());
        this.addProperty(metadata, "Total-Time", propsHolder.getTotalTime());
        this.addProperty(metadata, MSOffice.PAGE_COUNT, propsHolder.getPages());
        this.addProperty(metadata, MSOffice.SLIDE_COUNT, propsHolder.getSlides());
        this.addProperty(metadata, MSOffice.PARAGRAPH_COUNT, propsHolder.getParagraphs());
        this.addProperty(metadata, MSOffice.LINE_COUNT, propsHolder.getLines());
        this.addProperty(metadata, MSOffice.WORD_COUNT, propsHolder.getWords());
        this.addProperty(metadata, MSOffice.CHARACTER_COUNT, propsHolder.getCharacters());
        this.addProperty(metadata, MSOffice.CHARACTER_COUNT_WITH_SPACES, propsHolder.getCharactersWithSpaces());
    }
    
    private void extractMetadata(final POIXMLProperties.CustomProperties properties, final Metadata metadata) {
        final org.openxmlformats.schemas.officeDocument.x2006.customProperties.CTProperties props = properties.getUnderlyingProperties();
        for (final CTProperty property : props.getPropertyList()) {
            String val = null;
            Date date = null;
            if (property.isSetLpwstr()) {
                val = property.getLpwstr();
            }
            else if (property.isSetLpstr()) {
                val = property.getLpstr();
            }
            else if (property.isSetDate()) {
                date = property.getDate().getTime();
            }
            else if (property.isSetFiletime()) {
                date = property.getFiletime().getTime();
            }
            else if (property.isSetBool()) {
                val = Boolean.toString(property.getBool());
            }
            else if (property.isSetI1()) {
                val = Integer.toString(property.getI1());
            }
            else if (property.isSetI2()) {
                val = Integer.toString(property.getI2());
            }
            else if (property.isSetI4()) {
                val = Integer.toString(property.getI4());
            }
            else if (property.isSetI8()) {
                val = Long.toString(property.getI8());
            }
            else if (property.isSetInt()) {
                val = Integer.toString(property.getInt());
            }
            else if (property.isSetUi1()) {
                val = Integer.toString(property.getUi1());
            }
            else if (property.isSetUi2()) {
                val = Integer.toString(property.getUi2());
            }
            else if (property.isSetUi4()) {
                val = Long.toString(property.getUi4());
            }
            else if (property.isSetUi8()) {
                val = property.getUi8().toString();
            }
            else if (property.isSetUint()) {
                val = Long.toString(property.getUint());
            }
            else if (property.isSetR4()) {
                val = Float.toString(property.getR4());
            }
            else if (property.isSetR8()) {
                val = Double.toString(property.getR8());
            }
            else if (property.isSetDecimal()) {
                final BigDecimal d = property.getDecimal();
                if (d == null) {
                    val = null;
                }
                else {
                    val = d.toPlainString();
                }
            }
            else if (!property.isSetArray()) {
                if (!property.isSetVector()) {
                    if (!property.isSetBlob()) {
                        if (!property.isSetOblob()) {
                            if (!property.isSetStream() && !property.isSetOstream()) {
                                if (!property.isSetVstream()) {
                                    if (property.isSetStorage() || property.isSetOstorage()) {}
                                }
                            }
                        }
                    }
                }
            }
            final String propName = "custom:" + property.getName();
            if (date != null) {
                final Property tikaProp = Property.externalDate(propName);
                metadata.set(tikaProp, date);
            }
            else {
                if (val == null) {
                    continue;
                }
                metadata.set(propName, val);
            }
        }
    }
    
    private <T> void addProperty(final Metadata metadata, final Property property, final Nullable<T> nullableValue) {
        final T value = nullableValue.getValue();
        if (value != null) {
            if (value instanceof Date) {
                metadata.set(property, (Date)value);
            }
            else if (value instanceof String) {
                metadata.set(property, (String)value);
            }
            else if (value instanceof Integer) {
                metadata.set(property, (int)value);
            }
            else if (value instanceof Double) {
                metadata.set(property, (double)value);
            }
        }
    }
    
    private void addProperty(final Metadata metadata, final String name, final Nullable<?> value) {
        if (value.getValue() != null) {
            this.addProperty(metadata, name, value.getValue().toString());
        }
    }
    
    private void addProperty(final Metadata metadata, final Property property, final String value) {
        if (value != null) {
            metadata.set(property, value);
        }
    }
    
    private void addProperty(final Metadata metadata, final String name, final String value) {
        if (value != null) {
            metadata.set(name, value);
        }
    }
    
    private void addProperty(final Metadata metadata, final Property property, final int value) {
        if (value > 0) {
            metadata.set(property, value);
        }
    }
    
    private void addProperty(final Metadata metadata, final String name, final int value) {
        if (value > 0) {
            metadata.set(name, Integer.toString(value));
        }
    }
}
