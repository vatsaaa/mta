// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.parser.microsoft.ooxml;

import org.xml.sax.SAXException;
import java.io.IOException;
import org.apache.poi.POIXMLDocument;
import org.apache.poi.POIXMLTextExtractor;
import org.apache.tika.mime.MediaType;
import org.apache.xmlbeans.XmlException;
import org.apache.poi.openxml4j.exceptions.OpenXML4JException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.tika.sax.EndDocumentShieldingContentHandler;
import org.apache.poi.xwpf.extractor.XWPFWordExtractor;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xslf.extractor.XSLFPowerPointExtractor;
import org.apache.poi.xslf.usermodel.XMLSlideShow;
import org.apache.tika.exception.TikaException;
import org.apache.poi.xssf.extractor.XSSFEventBasedExcelExtractor;
import org.apache.tika.parser.EmptyParser;
import org.apache.tika.parser.pkg.ZipContainerDetector;
import org.apache.tika.io.CloseShieldInputStream;
import org.apache.poi.openxml4j.opc.PackageAccess;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.tika.io.TikaInputStream;
import org.apache.poi.extractor.ExtractorFactory;
import java.util.Locale;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.metadata.Metadata;
import org.xml.sax.ContentHandler;
import java.io.InputStream;

public class OOXMLExtractorFactory
{
    public static void parse(final InputStream stream, final ContentHandler baseHandler, final Metadata metadata, final ParseContext context) throws IOException, SAXException, TikaException {
        final Locale locale = context.get(Locale.class, Locale.getDefault());
        ExtractorFactory.setThreadPrefersEventExtractors(true);
        try {
            final TikaInputStream tis = TikaInputStream.cast(stream);
            OPCPackage pkg;
            if (tis != null && tis.getOpenContainer() instanceof OPCPackage) {
                pkg = (OPCPackage)tis.getOpenContainer();
            }
            else if (tis != null && tis.hasFile()) {
                pkg = OPCPackage.open(tis.getFile().getPath(), PackageAccess.READ);
                tis.setOpenContainer(pkg);
            }
            else {
                final InputStream shield = new CloseShieldInputStream(stream);
                pkg = OPCPackage.open(shield);
            }
            final MediaType type = ZipContainerDetector.detectOfficeOpenXML(pkg);
            if (type == null || OOXMLParser.UNSUPPORTED_OOXML_TYPES.contains(type)) {
                EmptyParser.INSTANCE.parse(stream, baseHandler, metadata, context);
                return;
            }
            metadata.set("Content-Type", type.toString());
            final POIXMLTextExtractor poiExtractor = ExtractorFactory.createExtractor(pkg);
            final POIXMLDocument document = poiExtractor.getDocument();
            OOXMLExtractor extractor;
            if (poiExtractor instanceof XSSFEventBasedExcelExtractor) {
                extractor = new XSSFExcelExtractorDecorator(context, (XSSFEventBasedExcelExtractor)poiExtractor, locale);
            }
            else {
                if (document == null) {
                    throw new TikaException("Expecting UserModel based POI OOXML extractor with a document, but none found. The extractor returned was a " + poiExtractor);
                }
                if (document instanceof XMLSlideShow) {
                    extractor = new XSLFPowerPointExtractorDecorator(context, (XSLFPowerPointExtractor)poiExtractor);
                }
                else if (document instanceof XWPFDocument) {
                    extractor = new XWPFWordExtractorDecorator(context, (XWPFWordExtractor)poiExtractor);
                }
                else {
                    extractor = new POIXMLTextExtractorDecorator(context, poiExtractor);
                }
            }
            final EndDocumentShieldingContentHandler handler = new EndDocumentShieldingContentHandler(baseHandler);
            extractor.getXHTML(handler, metadata, context);
            extractor.getMetadataExtractor().extract(metadata);
            handler.reallyEndDocument();
        }
        catch (IllegalArgumentException e) {
            if (e.getMessage().startsWith("No supported documents found")) {
                throw new TikaException("TIKA-418: RuntimeException while getting content for thmx and xps file types", e);
            }
            throw new TikaException("Error creating OOXML extractor", e);
        }
        catch (InvalidFormatException e2) {
            throw new TikaException("Error creating OOXML extractor", e2);
        }
        catch (OpenXML4JException e3) {
            throw new TikaException("Error creating OOXML extractor", e3);
        }
        catch (XmlException e4) {
            throw new TikaException("Error creating OOXML extractor", e4);
        }
    }
}
