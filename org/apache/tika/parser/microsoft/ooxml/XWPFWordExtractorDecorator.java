// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.parser.microsoft.ooxml;

import java.util.ArrayList;
import org.apache.poi.openxml4j.opc.PackagePart;
import java.util.List;
import org.apache.poi.xwpf.usermodel.XWPFHeaderFooter;
import org.apache.poi.xwpf.usermodel.XWPFTableCell;
import org.apache.poi.xwpf.usermodel.XWPFTableRow;
import org.apache.poi.xwpf.usermodel.XWPFPictureData;
import org.apache.poi.xwpf.usermodel.XWPFHyperlink;
import org.apache.poi.xwpf.usermodel.XWPFStyle;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTSectPr;
import org.apache.poi.xwpf.model.XWPFParagraphDecorator;
import org.apache.poi.xwpf.model.XWPFCommentsDecorator;
import org.xml.sax.helpers.AttributesImpl;
import org.apache.poi.xwpf.usermodel.XWPFPicture;
import org.apache.poi.xwpf.usermodel.XWPFHyperlinkRun;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTBookmark;
import org.apache.tika.parser.microsoft.WordExtractor;
import org.apache.poi.xwpf.usermodel.BodyType;
import java.util.Iterator;
import org.apache.poi.xwpf.usermodel.XWPFTable;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.IBodyElement;
import java.io.IOException;
import org.apache.xmlbeans.XmlException;
import org.xml.sax.SAXException;
import org.apache.poi.xwpf.model.XWPFHeaderFooterPolicy;
import org.apache.poi.xwpf.usermodel.IBody;
import org.apache.tika.sax.XHTMLContentHandler;
import org.apache.poi.POIXMLTextExtractor;
import org.apache.poi.xwpf.extractor.XWPFWordExtractor;
import org.apache.tika.parser.ParseContext;
import org.apache.poi.xwpf.usermodel.XWPFStyles;
import org.apache.poi.xwpf.usermodel.XWPFDocument;

public class XWPFWordExtractorDecorator extends AbstractOOXMLExtractor
{
    private XWPFDocument document;
    private XWPFStyles styles;
    
    public XWPFWordExtractorDecorator(final ParseContext context, final XWPFWordExtractor extractor) {
        super(context, extractor);
        this.document = (XWPFDocument)extractor.getDocument();
        this.styles = this.document.getStyles();
    }
    
    @Override
    protected void buildXHTML(final XHTMLContentHandler xhtml) throws SAXException, XmlException, IOException {
        final XWPFHeaderFooterPolicy hfPolicy = this.document.getHeaderFooterPolicy();
        if (hfPolicy != null) {
            this.extractHeaders(xhtml, hfPolicy);
        }
        this.extractIBodyText(this.document, xhtml);
        if (hfPolicy != null) {
            this.extractFooters(xhtml, hfPolicy);
        }
    }
    
    private void extractIBodyText(final IBody bodyElement, final XHTMLContentHandler xhtml) throws SAXException, XmlException, IOException {
        for (final IBodyElement element : bodyElement.getBodyElements()) {
            if (element instanceof XWPFParagraph) {
                final XWPFParagraph paragraph = (XWPFParagraph)element;
                this.extractParagraph(paragraph, xhtml);
            }
            if (element instanceof XWPFTable) {
                final XWPFTable table = (XWPFTable)element;
                this.extractTable(table, xhtml);
            }
        }
    }
    
    private void extractParagraph(final XWPFParagraph paragraph, final XHTMLContentHandler xhtml) throws SAXException, XmlException, IOException {
        XWPFHeaderFooterPolicy headerFooterPolicy = null;
        if (paragraph.getCTP().getPPr() != null) {
            final CTSectPr ctSectPr = paragraph.getCTP().getPPr().getSectPr();
            if (ctSectPr != null) {
                headerFooterPolicy = new XWPFHeaderFooterPolicy(this.document, ctSectPr);
                this.extractHeaders(xhtml, headerFooterPolicy);
            }
        }
        String tag = "p";
        String styleClass = null;
        if (paragraph.getStyleID() != null) {
            final XWPFStyle style = this.styles.getStyle(paragraph.getStyleID());
            final WordExtractor.TagAndStyle tas = WordExtractor.buildParagraphTagAndStyle(style.getName(), paragraph.getPartType() == BodyType.TABLECELL);
            tag = tas.getTag();
            styleClass = tas.getStyleClass();
        }
        if (styleClass == null) {
            xhtml.startElement(tag);
        }
        else {
            xhtml.startElement(tag, "class", styleClass);
        }
        for (final CTBookmark bookmark : paragraph.getCTP().getBookmarkStartList()) {
            xhtml.startElement("a", "name", bookmark.getName());
            xhtml.endElement("a");
        }
        boolean curBold = false;
        boolean curItalic = false;
        for (final XWPFRun run : paragraph.getRuns()) {
            if (run.isBold() != curBold) {
                if (curItalic) {
                    xhtml.endElement("i");
                    curItalic = false;
                }
                if (run.isBold()) {
                    xhtml.startElement("b");
                }
                else {
                    xhtml.endElement("b");
                }
                curBold = run.isBold();
            }
            if (run.isItalic() != curItalic) {
                if (run.isItalic()) {
                    xhtml.startElement("i");
                }
                else {
                    xhtml.endElement("i");
                }
                curItalic = run.isItalic();
            }
            boolean addedHREF = false;
            if (run instanceof XWPFHyperlinkRun) {
                final XWPFHyperlinkRun linkRun = (XWPFHyperlinkRun)run;
                final XWPFHyperlink link = linkRun.getHyperlink(this.document);
                if (link != null && link.getURL() != null) {
                    xhtml.startElement("a", "href", link.getURL());
                    addedHREF = true;
                }
                else if (linkRun.getAnchor() != null && linkRun.getAnchor().length() > 0) {
                    xhtml.startElement("a", "href", "#" + linkRun.getAnchor());
                    addedHREF = true;
                }
            }
            xhtml.characters(run.toString());
            for (final XWPFPicture picture : run.getEmbeddedPictures()) {
                if (paragraph.getDocument() != null) {
                    final XWPFPictureData data = picture.getPictureData();
                    if (data == null) {
                        continue;
                    }
                    final AttributesImpl attr = new AttributesImpl();
                    attr.addAttribute("", "src", "src", "CDATA", "embedded:" + data.getFileName());
                    attr.addAttribute("", "alt", "alt", "CDATA", picture.getDescription());
                    xhtml.startElement("img", attr);
                    xhtml.endElement("img");
                }
            }
            if (addedHREF) {
                xhtml.endElement("a");
            }
        }
        if (curItalic) {
            xhtml.endElement("i");
            curItalic = false;
        }
        if (curBold) {
            xhtml.endElement("b");
            curBold = false;
        }
        final XWPFCommentsDecorator comments = new XWPFCommentsDecorator(paragraph, null);
        final String commentText = comments.getCommentText();
        if (commentText != null && commentText.length() > 0) {
            xhtml.characters(commentText);
        }
        final String footnameText = paragraph.getFootnoteText();
        if (footnameText != null && footnameText.length() > 0) {
            xhtml.characters(footnameText + "\n");
        }
        xhtml.endElement(tag);
        if (headerFooterPolicy != null) {
            this.extractFooters(xhtml, headerFooterPolicy);
        }
    }
    
    private void extractTable(final XWPFTable table, final XHTMLContentHandler xhtml) throws SAXException, XmlException, IOException {
        xhtml.startElement("table");
        xhtml.startElement("tbody");
        for (final XWPFTableRow row : table.getRows()) {
            xhtml.startElement("tr");
            for (final XWPFTableCell cell : row.getTableCells()) {
                xhtml.startElement("td");
                this.extractIBodyText(cell, xhtml);
                xhtml.endElement("td");
            }
            xhtml.endElement("tr");
        }
        xhtml.endElement("tbody");
        xhtml.endElement("table");
    }
    
    private void extractFooters(final XHTMLContentHandler xhtml, final XWPFHeaderFooterPolicy hfPolicy) throws SAXException, XmlException, IOException {
        if (hfPolicy.getFirstPageFooter() != null) {
            this.extractHeaderText(xhtml, hfPolicy.getFirstPageFooter());
        }
        if (hfPolicy.getEvenPageFooter() != null) {
            this.extractHeaderText(xhtml, hfPolicy.getEvenPageFooter());
        }
        if (hfPolicy.getDefaultFooter() != null) {
            this.extractHeaderText(xhtml, hfPolicy.getDefaultFooter());
        }
    }
    
    private void extractHeaders(final XHTMLContentHandler xhtml, final XWPFHeaderFooterPolicy hfPolicy) throws SAXException, XmlException, IOException {
        if (hfPolicy == null) {
            return;
        }
        if (hfPolicy.getFirstPageHeader() != null) {
            this.extractHeaderText(xhtml, hfPolicy.getFirstPageHeader());
        }
        if (hfPolicy.getEvenPageHeader() != null) {
            this.extractHeaderText(xhtml, hfPolicy.getEvenPageHeader());
        }
        if (hfPolicy.getDefaultHeader() != null) {
            this.extractHeaderText(xhtml, hfPolicy.getDefaultHeader());
        }
    }
    
    private void extractHeaderText(final XHTMLContentHandler xhtml, final XWPFHeaderFooter header) throws SAXException, XmlException, IOException {
        for (final XWPFParagraph p : header.getParagraphs()) {
            this.extractParagraph(p, xhtml);
        }
        for (final XWPFTable table : header.getTables()) {
            this.extractTable(table, xhtml);
        }
    }
    
    @Override
    protected List<PackagePart> getMainDocumentParts() {
        final List<PackagePart> parts = new ArrayList<PackagePart>();
        parts.add(this.document.getPackagePart());
        return parts;
    }
}
