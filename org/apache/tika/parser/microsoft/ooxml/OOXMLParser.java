// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.parser.microsoft.ooxml;

import java.util.Collections;
import java.util.Collection;
import java.util.HashSet;
import java.util.Arrays;
import org.apache.tika.exception.TikaException;
import org.xml.sax.SAXException;
import java.io.IOException;
import org.apache.tika.metadata.Metadata;
import org.xml.sax.ContentHandler;
import java.io.InputStream;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.mime.MediaType;
import java.util.Set;
import org.apache.tika.parser.AbstractParser;

public class OOXMLParser extends AbstractParser
{
    private static final long serialVersionUID = 6535995710857776481L;
    protected static final Set<MediaType> SUPPORTED_TYPES;
    protected static final Set<MediaType> UNSUPPORTED_OOXML_TYPES;
    
    public Set<MediaType> getSupportedTypes(final ParseContext context) {
        return OOXMLParser.SUPPORTED_TYPES;
    }
    
    public void parse(final InputStream stream, final ContentHandler handler, final Metadata metadata, final ParseContext context) throws IOException, SAXException, TikaException {
        OOXMLExtractorFactory.parse(stream, handler, metadata, context);
    }
    
    static {
        SUPPORTED_TYPES = Collections.unmodifiableSet((Set<? extends MediaType>)new HashSet<MediaType>(Arrays.asList(MediaType.application("x-tika-ooxml"), MediaType.application("vnd.openxmlformats-officedocument.presentationml.presentation"), MediaType.application("vnd.ms-powerpoint.presentation.macroenabled.12"), MediaType.application("vnd.openxmlformats-officedocument.presentationml.template"), MediaType.application("vnd.openxmlformats-officedocument.presentationml.slideshow"), MediaType.application("vnd.ms-powerpoint.slideshow.macroenabled.12"), MediaType.application("vnd.ms-powerpoint.addin.macroenabled.12"), MediaType.application("vnd.openxmlformats-officedocument.spreadsheetml.sheet"), MediaType.application("vnd.ms-excel.sheet.macroenabled.12"), MediaType.application("vnd.openxmlformats-officedocument.spreadsheetml.template"), MediaType.application("vnd.ms-excel.template.macroenabled.12"), MediaType.application("vnd.ms-excel.addin.macroenabled.12"), MediaType.application("vnd.openxmlformats-officedocument.wordprocessingml.document"), MediaType.application("vnd.ms-word.document.macroenabled.12"), MediaType.application("vnd.openxmlformats-officedocument.wordprocessingml.template"), MediaType.application("vnd.ms-word.template.macroenabled.12"))));
        UNSUPPORTED_OOXML_TYPES = Collections.unmodifiableSet((Set<? extends MediaType>)new HashSet<MediaType>(Arrays.asList(MediaType.application("vnd.ms-excel.sheet.binary.macroenabled.12"), MediaType.application("vnd.ms-xpsdocument"))));
    }
}
