// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.parser.microsoft.ooxml;

import org.apache.poi.openxml4j.opc.PackagePartName;
import org.apache.poi.xslf.XSLFSlideShow;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.openxml4j.opc.PackagingURIHelper;
import org.apache.poi.openxml4j.opc.TargetMode;
import org.apache.poi.openxml4j.opc.PackageRelationship;
import org.apache.poi.xslf.usermodel.XSLFRelation;
import org.apache.xmlbeans.XmlException;
import org.openxmlformats.schemas.presentationml.x2006.main.CTSlideIdListEntry;
import org.apache.tika.exception.TikaException;
import java.util.ArrayList;
import org.apache.poi.openxml4j.opc.PackagePart;
import java.util.List;
import org.apache.poi.xslf.usermodel.Placeholder;
import org.apache.poi.xslf.usermodel.XSLFTableCell;
import org.apache.poi.xslf.usermodel.XSLFTableRow;
import org.apache.poi.xslf.usermodel.XSLFTable;
import org.apache.poi.xslf.usermodel.XSLFGroupShape;
import org.apache.poi.xslf.usermodel.XSLFTextShape;
import org.apache.poi.xslf.usermodel.XSLFShape;
import java.io.IOException;
import org.xml.sax.SAXException;
import java.util.Iterator;
import org.apache.poi.xslf.usermodel.XSLFComments;
import org.apache.poi.xslf.usermodel.XSLFSheet;
import org.apache.poi.xslf.usermodel.XSLFSlide;
import org.openxmlformats.schemas.presentationml.x2006.main.CTComment;
import org.apache.poi.xslf.usermodel.XMLSlideShow;
import org.apache.tika.sax.XHTMLContentHandler;
import org.apache.poi.POIXMLTextExtractor;
import org.apache.poi.xslf.extractor.XSLFPowerPointExtractor;
import org.apache.tika.parser.ParseContext;

public class XSLFPowerPointExtractorDecorator extends AbstractOOXMLExtractor
{
    public XSLFPowerPointExtractorDecorator(final ParseContext context, final XSLFPowerPointExtractor extractor) {
        super(context, extractor);
    }
    
    @Override
    protected void buildXHTML(final XHTMLContentHandler xhtml) throws SAXException, IOException {
        final XMLSlideShow slideShow = (XMLSlideShow)this.extractor.getDocument();
        final XSLFSlide[] arr$;
        final XSLFSlide[] slides = arr$ = slideShow.getSlides();
        for (final XSLFSlide slide : arr$) {
            this.extractContent(slide.getShapes(), false, xhtml);
            final XSLFSheet slideLayout = slide.getMasterSheet();
            this.extractContent(slideLayout.getShapes(), true, xhtml);
            final XSLFSheet slideMaster = slideLayout.getMasterSheet();
            this.extractContent(slideMaster.getShapes(), true, xhtml);
            final XSLFSheet slideNotes = slide.getNotes();
            if (slideNotes != null) {
                this.extractContent(slideNotes.getShapes(), false, xhtml);
                final XSLFSheet notesMaster = slideNotes.getMasterSheet();
                this.extractContent(notesMaster.getShapes(), true, xhtml);
            }
            final XSLFComments comments = slide.getComments();
            if (comments != null) {
                for (final CTComment comment : comments.getCTCommentsList().getCmList()) {
                    xhtml.element("p", comment.getText());
                }
            }
        }
    }
    
    private void extractContent(final XSLFShape[] shapes, final boolean skipPlaceholders, final XHTMLContentHandler xhtml) throws SAXException {
        for (final XSLFShape sh : shapes) {
            if (sh instanceof XSLFTextShape) {
                final XSLFTextShape txt = (XSLFTextShape)sh;
                final Placeholder ph = txt.getTextType();
                if (!skipPlaceholders || ph == null) {
                    xhtml.element("p", txt.getText());
                }
            }
            else if (sh instanceof XSLFGroupShape) {
                final XSLFGroupShape group = (XSLFGroupShape)sh;
                this.extractContent(group.getShapes(), skipPlaceholders, xhtml);
            }
            else if (sh instanceof XSLFTable) {
                final XSLFTable tbl = (XSLFTable)sh;
                for (final XSLFTableRow row : tbl) {
                    final List<XSLFTableCell> cells = row.getCells();
                    this.extractContent(cells.toArray(new XSLFTableCell[cells.size()]), skipPlaceholders, xhtml);
                }
            }
        }
    }
    
    @Override
    protected List<PackagePart> getMainDocumentParts() throws TikaException {
        final List<PackagePart> parts = new ArrayList<PackagePart>();
        final XMLSlideShow slideShow = (XMLSlideShow)this.extractor.getDocument();
        XSLFSlideShow document = null;
        try {
            document = slideShow._getXSLFSlideShow();
        }
        catch (Exception e) {
            throw new TikaException(e.getMessage());
        }
        for (final CTSlideIdListEntry ctSlide : document.getSlideReferences().getSldIdList()) {
            PackagePart slidePart;
            try {
                slidePart = document.getSlidePart(ctSlide);
            }
            catch (IOException e2) {
                throw new TikaException("Broken OOXML file", e2);
            }
            catch (XmlException xe) {
                throw new TikaException("Broken OOXML file", xe);
            }
            parts.add(slidePart);
            try {
                for (final PackageRelationship rel : slidePart.getRelationshipsByType(XSLFRelation.VML_DRAWING.getRelation())) {
                    if (rel.getTargetMode() == TargetMode.INTERNAL) {
                        final PackagePartName relName = PackagingURIHelper.createPartName(rel.getTargetURI());
                        parts.add(rel.getPackage().getPart(relName));
                    }
                }
            }
            catch (InvalidFormatException e3) {
                throw new TikaException("Broken OOXML file", e3);
            }
        }
        return parts;
    }
}
