// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.parser.microsoft.ooxml;

import org.apache.tika.exception.TikaException;
import java.io.IOException;
import org.apache.xmlbeans.XmlException;
import org.xml.sax.SAXException;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.metadata.Metadata;
import org.xml.sax.ContentHandler;
import org.apache.poi.POIXMLDocument;

public interface OOXMLExtractor
{
    POIXMLDocument getDocument();
    
    MetadataExtractor getMetadataExtractor();
    
    void getXHTML(final ContentHandler p0, final Metadata p1, final ParseContext p2) throws SAXException, XmlException, IOException, TikaException;
}
