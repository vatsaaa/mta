// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.parser.microsoft.ooxml;

import java.util.List;
import org.apache.poi.poifs.filesystem.DirectoryNode;
import org.apache.poi.poifs.filesystem.Ole10NativeException;
import java.io.FileNotFoundException;
import org.apache.poi.poifs.filesystem.Ole10Native;
import org.apache.tika.sax.EmbeddedContentHandler;
import java.io.InputStream;
import org.apache.tika.io.TikaInputStream;
import org.apache.poi.poifs.filesystem.DirectoryEntry;
import org.apache.tika.parser.microsoft.OfficeParser;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import java.util.Iterator;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.openxml4j.opc.TargetMode;
import org.apache.poi.openxml4j.opc.PackageRelationship;
import org.apache.poi.openxml4j.opc.PackagePart;
import org.apache.tika.exception.TikaException;
import java.io.IOException;
import org.apache.xmlbeans.XmlException;
import org.xml.sax.SAXException;
import org.apache.tika.sax.XHTMLContentHandler;
import org.apache.tika.metadata.Metadata;
import org.xml.sax.ContentHandler;
import org.apache.poi.POIXMLDocument;
import org.apache.tika.extractor.ParsingEmbeddedDocumentExtractor;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.extractor.EmbeddedDocumentExtractor;
import org.apache.poi.POIXMLTextExtractor;

public abstract class AbstractOOXMLExtractor implements OOXMLExtractor
{
    static final String RELATION_AUDIO = "http://schemas.openxmlformats.org/officeDocument/2006/relationships/audio";
    static final String RELATION_IMAGE = "http://schemas.openxmlformats.org/officeDocument/2006/relationships/image";
    static final String RELATION_OLE_OBJECT = "http://schemas.openxmlformats.org/officeDocument/2006/relationships/oleObject";
    static final String RELATION_PACKAGE = "http://schemas.openxmlformats.org/officeDocument/2006/relationships/package";
    private static final String TYPE_OLE_OBJECT = "application/vnd.openxmlformats-officedocument.oleObject";
    protected POIXMLTextExtractor extractor;
    private final EmbeddedDocumentExtractor embeddedExtractor;
    
    public AbstractOOXMLExtractor(final ParseContext context, final POIXMLTextExtractor extractor) {
        this.extractor = extractor;
        final EmbeddedDocumentExtractor ex = context.get(EmbeddedDocumentExtractor.class);
        if (ex == null) {
            this.embeddedExtractor = new ParsingEmbeddedDocumentExtractor(context);
        }
        else {
            this.embeddedExtractor = ex;
        }
    }
    
    public POIXMLDocument getDocument() {
        return this.extractor.getDocument();
    }
    
    public MetadataExtractor getMetadataExtractor() {
        return new MetadataExtractor(this.extractor);
    }
    
    public void getXHTML(final ContentHandler handler, final Metadata metadata, final ParseContext context) throws SAXException, XmlException, IOException, TikaException {
        final XHTMLContentHandler xhtml = new XHTMLContentHandler(handler, metadata);
        xhtml.startDocument();
        this.buildXHTML(xhtml);
        this.handleEmbeddedParts(handler);
        xhtml.endDocument();
    }
    
    private void handleEmbeddedParts(final ContentHandler handler) throws TikaException, IOException, SAXException {
        try {
            for (final PackagePart source : this.getMainDocumentParts()) {
                for (final PackageRelationship rel : source.getRelationships()) {
                    if (rel.getTargetMode() == TargetMode.INTERNAL) {
                        PackagePart target;
                        try {
                            target = source.getRelatedPart(rel);
                        }
                        catch (IllegalArgumentException ex) {
                            continue;
                        }
                        final String type = rel.getRelationshipType();
                        if ("http://schemas.openxmlformats.org/officeDocument/2006/relationships/oleObject".equals(type) && "application/vnd.openxmlformats-officedocument.oleObject".equals(target.getContentType())) {
                            this.handleEmbeddedOLE(target, handler);
                        }
                        else {
                            if (!"http://schemas.openxmlformats.org/officeDocument/2006/relationships/audio".equals(type) && !"http://schemas.openxmlformats.org/officeDocument/2006/relationships/image".equals(type) && !"http://schemas.openxmlformats.org/officeDocument/2006/relationships/package".equals(type) && !"http://schemas.openxmlformats.org/officeDocument/2006/relationships/oleObject".equals(type)) {
                                continue;
                            }
                            this.handleEmbeddedFile(target, handler);
                        }
                    }
                }
            }
        }
        catch (InvalidFormatException e) {
            throw new TikaException("Broken OOXML file", e);
        }
    }
    
    private void handleEmbeddedOLE(final PackagePart part, final ContentHandler handler) throws IOException, SAXException {
        final POIFSFileSystem fs = new POIFSFileSystem(part.getInputStream());
        try {
            final Metadata metadata = new Metadata();
            TikaInputStream stream = null;
            final DirectoryNode root = fs.getRoot();
            final OfficeParser.POIFSDocumentType type = OfficeParser.POIFSDocumentType.detectType(root);
            if (root.hasEntry("CONTENTS") && root.hasEntry("\u0001Ole") && root.hasEntry("\u0001CompObj") && root.hasEntry("\u0003ObjInfo")) {
                stream = TikaInputStream.get(fs.createDocumentInputStream("CONTENTS"));
                if (this.embeddedExtractor.shouldParseEmbedded(metadata)) {
                    this.embeddedExtractor.parseEmbedded(stream, new EmbeddedContentHandler(handler), metadata, false);
                }
            }
            else if (OfficeParser.POIFSDocumentType.OLE10_NATIVE == type) {
                final Ole10Native ole = Ole10Native.createFromEmbeddedOleObject(fs);
                metadata.set("resourceName", ole.getLabel());
                final byte[] data = ole.getDataBuffer();
                if (data != null) {
                    stream = TikaInputStream.get(data);
                }
                if (stream != null && this.embeddedExtractor.shouldParseEmbedded(metadata)) {
                    this.embeddedExtractor.parseEmbedded(stream, new EmbeddedContentHandler(handler), metadata, false);
                }
            }
            else {
                this.handleEmbeddedFile(part, handler);
            }
        }
        catch (FileNotFoundException e) {}
        catch (Ole10NativeException ex) {}
    }
    
    protected void handleEmbeddedFile(final PackagePart part, final ContentHandler handler) throws SAXException, IOException {
        final Metadata metadata = new Metadata();
        final String name = part.getPartName().getName();
        metadata.set("resourceName", name.substring(name.lastIndexOf(47) + 1));
        metadata.set("Content-Type", part.getContentType());
        if (this.embeddedExtractor.shouldParseEmbedded(metadata)) {
            this.embeddedExtractor.parseEmbedded(TikaInputStream.get(part.getInputStream()), new EmbeddedContentHandler(handler), metadata, false);
        }
    }
    
    protected abstract void buildXHTML(final XHTMLContentHandler p0) throws SAXException, XmlException, IOException;
    
    protected abstract List<PackagePart> getMainDocumentParts() throws TikaException;
}
