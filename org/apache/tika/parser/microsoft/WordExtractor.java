// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.parser.microsoft;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.HashMap;
import org.apache.poi.hwpf.extractor.Word6Extractor;
import org.apache.poi.hwpf.HWPFOldDocument;
import org.apache.tika.io.TikaInputStream;
import org.xml.sax.helpers.AttributesImpl;
import java.util.List;
import java.util.ArrayList;
import org.apache.poi.hwpf.model.StyleDescription;
import org.apache.poi.hwpf.usermodel.TableCell;
import org.apache.poi.hwpf.usermodel.TableRow;
import org.apache.poi.hwpf.usermodel.Table;
import java.util.Iterator;
import org.apache.poi.hwpf.usermodel.Picture;
import org.apache.poi.hwpf.usermodel.Paragraph;
import org.apache.poi.hwpf.usermodel.Range;
import org.apache.poi.hwpf.model.PicturesTable;
import java.io.FileNotFoundException;
import org.apache.poi.poifs.filesystem.Entry;
import org.apache.poi.poifs.filesystem.DirectoryEntry;
import org.apache.poi.hwpf.usermodel.CharacterRun;
import org.apache.poi.hwpf.OldWordFileFormatException;
import org.apache.poi.hwpf.HWPFDocument;
import org.apache.poi.poifs.filesystem.DirectoryNode;
import org.apache.tika.exception.TikaException;
import org.xml.sax.SAXException;
import java.io.IOException;
import org.apache.tika.sax.XHTMLContentHandler;
import org.apache.poi.poifs.filesystem.NPOIFSFileSystem;
import org.apache.tika.parser.ParseContext;
import java.util.Map;

public class WordExtractor extends AbstractPOIFSExtractor
{
    private static final char UNICODECHAR_NONBREAKING_HYPHEN = '\u2011';
    private static final char UNICODECHAR_ZERO_WIDTH_SPACE = '\u200b';
    private boolean curStrikeThrough;
    private boolean curBold;
    private boolean curItalic;
    private static final Map<String, TagAndStyle> fixedParagraphStyles;
    private static final TagAndStyle defaultParagraphStyle;
    
    public WordExtractor(final ParseContext context) {
        super(context);
    }
    
    protected void parse(final NPOIFSFileSystem filesystem, final XHTMLContentHandler xhtml) throws IOException, SAXException, TikaException {
        this.parse(filesystem.getRoot(), xhtml);
    }
    
    protected void parse(final DirectoryNode root, final XHTMLContentHandler xhtml) throws IOException, SAXException, TikaException {
        HWPFDocument document;
        try {
            document = new HWPFDocument(root);
        }
        catch (OldWordFileFormatException e) {
            this.parseWord6(root, xhtml);
            return;
        }
        final org.apache.poi.hwpf.extractor.WordExtractor wordExtractor = new org.apache.poi.hwpf.extractor.WordExtractor(document);
        this.addTextIfAny(xhtml, "header", wordExtractor.getHeaderText());
        final PicturesTable pictureTable = document.getPicturesTable();
        final PicturesSource pictures = new PicturesSource(document);
        final Range r = document.getRange();
        Paragraph p;
        for (int i = 0; i < r.numParagraphs(); i += this.handleParagraph(p, 0, r, document, pictures, pictureTable, xhtml), ++i) {
            p = r.getParagraph(i);
        }
        for (final String paragraph : wordExtractor.getMainTextboxText()) {
            xhtml.element("p", paragraph);
        }
        for (final String paragraph : wordExtractor.getFootnoteText()) {
            xhtml.element("p", paragraph);
        }
        for (final String paragraph : wordExtractor.getCommentsText()) {
            xhtml.element("p", paragraph);
        }
        for (final String paragraph : wordExtractor.getEndnoteText()) {
            xhtml.element("p", paragraph);
        }
        this.addTextIfAny(xhtml, "footer", wordExtractor.getFooterText());
        for (Picture p2 = pictures.nextUnclaimed(); p2 != null; p2 = pictures.nextUnclaimed()) {
            this.handlePictureCharacterRun(null, p2, pictures, xhtml);
        }
        try {
            final DirectoryEntry op = (DirectoryEntry)root.getEntry("ObjectPool");
            for (final Entry entry : op) {
                if (entry.getName().startsWith("_") && entry instanceof DirectoryEntry) {
                    this.handleEmbeddedOfficeDoc((DirectoryEntry)entry, xhtml);
                }
            }
        }
        catch (FileNotFoundException ex) {}
    }
    
    private int handleParagraph(final Paragraph p, final int parentTableLevel, final Range r, final HWPFDocument document, final PicturesSource pictures, final PicturesTable pictureTable, final XHTMLContentHandler xhtml) throws SAXException, IOException, TikaException {
        if (p.isInTable() && p.getTableLevel() > parentTableLevel && parentTableLevel == 0) {
            final Table t = r.getTable(p);
            xhtml.startElement("table");
            xhtml.startElement("tbody");
            for (int rn = 0; rn < t.numRows(); ++rn) {
                final TableRow row = t.getRow(rn);
                xhtml.startElement("tr");
                for (int cn = 0; cn < row.numCells(); ++cn) {
                    final TableCell cell = row.getCell(cn);
                    xhtml.startElement("td");
                    for (int pn = 0; pn < cell.numParagraphs(); ++pn) {
                        final Paragraph cellP = cell.getParagraph(pn);
                        this.handleParagraph(cellP, p.getTableLevel(), cell, document, pictures, pictureTable, xhtml);
                    }
                    xhtml.endElement("td");
                }
                xhtml.endElement("tr");
            }
            xhtml.endElement("tbody");
            xhtml.endElement("table");
            return t.numParagraphs() - 1;
        }
        TagAndStyle tas;
        if (document.getStyleSheet().numStyles() > p.getStyleIndex()) {
            final StyleDescription style = document.getStyleSheet().getStyleDescription(p.getStyleIndex());
            if (style != null) {
                tas = buildParagraphTagAndStyle(style.getName(), parentTableLevel > 0);
            }
            else {
                tas = new TagAndStyle("p", null);
            }
        }
        else {
            tas = new TagAndStyle("p", null);
        }
        if (tas.getStyleClass() != null) {
            xhtml.startElement(tas.getTag(), "class", tas.getStyleClass());
        }
        else {
            xhtml.startElement(tas.getTag());
        }
        for (int j = 0; j < p.numCharacterRuns(); ++j) {
            final CharacterRun cr = p.getCharacterRun(j);
            if (cr.text().equals("\u0013")) {
                j += this.handleSpecialCharacterRuns(p, j, tas.isHeading(), pictures, xhtml);
            }
            else if (cr.text().startsWith("\b")) {
                for (int pn2 = 0; pn2 < cr.text().length(); ++pn2) {
                    final Picture picture = pictures.nextUnclaimed();
                    this.handlePictureCharacterRun(cr, picture, pictures, xhtml);
                }
            }
            else if (pictureTable.hasPicture(cr)) {
                final Picture picture2 = pictures.getFor(cr);
                this.handlePictureCharacterRun(cr, picture2, pictures, xhtml);
            }
            else {
                this.handleCharacterRun(cr, tas.isHeading(), xhtml);
            }
        }
        if (this.curStrikeThrough) {
            xhtml.endElement("s");
            this.curStrikeThrough = false;
        }
        if (this.curItalic) {
            xhtml.endElement("i");
            this.curItalic = false;
        }
        if (this.curBold) {
            xhtml.endElement("b");
            this.curBold = false;
        }
        xhtml.endElement(tas.getTag());
        return 0;
    }
    
    private void handleCharacterRun(final CharacterRun cr, final boolean skipStyling, final XHTMLContentHandler xhtml) throws SAXException {
        if (!this.isRendered(cr) || cr.text().equals("\r")) {
            return;
        }
        if (!skipStyling) {
            if (cr.isBold() != this.curBold) {
                if (this.curStrikeThrough) {
                    xhtml.endElement("s");
                    this.curStrikeThrough = false;
                }
                if (this.curItalic) {
                    xhtml.endElement("i");
                    this.curItalic = false;
                }
                if (cr.isBold()) {
                    xhtml.startElement("b");
                }
                else {
                    xhtml.endElement("b");
                }
                this.curBold = cr.isBold();
            }
            if (cr.isItalic() != this.curItalic) {
                if (this.curStrikeThrough) {
                    xhtml.endElement("s");
                    this.curStrikeThrough = false;
                }
                if (cr.isItalic()) {
                    xhtml.startElement("i");
                }
                else {
                    xhtml.endElement("i");
                }
                this.curItalic = cr.isItalic();
            }
            if (cr.isStrikeThrough() != this.curStrikeThrough) {
                if (cr.isStrikeThrough()) {
                    xhtml.startElement("s");
                }
                else {
                    xhtml.endElement("s");
                }
                this.curStrikeThrough = cr.isStrikeThrough();
            }
        }
        String text = cr.text();
        text = text.replace('\r', '\n');
        if (text.endsWith("\u0007")) {
            text = text.substring(0, text.length() - 1);
        }
        text = text.replace('\u001e', '\u2011');
        text = text.replace('\u001f', '\u200b');
        xhtml.characters(text);
    }
    
    private int handleSpecialCharacterRuns(final Paragraph p, final int index, final boolean skipStyling, final PicturesSource pictures, final XHTMLContentHandler xhtml) throws SAXException, TikaException, IOException {
        List<CharacterRun> controls = new ArrayList<CharacterRun>();
        List<CharacterRun> texts = new ArrayList<CharacterRun>();
        boolean has14 = false;
        int i;
        for (i = index + 1; i < p.numCharacterRuns(); ++i) {
            final CharacterRun cr = p.getCharacterRun(i);
            if (cr.text().equals("\u0013")) {
                final int increment = this.handleSpecialCharacterRuns(p, i + 1, skipStyling, pictures, xhtml);
                i += increment;
            }
            else if (cr.text().equals("\u0014")) {
                has14 = true;
            }
            else if (cr.text().equals("\u0015")) {
                if (!has14) {
                    texts = controls;
                    controls = new ArrayList<CharacterRun>();
                    break;
                }
                break;
            }
            else if (has14) {
                texts.add(cr);
            }
            else {
                controls.add(cr);
            }
        }
        if (controls.size() > 0) {
            String text = controls.get(0).text();
            for (int j = 1; j < controls.size(); ++j) {
                text += controls.get(j).text();
            }
            if (text.startsWith("HYPERLINK") && text.indexOf(34) > -1) {
                final String url = text.substring(text.indexOf(34) + 1, text.lastIndexOf(34));
                xhtml.startElement("a", "href", url);
                for (final CharacterRun cr2 : texts) {
                    this.handleCharacterRun(cr2, skipStyling, xhtml);
                }
                xhtml.endElement("a");
            }
            else {
                for (final CharacterRun cr3 : texts) {
                    if (pictures.hasPicture(cr3)) {
                        final Picture picture = pictures.getFor(cr3);
                        this.handlePictureCharacterRun(cr3, picture, pictures, xhtml);
                    }
                    else {
                        this.handleCharacterRun(cr3, skipStyling, xhtml);
                    }
                }
            }
        }
        else {
            for (final CharacterRun cr4 : texts) {
                this.handleCharacterRun(cr4, skipStyling, xhtml);
            }
        }
        return i - index;
    }
    
    private void handlePictureCharacterRun(final CharacterRun cr, final Picture picture, final PicturesSource pictures, final XHTMLContentHandler xhtml) throws SAXException, IOException, TikaException {
        if (!this.isRendered(cr) || picture == null) {
            return;
        }
        final String extension = picture.suggestFileExtension();
        final int pictureNumber = pictures.pictureNumber(picture);
        final String filename = "image" + pictureNumber + ((extension.length() > 0) ? ("." + extension) : "");
        final String mimeType = picture.getMimeType();
        final AttributesImpl attr = new AttributesImpl();
        attr.addAttribute("", "src", "src", "CDATA", "embedded:" + filename);
        attr.addAttribute("", "alt", "alt", "CDATA", filename);
        xhtml.startElement("img", attr);
        xhtml.endElement("img");
        if (!pictures.hasOutput(picture)) {
            final TikaInputStream stream = TikaInputStream.get(picture.getContent());
            this.handleEmbeddedResource(stream, filename, mimeType, xhtml, false);
            pictures.recordOutput(picture);
        }
    }
    
    private void addTextIfAny(final XHTMLContentHandler xhtml, final String section, final String text) throws SAXException {
        if (text != null && text.length() > 0) {
            xhtml.startElement("div", "class", section);
            xhtml.element("p", text);
            xhtml.endElement("div");
        }
    }
    
    protected void parseWord6(final NPOIFSFileSystem filesystem, final XHTMLContentHandler xhtml) throws IOException, SAXException, TikaException {
        this.parseWord6(filesystem.getRoot(), xhtml);
    }
    
    protected void parseWord6(final DirectoryNode root, final XHTMLContentHandler xhtml) throws IOException, SAXException, TikaException {
        final HWPFOldDocument doc = new HWPFOldDocument(root);
        final Word6Extractor extractor = new Word6Extractor(doc);
        for (final String p : extractor.getParagraphText()) {
            xhtml.element("p", p);
        }
    }
    
    public static TagAndStyle buildParagraphTagAndStyle(final String styleName, final boolean isTable) {
        final TagAndStyle tagAndStyle = WordExtractor.fixedParagraphStyles.get(styleName);
        if (tagAndStyle != null) {
            return tagAndStyle;
        }
        if (styleName.equals("Table Contents") && isTable) {
            return WordExtractor.defaultParagraphStyle;
        }
        String tag = "p";
        String styleClass = null;
        if (styleName.startsWith("heading") || styleName.startsWith("Heading")) {
            int num = 1;
            try {
                num = Integer.parseInt(styleName.substring(styleName.length() - 1));
            }
            catch (NumberFormatException ex) {}
            tag = "h" + Math.min(num, 6);
        }
        else {
            styleClass = styleName.replace(' ', '_');
            styleClass = styleClass.substring(0, 1).toLowerCase() + styleClass.substring(1);
        }
        return new TagAndStyle(tag, styleClass);
    }
    
    private boolean isRendered(final CharacterRun cr) {
        return cr == null || !cr.isMarkedDeleted();
    }
    
    static {
        fixedParagraphStyles = new HashMap<String, TagAndStyle>();
        defaultParagraphStyle = new TagAndStyle("p", null);
        WordExtractor.fixedParagraphStyles.put("Default", WordExtractor.defaultParagraphStyle);
        WordExtractor.fixedParagraphStyles.put("Normal", WordExtractor.defaultParagraphStyle);
        WordExtractor.fixedParagraphStyles.put("heading", new TagAndStyle("h1", null));
        WordExtractor.fixedParagraphStyles.put("Heading", new TagAndStyle("h1", null));
        WordExtractor.fixedParagraphStyles.put("Title", new TagAndStyle("h1", "title"));
        WordExtractor.fixedParagraphStyles.put("Subtitle", new TagAndStyle("h2", "subtitle"));
        WordExtractor.fixedParagraphStyles.put("HTML Preformatted", new TagAndStyle("pre", null));
    }
    
    public static class TagAndStyle
    {
        private String tag;
        private String styleClass;
        
        public TagAndStyle(final String tag, final String styleClass) {
            this.tag = tag;
            this.styleClass = styleClass;
        }
        
        public String getTag() {
            return this.tag;
        }
        
        public String getStyleClass() {
            return this.styleClass;
        }
        
        public boolean isHeading() {
            return this.tag.length() == 2 && this.tag.startsWith("h");
        }
    }
    
    private static class PicturesSource
    {
        private PicturesTable picturesTable;
        private Set<Picture> output;
        private Map<Integer, Picture> lookup;
        private List<Picture> nonU1based;
        private List<Picture> all;
        private int pn;
        
        private PicturesSource(final HWPFDocument doc) {
            this.output = new HashSet<Picture>();
            this.pn = 0;
            this.picturesTable = doc.getPicturesTable();
            this.all = this.picturesTable.getAllPictures();
            this.lookup = new HashMap<Integer, Picture>();
            for (final Picture p : this.all) {
                this.lookup.put(p.getStartOffset(), p);
            }
            (this.nonU1based = new ArrayList<Picture>()).addAll(this.all);
            final Range r = doc.getRange();
            for (int i = 0; i < r.numCharacterRuns(); ++i) {
                final CharacterRun cr = r.getCharacterRun(i);
                if (this.picturesTable.hasPicture(cr)) {
                    final Picture p2 = this.getFor(cr);
                    final int at = this.nonU1based.indexOf(p2);
                    this.nonU1based.set(at, null);
                }
            }
        }
        
        private boolean hasPicture(final CharacterRun cr) {
            return this.picturesTable.hasPicture(cr);
        }
        
        private void recordOutput(final Picture picture) {
            this.output.add(picture);
        }
        
        private boolean hasOutput(final Picture picture) {
            return this.output.contains(picture);
        }
        
        private int pictureNumber(final Picture picture) {
            return this.all.indexOf(picture) + 1;
        }
        
        private Picture getFor(final CharacterRun cr) {
            return this.lookup.get(cr.getPicOffset());
        }
        
        private Picture nextUnclaimed() {
            Picture p = null;
            while (this.pn < this.nonU1based.size()) {
                p = this.nonU1based.get(this.pn);
                ++this.pn;
                if (p != null) {
                    return p;
                }
            }
            return null;
        }
    }
}
