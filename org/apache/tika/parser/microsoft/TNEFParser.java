// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.parser.microsoft;

import java.util.Collections;
import java.util.Collection;
import java.util.HashSet;
import java.util.Arrays;
import org.apache.tika.sax.EmbeddedContentHandler;
import org.apache.tika.io.TikaInputStream;
import org.apache.tika.exception.TikaException;
import org.xml.sax.SAXException;
import java.io.IOException;
import java.util.Iterator;
import org.apache.poi.hmef.attribute.MAPIAttribute;
import org.apache.poi.hmef.Attachment;
import org.apache.poi.hmef.attribute.MAPIRtfAttribute;
import org.apache.poi.hsmf.datatypes.MAPIProperty;
import org.apache.tika.metadata.TikaCoreProperties;
import org.apache.poi.hmef.HMEFMessage;
import org.apache.tika.extractor.ParsingEmbeddedDocumentExtractor;
import org.apache.tika.extractor.EmbeddedDocumentExtractor;
import org.apache.tika.metadata.Metadata;
import org.xml.sax.ContentHandler;
import java.io.InputStream;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.mime.MediaType;
import java.util.Set;
import org.apache.tika.parser.AbstractParser;

public class TNEFParser extends AbstractParser
{
    private static final long serialVersionUID = 4611820730372823452L;
    private static final Set<MediaType> SUPPORTED_TYPES;
    
    public Set<MediaType> getSupportedTypes(final ParseContext context) {
        return TNEFParser.SUPPORTED_TYPES;
    }
    
    public void parse(final InputStream stream, final ContentHandler handler, final Metadata metadata, final ParseContext context) throws IOException, SAXException, TikaException {
        final EmbeddedDocumentExtractor ex = context.get(EmbeddedDocumentExtractor.class);
        EmbeddedDocumentExtractor embeddedExtractor;
        if (ex == null) {
            embeddedExtractor = new ParsingEmbeddedDocumentExtractor(context);
        }
        else {
            embeddedExtractor = ex;
        }
        final HMEFMessage msg = new HMEFMessage(stream);
        final String subject = msg.getSubject();
        if (subject != null && subject.length() > 0) {
            metadata.set(TikaCoreProperties.TRANSITION_SUBJECT_TO_DC_TITLE, subject);
        }
        final MAPIAttribute attr = msg.getMessageMAPIAttribute(MAPIProperty.RTF_COMPRESSED);
        if (attr != null && attr instanceof MAPIRtfAttribute) {
            final MAPIRtfAttribute rtf = (MAPIRtfAttribute)attr;
            this.handleEmbedded("message.rtf", "application/rtf", rtf.getData(), embeddedExtractor, handler);
        }
        for (final Attachment attachment : msg.getAttachments()) {
            String name = attachment.getLongFilename();
            if (name == null || name.length() == 0) {
                name = attachment.getFilename();
            }
            if (name == null || name.length() == 0) {
                final String ext = attachment.getExtension();
                if (ext != null) {
                    name = "unknown" + ext;
                }
            }
            this.handleEmbedded(name, null, attachment.getContents(), embeddedExtractor, handler);
        }
    }
    
    private void handleEmbedded(final String name, final String type, final byte[] contents, final EmbeddedDocumentExtractor embeddedExtractor, final ContentHandler handler) throws IOException, SAXException, TikaException {
        final Metadata metadata = new Metadata();
        if (name != null) {
            metadata.set("resourceName", name);
        }
        if (type != null) {
            metadata.set("Content-Type", type);
        }
        if (embeddedExtractor.shouldParseEmbedded(metadata)) {
            embeddedExtractor.parseEmbedded(TikaInputStream.get(contents), new EmbeddedContentHandler(handler), metadata, false);
        }
    }
    
    static {
        SUPPORTED_TYPES = Collections.unmodifiableSet((Set<? extends MediaType>)new HashSet<MediaType>(Arrays.asList(MediaType.application("vnd.ms-tnef"), MediaType.application("ms-tnef"), MediaType.application("x-tnef"))));
    }
}
