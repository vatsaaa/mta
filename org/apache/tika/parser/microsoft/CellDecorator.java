// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.parser.microsoft;

import org.xml.sax.SAXException;
import org.apache.tika.sax.XHTMLContentHandler;

public class CellDecorator implements Cell
{
    private final Cell cell;
    
    public CellDecorator(final Cell cell) {
        this.cell = cell;
    }
    
    public void render(final XHTMLContentHandler handler) throws SAXException {
        this.cell.render(handler);
    }
}
