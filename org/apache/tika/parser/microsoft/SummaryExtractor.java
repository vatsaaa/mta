// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.parser.microsoft;

import java.util.Iterator;
import org.apache.tika.metadata.Property;
import java.util.Date;
import org.apache.poi.hpsf.CustomProperties;
import org.apache.tika.metadata.MSOffice;
import org.apache.tika.metadata.PagedText;
import org.apache.tika.metadata.Office;
import org.apache.tika.metadata.OfficeOpenXMLCore;
import org.apache.tika.metadata.OfficeOpenXMLExtended;
import org.apache.tika.metadata.TikaCoreProperties;
import org.apache.poi.hpsf.MarkUnsupportedException;
import org.apache.poi.hpsf.UnexpectedPropertySetTypeException;
import org.apache.poi.hpsf.NoPropertySetStreamException;
import java.io.FileNotFoundException;
import org.apache.poi.hpsf.DocumentSummaryInformation;
import org.apache.poi.hpsf.SummaryInformation;
import java.io.InputStream;
import org.apache.poi.hpsf.PropertySet;
import org.apache.poi.poifs.filesystem.DocumentInputStream;
import org.apache.poi.poifs.filesystem.DocumentEntry;
import org.apache.poi.poifs.filesystem.DirectoryNode;
import org.apache.tika.exception.TikaException;
import java.io.IOException;
import org.apache.poi.poifs.filesystem.NPOIFSFileSystem;
import org.apache.tika.metadata.Metadata;

class SummaryExtractor
{
    private static final String SUMMARY_INFORMATION = "\u0005SummaryInformation";
    private static final String DOCUMENT_SUMMARY_INFORMATION = "\u0005DocumentSummaryInformation";
    private final Metadata metadata;
    
    public SummaryExtractor(final Metadata metadata) {
        this.metadata = metadata;
    }
    
    public void parseSummaries(final NPOIFSFileSystem filesystem) throws IOException, TikaException {
        this.parseSummaries(filesystem.getRoot());
    }
    
    public void parseSummaries(final DirectoryNode root) throws IOException, TikaException {
        this.parseSummaryEntryIfExists(root, "\u0005SummaryInformation");
        this.parseSummaryEntryIfExists(root, "\u0005DocumentSummaryInformation");
    }
    
    private void parseSummaryEntryIfExists(final DirectoryNode root, final String entryName) throws IOException, TikaException {
        try {
            final DocumentEntry entry = (DocumentEntry)root.getEntry(entryName);
            final PropertySet properties = new PropertySet(new DocumentInputStream(entry));
            if (properties.isSummaryInformation()) {
                this.parse(new SummaryInformation(properties));
            }
            if (properties.isDocumentSummaryInformation()) {
                this.parse(new DocumentSummaryInformation(properties));
            }
        }
        catch (FileNotFoundException e3) {}
        catch (NoPropertySetStreamException e4) {}
        catch (UnexpectedPropertySetTypeException e) {
            throw new TikaException("Unexpected HPSF document", e);
        }
        catch (MarkUnsupportedException e2) {
            throw new TikaException("Invalid DocumentInputStream", e2);
        }
    }
    
    private void parse(final SummaryInformation summary) {
        this.set(TikaCoreProperties.TITLE, summary.getTitle());
        this.set(TikaCoreProperties.CREATOR, summary.getAuthor());
        this.set(TikaCoreProperties.KEYWORDS, summary.getKeywords());
        this.set(TikaCoreProperties.TRANSITION_SUBJECT_TO_OO_SUBJECT, summary.getSubject());
        this.set(TikaCoreProperties.MODIFIER, summary.getLastAuthor());
        this.set(TikaCoreProperties.COMMENTS, summary.getComments());
        this.set(OfficeOpenXMLExtended.TEMPLATE, summary.getTemplate());
        this.set(OfficeOpenXMLExtended.APPLICATION, summary.getApplicationName());
        this.set(OfficeOpenXMLCore.REVISION, summary.getRevNumber());
        this.set(TikaCoreProperties.CREATED, summary.getCreateDateTime());
        this.set(TikaCoreProperties.MODIFIED, summary.getLastSaveDateTime());
        this.set(TikaCoreProperties.PRINT_DATE, summary.getLastPrinted());
        this.set("Edit-Time", summary.getEditTime());
        this.set(OfficeOpenXMLExtended.DOC_SECURITY, summary.getSecurity());
        this.set(Office.WORD_COUNT, summary.getWordCount());
        this.set(Office.CHARACTER_COUNT, summary.getCharCount());
        this.set(Office.PAGE_COUNT, summary.getPageCount());
        if (summary.getPageCount() > 0) {
            this.metadata.set(PagedText.N_PAGES, summary.getPageCount());
        }
        this.set("Template", summary.getTemplate());
        this.set("Application-Name", summary.getApplicationName());
        this.set("Revision-Number", summary.getRevNumber());
        this.set("Security", summary.getSecurity());
        this.set(MSOffice.WORD_COUNT, summary.getWordCount());
        this.set(MSOffice.CHARACTER_COUNT, summary.getCharCount());
        this.set(MSOffice.PAGE_COUNT, summary.getPageCount());
    }
    
    private void parse(final DocumentSummaryInformation summary) {
        this.set(OfficeOpenXMLExtended.COMPANY, summary.getCompany());
        this.set(OfficeOpenXMLExtended.MANAGER, summary.getManager());
        this.set(TikaCoreProperties.LANGUAGE, this.getLanguage(summary));
        this.set(OfficeOpenXMLCore.CATEGORY, summary.getCategory());
        this.set(Office.SLIDE_COUNT, summary.getSlideCount());
        if (summary.getSlideCount() > 0) {
            this.metadata.set(PagedText.N_PAGES, summary.getSlideCount());
        }
        this.set("Company", summary.getCompany());
        this.set("Manager", summary.getManager());
        this.set(MSOffice.SLIDE_COUNT, summary.getSlideCount());
        this.set("Category", summary.getCategory());
        this.parse(summary.getCustomProperties());
    }
    
    private String getLanguage(final DocumentSummaryInformation summary) {
        final CustomProperties customProperties = summary.getCustomProperties();
        if (customProperties != null) {
            final Object value = customProperties.get("Language");
            if (value instanceof String) {
                return (String)value;
            }
        }
        return null;
    }
    
    private void parse(final CustomProperties customProperties) {
        if (customProperties != null) {
            for (final String name : customProperties.nameSet()) {
                final String key = "custom:" + name;
                final Object value = customProperties.get(name);
                if (value instanceof String) {
                    this.set(key, (String)value);
                }
                else if (value instanceof Date) {
                    final Property prop = Property.externalDate(key);
                    this.metadata.set(prop, (Date)value);
                }
                else if (value instanceof Boolean) {
                    final Property prop = Property.externalBoolean(key);
                    this.metadata.set(prop, ((Boolean)value).toString());
                }
                else if (value instanceof Long) {
                    final Property prop = Property.externalInteger(key);
                    this.metadata.set(prop, ((Long)value).intValue());
                }
                else if (value instanceof Double) {
                    final Property prop = Property.externalReal(key);
                    this.metadata.set(prop, (double)value);
                }
                else {
                    if (!(value instanceof Integer)) {
                        continue;
                    }
                    final Property prop = Property.externalInteger(key);
                    this.metadata.set(prop, (int)value);
                }
            }
        }
    }
    
    private void set(final String name, final String value) {
        if (value != null) {
            this.metadata.set(name, value);
        }
    }
    
    private void set(final Property property, final String value) {
        if (value != null) {
            this.metadata.set(property, value);
        }
    }
    
    private void set(final Property property, final Date value) {
        if (value != null) {
            this.metadata.set(property, value);
        }
    }
    
    private void set(final Property property, final int value) {
        if (value > 0) {
            this.metadata.set(property, value);
        }
    }
    
    private void set(final String name, final long value) {
        if (value > 0L) {
            this.metadata.set(name, Long.toString(value));
        }
    }
}
