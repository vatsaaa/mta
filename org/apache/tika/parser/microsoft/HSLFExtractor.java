// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.parser.microsoft;

import org.apache.poi.hslf.usermodel.ObjectData;
import org.apache.poi.hslf.model.Shape;
import org.apache.poi.hslf.model.OLEShape;
import org.apache.poi.hslf.usermodel.PictureData;
import org.apache.tika.io.TikaInputStream;
import org.apache.poi.hslf.model.TextRun;
import org.apache.poi.hslf.model.Notes;
import org.apache.poi.hslf.model.Comment;
import org.apache.poi.hslf.model.HeadersFooters;
import org.apache.poi.hslf.model.Slide;
import java.util.HashSet;
import org.apache.poi.hslf.usermodel.SlideShow;
import org.apache.poi.hslf.HSLFSlideShow;
import org.apache.poi.poifs.filesystem.DirectoryNode;
import org.apache.tika.exception.TikaException;
import org.xml.sax.SAXException;
import java.io.IOException;
import org.apache.tika.sax.XHTMLContentHandler;
import org.apache.poi.poifs.filesystem.NPOIFSFileSystem;
import org.apache.tika.parser.ParseContext;

public class HSLFExtractor extends AbstractPOIFSExtractor
{
    public HSLFExtractor(final ParseContext context) {
        super(context);
    }
    
    protected void parse(final NPOIFSFileSystem filesystem, final XHTMLContentHandler xhtml) throws IOException, SAXException, TikaException {
        this.parse(filesystem.getRoot(), xhtml);
    }
    
    protected void parse(final DirectoryNode root, final XHTMLContentHandler xhtml) throws IOException, SAXException, TikaException {
        final HSLFSlideShow ss = new HSLFSlideShow(root);
        final SlideShow _show = new SlideShow(ss);
        final Slide[] _slides = _show.getSlides();
        xhtml.startElement("div", "class", "slideShow");
        for (final Slide slide : _slides) {
            xhtml.startElement("div", "class", "slide");
            final HeadersFooters hf = slide.getHeadersFooters();
            if (hf != null && hf.isHeaderVisible() && hf.getHeaderText() != null) {
                xhtml.startElement("p", "class", "slide-header");
                xhtml.characters(hf.getHeaderText());
                xhtml.endElement("p");
            }
            xhtml.startElement("p", "class", "slide-content");
            this.textRunsToText(xhtml, slide.getTextRuns());
            xhtml.endElement("p");
            if (hf != null && hf.isFooterVisible() && hf.getFooterText() != null) {
                xhtml.startElement("p", "class", "slide-footer");
                xhtml.characters(hf.getFooterText());
                xhtml.endElement("p");
            }
            for (final Comment comment : slide.getComments()) {
                xhtml.startElement("p", "class", "slide-comment");
                if (comment.getAuthor() != null) {
                    xhtml.startElement("b");
                    xhtml.characters(comment.getAuthor());
                    xhtml.endElement("b");
                    if (comment.getText() != null) {
                        xhtml.characters(" - ");
                    }
                }
                if (comment.getText() != null) {
                    xhtml.characters(comment.getText());
                }
                xhtml.endElement("p");
            }
            this.handleSlideEmbeddedResources(slide, xhtml);
            xhtml.endElement("div");
        }
        xhtml.endElement("div");
        xhtml.startElement("div", "class", "slideNotes");
        final HashSet<Integer> seenNotes = new HashSet<Integer>();
        final HeadersFooters hf2 = _show.getNotesHeadersFooters();
        for (final Slide slide2 : _slides) {
            final Notes notes = slide2.getNotesSheet();
            if (notes != null) {
                final Integer id = notes._getSheetNumber();
                if (!seenNotes.contains(id)) {
                    seenNotes.add(id);
                    if (hf2 != null && hf2.isHeaderVisible() && hf2.getHeaderText() != null) {
                        xhtml.startElement("p", "class", "slide-note-header");
                        xhtml.characters(hf2.getHeaderText());
                        xhtml.endElement("p");
                    }
                    this.textRunsToText(xhtml, notes.getTextRuns());
                    if (hf2 != null && hf2.isFooterVisible() && hf2.getFooterText() != null) {
                        xhtml.startElement("p", "class", "slide-note-footer");
                        xhtml.characters(hf2.getFooterText());
                        xhtml.endElement("p");
                    }
                }
            }
        }
        this.handleSlideEmbeddedPictures(_show, xhtml);
        xhtml.endElement("div");
    }
    
    private void textRunsToText(final XHTMLContentHandler xhtml, final TextRun[] runs) throws SAXException {
        if (runs == null) {
            return;
        }
        for (final TextRun run : runs) {
            if (run != null) {
                xhtml.characters(run.getText());
                xhtml.startElement("br");
                xhtml.endElement("br");
            }
        }
    }
    
    private void handleSlideEmbeddedPictures(final SlideShow slideshow, final XHTMLContentHandler xhtml) throws TikaException, SAXException, IOException {
        for (final PictureData pic : slideshow.getPictureData()) {
            String mediaType = null;
            switch (pic.getType()) {
                case 2: {
                    mediaType = "application/x-emf";
                    break;
                }
                case 5: {
                    mediaType = "image/jpeg";
                    break;
                }
                case 6: {
                    mediaType = "image/png";
                    break;
                }
                case 3: {
                    mediaType = "application/x-msmetafile";
                    break;
                }
                case 7: {
                    mediaType = "image/bmp";
                    break;
                }
            }
            this.handleEmbeddedResource(TikaInputStream.get(pic.getData()), null, mediaType, xhtml, false);
        }
    }
    
    private void handleSlideEmbeddedResources(final Slide slide, final XHTMLContentHandler xhtml) throws TikaException, SAXException, IOException {
        Shape[] shapes;
        try {
            shapes = slide.getShapes();
        }
        catch (NullPointerException e) {
            return;
        }
        for (final Shape shape : shapes) {
            if (shape instanceof OLEShape) {
                final OLEShape oleShape = (OLEShape)shape;
                try {
                    final ObjectData data = oleShape.getObjectData();
                    if (data != null) {
                        final TikaInputStream stream = TikaInputStream.get(data.getData());
                        try {
                            String mediaType = null;
                            if ("Excel.Chart.8".equals(oleShape.getProgID())) {
                                mediaType = "application/vnd.ms-excel";
                            }
                            this.handleEmbeddedResource(stream, Integer.toString(oleShape.getObjectID()), mediaType, xhtml, false);
                        }
                        finally {
                            stream.close();
                        }
                    }
                }
                catch (NullPointerException ex) {}
            }
        }
    }
}
