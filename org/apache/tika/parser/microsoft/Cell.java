// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.parser.microsoft;

import org.xml.sax.SAXException;
import org.apache.tika.sax.XHTMLContentHandler;

public interface Cell
{
    void render(final XHTMLContentHandler p0) throws SAXException;
}
