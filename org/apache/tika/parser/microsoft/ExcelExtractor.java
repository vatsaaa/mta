// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.parser.microsoft;

import org.apache.poi.ddf.EscherBlipRecord;
import org.apache.tika.io.TikaInputStream;
import org.apache.poi.hssf.usermodel.HSSFPictureData;
import org.apache.poi.ddf.EscherBSERecord;
import org.apache.poi.ddf.EscherRecord;
import java.util.Map;
import java.util.Comparator;
import java.util.TreeMap;
import org.apache.poi.hssf.record.common.UnicodeString;
import org.apache.poi.hssf.record.chart.SeriesTextRecord;
import org.apache.poi.hssf.record.TextObjectRecord;
import org.apache.poi.hssf.record.HyperlinkRecord;
import org.apache.poi.hssf.record.RKRecord;
import org.apache.poi.hssf.record.NumberRecord;
import org.apache.poi.hssf.record.LabelSSTRecord;
import org.apache.poi.hssf.record.LabelRecord;
import org.apache.poi.hssf.record.StringRecord;
import org.apache.poi.hssf.record.CellValueRecordInterface;
import org.apache.poi.hssf.record.BoundSheetRecord;
import org.apache.poi.hssf.record.BOFRecord;
import org.apache.poi.hssf.record.Record;
import org.apache.poi.poifs.filesystem.DocumentInputStream;
import org.apache.tika.exception.EncryptedDocumentException;
import java.io.InputStream;
import org.apache.poi.hssf.eventusermodel.HSSFEventFactory;
import org.apache.poi.hssf.eventusermodel.HSSFRequest;
import java.util.ArrayList;
import org.apache.poi.hssf.record.DrawingGroupRecord;
import java.text.NumberFormat;
import java.awt.Point;
import java.util.SortedMap;
import java.util.List;
import org.apache.poi.hssf.eventusermodel.FormatTrackingHSSFListener;
import org.apache.poi.hssf.record.FormulaRecord;
import org.apache.poi.hssf.record.SSTRecord;
import org.apache.poi.hssf.eventusermodel.HSSFListener;
import java.util.Iterator;
import org.apache.poi.poifs.filesystem.DirectoryEntry;
import org.apache.poi.poifs.filesystem.Entry;
import org.apache.poi.poifs.filesystem.DirectoryNode;
import org.apache.tika.exception.TikaException;
import org.xml.sax.SAXException;
import java.io.IOException;
import java.util.Locale;
import org.apache.tika.sax.XHTMLContentHandler;
import org.apache.poi.poifs.filesystem.NPOIFSFileSystem;
import org.apache.tika.parser.ParseContext;

public class ExcelExtractor extends AbstractPOIFSExtractor
{
    private boolean listenForAllRecords;
    
    public ExcelExtractor(final ParseContext context) {
        super(context);
        this.listenForAllRecords = false;
    }
    
    public boolean isListenForAllRecords() {
        return this.listenForAllRecords;
    }
    
    public void setListenForAllRecords(final boolean listenForAllRecords) {
        this.listenForAllRecords = listenForAllRecords;
    }
    
    protected void parse(final NPOIFSFileSystem filesystem, final XHTMLContentHandler xhtml, final Locale locale) throws IOException, SAXException, TikaException {
        this.parse(filesystem.getRoot(), xhtml, locale);
    }
    
    protected void parse(final DirectoryNode root, final XHTMLContentHandler xhtml, final Locale locale) throws IOException, SAXException, TikaException {
        final TikaHSSFListener listener = new TikaHSSFListener(xhtml, locale, (AbstractPOIFSExtractor)this);
        listener.processFile(root, this.isListenForAllRecords());
        listener.throwStoredException();
        for (final Entry entry : root) {
            if (entry.getName().startsWith("MBD") && entry instanceof DirectoryEntry) {
                try {
                    this.handleEmbeddedOfficeDoc((DirectoryEntry)entry, xhtml);
                }
                catch (TikaException ex) {}
            }
        }
    }
    
    private static class TikaHSSFListener implements HSSFListener
    {
        private final XHTMLContentHandler handler;
        private final AbstractPOIFSExtractor extractor;
        private Exception exception;
        private SSTRecord sstRecord;
        private FormulaRecord stringFormulaRecord;
        private short previousSid;
        private FormatTrackingHSSFListener formatListener;
        private List<String> sheetNames;
        private short currentSheetIndex;
        private SortedMap<Point, Cell> currentSheet;
        private List<Cell> extraTextCells;
        private final NumberFormat format;
        private List<DrawingGroupRecord> drawingGroups;
        
        private TikaHSSFListener(final XHTMLContentHandler handler, final Locale locale, final AbstractPOIFSExtractor extractor) {
            this.exception = null;
            this.sheetNames = new ArrayList<String>();
            this.currentSheet = null;
            this.extraTextCells = new ArrayList<Cell>();
            this.drawingGroups = new ArrayList<DrawingGroupRecord>();
            this.handler = handler;
            this.extractor = extractor;
            this.format = NumberFormat.getInstance(locale);
            this.formatListener = new FormatTrackingHSSFListener(this, locale);
        }
        
        public void processFile(final NPOIFSFileSystem filesystem, final boolean listenForAllRecords) throws IOException, SAXException, TikaException {
            this.processFile(filesystem.getRoot(), listenForAllRecords);
        }
        
        public void processFile(final DirectoryNode root, final boolean listenForAllRecords) throws IOException, SAXException, TikaException {
            final HSSFRequest hssfRequest = new HSSFRequest();
            if (listenForAllRecords) {
                hssfRequest.addListenerForAllRecords(this.formatListener);
            }
            else {
                hssfRequest.addListener(this.formatListener, (short)2057);
                hssfRequest.addListener(this.formatListener, (short)10);
                hssfRequest.addListener(this.formatListener, (short)34);
                hssfRequest.addListener(this.formatListener, (short)140);
                hssfRequest.addListener(this.formatListener, (short)133);
                hssfRequest.addListener(this.formatListener, (short)252);
                hssfRequest.addListener(this.formatListener, (short)6);
                hssfRequest.addListener(this.formatListener, (short)516);
                hssfRequest.addListener(this.formatListener, (short)253);
                hssfRequest.addListener(this.formatListener, (short)515);
                hssfRequest.addListener(this.formatListener, (short)638);
                hssfRequest.addListener(this.formatListener, (short)519);
                hssfRequest.addListener(this.formatListener, (short)440);
                hssfRequest.addListener(this.formatListener, (short)438);
                hssfRequest.addListener(this.formatListener, (short)4109);
                hssfRequest.addListener(this.formatListener, (short)1054);
                hssfRequest.addListener(this.formatListener, (short)224);
                hssfRequest.addListener(this.formatListener, (short)235);
            }
            final DocumentInputStream documentInputStream = root.createDocumentInputStream("Workbook");
            final HSSFEventFactory eventFactory = new HSSFEventFactory();
            try {
                eventFactory.processEvents(hssfRequest, documentInputStream);
            }
            catch (org.apache.poi.EncryptedDocumentException e) {
                throw new EncryptedDocumentException(e);
            }
            this.processExtraText();
            for (final DrawingGroupRecord dgr : this.drawingGroups) {
                dgr.decode();
                this.findPictures(dgr.getEscherRecords());
            }
        }
        
        public void processRecord(final Record record) {
            if (this.exception == null) {
                try {
                    this.internalProcessRecord(record);
                }
                catch (TikaException te) {
                    this.exception = te;
                }
                catch (IOException ie) {
                    this.exception = ie;
                }
                catch (SAXException se) {
                    this.exception = se;
                }
            }
        }
        
        public void throwStoredException() throws TikaException, SAXException, IOException {
            if (this.exception == null) {
                return;
            }
            if (this.exception instanceof IOException) {
                throw (IOException)this.exception;
            }
            if (this.exception instanceof SAXException) {
                throw (SAXException)this.exception;
            }
            if (this.exception instanceof TikaException) {
                throw (TikaException)this.exception;
            }
            throw new TikaException(this.exception.getMessage());
        }
        
        private void internalProcessRecord(final Record record) throws SAXException, TikaException, IOException {
            switch (record.getSid()) {
                case 2057: {
                    final BOFRecord bof = (BOFRecord)record;
                    if (bof.getType() == 5) {
                        this.currentSheetIndex = -1;
                        break;
                    }
                    if (bof.getType() == 32) {
                        if (this.previousSid == 10) {
                            this.newSheet();
                            break;
                        }
                        if (this.currentSheet != null) {
                            this.processSheet();
                            --this.currentSheetIndex;
                            this.newSheet();
                            break;
                        }
                        break;
                    }
                    else {
                        if (bof.getType() == 16) {
                            this.newSheet();
                            break;
                        }
                        break;
                    }
                    break;
                }
                case 10: {
                    if (this.currentSheet != null) {
                        this.processSheet();
                    }
                    this.currentSheet = null;
                    break;
                }
                case 133: {
                    final BoundSheetRecord boundSheetRecord = (BoundSheetRecord)record;
                    this.sheetNames.add(boundSheetRecord.getSheetname());
                    break;
                }
                case 252: {
                    this.sstRecord = (SSTRecord)record;
                    break;
                }
                case 6: {
                    final FormulaRecord formula = (FormulaRecord)record;
                    if (formula.hasCachedResultString()) {
                        this.stringFormulaRecord = formula;
                        break;
                    }
                    this.addTextCell(record, this.formatListener.formatNumberDateCell(formula));
                    break;
                }
                case 519: {
                    if (this.previousSid == 6) {
                        final StringRecord sr = (StringRecord)record;
                        this.addTextCell(this.stringFormulaRecord, sr.getString());
                        break;
                    }
                    break;
                }
                case 516: {
                    final LabelRecord label = (LabelRecord)record;
                    this.addTextCell(record, label.getValue());
                    break;
                }
                case 253: {
                    final LabelSSTRecord sst = (LabelSSTRecord)record;
                    final UnicodeString unicode = this.sstRecord.getString(sst.getSSTIndex());
                    this.addTextCell(record, unicode.getString());
                    break;
                }
                case 515: {
                    final NumberRecord number = (NumberRecord)record;
                    this.addTextCell(record, this.formatListener.formatNumberDateCell(number));
                    break;
                }
                case 638: {
                    final RKRecord rk = (RKRecord)record;
                    this.addCell(record, new NumberCell(rk.getRKNumber(), this.format));
                    break;
                }
                case 440: {
                    if (this.currentSheet != null) {
                        final HyperlinkRecord link = (HyperlinkRecord)record;
                        final Point point = new Point(link.getFirstColumn(), link.getFirstRow());
                        final Cell cell = this.currentSheet.get(point);
                        if (cell != null) {
                            final String address = link.getAddress();
                            if (address != null) {
                                this.addCell(record, new LinkedCell(cell, address));
                            }
                            else {
                                this.addCell(record, cell);
                            }
                        }
                        break;
                    }
                    break;
                }
                case 438: {
                    final TextObjectRecord tor = (TextObjectRecord)record;
                    this.addTextCell(record, tor.getStr().getString());
                    break;
                }
                case 4109: {
                    final SeriesTextRecord str = (SeriesTextRecord)record;
                    this.addTextCell(record, str.getText());
                    break;
                }
                case 235: {
                    this.drawingGroups.add((DrawingGroupRecord)record);
                    break;
                }
            }
            this.previousSid = record.getSid();
            if (this.stringFormulaRecord != record) {
                this.stringFormulaRecord = null;
            }
        }
        
        private void processExtraText() throws SAXException {
            if (this.extraTextCells.size() > 0) {
                for (final Cell cell : this.extraTextCells) {
                    this.handler.startElement("div", "class", "outside");
                    cell.render(this.handler);
                    this.handler.endElement("div");
                }
                this.extraTextCells.clear();
            }
        }
        
        private void addCell(final Record record, final Cell cell) throws SAXException {
            if (cell != null) {
                if (this.currentSheet != null && record instanceof CellValueRecordInterface) {
                    final CellValueRecordInterface value = (CellValueRecordInterface)record;
                    final Point point = new Point(value.getColumn(), value.getRow());
                    this.currentSheet.put(point, cell);
                }
                else {
                    this.extraTextCells.add(cell);
                }
            }
        }
        
        private void addTextCell(final Record record, String text) throws SAXException {
            if (text != null) {
                text = text.trim();
                if (text.length() > 0) {
                    this.addCell(record, new TextCell(text));
                }
            }
        }
        
        private void newSheet() {
            ++this.currentSheetIndex;
            this.currentSheet = new TreeMap<Point, Cell>(new PointComparator());
        }
        
        private void processSheet() throws SAXException {
            this.handler.startElement("div", "class", "page");
            if (this.currentSheetIndex < this.sheetNames.size()) {
                this.handler.element("h1", this.sheetNames.get(this.currentSheetIndex));
            }
            this.handler.startElement("table");
            this.handler.startElement("tbody");
            int currentRow = 0;
            int currentColumn = 0;
            this.handler.startElement("tr");
            this.handler.startElement("td");
            for (final Map.Entry<Point, Cell> entry : this.currentSheet.entrySet()) {
                while (currentRow < entry.getKey().y) {
                    this.handler.endElement("td");
                    this.handler.endElement("tr");
                    this.handler.startElement("tr");
                    this.handler.startElement("td");
                    ++currentRow;
                    currentColumn = 0;
                }
                while (currentColumn < entry.getKey().x) {
                    this.handler.endElement("td");
                    this.handler.startElement("td");
                    ++currentColumn;
                }
                entry.getValue().render(this.handler);
            }
            this.handler.endElement("td");
            this.handler.endElement("tr");
            this.handler.endElement("tbody");
            this.handler.endElement("table");
            this.processExtraText();
            this.handler.endElement("div");
        }
        
        private void findPictures(final List<EscherRecord> records) throws IOException, SAXException, TikaException {
            for (final EscherRecord escherRecord : records) {
                if (escherRecord instanceof EscherBSERecord) {
                    final EscherBlipRecord blip = ((EscherBSERecord)escherRecord).getBlipRecord();
                    if (blip != null) {
                        final HSSFPictureData picture = new HSSFPictureData(blip);
                        final String mimeType = picture.getMimeType();
                        final TikaInputStream stream = TikaInputStream.get(picture.getData());
                        this.extractor.handleEmbeddedResource(stream, null, mimeType, this.handler, true);
                    }
                }
                this.findPictures(escherRecord.getChildRecords());
            }
        }
    }
    
    private static class PointComparator implements Comparator<Point>
    {
        public int compare(final Point a, final Point b) {
            int diff = a.y - b.y;
            if (diff == 0) {
                diff = a.x - b.x;
            }
            return diff;
        }
    }
}
