// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.parser.microsoft;

import java.util.Iterator;
import org.apache.poi.poifs.filesystem.Entry;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import java.util.Collections;
import java.util.Collection;
import java.util.HashSet;
import java.util.Arrays;
import java.security.GeneralSecurityException;
import org.apache.tika.sax.EmbeddedContentHandler;
import org.apache.tika.sax.BodyContentHandler;
import org.apache.tika.parser.microsoft.ooxml.OOXMLParser;
import org.apache.tika.exception.EncryptedDocumentException;
import org.apache.tika.parser.PasswordProvider;
import org.apache.poi.poifs.crypt.Decryptor;
import org.apache.poi.poifs.crypt.EncryptionInfo;
import org.apache.poi.hdgf.extractor.VisioTextExtractor;
import java.util.Locale;
import org.apache.poi.hpbf.extractor.PublisherTextExtractor;
import org.apache.poi.poifs.filesystem.DirectoryEntry;
import org.apache.tika.exception.TikaException;
import org.xml.sax.SAXException;
import java.io.IOException;
import org.apache.poi.poifs.filesystem.DirectoryNode;
import org.apache.poi.poifs.filesystem.NPOIFSFileSystem;
import org.apache.tika.io.CloseShieldInputStream;
import org.apache.tika.io.TikaInputStream;
import org.apache.tika.sax.XHTMLContentHandler;
import org.apache.tika.metadata.Metadata;
import org.xml.sax.ContentHandler;
import java.io.InputStream;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.mime.MediaType;
import java.util.Set;
import org.apache.tika.parser.AbstractParser;

public class OfficeParser extends AbstractParser
{
    private static final long serialVersionUID = 7393462244028653479L;
    private static final Set<MediaType> SUPPORTED_TYPES;
    
    public Set<MediaType> getSupportedTypes(final ParseContext context) {
        return OfficeParser.SUPPORTED_TYPES;
    }
    
    public void parse(final InputStream stream, final ContentHandler handler, final Metadata metadata, final ParseContext context) throws IOException, SAXException, TikaException {
        final XHTMLContentHandler xhtml = new XHTMLContentHandler(handler, metadata);
        xhtml.startDocument();
        final TikaInputStream tstream = TikaInputStream.cast(stream);
        DirectoryNode root;
        if (tstream == null) {
            root = new NPOIFSFileSystem(new CloseShieldInputStream(stream)).getRoot();
        }
        else {
            final Object container = tstream.getOpenContainer();
            if (container instanceof NPOIFSFileSystem) {
                root = ((NPOIFSFileSystem)container).getRoot();
            }
            else if (container instanceof DirectoryNode) {
                root = (DirectoryNode)container;
            }
            else if (tstream.hasFile()) {
                root = new NPOIFSFileSystem(tstream.getFileChannel()).getRoot();
            }
            else {
                root = new NPOIFSFileSystem(new CloseShieldInputStream(tstream)).getRoot();
            }
        }
        this.parse(root, context, metadata, xhtml);
        xhtml.endDocument();
    }
    
    protected void parse(final DirectoryNode root, final ParseContext context, final Metadata metadata, final XHTMLContentHandler xhtml) throws IOException, SAXException, TikaException {
        new SummaryExtractor(metadata).parseSummaries(root);
        final POIFSDocumentType type = POIFSDocumentType.detectType(root);
        if (type != POIFSDocumentType.UNKNOWN) {
            this.setType(metadata, type.getType());
        }
        switch (type) {
            case PUBLISHER: {
                final PublisherTextExtractor publisherTextExtractor = new PublisherTextExtractor(root);
                xhtml.element("p", publisherTextExtractor.getText());
                break;
            }
            case WORDDOCUMENT: {
                new WordExtractor(context).parse(root, xhtml);
                break;
            }
            case POWERPOINT: {
                new HSLFExtractor(context).parse(root, xhtml);
                break;
            }
            case WORKBOOK:
            case XLR: {
                final Locale locale = context.get(Locale.class, Locale.getDefault());
                new ExcelExtractor(context).parse(root, xhtml, locale);
            }
            case VISIO: {
                final VisioTextExtractor visioTextExtractor = new VisioTextExtractor(root);
                for (final String text : visioTextExtractor.getAllText()) {
                    xhtml.element("p", text);
                }
                break;
            }
            case OUTLOOK: {
                final OutlookExtractor extractor = new OutlookExtractor(root, context);
                extractor.parse(xhtml, metadata);
                break;
            }
            case ENCRYPTED: {
                final EncryptionInfo info = new EncryptionInfo(root);
                final Decryptor d = Decryptor.getInstance(info);
                try {
                    String password = "VelvetSweatshop";
                    final PasswordProvider passwordProvider = context.get(PasswordProvider.class);
                    if (passwordProvider != null) {
                        password = passwordProvider.getPassword(metadata);
                    }
                    if (!d.verifyPassword(password)) {
                        throw new EncryptedDocumentException();
                    }
                    final OOXMLParser parser = new OOXMLParser();
                    parser.parse(d.getDataStream(root), new EmbeddedContentHandler(new BodyContentHandler(xhtml)), metadata, context);
                }
                catch (GeneralSecurityException ex) {
                    throw new EncryptedDocumentException(ex);
                }
                break;
            }
        }
    }
    
    private void setType(final Metadata metadata, final MediaType type) {
        metadata.set("Content-Type", type.toString());
    }
    
    static {
        SUPPORTED_TYPES = Collections.unmodifiableSet((Set<? extends MediaType>)new HashSet<MediaType>(Arrays.asList(POIFSDocumentType.WORKBOOK.type, POIFSDocumentType.OLE10_NATIVE.type, POIFSDocumentType.WORDDOCUMENT.type, POIFSDocumentType.UNKNOWN.type, POIFSDocumentType.ENCRYPTED.type, POIFSDocumentType.POWERPOINT.type, POIFSDocumentType.PUBLISHER.type, POIFSDocumentType.PROJECT.type, POIFSDocumentType.VISIO.type, POIFSDocumentType.XLR.type, POIFSDocumentType.OUTLOOK.type)));
    }
    
    public enum POIFSDocumentType
    {
        WORKBOOK("xls", MediaType.application("vnd.ms-excel")), 
        OLE10_NATIVE("ole", POIFSContainerDetector.OLE10_NATIVE), 
        COMP_OBJ("ole", POIFSContainerDetector.COMP_OBJ), 
        WORDDOCUMENT("doc", MediaType.application("msword")), 
        UNKNOWN("unknown", MediaType.application("x-tika-msoffice")), 
        ENCRYPTED("ole", MediaType.application("x-tika-ooxml-protected")), 
        POWERPOINT("ppt", MediaType.application("vnd.ms-powerpoint")), 
        PUBLISHER("pub", MediaType.application("x-mspublisher")), 
        PROJECT("mpp", MediaType.application("vnd.ms-project")), 
        VISIO("vsd", MediaType.application("vnd.visio")), 
        WORKS("wps", MediaType.application("vnd.ms-works")), 
        XLR("xlr", MediaType.application("x-tika-msworks-spreadsheet")), 
        OUTLOOK("msg", MediaType.application("vnd.ms-outlook"));
        
        private final String extension;
        private final MediaType type;
        
        private POIFSDocumentType(final String extension, final MediaType type) {
            this.extension = extension;
            this.type = type;
        }
        
        public String getExtension() {
            return this.extension;
        }
        
        public MediaType getType() {
            return this.type;
        }
        
        public static POIFSDocumentType detectType(final POIFSFileSystem fs) {
            return detectType(fs.getRoot());
        }
        
        public static POIFSDocumentType detectType(final NPOIFSFileSystem fs) {
            return detectType(fs.getRoot());
        }
        
        public static POIFSDocumentType detectType(final DirectoryEntry node) {
            final Set<String> names = new HashSet<String>();
            for (final Entry entry : node) {
                names.add(entry.getName());
            }
            final MediaType type = POIFSContainerDetector.detect(names, node);
            for (final POIFSDocumentType poifsType : values()) {
                if (type.equals(poifsType.type)) {
                    return poifsType;
                }
            }
            return POIFSDocumentType.UNKNOWN;
        }
    }
}
