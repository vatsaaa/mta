// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.parser.microsoft;

import java.util.HashSet;
import java.nio.channels.FileChannel;
import java.util.Collections;
import org.apache.poi.poifs.filesystem.Entry;
import org.apache.tika.io.IOUtils;
import org.apache.poi.poifs.filesystem.DocumentEntry;
import org.apache.poi.poifs.filesystem.DocumentInputStream;
import org.apache.poi.poifs.filesystem.DocumentNode;
import java.util.Iterator;
import java.io.IOException;
import java.util.Set;
import org.apache.poi.poifs.filesystem.DirectoryEntry;
import org.apache.poi.poifs.filesystem.DirectoryNode;
import org.apache.poi.poifs.filesystem.NPOIFSFileSystem;
import org.apache.tika.io.TikaInputStream;
import org.apache.tika.metadata.Metadata;
import java.io.InputStream;
import java.util.regex.Pattern;
import org.apache.tika.mime.MediaType;
import org.apache.tika.detect.Detector;

public class POIFSContainerDetector implements Detector
{
    private static final long serialVersionUID = -3028021741663605293L;
    private static final byte[] STAR_IMPRESS;
    private static final byte[] STAR_DRAW;
    private static final byte[] WORKS_QUILL96;
    public static final MediaType OLE;
    public static final MediaType OOXML_PROTECTED;
    public static final MediaType GENERAL_EMBEDDED;
    public static final MediaType OLE10_NATIVE;
    public static final MediaType COMP_OBJ;
    public static final MediaType XLS;
    public static final MediaType DOC;
    public static final MediaType PPT;
    public static final MediaType PUB;
    public static final MediaType VSD;
    public static final MediaType WPS;
    public static final MediaType XLR;
    public static final MediaType MSG;
    public static final MediaType MPP;
    public static final MediaType SDC;
    public static final MediaType SDA;
    public static final MediaType SDD;
    public static final MediaType SDW;
    private static final Pattern mppDataMatch;
    
    public MediaType detect(final InputStream input, final Metadata metadata) throws IOException {
        if (input == null) {
            return MediaType.OCTET_STREAM;
        }
        final TikaInputStream tis = TikaInputStream.cast(input);
        Set<String> names = null;
        if (tis != null) {
            final Object container = tis.getOpenContainer();
            if (container instanceof NPOIFSFileSystem) {
                names = getTopLevelNames(((NPOIFSFileSystem)container).getRoot());
            }
            else if (container instanceof DirectoryNode) {
                names = getTopLevelNames((DirectoryNode)container);
            }
        }
        if (names == null) {
            input.mark(8);
            try {
                if (input.read() != 208 || input.read() != 207 || input.read() != 17 || input.read() != 224 || input.read() != 161 || input.read() != 177 || input.read() != 26 || input.read() != 225) {
                    return MediaType.OCTET_STREAM;
                }
            }
            finally {
                input.reset();
            }
        }
        if (names == null && tis != null) {
            names = getTopLevelNames(tis);
        }
        if (tis != null && tis.getOpenContainer() != null && tis.getOpenContainer() instanceof NPOIFSFileSystem) {
            return detect(names, ((NPOIFSFileSystem)tis.getOpenContainer()).getRoot());
        }
        return detect(names, null);
    }
    
    @Deprecated
    protected static MediaType detect(final Set<String> names) {
        return detect(names, null);
    }
    
    protected static MediaType detect(final Set<String> names, final DirectoryEntry root) {
        if (names != null) {
            if (names.contains("StarCalcDocument")) {
                return POIFSContainerDetector.SDC;
            }
            if (names.contains("StarWriterDocument")) {
                return POIFSContainerDetector.SDW;
            }
            if (names.contains("StarDrawDocument3")) {
                if (root == null) {
                    return POIFSContainerDetector.OLE;
                }
                return processCompObjFormatType(root);
            }
            else {
                if (names.contains("WksSSWorkBook")) {
                    return POIFSContainerDetector.XLR;
                }
                if (names.contains("Workbook")) {
                    return POIFSContainerDetector.XLS;
                }
                if (names.contains("EncryptedPackage") && names.contains("EncryptionInfo") && names.contains("\u0006DataSpaces")) {
                    return POIFSContainerDetector.OOXML_PROTECTED;
                }
                if (names.contains("EncryptedPackage")) {
                    return POIFSContainerDetector.OLE;
                }
                if (names.contains("WordDocument")) {
                    return POIFSContainerDetector.DOC;
                }
                if (names.contains("Quill")) {
                    return POIFSContainerDetector.PUB;
                }
                if (names.contains("PowerPoint Document")) {
                    return POIFSContainerDetector.PPT;
                }
                if (names.contains("VisioDocument")) {
                    return POIFSContainerDetector.VSD;
                }
                if (names.contains("\u0001Ole10Native")) {
                    return POIFSContainerDetector.OLE10_NATIVE;
                }
                if (names.contains("MatOST")) {
                    return POIFSContainerDetector.WPS;
                }
                if (names.contains("CONTENTS") && names.contains("SPELLING")) {
                    return POIFSContainerDetector.WPS;
                }
                if (names.contains("CONTENTS") && names.contains("\u0001CompObj")) {
                    if (root == null) {
                        return POIFSContainerDetector.COMP_OBJ;
                    }
                    final MediaType type = processCompObjFormatType(root);
                    if (type == POIFSContainerDetector.WPS) {
                        return POIFSContainerDetector.WPS;
                    }
                    return POIFSContainerDetector.COMP_OBJ;
                }
                else {
                    if (names.contains("CONTENTS")) {
                        return POIFSContainerDetector.OLE;
                    }
                    if (names.contains("\u0001CompObj") && (names.contains("Props") || names.contains("Props9") || names.contains("Props12"))) {
                        for (final String name : names) {
                            if (POIFSContainerDetector.mppDataMatch.matcher(name).matches()) {
                                return POIFSContainerDetector.MPP;
                            }
                        }
                    }
                    else if (names.contains("PerfectOffice_MAIN")) {
                        if (names.contains("SlideShow")) {
                            return MediaType.application("x-corelpresentations");
                        }
                        if (names.contains("PerfectOffice_OBJECTS")) {
                            return MediaType.application("x-quattro-pro");
                        }
                    }
                    else {
                        if (names.contains("NativeContent_MAIN")) {
                            return MediaType.application("x-quattro-pro");
                        }
                        for (final String name : names) {
                            if (name.startsWith("__substg1.0_")) {
                                return POIFSContainerDetector.MSG;
                            }
                        }
                    }
                }
            }
        }
        return POIFSContainerDetector.OLE;
    }
    
    private static MediaType processCompObjFormatType(final DirectoryEntry root) {
        try {
            final Entry e = root.getEntry("\u0001CompObj");
            if (e != null && e.isDocumentEntry()) {
                final DocumentNode dn = (DocumentNode)e;
                final DocumentInputStream stream = new DocumentInputStream(dn);
                final byte[] bytes = IOUtils.toByteArray(stream);
                if (arrayContains(bytes, POIFSContainerDetector.STAR_DRAW)) {
                    return POIFSContainerDetector.SDA;
                }
                if (arrayContains(bytes, POIFSContainerDetector.STAR_IMPRESS)) {
                    return POIFSContainerDetector.SDD;
                }
                if (arrayContains(bytes, POIFSContainerDetector.WORKS_QUILL96)) {
                    return POIFSContainerDetector.WPS;
                }
            }
        }
        catch (Exception ex) {}
        return POIFSContainerDetector.OLE;
    }
    
    private static boolean arrayContains(final byte[] larger, final byte[] smaller) {
        int largerCounter = 0;
        int smallerCounter = 0;
        while (largerCounter < larger.length) {
            if (larger[largerCounter] == smaller[smallerCounter]) {
                ++largerCounter;
                if (++smallerCounter == smaller.length) {
                    return true;
                }
                continue;
            }
            else {
                largerCounter = largerCounter - smallerCounter + 1;
                smallerCounter = 0;
            }
        }
        return false;
    }
    
    private static Set<String> getTopLevelNames(final TikaInputStream stream) throws IOException {
        final FileChannel channel = stream.getFileChannel();
        try {
            final NPOIFSFileSystem fs = new NPOIFSFileSystem(channel);
            stream.setOpenContainer(fs);
            return getTopLevelNames(fs.getRoot());
        }
        catch (IOException e) {
            return Collections.emptySet();
        }
        catch (RuntimeException e2) {
            return Collections.emptySet();
        }
    }
    
    private static Set<String> getTopLevelNames(final DirectoryNode root) {
        final Set<String> names = new HashSet<String>();
        for (final Entry entry : root) {
            names.add(entry.getName());
        }
        return names;
    }
    
    static {
        STAR_IMPRESS = new byte[] { 83, 116, 97, 114, 73, 109, 112, 114, 101, 115, 115 };
        STAR_DRAW = new byte[] { 83, 116, 97, 114, 68, 114, 97, 119 };
        WORKS_QUILL96 = new byte[] { 81, 117, 105, 108, 108, 57, 54 };
        OLE = MediaType.application("x-tika-msoffice");
        OOXML_PROTECTED = MediaType.application("x-tika-ooxml-protected");
        GENERAL_EMBEDDED = MediaType.application("x-tika-msoffice-embedded");
        OLE10_NATIVE = new MediaType(POIFSContainerDetector.GENERAL_EMBEDDED, "format", "ole10_native");
        COMP_OBJ = new MediaType(POIFSContainerDetector.GENERAL_EMBEDDED, "format", "comp_obj");
        XLS = MediaType.application("vnd.ms-excel");
        DOC = MediaType.application("msword");
        PPT = MediaType.application("vnd.ms-powerpoint");
        PUB = MediaType.application("x-mspublisher");
        VSD = MediaType.application("vnd.visio");
        WPS = MediaType.application("vnd.ms-works");
        XLR = MediaType.application("x-tika-msworks-spreadsheet");
        MSG = MediaType.application("vnd.ms-outlook");
        MPP = MediaType.application("vnd.ms-project");
        SDC = MediaType.application("vnd.stardivision.calc");
        SDA = MediaType.application("vnd.stardivision.draw");
        SDD = MediaType.application("vnd.stardivision.impress");
        SDW = MediaType.application("vnd.stardivision.writer");
        mppDataMatch = Pattern.compile("\\s\\s\\s\\d+");
    }
}
