// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.parser.xml;

import org.xml.sax.SAXException;
import org.xml.sax.Attributes;
import org.apache.tika.metadata.Property;
import org.apache.tika.metadata.Metadata;

public class AttributeMetadataHandler extends AbstractMetadataHandler
{
    private final String uri;
    private final String localName;
    
    public AttributeMetadataHandler(final String uri, final String localName, final Metadata metadata, final String name) {
        super(metadata, name);
        this.uri = uri;
        this.localName = localName;
    }
    
    public AttributeMetadataHandler(final String uri, final String localName, final Metadata metadata, final Property property) {
        super(metadata, property);
        this.uri = uri;
        this.localName = localName;
    }
    
    @Override
    public void startElement(final String uri, final String localName, final String qName, final Attributes attributes) throws SAXException {
        for (int i = 0; i < attributes.getLength(); ++i) {
            if (attributes.getURI(i).equals(this.uri) && attributes.getLocalName(i).equals(this.localName)) {
                this.addMetadata(attributes.getValue(i).trim());
            }
        }
    }
}
