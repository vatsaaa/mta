// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.parser.xml;

import org.apache.commons.logging.LogFactory;
import java.util.Arrays;
import org.xml.sax.Attributes;
import org.apache.tika.metadata.Property;
import org.apache.tika.metadata.Metadata;
import org.apache.commons.logging.Log;

public class ElementMetadataHandler extends AbstractMetadataHandler
{
    private static final Log logger;
    private static final String LOCAL_NAME_RDF_BAG = "Bag";
    private static final String LOCAL_NAME_RDF_LI = "li";
    private static final String URI_RDF = "http://www.w3.org/1999/02/22-rdf-syntax-ns#";
    private final String uri;
    private final String localName;
    private final Metadata metadata;
    private final String name;
    private Property targetProperty;
    private final StringBuilder bufferBagged;
    private final StringBuilder bufferBagless;
    private boolean isBagless;
    private int matchLevel;
    private int parentMatchLevel;
    
    public ElementMetadataHandler(final String uri, final String localName, final Metadata metadata, final String name) {
        super(metadata, name);
        this.bufferBagged = new StringBuilder();
        this.bufferBagless = new StringBuilder();
        this.isBagless = true;
        this.matchLevel = 0;
        this.parentMatchLevel = 0;
        this.uri = uri;
        this.localName = localName;
        this.metadata = metadata;
        this.name = name;
        if (ElementMetadataHandler.logger.isTraceEnabled()) {
            ElementMetadataHandler.logger.trace("created simple handler for " + this.name);
        }
    }
    
    public ElementMetadataHandler(final String uri, final String localName, final Metadata metadata, final Property targetProperty) {
        super(metadata, targetProperty);
        this.bufferBagged = new StringBuilder();
        this.bufferBagless = new StringBuilder();
        this.isBagless = true;
        this.matchLevel = 0;
        this.parentMatchLevel = 0;
        this.uri = uri;
        this.localName = localName;
        this.metadata = metadata;
        this.targetProperty = targetProperty;
        this.name = targetProperty.getName();
        if (ElementMetadataHandler.logger.isTraceEnabled()) {
            ElementMetadataHandler.logger.trace("created property handler for " + this.name);
        }
    }
    
    protected boolean isMatchingParentElement(final String uri, final String localName) {
        return uri.equals(this.uri) && localName.equals(this.localName);
    }
    
    protected boolean isMatchingElement(final String uri, final String localName) {
        return (uri.equals(this.uri) && localName.equals(this.localName)) || (this.parentMatchLevel > 0 && ((uri.equals("http://www.w3.org/1999/02/22-rdf-syntax-ns#") && localName.equals("Bag")) || (uri.equals("http://www.w3.org/1999/02/22-rdf-syntax-ns#") && localName.equals("li"))));
    }
    
    @Override
    public void startElement(final String uri, final String localName, final String name, final Attributes attributes) {
        if (this.isMatchingElement(uri, localName)) {
            ++this.matchLevel;
        }
        if (this.isMatchingParentElement(uri, localName)) {
            ++this.parentMatchLevel;
        }
    }
    
    @Override
    public void endElement(final String uri, final String localName, final String name) {
        if (this.isMatchingParentElement(uri, localName)) {
            --this.parentMatchLevel;
        }
        if (this.isMatchingElement(uri, localName)) {
            --this.matchLevel;
            if (this.matchLevel == 2) {
                this.addMetadata(this.bufferBagged.toString().trim());
                this.bufferBagged.setLength(0);
                this.isBagless = false;
            }
            if (this.matchLevel == 0 && this.isBagless) {
                final String valueBagless = this.bufferBagless.toString();
                if (valueBagless.length() > 0 && !valueBagless.contains("Bag")) {
                    this.addMetadata(valueBagless.trim());
                    this.bufferBagless.setLength(0);
                }
                this.isBagless = true;
            }
        }
    }
    
    @Override
    public void characters(final char[] ch, final int start, final int length) {
        if (this.parentMatchLevel > 0 && this.matchLevel > 2) {
            this.bufferBagged.append(ch, start, length);
        }
        if (this.parentMatchLevel > 0 && this.matchLevel > 0) {
            this.bufferBagless.append(ch, start, length);
        }
    }
    
    @Override
    public void ignorableWhitespace(final char[] ch, final int start, final int length) {
        this.characters(ch, start, length);
    }
    
    @Override
    protected void addMetadata(final String value) {
        if (ElementMetadataHandler.logger.isTraceEnabled()) {
            ElementMetadataHandler.logger.trace("adding " + this.name + "=" + value);
        }
        if (this.targetProperty != null && this.targetProperty.isMultiValuePermitted()) {
            if (value != null && value.length() > 0) {
                final String[] previous = this.metadata.getValues(this.name);
                if (previous == null || !Arrays.asList(previous).contains(value)) {
                    this.metadata.add(this.targetProperty, value);
                }
            }
        }
        else {
            super.addMetadata(value);
        }
    }
    
    static {
        logger = LogFactory.getLog(ElementMetadataHandler.class);
    }
}
