// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.parser.xml;

import java.io.IOException;
import java.io.InputStream;
import java.io.ByteArrayInputStream;
import org.apache.commons.codec.binary.Base64;
import org.xml.sax.SAXException;
import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;
import org.apache.tika.extractor.ParsingEmbeddedDocumentExtractor;
import org.apache.tika.extractor.EmbeddedDocumentExtractor;
import org.apache.tika.metadata.Metadata;
import org.xml.sax.ContentHandler;
import java.util.Collections;
import org.apache.tika.mime.MediaType;
import java.util.Set;
import org.apache.tika.parser.ParseContext;

public class FictionBookParser extends XMLParser
{
    private static final long serialVersionUID = 4195954546491524374L;
    
    @Override
    public Set<MediaType> getSupportedTypes(final ParseContext context) {
        return Collections.singleton(MediaType.application("x-fictionbook+xml"));
    }
    
    @Override
    protected ContentHandler getContentHandler(final ContentHandler handler, final Metadata metadata, final ParseContext context) {
        EmbeddedDocumentExtractor ex = context.get(EmbeddedDocumentExtractor.class);
        if (ex == null) {
            ex = new ParsingEmbeddedDocumentExtractor(context);
        }
        return new BinaryElementsDataHandler(ex, handler);
    }
    
    private static class BinaryElementsDataHandler extends DefaultHandler
    {
        private static final String ELEMENT_BINARY = "binary";
        private boolean binaryMode;
        private static final String ATTRIBUTE_ID = "id";
        private final EmbeddedDocumentExtractor partExtractor;
        private final ContentHandler handler;
        private final StringBuilder binaryData;
        private Metadata metadata;
        private static final String ATTRIBUTE_CONTENT_TYPE = "content-type";
        
        private BinaryElementsDataHandler(final EmbeddedDocumentExtractor partExtractor, final ContentHandler handler) {
            this.binaryMode = false;
            this.binaryData = new StringBuilder();
            this.partExtractor = partExtractor;
            this.handler = handler;
        }
        
        @Override
        public void startElement(final String uri, final String localName, final String qName, final Attributes attributes) throws SAXException {
            this.binaryMode = "binary".equals(localName);
            if (this.binaryMode) {
                this.binaryData.setLength(0);
                (this.metadata = new Metadata()).set("resourceName", attributes.getValue("id"));
                this.metadata.set("Content-Type", attributes.getValue("content-type"));
            }
        }
        
        @Override
        public void endElement(final String uri, final String localName, final String qName) throws SAXException {
            if (this.binaryMode) {
                try {
                    this.partExtractor.parseEmbedded(new ByteArrayInputStream(Base64.decodeBase64(this.binaryData.toString())), this.handler, this.metadata, true);
                }
                catch (IOException e) {
                    throw new SAXException("IOException in parseEmbedded", e);
                }
                this.binaryMode = false;
                this.binaryData.setLength(0);
            }
        }
        
        @Override
        public void characters(final char[] ch, final int start, final int length) throws SAXException {
            if (!this.binaryMode) {
                this.handler.characters(ch, start, length);
            }
            else {
                this.binaryData.append(ch, start, length);
            }
        }
        
        @Override
        public void ignorableWhitespace(final char[] ch, final int start, final int length) throws SAXException {
            this.handler.ignorableWhitespace(ch, start, length);
        }
    }
}
