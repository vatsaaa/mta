// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.parser.xml;

import org.xml.sax.Attributes;
import org.apache.tika.metadata.Property;
import org.apache.tika.metadata.Metadata;
import org.xml.sax.helpers.DefaultHandler;

public class MetadataHandler extends DefaultHandler
{
    private final Metadata metadata;
    private final Property property;
    private final String name;
    private final StringBuilder buffer;
    
    public MetadataHandler(final Metadata metadata, final String name) {
        this.buffer = new StringBuilder();
        this.metadata = metadata;
        this.property = null;
        this.name = name;
    }
    
    public MetadataHandler(final Metadata metadata, final Property property) {
        this.buffer = new StringBuilder();
        this.metadata = metadata;
        this.property = property;
        this.name = property.getName();
    }
    
    public void addMetadata(String value) {
        if (value.length() > 0) {
            final String previous = this.metadata.get(this.name);
            if (previous != null && previous.length() > 0) {
                value = previous + ", " + value;
            }
            if (this.property != null) {
                this.metadata.set(this.property, value);
            }
            else {
                this.metadata.set(this.name, value);
            }
        }
    }
    
    @Override
    public void endElement(final String uri, final String localName, final String name) {
        this.addMetadata(this.buffer.toString());
        this.buffer.setLength(0);
    }
    
    @Override
    public void startElement(final String uri, final String localName, final String name, final Attributes attributes) {
        for (int i = 0; i < attributes.getLength(); ++i) {
            this.addMetadata(attributes.getValue(i));
        }
    }
    
    @Override
    public void characters(final char[] ch, final int start, final int length) {
        this.buffer.append(ch, start, length);
    }
}
