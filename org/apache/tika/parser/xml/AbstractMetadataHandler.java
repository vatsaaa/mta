// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.parser.xml;

import java.util.List;
import java.util.Arrays;
import org.apache.tika.metadata.Property;
import org.apache.tika.metadata.Metadata;
import org.xml.sax.helpers.DefaultHandler;

class AbstractMetadataHandler extends DefaultHandler
{
    private final Metadata metadata;
    private final Property property;
    private final String name;
    
    protected AbstractMetadataHandler(final Metadata metadata, final String name) {
        this.metadata = metadata;
        this.property = null;
        this.name = name;
    }
    
    protected AbstractMetadataHandler(final Metadata metadata, final Property property) {
        this.metadata = metadata;
        this.property = property;
        this.name = property.getName();
    }
    
    protected void addMetadata(final String value) {
        if (value != null && value.length() > 0) {
            if (this.metadata.isMultiValued(this.name)) {
                final List<String> previous = Arrays.asList(this.metadata.getValues(this.name));
                if (!previous.contains(value)) {
                    if (this.property != null) {
                        this.metadata.add(this.property, value);
                    }
                    else {
                        this.metadata.add(this.name, value);
                    }
                }
            }
            else {
                final String previous2 = this.metadata.get(this.name);
                if (previous2 != null && previous2.length() > 0) {
                    if (!previous2.equals(value)) {
                        if (this.property != null) {
                            if (this.property.isMultiValuePermitted()) {
                                this.metadata.add(this.property, value);
                            }
                            else {
                                this.metadata.set(this.property, value);
                            }
                        }
                        else {
                            this.metadata.add(this.name, value);
                        }
                    }
                }
                else if (this.property != null) {
                    this.metadata.set(this.property, value);
                }
                else {
                    this.metadata.set(this.name, value);
                }
            }
        }
    }
}
