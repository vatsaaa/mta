// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.parser.xml;

import java.util.Collections;
import java.util.Collection;
import java.util.HashSet;
import java.util.Arrays;
import org.apache.tika.sax.TextContentHandler;
import java.io.IOException;
import org.xml.sax.SAXException;
import org.apache.tika.exception.TikaException;
import org.xml.sax.helpers.DefaultHandler;
import org.apache.tika.sax.OfflineContentHandler;
import org.apache.tika.sax.EmbeddedContentHandler;
import org.apache.tika.io.CloseShieldInputStream;
import org.apache.tika.sax.TaggedContentHandler;
import org.apache.tika.sax.XHTMLContentHandler;
import org.apache.tika.metadata.Metadata;
import org.xml.sax.ContentHandler;
import java.io.InputStream;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.mime.MediaType;
import java.util.Set;
import org.apache.tika.parser.AbstractParser;

public class XMLParser extends AbstractParser
{
    private static final long serialVersionUID = -6028836725280212837L;
    private static final Set<MediaType> SUPPORTED_TYPES;
    
    public Set<MediaType> getSupportedTypes(final ParseContext context) {
        return XMLParser.SUPPORTED_TYPES;
    }
    
    public void parse(final InputStream stream, final ContentHandler handler, final Metadata metadata, final ParseContext context) throws IOException, SAXException, TikaException {
        if (metadata.get("Content-Type") == null) {
            metadata.set("Content-Type", "application/xml");
        }
        final XHTMLContentHandler xhtml = new XHTMLContentHandler(handler, metadata);
        xhtml.startDocument();
        xhtml.startElement("p");
        final TaggedContentHandler tagged = new TaggedContentHandler(handler);
        try {
            context.getSAXParser().parse(new CloseShieldInputStream(stream), new OfflineContentHandler(new EmbeddedContentHandler(this.getContentHandler(tagged, metadata, context))));
        }
        catch (SAXException e) {
            tagged.throwIfCauseOf(e);
            throw new TikaException("XML parse error", e);
        }
        xhtml.endElement("p");
        xhtml.endDocument();
    }
    
    protected ContentHandler getContentHandler(final ContentHandler handler, final Metadata metadata, final ParseContext context) {
        return new TextContentHandler(handler);
    }
    
    static {
        SUPPORTED_TYPES = Collections.unmodifiableSet((Set<? extends MediaType>)new HashSet<MediaType>(Arrays.asList(MediaType.application("xml"), MediaType.image("svg+xml"))));
    }
}
