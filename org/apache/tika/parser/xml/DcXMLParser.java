// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.parser.xml;

import org.apache.tika.sax.TeeContentHandler;
import org.apache.tika.metadata.TikaCoreProperties;
import org.apache.tika.parser.ParseContext;
import org.xml.sax.ContentHandler;
import org.apache.tika.metadata.Property;
import org.apache.tika.metadata.Metadata;

public class DcXMLParser extends XMLParser
{
    private static final long serialVersionUID = 4905318835463880819L;
    
    private static ContentHandler getDublinCoreHandler(final Metadata metadata, final Property property, final String element) {
        return new ElementMetadataHandler("http://purl.org/dc/elements/1.1/", element, metadata, property);
    }
    
    @Override
    protected ContentHandler getContentHandler(final ContentHandler handler, final Metadata metadata, final ParseContext context) {
        return new TeeContentHandler(new ContentHandler[] { super.getContentHandler(handler, metadata, context), getDublinCoreHandler(metadata, TikaCoreProperties.TITLE, "title"), getDublinCoreHandler(metadata, TikaCoreProperties.KEYWORDS, "subject"), getDublinCoreHandler(metadata, TikaCoreProperties.CREATOR, "creator"), getDublinCoreHandler(metadata, TikaCoreProperties.DESCRIPTION, "description"), getDublinCoreHandler(metadata, TikaCoreProperties.PUBLISHER, "publisher"), getDublinCoreHandler(metadata, TikaCoreProperties.CONTRIBUTOR, "contributor"), getDublinCoreHandler(metadata, TikaCoreProperties.CREATED, "date"), getDublinCoreHandler(metadata, TikaCoreProperties.TYPE, "type"), getDublinCoreHandler(metadata, TikaCoreProperties.FORMAT, "format"), getDublinCoreHandler(metadata, TikaCoreProperties.IDENTIFIER, "identifier"), getDublinCoreHandler(metadata, TikaCoreProperties.LANGUAGE, "language"), getDublinCoreHandler(metadata, TikaCoreProperties.RIGHTS, "rights") });
    }
}
