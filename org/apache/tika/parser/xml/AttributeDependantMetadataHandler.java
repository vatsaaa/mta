// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.parser.xml;

import org.xml.sax.Attributes;
import org.apache.tika.metadata.Metadata;
import org.xml.sax.helpers.DefaultHandler;

public class AttributeDependantMetadataHandler extends DefaultHandler
{
    private final Metadata metadata;
    private final String nameHoldingAttribute;
    private final String namePrefix;
    private String name;
    private final StringBuilder buffer;
    
    public AttributeDependantMetadataHandler(final Metadata metadata, final String nameHoldingAttribute, final String namePrefix) {
        this.buffer = new StringBuilder();
        this.metadata = metadata;
        this.nameHoldingAttribute = nameHoldingAttribute;
        this.namePrefix = namePrefix;
    }
    
    public void addMetadata(String value) {
        if (this.name == null || this.name.length() == 0) {
            return;
        }
        if (value.length() > 0) {
            final String previous = this.metadata.get(this.name);
            if (previous != null && previous.length() > 0) {
                value = previous + ", " + value;
            }
            this.metadata.set(this.name, value);
        }
    }
    
    @Override
    public void endElement(final String uri, final String localName, final String name) {
        this.addMetadata(this.buffer.toString());
        this.buffer.setLength(0);
    }
    
    @Override
    public void startElement(final String uri, final String localName, final String name, final Attributes attributes) {
        final String rawName = attributes.getValue(this.nameHoldingAttribute);
        if (rawName != null) {
            if (this.namePrefix == null) {
                this.name = rawName;
            }
            else {
                this.name = this.namePrefix + rawName;
            }
        }
    }
    
    @Override
    public void characters(final char[] ch, final int start, final int length) {
        this.buffer.append(ch, start, length);
    }
}
