// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.parser.font;

import java.util.Collections;
import org.apache.tika.exception.TikaException;
import org.xml.sax.SAXException;
import java.io.IOException;
import org.apache.fontbox.ttf.TrueTypeFont;
import org.apache.tika.sax.XHTMLContentHandler;
import org.apache.tika.metadata.TikaCoreProperties;
import org.apache.tika.io.TikaInputStream;
import org.apache.fontbox.ttf.TTFParser;
import org.apache.tika.metadata.Metadata;
import org.xml.sax.ContentHandler;
import java.io.InputStream;
import org.apache.tika.parser.ParseContext;
import java.util.Set;
import org.apache.tika.mime.MediaType;
import org.apache.tika.parser.AbstractParser;

public class TrueTypeParser extends AbstractParser
{
    private static final long serialVersionUID = 44788554612243032L;
    private static final MediaType TYPE;
    private static final Set<MediaType> SUPPORTED_TYPES;
    
    public Set<MediaType> getSupportedTypes(final ParseContext context) {
        return TrueTypeParser.SUPPORTED_TYPES;
    }
    
    public void parse(final InputStream stream, final ContentHandler handler, final Metadata metadata, final ParseContext context) throws IOException, SAXException, TikaException {
        final TTFParser parser = new TTFParser();
        final TikaInputStream tis = TikaInputStream.cast(stream);
        TrueTypeFont font;
        if (tis != null && tis.hasFile()) {
            font = parser.parseTTF(tis.getFile());
        }
        else {
            font = parser.parseTTF(stream);
        }
        metadata.set("Content-Type", TrueTypeParser.TYPE.toString());
        metadata.set(TikaCoreProperties.CREATED, font.getHeader().getCreated().getTime());
        metadata.set(TikaCoreProperties.MODIFIED, font.getHeader().getModified().getTime());
        final XHTMLContentHandler xhtml = new XHTMLContentHandler(handler, metadata);
        xhtml.startDocument();
        xhtml.endDocument();
    }
    
    static {
        TYPE = MediaType.application("x-font-ttf");
        SUPPORTED_TYPES = Collections.singleton(TrueTypeParser.TYPE);
    }
}
