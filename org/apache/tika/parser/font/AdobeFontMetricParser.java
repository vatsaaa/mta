// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.parser.font;

import java.util.Collections;
import org.apache.tika.metadata.Property;
import org.apache.tika.exception.TikaException;
import org.xml.sax.SAXException;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import org.apache.fontbox.afm.FontMetric;
import org.apache.tika.sax.XHTMLContentHandler;
import org.apache.tika.metadata.TikaCoreProperties;
import org.apache.fontbox.afm.AFMParser;
import org.apache.tika.metadata.Metadata;
import org.xml.sax.ContentHandler;
import java.io.InputStream;
import org.apache.tika.parser.ParseContext;
import java.util.Set;
import org.apache.tika.mime.MediaType;
import org.apache.tika.parser.AbstractParser;

public class AdobeFontMetricParser extends AbstractParser
{
    private static final long serialVersionUID = -4820306522217196835L;
    private static final MediaType AFM_TYPE;
    private static final Set<MediaType> SUPPORTED_TYPES;
    
    public Set<MediaType> getSupportedTypes(final ParseContext context) {
        return AdobeFontMetricParser.SUPPORTED_TYPES;
    }
    
    public void parse(final InputStream stream, final ContentHandler handler, final Metadata metadata, final ParseContext context) throws IOException, SAXException, TikaException {
        final AFMParser parser = new AFMParser(stream);
        parser.parse();
        final FontMetric fontMetrics = parser.getResult();
        final List<String> comments = fontMetrics.getComments();
        this.extractCreationDate(metadata, comments);
        metadata.set("Content-Type", AdobeFontMetricParser.AFM_TYPE.toString());
        metadata.set(TikaCoreProperties.TITLE, fontMetrics.getFullName());
        this.addMetadataByString(metadata, "AvgCharacterWidth", Float.toString(fontMetrics.getAverageCharacterWidth()));
        this.addMetadataByString(metadata, "DocVersion", Float.toString(fontMetrics.getAFMVersion()));
        this.addMetadataByString(metadata, "FontName", fontMetrics.getFontName());
        this.addMetadataByString(metadata, "FontFullName", fontMetrics.getFullName());
        this.addMetadataByString(metadata, "FontFamilyName", fontMetrics.getFamilyName());
        this.addMetadataByString(metadata, "FontVersion", fontMetrics.getFontVersion());
        this.addMetadataByString(metadata, "FontWeight", fontMetrics.getWeight());
        this.addMetadataByString(metadata, "FontNotice", fontMetrics.getNotice());
        this.addMetadataByString(metadata, "FontUnderlineThickness", Float.toString(fontMetrics.getUnderlineThickness()));
        final XHTMLContentHandler xhtml = new XHTMLContentHandler(handler, metadata);
        xhtml.startDocument();
        if (comments.size() > 0) {
            xhtml.element("h1", "Comments");
            xhtml.startElement("div", "class", "comments");
            for (final String comment : comments) {
                xhtml.element("p", comment);
            }
            xhtml.endElement("div");
        }
        xhtml.endDocument();
    }
    
    private void addMetadataByString(final Metadata metadata, final String name, final String value) {
        if (value != null) {
            metadata.add(name, value);
        }
    }
    
    private void addMetadataByProperty(final Metadata metadata, final Property property, final String value) {
        if (value != null) {
            metadata.set(property, value);
        }
    }
    
    private void extractCreationDate(final Metadata metadata, final List<String> comments) {
        String date = null;
        for (final String value : comments) {
            if (value.matches(".*Creation\\sDate.*")) {
                date = value.substring(value.indexOf(":") + 2);
                comments.remove(value);
                break;
            }
        }
        if (date != null) {
            this.addMetadataByProperty(metadata, Metadata.CREATION_DATE, date);
        }
    }
    
    static {
        AFM_TYPE = MediaType.application("x-font-adobe-metric");
        SUPPORTED_TYPES = Collections.singleton(AdobeFontMetricParser.AFM_TYPE);
    }
}
