// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.parser.odf;

import java.util.Stack;
import java.util.BitSet;
import org.xml.sax.helpers.AttributesImpl;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.helpers.DefaultHandler;
import org.apache.tika.sax.OfflineContentHandler;
import org.apache.tika.io.CloseShieldInputStream;
import org.xml.sax.SAXNotRecognizedException;
import javax.xml.parsers.SAXParserFactory;
import java.util.Map;
import org.apache.tika.exception.TikaException;
import org.xml.sax.SAXException;
import java.io.IOException;
import org.apache.tika.sax.XHTMLContentHandler;
import org.apache.tika.metadata.Metadata;
import org.xml.sax.ContentHandler;
import java.io.InputStream;
import java.util.Collections;
import org.apache.tika.mime.MediaType;
import java.util.Set;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.sax.ElementMappingContentHandler;
import javax.xml.namespace.QName;
import java.util.HashMap;
import org.xml.sax.Attributes;
import org.apache.tika.parser.AbstractParser;

public class OpenDocumentContentParser extends AbstractParser
{
    public static final String TEXT_NS = "urn:oasis:names:tc:opendocument:xmlns:text:1.0";
    public static final String TABLE_NS = "urn:oasis:names:tc:opendocument:xmlns:table:1.0";
    public static final String OFFICE_NS = "urn:oasis:names:tc:opendocument:xmlns:office:1.0";
    public static final String SVG_NS = "urn:oasis:names:tc:opendocument:xmlns:svg-compatible:1.0";
    public static final String PRESENTATION_NS = "urn:oasis:names:tc:opendocument:xmlns:presentation:1.0";
    public static final String DRAW_NS = "urn:oasis:names:tc:opendocument:xmlns:drawing:1.0";
    public static final String XLINK_NS = "http://www.w3.org/1999/xlink";
    protected static final char[] TAB;
    private static final Attributes EMPTY_ATTRIBUTES;
    private static final HashMap<QName, ElementMappingContentHandler.TargetElement> MAPPINGS;
    
    public Set<MediaType> getSupportedTypes(final ParseContext context) {
        return Collections.emptySet();
    }
    
    public void parse(final InputStream stream, final ContentHandler handler, final Metadata metadata, final ParseContext context) throws IOException, SAXException, TikaException {
        this.parseInternal(stream, new XHTMLContentHandler(handler, metadata), metadata, context);
    }
    
    void parseInternal(final InputStream stream, final ContentHandler handler, final Metadata metadata, final ParseContext context) throws IOException, SAXException, TikaException {
        final DefaultHandler dh = new OpenDocumentElementMappingContentHandler(handler, (Map)OpenDocumentContentParser.MAPPINGS);
        try {
            final SAXParserFactory factory = SAXParserFactory.newInstance();
            factory.setValidating(false);
            factory.setNamespaceAware(true);
            try {
                factory.setFeature("http://javax.xml.XMLConstants/feature/secure-processing", true);
            }
            catch (SAXNotRecognizedException ex) {}
            final SAXParser parser = factory.newSAXParser();
            parser.parse(new CloseShieldInputStream(stream), new OfflineContentHandler(new NSNormalizerContentHandler(dh)));
        }
        catch (ParserConfigurationException e) {
            throw new TikaException("XML parser configuration error", e);
        }
    }
    
    static {
        TAB = new char[] { '\t' };
        EMPTY_ATTRIBUTES = new AttributesImpl();
        (MAPPINGS = new HashMap<QName, ElementMappingContentHandler.TargetElement>()).put(new QName("urn:oasis:names:tc:opendocument:xmlns:text:1.0", "p"), new ElementMappingContentHandler.TargetElement("http://www.w3.org/1999/xhtml", "p"));
        OpenDocumentContentParser.MAPPINGS.put(new QName("urn:oasis:names:tc:opendocument:xmlns:text:1.0", "line-break"), new ElementMappingContentHandler.TargetElement("http://www.w3.org/1999/xhtml", "br"));
        OpenDocumentContentParser.MAPPINGS.put(new QName("urn:oasis:names:tc:opendocument:xmlns:text:1.0", "list"), new ElementMappingContentHandler.TargetElement("http://www.w3.org/1999/xhtml", "ul"));
        OpenDocumentContentParser.MAPPINGS.put(new QName("urn:oasis:names:tc:opendocument:xmlns:text:1.0", "list-item"), new ElementMappingContentHandler.TargetElement("http://www.w3.org/1999/xhtml", "li"));
        OpenDocumentContentParser.MAPPINGS.put(new QName("urn:oasis:names:tc:opendocument:xmlns:text:1.0", "note"), new ElementMappingContentHandler.TargetElement("http://www.w3.org/1999/xhtml", "div"));
        OpenDocumentContentParser.MAPPINGS.put(new QName("urn:oasis:names:tc:opendocument:xmlns:office:1.0", "annotation"), new ElementMappingContentHandler.TargetElement("http://www.w3.org/1999/xhtml", "div"));
        OpenDocumentContentParser.MAPPINGS.put(new QName("urn:oasis:names:tc:opendocument:xmlns:presentation:1.0", "notes"), new ElementMappingContentHandler.TargetElement("http://www.w3.org/1999/xhtml", "div"));
        OpenDocumentContentParser.MAPPINGS.put(new QName("urn:oasis:names:tc:opendocument:xmlns:drawing:1.0", "object"), new ElementMappingContentHandler.TargetElement("http://www.w3.org/1999/xhtml", "object"));
        OpenDocumentContentParser.MAPPINGS.put(new QName("urn:oasis:names:tc:opendocument:xmlns:drawing:1.0", "text-box"), new ElementMappingContentHandler.TargetElement("http://www.w3.org/1999/xhtml", "div"));
        OpenDocumentContentParser.MAPPINGS.put(new QName("urn:oasis:names:tc:opendocument:xmlns:svg-compatible:1.0", "title"), new ElementMappingContentHandler.TargetElement("http://www.w3.org/1999/xhtml", "span"));
        OpenDocumentContentParser.MAPPINGS.put(new QName("urn:oasis:names:tc:opendocument:xmlns:svg-compatible:1.0", "desc"), new ElementMappingContentHandler.TargetElement("http://www.w3.org/1999/xhtml", "span"));
        OpenDocumentContentParser.MAPPINGS.put(new QName("urn:oasis:names:tc:opendocument:xmlns:text:1.0", "span"), new ElementMappingContentHandler.TargetElement("http://www.w3.org/1999/xhtml", "span"));
        final HashMap<QName, QName> aAttsMapping = new HashMap<QName, QName>();
        aAttsMapping.put(new QName("http://www.w3.org/1999/xlink", "href"), new QName("href"));
        aAttsMapping.put(new QName("http://www.w3.org/1999/xlink", "title"), new QName("title"));
        OpenDocumentContentParser.MAPPINGS.put(new QName("urn:oasis:names:tc:opendocument:xmlns:text:1.0", "a"), new ElementMappingContentHandler.TargetElement("http://www.w3.org/1999/xhtml", "a", aAttsMapping));
        OpenDocumentContentParser.MAPPINGS.put(new QName("urn:oasis:names:tc:opendocument:xmlns:table:1.0", "table"), new ElementMappingContentHandler.TargetElement("http://www.w3.org/1999/xhtml", "table"));
        OpenDocumentContentParser.MAPPINGS.put(new QName("urn:oasis:names:tc:opendocument:xmlns:table:1.0", "table-row"), new ElementMappingContentHandler.TargetElement("http://www.w3.org/1999/xhtml", "tr"));
        final HashMap<QName, QName> tableCellAttsMapping = new HashMap<QName, QName>();
        tableCellAttsMapping.put(new QName("urn:oasis:names:tc:opendocument:xmlns:table:1.0", "number-columns-spanned"), new QName("colspan"));
        tableCellAttsMapping.put(new QName("urn:oasis:names:tc:opendocument:xmlns:table:1.0", "number-rows-spanned"), new QName("rowspan"));
        tableCellAttsMapping.put(new QName("urn:oasis:names:tc:opendocument:xmlns:table:1.0", "number-columns-repeated"), new QName("colspan"));
        OpenDocumentContentParser.MAPPINGS.put(new QName("urn:oasis:names:tc:opendocument:xmlns:table:1.0", "table-cell"), new ElementMappingContentHandler.TargetElement("http://www.w3.org/1999/xhtml", "td", tableCellAttsMapping));
    }
    
    private static final class OpenDocumentElementMappingContentHandler extends ElementMappingContentHandler
    {
        private final ContentHandler handler;
        private final BitSet textNodeStack;
        private int nodeDepth;
        private int completelyFiltered;
        private Stack<String> headingStack;
        
        private OpenDocumentElementMappingContentHandler(final ContentHandler handler, final Map<QName, TargetElement> mappings) {
            super(handler, mappings);
            this.textNodeStack = new BitSet();
            this.nodeDepth = 0;
            this.completelyFiltered = 0;
            this.headingStack = new Stack<String>();
            this.handler = handler;
        }
        
        @Override
        public void characters(final char[] ch, final int start, final int length) throws SAXException {
            if (this.completelyFiltered == 0 && this.nodeDepth > 0 && this.textNodeStack.get(this.nodeDepth - 1)) {
                super.characters(ch, start, length);
            }
        }
        
        private boolean needsCompleteFiltering(final String namespaceURI, final String localName) {
            if ("urn:oasis:names:tc:opendocument:xmlns:text:1.0".equals(namespaceURI)) {
                return localName.endsWith("-template") || localName.endsWith("-style");
            }
            return "urn:oasis:names:tc:opendocument:xmlns:table:1.0".equals(namespaceURI) && "covered-table-cell".equals(localName);
        }
        
        private String getXHTMLHeaderTagName(final Attributes atts) {
            final String depthStr = atts.getValue("urn:oasis:names:tc:opendocument:xmlns:text:1.0", "outline-level");
            if (depthStr == null) {
                return "h1";
            }
            final int depth = Integer.parseInt(depthStr);
            if (depth >= 6) {
                return "h6";
            }
            if (depth <= 1) {
                return "h1";
            }
            return "h" + depth;
        }
        
        private boolean isTextNode(final String namespaceURI, final String localName) {
            return ("urn:oasis:names:tc:opendocument:xmlns:text:1.0".equals(namespaceURI) && !localName.equals("page-number") && !localName.equals("page-count")) || ("urn:oasis:names:tc:opendocument:xmlns:svg-compatible:1.0".equals(namespaceURI) && ("title".equals(localName) || "desc".equals(localName)));
        }
        
        @Override
        public void startElement(final String namespaceURI, final String localName, final String qName, final Attributes atts) throws SAXException {
            assert this.nodeDepth >= 0;
            this.textNodeStack.set(this.nodeDepth++, this.isTextNode(namespaceURI, localName));
            assert this.completelyFiltered >= 0;
            if (this.needsCompleteFiltering(namespaceURI, localName)) {
                ++this.completelyFiltered;
            }
            if (this.completelyFiltered == 0) {
                if ("urn:oasis:names:tc:opendocument:xmlns:text:1.0".equals(namespaceURI) && "h".equals(localName)) {
                    final String el = this.headingStack.push(this.getXHTMLHeaderTagName(atts));
                    this.handler.startElement("http://www.w3.org/1999/xhtml", el, el, OpenDocumentContentParser.EMPTY_ATTRIBUTES);
                }
                else {
                    super.startElement(namespaceURI, localName, qName, atts);
                }
            }
        }
        
        @Override
        public void endElement(final String namespaceURI, final String localName, final String qName) throws SAXException {
            if (this.completelyFiltered == 0) {
                if ("urn:oasis:names:tc:opendocument:xmlns:text:1.0".equals(namespaceURI) && "h".equals(localName)) {
                    final String el = this.headingStack.pop();
                    this.handler.endElement("http://www.w3.org/1999/xhtml", el, el);
                }
                else {
                    super.endElement(namespaceURI, localName, qName);
                }
                if ("urn:oasis:names:tc:opendocument:xmlns:text:1.0".equals(namespaceURI) && ("tab-stop".equals(localName) || "tab".equals(localName))) {
                    this.characters(OpenDocumentContentParser.TAB, 0, OpenDocumentContentParser.TAB.length);
                }
            }
            if (this.needsCompleteFiltering(namespaceURI, localName)) {
                --this.completelyFiltered;
            }
            assert this.completelyFiltered >= 0;
            --this.nodeDepth;
            assert this.nodeDepth >= 0;
        }
        
        @Override
        public void startPrefixMapping(final String prefix, final String uri) {
        }
        
        @Override
        public void endPrefixMapping(final String prefix) {
        }
    }
}
