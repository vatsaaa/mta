// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.parser.odf;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.AttributesImpl;
import org.xml.sax.Attributes;
import org.xml.sax.ContentHandler;
import org.apache.tika.sax.ContentHandlerDecorator;

public class NSNormalizerContentHandler extends ContentHandlerDecorator
{
    private static final String OLD_NS = "http://openoffice.org/2000/";
    private static final String NEW_NS = "urn:oasis:names:tc:opendocument:xmlns:";
    private static final String DTD_PUBLIC_ID = "-//OpenOffice.org//DTD OfficeDocument 1.0//EN";
    
    public NSNormalizerContentHandler(final ContentHandler handler) {
        super(handler);
    }
    
    private String mapOldNS(final String ns) {
        if (ns != null && ns.startsWith("http://openoffice.org/2000/")) {
            return "urn:oasis:names:tc:opendocument:xmlns:" + ns.substring("http://openoffice.org/2000/".length()) + ":1.0";
        }
        return ns;
    }
    
    @Override
    public void startElement(final String namespaceURI, final String localName, final String qName, final Attributes atts) throws SAXException {
        final AttributesImpl natts = new AttributesImpl();
        for (int i = 0; i < atts.getLength(); ++i) {
            natts.addAttribute(this.mapOldNS(atts.getURI(i)), atts.getLocalName(i), atts.getQName(i), atts.getType(i), atts.getValue(i));
        }
        super.startElement(this.mapOldNS(namespaceURI), localName, qName, atts);
    }
    
    @Override
    public void endElement(final String namespaceURI, final String localName, final String qName) throws SAXException {
        super.endElement(this.mapOldNS(namespaceURI), localName, qName);
    }
    
    @Override
    public void startPrefixMapping(final String prefix, final String uri) throws SAXException {
        super.startPrefixMapping(prefix, this.mapOldNS(uri));
    }
    
    @Override
    public InputSource resolveEntity(final String publicId, final String systemId) throws IOException, SAXException {
        if ((systemId != null && systemId.toLowerCase().endsWith(".dtd")) || "-//OpenOffice.org//DTD OfficeDocument 1.0//EN".equals(publicId)) {
            return new InputSource(new StringReader(""));
        }
        return super.resolveEntity(publicId, systemId);
    }
}
