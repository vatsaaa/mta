// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.parser.odf;

import org.apache.tika.exception.TikaException;
import org.xml.sax.SAXException;
import java.io.IOException;
import org.apache.tika.metadata.OfficeOpenXMLCore;
import java.io.InputStream;
import org.apache.tika.metadata.MSOffice;
import org.apache.tika.metadata.PagedText;
import org.apache.tika.metadata.Office;
import org.apache.tika.metadata.TikaCoreProperties;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.parser.xml.AttributeMetadataHandler;
import org.apache.tika.parser.xml.AttributeDependantMetadataHandler;
import org.apache.tika.sax.xpath.Matcher;
import org.apache.tika.sax.TeeContentHandler;
import org.apache.tika.sax.xpath.MatchingContentHandler;
import org.apache.tika.parser.xml.MetadataHandler;
import org.apache.tika.sax.xpath.CompositeMatcher;
import org.apache.tika.parser.xml.ElementMetadataHandler;
import org.xml.sax.ContentHandler;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.metadata.Property;
import org.apache.tika.sax.xpath.XPathParser;
import org.apache.tika.parser.xml.XMLParser;

public class OpenDocumentMetaParser extends XMLParser
{
    private static final long serialVersionUID = -8739250869531737584L;
    private static final String META_NS = "urn:oasis:names:tc:opendocument:xmlns:meta:1.0";
    private static final XPathParser META_XPATH;
    @Deprecated
    private static final Property TRANSITION_INITIAL_CREATOR_TO_INITIAL_AUTHOR;
    
    private static ContentHandler getDublinCoreHandler(final Metadata metadata, final Property property, final String element) {
        return new ElementMetadataHandler("http://purl.org/dc/elements/1.1/", element, metadata, property);
    }
    
    private static ContentHandler getMeta(final ContentHandler ch, final Metadata md, final Property property, final String element) {
        final Matcher matcher = new CompositeMatcher(OpenDocumentMetaParser.META_XPATH.parse("//meta:" + element), OpenDocumentMetaParser.META_XPATH.parse("//meta:" + element + "//text()"));
        final ContentHandler branch = new MatchingContentHandler(new MetadataHandler(md, property), matcher);
        return new TeeContentHandler(new ContentHandler[] { ch, branch });
    }
    
    private static ContentHandler getUserDefined(final ContentHandler ch, final Metadata md) {
        final Matcher matcher = new CompositeMatcher(OpenDocumentMetaParser.META_XPATH.parse("//meta:user-defined/@meta:name"), OpenDocumentMetaParser.META_XPATH.parse("//meta:user-defined//text()"));
        final ContentHandler branch = new MatchingContentHandler(new AttributeDependantMetadataHandler(md, "meta:name", "custom:"), matcher);
        return new TeeContentHandler(new ContentHandler[] { ch, branch });
    }
    
    @Deprecated
    private static ContentHandler getStatistic(final ContentHandler ch, final Metadata md, final String name, final String attribute) {
        final Matcher matcher = OpenDocumentMetaParser.META_XPATH.parse("//meta:document-statistic/@meta:" + attribute);
        final ContentHandler branch = new MatchingContentHandler(new AttributeMetadataHandler("urn:oasis:names:tc:opendocument:xmlns:meta:1.0", attribute, md, name), matcher);
        return new TeeContentHandler(new ContentHandler[] { ch, branch });
    }
    
    private static ContentHandler getStatistic(final ContentHandler ch, final Metadata md, final Property property, final String attribute) {
        final Matcher matcher = OpenDocumentMetaParser.META_XPATH.parse("//meta:document-statistic/@meta:" + attribute);
        final ContentHandler branch = new MatchingContentHandler(new AttributeMetadataHandler("urn:oasis:names:tc:opendocument:xmlns:meta:1.0", attribute, md, property), matcher);
        return new TeeContentHandler(new ContentHandler[] { ch, branch });
    }
    
    @Override
    protected ContentHandler getContentHandler(ContentHandler ch, final Metadata md, final ParseContext context) {
        ch = new TeeContentHandler(new ContentHandler[] { super.getContentHandler(ch, md, context), getDublinCoreHandler(md, TikaCoreProperties.TITLE, "title"), getDublinCoreHandler(md, TikaCoreProperties.CREATOR, "creator"), getDublinCoreHandler(md, TikaCoreProperties.DESCRIPTION, "description"), getDublinCoreHandler(md, TikaCoreProperties.PUBLISHER, "publisher"), getDublinCoreHandler(md, TikaCoreProperties.CONTRIBUTOR, "contributor"), getDublinCoreHandler(md, TikaCoreProperties.TYPE, "type"), getDublinCoreHandler(md, TikaCoreProperties.FORMAT, "format"), getDublinCoreHandler(md, TikaCoreProperties.IDENTIFIER, "identifier"), getDublinCoreHandler(md, TikaCoreProperties.LANGUAGE, "language"), getDublinCoreHandler(md, TikaCoreProperties.RIGHTS, "rights") });
        ch = getMeta(ch, md, TikaCoreProperties.CREATED, "creation-date");
        ch = new TeeContentHandler(new ContentHandler[] { ch, new ElementMetadataHandler("http://purl.org/dc/elements/1.1/", "date", md, TikaCoreProperties.MODIFIED) });
        ch = new TeeContentHandler(new ContentHandler[] { ch, new ElementMetadataHandler("http://purl.org/dc/elements/1.1/", "subject", md, TikaCoreProperties.TRANSITION_SUBJECT_TO_OO_SUBJECT) });
        ch = getMeta(ch, md, TikaCoreProperties.TRANSITION_KEYWORDS_TO_DC_SUBJECT, "keyword");
        ch = getMeta(ch, md, Property.externalText("Edit-Time"), "editing-duration");
        ch = getMeta(ch, md, Property.externalText("editing-cycles"), "editing-cycles");
        ch = getMeta(ch, md, OpenDocumentMetaParser.TRANSITION_INITIAL_CREATOR_TO_INITIAL_AUTHOR, "initial-creator");
        ch = getMeta(ch, md, Property.externalText("generator"), "generator");
        ch = getUserDefined(ch, md);
        ch = getStatistic(ch, md, Office.OBJECT_COUNT, "object-count");
        ch = getStatistic(ch, md, Office.IMAGE_COUNT, "image-count");
        ch = getStatistic(ch, md, Office.PAGE_COUNT, "page-count");
        ch = getStatistic(ch, md, PagedText.N_PAGES, "page-count");
        ch = getStatistic(ch, md, Office.TABLE_COUNT, "table-count");
        ch = getStatistic(ch, md, Office.PARAGRAPH_COUNT, "paragraph-count");
        ch = getStatistic(ch, md, Office.WORD_COUNT, "word-count");
        ch = getStatistic(ch, md, Office.CHARACTER_COUNT, "character-count");
        ch = getStatistic(ch, md, MSOffice.OBJECT_COUNT, "object-count");
        ch = getStatistic(ch, md, MSOffice.IMAGE_COUNT, "image-count");
        ch = getStatistic(ch, md, MSOffice.PAGE_COUNT, "page-count");
        ch = getStatistic(ch, md, MSOffice.TABLE_COUNT, "table-count");
        ch = getStatistic(ch, md, MSOffice.PARAGRAPH_COUNT, "paragraph-count");
        ch = getStatistic(ch, md, MSOffice.WORD_COUNT, "word-count");
        ch = getStatistic(ch, md, MSOffice.CHARACTER_COUNT, "character-count");
        ch = getStatistic(ch, md, "nbPage", "page-count");
        ch = getStatistic(ch, md, "nbPara", "paragraph-count");
        ch = getStatistic(ch, md, "nbWord", "word-count");
        ch = getStatistic(ch, md, "nbCharacter", "character-count");
        ch = getStatistic(ch, md, "nbTab", "table-count");
        ch = getStatistic(ch, md, "nbObject", "object-count");
        ch = getStatistic(ch, md, "nbImg", "image-count");
        ch = new NSNormalizerContentHandler(ch);
        return ch;
    }
    
    @Override
    public void parse(final InputStream stream, final ContentHandler handler, final Metadata metadata, final ParseContext context) throws IOException, SAXException, TikaException {
        super.parse(stream, handler, metadata, context);
        final String odfSubject = metadata.get(OfficeOpenXMLCore.SUBJECT);
        if (odfSubject != null && !odfSubject.equals("") && (metadata.get(TikaCoreProperties.DESCRIPTION) == null || metadata.get(TikaCoreProperties.DESCRIPTION).equals(""))) {
            metadata.set(TikaCoreProperties.DESCRIPTION, odfSubject);
        }
    }
    
    static {
        META_XPATH = new XPathParser("meta", "urn:oasis:names:tc:opendocument:xmlns:meta:1.0");
        TRANSITION_INITIAL_CREATOR_TO_INITIAL_AUTHOR = Property.composite(Office.INITIAL_AUTHOR, new Property[] { Property.externalText("initial-creator") });
    }
}
