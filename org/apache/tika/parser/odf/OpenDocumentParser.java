// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.parser.odf;

import java.util.Collections;
import java.util.Collection;
import java.util.HashSet;
import java.util.Arrays;
import org.apache.tika.exception.TikaException;
import org.xml.sax.SAXException;
import java.io.IOException;
import java.util.zip.ZipEntry;
import org.xml.sax.helpers.DefaultHandler;
import org.apache.tika.io.IOUtils;
import java.util.zip.ZipInputStream;
import org.apache.tika.sax.EndDocumentShieldingContentHandler;
import org.apache.tika.sax.XHTMLContentHandler;
import org.apache.tika.metadata.Metadata;
import org.xml.sax.ContentHandler;
import java.io.InputStream;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.parser.Parser;
import org.apache.tika.mime.MediaType;
import java.util.Set;
import org.apache.tika.parser.AbstractParser;

public class OpenDocumentParser extends AbstractParser
{
    private static final long serialVersionUID = -6410276875438618287L;
    private static final Set<MediaType> SUPPORTED_TYPES;
    private Parser meta;
    private Parser content;
    
    public OpenDocumentParser() {
        this.meta = new OpenDocumentMetaParser();
        this.content = new OpenDocumentContentParser();
    }
    
    public Parser getMetaParser() {
        return this.meta;
    }
    
    public void setMetaParser(final Parser meta) {
        this.meta = meta;
    }
    
    public Parser getContentParser() {
        return this.content;
    }
    
    public void setContentParser(final Parser content) {
        this.content = content;
    }
    
    public Set<MediaType> getSupportedTypes(final ParseContext context) {
        return OpenDocumentParser.SUPPORTED_TYPES;
    }
    
    public void parse(final InputStream stream, final ContentHandler baseHandler, final Metadata metadata, final ParseContext context) throws IOException, SAXException, TikaException {
        final XHTMLContentHandler xhtml = new XHTMLContentHandler(baseHandler, metadata);
        final EndDocumentShieldingContentHandler handler = new EndDocumentShieldingContentHandler(xhtml);
        final ZipInputStream zip = new ZipInputStream(stream);
        for (ZipEntry entry = zip.getNextEntry(); entry != null; entry = zip.getNextEntry()) {
            if (entry.getName().equals("mimetype")) {
                final String type = IOUtils.toString(zip, "UTF-8");
                metadata.set("Content-Type", type);
            }
            else if (entry.getName().equals("meta.xml")) {
                this.meta.parse(zip, new DefaultHandler(), metadata, context);
            }
            else if (entry.getName().endsWith("content.xml")) {
                if (this.content instanceof OpenDocumentContentParser) {
                    ((OpenDocumentContentParser)this.content).parseInternal(zip, handler, metadata, context);
                }
                else {
                    this.content.parse(zip, handler, metadata, context);
                }
            }
            else if (entry.getName().endsWith("styles.xml")) {
                if (this.content instanceof OpenDocumentContentParser) {
                    ((OpenDocumentContentParser)this.content).parseInternal(zip, handler, metadata, context);
                }
                else {
                    this.content.parse(zip, handler, metadata, context);
                }
            }
        }
        if (handler.getEndDocumentWasCalled()) {
            handler.reallyEndDocument();
        }
    }
    
    static {
        SUPPORTED_TYPES = Collections.unmodifiableSet((Set<? extends MediaType>)new HashSet<MediaType>(Arrays.asList(MediaType.application("vnd.sun.xml.writer"), MediaType.application("vnd.oasis.opendocument.text"), MediaType.application("vnd.oasis.opendocument.graphics"), MediaType.application("vnd.oasis.opendocument.presentation"), MediaType.application("vnd.oasis.opendocument.spreadsheet"), MediaType.application("vnd.oasis.opendocument.chart"), MediaType.application("vnd.oasis.opendocument.image"), MediaType.application("vnd.oasis.opendocument.formula"), MediaType.application("vnd.oasis.opendocument.text-master"), MediaType.application("vnd.oasis.opendocument.text-web"), MediaType.application("vnd.oasis.opendocument.text-template"), MediaType.application("vnd.oasis.opendocument.graphics-template"), MediaType.application("vnd.oasis.opendocument.presentation-template"), MediaType.application("vnd.oasis.opendocument.spreadsheet-template"), MediaType.application("vnd.oasis.opendocument.chart-template"), MediaType.application("vnd.oasis.opendocument.image-template"), MediaType.application("vnd.oasis.opendocument.formula-template"), MediaType.application("x-vnd.oasis.opendocument.text"), MediaType.application("x-vnd.oasis.opendocument.graphics"), MediaType.application("x-vnd.oasis.opendocument.presentation"), MediaType.application("x-vnd.oasis.opendocument.spreadsheet"), MediaType.application("x-vnd.oasis.opendocument.chart"), MediaType.application("x-vnd.oasis.opendocument.image"), MediaType.application("x-vnd.oasis.opendocument.formula"), MediaType.application("x-vnd.oasis.opendocument.text-master"), MediaType.application("x-vnd.oasis.opendocument.text-web"), MediaType.application("x-vnd.oasis.opendocument.text-template"), MediaType.application("x-vnd.oasis.opendocument.graphics-template"), MediaType.application("x-vnd.oasis.opendocument.presentation-template"), MediaType.application("x-vnd.oasis.opendocument.spreadsheet-template"), MediaType.application("x-vnd.oasis.opendocument.chart-template"), MediaType.application("x-vnd.oasis.opendocument.image-template"), MediaType.application("x-vnd.oasis.opendocument.formula-template"))));
    }
}
