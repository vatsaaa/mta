// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.parser;

import org.apache.tika.exception.TikaException;
import org.apache.tika.metadata.Metadata;
import org.xml.sax.ContentHandler;
import java.io.InputStream;
import java.util.Collections;
import org.apache.tika.mime.MediaType;
import java.util.Set;

public class ErrorParser extends AbstractParser
{
    public static final ErrorParser INSTANCE;
    
    public Set<MediaType> getSupportedTypes(final ParseContext context) {
        return Collections.emptySet();
    }
    
    public void parse(final InputStream stream, final ContentHandler handler, final Metadata metadata, final ParseContext context) throws TikaException {
        throw new TikaException("Parse error");
    }
    
    static {
        INSTANCE = new ErrorParser();
    }
}
