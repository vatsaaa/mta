// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.parser.txt;

abstract class CharsetRecognizer
{
    abstract String getName();
    
    public String getLanguage() {
        return null;
    }
    
    abstract int match(final CharsetDetector p0);
}
