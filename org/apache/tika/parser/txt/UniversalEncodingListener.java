// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.parser.txt;

import org.apache.tika.utils.CharsetUtils;
import org.mozilla.universalchardet.Constants;
import org.apache.tika.mime.MediaType;
import org.apache.tika.metadata.Metadata;
import java.nio.charset.Charset;
import org.mozilla.universalchardet.UniversalDetector;
import org.apache.tika.detect.TextStatistics;
import org.mozilla.universalchardet.CharsetListener;

class UniversalEncodingListener implements CharsetListener
{
    private static final String CHARSET_ISO_8859_1 = "ISO-8859-1";
    private static final String CHARSET_ISO_8859_15 = "ISO-8859-15";
    private final TextStatistics statistics;
    private final UniversalDetector detector;
    private String hint;
    private Charset charset;
    
    public UniversalEncodingListener(final Metadata metadata) {
        this.statistics = new TextStatistics();
        this.detector = new UniversalDetector(this);
        this.hint = null;
        this.charset = null;
        final MediaType type = MediaType.parse(metadata.get("Content-Type"));
        if (type != null) {
            this.hint = type.getParameters().get("charset");
        }
        if (this.hint == null) {
            this.hint = metadata.get("Content-Encoding");
        }
    }
    
    public void report(String name) {
        if (Constants.CHARSET_WINDOWS_1252.equals(name)) {
            if (this.hint != null) {
                name = this.hint;
            }
            else if (this.statistics.count(13) == 0) {
                if (this.statistics.count(164) > 0) {
                    name = "ISO-8859-15";
                }
                else {
                    name = "ISO-8859-1";
                }
            }
        }
        try {
            this.charset = CharsetUtils.forName(name);
        }
        catch (Exception ex) {}
    }
    
    public boolean isDone() {
        return this.detector.isDone();
    }
    
    public void handleData(final byte[] buf, final int offset, final int length) {
        this.statistics.addData(buf, offset, length);
        this.detector.handleData(buf, offset, length);
    }
    
    public Charset dataEnd() {
        this.detector.dataEnd();
        if (this.charset == null && this.statistics.isMostlyAscii()) {
            this.report(Constants.CHARSET_WINDOWS_1252);
        }
        return this.charset;
    }
}
