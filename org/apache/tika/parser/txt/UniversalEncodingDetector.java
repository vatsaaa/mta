// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.parser.txt;

import java.io.IOException;
import java.nio.charset.Charset;
import org.apache.tika.metadata.Metadata;
import java.io.InputStream;
import org.apache.tika.detect.EncodingDetector;

public class UniversalEncodingDetector implements EncodingDetector
{
    private static final int BUFSIZE = 1024;
    private static final int LOOKAHEAD = 16384;
    
    public Charset detect(final InputStream input, final Metadata metadata) throws IOException {
        if (input == null) {
            return null;
        }
        input.mark(16384);
        try {
            final UniversalEncodingListener listener = new UniversalEncodingListener(metadata);
            final byte[] b = new byte[1024];
            for (int n = 0, m = input.read(b); m != -1 && n < 16384 && !listener.isDone(); m = input.read(b, 0, Math.min(b.length, 16384 - n))) {
                n += m;
                listener.handleData(b, 0, m);
            }
            return listener.dataEnd();
        }
        catch (IOException e) {
            throw e;
        }
        catch (Exception e2) {
            return null;
        }
        finally {
            input.reset();
        }
    }
}
