// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.parser.txt;

import java.io.IOException;
import org.apache.tika.utils.CharsetUtils;
import org.apache.tika.mime.MediaType;
import java.nio.charset.Charset;
import org.apache.tika.metadata.Metadata;
import java.io.InputStream;
import org.apache.tika.detect.EncodingDetector;

public class Icu4jEncodingDetector implements EncodingDetector
{
    public Charset detect(final InputStream input, final Metadata metadata) throws IOException {
        if (input == null) {
            return null;
        }
        final CharsetDetector detector = new CharsetDetector();
        String incomingCharset = metadata.get("Content-Encoding");
        final String incomingType = metadata.get("Content-Type");
        if (incomingCharset == null && incomingType != null) {
            final MediaType mt = MediaType.parse(incomingType);
            if (mt != null) {
                incomingCharset = mt.getParameters().get("charset");
            }
        }
        if (incomingCharset != null) {
            detector.setDeclaredEncoding(CharsetUtils.clean(incomingCharset));
        }
        detector.enableInputFilter(true);
        detector.setText(input);
        final CharsetMatch[] arr$ = detector.detectAll();
        final int len$ = arr$.length;
        int i$ = 0;
        while (i$ < len$) {
            final CharsetMatch match = arr$[i$];
            try {
                return CharsetUtils.forName(match.getName());
            }
            catch (Exception e) {
                ++i$;
                continue;
            }
            break;
        }
        return null;
    }
}
