// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.parser.txt;

import java.util.Collections;
import org.apache.tika.exception.TikaException;
import org.xml.sax.SAXException;
import java.io.IOException;
import java.nio.charset.Charset;
import org.apache.tika.sax.XHTMLContentHandler;
import org.apache.tika.detect.AutoDetectReader;
import org.apache.tika.io.CloseShieldInputStream;
import org.apache.tika.metadata.Metadata;
import org.xml.sax.ContentHandler;
import java.io.InputStream;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.config.ServiceLoader;
import org.apache.tika.mime.MediaType;
import java.util.Set;
import org.apache.tika.parser.AbstractParser;

public class TXTParser extends AbstractParser
{
    private static final long serialVersionUID = -6656102320836888910L;
    private static final Set<MediaType> SUPPORTED_TYPES;
    private static final ServiceLoader LOADER;
    
    public Set<MediaType> getSupportedTypes(final ParseContext context) {
        return TXTParser.SUPPORTED_TYPES;
    }
    
    public void parse(final InputStream stream, final ContentHandler handler, final Metadata metadata, final ParseContext context) throws IOException, SAXException, TikaException {
        final AutoDetectReader reader = new AutoDetectReader(new CloseShieldInputStream(stream), metadata, TXTParser.LOADER);
        try {
            final Charset charset = reader.getCharset();
            final MediaType type = new MediaType(MediaType.TEXT_PLAIN, charset);
            metadata.set("Content-Type", type.toString());
            metadata.set("Content-Encoding", charset.name());
            final XHTMLContentHandler xhtml = new XHTMLContentHandler(handler, metadata);
            xhtml.startDocument();
            xhtml.startElement("p");
            final char[] buffer = new char[4096];
            for (int n = reader.read(buffer); n != -1; n = reader.read(buffer)) {
                xhtml.characters(buffer, 0, n);
            }
            xhtml.endElement("p");
            xhtml.endDocument();
        }
        finally {
            reader.close();
        }
    }
    
    static {
        SUPPORTED_TYPES = Collections.singleton(MediaType.TEXT_PLAIN);
        LOADER = new ServiceLoader(TXTParser.class.getClassLoader());
    }
}
