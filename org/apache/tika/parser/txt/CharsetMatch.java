// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.parser.txt;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ByteArrayInputStream;
import java.io.Reader;
import java.io.InputStream;

public class CharsetMatch implements Comparable<CharsetMatch>
{
    public static final int ENCODING_SCHEME = 1;
    public static final int BOM = 2;
    public static final int DECLARED_ENCODING = 4;
    public static final int LANG_STATISTICS = 8;
    private int fConfidence;
    private CharsetRecognizer fRecognizer;
    private byte[] fRawInput;
    private int fRawLength;
    private InputStream fInputStream;
    
    public Reader getReader() {
        InputStream inputStream = this.fInputStream;
        if (inputStream == null) {
            inputStream = new ByteArrayInputStream(this.fRawInput, 0, this.fRawLength);
        }
        try {
            inputStream.reset();
            return new InputStreamReader(inputStream, this.getName());
        }
        catch (IOException e) {
            return null;
        }
    }
    
    public String getString() throws IOException {
        return this.getString(-1);
    }
    
    public String getString(final int maxLength) throws IOException {
        String result = null;
        if (this.fInputStream != null) {
            final StringBuffer sb = new StringBuffer();
            final char[] buffer = new char[1024];
            final Reader reader = this.getReader();
            for (int max = (maxLength < 0) ? Integer.MAX_VALUE : maxLength, bytesRead = 0; (bytesRead = reader.read(buffer, 0, Math.min(max, 1024))) >= 0; max -= bytesRead) {
                sb.append(buffer, 0, bytesRead);
            }
            reader.close();
            return sb.toString();
        }
        result = new String(this.fRawInput, this.getName());
        return result;
    }
    
    public int getConfidence() {
        return this.fConfidence;
    }
    
    public int getMatchType() {
        return 0;
    }
    
    public String getName() {
        return this.fRecognizer.getName();
    }
    
    public String getLanguage() {
        return this.fRecognizer.getLanguage();
    }
    
    public int compareTo(final CharsetMatch other) {
        int compareResult = 0;
        if (this.fConfidence > other.fConfidence) {
            compareResult = 1;
        }
        else if (this.fConfidence < other.fConfidence) {
            compareResult = -1;
        }
        return compareResult;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (o instanceof CharsetMatch) {
            final CharsetMatch that = (CharsetMatch)o;
            return this.fConfidence == that.fConfidence;
        }
        return false;
    }
    
    @Override
    public int hashCode() {
        return this.fConfidence;
    }
    
    CharsetMatch(final CharsetDetector det, final CharsetRecognizer rec, final int conf) {
        this.fRawInput = null;
        this.fInputStream = null;
        this.fRecognizer = rec;
        this.fConfidence = conf;
        if (det.fInputStream == null) {
            this.fRawInput = det.fRawInput;
            this.fRawLength = det.fRawLength;
        }
        this.fInputStream = det.fInputStream;
    }
    
    @Override
    public String toString() {
        String s = "Match of " + this.fRecognizer.getName();
        if (this.fRecognizer.getLanguage() != null) {
            s = s + " in " + this.fRecognizer.getLanguage();
        }
        s = s + " with confidence " + this.fConfidence;
        return s;
    }
}
