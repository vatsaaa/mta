// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.parser.executable;

import java.util.Collections;
import java.util.Collection;
import java.util.HashSet;
import java.util.Arrays;
import org.apache.tika.io.EndianUtils;
import java.sql.Date;
import org.apache.poi.util.LittleEndian;
import org.apache.tika.exception.TikaException;
import org.xml.sax.SAXException;
import java.io.IOException;
import org.apache.poi.util.IOUtils;
import org.apache.tika.sax.XHTMLContentHandler;
import org.apache.tika.metadata.Metadata;
import org.xml.sax.ContentHandler;
import java.io.InputStream;
import org.apache.tika.parser.ParseContext;
import java.util.Set;
import org.apache.tika.mime.MediaType;
import org.apache.tika.parser.AbstractParser;

public class ExecutableParser extends AbstractParser implements MachineMetadata
{
    private static final long serialVersionUID = 32128791892482L;
    private static final MediaType PE_EXE;
    private static final MediaType ELF_GENERAL;
    private static final MediaType ELF_OBJECT;
    private static final MediaType ELF_EXECUTABLE;
    private static final MediaType ELF_SHAREDLIB;
    private static final MediaType ELF_COREDUMP;
    private static final Set<MediaType> SUPPORTED_TYPES;
    
    public Set<MediaType> getSupportedTypes(final ParseContext context) {
        return ExecutableParser.SUPPORTED_TYPES;
    }
    
    public void parse(final InputStream stream, final ContentHandler handler, final Metadata metadata, final ParseContext context) throws IOException, SAXException, TikaException {
        final XHTMLContentHandler xhtml = new XHTMLContentHandler(handler, metadata);
        final byte[] first4 = new byte[4];
        IOUtils.readFully(stream, first4);
        if (first4[0] == 77 && first4[1] == 90) {
            this.parsePE(xhtml, metadata, stream, first4);
        }
        else if (first4[0] == 127 && first4[1] == 69 && first4[2] == 76 && first4[3] == 70) {
            this.parseELF(xhtml, metadata, stream, first4);
        }
        xhtml.endDocument();
    }
    
    public void parsePE(final XHTMLContentHandler xhtml, final Metadata metadata, final InputStream stream, final byte[] first4) throws TikaException, IOException {
        metadata.add("Content-Type", ExecutableParser.PE_EXE.toString());
        metadata.set(ExecutableParser.PLATFORM, "Windows");
        final byte[] msdosSection = new byte[56];
        IOUtils.readFully(stream, msdosSection);
        final int peOffset = LittleEndian.readInt(stream);
        if (peOffset > 4096 || peOffset < 63) {
            return;
        }
        stream.skip(peOffset - 64);
        final byte[] pe = new byte[24];
        IOUtils.readFully(stream, pe);
        if (pe[0] == 80 && pe[1] == 69 && pe[2] == 0 && pe[3] == 0) {
            final int machine = LittleEndian.getUShort(pe, 4);
            final int numSectors = LittleEndian.getUShort(pe, 6);
            final long createdAt = LittleEndian.getInt(pe, 8);
            final long symbolTableOffset = LittleEndian.getInt(pe, 12);
            final long numSymbols = LittleEndian.getInt(pe, 16);
            final int sizeOptHdrs = LittleEndian.getUShort(pe, 20);
            final int characteristcs = LittleEndian.getUShort(pe, 22);
            final Date createdAtD = new Date(createdAt * 1000L);
            metadata.set(Metadata.CREATION_DATE, createdAtD);
            switch (machine) {
                case 332: {
                    metadata.set(ExecutableParser.MACHINE_TYPE, "x86-32");
                    metadata.set(ExecutableParser.ENDIAN, Endian.LITTLE.getName());
                    metadata.set(ExecutableParser.ARCHITECTURE_BITS, "32");
                    break;
                }
                case 34404: {
                    metadata.set(ExecutableParser.MACHINE_TYPE, "x86-32");
                    metadata.set(ExecutableParser.ENDIAN, Endian.LITTLE.getName());
                    metadata.set(ExecutableParser.ARCHITECTURE_BITS, "64");
                    break;
                }
                case 512: {
                    metadata.set(ExecutableParser.MACHINE_TYPE, "IA-64");
                    metadata.set(ExecutableParser.ENDIAN, Endian.LITTLE.getName());
                    metadata.set(ExecutableParser.ARCHITECTURE_BITS, "64");
                    break;
                }
                case 388: {
                    metadata.set(ExecutableParser.MACHINE_TYPE, "Alpha");
                    metadata.set(ExecutableParser.ENDIAN, Endian.LITTLE.getName());
                    metadata.set(ExecutableParser.ARCHITECTURE_BITS, "32");
                    break;
                }
                case 644: {
                    metadata.set(ExecutableParser.MACHINE_TYPE, "Alpha");
                    metadata.set(ExecutableParser.ENDIAN, Endian.LITTLE.getName());
                    metadata.set(ExecutableParser.ARCHITECTURE_BITS, "64");
                    break;
                }
                case 448:
                case 452: {
                    metadata.set(ExecutableParser.MACHINE_TYPE, "ARM");
                    metadata.set(ExecutableParser.ENDIAN, Endian.LITTLE.getName());
                    metadata.set(ExecutableParser.ARCHITECTURE_BITS, "32");
                    break;
                }
                case 616: {
                    metadata.set(ExecutableParser.MACHINE_TYPE, "Motorola-68000");
                    metadata.set(ExecutableParser.ENDIAN, Endian.BIG.getName());
                    metadata.set(ExecutableParser.ARCHITECTURE_BITS, "32");
                    break;
                }
                case 614:
                case 870:
                case 1126: {
                    metadata.set(ExecutableParser.MACHINE_TYPE, "MIPS");
                    metadata.set(ExecutableParser.ENDIAN, Endian.BIG.getName());
                    metadata.set(ExecutableParser.ARCHITECTURE_BITS, "16");
                    break;
                }
                case 354:
                case 358:
                case 360:
                case 361: {
                    metadata.set(ExecutableParser.MACHINE_TYPE, "MIPS");
                    metadata.set(ExecutableParser.ENDIAN, Endian.LITTLE.getName());
                    metadata.set(ExecutableParser.ARCHITECTURE_BITS, "16");
                    break;
                }
                case 496:
                case 497: {
                    metadata.set(ExecutableParser.MACHINE_TYPE, "PPC");
                    metadata.set(ExecutableParser.ENDIAN, Endian.LITTLE.getName());
                    metadata.set(ExecutableParser.ARCHITECTURE_BITS, "32");
                    break;
                }
                case 418:
                case 419: {
                    metadata.set(ExecutableParser.MACHINE_TYPE, "SH3");
                    metadata.set(ExecutableParser.ENDIAN, Endian.BIG.getName());
                    metadata.set(ExecutableParser.ARCHITECTURE_BITS, "32");
                    break;
                }
                case 422: {
                    metadata.set(ExecutableParser.MACHINE_TYPE, "SH4");
                    metadata.set(ExecutableParser.ENDIAN, Endian.BIG.getName());
                    metadata.set(ExecutableParser.ARCHITECTURE_BITS, "32");
                    break;
                }
                case 424: {
                    metadata.set(ExecutableParser.MACHINE_TYPE, "SH3");
                    metadata.set(ExecutableParser.ENDIAN, Endian.BIG.getName());
                    metadata.set(ExecutableParser.ARCHITECTURE_BITS, "32");
                    break;
                }
                case 36929: {
                    metadata.set(ExecutableParser.MACHINE_TYPE, "M32R");
                    metadata.set(ExecutableParser.ENDIAN, Endian.BIG.getName());
                    metadata.set(ExecutableParser.ARCHITECTURE_BITS, "32");
                    break;
                }
                case 3772: {
                    metadata.set(ExecutableParser.MACHINE_TYPE, "EFI");
                    break;
                }
                default: {
                    metadata.set(ExecutableParser.MACHINE_TYPE, "Unknown");
                    break;
                }
            }
        }
    }
    
    public void parseELF(final XHTMLContentHandler xhtml, final Metadata metadata, final InputStream stream, final byte[] first4) throws TikaException, IOException {
        final int architecture = stream.read();
        if (architecture == 1) {
            metadata.set(ExecutableParser.ARCHITECTURE_BITS, "32");
        }
        else if (architecture == 2) {
            metadata.set(ExecutableParser.ARCHITECTURE_BITS, "64");
        }
        final int endian = stream.read();
        if (endian == 1) {
            metadata.set(ExecutableParser.ENDIAN, Endian.LITTLE.getName());
        }
        else if (endian == 2) {
            metadata.set(ExecutableParser.ENDIAN, Endian.BIG.getName());
        }
        final int elfVer = stream.read();
        final int os = stream.read();
        final int osVer = stream.read();
        if (os > 0 || osVer > 0) {
            switch (os) {
                case 0: {
                    metadata.set(ExecutableParser.PLATFORM, "System V");
                    break;
                }
                case 1: {
                    metadata.set(ExecutableParser.PLATFORM, "HP-UX");
                    break;
                }
                case 2: {
                    metadata.set(ExecutableParser.PLATFORM, "NetBSD");
                    break;
                }
                case 3: {
                    metadata.set(ExecutableParser.PLATFORM, "Linux");
                    break;
                }
                case 6: {
                    metadata.set(ExecutableParser.PLATFORM, "Solaris");
                    break;
                }
                case 7: {
                    metadata.set(ExecutableParser.PLATFORM, "AIX");
                    break;
                }
                case 8: {
                    metadata.set(ExecutableParser.PLATFORM, "IRIX");
                    break;
                }
                case 9: {
                    metadata.set(ExecutableParser.PLATFORM, "FreeBSD");
                    break;
                }
                case 10: {
                    metadata.set(ExecutableParser.PLATFORM, "Tru64");
                    break;
                }
                case 12: {
                    metadata.set(ExecutableParser.PLATFORM, "FreeBSD");
                    break;
                }
                case 64:
                case 97: {
                    metadata.set(ExecutableParser.PLATFORM, "ARM");
                    break;
                }
                case 255: {
                    metadata.set(ExecutableParser.PLATFORM, "Embedded");
                    break;
                }
            }
        }
        final byte[] padLength = new byte[7];
        IOUtils.readFully(stream, padLength);
        int type;
        if (endian == 1) {
            type = EndianUtils.readUShortLE(stream);
        }
        else {
            type = EndianUtils.readUShortBE(stream);
        }
        switch (type) {
            case 1: {
                metadata.add("Content-Type", ExecutableParser.ELF_OBJECT.toString());
                break;
            }
            case 2: {
                metadata.add("Content-Type", ExecutableParser.ELF_EXECUTABLE.toString());
                break;
            }
            case 3: {
                metadata.add("Content-Type", ExecutableParser.ELF_SHAREDLIB.toString());
                break;
            }
            case 4: {
                metadata.add("Content-Type", ExecutableParser.ELF_COREDUMP.toString());
                break;
            }
            default: {
                metadata.add("Content-Type", ExecutableParser.ELF_GENERAL.toString());
                break;
            }
        }
        int machine;
        if (endian == 1) {
            machine = EndianUtils.readUShortLE(stream);
        }
        else {
            machine = EndianUtils.readUShortBE(stream);
        }
        switch (machine) {
            case 2:
            case 18:
            case 43: {
                metadata.set(ExecutableParser.MACHINE_TYPE, "SPARC");
                break;
            }
            case 3: {
                metadata.set(ExecutableParser.MACHINE_TYPE, "x86-32");
                break;
            }
            case 4: {
                metadata.set(ExecutableParser.MACHINE_TYPE, "Motorola-68000");
                break;
            }
            case 5: {
                metadata.set(ExecutableParser.MACHINE_TYPE, "Motorola-88000");
                break;
            }
            case 8:
            case 10: {
                metadata.set(ExecutableParser.MACHINE_TYPE, "MIPS");
                break;
            }
            case 7: {
                metadata.set(ExecutableParser.MACHINE_TYPE, "S370");
                break;
            }
            case 20:
            case 21: {
                metadata.set(ExecutableParser.MACHINE_TYPE, "PPC");
                break;
            }
            case 22: {
                metadata.set(ExecutableParser.MACHINE_TYPE, "S390");
                break;
            }
            case 40: {
                metadata.set(ExecutableParser.MACHINE_TYPE, "ARM");
                break;
            }
            case 41:
            case 36902: {
                metadata.set(ExecutableParser.MACHINE_TYPE, "Alpha");
                break;
            }
            case 50: {
                metadata.set(ExecutableParser.MACHINE_TYPE, "IA-64");
                break;
            }
            case 62: {
                metadata.set(ExecutableParser.MACHINE_TYPE, "x86-64");
                break;
            }
            case 75: {
                metadata.set(ExecutableParser.MACHINE_TYPE, "Vax");
                break;
            }
            case 88: {
                metadata.set(ExecutableParser.MACHINE_TYPE, "M32R");
                break;
            }
        }
    }
    
    static {
        PE_EXE = MediaType.application("x-msdownload");
        ELF_GENERAL = MediaType.application("x-elf");
        ELF_OBJECT = MediaType.application("x-object");
        ELF_EXECUTABLE = MediaType.application("x-executable");
        ELF_SHAREDLIB = MediaType.application("x-sharedlib");
        ELF_COREDUMP = MediaType.application("x-coredump");
        SUPPORTED_TYPES = Collections.unmodifiableSet((Set<? extends MediaType>)new HashSet<MediaType>(Arrays.asList(ExecutableParser.PE_EXE, ExecutableParser.ELF_GENERAL, ExecutableParser.ELF_OBJECT, ExecutableParser.ELF_EXECUTABLE, ExecutableParser.ELF_SHAREDLIB, ExecutableParser.ELF_COREDUMP)));
    }
}
