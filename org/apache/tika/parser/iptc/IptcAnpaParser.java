// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.parser.iptc;

import java.util.Collections;
import org.apache.tika.metadata.TikaCoreProperties;
import java.text.ParseException;
import java.util.TimeZone;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.apache.tika.exception.TikaException;
import org.xml.sax.SAXException;
import java.io.IOException;
import java.util.HashMap;
import org.apache.tika.sax.XHTMLContentHandler;
import org.apache.tika.metadata.Metadata;
import org.xml.sax.ContentHandler;
import java.io.InputStream;
import org.apache.tika.parser.ParseContext;
import java.util.Set;
import org.apache.tika.mime.MediaType;
import org.apache.tika.parser.Parser;

public class IptcAnpaParser implements Parser
{
    private static final long serialVersionUID = -6062820170212879115L;
    private static final MediaType TYPE;
    private static final Set<MediaType> SUPPORTED_TYPES;
    private int FMT_ANPA_1312;
    private int FMT_ANPA_UPI;
    private int FMT_ANPA_UPI_DL;
    private int FMT_IPTC_7901;
    private int FMT_IPTC_PHOTO;
    private int FMT_IPTC_CHAR;
    private int FMT_NITF;
    private int FMT_NITF_TT;
    private int FMT_NITF_RB;
    private int FMT_IPTC_AP;
    private int FMT_IPTC_BLM;
    private int FMT_IPTC_NYT;
    private int FMT_IPTC_RTR;
    private int FORMAT;
    private static final char SOH = '\u0001';
    private static final char STX = '\u0002';
    private static final char ETX = '\u0003';
    private static final char EOT = '\u0004';
    private static final char SYN = '\u0016';
    private static final char BS = '\b';
    private static final char TB = '\t';
    private static final char LF = '\n';
    private static final char FF = '\f';
    private static final char CR = '\r';
    private static final char XQ = '\u0011';
    private static final char XS = '\u0013';
    private static final char FS = '\u001f';
    private static final char HY = '-';
    private static final char SP = ' ';
    private static final char LT = '<';
    private static final char EQ = '=';
    private static final char CT = '^';
    private static final char SL = '\u0091';
    private static final char SR = '\u0092';
    private static final char DL = '\u0093';
    private static final char DR = '\u0094';
    
    public IptcAnpaParser() {
        this.FMT_ANPA_1312 = 0;
        this.FMT_ANPA_UPI = 1;
        this.FMT_ANPA_UPI_DL = 2;
        this.FMT_IPTC_7901 = 3;
        this.FMT_IPTC_PHOTO = 4;
        this.FMT_IPTC_CHAR = 5;
        this.FMT_NITF = 6;
        this.FMT_NITF_TT = 7;
        this.FMT_NITF_RB = 8;
        this.FMT_IPTC_AP = 9;
        this.FMT_IPTC_BLM = 10;
        this.FMT_IPTC_NYT = 11;
        this.FMT_IPTC_RTR = 12;
        this.FORMAT = this.FMT_ANPA_1312;
    }
    
    public Set<MediaType> getSupportedTypes(final ParseContext context) {
        return IptcAnpaParser.SUPPORTED_TYPES;
    }
    
    public void parse(final InputStream stream, final ContentHandler handler, final Metadata metadata, final ParseContext context) throws IOException, SAXException, TikaException {
        final HashMap<String, String> properties = this.loadProperties(stream);
        this.setMetadata(metadata, properties);
        final XHTMLContentHandler xhtml = new XHTMLContentHandler(handler, metadata);
        xhtml.startDocument();
        xhtml.startElement("p");
        final String body = this.clean(properties.get("body"));
        if (body != null) {
            xhtml.characters(body);
        }
        xhtml.endElement("p");
        xhtml.endDocument();
    }
    
    @Deprecated
    public void parse(final InputStream stream, final ContentHandler handler, final Metadata metadata) throws IOException, SAXException, TikaException {
        this.parse(stream, handler, metadata, new ParseContext());
    }
    
    private HashMap<String, String> loadProperties(final InputStream is) {
        final HashMap<String, String> properties = new HashMap<String, String>();
        this.FORMAT = this.scanFormat(is);
        final byte[] residual = this.getSection(is, "residual");
        final byte[] header = this.getSection(is, "header");
        this.parseHeader(header, properties);
        final byte[] body = this.getSection(is, "body");
        this.parseBody(body, properties);
        final byte[] footer = this.getSection(is, "footer");
        this.parseFooter(footer, properties);
        return properties;
    }
    
    private int scanFormat(final InputStream is) {
        int format = this.FORMAT;
        final int maxsize = 524288;
        final byte[] buf = new byte[maxsize];
        try {
            if (is.markSupported()) {
                is.mark(maxsize);
            }
            final int msgsize = is.read(buf);
            final String message = new String(buf).toLowerCase();
            if (message.contains("ap-wf")) {
                format = this.FMT_IPTC_AP;
            }
            if (message.contains("reuters")) {
                format = this.FMT_IPTC_RTR;
            }
            if (message.contains("new york times")) {
                format = this.FMT_IPTC_NYT;
            }
            if (message.contains("bloomberg news")) {
                format = this.FMT_IPTC_BLM;
            }
        }
        catch (IOException ex) {}
        try {
            if (is.markSupported()) {
                is.reset();
            }
        }
        catch (IOException ex2) {}
        return format;
    }
    
    private void setFormat(final int format) {
        this.FORMAT = format;
    }
    
    private String getFormatName() {
        String name = "";
        if (this.FORMAT == this.FMT_IPTC_AP) {
            name = "Associated Press";
        }
        else if (this.FORMAT == this.FMT_IPTC_BLM) {
            name = "Bloomberg";
        }
        else if (this.FORMAT == this.FMT_IPTC_NYT) {
            name = "New York Times";
        }
        else if (this.FORMAT == this.FMT_IPTC_RTR) {
            name = "Reuters";
        }
        return name;
    }
    
    private byte[] getSection(final InputStream is, final String name) {
        byte[] value = new byte[0];
        if (name.equals("residual")) {
            final int maxsize = 8192;
            final byte bstart = 22;
            final byte bfinish = 1;
            value = this.getSection(is, maxsize, bstart, bfinish, true);
        }
        else if (name.equals("header")) {
            final int maxsize = 8192;
            final byte bstart = 1;
            final byte bfinish = 2;
            value = this.getSection(is, maxsize, bstart, bfinish, true);
        }
        else if (name.equals("body")) {
            final int maxsize = 524288;
            final byte bstart = 2;
            final byte bfinish = 3;
            value = this.getSection(is, maxsize, bstart, bfinish, true);
        }
        else if (name.equals("footer")) {
            final int maxsize = 8192;
            final byte bstart = 3;
            final byte bfinish = 4;
            value = this.getSection(is, maxsize, bstart, bfinish, true);
        }
        return value;
    }
    
    private byte[] getSection(final InputStream is, int maxsize, final byte bstart, final byte bfinish, final boolean ifincomplete) {
        byte[] value = new byte[0];
        try {
            boolean started = false;
            boolean finished = false;
            int read = 0;
            int start = 0;
            final int streammax = is.available();
            maxsize = Math.min(maxsize, streammax);
            is.mark(maxsize);
            final byte[] buf = new byte[maxsize];
            int totsize = 0;
            int msgsize;
            for (int remainder = maxsize - totsize; remainder > 0; remainder -= msgsize, totsize += msgsize) {
                msgsize = is.read(buf, maxsize - remainder, maxsize);
                if (msgsize == -1) {
                    msgsize = (remainder = 0);
                }
            }
            for (read = 0; read < totsize; ++read) {
                final byte b = buf[read];
                if (!started) {
                    started = (b == bstart);
                    start = read + 1;
                }
                else if (finished = (b == bfinish)) {
                    break;
                }
            }
            is.reset();
            if (finished) {
                is.skip(read);
                value = new byte[read - start];
                System.arraycopy(buf, start, value, 0, read - start);
            }
            else if (ifincomplete && started) {
                value = new byte[read - start];
                System.arraycopy(buf, start, value, 0, read - start);
            }
        }
        catch (IOException ex) {}
        return value;
    }
    
    private boolean parseHeader(final byte[] value, final HashMap<String, String> properties) {
        boolean added = false;
        String env_serviceid = "";
        String env_category = "";
        final String env_urgency = "";
        final String hdr_edcode = "";
        String hdr_subject = "";
        String hdr_date = "";
        String hdr_time = "";
        int read = 0;
        if (read < value.length) {
            while (read < value.length) {
                final byte val_next = value[read++];
                if (val_next == 31) {
                    break;
                }
                env_serviceid += (char)(val_next & 0xFF);
            }
            while (read < value.length) {
                byte val_next = value[read++];
                if (val_next != 19) {
                    env_category += (char)(val_next & 0xFF);
                }
                else {
                    val_next = value[read];
                    if (val_next == 17) {
                        ++read;
                        break;
                    }
                    break;
                }
            }
            while (read < value.length) {
                boolean subject = true;
                byte val_next2 = value[read++];
                while (subject && val_next2 != 32 && val_next2 != 0) {
                    hdr_subject += (char)(val_next2 & 0xFF);
                    val_next2 = (byte)((read < value.length) ? value[read++] : 0);
                    while (val_next2 == 32) {
                        subject = false;
                        val_next2 = (byte)((read < value.length) ? value[read++] : 0);
                        if (val_next2 != 32) {
                            --read;
                        }
                    }
                }
                if (!subject) {
                    break;
                }
            }
            while (read < value.length) {
                byte val_next = value[read++];
                if (hdr_date.length() == 0) {
                    while ((val_next >= 48 && val_next <= 57) || val_next == 45) {
                        hdr_date += (char)(val_next & 0xFF);
                        val_next = (byte)((read < value.length) ? value[read++] : 0);
                    }
                }
                else if (val_next == 32) {
                    while (val_next == 32) {
                        val_next = (byte)((read < value.length) ? value[read++] : 0);
                    }
                }
                else {
                    while ((val_next >= 48 && val_next <= 57) || val_next == 45) {
                        hdr_time += (char)(val_next & 0xFF);
                        val_next = (byte)((read < value.length) ? value[read++] : 0);
                    }
                }
            }
        }
        added = (env_serviceid.length() + env_category.length() + hdr_subject.length() + hdr_date.length() + hdr_time.length() > 0);
        return added;
    }
    
    private boolean parseBody(byte[] value, final HashMap<String, String> properties) {
        boolean added = false;
        String bdy_heading = "";
        String bdy_title = "";
        String bdy_source = "";
        String bdy_author = "";
        String bdy_body = "";
        int read = 0;
        for (boolean done = false; !done && read < value.length; done = true) {
            while (read < value.length) {
                byte val_next = value[read++];
                if (val_next == 94) {
                    val_next = (byte)((read < value.length) ? value[read++] : 0);
                    while (val_next != 60 && val_next != 13 && val_next != 10) {
                        bdy_heading += (char)(val_next & 0xFF);
                        val_next = (byte)((read < value.length) ? value[read++] : 0);
                        if (read > value.length) {
                            break;
                        }
                    }
                    if (val_next == 60) {
                        val_next = (byte)((read < value.length) ? value[read++] : 0);
                    }
                    while (bdy_heading.length() > 0 && (val_next == 13 || val_next == 10)) {
                        val_next = (byte)((read < value.length) ? value[read++] : 0);
                        if (val_next != 13 && val_next != 10) {
                            --read;
                        }
                    }
                    break;
                }
                if (this.FORMAT != this.FMT_IPTC_RTR || val_next == 94) {
                    break;
                }
                if (val_next != 32 && val_next != 9 && val_next != 13 && val_next != 10 && read == 1) {
                    final byte[] resize = new byte[value.length + 1];
                    System.arraycopy(value, 0, resize, 1, value.length);
                    value = resize;
                }
                value[--read] = 94;
            }
            while (read < value.length) {
                byte val_next = value[read++];
                if (val_next == 94) {
                    val_next = (byte)((read < value.length) ? value[read++] : 0);
                    while (val_next != 60 && val_next != 94 && val_next != 13 && val_next != 10) {
                        bdy_title += (char)(val_next & 0xFF);
                        val_next = (byte)((read < value.length) ? value[read++] : 0);
                        if (read > value.length) {
                            break;
                        }
                    }
                    if (val_next == 94) {
                        --read;
                    }
                    if (val_next == 60) {
                        val_next = (byte)((read < value.length) ? value[read++] : 0);
                    }
                    while (bdy_title.length() > 0 && (val_next == 13 || val_next == 10)) {
                        val_next = (byte)((read < value.length) ? value[read++] : 0);
                        if (val_next != 13 && val_next != 10) {
                            --read;
                        }
                    }
                    break;
                }
                if (this.FORMAT == this.FMT_IPTC_BLM && val_next == 9) {
                    value[--read] = 94;
                }
                else {
                    if (this.FORMAT != this.FMT_IPTC_RTR || val_next == 94) {
                        break;
                    }
                    if (val_next != 32 && val_next != 9 && val_next != 13 && val_next != 10) {
                        --read;
                    }
                    value[--read] = 94;
                }
            }
            boolean metastarted = false;
            String longline = "";
            String longkey = "";
            while (read < value.length) {
                byte val_next2 = value[read++];
                if (val_next2 != 32 && val_next2 != 9 && val_next2 != 13) {
                    if (val_next2 == 10) {
                        continue;
                    }
                    if (val_next2 == 94) {
                        val_next2 = (byte)((read < value.length) ? value[read++] : 0);
                        String tmp_line = "";
                        while (val_next2 != 60 && val_next2 != 94 && val_next2 != 13 && val_next2 != 10 && val_next2 != 0) {
                            tmp_line += (char)(val_next2 & 0xFF);
                            val_next2 = (byte)((read < value.length) ? value[read++] : 0);
                            if (read > value.length) {
                                break;
                            }
                        }
                        if (val_next2 == 94) {
                            --read;
                        }
                        if (val_next2 == 60) {
                            val_next2 = (byte)((read < value.length) ? value[read++] : 0);
                        }
                        while (val_next2 == 13 || val_next2 == 10) {
                            val_next2 = (byte)((read < value.length) ? value[read++] : 0);
                            if (val_next2 != 13 && val_next2 != 10) {
                                --read;
                            }
                        }
                        if (tmp_line.toLowerCase().startsWith("by") || longline.equals("bdy_author")) {
                            longkey = "bdy_author";
                            tmp_line = (longline.equals(longkey) ? " " : "") + tmp_line;
                            int term = tmp_line.length();
                            term = Math.min(term, (tmp_line.indexOf("<") > -1) ? tmp_line.indexOf("<") : term);
                            term = Math.min(term, (tmp_line.indexOf("=") > -1) ? tmp_line.indexOf("=") : term);
                            term = Math.min(term, (tmp_line.indexOf("\n") > -1) ? tmp_line.indexOf("\n") : term);
                            term = ((term > 0) ? term : tmp_line.length());
                            bdy_author += tmp_line.substring(tmp_line.indexOf(" "), term);
                            metastarted = true;
                            longline = ((tmp_line.indexOf("=") > -1 && !longline.equals(longkey)) ? longkey : "");
                        }
                        else if (this.FORMAT == this.FMT_IPTC_BLM) {
                            final String byline = "   by ";
                            if (tmp_line.toLowerCase().contains(byline)) {
                                longkey = "bdy_author";
                                int term2 = tmp_line.length();
                                term2 = Math.min(term2, (tmp_line.indexOf("<") > -1) ? tmp_line.indexOf("<") : term2);
                                term2 = Math.min(term2, (tmp_line.indexOf("=") > -1) ? tmp_line.indexOf("=") : term2);
                                term2 = Math.min(term2, (tmp_line.indexOf("\n") > -1) ? tmp_line.indexOf("\n") : term2);
                                term2 = ((term2 > 0) ? term2 : tmp_line.length());
                                bdy_author = bdy_author + tmp_line.substring(tmp_line.toLowerCase().indexOf(byline) + byline.length(), term2) + " ";
                                metastarted = true;
                                longline = ((tmp_line.indexOf("=") > -1 && !longline.equals(longkey)) ? longkey : "");
                            }
                            else if (tmp_line.toLowerCase().startsWith("c.")) {
                                if (val_next2 != 9) {
                                    continue;
                                }
                                value[--read] = 94;
                            }
                            else {
                                if (!tmp_line.toLowerCase().trim().startsWith("(") || !tmp_line.toLowerCase().trim().endsWith(")") || val_next2 != 9) {
                                    continue;
                                }
                                value[--read] = 94;
                            }
                        }
                        else if (tmp_line.toLowerCase().startsWith("eds") || longline.equals("bdy_source")) {
                            longkey = "bdy_source";
                            tmp_line = (longline.equals(longkey) ? " " : "") + tmp_line;
                            int term = tmp_line.length();
                            term = Math.min(term, (tmp_line.indexOf("<") > -1) ? tmp_line.indexOf("<") : term);
                            term = Math.min(term, (tmp_line.indexOf("=") > -1) ? tmp_line.indexOf("=") : term);
                            term = ((term > 0) ? term : tmp_line.length());
                            bdy_source = bdy_source + tmp_line.substring(tmp_line.indexOf(" ") + 1, term) + " ";
                            metastarted = true;
                            longline = (longline.equals(longkey) ? "" : longkey);
                        }
                        else if (!metastarted) {
                            bdy_title = bdy_title + " , " + tmp_line;
                        }
                        else {
                            bdy_body = bdy_body + " " + tmp_line + " , ";
                        }
                    }
                    else {
                        while (read < value.length && val_next2 != 0) {
                            bdy_body += (char)(val_next2 & 0xFF);
                            val_next2 = (byte)((read < value.length) ? value[read++] : 0);
                            if (read > value.length) {
                                break;
                            }
                        }
                    }
                }
            }
        }
        properties.put("body", bdy_body);
        properties.put("title", bdy_title);
        properties.put("subject", bdy_heading);
        properties.put("author", bdy_author);
        properties.put("source", bdy_source);
        added = (bdy_body.length() + bdy_title.length() + bdy_heading.length() + bdy_author.length() + bdy_source.length() > 0);
        return added;
    }
    
    private boolean parseFooter(final byte[] value, final HashMap<String, String> properties) {
        boolean added = false;
        String ftr_source = "";
        String ftr_datetime = "";
        int read = 0;
        for (boolean done = false; !done && read < value.length; done = true) {
            byte val_next = value[read++];
            final byte val_peek = (byte)((read < value.length) ? value[read + 1] : 0);
            while ((val_next < 48 || val_next > 57) && val_next != 0) {
                ftr_source += (char)(val_next & 0xFF);
                val_next = (byte)((read < value.length) ? value[read] : 0);
                if (++read > value.length) {
                    break;
                }
            }
            while (val_next != 60 && val_next != 13 && val_next != 10 && val_next != 0) {
                ftr_datetime += (char)(val_next & 0xFF);
                val_next = (byte)((read < value.length) ? value[read++] : 0);
                if (read > value.length) {
                    break;
                }
            }
            if (val_next == 60) {
                val_next = (byte)((read < value.length) ? value[read++] : 0);
            }
            if (ftr_datetime.length() > 0) {
                final String format_out = "yyyy-MM-dd'T'HH:mm:ss'Z'";
                Date dateunix = new Date();
                try {
                    String format_in = "MM-dd-yy HHmmzzz";
                    if (this.FORMAT == this.FMT_IPTC_RTR) {
                        format_in = "HH:mm MM-dd-yy";
                    }
                    final SimpleDateFormat dfi = new SimpleDateFormat(format_in);
                    dfi.setTimeZone(TimeZone.getTimeZone("UTC"));
                    dateunix = dfi.parse(ftr_datetime);
                }
                catch (ParseException ex) {}
                final SimpleDateFormat dfo = new SimpleDateFormat(format_out);
                dfo.setTimeZone(TimeZone.getTimeZone("UTC"));
                ftr_datetime = dfo.format(dateunix);
            }
            while (val_next == 13 || val_next == 10) {
                val_next = (byte)((read < value.length) ? value[read++] : 0);
                if (val_next != 13 && val_next != 10) {
                    --read;
                }
            }
        }
        properties.put("publisher", ftr_source);
        properties.put("created", ftr_datetime);
        properties.put("modified", ftr_datetime);
        added = (ftr_source.length() + ftr_datetime.length() > 0);
        return added;
    }
    
    private void setMetadata(final Metadata metadata, final HashMap<String, String> properties) {
        metadata.set("Content-Type", this.clean("text/anpa-1312"));
        metadata.set(TikaCoreProperties.TITLE, this.clean(properties.get("title")));
        metadata.set(TikaCoreProperties.KEYWORDS, this.clean(properties.get("subject")));
        metadata.set(TikaCoreProperties.CREATOR, this.clean(properties.get("author")));
        metadata.set(TikaCoreProperties.CREATED, this.clean(properties.get("created")));
        metadata.set(TikaCoreProperties.MODIFIED, this.clean(properties.get("modified")));
        metadata.set(TikaCoreProperties.SOURCE, this.clean(properties.get("source")));
        metadata.set(TikaCoreProperties.PUBLISHER, this.clean(this.getFormatName()));
    }
    
    private String clean(String value) {
        if (value == null) {
            value = "";
        }
        value = value.replaceAll("``", "`");
        value = value.replaceAll("''", "'");
        value = value.replaceAll(new String(new char[] { '\u0091' }), "'");
        value = value.replaceAll(new String(new char[] { '\u0092' }), "'");
        value = value.replaceAll(new String(new char[] { '\u0093' }), "\"");
        value = value.replaceAll(new String(new char[] { '\u0094' }), "\"");
        value = value.trim();
        return value;
    }
    
    static {
        TYPE = MediaType.text("vnd.iptc.anpa");
        SUPPORTED_TYPES = Collections.singleton(IptcAnpaParser.TYPE);
    }
}
