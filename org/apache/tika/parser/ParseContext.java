// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.parser;

import org.xml.sax.SAXNotRecognizedException;
import org.xml.sax.SAXNotSupportedException;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.SAXException;
import javax.xml.parsers.ParserConfigurationException;
import org.apache.tika.exception.TikaException;
import javax.xml.parsers.SAXParser;
import java.util.HashMap;
import java.util.Map;
import java.io.Serializable;

public class ParseContext implements Serializable
{
    private static final long serialVersionUID = -5921436862145826534L;
    private final Map<String, Object> context;
    
    public ParseContext() {
        this.context = new HashMap<String, Object>();
    }
    
    public <T> void set(final Class<T> key, final T value) {
        if (value != null) {
            this.context.put(key.getName(), value);
        }
        else {
            this.context.remove(key.getName());
        }
    }
    
    public <T> T get(final Class<T> key) {
        return (T)this.context.get(key.getName());
    }
    
    public <T> T get(final Class<T> key, final T defaultValue) {
        final T value = this.get(key);
        if (value != null) {
            return value;
        }
        return defaultValue;
    }
    
    public SAXParser getSAXParser() throws TikaException {
        final SAXParser parser = this.get(SAXParser.class);
        if (parser != null) {
            return parser;
        }
        try {
            return this.getSAXParserFactory().newSAXParser();
        }
        catch (ParserConfigurationException e) {
            throw new TikaException("Unable to configure a SAX parser", e);
        }
        catch (SAXException e2) {
            throw new TikaException("Unable to create a SAX parser", e2);
        }
    }
    
    public SAXParserFactory getSAXParserFactory() {
        SAXParserFactory factory = this.get(SAXParserFactory.class);
        if (factory == null) {
            factory = SAXParserFactory.newInstance();
            factory.setNamespaceAware(true);
            try {
                factory.setFeature("http://javax.xml.XMLConstants/feature/secure-processing", true);
            }
            catch (ParserConfigurationException e) {}
            catch (SAXNotSupportedException e2) {}
            catch (SAXNotRecognizedException ex) {}
        }
        return factory;
    }
}
