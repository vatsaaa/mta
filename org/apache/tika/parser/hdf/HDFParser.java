// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.parser.hdf;

import java.util.Collections;
import java.util.Iterator;
import ucar.nc2.Attribute;
import org.xml.sax.SAXException;
import org.apache.tika.sax.XHTMLContentHandler;
import java.io.IOException;
import org.apache.tika.exception.TikaException;
import ucar.nc2.Group;
import ucar.nc2.NetcdfFile;
import java.io.OutputStream;
import org.apache.tika.io.IOUtils;
import java.io.ByteArrayOutputStream;
import org.apache.tika.metadata.Metadata;
import org.xml.sax.ContentHandler;
import java.io.InputStream;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.mime.MediaType;
import java.util.Set;
import org.apache.tika.parser.AbstractParser;

public class HDFParser extends AbstractParser
{
    private static final long serialVersionUID = 1091208208003437549L;
    private static final Set<MediaType> SUPPORTED_TYPES;
    
    public Set<MediaType> getSupportedTypes(final ParseContext context) {
        return HDFParser.SUPPORTED_TYPES;
    }
    
    public void parse(final InputStream stream, final ContentHandler handler, final Metadata metadata, final ParseContext context) throws IOException, SAXException, TikaException {
        final ByteArrayOutputStream os = new ByteArrayOutputStream();
        IOUtils.copy(stream, os);
        String name = metadata.get("resourceName");
        if (name == null) {
            name = "";
        }
        try {
            final NetcdfFile ncFile = NetcdfFile.openInMemory(name, os.toByteArray());
            this.unravelStringMet(ncFile, null, metadata);
        }
        catch (IOException e) {
            throw new TikaException("HDF parse error", e);
        }
        final XHTMLContentHandler xhtml = new XHTMLContentHandler(handler, metadata);
        xhtml.startDocument();
        xhtml.endDocument();
    }
    
    protected void unravelStringMet(final NetcdfFile ncFile, Group group, final Metadata met) {
        if (group == null) {
            group = ncFile.getRootGroup();
        }
        for (final Attribute attribute : group.getAttributes()) {
            if (attribute.isString()) {
                met.add(attribute.getName(), attribute.getStringValue());
            }
            else {
                met.add(attribute.getName(), String.valueOf(attribute.getNumericValue()));
            }
        }
        for (final Group g : group.getGroups()) {
            this.unravelStringMet(ncFile, g, met);
        }
    }
    
    static {
        SUPPORTED_TYPES = Collections.singleton(MediaType.application("x-hdf"));
    }
}
