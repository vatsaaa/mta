// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.parser.internal;

import org.apache.tika.parser.DefaultParser;
import org.apache.tika.parser.Parser;
import java.util.Dictionary;
import java.util.Properties;
import org.apache.tika.detect.DefaultDetector;
import org.apache.tika.detect.Detector;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceRegistration;
import org.osgi.framework.BundleActivator;

public class Activator implements BundleActivator
{
    private ServiceRegistration detectorService;
    private ServiceRegistration parserService;
    
    public void start(final BundleContext context) throws Exception {
        this.detectorService = context.registerService(Detector.class.getName(), (Object)new DefaultDetector(Activator.class.getClassLoader()), (Dictionary)new Properties());
        this.parserService = context.registerService(Parser.class.getName(), (Object)new DefaultParser(Activator.class.getClassLoader()), (Dictionary)new Properties());
    }
    
    public void stop(final BundleContext context) throws Exception {
        this.parserService.unregister();
        this.detectorService.unregister();
    }
}
