// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.parser.external;

import java.util.regex.Matcher;
import java.util.Iterator;
import java.io.BufferedReader;
import org.apache.tika.io.NullOutputStream;
import java.io.OutputStream;
import org.apache.tika.io.IOUtils;
import java.io.Reader;
import java.io.InputStreamReader;
import java.io.File;
import java.io.FileInputStream;
import org.apache.tika.exception.TikaException;
import org.xml.sax.SAXException;
import java.io.IOException;
import org.apache.tika.io.TikaInputStream;
import org.apache.tika.io.TemporaryResources;
import org.apache.tika.sax.XHTMLContentHandler;
import org.apache.tika.metadata.Metadata;
import org.xml.sax.ContentHandler;
import java.io.InputStream;
import java.util.Collection;
import java.util.HashSet;
import org.apache.tika.parser.ParseContext;
import java.util.Collections;
import java.util.regex.Pattern;
import java.util.Map;
import org.apache.tika.mime.MediaType;
import java.util.Set;
import org.apache.tika.parser.AbstractParser;

public class ExternalParser extends AbstractParser
{
    private static final long serialVersionUID = -1079128990650687037L;
    public static final String INPUT_FILE_TOKEN = "${INPUT}";
    public static final String OUTPUT_FILE_TOKEN = "${OUTPUT}";
    private Set<MediaType> supportedTypes;
    private Map<Pattern, String> metadataPatterns;
    private String[] command;
    
    public ExternalParser() {
        this.supportedTypes = Collections.emptySet();
        this.metadataPatterns = null;
        this.command = new String[] { "cat" };
    }
    
    public Set<MediaType> getSupportedTypes(final ParseContext context) {
        return this.getSupportedTypes();
    }
    
    public Set<MediaType> getSupportedTypes() {
        return this.supportedTypes;
    }
    
    public void setSupportedTypes(final Set<MediaType> supportedTypes) {
        this.supportedTypes = Collections.unmodifiableSet((Set<? extends MediaType>)new HashSet<MediaType>(supportedTypes));
    }
    
    public String[] getCommand() {
        return this.command;
    }
    
    public void setCommand(final String... command) {
        this.command = command;
    }
    
    public Map<Pattern, String> getMetadataExtractionPatterns() {
        return this.metadataPatterns;
    }
    
    public void setMetadataExtractionPatterns(final Map<Pattern, String> patterns) {
        this.metadataPatterns = patterns;
    }
    
    public void parse(final InputStream stream, final ContentHandler handler, final Metadata metadata, final ParseContext context) throws IOException, SAXException, TikaException {
        final XHTMLContentHandler xhtml = new XHTMLContentHandler(handler, metadata);
        final TemporaryResources tmp = new TemporaryResources();
        try {
            this.parse(TikaInputStream.get(stream, tmp), xhtml, metadata, tmp);
        }
        finally {
            tmp.dispose();
        }
    }
    
    private void parse(final TikaInputStream stream, final XHTMLContentHandler xhtml, final Metadata metadata, final TemporaryResources tmp) throws IOException, SAXException, TikaException {
        boolean inputToStdIn = true;
        boolean outputFromStdOut = true;
        final boolean hasPatterns = this.metadataPatterns != null && !this.metadataPatterns.isEmpty();
        File output = null;
        final String[] cmd = new String[this.command.length];
        System.arraycopy(this.command, 0, cmd, 0, this.command.length);
        for (int i = 0; i < cmd.length; ++i) {
            if (cmd[i].indexOf("${INPUT}") != -1) {
                cmd[i] = cmd[i].replace("${INPUT}", stream.getFile().getPath());
                inputToStdIn = false;
            }
            if (cmd[i].indexOf("${OUTPUT}") != -1) {
                output = tmp.createTemporaryFile();
                outputFromStdOut = false;
            }
        }
        Process process;
        if (cmd.length == 1) {
            process = Runtime.getRuntime().exec(cmd[0]);
        }
        else {
            process = Runtime.getRuntime().exec(cmd);
        }
        try {
            if (inputToStdIn) {
                this.sendInput(process, stream);
            }
            else {
                process.getOutputStream().close();
            }
            final InputStream out = process.getInputStream();
            final InputStream err = process.getErrorStream();
            if (hasPatterns) {
                this.extractMetadata(err, metadata);
                if (outputFromStdOut) {
                    this.extractOutput(out, xhtml);
                }
                else {
                    this.extractMetadata(out, metadata);
                }
            }
            else {
                this.ignoreStream(err);
                if (outputFromStdOut) {
                    this.extractOutput(out, xhtml);
                }
                else {
                    this.ignoreStream(out);
                }
            }
        }
        finally {
            try {
                process.waitFor();
            }
            catch (InterruptedException ex) {}
        }
        if (!outputFromStdOut) {
            this.extractOutput(new FileInputStream(output), xhtml);
        }
    }
    
    private void extractOutput(final InputStream stream, final XHTMLContentHandler xhtml) throws SAXException, IOException {
        final Reader reader = new InputStreamReader(stream);
        try {
            xhtml.startDocument();
            xhtml.startElement("p");
            final char[] buffer = new char[1024];
            for (int n = reader.read(buffer); n != -1; n = reader.read(buffer)) {
                xhtml.characters(buffer, 0, n);
            }
            xhtml.endElement("p");
            xhtml.endDocument();
        }
        finally {
            reader.close();
        }
    }
    
    private void sendInput(final Process process, final InputStream stream) {
        new Thread() {
            @Override
            public void run() {
                final OutputStream stdin = process.getOutputStream();
                try {
                    IOUtils.copy(stream, stdin);
                }
                catch (IOException ex) {}
            }
        }.start();
    }
    
    private void ignoreStream(final InputStream stream) {
        new Thread() {
            @Override
            public void run() {
                try {
                    IOUtils.copy(stream, new NullOutputStream());
                }
                catch (IOException e) {}
                finally {
                    IOUtils.closeQuietly(stream);
                }
            }
        }.start();
    }
    
    private void extractMetadata(final InputStream stream, final Metadata metadata) {
        new Thread() {
            @Override
            public void run() {
                final BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
                try {
                    String line;
                    while ((line = reader.readLine()) != null) {
                        for (final Pattern p : ExternalParser.this.metadataPatterns.keySet()) {
                            final Matcher m = p.matcher(line);
                            if (m.find()) {
                                metadata.add(ExternalParser.this.metadataPatterns.get(p), m.group(1));
                            }
                        }
                    }
                }
                catch (IOException e) {}
                finally {
                    IOUtils.closeQuietly(reader);
                    IOUtils.closeQuietly(stream);
                }
            }
        }.start();
    }
    
    public static boolean check(final String checkCmd, final int... errorValue) {
        return check(new String[] { checkCmd }, errorValue);
    }
    
    public static boolean check(final String[] checkCmd, int... errorValue) {
        if (errorValue.length == 0) {
            errorValue = new int[] { 127 };
        }
        try {
            Process process;
            if (checkCmd.length == 1) {
                process = Runtime.getRuntime().exec(checkCmd[0]);
            }
            else {
                process = Runtime.getRuntime().exec(checkCmd);
            }
            final int result = process.waitFor();
            for (final int err : errorValue) {
                if (result == err) {
                    return false;
                }
            }
            return true;
        }
        catch (IOException e) {
            return false;
        }
        catch (InterruptedException ie) {
            return false;
        }
    }
}
