// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.parser.mail;

import java.util.Collections;
import org.apache.james.mime4j.MimeException;
import org.xml.sax.SAXException;
import java.io.IOException;
import org.apache.tika.exception.TikaException;
import org.apache.tika.io.TaggedInputStream;
import org.apache.tika.sax.XHTMLContentHandler;
import org.apache.james.mime4j.parser.MimeStreamParser;
import org.apache.james.mime4j.stream.MimeConfig;
import org.apache.tika.metadata.Metadata;
import org.xml.sax.ContentHandler;
import java.io.InputStream;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.mime.MediaType;
import java.util.Set;
import org.apache.tika.parser.AbstractParser;

public class RFC822Parser extends AbstractParser
{
    private static final long serialVersionUID = -5504243905998074168L;
    private static final Set<MediaType> SUPPORTED_TYPES;
    
    public Set<MediaType> getSupportedTypes(final ParseContext context) {
        return RFC822Parser.SUPPORTED_TYPES;
    }
    
    public void parse(final InputStream stream, final ContentHandler handler, final Metadata metadata, final ParseContext context) throws IOException, SAXException, TikaException {
        MimeConfig config = new MimeConfig();
        config.setMaxLineLen(100000);
        config.setMaxHeaderLen(100000);
        config = context.get(MimeConfig.class, config);
        final MimeStreamParser parser = new MimeStreamParser(config);
        final XHTMLContentHandler xhtml = new XHTMLContentHandler(handler, metadata);
        final MailContentHandler mch = new MailContentHandler(xhtml, metadata, context, config.isStrictParsing());
        parser.setContentHandler(mch);
        parser.setContentDecoding(true);
        final TaggedInputStream tagged = TaggedInputStream.get(stream);
        try {
            parser.parse(tagged);
        }
        catch (IOException e) {
            tagged.throwIfCauseOf(e);
            throw new TikaException("Failed to parse an email message", e);
        }
        catch (MimeException e2) {
            final Throwable cause = e2.getCause();
            if (cause instanceof TikaException) {
                throw (TikaException)cause;
            }
            if (cause instanceof SAXException) {
                throw (SAXException)cause;
            }
            throw new TikaException("Failed to parse an email message", e2);
        }
    }
    
    static {
        SUPPORTED_TYPES = Collections.singleton(MediaType.parse("message/rfc822"));
    }
}
