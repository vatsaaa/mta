// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.parser.mail;

import org.apache.james.mime4j.codec.DecoderUtil;
import org.apache.james.mime4j.dom.address.Mailbox;
import org.apache.james.mime4j.dom.address.AddressList;
import org.apache.james.mime4j.dom.field.AddressListField;
import org.apache.james.mime4j.dom.address.MailboxList;
import org.apache.james.mime4j.dom.field.ParsedField;
import org.apache.james.mime4j.dom.field.DateTimeField;
import org.apache.james.mime4j.dom.field.UnstructuredField;
import org.apache.tika.metadata.TikaCoreProperties;
import org.apache.james.mime4j.dom.address.Address;
import org.apache.james.mime4j.dom.field.MailboxListField;
import org.apache.james.mime4j.codec.DecodeMonitor;
import org.apache.james.mime4j.field.LenientFieldParser;
import org.apache.james.mime4j.stream.Field;
import java.io.IOException;
import org.apache.tika.exception.TikaException;
import org.xml.sax.SAXException;
import org.apache.james.mime4j.MimeException;
import org.apache.tika.sax.EmbeddedContentHandler;
import org.apache.tika.sax.BodyContentHandler;
import org.apache.tika.parser.Parser;
import org.apache.tika.parser.AutoDetectParser;
import java.io.InputStream;
import org.apache.james.mime4j.stream.BodyDescriptor;
import org.apache.tika.config.TikaConfig;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.sax.XHTMLContentHandler;
import org.apache.james.mime4j.parser.ContentHandler;

class MailContentHandler implements ContentHandler
{
    private boolean strictParsing;
    private XHTMLContentHandler handler;
    private ParseContext context;
    private Metadata metadata;
    private TikaConfig tikaConfig;
    private boolean inPart;
    
    MailContentHandler(final XHTMLContentHandler xhtml, final Metadata metadata, final ParseContext context, final boolean strictParsing) {
        this.strictParsing = false;
        this.tikaConfig = null;
        this.inPart = false;
        this.handler = xhtml;
        this.context = context;
        this.metadata = metadata;
        this.strictParsing = strictParsing;
    }
    
    public void body(final BodyDescriptor body, final InputStream is) throws MimeException, IOException {
        Parser parser = this.context.get(AutoDetectParser.class);
        if (parser == null) {
            parser = this.context.get(Parser.class);
        }
        if (parser == null) {
            if (this.tikaConfig == null) {
                this.tikaConfig = this.context.get(TikaConfig.class);
                if (this.tikaConfig == null) {
                    this.tikaConfig = TikaConfig.getDefaultConfig();
                }
            }
            parser = this.tikaConfig.getParser();
        }
        final Metadata submd = new Metadata();
        submd.set("Content-Type", body.getMimeType());
        submd.set("Content-Encoding", body.getCharset());
        try {
            final BodyContentHandler bch = new BodyContentHandler(this.handler);
            parser.parse(is, new EmbeddedContentHandler(bch), submd, this.context);
        }
        catch (SAXException e) {
            throw new MimeException(e);
        }
        catch (TikaException e2) {
            throw new MimeException(e2);
        }
    }
    
    public void endBodyPart() throws MimeException {
        try {
            this.handler.endElement("p");
            this.handler.endElement("div");
        }
        catch (SAXException e) {
            throw new MimeException(e);
        }
    }
    
    public void endHeader() throws MimeException {
    }
    
    public void startMessage() throws MimeException {
        try {
            this.handler.startDocument();
        }
        catch (SAXException e) {
            throw new MimeException(e);
        }
    }
    
    public void endMessage() throws MimeException {
        try {
            this.handler.endDocument();
        }
        catch (SAXException e) {
            throw new MimeException(e);
        }
    }
    
    public void endMultipart() throws MimeException {
        this.inPart = false;
    }
    
    public void epilogue(final InputStream is) throws MimeException, IOException {
    }
    
    public void field(final Field field) throws MimeException {
        if (this.inPart) {
            return;
        }
        try {
            final String fieldname = field.getName();
            final ParsedField parsedField = LenientFieldParser.getParser().parse(field, DecodeMonitor.SILENT);
            if (fieldname.equalsIgnoreCase("From")) {
                final MailboxListField fromField = (MailboxListField)parsedField;
                final MailboxList mailboxList = fromField.getMailboxList();
                if (fromField.isValidField() && mailboxList != null) {
                    for (int i = 0; i < mailboxList.size(); ++i) {
                        final String from = this.getDisplayString(mailboxList.get(i));
                        this.metadata.add("Message-From", from);
                        this.metadata.add(TikaCoreProperties.CREATOR, from);
                    }
                }
                else {
                    String from2 = this.stripOutFieldPrefix(field, "From:");
                    if (from2.startsWith("<")) {
                        from2 = from2.substring(1);
                    }
                    if (from2.endsWith(">")) {
                        from2 = from2.substring(0, from2.length() - 1);
                    }
                    this.metadata.add("Message-From", from2);
                    this.metadata.add(TikaCoreProperties.CREATOR, from2);
                }
            }
            else if (fieldname.equalsIgnoreCase("Subject")) {
                this.metadata.add(TikaCoreProperties.TRANSITION_SUBJECT_TO_DC_TITLE, ((UnstructuredField)parsedField).getValue());
            }
            else if (fieldname.equalsIgnoreCase("To")) {
                this.processAddressList(parsedField, "To:", "Message-To");
            }
            else if (fieldname.equalsIgnoreCase("CC")) {
                this.processAddressList(parsedField, "Cc:", "Message-Cc");
            }
            else if (fieldname.equalsIgnoreCase("BCC")) {
                this.processAddressList(parsedField, "Bcc:", "Message-Bcc");
            }
            else if (fieldname.equalsIgnoreCase("Date")) {
                final DateTimeField dateField = (DateTimeField)parsedField;
                this.metadata.set(TikaCoreProperties.CREATED, dateField.getDate());
            }
        }
        catch (RuntimeException me) {
            if (this.strictParsing) {
                throw me;
            }
        }
    }
    
    private void processAddressList(final ParsedField field, final String addressListType, final String metadataField) throws MimeException {
        final AddressListField toField = (AddressListField)field;
        if (toField.isValidField()) {
            final AddressList addressList = toField.getAddressList();
            for (int i = 0; i < addressList.size(); ++i) {
                this.metadata.add(metadataField, this.getDisplayString(addressList.get(i)));
            }
        }
        else {
            final String to = this.stripOutFieldPrefix(field, addressListType);
            for (final String eachTo : to.split(",")) {
                this.metadata.add(metadataField, eachTo.trim());
            }
        }
    }
    
    private String getDisplayString(final Address address) {
        if (!(address instanceof Mailbox)) {
            return address.toString();
        }
        final Mailbox mailbox = (Mailbox)address;
        String name = mailbox.getName();
        if (name != null && name.length() > 0) {
            name = DecoderUtil.decodeEncodedWords(name, DecodeMonitor.SILENT);
            return name + " <" + mailbox.getAddress() + ">";
        }
        return mailbox.getAddress();
    }
    
    public void preamble(final InputStream is) throws MimeException, IOException {
    }
    
    public void raw(final InputStream is) throws MimeException, IOException {
    }
    
    public void startBodyPart() throws MimeException {
        try {
            this.handler.startElement("div", "class", "email-entry");
            this.handler.startElement("p");
        }
        catch (SAXException e) {
            throw new MimeException(e);
        }
    }
    
    public void startHeader() throws MimeException {
    }
    
    public void startMultipart(final BodyDescriptor descr) throws MimeException {
        this.inPart = true;
    }
    
    private String stripOutFieldPrefix(final Field field, final String fieldname) {
        String temp;
        int loc;
        for (temp = field.getRaw().toString(), loc = fieldname.length(); temp.charAt(loc) == ' '; ++loc) {}
        return temp.substring(loc);
    }
}
