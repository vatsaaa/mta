// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.parser.crypto;

import org.xml.sax.SAXException;
import java.io.IOException;
import org.bouncycastle.cms.CMSTypedStream;
import org.bouncycastle.cms.CMSException;
import org.apache.tika.exception.TikaException;
import org.apache.tika.parser.EmptyParser;
import org.apache.tika.parser.Parser;
import org.bouncycastle.cms.CMSSignedDataParser;
import org.apache.tika.io.CloseShieldInputStream;
import org.apache.tika.metadata.Metadata;
import org.xml.sax.ContentHandler;
import java.io.InputStream;
import java.util.Set;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.mime.MediaType;
import org.apache.tika.parser.AbstractParser;

public class Pkcs7Parser extends AbstractParser
{
    private static final long serialVersionUID = -7310531559075115044L;
    private static final MediaType PKCS7_MIME;
    private static final MediaType PKCS7_SIGNATURE;
    
    public Set<MediaType> getSupportedTypes(final ParseContext context) {
        return MediaType.set(Pkcs7Parser.PKCS7_MIME, Pkcs7Parser.PKCS7_SIGNATURE);
    }
    
    public void parse(final InputStream stream, final ContentHandler handler, final Metadata metadata, final ParseContext context) throws IOException, SAXException, TikaException {
        try {
            final CMSSignedDataParser parser = new CMSSignedDataParser(new CloseShieldInputStream(stream));
            try {
                final CMSTypedStream content = parser.getSignedContent();
                final InputStream input = content.getContentStream();
                try {
                    final Parser delegate = context.get(Parser.class, EmptyParser.INSTANCE);
                    delegate.parse(input, handler, metadata, context);
                }
                finally {
                    input.close();
                }
            }
            finally {
                parser.close();
            }
        }
        catch (CMSException e) {
            throw new TikaException("Unable to parse pkcs7 signed data", e);
        }
    }
    
    static {
        PKCS7_MIME = MediaType.application("pkcs7-mime");
        PKCS7_SIGNATURE = MediaType.application("pkcs7-signature");
    }
}
