// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.parser.chm;

import java.util.Collections;
import org.apache.tika.exception.TikaException;
import org.xml.sax.SAXException;
import java.io.IOException;
import org.apache.tika.metadata.Metadata;
import org.xml.sax.ContentHandler;
import java.io.InputStream;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.mime.MediaType;
import java.util.Set;
import org.apache.tika.parser.AbstractParser;

public class ChmParser extends AbstractParser
{
    private static final long serialVersionUID = 5938777307516469802L;
    private static final Set<MediaType> SUPPORTED_TYPES;
    
    public Set<MediaType> getSupportedTypes(final ParseContext context) {
        return ChmParser.SUPPORTED_TYPES;
    }
    
    public void parse(final InputStream stream, final ContentHandler handler, final Metadata metadata, final ParseContext context) throws IOException, SAXException, TikaException {
        final CHMDocumentInformation chmInfo = CHMDocumentInformation.load(stream);
        metadata.set("Content-Type", "chm");
        this.extractMetadata(chmInfo, metadata);
        CHM2XHTML.process(chmInfo, handler);
    }
    
    private void extractMetadata(final CHMDocumentInformation chmInfo, final Metadata metadata) throws TikaException, IOException {
        chmInfo.getCHMDocInformation(metadata);
    }
    
    static {
        SUPPORTED_TYPES = Collections.singleton(MediaType.application("chm"));
    }
}
