// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.parser.chm.exception;

import org.apache.tika.exception.TikaException;

public class ChmParsingException extends TikaException
{
    private static final long serialVersionUID = 6497936044733665210L;
    
    public ChmParsingException(final String description) {
        super(description);
    }
}
