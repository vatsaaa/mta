// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.parser.chm.lzx;

import java.util.concurrent.CancellationException;
import org.apache.tika.exception.TikaException;
import org.apache.tika.parser.chm.exception.ChmParsingException;
import org.apache.tika.parser.chm.core.ChmCommons;

public class ChmLzxState
{
    private int window;
    private long window_size;
    private int window_position;
    private int main_tree_elements;
    private ChmCommons.LzxState hadStarted;
    private int block_type;
    private int block_length;
    private int block_remaining;
    private int frames_read;
    private int intel_file_size;
    private long intel_current_possition;
    private ChmCommons.IntelState intel_state;
    private long R0;
    private long R1;
    private long R2;
    protected short[] mainTreeLengtsTable;
    protected short[] mainTreeTable;
    protected short[] lengthTreeTable;
    protected short[] lengthTreeLengtsTable;
    protected short[] alignedLenTable;
    protected short[] alignedTreeTable;
    
    protected short[] getMainTreeTable() {
        return this.mainTreeTable;
    }
    
    protected short[] getAlignedTreeTable() {
        return this.alignedTreeTable;
    }
    
    protected void setAlignedTreeTable(final short[] alignedTreeTable) {
        this.alignedTreeTable = alignedTreeTable;
    }
    
    protected short[] getLengthTreeTable() throws TikaException {
        if (this.lengthTreeTable != null) {
            return this.lengthTreeTable;
        }
        throw new ChmParsingException("lengthTreeTable is null");
    }
    
    protected void setLengthTreeTable(final short[] lengthTreeTable) {
        this.lengthTreeTable = lengthTreeTable;
    }
    
    protected void setMainTreeTable(final short[] mainTreeTable) {
        this.mainTreeTable = mainTreeTable;
    }
    
    protected short[] getAlignedLenTable() {
        return this.alignedLenTable;
    }
    
    protected void setAlignedLenTable(final short[] alignedLenTable) {
        this.alignedLenTable = alignedLenTable;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("actual decoding window:=" + this.getWindow() + System.getProperty("line.separator"));
        sb.append("window size (32Kb through 2Mb):=" + this.getWindowSize() + System.getProperty("line.separator"));
        sb.append("current offset within the window:=" + this.getWindowPosition() + System.getProperty("line.separator"));
        sb.append("number of main tree elements:=" + this.getMainTreeElements() + System.getProperty("line.separator"));
        sb.append("have we started decoding at all yet?:=" + this.getHadStarted() + System.getProperty("line.separator"));
        sb.append("type of this block:=" + this.getBlockType() + System.getProperty("line.separator"));
        sb.append("uncompressed length of this block:=" + this.getBlockLength() + System.getProperty("line.separator"));
        sb.append("uncompressed bytes still left to decode:=" + this.getBlockRemaining() + System.getProperty("line.separator"));
        sb.append("the number of CFDATA blocks processed:=" + this.getFramesRead() + System.getProperty("line.separator"));
        sb.append("magic header value used for transform:=" + this.getIntelFileSize() + System.getProperty("line.separator"));
        sb.append("current offset in transform space:=" + this.getIntelCurrentPossition() + System.getProperty("line.separator"));
        sb.append("have we seen any translatable data yet?:=" + this.getIntelState() + System.getProperty("line.separator"));
        sb.append("R0 for the LRU offset system:=" + this.getR0() + System.getProperty("line.separator"));
        sb.append("R1 for the LRU offset system:=" + this.getR1() + System.getProperty("line.separator"));
        sb.append("R2 for the LRU offset system:=" + this.getR2() + System.getProperty("line.separator"));
        sb.append("main tree length:=" + this.getMainTreeLengtsTable().length + System.getProperty("line.separator"));
        sb.append("secondary tree length:=" + this.getLengthTreeLengtsTable().length + System.getProperty("line.separator"));
        return sb.toString();
    }
    
    public ChmLzxState(final int window) throws TikaException {
        if (window < 0) {
            throw new CancellationException("window size should be more than zero");
        }
        final int win = ChmCommons.getWindowSize(window);
        this.setWindowSize(1 << win);
        if (win < 15 || win > 21) {
            throw new ChmParsingException("window less than 15 or window greater than 21");
        }
        if (win == 20) {
            final int position_slots = 42;
        }
        else if (win == 21) {
            final int position_slots = 50;
        }
        else {
            final int position_slots = win << 1;
        }
        this.setR0(1L);
        this.setR1(1L);
        this.setR2(1L);
        this.setMainTreeElements(512);
        this.setHadStarted(ChmCommons.LzxState.NOT_STARTED_DECODING);
        this.setFramesRead(0);
        this.setBlockRemaining(0);
        this.setBlockType(0);
        this.setIntelCurrentPossition(0L);
        this.setIntelState(ChmCommons.IntelState.NOT_STARTED);
        this.setWindowPosition(0);
        this.setMainTreeLengtsTable(new short[this.getMainTreeElements()]);
        this.setLengthTreeLengtsTable(new short[249]);
    }
    
    protected void setWindow(final int window) {
        this.window = window;
    }
    
    protected int getWindow() {
        return this.window;
    }
    
    protected void setWindowSize(final long window_size) {
        this.window_size = window_size;
    }
    
    protected long getWindowSize() {
        return this.window_size;
    }
    
    protected void setWindowPosition(final int window_position) {
        this.window_position = window_position;
    }
    
    protected int getWindowPosition() {
        return this.window_position;
    }
    
    protected void setMainTreeElements(final int main_tree_elements) {
        this.main_tree_elements = main_tree_elements;
    }
    
    protected int getMainTreeElements() {
        return this.main_tree_elements;
    }
    
    protected void setHadStarted(final ChmCommons.LzxState hadStarted) {
        this.hadStarted = hadStarted;
    }
    
    protected ChmCommons.LzxState getHadStarted() {
        return this.hadStarted;
    }
    
    protected void setBlockType(final int block_type) {
        this.block_type = block_type;
    }
    
    public int getBlockType() {
        return this.block_type;
    }
    
    protected void setBlockLength(final int block_length) {
        this.block_length = block_length;
    }
    
    protected int getBlockLength() {
        return this.block_length;
    }
    
    protected void setBlockRemaining(final int block_remaining) {
        this.block_remaining = block_remaining;
    }
    
    protected int getBlockRemaining() {
        return this.block_remaining;
    }
    
    protected void setFramesRead(final int frames_read) {
        this.frames_read = frames_read;
    }
    
    protected void increaseFramesRead() {
        this.frames_read = this.getFramesRead() + 1;
    }
    
    protected int getFramesRead() {
        return this.frames_read;
    }
    
    protected void setIntelFileSize(final int intel_file_size) {
        this.intel_file_size = intel_file_size;
    }
    
    protected int getIntelFileSize() {
        return this.intel_file_size;
    }
    
    protected void setIntelCurrentPossition(final long intel_current_possition) {
        this.intel_current_possition = intel_current_possition;
    }
    
    protected long getIntelCurrentPossition() {
        return this.intel_current_possition;
    }
    
    protected void setIntelState(final ChmCommons.IntelState intel_state) {
        this.intel_state = intel_state;
    }
    
    protected ChmCommons.IntelState getIntelState() {
        return this.intel_state;
    }
    
    protected void setR0(final long r0) {
        this.R0 = r0;
    }
    
    protected long getR0() {
        return this.R0;
    }
    
    protected void setR1(final long r1) {
        this.R1 = r1;
    }
    
    protected long getR1() {
        return this.R1;
    }
    
    protected void setR2(final long r2) {
        this.R2 = r2;
    }
    
    protected long getR2() {
        return this.R2;
    }
    
    public static void main(final String[] args) {
    }
    
    public void setMainTreeLengtsTable(final short[] mainTreeLengtsTable) {
        this.mainTreeLengtsTable = mainTreeLengtsTable;
    }
    
    public short[] getMainTreeLengtsTable() {
        return this.mainTreeLengtsTable;
    }
    
    public void setLengthTreeLengtsTable(final short[] lengthTreeLengtsTable) {
        this.lengthTreeLengtsTable = lengthTreeLengtsTable;
    }
    
    public short[] getLengthTreeLengtsTable() {
        return this.lengthTreeLengtsTable;
    }
}
