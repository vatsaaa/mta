// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.parser.chm.lzx;

import org.apache.tika.exception.TikaException;
import org.apache.tika.parser.chm.exception.ChmParsingException;
import org.apache.tika.parser.chm.accessor.ChmLzxcControlData;
import org.apache.tika.parser.chm.accessor.DirectoryListingEntry;

public class ChmBlockInfo
{
    private int iniBlock;
    private int startBlock;
    private int endBlock;
    private int startOffset;
    private int endOffset;
    private static ChmBlockInfo chmBlockInfo;
    
    private ChmBlockInfo() {
    }
    
    protected ChmBlockInfo getChmBlockInfo(final DirectoryListingEntry dle, final int bytesPerBlock, final ChmLzxcControlData clcd, final ChmBlockInfo chmBlockInfo) throws TikaException {
        if (!this.validateParameters(dle, bytesPerBlock, clcd, chmBlockInfo)) {
            throw new ChmParsingException("Please check you parameters");
        }
        chmBlockInfo.setStartBlock(dle.getOffset() / bytesPerBlock);
        chmBlockInfo.setEndBlock((dle.getOffset() + dle.getLength()) / bytesPerBlock);
        chmBlockInfo.setStartOffset(dle.getOffset() % bytesPerBlock);
        chmBlockInfo.setEndOffset((dle.getOffset() + dle.getLength()) % bytesPerBlock);
        chmBlockInfo.setIniBlock((chmBlockInfo.startBlock - chmBlockInfo.startBlock) % (int)clcd.getResetInterval());
        return chmBlockInfo;
    }
    
    public static ChmBlockInfo getChmBlockInfoInstance(final DirectoryListingEntry dle, final int bytesPerBlock, final ChmLzxcControlData clcd) {
        setChmBlockInfo(new ChmBlockInfo());
        getChmBlockInfo().setStartBlock(dle.getOffset() / bytesPerBlock);
        getChmBlockInfo().setEndBlock((dle.getOffset() + dle.getLength()) / bytesPerBlock);
        getChmBlockInfo().setStartOffset(dle.getOffset() % bytesPerBlock);
        getChmBlockInfo().setEndOffset((dle.getOffset() + dle.getLength()) % bytesPerBlock);
        getChmBlockInfo().setIniBlock((getChmBlockInfo().startBlock - getChmBlockInfo().startBlock) % (int)clcd.getResetInterval());
        return getChmBlockInfo();
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("iniBlock:=" + this.getIniBlock() + ", ");
        sb.append("startBlock:=" + this.getStartBlock() + ", ");
        sb.append("endBlock:=" + this.getEndBlock() + ", ");
        sb.append("startOffset:=" + this.getStartOffset() + ", ");
        sb.append("endOffset:=" + this.getEndOffset() + System.getProperty("line.separator"));
        return sb.toString();
    }
    
    private boolean validateParameters(final DirectoryListingEntry dle, final int bytesPerBlock, final ChmLzxcControlData clcd, final ChmBlockInfo chmBlockInfo) {
        int goodParameter = 0;
        if (dle != null) {
            ++goodParameter;
        }
        if (bytesPerBlock > 0) {
            ++goodParameter;
        }
        if (clcd != null) {
            ++goodParameter;
        }
        if (chmBlockInfo != null) {
            ++goodParameter;
        }
        return goodParameter == 4;
    }
    
    public static void main(final String[] args) {
    }
    
    public int getIniBlock() {
        return this.iniBlock;
    }
    
    private void setIniBlock(final int iniBlock) {
        this.iniBlock = iniBlock;
    }
    
    public int getStartBlock() {
        return this.startBlock;
    }
    
    private void setStartBlock(final int startBlock) {
        this.startBlock = startBlock;
    }
    
    public int getEndBlock() {
        return this.endBlock;
    }
    
    private void setEndBlock(final int endBlock) {
        this.endBlock = endBlock;
    }
    
    public int getStartOffset() {
        return this.startOffset;
    }
    
    private void setStartOffset(final int startOffset) {
        this.startOffset = startOffset;
    }
    
    public int getEndOffset() {
        return this.endOffset;
    }
    
    private void setEndOffset(final int endOffset) {
        this.endOffset = endOffset;
    }
    
    public static void setChmBlockInfo(final ChmBlockInfo chmBlockInfo) {
        ChmBlockInfo.chmBlockInfo = chmBlockInfo;
    }
    
    public static ChmBlockInfo getChmBlockInfo() {
        return ChmBlockInfo.chmBlockInfo;
    }
    
    static {
        ChmBlockInfo.chmBlockInfo = null;
    }
}
