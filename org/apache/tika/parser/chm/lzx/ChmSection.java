// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.parser.chm.lzx;

import java.util.Arrays;
import java.math.BigInteger;
import org.apache.tika.exception.TikaException;
import org.apache.tika.parser.chm.core.ChmCommons;

public class ChmSection
{
    private byte[] data;
    private int swath;
    private int total;
    private int buffer;
    
    public ChmSection(final byte[] data) throws TikaException {
        ChmCommons.assertByteArrayNotNull(data);
        this.setData(data);
    }
    
    public byte[] reverseByteOrder(final byte[] toBeReversed) throws TikaException {
        ChmCommons.assertByteArrayNotNull(toBeReversed);
        ChmCommons.reverse(toBeReversed);
        return toBeReversed;
    }
    
    public int checkBit(final int i) {
        return ((this.getBuffer() & 1 << this.getTotal() - i) != 0x0) ? 1 : 0;
    }
    
    public int getSyncBits(final int bit) {
        return this.getDesyncBits(bit, bit);
    }
    
    public int getDesyncBits(final int bit, final int removeBit) {
        while (this.getTotal() < 16) {
            this.setBuffer((this.getBuffer() << 16) + this.unmarshalUByte() + (this.unmarshalUByte() << 8));
            this.setTotal(this.getTotal() + 16);
        }
        final int tmp = this.getBuffer() >>> this.getTotal() - bit;
        this.setTotal(this.getTotal() - removeBit);
        this.setBuffer(this.getBuffer() - (this.getBuffer() >>> this.getTotal() << this.getTotal()));
        return tmp;
    }
    
    public int unmarshalUByte() {
        return this.getByte() & 0xFF;
    }
    
    public byte getByte() {
        if (this.getSwath() < this.getData().length) {
            this.setSwath(this.getSwath() + 1);
            return this.getData()[this.getSwath() - 1];
        }
        return 0;
    }
    
    public int getLeft() {
        return this.getData().length - this.getSwath();
    }
    
    public byte[] getData() {
        return this.data;
    }
    
    public BigInteger getBigInteger(int i) {
        if (this.getData() == null) {
            return BigInteger.ZERO;
        }
        if (this.getData().length - this.getSwath() < i) {
            i = this.getData().length - this.getSwath();
        }
        final byte[] tmp = new byte[i];
        for (int j = i - 1; j >= 0; --j) {
            tmp[i - j - 1] = this.getData()[this.getSwath() + j];
        }
        this.setSwath(this.getSwath() + i);
        return new BigInteger(tmp);
    }
    
    public byte[] stringToAsciiBytes(final String s) {
        final char[] c = s.toCharArray();
        final byte[] byteval = new byte[c.length];
        for (int i = 0; i < c.length; ++i) {
            byteval[i] = (byte)c[i];
        }
        return byteval;
    }
    
    public BigInteger unmarshalUlong() {
        return this.getBigInteger(8);
    }
    
    public long unmarshalUInt() {
        return this.getBigInteger(4).longValue();
    }
    
    public int unmarshalInt() {
        return this.getBigInteger(4).intValue();
    }
    
    public byte[] unmarshalBytes(final int i) {
        if (i == 0) {
            return new byte[1];
        }
        final byte[] t = new byte[i];
        for (int j = 0; j < i; ++j) {
            t[j] = this.getData()[j + this.getSwath()];
        }
        this.setSwath(this.getSwath() + i);
        return t;
    }
    
    public BigInteger getEncint() {
        BigInteger bi = BigInteger.ZERO;
        final byte[] nb = { 0 };
        byte ob;
        while ((ob = this.getByte()) < 0) {
            nb[0] = (byte)(ob & 0x7F);
            bi = bi.shiftLeft(7).add(new BigInteger(nb));
        }
        nb[0] = (byte)(ob & 0x7F);
        bi = bi.shiftLeft(7).add(new BigInteger(nb));
        return bi;
    }
    
    public char unmarshalUtfChar() {
        int i = 1;
        final byte ob = this.getByte();
        if (ob < 0) {
            for (i = 2; ob << 24 + i < 0; ++i) {}
        }
        final byte[] ba = new byte[i];
        ba[0] = ob;
        for (int j = 1; j < i; ++j) {
            ba[j] = this.getByte();
        }
        i = ba.length;
        if (i == 1) {
            return (char)ba[0];
        }
        int n = ba[0] & 0xF;
        for (int j = 1; j < i; n = (n << 6) + (ba[j++] & 0x3F)) {}
        return (char)n;
    }
    
    private void setData(final byte[] data) {
        this.data = data;
    }
    
    public int getSwath() {
        return this.swath;
    }
    
    public void setSwath(final int swath) {
        this.swath = swath;
    }
    
    public int getTotal() {
        return this.total;
    }
    
    public void setTotal(final int total) {
        this.total = total;
    }
    
    private int getBuffer() {
        return this.buffer;
    }
    
    private void setBuffer(final int buffer) {
        this.buffer = buffer;
    }
    
    public static void main(final String[] args) throws TikaException {
        final byte[] array = { 4, 78, -67, 90, 1, -33 };
        final ChmSection chmSection = new ChmSection(array);
        System.out.println("before " + Arrays.toString(array));
        System.out.println("after " + Arrays.toString(chmSection.reverseByteOrder(array)));
    }
}
