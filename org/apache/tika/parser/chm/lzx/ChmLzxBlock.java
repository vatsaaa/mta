// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.parser.chm.lzx;

import org.apache.tika.parser.chm.core.ChmConstants;
import java.math.BigInteger;
import org.apache.tika.parser.chm.core.ChmCommons;
import org.apache.tika.parser.chm.exception.ChmParsingException;
import org.apache.tika.exception.TikaException;

public class ChmLzxBlock
{
    private int block_number;
    private long block_length;
    private ChmLzxState state;
    private byte[] content;
    private ChmSection chmSection;
    private int contentLength;
    private int previousBlockType;
    
    public ChmLzxBlock(final int blockNumber, final byte[] dataSegment, final long blockLength, final ChmLzxBlock prevBlock) {
        this.content = null;
        this.chmSection = null;
        this.contentLength = 0;
        this.previousBlockType = -1;
        try {
            if (!this.validateConstructorParams(blockNumber, dataSegment, blockLength)) {
                throw new TikaException("Check your chm lzx block parameters");
            }
            this.setBlockNumber(blockNumber);
            if (prevBlock != null && prevBlock.getState().getBlockLength() > prevBlock.getState().getBlockRemaining()) {
                this.setChmSection(new ChmSection(prevBlock.getContent()));
            }
            else {
                this.setChmSection(new ChmSection(dataSegment));
            }
            this.setBlockLength(blockLength);
            this.checkLzxBlock(prevBlock);
            this.setContent((int)blockLength);
            if (prevBlock == null || this.getContent().length < (int)this.getBlockLength()) {
                this.setContent((int)this.getBlockLength());
            }
            if (prevBlock != null && prevBlock.getState() != null) {
                this.previousBlockType = prevBlock.getState().getBlockType();
            }
            this.extractContent();
        }
        catch (Exception ex) {}
    }
    
    protected int getContentLength() {
        return this.contentLength;
    }
    
    protected void setContentLength(final int contentLength) {
        this.contentLength = contentLength;
    }
    
    private ChmSection getChmSection() {
        return this.chmSection;
    }
    
    private void setChmSection(final ChmSection chmSection) {
        this.chmSection = chmSection;
    }
    
    private void assertStateNotNull() throws TikaException {
        if (this.getState() == null) {
            throw new ChmParsingException("state is null");
        }
    }
    
    private void extractContent() throws TikaException {
        this.assertStateNotNull();
        if (this.getChmSection().getData() != null) {
            while (this.getContentLength() < this.getBlockLength()) {
                if (this.getState() != null && this.getState().getBlockRemaining() == 0) {
                    if (this.getState().getHadStarted() == ChmCommons.LzxState.NOT_STARTED_DECODING) {
                        this.getState().setHadStarted(ChmCommons.LzxState.STARTED_DECODING);
                        if (this.getChmSection().getSyncBits(1) == 1) {
                            final int intelSizeTemp = (this.getChmSection().getSyncBits(16) << 16) + this.getChmSection().getSyncBits(16);
                            if (intelSizeTemp >= 0) {
                                this.getState().setIntelFileSize(intelSizeTemp);
                            }
                            else {
                                this.getState().setIntelFileSize(0);
                            }
                        }
                    }
                    this.getState().setBlockType(this.getChmSection().getSyncBits(3));
                    this.getState().setBlockLength((this.getChmSection().getSyncBits(16) << 8) + this.getChmSection().getSyncBits(8));
                    this.getState().setBlockRemaining(this.getState().getBlockLength());
                    if (this.getState().getBlockType() > 3 && this.previousBlockType >= 0 && this.previousBlockType < 3) {
                        this.getState().setBlockType(this.previousBlockType);
                    }
                    switch (this.getState().getBlockType()) {
                        case 2: {
                            this.createAlignedTreeTable();
                        }
                        case 1: {
                            this.createMainTreeTable();
                            this.createLengthTreeTable();
                            if (this.getState().getMainTreeLengtsTable()[232] != 0) {
                                this.getState().setIntelState(ChmCommons.IntelState.STARTED);
                                break;
                            }
                            break;
                        }
                        case 3: {
                            this.getState().setIntelState(ChmCommons.IntelState.STARTED);
                            if (this.getChmSection().getTotal() > 16) {
                                this.getChmSection().setSwath(this.getChmSection().getSwath() - 1);
                            }
                            this.getState().setR0(new BigInteger(this.getChmSection().reverseByteOrder(this.getChmSection().unmarshalBytes(4))).longValue());
                            this.getState().setR1(new BigInteger(this.getChmSection().reverseByteOrder(this.getChmSection().unmarshalBytes(4))).longValue());
                            this.getState().setR2(new BigInteger(this.getChmSection().reverseByteOrder(this.getChmSection().unmarshalBytes(4))).longValue());
                            break;
                        }
                    }
                }
                int tempLen;
                if (this.getContentLength() + this.getState().getBlockRemaining() > this.getBlockLength()) {
                    this.getState().setBlockRemaining(this.getContentLength() + this.getState().getBlockRemaining() - (int)this.getBlockLength());
                    tempLen = (int)this.getBlockLength();
                }
                else {
                    tempLen = this.getContentLength() + this.getState().getBlockRemaining();
                    this.getState().setBlockRemaining(0);
                }
                switch (this.getState().getBlockType()) {
                    case 2: {
                        this.decompressAlignedBlock(tempLen, this.getChmSection().getData());
                        break;
                    }
                    case 1: {
                        this.decompressVerbatimBlock(tempLen, this.getChmSection().getData());
                        break;
                    }
                    case 3: {
                        this.decompressUncompressedBlock(tempLen, this.getChmSection().getData());
                        break;
                    }
                }
                this.getState().increaseFramesRead();
                if (this.getState().getFramesRead() < 32768 && this.getState().getIntelFileSize() != 0) {
                    this.intelE8Decoding();
                }
            }
        }
    }
    
    protected void intelE8Decoding() {
        if (this.getBlockLength() <= 6L || this.getState().getIntelState() == ChmCommons.IntelState.NOT_STARTED) {
            this.getState().setBlockRemaining(this.getState().getBlockRemaining() - (int)this.getBlockLength());
        }
        else {
            long curpos = this.getState().getBlockRemaining();
            this.getState().setBlockRemaining(this.getState().getBlockRemaining() - (int)this.getBlockLength());
            int i = 0;
            while (i < this.getBlockLength() - 10L) {
                if (this.content[i] != 232) {
                    ++i;
                }
                else {
                    final byte[] b = { this.getContent()[i + 3], this.getContent()[i + 2], this.getContent()[i + 1], this.getContent()[i + 0] };
                    final long absoff = new BigInteger(b).longValue();
                    if (absoff >= -curpos && absoff < this.getState().getIntelFileSize()) {
                        final long reloff = (absoff >= 0L) ? (absoff - curpos) : (absoff + this.getState().getIntelFileSize());
                        this.getContent()[i + 0] = (byte)reloff;
                        this.getContent()[i + 1] = (byte)(reloff >>> 8);
                        this.getContent()[i + 2] = (byte)(reloff >>> 16);
                        this.getContent()[i + 3] = (byte)(reloff >>> 24);
                    }
                    i += 4;
                    curpos += 5L;
                }
            }
        }
    }
    
    private short[] createPreLenTable() {
        final short[] tmp = new short[20];
        for (int i = 0; i < 20; ++i) {
            tmp[i] = (short)this.getChmSection().getSyncBits(4);
        }
        return tmp;
    }
    
    private void createLengthTreeTable() throws TikaException {
        final short[] prelentable = this.createPreLenTable();
        if (prelentable == null) {
            throw new ChmParsingException("pretreetable is null");
        }
        final short[] pretreetable = this.createTreeTable2(prelentable, 104, 6, 20);
        if (pretreetable == null) {
            throw new ChmParsingException("pretreetable is null");
        }
        this.createLengthTreeLenTable(0, 249, pretreetable, prelentable);
        this.getState().setLengthTreeTable(this.createTreeTable2(this.getState().getLengthTreeLengtsTable(), 4596, 12, 249));
    }
    
    public void decompressUncompressedBlock(final int len, final byte[] prevcontent) {
        if (this.getContentLength() + this.getState().getBlockRemaining() <= this.getBlockLength()) {
            for (int i = this.getContentLength(); i < this.getContentLength() + this.getState().getBlockRemaining(); ++i) {
                this.content[i] = this.getChmSection().getByte();
            }
            this.setContentLength(this.getContentLength() + this.getState().getBlockRemaining());
            this.getState().setBlockRemaining(0);
        }
        else {
            for (int i = this.getContentLength(); i < this.getBlockLength(); ++i) {
                this.content[i] = this.getChmSection().getByte();
            }
            this.getState().setBlockRemaining((int)this.getBlockLength() - this.getContentLength());
            this.setContentLength((int)this.getBlockLength());
        }
    }
    
    public void decompressAlignedBlock(final int len, final byte[] prevcontent) throws TikaException {
        if (this.getChmSection() == null || this.getState() == null || this.getState().getMainTreeTable() == null) {
            throw new ChmParsingException("chm section is null");
        }
        int matchlen = 0;
        int matchfooter = 0;
        int matchoffset = 0;
        for (int i = this.getContentLength(); i < len; ++i) {
            final int border = this.getChmSection().getDesyncBits(12, 0);
            if (border >= this.getState().mainTreeTable.length) {
                break;
            }
            short s = this.getState().mainTreeTable[this.getChmSection().getDesyncBits(12, 0)];
            if (s >= this.getState().getMainTreeElements()) {
                int x = 12;
                do {
                    ++x;
                    s <<= 1;
                    s += (short)this.getChmSection().checkBit(x);
                } while ((s = this.getState().mainTreeTable[s]) >= this.getState().getMainTreeElements());
            }
            this.getChmSection().getSyncBits(this.getState().mainTreeTable[s]);
            if (s < 256) {
                this.content[i] = (byte)s;
            }
            else {
                s -= 256;
                matchlen = (s & 0x7);
                if (matchlen == 7) {
                    matchfooter = this.getState().lengthTreeTable[this.getChmSection().getDesyncBits(12, 0)];
                    if (matchfooter >= 12) {
                        int x = 12;
                        do {
                            ++x;
                            matchfooter <<= 1;
                            matchfooter += this.getChmSection().checkBit(x);
                        } while ((matchfooter = this.getState().lengthTreeTable[matchfooter]) >= 249);
                    }
                    this.getChmSection().getSyncBits(this.getState().lengthTreeLengtsTable[matchfooter]);
                    matchlen += matchfooter;
                }
                matchlen += 2;
                matchoffset = s >>> 3;
                if (matchoffset > 2) {
                    int extra = ChmConstants.EXTRA_BITS[matchoffset];
                    matchoffset = ChmConstants.POSITION_BASE[matchoffset] - 2;
                    if (extra > 3) {
                        extra -= 3;
                        final long l = this.getChmSection().getSyncBits(extra);
                        matchoffset += (int)(l << 3);
                        final int g = this.getChmSection().getDesyncBits(7, 0);
                        int t = this.getState().getAlignedTreeTable()[g];
                        if (t >= this.getState().getMainTreeElements()) {
                            int x = 12;
                            do {
                                ++x;
                                t <<= 1;
                                t += this.getChmSection().checkBit(x);
                            } while ((t = this.getState().getAlignedTreeTable()[t]) >= this.getState().getMainTreeElements());
                        }
                        this.getChmSection().getSyncBits(this.getState().getAlignedTreeTable()[t]);
                        matchoffset += t;
                    }
                    else if (extra == 3) {
                        final int g2 = this.getChmSection().getDesyncBits(7, 0);
                        int t2 = this.getState().getAlignedTreeTable()[g2];
                        if (t2 >= this.getState().getMainTreeElements()) {
                            int x = 12;
                            do {
                                ++x;
                                t2 <<= 1;
                                t2 += this.getChmSection().checkBit(x);
                            } while ((t2 = this.getState().getAlignedTreeTable()[t2]) >= this.getState().getMainTreeElements());
                        }
                        this.getChmSection().getSyncBits(this.getState().getAlignedTreeTable()[t2]);
                        matchoffset += t2;
                    }
                    else if (extra > 0) {
                        final long l = this.getChmSection().getSyncBits(extra);
                        matchoffset += (int)l;
                    }
                    else {
                        matchoffset = 1;
                    }
                    this.getState().setR2(this.getState().getR1());
                    this.getState().setR1(this.getState().getR0());
                    this.getState().setR0(matchoffset);
                }
                else if (matchoffset == 0) {
                    matchoffset = (int)this.getState().getR0();
                }
                else if (matchoffset == 1) {
                    matchoffset = (int)this.getState().getR1();
                    this.getState().setR1(this.getState().getR0());
                    this.getState().setR0(matchoffset);
                }
                else {
                    matchoffset = (int)this.getState().getR2();
                    this.getState().setR2(this.getState().getR0());
                    this.getState().setR0(matchoffset);
                }
                int rundest = i;
                int runsrc = rundest - matchoffset;
                i += matchlen - 1;
                if (i > len) {
                    break;
                }
                if (runsrc < 0) {
                    if (matchlen + runsrc <= 0) {
                        runsrc += prevcontent.length;
                        while (matchlen-- > 0) {
                            this.content[rundest++] = prevcontent[runsrc++];
                        }
                    }
                    else {
                        for (runsrc += prevcontent.length; runsrc < prevcontent.length; this.content[rundest++] = prevcontent[runsrc++]) {}
                        matchlen = matchlen + runsrc - prevcontent.length;
                        runsrc = 0;
                        while (matchlen-- > 0) {
                            this.content[rundest++] = this.content[runsrc++];
                        }
                    }
                }
                else {
                    while (runsrc < 0 && matchlen-- > 0) {
                        this.content[rundest++] = this.content[(int)(runsrc + this.getBlockLength())];
                        ++runsrc;
                    }
                    while (matchlen-- > 0) {
                        this.content[rundest++] = this.content[runsrc++];
                    }
                }
            }
        }
        this.setContentLength(len);
    }
    
    private void assertShortArrayNotNull(final short[] array) throws TikaException {
        if (array == null) {
            throw new ChmParsingException("short[] is null");
        }
    }
    
    private void decompressVerbatimBlock(final int len, final byte[] prevcontent) throws TikaException {
        int matchlen = 0;
        int matchfooter = 0;
        int matchoffset = 0;
        for (int i = this.getContentLength(); i < len; ++i) {
            final int f = this.getChmSection().getDesyncBits(12, 0);
            this.assertShortArrayNotNull(this.getState().getMainTreeTable());
            short s = this.getState().getMainTreeTable()[f];
            if (s >= 512) {
                int x = 12;
                do {
                    ++x;
                    s <<= 1;
                    s += (short)this.getChmSection().checkBit(x);
                } while ((s = this.getState().getMainTreeTable()[s]) >= 512);
            }
            this.getChmSection().getSyncBits(this.getState().getMainTreeLengtsTable()[s]);
            if (s < 256) {
                this.content[i] = (byte)s;
            }
            else {
                s -= 256;
                matchlen = (s & 0x7);
                if (matchlen == 7) {
                    matchfooter = this.getState().getLengthTreeTable()[this.getChmSection().getDesyncBits(12, 0)];
                    if (matchfooter >= 249) {
                        int x = 12;
                        do {
                            ++x;
                            matchfooter <<= 1;
                            matchfooter += this.getChmSection().checkBit(x);
                        } while ((matchfooter = this.getState().getLengthTreeTable()[matchfooter]) >= 249);
                    }
                    this.getChmSection().getSyncBits(this.getState().getLengthTreeLengtsTable()[matchfooter]);
                    matchlen += matchfooter;
                }
                matchlen += 2;
                matchoffset = s >>> 3;
                if (matchoffset > 2) {
                    if (matchoffset != 3) {
                        final int extra = ChmConstants.EXTRA_BITS[matchoffset];
                        final long l = this.getChmSection().getSyncBits(extra);
                        matchoffset = (int)(ChmConstants.POSITION_BASE[matchoffset] - 2 + l);
                    }
                    else {
                        matchoffset = 1;
                    }
                    this.getState().setR2(this.getState().getR1());
                    this.getState().setR1(this.getState().getR0());
                    this.getState().setR0(matchoffset);
                }
                else if (matchoffset == 0) {
                    matchoffset = (int)this.getState().getR0();
                }
                else if (matchoffset == 1) {
                    matchoffset = (int)this.getState().getR1();
                    this.getState().setR1(this.getState().getR0());
                    this.getState().setR0(matchoffset);
                }
                else {
                    matchoffset = (int)this.getState().getR2();
                    this.getState().setR2(this.getState().getR0());
                    this.getState().setR0(matchoffset);
                }
                int rundest = i;
                int runsrc = rundest - matchoffset;
                i += matchlen - 1;
                if (i > len) {
                    break;
                }
                if (runsrc < 0) {
                    if (matchlen + runsrc <= 0) {
                        for (runsrc += prevcontent.length; matchlen-- > 0 && prevcontent != null && runsrc + 1 > 0; this.content[rundest++] = prevcontent[runsrc++]) {
                            if (rundest < this.content.length && runsrc < this.content.length) {}
                        }
                    }
                    else {
                        for (runsrc += prevcontent.length; runsrc < prevcontent.length; this.content[rundest++] = prevcontent[runsrc++]) {
                            if (rundest < this.content.length && runsrc < this.content.length) {}
                        }
                        matchlen = matchlen + runsrc - prevcontent.length;
                        runsrc = 0;
                        while (matchlen-- > 0) {
                            this.content[rundest++] = this.content[runsrc++];
                        }
                    }
                }
                else {
                    while (runsrc < 0 && matchlen-- > 0) {
                        this.content[rundest++] = this.content[(int)(runsrc + this.getBlockLength())];
                        ++runsrc;
                    }
                    while (matchlen-- > 0) {
                        if (rundest < this.content.length && runsrc < this.content.length) {
                            this.content[rundest++] = this.content[runsrc++];
                        }
                    }
                }
            }
        }
        this.setContentLength(len);
    }
    
    private void createLengthTreeLenTable(final int offset, final int tablelen, final short[] pretreetable, final short[] prelentable) throws TikaException {
        if (prelentable == null || this.getChmSection() == null || pretreetable == null || prelentable == null) {
            throw new ChmParsingException("is null");
        }
        int i = offset;
        while (i < tablelen) {
            int z = pretreetable[this.getChmSection().getDesyncBits(6, 0)];
            if (z >= 20) {
                int x = 6;
                do {
                    ++x;
                    z <<= 1;
                    z += this.getChmSection().checkBit(x);
                } while ((z = pretreetable[z]) >= 20);
            }
            this.getChmSection().getSyncBits(prelentable[z]);
            if (z < 17) {
                z = this.getState().getLengthTreeLengtsTable()[i] - z;
                if (z < 0) {
                    z += 17;
                }
                this.getState().getLengthTreeLengtsTable()[i] = (short)z;
                ++i;
            }
            else if (z == 17) {
                int y = this.getChmSection().getSyncBits(4);
                y += 4;
                for (int j = 0; j < y; ++j) {
                    if (i < this.getState().getLengthTreeLengtsTable().length) {
                        this.getState().getLengthTreeLengtsTable()[i++] = 0;
                    }
                }
            }
            else if (z == 18) {
                int y = this.getChmSection().getSyncBits(5);
                y += 20;
                for (int j = 0; j < y; ++j) {
                    if (i < this.getState().getLengthTreeLengtsTable().length) {
                        this.getState().getLengthTreeLengtsTable()[i++] = 0;
                    }
                }
            }
            else {
                if (z != 19) {
                    continue;
                }
                int y = this.getChmSection().getSyncBits(1);
                y += 4;
                z = pretreetable[this.getChmSection().getDesyncBits(6, 0)];
                if (z >= 20) {
                    int x = 6;
                    do {
                        ++x;
                        z <<= 1;
                        z += this.getChmSection().checkBit(x);
                    } while ((z = pretreetable[z]) >= 12);
                }
                this.getChmSection().getSyncBits(prelentable[z]);
                z = this.getState().getLengthTreeLengtsTable()[i] - z;
                if (z < 0) {
                    z += 17;
                }
                for (int j = 0; j < y; ++j) {
                    this.getState().getLengthTreeLengtsTable()[i++] = (short)z;
                }
            }
        }
    }
    
    private void createMainTreeTable() throws TikaException {
        short[] prelentable = this.createPreLenTable();
        short[] pretreetable = this.createTreeTable2(prelentable, 104, 6, 20);
        this.createMainTreeLenTable(0, 256, pretreetable, prelentable);
        prelentable = this.createPreLenTable();
        pretreetable = this.createTreeTable2(prelentable, 104, 6, 20);
        this.createMainTreeLenTable(256, this.getState().mainTreeLengtsTable.length, pretreetable, prelentable);
        this.getState().setMainTreeTable(this.createTreeTable2(this.getState().mainTreeLengtsTable, 5408, 12, this.getState().getMainTreeElements()));
    }
    
    private void createMainTreeLenTable(final int offset, final int tablelen, final short[] pretreetable, final short[] prelentable) throws TikaException {
        if (pretreetable == null) {
            throw new ChmParsingException("pretreetable is null");
        }
        int i = offset;
        while (i < tablelen) {
            final int f = this.getChmSection().getDesyncBits(6, 0);
            int z = pretreetable[f];
            if (z >= 20) {
                int x = 6;
                do {
                    ++x;
                    z <<= 1;
                    z += this.getChmSection().checkBit(x);
                } while ((z = pretreetable[z]) >= 20);
            }
            this.getChmSection().getSyncBits(prelentable[z]);
            if (z < 17) {
                z = this.getState().getMainTreeLengtsTable()[i] - z;
                if (z < 0) {
                    z += 17;
                }
                this.getState().mainTreeLengtsTable[i] = (short)z;
                ++i;
            }
            else if (z == 17) {
                int y = this.getChmSection().getSyncBits(4);
                y += 4;
                for (int j = 0; j < y; ++j) {
                    this.assertInRange(this.getState().getMainTreeLengtsTable(), i);
                    this.getState().mainTreeLengtsTable[i++] = 0;
                }
            }
            else if (z == 18) {
                int y = this.getChmSection().getSyncBits(5);
                y += 20;
                for (int j = 0; j < y; ++j) {
                    this.assertInRange(this.getState().getMainTreeLengtsTable(), i);
                    this.getState().mainTreeLengtsTable[i++] = 0;
                }
            }
            else {
                if (z != 19) {
                    continue;
                }
                int y = this.getChmSection().getSyncBits(1);
                y += 4;
                z = pretreetable[this.getChmSection().getDesyncBits(6, 0)];
                if (z >= 20) {
                    int x = 6;
                    do {
                        ++x;
                        z <<= 1;
                        z += this.getChmSection().checkBit(x);
                    } while ((z = pretreetable[z]) >= 20);
                }
                this.getChmSection().getSyncBits(prelentable[z]);
                z = this.getState().mainTreeLengtsTable[i] - z;
                if (z < 0) {
                    z += 17;
                }
                for (int j = 0; j < y; ++j) {
                    if (i < this.getState().getMainTreeLengtsTable().length) {
                        this.getState().mainTreeLengtsTable[i++] = (short)z;
                    }
                }
            }
        }
    }
    
    private void assertInRange(final short[] array, final int index) throws ChmParsingException {
        if (index >= array.length) {
            throw new ChmParsingException(index + " is bigger than " + array.length);
        }
    }
    
    private short[] createAlignedLenTable() {
        final int tablelen = 3;
        final int bits = 3;
        final short[] tmp = new short[tablelen];
        for (int i = 0; i < tablelen; ++i) {
            tmp[i] = (short)this.getChmSection().getSyncBits(bits);
        }
        return tmp;
    }
    
    private void createAlignedTreeTable() {
        this.getState().setAlignedLenTable(this.createAlignedLenTable());
        this.getState().setAlignedLenTable(this.createTreeTable2(this.getState().getAlignedLenTable(), 144, 7, 8));
    }
    
    private short[] createTreeTable2(final short[] lentable, final int tablelen, final int bits, final int maxsymbol) {
        final short[] tmp = new short[tablelen];
        int bit_num = 1;
        int pos = 0;
        long table_mask = 1 << bits;
        long next_symbol;
        long bit_mask = next_symbol = table_mask >> 1;
        while (bit_num <= bits) {
            for (short sym = 0; sym < maxsymbol; ++sym) {
                if (lentable.length > sym && lentable[sym] == bit_num) {
                    int leaf = pos;
                    if ((pos += (int)bit_mask) > table_mask) {
                        return null;
                    }
                    long fill = bit_mask;
                    while (fill-- > 0L) {
                        tmp[leaf++] = sym;
                    }
                }
            }
            bit_mask >>= 1;
            ++bit_num;
        }
        if (pos != table_mask) {
            for (int leaf = pos; leaf < table_mask; ++leaf) {
                tmp[leaf] = 0;
            }
            pos <<= 16;
            table_mask <<= 16;
            bit_mask = 32768L;
            while (bit_num <= 16) {
                for (short sym = 0; sym < maxsymbol; ++sym) {
                    if (lentable.length > sym && lentable[sym] == bit_num) {
                        int leaf = pos >> 16;
                        for (long fill = 0L; fill < bit_num - bits; ++fill) {
                            if (tmp[leaf] == 0 && (next_symbol << 1) + 1L < tmp.length) {
                                tmp[(int)(next_symbol << 1)] = 0;
                                tmp[(int)(next_symbol << 1) + 1] = 0;
                                tmp[leaf] = (short)(next_symbol++);
                            }
                            leaf = tmp[leaf] << 1;
                            if ((pos >> (int)(15L - fill) & 0x1) != 0x0) {
                                ++leaf;
                            }
                        }
                        tmp[leaf] = sym;
                        if ((pos += (int)bit_mask) > table_mask) {
                            return null;
                        }
                    }
                }
                bit_mask >>= 1;
                ++bit_num;
            }
        }
        if (pos == table_mask) {
            return tmp;
        }
        return tmp;
    }
    
    public byte[] getContent() {
        return this.content;
    }
    
    public byte[] getContent(final int startOffset, final int endOffset) {
        final int length = endOffset - startOffset;
        return (this.getContent() != null) ? ChmCommons.copyOfRange(this.getContent(), startOffset, startOffset + length) : new byte[1];
    }
    
    public byte[] getContent(final int start) {
        return (this.getContent() != null) ? ChmCommons.copyOfRange(this.getContent(), start, this.getContent().length + start) : new byte[1];
    }
    
    private void setContent(final int contentLength) {
        this.content = new byte[contentLength];
    }
    
    private void checkLzxBlock(final ChmLzxBlock chmPrevLzxBlock) throws TikaException {
        if (chmPrevLzxBlock == null && this.getBlockLength() < 2147483647L) {
            this.setState(new ChmLzxState((int)this.getBlockLength()));
        }
        else {
            this.setState(chmPrevLzxBlock.getState());
        }
    }
    
    private boolean validateConstructorParams(final int blockNumber, final byte[] dataSegment, final long blockLength) throws TikaException {
        int goodParameter = 0;
        if (blockNumber < 0) {
            throw new ChmParsingException("block number should be possitive");
        }
        ++goodParameter;
        if (dataSegment == null || dataSegment.length <= 0) {
            throw new ChmParsingException("data segment should not be null");
        }
        ++goodParameter;
        if (blockLength > 0L) {
            ++goodParameter;
            return goodParameter == 3;
        }
        throw new ChmParsingException("block length should be more than zero");
    }
    
    public int getBlockNumber() {
        return this.block_number;
    }
    
    private void setBlockNumber(final int block_number) {
        this.block_number = block_number;
    }
    
    private long getBlockLength() {
        return this.block_length;
    }
    
    private void setBlockLength(final long block_length) {
        this.block_length = block_length;
    }
    
    public ChmLzxState getState() {
        return this.state;
    }
    
    private void setState(final ChmLzxState state) {
        this.state = state;
    }
    
    public static void main(final String[] args) {
    }
}
