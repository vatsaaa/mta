// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.parser.chm.core;

import java.util.Iterator;
import java.util.List;
import org.apache.tika.parser.chm.exception.ChmParsingException;
import java.io.IOException;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import org.apache.tika.parser.chm.accessor.DirectoryListingEntry;
import org.apache.tika.parser.chm.assertion.ChmAssert;
import org.apache.tika.parser.chm.accessor.ChmLzxcResetTable;
import org.apache.tika.exception.TikaException;

public class ChmCommons
{
    public static final int UNDEFINED = 0;
    public static final int VERBATIM = 1;
    public static final int ALIGNED_OFFSET = 2;
    public static final int UNCOMPRESSED = 3;
    
    private ChmCommons() {
    }
    
    public static void assertByteArrayNotNull(final byte[] data) throws TikaException {
        if (data == null) {
            throw new TikaException("byte[] is null");
        }
    }
    
    public static int getWindowSize(int window) {
        int win;
        for (win = 0; window > 1; window >>>= 1, ++win) {}
        return win;
    }
    
    public static byte[] getChmBlockSegment(final byte[] data, final ChmLzxcResetTable resetTable, final int blockNumber, final int lzxcBlockOffset, final int lzxcBlockLength) throws TikaException {
        ChmAssert.assertChmBlockSegment(data, resetTable, blockNumber, lzxcBlockOffset, lzxcBlockLength);
        int blockLength = -1;
        if (blockNumber < resetTable.getBlockAddress().length - 1) {
            blockLength = (int)(resetTable.getBlockAddress()[blockNumber + 1] - resetTable.getBlockAddress()[blockNumber]);
        }
        else if (blockNumber >= resetTable.getBlockAddress().length) {
            blockLength = 0;
        }
        else {
            blockLength = (int)(lzxcBlockLength - resetTable.getBlockAddress()[blockNumber]);
        }
        final byte[] t = copyOfRange(data, (int)(lzxcBlockOffset + resetTable.getBlockAddress()[blockNumber]), (int)(lzxcBlockOffset + resetTable.getBlockAddress()[blockNumber] + blockLength));
        return (t != null) ? t : new byte[1];
    }
    
    public static String getLanguage(final long langID) {
        switch ((int)langID) {
            case 1025: {
                return "Arabic";
            }
            case 1069: {
                return "Basque";
            }
            case 1027: {
                return "Catalan";
            }
            case 2052: {
                return "Chinese (Simplified)";
            }
            case 1028: {
                return "Chinese (Traditional)";
            }
            case 1029: {
                return "Czech";
            }
            case 1030: {
                return "Danish";
            }
            case 1043: {
                return "Dutch";
            }
            case 1033: {
                return "English (United States)";
            }
            case 1035: {
                return "Finnish";
            }
            case 1036: {
                return "French";
            }
            case 1031: {
                return "German";
            }
            case 1032: {
                return "Greek";
            }
            case 1037: {
                return "Hebrew";
            }
            case 1038: {
                return "Hungarian";
            }
            case 1040: {
                return "Italian";
            }
            case 1041: {
                return "Japanese";
            }
            case 1042: {
                return "Korean";
            }
            case 1044: {
                return "Norwegian";
            }
            case 1045: {
                return "Polish";
            }
            case 2070: {
                return "Portuguese";
            }
            case 1046: {
                return "Portuguese (Brazil)";
            }
            case 1049: {
                return "Russian";
            }
            case 1051: {
                return "Slovakian";
            }
            case 1060: {
                return "Slovenian";
            }
            case 3082: {
                return "Spanish";
            }
            case 1053: {
                return "Swedish";
            }
            case 1055: {
                return "Turkish";
            }
            default: {
                return "unknown - http://msdn.microsoft.com/en-us/library/bb165625%28VS.80%29.aspx";
            }
        }
    }
    
    public static boolean hasSkip(final DirectoryListingEntry directoryListingEntry) {
        return directoryListingEntry.getName().startsWith("/$") || directoryListingEntry.getName().startsWith("/#") || directoryListingEntry.getName().startsWith("::");
    }
    
    public static void writeFile(final byte[][] buffer, final String fileToBeSaved) throws TikaException {
        FileOutputStream output = null;
        if (buffer != null && fileToBeSaved != null && !isEmpty(fileToBeSaved)) {
            try {
                output = new FileOutputStream(fileToBeSaved);
                if (output != null) {
                    for (int i = 0; i < buffer.length; ++i) {
                        output.write(buffer[i]);
                    }
                }
            }
            catch (FileNotFoundException e) {
                throw new TikaException(e.getMessage());
            }
            catch (IOException e2) {
                e2.printStackTrace();
            }
            finally {
                if (output != null) {
                    try {
                        output.flush();
                        output.close();
                    }
                    catch (IOException e3) {
                        e3.printStackTrace();
                    }
                }
            }
        }
    }
    
    public static void reverse(final byte[] array) {
        if (array == null) {
            return;
        }
        for (int i = 0, j = array.length - 1; j > i; --j, ++i) {
            final byte tmp = array[j];
            array[j] = array[i];
            array[i] = tmp;
        }
    }
    
    public static final int indexOfResetTableBlock(final byte[] text, final byte[] pattern) throws ChmParsingException {
        return indexOf(text, pattern) - 4;
    }
    
    public static int indexOf(final byte[] text, final byte[] pattern) throws ChmParsingException {
        int[] next = null;
        int i = 0;
        int j = -1;
        if (pattern == null || text == null) {
            throw new ChmParsingException("pattern and/or text should not be null");
        }
        next = new int[pattern.length];
        next[0] = -1;
        while (i < pattern.length - 1) {
            if (j == -1 || pattern[i] == pattern[j]) {
                ++i;
                ++j;
                if (pattern[i] != pattern[j]) {
                    next[i] = j;
                }
                else {
                    next[i] = next[j];
                }
            }
            else {
                j = next[j];
            }
        }
        j = (i = 0);
        while (i < text.length && j < pattern.length) {
            if (j == -1 || pattern[j] == text[i]) {
                ++i;
                ++j;
            }
            else {
                j = next[j];
            }
        }
        if (j == pattern.length) {
            return i - j;
        }
        return -1;
    }
    
    public static int indexOf(final List<DirectoryListingEntry> list, final String pattern) {
        int place = 0;
        for (final DirectoryListingEntry directoryListingEntry : list) {
            if (directoryListingEntry.toString().contains(pattern)) {
                return place;
            }
            ++place;
        }
        return -1;
    }
    
    public static byte[] copyOfRange(final byte[] original, final int from, final int to) {
        checkCopyOfRangeParams(original, from, to);
        final int newLength = to - from;
        if (newLength < 0) {
            throw new IllegalArgumentException(from + " > " + to);
        }
        final byte[] copy = new byte[newLength];
        System.arraycopy(original, from, copy, 0, Math.min(original.length - from, newLength));
        return copy;
    }
    
    private static void checkCopyOfRangeParams(final byte[] original, final int from, final int to) {
        if (original == null) {
            throw new NullPointerException("array is null");
        }
        if (from < 0) {
            throw new IllegalArgumentException(from + " should be > 0");
        }
        if (to < 0) {
            throw new IllegalArgumentException(to + " should be > 0");
        }
    }
    
    public static boolean isEmpty(final String str) {
        return str == null || str.length() == 0;
    }
    
    public static void main(final String[] args) {
    }
    
    public enum EntryType
    {
        UNCOMPRESSED, 
        COMPRESSED;
    }
    
    public enum LzxState
    {
        STARTED_DECODING, 
        NOT_STARTED_DECODING;
    }
    
    public enum IntelState
    {
        STARTED, 
        NOT_STARTED;
    }
}
