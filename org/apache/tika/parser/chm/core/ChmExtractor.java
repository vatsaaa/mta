// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.parser.chm.core;

import org.apache.tika.parser.chm.lzx.ChmBlockInfo;
import java.util.Iterator;
import org.apache.tika.exception.TikaException;
import java.io.IOException;
import java.util.ArrayList;
import org.apache.tika.parser.chm.accessor.DirectoryListingEntry;
import org.apache.tika.io.IOUtils;
import org.apache.tika.parser.chm.assertion.ChmAssert;
import java.io.InputStream;
import org.apache.tika.parser.chm.accessor.ChmLzxcControlData;
import org.apache.tika.parser.chm.accessor.ChmLzxcResetTable;
import org.apache.tika.parser.chm.accessor.ChmItspHeader;
import org.apache.tika.parser.chm.accessor.ChmItsfHeader;
import org.apache.tika.parser.chm.accessor.ChmDirectoryListingSet;
import org.apache.tika.parser.chm.lzx.ChmLzxBlock;
import java.util.List;

public class ChmExtractor
{
    private List<ChmLzxBlock> lzxBlocksCache;
    private ChmDirectoryListingSet chmDirList;
    private ChmItsfHeader chmItsfHeader;
    private ChmItspHeader chmItspHeader;
    private ChmLzxcResetTable chmLzxcResetTable;
    private ChmLzxcControlData chmLzxcControlData;
    private byte[] data;
    private int indexOfContent;
    private long lzxBlockOffset;
    private long lzxBlockLength;
    
    private ChmLzxcControlData getChmLzxcControlData() {
        return this.chmLzxcControlData;
    }
    
    private void setChmLzxcControlData(final ChmLzxcControlData chmLzxcControlData) {
        this.chmLzxcControlData = chmLzxcControlData;
    }
    
    private ChmItspHeader getChmItspHeader() {
        return this.chmItspHeader;
    }
    
    private void setChmItspHeader(final ChmItspHeader chmItspHeader) {
        this.chmItspHeader = chmItspHeader;
    }
    
    private ChmLzxcResetTable getChmLzxcResetTable() {
        return this.chmLzxcResetTable;
    }
    
    private void setChmLzxcResetTable(final ChmLzxcResetTable chmLzxcResetTable) {
        this.chmLzxcResetTable = chmLzxcResetTable;
    }
    
    private long getLzxBlockLength() {
        return this.lzxBlockLength;
    }
    
    private void setLzxBlockLength(final long lzxBlockLength) {
        this.lzxBlockLength = lzxBlockLength;
    }
    
    private long getLzxBlockOffset() {
        return this.lzxBlockOffset;
    }
    
    private void setLzxBlockOffset(final long lzxBlockOffset) {
        this.lzxBlockOffset = lzxBlockOffset;
    }
    
    private int getIndexOfContent() {
        return this.indexOfContent;
    }
    
    private void setIndexOfContent(final int indexOfContent) {
        this.indexOfContent = indexOfContent;
    }
    
    private byte[] getData() {
        return this.data;
    }
    
    private void setData(final byte[] data) {
        this.data = data;
    }
    
    public ChmExtractor(final InputStream is) throws TikaException, IOException {
        this.lzxBlocksCache = null;
        this.chmDirList = null;
        this.chmItsfHeader = null;
        this.chmItspHeader = null;
        this.chmLzxcResetTable = null;
        this.chmLzxcControlData = null;
        this.data = null;
        ChmAssert.assertInputStreamNotNull(is);
        try {
            this.setData(IOUtils.toByteArray(is));
            this.setChmItsfHeader(new ChmItsfHeader());
            this.getChmItsfHeader().parse(ChmCommons.copyOfRange(this.getData(), 0, 95), this.getChmItsfHeader());
            this.setChmItspHeader(new ChmItspHeader());
            this.getChmItspHeader().parse(ChmCommons.copyOfRange(this.getData(), (int)this.getChmItsfHeader().getDirOffset(), (int)this.getChmItsfHeader().getDirOffset() + 84), this.getChmItspHeader());
            this.setChmDirList(new ChmDirectoryListingSet(this.getData(), this.getChmItsfHeader(), this.getChmItspHeader()));
            final int indexOfControlData = this.getChmDirList().getControlDataIndex();
            final int indexOfResetData = ChmCommons.indexOfResetTableBlock(this.getData(), "LZXC".getBytes());
            byte[] dir_chunk = null;
            if (indexOfResetData > 0) {
                dir_chunk = ChmCommons.copyOfRange(this.getData(), indexOfResetData, indexOfResetData + this.getChmDirList().getDirectoryListingEntryList().get(indexOfControlData).getLength());
            }
            this.setChmLzxcControlData(new ChmLzxcControlData());
            this.getChmLzxcControlData().parse(dir_chunk, this.getChmLzxcControlData());
            final int indexOfResetTable = this.getChmDirList().getResetTableIndex();
            this.setChmLzxcResetTable(new ChmLzxcResetTable());
            final int startIndex = (int)this.getChmDirList().getDataOffset() + this.getChmDirList().getDirectoryListingEntryList().get(indexOfResetTable).getOffset();
            ChmAssert.assertCopyingDataIndex(startIndex, this.getData().length);
            dir_chunk = ChmCommons.copyOfRange(this.getData(), startIndex, startIndex + this.getChmDirList().getDirectoryListingEntryList().get(indexOfResetTable).getLength());
            this.getChmLzxcResetTable().parse(dir_chunk, this.getChmLzxcResetTable());
            this.setIndexOfContent(ChmCommons.indexOf(this.getChmDirList().getDirectoryListingEntryList(), "Content"));
            this.setLzxBlockOffset(this.getChmDirList().getDirectoryListingEntryList().get(this.getIndexOfContent()).getOffset() + this.getChmItsfHeader().getDataOffset());
            this.setLzxBlockLength(this.getChmDirList().getDirectoryListingEntryList().get(this.getIndexOfContent()).getLength());
            this.setLzxBlocksCache(new ArrayList<ChmLzxBlock>());
        }
        catch (IOException ex) {}
    }
    
    public List<String> enumerateChm() {
        final List<String> listOfEntries = new ArrayList<String>();
        final Iterator<DirectoryListingEntry> it = this.getChmDirList().getDirectoryListingEntryList().iterator();
        while (it.hasNext()) {
            listOfEntries.add(it.next().getName());
        }
        return listOfEntries;
    }
    
    public byte[][] extractChmEntry(final DirectoryListingEntry directoryListingEntry) throws TikaException {
        byte[][] tmp = null;
        byte[] dataSegment = null;
        ChmLzxBlock lzxBlock = null;
        try {
            if (directoryListingEntry.getEntryType() == ChmCommons.EntryType.UNCOMPRESSED && directoryListingEntry.getLength() > 0 && !ChmCommons.hasSkip(directoryListingEntry)) {
                final int dataOffset = (int)(this.getChmItsfHeader().getDataOffset() + directoryListingEntry.getOffset());
                dataSegment = ChmCommons.copyOfRange(this.getData(), dataOffset, dataOffset + directoryListingEntry.getLength());
            }
            else if (directoryListingEntry.getEntryType() == ChmCommons.EntryType.COMPRESSED && !ChmCommons.hasSkip(directoryListingEntry)) {
                final ChmBlockInfo bb = ChmBlockInfo.getChmBlockInfoInstance(directoryListingEntry, (int)this.getChmLzxcResetTable().getBlockLen(), this.getChmLzxcControlData());
                tmp = new byte[bb.getEndBlock() - bb.getStartBlock() + 1][];
                int i = 0;
                int start = 0;
                int block = 0;
                if (this.getLzxBlockLength() < 2147483647L && this.getLzxBlockOffset() < 2147483647L) {
                    if (this.getLzxBlocksCache().size() != 0) {
                        for (i = 0; i < this.getLzxBlocksCache().size(); ++i) {
                            lzxBlock = this.getLzxBlocksCache().get(i);
                            for (int j = bb.getIniBlock(); j <= bb.getStartBlock(); ++j) {
                                if (lzxBlock.getBlockNumber() == j && j > start) {
                                    start = j;
                                    block = i;
                                }
                                if (start == bb.getStartBlock()) {
                                    break;
                                }
                            }
                        }
                    }
                    if (i == this.getLzxBlocksCache().size() && i == 0) {
                        start = bb.getIniBlock();
                        dataSegment = ChmCommons.getChmBlockSegment(this.getData(), this.getChmLzxcResetTable(), start, (int)this.getLzxBlockOffset(), (int)this.getLzxBlockLength());
                        lzxBlock = new ChmLzxBlock(start, dataSegment, this.getChmLzxcResetTable().getBlockLen(), null);
                        this.getLzxBlocksCache().add(lzxBlock);
                    }
                    else {
                        lzxBlock = this.getLzxBlocksCache().get(block);
                    }
                    i = start;
                    while (i <= bb.getEndBlock()) {
                        if (i == bb.getStartBlock() && i == bb.getEndBlock()) {
                            dataSegment = lzxBlock.getContent(bb.getStartOffset(), bb.getEndOffset());
                            tmp[0] = dataSegment;
                            break;
                        }
                        if (i == bb.getStartBlock()) {
                            dataSegment = lzxBlock.getContent(bb.getStartOffset());
                            tmp[0] = dataSegment;
                        }
                        if (i > bb.getStartBlock() && i < bb.getEndBlock()) {
                            dataSegment = lzxBlock.getContent();
                            tmp[i - bb.getStartBlock()] = dataSegment;
                        }
                        if (i == bb.getEndBlock()) {
                            dataSegment = lzxBlock.getContent(0, bb.getEndOffset());
                            tmp[i - bb.getStartBlock()] = dataSegment;
                            break;
                        }
                        if (++i % this.getChmLzxcControlData().getResetInterval() == 0L) {
                            lzxBlock = new ChmLzxBlock(i, ChmCommons.getChmBlockSegment(this.getData(), this.getChmLzxcResetTable(), i, (int)this.getLzxBlockOffset(), (int)this.getLzxBlockLength()), this.getChmLzxcResetTable().getBlockLen(), null);
                        }
                        else {
                            lzxBlock = new ChmLzxBlock(i, ChmCommons.getChmBlockSegment(this.getData(), this.getChmLzxcResetTable(), i, (int)this.getLzxBlockOffset(), (int)this.getLzxBlockLength()), this.getChmLzxcResetTable().getBlockLen(), lzxBlock);
                        }
                        this.getLzxBlocksCache().add(lzxBlock);
                    }
                    if (this.getLzxBlocksCache().size() > this.getChmLzxcResetTable().getBlockCount()) {
                        this.getLzxBlocksCache().clear();
                    }
                }
            }
        }
        catch (Exception e) {
            throw new TikaException(e.getMessage());
        }
        return (tmp != null) ? tmp : new byte[1][];
    }
    
    private void setLzxBlocksCache(final List<ChmLzxBlock> lzxBlocksCache) {
        this.lzxBlocksCache = lzxBlocksCache;
    }
    
    private List<ChmLzxBlock> getLzxBlocksCache() {
        return this.lzxBlocksCache;
    }
    
    private void setChmDirList(final ChmDirectoryListingSet chmDirList) {
        this.chmDirList = chmDirList;
    }
    
    public ChmDirectoryListingSet getChmDirList() {
        return this.chmDirList;
    }
    
    private void setChmItsfHeader(final ChmItsfHeader chmItsfHeader) {
        this.chmItsfHeader = chmItsfHeader;
    }
    
    private ChmItsfHeader getChmItsfHeader() {
        return this.chmItsfHeader;
    }
}
