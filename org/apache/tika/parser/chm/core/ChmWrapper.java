// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.parser.chm.core;

import org.apache.tika.parser.chm.accessor.ChmLzxcControlData;
import org.apache.tika.parser.chm.accessor.ChmLzxcResetTable;
import org.apache.tika.parser.chm.accessor.ChmItspHeader;
import org.apache.tika.parser.chm.accessor.ChmItsfHeader;
import org.apache.tika.parser.chm.accessor.ChmDirectoryListingSet;
import org.apache.tika.parser.chm.lzx.ChmLzxBlock;
import java.util.List;

public class ChmWrapper
{
    private List<ChmLzxBlock> lzxBlocksCache;
    private ChmDirectoryListingSet chmDirList;
    private ChmItsfHeader chmItsfHeader;
    private ChmItspHeader chmItspHeader;
    private ChmLzxcResetTable chmLzxcResetTable;
    private ChmLzxcControlData chmLzxcControlData;
    private byte[] data;
    private int indexOfContent;
    private long lzxBlockOffset;
    private long lzxBlockLength;
    private int indexOfResetData;
    private int indexOfResetTable;
    private int startIndex;
    
    public ChmWrapper() {
        this.lzxBlocksCache = null;
        this.chmDirList = null;
        this.chmItsfHeader = null;
        this.chmItspHeader = null;
        this.chmLzxcResetTable = null;
        this.chmLzxcControlData = null;
        this.data = null;
    }
    
    protected int getStartIndex() {
        return this.startIndex;
    }
    
    protected void setStartIndex(final int startIndex) {
        this.startIndex = startIndex;
    }
    
    protected int getIndexOfResetTable() {
        return this.indexOfResetTable;
    }
    
    protected void setIndexOfResetTable(final int indexOfResetTable) {
        this.indexOfResetTable = indexOfResetTable;
    }
    
    protected List<ChmLzxBlock> getLzxBlocksCache() {
        return this.lzxBlocksCache;
    }
    
    protected void setLzxBlocksCache(final List<ChmLzxBlock> lzxBlocksCache) {
        this.lzxBlocksCache = lzxBlocksCache;
    }
    
    protected ChmDirectoryListingSet getChmDirList() {
        return this.chmDirList;
    }
    
    protected void setChmDirList(final ChmDirectoryListingSet chmDirList) {
        this.chmDirList = chmDirList;
    }
    
    protected ChmItsfHeader getChmItsfHeader() {
        return this.chmItsfHeader;
    }
    
    protected void setChmItsfHeader(final ChmItsfHeader chmItsfHeader) {
        this.chmItsfHeader = chmItsfHeader;
    }
    
    protected ChmLzxcResetTable getChmLzxcResetTable() {
        return this.chmLzxcResetTable;
    }
    
    protected void setChmLzxcResetTable(final ChmLzxcResetTable chmLzxcResetTable) {
        this.chmLzxcResetTable = chmLzxcResetTable;
    }
    
    protected ChmLzxcControlData getChmLzxcControlData() {
        return this.chmLzxcControlData;
    }
    
    protected void setChmLzxcControlData(final ChmLzxcControlData chmLzxcControlData) {
        this.chmLzxcControlData = chmLzxcControlData;
    }
    
    protected byte[] getData() {
        return this.data;
    }
    
    protected void setData(final byte[] data) {
        this.data = data;
    }
    
    protected int getIndexOfContent() {
        return this.indexOfContent;
    }
    
    protected void setIndexOfContent(final int indexOfContent) {
        this.indexOfContent = indexOfContent;
    }
    
    protected long getLzxBlockOffset() {
        return this.lzxBlockOffset;
    }
    
    protected void setLzxBlockOffset(final long lzxBlockOffset) {
        this.lzxBlockOffset = lzxBlockOffset;
    }
    
    protected long getLzxBlockLength() {
        return this.lzxBlockLength;
    }
    
    protected void setLzxBlockLength(final long lzxBlockLength) {
        this.lzxBlockLength = lzxBlockLength;
    }
    
    protected void setChmItspHeader(final ChmItspHeader chmItspHeader) {
        this.chmItspHeader = chmItspHeader;
    }
    
    protected ChmItspHeader getChmItspHeader() {
        return this.chmItspHeader;
    }
    
    protected void setIndexOfResetData(final int indexOfResetData) {
        this.indexOfResetData = indexOfResetData;
    }
    
    protected int getIndexOfResetData() {
        return this.indexOfResetData;
    }
}
