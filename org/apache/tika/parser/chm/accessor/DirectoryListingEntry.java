// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.parser.chm.accessor;

import org.apache.tika.exception.TikaException;
import org.apache.tika.parser.chm.assertion.ChmAssert;
import org.apache.tika.parser.chm.core.ChmCommons;

public class DirectoryListingEntry
{
    private int name_length;
    private String name;
    private ChmCommons.EntryType entryType;
    private int offset;
    private int length;
    
    public DirectoryListingEntry() {
    }
    
    public DirectoryListingEntry(final int name_length, final String name, final ChmCommons.EntryType isCompressed, final int offset, final int length) throws TikaException {
        ChmAssert.assertDirectoryListingEntry(name_length, name, isCompressed, offset, length);
        this.setNameLength(name_length);
        this.setName(name);
        this.setEntryType(isCompressed);
        this.setOffset(offset);
        this.setLength(length);
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("name_length:=" + this.getNameLength() + System.getProperty("line.separator"));
        sb.append("name:=" + this.getName() + System.getProperty("line.separator"));
        sb.append("entryType:=" + this.getEntryType() + System.getProperty("line.separator"));
        sb.append("offset:=" + this.getOffset() + System.getProperty("line.separator"));
        sb.append("length:=" + this.getLength());
        return sb.toString();
    }
    
    public int getNameLength() {
        return this.name_length;
    }
    
    protected void setNameLength(final int name_length) {
        this.name_length = name_length;
    }
    
    public String getName() {
        return this.name;
    }
    
    protected void setName(final String name) {
        this.name = name;
    }
    
    public ChmCommons.EntryType getEntryType() {
        return this.entryType;
    }
    
    protected void setEntryType(final ChmCommons.EntryType entryType) {
        this.entryType = entryType;
    }
    
    public int getOffset() {
        return this.offset;
    }
    
    protected void setOffset(final int offset) {
        this.offset = offset;
    }
    
    public int getLength() {
        return this.length;
    }
    
    protected void setLength(final int length) {
        this.length = length;
    }
    
    public static void main(final String[] args) {
    }
}
