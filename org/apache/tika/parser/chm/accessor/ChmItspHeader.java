// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.parser.chm.accessor;

import org.apache.tika.parser.chm.exception.ChmParsingException;
import org.apache.tika.exception.TikaException;
import org.apache.tika.parser.chm.assertion.ChmAssert;
import org.apache.tika.parser.chm.core.ChmCommons;

public class ChmItspHeader implements ChmAccessor<ChmItspHeader>
{
    private static final long serialVersionUID = 1962394421998181341L;
    private byte[] signature;
    private int version;
    private int header_len;
    private int unknown_000c;
    private long block_len;
    private int blockidx_intvl;
    private int index_depth;
    private int index_root;
    private int index_head;
    private int unknown_0024;
    private long num_blocks;
    private int unknown_002c;
    private long lang_id;
    private byte[] system_uuid;
    private byte[] unknown_0044;
    private int dataRemained;
    private int currentPlace;
    
    public ChmItspHeader() {
        this.signature = new String("ITSP").getBytes();
        this.system_uuid = new byte[16];
        this.unknown_0044 = new byte[16];
        this.currentPlace = 0;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("[ signature:=" + new String(this.getSignature()) + System.getProperty("line.separator"));
        sb.append("version:=\t" + this.getVersion() + System.getProperty("line.separator"));
        sb.append("header_len:=\t" + this.getHeader_len() + System.getProperty("line.separator"));
        sb.append("unknown_00c:=\t" + this.getUnknown_000c() + System.getProperty("line.separator"));
        sb.append("block_len:=\t" + this.getBlock_len() + " [directory chunk size]" + System.getProperty("line.separator"));
        sb.append("blockidx_intvl:=" + this.getBlockidx_intvl() + ", density of quickref section, usually 2" + System.getProperty("line.separator"));
        sb.append("index_depth:=\t" + this.getIndex_depth() + ", depth of the index tree - 1 there is no index, 2 if there is one level of PMGI chunk" + System.getProperty("line.separator"));
        sb.append("index_root:=\t" + this.getIndex_root() + ", chunk number of root index chunk, -1 if there is none" + System.getProperty("line.separator"));
        sb.append("index_head:=\t" + this.getIndex_head() + ", chunk number of first PMGL (listing) chunk" + System.getProperty("line.separator"));
        sb.append("unknown_0024:=\t" + this.getUnknown_0024() + ", chunk number of last PMGL (listing) chunk" + System.getProperty("line.separator"));
        sb.append("num_blocks:=\t" + this.getNum_blocks() + ", -1 (unknown)" + System.getProperty("line.separator"));
        sb.append("unknown_002c:=\t" + this.getUnknown_002c() + ", number of directory chunks (total)" + System.getProperty("line.separator"));
        sb.append("lang_id:=\t" + this.getLang_id() + " - " + ChmCommons.getLanguage(this.getLang_id()) + System.getProperty("line.separator"));
        sb.append("system_uuid:=" + this.getSystem_uuid() + System.getProperty("line.separator"));
        sb.append("unknown_0044:=" + this.getUnknown_0044() + " ]");
        return sb.toString();
    }
    
    private void unmarshalCharArray(final byte[] data, final ChmItspHeader chmItspHeader, final int count) throws TikaException {
        ChmAssert.assertByteArrayNotNull(data);
        ChmAssert.assertChmAccessorNotNull(chmItspHeader);
        this.setDataRemained(data.length);
        System.arraycopy(data, 0, chmItspHeader.signature, 0, count);
        this.setCurrentPlace(this.getCurrentPlace() + count);
        this.setDataRemained(this.getDataRemained() - count);
    }
    
    private int unmarshalInt32(final byte[] data, final int dataLenght, int dest) throws TikaException {
        ChmAssert.assertByteArrayNotNull(data);
        if (4 > this.getDataRemained()) {
            throw new TikaException("4 > dataLenght");
        }
        dest = (data[this.getCurrentPlace()] | data[this.getCurrentPlace() + 1] << 8 | data[this.getCurrentPlace() + 2] << 16 | data[this.getCurrentPlace() + 3] << 24);
        this.setCurrentPlace(this.getCurrentPlace() + 4);
        this.setDataRemained(this.getDataRemained() - 4);
        return dest;
    }
    
    private long unmarshalUInt32(final byte[] data, final int dataLenght, long dest) throws TikaException {
        ChmAssert.assertByteArrayNotNull(data);
        if (4 > dataLenght) {
            throw new TikaException("4 > dataLenght");
        }
        dest = (data[this.getCurrentPlace()] | data[this.getCurrentPlace() + 1] << 8 | data[this.getCurrentPlace() + 2] << 16 | data[this.getCurrentPlace() + 3] << 24);
        this.setDataRemained(this.getDataRemained() - 4);
        this.setCurrentPlace(this.getCurrentPlace() + 4);
        return dest;
    }
    
    private byte[] unmarshalUuid(final byte[] data, final int dataLenght, final byte[] dest, final int count) {
        System.arraycopy(data, this.getCurrentPlace(), dest, 0, count);
        this.setCurrentPlace(this.getCurrentPlace() + count);
        this.setDataRemained(this.getDataRemained() - count);
        return dest;
    }
    
    private int getDataRemained() {
        return this.dataRemained;
    }
    
    private void setDataRemained(final int dataRemained) {
        this.dataRemained = dataRemained;
    }
    
    private int getCurrentPlace() {
        return this.currentPlace;
    }
    
    private void setCurrentPlace(final int currentPlace) {
        this.currentPlace = currentPlace;
    }
    
    public byte[] getSignature() {
        return this.signature;
    }
    
    protected void setSignature(final byte[] signature) {
        this.signature = signature;
    }
    
    public int getVersion() {
        return this.version;
    }
    
    protected void setVersion(final int version) {
        this.version = version;
    }
    
    public int getHeader_len() {
        return this.header_len;
    }
    
    protected void setHeader_len(final int header_len) {
        this.header_len = header_len;
    }
    
    public int getUnknown_000c() {
        return this.unknown_000c;
    }
    
    protected void setUnknown_000c(final int unknown_000c) {
        this.unknown_000c = unknown_000c;
    }
    
    public long getBlock_len() {
        return this.block_len;
    }
    
    protected void setBlock_len(final long block_len) {
        this.block_len = block_len;
    }
    
    public int getBlockidx_intvl() {
        return this.blockidx_intvl;
    }
    
    protected void setBlockidx_intvl(final int blockidx_intvl) {
        this.blockidx_intvl = blockidx_intvl;
    }
    
    public int getIndex_depth() {
        return this.index_depth;
    }
    
    protected void setIndex_depth(final int index_depth) {
        this.index_depth = index_depth;
    }
    
    public int getIndex_root() {
        return this.index_root;
    }
    
    protected void setIndex_root(final int index_root) {
        this.index_root = index_root;
    }
    
    public int getIndex_head() {
        return this.index_head;
    }
    
    protected void setIndex_head(final int index_head) {
        this.index_head = index_head;
    }
    
    public int getUnknown_0024() {
        return this.unknown_0024;
    }
    
    protected void setUnknown_0024(final int unknown_0024) {
        this.unknown_0024 = unknown_0024;
    }
    
    public long getNum_blocks() {
        return this.num_blocks;
    }
    
    protected void setNum_blocks(final long num_blocks) {
        this.num_blocks = num_blocks;
    }
    
    public int getUnknown_002c() {
        return this.unknown_002c;
    }
    
    protected void setUnknown_002c(final int unknown_002c) {
        this.unknown_002c = unknown_002c;
    }
    
    public long getLang_id() {
        return this.lang_id;
    }
    
    protected void setLang_id(final long lang_id) {
        this.lang_id = lang_id;
    }
    
    public byte[] getSystem_uuid() {
        return this.system_uuid;
    }
    
    protected void setSystem_uuid(final byte[] system_uuid) {
        this.system_uuid = system_uuid;
    }
    
    public byte[] getUnknown_0044() {
        return this.unknown_0044;
    }
    
    protected void setUnknown_0044(final byte[] unknown_0044) {
        this.unknown_0044 = unknown_0044;
    }
    
    public void parse(final byte[] data, final ChmItspHeader chmItspHeader) throws TikaException {
        if (data.length != 84) {
            throw new ChmParsingException("we only know how to deal with the 0x58 and 0x60 byte structures");
        }
        chmItspHeader.unmarshalCharArray(data, chmItspHeader, 4);
        chmItspHeader.setVersion(chmItspHeader.unmarshalInt32(data, chmItspHeader.getDataRemained(), chmItspHeader.getVersion()));
        chmItspHeader.setHeader_len(chmItspHeader.unmarshalInt32(data, chmItspHeader.getDataRemained(), chmItspHeader.getHeader_len()));
        chmItspHeader.setUnknown_000c(chmItspHeader.unmarshalInt32(data, chmItspHeader.getDataRemained(), chmItspHeader.getUnknown_000c()));
        chmItspHeader.setBlock_len(chmItspHeader.unmarshalUInt32(data, chmItspHeader.getDataRemained(), chmItspHeader.getBlock_len()));
        chmItspHeader.setBlockidx_intvl(chmItspHeader.unmarshalInt32(data, chmItspHeader.getDataRemained(), chmItspHeader.getBlockidx_intvl()));
        chmItspHeader.setIndex_depth(chmItspHeader.unmarshalInt32(data, chmItspHeader.getDataRemained(), chmItspHeader.getIndex_depth()));
        chmItspHeader.setIndex_root(chmItspHeader.unmarshalInt32(data, chmItspHeader.getDataRemained(), chmItspHeader.getIndex_root()));
        chmItspHeader.setIndex_head(chmItspHeader.unmarshalInt32(data, chmItspHeader.getDataRemained(), chmItspHeader.getIndex_head()));
        chmItspHeader.setUnknown_0024(chmItspHeader.unmarshalInt32(data, chmItspHeader.getDataRemained(), chmItspHeader.getUnknown_0024()));
        chmItspHeader.setNum_blocks(chmItspHeader.unmarshalUInt32(data, chmItspHeader.getDataRemained(), chmItspHeader.getNum_blocks()));
        chmItspHeader.setUnknown_002c(chmItspHeader.unmarshalInt32(data, chmItspHeader.getDataRemained(), chmItspHeader.getUnknown_002c()));
        chmItspHeader.setLang_id(chmItspHeader.unmarshalUInt32(data, chmItspHeader.getDataRemained(), chmItspHeader.getLang_id()));
        chmItspHeader.setSystem_uuid(chmItspHeader.unmarshalUuid(data, chmItspHeader.getDataRemained(), chmItspHeader.getSystem_uuid(), 16));
        chmItspHeader.setUnknown_0044(chmItspHeader.unmarshalUuid(data, chmItspHeader.getDataRemained(), chmItspHeader.getUnknown_0044(), 16));
        if (!new String(chmItspHeader.getSignature()).equals("ITSP")) {
            throw new ChmParsingException("seems not valid signature");
        }
        if (chmItspHeader.getVersion() != 1) {
            throw new ChmParsingException("!=ChmConstants.CHM_VER_1");
        }
        if (chmItspHeader.getHeader_len() != 84) {
            throw new ChmParsingException("!= ChmConstants.CHM_ITSP_V1_LEN");
        }
    }
    
    public static void main(final String[] args) {
    }
}
