// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.parser.chm.accessor;

import org.apache.tika.parser.chm.exception.ChmParsingException;
import java.math.BigInteger;
import org.apache.tika.exception.TikaException;
import org.apache.tika.parser.chm.assertion.ChmAssert;
import java.util.Arrays;

public class ChmLzxcResetTable implements ChmAccessor<ChmLzxcResetTable>
{
    private static final long serialVersionUID = -8209574429411707460L;
    private long version;
    private long block_count;
    private long unknown;
    private long table_offset;
    private long uncompressed_len;
    private long compressed_len;
    private long block_len;
    private long[] block_address;
    private int dataRemained;
    private int currentPlace;
    
    public ChmLzxcResetTable() {
        this.currentPlace = 0;
    }
    
    private int getDataRemained() {
        return this.dataRemained;
    }
    
    private void setDataRemained(final int dataRemained) {
        this.dataRemained = dataRemained;
    }
    
    public long[] getBlockAddress() {
        return this.block_address;
    }
    
    public void setBlockAddress(final long[] block_address) {
        this.block_address = block_address;
    }
    
    private int getCurrentPlace() {
        return this.currentPlace;
    }
    
    private void setCurrentPlace(final int currentPlace) {
        this.currentPlace = currentPlace;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("version:=" + this.getVersion() + System.getProperty("line.separator"));
        sb.append("block_count:=" + this.getBlockCount() + System.getProperty("line.separator"));
        sb.append("unknown:=" + this.getUnknown() + System.getProperty("line.separator"));
        sb.append("table_offset:=" + this.getTableOffset() + System.getProperty("line.separator"));
        sb.append("uncompressed_len:=" + this.getUncompressedLen() + System.getProperty("line.separator"));
        sb.append("compressed_len:=" + this.getCompressedLen() + System.getProperty("line.separator"));
        sb.append("block_len:=" + this.getBlockLen() + System.getProperty("line.separator"));
        sb.append("block_addresses:=" + Arrays.toString(this.getBlockAddress()));
        return sb.toString();
    }
    
    private long[] enumerateBlockAddresses(final byte[] data) throws TikaException {
        ChmAssert.assertByteArrayNotNull(data);
        if (this.getBlockCount() > 5000L) {
            this.setBlockCount(5000L);
        }
        if (this.getBlockCount() < 0L && this.getDataRemained() / 8 > 0) {
            this.setBlockCount(this.getDataRemained() / 8);
        }
        final long[] addresses = new long[(int)this.getBlockCount()];
        for (int rem = this.getDataRemained() / 8, i = 0; i < rem; ++i) {
            final long num = -1L;
            try {
                addresses[i] = this.unmarshalUint64(data, num);
            }
            catch (Exception e) {
                throw new TikaException(e.getMessage());
            }
        }
        return addresses;
    }
    
    private boolean validateParamaters(final byte[] data, final ChmLzxcResetTable chmLzxcResetTable) throws TikaException {
        int goodParameter = 0;
        ChmAssert.assertByteArrayNotNull(data);
        ++goodParameter;
        ChmAssert.assertChmAccessorNotNull(chmLzxcResetTable);
        return ++goodParameter == 2;
    }
    
    private long unmarshalUInt32(final byte[] data, long dest) throws TikaException {
        ChmAssert.assertByteArrayNotNull(data);
        dest = (data[this.getCurrentPlace()] | data[this.getCurrentPlace() + 1] << 8 | data[this.getCurrentPlace() + 2] << 16 | data[this.getCurrentPlace() + 3] << 24);
        this.setDataRemained(this.getDataRemained() - 4);
        this.setCurrentPlace(this.getCurrentPlace() + 4);
        return dest;
    }
    
    private long unmarshalUint64(final byte[] data, long dest) throws TikaException {
        ChmAssert.assertByteArrayNotNull(data);
        final byte[] temp = new byte[8];
        int i = 8;
        int j = 7;
        while (i > 0) {
            if (data.length <= this.getCurrentPlace()) {
                throw new TikaException("data is too small to calculate address block");
            }
            temp[j--] = data[this.getCurrentPlace()];
            this.setCurrentPlace(this.getCurrentPlace() + 1);
            --i;
        }
        dest = new BigInteger(temp).longValue();
        this.setDataRemained(this.getDataRemained() - 8);
        return dest;
    }
    
    public long getVersion() {
        return this.version;
    }
    
    public void setVersion(final long version) {
        this.version = version;
    }
    
    public long getBlockCount() {
        return this.block_count;
    }
    
    public void setBlockCount(final long block_count) {
        this.block_count = block_count;
    }
    
    public long getUnknown() {
        return this.unknown;
    }
    
    public void setUnknown(final long unknown) {
        this.unknown = unknown;
    }
    
    public long getTableOffset() {
        return this.table_offset;
    }
    
    public void setTableOffset(final long table_offset) {
        this.table_offset = table_offset;
    }
    
    public long getUncompressedLen() {
        return this.uncompressed_len;
    }
    
    public void setUncompressedLen(final long uncompressed_len) {
        this.uncompressed_len = uncompressed_len;
    }
    
    public long getCompressedLen() {
        return this.compressed_len;
    }
    
    public void setCompressedLen(final long compressed_len) {
        this.compressed_len = compressed_len;
    }
    
    public long getBlockLen() {
        return this.block_len;
    }
    
    public void setBlockLlen(final long block_len) {
        this.block_len = block_len;
    }
    
    public static void main(final String[] args) {
    }
    
    public void parse(final byte[] data, final ChmLzxcResetTable chmLzxcResetTable) throws TikaException {
        this.setDataRemained(data.length);
        if (this.validateParamaters(data, chmLzxcResetTable)) {
            chmLzxcResetTable.setVersion(this.unmarshalUInt32(data, chmLzxcResetTable.getVersion()));
            chmLzxcResetTable.setBlockCount(this.unmarshalUInt32(data, chmLzxcResetTable.getBlockCount()));
            chmLzxcResetTable.setUnknown(this.unmarshalUInt32(data, chmLzxcResetTable.getUnknown()));
            chmLzxcResetTable.setTableOffset(this.unmarshalUInt32(data, chmLzxcResetTable.getTableOffset()));
            chmLzxcResetTable.setUncompressedLen(this.unmarshalUint64(data, chmLzxcResetTable.getUncompressedLen()));
            chmLzxcResetTable.setCompressedLen(this.unmarshalUint64(data, chmLzxcResetTable.getCompressedLen()));
            chmLzxcResetTable.setBlockLlen(this.unmarshalUint64(data, chmLzxcResetTable.getBlockLen()));
            chmLzxcResetTable.setBlockAddress(this.enumerateBlockAddresses(data));
        }
        if (chmLzxcResetTable.getVersion() != 2L) {
            throw new ChmParsingException("does not seem currect version of chmLzxcResetTable");
        }
    }
}
