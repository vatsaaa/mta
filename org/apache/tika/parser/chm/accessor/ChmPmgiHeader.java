// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.parser.chm.accessor;

import java.util.Arrays;
import org.apache.tika.exception.TikaException;
import org.apache.tika.parser.chm.exception.ChmParsingException;
import org.apache.tika.parser.chm.core.ChmCommons;
import org.apache.tika.parser.chm.assertion.ChmAssert;

public class ChmPmgiHeader implements ChmAccessor<ChmPmgiHeader>
{
    private static final long serialVersionUID = -2092282339894303701L;
    private byte[] signature;
    private long free_space;
    private int dataRemained;
    private int currentPlace;
    
    public ChmPmgiHeader() {
        this.signature = new String("PMGI").getBytes();
        this.currentPlace = 0;
    }
    
    private int getDataRemained() {
        return this.dataRemained;
    }
    
    private void setDataRemained(final int dataRemained) {
        this.dataRemained = dataRemained;
    }
    
    private int getCurrentPlace() {
        return this.currentPlace;
    }
    
    private void setCurrentPlace(final int currentPlace) {
        this.currentPlace = currentPlace;
    }
    
    private void unmarshalCharArray(final byte[] data, final ChmPmgiHeader chmPmgiHeader, final int count) throws ChmParsingException {
        int index = -1;
        ChmAssert.assertByteArrayNotNull(data);
        ChmAssert.assertChmAccessorNotNull(chmPmgiHeader);
        ChmAssert.assertPositiveInt(count);
        this.setDataRemained(data.length);
        index = ChmCommons.indexOf(data, "PMGI".getBytes());
        if (index >= 0) {
            System.arraycopy(data, index, chmPmgiHeader.getSignature(), 0, count);
        }
        this.setCurrentPlace(this.getCurrentPlace() + count);
        this.setDataRemained(this.getDataRemained() - count);
    }
    
    private long unmarshalUInt32(final byte[] data, long dest) throws ChmParsingException {
        ChmAssert.assertByteArrayNotNull(data);
        if (4 > this.getDataRemained()) {
            throw new ChmParsingException("4 > dataLenght");
        }
        dest = (data[this.getCurrentPlace()] | data[this.getCurrentPlace() + 1] << 8 | data[this.getCurrentPlace() + 2] << 16 | data[this.getCurrentPlace() + 3] << 24);
        this.setDataRemained(this.getDataRemained() - 4);
        this.setCurrentPlace(this.getCurrentPlace() + 4);
        return dest;
    }
    
    public byte[] getSignature() {
        return this.signature;
    }
    
    protected void setSignature(final byte[] signature) {
        this.signature = signature;
    }
    
    public long getFreeSpace() {
        return this.free_space;
    }
    
    protected void setFreeSpace(final long free_space) {
        this.free_space = free_space;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("signature:=" + new String(this.getSignature()) + ", ");
        sb.append("free space:=" + this.getFreeSpace() + System.getProperty("line.separator"));
        return sb.toString();
    }
    
    public void parse(final byte[] data, final ChmPmgiHeader chmPmgiHeader) throws TikaException {
        if (data.length < 8) {
            throw new TikaException("we only know how to deal with a 0x8 byte structures");
        }
        chmPmgiHeader.unmarshalCharArray(data, chmPmgiHeader, 4);
        chmPmgiHeader.setFreeSpace(chmPmgiHeader.unmarshalUInt32(data, chmPmgiHeader.getFreeSpace()));
        if (!Arrays.equals(chmPmgiHeader.getSignature(), "PMGI".getBytes())) {
            throw new TikaException("it does not seem to be valid a PMGI signature, check ChmItsp index_root if it was -1, means no PMGI, use PMGL insted");
        }
    }
    
    public static void main(final String[] args) {
    }
}
