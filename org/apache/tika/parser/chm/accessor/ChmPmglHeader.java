// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.parser.chm.accessor;

import org.apache.tika.parser.chm.exception.ChmParsingException;
import org.apache.tika.exception.TikaException;
import org.apache.tika.parser.chm.assertion.ChmAssert;

public class ChmPmglHeader implements ChmAccessor<ChmPmglHeader>
{
    private static final long serialVersionUID = -6139486487475923593L;
    private byte[] signature;
    private long free_space;
    private long unknown_0008;
    private int block_prev;
    private int block_next;
    private int dataRemained;
    private int currentPlace;
    
    public ChmPmglHeader() {
        this.signature = new String("PMGL").getBytes();
        this.currentPlace = 0;
    }
    
    private int getDataRemained() {
        return this.dataRemained;
    }
    
    private void setDataRemained(final int dataRemained) {
        this.dataRemained = dataRemained;
    }
    
    private int getCurrentPlace() {
        return this.currentPlace;
    }
    
    private void setCurrentPlace(final int currentPlace) {
        this.currentPlace = currentPlace;
    }
    
    public long getFreeSpace() {
        return this.free_space;
    }
    
    public void setFreeSpace(final long free_space) {
        this.free_space = free_space;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("signatute:=" + new String(this.getSignature()) + ", ");
        sb.append("free space:=" + this.getFreeSpace() + ", ");
        sb.append("unknown0008:=" + this.getUnknown0008() + ", ");
        sb.append("prev block:=" + this.getBlockPrev() + ", ");
        sb.append("next block:=" + this.getBlockNext() + System.getProperty("line.separator"));
        return sb.toString();
    }
    
    protected void unmarshalCharArray(final byte[] data, final ChmPmglHeader chmPmglHeader, final int count) throws TikaException {
        ChmAssert.assertByteArrayNotNull(data);
        this.setDataRemained(data.length);
        System.arraycopy(data, 0, chmPmglHeader.signature, 0, count);
        this.setCurrentPlace(this.getCurrentPlace() + count);
        this.setDataRemained(this.getDataRemained() - count);
    }
    
    private int unmarshalInt32(final byte[] data, int dest) throws TikaException {
        ChmAssert.assertByteArrayNotNull(data);
        if (4 > this.getDataRemained()) {
            throw new TikaException("4 > dataLenght");
        }
        dest = (data[this.getCurrentPlace()] | data[this.getCurrentPlace() + 1] << 8 | data[this.getCurrentPlace() + 2] << 16 | data[this.getCurrentPlace() + 3] << 24);
        this.setCurrentPlace(this.getCurrentPlace() + 4);
        this.setDataRemained(this.getDataRemained() - 4);
        return dest;
    }
    
    private long unmarshalUInt32(final byte[] data, long dest) throws ChmParsingException {
        ChmAssert.assertByteArrayNotNull(data);
        if (4 > this.getDataRemained()) {
            throw new ChmParsingException("4 > dataLenght");
        }
        dest = (data[this.getCurrentPlace()] | data[this.getCurrentPlace() + 1] << 8 | data[this.getCurrentPlace() + 2] << 16 | data[this.getCurrentPlace() + 3] << 24);
        this.setDataRemained(this.getDataRemained() - 4);
        this.setCurrentPlace(this.getCurrentPlace() + 4);
        return dest;
    }
    
    public void parse(final byte[] data, final ChmPmglHeader chmPmglHeader) throws TikaException {
        if (data.length < 20) {
            throw new TikaException(ChmPmglHeader.class.getName() + " we only know how to deal with a 0x14 byte structures");
        }
        chmPmglHeader.unmarshalCharArray(data, chmPmglHeader, 4);
        chmPmglHeader.setFreeSpace(chmPmglHeader.unmarshalUInt32(data, chmPmglHeader.getFreeSpace()));
        chmPmglHeader.setUnknown0008(chmPmglHeader.unmarshalUInt32(data, chmPmglHeader.getUnknown0008()));
        chmPmglHeader.setBlockPrev(chmPmglHeader.unmarshalInt32(data, chmPmglHeader.getBlockPrev()));
        chmPmglHeader.setBlockNext(chmPmglHeader.unmarshalInt32(data, chmPmglHeader.getBlockNext()));
        if (!new String(chmPmglHeader.getSignature()).equals("PMGL")) {
            throw new ChmParsingException(ChmPmglHeader.class.getName() + " pmgl != pmgl.signature");
        }
    }
    
    public byte[] getSignature() {
        return this.signature;
    }
    
    protected void setSignature(final byte[] signature) {
        this.signature = signature;
    }
    
    public long getUnknown0008() {
        return this.unknown_0008;
    }
    
    protected void setUnknown0008(final long unknown_0008) {
        this.unknown_0008 = unknown_0008;
    }
    
    public int getBlockPrev() {
        return this.block_prev;
    }
    
    protected void setBlockPrev(final int block_prev) {
        this.block_prev = block_prev;
    }
    
    public int getBlockNext() {
        return this.block_next;
    }
    
    protected void setBlockNext(final int block_next) {
        this.block_next = block_next;
    }
    
    public static void main(final String[] args) {
    }
}
