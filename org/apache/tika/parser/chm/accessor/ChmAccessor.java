// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.parser.chm.accessor;

import org.apache.tika.exception.TikaException;
import java.io.Serializable;

public interface ChmAccessor<T> extends Serializable
{
    void parse(final byte[] p0, final T p1) throws TikaException;
}
