// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.parser.chm.accessor;

import org.apache.tika.parser.chm.exception.ChmParsingException;
import java.math.BigInteger;
import org.apache.tika.exception.TikaException;
import org.apache.tika.parser.chm.assertion.ChmAssert;

public class ChmItsfHeader implements ChmAccessor<ChmItsfHeader>
{
    private static final long serialVersionUID = 2215291838533213826L;
    private byte[] signature;
    private int version;
    private int header_len;
    private int unknown_000c;
    private long last_modified;
    private long lang_id;
    private byte[] dir_uuid;
    private byte[] stream_uuid;
    private long unknown_offset;
    private long unknown_len;
    private long dir_offset;
    private long dir_len;
    private long data_offset;
    private int dataRemained;
    private int currentPlace;
    
    public ChmItsfHeader() {
        this.signature = new String("ITSF").getBytes();
        this.dir_uuid = new byte[16];
        this.stream_uuid = new byte[16];
        this.currentPlace = 0;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append(new String(this.getSignature()) + " ");
        sb.append(this.getVersion() + " ");
        sb.append(this.getHeaderLen() + " ");
        sb.append(this.getUnknown_000c() + " ");
        sb.append(this.getLastModified() + " ");
        sb.append(this.getLangId() + " ");
        sb.append(this.getDir_uuid() + " ");
        sb.append(this.getStream_uuid() + " ");
        sb.append(this.getUnknownOffset() + " ");
        sb.append(this.getUnknownLen() + " ");
        sb.append(this.getDirOffset() + " ");
        sb.append(this.getDirLen() + " ");
        sb.append(this.getDataOffset() + " ");
        return sb.toString();
    }
    
    public byte[] getSignature() {
        return this.signature;
    }
    
    protected void setSignature(final byte[] signature) {
        this.signature = signature;
    }
    
    public int getVersion() {
        return this.version;
    }
    
    protected void setVersion(final int version) {
        this.version = version;
    }
    
    public int getHeaderLen() {
        return this.header_len;
    }
    
    protected void setHeaderLen(final int header_len) {
        this.header_len = header_len;
    }
    
    public int getUnknown_000c() {
        return this.unknown_000c;
    }
    
    protected void setUnknown_000c(final int unknown_000c) {
        this.unknown_000c = unknown_000c;
    }
    
    public long getLastModified() {
        return this.last_modified;
    }
    
    protected void setLastModified(final long last_modified) {
        this.last_modified = last_modified;
    }
    
    public long getLangId() {
        return this.lang_id;
    }
    
    protected void setLangId(final long lang_id) {
        this.lang_id = lang_id;
    }
    
    public byte[] getDir_uuid() {
        return this.dir_uuid;
    }
    
    protected void setDir_uuid(final byte[] dir_uuid) {
        this.dir_uuid = dir_uuid;
    }
    
    public byte[] getStream_uuid() {
        return this.stream_uuid;
    }
    
    protected void setStream_uuid(final byte[] stream_uuid) {
        this.stream_uuid = stream_uuid;
    }
    
    public long getUnknownOffset() {
        return this.unknown_offset;
    }
    
    protected void setUnknownOffset(final long unknown_offset) {
        this.unknown_offset = unknown_offset;
    }
    
    public long getUnknownLen() {
        return this.unknown_len;
    }
    
    protected void setUnknownLen(final long unknown_len) {
        this.unknown_len = unknown_len;
    }
    
    public long getDirOffset() {
        return this.dir_offset;
    }
    
    protected void setDirOffset(final long dir_offset) {
        this.dir_offset = dir_offset;
    }
    
    public long getDirLen() {
        return this.dir_len;
    }
    
    protected void setDirLen(final long dir_len) {
        this.dir_len = dir_len;
    }
    
    public long getDataOffset() {
        return this.data_offset;
    }
    
    protected void setDataOffset(final long data_offset) {
        this.data_offset = data_offset;
    }
    
    private void unmarshalCharArray(final byte[] data, final ChmItsfHeader chmItsfHeader, final int count) throws TikaException {
        ChmAssert.assertChmAccessorParameters(data, chmItsfHeader, count);
        System.arraycopy(data, 0, chmItsfHeader.signature, 0, count);
        this.setCurrentPlace(this.getCurrentPlace() + count);
        this.setDataRemained(this.getDataRemained() - count);
    }
    
    private byte[] unmarshalUuid(final byte[] data, final byte[] dest, final int count) {
        System.arraycopy(data, this.getCurrentPlace(), dest, 0, count);
        this.setCurrentPlace(this.getCurrentPlace() + count);
        this.setDataRemained(this.getDataRemained() - count);
        return dest;
    }
    
    private long unmarshalUint64(final byte[] data, long dest) throws TikaException {
        final byte[] temp = new byte[8];
        if (8 > this.getDataRemained()) {
            throw new TikaException("8 > this.getDataRemained()");
        }
        int i = 8;
        int j = 7;
        while (i > 0) {
            temp[j--] = data[this.getCurrentPlace()];
            this.setCurrentPlace(this.getCurrentPlace() + 1);
            --i;
        }
        dest = new BigInteger(temp).longValue();
        this.setDataRemained(this.getDataRemained() - 8);
        return dest;
    }
    
    private int unmarshalInt32(final byte[] data, int dest) throws TikaException {
        ChmAssert.assertByteArrayNotNull(data);
        if (4 > this.getDataRemained()) {
            throw new TikaException("4 > dataLenght");
        }
        dest = (data[this.getCurrentPlace()] | data[this.getCurrentPlace() + 1] << 8 | data[this.getCurrentPlace() + 2] << 16 | data[this.getCurrentPlace() + 3] << 24);
        this.setCurrentPlace(this.getCurrentPlace() + 4);
        this.setDataRemained(this.getDataRemained() - 4);
        return dest;
    }
    
    private long unmarshalUInt32(final byte[] data, long dest) throws TikaException {
        ChmAssert.assertByteArrayNotNull(data);
        if (4 > this.getDataRemained()) {
            throw new TikaException("4 > dataLenght");
        }
        dest = (data[this.getCurrentPlace()] | data[this.getCurrentPlace() + 1] << 8 | data[this.getCurrentPlace() + 2] << 16 | data[this.getCurrentPlace() + 3] << 24);
        this.setDataRemained(this.getDataRemained() - 4);
        this.setCurrentPlace(this.getCurrentPlace() + 4);
        return dest;
    }
    
    public static void main(final String[] args) {
    }
    
    private void setDataRemained(final int dataRemained) {
        this.dataRemained = dataRemained;
    }
    
    private int getDataRemained() {
        return this.dataRemained;
    }
    
    private void setCurrentPlace(final int currentPlace) {
        this.currentPlace = currentPlace;
    }
    
    private int getCurrentPlace() {
        return this.currentPlace;
    }
    
    public void parse(final byte[] data, final ChmItsfHeader chmItsfHeader) throws TikaException {
        if (data.length < 88 || data.length > 96) {
            throw new TikaException("we only know how to deal with the 0x58 and 0x60 byte structures");
        }
        chmItsfHeader.setDataRemained(data.length);
        chmItsfHeader.unmarshalCharArray(data, chmItsfHeader, 4);
        chmItsfHeader.setVersion(chmItsfHeader.unmarshalInt32(data, chmItsfHeader.getVersion()));
        chmItsfHeader.setHeaderLen(chmItsfHeader.unmarshalInt32(data, chmItsfHeader.getHeaderLen()));
        chmItsfHeader.setUnknown_000c(chmItsfHeader.unmarshalInt32(data, chmItsfHeader.getUnknown_000c()));
        chmItsfHeader.setLastModified(chmItsfHeader.unmarshalUInt32(data, chmItsfHeader.getLastModified()));
        chmItsfHeader.setLangId(chmItsfHeader.unmarshalUInt32(data, chmItsfHeader.getLangId()));
        chmItsfHeader.setDir_uuid(chmItsfHeader.unmarshalUuid(data, chmItsfHeader.getDir_uuid(), 16));
        chmItsfHeader.setStream_uuid(chmItsfHeader.unmarshalUuid(data, chmItsfHeader.getStream_uuid(), 16));
        chmItsfHeader.setUnknownOffset(chmItsfHeader.unmarshalUint64(data, chmItsfHeader.getUnknownOffset()));
        chmItsfHeader.setUnknownLen(chmItsfHeader.unmarshalUint64(data, chmItsfHeader.getUnknownLen()));
        chmItsfHeader.setDirOffset(chmItsfHeader.unmarshalUint64(data, chmItsfHeader.getDirOffset()));
        chmItsfHeader.setDirLen(chmItsfHeader.unmarshalUint64(data, chmItsfHeader.getDirLen()));
        if (!new String(chmItsfHeader.getSignature()).equals("ITSF")) {
            throw new TikaException("seems not valid file");
        }
        if (chmItsfHeader.getVersion() == 2) {
            if (chmItsfHeader.getHeaderLen() < 88) {
                throw new TikaException("something wrong with header");
            }
        }
        else {
            if (chmItsfHeader.getVersion() != 3) {
                throw new ChmParsingException("unsupported chm format");
            }
            if (chmItsfHeader.getHeaderLen() < 96) {
                throw new TikaException("unknown v3 header lenght");
            }
        }
        if (chmItsfHeader.getVersion() == 3) {
            if (chmItsfHeader.getDataRemained() < 0) {
                throw new TikaException("cannot set data offset, no data remained");
            }
            chmItsfHeader.setDataOffset(chmItsfHeader.getDirOffset() + chmItsfHeader.getDirLen());
        }
        else {
            chmItsfHeader.setDataOffset(chmItsfHeader.getDirOffset() + chmItsfHeader.getDirLen());
        }
    }
}
