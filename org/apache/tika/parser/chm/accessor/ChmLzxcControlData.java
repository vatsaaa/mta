// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.parser.chm.accessor;

import org.apache.tika.exception.TikaException;
import org.apache.tika.parser.chm.assertion.ChmAssert;
import org.apache.tika.parser.chm.exception.ChmParsingException;

public class ChmLzxcControlData implements ChmAccessor<ChmLzxcControlData>
{
    private static final long serialVersionUID = -7897854774939631565L;
    private long size;
    private byte[] signature;
    private long version;
    private long resetInterval;
    private long windowSize;
    private long windowsPerReset;
    private long unknown_18;
    private int dataRemained;
    private int currentPlace;
    
    public ChmLzxcControlData() {
        this.signature = new String("LZXC").getBytes();
        this.currentPlace = 0;
    }
    
    private int getDataRemained() {
        return this.dataRemained;
    }
    
    private void setDataRemained(final int dataRemained) {
        this.dataRemained = dataRemained;
    }
    
    private int getCurrentPlace() {
        return this.currentPlace;
    }
    
    private void setCurrentPlace(final int currentPlace) {
        this.currentPlace = currentPlace;
    }
    
    public long getSize() {
        return this.size;
    }
    
    protected void setSize(final long size) {
        this.size = size;
    }
    
    public byte[] getSignature() {
        return this.signature;
    }
    
    protected void setSignature(final byte[] signature) {
        this.signature = signature;
    }
    
    public long getVersion() {
        return this.version;
    }
    
    protected void setVersion(final long version) {
        this.version = version;
    }
    
    public long getResetInterval() {
        return this.resetInterval;
    }
    
    protected void setResetInterval(final long resetInterval) {
        this.resetInterval = resetInterval;
    }
    
    public long getWindowSize() {
        return this.windowSize;
    }
    
    protected void setWindowSize(final long windowSize) {
        this.windowSize = windowSize;
    }
    
    public long getWindowsPerReset() {
        return this.windowsPerReset;
    }
    
    protected void setWindowsPerReset(final long windowsPerReset) {
        this.windowsPerReset = windowsPerReset;
    }
    
    public long getUnknown_18() {
        return this.unknown_18;
    }
    
    protected void setUnknown_18(final long unknown_18) {
        this.unknown_18 = unknown_18;
    }
    
    private long unmarshalUInt32(final byte[] data, long dest) throws ChmParsingException {
        assert data != null && data.length > 0;
        if (4 > this.getDataRemained()) {
            throw new ChmParsingException("4 > dataLenght");
        }
        dest = (data[this.getCurrentPlace()] | data[this.getCurrentPlace() + 1] << 8 | data[this.getCurrentPlace() + 2] << 16 | data[this.getCurrentPlace() + 3] << 24);
        this.setDataRemained(this.getDataRemained() - 4);
        this.setCurrentPlace(this.getCurrentPlace() + 4);
        return dest;
    }
    
    private void unmarshalCharArray(final byte[] data, final ChmLzxcControlData chmLzxcControlData, final int count) throws TikaException {
        ChmAssert.assertByteArrayNotNull(data);
        ChmAssert.assertChmAccessorNotNull(chmLzxcControlData);
        ChmAssert.assertPositiveInt(count);
        System.arraycopy(data, 4, chmLzxcControlData.getSignature(), 0, count);
        this.setCurrentPlace(this.getCurrentPlace() + count);
        this.setDataRemained(this.getDataRemained() - count);
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("size(unknown):=" + this.getSize() + ", ");
        sb.append("signature(Compression type identifier):=" + new String(this.getSignature()) + ", ");
        sb.append("version(Possibly numeric code for LZX):=" + this.getVersion() + System.getProperty("line.separator"));
        sb.append("resetInterval(The Huffman reset interval):=" + this.getResetInterval() + ", ");
        sb.append("windowSize:=" + this.getWindowSize() + ", ");
        sb.append("windowsPerReset(unknown (sometimes 2, sometimes 1, sometimes 0):=" + this.getWindowsPerReset() + ", ");
        sb.append("unknown_18:=" + this.getUnknown_18() + System.getProperty("line.separator"));
        return sb.toString();
    }
    
    public void parse(final byte[] data, final ChmLzxcControlData chmLzxcControlData) throws TikaException {
        if (data == null || data.length < 24) {
            throw new ChmParsingException("we want at least 0x18 bytes");
        }
        chmLzxcControlData.setDataRemained(data.length);
        chmLzxcControlData.setSize(this.unmarshalUInt32(data, chmLzxcControlData.getSize()));
        chmLzxcControlData.unmarshalCharArray(data, chmLzxcControlData, 4);
        chmLzxcControlData.setVersion(this.unmarshalUInt32(data, chmLzxcControlData.getVersion()));
        chmLzxcControlData.setResetInterval(this.unmarshalUInt32(data, chmLzxcControlData.getResetInterval()));
        chmLzxcControlData.setWindowSize(this.unmarshalUInt32(data, chmLzxcControlData.getWindowSize()));
        chmLzxcControlData.setWindowsPerReset(this.unmarshalUInt32(data, chmLzxcControlData.getWindowsPerReset()));
        if (data.length >= 28) {
            chmLzxcControlData.setUnknown_18(this.unmarshalUInt32(data, chmLzxcControlData.getUnknown_18()));
        }
        else {
            chmLzxcControlData.setUnknown_18(0L);
        }
        if (chmLzxcControlData.getVersion() == 2L) {
            chmLzxcControlData.setWindowSize(this.getWindowSize() * 32768L);
        }
        if (chmLzxcControlData.getWindowSize() == 0L || chmLzxcControlData.getResetInterval() == 0L) {
            throw new ChmParsingException("window size / resetInterval should be more than zero");
        }
        if (chmLzxcControlData.getWindowSize() == 1L) {
            throw new ChmParsingException("window size / resetInterval should be more than 1");
        }
        if (!new String(chmLzxcControlData.getSignature()).equals("LZXC")) {
            throw new ChmParsingException("the signature does not seem to be correct");
        }
    }
    
    public static void main(final String[] args) {
    }
}
