// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.parser.chm.assertion;

import org.apache.tika.parser.chm.core.ChmCommons;
import org.apache.tika.parser.chm.exception.ChmParsingException;
import org.apache.tika.parser.chm.accessor.ChmAccessor;
import java.io.IOException;
import java.io.InputStream;
import org.apache.tika.exception.TikaException;
import org.apache.tika.parser.chm.accessor.ChmLzxcResetTable;

public class ChmAssert
{
    public static final void assertChmBlockSegment(final byte[] data, final ChmLzxcResetTable resetTable, final int blockNumber, final int lzxcBlockOffset, final int lzxcBlockLength) throws TikaException {
        if (data == null) {
            throw new TikaException("data[] is null");
        }
        if (data.length <= 0) {
            throw new TikaException("data[] length should be greater than zero");
        }
        if (resetTable == null) {
            throw new TikaException("resetTable is null");
        }
        if (resetTable.getBlockAddress().length <= 1) {
            throw new TikaException("resetTable.getBlockAddress().length should be greater than zero");
        }
        if (blockNumber < 0) {
            throw new TikaException("blockNumber should be positive number");
        }
        if (lzxcBlockOffset < 0) {
            throw new TikaException("lzxcBlockOffset should be positive number");
        }
        if (lzxcBlockLength < 0) {
            throw new TikaException("lzxcBlockLength should be positive number");
        }
    }
    
    public static final void assertInputStreamNotNull(final InputStream is) throws IOException {
        if (is == null) {
            throw new IOException("input sream is null");
        }
    }
    
    public static final void assertChmAccessorParameters(final byte[] data, final ChmAccessor<?> chmAccessor, final int count) throws ChmParsingException {
        assertByteArrayNotNull(data);
        assertChmAccessorNotNull(chmAccessor);
    }
    
    public static final void assertByteArrayNotNull(final byte[] data) throws ChmParsingException {
        if (data == null) {
            throw new ChmParsingException("byte[] data is null");
        }
    }
    
    public static final void assertChmAccessorNotNull(final ChmAccessor<?> chmAccessor) throws ChmParsingException {
        if (chmAccessor == null) {
            throw new ChmParsingException("chm header is null");
        }
    }
    
    public static final void assertDirectoryListingEntry(final int name_length, final String name, final ChmCommons.EntryType entryType, final int offset, final int length) throws ChmParsingException {
        if (name_length < 0) {
            throw new ChmParsingException("invalid name length");
        }
        if (name == null) {
            throw new ChmParsingException("invalid name");
        }
        if (entryType != ChmCommons.EntryType.COMPRESSED && entryType != ChmCommons.EntryType.UNCOMPRESSED) {
            throw new ChmParsingException("invalid compressed type, should be EntryType.COMPRESSED | EntryType.UNCOMPRESSED");
        }
        if (offset < 0) {
            throw new ChmParsingException("invalid offset");
        }
        if (length < 0) {
            throw new ChmParsingException("invalid length");
        }
    }
    
    public static void assertCopyingDataIndex(final int index, final int dataLength) throws ChmParsingException {
        if (index >= dataLength) {
            throw new ChmParsingException("cannot parse chm file index > data.length");
        }
    }
    
    public static void assertPositiveInt(final int param) throws ChmParsingException {
        if (param <= 0) {
            throw new ChmParsingException("resetTable.getBlockAddress().length should be greater than zero");
        }
    }
}
