// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.parser.chm;

import org.xml.sax.SAXException;
import org.apache.tika.exception.TikaException;
import org.xml.sax.ContentHandler;
import org.apache.tika.sax.TextContentHandler;

public class CHM2XHTML
{
    protected TextContentHandler handler;
    
    public static void process(final CHMDocumentInformation chmDoc, final ContentHandler handler) throws TikaException {
        final String text = chmDoc.getText();
        try {
            if (text.length() <= 0) {
                throw new TikaException("Could not extract content");
            }
            handler.characters(text.toCharArray(), 0, text.length());
            new CHM2XHTML(chmDoc, handler);
        }
        catch (SAXException e) {
            throw new RuntimeException(e);
        }
    }
    
    protected String getText(final CHMDocumentInformation chmDoc) throws TikaException {
        return chmDoc.getText();
    }
    
    public CHM2XHTML(final CHMDocumentInformation chmDoc, final ContentHandler handler) {
        this.handler = new TextContentHandler(handler);
    }
}
