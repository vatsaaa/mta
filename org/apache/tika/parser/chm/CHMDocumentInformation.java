// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.parser.chm;

import org.xml.sax.SAXException;
import org.xml.sax.ContentHandler;
import java.io.ByteArrayInputStream;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.sax.BodyContentHandler;
import org.apache.tika.parser.html.HtmlParser;
import org.apache.tika.metadata.Metadata;
import java.util.Iterator;
import org.apache.tika.parser.chm.accessor.DirectoryListingEntry;
import java.io.IOException;
import org.apache.tika.exception.TikaException;
import java.io.InputStream;
import org.apache.tika.parser.chm.core.ChmExtractor;

public class CHMDocumentInformation
{
    private ChmExtractor chmExtractor;
    
    public CHMDocumentInformation() {
        this.chmExtractor = null;
    }
    
    public static CHMDocumentInformation load(final InputStream is) throws TikaException, IOException {
        final CHMDocumentInformation document = new CHMDocumentInformation();
        document.setChmExtractor(new ChmExtractor(is));
        return document;
    }
    
    private String getContent() {
        final StringBuilder sb = new StringBuilder();
        final Iterator<DirectoryListingEntry> it = this.getChmExtractor().getChmDirList().getDirectoryListingEntryList().iterator();
        while (it.hasNext()) {
            try {
                final DirectoryListingEntry entry = it.next();
                if (!this.isRightEntry(entry)) {
                    continue;
                }
                final byte[][] tmp = this.getChmExtractor().extractChmEntry(entry);
                if (tmp == null) {
                    continue;
                }
                sb.append(this.extract(tmp));
            }
            catch (TikaException e) {}
        }
        return sb.toString();
    }
    
    private boolean isRightEntry(final DirectoryListingEntry entry) {
        return entry.getName().endsWith(".html") || entry.getName().endsWith(".htm");
    }
    
    private ChmExtractor getChmExtractor() {
        return this.chmExtractor;
    }
    
    private void setChmExtractor(final ChmExtractor chmExtractor) {
        this.chmExtractor = chmExtractor;
    }
    
    public void getCHMDocInformation(final Metadata metadata) throws TikaException, IOException {
        if (this.getChmExtractor() != null) {
            metadata.add("Content-Type", "application/x-chm");
        }
        else {
            metadata.add("Content-Type", "unknown");
        }
    }
    
    public String getText() throws TikaException {
        return this.getContent();
    }
    
    private String extract(final byte[][] byteObject) {
        final StringBuilder wBuf = new StringBuilder();
        InputStream stream = null;
        final Metadata metadata = new Metadata();
        final HtmlParser htmlParser = new HtmlParser();
        final BodyContentHandler handler = new BodyContentHandler(-1);
        final ParseContext parser = new ParseContext();
        try {
            for (int i = 0; i < byteObject.length; ++i) {
                stream = new ByteArrayInputStream(byteObject[i]);
                try {
                    htmlParser.parse(stream, handler, metadata, parser);
                }
                catch (TikaException e2) {
                    wBuf.append(new String(byteObject[i]));
                }
                finally {
                    wBuf.append(handler.toString() + System.getProperty("line.separator"));
                    stream.close();
                }
            }
        }
        catch (SAXException e) {
            throw new RuntimeException(e);
        }
        catch (IOException ex) {}
        return wBuf.toString();
    }
    
    public static void main(final String[] args) {
    }
}
