// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.parser.mp4;

import java.util.Collections;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Date;
import org.apache.tika.metadata.Property;
import org.apache.tika.exception.TikaException;
import org.xml.sax.SAXException;
import java.io.IOException;
import com.coremedia.iso.boxes.SampleDescriptionBox;
import com.coremedia.iso.boxes.SampleTableBox;
import com.coremedia.iso.boxes.TrackHeaderBox;
import java.util.Iterator;
import com.coremedia.iso.boxes.Box;
import com.coremedia.iso.boxes.apple.AppleEncoderBox;
import com.coremedia.iso.boxes.apple.AppleCommentBox;
import com.coremedia.iso.boxes.apple.AppleTrackNumberBox;
import com.coremedia.iso.boxes.apple.AppleRecordingYearBox;
import com.coremedia.iso.boxes.apple.AppleCustomGenreBox;
import com.coremedia.iso.boxes.apple.AppleStandardGenreBox;
import com.coremedia.iso.boxes.apple.AppleTrackAuthorBox;
import com.coremedia.iso.boxes.apple.AppleAlbumBox;
import com.coremedia.iso.boxes.apple.AppleArtistBox;
import com.coremedia.iso.boxes.apple.AbstractAppleMetaDataBox;
import com.coremedia.iso.boxes.apple.AppleTrackTitleBox;
import com.coremedia.iso.boxes.apple.AppleItemListBox;
import com.coremedia.iso.boxes.MetaBox;
import com.coremedia.iso.boxes.UserDataBox;
import com.coremedia.iso.boxes.sampleentry.AudioSampleEntry;
import com.coremedia.iso.boxes.TrackBox;
import org.apache.tika.metadata.TikaCoreProperties;
import com.coremedia.iso.boxes.MovieHeaderBox;
import org.apache.tika.sax.XHTMLContentHandler;
import com.coremedia.iso.boxes.MovieBox;
import org.apache.tika.metadata.XMPDM;
import com.coremedia.iso.boxes.ContainerBox;
import com.coremedia.iso.boxes.FileTypeBox;
import java.nio.channels.ReadableByteChannel;
import com.coremedia.iso.IsoFile;
import org.apache.tika.io.TikaInputStream;
import org.apache.tika.metadata.Metadata;
import org.xml.sax.ContentHandler;
import java.io.InputStream;
import org.apache.tika.parser.ParseContext;
import java.util.Set;
import java.util.List;
import org.apache.tika.mime.MediaType;
import java.util.Map;
import org.apache.tika.parser.AbstractParser;

public class MP4Parser extends AbstractParser
{
    private static final long serialVersionUID = 84011216792285L;
    private static final Map<MediaType, List<String>> typesMap;
    private static final Set<MediaType> SUPPORTED_TYPES;
    private static final long EPOC_AS_MP4_TIME = 2082844800L;
    
    public Set<MediaType> getSupportedTypes(final ParseContext context) {
        return MP4Parser.SUPPORTED_TYPES;
    }
    
    public void parse(final InputStream stream, final ContentHandler handler, final Metadata metadata, final ParseContext context) throws IOException, SAXException, TikaException {
        final TikaInputStream tstream = TikaInputStream.get(stream);
        IsoFile isoFile;
        try {
            isoFile = new IsoFile(tstream.getFileChannel());
        }
        finally {
            tstream.close();
        }
        final FileTypeBox fileType = getOrNull(isoFile, FileTypeBox.class);
        if (fileType != null) {
            MediaType type = MediaType.application("mp4");
            for (final MediaType t : MP4Parser.typesMap.keySet()) {
                if (MP4Parser.typesMap.get(t).contains(fileType.getMajorBrand())) {
                    type = t;
                    break;
                }
            }
            metadata.set("Content-Type", type.toString());
            if (type.getType().equals("audio")) {
                metadata.set(XMPDM.AUDIO_COMPRESSOR, fileType.getMajorBrand().trim());
            }
        }
        else {
            metadata.set("Content-Type", "video/quicktime");
        }
        final MovieBox moov = getOrNull(isoFile, MovieBox.class);
        if (moov == null) {
            return;
        }
        final XHTMLContentHandler xhtml = new XHTMLContentHandler(handler, metadata);
        xhtml.startDocument();
        final MovieHeaderBox mHeader = getOrNull(moov, MovieHeaderBox.class);
        if (mHeader != null) {
            metadata.set(Metadata.CREATION_DATE, MP4TimeToDate(mHeader.getCreationTime()));
            metadata.set(TikaCoreProperties.MODIFIED, MP4TimeToDate(mHeader.getModificationTime()));
            final double durationSeconds = mHeader.getDuration() / (double)mHeader.getTimescale();
            metadata.set(XMPDM.AUDIO_SAMPLE_RATE, (int)mHeader.getTimescale());
        }
        final List<TrackBox> tb = moov.getBoxes(TrackBox.class);
        if (tb.size() > 0) {
            final TrackBox track = tb.get(0);
            final TrackHeaderBox header = track.getTrackHeaderBox();
            metadata.set(TikaCoreProperties.CREATED, MP4TimeToDate(header.getCreationTime()));
            metadata.set(TikaCoreProperties.MODIFIED, MP4TimeToDate(header.getModificationTime()));
            metadata.set(Metadata.IMAGE_WIDTH, (int)header.getWidth());
            metadata.set(Metadata.IMAGE_LENGTH, (int)header.getHeight());
            final SampleTableBox samples = track.getSampleTableBox();
            final SampleDescriptionBox sampleDesc = samples.getSampleDescriptionBox();
            if (sampleDesc != null) {
                final AudioSampleEntry sample = getOrNull(sampleDesc, AudioSampleEntry.class);
                if (sample != null) {
                    XMPDM.ChannelTypePropertyConverter.convertAndSet(metadata, sample.getChannelCount());
                    metadata.set(XMPDM.AUDIO_SAMPLE_RATE, (int)sample.getSampleRate());
                }
            }
        }
        final UserDataBox userData = getOrNull(moov, UserDataBox.class);
        if (userData != null) {
            final MetaBox meta = getOrNull(userData, MetaBox.class);
            final AppleItemListBox apple = getOrNull(meta, AppleItemListBox.class);
            if (apple != null) {
                final AppleTrackTitleBox title = getOrNull(apple, AppleTrackTitleBox.class);
                addMetadata(TikaCoreProperties.TITLE, metadata, title);
                final AppleArtistBox artist = getOrNull(apple, AppleArtistBox.class);
                addMetadata(TikaCoreProperties.CREATOR, metadata, artist);
                addMetadata(XMPDM.ARTIST, metadata, artist);
                final AppleAlbumBox album = getOrNull(apple, AppleAlbumBox.class);
                addMetadata(XMPDM.ALBUM, metadata, album);
                final AppleTrackAuthorBox composer = getOrNull(apple, AppleTrackAuthorBox.class);
                addMetadata(XMPDM.COMPOSER, metadata, composer);
                final AppleStandardGenreBox sGenre = getOrNull(apple, AppleStandardGenreBox.class);
                final AppleCustomGenreBox cGenre = getOrNull(apple, AppleCustomGenreBox.class);
                addMetadata(XMPDM.GENRE, metadata, sGenre);
                addMetadata(XMPDM.GENRE, metadata, cGenre);
                final AppleRecordingYearBox year = getOrNull(apple, AppleRecordingYearBox.class);
                addMetadata(XMPDM.RELEASE_DATE, metadata, year);
                final AppleTrackNumberBox trackNum = getOrNull(apple, AppleTrackNumberBox.class);
                if (trackNum != null) {
                    metadata.set(XMPDM.TRACK_NUMBER, trackNum.getTrackNumber());
                }
                final AppleCommentBox comment = getOrNull(apple, AppleCommentBox.class);
                addMetadata(XMPDM.LOG_COMMENT, metadata, comment);
                final AppleEncoderBox encoder = getOrNull(apple, AppleEncoderBox.class);
                for (final Box box : apple.getBoxes()) {
                    if (box instanceof AbstractAppleMetaDataBox) {
                        xhtml.element("p", ((AbstractAppleMetaDataBox)box).getValue());
                    }
                }
            }
        }
        xhtml.endDocument();
    }
    
    private static void addMetadata(final String key, final Metadata m, final AbstractAppleMetaDataBox metadata) {
        if (metadata != null) {
            m.add(key, metadata.getValue());
        }
    }
    
    private static void addMetadata(final Property prop, final Metadata m, final AbstractAppleMetaDataBox metadata) {
        if (metadata != null) {
            m.set(prop, metadata.getValue());
        }
    }
    
    private static Date MP4TimeToDate(final long mp4Time) {
        final long unix = mp4Time - 2082844800L;
        return new Date(unix * 1000L);
    }
    
    private static <T extends Box> T getOrNull(final ContainerBox box, final Class<T> clazz) {
        if (box == null) {
            return null;
        }
        final List<T> boxes = box.getBoxes(clazz);
        if (boxes.size() == 0) {
            return null;
        }
        return boxes.get(0);
    }
    
    static {
        (typesMap = new HashMap<MediaType, List<String>>()).put(MediaType.audio("mp4"), Arrays.asList("M4A ", "M4B ", "F4A ", "F4B "));
        MP4Parser.typesMap.put(MediaType.video("3gpp"), Arrays.asList("3ge6", "3ge7", "3gg6", "3gp1", "3gp2", "3gp3", "3gp4", "3gp5", "3gp6", "3gs7"));
        MP4Parser.typesMap.put(MediaType.video("3gpp2"), Arrays.asList("3g2a", "3g2b", "3g2c"));
        MP4Parser.typesMap.put(MediaType.video("mp4"), Arrays.asList("mp41", "mp42"));
        MP4Parser.typesMap.put(MediaType.video("x-m4v"), Arrays.asList("M4V ", "M4VH", "M4VP"));
        MP4Parser.typesMap.put(MediaType.video("quicktime"), Collections.emptyList());
        MP4Parser.typesMap.put(MediaType.application("mp4"), Collections.emptyList());
        SUPPORTED_TYPES = Collections.unmodifiableSet((Set<? extends MediaType>)MP4Parser.typesMap.keySet());
    }
}
