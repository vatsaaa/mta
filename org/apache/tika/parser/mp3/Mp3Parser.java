// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.parser.mp3;

import java.util.Collections;
import org.apache.tika.exception.TikaException;
import org.xml.sax.SAXException;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import org.apache.tika.metadata.TikaCoreProperties;
import org.apache.tika.sax.XHTMLContentHandler;
import org.apache.tika.metadata.XMPDM;
import org.apache.tika.metadata.Metadata;
import org.xml.sax.ContentHandler;
import java.io.InputStream;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.mime.MediaType;
import java.util.Set;
import org.apache.tika.parser.AbstractParser;

public class Mp3Parser extends AbstractParser
{
    private static final long serialVersionUID = 8537074922934844370L;
    private static final Set<MediaType> SUPPORTED_TYPES;
    
    public Set<MediaType> getSupportedTypes(final ParseContext context) {
        return Mp3Parser.SUPPORTED_TYPES;
    }
    
    public void parse(final InputStream stream, final ContentHandler handler, final Metadata metadata, final ParseContext context) throws IOException, SAXException, TikaException {
        metadata.set("Content-Type", "audio/mpeg");
        metadata.set(XMPDM.AUDIO_COMPRESSOR, "MP3");
        final XHTMLContentHandler xhtml = new XHTMLContentHandler(handler, metadata);
        xhtml.startDocument();
        final ID3TagsAndAudio audioAndTags = getAllTagHandlers(stream, handler);
        if (audioAndTags.tags.length > 0) {
            final CompositeTagHandler tag = new CompositeTagHandler(audioAndTags.tags);
            metadata.set(TikaCoreProperties.TITLE, tag.getTitle());
            metadata.set(TikaCoreProperties.CREATOR, tag.getArtist());
            metadata.set(XMPDM.ARTIST, tag.getArtist());
            metadata.set(XMPDM.COMPOSER, tag.getComposer());
            metadata.set(XMPDM.ALBUM, tag.getAlbum());
            metadata.set(XMPDM.RELEASE_DATE, tag.getYear());
            metadata.set(XMPDM.GENRE, tag.getGenre());
            final List<String> comments = new ArrayList<String>();
            for (final ID3Tags.ID3Comment comment : tag.getComments()) {
                final StringBuffer cmt = new StringBuffer();
                if (comment.getLanguage() != null) {
                    cmt.append(comment.getLanguage());
                    cmt.append(" - ");
                }
                if (comment.getDescription() != null) {
                    cmt.append(comment.getDescription());
                    if (comment.getText() != null) {
                        cmt.append("\n");
                    }
                }
                if (comment.getText() != null) {
                    cmt.append(comment.getText());
                }
                comments.add(cmt.toString());
                metadata.add(XMPDM.LOG_COMMENT.getName(), cmt.toString());
            }
            xhtml.element("h1", tag.getTitle());
            xhtml.element("p", tag.getArtist());
            if (tag.getTrackNumber() != null) {
                xhtml.element("p", tag.getAlbum() + ", track " + tag.getTrackNumber());
                metadata.set(XMPDM.TRACK_NUMBER, tag.getTrackNumber());
            }
            else {
                xhtml.element("p", tag.getAlbum());
            }
            xhtml.element("p", tag.getYear());
            xhtml.element("p", tag.getGenre());
            for (final String comment2 : comments) {
                xhtml.element("p", comment2);
            }
        }
        if (audioAndTags.audio != null) {
            metadata.set("samplerate", String.valueOf(audioAndTags.audio.getSampleRate()));
            metadata.set("channels", String.valueOf(audioAndTags.audio.getChannels()));
            metadata.set("version", audioAndTags.audio.getVersion());
            metadata.set(XMPDM.AUDIO_SAMPLE_RATE, Integer.toString(audioAndTags.audio.getSampleRate()));
            if (audioAndTags.audio.getChannels() == 1) {
                metadata.set(XMPDM.AUDIO_CHANNEL_TYPE, "Mono");
            }
            else if (audioAndTags.audio.getChannels() == 2) {
                metadata.set(XMPDM.AUDIO_CHANNEL_TYPE, "Stereo");
            }
            else if (audioAndTags.audio.getChannels() == 5) {
                metadata.set(XMPDM.AUDIO_CHANNEL_TYPE, "5.1");
            }
            else if (audioAndTags.audio.getChannels() == 7) {
                metadata.set(XMPDM.AUDIO_CHANNEL_TYPE, "7.1");
            }
        }
        if (audioAndTags.lyrics != null && audioAndTags.lyrics.hasLyrics()) {
            xhtml.startElement("p", "class", "lyrics");
            xhtml.characters(audioAndTags.lyrics.lyricsText);
            xhtml.endElement("p");
        }
        xhtml.endDocument();
    }
    
    protected static ID3TagsAndAudio getAllTagHandlers(final InputStream stream, final ContentHandler handler) throws IOException, SAXException, TikaException {
        ID3v24Handler v24 = null;
        ID3v23Handler v25 = null;
        ID3v22Handler v26 = null;
        ID3v1Handler v27 = null;
        LyricsHandler lyrics = null;
        AudioFrame firstAudio = null;
        MP3Frame f;
        while ((f = ID3v2Frame.createFrameIfPresent(stream)) != null && firstAudio == null) {
            if (f instanceof ID3v2Frame) {
                final ID3v2Frame id3F = (ID3v2Frame)f;
                if (id3F.getMajorVersion() == 4) {
                    v24 = new ID3v24Handler(id3F);
                }
                else if (id3F.getMajorVersion() == 3) {
                    v25 = new ID3v23Handler(id3F);
                }
                else {
                    if (id3F.getMajorVersion() != 2) {
                        continue;
                    }
                    v26 = new ID3v22Handler(id3F);
                }
            }
            else {
                if (!(f instanceof AudioFrame)) {
                    continue;
                }
                firstAudio = (AudioFrame)f;
            }
        }
        lyrics = new LyricsHandler(stream, handler);
        v27 = lyrics.id3v1;
        final List<ID3Tags> tags = new ArrayList<ID3Tags>();
        if (v24 != null && v24.getTagsPresent()) {
            tags.add(v24);
        }
        if (v25 != null && v25.getTagsPresent()) {
            tags.add(v25);
        }
        if (v26 != null && v26.getTagsPresent()) {
            tags.add(v26);
        }
        if (v27 != null && v27.getTagsPresent()) {
            tags.add(v27);
        }
        final ID3TagsAndAudio ret = new ID3TagsAndAudio();
        ret.audio = firstAudio;
        ret.lyrics = lyrics;
        ret.tags = tags.toArray(new ID3Tags[tags.size()]);
        return ret;
    }
    
    static {
        SUPPORTED_TYPES = Collections.singleton(MediaType.audio("mpeg"));
    }
    
    protected static class ID3TagsAndAudio
    {
        private ID3Tags[] tags;
        private AudioFrame audio;
        private LyricsHandler lyrics;
    }
}
