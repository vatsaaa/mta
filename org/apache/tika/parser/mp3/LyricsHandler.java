// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.parser.mp3;

import org.apache.tika.exception.TikaException;
import org.xml.sax.SAXException;
import java.io.IOException;
import org.xml.sax.ContentHandler;
import java.io.InputStream;

public class LyricsHandler
{
    boolean foundLyrics;
    String lyricsText;
    ID3v1Handler id3v1;
    
    public LyricsHandler(final InputStream stream, final ContentHandler handler) throws IOException, SAXException, TikaException {
        this(getSuffix(stream, 10368));
    }
    
    protected LyricsHandler(final byte[] tagData) throws IOException, SAXException, TikaException {
        this.foundLyrics = false;
        this.lyricsText = null;
        this.id3v1 = null;
        if (tagData.length < 128) {
            return;
        }
        final byte[] last128 = new byte[128];
        System.arraycopy(tagData, tagData.length - 128, last128, 0, 128);
        this.id3v1 = new ID3v1Handler(last128);
        if (tagData.length < 137) {
            return;
        }
        int lookat = tagData.length - 9;
        if (this.id3v1.found) {
            lookat -= 128;
        }
        if (tagData[lookat + 0] == 76 && tagData[lookat + 1] == 89 && tagData[lookat + 2] == 82 && tagData[lookat + 3] == 73 && tagData[lookat + 4] == 67 && tagData[lookat + 5] == 83 && tagData[lookat + 6] == 50 && tagData[lookat + 7] == 48 && tagData[lookat + 8] == 48) {
            this.foundLyrics = true;
            final int length = Integer.parseInt(new String(tagData, lookat - 6, 6));
            final String lyrics = new String(tagData, lookat - length + 5, length - 11, "ASCII");
            int endPos;
            for (int pos = 0; pos < lyrics.length() - 8; pos = endPos) {
                final String tagName = lyrics.substring(pos, pos + 3);
                final int tagLen = Integer.parseInt(lyrics.substring(pos + 3, pos + 8));
                final int startPos = pos + 8;
                endPos = startPos + tagLen;
                if (tagName.equals("LYR")) {
                    this.lyricsText = lyrics.substring(startPos, endPos);
                }
            }
        }
    }
    
    public boolean hasID3v1() {
        return this.id3v1 != null && this.id3v1.found;
    }
    
    public boolean hasLyrics() {
        return this.lyricsText != null && this.lyricsText.length() > 0;
    }
    
    protected static byte[] getSuffix(final InputStream stream, int length) throws IOException {
        final byte[] buffer = new byte[2 * length];
        int bytesInBuffer = 0;
        for (int n = stream.read(buffer); n != -1; n = stream.read(buffer, bytesInBuffer, buffer.length - bytesInBuffer)) {
            bytesInBuffer += n;
            if (bytesInBuffer == buffer.length) {
                System.arraycopy(buffer, bytesInBuffer - length, buffer, 0, length);
                bytesInBuffer = length;
            }
        }
        if (bytesInBuffer < length) {
            length = bytesInBuffer;
        }
        final byte[] result = new byte[length];
        System.arraycopy(buffer, bytesInBuffer - length, result, 0, length);
        return result;
    }
}
