// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.parser.mp3;

import java.util.Collections;
import java.util.List;

public class CompositeTagHandler implements ID3Tags
{
    private ID3Tags[] tags;
    
    public CompositeTagHandler(final ID3Tags[] tags) {
        this.tags = tags;
    }
    
    public boolean getTagsPresent() {
        for (final ID3Tags tag : this.tags) {
            if (tag.getTagsPresent()) {
                return true;
            }
        }
        return false;
    }
    
    public String getTitle() {
        for (final ID3Tags tag : this.tags) {
            if (tag.getTitle() != null) {
                return tag.getTitle();
            }
        }
        return null;
    }
    
    public String getArtist() {
        for (final ID3Tags tag : this.tags) {
            if (tag.getArtist() != null) {
                return tag.getArtist();
            }
        }
        return null;
    }
    
    public String getAlbum() {
        for (final ID3Tags tag : this.tags) {
            if (tag.getAlbum() != null) {
                return tag.getAlbum();
            }
        }
        return null;
    }
    
    public String getComposer() {
        for (final ID3Tags tag : this.tags) {
            if (tag.getComposer() != null) {
                return tag.getComposer();
            }
        }
        return null;
    }
    
    public String getYear() {
        for (final ID3Tags tag : this.tags) {
            if (tag.getYear() != null) {
                return tag.getYear();
            }
        }
        return null;
    }
    
    public List<ID3Comment> getComments() {
        for (final ID3Tags tag : this.tags) {
            final List<ID3Comment> comments = tag.getComments();
            if (comments != null && comments.size() > 0) {
                return comments;
            }
        }
        return Collections.emptyList();
    }
    
    public String getGenre() {
        for (final ID3Tags tag : this.tags) {
            if (tag.getGenre() != null) {
                return tag.getGenre();
            }
        }
        return null;
    }
    
    public String getTrackNumber() {
        for (final ID3Tags tag : this.tags) {
            if (tag.getTrackNumber() != null) {
                return tag.getTrackNumber();
            }
        }
        return null;
    }
}
