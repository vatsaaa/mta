// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.parser.mp3;

import java.util.List;

public interface ID3Tags
{
    public static final String[] GENRES = { "Blues", "Classic Rock", "Country", "Dance", "Disco", "Funk", "Grunge", "Hip-Hop", "Jazz", "Metal", "New Age", "Oldies", "Other", "Pop", "R&B", "Rap", "Reggae", "Rock", "Techno", "Industrial", "Alternative", "Ska", "Death Metal", "Pranks", "Soundtrack", "Euro-Techno", "Ambient", "Trip-Hop", "Vocal", "Jazz+Funk", "Fusion", "Trance", "Classical", "Instrumental", "Acid", "House", "Game", "Sound Clip", "Gospel", "Noise", "AlternRock", "Bass", "Soul", "Punk", "Space", "Meditative", "Instrumental Pop", "Instrumental Rock", "Ethnic", "Gothic", "Darkwave", "Techno-Industrial", "Electronic", "Pop-Folk", "Eurodance", "Dream", "Southern Rock", "Comedy", "Cult", "Gangsta", "Top 40", "Christian Rap", "Pop/Funk", "Jungle", "Native American", "Cabaret", "New Wave", "Psychadelic", "Rave", "Showtunes", "Trailer", "Lo-Fi", "Tribal", "Acid Punk", "Acid Jazz", "Polka", "Retro", "Musical", "Rock & Roll", "Hard Rock", "Folk", "Folk-Rock", "National Folk", "Swing", "Fast Fusion", "Bebob", "Latin", "Revival", "Celtic", "Bluegrass", "Avantgarde", "Gothic Rock", "Progressive Rock", "Psychedelic Rock", "Symphonic Rock", "Slow Rock", "Big Band", "Chorus", "Easy Listening", "Acoustic", "Humour", "Speech", "Chanson", "Opera", "Chamber Music", "Sonata", "Symphony", "Booty Bass", "Primus", "Porn Groove", "Satire", "Slow Jam", "Club", "Tango", "Samba", "Folklore", "Ballad", "Power Ballad", "Rhythmic Soul", "Freestyle", "Duet", "Punk Rock", "Drum Solo", "A capella", "Euro-House", "Dance Hall", "" };
    
    boolean getTagsPresent();
    
    String getTitle();
    
    String getArtist();
    
    String getAlbum();
    
    String getComposer();
    
    List<ID3Comment> getComments();
    
    String getGenre();
    
    String getYear();
    
    String getTrackNumber();
    
    public static class ID3Comment
    {
        private String language;
        private String description;
        private String text;
        
        public ID3Comment(final String id3v1Text) {
            this.text = id3v1Text;
        }
        
        public ID3Comment(final String language, final String description, final String text) {
            this.language = language;
            this.description = description;
            this.text = text;
        }
        
        public String getLanguage() {
            return this.language;
        }
        
        public String getDescription() {
            return this.description;
        }
        
        public String getText() {
            return this.text;
        }
    }
}
