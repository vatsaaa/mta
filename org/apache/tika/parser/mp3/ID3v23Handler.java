// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.parser.mp3;

import org.apache.tika.exception.TikaException;
import org.xml.sax.SAXException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ID3v23Handler implements ID3Tags
{
    private String title;
    private String artist;
    private String album;
    private String year;
    private String composer;
    private String genre;
    private String trackNumber;
    private List<ID3Comment> comments;
    
    public ID3v23Handler(final ID3v2Frame frame) throws IOException, SAXException, TikaException {
        this.comments = new ArrayList<ID3Comment>();
        final ID3v2Frame.RawTagIterator tags = frame.new RawV23TagIterator();
        while (tags.hasNext()) {
            final ID3v2Frame.RawTag tag = tags.next();
            if (tag.name.equals("TIT2")) {
                this.title = this.getTagString(tag.data, 0, tag.data.length);
            }
            else if (tag.name.equals("TPE1")) {
                this.artist = this.getTagString(tag.data, 0, tag.data.length);
            }
            else if (tag.name.equals("TALB")) {
                this.album = this.getTagString(tag.data, 0, tag.data.length);
            }
            else if (tag.name.equals("TYER")) {
                this.year = this.getTagString(tag.data, 0, tag.data.length);
            }
            else if (tag.name.equals("TCOM")) {
                this.composer = this.getTagString(tag.data, 0, tag.data.length);
            }
            else if (tag.name.equals("COMM")) {
                this.comments.add(this.getComment(tag.data, 0, tag.data.length));
            }
            else if (tag.name.equals("TRCK")) {
                this.trackNumber = this.getTagString(tag.data, 0, tag.data.length);
            }
            else {
                if (!tag.name.equals("TCON")) {
                    continue;
                }
                this.genre = ID3v22Handler.extractGenre(this.getTagString(tag.data, 0, tag.data.length));
            }
        }
    }
    
    private String getTagString(final byte[] data, final int offset, final int length) {
        return ID3v2Frame.getTagString(data, offset, length);
    }
    
    private ID3Comment getComment(final byte[] data, final int offset, final int length) {
        return ID3v2Frame.getComment(data, offset, length);
    }
    
    public boolean getTagsPresent() {
        return true;
    }
    
    public String getTitle() {
        return this.title;
    }
    
    public String getArtist() {
        return this.artist;
    }
    
    public String getAlbum() {
        return this.album;
    }
    
    public String getYear() {
        return this.year;
    }
    
    public String getComposer() {
        return this.composer;
    }
    
    public List<ID3Comment> getComments() {
        return this.comments;
    }
    
    public String getGenre() {
        return this.genre;
    }
    
    public String getTrackNumber() {
        return this.trackNumber;
    }
    
    private class RawV23TagIterator extends ID3v2Frame.RawTagIterator
    {
        private RawV23TagIterator(final ID3v2Frame frame) {
            frame.super(4, 4, 1, 2);
        }
    }
}
