// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.parser.mp3;

import org.apache.tika.exception.TikaException;
import org.xml.sax.SAXException;
import java.io.IOException;
import org.xml.sax.ContentHandler;
import java.io.InputStream;

public class AudioFrame implements MP3Frame
{
    private String version;
    private int sampleRate;
    private int channels;
    
    public String getVersion() {
        return this.version;
    }
    
    public int getSampleRate() {
        return this.sampleRate;
    }
    
    public int getChannels() {
        return this.channels;
    }
    
    public static boolean isAudioHeader(final int h1, final int h2, final int h3, final int h4) {
        return h1 != -1 && h2 != -1 && h3 != -1 && h4 != -1 && (h1 == 255 && (h2 & 0x60) == 0x60);
    }
    
    public AudioFrame(final InputStream stream, final ContentHandler handler) throws IOException, SAXException, TikaException {
        this(-2, -2, -2, -2, stream);
    }
    
    public AudioFrame(int h1, int h2, int h3, int h4, final InputStream in) throws IOException {
        if (h1 == -2 && h2 == -2 && h3 == -2 && h4 == -2) {
            h1 = in.read();
            h2 = in.read();
            h3 = in.read();
            h4 = in.read();
        }
        if (isAudioHeader(h1, h2, h3, h4)) {
            this.version = "MPEG 3 Layer ";
            final int layer = h2 >> 1 & 0x3;
            if (layer == 1) {
                this.version += "III";
            }
            else if (layer == 2) {
                this.version += "II";
            }
            else if (layer == 3) {
                this.version += "I";
            }
            else {
                this.version += "(reserved)";
            }
            this.version += " Version ";
            final int ver = h2 >> 3 & 0x3;
            if (ver == 0) {
                this.version += "2.5";
            }
            else if (ver == 2) {
                this.version += "2";
            }
            else if (ver == 3) {
                this.version += "1";
            }
            else {
                this.version += "(reseved)";
            }
            final int rate = h3 >> 2 & 0x3;
            switch (rate) {
                case 0: {
                    this.sampleRate = 11025;
                    break;
                }
                case 1: {
                    this.sampleRate = 12000;
                    break;
                }
                default: {
                    this.sampleRate = 8000;
                    break;
                }
            }
            if (ver == 2) {
                this.sampleRate *= 2;
            }
            else if (ver == 3) {
                this.sampleRate *= 4;
            }
            final int chans = h4 & 0x3;
            if (chans < 3) {
                this.channels = 2;
            }
            else {
                this.channels = 1;
            }
            return;
        }
        throw new IllegalArgumentException("Magic Audio Frame Header not found");
    }
}
