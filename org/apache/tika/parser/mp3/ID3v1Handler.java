// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.parser.mp3;

import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.List;
import org.apache.tika.exception.TikaException;
import org.xml.sax.SAXException;
import java.io.IOException;
import org.xml.sax.ContentHandler;
import java.io.InputStream;

public class ID3v1Handler implements ID3Tags
{
    private String title;
    private String artist;
    private String album;
    private String year;
    private ID3Comment comment;
    private String genre;
    private String trackNumber;
    boolean found;
    
    public ID3v1Handler(final InputStream stream, final ContentHandler handler) throws IOException, SAXException, TikaException {
        this(LyricsHandler.getSuffix(stream, 128));
    }
    
    protected ID3v1Handler(final byte[] tagData) throws IOException, SAXException, TikaException {
        this.found = false;
        if (tagData.length == 128 && tagData[0] == 84 && tagData[1] == 65 && tagData[2] == 71) {
            this.found = true;
            this.title = getString(tagData, 3, 33);
            this.artist = getString(tagData, 33, 63);
            this.album = getString(tagData, 63, 93);
            this.year = getString(tagData, 93, 97);
            final String commentStr = getString(tagData, 97, 127);
            this.comment = new ID3Comment(commentStr);
            final int genreID = tagData[127] & 0xFF;
            this.genre = ID3v1Handler.GENRES[Math.min(genreID, ID3v1Handler.GENRES.length - 1)];
            if (tagData[125] == 0 && tagData[126] != 0) {
                final int trackNum = tagData[126] & 0xFF;
                this.trackNumber = Integer.toString(trackNum);
            }
        }
    }
    
    public boolean getTagsPresent() {
        return this.found;
    }
    
    public String getTitle() {
        return this.title;
    }
    
    public String getArtist() {
        return this.artist;
    }
    
    public String getAlbum() {
        return this.album;
    }
    
    public String getYear() {
        return this.year;
    }
    
    public List<ID3Comment> getComments() {
        return Arrays.asList(this.comment);
    }
    
    public String getGenre() {
        return this.genre;
    }
    
    public String getTrackNumber() {
        return this.trackNumber;
    }
    
    public String getComposer() {
        return null;
    }
    
    private static String getString(final byte[] buffer, int start, int end) throws TikaException {
        int zero;
        for (zero = start; zero < end && buffer[zero] != 0; ++zero) {}
        for (end = zero; start < end && buffer[end - 1] <= 32; --end) {}
        while (start < end && buffer[start] <= 32) {
            ++start;
        }
        try {
            return new String(buffer, start, end - start, "ISO-8859-1");
        }
        catch (UnsupportedEncodingException e) {
            throw new TikaException("ISO-8859-1 encoding is not available", e);
        }
    }
}
