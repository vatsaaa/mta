// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.parser.mp3;

import java.util.Iterator;
import java.io.UnsupportedEncodingException;
import java.io.IOException;
import java.io.InputStream;

public class ID3v2Frame implements MP3Frame
{
    private int majorVersion;
    private int minorVersion;
    private int flags;
    private int length;
    private byte[] extendedHeader;
    private byte[] data;
    protected static final TextEncoding[] encodings;
    
    public int getMajorVersion() {
        return this.majorVersion;
    }
    
    public int getMinorVersion() {
        return this.minorVersion;
    }
    
    public int getFlags() {
        return this.flags;
    }
    
    public int getLength() {
        return this.length;
    }
    
    public byte[] getExtendedHeader() {
        return this.extendedHeader;
    }
    
    public byte[] getData() {
        return this.data;
    }
    
    public static MP3Frame createFrameIfPresent(final InputStream inp) throws IOException {
        final int h1 = inp.read();
        final int h2 = inp.read();
        final int h3 = inp.read();
        if (h1 == 73 && h2 == 68 && h3 == 51) {
            final int majorVersion = inp.read();
            final int minorVersion = inp.read();
            if (majorVersion == -1 || minorVersion == -1) {
                return null;
            }
            return new ID3v2Frame(majorVersion, minorVersion, inp);
        }
        else {
            final int h4 = inp.read();
            if (AudioFrame.isAudioHeader(h1, h2, h3, h4)) {
                return new AudioFrame(h1, h2, h3, h4, inp);
            }
            return null;
        }
    }
    
    private ID3v2Frame(final int majorVersion, final int minorVersion, final InputStream inp) throws IOException {
        this.majorVersion = majorVersion;
        this.minorVersion = minorVersion;
        this.flags = inp.read();
        this.length = get7BitsInt(readFully(inp, 4), 0);
        if ((this.flags & 0x2) == 0x2) {
            final int size = getInt(readFully(inp, 4));
            this.extendedHeader = readFully(inp, size);
        }
        this.data = readFully(inp, this.length, false);
    }
    
    protected static int getInt(final byte[] data) {
        return getInt(data, 0);
    }
    
    protected static int getInt(final byte[] data, final int offset) {
        final int b0 = data[offset + 0] & 0xFF;
        final int b2 = data[offset + 1] & 0xFF;
        final int b3 = data[offset + 2] & 0xFF;
        final int b4 = data[offset + 3] & 0xFF;
        return (b0 << 24) + (b2 << 16) + (b3 << 8) + (b4 << 0);
    }
    
    protected static int getInt3(final byte[] data, final int offset) {
        final int b0 = data[offset + 0] & 0xFF;
        final int b2 = data[offset + 1] & 0xFF;
        final int b3 = data[offset + 2] & 0xFF;
        return (b0 << 16) + (b2 << 8) + (b3 << 0);
    }
    
    protected static int getInt2(final byte[] data, final int offset) {
        final int b0 = data[offset + 0] & 0xFF;
        final int b2 = data[offset + 1] & 0xFF;
        return (b0 << 8) + (b2 << 0);
    }
    
    protected static int get7BitsInt(final byte[] data, final int offset) {
        final int b0 = data[offset + 0] & 0x7F;
        final int b2 = data[offset + 1] & 0x7F;
        final int b3 = data[offset + 2] & 0x7F;
        final int b4 = data[offset + 3] & 0x7F;
        return (b0 << 21) + (b2 << 14) + (b3 << 7) + (b4 << 0);
    }
    
    protected static byte[] readFully(final InputStream inp, final int length) throws IOException {
        return readFully(inp, length, true);
    }
    
    protected static byte[] readFully(final InputStream inp, final int length, final boolean shortDataIsFatal) throws IOException {
        final byte[] b = new byte[length];
        int pos = 0;
        while (pos < length) {
            final int read = inp.read(b, pos, length - pos);
            if (read == -1) {
                if (shortDataIsFatal) {
                    throw new IOException("Tried to read " + length + " bytes, but only " + pos + " bytes present");
                }
                return b;
            }
            else {
                pos += read;
            }
        }
        return b;
    }
    
    protected static String getTagString(final byte[] data, int offset, final int length) {
        int actualLength = length;
        if (actualLength == 0) {
            return "";
        }
        if (actualLength == 1 && data[offset] == 0) {
            return "";
        }
        TextEncoding encoding = ID3v2Frame.encodings[0];
        final byte maybeEncodingFlag = data[offset];
        if (maybeEncodingFlag >= 0 && maybeEncodingFlag < ID3v2Frame.encodings.length) {
            ++offset;
            --actualLength;
            encoding = ID3v2Frame.encodings[maybeEncodingFlag];
        }
        while (encoding.doubleByte && actualLength >= 2 && data[offset + actualLength - 1] == 0 && data[offset + actualLength - 2] == 0) {
            actualLength -= 2;
        }
        while (!encoding.doubleByte && actualLength >= 1 && data[offset + actualLength - 1] == 0) {
            --actualLength;
        }
        if (actualLength == 0) {
            return "";
        }
        try {
            return new String(data, offset, actualLength, encoding.encoding);
        }
        catch (UnsupportedEncodingException e) {
            throw new RuntimeException("Core encoding " + encoding.encoding + " is not available", e);
        }
    }
    
    protected static ID3Tags.ID3Comment getComment(final byte[] data, final int offset, final int length) {
        final int encodingFlag = data[offset];
        if (encodingFlag < 0 || encodingFlag >= ID3v2Frame.encodings.length) {
            return null;
        }
        final TextEncoding encoding = ID3v2Frame.encodings[encodingFlag];
        final String lang = getString(data, offset + 1, 3);
        final int descStart = offset + 4;
        int textStart = -1;
        String description = null;
        String text = null;
        try {
            for (int i = descStart; i < offset + length; ++i) {
                if (encoding.doubleByte && data[i] == 0 && data[i + 1] == 0) {
                    if (i + 2 < offset + length && data[i + 1] == 0 && data[i + 2] == 0) {
                        ++i;
                    }
                    textStart = i + 2;
                    description = new String(data, descStart, i - descStart, encoding.encoding);
                    break;
                }
                if (!encoding.doubleByte && data[i] == 0) {
                    textStart = i + 1;
                    description = new String(data, descStart, i - descStart, encoding.encoding);
                    break;
                }
            }
            if (textStart > -1) {
                text = new String(data, textStart, offset + length - textStart, encoding.encoding);
            }
            else {
                text = new String(data, descStart, offset + length - descStart, encoding.encoding);
            }
            return new ID3Tags.ID3Comment(lang, description, text);
        }
        catch (UnsupportedEncodingException e) {
            throw new RuntimeException("Core encoding " + encoding.encoding + " is not available", e);
        }
    }
    
    protected static String getString(final byte[] data, final int offset, final int length) {
        try {
            return new String(data, offset, length, "ISO-8859-1");
        }
        catch (UnsupportedEncodingException e) {
            throw new RuntimeException("Core encoding ISO-8859-1 encoding is not available", e);
        }
    }
    
    static {
        encodings = new TextEncoding[] { new TextEncoding("ISO-8859-1", false), new TextEncoding("UTF-16", true), new TextEncoding("UTF-16BE", true), new TextEncoding("UTF-8", false) };
    }
    
    protected static class TextEncoding
    {
        public final boolean doubleByte;
        public final String encoding;
        
        private TextEncoding(final String encoding, final boolean doubleByte) {
            this.doubleByte = doubleByte;
            this.encoding = encoding;
        }
    }
    
    protected class RawTagIterator implements Iterator<RawTag>
    {
        private int nameLength;
        private int sizeLength;
        private int sizeMultiplier;
        private int flagLength;
        private int offset;
        
        protected RawTagIterator(final int nameLength, final int sizeLength, final int sizeMultiplier, final int flagLength) {
            this.offset = 0;
            this.nameLength = nameLength;
            this.sizeLength = sizeLength;
            this.sizeMultiplier = sizeMultiplier;
            this.flagLength = flagLength;
        }
        
        public boolean hasNext() {
            return this.offset < ID3v2Frame.this.data.length && ID3v2Frame.this.data[this.offset] != 0;
        }
        
        public RawTag next() {
            final RawTag tag = new RawTag(this.nameLength, this.sizeLength, this.sizeMultiplier, this.flagLength, ID3v2Frame.this.data, this.offset);
            this.offset += tag.getSize();
            return tag;
        }
        
        public void remove() {
        }
    }
    
    protected static class RawTag
    {
        private int headerSize;
        protected String name;
        protected int flag;
        protected byte[] data;
        
        private RawTag(final int nameLength, final int sizeLength, final int sizeMultiplier, final int flagLength, final byte[] frameData, final int offset) {
            this.headerSize = nameLength + sizeLength + flagLength;
            this.name = ID3v2Frame.getString(frameData, offset, nameLength);
            int rawSize;
            if (sizeLength == 3) {
                rawSize = ID3v2Frame.getInt3(frameData, offset + nameLength);
            }
            else {
                rawSize = ID3v2Frame.getInt(frameData, offset + nameLength);
            }
            int size = rawSize * sizeMultiplier;
            if (flagLength > 0) {
                if (flagLength == 1) {
                    this.flag = frameData[offset + nameLength + sizeLength];
                }
                else {
                    this.flag = ID3v2Frame.getInt2(frameData, offset + nameLength + sizeLength);
                }
            }
            final int copyFrom = offset + nameLength + sizeLength + flagLength;
            size = Math.min(size, frameData.length - copyFrom);
            System.arraycopy(frameData, copyFrom, this.data = new byte[size], 0, size);
        }
        
        protected int getSize() {
            return this.headerSize + this.data.length;
        }
    }
}
