// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.parser.dwg;

import org.apache.tika.metadata.TikaCoreProperties;
import org.apache.poi.util.StringUtil;
import org.apache.tika.io.EndianUtils;
import org.xml.sax.SAXException;
import java.io.IOException;
import org.apache.tika.exception.TikaException;
import org.apache.tika.sax.XHTMLContentHandler;
import org.apache.poi.util.IOUtils;
import org.apache.tika.metadata.Metadata;
import org.xml.sax.ContentHandler;
import java.io.InputStream;
import java.util.Collections;
import java.util.Set;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.metadata.Property;
import org.apache.tika.mime.MediaType;
import org.apache.tika.parser.AbstractParser;

public class DWGParser extends AbstractParser
{
    private static final long serialVersionUID = -7744232583079169119L;
    private static MediaType TYPE;
    private static final Property[] HEADER_PROPERTIES_ENTRIES;
    private static final Property[] HEADER_2000_PROPERTIES_ENTRIES;
    private static final String HEADER_2000_PROPERTIES_MARKER_STR = "DWGPROPS COOKIE";
    private static final byte[] HEADER_2000_PROPERTIES_MARKER;
    private static final int CUSTOM_PROPERTIES_SKIP = 20;
    
    public Set<MediaType> getSupportedTypes(final ParseContext context) {
        return Collections.singleton(DWGParser.TYPE);
    }
    
    public void parse(final InputStream stream, final ContentHandler handler, final Metadata metadata, final ParseContext context) throws IOException, TikaException, SAXException {
        final byte[] header = new byte[128];
        IOUtils.readFully(stream, header);
        final String version = new String(header, 0, 6, "US-ASCII");
        final XHTMLContentHandler xhtml = new XHTMLContentHandler(handler, metadata);
        xhtml.startDocument();
        if (version.equals("AC1015")) {
            metadata.set("Content-Type", DWGParser.TYPE.toString());
            if (this.skipTo2000PropertyInfoSection(stream, header)) {
                this.get2000Props(stream, metadata, xhtml);
            }
        }
        else if (version.equals("AC1018")) {
            metadata.set("Content-Type", DWGParser.TYPE.toString());
            if (this.skipToPropertyInfoSection(stream, header)) {
                this.get2004Props(stream, metadata, xhtml);
            }
        }
        else {
            if (!version.equals("AC1021") && !version.equals("AC1024")) {
                throw new TikaException("Unsupported AutoCAD drawing version: " + version);
            }
            metadata.set("Content-Type", DWGParser.TYPE.toString());
            if (this.skipToPropertyInfoSection(stream, header)) {
                this.get2007and2010Props(stream, metadata, xhtml);
            }
        }
        xhtml.endDocument();
    }
    
    private void get2004Props(final InputStream stream, final Metadata metadata, final XHTMLContentHandler xhtml) throws IOException, TikaException, SAXException {
        for (int i = 0; i < DWGParser.HEADER_PROPERTIES_ENTRIES.length; ++i) {
            final String headerValue = this.read2004String(stream);
            this.handleHeader(i, headerValue, metadata, xhtml);
        }
        for (int customCount = this.skipToCustomProperties(stream), j = 0; j < customCount; ++j) {
            final String propName = this.read2004String(stream);
            final String propValue = this.read2004String(stream);
            if (propName.length() > 0 && propValue.length() > 0) {
                metadata.add(propName, propValue);
            }
        }
    }
    
    private String read2004String(final InputStream stream) throws IOException, TikaException {
        int stringLen = EndianUtils.readUShortLE(stream);
        final byte[] stringData = new byte[stringLen];
        IOUtils.readFully(stream, stringData);
        if (stringData[stringLen - 1] == 0) {
            --stringLen;
        }
        final String value = StringUtil.getFromCompressedUnicode(stringData, 0, stringLen);
        return value;
    }
    
    private void get2007and2010Props(final InputStream stream, final Metadata metadata, final XHTMLContentHandler xhtml) throws IOException, TikaException, SAXException {
        for (int i = 0; i < DWGParser.HEADER_PROPERTIES_ENTRIES.length; ++i) {
            final String headerValue = this.read2007and2010String(stream);
            this.handleHeader(i, headerValue, metadata, xhtml);
        }
        for (int customCount = this.skipToCustomProperties(stream), j = 0; j < customCount; ++j) {
            final String propName = this.read2007and2010String(stream);
            final String propValue = this.read2007and2010String(stream);
            if (propName.length() > 0 && propValue.length() > 0) {
                metadata.add(propName, propValue);
            }
        }
    }
    
    private String read2007and2010String(final InputStream stream) throws IOException, TikaException {
        final int stringLen = EndianUtils.readUShortLE(stream);
        final byte[] stringData = new byte[stringLen * 2];
        IOUtils.readFully(stream, stringData);
        String value = StringUtil.getFromUnicodeLE(stringData);
        if (value.charAt(value.length() - 1) == '\0') {
            value = value.substring(0, value.length() - 1);
        }
        return value;
    }
    
    private void get2000Props(final InputStream stream, final Metadata metadata, final XHTMLContentHandler xhtml) throws IOException, TikaException, SAXException {
        for (int propCount = 0; propCount < 30; ++propCount) {
            final int propIdx = EndianUtils.readUShortLE(stream);
            int length = EndianUtils.readUShortLE(stream);
            final int valueType = stream.read();
            if (propIdx == 40) {
                length = 25;
            }
            else if (propIdx == 90) {
                break;
            }
            final byte[] value = new byte[length];
            IOUtils.readFully(stream, value);
            if (valueType == 30) {
                final String val = StringUtil.getFromCompressedUnicode(value, 0, length);
                if (propIdx < DWGParser.HEADER_2000_PROPERTIES_ENTRIES.length) {
                    metadata.add(DWGParser.HEADER_2000_PROPERTIES_ENTRIES[propIdx], val);
                    xhtml.element("p", val);
                }
                else if (propIdx == 300) {
                    final int splitAt = val.indexOf(61);
                    if (splitAt > -1) {
                        final String propName = val.substring(0, splitAt);
                        final String propVal = val.substring(splitAt + 1);
                        metadata.add(propName, propVal);
                    }
                }
            }
        }
    }
    
    private void handleHeader(final int headerNumber, final String value, final Metadata metadata, final XHTMLContentHandler xhtml) throws SAXException {
        if (value == null || value.length() == 0) {
            return;
        }
        final Property headerProp = DWGParser.HEADER_PROPERTIES_ENTRIES[headerNumber];
        if (headerProp != null) {
            metadata.set(headerProp, value);
        }
        xhtml.element("p", value);
    }
    
    private boolean skipToPropertyInfoSection(final InputStream stream, final byte[] header) throws IOException, TikaException {
        long offsetToSection = EndianUtils.getLongLE(header, 32);
        if (offsetToSection > 10485760L) {
            offsetToSection = 0L;
        }
        long toSkip = offsetToSection - header.length;
        if (offsetToSection == 0L) {
            return false;
        }
        while (toSkip > 0L) {
            final byte[] skip = new byte[Math.min((int)toSkip, 16384)];
            IOUtils.readFully(stream, skip);
            toSkip -= skip.length;
        }
        return true;
    }
    
    private boolean skipTo2000PropertyInfoSection(final InputStream stream, final byte[] header) throws IOException {
        int val = 0;
        while (val != -1) {
            val = stream.read();
            if (val == DWGParser.HEADER_2000_PROPERTIES_MARKER[0]) {
                boolean going = true;
                for (int i = 1; i < DWGParser.HEADER_2000_PROPERTIES_MARKER.length && going; ++i) {
                    val = stream.read();
                    if (val != DWGParser.HEADER_2000_PROPERTIES_MARKER[i]) {
                        going = false;
                    }
                }
                if (going) {
                    return true;
                }
                continue;
            }
        }
        return false;
    }
    
    private int skipToCustomProperties(final InputStream stream) throws IOException, TikaException {
        byte[] padding = new byte[4];
        IOUtils.readFully(stream, padding);
        if (padding[0] != 0 || padding[1] != 0 || padding[2] != 0 || padding[3] != 0) {
            return 0;
        }
        padding = new byte[20];
        IOUtils.readFully(stream, padding);
        final int count = EndianUtils.readUShortLE(stream);
        if (count > 0 && count < 127) {
            return count;
        }
        return 0;
    }
    
    static {
        DWGParser.TYPE = MediaType.image("vnd.dwg");
        HEADER_PROPERTIES_ENTRIES = new Property[] { TikaCoreProperties.TITLE, TikaCoreProperties.TRANSITION_SUBJECT_TO_DC_DESCRIPTION, TikaCoreProperties.CREATOR, TikaCoreProperties.TRANSITION_KEYWORDS_TO_DC_SUBJECT, TikaCoreProperties.COMMENTS, TikaCoreProperties.MODIFIER, null, TikaCoreProperties.RELATION };
        HEADER_2000_PROPERTIES_ENTRIES = new Property[] { null, TikaCoreProperties.RELATION, TikaCoreProperties.TITLE, TikaCoreProperties.TRANSITION_SUBJECT_TO_DC_DESCRIPTION, TikaCoreProperties.CREATOR, null, TikaCoreProperties.COMMENTS, TikaCoreProperties.TRANSITION_KEYWORDS_TO_DC_SUBJECT, TikaCoreProperties.MODIFIER };
        StringUtil.putCompressedUnicode("DWGPROPS COOKIE", HEADER_2000_PROPERTIES_MARKER = new byte["DWGPROPS COOKIE".length()], 0);
    }
}
