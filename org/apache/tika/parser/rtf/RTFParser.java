// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.parser.rtf;

import java.util.Collections;
import org.xml.sax.SAXException;
import java.io.IOException;
import org.apache.tika.exception.TikaException;
import org.apache.tika.sax.XHTMLContentHandler;
import org.apache.tika.io.TaggedInputStream;
import org.apache.tika.metadata.Metadata;
import org.xml.sax.ContentHandler;
import java.io.InputStream;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.mime.MediaType;
import java.util.Set;
import org.apache.tika.parser.AbstractParser;

public class RTFParser extends AbstractParser
{
    private static final long serialVersionUID = -4165069489372320313L;
    private static final Set<MediaType> SUPPORTED_TYPES;
    
    public Set<MediaType> getSupportedTypes(final ParseContext context) {
        return RTFParser.SUPPORTED_TYPES;
    }
    
    public void parse(final InputStream stream, final ContentHandler handler, final Metadata metadata, final ParseContext context) throws IOException, SAXException, TikaException {
        final TaggedInputStream tagged = new TaggedInputStream(stream);
        try {
            final TextExtractor ert = new TextExtractor(new XHTMLContentHandler(handler, metadata), metadata);
            ert.extract(stream);
            metadata.add("Content-Type", "application/rtf");
        }
        catch (IOException e) {
            tagged.throwIfCauseOf(e);
            throw new TikaException("Error parsing an RTF document", e);
        }
    }
    
    static {
        SUPPORTED_TYPES = Collections.singleton(MediaType.application("rtf"));
    }
}
