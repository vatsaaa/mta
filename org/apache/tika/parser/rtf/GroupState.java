// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.parser.rtf;

import java.nio.charset.Charset;

class GroupState
{
    public int depth;
    public boolean bold;
    public boolean italic;
    public boolean ignore;
    public int ucSkip;
    public Charset fontCharset;
    
    public GroupState() {
        this.ucSkip = 1;
    }
    
    public GroupState(final GroupState other) {
        this.ucSkip = 1;
        this.bold = other.bold;
        this.italic = other.italic;
        this.ignore = other.ignore;
        this.ucSkip = other.ucSkip;
        this.fontCharset = other.fontCharset;
        this.depth = 1 + other.depth;
    }
}
