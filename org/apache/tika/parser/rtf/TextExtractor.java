// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.parser.rtf;

import org.apache.tika.metadata.OfficeOpenXMLExtended;
import org.apache.tika.metadata.OfficeOpenXMLCore;
import org.apache.tika.metadata.TikaCoreProperties;
import java.nio.charset.CodingErrorAction;
import java.nio.charset.CoderResult;
import java.io.PushbackInputStream;
import java.io.InputStream;
import org.apache.tika.exception.TikaException;
import org.xml.sax.SAXException;
import java.io.IOException;
import java.util.HashMap;
import org.apache.tika.utils.CharsetUtils;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.sax.XHTMLContentHandler;
import org.apache.tika.metadata.Property;
import java.util.LinkedList;
import java.util.Map;
import java.nio.charset.CharsetDecoder;
import java.nio.CharBuffer;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;

final class TextExtractor
{
    private static final Charset ASCII;
    private static final Charset WINDOWS_1252;
    private static final Charset MAC_ROMAN;
    private static final Charset SHIFT_JIS;
    private static final Charset WINDOWS_57011;
    private static final Charset WINDOWS_57010;
    private static final Charset WINDOWS_57009;
    private static final Charset WINDOWS_57008;
    private static final Charset WINDOWS_57007;
    private static final Charset WINDOWS_57006;
    private static final Charset WINDOWS_57005;
    private static final Charset WINDOWS_57004;
    private static final Charset WINDOWS_57003;
    private static final Charset X_ISCII91;
    private static final Charset X_MAC_CENTRAL_EUROPE;
    private static final Charset MAC_CYRILLIC;
    private static final Charset X_JOHAB;
    private static final Charset CP12582;
    private static final Charset CP12572;
    private static final Charset CP12562;
    private static final Charset CP12552;
    private static final Charset CP12542;
    private static final Charset CP12532;
    private static final Charset CP1252;
    private static final Charset CP12512;
    private static final Charset CP12502;
    private static final Charset CP950;
    private static final Charset CP949;
    private static final Charset MS9362;
    private static final Charset MS8742;
    private static final Charset CP866;
    private static final Charset CP865;
    private static final Charset CP864;
    private static final Charset CP863;
    private static final Charset CP862;
    private static final Charset CP860;
    private static final Charset CP852;
    private static final Charset CP8502;
    private static final Charset CP819;
    private static final Charset WINDOWS_720;
    private static final Charset WINDOWS_711;
    private static final Charset WINDOWS_710;
    private static final Charset WINDOWS_709;
    private static final Charset ISO_8859_6;
    private static final Charset CP4372;
    private static final Charset CP850;
    private static final Charset CP437;
    private static final Charset MS874;
    private static final Charset CP1257;
    private static final Charset CP1256;
    private static final Charset CP1255;
    private static final Charset CP1258;
    private static final Charset CP1254;
    private static final Charset CP1253;
    private static final Charset MS950;
    private static final Charset MS936;
    private static final Charset MS1361;
    private static final Charset MS932;
    private static final Charset CP1251;
    private static final Charset CP1250;
    private static final Charset MAC_THAI;
    private static final Charset MAC_TURKISH;
    private static final Charset MAC_GREEK;
    private static final Charset MAC_ARABIC;
    private static final Charset MAC_HEBREW;
    private static final Charset JOHAB;
    private static final Charset BIG5;
    private static final Charset GB2312;
    private static final Charset MS949;
    private byte[] pendingBytes;
    private int pendingByteCount;
    private ByteBuffer pendingByteBuffer;
    private char[] pendingChars;
    private int pendingCharCount;
    private byte[] pendingControl;
    private int pendingControlCount;
    private final char[] outputArray;
    private final CharBuffer outputBuffer;
    private CharsetDecoder decoder;
    private Charset lastCharset;
    private Charset globalCharset;
    private int globalDefaultFont;
    private int curFontID;
    private final Map<Integer, Charset> fontToCharset;
    private final LinkedList<GroupState> groupStates;
    private GroupState groupState;
    private boolean inHeader;
    private int fontTableState;
    private int fontTableDepth;
    private Property nextMetaData;
    private boolean inParagraph;
    private int fieldState;
    private String pendingURL;
    private final StringBuilder pendingBuffer;
    private int uprState;
    private final XHTMLContentHandler out;
    private final Metadata metadata;
    int ansiSkip;
    private static final Map<Integer, Charset> FCHARSET_MAP;
    private static final Map<Integer, Charset> ANSICPG_MAP;
    
    private static Charset getCharset(final String name) {
        try {
            return CharsetUtils.forName(name);
        }
        catch (Exception e) {
            return TextExtractor.ASCII;
        }
    }
    
    public TextExtractor(final XHTMLContentHandler out, final Metadata metadata) {
        this.pendingBytes = new byte[16];
        this.pendingByteBuffer = ByteBuffer.wrap(this.pendingBytes);
        this.pendingChars = new char[10];
        this.pendingControl = new byte[10];
        this.outputArray = new char[128];
        this.outputBuffer = CharBuffer.wrap(this.outputArray);
        this.globalCharset = TextExtractor.WINDOWS_1252;
        this.globalDefaultFont = -1;
        this.curFontID = -1;
        this.fontToCharset = new HashMap<Integer, Charset>();
        this.groupStates = new LinkedList<GroupState>();
        this.groupState = new GroupState();
        this.inHeader = true;
        this.pendingBuffer = new StringBuilder();
        this.uprState = -1;
        this.ansiSkip = 0;
        this.metadata = metadata;
        this.out = out;
    }
    
    private static boolean isHexChar(final int ch) {
        return (ch >= 48 && ch <= 57) || (ch >= 97 && ch <= 102) || (ch >= 65 && ch <= 70);
    }
    
    private static boolean isAlpha(final int ch) {
        return (ch >= 97 && ch <= 122) || (ch >= 65 && ch <= 90);
    }
    
    private static boolean isDigit(final int ch) {
        return ch >= 48 && ch <= 57;
    }
    
    private static int hexValue(final int ch) {
        if (ch >= 48 && ch <= 57) {
            return ch - 48;
        }
        if (ch >= 97 && ch <= 122) {
            return 10 + (ch - 97);
        }
        assert ch >= 65 && ch <= 90;
        return 10 + (ch - 65);
    }
    
    private void pushText() throws IOException, SAXException, TikaException {
        if (this.pendingByteCount != 0) {
            assert this.pendingCharCount == 0;
            this.pushBytes();
        }
        else {
            this.pushChars();
        }
    }
    
    private void addOutputByte(final int b) throws IOException, SAXException, TikaException {
        assert b >= 0 && b < 256 : "byte value out of range: " + b;
        if (this.pendingCharCount != 0) {
            this.pushChars();
        }
        if (this.pendingByteCount == this.pendingBytes.length) {
            final byte[] newArray = new byte[(int)(this.pendingBytes.length * 1.25)];
            System.arraycopy(this.pendingBytes, 0, newArray, 0, this.pendingBytes.length);
            this.pendingBytes = newArray;
            this.pendingByteBuffer = ByteBuffer.wrap(this.pendingBytes);
        }
        this.pendingBytes[this.pendingByteCount++] = (byte)b;
    }
    
    private void addControl(final int b) {
        assert isAlpha(b);
        if (this.pendingControlCount == this.pendingControl.length) {
            final byte[] newArray = new byte[(int)(this.pendingControl.length * 1.25)];
            System.arraycopy(this.pendingControl, 0, newArray, 0, this.pendingControl.length);
            this.pendingControl = newArray;
        }
        this.pendingControl[this.pendingControlCount++] = (byte)b;
    }
    
    private void addOutputChar(final char ch) throws IOException, SAXException, TikaException {
        if (this.pendingByteCount != 0) {
            this.pushBytes();
        }
        if (this.inHeader || this.fieldState == 1) {
            this.pendingBuffer.append(ch);
        }
        else {
            if (this.pendingCharCount == this.pendingChars.length) {
                final char[] newArray = new char[(int)(this.pendingChars.length * 1.25)];
                System.arraycopy(this.pendingChars, 0, newArray, 0, this.pendingChars.length);
                this.pendingChars = newArray;
            }
            this.pendingChars[this.pendingCharCount++] = ch;
        }
    }
    
    public void extract(final InputStream in) throws IOException, SAXException, TikaException {
        this.extract(new PushbackInputStream(in, 2));
    }
    
    private void extract(final PushbackInputStream in) throws IOException, SAXException, TikaException {
        this.out.startDocument();
        while (true) {
            final int b = in.read();
            if (b == -1) {
                break;
            }
            if (b == 92) {
                this.parseControlToken(in);
            }
            else if (b == 123) {
                this.pushText();
                this.processGroupStart(in);
            }
            else if (b == 125) {
                this.pushText();
                this.processGroupEnd();
                if (this.groupStates.isEmpty()) {
                    break;
                }
                continue;
            }
            else {
                if (b == 13 || b == 10 || (this.groupState.ignore && this.nextMetaData == null)) {
                    continue;
                }
                if (this.ansiSkip != 0) {
                    --this.ansiSkip;
                }
                else {
                    this.addOutputByte(b);
                }
            }
        }
        this.endParagraph(false);
        this.out.endDocument();
    }
    
    private void parseControlToken(final PushbackInputStream in) throws IOException, SAXException, TikaException {
        final int b = in.read();
        if (b == 39) {
            this.parseHexChar(in);
        }
        else if (isAlpha(b)) {
            this.parseControlWord((char)b, in);
        }
        else if (b == 123 || b == 125 || b == 92 || b == 13 || b == 10) {
            this.addOutputByte(b);
        }
        else if (b != -1) {
            this.processControlSymbol((char)b);
        }
    }
    
    private void parseHexChar(final PushbackInputStream in) throws IOException, SAXException, TikaException {
        final int hex1 = in.read();
        if (!isHexChar(hex1)) {
            in.unread(hex1);
            return;
        }
        final int hex2 = in.read();
        if (!isHexChar(hex2)) {
            in.unread(hex2);
            return;
        }
        if (this.ansiSkip != 0) {
            --this.ansiSkip;
        }
        else {
            this.addOutputByte(16 * hexValue(hex1) + hexValue(hex2));
        }
    }
    
    private void parseControlWord(final int firstChar, final PushbackInputStream in) throws IOException, SAXException, TikaException {
        this.addControl(firstChar);
        int b;
        for (b = in.read(); isAlpha(b); b = in.read()) {
            this.addControl(b);
        }
        boolean hasParam = false;
        boolean negParam = false;
        if (b == 45) {
            negParam = true;
            hasParam = true;
            b = in.read();
        }
        int param = 0;
        while (isDigit(b)) {
            param *= 10;
            param += b - 48;
            hasParam = true;
            b = in.read();
        }
        if (b != 32) {
            in.unread(b);
        }
        if (hasParam) {
            if (negParam) {
                param = -param;
            }
            this.processControlWord(param, in);
        }
        else {
            this.processControlWord();
        }
        this.pendingControlCount = 0;
    }
    
    private void lazyStartParagraph() throws IOException, SAXException, TikaException {
        if (!this.inParagraph) {
            if (this.groupState.italic) {
                this.end("i");
            }
            if (this.groupState.bold) {
                this.end("b");
            }
            this.out.startElement("p");
            if (this.groupState.bold) {
                this.start("b");
            }
            if (this.groupState.italic) {
                this.start("i");
            }
            this.inParagraph = true;
        }
    }
    
    private void endParagraph(final boolean preserveStyles) throws IOException, SAXException, TikaException {
        this.pushText();
        if (this.inParagraph) {
            if (this.groupState.italic) {
                this.end("i");
                this.groupState.italic = preserveStyles;
            }
            if (this.groupState.bold) {
                this.end("b");
                this.groupState.bold = preserveStyles;
            }
            this.out.endElement("p");
            if (preserveStyles && (this.groupState.bold || this.groupState.italic)) {
                this.start("p");
                if (this.groupState.bold) {
                    this.start("b");
                }
                if (this.groupState.italic) {
                    this.start("i");
                }
                this.inParagraph = true;
            }
            else {
                this.inParagraph = false;
            }
        }
    }
    
    private void pushChars() throws IOException, SAXException, TikaException {
        if (this.pendingCharCount != 0) {
            this.lazyStartParagraph();
            this.out.characters(this.pendingChars, 0, this.pendingCharCount);
            this.pendingCharCount = 0;
        }
    }
    
    private void pushBytes() throws IOException, SAXException, TikaException {
        if (this.pendingByteCount > 0 && (!this.groupState.ignore || this.nextMetaData != null)) {
            final CharsetDecoder decoder = this.getDecoder();
            this.pendingByteBuffer.limit(this.pendingByteCount);
            assert this.pendingByteBuffer.position() == 0;
            assert this.outputBuffer.position() == 0;
            CoderResult result;
            do {
                result = decoder.decode(this.pendingByteBuffer, this.outputBuffer, true);
                final int pos = this.outputBuffer.position();
                if (pos > 0) {
                    if (this.inHeader || this.fieldState == 1) {
                        this.pendingBuffer.append(this.outputArray, 0, pos);
                    }
                    else {
                        this.lazyStartParagraph();
                        this.out.characters(this.outputArray, 0, pos);
                    }
                    this.outputBuffer.position(0);
                }
            } while (result != CoderResult.UNDERFLOW);
            do {
                result = decoder.flush(this.outputBuffer);
                final int pos = this.outputBuffer.position();
                if (pos > 0) {
                    if (this.inHeader || this.fieldState == 1) {
                        this.pendingBuffer.append(this.outputArray, 0, pos);
                    }
                    else {
                        this.lazyStartParagraph();
                        this.out.characters(this.outputArray, 0, pos);
                    }
                    this.outputBuffer.position(0);
                }
            } while (result != CoderResult.UNDERFLOW);
            decoder.reset();
            this.pendingByteBuffer.position(0);
        }
        this.pendingByteCount = 0;
    }
    
    private boolean equals(final String s) {
        if (this.pendingControlCount != s.length()) {
            return false;
        }
        for (int idx = 0; idx < this.pendingControlCount; ++idx) {
            assert isAlpha(s.charAt(idx));
            if ((byte)s.charAt(idx) != this.pendingControl[idx]) {
                return false;
            }
        }
        return true;
    }
    
    private void processControlSymbol(final char ch) throws IOException, SAXException, TikaException {
        switch (ch) {
            case '~': {
                this.addOutputChar(' ');
            }
            case '-': {
                this.addOutputChar('\u00ad');
                break;
            }
            case '_': {
                this.addOutputChar('\u2011');
                break;
            }
        }
    }
    
    private CharsetDecoder getDecoder() throws TikaException {
        final Charset charset = this.getCharset();
        if (this.lastCharset == null || !charset.equals(this.lastCharset)) {
            (this.decoder = charset.newDecoder()).onMalformedInput(CodingErrorAction.REPLACE);
            this.decoder.onUnmappableCharacter(CodingErrorAction.REPLACE);
            this.lastCharset = charset;
        }
        return this.decoder;
    }
    
    private Charset getCharset() throws TikaException {
        if (this.groupState.fontCharset != null) {
            return this.groupState.fontCharset;
        }
        if (this.globalDefaultFont != -1 && !this.inHeader) {
            final Charset cs = this.fontToCharset.get(this.globalDefaultFont);
            if (cs != null) {
                return cs;
            }
        }
        if (this.globalCharset == null) {
            throw new TikaException("unable to determine charset");
        }
        return this.globalCharset;
    }
    
    private void processControlWord(final int param, final PushbackInputStream in) throws IOException, SAXException, TikaException {
        if (this.inHeader) {
            if (this.equals("ansicpg")) {
                final Charset cs = TextExtractor.ANSICPG_MAP.get(param);
                if (cs != null) {
                    this.globalCharset = cs;
                }
            }
            else if (this.equals("deff")) {
                this.globalDefaultFont = param;
            }
            if (this.fontTableState == 1) {
                if (this.groupState.depth < this.fontTableDepth) {
                    this.fontTableState = 2;
                }
                else if (this.equals("f")) {
                    this.curFontID = param;
                }
                else if (this.equals("fcharset")) {
                    final Charset cs = TextExtractor.FCHARSET_MAP.get(param);
                    if (cs != null) {
                        this.fontToCharset.put(this.curFontID, cs);
                    }
                }
            }
        }
        else if (this.equals("b")) {
            assert param == 0;
            if (this.groupState.bold) {
                this.pushText();
                if (this.groupState.italic) {
                    this.end("i");
                }
                this.end("b");
                if (this.groupState.italic) {
                    this.start("i");
                }
                this.groupState.bold = false;
            }
        }
        else if (this.equals("i")) {
            assert param == 0;
            if (this.groupState.italic) {
                this.pushText();
                this.end("i");
                this.groupState.italic = false;
            }
        }
        else if (this.equals("f")) {
            final Charset fontCharset = this.fontToCharset.get(param);
            this.pushText();
            if (fontCharset != null) {
                this.groupState.fontCharset = fontCharset;
            }
            else {
                this.groupState.fontCharset = null;
            }
        }
        if (this.equals("u")) {
            if (!this.groupState.ignore) {
                final char utf16CodeUnit = (char)(param & 0xFFFF);
                this.addOutputChar(utf16CodeUnit);
            }
            this.ansiSkip = this.groupState.ucSkip;
        }
        else if (this.equals("uc")) {
            this.groupState.ucSkip = param;
        }
        else if (this.equals("bin") && param >= 0) {
            int bytesToRead = param;
            final byte[] tmpArray = new byte[Math.min(1024, bytesToRead)];
            while (bytesToRead > 0) {
                final int r = in.read(tmpArray, 0, Math.min(bytesToRead, tmpArray.length));
                if (r < 0) {
                    throw new TikaException("unexpected end of file: need " + param + " bytes of binary data, found " + (param - bytesToRead));
                }
                bytesToRead -= r;
            }
        }
    }
    
    private void end(final String tag) throws IOException, SAXException, TikaException {
        this.out.endElement(tag);
    }
    
    private void start(final String tag) throws IOException, SAXException, TikaException {
        this.out.startElement(tag);
    }
    
    private void processControlWord() throws IOException, SAXException, TikaException {
        if (this.inHeader) {
            if (this.equals("ansi")) {
                this.globalCharset = TextExtractor.WINDOWS_1252;
            }
            else if (this.equals("pca")) {
                this.globalCharset = TextExtractor.CP850;
            }
            else if (this.equals("pc")) {
                this.globalCharset = TextExtractor.CP437;
            }
            else if (this.equals("mac")) {
                this.globalCharset = TextExtractor.MAC_ROMAN;
            }
            if (this.equals("colortbl") || this.equals("stylesheet") || this.equals("fonttbl")) {
                this.groupState.ignore = true;
            }
            if (this.uprState == -1) {
                if (this.equals("author")) {
                    this.nextMetaData = TikaCoreProperties.CREATOR;
                }
                else if (this.equals("title")) {
                    this.nextMetaData = TikaCoreProperties.TITLE;
                }
                else if (this.equals("subject")) {
                    this.nextMetaData = TikaCoreProperties.TRANSITION_SUBJECT_TO_OO_SUBJECT;
                }
                else if (this.equals("keywords")) {
                    this.nextMetaData = TikaCoreProperties.TRANSITION_KEYWORDS_TO_DC_SUBJECT;
                }
                else if (this.equals("category")) {
                    this.nextMetaData = OfficeOpenXMLCore.CATEGORY;
                }
                else if (this.equals("comment")) {
                    this.nextMetaData = TikaCoreProperties.COMMENTS;
                }
                else if (this.equals("company")) {
                    this.nextMetaData = OfficeOpenXMLExtended.COMPANY;
                }
                else if (this.equals("manager")) {
                    this.nextMetaData = OfficeOpenXMLExtended.MANAGER;
                }
                else if (this.equals("template")) {
                    this.nextMetaData = OfficeOpenXMLExtended.TEMPLATE;
                }
            }
            if (this.fontTableState == 0) {
                if (this.equals("fonttbl")) {
                    this.fontTableState = 1;
                    this.fontTableDepth = this.groupState.depth;
                }
            }
            else if (this.fontTableState == 1 && this.groupState.depth < this.fontTableDepth) {
                this.fontTableState = 2;
            }
            if (!this.groupState.ignore && (this.equals("par") || this.equals("pard") || this.equals("sect") || this.equals("sectd") || this.equals("plain") || this.equals("ltrch") || this.equals("rtlch"))) {
                this.inHeader = false;
            }
        }
        else if (this.equals("b")) {
            if (!this.groupState.bold) {
                this.pushText();
                this.lazyStartParagraph();
                if (this.groupState.italic) {
                    this.end("i");
                }
                this.groupState.bold = true;
                this.start("b");
                if (this.groupState.italic) {
                    this.start("i");
                }
            }
        }
        else if (this.equals("i") && !this.groupState.italic) {
            this.pushText();
            this.lazyStartParagraph();
            this.groupState.italic = true;
            this.start("i");
        }
        final boolean ignored = this.groupState.ignore;
        if (this.equals("pard")) {
            this.pushText();
            if (this.groupState.italic) {
                this.end("i");
                this.groupState.italic = false;
            }
            if (this.groupState.bold) {
                this.end("b");
                this.groupState.bold = false;
            }
        }
        else if (this.equals("par")) {
            if (!ignored) {
                this.endParagraph(true);
            }
        }
        else if (this.equals("shptxt")) {
            this.pushText();
            this.groupState.ignore = false;
        }
        else if (this.equals("atnid")) {
            this.pushText();
            this.groupState.ignore = false;
        }
        else if (this.equals("atnauthor")) {
            this.pushText();
            this.groupState.ignore = false;
        }
        else if (this.equals("annotation")) {
            this.pushText();
            this.groupState.ignore = false;
        }
        else if (this.equals("cell")) {
            this.endParagraph(true);
        }
        else if (this.equals("pict")) {
            this.pushText();
            this.groupState.ignore = true;
        }
        else if (this.equals("line")) {
            if (!ignored) {
                this.addOutputChar('\n');
            }
        }
        else if (this.equals("column")) {
            if (!ignored) {
                this.addOutputChar(' ');
            }
        }
        else if (this.equals("page")) {
            if (!ignored) {
                this.addOutputChar('\n');
            }
        }
        else if (this.equals("softline")) {
            if (!ignored) {
                this.addOutputChar('\n');
            }
        }
        else if (this.equals("softcolumn")) {
            if (!ignored) {
                this.addOutputChar(' ');
            }
        }
        else if (this.equals("softpage")) {
            if (!ignored) {
                this.addOutputChar('\n');
            }
        }
        else if (this.equals("tab")) {
            if (!ignored) {
                this.addOutputChar('\t');
            }
        }
        else if (this.equals("upr")) {
            this.uprState = 0;
        }
        else if (this.equals("ud") && this.uprState == 1) {
            this.uprState = -1;
            this.groupState.ignore = false;
        }
        else if (this.equals("bullet")) {
            if (!ignored) {
                this.addOutputChar('\u2022');
            }
        }
        else if (this.equals("endash")) {
            if (!ignored) {
                this.addOutputChar('\u2013');
            }
        }
        else if (this.equals("emdash")) {
            if (!ignored) {
                this.addOutputChar('\u2014');
            }
        }
        else if (this.equals("enspace")) {
            if (!ignored) {
                this.addOutputChar('\u2002');
            }
        }
        else if (this.equals("qmspace")) {
            if (!ignored) {
                this.addOutputChar('\u2005');
            }
        }
        else if (this.equals("emspace")) {
            if (!ignored) {
                this.addOutputChar('\u2003');
            }
        }
        else if (this.equals("lquote")) {
            if (!ignored) {
                this.addOutputChar('\u2018');
            }
        }
        else if (this.equals("rquote")) {
            if (!ignored) {
                this.addOutputChar('\u2019');
            }
        }
        else if (this.equals("ldblquote")) {
            if (!ignored) {
                this.addOutputChar('\u201c');
            }
        }
        else if (this.equals("rdblquote")) {
            if (!ignored) {
                this.addOutputChar('\u201d');
            }
        }
        else if (this.equals("fldinst")) {
            this.fieldState = 1;
            this.groupState.ignore = false;
        }
        else if (this.equals("fldrslt") && this.fieldState == 2) {
            assert this.pendingURL != null;
            this.lazyStartParagraph();
            this.out.startElement("a", "href", this.pendingURL);
            this.pendingURL = null;
            this.fieldState = 3;
            this.groupState.ignore = false;
        }
    }
    
    private void processGroupStart(final PushbackInputStream in) throws IOException {
        this.ansiSkip = 0;
        this.groupStates.add(this.groupState);
        this.groupState = new GroupState(this.groupState);
        assert this.groupStates.size() == this.groupState.depth : "size=" + this.groupStates.size() + " depth=" + this.groupState.depth;
        if (this.uprState == 0) {
            this.uprState = 1;
            this.groupState.ignore = true;
        }
        final int b2 = in.read();
        if (b2 == 92) {
            final int b3 = in.read();
            if (b3 == 42) {
                this.groupState.ignore = true;
            }
            in.unread(b3);
        }
        in.unread(b2);
    }
    
    private void processGroupEnd() throws IOException, SAXException, TikaException {
        if (this.inHeader) {
            if (this.nextMetaData != null) {
                if (this.nextMetaData.isMultiValuePermitted()) {
                    this.metadata.add(this.nextMetaData, this.pendingBuffer.toString());
                }
                else {
                    this.metadata.set(this.nextMetaData, this.pendingBuffer.toString());
                }
                this.nextMetaData = null;
            }
            this.pendingBuffer.setLength(0);
        }
        assert this.groupState.depth > 0;
        this.ansiSkip = 0;
        if (this.groupStates.size() > 0) {
            final GroupState outerGroupState = this.groupStates.removeLast();
            if (this.groupState.italic && (!outerGroupState.italic || this.groupState.bold != outerGroupState.bold)) {
                this.end("i");
                this.groupState.italic = false;
            }
            if (this.groupState.bold && !outerGroupState.bold) {
                this.end("b");
            }
            if (!this.groupState.bold && outerGroupState.bold) {
                this.start("b");
            }
            if (!this.groupState.italic && outerGroupState.italic) {
                this.start("i");
            }
            this.groupState = outerGroupState;
        }
        assert this.groupStates.size() == this.groupState.depth;
        if (this.fieldState == 1) {
            String s = this.pendingBuffer.toString().trim();
            this.pendingBuffer.setLength(0);
            if (s.startsWith("HYPERLINK")) {
                s = s.substring(9).trim();
                final boolean isLocalLink = s.indexOf("\\l ") != -1;
                final int idx = s.indexOf(34);
                if (idx != -1) {
                    final int idx2 = s.indexOf(34, 1 + idx);
                    if (idx2 != -1) {
                        s = s.substring(1 + idx, idx2);
                    }
                }
                this.pendingURL = (isLocalLink ? "#" : "") + s;
                this.fieldState = 2;
            }
            else {
                this.fieldState = 0;
            }
        }
        else if (this.fieldState == 3) {
            this.out.endElement("a");
            this.fieldState = 0;
        }
    }
    
    static {
        ASCII = Charset.forName("US-ASCII");
        WINDOWS_1252 = getCharset("WINDOWS-1252");
        MAC_ROMAN = getCharset("MacRoman");
        SHIFT_JIS = getCharset("Shift_JIS");
        WINDOWS_57011 = getCharset("windows-57011");
        WINDOWS_57010 = getCharset("windows-57010");
        WINDOWS_57009 = getCharset("windows-57009");
        WINDOWS_57008 = getCharset("windows-57008");
        WINDOWS_57007 = getCharset("windows-57007");
        WINDOWS_57006 = getCharset("windows-57006");
        WINDOWS_57005 = getCharset("windows-57005");
        WINDOWS_57004 = getCharset("windows-57004");
        WINDOWS_57003 = getCharset("windows-57003");
        X_ISCII91 = getCharset("x-ISCII91");
        X_MAC_CENTRAL_EUROPE = getCharset("x-MacCentralEurope");
        MAC_CYRILLIC = getCharset("MacCyrillic");
        X_JOHAB = getCharset("x-Johab");
        CP12582 = getCharset("CP1258");
        CP12572 = getCharset("CP1257");
        CP12562 = getCharset("CP1256");
        CP12552 = getCharset("CP1255");
        CP12542 = getCharset("CP1254");
        CP12532 = getCharset("CP1253");
        CP1252 = getCharset("CP1252");
        CP12512 = getCharset("CP1251");
        CP12502 = getCharset("CP1250");
        CP950 = getCharset("CP950");
        CP949 = getCharset("CP949");
        MS9362 = getCharset("MS936");
        MS8742 = getCharset("MS874");
        CP866 = getCharset("CP866");
        CP865 = getCharset("CP865");
        CP864 = getCharset("CP864");
        CP863 = getCharset("CP863");
        CP862 = getCharset("CP862");
        CP860 = getCharset("CP860");
        CP852 = getCharset("CP852");
        CP8502 = getCharset("CP850");
        CP819 = getCharset("CP819");
        WINDOWS_720 = getCharset("windows-720");
        WINDOWS_711 = getCharset("windows-711");
        WINDOWS_710 = getCharset("windows-710");
        WINDOWS_709 = getCharset("windows-709");
        ISO_8859_6 = getCharset("ISO-8859-6");
        CP4372 = getCharset("CP437");
        CP850 = getCharset("cp850");
        CP437 = getCharset("cp437");
        MS874 = getCharset("ms874");
        CP1257 = getCharset("cp1257");
        CP1256 = getCharset("cp1256");
        CP1255 = getCharset("cp1255");
        CP1258 = getCharset("cp1258");
        CP1254 = getCharset("cp1254");
        CP1253 = getCharset("cp1253");
        MS950 = getCharset("ms950");
        MS936 = getCharset("ms936");
        MS1361 = getCharset("ms1361");
        MS932 = getCharset("MS932");
        CP1251 = getCharset("cp1251");
        CP1250 = getCharset("cp1250");
        MAC_THAI = getCharset("MacThai");
        MAC_TURKISH = getCharset("MacTurkish");
        MAC_GREEK = getCharset("MacGreek");
        MAC_ARABIC = getCharset("MacArabic");
        MAC_HEBREW = getCharset("MacHebrew");
        JOHAB = getCharset("johab");
        BIG5 = getCharset("Big5");
        GB2312 = getCharset("GB2312");
        MS949 = getCharset("ms949");
        (FCHARSET_MAP = new HashMap<Integer, Charset>()).put(0, TextExtractor.WINDOWS_1252);
        TextExtractor.FCHARSET_MAP.put(77, TextExtractor.MAC_ROMAN);
        TextExtractor.FCHARSET_MAP.put(78, TextExtractor.SHIFT_JIS);
        TextExtractor.FCHARSET_MAP.put(79, TextExtractor.MS949);
        TextExtractor.FCHARSET_MAP.put(80, TextExtractor.GB2312);
        TextExtractor.FCHARSET_MAP.put(81, TextExtractor.BIG5);
        TextExtractor.FCHARSET_MAP.put(82, TextExtractor.JOHAB);
        TextExtractor.FCHARSET_MAP.put(83, TextExtractor.MAC_HEBREW);
        TextExtractor.FCHARSET_MAP.put(84, TextExtractor.MAC_ARABIC);
        TextExtractor.FCHARSET_MAP.put(85, TextExtractor.MAC_GREEK);
        TextExtractor.FCHARSET_MAP.put(86, TextExtractor.MAC_TURKISH);
        TextExtractor.FCHARSET_MAP.put(87, TextExtractor.MAC_THAI);
        TextExtractor.FCHARSET_MAP.put(88, TextExtractor.CP1250);
        TextExtractor.FCHARSET_MAP.put(89, TextExtractor.CP1251);
        TextExtractor.FCHARSET_MAP.put(128, TextExtractor.MS932);
        TextExtractor.FCHARSET_MAP.put(129, TextExtractor.MS949);
        TextExtractor.FCHARSET_MAP.put(130, TextExtractor.MS1361);
        TextExtractor.FCHARSET_MAP.put(134, TextExtractor.MS936);
        TextExtractor.FCHARSET_MAP.put(136, TextExtractor.MS950);
        TextExtractor.FCHARSET_MAP.put(161, TextExtractor.CP1253);
        TextExtractor.FCHARSET_MAP.put(162, TextExtractor.CP1254);
        TextExtractor.FCHARSET_MAP.put(163, TextExtractor.CP1258);
        TextExtractor.FCHARSET_MAP.put(177, TextExtractor.CP1255);
        TextExtractor.FCHARSET_MAP.put(178, TextExtractor.CP1256);
        TextExtractor.FCHARSET_MAP.put(186, TextExtractor.CP1257);
        TextExtractor.FCHARSET_MAP.put(204, TextExtractor.CP1251);
        TextExtractor.FCHARSET_MAP.put(222, TextExtractor.MS874);
        TextExtractor.FCHARSET_MAP.put(238, TextExtractor.CP1250);
        TextExtractor.FCHARSET_MAP.put(254, TextExtractor.CP437);
        TextExtractor.FCHARSET_MAP.put(255, TextExtractor.CP850);
        (ANSICPG_MAP = new HashMap<Integer, Charset>()).put(437, TextExtractor.CP4372);
        TextExtractor.ANSICPG_MAP.put(708, TextExtractor.ISO_8859_6);
        TextExtractor.ANSICPG_MAP.put(709, TextExtractor.WINDOWS_709);
        TextExtractor.ANSICPG_MAP.put(710, TextExtractor.WINDOWS_710);
        TextExtractor.ANSICPG_MAP.put(710, TextExtractor.WINDOWS_711);
        TextExtractor.ANSICPG_MAP.put(710, TextExtractor.WINDOWS_720);
        TextExtractor.ANSICPG_MAP.put(819, TextExtractor.CP819);
        TextExtractor.ANSICPG_MAP.put(819, TextExtractor.CP819);
        TextExtractor.ANSICPG_MAP.put(819, TextExtractor.CP819);
        TextExtractor.ANSICPG_MAP.put(850, TextExtractor.CP8502);
        TextExtractor.ANSICPG_MAP.put(852, TextExtractor.CP852);
        TextExtractor.ANSICPG_MAP.put(860, TextExtractor.CP860);
        TextExtractor.ANSICPG_MAP.put(862, TextExtractor.CP862);
        TextExtractor.ANSICPG_MAP.put(863, TextExtractor.CP863);
        TextExtractor.ANSICPG_MAP.put(864, TextExtractor.CP864);
        TextExtractor.ANSICPG_MAP.put(865, TextExtractor.CP865);
        TextExtractor.ANSICPG_MAP.put(866, TextExtractor.CP866);
        TextExtractor.ANSICPG_MAP.put(874, TextExtractor.MS8742);
        TextExtractor.ANSICPG_MAP.put(932, TextExtractor.MS932);
        TextExtractor.ANSICPG_MAP.put(936, TextExtractor.MS9362);
        TextExtractor.ANSICPG_MAP.put(949, TextExtractor.CP949);
        TextExtractor.ANSICPG_MAP.put(950, TextExtractor.CP950);
        TextExtractor.ANSICPG_MAP.put(1250, TextExtractor.CP12502);
        TextExtractor.ANSICPG_MAP.put(1251, TextExtractor.CP12512);
        TextExtractor.ANSICPG_MAP.put(1252, TextExtractor.CP1252);
        TextExtractor.ANSICPG_MAP.put(1253, TextExtractor.CP12532);
        TextExtractor.ANSICPG_MAP.put(1254, TextExtractor.CP12542);
        TextExtractor.ANSICPG_MAP.put(1255, TextExtractor.CP12552);
        TextExtractor.ANSICPG_MAP.put(1256, TextExtractor.CP12562);
        TextExtractor.ANSICPG_MAP.put(1257, TextExtractor.CP12572);
        TextExtractor.ANSICPG_MAP.put(1258, TextExtractor.CP12582);
        TextExtractor.ANSICPG_MAP.put(1361, TextExtractor.X_JOHAB);
        TextExtractor.ANSICPG_MAP.put(10000, TextExtractor.MAC_ROMAN);
        TextExtractor.ANSICPG_MAP.put(10001, TextExtractor.SHIFT_JIS);
        TextExtractor.ANSICPG_MAP.put(10004, TextExtractor.MAC_ARABIC);
        TextExtractor.ANSICPG_MAP.put(10005, TextExtractor.MAC_HEBREW);
        TextExtractor.ANSICPG_MAP.put(10006, TextExtractor.MAC_GREEK);
        TextExtractor.ANSICPG_MAP.put(10007, TextExtractor.MAC_CYRILLIC);
        TextExtractor.ANSICPG_MAP.put(10029, TextExtractor.X_MAC_CENTRAL_EUROPE);
        TextExtractor.ANSICPG_MAP.put(10081, TextExtractor.MAC_TURKISH);
        TextExtractor.ANSICPG_MAP.put(57002, TextExtractor.X_ISCII91);
        TextExtractor.ANSICPG_MAP.put(57003, TextExtractor.WINDOWS_57003);
        TextExtractor.ANSICPG_MAP.put(57004, TextExtractor.WINDOWS_57004);
        TextExtractor.ANSICPG_MAP.put(57005, TextExtractor.WINDOWS_57005);
        TextExtractor.ANSICPG_MAP.put(57006, TextExtractor.WINDOWS_57006);
        TextExtractor.ANSICPG_MAP.put(57007, TextExtractor.WINDOWS_57007);
        TextExtractor.ANSICPG_MAP.put(57008, TextExtractor.WINDOWS_57008);
        TextExtractor.ANSICPG_MAP.put(57009, TextExtractor.WINDOWS_57009);
        TextExtractor.ANSICPG_MAP.put(57010, TextExtractor.WINDOWS_57010);
        TextExtractor.ANSICPG_MAP.put(57011, TextExtractor.WINDOWS_57011);
    }
}
