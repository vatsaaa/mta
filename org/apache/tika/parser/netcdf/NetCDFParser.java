// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.parser.netcdf;

import org.apache.tika.metadata.TikaCoreProperties;
import org.xml.sax.SAXException;
import org.apache.tika.metadata.Property;
import java.util.Iterator;
import org.apache.tika.sax.XHTMLContentHandler;
import java.io.IOException;
import org.apache.tika.exception.TikaException;
import ucar.nc2.Attribute;
import ucar.nc2.NetcdfFile;
import java.io.OutputStream;
import org.apache.tika.io.IOUtils;
import java.io.ByteArrayOutputStream;
import org.apache.tika.metadata.Metadata;
import org.xml.sax.ContentHandler;
import java.io.InputStream;
import org.apache.tika.parser.ParseContext;
import java.util.Collections;
import org.apache.tika.mime.MediaType;
import java.util.Set;
import org.apache.tika.parser.AbstractParser;

public class NetCDFParser extends AbstractParser
{
    private static final long serialVersionUID = -5940938274907708665L;
    private final Set<MediaType> SUPPORTED_TYPES;
    
    public NetCDFParser() {
        this.SUPPORTED_TYPES = Collections.singleton(MediaType.application("x-netcdf"));
    }
    
    public Set<MediaType> getSupportedTypes(final ParseContext context) {
        return this.SUPPORTED_TYPES;
    }
    
    public void parse(final InputStream stream, final ContentHandler handler, final Metadata metadata, final ParseContext context) throws IOException, SAXException, TikaException {
        final ByteArrayOutputStream os = new ByteArrayOutputStream();
        IOUtils.copy(stream, os);
        String name = metadata.get("resourceName");
        if (name == null) {
            name = "";
        }
        try {
            final NetcdfFile ncFile = NetcdfFile.openInMemory(name, os.toByteArray());
            for (final Attribute attr : ncFile.getGlobalAttributes()) {
                final Property property = this.resolveMetadataKey(attr.getName());
                if (attr.getDataType().isString()) {
                    metadata.add(property, attr.getStringValue());
                }
                else {
                    if (!attr.getDataType().isNumeric()) {
                        continue;
                    }
                    final int value = attr.getNumericValue().intValue();
                    metadata.add(property, String.valueOf(value));
                }
            }
        }
        catch (IOException e) {
            throw new TikaException("NetCDF parse error", e);
        }
        final XHTMLContentHandler xhtml = new XHTMLContentHandler(handler, metadata);
        xhtml.startDocument();
        xhtml.endDocument();
    }
    
    private Property resolveMetadataKey(final String localName) {
        if ("title".equals(localName)) {
            return TikaCoreProperties.TITLE;
        }
        return Property.internalText(localName);
    }
}
