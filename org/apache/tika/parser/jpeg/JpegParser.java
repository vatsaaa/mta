// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.parser.jpeg;

import java.util.Collections;
import org.apache.tika.exception.TikaException;
import org.xml.sax.SAXException;
import java.io.IOException;
import org.apache.tika.sax.XHTMLContentHandler;
import org.apache.tika.parser.image.xmp.JempboxExtractor;
import org.apache.tika.parser.image.ImageMetadataExtractor;
import org.apache.tika.io.TikaInputStream;
import org.apache.tika.io.TemporaryResources;
import org.apache.tika.metadata.Metadata;
import org.xml.sax.ContentHandler;
import java.io.InputStream;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.mime.MediaType;
import java.util.Set;
import org.apache.tika.parser.AbstractParser;

public class JpegParser extends AbstractParser
{
    private static final long serialVersionUID = -1355028253756234603L;
    private static final Set<MediaType> SUPPORTED_TYPES;
    
    public Set<MediaType> getSupportedTypes(final ParseContext context) {
        return JpegParser.SUPPORTED_TYPES;
    }
    
    public void parse(final InputStream stream, final ContentHandler handler, final Metadata metadata, final ParseContext context) throws IOException, SAXException, TikaException {
        final TemporaryResources tmp = new TemporaryResources();
        try {
            final TikaInputStream tis = TikaInputStream.get(stream, tmp);
            new ImageMetadataExtractor(metadata).parseJpeg(tis.getFile());
            new JempboxExtractor(metadata).parse(tis);
        }
        finally {
            tmp.dispose();
        }
        final XHTMLContentHandler xhtml = new XHTMLContentHandler(handler, metadata);
        xhtml.startDocument();
        xhtml.endDocument();
    }
    
    static {
        SUPPORTED_TYPES = Collections.singleton(MediaType.image("jpeg"));
    }
}
