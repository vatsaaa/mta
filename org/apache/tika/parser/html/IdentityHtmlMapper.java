// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.parser.html;

import java.util.Locale;

public class IdentityHtmlMapper implements HtmlMapper
{
    public static final HtmlMapper INSTANCE;
    
    public boolean isDiscardElement(final String name) {
        return false;
    }
    
    public String mapSafeAttribute(final String elementName, final String attributeName) {
        return attributeName.toLowerCase(Locale.ENGLISH);
    }
    
    public String mapSafeElement(final String name) {
        return name;
    }
    
    static {
        INSTANCE = new IdentityHtmlMapper();
    }
}
