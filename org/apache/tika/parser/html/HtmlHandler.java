// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.parser.html;

import java.util.Collection;
import java.util.HashSet;
import java.util.Arrays;
import org.apache.tika.metadata.TikaCoreProperties;
import java.util.Locale;
import org.xml.sax.helpers.AttributesImpl;
import java.util.regex.Matcher;
import org.apache.tika.mime.MediaType;
import org.xml.sax.SAXException;
import org.xml.sax.Attributes;
import java.net.MalformedURLException;
import java.net.URL;
import org.xml.sax.ContentHandler;
import java.util.regex.Pattern;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.sax.XHTMLContentHandler;
import java.util.Set;
import org.apache.tika.sax.TextContentHandler;

class HtmlHandler extends TextContentHandler
{
    private static final Set<String> URI_ATTRIBUTES;
    private final HtmlMapper mapper;
    private final XHTMLContentHandler xhtml;
    private final Metadata metadata;
    private int bodyLevel;
    private int discardLevel;
    private int titleLevel;
    private final StringBuilder title;
    private static final Pattern ICBM;
    
    private HtmlHandler(final HtmlMapper mapper, final XHTMLContentHandler xhtml, final Metadata metadata) {
        super(xhtml);
        this.bodyLevel = 0;
        this.discardLevel = 0;
        this.titleLevel = 0;
        this.title = new StringBuilder();
        this.mapper = mapper;
        this.xhtml = xhtml;
        this.metadata = metadata;
        if (metadata.get("Content-Location") == null) {
            String name = metadata.get("resourceName");
            if (name != null) {
                name = name.trim();
                try {
                    new URL(name);
                    metadata.set("Content-Location", name);
                }
                catch (MalformedURLException ex) {}
            }
        }
    }
    
    public HtmlHandler(final HtmlMapper mapper, final ContentHandler handler, final Metadata metadata) {
        this(mapper, new XHTMLContentHandler(handler, metadata), metadata);
    }
    
    @Override
    public void startElement(final String uri, final String local, final String name, final Attributes atts) throws SAXException {
        if ("TITLE".equals(name) || this.titleLevel > 0) {
            ++this.titleLevel;
        }
        if ("BODY".equals(name) || "FRAMESET".equals(name) || this.bodyLevel > 0) {
            ++this.bodyLevel;
        }
        if (this.mapper.isDiscardElement(name) || this.discardLevel > 0) {
            ++this.discardLevel;
        }
        if (this.bodyLevel == 0 && this.discardLevel == 0) {
            if ("META".equals(name) && atts.getValue("content") != null) {
                if (atts.getValue("http-equiv") != null) {
                    this.addHtmlMetadata(atts.getValue("http-equiv"), atts.getValue("content"));
                }
                else if (atts.getValue("name") != null) {
                    this.addHtmlMetadata(atts.getValue("name"), atts.getValue("content"));
                }
            }
            else if ("BASE".equals(name) && atts.getValue("href") != null) {
                this.startElementWithSafeAttributes("base", atts);
                this.xhtml.endElement("base");
                this.metadata.set("Content-Location", this.resolve(atts.getValue("href")));
            }
            else if ("LINK".equals(name)) {
                this.startElementWithSafeAttributes("link", atts);
                this.xhtml.endElement("link");
            }
        }
        if (this.bodyLevel > 0 && this.discardLevel == 0) {
            final String safe = this.mapper.mapSafeElement(name);
            if (safe != null) {
                this.startElementWithSafeAttributes(safe, atts);
            }
        }
        this.title.setLength(0);
    }
    
    private void addHtmlMetadata(final String name, final String value) {
        if (name != null) {
            if (value != null) {
                if (name.equalsIgnoreCase("ICBM")) {
                    final Matcher m = HtmlHandler.ICBM.matcher(value);
                    if (m.matches()) {
                        this.metadata.set("ICBM", m.group(1) + ", " + m.group(2));
                        this.metadata.set(Metadata.LATITUDE, m.group(1));
                        this.metadata.set(Metadata.LONGITUDE, m.group(2));
                    }
                    else {
                        this.metadata.set("ICBM", value);
                    }
                }
                else if (name.equalsIgnoreCase("Content-Type")) {
                    final MediaType type = MediaType.parse(value);
                    if (type != null) {
                        this.metadata.set("Content-Type", type.toString());
                    }
                    else {
                        this.metadata.set("Content-Type", value);
                    }
                }
                else {
                    this.metadata.set(name, value);
                }
            }
        }
    }
    
    private void startElementWithSafeAttributes(final String name, final Attributes atts) throws SAXException {
        if (atts.getLength() == 0) {
            this.xhtml.startElement(name);
            return;
        }
        final boolean isObject = name.equals("object");
        String codebase = null;
        if (isObject) {
            codebase = atts.getValue("", "codebase");
            if (codebase != null) {
                codebase = this.resolve(codebase);
            }
            else {
                codebase = this.metadata.get("Content-Location");
            }
        }
        final AttributesImpl newAttributes = new AttributesImpl(atts);
        for (int att = 0; att < newAttributes.getLength(); ++att) {
            final String attrName = newAttributes.getLocalName(att);
            final String normAttrName = this.mapper.mapSafeAttribute(name, attrName);
            if (normAttrName == null) {
                newAttributes.removeAttribute(att);
                --att;
            }
            else {
                newAttributes.setLocalName(att, normAttrName);
                if (HtmlHandler.URI_ATTRIBUTES.contains(normAttrName)) {
                    newAttributes.setValue(att, this.resolve(newAttributes.getValue(att)));
                }
                else if (isObject && "codebase".equals(normAttrName)) {
                    newAttributes.setValue(att, codebase);
                }
                else if (isObject && ("data".equals(normAttrName) || "classid".equals(normAttrName))) {
                    newAttributes.setValue(att, this.resolve(codebase, newAttributes.getValue(att)));
                }
            }
        }
        if ("img".equals(name) && newAttributes.getValue("", "alt") == null) {
            newAttributes.addAttribute("", "alt", "alt", "CDATA", "");
        }
        this.xhtml.startElement(name, newAttributes);
    }
    
    @Override
    public void endElement(final String uri, final String local, final String name) throws SAXException {
        if (this.bodyLevel > 0 && this.discardLevel == 0) {
            final String safe = this.mapper.mapSafeElement(name);
            if (safe != null) {
                this.xhtml.endElement(safe);
            }
            else if (XHTMLContentHandler.ENDLINE.contains(name.toLowerCase(Locale.ENGLISH))) {
                this.xhtml.newline();
            }
        }
        if (this.titleLevel > 0) {
            --this.titleLevel;
            if (this.titleLevel == 0) {
                this.metadata.set(TikaCoreProperties.TITLE, this.title.toString().trim());
            }
        }
        if (this.bodyLevel > 0) {
            --this.bodyLevel;
        }
        if (this.discardLevel > 0) {
            --this.discardLevel;
        }
    }
    
    @Override
    public void characters(final char[] ch, final int start, final int length) throws SAXException {
        if (this.titleLevel > 0 && this.bodyLevel == 0) {
            this.title.append(ch, start, length);
        }
        if (this.bodyLevel > 0 && this.discardLevel == 0) {
            super.characters(ch, start, length);
        }
    }
    
    @Override
    public void ignorableWhitespace(final char[] ch, final int start, final int length) throws SAXException {
        if (this.bodyLevel > 0 && this.discardLevel == 0) {
            super.ignorableWhitespace(ch, start, length);
        }
    }
    
    private String resolve(final String url) {
        return this.resolve(this.metadata.get("Content-Location"), url);
    }
    
    private String resolve(final String base, String url) {
        url = url.trim();
        final String lower = url.toLowerCase(Locale.ENGLISH);
        if (base == null || lower.startsWith("urn:") || lower.startsWith("mailto:") || lower.startsWith("tel:") || lower.startsWith("data:") || lower.startsWith("javascript:") || lower.startsWith("about:")) {
            return url;
        }
        try {
            final URL baseURL = new URL(base.trim());
            final String path = baseURL.getPath();
            if (url.startsWith("?") && path.length() > 0 && !path.endsWith("/")) {
                return new URL(baseURL.getProtocol(), baseURL.getHost(), baseURL.getPort(), baseURL.getPath() + url).toExternalForm();
            }
            return new URL(baseURL, url).toExternalForm();
        }
        catch (MalformedURLException e) {
            return url;
        }
    }
    
    static {
        URI_ATTRIBUTES = new HashSet<String>(Arrays.asList("src", "href", "longdesc", "cite"));
        ICBM = Pattern.compile("\\s*(-?\\d+\\.\\d+)[,\\s]+(-?\\d+\\.\\d+)\\s*");
    }
}
