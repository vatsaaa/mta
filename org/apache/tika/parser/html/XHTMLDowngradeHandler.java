// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.parser.html;

import org.xml.sax.SAXException;
import org.xml.sax.helpers.AttributesImpl;
import java.util.Locale;
import org.xml.sax.Attributes;
import org.xml.sax.ContentHandler;
import org.apache.tika.sax.ContentHandlerDecorator;

class XHTMLDowngradeHandler extends ContentHandlerDecorator
{
    public XHTMLDowngradeHandler(final ContentHandler handler) {
        super(handler);
    }
    
    @Override
    public void startElement(final String uri, final String localName, final String name, final Attributes atts) throws SAXException {
        final String upper = localName.toUpperCase(Locale.ENGLISH);
        final AttributesImpl attributes = new AttributesImpl();
        for (int i = 0; i < atts.getLength(); ++i) {
            final String auri = atts.getURI(i);
            final String local = atts.getLocalName(i);
            final String qname = atts.getQName(i);
            if ("".equals(auri) && !local.equals("xmlns") && !qname.startsWith("xmlns:")) {
                attributes.addAttribute(auri, local, qname, atts.getType(i), atts.getValue(i));
            }
        }
        super.startElement("", upper, upper, attributes);
    }
    
    @Override
    public void endElement(final String uri, final String localName, final String name) throws SAXException {
        final String upper = localName.toUpperCase(Locale.ENGLISH);
        super.endElement("", upper, upper);
    }
    
    @Override
    public void startPrefixMapping(final String prefix, final String uri) {
    }
    
    @Override
    public void endPrefixMapping(final String prefix) {
    }
}
