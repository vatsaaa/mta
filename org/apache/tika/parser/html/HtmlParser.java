// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.parser.html;

import org.ccil.cowan.tagsoup.HTMLSchema;
import java.util.Collections;
import java.util.Collection;
import java.util.HashSet;
import java.util.Arrays;
import org.apache.tika.exception.TikaException;
import org.xml.sax.SAXException;
import java.io.IOException;
import java.nio.charset.Charset;
import org.ccil.cowan.tagsoup.Parser;
import org.apache.tika.detect.AutoDetectReader;
import org.apache.tika.io.CloseShieldInputStream;
import org.apache.tika.metadata.Metadata;
import org.xml.sax.ContentHandler;
import java.io.InputStream;
import org.apache.tika.parser.ParseContext;
import org.ccil.cowan.tagsoup.Schema;
import org.apache.tika.config.ServiceLoader;
import org.apache.tika.mime.MediaType;
import java.util.Set;
import org.apache.tika.parser.AbstractParser;

public class HtmlParser extends AbstractParser
{
    private static final long serialVersionUID = 7895315240498733128L;
    private static final Set<MediaType> SUPPORTED_TYPES;
    private static final ServiceLoader LOADER;
    private static final Schema HTML_SCHEMA;
    
    public Set<MediaType> getSupportedTypes(final ParseContext context) {
        return HtmlParser.SUPPORTED_TYPES;
    }
    
    public void parse(final InputStream stream, final ContentHandler handler, final Metadata metadata, final ParseContext context) throws IOException, SAXException, TikaException {
        final AutoDetectReader reader = new AutoDetectReader(new CloseShieldInputStream(stream), metadata, HtmlParser.LOADER);
        try {
            final Charset charset = reader.getCharset();
            final String previous = metadata.get("Content-Type");
            if (previous == null || previous.startsWith("text/html")) {
                final MediaType type = new MediaType(MediaType.TEXT_HTML, charset);
                metadata.set("Content-Type", type.toString());
            }
            metadata.set("Content-Encoding", charset.name());
            final HtmlMapper mapper = context.get((Class<HtmlParserMapper>)HtmlMapper.class, new HtmlParserMapper());
            final org.ccil.cowan.tagsoup.Parser parser = new org.ccil.cowan.tagsoup.Parser();
            parser.setProperty("http://www.ccil.org/~cowan/tagsoup/properties/schema", HtmlParser.HTML_SCHEMA);
            parser.setFeature("http://www.ccil.org/~cowan/tagsoup/features/ignore-bogons", true);
            parser.setContentHandler(new XHTMLDowngradeHandler(new HtmlHandler(mapper, handler, metadata)));
            parser.parse(reader.asInputSource());
        }
        finally {
            reader.close();
        }
    }
    
    @Deprecated
    protected String mapSafeElement(final String name) {
        return DefaultHtmlMapper.INSTANCE.mapSafeElement(name);
    }
    
    @Deprecated
    protected boolean isDiscardElement(final String name) {
        return DefaultHtmlMapper.INSTANCE.isDiscardElement(name);
    }
    
    @Deprecated
    public String mapSafeAttribute(final String elementName, final String attributeName) {
        return DefaultHtmlMapper.INSTANCE.mapSafeAttribute(elementName, attributeName);
    }
    
    static {
        SUPPORTED_TYPES = Collections.unmodifiableSet((Set<? extends MediaType>)new HashSet<MediaType>(Arrays.asList(MediaType.text("html"), MediaType.application("xhtml+xml"), MediaType.application("vnd.wap.xhtml+xml"), MediaType.application("x-asp"))));
        LOADER = new ServiceLoader(HtmlParser.class.getClassLoader());
        HTML_SCHEMA = new HTMLSchema();
    }
    
    private class HtmlParserMapper implements HtmlMapper
    {
        public String mapSafeElement(final String name) {
            return HtmlParser.this.mapSafeElement(name);
        }
        
        public boolean isDiscardElement(final String name) {
            return HtmlParser.this.isDiscardElement(name);
        }
        
        public String mapSafeAttribute(final String elementName, final String attributeName) {
            return HtmlParser.this.mapSafeAttribute(elementName, attributeName);
        }
    }
}
