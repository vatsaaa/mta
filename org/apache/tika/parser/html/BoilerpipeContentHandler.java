// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.parser.html;

import java.util.Iterator;
import de.l3s.boilerpipe.document.TextDocument;
import de.l3s.boilerpipe.document.TextBlock;
import java.util.BitSet;
import org.xml.sax.helpers.AttributesImpl;
import de.l3s.boilerpipe.BoilerpipeProcessingException;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import java.util.ArrayList;
import org.apache.tika.sax.WriteOutContentHandler;
import java.io.Writer;
import de.l3s.boilerpipe.extractors.DefaultExtractor;
import java.util.List;
import de.l3s.boilerpipe.BoilerpipeExtractor;
import org.xml.sax.ContentHandler;
import de.l3s.boilerpipe.sax.BoilerpipeHTMLContentHandler;

public class BoilerpipeContentHandler extends BoilerpipeHTMLContentHandler
{
    private static final char[] NL;
    private ContentHandler delegate;
    private BoilerpipeExtractor extractor;
    private boolean includeMarkup;
    private boolean inHeader;
    private boolean inFooter;
    private int headerCharOffset;
    private List<RecordedElement> elements;
    
    public BoilerpipeContentHandler(final ContentHandler delegate) {
        this(delegate, DefaultExtractor.INSTANCE);
    }
    
    public BoilerpipeContentHandler(final Writer writer) {
        this(new WriteOutContentHandler(writer));
    }
    
    public BoilerpipeContentHandler(final ContentHandler delegate, final BoilerpipeExtractor extractor) {
        this.delegate = delegate;
        this.extractor = extractor;
    }
    
    public void setIncludeMarkup(final boolean includeMarkup) {
        this.includeMarkup = includeMarkup;
    }
    
    public boolean isIncludeMarkup() {
        return this.includeMarkup;
    }
    
    @Override
    public void startDocument() throws SAXException {
        super.startDocument();
        this.delegate.startDocument();
        this.inHeader = true;
        this.inFooter = false;
        this.headerCharOffset = 0;
        if (this.includeMarkup) {
            this.elements = new ArrayList<RecordedElement>();
        }
    }
    
    @Override
    public void startPrefixMapping(final String prefix, final String uri) throws SAXException {
        super.startPrefixMapping(prefix, uri);
        this.delegate.startPrefixMapping(prefix, uri);
    }
    
    @Override
    public void startElement(final String uri, final String localName, final String qName, final Attributes atts) throws SAXException {
        super.startElement(uri, localName, qName, atts);
        if (this.inHeader) {
            this.delegate.startElement(uri, localName, qName, atts);
        }
        else if (!this.inFooter) {
            if (this.includeMarkup) {
                this.elements.add(new RecordedElement(uri, localName, qName, atts));
            }
            else {
                this.delegate.startElement(uri, localName, qName, atts);
            }
        }
    }
    
    @Override
    public void characters(final char[] chars, final int offset, final int length) throws SAXException {
        super.characters(chars, offset, length);
        if (this.inHeader) {
            this.delegate.characters(chars, offset, length);
            ++this.headerCharOffset;
        }
        else if (!this.inFooter) {
            if (this.includeMarkup) {
                final RecordedElement element = this.elements.get(this.elements.size() - 1);
                final char[] characters = new char[length];
                System.arraycopy(chars, offset, characters, 0, length);
                element.getCharacters().add(characters);
            }
        }
    }
    
    @Override
    public void endElement(final String uri, final String localName, final String qName) throws SAXException {
        super.endElement(uri, localName, qName);
        if (this.inHeader) {
            this.delegate.endElement(uri, localName, qName);
            this.inHeader = !localName.equals("head");
        }
        else if (!this.inFooter) {
            if (localName.equals("body")) {
                this.inFooter = true;
            }
            else if (this.includeMarkup) {
                this.elements.add(new RecordedElement(uri, localName, qName));
                this.elements.add(new RecordedElement());
            }
        }
    }
    
    @Override
    public void endDocument() throws SAXException {
        super.endDocument();
        final TextDocument td = this.toTextDocument();
        try {
            this.extractor.process(td);
        }
        catch (BoilerpipeProcessingException e) {
            throw new SAXException(e);
        }
        final Attributes emptyAttrs = new AttributesImpl();
        if (this.includeMarkup) {
            final BitSet validCharacterRuns = new BitSet();
            for (final TextBlock block : td.getTextBlocks()) {
                if (block.isContent()) {
                    final BitSet bs = block.getContainedTextElements();
                    if (bs == null) {
                        continue;
                    }
                    validCharacterRuns.or(bs);
                }
            }
            int curCharsIndex = this.headerCharOffset;
            for (final RecordedElement element : this.elements) {
                switch (element.getElementType()) {
                    case START: {
                        this.delegate.startElement(element.getUri(), element.getLocalName(), element.getQName(), element.getAttrs());
                    }
                    case CONTINUE: {
                        for (final char[] chars : element.getCharacters()) {
                            ++curCharsIndex;
                            if (validCharacterRuns.get(curCharsIndex)) {
                                this.delegate.characters(chars, 0, chars.length);
                            }
                        }
                        continue;
                    }
                    case END: {
                        this.delegate.endElement(element.getUri(), element.getLocalName(), element.getQName());
                        continue;
                    }
                    default: {
                        throw new RuntimeException("Unhandled element type: " + element.getElementType());
                    }
                }
            }
        }
        else {
            for (final TextBlock block2 : td.getTextBlocks()) {
                if (block2.isContent()) {
                    this.delegate.startElement("http://www.w3.org/1999/xhtml", "p", "p", emptyAttrs);
                    final char[] chars2 = block2.getText().toCharArray();
                    this.delegate.characters(chars2, 0, chars2.length);
                    this.delegate.endElement("http://www.w3.org/1999/xhtml", "p", "p");
                    this.delegate.ignorableWhitespace(BoilerpipeContentHandler.NL, 0, BoilerpipeContentHandler.NL.length);
                }
            }
        }
        this.delegate.endElement("http://www.w3.org/1999/xhtml", "body", "body");
        this.delegate.endElement("http://www.w3.org/1999/xhtml", "html", "html");
        this.delegate.endPrefixMapping("");
        this.delegate.endDocument();
    }
    
    static {
        NL = new char[] { '\n' };
    }
    
    private static class RecordedElement
    {
        private String uri;
        private String localName;
        private String qName;
        private Attributes attrs;
        private List<char[]> characters;
        private ElementType elementType;
        
        public RecordedElement(final String uri, final String localName, final String qName, final Attributes attrs) {
            this(uri, localName, qName, attrs, ElementType.START);
        }
        
        public RecordedElement(final String uri, final String localName, final String qName) {
            this(uri, localName, qName, null, ElementType.END);
        }
        
        public RecordedElement() {
            this(null, null, null, null, ElementType.CONTINUE);
        }
        
        protected RecordedElement(final String uri, final String localName, final String qName, final Attributes attrs, final ElementType elementType) {
            this.uri = uri;
            this.localName = localName;
            this.qName = qName;
            this.attrs = attrs;
            this.elementType = elementType;
            this.characters = new ArrayList<char[]>();
        }
        
        @Override
        public String toString() {
            return String.format("<%s> of type %s", this.localName, this.elementType);
        }
        
        public String getUri() {
            return this.uri;
        }
        
        public String getLocalName() {
            return this.localName;
        }
        
        public String getQName() {
            return this.qName;
        }
        
        public Attributes getAttrs() {
            return this.attrs;
        }
        
        public List<char[]> getCharacters() {
            return this.characters;
        }
        
        public ElementType getElementType() {
            return this.elementType;
        }
        
        public enum ElementType
        {
            START, 
            END, 
            CONTINUE;
        }
    }
}
