// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.parser.html;

import java.io.IOException;
import java.util.regex.Matcher;
import org.apache.tika.utils.CharsetUtils;
import org.apache.tika.mime.MediaType;
import java.nio.ByteBuffer;
import org.apache.tika.metadata.Metadata;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.regex.Pattern;
import org.apache.tika.detect.EncodingDetector;

public class HtmlEncodingDetector implements EncodingDetector
{
    private static final int META_TAG_BUFFER_SIZE = 8192;
    private static final Pattern HTTP_EQUIV_PATTERN;
    private static final Pattern META_CHARSET_PATTERN;
    private static final Charset ASCII;
    
    public Charset detect(final InputStream input, final Metadata metadata) throws IOException {
        if (input == null) {
            return null;
        }
        input.mark(8192);
        final byte[] buffer = new byte[8192];
        int n = 0;
        for (int m = input.read(buffer); m != -1 && n < buffer.length; n += m, m = input.read(buffer, n, buffer.length - n)) {}
        input.reset();
        String charset = null;
        final String head = HtmlEncodingDetector.ASCII.decode(ByteBuffer.wrap(buffer, 0, n)).toString();
        final Matcher equiv = HtmlEncodingDetector.HTTP_EQUIV_PATTERN.matcher(head);
        if (equiv.find()) {
            final MediaType type = MediaType.parse(equiv.group(1));
            if (type != null) {
                charset = type.getParameters().get("charset");
            }
        }
        if (charset == null) {
            final Matcher meta = HtmlEncodingDetector.META_CHARSET_PATTERN.matcher(head);
            if (meta.find()) {
                charset = meta.group(1);
            }
        }
        if (charset != null) {
            try {
                return CharsetUtils.forName(charset);
            }
            catch (Exception ex) {}
        }
        return null;
    }
    
    static {
        HTTP_EQUIV_PATTERN = Pattern.compile("(?is)<meta\\s+http-equiv\\s*=\\s*['\\\"]\\s*Content-Type['\\\"]\\s+content\\s*=\\s*['\\\"]([^'\\\"]+)['\\\"]");
        META_CHARSET_PATTERN = Pattern.compile("(?is)<meta\\s+charset\\s*=\\s*['\\\"]([^'\\\"]+)['\\\"]");
        ASCII = Charset.forName("US-ASCII");
    }
}
