// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.parser.html;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.Map;

public class DefaultHtmlMapper implements HtmlMapper
{
    private static final Map<String, String> SAFE_ELEMENTS;
    private static final Set<String> DISCARDABLE_ELEMENTS;
    private static final Map<String, Set<String>> SAFE_ATTRIBUTES;
    public static final HtmlMapper INSTANCE;
    
    private static Set<String> attrSet(final String... attrs) {
        final Set<String> result = new HashSet<String>();
        for (final String attr : attrs) {
            result.add(attr);
        }
        return result;
    }
    
    public String mapSafeElement(final String name) {
        return DefaultHtmlMapper.SAFE_ELEMENTS.get(name);
    }
    
    public String mapSafeAttribute(final String elementName, final String attributeName) {
        final Set<String> safeAttrs = DefaultHtmlMapper.SAFE_ATTRIBUTES.get(elementName);
        if (safeAttrs != null && safeAttrs.contains(attributeName)) {
            return attributeName;
        }
        return null;
    }
    
    public boolean isDiscardElement(final String name) {
        return DefaultHtmlMapper.DISCARDABLE_ELEMENTS.contains(name);
    }
    
    static {
        SAFE_ELEMENTS = new HashMap<String, String>() {
            {
                this.put("H1", "h1");
                this.put("H2", "h2");
                this.put("H3", "h3");
                this.put("H4", "h4");
                this.put("H5", "h5");
                this.put("H6", "h6");
                this.put("P", "p");
                this.put("PRE", "pre");
                this.put("BLOCKQUOTE", "blockquote");
                this.put("Q", "q");
                this.put("UL", "ul");
                this.put("OL", "ol");
                this.put("MENU", "ul");
                this.put("LI", "li");
                this.put("DL", "dl");
                this.put("DT", "dt");
                this.put("DD", "dd");
                this.put("TABLE", "table");
                this.put("THEAD", "thead");
                this.put("TBODY", "tbody");
                this.put("TR", "tr");
                this.put("TH", "th");
                this.put("TD", "td");
                this.put("ADDRESS", "address");
                this.put("A", "a");
                this.put("MAP", "map");
                this.put("AREA", "area");
                this.put("IMG", "img");
                this.put("FRAMESET", "frameset");
                this.put("FRAME", "frame");
                this.put("IFRAME", "iframe");
                this.put("OBJECT", "object");
                this.put("PARAM", "param");
                this.put("INS", "ins");
                this.put("DEL", "del");
            }
        };
        DISCARDABLE_ELEMENTS = new HashSet<String>() {
            {
                this.add("STYLE");
                this.add("SCRIPT");
            }
        };
        SAFE_ATTRIBUTES = new HashMap<String, Set<String>>() {
            {
                this.put("a", attrSet(new String[] { "charset", "type", "name", "href", "hreflang", "rel", "rev", "shape", "coords" }));
                this.put("img", attrSet(new String[] { "src", "alt", "longdesc", "height", "width", "usemap", "ismap" }));
                this.put("frame", attrSet(new String[] { "longdesc", "name", "src", "frameborder", "marginwidth", "marginheight", "noresize", "scrolling" }));
                this.put("iframe", attrSet(new String[] { "longdesc", "name", "src", "frameborder", "marginwidth", "marginheight", "scrolling", "align", "height", "width" }));
                this.put("link", attrSet(new String[] { "charset", "href", "hreflang", "type", "rel", "rev", "media" }));
                this.put("map", attrSet(new String[] { "id", "class", "style", "title", "name" }));
                this.put("area", attrSet(new String[] { "shape", "coords", "href", "nohref", "alt" }));
                this.put("object", attrSet(new String[] { "declare", "classid", "codebase", "data", "type", "codetype", "archive", "standby", "height", "width", "usemap", "name", "tabindex", "align", "border", "hspace", "vspace" }));
                this.put("param", attrSet(new String[] { "id", "name", "value", "valuetype", "type" }));
                this.put("blockquote", attrSet(new String[] { "cite" }));
                this.put("ins", attrSet(new String[] { "cite", "datetime" }));
                this.put("del", attrSet(new String[] { "cite", "datetime" }));
                this.put("q", attrSet(new String[] { "cite" }));
            }
        };
        INSTANCE = new DefaultHtmlMapper();
    }
}
