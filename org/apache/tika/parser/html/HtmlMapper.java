// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.parser.html;

public interface HtmlMapper
{
    String mapSafeElement(final String p0);
    
    boolean isDiscardElement(final String p0);
    
    String mapSafeAttribute(final String p0, final String p1);
}
