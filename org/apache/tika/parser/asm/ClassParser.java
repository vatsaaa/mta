// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.parser.asm;

import java.util.Collections;
import org.apache.tika.exception.TikaException;
import org.xml.sax.SAXException;
import java.io.IOException;
import org.apache.tika.metadata.Metadata;
import org.xml.sax.ContentHandler;
import java.io.InputStream;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.mime.MediaType;
import java.util.Set;
import org.apache.tika.parser.AbstractParser;

public class ClassParser extends AbstractParser
{
    private static final long serialVersionUID = -3531388963354454357L;
    private static final Set<MediaType> SUPPORTED_TYPES;
    
    public Set<MediaType> getSupportedTypes(final ParseContext context) {
        return ClassParser.SUPPORTED_TYPES;
    }
    
    public void parse(final InputStream stream, final ContentHandler handler, final Metadata metadata, final ParseContext context) throws IOException, SAXException, TikaException {
        new XHTMLClassVisitor(handler, metadata).parse(stream);
    }
    
    static {
        SUPPORTED_TYPES = Collections.singleton(MediaType.application("java-vm"));
    }
}
