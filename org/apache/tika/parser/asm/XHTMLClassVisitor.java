// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.parser.asm;

import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.FieldVisitor;
import org.objectweb.asm.Attribute;
import org.objectweb.asm.AnnotationVisitor;
import org.apache.tika.metadata.TikaCoreProperties;
import java.io.IOException;
import org.apache.tika.exception.TikaException;
import org.xml.sax.SAXException;
import org.objectweb.asm.ClassReader;
import java.io.InputStream;
import org.xml.sax.ContentHandler;
import org.objectweb.asm.Type;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.sax.XHTMLContentHandler;
import org.objectweb.asm.ClassVisitor;

class XHTMLClassVisitor implements ClassVisitor
{
    private final XHTMLContentHandler xhtml;
    private final Metadata metadata;
    private Type type;
    private String packageName;
    
    public XHTMLClassVisitor(final ContentHandler handler, final Metadata metadata) {
        this.xhtml = new XHTMLContentHandler(handler, metadata);
        this.metadata = metadata;
    }
    
    public void parse(final InputStream stream) throws TikaException, SAXException, IOException {
        try {
            final ClassReader reader = new ClassReader(stream);
            reader.accept(this, 5);
        }
        catch (RuntimeException e) {
            if (e.getCause() instanceof SAXException) {
                throw (SAXException)e.getCause();
            }
            throw new TikaException("Failed to parse a Java class", e);
        }
    }
    
    public void visit(final int version, final int access, final String name, final String signature, final String superName, final String[] interfaces) {
        this.type = Type.getObjectType(name);
        String className = this.type.getClassName();
        final int dot = className.lastIndexOf(46);
        if (dot != -1) {
            this.packageName = className.substring(0, dot);
            className = className.substring(dot + 1);
        }
        this.metadata.set(TikaCoreProperties.TITLE, className);
        this.metadata.set("resourceName", className + ".class");
        try {
            this.xhtml.startDocument();
            this.xhtml.startElement("pre");
            if (this.packageName != null) {
                this.writeKeyword("package");
                this.xhtml.characters(" " + this.packageName + ";\n");
            }
            this.writeAccess(access);
            if (isSet(access, 512)) {
                this.writeKeyword("interface");
                this.writeSpace();
                this.writeType(this.type);
                this.writeSpace();
                this.writeInterfaces("extends", interfaces);
            }
            else if (isSet(access, 16384)) {
                this.writeKeyword("enum");
                this.writeSpace();
                this.writeType(this.type);
                this.writeSpace();
            }
            else {
                this.writeKeyword("class");
                this.writeSpace();
                this.writeType(this.type);
                this.writeSpace();
                if (superName != null) {
                    final Type superType = Type.getObjectType(superName);
                    if (!superType.getClassName().equals("java.lang.Object")) {
                        this.writeKeyword("extends");
                        this.writeSpace();
                        this.writeType(superType);
                        this.writeSpace();
                    }
                }
                this.writeInterfaces("implements", interfaces);
            }
            this.xhtml.characters("{\n");
        }
        catch (SAXException e) {
            throw new RuntimeException(e);
        }
    }
    
    private void writeInterfaces(final String keyword, final String[] interfaces) throws SAXException {
        if (interfaces != null && interfaces.length > 0) {
            this.writeKeyword(keyword);
            String separator = " ";
            for (final String iface : interfaces) {
                this.xhtml.characters(separator);
                this.writeType(Type.getObjectType(iface));
                separator = ", ";
            }
            this.writeSpace();
        }
    }
    
    public void visitEnd() {
        try {
            this.xhtml.characters("}\n");
            this.xhtml.endElement("pre");
            this.xhtml.endDocument();
        }
        catch (SAXException e) {
            throw new RuntimeException(e);
        }
    }
    
    public void visitOuterClass(final String owner, final String name, final String desc) {
    }
    
    public void visitSource(final String source, final String debug) {
    }
    
    public AnnotationVisitor visitAnnotation(final String desc, final boolean visible) {
        return null;
    }
    
    public void visitAttribute(final Attribute attr) {
    }
    
    public void visitInnerClass(final String name, final String outerName, final String innerName, final int access) {
    }
    
    public FieldVisitor visitField(final int access, final String name, final String desc, final String signature, final Object value) {
        if (!isSet(access, 4096)) {
            try {
                this.xhtml.characters("    ");
                this.writeAccess(access);
                this.writeType(Type.getType(desc));
                this.writeSpace();
                this.writeIdentifier(name);
                if (isSet(access, 8) && value != null) {
                    this.xhtml.characters(" = ");
                    this.xhtml.characters(value.toString());
                }
                this.writeSemicolon();
                this.writeNewline();
            }
            catch (SAXException e) {
                throw new RuntimeException(e);
            }
        }
        return null;
    }
    
    public MethodVisitor visitMethod(final int access, final String name, final String desc, final String signature, final String[] exceptions) {
        if (!isSet(access, 4096)) {
            try {
                this.xhtml.characters("    ");
                this.writeAccess(access);
                this.writeType(Type.getReturnType(desc));
                this.writeSpace();
                if ("<init>".equals(name)) {
                    this.writeType(this.type);
                }
                else {
                    this.writeIdentifier(name);
                }
                this.xhtml.characters("(");
                String separator = "";
                for (final Type arg : Type.getArgumentTypes(desc)) {
                    this.xhtml.characters(separator);
                    this.writeType(arg);
                    separator = ", ";
                }
                this.xhtml.characters(")");
                if (exceptions != null && exceptions.length > 0) {
                    this.writeSpace();
                    this.writeKeyword("throws");
                    separator = " ";
                    for (final String exception : exceptions) {
                        this.xhtml.characters(separator);
                        this.writeType(Type.getObjectType(exception));
                        separator = ", ";
                    }
                }
                this.writeSemicolon();
                this.writeNewline();
            }
            catch (SAXException e) {
                throw new RuntimeException(e);
            }
        }
        return null;
    }
    
    private void writeIdentifier(final String identifier) throws SAXException {
        this.xhtml.startElement("span", "class", "java-identifier");
        this.xhtml.characters(identifier);
        this.xhtml.endElement("span");
    }
    
    private void writeKeyword(final String keyword) throws SAXException {
        this.xhtml.startElement("span", "class", "java-keyword");
        this.xhtml.characters(keyword);
        this.xhtml.endElement("span");
    }
    
    private void writeSemicolon() throws SAXException {
        this.xhtml.characters(";");
    }
    
    private void writeSpace() throws SAXException {
        this.xhtml.characters(" ");
    }
    
    private void writeNewline() throws SAXException {
        this.xhtml.characters("\n");
    }
    
    private void writeAccess(final int access) throws SAXException {
        this.writeAccess(access, 2, "private");
        this.writeAccess(access, 4, "protected");
        this.writeAccess(access, 1, "public");
        this.writeAccess(access, 8, "static");
        this.writeAccess(access, 16, "final");
        this.writeAccess(access, 1024, "abstract");
        this.writeAccess(access, 32, "synchronized");
        this.writeAccess(access, 128, "transient");
        this.writeAccess(access, 64, "volatile");
        this.writeAccess(access, 256, "native");
    }
    
    private void writeAccess(final int access, final int code, final String keyword) throws SAXException {
        if (isSet(access, code)) {
            this.writeKeyword(keyword);
            this.xhtml.characters(" ");
        }
    }
    
    private void writeType(final Type type) throws SAXException {
        final String name = type.getClassName();
        if (name.startsWith(this.packageName + ".")) {
            this.xhtml.characters(name.substring(this.packageName.length() + 1));
        }
        else if (name.startsWith("java.lang.")) {
            this.xhtml.characters(name.substring("java.lang.".length()));
        }
        else {
            this.xhtml.characters(name);
        }
    }
    
    private static boolean isSet(final int value, final int flag) {
        return (value & flag) != 0x0;
    }
}
