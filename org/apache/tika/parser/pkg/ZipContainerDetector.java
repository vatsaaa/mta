// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.parser.pkg;

import java.util.Enumeration;
import java.util.Iterator;
import org.apache.tika.parser.iwork.IWorkPackageParser;
import org.apache.poi.openxml4j.opc.PackagePart;
import org.apache.poi.openxml4j.opc.PackageRelationshipCollection;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.openxml4j.opc.PackageAccess;
import org.apache.commons.compress.archivers.zip.ZipArchiveEntry;
import org.apache.commons.compress.archivers.zip.ZipFile;
import org.apache.commons.compress.archivers.ArchiveInputStream;
import org.apache.commons.compress.archivers.ArchiveException;
import org.apache.commons.compress.archivers.tar.TarArchiveInputStream;
import org.apache.commons.compress.archivers.ArchiveStreamFactory;
import org.apache.commons.compress.compressors.CompressorInputStream;
import org.apache.commons.compress.compressors.CompressorException;
import org.apache.tika.io.IOUtils;
import java.io.ByteArrayInputStream;
import org.apache.commons.compress.compressors.CompressorStreamFactory;
import java.io.IOException;
import org.apache.tika.exception.TikaException;
import org.apache.tika.io.TikaInputStream;
import org.apache.tika.io.TemporaryResources;
import org.apache.tika.mime.MediaType;
import org.apache.tika.metadata.Metadata;
import java.io.InputStream;
import java.util.regex.Pattern;
import org.apache.tika.detect.Detector;

public class ZipContainerDetector implements Detector
{
    private static final Pattern MACRO_TEMPLATE_PATTERN;
    private static final long serialVersionUID = 2891763938430295453L;
    
    public MediaType detect(final InputStream input, final Metadata metadata) throws IOException {
        if (input == null) {
            return MediaType.OCTET_STREAM;
        }
        final TemporaryResources tmp = new TemporaryResources();
        try {
            final TikaInputStream tis = TikaInputStream.get(input, tmp);
            final byte[] prefix = new byte[1024];
            final int length = tis.peek(prefix);
            final MediaType type = detectArchiveFormat(prefix, length);
            if (PackageParser.isZipArchive(type) && TikaInputStream.isTikaInputStream(input)) {
                return detectZipFormat(tis);
            }
            if (!type.equals(MediaType.OCTET_STREAM)) {
                return type;
            }
            return detectCompressorFormat(prefix, length);
        }
        finally {
            try {
                tmp.dispose();
            }
            catch (TikaException ex) {}
        }
    }
    
    private static MediaType detectCompressorFormat(final byte[] prefix, final int length) {
        try {
            final CompressorStreamFactory factory = new CompressorStreamFactory();
            final CompressorInputStream cis = factory.createCompressorInputStream(new ByteArrayInputStream(prefix, 0, length));
            try {
                return CompressorParser.getMediaType(cis);
            }
            finally {
                IOUtils.closeQuietly(cis);
            }
        }
        catch (CompressorException e) {
            return MediaType.OCTET_STREAM;
        }
    }
    
    private static MediaType detectArchiveFormat(final byte[] prefix, final int length) {
        try {
            final ArchiveStreamFactory factory = new ArchiveStreamFactory();
            final ArchiveInputStream ais = factory.createArchiveInputStream(new ByteArrayInputStream(prefix, 0, length));
            try {
                if (ais instanceof TarArchiveInputStream && !TarArchiveInputStream.matches(prefix, length)) {
                    return MediaType.OCTET_STREAM;
                }
                return PackageParser.getMediaType(ais);
            }
            finally {
                IOUtils.closeQuietly(ais);
            }
        }
        catch (ArchiveException e) {
            return MediaType.OCTET_STREAM;
        }
    }
    
    private static MediaType detectZipFormat(final TikaInputStream tis) {
        try {
            final ZipFile zip = new ZipFile(tis.getFile());
            try {
                MediaType type = detectOpenDocument(zip);
                if (type == null) {
                    type = detectOfficeOpenXML(zip, tis);
                }
                if (type == null) {
                    type = detectIWork(zip);
                }
                if (type == null) {
                    type = detectJar(zip);
                }
                if (type == null) {
                    type = detectKmz(zip);
                }
                if (type != null) {
                    return type;
                }
            }
            finally {
                try {
                    zip.close();
                }
                catch (IOException ex) {}
            }
        }
        catch (IOException ex2) {}
        return MediaType.APPLICATION_ZIP;
    }
    
    private static MediaType detectOpenDocument(final ZipFile zip) {
        try {
            final ZipArchiveEntry mimetype = zip.getEntry("mimetype");
            if (mimetype != null) {
                final InputStream stream = zip.getInputStream(mimetype);
                try {
                    return MediaType.parse(IOUtils.toString(stream, "UTF-8"));
                }
                finally {
                    stream.close();
                }
            }
            return null;
        }
        catch (IOException e) {
            return null;
        }
    }
    
    private static MediaType detectOfficeOpenXML(final ZipFile zip, final TikaInputStream stream) {
        try {
            if (zip.getEntry("_rels/.rels") != null || zip.getEntry("[Content_Types].xml") != null) {
                final OPCPackage pkg = OPCPackage.open(stream.getFile().getPath(), PackageAccess.READ);
                stream.setOpenContainer(pkg);
                return detectOfficeOpenXML(pkg);
            }
            return null;
        }
        catch (IOException e) {
            return null;
        }
        catch (RuntimeException e2) {
            return null;
        }
        catch (InvalidFormatException e3) {
            return null;
        }
    }
    
    public static MediaType detectOfficeOpenXML(final OPCPackage pkg) {
        final PackageRelationshipCollection core = pkg.getRelationshipsByType("http://schemas.openxmlformats.org/officeDocument/2006/relationships/officeDocument");
        if (core.size() != 1) {
            return null;
        }
        final PackagePart corePart = pkg.getPart(core.getRelationship(0));
        final String coreType = corePart.getContentType();
        String docType = coreType.substring(0, coreType.lastIndexOf(46));
        if (docType.toLowerCase().endsWith("macroenabled")) {
            docType = docType.toLowerCase() + ".12";
        }
        if (docType.toLowerCase().endsWith("macroenabledtemplate")) {
            docType = ZipContainerDetector.MACRO_TEMPLATE_PATTERN.matcher(docType).replaceAll("macroenabled.12");
        }
        return MediaType.parse(docType);
    }
    
    private static MediaType detectIWork(final ZipFile zip) {
        if (zip.getEntry("buildVersionHistory.plist") != null) {
            for (final String entryName : IWorkPackageParser.IWORK_CONTENT_ENTRIES) {
                final IWorkPackageParser.IWORKDocumentType type = IWorkPackageParser.IWORKDocumentType.detectType(zip.getEntry(entryName), zip);
                if (type != null) {
                    return type.getType();
                }
            }
            return MediaType.application("vnd.apple.iwork");
        }
        return null;
    }
    
    private static MediaType detectJar(final ZipFile zip) {
        if (zip.getEntry("META-INF/MANIFEST.MF") != null) {
            if (zip.getEntry("AndroidManifest.xml") != null) {
                return MediaType.application("vnd.android.package-archive");
            }
            if (zip.getEntry("WEB-INF/") != null) {
                return MediaType.application("x-tika-java-web-archive");
            }
            if (zip.getEntry("META-INF/application.xml") != null) {
                return MediaType.application("x-tika-java-enterprise-archive");
            }
            return MediaType.application("java-archive");
        }
        else {
            if (zip.getEntry("AndroidManifest.xml") != null) {
                return MediaType.application("vnd.android.package-archive");
            }
            return null;
        }
    }
    
    private static MediaType detectKmz(final ZipFile zip) {
        boolean kmlFound = false;
        final Enumeration<ZipArchiveEntry> entries = zip.getEntries();
        while (entries.hasMoreElements()) {
            final ZipArchiveEntry entry = entries.nextElement();
            final String name = entry.getName();
            if (!entry.isDirectory() && name.indexOf(47) == -1 && name.indexOf(92) == -1) {
                if (!name.endsWith(".kml") || kmlFound) {
                    return null;
                }
                kmlFound = true;
            }
        }
        if (kmlFound) {
            return MediaType.application("vnd.google-earth.kmz");
        }
        return null;
    }
    
    static {
        MACRO_TEMPLATE_PATTERN = Pattern.compile("macroenabledtemplate$", 2);
    }
}
