// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.parser.pkg;

import org.xml.sax.SAXException;
import java.io.IOException;
import org.apache.tika.extractor.EmbeddedDocumentExtractor;
import org.apache.tika.extractor.ParsingEmbeddedDocumentExtractor;
import org.apache.commons.compress.compressors.gzip.GzipUtils;
import org.apache.tika.sax.XHTMLContentHandler;
import org.apache.commons.compress.compressors.CompressorException;
import org.apache.tika.exception.TikaException;
import org.apache.commons.compress.compressors.CompressorStreamFactory;
import java.io.BufferedInputStream;
import org.apache.tika.io.CloseShieldInputStream;
import org.apache.tika.metadata.Metadata;
import org.xml.sax.ContentHandler;
import java.io.InputStream;
import org.apache.tika.parser.ParseContext;
import org.apache.commons.compress.compressors.pack200.Pack200CompressorInputStream;
import org.apache.commons.compress.compressors.xz.XZCompressorInputStream;
import org.apache.commons.compress.compressors.gzip.GzipCompressorInputStream;
import org.apache.commons.compress.compressors.bzip2.BZip2CompressorInputStream;
import org.apache.commons.compress.compressors.CompressorInputStream;
import java.util.Set;
import org.apache.tika.mime.MediaType;
import org.apache.tika.parser.AbstractParser;

public class CompressorParser extends AbstractParser
{
    private static final long serialVersionUID = 2793565792967222459L;
    private static final MediaType BZIP;
    private static final MediaType BZIP2;
    private static final MediaType GZIP;
    private static final MediaType XZ;
    private static final MediaType PACK;
    private static final Set<MediaType> SUPPORTED_TYPES;
    
    static MediaType getMediaType(final CompressorInputStream stream) {
        if (stream instanceof BZip2CompressorInputStream) {
            return CompressorParser.BZIP2;
        }
        if (stream instanceof GzipCompressorInputStream) {
            return CompressorParser.GZIP;
        }
        if (stream instanceof XZCompressorInputStream) {
            return CompressorParser.XZ;
        }
        if (stream instanceof Pack200CompressorInputStream) {
            return CompressorParser.PACK;
        }
        return MediaType.OCTET_STREAM;
    }
    
    public Set<MediaType> getSupportedTypes(final ParseContext context) {
        return CompressorParser.SUPPORTED_TYPES;
    }
    
    public void parse(InputStream stream, final ContentHandler handler, final Metadata metadata, final ParseContext context) throws IOException, SAXException, TikaException {
        stream = new CloseShieldInputStream(stream);
        stream = new BufferedInputStream(stream);
        CompressorInputStream cis;
        try {
            final CompressorStreamFactory factory = new CompressorStreamFactory();
            cis = factory.createCompressorInputStream(stream);
        }
        catch (CompressorException e) {
            throw new TikaException("Unable to uncompress document stream", e);
        }
        final MediaType type = getMediaType(cis);
        if (!type.equals(MediaType.OCTET_STREAM)) {
            metadata.set("Content-Type", type.toString());
        }
        final XHTMLContentHandler xhtml = new XHTMLContentHandler(handler, metadata);
        xhtml.startDocument();
        try {
            final Metadata entrydata = new Metadata();
            String name = metadata.get("resourceName");
            if (name != null) {
                if (name.endsWith(".tbz")) {
                    name = name.substring(0, name.length() - 4) + ".tar";
                }
                else if (name.endsWith(".tbz2")) {
                    name = name.substring(0, name.length() - 5) + ".tar";
                }
                else if (name.endsWith(".bz")) {
                    name = name.substring(0, name.length() - 3);
                }
                else if (name.endsWith(".bz2")) {
                    name = name.substring(0, name.length() - 4);
                }
                else if (name.endsWith(".xz")) {
                    name = name.substring(0, name.length() - 3);
                }
                else if (name.endsWith(".pack")) {
                    name = name.substring(0, name.length() - 5);
                }
                else if (name.length() > 0) {
                    name = GzipUtils.getUncompressedFilename(name);
                }
                entrydata.set("resourceName", name);
            }
            final EmbeddedDocumentExtractor extractor = context.get((Class<ParsingEmbeddedDocumentExtractor>)EmbeddedDocumentExtractor.class, new ParsingEmbeddedDocumentExtractor(context));
            if (extractor.shouldParseEmbedded(entrydata)) {
                extractor.parseEmbedded(cis, xhtml, entrydata, true);
            }
        }
        finally {
            cis.close();
        }
        xhtml.endDocument();
    }
    
    static {
        BZIP = MediaType.application("x-bzip");
        BZIP2 = MediaType.application("x-bzip2");
        GZIP = MediaType.application("x-gzip");
        XZ = MediaType.application("x-xz");
        PACK = MediaType.application("application/x-java-pack200");
        SUPPORTED_TYPES = MediaType.set(CompressorParser.BZIP, CompressorParser.BZIP2, CompressorParser.GZIP, CompressorParser.XZ, CompressorParser.PACK);
    }
}
