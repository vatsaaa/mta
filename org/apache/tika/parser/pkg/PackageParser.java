// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.parser.pkg;

import org.apache.tika.io.TikaInputStream;
import org.apache.tika.io.TemporaryResources;
import org.xml.sax.SAXException;
import java.io.IOException;
import org.apache.commons.compress.archivers.ArchiveEntry;
import org.apache.tika.sax.XHTMLContentHandler;
import org.apache.tika.extractor.EmbeddedDocumentExtractor;
import org.apache.tika.extractor.ParsingEmbeddedDocumentExtractor;
import org.apache.commons.compress.archivers.ArchiveException;
import org.apache.tika.exception.TikaException;
import org.apache.commons.compress.archivers.ArchiveStreamFactory;
import java.io.BufferedInputStream;
import org.apache.tika.io.CloseShieldInputStream;
import org.apache.tika.metadata.Metadata;
import org.xml.sax.ContentHandler;
import java.io.InputStream;
import org.apache.tika.parser.ParseContext;
import org.apache.commons.compress.archivers.tar.TarArchiveInputStream;
import org.apache.commons.compress.archivers.dump.DumpArchiveInputStream;
import org.apache.commons.compress.archivers.cpio.CpioArchiveInputStream;
import org.apache.commons.compress.archivers.ar.ArArchiveInputStream;
import org.apache.commons.compress.archivers.zip.ZipArchiveInputStream;
import org.apache.commons.compress.archivers.jar.JarArchiveInputStream;
import org.apache.commons.compress.archivers.ArchiveInputStream;
import java.util.Set;
import org.apache.tika.mime.MediaType;
import org.apache.tika.parser.AbstractParser;

public class PackageParser extends AbstractParser
{
    private static final long serialVersionUID = -5331043266963888708L;
    private static final MediaType ZIP;
    private static final MediaType JAR;
    private static final MediaType AR;
    private static final MediaType CPIO;
    private static final MediaType DUMP;
    private static final MediaType TAR;
    private static final Set<MediaType> SUPPORTED_TYPES;
    
    static MediaType getMediaType(final ArchiveInputStream stream) {
        if (stream instanceof JarArchiveInputStream) {
            return PackageParser.JAR;
        }
        if (stream instanceof ZipArchiveInputStream) {
            return PackageParser.ZIP;
        }
        if (stream instanceof ArArchiveInputStream) {
            return PackageParser.AR;
        }
        if (stream instanceof CpioArchiveInputStream) {
            return PackageParser.CPIO;
        }
        if (stream instanceof DumpArchiveInputStream) {
            return PackageParser.DUMP;
        }
        if (stream instanceof TarArchiveInputStream) {
            return PackageParser.TAR;
        }
        return MediaType.OCTET_STREAM;
    }
    
    static boolean isZipArchive(final MediaType type) {
        return type.equals(PackageParser.ZIP) || type.equals(PackageParser.JAR);
    }
    
    public Set<MediaType> getSupportedTypes(final ParseContext context) {
        return PackageParser.SUPPORTED_TYPES;
    }
    
    public void parse(InputStream stream, final ContentHandler handler, final Metadata metadata, final ParseContext context) throws IOException, SAXException, TikaException {
        stream = new CloseShieldInputStream(stream);
        stream = new BufferedInputStream(stream);
        ArchiveInputStream ais;
        try {
            final ArchiveStreamFactory factory = new ArchiveStreamFactory();
            ais = factory.createArchiveInputStream(stream);
        }
        catch (ArchiveException e) {
            throw new TikaException("Unable to unpack document stream", e);
        }
        final MediaType type = getMediaType(ais);
        if (!type.equals(MediaType.OCTET_STREAM)) {
            metadata.set("Content-Type", type.toString());
        }
        final EmbeddedDocumentExtractor extractor = context.get((Class<ParsingEmbeddedDocumentExtractor>)EmbeddedDocumentExtractor.class, new ParsingEmbeddedDocumentExtractor(context));
        final XHTMLContentHandler xhtml = new XHTMLContentHandler(handler, metadata);
        xhtml.startDocument();
        try {
            for (ArchiveEntry entry = ais.getNextEntry(); entry != null; entry = ais.getNextEntry()) {
                if (!entry.isDirectory()) {
                    this.parseEntry(ais, entry, extractor, xhtml);
                }
            }
        }
        finally {
            ais.close();
        }
        xhtml.endDocument();
    }
    
    private void parseEntry(final ArchiveInputStream archive, final ArchiveEntry entry, final EmbeddedDocumentExtractor extractor, final XHTMLContentHandler xhtml) throws SAXException, IOException, TikaException {
        final String name = entry.getName();
        if (archive.canReadEntryData(entry)) {
            final Metadata entrydata = new Metadata();
            if (name != null && name.length() > 0) {
                entrydata.set("resourceName", name);
            }
            if (extractor.shouldParseEmbedded(entrydata)) {
                final TemporaryResources tmp = new TemporaryResources();
                try {
                    final TikaInputStream tis = TikaInputStream.get(archive, tmp);
                    extractor.parseEmbedded(tis, xhtml, entrydata, true);
                }
                finally {
                    tmp.dispose();
                }
            }
        }
        else if (name != null && name.length() > 0) {
            xhtml.element("p", name);
        }
    }
    
    static {
        ZIP = MediaType.APPLICATION_ZIP;
        JAR = MediaType.application("java-archive");
        AR = MediaType.application("x-archive");
        CPIO = MediaType.application("x-cpio");
        DUMP = MediaType.application("x-tika-unix-dump");
        TAR = MediaType.application("x-tar");
        SUPPORTED_TYPES = MediaType.set(PackageParser.ZIP, PackageParser.JAR, PackageParser.AR, PackageParser.CPIO, PackageParser.DUMP, PackageParser.TAR);
    }
}
