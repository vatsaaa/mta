// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.parser.feed;

import java.util.Collections;
import java.util.Collection;
import java.util.HashSet;
import java.util.Arrays;
import org.xml.sax.SAXException;
import java.io.IOException;
import com.sun.syndication.feed.synd.SyndContent;
import java.util.Iterator;
import com.sun.syndication.feed.synd.SyndFeed;
import com.sun.syndication.io.FeedException;
import org.apache.tika.exception.TikaException;
import com.sun.syndication.feed.synd.SyndEntry;
import org.apache.tika.sax.XHTMLContentHandler;
import org.apache.tika.metadata.TikaCoreProperties;
import org.xml.sax.InputSource;
import org.apache.tika.io.CloseShieldInputStream;
import com.sun.syndication.io.SyndFeedInput;
import org.apache.tika.metadata.Metadata;
import org.xml.sax.ContentHandler;
import java.io.InputStream;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.mime.MediaType;
import java.util.Set;
import org.apache.tika.parser.AbstractParser;

public class FeedParser extends AbstractParser
{
    private static final long serialVersionUID = -3785361933034525186L;
    private static final Set<MediaType> SUPPORTED_TYPES;
    
    public Set<MediaType> getSupportedTypes(final ParseContext context) {
        return FeedParser.SUPPORTED_TYPES;
    }
    
    public void parse(final InputStream stream, final ContentHandler handler, final Metadata metadata, final ParseContext context) throws IOException, SAXException, TikaException {
        try {
            final SyndFeed feed = new SyndFeedInput().build(new InputSource(new CloseShieldInputStream(stream)));
            final String title = stripTags(feed.getTitleEx());
            final String description = stripTags(feed.getDescriptionEx());
            metadata.set(TikaCoreProperties.TITLE, title);
            metadata.set(TikaCoreProperties.DESCRIPTION, description);
            final XHTMLContentHandler xhtml = new XHTMLContentHandler(handler, metadata);
            xhtml.startDocument();
            xhtml.element("h1", title);
            xhtml.element("p", description);
            xhtml.startElement("ul");
            for (final Object e : feed.getEntries()) {
                final SyndEntry entry = (SyndEntry)e;
                final String link = entry.getLink();
                if (link != null) {
                    xhtml.startElement("li");
                    xhtml.startElement("a", "href", link);
                    xhtml.characters(stripTags(entry.getTitleEx()));
                    xhtml.endElement("a");
                    final SyndContent content = entry.getDescription();
                    if (content != null) {
                        xhtml.newline();
                        xhtml.characters(content.getValue());
                    }
                    xhtml.endElement("li");
                }
            }
            xhtml.endElement("ul");
            xhtml.endDocument();
        }
        catch (FeedException e2) {
            throw new TikaException("RSS parse error", e2);
        }
    }
    
    private static String stripTags(final SyndContent c) {
        if (c == null) {
            return "";
        }
        final String value = c.getValue();
        final String[] parts = value.split("<[^>]*>");
        final StringBuffer buf = new StringBuffer();
        for (final String part : parts) {
            buf.append(part);
        }
        return buf.toString().trim();
    }
    
    static {
        SUPPORTED_TYPES = Collections.unmodifiableSet((Set<? extends MediaType>)new HashSet<MediaType>(Arrays.asList(MediaType.application("rss+xml"), MediaType.application("atom+xml"))));
    }
}
