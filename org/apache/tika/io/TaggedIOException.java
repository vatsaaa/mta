// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.io;

import java.io.IOException;

public class TaggedIOException extends IOExceptionWithCause
{
    private final Object tag;
    
    public TaggedIOException(final IOException original, final Object tag) {
        super(original.getMessage(), original);
        this.tag = tag;
    }
    
    public Object getTag() {
        return this.tag;
    }
    
    @Override
    public IOException getCause() {
        return (IOException)super.getCause();
    }
}
