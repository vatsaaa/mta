// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.io;

import java.io.IOException;
import java.util.UUID;
import java.io.InputStream;
import java.io.Serializable;

public class TaggedInputStream extends ProxyInputStream
{
    private final Serializable tag;
    
    public TaggedInputStream(final InputStream proxy) {
        super(proxy);
        this.tag = UUID.randomUUID();
    }
    
    public static TaggedInputStream get(final InputStream proxy) {
        if (proxy instanceof TaggedInputStream) {
            return (TaggedInputStream)proxy;
        }
        return new TaggedInputStream(proxy);
    }
    
    public boolean isCauseOf(final IOException exception) {
        if (exception instanceof TaggedIOException) {
            final TaggedIOException tagged = (TaggedIOException)exception;
            return this.tag.equals(tagged.getTag());
        }
        return false;
    }
    
    public void throwIfCauseOf(final Exception exception) throws IOException {
        if (exception instanceof TaggedIOException) {
            final TaggedIOException tagged = (TaggedIOException)exception;
            if (this.tag.equals(tagged.getTag())) {
                throw tagged.getCause();
            }
        }
    }
    
    @Override
    protected void handleIOException(final IOException e) throws IOException {
        throw new TaggedIOException(e, this.tag);
    }
    
    @Override
    public String toString() {
        return "Tika Tagged InputStream wrapping " + this.in;
    }
}
