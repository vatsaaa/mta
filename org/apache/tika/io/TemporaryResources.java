// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.io;

import org.apache.tika.exception.TikaException;
import java.util.List;
import java.util.Iterator;
import java.io.IOException;
import java.io.File;
import java.util.LinkedList;
import java.io.Closeable;

public class TemporaryResources implements Closeable
{
    private final LinkedList<Closeable> resources;
    private File tmp;
    
    public TemporaryResources() {
        this.resources = new LinkedList<Closeable>();
        this.tmp = null;
    }
    
    public void setTemporaryFileDirectory(final File tmp) {
        this.tmp = tmp;
    }
    
    public File createTemporaryFile() throws IOException {
        final File file = File.createTempFile("apache-tika-", ".tmp", this.tmp);
        this.addResource(new Closeable() {
            public void close() throws IOException {
                if (!file.delete()) {
                    throw new IOException("Could not delete temporary file " + file.getPath());
                }
            }
        });
        return file;
    }
    
    public void addResource(final Closeable resource) {
        this.resources.addFirst(resource);
    }
    
    public <T extends Closeable> T getResource(final Class<T> klass) {
        for (final Closeable resource : this.resources) {
            if (klass.isAssignableFrom(resource.getClass())) {
                return (T)resource;
            }
        }
        return null;
    }
    
    public void close() throws IOException {
        final List<IOException> exceptions = new LinkedList<IOException>();
        for (final Closeable resource : this.resources) {
            try {
                resource.close();
            }
            catch (IOException e) {
                exceptions.add(e);
            }
        }
        this.resources.clear();
        if (exceptions.isEmpty()) {
            return;
        }
        if (exceptions.size() == 1) {
            throw exceptions.get(0);
        }
        throw new IOExceptionWithCause("Multiple IOExceptions" + exceptions, exceptions.get(0));
    }
    
    public void dispose() throws TikaException {
        try {
            this.close();
        }
        catch (IOException e) {
            throw new TikaException("Failed to close temporary resources", e);
        }
    }
}
