// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.io;

import java.nio.channels.FileChannel;
import java.io.OutputStream;
import java.io.FileOutputStream;
import java.io.Closeable;
import java.io.FileInputStream;
import java.net.URLConnection;
import java.net.URISyntaxException;
import java.net.URL;
import java.io.IOException;
import java.net.URI;
import java.sql.SQLException;
import java.sql.Blob;
import java.io.FileNotFoundException;
import org.apache.tika.metadata.Metadata;
import java.io.ByteArrayInputStream;
import java.io.BufferedInputStream;
import java.io.InputStream;
import java.io.File;

public class TikaInputStream extends TaggedInputStream
{
    private static final int BLOB_SIZE_THRESHOLD = 1048576;
    private File file;
    private final TemporaryResources tmp;
    private long length;
    private long position;
    private long mark;
    private Object openContainer;
    
    public static boolean isTikaInputStream(final InputStream stream) {
        return stream instanceof TikaInputStream;
    }
    
    public static TikaInputStream get(InputStream stream, final TemporaryResources tmp) {
        if (stream == null) {
            throw new NullPointerException("The Stream must not be null");
        }
        if (stream instanceof TikaInputStream) {
            return (TikaInputStream)stream;
        }
        if (!(stream instanceof BufferedInputStream) && !(stream instanceof ByteArrayInputStream)) {
            stream = new BufferedInputStream(stream);
        }
        return new TikaInputStream(stream, tmp, -1L);
    }
    
    public static TikaInputStream get(final InputStream stream) {
        return get(stream, new TemporaryResources());
    }
    
    public static TikaInputStream cast(final InputStream stream) {
        if (stream instanceof TikaInputStream) {
            return (TikaInputStream)stream;
        }
        return null;
    }
    
    public static TikaInputStream get(final byte[] data) {
        return get(data, new Metadata());
    }
    
    public static TikaInputStream get(final byte[] data, final Metadata metadata) {
        metadata.set("Content-Length", Integer.toString(data.length));
        return new TikaInputStream(new ByteArrayInputStream(data), new TemporaryResources(), data.length);
    }
    
    public static TikaInputStream get(final File file) throws FileNotFoundException {
        return get(file, new Metadata());
    }
    
    public static TikaInputStream get(final File file, final Metadata metadata) throws FileNotFoundException {
        metadata.set("resourceName", file.getName());
        metadata.set("Content-Length", Long.toString(file.length()));
        return new TikaInputStream(file);
    }
    
    public static TikaInputStream get(final Blob blob) throws SQLException {
        return get(blob, new Metadata());
    }
    
    public static TikaInputStream get(final Blob blob, final Metadata metadata) throws SQLException {
        long length = -1L;
        try {
            length = blob.length();
            metadata.set("Content-Length", Long.toString(length));
        }
        catch (SQLException ex) {}
        if (0L <= length && length <= 1048576L) {
            return get(blob.getBytes(1L, (int)length), metadata);
        }
        return new TikaInputStream(new BufferedInputStream(blob.getBinaryStream()), new TemporaryResources(), length);
    }
    
    public static TikaInputStream get(final URI uri) throws IOException {
        return get(uri, new Metadata());
    }
    
    public static TikaInputStream get(final URI uri, final Metadata metadata) throws IOException {
        if ("file".equalsIgnoreCase(uri.getScheme())) {
            final File file = new File(uri);
            if (file.isFile()) {
                return get(file, metadata);
            }
        }
        return get(uri.toURL(), metadata);
    }
    
    public static TikaInputStream get(final URL url) throws IOException {
        return get(url, new Metadata());
    }
    
    public static TikaInputStream get(final URL url, final Metadata metadata) throws IOException {
        if ("file".equalsIgnoreCase(url.getProtocol())) {
            try {
                final File file = new File(url.toURI());
                if (file.isFile()) {
                    return get(file, metadata);
                }
            }
            catch (URISyntaxException ex) {}
        }
        final URLConnection connection = url.openConnection();
        final String path = url.getPath();
        final int slash = path.lastIndexOf(47);
        if (slash + 1 < path.length()) {
            metadata.set("resourceName", path.substring(slash + 1));
        }
        final String type = connection.getContentType();
        if (type != null) {
            metadata.set("Content-Type", type);
        }
        final String encoding = connection.getContentEncoding();
        if (encoding != null) {
            metadata.set("Content-Encoding", encoding);
        }
        final int length = connection.getContentLength();
        if (length >= 0) {
            metadata.set("Content-Length", Integer.toString(length));
        }
        return new TikaInputStream(new BufferedInputStream(connection.getInputStream()), new TemporaryResources(), length);
    }
    
    private TikaInputStream(final File file) throws FileNotFoundException {
        super(new BufferedInputStream(new FileInputStream(file)));
        this.position = 0L;
        this.mark = -1L;
        this.file = file;
        this.tmp = new TemporaryResources();
        this.length = file.length();
    }
    
    private TikaInputStream(final InputStream stream, final TemporaryResources tmp, final long length) {
        super(stream);
        this.position = 0L;
        this.mark = -1L;
        this.file = null;
        this.tmp = tmp;
        this.length = length;
    }
    
    public int peek(final byte[] buffer) throws IOException {
        int n = 0;
        this.mark(buffer.length);
        int m = this.read(buffer);
        while (m != -1) {
            n += m;
            if (n < buffer.length) {
                m = this.read(buffer, n, buffer.length - n);
            }
            else {
                m = -1;
            }
        }
        this.reset();
        return n;
    }
    
    public Object getOpenContainer() {
        return this.openContainer;
    }
    
    public void setOpenContainer(final Object container) {
        this.openContainer = container;
        if (container instanceof Closeable) {
            this.tmp.addResource((Closeable)container);
        }
    }
    
    public boolean hasFile() {
        return this.file != null;
    }
    
    public File getFile() throws IOException {
        if (this.file == null) {
            if (this.position > 0L) {
                throw new IOException("Stream is already being read");
            }
            this.file = this.tmp.createTemporaryFile();
            final OutputStream out = new FileOutputStream(this.file);
            try {
                IOUtils.copy(this.in, out);
            }
            finally {
                out.close();
            }
            final FileInputStream newStream = new FileInputStream(this.file);
            this.tmp.addResource(newStream);
            final InputStream oldStream = this.in;
            this.in = new BufferedInputStream(newStream) {
                @Override
                public void close() throws IOException {
                    oldStream.close();
                }
            };
            this.length = this.file.length();
        }
        return this.file;
    }
    
    public FileChannel getFileChannel() throws IOException {
        final FileInputStream fis = new FileInputStream(this.getFile());
        this.tmp.addResource(fis);
        final FileChannel channel = fis.getChannel();
        this.tmp.addResource(channel);
        return channel;
    }
    
    public boolean hasLength() {
        return this.length != -1L;
    }
    
    public long getLength() throws IOException {
        if (this.length == -1L) {
            this.length = this.getFile().length();
        }
        return this.length;
    }
    
    public long getPosition() {
        return this.position;
    }
    
    @Override
    public long skip(final long ln) throws IOException {
        final long n = super.skip(ln);
        this.position += n;
        return n;
    }
    
    @Override
    public void mark(final int readlimit) {
        super.mark(readlimit);
        this.mark = this.position;
    }
    
    @Override
    public boolean markSupported() {
        return true;
    }
    
    @Override
    public void reset() throws IOException {
        super.reset();
        this.position = this.mark;
        this.mark = -1L;
    }
    
    @Override
    public void close() throws IOException {
        this.file = null;
        this.mark = -1L;
        this.tmp.addResource(this.in);
        this.tmp.close();
    }
    
    @Override
    protected void afterRead(final int n) {
        if (n != -1) {
            this.position += n;
        }
    }
    
    @Override
    public String toString() {
        String str = "TikaInputStream of ";
        if (this.hasFile()) {
            str += this.file.toString();
        }
        else {
            str += this.in.toString();
        }
        if (this.openContainer != null) {
            str = str + " (in " + this.openContainer + ")";
        }
        return str;
    }
}
