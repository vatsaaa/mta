// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.xmp;

import java.io.ObjectOutputStream;
import java.io.IOException;
import java.io.NotSerializableException;
import java.io.ObjectInputStream;
import com.adobe.xmp.options.SerializeOptions;
import com.adobe.xmp.XMPIterator;
import com.adobe.xmp.options.IteratorOptions;
import java.util.Enumeration;
import java.util.Properties;
import com.adobe.xmp.options.PropertyOptions;
import org.apache.tika.metadata.PropertyTypeException;
import java.util.Calendar;
import com.adobe.xmp.XMPDateTime;
import java.util.Date;
import com.adobe.xmp.XMPUtils;
import com.adobe.xmp.properties.XMPProperty;
import org.apache.tika.metadata.Property;
import java.util.Map;
import com.adobe.xmp.XMPException;
import org.apache.tika.exception.TikaException;
import org.apache.tika.xmp.convert.TikaToXMP;
import com.adobe.xmp.XMPMetaFactory;
import com.adobe.xmp.XMPSchemaRegistry;
import com.adobe.xmp.XMPMeta;
import org.apache.tika.metadata.Metadata;

public class XMPMetadata extends Metadata
{
    private XMPMeta xmpData;
    private static final XMPSchemaRegistry registry;
    
    public XMPMetadata() {
        this.xmpData = XMPMetaFactory.create();
    }
    
    public XMPMetadata(final Metadata meta) throws TikaException {
        this.xmpData = TikaToXMP.convert(meta);
    }
    
    public XMPMetadata(final Metadata meta, final String mimetype) throws TikaException {
        this.xmpData = TikaToXMP.convert(meta, mimetype);
    }
    
    public void process(final Metadata meta) throws TikaException {
        this.xmpData = TikaToXMP.convert(meta);
    }
    
    public void process(final Metadata meta, final String mimetype) throws TikaException {
        this.xmpData = TikaToXMP.convert(meta, mimetype);
    }
    
    public XMPMeta getXMPData() {
        return this.xmpData;
    }
    
    public static String registerNamespace(final String namespaceURI, final String suggestedPrefix) throws XMPException {
        return XMPMetadata.registry.registerNamespace(namespaceURI, suggestedPrefix);
    }
    
    public static String getNamespacePrefix(final String namespaceURI) {
        return XMPMetadata.registry.getNamespacePrefix(namespaceURI);
    }
    
    public static String getNamespaceURI(final String namespacePrefix) {
        return XMPMetadata.registry.getNamespaceURI(namespacePrefix);
    }
    
    public static Map<String, String> getNamespaces() {
        return (Map<String, String>)XMPMetadata.registry.getNamespaces();
    }
    
    public static Map<String, String> getPrefixes() {
        return (Map<String, String>)XMPMetadata.registry.getPrefixes();
    }
    
    public static void deleteNamespace(final String namespaceURI) {
        XMPMetadata.registry.deleteNamespace(namespaceURI);
    }
    
    @Override
    public boolean isMultiValued(final Property property) {
        return this.isMultiValued(property.getName());
    }
    
    @Override
    public boolean isMultiValued(final String name) {
        this.checkKey(name);
        final String[] keyParts = this.splitKey(name);
        final String ns = XMPMetadata.registry.getNamespaceURI(keyParts[0]);
        if (ns != null) {
            try {
                final XMPProperty prop = this.xmpData.getProperty(ns, keyParts[1]);
                return prop.getOptions().isArray();
            }
            catch (XMPException ex) {}
        }
        return false;
    }
    
    @Override
    public String[] names() {
        throw new UnsupportedOperationException("Not implemented");
    }
    
    @Override
    public String get(final String name) {
        this.checkKey(name);
        String value = null;
        final String[] keyParts = this.splitKey(name);
        final String ns = XMPMetadata.registry.getNamespaceURI(keyParts[0]);
        if (ns != null) {
            try {
                XMPProperty prop = this.xmpData.getProperty(ns, keyParts[1]);
                if (prop != null && prop.getOptions().isSimple()) {
                    value = prop.getValue();
                }
                else if (prop != null && prop.getOptions().isArray()) {
                    prop = this.xmpData.getArrayItem(ns, keyParts[1], 1);
                    value = prop.getValue();
                }
            }
            catch (XMPException ex) {}
        }
        return value;
    }
    
    @Override
    public String get(final Property property) {
        return this.get(property.getName());
    }
    
    @Override
    public Integer getInt(final Property property) {
        Integer result = null;
        try {
            result = new Integer(XMPUtils.convertToInteger(this.get(property.getName())));
        }
        catch (XMPException ex) {}
        return result;
    }
    
    @Override
    public Date getDate(final Property property) {
        Date result = null;
        try {
            final XMPDateTime xmpDate = XMPUtils.convertToDate(this.get(property.getName()));
            if (xmpDate != null) {
                final Calendar cal = xmpDate.getCalendar();
                result = cal.getTime();
            }
        }
        catch (XMPException ex) {}
        return result;
    }
    
    @Override
    public String[] getValues(final Property property) {
        return this.getValues(property.getName());
    }
    
    @Override
    public String[] getValues(final String name) {
        this.checkKey(name);
        String[] value = null;
        final String[] keyParts = this.splitKey(name);
        final String ns = XMPMetadata.registry.getNamespaceURI(keyParts[0]);
        if (ns != null) {
            try {
                XMPProperty prop = this.xmpData.getProperty(ns, keyParts[1]);
                if (prop != null && prop.getOptions().isSimple()) {
                    value = new String[] { prop.getValue() };
                }
                else if (prop != null && prop.getOptions().isArray()) {
                    final int size = this.xmpData.countArrayItems(ns, keyParts[1]);
                    value = new String[size];
                    boolean onlySimpleChildren = true;
                    for (int i = 0; i < size && onlySimpleChildren; ++i) {
                        prop = this.xmpData.getArrayItem(ns, keyParts[1], i + 1);
                        if (prop.getOptions().isSimple()) {
                            value[i] = prop.getValue();
                        }
                        else {
                            onlySimpleChildren = false;
                        }
                    }
                    if (!onlySimpleChildren) {
                        value = null;
                    }
                }
            }
            catch (XMPException ex) {}
        }
        return value;
    }
    
    @Override
    public void add(final String name, final String value) {
        this.set(name, value);
    }
    
    @Override
    public void set(final String name, final String value) {
        this.checkKey(name);
        final String[] keyParts = this.splitKey(name);
        final String ns = XMPMetadata.registry.getNamespaceURI(keyParts[0]);
        if (ns != null) {
            try {
                this.xmpData.setProperty(ns, keyParts[1], value);
            }
            catch (XMPException ex) {}
        }
    }
    
    @Override
    public void set(final Property property, final String value) {
        this.set(property.getName(), value);
    }
    
    @Override
    public void set(final Property property, final int value) {
        super.set(property, value);
    }
    
    @Override
    public void set(final Property property, final double value) {
        super.set(property, value);
    }
    
    @Override
    public void set(final Property property, final Date date) {
        super.set(property, date);
    }
    
    @Override
    public void set(final Property property, final String[] values) {
        this.checkKey(property.getName());
        if (!property.isMultiValuePermitted()) {
            throw new PropertyTypeException("Property is not of an array type");
        }
        final String[] keyParts = this.splitKey(property.getName());
        final String ns = XMPMetadata.registry.getNamespaceURI(keyParts[0]);
        if (ns != null) {
            try {
                final int arrayType = this.tikaToXMPArrayType(property.getPrimaryProperty().getPropertyType());
                this.xmpData.setProperty(ns, keyParts[1], null, new PropertyOptions(arrayType));
                for (final String value : values) {
                    this.xmpData.appendArrayItem(ns, keyParts[1], value);
                }
            }
            catch (XMPException ex) {}
        }
    }
    
    @Override
    public void setAll(final Properties properties) {
        final Enumeration<String> names = (Enumeration<String>)properties.propertyNames();
        while (names.hasMoreElements()) {
            final String name = names.nextElement();
            final Property property = Property.get(name);
            if (property == null) {
                throw new PropertyTypeException("Unknown property: " + name);
            }
            final String value = properties.getProperty(name);
            if (property.isMultiValuePermitted()) {
                this.set(property, new String[] { value });
            }
            else {
                this.set(property, value);
            }
        }
    }
    
    public void remove(final Property property) {
        this.remove(property.getName());
    }
    
    @Override
    public void remove(final String name) {
        this.checkKey(name);
        final String[] keyParts = this.splitKey(name);
        final String ns = XMPMetadata.registry.getNamespaceURI(keyParts[0]);
        if (ns != null) {
            this.xmpData.deleteProperty(ns, keyParts[1]);
        }
    }
    
    @Override
    public int size() {
        int size = 0;
        try {
            final XMPIterator nsIter = this.xmpData.iterator(new IteratorOptions().setJustChildren(true).setOmitQualifiers(true));
            while (nsIter.hasNext()) {
                nsIter.next();
                ++size;
            }
        }
        catch (XMPException ex) {}
        return size;
    }
    
    @Override
    public boolean equals(final Object o) {
        throw new UnsupportedOperationException("Not implemented");
    }
    
    @Override
    public String toString() {
        String result = null;
        try {
            result = XMPMetaFactory.serializeToString(this.xmpData, new SerializeOptions().setOmitPacketWrapper(true).setUseCompactFormat(true));
        }
        catch (XMPException ex) {}
        return result;
    }
    
    private void readObject(final ObjectInputStream ois) throws ClassNotFoundException, IOException {
        throw new NotSerializableException();
    }
    
    private void writeObject(final ObjectOutputStream ois) throws IOException {
        throw new NotSerializableException();
    }
    
    private void checkKey(final String key) throws PropertyTypeException {
        if (key == null || key.length() == 0) {
            throw new PropertyTypeException("Key must not be null");
        }
        final String[] keyParts = this.splitKey(key);
        if (keyParts == null) {
            throw new PropertyTypeException("Key must be a QName in the form prefix:localName");
        }
        if (XMPMetadata.registry.getNamespaceURI(keyParts[0]) == null) {
            throw new PropertyTypeException("Key does not use a registered Namespace prefix");
        }
    }
    
    private String[] splitKey(final String key) {
        final String[] keyParts = key.split(":");
        if (keyParts.length > 0 && keyParts.length <= 2) {
            return keyParts;
        }
        return null;
    }
    
    private int tikaToXMPArrayType(final Property.PropertyType type) {
        int result = 0;
        switch (type) {
            case BAG: {
                result = 512;
                break;
            }
            case SEQ: {
                result = 1024;
                break;
            }
            case ALT: {
                result = 2048;
                break;
            }
        }
        return result;
    }
    
    static {
        registry = XMPMetaFactory.getSchemaRegistry();
    }
}
