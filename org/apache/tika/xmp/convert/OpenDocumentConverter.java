// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.xmp.convert;

import java.util.Collections;
import java.util.Collection;
import java.util.HashSet;
import java.util.Arrays;
import com.adobe.xmp.XMPException;
import org.apache.tika.metadata.PagedText;
import org.apache.tika.metadata.TikaCoreProperties;
import org.apache.tika.metadata.Office;
import com.adobe.xmp.XMPMeta;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.exception.TikaException;
import java.util.Set;

public class OpenDocumentConverter extends AbstractConverter
{
    protected static final Set<Namespace> ADDITIONAL_NAMESPACES;
    
    public OpenDocumentConverter() throws TikaException {
    }
    
    @Override
    public XMPMeta process(final Metadata metadata) throws XMPException {
        super.setMetadata(metadata);
        this.createProperty("Content-Type", "http://purl.org/dc/elements/1.1/", "format");
        this.createProperty(Office.CHARACTER_COUNT, "urn:oasis:names:tc:opendocument:xmlns:meta:1.0", "character-count");
        this.createProperty(TikaCoreProperties.CREATED, "http://ns.adobe.com/xap/1.0/", "CreateDate");
        this.createCommaSeparatedArray(TikaCoreProperties.CREATOR, "http://purl.org/dc/elements/1.1/", "creator", 1024);
        this.createProperty(TikaCoreProperties.MODIFIED, "http://ns.adobe.com/xap/1.0/", "ModifyDate");
        this.createProperty(TikaCoreProperties.COMMENTS, "http://ns.adobe.com/pdfx/1.3/", "Comments");
        this.createCommaSeparatedArray(TikaCoreProperties.KEYWORDS, "http://purl.org/dc/elements/1.1/", "subject", 512);
        this.createLangAltProperty(TikaCoreProperties.DESCRIPTION, "http://purl.org/dc/elements/1.1/", "description");
        this.createProperty("Edit-Time", "urn:oasis:names:tc:opendocument:xmlns:meta:1.0", "editing-duration");
        this.createProperty("editing-cycles", "urn:oasis:names:tc:opendocument:xmlns:meta:1.0", "editing-cycles");
        this.createProperty("generator", "http://ns.adobe.com/xap/1.0/", "CreatorTool");
        this.createProperty(Office.IMAGE_COUNT, "urn:oasis:names:tc:opendocument:xmlns:meta:1.0", "image-count");
        this.createProperty("initial-creator", "urn:oasis:names:tc:opendocument:xmlns:meta:1.0", "initial-creator");
        this.createProperty(Office.OBJECT_COUNT, "urn:oasis:names:tc:opendocument:xmlns:meta:1.0", "object-count");
        this.createProperty(PagedText.N_PAGES, "http://ns.adobe.com/xap/1.0/t/pg/", "NPages");
        this.createProperty(Office.PARAGRAPH_COUNT, "urn:oasis:names:tc:opendocument:xmlns:meta:1.0", "paragraph-count");
        this.createProperty(Office.TABLE_COUNT, "urn:oasis:names:tc:opendocument:xmlns:meta:1.0", "table-count");
        this.createLangAltProperty(TikaCoreProperties.TITLE, "http://purl.org/dc/elements/1.1/", "title");
        this.createProperty(Office.WORD_COUNT, "urn:oasis:names:tc:opendocument:xmlns:meta:1.0", "word-count");
        return super.getXMPMeta();
    }
    
    @Override
    protected Set<Namespace> getAdditionalNamespaces() {
        return OpenDocumentConverter.ADDITIONAL_NAMESPACES;
    }
    
    static {
        ADDITIONAL_NAMESPACES = Collections.unmodifiableSet((Set<? extends Namespace>)new HashSet<Namespace>(Arrays.asList(new Namespace("urn:oasis:names:tc:opendocument:xmlns:meta:1.0", "meta"))));
    }
}
