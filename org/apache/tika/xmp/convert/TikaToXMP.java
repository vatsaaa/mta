// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.xmp.convert;

import java.util.Iterator;
import java.util.Set;
import org.apache.tika.parser.odf.OpenDocumentParser;
import org.apache.tika.parser.microsoft.ooxml.OOXMLParser;
import org.apache.tika.parser.rtf.RTFParser;
import org.apache.tika.parser.microsoft.OfficeParser;
import org.apache.tika.parser.ParseContext;
import java.util.HashMap;
import com.adobe.xmp.XMPMetaFactory;
import com.adobe.xmp.XMPException;
import org.apache.tika.exception.TikaException;
import org.apache.tika.metadata.TikaCoreProperties;
import com.adobe.xmp.XMPMeta;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.mime.MediaType;
import java.util.Map;

public class TikaToXMP
{
    private static Map<MediaType, Class<? extends ITikaToXMPConverter>> converterMap;
    
    public static XMPMeta convert(final Metadata tikaMetadata) throws TikaException {
        if (tikaMetadata == null) {
            throw new IllegalArgumentException("Metadata parameter must not be null");
        }
        String mimetype = tikaMetadata.get("Content-Type");
        if (mimetype == null) {
            mimetype = tikaMetadata.get(TikaCoreProperties.FORMAT);
        }
        return convert(tikaMetadata, mimetype);
    }
    
    public static XMPMeta convert(final Metadata tikaMetadata, final String mimetype) throws TikaException {
        if (tikaMetadata == null) {
            throw new IllegalArgumentException("Metadata parameter must not be null");
        }
        ITikaToXMPConverter converter = null;
        if (isConverterAvailable(mimetype)) {
            converter = getConverter(mimetype);
        }
        else {
            converter = new GenericConverter();
        }
        XMPMeta xmp = null;
        if (converter != null) {
            try {
                xmp = converter.process(tikaMetadata);
                return xmp;
            }
            catch (XMPException e) {
                throw new TikaException("Tika metadata could not be converted to XMP", e);
            }
        }
        xmp = XMPMetaFactory.create();
        return xmp;
    }
    
    public static boolean isConverterAvailable(final String mimetype) {
        final MediaType type = MediaType.parse(mimetype);
        return type != null && getConverterMap().get(type) != null;
    }
    
    public static ITikaToXMPConverter getConverter(final String mimetype) throws TikaException {
        if (mimetype == null) {
            throw new IllegalArgumentException("mimetype must not be null");
        }
        ITikaToXMPConverter converter = null;
        final MediaType type = MediaType.parse(mimetype);
        if (type != null) {
            final Class<? extends ITikaToXMPConverter> clazz = getConverterMap().get(type);
            if (clazz != null) {
                try {
                    converter = (ITikaToXMPConverter)clazz.newInstance();
                }
                catch (Exception e) {
                    throw new TikaException("TikaToXMP converter class cannot be instantiated for mimetype: " + type.toString(), e);
                }
            }
        }
        return converter;
    }
    
    private static Map<MediaType, Class<? extends ITikaToXMPConverter>> getConverterMap() {
        if (TikaToXMP.converterMap == null) {
            TikaToXMP.converterMap = new HashMap<MediaType, Class<? extends ITikaToXMPConverter>>();
            initialize();
        }
        return TikaToXMP.converterMap;
    }
    
    private static void initialize() {
        final ParseContext parseContext = new ParseContext();
        addConverter(new OfficeParser().getSupportedTypes(parseContext), MSOfficeBinaryConverter.class);
        addConverter(new RTFParser().getSupportedTypes(parseContext), RTFConverter.class);
        addConverter(new OOXMLParser().getSupportedTypes(parseContext), MSOfficeXMLConverter.class);
        addConverter(new OpenDocumentParser().getSupportedTypes(parseContext), OpenDocumentConverter.class);
    }
    
    private static void addConverter(final Set<MediaType> supportedTypes, final Class<? extends ITikaToXMPConverter> converter) {
        for (final MediaType type : supportedTypes) {
            getConverterMap().put(type, converter);
        }
    }
}
