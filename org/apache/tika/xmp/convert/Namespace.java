// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.xmp.convert;

public class Namespace
{
    public String uri;
    public String prefix;
    
    public Namespace(final String uri, final String prefix) {
        this.uri = uri;
        this.prefix = prefix;
    }
}
