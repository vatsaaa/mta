// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.xmp.convert;

import com.adobe.xmp.XMPException;
import com.adobe.xmp.XMPMeta;
import org.apache.tika.metadata.Metadata;

public interface ITikaToXMPConverter
{
    XMPMeta process(final Metadata p0) throws XMPException;
}
