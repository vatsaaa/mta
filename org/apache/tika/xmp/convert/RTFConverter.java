// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.xmp.convert;

import java.util.Collections;
import java.util.Collection;
import java.util.HashSet;
import java.util.Arrays;
import com.adobe.xmp.XMPException;
import org.apache.tika.metadata.OfficeOpenXMLExtended;
import org.apache.tika.metadata.OfficeOpenXMLCore;
import org.apache.tika.metadata.TikaCoreProperties;
import com.adobe.xmp.XMPMeta;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.exception.TikaException;
import java.util.Set;

public class RTFConverter extends AbstractConverter
{
    protected static final Set<Namespace> ADDITIONAL_NAMESPACES;
    
    public RTFConverter() throws TikaException {
    }
    
    @Override
    public XMPMeta process(final Metadata metadata) throws XMPException {
        this.setMetadata(metadata);
        this.createProperty("Content-Type", "http://purl.org/dc/elements/1.1/", "format");
        this.createCommaSeparatedArray(TikaCoreProperties.CREATOR, "http://purl.org/dc/elements/1.1/", "creator", 1024);
        this.createLangAltProperty(TikaCoreProperties.TITLE, "http://purl.org/dc/elements/1.1/", "title");
        this.createLangAltProperty(TikaCoreProperties.DESCRIPTION, "http://purl.org/dc/elements/1.1/", "description");
        this.createCommaSeparatedArray(TikaCoreProperties.KEYWORDS, "http://purl.org/dc/elements/1.1/", "subject", 512);
        this.createProperty(OfficeOpenXMLCore.CATEGORY, "http://iptc.org/std/Iptc4xmpCore/1.0/xmlns/", "intellectualGenre");
        this.createProperty(OfficeOpenXMLExtended.TEMPLATE, "http://schemas.openxmlformats.org/officeDocument/2006/extended-properties/", "Template");
        this.createProperty(TikaCoreProperties.COMMENTS, "http://ns.adobe.com/pdfx/1.3/", "Comments");
        this.createProperty(OfficeOpenXMLExtended.COMPANY, "http://schemas.openxmlformats.org/officeDocument/2006/extended-properties/", "Company");
        this.createProperty(OfficeOpenXMLExtended.MANAGER, "http://schemas.openxmlformats.org/officeDocument/2006/extended-properties/", "Manager");
        return this.getXMPMeta();
    }
    
    @Override
    protected Set<Namespace> getAdditionalNamespaces() {
        return RTFConverter.ADDITIONAL_NAMESPACES;
    }
    
    static {
        ADDITIONAL_NAMESPACES = Collections.unmodifiableSet((Set<? extends Namespace>)new HashSet<Namespace>(Arrays.asList(new Namespace("http://schemas.openxmlformats.org/officeDocument/2006/extended-properties/", "extended-properties"))));
    }
}
