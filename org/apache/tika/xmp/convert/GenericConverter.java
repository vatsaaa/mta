// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.xmp.convert;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import com.adobe.xmp.XMPException;
import com.adobe.xmp.XMPSchemaRegistry;
import org.apache.tika.metadata.Property;
import org.apache.tika.metadata.XMPRights;
import org.apache.tika.metadata.DublinCore;
import com.adobe.xmp.XMPMetaFactory;
import com.adobe.xmp.XMPMeta;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.exception.TikaException;

public class GenericConverter extends AbstractConverter
{
    public GenericConverter() throws TikaException {
    }
    
    @Override
    public XMPMeta process(final Metadata metadata) throws XMPException {
        this.setMetadata(metadata);
        final XMPSchemaRegistry registry = XMPMetaFactory.getSchemaRegistry();
        final String[] arr$;
        final String[] keys = arr$ = metadata.names();
        for (final String key : arr$) {
            final String[] keyParts = key.split(":");
            if (keyParts.length > 0 && keyParts.length <= 2) {
                final String uri = registry.getNamespaceURI(keyParts[0]);
                if (uri != null) {
                    if (key.equals(DublinCore.TITLE.getName()) || key.equals(DublinCore.DESCRIPTION.getName()) || key.equals(XMPRights.USAGE_TERMS.getName())) {
                        this.createLangAltProperty(key, uri, keyParts[1]);
                    }
                    else if (key.equals(DublinCore.CREATOR.getName())) {
                        this.createArrayProperty(key, uri, keyParts[1], 1024);
                    }
                    else {
                        final Property.PropertyType type = Property.getPropertyType(key);
                        if (type != null) {
                            switch (type) {
                                case SIMPLE: {
                                    this.createProperty(key, uri, keyParts[1]);
                                    break;
                                }
                                case BAG: {
                                    this.createArrayProperty(key, uri, keyParts[1], 512);
                                    break;
                                }
                                case SEQ: {
                                    this.createArrayProperty(key, uri, keyParts[1], 1024);
                                    break;
                                }
                                case ALT: {
                                    this.createArrayProperty(key, uri, keyParts[1], 2048);
                                    break;
                                }
                            }
                        }
                    }
                }
            }
        }
        return this.getXMPMeta();
    }
    
    public Set<Namespace> getAdditionalNamespaces() {
        return Collections.unmodifiableSet((Set<? extends Namespace>)new HashSet<Namespace>());
    }
}
