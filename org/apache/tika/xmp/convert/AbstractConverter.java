// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.xmp.convert;

import com.adobe.xmp.XMPUtils;
import com.adobe.xmp.options.PropertyOptions;
import org.apache.tika.metadata.Property;
import java.util.Iterator;
import com.adobe.xmp.XMPSchemaRegistry;
import org.apache.tika.exception.TikaException;
import com.adobe.xmp.XMPMetaFactory;
import java.util.Set;
import com.adobe.xmp.XMPException;
import com.adobe.xmp.XMPMeta;
import org.apache.tika.metadata.Metadata;

public abstract class AbstractConverter implements ITikaToXMPConverter
{
    private Metadata metadata;
    protected XMPMeta meta;
    
    public abstract XMPMeta process(final Metadata p0) throws XMPException;
    
    protected abstract Set<Namespace> getAdditionalNamespaces();
    
    public AbstractConverter() throws TikaException {
        this.meta = XMPMetaFactory.create();
        this.metadata = new Metadata();
        this.registerNamespaces(this.getAdditionalNamespaces());
    }
    
    public void setMetadata(final Metadata metadata) {
        this.metadata = metadata;
    }
    
    public XMPMeta getXMPMeta() {
        return this.meta;
    }
    
    protected void registerNamespaces(final Set<Namespace> namespaces) throws TikaException {
        final XMPSchemaRegistry registry = XMPMetaFactory.getSchemaRegistry();
        for (final Namespace namespace : namespaces) {
            try {
                registry.registerNamespace(namespace.uri, namespace.prefix);
            }
            catch (XMPException e) {
                throw new TikaException("Namespace needed by converter could not be registiered with XMPCore", e);
            }
        }
    }
    
    protected void createProperty(final Property metadataProperty, final String ns, final String propertyName) throws XMPException {
        this.createProperty(metadataProperty.getName(), ns, propertyName);
    }
    
    protected void createProperty(final String tikaKey, final String ns, final String propertyName) throws XMPException {
        final String value = this.metadata.get(tikaKey);
        if (value != null && value.length() > 0) {
            this.meta.setProperty(ns, propertyName, value);
        }
    }
    
    protected void createLangAltProperty(final Property metadataProperty, final String ns, final String propertyName) throws XMPException {
        this.createLangAltProperty(metadataProperty.getName(), ns, propertyName);
    }
    
    protected void createLangAltProperty(final String tikaKey, final String ns, final String propertyName) throws XMPException {
        final String value = this.metadata.get(tikaKey);
        if (value != null && value.length() > 0) {
            this.meta.setLocalizedText(ns, propertyName, null, "x-default", value);
        }
    }
    
    protected void createArrayProperty(final Property metadataProperty, final String nsDc, final String arrayProperty, final int arrayType) throws XMPException {
        this.createArrayProperty(metadataProperty.getName(), nsDc, arrayProperty, arrayType);
    }
    
    protected void createArrayProperty(final String tikaKey, final String ns, final String propertyName, final int arrayType) throws XMPException {
        final String[] values = this.metadata.getValues(tikaKey);
        if (values != null) {
            this.meta.setProperty(ns, propertyName, null, new PropertyOptions(arrayType));
            for (final String value : values) {
                this.meta.appendArrayItem(ns, propertyName, value);
            }
        }
    }
    
    protected void createCommaSeparatedArray(final Property metadataProperty, final String nsDc, final String arrayProperty, final int arrayType) throws XMPException {
        this.createCommaSeparatedArray(metadataProperty.getName(), nsDc, arrayProperty, arrayType);
    }
    
    protected void createCommaSeparatedArray(final String tikaKey, final String ns, final String propertyName, final int arrayType) throws XMPException {
        final String value = this.metadata.get(tikaKey);
        if (value != null && value.length() > 0) {
            XMPUtils.separateArrayItems(this.meta, ns, propertyName, value, new PropertyOptions(arrayType), false);
        }
    }
}
