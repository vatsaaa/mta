// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.xmp.convert;

import java.util.Collections;
import java.util.Collection;
import java.util.HashSet;
import java.util.Arrays;
import com.adobe.xmp.XMPException;
import org.apache.tika.metadata.Office;
import org.apache.tika.metadata.OfficeOpenXMLExtended;
import org.apache.tika.metadata.TikaCoreProperties;
import org.apache.tika.metadata.OfficeOpenXMLCore;
import com.adobe.xmp.XMPMeta;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.exception.TikaException;
import java.util.Set;

public class MSOfficeXMLConverter extends AbstractConverter
{
    protected static final Set<Namespace> ADDITIONAL_NAMESPACES;
    
    public MSOfficeXMLConverter() throws TikaException {
    }
    
    @Override
    public XMPMeta process(final Metadata metadata) throws XMPException {
        super.setMetadata(metadata);
        this.createProperty("Content-Type", "http://purl.org/dc/elements/1.1/", "format");
        this.createProperty(OfficeOpenXMLCore.CATEGORY, "http://iptc.org/std/Iptc4xmpCore/1.0/xmlns/", "intellectualGenre");
        this.createProperty(OfficeOpenXMLCore.CONTENT_STATUS, "http://schemas.openxmlformats.org/package/2006/metadata/core-properties/", "contentStatus");
        this.createProperty(TikaCoreProperties.CREATED, "http://ns.adobe.com/xap/1.0/", "CreateDate");
        this.createCommaSeparatedArray(TikaCoreProperties.CREATOR, "http://purl.org/dc/elements/1.1/", "creator", 1024);
        this.createProperty(TikaCoreProperties.COMMENTS, "http://ns.adobe.com/pdfx/1.3/", "Comments");
        this.createProperty(TikaCoreProperties.IDENTIFIER, "http://purl.org/dc/elements/1.1/", "identifier");
        this.createCommaSeparatedArray(TikaCoreProperties.KEYWORDS, "http://purl.org/dc/elements/1.1/", "subject", 512);
        this.createLangAltProperty(TikaCoreProperties.DESCRIPTION, "http://purl.org/dc/elements/1.1/", "description");
        this.createProperty(TikaCoreProperties.LANGUAGE, "http://purl.org/dc/elements/1.1/", "language");
        this.createProperty(TikaCoreProperties.MODIFIER, "http://schemas.openxmlformats.org/package/2006/metadata/core-properties/", "lastModifiedBy");
        this.createProperty(TikaCoreProperties.PRINT_DATE, "http://schemas.openxmlformats.org/package/2006/metadata/core-properties/", "lastPrinted");
        this.createProperty(TikaCoreProperties.MODIFIED, "http://ns.adobe.com/xap/1.0/", "ModifyDate");
        this.createProperty(OfficeOpenXMLCore.REVISION, "http://schemas.openxmlformats.org/package/2006/metadata/core-properties/", "revision");
        this.createLangAltProperty(TikaCoreProperties.TITLE, "http://purl.org/dc/elements/1.1/", "title");
        this.createProperty(OfficeOpenXMLCore.VERSION, "http://schemas.openxmlformats.org/package/2006/metadata/core-properties/", "version");
        String creatorTool = "";
        String value = metadata.get(OfficeOpenXMLExtended.APPLICATION);
        if (value != null && value.length() > 0) {
            creatorTool = value;
            value = metadata.get(OfficeOpenXMLExtended.APP_VERSION);
            if (value != null && value.length() > 0) {
                creatorTool = creatorTool + " " + value;
            }
        }
        if (creatorTool.length() > 0) {
            this.meta.setProperty("http://ns.adobe.com/xap/1.0/", "CreatorTool", creatorTool);
        }
        this.createProperty(Office.CHARACTER_COUNT, "http://schemas.openxmlformats.org/officeDocument/2006/extended-properties/", "Characters");
        this.createProperty(Office.CHARACTER_COUNT_WITH_SPACES, "http://schemas.openxmlformats.org/officeDocument/2006/extended-properties/", "CharactersWithSpaces");
        this.createProperty(TikaCoreProperties.PUBLISHER, "http://schemas.openxmlformats.org/officeDocument/2006/extended-properties/", "Company");
        this.createProperty(Office.LINE_COUNT, "http://schemas.openxmlformats.org/officeDocument/2006/extended-properties/", "Lines");
        this.createProperty(OfficeOpenXMLExtended.MANAGER, "http://schemas.openxmlformats.org/officeDocument/2006/extended-properties/", "Manager");
        this.createProperty(OfficeOpenXMLExtended.NOTES, "http://schemas.openxmlformats.org/officeDocument/2006/extended-properties/", "Notes");
        this.createProperty(Office.PAGE_COUNT, "http://ns.adobe.com/xap/1.0/t/pg/", "NPages");
        this.createProperty(Office.PARAGRAPH_COUNT, "http://schemas.openxmlformats.org/officeDocument/2006/extended-properties/", "Paragraphs");
        this.createProperty(OfficeOpenXMLExtended.PRESENTATION_FORMAT, "http://schemas.openxmlformats.org/officeDocument/2006/extended-properties/", "PresentationFormat");
        this.createProperty(Office.SLIDE_COUNT, "http://schemas.openxmlformats.org/officeDocument/2006/extended-properties/", "Slides");
        this.createProperty(OfficeOpenXMLExtended.TEMPLATE, "http://schemas.openxmlformats.org/officeDocument/2006/extended-properties/", "Template");
        this.createProperty(OfficeOpenXMLExtended.TOTAL_TIME, "http://schemas.openxmlformats.org/officeDocument/2006/extended-properties/", "TotalTime");
        this.createProperty(Office.WORD_COUNT, "http://schemas.openxmlformats.org/officeDocument/2006/extended-properties/", "Words");
        return super.getXMPMeta();
    }
    
    @Override
    protected Set<Namespace> getAdditionalNamespaces() {
        return MSOfficeXMLConverter.ADDITIONAL_NAMESPACES;
    }
    
    static {
        ADDITIONAL_NAMESPACES = Collections.unmodifiableSet((Set<? extends Namespace>)new HashSet<Namespace>(Arrays.asList(new Namespace("http://schemas.openxmlformats.org/package/2006/metadata/core-properties/", "cp"), new Namespace("http://schemas.openxmlformats.org/officeDocument/2006/extended-properties/", "extended-properties"))));
    }
}
