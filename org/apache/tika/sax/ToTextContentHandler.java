// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.sax;

import java.io.IOException;
import org.xml.sax.SAXException;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.io.OutputStreamWriter;
import java.io.OutputStream;
import java.io.Writer;
import org.xml.sax.helpers.DefaultHandler;

public class ToTextContentHandler extends DefaultHandler
{
    private final Writer writer;
    
    public ToTextContentHandler(final Writer writer) {
        this.writer = writer;
    }
    
    public ToTextContentHandler(final OutputStream stream) {
        this(new OutputStreamWriter(stream));
    }
    
    public ToTextContentHandler(final OutputStream stream, final String encoding) throws UnsupportedEncodingException {
        this(new OutputStreamWriter(stream, encoding));
    }
    
    public ToTextContentHandler() {
        this(new StringWriter());
    }
    
    @Override
    public void characters(final char[] ch, final int start, final int length) throws SAXException {
        try {
            this.writer.write(ch, start, length);
        }
        catch (IOException e) {
            throw new SAXException("Error writing: " + new String(ch, start, length), e);
        }
    }
    
    @Override
    public void ignorableWhitespace(final char[] ch, final int start, final int length) throws SAXException {
        this.characters(ch, start, length);
    }
    
    @Override
    public void endDocument() throws SAXException {
        try {
            this.writer.flush();
        }
        catch (IOException e) {
            throw new SAXException("Error flushing character output", e);
        }
    }
    
    @Override
    public String toString() {
        return this.writer.toString();
    }
}
