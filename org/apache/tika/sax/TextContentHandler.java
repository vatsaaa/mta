// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.sax;

import org.xml.sax.SAXException;
import org.xml.sax.ContentHandler;
import org.xml.sax.helpers.DefaultHandler;

public class TextContentHandler extends DefaultHandler
{
    private final ContentHandler delegate;
    
    public TextContentHandler(final ContentHandler delegate) {
        this.delegate = delegate;
    }
    
    @Override
    public void characters(final char[] ch, final int start, final int length) throws SAXException {
        this.delegate.characters(ch, start, length);
    }
    
    @Override
    public void ignorableWhitespace(final char[] ch, final int start, final int length) throws SAXException {
        this.delegate.ignorableWhitespace(ch, start, length);
    }
    
    @Override
    public void startDocument() throws SAXException {
        this.delegate.startDocument();
    }
    
    @Override
    public void endDocument() throws SAXException {
        this.delegate.endDocument();
    }
    
    @Override
    public String toString() {
        return this.delegate.toString();
    }
}
