// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.sax.xpath;

public class NamedAttributeMatcher extends Matcher
{
    private final String namespace;
    private final String name;
    
    public NamedAttributeMatcher(final String namespace, final String name) {
        this.namespace = namespace;
        this.name = name;
    }
    
    @Override
    public boolean matchesAttribute(final String namespace, final String name) {
        return equals(namespace, this.namespace) && name.equals(this.name);
    }
    
    private static boolean equals(final String a, final String b) {
        return (a == null) ? (b == null) : a.equals(b);
    }
}
