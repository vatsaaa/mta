// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.sax;

import java.util.Iterator;
import org.xml.sax.Attributes;
import java.util.ArrayList;
import java.util.List;
import java.util.LinkedList;
import org.xml.sax.helpers.DefaultHandler;

public class LinkContentHandler extends DefaultHandler
{
    private final LinkedList<LinkBuilder> builderStack;
    private final List<Link> links;
    
    public LinkContentHandler() {
        this.builderStack = new LinkedList<LinkBuilder>();
        this.links = new ArrayList<Link>();
    }
    
    public List<Link> getLinks() {
        return this.links;
    }
    
    @Override
    public void startElement(final String uri, final String local, final String name, final Attributes attributes) {
        if ("http://www.w3.org/1999/xhtml".equals(uri)) {
            if ("a".equals(local)) {
                final LinkBuilder builder = new LinkBuilder("a");
                builder.setURI(attributes.getValue("", "href"));
                builder.setTitle(attributes.getValue("", "title"));
                builder.setRel(attributes.getValue("", "rel"));
                this.builderStack.addFirst(builder);
            }
            else if ("img".equals(local)) {
                final LinkBuilder builder = new LinkBuilder("img");
                builder.setURI(attributes.getValue("", "src"));
                builder.setTitle(attributes.getValue("", "title"));
                builder.setRel(attributes.getValue("", "rel"));
                this.builderStack.addFirst(builder);
                final String alt = attributes.getValue("", "alt");
                if (alt != null) {
                    final char[] ch = alt.toCharArray();
                    this.characters(ch, 0, ch.length);
                }
            }
        }
    }
    
    @Override
    public void characters(final char[] ch, final int start, final int length) {
        for (final LinkBuilder builder : this.builderStack) {
            builder.characters(ch, start, length);
        }
    }
    
    @Override
    public void ignorableWhitespace(final char[] ch, final int start, final int length) {
        this.characters(ch, start, length);
    }
    
    @Override
    public void endElement(final String uri, final String local, final String name) {
        if ("http://www.w3.org/1999/xhtml".equals(uri) && ("a".equals(local) || "img".equals(local))) {
            this.links.add(this.builderStack.removeFirst().getLink());
        }
    }
}
