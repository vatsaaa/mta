// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.sax;

import org.xml.sax.SAXException;
import java.io.StringWriter;
import java.io.OutputStreamWriter;
import java.io.OutputStream;
import java.io.Writer;
import java.util.UUID;
import org.xml.sax.ContentHandler;
import java.io.Serializable;

public class WriteOutContentHandler extends ContentHandlerDecorator
{
    private final Serializable tag;
    private final int writeLimit;
    private int writeCount;
    
    public WriteOutContentHandler(final ContentHandler handler, final int writeLimit) {
        super(handler);
        this.tag = UUID.randomUUID();
        this.writeCount = 0;
        this.writeLimit = writeLimit;
    }
    
    public WriteOutContentHandler(final Writer writer, final int writeLimit) {
        this(new ToTextContentHandler(writer), writeLimit);
    }
    
    public WriteOutContentHandler(final Writer writer) {
        this(writer, -1);
    }
    
    public WriteOutContentHandler(final OutputStream stream) {
        this(new OutputStreamWriter(stream));
    }
    
    public WriteOutContentHandler(final int writeLimit) {
        this(new StringWriter(), writeLimit);
    }
    
    public WriteOutContentHandler() {
        this(100000);
    }
    
    @Override
    public void characters(final char[] ch, final int start, final int length) throws SAXException {
        if (this.writeLimit == -1 || this.writeCount + length <= this.writeLimit) {
            super.characters(ch, start, length);
            this.writeCount += length;
            return;
        }
        super.characters(ch, start, this.writeLimit - this.writeCount);
        this.writeCount = this.writeLimit;
        throw new WriteLimitReachedException("Your document contained more than " + this.writeLimit + " characters, and so your requested limit has been" + " reached. To receive the full text of the document," + " increase your limit. (Text up to the limit is" + " however available).", this.tag);
    }
    
    @Override
    public void ignorableWhitespace(final char[] ch, final int start, final int length) throws SAXException {
        if (this.writeLimit == -1 || this.writeCount + length <= this.writeLimit) {
            super.ignorableWhitespace(ch, start, length);
            this.writeCount += length;
            return;
        }
        super.ignorableWhitespace(ch, start, this.writeLimit - this.writeCount);
        this.writeCount = this.writeLimit;
        throw new WriteLimitReachedException("Your document contained more than " + this.writeLimit + " characters, and so your requested limit has been" + " reached. To receive the full text of the document," + " increase your limit. (Text up to the limit is" + " however available).", this.tag);
    }
    
    public boolean isWriteLimitReached(final Throwable t) {
        if (t instanceof WriteLimitReachedException) {
            return this.tag.equals(((WriteLimitReachedException)t).tag);
        }
        return t.getCause() != null && this.isWriteLimitReached(t.getCause());
    }
    
    private static class WriteLimitReachedException extends SAXException
    {
        private static final long serialVersionUID = -1850581945459429943L;
        private final Serializable tag;
        
        public WriteLimitReachedException(final String message, final Serializable tag) {
            super(message);
            this.tag = tag;
        }
    }
}
