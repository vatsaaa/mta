// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.sax;

class LinkBuilder
{
    private final String type;
    private String uri;
    private String title;
    private String rel;
    private final StringBuilder text;
    
    public LinkBuilder(final String type) {
        this.uri = "";
        this.title = "";
        this.rel = "";
        this.text = new StringBuilder();
        this.type = type;
    }
    
    public void setURI(final String uri) {
        if (uri != null) {
            this.uri = uri;
        }
        else {
            this.uri = "";
        }
    }
    
    public void setTitle(final String title) {
        if (title != null) {
            this.title = title;
        }
        else {
            this.title = "";
        }
    }
    
    public void setRel(final String rel) {
        if (rel != null) {
            this.rel = rel;
        }
        else {
            this.rel = "";
        }
    }
    
    public void characters(final char[] ch, final int offset, final int length) {
        this.text.append(ch, offset, length);
    }
    
    public Link getLink() {
        return new Link(this.type, this.uri, this.title, this.text.toString(), this.rel);
    }
}
