// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.config;

import java.util.HashMap;
import java.io.Reader;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Iterator;
import java.util.ArrayList;
import java.util.List;
import java.io.IOException;
import java.util.Collection;
import java.util.Collections;
import java.net.URL;
import java.util.Enumeration;
import java.io.InputStream;
import java.util.regex.Pattern;
import java.util.Map;

public class ServiceLoader
{
    private static volatile ClassLoader contextClassLoader;
    private static final Map<Object, Object> services;
    private final ClassLoader loader;
    private final LoadErrorHandler handler;
    private final boolean dynamic;
    private static final Pattern COMMENT;
    private static final Pattern WHITESPACE;
    
    static ClassLoader getContextClassLoader() {
        ClassLoader loader = ServiceLoader.contextClassLoader;
        if (loader == null) {
            loader = ServiceLoader.class.getClassLoader();
        }
        if (loader == null) {
            loader = ClassLoader.getSystemClassLoader();
        }
        return loader;
    }
    
    public static void setContextClassLoader(final ClassLoader loader) {
        ServiceLoader.contextClassLoader = loader;
    }
    
    static void addService(final Object reference, final Object service) {
        synchronized (ServiceLoader.services) {
            ServiceLoader.services.put(reference, service);
        }
    }
    
    static Object removeService(final Object reference) {
        synchronized (ServiceLoader.services) {
            return ServiceLoader.services.remove(reference);
        }
    }
    
    public ServiceLoader(final ClassLoader loader, final LoadErrorHandler handler, final boolean dynamic) {
        this.loader = loader;
        this.handler = handler;
        this.dynamic = dynamic;
    }
    
    public ServiceLoader(final ClassLoader loader, final LoadErrorHandler handler) {
        this(loader, handler, false);
    }
    
    public ServiceLoader(final ClassLoader loader) {
        this(loader, LoadErrorHandler.IGNORE);
    }
    
    public ServiceLoader() {
        this(getContextClassLoader(), LoadErrorHandler.IGNORE, true);
    }
    
    public InputStream getResourceAsStream(final String name) {
        if (this.loader != null) {
            return this.loader.getResourceAsStream(name);
        }
        return null;
    }
    
    public <T> Class<? extends T> getServiceClass(final Class<T> iface, final String name) throws ClassNotFoundException {
        if (this.loader == null) {
            throw new ClassNotFoundException("Service class " + name + " is not available");
        }
        final Class<?> klass = Class.forName(name, true, this.loader);
        if (klass.isInterface()) {
            throw new ClassNotFoundException("Service class " + name + " is an interface");
        }
        if (!iface.isAssignableFrom(klass)) {
            throw new ClassNotFoundException("Service class " + name + " does not implement " + iface.getName());
        }
        return (Class<? extends T>)klass;
    }
    
    public Enumeration<URL> findServiceResources(final String filePattern) {
        try {
            final Enumeration<URL> resources = this.loader.getResources(filePattern);
            return resources;
        }
        catch (IOException ignore) {
            final List<URL> empty = Collections.emptyList();
            return Collections.enumeration(empty);
        }
    }
    
    public <T> List<T> loadServiceProviders(final Class<T> iface) {
        final List<T> providers = new ArrayList<T>();
        providers.addAll((Collection<? extends T>)this.loadDynamicServiceProviders((Class<Object>)iface));
        providers.addAll((Collection<? extends T>)this.loadStaticServiceProviders((Class<Object>)iface));
        return providers;
    }
    
    public <T> List<T> loadDynamicServiceProviders(final Class<T> iface) {
        final List<T> providers = new ArrayList<T>();
        if (this.dynamic) {
            synchronized (ServiceLoader.services) {
                for (final Object service : ServiceLoader.services.values()) {
                    if (iface.isAssignableFrom(service.getClass())) {
                        providers.add((T)service);
                    }
                }
            }
        }
        return providers;
    }
    
    public <T> List<T> loadStaticServiceProviders(final Class<T> iface) {
        final List<T> providers = new ArrayList<T>();
        if (this.loader != null) {
            final List<String> names = new ArrayList<String>();
            final String serviceName = iface.getName();
            final Enumeration<URL> resources = this.findServiceResources("META-INF/services/" + serviceName);
            for (final URL resource : Collections.list(resources)) {
                try {
                    this.collectServiceClassNames(resource, names);
                }
                catch (IOException e) {
                    this.handler.handleLoadError(serviceName, e);
                }
            }
            for (final String name : names) {
                try {
                    final Class<?> klass = this.loader.loadClass(name);
                    if (!iface.isAssignableFrom(klass)) {
                        continue;
                    }
                    providers.add((T)klass.newInstance());
                }
                catch (Throwable t) {
                    this.handler.handleLoadError(name, t);
                }
            }
        }
        return providers;
    }
    
    private void collectServiceClassNames(final URL resource, final Collection<String> names) throws IOException {
        final InputStream stream = resource.openStream();
        try {
            final BufferedReader reader = new BufferedReader(new InputStreamReader(stream, "UTF-8"));
            for (String line = reader.readLine(); line != null; line = reader.readLine()) {
                line = ServiceLoader.COMMENT.matcher(line).replaceFirst("");
                line = ServiceLoader.WHITESPACE.matcher(line).replaceAll("");
                if (line.length() > 0) {
                    names.add(line);
                }
            }
        }
        finally {
            stream.close();
        }
    }
    
    static {
        ServiceLoader.contextClassLoader = null;
        services = new HashMap<Object, Object>();
        COMMENT = Pattern.compile("#.*");
        WHITESPACE = Pattern.compile("\\s+");
    }
}
