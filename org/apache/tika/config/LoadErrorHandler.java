// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.config;

import java.util.logging.Level;
import java.util.logging.Logger;

public interface LoadErrorHandler
{
    public static final LoadErrorHandler IGNORE = new LoadErrorHandler() {
        public void handleLoadError(final String classname, final Throwable throwable) {
        }
    };
    public static final LoadErrorHandler WARN = new LoadErrorHandler() {
        public void handleLoadError(final String classname, final Throwable throwable) {
            Logger.getLogger(classname).log(Level.WARNING, "Unable to load " + classname, throwable);
        }
    };
    public static final LoadErrorHandler THROW = new LoadErrorHandler() {
        public void handleLoadError(final String classname, final Throwable throwable) {
            throw new RuntimeException("Unable to load " + classname, throwable);
        }
    };
    
    void handleLoadError(final String p0, final Throwable p1);
}
