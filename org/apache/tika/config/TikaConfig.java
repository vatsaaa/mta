// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.config;

import org.apache.tika.detect.CompositeDetector;
import java.util.Set;
import java.util.List;
import org.apache.tika.parser.ParserDecorator;
import java.util.HashSet;
import org.apache.tika.parser.AutoDetectParser;
import java.util.ArrayList;
import org.apache.tika.mime.MimeTypesFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import org.apache.tika.mime.MediaTypeRegistry;
import org.apache.tika.parser.Parser;
import org.apache.tika.mime.MediaType;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import java.io.FileInputStream;
import org.apache.tika.mime.MimeTypeException;
import org.w3c.dom.Element;
import org.w3c.dom.Document;
import java.io.InputStream;
import java.net.URL;
import org.xml.sax.SAXException;
import java.io.IOException;
import org.apache.tika.exception.TikaException;
import java.io.File;
import org.apache.tika.parser.DefaultParser;
import org.apache.tika.detect.DefaultDetector;
import org.apache.tika.mime.MimeTypes;
import org.apache.tika.detect.Detector;
import org.apache.tika.parser.CompositeParser;

public class TikaConfig
{
    private final CompositeParser parser;
    private final Detector detector;
    private final MimeTypes mimeTypes;
    
    private static MimeTypes getDefaultMimeTypes() {
        return MimeTypes.getDefaultMimeTypes();
    }
    
    private static Detector getDefaultDetector(final MimeTypes types, final ServiceLoader loader) {
        return new DefaultDetector(types, loader);
    }
    
    private static CompositeParser getDefaultParser(final MimeTypes types, final ServiceLoader loader) {
        return new DefaultParser(types.getMediaTypeRegistry(), loader);
    }
    
    public TikaConfig(final String file) throws TikaException, IOException, SAXException {
        this(new File(file));
    }
    
    public TikaConfig(final File file) throws TikaException, IOException, SAXException {
        this(getBuilder().parse(file));
    }
    
    public TikaConfig(final URL url) throws TikaException, IOException, SAXException {
        this(url, ServiceLoader.getContextClassLoader());
    }
    
    public TikaConfig(final URL url, final ClassLoader loader) throws TikaException, IOException, SAXException {
        this(getBuilder().parse(url.toString()).getDocumentElement(), loader);
    }
    
    public TikaConfig(final InputStream stream) throws TikaException, IOException, SAXException {
        this(getBuilder().parse(stream));
    }
    
    public TikaConfig(final Document document) throws TikaException, IOException {
        this(document.getDocumentElement());
    }
    
    public TikaConfig(final Element element) throws TikaException, IOException {
        this(element, new ServiceLoader());
    }
    
    public TikaConfig(final Element element, final ClassLoader loader) throws TikaException, IOException {
        this(element, new ServiceLoader(loader));
    }
    
    private TikaConfig(final Element element, final ServiceLoader loader) throws TikaException, IOException {
        this.mimeTypes = typesFromDomElement(element);
        this.detector = detectorFromDomElement(element, this.mimeTypes, loader);
        this.parser = parserFromDomElement(element, this.mimeTypes, loader);
    }
    
    public TikaConfig(final ClassLoader loader) throws MimeTypeException, IOException {
        final ServiceLoader serviceLoader = new ServiceLoader(loader);
        this.mimeTypes = getDefaultMimeTypes();
        this.detector = getDefaultDetector(this.mimeTypes, serviceLoader);
        this.parser = getDefaultParser(this.mimeTypes, serviceLoader);
    }
    
    public TikaConfig() throws TikaException, IOException {
        final ServiceLoader loader = new ServiceLoader();
        String config = System.getProperty("tika.config");
        if (config == null) {
            config = System.getenv("TIKA_CONFIG");
        }
        if (config == null) {
            this.mimeTypes = getDefaultMimeTypes();
            this.parser = getDefaultParser(this.mimeTypes, loader);
            this.detector = getDefaultDetector(this.mimeTypes, loader);
        }
        else {
            InputStream stream = null;
            final File file = new File(config);
            if (file.isFile()) {
                stream = new FileInputStream(file);
            }
            if (stream == null) {
                try {
                    stream = new URL(config).openStream();
                }
                catch (IOException ex) {}
            }
            if (stream == null) {
                stream = loader.getResourceAsStream(config);
            }
            if (stream == null) {
                throw new TikaException("Specified Tika configuration not found: " + config);
            }
            try {
                final Element element = getBuilder().parse(stream).getDocumentElement();
                this.mimeTypes = typesFromDomElement(element);
                this.parser = parserFromDomElement(element, this.mimeTypes, loader);
                this.detector = detectorFromDomElement(element, this.mimeTypes, loader);
            }
            catch (SAXException e) {
                throw new TikaException("Specified Tika configuration has syntax errors: " + config, e);
            }
            finally {
                stream.close();
            }
        }
    }
    
    private static String getText(final Node node) {
        if (node.getNodeType() == 3) {
            return node.getNodeValue();
        }
        if (node.getNodeType() == 1) {
            final StringBuilder builder = new StringBuilder();
            final NodeList list = node.getChildNodes();
            for (int i = 0; i < list.getLength(); ++i) {
                builder.append(getText(list.item(i)));
            }
            return builder.toString();
        }
        return "";
    }
    
    @Deprecated
    public Parser getParser(final MediaType mimeType) {
        return this.parser.getParsers().get(mimeType);
    }
    
    public Parser getParser() {
        return this.parser;
    }
    
    public Detector getDetector() {
        return this.detector;
    }
    
    public MimeTypes getMimeRepository() {
        return this.mimeTypes;
    }
    
    public MediaTypeRegistry getMediaTypeRegistry() {
        return this.mimeTypes.getMediaTypeRegistry();
    }
    
    public static TikaConfig getDefaultConfig() {
        try {
            return new TikaConfig();
        }
        catch (IOException e) {
            throw new RuntimeException("Unable to read default configuration", e);
        }
        catch (TikaException e2) {
            throw new RuntimeException("Unable to access default configuration", e2);
        }
    }
    
    private static DocumentBuilder getBuilder() throws TikaException {
        try {
            return DocumentBuilderFactory.newInstance().newDocumentBuilder();
        }
        catch (ParserConfigurationException e) {
            throw new TikaException("XML parser not available", e);
        }
    }
    
    private static Element getChild(final Element element, final String name) {
        for (Node child = element.getFirstChild(); child != null; child = child.getNextSibling()) {
            if (child.getNodeType() == 1 && name.equals(child.getNodeName())) {
                return (Element)child;
            }
        }
        return null;
    }
    
    private static MimeTypes typesFromDomElement(final Element element) throws TikaException, IOException {
        final Element mtr = getChild(element, "mimeTypeRepository");
        if (mtr != null && mtr.hasAttribute("resource")) {
            return MimeTypesFactory.create(mtr.getAttribute("resource"));
        }
        return getDefaultMimeTypes();
    }
    
    private static CompositeParser parserFromDomElement(final Element element, final MimeTypes mimeTypes, final ServiceLoader loader) throws TikaException, IOException {
        final List<Parser> parsers = new ArrayList<Parser>();
        final NodeList nodes = element.getElementsByTagName("parser");
        for (int i = 0; i < nodes.getLength(); ++i) {
            final Element node = (Element)nodes.item(i);
            final String name = node.getAttribute("class");
            try {
                final Class<? extends Parser> parserClass = loader.getServiceClass(Parser.class, name);
                if (AutoDetectParser.class.isAssignableFrom(parserClass)) {
                    throw new TikaException("AutoDetectParser not supported in a <parser> configuration element: " + name);
                }
                Parser parser = (Parser)parserClass.newInstance();
                final NodeList mimes = node.getElementsByTagName("mime");
                if (mimes.getLength() > 0) {
                    final Set<MediaType> types = new HashSet<MediaType>();
                    for (int j = 0; j < mimes.getLength(); ++j) {
                        final String mime = getText(mimes.item(j));
                        final MediaType type = MediaType.parse(mime);
                        if (type == null) {
                            throw new TikaException("Invalid media type name: " + mime);
                        }
                        types.add(type);
                    }
                    parser = ParserDecorator.withTypes(parser, types);
                }
                parsers.add(parser);
            }
            catch (ClassNotFoundException e) {
                throw new TikaException("Unable to find a parser class: " + name, e);
            }
            catch (IllegalAccessException e2) {
                throw new TikaException("Unable to access a parser class: " + name, e2);
            }
            catch (InstantiationException e3) {
                throw new TikaException("Unable to instantiate a parser class: " + name, e3);
            }
        }
        if (parsers.isEmpty()) {
            return getDefaultParser(mimeTypes, loader);
        }
        final MediaTypeRegistry registry = mimeTypes.getMediaTypeRegistry();
        return new CompositeParser(registry, parsers);
    }
    
    private static Detector detectorFromDomElement(final Element element, final MimeTypes mimeTypes, final ServiceLoader loader) throws TikaException, IOException {
        final List<Detector> detectors = new ArrayList<Detector>();
        final NodeList nodes = element.getElementsByTagName("detector");
        for (int i = 0; i < nodes.getLength(); ++i) {
            final Element node = (Element)nodes.item(i);
            final String name = node.getAttribute("class");
            try {
                final Class<? extends Detector> detectorClass = loader.getServiceClass(Detector.class, name);
                detectors.add((Detector)detectorClass.newInstance());
            }
            catch (ClassNotFoundException e) {
                throw new TikaException("Unable to find a detector class: " + name, e);
            }
            catch (IllegalAccessException e2) {
                throw new TikaException("Unable to access a detector class: " + name, e2);
            }
            catch (InstantiationException e3) {
                throw new TikaException("Unable to instantiate a detector class: " + name, e3);
            }
        }
        if (detectors.isEmpty()) {
            return getDefaultDetector(mimeTypes, loader);
        }
        final MediaTypeRegistry registry = mimeTypes.getMediaTypeRegistry();
        return new CompositeDetector(registry, detectors);
    }
}
