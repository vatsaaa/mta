// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.language;

import java.io.Writer;
import org.apache.tika.sax.WriteOutContentHandler;

public class ProfilingHandler extends WriteOutContentHandler
{
    private final ProfilingWriter writer;
    
    public ProfilingHandler(final ProfilingWriter writer) {
        super(writer);
        this.writer = writer;
    }
    
    public ProfilingHandler(final LanguageProfile profile) {
        this(new ProfilingWriter(profile));
    }
    
    public ProfilingHandler() {
        this(new ProfilingWriter());
    }
    
    public LanguageProfile getProfile() {
        return this.writer.getProfile();
    }
    
    public LanguageIdentifier getLanguage() {
        return this.writer.getLanguage();
    }
}
