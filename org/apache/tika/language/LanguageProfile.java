// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.language;

import java.util.Iterator;
import java.util.Set;
import java.util.Collection;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Map;

public class LanguageProfile
{
    public static final int DEFAULT_NGRAM_LENGTH = 3;
    private final int length;
    private final Map<String, Counter> ngrams;
    private long count;
    
    public LanguageProfile(final int length) {
        this.ngrams = new HashMap<String, Counter>();
        this.count = 0L;
        this.length = length;
    }
    
    public LanguageProfile() {
        this(3);
    }
    
    public LanguageProfile(final String content, final int length) {
        this(length);
        final ProfilingWriter writer = new ProfilingWriter(this);
        final char[] ch = content.toCharArray();
        writer.write(ch, 0, ch.length);
    }
    
    public LanguageProfile(final String content) {
        this(content, 3);
    }
    
    public long getCount() {
        return this.count;
    }
    
    public long getCount(final String ngram) {
        final Counter counter = this.ngrams.get(ngram);
        if (counter != null) {
            return counter.count;
        }
        return 0L;
    }
    
    public void add(final String ngram) {
        this.add(ngram, 1L);
    }
    
    public void add(final String ngram, final long count) {
        if (this.length != ngram.length()) {
            throw new IllegalArgumentException("Unable to add an ngram of incorrect length: " + ngram.length() + " != " + this.length);
        }
        Counter counter = this.ngrams.get(ngram);
        if (counter == null) {
            counter = new Counter();
            this.ngrams.put(ngram, counter);
        }
        counter.count += count;
        this.count += count;
    }
    
    public double distance(final LanguageProfile that) {
        if (this.length != that.length) {
            throw new IllegalArgumentException("Unable to calculage distance of language profiles with different ngram lengths: " + that.length + " != " + this.length);
        }
        double sumOfSquares = 0.0;
        final double thisCount = Math.max((double)this.count, 1.0);
        final double thatCount = Math.max((double)that.count, 1.0);
        final Set<String> ngrams = new HashSet<String>();
        ngrams.addAll(this.ngrams.keySet());
        ngrams.addAll(that.ngrams.keySet());
        for (final String ngram : ngrams) {
            final double thisFrequency = this.getCount(ngram) / thisCount;
            final double thatFrequency = that.getCount(ngram) / thatCount;
            final double difference = thisFrequency - thatFrequency;
            sumOfSquares += difference * difference;
        }
        return Math.sqrt(sumOfSquares);
    }
    
    @Override
    public String toString() {
        return this.ngrams.toString();
    }
    
    private static class Counter
    {
        private long count;
        
        private Counter() {
            this.count = 0L;
        }
        
        @Override
        public String toString() {
            return Long.toString(this.count);
        }
    }
}
