// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.language;

import java.util.HashMap;
import java.util.Set;
import java.io.IOException;
import java.util.Iterator;
import java.io.InputStream;
import java.io.Reader;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Properties;
import java.util.Map;

public class LanguageIdentifier
{
    private static final Map<String, LanguageProfile> PROFILES;
    private static final String PROFILE_SUFFIX = ".ngp";
    private static final String PROFILE_ENCODING = "UTF-8";
    private static Properties props;
    private static String errors;
    private static final String PROPERTIES_OVERRIDE_FILE = "tika.language.override.properties";
    private static final String PROPERTIES_FILE = "tika.language.properties";
    private static final String LANGUAGES_KEY = "languages";
    private static final double CERTAINTY_LIMIT = 0.022;
    private final String language;
    private final double distance;
    
    private static void addProfile(final String language) throws Exception {
        try {
            final LanguageProfile profile = new LanguageProfile();
            final InputStream stream = LanguageIdentifier.class.getResourceAsStream(language + ".ngp");
            try {
                final BufferedReader reader = new BufferedReader(new InputStreamReader(stream, "UTF-8"));
                for (String line = reader.readLine(); line != null; line = reader.readLine()) {
                    if (line.length() > 0 && !line.startsWith("#")) {
                        final int space = line.indexOf(32);
                        profile.add(line.substring(0, space), Long.parseLong(line.substring(space + 1)));
                    }
                }
            }
            finally {
                stream.close();
            }
            addProfile(language, profile);
        }
        catch (Throwable t) {
            throw new Exception("Failed trying to load language profile for language \"" + language + "\". Error: " + t.getMessage());
        }
    }
    
    public static void addProfile(final String language, final LanguageProfile profile) {
        LanguageIdentifier.PROFILES.put(language, profile);
    }
    
    public LanguageIdentifier(final LanguageProfile profile) {
        String minLanguage = "unknown";
        double minDistance = 1.0;
        for (final Map.Entry<String, LanguageProfile> entry : LanguageIdentifier.PROFILES.entrySet()) {
            final double distance = profile.distance(entry.getValue());
            if (distance < minDistance) {
                minDistance = distance;
                minLanguage = entry.getKey();
            }
        }
        this.language = minLanguage;
        this.distance = minDistance;
    }
    
    public LanguageIdentifier(final String content) {
        this(new LanguageProfile(content));
    }
    
    public String getLanguage() {
        return this.language;
    }
    
    public boolean isReasonablyCertain() {
        return this.distance < 0.022;
    }
    
    public static void initProfiles() {
        clearProfiles();
        LanguageIdentifier.errors = "";
        InputStream stream = LanguageIdentifier.class.getResourceAsStream("tika.language.override.properties");
        if (stream == null) {
            stream = LanguageIdentifier.class.getResourceAsStream("tika.language.properties");
        }
        if (stream != null) {
            try {
                (LanguageIdentifier.props = new Properties()).load(stream);
            }
            catch (IOException e) {
                LanguageIdentifier.errors = LanguageIdentifier.errors + "IOException while trying to load property file. Message: " + e.getMessage() + "\n";
            }
        }
        final String[] arr$;
        final String[] languages = arr$ = LanguageIdentifier.props.getProperty("languages").split(",");
        for (String language : arr$) {
            language = language.trim();
            final String name = LanguageIdentifier.props.getProperty("name." + language, "Unknown");
            try {
                addProfile(language);
            }
            catch (Exception e2) {
                LanguageIdentifier.errors = LanguageIdentifier.errors + "Language " + language + " (" + name + ") not initialized. Message: " + e2.getMessage() + "\n";
            }
        }
    }
    
    public static void initProfiles(final Map<String, LanguageProfile> profilesMap) {
        clearProfiles();
        for (final Map.Entry<String, LanguageProfile> entry : profilesMap.entrySet()) {
            addProfile(entry.getKey(), entry.getValue());
        }
    }
    
    public static void clearProfiles() {
        LanguageIdentifier.PROFILES.clear();
    }
    
    public static boolean hasErrors() {
        return LanguageIdentifier.errors != "";
    }
    
    public static String getErrors() {
        return LanguageIdentifier.errors;
    }
    
    public static Set<String> getSupportedLanguages() {
        return LanguageIdentifier.PROFILES.keySet();
    }
    
    @Override
    public String toString() {
        return this.language + " (" + this.distance + ")";
    }
    
    static {
        PROFILES = new HashMap<String, LanguageProfile>();
        LanguageIdentifier.props = new Properties();
        LanguageIdentifier.errors = "";
        initProfiles();
    }
}
