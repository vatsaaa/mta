// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.language;

import java.io.FileOutputStream;
import java.io.FileInputStream;
import java.io.File;
import java.util.Date;
import java.io.OutputStream;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.Reader;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.InputStream;
import org.apache.tika.exception.TikaException;
import java.util.Collections;
import java.util.Collection;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.HashMap;
import java.util.Map;
import java.util.List;

public class LanguageProfilerBuilder
{
    static final int ABSOLUTE_MIN_NGRAM_LENGTH = 3;
    static final int ABSOLUTE_MAX_NGRAM_LENGTH = 3;
    static final int DEFAULT_MIN_NGRAM_LENGTH = 3;
    static final int DEFAULT_MAX_NGRAM_LENGTH = 3;
    static final String FILE_EXTENSION = "ngp";
    static final int MAX_SIZE = 1000;
    static final char SEPARATOR = '_';
    private static final String SEP_CHARSEQ;
    private String name;
    private List<NGramEntry> sorted;
    private int minLength;
    private int maxLength;
    private int[] ngramcounts;
    private Map<CharSequence, NGramEntry> ngrams;
    private QuickStringBuffer word;
    
    public LanguageProfilerBuilder(final String name, final int minlen, final int maxlen) {
        this.name = null;
        this.sorted = null;
        this.minLength = 3;
        this.maxLength = 3;
        this.ngramcounts = null;
        this.ngrams = null;
        this.word = new QuickStringBuffer();
        this.ngrams = new HashMap<CharSequence, NGramEntry>(4000);
        this.minLength = minlen;
        this.maxLength = maxlen;
        this.name = name;
    }
    
    public LanguageProfilerBuilder(final String name) {
        this.name = null;
        this.sorted = null;
        this.minLength = 3;
        this.maxLength = 3;
        this.ngramcounts = null;
        this.ngrams = null;
        this.word = new QuickStringBuffer();
        this.ngrams = new HashMap<CharSequence, NGramEntry>(4000);
        this.minLength = 3;
        this.maxLength = 3;
        this.name = name;
    }
    
    public String getName() {
        return this.name;
    }
    
    public void add(final StringBuffer word) {
        for (int i = this.minLength; i <= this.maxLength && i < word.length(); ++i) {
            this.add(word, i);
        }
    }
    
    private void add(final QuickStringBuffer word) {
        final int wlen = word.length();
        if (wlen >= this.minLength) {
            for (int max = Math.min(this.maxLength, wlen), i = this.minLength; i <= max; ++i) {
                this.add(word.subSequence(wlen - i, wlen));
            }
        }
    }
    
    private void add(final CharSequence cs) {
        if (cs.equals(LanguageProfilerBuilder.SEP_CHARSEQ)) {
            return;
        }
        NGramEntry nge = this.ngrams.get(cs);
        if (nge == null) {
            nge = new NGramEntry(cs);
            this.ngrams.put(cs, nge);
        }
        nge.inc();
    }
    
    public void analyze(final StringBuilder text) {
        if (this.ngrams != null) {
            this.ngrams.clear();
            this.sorted = null;
            this.ngramcounts = null;
        }
        this.word.clear().append('_');
        for (int i = 0; i < text.length(); ++i) {
            final char c = Character.toLowerCase(text.charAt(i));
            if (Character.isLetter(c)) {
                this.add(this.word.append(c));
            }
            else if (this.word.length() > 1) {
                this.add(this.word.append('_'));
                this.word.clear().append('_');
            }
        }
        if (this.word.length() > 1) {
            this.add(this.word.append('_'));
        }
        this.normalize();
    }
    
    private void add(final StringBuffer word, final int n) {
        for (int i = 0; i <= word.length() - n; ++i) {
            this.add(word.subSequence(i, i + n));
        }
    }
    
    protected void normalize() {
        NGramEntry e = null;
        Iterator<NGramEntry> i = this.ngrams.values().iterator();
        if (this.ngramcounts == null) {
            this.ngramcounts = new int[this.maxLength + 1];
            while (i.hasNext()) {
                e = i.next();
                final int[] ngramcounts = this.ngramcounts;
                final int size = e.size();
                ngramcounts[size] += e.count;
            }
        }
        i = this.ngrams.values().iterator();
        while (i.hasNext()) {
            e = i.next();
            e.frequency = e.count / (float)this.ngramcounts[e.size()];
        }
    }
    
    public List<NGramEntry> getSorted() {
        if (this.sorted == null) {
            Collections.sort(this.sorted = new ArrayList<NGramEntry>(this.ngrams.values()));
            if (this.sorted.size() > 1000) {
                this.sorted = this.sorted.subList(0, 1000);
            }
        }
        return this.sorted;
    }
    
    @Override
    public String toString() {
        final StringBuffer s = new StringBuffer().append("NGramProfile: ").append(this.name).append("\n");
        for (final NGramEntry entry : this.getSorted()) {
            s.append("[").append(entry.seq).append("/").append(entry.count).append("/").append(entry.frequency).append("]\n");
        }
        return s.toString();
    }
    
    public float getSimilarity(final LanguageProfilerBuilder another) throws TikaException {
        float sum = 0.0f;
        try {
            for (final NGramEntry other : another.getSorted()) {
                if (this.ngrams.containsKey(other.seq)) {
                    sum += Math.abs(other.frequency - this.ngrams.get(other.seq).frequency) / 2.0f;
                }
                else {
                    sum += other.frequency;
                }
            }
            for (final NGramEntry other : this.getSorted()) {
                if (another.ngrams.containsKey(other.seq)) {
                    sum += Math.abs(other.frequency - another.ngrams.get(other.seq).frequency) / 2.0f;
                }
                else {
                    sum += other.frequency;
                }
            }
        }
        catch (Exception e) {
            throw new TikaException("Could not calculate a score how well NGramProfiles match each other");
        }
        return sum;
    }
    
    public void load(final InputStream is) throws IOException {
        this.ngrams.clear();
        this.ngramcounts = new int[this.maxLength + 1];
        final BufferedReader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
        String line = null;
        while ((line = reader.readLine()) != null) {
            if (line.charAt(0) != '#') {
                final int spacepos = line.indexOf(32);
                final String ngramsequence = line.substring(0, spacepos).trim();
                final int len = ngramsequence.length();
                if (len < this.minLength || len > this.maxLength) {
                    continue;
                }
                final int ngramcount = Integer.parseInt(line.substring(spacepos + 1));
                final NGramEntry en = new NGramEntry(ngramsequence, ngramcount);
                this.ngrams.put(en.getSeq(), en);
                final int[] ngramcounts = this.ngramcounts;
                final int n = len;
                ngramcounts[n] += ngramcount;
            }
        }
        this.normalize();
    }
    
    public static LanguageProfilerBuilder create(final String name, final InputStream is, final String encoding) throws TikaException {
        final LanguageProfilerBuilder newProfile = new LanguageProfilerBuilder(name, 3, 3);
        final BufferedInputStream bis = new BufferedInputStream(is);
        final byte[] buffer = new byte[4096];
        final StringBuilder text = new StringBuilder();
        try {
            int len;
            while ((len = bis.read(buffer)) != -1) {
                text.append(new String(buffer, 0, len, encoding));
            }
        }
        catch (IOException e) {
            throw new TikaException("Could not create profile, " + e.getMessage());
        }
        newProfile.analyze(text);
        return newProfile;
    }
    
    public void save(final OutputStream os) throws IOException {
        os.write(("# NgramProfile generated at " + new Date() + " for Apache Tika Language Identification\n").getBytes());
        final List<NGramEntry> list = new ArrayList<NGramEntry>();
        List<NGramEntry> sublist = new ArrayList<NGramEntry>();
        final NGramEntry[] entries = this.ngrams.values().toArray(new NGramEntry[this.ngrams.size()]);
        for (int i = this.minLength; i <= this.maxLength; ++i) {
            for (int j = 0; j < entries.length; ++j) {
                if (entries[j].getSeq().length() == i) {
                    sublist.add(entries[j]);
                }
            }
            Collections.sort(sublist);
            if (sublist.size() > 1000) {
                sublist = sublist.subList(0, 1000);
            }
            list.addAll(sublist);
            sublist.clear();
        }
        for (int i = 0; i < list.size(); ++i) {
            final NGramEntry e = list.get(i);
            final String line = e.toString() + " " + e.getCount() + "\n";
            os.write(line.getBytes("UTF-8"));
        }
        os.flush();
    }
    
    public static void main(final String[] args) {
        final String usage = "Usage: NGramProfile [-create profilename filename encoding] [-similarity file1 file2] [-score profile-name filename encoding]";
        int command = 0;
        final int CREATE = 1;
        final int SIMILARITY = 2;
        final int SCORE = 3;
        String profilename = "";
        String filename = "";
        String filename2 = "";
        String encoding = "";
        if (args.length == 0) {
            System.err.println(usage);
            System.exit(-1);
        }
        for (int i = 0; i < args.length; ++i) {
            if (args[i].equals("-create")) {
                command = 1;
                profilename = args[++i];
                filename = args[++i];
                encoding = args[++i];
            }
            if (args[i].equals("-similarity")) {
                command = 2;
                filename = args[++i];
                filename2 = args[++i];
                encoding = args[++i];
            }
            if (args[i].equals("-score")) {
                command = 3;
                profilename = args[++i];
                filename = args[++i];
                encoding = args[++i];
            }
        }
        try {
            switch (command) {
                case 1: {
                    File f = new File(filename);
                    final FileInputStream fis = new FileInputStream(f);
                    final LanguageProfilerBuilder newProfile = create(profilename, fis, encoding);
                    fis.close();
                    f = new File(profilename + "." + "ngp");
                    final FileOutputStream fos = new FileOutputStream(f);
                    newProfile.save(fos);
                    System.out.println("new profile " + profilename + "." + "ngp" + " was created.");
                    break;
                }
                case 2: {
                    File f = new File(filename);
                    FileInputStream fis = new FileInputStream(f);
                    final LanguageProfilerBuilder newProfile = create(filename, fis, encoding);
                    newProfile.normalize();
                    f = new File(filename2);
                    fis = new FileInputStream(f);
                    final LanguageProfilerBuilder newProfile2 = create(filename2, fis, encoding);
                    newProfile2.normalize();
                    System.out.println("Similarity is " + newProfile.getSimilarity(newProfile2));
                    break;
                }
                case 3: {
                    File f = new File(filename);
                    FileInputStream fis = new FileInputStream(f);
                    final LanguageProfilerBuilder newProfile = create(filename, fis, encoding);
                    f = new File(profilename + "." + "ngp");
                    fis = new FileInputStream(f);
                    final LanguageProfilerBuilder compare = new LanguageProfilerBuilder(profilename, 3, 3);
                    compare.load(fis);
                    System.out.println("Score is " + compare.getSimilarity(newProfile));
                    break;
                }
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    static {
        SEP_CHARSEQ = new String(new char[] { '_' });
    }
    
    static class NGramEntry implements Comparable<NGramEntry>
    {
        private LanguageProfilerBuilder profile;
        CharSequence seq;
        private int count;
        private float frequency;
        
        public NGramEntry(final CharSequence seq) {
            this.profile = null;
            this.seq = null;
            this.count = 0;
            this.frequency = 0.0f;
            this.seq = seq;
        }
        
        public NGramEntry(final String seq, final int count) {
            this.profile = null;
            this.seq = null;
            this.count = 0;
            this.frequency = 0.0f;
            this.seq = new StringBuffer(seq).subSequence(0, seq.length());
            this.count = count;
        }
        
        public int getCount() {
            return this.count;
        }
        
        public float getFrequency() {
            return this.frequency;
        }
        
        public CharSequence getSeq() {
            return this.seq;
        }
        
        public int size() {
            return this.seq.length();
        }
        
        public int compareTo(final NGramEntry ngram) {
            final int diff = Float.compare(ngram.getFrequency(), this.frequency);
            if (diff != 0) {
                return diff;
            }
            return this.toString().compareTo(ngram.toString());
        }
        
        public void inc() {
            ++this.count;
        }
        
        public void setProfile(final LanguageProfilerBuilder profile) {
            this.profile = profile;
        }
        
        public LanguageProfilerBuilder getProfile() {
            return this.profile;
        }
        
        @Override
        public String toString() {
            return this.seq.toString();
        }
        
        @Override
        public int hashCode() {
            return this.seq.hashCode();
        }
        
        @Override
        public boolean equals(final Object obj) {
            NGramEntry ngram = null;
            try {
                ngram = (NGramEntry)obj;
                return ngram.seq.equals(this.seq);
            }
            catch (Exception e) {
                return false;
            }
        }
    }
    
    private static class QuickStringBuffer implements CharSequence
    {
        private char[] value;
        private int count;
        
        QuickStringBuffer() {
            this(16);
        }
        
        QuickStringBuffer(final char[] value) {
            this.value = value;
            this.count = value.length;
        }
        
        QuickStringBuffer(final int length) {
            this.value = new char[length];
        }
        
        QuickStringBuffer(final String str) {
            this(str.length() + 16);
            this.append(str);
        }
        
        public int length() {
            return this.count;
        }
        
        private void expandCapacity(final int minimumCapacity) {
            int newCapacity = (this.value.length + 1) * 2;
            if (newCapacity < 0) {
                newCapacity = Integer.MAX_VALUE;
            }
            else if (minimumCapacity > newCapacity) {
                newCapacity = minimumCapacity;
            }
            final char[] newValue = new char[newCapacity];
            System.arraycopy(this.value, 0, newValue, 0, this.count);
            this.value = newValue;
        }
        
        QuickStringBuffer clear() {
            this.count = 0;
            return this;
        }
        
        public char charAt(final int index) {
            return this.value[index];
        }
        
        QuickStringBuffer append(String str) {
            if (str == null) {
                str = String.valueOf(str);
            }
            final int len = str.length();
            final int newcount = this.count + len;
            if (newcount > this.value.length) {
                this.expandCapacity(newcount);
            }
            str.getChars(0, len, this.value, this.count);
            this.count = newcount;
            return this;
        }
        
        QuickStringBuffer append(final char c) {
            final int newcount = this.count + 1;
            if (newcount > this.value.length) {
                this.expandCapacity(newcount);
            }
            this.value[this.count++] = c;
            return this;
        }
        
        public CharSequence subSequence(final int start, final int end) {
            return new String(this.value, start, end - start);
        }
        
        @Override
        public String toString() {
            return new String(this.value);
        }
    }
}
