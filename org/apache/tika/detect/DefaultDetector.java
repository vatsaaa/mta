// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.detect;

import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import org.apache.tika.mime.MimeTypes;
import org.apache.tika.config.ServiceLoader;

public class DefaultDetector extends CompositeDetector
{
    private static final long serialVersionUID = -8170114575326908027L;
    private final transient ServiceLoader loader;
    
    private static List<Detector> getDefaultDetectors(final MimeTypes types, final ServiceLoader loader) {
        final List<Detector> detectors = loader.loadStaticServiceProviders(Detector.class);
        Collections.sort(detectors, new Comparator<Detector>() {
            public int compare(final Detector d1, final Detector d2) {
                final String n1 = d1.getClass().getName();
                final String n2 = d2.getClass().getName();
                final boolean t1 = n1.startsWith("org.apache.tika.");
                final boolean t2 = n2.startsWith("org.apache.tika.");
                if (t1 == t2) {
                    return n1.compareTo(n2);
                }
                if (t1) {
                    return 1;
                }
                return -1;
            }
        });
        detectors.add(types);
        return detectors;
    }
    
    public DefaultDetector(final MimeTypes types, final ServiceLoader loader) {
        super(types.getMediaTypeRegistry(), getDefaultDetectors(types, loader));
        this.loader = loader;
    }
    
    public DefaultDetector(final MimeTypes types, final ClassLoader loader) {
        this(types, new ServiceLoader(loader));
    }
    
    public DefaultDetector(final ClassLoader loader) {
        this(MimeTypes.getDefaultMimeTypes(), loader);
    }
    
    public DefaultDetector(final MimeTypes types) {
        this(types, new ServiceLoader());
    }
    
    public DefaultDetector() {
        this(MimeTypes.getDefaultMimeTypes());
    }
    
    @Override
    public List<Detector> getDetectors() {
        if (this.loader != null) {
            final List<Detector> detectors = this.loader.loadDynamicServiceProviders(Detector.class);
            detectors.addAll(super.getDetectors());
            return detectors;
        }
        return super.getDetectors();
    }
}
