// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.detect;

import java.util.Collections;
import java.io.IOException;
import java.util.Iterator;
import org.apache.tika.mime.MediaType;
import org.apache.tika.metadata.Metadata;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;
import org.apache.tika.mime.MediaTypeRegistry;

public class CompositeDetector implements Detector
{
    private static final long serialVersionUID = 5980683158436430252L;
    private final MediaTypeRegistry registry;
    private final List<Detector> detectors;
    
    public CompositeDetector(final MediaTypeRegistry registry, final List<Detector> detectors) {
        this.registry = registry;
        this.detectors = detectors;
    }
    
    public CompositeDetector(final List<Detector> detectors) {
        this(new MediaTypeRegistry(), detectors);
    }
    
    public CompositeDetector(final Detector... detectors) {
        this(Arrays.asList(detectors));
    }
    
    public MediaType detect(final InputStream input, final Metadata metadata) throws IOException {
        MediaType type = MediaType.OCTET_STREAM;
        for (final Detector detector : this.getDetectors()) {
            final MediaType detected = detector.detect(input, metadata);
            if (this.registry.isSpecializationOf(detected, type)) {
                type = detected;
            }
        }
        return type;
    }
    
    public List<Detector> getDetectors() {
        return Collections.unmodifiableList((List<? extends Detector>)this.detectors);
    }
}
