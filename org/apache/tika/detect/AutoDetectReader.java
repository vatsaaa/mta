// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.detect;

import org.xml.sax.InputSource;
import java.io.BufferedInputStream;
import java.io.Reader;
import java.io.InputStreamReader;
import java.io.IOException;
import java.util.Iterator;
import org.apache.tika.exception.TikaException;
import org.apache.tika.utils.CharsetUtils;
import org.apache.tika.mime.MediaType;
import java.util.List;
import org.apache.tika.metadata.Metadata;
import java.io.InputStream;
import java.nio.charset.Charset;
import org.apache.tika.config.ServiceLoader;
import java.io.BufferedReader;

public class AutoDetectReader extends BufferedReader
{
    private static final ServiceLoader DEFAULT_LOADER;
    private final Charset charset;
    
    private static Charset detect(final InputStream input, final Metadata metadata, final List<EncodingDetector> detectors) throws IOException, TikaException {
        for (final EncodingDetector detector : detectors) {
            final Charset charset = detector.detect(input, metadata);
            if (charset != null) {
                return charset;
            }
        }
        final MediaType type = MediaType.parse(metadata.get("Content-Type"));
        if (type != null) {
            final String charset2 = type.getParameters().get("charset");
            if (charset2 != null) {
                try {
                    return CharsetUtils.forName(charset2);
                }
                catch (Exception ex) {}
            }
        }
        throw new TikaException("Failed to detect the character encoding of a document");
    }
    
    private AutoDetectReader(final InputStream stream, final Charset charset) throws IOException {
        super(new InputStreamReader(stream, charset));
        this.charset = charset;
        this.mark(1);
        if (this.read() != 65279) {
            this.reset();
        }
    }
    
    private AutoDetectReader(final BufferedInputStream stream, final Metadata metadata, final List<EncodingDetector> detectors) throws IOException, TikaException {
        this(stream, detect(stream, metadata, detectors));
    }
    
    public AutoDetectReader(final InputStream stream, final Metadata metadata, final ServiceLoader loader) throws IOException, TikaException {
        this(new BufferedInputStream(stream), metadata, loader.loadServiceProviders(EncodingDetector.class));
    }
    
    public AutoDetectReader(final InputStream stream, final Metadata metadata) throws IOException, TikaException {
        this(new BufferedInputStream(stream), metadata, AutoDetectReader.DEFAULT_LOADER);
    }
    
    public AutoDetectReader(final InputStream stream) throws IOException, TikaException {
        this(stream, new Metadata());
    }
    
    public Charset getCharset() {
        return this.charset;
    }
    
    public InputSource asInputSource() {
        final InputSource source = new InputSource(this);
        source.setEncoding(this.charset.name());
        return source;
    }
    
    static {
        DEFAULT_LOADER = new ServiceLoader(AutoDetectReader.class.getClassLoader());
    }
}
