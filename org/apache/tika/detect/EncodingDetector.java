// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.detect;

import java.io.IOException;
import java.nio.charset.Charset;
import org.apache.tika.metadata.Metadata;
import java.io.InputStream;

public interface EncodingDetector
{
    Charset detect(final InputStream p0, final Metadata p1) throws IOException;
}
