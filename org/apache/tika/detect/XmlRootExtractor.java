// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.detect;

import org.xml.sax.SAXException;
import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;
import org.xml.sax.ContentHandler;
import org.apache.tika.sax.OfflineContentHandler;
import org.apache.tika.io.CloseShieldInputStream;
import javax.xml.parsers.SAXParserFactory;
import java.io.InputStream;
import java.io.ByteArrayInputStream;
import javax.xml.namespace.QName;

public class XmlRootExtractor
{
    public QName extractRootElement(final byte[] data) {
        return this.extractRootElement(new ByteArrayInputStream(data));
    }
    
    public QName extractRootElement(final InputStream stream) {
        final ExtractorHandler handler = new ExtractorHandler();
        try {
            final SAXParserFactory factory = SAXParserFactory.newInstance();
            factory.setNamespaceAware(true);
            factory.setValidating(false);
            factory.setFeature("http://javax.xml.XMLConstants/feature/secure-processing", true);
            factory.newSAXParser().parse(new CloseShieldInputStream(stream), new OfflineContentHandler(handler));
        }
        catch (Exception ex) {}
        return handler.rootElement;
    }
    
    private static class ExtractorHandler extends DefaultHandler
    {
        private QName rootElement;
        
        private ExtractorHandler() {
            this.rootElement = null;
        }
        
        @Override
        public void startElement(final String uri, final String local, final String name, final Attributes attributes) throws SAXException {
            this.rootElement = new QName(uri, local);
            throw new SAXException("Aborting: root element received");
        }
    }
}
