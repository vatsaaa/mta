// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.fork;

import java.lang.reflect.Method;
import java.lang.reflect.InvocationTargetException;
import java.io.NotSerializableException;
import org.apache.tika.exception.TikaException;
import java.io.IOException;
import java.util.zip.CheckedOutputStream;
import java.util.zip.CheckedInputStream;
import java.io.InputStream;
import java.io.ByteArrayInputStream;
import java.io.OutputStream;
import java.net.URLStreamHandlerFactory;
import java.net.URL;
import java.io.DataOutputStream;
import java.io.DataInputStream;
import java.util.zip.Checksum;

class ForkServer implements Runnable, Checksum
{
    public static final byte ERROR = -1;
    public static final byte DONE = 0;
    public static final byte CALL = 1;
    public static final byte PING = 2;
    public static final byte RESOURCE = 3;
    public static final byte READY = 4;
    private final DataInputStream input;
    private final DataOutputStream output;
    private volatile boolean active;
    
    public static void main(final String[] args) throws Exception {
        URL.setURLStreamHandlerFactory(new MemoryURLStreamHandlerFactory());
        final ForkServer server = new ForkServer(System.in, System.out);
        System.setIn(new ByteArrayInputStream(new byte[0]));
        System.setOut(System.err);
        final Thread watchdog = new Thread(server, "Tika Watchdog");
        watchdog.setDaemon(true);
        watchdog.start();
        server.processRequests();
    }
    
    public ForkServer(final InputStream input, final OutputStream output) throws IOException {
        this.active = true;
        this.input = new DataInputStream(new CheckedInputStream(input, this));
        this.output = new DataOutputStream(new CheckedOutputStream(output, this));
    }
    
    public void run() {
        try {
            while (this.active) {
                this.active = false;
                Thread.sleep(5000L);
            }
            System.exit(0);
        }
        catch (InterruptedException ex) {}
    }
    
    public void processRequests() {
        try {
            this.output.writeByte(4);
            this.output.flush();
            final ClassLoader loader = (ClassLoader)this.readObject(ForkServer.class.getClassLoader());
            Thread.currentThread().setContextClassLoader(loader);
            final Object object = this.readObject(loader);
            while (true) {
                final int request = this.input.read();
                if (request == -1) {
                    break;
                }
                if (request == 2) {
                    this.output.writeByte(2);
                }
                else {
                    if (request != 1) {
                        throw new IllegalStateException("Unexpected request");
                    }
                    this.call(loader, object);
                }
                this.output.flush();
            }
        }
        catch (Throwable t) {
            t.printStackTrace();
        }
        System.err.flush();
    }
    
    private void call(final ClassLoader loader, final Object object) throws Exception {
        final Method method = this.getMethod(object, this.input.readUTF());
        final Object[] args = new Object[method.getParameterTypes().length];
        for (int i = 0; i < args.length; ++i) {
            args[i] = this.readObject(loader);
        }
        try {
            method.invoke(object, args);
            this.output.write(0);
        }
        catch (InvocationTargetException e) {
            this.output.write(-1);
            final Throwable toSend = e.getCause();
            try {
                ForkObjectInputStream.sendObject(toSend, this.output);
            }
            catch (NotSerializableException nse) {
                final TikaException te = new TikaException(toSend.getMessage());
                te.setStackTrace(toSend.getStackTrace());
                ForkObjectInputStream.sendObject(te, this.output);
            }
        }
    }
    
    private Method getMethod(final Object object, final String name) {
        for (Class<?> klass = object.getClass(); klass != null; klass = klass.getSuperclass()) {
            for (final Class<?> iface : klass.getInterfaces()) {
                for (final Method method : iface.getMethods()) {
                    if (name.equals(method.getName())) {
                        return method;
                    }
                }
            }
        }
        return null;
    }
    
    private Object readObject(final ClassLoader loader) throws IOException, ClassNotFoundException {
        final Object object = ForkObjectInputStream.readObject(this.input, loader);
        if (object instanceof ForkProxy) {
            ((ForkProxy)object).init(this.input, this.output);
        }
        this.output.writeByte(0);
        this.output.flush();
        return object;
    }
    
    public void update(final int b) {
        this.active = true;
    }
    
    public void update(final byte[] b, final int off, final int len) {
        this.active = true;
    }
    
    public long getValue() {
        return 0L;
    }
    
    public void reset() {
    }
}
