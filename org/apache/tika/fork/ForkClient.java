// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.fork;

import org.apache.tika.io.IOUtils;
import java.util.jar.JarEntry;
import java.util.zip.ZipEntry;
import java.io.OutputStream;
import java.util.jar.JarOutputStream;
import java.io.FileOutputStream;
import org.apache.tika.io.IOExceptionWithCause;
import java.io.NotSerializableException;
import org.xml.sax.ContentHandler;
import org.apache.tika.exception.TikaException;
import java.io.IOException;
import java.util.Collection;
import java.util.Arrays;
import java.util.ArrayList;
import java.io.InputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.util.List;

class ForkClient
{
    private final List<ForkResource> resources;
    private final ClassLoader loader;
    private final File jar;
    private final Process process;
    private final DataOutputStream output;
    private final DataInputStream input;
    private final InputStream error;
    
    public ForkClient(final ClassLoader loader, final Object object, final String java) throws IOException, TikaException {
        this.resources = new ArrayList<ForkResource>();
        boolean ok = false;
        try {
            this.loader = loader;
            this.jar = createBootstrapJar();
            final ProcessBuilder builder = new ProcessBuilder(new String[0]);
            final List<String> command = new ArrayList<String>();
            command.addAll(Arrays.asList(java.split("\\s+")));
            command.add("-jar");
            command.add(this.jar.getPath());
            builder.command(command);
            this.process = builder.start();
            this.output = new DataOutputStream(this.process.getOutputStream());
            this.input = new DataInputStream(this.process.getInputStream());
            this.error = this.process.getErrorStream();
            this.waitForStartBeacon();
            this.sendObject(loader, this.resources);
            this.sendObject(object, this.resources);
            ok = true;
        }
        finally {
            if (!ok) {
                this.close();
            }
        }
    }
    
    private void waitForStartBeacon() throws IOException {
        int type;
        do {
            this.consumeErrorStream();
            type = this.input.read();
        } while ((byte)type != 4);
        this.consumeErrorStream();
    }
    
    public synchronized boolean ping() {
        try {
            this.output.writeByte(2);
            this.output.flush();
            this.consumeErrorStream();
            final int type = this.input.read();
            if (type == 2) {
                this.consumeErrorStream();
                return true;
            }
            return false;
        }
        catch (IOException e) {
            return false;
        }
    }
    
    public synchronized Throwable call(final String method, final Object... args) throws IOException, TikaException {
        final List<ForkResource> r = new ArrayList<ForkResource>(this.resources);
        this.output.writeByte(1);
        this.output.writeUTF(method);
        for (int i = 0; i < args.length; ++i) {
            this.sendObject(args[i], r);
        }
        return this.waitForResponse(r);
    }
    
    private void sendObject(Object object, final List<ForkResource> resources) throws IOException, TikaException {
        final int n = resources.size();
        if (object instanceof InputStream) {
            resources.add(new InputStreamResource((InputStream)object));
            object = new InputStreamProxy(n);
        }
        else if (object instanceof ContentHandler) {
            resources.add(new ContentHandlerResource((ContentHandler)object));
            object = new ContentHandlerProxy(n);
        }
        else if (object instanceof ClassLoader) {
            resources.add(new ClassLoaderResource((ClassLoader)object));
            object = new ClassLoaderProxy(n);
        }
        try {
            ForkObjectInputStream.sendObject(object, this.output);
        }
        catch (NotSerializableException nse) {
            throw new TikaException("Unable to serialize " + object.getClass().getSimpleName() + " to pass to the Forked Parser", nse);
        }
        this.waitForResponse(resources);
    }
    
    public synchronized void close() {
        try {
            if (this.output != null) {
                this.output.close();
            }
            if (this.input != null) {
                this.input.close();
            }
            if (this.error != null) {
                this.error.close();
            }
        }
        catch (IOException ex) {}
        if (this.process != null) {
            this.process.destroy();
        }
        if (this.jar != null) {
            this.jar.delete();
        }
    }
    
    private Throwable waitForResponse(final List<ForkResource> resources) throws IOException {
        this.output.flush();
        while (true) {
            this.consumeErrorStream();
            final int type = this.input.read();
            if (type == -1) {
                this.consumeErrorStream();
                throw new IOException("Lost connection to a forked server process");
            }
            if (type != 3) {
                if ((byte)type == -1) {
                    try {
                        return (Throwable)ForkObjectInputStream.readObject(this.input, this.loader);
                    }
                    catch (ClassNotFoundException e) {
                        throw new IOExceptionWithCause("Unable to deserialize an exception", e);
                    }
                }
                return null;
            }
            final ForkResource resource = resources.get(this.input.readUnsignedByte());
            resource.process(this.input, this.output);
        }
    }
    
    private void consumeErrorStream() throws IOException {
        int n;
        while ((n = this.error.available()) > 0) {
            final byte[] b = new byte[n];
            n = this.error.read(b);
            if (n > 0) {
                System.err.write(b, 0, n);
            }
        }
    }
    
    private static File createBootstrapJar() throws IOException {
        final File file = File.createTempFile("apache-tika-fork-", ".jar");
        boolean ok = false;
        try {
            fillBootstrapJar(file);
            ok = true;
        }
        finally {
            if (!ok) {
                file.delete();
            }
        }
        return file;
    }
    
    private static void fillBootstrapJar(final File file) throws IOException {
        final JarOutputStream jar = new JarOutputStream(new FileOutputStream(file));
        try {
            final String manifest = "Main-Class: " + ForkServer.class.getName() + "\n";
            jar.putNextEntry(new ZipEntry("META-INF/MANIFEST.MF"));
            jar.write(manifest.getBytes("UTF-8"));
            final Class<?>[] bootstrap = (Class<?>[])new Class[] { ForkServer.class, ForkObjectInputStream.class, ForkProxy.class, ClassLoaderProxy.class, MemoryURLConnection.class, MemoryURLStreamHandler.class, MemoryURLStreamHandlerFactory.class, MemoryURLStreamRecord.class };
            final ClassLoader loader = ForkServer.class.getClassLoader();
            for (final Class<?> klass : bootstrap) {
                final String path = klass.getName().replace('.', '/') + ".class";
                final InputStream input = loader.getResourceAsStream(path);
                try {
                    jar.putNextEntry(new JarEntry(path));
                    IOUtils.copy(input, jar);
                }
                finally {
                    input.close();
                }
            }
        }
        finally {
            jar.close();
        }
    }
}
