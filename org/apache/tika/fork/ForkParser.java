// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.fork;

import java.util.Iterator;
import org.xml.sax.SAXException;
import java.io.IOException;
import org.apache.tika.exception.TikaException;
import org.apache.tika.metadata.Metadata;
import org.xml.sax.ContentHandler;
import java.io.InputStream;
import org.apache.tika.mime.MediaType;
import java.util.Set;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.parser.AutoDetectParser;
import java.util.LinkedList;
import java.util.Queue;
import org.apache.tika.parser.Parser;
import org.apache.tika.parser.AbstractParser;

public class ForkParser extends AbstractParser
{
    private static final long serialVersionUID = -4962742892274663950L;
    private final ClassLoader loader;
    private final Parser parser;
    private String java;
    private int poolSize;
    private int currentlyInUse;
    private final Queue<ForkClient> pool;
    
    public ForkParser(final ClassLoader loader, final Parser parser) {
        this.java = "java -Xmx32m";
        this.poolSize = 5;
        this.currentlyInUse = 0;
        this.pool = new LinkedList<ForkClient>();
        if (parser instanceof ForkParser) {
            throw new IllegalArgumentException("The underlying parser of a ForkParser should not be a ForkParser, but a specific implementation.");
        }
        this.loader = loader;
        this.parser = parser;
    }
    
    public ForkParser(final ClassLoader loader) {
        this(loader, new AutoDetectParser());
    }
    
    public ForkParser() {
        this(ForkParser.class.getClassLoader());
    }
    
    public synchronized int getPoolSize() {
        return this.poolSize;
    }
    
    public synchronized void setPoolSize(final int poolSize) {
        this.poolSize = poolSize;
    }
    
    public String getJavaCommand() {
        return this.java;
    }
    
    public void setJavaCommand(final String java) {
        this.java = java;
    }
    
    public Set<MediaType> getSupportedTypes(final ParseContext context) {
        return this.parser.getSupportedTypes(context);
    }
    
    public void parse(final InputStream stream, final ContentHandler handler, final Metadata metadata, final ParseContext context) throws IOException, SAXException, TikaException {
        if (stream == null) {
            throw new NullPointerException("null stream");
        }
        boolean alive = false;
        final ForkClient client = this.acquireClient();
        Throwable t;
        try {
            t = client.call("parse", stream, handler, metadata, context);
            alive = true;
        }
        catch (TikaException te) {
            alive = true;
            throw te;
        }
        catch (IOException e) {
            throw new TikaException("Failed to communicate with a forked parser process. The process has most likely crashed due to some error like running out of memory. A new process will be started for the next parsing request.", e);
        }
        finally {
            this.releaseClient(client, alive);
        }
        if (t instanceof IOException) {
            throw (IOException)t;
        }
        if (t instanceof SAXException) {
            throw (SAXException)t;
        }
        if (t instanceof TikaException) {
            throw (TikaException)t;
        }
        if (t != null) {
            throw new TikaException("Unexpected error in forked server process", t);
        }
    }
    
    public synchronized void close() {
        for (final ForkClient client : this.pool) {
            client.close();
        }
        this.pool.clear();
        this.poolSize = 0;
    }
    
    private synchronized ForkClient acquireClient() throws IOException, TikaException {
        ForkClient client;
        while (true) {
            client = this.pool.poll();
            if (client == null && this.currentlyInUse < this.poolSize) {
                client = new ForkClient(this.loader, this.parser, this.java);
            }
            if (client != null && !client.ping()) {
                client.close();
                client = null;
            }
            if (client != null) {
                break;
            }
            if (this.currentlyInUse < this.poolSize) {
                continue;
            }
            try {
                this.wait();
            }
            catch (InterruptedException e) {
                throw new TikaException("Interrupted while waiting for a fork parser", e);
            }
        }
        ++this.currentlyInUse;
        return client;
    }
    
    private synchronized void releaseClient(final ForkClient client, final boolean alive) {
        --this.currentlyInUse;
        if (this.currentlyInUse + this.pool.size() < this.poolSize && alive) {
            this.pool.offer(client);
            this.notifyAll();
        }
        else {
            client.close();
        }
    }
}
