// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.fork;

import java.net.URLStreamHandler;
import java.net.URLStreamHandlerFactory;

class MemoryURLStreamHandlerFactory implements URLStreamHandlerFactory
{
    public URLStreamHandler createURLStreamHandler(final String protocol) {
        if ("tika-in-memory".equals(protocol)) {
            return new MemoryURLStreamHandler();
        }
        return null;
    }
}
