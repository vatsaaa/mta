// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.mime;

public interface MimeTypesReaderMetKeys
{
    public static final String MIME_INFO_TAG = "mime-info";
    public static final String MIME_TYPE_TAG = "mime-type";
    public static final String MIME_TYPE_TYPE_ATTR = "type";
    public static final String COMMENT_TAG = "_comment";
    public static final String GLOB_TAG = "glob";
    public static final String ISREGEX_ATTR = "isregex";
    public static final String PATTERN_ATTR = "pattern";
    public static final String MAGIC_TAG = "magic";
    public static final String ALIAS_TAG = "alias";
    public static final String ALIAS_TYPE_ATTR = "type";
    public static final String ROOT_XML_TAG = "root-XML";
    public static final String SUB_CLASS_OF_TAG = "sub-class-of";
    public static final String SUB_CLASS_TYPE_ATTR = "type";
    public static final String MAGIC_PRIORITY_ATTR = "priority";
    public static final String MATCH_TAG = "match";
    public static final String MATCH_OFFSET_ATTR = "offset";
    public static final String MATCH_TYPE_ATTR = "type";
    public static final String MATCH_VALUE_ATTR = "value";
    public static final String MATCH_MASK_ATTR = "mask";
    public static final String NS_URI_ATTR = "namespaceURI";
    public static final String LOCAL_NAME_ATTR = "localName";
}
