// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.mime;

import java.io.Serializable;

interface Clause extends Serializable
{
    boolean eval(final byte[] p0);
    
    int size();
}
