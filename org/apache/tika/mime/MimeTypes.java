// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.mime;

import java.net.URISyntaxException;
import java.net.URI;
import java.util.Collections;
import java.util.Collection;
import javax.xml.namespace.QName;
import java.util.Iterator;
import java.io.InputStream;
import org.apache.tika.metadata.Metadata;
import java.io.ByteArrayInputStream;
import org.apache.tika.detect.TextDetector;
import org.apache.tika.detect.XmlRootExtractor;
import java.io.IOException;
import org.apache.tika.Tika;
import java.io.File;
import java.util.Locale;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.io.Serializable;
import org.apache.tika.detect.Detector;

public final class MimeTypes implements Detector, Serializable
{
    private static final long serialVersionUID = -1350863170146349036L;
    public static final String OCTET_STREAM = "application/octet-stream";
    public static final String PLAIN_TEXT = "text/plain";
    public static final String XML = "application/xml";
    private final MimeType rootMimeType;
    private final MimeType textMimeType;
    private final MimeType xmlMimeType;
    private final MediaTypeRegistry registry;
    private final Map<MediaType, MimeType> types;
    private Patterns patterns;
    private final List<Magic> magics;
    private final List<MimeType> xmls;
    private static MimeTypes DEFAULT_TYPES;
    
    public MimeTypes() {
        this.registry = new MediaTypeRegistry();
        this.types = new HashMap<MediaType, MimeType>();
        this.patterns = new Patterns(this.registry);
        this.magics = new ArrayList<Magic>();
        this.xmls = new ArrayList<MimeType>();
        this.rootMimeType = new MimeType(MediaType.OCTET_STREAM);
        this.textMimeType = new MimeType(MediaType.TEXT_PLAIN);
        this.xmlMimeType = new MimeType(MediaType.APPLICATION_XML);
        this.add(this.rootMimeType);
        this.add(this.textMimeType);
        this.add(this.xmlMimeType);
    }
    
    @Deprecated
    public MimeType getMimeType(final String name) {
        MimeType type = this.patterns.matches(name);
        if (type != null) {
            return type;
        }
        type = this.patterns.matches(name.toLowerCase(Locale.ENGLISH));
        if (type != null) {
            return type;
        }
        return this.rootMimeType;
    }
    
    @Deprecated
    public MimeType getMimeType(final File file) throws MimeTypeException, IOException {
        return this.forName(new Tika(this).detect(file));
    }
    
    private MimeType getMimeType(final byte[] data) {
        if (data == null) {
            throw new IllegalArgumentException("Data is missing");
        }
        if (data.length == 0) {
            return this.rootMimeType;
        }
        MimeType result = null;
        for (final Magic magic : this.magics) {
            if (magic.eval(data)) {
                result = magic.getType();
                break;
            }
        }
        if (result != null) {
            if ("application/xml".equals(result.getName()) || "text/html".equals(result.getName())) {
                final XmlRootExtractor extractor = new XmlRootExtractor();
                final QName rootElement = extractor.extractRootElement(data);
                if (rootElement != null) {
                    for (final MimeType type : this.xmls) {
                        if (type.matchesXML(rootElement.getNamespaceURI(), rootElement.getLocalPart())) {
                            result = type;
                            break;
                        }
                    }
                }
                else if ("application/xml".equals(result.getName())) {
                    result = this.textMimeType;
                }
            }
            return result;
        }
        try {
            final TextDetector detector = new TextDetector(this.getMinLength());
            final ByteArrayInputStream stream = new ByteArrayInputStream(data);
            return this.forName(detector.detect(stream, new Metadata()).toString());
        }
        catch (Exception e) {
            return this.rootMimeType;
        }
    }
    
    private byte[] readMagicHeader(final InputStream stream) throws IOException {
        if (stream == null) {
            throw new IllegalArgumentException("InputStream is missing");
        }
        final byte[] bytes = new byte[this.getMinLength()];
        int totalRead = 0;
        for (int lastRead = stream.read(bytes); lastRead != -1; lastRead = stream.read(bytes, totalRead, bytes.length - totalRead)) {
            totalRead += lastRead;
            if (totalRead == bytes.length) {
                return bytes;
            }
        }
        final byte[] shorter = new byte[totalRead];
        System.arraycopy(bytes, 0, shorter, 0, totalRead);
        return shorter;
    }
    
    public MimeType forName(final String name) throws MimeTypeException {
        final MediaType type = MediaType.parse(name);
        if (type != null) {
            final MediaType normalisedType = this.registry.normalize(type);
            MimeType mime = this.types.get(normalisedType);
            if (mime == null) {
                synchronized (this) {
                    mime = this.types.get(normalisedType);
                    if (mime == null) {
                        mime = new MimeType(type);
                        this.add(mime);
                        this.types.put(type, mime);
                    }
                }
            }
            return mime;
        }
        throw new MimeTypeException("Invalid media type name: " + name);
    }
    
    public synchronized void setSuperType(final MimeType type, final MediaType parent) {
        this.registry.addSuperType(type.getType(), parent);
    }
    
    synchronized void addAlias(final MimeType type, final MediaType alias) {
        this.registry.addAlias(type.getType(), alias);
    }
    
    public void addPattern(final MimeType type, final String pattern) throws MimeTypeException {
        this.addPattern(type, pattern, false);
    }
    
    public void addPattern(final MimeType type, final String pattern, final boolean isRegex) throws MimeTypeException {
        this.patterns.add(pattern, isRegex, type);
    }
    
    public MediaTypeRegistry getMediaTypeRegistry() {
        return this.registry;
    }
    
    public int getMinLength() {
        return 65536;
    }
    
    void add(final MimeType type) {
        this.registry.addType(type.getType());
        this.types.put(type.getType(), type);
        if (type.hasMagic()) {
            this.magics.addAll(type.getMagics());
        }
        if (type.hasRootXML()) {
            this.xmls.add(type);
        }
    }
    
    void init() {
        for (final MimeType type : this.types.values()) {
            this.magics.addAll(type.getMagics());
            if (type.hasRootXML()) {
                this.xmls.add(type);
            }
        }
        Collections.sort(this.magics);
        Collections.sort(this.xmls);
    }
    
    public MediaType detect(final InputStream input, final Metadata metadata) throws IOException {
        MediaType type = MediaType.OCTET_STREAM;
        if (input != null) {
            input.mark(this.getMinLength());
            try {
                final byte[] prefix = this.readMagicHeader(input);
                type = this.getMimeType(prefix).getType();
            }
            finally {
                input.reset();
            }
        }
        final String resourceName = metadata.get("resourceName");
        if (resourceName != null) {
            String name = null;
            try {
                final URI uri = new URI(resourceName);
                final String path = uri.getPath();
                if (path != null) {
                    final int slash = path.lastIndexOf(47);
                    if (slash + 1 < path.length()) {
                        name = path.substring(slash + 1);
                    }
                }
            }
            catch (URISyntaxException e) {
                name = resourceName;
            }
            if (name != null) {
                final MediaType hint = this.getMimeType(name).getType();
                if (this.registry.isSpecializationOf(hint, type)) {
                    type = hint;
                }
            }
        }
        final String typeName = metadata.get("Content-Type");
        if (typeName != null) {
            try {
                final MediaType hint = this.forName(typeName).getType();
                if (this.registry.isSpecializationOf(hint, type)) {
                    type = hint;
                }
            }
            catch (MimeTypeException ex) {}
        }
        return type;
    }
    
    public static synchronized MimeTypes getDefaultMimeTypes() {
        if (MimeTypes.DEFAULT_TYPES == null) {
            try {
                MimeTypes.DEFAULT_TYPES = MimeTypesFactory.create("tika-mimetypes.xml", "custom-mimetypes.xml");
            }
            catch (MimeTypeException e) {
                throw new RuntimeException("Unable to parse the default media type registry", e);
            }
            catch (IOException e2) {
                throw new RuntimeException("Unable to read the default media type registry", e2);
            }
        }
        return MimeTypes.DEFAULT_TYPES;
    }
    
    static {
        MimeTypes.DEFAULT_TYPES = null;
    }
}
