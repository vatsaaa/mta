// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.mime;

import java.util.Collection;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Iterator;
import org.xml.sax.Attributes;
import java.io.ByteArrayInputStream;
import org.xml.sax.InputSource;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import org.xml.sax.ContentHandler;
import javax.xml.transform.sax.SAXResult;
import org.w3c.dom.Node;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.TransformerFactory;
import org.w3c.dom.Document;
import java.io.IOException;
import javax.xml.parsers.SAXParser;
import org.xml.sax.SAXException;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParserFactory;
import java.io.InputStream;
import org.xml.sax.helpers.DefaultHandler;

class MimeTypesReader extends DefaultHandler implements MimeTypesReaderMetKeys
{
    private final MimeTypes types;
    private MimeType type;
    private int priority;
    private StringBuilder characters;
    private ClauseRecord current;
    
    MimeTypesReader(final MimeTypes types) {
        this.type = null;
        this.characters = null;
        this.current = new ClauseRecord(null);
        this.types = types;
    }
    
    void read(final InputStream stream) throws IOException, MimeTypeException {
        try {
            final SAXParserFactory factory = SAXParserFactory.newInstance();
            factory.setNamespaceAware(false);
            final SAXParser parser = factory.newSAXParser();
            parser.parse(stream, this);
        }
        catch (ParserConfigurationException e) {
            throw new MimeTypeException("Unable to create an XML parser", e);
        }
        catch (SAXException e2) {
            throw new MimeTypeException("Invalid type configuration", e2);
        }
    }
    
    void read(final Document document) throws MimeTypeException {
        try {
            final TransformerFactory factory = TransformerFactory.newInstance();
            final Transformer transformer = factory.newTransformer();
            transformer.transform(new DOMSource(document), new SAXResult(this));
        }
        catch (TransformerException e) {
            throw new MimeTypeException("Failed to parse type registry", e);
        }
    }
    
    @Override
    public InputSource resolveEntity(final String publicId, final String systemId) {
        return new InputSource(new ByteArrayInputStream(new byte[0]));
    }
    
    @Override
    public void startElement(final String uri, final String localName, final String qName, final Attributes attributes) throws SAXException {
        if (this.type == null) {
            if ("mime-type".equals(qName)) {
                final String name = attributes.getValue("type");
                try {
                    this.type = this.types.forName(name);
                }
                catch (MimeTypeException e) {
                    throw new SAXException(e);
                }
            }
        }
        else if ("alias".equals(qName)) {
            final String alias = attributes.getValue("type");
            this.types.addAlias(this.type, MediaType.parse(alias));
        }
        else if ("sub-class-of".equals(qName)) {
            final String parent = attributes.getValue("type");
            this.types.setSuperType(this.type, MediaType.parse(parent));
        }
        else if ("_comment".equals(qName)) {
            this.characters = new StringBuilder();
        }
        else if ("glob".equals(qName)) {
            final String pattern = attributes.getValue("pattern");
            final String isRegex = attributes.getValue("isregex");
            if (pattern != null) {
                try {
                    this.types.addPattern(this.type, pattern, Boolean.valueOf(isRegex));
                }
                catch (MimeTypeException e2) {
                    throw new SAXException(e2);
                }
            }
        }
        else if ("root-XML".equals(qName)) {
            final String namespace = attributes.getValue("namespaceURI");
            final String name2 = attributes.getValue("localName");
            this.type.addRootXML(namespace, name2);
        }
        else if ("match".equals(qName)) {
            String kind = attributes.getValue("type");
            final String offset = attributes.getValue("offset");
            final String value = attributes.getValue("value");
            final String mask = attributes.getValue("mask");
            if (kind == null) {
                kind = "string";
            }
            this.current = new ClauseRecord(new MagicMatch(this.type.getType(), kind, offset, value, mask));
        }
        else if ("magic".equals(qName)) {
            final String value2 = attributes.getValue("priority");
            if (value2 != null && value2.length() > 0) {
                this.priority = Integer.parseInt(value2);
            }
            else {
                this.priority = 50;
            }
            this.current = new ClauseRecord(null);
        }
    }
    
    @Override
    public void endElement(final String uri, final String localName, final String qName) {
        if (this.type != null) {
            if ("mime-type".equals(qName)) {
                this.type = null;
            }
            else if ("_comment".equals(qName)) {
                this.type.setDescription(this.characters.toString().trim());
                this.characters = null;
            }
            else if ("match".equals(qName)) {
                this.current.stop();
            }
            else if ("magic".equals(qName)) {
                for (final Clause clause : this.current.getClauses()) {
                    this.type.addMagic(new Magic(this.type, this.priority, clause));
                }
                this.current = null;
            }
        }
    }
    
    @Override
    public void characters(final char[] ch, final int start, final int length) {
        if (this.characters != null) {
            this.characters.append(ch, start, length);
        }
    }
    
    private class ClauseRecord
    {
        private ClauseRecord parent;
        private Clause clause;
        private List<Clause> subclauses;
        
        public ClauseRecord(final Clause clause) {
            this.subclauses = null;
            this.parent = MimeTypesReader.this.current;
            this.clause = clause;
        }
        
        public void stop() {
            if (this.subclauses != null) {
                Clause subclause;
                if (this.subclauses.size() == 1) {
                    subclause = this.subclauses.get(0);
                }
                else {
                    subclause = new OrClause(this.subclauses);
                }
                this.clause = new AndClause(new Clause[] { this.clause, subclause });
            }
            if (this.parent.subclauses == null) {
                this.parent.subclauses = Collections.singletonList(this.clause);
            }
            else {
                if (this.parent.subclauses.size() == 1) {
                    this.parent.subclauses = new ArrayList<Clause>(this.parent.subclauses);
                }
                this.parent.subclauses.add(this.clause);
            }
            MimeTypesReader.this.current = MimeTypesReader.this.current.parent;
        }
        
        public List<Clause> getClauses() {
            return this.subclauses;
        }
    }
}
