// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.mime;

import java.util.List;
import java.util.Collection;
import java.util.ArrayList;
import java.util.Collections;
import java.net.URL;
import java.io.IOException;
import java.io.InputStream;
import org.w3c.dom.Document;

public class MimeTypesFactory
{
    public static MimeTypes create() {
        return new MimeTypes();
    }
    
    public static MimeTypes create(final Document document) throws MimeTypeException {
        final MimeTypes mimeTypes = new MimeTypes();
        new MimeTypesReader(mimeTypes).read(document);
        mimeTypes.init();
        return mimeTypes;
    }
    
    public static MimeTypes create(final InputStream... inputStreams) throws IOException, MimeTypeException {
        final MimeTypes mimeTypes = new MimeTypes();
        final MimeTypesReader reader = new MimeTypesReader(mimeTypes);
        for (final InputStream inputStream : inputStreams) {
            reader.read(inputStream);
        }
        mimeTypes.init();
        return mimeTypes;
    }
    
    public static MimeTypes create(final InputStream stream) throws IOException, MimeTypeException {
        return create(new InputStream[] { stream });
    }
    
    public static MimeTypes create(final URL... urls) throws IOException, MimeTypeException {
        final InputStream[] streams = new InputStream[urls.length];
        for (int i = 0; i < streams.length; ++i) {
            streams[i] = urls[i].openStream();
        }
        try {
            return create(streams);
        }
        finally {
            for (final InputStream stream : streams) {
                stream.close();
            }
        }
    }
    
    public static MimeTypes create(final URL url) throws IOException, MimeTypeException {
        return create(new URL[] { url });
    }
    
    public static MimeTypes create(final String filePath) throws IOException, MimeTypeException {
        return create(MimeTypesReader.class.getResource(filePath));
    }
    
    public static MimeTypes create(final String coreFilePath, final String extensionFilePath) throws IOException, MimeTypeException {
        final String classPrefix = MimeTypesReader.class.getPackage().getName().replace('.', '/') + "/";
        final ClassLoader cl = MimeTypesReader.class.getClassLoader();
        final URL coreURL = cl.getResource(classPrefix + coreFilePath);
        final List<URL> extensionURLs = Collections.list(cl.getResources(classPrefix + extensionFilePath));
        final List<URL> urls = new ArrayList<URL>();
        urls.add(coreURL);
        urls.addAll(extensionURLs);
        return create((URL[])urls.toArray(new URL[urls.size()]));
    }
}
