// 
// Decompiled by Procyon v0.5.36
// 

package org.apache.tika.cli;

import java.text.ParsePosition;
import com.google.gson.Gson;
import java.text.NumberFormat;
import org.apache.tika.exception.TikaException;
import org.apache.tika.xmp.XMPMetadata;
import java.net.Socket;
import java.net.ServerSocket;
import org.apache.poi.poifs.filesystem.DocumentInputStream;
import org.apache.poi.poifs.filesystem.DocumentEntry;
import org.apache.poi.poifs.filesystem.Entry;
import java.io.IOException;
import org.xml.sax.SAXException;
import org.apache.tika.io.IOUtils;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.poifs.filesystem.DirectoryEntry;
import org.apache.tika.mime.MimeTypeException;
import org.apache.tika.config.TikaConfig;
import org.apache.tika.fork.ForkParser;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.Result;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.sax.SAXTransformerFactory;
import javax.xml.transform.sax.TransformerHandler;
import java.io.UnsupportedEncodingException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import org.apache.tika.mime.MediaTypeRegistry;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Set;
import java.util.Map;
import java.util.List;
import org.apache.tika.detect.CompositeDetector;
import java.util.Iterator;
import org.apache.tika.mime.MediaType;
import org.apache.tika.parser.ParserDecorator;
import org.apache.tika.parser.CompositeParser;
import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.Comparator;
import org.apache.tika.Tika;
import java.io.PrintStream;
import java.net.URL;
import org.apache.tika.io.TikaInputStream;
import org.apache.tika.io.CloseShieldInputStream;
import org.apache.tika.parser.NetworkParser;
import java.net.URI;
import org.apache.tika.extractor.EmbeddedDocumentExtractor;
import org.apache.tika.gui.TikaGUI;
import org.apache.tika.parser.PasswordProvider;
import org.apache.tika.parser.AutoDetectParser;
import org.apache.tika.detect.DefaultDetector;
import java.io.FileOutputStream;
import java.io.InputStream;
import org.apache.tika.language.ProfilingHandler;
import java.io.PrintWriter;
import org.apache.tika.parser.html.BoilerpipeContentHandler;
import org.xml.sax.helpers.DefaultHandler;
import org.apache.tika.sax.BodyContentHandler;
import org.xml.sax.ContentHandler;
import org.apache.tika.metadata.Metadata;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.Appender;
import org.apache.log4j.BasicConfigurator;
import java.io.OutputStream;
import org.apache.log4j.Layout;
import org.apache.log4j.WriterAppender;
import org.apache.log4j.SimpleLayout;
import org.apache.tika.language.LanguageProfilerBuilder;
import org.apache.tika.parser.Parser;
import org.apache.tika.detect.Detector;
import org.apache.tika.parser.ParseContext;
import java.io.File;

public class TikaCLI
{
    private File extractDir;
    private final OutputType XML;
    private final OutputType HTML;
    private final OutputType TEXT;
    private final OutputType NO_OUTPUT;
    private final OutputType TEXT_MAIN;
    private final OutputType METADATA;
    private final OutputType JSON;
    private final OutputType XMP;
    private final OutputType LANGUAGE;
    private final OutputType DETECT;
    private final OutputType CREATE_PROFILE;
    private ParseContext context;
    private Detector detector;
    private Parser parser;
    private OutputType type;
    private LanguageProfilerBuilder ngp;
    private String encoding;
    private String password;
    private boolean pipeMode;
    private boolean serverMode;
    private boolean fork;
    private String profileName;
    private boolean prettyPrint;
    
    public static void main(final String[] args) throws Exception {
        BasicConfigurator.configure(new WriterAppender(new SimpleLayout(), System.err));
        Logger.getRootLogger().setLevel(Level.INFO);
        final TikaCLI cli = new TikaCLI();
        if (args.length > 0) {
            for (int i = 0; i < args.length; ++i) {
                cli.process(args[i]);
            }
            if (cli.pipeMode) {
                cli.process("-");
            }
        }
        else {
            if (System.in.available() == 0) {
                Thread.sleep(100L);
            }
            if (System.in.available() > 0) {
                cli.process("-");
            }
            else {
                cli.process("--gui");
            }
        }
    }
    
    public TikaCLI() throws Exception {
        this.extractDir = new File(".");
        this.XML = new OutputType() {
            @Override
            protected ContentHandler getContentHandler(final OutputStream output, final Metadata metadata) throws Exception {
                return getTransformerHandler(output, "xml", TikaCLI.this.encoding, TikaCLI.this.prettyPrint);
            }
        };
        this.HTML = new OutputType() {
            @Override
            protected ContentHandler getContentHandler(final OutputStream output, final Metadata metadata) throws Exception {
                return getTransformerHandler(output, "html", TikaCLI.this.encoding, TikaCLI.this.prettyPrint);
            }
        };
        this.TEXT = new OutputType() {
            @Override
            protected ContentHandler getContentHandler(final OutputStream output, final Metadata metadata) throws Exception {
                return new BodyContentHandler(getOutputWriter(output, TikaCLI.this.encoding));
            }
        };
        this.NO_OUTPUT = new OutputType() {
            @Override
            protected ContentHandler getContentHandler(final OutputStream output, final Metadata metadata) {
                return new DefaultHandler();
            }
        };
        this.TEXT_MAIN = new OutputType() {
            @Override
            protected ContentHandler getContentHandler(final OutputStream output, final Metadata metadata) throws Exception {
                return new BoilerpipeContentHandler(getOutputWriter(output, TikaCLI.this.encoding));
            }
        };
        this.METADATA = new OutputType() {
            @Override
            protected ContentHandler getContentHandler(final OutputStream output, final Metadata metadata) throws Exception {
                final PrintWriter writer = new PrintWriter(getOutputWriter(output, TikaCLI.this.encoding));
                return new NoDocumentMetHandler(metadata, writer);
            }
        };
        this.JSON = new OutputType() {
            @Override
            protected ContentHandler getContentHandler(final OutputStream output, final Metadata metadata) throws Exception {
                final PrintWriter writer = new PrintWriter(getOutputWriter(output, TikaCLI.this.encoding));
                return new NoDocumentJSONMetHandler(metadata, writer);
            }
        };
        this.XMP = new OutputType() {
            @Override
            protected ContentHandler getContentHandler(final OutputStream output, final Metadata metadata) throws Exception {
                final PrintWriter writer = new PrintWriter(getOutputWriter(output, TikaCLI.this.encoding));
                return new NoDocumentXMPMetaHandler(metadata, writer);
            }
        };
        this.LANGUAGE = new OutputType() {
            @Override
            protected ContentHandler getContentHandler(final OutputStream output, final Metadata metadata) throws Exception {
                final PrintWriter writer = new PrintWriter(getOutputWriter(output, TikaCLI.this.encoding));
                return new ProfilingHandler() {
                    @Override
                    public void endDocument() {
                        writer.println(this.getLanguage().getLanguage());
                        writer.flush();
                    }
                };
            }
        };
        this.DETECT = new OutputType() {
            @Override
            public void process(final InputStream stream, final OutputStream output, final Metadata metadata) throws Exception {
                final PrintWriter writer = new PrintWriter(getOutputWriter(output, TikaCLI.this.encoding));
                writer.println(TikaCLI.this.detector.detect(stream, metadata).toString());
                writer.flush();
            }
        };
        this.CREATE_PROFILE = new OutputType() {
            @Override
            public void process(final InputStream stream, final OutputStream output, final Metadata metadata) throws Exception {
                TikaCLI.this.ngp = LanguageProfilerBuilder.create(TikaCLI.this.profileName, stream, TikaCLI.this.encoding);
                final FileOutputStream fos = new FileOutputStream(new File(TikaCLI.this.profileName + ".ngp"));
                TikaCLI.this.ngp.save(fos);
                fos.close();
                final PrintWriter writer = new PrintWriter(getOutputWriter(output, TikaCLI.this.encoding));
                writer.println("ngram profile location:=" + new File(TikaCLI.this.ngp.getName()).getCanonicalPath());
                writer.flush();
            }
        };
        this.type = this.XML;
        this.ngp = null;
        this.encoding = null;
        this.password = System.getenv("TIKA_PASSWORD");
        this.pipeMode = true;
        this.serverMode = false;
        this.fork = false;
        this.profileName = null;
        this.context = new ParseContext();
        this.detector = new DefaultDetector();
        this.parser = new AutoDetectParser(this.detector);
        this.context.set(Parser.class, this.parser);
        this.context.set((Class<TikaCLI$12>)PasswordProvider.class, new PasswordProvider() {
            public String getPassword(final Metadata metadata) {
                return TikaCLI.this.password;
            }
        });
    }
    
    public void process(final String arg) throws Exception {
        if (arg.equals("-?") || arg.equals("--help")) {
            this.pipeMode = false;
            this.usage();
        }
        else if (arg.equals("-V") || arg.equals("--version")) {
            this.pipeMode = false;
            this.version();
        }
        else if (arg.equals("-v") || arg.equals("--verbose")) {
            Logger.getRootLogger().setLevel(Level.DEBUG);
        }
        else if (arg.equals("-g") || arg.equals("--gui")) {
            this.pipeMode = false;
            TikaGUI.main(new String[0]);
        }
        else if (arg.equals("--list-parser") || arg.equals("--list-parsers")) {
            this.displayParsers(this.pipeMode = false);
        }
        else if (arg.equals("--list-detector") || arg.equals("--list-detectors")) {
            this.pipeMode = false;
            this.displayDetectors();
        }
        else if (arg.equals("--list-parser-detail") || arg.equals("--list-parser-details")) {
            this.pipeMode = false;
            this.displayParsers(true);
        }
        else if (arg.equals("--list-met-models")) {
            this.pipeMode = false;
            this.displayMetModels();
        }
        else if (arg.equals("--list-supported-types")) {
            this.pipeMode = false;
            this.displaySupportedTypes();
        }
        else if (!arg.equals("--container-aware")) {
            if (!arg.equals("--container-aware-detector")) {
                if (arg.equals("-f") || arg.equals("--fork")) {
                    this.fork = true;
                }
                else if (arg.startsWith("-e")) {
                    this.encoding = arg.substring("-e".length());
                }
                else if (arg.startsWith("--encoding=")) {
                    this.encoding = arg.substring("--encoding=".length());
                }
                else if (arg.startsWith("-p") && !arg.equals("-p")) {
                    this.password = arg.substring("-p".length());
                }
                else if (arg.startsWith("--password=")) {
                    this.password = arg.substring("--password=".length());
                }
                else if (arg.equals("-j") || arg.equals("--json")) {
                    this.type = this.JSON;
                }
                else if (arg.equals("-y") || arg.equals("--xmp")) {
                    this.type = this.XMP;
                }
                else if (arg.equals("-x") || arg.equals("--xml")) {
                    this.type = this.XML;
                }
                else if (arg.equals("-h") || arg.equals("--html")) {
                    this.type = this.HTML;
                }
                else if (arg.equals("-t") || arg.equals("--text")) {
                    this.type = this.TEXT;
                }
                else if (arg.equals("-T") || arg.equals("--text-main")) {
                    this.type = this.TEXT_MAIN;
                }
                else if (arg.equals("-m") || arg.equals("--metadata")) {
                    this.type = this.METADATA;
                }
                else if (arg.equals("-l") || arg.equals("--language")) {
                    this.type = this.LANGUAGE;
                }
                else if (arg.equals("-d") || arg.equals("--detect")) {
                    this.type = this.DETECT;
                }
                else if (arg.startsWith("--extract-dir=")) {
                    this.extractDir = new File(arg.substring("--extract-dir=".length()));
                }
                else if (arg.equals("-z") || arg.equals("--extract")) {
                    this.type = this.NO_OUTPUT;
                    this.context.set((Class<FileEmbeddedDocumentExtractor>)EmbeddedDocumentExtractor.class, new FileEmbeddedDocumentExtractor());
                }
                else if (arg.equals("-r") || arg.equals("--pretty-print")) {
                    this.prettyPrint = true;
                }
                else if (arg.equals("-p") || arg.equals("--port") || arg.equals("-s") || arg.equals("--server")) {
                    this.serverMode = true;
                    this.pipeMode = false;
                }
                else if (arg.startsWith("-c")) {
                    final URI uri = new URI(arg.substring("-c".length()));
                    this.parser = new NetworkParser(uri);
                }
                else if (arg.startsWith("--client=")) {
                    final URI uri = new URI(arg.substring("--client=".length()));
                    this.parser = new NetworkParser(uri);
                }
                else if (arg.startsWith("--create-profile=")) {
                    this.profileName = arg.substring("--create-profile=".length());
                    this.type = this.CREATE_PROFILE;
                }
                else {
                    this.pipeMode = false;
                    if (this.serverMode) {
                        new TikaServer(Integer.parseInt(arg)).start();
                    }
                    else if (arg.equals("-")) {
                        final InputStream stream = TikaInputStream.get(new CloseShieldInputStream(System.in));
                        try {
                            this.type.process(stream, System.out, new Metadata());
                        }
                        finally {
                            stream.close();
                        }
                    }
                    else {
                        final File file = new File(arg);
                        URL url;
                        if (file.isFile()) {
                            url = file.toURI().toURL();
                        }
                        else {
                            url = new URL(arg);
                        }
                        final Metadata metadata = new Metadata();
                        final InputStream input = TikaInputStream.get(url, metadata);
                        try {
                            this.type.process(input, System.out, metadata);
                        }
                        finally {
                            input.close();
                            System.out.flush();
                        }
                    }
                }
            }
        }
    }
    
    private void usage() {
        final PrintStream out = System.out;
        out.println("usage: java -jar tika-app.jar [option...] [file|port...]");
        out.println();
        out.println("Options:");
        out.println("    -?  or --help          Print this usage message");
        out.println("    -v  or --verbose       Print debug level messages");
        out.println("    -V  or --version       Print the Apache Tika version number");
        out.println();
        out.println("    -g  or --gui           Start the Apache Tika GUI");
        out.println("    -s  or --server        Start the Apache Tika server");
        out.println("    -f  or --fork          Use Fork Mode for out-of-process extraction");
        out.println();
        out.println("    -x  or --xml           Output XHTML content (default)");
        out.println("    -h  or --html          Output HTML content");
        out.println("    -t  or --text          Output plain text content");
        out.println("    -T  or --text-main     Output plain text content (main content only)");
        out.println("    -m  or --metadata      Output only metadata");
        out.println("    -j  or --json          Output metadata in JSON");
        out.println("    -y  or --xmp           Output metadata in XMP");
        out.println("    -l  or --language      Output only language");
        out.println("    -d  or --detect        Detect document type");
        out.println("    -eX or --encoding=X    Use output encoding X");
        out.println("    -pX or --password=X    Use document password X");
        out.println("    -z  or --extract       Extract all attachements into current directory");
        out.println("    --extract-dir=<dir>    Specify target directory for -z");
        out.println("    -r  or --pretty-print  For XML and XHTML outputs, adds newlines and");
        out.println("                           whitespace, for better readability");
        out.println();
        out.println("    --create-profile=X");
        out.println("         Create NGram profile, where X is a profile name");
        out.println("    --list-parsers");
        out.println("         List the available document parsers");
        out.println("    --list-parser-details");
        out.println("         List the available document parsers, and their supported mime types");
        out.println("    --list-detectors");
        out.println("         List the available document detectors");
        out.println("    --list-met-models");
        out.println("         List the available metadata models, and their supported keys");
        out.println("    --list-supported-types");
        out.println("         List all known media types and related information");
        out.println();
        out.println("Description:");
        out.println("    Apache Tika will parse the file(s) specified on the");
        out.println("    command line and output the extracted text content");
        out.println("    or metadata to standard output.");
        out.println();
        out.println("    Instead of a file name you can also specify the URL");
        out.println("    of a document to be parsed.");
        out.println();
        out.println("    If no file name or URL is specified (or the special");
        out.println("    name \"-\" is used), then the standard input stream");
        out.println("    is parsed. If no arguments were given and no input");
        out.println("    data is available, the GUI is started instead.");
        out.println();
        out.println("- GUI mode");
        out.println();
        out.println("    Use the \"--gui\" (or \"-g\") option to start the");
        out.println("    Apache Tika GUI. You can drag and drop files from");
        out.println("    a normal file explorer to the GUI window to extract");
        out.println("    text content and metadata from the files.");
        out.println();
        out.println("- Server mode");
        out.println();
        out.println("    Use the \"--server\" (or \"-s\") option to start the");
        out.println("    Apache Tika server. The server will listen to the");
        out.println("    ports you specify as one or more arguments.");
        out.println();
    }
    
    private void version() {
        System.out.println(new Tika().toString());
    }
    
    private void displayMetModels() {
        final Class<?>[] modelClasses = Metadata.class.getInterfaces();
        Arrays.sort(modelClasses, new Comparator<Class<?>>() {
            public int compare(final Class<?> o1, final Class<?> o2) {
                return o1.getName().compareTo(o2.getName());
            }
        });
        for (final Class<?> modelClass : modelClasses) {
            if (!modelClass.getSimpleName().contains("Tika")) {
                System.out.println(modelClass.getSimpleName());
                final Field[] keyFields = modelClass.getFields();
                Arrays.sort(keyFields, new Comparator<Field>() {
                    public int compare(final Field o1, final Field o2) {
                        return o1.getName().compareTo(o2.getName());
                    }
                });
                for (final Field keyField : keyFields) {
                    System.out.println(" " + keyField.getName());
                }
            }
        }
    }
    
    private void displayParsers(final boolean includeMimeTypes) {
        this.displayParser(this.parser, includeMimeTypes, 0);
    }
    
    private void displayParser(final Parser p, final boolean includeMimeTypes, final int i) {
        final boolean isComposite = p instanceof CompositeParser;
        final String name = (p instanceof ParserDecorator) ? ((ParserDecorator)p).getWrappedParser().getClass().getName() : p.getClass().getName();
        System.out.println(this.indent(i) + name + (isComposite ? " (Composite Parser):" : ""));
        if (includeMimeTypes && !isComposite) {
            for (final MediaType mt : p.getSupportedTypes(this.context)) {
                System.out.println(this.indent(i + 2) + mt);
            }
        }
        if (isComposite) {
            final Parser[] arr$;
            final Parser[] subParsers = arr$ = this.sortParsers(this.invertMediaTypeMap(((CompositeParser)p).getParsers()));
            for (final Parser sp : arr$) {
                this.displayParser(sp, includeMimeTypes, i + 2);
            }
        }
    }
    
    private void displayDetectors() {
        this.displayDetector(this.detector, 0);
    }
    
    private void displayDetector(final Detector d, final int i) {
        final boolean isComposite = d instanceof CompositeDetector;
        final String name = d.getClass().getName();
        System.out.println(this.indent(i) + name + (isComposite ? " (Composite Detector):" : ""));
        if (isComposite) {
            final List<Detector> subDetectors = ((CompositeDetector)d).getDetectors();
            for (final Detector sd : subDetectors) {
                this.displayDetector(sd, i + 2);
            }
        }
    }
    
    private String indent(final int indent) {
        return "                     ".substring(0, indent);
    }
    
    private Parser[] sortParsers(final Map<Parser, Set<MediaType>> parsers) {
        final Parser[] sortedParsers = parsers.keySet().toArray(new Parser[parsers.size()]);
        Arrays.sort(sortedParsers, new Comparator<Parser>() {
            public int compare(final Parser p1, final Parser p2) {
                final String name1 = p1.getClass().getName();
                final String name2 = p2.getClass().getName();
                return name1.compareTo(name2);
            }
        });
        return sortedParsers;
    }
    
    private Map<Parser, Set<MediaType>> invertMediaTypeMap(final Map<MediaType, Parser> supported) {
        final Map<Parser, Set<MediaType>> parsers = new HashMap<Parser, Set<MediaType>>();
        for (final Map.Entry<MediaType, Parser> e : supported.entrySet()) {
            if (!parsers.containsKey(e.getValue())) {
                parsers.put(e.getValue(), new HashSet<MediaType>());
            }
            parsers.get(e.getValue()).add(e.getKey());
        }
        return parsers;
    }
    
    private void displaySupportedTypes() {
        final AutoDetectParser parser = new AutoDetectParser();
        final MediaTypeRegistry registry = parser.getMediaTypeRegistry();
        final Map<MediaType, Parser> parsers = parser.getParsers();
        for (final MediaType type : registry.getTypes()) {
            System.out.println(type);
            for (final MediaType alias : registry.getAliases(type)) {
                System.out.println("  alias:     " + alias);
            }
            final MediaType supertype = registry.getSupertype(type);
            if (supertype != null) {
                System.out.println("  supertype: " + supertype);
            }
            final Parser p = parsers.get(type);
            if (p != null) {
                System.out.println("  parser:    " + p.getClass().getName());
            }
        }
    }
    
    private static Writer getOutputWriter(final OutputStream output, final String encoding) throws UnsupportedEncodingException {
        if (encoding != null) {
            return new OutputStreamWriter(output, encoding);
        }
        if (System.getProperty("os.name").toLowerCase().startsWith("mac os x")) {
            return new OutputStreamWriter(output, "UTF-8");
        }
        return new OutputStreamWriter(output);
    }
    
    private static TransformerHandler getTransformerHandler(final OutputStream output, final String method, final String encoding, final boolean prettyPrint) throws TransformerConfigurationException {
        final SAXTransformerFactory factory = (SAXTransformerFactory)TransformerFactory.newInstance();
        final TransformerHandler handler = factory.newTransformerHandler();
        handler.getTransformer().setOutputProperty("method", method);
        handler.getTransformer().setOutputProperty("indent", prettyPrint ? "yes" : "no");
        if (encoding != null) {
            handler.getTransformer().setOutputProperty("encoding", encoding);
        }
        handler.setResult(new StreamResult(output));
        return handler;
    }
    
    private class OutputType
    {
        public void process(final InputStream input, final OutputStream output, final Metadata metadata) throws Exception {
            Parser p = TikaCLI.this.parser;
            if (TikaCLI.this.fork) {
                p = new ForkParser(TikaCLI.class.getClassLoader(), p);
            }
            final ContentHandler handler = this.getContentHandler(output, metadata);
            p.parse(input, handler, metadata, TikaCLI.this.context);
            if (handler instanceof NoDocumentMetHandler) {
                final NoDocumentMetHandler metHandler = (NoDocumentMetHandler)handler;
                if (!metHandler.metOutput()) {
                    metHandler.endDocument();
                }
            }
        }
        
        protected ContentHandler getContentHandler(final OutputStream output, final Metadata metadata) throws Exception {
            throw new UnsupportedOperationException();
        }
    }
    
    private class FileEmbeddedDocumentExtractor implements EmbeddedDocumentExtractor
    {
        private int count;
        private final TikaConfig config;
        
        private FileEmbeddedDocumentExtractor() {
            this.count = 0;
            this.config = TikaConfig.getDefaultConfig();
        }
        
        public boolean shouldParseEmbedded(final Metadata metadata) {
            return true;
        }
        
        public void parseEmbedded(final InputStream inputStream, final ContentHandler contentHandler, final Metadata metadata, final boolean outputHtml) throws SAXException, IOException {
            String name = metadata.get("resourceName");
            if (name == null) {
                name = "file" + this.count++;
            }
            final MediaType contentType = TikaCLI.this.detector.detect(inputStream, metadata);
            if (name.indexOf(46) == -1 && contentType != null) {
                try {
                    name += this.config.getMimeRepository().forName(contentType.toString()).getExtension();
                }
                catch (MimeTypeException e) {
                    e.printStackTrace();
                }
            }
            final File outputFile = new File(TikaCLI.this.extractDir, name);
            if (outputFile.exists()) {
                System.err.println("File '" + name + "' already exists; skipping");
                return;
            }
            System.out.println("Extracting '" + name + "' (" + contentType + ")");
            final FileOutputStream os = new FileOutputStream(outputFile);
            if (inputStream instanceof TikaInputStream) {
                final TikaInputStream tin = (TikaInputStream)inputStream;
                if (tin.getOpenContainer() != null && tin.getOpenContainer() instanceof DirectoryEntry) {
                    final POIFSFileSystem fs = new POIFSFileSystem();
                    this.copy((DirectoryEntry)tin.getOpenContainer(), fs.getRoot());
                    fs.writeFilesystem(os);
                }
                else {
                    IOUtils.copy(inputStream, os);
                }
            }
            else {
                IOUtils.copy(inputStream, os);
            }
            os.close();
        }
        
        protected void copy(final DirectoryEntry sourceDir, final DirectoryEntry destDir) throws IOException {
            for (final Entry entry : sourceDir) {
                if (entry instanceof DirectoryEntry) {
                    final DirectoryEntry newDir = destDir.createDirectory(entry.getName());
                    this.copy((DirectoryEntry)entry, newDir);
                }
                else {
                    final InputStream contents = new DocumentInputStream((DocumentEntry)entry);
                    try {
                        destDir.createDocument(entry.getName(), contents);
                    }
                    finally {
                        contents.close();
                    }
                }
            }
        }
    }
    
    private class TikaServer extends Thread
    {
        private final ServerSocket server;
        
        public TikaServer(final int port) throws IOException {
            super("Tika server at port " + port);
            this.server = new ServerSocket(port);
        }
        
        @Override
        public void run() {
            try {
                try {
                    while (true) {
                        this.processSocketInBackground(this.server.accept());
                    }
                }
                finally {
                    this.server.close();
                }
            }
            catch (IOException ex) {}
        }
        
        private void processSocketInBackground(final Socket socket) {
            final Thread thread = new Thread() {
                @Override
                public void run() {
                    try {
                        try {
                            final InputStream input = socket.getInputStream();
                            final OutputStream output = socket.getOutputStream();
                            TikaCLI.this.type.process(input, output, new Metadata());
                            output.flush();
                        }
                        finally {
                            socket.close();
                        }
                    }
                    catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            };
            thread.setDaemon(true);
            thread.start();
        }
    }
    
    private class NoDocumentMetHandler extends DefaultHandler
    {
        protected final Metadata metadata;
        protected PrintWriter writer;
        private boolean metOutput;
        
        public NoDocumentMetHandler(final Metadata metadata, final PrintWriter writer) {
            this.metadata = metadata;
            this.writer = writer;
            this.metOutput = false;
        }
        
        @Override
        public void endDocument() {
            final String[] names = this.metadata.names();
            Arrays.sort(names);
            this.outputMetadata(names);
            this.writer.flush();
            this.metOutput = true;
        }
        
        public void outputMetadata(final String[] names) {
            for (final String name : names) {
                this.writer.println(name + ": " + this.metadata.get(name));
            }
        }
        
        public boolean metOutput() {
            return this.metOutput;
        }
    }
    
    private class NoDocumentXMPMetaHandler extends DefaultHandler
    {
        protected final Metadata metadata;
        protected PrintWriter writer;
        
        public NoDocumentXMPMetaHandler(final Metadata metadata, final PrintWriter writer) {
            this.metadata = metadata;
            this.writer = writer;
        }
        
        @Override
        public void endDocument() throws SAXException {
            try {
                final XMPMetadata xmp = new XMPMetadata(this.metadata);
                final String result = xmp.toString();
                this.writer.write(result);
                this.writer.flush();
            }
            catch (TikaException e) {
                throw new SAXException(e);
            }
        }
    }
    
    private class NoDocumentJSONMetHandler extends NoDocumentMetHandler
    {
        private NumberFormat formatter;
        private Gson gson;
        
        public NoDocumentJSONMetHandler(final Metadata metadata, final PrintWriter writer) {
            super(metadata, writer);
            this.formatter = NumberFormat.getInstance();
            this.gson = new Gson();
        }
        
        @Override
        public void outputMetadata(final String[] names) {
            this.writer.print("{ ");
            boolean first = true;
            for (final String name : names) {
                if (!first) {
                    this.writer.println(", ");
                }
                else {
                    first = false;
                }
                this.gson.toJson(name, this.writer);
                this.writer.print(":");
                this.outputValues(this.metadata.getValues(name));
            }
            this.writer.print(" }");
        }
        
        public void outputValues(final String[] values) {
            if (values.length > 1) {
                this.writer.print("[");
            }
            for (int i = 0; i < values.length; ++i) {
                String value = values[i];
                if (i > 0) {
                    this.writer.print(", ");
                }
                if (value == null || value.length() == 0) {
                    this.writer.print("null");
                }
                else {
                    final ParsePosition pos = new ParsePosition(0);
                    this.formatter.parse(value, pos);
                    if (value.length() == pos.getIndex()) {
                        value = value.replaceFirst("^0+(\\d)", "$1");
                        this.writer.print(value);
                    }
                    else {
                        this.gson.toJson(value, this.writer);
                    }
                }
            }
            if (values.length > 1) {
                this.writer.print("]");
            }
        }
    }
}
