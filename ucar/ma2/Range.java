// 
// Decompiled by Procyon v0.5.36
// 

package ucar.ma2;

import java.util.StringTokenizer;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.List;

public final class Range
{
    public static final Range EMPTY;
    public static final Range VLEN;
    private final int n;
    private final int first;
    private final int stride;
    private final String name;
    private int hashCode;
    
    private Range() {
        this.hashCode = 0;
        this.n = 0;
        this.first = 0;
        this.stride = 1;
        this.name = null;
    }
    
    public Range(final int first, final int last) throws InvalidRangeException {
        this(null, first, last, 1);
    }
    
    public Range(final int length) {
        this.hashCode = 0;
        this.name = null;
        this.first = 0;
        this.stride = 1;
        this.n = length;
    }
    
    public Range(final String name, final int first, final int last) throws InvalidRangeException {
        this(name, first, last, 1);
    }
    
    public Range(final int first, final int last, final int stride) throws InvalidRangeException {
        this(null, first, last, stride);
    }
    
    public Range(final String name, final int first, final int last, final int stride) throws InvalidRangeException {
        this.hashCode = 0;
        if (first < 0) {
            throw new InvalidRangeException("first (" + first + ") must be >= 0");
        }
        if (last < first) {
            throw new InvalidRangeException("last (" + last + ") must be >= first (" + first + ")");
        }
        if (stride < 1) {
            throw new InvalidRangeException("stride (" + stride + ") must be > 0");
        }
        this.name = name;
        this.first = first;
        this.stride = stride;
        this.n = Math.max(1 + (last - first) / stride, 1);
    }
    
    public Range(final Range r) {
        this.hashCode = 0;
        this.first = r.first();
        this.n = r.length();
        this.stride = r.stride();
        this.name = r.getName();
    }
    
    public Range(final String name, final Range r) {
        this.hashCode = 0;
        this.name = name;
        this.first = r.first();
        this.n = r.length();
        this.stride = r.stride();
    }
    
    public Range compose(final Range r) throws InvalidRangeException {
        if (this.length() == 0 || r.length() == 0) {
            return Range.EMPTY;
        }
        if (this == Range.VLEN || r == Range.VLEN) {
            return Range.VLEN;
        }
        final int first = this.element(r.first());
        final int stride = this.stride() * r.stride();
        final int last = this.element(r.last());
        return new Range(this.name, first, last, stride);
    }
    
    public Range compact() throws InvalidRangeException {
        if (this.stride() == 1) {
            return this;
        }
        final int first = this.first() / this.stride();
        final int last = first + this.length() - 1;
        return new Range(this.name, first, last, 1);
    }
    
    public Range shiftOrigin(final int origin) throws InvalidRangeException {
        if (this == Range.VLEN) {
            return Range.VLEN;
        }
        final int first = this.first() - origin;
        final int stride = this.stride();
        final int last = this.last() - origin;
        return new Range(this.name, first, last, stride);
    }
    
    public Range intersect(final Range r) throws InvalidRangeException {
        if (this.length() == 0 || r.length() == 0) {
            return Range.EMPTY;
        }
        if (this == Range.VLEN || r == Range.VLEN) {
            return Range.VLEN;
        }
        final int last = Math.min(this.last(), r.last());
        final int stride = this.stride() * r.stride();
        int useFirst;
        if (stride == 1) {
            useFirst = Math.max(this.first(), r.first());
        }
        else if (this.stride() == 1) {
            if (r.first() >= this.first()) {
                useFirst = r.first();
            }
            else {
                final int incr = (this.first() - r.first()) / stride;
                useFirst = r.first() + incr * stride;
                if (useFirst < this.first()) {
                    useFirst += stride;
                }
            }
        }
        else {
            if (r.stride() != 1) {
                throw new UnsupportedOperationException("Intersection when both ranges have a stride");
            }
            if (this.first() >= r.first()) {
                useFirst = this.first();
            }
            else {
                final int incr = (r.first() - this.first()) / stride;
                useFirst = this.first() + incr * stride;
                if (useFirst < r.first()) {
                    useFirst += stride;
                }
            }
        }
        if (useFirst > last) {
            return Range.EMPTY;
        }
        return new Range(this.name, useFirst, last, stride);
    }
    
    public boolean intersects(final Range r) {
        if (this.length() == 0 || r.length() == 0) {
            return false;
        }
        if (this == Range.VLEN || r == Range.VLEN) {
            return true;
        }
        final int last = Math.min(this.last(), r.last());
        final int stride = this.stride() * r.stride();
        int useFirst;
        if (stride == 1) {
            useFirst = Math.max(this.first(), r.first());
        }
        else if (this.stride() == 1) {
            if (r.first() >= this.first()) {
                useFirst = r.first();
            }
            else {
                final int incr = (this.first() - r.first()) / stride;
                useFirst = r.first() + incr * stride;
                if (useFirst < this.first()) {
                    useFirst += stride;
                }
            }
        }
        else {
            if (r.stride() != 1) {
                throw new UnsupportedOperationException("Intersection when both ranges have a stride");
            }
            if (this.first() >= r.first()) {
                useFirst = this.first();
            }
            else {
                final int incr = (r.first() - this.first()) / stride;
                useFirst = this.first() + incr * stride;
                if (useFirst < r.first()) {
                    useFirst += stride;
                }
            }
        }
        return useFirst <= last;
    }
    
    public boolean past(final Range want) {
        return this.first() > want.last();
    }
    
    public Range union(final Range r) throws InvalidRangeException {
        if (this.length() == 0) {
            return r;
        }
        if (this == Range.VLEN || r == Range.VLEN) {
            return Range.VLEN;
        }
        if (r.length() == 0) {
            return this;
        }
        final int first = Math.min(this.first(), r.first());
        final int last = Math.max(this.last(), r.last());
        return new Range(this.name, first, last);
    }
    
    public int length() {
        return this.n;
    }
    
    public int element(final int i) throws InvalidRangeException {
        if (i < 0) {
            throw new InvalidRangeException("i must be >= 0");
        }
        if (i >= this.n) {
            throw new InvalidRangeException("i must be < length");
        }
        return this.first + i * this.stride;
    }
    
    public int index(final int elem) throws InvalidRangeException {
        if (elem < this.first) {
            throw new InvalidRangeException("elem must be >= first");
        }
        final int result = (elem - this.first) / this.stride;
        if (result > this.n) {
            throw new InvalidRangeException("elem must be <= first = n * stride");
        }
        return result;
    }
    
    public boolean contains(final int i) {
        return i >= this.first() && i <= this.last() && (this.stride == 1 || (i - this.first) % this.stride == 0);
    }
    
    protected int elementNC(final int i) {
        return this.first + i * this.stride;
    }
    
    public int first() {
        return this.first;
    }
    
    public int last() {
        return this.first + (this.n - 1) * this.stride;
    }
    
    public int stride() {
        return this.stride;
    }
    
    public String getName() {
        return this.name;
    }
    
    public Iterator getIterator() {
        return new Iterator();
    }
    
    public int getFirstInInterval(final int start) {
        if (start > this.last()) {
            return -1;
        }
        if (start <= this.first) {
            return this.first;
        }
        if (this.stride == 1) {
            return start;
        }
        final int offset = start - this.first;
        final int incr = offset % this.stride;
        final int result = start + incr;
        return (result > this.last()) ? -1 : result;
    }
    
    @Override
    public String toString() {
        return this.first + ":" + this.last() + ((this.stride > 1) ? (":" + this.stride) : "");
    }
    
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Range)) {
            return false;
        }
        final Range or = (Range)o;
        return (this.n == 0 && or.n == 0) || (or.first == this.first && or.n == this.n && or.stride == this.stride);
    }
    
    @Override
    public int hashCode() {
        if (this.hashCode == 0) {
            int result = this.first();
            result = 37 * result + this.last();
            result = 37 * result + this.stride();
            this.hashCode = result;
        }
        return this.hashCode;
    }
    
    @Deprecated
    public int min() {
        if (this.n <= 0) {
            return this.first;
        }
        if (this.stride > 0) {
            return this.first;
        }
        return this.first + (this.n - 1) * this.stride;
    }
    
    @Deprecated
    public int max() {
        if (this.n > 0) {
            if (this.stride > 0) {
                return this.first + (this.n - 1) * this.stride;
            }
            return this.first;
        }
        else {
            if (this.stride > 0) {
                return this.first - 1;
            }
            return this.first + 1;
        }
    }
    
    @Deprecated
    public static List factory(final int[] shape) {
        final ArrayList result = new ArrayList();
        for (int i = 0; i < shape.length; ++i) {
            try {
                result.add(new Range(0, Math.max(shape[i] - 1, -1)));
            }
            catch (InvalidRangeException e) {
                return null;
            }
        }
        return result;
    }
    
    @Deprecated
    public static List setDefaults(List rangeList, final int[] shape) {
        try {
            if (rangeList == null) {
                rangeList = new ArrayList<Range>();
                for (int i = 0; i < shape.length; ++i) {
                    rangeList.add(new Range(0, shape[i]));
                }
                return rangeList;
            }
            for (int i = 0; i < shape.length; ++i) {
                final Range r = (Range)rangeList.get(i);
                if (r == null) {
                    rangeList.set(i, new Range(0, shape[i] - 1));
                }
            }
            return rangeList;
        }
        catch (InvalidRangeException ex) {
            return null;
        }
    }
    
    @Deprecated
    public static List factory(final int[] origin, final int[] shape) throws InvalidRangeException {
        final ArrayList result = new ArrayList();
        for (int i = 0; i < shape.length; ++i) {
            try {
                result.add(new Range(origin[i], origin[i] + shape[i] - 1));
            }
            catch (Exception e) {
                throw new InvalidRangeException(e.getMessage());
            }
        }
        return result;
    }
    
    @Deprecated
    public static int[] getShape(final List ranges) {
        if (ranges == null) {
            return null;
        }
        final int[] result = new int[ranges.size()];
        for (int i = 0; i < ranges.size(); ++i) {
            result[i] = ranges.get(i).length();
        }
        return result;
    }
    
    @Deprecated
    public static String toString(final List ranges) {
        if (ranges == null) {
            return "";
        }
        final StringBuilder sbuff = new StringBuilder();
        for (int i = 0; i < ranges.size(); ++i) {
            if (i > 0) {
                sbuff.append(",");
            }
            sbuff.append(ranges.get(i).length());
        }
        return sbuff.toString();
    }
    
    @Deprecated
    public static long computeSize(final List section) {
        final int[] shape = getShape(section);
        return Index.computeSize(shape);
    }
    
    @Deprecated
    public static List appendShape(final List ranges, final int size) throws InvalidRangeException {
        ranges.add(new Range(0, size - 1));
        return ranges;
    }
    
    @Deprecated
    public static int[] getOrigin(final List ranges) {
        if (ranges == null) {
            return null;
        }
        final int[] result = new int[ranges.size()];
        for (int i = 0; i < ranges.size(); ++i) {
            result[i] = ranges.get(i).first();
        }
        return result;
    }
    
    @Deprecated
    public static Range[] toArray(final List ranges) {
        if (ranges == null) {
            return null;
        }
        return ranges.toArray(new Range[ranges.size()]);
    }
    
    @Deprecated
    public static List toList(final Range[] ranges) {
        if (ranges == null) {
            return null;
        }
        return Arrays.asList(ranges);
    }
    
    @Deprecated
    public static String makeSectionSpec(final List ranges) {
        final StringBuilder sbuff = new StringBuilder();
        for (int i = 0; i < ranges.size(); ++i) {
            final Range r = ranges.get(i);
            if (i > 0) {
                sbuff.append(",");
            }
            sbuff.append(r.toString());
        }
        return sbuff.toString();
    }
    
    @Deprecated
    public static List parseSpec(final String sectionSpec) throws InvalidRangeException {
        final ArrayList result = new ArrayList();
        final StringTokenizer stoke = new StringTokenizer(sectionSpec, "(),");
        while (stoke.hasMoreTokens()) {
            final String s = stoke.nextToken().trim();
            Range section = null;
            Label_0263: {
                if (s.equals(":")) {
                    section = null;
                }
                else {
                    if (s.indexOf(58) < 0) {
                        try {
                            final int index = Integer.parseInt(s);
                            section = new Range(index, index);
                            break Label_0263;
                        }
                        catch (NumberFormatException e) {
                            throw new IllegalArgumentException(" illegal selector: " + s + " part of <" + sectionSpec + ">");
                        }
                    }
                    final StringTokenizer stoke2 = new StringTokenizer(s, ":");
                    final String s2 = stoke2.nextToken();
                    final String s3 = stoke2.nextToken();
                    final String s4 = stoke2.hasMoreTokens() ? stoke2.nextToken() : null;
                    try {
                        final int index2 = Integer.parseInt(s2);
                        final int index3 = Integer.parseInt(s3);
                        final int stride = (s4 != null) ? Integer.parseInt(s4) : 1;
                        section = new Range(index2, index3, stride);
                    }
                    catch (NumberFormatException e2) {
                        throw new IllegalArgumentException(" illegal selector: " + s + " part of <" + sectionSpec + ">");
                    }
                }
            }
            result.add(section);
        }
        return result;
    }
    
    @Deprecated
    public static String checkInRange(final List section, final int[] shape) {
        if (section.size() != shape.length) {
            return "Number of ranges in section must be =" + shape.length;
        }
        for (int i = 0; i < section.size(); ++i) {
            final Range r = section.get(i);
            if (r != null) {
                if (r.last() >= shape[i]) {
                    return "Illegal range for dimension " + i + ": requested " + r.last() + " >= max " + shape[i];
                }
            }
        }
        return null;
    }
    
    static {
        EMPTY = new Range();
        VLEN = new Range(-1);
    }
    
    public class Iterator
    {
        private int current;
        
        public Iterator() {
            this.current = 0;
        }
        
        public boolean hasNext() {
            return this.current < Range.this.n;
        }
        
        public int next() {
            return Range.this.elementNC(this.current++);
        }
    }
}
