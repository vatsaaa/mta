// 
// Decompiled by Procyon v0.5.36
// 

package ucar.ma2;

import java.util.Iterator;
import java.util.ArrayList;
import java.util.List;

public class ArrayStructureComposite extends ArrayStructure
{
    private List<ArrayStructure> compose;
    private int[] start;
    
    public ArrayStructureComposite(final StructureMembers members, final List<ArrayStructure> c, final int total) {
        super(members, new int[total]);
        this.compose = new ArrayList<ArrayStructure>();
        this.compose = c;
        this.start = new int[total];
        int count = 0;
        int i = 0;
        for (final ArrayStructure as : this.compose) {
            this.start[i++] = count;
            count += (int)as.getSize();
        }
    }
    
    @Override
    protected StructureData makeStructureData(final ArrayStructure me, final int recno) {
        for (int i = 0; i < this.start.length; ++i) {
            if (recno >= this.start[i]) {
                final ArrayStructure as = this.compose.get(i);
                return as.makeStructureData(as, recno - this.start[i]);
            }
        }
        throw new IllegalArgumentException();
    }
}
