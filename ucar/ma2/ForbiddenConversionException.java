// 
// Decompiled by Procyon v0.5.36
// 

package ucar.ma2;

public final class ForbiddenConversionException extends RuntimeException
{
    public ForbiddenConversionException() {
    }
    
    public ForbiddenConversionException(final String s) {
        super(s);
    }
}
