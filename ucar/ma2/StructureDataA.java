// 
// Decompiled by Procyon v0.5.36
// 

package ucar.ma2;

import java.util.Formatter;

public class StructureDataA extends StructureData
{
    protected ArrayStructure sa;
    protected int recno;
    
    public StructureDataA(final ArrayStructure sa, final int recno) {
        super(sa.getStructureMembers());
        this.sa = sa;
        this.recno = recno;
    }
    
    @Override
    public Array getArray(final StructureMembers.Member m) {
        return this.sa.getArray(this.recno, m);
    }
    
    @Override
    public int convertScalarInt(final StructureMembers.Member m) {
        return this.sa.convertScalarInt(this.recno, m);
    }
    
    @Override
    public float convertScalarFloat(final StructureMembers.Member m) {
        return this.sa.convertScalarFloat(this.recno, m);
    }
    
    @Override
    public double convertScalarDouble(final StructureMembers.Member m) {
        return this.sa.convertScalarDouble(this.recno, m);
    }
    
    @Override
    public double getScalarDouble(final StructureMembers.Member m) {
        return this.sa.getScalarDouble(this.recno, m);
    }
    
    @Override
    public double[] getJavaArrayDouble(final StructureMembers.Member m) {
        return this.sa.getJavaArrayDouble(this.recno, m);
    }
    
    @Override
    public float getScalarFloat(final StructureMembers.Member m) {
        return this.sa.getScalarFloat(this.recno, m);
    }
    
    @Override
    public float[] getJavaArrayFloat(final StructureMembers.Member m) {
        return this.sa.getJavaArrayFloat(this.recno, m);
    }
    
    @Override
    public byte getScalarByte(final StructureMembers.Member m) {
        return this.sa.getScalarByte(this.recno, m);
    }
    
    @Override
    public byte[] getJavaArrayByte(final StructureMembers.Member m) {
        return this.sa.getJavaArrayByte(this.recno, m);
    }
    
    @Override
    public short getScalarShort(final StructureMembers.Member m) {
        return this.sa.getScalarShort(this.recno, m);
    }
    
    @Override
    public short[] getJavaArrayShort(final StructureMembers.Member m) {
        return this.sa.getJavaArrayShort(this.recno, m);
    }
    
    @Override
    public int getScalarInt(final StructureMembers.Member m) {
        return this.sa.getScalarInt(this.recno, m);
    }
    
    @Override
    public int[] getJavaArrayInt(final StructureMembers.Member m) {
        return this.sa.getJavaArrayInt(this.recno, m);
    }
    
    @Override
    public long getScalarLong(final StructureMembers.Member m) {
        return this.sa.getScalarLong(this.recno, m);
    }
    
    @Override
    public long[] getJavaArrayLong(final StructureMembers.Member m) {
        return this.sa.getJavaArrayLong(this.recno, m);
    }
    
    @Override
    public char getScalarChar(final StructureMembers.Member m) {
        return this.sa.getScalarChar(this.recno, m);
    }
    
    @Override
    public char[] getJavaArrayChar(final StructureMembers.Member m) {
        return this.sa.getJavaArrayChar(this.recno, m);
    }
    
    @Override
    public String getScalarString(final StructureMembers.Member m) {
        return this.sa.getScalarString(this.recno, m);
    }
    
    @Override
    public String[] getJavaArrayString(final StructureMembers.Member m) {
        if (m.getDataType() == DataType.STRING) {
            final Array data = this.getArray(m);
            final int n = m.getSize();
            final String[] result = new String[n];
            for (int i = 0; i < result.length; ++i) {
                result[i] = (String)data.getObject(i);
            }
            return result;
        }
        if (m.getDataType() == DataType.CHAR) {
            final ArrayChar data2 = (ArrayChar)this.getArray(m);
            final ArrayChar.StringIterator iter = data2.getStringIterator();
            final String[] result = new String[iter.getNumElems()];
            int count = 0;
            while (iter.hasNext()) {
                result[count++] = iter.next();
            }
            return result;
        }
        throw new IllegalArgumentException("getJavaArrayString: not String DataType :" + m.getDataType());
    }
    
    @Override
    public StructureData getScalarStructure(final StructureMembers.Member m) {
        return this.sa.getScalarStructure(this.recno, m);
    }
    
    @Override
    public ArrayStructure getArrayStructure(final StructureMembers.Member m) {
        return this.sa.getArrayStructure(this.recno, m);
    }
    
    @Override
    public ArraySequence getArraySequence(final StructureMembers.Member m) {
        return this.sa.getArraySequence(this.recno, m);
    }
    
    @Override
    public void showInternal(final Formatter f, final String leadingSpace) {
        super.showInternal(f, leadingSpace);
        this.sa.showInternal(f, leadingSpace);
    }
}
