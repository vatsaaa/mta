// 
// Decompiled by Procyon v0.5.36
// 

package ucar.ma2;

import java.util.Collections;
import java.util.Iterator;
import java.util.StringTokenizer;
import java.util.Collection;
import java.util.ArrayList;
import java.util.List;

public class Section
{
    private List<Range> list;
    private boolean immutable;
    
    public Section(final int[] shape) {
        this.immutable = false;
        this.list = new ArrayList<Range>();
        for (int i = 0; i < shape.length; ++i) {
            if (shape[i] > 0) {
                this.list.add(new Range(shape[i]));
            }
            else if (shape[i] == 0) {
                this.list.add(Range.EMPTY);
            }
            else {
                this.list.add(Range.VLEN);
            }
        }
    }
    
    public Section(final int[] origin, final int[] shape) throws InvalidRangeException {
        this.immutable = false;
        this.list = new ArrayList<Range>();
        for (int i = 0; i < shape.length; ++i) {
            this.list.add((shape[i] > 0) ? new Range(origin[i], origin[i] + shape[i] - 1) : Range.EMPTY);
        }
    }
    
    public Section(final int[] origin, final int[] size, final int[] stride) throws InvalidRangeException {
        this.immutable = false;
        this.list = new ArrayList<Range>();
        for (int i = 0; i < size.length; ++i) {
            this.list.add((size[i] > 0) ? new Range(origin[i], origin[i] + size[i] - 1, stride[i]) : Range.EMPTY);
        }
    }
    
    public Section(final List<Range> from) {
        this.immutable = false;
        this.list = new ArrayList<Range>(from);
    }
    
    public Section(final Section from) {
        this.immutable = false;
        this.list = new ArrayList<Range>(from.getRanges());
    }
    
    public Section(final List<Range> from, final int[] shape) throws InvalidRangeException {
        this.immutable = false;
        this.list = new ArrayList<Range>(from);
        this.setDefaults(shape);
    }
    
    public static Section fill(final Section s, final int[] shape) throws InvalidRangeException {
        if (s == null) {
            return new Section(shape);
        }
        final String errs = s.checkInRange(shape);
        if (errs != null) {
            throw new InvalidRangeException(errs);
        }
        boolean ok = true;
        for (int i = 0; i < shape.length; ++i) {
            ok &= (s.getRange(i) != null);
        }
        if (ok) {
            return s;
        }
        return new Section(s.getRanges(), shape);
    }
    
    public Section(final String sectionSpec) throws InvalidRangeException {
        this.immutable = false;
        this.list = new ArrayList<Range>();
        final StringTokenizer stoke = new StringTokenizer(sectionSpec, "(),");
        while (stoke.hasMoreTokens()) {
            final String s = stoke.nextToken().trim();
            Range range = null;
            Label_0275: {
                if (s.equals(":")) {
                    range = null;
                }
                else {
                    if (s.indexOf(58) < 0) {
                        try {
                            final int index = Integer.parseInt(s);
                            range = new Range(index, index);
                            break Label_0275;
                        }
                        catch (NumberFormatException e) {
                            throw new IllegalArgumentException(" illegal selector: " + s + " part of <" + sectionSpec + ">");
                        }
                    }
                    final StringTokenizer stoke2 = new StringTokenizer(s, ":");
                    final String s2 = stoke2.nextToken();
                    final String s3 = stoke2.nextToken();
                    final String s4 = stoke2.hasMoreTokens() ? stoke2.nextToken() : null;
                    try {
                        final int index2 = Integer.parseInt(s2);
                        final int index3 = Integer.parseInt(s3);
                        final int stride = (s4 != null) ? Integer.parseInt(s4) : 1;
                        range = new Range(index2, index3, stride);
                    }
                    catch (NumberFormatException e2) {
                        throw new IllegalArgumentException(" illegal selector: " + s + " part of <" + sectionSpec + ">");
                    }
                }
            }
            this.list.add(range);
        }
    }
    
    public Section compact() throws InvalidRangeException {
        final List<Range> results = new ArrayList<Range>(this.getRank());
        for (final Range r : this.list) {
            results.add(r.compact());
        }
        return new Section(results);
    }
    
    public Section removeVlen() throws InvalidRangeException {
        boolean need = false;
        for (final Range r : this.list) {
            if (r == Range.VLEN) {
                need = true;
            }
        }
        if (!need) {
            return this;
        }
        final List<Range> results = new ArrayList<Range>(this.getRank());
        for (final Range r2 : this.list) {
            if (r2 != Range.VLEN) {
                results.add(r2);
            }
        }
        return new Section(results);
    }
    
    public Section compose(final Section want) throws InvalidRangeException {
        if (want == null) {
            return this;
        }
        if (want.getRank() != this.getRank()) {
            throw new InvalidRangeException("Invalid Section rank");
        }
        final List<Range> results = new ArrayList<Range>(this.getRank());
        for (int j = 0; j < this.list.size(); ++j) {
            final Range base = this.list.get(j);
            final Range r = want.getRange(j);
            if (r == null) {
                results.add(base);
            }
            else {
                results.add(base.compose(r));
            }
        }
        return new Section(results);
    }
    
    public Section intersect(final Section other) throws InvalidRangeException {
        if (other.getRank() != this.getRank()) {
            throw new InvalidRangeException("Invalid Section rank");
        }
        final List<Range> results = new ArrayList<Range>(this.getRank());
        for (int j = 0; j < this.list.size(); ++j) {
            final Range base = this.list.get(j);
            final Range r = other.getRange(j);
            results.add(base.intersect(r));
        }
        return new Section(results);
    }
    
    public int offset(final Section intersect) throws InvalidRangeException {
        if (intersect.getRank() != this.getRank()) {
            throw new InvalidRangeException("Invalid Section rank");
        }
        int result = 0;
        int stride = 1;
        for (int j = this.list.size() - 1; j >= 0; --j) {
            final Range base = this.list.get(j);
            final Range r = intersect.getRange(j);
            final int offset = base.index(r.first());
            result += offset * stride;
            stride *= base.length();
        }
        return result;
    }
    
    public Section union(final Section other) throws InvalidRangeException {
        if (other.getRank() != this.getRank()) {
            throw new InvalidRangeException("Invalid Section rank");
        }
        final List<Range> results = new ArrayList<Range>(this.getRank());
        for (int j = 0; j < this.list.size(); ++j) {
            final Range base = this.list.get(j);
            final Range r = other.getRange(j);
            results.add(base.union(r));
        }
        return new Section(results);
    }
    
    public Section shiftOrigin(final Section newOrigin) throws InvalidRangeException {
        if (newOrigin.getRank() != this.getRank()) {
            throw new InvalidRangeException("Invalid Section rank");
        }
        final List<Range> results = new ArrayList<Range>(this.getRank());
        for (int j = 0; j < this.list.size(); ++j) {
            final Range base = this.list.get(j);
            final Range r = newOrigin.getRange(j);
            results.add(base.shiftOrigin(r.first()));
        }
        return new Section(results);
    }
    
    public boolean intersects(final Section other) throws InvalidRangeException {
        if (other.getRank() != this.getRank()) {
            throw new InvalidRangeException("Invalid Section rank");
        }
        for (int j = 0; j < this.list.size(); ++j) {
            final Range base = this.list.get(j);
            final Range r = other.getRange(j);
            if (!base.intersects(r)) {
                return false;
            }
        }
        return true;
    }
    
    public boolean contains(final Section other) {
        if (other.getRank() != this.getRank()) {
            return false;
        }
        for (int j = 0; j < this.list.size(); ++j) {
            final Range base = this.list.get(j);
            final Range r = other.getRange(j);
            if (base.first() > r.first()) {
                return false;
            }
            if (base.last() < r.last()) {
                return false;
            }
        }
        return true;
    }
    
    @Override
    public String toString() {
        final StringBuilder sbuff = new StringBuilder();
        for (int i = 0; i < this.list.size(); ++i) {
            final Range r = this.list.get(i);
            if (i > 0) {
                sbuff.append(",");
            }
            if (r == null) {
                sbuff.append(":");
            }
            else {
                sbuff.append(r.toString());
            }
        }
        return sbuff.toString();
    }
    
    public Section() {
        this.immutable = false;
        this.list = new ArrayList<Range>();
    }
    
    public Section appendRange() {
        if (this.immutable) {
            throw new IllegalStateException("Cant modify");
        }
        this.list.add(null);
        return this;
    }
    
    public Section appendRange(final Range r) {
        if (this.immutable) {
            throw new IllegalStateException("Cant modify");
        }
        this.list.add(r);
        return this;
    }
    
    public Section appendRange(final int size) throws InvalidRangeException {
        if (this.immutable) {
            throw new IllegalStateException("Cant modify");
        }
        if (size > 0) {
            this.list.add(new Range(size));
        }
        else if (size == 0) {
            this.list.add(Range.EMPTY);
        }
        else {
            this.list.add(Range.VLEN);
        }
        return this;
    }
    
    public Section appendRange(final int first, final int last) throws InvalidRangeException {
        if (this.immutable) {
            throw new IllegalStateException("Cant modify");
        }
        this.list.add(new Range(first, last));
        return this;
    }
    
    public Section appendRange(final int first, final int last, final int stride) throws InvalidRangeException {
        if (this.immutable) {
            throw new IllegalStateException("Cant modify");
        }
        this.list.add(new Range(first, last, stride));
        return this;
    }
    
    public Section appendRange(final String name, final int first, final int last, final int stride) throws InvalidRangeException {
        if (this.immutable) {
            throw new IllegalStateException("Cant modify");
        }
        this.list.add(new Range(name, first, last, stride));
        return this;
    }
    
    public Section insertRange(final int index, final Range r) {
        if (this.immutable) {
            throw new IllegalStateException("Cant modify");
        }
        this.list.add(index, r);
        return this;
    }
    
    public Section removeRange(final int index) {
        if (this.immutable) {
            throw new IllegalStateException("Cant modify");
        }
        this.list.remove(index);
        return this;
    }
    
    public Section setRange(final int index, final Range r) {
        if (this.immutable) {
            throw new IllegalStateException("Cant modify");
        }
        this.list.set(index, r);
        return this;
    }
    
    public Section replaceRange(final int index, final Range r) {
        if (this.immutable) {
            throw new IllegalStateException("Cant modify");
        }
        this.list.set(index, r);
        return this;
    }
    
    public Section reduce() {
        if (this.immutable) {
            throw new IllegalStateException("Cant modify");
        }
        boolean needed = false;
        for (final Range r : this.list) {
            if (r.length() == 1) {
                needed = true;
            }
        }
        if (!needed) {
            return this;
        }
        final List<Range> newList = new ArrayList<Range>(this.list.size());
        for (final Range r2 : this.list) {
            if (r2.length() > 1) {
                newList.add(r2);
            }
        }
        return new Section(newList);
    }
    
    public void setDefaults(final int[] shape) throws InvalidRangeException {
        if (this.immutable) {
            throw new IllegalStateException("Cant modify");
        }
        if (shape.length != this.list.size()) {
            throw new InvalidRangeException(" shape[] must have same rank as Section");
        }
        for (int i = 0; i < shape.length; ++i) {
            final Range r = this.list.get(i);
            if (r == null) {
                if (shape[i] > 0) {
                    this.list.set(i, new Range(shape[i]));
                }
                else if (shape[i] == 0) {
                    this.list.set(i, Range.EMPTY);
                }
                else {
                    this.list.set(i, Range.VLEN);
                }
            }
        }
    }
    
    public Section makeImmutable() {
        if (this.immutable) {
            return this;
        }
        this.immutable = true;
        this.list = Collections.unmodifiableList((List<? extends Range>)this.list);
        return this;
    }
    
    public boolean isImmutable() {
        return this.immutable;
    }
    
    public int[] getShape() {
        final int[] result = new int[this.list.size()];
        for (int i = 0; i < this.list.size(); ++i) {
            result[i] = this.list.get(i).length();
        }
        return result;
    }
    
    public int[] getOrigin() {
        final int[] result = new int[this.list.size()];
        for (int i = 0; i < this.list.size(); ++i) {
            result[i] = this.list.get(i).first();
        }
        return result;
    }
    
    public int[] getStride() {
        final int[] result = new int[this.list.size()];
        for (int i = 0; i < this.list.size(); ++i) {
            result[i] = this.list.get(i).stride();
        }
        return result;
    }
    
    public int getOrigin(final int i) {
        return this.list.get(i).first();
    }
    
    public int getShape(final int i) {
        return this.list.get(i).length();
    }
    
    public int getStride(final int i) {
        return this.list.get(i).stride();
    }
    
    public int getRank() {
        return this.list.size();
    }
    
    public long computeSize() {
        long product = 1L;
        for (int ii = this.list.size() - 1; ii >= 0; --ii) {
            final Range r = this.list.get(ii);
            if (r != Range.VLEN) {
                product *= r.length();
            }
        }
        return product;
    }
    
    public List<Range> getRanges() {
        return this.list;
    }
    
    public Range getRange(final int i) {
        return this.list.get(i);
    }
    
    public Range find(final String rangeName) {
        for (final Range r : this.list) {
            if (rangeName.equals(r.getName())) {
                return r;
            }
        }
        return null;
    }
    
    public Section addRangeNames(final List<String> rangeNames) throws InvalidRangeException {
        if (rangeNames.size() != this.getRank()) {
            throw new InvalidRangeException("Invalid number of Range Names");
        }
        int count = 0;
        final Section result = new Section();
        for (final Range r : this.getRanges()) {
            final Range nr = new Range(rangeNames.get(count++), r);
            result.appendRange(nr);
        }
        return result;
    }
    
    public String checkInRange(final int[] shape) {
        if (this.list.size() != shape.length) {
            return "Number of ranges in section (" + this.list.size() + ") must be = " + shape.length;
        }
        for (int i = 0; i < this.list.size(); ++i) {
            final Range r = this.list.get(i);
            if (r != null) {
                if (r != Range.VLEN) {
                    if (r.last() >= shape[i]) {
                        return "Illegal Range for dimension " + i + ": last requested " + r.last() + " > max " + (shape[i] - 1);
                    }
                }
            }
        }
        return null;
    }
    
    public boolean equivalent(final int[] shape) throws InvalidRangeException {
        if (this.getRank() != shape.length) {
            throw new InvalidRangeException("Invalid Section rank");
        }
        for (int i = 0; i < this.list.size(); ++i) {
            final Range r = this.list.get(i);
            if (r != null) {
                if (r.first() != 0) {
                    return false;
                }
                if (r.length() != shape[i]) {
                    return false;
                }
            }
        }
        return true;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Section)) {
            return false;
        }
        final Section os = (Section)o;
        if (this.getRank() != os.getRank()) {
            return false;
        }
        for (int i = 0; i < this.getRank(); ++i) {
            final Range r = this.getRange(i);
            final Range or = os.getRange(i);
            if (r == null && or != null) {
                return false;
            }
            if (or == null && r != null) {
                return false;
            }
            if (r != null) {
                if (!r.equals(or)) {
                    return false;
                }
            }
        }
        return true;
    }
    
    @Override
    public int hashCode() {
        int result = 17;
        for (final Range r : this.list) {
            if (r != null) {
                result += 37 * result + r.hashCode();
            }
        }
        return result;
    }
    
    public Iterator getIterator(final int[] shape) {
        return new Iterator(shape);
    }
    
    public class Iterator
    {
        private int[] odo;
        private int[] stride;
        private long done;
        private long total;
        
        Iterator(final int[] shape) {
            this.odo = new int[Section.this.getRank()];
            this.stride = new int[Section.this.getRank()];
            int ss = 1;
            for (int i = Section.this.getRank() - 1; i >= 0; --i) {
                this.odo[i] = Section.this.getRange(i).first();
                this.stride[i] = ss;
                ss *= shape[i];
            }
            this.done = 0L;
            this.total = Index.computeSize(Section.this.getShape());
        }
        
        public boolean hasNext() {
            return this.done < this.total;
        }
        
        public int next() {
            final int next = this.currentElement();
            ++this.done;
            if (this.done < this.total) {
                this.incr();
            }
            return next;
        }
        
        private void incr() {
            int digit = Section.this.getRank() - 1;
            while (digit >= 0) {
                final Range r = Section.this.getRange(digit);
                final int[] odo = this.odo;
                final int n = digit;
                odo[n] += r.stride();
                if (this.odo[digit] <= r.last()) {
                    break;
                }
                this.odo[digit] = r.first();
                --digit;
                assert digit >= 0;
            }
        }
        
        private int currentElement() {
            int value = 0;
            for (int ii = 0; ii < Section.this.getRank(); ++ii) {
                value += this.odo[ii] * this.stride[ii];
            }
            return value;
        }
    }
}
