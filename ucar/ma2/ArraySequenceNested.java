// 
// Decompiled by Procyon v0.5.36
// 

package ucar.ma2;

import java.util.Iterator;

public class ArraySequenceNested extends ArrayStructure
{
    private int[] sequenceLen;
    private int[] sequenceOffset;
    private int total;
    
    public ArraySequenceNested(final StructureMembers members, final int nseq) {
        super(members, new int[] { nseq });
        this.total = 0;
        this.sequenceLen = new int[nseq];
    }
    
    @Override
    protected StructureData makeStructureData(final ArrayStructure as, final int index) {
        return new StructureDataA(as, index);
    }
    
    @Override
    public StructureData getStructureData(final int index) {
        return new StructureDataA(this, index);
    }
    
    public void setSequenceLength(final int outerIndex, final int len) {
        this.sequenceLen[outerIndex] = len;
    }
    
    public int getSequenceLength(final int outerIndex) {
        return this.sequenceLen[outerIndex];
    }
    
    public int getSequenceOffset(final int outerIndex) {
        return this.sequenceOffset[outerIndex];
    }
    
    public void finish() {
        this.sequenceOffset = new int[this.nelems];
        this.total = 0;
        for (int i = 0; i < this.nelems; ++i) {
            this.sequenceOffset[i] = this.total;
            this.total += this.sequenceLen[i];
        }
        this.sdata = new StructureData[this.nelems];
        for (int i = 0; i < this.nelems; ++i) {
            this.sdata[i] = new StructureDataA(this, this.sequenceOffset[i]);
        }
        for (final StructureMembers.Member m : this.members.getMembers()) {
            final int[] mShape = m.getShape();
            final int[] shape = new int[mShape.length + 1];
            shape[0] = this.total;
            System.arraycopy(mShape, 0, shape, 1, mShape.length);
            final Array data = Array.factory(m.getDataType(), shape);
            m.setDataArray(data);
        }
    }
    
    public int getTotalNumberOfStructures() {
        return this.total;
    }
    
    public ArrayStructure flatten() {
        final ArrayStructureW aw = new ArrayStructureW(this.getStructureMembers(), new int[] { this.total });
        for (int i = 0; i < this.total; ++i) {
            final StructureData sdata = new StructureDataA(this, i);
            aw.setStructureData(sdata, i);
        }
        return aw;
    }
    
    @Override
    public double getScalarDouble(final int recnum, final StructureMembers.Member m) {
        if (m.getDataType() != DataType.DOUBLE) {
            throw new IllegalArgumentException("Type is " + m.getDataType() + ", must be double");
        }
        final Array data = m.getDataArray();
        return data.getDouble(recnum * m.getSize());
    }
    
    @Override
    public double[] getJavaArrayDouble(final int recnum, final StructureMembers.Member m) {
        if (m.getDataType() != DataType.DOUBLE) {
            throw new IllegalArgumentException("Type is " + m.getDataType() + ", must be double");
        }
        final int count = m.getSize();
        final Array data = m.getDataArray();
        final double[] pa = new double[count];
        for (int i = 0; i < count; ++i) {
            pa[i] = data.getDouble(recnum * count + i);
        }
        return pa;
    }
    
    @Override
    public float getScalarFloat(final int recnum, final StructureMembers.Member m) {
        if (m.getDataType() != DataType.FLOAT) {
            throw new IllegalArgumentException("Type is " + m.getDataType() + ", must be float");
        }
        final Array data = m.getDataArray();
        return data.getFloat(recnum * m.getSize());
    }
    
    @Override
    public float[] getJavaArrayFloat(final int recnum, final StructureMembers.Member m) {
        if (m.getDataType() != DataType.FLOAT) {
            throw new IllegalArgumentException("Type is " + m.getDataType() + ", must be float");
        }
        final int count = m.getSize();
        final Array data = m.getDataArray();
        final float[] pa = new float[count];
        for (int i = 0; i < count; ++i) {
            pa[i] = data.getFloat(recnum * count + i);
        }
        return pa;
    }
    
    @Override
    public byte getScalarByte(final int recnum, final StructureMembers.Member m) {
        if (m.getDataType() != DataType.BYTE && m.getDataType() != DataType.ENUM1) {
            throw new IllegalArgumentException("Type is " + m.getDataType() + ", must be byte");
        }
        final Array data = m.getDataArray();
        return data.getByte(recnum * m.getSize());
    }
    
    @Override
    public byte[] getJavaArrayByte(final int recnum, final StructureMembers.Member m) {
        if (m.getDataType() != DataType.BYTE && m.getDataType() != DataType.ENUM1) {
            throw new IllegalArgumentException("Type is " + m.getDataType() + ", must be byte");
        }
        final int count = m.getSize();
        final Array data = m.getDataArray();
        final byte[] pa = new byte[count];
        for (int i = 0; i < count; ++i) {
            pa[i] = data.getByte(recnum * count + i);
        }
        return pa;
    }
    
    @Override
    public short getScalarShort(final int recnum, final StructureMembers.Member m) {
        if (m.getDataType() != DataType.SHORT && m.getDataType() != DataType.ENUM2) {
            throw new IllegalArgumentException("Type is " + m.getDataType() + ", must be short");
        }
        final Array data = m.getDataArray();
        return data.getShort(recnum * m.getSize());
    }
    
    @Override
    public short[] getJavaArrayShort(final int recnum, final StructureMembers.Member m) {
        if (m.getDataType() != DataType.SHORT && m.getDataType() != DataType.ENUM2) {
            throw new IllegalArgumentException("Type is " + m.getDataType() + ", must be short");
        }
        final int count = m.getSize();
        final Array data = m.getDataArray();
        final short[] pa = new short[count];
        for (int i = 0; i < count; ++i) {
            pa[i] = data.getShort(recnum * count + i);
        }
        return pa;
    }
    
    @Override
    public int getScalarInt(final int recnum, final StructureMembers.Member m) {
        if (m.getDataType() != DataType.INT && m.getDataType() != DataType.ENUM4) {
            throw new IllegalArgumentException("Type is " + m.getDataType() + ", must be int");
        }
        final Array data = m.getDataArray();
        return data.getInt(recnum * m.getSize());
    }
    
    @Override
    public int[] getJavaArrayInt(final int recnum, final StructureMembers.Member m) {
        if (m.getDataType() != DataType.INT && m.getDataType() != DataType.ENUM4) {
            throw new IllegalArgumentException("Type is " + m.getDataType() + ", must be int");
        }
        final int count = m.getSize();
        final Array data = m.getDataArray();
        final int[] pa = new int[count];
        for (int i = 0; i < count; ++i) {
            pa[i] = data.getInt(recnum * count + i);
        }
        return pa;
    }
    
    @Override
    public long getScalarLong(final int recnum, final StructureMembers.Member m) {
        if (m.getDataType() != DataType.LONG) {
            throw new IllegalArgumentException("Type is " + m.getDataType() + ", must be long");
        }
        final Array data = m.getDataArray();
        return data.getLong(recnum * m.getSize());
    }
    
    @Override
    public long[] getJavaArrayLong(final int recnum, final StructureMembers.Member m) {
        if (m.getDataType() != DataType.LONG) {
            throw new IllegalArgumentException("Type is " + m.getDataType() + ", must be long");
        }
        final int count = m.getSize();
        final Array data = m.getDataArray();
        final long[] pa = new long[count];
        for (int i = 0; i < count; ++i) {
            pa[i] = data.getLong(recnum * count + i);
        }
        return pa;
    }
    
    @Override
    public char getScalarChar(final int recnum, final StructureMembers.Member m) {
        if (m.getDataType() != DataType.CHAR) {
            throw new IllegalArgumentException("Type is " + m.getDataType() + ", must be char");
        }
        final Array data = m.getDataArray();
        return data.getChar(recnum * m.getSize());
    }
    
    @Override
    public char[] getJavaArrayChar(final int recnum, final StructureMembers.Member m) {
        if (m.getDataType() != DataType.CHAR) {
            throw new IllegalArgumentException("Type is " + m.getDataType() + ", must be char");
        }
        final int count = m.getSize();
        final Array data = m.getDataArray();
        final char[] pa = new char[count];
        for (int i = 0; i < count; ++i) {
            pa[i] = data.getChar(recnum * count + i);
        }
        return pa;
    }
    
    @Override
    public String getScalarString(final int recnum, final StructureMembers.Member m) {
        if (m.getDataType() == DataType.CHAR) {
            final ArrayChar data = (ArrayChar)m.getDataArray();
            return data.getString(recnum);
        }
        if (m.getDataType() == DataType.STRING) {
            final ArrayObject data2 = (ArrayObject)m.getDataArray();
            return (String)data2.getObject(recnum);
        }
        throw new IllegalArgumentException("Type is " + m.getDataType() + ", must be String or char");
    }
    
    @Override
    public String[] getJavaArrayString(final int recnum, final StructureMembers.Member m) {
        final int n = m.getSize();
        final String[] result = new String[n];
        if (m.getDataType() == DataType.CHAR) {
            final ArrayChar data = (ArrayChar)m.getDataArray();
            for (int i = 0; i < n; ++i) {
                result[i] = data.getString(recnum * n + i);
            }
            return result;
        }
        if (m.getDataType() == DataType.STRING) {
            final Array data2 = m.getDataArray();
            for (int i = 0; i < n; ++i) {
                result[i] = (String)data2.getObject(recnum * n + i);
            }
            return result;
        }
        throw new IllegalArgumentException("Type is " + m.getDataType() + ", must be String or char");
    }
    
    @Override
    public StructureData getScalarStructure(final int recnum, final StructureMembers.Member m) {
        if (m.getDataType() == DataType.STRUCTURE) {
            final ArrayStructure data = (ArrayStructure)m.getDataArray();
            return data.getStructureData(recnum * m.getSize());
        }
        throw new IllegalArgumentException("Type is " + m.getDataType() + ", must be Structure");
    }
    
    @Override
    public ArrayStructure getArrayStructure(final int recnum, final StructureMembers.Member m) {
        if (m.getDataType() == DataType.STRUCTURE) {
            final ArrayStructure data = (ArrayStructure)m.getDataArray();
            final int count = m.getSize();
            final StructureData[] sdata = new StructureData[count];
            for (int i = 0; i < count; ++i) {
                sdata[i] = data.getStructureData(recnum * count + i);
            }
            return new ArrayStructureW(data.getStructureMembers(), m.getShape(), sdata);
        }
        throw new IllegalArgumentException("Type is " + m.getDataType() + ", must be Structure");
    }
}
