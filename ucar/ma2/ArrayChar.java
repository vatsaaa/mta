// 
// Decompiled by Procyon v0.5.36
// 

package ucar.ma2;

import java.nio.ByteBuffer;

public class ArrayChar extends Array
{
    protected char[] storage;
    
    static ArrayChar factory(final Index index) {
        return factory(index, null);
    }
    
    static ArrayChar factory(final Index index, final char[] storage) {
        switch (index.getRank()) {
            case 0: {
                return new D0(index, storage);
            }
            case 1: {
                return new D1(index, storage);
            }
            case 2: {
                return new D2(index, storage);
            }
            case 3: {
                return new D3(index, storage);
            }
            case 4: {
                return new D4(index, storage);
            }
            case 5: {
                return new D5(index, storage);
            }
            case 6: {
                return new D6(index, storage);
            }
            case 7: {
                return new D7(index, storage);
            }
            default: {
                return new ArrayChar(index, storage);
            }
        }
    }
    
    public ArrayChar(final int[] dimensions) {
        super(dimensions);
        this.storage = new char[(int)this.indexCalc.getSize()];
    }
    
    ArrayChar(final Index ima, final char[] data) {
        super(ima);
        if (data != null) {
            this.storage = data;
        }
        else {
            this.storage = new char[(int)ima.getSize()];
        }
    }
    
    @Override
    Array createView(final Index index) {
        return factory(index, this.storage);
    }
    
    @Override
    public Object getStorage() {
        return this.storage;
    }
    
    @Override
    void copyFrom1DJavaArray(final IndexIterator iter, final Object javaArray) {
        final char[] ja = (char[])javaArray;
        for (int i = 0; i < ja.length; ++i) {
            iter.setCharNext(ja[i]);
        }
    }
    
    @Override
    void copyTo1DJavaArray(final IndexIterator iter, final Object javaArray) {
        final char[] ja = (char[])javaArray;
        for (int i = 0; i < ja.length; ++i) {
            ja[i] = iter.getCharNext();
        }
    }
    
    @Override
    public ByteBuffer getDataAsByteBuffer() {
        final ByteBuffer bb = ByteBuffer.allocate((int)this.getSize());
        this.resetLocalIterator();
        while (this.hasNext()) {
            bb.put(this.nextByte());
        }
        return bb;
    }
    
    @Override
    public Class getElementType() {
        return Character.TYPE;
    }
    
    public char get(final Index i) {
        return this.storage[i.currentElement()];
    }
    
    public void set(final Index i, final char value) {
        this.storage[i.currentElement()] = value;
    }
    
    @Override
    public double getDouble(final Index i) {
        return this.storage[i.currentElement()];
    }
    
    @Override
    public void setDouble(final Index i, final double value) {
        this.storage[i.currentElement()] = (char)value;
    }
    
    @Override
    public float getFloat(final Index i) {
        return this.storage[i.currentElement()];
    }
    
    @Override
    public void setFloat(final Index i, final float value) {
        this.storage[i.currentElement()] = (char)value;
    }
    
    @Override
    public long getLong(final Index i) {
        return this.storage[i.currentElement()];
    }
    
    @Override
    public void setLong(final Index i, final long value) {
        this.storage[i.currentElement()] = (char)value;
    }
    
    @Override
    public int getInt(final Index i) {
        return this.storage[i.currentElement()];
    }
    
    @Override
    public void setInt(final Index i, final int value) {
        this.storage[i.currentElement()] = (char)value;
    }
    
    @Override
    public short getShort(final Index i) {
        return (short)this.storage[i.currentElement()];
    }
    
    @Override
    public void setShort(final Index i, final short value) {
        this.storage[i.currentElement()] = (char)value;
    }
    
    @Override
    public byte getByte(final Index i) {
        return (byte)this.storage[i.currentElement()];
    }
    
    @Override
    public void setByte(final Index i, final byte value) {
        this.storage[i.currentElement()] = (char)value;
    }
    
    @Override
    public char getChar(final Index i) {
        return this.storage[i.currentElement()];
    }
    
    @Override
    public void setChar(final Index i, final char value) {
        this.storage[i.currentElement()] = value;
    }
    
    @Override
    public boolean getBoolean(final Index i) {
        throw new ForbiddenConversionException();
    }
    
    @Override
    public void setBoolean(final Index i, final boolean value) {
        throw new ForbiddenConversionException();
    }
    
    @Override
    public Object getObject(final Index i) {
        return this.storage[i.currentElement()];
    }
    
    @Override
    public void setObject(final Index i, final Object value) {
        this.storage[i.currentElement()] = (char)value;
    }
    
    public String getString() {
        final int rank = this.getRank();
        if (rank != 1) {
            throw new IllegalArgumentException("ArayChar.getString rank must be 1");
        }
        final int strLen = this.indexCalc.getShape(0);
        int count = 0;
        for (int k = 0; k < strLen && '\0' != this.storage[k]; ++k) {
            ++count;
        }
        return new String(this.storage, 0, count);
    }
    
    public String getString(final int index) {
        final Index ima = this.getIndex();
        return this.getString(ima.set(index));
    }
    
    public String getString(final Index ima) {
        final int rank = this.getRank();
        if (rank == 0) {
            throw new IllegalArgumentException("ArayChar.getString rank must not be 0");
        }
        if (rank == 1) {
            return this.getString();
        }
        final int strLen = this.indexCalc.getShape(rank - 1);
        final char[] carray = new char[strLen];
        int count = 0;
        for (int k = 0; k < strLen; ++k) {
            ima.setDim(rank - 1, k);
            carray[k] = this.getChar(ima);
            if ('\0' == carray[k]) {
                break;
            }
            ++count;
        }
        return new String(carray, 0, count);
    }
    
    public void setString(final String val) {
        final int rank = this.getRank();
        if (rank != 1) {
            throw new IllegalArgumentException("ArayChar.setString rank must be 1");
        }
        final int arrayLen = this.indexCalc.getShape(0);
        final int strLen = Math.min(val.length(), arrayLen);
        for (int k = 0; k < strLen; ++k) {
            this.storage[k] = val.charAt(k);
        }
        final char c = '\0';
        for (int i = strLen; i < arrayLen; ++i) {
            this.storage[i] = c;
        }
    }
    
    public void setString(final int index, final String val) {
        final int rank = this.getRank();
        if (rank != 2) {
            throw new IllegalArgumentException("ArrayChar.setString rank must be 2");
        }
        final Index ima = this.getIndex();
        this.setString(ima.set(index), val);
    }
    
    public void setString(final Index ima, final String val) {
        final int rank = this.getRank();
        if (rank == 0) {
            throw new IllegalArgumentException("ArrayChar.setString rank must not be 0");
        }
        final int arrayLen = this.indexCalc.getShape(rank - 1);
        final int strLen = Math.min(val.length(), arrayLen);
        int count = 0;
        for (int k = 0; k < strLen; ++k) {
            ima.setDim(rank - 1, k);
            this.setChar(ima, val.charAt(k));
            ++count;
        }
        final char c = '\0';
        for (int i = count; i < arrayLen; ++i) {
            ima.setDim(rank - 1, i);
            this.setChar(ima, c);
        }
    }
    
    @Override
    public double getDouble(final int index) {
        return this.storage[index];
    }
    
    @Override
    public void setDouble(final int index, final double value) {
        this.storage[index] = (char)value;
    }
    
    @Override
    public float getFloat(final int index) {
        return this.storage[index];
    }
    
    @Override
    public void setFloat(final int index, final float value) {
        this.storage[index] = (char)value;
    }
    
    @Override
    public long getLong(final int index) {
        return this.storage[index];
    }
    
    @Override
    public void setLong(final int index, final long value) {
        this.storage[index] = (char)value;
    }
    
    @Override
    public int getInt(final int index) {
        return this.storage[index];
    }
    
    @Override
    public void setInt(final int index, final int value) {
        this.storage[index] = (char)value;
    }
    
    @Override
    public short getShort(final int index) {
        return (short)this.storage[index];
    }
    
    @Override
    public void setShort(final int index, final short value) {
        this.storage[index] = (char)value;
    }
    
    @Override
    public byte getByte(final int index) {
        return (byte)this.storage[index];
    }
    
    @Override
    public void setByte(final int index, final byte value) {
        this.storage[index] = (char)value;
    }
    
    @Override
    public char getChar(final int index) {
        return this.storage[index];
    }
    
    @Override
    public void setChar(final int index, final char value) {
        this.storage[index] = value;
    }
    
    @Override
    public boolean getBoolean(final int index) {
        throw new ForbiddenConversionException();
    }
    
    @Override
    public void setBoolean(final int index, final boolean value) {
        throw new ForbiddenConversionException();
    }
    
    @Override
    public Object getObject(final int index) {
        return this.getChar(index);
    }
    
    @Override
    public void setObject(final int index, final Object value) {
        this.storage[index] = (char)value;
    }
    
    @Override
    public String toString() {
        final StringBuilder sbuff = new StringBuilder();
        final StringIterator ii = this.getStringIterator();
        int count = 0;
        while (ii.hasNext()) {
            if (count > 0) {
                sbuff.append(",");
            }
            final String data = ii.next();
            sbuff.append(data);
            ++count;
        }
        return sbuff.toString();
    }
    
    public StringIterator getStringIterator() {
        return new StringIterator();
    }
    
    public ArrayObject make1DStringArray() {
        final int nelems = (this.getRank() == 0) ? 1 : ((int)this.getSize() / this.indexCalc.getShape(this.getRank() - 1));
        final Array sarr = Array.factory(String.class, new int[] { nelems });
        final IndexIterator newsiter = sarr.getIndexIterator();
        final StringIterator siter = this.getStringIterator();
        while (siter.hasNext()) {
            newsiter.setObjectNext(siter.next());
        }
        return (ArrayObject)sarr;
    }
    
    public static ArrayChar makeFromString(final String s, final int max) {
        final ArrayChar result = new D1(max);
        for (int i = 0; i < max && i < s.length(); ++i) {
            result.setChar(i, s.charAt(i));
        }
        return result;
    }
    
    public static ArrayChar makeFromStringArray(final ArrayObject values) {
        final IndexIterator ii = values.getIndexIterator();
        int strlen = 0;
        while (ii.hasNext()) {
            final String s = (String)ii.next();
            strlen = Math.max(s.length(), strlen);
        }
        return makeFromStringArray(values, strlen);
    }
    
    public static ArrayChar makeFromStringArray(final ArrayObject values, final int strlen) {
        try {
            final Section section = new Section(values.getShape());
            section.appendRange(strlen);
            final int[] shape = section.getShape();
            final long size = section.computeSize();
            final char[] cdata = new char[(int)size];
            int start = 0;
            final IndexIterator ii = values.getIndexIterator();
            while (ii.hasNext()) {
                final String s = (String)ii.next();
                for (int k = 0; k < s.length() && k < strlen; ++k) {
                    cdata[start + k] = s.charAt(k);
                }
                start += strlen;
            }
            final Array carr = Array.factory(Character.TYPE, shape, cdata);
            return (ArrayChar)carr;
        }
        catch (InvalidRangeException e) {
            e.printStackTrace();
            return null;
        }
    }
    
    public static class D0 extends ArrayChar
    {
        private Index0D ix;
        
        public D0() {
            super(new int[0]);
            this.ix = (Index0D)this.indexCalc;
        }
        
        private D0(final Index i, final char[] store) {
            super(i, store);
            this.ix = (Index0D)this.indexCalc;
        }
        
        public char get() {
            return this.storage[this.ix.currentElement()];
        }
        
        public void set(final char value) {
            this.storage[this.ix.currentElement()] = value;
        }
    }
    
    public static class D1 extends ArrayChar
    {
        private Index1D ix;
        
        public D1(final int len0) {
            super(new int[] { len0 });
            this.ix = (Index1D)this.indexCalc;
        }
        
        private D1(final Index i, final char[] store) {
            super(i, store);
            this.ix = (Index1D)this.indexCalc;
        }
        
        public char get(final int i) {
            return this.storage[this.ix.setDirect(i)];
        }
        
        public void set(final int i, final char value) {
            this.storage[this.ix.setDirect(i)] = value;
        }
    }
    
    public static class D2 extends ArrayChar
    {
        private Index2D ix;
        
        public D2(final int len0, final int len1) {
            super(new int[] { len0, len1 });
            this.ix = (Index2D)this.indexCalc;
        }
        
        private D2(final Index i, final char[] store) {
            super(i, store);
            this.ix = (Index2D)this.indexCalc;
        }
        
        public char get(final int i, final int j) {
            return this.storage[this.ix.setDirect(i, j)];
        }
        
        public void set(final int i, final int j, final char value) {
            this.storage[this.ix.setDirect(i, j)] = value;
        }
    }
    
    public static class D3 extends ArrayChar
    {
        private Index3D ix;
        
        public D3(final int len0, final int len1, final int len2) {
            super(new int[] { len0, len1, len2 });
            this.ix = (Index3D)this.indexCalc;
        }
        
        private D3(final Index i, final char[] store) {
            super(i, store);
            this.ix = (Index3D)this.indexCalc;
        }
        
        public char get(final int i, final int j, final int k) {
            return this.storage[this.ix.setDirect(i, j, k)];
        }
        
        public void set(final int i, final int j, final int k, final char value) {
            this.storage[this.ix.setDirect(i, j, k)] = value;
        }
    }
    
    public static class D4 extends ArrayChar
    {
        private Index4D ix;
        
        public D4(final int len0, final int len1, final int len2, final int len3) {
            super(new int[] { len0, len1, len2, len3 });
            this.ix = (Index4D)this.indexCalc;
        }
        
        private D4(final Index i, final char[] store) {
            super(i, store);
            this.ix = (Index4D)this.indexCalc;
        }
        
        public char get(final int i, final int j, final int k, final int l) {
            return this.storage[this.ix.setDirect(i, j, k, l)];
        }
        
        public void set(final int i, final int j, final int k, final int l, final char value) {
            this.storage[this.ix.setDirect(i, j, k, l)] = value;
        }
    }
    
    public static class D5 extends ArrayChar
    {
        private Index5D ix;
        
        public D5(final int len0, final int len1, final int len2, final int len3, final int len4) {
            super(new int[] { len0, len1, len2, len3, len4 });
            this.ix = (Index5D)this.indexCalc;
        }
        
        private D5(final Index i, final char[] store) {
            super(i, store);
            this.ix = (Index5D)this.indexCalc;
        }
        
        public char get(final int i, final int j, final int k, final int l, final int m) {
            return this.storage[this.ix.setDirect(i, j, k, l, m)];
        }
        
        public void set(final int i, final int j, final int k, final int l, final int m, final char value) {
            this.storage[this.ix.setDirect(i, j, k, l, m)] = value;
        }
    }
    
    public static class D6 extends ArrayChar
    {
        private Index6D ix;
        
        public D6(final int len0, final int len1, final int len2, final int len3, final int len4, final int len5) {
            super(new int[] { len0, len1, len2, len3, len4, len5 });
            this.ix = (Index6D)this.indexCalc;
        }
        
        private D6(final Index i, final char[] store) {
            super(i, store);
            this.ix = (Index6D)this.indexCalc;
        }
        
        public char get(final int i, final int j, final int k, final int l, final int m, final int n) {
            return this.storage[this.ix.setDirect(i, j, k, l, m, n)];
        }
        
        public void set(final int i, final int j, final int k, final int l, final int m, final int n, final char value) {
            this.storage[this.ix.setDirect(i, j, k, l, m, n)] = value;
        }
    }
    
    public static class D7 extends ArrayChar
    {
        private Index7D ix;
        
        public D7(final int len0, final int len1, final int len2, final int len3, final int len4, final int len5, final int len6) {
            super(new int[] { len0, len1, len2, len3, len4, len5, len6 });
            this.ix = (Index7D)this.indexCalc;
        }
        
        private D7(final Index i, final char[] store) {
            super(i, store);
            this.ix = (Index7D)this.indexCalc;
        }
        
        public char get(final int i, final int j, final int k, final int l, final int m, final int n, final int o) {
            return this.storage[this.ix.setDirect(i, j, k, l, m, n, o)];
        }
        
        public void set(final int i, final int j, final int k, final int l, final int m, final int n, final int o, final char value) {
            this.storage[this.ix.setDirect(i, j, k, l, m, n, o)] = value;
        }
    }
    
    public class StringIterator
    {
        private IndexIterator ii;
        private int strLen;
        private char[] carray;
        
        StringIterator() {
            this.ii = ArrayChar.this.getIndexIterator();
            if (ArrayChar.this.rank == 0) {
                this.strLen = 1;
            }
            else {
                this.strLen = ArrayChar.this.indexCalc.getShape(ArrayChar.this.rank - 1);
            }
            this.carray = new char[this.strLen];
        }
        
        public int getNumElems() {
            return (int)ArrayChar.this.getSize() / this.strLen;
        }
        
        public boolean hasNext() {
            return this.ii.hasNext();
        }
        
        public String next() {
            int stop = this.strLen;
            for (int k = 0; k < this.strLen; ++k) {
                this.carray[k] = this.ii.getCharNext();
                if ('\0' == this.carray[k] && stop == this.strLen) {
                    stop = k;
                }
            }
            return new String(this.carray, 0, stop);
        }
    }
}
