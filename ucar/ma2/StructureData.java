// 
// Decompiled by Procyon v0.5.36
// 

package ucar.ma2;

import java.util.Iterator;
import java.util.Formatter;
import java.util.List;

public abstract class StructureData
{
    protected StructureMembers members;
    
    public StructureData(final StructureMembers members) {
        this.members = members;
    }
    
    public String getName() {
        return this.members.getName();
    }
    
    public StructureMembers getStructureMembers() {
        return this.members;
    }
    
    public List<StructureMembers.Member> getMembers() {
        return this.members.getMembers();
    }
    
    public StructureMembers.Member findMember(final String memberName) {
        return this.members.findMember(memberName);
    }
    
    public abstract Array getArray(final StructureMembers.Member p0);
    
    public Array getArray(final String memberName) {
        final StructureMembers.Member m = this.members.findMember(memberName);
        if (m == null) {
            throw new IllegalArgumentException("illegal member name =" + memberName);
        }
        return this.getArray(m);
    }
    
    public Object getScalarObject(final String memberName) {
        final StructureMembers.Member m = this.members.findMember(memberName);
        if (m == null) {
            throw new IllegalArgumentException("illegal member name =" + memberName);
        }
        return this.getScalarObject(m);
    }
    
    public Object getScalarObject(final StructureMembers.Member m) {
        final DataType dataType = m.getDataType();
        if (dataType == DataType.DOUBLE) {
            return this.getScalarDouble(m);
        }
        if (dataType == DataType.FLOAT) {
            return this.getScalarFloat(m);
        }
        if (dataType == DataType.BYTE || dataType == DataType.ENUM1) {
            return this.getScalarByte(m);
        }
        if (dataType == DataType.SHORT || dataType == DataType.ENUM2) {
            return this.getScalarShort(m);
        }
        if (dataType == DataType.INT || dataType == DataType.ENUM4) {
            return this.getScalarInt(m);
        }
        if (dataType == DataType.LONG) {
            return this.getScalarLong(m);
        }
        if (dataType == DataType.CHAR) {
            return this.getScalarString(m);
        }
        if (dataType == DataType.STRING) {
            return this.getScalarString(m);
        }
        if (dataType == DataType.STRUCTURE) {
            return this.getScalarStructure(m);
        }
        if (dataType == DataType.SEQUENCE) {
            return this.getArraySequence(m);
        }
        throw new RuntimeException("Dont have implemenation for " + dataType);
    }
    
    public float convertScalarFloat(final String memberName) {
        final StructureMembers.Member m = this.members.findMember(memberName);
        if (m == null) {
            throw new IllegalArgumentException("illegal member name =" + memberName);
        }
        return this.convertScalarFloat(m);
    }
    
    public abstract float convertScalarFloat(final StructureMembers.Member p0);
    
    public double convertScalarDouble(final String memberName) {
        final StructureMembers.Member m = this.members.findMember(memberName);
        if (m == null) {
            throw new IllegalArgumentException("illegal member name =" + memberName);
        }
        return this.convertScalarDouble(m);
    }
    
    public abstract double convertScalarDouble(final StructureMembers.Member p0);
    
    public int convertScalarInt(final String memberName) {
        final StructureMembers.Member m = this.members.findMember(memberName);
        if (m == null) {
            throw new IllegalArgumentException("illegal member name =" + memberName);
        }
        return this.convertScalarInt(m);
    }
    
    public abstract int convertScalarInt(final StructureMembers.Member p0);
    
    public double getScalarDouble(final String memberName) {
        final StructureMembers.Member m = this.members.findMember(memberName);
        if (m == null) {
            throw new IllegalArgumentException("illegal member name =" + memberName);
        }
        return this.getScalarDouble(m);
    }
    
    public abstract double getScalarDouble(final StructureMembers.Member p0);
    
    public double[] getJavaArrayDouble(final String memberName) {
        final StructureMembers.Member m = this.members.findMember(memberName);
        if (m == null) {
            throw new IllegalArgumentException("illegal member name =" + memberName);
        }
        return this.getJavaArrayDouble(m);
    }
    
    public abstract double[] getJavaArrayDouble(final StructureMembers.Member p0);
    
    public float getScalarFloat(final String memberName) {
        final StructureMembers.Member m = this.members.findMember(memberName);
        if (m == null) {
            throw new IllegalArgumentException("illegal member name =" + memberName);
        }
        return this.getScalarFloat(m);
    }
    
    public abstract float getScalarFloat(final StructureMembers.Member p0);
    
    public float[] getJavaArrayFloat(final String memberName) {
        final StructureMembers.Member m = this.members.findMember(memberName);
        if (m == null) {
            throw new IllegalArgumentException("illegal member name =" + memberName);
        }
        return this.getJavaArrayFloat(m);
    }
    
    public abstract float[] getJavaArrayFloat(final StructureMembers.Member p0);
    
    public byte getScalarByte(final String memberName) {
        final StructureMembers.Member m = this.members.findMember(memberName);
        if (m == null) {
            throw new IllegalArgumentException("illegal member name =" + memberName);
        }
        return this.getScalarByte(m);
    }
    
    public abstract byte getScalarByte(final StructureMembers.Member p0);
    
    public byte[] getJavaArrayByte(final String memberName) {
        final StructureMembers.Member m = this.members.findMember(memberName);
        if (m == null) {
            throw new IllegalArgumentException("illegal member name =" + memberName);
        }
        return this.getJavaArrayByte(m);
    }
    
    public abstract byte[] getJavaArrayByte(final StructureMembers.Member p0);
    
    public int getScalarInt(final String memberName) {
        final StructureMembers.Member m = this.members.findMember(memberName);
        if (m == null) {
            throw new IllegalArgumentException("illegal member name =" + memberName);
        }
        return this.getScalarInt(m);
    }
    
    public abstract int getScalarInt(final StructureMembers.Member p0);
    
    public int[] getJavaArrayInt(final String memberName) {
        final StructureMembers.Member m = this.members.findMember(memberName);
        if (m == null) {
            throw new IllegalArgumentException("illegal member name =" + memberName);
        }
        return this.getJavaArrayInt(m);
    }
    
    public abstract int[] getJavaArrayInt(final StructureMembers.Member p0);
    
    public short getScalarShort(final String memberName) {
        final StructureMembers.Member m = this.members.findMember(memberName);
        if (m == null) {
            throw new IllegalArgumentException("illegal member name =" + memberName);
        }
        return this.getScalarShort(m);
    }
    
    public abstract short getScalarShort(final StructureMembers.Member p0);
    
    public short[] getJavaArrayShort(final String memberName) {
        final StructureMembers.Member m = this.members.findMember(memberName);
        if (m == null) {
            throw new IllegalArgumentException("illegal member name =" + memberName);
        }
        return this.getJavaArrayShort(m);
    }
    
    public abstract short[] getJavaArrayShort(final StructureMembers.Member p0);
    
    public long getScalarLong(final String memberName) {
        final StructureMembers.Member m = this.members.findMember(memberName);
        if (m == null) {
            throw new IllegalArgumentException("illegal member name =" + memberName);
        }
        return this.getScalarLong(m);
    }
    
    public abstract long getScalarLong(final StructureMembers.Member p0);
    
    public long[] getJavaArrayLong(final String memberName) {
        final StructureMembers.Member m = this.members.findMember(memberName);
        if (m == null) {
            throw new IllegalArgumentException("illegal member name =" + memberName);
        }
        return this.getJavaArrayLong(m);
    }
    
    public abstract long[] getJavaArrayLong(final StructureMembers.Member p0);
    
    public char getScalarChar(final String memberName) {
        final StructureMembers.Member m = this.members.findMember(memberName);
        if (m == null) {
            throw new IllegalArgumentException("illegal member name =" + memberName);
        }
        return this.getScalarChar(m);
    }
    
    public abstract char getScalarChar(final StructureMembers.Member p0);
    
    public char[] getJavaArrayChar(final String memberName) {
        final StructureMembers.Member m = this.members.findMember(memberName);
        if (m == null) {
            throw new IllegalArgumentException("illegal member name =" + memberName);
        }
        return this.getJavaArrayChar(m);
    }
    
    public abstract char[] getJavaArrayChar(final StructureMembers.Member p0);
    
    public String getScalarString(final String memberName) {
        final StructureMembers.Member m = this.findMember(memberName);
        if (null == m) {
            throw new IllegalArgumentException("Member not found= " + memberName);
        }
        return this.getScalarString(m);
    }
    
    public abstract String getScalarString(final StructureMembers.Member p0);
    
    public String[] getJavaArrayString(final String memberName) {
        final StructureMembers.Member m = this.members.findMember(memberName);
        if (m == null) {
            throw new IllegalArgumentException("illegal member name =" + memberName);
        }
        return this.getJavaArrayString(m);
    }
    
    public abstract String[] getJavaArrayString(final StructureMembers.Member p0);
    
    public StructureData getScalarStructure(final String memberName) {
        final StructureMembers.Member m = this.findMember(memberName);
        if (null == m) {
            throw new IllegalArgumentException("Member not found= " + memberName);
        }
        return this.getScalarStructure(m);
    }
    
    public abstract StructureData getScalarStructure(final StructureMembers.Member p0);
    
    public ArrayStructure getArrayStructure(final String memberName) {
        final StructureMembers.Member m = this.findMember(memberName);
        if (null == m) {
            throw new IllegalArgumentException("Member not found= " + memberName);
        }
        return this.getArrayStructure(m);
    }
    
    public abstract ArrayStructure getArrayStructure(final StructureMembers.Member p0);
    
    public ArraySequence getArraySequence(final String memberName) {
        final StructureMembers.Member m = this.members.findMember(memberName);
        if (m == null) {
            throw new IllegalArgumentException("illegal member name =" + memberName);
        }
        return this.getArraySequence(m);
    }
    
    public abstract ArraySequence getArraySequence(final StructureMembers.Member p0);
    
    public void showInternal(final Formatter f, final String leadingSpace) {
        f.format("%sStructureData %s class=%s hash=0x%x%n", leadingSpace, this.members.getName(), this.getClass().getName(), this.hashCode());
    }
    
    public void showInternalMembers(final Formatter f, final String leadingSpace) {
        f.format("%sStructureData %s class=%s hash=0x%x%n", leadingSpace, this.members.getName(), this.getClass().getName(), this.hashCode());
        for (final StructureMembers.Member m : this.getMembers()) {
            m.showInternal(f, leadingSpace + "  ");
        }
    }
}
