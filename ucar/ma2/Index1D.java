// 
// Decompiled by Procyon v0.5.36
// 

package ucar.ma2;

public class Index1D extends Index
{
    private int curr0;
    private int stride0;
    private int shape0;
    
    Index1D() {
        super(1);
    }
    
    public Index1D(final int[] shape) {
        super(shape);
        this.precalc();
    }
    
    @Override
    public String toString() {
        return Integer.toString(this.curr0);
    }
    
    @Override
    protected void precalc() {
        this.shape0 = this.shape[0];
        this.stride0 = this.stride[0];
        this.curr0 = this.current[0];
    }
    
    @Override
    public int[] getCurrentCounter() {
        this.current[0] = this.curr0;
        return this.current.clone();
    }
    
    @Override
    public int currentElement() {
        return this.offset + this.curr0 * this.stride0;
    }
    
    @Override
    public int incr() {
        if (++this.curr0 >= this.shape0) {
            this.curr0 = 0;
        }
        return this.offset + this.curr0 * this.stride0;
    }
    
    @Override
    public void setDim(final int dim, final int value) {
        if (value < 0 || value >= this.shape[dim]) {
            throw new ArrayIndexOutOfBoundsException();
        }
        this.curr0 = value;
    }
    
    @Override
    public Index set0(final int v) {
        if (v < 0 || v >= this.shape0) {
            throw new ArrayIndexOutOfBoundsException();
        }
        this.curr0 = v;
        return this;
    }
    
    @Override
    public Index set(final int v0) {
        this.set0(v0);
        return this;
    }
    
    @Override
    public Index set(final int[] index) {
        if (index.length != this.rank) {
            throw new ArrayIndexOutOfBoundsException();
        }
        this.set0(index[0]);
        return this;
    }
    
    @Override
    public Object clone() {
        return super.clone();
    }
    
    int setDirect(final int v0) {
        if (v0 < 0 || v0 >= this.shape0) {
            throw new ArrayIndexOutOfBoundsException();
        }
        return this.offset + v0 * this.stride0;
    }
}
