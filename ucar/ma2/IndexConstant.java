// 
// Decompiled by Procyon v0.5.36
// 

package ucar.ma2;

import java.util.List;

public class IndexConstant extends Index
{
    protected IndexConstant(final int rank) {
        super(rank);
    }
    
    protected IndexConstant(final int[] shape) {
        super(shape);
    }
    
    protected IndexConstant(final int[] shape, final int[] stride) {
        super(shape, stride);
    }
    
    @Override
    public int currentElement() {
        return 0;
    }
    
    @Override
    Index flip(final int index) {
        return this;
    }
    
    @Override
    Index sectionNoReduce(final List<Range> ranges) throws InvalidRangeException {
        final Section curr = new Section(this.shape);
        final Section want = curr.compose(new Section(ranges));
        return new IndexConstant(want.getShape());
    }
    
    @Override
    Index section(final List<Range> ranges) throws InvalidRangeException {
        final Section curr = new Section(this.shape);
        final Section want = curr.compose(new Section(ranges)).reduce();
        return new IndexConstant(want.getShape());
    }
    
    @Override
    Index reduce() {
        final Section curr = new Section(this.shape);
        final Section want = curr.reduce();
        return new IndexConstant(want.getShape());
    }
    
    @Override
    Index reduce(final int dim) {
        if (dim < 0 || dim >= this.rank) {
            throw new IllegalArgumentException("illegal reduce dim " + dim);
        }
        if (this.shape[dim] != 1) {
            throw new IllegalArgumentException("illegal reduce dim " + dim + " : length != 1");
        }
        final Section curr = new Section(this.shape);
        final Section want = curr.removeRange(dim);
        return new IndexConstant(want.getShape());
    }
    
    @Override
    Index transpose(final int index1, final int index2) {
        return this;
    }
    
    @Override
    Index permute(final int[] dims) {
        return this;
    }
    
    @Override
    IndexIterator getIndexIterator(final Array maa) {
        return new IteratorConstant(this.size, maa);
    }
    
    private class IteratorConstant implements IndexIterator
    {
        private int currElement;
        private final Array maa;
        private long size;
        private Index counter;
        
        IteratorConstant(final long size, final Array maa) {
            this.currElement = -1;
            this.counter = null;
            this.size = size;
            this.maa = maa;
        }
        
        public boolean hasNext() {
            return this.currElement < this.size - 1L;
        }
        
        public boolean hasMore(final int howMany) {
            return this.currElement < this.size - howMany;
        }
        
        @Override
        public String toString() {
            if (this.counter == null) {
                this.counter = new Index(this.maa.getShape());
            }
            this.counter.setCurrentCounter(this.currElement);
            return this.counter.toString();
        }
        
        public int[] getCurrentCounter() {
            if (this.counter == null) {
                this.counter = new Index(this.maa.getShape());
            }
            this.counter.setCurrentCounter(this.currElement);
            return this.counter.current;
        }
        
        public double getDoubleCurrent() {
            return this.maa.getDouble(0);
        }
        
        public double getDoubleNext() {
            ++this.currElement;
            return this.maa.getDouble(0);
        }
        
        public void setDoubleCurrent(final double val) {
            this.maa.setDouble(0, val);
        }
        
        public void setDoubleNext(final double val) {
            ++this.currElement;
            this.maa.setDouble(0, val);
        }
        
        public float getFloatCurrent() {
            return this.maa.getFloat(this.currElement);
        }
        
        public float getFloatNext() {
            ++this.currElement;
            return this.maa.getFloat(0);
        }
        
        public void setFloatCurrent(final float val) {
            this.maa.setFloat(this.currElement, val);
        }
        
        public void setFloatNext(final float val) {
            ++this.currElement;
            this.maa.setFloat(0, val);
        }
        
        public long getLongCurrent() {
            return this.maa.getLong(this.currElement);
        }
        
        public long getLongNext() {
            ++this.currElement;
            return this.maa.getLong(0);
        }
        
        public void setLongCurrent(final long val) {
            this.maa.setLong(this.currElement, val);
        }
        
        public void setLongNext(final long val) {
            ++this.currElement;
            this.maa.setLong(0, val);
        }
        
        public int getIntCurrent() {
            return this.maa.getInt(this.currElement);
        }
        
        public int getIntNext() {
            ++this.currElement;
            return this.maa.getInt(0);
        }
        
        public void setIntCurrent(final int val) {
            this.maa.setInt(this.currElement, val);
        }
        
        public void setIntNext(final int val) {
            ++this.currElement;
            this.maa.setInt(0, val);
        }
        
        public short getShortCurrent() {
            return this.maa.getShort(this.currElement);
        }
        
        public short getShortNext() {
            ++this.currElement;
            return this.maa.getShort(0);
        }
        
        public void setShortCurrent(final short val) {
            this.maa.setShort(this.currElement, val);
        }
        
        public void setShortNext(final short val) {
            ++this.currElement;
            this.maa.setShort(0, val);
        }
        
        public byte getByteCurrent() {
            return this.maa.getByte(this.currElement);
        }
        
        public byte getByteNext() {
            ++this.currElement;
            return this.maa.getByte(0);
        }
        
        public void setByteCurrent(final byte val) {
            this.maa.setByte(this.currElement, val);
        }
        
        public void setByteNext(final byte val) {
            ++this.currElement;
            this.maa.setByte(0, val);
        }
        
        public char getCharCurrent() {
            return this.maa.getChar(this.currElement);
        }
        
        public char getCharNext() {
            ++this.currElement;
            return this.maa.getChar(0);
        }
        
        public void setCharCurrent(final char val) {
            this.maa.setChar(this.currElement, val);
        }
        
        public void setCharNext(final char val) {
            ++this.currElement;
            this.maa.setChar(0, val);
        }
        
        public boolean getBooleanCurrent() {
            return this.maa.getBoolean(this.currElement);
        }
        
        public boolean getBooleanNext() {
            ++this.currElement;
            return this.maa.getBoolean(0);
        }
        
        public void setBooleanCurrent(final boolean val) {
            this.maa.setBoolean(this.currElement, val);
        }
        
        public void setBooleanNext(final boolean val) {
            ++this.currElement;
            this.maa.setBoolean(0, val);
        }
        
        public Object getObjectCurrent() {
            return this.maa.getObject(this.currElement);
        }
        
        public Object getObjectNext() {
            ++this.currElement;
            return this.maa.getObject(0);
        }
        
        public void setObjectCurrent(final Object val) {
            this.maa.setObject(this.currElement, val);
        }
        
        public void setObjectNext(final Object val) {
            ++this.currElement;
            this.maa.setObject(0, val);
        }
        
        public Object next() {
            ++this.currElement;
            return this.maa.getObject(0);
        }
    }
}
