// 
// Decompiled by Procyon v0.5.36
// 

package ucar.ma2;

public class IteratorFast implements IndexIterator
{
    private int currElement;
    private final Array maa;
    private long size;
    private Index counter;
    
    IteratorFast(final long size, final Array maa) {
        this.currElement = -1;
        this.counter = null;
        this.size = size;
        this.maa = maa;
    }
    
    public boolean hasNext() {
        return this.currElement < this.size - 1L;
    }
    
    public boolean hasMore(final int howMany) {
        return this.currElement < this.size - howMany;
    }
    
    @Override
    public String toString() {
        if (this.counter == null) {
            this.counter = new Index(this.maa.getShape());
        }
        this.counter.setCurrentCounter(this.currElement);
        return this.counter.toString();
    }
    
    public int[] getCurrentCounter() {
        if (this.counter == null) {
            this.counter = new Index(this.maa.getShape());
        }
        this.counter.setCurrentCounter(this.currElement);
        return this.counter.current;
    }
    
    public double getDoubleCurrent() {
        return this.maa.getDouble(this.currElement);
    }
    
    public double getDoubleNext() {
        return this.maa.getDouble(++this.currElement);
    }
    
    public void setDoubleCurrent(final double val) {
        this.maa.setDouble(this.currElement, val);
    }
    
    public void setDoubleNext(final double val) {
        this.maa.setDouble(++this.currElement, val);
    }
    
    public float getFloatCurrent() {
        return this.maa.getFloat(this.currElement);
    }
    
    public float getFloatNext() {
        return this.maa.getFloat(++this.currElement);
    }
    
    public void setFloatCurrent(final float val) {
        this.maa.setFloat(this.currElement, val);
    }
    
    public void setFloatNext(final float val) {
        this.maa.setFloat(++this.currElement, val);
    }
    
    public long getLongCurrent() {
        return this.maa.getLong(this.currElement);
    }
    
    public long getLongNext() {
        return this.maa.getLong(++this.currElement);
    }
    
    public void setLongCurrent(final long val) {
        this.maa.setLong(this.currElement, val);
    }
    
    public void setLongNext(final long val) {
        this.maa.setLong(++this.currElement, val);
    }
    
    public int getIntCurrent() {
        return this.maa.getInt(this.currElement);
    }
    
    public int getIntNext() {
        return this.maa.getInt(++this.currElement);
    }
    
    public void setIntCurrent(final int val) {
        this.maa.setInt(this.currElement, val);
    }
    
    public void setIntNext(final int val) {
        this.maa.setInt(++this.currElement, val);
    }
    
    public short getShortCurrent() {
        return this.maa.getShort(this.currElement);
    }
    
    public short getShortNext() {
        return this.maa.getShort(++this.currElement);
    }
    
    public void setShortCurrent(final short val) {
        this.maa.setShort(this.currElement, val);
    }
    
    public void setShortNext(final short val) {
        this.maa.setShort(++this.currElement, val);
    }
    
    public byte getByteCurrent() {
        return this.maa.getByte(this.currElement);
    }
    
    public byte getByteNext() {
        return this.maa.getByte(++this.currElement);
    }
    
    public void setByteCurrent(final byte val) {
        this.maa.setByte(this.currElement, val);
    }
    
    public void setByteNext(final byte val) {
        this.maa.setByte(++this.currElement, val);
    }
    
    public char getCharCurrent() {
        return this.maa.getChar(this.currElement);
    }
    
    public char getCharNext() {
        return this.maa.getChar(++this.currElement);
    }
    
    public void setCharCurrent(final char val) {
        this.maa.setChar(this.currElement, val);
    }
    
    public void setCharNext(final char val) {
        this.maa.setChar(++this.currElement, val);
    }
    
    public boolean getBooleanCurrent() {
        return this.maa.getBoolean(this.currElement);
    }
    
    public boolean getBooleanNext() {
        return this.maa.getBoolean(++this.currElement);
    }
    
    public void setBooleanCurrent(final boolean val) {
        this.maa.setBoolean(this.currElement, val);
    }
    
    public void setBooleanNext(final boolean val) {
        this.maa.setBoolean(++this.currElement, val);
    }
    
    public Object getObjectCurrent() {
        return this.maa.getObject(this.currElement);
    }
    
    public Object getObjectNext() {
        return this.maa.getObject(++this.currElement);
    }
    
    public void setObjectCurrent(final Object val) {
        this.maa.setObject(this.currElement, val);
    }
    
    public void setObjectNext(final Object val) {
        this.maa.setObject(++this.currElement, val);
    }
    
    public Object next() {
        return this.maa.getObject(++this.currElement);
    }
}
