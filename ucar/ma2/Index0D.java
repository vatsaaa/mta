// 
// Decompiled by Procyon v0.5.36
// 

package ucar.ma2;

public class Index0D extends Index
{
    Index0D() {
        super(0);
        this.size = 1L;
        this.offset = 0;
    }
    
    public Index0D(final int[] shape) {
        super(shape);
    }
    
    @Override
    public int currentElement() {
        return this.offset;
    }
    
    @Override
    public int incr() {
        return this.offset;
    }
    
    @Override
    public Object clone() {
        return super.clone();
    }
}
