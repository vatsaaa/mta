// 
// Decompiled by Procyon v0.5.36
// 

package ucar.ma2;

public class MAMatrix
{
    private Array a;
    private int nrows;
    private int ncols;
    private Index ima;
    
    public MAMatrix(final int nrows, final int ncols) {
        this.a = new ArrayDouble.D2(nrows, ncols);
        this.nrows = nrows;
        this.ncols = ncols;
        this.ima = this.a.getIndex();
    }
    
    public MAMatrix(final Array a) {
        this.a = a;
        if (a.getRank() != 2) {
            throw new IllegalArgumentException("rank != 2, instead = " + a.getRank());
        }
        this.nrows = a.getShape()[0];
        this.ncols = a.getShape()[1];
        this.ima = a.getIndex();
    }
    
    public int getNrows() {
        return this.nrows;
    }
    
    public int getNcols() {
        return this.ncols;
    }
    
    public double getDouble(final int i, final int j) {
        return this.a.getDouble(this.ima.set(i, j));
    }
    
    public void setDouble(final int i, final int j, final double val) {
        this.a.setDouble(this.ima.set(i, j), val);
    }
    
    public MAMatrix copy() {
        return new MAMatrix(this.a.copy());
    }
    
    public MAMatrix transpose() {
        return new MAMatrix(this.a.transpose(0, 1));
    }
    
    public MAVector column(final int j) {
        return new MAVector(this.a.slice(1, j));
    }
    
    public MAVector row(final int i) {
        return new MAVector(this.a.slice(0, i));
    }
    
    public MAVector dot(final MAVector v) {
        if (this.ncols != v.getNelems()) {
            throw new IllegalArgumentException("MAMatrix.dot " + this.ncols + " != " + v.getNelems());
        }
        final ArrayDouble.D1 result = new ArrayDouble.D1(this.nrows);
        final Index imr = result.getIndex();
        for (int i = 0; i < this.nrows; ++i) {
            double sum = 0.0;
            for (int k = 0; k < this.ncols; ++k) {
                sum += this.getDouble(i, k) * v.getDouble(k);
            }
            result.setDouble(imr.set(i), sum);
        }
        return new MAVector(result);
    }
    
    public static MAMatrix multiply(final MAMatrix m1, final MAMatrix m2) {
        if (m1.getNcols() != m2.getNrows()) {
            throw new IllegalArgumentException("MAMatrix.multiply " + m1.getNcols() + " != " + m2.getNrows());
        }
        final int kdims = m1.getNcols();
        final ArrayDouble.D2 result = new ArrayDouble.D2(m1.getNrows(), m2.getNcols());
        final Index imr = result.getIndex();
        for (int i = 0; i < m1.getNrows(); ++i) {
            for (int j = 0; j < m2.getNcols(); ++j) {
                double sum = 0.0;
                for (int k = 0; k < kdims; ++k) {
                    sum += m1.getDouble(i, k) * m2.getDouble(k, j);
                }
                result.setDouble(imr.set(i, j), sum);
            }
        }
        return new MAMatrix(result);
    }
    
    public void postMultiplyDiagonal(final MAVector diag) {
        if (this.ncols != diag.getNelems()) {
            throw new IllegalArgumentException("MAMatrix.postMultiplyDiagonal " + this.ncols + " != " + diag.getNelems());
        }
        for (int i = 0; i < this.nrows; ++i) {
            for (int j = 0; j < this.ncols; ++j) {
                final double val = this.a.getDouble(this.ima.set(i, j)) * diag.getDouble(j);
                this.a.setDouble(this.ima, val);
            }
        }
    }
    
    public void preMultiplyDiagonal(final MAVector diag) {
        if (this.nrows != diag.getNelems()) {
            throw new IllegalArgumentException("MAMatrix.preMultiplyDiagonal " + this.nrows + " != " + diag.getNelems());
        }
        for (int i = 0; i < this.nrows; ++i) {
            for (int j = 0; j < this.ncols; ++j) {
                final double val = this.a.getDouble(this.ima.set(i, j)) * diag.getDouble(i);
                this.a.setDouble(this.ima, val);
            }
        }
    }
}
