// 
// Decompiled by Procyon v0.5.36
// 

package ucar.ma2;

public class MAVector
{
    private Array a;
    private int nelems;
    private Index ima;
    
    public MAVector(final double[] values) {
        this(Array.factory(values));
    }
    
    public MAVector(final int nelems) {
        this.a = new ArrayDouble.D1(nelems);
        this.nelems = nelems;
        this.ima = this.a.getIndex();
    }
    
    public MAVector(final Array a) {
        this.a = a;
        if (a.getRank() != 1) {
            throw new IllegalArgumentException("rank != 1, instead = " + a.getRank());
        }
        this.nelems = a.getShape()[0];
        this.ima = a.getIndex();
    }
    
    public int getNelems() {
        return this.nelems;
    }
    
    public double getDouble(final int i) {
        return this.a.getDouble(this.ima.set(i));
    }
    
    public void setDouble(final int i, final double val) {
        this.a.setDouble(this.ima.set(i), val);
    }
    
    public MAVector copy() {
        return new MAVector(this.a.copy());
    }
    
    public double cos(final MAVector v) {
        if (this.nelems != v.getNelems()) {
            throw new IllegalArgumentException("MAVector.cos " + this.nelems + " != " + v.getNelems());
        }
        final double norm = this.norm();
        final double normV = v.norm();
        if (norm == 0.0 || normV == 0.0) {
            return 0.0;
        }
        return this.dot(v) / (norm * normV);
    }
    
    public double dot(final MAVector v) {
        if (this.nelems != v.getNelems()) {
            throw new IllegalArgumentException("MAVector.dot " + this.nelems + " != " + v.getNelems());
        }
        double sum = 0.0;
        for (int k = 0; k < this.nelems; ++k) {
            sum += this.getDouble(k) * v.getDouble(k);
        }
        return sum;
    }
    
    public double norm() {
        double sum = 0.0;
        for (int k = 0; k < this.nelems; ++k) {
            final double val = this.getDouble(k);
            sum += val * val;
        }
        return Math.sqrt(sum);
    }
    
    public void normalize() {
        final double norm = this.norm();
        if (norm <= 0.0) {
            return;
        }
        for (int k = 0; k < this.nelems; ++k) {
            final double val = this.getDouble(k);
            this.setDouble(k, val / norm);
        }
    }
}
