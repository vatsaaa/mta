// 
// Decompiled by Procyon v0.5.36
// 

package ucar.ma2;

import java.nio.ByteOrder;
import java.nio.ByteBuffer;

public class ArrayStructureBBpos extends ArrayStructureBB
{
    protected int[] positions;
    
    public ArrayStructureBBpos(final StructureMembers members, final int[] shape, final ByteBuffer bbuffer, final int[] positions) {
        super(members, shape, bbuffer, 0);
        this.positions = positions;
    }
    
    @Override
    protected int calcOffsetSetOrder(final int recnum, final StructureMembers.Member m) {
        if (null != m.getDataObject()) {
            this.bbuffer.order((ByteOrder)m.getDataObject());
        }
        return this.positions[recnum] + m.getDataParam();
    }
}
