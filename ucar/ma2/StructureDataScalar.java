// 
// Decompiled by Procyon v0.5.36
// 

package ucar.ma2;

public class StructureDataScalar extends StructureDataW
{
    public StructureDataScalar(final String name) {
        super(new StructureMembers(name));
    }
    
    public void addMember(final String name, final String desc, final String units, final double val) {
        final StructureMembers.Member m = this.members.addMember(name, desc, units, DataType.DOUBLE, new int[0]);
        final ArrayDouble.D0 data = new ArrayDouble.D0();
        data.set(val);
        this.setMemberData(m, data);
    }
    
    public void addMember(final String name, final String desc, final String units, final long val) {
        final StructureMembers.Member m = this.members.addMember(name, desc, units, DataType.LONG, new int[0]);
        final ArrayLong.D0 data = new ArrayLong.D0();
        data.set(val);
        this.setMemberData(m, data);
    }
}
