// 
// Decompiled by Procyon v0.5.36
// 

package ucar.ma2;

import java.io.IOException;

public class SequenceIterator implements StructureDataIterator
{
    private int start;
    private int size;
    private int count;
    private ArrayStructure abb;
    
    public SequenceIterator(final int start, final int size, final ArrayStructure abb) {
        this.start = start;
        this.size = size;
        this.abb = abb;
        this.count = 0;
    }
    
    public boolean hasNext() throws IOException {
        return this.count < this.size;
    }
    
    public StructureData next() throws IOException {
        final StructureData result = this.abb.getStructureData(this.start + this.count);
        ++this.count;
        return result;
    }
    
    public void setBufferSize(final int bytes) {
    }
    
    public StructureDataIterator reset() {
        this.count = 0;
        return this;
    }
    
    public int getCurrentRecno() {
        return this.count;
    }
}
