// 
// Decompiled by Procyon v0.5.36
// 

package ucar.ma2;

import java.nio.ByteBuffer;

public enum DataType
{
    BOOLEAN("boolean", 1), 
    BYTE("byte", 1), 
    CHAR("char", 1), 
    SHORT("short", 2), 
    INT("int", 4), 
    LONG("long", 8), 
    FLOAT("float", 4), 
    DOUBLE("double", 8), 
    SEQUENCE("Sequence", 4), 
    STRING("String", 4), 
    STRUCTURE("Structure", 1), 
    ENUM1("enum1", 1), 
    ENUM2("enum2", 2), 
    ENUM4("enum4", 4), 
    OPAQUE("opaque", 1);
    
    private String niceName;
    private int size;
    
    private DataType(final String s, final int size) {
        this.niceName = s;
        this.size = size;
    }
    
    @Override
    public String toString() {
        return this.niceName;
    }
    
    public int getSize() {
        return this.size;
    }
    
    public Class getClassType() {
        return this.getPrimitiveClassType();
    }
    
    public Class getPrimitiveClassType() {
        if (this == DataType.FLOAT) {
            return Float.TYPE;
        }
        if (this == DataType.DOUBLE) {
            return Double.TYPE;
        }
        if (this == DataType.SHORT || this == DataType.ENUM2) {
            return Short.TYPE;
        }
        if (this == DataType.INT || this == DataType.ENUM4) {
            return Integer.TYPE;
        }
        if (this == DataType.BYTE || this == DataType.ENUM1) {
            return Byte.TYPE;
        }
        if (this == DataType.CHAR) {
            return Character.TYPE;
        }
        if (this == DataType.BOOLEAN) {
            return Boolean.TYPE;
        }
        if (this == DataType.LONG) {
            return Long.TYPE;
        }
        if (this == DataType.STRING) {
            return String.class;
        }
        if (this == DataType.STRUCTURE) {
            return StructureData.class;
        }
        if (this == DataType.SEQUENCE) {
            return StructureDataIterator.class;
        }
        if (this == DataType.OPAQUE) {
            return ByteBuffer.class;
        }
        return null;
    }
    
    public boolean isString() {
        return this == DataType.STRING || this == DataType.CHAR;
    }
    
    public boolean isNumeric() {
        return this == DataType.BYTE || this == DataType.FLOAT || this == DataType.DOUBLE || this == DataType.INT || this == DataType.SHORT || this == DataType.LONG;
    }
    
    public boolean isIntegral() {
        return this == DataType.BYTE || this == DataType.INT || this == DataType.SHORT || this == DataType.LONG;
    }
    
    public boolean isFloatingPoint() {
        return this == DataType.FLOAT || this == DataType.DOUBLE;
    }
    
    public boolean isEnum() {
        return this == DataType.ENUM1 || this == DataType.ENUM2 || this == DataType.ENUM4;
    }
    
    public static DataType getType(final String name) {
        if (name == null) {
            return null;
        }
        try {
            return valueOf(name.toUpperCase());
        }
        catch (IllegalArgumentException e) {
            return null;
        }
    }
    
    public static DataType getType(final Class c) {
        if (c == Float.TYPE || c == Float.class) {
            return DataType.FLOAT;
        }
        if (c == Double.TYPE || c == Double.class) {
            return DataType.DOUBLE;
        }
        if (c == Short.TYPE || c == Short.class) {
            return DataType.SHORT;
        }
        if (c == Integer.TYPE || c == Integer.class) {
            return DataType.INT;
        }
        if (c == Byte.TYPE || c == Byte.class) {
            return DataType.BYTE;
        }
        if (c == Character.TYPE || c == Character.class) {
            return DataType.CHAR;
        }
        if (c == Boolean.TYPE || c == Boolean.class) {
            return DataType.BOOLEAN;
        }
        if (c == Long.TYPE || c == Long.class) {
            return DataType.LONG;
        }
        if (c == String.class) {
            return DataType.STRING;
        }
        if (c == StructureData.class) {
            return DataType.STRUCTURE;
        }
        if (c == StructureDataIterator.class) {
            return DataType.SEQUENCE;
        }
        if (c == ByteBuffer.class) {
            return DataType.OPAQUE;
        }
        return null;
    }
    
    public static long unsignedIntToLong(final int i) {
        return (i < 0) ? (i + 4294967296L) : i;
    }
    
    public static int unsignedShortToInt(final short s) {
        return s & 0xFFFF;
    }
    
    public static short unsignedByteToShort(final byte b) {
        return (short)(b & 0xFF);
    }
}
