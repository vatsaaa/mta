// 
// Decompiled by Procyon v0.5.36
// 

package ucar.ma2;

import java.nio.IntBuffer;
import java.nio.ByteBuffer;

public class ArrayInt extends Array
{
    protected int[] storage;
    
    static ArrayInt factory(final Index index) {
        return factory(index, null);
    }
    
    static ArrayInt factory(final Index index, final int[] storage) {
        switch (index.getRank()) {
            case 0: {
                return new D0(index, storage);
            }
            case 1: {
                return new D1(index, storage);
            }
            case 2: {
                return new D2(index, storage);
            }
            case 3: {
                return new D3(index, storage);
            }
            case 4: {
                return new D4(index, storage);
            }
            case 5: {
                return new D5(index, storage);
            }
            case 6: {
                return new D6(index, storage);
            }
            case 7: {
                return new D7(index, storage);
            }
            default: {
                return new ArrayInt(index, storage);
            }
        }
    }
    
    public ArrayInt(final int[] dimensions) {
        super(dimensions);
        this.storage = new int[(int)this.indexCalc.getSize()];
    }
    
    ArrayInt(final Index ima, final int[] data) {
        super(ima);
        if (data != null) {
            this.storage = data;
        }
        else {
            this.storage = new int[(int)ima.getSize()];
        }
    }
    
    @Override
    Array createView(final Index index) {
        final Array result = factory(index, this.storage);
        result.setUnsigned(this.isUnsigned());
        return result;
    }
    
    @Override
    public Object getStorage() {
        return this.storage;
    }
    
    @Override
    void copyFrom1DJavaArray(final IndexIterator iter, final Object javaArray) {
        final int[] ja = (int[])javaArray;
        for (int i = 0; i < ja.length; ++i) {
            iter.setIntNext(ja[i]);
        }
    }
    
    @Override
    void copyTo1DJavaArray(final IndexIterator iter, final Object javaArray) {
        final int[] ja = (int[])javaArray;
        for (int i = 0; i < ja.length; ++i) {
            ja[i] = iter.getIntNext();
        }
    }
    
    @Override
    public ByteBuffer getDataAsByteBuffer() {
        final ByteBuffer bb = ByteBuffer.allocate((int)(4L * this.getSize()));
        final IntBuffer ib = bb.asIntBuffer();
        ib.put((int[])this.get1DJavaArray(Integer.TYPE));
        return bb;
    }
    
    @Override
    public Class getElementType() {
        return Integer.TYPE;
    }
    
    public int get(final Index i) {
        return this.storage[i.currentElement()];
    }
    
    public void set(final Index i, final int value) {
        this.storage[i.currentElement()] = value;
    }
    
    @Override
    public double getDouble(final Index i) {
        final int val = this.storage[i.currentElement()];
        return (double)(this.unsigned ? DataType.unsignedIntToLong(val) : val);
    }
    
    @Override
    public void setDouble(final Index i, final double value) {
        this.storage[i.currentElement()] = (int)value;
    }
    
    @Override
    public float getFloat(final Index i) {
        final int val = this.storage[i.currentElement()];
        return (float)(this.unsigned ? DataType.unsignedIntToLong(val) : val);
    }
    
    @Override
    public void setFloat(final Index i, final float value) {
        this.storage[i.currentElement()] = (int)value;
    }
    
    @Override
    public long getLong(final Index i) {
        final int val = this.storage[i.currentElement()];
        return this.unsigned ? DataType.unsignedIntToLong(val) : val;
    }
    
    @Override
    public void setLong(final Index i, final long value) {
        this.storage[i.currentElement()] = (int)value;
    }
    
    @Override
    public int getInt(final Index i) {
        return this.storage[i.currentElement()];
    }
    
    @Override
    public void setInt(final Index i, final int value) {
        this.storage[i.currentElement()] = value;
    }
    
    @Override
    public short getShort(final Index i) {
        return (short)this.storage[i.currentElement()];
    }
    
    @Override
    public void setShort(final Index i, final short value) {
        this.storage[i.currentElement()] = value;
    }
    
    @Override
    public byte getByte(final Index i) {
        return (byte)this.storage[i.currentElement()];
    }
    
    @Override
    public void setByte(final Index i, final byte value) {
        this.storage[i.currentElement()] = value;
    }
    
    @Override
    public char getChar(final Index i) {
        return (char)this.storage[i.currentElement()];
    }
    
    @Override
    public void setChar(final Index i, final char value) {
        this.storage[i.currentElement()] = value;
    }
    
    @Override
    public boolean getBoolean(final Index i) {
        throw new ForbiddenConversionException();
    }
    
    @Override
    public void setBoolean(final Index i, final boolean value) {
        throw new ForbiddenConversionException();
    }
    
    @Override
    public Object getObject(final Index i) {
        return this.storage[i.currentElement()];
    }
    
    @Override
    public void setObject(final Index i, final Object value) {
        this.storage[i.currentElement()] = ((Number)value).intValue();
    }
    
    @Override
    public double getDouble(final int index) {
        final int val = this.storage[index];
        return (double)(this.unsigned ? DataType.unsignedIntToLong(val) : val);
    }
    
    @Override
    public void setDouble(final int index, final double value) {
        this.storage[index] = (int)value;
    }
    
    @Override
    public float getFloat(final int index) {
        final int val = this.storage[index];
        return (float)(this.unsigned ? DataType.unsignedIntToLong(val) : val);
    }
    
    @Override
    public void setFloat(final int index, final float value) {
        this.storage[index] = (int)value;
    }
    
    @Override
    public long getLong(final int index) {
        final int val = this.storage[index];
        return this.unsigned ? DataType.unsignedIntToLong(val) : val;
    }
    
    @Override
    public void setLong(final int index, final long value) {
        this.storage[index] = (int)value;
    }
    
    @Override
    public int getInt(final int index) {
        return this.storage[index];
    }
    
    @Override
    public void setInt(final int index, final int value) {
        this.storage[index] = value;
    }
    
    @Override
    public short getShort(final int index) {
        return (short)this.storage[index];
    }
    
    @Override
    public void setShort(final int index, final short value) {
        this.storage[index] = value;
    }
    
    @Override
    public byte getByte(final int index) {
        return (byte)this.storage[index];
    }
    
    @Override
    public void setByte(final int index, final byte value) {
        this.storage[index] = value;
    }
    
    @Override
    public char getChar(final int index) {
        return (char)this.storage[index];
    }
    
    @Override
    public void setChar(final int index, final char value) {
        this.storage[index] = value;
    }
    
    @Override
    public boolean getBoolean(final int index) {
        throw new ForbiddenConversionException();
    }
    
    @Override
    public void setBoolean(final int index, final boolean value) {
        throw new ForbiddenConversionException();
    }
    
    @Override
    public Object getObject(final int index) {
        return this.getInt(index);
    }
    
    @Override
    public void setObject(final int index, final Object value) {
        this.storage[index] = ((Number)value).intValue();
    }
    
    public static class D0 extends ArrayInt
    {
        private Index0D ix;
        
        public D0() {
            super(new int[0]);
            this.ix = (Index0D)this.indexCalc;
        }
        
        private D0(final Index i, final int[] store) {
            super(i, store);
            this.ix = (Index0D)this.indexCalc;
        }
        
        public int get() {
            return this.storage[this.ix.currentElement()];
        }
        
        public void set(final int value) {
            this.storage[this.ix.currentElement()] = value;
        }
    }
    
    public static class D1 extends ArrayInt
    {
        private Index1D ix;
        
        public D1(final int len0) {
            super(new int[] { len0 });
            this.ix = (Index1D)this.indexCalc;
        }
        
        private D1(final Index i, final int[] store) {
            super(i, store);
            this.ix = (Index1D)this.indexCalc;
        }
        
        public int get(final int i) {
            return this.storage[this.ix.setDirect(i)];
        }
        
        public void set(final int i, final int value) {
            this.storage[this.ix.setDirect(i)] = value;
        }
    }
    
    public static class D2 extends ArrayInt
    {
        private Index2D ix;
        
        public D2(final int len0, final int len1) {
            super(new int[] { len0, len1 });
            this.ix = (Index2D)this.indexCalc;
        }
        
        private D2(final Index i, final int[] store) {
            super(i, store);
            this.ix = (Index2D)this.indexCalc;
        }
        
        public int get(final int i, final int j) {
            return this.storage[this.ix.setDirect(i, j)];
        }
        
        public void set(final int i, final int j, final int value) {
            this.storage[this.ix.setDirect(i, j)] = value;
        }
    }
    
    public static class D3 extends ArrayInt
    {
        private Index3D ix;
        
        public D3(final int len0, final int len1, final int len2) {
            super(new int[] { len0, len1, len2 });
            this.ix = (Index3D)this.indexCalc;
        }
        
        private D3(final Index i, final int[] store) {
            super(i, store);
            this.ix = (Index3D)this.indexCalc;
        }
        
        public int get(final int i, final int j, final int k) {
            return this.storage[this.ix.setDirect(i, j, k)];
        }
        
        public void set(final int i, final int j, final int k, final int value) {
            this.storage[this.ix.setDirect(i, j, k)] = value;
        }
    }
    
    public static class D4 extends ArrayInt
    {
        private Index4D ix;
        
        public D4(final int len0, final int len1, final int len2, final int len3) {
            super(new int[] { len0, len1, len2, len3 });
            this.ix = (Index4D)this.indexCalc;
        }
        
        private D4(final Index i, final int[] store) {
            super(i, store);
            this.ix = (Index4D)this.indexCalc;
        }
        
        public int get(final int i, final int j, final int k, final int l) {
            return this.storage[this.ix.setDirect(i, j, k, l)];
        }
        
        public void set(final int i, final int j, final int k, final int l, final int value) {
            this.storage[this.ix.setDirect(i, j, k, l)] = value;
        }
    }
    
    public static class D5 extends ArrayInt
    {
        private Index5D ix;
        
        public D5(final int len0, final int len1, final int len2, final int len3, final int len4) {
            super(new int[] { len0, len1, len2, len3, len4 });
            this.ix = (Index5D)this.indexCalc;
        }
        
        private D5(final Index i, final int[] store) {
            super(i, store);
            this.ix = (Index5D)this.indexCalc;
        }
        
        public int get(final int i, final int j, final int k, final int l, final int m) {
            return this.storage[this.ix.setDirect(i, j, k, l, m)];
        }
        
        public void set(final int i, final int j, final int k, final int l, final int m, final int value) {
            this.storage[this.ix.setDirect(i, j, k, l, m)] = value;
        }
    }
    
    public static class D6 extends ArrayInt
    {
        private Index6D ix;
        
        public D6(final int len0, final int len1, final int len2, final int len3, final int len4, final int len5) {
            super(new int[] { len0, len1, len2, len3, len4, len5 });
            this.ix = (Index6D)this.indexCalc;
        }
        
        private D6(final Index i, final int[] store) {
            super(i, store);
            this.ix = (Index6D)this.indexCalc;
        }
        
        public int get(final int i, final int j, final int k, final int l, final int m, final int n) {
            return this.storage[this.ix.setDirect(i, j, k, l, m, n)];
        }
        
        public void set(final int i, final int j, final int k, final int l, final int m, final int n, final int value) {
            this.storage[this.ix.setDirect(i, j, k, l, m, n)] = value;
        }
    }
    
    public static class D7 extends ArrayInt
    {
        private Index7D ix;
        
        public D7(final int len0, final int len1, final int len2, final int len3, final int len4, final int len5, final int len6) {
            super(new int[] { len0, len1, len2, len3, len4, len5, len6 });
            this.ix = (Index7D)this.indexCalc;
        }
        
        private D7(final Index i, final int[] store) {
            super(i, store);
            this.ix = (Index7D)this.indexCalc;
        }
        
        public int get(final int i, final int j, final int k, final int l, final int m, final int n, final int o) {
            return this.storage[this.ix.setDirect(i, j, k, l, m, n, o)];
        }
        
        public void set(final int i, final int j, final int k, final int l, final int m, final int n, final int o, final int value) {
            this.storage[this.ix.setDirect(i, j, k, l, m, n, o)] = value;
        }
    }
}
