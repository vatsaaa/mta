// 
// Decompiled by Procyon v0.5.36
// 

package ucar.ma2;

import ucar.nc2.util.Misc;

public class MAMath
{
    public static Array add(final Array a, final Array b) throws IllegalArgumentException {
        final Array result = Array.factory(a.getElementType(), a.getShape());
        if (a.getElementType() == Double.TYPE) {
            addDouble(result, a, b);
            return result;
        }
        throw new UnsupportedOperationException();
    }
    
    public static void addDouble(final Array result, final Array a, final Array b) throws IllegalArgumentException {
        if (!conformable(result, a) || !conformable(a, b)) {
            throw new IllegalArgumentException();
        }
        final IndexIterator iterR = result.getIndexIterator();
        final IndexIterator iterA = a.getIndexIterator();
        final IndexIterator iterB = b.getIndexIterator();
        while (iterA.hasNext()) {
            iterR.setDoubleNext(iterA.getDoubleNext() + iterB.getDoubleNext());
        }
    }
    
    public static boolean conformable(final Array a, final Array b) {
        return conformable(a.getShape(), b.getShape());
    }
    
    public static boolean conformable(final int[] shapeA, final int[] shapeB) {
        if (reducedRank(shapeA) != reducedRank(shapeB)) {
            return false;
        }
        final int rankA = shapeA.length;
        final int rankB = shapeB.length;
        int dimB = 0;
        for (int dimA = 0; dimA < rankA; ++dimA) {
            if (shapeA[dimA] != 1) {
                while (dimB < rankB && shapeB[dimB] == 1) {
                    ++dimB;
                }
                if (shapeA[dimA] != shapeB[dimB]) {
                    return false;
                }
                ++dimB;
            }
        }
        return true;
    }
    
    public static Array convertUnsigned(final Array unsigned) {
        if (unsigned.getElementType().equals(Byte.TYPE)) {
            final Array result = Array.factory(DataType.SHORT, unsigned.getShape());
            final IndexIterator ii = result.getIndexIterator();
            unsigned.resetLocalIterator();
            while (unsigned.hasNext()) {
                ii.setShortNext(DataType.unsignedByteToShort(unsigned.nextByte()));
            }
            return result;
        }
        if (unsigned.getElementType().equals(Short.TYPE)) {
            final Array result = Array.factory(DataType.INT, unsigned.getShape());
            final IndexIterator ii = result.getIndexIterator();
            unsigned.resetLocalIterator();
            while (unsigned.hasNext()) {
                ii.setIntNext(DataType.unsignedShortToInt(unsigned.nextShort()));
            }
            return result;
        }
        if (unsigned.getElementType().equals(Integer.TYPE)) {
            final Array result = Array.factory(DataType.LONG, unsigned.getShape());
            final IndexIterator ii = result.getIndexIterator();
            unsigned.resetLocalIterator();
            while (unsigned.hasNext()) {
                ii.setLongNext(DataType.unsignedIntToLong(unsigned.nextInt()));
            }
            return result;
        }
        throw new IllegalArgumentException("Cant convertUnsigned type= " + unsigned.getElementType());
    }
    
    public static Array convert(final Array org, final DataType wantType) {
        if (org == null) {
            return null;
        }
        final Class wantClass = wantType.getPrimitiveClassType();
        if (org.getElementType().equals(wantClass)) {
            return org;
        }
        final Array result = Array.factory(wantType, org.getShape());
        copy(wantType, org.getIndexIterator(), result.getIndexIterator());
        return result;
    }
    
    public static void copy(final DataType dataType, final IndexIterator from, final IndexIterator to) throws IllegalArgumentException {
        if (dataType == DataType.DOUBLE) {
            while (from.hasNext()) {
                to.setDoubleNext(from.getDoubleNext());
            }
        }
        else if (dataType == DataType.FLOAT) {
            while (from.hasNext()) {
                to.setFloatNext(from.getFloatNext());
            }
        }
        else if (dataType == DataType.LONG) {
            while (from.hasNext()) {
                to.setLongNext(from.getLongNext());
            }
        }
        else if (dataType == DataType.INT || dataType == DataType.ENUM4) {
            while (from.hasNext()) {
                to.setIntNext(from.getIntNext());
            }
        }
        else if (dataType == DataType.SHORT || dataType == DataType.ENUM2) {
            while (from.hasNext()) {
                to.setShortNext(from.getShortNext());
            }
        }
        else if (dataType == DataType.CHAR) {
            while (from.hasNext()) {
                to.setCharNext(from.getCharNext());
            }
        }
        else if (dataType == DataType.BYTE || dataType == DataType.ENUM1) {
            while (from.hasNext()) {
                to.setByteNext(from.getByteNext());
            }
        }
        else if (dataType == DataType.BOOLEAN) {
            while (from.hasNext()) {
                to.setBooleanNext(from.getBooleanNext());
            }
        }
        else {
            while (from.hasNext()) {
                to.setObjectNext(from.getObjectNext());
            }
        }
    }
    
    public static void copy(final Array result, final Array a) throws IllegalArgumentException {
        final Class classType = a.getElementType();
        if (classType == Double.TYPE) {
            copyDouble(result, a);
        }
        else if (classType == Float.TYPE) {
            copyFloat(result, a);
        }
        else if (classType == Long.TYPE) {
            copyLong(result, a);
        }
        else if (classType == Integer.TYPE) {
            copyInt(result, a);
        }
        else if (classType == Short.TYPE) {
            copyShort(result, a);
        }
        else if (classType == Character.TYPE) {
            copyChar(result, a);
        }
        else if (classType == Byte.TYPE) {
            copyByte(result, a);
        }
        else if (classType == Boolean.TYPE) {
            copyBoolean(result, a);
        }
        else {
            copyObject(result, a);
        }
    }
    
    public static void copyDouble(final Array result, final Array a) throws IllegalArgumentException {
        if (!conformable(a, result)) {
            throw new IllegalArgumentException("copy arrays are not conformable");
        }
        final IndexIterator iterA = a.getIndexIterator();
        final IndexIterator iterR = result.getIndexIterator();
        while (iterA.hasNext()) {
            iterR.setDoubleNext(iterA.getDoubleNext());
        }
    }
    
    public static void copyFloat(final Array result, final Array a) throws IllegalArgumentException {
        if (!conformable(a, result)) {
            throw new IllegalArgumentException("copy arrays are not conformable");
        }
        final IndexIterator iterA = a.getIndexIterator();
        final IndexIterator iterR = result.getIndexIterator();
        while (iterA.hasNext()) {
            iterR.setFloatNext(iterA.getFloatNext());
        }
    }
    
    public static void copyLong(final Array result, final Array a) throws IllegalArgumentException {
        if (!conformable(a, result)) {
            throw new IllegalArgumentException("copy arrays are not conformable");
        }
        final IndexIterator iterA = a.getIndexIterator();
        final IndexIterator iterR = result.getIndexIterator();
        while (iterA.hasNext()) {
            iterR.setLongNext(iterA.getLongNext());
        }
    }
    
    public static void copyInt(final Array result, final Array a) throws IllegalArgumentException {
        if (!conformable(a, result)) {
            throw new IllegalArgumentException("copy arrays are not conformable");
        }
        final IndexIterator iterA = a.getIndexIterator();
        final IndexIterator iterR = result.getIndexIterator();
        while (iterA.hasNext()) {
            iterR.setIntNext(iterA.getIntNext());
        }
    }
    
    public static void copyShort(final Array result, final Array a) throws IllegalArgumentException {
        if (!conformable(a, result)) {
            throw new IllegalArgumentException("copy arrays are not conformable");
        }
        final IndexIterator iterA = a.getIndexIterator();
        final IndexIterator iterR = result.getIndexIterator();
        while (iterA.hasNext()) {
            iterR.setShortNext(iterA.getShortNext());
        }
    }
    
    public static void copyChar(final Array result, final Array a) throws IllegalArgumentException {
        if (!conformable(a, result)) {
            throw new IllegalArgumentException("copy arrays are not conformable");
        }
        final IndexIterator iterA = a.getIndexIterator();
        final IndexIterator iterR = result.getIndexIterator();
        while (iterA.hasNext()) {
            iterR.setCharNext(iterA.getCharNext());
        }
    }
    
    public static void copyByte(final Array result, final Array a) throws IllegalArgumentException {
        if (!conformable(a, result)) {
            throw new IllegalArgumentException("copy arrays are not conformable");
        }
        final IndexIterator iterA = a.getIndexIterator();
        final IndexIterator iterR = result.getIndexIterator();
        while (iterA.hasNext()) {
            iterR.setByteNext(iterA.getByteNext());
        }
    }
    
    public static void copyBoolean(final Array result, final Array a) throws IllegalArgumentException {
        if (!conformable(a, result)) {
            throw new IllegalArgumentException("copy arrays are not conformable");
        }
        final IndexIterator iterA = a.getIndexIterator();
        final IndexIterator iterR = result.getIndexIterator();
        while (iterA.hasNext()) {
            iterR.setBooleanNext(iterA.getBooleanNext());
        }
    }
    
    public static void copyObject(final Array result, final Array a) throws IllegalArgumentException {
        if (!conformable(a, result)) {
            throw new IllegalArgumentException("copy arrays are not conformable");
        }
        final IndexIterator iterA = a.getIndexIterator();
        final IndexIterator iterR = result.getIndexIterator();
        while (iterA.hasNext()) {
            iterR.setObjectNext(iterA.getObjectNext());
        }
    }
    
    public static int reducedRank(final int[] shape) {
        int rank = 0;
        for (int ii = 0; ii < shape.length; ++ii) {
            if (shape[ii] > 1) {
                ++rank;
            }
        }
        return rank;
    }
    
    public static double getMinimum(final Array a) {
        final IndexIterator iter = a.getIndexIterator();
        double min = Double.MAX_VALUE;
        while (iter.hasNext()) {
            final double val = iter.getDoubleNext();
            if (Double.isNaN(val)) {
                continue;
            }
            if (val >= min) {
                continue;
            }
            min = val;
        }
        return min;
    }
    
    public static double getMaximum(final Array a) {
        final IndexIterator iter = a.getIndexIterator();
        double max = -1.7976931348623157E308;
        while (iter.hasNext()) {
            final double val = iter.getDoubleNext();
            if (Double.isNaN(val)) {
                continue;
            }
            if (val <= max) {
                continue;
            }
            max = val;
        }
        return max;
    }
    
    public static MinMax getMinMax(final Array a) {
        final IndexIterator iter = a.getIndexIterator();
        double max = -1.7976931348623157E308;
        double min = Double.MAX_VALUE;
        while (iter.hasNext()) {
            final double val = iter.getDoubleNext();
            if (Double.isNaN(val)) {
                continue;
            }
            if (val > max) {
                max = val;
            }
            if (val >= min) {
                continue;
            }
            min = val;
        }
        return new MinMax(min, max);
    }
    
    public static double getMinimumSkipMissingData(final Array a, final double missingValue) {
        final IndexIterator iter = a.getIndexIterator();
        double min = Double.MAX_VALUE;
        while (iter.hasNext()) {
            final double val = iter.getDoubleNext();
            if (val != missingValue && val < min) {
                min = val;
            }
        }
        return min;
    }
    
    public static double getMaximumSkipMissingData(final Array a, final double missingValue) {
        final IndexIterator iter = a.getIndexIterator();
        double max = -1.7976931348623157E308;
        while (iter.hasNext()) {
            final double val = iter.getDoubleNext();
            if (val != missingValue && val > max) {
                max = val;
            }
        }
        return max;
    }
    
    public static MinMax getMinMaxSkipMissingData(final Array a, final double missingValue) {
        final IndexIterator iter = a.getIndexIterator();
        double max = -1.7976931348623157E308;
        double min = Double.MAX_VALUE;
        while (iter.hasNext()) {
            final double val = iter.getDoubleNext();
            if (val == missingValue) {
                continue;
            }
            if (val > max) {
                max = val;
            }
            if (val >= min) {
                continue;
            }
            min = val;
        }
        return new MinMax(min, max);
    }
    
    public static void setDouble(final Array result, final double val) {
        final IndexIterator iter = result.getIndexIterator();
        while (iter.hasNext()) {
            iter.setDoubleNext(val);
        }
    }
    
    public static double sumDouble(final Array a) {
        double sum = 0.0;
        final IndexIterator iterA = a.getIndexIterator();
        while (iterA.hasNext()) {
            sum += iterA.getDoubleNext();
        }
        return sum;
    }
    
    public static double sumDoubleSkipMissingData(final Array a, final double missingValue) {
        double sum = 0.0;
        final IndexIterator iterA = a.getIndexIterator();
        while (iterA.hasNext()) {
            final double val = iterA.getDoubleNext();
            if (val != missingValue) {
                if (Double.isNaN(val)) {
                    continue;
                }
                sum += val;
            }
        }
        return sum;
    }
    
    public static ScaleOffset calcScaleOffsetSkipMissingData(final Array a, final double missingValue, final int nbits, final boolean isUnsigned) {
        final MinMax minmax = getMinMaxSkipMissingData(a, missingValue);
        if (isUnsigned) {
            final long size = (1 << nbits) - 1;
            final double offset = minmax.min;
            final double scale = (minmax.max - minmax.min) / size;
            return new ScaleOffset(scale, offset);
        }
        final long size = (1 << nbits) - 2;
        final double offset = (minmax.max + minmax.min) / 2.0;
        final double scale = (minmax.max - minmax.min) / size;
        return new ScaleOffset(scale, offset);
    }
    
    public static Array convert2packed(final Array unpacked, final double missingValue, final int nbits, final boolean isUnsigned, final DataType packedType) {
        final ScaleOffset scaleOffset = calcScaleOffsetSkipMissingData(unpacked, missingValue, nbits, isUnsigned);
        final Array result = Array.factory(packedType, unpacked.getShape());
        final IndexIterator riter = result.getIndexIterator();
        while (unpacked.hasNext()) {
            final double uv = unpacked.nextDouble();
            final double pv = (uv - scaleOffset.offset) / scaleOffset.scale;
            riter.setDoubleNext(pv);
        }
        return result;
    }
    
    public static Array convert2Unpacked(final Array packed, final ScaleOffset scaleOffset) {
        final Array result = Array.factory(DataType.DOUBLE, packed.getShape());
        final IndexIterator riter = result.getIndexIterator();
        while (packed.hasNext()) {
            riter.setDoubleNext(packed.nextDouble() * scaleOffset.scale + scaleOffset.offset);
        }
        return result;
    }
    
    public static boolean isEqual(final Array data1, final Array data2) {
        if (data1.getSize() != data2.getSize()) {
            return false;
        }
        final DataType dt = DataType.getType(data1.getElementType());
        final IndexIterator iter1 = data1.getIndexIterator();
        final IndexIterator iter2 = data2.getIndexIterator();
        if (dt == DataType.DOUBLE) {
            while (iter1.hasNext() && iter2.hasNext()) {
                final double v1 = iter1.getDoubleNext();
                final double v2 = iter2.getDoubleNext();
                if ((!Double.isNaN(v1) || !Double.isNaN(v2)) && !Misc.closeEnough(v1, v2, 1.0E-8)) {
                    return false;
                }
            }
        }
        else if (dt == DataType.FLOAT) {
            while (iter1.hasNext() && iter2.hasNext()) {
                final float v3 = iter1.getFloatNext();
                final float v4 = iter2.getFloatNext();
                if ((!Float.isNaN(v3) || !Float.isNaN(v4)) && !Misc.closeEnough(v3, v4, 1.0E-5)) {
                    return false;
                }
            }
        }
        else if (dt == DataType.INT) {
            while (iter1.hasNext() && iter2.hasNext()) {
                final int v5 = iter1.getIntNext();
                final int v6 = iter2.getIntNext();
                if (v5 != v6) {
                    return false;
                }
            }
        }
        else if (dt == DataType.SHORT) {
            while (iter1.hasNext() && iter2.hasNext()) {
                final short v7 = iter1.getShortNext();
                final short v8 = iter2.getShortNext();
                if (v7 != v8) {
                    return false;
                }
            }
        }
        else if (dt == DataType.BYTE) {
            while (iter1.hasNext() && iter2.hasNext()) {
                final byte v9 = iter1.getByteNext();
                final byte v10 = iter2.getByteNext();
                if (v9 != v10) {
                    return false;
                }
            }
        }
        else if (dt == DataType.LONG) {
            while (iter1.hasNext() && iter2.hasNext()) {
                final long v11 = iter1.getLongNext();
                final long v12 = iter2.getLongNext();
                if (v11 != v12) {
                    return false;
                }
            }
        }
        return true;
    }
    
    public static class MinMax
    {
        public double min;
        public double max;
        
        public MinMax(final double min, final double max) {
            this.min = min;
            this.max = max;
        }
        
        @Override
        public String toString() {
            return "MinMax{min=" + this.min + ", max=" + this.max + '}';
        }
    }
    
    public static class ScaleOffset
    {
        public double scale;
        public double offset;
        public boolean isUnsigned;
        
        public ScaleOffset(final double scale, final double offset) {
            this.scale = scale;
            this.offset = offset;
        }
    }
}
