// 
// Decompiled by Procyon v0.5.36
// 

package ucar.ma2;

import java.util.Formatter;
import java.util.Iterator;
import java.util.ArrayList;
import java.io.IOException;

public class ArraySequence extends ArrayStructure
{
    private StructureDataIterator iter;
    private int initial;
    
    protected ArraySequence(final StructureMembers sm, final int[] shape) {
        super(sm, shape);
        this.initial = 1000;
    }
    
    public ArraySequence(final StructureMembers members, final StructureDataIterator iter, final int nelems) {
        super(members, new int[] { 0 });
        this.initial = 1000;
        this.iter = iter;
        this.nelems = nelems;
    }
    
    @Override
    public Class getElementType() {
        return StructureDataIterator.class;
    }
    
    @Override
    public StructureDataIterator getStructureDataIterator() throws IOException {
        return this.iter = this.iter.reset();
    }
    
    public int getStructureDataCount() {
        return this.nelems;
    }
    
    @Override
    public long getSizeBytes() {
        return this.nelems * this.members.getStructureSize();
    }
    
    @Override
    protected StructureData makeStructureData(final ArrayStructure as, final int index) {
        throw new UnsupportedOperationException("Cannot subset a Sequence");
    }
    
    @Override
    public Array extractMemberArray(final StructureMembers.Member proxym) throws IOException {
        if (proxym.getDataArray() != null) {
            return proxym.getDataArray();
        }
        final DataType dataType = proxym.getDataType();
        final boolean isScalar = proxym.getSize() == 1 || dataType == DataType.SEQUENCE;
        final int[] mshape = proxym.getShape();
        final int rrank = 1 + mshape.length;
        final int[] rshape = new int[rrank];
        rshape[0] = this.nelems;
        System.arraycopy(mshape, 0, rshape, 1, mshape.length);
        if (this.nelems < 0) {
            return this.extractMemberArrayFromIteration(proxym, rshape);
        }
        Array result;
        if (dataType == DataType.STRUCTURE) {
            final StructureMembers membersw = new StructureMembers(proxym.getStructureMembers());
            result = new ArrayStructureW(membersw, rshape);
        }
        else {
            result = Array.factory(dataType.getPrimitiveClassType(), rshape);
        }
        final StructureDataIterator sdataIter = this.getStructureDataIterator();
        final IndexIterator resultIter = result.getIndexIterator();
        while (sdataIter.hasNext()) {
            final StructureData sdata = sdataIter.next();
            final StructureMembers.Member realm = sdata.getStructureMembers().findMember(proxym.getName());
            if (isScalar) {
                if (dataType == DataType.DOUBLE) {
                    resultIter.setDoubleNext(sdata.getScalarDouble(realm));
                }
                else if (dataType == DataType.FLOAT) {
                    resultIter.setFloatNext(sdata.getScalarFloat(realm));
                }
                else if (dataType == DataType.BYTE || dataType == DataType.ENUM1) {
                    resultIter.setByteNext(sdata.getScalarByte(realm));
                }
                else if (dataType == DataType.SHORT || dataType == DataType.ENUM2) {
                    resultIter.setShortNext(sdata.getScalarShort(realm));
                }
                else if (dataType == DataType.INT || dataType == DataType.ENUM4) {
                    resultIter.setIntNext(sdata.getScalarInt(realm));
                }
                else if (dataType == DataType.LONG) {
                    resultIter.setLongNext(sdata.getScalarLong(realm));
                }
                else if (dataType == DataType.CHAR) {
                    resultIter.setCharNext(sdata.getScalarChar(realm));
                }
                else if (dataType == DataType.STRING) {
                    resultIter.setObjectNext(sdata.getScalarString(realm));
                }
                else if (dataType == DataType.STRUCTURE) {
                    resultIter.setObjectNext(sdata.getScalarStructure(realm));
                }
                else {
                    if (dataType != DataType.SEQUENCE) {
                        continue;
                    }
                    resultIter.setObjectNext(sdata.getArraySequence(realm));
                }
            }
            else if (dataType == DataType.DOUBLE) {
                final double[] arr$;
                final double[] data = arr$ = sdata.getJavaArrayDouble(realm);
                for (final double aData : arr$) {
                    resultIter.setDoubleNext(aData);
                }
            }
            else if (dataType == DataType.FLOAT) {
                final float[] arr$2;
                final float[] data2 = arr$2 = sdata.getJavaArrayFloat(realm);
                for (final float aData2 : arr$2) {
                    resultIter.setFloatNext(aData2);
                }
            }
            else if (dataType == DataType.BYTE || dataType == DataType.ENUM1) {
                final byte[] arr$3;
                final byte[] data3 = arr$3 = sdata.getJavaArrayByte(realm);
                for (final byte aData3 : arr$3) {
                    resultIter.setByteNext(aData3);
                }
            }
            else if (dataType == DataType.SHORT || dataType == DataType.ENUM2) {
                final short[] arr$4;
                final short[] data4 = arr$4 = sdata.getJavaArrayShort(realm);
                for (final short aData4 : arr$4) {
                    resultIter.setShortNext(aData4);
                }
            }
            else if (dataType == DataType.INT || dataType == DataType.ENUM4) {
                final int[] arr$5;
                final int[] data5 = arr$5 = sdata.getJavaArrayInt(realm);
                for (final int aData5 : arr$5) {
                    resultIter.setIntNext(aData5);
                }
            }
            else if (dataType == DataType.LONG) {
                final long[] arr$6;
                final long[] data6 = arr$6 = sdata.getJavaArrayLong(realm);
                for (final long aData6 : arr$6) {
                    resultIter.setLongNext(aData6);
                }
            }
            else if (dataType == DataType.CHAR) {
                final char[] arr$7;
                final char[] data7 = arr$7 = sdata.getJavaArrayChar(realm);
                for (final char aData7 : arr$7) {
                    resultIter.setCharNext(aData7);
                }
            }
            else if (dataType == DataType.STRING) {
                final String[] arr$8;
                final String[] data8 = arr$8 = sdata.getJavaArrayString(realm);
                for (final String aData8 : arr$8) {
                    resultIter.setObjectNext(aData8);
                }
            }
            else {
                if (dataType != DataType.STRUCTURE) {
                    continue;
                }
                final ArrayStructure as = sdata.getArrayStructure(realm);
                final StructureDataIterator innerIter = as.getStructureDataIterator();
                while (innerIter.hasNext()) {
                    resultIter.setObjectNext(innerIter.next());
                }
            }
        }
        return result;
    }
    
    private Array extractMemberArrayFromIteration(final StructureMembers.Member proxym, final int[] rshape) throws IOException {
        final DataType dataType = proxym.getDataType();
        final StructureDataIterator sdataIter = this.getStructureDataIterator();
        Object dataArray = null;
        int count = 0;
        if (dataType == DataType.DOUBLE) {
            final ArrayList<Double> result = new ArrayList<Double>(this.initial);
            while (sdataIter.hasNext()) {
                final StructureData sdata = sdataIter.next();
                final StructureMembers.Member realm = sdata.getStructureMembers().findMember(proxym.getName());
                final double[] arr$;
                final double[] data = arr$ = sdata.getJavaArrayDouble(realm);
                for (final double aData : arr$) {
                    result.add(aData);
                }
                ++count;
            }
            final double[] da = new double[result.size()];
            int i = 0;
            for (final Double d : result) {
                da[i++] = d;
            }
            dataArray = da;
        }
        else if (dataType == DataType.FLOAT) {
            final ArrayList<Float> result2 = new ArrayList<Float>(this.initial);
            while (sdataIter.hasNext()) {
                final StructureData sdata = sdataIter.next();
                final StructureMembers.Member realm = sdata.getStructureMembers().findMember(proxym.getName());
                final float[] arr$2;
                final float[] data2 = arr$2 = sdata.getJavaArrayFloat(realm);
                for (final float aData2 : arr$2) {
                    result2.add(aData2);
                }
                ++count;
            }
            final float[] da2 = new float[result2.size()];
            int i = 0;
            for (final Float d2 : result2) {
                da2[i++] = d2;
            }
            dataArray = da2;
        }
        else if (dataType == DataType.BYTE || dataType == DataType.ENUM1) {
            final ArrayList<Byte> result3 = new ArrayList<Byte>(this.initial);
            while (sdataIter.hasNext()) {
                final StructureData sdata = sdataIter.next();
                final StructureMembers.Member realm = sdata.getStructureMembers().findMember(proxym.getName());
                final byte[] arr$3;
                final byte[] data3 = arr$3 = sdata.getJavaArrayByte(realm);
                for (final byte aData3 : arr$3) {
                    result3.add(aData3);
                }
                ++count;
            }
            final byte[] da3 = new byte[result3.size()];
            int i = 0;
            for (final Byte d3 : result3) {
                da3[i++] = d3;
            }
            dataArray = da3;
        }
        else if (dataType == DataType.SHORT || dataType == DataType.ENUM2) {
            final ArrayList<Short> result4 = new ArrayList<Short>(this.initial);
            while (sdataIter.hasNext()) {
                final StructureData sdata = sdataIter.next();
                final StructureMembers.Member realm = sdata.getStructureMembers().findMember(proxym.getName());
                final short[] arr$4;
                final short[] data4 = arr$4 = sdata.getJavaArrayShort(realm);
                for (final short aData4 : arr$4) {
                    result4.add(aData4);
                }
                ++count;
            }
            final short[] da4 = new short[result4.size()];
            int i = 0;
            for (final Short d4 : result4) {
                da4[i++] = d4;
            }
            dataArray = da4;
        }
        else if (dataType == DataType.INT || dataType == DataType.ENUM4) {
            final ArrayList<Integer> result5 = new ArrayList<Integer>(this.initial);
            while (sdataIter.hasNext()) {
                final StructureData sdata = sdataIter.next();
                final StructureMembers.Member realm = sdata.getStructureMembers().findMember(proxym.getName());
                final int[] arr$5;
                final int[] data5 = arr$5 = sdata.getJavaArrayInt(realm);
                for (final int aData5 : arr$5) {
                    result5.add(aData5);
                }
                ++count;
            }
            final int[] da5 = new int[result5.size()];
            int i = 0;
            for (final Integer d5 : result5) {
                da5[i++] = d5;
            }
            dataArray = da5;
        }
        else if (dataType == DataType.LONG) {
            final ArrayList<Long> result6 = new ArrayList<Long>(this.initial);
            while (sdataIter.hasNext()) {
                final StructureData sdata = sdataIter.next();
                final StructureMembers.Member realm = sdata.getStructureMembers().findMember(proxym.getName());
                final long[] arr$6;
                final long[] data6 = arr$6 = sdata.getJavaArrayLong(realm);
                for (final long aData6 : arr$6) {
                    result6.add(aData6);
                }
                ++count;
            }
            final long[] da6 = new long[result6.size()];
            int i = 0;
            for (final Long d6 : result6) {
                da6[i++] = d6;
            }
            dataArray = da6;
        }
        else if (dataType == DataType.CHAR) {
            final ArrayList<Character> result7 = new ArrayList<Character>(this.initial);
            while (sdataIter.hasNext()) {
                final StructureData sdata = sdataIter.next();
                final StructureMembers.Member realm = sdata.getStructureMembers().findMember(proxym.getName());
                final char[] arr$7;
                final char[] data7 = arr$7 = sdata.getJavaArrayChar(realm);
                for (final char aData7 : arr$7) {
                    result7.add(aData7);
                }
                ++count;
            }
            final char[] da7 = new char[result7.size()];
            int i = 0;
            for (final Character d7 : result7) {
                da7[i++] = d7;
            }
            dataArray = da7;
        }
        else if (dataType == DataType.STRING) {
            final ArrayList<String> result8 = new ArrayList<String>(this.initial);
            while (sdataIter.hasNext()) {
                final StructureData sdata = sdataIter.next();
                final StructureMembers.Member realm = sdata.getStructureMembers().findMember(proxym.getName());
                final String[] arr$8;
                final String[] data8 = arr$8 = sdata.getJavaArrayString(realm);
                for (final String aData8 : arr$8) {
                    result8.add(aData8);
                }
                ++count;
            }
            final String[] da8 = new String[result8.size()];
            int i = 0;
            for (final String d8 : result8) {
                da8[i++] = d8;
            }
            dataArray = da8;
        }
        else if (dataType == DataType.STRUCTURE) {
            final ArrayList<StructureData> result9 = new ArrayList<StructureData>(this.initial);
            while (sdataIter.hasNext()) {
                final StructureData sdata = sdataIter.next();
                final StructureMembers.Member realm = sdata.getStructureMembers().findMember(proxym.getName());
                final ArrayStructure as = sdata.getArrayStructure(realm);
                final StructureDataIterator innerIter = as.getStructureDataIterator();
                while (innerIter.hasNext()) {
                    result9.add(innerIter.next());
                }
                ++count;
            }
            final StructureData[] da9 = new StructureData[result9.size()];
            rshape[0] = count;
            final StructureMembers membersw = new StructureMembers(proxym.getStructureMembers());
            return new ArrayStructureW(membersw, rshape, da9);
        }
        rshape[0] = count;
        return Array.factory(dataType.getPrimitiveClassType(), rshape, dataArray);
    }
    
    @Override
    public String toString() {
        return this.getClass().getName() + " nelems=" + Integer.toString(this.nelems);
    }
    
    @Override
    public void showInternal(final Formatter f, final String leadingSpace) {
        super.showInternal(f, leadingSpace);
        f.format("%sStructureDataIterator Class=%s hash=0x%x%n", leadingSpace, this.iter.getClass().getName(), this.iter.hashCode());
        if (this.iter instanceof ArrayStructureIterator) {
            final ArrayStructureIterator ii = (ArrayStructureIterator)this.iter;
            ii.getArrayStructure().showInternal(f, leadingSpace + "  ");
        }
    }
}
