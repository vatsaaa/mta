// 
// Decompiled by Procyon v0.5.36
// 

package ucar.ma2;

import ucar.nc2.Sequence;
import ucar.nc2.Variable;
import ucar.nc2.Structure;
import java.io.IOException;
import java.util.Iterator;

public class ArrayStructureMA extends ArrayStructure
{
    public ArrayStructureMA(final StructureMembers members, final int[] shape) {
        super(members, shape);
    }
    
    public ArrayStructureMA(final StructureMembers members, final int[] shape, final StructureData[] sdata) {
        super(members, shape);
        if (this.nelems != sdata.length) {
            throw new IllegalArgumentException("StructureData length= " + sdata.length + "!= shape.length=" + this.nelems);
        }
        this.sdata = sdata;
    }
    
    public static ArrayStructureMA factoryMA(final ArrayStructure from) throws IOException {
        if (from instanceof ArrayStructureMA) {
            return (ArrayStructureMA)from;
        }
        final StructureMembers tosm = new StructureMembers(new StructureMembers(from.getStructureMembers()));
        final ArrayStructureMA to = new ArrayStructureMA(tosm, from.getShape());
        for (final StructureMembers.Member m : from.getMembers()) {
            to.setMemberArray(m.getName(), from.extractMemberArray(m));
        }
        return to;
    }
    
    @Override
    protected StructureData makeStructureData(final ArrayStructure as, final int index) {
        return new StructureDataA(as, index);
    }
    
    public void setMemberArray(final String memberName, final Array data) {
        final StructureMembers.Member m = this.members.findMember(memberName);
        m.setDataArray(data);
    }
    
    public static ArrayStructureMA factoryMA(final Structure from, final int[] shape) throws IOException {
        final StructureMembers sm = from.makeStructureMembers();
        for (final Variable v : from.getVariables()) {
            Array data;
            if (v instanceof Sequence) {
                data = new ArrayObject(ArraySequence.class, shape);
            }
            else if (v instanceof Structure) {
                data = factoryMA((Structure)v, combine(shape, v.getShape()));
            }
            else {
                data = Array.factory(v.getDataType(), combine(shape, v.getShape()));
            }
            final StructureMembers.Member m = sm.findMember(v.getShortName());
            m.setDataArray(data);
        }
        return new ArrayStructureMA(sm, shape);
    }
    
    private static int[] combine(final int[] shape1, final int[] shape2) {
        final int[] result = new int[shape1.length + shape2.length];
        System.arraycopy(shape1, 0, result, 0, shape1.length);
        System.arraycopy(shape2, 0, result, shape1.length, shape2.length);
        return result;
    }
}
