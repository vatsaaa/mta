// 
// Decompiled by Procyon v0.5.36
// 

package ucar.ma2;

public class Index6D extends Index
{
    private int curr0;
    private int curr1;
    private int curr2;
    private int curr3;
    private int curr4;
    private int curr5;
    private int stride0;
    private int stride1;
    private int stride2;
    private int stride3;
    private int stride4;
    private int stride5;
    private int shape0;
    private int shape1;
    private int shape2;
    private int shape3;
    private int shape4;
    private int shape5;
    
    Index6D() {
        super(6);
    }
    
    public Index6D(final int[] shape) {
        super(shape);
        this.precalc();
    }
    
    @Override
    protected void precalc() {
        this.shape0 = this.shape[0];
        this.shape1 = this.shape[1];
        this.shape2 = this.shape[2];
        this.shape3 = this.shape[3];
        this.shape4 = this.shape[4];
        this.shape5 = this.shape[5];
        this.stride0 = this.stride[0];
        this.stride1 = this.stride[1];
        this.stride2 = this.stride[2];
        this.stride3 = this.stride[3];
        this.stride4 = this.stride[4];
        this.stride5 = this.stride[5];
        this.curr0 = this.current[0];
        this.curr1 = this.current[1];
        this.curr2 = this.current[2];
        this.curr3 = this.current[3];
        this.curr4 = this.current[4];
        this.curr5 = this.current[5];
    }
    
    @Override
    public String toString() {
        return this.curr0 + "," + this.curr1 + "," + this.curr2 + "," + this.curr3 + "," + this.curr4 + "," + this.curr5;
    }
    
    @Override
    public int[] getCurrentCounter() {
        this.current[0] = this.curr0;
        this.current[1] = this.curr1;
        this.current[2] = this.curr2;
        this.current[3] = this.curr3;
        this.current[4] = this.curr4;
        this.current[5] = this.curr5;
        return this.current.clone();
    }
    
    @Override
    public int currentElement() {
        return this.offset + this.curr0 * this.stride0 + this.curr1 * this.stride1 + this.curr2 * this.stride2 + this.curr3 * this.stride3 + this.curr4 * this.stride4 + this.curr5 * this.stride5;
    }
    
    @Override
    public int incr() {
        if (++this.curr5 >= this.shape5) {
            this.curr5 = 0;
            if (++this.curr4 >= this.shape4) {
                this.curr4 = 0;
                if (++this.curr3 >= this.shape3) {
                    this.curr3 = 0;
                    if (++this.curr2 >= this.shape2) {
                        this.curr2 = 0;
                        if (++this.curr1 >= this.shape1) {
                            this.curr1 = 0;
                            if (++this.curr0 >= this.shape0) {
                                this.curr0 = 0;
                            }
                        }
                    }
                }
            }
        }
        return this.offset + this.curr0 * this.stride0 + this.curr1 * this.stride1 + this.curr2 * this.stride2 + this.curr3 * this.stride3 + this.curr4 * this.stride4 + this.curr5 * this.stride5;
    }
    
    @Override
    public void setDim(final int dim, final int value) {
        if (value < 0 || value >= this.shape[dim]) {
            throw new ArrayIndexOutOfBoundsException();
        }
        if (dim == 5) {
            this.curr5 = value;
        }
        else if (dim == 4) {
            this.curr4 = value;
        }
        else if (dim == 3) {
            this.curr3 = value;
        }
        else if (dim == 2) {
            this.curr2 = value;
        }
        else if (dim == 1) {
            this.curr1 = value;
        }
        else {
            this.curr0 = value;
        }
    }
    
    @Override
    public Index set0(final int v) {
        if (v < 0 || v >= this.shape0) {
            throw new ArrayIndexOutOfBoundsException();
        }
        this.curr0 = v;
        return this;
    }
    
    @Override
    public Index set1(final int v) {
        if (v < 0 || v >= this.shape1) {
            throw new ArrayIndexOutOfBoundsException();
        }
        this.curr1 = v;
        return this;
    }
    
    @Override
    public Index set2(final int v) {
        if (v < 0 || v >= this.shape2) {
            throw new ArrayIndexOutOfBoundsException();
        }
        this.curr2 = v;
        return this;
    }
    
    @Override
    public Index set3(final int v) {
        if (v < 0 || v >= this.shape3) {
            throw new ArrayIndexOutOfBoundsException();
        }
        this.curr3 = v;
        return this;
    }
    
    @Override
    public Index set4(final int v) {
        if (v < 0 || v >= this.shape4) {
            throw new ArrayIndexOutOfBoundsException();
        }
        this.curr4 = v;
        return this;
    }
    
    @Override
    public Index set5(final int v) {
        if (v < 0 || v >= this.shape5) {
            throw new ArrayIndexOutOfBoundsException();
        }
        this.curr5 = v;
        return this;
    }
    
    @Override
    public Index set(final int v0, final int v1, final int v2, final int v3, final int v4, final int v5) {
        this.set0(v0);
        this.set1(v1);
        this.set2(v2);
        this.set3(v3);
        this.set4(v4);
        this.set5(v5);
        return this;
    }
    
    @Override
    public Index set(final int[] index) {
        if (index.length != this.rank) {
            throw new ArrayIndexOutOfBoundsException();
        }
        this.set0(index[0]);
        this.set1(index[1]);
        this.set2(index[2]);
        this.set3(index[3]);
        this.set4(index[4]);
        this.set5(index[5]);
        return this;
    }
    
    @Override
    public Object clone() {
        return super.clone();
    }
    
    int setDirect(final int v0, final int v1, final int v2, final int v3, final int v4, final int v5) {
        return this.offset + v0 * this.stride0 + v1 * this.stride1 + v2 * this.stride2 + v3 * this.stride3 + v4 * this.stride4 + v5 * this.stride5;
    }
}
