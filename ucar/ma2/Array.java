// 
// Decompiled by Procyon v0.5.36
// 

package ucar.ma2;

import java.nio.DoubleBuffer;
import java.nio.FloatBuffer;
import java.nio.LongBuffer;
import java.nio.IntBuffer;
import java.nio.ShortBuffer;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public abstract class Array
{
    protected final Index indexCalc;
    protected final int rank;
    protected boolean unsigned;
    private IndexIterator ii;
    
    public static Array factory(final DataType dataType, final int[] shape) {
        final Index index = Index.factory(shape);
        return factory(dataType.getPrimitiveClassType(), index);
    }
    
    public static Array factory(final Class classType, final int[] shape) {
        final Index index = Index.factory(shape);
        return factory(classType, index);
    }
    
    private static Array factory(final Class classType, final Index index) {
        if (classType == Double.TYPE || classType == Double.class) {
            return ArrayDouble.factory(index);
        }
        if (classType == Float.TYPE || classType == Float.class) {
            return ArrayFloat.factory(index);
        }
        if (classType == Long.TYPE || classType == Long.class) {
            return ArrayLong.factory(index);
        }
        if (classType == Integer.TYPE || classType == Integer.class) {
            return ArrayInt.factory(index);
        }
        if (classType == Short.TYPE || classType == Short.class) {
            return ArrayShort.factory(index);
        }
        if (classType == Byte.TYPE || classType == Byte.class) {
            return ArrayByte.factory(index);
        }
        if (classType == Character.TYPE || classType == Character.class) {
            return ArrayChar.factory(index);
        }
        if (classType == Boolean.TYPE || classType == Boolean.class) {
            return ArrayBoolean.factory(index);
        }
        return ArrayObject.factory(classType, index);
    }
    
    public static Array factory(final DataType dataType, final int[] shape, final Object storage) {
        return factory(dataType.getPrimitiveClassType(), shape, storage);
    }
    
    public static Array factory(final Class classType, final int[] shape, final Object storage) {
        final Index indexCalc = Index.factory(shape);
        return factory(classType, indexCalc, storage);
    }
    
    private static Array factory(final Class classType, final Index indexCalc, final Object storage) {
        if (classType == Double.TYPE || classType == Double.class) {
            return ArrayDouble.factory(indexCalc, (double[])storage);
        }
        if (classType == Float.TYPE || classType == Float.class) {
            return ArrayFloat.factory(indexCalc, (float[])storage);
        }
        if (classType == Long.TYPE || classType == Long.class) {
            return ArrayLong.factory(indexCalc, (long[])storage);
        }
        if (classType == Integer.TYPE || classType == Integer.class) {
            return ArrayInt.factory(indexCalc, (int[])storage);
        }
        if (classType == Short.TYPE || classType == Short.class) {
            return ArrayShort.factory(indexCalc, (short[])storage);
        }
        if (classType == Byte.TYPE || classType == Byte.class) {
            return ArrayByte.factory(indexCalc, (byte[])storage);
        }
        if (classType == Character.TYPE || classType == Character.class) {
            return ArrayChar.factory(indexCalc, (char[])storage);
        }
        if (classType == Boolean.TYPE || classType == Boolean.class) {
            return ArrayBoolean.factory(indexCalc, (boolean[])storage);
        }
        return ArrayObject.factory(classType, indexCalc, (Object[])storage);
    }
    
    public static Array factoryConstant(final Class classType, final int[] shape, final Object storage) {
        final Index index = new IndexConstant(shape);
        if (classType == Double.TYPE || classType == Double.class) {
            return new ArrayDouble(index, (double[])storage);
        }
        if (classType == Float.TYPE || classType == Float.class) {
            return new ArrayFloat(index, (float[])storage);
        }
        if (classType == Long.TYPE || classType == Long.class) {
            return new ArrayLong(index, (long[])storage);
        }
        if (classType == Integer.TYPE || classType == Integer.class) {
            return new ArrayInt(index, (int[])storage);
        }
        if (classType == Short.TYPE || classType == Short.class) {
            return new ArrayShort(index, (short[])storage);
        }
        if (classType == Byte.TYPE || classType == Byte.class) {
            return new ArrayByte(index, (byte[])storage);
        }
        if (classType == Character.TYPE || classType == Character.class) {
            return new ArrayChar(index, (char[])storage);
        }
        if (classType == Boolean.TYPE || classType == Boolean.class) {
            return new ArrayBoolean(index, (boolean[])storage);
        }
        return new ArrayObject(classType, index, (Object[])storage);
    }
    
    public static Array factory(final Object javaArray) {
        int rank_ = 0;
        Class componentType;
        for (componentType = javaArray.getClass(); componentType.isArray(); componentType = componentType.getComponentType()) {
            ++rank_;
        }
        int count = 0;
        final int[] shape = new int[rank_];
        Object jArray = javaArray;
        for (Class cType = jArray.getClass(); cType.isArray(); cType = jArray.getClass()) {
            shape[count++] = java.lang.reflect.Array.getLength(jArray);
            jArray = java.lang.reflect.Array.get(jArray, 0);
        }
        final Array aa = factory(componentType, shape);
        final IndexIterator aaIter = aa.getIndexIterator();
        reflectArrayCopyIn(javaArray, aa, aaIter);
        return aa;
    }
    
    private static void reflectArrayCopyIn(final Object jArray, final Array aa, final IndexIterator aaIter) {
        final Class cType = jArray.getClass().getComponentType();
        if (cType.isPrimitive()) {
            aa.copyFrom1DJavaArray(aaIter, jArray);
        }
        else {
            for (int i = 0; i < java.lang.reflect.Array.getLength(jArray); ++i) {
                reflectArrayCopyIn(java.lang.reflect.Array.get(jArray, i), aa, aaIter);
            }
        }
    }
    
    private static void reflectArrayCopyOut(final Object jArray, final Array aa, final IndexIterator aaIter) {
        final Class cType = jArray.getClass().getComponentType();
        if (cType.isPrimitive()) {
            aa.copyTo1DJavaArray(aaIter, jArray);
        }
        else {
            for (int i = 0; i < java.lang.reflect.Array.getLength(jArray); ++i) {
                reflectArrayCopyOut(java.lang.reflect.Array.get(jArray, i), aa, aaIter);
            }
        }
    }
    
    public static void arraycopy(final Array arraySrc, final int srcPos, final Array arrayDst, final int dstPos, final int len) {
        if (arraySrc.isConstant()) {
            final double d = arraySrc.getDouble(0);
            for (int i = dstPos; i < dstPos + len; ++i) {
                arrayDst.setDouble(i, d);
            }
            return;
        }
        final Object src = arraySrc.get1DJavaArray(arraySrc.getElementType());
        final Object dst = arrayDst.getStorage();
        try {
            System.arraycopy(src, srcPos, dst, dstPos, len);
        }
        catch (ArrayIndexOutOfBoundsException e) {
            throw e;
        }
    }
    
    public static Array makeArray(final DataType dtype, final int npts, final double start, final double incr) {
        final Array result = factory(dtype.getPrimitiveClassType(), new int[] { npts });
        final IndexIterator dataI = result.getIndexIterator();
        for (int i = 0; i < npts; ++i) {
            final double val = start + i * incr;
            dataI.setDoubleNext(val);
        }
        return result;
    }
    
    public static Array makeArray(final DataType dtype, final List<String> stringValues) throws NumberFormatException {
        final Array result = factory(dtype.getPrimitiveClassType(), new int[] { stringValues.size() });
        final IndexIterator dataI = result.getIndexIterator();
        for (final String s : stringValues) {
            if (dtype == DataType.STRING) {
                dataI.setObjectNext(s);
            }
            else if (dtype == DataType.LONG) {
                final long val = Long.parseLong(s);
                dataI.setLongNext(val);
            }
            else {
                final double val2 = Double.parseDouble(s);
                dataI.setDoubleNext(val2);
            }
        }
        return result;
    }
    
    public static Array makeArray(final DataType dtype, final String[] stringValues) throws NumberFormatException {
        final Array result = factory(dtype.getPrimitiveClassType(), new int[] { stringValues.length });
        final IndexIterator dataI = result.getIndexIterator();
        for (final String s : stringValues) {
            if (dtype == DataType.STRING) {
                dataI.setObjectNext(s);
            }
            else if (dtype == DataType.LONG) {
                final long val = Long.parseLong(s);
                dataI.setLongNext(val);
            }
            else {
                final double val2 = Double.parseDouble(s);
                dataI.setDoubleNext(val2);
            }
        }
        return result;
    }
    
    protected Array(final int[] shape) {
        this.rank = shape.length;
        this.indexCalc = Index.factory(shape);
    }
    
    protected Array(final Index index) {
        this.rank = index.getRank();
        this.indexCalc = index;
    }
    
    public Index getIndex() {
        return (Index)this.indexCalc.clone();
    }
    
    public Index getIndexPrivate() {
        return this.indexCalc;
    }
    
    public IndexIterator getIndexIterator() {
        return this.indexCalc.getIndexIterator(this);
    }
    
    public int getRank() {
        return this.rank;
    }
    
    public int[] getShape() {
        return this.indexCalc.getShape();
    }
    
    public long getSize() {
        return this.indexCalc.getSize();
    }
    
    public long getSizeBytes() {
        final DataType dtype = DataType.getType(this.getElementType());
        return this.indexCalc.getSize() * dtype.getSize();
    }
    
    public IndexIterator getRangeIterator(final List<Range> ranges) throws InvalidRangeException {
        return this.section(ranges).getIndexIterator();
    }
    
    @Deprecated
    public IndexIterator getIndexIteratorFast() {
        return this.indexCalc.getIndexIteratorFast(this);
    }
    
    public abstract Class getElementType();
    
    abstract Array createView(final Index p0);
    
    public abstract Object getStorage();
    
    abstract void copyFrom1DJavaArray(final IndexIterator p0, final Object p1);
    
    abstract void copyTo1DJavaArray(final IndexIterator p0, final Object p1);
    
    public Array section(final List<Range> ranges) throws InvalidRangeException {
        return this.createView(this.indexCalc.section(ranges));
    }
    
    public Array section(final int[] origin, final int[] shape) throws InvalidRangeException {
        return this.section(origin, shape, null);
    }
    
    public Array section(final int[] origin, final int[] shape, int[] stride) throws InvalidRangeException {
        final List<Range> ranges = new ArrayList<Range>(origin.length);
        if (stride == null) {
            stride = new int[origin.length];
            for (int i = 0; i < stride.length; ++i) {
                stride[i] = 1;
            }
        }
        for (int i = 0; i < origin.length; ++i) {
            ranges.add(new Range(origin[i], origin[i] + stride[i] * shape[i] - 1, stride[i]));
        }
        return this.createView(this.indexCalc.section(ranges));
    }
    
    public Array sectionNoReduce(final List<Range> ranges) throws InvalidRangeException {
        return this.createView(this.indexCalc.sectionNoReduce(ranges));
    }
    
    public Array sectionNoReduce(final int[] origin, final int[] shape, int[] stride) throws InvalidRangeException {
        final List<Range> ranges = new ArrayList<Range>(origin.length);
        if (stride == null) {
            stride = new int[origin.length];
            for (int i = 0; i < stride.length; ++i) {
                stride[i] = 1;
            }
        }
        for (int i = 0; i < origin.length; ++i) {
            ranges.add(new Range(origin[i], origin[i] + stride[i] * shape[i] - 1, stride[i]));
        }
        return this.createView(this.indexCalc.sectionNoReduce(ranges));
    }
    
    public Array slice(final int dim, final int value) {
        final int[] origin = new int[this.rank];
        final int[] shape = this.getShape();
        origin[dim] = value;
        shape[dim] = 1;
        try {
            return this.sectionNoReduce(origin, shape, null).reduce(dim);
        }
        catch (InvalidRangeException e) {
            throw new IllegalArgumentException();
        }
    }
    
    public Array copy() {
        final Array newA = factory(this.getElementType(), this.getShape());
        MAMath.copy(newA, this);
        newA.setUnsigned(this.isUnsigned());
        return newA;
    }
    
    public Object get1DJavaArray(final Class wantType) {
        if (wantType != this.getElementType()) {
            final Array newA = factory(wantType, this.getShape());
            MAMath.copy(newA, this);
            return newA.getStorage();
        }
        if (this.indexCalc.isFastIterator()) {
            return this.getStorage();
        }
        return this.copyTo1DJavaArray();
    }
    
    public ByteBuffer getDataAsByteBuffer() {
        throw new UnsupportedOperationException();
    }
    
    public static Array factory(final DataType dtype, int[] shape, final ByteBuffer bb) {
        switch (dtype) {
            case ENUM1:
            case BYTE: {
                if (shape == null) {
                    shape = new int[] { bb.limit() };
                }
                return factory(dtype, shape, bb.array());
            }
            case CHAR: {
                final int size = bb.limit();
                if (shape == null) {
                    shape = new int[] { size };
                }
                final Array result = factory(dtype, shape);
                for (int i = 0; i < size; ++i) {
                    result.setChar(i, (char)bb.get(i));
                }
                return result;
            }
            case ENUM2:
            case SHORT: {
                final ShortBuffer sb = bb.asShortBuffer();
                final int size = sb.limit();
                if (shape == null) {
                    shape = new int[] { size };
                }
                final Array result = factory(dtype, shape);
                for (int j = 0; j < size; ++j) {
                    result.setShort(j, sb.get(j));
                }
                return result;
            }
            case ENUM4:
            case INT: {
                final IntBuffer ib = bb.asIntBuffer();
                final int size = ib.limit();
                if (shape == null) {
                    shape = new int[] { size };
                }
                final Array result = factory(dtype, shape);
                for (int k = 0; k < size; ++k) {
                    result.setInt(k, ib.get(k));
                }
                return result;
            }
            case LONG: {
                final LongBuffer lb = bb.asLongBuffer();
                final int size = lb.limit();
                if (shape == null) {
                    shape = new int[] { size };
                }
                final Array result = factory(dtype, shape);
                for (int l = 0; l < size; ++l) {
                    result.setLong(l, lb.get(l));
                }
                return result;
            }
            case FLOAT: {
                final FloatBuffer ffb = bb.asFloatBuffer();
                final int size = ffb.limit();
                if (shape == null) {
                    shape = new int[] { size };
                }
                final Array result = factory(dtype, shape);
                for (int m = 0; m < size; ++m) {
                    result.setFloat(m, ffb.get(m));
                }
                return result;
            }
            case DOUBLE: {
                final DoubleBuffer db = bb.asDoubleBuffer();
                final int size = db.limit();
                if (shape == null) {
                    shape = new int[] { size };
                }
                final Array result = factory(dtype, shape);
                for (int i2 = 0; i2 < size; ++i2) {
                    result.setDouble(i2, db.get(i2));
                }
                return result;
            }
            default: {
                throw new UnsupportedOperationException("" + dtype);
            }
        }
    }
    
    public Object copyTo1DJavaArray() {
        final Array newA = this.copy();
        return newA.getStorage();
    }
    
    public Object copyToNDJavaArray() {
        Object javaArray;
        try {
            javaArray = java.lang.reflect.Array.newInstance(this.getElementType(), this.getShape());
        }
        catch (Exception e) {
            throw new IllegalArgumentException(e);
        }
        final IndexIterator iter = this.getIndexIterator();
        reflectArrayCopyOut(javaArray, this, iter);
        return javaArray;
    }
    
    public Array flip(final int dim) {
        return this.createView(this.indexCalc.flip(dim));
    }
    
    public Array transpose(final int dim1, final int dim2) {
        return this.createView(this.indexCalc.transpose(dim1, dim2));
    }
    
    public Array permute(final int[] dims) {
        return this.createView(this.indexCalc.permute(dims));
    }
    
    public Array reshape(final int[] shape) {
        final Array result = factory(this.getElementType(), shape);
        if (result.getSize() != this.getSize()) {
            throw new IllegalArgumentException("reshape arrays must have same total size");
        }
        result.setUnsigned(this.isUnsigned());
        arraycopy(this, 0, result, 0, (int)this.getSize());
        return result;
    }
    
    public Array reshapeNoCopy(final int[] shape) {
        final Array result = factory(this.getElementType(), shape, this.getStorage());
        if (result.getSize() != this.getSize()) {
            throw new IllegalArgumentException("reshape arrays must have same total size");
        }
        return result;
    }
    
    public Array reduce() {
        final Index ri = this.indexCalc.reduce();
        if (ri == this.indexCalc) {
            return this;
        }
        return this.createView(ri);
    }
    
    public Array reduce(final int dim) {
        return this.createView(this.indexCalc.reduce(dim));
    }
    
    public boolean isUnsigned() {
        return this.unsigned;
    }
    
    public boolean isConstant() {
        return this.indexCalc instanceof IndexConstant;
    }
    
    public void setUnsigned(final boolean unsigned) {
        this.unsigned = unsigned;
    }
    
    public abstract double getDouble(final Index p0);
    
    public abstract void setDouble(final Index p0, final double p1);
    
    public abstract float getFloat(final Index p0);
    
    public abstract void setFloat(final Index p0, final float p1);
    
    public abstract long getLong(final Index p0);
    
    public abstract void setLong(final Index p0, final long p1);
    
    public abstract int getInt(final Index p0);
    
    public abstract void setInt(final Index p0, final int p1);
    
    public abstract short getShort(final Index p0);
    
    public abstract void setShort(final Index p0, final short p1);
    
    public abstract byte getByte(final Index p0);
    
    public abstract void setByte(final Index p0, final byte p1);
    
    public abstract char getChar(final Index p0);
    
    public abstract void setChar(final Index p0, final char p1);
    
    public abstract boolean getBoolean(final Index p0);
    
    public abstract void setBoolean(final Index p0, final boolean p1);
    
    public abstract Object getObject(final Index p0);
    
    public abstract void setObject(final Index p0, final Object p1);
    
    public abstract double getDouble(final int p0);
    
    public abstract void setDouble(final int p0, final double p1);
    
    public abstract float getFloat(final int p0);
    
    public abstract void setFloat(final int p0, final float p1);
    
    public abstract long getLong(final int p0);
    
    public abstract void setLong(final int p0, final long p1);
    
    public abstract int getInt(final int p0);
    
    public abstract void setInt(final int p0, final int p1);
    
    public abstract short getShort(final int p0);
    
    public abstract void setShort(final int p0, final short p1);
    
    public abstract byte getByte(final int p0);
    
    public abstract void setByte(final int p0, final byte p1);
    
    public abstract char getChar(final int p0);
    
    public abstract void setChar(final int p0, final char p1);
    
    public abstract boolean getBoolean(final int p0);
    
    public abstract void setBoolean(final int p0, final boolean p1);
    
    public abstract Object getObject(final int p0);
    
    public abstract void setObject(final int p0, final Object p1);
    
    @Override
    public String toString() {
        final StringBuilder sbuff = new StringBuilder();
        final IndexIterator ii = this.getIndexIterator();
        while (ii.hasNext()) {
            final Object data = ii.getObjectNext();
            sbuff.append(data);
            sbuff.append(" ");
        }
        return sbuff.toString();
    }
    
    public String shapeToString() {
        final int[] shape = this.getShape();
        if (shape.length == 0) {
            return "";
        }
        final StringBuilder sb = new StringBuilder();
        sb.append('(');
        for (int i = 0; i < shape.length; ++i) {
            final int s = shape[i];
            if (i > 0) {
                sb.append(",");
            }
            sb.append(s);
        }
        sb.append(')');
        return sb.toString();
    }
    
    public boolean hasNext() {
        if (null == this.ii) {
            this.ii = this.getIndexIterator();
        }
        return this.ii.hasNext();
    }
    
    public Object next() {
        return this.ii.getObjectNext();
    }
    
    public double nextDouble() {
        return this.ii.getDoubleNext();
    }
    
    public float nextFloat() {
        return this.ii.getFloatNext();
    }
    
    public byte nextByte() {
        return this.ii.getByteNext();
    }
    
    public short nextShort() {
        return this.ii.getShortNext();
    }
    
    public int nextInt() {
        return this.ii.getIntNext();
    }
    
    public long nextLong() {
        return this.ii.getLongNext();
    }
    
    public char nextChar() {
        return this.ii.getCharNext();
    }
    
    public boolean nextBoolean() {
        return this.ii.getBooleanNext();
    }
    
    public void resetLocalIterator() {
        this.ii = null;
    }
}
