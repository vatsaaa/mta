// 
// Decompiled by Procyon v0.5.36
// 

package ucar.ma2;

public final class InvalidRangeException extends Exception
{
    public InvalidRangeException() {
    }
    
    public InvalidRangeException(final String s) {
        super(s);
    }
}
