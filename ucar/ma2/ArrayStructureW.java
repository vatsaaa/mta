// 
// Decompiled by Procyon v0.5.36
// 

package ucar.ma2;

public class ArrayStructureW extends ArrayStructure
{
    public ArrayStructureW(final StructureMembers members, final int[] shape) {
        super(members, shape);
        this.sdata = new StructureData[this.nelems];
    }
    
    public ArrayStructureW(final StructureMembers members, final int[] shape, final StructureData[] sdata) {
        super(members, shape);
        if (this.nelems != sdata.length) {
            throw new IllegalArgumentException("StructureData length= " + sdata.length + "!= shape.length=" + this.nelems);
        }
        this.sdata = sdata;
    }
    
    public void setStructureData(final StructureData sd, final int index) {
        this.sdata[index] = sd;
    }
    
    @Override
    protected StructureData makeStructureData(final ArrayStructure as, final int index) {
        return new StructureDataW(as.getStructureMembers());
    }
    
    @Override
    public Array getArray(final int recnum, final StructureMembers.Member m) {
        if (m.getDataArray() != null) {
            return super.getArray(recnum, m);
        }
        final StructureData sd = this.getStructureData(recnum);
        return sd.getArray(m.getName());
    }
    
    @Override
    public double getScalarDouble(final int recnum, final StructureMembers.Member m) {
        if (m.getDataArray() != null) {
            return super.getScalarDouble(recnum, m);
        }
        final StructureData sd = this.getStructureData(recnum);
        return sd.getScalarDouble(m.getName());
    }
    
    @Override
    public double[] getJavaArrayDouble(final int recnum, final StructureMembers.Member m) {
        if (m.getDataArray() != null) {
            return super.getJavaArrayDouble(recnum, m);
        }
        final StructureData sd = this.getStructureData(recnum);
        return sd.getJavaArrayDouble(m.getName());
    }
    
    @Override
    public float getScalarFloat(final int recnum, final StructureMembers.Member m) {
        if (m.getDataArray() != null) {
            return super.getScalarFloat(recnum, m);
        }
        final StructureData sd = this.getStructureData(recnum);
        return sd.getScalarFloat(m.getName());
    }
    
    @Override
    public float[] getJavaArrayFloat(final int recnum, final StructureMembers.Member m) {
        if (m.getDataArray() != null) {
            return super.getJavaArrayFloat(recnum, m);
        }
        final StructureData sd = this.getStructureData(recnum);
        return sd.getJavaArrayFloat(m.getName());
    }
    
    @Override
    public byte getScalarByte(final int recnum, final StructureMembers.Member m) {
        if (m.getDataArray() != null) {
            return super.getScalarByte(recnum, m);
        }
        final StructureData sd = this.getStructureData(recnum);
        return sd.getScalarByte(m.getName());
    }
    
    @Override
    public byte[] getJavaArrayByte(final int recnum, final StructureMembers.Member m) {
        if (m.getDataArray() != null) {
            return super.getJavaArrayByte(recnum, m);
        }
        final StructureData sd = this.getStructureData(recnum);
        return sd.getJavaArrayByte(m.getName());
    }
    
    @Override
    public short getScalarShort(final int recnum, final StructureMembers.Member m) {
        if (m.getDataArray() != null) {
            return super.getScalarShort(recnum, m);
        }
        final StructureData sd = this.getStructureData(recnum);
        return sd.getScalarShort(m.getName());
    }
    
    @Override
    public short[] getJavaArrayShort(final int recnum, final StructureMembers.Member m) {
        if (m.getDataArray() != null) {
            return super.getJavaArrayShort(recnum, m);
        }
        final StructureData sd = this.getStructureData(recnum);
        return sd.getJavaArrayShort(m.getName());
    }
    
    @Override
    public int getScalarInt(final int recnum, final StructureMembers.Member m) {
        if (m.getDataArray() != null) {
            return super.getScalarInt(recnum, m);
        }
        final StructureData sd = this.getStructureData(recnum);
        return sd.getScalarInt(m.getName());
    }
    
    @Override
    public int[] getJavaArrayInt(final int recnum, final StructureMembers.Member m) {
        if (m.getDataArray() != null) {
            return super.getJavaArrayInt(recnum, m);
        }
        final StructureData sd = this.getStructureData(recnum);
        return sd.getJavaArrayInt(m.getName());
    }
    
    @Override
    public long getScalarLong(final int recnum, final StructureMembers.Member m) {
        if (m.getDataArray() != null) {
            return super.getScalarLong(recnum, m);
        }
        final StructureData sd = this.getStructureData(recnum);
        return sd.getScalarLong(m.getName());
    }
    
    @Override
    public long[] getJavaArrayLong(final int recnum, final StructureMembers.Member m) {
        if (m.getDataArray() != null) {
            return super.getJavaArrayLong(recnum, m);
        }
        final StructureData sd = this.getStructureData(recnum);
        return sd.getJavaArrayLong(m.getName());
    }
    
    @Override
    public char getScalarChar(final int recnum, final StructureMembers.Member m) {
        if (m.getDataArray() != null) {
            return super.getScalarChar(recnum, m);
        }
        final StructureData sd = this.getStructureData(recnum);
        return sd.getScalarChar(m.getName());
    }
    
    @Override
    public char[] getJavaArrayChar(final int recnum, final StructureMembers.Member m) {
        if (m.getDataArray() != null) {
            return super.getJavaArrayChar(recnum, m);
        }
        final StructureData sd = this.getStructureData(recnum);
        return sd.getJavaArrayChar(m.getName());
    }
    
    @Override
    public String getScalarString(final int recnum, final StructureMembers.Member m) {
        if (m.getDataArray() != null) {
            return super.getScalarString(recnum, m);
        }
        final StructureData sd = this.getStructureData(recnum);
        return sd.getScalarString(m.getName());
    }
    
    @Override
    public String[] getJavaArrayString(final int recnum, final StructureMembers.Member m) {
        if (m.getDataArray() != null) {
            return super.getJavaArrayString(recnum, m);
        }
        final StructureData sd = this.getStructureData(recnum);
        return sd.getJavaArrayString(m.getName());
    }
    
    @Override
    public StructureData getScalarStructure(final int recnum, final StructureMembers.Member m) {
        if (m.getDataArray() != null) {
            return super.getScalarStructure(recnum, m);
        }
        final StructureData sd = this.getStructureData(recnum);
        return sd.getScalarStructure(m.getName());
    }
    
    @Override
    public ArrayStructure getArrayStructure(final int recnum, final StructureMembers.Member m) {
        if (m.getDataArray() != null) {
            return super.getArrayStructure(recnum, m);
        }
        final StructureData sd = this.getStructureData(recnum);
        return sd.getArrayStructure(m.getName());
    }
    
    @Override
    public ArraySequence getArraySequence(final int recnum, final StructureMembers.Member m) {
        if (m.getDataArray() != null) {
            return super.getArraySequence(recnum, m);
        }
        final StructureData sd = this.getStructureData(recnum);
        return sd.getArraySequence(m.getName());
    }
}
