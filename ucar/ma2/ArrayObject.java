// 
// Decompiled by Procyon v0.5.36
// 

package ucar.ma2;

public class ArrayObject extends Array
{
    protected Class elementType;
    protected Object[] storage;
    
    static ArrayObject factory(final Class classType, final Index index) {
        return factory(classType, index, null);
    }
    
    static ArrayObject factory(final Class classType, final Index index, final Object[] storage) {
        switch (index.getRank()) {
            case 0: {
                return new D0(classType, index, storage);
            }
            case 1: {
                return new D1(classType, index, storage);
            }
            case 2: {
                return new D2(classType, index, storage);
            }
            case 3: {
                return new D3(classType, index, storage);
            }
            case 4: {
                return new D4(classType, index, storage);
            }
            case 5: {
                return new D5(classType, index, storage);
            }
            case 6: {
                return new D6(classType, index, storage);
            }
            case 7: {
                return new D7(classType, index, storage);
            }
            default: {
                return new ArrayObject(classType, index, storage);
            }
        }
    }
    
    public ArrayObject(final Class elementType, final int[] shape) {
        super(shape);
        this.elementType = elementType;
        this.storage = new Object[(int)this.indexCalc.getSize()];
    }
    
    public ArrayObject(final Class elementType, final int[] shape, final Object[] storage) {
        super(shape);
        this.elementType = elementType;
        this.storage = storage;
    }
    
    @Override
    Array createView(final Index index) {
        return factory(this.elementType, index, this.storage);
    }
    
    ArrayObject(final Class elementType, final Index ima, final Object[] data) {
        super(ima);
        this.elementType = elementType;
        if (data != null) {
            this.storage = data;
        }
        else {
            this.storage = new Object[(int)this.indexCalc.getSize()];
        }
    }
    
    @Override
    public Object getStorage() {
        return this.storage;
    }
    
    @Override
    void copyFrom1DJavaArray(final IndexIterator iter, final Object javaArray) {
        final Object[] ja = (Object[])javaArray;
        for (int i = 0; i < ja.length; ++i) {
            iter.setObjectNext(ja[i]);
        }
    }
    
    @Override
    void copyTo1DJavaArray(final IndexIterator iter, final Object javaArray) {
        final Object[] ja = (Object[])javaArray;
        for (int i = 0; i < ja.length; ++i) {
            ja[i] = iter.getObjectNext();
        }
    }
    
    @Override
    public Class getElementType() {
        return this.elementType;
    }
    
    @Override
    public double getDouble(final Index i) {
        throw new ForbiddenConversionException();
    }
    
    @Override
    public void setDouble(final Index i, final double value) {
        throw new ForbiddenConversionException();
    }
    
    @Override
    public float getFloat(final Index i) {
        throw new ForbiddenConversionException();
    }
    
    @Override
    public void setFloat(final Index i, final float value) {
        throw new ForbiddenConversionException();
    }
    
    @Override
    public long getLong(final Index i) {
        throw new ForbiddenConversionException();
    }
    
    @Override
    public void setLong(final Index i, final long value) {
        throw new ForbiddenConversionException();
    }
    
    @Override
    public int getInt(final Index i) {
        throw new ForbiddenConversionException();
    }
    
    @Override
    public void setInt(final Index i, final int value) {
        throw new ForbiddenConversionException();
    }
    
    @Override
    public short getShort(final Index i) {
        throw new ForbiddenConversionException();
    }
    
    @Override
    public void setShort(final Index i, final short value) {
        throw new ForbiddenConversionException();
    }
    
    @Override
    public byte getByte(final Index i) {
        throw new ForbiddenConversionException();
    }
    
    @Override
    public void setByte(final Index i, final byte value) {
        throw new ForbiddenConversionException();
    }
    
    @Override
    public boolean getBoolean(final Index i) {
        throw new ForbiddenConversionException();
    }
    
    @Override
    public void setBoolean(final Index i, final boolean value) {
        throw new ForbiddenConversionException();
    }
    
    @Override
    public char getChar(final Index i) {
        throw new ForbiddenConversionException();
    }
    
    @Override
    public void setChar(final Index i, final char value) {
        throw new ForbiddenConversionException();
    }
    
    @Override
    public Object getObject(final Index i) {
        return this.storage[i.currentElement()];
    }
    
    @Override
    public void setObject(final Index i, final Object value) {
        this.storage[i.currentElement()] = value;
    }
    
    @Override
    public double getDouble(final int index) {
        throw new ForbiddenConversionException();
    }
    
    @Override
    public void setDouble(final int index, final double value) {
        throw new ForbiddenConversionException();
    }
    
    @Override
    public float getFloat(final int index) {
        throw new ForbiddenConversionException();
    }
    
    @Override
    public void setFloat(final int index, final float value) {
        throw new ForbiddenConversionException();
    }
    
    @Override
    public long getLong(final int index) {
        throw new ForbiddenConversionException();
    }
    
    @Override
    public void setLong(final int index, final long value) {
        throw new ForbiddenConversionException();
    }
    
    @Override
    public int getInt(final int index) {
        throw new ForbiddenConversionException();
    }
    
    @Override
    public void setInt(final int index, final int value) {
        throw new ForbiddenConversionException();
    }
    
    @Override
    public short getShort(final int index) {
        throw new ForbiddenConversionException();
    }
    
    @Override
    public void setShort(final int index, final short value) {
        throw new ForbiddenConversionException();
    }
    
    @Override
    public byte getByte(final int index) {
        throw new ForbiddenConversionException();
    }
    
    @Override
    public void setByte(final int index, final byte value) {
        throw new ForbiddenConversionException();
    }
    
    @Override
    public char getChar(final int index) {
        throw new ForbiddenConversionException();
    }
    
    @Override
    public void setChar(final int index, final char value) {
        throw new ForbiddenConversionException();
    }
    
    @Override
    public boolean getBoolean(final int index) {
        throw new ForbiddenConversionException();
    }
    
    @Override
    public void setBoolean(final int index, final boolean value) {
        throw new ForbiddenConversionException();
    }
    
    @Override
    public Object getObject(final int index) {
        return this.storage[index];
    }
    
    @Override
    public void setObject(final int index, final Object value) {
        this.storage[index] = value;
    }
    
    public static class D0 extends ArrayObject
    {
        private Index0D ix;
        
        public D0(final Class classType) {
            super(classType, new int[0]);
            this.ix = (Index0D)this.indexCalc;
        }
        
        private D0(final Class classType, final Index i, final Object[] store) {
            super(classType, i, store);
            this.ix = (Index0D)this.indexCalc;
        }
        
        public Object get() {
            return this.storage[this.ix.currentElement()];
        }
        
        public void set(final Object value) {
            this.storage[this.ix.currentElement()] = value;
        }
    }
    
    public static class D1 extends ArrayObject
    {
        private Index1D ix;
        
        public D1(final Class classType, final int len0) {
            super(classType, new int[] { len0 });
            this.ix = (Index1D)this.indexCalc;
        }
        
        private D1(final Class classType, final Index i, final Object[] store) {
            super(classType, i, store);
            this.ix = (Index1D)this.indexCalc;
        }
        
        public Object get(final int i) {
            return this.storage[this.ix.setDirect(i)];
        }
        
        public void set(final int i, final Object value) {
            this.storage[this.ix.setDirect(i)] = value;
        }
    }
    
    public static class D2 extends ArrayObject
    {
        private Index2D ix;
        
        public D2(final Class classType, final int len0, final int len1) {
            super(classType, new int[] { len0, len1 });
            this.ix = (Index2D)this.indexCalc;
        }
        
        private D2(final Class classType, final Index i, final Object[] store) {
            super(classType, i, store);
            this.ix = (Index2D)this.indexCalc;
        }
        
        public Object get(final int i, final int j) {
            return this.storage[this.ix.setDirect(i, j)];
        }
        
        public void set(final int i, final int j, final Object value) {
            this.storage[this.ix.setDirect(i, j)] = value;
        }
    }
    
    public static class D3 extends ArrayObject
    {
        private Index3D ix;
        
        public D3(final Class classType, final int len0, final int len1, final int len2) {
            super(classType, new int[] { len0, len1, len2 });
            this.ix = (Index3D)this.indexCalc;
        }
        
        private D3(final Class classType, final Index i, final Object[] store) {
            super(classType, i, store);
            this.ix = (Index3D)this.indexCalc;
        }
        
        public Object get(final int i, final int j, final int k) {
            return this.storage[this.ix.setDirect(i, j, k)];
        }
        
        public void set(final int i, final int j, final int k, final Object value) {
            this.storage[this.ix.setDirect(i, j, k)] = value;
        }
    }
    
    public static class D4 extends ArrayObject
    {
        private Index4D ix;
        
        public D4(final Class classType, final int len0, final int len1, final int len2, final int len3) {
            super(classType, new int[] { len0, len1, len2, len3 });
            this.ix = (Index4D)this.indexCalc;
        }
        
        private D4(final Class classType, final Index i, final Object[] store) {
            super(classType, i, store);
            this.ix = (Index4D)this.indexCalc;
        }
        
        public Object get(final int i, final int j, final int k, final int l) {
            return this.storage[this.ix.setDirect(i, j, k, l)];
        }
        
        public void set(final int i, final int j, final int k, final int l, final Object value) {
            this.storage[this.ix.setDirect(i, j, k, l)] = value;
        }
    }
    
    public static class D5 extends ArrayObject
    {
        private Index5D ix;
        
        public D5(final Class classType, final int len0, final int len1, final int len2, final int len3, final int len4) {
            super(classType, new int[] { len0, len1, len2, len3, len4 });
            this.ix = (Index5D)this.indexCalc;
        }
        
        private D5(final Class classType, final Index i, final Object[] store) {
            super(classType, i, store);
            this.ix = (Index5D)this.indexCalc;
        }
        
        public Object get(final int i, final int j, final int k, final int l, final int m) {
            return this.storage[this.ix.setDirect(i, j, k, l, m)];
        }
        
        public void set(final int i, final int j, final int k, final int l, final int m, final Object value) {
            this.storage[this.ix.setDirect(i, j, k, l, m)] = value;
        }
    }
    
    public static class D6 extends ArrayObject
    {
        private Index6D ix;
        
        public D6(final Class classType, final int len0, final int len1, final int len2, final int len3, final int len4, final int len5) {
            super(classType, new int[] { len0, len1, len2, len3, len4, len5 });
            this.ix = (Index6D)this.indexCalc;
        }
        
        private D6(final Class classType, final Index i, final Object[] store) {
            super(classType, i, store);
            this.ix = (Index6D)this.indexCalc;
        }
        
        public Object get(final int i, final int j, final int k, final int l, final int m, final int n) {
            return this.storage[this.ix.setDirect(i, j, k, l, m, n)];
        }
        
        public void set(final int i, final int j, final int k, final int l, final int m, final int n, final Object value) {
            this.storage[this.ix.setDirect(i, j, k, l, m, n)] = value;
        }
    }
    
    public static class D7 extends ArrayObject
    {
        private Index7D ix;
        
        public D7(final Class classType, final int len0, final int len1, final int len2, final int len3, final int len4, final int len5, final int len6) {
            super(classType, new int[] { len0, len1, len2, len3, len4, len5, len6 });
            this.ix = (Index7D)this.indexCalc;
        }
        
        private D7(final Class classType, final Index i, final Object[] store) {
            super(classType, i, store);
            this.ix = (Index7D)this.indexCalc;
        }
        
        public Object get(final int i, final int j, final int k, final int l, final int m, final int n, final int o) {
            return this.storage[this.ix.setDirect(i, j, k, l, m, n, o)];
        }
        
        public void set(final int i, final int j, final int k, final int l, final int m, final int n, final int o, final Object value) {
            this.storage[this.ix.setDirect(i, j, k, l, m, n, o)] = value;
        }
    }
}
