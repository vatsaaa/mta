// 
// Decompiled by Procyon v0.5.36
// 

package ucar.ma2;

import java.util.Formatter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class StructureMembers
{
    private String name;
    private Map<String, Member> memberHash;
    private List<Member> members;
    private int structureSize;
    
    public StructureMembers(final String name) {
        this.structureSize = -1;
        this.name = name;
        this.members = new ArrayList<Member>();
    }
    
    public StructureMembers(final StructureMembers from) {
        this.structureSize = -1;
        this.name = from.name;
        this.members = new ArrayList<Member>(from.getMembers().size());
        for (final Member m : from.members) {
            final Member nm = new Member(m);
            this.addMember(nm);
            if (m.members != null) {
                nm.members = new StructureMembers(m.members);
            }
        }
    }
    
    public String getName() {
        return this.name;
    }
    
    public void addMember(final Member m) {
        this.members.add(m);
        if (this.memberHash != null) {
            this.memberHash.put(m.getName(), m);
        }
    }
    
    public Member addMember(final String name, final String desc, final String units, final DataType dtype, final int[] shape) {
        final Member m = new Member(name, desc, units, dtype, shape);
        this.addMember(m);
        return m;
    }
    
    public void hideMember(final Member m) {
        if (m == null) {
            return;
        }
        this.members.remove(m);
    }
    
    public int getStructureSize() {
        if (this.structureSize < 0) {
            this.calcStructureSize();
        }
        return this.structureSize;
    }
    
    private void calcStructureSize() {
        this.structureSize = 0;
        for (final Member member : this.members) {
            this.structureSize += member.getSizeBytes();
        }
    }
    
    public void setStructureSize(final int structureSize) {
        this.structureSize = structureSize;
    }
    
    public List<Member> getMembers() {
        return this.members;
    }
    
    public List<String> getMemberNames() {
        final List<String> memberNames = new ArrayList<String>();
        for (final Member m : this.members) {
            memberNames.add(m.getName());
        }
        return memberNames;
    }
    
    public Member getMember(final int index) {
        return this.members.get(index);
    }
    
    public Member findMember(final String memberName) {
        if (memberName == null) {
            return null;
        }
        if (this.memberHash == null) {
            final int initial_capacity = (int)(this.members.size() / 0.75) + 1;
            this.memberHash = new HashMap<String, Member>(initial_capacity);
            for (final Member m : this.members) {
                this.memberHash.put(m.getName(), m);
            }
        }
        return this.memberHash.get(memberName);
    }
    
    public class Member
    {
        private String name;
        private String desc;
        private String units;
        private DataType dtype;
        private int size;
        private int[] shape;
        private StructureMembers members;
        private Array dataArray;
        private Object dataObject;
        private int dataParam;
        
        public Member(final String name, final String desc, final String units, final DataType dtype, final int[] shape) {
            this.size = 1;
            this.name = name;
            this.desc = desc;
            this.units = units;
            this.dtype = dtype;
            this.shape = shape;
            this.size = (int)Index.computeSize(shape);
        }
        
        public Member(final Member from) {
            this.size = 1;
            this.name = from.name;
            this.desc = from.desc;
            this.units = from.units;
            this.dtype = from.dtype;
            this.shape = from.shape;
            this.size = (int)Index.computeSize(this.shape);
            this.members = from.members;
        }
        
        public void setStructureMembers(final StructureMembers members) {
            this.members = members;
        }
        
        public StructureMembers getStructureMembers() {
            return this.members;
        }
        
        public void setShape(final int[] shape) {
            this.shape = shape;
            this.size = (int)Index.computeSize(shape);
        }
        
        public String getName() {
            return this.name;
        }
        
        public String getUnitsString() {
            return this.units;
        }
        
        public String getDescription() {
            return this.desc;
        }
        
        public DataType getDataType() {
            return this.dtype;
        }
        
        public int[] getShape() {
            return this.shape;
        }
        
        public int getSize() {
            return this.size;
        }
        
        public int getSizeBytes() {
            if (this.getDataType() == DataType.SEQUENCE) {
                return this.getDataType().getSize();
            }
            if (this.getDataType() == DataType.STRING) {
                return this.getDataType().getSize();
            }
            if (this.getDataType() == DataType.STRUCTURE) {
                return this.size * this.members.getStructureSize();
            }
            return this.size * this.getDataType().getSize();
        }
        
        public boolean isScalar() {
            return this.size == 1;
        }
        
        public int getDataParam() {
            return this.dataParam;
        }
        
        public void setDataParam(final int dataParam) {
            this.dataParam = dataParam;
        }
        
        public Array getDataArray() {
            return this.dataArray;
        }
        
        public void setDataArray(final Array data) {
            this.dataArray = data;
        }
        
        public Object getDataObject() {
            return this.dataObject;
        }
        
        public void setDataObject(final Object o) {
            this.dataObject = o;
        }
        
        public void setVariableInfo(final String vname, final String desc, final String unitString, final DataType dtype) {
            if (!this.name.equals(vname) && StructureMembers.this.memberHash != null) {
                StructureMembers.this.memberHash.remove(this.name);
                StructureMembers.this.memberHash.put(vname, this);
            }
            this.name = vname;
            if (dtype != null) {
                this.dtype = dtype;
            }
            if (unitString != null) {
                this.units = unitString;
            }
            if (desc != null) {
                this.desc = desc;
            }
        }
        
        public void showInternal(final Formatter f, final String leadingSpace) {
            f.format("%sname='%s' desc='%s' units='%s' dtype=%s size=%d dataObject=%s dataParam=%d", leadingSpace, this.name, this.desc, this.units, this.dtype, this.size, this.dataObject, this.dataParam);
            if (this.members != null) {
                f.format("%n%sNested members %s%n", leadingSpace, this.members.getName());
                for (final Member m : this.members.getMembers()) {
                    m.showInternal(f, leadingSpace + "  ");
                }
            }
            f.format("%n", new Object[0]);
        }
    }
}
