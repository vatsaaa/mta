// 
// Decompiled by Procyon v0.5.36
// 

package ucar.ma2;

import java.util.Iterator;
import java.util.Formatter;
import java.util.HashMap;
import java.util.Map;

public class StructureDataW extends StructureData
{
    protected Map<StructureMembers.Member, Array> memberData;
    
    public StructureDataW(final StructureMembers members) {
        super(members);
        this.memberData = new HashMap<StructureMembers.Member, Array>();
    }
    
    public void setMemberData(final StructureMembers.Member m, final Array data) {
        if (data == null) {
            throw new IllegalArgumentException("data cant be null");
        }
        this.memberData.put(m, data);
    }
    
    public void setMemberData(final String memberName, final Array data) {
        final StructureMembers.Member m = this.members.findMember(memberName);
        if (m == null) {
            throw new IllegalArgumentException("illegal member name =" + memberName);
        }
        this.setMemberData(m, data);
    }
    
    @Override
    public Array getArray(final StructureMembers.Member m) {
        if (m == null) {
            throw new IllegalArgumentException("member is null");
        }
        return this.memberData.get(m);
    }
    
    @Override
    public float convertScalarFloat(final StructureMembers.Member m) {
        return this.getScalarFloat(m);
    }
    
    @Override
    public double convertScalarDouble(final StructureMembers.Member m) {
        return this.getScalarDouble(m);
    }
    
    @Override
    public int convertScalarInt(final StructureMembers.Member m) {
        return this.getScalarInt(m);
    }
    
    @Override
    public double getScalarDouble(final StructureMembers.Member m) {
        final Array data = this.getArray(m);
        return data.getDouble(Index.scalarIndexImmutable);
    }
    
    @Override
    public double[] getJavaArrayDouble(final StructureMembers.Member m) {
        final Array data = this.getArray(m);
        return (double[])data.getStorage();
    }
    
    @Override
    public float getScalarFloat(final StructureMembers.Member m) {
        final Array data = this.getArray(m);
        return data.getFloat(Index.scalarIndexImmutable);
    }
    
    @Override
    public float[] getJavaArrayFloat(final StructureMembers.Member m) {
        final Array data = this.getArray(m);
        return (float[])data.getStorage();
    }
    
    @Override
    public byte getScalarByte(final StructureMembers.Member m) {
        final Array data = this.getArray(m);
        return data.getByte(Index.scalarIndexImmutable);
    }
    
    @Override
    public byte[] getJavaArrayByte(final StructureMembers.Member m) {
        final Array data = this.getArray(m);
        return (byte[])data.getStorage();
    }
    
    @Override
    public int getScalarInt(final StructureMembers.Member m) {
        final Array data = this.getArray(m);
        return data.getInt(Index.scalarIndexImmutable);
    }
    
    @Override
    public int[] getJavaArrayInt(final StructureMembers.Member m) {
        final Array data = this.getArray(m);
        return (int[])data.getStorage();
    }
    
    @Override
    public short getScalarShort(final StructureMembers.Member m) {
        final Array data = this.getArray(m);
        return data.getShort(Index.scalarIndexImmutable);
    }
    
    @Override
    public short[] getJavaArrayShort(final StructureMembers.Member m) {
        final Array data = this.getArray(m);
        return (short[])data.getStorage();
    }
    
    @Override
    public long getScalarLong(final StructureMembers.Member m) {
        final Array data = this.getArray(m);
        return data.getLong(Index.scalarIndexImmutable);
    }
    
    @Override
    public long[] getJavaArrayLong(final StructureMembers.Member m) {
        final Array data = this.getArray(m);
        return (long[])data.getStorage();
    }
    
    @Override
    public char getScalarChar(final StructureMembers.Member m) {
        final Array data = this.getArray(m);
        return data.getChar(Index.scalarIndexImmutable);
    }
    
    @Override
    public char[] getJavaArrayChar(final StructureMembers.Member m) {
        final Array data = this.getArray(m);
        return (char[])data.getStorage();
    }
    
    @Override
    public String getScalarString(final StructureMembers.Member m) {
        if (m.getDataType() == DataType.STRING) {
            final Array data = this.getArray(m);
            return (String)data.getObject(0);
        }
        char[] ba;
        int count;
        for (ba = this.getJavaArrayChar(m), count = 0; count < ba.length && '\0' != ba[count]; ++count) {}
        return new String(ba, 0, count);
    }
    
    @Override
    public String[] getJavaArrayString(final StructureMembers.Member m) {
        if (m.getDataType() == DataType.STRING) {
            final Array data = this.getArray(m);
            final int n = m.getSize();
            final String[] result = new String[n];
            for (int i = 0; i < result.length; ++i) {
                result[i] = (String)data.getObject(i);
            }
            return result;
        }
        if (m.getDataType() == DataType.CHAR) {
            final ArrayChar data2 = (ArrayChar)this.getArray(m);
            final ArrayChar.StringIterator iter = data2.getStringIterator();
            final String[] result = new String[iter.getNumElems()];
            int count = 0;
            while (iter.hasNext()) {
                result[count++] = iter.next();
            }
            return result;
        }
        throw new IllegalArgumentException("getJavaArrayString: not String DataType :" + m.getDataType());
    }
    
    @Override
    public StructureData getScalarStructure(final StructureMembers.Member m) {
        final ArrayStructure data = (ArrayStructure)this.getArray(m);
        return data.getStructureData(0);
    }
    
    @Override
    public ArrayStructure getArrayStructure(final StructureMembers.Member m) {
        return (ArrayStructure)this.getArray(m);
    }
    
    @Override
    public ArraySequence getArraySequence(final StructureMembers.Member m) {
        return (ArraySequence)this.getArray(m);
    }
    
    @Override
    public void showInternal(final Formatter f, final String leadingSpace) {
        super.showInternal(f, leadingSpace);
        for (final StructureMembers.Member m : this.memberData.keySet()) {
            final Array data = this.memberData.get(m);
            f.format("%s %s = %s%n", leadingSpace, m, data);
        }
    }
}
