// 
// Decompiled by Procyon v0.5.36
// 

package ucar.ma2;

public interface IndexIterator
{
    boolean hasNext();
    
    double getDoubleNext();
    
    void setDoubleNext(final double p0);
    
    double getDoubleCurrent();
    
    void setDoubleCurrent(final double p0);
    
    float getFloatNext();
    
    void setFloatNext(final float p0);
    
    float getFloatCurrent();
    
    void setFloatCurrent(final float p0);
    
    long getLongNext();
    
    void setLongNext(final long p0);
    
    long getLongCurrent();
    
    void setLongCurrent(final long p0);
    
    int getIntNext();
    
    void setIntNext(final int p0);
    
    int getIntCurrent();
    
    void setIntCurrent(final int p0);
    
    short getShortNext();
    
    void setShortNext(final short p0);
    
    short getShortCurrent();
    
    void setShortCurrent(final short p0);
    
    byte getByteNext();
    
    void setByteNext(final byte p0);
    
    byte getByteCurrent();
    
    void setByteCurrent(final byte p0);
    
    char getCharNext();
    
    void setCharNext(final char p0);
    
    char getCharCurrent();
    
    void setCharCurrent(final char p0);
    
    boolean getBooleanNext();
    
    void setBooleanNext(final boolean p0);
    
    boolean getBooleanCurrent();
    
    void setBooleanCurrent(final boolean p0);
    
    Object getObjectNext();
    
    void setObjectNext(final Object p0);
    
    Object getObjectCurrent();
    
    void setObjectCurrent(final Object p0);
    
    Object next();
    
    int[] getCurrentCounter();
}
