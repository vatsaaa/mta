// 
// Decompiled by Procyon v0.5.36
// 

package ucar.ma2;

import java.io.IOException;

public interface StructureDataIterator
{
    boolean hasNext() throws IOException;
    
    StructureData next() throws IOException;
    
    void setBufferSize(final int p0);
    
    StructureDataIterator reset();
    
    int getCurrentRecno();
}
