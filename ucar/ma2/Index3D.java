// 
// Decompiled by Procyon v0.5.36
// 

package ucar.ma2;

public class Index3D extends Index
{
    private int curr0;
    private int curr1;
    private int curr2;
    private int stride0;
    private int stride1;
    private int stride2;
    private int shape0;
    private int shape1;
    private int shape2;
    
    Index3D() {
        super(3);
    }
    
    public Index3D(final int[] shape) {
        super(shape);
        this.precalc();
    }
    
    @Override
    protected void precalc() {
        this.shape0 = this.shape[0];
        this.shape1 = this.shape[1];
        this.shape2 = this.shape[2];
        this.stride0 = this.stride[0];
        this.stride1 = this.stride[1];
        this.stride2 = this.stride[2];
        this.curr0 = this.current[0];
        this.curr1 = this.current[1];
        this.curr2 = this.current[2];
    }
    
    @Override
    public int[] getCurrentCounter() {
        this.current[0] = this.curr0;
        this.current[1] = this.curr1;
        this.current[2] = this.curr2;
        return this.current.clone();
    }
    
    @Override
    public String toString() {
        return this.curr0 + "," + this.curr1 + "," + this.curr2;
    }
    
    @Override
    public int currentElement() {
        return this.offset + this.curr0 * this.stride0 + this.curr1 * this.stride1 + this.curr2 * this.stride2;
    }
    
    @Override
    public int incr() {
        if (++this.curr2 >= this.shape2) {
            this.curr2 = 0;
            if (++this.curr1 >= this.shape1) {
                this.curr1 = 0;
                if (++this.curr0 >= this.shape0) {
                    this.curr0 = 0;
                }
            }
        }
        return this.offset + this.curr0 * this.stride0 + this.curr1 * this.stride1 + this.curr2 * this.stride2;
    }
    
    @Override
    public void setDim(final int dim, final int value) {
        if (value < 0 || value >= this.shape[dim]) {
            throw new ArrayIndexOutOfBoundsException();
        }
        if (dim == 2) {
            this.curr2 = value;
        }
        else if (dim == 1) {
            this.curr1 = value;
        }
        else {
            this.curr0 = value;
        }
    }
    
    @Override
    public Index set0(final int v) {
        if (v < 0 || v >= this.shape0) {
            throw new ArrayIndexOutOfBoundsException();
        }
        this.curr0 = v;
        return this;
    }
    
    @Override
    public Index set1(final int v) {
        if (v < 0 || v >= this.shape1) {
            throw new ArrayIndexOutOfBoundsException();
        }
        this.curr1 = v;
        return this;
    }
    
    @Override
    public Index set2(final int v) {
        if (v < 0 || v >= this.shape2) {
            throw new ArrayIndexOutOfBoundsException();
        }
        this.curr2 = v;
        return this;
    }
    
    @Override
    public Index set(final int v0, final int v1, final int v2) {
        this.set0(v0);
        this.set1(v1);
        this.set2(v2);
        return this;
    }
    
    @Override
    public Index set(final int[] index) {
        if (index.length != this.rank) {
            throw new ArrayIndexOutOfBoundsException();
        }
        this.set0(index[0]);
        this.set1(index[1]);
        this.set2(index[2]);
        return this;
    }
    
    @Override
    public Object clone() {
        return super.clone();
    }
    
    int setDirect(final int v0, final int v1, final int v2) {
        if (v0 < 0 || v0 >= this.shape0) {
            throw new ArrayIndexOutOfBoundsException();
        }
        if (v1 < 0 || v1 >= this.shape1) {
            throw new ArrayIndexOutOfBoundsException();
        }
        if (v2 < 0 || v2 >= this.shape2) {
            throw new ArrayIndexOutOfBoundsException();
        }
        return this.offset + v0 * this.stride0 + v1 * this.stride1 + v2 * this.stride2;
    }
}
