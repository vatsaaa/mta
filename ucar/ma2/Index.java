// 
// Decompiled by Procyon v0.5.36
// 

package ucar.ma2;

import java.util.Iterator;
import java.util.List;

public class Index implements Cloneable
{
    public static final Index0D scalarIndexImmutable;
    protected int[] shape;
    protected int[] stride;
    protected int rank;
    protected long size;
    protected int offset;
    private boolean fastIterator;
    protected int[] current;
    
    public static Index factory(final int[] shape) {
        final int rank = shape.length;
        switch (rank) {
            case 0: {
                return new Index0D();
            }
            case 1: {
                return new Index1D(shape);
            }
            case 2: {
                return new Index2D(shape);
            }
            case 3: {
                return new Index3D(shape);
            }
            case 4: {
                return new Index4D(shape);
            }
            case 5: {
                return new Index5D(shape);
            }
            case 6: {
                return new Index6D(shape);
            }
            case 7: {
                return new Index7D(shape);
            }
            default: {
                return new Index(shape);
            }
        }
    }
    
    private static Index factory(final int rank) {
        switch (rank) {
            case 0: {
                return new Index0D();
            }
            case 1: {
                return new Index1D();
            }
            case 2: {
                return new Index2D();
            }
            case 3: {
                return new Index3D();
            }
            case 4: {
                return new Index4D();
            }
            case 5: {
                return new Index5D();
            }
            case 6: {
                return new Index6D();
            }
            case 7: {
                return new Index7D();
            }
            default: {
                return new Index(rank);
            }
        }
    }
    
    public static long computeSize(final int[] shape) {
        long product = 1L;
        for (int ii = shape.length - 1; ii >= 0; --ii) {
            product *= shape[ii];
        }
        return product;
    }
    
    private static long computeStrides(final int[] shape, final int[] stride) {
        long product = 1L;
        for (int ii = shape.length - 1; ii >= 0; --ii) {
            final int thisDim = shape[ii];
            if (thisDim < 0) {
                throw new NegativeArraySizeException();
            }
            stride[ii] = (int)product;
            product *= thisDim;
        }
        return product;
    }
    
    protected Index(final int rank) {
        this.fastIterator = true;
        this.rank = rank;
        this.shape = new int[rank];
        this.current = new int[rank];
        this.stride = new int[rank];
    }
    
    protected Index(final int[] _shape) {
        this.fastIterator = true;
        System.arraycopy(_shape, 0, this.shape = new int[_shape.length], 0, _shape.length);
        this.rank = this.shape.length;
        this.current = new int[this.rank];
        this.stride = new int[this.rank];
        this.size = computeStrides(this.shape, this.stride);
        this.offset = 0;
    }
    
    public Index(final int[] _shape, final int[] _stride) {
        this.fastIterator = true;
        System.arraycopy(_shape, 0, this.shape = new int[_shape.length], 0, _shape.length);
        System.arraycopy(_stride, 0, this.stride = new int[_stride.length], 0, _stride.length);
        this.rank = this.shape.length;
        this.current = new int[this.rank];
        this.size = computeSize(this.shape);
        this.offset = 0;
    }
    
    protected void precalc() {
    }
    
    Index flip(final int index) {
        if (index < 0 || index >= this.rank) {
            throw new IllegalArgumentException();
        }
        final Index index2;
        final Index i = index2 = (Index)this.clone();
        index2.offset += this.stride[index] * (this.shape[index] - 1);
        i.stride[index] = -this.stride[index];
        i.fastIterator = false;
        i.precalc();
        return i;
    }
    
    Index section(final List<Range> ranges) throws InvalidRangeException {
        if (ranges.size() != this.rank) {
            throw new InvalidRangeException("Bad ranges [] length");
        }
        for (int ii = 0; ii < this.rank; ++ii) {
            final Range r = ranges.get(ii);
            if (r != null) {
                if (r.first() < 0 || r.first() >= this.shape[ii]) {
                    throw new InvalidRangeException("Bad range starting value at index " + ii + " == " + r.first());
                }
                if (r.last() < 0 || r.last() >= this.shape[ii]) {
                    throw new InvalidRangeException("Bad range ending value at index " + ii + " == " + r.last());
                }
            }
        }
        int reducedRank = this.rank;
        for (final Range r2 : ranges) {
            if (r2 != null && r2.length() == 1) {
                --reducedRank;
            }
        }
        final Index newindex = factory(reducedRank);
        newindex.offset = this.offset;
        int newDim = 0;
        for (int ii2 = 0; ii2 < this.rank; ++ii2) {
            final Range r3 = ranges.get(ii2);
            if (r3 == null) {
                newindex.shape[newDim] = this.shape[ii2];
                newindex.stride[newDim] = this.stride[ii2];
                ++newDim;
            }
            else if (r3.length() != 1) {
                newindex.shape[newDim] = r3.length();
                newindex.stride[newDim] = this.stride[ii2] * r3.stride();
                final Index index = newindex;
                index.offset += this.stride[ii2] * r3.first();
                ++newDim;
            }
            else {
                final Index index2 = newindex;
                index2.offset += this.stride[ii2] * r3.first();
            }
        }
        newindex.size = computeSize(newindex.shape);
        newindex.fastIterator = (this.fastIterator && newindex.size == this.size);
        newindex.precalc();
        return newindex;
    }
    
    Index sectionNoReduce(final List<Range> ranges) throws InvalidRangeException {
        if (ranges.size() != this.rank) {
            throw new InvalidRangeException("Bad ranges [] length");
        }
        for (int ii = 0; ii < this.rank; ++ii) {
            final Range r = ranges.get(ii);
            if (r != null) {
                if (r.first() < 0 || r.first() >= this.shape[ii]) {
                    throw new InvalidRangeException("Bad range starting value at index " + ii + " == " + r.first());
                }
                if (r.last() < 0 || r.last() >= this.shape[ii]) {
                    throw new InvalidRangeException("Bad range ending value at index " + ii + " == " + r.last());
                }
            }
        }
        final Index newindex = factory(this.rank);
        newindex.offset = this.offset;
        for (int ii2 = 0; ii2 < this.rank; ++ii2) {
            final Range r2 = ranges.get(ii2);
            if (r2 == null) {
                newindex.shape[ii2] = this.shape[ii2];
                newindex.stride[ii2] = this.stride[ii2];
            }
            else {
                newindex.shape[ii2] = r2.length();
                newindex.stride[ii2] = this.stride[ii2] * r2.stride();
                final Index index = newindex;
                index.offset += this.stride[ii2] * r2.first();
            }
        }
        newindex.size = computeSize(newindex.shape);
        newindex.fastIterator = (this.fastIterator && newindex.size == this.size);
        newindex.precalc();
        return newindex;
    }
    
    Index reduce() {
        final Index c = this;
        for (int ii = 0; ii < this.rank; ++ii) {
            if (this.shape[ii] == 1) {
                final Index newc = c.reduce(ii);
                return newc.reduce();
            }
        }
        return c;
    }
    
    Index reduce(final int dim) {
        if (dim < 0 || dim >= this.rank) {
            throw new IllegalArgumentException("illegal reduce dim " + dim);
        }
        if (this.shape[dim] != 1) {
            throw new IllegalArgumentException("illegal reduce dim " + dim + " : length != 1");
        }
        final Index newindex = factory(this.rank - 1);
        newindex.offset = this.offset;
        int count = 0;
        for (int ii = 0; ii < this.rank; ++ii) {
            if (ii != dim) {
                newindex.shape[count] = this.shape[ii];
                newindex.stride[count] = this.stride[ii];
                ++count;
            }
        }
        newindex.size = computeSize(newindex.shape);
        newindex.fastIterator = this.fastIterator;
        newindex.precalc();
        return newindex;
    }
    
    Index transpose(final int index1, final int index2) {
        if (index1 < 0 || index1 >= this.rank) {
            throw new IllegalArgumentException();
        }
        if (index2 < 0 || index2 >= this.rank) {
            throw new IllegalArgumentException();
        }
        final Index newIndex = (Index)this.clone();
        newIndex.stride[index1] = this.stride[index2];
        newIndex.stride[index2] = this.stride[index1];
        newIndex.shape[index1] = this.shape[index2];
        newIndex.shape[index2] = this.shape[index1];
        newIndex.fastIterator = false;
        newIndex.precalc();
        return newIndex;
    }
    
    Index permute(final int[] dims) {
        if (dims.length != this.shape.length) {
            throw new IllegalArgumentException();
        }
        for (final int dim : dims) {
            if (dim < 0 || dim >= this.rank) {
                throw new IllegalArgumentException();
            }
        }
        boolean isPermuted = false;
        final Index newIndex = (Index)this.clone();
        for (int i = 0; i < dims.length; ++i) {
            newIndex.stride[i] = this.stride[dims[i]];
            newIndex.shape[i] = this.shape[dims[i]];
            if (i != dims[i]) {
                isPermuted = true;
            }
        }
        newIndex.fastIterator = (this.fastIterator && !isPermuted);
        newIndex.precalc();
        return newIndex;
    }
    
    public int getRank() {
        return this.rank;
    }
    
    public int[] getShape() {
        final int[] result = new int[this.shape.length];
        System.arraycopy(this.shape, 0, result, 0, this.shape.length);
        return result;
    }
    
    public int getShape(final int index) {
        return this.shape[index];
    }
    
    IndexIterator getIndexIterator(final Array maa) {
        if (this.fastIterator) {
            return new IteratorFast(this.size, maa);
        }
        return new IteratorImpl(maa);
    }
    
    IteratorFast getIndexIteratorFast(final Array maa) {
        return new IteratorFast(this.size, maa);
    }
    
    boolean isFastIterator() {
        return this.fastIterator;
    }
    
    public long getSize() {
        return this.size;
    }
    
    public int currentElement() {
        int value = this.offset;
        for (int ii = 0; ii < this.rank; ++ii) {
            value += this.current[ii] * this.stride[ii];
        }
        return value;
    }
    
    public int[] getCurrentCounter() {
        return this.current.clone();
    }
    
    public void setCurrentCounter(int currElement) {
        currElement -= this.offset;
        for (int ii = 0; ii < this.rank; ++ii) {
            this.current[ii] = currElement / this.stride[ii];
            currElement -= this.current[ii] * this.stride[ii];
        }
        this.set(this.current);
    }
    
    public int incr() {
        for (int digit = this.rank - 1; digit >= 0; --digit) {
            final int[] current = this.current;
            final int n = digit;
            ++current[n];
            if (this.current[digit] < this.shape[digit]) {
                break;
            }
            this.current[digit] = 0;
        }
        return this.currentElement();
    }
    
    public Index set(final int[] index) {
        if (index.length != this.rank) {
            throw new ArrayIndexOutOfBoundsException();
        }
        System.arraycopy(index, 0, this.current, 0, this.rank);
        return this;
    }
    
    public void setDim(final int dim, final int value) {
        if (value < 0 || value >= this.shape[dim]) {
            throw new ArrayIndexOutOfBoundsException();
        }
        this.current[dim] = value;
    }
    
    public Index set0(final int v) {
        this.setDim(0, v);
        return this;
    }
    
    public Index set1(final int v) {
        this.setDim(1, v);
        return this;
    }
    
    public Index set2(final int v) {
        this.setDim(2, v);
        return this;
    }
    
    public Index set3(final int v) {
        this.setDim(3, v);
        return this;
    }
    
    public Index set4(final int v) {
        this.setDim(4, v);
        return this;
    }
    
    public Index set5(final int v) {
        this.setDim(5, v);
        return this;
    }
    
    public Index set6(final int v) {
        this.setDim(6, v);
        return this;
    }
    
    public Index set(final int v0) {
        this.setDim(0, v0);
        return this;
    }
    
    public Index set(final int v0, final int v1) {
        this.setDim(0, v0);
        this.setDim(1, v1);
        return this;
    }
    
    public Index set(final int v0, final int v1, final int v2) {
        this.setDim(0, v0);
        this.setDim(1, v1);
        this.setDim(2, v2);
        return this;
    }
    
    public Index set(final int v0, final int v1, final int v2, final int v3) {
        this.setDim(0, v0);
        this.setDim(1, v1);
        this.setDim(2, v2);
        this.setDim(3, v3);
        return this;
    }
    
    public Index set(final int v0, final int v1, final int v2, final int v3, final int v4) {
        this.setDim(0, v0);
        this.setDim(1, v1);
        this.setDim(2, v2);
        this.setDim(3, v3);
        this.setDim(4, v4);
        return this;
    }
    
    public Index set(final int v0, final int v1, final int v2, final int v3, final int v4, final int v5) {
        this.setDim(0, v0);
        this.setDim(1, v1);
        this.setDim(2, v2);
        this.setDim(3, v3);
        this.setDim(4, v4);
        this.setDim(5, v5);
        return this;
    }
    
    public Index set(final int v0, final int v1, final int v2, final int v3, final int v4, final int v5, final int v6) {
        this.setDim(0, v0);
        this.setDim(1, v1);
        this.setDim(2, v2);
        this.setDim(3, v3);
        this.setDim(4, v4);
        this.setDim(5, v5);
        this.setDim(6, v6);
        return this;
    }
    
    public String toStringDebug() {
        final StringBuilder sbuff = new StringBuilder(100);
        sbuff.setLength(0);
        sbuff.append(" shape= ");
        for (int ii = 0; ii < this.rank; ++ii) {
            sbuff.append(this.shape[ii]);
            sbuff.append(" ");
        }
        sbuff.append(" stride= ");
        for (int ii = 0; ii < this.rank; ++ii) {
            sbuff.append(this.stride[ii]);
            sbuff.append(" ");
        }
        sbuff.append(" offset= ").append(this.offset);
        sbuff.append(" rank= ").append(this.rank);
        sbuff.append(" size= ").append(this.size);
        sbuff.append(" current= ");
        for (int ii = 0; ii < this.rank; ++ii) {
            sbuff.append(this.current[ii]);
            sbuff.append(" ");
        }
        return sbuff.toString();
    }
    
    @Override
    public String toString() {
        final StringBuilder sbuff = new StringBuilder(100);
        sbuff.setLength(0);
        for (int ii = 0; ii < this.rank; ++ii) {
            if (ii > 0) {
                sbuff.append(",");
            }
            sbuff.append(this.current[ii]);
        }
        return sbuff.toString();
    }
    
    public Object clone() {
        Index i;
        try {
            i = (Index)super.clone();
        }
        catch (CloneNotSupportedException e) {
            return null;
        }
        i.stride = this.stride.clone();
        i.shape = this.shape.clone();
        i.current = new int[this.rank];
        return i;
    }
    
    static {
        scalarIndexImmutable = new Index0D();
    }
    
    private class IteratorImpl implements IndexIterator
    {
        private int count;
        private int currElement;
        private Index counter;
        private Array maa;
        
        private IteratorImpl(final Array maa) {
            this.count = 0;
            this.currElement = 0;
            this.maa = maa;
            this.counter = (Index)Index.this.clone();
            if (Index.this.rank > 0) {
                this.counter.current[Index.this.rank - 1] = -1;
            }
            this.counter.precalc();
        }
        
        public boolean hasNext() {
            return this.count < Index.this.size;
        }
        
        @Override
        public String toString() {
            return this.counter.toString();
        }
        
        public int[] getCurrentCounter() {
            return this.counter.getCurrentCounter();
        }
        
        public Object next() {
            ++this.count;
            this.currElement = this.counter.incr();
            return this.maa.getObject(this.currElement);
        }
        
        public double getDoubleCurrent() {
            return this.maa.getDouble(this.currElement);
        }
        
        public double getDoubleNext() {
            ++this.count;
            this.currElement = this.counter.incr();
            return this.maa.getDouble(this.currElement);
        }
        
        public void setDoubleCurrent(final double val) {
            this.maa.setDouble(this.currElement, val);
        }
        
        public void setDoubleNext(final double val) {
            ++this.count;
            this.currElement = this.counter.incr();
            this.maa.setDouble(this.currElement, val);
        }
        
        public float getFloatCurrent() {
            return this.maa.getFloat(this.currElement);
        }
        
        public float getFloatNext() {
            ++this.count;
            this.currElement = this.counter.incr();
            return this.maa.getFloat(this.currElement);
        }
        
        public void setFloatCurrent(final float val) {
            this.maa.setFloat(this.currElement, val);
        }
        
        public void setFloatNext(final float val) {
            ++this.count;
            this.currElement = this.counter.incr();
            this.maa.setFloat(this.currElement, val);
        }
        
        public long getLongCurrent() {
            return this.maa.getLong(this.currElement);
        }
        
        public long getLongNext() {
            ++this.count;
            this.currElement = this.counter.incr();
            return this.maa.getLong(this.currElement);
        }
        
        public void setLongCurrent(final long val) {
            this.maa.setLong(this.currElement, val);
        }
        
        public void setLongNext(final long val) {
            ++this.count;
            this.currElement = this.counter.incr();
            this.maa.setLong(this.currElement, val);
        }
        
        public int getIntCurrent() {
            return this.maa.getInt(this.currElement);
        }
        
        public int getIntNext() {
            ++this.count;
            this.currElement = this.counter.incr();
            return this.maa.getInt(this.currElement);
        }
        
        public void setIntCurrent(final int val) {
            this.maa.setInt(this.currElement, val);
        }
        
        public void setIntNext(final int val) {
            ++this.count;
            this.currElement = this.counter.incr();
            this.maa.setInt(this.currElement, val);
        }
        
        public short getShortCurrent() {
            return this.maa.getShort(this.currElement);
        }
        
        public short getShortNext() {
            ++this.count;
            this.currElement = this.counter.incr();
            return this.maa.getShort(this.currElement);
        }
        
        public void setShortCurrent(final short val) {
            this.maa.setShort(this.currElement, val);
        }
        
        public void setShortNext(final short val) {
            ++this.count;
            this.currElement = this.counter.incr();
            this.maa.setShort(this.currElement, val);
        }
        
        public byte getByteCurrent() {
            return this.maa.getByte(this.currElement);
        }
        
        public byte getByteNext() {
            ++this.count;
            this.currElement = this.counter.incr();
            return this.maa.getByte(this.currElement);
        }
        
        public void setByteCurrent(final byte val) {
            this.maa.setByte(this.currElement, val);
        }
        
        public void setByteNext(final byte val) {
            ++this.count;
            this.currElement = this.counter.incr();
            this.maa.setByte(this.currElement, val);
        }
        
        public char getCharCurrent() {
            return this.maa.getChar(this.currElement);
        }
        
        public char getCharNext() {
            ++this.count;
            this.currElement = this.counter.incr();
            return this.maa.getChar(this.currElement);
        }
        
        public void setCharCurrent(final char val) {
            this.maa.setChar(this.currElement, val);
        }
        
        public void setCharNext(final char val) {
            ++this.count;
            this.currElement = this.counter.incr();
            this.maa.setChar(this.currElement, val);
        }
        
        public boolean getBooleanCurrent() {
            return this.maa.getBoolean(this.currElement);
        }
        
        public boolean getBooleanNext() {
            ++this.count;
            this.currElement = this.counter.incr();
            return this.maa.getBoolean(this.currElement);
        }
        
        public void setBooleanCurrent(final boolean val) {
            this.maa.setBoolean(this.currElement, val);
        }
        
        public void setBooleanNext(final boolean val) {
            ++this.count;
            this.currElement = this.counter.incr();
            this.maa.setBoolean(this.currElement, val);
        }
        
        public Object getObjectCurrent() {
            return this.maa.getObject(this.currElement);
        }
        
        public Object getObjectNext() {
            ++this.count;
            this.currElement = this.counter.incr();
            return this.maa.getObject(this.currElement);
        }
        
        public void setObjectCurrent(final Object val) {
            this.maa.setObject(this.currElement, val);
        }
        
        public void setObjectNext(final Object val) {
            ++this.count;
            this.currElement = this.counter.incr();
            this.maa.setObject(this.currElement, val);
        }
    }
}
