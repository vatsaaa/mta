// 
// Decompiled by Procyon v0.5.36
// 

package ucar.ma2;

import java.nio.DoubleBuffer;
import java.nio.ByteBuffer;

public class ArrayDouble extends Array
{
    protected double[] storageD;
    
    static ArrayDouble factory(final Index index) {
        return factory(index, null);
    }
    
    static ArrayDouble factory(final Index index, final double[] storage) {
        switch (index.getRank()) {
            case 0: {
                return new D0(index, storage);
            }
            case 1: {
                return new D1(index, storage);
            }
            case 2: {
                return new D2(index, storage);
            }
            case 3: {
                return new D3(index, storage);
            }
            case 4: {
                return new D4(index, storage);
            }
            case 5: {
                return new D5(index, storage);
            }
            case 6: {
                return new D6(index, storage);
            }
            case 7: {
                return new D7(index, storage);
            }
            default: {
                return new ArrayDouble(index, storage);
            }
        }
    }
    
    public ArrayDouble(final int[] shape) {
        super(shape);
        this.storageD = new double[(int)this.indexCalc.getSize()];
    }
    
    @Override
    Array createView(final Index index) {
        return factory(index, this.storageD);
    }
    
    ArrayDouble(final Index ima, final double[] data) {
        super(ima);
        if (data != null) {
            this.storageD = data;
        }
        else {
            this.storageD = new double[(int)this.indexCalc.getSize()];
        }
    }
    
    @Override
    public Object getStorage() {
        return this.storageD;
    }
    
    @Override
    void copyFrom1DJavaArray(final IndexIterator iter, final Object javaArray) {
        final double[] ja = (double[])javaArray;
        for (int i = 0; i < ja.length; ++i) {
            iter.setDoubleNext(ja[i]);
        }
    }
    
    @Override
    void copyTo1DJavaArray(final IndexIterator iter, final Object javaArray) {
        final double[] ja = (double[])javaArray;
        for (int i = 0; i < ja.length; ++i) {
            ja[i] = iter.getDoubleNext();
        }
    }
    
    @Override
    public ByteBuffer getDataAsByteBuffer() {
        final ByteBuffer bb = ByteBuffer.allocate((int)(8L * this.getSize()));
        final DoubleBuffer ib = bb.asDoubleBuffer();
        ib.put((double[])this.get1DJavaArray(Double.TYPE));
        return bb;
    }
    
    @Override
    public Class getElementType() {
        return Double.TYPE;
    }
    
    public double get(final Index i) {
        return this.storageD[i.currentElement()];
    }
    
    public void set(final Index i, final double value) {
        this.storageD[i.currentElement()] = value;
    }
    
    @Override
    public double getDouble(final Index i) {
        return this.storageD[i.currentElement()];
    }
    
    @Override
    public void setDouble(final Index i, final double value) {
        this.storageD[i.currentElement()] = value;
    }
    
    @Override
    public float getFloat(final Index i) {
        return (float)this.storageD[i.currentElement()];
    }
    
    @Override
    public void setFloat(final Index i, final float value) {
        this.storageD[i.currentElement()] = value;
    }
    
    @Override
    public long getLong(final Index i) {
        return (long)this.storageD[i.currentElement()];
    }
    
    @Override
    public void setLong(final Index i, final long value) {
        this.storageD[i.currentElement()] = (double)value;
    }
    
    @Override
    public int getInt(final Index i) {
        return (int)this.storageD[i.currentElement()];
    }
    
    @Override
    public void setInt(final Index i, final int value) {
        this.storageD[i.currentElement()] = value;
    }
    
    @Override
    public short getShort(final Index i) {
        return (short)this.storageD[i.currentElement()];
    }
    
    @Override
    public void setShort(final Index i, final short value) {
        this.storageD[i.currentElement()] = value;
    }
    
    @Override
    public byte getByte(final Index i) {
        return (byte)this.storageD[i.currentElement()];
    }
    
    @Override
    public void setByte(final Index i, final byte value) {
        this.storageD[i.currentElement()] = value;
    }
    
    @Override
    public char getChar(final Index i) {
        return (char)this.storageD[i.currentElement()];
    }
    
    @Override
    public void setChar(final Index i, final char value) {
        this.storageD[i.currentElement()] = value;
    }
    
    @Override
    public boolean getBoolean(final Index i) {
        throw new ForbiddenConversionException();
    }
    
    @Override
    public void setBoolean(final Index i, final boolean value) {
        throw new ForbiddenConversionException();
    }
    
    @Override
    public Object getObject(final Index i) {
        return this.storageD[i.currentElement()];
    }
    
    @Override
    public void setObject(final Index i, final Object value) {
        this.storageD[i.currentElement()] = ((Number)value).doubleValue();
    }
    
    @Override
    public double getDouble(final int index) {
        return this.storageD[index];
    }
    
    @Override
    public void setDouble(final int index, final double value) {
        this.storageD[index] = value;
    }
    
    @Override
    public float getFloat(final int index) {
        return (float)this.storageD[index];
    }
    
    @Override
    public void setFloat(final int index, final float value) {
        this.storageD[index] = value;
    }
    
    @Override
    public long getLong(final int index) {
        return (long)this.storageD[index];
    }
    
    @Override
    public void setLong(final int index, final long value) {
        this.storageD[index] = (double)value;
    }
    
    @Override
    public int getInt(final int index) {
        return (int)this.storageD[index];
    }
    
    @Override
    public void setInt(final int index, final int value) {
        this.storageD[index] = value;
    }
    
    @Override
    public short getShort(final int index) {
        return (short)this.storageD[index];
    }
    
    @Override
    public void setShort(final int index, final short value) {
        this.storageD[index] = value;
    }
    
    @Override
    public byte getByte(final int index) {
        return (byte)this.storageD[index];
    }
    
    @Override
    public void setByte(final int index, final byte value) {
        this.storageD[index] = value;
    }
    
    @Override
    public char getChar(final int index) {
        return (char)this.storageD[index];
    }
    
    @Override
    public void setChar(final int index, final char value) {
        this.storageD[index] = value;
    }
    
    @Override
    public boolean getBoolean(final int index) {
        throw new ForbiddenConversionException();
    }
    
    @Override
    public void setBoolean(final int index, final boolean value) {
        throw new ForbiddenConversionException();
    }
    
    @Override
    public Object getObject(final int index) {
        return this.getDouble(index);
    }
    
    @Override
    public void setObject(final int index, final Object value) {
        this.storageD[index] = ((Number)value).doubleValue();
    }
    
    public static class D0 extends ArrayDouble
    {
        private Index0D ix;
        
        public D0() {
            super(new int[0]);
            this.ix = (Index0D)this.indexCalc;
        }
        
        private D0(final Index i, final double[] store) {
            super(i, store);
            this.ix = (Index0D)this.indexCalc;
        }
        
        public double get() {
            return this.storageD[this.ix.currentElement()];
        }
        
        public void set(final double value) {
            this.storageD[this.ix.currentElement()] = value;
        }
    }
    
    public static class D1 extends ArrayDouble
    {
        private Index1D ix;
        
        public D1(final int len0) {
            super(new int[] { len0 });
            this.ix = (Index1D)this.indexCalc;
        }
        
        private D1(final Index i, final double[] store) {
            super(i, store);
            this.ix = (Index1D)this.indexCalc;
        }
        
        public double get(final int i) {
            return this.storageD[this.ix.setDirect(i)];
        }
        
        public void set(final int i, final double value) {
            this.storageD[this.ix.setDirect(i)] = value;
        }
    }
    
    public static class D2 extends ArrayDouble
    {
        private Index2D ix;
        
        public D2(final int len0, final int len1) {
            super(new int[] { len0, len1 });
            this.ix = (Index2D)this.indexCalc;
        }
        
        private D2(final Index i, final double[] store) {
            super(i, store);
            this.ix = (Index2D)this.indexCalc;
        }
        
        public double get(final int i, final int j) {
            return this.storageD[this.ix.setDirect(i, j)];
        }
        
        public void set(final int i, final int j, final double value) {
            this.storageD[this.ix.setDirect(i, j)] = value;
        }
    }
    
    public static class D3 extends ArrayDouble
    {
        private Index3D ix;
        
        public D3(final int len0, final int len1, final int len2) {
            super(new int[] { len0, len1, len2 });
            this.ix = (Index3D)this.indexCalc;
        }
        
        private D3(final Index i, final double[] store) {
            super(i, store);
            this.ix = (Index3D)this.indexCalc;
        }
        
        public double get(final int i, final int j, final int k) {
            return this.storageD[this.ix.setDirect(i, j, k)];
        }
        
        public void set(final int i, final int j, final int k, final double value) {
            this.storageD[this.ix.setDirect(i, j, k)] = value;
        }
        
        public IF getIF() {
            return new IF();
        }
        
        public class IF
        {
            private int currElement;
            private int size;
            
            public IF() {
                this.currElement = -1;
                this.size = (int)D3.this.ix.getSize();
            }
            
            public boolean hasNext(final int howMany) {
                return this.currElement < this.size - howMany;
            }
            
            public double getNext() {
                return D3.this.storageD[++this.currElement];
            }
            
            public void setNext(final double val) {
                D3.this.storageD[++this.currElement] = val;
            }
        }
    }
    
    public static class D4 extends ArrayDouble
    {
        private Index4D ix;
        
        public D4(final int len0, final int len1, final int len2, final int len3) {
            super(new int[] { len0, len1, len2, len3 });
            this.ix = (Index4D)this.indexCalc;
        }
        
        private D4(final Index i, final double[] store) {
            super(i, store);
            this.ix = (Index4D)this.indexCalc;
        }
        
        public double get(final int i, final int j, final int k, final int l) {
            return this.storageD[this.ix.setDirect(i, j, k, l)];
        }
        
        public void set(final int i, final int j, final int k, final int l, final double value) {
            this.storageD[this.ix.setDirect(i, j, k, l)] = value;
        }
    }
    
    public static class D5 extends ArrayDouble
    {
        private Index5D ix;
        
        public D5(final int len0, final int len1, final int len2, final int len3, final int len4) {
            super(new int[] { len0, len1, len2, len3, len4 });
            this.ix = (Index5D)this.indexCalc;
        }
        
        private D5(final Index i, final double[] store) {
            super(i, store);
            this.ix = (Index5D)this.indexCalc;
        }
        
        public double get(final int i, final int j, final int k, final int l, final int m) {
            return this.storageD[this.ix.setDirect(i, j, k, l, m)];
        }
        
        public void set(final int i, final int j, final int k, final int l, final int m, final double value) {
            this.storageD[this.ix.setDirect(i, j, k, l, m)] = value;
        }
    }
    
    public static class D6 extends ArrayDouble
    {
        private Index6D ix;
        
        public D6(final int len0, final int len1, final int len2, final int len3, final int len4, final int len5) {
            super(new int[] { len0, len1, len2, len3, len4, len5 });
            this.ix = (Index6D)this.indexCalc;
        }
        
        private D6(final Index i, final double[] store) {
            super(i, store);
            this.ix = (Index6D)this.indexCalc;
        }
        
        public double get(final int i, final int j, final int k, final int l, final int m, final int n) {
            return this.storageD[this.ix.setDirect(i, j, k, l, m, n)];
        }
        
        public void set(final int i, final int j, final int k, final int l, final int m, final int n, final double value) {
            this.storageD[this.ix.setDirect(i, j, k, l, m, n)] = value;
        }
    }
    
    public static class D7 extends ArrayDouble
    {
        private Index7D ix;
        
        public D7(final int len0, final int len1, final int len2, final int len3, final int len4, final int len5, final int len6) {
            super(new int[] { len0, len1, len2, len3, len4, len5, len6 });
            this.ix = (Index7D)this.indexCalc;
        }
        
        private D7(final Index i, final double[] store) {
            super(i, store);
            this.ix = (Index7D)this.indexCalc;
        }
        
        public double get(final int i, final int j, final int k, final int l, final int m, final int n, final int o) {
            return this.storageD[this.ix.setDirect(i, j, k, l, m, n, o)];
        }
        
        public void set(final int i, final int j, final int k, final int l, final int m, final int n, final int o, final double value) {
            this.storageD[this.ix.setDirect(i, j, k, l, m, n, o)] = value;
        }
    }
}
