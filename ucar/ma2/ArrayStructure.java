// 
// Decompiled by Procyon v0.5.36
// 

package ucar.ma2;

import java.util.Iterator;
import java.util.Formatter;
import java.nio.ByteBuffer;
import java.io.IOException;
import java.util.List;

public abstract class ArrayStructure extends Array
{
    protected StructureMembers members;
    protected int nelems;
    protected StructureData[] sdata;
    
    protected ArrayStructure(final StructureMembers members, final int[] shape) {
        super(shape);
        this.members = members;
        this.nelems = (int)this.indexCalc.getSize();
    }
    
    protected ArrayStructure(final StructureMembers members, final Index ima) {
        super(ima);
        this.members = members;
        this.nelems = (int)this.indexCalc.getSize();
    }
    
    @Override
    void copyFrom1DJavaArray(final IndexIterator iter, final Object javaArray) {
        final Object[] arr$;
        final Object[] ja = arr$ = (Object[])javaArray;
        for (final Object aJa : arr$) {
            iter.setObjectNext(aJa);
        }
    }
    
    @Override
    void copyTo1DJavaArray(final IndexIterator iter, final Object javaArray) {
        final Object[] ja = (Object[])javaArray;
        for (int i = 0; i < ja.length; ++i) {
            ja[i] = iter.getObjectNext();
        }
    }
    
    @Override
    public Class getElementType() {
        return StructureData.class;
    }
    
    public StructureMembers getStructureMembers() {
        return this.members;
    }
    
    public List<StructureMembers.Member> getMembers() {
        return this.members.getMembers();
    }
    
    public List<String> getStructureMemberNames() {
        return this.members.getMemberNames();
    }
    
    public StructureMembers.Member findMember(final String memberName) {
        return this.members.findMember(memberName);
    }
    
    @Override
    public long getSizeBytes() {
        return this.indexCalc.getSize() * this.members.getStructureSize();
    }
    
    @Override
    public Object getObject(final Index i) {
        return this.getObject(i.currentElement());
    }
    
    @Override
    public void setObject(final Index i, final Object value) {
        this.setObject(i.currentElement(), value);
    }
    
    @Override
    public Object getObject(final int index) {
        return this.getStructureData(index);
    }
    
    @Override
    public void setObject(final int index, final Object value) {
        if (this.sdata == null) {
            this.sdata = new StructureData[this.nelems];
        }
        this.sdata[index] = (StructureData)value;
    }
    
    public StructureData getStructureData(final Index i) {
        return this.getStructureData(i.currentElement());
    }
    
    public StructureData getStructureData(final int index) {
        if (this.sdata == null) {
            this.sdata = new StructureData[this.nelems];
        }
        if (index >= this.sdata.length) {
            throw new IllegalArgumentException(index + " > " + this.sdata.length);
        }
        if (this.sdata[index] == null) {
            this.sdata[index] = this.makeStructureData(this, index);
        }
        return this.sdata[index];
    }
    
    @Override
    public Object getStorage() {
        for (int i = 0; i < this.nelems; ++i) {
            this.getStructureData(i);
        }
        return this.sdata;
    }
    
    protected abstract StructureData makeStructureData(final ArrayStructure p0, final int p1);
    
    public int getStructureSize() {
        return this.members.getStructureSize();
    }
    
    public StructureDataIterator getStructureDataIterator() throws IOException {
        return new ArrayStructureIterator();
    }
    
    public Array getArray(final int recno, final StructureMembers.Member m) {
        final DataType dataType = m.getDataType();
        if (dataType == DataType.DOUBLE) {
            final double[] pa = this.getJavaArrayDouble(recno, m);
            return Array.factory(Double.TYPE, m.getShape(), pa);
        }
        if (dataType == DataType.FLOAT) {
            final float[] pa2 = this.getJavaArrayFloat(recno, m);
            return Array.factory(Float.TYPE, m.getShape(), pa2);
        }
        if (dataType == DataType.BYTE || dataType == DataType.ENUM1) {
            final byte[] pa3 = this.getJavaArrayByte(recno, m);
            return Array.factory(Byte.TYPE, m.getShape(), pa3);
        }
        if (dataType == DataType.SHORT || dataType == DataType.ENUM2) {
            final short[] pa4 = this.getJavaArrayShort(recno, m);
            return Array.factory(Short.TYPE, m.getShape(), pa4);
        }
        if (dataType == DataType.INT || dataType == DataType.ENUM4) {
            final int[] pa5 = this.getJavaArrayInt(recno, m);
            return Array.factory(Integer.TYPE, m.getShape(), pa5);
        }
        if (dataType == DataType.LONG) {
            final long[] pa6 = this.getJavaArrayLong(recno, m);
            return Array.factory(Long.TYPE, m.getShape(), pa6);
        }
        if (dataType == DataType.CHAR) {
            final char[] pa7 = this.getJavaArrayChar(recno, m);
            return Array.factory(Character.TYPE, m.getShape(), pa7);
        }
        if (dataType == DataType.STRING) {
            final String[] pa8 = this.getJavaArrayString(recno, m);
            return Array.factory(String.class, m.getShape(), pa8);
        }
        if (dataType == DataType.STRUCTURE) {
            return this.getArrayStructure(recno, m);
        }
        if (dataType == DataType.SEQUENCE) {
            return this.getArraySequence(recno, m);
        }
        if (dataType == DataType.OPAQUE) {
            return this.getArrayObject(recno, m);
        }
        throw new RuntimeException("Dont have implemenation for " + dataType);
    }
    
    public void setMemberArray(final StructureMembers.Member m, final Array memberArray) {
        m.setDataArray(memberArray);
        if (memberArray instanceof ArrayStructure) {
            final ArrayStructure as = (ArrayStructure)memberArray;
            m.setStructureMembers(as.getStructureMembers());
        }
    }
    
    public Array extractMemberArray(final StructureMembers.Member m) throws IOException {
        if (m.getDataArray() != null) {
            return m.getDataArray();
        }
        final DataType dataType = m.getDataType();
        final int[] mshape = m.getShape();
        final int rrank = this.rank + mshape.length;
        final int[] rshape = new int[rrank];
        System.arraycopy(this.getShape(), 0, rshape, 0, this.rank);
        System.arraycopy(mshape, 0, rshape, this.rank, mshape.length);
        Array result;
        if (dataType == DataType.STRUCTURE) {
            final StructureMembers membersw = new StructureMembers(m.getStructureMembers());
            result = new ArrayStructureW(membersw, rshape);
        }
        else if (dataType == DataType.OPAQUE) {
            result = new ArrayObject(ByteBuffer.class, rshape);
        }
        else {
            result = Array.factory(dataType.getPrimitiveClassType(), rshape);
        }
        final IndexIterator resultIter = result.getIndexIterator();
        if (dataType == DataType.DOUBLE) {
            for (int recno = 0; recno < this.getSize(); ++recno) {
                this.copyDoubles(recno, m, resultIter);
            }
        }
        else if (dataType == DataType.FLOAT) {
            for (int recno = 0; recno < this.getSize(); ++recno) {
                this.copyFloats(recno, m, resultIter);
            }
        }
        else if (dataType == DataType.BYTE || dataType == DataType.ENUM1) {
            for (int recno = 0; recno < this.getSize(); ++recno) {
                this.copyBytes(recno, m, resultIter);
            }
        }
        else if (dataType == DataType.SHORT || dataType == DataType.ENUM2) {
            for (int recno = 0; recno < this.getSize(); ++recno) {
                this.copyShorts(recno, m, resultIter);
            }
        }
        else if (dataType == DataType.INT || dataType == DataType.ENUM4) {
            for (int recno = 0; recno < this.getSize(); ++recno) {
                this.copyInts(recno, m, resultIter);
            }
        }
        else if (dataType == DataType.LONG) {
            for (int recno = 0; recno < this.getSize(); ++recno) {
                this.copyLongs(recno, m, resultIter);
            }
        }
        else if (dataType == DataType.CHAR) {
            for (int recno = 0; recno < this.getSize(); ++recno) {
                this.copyChars(recno, m, resultIter);
            }
        }
        else if (dataType == DataType.STRING || dataType == DataType.OPAQUE) {
            for (int recno = 0; recno < this.getSize(); ++recno) {
                this.copyObjects(recno, m, resultIter);
            }
        }
        else if (dataType == DataType.STRUCTURE) {
            for (int recno = 0; recno < this.getSize(); ++recno) {
                this.copyStructures(recno, m, resultIter);
            }
        }
        else if (dataType == DataType.SEQUENCE) {
            for (int recno = 0; recno < this.getSize(); ++recno) {
                this.copySequences(recno, m, resultIter);
            }
        }
        return result;
    }
    
    protected void copyChars(final int recnum, final StructureMembers.Member m, final IndexIterator result) {
        final IndexIterator dataIter = this.getArray(recnum, m).getIndexIterator();
        while (dataIter.hasNext()) {
            result.setCharNext(dataIter.getCharNext());
        }
    }
    
    protected void copyDoubles(final int recnum, final StructureMembers.Member m, final IndexIterator result) {
        final IndexIterator dataIter = this.getArray(recnum, m).getIndexIterator();
        while (dataIter.hasNext()) {
            result.setDoubleNext(dataIter.getDoubleNext());
        }
    }
    
    protected void copyFloats(final int recnum, final StructureMembers.Member m, final IndexIterator result) {
        final IndexIterator dataIter = this.getArray(recnum, m).getIndexIterator();
        while (dataIter.hasNext()) {
            result.setFloatNext(dataIter.getFloatNext());
        }
    }
    
    protected void copyBytes(final int recnum, final StructureMembers.Member m, final IndexIterator result) {
        final IndexIterator dataIter = this.getArray(recnum, m).getIndexIterator();
        while (dataIter.hasNext()) {
            result.setByteNext(dataIter.getByteNext());
        }
    }
    
    protected void copyShorts(final int recnum, final StructureMembers.Member m, final IndexIterator result) {
        final IndexIterator dataIter = this.getArray(recnum, m).getIndexIterator();
        while (dataIter.hasNext()) {
            result.setShortNext(dataIter.getShortNext());
        }
    }
    
    protected void copyInts(final int recnum, final StructureMembers.Member m, final IndexIterator result) {
        final IndexIterator dataIter = this.getArray(recnum, m).getIndexIterator();
        while (dataIter.hasNext()) {
            result.setIntNext(dataIter.getIntNext());
        }
    }
    
    protected void copyLongs(final int recnum, final StructureMembers.Member m, final IndexIterator result) {
        final IndexIterator dataIter = this.getArray(recnum, m).getIndexIterator();
        while (dataIter.hasNext()) {
            result.setLongNext(dataIter.getLongNext());
        }
    }
    
    protected void copyObjects(final int recnum, final StructureMembers.Member m, final IndexIterator result) {
        final IndexIterator dataIter = this.getArray(recnum, m).getIndexIterator();
        while (dataIter.hasNext()) {
            result.setObjectNext(dataIter.getObjectNext());
        }
    }
    
    protected void copyStructures(final int recnum, final StructureMembers.Member m, final IndexIterator result) {
        final Array data = this.getArray(recnum, m);
        final IndexIterator dataIter = data.getIndexIterator();
        while (dataIter.hasNext()) {
            result.setObjectNext(dataIter.getObjectNext());
        }
    }
    
    protected void copySequences(final int recnum, final StructureMembers.Member m, final IndexIterator result) {
        final Array data = this.getArray(recnum, m);
        result.setObjectNext(data);
    }
    
    public Object getScalarObject(final int recno, final StructureMembers.Member m) {
        final DataType dataType = m.getDataType();
        if (dataType == DataType.DOUBLE) {
            return this.getScalarDouble(recno, m);
        }
        if (dataType == DataType.FLOAT) {
            return this.getScalarFloat(recno, m);
        }
        if (dataType == DataType.BYTE || dataType == DataType.ENUM1) {
            return this.getScalarByte(recno, m);
        }
        if (dataType == DataType.SHORT || dataType == DataType.ENUM2) {
            return this.getScalarShort(recno, m);
        }
        if (dataType == DataType.INT || dataType == DataType.ENUM4) {
            return this.getScalarInt(recno, m);
        }
        if (dataType == DataType.LONG) {
            return this.getScalarLong(recno, m);
        }
        if (dataType == DataType.CHAR) {
            return this.getScalarString(recno, m);
        }
        if (dataType == DataType.STRING) {
            return this.getScalarString(recno, m);
        }
        if (dataType == DataType.STRUCTURE) {
            return this.getScalarStructure(recno, m);
        }
        if (dataType == DataType.OPAQUE) {
            final ArrayObject data = (ArrayObject)m.getDataArray();
            return data.getObject(recno * m.getSize());
        }
        throw new RuntimeException("Dont have implementation for " + dataType);
    }
    
    public float convertScalarFloat(final int recnum, final StructureMembers.Member m) {
        if (m.getDataType() == DataType.FLOAT) {
            return this.getScalarFloat(recnum, m);
        }
        if (m.getDataType() == DataType.DOUBLE) {
            return (float)this.getScalarDouble(recnum, m);
        }
        final Object o = this.getScalarObject(recnum, m);
        if (o instanceof Number) {
            return ((Number)o).floatValue();
        }
        throw new ForbiddenConversionException("Type is " + m.getDataType() + ", not convertible to float");
    }
    
    public double convertScalarDouble(final int recnum, final StructureMembers.Member m) {
        if (m.getDataType() == DataType.DOUBLE) {
            return this.getScalarDouble(recnum, m);
        }
        if (m.getDataType() == DataType.FLOAT) {
            return this.getScalarFloat(recnum, m);
        }
        final Object o = this.getScalarObject(recnum, m);
        if (o instanceof Number) {
            return ((Number)o).doubleValue();
        }
        throw new ForbiddenConversionException("Type is " + m.getDataType() + ", not convertible to double");
    }
    
    public int convertScalarInt(final int recnum, final StructureMembers.Member m) {
        if (m.getDataType() == DataType.INT) {
            return this.getScalarInt(recnum, m);
        }
        if (m.getDataType() == DataType.SHORT) {
            return this.getScalarShort(recnum, m);
        }
        if (m.getDataType() == DataType.BYTE) {
            return this.getScalarByte(recnum, m);
        }
        final Object o = this.getScalarObject(recnum, m);
        if (o instanceof Number) {
            return ((Number)o).intValue();
        }
        throw new ForbiddenConversionException("Type is " + m.getDataType() + ", not convertible to int");
    }
    
    public double getScalarDouble(final int recnum, final StructureMembers.Member m) {
        if (m.getDataType() != DataType.DOUBLE) {
            throw new IllegalArgumentException("Type is " + m.getDataType() + ", must be double");
        }
        final Array data = m.getDataArray();
        return data.getDouble(recnum * m.getSize());
    }
    
    public double[] getJavaArrayDouble(final int recnum, final StructureMembers.Member m) {
        if (m.getDataType() != DataType.DOUBLE) {
            throw new IllegalArgumentException("Type is " + m.getDataType() + ", must be double");
        }
        final int count = m.getSize();
        final Array data = m.getDataArray();
        final double[] pa = new double[count];
        for (int i = 0; i < count; ++i) {
            pa[i] = data.getDouble(recnum * count + i);
        }
        return pa;
    }
    
    public float getScalarFloat(final int recnum, final StructureMembers.Member m) {
        if (m.getDataType() != DataType.FLOAT) {
            throw new IllegalArgumentException("Type is " + m.getDataType() + ", must be float");
        }
        final Array data = m.getDataArray();
        return data.getFloat(recnum * m.getSize());
    }
    
    public float[] getJavaArrayFloat(final int recnum, final StructureMembers.Member m) {
        if (m.getDataType() != DataType.FLOAT) {
            throw new IllegalArgumentException("Type is " + m.getDataType() + ", must be float");
        }
        final int count = m.getSize();
        final Array data = m.getDataArray();
        final float[] pa = new float[count];
        for (int i = 0; i < count; ++i) {
            pa[i] = data.getFloat(recnum * count + i);
        }
        return pa;
    }
    
    public byte getScalarByte(final int recnum, final StructureMembers.Member m) {
        if (m.getDataType() != DataType.BYTE && m.getDataType() != DataType.ENUM1) {
            throw new IllegalArgumentException("Type is " + m.getDataType() + ", must be byte");
        }
        final Array data = m.getDataArray();
        return data.getByte(recnum * m.getSize());
    }
    
    public byte[] getJavaArrayByte(final int recnum, final StructureMembers.Member m) {
        if (m.getDataType() != DataType.BYTE && m.getDataType() != DataType.ENUM1) {
            throw new IllegalArgumentException("Type is " + m.getDataType() + ", must be byte");
        }
        final int count = m.getSize();
        final Array data = m.getDataArray();
        final byte[] pa = new byte[count];
        for (int i = 0; i < count; ++i) {
            pa[i] = data.getByte(recnum * count + i);
        }
        return pa;
    }
    
    public short getScalarShort(final int recnum, final StructureMembers.Member m) {
        if (m.getDataType() != DataType.SHORT && m.getDataType() != DataType.ENUM2) {
            throw new IllegalArgumentException("Type is " + m.getDataType() + ", must be short");
        }
        final Array data = m.getDataArray();
        return data.getShort(recnum * m.getSize());
    }
    
    public short[] getJavaArrayShort(final int recnum, final StructureMembers.Member m) {
        if (m.getDataType() != DataType.SHORT && m.getDataType() != DataType.ENUM2) {
            throw new IllegalArgumentException("Type is " + m.getDataType() + ", must be short");
        }
        final int count = m.getSize();
        final Array data = m.getDataArray();
        final short[] pa = new short[count];
        for (int i = 0; i < count; ++i) {
            pa[i] = data.getShort(recnum * count + i);
        }
        return pa;
    }
    
    public int getScalarInt(final int recnum, final StructureMembers.Member m) {
        if (m.getDataType() != DataType.INT && m.getDataType() != DataType.ENUM4) {
            throw new IllegalArgumentException("Type is " + m.getDataType() + ", must be int");
        }
        final Array data = m.getDataArray();
        return data.getInt(recnum * m.getSize());
    }
    
    public int[] getJavaArrayInt(final int recnum, final StructureMembers.Member m) {
        if (m.getDataType() != DataType.INT && m.getDataType() != DataType.ENUM4) {
            throw new IllegalArgumentException("Type is " + m.getDataType() + ", must be int");
        }
        final int count = m.getSize();
        final Array data = m.getDataArray();
        final int[] pa = new int[count];
        for (int i = 0; i < count; ++i) {
            pa[i] = data.getInt(recnum * count + i);
        }
        return pa;
    }
    
    public long getScalarLong(final int recnum, final StructureMembers.Member m) {
        if (m.getDataType() != DataType.LONG) {
            throw new IllegalArgumentException("Type is " + m.getDataType() + ", must be long");
        }
        final Array data = m.getDataArray();
        return data.getLong(recnum * m.getSize());
    }
    
    public long[] getJavaArrayLong(final int recnum, final StructureMembers.Member m) {
        if (m.getDataType() != DataType.LONG) {
            throw new IllegalArgumentException("Type is " + m.getDataType() + ", must be long");
        }
        final int count = m.getSize();
        final Array data = m.getDataArray();
        final long[] pa = new long[count];
        for (int i = 0; i < count; ++i) {
            pa[i] = data.getLong(recnum * count + i);
        }
        return pa;
    }
    
    public char getScalarChar(final int recnum, final StructureMembers.Member m) {
        if (m.getDataType() != DataType.CHAR) {
            throw new IllegalArgumentException("Type is " + m.getDataType() + ", must be char");
        }
        final Array data = m.getDataArray();
        return data.getChar(recnum * m.getSize());
    }
    
    public char[] getJavaArrayChar(final int recnum, final StructureMembers.Member m) {
        if (m.getDataType() != DataType.CHAR) {
            throw new IllegalArgumentException("Type is " + m.getDataType() + ", must be char");
        }
        final int count = m.getSize();
        final Array data = m.getDataArray();
        final char[] pa = new char[count];
        for (int i = 0; i < count; ++i) {
            pa[i] = data.getChar(recnum * count + i);
        }
        return pa;
    }
    
    public String getScalarString(final int recnum, final StructureMembers.Member m) {
        if (m.getDataType() == DataType.CHAR) {
            final ArrayChar data = (ArrayChar)m.getDataArray();
            return data.getString(recnum);
        }
        if (m.getDataType() == DataType.STRING) {
            final Array data2 = m.getDataArray();
            return (String)data2.getObject(recnum);
        }
        throw new IllegalArgumentException("Type is " + m.getDataType() + ", must be String or char");
    }
    
    public String[] getJavaArrayString(final int recnum, final StructureMembers.Member m) {
        if (m.getDataType() == DataType.STRING) {
            final int n = m.getSize();
            final String[] result = new String[n];
            final Array data = m.getDataArray();
            for (int i = 0; i < n; ++i) {
                result[i] = (String)data.getObject(recnum * n + i);
            }
            return result;
        }
        if (m.getDataType() == DataType.CHAR) {
            final int strlen = this.indexCalc.getShape(this.rank - 1);
            final int n2 = m.getSize() / strlen;
            final String[] result2 = new String[n2];
            final ArrayChar data2 = (ArrayChar)m.getDataArray();
            for (int j = 0; j < n2; ++j) {
                result2[j] = data2.getString((recnum * n2 + j) * strlen);
            }
            return result2;
        }
        throw new IllegalArgumentException("Type is " + m.getDataType() + ", must be String or char");
    }
    
    public StructureData getScalarStructure(final int recnum, final StructureMembers.Member m) {
        if (m.getDataType() != DataType.STRUCTURE) {
            throw new IllegalArgumentException("Type is " + m.getDataType() + ", must be Structure");
        }
        final ArrayStructure data = (ArrayStructure)m.getDataArray();
        return data.getStructureData(recnum * m.getSize());
    }
    
    public ArrayStructure getArrayStructure(final int recnum, final StructureMembers.Member m) {
        if (m.getDataType() != DataType.STRUCTURE && m.getDataType() != DataType.SEQUENCE) {
            throw new IllegalArgumentException("Type is " + m.getDataType() + ", must be Structure or Sequence");
        }
        if (m.getDataType() == DataType.SEQUENCE) {
            return this.getArraySequence(recnum, m);
        }
        final ArrayStructure array = (ArrayStructure)m.getDataArray();
        final int count = m.getSize();
        final StructureData[] this_sdata = new StructureData[count];
        for (int i = 0; i < count; ++i) {
            this_sdata[i] = array.getStructureData(recnum * count + i);
        }
        final StructureMembers membersw = new StructureMembers(array.getStructureMembers());
        return new ArrayStructureW(membersw, m.getShape(), this_sdata);
    }
    
    public ArraySequence getArraySequence(final int recnum, final StructureMembers.Member m) {
        if (m.getDataType() != DataType.SEQUENCE) {
            throw new IllegalArgumentException("Type is " + m.getDataType() + ", must be Sequence");
        }
        final ArrayObject array = (ArrayObject)m.getDataArray();
        return (ArraySequence)array.getObject(recnum);
    }
    
    public ArrayObject getArrayObject(final int recnum, final StructureMembers.Member m) {
        if (m.getDataType() != DataType.OPAQUE) {
            throw new IllegalArgumentException("Type is " + m.getDataType() + ", must be Sequence");
        }
        final ArrayObject array = (ArrayObject)m.getDataArray();
        return (ArrayObject)array.getObject(recnum);
    }
    
    public void showInternal(final Formatter f, final String leadingSpace) {
        f.format("%sArrayStructure %s size=%d class=%s hash=0x%x%n", leadingSpace, this.members.getName(), this.getSize(), this.getClass().getName(), this.hashCode());
    }
    
    public void showInternalMembers(final Formatter f, final String leadingSpace) {
        f.format("%sArrayStructure %s class=%s hash=0x%x%n", leadingSpace, this.members.getName(), this.getClass().getName(), this.hashCode());
        for (final StructureMembers.Member m : this.getMembers()) {
            m.showInternal(f, leadingSpace + "  ");
        }
    }
    
    public Array createView(final Index index) {
        if (index.getSize() == this.getSize()) {
            return this;
        }
        throw new UnsupportedOperationException();
    }
    
    @Override
    public Array copy() {
        throw new UnsupportedOperationException();
    }
    
    @Override
    public double getDouble(final Index i) {
        throw new ForbiddenConversionException();
    }
    
    @Override
    public void setDouble(final Index i, final double value) {
        throw new ForbiddenConversionException();
    }
    
    @Override
    public float getFloat(final Index i) {
        throw new ForbiddenConversionException();
    }
    
    @Override
    public void setFloat(final Index i, final float value) {
        throw new ForbiddenConversionException();
    }
    
    @Override
    public long getLong(final Index i) {
        throw new ForbiddenConversionException();
    }
    
    @Override
    public void setLong(final Index i, final long value) {
        throw new ForbiddenConversionException();
    }
    
    @Override
    public int getInt(final Index i) {
        throw new ForbiddenConversionException();
    }
    
    @Override
    public void setInt(final Index i, final int value) {
        throw new ForbiddenConversionException();
    }
    
    @Override
    public short getShort(final Index i) {
        throw new ForbiddenConversionException();
    }
    
    @Override
    public void setShort(final Index i, final short value) {
        throw new ForbiddenConversionException();
    }
    
    @Override
    public byte getByte(final Index i) {
        throw new ForbiddenConversionException();
    }
    
    @Override
    public void setByte(final Index i, final byte value) {
        throw new ForbiddenConversionException();
    }
    
    @Override
    public boolean getBoolean(final Index i) {
        throw new ForbiddenConversionException();
    }
    
    @Override
    public void setBoolean(final Index i, final boolean value) {
        throw new ForbiddenConversionException();
    }
    
    @Override
    public char getChar(final Index i) {
        throw new ForbiddenConversionException();
    }
    
    @Override
    public void setChar(final Index i, final char value) {
        throw new ForbiddenConversionException();
    }
    
    @Override
    public double getDouble(final int index) {
        throw new ForbiddenConversionException();
    }
    
    @Override
    public void setDouble(final int index, final double value) {
        throw new ForbiddenConversionException();
    }
    
    @Override
    public float getFloat(final int index) {
        throw new ForbiddenConversionException();
    }
    
    @Override
    public void setFloat(final int index, final float value) {
        throw new ForbiddenConversionException();
    }
    
    @Override
    public long getLong(final int index) {
        throw new ForbiddenConversionException();
    }
    
    @Override
    public void setLong(final int index, final long value) {
        throw new ForbiddenConversionException();
    }
    
    @Override
    public int getInt(final int index) {
        throw new ForbiddenConversionException();
    }
    
    @Override
    public void setInt(final int index, final int value) {
        throw new ForbiddenConversionException();
    }
    
    @Override
    public short getShort(final int index) {
        throw new ForbiddenConversionException();
    }
    
    @Override
    public void setShort(final int index, final short value) {
        throw new ForbiddenConversionException();
    }
    
    @Override
    public byte getByte(final int index) {
        throw new ForbiddenConversionException();
    }
    
    @Override
    public void setByte(final int index, final byte value) {
        throw new ForbiddenConversionException();
    }
    
    @Override
    public char getChar(final int index) {
        throw new ForbiddenConversionException();
    }
    
    @Override
    public void setChar(final int index, final char value) {
        throw new ForbiddenConversionException();
    }
    
    @Override
    public boolean getBoolean(final int index) {
        throw new ForbiddenConversionException();
    }
    
    @Override
    public void setBoolean(final int index, final boolean value) {
        throw new ForbiddenConversionException();
    }
    
    public class ArrayStructureIterator implements StructureDataIterator
    {
        private int count;
        private int size;
        
        public ArrayStructureIterator() {
            this.count = 0;
            this.size = (int)ArrayStructure.this.getSize();
        }
        
        public boolean hasNext() throws IOException {
            return this.count < this.size;
        }
        
        public StructureData next() throws IOException {
            return ArrayStructure.this.getStructureData(this.count++);
        }
        
        public void setBufferSize(final int bytes) {
        }
        
        public StructureDataIterator reset() {
            this.count = 0;
            return this;
        }
        
        public int getCurrentRecno() {
            return this.count - 1;
        }
        
        public ArrayStructure getArrayStructure() {
            return ArrayStructure.this;
        }
    }
}
