// 
// Decompiled by Procyon v0.5.36
// 

package ucar.ma2;

public class ArrayRagged extends Array
{
    protected ArrayRagged(final int[] shape) {
        super(shape);
    }
    
    @Override
    public Class getElementType() {
        return null;
    }
    
    public Array createView(final Index index) {
        if (index.getSize() == this.getSize()) {
            return this;
        }
        throw new UnsupportedOperationException();
    }
    
    @Override
    public Object getStorage() {
        return null;
    }
    
    @Override
    void copyFrom1DJavaArray(final IndexIterator iter, final Object javaArray) {
    }
    
    @Override
    void copyTo1DJavaArray(final IndexIterator iter, final Object javaArray) {
    }
    
    @Override
    public Array copy() {
        throw new UnsupportedOperationException();
    }
    
    @Override
    public double getDouble(final Index i) {
        throw new ForbiddenConversionException();
    }
    
    @Override
    public void setDouble(final Index i, final double value) {
        throw new ForbiddenConversionException();
    }
    
    @Override
    public float getFloat(final Index i) {
        throw new ForbiddenConversionException();
    }
    
    @Override
    public void setFloat(final Index i, final float value) {
        throw new ForbiddenConversionException();
    }
    
    @Override
    public long getLong(final Index i) {
        throw new ForbiddenConversionException();
    }
    
    @Override
    public void setLong(final Index i, final long value) {
        throw new ForbiddenConversionException();
    }
    
    @Override
    public int getInt(final Index i) {
        throw new ForbiddenConversionException();
    }
    
    @Override
    public void setInt(final Index i, final int value) {
        throw new ForbiddenConversionException();
    }
    
    @Override
    public short getShort(final Index i) {
        throw new ForbiddenConversionException();
    }
    
    @Override
    public void setShort(final Index i, final short value) {
        throw new ForbiddenConversionException();
    }
    
    @Override
    public byte getByte(final Index i) {
        throw new ForbiddenConversionException();
    }
    
    @Override
    public void setByte(final Index i, final byte value) {
        throw new ForbiddenConversionException();
    }
    
    @Override
    public boolean getBoolean(final Index i) {
        throw new ForbiddenConversionException();
    }
    
    @Override
    public void setBoolean(final Index i, final boolean value) {
        throw new ForbiddenConversionException();
    }
    
    @Override
    public Object getObject(final Index ima) {
        return null;
    }
    
    @Override
    public void setObject(final Index ima, final Object value) {
    }
    
    @Override
    public char getChar(final Index i) {
        throw new ForbiddenConversionException();
    }
    
    @Override
    public void setChar(final Index i, final char value) {
        throw new ForbiddenConversionException();
    }
    
    @Override
    public double getDouble(final int index) {
        throw new ForbiddenConversionException();
    }
    
    @Override
    public void setDouble(final int index, final double value) {
        throw new ForbiddenConversionException();
    }
    
    @Override
    public float getFloat(final int index) {
        throw new ForbiddenConversionException();
    }
    
    @Override
    public void setFloat(final int index, final float value) {
        throw new ForbiddenConversionException();
    }
    
    @Override
    public long getLong(final int index) {
        throw new ForbiddenConversionException();
    }
    
    @Override
    public void setLong(final int index, final long value) {
        throw new ForbiddenConversionException();
    }
    
    @Override
    public int getInt(final int index) {
        throw new ForbiddenConversionException();
    }
    
    @Override
    public void setInt(final int index, final int value) {
        throw new ForbiddenConversionException();
    }
    
    @Override
    public short getShort(final int index) {
        throw new ForbiddenConversionException();
    }
    
    @Override
    public void setShort(final int index, final short value) {
        throw new ForbiddenConversionException();
    }
    
    @Override
    public byte getByte(final int index) {
        throw new ForbiddenConversionException();
    }
    
    @Override
    public void setByte(final int index, final byte value) {
        throw new ForbiddenConversionException();
    }
    
    @Override
    public char getChar(final int index) {
        throw new ForbiddenConversionException();
    }
    
    @Override
    public void setChar(final int index, final char value) {
        throw new ForbiddenConversionException();
    }
    
    @Override
    public boolean getBoolean(final int index) {
        throw new ForbiddenConversionException();
    }
    
    @Override
    public void setBoolean(final int index, final boolean value) {
        throw new ForbiddenConversionException();
    }
    
    @Override
    public Object getObject(final int elem) {
        return null;
    }
    
    @Override
    public void setObject(final int elem, final Object value) {
    }
}
