// 
// Decompiled by Procyon v0.5.36
// 

package ucar.ma2;

import java.util.Iterator;
import java.util.HashMap;
import java.util.Map;

public class StructureDataComposite extends StructureData
{
    protected Map<StructureMembers.Member, StructureData> proxy;
    
    public StructureDataComposite() {
        super(new StructureMembers(""));
        this.proxy = new HashMap<StructureMembers.Member, StructureData>(32);
    }
    
    public void add(final StructureData sdata) {
        for (final StructureMembers.Member m : sdata.getMembers()) {
            if (this.members.findMember(m.getName()) == null) {
                this.members.addMember(m);
                this.proxy.put(m, sdata);
            }
        }
    }
    
    @Override
    public Array getArray(final StructureMembers.Member m) {
        final StructureData sdata = this.proxy.get(m);
        return sdata.getArray(m);
    }
    
    @Override
    public float convertScalarFloat(final StructureMembers.Member m) {
        return this.proxy.get(m).convertScalarFloat(m);
    }
    
    @Override
    public double convertScalarDouble(final StructureMembers.Member m) {
        return this.proxy.get(m).convertScalarDouble(m);
    }
    
    @Override
    public int convertScalarInt(final StructureMembers.Member m) {
        return this.proxy.get(m).convertScalarInt(m);
    }
    
    @Override
    public double getScalarDouble(final StructureMembers.Member m) {
        return this.proxy.get(m).getScalarDouble(m);
    }
    
    @Override
    public double[] getJavaArrayDouble(final StructureMembers.Member m) {
        return this.proxy.get(m).getJavaArrayDouble(m);
    }
    
    @Override
    public float getScalarFloat(final StructureMembers.Member m) {
        return this.proxy.get(m).getScalarFloat(m);
    }
    
    @Override
    public float[] getJavaArrayFloat(final StructureMembers.Member m) {
        return this.proxy.get(m).getJavaArrayFloat(m);
    }
    
    @Override
    public byte getScalarByte(final StructureMembers.Member m) {
        return this.proxy.get(m).getScalarByte(m);
    }
    
    @Override
    public byte[] getJavaArrayByte(final StructureMembers.Member m) {
        return this.proxy.get(m).getJavaArrayByte(m);
    }
    
    @Override
    public int getScalarInt(final StructureMembers.Member m) {
        return this.proxy.get(m).getScalarInt(m);
    }
    
    @Override
    public int[] getJavaArrayInt(final StructureMembers.Member m) {
        return this.proxy.get(m).getJavaArrayInt(m);
    }
    
    @Override
    public short getScalarShort(final StructureMembers.Member m) {
        return this.proxy.get(m).getScalarShort(m);
    }
    
    @Override
    public short[] getJavaArrayShort(final StructureMembers.Member m) {
        return this.proxy.get(m).getJavaArrayShort(m);
    }
    
    @Override
    public long getScalarLong(final StructureMembers.Member m) {
        return this.proxy.get(m).getScalarLong(m);
    }
    
    @Override
    public long[] getJavaArrayLong(final StructureMembers.Member m) {
        return this.proxy.get(m).getJavaArrayLong(m);
    }
    
    @Override
    public char getScalarChar(final StructureMembers.Member m) {
        return this.proxy.get(m).getScalarChar(m);
    }
    
    @Override
    public char[] getJavaArrayChar(final StructureMembers.Member m) {
        return this.proxy.get(m).getJavaArrayChar(m);
    }
    
    @Override
    public String getScalarString(final StructureMembers.Member m) {
        return this.proxy.get(m).getScalarString(m);
    }
    
    @Override
    public String[] getJavaArrayString(final StructureMembers.Member m) {
        return this.proxy.get(m).getJavaArrayString(m);
    }
    
    @Override
    public StructureData getScalarStructure(final StructureMembers.Member m) {
        return this.proxy.get(m).getScalarStructure(m);
    }
    
    @Override
    public ArrayStructure getArrayStructure(final StructureMembers.Member m) {
        return this.proxy.get(m).getArrayStructure(m);
    }
    
    @Override
    public ArraySequence getArraySequence(final StructureMembers.Member m) {
        return this.proxy.get(m).getArraySequence(m);
    }
}
