// 
// Decompiled by Procyon v0.5.36
// 

package ucar.ma2;

import java.util.Formatter;
import java.util.ArrayList;
import java.nio.ByteOrder;
import java.util.Iterator;
import java.util.List;
import java.nio.ByteBuffer;

public class ArrayStructureBB extends ArrayStructure
{
    protected ByteBuffer bbuffer;
    protected int bb_offset;
    private List<Object> heap;
    
    public static int setOffsets(final StructureMembers members) {
        int offset = 0;
        for (final StructureMembers.Member m : members.getMembers()) {
            m.setDataParam(offset);
            offset += m.getSizeBytes();
            if (m.getStructureMembers() != null) {
                setOffsets(m.getStructureMembers());
            }
        }
        members.setStructureSize(offset);
        return offset;
    }
    
    public ArrayStructureBB(final StructureMembers members, final int[] shape) {
        super(members, shape);
        this.bb_offset = 0;
        (this.bbuffer = ByteBuffer.allocate(this.nelems * this.getStructureSize())).order(ByteOrder.BIG_ENDIAN);
    }
    
    public ArrayStructureBB(final StructureMembers members, final int[] shape, final ByteBuffer bbuffer, final int offset) {
        super(members, shape);
        this.bb_offset = 0;
        this.bbuffer = bbuffer;
        this.bb_offset = offset;
    }
    
    @Override
    protected StructureData makeStructureData(final ArrayStructure as, final int index) {
        return new StructureDataA(as, index);
    }
    
    public ByteBuffer getByteBuffer() {
        return this.bbuffer;
    }
    
    @Override
    public double getScalarDouble(final int recnum, final StructureMembers.Member m) {
        if (m.getDataType() != DataType.DOUBLE) {
            throw new IllegalArgumentException("Type is " + m.getDataType() + ", must be double");
        }
        if (m.getDataArray() != null) {
            return super.getScalarDouble(recnum, m);
        }
        final int offset = this.calcOffsetSetOrder(recnum, m);
        return this.bbuffer.getDouble(offset);
    }
    
    @Override
    public double[] getJavaArrayDouble(final int recnum, final StructureMembers.Member m) {
        if (m.getDataType() != DataType.DOUBLE) {
            throw new IllegalArgumentException("Type is " + m.getDataType() + ", must be double");
        }
        if (m.getDataArray() != null) {
            return super.getJavaArrayDouble(recnum, m);
        }
        final int offset = this.calcOffsetSetOrder(recnum, m);
        final int count = m.getSize();
        final double[] pa = new double[count];
        for (int i = 0; i < count; ++i) {
            pa[i] = this.bbuffer.getDouble(offset + i * 8);
        }
        return pa;
    }
    
    @Override
    protected void copyDoubles(final int recnum, final StructureMembers.Member m, final IndexIterator result) {
        final int offset = this.calcOffsetSetOrder(recnum, m);
        for (int count = m.getSize(), i = 0; i < count; ++i) {
            result.setDoubleNext(this.bbuffer.getDouble(offset + i * 8));
        }
    }
    
    @Override
    public float getScalarFloat(final int recnum, final StructureMembers.Member m) {
        if (m.getDataType() != DataType.FLOAT) {
            throw new IllegalArgumentException("Type is " + m.getDataType() + ", must be float");
        }
        if (m.getDataArray() != null) {
            return super.getScalarFloat(recnum, m);
        }
        final int offset = this.calcOffsetSetOrder(recnum, m);
        return this.bbuffer.getFloat(offset);
    }
    
    @Override
    public float[] getJavaArrayFloat(final int recnum, final StructureMembers.Member m) {
        if (m.getDataType() != DataType.FLOAT) {
            throw new IllegalArgumentException("Type is " + m.getDataType() + ", must be float");
        }
        if (m.getDataArray() != null) {
            return super.getJavaArrayFloat(recnum, m);
        }
        final int offset = this.calcOffsetSetOrder(recnum, m);
        final int count = m.getSize();
        final float[] pa = new float[count];
        for (int i = 0; i < count; ++i) {
            pa[i] = this.bbuffer.getFloat(offset + i * 4);
        }
        return pa;
    }
    
    @Override
    protected void copyFloats(final int recnum, final StructureMembers.Member m, final IndexIterator result) {
        final int offset = this.calcOffsetSetOrder(recnum, m);
        for (int count = m.getSize(), i = 0; i < count; ++i) {
            result.setFloatNext(this.bbuffer.getFloat(offset + i * 4));
        }
    }
    
    @Override
    public byte getScalarByte(final int recnum, final StructureMembers.Member m) {
        if (m.getDataType() != DataType.BYTE && m.getDataType() != DataType.ENUM1) {
            throw new IllegalArgumentException("Type is " + m.getDataType() + ", must be byte");
        }
        if (m.getDataArray() != null) {
            return super.getScalarByte(recnum, m);
        }
        final int offset = this.calcOffsetSetOrder(recnum, m);
        return this.bbuffer.get(offset);
    }
    
    @Override
    public byte[] getJavaArrayByte(final int recnum, final StructureMembers.Member m) {
        if (m.getDataType() != DataType.BYTE && m.getDataType() != DataType.ENUM1) {
            throw new IllegalArgumentException("Type is " + m.getDataType() + ", must be byte");
        }
        if (m.getDataArray() != null) {
            return super.getJavaArrayByte(recnum, m);
        }
        final int offset = this.calcOffsetSetOrder(recnum, m);
        final int count = m.getSize();
        final byte[] pa = new byte[count];
        for (int i = 0; i < count; ++i) {
            pa[i] = this.bbuffer.get(offset + i);
        }
        return pa;
    }
    
    @Override
    protected void copyBytes(final int recnum, final StructureMembers.Member m, final IndexIterator result) {
        final int offset = this.calcOffsetSetOrder(recnum, m);
        for (int count = m.getSize(), i = 0; i < count; ++i) {
            result.setByteNext(this.bbuffer.get(offset + i));
        }
    }
    
    @Override
    public short getScalarShort(final int recnum, final StructureMembers.Member m) {
        if (m.getDataType() != DataType.SHORT && m.getDataType() != DataType.ENUM2) {
            throw new IllegalArgumentException("Type is " + m.getDataType() + ", must be short");
        }
        if (m.getDataArray() != null) {
            return super.getScalarShort(recnum, m);
        }
        final int offset = this.calcOffsetSetOrder(recnum, m);
        return this.bbuffer.getShort(offset);
    }
    
    @Override
    public short[] getJavaArrayShort(final int recnum, final StructureMembers.Member m) {
        if (m.getDataType() != DataType.SHORT && m.getDataType() != DataType.ENUM2) {
            throw new IllegalArgumentException("Type is " + m.getDataType() + ", must be short");
        }
        if (m.getDataArray() != null) {
            return super.getJavaArrayShort(recnum, m);
        }
        final int offset = this.calcOffsetSetOrder(recnum, m);
        final int count = m.getSize();
        final short[] pa = new short[count];
        for (int i = 0; i < count; ++i) {
            pa[i] = this.bbuffer.getShort(offset + i * 2);
        }
        return pa;
    }
    
    @Override
    protected void copyShorts(final int recnum, final StructureMembers.Member m, final IndexIterator result) {
        final int offset = this.calcOffsetSetOrder(recnum, m);
        for (int count = m.getSize(), i = 0; i < count; ++i) {
            result.setShortNext(this.bbuffer.getShort(offset + i * 2));
        }
    }
    
    @Override
    public int getScalarInt(final int recnum, final StructureMembers.Member m) {
        if (m.getDataType() != DataType.INT && m.getDataType() != DataType.ENUM4) {
            throw new IllegalArgumentException("Type is " + m.getDataType() + ", must be int");
        }
        if (m.getDataArray() != null) {
            return super.getScalarInt(recnum, m);
        }
        final int offset = this.calcOffsetSetOrder(recnum, m);
        return this.bbuffer.getInt(offset);
    }
    
    @Override
    public int[] getJavaArrayInt(final int recnum, final StructureMembers.Member m) {
        if (m.getDataType() != DataType.INT && m.getDataType() != DataType.ENUM4) {
            throw new IllegalArgumentException("Type is " + m.getDataType() + ", must be int");
        }
        if (m.getDataArray() != null) {
            return super.getJavaArrayInt(recnum, m);
        }
        final int offset = this.calcOffsetSetOrder(recnum, m);
        final int count = m.getSize();
        final int[] pa = new int[count];
        for (int i = 0; i < count; ++i) {
            pa[i] = this.bbuffer.getInt(offset + i * 4);
        }
        return pa;
    }
    
    @Override
    protected void copyInts(final int recnum, final StructureMembers.Member m, final IndexIterator result) {
        final int offset = this.calcOffsetSetOrder(recnum, m);
        for (int count = m.getSize(), i = 0; i < count; ++i) {
            result.setIntNext(this.bbuffer.getInt(offset + i * 4));
        }
    }
    
    @Override
    public long getScalarLong(final int recnum, final StructureMembers.Member m) {
        if (m.getDataType() != DataType.LONG) {
            throw new IllegalArgumentException("Type is " + m.getDataType() + ", must be long");
        }
        if (m.getDataArray() != null) {
            return super.getScalarLong(recnum, m);
        }
        final int offset = this.calcOffsetSetOrder(recnum, m);
        return this.bbuffer.getLong(offset);
    }
    
    @Override
    public long[] getJavaArrayLong(final int recnum, final StructureMembers.Member m) {
        if (m.getDataType() != DataType.LONG) {
            throw new IllegalArgumentException("Type is " + m.getDataType() + ", must be long");
        }
        if (m.getDataArray() != null) {
            return super.getJavaArrayLong(recnum, m);
        }
        final int offset = this.calcOffsetSetOrder(recnum, m);
        final int count = m.getSize();
        final long[] pa = new long[count];
        for (int i = 0; i < count; ++i) {
            pa[i] = this.bbuffer.getLong(offset + i * 8);
        }
        return pa;
    }
    
    @Override
    protected void copyLongs(final int recnum, final StructureMembers.Member m, final IndexIterator result) {
        final int offset = this.calcOffsetSetOrder(recnum, m);
        for (int count = m.getSize(), i = 0; i < count; ++i) {
            result.setLongNext(this.bbuffer.getLong(offset + i * 8));
        }
    }
    
    @Override
    public char getScalarChar(final int recnum, final StructureMembers.Member m) {
        if (m.getDataType() != DataType.CHAR) {
            throw new IllegalArgumentException("Type is " + m.getDataType() + ", must be char");
        }
        if (m.getDataArray() != null) {
            return super.getScalarChar(recnum, m);
        }
        final int offset = this.calcOffsetSetOrder(recnum, m);
        return (char)this.bbuffer.get(offset);
    }
    
    @Override
    public char[] getJavaArrayChar(final int recnum, final StructureMembers.Member m) {
        if (m.getDataType() != DataType.CHAR) {
            throw new IllegalArgumentException("Type is " + m.getDataType() + ", must be char");
        }
        if (m.getDataArray() != null) {
            return super.getJavaArrayChar(recnum, m);
        }
        final int offset = this.calcOffsetSetOrder(recnum, m);
        final int count = m.getSize();
        final char[] pa = new char[count];
        for (int i = 0; i < count; ++i) {
            pa[i] = (char)this.bbuffer.get(offset + i);
        }
        return pa;
    }
    
    @Override
    protected void copyChars(final int recnum, final StructureMembers.Member m, final IndexIterator result) {
        final int offset = this.calcOffsetSetOrder(recnum, m);
        for (int count = m.getSize(), i = 0; i < count; ++i) {
            result.setCharNext((char)this.bbuffer.get(offset + i));
        }
    }
    
    @Override
    public String getScalarString(final int recnum, final StructureMembers.Member m) {
        if (m.getDataArray() != null) {
            return super.getScalarString(recnum, m);
        }
        if (m.getDataType() == DataType.STRING) {
            final int offset = this.calcOffsetSetOrder(recnum, m);
            final int index = this.bbuffer.getInt(offset);
            final Object data = this.heap.get(index);
            if (data instanceof String) {
                return (String)data;
            }
            return ((String[])data)[0];
        }
        else {
            if (m.getDataType() == DataType.CHAR) {
                final int offset = this.calcOffsetSetOrder(recnum, m);
                final int count = m.getSize();
                final byte[] pa = new byte[count];
                int i;
                for (i = 0; i < count; ++i) {
                    pa[i] = this.bbuffer.get(offset + i);
                    if (0 == pa[i]) {
                        break;
                    }
                }
                return new String(pa, 0, i);
            }
            throw new IllegalArgumentException("Type is " + m.getDataType() + ", must be String or char");
        }
    }
    
    @Override
    public String[] getJavaArrayString(final int recnum, final StructureMembers.Member m) {
        if (m.getDataArray() != null) {
            return super.getJavaArrayString(recnum, m);
        }
        if (m.getDataType() == DataType.STRING) {
            final int n = m.getSize();
            final int offset = this.calcOffsetSetOrder(recnum, m);
            final int heapIndex = this.bbuffer.getInt(offset);
            return this.heap.get(heapIndex);
        }
        if (m.getDataType() != DataType.CHAR) {
            throw new IllegalArgumentException("Type is " + m.getDataType() + ", must be char");
        }
        final int[] shape = m.getShape();
        final int rank = shape.length;
        if (rank < 2) {
            final String[] result = { this.getScalarString(recnum, m) };
            return result;
        }
        final int strlen = shape[rank - 1];
        final int n2 = m.getSize() / strlen;
        final int offset2 = this.calcOffsetSetOrder(recnum, m);
        final String[] result2 = new String[n2];
        for (int i = 0; i < n2; ++i) {
            final byte[] bytes = new byte[strlen];
            for (int j = 0; j < bytes.length; ++j) {
                bytes[j] = this.bbuffer.get(offset2 + i * strlen + j);
            }
            result2[i] = new String(bytes);
        }
        return result2;
    }
    
    @Override
    protected void copyObjects(final int recnum, final StructureMembers.Member m, final IndexIterator result) {
        final int offset = this.calcOffsetSetOrder(recnum, m);
        final int count = m.getSize();
        final int index = this.bbuffer.getInt(offset);
        final String[] data = this.heap.get(index);
        for (int i = 0; i < count; ++i) {
            result.setObjectNext(data[i]);
        }
    }
    
    @Override
    public StructureData getScalarStructure(final int recnum, final StructureMembers.Member m) {
        if (m.getDataType() != DataType.STRUCTURE) {
            throw new IllegalArgumentException("Type is " + m.getDataType() + ", must be Structure");
        }
        if (m.getDataArray() != null) {
            return super.getScalarStructure(recnum, m);
        }
        final int offset = this.calcOffsetSetOrder(recnum, m);
        final ArrayStructureBB subset = new ArrayStructureBB(m.getStructureMembers(), new int[] { 1 }, this.bbuffer, offset);
        return new StructureDataA(subset, 0);
    }
    
    @Override
    public ArrayStructure getArrayStructure(final int recnum, final StructureMembers.Member m) {
        if (m.getDataType() != DataType.STRUCTURE) {
            throw new IllegalArgumentException("Type is " + m.getDataType() + ", must be Structure");
        }
        if (m.getDataArray() != null) {
            return super.getArrayStructure(recnum, m);
        }
        final int offset = this.calcOffsetSetOrder(recnum, m);
        final ArrayStructureBB result = new ArrayStructureBB(m.getStructureMembers(), m.getShape(), this.bbuffer, offset);
        result.heap = this.heap;
        return result;
    }
    
    @Override
    public ArraySequence getArraySequence(final int recnum, final StructureMembers.Member m) {
        if (m.getDataType() != DataType.SEQUENCE) {
            throw new IllegalArgumentException("Type is " + m.getDataType() + ", must be Sequence");
        }
        final int offset = this.calcOffsetSetOrder(recnum, m);
        final int index = this.bbuffer.getInt(offset);
        if (this.heap == null) {
            System.out.println("ArrayStructureBB null heap");
            return null;
        }
        if (index > this.heap.size()) {
            System.out.println("HEY index " + index);
        }
        return this.heap.get(index);
    }
    
    protected int calcOffsetSetOrder(final int recnum, final StructureMembers.Member m) {
        if (null != m.getDataObject()) {
            this.bbuffer.order((ByteOrder)m.getDataObject());
        }
        return this.bb_offset + recnum * this.getStructureSize() + m.getDataParam();
    }
    
    public int addObjectToHeap(final Object s) {
        if (null == this.heap) {
            this.heap = new ArrayList<Object>();
        }
        this.heap.add(s);
        return this.heap.size() - 1;
    }
    
    @Override
    public void showInternal(final Formatter f, final String leadingSpace) {
        super.showInternal(f, leadingSpace);
        f.format("%sByteBuffer = %s (hash=0x%x)%n", leadingSpace, this.bbuffer, this.bbuffer.hashCode());
        if (null != this.heap) {
            f.format("%s  Heap Objects%n", leadingSpace);
            for (int i = 0; i < this.heap.size(); ++i) {
                final Object o = this.heap.get(i);
                f.format("%s   %d class=%s hash=0x%x = %s%n", leadingSpace, i, o.getClass().getName(), o.hashCode(), o);
                if (o instanceof ArrayStructure) {
                    ((ArrayStructure)o).showInternal(f, leadingSpace + "    ");
                }
            }
            f.format("%n", new Object[0]);
        }
    }
    
    public static void main(final String[] argv) {
        final byte[] ba = new byte[20];
        for (int i = 0; i < ba.length; ++i) {
            ba[i] = (byte)i;
        }
        final ByteBuffer bbw = ByteBuffer.wrap(ba, 5, 15);
        bbw.get(0);
        System.out.println(" bbw(0)=" + bbw.get(0) + " i would expect = 5");
        bbw.position(5);
        System.out.println(" bbw(0)=" + bbw.get(0) + " i would expect = 4");
    }
}
