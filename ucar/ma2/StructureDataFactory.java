// 
// Decompiled by Procyon v0.5.36
// 

package ucar.ma2;

public class StructureDataFactory
{
    public static StructureData make(final String name, final Object value) {
        final StructureMembers members = new StructureMembers("");
        final DataType dtype = DataType.getType(value.getClass());
        final StructureMembers.Member m = members.addMember(name, null, null, dtype, new int[] { 1 });
        final StructureDataW sw = new StructureDataW(members);
        final Array dataArray = Array.factory(dtype, new int[] { 1 });
        dataArray.setObject(dataArray.getIndex(), value);
        sw.setMemberData(m, dataArray);
        return sw;
    }
    
    public static StructureData make(final StructureData s1, final StructureData s2) {
        return make(new StructureData[] { s1, s2 });
    }
    
    public static StructureData make(final StructureData[] sdatas) {
        if (sdatas.length == 1) {
            return sdatas[0];
        }
        int count = 0;
        StructureData result = null;
        for (final StructureData sdata : sdatas) {
            if (sdata != null) {
                ++count;
                result = sdata;
            }
        }
        if (count == 1) {
            return result;
        }
        final StructureDataComposite result2 = new StructureDataComposite();
        for (final StructureData sdata2 : sdatas) {
            if (sdata2 != null) {
                result2.add(sdata2);
            }
        }
        return result2;
    }
}
