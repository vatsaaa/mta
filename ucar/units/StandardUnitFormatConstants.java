// 
// Decompiled by Procyon v0.5.36
// 

package ucar.units;

public interface StandardUnitFormatConstants
{
    public static final int EOF = 0;
    public static final int SP = 1;
    public static final int PLUS = 2;
    public static final int MINUS = 3;
    public static final int COLON = 4;
    public static final int UINT = 5;
    public static final int SIGN = 6;
    public static final int LETTER = 7;
    public static final int LPAREN = 8;
    public static final int RPAREN = 9;
    public static final int REAL_EXP = 10;
    public static final int RAISE = 11;
    public static final int PERIOD = 12;
    public static final int STAR = 13;
    public static final int DIVIDE = 14;
    public static final int SHIFT = 15;
    public static final int SYMBOL = 16;
    public static final int T = 17;
    public static final int NAME = 18;
    public static final int LB = 19;
    public static final int LN = 20;
    public static final int LG = 21;
    public static final int DEFAULT = 0;
    public static final String[] tokenImage = { "<EOF>", "<SP>", "\"+\"", "\"-\"", "\":\"", "<UINT>", "<SIGN>", "<LETTER>", "\"(\"", "\")\"", "<REAL_EXP>", "\"^\"", "\".\"", "\"*\"", "<DIVIDE>", "<SHIFT>", "<SYMBOL>", "\"t\"", "<NAME>", "<LB>", "<LN>", "<LG>" };
}
