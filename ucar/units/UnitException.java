// 
// Decompiled by Procyon v0.5.36
// 

package ucar.units;

import java.io.Serializable;

public abstract class UnitException extends Exception implements Serializable
{
    private static final long serialVersionUID = 1L;
    
    public UnitException() {
    }
    
    public UnitException(final String message) {
        super(message);
    }
    
    public UnitException(final String message, final Exception e) {
        super(message);
        this.initCause(e);
    }
}
