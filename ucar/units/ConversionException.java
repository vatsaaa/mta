// 
// Decompiled by Procyon v0.5.36
// 

package ucar.units;

import java.io.Serializable;

public final class ConversionException extends UnitException implements Serializable
{
    private static final long serialVersionUID = 1L;
    
    public ConversionException() {
    }
    
    private ConversionException(final String message) {
        super(message);
    }
    
    public ConversionException(final Unit fromUnit, final Unit toUnit) {
        this("Can't convert from unit \"" + fromUnit + "\" to unit \"" + toUnit + "\"");
    }
}
