// 
// Decompiled by Procyon v0.5.36
// 

package ucar.units;

import java.io.Serializable;

public final class Factor implements Serializable
{
    private static final long serialVersionUID = 1L;
    private final Base _base;
    private final int _exponent;
    
    public Factor(final Base base) {
        this(base, 1);
    }
    
    public Factor(final Factor factor, final int exponent) {
        this(factor.getBase(), exponent);
    }
    
    public Factor(final Base base, final int exponent) {
        this._base = base;
        this._exponent = exponent;
    }
    
    public Base getBase() {
        return this._base;
    }
    
    public String getID() {
        return this.getBase().getID();
    }
    
    public int getExponent() {
        return this._exponent;
    }
    
    public Factor pow(final int power) {
        return new Factor(this.getBase(), this.getExponent() * power);
    }
    
    @Override
    public final String toString() {
        return (this.getExponent() == 0) ? "" : ((this.getExponent() == 1) ? this.getBase().toString() : (this.getBase().toString() + this.getExponent()));
    }
    
    @Override
    public boolean equals(final Object object) {
        boolean equals;
        if (this == object) {
            equals = true;
        }
        else if (!(object instanceof Factor)) {
            equals = false;
        }
        else {
            final Factor that = (Factor)object;
            equals = (this.getExponent() == that.getExponent() && (this.getExponent() == 0 || this.getBase().equals(that.getBase())));
        }
        return equals;
    }
    
    @Override
    public int hashCode() {
        return (this.getExponent() == 0) ? this.getClass().hashCode() : (this.getExponent() ^ this.getBase().hashCode());
    }
    
    public boolean isReciprocalOf(final Factor that) {
        return this.getBase().equals(that.getBase()) && this.getExponent() == -that.getExponent();
    }
    
    public boolean isDimensionless() {
        return this.getExponent() == 0 || this.getBase().isDimensionless();
    }
}
