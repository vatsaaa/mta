// 
// Decompiled by Procyon v0.5.36
// 

package ucar.units;

public interface Converter
{
    float convert(final float p0);
    
    double convert(final double p0);
    
    float[] convert(final float[] p0);
    
    double[] convert(final double[] p0);
    
    float[] convert(final float[] p0, final float[] p1);
    
    double[] convert(final double[] p0, final double[] p1);
}
