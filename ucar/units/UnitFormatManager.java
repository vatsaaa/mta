// 
// Decompiled by Procyon v0.5.36
// 

package ucar.units;

import java.io.Serializable;

public final class UnitFormatManager implements Serializable
{
    private static final long serialVersionUID = 1L;
    private static UnitFormat instance;
    
    public static final UnitFormat instance() {
        if (UnitFormatManager.instance == null) {
            synchronized (UnitFormatManager.class) {
                if (UnitFormatManager.instance == null) {
                    UnitFormatManager.instance = StandardUnitFormat.instance();
                }
            }
        }
        return UnitFormatManager.instance;
    }
    
    public static final synchronized void setInstance(final UnitFormat instance) {
        UnitFormatManager.instance = instance;
    }
}
