// 
// Decompiled by Procyon v0.5.36
// 

package ucar.units;

public class UnitParseException extends SpecificationException
{
    private static final long serialVersionUID = 1L;
    
    public UnitParseException(final String reason) {
        super("Parse error: " + reason);
    }
    
    public UnitParseException(final String spec, final String reason) {
        super("Couldn't parse \"" + spec + "\": " + reason);
    }
    
    public UnitParseException(final String message, final Throwable e) {
        super(message);
        this.initCause(e);
    }
}
