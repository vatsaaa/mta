// 
// Decompiled by Procyon v0.5.36
// 

package ucar.units;

import java.io.Serializable;

public abstract class BaseQuantityException extends UnitException implements Serializable
{
    private static final long serialVersionUID = 1L;
    
    public BaseQuantityException() {
    }
    
    public BaseQuantityException(final String message) {
        super(message);
    }
}
