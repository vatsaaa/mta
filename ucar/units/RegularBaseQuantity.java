// 
// Decompiled by Procyon v0.5.36
// 

package ucar.units;

public final class RegularBaseQuantity extends BaseQuantity
{
    private static final long serialVersionUID = 1L;
    
    public RegularBaseQuantity(final String name, final String symbol) throws NameException {
        super(name, symbol);
    }
    
    protected RegularBaseQuantity(final String name, final String symbol, final boolean trusted) {
        super(name, symbol, trusted);
    }
    
    @Override
    public boolean isDimensionless() {
        return false;
    }
}
