// 
// Decompiled by Procyon v0.5.36
// 

package ucar.units;

import java.io.Serializable;

public final class PrefixDBManager implements Serializable
{
    private static final long serialVersionUID = 1L;
    private static PrefixDB instance;
    
    public static final PrefixDB instance() throws PrefixDBException {
        if (PrefixDBManager.instance == null) {
            synchronized (PrefixDBManager.class) {
                if (PrefixDBManager.instance == null) {
                    PrefixDBManager.instance = StandardPrefixDB.instance();
                }
            }
        }
        return PrefixDBManager.instance;
    }
    
    public static final synchronized void setInstance(final PrefixDB instance) {
        PrefixDBManager.instance = instance;
    }
}
