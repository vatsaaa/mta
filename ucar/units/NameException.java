// 
// Decompiled by Procyon v0.5.36
// 

package ucar.units;

public final class NameException extends UnitException
{
    private static final long serialVersionUID = 1L;
    
    public NameException(final String msg) {
        super(msg);
    }
}
