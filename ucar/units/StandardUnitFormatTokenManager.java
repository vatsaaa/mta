// 
// Decompiled by Procyon v0.5.36
// 

package ucar.units;

import java.io.IOException;
import java.io.PrintStream;

public class StandardUnitFormatTokenManager implements StandardUnitFormatConstants
{
    public PrintStream debugStream;
    static final int[] jjnextStates;
    public static final String[] jjstrLiteralImages;
    public static final String[] lexStateNames;
    protected SimpleCharStream input_stream;
    private final int[] jjrounds;
    private final int[] jjstateSet;
    protected char curChar;
    int curLexState;
    int defaultLexState;
    int jjnewStateCnt;
    int jjround;
    int jjmatchedPos;
    int jjmatchedKind;
    
    public void setDebugStream(final PrintStream ds) {
        this.debugStream = ds;
    }
    
    private final int jjStopStringLiteralDfa_0(final int pos, final long active0) {
        return -1;
    }
    
    private final int jjStartNfa_0(final int pos, final long active0) {
        return this.jjMoveNfa_0(this.jjStopStringLiteralDfa_0(pos, active0), pos + 1);
    }
    
    private int jjStopAtPos(final int pos, final int kind) {
        this.jjmatchedKind = kind;
        return (this.jjmatchedPos = pos) + 1;
    }
    
    private int jjMoveStringLiteralDfa0_0() {
        switch (this.curChar) {
            case '(': {
                return this.jjStopAtPos(0, 8);
            }
            case ')': {
                return this.jjStopAtPos(0, 9);
            }
            case '*': {
                return this.jjStopAtPos(0, 13);
            }
            case '+': {
                return this.jjStopAtPos(0, 2);
            }
            case '-': {
                return this.jjStopAtPos(0, 3);
            }
            case '.': {
                return this.jjStopAtPos(0, 12);
            }
            case ':': {
                return this.jjStopAtPos(0, 4);
            }
            case 'T': {
                return this.jjStartNfaWithStates_0(0, 17, 49);
            }
            case '^': {
                return this.jjStopAtPos(0, 11);
            }
            case 't': {
                return this.jjStartNfaWithStates_0(0, 17, 49);
            }
            default: {
                return this.jjMoveNfa_0(1, 0);
            }
        }
    }
    
    private int jjStartNfaWithStates_0(final int pos, final int kind, final int state) {
        this.jjmatchedKind = kind;
        this.jjmatchedPos = pos;
        try {
            this.curChar = this.input_stream.readChar();
        }
        catch (IOException e) {
            return pos + 1;
        }
        return this.jjMoveNfa_0(state, pos + 1);
    }
    
    private int jjMoveNfa_0(final int startState, int curPos) {
        int startsAt = 0;
        this.jjnewStateCnt = 49;
        int i = 1;
        this.jjstateSet[0] = startState;
        int kind = Integer.MAX_VALUE;
        while (true) {
            if (++this.jjround == Integer.MAX_VALUE) {
                this.ReInitRounds();
            }
            if (this.curChar < '@') {
                final long l = 1L << this.curChar;
                do {
                    switch (this.jjstateSet[--i]) {
                        case 1: {
                            if ((0x3FF000000000000L & l) != 0x0L) {
                                if (kind > 5) {
                                    kind = 5;
                                }
                                this.jjCheckNAdd(0);
                                continue;
                            }
                            if ((0x100002600L & l) != 0x0L) {
                                if (kind > 1) {
                                    kind = 1;
                                }
                                this.jjCheckNAddStates(0, 7);
                                continue;
                            }
                            if ((0xA400000000L & l) != 0x0L) {
                                if (kind > 16) {
                                    kind = 16;
                                    continue;
                                }
                                continue;
                            }
                            else {
                                if (this.curChar == '/' && kind > 14) {
                                    kind = 14;
                                    continue;
                                }
                                continue;
                            }
                            break;
                        }
                        case 9:
                        case 49: {
                            if ((0x3FF000000000000L & l) != 0x0L) {
                                this.jjCheckNAddTwoStates(9, 10);
                                continue;
                            }
                            continue;
                        }
                        case 0: {
                            if ((0x3FF000000000000L & l) == 0x0L) {
                                continue;
                            }
                            if (kind > 5) {
                                kind = 5;
                            }
                            this.jjCheckNAdd(0);
                            continue;
                        }
                        case 2: {
                            if ((0x280000000000L & l) != 0x0L) {
                                this.jjCheckNAdd(3);
                                continue;
                            }
                            continue;
                        }
                        case 3: {
                            if ((0x3FF000000000000L & l) == 0x0L) {
                                continue;
                            }
                            if (kind > 10) {
                                kind = 10;
                            }
                            this.jjCheckNAdd(3);
                            continue;
                        }
                        case 4: {
                            if (this.curChar == '/') {
                                kind = 14;
                                continue;
                            }
                            continue;
                        }
                        case 6: {
                            if ((0x100002600L & l) == 0x0L) {
                                continue;
                            }
                            if (kind > 15) {
                                kind = 15;
                            }
                            this.jjstateSet[this.jjnewStateCnt++] = 6;
                            continue;
                        }
                        case 7: {
                            if ((0xA400000000L & l) != 0x0L) {
                                kind = 16;
                                continue;
                            }
                            continue;
                        }
                        case 11: {
                            if ((0x100002600L & l) == 0x0L) {
                                continue;
                            }
                            if (kind > 1) {
                                kind = 1;
                            }
                            this.jjCheckNAddStates(0, 7);
                            continue;
                        }
                        case 12: {
                            if ((0x100002600L & l) == 0x0L) {
                                continue;
                            }
                            if (kind > 1) {
                                kind = 1;
                            }
                            this.jjCheckNAdd(12);
                            continue;
                        }
                        case 13: {
                            if ((0x100002600L & l) != 0x0L) {
                                this.jjCheckNAddTwoStates(13, 17);
                                continue;
                            }
                            continue;
                        }
                        case 15: {
                            if ((0x100002600L & l) == 0x0L) {
                                continue;
                            }
                            if (kind > 14) {
                                kind = 14;
                            }
                            this.jjstateSet[this.jjnewStateCnt++] = 15;
                            continue;
                        }
                        case 18: {
                            if ((0x100002600L & l) != 0x0L) {
                                this.jjCheckNAddTwoStates(18, 5);
                                continue;
                            }
                            continue;
                        }
                        case 19: {
                            if ((0x100002600L & l) != 0x0L) {
                                this.jjCheckNAddStates(8, 10);
                                continue;
                            }
                            continue;
                        }
                        case 21: {
                            if ((0x100002600L & l) == 0x0L) {
                                continue;
                            }
                            if (kind > 15) {
                                kind = 15;
                            }
                            this.jjstateSet[this.jjnewStateCnt++] = 21;
                            continue;
                        }
                        case 32: {
                            if (this.curChar != ':') {
                                continue;
                            }
                            if (kind > 19) {
                                kind = 19;
                            }
                            this.jjCheckNAdd(33);
                            continue;
                        }
                        case 33: {
                            if ((0x100002600L & l) == 0x0L) {
                                continue;
                            }
                            if (kind > 19) {
                                kind = 19;
                            }
                            this.jjCheckNAdd(33);
                            continue;
                        }
                        case 35: {
                            if (this.curChar == '(') {
                                this.jjstateSet[this.jjnewStateCnt++] = 34;
                                continue;
                            }
                            continue;
                        }
                        case 38: {
                            if (this.curChar != ':') {
                                continue;
                            }
                            if (kind > 20) {
                                kind = 20;
                            }
                            this.jjCheckNAdd(39);
                            continue;
                        }
                        case 39: {
                            if ((0x100002600L & l) == 0x0L) {
                                continue;
                            }
                            if (kind > 20) {
                                kind = 20;
                            }
                            this.jjCheckNAdd(39);
                            continue;
                        }
                        case 41: {
                            if (this.curChar == '(') {
                                this.jjstateSet[this.jjnewStateCnt++] = 40;
                                continue;
                            }
                            continue;
                        }
                        case 44: {
                            if (this.curChar != ':') {
                                continue;
                            }
                            if (kind > 21) {
                                kind = 21;
                            }
                            this.jjCheckNAdd(45);
                            continue;
                        }
                        case 45: {
                            if ((0x100002600L & l) == 0x0L) {
                                continue;
                            }
                            if (kind > 21) {
                                kind = 21;
                            }
                            this.jjCheckNAdd(45);
                            continue;
                        }
                        case 47: {
                            if (this.curChar == '(') {
                                this.jjstateSet[this.jjnewStateCnt++] = 46;
                                continue;
                            }
                            continue;
                        }
                        default: {
                            continue;
                        }
                    }
                } while (i != startsAt);
            }
            else if (this.curChar < '\u0080') {
                final long l = 1L << (this.curChar & '?');
                do {
                    switch (this.jjstateSet[--i]) {
                        case 1: {
                            if ((0x7FFFFFE87FFFFFEL & l) != 0x0L) {
                                if (kind > 18) {
                                    kind = 18;
                                }
                                this.jjCheckNAddTwoStates(8, 9);
                            }
                            else if (this.curChar == '@') {
                                if (kind > 15) {
                                    kind = 15;
                                }
                                this.jjstateSet[this.jjnewStateCnt++] = 6;
                            }
                            if ((0x100000001000L & l) != 0x0L) {
                                this.jjAddStates(11, 13);
                                continue;
                            }
                            if ((0x2000000020L & l) != 0x0L) {
                                this.jjAddStates(14, 15);
                                continue;
                            }
                            continue;
                        }
                        case 8:
                        case 49: {
                            if ((0x7FFFFFE87FFFFFEL & l) == 0x0L) {
                                continue;
                            }
                            if (kind > 18) {
                                kind = 18;
                            }
                            this.jjCheckNAddTwoStates(8, 9);
                            continue;
                        }
                        case 5: {
                            if (this.curChar != '@') {
                                continue;
                            }
                            kind = 15;
                            this.jjstateSet[this.jjnewStateCnt++] = 6;
                            continue;
                        }
                        case 10: {
                            if ((0x7FFFFFE87FFFFFEL & l) == 0x0L) {
                                continue;
                            }
                            if (kind > 18) {
                                kind = 18;
                            }
                            this.jjstateSet[this.jjnewStateCnt++] = 10;
                            continue;
                        }
                        case 14: {
                            if ((0x4000000040000L & l) != 0x0L) {
                                this.jjstateSet[this.jjnewStateCnt++] = 15;
                                continue;
                            }
                            continue;
                        }
                        case 16: {
                            if ((0x2000000020L & l) != 0x0L) {
                                this.jjstateSet[this.jjnewStateCnt++] = 14;
                                continue;
                            }
                            continue;
                        }
                        case 17: {
                            if ((0x1000000010000L & l) != 0x0L) {
                                this.jjstateSet[this.jjnewStateCnt++] = 16;
                                continue;
                            }
                            continue;
                        }
                        case 20: {
                            if ((0x2000000020L & l) != 0x0L) {
                                this.jjCheckNAdd(21);
                                continue;
                            }
                            continue;
                        }
                        case 22: {
                            if ((0x800000008L & l) != 0x0L) {
                                this.jjstateSet[this.jjnewStateCnt++] = 20;
                                continue;
                            }
                            continue;
                        }
                        case 23: {
                            if ((0x400000004000L & l) != 0x0L) {
                                this.jjstateSet[this.jjnewStateCnt++] = 22;
                                continue;
                            }
                            continue;
                        }
                        case 24: {
                            if ((0x20000000200L & l) != 0x0L) {
                                this.jjstateSet[this.jjnewStateCnt++] = 23;
                                continue;
                            }
                            continue;
                        }
                        case 25: {
                            if ((0x8000000080000L & l) != 0x0L) {
                                this.jjstateSet[this.jjnewStateCnt++] = 24;
                                continue;
                            }
                            continue;
                        }
                        case 26: {
                            if ((0x200000002000L & l) != 0x0L) {
                                this.jjCheckNAdd(21);
                                continue;
                            }
                            continue;
                        }
                        case 27: {
                            if ((0x800000008000L & l) != 0x0L) {
                                this.jjstateSet[this.jjnewStateCnt++] = 26;
                                continue;
                            }
                            continue;
                        }
                        case 28: {
                            if ((0x4000000040000L & l) != 0x0L) {
                                this.jjstateSet[this.jjnewStateCnt++] = 27;
                                continue;
                            }
                            continue;
                        }
                        case 29: {
                            if ((0x4000000040L & l) != 0x0L) {
                                this.jjstateSet[this.jjnewStateCnt++] = 28;
                                continue;
                            }
                            continue;
                        }
                        case 30: {
                            if ((0x100000001000L & l) != 0x0L) {
                                this.jjAddStates(11, 13);
                                continue;
                            }
                            continue;
                        }
                        case 31: {
                            if ((0x2000000020L & l) == 0x0L) {
                                continue;
                            }
                            if (kind > 19) {
                                kind = 19;
                            }
                            this.jjAddStates(16, 17);
                            continue;
                        }
                        case 34: {
                            if ((0x4000000040000L & l) != 0x0L) {
                                this.jjstateSet[this.jjnewStateCnt++] = 31;
                                continue;
                            }
                            continue;
                        }
                        case 36: {
                            if ((0x400000004L & l) != 0x0L) {
                                this.jjstateSet[this.jjnewStateCnt++] = 35;
                                continue;
                            }
                            continue;
                        }
                        case 37: {
                            if ((0x2000000020L & l) == 0x0L) {
                                continue;
                            }
                            if (kind > 20) {
                                kind = 20;
                            }
                            this.jjAddStates(18, 19);
                            continue;
                        }
                        case 40: {
                            if ((0x4000000040000L & l) != 0x0L) {
                                this.jjstateSet[this.jjnewStateCnt++] = 37;
                                continue;
                            }
                            continue;
                        }
                        case 42: {
                            if ((0x400000004000L & l) != 0x0L) {
                                this.jjstateSet[this.jjnewStateCnt++] = 41;
                                continue;
                            }
                            continue;
                        }
                        case 43: {
                            if ((0x2000000020L & l) == 0x0L) {
                                continue;
                            }
                            if (kind > 21) {
                                kind = 21;
                            }
                            this.jjAddStates(20, 21);
                            continue;
                        }
                        case 46: {
                            if ((0x4000000040000L & l) != 0x0L) {
                                this.jjstateSet[this.jjnewStateCnt++] = 43;
                                continue;
                            }
                            continue;
                        }
                        case 48: {
                            if ((0x8000000080L & l) != 0x0L) {
                                this.jjstateSet[this.jjnewStateCnt++] = 47;
                                continue;
                            }
                            continue;
                        }
                        default: {
                            continue;
                        }
                    }
                } while (i != startsAt);
            }
            else {
                final int i2 = (this.curChar & '\u00ff') >> 6;
                final long l2 = 1L << (this.curChar & '?');
                do {
                    final int n = this.jjstateSet[--i];
                } while (i != startsAt);
            }
            if (kind != Integer.MAX_VALUE) {
                this.jjmatchedKind = kind;
                this.jjmatchedPos = curPos;
                kind = Integer.MAX_VALUE;
            }
            ++curPos;
            final int n2 = i = this.jjnewStateCnt;
            final int n3 = 49;
            final int jjnewStateCnt = startsAt;
            this.jjnewStateCnt = jjnewStateCnt;
            if (n2 == (startsAt = n3 - jjnewStateCnt)) {
                break;
            }
            try {
                this.curChar = this.input_stream.readChar();
            }
            catch (IOException e) {
                return curPos;
            }
        }
        return curPos;
    }
    
    public StandardUnitFormatTokenManager(final SimpleCharStream stream) {
        this.debugStream = System.out;
        this.jjrounds = new int[49];
        this.jjstateSet = new int[98];
        this.curLexState = 0;
        this.defaultLexState = 0;
        this.input_stream = stream;
    }
    
    public StandardUnitFormatTokenManager(final SimpleCharStream stream, final int lexState) {
        this(stream);
        this.SwitchTo(lexState);
    }
    
    public void ReInit(final SimpleCharStream stream) {
        final int n = 0;
        this.jjnewStateCnt = n;
        this.jjmatchedPos = n;
        this.curLexState = this.defaultLexState;
        this.input_stream = stream;
        this.ReInitRounds();
    }
    
    private void ReInitRounds() {
        this.jjround = -2147483647;
        int i = 49;
        while (i-- > 0) {
            this.jjrounds[i] = Integer.MIN_VALUE;
        }
    }
    
    public void ReInit(final SimpleCharStream stream, final int lexState) {
        this.ReInit(stream);
        this.SwitchTo(lexState);
    }
    
    public void SwitchTo(final int lexState) {
        if (lexState >= 1 || lexState < 0) {
            throw new TokenMgrError("Error: Ignoring invalid lexical state : " + lexState + ". State unchanged.", 2);
        }
        this.curLexState = lexState;
    }
    
    protected Token jjFillToken() {
        final String im = StandardUnitFormatTokenManager.jjstrLiteralImages[this.jjmatchedKind];
        final String curTokenImage = (im == null) ? this.input_stream.GetImage() : im;
        final int beginLine = this.input_stream.getBeginLine();
        final int beginColumn = this.input_stream.getBeginColumn();
        final int endLine = this.input_stream.getEndLine();
        final int endColumn = this.input_stream.getEndColumn();
        final Token t = Token.newToken(this.jjmatchedKind, curTokenImage);
        t.beginLine = beginLine;
        t.endLine = endLine;
        t.beginColumn = beginColumn;
        t.endColumn = endColumn;
        return t;
    }
    
    public Token getNextToken() {
        int curPos = 0;
        try {
            this.curChar = this.input_stream.BeginToken();
        }
        catch (IOException e) {
            this.jjmatchedKind = 0;
            final Token matchedToken = this.jjFillToken();
            return matchedToken;
        }
        this.jjmatchedKind = Integer.MAX_VALUE;
        this.jjmatchedPos = 0;
        curPos = this.jjMoveStringLiteralDfa0_0();
        if (this.jjmatchedKind != Integer.MAX_VALUE) {
            if (this.jjmatchedPos + 1 < curPos) {
                this.input_stream.backup(curPos - this.jjmatchedPos - 1);
            }
            final Token matchedToken = this.jjFillToken();
            return matchedToken;
        }
        int error_line = this.input_stream.getEndLine();
        int error_column = this.input_stream.getEndColumn();
        String error_after = null;
        boolean EOFSeen = false;
        try {
            this.input_stream.readChar();
            this.input_stream.backup(1);
        }
        catch (IOException e2) {
            EOFSeen = true;
            error_after = ((curPos <= 1) ? "" : this.input_stream.GetImage());
            if (this.curChar == '\n' || this.curChar == '\r') {
                ++error_line;
                error_column = 0;
            }
            else {
                ++error_column;
            }
        }
        if (!EOFSeen) {
            this.input_stream.backup(1);
            error_after = ((curPos <= 1) ? "" : this.input_stream.GetImage());
        }
        throw new TokenMgrError(EOFSeen, this.curLexState, error_line, error_column, error_after, this.curChar, 0);
    }
    
    private void jjCheckNAdd(final int state) {
        if (this.jjrounds[state] != this.jjround) {
            this.jjstateSet[this.jjnewStateCnt++] = state;
            this.jjrounds[state] = this.jjround;
        }
    }
    
    private void jjAddStates(int start, final int end) {
        do {
            this.jjstateSet[this.jjnewStateCnt++] = StandardUnitFormatTokenManager.jjnextStates[start];
        } while (start++ != end);
    }
    
    private void jjCheckNAddTwoStates(final int state1, final int state2) {
        this.jjCheckNAdd(state1);
        this.jjCheckNAdd(state2);
    }
    
    private void jjCheckNAddStates(int start, final int end) {
        do {
            this.jjCheckNAdd(StandardUnitFormatTokenManager.jjnextStates[start]);
        } while (start++ != end);
    }
    
    static {
        jjnextStates = new int[] { 12, 13, 17, 18, 5, 19, 25, 29, 19, 25, 29, 36, 42, 48, 2, 3, 32, 33, 38, 39, 44, 45 };
        jjstrLiteralImages = new String[] { "", null, "+", "-", ":", null, null, null, "(", ")", null, "^", ".", "*", null, null, null, null, null, null, null, null };
        lexStateNames = new String[] { "DEFAULT" };
    }
}
