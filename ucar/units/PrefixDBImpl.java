// 
// Decompiled by Procyon v0.5.36
// 

package ucar.units;

import java.util.Iterator;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.Map;
import java.util.SortedSet;
import java.io.Serializable;

public class PrefixDBImpl implements PrefixDB, Serializable
{
    private static final long serialVersionUID = 1L;
    private final SortedSet<Prefix> nameSet;
    private final SortedSet<Prefix> symbolSet;
    private final Map<Double, Prefix> valueMap;
    
    public PrefixDBImpl() {
        this.nameSet = new TreeSet<Prefix>();
        this.symbolSet = new TreeSet<Prefix>();
        this.valueMap = new TreeMap<Double, Prefix>();
    }
    
    public void addName(final String name, final double value) throws PrefixExistsException {
        final Prefix prefix = new PrefixName(name, value);
        this.nameSet.add(prefix);
    }
    
    public void addSymbol(final String symbol, final double value) throws PrefixExistsException {
        final Prefix prefix = new PrefixSymbol(symbol, value);
        this.symbolSet.add(prefix);
        this.valueMap.put(new Double(value), prefix);
    }
    
    public Prefix getPrefixByName(final String string) {
        return getPrefix(string, this.nameSet);
    }
    
    public Prefix getPrefixBySymbol(final String string) {
        return getPrefix(string, this.symbolSet);
    }
    
    public Prefix getPrefixByValue(final double value) {
        return this.valueMap.get(new Double(value));
    }
    
    private static Prefix getPrefix(final String string, final Set<Prefix> set) {
        for (final Prefix prefix : set) {
            final int comp = prefix.compareTo(string);
            if (comp == 0) {
                return prefix;
            }
            if (comp > 0) {
                break;
            }
        }
        return null;
    }
    
    @Override
    public String toString() {
        return "nameSet=" + this.nameSet + "symbolSet=" + this.symbolSet + "valueMap=" + this.valueMap;
    }
    
    public Iterator iterator() {
        return this.nameSet.iterator();
    }
    
    public static void main(final String[] args) throws Exception {
        final PrefixDB db = new PrefixDBImpl();
        db.addName("mega", 1000000.0);
        System.out.println("mega=" + db.getPrefixByName("mega").getValue());
        db.addSymbol("m", 0.001);
        System.out.println("m=" + db.getPrefixBySymbol("m").getValue());
        System.out.println("1e-3=" + db.getPrefixByValue(0.001).getID());
    }
}
