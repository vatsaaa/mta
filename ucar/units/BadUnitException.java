// 
// Decompiled by Procyon v0.5.36
// 

package ucar.units;

public final class BadUnitException extends UnitDBException
{
    private static final long serialVersionUID = 1L;
    
    public BadUnitException(final String msg) {
        super(msg);
    }
}
