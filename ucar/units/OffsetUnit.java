// 
// Decompiled by Procyon v0.5.36
// 

package ucar.units;

public final class OffsetUnit extends UnitImpl implements DerivableUnit
{
    private static final long serialVersionUID = 1L;
    private final double _offset;
    private final Unit _unit;
    private DerivedUnit _derivedUnit;
    
    public OffsetUnit(final Unit unit, final double offset) {
        this(unit, offset, null);
    }
    
    public OffsetUnit(final Unit unit, final double offset, final UnitName id) {
        super(id);
        if (!(unit instanceof OffsetUnit)) {
            this._unit = unit;
            this._offset = offset;
        }
        else {
            this._unit = ((OffsetUnit)unit)._unit;
            this._offset = ((OffsetUnit)unit)._offset + offset;
        }
    }
    
    static Unit getInstance(final Unit unit, final double origin) {
        return (origin == 0.0) ? unit : new OffsetUnit(unit, origin);
    }
    
    public Unit getUnit() {
        return this._unit;
    }
    
    public double getOffset() {
        return this._offset;
    }
    
    public Unit clone(final UnitName id) {
        return new OffsetUnit(this.getUnit(), this.getOffset(), id);
    }
    
    @Override
    public Unit multiplyBy(final double scale) throws MultiplyException {
        if (scale == 0.0) {
            throw new MultiplyException(scale, this);
        }
        return getInstance(this._unit.multiplyBy(scale), this._offset / scale);
    }
    
    @Override
    public Unit shiftTo(final double origin) {
        return getInstance(this._unit, origin + this._offset);
    }
    
    @Override
    protected Unit myMultiplyBy(final Unit that) throws MultiplyException {
        return (that instanceof OffsetUnit) ? this.getUnit().multiplyBy(((OffsetUnit)that).getUnit()) : this.getUnit().multiplyBy(that);
    }
    
    @Override
    protected Unit myDivideBy(final Unit that) throws OperationException {
        return (that instanceof OffsetUnit) ? this.getUnit().divideBy(((OffsetUnit)that).getUnit()) : this.getUnit().divideBy(that);
    }
    
    @Override
    protected Unit myDivideInto(final Unit that) throws OperationException {
        return (that instanceof OffsetUnit) ? this.getUnit().divideInto(((OffsetUnit)that).getUnit()) : this.getUnit().divideInto(that);
    }
    
    @Override
    protected Unit myRaiseTo(final int power) throws RaiseException {
        return this.getUnit().raiseTo(power);
    }
    
    public DerivedUnit getDerivedUnit() {
        if (this._derivedUnit == null) {
            this._derivedUnit = this.getUnit().getDerivedUnit();
        }
        return this._derivedUnit;
    }
    
    public float toDerivedUnit(final float amount) throws ConversionException {
        return (float)this.toDerivedUnit((double)amount);
    }
    
    public double toDerivedUnit(final double amount) throws ConversionException {
        if (!(this._unit instanceof DerivableUnit)) {
            throw new ConversionException(this, this.getDerivedUnit());
        }
        return ((DerivableUnit)this.getUnit()).toDerivedUnit(amount + this.getOffset());
    }
    
    public float[] toDerivedUnit(final float[] input, final float[] output) throws ConversionException {
        if (!(this._unit instanceof DerivableUnit)) {
            throw new ConversionException(this, this.getDerivedUnit());
        }
        final float origin = (float)this.getOffset();
        int i = input.length;
        while (--i >= 0) {
            output[i] = input[i] + origin;
        }
        return ((DerivableUnit)this.getUnit()).toDerivedUnit(output, output);
    }
    
    public double[] toDerivedUnit(final double[] input, final double[] output) throws ConversionException {
        if (!(this._unit instanceof DerivableUnit)) {
            throw new ConversionException(this, this.getDerivedUnit());
        }
        final double origin = this.getOffset();
        int i = input.length;
        while (--i >= 0) {
            output[i] = input[i] + origin;
        }
        return ((DerivableUnit)this.getUnit()).toDerivedUnit(output, output);
    }
    
    public float fromDerivedUnit(final float amount) throws ConversionException {
        return (float)this.fromDerivedUnit((double)amount);
    }
    
    public double fromDerivedUnit(final double amount) throws ConversionException {
        if (!(this._unit instanceof DerivableUnit)) {
            throw new ConversionException(this.getDerivedUnit(), this);
        }
        return ((DerivableUnit)this.getUnit()).fromDerivedUnit(amount) - this.getOffset();
    }
    
    public float[] fromDerivedUnit(final float[] input, final float[] output) throws ConversionException {
        if (!(this._unit instanceof DerivableUnit)) {
            throw new ConversionException(this.getDerivedUnit(), this);
        }
        ((DerivableUnit)this.getUnit()).fromDerivedUnit(input, output);
        final float origin = (float)this.getOffset();
        int i = input.length;
        while (--i >= 0) {
            final int n = i;
            output[n] -= origin;
        }
        return output;
    }
    
    public double[] fromDerivedUnit(final double[] input, final double[] output) throws ConversionException {
        if (!(this._unit instanceof DerivableUnit)) {
            throw new ConversionException(this.getDerivedUnit(), this);
        }
        ((DerivableUnit)this.getUnit()).fromDerivedUnit(input, output);
        final double origin = this.getOffset();
        int i = input.length;
        while (--i >= 0) {
            final int n = i;
            output[n] -= origin;
        }
        return output;
    }
    
    @Override
    public boolean equals(final Object object) {
        if (this == object) {
            return true;
        }
        if (this._offset == 0.0) {
            return object.equals(this._unit);
        }
        if (!(object instanceof OffsetUnit)) {
            return false;
        }
        final OffsetUnit that = (OffsetUnit)object;
        return this._offset == that._offset && this._unit.equals(that._unit);
    }
    
    @Override
    public int hashCode() {
        return ((this.getOffset() == 0.0) ? 0 : Double.valueOf(this.getOffset()).hashCode()) ^ this.getUnit().hashCode();
    }
    
    public boolean isDimensionless() {
        return this.getUnit().isDimensionless();
    }
    
    @Override
    public String toString() {
        final String string = super.toString();
        return (string != null) ? string : this.getCanonicalString();
    }
    
    public String getCanonicalString() {
        return "(" + this.getUnit().toString() + ") @ " + this.getOffset();
    }
    
    public static void main(final String[] args) throws Exception {
        final BaseUnit kelvin = BaseUnit.getOrCreate(UnitName.newUnitName("kelvin", null, "K"), BaseQuantity.THERMODYNAMIC_TEMPERATURE);
        final OffsetUnit celsius = new OffsetUnit(kelvin, 273.15);
        System.out.println("celsius.equals(kelvin)=" + celsius.equals(kelvin));
        System.out.println("celsius.getUnit().equals(kelvin)=" + celsius.getUnit().equals(kelvin));
        final Unit celsiusKelvin = celsius.multiplyBy(kelvin);
        System.out.println("celsiusKelvin.divideBy(celsius)=" + celsiusKelvin.divideBy(celsius));
        System.out.println("celsius.divideBy(kelvin)=" + celsius.divideBy(kelvin));
        System.out.println("kelvin.divideBy(celsius)=" + kelvin.divideBy(celsius));
        System.out.println("celsius.raiseTo(2)=" + celsius.raiseTo(2));
        System.out.println("celsius.toDerivedUnit(1.)=" + celsius.toDerivedUnit(1.0));
        System.out.println("celsius.toDerivedUnit(new float[]{1,2,3}, new float[3])[1]=" + celsius.toDerivedUnit(new float[] { 1.0f, 2.0f, 3.0f }, new float[3])[1]);
        System.out.println("celsius.fromDerivedUnit(274.15)=" + celsius.fromDerivedUnit(274.15));
        System.out.println("celsius.fromDerivedUnit(new float[]{274.15f},new float[1])[0]=" + celsius.fromDerivedUnit(new float[] { 274.15f }, new float[1])[0]);
        System.out.println("celsius.equals(celsius)=" + celsius.equals(celsius));
        final OffsetUnit celsius2 = new OffsetUnit(celsius, 100.0);
        System.out.println("celsius.equals(celsius100)=" + celsius.equals(celsius2));
        System.out.println("celsius.isDimensionless()=" + celsius.isDimensionless());
        final BaseUnit radian = BaseUnit.getOrCreate(UnitName.newUnitName("radian", null, "rad"), BaseQuantity.PLANE_ANGLE);
        final OffsetUnit offRadian = new OffsetUnit(radian, 1.570795);
        System.out.println("offRadian.isDimensionless()=" + offRadian.isDimensionless());
    }
}
