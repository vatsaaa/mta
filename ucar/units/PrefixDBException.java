// 
// Decompiled by Procyon v0.5.36
// 

package ucar.units;

public class PrefixDBException extends UnitException
{
    private static final long serialVersionUID = 1L;
    
    public PrefixDBException() {
        super("Prefix database exception");
    }
    
    public PrefixDBException(final String message) {
        super("Prefix database exception: " + message);
    }
    
    public PrefixDBException(final Exception e) {
        this("Prefix database exception", e);
    }
    
    public PrefixDBException(final String message, final Exception e) {
        super(message, e);
    }
}
