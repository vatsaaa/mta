// 
// Decompiled by Procyon v0.5.36
// 

package ucar.units;

public final class QuantityDimension extends Dimension
{
    public QuantityDimension() {
    }
    
    public QuantityDimension(final BaseQuantity baseQuantity) {
        super(new Factor(baseQuantity));
    }
    
    protected QuantityDimension(final Factor[] factors) {
        super(factors);
    }
    
    public QuantityDimension multiplyBy(final QuantityDimension that) {
        return new QuantityDimension(this.mult(that));
    }
    
    public QuantityDimension divideBy(final QuantityDimension that) {
        return this.multiplyBy(that.raiseTo(-1));
    }
    
    public QuantityDimension raiseTo(final int power) {
        return new QuantityDimension(this.pow(power));
    }
    
    public static void main(final String[] args) throws Exception {
        System.out.println("new QuantityDimension() = \"" + new QuantityDimension() + '\"');
        final QuantityDimension timeDimension = new QuantityDimension(BaseQuantity.TIME);
        System.out.println("timeDimension = \"" + timeDimension + '\"');
        final QuantityDimension lengthDimension = new QuantityDimension(BaseQuantity.LENGTH);
        System.out.println("lengthDimension = \"" + lengthDimension + '\"');
        System.out.println("lengthDimension.isReciprocalOf(timeDimension) = \"" + lengthDimension.isReciprocalOf(timeDimension) + '\"');
        final QuantityDimension hertzDimension = timeDimension.raiseTo(-1);
        System.out.println("hertzDimension = \"" + hertzDimension + '\"');
        System.out.println("hertzDimension.isReciprocalOf(timeDimension) = \"" + hertzDimension.isReciprocalOf(timeDimension) + '\"');
        System.out.println("lengthDimension.divideBy(timeDimension) = \"" + lengthDimension.divideBy(timeDimension) + '\"');
        System.out.println("lengthDimension.divideBy(timeDimension).raiseTo(2) = \"" + lengthDimension.divideBy(timeDimension).raiseTo(2) + '\"');
    }
}
