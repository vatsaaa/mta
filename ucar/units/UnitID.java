// 
// Decompiled by Procyon v0.5.36
// 

package ucar.units;

import java.io.Serializable;

public abstract class UnitID implements Serializable
{
    private static final long serialVersionUID = 1L;
    
    public static UnitID newUnitID(final String name, final String plural, final String symbol) {
        UnitID id;
        try {
            id = ((name == null) ? new UnitSymbol(symbol) : UnitName.newUnitName(name, plural, symbol));
        }
        catch (NameException e) {
            id = null;
        }
        return id;
    }
    
    public abstract String getName();
    
    public abstract String getPlural();
    
    public abstract String getSymbol();
    
    @Override
    public abstract String toString();
}
