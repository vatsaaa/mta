// 
// Decompiled by Procyon v0.5.36
// 

package ucar.units;

public final class UnitSymbol extends UnitID
{
    private static final long serialVersionUID = 1L;
    private final String symbol;
    
    public UnitSymbol(final String symbol) throws NameException {
        if (symbol == null) {
            throw new NameException("Symbol can't be null");
        }
        this.symbol = symbol;
    }
    
    @Override
    public String getName() {
        return null;
    }
    
    @Override
    public String getPlural() {
        return null;
    }
    
    @Override
    public String getSymbol() {
        return this.symbol;
    }
    
    @Override
    public String toString() {
        return this.getSymbol();
    }
}
