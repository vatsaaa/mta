// 
// Decompiled by Procyon v0.5.36
// 

package ucar.units;

public abstract class Prefix implements Comparable<Object>
{
    private final double value;
    private final String id;
    
    protected Prefix(final String id, final double value) {
        this.id = id;
        this.value = value;
    }
    
    public final String getID() {
        return this.id;
    }
    
    @Override
    public final String toString() {
        return this.getID();
    }
    
    public final double getValue() {
        return this.value;
    }
    
    public abstract int compareTo(final Object p0);
    
    public abstract int compareTo(final String p0);
    
    public final int length() {
        return this.id.length();
    }
}
