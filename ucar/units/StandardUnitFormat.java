// 
// Decompiled by Procyon v0.5.36
// 

package ucar.units;

import java.text.DateFormat;
import java.util.Locale;
import java.util.Iterator;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.io.InputStream;
import java.util.TimeZone;
import java.util.Calendar;
import java.io.LineNumberReader;
import java.io.InputStreamReader;
import java.util.Date;
import java.util.Arrays;
import java.io.Reader;
import java.io.StringReader;
import java.util.List;
import java.util.Comparator;
import java.text.SimpleDateFormat;

public final class StandardUnitFormat extends UnitFormatImpl implements StandardUnitFormatConstants
{
    private static final long serialVersionUID = 2L;
    private static StandardUnitFormat _instance;
    private static final SimpleDateFormat dateFormat;
    private static final Comparator<Factor> factorComparator;
    public StandardUnitFormatTokenManager token_source;
    SimpleCharStream jj_input_stream;
    public Token token;
    public Token jj_nt;
    private int jj_ntk;
    private Token jj_scanpos;
    private Token jj_lastpos;
    private int jj_la;
    private int jj_gen;
    private final int[] jj_la1;
    private static int[] jj_la1_0;
    private final JJCalls[] jj_2_rtns;
    private boolean jj_rescan;
    private int jj_gc;
    private final LookaheadSuccess jj_ls;
    private List<int[]> jj_expentries;
    private int[] jj_expentry;
    private int jj_kind;
    private int[] jj_lasttokens;
    private int jj_endpos;
    
    private StandardUnitFormat() {
        this(new StringReader(""));
    }
    
    public static StandardUnitFormat instance() {
        if (StandardUnitFormat._instance == null) {
            synchronized (StandardUnitFormat.class) {
                if (StandardUnitFormat._instance == null) {
                    StandardUnitFormat._instance = new StandardUnitFormat();
                }
            }
        }
        return StandardUnitFormat._instance;
    }
    
    private static boolean isTimeUnit(final Unit unit) throws UnitSystemException {
        return unit.isCompatible(UnitSystemManager.instance().getBaseUnit(BaseQuantity.TIME));
    }
    
    public Unit parse(final String spec, final UnitDB unitDB) throws UnitParseException, SpecificationException, UnitDBException, PrefixDBException, UnitSystemException {
        this.ReInit(new StringReader(spec.trim()));
        try {
            final Unit unit = this.unitSpec(unitDB);
            return unit;
        }
        catch (TokenMgrError e) {
            throw new UnitParseException(spec, e);
        }
        catch (ParseException e2) {
            throw new UnitParseException(spec, (Throwable)e2);
        }
        catch (OperationException e3) {
            throw new SpecificationException(spec, (Throwable)e3);
        }
    }
    
    public StringBuffer format(final Factor factor, final StringBuffer buf) {
        return buf.append(factor.toString());
    }
    
    public StringBuffer format(final Unit unit, final StringBuffer buf) throws UnitClassException {
        return this.format(unit, buf, true);
    }
    
    public StringBuffer longFormat(final Unit unit, final StringBuffer buf) throws UnitClassException {
        return this.format(unit, buf, false);
    }
    
    private StringBuffer format(final Unit unit, final StringBuffer buf, final boolean normalize) throws UnitClassException {
        boolean done = false;
        if (!normalize) {
            String id = unit.getSymbol();
            if (id == null) {
                id = unit.getName();
            }
            if (id != null) {
                buf.append(id.replace(' ', '_'));
                done = true;
            }
        }
        if (!done) {
            if (unit instanceof BaseUnit) {
                this.format((BaseUnit)unit, buf);
            }
            else if (unit instanceof DerivedUnit) {
                this.format((DerivedUnit)unit, buf);
            }
            else if (unit instanceof ScaledUnit) {
                this.format((ScaledUnit)unit, buf, normalize);
            }
            else if (unit instanceof OffsetUnit) {
                this.format((OffsetUnit)unit, buf, normalize);
            }
            else {
                if (!(unit instanceof TimeScaleUnit)) {
                    throw new UnitClassException(unit);
                }
                this.format((TimeScaleUnit)unit, buf, normalize);
            }
        }
        return buf;
    }
    
    private StringBuffer format(final BaseUnit baseUnit, final StringBuffer buf) {
        return buf.append(baseUnit.getSymbol());
    }
    
    private StringBuffer format(final DerivedUnit unit, final StringBuffer buf) {
        final Factor[] factors = unit.getDimension().getFactors();
        Arrays.sort(factors, StandardUnitFormat.factorComparator);
        for (int i = 0; i < factors.length; ++i) {
            this.format(factors[i], buf).append('.');
        }
        if (factors.length != 0) {
            buf.setLength(buf.length() - 1);
        }
        return buf;
    }
    
    private StringBuffer format(final ScaledUnit unit, final StringBuffer buf, final boolean normalize) throws UnitClassException {
        final double scale = unit.getScale();
        if (scale != 0.0) {
            if (scale == 1.0) {
                this.format(unit.getUnit(), buf, normalize);
            }
            else {
                buf.append(scale).append(' ');
                final int start = buf.length();
                this.format(unit.getUnit(), buf, normalize);
                if (start == buf.length()) {
                    buf.setLength(start - 1);
                }
            }
        }
        return buf;
    }
    
    private StringBuffer format(final OffsetUnit unit, final StringBuffer buf, final boolean normalize) throws UnitClassException {
        final double offset = unit.getOffset();
        if (offset == 0.0) {
            this.format(unit.getUnit(), buf, normalize);
            return buf;
        }
        final int start = buf.length();
        this.format(unit.getUnit(), buf, normalize);
        return (isBlackSpace(buf, start) ? buf : buf.insert(start, '(').append(')')).append(" @ ").append(offset);
    }
    
    private static boolean isBlackSpace(final StringBuffer buf, final int start) {
        return buf.substring(start).indexOf(32) == -1;
    }
    
    private StringBuffer format(final TimeScaleUnit unit, final StringBuffer buf, final boolean normalize) throws UnitClassException {
        return this.format(unit.getUnit(), buf, normalize).append(StandardUnitFormat.dateFormat.format(unit.getOrigin()));
    }
    
    private static Unit getUnit(final UnitDB unitDB, final String string) throws UnitDBAccessException {
        return unitDB.get(string);
    }
    
    private static Prefix getPrefix(final String string) throws PrefixDBException {
        final PrefixDB prefixDB = PrefixDBManager.instance();
        Prefix prefix = prefixDB.getPrefixByName(string);
        if (prefix == null) {
            prefix = prefixDB.getPrefixBySymbol(string);
        }
        return prefix;
    }
    
    private static void myAssert(final StandardUnitFormat parser, final String spec, final Unit unit) throws NoSuchUnitException, UnitParseException, SpecificationException, UnitDBException, PrefixDBException, UnitSystemException {
        if (!parser.parse(spec).equals(unit)) {
            throw new AssertionError((Object)(spec + " != " + unit));
        }
        System.out.println(spec + " -> " + unit);
    }
    
    public static void main(final String[] args) throws Exception {
        final StandardUnitFormat parser = instance();
        final Unit m = parser.parse("m");
        final Unit s = parser.parse("s");
        final Unit epoch = parser.parse("s @ 1970-01-01 00:00:00 UTC");
        myAssert(parser, "m m", m.multiplyBy(m));
        myAssert(parser, "m.m", m.multiplyBy(m));
        myAssert(parser, "(m)(m)", m.multiplyBy(m));
        myAssert(parser, "m/s/s", m.divideBy(s).divideBy(s));
        myAssert(parser, "m2", m.raiseTo(2));
        myAssert(parser, "m2.s", m.raiseTo(2).multiplyBy(s));
        myAssert(parser, "m2/s", m.raiseTo(2).divideBy(s));
        myAssert(parser, "m^2/s", m.raiseTo(2).divideBy(s));
        myAssert(parser, "m s @ 5", m.multiplyBy(s).shiftTo(5.0));
        myAssert(parser, "m2 s @ 5", m.raiseTo(2).multiplyBy(s).shiftTo(5.0));
        myAssert(parser, "m2 s-1 @ 5", m.raiseTo(2).multiplyBy(s.raiseTo(-1)).shiftTo(5.0));
        myAssert(parser, "m s from 5", m.multiplyBy(s).shiftTo(5.0));
        myAssert(parser, "s@19700101", epoch);
        myAssert(parser, "s@19700101T000000", epoch);
        myAssert(parser, "s@19700101T000000.00", epoch);
        myAssert(parser, "s @ 1970-01-01T00:00:00.00", epoch);
        myAssert(parser, "s @ 1970-01-01 00:00:00.00", epoch);
        myAssert(parser, "s @ 1970-01-01 00:00:00.00 +0", epoch);
        myAssert(parser, "s @ 1970-01-01T00:00:00.00 -12", epoch.shiftTo(new Date(43200000L)));
        if (!parser.parse("days since 2009-06-14 04:00:00").equals(parser.parse("days since 2009-06-14 04:00:00 +00:00"))) {
            throw new AssertionError();
        }
        myAssert(parser, "lg(re: 1)", DerivedUnitImpl.DIMENSIONLESS.log(10.0));
        myAssert(parser, "0.1 lg(re 1 mm)", m.multiplyBy(0.001).log(10.0).multiplyBy(0.1));
        myAssert(parser, "m", m);
        myAssert(parser, "2 m s", m.multiplyBy(s).multiplyBy(2.0));
        myAssert(parser, "3.14 m.s", m.multiplyBy(s).multiplyBy(3.14));
        myAssert(parser, "1e9 (m)", m.multiplyBy(1.0E9));
        myAssert(parser, "(m s)2", m.multiplyBy(s).raiseTo(2));
        myAssert(parser, "m2.s-1", m.raiseTo(2).divideBy(s));
        myAssert(parser, "m2 s^-1", m.raiseTo(2).divideBy(s));
        myAssert(parser, "(m/s)2", m.divideBy(s).raiseTo(2));
        myAssert(parser, "m2/s-1", m.raiseTo(2).divideBy(s.raiseTo(-1)));
        myAssert(parser, "m2/s^-1", m.raiseTo(2).divideBy(s.raiseTo(-1)));
        myAssert(parser, ".5 m/(.25 s)2", m.multiplyBy(0.5).divideBy(s.multiplyBy(0.25).raiseTo(2)));
        myAssert(parser, "m.m-1.m", m.multiplyBy(m.raiseTo(-1)).multiplyBy(m));
        myAssert(parser, "2.0 m 1/2 s-1*(m/s^1)^-1 (1e9 m-1)(1e9 s-1)-1.m/s", m.multiplyBy(2.0).multiplyBy(0.5).multiplyBy(s.raiseTo(-1)).multiplyBy(m.divideBy(s.raiseTo(1)).raiseTo(-1)).multiplyBy(m.raiseTo(-1).multiplyBy(1.0E9)).multiplyBy(s.raiseTo(-1).multiplyBy(1.0E9).raiseTo(-1)).multiplyBy(m).divideBy(s));
        myAssert(parser, "m/km", m.divideBy(m.multiplyBy(1000.0)));
        final LineNumberReader lineInput = new LineNumberReader(new InputStreamReader(System.in));
        while (true) {
            System.out.print("Enter a unit specification or ^D to quit: ");
            final String spec = lineInput.readLine();
            if (spec == null) {
                break;
            }
            try {
                System.out.println(parser.parse(spec.trim()));
            }
            catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }
        System.out.println("");
    }
    
    public final Unit unitSpec(final UnitDB unitDB) throws ParseException, OperationException, UnitSystemException, PrefixDBException, UnitDBException {
        Unit unit = DerivedUnitImpl.DIMENSIONLESS;
        switch ((this.jj_ntk == -1) ? this.jj_ntk() : this.jj_ntk) {
            case 2:
            case 3:
            case 5:
            case 8:
            case 12:
            case 16:
            case 17:
            case 18:
            case 19:
            case 20:
            case 21: {
                unit = this.shiftExpr(unitDB);
                break;
            }
            default: {
                this.jj_la1[0] = this.jj_gen;
                break;
            }
        }
        this.jj_consume_token(0);
        return unit;
    }
    
    public final Unit shiftExpr(final UnitDB unitDB) throws ParseException, OperationException, UnitSystemException, PrefixDBException, UnitDBException {
        Unit unit = this.productExpr(unitDB);
        Label_0188: {
            switch ((this.jj_ntk == -1) ? this.jj_ntk() : this.jj_ntk) {
                case 15: {
                    this.jj_consume_token(15);
                    if (isTimeUnit(unit)) {
                        final Date timestamp = this.timeOriginExpr();
                        unit = unit.shiftTo(timestamp);
                        break;
                    }
                    switch ((this.jj_ntk == -1) ? this.jj_ntk() : this.jj_ntk) {
                        case 2:
                        case 3:
                        case 5:
                        case 12: {
                            final double origin = this.number();
                            unit = unit.shiftTo(origin);
                            break Label_0188;
                        }
                        default: {
                            this.jj_la1[1] = this.jj_gen;
                            this.jj_consume_token(-1);
                            throw new ParseException();
                        }
                    }
                    break;
                }
                default: {
                    this.jj_la1[2] = this.jj_gen;
                    break;
                }
            }
        }
        return unit;
    }
    
    public final Unit productExpr(final UnitDB unitDB) throws ParseException, OperationException, UnitSystemException, PrefixDBException, UnitDBException {
        Unit unit = this.powerExpr(unitDB);
        while (true) {
            switch ((this.jj_ntk == -1) ? this.jj_ntk() : this.jj_ntk) {
                case 1:
                case 2:
                case 3:
                case 5:
                case 8:
                case 12:
                case 13:
                case 14:
                case 16:
                case 17:
                case 18:
                case 19:
                case 20:
                case 21: {
                    switch ((this.jj_ntk == -1) ? this.jj_ntk() : this.jj_ntk) {
                        case 14: {
                            this.jj_consume_token(14);
                            final Unit unit2 = this.powerExpr(unitDB);
                            unit = unit.divideBy(unit2);
                            continue;
                        }
                        default: {
                            this.jj_la1[6] = this.jj_gen;
                            if (this.jj_2_1(2)) {
                                Label_0387: {
                                    switch ((this.jj_ntk == -1) ? this.jj_ntk() : this.jj_ntk) {
                                        case 1:
                                        case 12:
                                        case 13: {
                                            switch ((this.jj_ntk == -1) ? this.jj_ntk() : this.jj_ntk) {
                                                case 12: {
                                                    this.jj_consume_token(12);
                                                    break Label_0387;
                                                }
                                                case 13: {
                                                    this.jj_consume_token(13);
                                                    break Label_0387;
                                                }
                                                case 1: {
                                                    this.jj_consume_token(1);
                                                    break Label_0387;
                                                }
                                                default: {
                                                    this.jj_la1[4] = this.jj_gen;
                                                    this.jj_consume_token(-1);
                                                    throw new ParseException();
                                                }
                                            }
                                            break;
                                        }
                                        default: {
                                            this.jj_la1[5] = this.jj_gen;
                                            break;
                                        }
                                    }
                                }
                                final Unit unit2 = this.powerExpr(unitDB);
                                unit = unit.multiplyBy(unit2);
                                continue;
                            }
                            this.jj_consume_token(-1);
                            throw new ParseException();
                        }
                    }
                    break;
                }
                default: {
                    this.jj_la1[3] = this.jj_gen;
                    return unit;
                }
            }
        }
    }
    
    public final Unit powerExpr(final UnitDB unitDB) throws ParseException, OperationException, UnitSystemException, PrefixDBException, UnitDBException {
        Unit unit = this.basicExpr(unitDB);
        switch ((this.jj_ntk == -1) ? this.jj_ntk() : this.jj_ntk) {
            case 2:
            case 3:
            case 5:
            case 11: {
                switch ((this.jj_ntk == -1) ? this.jj_ntk() : this.jj_ntk) {
                    case 11: {
                        this.jj_consume_token(11);
                        break;
                    }
                    default: {
                        this.jj_la1[7] = this.jj_gen;
                        break;
                    }
                }
                final int exponent = this.integer();
                unit = unit.raiseTo(exponent);
                break;
            }
            default: {
                this.jj_la1[8] = this.jj_gen;
                break;
            }
        }
        return unit;
    }
    
    public final Unit basicExpr(final UnitDB unitDB) throws ParseException, OperationException, UnitSystemException, PrefixDBException, UnitDBException {
        Unit unit = null;
        switch ((this.jj_ntk == -1) ? this.jj_ntk() : this.jj_ntk) {
            case 2:
            case 3:
            case 5:
            case 12: {
                final double number = this.number();
                unit = DerivedUnitImpl.DIMENSIONLESS.multiplyBy(number);
                break;
            }
            case 16:
            case 17:
            case 18: {
                unit = this.unitIdentifier(unitDB);
                break;
            }
            case 19:
            case 20:
            case 21: {
                unit = this.logExpr(unitDB);
                break;
            }
            case 8: {
                this.jj_consume_token(8);
                switch ((this.jj_ntk == -1) ? this.jj_ntk() : this.jj_ntk) {
                    case 1: {
                        this.jj_consume_token(1);
                        break;
                    }
                    default: {
                        this.jj_la1[9] = this.jj_gen;
                        break;
                    }
                }
                unit = this.shiftExpr(unitDB);
                switch ((this.jj_ntk == -1) ? this.jj_ntk() : this.jj_ntk) {
                    case 1: {
                        this.jj_consume_token(1);
                        break;
                    }
                    default: {
                        this.jj_la1[10] = this.jj_gen;
                        break;
                    }
                }
                this.jj_consume_token(9);
                break;
            }
            default: {
                this.jj_la1[11] = this.jj_gen;
                this.jj_consume_token(-1);
                throw new ParseException();
            }
        }
        return unit;
    }
    
    public final Unit logExpr(final UnitDB unitDB) throws ParseException, OperationException, UnitSystemException, PrefixDBException, UnitDBException {
        double base = 0.0;
        switch ((this.jj_ntk == -1) ? this.jj_ntk() : this.jj_ntk) {
            case 19: {
                this.jj_consume_token(19);
                base = 2.0;
                break;
            }
            case 20: {
                this.jj_consume_token(20);
                base = 2.718281828459045;
                break;
            }
            case 21: {
                this.jj_consume_token(21);
                base = 10.0;
                break;
            }
            default: {
                this.jj_la1[12] = this.jj_gen;
                this.jj_consume_token(-1);
                throw new ParseException();
            }
        }
        final Unit ref = this.productExpr(unitDB);
        switch ((this.jj_ntk == -1) ? this.jj_ntk() : this.jj_ntk) {
            case 1: {
                this.jj_consume_token(1);
                break;
            }
            default: {
                this.jj_la1[13] = this.jj_gen;
                break;
            }
        }
        this.jj_consume_token(9);
        return ref.log(base);
    }
    
    public final double number() throws ParseException {
        double number = 0.0;
        if (this.jj_2_2(Integer.MAX_VALUE)) {
            number = this.real();
        }
        else {
            switch ((this.jj_ntk == -1) ? this.jj_ntk() : this.jj_ntk) {
                case 2:
                case 3:
                case 5: {
                    number = this.integer();
                    break;
                }
                default: {
                    this.jj_la1[14] = this.jj_gen;
                    this.jj_consume_token(-1);
                    throw new ParseException();
                }
            }
        }
        return number;
    }
    
    public final double real() throws ParseException {
        int sign = 1;
        double tenFactor = 1.0;
        switch ((this.jj_ntk == -1) ? this.jj_ntk() : this.jj_ntk) {
            case 2:
            case 3: {
                sign = this.sign();
                break;
            }
            default: {
                this.jj_la1[15] = this.jj_gen;
                break;
            }
        }
        double udecimal = 0.0;
        if (this.jj_2_3(2)) {
            udecimal = this.unsignedDecimal();
            switch ((this.jj_ntk == -1) ? this.jj_ntk() : this.jj_ntk) {
                case 10: {
                    tenFactor = this.tenFactor();
                    break;
                }
                default: {
                    this.jj_la1[16] = this.jj_gen;
                    break;
                }
            }
        }
        else {
            switch ((this.jj_ntk == -1) ? this.jj_ntk() : this.jj_ntk) {
                case 5: {
                    udecimal = this.unsignedInteger();
                    tenFactor = this.tenFactor();
                    break;
                }
                default: {
                    this.jj_la1[17] = this.jj_gen;
                    this.jj_consume_token(-1);
                    throw new ParseException();
                }
            }
        }
        return sign * udecimal * tenFactor;
    }
    
    public final int sign() throws ParseException {
        switch ((this.jj_ntk == -1) ? this.jj_ntk() : this.jj_ntk) {
            case 2: {
                this.jj_consume_token(2);
                return 1;
            }
            case 3: {
                this.jj_consume_token(3);
                return -1;
            }
            default: {
                this.jj_la1[18] = this.jj_gen;
                this.jj_consume_token(-1);
                throw new ParseException();
            }
        }
    }
    
    public final double unsignedDecimal() throws ParseException {
        int integer = 0;
        double fraction = 0.0;
        if (this.jj_2_4(3)) {
            switch ((this.jj_ntk == -1) ? this.jj_ntk() : this.jj_ntk) {
                case 5: {
                    integer = this.unsignedInteger();
                    break;
                }
                default: {
                    this.jj_la1[19] = this.jj_gen;
                    break;
                }
            }
            this.jj_consume_token(12);
            final Token token = this.jj_consume_token(5);
            fraction = Double.valueOf("." + token.image);
        }
        else {
            switch ((this.jj_ntk == -1) ? this.jj_ntk() : this.jj_ntk) {
                case 5: {
                    integer = this.unsignedInteger();
                    this.jj_consume_token(12);
                    break;
                }
                default: {
                    this.jj_la1[20] = this.jj_gen;
                    this.jj_consume_token(-1);
                    throw new ParseException();
                }
            }
        }
        return integer + fraction;
    }
    
    public final double tenFactor() throws ParseException {
        final Token token = this.jj_consume_token(10);
        return Double.valueOf("1" + token.image);
    }
    
    public final int integer() throws ParseException {
        int sign = 1;
        switch ((this.jj_ntk == -1) ? this.jj_ntk() : this.jj_ntk) {
            case 2:
            case 3: {
                sign = this.sign();
                break;
            }
            default: {
                this.jj_la1[21] = this.jj_gen;
                break;
            }
        }
        final int magnitude = this.unsignedInteger();
        return sign * magnitude;
    }
    
    public final int unsignedInteger() throws ParseException {
        final Token token = this.jj_consume_token(5);
        return Integer.valueOf(token.image);
    }
    
    public final Unit unitIdentifier(final UnitDB unitDB) throws ParseException, UnitDBException, UnitSystemException, PrefixDBException, OperationException {
        Token token = null;
        switch ((this.jj_ntk == -1) ? this.jj_ntk() : this.jj_ntk) {
            case 17: {
                token = this.jj_consume_token(17);
                break;
            }
            case 18: {
                token = this.jj_consume_token(18);
                break;
            }
            case 16: {
                token = this.jj_consume_token(16);
                break;
            }
            default: {
                this.jj_la1[22] = this.jj_gen;
                this.jj_consume_token(-1);
                throw new ParseException();
            }
        }
        String string = token.image;
        double scale = 1.0;
        Unit unit;
        for (unit = getUnit(unitDB, string); unit == null; unit = getUnit(unitDB, string)) {
            final Prefix prefix = getPrefix(string);
            if (prefix == null) {
                try {
                    unit = UnknownUnit.create(string);
                    break;
                }
                catch (NameException ex) {}
            }
            scale *= prefix.getValue();
            string = string.substring(prefix.length());
        }
        unit = unit.multiplyBy(scale);
        return unit;
    }
    
    public final Date timeOriginExpr() throws ParseException {
        final Calendar calendar = this.dateExpr();
        if (this.jj_2_6(2)) {
            switch ((this.jj_ntk == -1) ? this.jj_ntk() : this.jj_ntk) {
                case 17: {
                    this.jj_consume_token(17);
                    break;
                }
                case 1: {
                    this.jj_consume_token(1);
                    break;
                }
                default: {
                    this.jj_la1[23] = this.jj_gen;
                    this.jj_consume_token(-1);
                    throw new ParseException();
                }
            }
            this.clockExpr(calendar);
            if (this.jj_2_5(2)) {
                switch ((this.jj_ntk == -1) ? this.jj_ntk() : this.jj_ntk) {
                    case 1: {
                        this.jj_consume_token(1);
                        break;
                    }
                    default: {
                        this.jj_la1[24] = this.jj_gen;
                        break;
                    }
                }
                this.zoneExpr(calendar);
            }
        }
        return calendar.getTime();
    }
    
    public final Calendar dateExpr() throws ParseException {
        int sign = 1;
        int month = 1;
        int day = 1;
        boolean packed = true;
        switch ((this.jj_ntk == -1) ? this.jj_ntk() : this.jj_ntk) {
            case 2:
            case 3: {
                sign = this.sign();
                break;
            }
            default: {
                this.jj_la1[25] = this.jj_gen;
                break;
            }
        }
        int year = this.unsignedInteger();
        Label_0208: {
            switch ((this.jj_ntk == -1) ? this.jj_ntk() : this.jj_ntk) {
                case 3: {
                    this.jj_consume_token(3);
                    month = this.unsignedInteger();
                    packed = false;
                    switch ((this.jj_ntk == -1) ? this.jj_ntk() : this.jj_ntk) {
                        case 3: {
                            this.jj_consume_token(3);
                            day = this.unsignedInteger();
                            break Label_0208;
                        }
                        default: {
                            this.jj_la1[26] = this.jj_gen;
                            break Label_0208;
                        }
                    }
                    break;
                }
                default: {
                    this.jj_la1[27] = this.jj_gen;
                    break;
                }
            }
        }
        if (packed) {
            if (year >= 10000101) {
                day = year % 100;
                year /= 100;
            }
            if (year >= 100001) {
                month = year % 100;
                year /= 100;
            }
            if (sign < 0) {
                year = -year;
            }
        }
        if (month < 1 || month > 12) {
            throw new ParseException("invalid month in timestamp");
        }
        if (day < 1 || day > 31) {
            throw new ParseException("invalid day in timestamp");
        }
        final Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        calendar.clear();
        calendar.set(year, month - 1, day);
        return calendar;
    }
    
    public final Calendar clockExpr(final Calendar calendar) throws ParseException {
        int minute = 0;
        double seconds = 0.0;
        boolean packed = true;
        double hour = 0.0;
        if (this.jj_2_7(Integer.MAX_VALUE)) {
            hour = this.unsignedDecimal();
        }
        else {
            switch ((this.jj_ntk == -1) ? this.jj_ntk() : this.jj_ntk) {
                case 5: {
                    hour = this.unsignedInteger();
                    break;
                }
                default: {
                    this.jj_la1[28] = this.jj_gen;
                    this.jj_consume_token(-1);
                    throw new ParseException();
                }
            }
        }
        Label_0308: {
            switch ((this.jj_ntk == -1) ? this.jj_ntk() : this.jj_ntk) {
                case 4: {
                    this.jj_consume_token(4);
                    minute = this.unsignedInteger();
                    packed = false;
                    switch ((this.jj_ntk == -1) ? this.jj_ntk() : this.jj_ntk) {
                        case 4: {
                            this.jj_consume_token(4);
                            if (this.jj_2_8(Integer.MAX_VALUE)) {
                                seconds = this.unsignedDecimal();
                                break Label_0308;
                            }
                            switch ((this.jj_ntk == -1) ? this.jj_ntk() : this.jj_ntk) {
                                case 5: {
                                    seconds = this.unsignedInteger();
                                    break Label_0308;
                                }
                                default: {
                                    this.jj_la1[29] = this.jj_gen;
                                    this.jj_consume_token(-1);
                                    throw new ParseException();
                                }
                            }
                            break;
                        }
                        default: {
                            this.jj_la1[30] = this.jj_gen;
                            break Label_0308;
                        }
                    }
                    break;
                }
                default: {
                    this.jj_la1[31] = this.jj_gen;
                    break;
                }
            }
        }
        if (packed) {
            if (hour >= 100000.0) {
                seconds = hour % 100.0;
                hour /= 100.0;
            }
            if (hour >= 1000.0) {
                minute = (int)(hour % 100.0);
                hour /= 100.0;
            }
        }
        if (hour < 0.0 || hour > 23.0) {
            throw new ParseException("invalid hour in timestamp");
        }
        if (minute < 0 || minute > 59) {
            throw new ParseException("invalid minute in timestamp");
        }
        if (seconds < 0.0 || seconds > 61.0) {
            throw new ParseException("invalid seconds in timestamp");
        }
        calendar.set(11, (int)Math.round(hour));
        calendar.set(12, minute);
        final int s = (int)seconds;
        calendar.set(13, s);
        final int ms = (int)((seconds - s) * 1000.0);
        calendar.set(14, ms);
        return calendar;
    }
    
    public final Calendar zoneExpr(final Calendar calendar) throws ParseException {
        int sign = 1;
        int zoneMinute = 0;
        TimeZone timeZone = null;
        switch ((this.jj_ntk == -1) ? this.jj_ntk() : this.jj_ntk) {
            case 2:
            case 3:
            case 5: {
                switch ((this.jj_ntk == -1) ? this.jj_ntk() : this.jj_ntk) {
                    case 2:
                    case 3: {
                        sign = this.sign();
                        break;
                    }
                    default: {
                        this.jj_la1[32] = this.jj_gen;
                        break;
                    }
                }
                int zoneHour = this.unsignedInteger();
                switch ((this.jj_ntk == -1) ? this.jj_ntk() : this.jj_ntk) {
                    case 4:
                    case 5: {
                        switch ((this.jj_ntk == -1) ? this.jj_ntk() : this.jj_ntk) {
                            case 4: {
                                this.jj_consume_token(4);
                                break;
                            }
                            default: {
                                this.jj_la1[33] = this.jj_gen;
                                break;
                            }
                        }
                        zoneMinute = this.unsignedInteger();
                        break;
                    }
                    default: {
                        this.jj_la1[34] = this.jj_gen;
                        break;
                    }
                }
                if (zoneHour >= 100) {
                    zoneMinute += zoneHour % 100;
                    zoneHour /= 100;
                }
                if (zoneHour > 23 || zoneMinute > 59) {
                    throw new ParseException("invalid time-zone in timestamp");
                }
                timeZone = TimeZone.getTimeZone("UTC");
                timeZone.setRawOffset(sign * (zoneHour * 60 + zoneMinute) * 60 * 1000);
                break;
            }
            case 18: {
                final Token token = this.jj_consume_token(18);
                timeZone = TimeZone.getTimeZone(token.image);
                break;
            }
            default: {
                this.jj_la1[35] = this.jj_gen;
                this.jj_consume_token(-1);
                throw new ParseException();
            }
        }
        calendar.setTimeZone(timeZone);
        return calendar;
    }
    
    private boolean jj_2_1(final int xla) {
        this.jj_la = xla;
        final Token token = this.token;
        this.jj_scanpos = token;
        this.jj_lastpos = token;
        try {
            return !this.jj_3_1();
        }
        catch (LookaheadSuccess ls) {
            return true;
        }
        finally {
            this.jj_save(0, xla);
        }
    }
    
    private boolean jj_2_2(final int xla) {
        this.jj_la = xla;
        final Token token = this.token;
        this.jj_scanpos = token;
        this.jj_lastpos = token;
        try {
            return !this.jj_3_2();
        }
        catch (LookaheadSuccess ls) {
            return true;
        }
        finally {
            this.jj_save(1, xla);
        }
    }
    
    private boolean jj_2_3(final int xla) {
        this.jj_la = xla;
        final Token token = this.token;
        this.jj_scanpos = token;
        this.jj_lastpos = token;
        try {
            return !this.jj_3_3();
        }
        catch (LookaheadSuccess ls) {
            return true;
        }
        finally {
            this.jj_save(2, xla);
        }
    }
    
    private boolean jj_2_4(final int xla) {
        this.jj_la = xla;
        final Token token = this.token;
        this.jj_scanpos = token;
        this.jj_lastpos = token;
        try {
            return !this.jj_3_4();
        }
        catch (LookaheadSuccess ls) {
            return true;
        }
        finally {
            this.jj_save(3, xla);
        }
    }
    
    private boolean jj_2_5(final int xla) {
        this.jj_la = xla;
        final Token token = this.token;
        this.jj_scanpos = token;
        this.jj_lastpos = token;
        try {
            return !this.jj_3_5();
        }
        catch (LookaheadSuccess ls) {
            return true;
        }
        finally {
            this.jj_save(4, xla);
        }
    }
    
    private boolean jj_2_6(final int xla) {
        this.jj_la = xla;
        final Token token = this.token;
        this.jj_scanpos = token;
        this.jj_lastpos = token;
        try {
            return !this.jj_3_6();
        }
        catch (LookaheadSuccess ls) {
            return true;
        }
        finally {
            this.jj_save(5, xla);
        }
    }
    
    private boolean jj_2_7(final int xla) {
        this.jj_la = xla;
        final Token token = this.token;
        this.jj_scanpos = token;
        this.jj_lastpos = token;
        try {
            return !this.jj_3_7();
        }
        catch (LookaheadSuccess ls) {
            return true;
        }
        finally {
            this.jj_save(6, xla);
        }
    }
    
    private boolean jj_2_8(final int xla) {
        this.jj_la = xla;
        final Token token = this.token;
        this.jj_scanpos = token;
        this.jj_lastpos = token;
        try {
            return !this.jj_3_8();
        }
        catch (LookaheadSuccess ls) {
            return true;
        }
        finally {
            this.jj_save(7, xla);
        }
    }
    
    private boolean jj_3R_40() {
        return this.jj_scan_token(21);
    }
    
    private boolean jj_3R_33() {
        return this.jj_3R_24();
    }
    
    private boolean jj_3R_39() {
        return this.jj_scan_token(20);
    }
    
    private boolean jj_3R_41() {
        return this.jj_3R_3();
    }
    
    private boolean jj_3R_23() {
        final Token xsp = this.jj_scanpos;
        if (this.jj_3R_33()) {
            this.jj_scanpos = xsp;
        }
        return this.jj_3R_14();
    }
    
    private boolean jj_3R_38() {
        return this.jj_scan_token(19);
    }
    
    private boolean jj_3_8() {
        return this.jj_3R_5();
    }
    
    private boolean jj_3_5() {
        final Token xsp = this.jj_scanpos;
        if (this.jj_scan_token(1)) {
            this.jj_scanpos = xsp;
        }
        return this.jj_3R_7();
    }
    
    private boolean jj_3R_31() {
        final Token xsp = this.jj_scanpos;
        if (this.jj_3R_38()) {
            this.jj_scanpos = xsp;
            if (this.jj_3R_39()) {
                this.jj_scanpos = xsp;
                if (this.jj_3R_40()) {
                    return true;
                }
            }
        }
        return this.jj_3R_41();
    }
    
    private boolean jj_3R_26() {
        return this.jj_scan_token(10);
    }
    
    private boolean jj_3_6() {
        final Token xsp = this.jj_scanpos;
        if (this.jj_scan_token(17)) {
            this.jj_scanpos = xsp;
            if (this.jj_scan_token(1)) {
                return true;
            }
        }
        return this.jj_3R_8();
    }
    
    private boolean jj_3R_13() {
        return this.jj_3R_14() || this.jj_scan_token(12);
    }
    
    private boolean jj_3R_22() {
        if (this.jj_scan_token(8)) {
            return true;
        }
        final Token xsp = this.jj_scanpos;
        if (this.jj_scan_token(1)) {
            this.jj_scanpos = xsp;
        }
        return this.jj_3R_32();
    }
    
    private boolean jj_3R_21() {
        return this.jj_3R_31();
    }
    
    private boolean jj_3R_20() {
        return this.jj_3R_30();
    }
    
    private boolean jj_3R_6() {
        return this.jj_3R_14();
    }
    
    private boolean jj_3_7() {
        return this.jj_3R_5();
    }
    
    private boolean jj_3_4() {
        final Token xsp = this.jj_scanpos;
        if (this.jj_3R_6()) {
            this.jj_scanpos = xsp;
        }
        return this.jj_scan_token(12) || this.jj_scan_token(5);
    }
    
    private boolean jj_3R_19() {
        return this.jj_3R_29();
    }
    
    private boolean jj_3R_18() {
        return this.jj_3R_14();
    }
    
    private boolean jj_3R_32() {
        return this.jj_3R_41();
    }
    
    private boolean jj_3R_17() {
        return this.jj_3R_5();
    }
    
    private boolean jj_3R_5() {
        final Token xsp = this.jj_scanpos;
        if (this.jj_3_4()) {
            this.jj_scanpos = xsp;
            if (this.jj_3R_13()) {
                return true;
            }
        }
        return false;
    }
    
    private boolean jj_3R_9() {
        final Token xsp = this.jj_scanpos;
        if (this.jj_3R_19()) {
            this.jj_scanpos = xsp;
            if (this.jj_3R_20()) {
                this.jj_scanpos = xsp;
                if (this.jj_3R_21()) {
                    this.jj_scanpos = xsp;
                    if (this.jj_3R_22()) {
                        return true;
                    }
                }
            }
        }
        return false;
    }
    
    private boolean jj_3R_35() {
        return this.jj_scan_token(3);
    }
    
    private boolean jj_3R_8() {
        final Token xsp = this.jj_scanpos;
        if (this.jj_3R_17()) {
            this.jj_scanpos = xsp;
            if (this.jj_3R_18()) {
                return true;
            }
        }
        return false;
    }
    
    private boolean jj_3R_34() {
        return this.jj_scan_token(2);
    }
    
    private boolean jj_3R_24() {
        final Token xsp = this.jj_scanpos;
        if (this.jj_3R_34()) {
            this.jj_scanpos = xsp;
            if (this.jj_3R_35()) {
                return true;
            }
        }
        return false;
    }
    
    private boolean jj_3R_12() {
        return this.jj_3R_14() || this.jj_3R_26();
    }
    
    private boolean jj_3R_25() {
        return this.jj_3R_26();
    }
    
    private boolean jj_3R_16() {
        return this.jj_scan_token(18);
    }
    
    private boolean jj_3R_10() {
        final Token xsp = this.jj_scanpos;
        if (this.jj_scan_token(11)) {
            this.jj_scanpos = xsp;
        }
        return this.jj_3R_23();
    }
    
    private boolean jj_3_3() {
        if (this.jj_3R_5()) {
            return true;
        }
        final Token xsp = this.jj_scanpos;
        if (this.jj_3R_25()) {
            this.jj_scanpos = xsp;
        }
        return false;
    }
    
    private boolean jj_3R_3() {
        if (this.jj_3R_9()) {
            return true;
        }
        final Token xsp = this.jj_scanpos;
        if (this.jj_3R_10()) {
            this.jj_scanpos = xsp;
        }
        return false;
    }
    
    private boolean jj_3R_11() {
        return this.jj_3R_24();
    }
    
    private boolean jj_3R_4() {
        Token xsp = this.jj_scanpos;
        if (this.jj_3R_11()) {
            this.jj_scanpos = xsp;
        }
        xsp = this.jj_scanpos;
        if (this.jj_3_3()) {
            this.jj_scanpos = xsp;
            if (this.jj_3R_12()) {
                return true;
            }
        }
        return false;
    }
    
    private boolean jj_3_2() {
        return this.jj_3R_4();
    }
    
    private boolean jj_3R_30() {
        final Token xsp = this.jj_scanpos;
        if (this.jj_scan_token(17)) {
            this.jj_scanpos = xsp;
            if (this.jj_scan_token(18)) {
                this.jj_scanpos = xsp;
                if (this.jj_scan_token(16)) {
                    return true;
                }
            }
        }
        return false;
    }
    
    private boolean jj_3R_28() {
        final Token xsp = this.jj_scanpos;
        if (this.jj_scan_token(4)) {
            this.jj_scanpos = xsp;
        }
        return this.jj_3R_14();
    }
    
    private boolean jj_3R_27() {
        return this.jj_3R_24();
    }
    
    private boolean jj_3R_15() {
        Token xsp = this.jj_scanpos;
        if (this.jj_3R_27()) {
            this.jj_scanpos = xsp;
        }
        if (this.jj_3R_14()) {
            return true;
        }
        xsp = this.jj_scanpos;
        if (this.jj_3R_28()) {
            this.jj_scanpos = xsp;
        }
        return false;
    }
    
    private boolean jj_3R_37() {
        return this.jj_3R_23();
    }
    
    private boolean jj_3R_2() {
        final Token xsp = this.jj_scanpos;
        if (this.jj_scan_token(12)) {
            this.jj_scanpos = xsp;
            if (this.jj_scan_token(13)) {
                this.jj_scanpos = xsp;
                if (this.jj_scan_token(1)) {
                    return true;
                }
            }
        }
        return false;
    }
    
    private boolean jj_3R_36() {
        return this.jj_3R_4();
    }
    
    private boolean jj_3_1() {
        final Token xsp = this.jj_scanpos;
        if (this.jj_3R_2()) {
            this.jj_scanpos = xsp;
        }
        return this.jj_3R_3();
    }
    
    private boolean jj_3R_7() {
        final Token xsp = this.jj_scanpos;
        if (this.jj_3R_15()) {
            this.jj_scanpos = xsp;
            if (this.jj_3R_16()) {
                return true;
            }
        }
        return false;
    }
    
    private boolean jj_3R_14() {
        return this.jj_scan_token(5);
    }
    
    private boolean jj_3R_29() {
        final Token xsp = this.jj_scanpos;
        if (this.jj_3R_36()) {
            this.jj_scanpos = xsp;
            if (this.jj_3R_37()) {
                return true;
            }
        }
        return false;
    }
    
    private static void jj_la1_init_0() {
        StandardUnitFormat.jj_la1_0 = new int[] { 4133164, 4140, 32768, 4157742, 12290, 12290, 16384, 2048, 2092, 2, 2, 4133164, 3670016, 2, 44, 12, 1024, 32, 12, 32, 32, 12, 458752, 131074, 2, 12, 8, 8, 32, 32, 16, 16, 12, 16, 48, 262188 };
    }
    
    public StandardUnitFormat(final InputStream stream) {
        this(stream, null);
    }
    
    public StandardUnitFormat(final InputStream stream, final String encoding) {
        this.jj_la1 = new int[36];
        this.jj_2_rtns = new JJCalls[8];
        this.jj_rescan = false;
        this.jj_gc = 0;
        this.jj_ls = new LookaheadSuccess();
        this.jj_expentries = new ArrayList<int[]>();
        this.jj_kind = -1;
        this.jj_lasttokens = new int[100];
        try {
            this.jj_input_stream = new SimpleCharStream(stream, encoding, 1, 1);
        }
        catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
        this.token_source = new StandardUnitFormatTokenManager(this.jj_input_stream);
        this.token = new Token();
        this.jj_ntk = -1;
        this.jj_gen = 0;
        for (int i = 0; i < 36; ++i) {
            this.jj_la1[i] = -1;
        }
        for (int i = 0; i < this.jj_2_rtns.length; ++i) {
            this.jj_2_rtns[i] = new JJCalls();
        }
    }
    
    public void ReInit(final InputStream stream) {
        this.ReInit(stream, null);
    }
    
    public void ReInit(final InputStream stream, final String encoding) {
        try {
            this.jj_input_stream.ReInit(stream, encoding, 1, 1);
        }
        catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
        this.token_source.ReInit(this.jj_input_stream);
        this.token = new Token();
        this.jj_ntk = -1;
        this.jj_gen = 0;
        for (int i = 0; i < 36; ++i) {
            this.jj_la1[i] = -1;
        }
        for (int i = 0; i < this.jj_2_rtns.length; ++i) {
            this.jj_2_rtns[i] = new JJCalls();
        }
    }
    
    public StandardUnitFormat(final Reader stream) {
        this.jj_la1 = new int[36];
        this.jj_2_rtns = new JJCalls[8];
        this.jj_rescan = false;
        this.jj_gc = 0;
        this.jj_ls = new LookaheadSuccess();
        this.jj_expentries = new ArrayList<int[]>();
        this.jj_kind = -1;
        this.jj_lasttokens = new int[100];
        this.jj_input_stream = new SimpleCharStream(stream, 1, 1);
        this.token_source = new StandardUnitFormatTokenManager(this.jj_input_stream);
        this.token = new Token();
        this.jj_ntk = -1;
        this.jj_gen = 0;
        for (int i = 0; i < 36; ++i) {
            this.jj_la1[i] = -1;
        }
        for (int i = 0; i < this.jj_2_rtns.length; ++i) {
            this.jj_2_rtns[i] = new JJCalls();
        }
    }
    
    public void ReInit(final Reader stream) {
        this.jj_input_stream.ReInit(stream, 1, 1);
        this.token_source.ReInit(this.jj_input_stream);
        this.token = new Token();
        this.jj_ntk = -1;
        this.jj_gen = 0;
        for (int i = 0; i < 36; ++i) {
            this.jj_la1[i] = -1;
        }
        for (int i = 0; i < this.jj_2_rtns.length; ++i) {
            this.jj_2_rtns[i] = new JJCalls();
        }
    }
    
    public StandardUnitFormat(final StandardUnitFormatTokenManager tm) {
        this.jj_la1 = new int[36];
        this.jj_2_rtns = new JJCalls[8];
        this.jj_rescan = false;
        this.jj_gc = 0;
        this.jj_ls = new LookaheadSuccess();
        this.jj_expentries = new ArrayList<int[]>();
        this.jj_kind = -1;
        this.jj_lasttokens = new int[100];
        this.token_source = tm;
        this.token = new Token();
        this.jj_ntk = -1;
        this.jj_gen = 0;
        for (int i = 0; i < 36; ++i) {
            this.jj_la1[i] = -1;
        }
        for (int i = 0; i < this.jj_2_rtns.length; ++i) {
            this.jj_2_rtns[i] = new JJCalls();
        }
    }
    
    public void ReInit(final StandardUnitFormatTokenManager tm) {
        this.token_source = tm;
        this.token = new Token();
        this.jj_ntk = -1;
        this.jj_gen = 0;
        for (int i = 0; i < 36; ++i) {
            this.jj_la1[i] = -1;
        }
        for (int i = 0; i < this.jj_2_rtns.length; ++i) {
            this.jj_2_rtns[i] = new JJCalls();
        }
    }
    
    private Token jj_consume_token(final int kind) throws ParseException {
        final Token oldToken;
        if ((oldToken = this.token).next != null) {
            this.token = this.token.next;
        }
        else {
            final Token token = this.token;
            final Token nextToken = this.token_source.getNextToken();
            token.next = nextToken;
            this.token = nextToken;
        }
        this.jj_ntk = -1;
        if (this.token.kind == kind) {
            ++this.jj_gen;
            if (++this.jj_gc > 100) {
                this.jj_gc = 0;
                for (int i = 0; i < this.jj_2_rtns.length; ++i) {
                    for (JJCalls c = this.jj_2_rtns[i]; c != null; c = c.next) {
                        if (c.gen < this.jj_gen) {
                            c.first = null;
                        }
                    }
                }
            }
            return this.token;
        }
        this.token = oldToken;
        this.jj_kind = kind;
        throw this.generateParseException();
    }
    
    private boolean jj_scan_token(final int kind) {
        if (this.jj_scanpos == this.jj_lastpos) {
            --this.jj_la;
            if (this.jj_scanpos.next == null) {
                final Token jj_scanpos = this.jj_scanpos;
                final Token nextToken = this.token_source.getNextToken();
                jj_scanpos.next = nextToken;
                this.jj_scanpos = nextToken;
                this.jj_lastpos = nextToken;
            }
            else {
                final Token next = this.jj_scanpos.next;
                this.jj_scanpos = next;
                this.jj_lastpos = next;
            }
        }
        else {
            this.jj_scanpos = this.jj_scanpos.next;
        }
        if (this.jj_rescan) {
            int i = 0;
            Token tok;
            for (tok = this.token; tok != null && tok != this.jj_scanpos; tok = tok.next) {
                ++i;
            }
            if (tok != null) {
                this.jj_add_error_token(kind, i);
            }
        }
        if (this.jj_scanpos.kind != kind) {
            return true;
        }
        if (this.jj_la == 0 && this.jj_scanpos == this.jj_lastpos) {
            throw this.jj_ls;
        }
        return false;
    }
    
    public final Token getNextToken() {
        if (this.token.next != null) {
            this.token = this.token.next;
        }
        else {
            final Token token = this.token;
            final Token nextToken = this.token_source.getNextToken();
            token.next = nextToken;
            this.token = nextToken;
        }
        this.jj_ntk = -1;
        ++this.jj_gen;
        return this.token;
    }
    
    public final Token getToken(final int index) {
        Token t = this.token;
        for (int i = 0; i < index; ++i) {
            if (t.next != null) {
                t = t.next;
            }
            else {
                final Token token = t;
                final Token nextToken = this.token_source.getNextToken();
                token.next = nextToken;
                t = nextToken;
            }
        }
        return t;
    }
    
    private int jj_ntk() {
        final Token next = this.token.next;
        this.jj_nt = next;
        if (next == null) {
            final Token token = this.token;
            final Token nextToken = this.token_source.getNextToken();
            token.next = nextToken;
            return this.jj_ntk = nextToken.kind;
        }
        return this.jj_ntk = this.jj_nt.kind;
    }
    
    private void jj_add_error_token(final int kind, final int pos) {
        if (pos >= 100) {
            return;
        }
        if (pos == this.jj_endpos + 1) {
            this.jj_lasttokens[this.jj_endpos++] = kind;
        }
        else if (this.jj_endpos != 0) {
            this.jj_expentry = new int[this.jj_endpos];
            for (int i = 0; i < this.jj_endpos; ++i) {
                this.jj_expentry[i] = this.jj_lasttokens[i];
            }
        Label_0092:
            for (final int[] oldentry : this.jj_expentries) {
                if (oldentry.length == this.jj_expentry.length) {
                    for (int j = 0; j < this.jj_expentry.length; ++j) {
                        if (oldentry[j] != this.jj_expentry[j]) {
                            continue Label_0092;
                        }
                    }
                    this.jj_expentries.add(this.jj_expentry);
                    break;
                }
            }
            if (pos != 0) {
                this.jj_lasttokens[(this.jj_endpos = pos) - 1] = kind;
            }
        }
    }
    
    public ParseException generateParseException() {
        this.jj_expentries.clear();
        final boolean[] la1tokens = new boolean[22];
        if (this.jj_kind >= 0) {
            la1tokens[this.jj_kind] = true;
            this.jj_kind = -1;
        }
        for (int i = 0; i < 36; ++i) {
            if (this.jj_la1[i] == this.jj_gen) {
                for (int j = 0; j < 32; ++j) {
                    if ((StandardUnitFormat.jj_la1_0[i] & 1 << j) != 0x0) {
                        la1tokens[j] = true;
                    }
                }
            }
        }
        for (int i = 0; i < 22; ++i) {
            if (la1tokens[i]) {
                (this.jj_expentry = new int[1])[0] = i;
                this.jj_expentries.add(this.jj_expentry);
            }
        }
        this.jj_endpos = 0;
        this.jj_rescan_token();
        this.jj_add_error_token(0, 0);
        final int[][] exptokseq = new int[this.jj_expentries.size()][];
        for (int k = 0; k < this.jj_expentries.size(); ++k) {
            exptokseq[k] = this.jj_expentries.get(k);
        }
        return new ParseException(this.token, exptokseq, StandardUnitFormat.tokenImage);
    }
    
    public final void enable_tracing() {
    }
    
    public final void disable_tracing() {
    }
    
    private void jj_rescan_token() {
        this.jj_rescan = true;
        for (int i = 0; i < 8; ++i) {
            try {
                JJCalls p = this.jj_2_rtns[i];
                do {
                    if (p.gen > this.jj_gen) {
                        this.jj_la = p.arg;
                        final Token first = p.first;
                        this.jj_scanpos = first;
                        this.jj_lastpos = first;
                        switch (i) {
                            case 0: {
                                this.jj_3_1();
                                break;
                            }
                            case 1: {
                                this.jj_3_2();
                                break;
                            }
                            case 2: {
                                this.jj_3_3();
                                break;
                            }
                            case 3: {
                                this.jj_3_4();
                                break;
                            }
                            case 4: {
                                this.jj_3_5();
                                break;
                            }
                            case 5: {
                                this.jj_3_6();
                                break;
                            }
                            case 6: {
                                this.jj_3_7();
                                break;
                            }
                            case 7: {
                                this.jj_3_8();
                                break;
                            }
                        }
                    }
                    p = p.next;
                } while (p != null);
            }
            catch (LookaheadSuccess lookaheadSuccess) {}
        }
        this.jj_rescan = false;
    }
    
    private void jj_save(final int index, final int xla) {
        JJCalls p;
        for (p = this.jj_2_rtns[index]; p.gen > this.jj_gen; p = p.next) {
            if (p.next == null) {
                final JJCalls jjCalls = p;
                final JJCalls next = new JJCalls();
                jjCalls.next = next;
                p = next;
                break;
            }
        }
        p.gen = this.jj_gen + xla - this.jj_la;
        p.first = this.token;
        p.arg = xla;
    }
    
    static {
        factorComparator = new Comparator<Factor>() {
            public int compare(final Factor f1, final Factor f2) {
                int comp = f2.getExponent() - f1.getExponent();
                if (comp == 0) {
                    comp = f1.getID().compareTo(f2.getID());
                }
                return comp;
            }
        };
        (dateFormat = (SimpleDateFormat)DateFormat.getDateInstance(3, Locale.US)).setTimeZone(TimeZone.getTimeZone("UTC"));
        StandardUnitFormat.dateFormat.applyPattern(" '@' yyyy-MM-dd HH:mm:ss.SSS 'UTC'");
        jj_la1_init_0();
    }
    
    private static final class LookaheadSuccess extends Error
    {
    }
    
    static final class JJCalls
    {
        int gen;
        Token first;
        int arg;
        JJCalls next;
    }
}
