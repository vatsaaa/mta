// 
// Decompiled by Procyon v0.5.36
// 

package ucar.units;

public class UnitDBAccessException extends UnitDBException
{
    private static final long serialVersionUID = 1L;
    
    public UnitDBAccessException(final String reason) {
        super("Couldn't access unit database: " + reason);
    }
}
