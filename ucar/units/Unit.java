// 
// Decompiled by Procyon v0.5.36
// 

package ucar.units;

import java.util.Date;

public interface Unit
{
    UnitName getUnitName();
    
    String getName();
    
    String getPlural();
    
    String getSymbol();
    
    String toString();
    
    String getCanonicalString();
    
    DerivedUnit getDerivedUnit();
    
    Unit clone(final UnitName p0);
    
    Unit multiplyBy(final Unit p0) throws MultiplyException;
    
    Unit multiplyBy(final double p0) throws MultiplyException;
    
    Unit divideBy(final Unit p0) throws OperationException;
    
    Unit divideInto(final Unit p0) throws OperationException;
    
    Unit raiseTo(final int p0) throws RaiseException;
    
    Unit shiftTo(final double p0) throws ShiftException;
    
    Unit shiftTo(final Date p0) throws ShiftException;
    
    Unit log(final double p0);
    
    Converter getConverterTo(final Unit p0) throws ConversionException;
    
    float convertTo(final float p0, final Unit p1) throws ConversionException;
    
    double convertTo(final double p0, final Unit p1) throws ConversionException;
    
    float[] convertTo(final float[] p0, final Unit p1) throws ConversionException;
    
    double[] convertTo(final double[] p0, final Unit p1) throws ConversionException;
    
    float[] convertTo(final float[] p0, final Unit p1, final float[] p2) throws ConversionException;
    
    double[] convertTo(final double[] p0, final Unit p1, final double[] p2) throws ConversionException;
    
    boolean isCompatible(final Unit p0);
    
    boolean equals(final Object p0);
    
    String makeLabel(final String p0);
    
    boolean isDimensionless();
}
