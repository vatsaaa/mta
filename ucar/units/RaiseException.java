// 
// Decompiled by Procyon v0.5.36
// 

package ucar.units;

public final class RaiseException extends OperationException
{
    private static final long serialVersionUID = 1L;
    
    public RaiseException(final Unit unit) {
        super("Can't exponentiate unit \"" + unit + "\"");
    }
}
