// 
// Decompiled by Procyon v0.5.36
// 

package ucar.units;

public class UnitFormatException extends UnitException
{
    private static final long serialVersionUID = 1L;
    
    public UnitFormatException() {
    }
    
    public UnitFormatException(final String message) {
        super(message);
    }
}
