// 
// Decompiled by Procyon v0.5.36
// 

package ucar.units;

public interface DerivableUnit
{
    DerivedUnit getDerivedUnit();
    
    float toDerivedUnit(final float p0) throws ConversionException;
    
    double toDerivedUnit(final double p0) throws ConversionException;
    
    float[] toDerivedUnit(final float[] p0, final float[] p1) throws ConversionException;
    
    double[] toDerivedUnit(final double[] p0, final double[] p1) throws ConversionException;
    
    float fromDerivedUnit(final float p0) throws ConversionException;
    
    double fromDerivedUnit(final double p0) throws ConversionException;
    
    float[] fromDerivedUnit(final float[] p0, final float[] p1) throws ConversionException;
    
    double[] fromDerivedUnit(final double[] p0, final double[] p1) throws ConversionException;
}
