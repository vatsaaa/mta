// 
// Decompiled by Procyon v0.5.36
// 

package ucar.units;

public final class UnitName extends UnitID implements Comparable<Object>
{
    private static final long serialVersionUID = 1L;
    private final String name;
    private final String plural;
    private final String symbol;
    
    protected UnitName(final String name, final String symbol) throws NameException {
        this(name, null, symbol);
    }
    
    protected UnitName(final String name, final String plural, final String symbol) throws NameException {
        if (name == null) {
            throw new NameException("Unit name can't be null");
        }
        this.name = name;
        this.plural = ((plural == null) ? this.makePlural(name) : plural);
        this.symbol = symbol;
    }
    
    public static UnitName newUnitName(final String name) throws NameException {
        return newUnitName(name, null);
    }
    
    public static UnitName newUnitName(final String name, final String plural) throws NameException {
        return newUnitName(name, plural, null);
    }
    
    public static UnitName newUnitName(final String name, final String plural, final String symbol) throws NameException {
        return new UnitName(name, plural, symbol);
    }
    
    @Override
    public final String getName() {
        return this.name;
    }
    
    @Override
    public String getPlural() {
        return this.plural;
    }
    
    @Override
    public final String getSymbol() {
        return this.symbol;
    }
    
    @Override
    public final String toString() {
        final String string = this.getSymbol();
        return (string == null) ? this.getName() : string;
    }
    
    public final int compareTo(final Object object) {
        return this.getName().compareToIgnoreCase(((UnitName)object).getName());
    }
    
    @Override
    public final boolean equals(final Object object) {
        return object instanceof UnitName && this.compareTo(object) == 0;
    }
    
    @Override
    public int hashCode() {
        return this.getName().toLowerCase().hashCode();
    }
    
    protected String makePlural(final String name) {
        final int length = name.length();
        final char lastChar = name.charAt(length - 1);
        String plural;
        if (lastChar != 'y') {
            plural = name + ((lastChar == 's' || lastChar == 'x' || lastChar == 'z' || name.endsWith("ch")) ? "es" : "s");
        }
        else if (length == 1) {
            plural = name + "s";
        }
        else {
            final char penultimateChar = name.charAt(length - 2);
            plural = ((penultimateChar == 'a' || penultimateChar == 'e' || penultimateChar == 'i' || penultimateChar == 'o' || penultimateChar == 'u') ? (name + "s") : (name.substring(0, length - 1) + "ies"));
        }
        return plural;
    }
}
