// 
// Decompiled by Procyon v0.5.36
// 

package ucar.units;

import java.util.Date;
import java.io.Serializable;

public abstract class UnitImpl implements Unit, Serializable
{
    private static final long serialVersionUID = 1L;
    private final UnitName id;
    
    protected UnitImpl() {
        this(null);
    }
    
    protected UnitImpl(final UnitName id) {
        this.id = id;
    }
    
    public final UnitName getUnitName() {
        return this.id;
    }
    
    public final String getName() {
        return (this.id == null) ? null : this.id.getName();
    }
    
    public final String getPlural() {
        return (this.id == null) ? null : this.id.getPlural();
    }
    
    public final String getSymbol() {
        return (this.id == null) ? null : this.id.getSymbol();
    }
    
    public Unit shiftTo(final double origin) throws ShiftException {
        return OffsetUnit.getInstance(this, origin);
    }
    
    public Unit shiftTo(final Date origin) throws ShiftException {
        return TimeScaleUnit.getInstance(this, origin);
    }
    
    public final Unit multiplyBy(final Unit that) throws MultiplyException {
        return this.myMultiplyBy(that);
    }
    
    public Unit multiplyBy(final double scale) throws MultiplyException {
        return ScaledUnit.getInstance(scale, this);
    }
    
    protected abstract Unit myMultiplyBy(final Unit p0) throws MultiplyException;
    
    public final Unit divideBy(final Unit that) throws OperationException {
        return this.myDivideBy(that);
    }
    
    protected abstract Unit myDivideBy(final Unit p0) throws OperationException;
    
    public final Unit divideInto(final Unit that) throws OperationException {
        return this.myDivideInto(that);
    }
    
    protected abstract Unit myDivideInto(final Unit p0) throws OperationException;
    
    public final Unit raiseTo(final int power) throws RaiseException {
        return this.myRaiseTo(power);
    }
    
    protected abstract Unit myRaiseTo(final int p0) throws RaiseException;
    
    public Unit log(final double base) {
        return LogarithmicUnit.getInstance(this, base);
    }
    
    public Converter getConverterTo(final Unit outputUnit) throws ConversionException {
        return new MyConverter(this, outputUnit);
    }
    
    public float convertTo(final float amount, final Unit outputUnit) throws ConversionException {
        return (float)this.convertTo((double)amount, outputUnit);
    }
    
    public double convertTo(final double amount, final Unit outputUnit) throws ConversionException {
        return this.getConverterTo(outputUnit).convert(amount);
    }
    
    public float[] convertTo(final float[] amounts, final Unit outputUnit) throws ConversionException {
        return this.convertTo(amounts, outputUnit, new float[amounts.length]);
    }
    
    public double[] convertTo(final double[] amounts, final Unit outputUnit) throws ConversionException {
        return this.convertTo(amounts, outputUnit, new double[amounts.length]);
    }
    
    public float[] convertTo(final float[] input, final Unit outputUnit, final float[] output) throws ConversionException {
        return this.getConverterTo(outputUnit).convert(input, output);
    }
    
    public double[] convertTo(final double[] input, final Unit outputUnit, final double[] output) throws ConversionException {
        return this.getConverterTo(outputUnit).convert(input, output);
    }
    
    public boolean isCompatible(final Unit that) {
        final Unit u1 = this.getDerivedUnit();
        return u1.equals(that.getDerivedUnit());
    }
    
    @Override
    public abstract int hashCode();
    
    private static final boolean equals(final String s1, final String s2) {
        return (s1 == null && s2 == null) || (s1 != null && s2 != null && s1.equals(s2));
    }
    
    private static final boolean equalsIgnoreCase(final String s1, final String s2) {
        return (s1 == null && s2 == null) || (s1 != null && s2 != null && s1.equalsIgnoreCase(s2));
    }
    
    @Override
    public String toString() {
        final String string = this.getSymbol();
        return (string != null) ? string : this.getName();
    }
    
    public String makeLabel(final String quantityID) {
        final StringBuffer buf = new StringBuffer(quantityID);
        if (quantityID.indexOf(" ") != -1) {
            buf.insert(0, '(').append(')');
        }
        buf.append('/');
        final int start = buf.length();
        buf.append(this.toString());
        if (buf.substring(start).indexOf(32) != -1) {
            buf.insert(start, '(').append(')');
        }
        return buf.toString();
    }
    
    protected static class MyConverter extends ConverterImpl
    {
        private final DerivableUnit fromUnit;
        private final DerivableUnit toUnit;
        
        protected MyConverter(final Unit fromUnit, final Unit toUnit) throws ConversionException {
            super(fromUnit, toUnit);
            if (!(fromUnit instanceof DerivableUnit) || !(toUnit instanceof DerivableUnit)) {
                throw new ConversionException(fromUnit, toUnit);
            }
            this.fromUnit = (DerivableUnit)fromUnit;
            this.toUnit = (DerivableUnit)toUnit;
        }
        
        public double convert(final double amount) {
            double output;
            try {
                output = this.toUnit.fromDerivedUnit(this.fromUnit.toDerivedUnit(amount));
            }
            catch (ConversionException e) {
                output = 0.0;
            }
            return output;
        }
        
        public float[] convert(final float[] input, final float[] output) {
            try {
                this.toUnit.fromDerivedUnit(this.fromUnit.toDerivedUnit(input, output), output);
            }
            catch (ConversionException ex) {}
            return output;
        }
        
        public double[] convert(final double[] input, final double[] output) {
            try {
                this.toUnit.fromDerivedUnit(this.fromUnit.toDerivedUnit(input, output), output);
            }
            catch (ConversionException ex) {}
            return output;
        }
    }
}
