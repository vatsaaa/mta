// 
// Decompiled by Procyon v0.5.36
// 

package ucar.units;

import java.text.DateFormat;
import java.util.Locale;
import java.util.Calendar;
import java.util.TimeZone;
import java.text.SimpleDateFormat;
import java.util.Date;

public final class TimeScaleUnit extends UnitImpl
{
    private static final long serialVersionUID = 1L;
    private final Unit _unit;
    private final Date _origin;
    private static final SimpleDateFormat dateFormat;
    private static final BaseUnit SECOND;
    
    public TimeScaleUnit(final Unit unit, final Date origin) throws BadUnitException, UnitSystemException {
        this(unit, origin, null);
    }
    
    public TimeScaleUnit(final Unit unit, final Date origin, final UnitName id) throws BadUnitException, UnitSystemException {
        super(id);
        if (!unit.isCompatible(UnitSystemManager.instance().getBaseUnit(BaseQuantity.TIME))) {
            throw new BadUnitException("\"" + unit + "\" is not a unit of time");
        }
        this._unit = unit;
        this._origin = origin;
    }
    
    static Unit getInstance(final Unit unit, final Date origin) throws ShiftException {
        try {
            return (unit instanceof TimeScaleUnit) ? new TimeScaleUnit(((TimeScaleUnit)unit)._unit, origin) : new TimeScaleUnit(unit, origin);
        }
        catch (Exception e) {
            throw (ShiftException)new ShiftException(unit, origin).initCause(e);
        }
    }
    
    public Unit getUnit() {
        return this._unit;
    }
    
    public Date getOrigin() {
        return this._origin;
    }
    
    public Unit clone(final UnitName id) {
        Unit clone;
        try {
            clone = new TimeScaleUnit(this.getUnit(), this.getOrigin(), id);
        }
        catch (UnitException e) {
            clone = null;
        }
        return clone;
    }
    
    @Override
    public Unit shiftTo(final double origin) throws ShiftException {
        Date newOrigin;
        try {
            newOrigin = new Date(this._origin.getTime() + (long)(this._unit.convertTo(origin, TimeScaleUnit.SECOND) * 1000.0));
        }
        catch (ConversionException e) {
            throw (ShiftException)new ShiftException(this, origin).initCause(e);
        }
        try {
            return new TimeScaleUnit(this._unit, newOrigin);
        }
        catch (BadUnitException e3) {
            throw new AssertionError();
        }
        catch (UnitSystemException e2) {
            throw (ShiftException)new ShiftException(this, origin).initCause(e2);
        }
    }
    
    @Override
    public Unit shiftTo(final Date origin) throws ShiftException {
        return getInstance(this._unit, origin);
    }
    
    @Override
    protected Unit myMultiplyBy(final Unit that) throws MultiplyException {
        throw new MultiplyException(this);
    }
    
    @Override
    protected Unit myDivideBy(final Unit that) throws DivideException {
        throw new DivideException(this);
    }
    
    @Override
    protected Unit myDivideInto(final Unit that) throws DivideException {
        throw new DivideException(that, this);
    }
    
    @Override
    protected Unit myRaiseTo(final int power) throws RaiseException {
        throw new RaiseException(this);
    }
    
    public DerivedUnit getDerivedUnit() {
        return this.getUnit().getDerivedUnit();
    }
    
    @Override
    public Converter getConverterTo(final Unit outputUnit) throws ConversionException {
        return new MyConverter(this, outputUnit);
    }
    
    @Override
    public final boolean isCompatible(final Unit that) {
        return that instanceof TimeScaleUnit;
    }
    
    @Override
    public boolean equals(final Object object) {
        if (this == object) {
            return true;
        }
        if (!(object instanceof TimeScaleUnit)) {
            return false;
        }
        final TimeScaleUnit that = (TimeScaleUnit)object;
        return this._origin.equals(that._origin) && this._unit.equals(that._unit);
    }
    
    @Override
    public int hashCode() {
        return this.getUnit().hashCode() ^ this.getOrigin().hashCode();
    }
    
    public boolean isDimensionless() {
        return false;
    }
    
    @Override
    public String toString() {
        final String string = super.toString();
        return (string != null) ? string : this.getCanonicalString();
    }
    
    public String getCanonicalString() {
        return this.getUnit().toString() + TimeScaleUnit.dateFormat.format(this.getOrigin());
    }
    
    public static void main(final String[] args) throws Exception {
        final TimeZone tz = TimeZone.getTimeZone("UTC");
        final Calendar calendar = Calendar.getInstance(tz);
        calendar.clear();
        calendar.set(1970, 0, 1);
        new TimeScaleUnit(TimeScaleUnit.SECOND, calendar.getTime());
    }
    
    static {
        (dateFormat = (SimpleDateFormat)DateFormat.getDateInstance(3, Locale.US)).setTimeZone(TimeZone.getTimeZone("UTC"));
        TimeScaleUnit.dateFormat.applyPattern(" 'since' yyyy-MM-dd HH:mm:ss.SSS 'UTC'");
        try {
            SECOND = BaseUnit.getOrCreate(UnitName.newUnitName("second", null, "s"), BaseQuantity.TIME);
        }
        catch (Exception e) {
            throw (ExceptionInInitializerError)new ExceptionInInitializerError().initCause(e);
        }
    }
    
    protected static final class MyConverter extends ConverterImpl
    {
        private final double offset;
        private final Converter converter;
        
        protected MyConverter(final TimeScaleUnit fromUnit, final Unit toUnit) throws ConversionException {
            super(fromUnit, toUnit);
            this.converter = fromUnit.getUnit().getConverterTo(((TimeScaleUnit)toUnit).getUnit());
            this.offset = SI.SECOND.convertTo((fromUnit.getOrigin().getTime() - ((TimeScaleUnit)toUnit).getOrigin().getTime()) / 1000.0, ((TimeScaleUnit)toUnit).getUnit());
        }
        
        public double convert(final double amount) {
            return this.converter.convert(amount) + this.offset;
        }
        
        public float[] convert(final float[] input, float[] output) {
            output = this.converter.convert(input, output);
            int i = input.length;
            while (--i >= 0) {
                final float[] array = output;
                final int n = i;
                array[n] += (float)this.offset;
            }
            return output;
        }
        
        public double[] convert(final double[] input, double[] output) {
            output = this.converter.convert(input, output);
            int i = input.length;
            while (--i >= 0) {
                final double[] array = output;
                final int n = i;
                array[n] += this.offset;
            }
            return output;
        }
    }
}
