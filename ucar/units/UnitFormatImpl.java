// 
// Decompiled by Procyon v0.5.36
// 

package ucar.units;

public abstract class UnitFormatImpl implements UnitFormat
{
    private static final long serialVersionUID = 1L;
    private static Object MUTEX;
    
    public final Unit parse(final String spec) throws NoSuchUnitException, UnitParseException, SpecificationException, UnitDBException, PrefixDBException, UnitSystemException {
        synchronized (UnitFormatImpl.MUTEX) {
            return this.parse(spec, UnitDBManager.instance());
        }
    }
    
    public final String format(final Factor factor) {
        return this.format(factor, new StringBuffer(8)).toString();
    }
    
    public final String format(final Unit unit) throws UnitClassException {
        return this.format(unit, new StringBuffer(80)).toString();
    }
    
    public final String longFormat(final Unit unit) throws UnitClassException {
        return this.longFormat(unit, new StringBuffer(80)).toString();
    }
    
    static {
        UnitFormatImpl.MUTEX = new Object();
    }
}
