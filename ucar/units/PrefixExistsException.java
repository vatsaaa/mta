// 
// Decompiled by Procyon v0.5.36
// 

package ucar.units;

public class PrefixExistsException extends PrefixDBException
{
    private static final long serialVersionUID = 1L;
    
    public PrefixExistsException(final Prefix oldPrefix, final Prefix newPrefix) {
        super("Attempt to replace \"" + oldPrefix + "\" with \"" + newPrefix + "\" in prefix database");
    }
}
