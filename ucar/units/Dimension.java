// 
// Decompiled by Procyon v0.5.36
// 

package ucar.units;

public abstract class Dimension
{
    protected final Factor[] _factors;
    private transient volatile int hashCode;
    
    public Dimension() {
        this(new Factor[0]);
    }
    
    protected Dimension(final Factor factor) {
        this(new Factor[] { factor });
    }
    
    protected Dimension(final Factor[] factors) {
        this._factors = factors;
    }
    
    public final int getRank() {
        return this._factors.length;
    }
    
    public final Factor[] getFactors() {
        final Factor[] factors = new Factor[this._factors.length];
        System.arraycopy(this._factors, 0, factors, 0, factors.length);
        return factors;
    }
    
    protected Factor[] mult(final Dimension that) {
        final Factor[] factors1 = this._factors;
        final Factor[] factors2 = that._factors;
        int i1 = 0;
        int i2 = 0;
        int k = 0;
        Factor[] newFactors = new Factor[factors1.length + factors2.length];
        while (true) {
            while (i1 != factors1.length) {
                if (i2 == factors2.length) {
                    final int n = factors1.length - i1;
                    System.arraycopy(factors1, i1, newFactors, k, n);
                    k += n;
                    if (k < newFactors.length) {
                        final Factor[] tmp = new Factor[k];
                        System.arraycopy(newFactors, 0, tmp, 0, k);
                        newFactors = tmp;
                    }
                    return newFactors;
                }
                final Factor f1 = factors1[i1];
                final Factor f2 = factors2[i2];
                final int comp = f1.getID().compareTo(f2.getID());
                if (comp < 0) {
                    newFactors[k++] = f1;
                    ++i1;
                }
                else if (comp == 0) {
                    final int exponent = f1.getExponent() + f2.getExponent();
                    if (exponent != 0) {
                        newFactors[k++] = new Factor(f1, exponent);
                    }
                    ++i1;
                    ++i2;
                }
                else {
                    newFactors[k++] = f2;
                    ++i2;
                }
            }
            final int n = factors2.length - i2;
            System.arraycopy(factors2, i2, newFactors, k, n);
            k += n;
            continue;
        }
    }
    
    protected Factor[] pow(final int power) {
        Factor[] factors;
        if (power == 0) {
            factors = new Factor[0];
        }
        else {
            factors = this.getFactors();
            if (power != 1) {
                int i = factors.length;
                while (--i >= 0) {
                    factors[i] = factors[i].pow(power);
                }
            }
        }
        return factors;
    }
    
    public final boolean isReciprocalOf(final Dimension that) {
        final Factor[] theseFactors = this._factors;
        final Factor[] thoseFactors = that._factors;
        boolean isReciprocalOf;
        if (theseFactors.length != thoseFactors.length) {
            isReciprocalOf = false;
        }
        else {
            int i = theseFactors.length;
            while (--i >= 0 && theseFactors[i].isReciprocalOf(thoseFactors[i])) {}
            isReciprocalOf = (i < 0);
        }
        return isReciprocalOf;
    }
    
    @Override
    public final boolean equals(final Object object) {
        boolean equals;
        if (this == object) {
            equals = true;
        }
        else if (!(object instanceof Dimension)) {
            equals = false;
        }
        else {
            final Factor[] thatFactors = ((Dimension)object)._factors;
            if (this._factors.length != thatFactors.length) {
                equals = false;
            }
            else {
                int i = this._factors.length;
                while (--i >= 0 && this._factors[i].equals(thatFactors[i])) {}
                equals = (i < 0);
            }
        }
        return equals;
    }
    
    @Override
    public int hashCode() {
        if (this.hashCode == 0) {
            int hash = 0;
            for (int i = 0; i < this._factors.length; ++i) {
                hash ^= this._factors[i].hashCode();
            }
            this.hashCode = hash;
        }
        return this.hashCode;
    }
    
    public final boolean isDimensionless() {
        int i = this._factors.length;
        while (--i >= 0) {
            if (!this._factors[i].isDimensionless()) {
                return false;
            }
        }
        return true;
    }
    
    @Override
    public String toString() {
        final StringBuffer buf = new StringBuffer(40);
        for (int i = 0; i < this._factors.length; ++i) {
            buf.append(this._factors[i]).append('.');
        }
        if (buf.length() != 0) {
            buf.setLength(buf.length() - 1);
        }
        return buf.toString();
    }
}
