// 
// Decompiled by Procyon v0.5.36
// 

package ucar.units;

public interface UnitSystem
{
    UnitDB getBaseUnitDB();
    
    UnitDB getUnitDB();
    
    BaseUnit getBaseUnit(final BaseQuantity p0);
}
