// 
// Decompiled by Procyon v0.5.36
// 

package ucar.units;

public final class PrefixSymbol extends Prefix
{
    public PrefixSymbol(final String name, final double value) {
        super(name, value);
    }
    
    @Override
    public final int compareTo(final Object obj) {
        final String thatID = ((PrefixSymbol)obj).getID();
        int comp = thatID.length() - this.getID().length();
        if (comp == 0) {
            comp = this.getID().compareTo(thatID);
        }
        return comp;
    }
    
    @Override
    public final int compareTo(final String string) {
        final int comp = string.length() - this.getID().length();
        return (comp < 0) ? comp : ((comp == 0) ? ((this.getID().compareTo(string) == 0) ? 0 : -1) : ((this.getID().compareTo(string.substring(0, this.getID().length())) == 0) ? 0 : -1));
    }
}
