// 
// Decompiled by Procyon v0.5.36
// 

package ucar.units;

public class PrefixDBAccessException extends PrefixDBException
{
    private static final long serialVersionUID = 1L;
    
    public PrefixDBAccessException(final String reason) {
        super("Couldn't access unit-prefix database: " + reason);
    }
}
