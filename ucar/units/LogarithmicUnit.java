// 
// Decompiled by Procyon v0.5.36
// 

package ucar.units;

public final class LogarithmicUnit extends UnitImpl implements DerivableUnit
{
    private static final long serialVersionUID = 1L;
    private final double base;
    private final transient double lnBase;
    private final DerivableUnit reference;
    private final transient DerivedUnit derivedUnit;
    
    public LogarithmicUnit(final Unit reference, final double base) {
        this(reference, base, null);
    }
    
    public LogarithmicUnit(final Unit reference, final double base, final UnitName id) {
        super(id);
        if (reference == null) {
            throw new NullPointerException("Null reference argument");
        }
        if (!(reference instanceof DerivableUnit)) {
            throw new IllegalArgumentException("Not a DerivableUnit: " + reference);
        }
        this.reference = (DerivableUnit)reference;
        if (base != 2.0 && base != 10.0 && base != 2.718281828459045) {
            throw new IllegalArgumentException("Invalid base: " + base);
        }
        this.base = base;
        this.lnBase = ((base == 2.718281828459045) ? 1.0 : Math.log(base));
        this.derivedUnit = reference.getDerivedUnit();
    }
    
    static Unit getInstance(final Unit unit, final double base) {
        return new LogarithmicUnit(unit, base);
    }
    
    public DerivableUnit getReference() {
        return this.reference;
    }
    
    public double getBase() {
        return this.base;
    }
    
    public Unit clone(final UnitName id) {
        return new LogarithmicUnit((Unit)this.reference, this.getBase(), id);
    }
    
    @Override
    protected Unit myMultiplyBy(final Unit that) throws MultiplyException {
        if (!that.isDimensionless()) {
            throw new MultiplyException(that);
        }
        return (that instanceof ScaledUnit) ? new ScaledUnit(((ScaledUnit)that).getScale(), this) : this;
    }
    
    @Override
    protected Unit myDivideBy(final Unit that) throws DivideException {
        if (!that.isDimensionless()) {
            throw new DivideException(that);
        }
        return (that instanceof ScaledUnit) ? new ScaledUnit(1.0 / ((ScaledUnit)that).getScale(), this) : this;
    }
    
    @Override
    protected Unit myDivideInto(final Unit that) throws OperationException {
        throw new DivideException(that);
    }
    
    @Override
    protected Unit myRaiseTo(final int power) throws RaiseException {
        if (power == 0) {
            return DerivedUnitImpl.DIMENSIONLESS;
        }
        if (power == 1) {
            return this;
        }
        throw new RaiseException(this);
    }
    
    public DerivedUnit getDerivedUnit() {
        return this.derivedUnit;
    }
    
    public float toDerivedUnit(final float amount) throws ConversionException {
        return (float)this.toDerivedUnit((double)amount);
    }
    
    public double toDerivedUnit(final double amount) throws ConversionException {
        return this.reference.toDerivedUnit(Math.exp(amount * this.lnBase));
    }
    
    public float[] toDerivedUnit(final float[] input, final float[] output) throws ConversionException {
        int i = input.length;
        while (--i >= 0) {
            output[i] = (float)Math.exp(input[i] * this.lnBase);
        }
        return this.reference.toDerivedUnit(output, output);
    }
    
    public double[] toDerivedUnit(final double[] input, final double[] output) throws ConversionException {
        int i = input.length;
        while (--i >= 0) {
            output[i] = Math.exp(input[i] * this.lnBase);
        }
        return this.reference.toDerivedUnit(output, output);
    }
    
    public float fromDerivedUnit(final float amount) throws ConversionException {
        return (float)this.fromDerivedUnit((double)amount);
    }
    
    public double fromDerivedUnit(final double amount) throws ConversionException {
        return Math.log(this.reference.fromDerivedUnit(amount)) / this.lnBase;
    }
    
    public float[] fromDerivedUnit(final float[] input, final float[] output) throws ConversionException {
        this.reference.fromDerivedUnit(input, output);
        int i = input.length;
        while (--i >= 0) {
            output[i] = (float)(Math.log(output[i]) / this.lnBase);
        }
        return output;
    }
    
    public double[] fromDerivedUnit(final double[] input, final double[] output) throws ConversionException {
        this.reference.fromDerivedUnit(input, output);
        int i = input.length;
        while (--i >= 0) {
            output[i] = (float)(Math.log(output[i]) / this.lnBase);
        }
        return output;
    }
    
    @Override
    public boolean equals(final Object object) {
        if (this == object) {
            return true;
        }
        if (!(object instanceof LogarithmicUnit)) {
            return false;
        }
        final LogarithmicUnit that = (LogarithmicUnit)object;
        return this.base == that.base && this.reference.equals(that.reference);
    }
    
    @Override
    public int hashCode() {
        return Double.valueOf(this.base).hashCode() ^ this.getReference().hashCode();
    }
    
    public boolean isDimensionless() {
        return true;
    }
    
    @Override
    public String toString() {
        final String string = super.toString();
        return (string != null) ? string : this.getCanonicalString();
    }
    
    public String getCanonicalString() {
        return ((this.base == 2.0) ? "lb" : ((this.base == 2.718281828459045) ? "ln" : "lg")) + "(re " + this.getReference().toString() + ")";
    }
    
    public static void main(final String[] args) throws Exception {
        final BaseUnit meter = BaseUnit.getOrCreate(UnitName.newUnitName("meter", null, "m"), BaseQuantity.LENGTH);
        final ScaledUnit micron = new ScaledUnit(1.0E-6, meter);
        final Unit cubicMicron = micron.raiseTo(3);
        final LogarithmicUnit Bz = new LogarithmicUnit(cubicMicron, 10.0);
        assert Bz.isDimensionless();
        assert Bz.equals(Bz);
        assert Bz.getReference().equals(cubicMicron);
        assert Bz.getBase() == 10.0;
        assert !Bz.equals(cubicMicron);
        assert !Bz.equals(micron);
        assert !Bz.equals(meter);
        try {
            Bz.multiplyBy(meter);
            assert false;
        }
        catch (MultiplyException ex) {}
        try {
            Bz.divideBy(meter);
            assert false;
        }
        catch (DivideException ex2) {}
        try {
            Bz.raiseTo(2);
            assert false;
        }
        catch (RaiseException ex3) {}
        double value = Bz.toDerivedUnit(0.0f);
        assert 9.0E-19 < value && value < 1.1E-18 : value;
        value = Bz.toDerivedUnit(1.0f);
        assert 9.0E-18 < value && value < 1.1E-17 : value;
        value = Bz.fromDerivedUnit(1.0E-18);
        assert -0.1 < value && value < 0.1 : value;
        value = Bz.fromDerivedUnit(1.0E-17);
        assert 0.9 < value && value < 1.1 : value;
        final String string = Bz.toString();
        assert string.equals("lg(re 9.999999999999999E-19 m3)") : string;
    }
}
