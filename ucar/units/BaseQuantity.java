// 
// Decompiled by Procyon v0.5.36
// 

package ucar.units;

import java.io.Serializable;

public abstract class BaseQuantity implements Base, Comparable<BaseQuantity>, Serializable
{
    private static final long serialVersionUID = 1L;
    public static final RegularBaseQuantity AMOUNT_OF_SUBSTANCE;
    public static final RegularBaseQuantity ELECTRIC_CURRENT;
    public static final RegularBaseQuantity LENGTH;
    public static final RegularBaseQuantity LUMINOUS_INTENSITY;
    public static final RegularBaseQuantity MASS;
    public static final SupplementaryBaseQuantity PLANE_ANGLE;
    public static final SupplementaryBaseQuantity SOLID_ANGLE;
    public static final RegularBaseQuantity THERMODYNAMIC_TEMPERATURE;
    public static final RegularBaseQuantity TIME;
    public static final UnknownBaseQuantity UNKNOWN;
    private final String name;
    private final String symbol;
    
    public BaseQuantity(final String name, final String symbol) throws NameException {
        this(name, symbol, true);
        if (name == null || name.length() == 0 || (symbol != null && symbol.length() == 0)) {
            throw new NameException("Invalid name or symbol");
        }
    }
    
    protected BaseQuantity(final String name, final String symbol, final boolean trusted) {
        this.name = name;
        this.symbol = symbol;
    }
    
    public String getName() {
        return this.name;
    }
    
    public String getSymbol() {
        return this.symbol;
    }
    
    public final String getID() {
        final String id = this.getSymbol();
        return (id == null) ? this.getName() : id;
    }
    
    @Override
    public final String toString() {
        return this.getID();
    }
    
    @Override
    public boolean equals(final Object object) {
        return this == object || (object instanceof BaseQuantity && (this.getName().equalsIgnoreCase(((BaseQuantity)object).getName()) && (this.getSymbol() == null || this.getSymbol().equals(((BaseQuantity)object).getSymbol()))));
    }
    
    @Override
    public int hashCode() {
        return this.getName().toLowerCase().hashCode() ^ ((this.getSymbol() == null) ? 0 : this.getSymbol().hashCode());
    }
    
    public int compareTo(final BaseQuantity that) {
        int comp;
        if (this == that) {
            comp = 0;
        }
        else {
            comp = this.getName().compareToIgnoreCase(that.getName());
            if (comp == 0 && this.getSymbol() != null) {
                comp = this.getSymbol().compareTo(that.getSymbol());
            }
        }
        return comp;
    }
    
    public abstract boolean isDimensionless();
    
    public static void main(final String[] args) {
        System.out.println("AMOUNT_OF_SUBSTANCE.getName() = " + BaseQuantity.AMOUNT_OF_SUBSTANCE.getName());
        System.out.println("LUMINOUS_INTENSITY.getSymbol() = " + BaseQuantity.LUMINOUS_INTENSITY.getSymbol());
        System.out.println("PLANE_ANGLE.getSymbol() = " + BaseQuantity.PLANE_ANGLE.getSymbol());
        System.out.println("LENGTH.equals(LENGTH) = " + BaseQuantity.LENGTH.equals(BaseQuantity.LENGTH));
        System.out.println("LENGTH.equals(MASS) = " + BaseQuantity.LENGTH.equals(BaseQuantity.MASS));
        System.out.println("LENGTH.equals(PLANE_ANGLE) = " + BaseQuantity.LENGTH.equals(BaseQuantity.PLANE_ANGLE));
        System.out.println("PLANE_ANGLE.equals(PLANE_ANGLE) = " + BaseQuantity.PLANE_ANGLE.equals(BaseQuantity.PLANE_ANGLE));
        System.out.println("PLANE_ANGLE.equals(SOLID_ANGLE) = " + BaseQuantity.PLANE_ANGLE.equals(BaseQuantity.SOLID_ANGLE));
        System.out.println("LENGTH.compareTo(LENGTH) = " + BaseQuantity.LENGTH.compareTo((BaseQuantity)BaseQuantity.LENGTH));
        System.out.println("LENGTH.compareTo(MASS) = " + BaseQuantity.LENGTH.compareTo((BaseQuantity)BaseQuantity.MASS));
        System.out.println("LENGTH.compareTo(PLANE_ANGLE) = " + BaseQuantity.LENGTH.compareTo((BaseQuantity)BaseQuantity.PLANE_ANGLE));
        System.out.println("PLANE_ANGLE.compareTo(PLANE_ANGLE) = " + BaseQuantity.PLANE_ANGLE.compareTo((BaseQuantity)BaseQuantity.PLANE_ANGLE));
        System.out.println("PLANE_ANGLE.compareTo(SOLID_ANGLE) = " + BaseQuantity.PLANE_ANGLE.compareTo((BaseQuantity)BaseQuantity.SOLID_ANGLE));
    }
    
    static {
        AMOUNT_OF_SUBSTANCE = new RegularBaseQuantity("Amount of Substance", "N", true);
        ELECTRIC_CURRENT = new RegularBaseQuantity("Electric Current", "I", true);
        LENGTH = new RegularBaseQuantity("Length", "L", true);
        LUMINOUS_INTENSITY = new RegularBaseQuantity("Luminous Intensity", "J", true);
        MASS = new RegularBaseQuantity("Mass", "M", true);
        PLANE_ANGLE = new SupplementaryBaseQuantity("Plane Angle", null, true);
        SOLID_ANGLE = new SupplementaryBaseQuantity("Solid Angle", null, true);
        THERMODYNAMIC_TEMPERATURE = new RegularBaseQuantity("Thermodynamic Temperature", "T", true);
        TIME = new RegularBaseQuantity("Time", "t", true);
        UNKNOWN = new UnknownBaseQuantity();
    }
}
