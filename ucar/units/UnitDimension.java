// 
// Decompiled by Procyon v0.5.36
// 

package ucar.units;

public final class UnitDimension extends Dimension
{
    public UnitDimension() {
    }
    
    public UnitDimension(final BaseUnit baseUnit) {
        super(new Factor(baseUnit));
    }
    
    private UnitDimension(final Factor[] factors) {
        super(factors);
    }
    
    public UnitDimension multiplyBy(final UnitDimension that) {
        return new UnitDimension(this.mult(that));
    }
    
    public UnitDimension divideBy(final UnitDimension that) {
        return this.multiplyBy(that.raiseTo(-1));
    }
    
    public UnitDimension raiseTo(final int power) {
        return new UnitDimension(this.pow(power));
    }
    
    public QuantityDimension getQuantityDimension() {
        final Factor[] factors = this.getFactors();
        int i = factors.length;
        while (--i >= 0) {
            final Factor factor = factors[i];
            factors[i] = new Factor(((BaseUnit)factor.getBase()).getBaseQuantity(), factor.getExponent());
        }
        return new QuantityDimension(factors);
    }
    
    public static void main(final String[] args) throws Exception {
        System.out.println("new UnitDimension() = \"" + new UnitDimension() + '\"');
        final UnitDimension timeDimension = new UnitDimension(BaseUnit.getOrCreate(UnitName.newUnitName("second", null, "s"), BaseQuantity.TIME));
        System.out.println("timeDimension = \"" + timeDimension + '\"');
        final UnitDimension lengthDimension = new UnitDimension(BaseUnit.getOrCreate(UnitName.newUnitName("meter", null, "m"), BaseQuantity.LENGTH));
        System.out.println("lengthDimension = \"" + lengthDimension + '\"');
        System.out.println("lengthDimension.isReciprocalOf(timeDimension) = \"" + lengthDimension.isReciprocalOf(timeDimension) + '\"');
        final UnitDimension hertzDimension = timeDimension.raiseTo(-1);
        System.out.println("hertzDimension = \"" + hertzDimension + '\"');
        System.out.println("hertzDimension.isReciprocalOf(timeDimension) = \"" + hertzDimension.isReciprocalOf(timeDimension) + '\"');
        System.out.println("lengthDimension.divideBy(timeDimension) = \"" + lengthDimension.divideBy(timeDimension) + '\"');
        System.out.println("lengthDimension.divideBy(timeDimension).raiseTo(2) = \"" + lengthDimension.divideBy(timeDimension).raiseTo(2) + '\"');
    }
}
