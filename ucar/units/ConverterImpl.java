// 
// Decompiled by Procyon v0.5.36
// 

package ucar.units;

public abstract class ConverterImpl implements Converter
{
    protected ConverterImpl(final Unit fromUnit, final Unit toUnit) throws ConversionException {
        if (!fromUnit.isCompatible(toUnit)) {
            throw new ConversionException(fromUnit, toUnit);
        }
    }
    
    public static Converter create(final Unit fromUnit, final Unit toUnit) throws ConversionException {
        return fromUnit.getConverterTo(toUnit);
    }
    
    public final float convert(final float amount) {
        return (float)this.convert((double)amount);
    }
    
    public final float[] convert(final float[] amounts) {
        return this.convert(amounts, new float[amounts.length]);
    }
    
    public final double[] convert(final double[] amounts) {
        return this.convert(amounts, new double[amounts.length]);
    }
}
