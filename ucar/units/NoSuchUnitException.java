// 
// Decompiled by Procyon v0.5.36
// 

package ucar.units;

public class NoSuchUnitException extends SpecificationException
{
    private static final long serialVersionUID = 1L;
    
    public NoSuchUnitException(final UnitID id) {
        this(id.toString());
    }
    
    public NoSuchUnitException(final String id) {
        super("Unit \"" + id + "\" not in database");
    }
}
