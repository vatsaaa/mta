// 
// Decompiled by Procyon v0.5.36
// 

package ucar.units;

public final class DivideException extends OperationException
{
    private static final long serialVersionUID = 1L;
    
    public DivideException(final Unit unit) {
        super("Can't divide unit \"" + unit + "\"");
    }
    
    public DivideException(final Unit numerator, final Unit denominator) {
        super("Can't divide unit \"" + numerator + "\" by unit \"" + denominator + "\"");
    }
}
