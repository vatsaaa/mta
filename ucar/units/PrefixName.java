// 
// Decompiled by Procyon v0.5.36
// 

package ucar.units;

public final class PrefixName extends Prefix
{
    public PrefixName(final String name, final double value) {
        super(name, value);
    }
    
    @Override
    public final int compareTo(final Object obj) {
        return this.getID().compareToIgnoreCase(((PrefixName)obj).getID());
    }
    
    @Override
    public final int compareTo(final String string) {
        return (this.getID().length() >= string.length()) ? this.getID().compareToIgnoreCase(string) : this.getID().compareToIgnoreCase(string.substring(0, this.getID().length()));
    }
}
