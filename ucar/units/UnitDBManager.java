// 
// Decompiled by Procyon v0.5.36
// 

package ucar.units;

import java.io.Serializable;

public final class UnitDBManager implements Serializable
{
    private static final long serialVersionUID = 1L;
    private static UnitDB instance;
    
    public static final UnitDB instance() throws UnitDBException {
        synchronized (UnitDBManager.class) {
            if (UnitDBManager.instance == null) {
                UnitDBManager.instance = StandardUnitDB.instance();
            }
        }
        return UnitDBManager.instance;
    }
    
    public static final synchronized void setInstance(final UnitDB instance) {
        UnitDBManager.instance = instance;
    }
}
