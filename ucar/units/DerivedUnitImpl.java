// 
// Decompiled by Procyon v0.5.36
// 

package ucar.units;

public class DerivedUnitImpl extends UnitImpl implements DerivedUnit, DerivableUnit
{
    private static final long serialVersionUID = 1L;
    public static final DerivedUnitImpl DIMENSIONLESS;
    private UnitDimension dimension;
    
    protected DerivedUnitImpl() {
        this(new UnitDimension(), dimensionlessID());
    }
    
    private static UnitName dimensionlessID() {
        UnitName id;
        try {
            id = UnitName.newUnitName("1", "1", "1");
        }
        catch (NameException e) {
            id = null;
        }
        return id;
    }
    
    protected DerivedUnitImpl(final UnitDimension dimension) {
        this(dimension, null);
    }
    
    protected DerivedUnitImpl(final UnitName id) {
        this(null, id);
    }
    
    protected DerivedUnitImpl(final UnitDimension dimension, final UnitName id) {
        super(id);
        this.dimension = dimension;
    }
    
    protected void setDimension(final UnitDimension dimension) {
        this.dimension = dimension;
    }
    
    public final UnitDimension getDimension() {
        return this.dimension;
    }
    
    public final QuantityDimension getQuantityDimension() {
        return this.getDimension().getQuantityDimension();
    }
    
    public final boolean isReciprocalOf(final DerivedUnit that) {
        return this.dimension.isReciprocalOf(that.getDimension());
    }
    
    public final DerivedUnit getDerivedUnit() {
        return this;
    }
    
    public final Unit clone(final UnitName id) {
        return new DerivedUnitImpl(this.dimension, id);
    }
    
    @Override
    protected Unit myMultiplyBy(final Unit that) throws MultiplyException {
        Unit result;
        if (this.dimension.getRank() == 0) {
            result = that;
        }
        else if (!(that instanceof DerivedUnit)) {
            result = that.multiplyBy(this);
        }
        else {
            final UnitDimension thatDimension = ((DerivedUnit)that).getDimension();
            result = ((thatDimension.getRank() == 0) ? this : new DerivedUnitImpl(this.dimension.multiplyBy(thatDimension)));
        }
        return result;
    }
    
    @Override
    protected Unit myDivideBy(final Unit that) throws OperationException {
        Unit result;
        if (this.dimension.getRank() == 0) {
            result = that.raiseTo(-1);
        }
        else if (!(that instanceof DerivedUnit)) {
            result = that.divideInto(this);
        }
        else {
            final UnitDimension thatDimension = ((DerivedUnit)that).getDimension();
            result = ((thatDimension.getRank() == 0) ? this : new DerivedUnitImpl(this.dimension.divideBy(thatDimension)));
        }
        return result;
    }
    
    @Override
    protected Unit myDivideInto(final Unit that) throws OperationException {
        return that.divideBy(this);
    }
    
    @Override
    protected Unit myRaiseTo(final int power) {
        return (power == 1) ? this : new DerivedUnitImpl(this.dimension.raiseTo(power));
    }
    
    public final float toDerivedUnit(final float amount) {
        return amount;
    }
    
    public final double toDerivedUnit(final double amount) {
        return amount;
    }
    
    public final float[] toDerivedUnit(final float[] input, final float[] output) {
        if (input != output) {
            System.arraycopy(input, 0, output, 0, input.length);
        }
        return output;
    }
    
    public final double[] toDerivedUnit(final double[] input, final double[] output) {
        if (input != output) {
            System.arraycopy(input, 0, output, 0, input.length);
        }
        return output;
    }
    
    public final float fromDerivedUnit(final float amount) {
        return amount;
    }
    
    public final double fromDerivedUnit(final double amount) {
        return amount;
    }
    
    public final float[] fromDerivedUnit(final float[] input, final float[] output) {
        return this.toDerivedUnit(input, output);
    }
    
    public final double[] fromDerivedUnit(final double[] input, final double[] output) {
        return this.toDerivedUnit(input, output);
    }
    
    @Override
    public final boolean isCompatible(final Unit that) {
        final DerivedUnit unit = that.getDerivedUnit();
        return this.equals(unit) || this.isReciprocalOf(unit);
    }
    
    @Override
    public boolean equals(final Object object) {
        if (this == object) {
            return true;
        }
        if (!(object instanceof DerivedUnit)) {
            return false;
        }
        final DerivedUnit that = (DerivedUnit)object;
        return (!(this instanceof BaseUnit) || !(that instanceof BaseUnit)) && this.dimension.equals(that.getDimension());
    }
    
    @Override
    public int hashCode() {
        return (this instanceof BaseUnit) ? System.identityHashCode(this) : this.dimension.hashCode();
    }
    
    public boolean isDimensionless() {
        return this.dimension.isDimensionless();
    }
    
    @Override
    public String toString() {
        final String string = super.toString();
        return (string != null) ? string : this.getCanonicalString();
    }
    
    public String getCanonicalString() {
        return this.dimension.toString();
    }
    
    public static void main(final String[] args) throws Exception {
        final BaseUnit second = BaseUnit.getOrCreate(UnitName.newUnitName("second", null, "s"), BaseQuantity.TIME);
        System.out.println("second = \"" + second + '\"');
        final BaseUnit meter = BaseUnit.getOrCreate(UnitName.newUnitName("meter", null, "m"), BaseQuantity.LENGTH);
        System.out.println("meter = \"" + meter + '\"');
        final DerivedUnitImpl meterSecond = (DerivedUnitImpl)meter.myMultiplyBy(second);
        System.out.println("meterSecond = \"" + meterSecond + '\"');
        final DerivedUnitImpl meterPerSecond = (DerivedUnitImpl)meter.myDivideBy(second);
        System.out.println("meterPerSecond = \"" + meterPerSecond + '\"');
        final DerivedUnitImpl secondPerMeter = (DerivedUnitImpl)second.myDivideBy(meter);
        System.out.println("secondPerMeter = \"" + secondPerMeter + '\"');
        System.out.println("meterPerSecond.isReciprocalOf(secondPerMeter)=" + meterPerSecond.isReciprocalOf(secondPerMeter));
        System.out.println("meter.toDerivedUnit(1.0)=" + meter.toDerivedUnit(1.0));
        System.out.println("meter.toDerivedUnit(new double[] {1,2,3}, new double[3])[1]=" + meter.toDerivedUnit(new double[] { 1.0, 2.0, 3.0 }, new double[3])[1]);
        System.out.println("meter.fromDerivedUnit(1.0)=" + meter.fromDerivedUnit(1.0));
        System.out.println("meter.fromDerivedUnit(new double[] {1,2,3}, new double[3])[2]=" + meter.fromDerivedUnit(new double[] { 1.0, 2.0, 3.0 }, new double[3])[2]);
        System.out.println("meter.isCompatible(meter)=" + meter.isCompatible(meter));
        System.out.println("meter.isCompatible(second)=" + meter.isCompatible(second));
        System.out.println("meter.equals(meter)=" + meter.equals(meter));
        System.out.println("meter.equals(second)=" + meter.equals(second));
        System.out.println("meter.isDimensionless()=" + meter.isDimensionless());
        final Unit sPerS = second.myDivideBy(second);
        System.out.println("sPerS = \"" + sPerS + '\"');
        System.out.println("sPerS.isDimensionless()=" + sPerS.isDimensionless());
        meterPerSecond.raiseTo(2);
        meter.myDivideBy(meterPerSecond);
    }
    
    static {
        DIMENSIONLESS = new DerivedUnitImpl();
    }
}
