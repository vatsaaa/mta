// 
// Decompiled by Procyon v0.5.36
// 

package ucar.units;

public interface UnitFormat
{
    Unit parse(final String p0) throws NoSuchUnitException, UnitParseException, SpecificationException, UnitDBException, PrefixDBException, UnitSystemException;
    
    Unit parse(final String p0, final UnitDB p1) throws NoSuchUnitException, UnitParseException, SpecificationException, UnitDBException, PrefixDBException, UnitSystemException;
    
    String format(final Factor p0);
    
    StringBuffer format(final Factor p0, final StringBuffer p1);
    
    String format(final Unit p0) throws UnitClassException;
    
    String longFormat(final Unit p0) throws UnitClassException;
    
    StringBuffer format(final Unit p0, final StringBuffer p1) throws UnitClassException;
    
    StringBuffer longFormat(final Unit p0, final StringBuffer p1) throws UnitClassException;
}
