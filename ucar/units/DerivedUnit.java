// 
// Decompiled by Procyon v0.5.36
// 

package ucar.units;

public interface DerivedUnit extends Unit
{
    boolean isReciprocalOf(final DerivedUnit p0);
    
    UnitDimension getDimension();
    
    QuantityDimension getQuantityDimension();
}
