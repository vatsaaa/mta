// 
// Decompiled by Procyon v0.5.36
// 

package ucar.units;

public class SpecificationException extends UnitException
{
    private static final long serialVersionUID = 1L;
    
    public SpecificationException(final String reason) {
        super("Specification error: " + reason);
    }
    
    public SpecificationException(final String spec, final String reason) {
        super("Specification error in \"" + spec + "\": " + reason);
    }
    
    public SpecificationException(final String message, final Throwable e) {
        super(message);
        this.initCause(e);
    }
}
