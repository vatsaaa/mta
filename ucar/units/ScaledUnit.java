// 
// Decompiled by Procyon v0.5.36
// 

package ucar.units;

public final class ScaledUnit extends UnitImpl implements DerivableUnit
{
    private static final long serialVersionUID = 1L;
    private final double _scale;
    private final Unit _unit;
    
    public ScaledUnit(final double scale) {
        this(scale, DerivedUnitImpl.DIMENSIONLESS);
    }
    
    public ScaledUnit(final double scale, final Unit unit) {
        this(scale, unit, null);
    }
    
    public ScaledUnit(final double scale, final Unit unit, final UnitName id) {
        super(id);
        if (!(unit instanceof ScaledUnit)) {
            this._unit = unit;
            this._scale = scale;
        }
        else {
            this._unit = ((ScaledUnit)unit)._unit;
            this._scale = ((ScaledUnit)unit)._scale * scale;
        }
    }
    
    static Unit getInstance(final double scale, final Unit unit) throws MultiplyException {
        if (scale == 0.0) {
            throw new MultiplyException(scale, unit);
        }
        return (scale == 1.0) ? unit : new ScaledUnit(scale, unit);
    }
    
    public double getScale() {
        return this._scale;
    }
    
    public Unit getUnit() {
        return this._unit;
    }
    
    public Unit clone(final UnitName id) {
        return new ScaledUnit(this._scale, this.getUnit(), id);
    }
    
    @Override
    public Unit multiplyBy(final double scale) throws MultiplyException {
        return getInstance(scale * this._scale, this._unit);
    }
    
    @Override
    protected Unit myMultiplyBy(final Unit that) throws MultiplyException {
        return (that instanceof ScaledUnit) ? new ScaledUnit(this.getScale() * ((ScaledUnit)that).getScale(), this.getUnit().multiplyBy(((ScaledUnit)that).getUnit())) : new ScaledUnit(this.getScale(), this.getUnit().multiplyBy(that));
    }
    
    @Override
    protected Unit myDivideBy(final Unit that) throws OperationException {
        return (that instanceof ScaledUnit) ? new ScaledUnit(this.getScale() / ((ScaledUnit)that).getScale(), this.getUnit().divideBy(((ScaledUnit)that).getUnit())) : new ScaledUnit(this.getScale(), this.getUnit().divideBy(that));
    }
    
    @Override
    protected Unit myDivideInto(final Unit that) throws OperationException {
        return (that instanceof ScaledUnit) ? new ScaledUnit(((ScaledUnit)that).getScale() / this.getScale(), this.getUnit().divideInto(((ScaledUnit)that).getUnit())) : new ScaledUnit(1.0 / this.getScale(), this.getUnit().divideInto(that));
    }
    
    @Override
    protected Unit myRaiseTo(final int power) throws RaiseException {
        return new ScaledUnit(Math.pow(this.getScale(), power), this.getUnit().raiseTo(power));
    }
    
    public DerivedUnit getDerivedUnit() {
        return this.getUnit().getDerivedUnit();
    }
    
    public float toDerivedUnit(final float amount) throws ConversionException {
        return (float)this.toDerivedUnit((double)amount);
    }
    
    public double toDerivedUnit(final double amount) throws ConversionException {
        if (!(this._unit instanceof DerivableUnit)) {
            throw new ConversionException(this, this.getDerivedUnit());
        }
        return ((DerivableUnit)this._unit).toDerivedUnit(amount * this.getScale());
    }
    
    public float[] toDerivedUnit(final float[] input, final float[] output) throws ConversionException {
        final float scale = (float)this.getScale();
        int i = input.length;
        while (--i >= 0) {
            output[i] = input[i] * scale;
        }
        if (!(this._unit instanceof DerivableUnit)) {
            throw new ConversionException(this, this.getDerivedUnit());
        }
        return ((DerivableUnit)this.getUnit()).toDerivedUnit(output, output);
    }
    
    public double[] toDerivedUnit(final double[] input, final double[] output) throws ConversionException {
        final double scale = this.getScale();
        int i = input.length;
        while (--i >= 0) {
            output[i] = input[i] * scale;
        }
        if (!(this._unit instanceof DerivableUnit)) {
            throw new ConversionException(this, this.getDerivedUnit());
        }
        return ((DerivableUnit)this.getUnit()).toDerivedUnit(output, output);
    }
    
    public float fromDerivedUnit(final float amount) throws ConversionException {
        return (float)this.fromDerivedUnit((double)amount);
    }
    
    public double fromDerivedUnit(final double amount) throws ConversionException {
        if (!(this._unit instanceof DerivableUnit)) {
            throw new ConversionException(this.getDerivedUnit(), this);
        }
        return ((DerivableUnit)this.getUnit()).fromDerivedUnit(amount) / this.getScale();
    }
    
    public float[] fromDerivedUnit(final float[] input, final float[] output) throws ConversionException {
        if (!(this._unit instanceof DerivableUnit)) {
            throw new ConversionException(this.getDerivedUnit(), this);
        }
        ((DerivableUnit)this.getUnit()).fromDerivedUnit(input, output);
        final float scale = (float)this.getScale();
        int i = input.length;
        while (--i >= 0) {
            final int n = i;
            output[n] /= scale;
        }
        return output;
    }
    
    public double[] fromDerivedUnit(final double[] input, final double[] output) throws ConversionException {
        if (!(this._unit instanceof DerivableUnit)) {
            throw new ConversionException(this.getDerivedUnit(), this);
        }
        ((DerivableUnit)this.getUnit()).fromDerivedUnit(input, output);
        final double scale = this.getScale();
        int i = input.length;
        while (--i >= 0) {
            final int n = i;
            output[n] /= scale;
        }
        return output;
    }
    
    @Override
    public boolean equals(final Object object) {
        if (this == object) {
            return true;
        }
        if (this._scale == 1.0) {
            return object.equals(this._unit);
        }
        if (!(object instanceof ScaledUnit)) {
            return false;
        }
        final ScaledUnit that = (ScaledUnit)object;
        return this._scale == that._scale && this._unit.equals(that._unit);
    }
    
    @Override
    public int hashCode() {
        return ((this.getScale() == 1.0) ? 0 : Double.valueOf(this.getScale()).hashCode()) ^ this.getUnit().hashCode();
    }
    
    public boolean isDimensionless() {
        return this.getUnit().isDimensionless();
    }
    
    @Override
    public String toString() {
        final String string = super.toString();
        return (string != null) ? string : this.getCanonicalString();
    }
    
    public String getCanonicalString() {
        return DerivedUnitImpl.DIMENSIONLESS.equals(this._unit) ? Double.toString(this.getScale()) : (Double.toString(this.getScale()) + " " + this._unit.toString());
    }
    
    public static void main(final String[] args) throws Exception {
        final BaseUnit meter = BaseUnit.getOrCreate(UnitName.newUnitName("meter", null, "m"), BaseQuantity.LENGTH);
        final ScaledUnit nauticalMile = new ScaledUnit(1852.0, meter);
        System.out.println("nauticalMile.getUnit().equals(meter)=" + nauticalMile.getUnit().equals(meter));
        final ScaledUnit nauticalMileMeter = (ScaledUnit)nauticalMile.multiplyBy(meter);
        System.out.println("nauticalMileMeter.divideBy(nauticalMile)=" + nauticalMileMeter.divideBy(nauticalMile));
        System.out.println("meter.divideBy(nauticalMile)=" + meter.divideBy(nauticalMile));
        System.out.println("nauticalMile.raiseTo(2)=" + nauticalMile.raiseTo(2));
        System.out.println("nauticalMile.toDerivedUnit(1.)=" + nauticalMile.toDerivedUnit(1.0));
        System.out.println("nauticalMile.toDerivedUnit(new float[]{1,2,3}, new float[3])[1]=" + nauticalMile.toDerivedUnit(new float[] { 1.0f, 2.0f, 3.0f }, new float[3])[1]);
        System.out.println("nauticalMile.fromDerivedUnit(1852.)=" + nauticalMile.fromDerivedUnit(1852.0));
        System.out.println("nauticalMile.fromDerivedUnit(new float[]{1852},new float[1])[0]=" + nauticalMile.fromDerivedUnit(new float[] { 1852.0f }, new float[1])[0]);
        System.out.println("nauticalMile.equals(nauticalMile)=" + nauticalMile.equals(nauticalMile));
        final ScaledUnit nautical2Mile = new ScaledUnit(2.0, nauticalMile);
        System.out.println("nauticalMile.equals(nautical2Mile)=" + nauticalMile.equals(nautical2Mile));
        System.out.println("nauticalMile.isDimensionless()=" + nauticalMile.isDimensionless());
        final BaseUnit radian = BaseUnit.getOrCreate(UnitName.newUnitName("radian", null, "rad"), BaseQuantity.PLANE_ANGLE);
        final ScaledUnit degree = new ScaledUnit(0.017453277777777776, radian);
        System.out.println("degree.isDimensionless()=" + degree.isDimensionless());
    }
}
