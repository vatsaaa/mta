// 
// Decompiled by Procyon v0.5.36
// 

package ucar.units;

public abstract class OperationException extends UnitException
{
    private static final long serialVersionUID = 1L;
    
    public OperationException() {
    }
    
    protected OperationException(final String message) {
        super(message);
    }
    
    protected OperationException(final String message, final Exception e) {
        super(message, e);
    }
}
