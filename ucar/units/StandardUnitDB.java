// 
// Decompiled by Procyon v0.5.36
// 

package ucar.units;

public final class StandardUnitDB extends UnitDBImpl
{
    private static final long serialVersionUID = 1L;
    private static StandardUnitDB instance;
    private static UnitFormat format;
    
    private StandardUnitDB() throws UnitParseException, SpecificationException, UnitDBException, PrefixDBException, NameException, OperationException, UnitSystemException {
        super(50, 50);
        this.add((UnitDBImpl)SI.instance().getUnitDB());
        StandardUnitDB.format = StandardUnitFormat.instance();
        this.au("PI", Double.toString(3.141592653589793));
        this.au("percent", "0.01", "%");
        this.au("bakersdozen", "13");
        this.au("pair", "2");
        this.au("dozen", "12");
        this.au("ten", "10");
        this.au("score", "20");
        this.au("hundred", "100");
        this.au("thousand", "1000");
        this.au("million", "1e6");
        this.addUnit(DerivedUnitImpl.DIMENSIONLESS);
        this.au("ppt", "1e-3");
        this.au("pptv", "1e-3");
        this.au("pptm", "1e-3");
        this.au("pptn", "1e-3");
        this.au("ppm", "1e-6");
        this.au("ppmv", "1e-6");
        this.au("ppmm", "1e-6");
        this.au("ppmn", "1e-6");
        this.au("ppb", "1e-9");
        this.au("ppbv", "1e-9");
        this.au("ppbm", "1e-9");
        this.au("ppbn", "1e-9");
        this.au("abampere", "10 ampere");
        this.au("gilbert", "7.957747e-1 ampere");
        this.au("statampere", "3.335640e-10 ampere");
        this.aa("amp", "ampere");
        this.aa("biot", "abampere");
        this.aa("candle", "candela");
        this.au("rankine", "K/1.8");
        this.au("fahrenheit", "rankine @ 459.67");
        this.aa("degree Kelvin", "kelvin", "degrees Kelvin");
        this.aa("degreeK", "kelvin", "degreesK");
        this.aa("degree K", "kelvin", "degrees K");
        this.aa("Celsius", "degree_Celsius");
        this.aa("degreeC", "degree_Celsius", "degreesC");
        this.aa("degree C", "degree_Celsius", "degrees C");
        this.aa("degree centigrade", "degree_Celsius", "degrees centigrade");
        this.aa("centigrade", "degree_Celsius");
        this.aa("degree Rankine", "rankine", "degrees Rankine");
        this.aa("degreeR", "rankine", "degreesR");
        this.aa("degree R", "rankine", "degrees R");
        this.aa("degree Fahrenheit", "fahrenheit", "degrees Fahrenheit");
        this.aa("degreeF", "fahrenheit", "degreesF");
        this.aa("degree F", "fahrenheit", "degrees F");
        this.as("degK", "kelvin");
        this.as("deg K", "kelvin");
        this.as("degC", "degree_Celsius");
        this.as("deg C", "degree_Celsius");
        this.as("degR", "rankine");
        this.as("deg R", "rankine");
        this.as("degF", "fahrenheit");
        this.as("deg F", "fahrenheit");
        this.au("assay ton", "2.916667e-2 kg");
        this.au("avoirdupois ounce", "2.834952e-2 kg");
        this.au("avoirdupois pound", "4.5359237e-1 kg", "lb");
        this.au("carat", "2e-4 kg");
        this.au("grain", "6.479891e-5 kg", "gr");
        this.au("gram", "1e-3 kg", "g");
        this.au("long hundredweight", "5.080235e1 kg");
        this.au("pennyweight", "1.555174e-3 kg");
        this.au("short hundredweight", "4.535924e1 kg");
        this.au("slug", "14.59390 kg");
        this.au("troy ounce", "3.110348e-2 kg");
        this.au("troy pound", "3.732417e-1 kg");
        this.au("unified atomic mass unit", "1.66054e-27 kg");
        this.au("scruple", "20 gr");
        this.au("apdram", "60 gr");
        this.au("apounce", "480 gr");
        this.au("appound", "5760 gr");
        this.aa("atomic mass unit", "unified_atomic_mass_unit");
        this.aa("amu", "unified_atomic_mass_unit");
        this.aa("metricton", "metric_ton");
        this.aa("apothecary ounce", "troy_ounce");
        this.aa("apothecary pound", "troy_pound");
        this.aa("pound", "avoirdupois_pound");
        this.aa("atomicmassunit", "unified_atomic_mass_unit");
        this.au("bag", "94 lb");
        this.au("short ton", "2000 lb");
        this.au("long ton", "2240 lb");
        this.aa("ton", "short_ton");
        this.aa("shortton", "short_ton");
        this.aa("longton", "long_ton");
        this.au("astronomical unit", "1.495979e11 m", "AU");
        this.au("fermi", "femtometer");
        this.au("light year", "9.46073e15 m");
        this.au("micron", "micrometer");
        this.au("mil", "2.54e-5 m");
        this.au("parsec", "3.085678e16 m", "prs");
        this.au("printers point", "3.514598e-4 m");
        this.au("US survey foot", "1200/3937 m", null, "US survey feet");
        this.au("US survey yard", "3 US_survey_feet");
        this.au("US survey mile", "5280 US_survey_feet");
        this.aa("US statute mile", "US_survey_mile");
        this.au("rod", "16.5 US_survey_feet");
        this.au("perch", "5.02921005842012 m");
        this.au("furlong", "660 US_survey_feet");
        this.au("fathom", "6 US_survey_feet");
        this.aa("pole", "rod");
        this.au("international inch", ".0254 m", null, "international inches");
        this.au("international foot", "12 international_inches", null, "international feet");
        this.au("international yard", "3 international_feet");
        this.au("international mile", "5280 international_feet");
        this.aa("inch", "international_inch", null, "in");
        this.aa("foot", "international_foot", "feet", "ft");
        this.aa("yard", "international_yard", null, "yd");
        this.aa("mile", "international_mile");
        this.au("chain", "2.011684e1 m");
        this.au("printers pica", "12 printers_point");
        this.aa("astronomicalunit", "astronomical_unit");
        this.aa("asu", "astronomical_unit");
        this.aa("nmile", "nautical_mile");
        this.aa("pica", "printers_pica");
        this.au("big point", "in/72");
        this.au("barleycorn", "in/3");
        this.au("arpentlin", "191.835 ft");
        this.aa("sec", "second");
        this.au("shake", "1e-8 s");
        this.au("sidereal day", "8.616409e4 s");
        this.au("sidereal hour", "3.590170e3 s");
        this.au("sidereal minute", "5.983617e1 s");
        this.au("sidereal second", "0.9972696 s");
        this.au("sidereal year", "3.155815e7 s");
        this.au("tropical year", "3.15569259747e7 s");
        this.au("lunar month", "29.530589 d");
        this.au("common year", "365 d");
        this.au("leap year", "366 d");
        this.au("Julian year", "365.25 d");
        this.au("Gregorian year", "365.2425 d");
        this.au("sidereal month", "27.321661 d");
        this.au("tropical month", "27.321582 d");
        this.as("hr", "hour");
        this.au("fortnight", "14 d");
        this.au("week", "7 d");
        this.au("jiffy", "cs");
        this.aa("year", "tropical_year", null, "yr");
        this.au("eon", "Gyr");
        this.au("month", "yr/12");
        this.aa("anno", "year", null, "ann");
        this.au("circle", "360 deg");
        this.au("grade", "0.9 deg");
        this.aa("turn", "circle");
        this.aa("revolution", "circle", null, "r");
        this.aa("degree", "arc_degree");
        this.aa("degree north", "arc_degree", "degrees north");
        this.aa("degree east", "arc_degree", "degrees east");
        this.aa("degree true", "arc_degree", "degrees true");
        this.aa("angular degree", "arc_degree");
        this.aa("angular minute", "arc_minute");
        this.aa("angular second", "arc_second");
        this.aa("gon", "grade");
        this.aa("arcdegree", "arc_degree");
        this.aa("arcminute", "arc_minute");
        this.aa("arcsecond", "arc_second");
        this.aa("arcdeg", "arc_degree");
        this.aa("arcmin", "arc_minute");
        this.as("mnt", "arc_minute");
        this.aa("arcsec", "arc_second");
        this.aa("degree N", "degree_north", "degrees N");
        this.aa("degreeE", "degree_east", "degreesE");
        this.aa("degree E", "degree_east", "degrees E");
        this.au("degree west", "-1 degree_east", null, "degrees west");
        this.aa("degreeW", "degree_west", "degreesW");
        this.aa("degree W", "degree_west", "degrees W");
        this.aa("degreeT", "degree_true", "degreesT");
        this.aa("degree T", "degree_true", "degrees T");
        this.au("sphere", "4 PI sr");
        this.au("standard free fall", "9.806650 m/s2");
        this.aa("force", "standard_free_fall");
        this.aa("gravity", "standard_free_fall");
        this.aa("free fall", "standard_free_fall");
        this.au("mercury 0C", "13595.1 gravity kg/m3");
        this.au("mercury 60F", "13556.8 gravity kg/m3");
        this.au("conventional water", "1000 gravity kg/m3");
        this.au("water 4C", "999.972 gravity kg/m3");
        this.au("water 60F", "999.001 gravity kg/m3");
        this.aa("conventional mercury", "mercury_0C");
        this.aa("mercury0C", "mercury_0C");
        this.aa("mercury 32F", "mercury_0C");
        this.aa("water4C", "water_4C");
        this.aa("water 39F", "water_4C");
        this.aa("mercury", "conventional_mercury");
        this.aa("water", "conventional_water");
        this.as("Hg", "mercury");
        this.as("H2O", "water");
        this.au("circular mil", "5.067075e-10 m2");
        this.au("darcy", "9.869233e-13 m2");
        this.aa("ha", "hectare");
        this.au("acre", "160 rod2");
        this.au("abfarad", "GF");
        this.au("abhenry", "nH");
        this.au("abmho", "GS");
        this.au("abohm", "nOhm");
        this.au("megohm", "MOhm");
        this.au("kilohm", "kOhm");
        this.au("abvolt", "1e-8 V");
        this.au("e", "1.60217733e-19 C");
        this.au("chemical faraday", "9.64957e4 C");
        this.au("physical faraday", "9.65219e4 C");
        this.au("C12 faraday", "9.648531e4 C");
        this.au("gamma", "nT");
        this.au("gauss", "1e-4 T");
        this.au("maxwell", "1e-8 Wb");
        this.au("oersted", "7.957747e1 A/m", "Oe");
        this.au("statcoulomb", "3.335640e-10 C");
        this.au("statfarad", "1.112650e-12 F");
        this.au("stathenry", "8.987554e11 H");
        this.au("statmho", "1.112650e-12 S");
        this.au("statohm", "8.987554e11 Ohm");
        this.au("statvolt", "2.997925e2 V");
        this.au("unit pole", "1.256637e-7 Wb");
        this.au("mho", "S");
        this.aa("faraday", "C12 faraday");
        this.au("electronvolt", "1.602177e-19 J");
        this.au("erg", "1e-7 J");
        this.au("IT Btu", "1.05505585262e3 J");
        this.au("EC therm", "1.05506e8 J");
        this.au("thermochemical calorie", "4.184000 J");
        this.au("IT calorie", "4.1868 J");
        this.au("ton TNT", "4.184e9 J");
        this.au("US therm", "1.054804e8 J");
        this.au("watthour", "W.h");
        this.aa("therm", "US_therm");
        this.as("Wh", "watthour");
        this.aa("Btu", "IT_Btu");
        this.aa("calorie", "IT_calorie");
        this.aa("electron volt", "electronvolt");
        this.au("thm", "therm");
        this.au("cal", "calorie");
        this.as("eV", "electronvolt");
        this.au("bev", "gigaelectronvolt");
        this.au("dyne", "1e-5 N");
        this.au("pond", "9.806650e-3 N");
        this.au("force kilogram", "9.806650 N", "kgf");
        this.au("force ounce", "2.780139e-1 N", "ozf");
        this.au("force pound", "4.4482216152605 N", "lbf");
        this.au("poundal", "1.382550e-1 N");
        this.au("gf", "gram force");
        this.au("force gram", "milliforce_kilogram");
        this.au("force ton", "2000 force_pound");
        this.aa("ounce force", "force_ounce");
        this.aa("kilogram force", "force_kilogram");
        this.aa("pound force", "force_pound");
        this.au("kip", "kilolbf");
        this.aa("ton force", "force_ton");
        this.au("gram force", "force_gram");
        this.au("clo", "1.55e-1 K.m2.W-1");
        this.au("footcandle", "1.076391e-1 lx");
        this.au("footlambert", "3.426259 cd/m2");
        this.au("lambert", "1e4/PI cd/m2");
        this.au("stilb", "1e4 cd/m2", "sb");
        this.au("phot", "1e4 lm/m2", "ph");
        this.au("nit", "cd.m2", "nt");
        this.au("langley", "4.184000e4 J/m2");
        this.au("blondel", "1/PI cd/m2");
        this.aa("apostilb", "blondel");
        this.au("denier", "1.111111e-7 kg/m");
        this.au("tex", "1e-6 kg/m");
        this.au("perm 0C", "5.72135e-11 kg/(Pa.s.m2)");
        this.au("perm 23C", "5.74525e-11 kg/(Pa.s.m2)");
        this.au("voltampere", "V.A", "VA");
        this.au("boiler horsepower", "9.80950e3 W");
        this.au("shaft horsepower", "7.456999e2 W");
        this.au("metric horsepower", "7.35499 W");
        this.au("electric horsepower", "7.460000e2 W");
        this.au("water horsepower", "7.46043e2 W");
        this.au("UK horsepower", "7.4570e2 W");
        this.au("refrigeration ton", "12000 Btu/h");
        this.aa("horsepower", "shaft_horsepower", null, "hp");
        this.aa("ton of refrigeration", "refrigeration_ton");
        this.au("standard atmosphere", "1.01325e5 Pa", "atm");
        this.au("technical atmosphere", "kg.(0.01 gravity/m)2", "at");
        this.au("inch H2O 39F", "inch.water_39F");
        this.au("inch H2O 60F", "inch.water_60F");
        this.au("inch Hg 32F", "inch.mercury_32F");
        this.au("inch Hg 60F", "inch.mercury_60F");
        this.au("millimeter Hg 0C", "mm.mercury_0C");
        this.au("footH2O", "foot.water");
        this.au("cmHg", "cm.Hg");
        this.au("cmH2O", "cm.water");
        this.as("pal", "pascal");
        this.au("inch Hg", "inch.Hg");
        this.aa("inch hg", "inch_Hg");
        this.aa("inHg", "inch_Hg");
        this.aa("in Hg", "inch_Hg");
        this.au("millimeter Hg", "mm.Hg");
        this.aa("mmHg", "millimeter_Hg");
        this.aa("mm Hg", "millimeter_Hg");
        this.aa("torr", "millimeter_Hg");
        this.au("foot H2O", "foot.water");
        this.aa("ftH2O", "foot_H2O");
        this.au("psi", "pound.gravity/inch2");
        this.au("ksi", "kip/inch2");
        this.au("barie", "0.1 N/m2");
        this.aa("atmosphere", "standard_atmosphere");
        this.aa("barye", "barie");
        this.aa("sie", "sievert");
        this.aa("knot international", "knot");
        this.aa("international knot", "knot");
        this.au("poise", "1e-1 Pa.s", "P");
        this.au("stokes", "1e-4 m2/s", "St");
        this.au("rhe", "10 (Pa.s)-1");
        this.au("acre foot", "1.233489e3 m3");
        this.au("board foot", "2.359737e-3 m3");
        this.au("bushel", "3.523907e-2 m3", "bu");
        this.au("UK liquid gallon", "4.546090e-3 m3");
        this.au("Canadian liquid gallon", "4.546090e-3 m3");
        this.au("US dry gallon", "4.404884e-3 m3");
        this.au("US liquid gallon", "3.785412e-3 m3");
        this.au("cc", "1e-6 m3");
        this.au("stere", "m3");
        this.au("register ton", "2.831685 m3");
        this.au("US dry quart", "1/4 US_dry_gallon");
        this.au("US dry pint", "1/8 US_dry_gallon");
        this.au("US liquid quart", "1/4 US_liquid_gallon");
        this.au("US liquid pint", "1/8 US_liquid_gallon");
        this.au("US liquid cup", "1/16 US_liquid_gallon");
        this.au("US liquid gill", "1/32 US_liquid_gallon");
        this.au("US fluid ounce", "1/128 US_liquid_gallon");
        this.aa("US liquid ounce", "US_fluid_ounce");
        this.au("UK liquid quart", "1/4 UK_liquid_gallon");
        this.au("UK liquid pint", "1/8 UK_liquid_gallon");
        this.au("UK liquid cup", "1/16 UK_liquid_gallon");
        this.au("UK liquid gill", "1/32 UK_liquid_gallon");
        this.au("UK fluid ounce", "1/160 UK_liquid_gallon");
        this.aa("UK liquid ounce", "UK_fluid_ounce");
        this.aa("liquid gallon", "US_liquid_gallon");
        this.aa("fluid ounce", "US_fluid_ounce", null, "fl_oz");
        this.aa("dry quart", "US_dry_quart");
        this.aa("dry pint", "US_dry_pint");
        this.au("liquid quart", "1/4 liquid_gallon");
        this.au("liquid pint", "1/8 liquid_gallon");
        this.aa("gallon", "liquid_gallon");
        this.au("barrel", "42 US_liquid_gallon", "bbl");
        this.aa("quart", "liquid_quart");
        this.aa("pint", "liquid_pint", null, "pt");
        this.au("cup", "1/16 liquid_gallon");
        this.au("gill", "1/32 liquid_gallon");
        this.au("tablespoon", "0.5 US_fluid_ounce", "Tbl");
        this.au("teaspoon", "1/3 tablespoon", "tsp");
        this.au("peck", "1/4 bushel", "pk");
        this.as("floz", "fluid_ounce");
        this.aa("acre_feet", "acre_foot");
        this.aa("board_feet", "board_foot");
        this.aa("tbsp", "tablespoon");
        this.aa("tblsp", "tablespoon");
        this.au("fldr", "1/8 floz");
        this.au("dram", "1/16 floz", "dr");
        this.au("firkin", "1/4 barrel");
        this.au("sverdrup", "1e6 m3/s");
        this.au("bit", "1");
        this.au("baud", "s-1", "Bd");
        this.au("bps", "s-1");
        this.au("cps", "s-1");
        this.au("count", "1");
        this.au("kayser", "100 m-1");
        this.au("rps", "r/s");
        this.au("rpm", "1/60 rps");
        this.au("work_year", "2056 h");
        this.au("work_month", "1/12 work_year");
        this.aa("geopotential", "gravity", null, "gp");
        this.aa("dynamic", "geopotential");
        StandardUnitDB.format = null;
    }
    
    public static StandardUnitDB instance() throws UnitDBException {
        synchronized (StandardUnitDB.class) {
            if (StandardUnitDB.instance == null) {
                try {
                    StandardUnitDB.instance = new StandardUnitDB();
                }
                catch (Exception e) {
                    throw new UnitDBException("Couldn't create standard unit-database", e);
                }
            }
        }
        return StandardUnitDB.instance;
    }
    
    private void au(final String name, final String definition) throws UnitExistsException, NoSuchUnitException, UnitParseException, SpecificationException, UnitDBException, PrefixDBException, OperationException, NameException, UnitSystemException {
        this.au(name, definition, null);
    }
    
    private void au(final String name, final String definition, final String symbol) throws UnitExistsException, NoSuchUnitException, UnitParseException, SpecificationException, UnitDBException, PrefixDBException, OperationException, NameException, UnitSystemException {
        this.au(name, definition, symbol, null);
    }
    
    private void au(final String name, final String definition, final String symbol, final String plural) throws UnitExistsException, NoSuchUnitException, UnitParseException, SpecificationException, UnitDBException, PrefixDBException, OperationException, NameException, UnitSystemException {
        final Unit unit = StandardUnitDB.format.parse(definition, this);
        if (unit == null) {
            throw new NoSuchUnitException(definition);
        }
        this.addUnit(unit.clone(UnitName.newUnitName(name, plural, symbol)));
    }
    
    private void aa(final String alias, final String name) throws UnitExistsException, NoSuchUnitException, UnitParseException, SpecificationException, UnitDBException, PrefixDBException, OperationException, NameException, UnitSystemException {
        this.aa(alias, name, null);
    }
    
    private void aa(final String alias, final String name, final String plural) throws UnitExistsException, NoSuchUnitException, UnitParseException, SpecificationException, UnitDBException, PrefixDBException, OperationException, NameException, UnitSystemException {
        this.aa(alias, name, plural, null);
    }
    
    private void aa(final String alias, final String name, final String plural, final String symbol) throws UnitExistsException, NoSuchUnitException, UnitParseException, SpecificationException, UnitDBException, PrefixDBException, OperationException, NameException, UnitSystemException {
        this.addAlias(alias, name, symbol, plural);
    }
    
    private void as(final String symbol, final String name) throws UnitExistsException, NoSuchUnitException, UnitParseException, SpecificationException, UnitDBException, PrefixDBException, OperationException, NameException, UnitSystemException {
        this.addSymbol(symbol, name);
    }
    
    public static void main(final String[] args) throws Exception {
        final UnitDB db = instance();
        System.out.println("db.get(\"meter\")=" + db.get("meter"));
        System.out.println("db.get(\"meters\")=" + db.get("meters"));
        System.out.println("db.get(\"metre\")=" + db.get("metre"));
        System.out.println("db.get(\"metres\")=" + db.get("metres"));
        System.out.println("db.get(\"m\")=" + db.get("m"));
        System.out.println("db.get(\"newton\")=" + db.get("newton"));
        System.out.println("db.get(\"Cel\")=" + db.get("Cel"));
        System.out.println("db.get(\"Roentgen\")=" + db.get("Roentgen"));
        System.out.println("db.get(\"rad\")=" + db.get("rad"));
        System.out.println("db.get(\"rd\")=" + db.get("rd"));
        System.out.println("db.get(\"perches\")=" + db.get("perches"));
        System.out.println("db.get(\"jiffies\")=" + db.get("jiffies"));
        System.out.println("db.get(\"foo\")=" + db.get("foo"));
    }
    
    static {
        StandardUnitDB.instance = null;
    }
}
