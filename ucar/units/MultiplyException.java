// 
// Decompiled by Procyon v0.5.36
// 

package ucar.units;

public final class MultiplyException extends OperationException
{
    private static final long serialVersionUID = 1L;
    
    public MultiplyException(final Unit unit) {
        super("Can't multiply unit \"" + unit + '\"');
    }
    
    public MultiplyException(final Unit A, final Unit B) {
        super("Can't multiply unit \"" + A + "\" by unit \"" + B + '\"');
    }
    
    public MultiplyException(final double scale, final Unit unit) {
        super("Can't multiply unit \"" + unit + "\" by " + scale);
    }
}
