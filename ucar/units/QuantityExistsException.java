// 
// Decompiled by Procyon v0.5.36
// 

package ucar.units;

public class QuantityExistsException extends UnitException
{
    private static final long serialVersionUID = 1L;
    
    public QuantityExistsException(final BaseQuantity quantity) {
        this("Base quantity \"" + quantity + " already exists");
    }
    
    private QuantityExistsException(final String msg) {
        super(msg);
    }
}
