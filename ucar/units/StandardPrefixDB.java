// 
// Decompiled by Procyon v0.5.36
// 

package ucar.units;

public final class StandardPrefixDB extends PrefixDBImpl
{
    private static final long serialVersionUID = 1L;
    private static StandardPrefixDB instance;
    
    private StandardPrefixDB() throws PrefixExistsException {
        this.add("yotta", "Y", 1.0E24);
        this.add("zetta", "Z", 1.0E21);
        this.add("exa", "E", 1.0E18);
        this.add("peta", "P", 1.0E15);
        this.add("tera", "T", 1.0E12);
        this.add("giga", "G", 1.0E9);
        this.add("mega", "M", 1000000.0);
        this.add("kilo", "k", 1000.0);
        this.add("hecto", "h", 100.0);
        this.add("deca", "da", 10.0);
        this.addName("deka", 10.0);
        this.add("deci", "d", 0.1);
        this.add("centi", "c", 0.01);
        this.add("milli", "m", 0.001);
        this.add("micro", "u", 1.0E-6);
        this.add("nano", "n", 1.0E-9);
        this.add("pico", "p", 1.0E-12);
        this.add("femto", "f", 1.0E-15);
        this.add("atto", "a", 1.0E-18);
        this.add("zepto", "z", 1.0E-21);
        this.add("yocto", "y", 1.0E-24);
    }
    
    public static StandardPrefixDB instance() throws PrefixDBException {
        if (StandardPrefixDB.instance == null) {
            synchronized (StandardPrefixDB.class) {
                if (StandardPrefixDB.instance == null) {
                    try {
                        StandardPrefixDB.instance = new StandardPrefixDB();
                    }
                    catch (Exception e) {
                        throw new PrefixDBException("Couldn't create standard prefix-database", e);
                    }
                }
            }
        }
        return StandardPrefixDB.instance;
    }
    
    private void add(final String name, final String symbol, final double definition) throws PrefixExistsException {
        this.addName(name, definition);
        this.addSymbol(symbol, definition);
    }
    
    public static void main(final String[] args) throws Exception {
        final PrefixDB db = instance();
        System.out.println("db.getPrefixBySymbol(\"cm\") = \"" + db.getPrefixBySymbol("cm") + '\"');
        System.out.println("db.getPrefixBySymbol(\"dm\") = \"" + db.getPrefixBySymbol("dm") + '\"');
    }
    
    static {
        StandardPrefixDB.instance = null;
    }
}
