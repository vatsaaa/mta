// 
// Decompiled by Procyon v0.5.36
// 

package ucar.units;

import java.io.Serializable;

public final class UnitSystemManager implements Serializable
{
    private static final long serialVersionUID = 1L;
    private static UnitSystem instance;
    
    public static final UnitSystem instance() throws UnitSystemException {
        synchronized (UnitSystemManager.class) {
            if (UnitSystemManager.instance == null) {
                UnitSystemManager.instance = SI.instance();
            }
        }
        return UnitSystemManager.instance;
    }
    
    public static final synchronized void setInstance(final UnitSystem instance) throws UnitSystemException {
        if (instance != null) {
            throw new UnitSystemException("Unit system already used");
        }
        UnitSystemManager.instance = instance;
    }
}
