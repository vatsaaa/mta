// 
// Decompiled by Procyon v0.5.36
// 

package ucar.units;

public final class UnitClassException extends UnitFormatException
{
    private static final long serialVersionUID = 1L;
    
    private UnitClassException(final String msg) {
        super(msg);
    }
    
    public UnitClassException(final Unit unit) {
        this("\"" + unit.getClass().getName() + "\" is an unknown unit class");
    }
}
