// 
// Decompiled by Procyon v0.5.36
// 

package ucar.units;

import java.util.Iterator;

public interface UnitDB
{
    void addUnit(final Unit p0) throws UnitExistsException, UnitDBAccessException, NameException;
    
    void addAlias(final String p0, final String p1) throws NoSuchUnitException, UnitExistsException, UnitDBAccessException, NameException;
    
    void addAlias(final String p0, final String p1, final String p2) throws NoSuchUnitException, UnitExistsException, UnitDBAccessException, NameException;
    
    void addAlias(final String p0, final String p1, final String p2, final String p3) throws NoSuchUnitException, UnitExistsException, UnitDBAccessException, NameException;
    
    void addAlias(final UnitID p0, final String p1) throws NoSuchUnitException, UnitExistsException, UnitDBAccessException;
    
    void addSymbol(final String p0, final String p1) throws NoSuchUnitException, UnitExistsException, UnitDBAccessException, NameException;
    
    Unit get(final String p0) throws UnitDBAccessException;
    
    Unit getByName(final String p0) throws UnitDBAccessException;
    
    Unit getBySymbol(final String p0) throws UnitDBAccessException;
    
    String toString();
    
    Iterator<?> getIterator();
}
