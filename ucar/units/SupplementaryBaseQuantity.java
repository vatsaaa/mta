// 
// Decompiled by Procyon v0.5.36
// 

package ucar.units;

public final class SupplementaryBaseQuantity extends BaseQuantity
{
    private static final long serialVersionUID = 1L;
    
    public SupplementaryBaseQuantity(final String name, final String symbol) throws NameException {
        super(name, symbol);
    }
    
    protected SupplementaryBaseQuantity(final String name, final String symbol, final boolean trusted) {
        super(name, symbol, trusted);
    }
    
    @Override
    public boolean isDimensionless() {
        return true;
    }
}
