// 
// Decompiled by Procyon v0.5.36
// 

package ucar.units;

import java.util.Date;

public final class ShiftException extends OperationException
{
    private static final long serialVersionUID = 1L;
    
    public ShiftException(final Unit unit, final double origin) {
        super("Can't shift origin of unit \"" + unit + "\" to " + origin);
    }
    
    public ShiftException(final Unit unit, final Date origin) {
        super("Can't shift origin of unit \"" + unit + "\" to " + origin);
    }
}
