// 
// Decompiled by Procyon v0.5.36
// 

package ucar.units;

public class UnitDBException extends UnitException
{
    private static final long serialVersionUID = 1L;
    
    protected UnitDBException() {
    }
    
    public UnitDBException(final String message) {
        super(message);
    }
    
    public UnitDBException(final String message, final Exception e) {
        super(message, e);
    }
}
