// 
// Decompiled by Procyon v0.5.36
// 

package ucar.units;

public final class SI extends UnitSystemImpl
{
    private static final long serialVersionUID = 1L;
    private static SI si;
    public static final Unit AMOUNT_OF_SUBSTANCE_UNIT;
    public static final Unit ELECTRIC_CURRENT_UNIT;
    public static final Unit LENGTH_UNIT;
    public static final Unit LUMINOUS_INTENSITY_UNIT;
    public static final Unit MASS_UNIT;
    public static final Unit PLANE_ANGLE_UNIT;
    public static final Unit SOLID_ANGLE_UNIT;
    public static final Unit THERMODYNAMIC_TEMPERATURE_UNIT;
    public static final Unit TIME_UNIT;
    public static final BaseUnit AMPERE;
    public static final BaseUnit CANDELA;
    public static final BaseUnit KELVIN;
    public static final BaseUnit KILOGRAM;
    public static final BaseUnit METER;
    public static final BaseUnit METRE;
    public static final BaseUnit MOLE;
    public static final BaseUnit SECOND;
    public static final BaseUnit RADIAN;
    public static final BaseUnit STERADIAN;
    public static final Unit HERTZ;
    public static final Unit NEWTON;
    public static final Unit PASCAL;
    public static final Unit JOULE;
    public static final Unit WATT;
    public static final Unit COULOMB;
    public static final Unit VOLT;
    public static final Unit FARAD;
    public static final Unit OHM;
    public static final Unit SIEMENS;
    public static final Unit WEBER;
    public static final Unit TESLA;
    public static final Unit HENRY;
    public static final Unit DEGREE_CELSIUS;
    public static final Unit LUMEN;
    public static final Unit LUX;
    public static final Unit BECQUEREL;
    public static final Unit GRAY;
    public static final Unit SIEVERT;
    public static final Unit MINUTE;
    public static final Unit HOUR;
    public static final Unit DAY;
    public static final Unit ARC_DEGREE;
    public static final Unit ARC_MINUTE;
    public static final Unit ARC_SECOND;
    public static final Unit LITER;
    public static final Unit LITRE;
    public static final Unit METRIC_TON;
    public static final Unit TONNE;
    public static final Unit NAUTICAL_MILE;
    public static final Unit KNOT;
    public static final Unit ANGSTROM;
    public static final Unit ARE;
    public static final Unit HECTARE;
    public static final Unit BARN;
    public static final Unit BAR;
    public static final Unit GAL;
    public static final Unit CURIE;
    public static final Unit ROENTGEN;
    public static final Unit RAD;
    public static final Unit REM;
    
    private static BaseUnit bu(final String name, final String symbol, final BaseQuantity quantity) throws NameException, UnitExistsException {
        return BaseUnit.getOrCreate(UnitName.newUnitName(name, null, symbol), quantity);
    }
    
    private static Unit du(final String name, final String symbol, final Unit definition) throws NameException {
        return definition.clone(UnitName.newUnitName(name, null, symbol));
    }
    
    private SI() throws UnitExistsException, NameException, PrefixDBException, NoSuchUnitException {
        super(baseUnitDB(), derivedUnitDB());
    }
    
    private static UnitDBImpl baseUnitDB() throws NameException, UnitExistsException, NoSuchUnitException {
        final UnitDBImpl db = new UnitDBImpl(9, 9);
        db.addUnit(SI.AMPERE);
        db.addUnit(SI.CANDELA);
        db.addUnit(SI.KELVIN);
        db.addUnit(SI.KILOGRAM);
        db.addUnit(SI.METER);
        db.addUnit(SI.MOLE);
        db.addUnit(SI.SECOND);
        db.addUnit(SI.RADIAN);
        db.addUnit(SI.STERADIAN);
        db.addAlias("metre", "meter");
        return db;
    }
    
    private static UnitDBImpl derivedUnitDB() throws NameException, UnitExistsException, NoSuchUnitException {
        final UnitDBImpl db = new UnitDBImpl(42, 43);
        db.addUnit(SI.HERTZ);
        db.addUnit(SI.NEWTON);
        db.addUnit(SI.PASCAL);
        db.addUnit(SI.JOULE);
        db.addUnit(SI.WATT);
        db.addUnit(SI.COULOMB);
        db.addUnit(SI.VOLT);
        db.addUnit(SI.FARAD);
        db.addUnit(SI.OHM);
        db.addUnit(SI.SIEMENS);
        db.addUnit(SI.WEBER);
        db.addUnit(SI.TESLA);
        db.addUnit(SI.HENRY);
        db.addUnit(SI.DEGREE_CELSIUS);
        db.addUnit(SI.LUMEN);
        db.addUnit(SI.LUX);
        db.addUnit(SI.BECQUEREL);
        db.addUnit(SI.GRAY);
        db.addUnit(SI.SIEVERT);
        db.addUnit(SI.MINUTE);
        db.addUnit(SI.HOUR);
        db.addUnit(SI.DAY);
        db.addUnit(SI.ARC_DEGREE);
        db.addUnit(SI.ARC_MINUTE);
        db.addUnit(SI.ARC_SECOND);
        db.addUnit(SI.LITER);
        db.addUnit(SI.METRIC_TON);
        db.addUnit(SI.NAUTICAL_MILE);
        db.addUnit(SI.KNOT);
        db.addUnit(SI.ANGSTROM);
        db.addUnit(SI.ARE);
        db.addUnit(SI.HECTARE);
        db.addUnit(SI.BARN);
        db.addUnit(SI.BAR);
        db.addUnit(SI.GAL);
        db.addUnit(SI.CURIE);
        db.addUnit(SI.ROENTGEN);
        db.addUnit(SI.RAD);
        db.addUnit(SI.REM);
        db.addAlias("litre", "liter", "l");
        db.addAlias("tonne", "metric ton");
        db.addSymbol("tne", "tonne");
        return db;
    }
    
    public static SI instance() throws UnitSystemException {
        synchronized (SI.class) {
            if (SI.si == null) {
                try {
                    SI.si = new SI();
                }
                catch (UnitException e) {
                    throw new UnitSystemException("Couldn't initialize class SI", e);
                }
            }
        }
        return SI.si;
    }
    
    public static void main(final String[] args) throws Exception {
        instance();
    }
    
    static {
        BaseUnit ampere = null;
        BaseUnit candela = null;
        BaseUnit kelvin = null;
        BaseUnit kilogram = null;
        BaseUnit meter = null;
        BaseUnit mole = null;
        BaseUnit second = null;
        BaseUnit radian = null;
        BaseUnit steradian = null;
        Unit hertz = null;
        Unit newton = null;
        Unit pascal = null;
        Unit joule = null;
        Unit watt = null;
        Unit coulomb = null;
        Unit volt = null;
        Unit farad = null;
        Unit ohm = null;
        Unit siemens = null;
        Unit weber = null;
        Unit tesla = null;
        Unit henry = null;
        Unit degree_celsius = null;
        Unit lumen = null;
        Unit lux = null;
        Unit becquerel = null;
        Unit gray = null;
        Unit sievert = null;
        Unit minute = null;
        Unit hour = null;
        Unit day = null;
        Unit arc_degree = null;
        Unit arc_minute = null;
        Unit arc_second = null;
        Unit liter = null;
        Unit metric_ton = null;
        Unit nautical_mile = null;
        Unit knot = null;
        Unit angstrom = null;
        Unit are = null;
        Unit hectare = null;
        Unit barn = null;
        Unit bar = null;
        Unit gal = null;
        Unit curie = null;
        Unit roentgen = null;
        Unit rad = null;
        Unit rem = null;
        try {
            ampere = bu("ampere", "A", BaseQuantity.ELECTRIC_CURRENT);
            candela = bu("candela", "cd", BaseQuantity.LUMINOUS_INTENSITY);
            kelvin = bu("kelvin", "K", BaseQuantity.THERMODYNAMIC_TEMPERATURE);
            kilogram = bu("kilogram", "kg", BaseQuantity.MASS);
            meter = bu("meter", "m", BaseQuantity.LENGTH);
            mole = bu("mole", "mol", BaseQuantity.AMOUNT_OF_SUBSTANCE);
            second = bu("second", "s", BaseQuantity.TIME);
            radian = bu("radian", "rad", BaseQuantity.PLANE_ANGLE);
            steradian = bu("steradian", "sr", BaseQuantity.SOLID_ANGLE);
            hertz = du("hertz", "Hz", second.raiseTo(-1));
            newton = du("newton", "N", kilogram.multiplyBy(meter).divideBy(second.raiseTo(2)));
            pascal = du("pascal", "Pa", newton.divideBy(meter.raiseTo(2)));
            joule = du("joule", "J", newton.multiplyBy(meter));
            watt = du("watt", "W", joule.divideBy(second));
            coulomb = du("coulomb", "C", ampere.multiplyBy(second));
            volt = du("volt", "V", watt.divideBy(ampere));
            farad = du("farad", "F", coulomb.divideBy(volt));
            ohm = du("ohm", "Ohm", volt.divideBy(ampere));
            siemens = du("siemens", "S", ohm.raiseTo(-1));
            weber = du("weber", "Wb", volt.multiplyBy(second));
            tesla = du("tesla", "T", weber.divideBy(meter.raiseTo(2)));
            henry = du("henry", "H", weber.divideBy(ampere));
            degree_celsius = du("degree celsius", "Cel", new OffsetUnit(kelvin, 273.15));
            lumen = du("lumen", "lm", candela.multiplyBy(steradian));
            lux = du("lux", "lx", lumen.divideBy(meter.raiseTo(2)));
            becquerel = du("becquerel", "Bq", hertz);
            gray = du("gray", "Gy", joule.divideBy(kilogram));
            sievert = du("sievert", "Sv", joule.divideBy(kilogram));
            minute = du("minute", "min", new ScaledUnit(60.0, second));
            hour = du("hour", "h", new ScaledUnit(60.0, minute));
            day = du("day", "d", new ScaledUnit(24.0, hour));
            arc_degree = du("arc degree", "deg", new ScaledUnit(0.017453292519943295, radian));
            arc_minute = du("arc minute", "'", new ScaledUnit(0.016666666666666666, arc_degree));
            arc_second = du("arc second", "\"", new ScaledUnit(0.016666666666666666, arc_minute));
            liter = du("liter", "L", new ScaledUnit(0.001, meter.raiseTo(3)));
            metric_ton = du("metric ton", "t", new ScaledUnit(1000.0, kilogram));
            nautical_mile = du("nautical mile", "nmi", new ScaledUnit(1852.0, meter));
            knot = du("knot", "kt", nautical_mile.divideBy(hour));
            angstrom = du("angstrom", null, new ScaledUnit(1.0E-10, meter));
            are = du("are", "are", new ScaledUnit(10.0, meter).raiseTo(2));
            hectare = du("hectare", "ha", new ScaledUnit(100.0, are));
            barn = du("barn", "b", new ScaledUnit(1.0E-28, meter.raiseTo(2)));
            bar = du("bar", "bar", new ScaledUnit(100000.0, pascal));
            gal = du("gal", "Gal", new ScaledUnit(0.01, meter).divideBy(second.raiseTo(2)));
            curie = du("curie", "Ci", new ScaledUnit(3.7E10, becquerel));
            roentgen = du("roentgen", "R", new ScaledUnit(2.58E-4, coulomb.divideBy(kilogram)));
            rad = du("rad", "rd", new ScaledUnit(0.01, gray));
            rem = du("rem", "rem", new ScaledUnit(0.01, sievert));
        }
        catch (UnitException e) {
            final String reason = e.getMessage();
            System.err.println(("Couldn't initialize class SI" + reason == null) ? "" : (": " + reason));
        }
        AMOUNT_OF_SUBSTANCE_UNIT = mole;
        ELECTRIC_CURRENT_UNIT = ampere;
        LENGTH_UNIT = meter;
        LUMINOUS_INTENSITY_UNIT = candela;
        MASS_UNIT = kilogram;
        PLANE_ANGLE_UNIT = radian;
        SOLID_ANGLE_UNIT = steradian;
        THERMODYNAMIC_TEMPERATURE_UNIT = kelvin;
        TIME_UNIT = second;
        AMPERE = ampere;
        CANDELA = candela;
        KELVIN = kelvin;
        KILOGRAM = kilogram;
        METER = meter;
        METRE = SI.METER;
        MOLE = mole;
        SECOND = second;
        RADIAN = radian;
        STERADIAN = steradian;
        HERTZ = hertz;
        NEWTON = newton;
        PASCAL = pascal;
        JOULE = joule;
        WATT = watt;
        COULOMB = coulomb;
        VOLT = volt;
        FARAD = farad;
        OHM = ohm;
        SIEMENS = siemens;
        WEBER = weber;
        TESLA = tesla;
        HENRY = henry;
        DEGREE_CELSIUS = degree_celsius;
        LUMEN = lumen;
        LUX = lux;
        BECQUEREL = becquerel;
        GRAY = gray;
        SIEVERT = sievert;
        MINUTE = minute;
        HOUR = hour;
        DAY = day;
        ARC_DEGREE = arc_degree;
        ARC_MINUTE = arc_minute;
        ARC_SECOND = arc_second;
        LITER = liter;
        LITRE = SI.LITER;
        METRIC_TON = metric_ton;
        TONNE = SI.METRIC_TON;
        NAUTICAL_MILE = nautical_mile;
        KNOT = knot;
        ANGSTROM = angstrom;
        ARE = are;
        HECTARE = hectare;
        BARN = barn;
        BAR = bar;
        GAL = gal;
        CURIE = curie;
        ROENTGEN = roentgen;
        RAD = rad;
        REM = rem;
    }
}
