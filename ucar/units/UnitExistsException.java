// 
// Decompiled by Procyon v0.5.36
// 

package ucar.units;

public class UnitExistsException extends UnitDBException
{
    private static final long serialVersionUID = 1L;
    
    public UnitExistsException(final Unit oldUnit, final Unit newUnit) {
        this("Attempt to replace \"" + oldUnit + "\" with \"" + newUnit + "\" in unit database");
    }
    
    public UnitExistsException(final String msg) {
        super(msg);
    }
}
