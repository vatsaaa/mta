// 
// Decompiled by Procyon v0.5.36
// 

package ucar.units;

public final class UnknownBaseQuantity extends BaseQuantity
{
    private static final long serialVersionUID = 1L;
    
    protected UnknownBaseQuantity() {
        super("Unknown", "x", true);
    }
    
    @Override
    public boolean equals(final Object object) {
        return false;
    }
    
    @Override
    public int hashCode() {
        return System.identityHashCode(this);
    }
    
    @Override
    public boolean isDimensionless() {
        return false;
    }
}
