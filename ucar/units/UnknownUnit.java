// 
// Decompiled by Procyon v0.5.36
// 

package ucar.units;

import java.util.TreeMap;
import java.util.SortedMap;

public final class UnknownUnit extends BaseUnit
{
    private static final long serialVersionUID = 1L;
    private static final SortedMap<String, UnknownUnit> map;
    
    private UnknownUnit(final String name) throws NameException {
        super(UnitName.newUnitName(name, null, name), BaseQuantity.UNKNOWN);
    }
    
    public static UnknownUnit create(String name) throws NameException {
        name = name.toLowerCase();
        UnknownUnit unit;
        synchronized (UnknownUnit.map) {
            unit = UnknownUnit.map.get(name);
            if (unit == null) {
                unit = new UnknownUnit(name);
                UnknownUnit.map.put(unit.getName(), unit);
                UnknownUnit.map.put(unit.getPlural(), unit);
            }
        }
        return unit;
    }
    
    @Override
    public boolean equals(final Object object) {
        if (this == object) {
            return true;
        }
        if (!(object instanceof UnknownUnit)) {
            return false;
        }
        final UnknownUnit that = (UnknownUnit)object;
        return this.getName().equalsIgnoreCase(that.getName());
    }
    
    @Override
    public int hashCode() {
        return this.getName().toLowerCase().hashCode();
    }
    
    @Override
    public boolean isDimensionless() {
        return false;
    }
    
    public static void main(final String[] args) throws Exception {
        final UnknownUnit unit1 = create("a");
        System.out.println("unit_a.equals(unit_a)=" + unit1.equals(unit1));
        System.out.println("unit_a.isDimensionless()=" + unit1.isDimensionless());
        UnknownUnit unit2 = create("b");
        System.out.println("unit_a.equals(unit_b)=" + unit1.equals(unit2));
        unit2 = create("A");
        System.out.println("unit_a.equals(unit_A)=" + unit1.equals(unit2));
    }
    
    static {
        map = new TreeMap<String, UnknownUnit>();
    }
}
