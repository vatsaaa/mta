// 
// Decompiled by Procyon v0.5.36
// 

package ucar.units;

public class UnitSystemException extends UnitException
{
    private static final long serialVersionUID = 1L;
    
    public UnitSystemException(final String message) {
        super(message);
    }
    
    public UnitSystemException(final String message, final Exception e) {
        super(message, e);
    }
}
