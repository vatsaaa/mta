// 
// Decompiled by Procyon v0.5.36
// 

package ucar.units;

import java.util.Iterator;
import java.util.Collection;
import java.util.Hashtable;
import java.util.TreeSet;
import java.util.Comparator;
import java.util.Map;
import java.util.Set;
import java.io.Serializable;

public class UnitDBImpl implements UnitDB, Serializable
{
    private static final long serialVersionUID = 1L;
    private final Set<Unit> unitSet;
    private final Map<String, Unit> nameMap;
    private final Map<String, Unit> symbolMap;
    
    protected UnitDBImpl(final int nameCount, final int symbolCount) {
        this.unitSet = new TreeSet<Unit>(new Comparator<Unit>() {
            public int compare(final Unit obj1, final Unit obj2) {
                return obj1.getName().compareTo(obj2.getName());
            }
        });
        this.nameMap = new Hashtable<String, Unit>(nameCount + 1);
        this.symbolMap = new Hashtable<String, Unit>(symbolCount + 1);
    }
    
    public void add(final UnitDBImpl that) throws UnitExistsException {
        this.unitSet.addAll(that.unitSet);
        this.nameMap.putAll(that.nameMap);
        this.symbolMap.putAll(that.symbolMap);
    }
    
    public int nameCount() {
        return this.nameMap.size();
    }
    
    public int symbolCount() {
        return this.symbolMap.size();
    }
    
    public void addUnit(final Unit unit) throws UnitExistsException, NameException {
        if (unit.getName() == null) {
            throw new NameException("Unit name can't be null");
        }
        this.addByName(unit.getName(), unit);
        this.addByName(unit.getPlural(), unit);
        this.addBySymbol(unit.getSymbol(), unit);
        this.unitSet.add(unit);
    }
    
    public final void addAlias(final String alias, final String name) throws NoSuchUnitException, UnitExistsException {
        this.addAlias(alias, name, null);
    }
    
    public final void addAlias(final String alias, final String name, final String symbol) throws NoSuchUnitException, UnitExistsException {
        this.addAlias(alias, name, symbol, null);
    }
    
    public final void addSymbol(final String symbol, final String name) throws NoSuchUnitException, UnitExistsException {
        this.addAlias(null, name, symbol, null);
    }
    
    public final void addAlias(final String alias, final String name, final String symbol, final String plural) throws NoSuchUnitException, UnitExistsException {
        this.addAlias(UnitID.newUnitID(alias, plural, symbol), name);
    }
    
    public final void addAlias(final UnitID alias, final String name) throws NoSuchUnitException, UnitExistsException {
        final Unit unit = this.getByName(name);
        if (unit == null) {
            throw new NoSuchUnitException(name);
        }
        this.addByName(alias.getName(), unit);
        this.addByName(alias.getPlural(), unit);
        this.addBySymbol(alias.getSymbol(), unit);
    }
    
    public Unit get(final String id) {
        Unit unit = this.getBySymbol(id);
        if (unit == null) {
            unit = this.getByName(id);
        }
        return unit;
    }
    
    public Unit getByName(final String name) {
        return this.nameMap.get(canonicalize(name));
    }
    
    private static String canonicalize(final String name) {
        return name.toLowerCase().replace(' ', '_');
    }
    
    public Unit getBySymbol(final String symbol) {
        return this.symbolMap.get(symbol);
    }
    
    @Override
    public String toString() {
        return this.unitSet.toString();
    }
    
    public final Iterator getIterator() {
        return this.unitSet.iterator();
    }
    
    private final void addByName(final String name, final Unit newUnit) throws UnitExistsException {
        if (name != null) {
            addUnique(this.nameMap, canonicalize(name), newUnit);
        }
    }
    
    private final void addBySymbol(final String symbol, final Unit newUnit) throws UnitExistsException {
        if (symbol != null) {
            addUnique(this.symbolMap, symbol, newUnit);
        }
    }
    
    private static final void addUnique(final Map<String, Unit> map, final String key, final Unit newUnit) throws UnitExistsException {
        final Unit oldUnit = map.put(key, newUnit);
        if (oldUnit != null && !oldUnit.equals(newUnit)) {
            throw new UnitExistsException(oldUnit, newUnit);
        }
    }
}
