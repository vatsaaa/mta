// 
// Decompiled by Procyon v0.5.36
// 

package ucar.units;

import java.util.TreeMap;
import java.util.SortedMap;

public class BaseUnit extends DerivedUnitImpl implements Base
{
    private static final long serialVersionUID = 1L;
    private static final SortedMap<UnitName, BaseUnit> nameMap;
    private static final SortedMap<BaseQuantity, BaseUnit> quantityMap;
    private final BaseQuantity baseQuantity;
    
    protected BaseUnit(final UnitName id, final BaseQuantity baseQuantity) throws NameException {
        super(id);
        if (id.getSymbol() == null) {
            throw new NameException("Base unit must have symbol");
        }
        this.setDimension(new UnitDimension(this));
        this.baseQuantity = baseQuantity;
    }
    
    public static synchronized BaseUnit getOrCreate(final UnitName id, final BaseQuantity baseQuantity) throws NameException, UnitExistsException {
        final BaseUnit nameUnit = BaseUnit.nameMap.get(id);
        final BaseUnit quantityUnit = BaseUnit.quantityMap.get(baseQuantity);
        BaseUnit baseUnit;
        if (nameUnit != null || quantityUnit != null) {
            baseUnit = ((nameUnit != null) ? nameUnit : quantityUnit);
            if ((nameUnit != null && !baseQuantity.equals(nameUnit.getBaseQuantity())) || (quantityUnit != null && !id.equals(quantityUnit.getUnitName()))) {
                throw new UnitExistsException("Attempt to incompatibly redefine base unit \"" + baseUnit + '\"');
            }
        }
        else {
            baseUnit = new BaseUnit(id, baseQuantity);
            BaseUnit.quantityMap.put(baseQuantity, baseUnit);
            BaseUnit.nameMap.put(id, baseUnit);
        }
        return baseUnit;
    }
    
    public final BaseQuantity getBaseQuantity() {
        return this.baseQuantity;
    }
    
    public final String getID() {
        return this.getSymbol();
    }
    
    @Override
    public final String toString() {
        return this.getID();
    }
    
    @Override
    public boolean isDimensionless() {
        return this.baseQuantity.isDimensionless();
    }
    
    public static void main(final String[] args) throws Exception {
        final BaseUnit meter = new BaseUnit(UnitName.newUnitName("meter", null, "m"), BaseQuantity.LENGTH);
        System.out.println("meter.getBaseQuantity()=" + meter.getBaseQuantity());
        System.out.println("meter.toDerivedUnit(1.)=" + meter.toDerivedUnit(1.0));
        System.out.println("meter.toDerivedUnit(new float[] {2})[0]=" + meter.toDerivedUnit(new float[] { 2.0f }, new float[1])[0]);
        System.out.println("meter.fromDerivedUnit(1.)=" + meter.fromDerivedUnit(1.0));
        System.out.println("meter.fromDerivedUnit(new float[] {3})[0]=" + meter.fromDerivedUnit(new float[] { 3.0f }, new float[1])[0]);
        System.out.println("meter.isCompatible(meter)=" + meter.isCompatible(meter));
        final BaseUnit radian = new BaseUnit(UnitName.newUnitName("radian", null, "rad"), BaseQuantity.PLANE_ANGLE);
        System.out.println("meter.isCompatible(radian)=" + meter.isCompatible(radian));
        System.out.println("meter.isDimensionless()=" + meter.isDimensionless());
        System.out.println("radian.isDimensionless()=" + radian.isDimensionless());
    }
    
    static {
        nameMap = new TreeMap<UnitName, BaseUnit>();
        quantityMap = new TreeMap<BaseQuantity, BaseUnit>();
    }
}
