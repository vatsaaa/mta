// 
// Decompiled by Procyon v0.5.36
// 

package ucar.units;

import java.util.Iterator;
import java.util.HashMap;
import java.io.Serializable;

public class UnitSystemImpl implements UnitSystem, Serializable
{
    private static final long serialVersionUID = 1L;
    private final HashMap<BaseQuantity, BaseUnit> quantityMap;
    private final UnitDB baseUnitDB;
    private final UnitDBImpl acceptableUnitDB;
    
    protected UnitSystemImpl(final UnitDBImpl baseUnitDB, final UnitDBImpl derivedUnitDB) throws UnitExistsException {
        this.quantityMap = new HashMap<BaseQuantity, BaseUnit>(baseUnitDB.nameCount());
        final Iterator<?> iter = (Iterator<?>)baseUnitDB.getIterator();
        while (iter.hasNext()) {
            final Unit unit = (Unit)iter.next();
            final BaseUnit baseUnit = (BaseUnit)unit;
            this.quantityMap.put(baseUnit.getBaseQuantity(), baseUnit);
        }
        this.baseUnitDB = baseUnitDB;
        (this.acceptableUnitDB = new UnitDBImpl(baseUnitDB.nameCount() + derivedUnitDB.nameCount(), baseUnitDB.symbolCount() + derivedUnitDB.symbolCount())).add(baseUnitDB);
        this.acceptableUnitDB.add(derivedUnitDB);
    }
    
    public final UnitDB getBaseUnitDB() {
        return this.baseUnitDB;
    }
    
    public final UnitDB getUnitDB() {
        return this.acceptableUnitDB;
    }
    
    public final BaseUnit getBaseUnit(final BaseQuantity quantity) {
        return this.quantityMap.get(quantity);
    }
}
