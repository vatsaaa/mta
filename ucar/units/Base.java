// 
// Decompiled by Procyon v0.5.36
// 

package ucar.units;

public interface Base
{
    boolean isDimensionless();
    
    String getID();
    
    boolean equals(final Object p0);
}
