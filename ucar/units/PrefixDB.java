// 
// Decompiled by Procyon v0.5.36
// 

package ucar.units;

import java.util.Iterator;

public interface PrefixDB
{
    void addName(final String p0, final double p1) throws PrefixExistsException, PrefixDBAccessException;
    
    void addSymbol(final String p0, final double p1) throws PrefixExistsException, PrefixDBAccessException;
    
    Prefix getPrefixByName(final String p0) throws PrefixDBAccessException;
    
    Prefix getPrefixBySymbol(final String p0) throws PrefixDBAccessException;
    
    Prefix getPrefixByValue(final double p0) throws PrefixDBAccessException;
    
    String toString();
    
    Iterator<?> iterator();
}
