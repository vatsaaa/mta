// 
// Decompiled by Procyon v0.5.36
// 

package ucar.unidata.util;

import java.util.Collection;
import java.util.Arrays;
import java.util.Comparator;
import java.util.ArrayList;
import java.util.List;
import java.util.Date;

public class DatedObject implements DatedThing
{
    private Date date;
    private Object object;
    
    public DatedObject() {
    }
    
    public DatedObject(final Date date) {
        this(date, null);
    }
    
    public DatedObject(final Date date, final Object object) {
        this.date = date;
        this.object = object;
    }
    
    public static List select(Date startDate, Date endDate, final List datedThings) {
        if (startDate.getTime() > endDate.getTime()) {
            final Date tmp = startDate;
            startDate = endDate;
            endDate = tmp;
        }
        final long t1 = startDate.getTime();
        final long t2 = endDate.getTime();
        final List selected = new ArrayList();
        for (int i = 0; i < datedThings.size(); ++i) {
            final DatedThing datedThing = datedThings.get(i);
            final long time = datedThing.getDate().getTime();
            if (time >= t1 && time <= t2) {
                selected.add(datedThing);
            }
        }
        return selected;
    }
    
    public static List wrap(final List dates) {
        final List result = new ArrayList();
        for (int i = 0; i < dates.size(); ++i) {
            result.add(new DatedObject(dates.get(i)));
        }
        return result;
    }
    
    public static List unwrap(final List datedThings) {
        final List result = new ArrayList();
        for (int i = 0; i < datedThings.size(); ++i) {
            result.add(datedThings.get(i).getDate());
        }
        return result;
    }
    
    public static List getObjects(final List datedObjects) {
        final List result = new ArrayList();
        if (datedObjects == null) {
            return result;
        }
        for (int i = 0; i < datedObjects.size(); ++i) {
            result.add(datedObjects.get(i).getObject());
        }
        return result;
    }
    
    public static List sort(List datedThings, final boolean ascending) {
        final Comparator comp = new Comparator() {
            @Override
            public int compare(final Object o1, final Object o2) {
                final DatedThing a1 = (DatedThing)o1;
                final DatedThing a2 = (DatedThing)o2;
                int result = a1.getDate().compareTo(a2.getDate());
                if (!ascending) {
                    result = -result;
                }
                return result;
            }
            
            @Override
            public boolean equals(final Object obj) {
                return obj == this;
            }
        };
        final Object[] array = datedThings.toArray();
        Arrays.sort(array, comp);
        final List result = Arrays.asList(array);
        datedThings = new ArrayList();
        datedThings.addAll(result);
        return datedThings;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (!(o instanceof DatedObject)) {
            return false;
        }
        final DatedObject that = (DatedObject)o;
        if (!this.date.equals(that.date)) {
            return false;
        }
        if (this.object == null) {
            return that.object == null;
        }
        if (that.object == null) {
            return this.object == null;
        }
        return this.object.equals(that.object);
    }
    
    public void setDate(final Date value) {
        this.date = value;
    }
    
    @Override
    public Date getDate() {
        return this.date;
    }
    
    public void setObject(final Object value) {
        this.object = value;
    }
    
    public Object getObject() {
        return this.object;
    }
    
    @Override
    public String toString() {
        if (this.object != null) {
            return "" + this.object;
        }
        return "";
    }
}
