// 
// Decompiled by Procyon v0.5.36
// 

package ucar.unidata.util;

import java.util.Locale;
import java.util.List;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.util.TimeZone;

public class DateUtil
{
    public static final String[] MONTH_NAMES;
    public static final long MILLIS = 1L;
    public static final long MILLIS_SECOND = 1000L;
    public static final long MILLIS_MINUTE = 60000L;
    public static final long MILLIS_HOUR = 3600000L;
    public static final long MILLIS_DAY = 86400000L;
    public static final long MILLIS_WEEK = 604800000L;
    public static final long MILLIS_MONTH = 2592000000L;
    public static final long MILLIS_YEAR = 31536000000L;
    public static final long MILLIS_DECADE = 315360000000L;
    public static final long MILLIS_CENTURY = 3153600000000L;
    public static final long MILLIS_MILLENIUM = 31536000000000L;
    public static final TimeZone TIMEZONE_GMT;
    public static final TimeZone TIMEZONE_UTC;
    public static final String[] DATE_PATTERNS;
    public static final String[] DATE_FORMATS;
    private static final String[] formats;
    private static SimpleDateFormat[] sdfs;
    private static SimpleDateFormat lastSdf;
    
    public static String getCurrentSystemTimeAsISO8601() {
        return getTimeAsISO8601(System.currentTimeMillis());
    }
    
    public static String getTimeAsISO8601(final Date date) {
        return getTimeAsISO8601(date.getTime());
    }
    
    public static Date min(final Date date1, final Date date2) {
        if (date1.getTime() < date2.getTime()) {
            return date1;
        }
        return date2;
    }
    
    public static Date max(final Date date1, final Date date2) {
        if (date1.getTime() > date2.getTime()) {
            return date1;
        }
        return date2;
    }
    
    public static String getTimeAsISO8601(final long time) {
        final Calendar cal = Calendar.getInstance(DateUtil.TIMEZONE_GMT);
        cal.setTimeInMillis(time);
        final Date curSysDate = cal.getTime();
        return DateFormatHandler.ISO_DATE_TIME.getDateTimeStringFromDate(curSysDate);
    }
    
    public static Date roundByDay(final Date dttm, int day) {
        if (day == 0) {
            return dttm;
        }
        if (day < 0) {
            ++day;
        }
        final Calendar cal = Calendar.getInstance(DateUtil.TIMEZONE_GMT);
        cal.setTimeInMillis(dttm.getTime());
        cal.clear(14);
        cal.set(13, 0);
        cal.set(12, 0);
        cal.set(11, 0);
        cal.add(6, day);
        return new Date(cal.getTimeInMillis());
    }
    
    public static SimpleDateFormat findFormatter(final String dateString) {
        if (DateUtil.sdfs == null) {
            DateUtil.sdfs = new SimpleDateFormat[DateUtil.formats.length];
            for (int i = 0; i < DateUtil.formats.length; ++i) {
                DateUtil.sdfs[i] = new SimpleDateFormat(DateUtil.formats[i]);
            }
        }
        if (DateUtil.lastSdf != null) {
            try {
                DateUtil.lastSdf.parse(dateString);
                return DateUtil.lastSdf;
            }
            catch (ParseException ex) {}
        }
        int i = 0;
        while (i < DateUtil.formats.length) {
            try {
                final Date dttm = DateUtil.sdfs[i].parse(dateString);
                DateUtil.lastSdf = DateUtil.sdfs[i];
                return DateUtil.sdfs[i];
            }
            catch (ParseException pe) {
                ++i;
                continue;
            }
            break;
        }
        throw new IllegalArgumentException("Could not find date format for:" + dateString);
    }
    
    public static Date[] getDateRange(final String fromDate, final String toDate, final Date dflt) throws ParseException {
        Date fromDttm = parseRelative(dflt, fromDate, -1);
        Date toDttm = parseRelative(dflt, toDate, 1);
        if (fromDate.length() > 0 && fromDttm == null && !fromDate.startsWith("-")) {
            fromDttm = parse(fromDate);
        }
        if (toDate.length() > 0 && toDttm == null && !toDate.startsWith("+")) {
            toDttm = parse(toDate);
        }
        if (fromDttm == null && fromDate.startsWith("-")) {
            if (toDttm == null) {
                throw new IllegalArgumentException("Cannot do relative From Date when To Date is not set");
            }
            fromDttm = getRelativeDate(toDttm, fromDate);
        }
        if (toDttm == null && toDate.startsWith("+")) {
            if (fromDttm == null) {
                throw new IllegalArgumentException("Cannot do relative From Date when To Date is not set");
            }
            toDttm = getRelativeDate(fromDttm, toDate);
        }
        return new Date[] { fromDttm, toDttm };
    }
    
    public static Date parseRelative(final Date baseDate, String s, final int roundDays) throws ParseException {
        s = s.trim();
        final Calendar cal = Calendar.getInstance(DateUtil.TIMEZONE_GMT);
        cal.setTimeInMillis(baseDate.getTime());
        Date dttm = null;
        if (s.equals("now")) {
            dttm = cal.getTime();
            return dttm;
        }
        if (s.equals("today")) {
            dttm = cal.getTime();
        }
        else if (s.equals("yesterday")) {
            dttm = new Date(cal.getTime().getTime() - daysToMillis(1.0));
        }
        else if (s.equals("tomorrow")) {
            dttm = new Date(cal.getTime().getTime() + daysToMillis(1.0));
        }
        else if (s.startsWith("last") || s.startsWith("next")) {
            final List toks = StringUtil.split(s, " ", true, true);
            if (toks.size() != 2) {
                throw new IllegalArgumentException("Bad time format:" + s);
            }
            final int factor = toks.get(0).equals("last") ? -1 : 1;
            final String unit = toks.get(1);
            if (unit.equals("week")) {
                cal.add(4, factor);
            }
            else if (unit.equals("month")) {
                cal.add(2, factor);
            }
            else if (unit.equals("year")) {
                cal.add(1, factor);
            }
            else if (unit.equals("century")) {
                cal.add(1, factor * 100);
            }
            else {
                if (!unit.equals("millenium")) {
                    throw new IllegalArgumentException("Bad time format:" + s + " unknown time field:" + unit);
                }
                cal.add(1, factor * 1000);
            }
            dttm = cal.getTime();
        }
        if (dttm != null) {
            return roundByDay(dttm, roundDays);
        }
        return null;
    }
    
    public static Date parse(final String s) throws ParseException {
        final SimpleDateFormat sdf = findFormatter(s);
        return sdf.parse(s);
    }
    
    public static double[] toSeconds(final String[] s) throws ParseException {
        final double[] d = new double[s.length];
        if (s.length == 0) {
            return d;
        }
        final SimpleDateFormat sdf = findFormatter(s[0]);
        double lastTime = 0.0;
        for (int i = 0; i < s.length; ++i) {
            d[i] = sdf.parse(s[i]).getTime() / 1000.0;
            if (d[i] < lastTime) {
                System.out.println("****" + s[i]);
            }
            lastTime = d[i];
        }
        return d;
    }
    
    public static long daysToMillis(final double days) {
        return hoursToMillis(days * 24.0);
    }
    
    public static long hoursToMillis(final double hour) {
        return minutesToMillis(hour * 60.0);
    }
    
    public static double millisToMinutes(final double millis) {
        return millis / 1000.0 / 60.0;
    }
    
    public static double millisToHours(final double millis) {
        return millis / 1000.0 / 60.0 / 60.0;
    }
    
    public static long minutesToMillis(final double minutes) {
        return (long)(minutes * 60.0 * 1000.0);
    }
    
    public static Date getRelativeDate(final Date from, final String relativeTimeString) {
        final Date result = new Date(from.getTime() + parseRelativeTimeString(relativeTimeString));
        return result;
    }
    
    public static long parseRelativeTimeString(final String relativeTimeString) {
        final List toks = StringUtil.split(relativeTimeString, " ", true, true);
        if (toks.size() != 2) {
            throw new IllegalArgumentException("Bad format for relative time string:" + relativeTimeString + " Needs to be of the form: +/-<number> timeunit");
        }
        long delta = 0L;
        try {
            String s = toks.get(0).toString();
            long factor = 1L;
            if (s.startsWith("+")) {
                s = s.substring(1);
            }
            else if (s.startsWith("-")) {
                s = s.substring(1);
                factor = -1L;
            }
            delta = factor * new Integer(s);
        }
        catch (Exception exc) {
            throw new IllegalArgumentException("Bad format for relative time string:" + relativeTimeString + " Could not parse initial number:" + toks.get(0));
        }
        final String what = toks.get(1);
        long milliseconds = 0L;
        if (what.startsWith("second")) {
            milliseconds = delta * 1000L;
        }
        else if (what.startsWith("minute")) {
            milliseconds = 60L * delta * 1000L;
        }
        else if (what.startsWith("hour")) {
            milliseconds = 3600L * delta * 1000L;
        }
        else if (what.startsWith("day")) {
            milliseconds = 86400L * delta * 1000L;
        }
        else if (what.startsWith("week")) {
            milliseconds = 604800L * delta * 1000L;
        }
        else if (what.startsWith("month")) {
            milliseconds = 2592000L * delta * 1000L;
        }
        else if (what.startsWith("year")) {
            milliseconds = 31536000L * delta * 1000L;
        }
        else if (what.startsWith("century")) {
            milliseconds = -1141367296L * delta * 1000L;
        }
        else {
            if (!what.startsWith("millenium")) {
                throw new IllegalArgumentException("Unknown unit in relative time string:" + relativeTimeString);
            }
            milliseconds = 1471228928L * delta * 1000L;
        }
        return milliseconds;
    }
    
    public static Date decodeWMODate(String wmoDate, Date baseDate) {
        if (baseDate == null) {
            baseDate = new Date();
        }
        if (wmoDate.length() > 6) {
            return baseDate;
        }
        wmoDate = StringUtil.padLeft(wmoDate, 6, "0");
        final Calendar cal = Calendar.getInstance(DateUtil.TIMEZONE_GMT);
        cal.setTimeInMillis(baseDate.getTime());
        final int day = Integer.parseInt(wmoDate.substring(0, 2));
        final int hour = Integer.parseInt(wmoDate.substring(2, 4));
        final int min = Integer.parseInt(wmoDate.substring(4));
        final int calDay = cal.get(5);
        if (calDay - day > 26) {
            cal.add(2, 1);
        }
        else if (day - calDay > 20) {
            cal.add(2, -1);
        }
        cal.set(5, day);
        cal.set(11, hour);
        cal.set(12, min);
        return cal.getTime();
    }
    
    public static void main(final String[] args) throws Exception {
        System.err.println(TimeZone.getTimeZone(args[0]));
        System.err.println(TimeZone.getDefault());
        final String[] ids = TimeZone.getAvailableIDs();
        for (int i = 0; i < ids.length; ++i) {
            System.out.println(ids[i] + " " + TimeZone.getTimeZone(ids[i]));
        }
    }
    
    static {
        MONTH_NAMES = new String[] { "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" };
        TIMEZONE_GMT = TimeZone.getTimeZone("GMT");
        TIMEZONE_UTC = TimeZone.getTimeZone("UTC");
        DATE_PATTERNS = new String[] { "(\\d\\d\\d\\d\\d\\d\\d\\d_\\d\\d\\d\\d)", "(\\d\\d\\d\\d\\d\\d\\d\\d_\\d\\d)", "(\\d\\d\\d\\d\\d\\d\\d\\d)" };
        DATE_FORMATS = new String[] { "yyyyMMdd_HHmm", "yyyyMMdd_HH", "yyyyMMdd" };
        formats = new String[] { "yyyy-MM-dd'T'HH:mm:ss Z", "yyyyMMdd'T'HHmmss Z", "yyyy/MM/dd HH:mm:ss Z", "yyyy-MM-dd HH:mm:ss Z", "EEE MMM dd HH:mm:ss Z yyyy", "yyyy-MM-dd'T'HH:mm:ss", "yyyyMMdd'T'HHmmss", "yyyy/MM/dd HH:mm:ss", "yyyy-MM-dd HH:mm:ss", "yyyy-MM-dd'T'HH:mm Z", "yyyyMMdd'T'HHmm Z", "yyyy/MM/dd HH:mm Z", "yyyy-MM-dd HH:mm Z", "yyyy-MM-dd'T'HH:mm", "yyyyMMdd'T'HHmm", "yyyy/MM/dd HH:mm", "yyyy-MM-dd HH:mm", "yyyy-MM-dd", "yyyy/MM/dd", "yyyyMMdd", "yyyy-MM", "yyyyMM", "yyyy/MM", "yyyy" };
    }
    
    static class DateFormatHandler
    {
        public static final DateFormatHandler ISO_DATE;
        public static final DateFormatHandler ISO_TIME;
        public static final DateFormatHandler ISO_DATE_TIME;
        public static final DateFormatHandler ISO_DATE_TIME_MILLIS;
        private String dateTimeFormatString;
        
        private DateFormatHandler(final String dateTimeFormatString) {
            this.dateTimeFormatString = null;
            this.dateTimeFormatString = dateTimeFormatString;
        }
        
        public String getDateTimeFormatString() {
            return this.dateTimeFormatString;
        }
        
        public Date getDateFromDateTimeString(final String dateTimeString) {
            Date theDate = null;
            final SimpleDateFormat dateFormat = new SimpleDateFormat(this.dateTimeFormatString, Locale.US);
            dateFormat.setTimeZone(DateUtil.TIMEZONE_GMT);
            try {
                theDate = dateFormat.parse(dateTimeString);
            }
            catch (ParseException e) {
                throw new IllegalArgumentException(e.getMessage());
            }
            return theDate;
        }
        
        public String getDateTimeStringFromDate(final Date date) {
            final SimpleDateFormat dateFormat = new SimpleDateFormat(this.dateTimeFormatString, Locale.US);
            dateFormat.setTimeZone(DateUtil.TIMEZONE_GMT);
            final String dateString = dateFormat.format(date);
            return dateString;
        }
        
        static {
            ISO_DATE = new DateFormatHandler("yyyy-MM-dd");
            ISO_TIME = new DateFormatHandler("HH:mm:ss.SSSz");
            ISO_DATE_TIME = new DateFormatHandler("yyyy-MM-dd'T'HH:mm:ssz");
            ISO_DATE_TIME_MILLIS = new DateFormatHandler("yyyy-MM-dd'T'HH:mm:ss.SSSz");
        }
    }
}
