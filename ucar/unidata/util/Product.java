// 
// Decompiled by Procyon v0.5.36
// 

package ucar.unidata.util;

public class Product
{
    private String id;
    private String name;
    
    public Product() {
    }
    
    public Product(final String id, final String name) {
        this.id = id;
        this.name = name;
    }
    
    public String getID() {
        return this.id;
    }
    
    public String getName() {
        return this.name;
    }
    
    public void setID(final String id) {
        this.id = id;
    }
    
    public void setName(final String name) {
        this.name = name;
    }
    
    @Override
    public boolean equals(final Object oo) {
        if (!(oo instanceof Product)) {
            return false;
        }
        final Product that = (Product)oo;
        return this.id.equals(that.id);
    }
}
