// 
// Decompiled by Procyon v0.5.36
// 

package ucar.unidata.util;

import java.util.regex.Matcher;
import java.util.ArrayList;
import java.text.ParsePosition;
import java.util.TimeZone;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.StringTokenizer;
import java.util.regex.Pattern;
import java.util.List;
import java.awt.Color;
import java.util.Hashtable;

public class StringUtil
{
    public static boolean debug;
    public static final String[] ordinalNames;
    private static char[] htmlIn;
    private static String[] htmlOut;
    private static char[] xmlInC;
    private static String[] xmlOutC;
    private static char[] xmlIn;
    private static String[] xmlOut;
    private static final Object MATCH_MUTEX;
    private static Hashtable patternCache;
    
    public static boolean notEmpty(final String s) {
        return s != null && s.trim().length() > 0;
    }
    
    public static String collapseWhitespace(final String s) {
        final int len = s.length();
        final StringBuffer b = new StringBuffer(len);
        for (int i = 0; i < len; ++i) {
            final char c = s.charAt(i);
            if (!Character.isWhitespace(c)) {
                b.append(c);
            }
            else {
                b.append(' ');
                while (i + 1 < len && Character.isWhitespace(s.charAt(i + 1))) {
                    ++i;
                }
            }
        }
        return b.toString();
    }
    
    public static String remove(String s, final String sub) {
        int pos;
        for (int len = sub.length(); 0 <= (pos = s.indexOf(sub)); s = s.substring(0, pos) + s.substring(pos + len)) {}
        return s;
    }
    
    public static String remove(final String s, final int c) {
        if (0 > s.indexOf(c)) {
            return s;
        }
        final StringBuffer buff = new StringBuffer(s);
        int i = 0;
        while (i < buff.length()) {
            if (buff.charAt(i) == c) {
                buff.deleteCharAt(i);
            }
            else {
                ++i;
            }
        }
        return buff.toString();
    }
    
    public static String findFormatString(final String macroName, final String macroDelimiter, final String text) {
        final String prefix = macroDelimiter + macroName + ":";
        final int idx1 = text.indexOf(prefix);
        if (idx1 >= 0) {
            final int idx2 = text.indexOf(macroDelimiter, idx1 + 1);
            if (idx2 > idx1) {
                return text.substring(idx1 + prefix.length(), idx2);
            }
        }
        return null;
    }
    
    public static String substitute(final String original, final String match, final String subst) {
        String s;
        int pos;
        StringBuffer sb;
        for (s = original; 0 <= (pos = s.indexOf(match)); s = sb.replace(pos, pos + match.length(), subst).toString()) {
            sb = new StringBuffer(s);
        }
        return s;
    }
    
    public static String repeat(final String s, final int cnt) {
        final StringBuffer sb = new StringBuffer();
        for (int i = 0; i < cnt; ++i) {
            sb.append(s);
        }
        return sb.toString();
    }
    
    public static String substitute(final String original, final String[] match, final String[] subst) {
        boolean ok = true;
        for (int i = 0; i < match.length; ++i) {
            if (0 <= original.indexOf(match[i])) {
                ok = false;
                break;
            }
        }
        if (ok) {
            return original;
        }
        final StringBuffer sb = new StringBuffer(original);
        for (int j = 0; j < match.length; ++j) {
            substitute(sb, match[j], subst[j]);
        }
        return sb.toString();
    }
    
    public static void substitute(final StringBuffer sbuff, final String match, final String subst) {
        int fromIndex = 0;
        final int substLen = subst.length();
        final int matchLen = match.length();
        int pos;
        while (0 <= (pos = sbuff.indexOf(match, fromIndex))) {
            sbuff.replace(pos, pos + matchLen, subst);
            fromIndex = pos + substLen;
        }
    }
    
    public static boolean isDigits(final String s) {
        for (int i = 0; i < s.length(); ++i) {
            if (!Character.isDigit(s.charAt(i))) {
                return false;
            }
        }
        return true;
    }
    
    public static String quoteHtmlContent(final String x) {
        return replace(x, StringUtil.htmlIn, StringUtil.htmlOut);
    }
    
    public static String quoteXmlContent(final String x) {
        return replace(x, StringUtil.xmlInC, StringUtil.xmlOutC);
    }
    
    public static String unquoteXmlContent(final String x) {
        return unreplace(x, StringUtil.xmlOutC, StringUtil.xmlInC);
    }
    
    public static String quoteXmlAttribute(final String x) {
        return replace(x, StringUtil.xmlIn, StringUtil.xmlOut);
    }
    
    public static String unquoteXmlAttribute(final String x) {
        return unreplace(x, StringUtil.xmlOut, StringUtil.xmlIn);
    }
    
    public static String replace(final String x, final char[] replaceChar, final String[] replaceWith) {
        boolean ok = true;
        for (int i = 0; i < replaceChar.length; ++i) {
            final int pos = x.indexOf(replaceChar[i]);
            ok = (pos < 0);
            if (!ok) {
                break;
            }
        }
        if (ok) {
            return x;
        }
        final StringBuffer sb = new StringBuffer(x);
        for (int j = 0; j < replaceChar.length; ++j) {
            final int pos2 = x.indexOf(replaceChar[j]);
            if (pos2 >= 0) {
                replace(sb, replaceChar[j], replaceWith[j]);
            }
        }
        return sb.toString();
    }
    
    public static String unreplace(final String x, final String[] orgReplace, final char[] orgChar) {
        boolean ok = true;
        for (int i = 0; i < orgReplace.length; ++i) {
            final int pos = x.indexOf(orgReplace[i]);
            ok = (pos < 0);
            if (!ok) {
                break;
            }
        }
        if (ok) {
            return x;
        }
        final StringBuffer result = new StringBuffer(x);
        for (int j = 0; j < orgReplace.length; ++j) {
            final int pos2 = result.indexOf(orgReplace[j]);
            if (pos2 >= 0) {
                unreplace(result, orgReplace[j], orgChar[j]);
            }
        }
        return result.toString();
    }
    
    public static int match(final String s1, final String s2) {
        int i;
        for (i = 0; i < s1.length() && i < s2.length() && s1.charAt(i) == s2.charAt(i); ++i) {}
        return i;
    }
    
    public static void replace(final StringBuffer sb, final char out, final String in) {
        for (int i = 0; i < sb.length(); ++i) {
            if (sb.charAt(i) == out) {
                sb.replace(i, i + 1, in);
                i += in.length() - 1;
            }
        }
    }
    
    public static void unreplace(final StringBuffer sb, final String out, final char in) {
        int pos;
        while (0 <= (pos = sb.indexOf(out))) {
            sb.setCharAt(pos, in);
            sb.delete(pos + 1, pos + out.length());
        }
    }
    
    public static String replace(final String s, final char out, final String in) {
        if (s.indexOf(out) < 0) {
            return s;
        }
        final StringBuffer sb = new StringBuffer(s);
        replace(sb, out, in);
        return sb.toString();
    }
    
    public static String escape(final String x, final String okChars) {
        boolean ok = true;
        for (int pos = 0; pos < x.length(); ++pos) {
            final char c = x.charAt(pos);
            if (!Character.isLetterOrDigit(c) && 0 > okChars.indexOf(c)) {
                ok = false;
                break;
            }
        }
        if (ok) {
            return x;
        }
        final StringBuffer sb = new StringBuffer(x);
        for (int pos2 = 0; pos2 < sb.length(); ++pos2) {
            final char c2 = sb.charAt(pos2);
            if (!Character.isLetterOrDigit(c2)) {
                if (0 > okChars.indexOf(c2)) {
                    sb.setCharAt(pos2, '%');
                    final int value = c2;
                    ++pos2;
                    sb.insert(pos2, Integer.toHexString(value));
                    ++pos2;
                }
            }
        }
        return sb.toString();
    }
    
    public static String escape2(final String x, final String reservedChars) {
        boolean ok = true;
        for (int pos = 0; pos < x.length(); ++pos) {
            final char c = x.charAt(pos);
            if (reservedChars.indexOf(c) >= 0) {
                ok = false;
                break;
            }
        }
        if (ok) {
            return x;
        }
        final StringBuffer sb = new StringBuffer(x);
        for (int pos2 = 0; pos2 < sb.length(); ++pos2) {
            final char c2 = sb.charAt(pos2);
            if (reservedChars.indexOf(c2) >= 0) {
                sb.setCharAt(pos2, '%');
                final int value = c2;
                ++pos2;
                sb.insert(pos2, Integer.toHexString(value));
                ++pos2;
            }
        }
        return sb.toString();
    }
    
    public static String toHexString(final Color c) {
        return "#" + padRight(Integer.toHexString(c.getRed()), 2, "0") + padRight(Integer.toHexString(c.getGreen()), 2, "0") + padRight(Integer.toHexString(c.getBlue()), 2, "0");
    }
    
    public static String filter7bits(final String s) {
        final byte[] b = s.getBytes();
        final byte[] bo = new byte[b.length];
        int count = 0;
        for (int i = 0; i < s.length(); ++i) {
            if ((b[i] < 128 && b[i] > 31) || b[i] == 10 || b[i] == 9) {
                bo[count++] = b[i];
            }
        }
        return new String(bo, 0, count);
    }
    
    public static String filter(final String x, final String okChars) {
        boolean ok = true;
        for (int pos = 0; pos < x.length(); ++pos) {
            final char c = x.charAt(pos);
            if (!Character.isLetterOrDigit(c) && 0 > okChars.indexOf(c)) {
                ok = false;
                break;
            }
        }
        if (ok) {
            return x;
        }
        final StringBuffer sb = new StringBuffer(x.length());
        for (int pos2 = 0; pos2 < x.length(); ++pos2) {
            final char c2 = x.charAt(pos2);
            if (Character.isLetterOrDigit(c2) || 0 <= okChars.indexOf(c2)) {
                sb.append(c2);
            }
        }
        return sb.toString();
    }
    
    public static String allow(final String x, final String allowChars, final char replaceChar) {
        boolean ok = true;
        for (int pos = 0; pos < x.length(); ++pos) {
            final char c = x.charAt(pos);
            if (!Character.isLetterOrDigit(c) && 0 > allowChars.indexOf(c)) {
                ok = false;
                break;
            }
        }
        if (ok) {
            return x;
        }
        final StringBuffer sb = new StringBuffer(x);
        for (int pos2 = 0; pos2 < sb.length(); ++pos2) {
            final char c2 = sb.charAt(pos2);
            if (!Character.isLetterOrDigit(c2)) {
                if (0 > allowChars.indexOf(c2)) {
                    sb.setCharAt(pos2, replaceChar);
                }
            }
        }
        return sb.toString();
    }
    
    public static String unescape(final String x) {
        if (x.indexOf(37) < 0) {
            return x;
        }
        final char[] b = new char[2];
        final StringBuffer sb = new StringBuffer(x);
        for (int pos = 0; pos < sb.length(); ++pos) {
            char c = sb.charAt(pos);
            if (c == '%') {
                b[0] = sb.charAt(pos + 1);
                b[1] = sb.charAt(pos + 2);
                int value;
                try {
                    value = Integer.parseInt(new String(b), 16);
                }
                catch (NumberFormatException e) {
                    continue;
                }
                c = (char)value;
                sb.setCharAt(pos, c);
                sb.delete(pos + 1, pos + 3);
            }
        }
        return sb.toString();
    }
    
    public static Object findMatch(final String source, final List patternList, final Object dflt) {
        if (StringUtil.debug) {
            System.err.println("findMatch:" + source + ":");
        }
        for (int i = 0; i < patternList.size(); ++i) {
            final Object object = patternList.get(i);
            if (object != null) {
                if (StringUtil.debug) {
                    System.err.println("\t:" + object.toString() + ":");
                }
                if (stringMatch(source, object.toString())) {
                    return object;
                }
            }
        }
        return dflt;
    }
    
    public static Object findMatch(final String source, final List patternList, final List results, final Object dflt) {
        for (int i = 0; i < patternList.size(); ++i) {
            if (stringMatch(source, patternList.get(i).toString())) {
                return results.get(i);
            }
        }
        return dflt;
    }
    
    public static boolean containsRegExp(final String patternString) {
        return patternString.indexOf(94) >= 0 || patternString.indexOf(42) >= 0 || patternString.indexOf(124) >= 0 || patternString.indexOf(40) >= 0 || patternString.indexOf(36) >= 0 || patternString.indexOf(63) >= 0 || patternString.indexOf(46) >= 0 || (patternString.indexOf(91) >= 0 && patternString.indexOf(93) >= 0) || patternString.indexOf(43) >= 0;
    }
    
    public static boolean stringMatch(final String input, final String patternString) {
        return stringMatch(input, patternString, false, true);
    }
    
    public static boolean stringMatch(final String input, String patternString, final boolean substring, final boolean caseSensitive) {
        synchronized (StringUtil.MATCH_MUTEX) {
            try {
                if (!caseSensitive) {
                    if (input.equalsIgnoreCase(patternString)) {
                        return true;
                    }
                }
                else if (input.equals(patternString)) {
                    return true;
                }
                if (substring) {
                    if (!caseSensitive) {
                        if (input.toLowerCase().indexOf(patternString.toLowerCase()) >= 0) {
                            return true;
                        }
                    }
                    else if (input.indexOf(patternString) >= 0) {
                        return true;
                    }
                }
                if (patternString.toLowerCase().indexOf("t_") >= 0) {}
                if (patternString.startsWith("*") || patternString.equals("*")) {
                    patternString = "." + patternString;
                }
                else if (patternString.startsWith("glob:")) {
                    patternString = patternString.substring("glob:".length());
                    patternString = wildcardToRegexp(patternString);
                }
                if (!containsRegExp(patternString)) {
                    return false;
                }
                Pattern pattern = StringUtil.patternCache.get(patternString);
                if (pattern == null) {
                    pattern = Pattern.compile(patternString);
                    StringUtil.patternCache.put(patternString, pattern);
                }
                return pattern.matcher(input).find();
            }
            catch (Exception exc) {
                System.err.println("Error regexpMatch:" + exc);
                exc.printStackTrace();
                return true;
            }
        }
    }
    
    public static boolean regexpMatch(final String input, final String patternString) {
        synchronized (StringUtil.MATCH_MUTEX) {
            Pattern pattern = StringUtil.patternCache.get(patternString);
            if (pattern == null) {
                pattern = Pattern.compile(patternString);
                StringUtil.patternCache.put(patternString, pattern);
            }
            return pattern.matcher(input).find();
        }
    }
    
    public static String wildcardToRegexp(final String wildcard) {
        final StringBuffer s = new StringBuffer(wildcard.length());
        s.append('^');
        for (int i = 0, is = wildcard.length(); i < is; ++i) {
            final char c = wildcard.charAt(i);
            switch (c) {
                case '*': {
                    s.append(".*");
                    break;
                }
                case '?': {
                    s.append(".");
                    break;
                }
                case '$':
                case '(':
                case ')':
                case '.':
                case '[':
                case '\\':
                case ']':
                case '^':
                case '{':
                case '|':
                case '}': {
                    s.append("\\");
                    s.append(c);
                    break;
                }
                default: {
                    s.append(c);
                    break;
                }
            }
        }
        s.append('$');
        return s.toString();
    }
    
    public static boolean startsWithVowel(final String value) {
        if (value == null || value.equals("")) {
            return false;
        }
        final char lower = Character.toLowerCase(value.charAt(0));
        return lower == 'a' || lower == 'e' || lower == 'i' || lower == 'o' || lower == 'u';
    }
    
    public static String breakText(String text, final String insert, final int lineSize) {
        text = replace(text, "\n", " ");
        final StringBuffer buff = new StringBuffer();
        while (text.length() > 0) {
            final int len = text.length();
            if (len < lineSize) {
                buff.append(text);
                break;
            }
            int idx;
            for (idx = lineSize; idx < len && text.charAt(idx) != ' '; ++idx) {}
            if (idx == len) {
                buff.append(text);
                break;
            }
            buff.append(text.substring(0, idx));
            buff.append(insert);
            text = text.substring(idx);
        }
        return buff.toString();
    }
    
    public static String breakTextAtWords(final String text, final String insert, final int lineSize) {
        final StringBuffer buff = new StringBuffer();
        final StringTokenizer stoker = new StringTokenizer(text);
        int lineCount = 0;
        while (stoker.hasMoreTokens()) {
            final String tok = stoker.nextToken();
            if (tok.length() + lineCount >= lineSize) {
                buff.append(insert);
                lineCount = 0;
            }
            buff.append(tok);
            buff.append(" ");
            lineCount += tok.length() + 1;
        }
        return buff.toString();
    }
    
    public static String stripHtmlTag(String html) {
        html = html.trim();
        if (html.startsWith("<html>")) {
            html = html.substring(6);
        }
        if (html.endsWith("</html>")) {
            html = html.substring(0, html.length() - 7);
        }
        return html;
    }
    
    public static String stripTags(String html) {
        StringBuffer stripped = new StringBuffer();
        while (html.length() > 0) {
            final int idx = html.indexOf("<");
            if (idx < 0) {
                stripped.append(html.trim());
                break;
            }
            String text = html.substring(0, idx);
            text = text.trim();
            if (text.length() > 0) {
                stripped.append(text + " \n");
            }
            html = html.substring(idx);
            final int idx2 = html.indexOf(">");
            if (idx2 < 0) {
                break;
            }
            html = html.substring(idx2 + 1);
        }
        stripped = new StringBuffer(replace(stripped.toString(), "&nbsp;", ""));
        return stripped.toString();
    }
    
    public static String stripAndReplace(String s, final String pattern1, final String pattern2, final String replace) {
        final StringBuffer stripped = new StringBuffer();
        while (s.length() > 0) {
            final int idx = s.indexOf(pattern1);
            if (idx < 0) {
                stripped.append(s);
                break;
            }
            final String text = s.substring(0, idx);
            if (text.length() > 0) {
                stripped.append(text);
            }
            s = s.substring(idx + 1);
            final int idx2 = s.indexOf(pattern2);
            if (idx2 < 0) {
                break;
            }
            stripped.append(replace);
            s = s.substring(idx2 + 1);
        }
        stripped.append(s);
        return stripped.toString();
    }
    
    public static String removeWhitespace(final String inputString) {
        final StringBuffer sb = new StringBuffer();
        final char[] chars = inputString.toCharArray();
        for (int i = 0; i < chars.length; ++i) {
            final char c = chars[i];
            if (!Character.isWhitespace(c)) {
                sb.append(c);
            }
        }
        return sb.toString();
    }
    
    public static String zeroString(final int value) {
        return padZero(value, 2);
    }
    
    public static String padZero(final int value, final int numDigits) {
        return padLeft(String.valueOf(value), numDigits, "0");
    }
    
    public static String padLeft(final String s, final int desiredLength) {
        return padLeft(s, desiredLength, " ");
    }
    
    public static String padLeft(String s, final int desiredLength, final String padString) {
        while (s.length() < desiredLength) {
            s = padString + s;
        }
        return s;
    }
    
    public static String padRight(final String s, final int desiredLength) {
        return padRight(s, desiredLength, " ");
    }
    
    public static String padRight(String s, final int desiredLength, final String padString) {
        while (s.length() < desiredLength) {
            s += padString;
        }
        return s;
    }
    
    public static String join(final String[] args) {
        return join(" ", args);
    }
    
    public static String join(final String delimiter, final Object[] args) {
        return join(delimiter, args, false);
    }
    
    public static String join(final String delimiter, final Object[] args, final boolean ignoreEmptyStrings) {
        final StringBuffer sb = new StringBuffer();
        for (int i = 0; i < args.length; ++i) {
            if (ignoreEmptyStrings) {
                if (args[i] == null) {
                    continue;
                }
                if (args[i].toString().length() == 0) {
                    continue;
                }
            }
            if (i > 0) {
                sb.append(delimiter);
            }
            sb.append(args[i].toString());
        }
        return sb.toString();
    }
    
    public static String join(final String delimiter, final List args) {
        return join(delimiter, listToStringArray(args));
    }
    
    public static String join(final String delimiter, final List args, final boolean ignoreEmptyStrings) {
        return join(delimiter, listToStringArray(args), ignoreEmptyStrings);
    }
    
    public static List<String> split(final Object source) {
        return split(source, ",");
    }
    
    public static Date parseDate(final String dttm) {
        final String[] formats = { "yyyy-MM-dd HH:mm:ss z", "yyyy-MM-dd HH:mm:ss", "yyyy-MM-dd HH:mm", "yyyy-MM-dd", "yyyyMMddHHmmss", "yyyyMMddHHmm", "yyyyMMddHH", "yyyyMMdd" };
        for (int i = 0; i < formats.length; ++i) {
            final SimpleDateFormat dateFormat = new SimpleDateFormat(formats[i]);
            dateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
            final Date date = dateFormat.parse(dttm, new ParsePosition(0));
            if (date != null) {
                return date;
            }
        }
        return null;
    }
    
    public static List parseIntegerListString(final String s) {
        final List items = new ArrayList();
        final List tokens = split(s, ",");
        for (int tokIdx = 0; tokIdx < tokens.size(); ++tokIdx) {
            final String token = tokens.get(tokIdx);
            if (token.indexOf(":") >= 0) {
                int stride = 1;
                final List subTokens = split(token, ":");
                if (subTokens.size() > 2) {
                    stride = new Integer(subTokens.get(2).toString());
                }
                final int start = new Integer(subTokens.get(0).toString());
                for (int end = new Integer(subTokens.get(1).toString()), i = start; i <= end; i += stride) {
                    items.add(new Integer(i));
                }
            }
            else {
                items.add(new Integer(token));
            }
        }
        return items;
    }
    
    public static List<String[]> parseLineWords(final String content, final int[] lengths, final String lineDelimiter, final String commentString, final boolean trimWords) {
        final int[] indices = new int[lengths.length];
        int length = 0;
        for (int i = 0; i < indices.length; ++i) {
            indices[i] = length;
            length += lengths[i];
        }
        return parseLineWords(content, indices, lengths, lineDelimiter, commentString, trimWords);
    }
    
    public static List<String[]> parseLineWords(final String content, final int[] indices, final int[] lengths, final String lineDelimiter, final String commentString, final boolean trimWords) {
        final List lines = split(content, lineDelimiter, false);
        final List result = new ArrayList();
        for (int i = 0; i < lines.size(); ++i) {
            final String line = lines.get(i);
            final String tline = line.trim();
            if (tline.length() != 0) {
                if (commentString == null || !tline.startsWith(commentString)) {
                    final String[] words = new String[indices.length];
                    for (int idx = 0; idx < indices.length; ++idx) {
                        int endIndex = indices[idx] + lengths[idx];
                        if (endIndex > line.length()) {
                            endIndex = line.length();
                        }
                        words[idx] = line.substring(indices[idx], endIndex);
                        if (trimWords) {
                            words[idx] = words[idx].trim();
                        }
                    }
                    result.add(words);
                }
            }
        }
        return (List<String[]>)result;
    }
    
    public static String[] splitString(final String source) {
        return source.trim().split("\\s+");
    }
    
    public static List<String> split(final Object source, final String delimiter) {
        return split(source, delimiter, true);
    }
    
    public static List<String> split(final Object source, final String delimiter, final boolean trim) {
        return split(source, delimiter, trim, false);
    }
    
    public static List<String> splitWithQuotes(String s) {
        final ArrayList<String> list = new ArrayList<String>();
        if (s == null) {
            return list;
        }
        while (true) {
            s = s.trim();
            final int qidx1 = s.indexOf("\"");
            final int qidx2 = s.indexOf("\"", qidx1 + 1);
            final int sidx1 = 0;
            final int sidx2 = s.indexOf(" ", sidx1 + 1);
            if (qidx1 < 0 && sidx2 < 0) {
                if (s.length() > 0) {
                    list.add(s);
                    break;
                }
                break;
            }
            else if (qidx1 >= 0 && (sidx2 == -1 || qidx1 < sidx2)) {
                if (qidx1 >= qidx2) {
                    if (qidx1 == 0) {
                        s = s.substring(qidx1 + 1);
                    }
                    else if (qidx1 > 0) {
                        s = s.substring(0, qidx1);
                    }
                    if (s.length() > 0) {
                        list.add(s);
                        break;
                    }
                    break;
                }
                else {
                    if (qidx2 < 0) {
                        s = s.substring(1);
                        list.add(s);
                        break;
                    }
                    final String tok = s.substring(qidx1 + 1, qidx2);
                    if (tok.length() > 0) {
                        list.add(tok);
                    }
                    s = s.substring(qidx2 + 1);
                }
            }
            else {
                if (sidx2 < 0) {
                    list.add(s);
                    break;
                }
                final String tok = s.substring(sidx1, sidx2);
                if (tok.length() > 0) {
                    list.add(tok);
                }
                s = s.substring(sidx2);
            }
        }
        return list;
    }
    
    public static List<String> split(final Object source, final String delimiter, final boolean trim, final boolean excludeZeroLength) {
        final ArrayList<String> list = new ArrayList<String>();
        if (source == null) {
            return list;
        }
        String sourceString = source.toString();
        final int length = delimiter.length();
        while (true) {
            final int idx = sourceString.indexOf(delimiter);
            String theString;
            if (idx < 0) {
                theString = sourceString;
            }
            else {
                theString = sourceString.substring(0, idx);
                sourceString = sourceString.substring(idx + length);
            }
            if (trim) {
                theString = theString.trim();
            }
            if (excludeZeroLength && theString.length() == 0) {
                if (idx < 0) {
                    break;
                }
                continue;
            }
            else {
                list.add(theString);
                if (idx < 0) {
                    break;
                }
                continue;
            }
        }
        return list;
    }
    
    public static String[] split(String s, final String delimiter, final int cnt) {
        final String[] a = new String[cnt];
        for (int i = 0; i < cnt - 1; ++i) {
            final int idx = s.indexOf(delimiter);
            if (idx < 0) {
                return null;
            }
            a[i] = s.substring(0, idx);
            s = s.substring(idx + 1);
        }
        a[cnt - 1] = s;
        return a;
    }
    
    public static List<String> splitUpTo(String s, final String delimiter, final int cnt) {
        final List<String> toks = new ArrayList<String>();
        for (int i = 0; i < cnt - 1; ++i) {
            final int idx = s.indexOf(delimiter);
            if (idx < 0) {
                break;
            }
            toks.add(s.substring(0, idx));
            s = s.substring(idx + 1).trim();
        }
        if (s.length() > 0) {
            toks.add(s);
        }
        return toks;
    }
    
    public static String replaceDate(final String s, final String macroName, final Date date) {
        return replaceDate(s, macroName, date, "${", "}");
    }
    
    public static String replaceDate(final String s, final String macroName, final Date date, final TimeZone timeZone) {
        return replaceDate(s, macroName, date, "${", "}", timeZone);
    }
    
    public static String replaceDate(final String s, final String macroName, final Date date, final String macroPrefix, final String macroSuffix) {
        return replaceDate(s, macroName, date, macroPrefix, macroSuffix, DateUtil.TIMEZONE_GMT);
    }
    
    public static String replaceDate(String s, final String macroName, final Date date, final String macroPrefix, final String macroSuffix, TimeZone timeZone) {
        while (true) {
            final int idx1 = s.indexOf(macroPrefix + macroName);
            if (idx1 < 0) {
                return s;
            }
            final int idx2 = s.indexOf(macroSuffix, idx1);
            if (idx2 < 0) {
                return s;
            }
            final String fullMacro = s.substring(idx1 + macroPrefix.length(), idx2);
            final String[] toks = split(fullMacro, ":", 2);
            if (toks == null || toks.length != 2) {
                throw new IllegalArgumentException("Could not find date format:" + s);
            }
            final SimpleDateFormat sdf = new SimpleDateFormat(toks[1]);
            if (timeZone == null) {
                timeZone = DateUtil.TIMEZONE_GMT;
            }
            sdf.setTimeZone(timeZone);
            final String formattedDate = sdf.format(date);
            s = s.replace(macroPrefix + fullMacro + macroSuffix, formattedDate);
        }
    }
    
    public static String[] listToStringArray(final List objectList) {
        final String[] sa = new String[objectList.size()];
        for (int i = 0; i < objectList.size(); ++i) {
            final Object o = objectList.get(i);
            if (o != null) {
                sa[i] = o.toString();
            }
        }
        return sa;
    }
    
    public static String listToString(final List l) {
        final StringBuffer sb = new StringBuffer();
        for (int i = 0; i < l.size(); ++i) {
            if (i > 0) {
                sb.append(";");
            }
            sb.append(l.get(i).toString());
        }
        return sb.toString();
    }
    
    public static List toString(final List l) {
        final List stringList = new ArrayList();
        for (int i = 0; i < l.size(); ++i) {
            stringList.add(l.get(i).toString());
        }
        return stringList;
    }
    
    public static String replace(String string, final String pattern, final String value) {
        if (pattern.length() == 0) {
            return string;
        }
        final StringBuffer returnValue = new StringBuffer();
        final int patternLength = pattern.length();
        while (true) {
            final int idx = string.indexOf(pattern);
            if (idx < 0) {
                break;
            }
            returnValue.append(string.substring(0, idx));
            if (value != null) {
                returnValue.append(value);
            }
            string = string.substring(idx + patternLength);
        }
        returnValue.append(string);
        return returnValue.toString();
    }
    
    public static String replaceList(String v, final String[] patterns, final String[] values) {
        for (int i = 0; i < patterns.length; ++i) {
            v = replace(v, patterns[i], values[i]);
        }
        return v;
    }
    
    public static String replaceList(String v, final List patterns, final List values) {
        if (patterns.size() != values.size()) {
            throw new IllegalArgumentException("Patterns list not the same size as values list");
        }
        for (int i = 0; i < patterns.size(); ++i) {
            v = replace(v, patterns.get(i), values.get(i));
        }
        return v;
    }
    
    public static List replaceList(final List sourceList, final String[] patterns, final String[] values) {
        final List result = new ArrayList();
        for (int i = 0; i < sourceList.size(); ++i) {
            String str = sourceList.get(i);
            for (int patternIdx = 0; patternIdx < patterns.length; ++patternIdx) {
                if (patterns[patternIdx] != null) {
                    str = replace(str, patterns[patternIdx], values[patternIdx]);
                }
            }
            result.add(str);
        }
        return result;
    }
    
    public static StringBuffer append(StringBuffer sb, final Object s1) {
        if (sb == null) {
            sb = new StringBuffer();
        }
        sb.append((s1 == null) ? "null" : s1.toString());
        return sb;
    }
    
    public static StringBuffer append(StringBuffer sb, final Object s1, final Object s2) {
        sb = append(sb, s1);
        sb.append((s2 == null) ? "null" : s2.toString());
        return sb;
    }
    
    public static StringBuffer append(StringBuffer sb, final Object s1, final Object s2, final Object s3) {
        sb = append(sb, s1, s2);
        sb.append((s3 == null) ? "null" : s3.toString());
        return sb;
    }
    
    public static StringBuffer append(StringBuffer sb, final Object s1, final Object s2, final Object s3, final Object s4) {
        sb = append(sb, s1, s2, s3);
        sb.append((s4 == null) ? "null" : s4.toString());
        return sb;
    }
    
    public static StringBuffer append(StringBuffer sb, final Object s1, final Object s2, final Object s3, final Object s4, final Object s5) {
        sb = append(sb, s1, s2, s3, s4);
        sb.append((s5 == null) ? "null" : s5.toString());
        return sb;
    }
    
    public static List parseCsv(String s, boolean skipFirst) {
        s = s.trim();
        s += "\n";
        s = replaceList(s, new String[] { "\"\"\",", ",\"\"\"", "\"\"" }, new String[] { "_QUOTE_\",", ",\"_QUOTE_", "_QUOTE_" });
        final int cnt = s.length();
        final ArrayList lines = new ArrayList();
        ArrayList line = new ArrayList();
        String word = "";
        final int INWORD = 0;
        final int INQUOTE = 1;
        final int LOOKING = 2;
        int state = 0;
        for (int i = 0; i < cnt; ++i) {
            final char c = s.charAt(i);
            switch (state) {
                case 2: {
                    if (c == ',') {
                        state = 0;
                        break;
                    }
                    if (c == '\n') {
                        if (line.size() > 0) {
                            if (!skipFirst && !line.get(0).toString().startsWith("#")) {
                                lines.add(line);
                            }
                            line = new ArrayList();
                            word = "";
                        }
                        state = 0;
                        break;
                    }
                    break;
                }
                case 0: {
                    if (c == '\"') {
                        state = 1;
                        break;
                    }
                    if (c == ',') {
                        line.add(replace(word, "_QUOTE_", "\""));
                        word = "";
                        break;
                    }
                    if (c == '\n') {
                        line.add(replace(word, "_QUOTE_", "\""));
                        word = "";
                        if ((!skipFirst || lines.size() > 1) && !line.get(0).toString().startsWith("#")) {
                            lines.add(line);
                        }
                        skipFirst = false;
                        line = new ArrayList();
                        break;
                    }
                    word += c;
                    break;
                }
                case 1: {
                    if (c == '\"') {
                        line.add(replace(word, "_QUOTE_", "\""));
                        word = "";
                        state = 2;
                        break;
                    }
                    word += c;
                    break;
                }
            }
        }
        if (line.size() > 0 || word.length() > 0) {
            if (word.length() > 0) {
                line.add(replace(word, "_QUOTE_", "\""));
            }
            if ((!skipFirst || lines.size() > 1) && !line.get(0).toString().startsWith("#")) {
                lines.add(line);
            }
        }
        return lines;
    }
    
    public static final String shorten(String s, final int length) {
        if (s.length() > length) {
            s = s.substring(0, length - 1) + "...";
        }
        return s;
    }
    
    public static String toString(final Object[] array) {
        final StringBuffer buf = new StringBuffer();
        buf.append(": ");
        for (int i = 0; i < array.length; ++i) {
            buf.append("[");
            buf.append(i);
            buf.append("]: ");
            buf.append((array[i] == null) ? "null" : array[i]);
            buf.append(" ");
        }
        return buf.toString();
    }
    
    public static List expandIso8601(final String time) {
        final List times = new ArrayList();
        final List tokens = split(time, ",", true, true);
        for (int i = 0; i < tokens.size(); ++i) {
            final String tok = tokens.get(i);
            if (tok.indexOf("/") < 0) {
                times.add(parseIso8601(tok));
            }
            else {
                final List ranges = split(tok, "/", true, true);
                if (ranges.size() != 3) {
                    throw new IllegalArgumentException("Invalid date format:" + tok);
                }
                final Date min = parseIso8601(ranges.get(0));
                final Date max = parseIso8601(ranges.get(1));
                final long step = parseTimePeriod(ranges.get(2));
            }
        }
        return times;
    }
    
    private static String str(final String s1, final String dflt) {
        return (s1 != null) ? s1 : dflt;
    }
    
    public static String findPattern(final String source, final String patternString) {
        final Pattern pattern = Pattern.compile(patternString);
        final Matcher matcher = pattern.matcher(source);
        final boolean ok = matcher.find();
        if (!ok) {
            return null;
        }
        if (matcher.groupCount() > 0) {
            return matcher.group(1);
        }
        return null;
    }
    
    public static boolean isUpperCase(final String s) {
        return s.toUpperCase().equals(s);
    }
    
    public static boolean isLowerCase(final String s) {
        return s.toLowerCase().equals(s);
    }
    
    public static String camelCase(final String s) {
        return s.substring(0, 1).toUpperCase() + s.substring(1).toLowerCase();
    }
    
    public static Date parseIso8601(final String time) {
        final String tmp = "((\\d\\d\\d\\d)-(\\d\\d)-(\\d\\d))?(T(\\d\\d):(\\d\\d)(:(\\d\\d)?)?(.*)?)?";
        final Pattern pattern = Pattern.compile(tmp);
        final Matcher matcher = pattern.matcher(time);
        final boolean ok = matcher.find();
        if (!ok) {
            System.err.println("No match:" + time);
            return null;
        }
        System.err.println("Time:" + time);
        for (int i = 1; i <= matcher.groupCount(); ++i) {}
        int gidx = 1;
        ++gidx;
        final String year = str(matcher.group(gidx++), "0000");
        final String month = str(matcher.group(gidx++), "01");
        final String day = str(matcher.group(gidx++), "01");
        ++gidx;
        final String hh = str(matcher.group(gidx++), "00");
        final String mm = str(matcher.group(gidx++), "00");
        ++gidx;
        final String ss = str(matcher.group(gidx++), "00");
        String tzd = str(matcher.group(gidx++), "GMT");
        if (tzd.equals("Z") || tzd.length() == 0) {
            tzd = "GMT";
        }
        final String format = "yyyy-MM-dd-HH-mm-ss-Z";
        final SimpleDateFormat sdf = new SimpleDateFormat();
        sdf.applyPattern(format);
        final String dateString = year + "-" + month + "-" + day + "-" + hh + "-" + mm + "-" + ss + "-" + tzd;
        try {
            final Date dttm = sdf.parse(dateString);
            return dttm;
        }
        catch (Exception exc) {
            System.err.println("exc:" + exc);
            return null;
        }
    }
    
    public static long parseTimePeriod(String s) {
        if (!s.startsWith("P")) {
            throw new IllegalArgumentException("Unknown time period:" + s);
        }
        s = s.substring(1);
        final String tmp = "((\\d+)Y)?((\\d+)M)?((\\d+)D)?(T((\\d+)H)?((\\d+)M)?((\\d+)S)?)";
        final Pattern pattern = Pattern.compile(tmp);
        final Matcher matcher = pattern.matcher(s);
        final boolean ok = matcher.find();
        if (!ok) {
            System.err.println("No match:" + s);
            return 0L;
        }
        System.err.println("Duration:" + s);
        for (int i = 1; i <= matcher.groupCount(); ++i) {
            System.err.println("\t" + matcher.group(i));
        }
        int gidx = 1;
        ++gidx;
        final long y = convert(str(matcher.group(gidx++), "0"));
        ++gidx;
        final long m = convert(str(matcher.group(gidx++), "0"));
        ++gidx;
        final long d = convert(str(matcher.group(gidx++), "0"));
        ++gidx;
        ++gidx;
        final long h = convert(str(matcher.group(gidx++), "0"));
        ++gidx;
        final long min = convert(str(matcher.group(gidx++), "0"));
        ++gidx;
        final long sec = convert(str(matcher.group(gidx++), "0"));
        return h * 3600L + m * 60L + sec;
    }
    
    private static long convert(final String s) {
        return new Long(s);
    }
    
    public static void main2(final String[] args) {
        for (int i = 0; i < args.length; ++i) {
            System.err.println(parseIntegerListString(args[i]));
        }
    }
    
    public static Hashtable parsePropertiesString(final String s) {
        final Hashtable properties = new Hashtable();
        if (s != null) {
            final StringTokenizer tok = new StringTokenizer(s, ";");
            while (tok.hasMoreTokens()) {
                final String nameValue = tok.nextToken();
                final int idx = nameValue.indexOf("=");
                if (idx < 0) {
                    continue;
                }
                properties.put(nameValue.substring(0, idx).trim(), nameValue.substring(idx + 1));
            }
        }
        return properties;
    }
    
    public static Hashtable parseHtmlProperties(String s) {
        final boolean debug = false;
        final Hashtable properties = new Hashtable();
        if (debug) {
            System.err.println("Source:" + s);
        }
        while (true) {
            if (debug) {
                System.err.println("S:" + s);
            }
            int idx = s.indexOf("=");
            if (idx < 0) {
                s = s.trim();
                if (s.length() > 0) {
                    if (debug) {
                        System.err.println("\tsingle name:" + s + ":");
                    }
                    properties.put(s, "");
                    break;
                }
                break;
            }
            else {
                final String name = s.substring(0, idx).trim();
                s = s.substring(idx + 1).trim();
                if (s.length() == 0) {
                    if (debug) {
                        System.err.println("\tsingle name=" + name);
                    }
                    properties.put(name, "");
                    break;
                }
                if (s.charAt(0) == '\"') {
                    s = s.substring(1);
                    idx = s.indexOf("\"");
                    if (idx < 0) {
                        properties.put(name, s);
                        break;
                    }
                    final String value = s.substring(0, idx);
                    if (debug) {
                        System.err.println("\tname=" + name);
                    }
                    if (debug) {
                        System.err.println("\tvalue=" + value);
                    }
                    properties.put(name, value);
                    s = s.substring(idx + 1);
                }
                else {
                    idx = s.indexOf(" ");
                    if (idx < 0) {
                        if (debug) {
                            System.err.println("\tname=" + name);
                        }
                        if (debug) {
                            System.err.println("\tvalue=" + s);
                        }
                        properties.put(name, s);
                        break;
                    }
                    final String value = s.substring(0, idx);
                    properties.put(name, value);
                    if (debug) {
                        System.err.println("\tname=" + name);
                    }
                    if (debug) {
                        System.err.println("\tvalue=" + value);
                    }
                    s = s.substring(idx + 1);
                }
            }
        }
        if (debug) {
            System.err.println("props:" + properties);
        }
        return properties;
    }
    
    private static void showUsage() {
        System.out.println(" StringUtil escape <string> [okChars]");
        System.out.println(" StringUtil unescape <string>");
    }
    
    public static void main(final String[] args) throws Exception {
        System.err.println(splitWithQuotes(" single  again \"hello there\" another couple of toks  \"how are you\" I am fine \"and you"));
        System.err.println(splitWithQuotes("text1 text2"));
        System.err.println(splitWithQuotes("hello"));
        System.err.println(splitWithQuotes("\"hello"));
        System.err.println(splitWithQuotes("\"hello\""));
        System.err.println(splitWithQuotes("hello\""));
    }
    
    public static void main3(final String[] args) {
        if (args.length < 2) {
            showUsage();
            return;
        }
        if (args[0].equalsIgnoreCase("escape")) {
            final String ok = (args.length > 2) ? args[2] : "";
            System.out.println(" escape(" + args[1] + "," + ok + ")= " + escape(args[1], ok));
        }
        else if (args[0].equalsIgnoreCase("unescape")) {
            System.out.println(" unescape(" + args[1] + ")= " + unescape(args[1]));
        }
        else {
            showUsage();
        }
    }
    
    public static List<String> splitMacros(String s) {
        final List<String> tokens = new ArrayList<String>();
        for (int idx1 = s.indexOf("${"); idx1 >= 0; idx1 = s.indexOf("${")) {
            final int idx2 = s.indexOf("}", idx1);
            if (idx2 < 0) {
                break;
            }
            tokens.add(s.substring(0, idx1));
            tokens.add(s.substring(idx1 + 2, idx2));
            s = s.substring(idx2 + 1);
        }
        if (s.length() > 0) {
            tokens.add(s);
        }
        return tokens;
    }
    
    public static String applyMacros(final String s, final Hashtable props, final boolean throwError) {
        final List toks = splitMacros(s);
        final StringBuffer sb = new StringBuffer("");
        boolean nextMacro = false;
        for (int i = 0; i < toks.size(); ++i) {
            final String tok = toks.get(i);
            if (nextMacro) {
                final Object obj = props.get(tok);
                final String value = (obj != null) ? obj.toString() : null;
                if (value == null) {
                    if (throwError) {
                        throw new IllegalArgumentException("Undefined macro: ${" + tok + "} in:" + s);
                    }
                    sb.append("${" + tok + "}");
                }
                else {
                    sb.append(value);
                }
            }
            else {
                sb.append(tok);
            }
            nextMacro = !nextMacro;
        }
        return sb.toString();
    }
    
    public static double[][] parseCoordinates(String coords) {
        coords = replace(coords, "\n", " ");
        while (true) {
            final String newCoords = replace(coords, " ,", ",");
            if (newCoords.equals(coords)) {
                break;
            }
            coords = newCoords;
        }
        while (true) {
            final String newCoords = replace(coords, ", ", ",");
            if (newCoords.equals(coords)) {
                break;
            }
            coords = newCoords;
        }
        final List tokens = split(coords, " ", true, true);
        double[][] result = null;
        int pointIdx = 0;
        while (pointIdx < tokens.size()) {
            final String tok = tokens.get(pointIdx);
            final List numbers = split(tok, ",");
            if (numbers.size() != 2 && numbers.size() != 3) {
                if (numbers.size() > 3 && tokens.size() == 1 && numbers.size() / 3 * 3 == numbers.size()) {
                    result = new double[3][numbers.size() / 3];
                    int cnt = 0;
                    for (int i = 0; i < numbers.size(); i += 3) {
                        result[0][cnt] = new Double(numbers.get(i).toString());
                        result[1][cnt] = new Double(numbers.get(i + 1).toString());
                        result[2][cnt] = new Double(numbers.get(i + 2).toString());
                        ++cnt;
                    }
                    return result;
                }
                throw new IllegalStateException("Bad number of coordinate values:" + numbers);
            }
            else {
                if (result == null) {
                    result = new double[numbers.size()][tokens.size()];
                }
                for (int coordIdx = 0; coordIdx < numbers.size() && coordIdx < 3; ++coordIdx) {
                    result[coordIdx][pointIdx] = new Double(numbers.get(coordIdx).toString());
                }
                ++pointIdx;
            }
        }
        return result;
    }
    
    public static String getAnOrA(final String subject) {
        final String s = subject.toLowerCase();
        if (s.startsWith("a") || s.startsWith("e") || s.startsWith("o") || s.startsWith("i") || s.startsWith("u")) {
            return "an";
        }
        return "a";
    }
    
    static {
        StringUtil.debug = false;
        ordinalNames = new String[] { "Latest", "Second", "Third", "Fourth", "Fifth", "Sixth", "Seventh", "Eighth", "Ninth", "Tenth" };
        StringUtil.htmlIn = new char[] { '&', '\"', '\'', '<', '>', '\n' };
        StringUtil.htmlOut = new String[] { "&amp;", "&quot;", "&#39;", "&lt;", "&gt;", "\n<p>" };
        StringUtil.xmlInC = new char[] { '&', '<', '>' };
        StringUtil.xmlOutC = new String[] { "&amp;", "&lt;", "&gt;" };
        StringUtil.xmlIn = new char[] { '&', '\"', '\'', '<', '>', '\r', '\n' };
        StringUtil.xmlOut = new String[] { "&amp;", "&quot;", "&apos;", "&lt;", "&gt;", "&#13;", "&#10;" };
        MATCH_MUTEX = new Object();
        StringUtil.patternCache = new Hashtable();
    }
}
