// 
// Decompiled by Procyon v0.5.36
// 

package ucar.unidata.util;

import java.util.Date;

public class DateSelectionInfo
{
    public Date start;
    public Date end;
    public int interval;
    public int roundTo;
    public int preInt;
    public int postInt;
    
    public DateSelectionInfo() {
        this.start = null;
        this.end = null;
        this.interval = 0;
        this.roundTo = 0;
        this.preInt = 0;
        this.postInt = 0;
    }
    
    public DateSelectionInfo(final Date start, final Date end, final int interval, final int roundTo, final int preInt, final int postInt) {
        this.start = start;
        this.end = end;
        this.interval = interval;
        this.roundTo = roundTo;
        this.preInt = preInt;
        this.postInt = postInt;
    }
    
    public DateSelectionInfo(final Date start, final Date end) {
        this.start = start;
        this.end = end;
        this.interval = 0;
        this.roundTo = 0;
        this.preInt = 0;
        this.postInt = 0;
    }
    
    @Override
    public boolean equals(final Object oo) {
        return this == oo || (!(oo instanceof DateSelectionInfo) && false);
    }
    
    public Date setStartDateInfo() {
        return this.start;
    }
    
    public Date setEndDateInfo() {
        return this.end;
    }
}
