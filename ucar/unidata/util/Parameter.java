// 
// Decompiled by Procyon v0.5.36
// 

package ucar.unidata.util;

import java.io.Serializable;

public class Parameter implements Serializable
{
    private String name;
    private String valueS;
    private double[] valueD;
    private boolean isString;
    private volatile int hashCode;
    
    public String getName() {
        return this.name;
    }
    
    public boolean isString() {
        return this.isString;
    }
    
    public String getStringValue() {
        if (this.valueS == null) {
            final StringBuilder sbuff = new StringBuilder();
            for (int i = 0; i < this.valueD.length; ++i) {
                final double v = this.valueD[i];
                sbuff.append(v + " ");
            }
            this.valueS = sbuff.toString();
        }
        return this.valueS;
    }
    
    public double getNumericValue() {
        return this.valueD[0];
    }
    
    public double getNumericValue(final int i) {
        return this.valueD[i];
    }
    
    public int getLength() {
        return this.valueD.length;
    }
    
    public double[] getNumericValues() {
        return this.valueD;
    }
    
    @Override
    public boolean equals(final Object oo) {
        return this == oo || (oo instanceof Parameter && this.hashCode() == oo.hashCode());
    }
    
    @Override
    public int hashCode() {
        if (this.hashCode == 0) {
            int result = 17;
            result = 37 * result + this.getName().hashCode();
            if (this.valueS != null) {
                result = 37 * result + this.getStringValue().hashCode();
            }
            if (this.valueD != null) {
                for (int i = 0; i < this.valueD.length; ++i) {
                    result += (int)(1000.0 * this.valueD[i]);
                }
            }
            this.hashCode = result;
        }
        return this.hashCode;
    }
    
    @Override
    public String toString() {
        final StringBuilder buff = new StringBuilder();
        buff.append(this.getName());
        if (this.isString()) {
            buff.append(" = ");
            buff.append(this.valueS);
        }
        else {
            buff.append(" = ");
            for (int i = 0; i < this.getLength(); ++i) {
                if (i != 0) {
                    buff.append(", ");
                }
                buff.append(this.getNumericValue(i));
            }
        }
        return buff.toString();
    }
    
    public Parameter(final String name, final Parameter from) {
        this.hashCode = 0;
        this.name = name;
        this.valueS = from.valueS;
        this.valueD = from.valueD;
        this.isString = from.isString;
    }
    
    public Parameter(final String name, final String val) {
        this.hashCode = 0;
        this.name = name;
        this.valueS = val;
        this.isString = true;
    }
    
    public Parameter(final String name, final double value) {
        this.hashCode = 0;
        this.name = name;
        (this.valueD = new double[1])[0] = value;
    }
    
    public Parameter(final String name, final double[] value) {
        this.hashCode = 0;
        this.name = name;
        this.valueD = value.clone();
    }
}
