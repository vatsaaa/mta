// 
// Decompiled by Procyon v0.5.36
// 

package ucar.unidata.util;

public class GaussianLatitudes
{
    private static final double XLIM = 1.0E-7;
    public double[] cosc;
    public double[] colat;
    public double[] gaussw;
    public double[] latd;
    public double[] sinc;
    public double[] wos2;
    
    public GaussianLatitudes(final int nlat) {
        final int nzero = nlat / 2;
        this.cosc = new double[nlat];
        this.colat = new double[nlat];
        this.gaussw = new double[nlat];
        this.latd = new double[nlat];
        this.sinc = new double[nlat];
        this.wos2 = new double[nlat];
        for (int i = 0; i < nzero; ++i) {
            this.cosc[i] = Math.sin((i + 0.5) * 3.141592653589793 / nlat + 1.5707963267948966);
        }
        final double FI = nlat;
        final double FI2 = nlat + 1.0;
        final double A = FI * FI2 / Math.sqrt(4.0 * FI2 * FI2 - 1.0);
        final double B = FI * FI2 / Math.sqrt(4.0 * FI * FI - 1.0);
        for (int j = 0; j < nzero; ++j) {
            int countIterations = 0;
            double delta;
            do {
                final double G = this.lgord(this.cosc[j], nlat);
                final double GM = this.lgord(this.cosc[j], nlat - 1);
                final double GP = this.lgord(this.cosc[j], nlat + 1);
                final double GT = (this.cosc[j] * this.cosc[j] - 1.0) / (A * GP - B * GM);
                delta = G * GT;
                final double[] cosc = this.cosc;
                final int n = j;
                cosc[n] -= delta;
                ++countIterations;
            } while (Math.abs(delta) > 1.0E-7);
            final double C = 2.0 * (1.0 - this.cosc[j] * this.cosc[j]);
            double D = this.lgord(this.cosc[j], nlat - 1);
            D = D * D * FI * FI;
            this.gaussw[j] = C * (FI - 0.5) / D;
        }
        for (int j = 0; j < nzero; ++j) {
            this.colat[j] = Math.acos(this.cosc[j]);
            this.sinc[j] = Math.sin(this.colat[j]);
            this.wos2[j] = this.gaussw[j] / (this.sinc[j] * this.sinc[j]);
        }
        int next = nzero;
        if (nlat % 2 != 0) {
            this.cosc[next] = 0.0;
            final double C2 = 2.0;
            double D2 = this.lgord(this.cosc[next], nlat - 1);
            D2 = D2 * D2 * FI * FI;
            this.gaussw[next] = C2 * (FI - 0.5) / D2;
            this.colat[next] = 1.5707963267948966;
            this.sinc[next] = 1.0;
            this.wos2[next] = this.gaussw[next];
            ++next;
        }
        for (int k = next; k < nlat; ++k) {
            this.cosc[k] = -this.cosc[nlat - k - 1];
            this.gaussw[k] = this.gaussw[nlat - k - 1];
            this.colat[k] = 3.141592653589793 - this.colat[nlat - k - 1];
            this.sinc[k] = this.sinc[nlat - k - 1];
            this.wos2[k] = this.wos2[nlat - k - 1];
        }
        for (int k = 0; k < nlat; ++k) {
            this.latd[k] = Math.toDegrees(1.5707963267948966 - this.colat[k]);
        }
    }
    
    private double lgord(final double cosc, final int nlat) {
        final double colat = Math.acos(cosc);
        double c = Math.sqrt(2.0);
        for (int k = 1; k <= nlat; ++k) {
            c *= Math.sqrt(1.0 - 1.0 / (4 * k * k));
        }
        final double FN = nlat;
        double ANG = FN * colat;
        double S1 = 0.0;
        double C4 = 1.0;
        double A = -1.0;
        double B = 0.0;
        for (int i = 0; i <= nlat; i += 2) {
            if (i == nlat) {
                C4 *= 0.5;
            }
            S1 += C4 * Math.cos(ANG);
            A += 2.0;
            ++B;
            final double FK = i;
            ANG = colat * (FN - FK - 2.0);
            C4 *= A * (FN - B + 1.0) / (B * (FN + FN - A));
        }
        return S1 * c;
    }
    
    public static void main(final String[] args) {
        final int nlats = 512;
        final GaussianLatitudes glat = new GaussianLatitudes(nlats);
        for (int i = 0; i < nlats - 1; ++i) {
            System.out.print(" lat " + i + " = " + Format.dfrac(glat.latd[i], 4));
            System.out.print(" diff " + i + " = " + (glat.latd[i + 1] - glat.latd[i]));
            System.out.println(" weight= " + Format.dfrac(glat.gaussw[i], 6));
        }
    }
}
