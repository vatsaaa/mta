// 
// Decompiled by Procyon v0.5.36
// 

package ucar.unidata.util;

public class Format
{
    public static void tab(final StringBuffer sbuff, final int tabStop, final boolean alwaysOne) {
        final int len = sbuff.length();
        if (tabStop > len) {
            sbuff.setLength(tabStop);
            for (int i = len; i < tabStop; ++i) {
                sbuff.setCharAt(i, ' ');
            }
        }
        else if (alwaysOne) {
            sbuff.setLength(len + 1);
            sbuff.setCharAt(len, ' ');
        }
    }
    
    public static void tab(final StringBuilder sbuff, final int tabStop, final boolean alwaysOne) {
        final int len = sbuff.length();
        if (tabStop > len) {
            sbuff.setLength(tabStop);
            for (int i = len; i < tabStop; ++i) {
                sbuff.setCharAt(i, ' ');
            }
        }
        else if (alwaysOne) {
            sbuff.setLength(len + 1);
            sbuff.setCharAt(len, ' ');
        }
    }
    
    public static String s(final String s, final int width) {
        return pad(s, width, false);
    }
    
    public static String pad(final String s, final int width, final boolean rightJustify) {
        if (s.length() >= width) {
            return s;
        }
        final StringBuilder sbuff = new StringBuilder(width);
        final int need = width - s.length();
        sbuff.setLength(need);
        for (int i = 0; i < need; ++i) {
            sbuff.setCharAt(i, ' ');
        }
        if (rightJustify) {
            sbuff.append(s);
        }
        else {
            sbuff.insert(0, s);
        }
        return sbuff.toString();
    }
    
    public static String i(final int v, final int width) {
        return pad(Integer.toString(v), width, true);
    }
    
    public static String l(final long v, final int width) {
        return pad(Long.toString(v), width, true);
    }
    
    public static String d(final double d, final int min_sigfig) {
        return formatDouble(d, min_sigfig, -1).trim();
    }
    
    public static String d(final double d, final int min_sigfig, final int width) {
        final String s = d(d, min_sigfig);
        return pad(s, width, true);
    }
    
    public static String dfrac(final double d, final int fixed_decimals) {
        return formatDouble(d, 100, fixed_decimals).trim();
    }
    
    public static String formatDouble(final double d, final int min_sigFigs, final int fixed_decimals) {
        final String s = Double.toString(d);
        String sign;
        String unsigned;
        if (s.startsWith("-") || s.startsWith("+")) {
            sign = s.substring(0, 1);
            unsigned = s.substring(1);
        }
        else {
            sign = "";
            unsigned = s;
        }
        int eInd = unsigned.indexOf(69);
        if (eInd == -1) {
            eInd = unsigned.indexOf(101);
        }
        String mantissa;
        String exponent;
        if (eInd == -1) {
            mantissa = unsigned;
            exponent = "";
        }
        else {
            mantissa = unsigned.substring(0, eInd);
            exponent = unsigned.substring(eInd);
        }
        final int dotInd = mantissa.indexOf(46);
        StringBuilder number;
        StringBuilder fraction;
        if (dotInd == -1) {
            number = new StringBuilder(mantissa);
            fraction = new StringBuilder("");
        }
        else {
            number = new StringBuilder(mantissa.substring(0, dotInd));
            fraction = new StringBuilder(mantissa.substring(dotInd + 1));
        }
        int numFigs = number.length();
        int fracFigs = fraction.length();
        if (fixed_decimals != -1) {
            if (fixed_decimals == 0) {
                fraction.setLength(0);
            }
            else if (fixed_decimals > fracFigs) {
                for (int want = fixed_decimals - fracFigs, i = 0; i < want; ++i) {
                    fraction.append("0");
                }
            }
            else if (fixed_decimals < fracFigs) {
                final int chop = fracFigs - fixed_decimals;
                fraction.setLength(fraction.length() - chop);
            }
            fracFigs = fixed_decimals;
        }
        else {
            if ((numFigs == 0 || number.toString().equals("0")) && fracFigs > 0) {
                numFigs = 0;
                number = new StringBuilder("");
                for (int j = 0; j < fraction.length(); ++j) {
                    if (fraction.charAt(j) != '0') {
                        break;
                    }
                    --fracFigs;
                }
            }
            if (fracFigs == 0 && numFigs > 0) {
                for (int j = number.length() - 1; j > 0; --j) {
                    if (number.charAt(j) != '0') {
                        break;
                    }
                    --numFigs;
                }
            }
            final int sigFigs = numFigs + fracFigs;
            if (sigFigs > min_sigFigs) {
                final int chop2 = Math.min(sigFigs - min_sigFigs, fracFigs);
                fraction.setLength(fraction.length() - chop2);
                fracFigs -= chop2;
            }
        }
        if (fraction.length() == 0) {
            return sign + (Object)number + exponent;
        }
        return sign + (Object)number + "." + (Object)fraction + exponent;
    }
    
    public static String formatByteSize(double size) {
        String unit = null;
        if (size > 1.0E15) {
            unit = "Pbytes";
            size *= 1.0E-15;
        }
        else if (size > 1.0E12) {
            unit = "Tbytes";
            size *= 1.0E-12;
        }
        else if (size > 1.0E9) {
            unit = "Gbytes";
            size *= 1.0E-9;
        }
        else if (size > 1000000.0) {
            unit = "Mbytes";
            size *= 1.0E-6;
        }
        else if (size > 1000.0) {
            unit = "Kbytes";
            size *= 0.001;
        }
        else {
            unit = "bytes";
        }
        return d(size, 4) + " " + unit;
    }
    
    private static void show(final double d, final int sigfig) {
        System.out.println("Format.d(" + d + "," + sigfig + ") == " + d(d, sigfig));
    }
    
    private static void show2(final double d, final int dec_places) {
        System.out.println("Format.dfrac(" + d + "," + dec_places + ") == " + dfrac(d, dec_places));
    }
    
    public static void main(final String[] argv) {
        show(1.2345678E-6, 3);
    }
}
