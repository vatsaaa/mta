// 
// Decompiled by Procyon v0.5.36
// 

package ucar.unidata.util;

import java.util.Enumeration;
import java.util.ArrayList;
import java.text.ParseException;
import java.util.Hashtable;
import java.util.Date;
import java.util.List;
import java.text.SimpleDateFormat;

public class DateSelection
{
    private static SimpleDateFormat sdf;
    private static final String ARG_PREFIX = "dateselection.";
    public static final String ARG_STARTMODE = "dateselection.start_mode";
    public static final String ARG_ENDMODE = "dateselection.end_mode";
    public static final String ARG_STARTFIXEDTIME = "dateselection.start_fixedtime";
    public static final String ARG_ENDFIXEDTIME = "dateselection.end_fixedtime";
    public static final String ARG_STARTOFFSET = "dateselection.start_offset";
    public static final String ARG_ENDOFFSET = "dateselection.end_offset";
    public static final String ARG_POSTRANGE = "dateselection.postrange";
    public static final String ARG_PRERANGE = "dateselection.prerange";
    public static final String ARG_INTERVAL = "dateselection.interval";
    public static final String ARG_ROUNDTO = "dateselection.roundto";
    public static final String ARG_COUNT = "dateselection.count";
    public static final String ARG_SKIP = "dateselection.skip";
    public boolean debug;
    public static final int TIMEMODE_FIXED = 0;
    public static final int TIMEMODE_CURRENT = 1;
    public static final int TIMEMODE_RELATIVE = 2;
    public static final int TIMEMODE_DATA = 3;
    public static String[] TIMEMODESTRINGS;
    public static int[] TIMEMODES;
    public static String[] STARTMODELABELS;
    public static String[] ENDMODELABELS;
    private int startMode;
    private int endMode;
    private long startFixedTime;
    private long endFixedTime;
    private double startOffset;
    private double endOffset;
    private int skip;
    private double preRange;
    private double postRange;
    private double interval;
    private double roundTo;
    private int count;
    public static final int MAX_COUNT = Integer.MAX_VALUE;
    private int numTimesInRange;
    private List times;
    private boolean doLatest;
    private boolean doAll;
    private Date nowTime;
    
    public DateSelection() {
        this.debug = false;
        this.startMode = 0;
        this.endMode = 0;
        this.startFixedTime = Long.MAX_VALUE;
        this.endFixedTime = Long.MAX_VALUE;
        this.startOffset = 0.0;
        this.endOffset = 0.0;
        this.skip = 0;
        this.preRange = Double.NaN;
        this.postRange = Double.NaN;
        this.interval = Double.NaN;
        this.roundTo = 0.0;
        this.count = Integer.MAX_VALUE;
        this.numTimesInRange = 1;
        this.doLatest = false;
        this.doAll = false;
    }
    
    public DateSelection(final Hashtable<String, String> args) {
        this.debug = false;
        this.startMode = 0;
        this.endMode = 0;
        this.startFixedTime = Long.MAX_VALUE;
        this.endFixedTime = Long.MAX_VALUE;
        this.startOffset = 0.0;
        this.endOffset = 0.0;
        this.skip = 0;
        this.preRange = Double.NaN;
        this.postRange = Double.NaN;
        this.interval = Double.NaN;
        this.roundTo = 0.0;
        this.count = Integer.MAX_VALUE;
        this.numTimesInRange = 1;
        this.doLatest = false;
        this.doAll = false;
        String s = args.get("dateselection.start_mode");
        if (s != null) {
            boolean ok = false;
            for (int i = 0; i < DateSelection.TIMEMODESTRINGS.length; ++i) {
                if (DateSelection.TIMEMODESTRINGS[i].equals(s)) {
                    this.startMode = DateSelection.TIMEMODES[i];
                    ok = true;
                    break;
                }
            }
            if (!ok) {
                throw new IllegalArgumentException("Unknown dateselection.start_mode argument:" + s);
            }
        }
        s = args.get("dateselection.end_mode");
        if (s != null) {
            boolean ok = false;
            for (int i = 0; i < DateSelection.TIMEMODESTRINGS.length; ++i) {
                if (DateSelection.TIMEMODESTRINGS[i].equals(s)) {
                    this.endMode = DateSelection.TIMEMODES[i];
                    ok = true;
                    break;
                }
            }
            if (!ok) {
                throw new IllegalArgumentException("Unknown dateselection.end_mode argument:" + s);
            }
        }
        s = args.get("dateselection.start_fixedtime");
        if (s != null) {
            try {
                this.startFixedTime = this.parseDate(s).getTime();
            }
            catch (ParseException pe) {
                throw new IllegalArgumentException("Incorrect date format for argument dateselection.start_fixedtime " + s);
            }
        }
        s = args.get("dateselection.end_fixedtime");
        if (s != null) {
            try {
                this.endFixedTime = this.parseDate(s).getTime();
            }
            catch (ParseException pe) {
                throw new IllegalArgumentException("Incorrect date format for argument dateselection.end_fixedtime " + s);
            }
        }
        this.roundTo = this.toDouble(args.get("dateselection.roundto"), this.roundTo);
        this.interval = this.toDouble(args.get("dateselection.interval"), this.interval);
        this.postRange = this.toDouble(args.get("dateselection.postrange"), this.postRange);
        this.preRange = this.toDouble(args.get("dateselection.prerange"), this.preRange);
        this.startOffset = this.toDouble(args.get("dateselection.start_offset"), this.startOffset);
        this.endOffset = this.toDouble(args.get("dateselection.end_offset"), this.endOffset);
        this.skip = (int)this.toDouble(args.get("dateselection.skip"), this.skip);
        this.count = (int)this.toDouble(args.get("dateselection.count"), this.count);
    }
    
    private double toDouble(final String s, final double dflt) {
        if (s == null) {
            return dflt;
        }
        return new Double(s);
    }
    
    public DateSelection(final boolean doLatest, final int count) {
        this.debug = false;
        this.startMode = 0;
        this.endMode = 0;
        this.startFixedTime = Long.MAX_VALUE;
        this.endFixedTime = Long.MAX_VALUE;
        this.startOffset = 0.0;
        this.endOffset = 0.0;
        this.skip = 0;
        this.preRange = Double.NaN;
        this.postRange = Double.NaN;
        this.interval = Double.NaN;
        this.roundTo = 0.0;
        this.count = Integer.MAX_VALUE;
        this.numTimesInRange = 1;
        this.doLatest = false;
        this.doAll = false;
        this.doLatest = doLatest;
        this.count = count;
    }
    
    public DateSelection(final boolean doAll) {
        this.debug = false;
        this.startMode = 0;
        this.endMode = 0;
        this.startFixedTime = Long.MAX_VALUE;
        this.endFixedTime = Long.MAX_VALUE;
        this.startOffset = 0.0;
        this.endOffset = 0.0;
        this.skip = 0;
        this.preRange = Double.NaN;
        this.postRange = Double.NaN;
        this.interval = Double.NaN;
        this.roundTo = 0.0;
        this.count = Integer.MAX_VALUE;
        this.numTimesInRange = 1;
        this.doLatest = false;
        this.doAll = false;
        this.doAll = doAll;
    }
    
    public DateSelection(final int startMode, final double startOffset, final int endMode, final double endOffset) {
        this.debug = false;
        this.startMode = 0;
        this.endMode = 0;
        this.startFixedTime = Long.MAX_VALUE;
        this.endFixedTime = Long.MAX_VALUE;
        this.startOffset = 0.0;
        this.endOffset = 0.0;
        this.skip = 0;
        this.preRange = Double.NaN;
        this.postRange = Double.NaN;
        this.interval = Double.NaN;
        this.roundTo = 0.0;
        this.count = Integer.MAX_VALUE;
        this.numTimesInRange = 1;
        this.doLatest = false;
        this.doAll = false;
        this.startMode = startMode;
        this.startOffset = startOffset;
        this.endMode = endMode;
        this.endOffset = endOffset;
    }
    
    public DateSelection(final Date startTime, final Date endTime) {
        this.debug = false;
        this.startMode = 0;
        this.endMode = 0;
        this.startFixedTime = Long.MAX_VALUE;
        this.endFixedTime = Long.MAX_VALUE;
        this.startOffset = 0.0;
        this.endOffset = 0.0;
        this.skip = 0;
        this.preRange = Double.NaN;
        this.postRange = Double.NaN;
        this.interval = Double.NaN;
        this.roundTo = 0.0;
        this.count = Integer.MAX_VALUE;
        this.numTimesInRange = 1;
        this.doLatest = false;
        this.doAll = false;
        if (startTime != null) {
            this.startFixedTime = startTime.getTime();
        }
        if (endTime != null) {
            this.endFixedTime = endTime.getTime();
        }
        this.startMode = 0;
        this.endMode = 0;
        this.interval = 0.0;
    }
    
    public DateSelection(final DateSelection that) {
        this.debug = false;
        this.startMode = 0;
        this.endMode = 0;
        this.startFixedTime = Long.MAX_VALUE;
        this.endFixedTime = Long.MAX_VALUE;
        this.startOffset = 0.0;
        this.endOffset = 0.0;
        this.skip = 0;
        this.preRange = Double.NaN;
        this.postRange = Double.NaN;
        this.interval = Double.NaN;
        this.roundTo = 0.0;
        this.count = Integer.MAX_VALUE;
        this.numTimesInRange = 1;
        this.doLatest = false;
        this.doAll = false;
        this.startMode = that.startMode;
        this.endMode = that.endMode;
        this.startFixedTime = that.startFixedTime;
        this.endFixedTime = that.endFixedTime;
        this.doLatest = that.doLatest;
        this.nowTime = that.nowTime;
        this.startOffset = that.startOffset;
        this.endOffset = that.endOffset;
        this.skip = that.skip;
        this.postRange = that.postRange;
        this.preRange = that.preRange;
        this.interval = that.interval;
        this.roundTo = that.roundTo;
        this.count = that.count;
    }
    
    public double[] getIntervalTicks() {
        final Date[] range = this.getRange();
        final long startTime = range[0].getTime();
        final long endTime = range[1].getTime();
        final double tickStartTime = startTime - this.interval;
        final double tickEndTime = endTime + this.interval;
        final double base = this.round(tickEndTime);
        return computeTicks(tickEndTime, tickStartTime, base, this.interval);
    }
    
    public List apply(final List times) {
        final List<DatedThing> datedThings = (List<DatedThing>)DatedObject.sort(times, false);
        final List result = new ArrayList();
        final Date[] range = this.getRange(datedThings);
        final long startTime = range[0].getTime();
        final long endTime = range[1].getTime();
        final boolean hasInterval = this.hasInterval();
        final double beforeRange = this.getPreRangeToUse();
        final double afterRange = this.getPostRangeToUse();
        if (this.debug) {
            System.err.println("range:" + range[0] + " -- " + range[1]);
        }
        double[] ticks = null;
        if (hasInterval) {
            ticks = this.getIntervalTicks();
            if (ticks == null) {
                return result;
            }
            for (int i = 0; i < ticks.length; ++i) {
                if (this.debug) {
                    System.err.println("Interval " + i + ": " + new Date((long)(ticks[i] - beforeRange)) + " -- " + new Date((long)ticks[i]) + " -- " + new Date((long)(ticks[i] + afterRange)));
                }
            }
        }
        int totalThings = 0;
        DatedThing[] closest = null;
        double[] minDistance = null;
        int currentInterval = 0;
        if (ticks != null) {
            closest = new DatedThing[ticks.length];
            minDistance = new double[ticks.length];
            currentInterval = ticks.length - 1;
        }
        int skipCnt = 0;
        for (int j = 0; j < datedThings.size(); ++j) {
            if (hasInterval && totalThings >= this.count) {
                if (this.skip <= 0) {
                    break;
                }
                if (totalThings / (this.skip + 1) >= this.count) {
                    break;
                }
            }
            final DatedThing datedThing = datedThings.get(j);
            final long time = datedThing.getDate().getTime();
            if (time > endTime) {
                if (this.debug) {
                    System.err.println("after range:" + datedThing);
                }
            }
            else if (time < startTime) {
                if (this.debug) {
                    System.err.println("before range:" + datedThing);
                    break;
                }
                break;
            }
            else if (!hasInterval) {
                if (this.skip == 0) {
                    result.add(datedThing);
                }
                else {
                    if (skipCnt == 0) {
                        result.add(datedThing);
                    }
                    if (++skipCnt >= this.skip + 1) {
                        skipCnt = 0;
                    }
                }
                if (result.size() >= this.count) {
                    break;
                }
            }
            else {
                while (currentInterval >= 0 && ticks[currentInterval] - beforeRange > time) {
                    --currentInterval;
                }
                if (currentInterval < 0) {
                    break;
                }
                final boolean thingInInterval = time >= ticks[currentInterval] - beforeRange && time <= ticks[currentInterval] + afterRange;
                if (!thingInInterval) {
                    if (this.debug) {
                        System.err.println("Not in interval:" + datedThing);
                    }
                }
                else {
                    final double distance = Math.abs(time - ticks[currentInterval]);
                    if (closest[currentInterval] == null || distance < minDistance[currentInterval]) {
                        if (closest[currentInterval] == null) {
                            ++totalThings;
                        }
                        closest[currentInterval] = datedThing;
                        minDistance[currentInterval] = distance;
                    }
                }
            }
        }
        if (closest != null) {
            skipCnt = 0;
            for (int j = closest.length - 1; j >= 0; --j) {
                final DatedThing datedThing = closest[j];
                if (datedThing != null) {
                    if (this.skip == 0) {
                        result.add(datedThing);
                    }
                    else {
                        if (skipCnt == 0) {
                            result.add(datedThing);
                        }
                        if (++skipCnt >= this.skip + 1) {
                            skipCnt = 0;
                        }
                    }
                }
            }
        }
        return result;
    }
    
    private static double[] computeTicks(final double high, final double low, final double base, final double interval) {
        double[] vals = null;
        final long nlo = Math.round(Math.ceil((low - base) / Math.abs(interval)));
        final long nhi = Math.round(Math.floor((high - base) / Math.abs(interval)));
        final int numc = (int)(nhi - nlo) + 1;
        if (numc < 1) {
            return vals;
        }
        vals = new double[numc];
        for (int i = 0; i < numc; ++i) {
            vals[i] = base + (nlo + i) * interval;
        }
        return vals;
    }
    
    public Date[] getRange() {
        return this.getRange(null);
    }
    
    public Date[] getRange(final List<DatedThing> dataTimes) {
        if (this.doLatest) {
            return null;
        }
        if (this.doAll) {
            return null;
        }
        final double now = (double)((this.nowTime != null) ? this.nowTime.getTime() : System.currentTimeMillis());
        double start = 0.0;
        double end = 0.0;
        if (this.startMode == 1) {
            start = now;
        }
        else if (this.startMode == 0) {
            start = (double)this.startFixedTime;
        }
        else if (this.startMode == 3 && dataTimes != null) {
            start = (double)dataTimes.get(dataTimes.size() - 1).getDate().getTime();
        }
        if (this.endMode == 1) {
            end = now;
        }
        else if (this.endMode == 0) {
            end = (double)this.endFixedTime;
        }
        else if (this.startMode == 3 && dataTimes != null) {
            end = (double)dataTimes.get(0).getDate().getTime();
        }
        if (this.startMode != 2) {
            start += this.startOffset;
        }
        if (this.endMode != 2) {
            end += this.endOffset;
        }
        if (this.startMode == 2) {
            start = end + this.startOffset;
        }
        if (this.endMode == 2) {
            end = start + this.endOffset;
        }
        final Date startDate = new Date((long)start);
        final Date endDate = new Date((long)end);
        return new Date[] { startDate, endDate };
    }
    
    private double round(final double milliSeconds) {
        return roundTo(this.roundTo, milliSeconds);
    }
    
    public static double roundTo(final double roundTo, final double milliSeconds) {
        if (roundTo == 0.0) {
            return milliSeconds;
        }
        final double seconds = milliSeconds / 1000.0;
        final double rtSeconds = roundTo / 1000.0;
        return 1000.0 * (seconds - (int)seconds % rtSeconds);
    }
    
    protected Object makeTimeSet() {
        return null;
    }
    
    public void setStartMode(final int value) {
        this.startMode = value;
    }
    
    public int getStartMode() {
        return this.startMode;
    }
    
    public void setEndMode(final int value) {
        this.endMode = value;
    }
    
    public int getEndMode() {
        return this.endMode;
    }
    
    public boolean hasInterval() {
        return this.interval == this.interval && this.interval > 0.0;
    }
    
    public boolean hasPreRange() {
        return this.preRange == this.preRange;
    }
    
    public boolean hasPostRange() {
        return this.postRange == this.postRange;
    }
    
    public void setInterval(final double value) {
        this.interval = value;
    }
    
    public double getInterval() {
        return this.interval;
    }
    
    public void setStartOffset(final double value) {
        this.startOffset = value;
    }
    
    public double getStartOffset() {
        return this.startOffset;
    }
    
    public void setEndOffset(final double value) {
        this.endOffset = value;
    }
    
    public double getEndOffset() {
        return this.endOffset;
    }
    
    public void setRoundTo(final double value) {
        this.roundTo = value;
    }
    
    public double getRoundTo() {
        return this.roundTo;
    }
    
    public void setStartFixedTime(final long value) {
        this.startFixedTime = value;
    }
    
    public void setStartFixedTime(final Date d) {
        this.startFixedTime = d.getTime();
        this.startMode = 0;
    }
    
    public void setEndFixedTime(final Date d) {
        this.endFixedTime = d.getTime();
        this.endMode = 0;
    }
    
    public Date getStartFixedDate() {
        return new Date(this.getStartFixedTime());
    }
    
    public Date getEndFixedDate() {
        return new Date(this.getEndFixedTime());
    }
    
    public long getStartFixedTime() {
        return this.startFixedTime;
    }
    
    public void setEndFixedTime(final long value) {
        this.endFixedTime = value;
    }
    
    public long getEndFixedTime() {
        return this.endFixedTime;
    }
    
    public void setIntervalRange(final double value) {
        this.setPreRange(value / 2.0);
        this.setPostRange(value / 2.0);
    }
    
    public void setPreRange(final double value) {
        this.preRange = value;
    }
    
    public double getPreRangeToUse() {
        return this.hasPreRange() ? this.preRange : (this.interval / 2.0);
    }
    
    public double getPostRangeToUse() {
        return this.hasPostRange() ? this.postRange : (this.interval / 2.0);
    }
    
    public double getPreRange() {
        return this.preRange;
    }
    
    public void setPostRange(final double value) {
        this.postRange = value;
    }
    
    public double getPostRange() {
        return this.postRange;
    }
    
    public void setCount(final int value) {
        this.count = value;
    }
    
    public boolean hasCount() {
        return this.count != Integer.MAX_VALUE;
    }
    
    public int getCount() {
        return this.count;
    }
    
    public void setNumTimesInRange(final int value) {
        this.numTimesInRange = value;
    }
    
    public int getNumTimesInRange() {
        return this.numTimesInRange;
    }
    
    public void setTimes(final List value) {
        this.times = value;
    }
    
    public List getTimes() {
        return this.times;
    }
    
    @Override
    public int hashCode() {
        int hashCode = 0;
        if (this.times != null) {
            hashCode ^= this.times.hashCode();
        }
        return hashCode ^ new Double(this.startMode).hashCode() ^ new Double(this.endMode).hashCode() ^ new Double((double)this.startFixedTime).hashCode() ^ new Double((double)this.endFixedTime).hashCode() ^ new Double(this.startOffset).hashCode() ^ new Double(this.endOffset).hashCode() ^ new Double(this.skip).hashCode() ^ new Double(this.postRange).hashCode() ^ new Double(this.preRange).hashCode() ^ new Double(this.interval).hashCode() ^ new Double(this.roundTo).hashCode() ^ this.numTimesInRange ^ this.count;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (!(o instanceof DateSelection)) {
            return false;
        }
        final DateSelection that = (DateSelection)o;
        return this.times == that.times && (this.times == null || this.times.equals(that.times)) && this.startMode == that.startMode && this.endMode == that.endMode && this.startFixedTime == that.startFixedTime && this.endFixedTime == that.endFixedTime && this.startOffset == that.startOffset && this.endOffset == that.endOffset && this.skip == that.skip && this.postRange == that.postRange && this.preRange == that.preRange && this.interval == that.interval && this.roundTo == that.roundTo && this.numTimesInRange == that.numTimesInRange && this.count == that.count;
    }
    
    public void setSkip(final int value) {
        this.skip = value;
    }
    
    public int getSkip() {
        return this.skip;
    }
    
    private void testRange(final String msg) {
        final Date[] range = this.getRange();
        if (msg != null) {
            System.err.println(msg);
        }
        System.err.println(range[0] + " --  " + range[1]);
    }
    
    public void setDoLatest(final boolean value) {
        this.doLatest = value;
    }
    
    public boolean isLatest() {
        return this.doLatest;
    }
    
    public boolean getDoLatest() {
        return this.doLatest;
    }
    
    public void setDoAll(final boolean value) {
        this.doAll = value;
    }
    
    public boolean isAll() {
        return this.doAll;
    }
    
    public boolean getDoAll() {
        return this.doAll;
    }
    
    public void setNowTime(final Date value) {
        this.nowTime = value;
    }
    
    public Date getNowTime() {
        return this.nowTime;
    }
    
    public static void main(final String[] cmdLineArgs) throws Exception {
        final DateSelection dateSelection = new DateSelection();
        List dates = new ArrayList();
        final long now = dateSelection.parseDate(dateSelection.formatDate(new Date(System.currentTimeMillis()))).getTime();
        for (int i = 0; i < 20; ++i) {
            dates.add(new DatedObject(new Date(now + DateUtil.minutesToMillis(20.0) - i * 10 * 60 * 1000)));
        }
        dateSelection.setEndMode(0);
        dateSelection.setEndFixedTime(now);
        dateSelection.setStartMode(2);
        dateSelection.setStartOffset((double)DateUtil.hoursToMillis(-2.0));
        dateSelection.setRoundTo((double)DateUtil.hoursToMillis(12.0));
        dateSelection.setInterval((double)DateUtil.minutesToMillis(15.0));
        dateSelection.setIntervalRange((double)DateUtil.minutesToMillis(6.0));
        dates = dateSelection.apply(dates);
        final Hashtable<String, String> args = new Hashtable<String, String>();
        dateSelection.getUrlArgs(args);
        System.err.println("url string:" + dateSelection.toUrlString());
        final DateSelection dateSelection2 = new DateSelection(args);
        if (!dateSelection.equals(dateSelection2)) {
            System.err.println("date selection 2 != date selection");
            System.err.println(dateSelection);
            System.err.println(dateSelection2);
        }
        else {
            System.err.println("date selection 2 == date selection");
        }
    }
    
    @Override
    public String toString() {
        return " startMode      =" + this.startMode + "\n" + " endMode        =" + this.endMode + "\n" + " startFixedTime =" + this.startFixedTime + "\n" + " endFixedTime   =" + this.endFixedTime + "\n" + " startOffset    =" + this.startOffset + "\n" + " endOffset      =" + this.endOffset + "\n" + " postRange      =" + this.postRange + "\n" + " preRange       =" + this.preRange + "\n" + " interval       =" + this.interval + "\n" + " roundTo        =" + this.roundTo + "\n" + " count          =" + this.count + "\n";
    }
    
    private static String urlArg(final String name, final String value) {
        return "&" + name + "=" + value;
    }
    
    private String formatDate(final Date d) {
        if (DateSelection.sdf == null) {
            (DateSelection.sdf = new SimpleDateFormat(DateUtil.DateFormatHandler.ISO_DATE_TIME.getDateTimeFormatString())).setTimeZone(DateUtil.TIMEZONE_GMT);
        }
        return DateSelection.sdf.format(d);
    }
    
    private Date parseDate(final String s) throws ParseException {
        if (s == null || s.trim().length() == 0) {
            return null;
        }
        if (DateSelection.sdf == null) {
            (DateSelection.sdf = new SimpleDateFormat(DateUtil.DateFormatHandler.ISO_DATE_TIME.getDateTimeFormatString())).setTimeZone(DateUtil.TIMEZONE_GMT);
        }
        return DateSelection.sdf.parse(s);
    }
    
    public void getUrlArgs(final Hashtable<String, String> args) {
        args.put("dateselection.start_mode", DateSelection.TIMEMODESTRINGS[this.startMode]);
        args.put("dateselection.end_mode", DateSelection.TIMEMODESTRINGS[this.endMode]);
        if (this.startMode == 0) {
            args.put("dateselection.start_fixedtime", this.formatDate(new Date(this.startFixedTime)));
        }
        if (this.endMode == 0) {
            args.put("dateselection.end_fixedtime", this.formatDate(new Date(this.endFixedTime)));
        }
        if (this.startOffset != 0.0) {
            args.put("dateselection.start_offset", "" + this.startOffset);
        }
        if (this.endOffset != 0.0) {
            args.put("dateselection.end_offset", "" + this.endOffset);
        }
        if (this.preRange == this.preRange) {
            args.put("dateselection.prerange", "" + this.preRange);
        }
        if (this.postRange == this.postRange) {
            args.put("dateselection.postrange", "" + this.postRange);
        }
        if (this.interval == this.interval) {
            args.put("dateselection.interval", "" + this.interval);
        }
        if (this.roundTo != 0.0) {
            args.put("dateselection.roundto", "" + this.roundTo);
        }
        if (this.count != Integer.MAX_VALUE) {
            args.put("dateselection.count", "" + this.count);
        }
        if (this.skip != 0) {
            args.put("dateselection.skip", "" + this.skip);
        }
    }
    
    public String toUrlString() {
        final StringBuffer sb = new StringBuffer();
        final Hashtable<String, String> args = new Hashtable<String, String>();
        this.getUrlArgs(args);
        final Enumeration keys = args.keys();
        while (keys.hasMoreElements()) {
            final String key = keys.nextElement();
            final String value = args.get(key);
            sb.append(urlArg(key, value));
        }
        return sb.toString();
    }
    
    static {
        DateSelection.TIMEMODESTRINGS = new String[] { "FIXED", "CURRENT", "RELATIVE", "DATA" };
        DateSelection.TIMEMODES = new int[] { 0, 1, 2 };
        DateSelection.STARTMODELABELS = new String[] { "Fixed", "Current Time (Now)", "Relative to End Time  " };
        DateSelection.ENDMODELABELS = new String[] { "Fixed", "Current Time (Now)", "Relative to Start Time" };
    }
}
