// 
// Decompiled by Procyon v0.5.36
// 

package ucar.unidata.geoloc;

public class EarthLocationImpl implements EarthLocation
{
    protected double lat;
    protected double lon;
    protected double alt;
    
    @Override
    public double getLatitude() {
        return this.lat;
    }
    
    @Override
    public double getLongitude() {
        return this.lon;
    }
    
    @Override
    public double getAltitude() {
        return this.alt;
    }
    
    @Override
    public LatLonPoint getLatLon() {
        return new LatLonPointImpl(this.lat, this.lon);
    }
    
    @Override
    public boolean isMissing() {
        return Double.isNaN(this.lat) || Double.isNaN(this.lon);
    }
    
    protected EarthLocationImpl() {
    }
    
    public EarthLocationImpl(final double lat, final double lon, final double alt) {
        this.lat = lat;
        this.lon = lon;
        this.alt = alt;
    }
    
    protected void setLatitude(final double lat) {
        this.lat = lat;
    }
    
    protected void setLongitude(final double lon) {
        this.lon = lon;
    }
    
    protected void setAltitude(final double alt) {
        this.alt = alt;
    }
    
    @Override
    public String toString() {
        return "lat=" + this.lat + " lon=" + this.lon + " alt=" + this.alt;
    }
}
