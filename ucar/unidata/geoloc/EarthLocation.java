// 
// Decompiled by Procyon v0.5.36
// 

package ucar.unidata.geoloc;

public interface EarthLocation
{
    double getLatitude();
    
    double getLongitude();
    
    double getAltitude();
    
    LatLonPoint getLatLon();
    
    boolean isMissing();
}
