// 
// Decompiled by Procyon v0.5.36
// 

package ucar.unidata.geoloc;

public interface ProjectionPoint
{
    double getX();
    
    double getY();
    
    boolean equals(final ProjectionPoint p0);
}
