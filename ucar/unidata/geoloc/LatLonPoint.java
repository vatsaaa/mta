// 
// Decompiled by Procyon v0.5.36
// 

package ucar.unidata.geoloc;

public interface LatLonPoint
{
    double getLongitude();
    
    double getLatitude();
    
    boolean equals(final LatLonPoint p0);
}
