// 
// Decompiled by Procyon v0.5.36
// 

package ucar.unidata.geoloc;

import java.util.Formatter;

public class Earth
{
    private static final double earthRadius = 6371229.0;
    private double eccentricity;
    private double eccentricitySquared;
    private double equatorRadius;
    private double poleRadius;
    private double flattening;
    private String name;
    
    public Earth() {
        this(6371229.0);
    }
    
    public Earth(final double radius) {
        this.name = "earth";
        this.equatorRadius = radius;
        this.poleRadius = radius;
        this.flattening = 0.0;
        this.eccentricity = 1.0;
        this.eccentricitySquared = 1.0;
    }
    
    public Earth(final double equatorRadius, final double poleRadius, final double reciprocalFlattening) {
        this(equatorRadius, poleRadius, reciprocalFlattening, "earth");
    }
    
    public Earth(final double equatorRadius, double poleRadius, final double reciprocalFlattening, final String name) {
        this.name = "earth";
        this.equatorRadius = equatorRadius;
        this.poleRadius = poleRadius;
        this.name = name;
        if (reciprocalFlattening != 0.0) {
            this.flattening = 1.0 / reciprocalFlattening;
            this.eccentricitySquared = 2.0 * this.flattening - this.flattening * this.flattening;
            poleRadius = equatorRadius * Math.sqrt(1.0 - this.eccentricitySquared);
        }
        else {
            this.eccentricitySquared = 1.0 - poleRadius * poleRadius / (equatorRadius * equatorRadius);
            this.flattening = 1.0 - poleRadius / equatorRadius;
        }
        this.eccentricity = Math.sqrt(this.eccentricitySquared);
    }
    
    public static double getRadius() {
        return 6371229.0;
    }
    
    public double getMajor() {
        return this.equatorRadius;
    }
    
    public double getMinor() {
        return this.poleRadius;
    }
    
    public void setName(final String value) {
        this.name = value;
    }
    
    public String getName() {
        return this.name;
    }
    
    public void setEccentricity(final double value) {
        this.eccentricity = value;
    }
    
    public double getEccentricity() {
        return this.eccentricity;
    }
    
    public void setEccentricitySquared(final double value) {
        this.eccentricitySquared = value;
    }
    
    public double getEccentricitySquared() {
        return this.eccentricitySquared;
    }
    
    public void setEquatorRadius(final double value) {
        this.equatorRadius = value;
    }
    
    public double getEquatorRadius() {
        return this.equatorRadius;
    }
    
    public void setPoleRadius(final double value) {
        this.poleRadius = value;
    }
    
    public double getPoleRadius() {
        return this.poleRadius;
    }
    
    public void setFlattening(final double value) {
        this.flattening = value;
    }
    
    public double getFlattening() {
        return this.flattening;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Earth)) {
            return false;
        }
        final Earth oo = (Earth)o;
        return this.getMajor() == oo.getMajor() && this.getMinor() == oo.getMinor();
    }
    
    @Override
    public String toString() {
        final Formatter ff = new Formatter();
        ff.format("equatorRadius=%f inverseFlattening=%f", this.equatorRadius, 1.0 / this.flattening);
        return ff.toString();
    }
}
