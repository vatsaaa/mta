// 
// Decompiled by Procyon v0.5.36
// 

package ucar.unidata.geoloc.vertical;

import ucar.ma2.InvalidRangeException;
import java.io.IOException;
import ucar.ma2.ArrayDouble;
import java.util.ArrayList;
import ucar.nc2.Dimension;
import java.util.List;
import ucar.ma2.Range;

public class VerticalTransformSubset extends VerticalTransformImpl
{
    private VerticalTransform original;
    private Range t_range;
    private List<Range> subsetList;
    
    public VerticalTransformSubset(final VerticalTransform original, final Range t_range, final Range z_range, final Range y_range, final Range x_range) {
        super(null);
        this.subsetList = new ArrayList<Range>();
        this.original = original;
        this.t_range = t_range;
        this.subsetList.add(z_range);
        this.subsetList.add(y_range);
        this.subsetList.add(x_range);
        this.units = original.getUnitString();
    }
    
    @Override
    public ArrayDouble.D3 getCoordinateArray(final int subsetIndex) throws IOException, InvalidRangeException {
        int orgIndex = subsetIndex;
        if (this.isTimeDependent() && this.t_range != null) {
            orgIndex = this.t_range.element(subsetIndex);
        }
        final ArrayDouble.D3 data = this.original.getCoordinateArray(orgIndex);
        return (ArrayDouble.D3)data.sectionNoReduce(this.subsetList);
    }
    
    @Override
    public boolean isTimeDependent() {
        return this.original.isTimeDependent();
    }
}
