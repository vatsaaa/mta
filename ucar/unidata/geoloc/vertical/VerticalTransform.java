// 
// Decompiled by Procyon v0.5.36
// 

package ucar.unidata.geoloc.vertical;

import ucar.ma2.Range;
import ucar.ma2.InvalidRangeException;
import java.io.IOException;
import ucar.ma2.ArrayDouble;

public interface VerticalTransform
{
    ArrayDouble.D3 getCoordinateArray(final int p0) throws IOException, InvalidRangeException;
    
    String getUnitString();
    
    boolean isTimeDependent();
    
    VerticalTransform subset(final Range p0, final Range p1, final Range p2, final Range p3) throws InvalidRangeException;
}
