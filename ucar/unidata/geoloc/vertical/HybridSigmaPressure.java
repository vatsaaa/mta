// 
// Decompiled by Procyon v0.5.36
// 

package ucar.unidata.geoloc.vertical;

import ucar.ma2.InvalidRangeException;
import java.io.IOException;
import ucar.ma2.Index;
import ucar.ma2.ArrayDouble;
import ucar.unidata.util.Parameter;
import java.util.List;
import ucar.nc2.Dimension;
import ucar.nc2.NetcdfFile;
import ucar.ma2.Array;
import ucar.nc2.Variable;

public class HybridSigmaPressure extends VerticalTransformImpl
{
    public static final String P0 = "P0_variableName";
    public static final String PS = "SurfacePressure_variableName";
    public static final String A = "A_variableName";
    public static final String AP = "AP_variableName";
    public static final String B = "B_variableName";
    private double p0;
    private Variable psVar;
    private Variable aVar;
    private Variable bVar;
    private Variable p0Var;
    private Array aArray;
    private Array bArray;
    
    public HybridSigmaPressure(final NetcdfFile ds, final Dimension timeDim, final List<Parameter> params) {
        super(timeDim);
        this.aArray = null;
        this.bArray = null;
        final String psName = this.getParameterStringValue(params, "SurfacePressure_variableName");
        final String aName = this.getParameterStringValue(params, "A_variableName");
        final String bName = this.getParameterStringValue(params, "B_variableName");
        final String p0Name = this.getParameterStringValue(params, "P0_variableName");
        final String apName = this.getParameterStringValue(params, "AP_variableName");
        if (apName != null) {
            this.aVar = ds.findVariable(apName);
        }
        else {
            this.aVar = ds.findVariable(aName);
        }
        this.psVar = ds.findVariable(psName);
        this.bVar = ds.findVariable(bName);
        if (p0Name != null) {
            this.p0Var = ds.findVariable(p0Name);
        }
        this.units = ds.findAttValueIgnoreCase(this.psVar, "units", "none");
    }
    
    @Override
    public ArrayDouble.D3 getCoordinateArray(final int timeIndex) throws IOException, InvalidRangeException {
        Array psArray = this.readArray(this.psVar, timeIndex);
        if (null == this.aArray) {
            this.aArray = this.aVar.read();
            this.bArray = this.bVar.read();
            this.p0 = ((this.p0Var == null) ? 1.0 : this.p0Var.readScalarDouble());
        }
        final int nz = (int)this.aArray.getSize();
        final Index aIndex = this.aArray.getIndex();
        final Index bIndex = this.bArray.getIndex();
        if (psArray.getRank() == 3) {
            psArray = psArray.reduce(0);
        }
        final int[] shape2D = psArray.getShape();
        final int ny = shape2D[0];
        final int nx = shape2D[1];
        final Index psIndex = psArray.getIndex();
        final ArrayDouble.D3 press = new ArrayDouble.D3(nz, ny, nx);
        for (int z = 0; z < nz; ++z) {
            final double term1 = this.aArray.getDouble(aIndex.set(z)) * this.p0;
            final double bz = this.bArray.getDouble(bIndex.set(z));
            for (int y = 0; y < ny; ++y) {
                for (int x = 0; x < nx; ++x) {
                    final double ps = psArray.getDouble(psIndex.set(y, x));
                    press.set(z, y, x, term1 + bz * ps);
                }
            }
        }
        return press;
    }
}
