// 
// Decompiled by Procyon v0.5.36
// 

package ucar.unidata.geoloc.vertical;

import ucar.ma2.InvalidRangeException;
import java.io.IOException;
import ucar.ma2.MAMath;
import ucar.ma2.Array;
import ucar.ma2.ArrayDouble;
import ucar.unidata.util.Parameter;
import java.util.List;
import ucar.nc2.Dimension;
import ucar.nc2.NetcdfFile;
import ucar.nc2.Variable;

public class VTfromExistingData extends VerticalTransformImpl
{
    public static final String existingDataField = "existingDataField";
    private Variable existingData;
    
    public VTfromExistingData(final NetcdfFile ds, final Dimension timeDim, final List<Parameter> params) {
        super(timeDim);
        final String vname = this.getParameterStringValue(params, "existingDataField");
        this.existingData = ds.findVariable(vname);
        this.units = this.existingData.getUnitsString();
    }
    
    @Override
    public ArrayDouble.D3 getCoordinateArray(final int timeIndex) throws IOException, InvalidRangeException {
        final Array data = this.readArray(this.existingData, timeIndex);
        final int[] shape = data.getShape();
        final ArrayDouble.D3 ddata = (ArrayDouble.D3)Array.factory(Double.TYPE, shape);
        MAMath.copyDouble(ddata, data);
        return ddata;
    }
}
