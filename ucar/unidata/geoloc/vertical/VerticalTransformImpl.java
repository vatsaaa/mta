// 
// Decompiled by Procyon v0.5.36
// 

package ucar.unidata.geoloc.vertical;

import java.util.Iterator;
import ucar.unidata.util.Parameter;
import java.util.List;
import ucar.ma2.Range;
import ucar.ma2.Array;
import ucar.nc2.Variable;
import ucar.ma2.InvalidRangeException;
import java.io.IOException;
import ucar.ma2.ArrayDouble;
import ucar.nc2.Dimension;

public abstract class VerticalTransformImpl implements VerticalTransform
{
    protected String units;
    private Dimension timeDim;
    
    public VerticalTransformImpl(final Dimension timeDim) {
        this.timeDim = timeDim;
    }
    
    public abstract ArrayDouble.D3 getCoordinateArray(final int p0) throws IOException, InvalidRangeException;
    
    public String getUnitString() {
        return this.units;
    }
    
    public boolean isTimeDependent() {
        return this.timeDim != null;
    }
    
    protected Dimension getTimeDimension() {
        return this.timeDim;
    }
    
    protected Array readArray(final Variable v, final int timeIndex) throws IOException, InvalidRangeException {
        final int[] shape = v.getShape();
        final int[] origin = new int[v.getRank()];
        if (this.getTimeDimension() != null) {
            final int dimIndex = v.findDimensionIndex(this.getTimeDimension().getName());
            if (dimIndex >= 0) {
                shape[dimIndex] = 1;
                origin[dimIndex] = timeIndex;
                return v.read(origin, shape).reduce(dimIndex);
            }
        }
        return v.read(origin, shape);
    }
    
    public VerticalTransform subset(final Range t_range, final Range z_range, final Range y_range, final Range x_range) throws InvalidRangeException {
        return new VerticalTransformSubset(this, t_range, z_range, y_range, x_range);
    }
    
    protected String getParameterStringValue(final List<Parameter> params, final String name) {
        for (final Parameter a : params) {
            if (name.equalsIgnoreCase(a.getName())) {
                return a.getStringValue();
            }
        }
        return null;
    }
    
    protected boolean getParameterBooleanValue(final List<Parameter> params, final String name) {
        for (final Parameter p : params) {
            if (name.equalsIgnoreCase(p.getName())) {
                return Boolean.valueOf(p.getStringValue());
            }
        }
        return false;
    }
}
