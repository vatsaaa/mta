// 
// Decompiled by Procyon v0.5.36
// 

package ucar.unidata.geoloc.vertical;

import ucar.ma2.InvalidRangeException;
import java.io.IOException;
import ucar.ma2.Index;
import ucar.ma2.ArrayDouble;
import ucar.unidata.util.Parameter;
import java.util.List;
import ucar.nc2.Dimension;
import ucar.nc2.NetcdfFile;
import ucar.ma2.Array;
import ucar.nc2.Variable;

public class HybridHeight extends VerticalTransformImpl
{
    public static final String OROG = "Orography_variableName";
    public static final String A = "A_variableName";
    public static final String B = "B_variableName";
    private Variable aVar;
    private Variable bVar;
    private Variable orogVar;
    private Array aArray;
    private Array bArray;
    
    public HybridHeight(final NetcdfFile ds, final Dimension timeDim, final List<Parameter> params) {
        super(timeDim);
        this.aArray = null;
        this.bArray = null;
        final String aName = this.getParameterStringValue(params, "A_variableName");
        final String bName = this.getParameterStringValue(params, "B_variableName");
        final String orogName = this.getParameterStringValue(params, "Orography_variableName");
        this.aVar = ds.findVariable(aName);
        this.bVar = ds.findVariable(bName);
        this.orogVar = ds.findVariable(orogName);
        this.units = ds.findAttValueIgnoreCase(this.orogVar, "units", "none");
    }
    
    @Override
    public ArrayDouble.D3 getCoordinateArray(final int timeIndex) throws IOException, InvalidRangeException {
        final Array orogArray = this.readArray(this.orogVar, timeIndex);
        if (null == this.aArray) {
            this.aArray = this.aVar.read();
            this.bArray = this.bVar.read();
        }
        final int nz = (int)this.aArray.getSize();
        final Index aIndex = this.aArray.getIndex();
        final Index bIndex = this.bArray.getIndex();
        final int[] shape2D = orogArray.getShape();
        final int ny = shape2D[0];
        final int nx = shape2D[1];
        final Index orogIndex = orogArray.getIndex();
        final ArrayDouble.D3 height = new ArrayDouble.D3(nz, ny, nx);
        for (int z = 0; z < nz; ++z) {
            final double az = this.aArray.getDouble(aIndex.set(z));
            final double bz = this.bArray.getDouble(bIndex.set(z));
            for (int y = 0; y < ny; ++y) {
                for (int x = 0; x < nx; ++x) {
                    final double orog = orogArray.getDouble(orogIndex.set(y, x));
                    height.set(z, y, x, az + bz * orog);
                }
            }
        }
        return height;
    }
}
