// 
// Decompiled by Procyon v0.5.36
// 

package ucar.unidata.geoloc.vertical;

import ucar.ma2.InvalidRangeException;
import ucar.ma2.Index;
import ucar.ma2.ArrayDouble;
import ucar.ma2.Array;
import java.io.IOException;
import ucar.unidata.util.Parameter;
import java.util.List;
import ucar.nc2.Dimension;
import ucar.nc2.NetcdfFile;
import ucar.nc2.Variable;

public class AtmosSigma extends VerticalTransformImpl
{
    public static final String PTOP = "PressureTop_variableName";
    public static final String PS = "SurfacePressure_variableName";
    public static final String SIGMA = "Sigma_variableName";
    private Variable psVar;
    private double[] sigma;
    private double ptop;
    
    public AtmosSigma(final NetcdfFile ds, final Dimension timeDim, final List<Parameter> params) {
        super(timeDim);
        final String psName = this.getParameterStringValue(params, "SurfacePressure_variableName");
        this.psVar = ds.findVariable(psName);
        final String ptopName = this.getParameterStringValue(params, "PressureTop_variableName");
        final Variable ptopVar = ds.findVariable(ptopName);
        try {
            this.ptop = ptopVar.readScalarDouble();
        }
        catch (IOException e) {
            throw new IllegalArgumentException("AtmosSigma failed to read " + ptopVar + " err= " + e.getMessage());
        }
        final String sigmaName = this.getParameterStringValue(params, "Sigma_variableName");
        final Variable sigmaVar = ds.findVariable(sigmaName);
        try {
            final Array data = sigmaVar.read();
            this.sigma = (double[])data.get1DJavaArray(Double.TYPE);
        }
        catch (IOException e2) {
            throw new IllegalArgumentException("AtmosSigma failed to read " + sigmaName + " err= " + e2.getMessage());
        }
        this.units = ds.findAttValueIgnoreCase(this.psVar, "units", "none");
    }
    
    @Override
    public ArrayDouble.D3 getCoordinateArray(final int timeIndex) throws IOException, InvalidRangeException {
        final Array ps = this.readArray(this.psVar, timeIndex);
        final Index psIndex = ps.getIndex();
        final int nz = this.sigma.length;
        final int[] shape2D = ps.getShape();
        final int ny = shape2D[0];
        final int nx = shape2D[1];
        final ArrayDouble.D3 result = new ArrayDouble.D3(nz, ny, nx);
        for (int y = 0; y < ny; ++y) {
            for (int x = 0; x < nx; ++x) {
                final double psVal = ps.getDouble(psIndex.set(y, x));
                for (int z = 0; z < nz; ++z) {
                    result.set(z, y, x, this.ptop + this.sigma[z] * (psVal - this.ptop));
                }
            }
        }
        return result;
    }
}
