// 
// Decompiled by Procyon v0.5.36
// 

package ucar.unidata.geoloc.vertical;

import ucar.ma2.InvalidRangeException;
import ucar.ma2.Index;
import ucar.ma2.IndexIterator;
import ucar.nc2.Variable;
import ucar.ma2.ArrayDouble;
import java.io.IOException;
import ucar.unidata.util.Parameter;
import java.util.List;
import ucar.nc2.Dimension;
import ucar.nc2.NetcdfFile;
import ucar.ma2.Array;

public class AtmosLnPressure extends VerticalTransformImpl
{
    public static final String P0 = "ReferencePressureVariableName";
    public static final String LEV = "VerticalCoordinateVariableName";
    private Array pressure;
    private double p0;
    
    public AtmosLnPressure(final NetcdfFile ds, final Dimension timeDim, final List<Parameter> params) {
        super(timeDim);
        final String p0name = this.getParameterStringValue(params, "ReferencePressureVariableName");
        final Variable p0var = ds.findVariable(p0name);
        try {
            this.p0 = p0var.readScalarDouble();
        }
        catch (IOException e) {
            throw new IllegalArgumentException("AtmosLnPressure failed to read " + p0name + " err= " + e.getMessage());
        }
        final String levName = this.getParameterStringValue(params, "VerticalCoordinateVariableName");
        final Variable levVar = ds.findVariable(levName);
        try {
            final Array lev = levVar.read();
            assert lev.getRank() == 1;
            final ArrayDouble.D1 pressure = new ArrayDouble.D1((int)lev.getSize());
            final IndexIterator ii = pressure.getIndexIterator();
            while (lev.hasNext()) {
                final double result = this.p0 * Math.exp(-lev.nextDouble());
                ii.setDoubleNext(result);
            }
        }
        catch (IOException e2) {
            throw new IllegalArgumentException("AtmosLnPressure failed to read " + levName + " err= " + e2.getMessage());
        }
        this.units = p0var.getUnitsString();
    }
    
    @Override
    public ArrayDouble.D3 getCoordinateArray(final int timeIndex) throws IOException, InvalidRangeException {
        final Array ps = null;
        final Index psIndex = ps.getIndex();
        final int nz = (int)this.pressure.getSize();
        final int[] shape2D = ps.getShape();
        final int ny = shape2D[0];
        final int nx = shape2D[1];
        final ArrayDouble.D3 result = new ArrayDouble.D3(nz, ny, nx);
        final IndexIterator ii = this.pressure.getIndexIterator();
        for (int z = 0; z < nz; ++z) {
            final double zcoord = ii.getDoubleNext();
            for (int y = 0; y < ny; ++y) {
                for (int x = 0; x < nx; ++x) {
                    result.set(z, y, x, zcoord);
                }
            }
        }
        return result;
    }
}
