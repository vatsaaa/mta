// 
// Decompiled by Procyon v0.5.36
// 

package ucar.unidata.geoloc.vertical;

import ucar.ma2.Index;
import ucar.unidata.util.SpecialMathFunction;
import ucar.ma2.InvalidRangeException;
import java.io.IOException;
import ucar.ma2.ArrayDouble;
import ucar.unidata.util.Parameter;
import java.util.List;
import ucar.nc2.Dimension;
import ucar.nc2.NetcdfFile;
import ucar.ma2.Array;
import ucar.nc2.Variable;

public class OceanS extends VerticalTransformImpl
{
    public static final String ETA = "Eta_variableName";
    public static final String S = "S_variableName";
    public static final String DEPTH = "Depth_variableName";
    public static final String DEPTH_C = "Depth_c_variableName";
    public static final String A = "A_variableName";
    public static final String B = "B_variableName";
    private double depth_c;
    private Variable etaVar;
    private Variable sVar;
    private Variable depthVar;
    private Variable aVar;
    private Variable bVar;
    private Variable depthCVar;
    private Array c;
    
    public OceanS(final NetcdfFile ds, final Dimension timeDim, final List<Parameter> params) {
        super(timeDim);
        this.c = null;
        final String etaName = this.getParameterStringValue(params, "Eta_variableName");
        final String sName = this.getParameterStringValue(params, "S_variableName");
        final String depthName = this.getParameterStringValue(params, "Depth_variableName");
        final String aName = this.getParameterStringValue(params, "A_variableName");
        final String bName = this.getParameterStringValue(params, "B_variableName");
        final String depthCName = this.getParameterStringValue(params, "Depth_c_variableName");
        this.etaVar = ds.findVariable(etaName);
        this.sVar = ds.findVariable(sName);
        this.depthVar = ds.findVariable(depthName);
        this.aVar = ds.findVariable(aName);
        this.bVar = ds.findVariable(bName);
        this.depthCVar = ds.findVariable(depthCName);
        this.units = ds.findAttValueIgnoreCase(this.depthVar, "units", "none");
    }
    
    @Override
    public ArrayDouble.D3 getCoordinateArray(final int timeIndex) throws IOException, InvalidRangeException {
        final Array etaArray = this.readArray(this.etaVar, timeIndex);
        final Array sArray = this.readArray(this.sVar, timeIndex);
        final Array depthArray = this.readArray(this.depthVar, timeIndex);
        if (null == this.c) {
            final double a = this.aVar.readScalarDouble();
            final double b = this.bVar.readScalarDouble();
            this.depth_c = this.depthCVar.readScalarDouble();
            this.c = this.makeC(sArray, a, b);
        }
        return this.makeHeight(etaArray, sArray, depthArray, this.c, this.depth_c);
    }
    
    public ArrayDouble.D1 getCoordinateArray1D(final int timeIndex, final int xIndex, final int yIndex) throws IOException, InvalidRangeException {
        final Array etaArray = this.readArray(this.etaVar, timeIndex);
        final Array sArray = this.readArray(this.sVar, timeIndex);
        final Array depthArray = this.readArray(this.depthVar, timeIndex);
        if (null == this.c) {
            final double a = this.aVar.readScalarDouble();
            final double b = this.bVar.readScalarDouble();
            this.depth_c = this.depthCVar.readScalarDouble();
            this.c = this.makeC(sArray, a, b);
        }
        return this.makeHeight1D(etaArray, sArray, depthArray, this.c, this.depth_c, xIndex, yIndex);
    }
    
    private Array makeC(final Array s, final double a, final double b) {
        final int nz = (int)s.getSize();
        final Index sIndex = s.getIndex();
        if (a == 0.0) {
            return s;
        }
        final ArrayDouble.D1 c = new ArrayDouble.D1(nz);
        final double fac1 = 1.0 - b;
        final double denom1 = 1.0 / SpecialMathFunction.sinh(a);
        final double denom2 = 1.0 / (2.0 * SpecialMathFunction.tanh(0.5 * a));
        for (int i = 0; i < nz; ++i) {
            final double sz = s.getDouble(sIndex.set(i));
            final double term1 = fac1 * SpecialMathFunction.sinh(a * sz) * denom1;
            final double term2 = b * (SpecialMathFunction.tanh(a * (sz + 0.5)) * denom2 - 0.5);
            c.set(i, term1 + term2);
        }
        return c;
    }
    
    private ArrayDouble.D3 makeHeight(final Array eta, final Array s, final Array depth, final Array c, final double depth_c) {
        final int nz = (int)s.getSize();
        final Index sIndex = s.getIndex();
        final Index cIndex = c.getIndex();
        final int[] shape2D = eta.getShape();
        final int ny = shape2D[0];
        final int nx = shape2D[1];
        final Index etaIndex = eta.getIndex();
        final Index depthIndex = depth.getIndex();
        final ArrayDouble.D3 height = new ArrayDouble.D3(nz, ny, nx);
        for (int z = 0; z < nz; ++z) {
            final double sz = s.getDouble(sIndex.set(z));
            final double cz = c.getDouble(cIndex.set(z));
            final double term1 = depth_c * sz;
            for (int y = 0; y < ny; ++y) {
                for (int x = 0; x < nx; ++x) {
                    final double fac1 = depth.getDouble(depthIndex.set(y, x));
                    final double term2 = (fac1 - depth_c) * cz;
                    final double Sterm = term1 + term2;
                    final double term3 = eta.getDouble(etaIndex.set(y, x));
                    final double term4 = 1.0 + Sterm / fac1;
                    final double hterm = Sterm + term3 * term4;
                    height.set(z, y, x, hterm);
                }
            }
        }
        return height;
    }
    
    private ArrayDouble.D1 makeHeight1D(final Array eta, final Array s, final Array depth, final Array c, final double depth_c, final int x_index, final int y_index) {
        final int nz = (int)s.getSize();
        final Index sIndex = s.getIndex();
        final Index cIndex = c.getIndex();
        final Index etaIndex = eta.getIndex();
        final Index depthIndex = depth.getIndex();
        final ArrayDouble.D1 height = new ArrayDouble.D1(nz);
        for (int z = 0; z < nz; ++z) {
            final double sz = s.getDouble(sIndex.set(z));
            final double cz = c.getDouble(cIndex.set(z));
            final double term1 = depth_c * sz;
            final double fac1 = depth.getDouble(depthIndex.set(y_index, x_index));
            final double term2 = (fac1 - depth_c) * cz;
            final double Sterm = term1 + term2;
            final double term3 = eta.getDouble(etaIndex.set(y_index, x_index));
            final double term4 = 1.0 + Sterm / fac1;
            final double hterm = Sterm + term3 * term4;
            height.set(z, hterm);
        }
        return height;
    }
}
