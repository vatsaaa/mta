// 
// Decompiled by Procyon v0.5.36
// 

package ucar.unidata.geoloc.vertical;

import ucar.ma2.IndexIterator;
import ucar.ma2.InvalidRangeException;
import java.io.IOException;
import ucar.ma2.Index;
import ucar.ma2.Array;
import ucar.ma2.ArrayDouble;
import ucar.unidata.util.Parameter;
import java.util.List;
import ucar.nc2.Dimension;
import ucar.nc2.NetcdfFile;
import ucar.nc2.Variable;

public class WRFEta extends VerticalTransformImpl
{
    public static final String BasePressureVariable = "base_presure";
    public static final String PerturbationPressureVariable = "perturbation_presure";
    public static final String BaseGeopotentialVariable = "base_geopotential";
    public static final String PerturbationGeopotentialVariable = "perturbation_geopotential";
    public static final String IsStaggeredX = "staggered_x";
    public static final String IsStaggeredY = "staggered_y";
    public static final String IsStaggeredZ = "staggered_z";
    private Variable pertVar;
    private Variable baseVar;
    private boolean isXStag;
    private boolean isYStag;
    private boolean isZStag;
    
    public WRFEta(final NetcdfFile ds, final Dimension timeDim, final List<Parameter> params) {
        super(timeDim);
        this.isXStag = this.getParameterBooleanValue(params, "staggered_x");
        this.isYStag = this.getParameterBooleanValue(params, "staggered_y");
        this.isZStag = this.getParameterBooleanValue(params, "staggered_z");
        String pertVarName;
        String baseVarName;
        if (this.isZStag) {
            pertVarName = this.getParameterStringValue(params, "perturbation_geopotential");
            baseVarName = this.getParameterStringValue(params, "base_geopotential");
            this.units = "m";
        }
        else {
            pertVarName = this.getParameterStringValue(params, "perturbation_presure");
            baseVarName = this.getParameterStringValue(params, "base_presure");
            this.units = "Pa";
        }
        this.pertVar = ds.findVariable(pertVarName);
        this.baseVar = ds.findVariable(baseVarName);
        if (this.pertVar == null) {
            throw new RuntimeException("Cant find perturbation pressure variable= " + pertVarName + " in WRF file");
        }
        if (this.baseVar == null) {
            throw new RuntimeException("Cant find base state pressure variable=  " + baseVarName + " in WRF file");
        }
    }
    
    @Override
    public ArrayDouble.D3 getCoordinateArray(final int timeIndex) throws IOException {
        final Array pertArray = this.getTimeSlice(this.pertVar, timeIndex);
        final Array baseArray = this.getTimeSlice(this.baseVar, timeIndex);
        final int[] shape = pertArray.getShape();
        final int ni = shape[0];
        final int nj = shape[1];
        final int nk = shape[2];
        ArrayDouble.D3 array = new ArrayDouble.D3(ni, nj, nk);
        final Index index = array.getIndex();
        for (int i = 0; i < ni; ++i) {
            for (int j = 0; j < nj; ++j) {
                for (int k = 0; k < nk; ++k) {
                    index.set(i, j, k);
                    double d = pertArray.getDouble(index) + baseArray.getDouble(index);
                    if (this.isZStag) {
                        d /= 9.81;
                    }
                    array.setDouble(index, d);
                }
            }
        }
        if (this.isXStag) {
            array = this.addStagger(array, 2);
        }
        if (this.isYStag) {
            array = this.addStagger(array, 1);
        }
        return array;
    }
    
    private ArrayDouble.D3 addStagger(final ArrayDouble.D3 array, final int dimIndex) {
        final int[] shape = array.getShape();
        final int[] newShape = new int[3];
        for (int i = 0; i < 3; ++i) {
            newShape[i] = shape[i];
        }
        final int[] array2 = newShape;
        ++array2[dimIndex];
        final int ni = newShape[0];
        final int nj = newShape[1];
        final int nk = newShape[2];
        final ArrayDouble.D3 newArray = new ArrayDouble.D3(ni, nj, nk);
        final int n = shape[dimIndex];
        final double[] d = new double[n];
        final int[] eshape = new int[3];
        final int[] neweshape = new int[3];
        for (int j = 0; j < 3; ++j) {
            eshape[j] = ((j == dimIndex) ? n : 1);
            neweshape[j] = ((j == dimIndex) ? (n + 1) : 1);
        }
        final int[] origin = new int[3];
        try {
            for (int k = 0; k < ((dimIndex == 0) ? 1 : ni); ++k) {
                for (int l = 0; l < ((dimIndex == 1) ? 1 : nj); ++l) {
                    for (int m = 0; m < ((dimIndex == 2) ? 1 : nk); ++m) {
                        origin[0] = k;
                        origin[1] = l;
                        origin[2] = m;
                        final IndexIterator it = array.section(origin, eshape).getIndexIterator();
                        for (int l2 = 0; l2 < n; ++l2) {
                            d[l2] = it.getDoubleNext();
                        }
                        final double[] d2 = this.extrapinterpolate(d);
                        final IndexIterator newit = newArray.section(origin, neweshape).getIndexIterator();
                        for (int l3 = 0; l3 < n + 1; ++l3) {
                            newit.setDoubleNext(d2[l3]);
                        }
                    }
                }
            }
        }
        catch (InvalidRangeException e) {
            return null;
        }
        return newArray;
    }
    
    private double[] extrapinterpolate(final double[] array) {
        final int n = array.length;
        final double[] d = new double[n + 1];
        d[0] = 1.5 * array[0] - 0.5 * array[1];
        d[n] = 1.5 * array[n - 1] - 0.5 * array[n - 2];
        for (int i = 1; i < n; ++i) {
            d[i] = 0.5 * (array[i - 1] + array[i]);
        }
        return d;
    }
    
    private Array getTimeSlice(final Variable v, final int timeIndex) throws IOException {
        final int[] shape = v.getShape();
        final int[] origin = new int[v.getRank()];
        if (this.getTimeDimension() != null) {
            final int dimIndex = v.findDimensionIndex(this.getTimeDimension().getName());
            if (dimIndex >= 0) {
                shape[dimIndex] = 1;
                origin[dimIndex] = timeIndex;
            }
        }
        try {
            return v.read(origin, shape).reduce();
        }
        catch (InvalidRangeException e) {
            return null;
        }
    }
}
