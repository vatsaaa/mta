// 
// Decompiled by Procyon v0.5.36
// 

package ucar.unidata.geoloc.vertical;

import ucar.ma2.InvalidRangeException;
import java.io.IOException;
import ucar.ma2.Index;
import ucar.ma2.Array;
import ucar.ma2.ArrayDouble;
import ucar.unidata.util.Parameter;
import java.util.List;
import ucar.nc2.Dimension;
import ucar.nc2.NetcdfFile;
import ucar.nc2.Variable;

public class OceanSigma extends VerticalTransformImpl
{
    public static final String ETA = "Eta_variableName";
    public static final String SIGMA = "Sigma_variableName";
    public static final String DEPTH = "Depth_variableName";
    private Variable etaVar;
    private Variable sVar;
    private Variable depthVar;
    
    public OceanSigma(final NetcdfFile ds, final Dimension timeDim, final List<Parameter> params) {
        super(timeDim);
        final String etaName = this.getParameterStringValue(params, "Eta_variableName");
        final String sName = this.getParameterStringValue(params, "Sigma_variableName");
        final String depthName = this.getParameterStringValue(params, "Depth_variableName");
        this.etaVar = ds.findVariable(etaName);
        this.sVar = ds.findVariable(sName);
        this.depthVar = ds.findVariable(depthName);
        this.units = ds.findAttValueIgnoreCase(this.depthVar, "units", "none");
    }
    
    @Override
    public ArrayDouble.D3 getCoordinateArray(final int timeIndex) throws IOException, InvalidRangeException {
        final Array eta = this.readArray(this.etaVar, timeIndex);
        final Array sigma = this.readArray(this.sVar, timeIndex);
        final Array depth = this.readArray(this.depthVar, timeIndex);
        final int nz = (int)sigma.getSize();
        final Index sIndex = sigma.getIndex();
        final int[] shape2D = eta.getShape();
        final int ny = shape2D[0];
        final int nx = shape2D[1];
        final Index etaIndex = eta.getIndex();
        final Index depthIndex = depth.getIndex();
        final ArrayDouble.D3 height = new ArrayDouble.D3(nz, ny, nx);
        for (int z = 0; z < nz; ++z) {
            final double sigmaVal = sigma.getDouble(sIndex.set(z));
            for (int y = 0; y < ny; ++y) {
                for (int x = 0; x < nx; ++x) {
                    final double etaVal = eta.getDouble(etaIndex.set(y, x));
                    final double depthVal = depth.getDouble(depthIndex.set(y, x));
                    height.set(z, y, x, etaVal + sigmaVal * (depthVal + etaVal));
                }
            }
        }
        return height;
    }
}
