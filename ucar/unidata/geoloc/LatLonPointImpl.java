// 
// Decompiled by Procyon v0.5.36
// 

package ucar.unidata.geoloc;

import ucar.unidata.util.Format;
import java.io.Serializable;

public class LatLonPointImpl implements LatLonPoint, Serializable
{
    private double lat;
    private double lon;
    
    @Deprecated
    public static boolean betweenLon(final double lon, double lonBeg, double lonEnd) {
        lonBeg = lonNormal(lonBeg, lon);
        lonEnd = lonNormal(lonEnd, lon);
        return lon >= lonBeg && lon <= lonEnd;
    }
    
    public static double getClockwiseDistanceTo(final double from, final double to) {
        double distance;
        for (distance = to - from; distance < 0.0; distance += 360.0) {}
        return distance;
    }
    
    public static double range180(final double lon) {
        return lonNormal(lon);
    }
    
    public static double lonNormal360(final double lon) {
        return lonNormal(lon, 180.0);
    }
    
    public static double lonNormal(final double lon, final double center) {
        return center + Math.IEEEremainder(lon - center, 360.0);
    }
    
    public static double lonNormalFrom(double lon, final double start) {
        while (lon < start) {
            lon += 360.0;
        }
        while (lon > start + 360.0) {
            lon -= 360.0;
        }
        return lon;
    }
    
    public static double lonNormal(final double lon) {
        if (lon < -180.0 || lon > 180.0) {
            return Math.IEEEremainder(lon, 360.0);
        }
        return lon;
    }
    
    public static double latNormal(final double lat) {
        if (lat < -90.0) {
            return -90.0;
        }
        if (lat > 90.0) {
            return 90.0;
        }
        return lat;
    }
    
    public static String latToString(double lat, final int sigDigits) {
        final boolean is_north = lat >= 0.0;
        if (!is_north) {
            lat = -lat;
        }
        final StringBuilder latBuff = new StringBuilder(20);
        latBuff.setLength(0);
        latBuff.append(Format.d(lat, sigDigits));
        latBuff.append(is_north ? "N" : "S");
        return latBuff.toString();
    }
    
    public static String lonToString(final double lon, final int sigDigits) {
        double wlon = lonNormal(lon);
        final boolean is_east = wlon >= 0.0;
        if (!is_east) {
            wlon = -wlon;
        }
        final StringBuilder lonBuff = new StringBuilder(20);
        lonBuff.setLength(0);
        lonBuff.append(Format.d(wlon, sigDigits));
        lonBuff.append(is_east ? "E" : "W");
        return lonBuff.toString();
    }
    
    public LatLonPointImpl() {
        this(0.0, 0.0);
    }
    
    public LatLonPointImpl(final LatLonPoint pt) {
        this(pt.getLatitude(), pt.getLongitude());
    }
    
    public LatLonPointImpl(final double lat, final double lon) {
        this.setLatitude(lat);
        this.setLongitude(lon);
    }
    
    @Override
    public double getLongitude() {
        return this.lon;
    }
    
    @Override
    public double getLatitude() {
        return this.lat;
    }
    
    public void set(final LatLonPoint pt) {
        this.setLongitude(pt.getLongitude());
        this.setLatitude(pt.getLatitude());
    }
    
    public void set(final double lat, final double lon) {
        this.setLongitude(lon);
        this.setLatitude(lat);
    }
    
    public void set(final float lat, final float lon) {
        this.setLongitude(lon);
        this.setLatitude(lat);
    }
    
    public void setLongitude(final double lon) {
        this.lon = lonNormal(lon);
    }
    
    public void setLatitude(final double lat) {
        this.lat = latNormal(lat);
    }
    
    @Override
    public boolean equals(final Object obj) {
        if (!(obj instanceof LatLonPointImpl)) {
            return false;
        }
        final LatLonPointImpl that = (LatLonPointImpl)obj;
        return this.lat == that.lat && this.lon == that.lon;
    }
    
    @Override
    public boolean equals(final LatLonPoint pt) {
        boolean lonOk = this.closeEnough(pt.getLongitude(), this.lon);
        if (!lonOk) {
            lonOk = this.closeEnough(lonNormal360(pt.getLongitude()), lonNormal360(this.lon));
        }
        return lonOk && this.closeEnough(pt.getLatitude(), this.lat);
    }
    
    private boolean closeEnough(final double d1, final double d2) {
        if (d1 != 0.0) {
            return Math.abs((d1 - d2) / d1) < 1.0E-9;
        }
        return d2 == 0.0 || Math.abs((d1 - d2) / d2) < 1.0E-9;
    }
    
    @Override
    public String toString() {
        return this.toString(4);
    }
    
    public String toString(final int sigDigits) {
        final StringBuilder sbuff = new StringBuilder(40);
        sbuff.setLength(0);
        sbuff.append(latToString(this.lat, sigDigits));
        sbuff.append(" ");
        sbuff.append(lonToString(this.lon, sigDigits));
        return sbuff.toString();
    }
}
