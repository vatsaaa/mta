// 
// Decompiled by Procyon v0.5.36
// 

package ucar.unidata.geoloc;

import java.awt.geom.Point2D;
import ucar.unidata.geoloc.projection.LatLonProjection;
import java.awt.geom.Rectangle2D;
import ucar.unidata.util.Format;
import java.util.ArrayList;
import ucar.unidata.util.Parameter;
import java.util.List;
import java.io.Serializable;

public abstract class ProjectionImpl implements Projection, Cloneable, Serializable
{
    public static final String ATTR_NAME = "grid_mapping_name";
    public static final double EARTH_RADIUS;
    public static final int INDEX_LAT = 0;
    public static final int INDEX_LON = 1;
    public static final int INDEX_X = 0;
    public static final int INDEX_Y = 1;
    protected static final double TOLERANCE = 1.0E-6;
    public static final double PI = 3.141592653589793;
    public static final double PI_OVER_2 = 1.5707963267948966;
    public static final double PI_OVER_4 = 0.7853981633974483;
    protected String name;
    protected boolean isLatLon;
    protected List<Parameter> atts;
    protected ProjectionRect defaultMapArea;
    private static String header;
    
    public ProjectionImpl() {
        this.name = "";
        this.isLatLon = false;
        this.atts = new ArrayList<Parameter>();
        this.defaultMapArea = new ProjectionRect();
    }
    
    public abstract ProjectionImpl constructCopy();
    
    @Override
    public String getClassName() {
        String className = this.getClass().getName();
        final int index = className.lastIndexOf(".");
        if (index >= 0) {
            className = className.substring(index + 1);
        }
        return className;
    }
    
    @Override
    public abstract String paramsToString();
    
    public String getProjectionTypeLabel() {
        return this.getClassName();
    }
    
    @Override
    public abstract ProjectionPoint latLonToProj(final LatLonPoint p0, final ProjectionPointImpl p1);
    
    @Override
    public abstract LatLonPoint projToLatLon(final ProjectionPoint p0, final LatLonPointImpl p1);
    
    public ProjectionPoint latLonToProj(final LatLonPoint latLon) {
        return this.latLonToProj(latLon, new ProjectionPointImpl());
    }
    
    public LatLonPoint projToLatLon(final ProjectionPoint ppt) {
        return this.projToLatLon(ppt, new LatLonPointImpl());
    }
    
    @Override
    public abstract boolean crossSeam(final ProjectionPoint p0, final ProjectionPoint p1);
    
    @Override
    public abstract boolean equals(final Object p0);
    
    @Override
    public String getName() {
        return this.name;
    }
    
    public void setName(final String name) {
        this.name = name;
    }
    
    @Override
    public List<Parameter> getProjectionParameters() {
        return this.atts;
    }
    
    protected void addParameter(final String name, final String value) {
        this.atts.add(new Parameter(name, value));
    }
    
    protected void addParameter(final String name, final double value) {
        this.atts.add(new Parameter(name, value));
    }
    
    protected void addParameter(final Parameter p) {
        this.atts.add(p);
    }
    
    public boolean isLatLon() {
        return this.isLatLon;
    }
    
    public static String getHeader() {
        if (ProjectionImpl.header == null) {
            final StringBuilder headerB = new StringBuilder(60);
            headerB.append("Name");
            Format.tab(headerB, 20, true);
            headerB.append("Class");
            Format.tab(headerB, 40, true);
            headerB.append("Parameters");
            ProjectionImpl.header = headerB.toString();
        }
        return ProjectionImpl.header;
    }
    
    @Override
    public String toString() {
        return this.getName();
    }
    
    public Object clone() {
        ProjectionImpl p;
        try {
            p = (ProjectionImpl)super.clone();
        }
        catch (CloneNotSupportedException e) {
            return null;
        }
        p.name = this.name;
        p.defaultMapArea = new ProjectionRect(this.defaultMapArea);
        return p;
    }
    
    @Override
    public ProjectionRect getDefaultMapArea() {
        return this.defaultMapArea;
    }
    
    public LatLonRect getDefaultMapAreaLL() {
        return this.projToLatLonBB(this.defaultMapArea);
    }
    
    public void setDefaultMapArea(final ProjectionRect bb) {
        if (bb == null) {
            return;
        }
        this.defaultMapArea = (ProjectionRect)bb.clone();
    }
    
    public ProjectionPointImpl latLonToProj(final double lat, final double lon) {
        return (ProjectionPointImpl)this.latLonToProj(new LatLonPointImpl(lat, lon));
    }
    
    public LatLonPointImpl projToLatLon(final double x, final double y) {
        return (LatLonPointImpl)this.projToLatLon(new ProjectionPointImpl(x, y));
    }
    
    public double[][] projToLatLon(final double[][] from) {
        return this.projToLatLon(from, new double[2][from[0].length]);
    }
    
    public double[][] projToLatLon(final double[][] from, final double[][] to) {
        if (from == null || from.length != 2) {
            throw new IllegalArgumentException("ProjectionImpl.projToLatLon:null array argument or wrong dimension (from)");
        }
        if (to == null || to.length != 2) {
            throw new IllegalArgumentException("ProjectionImpl.projToLatLon:null array argument or wrong dimension (to)");
        }
        if (from[0].length != to[0].length) {
            throw new IllegalArgumentException("ProjectionImpl.projToLatLon:from array not same length as to array");
        }
        for (int i = 0; i < from[0].length; ++i) {
            final LatLonPoint endL = this.projToLatLon(from[0][i], from[1][i]);
            to[0][i] = endL.getLatitude();
            to[1][i] = endL.getLongitude();
        }
        return to;
    }
    
    public float[][] projToLatLon(final float[][] from) {
        return this.projToLatLon(from, new float[2][from[0].length]);
    }
    
    public float[][] projToLatLon(final float[][] from, final float[][] to) {
        if (from == null || from.length != 2) {
            throw new IllegalArgumentException("ProjectionImpl.projToLatLon:null array argument or wrong dimension (from)");
        }
        if (to == null || to.length != 2) {
            throw new IllegalArgumentException("ProjectionImpl.projToLatLon:null array argument or wrong dimension (to)");
        }
        if (from[0].length != to[0].length) {
            throw new IllegalArgumentException("ProjectionImpl.projToLatLon:from array not same length as to array");
        }
        final ProjectionPointImpl ppi = new ProjectionPointImpl();
        final LatLonPointImpl llpi = new LatLonPointImpl();
        for (int i = 0; i < from[0].length; ++i) {
            ppi.setLocation(from[0][i], from[1][i]);
            this.projToLatLon(ppi, llpi);
            to[0][i] = (float)llpi.getLatitude();
            to[1][i] = (float)llpi.getLongitude();
        }
        return to;
    }
    
    public double[][] latLonToProj(final double[][] from) {
        return this.latLonToProj(from, new double[2][from[0].length]);
    }
    
    public double[][] latLonToProj(final double[][] from, final double[][] to) {
        return this.latLonToProj(from, to, 0, 1);
    }
    
    public double[][] latLonToProj(final double[][] from, final int latIndex, final int lonIndex) {
        return this.latLonToProj(from, new double[2][from[0].length], latIndex, lonIndex);
    }
    
    public double[][] latLonToProj(final double[][] from, final double[][] to, final int latIndex, final int lonIndex) {
        if (from == null || from.length != 2) {
            throw new IllegalArgumentException("ProjectionImpl.latLonToProj:null array argument or wrong dimension (from)");
        }
        if (to == null || to.length != 2) {
            throw new IllegalArgumentException("ProjectionImpl.latLonToProj:null array argument or wrong dimension (to)");
        }
        if (from[0].length != to[0].length) {
            throw new IllegalArgumentException("ProjectionImpl.latLonToProj:from array not same length as to array");
        }
        final ProjectionPointImpl ppi = new ProjectionPointImpl();
        final LatLonPointImpl llpi = new LatLonPointImpl();
        for (int i = 0; i < from[0].length; ++i) {
            llpi.setLatitude(from[latIndex][i]);
            llpi.setLongitude(from[lonIndex][i]);
            this.latLonToProj(llpi, ppi);
            to[0][i] = ppi.getX();
            to[1][i] = ppi.getY();
        }
        return to;
    }
    
    public float[][] latLonToProj(final float[][] from) {
        return this.latLonToProj(from, new float[2][from[0].length]);
    }
    
    public float[][] latLonToProj(final float[][] from, final float[][] to) {
        return this.latLonToProj(from, to, 0, 1);
    }
    
    public float[][] latLonToProj(final float[][] from, final int latIndex, final int lonIndex) {
        return this.latLonToProj(from, new float[2][from[0].length], latIndex, lonIndex);
    }
    
    public float[][] latLonToProj(final float[][] from, final float[][] to, final int latIndex, final int lonIndex) {
        if (from == null || from.length != 2) {
            throw new IllegalArgumentException("ProjectionImpl.latLonToProj:null array argument or wrong dimension (from)");
        }
        if (to == null || to.length != 2) {
            throw new IllegalArgumentException("ProjectionImpl.latLonToProj:null array argument or wrong dimension (to)");
        }
        if (from[0].length != to[0].length) {
            throw new IllegalArgumentException("ProjectionImpl.latLonToProj:from array not same length as to array");
        }
        final ProjectionPointImpl ppi = new ProjectionPointImpl();
        final LatLonPointImpl llpi = new LatLonPointImpl();
        for (int i = 0; i < from[0].length; ++i) {
            llpi.setLatitude(from[latIndex][i]);
            llpi.setLongitude(from[lonIndex][i]);
            this.latLonToProj(llpi, ppi);
            to[0][i] = (float)ppi.getX();
            to[1][i] = (float)ppi.getY();
        }
        return to;
    }
    
    ProjectionRect latLonToProjBB2(final LatLonRect latlonRect) {
        final LatLonPointImpl llpt = latlonRect.getLowerLeftPoint();
        final LatLonPointImpl urpt = latlonRect.getUpperRightPoint();
        final LatLonPointImpl lrpt = latlonRect.getLowerRightPoint();
        final LatLonPointImpl ulpt = latlonRect.getUpperLeftPoint();
        double minx;
        double miny;
        double maxx;
        double maxy;
        if (this.isLatLon()) {
            minx = this.getMinOrMaxLon(llpt.getLongitude(), ulpt.getLongitude(), true);
            miny = Math.min(llpt.getLatitude(), lrpt.getLatitude());
            maxx = this.getMinOrMaxLon(urpt.getLongitude(), lrpt.getLongitude(), false);
            maxy = Math.min(ulpt.getLatitude(), urpt.getLatitude());
        }
        else {
            final ProjectionPoint ll = this.latLonToProj(llpt, new ProjectionPointImpl());
            final ProjectionPoint ur = this.latLonToProj(urpt, new ProjectionPointImpl());
            final ProjectionPoint lr = this.latLonToProj(lrpt, new ProjectionPointImpl());
            final ProjectionPoint ul = this.latLonToProj(ulpt, new ProjectionPointImpl());
            minx = Math.min(ll.getX(), ul.getX());
            miny = Math.min(ll.getY(), lr.getY());
            maxx = Math.max(ur.getX(), lr.getX());
            maxy = Math.max(ul.getY(), ur.getY());
        }
        return new ProjectionRect(minx, miny, maxx, maxy);
    }
    
    private double getMinOrMaxLon(double lon1, double lon2, final boolean wantMin) {
        final double midpoint = (lon1 + lon2) / 2.0;
        lon1 = LatLonPointImpl.lonNormal(lon1, midpoint);
        lon2 = LatLonPointImpl.lonNormal(lon2, midpoint);
        return wantMin ? Math.min(lon1, lon2) : Math.max(lon1, lon2);
    }
    
    public ProjectionRect latLonToProjBB(final LatLonRect latlonRect) {
        if (this.isLatLon) {
            final LatLonProjection llp = (LatLonProjection)this;
            llp.setCenterLon(latlonRect.getCenterLon());
        }
        final ProjectionPointImpl w1 = new ProjectionPointImpl();
        final ProjectionPointImpl w2 = new ProjectionPointImpl();
        final LatLonPoint ll = latlonRect.getLowerLeftPoint();
        final LatLonPoint ur = latlonRect.getUpperRightPoint();
        this.latLonToProj(ll, w1);
        this.latLonToProj(ur, w2);
        final ProjectionRect world = new ProjectionRect(w1.getX(), w1.getY(), w2.getX(), w2.getY());
        final LatLonPointImpl la = new LatLonPointImpl();
        final LatLonPointImpl lb = new LatLonPointImpl();
        la.setLatitude(ur.getLatitude());
        la.setLongitude(ll.getLongitude());
        this.latLonToProj(la, w1);
        world.add(w1);
        lb.setLatitude(ll.getLatitude());
        lb.setLongitude(ur.getLongitude());
        this.latLonToProj(lb, w2);
        world.add(w2);
        return world;
    }
    
    public LatLonRect projToLatLonBB(final ProjectionRect world) {
        final ProjectionPoint min = world.getMinPoint();
        final ProjectionPoint max = world.getMaxPoint();
        final LatLonPointImpl llmin = new LatLonPointImpl();
        final LatLonPointImpl llmax = new LatLonPointImpl();
        this.projToLatLon(min, llmin);
        this.projToLatLon(max, llmax);
        final LatLonRect llbb = new LatLonRect(llmin, llmax);
        final ProjectionPointImpl w1 = new ProjectionPointImpl();
        final ProjectionPointImpl w2 = new ProjectionPointImpl();
        w1.setLocation(min.getX(), max.getY());
        this.projToLatLon(w1, llmin);
        llbb.extend(llmin);
        w2.setLocation(max.getX(), min.getY());
        this.projToLatLon(w2, llmax);
        llbb.extend(llmax);
        return llbb;
    }
    
    public LatLonRect getLatLonBoundingBox(final ProjectionRect bb) {
        final LatLonPointImpl llpt = (LatLonPointImpl)this.projToLatLon(bb.getLowerLeftPoint(), new LatLonPointImpl());
        final LatLonPointImpl lrpt = (LatLonPointImpl)this.projToLatLon(bb.getLowerRightPoint(), new LatLonPointImpl());
        final LatLonPointImpl urpt = (LatLonPointImpl)this.projToLatLon(bb.getUpperRightPoint(), new LatLonPointImpl());
        final LatLonPointImpl ulpt = (LatLonPointImpl)this.projToLatLon(bb.getUpperLeftPoint(), new LatLonPointImpl());
        final boolean includesNorthPole = false;
        final boolean includesSouthPole = false;
        LatLonRect llbb;
        if (includesNorthPole && !includesSouthPole) {
            llbb = new LatLonRect(llpt, new LatLonPointImpl(90.0, 0.0));
            llbb.extend(lrpt);
            llbb.extend(urpt);
            llbb.extend(ulpt);
        }
        else if (includesSouthPole && !includesNorthPole) {
            llbb = new LatLonRect(llpt, new LatLonPointImpl(-90.0, -180.0));
            llbb.extend(lrpt);
            llbb.extend(urpt);
            llbb.extend(ulpt);
        }
        else {
            final double latMin = Math.min(llpt.getLatitude(), lrpt.getLatitude());
            final double latMax = Math.max(ulpt.getLatitude(), urpt.getLatitude());
            final double lonMin = this.getMinOrMaxLon(llpt.getLongitude(), ulpt.getLongitude(), true);
            final double lonMax = this.getMinOrMaxLon(lrpt.getLongitude(), urpt.getLongitude(), false);
            llpt.set(latMin, lonMin);
            urpt.set(latMax, lonMax);
            llbb = new LatLonRect(llpt, urpt);
        }
        return llbb;
    }
    
    static {
        EARTH_RADIUS = Earth.getRadius() * 0.001;
        ProjectionImpl.header = null;
    }
}
