// 
// Decompiled by Procyon v0.5.36
// 

package ucar.unidata.geoloc;

public class StationImpl extends EarthLocationImpl implements Station
{
    protected String name;
    protected String desc;
    protected String wmoId;
    
    protected StationImpl() {
    }
    
    public StationImpl(final String name, final String desc, final String wmoId, final double lat, final double lon, final double alt) {
        super(lat, lon, alt);
        this.name = name;
        this.desc = desc;
        this.wmoId = wmoId;
    }
    
    @Override
    public String getName() {
        return this.name;
    }
    
    @Override
    public String getDescription() {
        return this.desc;
    }
    
    @Override
    public String getWmoId() {
        return this.wmoId;
    }
    
    protected void setName(final String name) {
        this.name = name;
    }
    
    protected void setDescription(final String desc) {
        this.desc = desc;
    }
    
    protected void setWmoId(final String wmoId) {
        this.wmoId = wmoId;
    }
    
    @Override
    public int compareTo(final Station so) {
        return this.name.compareTo(so.getName());
    }
    
    @Override
    public String toString() {
        return "name=" + this.name + " desc=" + this.desc + " " + super.toString();
    }
}
