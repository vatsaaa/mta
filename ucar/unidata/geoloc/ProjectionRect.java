// 
// Decompiled by Procyon v0.5.36
// 

package ucar.unidata.geoloc;

import java.io.ObjectOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import ucar.unidata.util.Format;
import java.io.Serializable;
import java.awt.geom.Rectangle2D;

public class ProjectionRect extends Double implements Serializable
{
    public ProjectionRect() {
        this(-5000.0, -5000.0, 5000.0, 5000.0);
    }
    
    public ProjectionRect(final Rectangle2D r) {
        this(r.getMinX(), r.getMinY(), r.getMaxX(), r.getMaxY());
    }
    
    public ProjectionRect(final double x1, final double y1, final double x2, final double y2) {
        final double wx0 = 0.5 * (x1 + x2);
        final double wy0 = 0.5 * (y1 + y2);
        final double width = Math.abs(x1 - x2);
        final double height = Math.abs(y1 - y2);
        this.setRect(wx0 - width / 2.0, wy0 - height / 2.0, width, height);
    }
    
    public ProjectionPoint getLowerRightPoint() {
        return new ProjectionPointImpl(this.getMaxPoint().getX(), this.getMinPoint().getY());
    }
    
    public ProjectionPoint getUpperRightPoint() {
        return this.getMaxPoint();
    }
    
    public ProjectionPoint getLowerLeftPoint() {
        return this.getMinPoint();
    }
    
    public ProjectionPoint getUpperLeftPoint() {
        return new ProjectionPointImpl(this.getMinPoint().getX(), this.getMaxPoint().getY());
    }
    
    public ProjectionPoint getMinPoint() {
        return new ProjectionPointImpl(this.getX(), this.getY());
    }
    
    public ProjectionPoint getMaxPoint() {
        return new ProjectionPointImpl(this.getX() + this.getWidth(), this.getY() + this.getHeight());
    }
    
    @Override
    public String toString() {
        return "min: " + Format.d(this.getX(), 3) + " " + Format.d(this.getY(), 3) + " size: " + Format.d(this.getWidth(), 3) + " " + Format.d(this.getHeight(), 3);
    }
    
    public void setX(final double x) {
        this.setRect(x, this.getY(), this.getWidth(), this.getHeight());
    }
    
    public void setY(final double y) {
        this.setRect(this.getX(), y, this.getWidth(), this.getHeight());
    }
    
    public void setWidth(final double w) {
        this.setRect(this.getX(), this.getY(), w, this.getHeight());
    }
    
    public void setHeight(final double h) {
        this.setRect(this.getX(), this.getY(), this.getWidth(), h);
    }
    
    private void readObject(final ObjectInputStream s) throws IOException, ClassNotFoundException {
        final double x = s.readDouble();
        final double y = s.readDouble();
        final double w = s.readDouble();
        final double h = s.readDouble();
        this.setRect(x, y, w, h);
    }
    
    private void writeObject(final ObjectOutputStream s) throws IOException {
        s.writeDouble(this.getX());
        s.writeDouble(this.getY());
        s.writeDouble(this.getWidth());
        s.writeDouble(this.getHeight());
    }
}
