// 
// Decompiled by Procyon v0.5.36
// 

package ucar.unidata.geoloc;

public interface Station extends EarthLocation, Comparable<Station>
{
    String getName();
    
    String getWmoId();
    
    String getDescription();
}
