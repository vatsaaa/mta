// 
// Decompiled by Procyon v0.5.36
// 

package ucar.unidata.geoloc;

import ucar.unidata.util.Parameter;
import java.util.List;

public interface Projection
{
    String getClassName();
    
    String getName();
    
    String paramsToString();
    
    ProjectionPoint latLonToProj(final LatLonPoint p0, final ProjectionPointImpl p1);
    
    LatLonPoint projToLatLon(final ProjectionPoint p0, final LatLonPointImpl p1);
    
    boolean crossSeam(final ProjectionPoint p0, final ProjectionPoint p1);
    
    ProjectionRect getDefaultMapArea();
    
    boolean equals(final Object p0);
    
    List<Parameter> getProjectionParameters();
}
