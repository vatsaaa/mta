// 
// Decompiled by Procyon v0.5.36
// 

package ucar.unidata.geoloc;

import ucar.unidata.util.Format;
import java.util.StringTokenizer;

public class LatLonRect
{
    private LatLonPointImpl upperRight;
    private LatLonPointImpl lowerLeft;
    private boolean crossDateline;
    private boolean allLongitude;
    private double width;
    private double lon0;
    private double eps;
    
    public LatLonRect(final LatLonPoint p1, final double deltaLat, final double deltaLon) {
        this.crossDateline = false;
        this.allLongitude = false;
        this.eps = 1.0E-9;
        this.init(p1, deltaLat, deltaLon);
    }
    
    private void init(final LatLonPoint p1, final double deltaLat, final double deltaLon) {
        final double latmin = Math.min(p1.getLatitude(), p1.getLatitude() + deltaLat);
        final double latmax = Math.max(p1.getLatitude(), p1.getLatitude() + deltaLat);
        final double lonpt = p1.getLongitude();
        double lonmin;
        double lonmax;
        if (deltaLon > 0.0) {
            lonmin = lonpt;
            lonmax = lonpt + deltaLon;
            this.crossDateline = (lonmax > 180.0);
        }
        else {
            lonmax = lonpt;
            lonmin = lonpt + deltaLon;
            this.crossDateline = (lonmin < -180.0);
        }
        this.lowerLeft = new LatLonPointImpl(latmin, lonmin);
        this.upperRight = new LatLonPointImpl(latmax, lonmax);
        this.width = Math.abs(deltaLon);
        this.lon0 = LatLonPointImpl.lonNormal(p1.getLongitude() + deltaLon / 2.0);
        this.allLongitude = (this.width >= 360.0);
    }
    
    public LatLonRect(final LatLonPoint left, final LatLonPoint right) {
        this(left, right.getLatitude() - left.getLatitude(), LatLonPointImpl.lonNormal360(right.getLongitude() - left.getLongitude()));
    }
    
    public LatLonRect(final String spec) {
        this.crossDateline = false;
        this.allLongitude = false;
        this.eps = 1.0E-9;
        final StringTokenizer stoker = new StringTokenizer(spec, " ,");
        final int n = stoker.countTokens();
        if (n != 4) {
            throw new IllegalArgumentException("Must be 4 numbers = lat, lon, latWidth, lonWidth");
        }
        final double lat = Double.parseDouble(stoker.nextToken());
        final double lon = Double.parseDouble(stoker.nextToken());
        final double deltaLat = Double.parseDouble(stoker.nextToken());
        final double deltaLon = Double.parseDouble(stoker.nextToken());
        this.init(new LatLonPointImpl(lat, lon), deltaLat, deltaLon);
    }
    
    public LatLonRect(final LatLonRect r) {
        this(r.getLowerLeftPoint(), r.getUpperRightPoint().getLatitude() - r.getLowerLeftPoint().getLatitude(), r.getWidth());
    }
    
    public LatLonRect() {
        this(new LatLonPointImpl(-90.0, -180.0), 180.0, 360.0);
    }
    
    public LatLonPointImpl getUpperRightPoint() {
        return this.upperRight;
    }
    
    public LatLonPointImpl getLowerLeftPoint() {
        return this.lowerLeft;
    }
    
    public LatLonPointImpl getUpperLeftPoint() {
        return new LatLonPointImpl(this.upperRight.getLatitude(), this.lowerLeft.getLongitude());
    }
    
    public LatLonPointImpl getLowerRightPoint() {
        return new LatLonPointImpl(this.lowerLeft.getLatitude(), this.upperRight.getLongitude());
    }
    
    public boolean crossDateline() {
        return this.crossDateline;
    }
    
    public boolean equals(final LatLonRect other) {
        return this.lowerLeft.equals(other.getLowerLeftPoint()) && this.upperRight.equals(other.getUpperRightPoint());
    }
    
    public double getWidth() {
        return this.width;
    }
    
    public double getHeight() {
        return this.getLatMax() - this.getLatMin();
    }
    
    public double getCenterLon() {
        return this.lon0;
    }
    
    public double getLonMin() {
        return this.lowerLeft.getLongitude();
    }
    
    public double getLonMax() {
        return this.lowerLeft.getLongitude() + this.width;
    }
    
    public double getLatMin() {
        return this.lowerLeft.getLatitude();
    }
    
    public double getLatMax() {
        return this.upperRight.getLatitude();
    }
    
    public boolean contains(final LatLonPoint p) {
        return this.contains(p.getLatitude(), p.getLongitude());
    }
    
    public boolean contains(final double lat, final double lon) {
        if (lat + this.eps < this.lowerLeft.getLatitude() || lat - this.eps > this.upperRight.getLatitude()) {
            return false;
        }
        if (this.allLongitude) {
            return true;
        }
        if (this.crossDateline) {
            return lon >= this.lowerLeft.getLongitude() || lon <= this.upperRight.getLongitude();
        }
        return lon >= this.lowerLeft.getLongitude() && lon <= this.upperRight.getLongitude();
    }
    
    public boolean containedIn(final LatLonRect b) {
        return b.getWidth() >= this.width && b.contains(this.upperRight) && b.contains(this.lowerLeft);
    }
    
    public void extend(final LatLonPoint p) {
        if (this.contains(p)) {
            return;
        }
        final double lat = p.getLatitude();
        final double lon = p.getLongitude();
        if (lat > this.upperRight.getLatitude()) {
            this.upperRight.setLatitude(lat);
        }
        if (lat < this.lowerLeft.getLatitude()) {
            this.lowerLeft.setLatitude(lat);
        }
        if (!this.allLongitude) {
            if (this.crossDateline) {
                final double d1 = lon - this.upperRight.getLongitude();
                final double d2 = this.lowerLeft.getLongitude() - lon;
                if (d1 > 0.0 && d2 > 0.0) {
                    if (d1 > d2) {
                        this.lowerLeft.setLongitude(lon);
                    }
                    else {
                        this.upperRight.setLongitude(lon);
                    }
                }
            }
            else if (lon > this.upperRight.getLongitude()) {
                if (lon - this.upperRight.getLongitude() > this.lowerLeft.getLongitude() - lon + 360.0) {
                    this.crossDateline = true;
                    this.lowerLeft.setLongitude(lon);
                }
                else {
                    this.upperRight.setLongitude(lon);
                }
            }
            else if (lon < this.lowerLeft.getLongitude()) {
                if (this.lowerLeft.getLongitude() - lon > lon + 360.0 - this.upperRight.getLongitude()) {
                    this.crossDateline = true;
                    this.upperRight.setLongitude(lon);
                }
                else {
                    this.lowerLeft.setLongitude(lon);
                }
            }
        }
        this.width = this.upperRight.getLongitude() - this.lowerLeft.getLongitude();
        this.lon0 = (this.upperRight.getLongitude() + this.lowerLeft.getLongitude()) / 2.0;
        if (this.crossDateline) {
            this.width += 360.0;
            this.lon0 -= 180.0;
        }
        this.allLongitude = (this.allLongitude || this.width >= 360.0);
    }
    
    public void extend(final LatLonRect r) {
        final double latMin = r.getLatMin();
        final double latMax = r.getLatMax();
        if (latMax > this.upperRight.getLatitude()) {
            this.upperRight.setLatitude(latMax);
        }
        if (latMin < this.lowerLeft.getLatitude()) {
            this.lowerLeft.setLatitude(latMin);
        }
        if (this.allLongitude) {
            return;
        }
        double lonMin = this.getLonMin();
        double lonMax = this.getLonMax();
        final double nlonMin = LatLonPointImpl.lonNormal(r.getLonMin(), lonMin);
        final double nlonMax = nlonMin + r.getWidth();
        lonMin = Math.min(lonMin, nlonMin);
        lonMax = Math.max(lonMax, nlonMax);
        this.width = lonMax - lonMin;
        this.allLongitude = (this.width >= 360.0);
        if (this.allLongitude) {
            this.width = 360.0;
            lonMin = -180.0;
        }
        else {
            lonMin = LatLonPointImpl.lonNormal(lonMin);
        }
        this.lowerLeft.setLongitude(lonMin);
        this.upperRight.setLongitude(lonMin + this.width);
        this.lon0 = lonMin + this.width / 2.0;
        this.crossDateline = (this.lowerLeft.getLongitude() > this.upperRight.getLongitude());
    }
    
    public LatLonRect intersect(final LatLonRect clip) {
        final double latMin = Math.max(this.getLatMin(), clip.getLatMin());
        final double latMax = Math.min(this.getLatMax(), clip.getLatMax());
        final double deltaLat = latMax - latMin;
        if (deltaLat < 0.0) {
            return null;
        }
        final double lon1min = this.getLonMin();
        final double lon1max = this.getLonMax();
        double lon2min = clip.getLonMin();
        double lon2max = clip.getLonMax();
        if (!this.intersect(lon1min, lon1max, lon2min, lon2max)) {
            lon2min = clip.getLonMin() + 360.0;
            lon2max = clip.getLonMax() + 360.0;
            if (!this.intersect(lon1min, lon1max, lon2min, lon2max)) {
                lon2min = clip.getLonMin() - 360.0;
                lon2max = clip.getLonMax() - 360.0;
            }
        }
        final double lonMin = Math.max(lon1min, lon2min);
        final double lonMax = Math.min(lon1max, lon2max);
        final double deltaLon = lonMax - lonMin;
        if (deltaLon < 0.0) {
            return null;
        }
        return new LatLonRect(new LatLonPointImpl(latMin, lonMin), deltaLat, deltaLon);
    }
    
    private boolean intersect(final double min1, final double max1, final double min2, final double max2) {
        final double min3 = Math.max(min1, min2);
        final double max3 = Math.min(max1, max2);
        return min3 < max3;
    }
    
    @Override
    public String toString() {
        return " ll: " + this.lowerLeft + "+ ur: " + this.upperRight;
    }
    
    public String toString2() {
        return " lat= [" + Format.dfrac(this.getLatMin(), 2) + "," + Format.dfrac(this.getLatMax(), 2) + "] lon= [" + Format.dfrac(this.getLonMin(), 2) + "," + Format.dfrac(this.getLonMax(), 2) + "]";
    }
}
