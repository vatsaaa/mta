// 
// Decompiled by Procyon v0.5.36
// 

package ucar.unidata.geoloc;

import java.util.Formatter;
import ucar.unidata.util.Format;
import java.io.Serializable;
import java.awt.geom.Point2D;

public class ProjectionPointImpl extends Double implements ProjectionPoint, Serializable
{
    public ProjectionPointImpl() {
        this(0.0, 0.0);
    }
    
    public ProjectionPointImpl(final Point2D pt) {
        super(pt.getX(), pt.getY());
    }
    
    public ProjectionPointImpl(final ProjectionPoint pt) {
        super(pt.getX(), pt.getY());
    }
    
    public ProjectionPointImpl(final double x, final double y) {
        super(x, y);
    }
    
    @Override
    public boolean equals(final ProjectionPoint pt) {
        return pt.getX() == this.getX() && pt.getY() == this.getY();
    }
    
    @Override
    public String toString() {
        return Format.d(this.getX(), 4) + " " + Format.d(this.getY(), 4);
    }
    
    public void toString(final Formatter f) {
        f.format("x=%f y=%f ", this.getX(), this.getY());
    }
    
    public void setLocation(final ProjectionPoint pt) {
        this.setLocation(pt.getX(), pt.getY());
    }
    
    @Override
    public void setLocation(final Point2D pt) {
        this.setLocation(pt.getX(), pt.getY());
    }
    
    public boolean isInfinite() {
        return this.x == java.lang.Double.POSITIVE_INFINITY || this.x == java.lang.Double.NEGATIVE_INFINITY || this.y == java.lang.Double.POSITIVE_INFINITY || this.y == java.lang.Double.NEGATIVE_INFINITY;
    }
    
    public static boolean isInfinite(final ProjectionPoint pt) {
        return pt.getX() == java.lang.Double.POSITIVE_INFINITY || pt.getX() == java.lang.Double.NEGATIVE_INFINITY || pt.getY() == java.lang.Double.POSITIVE_INFINITY || pt.getY() == java.lang.Double.NEGATIVE_INFINITY;
    }
}
