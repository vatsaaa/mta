// 
// Decompiled by Procyon v0.5.36
// 

package ucar.unidata.geoloc;

public class Bearing
{
    private static final Earth defaultEarth;
    private static double A;
    private static double F;
    private static final double EPS = 5.0E-14;
    private static double R;
    private static final double rad;
    private static final double deg;
    private static int maxLoopCnt;
    private double azimuth;
    private double backazimuth;
    private double distance;
    
    public static Bearing calculateBearing(final Earth e, final LatLonPoint pt1, final LatLonPoint pt2, final Bearing result) {
        return calculateBearing(e, pt1.getLatitude(), pt1.getLongitude(), pt2.getLatitude(), pt2.getLongitude(), result);
    }
    
    public static Bearing calculateBearing(final LatLonPoint pt1, final LatLonPoint pt2, final Bearing result) {
        return calculateBearing(Bearing.defaultEarth, pt1.getLatitude(), pt1.getLongitude(), pt2.getLatitude(), pt2.getLongitude(), result);
    }
    
    public static Bearing calculateBearing(final double lat1, final double lon1, final double lat2, final double lon2, final Bearing result) {
        return calculateBearing(Bearing.defaultEarth, lat1, lon1, lat2, lon2, result);
    }
    
    public static Bearing calculateBearing(final Earth e, final double lat1, final double lon1, final double lat2, final double lon2, Bearing result) {
        if (result == null) {
            result = new Bearing();
        }
        if (lat1 == lat2 && lon1 == lon2) {
            result.distance = 0.0;
            result.azimuth = 0.0;
            result.backazimuth = 0.0;
            return result;
        }
        Bearing.A = e.getMajor();
        Bearing.F = e.getFlattening();
        Bearing.R = 1.0 - Bearing.F;
        final double GLAT1 = Bearing.rad * lat1;
        final double GLAT2 = Bearing.rad * lat2;
        double TU1 = Bearing.R * Math.sin(GLAT1) / Math.cos(GLAT1);
        double TU2 = Bearing.R * Math.sin(GLAT2) / Math.cos(GLAT2);
        final double CU1 = 1.0 / Math.sqrt(TU1 * TU1 + 1.0);
        final double SU1 = CU1 * TU1;
        final double CU2 = 1.0 / Math.sqrt(TU2 * TU2 + 1.0);
        double S = CU1 * CU2;
        double BAZ = S * TU2;
        double FAZ = BAZ * TU1;
        final double GLON1 = Bearing.rad * lon1;
        final double GLON2 = Bearing.rad * lon2;
        double X = GLON2 - GLON1;
        int loopCnt = 0;
        while (++loopCnt <= 1000) {
            final double SX = Math.sin(X);
            final double CX = Math.cos(X);
            TU1 = CU2 * SX;
            TU2 = BAZ - SU1 * CU2 * CX;
            final double SY = Math.sqrt(TU1 * TU1 + TU2 * TU2);
            final double CY = S * CX + FAZ;
            final double Y = Math.atan2(SY, CY);
            final double SA = S * SX / SY;
            final double C2A = -SA * SA + 1.0;
            double CZ = FAZ + FAZ;
            if (C2A > 0.0) {
                CZ = -CZ / C2A + CY;
            }
            final double E = CZ * CZ * 2.0 - 1.0;
            double C = ((-3.0 * C2A + 4.0) * Bearing.F + 4.0) * C2A * Bearing.F / 16.0;
            double D = X;
            X = ((E * CY * C + CZ) * SY * C + Y) * SA;
            X = (1.0 - C) * X * Bearing.F + GLON2 - GLON1;
            if (Math.abs(D - X) <= 5.0E-14) {
                if (loopCnt > Bearing.maxLoopCnt) {
                    Bearing.maxLoopCnt = loopCnt;
                }
                FAZ = Math.atan2(TU1, TU2);
                BAZ = Math.atan2(CU1 * SX, BAZ * CX - SU1 * CU2) + 3.141592653589793;
                X = Math.sqrt((1.0 / Bearing.R / Bearing.R - 1.0) * C2A + 1.0) + 1.0;
                X = (X - 2.0) / X;
                C = 1.0 - X;
                C = (X * X / 4.0 + 1.0) / C;
                D = (0.375 * X * X - 1.0) * X;
                X = E * CY;
                S = 1.0 - E - E;
                S = ((((SY * SY * 4.0 - 3.0) * S * CZ * D / 6.0 - X) * D / 4.0 + CZ) * SY * D + Y) * C * Bearing.A * Bearing.R;
                result.distance = S / 1000.0;
                result.azimuth = FAZ * Bearing.deg;
                if (result.azimuth < 0.0) {
                    final Bearing bearing = result;
                    bearing.azimuth += 360.0;
                }
                result.backazimuth = BAZ * Bearing.deg;
                return result;
            }
        }
        throw new IllegalArgumentException("Too many iterations calculating bearing:" + lat1 + " " + lon1 + " " + lat2 + " " + lon2);
    }
    
    public double getAngle() {
        return this.azimuth;
    }
    
    public double getBackAzimuth() {
        return this.backazimuth;
    }
    
    public double getDistance() {
        return this.distance;
    }
    
    @Override
    public String toString() {
        final StringBuilder buf = new StringBuilder();
        buf.append("Azimuth: ");
        buf.append(this.azimuth);
        buf.append(" Back azimuth: ");
        buf.append(this.backazimuth);
        buf.append(" Distance: ");
        buf.append(this.distance);
        return buf.toString();
    }
    
    public static void main(final String[] args) {
        final LatLonPointImpl pt1 = new LatLonPointImpl(40.0, -105.0);
        final LatLonPointImpl pt2 = new LatLonPointImpl(37.4, -118.4);
        final Bearing b = calculateBearing(pt1, pt2, null);
        System.out.println("Bearing from " + pt1 + " to " + pt2 + " = \n\t" + b);
        LatLonPointImpl pt3 = new LatLonPointImpl();
        pt3 = findPoint(pt1, b.getAngle(), b.getDistance(), pt3);
        System.out.println("using first point, angle and distance, found second point at " + pt3);
        pt3 = findPoint(pt2, b.getBackAzimuth(), b.getDistance(), pt3);
        System.out.println("using second point, backazimuth and distance, found first point at " + pt3);
    }
    
    public static LatLonPointImpl findPoint(final Earth e, final LatLonPoint pt1, final double az, final double dist, final LatLonPointImpl result) {
        return findPoint(e, pt1.getLatitude(), pt1.getLongitude(), az, dist, result);
    }
    
    public static LatLonPointImpl findPoint(final LatLonPoint pt1, final double az, final double dist, final LatLonPointImpl result) {
        return findPoint(Bearing.defaultEarth, pt1.getLatitude(), pt1.getLongitude(), az, dist, result);
    }
    
    public static LatLonPointImpl findPoint(final double lat1, final double lon1, final double az, final double dist, final LatLonPointImpl result) {
        return findPoint(Bearing.defaultEarth, lat1, lon1, az, dist, result);
    }
    
    public static LatLonPointImpl findPoint(final Earth e, final double lat1, final double lon1, double az, final double dist, LatLonPointImpl result) {
        if (result == null) {
            result = new LatLonPointImpl();
        }
        if (dist == 0.0) {
            result.setLatitude(lat1);
            result.setLongitude(lon1);
            return result;
        }
        Bearing.A = e.getMajor();
        Bearing.F = e.getFlattening();
        Bearing.R = 1.0 - Bearing.F;
        if (az < 0.0) {
            az += 360.0;
        }
        final double FAZ = az * Bearing.rad;
        final double GLAT1 = lat1 * Bearing.rad;
        final double GLON1 = lon1 * Bearing.rad;
        final double S = dist * 1000.0;
        double TU = Bearing.R * Math.sin(GLAT1) / Math.cos(GLAT1);
        final double SF = Math.sin(FAZ);
        final double CF = Math.cos(FAZ);
        double BAZ = 0.0;
        if (CF != 0.0) {
            BAZ = Math.atan2(TU, CF) * 2.0;
        }
        final double CU = 1.0 / Math.sqrt(TU * TU + 1.0);
        final double SU = TU * CU;
        final double SA = CU * SF;
        final double C2A = -SA * SA + 1.0;
        double X = Math.sqrt((1.0 / Bearing.R / Bearing.R - 1.0) * C2A + 1.0) + 1.0;
        X = (X - 2.0) / X;
        double C = 1.0 - X;
        C = (X * X / 4.0 + 1.0) / C;
        double D = (0.375 * X * X - 1.0) * X;
        double Y;
        TU = (Y = S / Bearing.R / Bearing.A / C);
        double SY;
        double CY;
        double CZ;
        double E;
        do {
            SY = Math.sin(Y);
            CY = Math.cos(Y);
            CZ = Math.cos(BAZ + Y);
            E = CZ * CZ * 2.0 - 1.0;
            C = Y;
            X = E * CY;
            Y = E + E - 1.0;
            Y = (((SY * SY * 4.0 - 3.0) * Y * CZ * D / 6.0 + X) * D / 4.0 - CZ) * SY * D + TU;
        } while (Math.abs(Y - C) > 5.0E-14);
        BAZ = CU * CY * CF - SU * SY;
        C = Bearing.R * Math.sqrt(SA * SA + BAZ * BAZ);
        D = SU * CY + CU * SY * CF;
        final double GLAT2 = Math.atan2(D, C);
        C = CU * CY - SU * SY * CF;
        X = Math.atan2(SY * SF, C);
        C = ((-3.0 * C2A + 4.0) * Bearing.F + 4.0) * C2A * Bearing.F / 16.0;
        D = ((E * CY * C + CZ) * SY * C + Y) * SA;
        final double GLON2 = GLON1 + X - (1.0 - C) * D * Bearing.F;
        BAZ = (Math.atan2(SA, BAZ) + 3.141592653589793) * Bearing.deg;
        result.setLatitude(GLAT2 * Bearing.deg);
        result.setLongitude(GLON2 * Bearing.deg);
        return result;
    }
    
    static {
        defaultEarth = new Earth(6378137.0, 0.0, 298.257223563);
        rad = Math.toRadians(1.0);
        deg = Math.toDegrees(1.0);
        Bearing.maxLoopCnt = 0;
    }
}
