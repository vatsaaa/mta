// 
// Decompiled by Procyon v0.5.36
// 

package ucar.unidata.geoloc;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Collection;
import java.util.Map;

public final class EarthEllipsoid extends Earth
{
    public static final EarthEllipsoid WGS84;
    public static final EarthEllipsoid Airy1830;
    private static Map<String, EarthEllipsoid> hash;
    private String name;
    private int epsgId;
    
    public static Collection<EarthEllipsoid> getAll() {
        if (EarthEllipsoid.hash == null) {
            EarthEllipsoid.hash = new LinkedHashMap<String, EarthEllipsoid>(10);
        }
        return EarthEllipsoid.hash.values();
    }
    
    public static EarthEllipsoid getType(final String name) {
        if (name == null) {
            return null;
        }
        if (EarthEllipsoid.hash == null) {
            EarthEllipsoid.hash = new LinkedHashMap<String, EarthEllipsoid>(10);
        }
        return EarthEllipsoid.hash.get(name);
    }
    
    public static EarthEllipsoid getType(final int epsgId) {
        final Collection<EarthEllipsoid> all = getAll();
        for (final EarthEllipsoid ellipsoid : all) {
            if (ellipsoid.epsgId == epsgId) {
                return ellipsoid;
            }
        }
        return null;
    }
    
    private EarthEllipsoid(final String name, final int epsgId, final double a, final double invF) {
        super(a, 0.0, invF);
        this.name = name;
        this.epsgId = epsgId;
        if (EarthEllipsoid.hash == null) {
            EarthEllipsoid.hash = new LinkedHashMap<String, EarthEllipsoid>(10);
        }
        EarthEllipsoid.hash.put(name, this);
    }
    
    @Override
    public String getName() {
        return this.name;
    }
    
    public int getEpsgId() {
        return this.epsgId;
    }
    
    @Override
    public String toString() {
        return this.name;
    }
    
    @Override
    public int hashCode() {
        return this.name.hashCode();
    }
    
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof EarthEllipsoid)) {
            return false;
        }
        final EarthEllipsoid oe = (EarthEllipsoid)o;
        return oe.name.equals(this.name);
    }
    
    static {
        WGS84 = new EarthEllipsoid("WGS84", 7030, 6378137.0, 298.257223563);
        Airy1830 = new EarthEllipsoid("Airy 1830", 7001, 6377563.396, 299.3249646);
    }
}
