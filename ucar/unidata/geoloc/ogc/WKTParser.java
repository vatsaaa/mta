// 
// Decompiled by Procyon v0.5.36
// 

package ucar.unidata.geoloc.ogc;

import ucar.unidata.geoloc.projection.Mercator;
import ucar.unidata.geoloc.projection.Stereographic;
import ucar.unidata.geoloc.projection.AlbersEqualArea;
import ucar.unidata.geoloc.projection.LambertConformal;
import ucar.unidata.geoloc.projection.TransverseMercator;
import ucar.nc2.units.SimpleUnit;
import ucar.unidata.geoloc.projection.LatLonProjection;
import ucar.unidata.geoloc.ProjectionImpl;
import java.io.IOException;
import java.text.ParseException;
import java.io.StringReader;
import java.util.HashMap;

public class WKTParser
{
    private String geogcsName;
    private String datumName;
    private String spheroidName;
    private double majorAxis;
    private double inverseMinor;
    private String primeMeridianName;
    private double primeMeridianValue;
    private String geogUnitName;
    private double geogUnitValue;
    private boolean isAProjection;
    private String projName;
    private String projectionType;
    private HashMap parameters;
    private String projUnitName;
    private double projUnitValue;
    private int position;
    StringReader reader;
    
    public WKTParser(final String srtext) throws ParseException {
        this.parameters = new HashMap();
        this.position = 0;
        this.reader = new StringReader(srtext);
        if (srtext.startsWith("PROJCS")) {
            this.isAProjection = true;
            this.parseProjcs();
        }
        else {
            this.isAProjection = false;
            this.parseGeogcs();
        }
    }
    
    private char peek() throws ParseException {
        try {
            this.reader.mark(10);
            final int aChar = this.reader.read();
            this.reader.reset();
            if (aChar < 0) {
                return '\0';
            }
            return (char)aChar;
        }
        catch (IOException e1) {
            throw new ParseException("Strange io error " + e1, this.position);
        }
    }
    
    private char getChar() throws ParseException {
        try {
            final int val = this.reader.read();
            ++this.position;
            if (val < 0) {
                throw new ParseException("unexpected eof of srtext", this.position);
            }
            return (char)val;
        }
        catch (IOException e1) {
            throw new ParseException(e1.toString(), this.position);
        }
    }
    
    private void eatLiteral(final String literal) throws ParseException {
        for (int n = literal.length(), i = 0; i < n; ++i) {
            final char v = this.getChar();
            if (v != literal.charAt(i)) {
                throw new ParseException("bad srtext", this.position);
            }
        }
    }
    
    private double eatReal() throws ParseException {
        final StringBuffer b = new StringBuffer();
        while (true) {
            final char t = this.peek();
            if (!Character.isDigit(t) && t != 'e' && t != 'E' && t != '.' && t != '-' && t != '+') {
                break;
            }
            b.append(this.getChar());
        }
        try {
            return Double.parseDouble(b.toString());
        }
        catch (NumberFormatException e1) {
            throw new ParseException("bad number" + e1, this.position);
        }
    }
    
    private String eatString() throws ParseException {
        final StringBuffer b = new StringBuffer();
        if (this.getChar() != '\"') {
            throw new ParseException("expected string", this.position);
        }
        while (true) {
            final char t = this.getChar();
            if (t == '\"') {
                break;
            }
            b.append(t);
        }
        return b.toString();
    }
    
    private String eatTerm() throws ParseException {
        final StringBuffer b = new StringBuffer();
        while (true) {
            final char val = this.peek();
            if (!Character.isJavaIdentifierPart(val)) {
                break;
            }
            b.append(this.getChar());
        }
        return b.toString();
    }
    
    private void eatComma() throws ParseException {
        if (this.getChar() != ',') {
            throw new ParseException("expected comma", this.position);
        }
    }
    
    private void eatOpenBrace() throws ParseException {
        if (this.getChar() != '[') {
            throw new ParseException("expected [", this.position);
        }
    }
    
    private void eatCloseBrace() throws ParseException {
        if (this.getChar() != ']') {
            throw new ParseException("expected ]", this.position);
        }
    }
    
    private void parseProjcs() throws ParseException {
        this.eatLiteral("PROJCS[");
        this.projName = this.eatString();
        this.eatComma();
        this.parseGeogcs();
        while (true) {
            final char next = this.getChar();
            if (next == ']') {
                return;
            }
            if (next != ',') {
                throw new ParseException("expected , or ]", this.position);
            }
            final String term = this.eatTerm();
            if ("PARAMETER".equals(term)) {
                this.eatParameter();
            }
            else if ("UNIT".equals(term)) {
                this.eatProjcsUnit();
            }
            else {
                if (!"PROJECTION".equals(term)) {
                    continue;
                }
                this.eatProjectionType();
            }
        }
    }
    
    private void eatParameter() throws ParseException {
        this.eatOpenBrace();
        final String parameterName = this.eatString();
        this.eatComma();
        final Double value = new Double(this.eatReal());
        this.eatCloseBrace();
        this.parameters.put(parameterName.toLowerCase(), value);
    }
    
    private void eatProjcsUnit() throws ParseException {
        this.eatOpenBrace();
        this.projUnitName = this.eatString();
        this.eatComma();
        this.projUnitValue = this.eatReal();
        this.eatCloseBrace();
    }
    
    private void eatProjectionType() throws ParseException {
        this.eatOpenBrace();
        this.projectionType = this.eatString();
        this.eatCloseBrace();
    }
    
    private void parseGeogcs() throws ParseException {
        this.eatLiteral("GEOGCS[");
        this.geogcsName = this.eatString();
        while (true) {
            final char t = this.getChar();
            if (t == ']') {
                return;
            }
            if (t != ',') {
                throw new ParseException("expected , or ]", this.position);
            }
            final String term = this.eatTerm();
            if ("DATUM".equals(term)) {
                this.eatDatum();
            }
            else if ("PRIMEM".equals(term)) {
                this.eatPrimem();
            }
            else {
                if (!"UNIT".equals(term)) {
                    continue;
                }
                this.eatUnit();
            }
        }
    }
    
    private void eatDatum() throws ParseException {
        this.eatOpenBrace();
        this.datumName = this.eatString();
        this.eatComma();
        this.eatSpheroid();
        this.eatCloseBrace();
    }
    
    private void eatPrimem() throws ParseException {
        this.eatOpenBrace();
        this.primeMeridianName = this.eatString();
        this.eatComma();
        this.primeMeridianValue = this.eatReal();
        this.eatCloseBrace();
    }
    
    private void eatSpheroid() throws ParseException {
        this.eatLiteral("SPHEROID");
        this.eatOpenBrace();
        this.spheroidName = this.eatString();
        this.eatComma();
        this.majorAxis = this.eatReal();
        this.eatComma();
        this.inverseMinor = this.eatReal();
        this.eatCloseBrace();
    }
    
    private void eatUnit() throws ParseException {
        this.eatOpenBrace();
        this.geogUnitName = this.eatString();
        this.eatComma();
        this.geogUnitValue = this.eatReal();
        this.eatCloseBrace();
    }
    
    public String getGeogcsName() {
        return this.geogcsName;
    }
    
    public String getDatumName() {
        return this.datumName;
    }
    
    public String getSpheroidName() {
        return this.spheroidName;
    }
    
    public double getMajorAxis() {
        return this.majorAxis;
    }
    
    public double getInverseFlattening() {
        return this.inverseMinor;
    }
    
    public String getPrimeMeridianName() {
        return this.primeMeridianName;
    }
    
    public double getPrimeMeridianValue() {
        return this.primeMeridianValue;
    }
    
    public String getGeogUnitName() {
        return this.geogUnitName;
    }
    
    public double getGeogUnitValue() {
        return this.geogUnitValue;
    }
    
    public boolean hasParameter(final String name) {
        return this.parameters.get(name.toLowerCase()) != null;
    }
    
    public double getParameter(final String name) {
        final Double val = this.parameters.get(name.toLowerCase());
        if (val == null) {
            throw new IllegalArgumentException("no parameter called " + name);
        }
        return val;
    }
    
    public boolean isPlanarProjection() {
        return this.isAProjection;
    }
    
    public String getProjName() {
        return this.projName;
    }
    
    public String getProjectionType() {
        return this.projectionType;
    }
    
    public String getProjUnitName() {
        return this.projUnitName;
    }
    
    public double getProjUnitValue() {
        return this.projUnitValue;
    }
    
    public static ProjectionImpl convertWKTToProjection(final WKTParser srp) {
        if (!srp.isPlanarProjection()) {
            return new LatLonProjection();
        }
        final String projectionType = srp.getProjectionType();
        double falseEasting = 0.0;
        double falseNorthing = 0.0;
        ProjectionImpl proj = null;
        if (srp.hasParameter("False_Easting")) {
            falseEasting = srp.getParameter("False_Easting");
        }
        if (srp.hasParameter("False_Northing")) {
            falseNorthing = srp.getParameter("False_Northing");
        }
        if (falseEasting != 0.0 || falseNorthing != 0.0) {
            double scalef = 1.0;
            if (srp.getProjUnitName() != null) {
                try {
                    final SimpleUnit unit = SimpleUnit.factoryWithExceptions(srp.getProjUnitName());
                    scalef = unit.convertTo(srp.getProjUnitValue(), SimpleUnit.kmUnit);
                }
                catch (Exception e) {
                    System.out.println(srp.getProjUnitValue() + " " + srp.getProjUnitName() + " not convertible to km");
                }
            }
            falseEasting *= scalef;
            falseNorthing *= scalef;
        }
        if ("Transverse_Mercator".equals(projectionType)) {
            final double lat0 = srp.getParameter("Latitude_Of_Origin");
            final double scale = srp.getParameter("Scale_Factor");
            final double tangentLon = srp.getParameter("Central_Meridian");
            proj = new TransverseMercator(lat0, tangentLon, scale, falseEasting, falseNorthing);
        }
        else {
            if ("Lambert_Conformal_Conic".equals(projectionType)) {
                final double lon0 = srp.getParameter("Central_Meridian");
                double par2;
                final double par1 = par2 = srp.getParameter("Standard_Parallel_1");
                if (srp.hasParameter("Standard_Parallel_2")) {
                    par2 = srp.getParameter("Standard_Parallel_2");
                }
                final double lat2 = srp.getParameter("Latitude_Of_Origin");
                return new LambertConformal(lat2, lon0, par1, par2, falseEasting, falseNorthing);
            }
            if ("Albers".equals(projectionType)) {
                final double lon0 = srp.getParameter("Central_Meridian");
                double par2;
                final double par1 = par2 = srp.getParameter("Standard_Parallel_1");
                if (srp.hasParameter("Standard_Parallel_2")) {
                    par2 = srp.getParameter("Standard_Parallel_2");
                }
                final double lat2 = srp.getParameter("Latitude_Of_Origin");
                return new AlbersEqualArea(lat2, lon0, par1, par2, falseEasting, falseNorthing);
            }
            if ("Stereographic".equals(projectionType)) {
                final double lont = srp.getParameter("Central_Meridian");
                final double scale = srp.getParameter("Scale_Factor");
                final double latt = srp.getParameter("Latitude_Of_Origin");
                return new Stereographic(latt, lont, scale, falseEasting, falseNorthing);
            }
            if ("Mercator".equals(projectionType)) {
                final double lat0 = srp.getParameter("Latitude_Of_Origin");
                final double lon2 = srp.getParameter("Central_Meridian");
                proj = new Mercator(lon2, lat0, falseEasting, falseNorthing);
            }
            else if ("Universal_Transverse_Mercator".equals(projectionType)) {}
        }
        return proj;
    }
}
