// 
// Decompiled by Procyon v0.5.36
// 

package ucar.unidata.geoloc.ogc;

import org.jdom.Content;
import org.jdom.Element;
import ucar.unidata.geoloc.Projection;
import org.jdom.Document;
import java.io.IOException;
import java.io.Writer;
import org.jdom.output.XMLOutputter;
import org.jdom.output.Format;
import ucar.nc2.dataset.CoordinateSystem;
import java.io.PrintWriter;
import org.slf4j.LoggerFactory;
import org.jdom.Namespace;
import org.slf4j.Logger;

public class CoordRefSysToGML
{
    private Logger logger;
    protected static final Namespace gmlNS;
    protected static final Namespace xlinkNS;
    
    public CoordRefSysToGML() {
        this.logger = LoggerFactory.getLogger(CoordRefSysToGML.class);
    }
    
    public static void writeCoordRefSysAsGML(final PrintWriter pw, final CoordinateSystem coordSys) throws IOException {
        final XMLOutputter xmlOutputter = new XMLOutputter(Format.getPrettyFormat());
        xmlOutputter.output(genCoordRefSysAsGML(coordSys), pw);
    }
    
    public static Document genCoordRefSysAsGML(final CoordinateSystem coordSys) {
        if (coordSys == null) {
            throw new IllegalArgumentException("CoordinateSystem must be non-null.");
        }
        if (!coordSys.isGeoReferencing()) {
            throw new IllegalArgumentException("CoordinateSystem must be a georeferencing CS.");
        }
        if (coordSys.isGeoXY()) {
            final Element xyCrsElem = genProjectedCRS(coordSys.getProjection());
        }
        else {
            coordSys.getLatAxis();
            coordSys.getLonAxis();
        }
        final Element rootElem = new Element("CompoundCRS", CoordRefSysToGML.gmlNS);
        rootElem.addContent("");
        rootElem.addNamespaceDeclaration(CoordRefSysToGML.gmlNS);
        rootElem.addNamespaceDeclaration(CoordRefSysToGML.xlinkNS);
        return new Document(rootElem);
    }
    
    public static Element genProjectedCRS(final Projection proj) {
        final Element projElem = new Element("ProjectedCRS", CoordRefSysToGML.gmlNS);
        projElem.setAttribute("id", "", CoordRefSysToGML.gmlNS);
        projElem.addContent(new Element("identifier", CoordRefSysToGML.gmlNS).setAttribute("codeSpace", "", CoordRefSysToGML.gmlNS).addContent(""));
        projElem.addContent(new Element("name", CoordRefSysToGML.gmlNS).addContent(proj.getName()));
        projElem.addContent(new Element("scope", CoordRefSysToGML.gmlNS).addContent(""));
        projElem.addContent(new Element("conversion", CoordRefSysToGML.gmlNS).addContent(""));
        return projElem;
    }
    
    static {
        gmlNS = Namespace.getNamespace("http://www.opengis.net/gml");
        xlinkNS = Namespace.getNamespace("xlink", "http://www.w3.org/1999/xlink");
    }
}
