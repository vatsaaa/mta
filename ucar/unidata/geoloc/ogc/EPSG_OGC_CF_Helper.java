// 
// Decompiled by Procyon v0.5.36
// 

package ucar.unidata.geoloc.ogc;

import org.slf4j.LoggerFactory;
import ucar.nc2.Variable;
import ucar.nc2.Attribute;
import ucar.nc2.dt.GridDataset;
import ucar.nc2.dt.GridDatatype;
import java.util.Iterator;
import ucar.unidata.util.Parameter;
import ucar.unidata.geoloc.Projection;
import org.slf4j.Logger;

public class EPSG_OGC_CF_Helper
{
    private static Logger log;
    
    public static String getWcs1_0CrsId(final Projection proj) {
        String paramName = null;
        if (proj == null) {
            paramName = "LatLon";
        }
        else {
            for (final Parameter curParam : proj.getProjectionParameters()) {
                if (curParam.getName().equalsIgnoreCase("grid_mapping_name") && curParam.isString()) {
                    paramName = curParam.getStringValue();
                }
            }
        }
        if (paramName == null) {
            return null;
        }
        if (paramName.equalsIgnoreCase("LatLon")) {
            paramName = "latitude_longitude";
            return "OGC:CRS84";
        }
        final ProjectionStandardsInfo psi = ProjectionStandardsInfo.getProjectionByCfName(paramName);
        return "EPSG:" + psi.getEpsgCode() + "[" + psi.name() + "]";
    }
    
    public String getWcs1_0CrsId(final GridDatatype gridDatatype, final GridDataset gridDataset) throws IllegalArgumentException {
        gridDataset.getTitle();
        gridDatatype.getName();
        final StringBuilder buf = new StringBuilder();
        final Attribute gridMappingAtt = gridDatatype.findAttributeIgnoreCase("grid_mapping");
        final String gridMapping = gridMappingAtt.getStringValue();
        final Variable gridMapVar = gridDataset.getNetcdfFile().findTopVariable(gridMapping);
        final Attribute gridMappingNameAtt = gridMapVar.findAttributeIgnoreCase("grid_mapping_name");
        final String gridMappingName = gridMappingNameAtt.getStringValue();
        buf.append("EPSG:").append(ProjectionStandardsInfo.getProjectionByCfName(gridMappingName));
        return buf.toString();
    }
    
    static {
        EPSG_OGC_CF_Helper.log = LoggerFactory.getLogger(EPSG_OGC_CF_Helper.class);
    }
    
    public enum ProjectionStandardsInfo
    {
        Albers_Conic_Equal_Area(9822, "Albers Equal Area", "albers_conical_equal_area"), 
        Azimuthal_Equidistant(-1, "", "azimuthal_equidistant"), 
        Lambert_Azimuthal_Equal_Area(9820, "Lambert Azimuthal Equal Area", "lambert_azimuthal_equal_area"), 
        Lambert_Conformal_Conic_2SP(9802, "Lambert Conic Conformal (2SP)", "lambert_conformal_conic"), 
        Polar_Stereographic(9810, "Polar Stereographic (Variant A)", "polar_stereographic"), 
        Rotated_Pole(-2, "", "rotated_latitude_longitude"), 
        Stereographic(-3, "", "stereographic"), 
        Transverse_Mercator(9807, "Transverse Mercator", "transverse_mercator"), 
        Latitude_Longitude(0, "", "latitude_longitude"), 
        Vertical_Perspective(9838, "Vertical Perspective", "vertical_perspective"), 
        Lambert_Cylindrical_Equal_Area(9835, "Lambert Cylindrical Equal Area", "lambert_cylindrical_equal_area"), 
        Mercator(9805, "Mercator (2SP)", "mercator"), 
        Orthographic(9840, "Orthographic", "orthographic");
        
        private final int epsgCode;
        private final String epsgName;
        private final String cfName;
        
        public String getOgcName() {
            return this.name();
        }
        
        public int getEpsgCode() {
            return this.epsgCode;
        }
        
        public String getEpsgName() {
            return this.epsgName;
        }
        
        public String getCfName() {
            return this.cfName;
        }
        
        private ProjectionStandardsInfo(final int epsgCode, final String epsgName, final String cfName) {
            this.epsgCode = epsgCode;
            this.epsgName = epsgName;
            this.cfName = cfName;
        }
        
        @Override
        public String toString() {
            final StringBuilder buf = new StringBuilder();
            buf.append("[[OGC: ").append(this.name()).append("] [EPSG ").append(this.getEpsgCode()).append(": ").append(this.getEpsgName()).append("] [CF: ").append(this.getCfName()).append("]]");
            return buf.toString();
        }
        
        public static ProjectionStandardsInfo getProjectionByOgcName(final String ogcName) {
            for (final ProjectionStandardsInfo curProjStdInfo : values()) {
                if (curProjStdInfo.name().equals(ogcName)) {
                    return curProjStdInfo;
                }
            }
            throw new IllegalArgumentException("No such instance <" + ogcName + ">.");
        }
        
        public static ProjectionStandardsInfo getProjectionByEpsgCode(final int epsgCode) {
            for (final ProjectionStandardsInfo curProjStdInfo : values()) {
                if (curProjStdInfo.getEpsgCode() == epsgCode) {
                    return curProjStdInfo;
                }
            }
            throw new IllegalArgumentException("No such instance <" + epsgCode + ">.");
        }
        
        public static ProjectionStandardsInfo getProjectionByEpsgName(final String epsgName) {
            for (final ProjectionStandardsInfo curProjStdInfo : values()) {
                if (curProjStdInfo.getEpsgName().equals(epsgName)) {
                    return curProjStdInfo;
                }
            }
            throw new IllegalArgumentException("No such instance <" + epsgName + ">.");
        }
        
        public static ProjectionStandardsInfo getProjectionByCfName(final String cfName) {
            for (final ProjectionStandardsInfo curProjStdInfo : values()) {
                if (curProjStdInfo.getCfName().equals(cfName)) {
                    return curProjStdInfo;
                }
            }
            throw new IllegalArgumentException("No such instance <" + cfName + ">.");
        }
    }
}
