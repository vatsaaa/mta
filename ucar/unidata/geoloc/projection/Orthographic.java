// 
// Decompiled by Procyon v0.5.36
// 

package ucar.unidata.geoloc.projection;

import ucar.unidata.geoloc.LatLonPoint;
import ucar.unidata.geoloc.ProjectionPointImpl;
import ucar.unidata.geoloc.ProjectionPoint;
import ucar.unidata.geoloc.LatLonPointImpl;
import ucar.unidata.geoloc.ProjectionImpl;

public class Orthographic extends ProjectionImpl
{
    private double R;
    private double lon0Degrees;
    private double lat0;
    private double lon0;
    private double cosLat0;
    private double sinLat0;
    private LatLonPointImpl origin;
    private boolean spherical;
    
    @Override
    public ProjectionImpl constructCopy() {
        return new Orthographic(this.getOriginLat(), this.getOriginLon(), this.R);
    }
    
    public Orthographic() {
        this(0.0, 0.0);
    }
    
    public Orthographic(final double lat0, final double lon0) {
        this(lat0, lon0, Orthographic.EARTH_RADIUS);
    }
    
    public Orthographic(final double lat0, final double lon0, final double earthRadius) {
        this.spherical = true;
        this.lat0 = Math.toRadians(lat0);
        this.lon0 = Math.toRadians(lon0);
        this.R = earthRadius;
        this.origin = new LatLonPointImpl(lat0, lon0);
        this.precalculate();
        this.addParameter("grid_mapping_name", "orthographic");
        this.addParameter("latitude_of_projection_origin", lat0);
        this.addParameter("longitude_of_projection_origin", lon0);
    }
    
    private void precalculate() {
        this.sinLat0 = Math.sin(this.lat0);
        this.cosLat0 = Math.cos(this.lat0);
        this.lon0Degrees = Math.toDegrees(this.lon0);
    }
    
    @Override
    public Object clone() {
        final Orthographic cl = (Orthographic)super.clone();
        cl.origin = new LatLonPointImpl(this.getOriginLat(), this.getOriginLon());
        return cl;
    }
    
    @Override
    public boolean equals(final Object proj) {
        if (!(proj instanceof Orthographic)) {
            return false;
        }
        final Orthographic oo = (Orthographic)proj;
        return this.getOriginLat() == oo.getOriginLat() && this.getOriginLon() == oo.getOriginLon() && this.defaultMapArea.equals(oo.defaultMapArea);
    }
    
    public double getOriginLon() {
        return this.origin.getLongitude();
    }
    
    public void setOriginLon(final double lon) {
        this.origin.setLongitude(lon);
        this.lon0 = Math.toRadians(lon);
        this.precalculate();
    }
    
    public double getOriginLat() {
        return this.origin.getLatitude();
    }
    
    public void setOriginLat(final double lat) {
        this.origin.setLatitude(lat);
        this.lat0 = Math.toRadians(lat);
        this.precalculate();
    }
    
    @Override
    public String getProjectionTypeLabel() {
        return "Orthographic";
    }
    
    @Override
    public String paramsToString() {
        return " origin " + this.origin.toString();
    }
    
    @Override
    public boolean crossSeam(final ProjectionPoint pt1, final ProjectionPoint pt2) {
        return ProjectionPointImpl.isInfinite(pt1) || ProjectionPointImpl.isInfinite(pt2) || (pt1.getX() * pt2.getX() < 0.0 && Math.abs(pt1.getX() - pt2.getX()) > 5000.0);
    }
    
    @Override
    public ProjectionPoint latLonToProj(final LatLonPoint latLon, final ProjectionPointImpl result) {
        double fromLat = latLon.getLatitude();
        final double fromLon = latLon.getLongitude();
        fromLat = Math.toRadians(fromLat);
        final double lonDiff = Math.toRadians(LatLonPointImpl.lonNormal(fromLon - this.lon0Degrees));
        final double cosc = this.sinLat0 * Math.sin(fromLat) + this.cosLat0 * Math.cos(fromLat) * Math.cos(lonDiff);
        double toX;
        double toY;
        if (cosc >= 0.0) {
            toX = this.R * Math.cos(fromLat) * Math.sin(lonDiff);
            toY = this.R * (this.cosLat0 * Math.sin(fromLat) - this.sinLat0 * Math.cos(fromLat) * Math.cos(lonDiff));
        }
        else {
            toX = Double.POSITIVE_INFINITY;
            toY = Double.POSITIVE_INFINITY;
        }
        result.setLocation(toX, toY);
        return result;
    }
    
    @Override
    public LatLonPoint projToLatLon(final ProjectionPoint world, final LatLonPointImpl result) {
        final double fromX = world.getX();
        final double fromY = world.getY();
        final double rho = Math.sqrt(fromX * fromX + fromY * fromY);
        final double c = Math.asin(rho / this.R);
        double toLon = this.lon0;
        double temp = 0.0;
        double toLat;
        if (Math.abs(rho) > 1.0E-6) {
            toLat = Math.asin(Math.cos(c) * this.sinLat0 + fromY * Math.sin(c) * this.cosLat0 / rho);
            if (Math.abs(this.lat0 - 0.7853981633974483) > 1.0E-6) {
                temp = rho * this.cosLat0 * Math.cos(c) - fromY * this.sinLat0 * Math.sin(c);
                toLon = this.lon0 + Math.atan(fromX * Math.sin(c) / temp);
            }
            else if (this.lat0 == 0.7853981633974483) {
                toLon = this.lon0 + Math.atan(fromX / -fromY);
                temp = -fromY;
            }
            else {
                toLon = this.lon0 + Math.atan(fromX / fromY);
                temp = fromY;
            }
        }
        else {
            toLat = this.lat0;
        }
        toLat = Math.toDegrees(toLat);
        toLon = Math.toDegrees(toLon);
        if (temp < 0.0) {
            toLon += 180.0;
        }
        toLon = LatLonPointImpl.lonNormal(toLon);
        result.setLatitude(toLat);
        result.setLongitude(toLon);
        return result;
    }
    
    @Override
    public float[][] latLonToProj(final float[][] from, final float[][] to, final int latIndex, final int lonIndex) {
        final int cnt = from[0].length;
        final float[] fromLatA = from[latIndex];
        final float[] fromLonA = from[lonIndex];
        final float[] resultXA = to[0];
        final float[] resultYA = to[1];
        for (int i = 0; i < cnt; ++i) {
            double fromLat = fromLatA[i];
            final double fromLon = fromLonA[i];
            fromLat = Math.toRadians(fromLat);
            final double lonDiff = Math.toRadians(LatLonPointImpl.lonNormal(fromLon - this.lon0Degrees));
            final double cosc = this.sinLat0 * Math.sin(fromLat) + this.cosLat0 * Math.cos(fromLat) * Math.cos(lonDiff);
            double toX;
            double toY;
            if (cosc >= 0.0) {
                toX = this.R * Math.cos(fromLat) * Math.sin(lonDiff);
                toY = this.R * (this.cosLat0 * Math.sin(fromLat) - this.sinLat0 * Math.cos(fromLat) * Math.cos(lonDiff));
            }
            else {
                toX = Double.POSITIVE_INFINITY;
                toY = Double.POSITIVE_INFINITY;
            }
            resultXA[i] = (float)toX;
            resultYA[i] = (float)toY;
        }
        return to;
    }
    
    @Override
    public float[][] projToLatLon(final float[][] from, final float[][] to) {
        final int cnt = from[0].length;
        final float[] fromXA = from[0];
        final float[] fromYA = from[1];
        final float[] toLatA = to[0];
        final float[] toLonA = to[1];
        for (int i = 0; i < cnt; ++i) {
            final double fromX = fromXA[i];
            final double fromY = fromYA[i];
            final double rho = Math.sqrt(fromX * fromX + fromY * fromY);
            final double c = Math.asin(rho / this.R);
            double toLon = this.lon0;
            double temp = 0.0;
            double toLat;
            if (Math.abs(rho) > 1.0E-6) {
                toLat = Math.asin(Math.cos(c) * this.sinLat0 + fromY * Math.sin(c) * this.cosLat0 / rho);
                if (Math.abs(this.lat0 - 0.7853981633974483) > 1.0E-6) {
                    temp = rho * this.cosLat0 * Math.cos(c) - fromY * this.sinLat0 * Math.sin(c);
                    toLon = this.lon0 + Math.atan(fromX * Math.sin(c) / temp);
                }
                else if (this.lat0 == 0.7853981633974483) {
                    toLon = this.lon0 + Math.atan(fromX / -fromY);
                    temp = -fromY;
                }
                else {
                    toLon = this.lon0 + Math.atan(fromX / fromY);
                    temp = fromY;
                }
            }
            else {
                toLat = this.lat0;
            }
            toLat = Math.toDegrees(toLat);
            toLon = Math.toDegrees(toLon);
            if (temp < 0.0) {
                toLon += 180.0;
            }
            toLon = LatLonPointImpl.lonNormal(toLon);
            toLatA[i] = (float)toLat;
            toLonA[i] = (float)toLon;
        }
        return to;
    }
    
    @Override
    public double[][] latLonToProj(final double[][] from, final double[][] to, final int latIndex, final int lonIndex) {
        final int cnt = from[0].length;
        final double[] fromLatA = from[latIndex];
        final double[] fromLonA = from[lonIndex];
        final double[] resultXA = to[0];
        final double[] resultYA = to[1];
        for (int i = 0; i < cnt; ++i) {
            double fromLat = fromLatA[i];
            final double fromLon = fromLonA[i];
            fromLat = Math.toRadians(fromLat);
            final double lonDiff = Math.toRadians(LatLonPointImpl.lonNormal(fromLon - this.lon0Degrees));
            final double cosc = this.sinLat0 * Math.sin(fromLat) + this.cosLat0 * Math.cos(fromLat) * Math.cos(lonDiff);
            double toX;
            double toY;
            if (cosc >= 0.0) {
                toX = this.R * Math.cos(fromLat) * Math.sin(lonDiff);
                toY = this.R * (this.cosLat0 * Math.sin(fromLat) - this.sinLat0 * Math.cos(fromLat) * Math.cos(lonDiff));
            }
            else {
                toX = Double.POSITIVE_INFINITY;
                toY = Double.POSITIVE_INFINITY;
            }
            resultXA[i] = toX;
            resultYA[i] = toY;
        }
        return to;
    }
    
    @Override
    public double[][] projToLatLon(final double[][] from, final double[][] to) {
        final int cnt = from[0].length;
        final double[] fromXA = from[0];
        final double[] fromYA = from[1];
        final double[] toLatA = to[0];
        final double[] toLonA = to[1];
        for (int i = 0; i < cnt; ++i) {
            final double fromX = fromXA[i];
            final double fromY = fromYA[i];
            final double rho = Math.sqrt(fromX * fromX + fromY * fromY);
            final double c = Math.asin(rho / this.R);
            double toLon = this.lon0;
            double temp = 0.0;
            double toLat;
            if (Math.abs(rho) > 1.0E-6) {
                toLat = Math.asin(Math.cos(c) * this.sinLat0 + fromY * Math.sin(c) * this.cosLat0 / rho);
                if (Math.abs(this.lat0 - 0.7853981633974483) > 1.0E-6) {
                    temp = rho * this.cosLat0 * Math.cos(c) - fromY * this.sinLat0 * Math.sin(c);
                    toLon = this.lon0 + Math.atan(fromX * Math.sin(c) / temp);
                }
                else if (this.lat0 == 0.7853981633974483) {
                    toLon = this.lon0 + Math.atan(fromX / -fromY);
                    temp = -fromY;
                }
                else {
                    toLon = this.lon0 + Math.atan(fromX / fromY);
                    temp = fromY;
                }
            }
            else {
                toLat = this.lat0;
            }
            toLat = Math.toDegrees(toLat);
            toLon = Math.toDegrees(toLon);
            if (temp < 0.0) {
                toLon += 180.0;
            }
            toLon = LatLonPointImpl.lonNormal(toLon);
            toLatA[i] = toLat;
            toLonA[i] = toLon;
        }
        return to;
    }
    
    public static void main(final String[] args) {
        final Orthographic a = new Orthographic(40.0, -100.0);
        final ProjectionPointImpl p = a.latLonToProj(30.0, -110.0);
        System.out.println("proj point = " + p);
        final LatLonPoint ll = a.projToLatLon(p);
        System.out.println("ll = " + ll);
    }
}
