// 
// Decompiled by Procyon v0.5.36
// 

package ucar.unidata.geoloc.projection;

import ucar.unidata.geoloc.LatLonPointImpl;
import ucar.unidata.geoloc.ProjectionPointImpl;
import ucar.unidata.geoloc.LatLonPoint;
import ucar.unidata.geoloc.ProjectionPoint;
import ucar.unidata.geoloc.ProjectionImpl;

public class UtmProjection extends ProjectionImpl
{
    private Utm_To_Gdc_Converter convert2latlon;
    private Gdc_To_Utm_Converter convert2xy;
    private SaveParams save;
    
    @Override
    public ProjectionImpl constructCopy() {
        return (this.save == null) ? new UtmProjection(this.getZone(), this.isNorth()) : new UtmProjection(this.save.a, this.save.f, this.getZone(), this.isNorth());
    }
    
    public UtmProjection() {
        this(5, true);
    }
    
    public UtmProjection(final int zone, final boolean isNorth) {
        this.save = null;
        this.convert2latlon = new Utm_To_Gdc_Converter(zone, isNorth);
        this.convert2xy = new Gdc_To_Utm_Converter(zone, isNorth);
        this.addParameter("grid_mapping_name", "UTM");
        this.addParameter("semi-major_axis", this.convert2latlon.getA());
        this.addParameter("inverse_flattening", this.convert2latlon.getF());
        this.addParameter("UTM_zone", zone);
        this.addParameter("north_hemisphere", isNorth ? "true" : "false");
    }
    
    public UtmProjection(final double a, final double f, final int zone, final boolean isNorth) {
        this.save = null;
        this.save = new SaveParams(a, f);
        this.convert2latlon = new Utm_To_Gdc_Converter(a, f, zone, isNorth);
        this.convert2xy = new Gdc_To_Utm_Converter(a, f, zone, isNorth);
        this.addParameter("grid_mapping_name", "universal_transverse_mercator");
        this.addParameter("semi-major_axis", a);
        this.addParameter("inverse_flattening", f);
        this.addParameter("UTM_zone", zone);
        this.addParameter("north_hemisphere", isNorth ? "true" : "false");
    }
    
    public int getZone() {
        return this.convert2latlon.getZone();
    }
    
    public void setZone(final int newZone) {
        this.convert2latlon = new Utm_To_Gdc_Converter(this.convert2latlon.getA(), this.convert2latlon.getF(), newZone, this.convert2latlon.isNorth());
        this.convert2xy = new Gdc_To_Utm_Converter(this.convert2latlon.getA(), this.convert2latlon.getF(), this.convert2latlon.getZone(), this.convert2latlon.isNorth());
    }
    
    public boolean isNorth() {
        return this.convert2latlon.isNorth();
    }
    
    public void setNorth(final boolean newNorth) {
        this.convert2latlon = new Utm_To_Gdc_Converter(this.convert2latlon.getA(), this.convert2latlon.getF(), this.convert2latlon.getZone(), newNorth);
        this.convert2xy = new Gdc_To_Utm_Converter(this.convert2latlon.getA(), this.convert2latlon.getF(), this.convert2latlon.getZone(), this.convert2latlon.isNorth());
    }
    
    @Override
    public String getProjectionTypeLabel() {
        return "Universal transverse mercator";
    }
    
    public double getCentralMeridian() {
        return this.convert2xy.getCentralMeridian();
    }
    
    @Override
    public String paramsToString() {
        return this.getZone() + " " + this.isNorth();
    }
    
    @Override
    public boolean crossSeam(final ProjectionPoint pt1, final ProjectionPoint pt2) {
        return false;
    }
    
    @Override
    public boolean equals(final Object proj) {
        if (!(proj instanceof UtmProjection)) {
            return false;
        }
        final UtmProjection op = (UtmProjection)proj;
        return op.getZone() == this.getZone();
    }
    
    @Override
    public ProjectionPoint latLonToProj(final LatLonPoint latLon, final ProjectionPointImpl result) {
        final double fromLat = latLon.getLatitude();
        final double fromLon = latLon.getLongitude();
        return this.convert2xy.latLonToProj(fromLat, fromLon, result);
    }
    
    @Override
    public double[][] latLonToProj(final double[][] from, final double[][] to, final int latIndex, final int lonIndex) {
        if (from == null || from.length != 2) {
            throw new IllegalArgumentException("ProjectionImpl.latLonToProj:null array argument or wrong dimension (from)");
        }
        if (to == null || to.length != 2) {
            throw new IllegalArgumentException("ProjectionImpl.latLonToProj:null array argument or wrong dimension (to)");
        }
        if (from[0].length != to[0].length) {
            throw new IllegalArgumentException("ProjectionImpl.latLonToProj:from array not same length as to array");
        }
        return this.convert2xy.latLonToProj(from, to, latIndex, lonIndex);
    }
    
    @Override
    public float[][] latLonToProj(final float[][] from, final float[][] to, final int latIndex, final int lonIndex) {
        if (from == null || from.length != 2) {
            throw new IllegalArgumentException("ProjectionImpl.latLonToProj:null array argument or wrong dimension (from)");
        }
        if (to == null || to.length != 2) {
            throw new IllegalArgumentException("ProjectionImpl.latLonToProj:null array argument or wrong dimension (to)");
        }
        if (from[0].length != to[0].length) {
            throw new IllegalArgumentException("ProjectionImpl.latLonToProj:from array not same length as to array");
        }
        return this.convert2xy.latLonToProj(from, to, latIndex, lonIndex);
    }
    
    @Override
    public LatLonPoint projToLatLon(final ProjectionPoint world, final LatLonPointImpl result) {
        return this.convert2latlon.projToLatLon(world.getX(), world.getY(), result);
    }
    
    @Override
    public float[][] projToLatLon(final float[][] from, final float[][] to) {
        if (from == null || from.length != 2) {
            throw new IllegalArgumentException("ProjectionImpl.projToLatLon:null array argument or wrong dimension (from)");
        }
        if (to == null || to.length != 2) {
            throw new IllegalArgumentException("ProjectionImpl.projToLatLon:null array argument or wrong dimension (to)");
        }
        if (from[0].length != to[0].length) {
            throw new IllegalArgumentException("ProjectionImpl.projToLatLon:from array not same length as to array");
        }
        return this.convert2latlon.projToLatLon(from, to);
    }
    
    @Override
    public double[][] projToLatLon(final double[][] from, final double[][] to) {
        if (from == null || from.length != 2) {
            throw new IllegalArgumentException("ProjectionImpl.projToLatLon:null array argument or wrong dimension (from)");
        }
        if (to == null || to.length != 2) {
            throw new IllegalArgumentException("ProjectionImpl.projToLatLon:null array argument or wrong dimension (to)");
        }
        if (from[0].length != to[0].length) {
            throw new IllegalArgumentException("ProjectionImpl.projToLatLon:from array not same length as to array");
        }
        return this.convert2latlon.projToLatLon(from, to);
    }
    
    public static void main(final String[] arg) {
        final UtmProjection utm = new UtmProjection(17, true);
        final LatLonPointImpl ll = utm.projToLatLon(577.8000000000001, 2951.8);
        System.out.printf("%15.12f %15.12f%n", ll.getLatitude(), ll.getLongitude());
        assert closeEnough(ll.getLongitude(), -80.21802662821469, 1.0E-8);
        assert closeEnough(ll.getLatitude(), 26.685132668190793, 1.0E-8);
    }
    
    private static boolean closeEnough(final double v1, final double v2, final double tol) {
        final double diff = (v2 == 0.0) ? Math.abs(v1 - v2) : Math.abs(v1 / v2 - 1.0);
        return diff < tol;
    }
    
    private static class SaveParams
    {
        double a;
        double f;
        
        SaveParams(final double a, final double f) {
            this.a = a;
            this.f = f;
        }
    }
}
