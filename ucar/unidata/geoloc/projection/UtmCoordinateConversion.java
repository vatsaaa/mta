// 
// Decompiled by Procyon v0.5.36
// 

package ucar.unidata.geoloc.projection;

import java.util.Hashtable;
import java.util.Map;

public class UtmCoordinateConversion
{
    public double[] utm2LatLon(final String UTM) {
        final UTM2LatLon c = new UTM2LatLon();
        return c.convertUTMToLatLong(UTM);
    }
    
    public String latLon2UTM(final double latitude, final double longitude) {
        final LatLon2UTM c = new LatLon2UTM();
        return c.convertLatLonToUTM(latitude, longitude);
    }
    
    private void validate(final double latitude, final double longitude) {
        if (latitude < -90.0 || latitude > 90.0 || longitude < -180.0 || longitude >= 180.0) {
            throw new IllegalArgumentException("Legal ranges: latitude [-90,90], longitude [-180,180).");
        }
    }
    
    public String latLon2MGRUTM(final double latitude, final double longitude) {
        final LatLon2MGRUTM c = new LatLon2MGRUTM();
        return c.convertLatLonToMGRUTM(latitude, longitude);
    }
    
    public double[] mgrutm2LatLon(final String MGRUTM) {
        final MGRUTM2LatLon c = new MGRUTM2LatLon();
        return c.convertMGRUTMToLatLong(MGRUTM);
    }
    
    public double degreeToRadian(final double degree) {
        return degree * 3.141592653589793 / 180.0;
    }
    
    public double radianToDegree(final double radian) {
        return radian * 180.0 / 3.141592653589793;
    }
    
    private double POW(final double a, final double b) {
        return Math.pow(a, b);
    }
    
    private double SIN(final double value) {
        return Math.sin(value);
    }
    
    private double COS(final double value) {
        return Math.cos(value);
    }
    
    private double TAN(final double value) {
        return Math.tan(value);
    }
    
    public static void main(final String[] args) {
        final UtmCoordinateConversion utm = new UtmCoordinateConversion();
        assert utm.latLon2UTM(0.0, 0.0).equals("31 N 166021 0");
        assert utm.latLon2UTM(0.13, -0.2324).equals("30 N 808084 14385");
        assert utm.latLon2UTM(-45.6456, 23.3545).equals("34 G 683473 4942631");
        assert utm.latLon2UTM(-12.765, -33.8765).equals("25 L 404859 8588690");
        assert utm.latLon2UTM(-80.5434, -170.654).equals("02 C 506346 1057742");
        assert utm.latLon2UTM(90.0, 177.0).equals("60 Z 500000 9997964");
        assert utm.latLon2UTM(-90.0, -177.0).equals("01 A 500000 2035");
        assert utm.latLon2UTM(90.0, 3.0).equals("31 Z 500000 9997964");
        assert utm.latLon2UTM(23.4578, -135.4545).equals("08 Q 453580 2594272");
        assert utm.latLon2UTM(77.345, 156.9876).equals("57 X 450793 8586116");
        assert utm.latLon2UTM(-89.3454, -48.9306).equals("22 A 502639 75072");
        System.out.println(utm.latLon2UTM(0.0, 0.0));
        System.out.println(utm.latLon2UTM(0.13, -0.2324));
        System.out.println(utm.latLon2UTM(-45.6456, 23.3545));
        System.out.println(utm.latLon2UTM(-12.765, -33.8765));
        System.out.println(utm.latLon2UTM(-80.5434, -170.654));
        System.out.println(utm.latLon2UTM(90.0, 177.0));
        System.out.println(utm.latLon2UTM(-90.0, -177.0));
        System.out.println(utm.latLon2UTM(90.0, 3.0));
        System.out.println(utm.latLon2UTM(23.4578, -135.4545));
        System.out.println(utm.latLon2UTM(77.345, 156.9876));
        System.out.println(utm.latLon2UTM(-89.3454, -48.9306));
        double[] ll = utm.utm2LatLon("31 N 166021 0");
        System.out.println("latitude =" + ll[0] + " longitude =" + ll[1]);
        ll = utm.utm2LatLon("30 N 808084 14385");
        System.out.println("latitude =" + ll[0] + " longitude =" + ll[1]);
        ll = utm.utm2LatLon("34 G 683473 4942631");
        System.out.println("latitude =" + ll[0] + " longitude =" + ll[1]);
        ll = utm.utm2LatLon("25 L 404859 8588690");
        System.out.println("latitude =" + ll[0] + " longitude =" + ll[1]);
        ll = utm.utm2LatLon("02 C 506346 1057742");
        System.out.println("latitude =" + ll[0] + " longitude =" + ll[1]);
        ll = utm.utm2LatLon("60 Z 500000 9997964");
        System.out.println("latitude =" + ll[0] + " longitude =" + ll[1]);
        ll = utm.utm2LatLon("01 A 500000 2035");
        System.out.println("latitude =" + ll[0] + " longitude =" + ll[1]);
        ll = utm.utm2LatLon("31 Z 500000 9997964");
        System.out.println("latitude =" + ll[0] + " longitude =" + ll[1]);
        ll = utm.utm2LatLon("08 Q 453580 2594272");
        System.out.println("latitude =" + ll[0] + " longitude =" + ll[1]);
        ll = utm.utm2LatLon("57 X 450793 8586116");
        System.out.println("latitude =" + ll[0] + " longitude =" + ll[1]);
        ll = utm.utm2LatLon("22 A 502639 75072");
        System.out.println("latitude =" + ll[0] + " longitude =" + ll[1]);
    }
    
    private class LatLon2UTM
    {
        double equatorialRadius;
        double polarRadius;
        double flattening;
        double inverseFlattening;
        double rm;
        double k0;
        double e;
        double e1sq;
        double n;
        double rho;
        double nu;
        double S;
        double A0;
        double B0;
        double C0;
        double D0;
        double E0;
        double p;
        double sin1;
        double K1;
        double K2;
        double K3;
        double K4;
        double K5;
        double A6;
        
        private LatLon2UTM() {
            this.equatorialRadius = 6378137.0;
            this.polarRadius = 6356752.314;
            this.flattening = 0.00335281066474748;
            this.inverseFlattening = 298.257223563;
            this.rm = UtmCoordinateConversion.this.POW(this.equatorialRadius * this.polarRadius, 0.5);
            this.k0 = 0.9996;
            this.e = Math.sqrt(1.0 - UtmCoordinateConversion.this.POW(this.polarRadius / this.equatorialRadius, 2.0));
            this.e1sq = this.e * this.e / (1.0 - this.e * this.e);
            this.n = (this.equatorialRadius - this.polarRadius) / (this.equatorialRadius + this.polarRadius);
            this.rho = 6368573.744;
            this.nu = 6389236.914;
            this.S = 5103266.421;
            this.A0 = 6367449.146;
            this.B0 = 16038.42955;
            this.C0 = 16.83261333;
            this.D0 = 0.021984404;
            this.E0 = 3.12705E-4;
            this.p = -0.483084;
            this.sin1 = 4.84814E-6;
            this.K1 = 5101225.115;
            this.K2 = 3750.291596;
            this.K3 = 1.397608151;
            this.K4 = 214839.3105;
            this.K5 = -2.995382942;
            this.A6 = -1.00541E-7;
        }
        
        public String convertLatLonToUTM(final double latitude, final double longitude) {
            UtmCoordinateConversion.this.validate(latitude, longitude);
            String UTM = "";
            this.setVariables(latitude, longitude);
            final String longZone = this.getLongZone(longitude);
            final LatZones latZones = new LatZones();
            final String latZone = latZones.getLatZone(latitude);
            final double _easting = this.getEasting();
            final double _northing = this.getNorthing(latitude);
            UTM = longZone + " " + latZone + " " + (int)_easting + " " + (int)_northing;
            return UTM;
        }
        
        protected void setVariables(double latitude, final double longitude) {
            latitude = UtmCoordinateConversion.this.degreeToRadian(latitude);
            this.rho = this.equatorialRadius * (1.0 - this.e * this.e) / UtmCoordinateConversion.this.POW(1.0 - UtmCoordinateConversion.this.POW(this.e * UtmCoordinateConversion.this.SIN(latitude), 2.0), 1.5);
            this.nu = this.equatorialRadius / UtmCoordinateConversion.this.POW(1.0 - UtmCoordinateConversion.this.POW(this.e * UtmCoordinateConversion.this.SIN(latitude), 2.0), 0.5);
            double var1;
            if (longitude < 0.0) {
                var1 = (int)((180.0 + longitude) / 6.0) + 1;
            }
            else {
                var1 = (int)(longitude / 6.0) + 31;
            }
            final double var2 = 6.0 * var1 - 183.0;
            final double var3 = longitude - var2;
            this.p = var3 * 3600.0 / 10000.0;
            this.S = this.A0 * latitude - this.B0 * UtmCoordinateConversion.this.SIN(2.0 * latitude) + this.C0 * UtmCoordinateConversion.this.SIN(4.0 * latitude) - this.D0 * UtmCoordinateConversion.this.SIN(6.0 * latitude) + this.E0 * UtmCoordinateConversion.this.SIN(8.0 * latitude);
            this.K1 = this.S * this.k0;
            this.K2 = this.nu * UtmCoordinateConversion.this.SIN(latitude) * UtmCoordinateConversion.this.COS(latitude) * UtmCoordinateConversion.this.POW(this.sin1, 2.0) * this.k0 * 1.0E8 / 2.0;
            this.K3 = UtmCoordinateConversion.this.POW(this.sin1, 4.0) * this.nu * UtmCoordinateConversion.this.SIN(latitude) * Math.pow(UtmCoordinateConversion.this.COS(latitude), 3.0) / 24.0 * (5.0 - UtmCoordinateConversion.this.POW(UtmCoordinateConversion.this.TAN(latitude), 2.0) + 9.0 * this.e1sq * UtmCoordinateConversion.this.POW(UtmCoordinateConversion.this.COS(latitude), 2.0) + 4.0 * UtmCoordinateConversion.this.POW(this.e1sq, 2.0) * UtmCoordinateConversion.this.POW(UtmCoordinateConversion.this.COS(latitude), 4.0)) * this.k0 * 1.0E16;
            this.K4 = this.nu * UtmCoordinateConversion.this.COS(latitude) * this.sin1 * this.k0 * 10000.0;
            this.K5 = UtmCoordinateConversion.this.POW(this.sin1 * UtmCoordinateConversion.this.COS(latitude), 3.0) * (this.nu / 6.0) * (1.0 - UtmCoordinateConversion.this.POW(UtmCoordinateConversion.this.TAN(latitude), 2.0) + this.e1sq * UtmCoordinateConversion.this.POW(UtmCoordinateConversion.this.COS(latitude), 2.0)) * this.k0 * 1.0E12;
            this.A6 = UtmCoordinateConversion.this.POW(this.p * this.sin1, 6.0) * this.nu * UtmCoordinateConversion.this.SIN(latitude) * UtmCoordinateConversion.this.POW(UtmCoordinateConversion.this.COS(latitude), 5.0) / 720.0 * (61.0 - 58.0 * UtmCoordinateConversion.this.POW(UtmCoordinateConversion.this.TAN(latitude), 2.0) + UtmCoordinateConversion.this.POW(UtmCoordinateConversion.this.TAN(latitude), 4.0) + 270.0 * this.e1sq * UtmCoordinateConversion.this.POW(UtmCoordinateConversion.this.COS(latitude), 2.0) - 330.0 * this.e1sq * UtmCoordinateConversion.this.POW(UtmCoordinateConversion.this.SIN(latitude), 2.0)) * this.k0 * 1.0E24;
        }
        
        protected String getLongZone(final double longitude) {
            double longZone = 0.0;
            if (longitude < 0.0) {
                longZone = (180.0 + longitude) / 6.0 + 1.0;
            }
            else {
                longZone = longitude / 6.0 + 31.0;
            }
            String val = String.valueOf((int)longZone);
            if (val.length() == 1) {
                val = "0" + val;
            }
            return val;
        }
        
        protected double getNorthing(final double latitude) {
            double northing = this.K1 + this.K2 * this.p * this.p + this.K3 * UtmCoordinateConversion.this.POW(this.p, 4.0);
            if (latitude < 0.0) {
                northing += 1.0E7;
            }
            return northing;
        }
        
        protected double getEasting() {
            return 500000.0 + (this.K4 * this.p + this.K5 * UtmCoordinateConversion.this.POW(this.p, 3.0));
        }
    }
    
    private class LatLon2MGRUTM extends LatLon2UTM
    {
        public String convertLatLonToMGRUTM(final double latitude, final double longitude) {
            UtmCoordinateConversion.this.validate(latitude, longitude);
            String mgrUTM = "";
            this.setVariables(latitude, longitude);
            final String longZone = this.getLongZone(longitude);
            final LatZones latZones = new LatZones();
            final String latZone = latZones.getLatZone(latitude);
            final double _easting = this.getEasting();
            final double _northing = this.getNorthing(latitude);
            final Digraphs digraphs = new Digraphs();
            final String digraph1 = digraphs.getDigraph1(Integer.parseInt(longZone), _easting);
            final String digraph2 = digraphs.getDigraph2(Integer.parseInt(longZone), _northing);
            String easting = String.valueOf((int)_easting);
            if (easting.length() < 5) {
                easting = "00000" + easting;
            }
            easting = easting.substring(easting.length() - 5);
            String northing = String.valueOf((int)_northing);
            if (northing.length() < 5) {
                northing = "0000" + northing;
            }
            northing = northing.substring(northing.length() - 5);
            mgrUTM = longZone + latZone + digraph1 + digraph2 + easting + northing;
            return mgrUTM;
        }
    }
    
    private class MGRUTM2LatLon extends UTM2LatLon
    {
        public double[] convertMGRUTMToLatLong(final String mgrutm) {
            final double[] latlon = { 0.0, 0.0 };
            final int zone = Integer.parseInt(mgrutm.substring(0, 2));
            final String latZone = mgrutm.substring(2, 3);
            final String digraph1 = mgrutm.substring(3, 4);
            final String digraph2 = mgrutm.substring(4, 5);
            this.easting = Double.parseDouble(mgrutm.substring(5, 10));
            this.northing = Double.parseDouble(mgrutm.substring(10, 15));
            final LatZones lz = new LatZones();
            final double latZoneDegree = lz.getLatZoneDegree(latZone);
            final double a1 = latZoneDegree * 4.0E7 / 360.0;
            final double a2 = 2000000.0 * Math.floor(a1 / 2000000.0);
            final Digraphs digraphs = new Digraphs();
            final double digraph2Index = digraphs.getDigraph2Index(digraph2);
            double startindexEquator = 1.0;
            if (1 + zone % 2 == 1) {
                startindexEquator = 6.0;
            }
            double a3 = a2 + (digraph2Index - startindexEquator) * 100000.0;
            if (a3 <= 0.0) {
                a3 += 1.0E7;
            }
            this.northing += a3;
            this.zoneCM = -183 + 6 * zone;
            final double digraph1Index = digraphs.getDigraph1Index(digraph1);
            final int a4 = 1 + zone % 3;
            final double[] a5 = { 16.0, 0.0, 8.0 };
            final double a6 = 100000.0 * (digraph1Index - a5[a4 - 1]);
            this.easting += a6;
            this.setVariables();
            double latitude = 0.0;
            latitude = 180.0 * (this.phi1 - this.fact1 * (this.fact2 + this.fact3 + this.fact4)) / 3.141592653589793;
            if (latZoneDegree < 0.0) {
                latitude = 90.0 - latitude;
            }
            final double d = this._a2 * 180.0 / 3.141592653589793;
            final double longitude = this.zoneCM - d;
            if (this.getHemisphere(latZone).equals("S")) {
                latitude = -latitude;
            }
            latlon[0] = latitude;
            latlon[1] = longitude;
            return latlon;
        }
    }
    
    private class UTM2LatLon
    {
        double easting;
        double northing;
        int zone;
        String southernHemisphere;
        double arc;
        double mu;
        double ei;
        double ca;
        double cb;
        double cc;
        double cd;
        double n0;
        double r0;
        double _a1;
        double dd0;
        double t0;
        double Q0;
        double lof1;
        double lof2;
        double lof3;
        double _a2;
        double phi1;
        double fact1;
        double fact2;
        double fact3;
        double fact4;
        double zoneCM;
        double _a3;
        double b;
        double a;
        double e;
        double e1sq;
        double k0;
        
        private UTM2LatLon() {
            this.southernHemisphere = "ACDEFGHJKLM";
            this.b = 6356752.314;
            this.a = 6378137.0;
            this.e = 0.081819191;
            this.e1sq = 0.006739497;
            this.k0 = 0.9996;
        }
        
        protected String getHemisphere(final String latZone) {
            String hemisphere = "N";
            if (this.southernHemisphere.indexOf(latZone) > -1) {
                hemisphere = "S";
            }
            return hemisphere;
        }
        
        public double[] convertUTMToLatLong(final String UTM) {
            final double[] latlon = { 0.0, 0.0 };
            final String[] utm = UTM.split(" ");
            this.zone = Integer.parseInt(utm[0]);
            final String latZone = utm[1];
            this.easting = Double.parseDouble(utm[2]);
            this.northing = Double.parseDouble(utm[3]);
            final String hemisphere = this.getHemisphere(latZone);
            double latitude = 0.0;
            double longitude = 0.0;
            if (hemisphere.equals("S")) {
                this.northing = 1.0E7 - this.northing;
            }
            this.setVariables();
            latitude = 180.0 * (this.phi1 - this.fact1 * (this.fact2 + this.fact3 + this.fact4)) / 3.141592653589793;
            if (this.zone > 0) {
                this.zoneCM = 6 * this.zone - 183.0;
            }
            else {
                this.zoneCM = 3.0;
            }
            longitude = this.zoneCM - this._a3;
            if (hemisphere.equals("S")) {
                latitude = -latitude;
            }
            latlon[0] = latitude;
            latlon[1] = longitude;
            return latlon;
        }
        
        protected void setVariables() {
            this.arc = this.northing / this.k0;
            this.mu = this.arc / (this.a * (1.0 - UtmCoordinateConversion.this.POW(this.e, 2.0) / 4.0 - 3.0 * UtmCoordinateConversion.this.POW(this.e, 4.0) / 64.0 - 5.0 * UtmCoordinateConversion.this.POW(this.e, 6.0) / 256.0));
            this.ei = (1.0 - UtmCoordinateConversion.this.POW(1.0 - this.e * this.e, 0.5)) / (1.0 + UtmCoordinateConversion.this.POW(1.0 - this.e * this.e, 0.5));
            this.ca = 3.0 * this.ei / 2.0 - 27.0 * UtmCoordinateConversion.this.POW(this.ei, 3.0) / 32.0;
            this.cb = 21.0 * UtmCoordinateConversion.this.POW(this.ei, 2.0) / 16.0 - 55.0 * UtmCoordinateConversion.this.POW(this.ei, 4.0) / 32.0;
            this.cc = 151.0 * UtmCoordinateConversion.this.POW(this.ei, 3.0) / 96.0;
            this.cd = 1097.0 * UtmCoordinateConversion.this.POW(this.ei, 4.0) / 512.0;
            this.phi1 = this.mu + this.ca * UtmCoordinateConversion.this.SIN(2.0 * this.mu) + this.cb * UtmCoordinateConversion.this.SIN(4.0 * this.mu) + this.cc * UtmCoordinateConversion.this.SIN(6.0 * this.mu) + this.cd * UtmCoordinateConversion.this.SIN(8.0 * this.mu);
            this.n0 = this.a / UtmCoordinateConversion.this.POW(1.0 - UtmCoordinateConversion.this.POW(this.e * UtmCoordinateConversion.this.SIN(this.phi1), 2.0), 0.5);
            this.r0 = this.a * (1.0 - this.e * this.e) / UtmCoordinateConversion.this.POW(1.0 - UtmCoordinateConversion.this.POW(this.e * UtmCoordinateConversion.this.SIN(this.phi1), 2.0), 1.5);
            this.fact1 = this.n0 * UtmCoordinateConversion.this.TAN(this.phi1) / this.r0;
            this._a1 = 500000.0 - this.easting;
            this.dd0 = this._a1 / (this.n0 * this.k0);
            this.fact2 = this.dd0 * this.dd0 / 2.0;
            this.t0 = UtmCoordinateConversion.this.POW(UtmCoordinateConversion.this.TAN(this.phi1), 2.0);
            this.Q0 = this.e1sq * UtmCoordinateConversion.this.POW(UtmCoordinateConversion.this.COS(this.phi1), 2.0);
            this.fact3 = (5.0 + 3.0 * this.t0 + 10.0 * this.Q0 - 4.0 * this.Q0 * this.Q0 - 9.0 * this.e1sq) * UtmCoordinateConversion.this.POW(this.dd0, 4.0) / 24.0;
            this.fact4 = (61.0 + 90.0 * this.t0 + 298.0 * this.Q0 + 45.0 * this.t0 * this.t0 - 252.0 * this.e1sq - 3.0 * this.Q0 * this.Q0) * UtmCoordinateConversion.this.POW(this.dd0, 6.0) / 720.0;
            this.lof1 = this._a1 / (this.n0 * this.k0);
            this.lof2 = (1.0 + 2.0 * this.t0 + this.Q0) * UtmCoordinateConversion.this.POW(this.dd0, 3.0) / 6.0;
            this.lof3 = (5.0 - 2.0 * this.Q0 + 28.0 * this.t0 - 3.0 * UtmCoordinateConversion.this.POW(this.Q0, 2.0) + 8.0 * this.e1sq + 24.0 * UtmCoordinateConversion.this.POW(this.t0, 2.0)) * UtmCoordinateConversion.this.POW(this.dd0, 5.0) / 120.0;
            this._a2 = (this.lof1 - this.lof2 + this.lof3) / UtmCoordinateConversion.this.COS(this.phi1);
            this._a3 = this._a2 * 180.0 / 3.141592653589793;
        }
    }
    
    private class Digraphs
    {
        private Map digraph1;
        private Map digraph2;
        private String[] digraph1Array;
        private String[] digraph2Array;
        
        public Digraphs() {
            this.digraph1 = new Hashtable();
            this.digraph2 = new Hashtable();
            this.digraph1Array = new String[] { "A", "B", "C", "D", "E", "F", "G", "H", "J", "K", "L", "M", "N", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" };
            this.digraph2Array = new String[] { "V", "A", "B", "C", "D", "E", "F", "G", "H", "J", "K", "L", "M", "N", "P", "Q", "R", "S", "T", "U", "V" };
            this.digraph1.put(new Integer(1), "A");
            this.digraph1.put(new Integer(2), "B");
            this.digraph1.put(new Integer(3), "C");
            this.digraph1.put(new Integer(4), "D");
            this.digraph1.put(new Integer(5), "E");
            this.digraph1.put(new Integer(6), "F");
            this.digraph1.put(new Integer(7), "G");
            this.digraph1.put(new Integer(8), "H");
            this.digraph1.put(new Integer(9), "J");
            this.digraph1.put(new Integer(10), "K");
            this.digraph1.put(new Integer(11), "L");
            this.digraph1.put(new Integer(12), "M");
            this.digraph1.put(new Integer(13), "N");
            this.digraph1.put(new Integer(14), "P");
            this.digraph1.put(new Integer(15), "Q");
            this.digraph1.put(new Integer(16), "R");
            this.digraph1.put(new Integer(17), "S");
            this.digraph1.put(new Integer(18), "T");
            this.digraph1.put(new Integer(19), "U");
            this.digraph1.put(new Integer(20), "V");
            this.digraph1.put(new Integer(21), "W");
            this.digraph1.put(new Integer(22), "X");
            this.digraph1.put(new Integer(23), "Y");
            this.digraph1.put(new Integer(24), "Z");
            this.digraph2.put(new Integer(0), "V");
            this.digraph2.put(new Integer(1), "A");
            this.digraph2.put(new Integer(2), "B");
            this.digraph2.put(new Integer(3), "C");
            this.digraph2.put(new Integer(4), "D");
            this.digraph2.put(new Integer(5), "E");
            this.digraph2.put(new Integer(6), "F");
            this.digraph2.put(new Integer(7), "G");
            this.digraph2.put(new Integer(8), "H");
            this.digraph2.put(new Integer(9), "J");
            this.digraph2.put(new Integer(10), "K");
            this.digraph2.put(new Integer(11), "L");
            this.digraph2.put(new Integer(12), "M");
            this.digraph2.put(new Integer(13), "N");
            this.digraph2.put(new Integer(14), "P");
            this.digraph2.put(new Integer(15), "Q");
            this.digraph2.put(new Integer(16), "R");
            this.digraph2.put(new Integer(17), "S");
            this.digraph2.put(new Integer(18), "T");
            this.digraph2.put(new Integer(19), "U");
            this.digraph2.put(new Integer(20), "V");
        }
        
        public int getDigraph1Index(final String letter) {
            for (int i = 0; i < this.digraph1Array.length; ++i) {
                if (this.digraph1Array[i].equals(letter)) {
                    return i + 1;
                }
            }
            return -1;
        }
        
        public int getDigraph2Index(final String letter) {
            for (int i = 0; i < this.digraph2Array.length; ++i) {
                if (this.digraph2Array[i].equals(letter)) {
                    return i;
                }
            }
            return -1;
        }
        
        public String getDigraph1(final int longZone, final double easting) {
            final int a1 = longZone;
            final double a2 = 8 * ((a1 - 1) % 3) + 1;
            final double a3 = easting;
            final double a4 = a2 + (int)(a3 / 100000.0) - 1.0;
            return this.digraph1.get(new Integer((int)Math.floor(a4)));
        }
        
        public String getDigraph2(final int longZone, final double northing) {
            final int a1 = longZone;
            final double a2 = 1 + 5 * ((a1 - 1) % 2);
            final double a3 = northing;
            double a4 = a2 + (int)(a3 / 100000.0);
            a4 = (a2 + (int)(a3 / 100000.0)) % 20.0;
            a4 = Math.floor(a4);
            if (a4 < 0.0) {
                a4 += 19.0;
            }
            return this.digraph2.get(new Integer((int)Math.floor(a4)));
        }
    }
    
    private class LatZones
    {
        private char[] letters;
        private int[] degrees;
        private char[] negLetters;
        private int[] negDegrees;
        private char[] posLetters;
        private int[] posDegrees;
        private int arrayLength;
        
        public LatZones() {
            this.letters = new char[] { 'A', 'C', 'D', 'E', 'F', 'G', 'H', 'J', 'K', 'L', 'M', 'N', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Z' };
            this.degrees = new int[] { -90, -84, -72, -64, -56, -48, -40, -32, -24, -16, -8, 0, 8, 16, 24, 32, 40, 48, 56, 64, 72, 84 };
            this.negLetters = new char[] { 'A', 'C', 'D', 'E', 'F', 'G', 'H', 'J', 'K', 'L', 'M' };
            this.negDegrees = new int[] { -90, -84, -72, -64, -56, -48, -40, -32, -24, -16, -8 };
            this.posLetters = new char[] { 'N', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Z' };
            this.posDegrees = new int[] { 0, 8, 16, 24, 32, 40, 48, 56, 64, 72, 84 };
            this.arrayLength = 22;
        }
        
        public int getLatZoneDegree(final String letter) {
            final char ltr = letter.charAt(0);
            for (int i = 0; i < this.arrayLength; ++i) {
                if (this.letters[i] == ltr) {
                    return this.degrees[i];
                }
            }
            return -100;
        }
        
        public String getLatZone(final double latitude) {
            int latIndex = -2;
            final int lat = (int)latitude;
            if (lat >= 0) {
                for (int len = this.posLetters.length, i = 0; i < len; ++i) {
                    if (lat == this.posDegrees[i]) {
                        latIndex = i;
                        break;
                    }
                    if (lat <= this.posDegrees[i]) {
                        latIndex = i - 1;
                        break;
                    }
                }
            }
            else {
                for (int len = this.negLetters.length, i = 0; i < len; ++i) {
                    if (lat == this.negDegrees[i]) {
                        latIndex = i;
                        break;
                    }
                    if (lat < this.negDegrees[i]) {
                        latIndex = i - 1;
                        break;
                    }
                }
            }
            if (latIndex == -1) {
                latIndex = 0;
            }
            if (lat >= 0) {
                if (latIndex == -2) {
                    latIndex = this.posLetters.length - 1;
                }
                return String.valueOf(this.posLetters[latIndex]);
            }
            if (latIndex == -2) {
                latIndex = this.negLetters.length - 1;
            }
            return String.valueOf(this.negLetters[latIndex]);
        }
    }
}
