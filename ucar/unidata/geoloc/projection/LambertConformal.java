// 
// Decompiled by Procyon v0.5.36
// 

package ucar.unidata.geoloc.projection;

import ucar.unidata.geoloc.LatLonPoint;
import ucar.unidata.geoloc.ProjectionPointImpl;
import ucar.unidata.geoloc.ProjectionPoint;
import ucar.unidata.util.Format;
import ucar.unidata.util.Parameter;
import ucar.unidata.geoloc.Earth;
import ucar.unidata.geoloc.LatLonPointImpl;
import ucar.unidata.geoloc.ProjectionImpl;

public class LambertConformal extends ProjectionImpl
{
    private double n;
    private double F;
    private double rho;
    private double earthRadiusTimesF;
    private double lon0Degrees;
    private double lat0;
    private double lon0;
    private double par1;
    private double par2;
    private double falseEasting;
    private double falseNorthing;
    private double earth_radius;
    private LatLonPointImpl origin;
    
    @Override
    public ProjectionImpl constructCopy() {
        return new LambertConformal(this.getOriginLat(), this.getOriginLon(), this.getParallelOne(), this.getParallelTwo(), this.getFalseEasting(), this.getFalseNorthing());
    }
    
    public LambertConformal() {
        this(40.0, -105.0, 20.0, 60.0, 0.0, 0.0, Earth.getRadius() * 0.001);
    }
    
    public LambertConformal(final double lat0, final double lon0, final double par1, final double par2) {
        this(lat0, lon0, par1, par2, 0.0, 0.0, Earth.getRadius() * 0.001);
    }
    
    public LambertConformal(final double lat0, final double lon0, final double par1, final double par2, final double false_easting, final double false_northing) {
        this(lat0, lon0, par1, par2, false_easting, false_northing, Earth.getRadius() * 0.001);
    }
    
    public LambertConformal(final double lat0, final double lon0, final double par1, final double par2, final double false_easting, final double false_northing, final double earth_radius) {
        this.lat0 = Math.toRadians(lat0);
        this.lon0 = Math.toRadians(lon0);
        this.par1 = par1;
        this.par2 = par2;
        this.falseEasting = false_easting;
        this.falseNorthing = false_northing;
        this.earth_radius = earth_radius;
        this.origin = new LatLonPointImpl(lat0, lon0);
        this.precalculate();
        this.addParameter("grid_mapping_name", "lambert_conformal_conic");
        this.addParameter("latitude_of_projection_origin", lat0);
        this.addParameter("longitude_of_central_meridian", lon0);
        if (par2 == par1) {
            this.addParameter("standard_parallel", par1);
        }
        else {
            final double[] data = { par1, par2 };
            this.addParameter(new Parameter("standard_parallel", data));
        }
        if (false_easting != 0.0 || false_northing != 0.0) {
            this.addParameter("false_easting", false_easting);
            this.addParameter("false_northing", false_northing);
            this.addParameter("units", "km");
        }
    }
    
    private void precalculate() {
        if (Math.abs(this.lat0 - 1.5707963267948966) < 1.0E-6) {
            throw new IllegalArgumentException("LambertConformal lat0 = 90");
        }
        if (Math.abs(this.lat0 + 1.5707963267948966) < 1.0E-6) {
            throw new IllegalArgumentException("LambertConformal lat0 = -90");
        }
        if (Math.abs(this.par1 - 90.0) < 1.0E-6) {
            throw new IllegalArgumentException("LambertConformal par1 = 90");
        }
        if (Math.abs(this.par1 + 90.0) < 1.0E-6) {
            throw new IllegalArgumentException("LambertConformal par1 = -90");
        }
        if (Math.abs(this.par2 - 90.0) < 1.0E-6) {
            throw new IllegalArgumentException("LambertConformal par2 = 90");
        }
        if (Math.abs(this.par2 + 90.0) < 1.0E-6) {
            throw new IllegalArgumentException("LambertConformal par2 = -90");
        }
        final double par1r = Math.toRadians(this.par1);
        final double par2r = Math.toRadians(this.par2);
        final double t1 = Math.tan(0.7853981633974483 + par1r / 2.0);
        final double t2 = Math.tan(0.7853981633974483 + par2r / 2.0);
        if (Math.abs(this.par2 - this.par1) < 1.0E-6) {
            this.n = Math.sin(par1r);
        }
        else {
            this.n = Math.log(Math.cos(par1r) / Math.cos(par2r)) / Math.log(t2 / t1);
        }
        final double t1n = Math.pow(t1, this.n);
        this.F = Math.cos(par1r) * t1n / this.n;
        this.earthRadiusTimesF = this.earth_radius * this.F;
        final double t0n = Math.pow(Math.tan(0.7853981633974483 + this.lat0 / 2.0), this.n);
        this.rho = this.earthRadiusTimesF / t0n;
        this.lon0Degrees = Math.toDegrees(this.lon0);
    }
    
    @Override
    public Object clone() {
        final LambertConformal cl = (LambertConformal)super.clone();
        cl.origin = new LatLonPointImpl(this.getOriginLat(), this.getOriginLon());
        return cl;
    }
    
    @Override
    public boolean equals(final Object proj) {
        if (!(proj instanceof LambertConformal)) {
            return false;
        }
        final LambertConformal oo = (LambertConformal)proj;
        return this.getParallelOne() == oo.getParallelOne() && this.getParallelTwo() == oo.getParallelTwo() && this.getOriginLat() == oo.getOriginLat() && this.getOriginLon() == oo.getOriginLon() && this.defaultMapArea.equals(oo.defaultMapArea);
    }
    
    public double getParallelTwo() {
        return this.par2;
    }
    
    public void setParallelTwo(final double par) {
        this.par2 = par;
        this.precalculate();
    }
    
    @Deprecated
    public void setParellelTwo(final double par) {
        this.par2 = par;
        this.precalculate();
    }
    
    public double getParallelOne() {
        return this.par1;
    }
    
    public void setParallelOne(final double par) {
        this.par1 = par;
        this.precalculate();
    }
    
    @Deprecated
    public void setParellelOne(final double par) {
        this.par1 = par;
        this.precalculate();
    }
    
    public double getOriginLon() {
        return this.origin.getLongitude();
    }
    
    public void setOriginLon(final double lon) {
        this.origin.setLongitude(lon);
        this.lon0 = Math.toRadians(lon);
        this.precalculate();
    }
    
    public double getOriginLat() {
        return this.origin.getLatitude();
    }
    
    public void setOriginLat(final double lat) {
        this.origin.setLatitude(lat);
        this.lat0 = Math.toRadians(lat);
        this.precalculate();
    }
    
    public double getFalseEasting() {
        return this.falseEasting;
    }
    
    public void setFalseEasting(final double falseEasting) {
        this.falseEasting = falseEasting;
    }
    
    public double getFalseNorthing() {
        return this.falseNorthing;
    }
    
    public void setFalseNorthing(final double falseNorthing) {
        this.falseNorthing = falseNorthing;
    }
    
    @Override
    public String getProjectionTypeLabel() {
        return "Lambert conformal conic";
    }
    
    @Override
    public String paramsToString() {
        return " origin " + this.origin.toString() + " parallels: " + Format.d(this.par1, 3) + " " + Format.d(this.par2, 3);
    }
    
    public String toWKS() {
        final StringBuilder sbuff = new StringBuilder();
        sbuff.append("PROJCS[\"").append(this.getName()).append("\",");
        sbuff.append("GEOGCS[\"Normal Sphere (r=6371007)\",");
        sbuff.append("DATUM[\"unknown\",");
        sbuff.append("SPHEROID[\"sphere\",6371007,0]],");
        sbuff.append("PRIMEM[\"Greenwich\",0],");
        sbuff.append("UNIT[\"degree\",0.0174532925199433]],");
        sbuff.append("PROJECTION[\"Lambert_Conformal_Conic_1SP\"],");
        sbuff.append("PARAMETER[\"latitude_of_origin\",").append(this.getOriginLat()).append("],");
        sbuff.append("PARAMETER[\"central_meridian\",").append(this.getOriginLon()).append("],");
        sbuff.append("PARAMETER[\"scale_factor\",1],");
        sbuff.append("PARAMETER[\"false_easting\",").append(this.falseEasting).append("],");
        sbuff.append("PARAMETER[\"false_northing\",").append(this.falseNorthing).append("],");
        return sbuff.toString();
    }
    
    public double getScale(double lat) {
        lat = Math.toRadians(lat);
        final double t = Math.tan(0.7853981633974483 + lat / 2.0);
        final double tn = Math.pow(t, this.n);
        final double r1 = this.n * this.F;
        final double r2 = Math.cos(lat) * tn;
        return r1 / r2;
    }
    
    @Override
    public boolean crossSeam(final ProjectionPoint pt1, final ProjectionPoint pt2) {
        return ProjectionPointImpl.isInfinite(pt1) || ProjectionPointImpl.isInfinite(pt2) || (pt1.getX() * pt2.getX() < 0.0 && Math.abs(pt1.getX() - pt2.getX()) > 20000.0);
    }
    
    @Override
    public ProjectionPoint latLonToProj(final LatLonPoint latLon, final ProjectionPointImpl result) {
        double fromLat = latLon.getLatitude();
        final double fromLon = latLon.getLongitude();
        fromLat = Math.toRadians(fromLat);
        final double dlon = LatLonPointImpl.lonNormal(fromLon - this.lon0Degrees);
        final double theta = this.n * Math.toRadians(dlon);
        final double tn = Math.pow(Math.tan(0.7853981633974483 + fromLat / 2.0), this.n);
        final double r = this.earthRadiusTimesF / tn;
        final double toX = r * Math.sin(theta);
        final double toY = this.rho - r * Math.cos(theta);
        result.setLocation(toX + this.falseEasting, toY + this.falseNorthing);
        return result;
    }
    
    @Override
    public LatLonPoint projToLatLon(final ProjectionPoint world, final LatLonPointImpl result) {
        double fromX = world.getX() - this.falseEasting;
        double fromY = world.getY() - this.falseNorthing;
        double rhop = this.rho;
        if (this.n < 0.0) {
            rhop *= -1.0;
            fromX *= -1.0;
            fromY *= -1.0;
        }
        final double yd = rhop - fromY;
        final double theta = Math.atan2(fromX, yd);
        double r = Math.sqrt(fromX * fromX + yd * yd);
        if (this.n < 0.0) {
            r *= -1.0;
        }
        final double toLon = Math.toDegrees(theta / this.n + this.lon0);
        double toLat;
        if (Math.abs(r) < 1.0E-6) {
            toLat = ((this.n < 0.0) ? -90.0 : 90.0);
        }
        else {
            final double rn = Math.pow(this.earth_radius * this.F / r, 1.0 / this.n);
            toLat = Math.toDegrees(2.0 * Math.atan(rn) - 1.5707963267948966);
        }
        result.setLatitude(toLat);
        result.setLongitude(toLon);
        return result;
    }
    
    @Override
    public float[][] latLonToProj(final float[][] from, final float[][] to, final int latIndex, final int lonIndex) {
        final int cnt = from[0].length;
        final float[] fromLatA = from[latIndex];
        final float[] fromLonA = from[lonIndex];
        final float[] resultXA = to[0];
        final float[] resultYA = to[1];
        for (int i = 0; i < cnt; ++i) {
            double fromLat = fromLatA[i];
            final double fromLon = fromLonA[i];
            fromLat = Math.toRadians(fromLat);
            final double dlon = LatLonPointImpl.lonNormal(fromLon - this.lon0Degrees);
            final double theta = this.n * Math.toRadians(dlon);
            final double tn = Math.pow(Math.tan(0.7853981633974483 + fromLat / 2.0), this.n);
            final double r = this.earthRadiusTimesF / tn;
            final double toX = r * Math.sin(theta);
            final double toY = this.rho - r * Math.cos(theta);
            resultXA[i] = (float)(toX + this.falseEasting);
            resultYA[i] = (float)(toY + this.falseNorthing);
        }
        return to;
    }
    
    @Override
    public float[][] projToLatLon(final float[][] from, final float[][] to) {
        final int cnt = from[0].length;
        final float[] fromXA = from[0];
        final float[] fromYA = from[1];
        final float[] toLatA = to[0];
        final float[] toLonA = to[1];
        for (int i = 0; i < cnt; ++i) {
            double fromX = fromXA[i] - this.falseEasting;
            double fromY = fromYA[i] - this.falseNorthing;
            double rhop = this.rho;
            if (this.n < 0.0) {
                rhop *= -1.0;
                fromX *= -1.0;
                fromY *= -1.0;
            }
            final double yd = rhop - fromY;
            final double theta = Math.atan2(fromX, yd);
            double r = Math.sqrt(fromX * fromX + yd * yd);
            if (this.n < 0.0) {
                r *= -1.0;
            }
            final double toLon = Math.toDegrees(theta / this.n + this.lon0);
            double toLat;
            if (Math.abs(r) < 1.0E-6) {
                toLat = ((this.n < 0.0) ? -90.0 : 90.0);
            }
            else {
                final double rn = Math.pow(this.earth_radius * this.F / r, 1.0 / this.n);
                toLat = Math.toDegrees(2.0 * Math.atan(rn) - 1.5707963267948966);
            }
            toLatA[i] = (float)toLat;
            toLonA[i] = (float)toLon;
        }
        return to;
    }
    
    @Override
    public double[][] latLonToProj(final double[][] from, final double[][] to, final int latIndex, final int lonIndex) {
        final int cnt = from[0].length;
        final double[] fromLatA = from[latIndex];
        final double[] fromLonA = from[lonIndex];
        final double[] resultXA = to[0];
        final double[] resultYA = to[1];
        for (int i = 0; i < cnt; ++i) {
            double fromLat = fromLatA[i];
            final double fromLon = fromLonA[i];
            fromLat = Math.toRadians(fromLat);
            final double dlon = LatLonPointImpl.lonNormal(fromLon - this.lon0Degrees);
            final double theta = this.n * Math.toRadians(dlon);
            final double tn = Math.pow(Math.tan(0.7853981633974483 + fromLat / 2.0), this.n);
            final double r = this.earthRadiusTimesF / tn;
            final double toX = r * Math.sin(theta);
            final double toY = this.rho - r * Math.cos(theta);
            resultXA[i] = toX + this.falseEasting;
            resultYA[i] = toY + this.falseNorthing;
        }
        return to;
    }
    
    @Override
    public double[][] projToLatLon(final double[][] from, final double[][] to) {
        final int cnt = from[0].length;
        final double[] fromXA = from[0];
        final double[] fromYA = from[1];
        final double[] toLatA = to[0];
        final double[] toLonA = to[1];
        for (int i = 0; i < cnt; ++i) {
            double fromX = fromXA[i] - this.falseEasting;
            double fromY = fromYA[i] - this.falseNorthing;
            double rhop = this.rho;
            if (this.n < 0.0) {
                rhop *= -1.0;
                fromX *= -1.0;
                fromY *= -1.0;
            }
            final double yd = rhop - fromY;
            final double theta = Math.atan2(fromX, yd);
            double r = Math.sqrt(fromX * fromX + yd * yd);
            if (this.n < 0.0) {
                r *= -1.0;
            }
            final double toLon = Math.toDegrees(theta / this.n + this.lon0);
            double toLat;
            if (Math.abs(r) < 1.0E-6) {
                toLat = ((this.n < 0.0) ? -90.0 : 90.0);
            }
            else {
                final double rn = Math.pow(this.earth_radius * this.F / r, 1.0 / this.n);
                toLat = Math.toDegrees(2.0 * Math.atan(rn) - 1.5707963267948966);
            }
            toLatA[i] = toLat;
            toLonA[i] = toLon;
        }
        return to;
    }
}
