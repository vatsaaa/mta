// 
// Decompiled by Procyon v0.5.36
// 

package ucar.unidata.geoloc.projection;

import ucar.unidata.geoloc.ProjectionPointImpl;
import ucar.unidata.geoloc.LatLonPoint;
import ucar.unidata.geoloc.ProjectionPoint;
import ucar.unidata.util.Format;
import ucar.unidata.geoloc.LatLonPointImpl;
import ucar.unidata.geoloc.ProjectionImpl;

public class Stereographic extends ProjectionImpl
{
    private double latts;
    private double latt;
    private double lont;
    private double scale;
    private double sinlatt;
    private double coslatt;
    private double central_meridian;
    private boolean isNorth;
    private boolean isPolar;
    private double falseEasting;
    private double falseNorthing;
    private LatLonPointImpl origin;
    
    @Override
    public ProjectionImpl constructCopy() {
        return new Stereographic(this.getTangentLat(), this.getTangentLon(), this.getScale(), this.getFalseEasting(), this.getFalseNorthing());
    }
    
    public Stereographic() {
        this(90.0, -105.0, 1.0);
    }
    
    public Stereographic(final double latt, final double lont, final double scale) {
        this(latt, lont, scale, 0.0, 0.0);
    }
    
    public Stereographic(final double latt, final double lont, final double scale, final double false_easting, final double false_northing) {
        this.isNorth = false;
        this.isPolar = false;
        this.latt = Math.toRadians(latt);
        this.lont = Math.toRadians(lont);
        this.scale = scale * Stereographic.EARTH_RADIUS;
        this.falseEasting = false_easting;
        this.falseNorthing = false_northing;
        this.precalculate();
        this.origin = new LatLonPointImpl(latt, lont);
        this.addParameter("grid_mapping_name", "stereographic");
        this.addParameter("longitude_of_projection_origin", lont);
        this.addParameter("latitude_of_projection_origin", latt);
        this.addParameter("scale_factor_at_projection_origin", scale);
        if (false_easting != 0.0 || false_northing != 0.0) {
            this.addParameter("false_easting", false_easting);
            this.addParameter("false_northing", false_northing);
            this.addParameter("units", "km");
        }
    }
    
    public Stereographic(final double lat_ts_deg, final double latt_deg, final double lont_deg, final boolean north) {
        this.isNorth = false;
        this.isPolar = false;
        this.latts = Math.toRadians(lat_ts_deg);
        this.latt = Math.toRadians(latt_deg);
        this.lont = Math.toRadians(lont_deg);
        this.isPolar = true;
        this.isNorth = north;
        this.precalculate();
        this.origin = new LatLonPointImpl(latt_deg, lont_deg);
        final double scaleFactor = (lat_ts_deg == 90.0 || lat_ts_deg == -90.0) ? 1.0 : this.getScaleFactor(this.latts, north);
        this.scale = scaleFactor * Stereographic.EARTH_RADIUS;
        this.addParameter("grid_mapping_name", "polar_stereographic");
        this.addParameter("longitude_of_projection_origin", lont_deg);
        this.addParameter("latitude_of_projection_origin", latt_deg);
        this.addParameter("scale_factor_at_projection_origin", scaleFactor);
    }
    
    private double getScaleFactor(final double lat_ts, final boolean north) {
        final double e = 0.081819191;
        double tf = 1.0;
        double mf = 1.0;
        double k0 = 1.0;
        final double root = (1.0 + e * Math.sin(lat_ts)) / (1.0 - e * Math.sin(lat_ts));
        final double power = e / 2.0;
        if (north) {
            tf = Math.tan(0.7853981633974483 - lat_ts / 2.0) * Math.pow(root, power);
        }
        else {
            tf = Math.tan(0.7853981633974483 + lat_ts / 2.0) / Math.pow(root, power);
        }
        mf = Math.cos(lat_ts) / Math.sqrt(1.0 - e * e * Math.pow(Math.sin(lat_ts), 2.0));
        k0 = mf * Math.sqrt(Math.pow(1.0 + e, 1.0 + e) * Math.pow(1.0 - e, 1.0 - e)) / (2.0 * tf);
        return Double.isNaN(k0) ? 1.0 : k0;
    }
    
    public static Stereographic factory(final double latt, final double lont, final double latTrue) {
        final double scale = (1.0 + Math.sin(Math.toRadians(latTrue))) / 2.0;
        return new Stereographic(latt, lont, scale);
    }
    
    private void precalculate() {
        this.sinlatt = Math.sin(this.latt);
        this.coslatt = Math.cos(this.latt);
    }
    
    public double getScale() {
        return this.scale / Stereographic.EARTH_RADIUS;
    }
    
    public void setScale(final double scale) {
        this.scale = Stereographic.EARTH_RADIUS * scale;
    }
    
    public double getNaturalOriginLat() {
        return Math.toDegrees(this.latts);
    }
    
    public double getTangentLon() {
        return this.origin.getLongitude();
    }
    
    public void setTangentLon(final double lon) {
        this.origin.setLongitude(lon);
        this.lont = Math.toRadians(lon);
    }
    
    public double getTangentLat() {
        return this.origin.getLatitude();
    }
    
    public void setTangentLat(final double lat) {
        this.origin.setLatitude(lat);
        this.latt = Math.toRadians(lat);
        this.precalculate();
    }
    
    public double getCentralMeridian() {
        return this.central_meridian;
    }
    
    public void setCentralMeridian(final double cm) {
        this.central_meridian = cm;
    }
    
    public boolean isNorth() {
        return this.isNorth;
    }
    
    public boolean isPolar() {
        return this.isPolar;
    }
    
    @Override
    public String paramsToString() {
        return " tangent " + this.origin.toString() + " scale: " + Format.d(this.getScale(), 6);
    }
    
    @Override
    public boolean crossSeam(final ProjectionPoint pt1, final ProjectionPoint pt2) {
        return false;
    }
    
    @Override
    public Object clone() {
        final Stereographic cl = (Stereographic)super.clone();
        cl.origin = new LatLonPointImpl(this.getTangentLat(), this.getTangentLon());
        return cl;
    }
    
    @Override
    public boolean equals(final Object proj) {
        if (!(proj instanceof Stereographic)) {
            return false;
        }
        final Stereographic oo = (Stereographic)proj;
        return this.getScale() == oo.getScale() && this.getTangentLat() == oo.getTangentLat() && this.getTangentLon() == oo.getTangentLon() && this.defaultMapArea.equals(oo.defaultMapArea);
    }
    
    public double getFalseEasting() {
        return this.falseEasting;
    }
    
    public void setFalseEasting(final double falseEasting) {
        this.falseEasting = falseEasting;
    }
    
    public double getFalseNorthing() {
        return this.falseNorthing;
    }
    
    public void setFalseNorthing(final double falseNorthing) {
        this.falseNorthing = falseNorthing;
    }
    
    @Override
    public ProjectionPoint latLonToProj(final LatLonPoint latLon, final ProjectionPointImpl result) {
        final double fromLat = latLon.getLatitude();
        final double fromLon = latLon.getLongitude();
        double lat = Math.toRadians(fromLat);
        final double lon = Math.toRadians(fromLon);
        if (Math.abs(lat + this.latt) <= 1.0E-6) {
            lat = -this.latt * 0.999999;
        }
        final double sdlon = Math.sin(lon - this.lont);
        final double cdlon = Math.cos(lon - this.lont);
        final double sinlat = Math.sin(lat);
        final double coslat = Math.cos(lat);
        final double k = 2.0 * this.scale / (1.0 + this.sinlatt * sinlat + this.coslatt * coslat * cdlon);
        final double toX = k * coslat * sdlon;
        final double toY = k * (this.coslatt * sinlat - this.sinlatt * coslat * cdlon);
        result.setLocation(toX + this.falseEasting, toY + this.falseNorthing);
        return result;
    }
    
    @Override
    public LatLonPoint projToLatLon(final ProjectionPoint world, final LatLonPointImpl result) {
        final double fromX = world.getX() - this.falseEasting;
        final double fromY = world.getY() - this.falseNorthing;
        final double rho = Math.sqrt(fromX * fromX + fromY * fromY);
        final double c = 2.0 * Math.atan2(rho, 2.0 * this.scale);
        final double sinc = Math.sin(c);
        final double cosc = Math.cos(c);
        double phi;
        if (Math.abs(rho) < 1.0E-6) {
            phi = this.latt;
        }
        else {
            phi = Math.asin(cosc * this.sinlatt + fromY * sinc * this.coslatt / rho);
        }
        final double toLat = Math.toDegrees(phi);
        double lam;
        if (Math.abs(fromX) < 1.0E-6 && Math.abs(fromY) < 1.0E-6) {
            lam = this.lont;
        }
        else if (Math.abs(this.coslatt) < 1.0E-6) {
            lam = this.lont + Math.atan2(fromX, (this.latt > 0.0) ? (-fromY) : fromY);
        }
        else {
            lam = this.lont + Math.atan2(fromX * sinc, rho * this.coslatt * cosc - fromY * sinc * this.sinlatt);
        }
        final double toLon = Math.toDegrees(lam);
        result.setLatitude(toLat);
        result.setLongitude(toLon);
        return result;
    }
    
    @Override
    public float[][] latLonToProj(final float[][] from, final float[][] to, final int latIndex, final int lonIndex) {
        final int cnt = from[0].length;
        final float[] fromLatA = from[latIndex];
        final float[] fromLonA = from[lonIndex];
        final float[] resultXA = to[0];
        final float[] resultYA = to[1];
        for (int i = 0; i < cnt; ++i) {
            final double fromLat = fromLatA[i];
            final double fromLon = fromLonA[i];
            double lat = Math.toRadians(fromLat);
            final double lon = Math.toRadians(fromLon);
            if (Math.abs(lat + this.latt) <= 1.0E-6) {
                lat = -this.latt * 0.999999;
            }
            final double sdlon = Math.sin(lon - this.lont);
            final double cdlon = Math.cos(lon - this.lont);
            final double sinlat = Math.sin(lat);
            final double coslat = Math.cos(lat);
            final double k = 2.0 * this.scale / (1.0 + this.sinlatt * sinlat + this.coslatt * coslat * cdlon);
            final double toX = k * coslat * sdlon;
            final double toY = k * (this.coslatt * sinlat - this.sinlatt * coslat * cdlon);
            resultXA[i] = (float)(toX + this.falseEasting);
            resultYA[i] = (float)(toY + this.falseNorthing);
        }
        return to;
    }
    
    @Override
    public float[][] projToLatLon(final float[][] from, final float[][] to) {
        final int cnt = from[0].length;
        final float[] fromXA = from[0];
        final float[] fromYA = from[1];
        final float[] toLatA = to[0];
        final float[] toLonA = to[1];
        for (int i = 0; i < cnt; ++i) {
            final double fromX = fromXA[i] - this.falseEasting;
            final double fromY = fromYA[i] - this.falseNorthing;
            final double rho = Math.sqrt(fromX * fromX + fromY * fromY);
            final double c = 2.0 * Math.atan2(rho, 2.0 * this.scale);
            final double sinc = Math.sin(c);
            final double cosc = Math.cos(c);
            double phi;
            if (Math.abs(rho) < 1.0E-6) {
                phi = this.latt;
            }
            else {
                phi = Math.asin(cosc * this.sinlatt + fromY * sinc * this.coslatt / rho);
            }
            final double toLat = Math.toDegrees(phi);
            double lam;
            if (Math.abs(fromX) < 1.0E-6 && Math.abs(fromY) < 1.0E-6) {
                lam = this.lont;
            }
            else if (Math.abs(this.coslatt) < 1.0E-6) {
                lam = this.lont + Math.atan2(fromX, (this.latt > 0.0) ? (-fromY) : fromY);
            }
            else {
                lam = this.lont + Math.atan2(fromX * sinc, rho * this.coslatt * cosc - fromY * sinc * this.sinlatt);
            }
            final double toLon = Math.toDegrees(lam);
            toLatA[i] = (float)toLat;
            toLonA[i] = (float)toLon;
        }
        return to;
    }
    
    @Override
    public double[][] latLonToProj(final double[][] from, final double[][] to, final int latIndex, final int lonIndex) {
        final int cnt = from[0].length;
        final double[] fromLatA = from[latIndex];
        final double[] fromLonA = from[lonIndex];
        final double[] resultXA = to[0];
        final double[] resultYA = to[1];
        for (int i = 0; i < cnt; ++i) {
            final double fromLat = fromLatA[i];
            final double fromLon = fromLonA[i];
            double lat = Math.toRadians(fromLat);
            final double lon = Math.toRadians(fromLon);
            if (Math.abs(lat + this.latt) <= 1.0E-6) {
                lat = -this.latt * 0.999999;
            }
            final double sdlon = Math.sin(lon - this.lont);
            final double cdlon = Math.cos(lon - this.lont);
            final double sinlat = Math.sin(lat);
            final double coslat = Math.cos(lat);
            final double k = 2.0 * this.scale / (1.0 + this.sinlatt * sinlat + this.coslatt * coslat * cdlon);
            final double toX = k * coslat * sdlon;
            final double toY = k * (this.coslatt * sinlat - this.sinlatt * coslat * cdlon);
            resultXA[i] = toX + this.falseEasting;
            resultYA[i] = toY + this.falseNorthing;
        }
        return to;
    }
    
    @Override
    public double[][] projToLatLon(final double[][] from, final double[][] to) {
        final int cnt = from[0].length;
        final double[] fromXA = from[0];
        final double[] fromYA = from[1];
        final double[] toLatA = to[0];
        final double[] toLonA = to[1];
        for (int i = 0; i < cnt; ++i) {
            final double fromX = fromXA[i] - this.falseEasting;
            final double fromY = fromYA[i] - this.falseNorthing;
            final double rho = Math.sqrt(fromX * fromX + fromY * fromY);
            final double c = 2.0 * Math.atan2(rho, 2.0 * this.scale);
            final double sinc = Math.sin(c);
            final double cosc = Math.cos(c);
            double phi;
            if (Math.abs(rho) < 1.0E-6) {
                phi = this.latt;
            }
            else {
                phi = Math.asin(cosc * this.sinlatt + fromY * sinc * this.coslatt / rho);
            }
            final double toLat = Math.toDegrees(phi);
            double lam;
            if (Math.abs(fromX) < 1.0E-6 && Math.abs(fromY) < 1.0E-6) {
                lam = this.lont;
            }
            else if (Math.abs(this.coslatt) < 1.0E-6) {
                lam = this.lont + Math.atan2(fromX, (this.latt > 0.0) ? (-fromY) : fromY);
            }
            else {
                lam = this.lont + Math.atan2(fromX * sinc, rho * this.coslatt * cosc - fromY * sinc * this.sinlatt);
            }
            final double toLon = Math.toDegrees(lam);
            toLatA[i] = toLat;
            toLonA[i] = toLon;
        }
        return to;
    }
}
