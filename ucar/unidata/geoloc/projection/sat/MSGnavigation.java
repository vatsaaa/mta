// 
// Decompiled by Procyon v0.5.36
// 

package ucar.unidata.geoloc.projection.sat;

import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import ucar.unidata.geoloc.ProjectionRect;
import ucar.unidata.geoloc.LatLonRect;
import ucar.unidata.geoloc.ProjectionPoint;
import ucar.unidata.geoloc.LatLonPoint;
import ucar.unidata.geoloc.ProjectionPointImpl;
import ucar.unidata.geoloc.LatLonPointImpl;
import ucar.unidata.geoloc.ProjectionImpl;

public class MSGnavigation extends ProjectionImpl
{
    private static final double SAT_HEIGHT = 42164.0;
    private static final double R_EQ = 6378.169;
    private static final double R_POL = 6356.5838;
    private static final double SUB_LON = 0.0;
    private double lat0;
    private double lon0;
    private double major_axis;
    private double minor_axis;
    private double sat_height;
    private double scale_x;
    private double scale_y;
    private double const1;
    private double const2;
    private double const3;
    private double maxR;
    private double maxR2;
    
    public MSGnavigation() {
        this(0.0, 0.0, 6378.169, 6356.5838, 42164.0, 35785.831, 35785.831);
    }
    
    public MSGnavigation(final double lat0, final double lon0, final double major_axis, final double minor_axis, final double sat_height, final double scale_x, final double scale_y) {
        this.lat0 = 0.0;
        this.name = "MSGnavigation";
        this.lon0 = Math.toRadians(lon0);
        this.major_axis = 0.001 * major_axis;
        this.minor_axis = 0.001 * minor_axis;
        this.sat_height = 0.001 * sat_height;
        this.scale_x = scale_x;
        this.scale_y = scale_y;
        this.const1 = major_axis / minor_axis;
        this.const1 *= this.const1;
        this.const2 = 1.0 - minor_axis * minor_axis / (major_axis * major_axis);
        this.const3 = this.sat_height * this.sat_height - this.major_axis * this.major_axis;
        final double P = sat_height / major_axis;
        this.maxR = 0.99 * this.major_axis * Math.sqrt((P - 1.0) / (P + 1.0));
        this.maxR2 = this.maxR * this.maxR;
        this.addParameter("grid_mapping_name", "MSGnavigation");
        this.addParameter("longitude_of_projection_origin", new Double(lon0));
        this.addParameter("latitude_of_projection_origin", new Double(lat0));
        this.addParameter("semi_major_axis", new Double(major_axis));
        this.addParameter("semi_minor_axis", new Double(minor_axis));
        this.addParameter("height_from_earth_center", new Double(sat_height));
        this.addParameter("scale_x", new Double(scale_x));
        this.addParameter("scale_y", new Double(scale_y));
    }
    
    @Override
    public String toString() {
        return "MSGnavigation{lat0=" + this.lat0 + ", lon0=" + this.lon0 + ", major_axis=" + this.major_axis + ", minor_axis=" + this.minor_axis + ", sat_height=" + this.sat_height + ", scale_x=" + this.scale_x + ", scale_y=" + this.scale_y + '}';
    }
    
    private int pixcoord2geocoord(final double xkm, final double ykm, final LatLonPointImpl result) {
        final double xrad = xkm / this.scale_x;
        final double yrad = ykm / this.scale_y;
        final double cosx = Math.cos(xrad);
        final double cosy = Math.cos(yrad);
        final double siny = Math.sin(yrad);
        final double sa = Math.pow(this.sat_height * cosx * cosy, 2.0) - (cosy * cosy + this.const1 * siny * siny) * this.const3;
        if (sa <= 0.0) {
            result.set(Double.POSITIVE_INFINITY, Double.POSITIVE_INFINITY);
            return -1;
        }
        final double sd = Math.sqrt(sa);
        final double sn = (this.sat_height * cosx * cosy - sd) / (cosy * cosy + this.const1 * siny * siny);
        final double s1 = this.sat_height - sn * cosx * cosy;
        final double s2 = sn * Math.sin(xrad) * cosy;
        final double s3 = -sn * siny;
        final double sxy = Math.sqrt(s1 * s1 + s2 * s2);
        final double longi = Math.atan(s2 / s1) + this.lon0;
        final double lati = Math.atan(this.const1 * s3 / sxy);
        result.setLatitude(Math.toDegrees(lati));
        result.setLongitude(Math.toDegrees(longi));
        return 0;
    }
    
    private int geocoord2pixcoord(final double latitude, final double longitude, final ProjectionPointImpl result) {
        if (latitude < -90.0 || latitude > 90.0 || longitude < -180.0 || longitude > 180.0) {
            result.setLocation(Double.POSITIVE_INFINITY, Double.POSITIVE_INFINITY);
            return -1;
        }
        final double lat = Math.toRadians(latitude);
        final double lon = Math.toRadians(longitude) - this.lon0;
        final double cosLon = Math.cos(lon);
        final double c_lat = Math.atan(Math.tan(lat) / this.const1);
        final double coscLat = Math.cos(c_lat);
        final double re = this.minor_axis / Math.sqrt(1.0 - this.const2 * coscLat * coscLat);
        final double r1 = this.sat_height - re * coscLat * cosLon;
        final double r2 = -re * coscLat * Math.sin(lon);
        final double r3 = re * Math.sin(c_lat);
        final double rn = Math.sqrt(r1 * r1 + r2 * r2 + r3 * r3);
        final double dotprod = r1 * (re * coscLat * cosLon) - r2 * r2 - r3 * r3 * this.const1;
        if (dotprod <= 0.0) {
            result.setLocation(Double.POSITIVE_INFINITY, Double.POSITIVE_INFINITY);
            return -1;
        }
        final double xx = Math.atan(-r2 / r1);
        final double yy = Math.asin(-r3 / rn);
        result.setLocation(this.scale_x * xx, this.scale_y * yy);
        return 0;
    }
    
    @Override
    public ProjectionImpl constructCopy() {
        return new MSGnavigation(this.lat0, this.lon0, 1000.0 * this.major_axis, 1000.0 * this.minor_axis, 1000.0 * this.sat_height, this.scale_x, this.scale_y);
    }
    
    @Override
    public String paramsToString() {
        return "";
    }
    
    @Override
    public ProjectionPoint latLonToProj(final LatLonPoint latlon, final ProjectionPointImpl destPoint) {
        final int status = this.geocoord2pixcoord(latlon.getLatitude(), latlon.getLongitude(), destPoint);
        return destPoint;
    }
    
    @Override
    public LatLonPoint projToLatLon(final ProjectionPoint ppt, final LatLonPointImpl destPoint) {
        final int status = this.pixcoord2geocoord(ppt.getX(), ppt.getY(), destPoint);
        return destPoint;
    }
    
    @Override
    public boolean crossSeam(final ProjectionPoint pt1, final ProjectionPoint pt2) {
        return ProjectionPointImpl.isInfinite(pt1) || ProjectionPointImpl.isInfinite(pt2) || (pt1.getX() * pt2.getX() < 0.0 && Math.abs(pt1.getX() - pt2.getX()) > 100.0);
    }
    
    @Override
    public boolean equals(final Object proj) {
        return proj instanceof MSGnavigation;
    }
    
    @Override
    public ProjectionRect latLonToProjBB(final LatLonRect rect) {
        final ProjectionPoint llpt = this.latLonToProj(rect.getLowerLeftPoint(), new ProjectionPointImpl());
        final ProjectionPoint urpt = this.latLonToProj(rect.getUpperRightPoint(), new ProjectionPointImpl());
        final ProjectionPoint lrpt = this.latLonToProj(rect.getLowerRightPoint(), new ProjectionPointImpl());
        final ProjectionPoint ulpt = this.latLonToProj(rect.getUpperLeftPoint(), new ProjectionPointImpl());
        final List<ProjectionPoint> goodPts = new ArrayList<ProjectionPoint>(4);
        int countBad = 0;
        if (!this.addGoodPts(goodPts, llpt)) {
            ++countBad;
        }
        if (!this.addGoodPts(goodPts, urpt)) {
            ++countBad;
        }
        if (!this.addGoodPts(goodPts, lrpt)) {
            ++countBad;
        }
        if (!this.addGoodPts(goodPts, ulpt)) {
            ++countBad;
        }
        if (countBad == 2) {
            if (!ProjectionPointImpl.isInfinite(llpt) && !ProjectionPointImpl.isInfinite(lrpt)) {
                this.addGoodPts(goodPts, new ProjectionPointImpl(0.0, this.maxR));
            }
            else if (!ProjectionPointImpl.isInfinite(ulpt) && !ProjectionPointImpl.isInfinite(llpt)) {
                this.addGoodPts(goodPts, new ProjectionPointImpl(this.maxR, 0.0));
            }
            else if (!ProjectionPointImpl.isInfinite(ulpt) && !ProjectionPointImpl.isInfinite(urpt)) {
                this.addGoodPts(goodPts, new ProjectionPointImpl(0.0, -this.maxR));
            }
            else {
                if (ProjectionPointImpl.isInfinite(urpt) || ProjectionPointImpl.isInfinite(lrpt)) {
                    throw new IllegalStateException();
                }
                this.addGoodPts(goodPts, new ProjectionPointImpl(-this.maxR, 0.0));
            }
        }
        else if (countBad == 3) {
            if (!ProjectionPointImpl.isInfinite(llpt)) {
                final double xcoord = llpt.getX();
                this.addGoodPts(goodPts, new ProjectionPointImpl(xcoord, this.getLimitCoord(xcoord)));
                final double ycoord = llpt.getY();
                this.addGoodPts(goodPts, new ProjectionPointImpl(this.getLimitCoord(ycoord), ycoord));
            }
            else if (!ProjectionPointImpl.isInfinite(urpt)) {
                final double xcoord = urpt.getX();
                this.addGoodPts(goodPts, new ProjectionPointImpl(xcoord, -this.getLimitCoord(xcoord)));
                final double ycoord = urpt.getY();
                this.addGoodPts(goodPts, new ProjectionPointImpl(-this.getLimitCoord(ycoord), ycoord));
            }
            else if (!ProjectionPointImpl.isInfinite(ulpt)) {
                final double xcoord = ulpt.getX();
                this.addGoodPts(goodPts, new ProjectionPointImpl(xcoord, -this.getLimitCoord(xcoord)));
                final double ycoord = ulpt.getY();
                this.addGoodPts(goodPts, new ProjectionPointImpl(this.getLimitCoord(ycoord), ycoord));
            }
            else {
                if (ProjectionPointImpl.isInfinite(lrpt)) {
                    throw new IllegalStateException();
                }
                final double xcoord = lrpt.getX();
                this.addGoodPts(goodPts, new ProjectionPointImpl(xcoord, this.getLimitCoord(xcoord)));
                final double ycoord = lrpt.getY();
                this.addGoodPts(goodPts, new ProjectionPointImpl(-this.getLimitCoord(ycoord), ycoord));
            }
        }
        return this.makeRect(goodPts);
    }
    
    private boolean addGoodPts(final List<ProjectionPoint> goodPts, final ProjectionPoint pt) {
        if (!ProjectionPointImpl.isInfinite(pt)) {
            goodPts.add(pt);
            return true;
        }
        return false;
    }
    
    private double getLimitCoord(final double coord) {
        return Math.sqrt(this.maxR2 - coord * coord);
    }
    
    private ProjectionRect makeRect(final List<ProjectionPoint> goodPts) {
        double minx = Double.MAX_VALUE;
        double miny = Double.MAX_VALUE;
        double maxx = -1.7976931348623157E308;
        double maxy = -1.7976931348623157E308;
        for (final ProjectionPoint pp : goodPts) {
            minx = Math.min(minx, pp.getX());
            maxx = Math.max(maxx, pp.getX());
            miny = Math.min(miny, pp.getY());
            maxy = Math.max(maxy, pp.getY());
        }
        return new ProjectionRect(minx, miny, maxx, maxy);
    }
    
    static void tryit(final double want, final double x) {
        System.out.printf("x = %f %f %f %n", x, x / want, want / x);
    }
    
    private static void doOne(final ProjectionImpl proj, final double lat, final double lon) {
        final LatLonPointImpl startL = new LatLonPointImpl(lat, lon);
        final ProjectionPoint p = proj.latLonToProj(startL);
        final LatLonPointImpl endL = (LatLonPointImpl)proj.projToLatLon(p);
        System.out.println("start  = " + startL.toString(8));
        System.out.println("xy   = " + p.toString());
        System.out.println("end  = " + endL.toString(8));
    }
    
    private static void doTwo(final ProjectionImpl proj, final double x, final double y) {
        final ProjectionPointImpl startL = new ProjectionPointImpl(x, y);
        final LatLonPoint p = proj.projToLatLon(startL);
        final ProjectionPointImpl endL = (ProjectionPointImpl)proj.latLonToProj(p);
        System.out.println("start  = " + startL.toString());
        System.out.println("lat,lon   = " + p.toString());
        System.out.println("end  = " + endL.toString());
    }
    
    public static void main(final String[] arg) {
        final double dx = 1207.0;
        final double dy = 1189.0;
        final double nr = 6610700.0;
        final double scanx = 2.0 * Math.asin(1000000.0 / nr) / dx;
        final double scany = 2.0 * Math.asin(1000000.0 / nr) / dy;
        System.out.printf("scanx = %g urad %n", scanx * 1000000.0);
        System.out.printf("scany = %g urad %n", scany * 1000000.0);
        final double scan2 = 2.0 * Math.asin(1000000.0 / nr) / 3566.0;
        System.out.printf("scan2 = %g urad %n", scan2 * 1000000.0);
    }
}
