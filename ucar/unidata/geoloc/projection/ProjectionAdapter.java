// 
// Decompiled by Procyon v0.5.36
// 

package ucar.unidata.geoloc.projection;

import ucar.unidata.geoloc.ProjectionRect;
import ucar.unidata.geoloc.LatLonPointImpl;
import ucar.unidata.geoloc.ProjectionPoint;
import ucar.unidata.geoloc.ProjectionPointImpl;
import ucar.unidata.geoloc.LatLonPoint;
import ucar.unidata.geoloc.Projection;
import ucar.unidata.geoloc.ProjectionImpl;

public class ProjectionAdapter extends ProjectionImpl
{
    private Projection proj;
    
    public static ProjectionImpl factory(final Projection proj) {
        if (proj instanceof ProjectionImpl) {
            return (ProjectionImpl)proj;
        }
        return new ProjectionAdapter(proj);
    }
    
    @Override
    public ProjectionImpl constructCopy() {
        return new ProjectionAdapter(this.proj);
    }
    
    public ProjectionAdapter(final Projection proj) {
        this.proj = proj;
    }
    
    @Override
    public String getClassName() {
        return this.proj.getClassName();
    }
    
    @Override
    public String paramsToString() {
        return this.proj.paramsToString();
    }
    
    @Override
    public boolean equals(final Object p) {
        return this.proj.equals(p);
    }
    
    @Override
    public ProjectionPoint latLonToProj(final LatLonPoint latlon, final ProjectionPointImpl result) {
        return this.proj.latLonToProj(latlon, result);
    }
    
    @Override
    public LatLonPoint projToLatLon(final ProjectionPoint world, final LatLonPointImpl result) {
        return this.proj.projToLatLon(world, result);
    }
    
    @Override
    public boolean crossSeam(final ProjectionPoint pt1, final ProjectionPoint pt2) {
        return this.proj.crossSeam(pt1, pt2);
    }
    
    @Override
    public ProjectionRect getDefaultMapArea() {
        return this.proj.getDefaultMapArea();
    }
}
