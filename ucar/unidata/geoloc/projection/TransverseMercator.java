// 
// Decompiled by Procyon v0.5.36
// 

package ucar.unidata.geoloc.projection;

import ucar.unidata.util.SpecialMathFunction;
import ucar.unidata.geoloc.LatLonPoint;
import ucar.unidata.geoloc.ProjectionPointImpl;
import ucar.unidata.geoloc.ProjectionPoint;
import ucar.unidata.util.Format;
import ucar.unidata.geoloc.LatLonPointImpl;
import ucar.unidata.geoloc.ProjectionImpl;

public class TransverseMercator extends ProjectionImpl
{
    private double lat0;
    private double lon0;
    private double scale;
    private LatLonPointImpl origin;
    private double falseEasting;
    private double falseNorthing;
    
    @Override
    public ProjectionImpl constructCopy() {
        return new TransverseMercator(this.getOriginLat(), this.getTangentLon(), this.getScale(), this.getFalseEasting(), this.getFalseNorthing());
    }
    
    public TransverseMercator() {
        this(40.0, -105.0, 0.9996);
    }
    
    public TransverseMercator(final double lat0, final double tangentLon, final double scale) {
        this.falseEasting = 0.0;
        this.falseNorthing = 0.0;
        this.lat0 = Math.toRadians(lat0);
        this.lon0 = Math.toRadians(tangentLon);
        this.scale = scale * TransverseMercator.EARTH_RADIUS;
        this.origin = new LatLonPointImpl(lat0, tangentLon);
        this.addParameter("grid_mapping_name", "transverse_mercator");
        this.addParameter("longitude_of_central_meridian", tangentLon);
        this.addParameter("latitude_of_projection_origin", lat0);
        this.addParameter("scale_factor_at_central_meridian", scale);
    }
    
    public TransverseMercator(final double lat0, final double tangentLon, final double scale, final double east, final double north) {
        this.falseEasting = 0.0;
        this.falseNorthing = 0.0;
        this.lat0 = Math.toRadians(lat0);
        this.lon0 = Math.toRadians(tangentLon);
        this.scale = scale * TransverseMercator.EARTH_RADIUS;
        if (!Double.isNaN(east)) {
            this.falseEasting = east;
        }
        if (!Double.isNaN(north)) {
            this.falseNorthing = north;
        }
        this.origin = new LatLonPointImpl(lat0, tangentLon);
        this.addParameter("grid_mapping_name", "transverse_mercator");
        this.addParameter("longitude_of_central_meridian", tangentLon);
        this.addParameter("latitude_of_projection_origin", lat0);
        this.addParameter("scale_factor_at_central_meridian", scale);
        if (this.falseEasting != 0.0 || this.falseNorthing != 0.0) {
            this.addParameter("false_easting", this.falseEasting);
            this.addParameter("false_northing", this.falseNorthing);
            this.addParameter("units", "km");
        }
    }
    
    public double getScale() {
        return this.scale / TransverseMercator.EARTH_RADIUS;
    }
    
    public void setScale(final double scale) {
        this.scale = TransverseMercator.EARTH_RADIUS * scale;
    }
    
    public double getTangentLon() {
        return this.origin.getLongitude();
    }
    
    public void setTangentLon(final double lon) {
        this.origin.setLongitude(lon);
        this.lon0 = Math.toRadians(lon);
    }
    
    public double getOriginLat() {
        return this.origin.getLatitude();
    }
    
    public void setOriginLat(final double lat) {
        this.origin.setLatitude(lat);
        this.lat0 = Math.toRadians(lat);
    }
    
    public double getFalseEasting() {
        return this.falseEasting;
    }
    
    public void setFalseEasting(final double falseEasting) {
        this.falseEasting = falseEasting;
    }
    
    public double getFalseNorthing() {
        return this.falseNorthing;
    }
    
    public void setFalseNorthing(final double falseNorthing) {
        this.falseNorthing = falseNorthing;
    }
    
    @Override
    public String getProjectionTypeLabel() {
        return "Transverse mercator";
    }
    
    @Override
    public String paramsToString() {
        return " origin " + this.origin.toString() + " scale: " + Format.d(this.getScale(), 6);
    }
    
    @Override
    public boolean crossSeam(final ProjectionPoint pt1, final ProjectionPoint pt2) {
        if (ProjectionPointImpl.isInfinite(pt1) || ProjectionPointImpl.isInfinite(pt2)) {
            return true;
        }
        final double y1 = pt1.getY() - this.falseNorthing;
        final double y2 = pt2.getY() - this.falseNorthing;
        return y1 * y2 < 0.0 && Math.abs(y1 - y2) > 2.0 * TransverseMercator.EARTH_RADIUS;
    }
    
    @Override
    public Object clone() {
        final TransverseMercator cl = (TransverseMercator)super.clone();
        cl.origin = new LatLonPointImpl(this.getOriginLat(), this.getTangentLon());
        cl.falseEasting = this.falseEasting;
        cl.falseNorthing = this.falseNorthing;
        return cl;
    }
    
    @Override
    public boolean equals(final Object proj) {
        if (!(proj instanceof TransverseMercator)) {
            return false;
        }
        final TransverseMercator oo = (TransverseMercator)proj;
        return this.getScale() == oo.getScale() && this.getOriginLat() == oo.getOriginLat() && this.getTangentLon() == oo.getTangentLon() && this.falseEasting == this.falseEasting && this.falseNorthing == this.falseNorthing && this.defaultMapArea.equals(oo.defaultMapArea);
    }
    
    @Override
    public ProjectionPoint latLonToProj(final LatLonPoint latLon, final ProjectionPointImpl result) {
        final double fromLat = latLon.getLatitude();
        final double fromLon = latLon.getLongitude();
        final double lon = Math.toRadians(fromLon);
        final double lat = Math.toRadians(fromLat);
        final double dlon = lon - this.lon0;
        final double b = Math.cos(lat) * Math.sin(dlon);
        double toX;
        double toY;
        if (Math.abs(Math.abs(b) - 1.0) < 1.0E-6) {
            toX = 0.0;
            toY = 0.0;
        }
        else {
            toX = this.scale * SpecialMathFunction.atanh(b);
            toY = this.scale * (Math.atan2(Math.tan(lat), Math.cos(dlon)) - this.lat0);
        }
        result.setLocation(toX + this.falseEasting, toY + this.falseNorthing);
        return result;
    }
    
    @Override
    public LatLonPoint projToLatLon(final ProjectionPoint world, final LatLonPointImpl result) {
        final double fromX = world.getX();
        final double fromY = world.getY();
        final double x = (fromX - this.falseEasting) / this.scale;
        final double d = (fromY - this.falseNorthing) / this.scale + this.lat0;
        final double toLon = Math.toDegrees(this.lon0 + Math.atan2(SpecialMathFunction.sinh(x), Math.cos(d)));
        final double toLat = Math.toDegrees(Math.asin(Math.sin(d) / SpecialMathFunction.cosh(x)));
        result.setLatitude(toLat);
        result.setLongitude(toLon);
        return result;
    }
    
    @Override
    public float[][] latLonToProj(final float[][] from, final float[][] to, final int latIndex, final int lonIndex) {
        final int cnt = from[0].length;
        final float[] fromLatA = from[latIndex];
        final float[] fromLonA = from[lonIndex];
        final float[] resultXA = to[0];
        final float[] resultYA = to[1];
        for (int i = 0; i < cnt; ++i) {
            final double fromLat = fromLatA[i];
            final double fromLon = fromLonA[i];
            final double lon = Math.toRadians(fromLon);
            final double lat = Math.toRadians(fromLat);
            final double dlon = lon - this.lon0;
            final double b = Math.cos(lat) * Math.sin(dlon);
            double toX;
            double toY;
            if (Math.abs(Math.abs(b) - 1.0) < 1.0E-6) {
                toX = 0.0;
                toY = 0.0;
            }
            else {
                toX = this.scale * SpecialMathFunction.atanh(b) + this.falseEasting;
                toY = this.scale * (Math.atan2(Math.tan(lat), Math.cos(dlon)) - this.lat0) + this.falseNorthing;
            }
            resultXA[i] = (float)toX;
            resultYA[i] = (float)toY;
        }
        return to;
    }
    
    @Override
    public float[][] projToLatLon(final float[][] from, final float[][] to) {
        final int cnt = from[0].length;
        final float[] fromXA = from[0];
        final float[] fromYA = from[1];
        final float[] toLatA = to[0];
        final float[] toLonA = to[1];
        for (int i = 0; i < cnt; ++i) {
            final double fromX = fromXA[i];
            final double fromY = fromYA[i];
            final double x = (fromX - this.falseEasting) / this.scale;
            final double d = (fromY - this.falseNorthing) / this.scale + this.lat0;
            final double toLon = Math.toDegrees(this.lon0 + Math.atan2(SpecialMathFunction.sinh(x), Math.cos(d)));
            final double toLat = Math.toDegrees(Math.asin(Math.sin(d) / SpecialMathFunction.cosh(x)));
            toLatA[i] = (float)toLat;
            toLonA[i] = (float)toLon;
        }
        return to;
    }
    
    @Override
    public double[][] latLonToProj(final double[][] from, final double[][] to, final int latIndex, final int lonIndex) {
        final int cnt = from[0].length;
        final double[] fromLatA = from[latIndex];
        final double[] fromLonA = from[lonIndex];
        final double[] resultXA = to[0];
        final double[] resultYA = to[1];
        for (int i = 0; i < cnt; ++i) {
            final double fromLat = fromLatA[i];
            final double fromLon = fromLonA[i];
            final double lon = Math.toRadians(fromLon);
            final double lat = Math.toRadians(fromLat);
            final double dlon = lon - this.lon0;
            final double b = Math.cos(lat) * Math.sin(dlon);
            double toX;
            double toY;
            if (Math.abs(Math.abs(b) - 1.0) < 1.0E-6) {
                toX = 0.0;
                toY = 0.0;
            }
            else {
                toX = this.scale * SpecialMathFunction.atanh(b) + this.falseEasting;
                toY = this.scale * (Math.atan2(Math.tan(lat), Math.cos(dlon)) - this.lat0) + this.falseNorthing;
            }
            resultXA[i] = toX;
            resultYA[i] = toY;
        }
        return to;
    }
    
    @Override
    public double[][] projToLatLon(final double[][] from, final double[][] to) {
        final int cnt = from[0].length;
        final double[] fromXA = from[0];
        final double[] fromYA = from[1];
        final double[] toLatA = to[0];
        final double[] toLonA = to[1];
        for (int i = 0; i < cnt; ++i) {
            final double fromX = fromXA[i];
            final double fromY = fromYA[i];
            final double x = (fromX - this.falseEasting) / this.scale;
            final double d = (fromY - this.falseNorthing) / this.scale + this.lat0;
            final double toLon = Math.toDegrees(this.lon0 + Math.atan2(SpecialMathFunction.sinh(x), Math.cos(d)));
            final double toLat = Math.toDegrees(Math.asin(Math.sin(d) / SpecialMathFunction.cosh(x)));
            toLatA[i] = toLat;
            toLonA[i] = toLon;
        }
        return to;
    }
}
