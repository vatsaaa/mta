// 
// Decompiled by Procyon v0.5.36
// 

package ucar.unidata.geoloc.projection;

import ucar.unidata.util.SpecialMathFunction;
import ucar.unidata.geoloc.LatLonPoint;
import ucar.unidata.geoloc.ProjectionPointImpl;
import ucar.unidata.geoloc.ProjectionPoint;
import ucar.unidata.util.Format;
import ucar.unidata.geoloc.LatLonPointImpl;
import ucar.unidata.geoloc.ProjectionImpl;

public class Mercator extends ProjectionImpl
{
    private double lon0;
    private double par;
    private double par_r;
    private double A;
    private double falseEasting;
    private double falseNorthing;
    private LatLonPointImpl origin;
    
    @Override
    public ProjectionImpl constructCopy() {
        return new Mercator(this.getOriginLon(), this.getParallel(), this.getFalseEasting(), this.getFalseNorthing());
    }
    
    public Mercator() {
        this(-105.0, 20.0);
    }
    
    @Deprecated
    public Mercator(final double lat0, final double lon0, final double par) {
        this(lon0, par);
    }
    
    public Mercator(final double lon0, final double par) {
        this(lon0, par, 0.0, 0.0);
    }
    
    public Mercator(final double lon0, final double par, final double false_easting, final double false_northing) {
        this.origin = new LatLonPointImpl(0.0, lon0);
        this.lon0 = lon0;
        this.par = par;
        this.falseEasting = false_easting;
        this.falseNorthing = false_northing;
        this.par_r = Math.toRadians(par);
        this.precalculate();
        this.addParameter("grid_mapping_name", "mercator");
        this.addParameter("longitude_of_projection_origin", lon0);
        this.addParameter("standard_parallel", par);
        if (false_easting != 0.0) {
            this.addParameter("false_easting", false_easting);
        }
        if (false_northing != 0.0) {
            this.addParameter("false_northing", false_northing);
        }
    }
    
    private void precalculate() {
        this.A = Mercator.EARTH_RADIUS * Math.cos(this.par_r);
    }
    
    public double getParallel() {
        return this.par;
    }
    
    public void setParallel(final double par) {
        this.par = par;
        this.precalculate();
    }
    
    public double getOriginLon() {
        return this.origin.getLongitude();
    }
    
    public void setOriginLon(final double lon) {
        this.origin.setLongitude(lon);
        this.lon0 = lon;
        this.precalculate();
    }
    
    @Deprecated
    public void setOriginLat(final double lat) {
    }
    
    public double getFalseEasting() {
        return this.falseEasting;
    }
    
    public void setFalseEasting(final double falseEasting) {
        this.falseEasting = falseEasting;
    }
    
    public double getFalseNorthing() {
        return this.falseNorthing;
    }
    
    public void setFalseNorthing(final double falseNorthing) {
        this.falseNorthing = falseNorthing;
    }
    
    @Override
    public String paramsToString() {
        return " origin " + this.origin.toString() + " parellel: " + Format.d(this.getParallel(), 6);
    }
    
    @Override
    public boolean crossSeam(final ProjectionPoint pt1, final ProjectionPoint pt2) {
        return ProjectionPointImpl.isInfinite(pt1) || ProjectionPointImpl.isInfinite(pt2) || pt1.getX() * pt2.getX() < 0.0;
    }
    
    @Override
    public Object clone() {
        final Mercator cl = (Mercator)super.clone();
        cl.origin = new LatLonPointImpl(0.0, this.getOriginLon());
        return cl;
    }
    
    @Override
    public boolean equals(final Object proj) {
        if (!(proj instanceof Mercator)) {
            return false;
        }
        final Mercator oo = (Mercator)proj;
        return this.getParallel() == oo.getParallel() && this.getOriginLon() == oo.getOriginLon() && this.defaultMapArea.equals(oo.defaultMapArea);
    }
    
    @Override
    public ProjectionPoint latLonToProj(final LatLonPoint latLon, final ProjectionPointImpl result) {
        final double fromLat = latLon.getLatitude();
        final double fromLon = latLon.getLongitude();
        final double fromLat_r = Math.toRadians(fromLat);
        double toX;
        double toY;
        if (Math.abs(90.0 - Math.abs(fromLat)) < 1.0E-6) {
            toX = Double.POSITIVE_INFINITY;
            toY = Double.POSITIVE_INFINITY;
        }
        else {
            toX = this.A * Math.toRadians(LatLonPointImpl.range180(fromLon - this.lon0));
            toY = this.A * SpecialMathFunction.atanh(Math.sin(fromLat_r));
        }
        result.setLocation(toX + this.falseEasting, toY + this.falseNorthing);
        return result;
    }
    
    @Override
    public LatLonPoint projToLatLon(final ProjectionPoint world, final LatLonPointImpl result) {
        final double fromX = world.getX() - this.falseEasting;
        final double fromY = world.getY() - this.falseNorthing;
        final double toLon = Math.toDegrees(fromX / this.A) + this.lon0;
        final double e = Math.exp(-fromY / this.A);
        final double toLat = Math.toDegrees(1.5707963267948966 - 2.0 * Math.atan(e));
        result.setLatitude(toLat);
        result.setLongitude(toLon);
        return result;
    }
}
