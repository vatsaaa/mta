// 
// Decompiled by Procyon v0.5.36
// 

package ucar.unidata.geoloc.projection;

import ucar.unidata.geoloc.LatLonPoint;
import ucar.unidata.geoloc.LatLonPointImpl;
import ucar.unidata.geoloc.EarthEllipsoid;

class Utm_To_Gdc_Converter
{
    static final double DEGREES_PER_RADIAN = 57.29577951308232;
    private double A;
    private double F;
    private double C;
    private double Eps2;
    private double Eps21;
    private double Eps25;
    private double Con;
    private double Con2;
    private double EF;
    private double Epsp2;
    private double Con6;
    private double Con24;
    private double Con120;
    private double Con720;
    private double polx2b;
    private double polx3b;
    private double polx4b;
    private double polx5b;
    private double conap;
    private int zone;
    private boolean hemisphere_north;
    
    public Utm_To_Gdc_Converter(final double a, final double f, final int zone, final boolean hemisphere_north) {
        this.init(a, f, zone, hemisphere_north);
    }
    
    public Utm_To_Gdc_Converter(final int zone, final boolean hemisphere_north) {
        this(EarthEllipsoid.WGS84, zone, hemisphere_north);
    }
    
    public Utm_To_Gdc_Converter(final EarthEllipsoid ellipse, final int zone, final boolean hemisphere_north) {
        this.init(ellipse.getMajor(), 1.0 / ellipse.getFlattening(), zone, hemisphere_north);
    }
    
    private void init(final double a, final double f, final int zone, final boolean hemisphere_north) {
        this.A = a;
        this.F = 1.0 / f;
        this.zone = zone;
        this.hemisphere_north = hemisphere_north;
        this.C = this.A * (1.0 - this.F);
        this.Eps2 = this.F * (2.0 - this.F);
        this.Eps25 = 0.25 * this.Eps2;
        this.EF = this.F / (2.0 - this.F);
        this.Con = 1.0 - this.Eps2;
        this.Con2 = 2.0 / (1.0 - this.Eps2);
        this.Con6 = 0.166666666666667;
        this.Con24 = 0.1666666666666668 / (1.0 - this.Eps2);
        this.Con120 = 0.00833333333333333;
        this.Con720 = 0.00555555555555552 / (1.0 - this.Eps2);
        final double polx1a = 1.0 - this.Eps2 / 4.0 - 0.046875 * Math.pow(this.Eps2, 2.0) - 0.01953125 * Math.pow(this.Eps2, 3.0) - 0.01068115234375 * Math.pow(this.Eps2, 4.0);
        this.conap = this.A * polx1a;
        final double polx2a = 1.5 * this.EF - 0.84375 * Math.pow(this.EF, 3.0);
        final double polx4a = 1.3125 * Math.pow(this.EF, 2.0) - 1.71875 * Math.pow(this.EF, 4.0);
        final double polx6a = 1.5729166666666667 * Math.pow(this.EF, 3.0);
        final double polx8a = 2.142578125 * Math.pow(this.EF, 4.0);
        this.polx2b = polx2a * 2.0 + polx4a * 4.0 + polx6a * 6.0 + polx8a * 8.0;
        this.polx3b = polx4a * -8.0 - polx6a * 32.0 - 80.0 * polx8a;
        this.polx4b = polx6a * 32.0 + 192.0 * polx8a;
        this.polx5b = -128.0 * polx8a;
    }
    
    public double getA() {
        return this.A;
    }
    
    public double getF() {
        return this.F;
    }
    
    public int getZone() {
        return this.zone;
    }
    
    public boolean isNorth() {
        return this.hemisphere_north;
    }
    
    public LatLonPoint projToLatLon(final double x, final double y, final LatLonPointImpl latlon) {
        double source_x = x * 1000.0;
        source_x = (source_x - 500000.0) / 0.9996;
        double source_y;
        if (this.hemisphere_north) {
            source_y = y * 1000.0 / 0.9996;
        }
        else {
            source_y = (y * 1000.0 - 1.0E7) / 0.9996;
        }
        final double u = source_y / this.conap;
        final double su = Math.sin(u);
        final double cu = Math.cos(u);
        final double su2 = su * su;
        final double xlon0 = (6.0 * this.zone - 183.0) / 57.29577951308232;
        final double temp = this.polx2b + su2 * (this.polx3b + su2 * (this.polx4b + su2 * this.polx5b));
        final double phi1 = u + su * cu * temp;
        final double sp = Math.sin(phi1);
        final double cp = Math.cos(phi1);
        final double tp = sp / cp;
        final double tp2 = tp * tp;
        final double sp2 = sp * sp;
        final double cp2 = cp * cp;
        final double eta2 = this.Epsp2 * cp2;
        final double top = 0.25 - sp2 * (this.Eps2 / 4.0);
        final double rn = this.A / (0.25 - this.Eps25 * sp2 + 0.249998608869975 + (0.25 - this.Eps25 * sp2) / (0.25 - this.Eps25 * sp2 + 0.249998608869975));
        final double b3 = 1.0 + tp2 + tp2 + eta2;
        final double b4 = 5.0 + tp2 * (3.0 - 9.0 * eta2) + eta2 * (1.0 - 4.0 * eta2);
        double b5 = 5.0 + tp2 * (tp2 * 24.0 + 28.0);
        b5 += eta2 * (tp2 * 8.0 + 6.0);
        double b6 = 46.0 - 3.0 * eta2 + tp2 * (-252.0 - tp2 * 90.0);
        b6 = eta2 * (b6 + eta2 * tp2 * (tp2 * 225.0 - 66.0));
        b6 += 61.0 + tp2 * (tp2 * 45.0 + 90.0);
        final double d1 = source_x / rn;
        final double d2 = d1 * d1;
        final double latitude = phi1 - tp * top * (d2 * (this.Con2 + d2 * (-this.Con24 * b4 + d2 * this.Con720 * b6)));
        final double longitude = xlon0 + d1 * (1.0 + d2 * (-this.Con6 * b3 + d2 * this.Con120 * b5)) / cp;
        latlon.setLatitude(latitude * 57.29577951308232);
        latlon.setLongitude(longitude * 57.29577951308232);
        return latlon;
    }
    
    public float[][] projToLatLon(final float[][] from, final float[][] to) {
        final double xlon0 = (6.0 * this.zone - 183.0) / 57.29577951308232;
        for (int i = 0; i < from[0].length; ++i) {
            double source_x = from[0][i] * 1000.0;
            source_x = (source_x - 500000.0) / 0.9996;
            double source_y;
            if (this.hemisphere_north) {
                source_y = from[1][i] * 1000.0 / 0.9996;
            }
            else {
                source_y = (from[1][i] * 1000.0 - 1.0E7) / 0.9996;
            }
            final double u = source_y / this.conap;
            final double su = Math.sin(u);
            final double cu = Math.cos(u);
            final double su2 = su * su;
            final double temp = this.polx2b + su2 * (this.polx3b + su2 * (this.polx4b + su2 * this.polx5b));
            final double phi1 = u + su * cu * temp;
            final double sp = Math.sin(phi1);
            final double cp = Math.cos(phi1);
            final double tp = sp / cp;
            final double tp2 = tp * tp;
            final double sp2 = sp * sp;
            final double cp2 = cp * cp;
            final double eta2 = this.Epsp2 * cp2;
            final double top = 0.25 - sp2 * (this.Eps2 / 4.0);
            final double rn = this.A / (0.25 - this.Eps25 * sp2 + 0.249998608869975 + (0.25 - this.Eps25 * sp2) / (0.25 - this.Eps25 * sp2 + 0.249998608869975));
            final double b3 = 1.0 + tp2 + tp2 + eta2;
            final double b4 = 5.0 + tp2 * (3.0 - 9.0 * eta2) + eta2 * (1.0 - 4.0 * eta2);
            double b5 = 5.0 + tp2 * (tp2 * 24.0 + 28.0);
            b5 += eta2 * (tp2 * 8.0 + 6.0);
            double b6 = 46.0 - 3.0 * eta2 + tp2 * (-252.0 - tp2 * 90.0);
            b6 = eta2 * (b6 + eta2 * tp2 * (tp2 * 225.0 - 66.0));
            b6 += 61.0 + tp2 * (tp2 * 45.0 + 90.0);
            final double d1 = source_x / rn;
            final double d2 = d1 * d1;
            final double latitude = phi1 - tp * top * (d2 * (this.Con2 + d2 * (-this.Con24 * b4 + d2 * this.Con720 * b6)));
            final double longitude = xlon0 + d1 * (1.0 + d2 * (-this.Con6 * b3 + d2 * this.Con120 * b5)) / cp;
            to[0][i] = (float)(latitude * 57.29577951308232);
            to[1][i] = (float)(longitude * 57.29577951308232);
        }
        return to;
    }
    
    public double[][] projToLatLon(final double[][] from, final double[][] to) {
        final double xlon0 = (6.0 * this.zone - 183.0) / 57.29577951308232;
        for (int i = 0; i < from[0].length; ++i) {
            double source_x = from[0][i] * 1000.0;
            source_x = (source_x - 500000.0) / 0.9996;
            double source_y;
            if (this.hemisphere_north) {
                source_y = from[1][i] * 1000.0 / 0.9996;
            }
            else {
                source_y = (from[1][i] * 1000.0 - 1.0E7) / 0.9996;
            }
            final double u = source_y / this.conap;
            final double su = Math.sin(u);
            final double cu = Math.cos(u);
            final double su2 = su * su;
            final double temp = this.polx2b + su2 * (this.polx3b + su2 * (this.polx4b + su2 * this.polx5b));
            final double phi1 = u + su * cu * temp;
            final double sp = Math.sin(phi1);
            final double cp = Math.cos(phi1);
            final double tp = sp / cp;
            final double tp2 = tp * tp;
            final double sp2 = sp * sp;
            final double cp2 = cp * cp;
            final double eta2 = this.Epsp2 * cp2;
            final double top = 0.25 - sp2 * (this.Eps2 / 4.0);
            final double rn = this.A / (0.25 - this.Eps25 * sp2 + 0.249998608869975 + (0.25 - this.Eps25 * sp2) / (0.25 - this.Eps25 * sp2 + 0.249998608869975));
            final double b3 = 1.0 + tp2 + tp2 + eta2;
            final double b4 = 5.0 + tp2 * (3.0 - 9.0 * eta2) + eta2 * (1.0 - 4.0 * eta2);
            double b5 = 5.0 + tp2 * (tp2 * 24.0 + 28.0);
            b5 += eta2 * (tp2 * 8.0 + 6.0);
            double b6 = 46.0 - 3.0 * eta2 + tp2 * (-252.0 - tp2 * 90.0);
            b6 = eta2 * (b6 + eta2 * tp2 * (tp2 * 225.0 - 66.0));
            b6 += 61.0 + tp2 * (tp2 * 45.0 + 90.0);
            final double d1 = source_x / rn;
            final double d2 = d1 * d1;
            final double latitude = phi1 - tp * top * (d2 * (this.Con2 + d2 * (-this.Con24 * b4 + d2 * this.Con720 * b6)));
            final double longitude = xlon0 + d1 * (1.0 + d2 * (-this.Con6 * b3 + d2 * this.Con120 * b5)) / cp;
            to[0][i] = latitude * 57.29577951308232;
            to[1][i] = longitude * 57.29577951308232;
        }
        return to;
    }
}
