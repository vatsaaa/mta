// 
// Decompiled by Procyon v0.5.36
// 

package ucar.unidata.geoloc.projection;

import ucar.unidata.geoloc.LatLonPointImpl;
import ucar.unidata.geoloc.ProjectionPoint;
import ucar.unidata.geoloc.ProjectionPointImpl;
import ucar.unidata.geoloc.EarthEllipsoid;

class Gdc_To_Utm_Converter
{
    static final double RADIANS_PER_DEGREE = 0.017453292519943295;
    private double A;
    private double F;
    private double C;
    private double Eps2;
    private double Eps25;
    private double Epps2;
    private double CScale;
    private double poly1b;
    private double poly2b;
    private double poly3b;
    private double poly4b;
    private double poly5b;
    private int zone;
    private double axlon0;
    private double axlon0_deg;
    private boolean isNorth;
    
    public Gdc_To_Utm_Converter(final double a, final double f, final int zone, final boolean hemisphere_north) {
        this.CScale = 0.9996;
        this.init(a, f, zone, hemisphere_north);
    }
    
    public Gdc_To_Utm_Converter(final int zone, final boolean hemisphere_north) {
        this(EarthEllipsoid.WGS84, zone, hemisphere_north);
    }
    
    public Gdc_To_Utm_Converter(final EarthEllipsoid ellipse, final int zone, final boolean isNorth) {
        this.CScale = 0.9996;
        this.init(ellipse.getMajor(), 1.0 / ellipse.getFlattening(), zone, isNorth);
    }
    
    protected void init(final double a, final double f, final int zone, final boolean isNorth) {
        this.A = a;
        this.F = 1.0 / f;
        this.zone = zone;
        this.isNorth = isNorth;
        this.axlon0_deg = zone * 6 - 183;
        this.axlon0 = this.axlon0_deg * 0.017453292519943295;
        this.C = this.A * (1.0 - this.F);
        this.Eps2 = this.F * (2.0 - this.F);
        this.Eps25 = 0.25 * this.Eps2;
        this.Epps2 = this.Eps2 / (1.0 - this.Eps2);
        double polx2b = this.Eps2 + 0.25 * Math.pow(this.Eps2, 2.0) + 0.1171875 * Math.pow(this.Eps2, 3.0) - 0.111083984375 * Math.pow(this.Eps2, 4.0);
        polx2b *= 0.375;
        double polx3b = Math.pow(this.Eps2, 2.0) + 0.75 * Math.pow(this.Eps2, 3.0) - 0.6015625 * Math.pow(this.Eps2, 4.0);
        polx3b *= 0.05859375;
        double polx4b = Math.pow(this.Eps2, 3.0) - 1.28125 * Math.pow(this.Eps2, 4.0);
        polx4b = polx4b * 35.0 / 3072.0;
        final double polx5b = -0.00240325927734375 * Math.pow(this.Eps2, 4.0);
        this.poly1b = 1.0 - 0.25 * this.Eps2 - 0.046875 * Math.pow(this.Eps2, 2.0) - 0.01953125 * Math.pow(this.Eps2, 3.0) - 0.01068115234375 * Math.pow(this.Eps2, 4.0);
        this.poly2b = polx2b * -2.0 + polx3b * 4.0 - polx4b * 6.0 + polx5b * 8.0;
        this.poly3b = polx3b * -8.0 + polx4b * 32.0 - polx5b * 80.0;
        this.poly4b = polx4b * -32.0 + polx5b * 192.0;
        this.poly5b = polx5b * -128.0;
    }
    
    public double getCentralMeridian() {
        return this.axlon0_deg;
    }
    
    public ProjectionPoint latLonToProj(final double latitude, double longitude, final ProjectionPointImpl result) {
        longitude = LatLonPointImpl.lonNormal(longitude, this.axlon0_deg);
        final double source_lat = latitude * 0.017453292519943295;
        final double source_lon = longitude * 0.017453292519943295;
        final double s1 = Math.sin(source_lat);
        final double c1 = Math.cos(source_lat);
        final double tx = s1 / c1;
        final double s2 = s1 * s1;
        final double rn = this.A / (0.25 - this.Eps25 * s2 + 0.249998608869975 + (0.25 - this.Eps25 * s2) / (0.25 - this.Eps25 * s2 + 0.249998608869975));
        final double al = (source_lon - this.axlon0) * c1;
        double sm = s1 * c1 * (this.poly2b + s2 * (this.poly3b + s2 * (this.poly4b + s2 * this.poly5b)));
        sm = this.A * (this.poly1b * source_lat + sm);
        final double tn2 = tx * tx;
        final double cee = this.Epps2 * c1 * c1;
        final double al2 = al * al;
        double poly1 = 1.0 - tn2 + cee;
        double poly2 = 5.0 + tn2 * (tn2 - 18.0) + cee * (14.0 - tn2 * 58.0);
        double x = this.CScale * rn * al * (1.0 + al2 * (0.166666666666667 * poly1 + 0.00833333333333333 * al2 * poly2));
        x += 500000.0;
        poly1 = 5.0 - tn2 + cee * (cee * 4.0 + 9.0);
        poly2 = 61.0 + tn2 * (tn2 - 58.0) + cee * (270.0 - tn2 * 330.0);
        double y = this.CScale * (sm + rn * tx * al2 * (0.5 + al2 * (0.0416666666666667 * poly1 + 0.00138888888888888 * al2 * poly2)));
        if (source_lat < 0.0) {
            y += 1.0E7;
        }
        result.setLocation(x * 0.001, y * 0.001);
        return result;
    }
    
    public double[][] latLonToProj(final double[][] from, final double[][] to, final int latIndex, final int lonIndex) {
        for (int i = 0; i < from[0].length; ++i) {
            final double longitude = LatLonPointImpl.lonNormal(from[lonIndex][i], this.axlon0_deg);
            final double source_lat = from[latIndex][i] * 0.017453292519943295;
            final double source_lon = longitude * 0.017453292519943295;
            final double s1 = Math.sin(source_lat);
            final double c1 = Math.cos(source_lat);
            final double tx = s1 / c1;
            final double s2 = s1 * s1;
            final double rn = this.A / (0.25 - this.Eps25 * s2 + 0.249998608869975 + (0.25 - this.Eps25 * s2) / (0.25 - this.Eps25 * s2 + 0.249998608869975));
            final double al = (source_lon - this.axlon0) * c1;
            double sm = s1 * c1 * (this.poly2b + s2 * (this.poly3b + s2 * (this.poly4b + s2 * this.poly5b)));
            sm = this.A * (this.poly1b * source_lat + sm);
            final double tn2 = tx * tx;
            final double cee = this.Epps2 * c1 * c1;
            final double al2 = al * al;
            double poly1 = 1.0 - tn2 + cee;
            double poly2 = 5.0 + tn2 * (tn2 - 18.0) + cee * (14.0 - tn2 * 58.0);
            double x = this.CScale * rn * al * (1.0 + al2 * (0.166666666666667 * poly1 + 0.00833333333333333 * al2 * poly2));
            x += 500000.0;
            poly1 = 5.0 - tn2 + cee * (cee * 4.0 + 9.0);
            poly2 = 61.0 + tn2 * (tn2 - 58.0) + cee * (270.0 - tn2 * 330.0);
            double y = this.CScale * (sm + rn * tx * al2 * (0.5 + al2 * (0.0416666666666667 * poly1 + 0.00138888888888888 * al2 * poly2)));
            if (source_lat < 0.0) {
                y += 1.0E7;
            }
            to[0][i] = x * 0.001;
            to[1][i] = y * 0.001;
        }
        return to;
    }
    
    public float[][] latLonToProj(final float[][] from, final float[][] to, final int latIndex, final int lonIndex) {
        for (int i = 0; i < from[0].length; ++i) {
            final double longitude = LatLonPointImpl.lonNormal(from[lonIndex][i], this.axlon0_deg);
            final double source_lat = from[latIndex][i] * 0.017453292519943295;
            final double source_lon = longitude * 0.017453292519943295;
            final double s1 = Math.sin(source_lat);
            final double c1 = Math.cos(source_lat);
            final double tx = s1 / c1;
            final double s2 = s1 * s1;
            final double rn = this.A / (0.25 - this.Eps25 * s2 + 0.249998608869975 + (0.25 - this.Eps25 * s2) / (0.25 - this.Eps25 * s2 + 0.249998608869975));
            final double al = (source_lon - this.axlon0) * c1;
            double sm = s1 * c1 * (this.poly2b + s2 * (this.poly3b + s2 * (this.poly4b + s2 * this.poly5b)));
            sm = this.A * (this.poly1b * source_lat + sm);
            final double tn2 = tx * tx;
            final double cee = this.Epps2 * c1 * c1;
            final double al2 = al * al;
            double poly1 = 1.0 - tn2 + cee;
            double poly2 = 5.0 + tn2 * (tn2 - 18.0) + cee * (14.0 - tn2 * 58.0);
            double x = this.CScale * rn * al * (1.0 + al2 * (0.166666666666667 * poly1 + 0.00833333333333333 * al2 * poly2));
            x += 500000.0;
            poly1 = 5.0 - tn2 + cee * (cee * 4.0 + 9.0);
            poly2 = 61.0 + tn2 * (tn2 - 58.0) + cee * (270.0 - tn2 * 330.0);
            double y = this.CScale * (sm + rn * tx * al2 * (0.5 + al2 * (0.0416666666666667 * poly1 + 0.00138888888888888 * al2 * poly2)));
            if (source_lat < 0.0) {
                y += 1.0E7;
            }
            to[0][i] = (float)(x * 0.001);
            to[1][i] = (float)(y * 0.001);
        }
        return to;
    }
}
