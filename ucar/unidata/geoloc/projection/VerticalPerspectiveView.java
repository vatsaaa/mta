// 
// Decompiled by Procyon v0.5.36
// 

package ucar.unidata.geoloc.projection;

import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import ucar.unidata.geoloc.ProjectionRect;
import ucar.unidata.geoloc.LatLonRect;
import ucar.unidata.geoloc.LatLonPoint;
import ucar.unidata.geoloc.ProjectionPointImpl;
import ucar.unidata.geoloc.ProjectionPoint;
import ucar.unidata.geoloc.LatLonPointImpl;
import ucar.unidata.geoloc.ProjectionImpl;

public class VerticalPerspectiveView extends ProjectionImpl
{
    private double P;
    private double H;
    private double R;
    private double lon0Degrees;
    private double lat0;
    private double lon0;
    private double cosLat0;
    private double sinLat0;
    private double false_east;
    private double false_north;
    private double maxR;
    private double maxR2;
    private LatLonPointImpl origin;
    private boolean spherical;
    
    @Override
    public ProjectionImpl constructCopy() {
        return new VerticalPerspectiveView(this.getOriginLat(), this.getOriginLon(), this.R, this.getHeight(), this.false_east, this.false_north);
    }
    
    public VerticalPerspectiveView() {
        this(0.0, 0.0, VerticalPerspectiveView.EARTH_RADIUS, 35800.0);
    }
    
    public VerticalPerspectiveView(final double lat0, final double lon0) {
        this(lat0, lon0, VerticalPerspectiveView.EARTH_RADIUS, 35800.0);
    }
    
    public VerticalPerspectiveView(final double lat0, final double lon0, final double earthRadius, final double distance) {
        this(lat0, lon0, earthRadius, distance, 0.0, 0.0);
    }
    
    public VerticalPerspectiveView(final double lat0, final double lon0, final double earthRadius, final double distance, final double false_easting, final double false_northing) {
        this.spherical = true;
        this.lat0 = Math.toRadians(lat0);
        this.lon0 = Math.toRadians(lon0);
        this.R = earthRadius;
        this.H = distance;
        this.false_east = false_easting;
        this.false_north = false_northing;
        this.origin = new LatLonPointImpl(lat0, lon0);
        this.precalculate();
        this.addParameter("grid_mapping_name", "vertical_perspective");
        this.addParameter("latitude_of_projection_origin", lat0);
        this.addParameter("longitude_of_projection_origin", lon0);
        this.addParameter("height_above_earth", distance);
        this.addParameter("false_easting", false_easting);
        this.addParameter("false_northing", false_northing);
        this.addParameter("earth_radius", earthRadius);
    }
    
    private void precalculate() {
        this.sinLat0 = Math.sin(this.lat0);
        this.cosLat0 = Math.cos(this.lat0);
        this.lon0Degrees = Math.toDegrees(this.lon0);
        this.P = 1.0 + this.H / this.R;
        this.maxR = 0.99 * this.R * Math.sqrt((this.P - 1.0) / (this.P + 1.0));
        this.maxR2 = this.maxR * this.maxR;
    }
    
    @Override
    public Object clone() {
        final VerticalPerspectiveView cl = (VerticalPerspectiveView)super.clone();
        cl.origin = new LatLonPointImpl(this.getOriginLat(), this.getOriginLon());
        return cl;
    }
    
    @Override
    public boolean equals(final Object proj) {
        if (!(proj instanceof VerticalPerspectiveView)) {
            return false;
        }
        final VerticalPerspectiveView oo = (VerticalPerspectiveView)proj;
        return this.getOriginLat() == oo.getOriginLat() && this.getOriginLon() == oo.getOriginLon() && this.getHeight() == oo.getHeight() && this.defaultMapArea.equals(oo.defaultMapArea);
    }
    
    public double getOriginLon() {
        return this.origin.getLongitude();
    }
    
    public void setOriginLon(final double lon) {
        this.origin.setLongitude(lon);
        this.lon0 = Math.toRadians(lon);
        this.precalculate();
    }
    
    public double getHeight() {
        return this.H;
    }
    
    public void setHeight(final double height) {
        this.H = height;
        this.precalculate();
    }
    
    public double getOriginLat() {
        return this.origin.getLatitude();
    }
    
    public void setOriginLat(final double lat) {
        this.origin.setLatitude(lat);
        this.lat0 = Math.toRadians(lat);
        this.precalculate();
    }
    
    @Override
    public String getProjectionTypeLabel() {
        return "VerticalPerspectiveView";
    }
    
    @Override
    public String paramsToString() {
        return " origin " + this.origin.toString();
    }
    
    @Override
    public boolean crossSeam(final ProjectionPoint pt1, final ProjectionPoint pt2) {
        return ProjectionPointImpl.isInfinite(pt1) || ProjectionPointImpl.isInfinite(pt2) || (pt1.getX() * pt2.getX() < 0.0 && Math.abs(pt1.getX() - pt2.getX()) > 5000.0);
    }
    
    @Override
    public ProjectionPoint latLonToProj(final LatLonPoint latLon, final ProjectionPointImpl result) {
        double fromLat = latLon.getLatitude();
        final double fromLon = latLon.getLongitude();
        fromLat = Math.toRadians(fromLat);
        final double lonDiff = Math.toRadians(LatLonPointImpl.lonNormal(fromLon - this.lon0Degrees));
        final double cosc = this.sinLat0 * Math.sin(fromLat) + this.cosLat0 * Math.cos(fromLat) * Math.cos(lonDiff);
        final double ksp = (this.P - 1.0) / (this.P - cosc);
        double toX;
        double toY;
        if (cosc < 1.0 / this.P) {
            toX = Double.POSITIVE_INFINITY;
            toY = Double.POSITIVE_INFINITY;
        }
        else {
            toX = this.false_east + this.R * ksp * Math.cos(fromLat) * Math.sin(lonDiff);
            toY = this.false_north + this.R * ksp * (this.cosLat0 * Math.sin(fromLat) - this.sinLat0 * Math.cos(fromLat) * Math.cos(lonDiff));
        }
        result.setLocation(toX, toY);
        return result;
    }
    
    @Override
    public LatLonPoint projToLatLon(final ProjectionPoint world, final LatLonPointImpl result) {
        double fromX = world.getX();
        double fromY = world.getY();
        fromX -= this.false_east;
        fromY -= this.false_north;
        final double rho = Math.sqrt(fromX * fromX + fromY * fromY);
        final double r = rho / this.R;
        final double con = this.P - 1.0;
        final double com = this.P + 1.0;
        final double c = Math.asin((this.P - Math.sqrt(1.0 - r * r * com / con)) / (con / r + r / con));
        double toLon = this.lon0;
        double temp = 0.0;
        double toLat;
        if (Math.abs(rho) > 1.0E-6) {
            toLat = Math.asin(Math.cos(c) * this.sinLat0 + fromY * Math.sin(c) * this.cosLat0 / rho);
            if (Math.abs(this.lat0 - 0.7853981633974483) > 1.0E-6) {
                temp = rho * this.cosLat0 * Math.cos(c) - fromY * this.sinLat0 * Math.sin(c);
                toLon = this.lon0 + Math.atan(fromX * Math.sin(c) / temp);
            }
            else if (this.lat0 == 0.7853981633974483) {
                toLon = this.lon0 + Math.atan(fromX / -fromY);
                temp = -fromY;
            }
            else {
                toLon = this.lon0 + Math.atan(fromX / fromY);
                temp = fromY;
            }
        }
        else {
            toLat = this.lat0;
        }
        toLat = Math.toDegrees(toLat);
        toLon = Math.toDegrees(toLon);
        if (temp < 0.0) {
            toLon += 180.0;
        }
        toLon = LatLonPointImpl.lonNormal(toLon);
        result.setLatitude(toLat);
        result.setLongitude(toLon);
        return result;
    }
    
    @Override
    public float[][] latLonToProj(final float[][] from, final float[][] to, final int latIndex, final int lonIndex) {
        final int cnt = from[0].length;
        final float[] fromLatA = from[latIndex];
        final float[] fromLonA = from[lonIndex];
        final float[] resultXA = to[0];
        final float[] resultYA = to[1];
        for (int i = 0; i < cnt; ++i) {
            double fromLat = fromLatA[i];
            final double fromLon = fromLonA[i];
            fromLat = Math.toRadians(fromLat);
            final double lonDiff = Math.toRadians(LatLonPointImpl.lonNormal(fromLon - this.lon0Degrees));
            final double cosc = this.sinLat0 * Math.sin(fromLat) + this.cosLat0 * Math.cos(fromLat) * Math.cos(lonDiff);
            final double ksp = (this.P - 1.0) / (this.P - cosc);
            double toX;
            double toY;
            if (cosc < 1.0 / this.P) {
                toX = Double.POSITIVE_INFINITY;
                toY = Double.POSITIVE_INFINITY;
            }
            else {
                toX = this.false_east + this.R * ksp * Math.cos(fromLat) * Math.sin(lonDiff);
                toY = this.false_north + this.R * ksp * (this.cosLat0 * Math.sin(fromLat) - this.sinLat0 * Math.cos(fromLat) * Math.cos(lonDiff));
            }
            resultXA[i] = (float)toX;
            resultYA[i] = (float)toY;
        }
        return to;
    }
    
    @Override
    public float[][] projToLatLon(final float[][] from, final float[][] to) {
        final int cnt = from[0].length;
        final float[] fromXA = from[0];
        final float[] fromYA = from[1];
        final float[] toLatA = to[0];
        final float[] toLonA = to[1];
        for (int i = 0; i < cnt; ++i) {
            double fromX = fromXA[i];
            double fromY = fromYA[i];
            fromX -= this.false_east;
            fromY -= this.false_north;
            final double rho = Math.sqrt(fromX * fromX + fromY * fromY);
            final double r = rho / this.R;
            final double con = this.P - 1.0;
            final double com = this.P + 1.0;
            final double c = Math.asin((this.P - Math.sqrt(1.0 - r * r * com / con)) / (con / r + r / con));
            double toLon = this.lon0;
            double temp = 0.0;
            double toLat;
            if (Math.abs(rho) > 1.0E-6) {
                toLat = Math.asin(Math.cos(c) * this.sinLat0 + fromY * Math.sin(c) * this.cosLat0 / rho);
                if (Math.abs(this.lat0 - 0.7853981633974483) > 1.0E-6) {
                    temp = rho * this.cosLat0 * Math.cos(c) - fromY * this.sinLat0 * Math.sin(c);
                    toLon = this.lon0 + Math.atan(fromX * Math.sin(c) / temp);
                }
                else if (this.lat0 == 0.7853981633974483) {
                    toLon = this.lon0 + Math.atan(fromX / -fromY);
                    temp = -fromY;
                }
                else {
                    toLon = this.lon0 + Math.atan(fromX / fromY);
                    temp = fromY;
                }
            }
            else {
                toLat = this.lat0;
            }
            toLat = Math.toDegrees(toLat);
            toLon = Math.toDegrees(toLon);
            if (temp < 0.0) {
                toLon += 180.0;
            }
            toLon = LatLonPointImpl.lonNormal(toLon);
            toLatA[i] = (float)toLat;
            toLonA[i] = (float)toLon;
        }
        return to;
    }
    
    @Override
    public double[][] latLonToProj(final double[][] from, final double[][] to, final int latIndex, final int lonIndex) {
        final int cnt = from[0].length;
        final double[] fromLatA = from[latIndex];
        final double[] fromLonA = from[lonIndex];
        final double[] resultXA = to[0];
        final double[] resultYA = to[1];
        for (int i = 0; i < cnt; ++i) {
            double fromLat = fromLatA[i];
            final double fromLon = fromLonA[i];
            fromLat = Math.toRadians(fromLat);
            final double lonDiff = Math.toRadians(LatLonPointImpl.lonNormal(fromLon - this.lon0Degrees));
            final double cosc = this.sinLat0 * Math.sin(fromLat) + this.cosLat0 * Math.cos(fromLat) * Math.cos(lonDiff);
            final double ksp = (this.P - 1.0) / (this.P - cosc);
            double toX;
            double toY;
            if (cosc < 1.0 / this.P) {
                toX = Double.POSITIVE_INFINITY;
                toY = Double.POSITIVE_INFINITY;
            }
            else {
                toX = this.false_east + this.R * ksp * Math.cos(fromLat) * Math.sin(lonDiff);
                toY = this.false_north + this.R * ksp * (this.cosLat0 * Math.sin(fromLat) - this.sinLat0 * Math.cos(fromLat) * Math.cos(lonDiff));
            }
            resultXA[i] = toX;
            resultYA[i] = toY;
        }
        return to;
    }
    
    @Override
    public double[][] projToLatLon(final double[][] from, final double[][] to) {
        final int cnt = from[0].length;
        final double[] fromXA = from[0];
        final double[] fromYA = from[1];
        final double[] toLatA = to[0];
        final double[] toLonA = to[1];
        for (int i = 0; i < cnt; ++i) {
            double fromX = fromXA[i];
            double fromY = fromYA[i];
            fromX -= this.false_east;
            fromY -= this.false_north;
            final double rho = Math.sqrt(fromX * fromX + fromY * fromY);
            final double r = rho / this.R;
            final double con = this.P - 1.0;
            final double com = this.P + 1.0;
            final double c = Math.asin((this.P - Math.sqrt(1.0 - r * r * com / con)) / (con / r + r / con));
            double toLon = this.lon0;
            double temp = 0.0;
            double toLat;
            if (Math.abs(rho) > 1.0E-6) {
                toLat = Math.asin(Math.cos(c) * this.sinLat0 + fromY * Math.sin(c) * this.cosLat0 / rho);
                if (Math.abs(this.lat0 - 0.7853981633974483) > 1.0E-6) {
                    temp = rho * this.cosLat0 * Math.cos(c) - fromY * this.sinLat0 * Math.sin(c);
                    toLon = this.lon0 + Math.atan(fromX * Math.sin(c) / temp);
                }
                else if (this.lat0 == 0.7853981633974483) {
                    toLon = this.lon0 + Math.atan(fromX / -fromY);
                    temp = -fromY;
                }
                else {
                    toLon = this.lon0 + Math.atan(fromX / fromY);
                    temp = fromY;
                }
            }
            else {
                toLat = this.lat0;
            }
            toLat = Math.toDegrees(toLat);
            toLon = Math.toDegrees(toLon);
            if (temp < 0.0) {
                toLon += 180.0;
            }
            toLon = LatLonPointImpl.lonNormal(toLon);
            toLatA[i] = toLat;
            toLonA[i] = toLon;
        }
        return to;
    }
    
    @Override
    public ProjectionRect latLonToProjBB(final LatLonRect rect) {
        final ProjectionPoint llpt = this.latLonToProj(rect.getLowerLeftPoint(), new ProjectionPointImpl());
        final ProjectionPoint urpt = this.latLonToProj(rect.getUpperRightPoint(), new ProjectionPointImpl());
        final ProjectionPoint lrpt = this.latLonToProj(rect.getLowerRightPoint(), new ProjectionPointImpl());
        final ProjectionPoint ulpt = this.latLonToProj(rect.getUpperLeftPoint(), new ProjectionPointImpl());
        final List<ProjectionPoint> goodPts = new ArrayList<ProjectionPoint>(4);
        int countBad = 0;
        if (!this.addGoodPts(goodPts, llpt)) {
            ++countBad;
        }
        if (!this.addGoodPts(goodPts, urpt)) {
            ++countBad;
        }
        if (!this.addGoodPts(goodPts, lrpt)) {
            ++countBad;
        }
        if (!this.addGoodPts(goodPts, ulpt)) {
            ++countBad;
        }
        if (countBad == 2) {
            if (!ProjectionPointImpl.isInfinite(llpt) && !ProjectionPointImpl.isInfinite(lrpt)) {
                this.addGoodPts(goodPts, new ProjectionPointImpl(0.0, this.maxR));
            }
            else if (!ProjectionPointImpl.isInfinite(ulpt) && !ProjectionPointImpl.isInfinite(llpt)) {
                this.addGoodPts(goodPts, new ProjectionPointImpl(this.maxR, 0.0));
            }
            else if (!ProjectionPointImpl.isInfinite(ulpt) && !ProjectionPointImpl.isInfinite(urpt)) {
                this.addGoodPts(goodPts, new ProjectionPointImpl(0.0, -this.maxR));
            }
            else {
                if (ProjectionPointImpl.isInfinite(urpt) || ProjectionPointImpl.isInfinite(lrpt)) {
                    throw new IllegalStateException();
                }
                this.addGoodPts(goodPts, new ProjectionPointImpl(-this.maxR, 0.0));
            }
        }
        else if (countBad == 3) {
            if (!ProjectionPointImpl.isInfinite(llpt)) {
                final double xcoord = llpt.getX();
                this.addGoodPts(goodPts, new ProjectionPointImpl(xcoord, this.getLimitCoord(xcoord)));
                final double ycoord = llpt.getY();
                this.addGoodPts(goodPts, new ProjectionPointImpl(this.getLimitCoord(ycoord), ycoord));
            }
            else if (!ProjectionPointImpl.isInfinite(urpt)) {
                final double xcoord = urpt.getX();
                this.addGoodPts(goodPts, new ProjectionPointImpl(xcoord, -this.getLimitCoord(xcoord)));
                final double ycoord = urpt.getY();
                this.addGoodPts(goodPts, new ProjectionPointImpl(-this.getLimitCoord(ycoord), ycoord));
            }
            else if (!ProjectionPointImpl.isInfinite(ulpt)) {
                final double xcoord = ulpt.getX();
                this.addGoodPts(goodPts, new ProjectionPointImpl(xcoord, -this.getLimitCoord(xcoord)));
                final double ycoord = ulpt.getY();
                this.addGoodPts(goodPts, new ProjectionPointImpl(this.getLimitCoord(ycoord), ycoord));
            }
            else {
                if (ProjectionPointImpl.isInfinite(lrpt)) {
                    throw new IllegalStateException();
                }
                final double xcoord = lrpt.getX();
                this.addGoodPts(goodPts, new ProjectionPointImpl(xcoord, this.getLimitCoord(xcoord)));
                final double ycoord = lrpt.getY();
                this.addGoodPts(goodPts, new ProjectionPointImpl(-this.getLimitCoord(ycoord), ycoord));
            }
        }
        return this.makeRect(goodPts);
    }
    
    private boolean addGoodPts(final List<ProjectionPoint> goodPts, final ProjectionPoint pt) {
        if (!ProjectionPointImpl.isInfinite(pt)) {
            goodPts.add(pt);
            return true;
        }
        return false;
    }
    
    private double getLimitCoord(final double coord) {
        return Math.sqrt(this.maxR2 - coord * coord);
    }
    
    private ProjectionRect makeRect(final List<ProjectionPoint> goodPts) {
        double minx = Double.MAX_VALUE;
        double miny = Double.MAX_VALUE;
        double maxx = -1.7976931348623157E308;
        double maxy = -1.7976931348623157E308;
        for (final ProjectionPoint pp : goodPts) {
            minx = Math.min(minx, pp.getX());
            maxx = Math.max(maxx, pp.getX());
            miny = Math.min(miny, pp.getY());
            maxy = Math.max(maxy, pp.getY());
        }
        return new ProjectionRect(minx, miny, maxx, maxy);
    }
    
    private static void test(final double lat, final double lon) {
        final double radius = 6371.0;
        final VerticalPerspectiveView a = new VerticalPerspectiveView(0.0, 0.0, radius, 5.62 * radius);
        final ProjectionPointImpl p = a.latLonToProj(lat, lon);
        System.out.println("-----\nproj point = " + p);
        System.out.println("x/r = " + p.getX() / radius);
        System.out.println("y/r = " + p.getY() / radius);
        final LatLonPoint ll = a.projToLatLon(p);
        System.out.println(" lat = " + ll.getLatitude() + " should be= " + lat);
        System.out.println(" lon = " + ll.getLongitude() + " should be= " + lon);
    }
    
    public static void main2(final String[] args) {
        test(40.0, 0.0);
        test(40.0, 40.0);
        test(0.0, 40.0);
    }
    
    public static void main(final String[] args) {
        final double radius = 6371.0;
        final double height = 35747.0;
        final VerticalPerspectiveView a = new VerticalPerspectiveView(0.0, 0.0, radius, height);
        final double limit = 0.99 * Math.sqrt((a.P - 1.0) / (a.P + 1.0));
        System.out.println(" limit = " + limit);
        System.out.println(" limit*90 = " + limit * 90.0);
        final LatLonRect rect = new LatLonRect(new LatLonPointImpl(-45.0, -45.0), -45.0, -45.0);
        final ProjectionRect r = a.latLonToProjBB(rect);
        System.out.println(" ProjectionRect result = " + r);
    }
}
