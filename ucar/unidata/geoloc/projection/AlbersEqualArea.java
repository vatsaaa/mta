// 
// Decompiled by Procyon v0.5.36
// 

package ucar.unidata.geoloc.projection;

import ucar.unidata.geoloc.LatLonPoint;
import ucar.unidata.geoloc.ProjectionPointImpl;
import ucar.unidata.geoloc.ProjectionPoint;
import ucar.unidata.util.Format;
import ucar.unidata.util.Parameter;
import ucar.unidata.geoloc.Earth;
import ucar.unidata.geoloc.LatLonPointImpl;
import ucar.unidata.geoloc.ProjectionImpl;

public class AlbersEqualArea extends ProjectionImpl
{
    private double n;
    private double C;
    private double rho0;
    private double lat0;
    private double lon0;
    private double par1;
    private double par2;
    private double lon0Degrees;
    private LatLonPointImpl origin;
    private double falseEasting;
    private double falseNorthing;
    private double earth_radius;
    
    @Override
    public ProjectionImpl constructCopy() {
        return new AlbersEqualArea(this.getOriginLat(), this.getOriginLon(), this.getParallelOne(), this.getParallelTwo(), this.getFalseEasting(), this.getFalseNorthing(), this.getEarthRadius());
    }
    
    public AlbersEqualArea() {
        this(23.0, -96.0, 29.5, 45.5);
    }
    
    public AlbersEqualArea(final double lat0, final double lon0, final double par1, final double par2) {
        this(lat0, lon0, par1, par2, 0.0, 0.0, Earth.getRadius() * 0.001);
    }
    
    public AlbersEqualArea(final double lat0, final double lon0, final double par1, final double par2, final double falseEasting, final double falseNorthing) {
        this(lat0, lon0, par1, par2, falseEasting, falseNorthing, Earth.getRadius() * 0.001);
    }
    
    public AlbersEqualArea(final double lat0, final double lon0, final double par1, final double par2, final double falseEasting, final double falseNorthing, final double earth_radius) {
        this.name = "AlbersEqualArea";
        this.lat0 = Math.toRadians(lat0);
        this.lon0Degrees = lon0;
        this.lon0 = Math.toRadians(lon0);
        this.par1 = par1;
        this.par2 = par2;
        this.falseEasting = falseEasting;
        this.falseNorthing = falseNorthing;
        this.earth_radius = earth_radius;
        this.origin = new LatLonPointImpl(lat0, lon0);
        this.precalculate();
        this.addParameter("grid_mapping_name", "albers_conical_equal_area");
        this.addParameter("latitude_of_projection_origin", lat0);
        this.addParameter("longitude_of_central_meridian", lon0);
        if (par2 == par1) {
            this.addParameter("standard_parallel", par1);
        }
        else {
            final double[] data = { par1, par2 };
            this.addParameter(new Parameter("standard_parallel", data));
        }
        if (falseEasting != 0.0 || falseNorthing != 0.0) {
            this.addParameter("false_easting", falseEasting);
            this.addParameter("false_northing", falseNorthing);
            this.addParameter("units", "km");
        }
        this.addParameter("earth_radius", earth_radius);
    }
    
    private void precalculate() {
        final double par1r = Math.toRadians(this.par1);
        final double par2r = Math.toRadians(this.par2);
        if (Math.abs(this.par2 - this.par1) < 1.0E-6) {
            this.n = Math.sin(par1r);
        }
        else {
            this.n = (Math.sin(par1r) + Math.sin(par2r)) / 2.0;
        }
        final double c2 = Math.pow(Math.cos(par1r), 2.0);
        this.C = c2 + 2.0 * this.n * Math.sin(par1r);
        this.rho0 = this.computeRho(this.lat0);
    }
    
    private double computeRho(final double lat) {
        return this.earth_radius * Math.sqrt(this.C - 2.0 * this.n * Math.sin(lat)) / this.n;
    }
    
    private double computeTheta(final double lon) {
        final double dlon = LatLonPointImpl.lonNormal(Math.toDegrees(lon) - this.lon0Degrees);
        return this.n * Math.toRadians(dlon);
    }
    
    @Override
    public Object clone() {
        final AlbersEqualArea cl = (AlbersEqualArea)super.clone();
        cl.origin = new LatLonPointImpl(this.getOriginLat(), this.getOriginLon());
        return cl;
    }
    
    @Override
    public boolean equals(final Object proj) {
        if (!(proj instanceof AlbersEqualArea)) {
            return false;
        }
        final AlbersEqualArea oo = (AlbersEqualArea)proj;
        return this.getParallelOne() == oo.getParallelOne() && this.getParallelTwo() == oo.getParallelTwo() && this.getOriginLat() == oo.getOriginLat() && this.getOriginLon() == oo.getOriginLon() && this.defaultMapArea.equals(oo.defaultMapArea);
    }
    
    public double getParallelTwo() {
        return this.par2;
    }
    
    public void setParallelTwo(final double par) {
        this.par2 = par;
        this.precalculate();
    }
    
    public double getParallelOne() {
        return this.par1;
    }
    
    public void setParallelOne(final double par) {
        this.par1 = par;
        this.precalculate();
    }
    
    public double getOriginLon() {
        return this.origin.getLongitude();
    }
    
    public void setOriginLon(final double lon) {
        this.origin.setLongitude(lon);
        this.lon0 = Math.toRadians(lon);
        this.precalculate();
    }
    
    public double getOriginLat() {
        return this.origin.getLatitude();
    }
    
    public void setOriginLat(final double lat) {
        this.origin.setLatitude(lat);
        this.lat0 = Math.toRadians(lat);
        this.precalculate();
    }
    
    public double getFalseEasting() {
        return this.falseEasting;
    }
    
    public void setFalseEasting(final double falseEasting) {
        this.falseEasting = falseEasting;
    }
    
    public double getFalseNorthing() {
        return this.falseNorthing;
    }
    
    public void setFalseNorthing(final double falseNorthing) {
        this.falseNorthing = falseNorthing;
    }
    
    public double getEarthRadius() {
        return this.earth_radius;
    }
    
    @Override
    public String getProjectionTypeLabel() {
        return "Albers Equal Area";
    }
    
    @Override
    public String paramsToString() {
        return " origin " + this.origin.toString() + " parallels: " + Format.d(this.par1, 3) + " " + Format.d(this.par2, 3);
    }
    
    public double getScale(double lat) {
        lat = Math.toRadians(lat);
        final double n = Math.cos(lat);
        final double d = Math.sqrt(this.C - 2.0 * n * Math.sin(lat));
        return n / d;
    }
    
    @Override
    public boolean crossSeam(final ProjectionPoint pt1, final ProjectionPoint pt2) {
        return ProjectionPointImpl.isInfinite(pt1) || ProjectionPointImpl.isInfinite(pt2) || (pt1.getX() * pt2.getX() < 0.0 && Math.abs(pt1.getX() - pt2.getX()) > 5000.0);
    }
    
    @Override
    public ProjectionPoint latLonToProj(final LatLonPoint latLon, final ProjectionPointImpl result) {
        double fromLat = latLon.getLatitude();
        double fromLon = latLon.getLongitude();
        fromLat = Math.toRadians(fromLat);
        fromLon = Math.toRadians(fromLon);
        final double rho = this.computeRho(fromLat);
        final double theta = this.computeTheta(fromLon);
        final double toX = rho * Math.sin(theta) + this.falseEasting;
        final double toY = this.rho0 - rho * Math.cos(theta) + this.falseNorthing;
        result.setLocation(toX, toY);
        return result;
    }
    
    @Override
    public LatLonPoint projToLatLon(final ProjectionPoint world, final LatLonPointImpl result) {
        double fromX = world.getX() - this.falseEasting;
        double fromY = world.getY() - this.falseNorthing;
        double rrho0 = this.rho0;
        if (this.n < 0.0) {
            rrho0 *= -1.0;
            fromX *= -1.0;
            fromY *= -1.0;
        }
        final double yd = rrho0 - fromY;
        double rho = Math.sqrt(fromX * fromX + yd * yd);
        final double theta = Math.atan2(fromX, yd);
        if (this.n < 0.0) {
            rho *= -1.0;
        }
        final double toLat = Math.toDegrees(Math.asin((this.C - Math.pow(rho * this.n / this.earth_radius, 2.0)) / (2.0 * this.n)));
        final double toLon = Math.toDegrees(theta / this.n + this.lon0);
        result.setLatitude(toLat);
        result.setLongitude(toLon);
        return result;
    }
    
    @Override
    public float[][] latLonToProj(final float[][] from, final float[][] to, final int latIndex, final int lonIndex) {
        final int cnt = from[0].length;
        final float[] fromLatA = from[latIndex];
        final float[] fromLonA = from[lonIndex];
        final float[] resultXA = to[0];
        final float[] resultYA = to[1];
        for (int i = 0; i < cnt; ++i) {
            double fromLat = fromLatA[i];
            double fromLon = fromLonA[i];
            fromLat = Math.toRadians(fromLat);
            fromLon = Math.toRadians(fromLon);
            final double rho = this.computeRho(fromLat);
            final double theta = this.computeTheta(fromLon);
            final double toX = rho * Math.sin(theta);
            final double toY = this.rho0 - rho * Math.cos(theta);
            resultXA[i] = (float)(toX + this.falseEasting);
            resultYA[i] = (float)(toY + this.falseNorthing);
        }
        return to;
    }
    
    @Override
    public float[][] projToLatLon(final float[][] from, final float[][] to) {
        final int cnt = from[0].length;
        final float[] fromXA = from[0];
        final float[] fromYA = from[1];
        final float[] toLatA = to[0];
        final float[] toLonA = to[1];
        double rrho0 = this.rho0;
        for (int i = 0; i < cnt; ++i) {
            double fromX = fromXA[i] - this.falseEasting;
            double fromY = fromYA[i] - this.falseNorthing;
            if (this.n < 0.0) {
                rrho0 *= -1.0;
                fromX *= -1.0;
                fromY *= -1.0;
            }
            final double yd = rrho0 - fromY;
            double rho = Math.sqrt(fromX * fromX + yd * yd);
            final double theta = Math.atan2(fromX, yd);
            if (this.n < 0.0) {
                rho *= -1.0;
            }
            final double toLat = Math.toDegrees(Math.asin((this.C - Math.pow(rho * this.n / this.earth_radius, 2.0)) / (2.0 * this.n)));
            final double toLon = Math.toDegrees(theta / this.n + this.lon0);
            toLatA[i] = (float)toLat;
            toLonA[i] = (float)toLon;
        }
        return to;
    }
    
    @Override
    public double[][] latLonToProj(final double[][] from, final double[][] to, final int latIndex, final int lonIndex) {
        final int cnt = from[0].length;
        final double[] fromLatA = from[latIndex];
        final double[] fromLonA = from[lonIndex];
        final double[] resultXA = to[0];
        final double[] resultYA = to[1];
        for (int i = 0; i < cnt; ++i) {
            double fromLat = fromLatA[i];
            double fromLon = fromLonA[i];
            fromLat = Math.toRadians(fromLat);
            fromLon = Math.toRadians(fromLon);
            final double rho = this.computeRho(fromLat);
            final double theta = this.computeTheta(fromLon);
            final double toX = rho * Math.sin(theta);
            final double toY = this.rho0 - rho * Math.cos(theta);
            resultXA[i] = toX + this.falseEasting;
            resultYA[i] = toY + this.falseNorthing;
        }
        return to;
    }
    
    @Override
    public double[][] projToLatLon(final double[][] from, final double[][] to) {
        final int cnt = from[0].length;
        final double[] fromXA = from[0];
        final double[] fromYA = from[1];
        final double[] toLatA = to[0];
        final double[] toLonA = to[1];
        double rrho0 = this.rho0;
        for (int i = 0; i < cnt; ++i) {
            double fromX = fromXA[i] - this.falseEasting;
            double fromY = fromYA[i] - this.falseNorthing;
            if (this.n < 0.0) {
                rrho0 *= -1.0;
                fromX *= -1.0;
                fromY *= -1.0;
            }
            final double yd = rrho0 - fromY;
            double rho = Math.sqrt(fromX * fromX + yd * yd);
            final double theta = Math.atan2(fromX, yd);
            if (this.n < 0.0) {
                rho *= -1.0;
            }
            final double toLat = Math.toDegrees(Math.asin((this.C - Math.pow(rho * this.n / this.earth_radius, 2.0)) / (2.0 * this.n)));
            final double toLon = Math.toDegrees(theta / this.n + this.lon0);
            toLatA[i] = toLat;
            toLonA[i] = toLon;
        }
        return to;
    }
    
    public static void main(final String[] args) {
        final AlbersEqualArea a = new AlbersEqualArea(23.0, -96.0, 29.5, 45.5);
        System.out.printf("name=%s%n", a.getName());
        System.out.println("ll = 35N 75W");
        final ProjectionPointImpl p = a.latLonToProj(35.0, -75.0);
        System.out.println("proj point = " + p);
        final LatLonPoint ll = a.projToLatLon(p);
        System.out.println("ll = " + ll);
    }
}
