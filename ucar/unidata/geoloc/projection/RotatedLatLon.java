// 
// Decompiled by Procyon v0.5.36
// 

package ucar.unidata.geoloc.projection;

import java.util.Arrays;
import java.io.PrintStream;
import ucar.unidata.geoloc.LatLonPointImpl;
import ucar.unidata.geoloc.ProjectionPoint;
import ucar.unidata.geoloc.ProjectionPointImpl;
import ucar.unidata.geoloc.LatLonPoint;
import ucar.unidata.geoloc.ProjectionImpl;

public class RotatedLatLon extends ProjectionImpl
{
    private static final double RAD_PER_DEG = 0.017453292519943295;
    private static final double DEG_PER_RAD = 57.29577951308232;
    private static boolean show;
    private double lonpole;
    private double latpole;
    private double polerotate;
    private double cosDlat;
    private double sinDlat;
    
    public RotatedLatLon() {
        this(0.0, 0.0, 0.0);
    }
    
    public RotatedLatLon(final double southPoleLat, final double southPoleLon, final double southPoleAngle) {
        this.latpole = southPoleLat;
        this.lonpole = southPoleLon;
        this.polerotate = southPoleAngle;
        final double dlat_rad = (this.latpole + 90.0) * 0.017453292519943295;
        this.sinDlat = Math.sin(dlat_rad);
        this.cosDlat = Math.cos(dlat_rad);
        this.addParameter("grid_mapping_name", "rotated_latlon_grib");
        this.addParameter("grid_south_pole_latitude", southPoleLat);
        this.addParameter("grid_south_pole_longitude", southPoleLon);
        this.addParameter("grid_south_pole_angle", southPoleAngle);
    }
    
    @Override
    public ProjectionImpl constructCopy() {
        return new RotatedLatLon(this.latpole, this.lonpole, this.polerotate);
    }
    
    @Override
    public String paramsToString() {
        return " southPoleLat =" + this.latpole + " southPoleLon =" + this.lonpole + " southPoleAngle =" + this.polerotate;
    }
    
    @Override
    public ProjectionPoint latLonToProj(final LatLonPoint latlon, ProjectionPointImpl destPoint) {
        final double[] lonlat = { latlon.getLongitude(), latlon.getLatitude() };
        final double[] rlonlat = this.rotate(lonlat, this.lonpole, this.polerotate, this.sinDlat);
        if (destPoint == null) {
            destPoint = new ProjectionPointImpl(rlonlat[0], rlonlat[1]);
        }
        else {
            destPoint.setLocation(rlonlat[0], rlonlat[1]);
        }
        if (RotatedLatLon.show) {
            System.out.println("LatLon= " + latlon + " proj= " + destPoint);
        }
        return destPoint;
    }
    
    @Override
    public LatLonPoint projToLatLon(final ProjectionPoint ppt, LatLonPointImpl destPoint) {
        final double[] lonlat = { ppt.getX(), ppt.getY() };
        final double[] rlonlat = this.rotate(lonlat, -this.polerotate, -this.lonpole, -this.sinDlat);
        if (destPoint == null) {
            destPoint = new LatLonPointImpl(rlonlat[1], rlonlat[0]);
        }
        else {
            destPoint.set(rlonlat[1], rlonlat[0]);
        }
        if (RotatedLatLon.show) {
            System.out.println("Proj= " + ppt + " latlon= " + destPoint);
        }
        return destPoint;
    }
    
    private double[] rotate(final double[] lonlat, final double rot1, final double rot2, final double s) {
        final double e = 0.017453292519943295 * (lonlat[0] - rot1);
        final double n = 0.017453292519943295 * lonlat[1];
        final double cn = Math.cos(n);
        final double x = cn * Math.cos(e);
        final double y = cn * Math.sin(e);
        final double z = Math.sin(n);
        final double x2 = this.cosDlat * x + s * z;
        final double z2 = -s * x + this.cosDlat * z;
        final double R = Math.sqrt(x2 * x2 + y * y);
        final double e2 = Math.atan2(y, x2);
        final double n2 = Math.atan2(z2, R);
        final double rlon = 57.29577951308232 * e2 - rot2;
        final double rlat = 57.29577951308232 * n2;
        return new double[] { rlon, rlat };
    }
    
    @Override
    public boolean crossSeam(final ProjectionPoint pt1, final ProjectionPoint pt2) {
        return false;
    }
    
    @Override
    public boolean equals(final Object proj) {
        if (!(proj instanceof RotatedLatLon)) {
            return false;
        }
        final RotatedLatLon oo = (RotatedLatLon)proj;
        return this.lonpole == oo.lonpole && this.latpole == oo.latpole && this.polerotate == oo.polerotate;
    }
    
    private static void test() {
        final Test tst0 = new Test(0.0, -25.0, 0.0);
        final double[] pos = tst0.proj(0.0, -25.0, true);
        final double[] pos2 = tst0.proj(pos[0], pos[1], false);
        final double[] pos3 = tst0.proj(-46.5, -36.5, false);
        final double[] pos4 = tst0.proj(46.9, 38.9, false);
        Test t = new Test(0.0, 90.0, 0.0);
        t.test(0.0f, 0.0f);
        t.test(90.0f, 0.0f);
        t.test(0.0f, 30.0f);
        t = new Test(0.0, 0.0, 0.0);
        t.test(0.0f, 0.0f);
        t.test(90.0f, 0.0f);
        t.test(0.0f, 30.0f);
        t = new Test(10.0, 50.0, 25.0);
        t.test(0.0f, 0.0f);
        t.test(90.0f, 0.0f);
        t.test(0.0f, 30.0f);
        t = null;
        final RotatedLatLon rll = new RotatedLatLon(-50.0, 10.0, 20.0);
        final long t2 = System.currentTimeMillis();
        long dt = 0L;
        final double[] p = { 12.0, 60.0 };
        int i = 0;
        while (dt < 1000L) {
            rll.rotate(p, rll.lonpole, rll.polerotate, rll.sinDlat);
            rll.rotate(p, rll.lonpole, rll.polerotate, rll.sinDlat);
            rll.rotate(p, rll.lonpole, rll.polerotate, rll.sinDlat);
            rll.rotate(p, rll.lonpole, rll.polerotate, rll.sinDlat);
            rll.rotate(p, rll.lonpole, rll.polerotate, rll.sinDlat);
            rll.rotate(p, rll.lonpole, rll.polerotate, rll.sinDlat);
            rll.rotate(p, rll.lonpole, rll.polerotate, rll.sinDlat);
            rll.rotate(p, rll.lonpole, rll.polerotate, rll.sinDlat);
            rll.rotate(p, rll.lonpole, rll.polerotate, rll.sinDlat);
            rll.rotate(p, rll.lonpole, rll.polerotate, rll.sinDlat);
            ++i;
            dt = System.currentTimeMillis() - t2;
        }
        System.out.println("fwd/sec: " + i * 10);
    }
    
    public static void main(final String[] args) {
        test();
    }
    
    static {
        RotatedLatLon.show = false;
    }
    
    private static class Test
    {
        RotatedLatLon rll;
        static PrintStream ps;
        static final double err = 1.0E-4;
        
        public Test(final double lo, final double la, final double rot) {
            this.rll = new RotatedLatLon(la, lo, rot);
            Test.ps.println("lonsp:" + this.rll.lonpole + ", latsp:" + this.rll.latpole + ", rotsp:" + this.rll.polerotate);
        }
        
        void pr(final double[] pos, final double[] pos2, final double[] pos3) {
            Test.ps.println(" " + pos[0] + "   " + pos[1]);
            Test.ps.println("    fwd: " + pos2[0] + "   " + pos2[1]);
            Test.ps.println("    inv: " + pos3[0] + "   " + pos3[1]);
        }
        
        private double[] test(final float lon, final float lat) {
            final double[] p = { lon, lat };
            final double[] p2 = this.rll.rotate(p, this.rll.lonpole, this.rll.polerotate, this.rll.sinDlat);
            final double[] p3 = this.rll.rotate(p2, -this.rll.polerotate, -this.rll.lonpole, -this.rll.sinDlat);
            assert Math.abs(p[0] - p3[0]) < 1.0E-4;
            assert Math.abs(p[1] - p3[1]) < 1.0E-4;
            this.pr(p, p2, p3);
            return p2;
        }
        
        double[] proj(final double lon, final double lat, final boolean fwd) {
            final double[] pos = { lon, lat };
            final double[] pos2 = fwd ? this.rll.rotate(pos, this.rll.lonpole, this.rll.polerotate, this.rll.sinDlat) : this.rll.rotate(pos, -this.rll.polerotate, -this.rll.lonpole, -this.rll.sinDlat);
            Test.ps.println((fwd ? " fwd" : " inv") + " [" + lon + ", " + lat + "] -> " + Arrays.toString(pos2));
            return pos2;
        }
        
        static {
            Test.ps = System.out;
        }
    }
}
