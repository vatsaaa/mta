// 
// Decompiled by Procyon v0.5.36
// 

package ucar.unidata.geoloc.projection;

import ucar.unidata.geoloc.LatLonPointImpl;
import ucar.unidata.geoloc.ProjectionPoint;
import ucar.unidata.geoloc.LatLonPoint;
import java.awt.geom.Point2D;
import ucar.unidata.geoloc.ProjectionPointImpl;
import ucar.unidata.geoloc.ProjectionImpl;

public class RotatedPole extends ProjectionImpl
{
    private static final double RAD_PER_DEG = 0.017453292519943295;
    private static final double DEG_PER_RAD = 57.29577951308232;
    private static boolean show;
    private ProjectionPointImpl northPole;
    private double[][] rotY;
    private double[][] rotZ;
    
    public RotatedPole() {
        this(0.0, 0.0);
    }
    
    public RotatedPole(final double northPoleLat, final double northPoleLon) {
        this.rotY = new double[3][3];
        this.rotZ = new double[3][3];
        this.northPole = new ProjectionPointImpl(northPoleLon, northPoleLat);
        this.buildRotationMatrices();
        this.addParameter("grid_mapping_name", "rotated_latitude_longitude");
        this.addParameter("grid_north_pole_latitude", northPoleLat);
        this.addParameter("grid_north_pole_longitude", northPoleLon);
    }
    
    private void buildRotationMatrices() {
        final double betaRad = -(90.0 - this.northPole.y) * 0.017453292519943295;
        final double gammaRad = (this.northPole.x + 180.0) * 0.017453292519943295;
        final double cosBeta = Math.cos(betaRad);
        final double sinBeta = Math.sin(betaRad);
        final double cosGamma = Math.cos(gammaRad);
        final double sinGamma = Math.sin(gammaRad);
        this.rotY[0][0] = cosBeta;
        this.rotY[0][1] = 0.0;
        this.rotY[0][2] = -sinBeta;
        this.rotY[1][0] = 0.0;
        this.rotY[1][1] = 1.0;
        this.rotY[1][2] = 0.0;
        this.rotY[2][0] = sinBeta;
        this.rotY[2][1] = 0.0;
        this.rotY[2][2] = cosBeta;
        this.rotZ[0][0] = cosGamma;
        this.rotZ[0][1] = sinGamma;
        this.rotZ[0][2] = 0.0;
        this.rotZ[1][0] = -sinGamma;
        this.rotZ[1][1] = cosGamma;
        this.rotZ[1][2] = 0.0;
        this.rotZ[2][0] = 0.0;
        this.rotZ[2][1] = 0.0;
        this.rotZ[2][2] = 1.0;
    }
    
    public Point2D.Double getNorthPole() {
        return (Point2D.Double)this.northPole.clone();
    }
    
    @Override
    public ProjectionImpl constructCopy() {
        return new RotatedPole(this.northPole.getY(), this.northPole.getX());
    }
    
    @Override
    public String paramsToString() {
        return " northPole= " + this.northPole;
    }
    
    @Override
    public ProjectionPoint latLonToProj(final LatLonPoint latlon, ProjectionPointImpl destPoint) {
        final double lat = latlon.getLatitude();
        final double lon = latlon.getLongitude();
        final double[] p0 = { Math.cos(lat * 0.017453292519943295) * Math.cos(lon * 0.017453292519943295), Math.cos(lat * 0.017453292519943295) * Math.sin(lon * 0.017453292519943295), Math.sin(lat * 0.017453292519943295) };
        final double[] p2 = { this.rotZ[0][0] * p0[0] + this.rotZ[0][1] * p0[1] + this.rotZ[0][2] * p0[2], this.rotZ[1][0] * p0[0] + this.rotZ[1][1] * p0[1] + this.rotZ[1][2] * p0[2], this.rotZ[2][0] * p0[0] + this.rotZ[2][1] * p0[1] + this.rotZ[2][2] * p0[2] };
        final double[] p3 = { this.rotY[0][0] * p2[0] + this.rotY[0][1] * p2[1] + this.rotY[0][2] * p2[2], this.rotY[1][0] * p2[0] + this.rotY[1][1] * p2[1] + this.rotY[1][2] * p2[2], this.rotY[2][0] * p2[0] + this.rotY[2][1] * p2[1] + this.rotY[2][2] * p2[2] };
        final double lonR = LatLonPointImpl.range180(Math.atan2(p3[1], p3[0]) * 57.29577951308232);
        final double latR = Math.asin(p3[2]) * 57.29577951308232;
        if (destPoint == null) {
            destPoint = new ProjectionPointImpl(lonR, latR);
        }
        else {
            destPoint.setLocation(lonR, latR);
        }
        if (RotatedPole.show) {
            System.out.println("LatLon= " + latlon + " proj= " + destPoint);
        }
        return destPoint;
    }
    
    @Override
    public LatLonPoint projToLatLon(final ProjectionPoint ppt, LatLonPointImpl destPoint) {
        final double lonR = LatLonPointImpl.range180(ppt.getX());
        final double latR = ppt.getY();
        final double[] p0 = { Math.cos(latR * 0.017453292519943295) * Math.cos(lonR * 0.017453292519943295), Math.cos(latR * 0.017453292519943295) * Math.sin(lonR * 0.017453292519943295), Math.sin(latR * 0.017453292519943295) };
        final double[] p2 = { this.rotY[0][0] * p0[0] + this.rotY[1][0] * p0[1] + this.rotY[2][0] * p0[2], this.rotY[0][1] * p0[0] + this.rotY[1][1] * p0[1] + this.rotY[2][1] * p0[2], this.rotY[0][2] * p0[0] + this.rotY[1][2] * p0[1] + this.rotY[2][2] * p0[2] };
        final double[] p3 = { this.rotZ[0][0] * p2[0] + this.rotZ[1][0] * p2[1] + this.rotZ[2][0] * p2[2], this.rotZ[0][1] * p2[0] + this.rotZ[1][1] * p2[1] + this.rotZ[2][1] * p2[2], this.rotZ[0][2] * p2[0] + this.rotZ[1][2] * p2[1] + this.rotZ[2][2] * p2[2] };
        final double lon = Math.atan2(p3[1], p3[0]) * 57.29577951308232;
        final double lat = Math.asin(p3[2]) * 57.29577951308232;
        if (destPoint == null) {
            destPoint = new LatLonPointImpl(lat, lon);
        }
        else {
            destPoint.set(lat, lon);
        }
        if (RotatedPole.show) {
            System.out.println("Proj= " + ppt + " latlon= " + destPoint);
        }
        return destPoint;
    }
    
    @Override
    public boolean crossSeam(final ProjectionPoint pt1, final ProjectionPoint pt2) {
        return Math.abs(pt1.getX() - pt2.getX()) > 270.0;
    }
    
    @Override
    public boolean equals(final Object proj) {
        if (!(proj instanceof RotatedPole)) {
            return false;
        }
        final RotatedPole oo = (RotatedPole)proj;
        return this.getNorthPole().equals(oo.getNorthPole());
    }
    
    static {
        RotatedPole.show = false;
    }
}
