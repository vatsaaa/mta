// 
// Decompiled by Procyon v0.5.36
// 

package ucar.unidata.geoloc.projection.proj4;

import ucar.unidata.geoloc.LatLonPointImpl;
import ucar.unidata.geoloc.ProjectionPoint;
import ucar.unidata.geoloc.ProjectionPointImpl;
import ucar.unidata.geoloc.LatLonPoint;
import java.util.Formatter;
import java.awt.geom.Point2D;
import ucar.unidata.geoloc.Earth;
import ucar.unidata.geoloc.ProjectionImpl;

public class StereographicAzimuthalProjection extends ProjectionImpl
{
    double projectionLatitude;
    double projectionLongitude;
    double n;
    double scaleFactor;
    double trueScaleLatitude;
    double falseEasting;
    double falseNorthing;
    private Earth earth;
    private double e;
    private double es;
    private double one_es;
    private double totalScale;
    private boolean isSpherical;
    public static final int NORTH_POLE = 1;
    public static final int SOUTH_POLE = 2;
    public static final int EQUATOR = 3;
    public static final int OBLIQUE = 4;
    private static final double TOL = 1.0E-8;
    private double EPS10;
    private double akm1;
    private double sinphi0;
    private double cosphi0;
    private int mode;
    
    public StereographicAzimuthalProjection() {
        this(Math.toRadians(90.0), Math.toRadians(0.0), 0.994, 90.0, 0.0, 0.0, new Earth());
    }
    
    public StereographicAzimuthalProjection(final double latt, final double lont, final double scale, final double trueScaleLat, final double false_easting, final double false_northing, final Earth earth) {
        this.EPS10 = 1.0E-10;
        this.projectionLatitude = Math.toRadians(latt);
        this.n = Math.sin(this.projectionLatitude);
        this.projectionLongitude = Math.toRadians(lont);
        this.trueScaleLatitude = Math.toRadians(trueScaleLat);
        this.scaleFactor = scale;
        this.falseEasting = false_easting;
        this.falseNorthing = false_northing;
        this.earth = earth;
        this.e = earth.getEccentricity();
        this.es = earth.getEccentricitySquared();
        this.isSpherical = (this.e == 0.0);
        this.one_es = 1.0 - this.es;
        this.totalScale = earth.getMajor();
        this.initialize();
        this.addParameter("grid_mapping_name", "stereographic");
        this.addParameter("longitude_of_projection_origin", lont);
        this.addParameter("latitude_of_projection_origin", latt);
        this.addParameter("scale_factor_at_projection_origin", scale);
        if (false_easting != 0.0 || false_northing != 0.0) {
            this.addParameter("false_easting", false_easting);
            this.addParameter("false_northing", false_northing);
            this.addParameter("units", "km");
        }
        this.addParameter("semi_major_axis", earth.getMajor());
        this.addParameter("inverse_flattening", 1.0 / earth.getFlattening());
    }
    
    public void initialize() {
        double t;
        if (Math.abs((t = Math.abs(this.projectionLatitude)) - 1.5707963267948966) < this.EPS10) {
            this.mode = ((this.projectionLatitude < 0.0) ? 2 : 1);
        }
        else {
            this.mode = ((t > this.EPS10) ? 4 : 3);
        }
        this.trueScaleLatitude = Math.abs(this.trueScaleLatitude);
        if (this.isSpherical) {
            switch (this.mode) {
                case 4: {
                    this.sinphi0 = Math.sin(this.projectionLatitude);
                    this.cosphi0 = Math.cos(this.projectionLatitude);
                }
                case 3: {
                    this.akm1 = 2.0 * this.scaleFactor;
                    break;
                }
                case 1:
                case 2: {
                    this.akm1 = ((Math.abs(this.trueScaleLatitude - 1.5707963267948966) >= this.EPS10) ? (Math.cos(this.trueScaleLatitude) / Math.tan(0.7853981633974483 - 0.5 * this.trueScaleLatitude)) : (2.0 * this.scaleFactor));
                    break;
                }
            }
        }
        else {
            switch (this.mode) {
                case 1:
                case 2: {
                    if (Math.abs(this.trueScaleLatitude - 1.5707963267948966) < this.EPS10) {
                        this.akm1 = 2.0 * this.scaleFactor / Math.sqrt(Math.pow(1.0 + this.e, 1.0 + this.e) * Math.pow(1.0 - this.e, 1.0 - this.e));
                        break;
                    }
                    this.akm1 = Math.cos(this.trueScaleLatitude) / MapMath.tsfn(this.trueScaleLatitude, t = Math.sin(this.trueScaleLatitude), this.e);
                    t *= this.e;
                    this.akm1 /= Math.sqrt(1.0 - t * t);
                    break;
                }
                case 3: {
                    this.akm1 = 2.0 * this.scaleFactor;
                    break;
                }
                case 4: {
                    t = Math.sin(this.projectionLatitude);
                    final double X = 2.0 * Math.atan(this.ssfn(this.projectionLatitude, t, this.e)) - 1.5707963267948966;
                    t *= this.e;
                    this.akm1 = 2.0 * this.scaleFactor * Math.cos(this.projectionLatitude) / Math.sqrt(1.0 - t * t);
                    this.sinphi0 = Math.sin(X);
                    this.cosphi0 = Math.cos(X);
                    break;
                }
            }
        }
    }
    
    public Point2D.Double project(final double lam, double phi, final Point2D.Double xy) {
        double coslam = Math.cos(lam);
        final double sinlam = Math.sin(lam);
        double sinphi = Math.sin(phi);
        if (this.isSpherical) {
            final double cosphi = Math.cos(phi);
            switch (this.mode) {
                case 3: {
                    xy.y = 1.0 + cosphi * coslam;
                    if (xy.y <= this.EPS10) {
                        throw new RuntimeException("I");
                    }
                    final double y = this.akm1 / xy.y;
                    xy.y = y;
                    xy.x = y * cosphi * sinlam;
                    xy.y *= sinphi;
                    break;
                }
                case 4: {
                    xy.y = 1.0 + this.sinphi0 * sinphi + this.cosphi0 * cosphi * coslam;
                    if (xy.y <= this.EPS10) {
                        throw new RuntimeException("I");
                    }
                    final double y2 = this.akm1 / xy.y;
                    xy.y = y2;
                    xy.x = y2 * cosphi * sinlam;
                    xy.y *= this.cosphi0 * sinphi - this.sinphi0 * cosphi * coslam;
                    break;
                }
                case 1: {
                    coslam = -coslam;
                    phi = -phi;
                }
                case 2: {
                    if (Math.abs(phi - 1.5707963267948966) < 1.0E-8) {
                        throw new RuntimeException("I");
                    }
                    final double n = sinlam;
                    final double y3 = this.akm1 * Math.tan(0.7853981633974483 + 0.5 * phi);
                    xy.y = y3;
                    xy.x = n * y3;
                    xy.y *= coslam;
                    break;
                }
            }
        }
        else {
            double sinX = 0.0;
            double cosX = 0.0;
            if (this.mode == 4 || this.mode == 3) {
                final double X;
                sinX = Math.sin(X = 2.0 * Math.atan(this.ssfn(phi, sinphi, this.e)) - 1.5707963267948966);
                cosX = Math.cos(X);
            }
            switch (this.mode) {
                case 4: {
                    final double A = this.akm1 / (this.cosphi0 * (1.0 + this.sinphi0 * sinX + this.cosphi0 * cosX * coslam));
                    xy.y = A * (this.cosphi0 * sinX - this.sinphi0 * cosX * coslam);
                    xy.x = A * cosX;
                    break;
                }
                case 3: {
                    final double A = 2.0 * this.akm1 / (1.0 + cosX * coslam);
                    xy.y = A * sinX;
                    xy.x = A * cosX;
                    break;
                }
                case 2: {
                    phi = -phi;
                    coslam = -coslam;
                    sinphi = -sinphi;
                }
                case 1: {
                    xy.x = this.akm1 * MapMath.tsfn(phi, sinphi, this.e);
                    xy.y = -xy.x * coslam;
                    break;
                }
            }
            xy.x *= sinlam;
        }
        return xy;
    }
    
    public Point2D.Double projectInverse(double x, double y, final Point2D.Double lp) {
        if (this.isSpherical) {
            final double rh;
            double c;
            final double sinc = Math.sin(c = 2.0 * Math.atan((rh = MapMath.distance(x, y)) / this.akm1));
            final double cosc = Math.cos(c);
            lp.x = 0.0;
            switch (this.mode) {
                case 3: {
                    if (Math.abs(rh) <= this.EPS10) {
                        lp.y = 0.0;
                    }
                    else {
                        lp.y = Math.asin(y * sinc / rh);
                    }
                    if (cosc != 0.0 || x != 0.0) {
                        lp.x = Math.atan2(x * sinc, cosc * rh);
                        break;
                    }
                    break;
                }
                case 4: {
                    if (Math.abs(rh) <= this.EPS10) {
                        lp.y = this.projectionLatitude;
                    }
                    else {
                        lp.y = Math.asin(cosc * this.sinphi0 + y * sinc * this.cosphi0 / rh);
                    }
                    if ((c = cosc - this.sinphi0 * Math.sin(lp.y)) != 0.0 || x != 0.0) {
                        lp.x = Math.atan2(x * sinc * this.cosphi0, c * rh);
                        break;
                    }
                    break;
                }
                case 1: {
                    y = -y;
                }
                case 2: {
                    if (Math.abs(rh) <= this.EPS10) {
                        lp.y = this.projectionLatitude;
                    }
                    else {
                        lp.y = Math.asin((this.mode == 2) ? (-cosc) : cosc);
                    }
                    lp.x = ((x == 0.0 && y == 0.0) ? 0.0 : Math.atan2(x, y));
                    break;
                }
            }
            return lp;
        }
        final double rho = MapMath.distance(x, y);
        double tp = 0.0;
        double phi_l = 0.0;
        double halfpi = 0.0;
        double halfe = 0.0;
        switch (this.mode) {
            default: {
                final double cosphi = Math.cos(tp = 2.0 * Math.atan2(rho * this.cosphi0, this.akm1));
                final double sinphi = Math.sin(tp);
                phi_l = Math.asin(cosphi * this.sinphi0 + y * sinphi * this.cosphi0 / rho);
                tp = Math.tan(0.5 * (1.5707963267948966 + phi_l));
                x *= sinphi;
                y = rho * this.cosphi0 * cosphi - y * this.sinphi0 * sinphi;
                halfpi = 1.5707963267948966;
                halfe = 0.5 * this.e;
                break;
            }
            case 1: {
                y = -y;
            }
            case 2: {
                phi_l = 1.5707963267948966 - 2.0 * Math.atan(tp = -rho / this.akm1);
                halfpi = -1.5707963267948966;
                halfe = -0.5 * this.e;
                break;
            }
        }
        int i = 8;
        while (i-- != 0) {
            final double sinphi = this.e * Math.sin(phi_l);
            lp.y = 2.0 * Math.atan(tp * Math.pow((1.0 + sinphi) / (1.0 - sinphi), halfe)) - halfpi;
            if (Math.abs(phi_l - lp.y) < this.EPS10) {
                if (this.mode == 2) {
                    lp.y = -lp.y;
                }
                lp.x = ((x == 0.0 && y == 0.0) ? 0.0 : Math.atan2(x, y));
                return lp;
            }
            phi_l = lp.y;
        }
        throw new RuntimeException("Iteration didn't converge");
    }
    
    public boolean isConformal() {
        return true;
    }
    
    public boolean hasInverse() {
        return true;
    }
    
    private double ssfn(final double phit, double sinphi, final double eccen) {
        sinphi *= eccen;
        return Math.tan(0.5 * (1.5707963267948966 + phit)) * Math.pow((1.0 - sinphi) / (1.0 + sinphi), 0.5 * eccen);
    }
    
    @Override
    public String getProjectionTypeLabel() {
        return "Stereographic Azimuthal Ellipsoidal Earth";
    }
    
    @Override
    public ProjectionImpl constructCopy() {
        return new StereographicAzimuthalProjection(Math.toDegrees(this.projectionLatitude), Math.toDegrees(this.projectionLongitude), this.scaleFactor, Math.toDegrees(this.trueScaleLatitude), this.falseEasting, this.falseNorthing, this.earth);
    }
    
    @Override
    public String paramsToString() {
        final Formatter f = new Formatter();
        f.format("origin lat,lon=%f,%f scale,trueScaleLat=%f,%f earth=%s", Math.toDegrees(this.projectionLatitude), Math.toDegrees(this.projectionLongitude), this.scaleFactor, Math.toDegrees(this.trueScaleLatitude), this.earth);
        return f.toString();
    }
    
    @Override
    public ProjectionPoint latLonToProj(final LatLonPoint latLon, final ProjectionPointImpl destPoint) {
        final double fromLat = Math.toRadians(latLon.getLatitude());
        final double theta = this.computeTheta(latLon.getLongitude());
        final Point2D.Double res = this.project(theta, fromLat, new Point2D.Double());
        destPoint.setLocation(this.totalScale * res.x + this.falseEasting, this.totalScale * res.y + this.falseNorthing);
        return destPoint;
    }
    
    @Override
    public LatLonPoint projToLatLon(final ProjectionPoint world, final LatLonPointImpl result) {
        final double fromX = (world.getX() - this.falseEasting) / this.totalScale;
        final double fromY = (world.getY() - this.falseNorthing) / this.totalScale;
        final Point2D.Double dst = this.projectInverse(fromX, fromY, new Point2D.Double());
        if (dst.x < -3.141592653589793) {
            dst.x = -3.141592653589793;
        }
        else if (dst.x > 3.141592653589793) {
            dst.x = 3.141592653589793;
        }
        if (this.projectionLongitude != 0.0) {
            dst.x = MapMath.normalizeLongitude(dst.x + this.projectionLongitude);
        }
        result.setLongitude(Math.toDegrees(dst.x));
        result.setLatitude(Math.toDegrees(dst.y));
        return result;
    }
    
    @Override
    public boolean crossSeam(final ProjectionPoint pt1, final ProjectionPoint pt2) {
        return false;
    }
    
    @Override
    public boolean equals(final Object proj) {
        if (!(proj instanceof StereographicAzimuthalProjection)) {
            return false;
        }
        final StereographicAzimuthalProjection oo = (StereographicAzimuthalProjection)proj;
        return this.projectionLatitude == oo.projectionLatitude && this.projectionLongitude == oo.projectionLongitude && this.scaleFactor == oo.scaleFactor && this.trueScaleLatitude == oo.trueScaleLatitude && this.falseEasting == oo.falseEasting && this.falseNorthing == oo.falseNorthing && this.earth.equals(oo.earth);
    }
    
    @Override
    public int hashCode() {
        int hash = 3;
        hash = 67 * hash + (int)(Double.doubleToLongBits(this.projectionLatitude) ^ Double.doubleToLongBits(this.projectionLatitude) >>> 32);
        hash = 67 * hash + (int)(Double.doubleToLongBits(this.projectionLongitude) ^ Double.doubleToLongBits(this.projectionLongitude) >>> 32);
        hash = 67 * hash + (int)(Double.doubleToLongBits(this.scaleFactor) ^ Double.doubleToLongBits(this.scaleFactor) >>> 32);
        hash = 67 * hash + (int)(Double.doubleToLongBits(this.trueScaleLatitude) ^ Double.doubleToLongBits(this.trueScaleLatitude) >>> 32);
        hash = 67 * hash + (int)(Double.doubleToLongBits(this.falseEasting) ^ Double.doubleToLongBits(this.falseEasting) >>> 32);
        hash = 67 * hash + (int)(Double.doubleToLongBits(this.falseNorthing) ^ Double.doubleToLongBits(this.falseNorthing) >>> 32);
        hash = 67 * hash + ((this.earth != null) ? this.earth.hashCode() : 0);
        return hash;
    }
    
    private double computeTheta(final double lon) {
        final double dlon = LatLonPointImpl.lonNormal(lon - Math.toDegrees(this.projectionLongitude));
        return this.n * Math.toRadians(dlon);
    }
    
    private static void test(final ProjectionImpl proj, final double[] lat, final double[] lon) {
        final double[] x = new double[lat.length];
        final double[] y = new double[lat.length];
        for (int i = 0; i < lat.length; ++i) {
            final LatLonPoint lp = new LatLonPointImpl(lat[i], lon[i]);
            final ProjectionPointImpl p = (ProjectionPointImpl)proj.latLonToProj(lp, new ProjectionPointImpl());
            System.out.println(lp.getLatitude() + ", " + lp.getLongitude() + ": " + p.x + ", " + p.y);
            x[i] = p.x;
            y[i] = p.y;
        }
        for (int i = 0; i < lat.length; ++i) {
            final ProjectionPointImpl p2 = new ProjectionPointImpl(x[i], y[i]);
            final LatLonPointImpl lp2 = (LatLonPointImpl)proj.projToLatLon(p2);
            if (Math.abs(lp2.getLatitude() - lat[i]) > 1.0E-5 || Math.abs(lp2.getLongitude() - lon[i]) > 1.0E-5) {
                if (Math.abs(lp2.getLatitude()) <= 89.99 || Math.abs(lp2.getLatitude() - lat[i]) >= 1.0E-5) {
                    System.err.print("ERROR:");
                }
            }
            System.out.println("reverse:" + p2.x + ", " + p2.y + ": " + lp2.getLatitude() + ", " + lp2.getLongitude());
        }
    }
    
    public static void main(final String[] args) {
        final Earth e = new Earth(6378.137, 0.0, 298.257224);
        StereographicAzimuthalProjection proj = new StereographicAzimuthalProjection(90.0, 0.0, 0.93306907, 90.0, 0.0, 0.0, e);
        final double[] lat = { 60.0, 90.0, 60.0 };
        final double[] lon = { 0.0, 0.0, 10.0 };
        test(proj, lat, lon);
        proj = new StereographicAzimuthalProjection(90.0, -45.0, 0.96985819, 90.0, 0.0, 0.0, e);
        test(proj, lat, lon);
    }
}
