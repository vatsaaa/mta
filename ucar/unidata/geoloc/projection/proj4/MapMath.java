// 
// Decompiled by Procyon v0.5.36
// 

package ucar.unidata.geoloc.projection.proj4;

import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;

public class MapMath
{
    public static final double HALFPI = 1.5707963267948966;
    public static final double QUARTERPI = 0.7853981633974483;
    public static final double TWOPI = 6.283185307179586;
    public static final double RTD = 57.29577951308232;
    public static final double DTR = 0.017453292519943295;
    public static final Rectangle2D WORLD_BOUNDS_RAD;
    public static final Rectangle2D WORLD_BOUNDS;
    public static final int DONT_INTERSECT = 0;
    public static final int DO_INTERSECT = 1;
    public static final int COLLINEAR = 2;
    private static final int N_ITER = 15;
    private static final double C00 = 1.0;
    private static final double C02 = 0.25;
    private static final double C04 = 0.046875;
    private static final double C06 = 0.01953125;
    private static final double C08 = 0.01068115234375;
    private static final double C22 = 0.75;
    private static final double C44 = 0.46875;
    private static final double C46 = 0.013020833333333334;
    private static final double C48 = 0.007120768229166667;
    private static final double C66 = 0.3645833333333333;
    private static final double C68 = 0.005696614583333333;
    private static final double C88 = 0.3076171875;
    private static final int MAX_ITER = 10;
    private static final double P00 = 0.3333333333333333;
    private static final double P01 = 0.17222222222222222;
    private static final double P02 = 0.10257936507936508;
    private static final double P10 = 0.06388888888888888;
    private static final double P11 = 0.0664021164021164;
    private static final double P20 = 0.016415012942191543;
    
    public static double sind(final double v) {
        return Math.sin(v * 0.017453292519943295);
    }
    
    public static double cosd(final double v) {
        return Math.cos(v * 0.017453292519943295);
    }
    
    public static double tand(final double v) {
        return Math.tan(v * 0.017453292519943295);
    }
    
    public static double asind(final double v) {
        return Math.asin(v) * 57.29577951308232;
    }
    
    public static double acosd(final double v) {
        return Math.acos(v) * 57.29577951308232;
    }
    
    public static double atand(final double v) {
        return Math.atan(v) * 57.29577951308232;
    }
    
    public static double atan2d(final double y, final double x) {
        return Math.atan2(y, x) * 57.29577951308232;
    }
    
    public static double asin(final double v) {
        if (Math.abs(v) > 1.0) {
            return (v < 0.0) ? -1.5707963267948966 : 1.5707963267948966;
        }
        return Math.asin(v);
    }
    
    public static double acos(final double v) {
        if (Math.abs(v) > 1.0) {
            return (v < 0.0) ? 3.141592653589793 : 0.0;
        }
        return Math.acos(v);
    }
    
    public static double sqrt(final double v) {
        return (v < 0.0) ? 0.0 : Math.sqrt(v);
    }
    
    public static double distance(final double dx, final double dy) {
        return Math.sqrt(dx * dx + dy * dy);
    }
    
    public static double distance(final Point2D.Double a, final Point2D.Double b) {
        return distance(a.x - b.x, a.y - b.y);
    }
    
    public static double hypot(double x, double y) {
        if (x < 0.0) {
            x = -x;
        }
        else if (x == 0.0) {
            return (y < 0.0) ? (-y) : y;
        }
        if (y < 0.0) {
            y = -y;
        }
        else if (y == 0.0) {
            return x;
        }
        if (x < y) {
            x /= y;
            return y * Math.sqrt(1.0 + x * x);
        }
        y /= x;
        return x * Math.sqrt(1.0 + y * y);
    }
    
    public static double atan2(final double y, final double x) {
        return Math.atan2(y, x);
    }
    
    public static double trunc(final double v) {
        return (v < 0.0) ? Math.ceil(v) : Math.floor(v);
    }
    
    public static double frac(final double v) {
        return v - trunc(v);
    }
    
    public static double degToRad(final double v) {
        return v * 3.141592653589793 / 180.0;
    }
    
    public static double radToDeg(final double v) {
        return v * 180.0 / 3.141592653589793;
    }
    
    public static double dmsToRad(final double d, final double m, final double s) {
        if (d >= 0.0) {
            return (d + m / 60.0 + s / 3600.0) * 3.141592653589793 / 180.0;
        }
        return (d - m / 60.0 - s / 3600.0) * 3.141592653589793 / 180.0;
    }
    
    public static double dmsToDeg(final double d, final double m, final double s) {
        if (d >= 0.0) {
            return d + m / 60.0 + s / 3600.0;
        }
        return d - m / 60.0 - s / 3600.0;
    }
    
    public static double normalizeLatitude(double angle) {
        if (Double.isInfinite(angle) || Double.isNaN(angle)) {
            throw new RuntimeException("Infinite latitude");
        }
        while (angle > 1.5707963267948966) {
            angle -= 3.141592653589793;
        }
        while (angle < -1.5707963267948966) {
            angle += 3.141592653589793;
        }
        return angle;
    }
    
    public static double normalizeLongitude(double angle) {
        if (Double.isInfinite(angle) || Double.isNaN(angle)) {
            throw new RuntimeException("Infinite longitude");
        }
        while (angle > 3.141592653589793) {
            angle -= 6.283185307179586;
        }
        while (angle < -3.141592653589793) {
            angle += 6.283185307179586;
        }
        return angle;
    }
    
    public static double normalizeAngle(double angle) {
        if (Double.isInfinite(angle) || Double.isNaN(angle)) {
            throw new RuntimeException("Infinite angle");
        }
        while (angle > 6.283185307179586) {
            angle -= 6.283185307179586;
        }
        while (angle < 0.0) {
            angle += 6.283185307179586;
        }
        return angle;
    }
    
    public static double greatCircleDistance(final double lon1, final double lat1, final double lon2, final double lat2) {
        final double dlat = Math.sin((lat2 - lat1) / 2.0);
        final double dlon = Math.sin((lon2 - lon1) / 2.0);
        final double r = Math.sqrt(dlat * dlat + Math.cos(lat1) * Math.cos(lat2) * dlon * dlon);
        return 2.0 * Math.asin(r);
    }
    
    public static double sphericalAzimuth(final double lat0, final double lon0, final double lat, final double lon) {
        final double diff = lon - lon0;
        final double coslat = Math.cos(lat);
        return Math.atan2(coslat * Math.sin(diff), Math.cos(lat0) * Math.sin(lat) - Math.sin(lat0) * coslat * Math.cos(diff));
    }
    
    public static boolean sameSigns(final double a, final double b) {
        return a < 0.0 == b < 0.0;
    }
    
    public static boolean sameSigns(final int a, final int b) {
        return a < 0 == b < 0;
    }
    
    public static double takeSign(double a, final double b) {
        a = Math.abs(a);
        if (b < 0.0) {
            return -a;
        }
        return a;
    }
    
    public static int takeSign(int a, final int b) {
        a = Math.abs(a);
        if (b < 0) {
            return -a;
        }
        return a;
    }
    
    public static int intersectSegments(final Point2D.Double aStart, final Point2D.Double aEnd, final Point2D.Double bStart, final Point2D.Double bEnd, final Point2D.Double p) {
        final double a1 = aEnd.y - aStart.y;
        final double b1 = aStart.x - aEnd.x;
        final double c1 = aEnd.x * aStart.y - aStart.x * aEnd.y;
        final double r3 = a1 * bStart.x + b1 * bStart.y + c1;
        final double r4 = a1 * bEnd.x + b1 * bEnd.y + c1;
        if (r3 != 0.0 && r4 != 0.0 && sameSigns(r3, r4)) {
            return 0;
        }
        final double a2 = bEnd.y - bStart.y;
        final double b2 = bStart.x - bEnd.x;
        final double c2 = bEnd.x * bStart.y - bStart.x * bEnd.y;
        final double r5 = a2 * aStart.x + b2 * aStart.y + c2;
        final double r6 = a2 * aEnd.x + b2 * aEnd.y + c2;
        if (r5 != 0.0 && r6 != 0.0 && sameSigns(r5, r6)) {
            return 0;
        }
        final double denom = a1 * b2 - a2 * b1;
        if (denom == 0.0) {
            return 2;
        }
        final double offset = (denom < 0.0) ? (-denom / 2.0) : (denom / 2.0);
        double num = b1 * c2 - b2 * c1;
        p.x = ((num < 0.0) ? (num - offset) : (num + offset)) / denom;
        num = a2 * c1 - a1 * c2;
        p.y = ((num < 0.0) ? (num - offset) : (num + offset)) / denom;
        return 1;
    }
    
    public static double dot(final Point2D.Double a, final Point2D.Double b) {
        return a.x * b.x + a.y * b.y;
    }
    
    public static Point2D.Double perpendicular(final Point2D.Double a) {
        return new Point2D.Double(-a.y, a.x);
    }
    
    public static Point2D.Double add(final Point2D.Double a, final Point2D.Double b) {
        return new Point2D.Double(a.x + b.x, a.y + b.y);
    }
    
    public static Point2D.Double subtract(final Point2D.Double a, final Point2D.Double b) {
        return new Point2D.Double(a.x - b.x, a.y - b.y);
    }
    
    public static Point2D.Double multiply(final Point2D.Double a, final Point2D.Double b) {
        return new Point2D.Double(a.x * b.x, a.y * b.y);
    }
    
    public static double cross(final Point2D.Double a, final Point2D.Double b) {
        return a.x * b.y - b.x * a.y;
    }
    
    public static double cross(final double x1, final double y1, final double x2, final double y2) {
        return x1 * y2 - x2 * y1;
    }
    
    public static void normalize(final Point2D.Double a) {
        final double d = distance(a.x, a.y);
        a.x /= d;
        a.y /= d;
    }
    
    public static void negate(final Point2D.Double a) {
        a.x = -a.x;
        a.y = -a.y;
    }
    
    public static double longitudeDistance(final double l1, final double l2) {
        return Math.min(Math.abs(l1 - l2), ((l1 < 0.0) ? (l1 + 3.141592653589793) : (3.141592653589793 - l1)) + ((l2 < 0.0) ? (l2 + 3.141592653589793) : (3.141592653589793 - l2)));
    }
    
    public static double geocentricLatitude(final double lat, final double flatness) {
        final double f = 1.0 - flatness;
        return Math.atan(f * f * Math.tan(lat));
    }
    
    public static double geographicLatitude(final double lat, final double flatness) {
        final double f = 1.0 - flatness;
        return Math.atan(Math.tan(lat) / (f * f));
    }
    
    public static double tsfn(final double phi, double sinphi, final double e) {
        sinphi *= e;
        return Math.tan(0.5 * (1.5707963267948966 - phi)) / Math.pow((1.0 - sinphi) / (1.0 + sinphi), 0.5 * e);
    }
    
    public static double msfn(final double sinphi, final double cosphi, final double es) {
        return cosphi / Math.sqrt(1.0 - es * sinphi * sinphi);
    }
    
    public static double phi2(final double ts, final double e) {
        final double eccnth = 0.5 * e;
        double phi = 1.5707963267948966 - 2.0 * Math.atan(ts);
        int i = 15;
        double dphi;
        do {
            final double con = e * Math.sin(phi);
            dphi = 1.5707963267948966 - 2.0 * Math.atan(ts * Math.pow((1.0 - con) / (1.0 + con), eccnth)) - phi;
            phi += dphi;
        } while (Math.abs(dphi) > 1.0E-10 && --i != 0);
        if (i <= 0) {
            throw new RuntimeException();
        }
        return phi;
    }
    
    public static double[] enfn(final double es) {
        double t;
        final double[] en = { 1.0 - es * (0.25 + es * (0.046875 + es * (0.01953125 + es * 0.01068115234375))), es * (0.75 - es * (0.046875 + es * (0.01953125 + es * 0.01068115234375))), (t = es * es) * (0.46875 - es * (0.013020833333333334 + es * 0.007120768229166667)), (t *= es) * (0.3645833333333333 - es * 0.005696614583333333), t * es * 0.3076171875 };
        return en;
    }
    
    public static double mlfn(final double phi, double sphi, double cphi, final double[] en) {
        cphi *= sphi;
        sphi *= sphi;
        return en[0] * phi - cphi * (en[1] + sphi * (en[2] + sphi * (en[3] + sphi * en[4])));
    }
    
    public static double inv_mlfn(final double arg, final double es, final double[] en) {
        final double k = 1.0 / (1.0 - es);
        double phi = arg;
        for (int i = 10; i != 0; --i) {
            final double s = Math.sin(phi);
            double t = 1.0 - es * s * s;
            phi -= (t = (mlfn(phi, s, Math.cos(phi), en) - arg) * (t * Math.sqrt(t)) * k);
            if (Math.abs(t) < 1.0E-11) {
                return phi;
            }
        }
        return phi;
    }
    
    public static double[] authset(final double es) {
        final double[] APA = { es * 0.3333333333333333, 0.0, 0.0 };
        double t = es * es;
        final double[] array = APA;
        final int n = 0;
        array[n] += t * 0.17222222222222222;
        APA[1] = t * 0.06388888888888888;
        t *= es;
        final double[] array2 = APA;
        final int n2 = 0;
        array2[n2] += t * 0.10257936507936508;
        final double[] array3 = APA;
        final int n3 = 1;
        array3[n3] += t * 0.0664021164021164;
        APA[2] = t * 0.016415012942191543;
        return APA;
    }
    
    public static double authlat(final double beta, final double[] APA) {
        final double t = beta + beta;
        return beta + APA[0] * Math.sin(t) + APA[1] * Math.sin(t + t) + APA[2] * Math.sin(t + t + t);
    }
    
    public static double qsfn(final double sinphi, final double e, final double one_es) {
        if (e >= 1.0E-7) {
            final double con = e * sinphi;
            return one_es * (sinphi / (1.0 - con * con) - 0.5 / e * Math.log((1.0 - con) / (1.0 + con)));
        }
        return sinphi + sinphi;
    }
    
    public static double niceNumber(final double x, final boolean round) {
        final int expv = (int)Math.floor(Math.log(x) / Math.log(10.0));
        final double f = x / Math.pow(10.0, expv);
        double nf;
        if (round) {
            if (f < 1.5) {
                nf = 1.0;
            }
            else if (f < 3.0) {
                nf = 2.0;
            }
            else if (f < 7.0) {
                nf = 5.0;
            }
            else {
                nf = 10.0;
            }
        }
        else if (f <= 1.0) {
            nf = 1.0;
        }
        else if (f <= 2.0) {
            nf = 2.0;
        }
        else if (f <= 5.0) {
            nf = 5.0;
        }
        else {
            nf = 10.0;
        }
        return nf * Math.pow(10.0, expv);
    }
    
    static {
        WORLD_BOUNDS_RAD = new Rectangle2D.Double(-3.141592653589793, -1.5707963267948966, 6.283185307179586, 3.141592653589793);
        WORLD_BOUNDS = new Rectangle2D.Double(-180.0, -90.0, 360.0, 180.0);
    }
}
