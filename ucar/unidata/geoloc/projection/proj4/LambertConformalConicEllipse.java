// 
// Decompiled by Procyon v0.5.36
// 

package ucar.unidata.geoloc.projection.proj4;

import ucar.unidata.geoloc.LatLonPoint;
import ucar.unidata.geoloc.LatLonPointImpl;
import ucar.unidata.geoloc.ProjectionPointImpl;
import ucar.unidata.geoloc.ProjectionPoint;
import java.util.Formatter;
import ucar.unidata.util.Parameter;
import ucar.unidata.geoloc.Earth;
import ucar.unidata.geoloc.ProjectionImpl;

public class LambertConformalConicEllipse extends ProjectionImpl
{
    private static final double TOL = 1.0E-10;
    private double lat0deg;
    private double lon0deg;
    private double lat0rad;
    private double lon0rad;
    private double par1deg;
    private double par2deg;
    private double par1rad;
    private double par2rad;
    private double falseEasting;
    private double falseNorthing;
    private Earth earth;
    private double e;
    private double es;
    private double totalScale;
    private double n;
    private double c;
    private double rho0;
    private boolean isSpherical;
    
    @Override
    public ProjectionImpl constructCopy() {
        return new LambertConformalConicEllipse(this.getOriginLat(), this.getOriginLon(), this.getParallelOne(), this.getParallelTwo(), this.getFalseEasting(), this.getFalseNorthing(), this.getEarth());
    }
    
    public LambertConformalConicEllipse() {
        this(23.0, -96.0, 29.5, 45.5, 0.0, 0.0, new Earth(6378137.0, 0.0, 298.257222101));
    }
    
    public LambertConformalConicEllipse(final double lat0, final double lon0, final double par1, final double par2, final double falseEasting, final double falseNorthing, final Earth earth) {
        this.name = "LambertConformalConicEllipse";
        this.lat0deg = lat0;
        this.lon0deg = lon0;
        this.lat0rad = Math.toRadians(lat0);
        this.lon0rad = Math.toRadians(lat0);
        this.par1deg = par1;
        this.par2deg = par2;
        this.par1rad = Math.toRadians(par1);
        this.par2rad = Math.toRadians(par2);
        this.falseEasting = falseEasting;
        this.falseNorthing = falseNorthing;
        this.earth = earth;
        this.e = earth.getEccentricity();
        this.es = earth.getEccentricitySquared();
        this.isSpherical = (this.e == 0.0);
        this.totalScale = earth.getMajor() * 0.001;
        this.initialize();
        this.addParameter("grid_mapping_name", "lambert_conformal_conic");
        this.addParameter("latitude_of_projection_origin", lat0);
        this.addParameter("longitude_of_central_meridian", lon0);
        if (par2 == par1) {
            this.addParameter("standard_parallel", par1);
        }
        else {
            final double[] data = { par1, par2 };
            this.addParameter(new Parameter("standard_parallel", data));
        }
        if (falseEasting != 0.0 || falseNorthing != 0.0) {
            this.addParameter("false_easting", falseEasting);
            this.addParameter("false_northing", falseNorthing);
            this.addParameter("units", "km");
        }
        this.addParameter("semi_major_axis", earth.getMajor());
        this.addParameter("inverse_flattening", 1.0 / earth.getFlattening());
    }
    
    private void initialize() {
        if (this.par1rad == 0.0) {
            final double lat0rad = this.lat0rad;
            this.par2rad = lat0rad;
            this.par1rad = lat0rad;
        }
        if (Math.abs(this.par1rad + this.par2rad) < 1.0E-10) {
            throw new IllegalArgumentException("par1rad + par2rad < TOL");
        }
        double sinphi = Math.sin(this.par1rad);
        final double cosphi = Math.cos(this.par1rad);
        final boolean isSecant = Math.abs(this.par1rad - this.par2rad) >= 1.0E-10;
        this.n = sinphi;
        if (!this.isSpherical) {
            final double m1 = MapMath.msfn(sinphi, cosphi, this.es);
            final double ml1 = MapMath.tsfn(this.par1rad, sinphi, this.e);
            if (isSecant) {
                this.n = Math.log(m1 / MapMath.msfn(sinphi = Math.sin(this.par2rad), Math.cos(this.par2rad), this.es));
                this.n /= Math.log(ml1 / MapMath.tsfn(this.par2rad, sinphi, this.e));
            }
            final double n = m1 * Math.pow(ml1, -this.n) / this.n;
            this.rho0 = n;
            this.c = n;
            this.rho0 *= ((Math.abs(Math.abs(this.lat0rad) - 1.5707963267948966) < 1.0E-10) ? 0.0 : Math.pow(MapMath.tsfn(this.lat0rad, Math.sin(this.lat0rad), this.e), this.n));
        }
        else {
            if (isSecant) {
                this.n = Math.log(cosphi / Math.cos(this.par2rad)) / Math.log(Math.tan(0.7853981633974483 + 0.5 * this.par2rad) / Math.tan(0.7853981633974483 + 0.5 * this.par1rad));
            }
            this.c = cosphi * Math.pow(Math.tan(0.7853981633974483 + 0.5 * this.par1rad), this.n) / this.n;
            this.rho0 = ((Math.abs(Math.abs(this.lat0rad) - 1.5707963267948966) < 1.0E-10) ? 0.0 : (this.c * Math.pow(Math.tan(0.7853981633974483 + 0.5 * this.lat0rad), -this.n)));
        }
    }
    
    @Override
    public Object clone() {
        final LambertConformalConicEllipse cl = (LambertConformalConicEllipse)super.clone();
        cl.earth = this.earth;
        return cl;
    }
    
    @Override
    public boolean equals(final Object proj) {
        if (!(proj instanceof LambertConformalConicEllipse)) {
            return false;
        }
        final LambertConformalConicEllipse oo = (LambertConformalConicEllipse)proj;
        return this.getParallelOne() == oo.getParallelOne() && this.getParallelTwo() == oo.getParallelTwo() && this.getOriginLat() == oo.getOriginLat() && this.getOriginLon() == oo.getOriginLon() && this.earth.equals(oo.earth);
    }
    
    public Earth getEarth() {
        return this.earth;
    }
    
    public double getParallelTwo() {
        return this.par2deg;
    }
    
    public double getParallelOne() {
        return this.par1deg;
    }
    
    public double getOriginLon() {
        return this.lon0deg;
    }
    
    public double getOriginLat() {
        return this.lat0deg;
    }
    
    public double getFalseEasting() {
        return this.falseEasting;
    }
    
    public double getFalseNorthing() {
        return this.falseNorthing;
    }
    
    @Override
    public String getProjectionTypeLabel() {
        return "Lambert Conformal Conic Ellipsoidal Earth";
    }
    
    @Override
    public String paramsToString() {
        final Formatter f = new Formatter();
        f.format("origin lat,lon=%f,%f parellels=%f,%f earth=%s", this.lat0deg, this.lon0deg, this.par1deg, this.par2deg, this.earth);
        return f.toString();
    }
    
    @Override
    public boolean crossSeam(final ProjectionPoint pt1, final ProjectionPoint pt2) {
        return ProjectionPointImpl.isInfinite(pt1) || ProjectionPointImpl.isInfinite(pt2);
    }
    
    private double computeTheta(final double lon) {
        final double dlon = LatLonPointImpl.lonNormal(lon - this.lon0deg);
        return this.n * Math.toRadians(dlon);
    }
    
    @Override
    public ProjectionPoint latLonToProj(final LatLonPoint latLon, final ProjectionPointImpl result) {
        final double fromLat = Math.toRadians(latLon.getLatitude());
        final double theta = this.computeTheta(latLon.getLongitude());
        double rho = 0.0;
        if (Math.abs(Math.abs(fromLat) - 1.5707963267948966) >= 1.0E-10) {
            double term;
            if (this.isSpherical) {
                term = Math.pow(Math.tan(0.7853981633974483 + 0.5 * fromLat), -this.n);
            }
            else {
                term = Math.pow(MapMath.tsfn(fromLat, Math.sin(fromLat), this.e), this.n);
            }
            rho = this.c * term;
        }
        final double toX = rho * Math.sin(theta);
        final double toY = this.rho0 - rho * Math.cos(theta);
        result.setLocation(this.totalScale * toX + this.falseEasting, this.totalScale * toY + this.falseNorthing);
        return result;
    }
    
    @Override
    public LatLonPoint projToLatLon(final ProjectionPoint world, final LatLonPointImpl result) {
        double fromX = (world.getX() - this.falseEasting) / this.totalScale;
        double fromY = (world.getY() - this.falseNorthing) / this.totalScale;
        fromY = this.rho0 - fromY;
        double rho = MapMath.distance(fromX, fromY);
        double toLat;
        double toLon;
        if (rho != 0.0) {
            if (this.n < 0.0) {
                rho = -rho;
                fromX = -fromX;
                fromY = -fromY;
            }
            if (this.isSpherical) {
                toLat = 2.0 * Math.atan(Math.pow(this.c / rho, 1.0 / this.n)) - 1.5707963267948966;
            }
            else {
                toLat = MapMath.phi2(Math.pow(rho / this.c, 1.0 / this.n), this.e);
            }
            toLon = Math.atan2(fromX, fromY) / this.n;
        }
        else {
            toLon = 0.0;
            toLat = ((this.n > 0.0) ? 1.5707963267948966 : -1.5707963267948966);
        }
        result.setLatitude(Math.toDegrees(toLat));
        result.setLongitude(Math.toDegrees(toLon) + this.lon0deg);
        return result;
    }
    
    private static void toProj(final ProjectionImpl p, final double lat, final double lon) {
        System.out.printf("lon,lat = %f %f%n", lon, lat);
        final ProjectionPoint pt = p.latLonToProj(lat, lon);
        System.out.printf("x,y     = %f %f%n", pt.getX(), pt.getY());
        final LatLonPoint ll = p.projToLatLon(pt);
        System.out.printf("lon,lat = %f %f%n%n", ll.getLongitude(), ll.getLatitude());
    }
    
    private static void fromProj(final ProjectionImpl p, final double x, final double y) {
        System.out.printf("x,y     = %f %f%n", x, y);
        final LatLonPoint ll = p.projToLatLon(x, y);
        System.out.printf("lon,lat = %f %f%n", ll.getLongitude(), ll.getLatitude());
        final ProjectionPoint pt = p.latLonToProj(ll);
        System.out.printf("x,y     = %f %f%n%n", pt.getX(), pt.getY());
    }
    
    public static void main(final String[] args) {
        final LambertConformalConicEllipse a = new LambertConformalConicEllipse(23.0, -96.0, 29.5, 45.5, 0.0, 0.0, new Earth(6378137.0, 0.0, 298.257222101));
        System.out.printf("proj = %s %s%n%n", a.getName(), a.paramsToString());
        fromProj(a, 5747.0, 13470.0);
    }
}
