// 
// Decompiled by Procyon v0.5.36
// 

package ucar.unidata.geoloc.projection.proj4;

import ucar.unidata.geoloc.LatLonPoint;
import ucar.unidata.geoloc.LatLonPointImpl;
import ucar.unidata.geoloc.ProjectionPointImpl;
import ucar.unidata.geoloc.ProjectionPoint;
import java.util.Formatter;
import ucar.unidata.util.Parameter;
import ucar.unidata.geoloc.Earth;
import ucar.unidata.geoloc.ProjectionImpl;

public class AlbersEqualAreaEllipse extends ProjectionImpl
{
    private static final double EPS10 = 1.0E-10;
    private static final double TOL7 = 1.0E-7;
    private static final int N_ITER = 15;
    private static final double EPSILON = 1.0E-7;
    private static final double TOL = 1.0E-10;
    private double lat0deg;
    private double lon0deg;
    private double lat0rad;
    private double lon0rad;
    private double par1deg;
    private double par2deg;
    private double phi1;
    private double phi2;
    private double falseEasting;
    private double falseNorthing;
    private Earth earth;
    private double e;
    private double es;
    private double one_es;
    private double totalScale;
    private double ec;
    private double n;
    private double c;
    private double dd;
    private double n2;
    private double rho0;
    private boolean isSpherical;
    
    @Override
    public ProjectionImpl constructCopy() {
        return new AlbersEqualAreaEllipse(this.getOriginLat(), this.getOriginLon(), this.getParallelOne(), this.getParallelTwo(), this.getFalseEasting(), this.getFalseNorthing(), this.getEarth());
    }
    
    public AlbersEqualAreaEllipse() {
        this(23.0, -96.0, 29.5, 45.5, 0.0, 0.0, new Earth(6378137.0, 0.0, 298.257222101));
    }
    
    public AlbersEqualAreaEllipse(final double lat0, final double lon0, final double par1, final double par2, final double falseEasting, final double falseNorthing, final Earth earth) {
        this.name = "AlbersEqualAreaEllipse";
        this.lat0deg = lat0;
        this.lon0deg = lon0;
        this.lat0rad = Math.toRadians(lat0);
        this.lon0rad = Math.toRadians(lat0);
        this.par1deg = par1;
        this.par2deg = par2;
        this.phi1 = Math.toRadians(par1);
        this.phi2 = Math.toRadians(par2);
        this.falseEasting = falseEasting;
        this.falseNorthing = falseNorthing;
        this.earth = earth;
        this.e = earth.getEccentricity();
        this.es = earth.getEccentricitySquared();
        this.isSpherical = (this.e == 0.0);
        this.one_es = 1.0 - this.es;
        this.totalScale = earth.getMajor() * 0.001;
        this.precalculate();
        this.addParameter("grid_mapping_name", "albers_conical_equal_area");
        this.addParameter("latitude_of_projection_origin", lat0);
        this.addParameter("longitude_of_central_meridian", lon0);
        if (par2 == par1) {
            this.addParameter("standard_parallel", par1);
        }
        else {
            final double[] data = { par1, par2 };
            this.addParameter(new Parameter("standard_parallel", data));
        }
        if (falseEasting != 0.0 || falseNorthing != 0.0) {
            this.addParameter("false_easting", falseEasting);
            this.addParameter("false_northing", falseNorthing);
            this.addParameter("units", "km");
        }
        this.addParameter("semi_major_axis", earth.getMajor());
        this.addParameter("inverse_flattening", 1.0 / earth.getFlattening());
    }
    
    private void precalculate() {
        if (Math.abs(this.phi1 + this.phi2) < 1.0E-10) {
            throw new IllegalArgumentException("Math.abs(par1 + par2) < 1.e-10");
        }
        double sinphi = Math.sin(this.phi1);
        this.n = sinphi;
        double cosphi = Math.cos(this.phi1);
        final boolean secant = Math.abs(this.phi1 - this.phi2) >= 1.0E-10;
        if (!this.isSpherical) {
            if (MapMath.enfn(this.es) == null) {
                throw new IllegalArgumentException("0");
            }
            final double m1 = MapMath.msfn(sinphi, cosphi, this.es);
            final double ml1 = MapMath.qsfn(sinphi, this.e, this.one_es);
            if (secant) {
                sinphi = Math.sin(this.phi2);
                cosphi = Math.cos(this.phi2);
                final double m2 = MapMath.msfn(sinphi, cosphi, this.es);
                final double ml2 = MapMath.qsfn(sinphi, this.e, this.one_es);
                this.n = (m1 * m1 - m2 * m2) / (ml2 - ml1);
            }
            this.ec = 1.0 - 0.5 * this.one_es * Math.log((1.0 - this.e) / (1.0 + this.e)) / this.e;
            this.c = m1 * m1 + this.n * ml1;
            this.dd = 1.0 / this.n;
            this.rho0 = this.dd * Math.sqrt(this.c - this.n * MapMath.qsfn(Math.sin(this.lat0rad), this.e, this.one_es));
        }
        else {
            if (secant) {
                this.n = 0.5 * (this.n + Math.sin(this.phi2));
            }
            this.n2 = this.n + this.n;
            this.c = cosphi * cosphi + this.n2 * sinphi;
            this.dd = 1.0 / this.n;
            this.rho0 = this.dd * Math.sqrt(this.c - this.n2 * Math.sin(this.lat0rad));
        }
    }
    
    @Override
    public Object clone() {
        final AlbersEqualAreaEllipse cl = (AlbersEqualAreaEllipse)super.clone();
        cl.earth = this.earth;
        return cl;
    }
    
    @Override
    public boolean equals(final Object proj) {
        if (!(proj instanceof AlbersEqualAreaEllipse)) {
            return false;
        }
        final AlbersEqualAreaEllipse oo = (AlbersEqualAreaEllipse)proj;
        return this.getParallelOne() == oo.getParallelOne() && this.getParallelTwo() == oo.getParallelTwo() && this.getOriginLat() == oo.getOriginLat() && this.getOriginLon() == oo.getOriginLon() && this.earth.equals(oo.earth);
    }
    
    public Earth getEarth() {
        return this.earth;
    }
    
    public double getParallelTwo() {
        return this.par2deg;
    }
    
    public double getParallelOne() {
        return this.par1deg;
    }
    
    public double getOriginLon() {
        return this.lon0deg;
    }
    
    public double getOriginLat() {
        return this.lat0deg;
    }
    
    public double getFalseEasting() {
        return this.falseEasting;
    }
    
    public double getFalseNorthing() {
        return this.falseNorthing;
    }
    
    @Override
    public String getProjectionTypeLabel() {
        return "Albers Equal Area Ellipsoidal Earth";
    }
    
    @Override
    public String paramsToString() {
        final Formatter f = new Formatter();
        f.format("origin lat,lon=%f,%f parellels=%f,%f earth=%s", this.lat0deg, this.lon0deg, this.par1deg, this.par2deg, this.earth);
        return f.toString();
    }
    
    @Override
    public boolean crossSeam(final ProjectionPoint pt1, final ProjectionPoint pt2) {
        return ProjectionPointImpl.isInfinite(pt1) || ProjectionPointImpl.isInfinite(pt2);
    }
    
    private double computeTheta(final double lon) {
        final double dlon = LatLonPointImpl.lonNormal(lon - this.lon0deg);
        return this.n * Math.toRadians(dlon);
    }
    
    @Override
    public ProjectionPoint latLonToProj(final LatLonPoint latLon, final ProjectionPointImpl result) {
        final double fromLat = Math.toRadians(latLon.getLatitude());
        final double theta = this.computeTheta(latLon.getLongitude());
        final double term = this.isSpherical ? (this.n2 * Math.sin(fromLat)) : (this.n * MapMath.qsfn(Math.sin(fromLat), this.e, this.one_es));
        double rho = this.c - term;
        if (rho < 0.0) {
            throw new RuntimeException("F");
        }
        rho = this.dd * Math.sqrt(rho);
        final double toX = rho * Math.sin(theta);
        final double toY = this.rho0 - rho * Math.cos(theta);
        result.setLocation(this.totalScale * toX + this.falseEasting, this.totalScale * toY + this.falseNorthing);
        return result;
    }
    
    @Override
    public LatLonPoint projToLatLon(final ProjectionPoint world, final LatLonPointImpl result) {
        double fromX = (world.getX() - this.falseEasting) / this.totalScale;
        double fromY = (world.getY() - this.falseNorthing) / this.totalScale;
        fromY = this.rho0 - fromY;
        double rho = MapMath.distance(fromX, fromY);
        double toLon;
        double toLat;
        if (rho == 0.0) {
            toLon = 0.0;
            toLat = ((this.n > 0.0) ? 1.5707963267948966 : -1.5707963267948966);
        }
        else {
            if (this.n < 0.0) {
                rho = -rho;
                fromX = -fromX;
                fromY = -fromY;
            }
            double lpphi = rho / this.dd;
            if (!this.isSpherical) {
                lpphi = (this.c - lpphi * lpphi) / this.n;
                if (Math.abs(this.ec - Math.abs(lpphi)) > 1.0E-7) {
                    if (Math.abs(lpphi) > 2.0) {
                        throw new IllegalArgumentException("AlbersEqualAreaEllipse x,y=" + world);
                    }
                    lpphi = phi1_(lpphi, this.e, this.one_es);
                    if (lpphi == Double.MAX_VALUE) {
                        throw new RuntimeException("I");
                    }
                }
                else {
                    lpphi = ((lpphi < 0.0) ? -1.5707963267948966 : 1.5707963267948966);
                }
            }
            else {
                lpphi = (this.c - lpphi * lpphi) / this.n2;
                if (Math.abs(lpphi) <= 1.0) {
                    lpphi = Math.asin(lpphi);
                }
                else {
                    lpphi = ((lpphi < 0.0) ? -1.5707963267948966 : 1.5707963267948966);
                }
            }
            toLon = Math.atan2(fromX, fromY) / this.n;
            toLat = lpphi;
        }
        result.setLatitude(Math.toDegrees(toLat));
        result.setLongitude(Math.toDegrees(toLon) + this.lon0deg);
        return result;
    }
    
    private static double phi1_(final double qs, final double Te, final double Tone_es) {
        double phi = Math.asin(0.5 * qs);
        if (Te < 1.0E-7) {
            return phi;
        }
        int countIter = 15;
        double dphi;
        do {
            final double sinpi = Math.sin(phi);
            final double cospi = Math.cos(phi);
            final double con = Te * sinpi;
            final double com = 1.0 - con * con;
            dphi = 0.5 * com * com / cospi * (qs / Tone_es - sinpi / com + 0.5 / Te * Math.log((1.0 - con) / (1.0 + con)));
            phi += dphi;
        } while (Math.abs(dphi) > 1.0E-10 && --countIter != 0);
        return (countIter != 0) ? phi : Double.MAX_VALUE;
    }
    
    private static void toProj(final ProjectionImpl p, final double lat, final double lon) {
        System.out.printf("lon,lat = %f %f%n", lon, lat);
        final ProjectionPoint pt = p.latLonToProj(lat, lon);
        System.out.printf("x,y     = %f %f%n", pt.getX(), pt.getY());
        final LatLonPoint ll = p.projToLatLon(pt);
        System.out.printf("lon,lat = %f %f%n%n", ll.getLongitude(), ll.getLatitude());
    }
    
    private static void fromProj(final ProjectionImpl p, final double x, final double y) {
        System.out.printf("x,y     = %f %f%n", x, y);
        final LatLonPoint ll = p.projToLatLon(x, y);
        System.out.printf("lon,lat = %f %f%n", ll.getLongitude(), ll.getLatitude());
        final ProjectionPoint pt = p.latLonToProj(ll);
        System.out.printf("x,y     = %f %f%n%n", pt.getX(), pt.getY());
    }
    
    public static void main(final String[] args) {
        final AlbersEqualAreaEllipse a = new AlbersEqualAreaEllipse(23.0, -96.0, 29.5, 45.5, 0.0, 0.0, new Earth(6378137.0, 0.0, 298.257222101));
        System.out.printf("proj = %s %s%n%n", a.getName(), a.paramsToString());
        fromProj(a, 5747.0, 13470.0);
    }
}
