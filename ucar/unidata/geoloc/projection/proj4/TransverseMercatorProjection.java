// 
// Decompiled by Procyon v0.5.36
// 

package ucar.unidata.geoloc.projection.proj4;

import ucar.unidata.geoloc.LatLonPointImpl;
import ucar.unidata.geoloc.ProjectionPoint;
import ucar.unidata.geoloc.ProjectionPointImpl;
import ucar.unidata.geoloc.LatLonPoint;
import java.util.Formatter;
import java.awt.geom.Point2D;
import ucar.unidata.geoloc.Earth;
import ucar.unidata.geoloc.ProjectionImpl;

public class TransverseMercatorProjection extends ProjectionImpl
{
    private static final double FC1 = 1.0;
    private static final double FC2 = 0.5;
    private static final double FC3 = 0.16666666666666666;
    private static final double FC4 = 0.08333333333333333;
    private static final double FC5 = 0.05;
    private static final double FC6 = 0.03333333333333333;
    private static final double FC7 = 0.023809523809523808;
    private static final double FC8 = 0.017857142857142856;
    private double esp;
    private double ml0;
    private double[] en;
    private double projectionLatitude;
    private double projectionLongitude;
    private double scaleFactor;
    private double falseEasting;
    private double falseNorthing;
    Earth ellipsoid;
    private double e;
    private double es;
    private double one_es;
    private double totalScale;
    private boolean spherical;
    
    public TransverseMercatorProjection() {
        this.ellipsoid = new Earth();
        this.projectionLatitude = Math.toRadians(0.0);
        this.projectionLongitude = Math.toRadians(0.0);
        this.initialize();
    }
    
    public TransverseMercatorProjection(final Earth ellipsoid, final double lon_0_deg, final double lat_0_deg, final double k, final double falseEast, final double falseNorth) {
        this.ellipsoid = ellipsoid;
        this.projectionLongitude = Math.toRadians(lon_0_deg);
        this.projectionLatitude = Math.toRadians(lat_0_deg);
        this.scaleFactor = k;
        this.falseEasting = falseEast;
        this.falseNorthing = falseNorth;
        this.initialize();
        this.addParameter("grid_mapping_name", "transverse_mercator");
        this.addParameter("longitude_of_central_meridian", lon_0_deg);
        this.addParameter("latitude_of_projection_origin", lat_0_deg);
        this.addParameter("scale_factor_at_central_meridian", this.scaleFactor);
        if (this.falseEasting != 0.0 || this.falseNorthing != 0.0) {
            this.addParameter("false_easting", this.falseEasting);
            this.addParameter("false_northing", this.falseNorthing);
            this.addParameter("units", "km");
        }
        this.addParameter("semi_major_axis", ellipsoid.getMajor());
        this.addParameter("inverse_flattening", 1.0 / ellipsoid.getFlattening());
    }
    
    @Override
    public Object clone() {
        return new TransverseMercatorProjection(this.ellipsoid, Math.toDegrees(this.projectionLongitude), Math.toDegrees(this.projectionLatitude), this.scaleFactor, this.falseEasting, this.falseNorthing);
    }
    
    public boolean isRectilinear() {
        return false;
    }
    
    public void initialize() {
        this.e = this.ellipsoid.getEccentricity();
        this.es = this.ellipsoid.getEccentricitySquared();
        this.spherical = (this.e == 0.0);
        this.one_es = 1.0 - this.es;
        this.totalScale = this.ellipsoid.getMajor();
        if (this.spherical) {
            this.esp = this.scaleFactor;
            this.ml0 = 0.5 * this.esp;
        }
        else {
            this.en = MapMath.enfn(this.es);
            this.ml0 = MapMath.mlfn(this.projectionLatitude, Math.sin(this.projectionLatitude), Math.cos(this.projectionLatitude), this.en);
            this.esp = this.es / (1.0 - this.es);
        }
    }
    
    public int getRowFromNearestParallel(final double latitude) {
        final int degrees = (int)MapMath.radToDeg(MapMath.normalizeLatitude(latitude));
        if (degrees < -80 || degrees > 84) {
            return 0;
        }
        if (degrees > 80) {
            return 24;
        }
        return (degrees + 80) / 8 + 3;
    }
    
    public int getZoneFromNearestMeridian(final double longitude) {
        int zone = (int)Math.floor((MapMath.normalizeLongitude(longitude) + 3.141592653589793) * 30.0 / 3.141592653589793) + 1;
        if (zone < 1) {
            zone = 1;
        }
        else if (zone > 60) {
            zone = 60;
        }
        return zone;
    }
    
    public void setUTMZone(int zone) {
        --zone;
        this.projectionLongitude = (zone + 0.5) * 3.141592653589793 / 30.0 - 3.141592653589793;
        this.projectionLatitude = 0.0;
        this.scaleFactor = 0.9996;
        this.falseEasting = 500000.0;
        this.initialize();
    }
    
    public Point2D.Double project(final double lplam, final double lpphi, final Point2D.Double xy) {
        if (this.spherical) {
            final double cosphi = Math.cos(lpphi);
            final double b = cosphi * Math.sin(lplam);
            xy.x = this.ml0 * this.scaleFactor * Math.log((1.0 + b) / (1.0 - b));
            double ty = cosphi * Math.cos(lplam) / Math.sqrt(1.0 - b * b);
            ty = MapMath.acos(ty);
            if (lpphi < 0.0) {
                ty = -ty;
            }
            xy.y = this.esp * (ty - this.projectionLatitude);
        }
        else {
            final double sinphi = Math.sin(lpphi);
            final double cosphi2 = Math.cos(lpphi);
            double t = (Math.abs(cosphi2) > 1.0E-10) ? (sinphi / cosphi2) : 0.0;
            t *= t;
            double al = cosphi2 * lplam;
            final double als = al * al;
            al /= Math.sqrt(1.0 - this.es * sinphi * sinphi);
            final double n = this.esp * cosphi2 * cosphi2;
            xy.x = this.scaleFactor * al * (1.0 + 0.16666666666666666 * als * (1.0 - t + n + 0.05 * als * (5.0 + t * (t - 18.0) + n * (14.0 - 58.0 * t) + 0.023809523809523808 * als * (61.0 + t * (t * (179.0 - t) - 479.0)))));
            xy.y = this.scaleFactor * (MapMath.mlfn(lpphi, sinphi, cosphi2, this.en) - this.ml0 + sinphi * al * lplam * 0.5 * (1.0 + 0.08333333333333333 * als * (5.0 - t + n * (9.0 + 4.0 * n) + 0.03333333333333333 * als * (61.0 + t * (t - 58.0) + n * (270.0 - 330.0 * t) + 0.017857142857142856 * als * (1385.0 + t * (t * (543.0 - t) - 3111.0))))));
        }
        return xy;
    }
    
    public Point2D.Double projectInverse(final double x, final double y, final Point2D.Double out) {
        if (this.spherical) {
            double h = Math.exp(x / this.scaleFactor);
            final double g = 0.5 * (h - 1.0 / h);
            h = Math.cos(this.projectionLatitude + y / this.scaleFactor);
            out.y = MapMath.asin(Math.sqrt((1.0 - h * h) / (1.0 + g * g)));
            if (y < 0.0) {
                out.y = -out.y;
            }
            out.x = Math.atan2(g, h);
        }
        else {
            out.y = MapMath.inv_mlfn(this.ml0 + y / this.scaleFactor, this.es, this.en);
            if (Math.abs(y) >= 1.5707963267948966) {
                out.y = ((y < 0.0) ? -1.5707963267948966 : 1.5707963267948966);
                out.x = 0.0;
            }
            else {
                final double sinphi = Math.sin(out.y);
                final double cosphi = Math.cos(out.y);
                double t = (Math.abs(cosphi) > 1.0E-10) ? (sinphi / cosphi) : 0.0;
                final double n = this.esp * cosphi * cosphi;
                double con;
                final double d = x * Math.sqrt(con = 1.0 - this.es * sinphi * sinphi) / this.scaleFactor;
                con *= t;
                t *= t;
                final double ds = d * d;
                out.y -= con * ds / (1.0 - this.es) * 0.5 * (1.0 - ds * 0.08333333333333333 * (5.0 + t * (3.0 - 9.0 * n) + n * (1.0 - 4.0 * n) - ds * 0.03333333333333333 * (61.0 + t * (90.0 - 252.0 * n + 45.0 * t) + 46.0 * n - ds * 0.017857142857142856 * (1385.0 + t * (3633.0 + t * (4095.0 + 1574.0 * t))))));
                out.x = d * (1.0 - ds * 0.16666666666666666 * (1.0 + 2.0 * t + n - ds * 0.05 * (5.0 + t * (28.0 + 24.0 * t + 8.0 * n) + 6.0 * n - ds * 0.023809523809523808 * (61.0 + t * (662.0 + t * (1320.0 + 720.0 * t)))))) / cosphi;
            }
        }
        return out;
    }
    
    public boolean hasInverse() {
        return true;
    }
    
    @Override
    public String getProjectionTypeLabel() {
        return "Transverse Mercator Ellipsoidal Earth";
    }
    
    @Override
    public ProjectionImpl constructCopy() {
        return (ProjectionImpl)this.clone();
    }
    
    @Override
    public String paramsToString() {
        final Formatter f = new Formatter();
        f.format("origin lat,lon=%f,%f scale=%f earth=%s falseEast/North=%f,%f", Math.toDegrees(this.projectionLatitude), Math.toDegrees(this.projectionLongitude), this.scaleFactor, this.ellipsoid, this.falseEasting, this.falseNorthing);
        return f.toString();
    }
    
    @Override
    public ProjectionPoint latLonToProj(final LatLonPoint latLon, final ProjectionPointImpl destPoint) {
        final double fromLat = Math.toRadians(latLon.getLatitude());
        double theta = Math.toRadians(latLon.getLongitude());
        if (this.projectionLongitude != 0.0) {
            theta = MapMath.normalizeLongitude(theta - this.projectionLongitude);
        }
        final Point2D.Double res = this.project(theta, fromLat, new Point2D.Double());
        destPoint.setLocation(this.totalScale * res.x + this.falseEasting, this.totalScale * res.y + this.falseNorthing);
        return destPoint;
    }
    
    @Override
    public LatLonPoint projToLatLon(final ProjectionPoint world, final LatLonPointImpl result) {
        final double fromX = (world.getX() - this.falseEasting) / this.totalScale;
        final double fromY = (world.getY() - this.falseNorthing) / this.totalScale;
        final Point2D.Double dst = this.projectInverse(fromX, fromY, new Point2D.Double());
        if (dst.x < -3.141592653589793) {
            dst.x = -3.141592653589793;
        }
        else if (dst.x > 3.141592653589793) {
            dst.x = 3.141592653589793;
        }
        if (this.projectionLongitude != 0.0) {
            dst.x = MapMath.normalizeLongitude(dst.x + this.projectionLongitude);
        }
        result.setLongitude(Math.toDegrees(dst.x));
        result.setLatitude(Math.toDegrees(dst.y));
        return result;
    }
    
    @Override
    public boolean crossSeam(final ProjectionPoint pt1, final ProjectionPoint pt2) {
        if (ProjectionPointImpl.isInfinite(pt1) || ProjectionPointImpl.isInfinite(pt2)) {
            return true;
        }
        final double y1 = pt1.getY() - this.falseNorthing;
        final double y2 = pt2.getY() - this.falseNorthing;
        return y1 * y2 < 0.0 && Math.abs(y1 - y2) > 2.0 * this.ellipsoid.getMajor();
    }
    
    @Override
    public boolean equals(final Object proj) {
        if (!(proj instanceof TransverseMercatorProjection)) {
            return false;
        }
        final TransverseMercatorProjection oo = (TransverseMercatorProjection)proj;
        return this.projectionLatitude == oo.projectionLatitude && this.projectionLongitude == oo.projectionLongitude && this.scaleFactor == oo.scaleFactor && this.falseEasting == oo.falseEasting && this.falseNorthing == oo.falseNorthing && this.ellipsoid.equals(oo.ellipsoid);
    }
    
    @Override
    public int hashCode() {
        int hash = 5;
        hash = 97 * hash + (int)(Double.doubleToLongBits(this.projectionLatitude) ^ Double.doubleToLongBits(this.projectionLatitude) >>> 32);
        hash = 97 * hash + (int)(Double.doubleToLongBits(this.projectionLongitude) ^ Double.doubleToLongBits(this.projectionLongitude) >>> 32);
        hash = 97 * hash + (int)(Double.doubleToLongBits(this.scaleFactor) ^ Double.doubleToLongBits(this.scaleFactor) >>> 32);
        hash = 97 * hash + (int)(Double.doubleToLongBits(this.falseEasting) ^ Double.doubleToLongBits(this.falseEasting) >>> 32);
        hash = 97 * hash + (int)(Double.doubleToLongBits(this.falseNorthing) ^ Double.doubleToLongBits(this.falseNorthing) >>> 32);
        hash = 97 * hash + ((this.ellipsoid != null) ? this.ellipsoid.hashCode() : 0);
        return hash;
    }
    
    private static void test(final ProjectionImpl proj, final double[] lat, final double[] lon) {
        final double[] x = new double[lat.length];
        final double[] y = new double[lat.length];
        for (int i = 0; i < lat.length; ++i) {
            final LatLonPoint lp = new LatLonPointImpl(lat[i], lon[i]);
            final ProjectionPointImpl p = (ProjectionPointImpl)proj.latLonToProj(lp, new ProjectionPointImpl());
            System.out.println(lp.getLatitude() + ", " + lp.getLongitude() + ": " + p.x + ", " + p.y);
            x[i] = p.x;
            y[i] = p.y;
        }
        for (int i = 0; i < lat.length; ++i) {
            final ProjectionPointImpl p2 = new ProjectionPointImpl(x[i], y[i]);
            final LatLonPointImpl lp2 = (LatLonPointImpl)proj.projToLatLon(p2);
            if (Math.abs(lp2.getLatitude() - lat[i]) > 1.0E-5 || Math.abs(lp2.getLongitude() - lon[i]) > 1.0E-5) {
                if (Math.abs(lp2.getLatitude()) <= 89.99 || Math.abs(lp2.getLatitude() - lat[i]) >= 1.0E-5) {
                    System.err.print("ERROR:");
                }
            }
            System.out.println("reverse:" + p2.x + ", " + p2.y + ": " + lp2.getLatitude() + ", " + lp2.getLongitude());
        }
    }
    
    public static void main(final String[] args) {
        final Earth e = new Earth(6378.137, 6356.7523142, 0.0);
        ProjectionImpl proj = new TransverseMercatorProjection(e, 9.0, 0.0, 0.9996, 500.0, 0.0);
        final double[] lat = { 60.0, 90.0, 60.0 };
        final double[] lon = { 0.0, 0.0, 10.0 };
        test(proj, lat, lon);
        proj = new TransverseMercatorProjection(e, 9.0, 0.0, 0.9996, 500.0, 0.0);
        test(proj, lat, lon);
    }
}
