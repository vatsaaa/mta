// 
// Decompiled by Procyon v0.5.36
// 

package ucar.unidata.geoloc.projection;

import ucar.unidata.geoloc.ProjectionPoint;
import ucar.unidata.geoloc.ProjectionPointImpl;
import ucar.unidata.geoloc.LatLonPoint;
import ucar.unidata.geoloc.Earth;
import ucar.unidata.geoloc.LatLonPointImpl;
import ucar.unidata.geoloc.ProjectionImpl;

public class FlatEarth extends ProjectionImpl
{
    private double rotAngle;
    private double lat0;
    private double lon0;
    private double cosRot;
    private double sinRot;
    private LatLonPointImpl origin;
    private boolean spherical;
    private double radius;
    
    @Override
    public ProjectionImpl constructCopy() {
        return new FlatEarth(this.getOriginLat(), this.getOriginLon(), this.getRotationAngle());
    }
    
    public FlatEarth() {
        this(0.0, 0.0, 0.0);
    }
    
    public FlatEarth(final double lat0, final double lon0, final double rotAngle) {
        this.spherical = false;
        this.radius = Earth.getRadius() * 0.001;
        this.lat0 = Math.toRadians(lat0);
        this.lon0 = Math.toRadians(lon0);
        this.rotAngle = Math.toRadians(rotAngle);
        this.origin = new LatLonPointImpl(lat0, lon0);
        this.precalculate();
        this.addParameter("grid_mapping_name", "flat_earth");
        this.addParameter("latitude_of_projection_origin", lat0);
        this.addParameter("longitude_of_projection_origin", lon0);
        this.addParameter("rotationAngle", rotAngle);
    }
    
    public FlatEarth(final double lat0, final double lon0) {
        this.spherical = false;
        this.radius = Earth.getRadius() * 0.001;
        this.lat0 = Math.toRadians(lat0);
        this.lon0 = Math.toRadians(lon0);
        this.rotAngle = Math.toRadians(0.0);
        this.origin = new LatLonPointImpl(lat0, lon0);
        this.precalculate();
        this.addParameter("grid_mapping_name", "flat_earth");
        this.addParameter("latitude_of_projection_origin", lat0);
        this.addParameter("longitude_of_projection_origin", lon0);
        this.addParameter("rotationAngle", this.rotAngle);
    }
    
    private void precalculate() {
        this.sinRot = Math.sin(this.rotAngle);
        this.cosRot = Math.cos(this.rotAngle);
    }
    
    @Override
    public Object clone() {
        final FlatEarth cl = (FlatEarth)super.clone();
        cl.origin = new LatLonPointImpl(this.getOriginLat(), this.getOriginLon());
        return cl;
    }
    
    @Override
    public boolean equals(final Object proj) {
        if (!(proj instanceof FlatEarth)) {
            return false;
        }
        final FlatEarth oo = (FlatEarth)proj;
        return this.getOriginLat() == oo.getOriginLat() && this.getOriginLon() == oo.getOriginLon() && this.rotAngle == oo.rotAngle;
    }
    
    public double getOriginLon() {
        return this.origin.getLongitude();
    }
    
    public void setOriginLon(final double lon) {
        this.origin.setLongitude(lon);
        this.lon0 = Math.toRadians(lon);
        this.precalculate();
    }
    
    public double getOriginLat() {
        return this.origin.getLatitude();
    }
    
    public double getRotationAngle() {
        return this.rotAngle;
    }
    
    public void setOriginLat(final double lat) {
        this.origin.setLatitude(lat);
        this.lat0 = Math.toRadians(lat);
        this.precalculate();
    }
    
    @Override
    public String getProjectionTypeLabel() {
        return "FlatEarth";
    }
    
    @Override
    public String paramsToString() {
        return " origin " + this.origin.toString() + " rotationAngle " + this.rotAngle;
    }
    
    @Override
    public ProjectionPoint latLonToProj(final LatLonPoint latLon, final ProjectionPointImpl result) {
        double fromLat = latLon.getLatitude();
        final double fromLon = latLon.getLongitude();
        fromLat = Math.toRadians(fromLat);
        final double dy = this.radius * (fromLat - this.lat0);
        final double dx = this.radius * Math.cos(fromLat) * (Math.toRadians(fromLon) - this.lon0);
        final double toX = this.cosRot * dx - this.sinRot * dy;
        final double toY = this.sinRot * dx + this.cosRot * dy;
        result.setLocation(toX, toY);
        return result;
    }
    
    @Override
    public LatLonPoint projToLatLon(final ProjectionPoint world, final LatLonPointImpl result) {
        final double x = world.getX();
        final double y = world.getY();
        final int TOLERENCE = 1;
        final double xp = this.cosRot * x + this.sinRot * y;
        final double yp = -this.sinRot * x + this.cosRot * y;
        final double toLat = Math.toDegrees(this.lat0) + Math.toDegrees(yp / this.radius);
        final double cosl = Math.cos(Math.toRadians(toLat));
        double toLon;
        if (Math.abs(cosl) < 1.0E-6) {
            toLon = Math.toDegrees(this.lon0);
        }
        else {
            toLon = Math.toDegrees(this.lon0) + Math.toDegrees(xp / cosl / this.radius);
        }
        toLon = LatLonPointImpl.lonNormal(toLon);
        result.setLatitude(toLat);
        result.setLongitude(toLon);
        return result;
    }
    
    @Override
    public float[][] latLonToProj(final float[][] from, final float[][] to, final int latIndex, final int lonIndex) {
        final int cnt = from[0].length;
        final float[] fromLatA = from[latIndex];
        final float[] fromLonA = from[lonIndex];
        final float[] resultXA = to[0];
        final float[] resultYA = to[1];
        for (int i = 0; i < cnt; ++i) {
            double fromLat = fromLatA[i];
            final double fromLon = fromLonA[i];
            fromLat = Math.toRadians(fromLat);
            final double dy = this.radius * (fromLat - this.lat0);
            final double dx = this.radius * Math.cos(fromLat) * (Math.toRadians(fromLon) - this.lon0);
            final double toX = this.cosRot * dx - this.sinRot * dy;
            final double toY = this.sinRot * dx + this.cosRot * dy;
            resultXA[i] = (float)toX;
            resultYA[i] = (float)toY;
        }
        return to;
    }
    
    @Override
    public boolean crossSeam(final ProjectionPoint pt1, final ProjectionPoint pt2) {
        return pt1.getX() * pt2.getX() < 0.0;
    }
    
    @Override
    public float[][] projToLatLon(final float[][] from, final float[][] to) {
        final int cnt = from[0].length;
        final float[] fromXA = from[0];
        final float[] fromYA = from[1];
        final float[] toLatA = to[0];
        final float[] toLonA = to[1];
        for (int i = 0; i < cnt; ++i) {
            final double fromX = fromXA[i];
            final double fromY = fromYA[i];
            final double xp = this.cosRot * fromX + this.sinRot * fromY;
            final double yp = -this.sinRot * fromX + this.cosRot * fromY;
            final double toLat = Math.toDegrees(this.lat0) + Math.toDegrees(yp / this.radius);
            final double cosl = Math.cos(Math.toRadians(toLat));
            double toLon;
            if (Math.abs(cosl) < 1.0E-6) {
                toLon = Math.toDegrees(this.lon0);
            }
            else {
                toLon = Math.toDegrees(this.lon0) + Math.toDegrees(xp / cosl / this.radius);
            }
            toLon = LatLonPointImpl.lonNormal(toLon);
            toLatA[i] = (float)toLat;
            toLonA[i] = (float)toLon;
        }
        return to;
    }
    
    @Override
    public double[][] latLonToProj(final double[][] from, final double[][] to, final int latIndex, final int lonIndex) {
        final int cnt = from[0].length;
        final double[] fromLatA = from[latIndex];
        final double[] fromLonA = from[lonIndex];
        final double[] resultXA = to[0];
        final double[] resultYA = to[1];
        for (int i = 0; i < cnt; ++i) {
            double fromLat = fromLatA[i];
            final double fromLon = fromLonA[i];
            fromLat = Math.toRadians(fromLat);
            final double dy = this.radius * (fromLat - this.lat0);
            final double dx = this.radius * Math.cos(fromLat) * (Math.toRadians(fromLon) - this.lon0);
            final double toX = this.cosRot * dx - this.sinRot * dy;
            final double toY = this.sinRot * dx + this.cosRot * dy;
            resultXA[i] = toX;
            resultYA[i] = toY;
        }
        return to;
    }
    
    @Override
    public double[][] projToLatLon(final double[][] from, final double[][] to) {
        final int cnt = from[0].length;
        final double[] fromXA = from[0];
        final double[] fromYA = from[1];
        final double[] toLatA = to[0];
        final double[] toLonA = to[1];
        for (int i = 0; i < cnt; ++i) {
            final double fromX = fromXA[i];
            final double fromY = fromYA[i];
            final double xp = this.cosRot * fromX + this.sinRot * fromY;
            final double yp = -this.sinRot * fromX + this.cosRot * fromY;
            final double toLat = Math.toDegrees(this.lat0) + Math.toDegrees(yp / this.radius);
            final double cosl = Math.cos(Math.toRadians(toLat));
            double toLon;
            if (Math.abs(cosl) < 1.0E-6) {
                toLon = Math.toDegrees(this.lon0);
            }
            else {
                toLon = Math.toDegrees(this.lon0) + Math.toDegrees(xp / cosl / this.radius);
            }
            toLon = LatLonPointImpl.lonNormal(toLon);
            toLatA[i] = toLat;
            toLonA[i] = toLon;
        }
        return to;
    }
    
    public static void main(final String[] args) {
        final FlatEarth a = new FlatEarth(90.0, -100.0, 0.0);
        final ProjectionPointImpl p = a.latLonToProj(89.0, -101.0);
        System.out.println("proj point = " + p);
        final LatLonPoint ll = a.projToLatLon(p);
        System.out.println("ll = " + ll);
    }
}
