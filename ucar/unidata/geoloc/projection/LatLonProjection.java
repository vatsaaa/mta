// 
// Decompiled by Procyon v0.5.36
// 

package ucar.unidata.geoloc.projection;

import ucar.unidata.geoloc.LatLonRect;
import ucar.unidata.geoloc.LatLonPointImpl;
import ucar.unidata.geoloc.ProjectionPoint;
import ucar.unidata.geoloc.ProjectionPointImpl;
import ucar.unidata.geoloc.LatLonPoint;
import ucar.unidata.util.Format;
import ucar.unidata.geoloc.ProjectionRect;
import ucar.unidata.geoloc.ProjectionImpl;

public class LatLonProjection extends ProjectionImpl
{
    private double centerLon;
    private ProjectionRect projR1;
    private ProjectionRect projR2;
    private ProjectionRect[] rects;
    
    @Override
    public ProjectionImpl constructCopy() {
        return new LatLonProjection(this.getName(), this.getDefaultMapArea());
    }
    
    public LatLonProjection() {
        this("");
    }
    
    public LatLonProjection(final String name) {
        this(name, new ProjectionRect(-180.0, -90.0, 180.0, 90.0));
    }
    
    public LatLonProjection(final String name, final ProjectionRect defaultMapArea) {
        this.centerLon = 0.0;
        this.projR1 = new ProjectionRect();
        this.projR2 = new ProjectionRect();
        this.rects = new ProjectionRect[2];
        this.addParameter("grid_mapping_name", "LatLon");
        this.name = name;
        this.isLatLon = true;
        this.defaultMapArea = defaultMapArea;
    }
    
    @Override
    public String getClassName() {
        return "LatLon";
    }
    
    @Override
    public String getProjectionTypeLabel() {
        return "Lat/Lon";
    }
    
    @Override
    public String paramsToString() {
        return "Center lon:" + Format.d(this.centerLon, 3);
    }
    
    @Override
    public boolean equals(final Object p) {
        if (!(p instanceof LatLonProjection)) {
            return false;
        }
        final LatLonProjection that = (LatLonProjection)p;
        return that.defaultMapArea.equals(this.defaultMapArea) && Double.doubleToLongBits(that.centerLon) == Double.doubleToLongBits(this.centerLon);
    }
    
    @Override
    public ProjectionPoint latLonToProj(final LatLonPoint latlon, final ProjectionPointImpl result) {
        result.setLocation(LatLonPointImpl.lonNormal(latlon.getLongitude(), this.centerLon), latlon.getLatitude());
        return result;
    }
    
    @Override
    public LatLonPoint projToLatLon(final ProjectionPoint world, final LatLonPointImpl result) {
        result.setLongitude(world.getX());
        result.setLatitude(world.getY());
        return result;
    }
    
    @Override
    public float[][] projToLatLon(final float[][] from, final float[][] to) {
        final float[] fromX = from[0];
        final float[] fromY = from[1];
        to[0] = fromY;
        to[1] = fromX;
        return to;
    }
    
    @Override
    public float[][] latLonToProj(final float[][] from, final float[][] to, final int latIndex, final int lonIndex) {
        final int cnt = from[0].length;
        final float[] toX = to[0];
        final float[] toY = to[1];
        final float[] fromLat = from[latIndex];
        final float[] fromLon = from[lonIndex];
        for (int i = 0; i < cnt; ++i) {
            final float lat = fromLat[i];
            final float lon = (float)(this.centerLon + Math.IEEEremainder(fromLon[i] - this.centerLon, 360.0));
            toX[i] = lon;
            toY[i] = lat;
        }
        return to;
    }
    
    @Override
    public double[][] projToLatLon(final double[][] from, final double[][] to) {
        final double[] fromX = from[0];
        final double[] fromY = from[1];
        to[0] = fromY;
        to[1] = fromX;
        return to;
    }
    
    @Override
    public double[][] latLonToProj(final double[][] from, final double[][] to, final int latIndex, final int lonIndex) {
        final int cnt = from[0].length;
        final double[] toX = to[0];
        final double[] toY = to[1];
        final double[] fromLat = from[latIndex];
        final double[] fromLon = from[lonIndex];
        for (int i = 0; i < cnt; ++i) {
            final double lat = fromLat[i];
            final double lon = this.centerLon + Math.IEEEremainder(fromLon[i] - this.centerLon, 360.0);
            toX[i] = lon;
            toY[i] = lat;
        }
        return to;
    }
    
    @Override
    public Object clone() {
        final LatLonProjection pp = (LatLonProjection)super.clone();
        pp.centerLon = this.centerLon;
        return pp;
    }
    
    public double setCenterLon(final double centerLon) {
        return this.centerLon = LatLonPointImpl.lonNormal(centerLon);
    }
    
    public double getCenterLon() {
        return this.centerLon;
    }
    
    @Override
    public boolean crossSeam(final ProjectionPoint pt1, final ProjectionPoint pt2) {
        return Math.abs(pt1.getX() - pt2.getX()) > 270.0;
    }
    
    @Override
    public void setDefaultMapArea(final ProjectionRect bb) {
        super.setDefaultMapArea(bb);
        this.centerLon = bb.getCenterX();
    }
    
    public ProjectionRect[] latLonToProjRect(final LatLonRect latlonR) {
        final double lat0 = latlonR.getLowerLeftPoint().getLatitude();
        final double height = Math.abs(latlonR.getUpperRightPoint().getLatitude() - lat0);
        final double width = latlonR.getWidth();
        final double lon0 = LatLonPointImpl.lonNormal(latlonR.getLowerLeftPoint().getLongitude(), this.centerLon);
        final double lon2 = LatLonPointImpl.lonNormal(latlonR.getUpperRightPoint().getLongitude(), this.centerLon);
        if (lon0 < lon2) {
            this.projR1.setRect(lon0, lat0, width, height);
            this.rects[0] = this.projR1;
            this.rects[1] = null;
        }
        else {
            final double y = this.centerLon + 180.0 - lon0;
            this.projR1.setRect(lon0, lat0, y, height);
            this.projR2.setRect(lon2 - width + y, lat0, width - y, height);
            this.rects[0] = this.projR1;
            this.rects[1] = this.projR2;
        }
        return this.rects;
    }
    
    @Override
    public LatLonRect projToLatLonBB(final ProjectionRect world) {
        final double startLat = world.getMinY();
        final double startLon = world.getMinX();
        final double deltaLat = world.getHeight();
        final double deltaLon = world.getWidth();
        final LatLonPoint llpt = new LatLonPointImpl(startLat, startLon);
        return new LatLonRect(llpt, deltaLat, deltaLon);
    }
    
    public ProjectionRect[] latLonToProjRect(double lat0, double lon0, final double lat1, double lon1) {
        final double height = Math.abs(lat1 - lat0);
        lat0 = Math.min(lat1, lat0);
        double width = lon1 - lon0;
        if (width < 1.0E-8) {
            width = 360.0;
        }
        lon0 = LatLonPointImpl.lonNormal(lon0, this.centerLon);
        lon1 = LatLonPointImpl.lonNormal(lon1, this.centerLon);
        if (width >= 360.0) {
            this.projR1.setRect(this.centerLon - 180.0, lat0, 360.0, height);
            this.rects[0] = this.projR1;
            this.rects[1] = null;
        }
        else if (lon0 < lon1) {
            this.projR1.setRect(lon0, lat0, width, height);
            this.rects[0] = this.projR1;
            this.rects[1] = null;
        }
        else {
            final double y = this.centerLon + 180.0 - lon0;
            this.projR1.setRect(lon0, lat0, y, height);
            this.projR2.setRect(lon1 - width + y, lat0, width - y, height);
            this.rects[0] = this.projR1;
            this.rects[1] = this.projR2;
        }
        return this.rects;
    }
}
