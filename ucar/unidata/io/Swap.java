// 
// Decompiled by Procyon v0.5.36
// 

package ucar.unidata.io;

public class Swap
{
    public static short swapShort(final byte[] b, final int offset) {
        final int low = b[offset] & 0xFF;
        final int high = b[offset + 1] & 0xFF;
        return (short)(high << 8 | low);
    }
    
    public static int swapInt(final byte[] b, final int offset) {
        int accum = 0;
        for (int shiftBy = 0, i = offset; shiftBy < 32; shiftBy += 8, ++i) {
            accum |= (b[i] & 0xFF) << shiftBy;
        }
        return accum;
    }
    
    public static long swapLong(final byte[] b, final int offset) {
        long accum = 0L;
        for (int shiftBy = 0, i = offset; shiftBy < 64; shiftBy += 8, ++i) {
            final long shiftedval = (long)(b[i] & 0xFF) << shiftBy;
            accum |= shiftedval;
        }
        return accum;
    }
    
    public static float swapFloat(final byte[] b, final int offset) {
        int accum = 0;
        for (int shiftBy = 0, i = offset; shiftBy < 32; shiftBy += 8, ++i) {
            accum |= (b[i] & 0xFF) << shiftBy;
        }
        return Float.intBitsToFloat(accum);
    }
    
    public static double swapDouble(final byte[] b, final int offset) {
        long accum = 0L;
        for (int shiftBy = 0, i = offset; shiftBy < 64; shiftBy += 8, ++i) {
            final long shiftedval = (long)(b[i] & 0xFF) << shiftBy;
            accum |= shiftedval;
        }
        return Double.longBitsToDouble(accum);
    }
    
    public static char swapChar(final byte[] b, final int offset) {
        final int low = b[offset] & 0xFF;
        final int high = b[offset + 1] & 0xFF;
        return (char)(high << 8 | low);
    }
    
    public static short swapShort(final short s) {
        return swapShort(shortToBytes(s), 0);
    }
    
    public static int swapInt(final int v) {
        return swapInt(intToBytes(v), 0);
    }
    
    public static long swapLong(final long l) {
        return swapLong(longToBytes(l), 0);
    }
    
    public static float swapFloat(final float v) {
        final int l = swapInt(Float.floatToIntBits(v));
        return Float.intBitsToFloat(l);
    }
    
    public static double swapDouble(final double v) {
        final long l = swapLong(Double.doubleToLongBits(v));
        return Double.longBitsToDouble(l);
    }
    
    public static byte[] shortToBytes(final short v) {
        final byte[] b = new byte[2];
        final int allbits = 255;
        for (int i = 0; i < 2; ++i) {
            b[1 - i] = (byte)((v & allbits << i * 8) >> i * 8);
        }
        return b;
    }
    
    public static byte[] intToBytes(final int v) {
        final byte[] b = new byte[4];
        final int allbits = 255;
        for (int i = 0; i < 4; ++i) {
            b[3 - i] = (byte)((v & allbits << i * 8) >> i * 8);
        }
        return b;
    }
    
    public static byte[] longToBytes(final long v) {
        final byte[] b = new byte[8];
        final long allbits = 255L;
        for (int i = 0; i < 8; ++i) {
            b[7 - i] = (byte)((v & allbits << i * 8) >> i * 8);
        }
        return b;
    }
}
