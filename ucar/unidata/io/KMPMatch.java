// 
// Decompiled by Procyon v0.5.36
// 

package ucar.unidata.io;

import net.jcip.annotations.Immutable;

@Immutable
public class KMPMatch
{
    private final byte[] match;
    private final int[] failure;
    
    public KMPMatch(final byte[] match) {
        this.match = match;
        this.failure = this.computeFailure(match);
    }
    
    public int getMatchLength() {
        return this.match.length;
    }
    
    public int indexOf(final byte[] data, final int start, final int max) {
        int j = 0;
        if (data.length == 0) {
            return -1;
        }
        for (int i = start; i < start + max; ++i) {
            while (j > 0 && this.match[j] != data[i]) {
                j = this.failure[j - 1];
            }
            if (this.match[j] == data[i]) {
                ++j;
            }
            if (j == this.match.length) {
                return i - this.match.length + 1;
            }
        }
        return -1;
    }
    
    private int[] computeFailure(final byte[] match) {
        final int[] result = new int[match.length];
        int j = 0;
        for (int i = 1; i < match.length; ++i) {
            while (j > 0 && match[j] != match[i]) {
                j = result[j - 1];
            }
            if (match[i] == match[i]) {
                ++j;
            }
            result[i] = j;
        }
        return result;
    }
}
