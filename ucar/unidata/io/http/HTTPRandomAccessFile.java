// 
// Decompiled by Procyon v0.5.36
// 

package ucar.unidata.io.http;

import java.nio.ByteBuffer;
import java.nio.channels.WritableByteChannel;
import java.io.InputStream;
import java.io.FileNotFoundException;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.Header;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.methods.HeadMethod;
import java.io.IOException;
import org.apache.commons.httpclient.HttpConnectionManager;
import org.apache.commons.httpclient.MultiThreadedHttpConnectionManager;
import org.apache.commons.httpclient.HttpClient;
import ucar.unidata.io.RandomAccessFile;

public class HTTPRandomAccessFile extends RandomAccessFile
{
    public static int defaultHTTPBufferSize;
    private static HttpClient _client;
    private String url;
    private long total_length;
    private boolean debug;
    private boolean debugDetails;
    
    public static void setHttpClient(final HttpClient client) {
        HTTPRandomAccessFile._client = client;
    }
    
    public static HttpClient getHttpClient() {
        return HTTPRandomAccessFile._client;
    }
    
    private synchronized void initHttpClient() {
        if (HTTPRandomAccessFile._client != null) {
            return;
        }
        final MultiThreadedHttpConnectionManager connectionManager = new MultiThreadedHttpConnectionManager();
        HTTPRandomAccessFile._client = new HttpClient((HttpConnectionManager)connectionManager);
    }
    
    public HTTPRandomAccessFile(final String url) throws IOException {
        this(url, HTTPRandomAccessFile.defaultHTTPBufferSize);
        this.location = url;
    }
    
    public HTTPRandomAccessFile(final String url, final int bufferSize) throws IOException {
        super(bufferSize);
        this.total_length = 0L;
        this.debug = false;
        this.debugDetails = false;
        this.file = null;
        this.url = url;
        this.location = url;
        this.initHttpClient();
        boolean needtest = true;
        HttpMethod method = null;
        try {
            method = (HttpMethod)new HeadMethod(url);
            method.setFollowRedirects(true);
            this.doConnect(method);
            Header head = method.getResponseHeader("Accept-Ranges");
            if (head == null) {
                needtest = true;
            }
            else if (head.getValue().equalsIgnoreCase("bytes")) {
                needtest = false;
            }
            else if (head.getValue().equalsIgnoreCase("none")) {
                throw new IOException("Server does not support byte Ranges");
            }
            head = method.getResponseHeader("Content-Length");
            if (head == null) {
                throw new IOException("Server does not support Content-Length");
            }
            try {
                this.total_length = Long.parseLong(head.getValue());
            }
            catch (NumberFormatException e) {
                throw new IOException("Server has malformed Content-Length header");
            }
        }
        finally {
            if (method != null) {
                method.releaseConnection();
            }
        }
        if (needtest && !this.rangeOk(url)) {
            throw new IOException("Server does not support byte Ranges");
        }
        if (HTTPRandomAccessFile.debugLeaks) {
            HTTPRandomAccessFile.openFiles.add(this.location);
        }
    }
    
    private boolean rangeOk(final String url) {
        HttpMethod method = null;
        try {
            method = (HttpMethod)new GetMethod(url);
            method.setFollowRedirects(true);
            method.setRequestHeader("Range", "bytes=0-1");
            this.doConnect(method);
            final int code = method.getStatusCode();
            if (code != 206) {
                throw new IOException("Server does not support Range requests, code= " + code);
            }
            method.getResponseBody();
            return true;
        }
        catch (IOException e) {
            return false;
        }
        finally {
            if (method != null) {
                method.releaseConnection();
            }
        }
    }
    
    private void doConnect(final HttpMethod method) throws IOException {
        final int statusCode = HTTPRandomAccessFile._client.executeMethod(method);
        if (statusCode == 404) {
            throw new FileNotFoundException(this.url + " " + method.getStatusLine());
        }
        if (statusCode >= 300) {
            throw new IOException(this.url + " " + method.getStatusLine());
        }
        if (this.debugDetails) {
            this.printHeaders("Request: " + method.getName() + " " + method.getPath(), method.getRequestHeaders());
            this.printHeaders("Response: " + method.getStatusCode(), method.getResponseHeaders());
        }
    }
    
    private void printHeaders(final String title, final Header[] heads) {
        System.out.println(title);
        for (final Header head : heads) {
            System.out.print("  " + head.toString());
        }
        System.out.println();
    }
    
    @Override
    protected int read_(final long pos, final byte[] buff, final int offset, final int len) throws IOException {
        long end = pos + len - 1L;
        if (end >= this.total_length) {
            end = this.total_length - 1L;
        }
        if (this.debug) {
            System.out.println(" HTTPRandomAccessFile bytes=" + pos + "-" + end + ": ");
        }
        HttpMethod method = null;
        try {
            method = (HttpMethod)new GetMethod(this.url);
            method.setFollowRedirects(true);
            method.setRequestHeader("Range", "bytes=" + pos + "-" + end);
            this.doConnect(method);
            final int code = method.getStatusCode();
            if (code != 206) {
                throw new IOException("Server does not support Range requests, code= " + code);
            }
            final String s = method.getResponseHeader("Content-Length").getValue();
            if (s == null) {
                throw new IOException("Server does not send Content-Length header");
            }
            int readLen = Integer.parseInt(s);
            readLen = Math.min(len, readLen);
            final InputStream is = method.getResponseBodyAsStream();
            readLen = this.copy(is, buff, offset, readLen);
            return readLen;
        }
        finally {
            if (method != null) {
                method.releaseConnection();
            }
        }
    }
    
    private int copy(final InputStream in, final byte[] buff, final int offset, int want) throws IOException {
        int done = 0;
        while (want > 0) {
            final int bytesRead = in.read(buff, offset + done, want);
            if (bytesRead == -1) {
                break;
            }
            done += bytesRead;
            want -= bytesRead;
        }
        return done;
    }
    
    @Override
    public long readToByteChannel(final WritableByteChannel dest, final long offset, final long nbytes) throws IOException {
        final int n = (int)nbytes;
        final byte[] buff = new byte[n];
        final int done = this.read_(offset, buff, 0, n);
        dest.write(ByteBuffer.wrap(buff));
        return done;
    }
    
    @Override
    public long length() throws IOException {
        final long fileLength = this.total_length;
        if (fileLength < this.dataEnd) {
            return this.dataEnd;
        }
        return fileLength;
    }
    
    static {
        HTTPRandomAccessFile.defaultHTTPBufferSize = 20000;
    }
}
