// 
// Decompiled by Procyon v0.5.36
// 

package ucar.unidata.io;

import java.nio.ByteBuffer;
import java.nio.channels.WritableByteChannel;
import java.io.IOException;

public class InMemoryRandomAccessFile extends RandomAccessFile
{
    public InMemoryRandomAccessFile(final String name, final byte[] data) {
        super(1);
        this.location = name;
        this.file = null;
        if (data == null) {
            throw new IllegalArgumentException("data array is null");
        }
        this.buffer = data;
        this.bufferStart = 0L;
        this.dataSize = this.buffer.length;
        this.dataEnd = this.buffer.length;
        this.filePosition = 0L;
        this.endOfFile = false;
        if (InMemoryRandomAccessFile.debugLeaks) {
            InMemoryRandomAccessFile.openFiles.add(this.location);
        }
    }
    
    @Override
    public long length() {
        return this.dataEnd;
    }
    
    @Override
    public void setBufferSize(final int bufferSize) {
    }
    
    @Override
    protected int read_(final long pos, final byte[] b, final int offset, int len) throws IOException {
        len = Math.min(len, (int)(this.buffer.length - pos));
        System.arraycopy(this.buffer, (int)pos, b, offset, len);
        return len;
    }
    
    @Override
    public long readToByteChannel(final WritableByteChannel dest, final long offset, final long nbytes) throws IOException {
        return dest.write(ByteBuffer.wrap(this.buffer, (int)offset, (int)nbytes));
    }
}
