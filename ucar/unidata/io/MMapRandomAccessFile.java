// 
// Decompiled by Procyon v0.5.36
// 

package ucar.unidata.io;

import java.io.IOException;
import java.nio.channels.FileChannel;
import java.nio.MappedByteBuffer;

class MMapRandomAccessFile extends RandomAccessFile
{
    private MappedByteBuffer source;
    
    public MMapRandomAccessFile(final String location, final String mode) throws IOException {
        super(location, mode, 1);
        final FileChannel channel = this.file.getChannel();
        this.source = channel.map(this.readonly ? FileChannel.MapMode.READ_ONLY : FileChannel.MapMode.READ_WRITE, 0L, channel.size());
        channel.close();
        this.bufferStart = 0L;
        this.dataSize = (int)channel.size();
        this.dataEnd = channel.size();
        this.filePosition = 0L;
        this.buffer = null;
        this.endOfFile = false;
    }
    
    @Override
    public void flush() throws IOException {
        if (this.bufferModified) {
            this.source.force();
            this.bufferModified = false;
        }
    }
    
    @Override
    public void close() throws IOException {
        if (!this.readonly) {
            this.flush();
        }
        this.source = null;
    }
    
    @Override
    public long length() {
        return this.dataEnd;
    }
    
    @Override
    public void seek(final long pos) {
        this.filePosition = pos;
        if (this.filePosition < this.dataEnd) {
            this.source.position((int)this.filePosition);
        }
        else {
            this.endOfFile = true;
        }
    }
    
    @Override
    public void unread() {
        this.seek(this.filePosition - 1L);
    }
    
    @Override
    public int read() throws IOException {
        if (this.filePosition < this.dataEnd) {
            ++this.filePosition;
            return this.source.get() & 0xFF;
        }
        return -1;
    }
    
    @Override
    protected int readBytes(final byte[] dst, final int offset, int length) throws IOException {
        if (this.endOfFile) {
            return -1;
        }
        length = (int)Math.min(length, this.dataEnd - this.filePosition);
        if (length > 0) {
            this.source.get(dst, offset, length);
            this.filePosition += length;
        }
        return length;
    }
    
    @Override
    public void write(final int b) throws IOException {
        this.source.put((byte)b);
        ++this.filePosition;
        this.bufferModified = true;
    }
    
    @Override
    public void writeBytes(final byte[] dst, final int offset, final int length) throws IOException {
        this.source.put(dst, offset, length);
        this.filePosition += length;
    }
}
