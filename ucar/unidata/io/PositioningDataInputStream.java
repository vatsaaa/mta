// 
// Decompiled by Procyon v0.5.36
// 

package ucar.unidata.io;

import java.io.IOException;
import java.io.InputStream;
import java.io.DataInputStream;

public class PositioningDataInputStream
{
    private DataInputStream delegate;
    private long cpos;
    
    public PositioningDataInputStream(final InputStream is) {
        this.cpos = 0L;
        if (is instanceof DataInputStream) {
            this.delegate = (DataInputStream)is;
        }
        else {
            this.delegate = new DataInputStream(is);
        }
    }
    
    private void seek(final long pos) throws IOException {
        if (pos < this.cpos) {
            throw new IllegalArgumentException("Cannot go backwards; current=" + this.cpos + " request=" + pos);
        }
        for (long want = pos - this.cpos; want > 0L; want -= this.delegate.skip(want)) {}
        this.cpos = pos;
    }
    
    public void read(final long pos, final byte[] dest, final int off, final int len) throws IOException {
        this.seek(pos);
        this.delegate.readFully(dest, off, len);
        this.cpos += len;
    }
    
    public void readShort(final long pos, final short[] dest, final int off, final int len) throws IOException {
        this.seek(pos);
        for (int i = 0; i < len; ++i) {
            dest[off + i] = this.delegate.readShort();
        }
        this.cpos += len * 2;
    }
    
    public void readInt(final long pos, final int[] dest, final int off, final int len) throws IOException {
        this.seek(pos);
        for (int i = 0; i < len; ++i) {
            dest[off + i] = this.delegate.readInt();
        }
        this.cpos += len * 4;
    }
    
    public void readLong(final long pos, final long[] dest, final int off, final int len) throws IOException {
        this.seek(pos);
        for (int i = 0; i < len; ++i) {
            dest[off + i] = this.delegate.readLong();
        }
        this.cpos += len * 8;
    }
    
    public void readFloat(final long pos, final float[] dest, final int off, final int len) throws IOException {
        this.seek(pos);
        for (int i = 0; i < len; ++i) {
            dest[off + i] = this.delegate.readFloat();
        }
        this.cpos += len * 4;
    }
    
    public void readDouble(final long pos, final double[] dest, final int off, final int len) throws IOException {
        this.seek(pos);
        for (int i = 0; i < len; ++i) {
            dest[off + i] = this.delegate.readDouble();
        }
        this.cpos += len * 8;
    }
}
