// 
// Decompiled by Procyon v0.5.36
// 

package ucar.unidata.io;

import java.util.HashSet;
import java.io.UTFDataFormatException;
import java.io.DataInputStream;
import java.io.EOFException;
import java.nio.channels.WritableByteChannel;
import java.io.FileDescriptor;
import java.io.IOException;
import java.util.Iterator;
import java.util.Collections;
import java.util.ArrayList;
import java.nio.channels.FileChannel;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.List;
import java.util.Set;
import net.jcip.annotations.NotThreadSafe;
import java.io.DataOutput;
import java.io.DataInput;

@NotThreadSafe
public class RandomAccessFile implements DataInput, DataOutput
{
    public static final int BIG_ENDIAN = 0;
    public static final int LITTLE_ENDIAN = 1;
    protected static boolean debugLeaks;
    protected static boolean debugAccess;
    protected static Set<String> allFiles;
    protected static List<String> openFiles;
    private static AtomicInteger debug_nseeks;
    private static AtomicLong debug_nbytes;
    protected static boolean showOpen;
    protected static boolean showRead;
    protected static final int defaultBufferSize = 8092;
    protected String location;
    protected java.io.RandomAccessFile file;
    protected FileChannel fileChannel;
    protected long filePosition;
    protected byte[] buffer;
    protected long bufferStart;
    protected long dataEnd;
    protected int dataSize;
    protected boolean endOfFile;
    protected boolean readonly;
    protected boolean bigEndian;
    boolean bufferModified;
    private long minLength;
    boolean extendMode;
    
    public static boolean getDebugLeaks() {
        return RandomAccessFile.debugLeaks;
    }
    
    public static void setDebugLeaks(final boolean b) {
        RandomAccessFile.debugLeaks = b;
    }
    
    public static List<String> getOpenFiles() {
        return RandomAccessFile.openFiles;
    }
    
    public static List<String> getAllFiles() {
        final List<String> result = new ArrayList<String>();
        if (null == RandomAccessFile.allFiles) {
            return null;
        }
        final Iterator<String> iter = RandomAccessFile.allFiles.iterator();
        while (iter.hasNext()) {
            result.add(iter.next());
        }
        Collections.sort(result);
        return result;
    }
    
    public static void setDebugAccess(final boolean b) {
        RandomAccessFile.debugAccess = b;
        if (b) {
            RandomAccessFile.debug_nseeks = new AtomicInteger();
            RandomAccessFile.debug_nbytes = new AtomicLong();
        }
    }
    
    public static int getDebugNseeks() {
        return (RandomAccessFile.debug_nseeks == null) ? 0 : RandomAccessFile.debug_nseeks.intValue();
    }
    
    public static long getDebugNbytes() {
        return (RandomAccessFile.debug_nbytes == null) ? 0L : RandomAccessFile.debug_nbytes.longValue();
    }
    
    protected RandomAccessFile(final int bufferSize) {
        this.bufferModified = false;
        this.minLength = 0L;
        this.extendMode = false;
        this.file = null;
        this.readonly = true;
        this.init(bufferSize);
    }
    
    public RandomAccessFile(final String location, final String mode) throws IOException {
        this(location, mode, 8092);
        this.location = location;
    }
    
    public RandomAccessFile(final String location, final String mode, final int bufferSize) throws IOException {
        this.bufferModified = false;
        this.minLength = 0L;
        this.extendMode = false;
        this.location = location;
        this.file = new java.io.RandomAccessFile(location, mode);
        this.readonly = mode.equals("r");
        this.init(bufferSize);
        if (RandomAccessFile.debugLeaks) {
            RandomAccessFile.openFiles.add(location);
            RandomAccessFile.allFiles.add(location);
            if (RandomAccessFile.showOpen) {
                System.out.println("  open " + location);
            }
        }
    }
    
    public java.io.RandomAccessFile getRandomAccessFile() {
        return this.file;
    }
    
    private void init(final int bufferSize) {
        this.bufferStart = 0L;
        this.dataEnd = 0L;
        this.dataSize = 0;
        this.filePosition = 0L;
        this.buffer = new byte[bufferSize];
        this.endOfFile = false;
    }
    
    public void setBufferSize(final int bufferSize) {
        this.init(bufferSize);
    }
    
    public int getBufferSize() {
        return this.buffer.length;
    }
    
    public void close() throws IOException {
        if (RandomAccessFile.debugLeaks) {
            RandomAccessFile.openFiles.remove(this.location);
            if (RandomAccessFile.showOpen) {
                System.out.println("  close " + this.location);
            }
        }
        if (this.file == null) {
            return;
        }
        this.flush();
        final long fileSize = this.file.length();
        if (!this.readonly && this.minLength != 0L && this.minLength != fileSize) {
            this.file.setLength(this.minLength);
        }
        this.file.close();
        this.file = null;
    }
    
    public boolean isAtEndOfFile() {
        return this.endOfFile;
    }
    
    public void seek(final long pos) throws IOException {
        if (pos >= this.bufferStart && pos < this.dataEnd) {
            this.filePosition = pos;
            return;
        }
        this.readBuffer(pos);
    }
    
    protected void readBuffer(final long pos) throws IOException {
        if (this.bufferModified) {
            this.flush();
        }
        this.bufferStart = pos;
        this.filePosition = pos;
        this.dataSize = this.read_(pos, this.buffer, 0, this.buffer.length);
        if (this.dataSize <= 0) {
            this.dataSize = 0;
            this.endOfFile = true;
        }
        else {
            this.endOfFile = false;
        }
        this.dataEnd = this.bufferStart + this.dataSize;
    }
    
    public long getFilePointer() throws IOException {
        return this.filePosition;
    }
    
    public String getLocation() {
        return this.location;
    }
    
    public long length() throws IOException {
        final long fileLength = this.file.length();
        if (fileLength < this.dataEnd) {
            return this.dataEnd;
        }
        return fileLength;
    }
    
    public void order(final int endian) {
        if (endian < 0) {
            return;
        }
        this.bigEndian = (endian == 0);
    }
    
    public FileDescriptor getFD() throws IOException {
        return (this.file == null) ? null : this.file.getFD();
    }
    
    public void flush() throws IOException {
        if (this.bufferModified) {
            this.file.seek(this.bufferStart);
            this.file.write(this.buffer, 0, this.dataSize);
            this.bufferModified = false;
        }
    }
    
    public void setMinLength(final long minLength) {
        this.minLength = minLength;
    }
    
    public void setExtendMode() {
        this.extendMode = true;
    }
    
    public int read() throws IOException {
        if (this.filePosition < this.dataEnd) {
            final int pos = (int)(this.filePosition - this.bufferStart);
            ++this.filePosition;
            return this.buffer[pos] & 0xFF;
        }
        if (this.endOfFile) {
            return -1;
        }
        this.seek(this.filePosition);
        return this.read();
    }
    
    protected int readBytes(final byte[] b, final int off, final int len) throws IOException {
        if (this.endOfFile) {
            return -1;
        }
        final int bytesAvailable = (int)(this.dataEnd - this.filePosition);
        if (bytesAvailable < 1) {
            this.seek(this.filePosition);
            return this.readBytes(b, off, len);
        }
        final int copyLength = (bytesAvailable >= len) ? len : bytesAvailable;
        System.arraycopy(this.buffer, (int)(this.filePosition - this.bufferStart), b, off, copyLength);
        this.filePosition += copyLength;
        if (copyLength < len) {
            int extraCopy = len - copyLength;
            if (extraCopy > this.buffer.length) {
                extraCopy = this.read_(this.filePosition, b, off + copyLength, len - copyLength);
            }
            else {
                this.seek(this.filePosition);
                if (!this.endOfFile) {
                    extraCopy = ((extraCopy > this.dataSize) ? this.dataSize : extraCopy);
                    System.arraycopy(this.buffer, 0, b, off + copyLength, extraCopy);
                }
                else {
                    extraCopy = -1;
                }
            }
            if (extraCopy > 0) {
                this.filePosition += extraCopy;
                return copyLength + extraCopy;
            }
        }
        return copyLength;
    }
    
    public long readToByteChannel(final WritableByteChannel dest, long offset, final long nbytes) throws IOException {
        if (this.fileChannel == null) {
            this.fileChannel = this.file.getChannel();
        }
        long need;
        long count;
        for (need = nbytes; need > 0L; need -= count, offset += count) {
            count = this.fileChannel.transferTo(offset, need, dest);
        }
        return nbytes - need;
    }
    
    protected int read_(final long pos, final byte[] b, final int offset, final int len) throws IOException {
        this.file.seek(pos);
        int n = this.file.read(b, offset, len);
        if (RandomAccessFile.debugAccess) {
            if (RandomAccessFile.showRead) {
                System.out.println(" **read_ " + this.location + " = " + len + " bytes at " + pos + "; block = " + pos / this.buffer.length);
            }
            RandomAccessFile.debug_nseeks.incrementAndGet();
            RandomAccessFile.debug_nbytes.addAndGet(len);
        }
        if (this.extendMode && n < len) {
            n = len;
        }
        return n;
    }
    
    public int read(final byte[] b, final int off, final int len) throws IOException {
        return this.readBytes(b, off, len);
    }
    
    public int read(final byte[] b) throws IOException {
        return this.readBytes(b, 0, b.length);
    }
    
    public byte[] readBytes(final int count) throws IOException {
        final byte[] b = new byte[count];
        this.readFully(b);
        return b;
    }
    
    public final void readFully(final byte[] b) throws IOException {
        this.readFully(b, 0, b.length);
    }
    
    public final void readFully(final byte[] b, final int off, final int len) throws IOException {
        int count;
        for (int n = 0; n < len; n += count) {
            count = this.read(b, off + n, len - n);
            if (count < 0) {
                throw new EOFException();
            }
        }
    }
    
    public int skipBytes(final int n) throws IOException {
        this.seek(this.getFilePointer() + n);
        return n;
    }
    
    public void unread() {
        --this.filePosition;
    }
    
    public void write(final int b) throws IOException {
        if (this.filePosition < this.dataEnd) {
            final int pos = (int)(this.filePosition - this.bufferStart);
            this.buffer[pos] = (byte)b;
            this.bufferModified = true;
            ++this.filePosition;
        }
        else if (this.dataSize != this.buffer.length) {
            final int pos = (int)(this.filePosition - this.bufferStart);
            this.buffer[pos] = (byte)b;
            this.bufferModified = true;
            ++this.filePosition;
            ++this.dataSize;
            ++this.dataEnd;
        }
        else {
            this.seek(this.filePosition);
            this.write(b);
        }
    }
    
    public void writeBytes(final byte[] b, final int off, final int len) throws IOException {
        if (len < this.buffer.length) {
            int spaceInBuffer = 0;
            int copyLength = 0;
            if (this.filePosition >= this.bufferStart) {
                spaceInBuffer = (int)(this.bufferStart + this.buffer.length - this.filePosition);
            }
            if (spaceInBuffer > 0) {
                copyLength = ((spaceInBuffer > len) ? len : spaceInBuffer);
                System.arraycopy(b, off, this.buffer, (int)(this.filePosition - this.bufferStart), copyLength);
                this.bufferModified = true;
                final long myDataEnd = this.filePosition + copyLength;
                this.dataEnd = ((myDataEnd > this.dataEnd) ? myDataEnd : this.dataEnd);
                this.dataSize = (int)(this.dataEnd - this.bufferStart);
                this.filePosition += copyLength;
            }
            if (copyLength < len) {
                this.seek(this.filePosition);
                System.arraycopy(b, off + copyLength, this.buffer, (int)(this.filePosition - this.bufferStart), len - copyLength);
                this.bufferModified = true;
                final long myDataEnd = this.filePosition + (len - copyLength);
                this.dataEnd = ((myDataEnd > this.dataEnd) ? myDataEnd : this.dataEnd);
                this.dataSize = (int)(this.dataEnd - this.bufferStart);
                this.filePosition += len - copyLength;
            }
        }
        else {
            if (this.bufferModified) {
                this.flush();
            }
            this.file.seek(this.filePosition);
            this.file.write(b, off, len);
            this.filePosition += len;
            this.bufferStart = this.filePosition;
            this.dataSize = 0;
            this.dataEnd = this.bufferStart + this.dataSize;
        }
    }
    
    public void write(final byte[] b) throws IOException {
        this.writeBytes(b, 0, b.length);
    }
    
    public void write(final byte[] b, final int off, final int len) throws IOException {
        this.writeBytes(b, off, len);
    }
    
    public final boolean readBoolean() throws IOException {
        final int ch = this.read();
        if (ch < 0) {
            throw new EOFException();
        }
        return ch != 0;
    }
    
    public final byte readByte() throws IOException {
        final int ch = this.read();
        if (ch < 0) {
            throw new EOFException();
        }
        return (byte)ch;
    }
    
    public final int readUnsignedByte() throws IOException {
        final int ch = this.read();
        if (ch < 0) {
            throw new EOFException();
        }
        return ch;
    }
    
    public final short readShort() throws IOException {
        final int ch1 = this.read();
        final int ch2 = this.read();
        if ((ch1 | ch2) < 0) {
            throw new EOFException();
        }
        if (this.bigEndian) {
            return (short)((ch1 << 8) + ch2);
        }
        return (short)((ch2 << 8) + ch1);
    }
    
    public final void readShort(final short[] pa, final int start, final int n) throws IOException {
        for (int i = 0; i < n; ++i) {
            pa[start + i] = this.readShort();
        }
    }
    
    public final int readUnsignedShort() throws IOException {
        final int ch1 = this.read();
        final int ch2 = this.read();
        if ((ch1 | ch2) < 0) {
            throw new EOFException();
        }
        if (this.bigEndian) {
            return (ch1 << 8) + ch2;
        }
        return (ch2 << 8) + ch1;
    }
    
    public final char readChar() throws IOException {
        final int ch1 = this.read();
        final int ch2 = this.read();
        if ((ch1 | ch2) < 0) {
            throw new EOFException();
        }
        if (this.bigEndian) {
            return (char)((ch1 << 8) + ch2);
        }
        return (char)((ch2 << 8) + ch1);
    }
    
    public final int readInt() throws IOException {
        final int ch1 = this.read();
        final int ch2 = this.read();
        final int ch3 = this.read();
        final int ch4 = this.read();
        if ((ch1 | ch2 | ch3 | ch4) < 0) {
            throw new EOFException();
        }
        if (this.bigEndian) {
            return (ch1 << 24) + (ch2 << 16) + (ch3 << 8) + ch4;
        }
        return (ch4 << 24) + (ch3 << 16) + (ch2 << 8) + ch1;
    }
    
    public final int readIntUnbuffered(final long pos) throws IOException {
        final byte[] bb = new byte[4];
        this.read_(pos, bb, 0, 4);
        final int ch1 = bb[0] & 0xFF;
        final int ch2 = bb[1] & 0xFF;
        final int ch3 = bb[2] & 0xFF;
        final int ch4 = bb[3] & 0xFF;
        if ((ch1 | ch2 | ch3 | ch4) < 0) {
            throw new EOFException();
        }
        if (this.bigEndian) {
            return (ch1 << 24) + (ch2 << 16) + (ch3 << 8) + ch4;
        }
        return (ch4 << 24) + (ch3 << 16) + (ch2 << 8) + ch1;
    }
    
    public final void readInt(final int[] pa, final int start, final int n) throws IOException {
        for (int i = 0; i < n; ++i) {
            pa[start + i] = this.readInt();
        }
    }
    
    public final long readLong() throws IOException {
        if (this.bigEndian) {
            return ((long)this.readInt() << 32) + ((long)this.readInt() & 0xFFFFFFFFL);
        }
        return ((long)this.readInt() & 0xFFFFFFFFL) + ((long)this.readInt() << 32);
    }
    
    public final void readLong(final long[] pa, final int start, final int n) throws IOException {
        for (int i = 0; i < n; ++i) {
            pa[start + i] = this.readLong();
        }
    }
    
    public final float readFloat() throws IOException {
        return Float.intBitsToFloat(this.readInt());
    }
    
    public final void readFloat(final float[] pa, final int start, final int n) throws IOException {
        for (int i = 0; i < n; ++i) {
            pa[start + i] = Float.intBitsToFloat(this.readInt());
        }
    }
    
    public final double readDouble() throws IOException {
        return Double.longBitsToDouble(this.readLong());
    }
    
    public final void readDouble(final double[] pa, final int start, final int n) throws IOException {
        for (int i = 0; i < n; ++i) {
            pa[start + i] = Double.longBitsToDouble(this.readLong());
        }
    }
    
    public final String readLine() throws IOException {
        final StringBuilder input = new StringBuilder();
        int c;
        while ((c = this.read()) != -1 && c != 10) {
            input.append((char)c);
        }
        if (c == -1 && input.length() == 0) {
            return null;
        }
        return input.toString();
    }
    
    public final String readUTF() throws IOException {
        return DataInputStream.readUTF(this);
    }
    
    public String readString(final int nbytes) throws IOException {
        final byte[] data = new byte[nbytes];
        this.readFully(data);
        return new String(data);
    }
    
    public final void writeBoolean(final boolean v) throws IOException {
        this.write(v ? 1 : 0);
    }
    
    public final void writeBoolean(final boolean[] pa, final int start, final int n) throws IOException {
        for (int i = 0; i < n; ++i) {
            this.writeBoolean(pa[start + i]);
        }
    }
    
    public final void writeByte(final int v) throws IOException {
        this.write(v);
    }
    
    public final void writeShort(final int v) throws IOException {
        this.write(v >>> 8 & 0xFF);
        this.write(v & 0xFF);
    }
    
    public final void writeShort(final short[] pa, final int start, final int n) throws IOException {
        for (int i = 0; i < n; ++i) {
            this.writeShort(pa[start + i]);
        }
    }
    
    public final void writeChar(final int v) throws IOException {
        this.write(v >>> 8 & 0xFF);
        this.write(v & 0xFF);
    }
    
    public final void writeChar(final char[] pa, final int start, final int n) throws IOException {
        for (int i = 0; i < n; ++i) {
            this.writeChar(pa[start + i]);
        }
    }
    
    public final void writeInt(final int v) throws IOException {
        this.write(v >>> 24 & 0xFF);
        this.write(v >>> 16 & 0xFF);
        this.write(v >>> 8 & 0xFF);
        this.write(v & 0xFF);
    }
    
    public final void writeInt(final int[] pa, final int start, final int n) throws IOException {
        for (int i = 0; i < n; ++i) {
            this.writeInt(pa[start + i]);
        }
    }
    
    public final void writeLong(final long v) throws IOException {
        this.write((int)(v >>> 56) & 0xFF);
        this.write((int)(v >>> 48) & 0xFF);
        this.write((int)(v >>> 40) & 0xFF);
        this.write((int)(v >>> 32) & 0xFF);
        this.write((int)(v >>> 24) & 0xFF);
        this.write((int)(v >>> 16) & 0xFF);
        this.write((int)(v >>> 8) & 0xFF);
        this.write((int)v & 0xFF);
    }
    
    public final void writeLong(final long[] pa, final int start, final int n) throws IOException {
        for (int i = 0; i < n; ++i) {
            this.writeLong(pa[start + i]);
        }
    }
    
    public final void writeFloat(final float v) throws IOException {
        this.writeInt(Float.floatToIntBits(v));
    }
    
    public final void writeFloat(final float[] pa, final int start, final int n) throws IOException {
        for (int i = 0; i < n; ++i) {
            this.writeFloat(pa[start + i]);
        }
    }
    
    public final void writeDouble(final double v) throws IOException {
        this.writeLong(Double.doubleToLongBits(v));
    }
    
    public final void writeDouble(final double[] pa, final int start, final int n) throws IOException {
        for (int i = 0; i < n; ++i) {
            this.writeDouble(pa[start + i]);
        }
    }
    
    public final void writeBytes(final String s) throws IOException {
        for (int len = s.length(), i = 0; i < len; ++i) {
            this.write((byte)s.charAt(i));
        }
    }
    
    public final void writeBytes(final char[] b, final int off, final int len) throws IOException {
        for (int i = off; i < len; ++i) {
            this.write((byte)b[i]);
        }
    }
    
    public final void writeChars(final String s) throws IOException {
        for (int len = s.length(), i = 0; i < len; ++i) {
            final int v = s.charAt(i);
            this.write(v >>> 8 & 0xFF);
            this.write(v & 0xFF);
        }
    }
    
    public final void writeUTF(final String str) throws IOException {
        final int strlen = str.length();
        int utflen = 0;
        for (int i = 0; i < strlen; ++i) {
            final int c = str.charAt(i);
            if (c >= 1 && c <= 127) {
                ++utflen;
            }
            else if (c > 2047) {
                utflen += 3;
            }
            else {
                utflen += 2;
            }
        }
        if (utflen > 65535) {
            throw new UTFDataFormatException();
        }
        this.write(utflen >>> 8 & 0xFF);
        this.write(utflen & 0xFF);
        for (int i = 0; i < strlen; ++i) {
            final int c = str.charAt(i);
            if (c >= 1 && c <= 127) {
                this.write(c);
            }
            else if (c > 2047) {
                this.write(0xE0 | (c >> 12 & 0xF));
                this.write(0x80 | (c >> 6 & 0x3F));
                this.write(0x80 | (c & 0x3F));
            }
            else {
                this.write(0xC0 | (c >> 6 & 0x1F));
                this.write(0x80 | (c & 0x3F));
            }
        }
    }
    
    @Override
    public String toString() {
        return "fp=" + this.filePosition + ", bs=" + this.bufferStart + ", de=" + this.dataEnd + ", ds=" + this.dataSize + ", bl=" + this.buffer.length + ", readonly=" + this.readonly + ", bm=" + this.bufferModified;
    }
    
    public boolean searchForward(final KMPMatch match, final int maxBytes) throws IOException {
        final long start = this.getFilePointer();
        final long last = (maxBytes < 0) ? this.length() : Math.min(this.length(), start + maxBytes);
        long needToScan = last - start;
        int bytesAvailable = (int)(this.dataEnd - this.filePosition);
        if (bytesAvailable < 1) {
            this.seek(this.filePosition);
            bytesAvailable = (int)(this.dataEnd - this.filePosition);
        }
        final int bufStart = (int)(this.filePosition - this.bufferStart);
        int scanBytes = (int)Math.min(bytesAvailable, needToScan);
        int pos = match.indexOf(this.buffer, bufStart, scanBytes);
        if (pos >= 0) {
            this.seek(this.bufferStart + pos);
            return true;
        }
        int matchLen;
        for (matchLen = match.getMatchLength(), needToScan -= scanBytes - matchLen; needToScan > matchLen; needToScan -= scanBytes - matchLen) {
            this.readBuffer(this.dataEnd - matchLen);
            scanBytes = (int)Math.min(this.buffer.length, needToScan);
            pos = match.indexOf(this.buffer, 0, scanBytes);
            if (pos > 0) {
                this.seek(this.bufferStart + pos);
                return true;
            }
        }
        this.seek(last);
        return false;
    }
    
    static {
        RandomAccessFile.debugLeaks = false;
        RandomAccessFile.debugAccess = false;
        RandomAccessFile.allFiles = new HashSet<String>();
        RandomAccessFile.openFiles = Collections.synchronizedList(new ArrayList<String>());
        RandomAccessFile.debug_nseeks = new AtomicInteger();
        RandomAccessFile.debug_nbytes = new AtomicLong();
        RandomAccessFile.showOpen = false;
        RandomAccessFile.showRead = false;
    }
}
