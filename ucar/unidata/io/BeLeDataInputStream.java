// 
// Decompiled by Procyon v0.5.36
// 

package ucar.unidata.io;

import java.net.URL;
import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.DataInputStream;

public class BeLeDataInputStream extends DataInputStream
{
    private byte[] w;
    private static final int kLongs = 128;
    private long[] longWorkSpace;
    private byte[] byteWorkSpace;
    
    public BeLeDataInputStream(final InputStream inputStream) throws IOException {
        super(inputStream);
        this.w = new byte[8];
        this.longWorkSpace = new long[128];
        this.byteWorkSpace = new byte[1024];
    }
    
    public BeLeDataInputStream(final String filename) throws IOException {
        this(new BufferedInputStream(new FileInputStream(filename)));
    }
    
    public BeLeDataInputStream(final URL url) throws IOException {
        this(new BufferedInputStream(new DataInputStream(url.openStream())));
    }
    
    public int readLEInt() throws IOException {
        this.readFully(this.w, 0, 4);
        return (this.w[3] & 0xFF) << 24 | (this.w[2] & 0xFF) << 16 | (this.w[1] & 0xFF) << 8 | (this.w[0] & 0xFF);
    }
    
    public float readLEFloat() throws IOException {
        return Float.intBitsToFloat(this.readLEInt());
    }
    
    public double readLEDouble() throws IOException {
        return Double.longBitsToDouble(this.readLELong());
    }
    
    public final void readLEDoubles(final double[] d, final int n) throws IOException {
        int nLeft = n;
        int dCount = 0;
        for (int nToRead = 128; nLeft > 0; nLeft -= nToRead) {
            if (nToRead > nLeft) {
                nToRead = nLeft;
            }
            this.readLELongs(this.longWorkSpace, nToRead);
            for (int i = 0; i < nToRead; ++i) {
                d[dCount++] = Double.longBitsToDouble(this.longWorkSpace[i]);
            }
        }
    }
    
    public long readLELong() throws IOException {
        this.readFully(this.w, 0, 8);
        return (long)(this.w[7] & 0xFF) << 56 | (long)(this.w[6] & 0xFF) << 48 | (long)(this.w[5] & 0xFF) << 40 | (long)(this.w[4] & 0xFF) << 32 | (long)(this.w[3] & 0xFF) << 24 | (long)(this.w[2] & 0xFF) << 16 | (long)(this.w[1] & 0xFF) << 8 | (long)(this.w[0] & 0xFF);
    }
    
    public final void readLELongs(final long[] lbuf, final int n) throws IOException {
        int nLeft = n;
        int lCount = 0;
        for (int nToRead = 128; nLeft > 0; nLeft -= nToRead) {
            if (nToRead > nLeft) {
                nToRead = nLeft;
            }
            this.readFully(this.byteWorkSpace, 0, 8 * nToRead);
            int j = 0;
            for (int i = 0; i < nToRead; ++i) {
                lbuf[lCount++] = ((long)(this.byteWorkSpace[j] & 0xFF) | (long)(this.byteWorkSpace[j + 1] & 0xFF) << 8 | (long)(this.byteWorkSpace[j + 2] & 0xFF) << 16 | (long)(this.byteWorkSpace[j + 3] & 0xFF) << 24 | (long)(this.byteWorkSpace[j + 4] & 0xFF) << 32 | (long)(this.byteWorkSpace[j + 5] & 0xFF) << 40 | (long)(this.byteWorkSpace[j + 6] & 0xFF) << 48 | (long)(this.byteWorkSpace[j + 7] & 0xFF) << 56);
                j += 8;
            }
        }
    }
}
