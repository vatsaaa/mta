// 
// Decompiled by Procyon v0.5.36
// 

package ucar.unidata.io.bzip2;

public class BZip2ReadException extends RuntimeException
{
    public BZip2ReadException() {
    }
    
    public BZip2ReadException(final String message) {
        super(message);
    }
}
