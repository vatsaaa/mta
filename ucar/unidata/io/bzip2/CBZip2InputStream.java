// 
// Decompiled by Procyon v0.5.36
// 

package ucar.unidata.io.bzip2;

import java.io.IOException;
import java.io.InputStream;

public class CBZip2InputStream extends InputStream implements BZip2Constants
{
    private static final int START_BLOCK_STATE = 1;
    private static final int RAND_PART_A_STATE = 2;
    private static final int RAND_PART_B_STATE = 3;
    private static final int RAND_PART_C_STATE = 4;
    private static final int NO_RAND_PART_A_STATE = 5;
    private static final int NO_RAND_PART_B_STATE = 6;
    private static final int NO_RAND_PART_C_STATE = 7;
    private int last;
    private int origPtr;
    private int blockSize100k;
    private boolean blockRandomised;
    private int bsBuff;
    private int bsLive;
    private CRC mCrc;
    private boolean[] inUse;
    private int nInUse;
    private char[] seqToUnseq;
    private char[] unseqToSeq;
    private char[] selector;
    private char[] selectorMtf;
    private int[] tt;
    private char[] ll8;
    private int[] unzftab;
    private int[][] limit;
    private int[][] base;
    private int[][] perm;
    private int[] minLens;
    private InputStream bsStream;
    private boolean streamEnd;
    private int currentChar;
    private int currentState;
    private int storedBlockCRC;
    private int storedCombinedCRC;
    private int computedBlockCRC;
    private int computedCombinedCRC;
    int i2;
    int count;
    int chPrev;
    int ch2;
    int i;
    int tPos;
    int rNToGo;
    int rTPos;
    int j2;
    char z;
    
    public CBZip2InputStream() {
        this.mCrc = new CRC();
        this.inUse = new boolean[256];
        this.seqToUnseq = new char[256];
        this.unseqToSeq = new char[256];
        this.selector = new char[18002];
        this.selectorMtf = new char[18002];
        this.unzftab = new int[256];
        this.limit = new int[6][258];
        this.base = new int[6][258];
        this.perm = new int[6][258];
        this.minLens = new int[6];
        this.streamEnd = false;
        this.currentChar = -1;
        this.currentState = 1;
        this.rNToGo = 0;
        this.rTPos = 0;
    }
    
    public CBZip2InputStream(final InputStream zStream) {
        this.mCrc = new CRC();
        this.inUse = new boolean[256];
        this.seqToUnseq = new char[256];
        this.unseqToSeq = new char[256];
        this.selector = new char[18002];
        this.selectorMtf = new char[18002];
        this.unzftab = new int[256];
        this.limit = new int[6][258];
        this.base = new int[6][258];
        this.perm = new int[6][258];
        this.minLens = new int[6];
        this.streamEnd = false;
        this.currentChar = -1;
        this.currentState = 1;
        this.rNToGo = 0;
        this.rTPos = 0;
        this.setStream(zStream);
    }
    
    public CBZip2InputStream(final InputStream zStream, final boolean skip) throws IOException {
        this.mCrc = new CRC();
        this.inUse = new boolean[256];
        this.seqToUnseq = new char[256];
        this.unseqToSeq = new char[256];
        this.selector = new char[18002];
        this.selectorMtf = new char[18002];
        this.unzftab = new int[256];
        this.limit = new int[6][258];
        this.base = new int[6][258];
        this.perm = new int[6][258];
        this.minLens = new int[6];
        this.streamEnd = false;
        this.currentChar = -1;
        this.currentState = 1;
        this.rNToGo = 0;
        this.rTPos = 0;
        if (skip) {
            final char c = (char)zStream.read();
            final char c2 = (char)zStream.read();
        }
        this.setStream(zStream);
    }
    
    public void setStream(final InputStream zStream) {
        this.last = 0;
        this.origPtr = 0;
        this.blockSize100k = 0;
        this.blockRandomised = false;
        this.bsBuff = 0;
        this.bsLive = 0;
        this.mCrc = new CRC();
        this.nInUse = 0;
        this.bsStream = null;
        this.streamEnd = false;
        this.currentChar = -1;
        this.currentState = 1;
        final int n = 0;
        this.storedCombinedCRC = n;
        this.storedBlockCRC = n;
        final int n2 = 0;
        this.computedCombinedCRC = n2;
        this.computedBlockCRC = n2;
        final int n3 = 0;
        this.ch2 = n3;
        this.chPrev = n3;
        this.count = n3;
        this.i2 = n3;
        final int n4 = 0;
        this.tPos = n4;
        this.i = n4;
        this.rNToGo = 0;
        this.rTPos = 0;
        this.j2 = 0;
        this.z = '\0';
        this.bsSetStream(zStream);
        this.initialize();
        this.initBlock();
        this.setupBlock();
    }
    
    @Override
    public int read() {
        if (this.streamEnd) {
            return -1;
        }
        final int retChar = this.currentChar;
        switch (this.currentState) {
            case 1: {}
            case 3: {
                this.setupRandPartB();
                break;
            }
            case 4: {
                this.setupRandPartC();
            }
            case 6: {
                this.setupNoRandPartB();
                break;
            }
            case 7: {
                this.setupNoRandPartC();
                break;
            }
        }
        return retChar;
    }
    
    private void initialize() {
        final char magic3 = this.bsGetUChar();
        final char magic4 = this.bsGetUChar();
        if (magic3 != 'h' || magic4 < '1' || magic4 > '9') {
            this.bsFinishedWithStream();
            this.streamEnd = true;
            return;
        }
        this.setDecompressStructureSizes(magic4 - '0');
        this.computedCombinedCRC = 0;
    }
    
    private void initBlock() {
        final char magic1 = this.bsGetUChar();
        final char magic2 = this.bsGetUChar();
        final char magic3 = this.bsGetUChar();
        final char magic4 = this.bsGetUChar();
        final char magic5 = this.bsGetUChar();
        final char magic6 = this.bsGetUChar();
        if (magic1 == '\u0017' && magic2 == 'r' && magic3 == 'E' && magic4 == '8' && magic5 == 'P' && magic6 == '\u0090') {
            this.complete();
            return;
        }
        if (magic1 != '1' || magic2 != 'A' || magic3 != 'Y' || magic4 != '&' || magic5 != 'S' || magic6 != 'Y') {
            this.badBlockHeader();
            this.streamEnd = true;
            return;
        }
        this.storedBlockCRC = this.bsGetInt32();
        if (this.bsR(1) == 1) {
            this.blockRandomised = true;
        }
        else {
            this.blockRandomised = false;
        }
        this.getAndMoveToFrontDecode();
        this.mCrc.initialiseCRC();
        this.currentState = 1;
    }
    
    private void endBlock() {
        this.computedBlockCRC = this.mCrc.getFinalCRC();
        if (this.storedBlockCRC != this.computedBlockCRC) {
            this.cadvise("CRC error: storedBlockCRC != computedBlockCRC");
        }
        this.computedCombinedCRC = (this.computedCombinedCRC << 1 | this.computedCombinedCRC >>> 31);
        this.computedCombinedCRC ^= this.computedBlockCRC;
    }
    
    private void complete() {
        this.storedCombinedCRC = this.bsGetInt32();
        if (this.storedCombinedCRC != this.computedCombinedCRC) {
            this.cadvise("CRC error: storedCombinedCRC != computedCombinedCRC");
        }
        this.bsFinishedWithStream();
        this.streamEnd = true;
    }
    
    private void blockOverrun() {
        this.cadvise("Block Overrun");
    }
    
    private void badBlockHeader() {
        this.cadvise("Bad Block Header");
    }
    
    private void crcError() {
        this.cadvise();
    }
    
    private void bsFinishedWithStream() {
        try {
            if (this.bsStream != null && this.bsStream != System.in) {
                this.bsStream.close();
                this.bsStream = null;
            }
        }
        catch (IOException ex) {}
    }
    
    private void bsSetStream(final InputStream f) {
        this.bsStream = f;
        this.bsLive = 0;
        this.bsBuff = 0;
    }
    
    private int bsR(final int n) {
        while (this.bsLive < n) {
            char thech = '\0';
            try {
                thech = (char)this.bsStream.read();
            }
            catch (IOException e) {
                this.compressedStreamEOF();
            }
            if (thech == -1) {
                this.compressedStreamEOF();
            }
            final int zzi = thech;
            this.bsBuff = (this.bsBuff << 8 | (zzi & 0xFF));
            this.bsLive += 8;
        }
        final int v = this.bsBuff >> this.bsLive - n & (1 << n) - 1;
        this.bsLive -= n;
        return v;
    }
    
    private char bsGetUChar() {
        return (char)this.bsR(8);
    }
    
    private int bsGetint() {
        int u = 0;
        u = (u << 8 | this.bsR(8));
        u = (u << 8 | this.bsR(8));
        u = (u << 8 | this.bsR(8));
        u = (u << 8 | this.bsR(8));
        return u;
    }
    
    private int bsGetIntVS(final int numBits) {
        return this.bsR(numBits);
    }
    
    private int bsGetInt32() {
        return this.bsGetint();
    }
    
    private void hbCreateDecodeTables(final int[] limit, final int[] base, final int[] perm, final char[] length, final int minLen, final int maxLen, final int alphaSize) {
        int pp = 0;
        for (int i = minLen; i <= maxLen; ++i) {
            for (int j = 0; j < alphaSize; ++j) {
                if (length[j] == i) {
                    perm[pp] = j;
                    ++pp;
                }
            }
        }
        for (int i = 0; i < 23; ++i) {
            base[i] = 0;
        }
        for (int i = 0; i < alphaSize; ++i) {
            final int n = length[i] + '\u0001';
            ++base[n];
        }
        for (int i = 1; i < 23; ++i) {
            final int n2 = i;
            base[n2] += base[i - 1];
        }
        for (int i = 0; i < 23; ++i) {
            limit[i] = 0;
        }
        int vec = 0;
        for (int i = minLen; i <= maxLen; ++i) {
            vec += base[i + 1] - base[i];
            limit[i] = vec - 1;
            vec <<= 1;
        }
        for (int i = minLen + 1; i <= maxLen; ++i) {
            base[i] = (limit[i - 1] + 1 << 1) - base[i];
        }
    }
    
    private void recvDecodingTables() {
        final char[][] len = new char[6][258];
        final boolean[] inUse16 = new boolean[16];
        for (int i = 0; i < 16; ++i) {
            if (this.bsR(1) == 1) {
                inUse16[i] = true;
            }
            else {
                inUse16[i] = false;
            }
        }
        for (int i = 0; i < 256; ++i) {
            this.inUse[i] = false;
        }
        for (int i = 0; i < 16; ++i) {
            if (inUse16[i]) {
                for (int j = 0; j < 16; ++j) {
                    if (this.bsR(1) == 1) {
                        this.inUse[i * 16 + j] = true;
                    }
                }
            }
        }
        this.makeMaps();
        final int alphaSize = this.nInUse + 2;
        final int nGroups = this.bsR(3);
        final int nSelectors = this.bsR(15);
        for (int i = 0; i < nSelectors; ++i) {
            int j = 0;
            while (this.bsR(1) == 1) {
                ++j;
            }
            this.selectorMtf[i] = (char)j;
        }
        final char[] pos = new char[6];
        for (char v = '\0'; v < nGroups; ++v) {
            pos[v] = v;
        }
        for (int i = 0; i < nSelectors; ++i) {
            char v = this.selectorMtf[i];
            final char tmp = pos[v];
            while (v > '\0') {
                pos[v] = pos[v - '\u0001'];
                --v;
            }
            pos[0] = tmp;
            this.selector[i] = tmp;
        }
        for (int t = 0; t < nGroups; ++t) {
            int curr = this.bsR(5);
            for (int i = 0; i < alphaSize; ++i) {
                while (this.bsR(1) == 1) {
                    if (this.bsR(1) == 0) {
                        ++curr;
                    }
                    else {
                        --curr;
                    }
                }
                len[t][i] = (char)curr;
            }
        }
        for (int t = 0; t < nGroups; ++t) {
            int minLen = 32;
            int maxLen = 0;
            for (int i = 0; i < alphaSize; ++i) {
                if (len[t][i] > maxLen) {
                    maxLen = len[t][i];
                }
                if (len[t][i] < minLen) {
                    minLen = len[t][i];
                }
            }
            this.hbCreateDecodeTables(this.limit[t], this.base[t], this.perm[t], len[t], minLen, maxLen, alphaSize);
            this.minLens[t] = minLen;
        }
    }
    
    private void getAndMoveToFrontDecode() {
        final char[] yy = new char[256];
        final int limitLast = 100000 * this.blockSize100k;
        this.origPtr = this.bsGetIntVS(24);
        this.recvDecodingTables();
        final int EOB = this.nInUse + 1;
        int groupNo = -1;
        int groupPos = 0;
        for (int i = 0; i <= 255; ++i) {
            this.unzftab[i] = 0;
        }
        for (int i = 0; i <= 255; ++i) {
            yy[i] = (char)i;
        }
        this.last = -1;
        if (groupPos == 0) {
            ++groupNo;
            groupPos = 50;
        }
        --groupPos;
        int zt;
        int zn;
        int zvec;
        int zj;
        for (zt = this.selector[groupNo], zn = this.minLens[zt], zvec = this.bsR(zn); zvec > this.limit[zt][zn]; zvec = (zvec << 1 | zj)) {
            ++zn;
            while (this.bsLive < 1) {
                char thech = '\0';
                try {
                    thech = (char)this.bsStream.read();
                }
                catch (IOException e) {
                    this.compressedStreamEOF();
                }
                if (thech == -1) {
                    this.compressedStreamEOF();
                }
                final int zzi = thech;
                this.bsBuff = (this.bsBuff << 8 | (zzi & 0xFF));
                this.bsLive += 8;
            }
            zj = (this.bsBuff >> this.bsLive - 1 & 0x1);
            --this.bsLive;
        }
        int nextSym = this.perm[zt][zvec - this.base[zt][zn]];
        while (nextSym != EOB) {
            if (nextSym == 0 || nextSym == 1) {
                int s = -1;
                int N = 1;
                do {
                    if (nextSym == 0) {
                        s += 1 * N;
                    }
                    else if (nextSym == 1) {
                        s += 2 * N;
                    }
                    N *= 2;
                    if (groupPos == 0) {
                        ++groupNo;
                        groupPos = 50;
                    }
                    --groupPos;
                    int zt2;
                    int zn2;
                    int zvec2;
                    int zj2;
                    for (zt2 = this.selector[groupNo], zn2 = this.minLens[zt2], zvec2 = this.bsR(zn2); zvec2 > this.limit[zt2][zn2]; zvec2 = (zvec2 << 1 | zj2)) {
                        ++zn2;
                        while (this.bsLive < 1) {
                            char thech2 = '\0';
                            try {
                                thech2 = (char)this.bsStream.read();
                            }
                            catch (IOException e2) {
                                this.compressedStreamEOF();
                            }
                            if (thech2 == -1) {
                                this.compressedStreamEOF();
                            }
                            final int zzi2 = thech2;
                            this.bsBuff = (this.bsBuff << 8 | (zzi2 & 0xFF));
                            this.bsLive += 8;
                        }
                        zj2 = (this.bsBuff >> this.bsLive - 1 & 0x1);
                        --this.bsLive;
                    }
                    nextSym = this.perm[zt2][zvec2 - this.base[zt2][zn2]];
                } while (nextSym == 0 || nextSym == 1);
                ++s;
                final char ch = this.seqToUnseq[yy[0]];
                final int[] unzftab = this.unzftab;
                final char c = ch;
                unzftab[c] += s;
                while (s > 0) {
                    ++this.last;
                    this.ll8[this.last] = ch;
                    --s;
                }
                if (this.last < limitLast) {
                    continue;
                }
                this.blockOverrun();
            }
            else {
                ++this.last;
                if (this.last >= limitLast) {
                    this.blockOverrun();
                }
                final char tmp = yy[nextSym - 1];
                final int[] unzftab2 = this.unzftab;
                final char c2 = this.seqToUnseq[tmp];
                ++unzftab2[c2];
                this.ll8[this.last] = this.seqToUnseq[tmp];
                int j;
                for (j = nextSym - 1; j > 3; j -= 4) {
                    yy[j] = yy[j - 1];
                    yy[j - 1] = yy[j - 2];
                    yy[j - 2] = yy[j - 3];
                    yy[j - 3] = yy[j - 4];
                }
                while (j > 0) {
                    yy[j] = yy[j - 1];
                    --j;
                }
                yy[0] = tmp;
                if (groupPos == 0) {
                    ++groupNo;
                    groupPos = 50;
                }
                --groupPos;
                int zt3;
                int zn3;
                int zvec3;
                int zj3;
                for (zt3 = this.selector[groupNo], zn3 = this.minLens[zt3], zvec3 = this.bsR(zn3); zvec3 > this.limit[zt3][zn3]; zvec3 = (zvec3 << 1 | zj3)) {
                    ++zn3;
                    while (this.bsLive < 1) {
                        char thech3 = '\0';
                        try {
                            thech3 = (char)this.bsStream.read();
                        }
                        catch (IOException e3) {
                            this.compressedStreamEOF();
                        }
                        final int zzi3 = thech3;
                        this.bsBuff = (this.bsBuff << 8 | (zzi3 & 0xFF));
                        this.bsLive += 8;
                    }
                    zj3 = (this.bsBuff >> this.bsLive - 1 & 0x1);
                    --this.bsLive;
                }
                nextSym = this.perm[zt3][zvec3 - this.base[zt3][zn3]];
            }
        }
    }
    
    private void setupBlock() {
        int[] cftab = new int[257];
        cftab[0] = 0;
        this.i = 1;
        while (this.i <= 256) {
            cftab[this.i] = this.unzftab[this.i - 1];
            ++this.i;
        }
        this.i = 1;
        while (this.i <= 256) {
            final int[] array = cftab;
            final int i = this.i;
            array[i] += cftab[this.i - 1];
            ++this.i;
        }
        this.i = 0;
        while (this.i <= this.last) {
            final char ch = this.ll8[this.i];
            this.tt[cftab[ch]] = this.i;
            final int[] array2 = cftab;
            final char c = ch;
            ++array2[c];
            ++this.i;
        }
        cftab = null;
        this.tPos = this.tt[this.origPtr];
        this.count = 0;
        this.i2 = 0;
        this.ch2 = 256;
        if (this.blockRandomised) {
            this.rNToGo = 0;
            this.rTPos = 0;
            this.setupRandPartA();
        }
        else {
            this.setupNoRandPartA();
        }
    }
    
    private void setupRandPartA() {
        if (this.i2 <= this.last) {
            this.chPrev = this.ch2;
            this.ch2 = this.ll8[this.tPos];
            this.tPos = this.tt[this.tPos];
            if (this.rNToGo == 0) {
                this.rNToGo = CBZip2InputStream.rNums[this.rTPos];
                ++this.rTPos;
                if (this.rTPos == 512) {
                    this.rTPos = 0;
                }
            }
            --this.rNToGo;
            this.ch2 ^= ((this.rNToGo == 1) ? 1 : 0);
            ++this.i2;
            this.currentChar = this.ch2;
            this.currentState = 3;
            this.mCrc.updateCRC(this.ch2);
        }
        else {
            this.endBlock();
            this.initBlock();
            this.setupBlock();
        }
    }
    
    private void setupNoRandPartA() {
        if (this.i2 <= this.last) {
            this.chPrev = this.ch2;
            this.ch2 = this.ll8[this.tPos];
            this.tPos = this.tt[this.tPos];
            ++this.i2;
            this.currentChar = this.ch2;
            this.currentState = 6;
            this.mCrc.updateCRC(this.ch2);
        }
        else {
            this.endBlock();
            this.initBlock();
            this.setupBlock();
        }
    }
    
    private void setupRandPartB() {
        if (this.ch2 != this.chPrev) {
            this.currentState = 2;
            this.count = 1;
            this.setupRandPartA();
        }
        else {
            ++this.count;
            if (this.count >= 4) {
                this.z = this.ll8[this.tPos];
                this.tPos = this.tt[this.tPos];
                if (this.rNToGo == 0) {
                    this.rNToGo = CBZip2InputStream.rNums[this.rTPos];
                    ++this.rTPos;
                    if (this.rTPos == 512) {
                        this.rTPos = 0;
                    }
                }
                --this.rNToGo;
                this.z ^= (char)((this.rNToGo == 1) ? 1 : 0);
                this.j2 = 0;
                this.currentState = 4;
                this.setupRandPartC();
            }
            else {
                this.currentState = 2;
                this.setupRandPartA();
            }
        }
    }
    
    private void setupRandPartC() {
        if (this.j2 < this.z) {
            this.currentChar = this.ch2;
            this.mCrc.updateCRC(this.ch2);
            ++this.j2;
        }
        else {
            this.currentState = 2;
            ++this.i2;
            this.count = 0;
            this.setupRandPartA();
        }
    }
    
    private void setupNoRandPartB() {
        if (this.ch2 != this.chPrev) {
            this.currentState = 5;
            this.count = 1;
            this.setupNoRandPartA();
        }
        else {
            ++this.count;
            if (this.count >= 4) {
                this.z = this.ll8[this.tPos];
                this.tPos = this.tt[this.tPos];
                this.currentState = 7;
                this.j2 = 0;
                this.setupNoRandPartC();
            }
            else {
                this.currentState = 5;
                this.setupNoRandPartA();
            }
        }
    }
    
    private void setupNoRandPartC() {
        if (this.j2 < this.z) {
            this.currentChar = this.ch2;
            this.mCrc.updateCRC(this.ch2);
            ++this.j2;
        }
        else {
            this.currentState = 5;
            ++this.i2;
            this.count = 0;
            this.setupNoRandPartA();
        }
    }
    
    private void setDecompressStructureSizes(final int newSize100k) {
        if (0 > newSize100k || newSize100k > 9 || 0 > this.blockSize100k || this.blockSize100k > 9) {}
        if ((this.blockSize100k = newSize100k) == 0) {
            return;
        }
        final int n = 100000 * newSize100k;
        if (this.ll8 != null && this.ll8.length != n) {
            this.ll8 = null;
        }
        if (this.tt != null && this.tt.length != n) {
            this.tt = null;
        }
        if (this.ll8 == null) {
            this.ll8 = new char[n];
        }
        if (this.tt == null) {
            this.tt = new int[n];
        }
    }
    
    private void cadvise() {
        System.out.println("CRC Error");
    }
    
    private void cadvise(final String msg) {
        throw new BZip2ReadException(msg);
    }
    
    private void compressedStreamEOF() {
        this.cadvise("Compressed Stream EOF");
    }
    
    private void makeMaps() {
        this.nInUse = 0;
        for (int i = 0; i < 256; ++i) {
            if (this.inUse[i]) {
                this.seqToUnseq[this.nInUse] = (char)i;
                this.unseqToSeq[i] = (char)this.nInUse;
                ++this.nInUse;
            }
        }
    }
}
