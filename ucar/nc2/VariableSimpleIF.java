// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2;

import ucar.ma2.DataType;
import java.util.List;

public interface VariableSimpleIF extends Comparable<VariableSimpleIF>
{
    String getName();
    
    String getShortName();
    
    String getDescription();
    
    String getUnitsString();
    
    int getRank();
    
    int[] getShape();
    
    List<Dimension> getDimensions();
    
    DataType getDataType();
    
    List<Attribute> getAttributes();
    
    Attribute findAttributeIgnoreCase(final String p0);
}
