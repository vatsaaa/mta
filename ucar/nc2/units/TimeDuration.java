// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.units;

import ucar.units.ConversionException;
import javax.xml.datatype.Duration;
import java.text.ParseException;
import java.util.Date;
import javax.xml.datatype.DatatypeFactory;

public class TimeDuration
{
    private String text;
    private TimeUnit timeUnit;
    private boolean isBlank;
    private static boolean debug;
    
    private TimeDuration() {
    }
    
    public TimeDuration(final TimeDuration src) {
        this.text = src.getText();
        this.timeUnit = new TimeUnit(src.getTimeUnit());
        this.isBlank = src.isBlank();
    }
    
    public TimeDuration(final TimeUnit timeUnit) {
        this.timeUnit = timeUnit;
        this.text = timeUnit.toString();
    }
    
    public TimeDuration(String text) throws ParseException {
        text = ((text == null) ? "" : text.trim());
        this.text = text;
        if (text.length() == 0) {
            this.isBlank = true;
            try {
                this.timeUnit = new TimeUnit("1 sec");
            }
            catch (Exception ex) {}
            return;
        }
        try {
            this.timeUnit = new TimeUnit(text);
            if (TimeDuration.debug) {
                System.out.println(" set time unit= " + this.timeUnit);
            }
        }
        catch (Exception e) {
            try {
                final DatatypeFactory factory = DatatypeFactory.newInstance();
                final Duration d = factory.newDuration(text);
                final long secs = d.getTimeInMillis(new Date()) / 1000L;
                this.timeUnit = new TimeUnit(secs + " secs");
            }
            catch (Exception e2) {
                throw new ParseException(e.getMessage(), 0);
            }
        }
    }
    
    public static TimeDuration parseW3CDuration(String text) throws ParseException {
        final TimeDuration td = new TimeDuration();
        text = ((text == null) ? "" : text.trim());
        td.text = text;
        try {
            final DatatypeFactory factory = DatatypeFactory.newInstance();
            final Duration d = factory.newDuration(text);
            final long secs = d.getTimeInMillis(new Date()) / 1000L;
            td.timeUnit = new TimeUnit(secs + " secs");
        }
        catch (Exception e) {
            throw new ParseException(e.getMessage(), 0);
        }
        return td;
    }
    
    public double getValue() {
        return this.timeUnit.getValue();
    }
    
    public double getValue(final TimeUnit want) throws ConversionException {
        return this.timeUnit.convertTo(this.timeUnit.getValue(), want);
    }
    
    public double getValueInSeconds() {
        return this.timeUnit.getValueInSeconds();
    }
    
    public void setValueInSeconds(final double secs) {
        this.timeUnit.setValueInSeconds(secs);
        this.text = null;
    }
    
    public boolean isBlank() {
        return this.isBlank;
    }
    
    public TimeUnit getTimeUnit() {
        return this.timeUnit;
    }
    
    public String getText() {
        return (this.text == null) ? this.timeUnit.toString() : this.text;
    }
    
    @Override
    public String toString() {
        return this.getText();
    }
    
    @Override
    public int hashCode() {
        return this.isBlank() ? 0 : ((int)this.getValueInSeconds());
    }
    
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof TimeDuration)) {
            return false;
        }
        final TimeDuration to = (TimeDuration)o;
        return to.getValueInSeconds() == this.getValueInSeconds();
    }
    
    private static void doDuration(final String s) {
        try {
            System.out.println("start = (" + s + ")");
            final TimeDuration d = new TimeDuration(s);
            System.out.println("duration = (" + d.toString() + ")");
        }
        catch (ParseException e) {
            e.printStackTrace();
        }
    }
    
    public static void main(final String[] args) {
        doDuration("3 days");
    }
    
    static {
        TimeDuration.debug = false;
    }
}
