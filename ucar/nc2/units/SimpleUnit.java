// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.units;

import ucar.units.UnitDB;
import ucar.units.UnitName;
import ucar.units.UnitDBManager;
import ucar.units.Factor;
import ucar.units.UnitDimension;
import ucar.units.ScaledUnit;
import ucar.units.DerivedUnit;
import ucar.units.UnknownUnit;
import ucar.units.ConversionException;
import ucar.units.UnitFormat;
import ucar.units.UnitFormatManager;
import ucar.units.Unit;

public class SimpleUnit
{
    public static final SimpleUnit kmUnit;
    public static final SimpleUnit meterUnit;
    public static final SimpleUnit pressureUnit;
    protected static Unit secsUnit;
    protected static Unit dateReferenceUnit;
    protected static boolean debugParse;
    protected Unit uu;
    
    public static SimpleUnit factory(final String name) {
        try {
            return factoryWithExceptions(name);
        }
        catch (Exception e) {
            if (SimpleUnit.debugParse) {
                System.out.println("Parse " + name + " got Exception " + e);
            }
            return null;
        }
    }
    
    public static SimpleUnit factoryWithExceptions(final String name) throws Exception {
        final UnitFormat format = UnitFormatManager.instance();
        final Unit uu = format.parse(name);
        if (isTimeUnit(uu)) {
            return new TimeUnit(name);
        }
        return new SimpleUnit(uu);
    }
    
    protected static Unit makeUnit(final String name) throws Exception {
        final UnitFormat format = UnitFormatManager.instance();
        return format.parse(name);
    }
    
    public static boolean isCompatible(final String unitString1, final String unitString2) {
        Unit uu1;
        try {
            final UnitFormat format = UnitFormatManager.instance();
            uu1 = format.parse(unitString1);
        }
        catch (Exception e) {
            if (SimpleUnit.debugParse) {
                System.out.println("Parse " + unitString1 + " got Exception1 " + e);
            }
            return false;
        }
        Unit uu2;
        try {
            final UnitFormat format = UnitFormatManager.instance();
            uu2 = format.parse(unitString2);
        }
        catch (Exception e) {
            if (SimpleUnit.debugParse) {
                System.out.println("Parse " + unitString2 + " got Exception2 " + e);
            }
            return false;
        }
        return uu1.isCompatible(uu2);
    }
    
    public static boolean isCompatibleWithExceptions(final String unitString1, final String unitString2) throws Exception {
        final UnitFormat format = UnitFormatManager.instance();
        final Unit uu1 = format.parse(unitString1);
        final Unit uu2 = format.parse(unitString2);
        return uu1.isCompatible(uu2);
    }
    
    public static boolean isDateUnit(final Unit uu) {
        final boolean ok = uu.isCompatible(SimpleUnit.dateReferenceUnit);
        if (!ok) {
            return false;
        }
        try {
            uu.getConverterTo(SimpleUnit.dateReferenceUnit);
            return true;
        }
        catch (ConversionException e) {
            return false;
        }
    }
    
    public static boolean isTimeUnit(final Unit uu) {
        return uu.isCompatible(SimpleUnit.secsUnit);
    }
    
    public static boolean isDateUnit(final String unitString) {
        final SimpleUnit su = factory(unitString);
        return su != null && isDateUnit(su.getUnit());
    }
    
    public static boolean isTimeUnit(final String unitString) {
        final SimpleUnit su = factory(unitString);
        return su != null && isTimeUnit(su.getUnit());
    }
    
    public static double getConversionFactor(final String inputUnitString, final String outputUnitString) throws IllegalArgumentException {
        final SimpleUnit inputUnit = factory(inputUnitString);
        final SimpleUnit outputUnit = factory(outputUnitString);
        return inputUnit.convertTo(1.0, outputUnit);
    }
    
    protected SimpleUnit() {
        this.uu = null;
    }
    
    SimpleUnit(final Unit uu) {
        this.uu = null;
        this.uu = uu;
    }
    
    @Override
    public String toString() {
        return this.uu.toString();
    }
    
    public Unit getUnit() {
        return this.uu;
    }
    
    public double convertTo(final double value, final SimpleUnit outputUnit) throws IllegalArgumentException {
        try {
            return this.uu.convertTo(value, outputUnit.getUnit());
        }
        catch (ConversionException e) {
            throw new IllegalArgumentException(e.getMessage());
        }
    }
    
    public boolean isCompatible(final String unitString) {
        Unit uuWant;
        try {
            final UnitFormat format = UnitFormatManager.instance();
            uuWant = format.parse(unitString);
        }
        catch (Exception e) {
            if (SimpleUnit.debugParse) {
                System.out.println("Parse " + unitString + " got Exception1 " + e);
            }
            return false;
        }
        return this.uu.isCompatible(uuWant);
    }
    
    public boolean isUnknownUnit() {
        final Unit uu = this.getUnit();
        if (uu instanceof UnknownUnit) {
            return true;
        }
        if (uu instanceof DerivedUnit) {
            return this.isUnknownUnit((DerivedUnit)uu);
        }
        if (uu instanceof ScaledUnit) {
            final ScaledUnit scu = (ScaledUnit)uu;
            final Unit u = scu.getUnit();
            if (u instanceof UnknownUnit) {
                return true;
            }
            if (u instanceof DerivedUnit) {
                return this.isUnknownUnit((DerivedUnit)u);
            }
        }
        return false;
    }
    
    private boolean isUnknownUnit(final DerivedUnit du) {
        final UnitDimension dim = du.getDimension();
        for (final Factor f : dim.getFactors()) {
            if (f.getBase() instanceof UnknownUnit) {
                return true;
            }
        }
        return false;
    }
    
    public double getValue() {
        if (!(this.uu instanceof ScaledUnit)) {
            return Double.NaN;
        }
        final ScaledUnit offset = (ScaledUnit)this.uu;
        return offset.getScale();
    }
    
    public String getUnitString() {
        return this.uu.getDerivedUnit().toString();
    }
    
    public String getCanonicalString() {
        return this.uu.getCanonicalString();
    }
    
    public String getImplementingClass() {
        return this.uu.getClass().getName();
    }
    
    static {
        SimpleUnit.debugParse = false;
        try {
            final UnitFormat udunit = UnitFormatManager.instance();
            SimpleUnit.secsUnit = udunit.parse("sec");
            SimpleUnit.dateReferenceUnit = udunit.parse("ms since 1970-01-01");
            final UnitDB unitDB = UnitDBManager.instance();
            final Unit u = udunit.parse("millibar");
            final Unit alias = u.clone(UnitName.newUnitName("mb"));
            unitDB.addUnit(alias);
            kmUnit = factoryWithExceptions("km");
            meterUnit = factoryWithExceptions("m");
            pressureUnit = factoryWithExceptions("Pa");
        }
        catch (Exception e) {
            System.out.println("SimpleUnit initialization failed " + e);
            throw new RuntimeException("SimpleUnit initialization failed " + e);
        }
    }
}
