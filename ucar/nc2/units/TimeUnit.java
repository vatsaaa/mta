// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.units;

import java.util.Calendar;
import java.util.Date;
import ucar.units.ConversionException;
import ucar.unidata.util.Format;
import java.util.StringTokenizer;

public class TimeUnit extends SimpleUnit
{
    private double value;
    private double factor;
    private String unitString;
    private int hashCode;
    
    public TimeUnit(final String text) throws Exception {
        this.factor = 1.0;
        this.hashCode = 0;
        if (text == null) {
            this.value = 1.0;
            this.unitString = "secs";
            this.uu = SimpleUnit.makeUnit(this.unitString);
            return;
        }
        final StringTokenizer stoker = new StringTokenizer(text);
        final int ntoke = stoker.countTokens();
        if (ntoke == 1) {
            this.value = 1.0;
            this.unitString = stoker.nextToken();
        }
        else {
            if (ntoke != 2) {
                throw new IllegalArgumentException("Not TimeUnit = " + text);
            }
            this.value = Double.parseDouble(stoker.nextToken());
            this.unitString = stoker.nextToken();
        }
        this.uu = SimpleUnit.makeUnit(this.unitString);
        this.factor = this.uu.convertTo(1.0, SimpleUnit.secsUnit);
    }
    
    public TimeUnit(final double value, final String unitString) throws Exception {
        this.factor = 1.0;
        this.hashCode = 0;
        this.value = value;
        this.unitString = unitString;
        this.uu = SimpleUnit.makeUnit(unitString);
        this.factor = this.uu.convertTo(1.0, SimpleUnit.secsUnit);
    }
    
    public TimeUnit(final TimeUnit src) {
        this.factor = 1.0;
        this.hashCode = 0;
        this.value = src.getValue();
        this.unitString = src.getUnitString();
        this.uu = src.uu;
        this.factor = src.getFactor();
    }
    
    @Override
    public double getValue() {
        return this.value;
    }
    
    public double getFactor() {
        return this.factor;
    }
    
    public void setValue(final double value) {
        this.value = value;
    }
    
    @Override
    public String getUnitString() {
        return this.unitString;
    }
    
    @Override
    public String toString() {
        return Format.d(this.value, 5) + " " + this.unitString;
    }
    
    public double getValueInSeconds() {
        return this.factor * this.value;
    }
    
    public double getValueInSeconds(final double value) {
        return this.factor * value;
    }
    
    public void setValueInSeconds(final double secs) {
        this.value = secs / this.factor;
    }
    
    public double convertTo(final double value, final TimeUnit outputUnit) throws ConversionException {
        return this.uu.convertTo(value, outputUnit.uu);
    }
    
    public Date add(final Date d) {
        final Calendar cal = Calendar.getInstance();
        cal.setTime(d);
        cal.add(13, (int)this.getValueInSeconds());
        return cal.getTime();
    }
    
    @Override
    public boolean equals(final Object o) {
        return this == o || (o instanceof TimeUnit && o.hashCode() == this.hashCode());
    }
    
    @Override
    public int hashCode() {
        if (this.hashCode == 0) {
            int result = 17;
            result = 37 * result + this.unitString.hashCode();
            result = 37 * result + (int)(1000.0 * this.value);
            this.hashCode = result;
        }
        return this.hashCode;
    }
}
