// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.units;

import ucar.units.Converter;
import ucar.units.UnitFormat;
import ucar.units.UnitFormatManager;
import ucar.units.TimeScaleUnit;
import java.util.StringTokenizer;
import java.util.Date;
import ucar.units.Unit;

public class DateUnit
{
    private double value;
    private String udunitString;
    private TimeUnit timeUnit;
    private Unit uu;
    
    public static Date getStandardDate(String text) {
        text = text.trim();
        final StringTokenizer stoker = new StringTokenizer(text);
        final String firstToke = stoker.nextToken();
        double value;
        String udunitString;
        try {
            value = Double.parseDouble(firstToke);
            udunitString = text.substring(firstToke.length());
        }
        catch (NumberFormatException e) {
            value = 0.0;
            udunitString = text;
        }
        DateUnit du;
        try {
            du = new DateUnit(udunitString);
        }
        catch (Exception e2) {
            return null;
        }
        return du.makeDate(value);
    }
    
    public static Date getStandardOrISO(final String text) {
        Date result = getStandardDate(text);
        if (result == null) {
            final DateFormatter formatter = new DateFormatter();
            result = formatter.getISODate(text);
        }
        return result;
    }
    
    public static DateUnit getUnixDateUnit() {
        try {
            return new DateUnit("secs since 1970-00-00:00.00");
        }
        catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
    
    public DateUnit(String text) throws Exception {
        this.timeUnit = null;
        text = text.trim();
        final StringTokenizer stoker = new StringTokenizer(text);
        final String firstToke = stoker.nextToken();
        String timeUnitString;
        try {
            this.value = Double.parseDouble(firstToke);
            this.udunitString = text.substring(firstToke.length());
            timeUnitString = stoker.nextToken();
        }
        catch (NumberFormatException e) {
            this.value = 0.0;
            this.udunitString = text;
            timeUnitString = firstToke;
        }
        this.uu = SimpleUnit.makeUnit(this.udunitString);
        this.timeUnit = new TimeUnit(timeUnitString);
    }
    
    public DateUnit(final double value, final String timeUnitString, final Date since) throws Exception {
        this.timeUnit = null;
        this.value = value;
        final DateFormatter df = new DateFormatter();
        this.udunitString = timeUnitString + " since " + df.toDateTimeStringISO(since);
        this.uu = SimpleUnit.makeUnit(this.udunitString);
        this.timeUnit = new TimeUnit(timeUnitString);
    }
    
    public Date getDateOrigin() {
        if (!(this.uu instanceof TimeScaleUnit)) {
            return null;
        }
        final TimeScaleUnit tu = (TimeScaleUnit)this.uu;
        return tu.getOrigin();
    }
    
    public String getTimeUnitString() {
        return this.timeUnit.getUnitString();
    }
    
    public TimeUnit getTimeUnit() {
        return this.timeUnit;
    }
    
    public Date getDate() {
        final double secs = this.timeUnit.getValueInSeconds(this.value);
        return new Date(this.getDateOrigin().getTime() + (long)(1000.0 * secs));
    }
    
    public Date makeDate(final double val) {
        if (Double.isNaN(val)) {
            return null;
        }
        final double secs = this.timeUnit.getValueInSeconds(val);
        return new Date(this.getDateOrigin().getTime() + (long)(1000.0 * secs));
    }
    
    public double makeValue(final Date date) {
        final double secs = (double)(date.getTime() / 1000L);
        final double origin_secs = (double)(this.getDateOrigin().getTime() / 1000L);
        final double diff = secs - origin_secs;
        try {
            this.timeUnit.setValueInSeconds(diff);
        }
        catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }
        return this.timeUnit.getValue();
    }
    
    public String makeStandardDateString(final double value) {
        final Date date = this.makeDate(value);
        if (date == null) {
            return null;
        }
        final DateFormatter formatter = new DateFormatter();
        return formatter.toDateTimeStringISO(date);
    }
    
    @Override
    public String toString() {
        return this.value + " " + this.udunitString;
    }
    
    public String getUnitsString() {
        return this.udunitString;
    }
    
    public static void main(final String[] args) throws Exception {
        final UnitFormat udunit = UnitFormatManager.instance();
        final String text2 = "days since 2009-06-14 04:00:00 +00:00";
        final Unit uu2 = udunit.parse(text2);
        System.out.printf("%s == %s %n", text2, uu2);
        final String text3 = "days since 2009-06-14 04:00:00";
        final Unit uu3 = udunit.parse(text3);
        System.out.printf("%s == %s %n", text3, uu3);
        final Unit ref = udunit.parse("ms since 1970-01-01");
        final Converter converter = uu3.getConverterTo(ref);
        final DateFormatter formatter = new DateFormatter();
        final double val = converter.convert(1.0);
        final Date d = new Date((long)val);
        System.out.printf(" val=%f date=%s (%s)%n", 1.0, d, formatter.toDateTimeStringISO(d));
        final double val2 = converter.convert(2.0);
        final Date d2 = new Date((long)val2);
        System.out.printf(" val=%f date=%s (%s)%n", 2.0, d, formatter.toDateTimeStringISO(d2));
    }
}
