// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.units;

import java.text.ParseException;
import java.util.Date;
import java.util.TimeZone;
import java.text.SimpleDateFormat;

public class DateFormatter
{
    private SimpleDateFormat isoDateTimeFormat;
    private SimpleDateFormat isoDateNoSecsFormat;
    private SimpleDateFormat stdDateTimeFormat;
    private SimpleDateFormat stdDateNoSecsFormat;
    private SimpleDateFormat dateOnlyFormat;
    
    private void isoDateTimeFormat() {
        if (this.isoDateTimeFormat != null) {
            return;
        }
        (this.isoDateTimeFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")).setTimeZone(TimeZone.getTimeZone("GMT"));
    }
    
    private void isoDateNoSecsFormat() {
        if (this.isoDateNoSecsFormat != null) {
            return;
        }
        (this.isoDateNoSecsFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm")).setTimeZone(TimeZone.getTimeZone("GMT"));
    }
    
    private void stdDateTimeFormat() {
        if (this.stdDateTimeFormat != null) {
            return;
        }
        (this.stdDateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")).setTimeZone(TimeZone.getTimeZone("GMT"));
    }
    
    private void stdDateNoSecsFormat() {
        if (this.stdDateNoSecsFormat != null) {
            return;
        }
        (this.stdDateNoSecsFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm")).setTimeZone(TimeZone.getTimeZone("GMT"));
    }
    
    private void dateOnlyFormat() {
        if (this.dateOnlyFormat != null) {
            return;
        }
        (this.dateOnlyFormat = new SimpleDateFormat("yyyy-MM-dd")).setTimeZone(TimeZone.getTimeZone("GMT"));
    }
    
    public Date getISODate(final String text) {
        try {
            final Date result = this.stdDateTimeFormat(text);
            return result;
        }
        catch (ParseException e) {
            try {
                final Date result = this.isoDateTimeFormat(text);
                return result;
            }
            catch (ParseException e) {
                try {
                    final Date result = this.isoDateNoSecsFormat(text);
                    return result;
                }
                catch (ParseException e) {
                    try {
                        final Date result = this.stdDateNoSecsFormat(text);
                        return result;
                    }
                    catch (ParseException e) {
                        try {
                            final Date result = this.dateOnlyFormat(text);
                            return result;
                        }
                        catch (ParseException e) {
                            return null;
                        }
                    }
                }
            }
        }
    }
    
    public Date stdDateTimeFormat(String text) throws ParseException {
        text = ((text == null) ? "" : text.trim());
        this.stdDateTimeFormat();
        return this.stdDateTimeFormat.parse(text);
    }
    
    public Date stdDateNoSecsFormat(String text) throws ParseException {
        text = ((text == null) ? "" : text.trim());
        this.stdDateNoSecsFormat();
        return this.stdDateNoSecsFormat.parse(text);
    }
    
    public Date isoDateTimeFormat(String text) throws ParseException {
        text = ((text == null) ? "" : text.trim());
        this.isoDateTimeFormat();
        return this.isoDateTimeFormat.parse(text);
    }
    
    public Date isoDateNoSecsFormat(String text) throws ParseException {
        text = ((text == null) ? "" : text.trim());
        this.isoDateNoSecsFormat();
        return this.isoDateNoSecsFormat.parse(text);
    }
    
    public Date dateOnlyFormat(String text) throws ParseException {
        text = ((text == null) ? "" : text.trim());
        this.dateOnlyFormat();
        return this.dateOnlyFormat.parse(text);
    }
    
    @Deprecated
    public String getStandardDateOnlyString(final Date date) {
        return this.toDateOnlyString(date);
    }
    
    @Deprecated
    public String toDateString(final Date date) {
        return this.toDateOnlyString(date);
    }
    
    public String toDateOnlyString(final Date date) {
        this.dateOnlyFormat();
        return this.dateOnlyFormat.format(date);
    }
    
    @Deprecated
    public String getStandardDateString2(final Date date) {
        return this.toDateTimeString(date);
    }
    
    public String toDateTimeString(final Date date) {
        if (date == null) {
            return "Unknown";
        }
        this.stdDateTimeFormat();
        return this.stdDateTimeFormat.format(date) + "Z";
    }
    
    @Deprecated
    public String getStandardDateString(final Date date) {
        return this.toDateTimeStringISO(date);
    }
    
    public String toDateTimeStringISO(final Date date) {
        this.isoDateTimeFormat();
        return this.isoDateTimeFormat.format(date) + "Z";
    }
    
    private static void test(final String text) {
        final DateFormatter formatter = new DateFormatter();
        final Date date = formatter.getISODate(text);
        final String text2 = formatter.toDateTimeStringISO(date);
        final Date date2 = formatter.getISODate(text2);
        assert date.equals(date2);
        System.out.println(text + " == " + text2);
    }
    
    public static void main(final String[] args) {
        test("2001-09-11T12:09:20");
        test("2001-09-11 12:10:12");
        test("2001-09-11T12:10");
        test("2001-09-11 12:01");
        test("2001-09-11");
    }
}
