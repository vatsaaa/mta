// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.units;

import java.text.ParseException;
import java.util.Date;

public class DateRange
{
    private DateType start;
    private DateType end;
    private TimeDuration duration;
    private TimeDuration resolution;
    private boolean isEmpty;
    private boolean isMoving;
    private boolean useStart;
    private boolean useEnd;
    private boolean useDuration;
    private boolean useResolution;
    private int hashCode;
    
    public DateRange() throws ParseException {
        this(null, new DateType(false, new Date()), new TimeDuration("1 day"), new TimeDuration("15 min"));
    }
    
    public DateRange(final Date start, final Date end) {
        this(new DateType(false, start), new DateType(false, end), null, null);
    }
    
    public DateRange(final Date start, final TimeDuration duration) {
        this(new DateType(false, start), null, duration, null);
    }
    
    public DateRange(final DateRange range, final String timeUnits) throws Exception {
        this(new DateType(false, range.getStart().getDate()), new DateType(false, range.getEnd().getDate()), null, new TimeDuration(timeUnits));
    }
    
    public DateRange(final DateType start, final DateType end, final TimeDuration duration, final TimeDuration resolution) {
        this.hashCode = 0;
        this.start = start;
        this.end = end;
        this.duration = duration;
        this.resolution = resolution;
        this.useStart = (start != null && !start.isBlank());
        this.useEnd = (end != null && !end.isBlank());
        this.useDuration = (duration != null);
        this.useResolution = (resolution != null);
        boolean invalid = true;
        if (this.useStart && this.useEnd) {
            invalid = false;
            this.isMoving = (this.start.isPresent() || this.end.isPresent());
            this.useDuration = false;
            this.recalcDuration();
        }
        else if (this.useStart && this.useDuration) {
            invalid = false;
            this.isMoving = this.start.isPresent();
            this.end = this.start.add(duration);
        }
        else if (this.useEnd && this.useDuration) {
            invalid = false;
            this.isMoving = this.end.isPresent();
            this.start = this.end.subtract(duration);
        }
        if (invalid) {
            throw new IllegalArgumentException("DateRange must have 2 of start, end, duration");
        }
        this.checkIfEmpty();
        this.hashCode = 0;
    }
    
    private void checkIfEmpty() {
        if (this.start.isPresent() && this.end.isPresent()) {
            this.isEmpty = true;
        }
        else if (this.start.isPresent() || this.end.isPresent()) {
            this.isEmpty = (this.duration.getValue() <= 0.0);
        }
        else {
            this.isEmpty = this.end.before(this.start);
        }
        if (this.isEmpty) {
            this.duration.setValueInSeconds(0.0);
        }
    }
    
    private String chooseResolution(double time) {
        if (time < 180.0) {
            return "secs";
        }
        time /= 60.0;
        if (time < 180.0) {
            return "minutes";
        }
        time /= 60.0;
        if (time < 72.0) {
            return "hours";
        }
        time /= 24.0;
        if (time < 90.0) {
            return "days";
        }
        time /= 30.0;
        if (time < 36.0) {
            return "months";
        }
        return "years";
    }
    
    public boolean included(final Date d) {
        return !this.isEmpty && !this.getStart().after(d) && !this.getEnd().before(d);
    }
    
    public boolean contains(final Date d) {
        return this.included(d);
    }
    
    public boolean intersects(final Date start_want, final Date end_want) {
        return !this.isEmpty && !this.getStart().after(end_want) && !this.getEnd().before(start_want);
    }
    
    public boolean intersects(final DateRange other) {
        return this.intersects(other.getStart().getDate(), other.getEnd().getDate());
    }
    
    public DateRange intersect(final DateRange clip) {
        if (this.isEmpty) {
            return this;
        }
        if (clip.isEmpty) {
            return clip;
        }
        final DateType ss = this.getStart();
        final DateType s = ss.before(clip.getStart()) ? clip.getStart() : ss;
        final DateType ee = this.getEnd();
        final DateType e = ee.before(clip.getEnd()) ? ee : clip.getEnd();
        return new DateRange(s, e, null, this.resolution);
    }
    
    public void extend(final DateRange dr) {
        final boolean localEmpty = this.isEmpty;
        if (localEmpty || dr.getStart().before(this.getStart())) {
            this.setStart(dr.getStart());
        }
        if (localEmpty || this.getEnd().before(dr.getEnd())) {
            this.setEnd(dr.getEnd());
        }
    }
    
    public void extend(final Date d) {
        if (d.before(this.getStart().getDate())) {
            this.setStart(new DateType(false, d));
        }
        if (this.getEnd().before(d)) {
            this.setEnd(new DateType(false, d));
        }
    }
    
    public DateType getStart() {
        return (this.isMoving && !this.useStart) ? this.end.subtract(this.duration) : this.start;
    }
    
    public void setStart(final DateType start) {
        this.start = start;
        this.useStart = true;
        if (this.useEnd) {
            this.isMoving = (this.start.isPresent() || this.end.isPresent());
            this.useDuration = false;
            this.recalcDuration();
        }
        else {
            this.isMoving = this.start.isPresent();
            this.end = this.start.add(this.duration);
        }
        this.checkIfEmpty();
    }
    
    public DateType getEnd() {
        return (this.isMoving && !this.useEnd) ? this.start.add(this.duration) : this.end;
    }
    
    public void setEnd(final DateType end) {
        this.end = end;
        this.useEnd = true;
        if (this.useStart) {
            this.isMoving = (this.start.isPresent() || this.end.isPresent());
            this.useDuration = false;
            this.recalcDuration();
        }
        else {
            this.isMoving = this.end.isPresent();
            this.start = this.end.subtract(this.duration);
        }
        this.checkIfEmpty();
    }
    
    public TimeDuration getDuration() {
        if (this.isMoving && !this.useDuration) {
            this.recalcDuration();
        }
        return this.duration;
    }
    
    public void setDuration(final TimeDuration duration) {
        this.duration = duration;
        this.useDuration = true;
        if (this.useStart) {
            this.isMoving = this.start.isPresent();
            this.end = this.start.add(duration);
            this.useEnd = false;
        }
        else {
            this.isMoving = this.end.isPresent();
            this.start = this.end.subtract(duration);
        }
        this.checkIfEmpty();
    }
    
    private void recalcDuration() {
        final long min = this.getStart().getDate().getTime();
        final long max = this.getEnd().getDate().getTime();
        double secs = 0.001 * (max - min);
        if (secs < 0.0) {
            secs = 0.0;
        }
        if (this.duration == null) {
            try {
                this.duration = new TimeDuration(this.chooseResolution(secs));
            }
            catch (ParseException ex) {}
        }
        if (this.resolution == null) {
            this.duration.setValueInSeconds(secs);
        }
        else {
            final double resSecs = this.resolution.getValueInSeconds();
            final double closest = (double)Math.round(secs / resSecs);
            secs = closest * resSecs;
            this.duration.setValueInSeconds(secs);
        }
        this.hashCode = 0;
    }
    
    public TimeDuration getResolution() {
        return this.resolution;
    }
    
    public void setResolution(final TimeDuration resolution) {
        this.resolution = resolution;
        this.useResolution = true;
    }
    
    public boolean useStart() {
        return this.useStart;
    }
    
    public boolean useEnd() {
        return this.useEnd;
    }
    
    public boolean useDuration() {
        return this.useDuration;
    }
    
    public boolean useResolution() {
        return this.useResolution;
    }
    
    public boolean isPoint() {
        return !this.isEmpty && this.start.equals(this.end);
    }
    
    public boolean isEmpty() {
        return this.isEmpty;
    }
    
    @Override
    public String toString() {
        return "start= " + this.start + " end= " + this.end + " duration= " + this.duration + " resolution= " + this.resolution;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof DateRange)) {
            return false;
        }
        final DateRange oo = (DateRange)o;
        return (!this.useStart || this.start.equals(oo.start)) && (!this.useEnd || this.end.equals(oo.end)) && (!this.useDuration || this.duration.equals(oo.duration)) && (!this.useResolution || this.resolution.equals(oo.resolution));
    }
    
    @Override
    public int hashCode() {
        if (this.hashCode == 0) {
            int result = 17;
            if (this.useStart) {
                result = 37 * result + this.start.hashCode();
            }
            if (this.useEnd) {
                result = 37 * result + this.end.hashCode();
            }
            if (this.useDuration) {
                result = 37 * result + this.duration.hashCode();
            }
            if (this.useResolution) {
                result = 37 * result + this.resolution.hashCode();
            }
            this.hashCode = result;
        }
        return this.hashCode;
    }
}
