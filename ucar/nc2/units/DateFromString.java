// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.units;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.text.ParseException;
import java.util.TimeZone;
import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.Date;

public class DateFromString
{
    public static Date getDateUsingSimpleDateFormat(final String dateString, final String dateFormatString) {
        int smallestIndex = dateString.length();
        if (smallestIndex == 0) {
            return null;
        }
        for (int i = 0; i < 10; ++i) {
            final int curIndex = dateString.indexOf(String.valueOf(i));
            if (curIndex != -1 && smallestIndex > curIndex) {
                smallestIndex = curIndex;
            }
        }
        return getDateUsingCompleteDateFormatWithOffset(dateString, dateFormatString, smallestIndex);
    }
    
    public static Date getDateUsingDemarkatedCount(final String dateString, String dateFormatString, final char demark) {
        final int pos1 = dateFormatString.indexOf(demark);
        dateFormatString = dateFormatString.substring(pos1 + 1);
        return getDateUsingCompleteDateFormatWithOffset(dateString, dateFormatString, pos1);
    }
    
    public static Date getDateUsingDemarkatedMatch(String dateString, String dateFormatString, final char demark) {
        final int pos1 = dateFormatString.indexOf(demark);
        final int pos2 = dateFormatString.indexOf(demark, pos1 + 1);
        if (pos1 < 0 || pos2 < 0) {
            return null;
        }
        final String match = dateFormatString.substring(pos1 + 1, pos2);
        final int pos3 = dateString.indexOf(match);
        if (pos3 < 0) {
            return null;
        }
        if (pos1 > 0) {
            dateFormatString = dateFormatString.substring(0, pos1);
            dateString = dateString.substring(pos3 - dateFormatString.length(), pos3);
        }
        else {
            dateFormatString = dateFormatString.substring(pos2 + 1);
            dateString = dateString.substring(pos3 + match.length());
        }
        return getDateUsingCompleteDateFormatWithOffset(dateString, dateFormatString, 0);
    }
    
    public static Double getHourUsingDemarkatedMatch(String hourString, final String formatString, final char demark) {
        final int pos1 = formatString.indexOf(demark);
        final int pos2 = formatString.indexOf(demark, pos1 + 1);
        if (pos1 < 0 || pos2 < 0) {
            return null;
        }
        final String match = formatString.substring(pos1 + 1, pos2);
        final int pos3 = hourString.indexOf(match);
        if (pos3 < 0) {
            return null;
        }
        if (pos1 > 0) {
            hourString = hourString.substring(pos3 - pos1, pos3);
        }
        else {
            final int len = formatString.length() - pos2 - 1;
            final int start = pos3 + match.length();
            hourString = hourString.substring(start, start + len);
        }
        return Double.valueOf(hourString);
    }
    
    public static Date getDateUsingCompleteDateFormat(final String dateString, final String dateFormatString) {
        return getDateUsingCompleteDateFormatWithOffset(dateString, dateFormatString, 0);
    }
    
    public static Date getDateUsingCompleteDateFormatWithOffset(final String dateString, final String dateFormatString, final int startIndex) {
        try {
            final SimpleDateFormat dateFormat = new SimpleDateFormat(dateFormatString, Locale.US);
            dateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
            final String s = dateString.substring(startIndex, startIndex + dateFormatString.length());
            final Date result = dateFormat.parse(s);
            if (result == null) {
                throw new RuntimeException("SimpleDateFormat bad =" + dateFormatString + " working on =" + s);
            }
            return result;
        }
        catch (ParseException e) {
            throw new RuntimeException("SimpleDateFormat = " + dateFormatString + " fails on " + dateString + " ParseException:" + e.getMessage());
        }
        catch (IllegalArgumentException e2) {
            throw new RuntimeException("SimpleDateFormat = " + dateFormatString + " fails on " + dateString + " IllegalArgumentException:" + e2.getMessage());
        }
    }
    
    public static Date getDateUsingRegExp(final String dateString, final String matchPattern, final String substitutionPattern) {
        final String dateFormatString = "yyyy-MM-dd'T'HH:mm";
        return getDateUsingRegExpAndDateFormat(dateString, matchPattern, substitutionPattern, dateFormatString);
    }
    
    public static Date getDateUsingRegExpAndDateFormat(final String dateString, final String matchPattern, final String substitutionPattern, final String dateFormatString) {
        final Pattern pattern = Pattern.compile(matchPattern);
        final Matcher matcher = pattern.matcher(dateString);
        if (!matcher.matches()) {
            return null;
        }
        final StringBuffer dateStringFormatted = new StringBuffer();
        matcher.appendReplacement(dateStringFormatted, substitutionPattern);
        if (dateStringFormatted.length() == 0) {
            return null;
        }
        return getDateUsingCompleteDateFormat(dateStringFormatted.toString(), dateFormatString);
    }
    
    public static void main(final String[] args) throws ParseException {
        final DateFormatter formatter = new DateFormatter();
        Date result = getDateUsingDemarkatedMatch("/data/anything/2006070611/wrfout_d01_2006-07-06_080000.nc", "#wrfout_d01_#yyyy-MM-dd_HHmm", '#');
        System.out.println(" 2006-07-06_080000 -> " + formatter.toDateTimeStringISO(result));
        result = getDateUsingDemarkatedMatch("C:\\data\\nomads\\gfs-hi\\gfs_3_20061129_0600", "#gfs_3_#yyyyMMdd_HH", '#');
        System.out.println(" 20061129_06 -> " + formatter.toDateTimeStringISO(result));
        System.out.println(new SimpleDateFormat("yyyyMMdd_HH").parse("20061129_06"));
        System.out.println(new SimpleDateFormat("yyyyMMdd_HH").parse("20061129_0600"));
    }
}
