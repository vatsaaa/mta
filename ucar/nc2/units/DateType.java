// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.units;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateType
{
    private DateFormatter formatter;
    private String text;
    private String format;
    private String type;
    private boolean isPresent;
    private boolean isBlank;
    private Date date;
    private Calendar cal;
    
    public static String hiddenProperties() {
        return "text blank present";
    }
    
    public DateType(final boolean isPresent, final Date date) {
        this.formatter = null;
        this.cal = null;
        this.isPresent = isPresent;
        this.date = date;
    }
    
    public DateType() {
        this.formatter = null;
        this.cal = null;
        this.isBlank = true;
    }
    
    public DateType(final DateType src) {
        this.formatter = null;
        this.cal = null;
        this.text = src.getText();
        this.format = src.getFormat();
        this.type = src.getType();
        this.isPresent = src.isPresent();
        this.isBlank = src.isBlank();
        this.date = src.getDate();
    }
    
    public DateType(String text, final String format, final String type) throws ParseException {
        this.formatter = null;
        this.cal = null;
        text = ((text == null) ? "" : text.trim());
        this.text = text;
        this.format = format;
        this.type = type;
        if (text.length() == 0) {
            this.isBlank = true;
            return;
        }
        this.isPresent = text.equalsIgnoreCase("present");
        if (this.isPresent) {
            return;
        }
        if (format != null) {
            final SimpleDateFormat dateFormat = new SimpleDateFormat(format);
            this.date = dateFormat.parse(text);
            return;
        }
        if (text.indexOf("since") > 0) {
            this.date = DateUnit.getStandardDate(text);
            if (this.date == null) {
                throw new ParseException("invalid udunit date unit", 0);
            }
        }
        else {
            if (null == this.formatter) {
                this.formatter = new DateFormatter();
            }
            this.date = this.formatter.getISODate(text);
            if (this.date == null) {
                throw new ParseException("invalid ISO date unit", 0);
            }
        }
    }
    
    public Date getDate() {
        return this.isPresent() ? new Date() : this.date;
    }
    
    public void setDate(final Date date) {
        this.date = date;
        this.text = null;
        this.isPresent = false;
    }
    
    public boolean isPresent() {
        return this.isPresent;
    }
    
    public boolean isBlank() {
        return this.isBlank;
    }
    
    public String getText() {
        if (this.isPresent) {
            this.text = "present";
        }
        if (this.text == null) {
            this.text = this.toDateTimeString();
        }
        return this.text;
    }
    
    public String getFormat() {
        return this.format;
    }
    
    public String getType() {
        return this.type;
    }
    
    public void setType(final String type) {
        this.type = type;
    }
    
    public boolean before(final Date d) {
        return !this.isPresent() && this.date.before(d);
    }
    
    public boolean before(final DateType d) {
        return d.isPresent() || (!this.isPresent() && this.date.before(d.getDate()));
    }
    
    public boolean after(final Date d) {
        return this.isPresent() || this.date.after(d);
    }
    
    public String toDateString() {
        if (null == this.formatter) {
            this.formatter = new DateFormatter();
        }
        return this.formatter.toDateOnlyString(this.getDate());
    }
    
    public String toDateTimeString() {
        if (null == this.formatter) {
            this.formatter = new DateFormatter();
        }
        return this.formatter.toDateTimeString(this.getDate());
    }
    
    public String toDateTimeStringISO() {
        if (null == this.formatter) {
            this.formatter = new DateFormatter();
        }
        return this.formatter.toDateTimeStringISO(this.getDate());
    }
    
    @Override
    public String toString() {
        return this.getText();
    }
    
    @Override
    public int hashCode() {
        if (this.isBlank()) {
            return 0;
        }
        if (this.isPresent()) {
            return 1;
        }
        return this.getDate().hashCode();
    }
    
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof DateType)) {
            return false;
        }
        final DateType oo = (DateType)o;
        return (this.isPresent() && oo.isPresent()) || (this.isBlank() && oo.isBlank()) || oo.getDate().equals(this.getDate());
    }
    
    public DateType add(final TimeDuration d) {
        return this.add(d.getTimeUnit());
    }
    
    public DateType add(final TimeUnit d) {
        if (this.cal == null) {
            this.cal = Calendar.getInstance();
        }
        this.cal.setTime(this.getDate());
        this.cal.add(13, (int)d.getValueInSeconds());
        return new DateType(false, (Date)this.cal.getTime().clone());
    }
    
    public DateType subtract(final TimeDuration d) {
        return this.subtract(d.getTimeUnit());
    }
    
    public DateType subtract(final TimeUnit d) {
        if (this.cal == null) {
            this.cal = Calendar.getInstance();
        }
        this.cal.setTime(this.getDate());
        this.cal.add(13, (int)(-d.getValueInSeconds()));
        return new DateType(false, (Date)this.cal.getTime().clone());
    }
    
    private static void doOne(final String s) {
        try {
            System.out.println("\nString = (" + s + ")");
            final DateType d = new DateType(s, null, null);
            System.out.println("DateType = (" + d.toString() + ")");
            System.out.println("Date = (" + d.getDate() + ")");
        }
        catch (ParseException e) {
            e.printStackTrace();
        }
    }
    
    public static void main(final String[] args) {
        doOne("T00:00:00Z");
    }
}
