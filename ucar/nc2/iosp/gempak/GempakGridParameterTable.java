// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.iosp.gempak;

import java.io.IOException;

public class GempakGridParameterTable
{
    private static GempakParameterTable paramTable;
    
    public static void addParameters(final String tbl) throws IOException {
        GempakGridParameterTable.paramTable.addParameters(tbl);
    }
    
    public static GempakParameter getParameter(final String name) {
        return GempakGridParameterTable.paramTable.getParameter(name);
    }
    
    static {
        GempakGridParameterTable.paramTable = new GempakParameterTable();
    }
}
