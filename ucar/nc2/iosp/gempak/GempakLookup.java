// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.iosp.gempak;

import java.util.Date;
import ucar.grid.GridParameter;
import ucar.grid.GridRecord;
import ucar.grid.GridDefRecord;
import ucar.grid.GridTableLookup;

public final class GempakLookup implements GridTableLookup
{
    private GempakGridRecord sample;
    
    public GempakLookup(final GempakGridRecord sample) {
        this.sample = sample;
    }
    
    public String getShapeName(final GridDefRecord gds) {
        return "Spherical";
    }
    
    public final String getGridName(final GridDefRecord gds) {
        return this.getProjectionName(gds);
    }
    
    public final GridParameter getParameter(final GridRecord gr) {
        final String name = gr.getParameterName();
        final GridParameter gp = GempakGridParameterTable.getParameter(name);
        if (gp != null) {
            return gp;
        }
        return new GridParameter(0, name, name, "");
    }
    
    public final String getDisciplineName(final GridRecord gr) {
        return "Meteorological Products";
    }
    
    public final String getCategoryName(final GridRecord gr) {
        return "Meteorological Parameters";
    }
    
    public final String getLevelName(final GridRecord gr) {
        final String levelName = GempakUtil.LV_CCRD(gr.getLevelType1());
        return levelName;
    }
    
    public final String getLevelDescription(final GridRecord gr) {
        final String levelName = this.getLevelName(gr);
        if (levelName.equals("PRES")) {
            return "pressure";
        }
        if (levelName.equals("NONE")) {
            return "surface";
        }
        if (levelName.equals("HGHT")) {
            return "height_above_ground";
        }
        if (levelName.equals("THTA")) {
            return "isentropic";
        }
        if (levelName.equals("SGMA")) {
            return "sigma";
        }
        if (levelName.equals("DPTH")) {
            return "depth";
        }
        if (levelName.equals("PDLY")) {
            return "layer_between_two_pressure_difference_from_ground";
        }
        if (levelName.equals("FRZL")) {
            return "zeroDegC_isotherm";
        }
        if (levelName.equals("TROP")) {
            return "tropopause";
        }
        if (levelName.equals("CLDL")) {
            return "cloud_base";
        }
        if (levelName.equals("CLDT")) {
            return "cloud_tops";
        }
        if (levelName.equals("MWSL")) {
            return "maximum_wind_level";
        }
        return levelName;
    }
    
    public final String getLevelUnit(final GridRecord gr) {
        final String levelName = this.getLevelName(gr);
        if (levelName.equals("PRES")) {
            return "hPa";
        }
        if (levelName.equals("HGHT")) {
            return "m";
        }
        if (levelName.equals("THTA")) {
            return "K";
        }
        if (levelName.equals("SGMA")) {
            return "";
        }
        if (levelName.equals("DPTH")) {
            return "m";
        }
        if (levelName.equals("PDLY")) {
            return "hPa";
        }
        return "";
    }
    
    public final String getTimeRangeUnitName(final int tunit) {
        return "minute";
    }
    
    public final Date getFirstBaseTime() {
        return this.sample.getReferenceTime();
    }
    
    public final boolean isLatLon(final GridDefRecord gds) {
        return this.getProjectionName(gds).equals("CED");
    }
    
    public final int getProjectionType(final GridDefRecord gds) {
        final String name = this.getProjectionName(gds).trim();
        if (name.equals("CED")) {
            return -1;
        }
        if (name.equals("MER")) {
            return 3;
        }
        if (name.equals("MCD")) {
            return 3;
        }
        if (name.equals("LCC")) {
            return 2;
        }
        if (name.equals("SCC")) {
            return 2;
        }
        if (name.equals("PS")) {
            return 1;
        }
        if (name.equals("STR")) {
            return 1;
        }
        return -1;
    }
    
    public final boolean isVerticalCoordinate(final GridRecord gr) {
        final int type = gr.getLevelType1();
        return type > GempakUtil.vertCoords.length || !GempakUtil.vertCoords[type].equals("NONE");
    }
    
    public final boolean isPositiveUp(final GridRecord gr) {
        final int type = gr.getLevelType1();
        return type != 1 && type != 5;
    }
    
    public final float getFirstMissingValue() {
        return -9999.0f;
    }
    
    public boolean isLayer(final GridRecord gr) {
        return gr.getLevel2() != -1.0;
    }
    
    private String getProjectionName(final GridDefRecord gds) {
        return gds.getParam("ProjFlag");
    }
    
    public final String getTitle() {
        return "GRID data";
    }
    
    public String getInstitution() {
        return null;
    }
    
    public final String getSource() {
        return null;
    }
    
    public final String getComment() {
        return null;
    }
    
    public String getGridType() {
        return "GEMPAK";
    }
}
