// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.iosp.gempak;

import ucar.ma2.StructureData;
import ucar.nc2.constants.AxisType;
import java.util.ArrayList;
import java.util.Date;
import ucar.ma2.ArrayDouble;
import ucar.nc2.Attribute;
import ucar.ma2.DataType;
import ucar.nc2.Group;
import ucar.nc2.NetcdfFile;
import ucar.nc2.iosp.IOServiceProvider;
import ucar.nc2.FileWriter;
import ucar.nc2.util.CancelTask;
import ucar.ma2.ArrayStructure;
import ucar.ma2.StructureDataIterator;
import ucar.ma2.ArraySequence;
import java.util.Iterator;
import java.util.List;
import java.nio.ByteBuffer;
import ucar.ma2.StructureMembers;
import ucar.ma2.Range;
import visad.util.Trace;
import ucar.nc2.Sequence;
import ucar.ma2.ArrayStructureBB;
import ucar.nc2.Structure;
import ucar.ma2.InvalidRangeException;
import ucar.ma2.Array;
import ucar.ma2.Section;
import ucar.nc2.Variable;
import ucar.nc2.constants.CF;
import java.io.IOException;
import ucar.unidata.io.RandomAccessFile;
import ucar.nc2.Dimension;

public class GempakSoundingIOSP extends GempakStationFileIOSP
{
    protected static final Dimension DIM_MAXMERGELEVELS;
    
    @Override
    protected AbstractGempakStationFileReader makeStationReader() {
        return new GempakSoundingFileReader();
    }
    
    @Override
    public boolean isValidFile(final RandomAccessFile raf) throws IOException {
        return super.isValidFile(raf) && (this.gemreader.getFileSubType().equals("merged") || this.gemreader.getFileSubType().equals("unmerged"));
    }
    
    public String getFileTypeId() {
        return "GempakSounding";
    }
    
    public String getFileTypeDescription() {
        return "GEMPAK Sounding Obs Data";
    }
    
    @Override
    public String getCFFeatureType() {
        return CF.FeatureType.timeSeriesProfile.toString();
    }
    
    public Array readData(final Variable v2, final Section section) throws IOException, InvalidRangeException {
        if (this.gemreader == null) {
            return null;
        }
        final Array array = this.readSoundingData(v2, section, this.gemreader.getFileSubType().equals("merged"));
        return array;
    }
    
    private Array readSoundingData(final Variable v2, final Section section, final boolean isMerged) throws IOException {
        Array array = null;
        if (v2 instanceof Structure) {
            final Range stationRange = section.getRange(0);
            final Range timeRange = section.getRange(1);
            final int size = stationRange.length() * timeRange.length();
            final Structure pdata = (Structure)v2;
            final StructureMembers members = pdata.makeStructureMembers();
            ArrayStructureBB.setOffsets(members);
            final ArrayStructureBB abb = new ArrayStructureBB(members, new int[] { size });
            final ByteBuffer buf = abb.getByteBuffer();
            for (int y = stationRange.first(); y <= stationRange.last(); y += stationRange.stride()) {
                for (int x = timeRange.first(); x <= timeRange.last(); x += timeRange.stride()) {
                    final List<String> parts = isMerged ? ((GempakSoundingFileReader)this.gemreader).getMergedParts() : ((GempakSoundingFileReader)this.gemreader).getUnmergedParts();
                    boolean allMissing = true;
                    for (final String part : parts) {
                        final List<GempakParameter> params = this.gemreader.getParameters(part);
                        final GempakFileReader.RData vals = this.gemreader.DM_RDTR(x + 1, y + 1, part);
                        ArraySequence aseq = null;
                        final Sequence seq = (Sequence)pdata.findVariable(part);
                        if (vals == null) {
                            aseq = this.makeEmptySequence(seq);
                        }
                        else {
                            allMissing = false;
                            aseq = this.makeArraySequence(seq, params, vals.data);
                        }
                        final int index = abb.addObjectToHeap(aseq);
                        buf.putInt(index);
                    }
                    buf.put((byte)(allMissing ? 1 : 0));
                }
            }
            array = abb;
            Trace.call2("GEMPAKSIOSP: readMergedData");
        }
        return array;
    }
    
    private ArraySequence makeEmptySequence(final Sequence seq) {
        final StructureMembers members = seq.makeStructureMembers();
        return new ArraySequence(members, new EmptyStructureDataIterator(), -1);
    }
    
    private ArraySequence makeArraySequence(final Sequence seq, final List<GempakParameter> params, final float[] values) {
        if (values == null) {
            return this.makeEmptySequence(seq);
        }
        final int numLevels = values.length / params.size();
        final StructureMembers members = seq.makeStructureMembers();
        final int offset = ArrayStructureBB.setOffsets(members);
        final int size = offset * numLevels;
        final byte[] bytes = new byte[size];
        final ByteBuffer buf = ByteBuffer.wrap(bytes);
        final ArrayStructureBB abb = new ArrayStructureBB(members, new int[] { numLevels }, buf, 0);
        int var = 0;
        for (int i = 0; i < numLevels; ++i) {
            for (final GempakParameter param : params) {
                if (members.findMember(param.getName()) != null) {
                    buf.putFloat(values[var]);
                }
                ++var;
            }
        }
        return new ArraySequence(members, new SequenceIterator(numLevels, abb), numLevels);
    }
    
    public static void main(final String[] args) throws IOException {
        final IOServiceProvider mciosp = new GempakSoundingIOSP();
        final RandomAccessFile rf = new RandomAccessFile(args[0], "r", 2048);
        final NetcdfFile ncfile = new MakeNetcdfFile(mciosp, rf, args[0], null);
        if (args.length > 1) {
            FileWriter.writeToFile(ncfile, args[1]);
        }
        else {
            System.out.println(ncfile);
        }
    }
    
    @Override
    protected void fillNCFile() throws IOException {
        final String fileType = this.gemreader.getFileSubType();
        this.buildFile(fileType.equals("merged"));
    }
    
    private void buildFile(final boolean isMerged) {
        final List<GempakStation> stations = this.gemreader.getStations();
        final Dimension station = new Dimension("station", stations.size(), true);
        this.ncfile.addDimension(null, station);
        this.ncfile.addDimension(null, GempakSoundingIOSP.DIM_LEN8);
        this.ncfile.addDimension(null, GempakSoundingIOSP.DIM_LEN4);
        this.ncfile.addDimension(null, GempakSoundingIOSP.DIM_LEN2);
        final List<Variable> stationVars = this.makeStationVars(stations, station);
        for (final Variable stnVar : stationVars) {
            this.ncfile.addVariable(null, stnVar);
        }
        final List<Date> timeList = this.gemreader.getDates();
        final int numTimes = timeList.size();
        final Dimension times = new Dimension("time", numTimes, true);
        this.ncfile.addDimension(null, times);
        Array varArray = null;
        final Variable timeVar = new Variable(this.ncfile, null, null, "time", DataType.DOUBLE, "time");
        timeVar.addAttribute(new Attribute("units", "seconds since 1970-01-01 00:00:00"));
        timeVar.addAttribute(new Attribute("long_name", "time"));
        varArray = new ArrayDouble.D1(numTimes);
        int i = 0;
        for (final Date date : timeList) {
            ((ArrayDouble.D1)varArray).set(i, date.getTime() / 1000.0);
            ++i;
        }
        timeVar.setCachedData(varArray, false);
        this.ncfile.addVariable(null, timeVar);
        final List<Dimension> stationTime = new ArrayList<Dimension>();
        stationTime.add(station);
        stationTime.add(times);
        String structName = isMerged ? "merged" : "unmerged";
        structName += "Sounding";
        final Structure sVar = new Structure(this.ncfile, null, null, structName);
        sVar.setDimensions(stationTime);
        sVar.addAttribute(new Attribute("coordinates", "time SLAT SLON SELV"));
        List<String> sequenceNames;
        if (isMerged) {
            sequenceNames = new ArrayList<String>();
            sequenceNames.add("SNDT");
        }
        else {
            sequenceNames = ((GempakSoundingFileReader)this.gemreader).getUnmergedParts();
        }
        for (final String seqName : sequenceNames) {
            final Sequence paramData = this.makeSequence(sVar, seqName, false);
            if (paramData == null) {
                continue;
            }
            sVar.addMemberVariable(paramData);
        }
        sVar.addMemberVariable(this.makeMissingVariable());
        this.ncfile.addAttribute(null, new Attribute("CF:featureType", CF.FeatureType.timeSeriesProfile.toString()));
        this.ncfile.addVariable(null, sVar);
    }
    
    protected Sequence makeSequence(final Structure parent, final String partName, final boolean includeMissing) {
        final List<GempakParameter> params = this.gemreader.getParameters(partName);
        if (params == null) {
            return null;
        }
        final Sequence sVar = new Sequence(this.ncfile, null, parent, partName);
        sVar.setDimensions("");
        for (final GempakParameter param : params) {
            final Variable v = this.makeParamVariable(param, null);
            this.addVerticalCoordAttribute(v);
            sVar.addMemberVariable(v);
        }
        if (includeMissing) {
            sVar.addMemberVariable(this.makeMissingVariable());
        }
        return sVar;
    }
    
    private void addVerticalCoordAttribute(final Variable v) {
        final GempakSoundingFileReader gsfr = (GempakSoundingFileReader)this.gemreader;
        final int vertType = gsfr.getVerticalCoordinate();
        final String pName = v.getName();
        if (this.gemreader.getFileSubType().equals("merged")) {
            if (vertType == 1 && pName.equals("PRES")) {
                v.addAttribute(new Attribute("_CoordinateAxisType", AxisType.Pressure.name()));
            }
            else if (vertType == 3 && (pName.equals("HGHT") || pName.equals("MHGT") || pName.equals("DHGT"))) {
                v.addAttribute(new Attribute("_CoordinateAxisType", AxisType.Height.name()));
            }
        }
        else if (pName.equals("PRES")) {
            v.addAttribute(new Attribute("_CoordinateAxisType", AxisType.Pressure.name()));
        }
    }
    
    static {
        DIM_MAXMERGELEVELS = new Dimension("maxMergeLevels", 50, true);
    }
    
    protected static class MakeNetcdfFile extends NetcdfFile
    {
        MakeNetcdfFile(final IOServiceProvider spi, final RandomAccessFile raf, final String location, final CancelTask cancelTask) throws IOException {
            super(spi, raf, location, cancelTask);
        }
    }
    
    class EmptyStructureDataIterator implements StructureDataIterator
    {
        public boolean hasNext() throws IOException {
            return false;
        }
        
        public StructureData next() throws IOException {
            return null;
        }
        
        public void setBufferSize(final int bytes) {
        }
        
        public StructureDataIterator reset() {
            return this;
        }
        
        public int getCurrentRecno() {
            return -1;
        }
    }
    
    private class SequenceIterator implements StructureDataIterator
    {
        private int count;
        private ArrayStructure abb;
        private StructureDataIterator siter;
        
        SequenceIterator(final int count, final ArrayStructure abb) {
            this.count = count;
            this.abb = abb;
        }
        
        public boolean hasNext() throws IOException {
            if (this.siter == null) {
                this.siter = this.abb.getStructureDataIterator();
            }
            return this.siter.hasNext();
        }
        
        public StructureData next() throws IOException {
            return this.siter.next();
        }
        
        public void setBufferSize(final int bytes) {
            this.siter.setBufferSize(bytes);
        }
        
        public StructureDataIterator reset() {
            this.siter = null;
            return this;
        }
        
        public int getCurrentRecno() {
            return this.siter.getCurrentRecno();
        }
    }
}
