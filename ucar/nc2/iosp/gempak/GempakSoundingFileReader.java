// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.iosp.gempak;

import java.util.Iterator;
import ucar.unidata.util.Format;
import ucar.unidata.util.StringUtil;
import java.util.Collection;
import java.util.ArrayList;
import java.io.IOException;
import ucar.unidata.io.RandomAccessFile;
import java.util.List;

public class GempakSoundingFileReader extends AbstractGempakStationFileReader
{
    public static final String SNDT = "SNDT";
    public static final String DATE = "DATE";
    public static final String TIME = "TIME";
    public static final int PRES_COORD = 1;
    public static final int THTA_COORD = 2;
    public static final int HGHT_COORD = 3;
    public static final String MERGED = "merged";
    public static final String UNMERGED = "unmerged";
    private int ivert;
    private List<String> unmergedParts;
    private final String[] mandpp;
    private final String[] sigtpp;
    private final String[] sigwpp;
    private final String[] troppp;
    private final String[] maxwpp;
    private final String[] belowGroups;
    private final String[] aboveGroups;
    private final String[][] parmLists;
    
    GempakSoundingFileReader() {
        this.ivert = -1;
        this.mandpp = new String[] { "PRES", "TEMP", "DWPT", "DRCT", "SPED", "HGHT" };
        this.sigtpp = new String[] { "PRES", "TEMP", "DWPT" };
        this.sigwpp = new String[] { "HGHT", "DRCT", "SPED" };
        this.troppp = new String[] { "PRES", "TEMP", "DWPT", "DRCT", "SPED" };
        this.maxwpp = new String[] { "PRES", "DRCT", "SPED" };
        this.belowGroups = new String[] { "TTAA", "TRPA", "MXWA", "PPAA", "TTBB", "PPBB" };
        this.aboveGroups = new String[] { "TTCC", "TRPC", "MXWC", "PPCC", "TTDD", "PPDD" };
        this.parmLists = new String[][] { this.mandpp, this.troppp, this.maxwpp, this.maxwpp, this.sigtpp, this.sigwpp };
    }
    
    public static GempakSoundingFileReader getInstance(final RandomAccessFile raf, final boolean fullCheck) throws IOException {
        final GempakSoundingFileReader gsfr = new GempakSoundingFileReader();
        gsfr.init(raf, fullCheck);
        return gsfr;
    }
    
    @Override
    protected boolean init() throws IOException {
        return this.init(true);
    }
    
    @Override
    protected boolean init(final boolean fullCheck) throws IOException {
        if (!super.init(fullCheck)) {
            return false;
        }
        if (this.dmLabel.kftype != 2) {
            this.logError("not a sounding data file ");
            return false;
        }
        final DMPart part = this.getPart("SNDT");
        if (part != null) {
            this.subType = "merged";
            final String vertName = part.params.get(0).kprmnm;
            if (vertName.equals("PRES")) {
                this.ivert = 1;
            }
            else if (vertName.equals("THTA")) {
                this.ivert = 2;
            }
            else {
                if (!vertName.equals("HGHT") && !vertName.equals("MHGT") && !vertName.equals("DHGT")) {
                    this.logError("unknown vertical coordinate in merged file");
                    return false;
                }
                this.ivert = 3;
            }
        }
        else {
            this.unmergedParts = this.SN_CKUA();
            final boolean haveUnMerged = !this.unmergedParts.isEmpty();
            if (!haveUnMerged) {
                this.logError("unknown sounding file type - not merged/unmerged");
                return false;
            }
            this.ivert = 1;
            this.subType = "unmerged";
        }
        if (!this.readStationsAndTimes(true)) {
            this.logError("Unable to read stations and times");
            return false;
        }
        return true;
    }
    
    public int getVerticalCoordinate() {
        return this.ivert;
    }
    
    public List<String> getMergedParts() {
        final List<String> list = new ArrayList<String>(1);
        list.add("SNDT");
        return list;
    }
    
    public List<String> getUnmergedParts() {
        return new ArrayList<String>(this.unmergedParts);
    }
    
    @Override
    protected void makeFileSubType() {
    }
    
    public void printOb(final int row, final int col) {
        final GempakStation station = this.getStations().get(col - 1);
        final String time = this.getDateString(row - 1);
        final StringBuilder builder = new StringBuilder("\n");
        builder.append(this.makeHeader(station, time));
        builder.append("\n");
        final boolean merge = this.getFileSubType().equals("merged");
        List<String> parts;
        if (merge) {
            parts = new ArrayList<String>();
            parts.add("SNDT");
        }
        else {
            parts = this.unmergedParts;
        }
        for (final String part : parts) {
            RData rd = null;
            try {
                rd = this.DM_RDTR(row, col, part);
            }
            catch (IOException ioe) {
                ioe.printStackTrace();
                rd = null;
            }
            if (rd == null) {
                continue;
            }
            if (!merge) {
                builder.append("    ");
                builder.append(part);
                builder.append("    ");
                builder.append(time.substring(time.indexOf("/") + 1));
            }
            builder.append("\n");
            if (!merge) {
                builder.append("\t");
            }
            final List<GempakParameter> params = this.getParameters(part);
            for (final GempakParameter parm : params) {
                builder.append(StringUtil.padLeft(parm.getName(), 7));
                builder.append("\t");
            }
            builder.append("\n");
            if (!merge) {
                builder.append("\t");
            }
            final float[] data = rd.data;
            final int numParams = params.size();
            for (int numLevels = data.length / numParams, j = 0; j < numLevels; ++j) {
                for (int i = 0; i < numParams; ++i) {
                    builder.append(StringUtil.padLeft(Format.formatDouble(data[j * numParams + i], 7, 1), 7));
                    builder.append("\t");
                }
                builder.append("\n");
                if (!merge) {
                    builder.append("\t");
                }
            }
            builder.append("\n");
        }
        builder.append("\n");
        System.out.println(builder.toString());
    }
    
    private String makeHeader(final GempakStation stn, final String date) {
        final StringBuilder builder = new StringBuilder();
        builder.append("STID = ");
        builder.append(StringUtil.padRight(stn.getSTID().trim() + stn.getSTD2().trim(), 8));
        builder.append("\t");
        builder.append("STNM = ");
        builder.append(Format.i(stn.getSTNM(), 6));
        builder.append("\t");
        builder.append("TIME = ");
        builder.append(date);
        builder.append("\n");
        builder.append("SLAT = ");
        builder.append(Format.d(stn.getLatitude(), 5));
        builder.append("\t");
        builder.append("SLON = ");
        builder.append(Format.d(stn.getLongitude(), 5));
        builder.append("\t");
        builder.append("SELV = ");
        builder.append(Format.d(stn.getAltitude(), 5));
        builder.append("\n");
        return builder.toString();
    }
    
    public static void main(final String[] args) throws IOException {
        if (args.length == 0) {
            System.out.println("need to supply a GEMPAK sounding file name");
            System.exit(1);
        }
        try {
            GempakParameters.addParameters("resources/nj22/tables/gempak/params.tbl");
        }
        catch (Exception e) {
            System.out.println("unable to init param tables");
        }
        final GempakSoundingFileReader gsfr = getInstance(GempakFileReader.getFile(args[0]), true);
        System.out.println("Type = " + gsfr.getFileType());
        gsfr.printFileLabel();
        gsfr.printKeys();
        gsfr.printHeaders();
        gsfr.printParts();
        gsfr.printDates();
        gsfr.printStations(false);
        int row = 1;
        int col = 1;
        if (args.length > 1) {
            row = Integer.parseInt(args[1]);
        }
        if (args.length > 2) {
            try {
                col = Integer.parseInt(args[2]);
            }
            catch (Exception npe) {
                col = gsfr.findStationIndex(args[2]);
                if (col == -1) {
                    System.out.println("couldn't find station " + args[2]);
                    System.exit(1);
                }
                System.out.println("found station at column " + col);
            }
        }
        gsfr.printOb(row, col);
    }
    
    private List<String> SN_CKUA() {
        final List<String> types = new ArrayList<String>();
        boolean above = false;
        boolean done = false;
        String partToCheck = "";
        while (!done) {
            for (int group = 0; group < this.belowGroups.length; ++group) {
                if (above) {
                    partToCheck = this.aboveGroups[group];
                }
                else {
                    partToCheck = this.belowGroups[group];
                }
                if (this.checkForValidGroup(partToCheck, this.parmLists[group])) {
                    types.add(partToCheck);
                }
            }
            if (!above) {
                above = true;
            }
            else {
                done = true;
            }
        }
        return types;
    }
    
    private boolean checkForValidGroup(final String partToCheck, final String[] params) {
        final DMPart part = this.getPart(partToCheck);
        if (part == null) {
            return false;
        }
        int i = 0;
        for (final DMParam parm : part.params) {
            if (!parm.kprmnm.equals(params[i++])) {
                return false;
            }
        }
        return true;
    }
}
