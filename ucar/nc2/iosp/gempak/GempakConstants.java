// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.iosp.gempak;

public interface GempakConstants
{
    public static final int MTVAX = 2;
    public static final int MTSUN = 3;
    public static final int MTIRIS = 4;
    public static final int MTAPOL = 5;
    public static final int MTIBM = 6;
    public static final int MTIGPH = 7;
    public static final int MTULTX = 8;
    public static final int MTHP = 9;
    public static final int MTALPH = 10;
    public static final int MTLNUX = 11;
    public static final int IMISSD = -9999;
    public static final float RMISSD = -9999.0f;
    public static final float RDIFFD = 0.1f;
    public static final int MFSF = 1;
    public static final int MFSN = 2;
    public static final int MFGD = 3;
    public static final int MDREAL = 1;
    public static final int MDINTG = 2;
    public static final int MDCHAR = 3;
    public static final int MDRPCK = 4;
    public static final int MDGRID = 5;
    public static final int LLNNAV = 256;
    public static final int LLNANL = 128;
    public static final int LLSTHL = 20;
    public static final int LLGDHD = 128;
    public static final int MDGNON = 0;
    public static final int MDGGRB = 1;
    public static final int MDGNMC = 2;
    public static final int MDGDIF = 3;
    public static final int MDGDEC = 4;
    public static final int MDGRB2 = 5;
    public static final String ROW = "ROW";
    public static final String COL = "COL";
    public static final int MBLKSZ = 128;
}
