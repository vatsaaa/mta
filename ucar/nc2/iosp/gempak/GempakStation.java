// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.iosp.gempak;

import ucar.unidata.geoloc.LatLonPointImpl;
import ucar.unidata.geoloc.LatLonPoint;
import ucar.unidata.util.Format;
import ucar.unidata.util.StringUtil;
import ucar.unidata.geoloc.Station;

public class GempakStation implements Station
{
    public static final String STID = "STID";
    public static final String STNM = "STNM";
    public static final String SLAT = "SLAT";
    public static final String SLON = "SLON";
    public static final String SELV = "SELV";
    public static final String STAT = "STAT";
    public static final String COUN = "COUN";
    public static final String STD2 = "STD2";
    public static final String SPRI = "SPRI";
    public static final String SWFO = "SWFO";
    public static final String WFO2 = "WFO2";
    private String stid;
    private String std2;
    private int stnm;
    private String stat;
    private String coun;
    private String swfo;
    private String wfo2;
    private int slat;
    private int slon;
    private int selv;
    private int spri;
    private int index;
    private String sdesc;
    
    public GempakStation() {
        this.stid = "";
        this.std2 = "";
        this.stnm = -9999;
        this.stat = "";
        this.coun = "";
        this.swfo = "";
        this.wfo2 = "";
        this.slat = -9999;
        this.slon = -9999;
        this.selv = -9999;
        this.sdesc = "";
    }
    
    @Override
    public String toString() {
        final StringBuilder builder = new StringBuilder();
        builder.append(StringUtil.padRight(this.stid.trim() + this.std2.trim(), 8));
        builder.append(" ");
        builder.append(Format.i(this.stnm, 6));
        builder.append(" ");
        builder.append(StringUtil.padRight(this.sdesc, 32));
        builder.append(" ");
        builder.append(StringUtil.padLeft(this.stat.trim(), 2));
        builder.append(" ");
        builder.append(StringUtil.padLeft(this.coun.trim(), 2));
        builder.append(" ");
        builder.append(Format.i(this.slat, 5));
        builder.append(" ");
        builder.append(Format.i(this.slon, 6));
        builder.append(" ");
        builder.append(Format.i(this.selv, 5));
        builder.append(" ");
        builder.append(Format.i(this.spri, 2));
        builder.append(" ");
        builder.append(StringUtil.padLeft(this.swfo.trim(), 3));
        return builder.toString();
    }
    
    public void setSTID(final String value) {
        this.stid = value;
    }
    
    public String getSTID() {
        return this.stid;
    }
    
    public void setSTNM(final int value) {
        this.stnm = value;
    }
    
    public int getSTNM() {
        return this.stnm;
    }
    
    public void setSTAT(final String value) {
        this.stat = value;
    }
    
    public String getSTAT() {
        return this.stat;
    }
    
    public void setCOUN(final String value) {
        this.coun = value;
    }
    
    public String getCOUN() {
        return this.coun;
    }
    
    public void setSTD2(final String value) {
        this.std2 = value;
    }
    
    public String getSTD2() {
        return this.std2;
    }
    
    public void setSWFO(final String value) {
        this.swfo = value;
    }
    
    public String getSWFO() {
        return this.swfo;
    }
    
    public void setWFO2(final String value) {
        this.wfo2 = value;
    }
    
    public String getWFO2() {
        return this.wfo2;
    }
    
    public void setSLAT(final int value) {
        this.slat = value;
    }
    
    public int getSLAT() {
        return this.slat;
    }
    
    public void setSLON(final int value) {
        this.slon = value;
    }
    
    public int getSLON() {
        return this.slon;
    }
    
    public void setSELV(final int value) {
        this.selv = value;
    }
    
    public int getSELV() {
        return this.selv;
    }
    
    public void setSPRI(final int value) {
        this.spri = value;
    }
    
    public int getSPRI() {
        return this.spri;
    }
    
    public double getLatitude() {
        if (this.slat == -9999) {
            return this.slat;
        }
        return this.slat / 100.0;
    }
    
    public double getLongitude() {
        if (this.slon == -9999) {
            return this.slon;
        }
        return this.slon / 100.0;
    }
    
    public double getAltitude() {
        return this.selv;
    }
    
    public String getName() {
        return this.stid.trim() + this.std2.trim();
    }
    
    public String getDescription() {
        return this.sdesc;
    }
    
    public void setDescription(final String desc) {
        this.sdesc = desc;
    }
    
    public String getWmoId() {
        String wmoID = "";
        if (this.stnm != -9999) {
            wmoID = String.valueOf(this.stnm / 10);
        }
        return wmoID;
    }
    
    public int compareTo(final Station o) {
        return this.getName().compareTo(o.getName());
    }
    
    public boolean isMissing() {
        return this.slat == -9999 || this.slon == -9999;
    }
    
    public LatLonPoint getLatLon() {
        return new LatLonPointImpl(this.getLatitude(), this.getLongitude());
    }
    
    public void setIndex(final int rowOrCol) {
        this.index = rowOrCol;
    }
    
    public int getIndex() {
        return this.index;
    }
}
