// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.iosp.gempak;

import java.net.URLConnection;
import java.net.URL;
import java.io.FileInputStream;
import java.io.File;
import java.util.regex.Matcher;
import java.util.Iterator;
import java.util.Set;
import java.util.regex.Pattern;
import java.util.List;
import java.io.InputStream;
import java.util.ArrayList;
import ucar.unidata.util.StringUtil;
import java.io.IOException;
import java.util.HashMap;

public class GempakParameterTable
{
    private HashMap<String, GempakParameter> paramMap;
    private HashMap<String, GempakParameter> templateParamMap;
    private static int[] indices;
    private static int[] lengths;
    
    public GempakParameterTable() {
        this.paramMap = new HashMap<String, GempakParameter>(256);
        this.templateParamMap = new HashMap<String, GempakParameter>(20);
    }
    
    public void addParameters(final String tbl) throws IOException {
        final InputStream is = this.getInputStream(tbl);
        if (is == null) {
            throw new IOException("Unable to open " + tbl);
        }
        final String content = this.readContents(is);
        final List lines = StringUtil.split(content, "\n", false);
        final List<String[]> result = new ArrayList<String[]>();
        for (int i = 0; i < lines.size(); ++i) {
            final String line = lines.get(i);
            final String tline = line.trim();
            if (tline.length() != 0) {
                if (!tline.startsWith("!")) {
                    final String[] words = new String[GempakParameterTable.indices.length];
                    for (int idx = 0; idx < GempakParameterTable.indices.length; ++idx) {
                        if (GempakParameterTable.indices[idx] < tline.length()) {
                            if (GempakParameterTable.indices[idx] + GempakParameterTable.lengths[idx] > tline.length()) {
                                words[idx] = line.substring(GempakParameterTable.indices[idx]);
                            }
                            else {
                                words[idx] = line.substring(GempakParameterTable.indices[idx], GempakParameterTable.indices[idx] + GempakParameterTable.lengths[idx]);
                            }
                            words[idx] = words[idx].trim();
                        }
                    }
                    result.add(words);
                }
            }
        }
        for (int i = 0; i < result.size(); ++i) {
            final GempakParameter p = this.makeParameter(result.get(i));
            if (p != null) {
                if (p.getName().indexOf("(") >= 0) {
                    this.templateParamMap.put(p.getName(), p);
                }
                else {
                    this.paramMap.put(p.getName(), p);
                }
            }
        }
    }
    
    private GempakParameter makeParameter(final String[] words) {
        int num = 0;
        if (words[0] != null) {
            num = (int)Double.parseDouble(words[0]);
        }
        if (words[3] == null || words[3].equals("")) {
            return null;
        }
        String name = words[3];
        if (name.indexOf("-") >= 0) {
            final int first = name.indexOf("-");
            final int last = name.lastIndexOf("-");
            final StringBuffer buf = new StringBuffer(name.substring(0, first));
            buf.append("(");
            for (int i = first; i <= last; ++i) {
                buf.append("\\d");
            }
            buf.append(")");
            buf.append(name.substring(last + 1));
            name = buf.toString();
        }
        String description;
        if (words[1] == null || words[1].equals("")) {
            description = words[3];
        }
        else {
            description = words[1];
        }
        String unit = words[2];
        if (unit != null) {
            unit = unit.replaceAll("\\*\\*", "");
            if (unit.equals("-")) {
                unit = "";
            }
        }
        int decimalScale = 0;
        try {
            decimalScale = Integer.parseInt(words[4].trim());
        }
        catch (NumberFormatException ne) {
            decimalScale = 0;
        }
        return new GempakParameter(num, name, description, unit, decimalScale);
    }
    
    public GempakParameter getParameter(final String name) {
        GempakParameter param = this.paramMap.get(name);
        if (param == null) {
            final Set<String> keys = this.templateParamMap.keySet();
            if (!keys.isEmpty()) {
                for (final String key : keys) {
                    final Pattern p = Pattern.compile(key);
                    final Matcher m = p.matcher(name);
                    if (m.matches()) {
                        final String value = m.group(1);
                        final GempakParameter match = this.templateParamMap.get(key);
                        param = new GempakParameter(match.getNumber(), name, match.getDescription() + " (" + value + " hour)", match.getUnit(), match.getDecimalScale());
                        this.paramMap.put(name, param);
                        break;
                    }
                }
            }
        }
        return param;
    }
    
    public static void main(final String[] args) throws IOException {
        final GempakParameterTable pt = new GempakParameterTable();
        pt.addParameters("resources/nj22/tables/gempak/params.tbl");
        if (args.length > 0) {
            final String param = args[0];
            final GempakParameter parm = pt.getParameter(param);
            if (parm != null) {
                System.out.println("Found " + param + ": " + parm);
            }
        }
    }
    
    private String readContents(final InputStream is) throws IOException {
        return new String(this.readBytes(is));
    }
    
    private byte[] readBytes(final InputStream is) throws IOException {
        int totalRead = 0;
        byte[] content = new byte[1000000];
        while (true) {
            final int howMany = is.read(content, totalRead, content.length - totalRead);
            if (howMany < 0) {
                break;
            }
            if (howMany == 0) {
                continue;
            }
            totalRead += howMany;
            if (totalRead < content.length) {
                continue;
            }
            final byte[] tmp = content;
            final int newLength = (content.length < 25000000) ? (content.length * 2) : (content.length + 5000000);
            content = new byte[newLength];
            System.arraycopy(tmp, 0, content, 0, totalRead);
        }
        is.close();
        final byte[] results = new byte[totalRead];
        System.arraycopy(content, 0, results, 0, totalRead);
        return results;
    }
    
    private InputStream getInputStream(final String resourceName) {
        InputStream s = null;
        final ClassLoader cl = GempakParameterTable.class.getClassLoader();
        s = cl.getResourceAsStream(resourceName);
        if (s != null) {
            return s;
        }
        final File f = new File(resourceName);
        if (f.exists()) {
            try {
                s = new FileInputStream(f);
            }
            catch (Exception ex) {}
        }
        if (s != null) {
            return s;
        }
        try {
            final Matcher m = Pattern.compile(" ").matcher(resourceName);
            final String encodedUrl = m.replaceAll("%20");
            final URL dataUrl = new URL(encodedUrl);
            final URLConnection connection = dataUrl.openConnection();
            s = connection.getInputStream();
        }
        catch (Exception ex2) {}
        return s;
    }
    
    static {
        GempakParameterTable.indices = new int[] { 0, 4, 38, 59, 72, 79, 90, 97 };
        GempakParameterTable.lengths = new int[] { 4, 33, 21, 13, 7, 11, 6, 6 };
    }
}
