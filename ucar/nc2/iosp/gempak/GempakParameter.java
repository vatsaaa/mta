// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.iosp.gempak;

import ucar.unidata.util.StringUtil;
import ucar.grid.GridParameter;

public class GempakParameter extends GridParameter
{
    private int decimalScale;
    private boolean isNumeric;
    
    public GempakParameter(final int number, final String name, final String description, final String unit, final int scale) {
        this(number, name, description, unit, scale, true);
    }
    
    public GempakParameter(final int number, final String name, final String description, final String unit, final int scale, final boolean isNumeric) {
        super(number, name, description, unit);
        this.decimalScale = 0;
        this.isNumeric = true;
        this.decimalScale = scale;
        this.isNumeric = isNumeric;
    }
    
    public int getDecimalScale() {
        return this.decimalScale;
    }
    
    public boolean getIsNumeric() {
        return this.isNumeric;
    }
    
    public void setIsNumeric(final boolean yesorno) {
        this.isNumeric = yesorno;
    }
    
    public String toString() {
        final StringBuilder buf = new StringBuilder("GridParameter: ");
        buf.append(StringUtil.padLeft(String.valueOf(this.getNumber()), 4));
        buf.append(" ");
        final String param = this.getName() + " (" + this.getDescription() + ")";
        buf.append(StringUtil.padRight(param, 40));
        buf.append(" [");
        buf.append(this.getUnit());
        buf.append("]");
        buf.append(" scale: ");
        buf.append(this.getDecimalScale());
        return buf.toString();
    }
    
    public boolean equals(final Object o) {
        if (o == null || !(o instanceof GempakParameter)) {
            return false;
        }
        final GempakParameter that = (GempakParameter)o;
        return super.equals((Object)that) && this.decimalScale == that.decimalScale;
    }
    
    public int hashCode() {
        return super.hashCode() + 17 * this.decimalScale;
    }
}
