// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.iosp.gempak;

import java.io.EOFException;
import java.util.Iterator;
import java.util.ArrayList;
import java.io.IOException;
import java.util.List;
import ucar.unidata.io.RandomAccessFile;

public class GempakFileReader implements GempakConstants
{
    protected RandomAccessFile rf;
    private String errorMessage;
    protected DMLabel dmLabel;
    protected List<DMFileHeaderInfo> fileHeaderInfo;
    protected DMHeaders headers;
    protected DMKeys keys;
    protected List<DMPart> parts;
    protected int MTMACH;
    protected boolean mvmst;
    protected boolean needToSwap;
    protected long fileSize;
    private static int mskpat;
    private static String[] swapKeys;
    private static int[] swapNum;
    
    GempakFileReader() {
        this.MTMACH = 0;
        this.mvmst = false;
        this.needToSwap = false;
        this.fileSize = 0L;
    }
    
    public static RandomAccessFile getFile(final String filename) throws IOException {
        return new RandomAccessFile(filename, "r", 2048);
    }
    
    public static GempakFileReader getInstance(final RandomAccessFile raf, final boolean fullCheck) throws IOException {
        final GempakFileReader gfr = new GempakFileReader();
        gfr.init(raf, fullCheck);
        return gfr;
    }
    
    public final void init(final RandomAccessFile raf, final boolean fullCheck) throws IOException {
        this.setByteOrder();
        (this.rf = raf).seek(0L);
        final boolean ok = this.init(fullCheck);
        this.fileSize = this.rf.length();
        if (!ok) {
            throw new IOException("Unable to open GEMPAK file: " + this.errorMessage);
        }
    }
    
    protected boolean init() throws IOException {
        return this.init(true);
    }
    
    protected boolean init(final boolean fullCheck) throws IOException {
        if (this.rf == null) {
            throw new IOException("file has not been set");
        }
        this.dmLabel = new DMLabel();
        final boolean labelOk = this.dmLabel.init();
        if (!labelOk) {
            this.logError("not a GEMPAK file");
            return false;
        }
        this.readKeys();
        if (this.keys == null) {
            this.logError("Couldn't read keys");
            return false;
        }
        this.readHeaders();
        if (this.headers == null) {
            this.logError("Couldn't read headers");
            return false;
        }
        this.readParts();
        if (this.parts == null) {
            this.logError("Couldn't read parts");
            return false;
        }
        this.readFileHeaderInfo();
        if (this.fileHeaderInfo == null) {
            this.logError("Couldn't read file header info");
            return false;
        }
        return true;
    }
    
    public String getFilename() {
        return (this.rf == null) ? null : this.rf.getLocation();
    }
    
    public long getInitFileSize() {
        return this.fileSize;
    }
    
    public int getByteOrder() {
        return this.MTMACH;
    }
    
    public int getByteOrder(final int kmachn) {
        if (kmachn == 2 || kmachn == 8 || kmachn == 10 || kmachn == 11 || kmachn == 7) {
            return 1;
        }
        return 0;
    }
    
    private void setByteOrder() {
        final String arch = System.getProperty("os.arch");
        if (arch.equals("x86") || arch.equals("arm") || arch.equals("alpha")) {
            this.MTMACH = 1;
        }
        else {
            this.MTMACH = 0;
        }
    }
    
    protected void readFileHeaderInfo() throws IOException {
        if (this.dmLabel == null) {
            return;
        }
        int iread = this.dmLabel.kpfile;
        final int numheaders = this.dmLabel.kfhdrs;
        final String[] names = new String[numheaders];
        final int[] lens = new int[numheaders];
        final int[] types = new int[numheaders];
        for (int i = 0; i < numheaders; ++i) {
            names[i] = this.DM_RSTR(iread++);
        }
        for (int i = 0; i < numheaders; ++i) {
            lens[i] = this.DM_RINT(iread++);
        }
        for (int i = 0; i < numheaders; ++i) {
            types[i] = this.DM_RINT(iread++);
        }
        this.fileHeaderInfo = new ArrayList<DMFileHeaderInfo>();
        for (int i = 0; i < numheaders; ++i) {
            final DMFileHeaderInfo ghi = new DMFileHeaderInfo();
            ghi.kfhnam = names[i];
            ghi.kfhlen = lens[i];
            ghi.kfhtyp = types[i];
            this.fileHeaderInfo.add(ghi);
        }
    }
    
    protected void readKeys() throws IOException {
        if (this.dmLabel == null) {
            return;
        }
        this.keys = new DMKeys();
        int num = this.dmLabel.krkeys;
        final List<Key> rkeys = new ArrayList<Key>(num);
        for (int i = 0; i < num; ++i) {
            final String key = this.DM_RSTR(this.dmLabel.kprkey + i);
            rkeys.add(new Key(key, i, "ROW"));
        }
        this.keys.kkrow = rkeys;
        num = this.dmLabel.kckeys;
        final List<Key> ckeys = new ArrayList<Key>(num);
        for (int j = 0; j < num; ++j) {
            final String key2 = this.DM_RSTR(this.dmLabel.kpckey + j);
            ckeys.add(new Key(key2, j, "COL"));
        }
        this.keys.kkcol = ckeys;
    }
    
    protected void readHeaders() throws IOException {
        if (this.dmLabel == null) {
            return;
        }
        this.headers = new DMHeaders();
        final List<int[]> rowHeaders = new ArrayList<int[]>(this.dmLabel.krow);
        int istart = this.dmLabel.kprowh;
        for (int i = 0; i < this.dmLabel.krow; ++i) {
            final int[] header = new int[this.dmLabel.krkeys + 1];
            this.DM_RINT(istart, header);
            if (header[0] != -9999) {
                this.headers.lstrw = i;
            }
            rowHeaders.add(header);
            istart += header.length;
        }
        this.headers.rowHeaders = rowHeaders;
        final List<int[]> colHeaders = new ArrayList<int[]>(this.dmLabel.kcol);
        istart = this.dmLabel.kpcolh;
        for (int j = 0; j < this.dmLabel.kcol; ++j) {
            final int[] header = new int[this.dmLabel.kckeys + 1];
            this.DM_RINT(istart, header);
            if (header[0] != -9999) {
                this.headers.lstcl = j;
            }
            colHeaders.add(header);
            istart += header.length;
        }
        this.headers.colHeaders = colHeaders;
        if (this.needToSwap) {
            final int[] keyLoc = new int[GempakFileReader.swapKeys.length];
            final String[] keyType = new String[GempakFileReader.swapKeys.length];
            boolean haveRow = false;
            boolean haveCol = false;
            for (int k = 0; k < GempakFileReader.swapKeys.length; ++k) {
                final Key key = this.findKey(GempakFileReader.swapKeys[k]);
                keyLoc[k] = ((key != null) ? (key.loc + 1) : 0);
                keyType[k] = ((key != null) ? key.type : "");
                if (keyType[k].equals("ROW")) {
                    haveRow = true;
                }
                if (keyType[k].equals("COL")) {
                    haveCol = true;
                }
            }
            if (haveRow) {
                for (final int[] toCheck : this.headers.rowHeaders) {
                    for (int l = 0; l < GempakFileReader.swapKeys.length; ++l) {
                        if (keyType[l].equals("ROW")) {
                            if (!GempakFileReader.swapKeys[l].equals("GVCD") || toCheck[keyLoc[l]] > GempakUtil.vertCoords.length) {
                                GempakUtil.swp4(toCheck, keyLoc[l], GempakFileReader.swapNum[l]);
                            }
                        }
                    }
                }
            }
            if (haveCol) {
                for (final int[] toCheck : this.headers.colHeaders) {
                    for (int l = 0; l < GempakFileReader.swapKeys.length; ++l) {
                        if (keyType[l].equals("COL")) {
                            if (!GempakFileReader.swapKeys[l].equals("GVCD") || toCheck[keyLoc[l]] > GempakUtil.vertCoords.length) {
                                GempakUtil.swp4(toCheck, keyLoc[l], GempakFileReader.swapNum[l]);
                            }
                        }
                    }
                }
            }
        }
    }
    
    protected void readParts() throws IOException {
        if (this.dmLabel == null) {
            return;
        }
        int iread = this.dmLabel.kppart;
        final int numParts = this.dmLabel.kprt;
        final DMPart[] partArray = new DMPart[numParts];
        for (int i = 0; i < numParts; ++i) {
            partArray[i] = new DMPart();
            final String partName = this.DM_RSTR(iread++);
            partArray[i].kprtnm = partName;
        }
        for (int i = 0; i < numParts; ++i) {
            final int headerLen = this.DM_RINT(iread++);
            partArray[i].klnhdr = headerLen;
        }
        for (int i = 0; i < numParts; ++i) {
            final int partType = this.DM_RINT(iread++);
            partArray[i].ktyprt = partType;
        }
        for (int i = 0; i < numParts; ++i) {
            partArray[i].kparms = this.DM_RINT(iread++);
        }
        for (int i = 0; i < numParts; ++i) {
            final int numParms = partArray[i].kparms;
            final List<DMParam> parms = new ArrayList<DMParam>(numParms);
            for (int j = 0; j < numParms; ++j) {
                final DMParam dmp = new DMParam();
                parms.add(dmp);
                dmp.kprmnm = this.DM_RSTR(iread++);
            }
            partArray[i].params = parms;
        }
        for (int i = 0; i < numParts; ++i) {
            final int numParms = partArray[i].kparms;
            final List parms2 = partArray[i].params;
            for (int j = 0; j < numParms; ++j) {
                final DMParam dmp = parms2.get(j);
                dmp.kscale = this.DM_RINT(iread++);
            }
        }
        for (int i = 0; i < numParts; ++i) {
            final int numParms = partArray[i].kparms;
            final List parms2 = partArray[i].params;
            for (int j = 0; j < numParms; ++j) {
                final DMParam dmp = parms2.get(j);
                dmp.koffst = this.DM_RINT(iread++);
            }
        }
        for (int i = 0; i < numParts; ++i) {
            final int numParms = partArray[i].kparms;
            final List parms2 = partArray[i].params;
            for (int j = 0; j < numParms; ++j) {
                final DMParam dmp = parms2.get(j);
                dmp.kbits = this.DM_RINT(iread++);
            }
        }
        this.parts = new ArrayList<DMPart>(numParts);
        for (int i = 0; i < numParts; ++i) {
            this.parts.add(partArray[i]);
        }
        for (final DMPart part : this.parts) {
            if (part.ktyprt == 4) {
                part.packInfo = new PackingInfo(part);
            }
        }
    }
    
    public static long getOffset(final int fortranWord) {
        return (fortranWord - 1) * 4L;
    }
    
    public static void main(final String[] args) throws IOException {
        if (args.length == 0) {
            System.out.println("need to supply a GEMPAK grid file name");
            System.exit(1);
        }
        final GempakFileReader gfr = getInstance(getFile(args[0]), true);
        gfr.printFileLabel();
        gfr.printKeys();
        gfr.printHeaders();
        gfr.printParts();
    }
    
    public Key findKey(final String name) {
        if (this.keys == null) {
            return null;
        }
        for (final Key key : this.keys.kkrow) {
            if (key.name.equals(name)) {
                return key;
            }
        }
        for (final Key key : this.keys.kkcol) {
            if (key.name.equals(name)) {
                return key;
            }
        }
        return null;
    }
    
    public DMFileHeaderInfo findFileHeader(final String name) {
        if (this.fileHeaderInfo == null || this.fileHeaderInfo.isEmpty()) {
            return null;
        }
        for (final DMFileHeaderInfo fhi : this.fileHeaderInfo) {
            if (name.equals(fhi.kfhnam)) {
                return fhi;
            }
        }
        return null;
    }
    
    public float[] getFileHeader(final String name) throws IOException {
        final DMFileHeaderInfo fh = this.findFileHeader(name);
        if (fh == null || fh.kfhtyp != 1) {
            return null;
        }
        final int knt = this.fileHeaderInfo.indexOf(fh);
        int iread = this.dmLabel.kpfile + 3 * this.dmLabel.kfhdrs;
        for (int i = 0; i < knt; ++i) {
            final DMFileHeaderInfo fhi = this.fileHeaderInfo.get(i);
            iread = iread + fhi.kfhlen + 1;
        }
        final int nword = this.DM_RINT(iread);
        if (nword <= 0) {
            this.logError("Invalid header length for " + name);
            return null;
        }
        ++iread;
        final float[] rheader = new float[nword];
        if (name.equals("NAVB") && this.needToSwap) {
            this.DM_RFLT(iread, 1, rheader, 0);
            this.needToSwap = false;
            ++iread;
            this.DM_RFLT(iread, 1, rheader, 1);
            this.needToSwap = true;
            ++iread;
            this.DM_RFLT(iread, nword - 2, rheader, 2);
        }
        else {
            this.DM_RFLT(iread, rheader);
        }
        return rheader;
    }
    
    protected void logError(final String errMsg) {
        this.errorMessage = errMsg;
    }
    
    public void printFileLabel() {
        if (this.dmLabel == null) {
            return;
        }
        System.out.println(this.dmLabel);
    }
    
    public void printKeys() {
        if (this.keys == null) {
            return;
        }
        System.out.println(this.keys);
    }
    
    public void printHeaders() {
        if (this.headers == null) {
            return;
        }
        System.out.println(this.headers);
    }
    
    public void printParts() {
        if (this.parts == null) {
            return;
        }
        for (int i = 0; i < this.parts.size(); ++i) {
            System.out.println("\nParts[" + i + "]:");
            System.out.println(this.parts.get(i));
        }
    }
    
    public int getPartNumber(final String name) {
        int part = 0;
        if (this.parts != null && !this.parts.isEmpty()) {
            for (int i = 0; i < this.parts.size(); ++i) {
                final String partName = this.parts.get(i).kprtnm;
                if (partName.equals(name)) {
                    part = i + 1;
                    break;
                }
            }
        }
        return part;
    }
    
    public DMPart getPart(final String name) {
        if (this.parts != null && !this.parts.isEmpty()) {
            for (int i = 0; i < this.parts.size(); ++i) {
                final DMPart part = this.parts.get(i);
                final String partName = part.kprtnm;
                if (partName.equals(name)) {
                    return part;
                }
            }
        }
        return null;
    }
    
    public int getDataPointer(final int irow, final int icol, final String partName) {
        int ipoint = -1;
        if (irow < 1 || irow > this.dmLabel.krow || icol < 1 || icol > this.dmLabel.kcol) {
            System.out.println("bad row or column number: " + irow + "/" + icol);
            return ipoint;
        }
        final int iprt = this.getPartNumber(partName);
        if (iprt == 0) {
            System.out.println("couldn't find part");
            return ipoint;
        }
        final DMPart part = this.parts.get(iprt - 1);
        if (part.ktyprt != 1 && part.ktyprt != 5 && part.ktyprt != 4) {
            System.out.println("Not a valid type");
            return ipoint;
        }
        final int ilenhd = part.klnhdr;
        ipoint = this.dmLabel.kpdata + (irow - 1) * this.dmLabel.kcol * this.dmLabel.kprt + (icol - 1) * this.dmLabel.kprt + (iprt - 1);
        return ipoint;
    }
    
    public int DM_RINT(final int word) throws IOException {
        if (this.rf == null) {
            throw new IOException("DM_RINT: no file to read from");
        }
        if (this.dmLabel == null) {
            throw new IOException("DM_RINT: reader not initialized");
        }
        this.rf.seek(getOffset(word));
        if (this.needToSwap) {
            this.rf.order(1);
        }
        else {
            this.rf.order(0);
        }
        int idata = this.rf.readInt();
        if (-9999 != this.dmLabel.kmissd && idata == this.dmLabel.kmissd) {
            idata = -9999;
        }
        this.rf.order(0);
        return idata;
    }
    
    public void DM_RINT(final int word, final int[] iarray) throws IOException {
        this.DM_RINT(word, iarray.length, iarray, 0);
    }
    
    public void DM_RINT(final int word, final int num, final int[] iarray, final int start) throws IOException {
        for (int i = 0; i < num; ++i) {
            if (start + i > iarray.length) {
                throw new IOException("DM_RINT: start+num exceeds iarray length");
            }
            iarray[start + i] = this.DM_RINT(word + i);
        }
    }
    
    public float DM_RFLT(final int word) throws IOException {
        if (this.rf == null) {
            throw new IOException("DM_RFLT: no file to read from");
        }
        if (this.dmLabel == null) {
            throw new IOException("DM_RFLT: reader not initialized");
        }
        this.rf.seek(getOffset(word));
        if (this.needToSwap) {
            this.rf.order(1);
        }
        else {
            this.rf.order(0);
        }
        float rdata = this.rf.readFloat();
        if (-9999.0 != this.dmLabel.smissd && Math.abs(rdata - this.dmLabel.smissd) < 0.10000000149011612) {
            rdata = -9999.0f;
        }
        this.rf.order(0);
        return rdata;
    }
    
    public void DM_RFLT(final int word, final float[] rarray) throws IOException {
        this.DM_RFLT(word, rarray.length, rarray, 0);
    }
    
    public void DM_RFLT(final int word, final int num, final float[] rarray, final int start) throws IOException {
        for (int i = 0; i < num; ++i) {
            if (start + i > rarray.length) {
                throw new IOException("DM_RFLT: start+num exceeds rarray length");
            }
            rarray[start + i] = this.DM_RFLT(word + i);
        }
    }
    
    public String DM_RSTR(final int isword) throws IOException {
        return this.DM_RSTR(isword, 4);
    }
    
    public String DM_RSTR(final int isword, final int nchar) throws IOException {
        if (this.rf == null) {
            throw new IOException("DM_RSTR: no file to read from");
        }
        this.rf.seek(getOffset(isword));
        return this.rf.readString(nchar);
    }
    
    public RData DM_RDTR(final int irow, final int icol, final String partName) throws IOException {
        return this.DM_RDTR(irow, icol, partName, 1);
    }
    
    public RData DM_RDTR(final int irow, final int icol, final String partName, final int decimalScale) throws IOException {
        int ipoint = -1;
        if (irow < 1 || irow > this.dmLabel.krow || icol < 1 || icol > this.dmLabel.kcol) {
            System.out.println("bad row/column number " + irow + "/" + icol);
            return null;
        }
        final int iprt = this.getPartNumber(partName);
        if (iprt == 0) {
            System.out.println("couldn't find part: " + partName);
            return null;
        }
        final DMPart part = this.parts.get(iprt - 1);
        if (part.ktyprt != 1 && part.ktyprt != 5 && part.ktyprt != 4) {
            System.out.println("Not a valid type");
            return null;
        }
        final int ilenhd = part.klnhdr;
        ipoint = this.dmLabel.kpdata + (irow - 1) * this.dmLabel.kcol * this.dmLabel.kprt + (icol - 1) * this.dmLabel.kprt + (iprt - 1);
        float[] rdata = null;
        int[] header = null;
        final int istart = this.DM_RINT(ipoint);
        if (istart == 0) {
            return null;
        }
        try {
            final int length = this.DM_RINT(istart);
            int isword = istart + 1;
            if (length <= ilenhd) {
                return null;
            }
            if (Math.abs(length) > 10000000) {
                return null;
            }
            header = new int[ilenhd];
            this.DM_RINT(isword, header);
            final int nword = length - ilenhd;
            isword += header.length;
            if (part.ktyprt == 1) {
                rdata = new float[nword];
                this.DM_RFLT(isword, rdata);
            }
            else if (part.ktyprt == 5) {
                rdata = this.DM_RPKG(isword, nword, decimalScale);
            }
            else {
                final int[] idata = new int[nword];
                this.DM_RINT(isword, idata);
                rdata = this.DM_UNPK(part, idata);
            }
        }
        catch (EOFException eof) {
            rdata = null;
        }
        RData rd = null;
        if (rdata != null) {
            rd = new RData(header, rdata);
        }
        return rd;
    }
    
    public float[] DM_UNPK(final DMPart part, final int[] ibitst) {
        final int nparms = part.kparms;
        final int nwordp = part.kwordp;
        final int npack = (ibitst.length - 1) / nwordp + 1;
        if (npack * nwordp != ibitst.length) {
            return null;
        }
        final float[] data = new float[nparms * npack];
        final PackingInfo pkinf = part.packInfo;
        int ir = 0;
        int ii = 0;
        for (int pack = 0; pack < npack; ++pack) {
            final int[] jdata = new int[nwordp];
            for (int i = 0; i < nwordp; ++i) {
                jdata[i] = ibitst[ii + i];
            }
            for (int idata = 0; idata < nparms; ++idata) {
                final int jbit = pkinf.nbitsc[idata];
                final int jsbit = pkinf.isbitc[idata];
                int jshift = 1 - jsbit;
                final int jsword = pkinf.iswrdc[idata];
                int jword = jdata[jsword];
                final int mask = GempakFileReader.mskpat >>> 32 - jbit;
                int ifield = jword >>> Math.abs(jshift);
                ifield &= mask;
                if (jsbit + jbit - 1 > 32) {
                    jword = jdata[jsword + 1];
                    jshift += 32;
                    int iword = jword << jshift;
                    iword &= mask;
                    ifield |= iword;
                }
                if (ifield == pkinf.imissc[idata]) {
                    data[ir + idata] = -9999.0f;
                }
                else {
                    data[ir + idata] = (ifield + pkinf.koffst[idata]) * (float)pkinf.scalec[idata];
                }
            }
            ir += nparms;
            ii += nwordp;
        }
        return data;
    }
    
    protected static String getBits(final int b) {
        String s = "";
        for (int i = 31; i >= 0; --i) {
            if ((b & 1 << i) != 0x0) {
                s += "1";
            }
            else {
                s += "0";
            }
            if (i % 8 == 0) {
                s += "|";
            }
        }
        return s;
    }
    
    public float[] DM_RPKG(final int isword, final int nword, final int decimalScale) throws IOException {
        return null;
    }
    
    static {
        GempakFileReader.mskpat = -1;
        GempakFileReader.swapKeys = new String[] { "STID", "STD2", "STAT", "COUN", "GPM1", "GVCD" };
        GempakFileReader.swapNum = new int[] { 1, 1, 1, 1, 3, 1 };
    }
    
    protected class DMLabel
    {
        public static final String DMLABEL = "GEMPAK DATA MANAGEMENT FILE ";
        public int kversn;
        public int kfhdrs;
        public int kpfile;
        public int krow;
        public int krkeys;
        public int kprkey;
        public int kprowh;
        public int kcol;
        public int kckeys;
        public int kpckey;
        public int kpcolh;
        public int kprt;
        public int kppart;
        public int kpdmgt;
        public int kldmgt;
        public int kpdata;
        public int kftype;
        public int kfsrce;
        public int kmachn;
        public int kmissd;
        public double smissd;
        public boolean kvmst;
        public boolean kieeet;
        
        public DMLabel() {
        }
        
        public boolean init() throws IOException {
            if (GempakFileReader.this.rf == null) {
                throw new IOException("File is null");
            }
            GempakFileReader.this.rf.order(0);
            int mmmm = GempakFileReader.this.DM_RINT(26);
            if (mmmm > 100) {
                mmmm = GempakUtil.swp4(mmmm);
                GempakFileReader.this.needToSwap = true;
            }
            this.kmachn = mmmm;
            GempakFileReader.this.mvmst = (GempakFileReader.this.getByteOrder() == 0);
            this.kvmst = (this.kmachn == 2 || this.kmachn == 8 || this.kmachn == 10 || this.kmachn == 11 || this.kmachn == 7);
            this.kmissd = -9999;
            this.smissd = -9999.0;
            final String label = GempakFileReader.this.DM_RSTR(1, 28);
            if (!label.equals("GEMPAK DATA MANAGEMENT FILE ")) {
                return false;
            }
            final int[] words = new int[23];
            GempakFileReader.this.DM_RINT(8, words);
            this.kversn = words[0];
            this.kfhdrs = words[1];
            this.kpfile = words[2];
            this.krow = words[3];
            this.krkeys = words[4];
            this.kprkey = words[5];
            this.kprowh = words[6];
            this.kcol = words[7];
            this.kckeys = words[8];
            this.kpckey = words[9];
            this.kpcolh = words[10];
            this.kprt = words[11];
            this.kppart = words[12];
            this.kpdmgt = words[13];
            this.kldmgt = words[14];
            this.kpdata = words[15];
            this.kftype = words[16];
            this.kfsrce = words[17];
            this.kmissd = words[19];
            this.smissd = GempakFileReader.this.DM_RFLT(31);
            return true;
        }
        
        @Override
        public String toString() {
            final StringBuffer buf = new StringBuffer();
            buf.append("GEMPAK file label:\n");
            buf.append("\tVersion: " + this.kversn + "\n");
            buf.append("\t# File keys: " + this.kfhdrs + "\n");
            buf.append("\tptr to file keys: " + this.kpfile + "\n");
            buf.append("\t# rows: " + this.krow + "\n");
            buf.append("\t# row keys: " + this.krkeys + "\n");
            buf.append("\tptr to row  keys: " + this.kprkey + "\n");
            buf.append("\tprt to row header: " + this.kprowh + "\n");
            buf.append("\t# cols: " + this.kcol + "\n");
            buf.append("\t# cols keys: " + this.kckeys + "\n");
            buf.append("\tptr to col keys: " + this.kpckey + "\n");
            buf.append("\tptr to col header: " + this.kpcolh + "\n");
            buf.append("\t# parts: " + this.kprt + "\n");
            buf.append("\tptr part info: " + this.kppart + "\n");
            buf.append("\tptr to data mgmt record: " + this.kpdmgt + "\n");
            buf.append("\tlen of data mgmt record: " + this.kldmgt + "\n");
            buf.append("\tdata pointer: " + this.kpdata + "\n");
            buf.append("\tfile type: " + this.kftype + "\n");
            buf.append("\tfile source: " + this.kfsrce + "\n");
            buf.append("\tmachine type: " + this.kmachn + "\n");
            buf.append("\tinteger missing value: " + this.kmissd + "\n");
            buf.append("\tfloat missing value: " + this.smissd + "\n");
            buf.append("\tswap? " + GempakFileReader.this.needToSwap);
            return buf.toString();
        }
    }
    
    protected class DMFileHeaderInfo
    {
        public String kfhnam;
        public int kfhlen;
        public int kfhtyp;
        
        public DMFileHeaderInfo() {
        }
        
        @Override
        public String toString() {
            final StringBuffer buf = new StringBuffer();
            buf.append("Name = ");
            buf.append(this.kfhnam);
            buf.append("; length = ");
            buf.append(this.kfhlen);
            buf.append("; type = ");
            buf.append(this.kfhtyp);
            return buf.toString();
        }
    }
    
    protected class DMPart
    {
        public String kprtnm;
        public int klnhdr;
        public int ktyprt;
        public int kparms;
        public List<DMParam> params;
        public int kpkno;
        public int kwordp;
        public PackingInfo packInfo;
        
        public DMPart() {
            this.packInfo = null;
        }
        
        @Override
        public String toString() {
            final StringBuffer buf = new StringBuffer();
            buf.append("Part Name = ");
            buf.append(this.kprtnm);
            buf.append("; header length = ");
            buf.append(this.klnhdr);
            buf.append("; type = ");
            buf.append(this.ktyprt);
            buf.append("; packing num = ");
            buf.append(this.kpkno);
            buf.append("; packed rec len = ");
            buf.append(this.kwordp);
            buf.append("\nParameters: ");
            if (this.params != null && !this.params.isEmpty()) {
                for (int i = 0; i < this.params.size(); ++i) {
                    buf.append("\n  " + this.params.get(i));
                }
            }
            return buf.toString();
        }
    }
    
    protected class DMParam
    {
        public String kprmnm;
        public int kscale;
        public int koffst;
        public int kbits;
        
        public DMParam() {
        }
        
        @Override
        public String toString() {
            final StringBuffer buf = new StringBuffer();
            buf.append("Param name = ");
            buf.append(this.kprmnm);
            buf.append("; scale = ");
            buf.append(this.kscale);
            buf.append("; offset = ");
            buf.append(this.koffst);
            buf.append("; bits = ");
            buf.append(this.kbits);
            return buf.toString();
        }
    }
    
    protected class PackingInfo
    {
        public int[] koffst;
        public int[] nbitsc;
        public double[] scalec;
        public int[] imissc;
        public int[] iswrdc;
        public int[] isbitc;
        
        public PackingInfo(final DMPart part) {
            final List<DMParam> params = part.params;
            final int numParams = params.size();
            this.koffst = new int[numParams];
            this.nbitsc = new int[numParams];
            this.scalec = new double[numParams];
            this.imissc = new int[numParams];
            this.iswrdc = new int[numParams];
            this.isbitc = new int[numParams];
            int i = 0;
            int itotal = 0;
            for (final DMParam param : params) {
                this.koffst[i] = param.koffst;
                this.nbitsc[i] = param.kbits;
                this.scalec[i] = Math.pow(10.0, param.kscale);
                this.imissc[i] = GempakFileReader.mskpat >>> 32 - param.kbits;
                this.iswrdc[i] = itotal / 32;
                this.isbitc[i++] = itotal % 32 + 1;
                itotal += param.kbits;
            }
            part.kwordp = (itotal - 1) / 32 + 1;
        }
    }
    
    protected class Key
    {
        public String name;
        public int loc;
        public String type;
        
        public Key(final String name, final int loc, final String type) {
            this.name = name;
            this.loc = loc;
            this.type = type;
        }
    }
    
    protected class DMKeys
    {
        public List<Key> kkrow;
        public List<Key> kkcol;
        
        public DMKeys() {
        }
        
        @Override
        public String toString() {
            final StringBuffer buf = new StringBuffer();
            buf.append("\nKeys:\n");
            buf.append("  row keys = ");
            for (final Key key : GempakFileReader.this.keys.kkrow) {
                buf.append(key.name);
                buf.append(", ");
            }
            buf.append("\n");
            buf.append("  column keys = ");
            for (final Key key : GempakFileReader.this.keys.kkcol) {
                buf.append(key.name);
                buf.append(", ");
            }
            return buf.toString();
        }
    }
    
    protected class DMHeaders
    {
        public int lstrw;
        public int lstcl;
        public List<int[]> rowHeaders;
        public List<int[]> colHeaders;
        
        public DMHeaders() {
            this.lstrw = 0;
            this.lstcl = 0;
        }
        
        @Override
        public String toString() {
            final StringBuilder buf = new StringBuilder();
            buf.append("\nHeaders:\n");
            if (this.rowHeaders != null) {
                buf.append("  num row headers = ");
                buf.append(this.rowHeaders.size());
                buf.append("\n");
            }
            buf.append("  last row = ");
            buf.append(this.lstrw);
            buf.append("\n");
            if (this.colHeaders != null) {
                buf.append("  num column headers = ");
                buf.append(this.colHeaders.size());
                buf.append("\n");
            }
            buf.append("  last column = ");
            buf.append(this.lstcl);
            return buf.toString();
        }
    }
    
    public class RData
    {
        public int[] header;
        public float[] data;
        
        public RData(final int[] header, final float[] data) {
            this.header = header;
            this.data = data;
        }
    }
}
