// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.iosp.gempak;

import java.io.IOException;
import ucar.grib.QuasiRegular;
import ucar.grib.grib2.Grib2DataSection;
import ucar.grib.grib2.Grib2BitMapSection;
import ucar.grib.grib2.Grib2DataRepresentationSection;
import ucar.grib.grib2.Grib2ProductDefinitionSection;
import ucar.grib.grib2.Grib2GridDefinitionSection;
import ucar.grib.grib2.Grib2LocalUseSection;
import ucar.grib.grib2.Grib2IdentificationSection;
import ucar.unidata.io.RandomAccessFile;

public final class GempakGrib2Data
{
    private RandomAccessFile raf;
    private boolean expandQuasi;
    
    public GempakGrib2Data(final RandomAccessFile raf) {
        this.raf = null;
        this.expandQuasi = true;
        this.raf = raf;
    }
    
    public GempakGrib2Data(final RandomAccessFile raf, final boolean expandQuasi) {
        this.raf = null;
        this.expandQuasi = true;
        this.raf = raf;
        this.expandQuasi = expandQuasi;
    }
    
    public final float[] getData(final long start, final long refTime) throws IOException {
        final long time = System.currentTimeMillis();
        Grib2IdentificationSection id = null;
        Grib2LocalUseSection lus = null;
        Grib2GridDefinitionSection gds = null;
        Grib2ProductDefinitionSection pds = null;
        Grib2DataRepresentationSection drs = null;
        Grib2BitMapSection bms = null;
        Grib2DataSection ds = null;
        final RandomAccessFile raf = this.raf;
        final RandomAccessFile raf2 = this.raf;
        raf.order(0);
        this.raf.seek(start);
        int secLength = this.raf.readInt();
        if (secLength > 0) {
            id = new Grib2IdentificationSection(this.raf);
        }
        secLength = this.raf.readInt();
        if (secLength > 0) {
            lus = new Grib2LocalUseSection(this.raf);
        }
        secLength = this.raf.readInt();
        if (secLength > 0) {
            gds = new Grib2GridDefinitionSection(this.raf, false);
        }
        secLength = this.raf.readInt();
        if (secLength > 0) {
            pds = new Grib2ProductDefinitionSection(this.raf, refTime);
        }
        secLength = this.raf.readInt();
        if (secLength > 0) {
            drs = new Grib2DataRepresentationSection(this.raf);
        }
        secLength = this.raf.readInt();
        if (secLength > 0) {
            bms = new Grib2BitMapSection(true, this.raf, gds);
        }
        if (bms.getBitmapIndicator() == 254) {
            final long offset = this.raf.getFilePointer();
            gds = new Grib2GridDefinitionSection(this.raf, false);
            final Grib2ProductDefinitionSection savepds = pds;
            pds = new Grib2ProductDefinitionSection(this.raf, refTime);
            final Grib2DataRepresentationSection savedrs = drs;
            drs = new Grib2DataRepresentationSection(this.raf);
            bms = new Grib2BitMapSection(true, this.raf, gds);
            pds = savepds;
            drs = savedrs;
            this.raf.seek(offset);
        }
        secLength = this.raf.readInt();
        if (secLength > 0) {
            ds = new Grib2DataSection(true, this.raf, gds, drs, bms);
        }
        if (gds.getOlon() == 0 || !this.expandQuasi) {
            return ds.getData();
        }
        final QuasiRegular qr = new QuasiRegular(ds.getData(), (Object)gds);
        return qr.getData();
    }
}
