// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.iosp.gempak;

import org.slf4j.LoggerFactory;
import java.util.Iterator;
import java.util.List;
import ucar.grid.GridRecord;
import java.util.ArrayList;
import ucar.grid.GridDefRecord;
import java.io.IOException;
import ucar.unidata.io.RandomAccessFile;
import ucar.grid.GridIndex;
import org.slf4j.Logger;

public class GempakGridReader extends GempakFileReader
{
    private static Logger log;
    public static final String GRID = "GRID";
    public static final String ANLB = "ANLB";
    public static final String NAVB = "NAVB";
    private NavigationBlock navBlock;
    private AnalysisBlock analBlock;
    private GridIndex gridIndex;
    private static final String[] kcolnm;
    private int khdrln;
    private final String filename;
    public static boolean useDP;
    private int bitPos;
    private int bitBuf;
    private int next;
    private int ch1;
    private int ch2;
    private int ch3;
    private int ch4;
    
    GempakGridReader(final String filename) {
        this.khdrln = 0;
        this.bitPos = 0;
        this.bitBuf = 0;
        this.next = 0;
        this.ch1 = 0;
        this.ch2 = 0;
        this.ch3 = 0;
        this.ch4 = 0;
        this.filename = filename;
    }
    
    public static GempakGridReader getInstance(final RandomAccessFile raf, final boolean fullCheck) throws IOException {
        final GempakGridReader ggr = new GempakGridReader(raf.getLocation());
        ggr.init(raf, fullCheck);
        return ggr;
    }
    
    @Override
    protected boolean init(final boolean fullCheck) throws IOException {
        if (!super.init(fullCheck)) {
            return false;
        }
        if (this.dmLabel.kftype != 3) {
            this.logError("not a grid file");
            return false;
        }
        final DMPart part = this.getPart("GRID");
        if (part == null) {
            this.logError("No part named GRID found");
            return false;
        }
        final int lenhdr = part.klnhdr;
        if (lenhdr > 128) {
            this.logError("Grid part header too long");
            return false;
        }
        this.khdrln = lenhdr - 2;
        for (int i = 0; i < this.keys.kkcol.size(); ++i) {
            final Key colkey = this.keys.kkcol.get(i);
            if (!colkey.name.equals(GempakGridReader.kcolnm[i])) {
                this.logError("Column name " + colkey + " doesn't match " + GempakGridReader.kcolnm[i]);
                return false;
            }
        }
        if (!fullCheck) {
            return true;
        }
        this.gridIndex = new GridIndex(this.filename);
        float[] headerArray = this.getFileHeader("NAVB");
        if (headerArray == null) {
            return false;
        }
        this.navBlock = new NavigationBlock(headerArray);
        this.gridIndex.addHorizCoordSys((GridDefRecord)this.navBlock);
        headerArray = this.getFileHeader("ANLB");
        if (headerArray == null) {
            return false;
        }
        this.analBlock = new AnalysisBlock(headerArray);
        final List<GempakGridRecord> tmpList = new ArrayList<GempakGridRecord>();
        final int[] header = new int[this.dmLabel.kckeys];
        if (this.headers == null || this.headers.colHeaders == null) {
            return false;
        }
        int gridNum = 0;
        for (final int[] fullHeader : this.headers.colHeaders) {
            ++gridNum;
            if (fullHeader != null) {
                if (fullHeader[0] == -9999) {
                    continue;
                }
                System.arraycopy(fullHeader, 1, header, 0, header.length);
                final GempakGridRecord gh = new GempakGridRecord(gridNum, header);
                gh.navBlock = this.navBlock;
                final String name = gh.getParameterName();
                tmpList.add(gh);
            }
        }
        this.fileSize = this.rf.length();
        if (!tmpList.isEmpty()) {
            for (int j = 0; j < tmpList.size(); ++j) {
                final GempakGridRecord gh2 = tmpList.get(j);
                gh2.packingType = this.getGridPackingType(gh2.gridNumber);
                if (gh2.packingType == 1 || gh2.packingType == 5 || gh2.packingType == 0) {
                    this.gridIndex.addGridRecord((GridRecord)gh2);
                }
            }
            return !this.gridIndex.getGridRecords().isEmpty();
        }
        return false;
    }
    
    public static void main(final String[] args) throws IOException {
        if (args.length == 0) {
            System.out.println("need to supply a GEMPAK grid file name");
            System.exit(1);
        }
        try {
            GempakGridParameterTable.addParameters("resources/nj22/tables/gempak/wmogrib3.tbl");
            GempakGridParameterTable.addParameters("resources/nj22/tables/gempak/ncepgrib2.tbl");
        }
        catch (Exception e) {
            System.out.println("unable to init param tables");
        }
        final GempakGridReader ggr = getInstance(GempakFileReader.getFile(args[0]), true);
        String var = "PMSL";
        if (args.length > 1 && !args[1].equalsIgnoreCase("X")) {
            var = args[1];
        }
        ggr.showGridInfo(args.length != 3);
        final GempakGridRecord gh = ggr.findGrid(var);
        if (gh != null) {
            System.out.println("\n" + var + ":");
            System.out.println(gh);
            for (int j = 0; j < 2; ++j) {
                System.out.println("Using DP: " + GempakGridReader.useDP);
                final float[] data = ggr.readGrid((GridRecord)gh);
                if (data != null) {
                    System.out.println("# of points = " + data.length);
                    int cnt = 0;
                    final int it = 10;
                    float min = Float.POSITIVE_INFINITY;
                    float max = Float.NEGATIVE_INFINITY;
                    for (int i = 0; i < data.length; ++i) {
                        if (cnt == it) {
                            cnt = 0;
                        }
                        ++cnt;
                        if (data[i] != -9999.0f && data[i] < min) {
                            min = data[i];
                        }
                        if (data[i] != -9999.0f && data[i] > max) {
                            max = data[i];
                        }
                    }
                    System.out.println("max/min = " + max + "/" + min);
                }
                else {
                    System.out.println("unable to decode grid data");
                }
                GempakGridReader.useDP = !GempakGridReader.useDP;
            }
        }
    }
    
    public GridIndex getGridIndex() {
        return this.gridIndex;
    }
    
    public int getGridCount() {
        return this.gridIndex.getGridCount();
    }
    
    public int getGridPackingType(final int gridNumber) throws IOException {
        final int irow = 1;
        final int icol = gridNumber;
        if (icol < 1 || icol > this.dmLabel.kcol) {
            this.logWarning("bad grid number " + icol);
            return -9;
        }
        final int iprt = this.getPartNumber("GRID");
        if (iprt == 0) {
            this.logWarning("couldn't find part: GRID");
            return -10;
        }
        final DMPart part = this.parts.get(iprt - 1);
        if (part.ktyprt != 5) {
            this.logWarning("Not a valid type: " + GempakUtil.getDataType(part.ktyprt));
            return -21;
        }
        final int ilenhd = part.klnhdr;
        final int ipoint = this.dmLabel.kpdata + (irow - 1) * this.dmLabel.kcol * this.dmLabel.kprt + (icol - 1) * this.dmLabel.kprt + (iprt - 1);
        final int istart = this.DM_RINT(ipoint);
        if (istart == 0) {
            return -15;
        }
        final int length = this.DM_RINT(istart);
        int isword = istart + 1;
        if (length <= ilenhd) {
            this.logWarning("length (" + length + ") is less than header length (" + ilenhd + ")");
            return -15;
        }
        if (Math.abs(length) > 10000000) {
            this.logWarning("length is huge: " + length);
            return -34;
        }
        final int[] header = new int[ilenhd];
        this.DM_RINT(isword, header);
        final int nword = length - ilenhd;
        isword += ilenhd;
        final int ipktyp = this.DM_RINT(isword);
        return ipktyp;
    }
    
    public GempakGridRecord findGrid(final String parm) {
        final List gridList = this.gridIndex.getGridRecords();
        if (gridList == null) {
            return null;
        }
        for (int i = 0; i < gridList.size(); ++i) {
            final GempakGridRecord gh = gridList.get(i);
            if (gh.param.trim().equals(parm)) {
                return gh;
            }
        }
        return null;
    }
    
    public float[] readGrid(final GridRecord gr) throws IOException {
        final int gridNumber = ((GempakGridRecord)gr).getGridNumber();
        final int irow = 1;
        final int icol = gridNumber;
        final RData data = this.DM_RDTR(1, gridNumber, "GRID", gr.getDecimalScale());
        float[] vals = null;
        if (data != null) {
            vals = data.data;
        }
        return vals;
    }
    
    @Override
    public float[] DM_RPKG(final int isword, final int nword, final int decimalScale) throws IOException {
        float[] data = null;
        final int ipktyp = this.DM_RINT(isword);
        int iiword = isword + 1;
        int lendat = nword - 1;
        if (ipktyp == 0) {
            data = new float[lendat];
            this.DM_RFLT(iiword, data);
            return data;
        }
        int iiw;
        int irw;
        if (ipktyp == 3) {
            iiw = 4;
            irw = 3;
        }
        else if (ipktyp == 5) {
            iiw = 4;
            irw = 1;
        }
        else {
            iiw = 3;
            irw = 2;
        }
        final int[] iarray = new int[iiw];
        final float[] rarray = new float[irw];
        this.DM_RINT(iiword, iarray);
        iiword += iiw;
        lendat -= iiw;
        this.DM_RFLT(iiword, rarray);
        iiword += irw;
        lendat -= irw;
        if (ipktyp == 5) {
            data = this.unpackGrib2Data(iiword, lendat, iarray, rarray);
            return data;
        }
        final int nbits = iarray[0];
        final int misflg = iarray[1];
        final boolean miss = misflg != 0;
        final int mword;
        final int kxky = mword = iarray[2];
        int kx = 0;
        if (iiw == 4) {
            kx = iarray[3];
        }
        final float ref = rarray[0];
        final float scale = rarray[1];
        float difmin = 0.0f;
        if (irw == 3) {
            difmin = rarray[2];
        }
        data = this.unpackData(iiword, lendat, ipktyp, kxky, nbits, ref, scale, miss, difmin, kx, decimalScale);
        return data;
    }
    
    private synchronized float[] unpackData(final int iiword, int nword, final int ipktyp, final int kxky, final int nbits, final float ref, final float scale, final boolean miss, final float difmin, final int kx, final int decimalScale) throws IOException {
        if (ipktyp == 1) {
            if (!GempakGridReader.useDP) {
                return this.unpackGrib1Data(iiword, nword, kxky, nbits, ref, scale, miss, decimalScale);
            }
            if (nword * 32 < kxky * nbits) {
                ++nword;
            }
            final int[] ksgrid = new int[nword];
            this.DM_RINT(iiword, ksgrid);
            return this.DP_UGRB(ksgrid, kxky, nbits, ref, scale, miss, decimalScale);
        }
        else {
            if (ipktyp == 2) {
                return null;
            }
            if (ipktyp == 3) {
                return null;
            }
            return null;
        }
    }
    
    private synchronized float[] DP_UGRB(final int[] idata, final int kxky, final int nbits, final float qmin, final float scale, final boolean misflg, final int decimalScale) throws IOException {
        final float scaleFactor = (decimalScale == 0) ? 1.0f : ((float)Math.pow(10.0, -decimalScale));
        final float[] grid = new float[kxky];
        if (nbits <= 1 || nbits > 31) {
            return grid;
        }
        if (scale == 0.0) {
            return grid;
        }
        final int imax = (int)(Math.pow(2.0, nbits) - 1.0);
        int iword = 0;
        int ibit = 1;
        for (int i = 0; i < kxky; ++i) {
            int jshft = nbits + ibit - 33;
            int idat = 0;
            idat = ((jshft < 0) ? (idata[iword] >>> Math.abs(jshft)) : (idata[iword] << jshft));
            idat &= imax;
            if (jshft > 0) {
                jshft -= 32;
                int idat2 = 0;
                idat2 = idata[iword + 1] >>> Math.abs(jshft);
                idat |= idat2;
            }
            if (idat == imax && misflg) {
                grid[i] = -9999.0f;
            }
            else {
                grid[i] = (qmin + idat * scale) * scaleFactor;
            }
            ibit += nbits;
            if (ibit > 32) {
                ibit -= 32;
                ++iword;
            }
        }
        return grid;
    }
    
    private float[] unpackGrib1Data(final int iiword, final int nword, final int kxky, final int nbits, final float ref, final float scale, final boolean miss, final int decimalScale) throws IOException {
        final float[] values = new float[kxky];
        this.bitPos = 0;
        this.bitBuf = 0;
        this.next = 0;
        this.ch1 = 0;
        this.ch2 = 0;
        this.ch3 = 0;
        this.ch4 = 0;
        this.rf.seek(GempakFileReader.getOffset(iiword));
        final float scaleFactor = (decimalScale == 0) ? 1.0f : ((float)Math.pow(10.0, -decimalScale));
        for (int i = 0; i < values.length; ++i) {
            final int idat = this.bits2UInt(nbits);
            if (miss && idat == -9999) {
                values[i] = -9999.0f;
            }
            else {
                values[i] = (ref + scale * idat) * scaleFactor;
            }
        }
        return values;
    }
    
    private float[] unpackGrib2Data(final int iiword, final int lendat, final int[] iarray, final float[] rarray) throws IOException {
        final long start = GempakFileReader.getOffset(iiword);
        final GempakGrib2Data gemGrib2 = new GempakGrib2Data(this.rf);
        float[] data = gemGrib2.getData(start, 0L);
        if ((iarray[3] >> 6 & 0x1) == 0x0) {
            data = this.gb2_ornt(iarray[1], iarray[2], iarray[3], data);
        }
        return data;
    }
    
    public void printNavBlock() {
        final StringBuffer buf = new StringBuffer("GRID NAVIGATION:");
        if (this.navBlock != null) {
            buf.append(this.navBlock.toString());
        }
        else {
            buf.append("\n\tUNKNOWN GRID NAVIGATION");
        }
        System.out.println(buf.toString());
    }
    
    public void printAnalBlock() {
        final StringBuffer buf = new StringBuffer("GRID ANALYSIS BLOCK:");
        if (this.analBlock != null) {
            buf.append(this.analBlock.toString());
        }
        else {
            buf.append("\n\tUNKNOWN ANALYSIS TYPE");
        }
        System.out.println(buf.toString());
    }
    
    public List<GridRecord> getGridList() {
        return (List<GridRecord>)this.gridIndex.getGridRecords();
    }
    
    public void printGrids() {
        final List gridList = this.gridIndex.getGridRecords();
        if (gridList == null) {
            return;
        }
        System.out.println("  NUM       TIME1              TIME2           LEVL1 LEVL2  VCORD PARM");
        final Iterator iter = gridList.iterator();
        while (iter.hasNext()) {
            System.out.println(iter.next());
        }
    }
    
    public void showGridInfo(final boolean printGrids) {
        final List gridList = this.gridIndex.getGridRecords();
        System.out.println("\nGRID FILE: " + this.getFilename() + "\n");
        this.printNavBlock();
        System.out.println("");
        this.printAnalBlock();
        System.out.println("\nNumber of grids in file:  " + gridList.size());
        System.out.println("\nMaximum number of grids in file:  " + this.dmLabel.kcol);
        System.out.println("");
        if (printGrids) {
            this.printGrids();
        }
    }
    
    private float[] gb2_ornt(final int kx, final int ky, final int scan_mode, final float[] ingrid) {
        float[] fgrid = new float[ingrid.length];
        final int idrct = scan_mode >> 7 & 0x1;
        final int jdrct = scan_mode >> 6 & 0x1;
        final int consec = scan_mode >> 5 & 0x1;
        final int boustr = scan_mode >> 4 & 0x1;
        int ibeg;
        int iinc;
        if (idrct == 0) {
            ibeg = 0;
            iinc = 1;
        }
        else {
            ibeg = kx - 1;
            iinc = -1;
        }
        int jbeg;
        int jinc;
        if (jdrct == 1) {
            jbeg = 0;
            jinc = 1;
        }
        else {
            jbeg = ky - 1;
            jinc = -1;
        }
        int kcnt = 0;
        if (consec == 1 && boustr == 0) {
            for (int jcnt = jbeg; 0 <= jcnt && jcnt < ky; jcnt += jinc) {
                for (int icnt = ibeg; 0 <= icnt && icnt < kx; icnt += iinc) {
                    final int idxarr = ky * icnt + jcnt;
                    fgrid[kcnt] = ingrid[idxarr];
                    ++kcnt;
                }
            }
        }
        else if (consec == 0 && boustr == 0) {
            for (int jcnt = jbeg; 0 <= jcnt && jcnt < ky; jcnt += jinc) {
                for (int icnt = ibeg; 0 <= icnt && icnt < kx; icnt += iinc) {
                    final int idxarr = kx * jcnt + icnt;
                    fgrid[kcnt] = ingrid[idxarr];
                    ++kcnt;
                }
            }
        }
        else if (consec == 1 && boustr == 1) {
            for (int jcnt = jbeg; 0 <= jcnt && jcnt < ky; jcnt += jinc) {
                int itmp = jcnt;
                if (idrct == 1 && kx % 2 == 0) {
                    itmp = ky - jcnt - 1;
                }
                for (int icnt = ibeg; 0 <= icnt && icnt < kx; icnt += iinc) {
                    final int idxarr = ky * icnt + itmp;
                    fgrid[kcnt] = ingrid[idxarr];
                    itmp = ((itmp != jcnt) ? jcnt : (ky - jcnt - 1));
                    ++kcnt;
                }
            }
        }
        else if (consec == 0 && boustr == 1) {
            if (jdrct == 0) {
                if (idrct == 0 && ky % 2 == 0) {
                    ibeg = kx - 1;
                    iinc = -1;
                }
                if (idrct == 1 && ky % 2 == 0) {
                    ibeg = 0;
                    iinc = 1;
                }
            }
            for (int jcnt = jbeg; 0 <= jcnt && jcnt < ky; jcnt += jinc) {
                for (int icnt = ibeg; 0 <= icnt && icnt < kx; icnt += iinc) {
                    final int idxarr = kx * jcnt + icnt;
                    fgrid[kcnt] = ingrid[idxarr];
                    ++kcnt;
                }
                ibeg = ((ibeg != 0) ? 0 : (kx - 1));
                iinc = ((iinc != 1) ? 1 : -1);
            }
        }
        else {
            fgrid = ingrid;
        }
        return fgrid;
    }
    
    private int bits2UInt(final int nb) throws IOException {
        int bitsLeft = nb;
        int result = 0;
        if (this.bitPos == 0) {
            this.getNextByte();
            this.bitPos = 8;
        }
        int shift;
        while (true) {
            shift = bitsLeft - this.bitPos;
            if (shift <= 0) {
                break;
            }
            result |= this.bitBuf << shift;
            bitsLeft -= this.bitPos;
            this.getNextByte();
            this.bitPos = 8;
        }
        result |= this.bitBuf >> -shift;
        this.bitPos -= bitsLeft;
        this.bitBuf &= 255 >> 8 - this.bitPos;
        return result;
    }
    
    private void getNextByte() throws IOException {
        if (!this.needToSwap) {
            this.bitBuf = this.rf.read();
        }
        else {
            if (this.next == 3) {
                this.bitBuf = this.ch3;
            }
            else if (this.next == 2) {
                this.bitBuf = this.ch2;
            }
            else if (this.next == 1) {
                this.bitBuf = this.ch1;
            }
            else {
                this.ch1 = this.rf.read();
                this.ch2 = this.rf.read();
                this.ch3 = this.rf.read();
                this.ch4 = this.rf.read();
                this.bitBuf = this.ch4;
                this.next = 4;
            }
            --this.next;
        }
    }
    
    private void logWarning(final String message) {
        GempakGridReader.log.warn(this.rf.getLocation() + ": " + message);
    }
    
    static {
        GempakGridReader.log = LoggerFactory.getLogger(GempakGridReader.class);
        kcolnm = new String[] { "GDT1", "GTM1", "GDT2", "GTM2", "GLV1", "GLV2", "GVCD", "GPM1", "GPM2", "GPM3" };
        GempakGridReader.useDP = true;
    }
}
