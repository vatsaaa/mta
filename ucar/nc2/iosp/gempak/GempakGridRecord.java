// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.iosp.gempak;

import ucar.unidata.util.StringUtil;
import java.util.Formatter;
import ucar.grid.GridTableLookup;
import java.util.TimeZone;
import java.util.Calendar;
import edu.wisc.ssec.mcidas.McIDASUtil;
import java.util.Date;
import ucar.grid.GridRecord;

public class GempakGridRecord implements GridRecord
{
    public String time1;
    public String time2;
    public int level1;
    public int level2;
    public int ivcord;
    public String param;
    public int gridNumber;
    public int packingType;
    public NavigationBlock navBlock;
    private Date refTime;
    private int validOffset;
    private int decimalScale;
    private Date validTime;
    
    public GempakGridRecord(final int number, final int[] header) {
        this.level1 = -9999;
        this.level2 = -9999;
        this.decimalScale = 0;
        this.gridNumber = number;
        final int[] times1 = GempakUtil.TG_FTOI(header, 0);
        this.time1 = GempakUtil.TG_ITOC(times1);
        final int[] times2 = GempakUtil.TG_FTOI(header, 2);
        this.time2 = GempakUtil.TG_ITOC(times2);
        this.level1 = header[4];
        this.level2 = header[5];
        this.ivcord = header[6];
        this.param = GempakUtil.ST_ITOC(new int[] { header[7], header[8], header[9] });
        this.param = this.param.trim();
        int ymd = times1[0];
        if (ymd / 10000 < 50) {
            ymd += 20000000;
        }
        else if (ymd / 10000 < 100) {
            ymd += 19000000;
        }
        final int hms = times1[1] * 100;
        this.refTime = new Date(McIDASUtil.mcDateHmsToSecs(ymd, hms) * 1000L);
        final int offset = times1[2] % 100000;
        if (offset == 0 || offset % 100 == 0) {
            this.validOffset = offset / 100 * 60;
        }
        else {
            this.validOffset = offset / 100 * 60 + offset % 100;
        }
        final Calendar calendar = Calendar.getInstance();
        calendar.setTimeZone(TimeZone.getTimeZone("GMT"));
        calendar.setTime(this.refTime);
        calendar.add(12, this.validOffset);
        this.validTime = calendar.getTime();
        final GempakParameter ggp = GempakGridParameterTable.getParameter(this.param);
        if (ggp != null) {
            this.decimalScale = ggp.getDecimalScale();
        }
    }
    
    public double getLevel1() {
        return this.level1;
    }
    
    public double getLevel2() {
        return this.level2;
    }
    
    public int getLevelType1() {
        return this.ivcord;
    }
    
    public int getLevelType2() {
        return this.ivcord;
    }
    
    public Date getReferenceTime() {
        return this.refTime;
    }
    
    public Date getValidTime() {
        return this.validTime;
    }
    
    public int getValidTimeOffset() {
        return this.validOffset;
    }
    
    public String getParameterName() {
        return this.param;
    }
    
    public String getParameterDescription() {
        return this.param;
    }
    
    public String getGridDefRecordId() {
        return this.navBlock.toString();
    }
    
    public int getGridNumber() {
        return this.gridNumber;
    }
    
    public int getDecimalScale() {
        return this.decimalScale;
    }
    
    public int getTimeUnit() {
        return 0;
    }
    
    public String getTimeUdunitName() {
        return "minutes";
    }
    
    public int cdmVariableHash() {
        return this.param.hashCode() + 37 * this.getLevelType1();
    }
    
    public String cdmVariableName(final GridTableLookup lookup, final boolean useLevel, final boolean useStat) {
        final Formatter f = new Formatter();
        f.format("%s", this.getParameterName());
        final String levelName = lookup.getLevelName((GridRecord)this);
        if (levelName.length() != 0) {
            if (lookup.isLayer((GridRecord)this)) {
                f.format("_%s_layer", lookup.getLevelName((GridRecord)this));
            }
            else {
                f.format("_%s", lookup.getLevelName((GridRecord)this));
            }
        }
        return f.toString();
    }
    
    @Override
    public String toString() {
        final StringBuffer buf = new StringBuffer();
        buf.append(StringUtil.padLeft(String.valueOf(this.gridNumber), 5));
        buf.append(StringUtil.padLeft(this.time1, 20));
        buf.append(" ");
        buf.append(StringUtil.padLeft(this.time2, 20));
        buf.append(" ");
        buf.append(StringUtil.padLeft(String.valueOf(this.level1), 5));
        if (this.level2 != -1) {
            buf.append(StringUtil.padLeft(String.valueOf(this.level2), 5));
        }
        else {
            buf.append("     ");
        }
        buf.append("  ");
        buf.append(StringUtil.padLeft(GempakUtil.LV_CCRD(this.ivcord), 6));
        buf.append(" ");
        buf.append(this.param);
        buf.append(" ");
        buf.append(GempakUtil.getGridPackingName(this.packingType));
        return buf.toString();
    }
}
