// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.iosp.gempak;

import ucar.unidata.util.StringUtil;

public final class GempakUtil
{
    private static int[] month;
    public static String[] vertCoords;
    
    public static int[] TG_FTOI(final int[] iftime, final int start) {
        final int[] intdtf = new int[3];
        if (iftime[start] < 100000000) {
            intdtf[0] = iftime[start];
            intdtf[1] = iftime[start + 1];
            intdtf[2] = 0;
        }
        else {
            intdtf[0] = iftime[start] / 10000;
            intdtf[1] = iftime[start] - intdtf[0] * 10000;
            final int mmdd = intdtf[0] / 100;
            final int iyyy = intdtf[0] - mmdd * 100;
            intdtf[0] = iyyy * 10000 + mmdd;
            intdtf[2] = iftime[start + 1];
        }
        return intdtf;
    }
    
    public static String TG_ITOC(final int[] intdtf) {
        String gdattim = "";
        if (intdtf[0] == 0 && intdtf[2] == 0 && intdtf[2] == 0) {
            return gdattim;
        }
        gdattim = TI_CDTM(intdtf[0], intdtf[1]);
        if (intdtf[2] != 0) {
            final String[] timeType = TG_CFTM(intdtf[2]);
            final String ftype = timeType[0];
            final String ftime = timeType[1];
            gdattim = gdattim.substring(0, 11) + ftype + ftime;
        }
        return gdattim;
    }
    
    public static String[] TG_CFTM(final int ifcast) {
        String ftype = "";
        String ftime = "";
        if (ifcast < 0) {
            return new String[] { ftype, ftime };
        }
        final int iftype = ifcast / 100000;
        if (iftype == 0) {
            ftype = "A";
        }
        else if (iftype == 1) {
            ftype = "F";
        }
        else if (iftype == 2) {
            ftype = "G";
        }
        else if (iftype == 3) {
            ftype = "I";
        }
        final int iftime = ifcast - iftype * 100000;
        final int ietime = iftime + 100000;
        final String fff = ST_INCH(ietime);
        if (ietime % 100 == 0) {
            ftime = fff.substring(1, 4);
        }
        else {
            ftime = fff.substring(1);
        }
        return new String[] { ftype, ftime };
    }
    
    public static String TI_CDTM(final int idate, final int itime) {
        final int[] idtarr = new int[5];
        idtarr[0] = idate / 10000;
        idtarr[1] = (idate - idtarr[0] * 10000) / 100;
        idtarr[2] = idate % 100;
        idtarr[3] = itime / 100;
        idtarr[4] = itime % 100;
        final String dattim = TI_ITOC(idtarr);
        return dattim;
    }
    
    public static String TI_ITOC(final int[] idtarr) {
        String dattim = "";
        int iyear = idtarr[0];
        final int imonth = idtarr[1];
        final int iday = idtarr[2];
        final int ihour = idtarr[3];
        final int iminut = idtarr[4];
        final int ndays = TI_DAYM(iyear, imonth);
        iyear %= 100;
        final int idate = iyear * 10000 + imonth * 100 + iday;
        final int itime = ihour * 100 + iminut;
        final String date = StringUtil.padZero(idate, 6);
        final String time = StringUtil.padZero(itime, 4);
        dattim = date + "/" + time;
        return dattim;
    }
    
    public static int TI_DAYM(final int iyear, final int imon) {
        int iday = 0;
        if (imon > 0 && imon < 13) {
            iday = GempakUtil.month[imon - 1];
            if (imon == 2 && LEAP(iyear)) {
                ++iday;
            }
        }
        return iday;
    }
    
    public static boolean LEAP(final int iyr) {
        return iyr % 4 == 0 && (iyr % 100 != 0 || iyr % 400 == 0);
    }
    
    public static String ST_INCH(final int value) {
        return String.valueOf(value);
    }
    
    public static String ST_ITOC(final int value) {
        final byte[] bval = { (byte)((value & 0xFF000000) >>> 24), (byte)((value & 0xFF0000) >>> 16), (byte)((value & 0xFF00) >>> 8), (byte)((value & 0xFF) >>> 0) };
        return new String(bval);
    }
    
    public static String ST_ITOC(final int[] values) {
        final StringBuilder sb = new StringBuilder();
        for (int i = 0; i < values.length; ++i) {
            sb.append(ST_ITOC(values[i]));
        }
        return sb.toString();
    }
    
    public static boolean ERMISS(final float value) {
        return Math.abs(value + 9999.0f) < 0.1f;
    }
    
    public static String LV_CCRD(final int ivcord) {
        String vcoord = "";
        if (ivcord >= 0 && ivcord < GempakUtil.vertCoords.length) {
            vcoord = GempakUtil.vertCoords[ivcord];
        }
        else if (ivcord > 100) {
            vcoord = ST_ITOC(ivcord);
        }
        return vcoord;
    }
    
    public static int swp4(final int value) {
        return Integer.reverseBytes(value);
    }
    
    public static int[] swp4(final int[] values, final int startIndex, final int number) {
        for (int i = startIndex; i < startIndex + number; ++i) {
            values[i] = Integer.reverseBytes(values[i]);
        }
        return values;
    }
    
    public static String getGridPackingName(final int pktyp) {
        String packingType = "UNKNOWN";
        switch (pktyp) {
            case 0: {
                packingType = "MDGNON";
                break;
            }
            case 1: {
                packingType = "MDGGRB";
                break;
            }
            case 2: {
                packingType = "MDGNMC";
                break;
            }
            case 3: {
                packingType = "MDGDIF";
                break;
            }
            case 4: {
                packingType = "MDGDEC";
                break;
            }
            case 5: {
                packingType = "MDGRB2";
                break;
            }
        }
        return packingType;
    }
    
    public static String getDataType(final int typrt) {
        String dataType = "" + typrt;
        switch (typrt) {
            case 1: {
                dataType = "MDREAL";
                break;
            }
            case 2: {
                dataType = "MDINTG";
                break;
            }
            case 3: {
                dataType = "MDCHAR";
                break;
            }
            case 4: {
                dataType = "MDRPCK";
                break;
            }
            case 5: {
                dataType = "MDGRID";
                break;
            }
        }
        return dataType;
    }
    
    static {
        GempakUtil.month = new int[] { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
        GempakUtil.vertCoords = new String[] { "NONE", "PRES", "THTA", "HGHT", "SGMA", "DPTH", "HYBL" };
    }
}
