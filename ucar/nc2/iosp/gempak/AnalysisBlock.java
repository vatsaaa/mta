// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.iosp.gempak;

public class AnalysisBlock
{
    float[] vals;
    
    public AnalysisBlock() {
        this.vals = null;
    }
    
    public AnalysisBlock(final float[] words) {
        this.vals = null;
        this.setValues(words);
    }
    
    public void setValues(final float[] values) {
        this.vals = values;
    }
    
    @Override
    public String toString() {
        final StringBuffer buf = new StringBuffer();
        if (this.vals != null) {}
        buf.append("\n\tUNKNOWN ANALYSIS TYPE");
        return buf.toString();
    }
}
