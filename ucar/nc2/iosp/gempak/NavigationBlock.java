// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.iosp.gempak;

import ucar.grid.GridDefRecord;

public class NavigationBlock extends GridDefRecord
{
    float[] vals;
    private String proj;
    
    public NavigationBlock() {
        this.vals = null;
    }
    
    public NavigationBlock(final float[] words) {
        this.vals = null;
        this.setValues(words);
    }
    
    public void setValues(final float[] values) {
        this.vals = values;
        this.addParam("ProjFlag", this.proj = GempakUtil.ST_ITOC(Float.floatToIntBits(this.vals[1])).trim());
        this.addParam("GDSkey", this.toString());
        this.setParams();
    }
    
    public String toString() {
        final StringBuffer buf = new StringBuffer();
        buf.append("\n    PROJECTION:         ");
        buf.append(this.proj);
        buf.append("\n    ANGLES:             ");
        buf.append(this.vals[10]);
        buf.append("  ");
        buf.append(this.vals[11]);
        buf.append("  ");
        buf.append(this.vals[12]);
        buf.append("\n    GRID SIZE:          ");
        buf.append(this.vals[4]);
        buf.append("  ");
        buf.append(this.vals[5]);
        buf.append("\n    LL CORNER:          ");
        buf.append(this.vals[6]);
        buf.append("  ");
        buf.append(this.vals[7]);
        buf.append("\n    UR CORNER:          ");
        buf.append(this.vals[8]);
        buf.append("  ");
        buf.append(this.vals[9]);
        return buf.toString();
    }
    
    private void setParams() {
        final String angle1 = String.valueOf(this.vals[10]);
        final String angle2 = String.valueOf(this.vals[11]);
        final String angle3 = String.valueOf(this.vals[12]);
        final String lllat = String.valueOf(this.vals[6]);
        final String lllon = String.valueOf(this.vals[7]);
        final String urlat = String.valueOf(this.vals[8]);
        final String urlon = String.valueOf(this.vals[9]);
        this.addParam("Nx", String.valueOf(this.vals[4]));
        this.addParam("Ny", String.valueOf(this.vals[5]));
        this.addParam("La1", lllat);
        this.addParam("Lo1", lllon);
        this.addParam("La2", urlat);
        this.addParam("Lo2", urlon);
        if (this.proj.equals("STR") || this.proj.equals("NPS") || this.proj.equals("SPS")) {
            this.addParam("LoV", angle2);
            if (this.proj.equals("SPS")) {
                this.addParam("NpProj", "false");
            }
        }
        else if (this.proj.equals("LCC") || this.proj.equals("SCC")) {
            this.addParam("Latin1", angle1);
            this.addParam("LoV", angle2);
            this.addParam("Latin2", angle3);
        }
        else if (this.proj.equals("MER") || this.proj.equals("MCD")) {
            String standardLat = "0";
            if (this.vals[10] == 0.0f) {
                final float lat = (this.vals[8] + this.vals[6]) / 2.0f;
                standardLat = String.valueOf(lat);
            }
            else {
                standardLat = angle1;
            }
            this.addParam("Latin", standardLat);
            this.addParam("LoV", angle2);
        }
        else if (this.proj.equals("CED")) {
            final double lllatv = this.vals[6];
            final double lllonv = this.vals[7];
            final double urlatv = this.vals[8];
            double urlonv = this.vals[9];
            if (urlonv < lllonv) {
                urlonv += 360.0;
            }
            final double dx = Math.abs((urlonv - lllonv) / (this.vals[4] - 1.0f));
            final double dy = Math.abs((urlatv - lllatv) / (this.vals[5] - 1.0f));
            this.addParam("Dx", String.valueOf(dx));
            this.addParam("Dy", String.valueOf(dy));
            this.addParam("Lo2", String.valueOf(urlonv));
        }
    }
    
    public String getGroupName() {
        final StringBuffer buf = new StringBuffer();
        buf.append(this.proj);
        buf.append("_");
        buf.append(this.getParam("Nx"));
        buf.append("x");
        buf.append(this.getParam("Ny"));
        return buf.toString();
    }
}
