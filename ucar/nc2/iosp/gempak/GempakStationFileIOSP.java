// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.iosp.gempak;

import java.io.OutputStream;
import java.io.PrintStream;
import java.io.ByteArrayOutputStream;
import java.util.StringTokenizer;
import ucar.ma2.ArrayDouble;
import ucar.ma2.Array;
import ucar.ma2.ArrayFloat;
import ucar.ma2.ArrayInt;
import ucar.ma2.ArrayChar;
import java.util.ArrayList;
import ucar.nc2.constants.CF;
import ucar.nc2.Attribute;
import ucar.ma2.DataType;
import ucar.nc2.Variable;
import java.util.Iterator;
import ucar.nc2.Group;
import ucar.nc2.Structure;
import java.util.List;
import ucar.nc2.util.CancelTask;
import java.io.IOException;
import visad.util.Trace;
import ucar.nc2.Dimension;
import ucar.nc2.units.DateFormatter;
import ucar.unidata.io.RandomAccessFile;
import ucar.nc2.NetcdfFile;
import ucar.nc2.iosp.AbstractIOServiceProvider;

public abstract class GempakStationFileIOSP extends AbstractIOServiceProvider
{
    protected NetcdfFile ncfile;
    protected RandomAccessFile raf;
    protected AbstractGempakStationFileReader gemreader;
    protected StringBuilder parseInfo;
    private DateFormatter dateFormat;
    protected static final Number RMISS;
    protected static final Number IMISS;
    protected static final Dimension DIM_LEN8;
    protected static final Dimension DIM_LEN4;
    protected static final Dimension DIM_LEN2;
    protected static final String TIME_VAR = "time";
    protected static final String MISSING_VAR = "_isMissing";
    private static String[] stnVarNames;
    private static int[] stnVarSizes;
    
    public GempakStationFileIOSP() {
        this.parseInfo = new StringBuilder();
        this.dateFormat = new DateFormatter();
    }
    
    public boolean isValidFile(final RandomAccessFile raf) throws IOException {
        try {
            this.gemreader = this.makeStationReader();
            Trace.call1("GEMPAKSIOSP.isValidFile: reader.init");
            this.gemreader.init(raf, false);
            Trace.call2("GEMPAKSIOSP.isValidFile: reader.init");
        }
        catch (Exception ioe) {
            return false;
        }
        return true;
    }
    
    protected abstract AbstractGempakStationFileReader makeStationReader();
    
    @Override
    public void open(final RandomAccessFile raf, final NetcdfFile ncfile, final CancelTask cancelTask) throws IOException {
        this.raf = raf;
        this.ncfile = ncfile;
        final long start = System.currentTimeMillis();
        if (this.gemreader == null) {
            this.gemreader = this.makeStationReader();
        }
        Trace.call1("GEMPAKStationIOSP.open: initTables");
        this.initTables();
        Trace.call2("GEMPAKStationIOSP.open: initTables");
        Trace.call1("GEMPAKStationIOSP.open: reader.init");
        this.gemreader.init(raf, true);
        Trace.call2("GEMPAKStationIOSP.open: reader.init");
        this.buildNCFile();
    }
    
    private void initTables() {
        try {
            GempakParameters.addParameters("resources/nj22/tables/gempak/params.tbl");
        }
        catch (Exception e) {
            System.out.println("unable to init param tables");
        }
    }
    
    @Override
    public void close() throws IOException {
        this.raf.close();
    }
    
    @Override
    public boolean syncExtend() {
        return false;
    }
    
    @Override
    public String getDetailInfo() {
        return this.parseInfo.toString();
    }
    
    @Override
    public boolean sync() throws IOException {
        if (this.gemreader.getInitFileSize() < this.raf.length()) {
            final long start = System.currentTimeMillis();
            Trace.msg("GEMPAKStationIOSP.sync: file " + this.raf.getLocation() + " is bigger: " + this.raf.length() + " > " + this.gemreader.getInitFileSize());
            Trace.call1("GEMPAKStationIOSP.sync: reader.init");
            this.gemreader.init(this.raf, true);
            Trace.call2("GEMPAKStationIOSP.sync: reader.init");
            Trace.call1("GEMPAKStationIOSP.sync: buildNCFile");
            this.buildNCFile();
            Trace.call2("GEMPAKSIOSP.sync: buildNCFile");
            return true;
        }
        return false;
    }
    
    protected void buildNCFile() throws IOException {
        Trace.call1("GEMPAKSIOSP: buildNCFile");
        this.ncfile.empty();
        this.fillNCFile();
        this.addGlobalAttributes();
        this.ncfile.finish();
        Trace.call2("GEMPAKSIOSP: buildNCFile");
    }
    
    protected abstract void fillNCFile() throws IOException;
    
    protected Structure makeStructure(final String partName, final List dimensions, final boolean includeMissing) {
        final List<GempakParameter> params = this.gemreader.getParameters(partName);
        if (params == null) {
            return null;
        }
        final Structure sVar = new Structure(this.ncfile, null, null, partName);
        sVar.setDimensions(dimensions);
        for (final GempakParameter param : params) {
            sVar.addMemberVariable(this.makeParamVariable(param, null));
        }
        if (includeMissing) {
            sVar.addMemberVariable(this.makeMissingVariable());
        }
        return sVar;
    }
    
    protected Variable makeMissingVariable() {
        final Variable var = new Variable(this.ncfile, null, null, "_isMissing");
        var.setDataType(DataType.BYTE);
        var.setDimensions((List<Dimension>)null);
        var.addAttribute(new Attribute("description", "missing flag - 1 means all params are missing"));
        var.addAttribute(new Attribute("missing_value", new Byte((byte)1)));
        return var;
    }
    
    protected Variable makeParamVariable(final GempakParameter param, final List<Dimension> dims) {
        final Variable var = new Variable(this.ncfile, null, null, param.getName());
        var.setDataType(DataType.FLOAT);
        var.setDimensions(dims);
        var.addAttribute(new Attribute("long_name", param.getDescription()));
        final String units = param.getUnit();
        if (units != null && !units.equals("")) {
            var.addAttribute(new Attribute("units", units));
        }
        var.addAttribute(new Attribute("missing_value", GempakStationFileIOSP.RMISS));
        return var;
    }
    
    protected void addGlobalAttributes() {
        this.ncfile.addAttribute(null, new Attribute("Conventions", this.getConventions()));
        final String fileType = "GEMPAK " + this.gemreader.getFileType();
        this.ncfile.addAttribute(null, new Attribute("file_format", fileType));
        this.ncfile.addAttribute(null, new Attribute("history", "Direct read of " + fileType + " into NetCDF-Java 4.1 API"));
        this.ncfile.addAttribute(null, new Attribute("CF:featureType", this.getCFFeatureType()));
    }
    
    public String getConventions() {
        return "GEMPAK/CDM";
    }
    
    public String getCFFeatureType() {
        return CF.FeatureType.point.toString();
    }
    
    protected int getStnVarSize(final String name) {
        int size = -1;
        for (int i = 0; i < GempakStationFileIOSP.stnVarNames.length; ++i) {
            if (name.equals(GempakStationFileIOSP.stnVarNames[i])) {
                size = GempakStationFileIOSP.stnVarSizes[i];
                break;
            }
        }
        return size;
    }
    
    protected List<Variable> makeStationVars(final List<GempakStation> stations, final Dimension dim) {
        final int numStations = stations.size();
        boolean useSTID = true;
        for (final GempakStation station : stations) {
            if (station.getSTID().equals("")) {
                useSTID = false;
                break;
            }
        }
        final List<Variable> vars = new ArrayList<Variable>();
        final List<String> stnKeyNames = this.gemreader.getStationKeyNames();
        for (final String varName : stnKeyNames) {
            final Variable v = this.makeStationVariable(varName, dim);
            final Attribute stIDAttr = new Attribute("standard_name", "station_id");
            if (varName.equals("STID") && useSTID) {
                v.addAttribute(stIDAttr);
            }
            if (varName.equals("STNM") && !useSTID) {
                v.addAttribute(stIDAttr);
            }
            vars.add(v);
        }
        if (dim != null && numStations > 0) {
            for (final Variable v2 : vars) {
                Array varArray;
                if (v2.getDataType().equals(DataType.CHAR)) {
                    final int[] shape = v2.getShape();
                    varArray = new ArrayChar.D2(shape[0], shape[1]);
                }
                else {
                    varArray = this.get1DArray(v2.getDataType(), numStations);
                }
                int index = 0;
                final String varname = v2.getName();
                for (final GempakStation stn : stations) {
                    String test = "";
                    if (varname.equals("STID")) {
                        test = stn.getName();
                    }
                    else if (varname.equals("STNM")) {
                        ((ArrayInt.D1)varArray).set(index, stn.getSTNM());
                    }
                    else if (varname.equals("SLAT")) {
                        ((ArrayFloat.D1)varArray).set(index, (float)stn.getLatitude());
                    }
                    else if (varname.equals("SLON")) {
                        ((ArrayFloat.D1)varArray).set(index, (float)stn.getLongitude());
                    }
                    else if (varname.equals("SELV")) {
                        ((ArrayFloat.D1)varArray).set(index, (float)stn.getAltitude());
                    }
                    else if (varname.equals("STAT")) {
                        test = stn.getSTAT();
                    }
                    else if (varname.equals("COUN")) {
                        test = stn.getCOUN();
                    }
                    else if (varname.equals("STD2")) {
                        test = stn.getSTD2();
                    }
                    else if (varname.equals("SPRI")) {
                        ((ArrayInt.D1)varArray).set(index, stn.getSPRI());
                    }
                    else if (varname.equals("SWFO")) {
                        test = stn.getSWFO();
                    }
                    else if (varname.equals("WFO2")) {
                        test = stn.getWFO2();
                    }
                    if (!test.equals("")) {
                        ((ArrayChar.D2)varArray).setString(index, test);
                    }
                    ++index;
                }
                v2.setCachedData(varArray, false);
            }
        }
        return vars;
    }
    
    private Array get1DArray(final DataType type, final int len) {
        Array varArray = null;
        if (type.equals(DataType.FLOAT)) {
            varArray = new ArrayFloat.D1(len);
        }
        else if (type.equals(DataType.DOUBLE)) {
            varArray = new ArrayDouble.D1(len);
        }
        else if (type.equals(DataType.INT)) {
            varArray = new ArrayInt.D1(len);
        }
        return varArray;
    }
    
    protected Variable makeStationVariable(final String varname, final Dimension firstDim) {
        String longName = varname;
        String unit = null;
        DataType type = DataType.CHAR;
        final List<Dimension> dims = new ArrayList<Dimension>();
        final List<Attribute> attrs = new ArrayList<Attribute>();
        if (firstDim != null) {
            dims.add(firstDim);
        }
        if (varname.equals("STID")) {
            longName = "Station identifier";
            dims.add(GempakStationFileIOSP.DIM_LEN8);
        }
        else if (varname.equals("STNM")) {
            longName = "WMO station id";
            type = DataType.INT;
        }
        else if (varname.equals("SLAT")) {
            longName = "latitude";
            unit = "degrees_north";
            type = DataType.FLOAT;
            attrs.add(new Attribute("standard_name", "latitude"));
        }
        else if (varname.equals("SLON")) {
            longName = "longitude";
            unit = "degrees_east";
            type = DataType.FLOAT;
            attrs.add(new Attribute("standard_name", "longitude"));
        }
        else if (varname.equals("SELV")) {
            longName = "altitude";
            unit = "meter";
            type = DataType.FLOAT;
            attrs.add(new Attribute("positive", "up"));
            attrs.add(new Attribute("standard_name", "station_altitude"));
        }
        else if (varname.equals("STAT")) {
            longName = "state or province";
            dims.add(GempakStationFileIOSP.DIM_LEN2);
        }
        else if (varname.equals("COUN")) {
            longName = "country code";
            dims.add(GempakStationFileIOSP.DIM_LEN2);
        }
        else if (varname.equals("STD2")) {
            longName = "Extended station id";
            dims.add(GempakStationFileIOSP.DIM_LEN4);
        }
        else if (varname.equals("SPRI")) {
            longName = "Station priority";
            type = DataType.INT;
        }
        else if (varname.equals("SWFO")) {
            longName = "WFO code";
            dims.add(GempakStationFileIOSP.DIM_LEN4);
        }
        else if (varname.equals("WFO2")) {
            longName = "Second WFO code";
            dims.add(GempakStationFileIOSP.DIM_LEN4);
        }
        final Variable v = new Variable(this.ncfile, null, null, varname);
        v.setDataType(type);
        v.addAttribute(new Attribute("long_name", longName));
        if (unit != null) {
            v.addAttribute(new Attribute("units", unit));
        }
        if (type.equals(DataType.FLOAT)) {
            v.addAttribute(new Attribute("missing_value", GempakStationFileIOSP.RMISS));
        }
        else if (type.equals(DataType.INT)) {
            v.addAttribute(new Attribute("missing_value", GempakStationFileIOSP.IMISS));
        }
        if (!attrs.isEmpty()) {
            for (final Attribute attr : attrs) {
                v.addAttribute(attr);
            }
        }
        if (!dims.isEmpty()) {
            v.setDimensions(dims);
        }
        else {
            v.setDimensions((String)null);
        }
        return v;
    }
    
    protected void printStack(final String msg, final int maxLines) {
        final String trace = this.getStackTrace();
        if (msg != null) {
            System.out.println(msg);
        }
        final StringTokenizer tok = new StringTokenizer(trace, "\n");
        int allcnt = 0;
        int cnt = 0;
        while (tok.hasMoreTokens()) {
            final String line = tok.nextToken();
            if (++allcnt > 4) {
                System.out.println(line);
                if (++cnt > maxLines) {
                    break;
                }
                continue;
            }
        }
    }
    
    protected String getStackTrace() {
        final ByteArrayOutputStream baos = new ByteArrayOutputStream();
        new IllegalArgumentException("").printStackTrace(new PrintStream(baos));
        return baos.toString();
    }
    
    static {
        RMISS = new Float(-9999.0f);
        IMISS = new Integer(-9999);
        DIM_LEN8 = new Dimension("len8", 8, true);
        DIM_LEN4 = new Dimension("len4", 4, true);
        DIM_LEN2 = new Dimension("len2", 2, true);
        GempakStationFileIOSP.stnVarNames = new String[] { "STID", "STNM", "SLAT", "SLON", "SELV", "STAT", "COUN", "STD2", "SPRI", "SWFO", "WFO2" };
        GempakStationFileIOSP.stnVarSizes = new int[] { 8, 4, 4, 4, 4, 2, 2, 4, 4, 4, 4 };
    }
}
