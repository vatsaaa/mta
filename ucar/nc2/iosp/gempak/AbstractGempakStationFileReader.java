// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.iosp.gempak;

import java.text.ParsePosition;
import java.util.TimeZone;
import java.util.Collection;
import java.util.SortedSet;
import java.util.Collections;
import java.util.TreeSet;
import java.util.ArrayList;
import java.util.Iterator;
import visad.util.Trace;
import java.io.IOException;
import java.util.HashMap;
import java.text.SimpleDateFormat;
import java.util.Map;
import java.util.Date;
import java.util.List;

public abstract class AbstractGempakStationFileReader extends GempakFileReader
{
    public static final String DATE = "DATE";
    public static final String TIME = "TIME";
    private static final String DATE_FORMAT = "yyMMdd'/'HHmm";
    private List<Key> dateTimeKeys;
    private List<Key> stationKeys;
    private List<String> dateList;
    private List<Date> dates;
    private List<GempakStation> stations;
    private List<GempakParameter> params;
    private Map<String, List<GempakParameter>> partParamMap;
    private SimpleDateFormat dateFmt;
    protected String subType;
    
    AbstractGempakStationFileReader() {
        this.partParamMap = new HashMap<String, List<GempakParameter>>();
        this.dateFmt = new SimpleDateFormat("yyMMdd'/'HHmm");
        this.subType = "";
    }
    
    @Override
    protected boolean init() throws IOException {
        return this.init(true);
    }
    
    @Override
    protected boolean init(final boolean fullCheck) throws IOException {
        if (!super.init(fullCheck)) {
            return false;
        }
        if (this.dmLabel.kftype != 2 && this.dmLabel.kftype != 1) {
            this.logError("not a point data file ");
            return false;
        }
        return true;
    }
    
    protected boolean readStationsAndTimes(final boolean uniqueTimes) {
        Trace.call1("GEMPAK: making params");
        for (final DMPart apart : this.parts) {
            final List<GempakParameter> params = this.makeParams(apart);
            this.partParamMap.put(apart.kprtnm, params);
        }
        Trace.call2("GEMPAK: making params");
        this.dateTimeKeys = this.getDateTimeKeys();
        if (this.dateTimeKeys == null || this.dateTimeKeys.isEmpty()) {
            return false;
        }
        this.stationKeys = this.findStationKeys();
        if (this.stationKeys == null || this.stationKeys.isEmpty()) {
            return false;
        }
        Trace.call1("GEMPAK: get station list");
        this.stations = this.getStationList();
        Trace.call2("GEMPAK: get station list");
        this.makeFileSubType();
        Trace.call1("GEMPAK: get date list");
        this.dates = null;
        this.dateList = this.makeDateList(uniqueTimes);
        Trace.call2("GEMPAK: get date list");
        return true;
    }
    
    private List<Key> getDateTimeKeys() {
        final Key date = this.findKey("DATE");
        final Key time = this.findKey("TIME");
        if (date == null || time == null || !date.type.equals(time.type)) {
            return null;
        }
        final List<Key> dt = new ArrayList<Key>(2);
        dt.add(date);
        dt.add(time);
        return dt;
    }
    
    protected List<String> makeDateList(final boolean unique) {
        final Key date = this.dateTimeKeys.get(0);
        final Key time = this.dateTimeKeys.get(1);
        List<int[]> toCheck;
        if (date.type.equals("ROW")) {
            toCheck = this.headers.rowHeaders;
        }
        else {
            toCheck = this.headers.colHeaders;
        }
        final List<String> fileDates = new ArrayList<String>();
        for (final int[] header : toCheck) {
            if (header[0] != -9999) {
                final int idate = header[date.loc + 1];
                final int itime = header[time.loc + 1];
                final String dateTime = GempakUtil.TI_CDTM(idate, itime);
                fileDates.add(dateTime);
            }
        }
        if (unique && !fileDates.isEmpty()) {
            final SortedSet<String> uniqueTimes = Collections.synchronizedSortedSet(new TreeSet<String>());
            uniqueTimes.addAll((Collection<?>)fileDates);
            fileDates.clear();
            fileDates.addAll(uniqueTimes);
        }
        return fileDates;
    }
    
    private List<GempakParameter> makeParams(final DMPart part) {
        final List<GempakParameter> gemparms = new ArrayList<GempakParameter>(part.kparms);
        for (final DMParam param : part.params) {
            final String name = param.kprmnm;
            GempakParameter parm = GempakParameters.getParameter(name);
            if (parm == null) {
                parm = new GempakParameter(1, name, name, "", 0);
            }
            gemparms.add(parm);
        }
        return gemparms;
    }
    
    public List<GempakParameter> getParameters(final String partName) {
        return this.partParamMap.get(partName);
    }
    
    private List<GempakStation> getStationList() {
        final Key slat = this.findKey("SLAT");
        if (slat == null) {
            return null;
        }
        List<int[]> toCheck;
        if (slat.type.equals("ROW")) {
            toCheck = this.headers.rowHeaders;
        }
        else {
            toCheck = this.headers.colHeaders;
        }
        final List<GempakStation> fileStations = new ArrayList<GempakStation>();
        int i = 0;
        for (final int[] header : toCheck) {
            if (header[0] != -9999) {
                final GempakStation station = this.makeStation(header);
                if (station != null) {
                    station.setIndex(i + 1);
                    fileStations.add(station);
                }
            }
            ++i;
        }
        return fileStations;
    }
    
    private GempakStation makeStation(final int[] header) {
        if (this.stationKeys == null || this.stationKeys.isEmpty()) {
            return null;
        }
        final GempakStation newStation = new GempakStation();
        for (final Key key : this.stationKeys) {
            final int loc = key.loc + 1;
            if (key.name.equals("STID")) {
                newStation.setSTID(GempakUtil.ST_ITOC(header[loc]).trim());
            }
            else if (key.name.equals("STNM")) {
                newStation.setSTNM(header[loc]);
            }
            else if (key.name.equals("SLAT")) {
                newStation.setSLAT(header[loc]);
            }
            else if (key.name.equals("SLON")) {
                newStation.setSLON(header[loc]);
            }
            else if (key.name.equals("SELV")) {
                newStation.setSELV(header[loc]);
            }
            else if (key.name.equals("SPRI")) {
                newStation.setSPRI(header[loc]);
            }
            else if (key.name.equals("STAT")) {
                newStation.setSTAT(GempakUtil.ST_ITOC(header[loc]).trim());
            }
            else if (key.name.equals("COUN")) {
                newStation.setCOUN(GempakUtil.ST_ITOC(header[loc]).trim());
            }
            else if (key.name.equals("SWFO")) {
                newStation.setSWFO(GempakUtil.ST_ITOC(header[loc]).trim());
            }
            else if (key.name.equals("WFO2")) {
                newStation.setWFO2(GempakUtil.ST_ITOC(header[loc]).trim());
            }
            else {
                if (!key.name.equals("STD2")) {
                    continue;
                }
                newStation.setSTD2(GempakUtil.ST_ITOC(header[loc]).trim());
            }
        }
        return newStation;
    }
    
    public List<String> getStationKeyNames() {
        final List<String> keys = new ArrayList<String>();
        if (this.stationKeys != null && !this.stationKeys.isEmpty()) {
            for (final Key key : this.stationKeys) {
                keys.add(key.name);
            }
        }
        return keys;
    }
    
    private List<Key> findStationKeys() {
        final Key stid = this.findKey("STID");
        final Key stnm = this.findKey("STNM");
        final Key slat = this.findKey("SLAT");
        final Key slon = this.findKey("SLON");
        final Key selv = this.findKey("SELV");
        final Key stat = this.findKey("STAT");
        final Key coun = this.findKey("COUN");
        final Key std2 = this.findKey("STD2");
        final Key spri = this.findKey("SPRI");
        final Key swfo = this.findKey("SWFO");
        final Key wfo2 = this.findKey("WFO2");
        if (slat == null || slon == null || !slat.type.equals(slon.type)) {
            return null;
        }
        final String tslat = slat.type;
        final List<Key> stKeys = new ArrayList<Key>();
        stKeys.add(slat);
        stKeys.add(slon);
        if (stid != null && !stid.type.equals(tslat)) {
            return null;
        }
        if (stnm != null && !stnm.type.equals(tslat)) {
            return null;
        }
        if (selv != null && !selv.type.equals(tslat)) {
            return null;
        }
        if (stat != null && !stat.type.equals(tslat)) {
            return null;
        }
        if (coun != null && !coun.type.equals(tslat)) {
            return null;
        }
        if (std2 != null && !std2.type.equals(tslat)) {
            return null;
        }
        if (spri != null && !spri.type.equals(tslat)) {
            return null;
        }
        if (swfo != null && !swfo.type.equals(tslat)) {
            return null;
        }
        if (wfo2 != null && !wfo2.type.equals(tslat)) {
            return null;
        }
        if (stid != null) {
            stKeys.add(stid);
        }
        if (stnm != null) {
            stKeys.add(stnm);
        }
        if (selv != null) {
            stKeys.add(selv);
        }
        if (stat != null) {
            stKeys.add(stat);
        }
        if (coun != null) {
            stKeys.add(coun);
        }
        if (std2 != null) {
            stKeys.add(std2);
        }
        if (spri != null) {
            stKeys.add(spri);
        }
        if (swfo != null) {
            stKeys.add(swfo);
        }
        if (wfo2 != null) {
            stKeys.add(wfo2);
        }
        return stKeys;
    }
    
    public List<GempakStation> getStations() {
        return this.stations;
    }
    
    public List<Date> getDates() {
        if ((this.dates == null || this.dates.isEmpty()) && !this.dateList.isEmpty()) {
            this.dates = new ArrayList<Date>(this.dateList.size());
            this.dateFmt.setTimeZone(TimeZone.getTimeZone("GMT"));
            for (final String dateString : this.dateList) {
                final Date d = this.dateFmt.parse(dateString, new ParsePosition(0));
                this.dates.add(d);
            }
        }
        return this.dates;
    }
    
    protected String getDateString(final int index) {
        return this.dateList.get(index);
    }
    
    public void printDates() {
        final StringBuilder builder = new StringBuilder();
        builder.append("\nDates:\n");
        for (final String date : this.dateList) {
            builder.append("\t");
            builder.append(date);
            builder.append("\n");
        }
        System.out.println(builder.toString());
    }
    
    public void printStations(final boolean list) {
        final StringBuilder builder = new StringBuilder();
        builder.append("\nStations:\n");
        if (list) {
            for (final GempakStation station : this.getStations()) {
                builder.append(station);
                builder.append("\n");
            }
        }
        else {
            builder.append("\t");
            builder.append(this.getStations().size());
        }
        System.out.println(builder.toString());
    }
    
    public int findStationIndex(final String id) {
        for (final GempakStation station : this.getStations()) {
            if (station.getSTID().equals(id)) {
                return station.getIndex();
            }
        }
        return -1;
    }
    
    public String getFileType() {
        String type = "Unknown";
        switch (this.dmLabel.kftype) {
            case 2: {
                type = "Sounding";
                break;
            }
            case 1: {
                type = "Surface";
                break;
            }
        }
        if (!this.subType.equals("")) {
            type = type + " (" + this.subType + ")";
        }
        return type;
    }
    
    protected abstract void makeFileSubType();
    
    protected String getFileSubType() {
        return this.subType;
    }
}
