// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.iosp.gempak;

import java.util.Date;
import java.io.FileNotFoundException;
import ucar.nc2.FileWriter;
import ucar.nc2.iosp.IOServiceProvider;
import java.util.Calendar;
import ucar.grid.GridRecord;
import ucar.grid.GridTableLookup;
import ucar.nc2.iosp.grid.GridIndexToNC;
import ucar.grid.GridIndex;
import ucar.nc2.util.CancelTask;
import ucar.nc2.NetcdfFile;
import java.io.IOException;
import ucar.unidata.io.RandomAccessFile;
import ucar.nc2.iosp.grid.GridServiceProvider;

public class GempakGridServiceProvider extends GridServiceProvider
{
    protected GempakGridReader gemreader;
    public static boolean extendIndex;
    
    public boolean isValidFile(final RandomAccessFile raf) throws IOException {
        try {
            (this.gemreader = new GempakGridReader(raf.getLocation())).init(raf, false);
        }
        catch (Exception ioe) {
            return false;
        }
        return true;
    }
    
    public String getFileTypeId() {
        return "GempakGrid";
    }
    
    public String getFileTypeDescription() {
        return "GEMPAK Gridded Data";
    }
    
    @Override
    public void open(final RandomAccessFile raf, final NetcdfFile ncfile, final CancelTask cancelTask) throws IOException {
        super.open(raf, ncfile, cancelTask);
        final long start = System.currentTimeMillis();
        if (this.gemreader == null) {
            this.gemreader = new GempakGridReader(raf.getLocation());
        }
        this.initTables();
        this.gemreader.init(raf, true);
        final GridIndex index = this.gemreader.getGridIndex();
        this.open(index, cancelTask);
        if (GempakGridServiceProvider.debugOpen) {
            System.out.println(" GridServiceProvider.open " + ncfile.getLocation() + " took " + (System.currentTimeMillis() - start));
        }
    }
    
    @Override
    protected void open(final GridIndex index, final CancelTask cancelTask) throws IOException {
        final GempakLookup lookup = new GempakLookup(index.getGridRecords().get(0));
        final GridIndexToNC delegate = new GridIndexToNC(index.filename);
        delegate.open(index, (GridTableLookup)lookup, 4, this.ncfile, this.fmrcCoordSys, cancelTask);
        this.ncfile.finish();
    }
    
    @Override
    public boolean sync() throws IOException {
        if (this.gemreader.getInitFileSize() < this.raf.length() && GempakGridServiceProvider.extendIndex) {
            this.gemreader.init(true);
            final GridIndex index = this.gemreader.getGridIndex();
            this.ncfile.empty();
            this.open(index, null);
            return true;
        }
        return false;
    }
    
    @Override
    protected float[] _readData(final GridRecord gr) throws IOException {
        return this.gemreader.readGrid(gr);
    }
    
    public static void main(final String[] args) throws IOException {
        final GempakGridServiceProvider ggsp = new GempakGridServiceProvider();
        final String className = ggsp.getClass().getName();
        if (args.length < 1) {
            System.out.println("\nUsage of " + className + ":\n");
            System.out.println("Parameters:");
            System.out.println("\t<GEMPAK Grid File> GEMPAK grid file to read");
            System.out.println("\t<NetCDF output file> file to store results (optional)\n");
            System.out.println("java -Xmx256m " + className + " <GEMPAK Grid File> <NetCDF output file>");
            System.exit(0);
        }
        Date now = Calendar.getInstance().getTime();
        System.out.println(now.toString() + " ... Start of " + className);
        try {
            System.out.println("reading GEMPAK grid file=" + args[0]);
            final RandomAccessFile rf = new RandomAccessFile(args[0], "r", 2048);
            final NetcdfFile ncfile = new MakeNetcdfFile(ggsp, rf, args[0], null);
            if (args.length == 2) {
                System.out.print("writing to netCDF file=" + args[1]);
                final NetcdfFile nc = FileWriter.writeToFile(ncfile, args[1]);
                nc.close();
            }
            rf.close();
        }
        catch (FileNotFoundException noFileError) {
            System.err.println("FileNotFoundException : " + noFileError);
        }
        catch (IOException ioError) {
            System.err.println("IOException : " + ioError);
        }
        now = Calendar.getInstance().getTime();
        System.out.println(now.toString() + " ... End of " + className + "!");
    }
    
    private void initTables() {
        try {
            GempakGridParameterTable.addParameters("resources/nj22/tables/gempak/wmogrib3.tbl");
            GempakGridParameterTable.addParameters("resources/nj22/tables/gempak/ncepgrib2.tbl");
        }
        catch (Exception e) {
            System.out.println("unable to init tables");
        }
    }
    
    public static void setExtendIndex(final boolean b) {
        GempakGridServiceProvider.extendIndex = b;
    }
    
    static {
        GempakGridServiceProvider.extendIndex = true;
    }
    
    protected static class MakeNetcdfFile extends NetcdfFile
    {
        MakeNetcdfFile(final IOServiceProvider spi, final RandomAccessFile raf, final String location, final CancelTask cancelTask) throws IOException {
            super(spi, raf, location, cancelTask);
        }
    }
}
