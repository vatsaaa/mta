// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.iosp.gempak;

import java.io.IOException;

public class GempakParameters
{
    private static GempakParameterTable paramTable;
    
    public static void addParameters(final String tbl) throws IOException {
        GempakParameters.paramTable.addParameters(tbl);
    }
    
    public static GempakParameter getParameter(final String name) {
        return GempakParameters.paramTable.getParameter(name);
    }
    
    static {
        GempakParameters.paramTable = new GempakParameterTable();
    }
}
