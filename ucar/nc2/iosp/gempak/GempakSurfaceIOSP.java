// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.iosp.gempak;

import java.util.ArrayList;
import ucar.ma2.ArrayDouble;
import ucar.nc2.Group;
import ucar.nc2.Dimension;
import visad.util.Trace;
import ucar.nc2.NetcdfFile;
import ucar.nc2.iosp.IOServiceProvider;
import ucar.nc2.FileWriter;
import ucar.nc2.util.CancelTask;
import java.util.Date;
import ucar.unidata.util.StringUtil;
import ucar.ma2.Range;
import ucar.nc2.Attribute;
import java.util.Iterator;
import java.util.List;
import ucar.ma2.DataType;
import ucar.ma2.ArrayStructureBB;
import java.nio.ByteBuffer;
import ucar.ma2.StructureMembers;
import ucar.nc2.Structure;
import ucar.ma2.InvalidRangeException;
import ucar.ma2.Array;
import ucar.ma2.Section;
import ucar.nc2.Variable;
import ucar.nc2.constants.CF;
import java.io.IOException;
import ucar.unidata.io.RandomAccessFile;

public class GempakSurfaceIOSP extends GempakStationFileIOSP
{
    @Override
    protected AbstractGempakStationFileReader makeStationReader() {
        return new GempakSurfaceFileReader();
    }
    
    @Override
    public boolean isValidFile(final RandomAccessFile raf) throws IOException {
        return super.isValidFile(raf) && (this.gemreader.getFileSubType().equals("standard") || this.gemreader.getFileSubType().equals("ship"));
    }
    
    public String getFileTypeId() {
        return "GempakSurface";
    }
    
    public String getFileTypeDescription() {
        return "GEMPAK Surface Obs Data";
    }
    
    @Override
    public String getCFFeatureType() {
        if (this.gemreader.getFileSubType().equals("ship")) {
            return CF.FeatureType.point.toString();
        }
        return CF.FeatureType.timeSeries.toString();
    }
    
    public Array readData(final Variable v2, final Section section) throws IOException, InvalidRangeException {
        if (this.gemreader == null) {
            return null;
        }
        Array array = null;
        if (this.gemreader.getFileSubType().equals("ship")) {
            array = this.readShipData(v2, section);
        }
        else if (this.gemreader.getFileSubType().equals("standard")) {
            array = this.readStandardData(v2, section);
        }
        return array;
    }
    
    private Array readStandardData(final Variable v2, final Section section) throws IOException {
        Array array = null;
        if (v2 instanceof Structure) {
            final List<GempakParameter> params = this.gemreader.getParameters("SFDT");
            final Structure pdata = (Structure)v2;
            final StructureMembers members = pdata.makeStructureMembers();
            final List<StructureMembers.Member> mbers = members.getMembers();
            int i = 0;
            int numBytes = 0;
            int totalNumBytes = 0;
            for (final StructureMembers.Member member : mbers) {
                member.setDataParam(4 * i++);
                numBytes = member.getDataType().getSize();
                totalNumBytes += numBytes;
            }
            members.setStructureSize(totalNumBytes);
            final float[] missing = new float[mbers.size()];
            int missnum = 0;
            for (final Variable v3 : pdata.getVariables()) {
                final Attribute att = v3.findAttribute("missing_value");
                missing[missnum++] = ((att == null) ? -9999.0f : att.getNumericValue().floatValue());
            }
            final int num = 0;
            final Range stationRange = section.getRange(0);
            final Range timeRange = section.getRange(1);
            final int size = stationRange.length() * timeRange.length();
            final byte[] bytes = new byte[totalNumBytes * size];
            final ByteBuffer buf = ByteBuffer.wrap(bytes);
            array = new ArrayStructureBB(members, new int[] { size }, buf, 0);
            for (int y = stationRange.first(); y <= stationRange.last(); y += stationRange.stride()) {
                for (int x = timeRange.first(); x <= timeRange.last(); x += timeRange.stride()) {
                    final GempakFileReader.RData vals = this.gemreader.DM_RDTR(x + 1, y + 1, "SFDT");
                    if (vals == null) {
                        int k = 0;
                        for (final StructureMembers.Member member2 : mbers) {
                            if (member2.getDataType().equals(DataType.FLOAT)) {
                                buf.putFloat(missing[k]);
                            }
                            else {
                                buf.put((byte)1);
                            }
                            ++k;
                        }
                    }
                    else {
                        final float[] reals = vals.data;
                        int var = 0;
                        for (final GempakParameter param : params) {
                            if (members.findMember(param.getName()) != null) {
                                buf.putFloat(reals[var]);
                            }
                            ++var;
                        }
                        buf.put((byte)0);
                    }
                }
            }
        }
        return array;
    }
    
    private Array readShipData(final Variable v2, final Section section) throws IOException {
        Array array = null;
        if (v2 instanceof Structure) {
            final List<GempakParameter> params = this.gemreader.getParameters("SFDT");
            final Structure pdata = (Structure)v2;
            final StructureMembers members = pdata.makeStructureMembers();
            final List<StructureMembers.Member> mbers = members.getMembers();
            int ssize = 0;
            final int stnVarNum = 0;
            final List<String> stnKeyNames = this.gemreader.getStationKeyNames();
            for (final StructureMembers.Member member : mbers) {
                if (stnKeyNames.contains(member.getName())) {
                    final int varSize = this.getStnVarSize(member.getName());
                    member.setDataParam(ssize);
                    ssize += varSize;
                }
                else if (member.getName().equals("time")) {
                    member.setDataParam(ssize);
                    ssize += 8;
                }
                else if (member.getName().equals("_isMissing")) {
                    member.setDataParam(ssize);
                    ++ssize;
                }
                else {
                    member.setDataParam(ssize);
                    ssize += 4;
                }
            }
            members.setStructureSize(ssize);
            final Range recordRange = section.getRange(0);
            final int size = recordRange.length();
            final byte[] bytes = new byte[ssize * size];
            final ByteBuffer buf = ByteBuffer.wrap(bytes);
            array = new ArrayStructureBB(members, new int[] { size }, buf, 0);
            final List<GempakStation> stationList = this.gemreader.getStations();
            final List<Date> dateList = this.gemreader.getDates();
            boolean needToReadData = !pdata.isSubset();
            if (!needToReadData) {
                for (final GempakParameter param : params) {
                    if (members.findMember(param.getName()) != null) {
                        needToReadData = true;
                        break;
                    }
                }
            }
            final boolean hasTime = members.findMember("time") != null;
            for (int x = recordRange.first(); x <= recordRange.last(); x += recordRange.stride()) {
                final GempakStation stn = stationList.get(x);
                for (final String varname : stnKeyNames) {
                    if (members.findMember(varname) == null) {
                        continue;
                    }
                    String temp = null;
                    if (varname.equals("STID")) {
                        temp = StringUtil.padRight(stn.getName(), 8);
                    }
                    else if (varname.equals("STNM")) {
                        buf.putInt(stn.getSTNM());
                    }
                    else if (varname.equals("SLAT")) {
                        buf.putFloat((float)stn.getLatitude());
                    }
                    else if (varname.equals("SLON")) {
                        buf.putFloat((float)stn.getLongitude());
                    }
                    else if (varname.equals("SELV")) {
                        buf.putFloat((float)stn.getAltitude());
                    }
                    else if (varname.equals("STAT")) {
                        temp = StringUtil.padRight(stn.getSTAT(), 2);
                    }
                    else if (varname.equals("COUN")) {
                        temp = StringUtil.padRight(stn.getCOUN(), 2);
                    }
                    else if (varname.equals("STD2")) {
                        temp = StringUtil.padRight(stn.getSTD2(), 4);
                    }
                    else if (varname.equals("SPRI")) {
                        buf.putInt(stn.getSPRI());
                    }
                    else if (varname.equals("SWFO")) {
                        temp = StringUtil.padRight(stn.getSWFO(), 4);
                    }
                    else if (varname.equals("WFO2")) {
                        temp = StringUtil.padRight(stn.getWFO2(), 4);
                    }
                    if (temp == null) {
                        continue;
                    }
                    buf.put(temp.getBytes());
                }
                if (members.findMember("time") != null) {
                    final Date time = dateList.get(x);
                    buf.putDouble(time.getTime() / 1000.0);
                }
                if (needToReadData) {
                    final int column = stn.getIndex();
                    final GempakFileReader.RData vals = this.gemreader.DM_RDTR(1, column, "SFDT");
                    if (vals == null) {
                        for (final GempakParameter param2 : params) {
                            if (members.findMember(param2.getName()) != null) {
                                buf.putFloat(-9999.0f);
                            }
                        }
                        buf.put((byte)1);
                    }
                    else {
                        final float[] reals = vals.data;
                        int var = 0;
                        for (final GempakParameter param3 : params) {
                            if (members.findMember(param3.getName()) != null) {
                                buf.putFloat(reals[var]);
                            }
                            ++var;
                        }
                        buf.put((byte)0);
                    }
                }
            }
        }
        return array;
    }
    
    public static void main(final String[] args) throws IOException {
        final IOServiceProvider mciosp = new GempakSurfaceIOSP();
        final RandomAccessFile rf = new RandomAccessFile(args[0], "r", 2048);
        final NetcdfFile ncfile = new MakeNetcdfFile(mciosp, rf, args[0], null);
        if (args.length > 1) {
            FileWriter.writeToFile(ncfile, args[1]);
        }
        else {
            System.out.println(ncfile);
        }
    }
    
    @Override
    protected void fillNCFile() throws IOException {
        final String fileType = this.gemreader.getFileSubType();
        if (fileType.equals("standard")) {
            this.buildStandardFile();
        }
        else if (fileType.equals("ship")) {
            this.buildShipFile();
        }
        else {
            this.buildClimateFile();
        }
    }
    
    private void buildStandardFile() {
        final List<GempakStation> stations = this.gemreader.getStations();
        Trace.msg("GEMPAKSIOSP: now have " + stations.size() + " stations");
        final Dimension station = new Dimension("station", stations.size(), true);
        this.ncfile.addDimension(null, station);
        this.ncfile.addDimension(null, GempakSurfaceIOSP.DIM_LEN8);
        this.ncfile.addDimension(null, GempakSurfaceIOSP.DIM_LEN4);
        this.ncfile.addDimension(null, GempakSurfaceIOSP.DIM_LEN2);
        final List<Variable> stationVars = this.makeStationVars(stations, station);
        for (final Variable stnVar : stationVars) {
            this.ncfile.addVariable(null, stnVar);
        }
        final List<Date> timeList = this.gemreader.getDates();
        final int numTimes = timeList.size();
        final Dimension times = new Dimension("time", numTimes, true);
        this.ncfile.addDimension(null, times);
        Array varArray = null;
        final Variable timeVar = new Variable(this.ncfile, null, null, "time", DataType.DOUBLE, "time");
        timeVar.addAttribute(new Attribute("units", "seconds since 1970-01-01 00:00:00"));
        timeVar.addAttribute(new Attribute("long_name", "time"));
        varArray = new ArrayDouble.D1(numTimes);
        int i = 0;
        for (final Date date : timeList) {
            ((ArrayDouble.D1)varArray).set(i, date.getTime() / 1000.0);
            ++i;
        }
        timeVar.setCachedData(varArray, false);
        this.ncfile.addVariable(null, timeVar);
        final List<Dimension> stationTime = new ArrayList<Dimension>();
        stationTime.add(station);
        stationTime.add(times);
        final Structure sfData = this.makeStructure("SFDT", stationTime, true);
        if (sfData == null) {
            return;
        }
        sfData.addAttribute(new Attribute("coordinates", "time SLAT SLON SELV"));
        this.ncfile.addVariable(null, sfData);
        this.ncfile.addAttribute(null, new Attribute("CF:featureType", CF.FeatureType.timeSeries.toString()));
    }
    
    private void buildShipFile() {
        final List<GempakStation> stations = this.gemreader.getStations();
        final int numObs = stations.size();
        Trace.msg("GEMPAKSIOSP: now have " + numObs + " stations");
        final Dimension record = new Dimension("record", numObs, true, numObs == 0, false);
        this.ncfile.addDimension(null, record);
        final List<Dimension> records = new ArrayList<Dimension>(1);
        records.add(record);
        final Variable timeVar = new Variable(this.ncfile, null, null, "time", DataType.DOUBLE, null);
        timeVar.addAttribute(new Attribute("units", "seconds since 1970-01-01 00:00:00"));
        timeVar.addAttribute(new Attribute("long_name", "time"));
        this.ncfile.addDimension(null, GempakSurfaceIOSP.DIM_LEN8);
        this.ncfile.addDimension(null, GempakSurfaceIOSP.DIM_LEN4);
        this.ncfile.addDimension(null, GempakSurfaceIOSP.DIM_LEN2);
        final List<Variable> stationVars = this.makeStationVars(stations, null);
        final List<GempakParameter> params = this.gemreader.getParameters("SFDT");
        if (params == null) {
            return;
        }
        final Structure sVar = new Structure(this.ncfile, null, null, "Obs");
        sVar.setDimensions(records);
        boolean hasElevation = false;
        for (final Variable stnVar : stationVars) {
            if (stnVar.getName().equals("SELV")) {
                hasElevation = true;
            }
            sVar.addMemberVariable(stnVar);
        }
        sVar.addMemberVariable(timeVar);
        for (final GempakParameter param : params) {
            final Variable var = this.makeParamVariable(param, null);
            sVar.addMemberVariable(var);
        }
        sVar.addMemberVariable(this.makeMissingVariable());
        String coords = "Obs.time Obs.SLAT Obs.SLON";
        if (hasElevation) {
            coords += " Obs.SELV";
        }
        sVar.addAttribute(new Attribute("coordinates", coords));
        this.ncfile.addVariable(null, sVar);
        this.ncfile.addAttribute(null, new Attribute("CF:featureType", CF.FeatureType.point.toString()));
    }
    
    private void buildClimateFile() {
    }
    
    protected static class MakeNetcdfFile extends NetcdfFile
    {
        MakeNetcdfFile(final IOServiceProvider spi, final RandomAccessFile raf, final String location, final CancelTask cancelTask) throws IOException {
            super(spi, raf, location, cancelTask);
        }
    }
}
