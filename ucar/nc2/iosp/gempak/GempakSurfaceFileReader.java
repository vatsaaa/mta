// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.iosp.gempak;

import java.util.Iterator;
import ucar.unidata.util.Format;
import ucar.unidata.util.StringUtil;
import java.util.List;
import java.io.IOException;
import ucar.unidata.io.RandomAccessFile;

public class GempakSurfaceFileReader extends AbstractGempakStationFileReader
{
    public static final String SFTX = "SFTX";
    public static final String SFDT = "SFDT";
    public static final String SFSP = "SFSP";
    public static final String STANDARD = "standard";
    public static final String CLIMATE = "climate";
    public static final String SHIP = "ship";
    
    GempakSurfaceFileReader() {
    }
    
    public static GempakSurfaceFileReader getInstance(final RandomAccessFile raf, final boolean fullCheck) throws IOException {
        final GempakSurfaceFileReader gsfr = new GempakSurfaceFileReader();
        gsfr.init(raf, fullCheck);
        return gsfr;
    }
    
    @Override
    protected boolean init() throws IOException {
        return this.init(true);
    }
    
    @Override
    protected boolean init(final boolean fullCheck) throws IOException {
        if (!super.init(fullCheck)) {
            return false;
        }
        if (this.dmLabel.kftype != 1) {
            this.logError("not a surface data file ");
            return false;
        }
        int numParams = 0;
        final String partType = (this.dmLabel.kfsrce == 100 && this.dmLabel.kprt == 1) ? "SFTX" : "SFDT";
        final DMPart part = this.getPart(partType);
        if (part == null) {
            this.logError("No part named " + partType + " found");
            return false;
        }
        numParams = part.kparms;
        if (!this.readStationsAndTimes(true)) {
            this.logError("Unable to read stations and times");
            return false;
        }
        if (this.subType.equals("standard")) {
            this.rf.setBufferSize(256);
        }
        return true;
    }
    
    @Override
    protected List<String> makeDateList(final boolean uniqueTimes) {
        return super.makeDateList(!this.getFileSubType().equals("ship"));
    }
    
    @Override
    protected void makeFileSubType() {
        final String latType = this.findKey("SLAT").type;
        if (!this.findKey("DATE").type.equals(latType)) {
            if (latType.equals("ROW")) {
                this.subType = "climate";
            }
            else {
                this.subType = "standard";
            }
        }
        else {
            this.subType = "ship";
        }
    }
    
    public void printOb(final int row, final int col) {
        final int stnIndex = this.getFileSubType().equals("climate") ? row : col;
        final List<GempakStation> stations = this.getStations();
        if (stations.isEmpty() || stnIndex > stations.size()) {
            System.out.println("\nNo data available");
            return;
        }
        final GempakStation station = this.getStations().get(stnIndex - 1);
        final StringBuilder builder = new StringBuilder();
        builder.append("\nStation:\n");
        builder.append(station.toString());
        builder.append("\nObs\n\t");
        final List<GempakParameter> params = this.getParameters("SFDT");
        for (final GempakParameter parm : params) {
            builder.append(StringUtil.padLeft(parm.getName(), 7));
            builder.append("\t");
        }
        builder.append("\n");
        RData rd = null;
        try {
            rd = this.DM_RDTR(row, col, "SFDT");
        }
        catch (IOException ioe) {
            ioe.printStackTrace();
            rd = null;
        }
        if (rd == null) {
            builder.append("No Data Available");
        }
        else {
            builder.append("\t");
            final float[] data = rd.data;
            for (int i = 0; i < data.length; ++i) {
                builder.append(StringUtil.padLeft(Format.formatDouble(data[i], 7, 1), 7));
                builder.append("\t");
            }
            final int[] header = rd.header;
            if (header.length > 0) {
                builder.append("\nOb Time = ");
                builder.append(header[0]);
            }
        }
        System.out.println(builder.toString());
    }
    
    public String getSurfaceFileType() {
        return this.getFileSubType();
    }
    
    public static void main(final String[] args) throws IOException {
        if (args.length == 0) {
            System.out.println("need to supply a GEMPAK surface file name");
            System.exit(1);
        }
        try {
            GempakParameters.addParameters("resources/nj22/tables/gempak/params.tbl");
        }
        catch (Exception e) {
            System.out.println("unable to init param tables");
        }
        final GempakSurfaceFileReader gsfr = getInstance(GempakFileReader.getFile(args[0]), true);
        System.out.println("Type = " + gsfr.getSurfaceFileType());
        gsfr.printFileLabel();
        gsfr.printKeys();
        gsfr.printHeaders();
        gsfr.printParts();
        int row = 1;
        int col = 1;
        if (args.length > 1) {
            row = Integer.parseInt(args[1]);
        }
        if (args.length > 2) {
            col = Integer.parseInt(args[2]);
        }
        gsfr.printOb(row, col);
    }
}
