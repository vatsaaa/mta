// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.iosp.hdf4;

import ucar.unidata.util.Format;
import java.util.Collections;
import java.nio.ByteBuffer;
import ucar.ma2.StructureMembers;
import ucar.nc2.util.CancelTask;
import ucar.nc2.NCdumpW;
import ucar.ma2.ArrayStructure;
import org.slf4j.LoggerFactory;
import java.io.File;
import ucar.ma2.InvalidRangeException;
import ucar.nc2.Structure;
import ucar.ma2.Array;
import ucar.ma2.DataType;
import java.util.Set;
import ucar.nc2.Dimension;
import java.util.Formatter;
import java.util.Iterator;
import ucar.nc2.Group;
import ucar.nc2.Attribute;
import ucar.nc2.Variable;
import java.util.ArrayList;
import ucar.nc2.util.DebugFlags;
import ucar.unidata.util.StringUtil;
import java.io.IOException;
import java.util.HashMap;
import java.io.PrintStream;
import java.util.Map;
import java.util.List;
import ucar.unidata.io.RandomAccessFile;
import ucar.nc2.NetcdfFile;
import org.slf4j.Logger;

public class H4header
{
    private static Logger log;
    private static final byte[] head;
    private static final String shead;
    private static final long maxHeaderPos = 500000L;
    private static boolean debugDD;
    private static boolean debugTag1;
    private static boolean debugTag2;
    private static boolean debugTagDetail;
    private static boolean debugConstruct;
    private static boolean debugAtt;
    private static boolean debugLinked;
    private static boolean debugChunkTable;
    private static boolean debugChunkDetail;
    private static boolean debugTracker;
    private static boolean warnings;
    private static boolean debugHdfEosOff;
    private NetcdfFile ncfile;
    private RandomAccessFile raf;
    private List<Tag> alltags;
    private Map<Integer, Tag> tagMap;
    private Map<Short, Vinfo> refnoMap;
    private MemTracker memTracker;
    private PrintStream debugOut;
    private static boolean showFile;
    
    public H4header() {
        this.tagMap = new HashMap<Integer, Tag>();
        this.refnoMap = new HashMap<Short, Vinfo>();
        this.debugOut = System.out;
    }
    
    static boolean isValidFile(final RandomAccessFile raf) throws IOException {
        long pos = 0L;
        final long size = raf.length();
        final byte[] b = new byte[4];
        while (pos < size && pos < 500000L) {
            raf.seek(pos);
            raf.read(b);
            final String magic = new String(b);
            if (magic.equals(H4header.shead)) {
                return true;
            }
            pos = ((pos == 0L) ? 512L : (2L * pos));
        }
        return false;
    }
    
    static String createValidObjectName(final String name) {
        return StringUtil.replace(name, ' ', "_");
    }
    
    public static void setDebugFlags(final DebugFlags debugFlag) {
        H4header.debugTag1 = debugFlag.isSet("H4header/tag1");
        H4header.debugTag2 = debugFlag.isSet("H4header/tag2");
        H4header.debugTagDetail = debugFlag.isSet("H4header/tagDetail");
        H4header.debugConstruct = debugFlag.isSet("H4header/construct");
        H4header.debugAtt = debugFlag.isSet("H4header/att");
        H4header.debugLinked = debugFlag.isSet("H4header/linked");
        H4header.debugChunkTable = debugFlag.isSet("H4header/chunkTable");
        H4header.debugChunkDetail = debugFlag.isSet("H4header/chunkDetail");
        H4header.debugTracker = debugFlag.isSet("H4header/memTracker");
        H4header.debugHdfEosOff = debugFlag.isSet("HdfEos/turnOff");
        if (debugFlag.isSet("HdfEos/showWork")) {
            HdfEos.showWork = true;
        }
    }
    
    void read(final RandomAccessFile myRaf, final NetcdfFile ncfile) throws IOException {
        this.raf = myRaf;
        this.ncfile = ncfile;
        final long actualSize = this.raf.length();
        this.memTracker = new MemTracker(actualSize);
        if (!isValidFile(myRaf)) {
            throw new IOException("Not an HDF4 file ");
        }
        this.memTracker.add("header", 0L, this.raf.getFilePointer());
        this.raf.order(0);
        if (H4header.debugConstruct) {
            this.debugOut.println("H4header 0pened file to read:'" + this.raf.getLocation() + "', size=" + actualSize / 1000L + " Kb");
        }
        this.alltags = new ArrayList<Tag>();
        for (long link = this.raf.getFilePointer(); link > 0L; link = this.readDDH(this.alltags, link)) {}
        for (final Tag tag : this.alltags) {
            tag.read();
            this.tagMap.put(tagid(tag.refno, tag.code), tag);
            if (H4header.debugTag1) {
                System.out.println(H4header.debugTagDetail ? tag.detail() : tag);
            }
        }
        ncfile.setLocation(myRaf.getLocation());
        this.construct(ncfile, this.alltags);
        if (!H4header.debugHdfEosOff) {
            final boolean used = HdfEos.amendFromODL(ncfile, ncfile.getRootGroup());
            if (used) {
                this.adjustDimensions();
                final String history = ncfile.findAttValueIgnoreCase(null, "_History", "");
                ncfile.addAttribute(null, new Attribute("_History", history + "; HDF-EOS StructMetadata information was read"));
            }
        }
        if (H4header.debugTag2) {
            for (final Tag tag : this.alltags) {
                this.debugOut.println(H4header.debugTagDetail ? tag.detail() : tag);
            }
        }
        if (H4header.debugTracker) {
            this.memTracker.report();
        }
    }
    
    public void getEosInfo(final Formatter f) throws IOException {
        HdfEos.getEosInfo(this.ncfile, this.ncfile.getRootGroup(), f);
    }
    
    private static int tagid(final short refno, final short code) {
        int result = (code & 0x3FFF) << 16;
        final int result2 = refno & 0xFFFF;
        result += result2;
        return result;
    }
    
    private void construct(final NetcdfFile ncfile, final List<Tag> alltags) throws IOException {
        final List<Variable> vars = new ArrayList<Variable>();
        final List<Group> groups = new ArrayList<Group>();
        for (final Tag t : alltags) {
            if (t.code == 306) {
                final Variable v = this.makeImage((TagGroup)t);
                if (v == null) {
                    continue;
                }
                vars.add(v);
            }
            else {
                if (t.code != 1965) {
                    continue;
                }
                final TagVGroup vgroup = (TagVGroup)t;
                if (vgroup.className.startsWith("Dim") || vgroup.className.startsWith("UDim")) {
                    this.makeDimension(vgroup);
                }
                else if (vgroup.className.startsWith("Var")) {
                    final Variable v2 = this.makeVariable(vgroup);
                    if (v2 == null) {
                        continue;
                    }
                    vars.add(v2);
                }
                else {
                    if (!vgroup.className.startsWith("CDF0.0")) {
                        continue;
                    }
                    this.addGlobalAttributes(vgroup);
                }
            }
        }
        for (final Tag t : alltags) {
            if (t.used) {
                continue;
            }
            if (t.code == 1962) {
                final TagVH tagVH = (TagVH)t;
                if (!tagVH.className.startsWith("Data")) {
                    continue;
                }
                final Variable v2 = this.makeVariable(tagVH);
                if (v2 == null) {
                    continue;
                }
                vars.add(v2);
            }
            else {
                if (t.code != 720) {
                    continue;
                }
                final Variable v = this.makeVariable((TagGroup)t);
                if (v == null) {
                    continue;
                }
                vars.add(v);
            }
        }
        for (final Tag t : alltags) {
            if (t.used) {
                continue;
            }
            if (t.code != 1962) {
                continue;
            }
            final TagVH vh = (TagVH)t;
            if (vh.className.startsWith("Att") || vh.className.startsWith("_HDF_CHK_TBL")) {
                continue;
            }
            final Variable v2 = this.makeVariable(vh);
            if (v2 == null) {
                continue;
            }
            vars.add(v2);
        }
        for (final Tag t : alltags) {
            if (t.used) {
                continue;
            }
            if (t.code != 1965) {
                continue;
            }
            final TagVGroup vgroup = (TagVGroup)t;
            final Group g = this.makeGroup(vgroup, null);
            if (g == null) {
                continue;
            }
            groups.add(g);
        }
        for (final Group g2 : groups) {
            if (g2.getParentGroup() == ncfile.getRootGroup()) {
                ncfile.addGroup(null, g2);
            }
        }
        final Group root = ncfile.getRootGroup();
        Iterator i$2 = vars.iterator();
        while (i$2.hasNext()) {
            final Variable v = i$2.next();
            if (v.getParentGroup() == root && root.findVariable(v.getShortName()) == null) {
                root.addVariable(v);
            }
        }
        i$2 = alltags.iterator();
        while (i$2.hasNext()) {
            final Tag t2 = i$2.next();
            if (t2 instanceof TagAnnotate) {
                final TagAnnotate ta = (TagAnnotate)t2;
                final Vinfo vinfo = this.refnoMap.get(ta.obj_refno);
                if (vinfo == null) {
                    continue;
                }
                vinfo.v.addAttribute(new Attribute((t2.code == 105) ? "description" : "long_name", ta.text));
                t2.used = true;
            }
        }
        ncfile.addAttribute(null, new Attribute("_History", "Direct read of HDF4 file through CDM library"));
        i$2 = alltags.iterator();
        while (i$2.hasNext()) {
            final Tag t2 = i$2.next();
            if (t2.code == 30) {
                ncfile.addAttribute(null, new Attribute("HDF4_Version", ((TagVersion)t2).value()));
                t2.used = true;
            }
            else if (t2.code == 100) {
                ncfile.addAttribute(null, new Attribute("Title-" + t2.refno, ((TagText)t2).text));
                t2.used = true;
            }
            else {
                if (t2.code != 101) {
                    continue;
                }
                ncfile.addAttribute(null, new Attribute("Description-" + t2.refno, ((TagText)t2).text));
                t2.used = true;
            }
        }
    }
    
    private void adjustDimensions() {
        final Map<Dimension, List<Variable>> dimUsedMap = new HashMap<Dimension, List<Variable>>();
        this.findUsedDimensions(this.ncfile.getRootGroup(), dimUsedMap);
        final Set<Dimension> dimUsed = dimUsedMap.keySet();
        final Iterator iter = this.ncfile.getRootGroup().getDimensions().iterator();
        while (iter.hasNext()) {
            final Dimension dim = iter.next();
            if (!dimUsed.contains(dim)) {
                iter.remove();
            }
        }
        for (final Dimension dim2 : dimUsed) {
            Group lowest = null;
            final List<Variable> vlist = dimUsedMap.get(dim2);
            for (final Variable v : vlist) {
                if (lowest == null) {
                    lowest = v.getParentGroup();
                }
                else {
                    lowest = lowest.commonParent(v.getParentGroup());
                }
            }
            final Group current = dim2.getGroup();
            if (current == null) {
                System.out.println("HEY! current == null");
            }
            if (current != lowest) {
                lowest.addDimension(dim2);
                current.remove(dim2);
            }
        }
    }
    
    private void findUsedDimensions(final Group parent, final Map<Dimension, List<Variable>> dimUsedMap) {
        for (final Variable v : parent.getVariables()) {
            for (final Dimension d : v.getDimensions()) {
                if (!d.isShared()) {
                    continue;
                }
                List<Variable> vlist = dimUsedMap.get(d);
                if (vlist == null) {
                    vlist = new ArrayList<Variable>();
                    dimUsedMap.put(d, vlist);
                }
                vlist.add(v);
            }
        }
        for (final Group g : parent.getGroups()) {
            this.findUsedDimensions(g, dimUsedMap);
        }
    }
    
    private void makeDimension(final TagVGroup group) throws IOException {
        final List<TagVH> dims = new ArrayList<TagVH>();
        Tag data = null;
        for (int i = 0; i < group.nelems; ++i) {
            final Tag tag = this.tagMap.get(tagid(group.elem_ref[i], group.elem_tag[i]));
            if (tag == null) {
                throw new IllegalStateException();
            }
            if (tag.code == 1962) {
                dims.add((TagVH)tag);
            }
            if (tag.code == 1963) {
                data = tag;
            }
        }
        if (dims.size() == 0) {
            throw new IllegalStateException();
        }
        int length = 0;
        if (data != null) {
            this.raf.seek(data.offset);
            length = this.raf.readInt();
            data.used = true;
        }
        else {
            for (final TagVH vh : dims) {
                vh.used = true;
                data = this.tagMap.get(tagid(vh.refno, TagEnum.VS.getCode()));
                if (null != data) {
                    data.used = true;
                    this.raf.seek(data.offset);
                    final int length2 = this.raf.readInt();
                    if (H4header.debugConstruct) {
                        System.out.println("dimension length=" + length2 + " for TagVGroup= " + group + " using data " + data.refno);
                    }
                    if (length2 > 0) {
                        length = length2;
                        break;
                    }
                    continue;
                }
            }
        }
        if (data == null) {
            H4header.log.error("**no data for dimension TagVGroup= " + group);
            return;
        }
        if (length <= 0) {
            H4header.log.warn("**dimension length=" + length + " for TagVGroup= " + group + " using data " + data.refno);
        }
        final boolean isUnlimited = length == 0;
        final Dimension dim = new Dimension(createValidObjectName(group.name), length, true, isUnlimited, false);
        if (H4header.debugConstruct) {
            System.out.println("added dimension " + dim + " from VG " + group.refno);
        }
        this.ncfile.addDimension(null, dim);
    }
    
    private void addGlobalAttributes(final TagVGroup group) throws IOException {
        for (int i = 0; i < group.nelems; ++i) {
            final Tag tag = this.tagMap.get(tagid(group.elem_ref[i], group.elem_tag[i]));
            if (tag == null) {
                throw new IllegalStateException();
            }
            if (tag.code == 1962) {
                final TagVH vh = (TagVH)tag;
                if (vh.className.startsWith("Att")) {
                    final String lowername = vh.name.toLowerCase();
                    if (vh.nfields == 1 && H4type.setDataType(vh.fld_type[0], null) == DataType.CHAR && (vh.fld_isize[0] > 4000 || lowername.startsWith("archivemetadata") || lowername.startsWith("coremetadata") || lowername.startsWith("productmetadata") || lowername.startsWith("structmetadata"))) {
                        this.ncfile.addVariable(null, this.makeVariable(vh));
                    }
                    else {
                        final Attribute att = this.makeAttribute(vh);
                        if (null != att) {
                            this.ncfile.addAttribute(null, att);
                        }
                    }
                }
            }
        }
        group.used = true;
    }
    
    private Attribute makeAttribute(final TagVH vh) throws IOException {
        final Tag data = this.tagMap.get(tagid(vh.refno, TagEnum.VS.getCode()));
        if (data == null) {
            throw new IllegalStateException();
        }
        if (vh.nfields != 1) {
            throw new IllegalStateException();
        }
        final String name = vh.name;
        final short type = vh.fld_type[0];
        final int size = vh.fld_isize[0];
        final int nelems = vh.nvert;
        vh.used = true;
        data.used = true;
        Attribute att = null;
        this.raf.seek(data.offset);
        switch (type) {
            case 3:
            case 4: {
                if (nelems == 1) {
                    att = new Attribute(name, this.readString(size));
                    break;
                }
                final String[] vals = new String[nelems];
                for (int i = 0; i < nelems; ++i) {
                    vals[i] = this.readString(size);
                }
                att = new Attribute(name, Array.factory(DataType.STRING.getPrimitiveClassType(), new int[] { nelems }, vals));
                break;
            }
            case 5: {
                if (nelems == 1) {
                    att = new Attribute(name, this.raf.readFloat());
                    break;
                }
                final float[] vals2 = new float[nelems];
                for (int i = 0; i < nelems; ++i) {
                    vals2[i] = this.raf.readFloat();
                }
                att = new Attribute(name, Array.factory(DataType.FLOAT.getPrimitiveClassType(), new int[] { nelems }, vals2));
                break;
            }
            case 6: {
                if (nelems == 1) {
                    att = new Attribute(name, this.raf.readDouble());
                    break;
                }
                final double[] vals3 = new double[nelems];
                for (int i = 0; i < nelems; ++i) {
                    vals3[i] = this.raf.readDouble();
                }
                att = new Attribute(name, Array.factory(DataType.DOUBLE.getPrimitiveClassType(), new int[] { nelems }, vals3));
                break;
            }
            case 20:
            case 21: {
                if (nelems == 1) {
                    att = new Attribute(name, this.raf.readByte());
                    break;
                }
                final byte[] vals4 = new byte[nelems];
                for (int i = 0; i < nelems; ++i) {
                    vals4[i] = this.raf.readByte();
                }
                att = new Attribute(name, Array.factory(DataType.BYTE.getPrimitiveClassType(), new int[] { nelems }, vals4));
                break;
            }
            case 22:
            case 23: {
                if (nelems == 1) {
                    att = new Attribute(name, this.raf.readShort());
                    break;
                }
                final short[] vals5 = new short[nelems];
                for (int i = 0; i < nelems; ++i) {
                    vals5[i] = this.raf.readShort();
                }
                att = new Attribute(name, Array.factory(DataType.SHORT.getPrimitiveClassType(), new int[] { nelems }, vals5));
                break;
            }
            case 24:
            case 25: {
                if (nelems == 1) {
                    att = new Attribute(name, this.raf.readInt());
                    break;
                }
                final int[] vals6 = new int[nelems];
                for (int i = 0; i < nelems; ++i) {
                    vals6[i] = this.raf.readInt();
                }
                att = new Attribute(name, Array.factory(DataType.INT.getPrimitiveClassType(), new int[] { nelems }, vals6));
                break;
            }
            case 26:
            case 27: {
                if (nelems == 1) {
                    att = new Attribute(name, this.raf.readLong());
                    break;
                }
                final long[] vals7 = new long[nelems];
                for (int i = 0; i < nelems; ++i) {
                    vals7[i] = this.raf.readLong();
                }
                att = new Attribute(name, Array.factory(DataType.LONG.getPrimitiveClassType(), new int[] { nelems }, vals7));
                break;
            }
        }
        if (H4header.debugAtt) {
            System.out.println("added attribute " + att);
        }
        return att;
    }
    
    private Group makeGroup(final TagVGroup tagGroup, final Group parent) throws IOException {
        if (tagGroup.nelems < 1) {
            return null;
        }
        final Group group = new Group(this.ncfile, parent, tagGroup.name);
        tagGroup.used = true;
        tagGroup.group = group;
        for (int i = 0; i < tagGroup.nelems; ++i) {
            final Tag tag = this.tagMap.get(tagid(tagGroup.elem_ref[i], tagGroup.elem_tag[i]));
            if (tag == null) {
                H4header.log.error("Reference tag missing= " + tagGroup.elem_ref[i] + "/" + tagGroup.elem_tag[i] + " for group " + tagGroup.refno);
            }
            else {
                if (tag.code == 720 && tag.vinfo != null) {
                    final Variable v = tag.vinfo.v;
                    if (v != null) {
                        this.addVariableToGroup(group, v, tag);
                    }
                    else {
                        H4header.log.error("Missing variable " + tag.refno);
                    }
                }
                if (tag.code == 1962) {
                    final TagVH vh = (TagVH)tag;
                    if (vh.className.startsWith("Att")) {
                        final Attribute att = this.makeAttribute(vh);
                        if (null != att) {
                            group.addAttribute(att);
                        }
                    }
                    else if (tag.vinfo != null) {
                        final Variable v2 = tag.vinfo.v;
                        this.addVariableToGroup(group, v2, tag);
                    }
                }
                if (tag.code == 1965) {
                    final TagVGroup vg = (TagVGroup)tag;
                    if (vg.group != null && vg.group.getParentGroup() == this.ncfile.getRootGroup()) {
                        this.addGroupToGroup(group, vg.group, vg);
                        vg.group.setParentGroup(group);
                    }
                    else {
                        final Group nested = this.makeGroup(vg, group);
                        if (nested != null) {
                            this.addGroupToGroup(group, nested, vg);
                        }
                    }
                }
            }
        }
        if (H4header.debugConstruct) {
            System.out.println("added group " + group.getName() + " from VG " + tagGroup.refno);
        }
        return group;
    }
    
    private void addVariableToGroup(final Group g, final Variable v, final Tag tag) {
        final Variable varExisting = g.findVariable(v.getShortName());
        if (varExisting != null) {
            v.setName(v.getShortName() + tag.refno);
        }
        g.addVariable(v);
    }
    
    private void addGroupToGroup(final Group parent, final Group g, final Tag tag) {
        final Group groupExisting = parent.findGroup(g.getShortName());
        if (groupExisting != null) {
            g.setName(g.getShortName() + tag.refno);
        }
        parent.addGroup(g);
    }
    
    private Variable makeImage(final TagGroup group) {
        TagRIDimension dimTag = null;
        TagRIPalette palette = null;
        TagNumberType ntag = null;
        Tag data = null;
        final Vinfo vinfo = new Vinfo(group.refno);
        group.used = true;
        for (int i = 0; i < group.nelems; ++i) {
            final Tag tag = this.tagMap.get(tagid(group.elem_ref[i], group.elem_tag[i]));
            if (tag == null) {
                throw new IllegalStateException();
            }
            vinfo.tags.add(tag);
            tag.vinfo = vinfo;
            tag.used = true;
            if (tag.code == 300) {
                dimTag = (TagRIDimension)tag;
            }
            if (tag.code == 302) {
                data = tag;
            }
            if (tag.code == 301) {
                palette = (TagRIPalette)tag;
            }
        }
        if (dimTag == null || data == null) {
            throw new IllegalStateException();
        }
        final Tag tag2 = this.tagMap.get(tagid(dimTag.nt_ref, TagEnum.NT.getCode()));
        if (tag2 == null) {
            throw new IllegalStateException();
        }
        ntag = (TagNumberType)tag2;
        if (H4header.debugConstruct) {
            System.out.println("construct image " + group.refno);
        }
        vinfo.start = data.offset;
        vinfo.tags.add(group);
        vinfo.tags.add(dimTag);
        vinfo.tags.add(data);
        vinfo.tags.add(ntag);
        if (dimTag.dims == null) {
            (dimTag.dims = new ArrayList<Dimension>()).add(this.makeDimensionUnshared("ydim", dimTag.ydim));
            dimTag.dims.add(this.makeDimensionUnshared("xdim", dimTag.xdim));
        }
        final Variable v = new Variable(this.ncfile, null, null, "Image-" + group.refno);
        H4type.setDataType(ntag.type, v);
        v.setDimensions(dimTag.dims);
        vinfo.setVariable(v);
        return v;
    }
    
    private Dimension makeDimensionUnshared(final String dimName, final int len) {
        return new Dimension(dimName, len, false);
    }
    
    private Dimension makeDimensionShared(String dimName, final int len) {
        final Group root = this.ncfile.getRootGroup();
        Dimension d = root.findDimension(dimName);
        if (d != null && d.getLength() == len) {
            return d;
        }
        if (d != null) {
            dimName += len;
            d = root.findDimension(dimName);
            if (d != null && d.getLength() == len) {
                return d;
            }
        }
        return this.ncfile.addDimension(null, new Dimension(dimName, len));
    }
    
    private Variable makeVariable(final TagVH vh) throws IOException {
        final Vinfo vinfo = new Vinfo(vh.refno);
        vinfo.tags.add(vh);
        vh.vinfo = vinfo;
        vh.used = true;
        final TagData data = this.tagMap.get(tagid(vh.refno, TagEnum.VS.getCode()));
        if (data == null) {
            H4header.log.error("Cant find tag " + vh.refno + "/" + TagEnum.VS.getCode() + " for TagVH=" + vh.detail());
            return null;
        }
        vinfo.tags.add(data);
        data.used = true;
        data.vinfo = vinfo;
        if (vh.nfields < 1) {
            throw new IllegalStateException();
        }
        Variable v;
        if (vh.nfields == 1) {
            v = new Variable(this.ncfile, null, null, vh.name);
            vinfo.setVariable(v);
            H4type.setDataType(vh.fld_type[0], v);
            try {
                if (vh.nvert > 1) {
                    if (vh.fld_order[0] > 1) {
                        v.setDimensionsAnonymous(new int[] { vh.nvert, vh.fld_order[0] });
                    }
                    else if (vh.fld_order[0] < 0) {
                        v.setDimensionsAnonymous(new int[] { vh.nvert, vh.fld_isize[0] });
                    }
                    else {
                        v.setDimensionsAnonymous(new int[] { vh.nvert });
                    }
                }
                else if (vh.fld_order[0] > 1) {
                    v.setDimensionsAnonymous(new int[] { vh.fld_order[0] });
                }
                else if (vh.fld_order[0] < 0) {
                    v.setDimensionsAnonymous(new int[] { vh.fld_isize[0] });
                }
                else {
                    v.setIsScalar();
                }
            }
            catch (InvalidRangeException e2) {
                throw new IllegalStateException();
            }
            vinfo.setData(data, v.getElementSize());
        }
        else {
            Structure s;
            try {
                s = new Structure(this.ncfile, null, null, vh.name);
                vinfo.setVariable(s);
                if (vh.nvert > 1) {
                    s.setDimensionsAnonymous(new int[] { vh.nvert });
                }
                else {
                    s.setIsScalar();
                }
                for (int fld = 0; fld < vh.nfields; ++fld) {
                    final Variable m = new Variable(this.ncfile, null, s, vh.fld_name[fld]);
                    final short type = vh.fld_type[fld];
                    final int nbytes = vh.fld_isize[fld];
                    final short nelems = vh.fld_order[fld];
                    H4type.setDataType(type, m);
                    if (nelems > 1) {
                        m.setDimensionsAnonymous(new int[] { nelems });
                    }
                    else {
                        m.setIsScalar();
                    }
                    m.setSPobject(new Minfo(vh.fld_offset[fld], nbytes, nelems));
                    s.addMemberVariable(m);
                }
            }
            catch (InvalidRangeException e) {
                throw new IllegalStateException(e.getMessage());
            }
            vinfo.setData(data, vh.ivsize);
            v = s;
        }
        if (H4header.debugConstruct) {
            System.out.println("added variable " + v.getNameAndDimensions() + " from VH " + vh);
        }
        return v;
    }
    
    private Variable makeVariable(final TagVGroup group) throws IOException {
        final Vinfo vinfo = new Vinfo(group.refno);
        vinfo.tags.add(group);
        group.used = true;
        TagSDDimension dim = null;
        TagNumberType ntag = null;
        TagData data = null;
        final List<Dimension> dims = new ArrayList<Dimension>();
        for (int i = 0; i < group.nelems; ++i) {
            final Tag tag = this.tagMap.get(tagid(group.elem_ref[i], group.elem_tag[i]));
            if (tag == null) {
                H4header.log.error("Reference tag missing= " + group.elem_ref[i] + "/" + group.elem_tag[i]);
            }
            else {
                vinfo.tags.add(tag);
                tag.vinfo = vinfo;
                tag.used = true;
                if (tag.code == 106) {
                    ntag = (TagNumberType)tag;
                }
                if (tag.code == 701) {
                    dim = (TagSDDimension)tag;
                }
                if (tag.code == 702) {
                    data = (TagData)tag;
                }
                if (tag.code == 1965) {
                    final TagVGroup vg = (TagVGroup)tag;
                    if (vg.className.startsWith("Dim") || vg.className.startsWith("UDim")) {
                        final String dimName = createValidObjectName(vg.name);
                        final Dimension d = this.ncfile.getRootGroup().findDimension(dimName);
                        if (d == null) {
                            throw new IllegalStateException();
                        }
                        dims.add(d);
                    }
                }
            }
        }
        if (ntag == null) {
            H4header.log.error("ntype tag missing vgroup= " + group.refno);
            return null;
        }
        if (dim == null) {
            H4header.log.error("dim tag missing vgroup= " + group.refno);
            return null;
        }
        if (data == null) {
            H4header.log.warn("data tag missing vgroup= " + group.refno + " " + group.name);
        }
        final Variable v = new Variable(this.ncfile, null, null, group.name);
        v.setDimensions(dims);
        H4type.setDataType(ntag.type, v);
        vinfo.setVariable(v);
        vinfo.setData(data, v.getElementSize());
        assert dim.shape.length == v.getRank();
        boolean ok = true;
        for (int j = 0; j < dim.shape.length; ++j) {
            if (dim.shape[j] != v.getDimension(j).getLength()) {
                if (H4header.warnings) {
                    H4header.log.info(dim.shape[j] + " != " + v.getDimension(j).getLength() + " for " + v.getName());
                }
                ok = false;
            }
        }
        if (!ok) {
            try {
                v.setDimensionsAnonymous(dim.shape);
            }
            catch (InvalidRangeException e) {
                e.printStackTrace();
            }
        }
        this.addVariableAttributes(group, vinfo);
        if (H4header.debugConstruct) {
            System.out.println("added variable " + v.getNameAndDimensions() + " from VG " + group.refno);
            System.out.println("  SDdim= " + dim.detail());
            System.out.print("  VGdim= ");
            for (final Dimension vdim : dims) {
                System.out.print(vdim + " ");
            }
            System.out.println();
        }
        return v;
    }
    
    private Variable makeVariable(final TagGroup group) throws IOException {
        final Vinfo vinfo = new Vinfo(group.refno);
        vinfo.tags.add(group);
        group.used = true;
        TagSDDimension dim = null;
        TagData data = null;
        for (int i = 0; i < group.nelems; ++i) {
            final Tag tag = this.tagMap.get(tagid(group.elem_ref[i], group.elem_tag[i]));
            if (tag == null) {
                H4header.log.error("Cant find tag " + group.elem_ref[i] + "/" + group.elem_tag[i] + " for group=" + group.refno);
            }
            else {
                vinfo.tags.add(tag);
                tag.vinfo = vinfo;
                tag.used = true;
                if (tag.code == 701) {
                    dim = (TagSDDimension)tag;
                }
                if (tag.code == 702) {
                    data = (TagData)tag;
                }
            }
        }
        if (dim == null || data == null) {
            throw new IllegalStateException();
        }
        final TagNumberType nt = this.tagMap.get(tagid(dim.nt_ref, TagEnum.NT.getCode()));
        if (null == nt) {
            throw new IllegalStateException();
        }
        final Variable v = new Variable(this.ncfile, null, null, "SDS-" + group.refno);
        try {
            v.setDimensionsAnonymous(dim.shape);
        }
        catch (InvalidRangeException e) {
            throw new IllegalStateException();
        }
        final DataType dataType = H4type.setDataType(nt.type, v);
        vinfo.setVariable(v);
        vinfo.setData(data, v.getElementSize());
        for (int j = 0; j < group.nelems; ++j) {
            final Tag tag2 = this.tagMap.get(tagid(group.elem_ref[j], group.elem_tag[j]));
            if (tag2 == null) {
                throw new IllegalStateException();
            }
            if (tag2.code == 704) {
                final TagTextN labels = (TagTextN)tag2;
                labels.read(dim.rank);
                tag2.used = true;
                v.addAttribute(new Attribute("long_name", labels.getList()));
            }
            if (tag2.code == 705) {
                final TagTextN units = (TagTextN)tag2;
                units.read(dim.rank);
                tag2.used = true;
                v.addAttribute(new Attribute("units", units.getList()));
            }
            if (tag2.code == 706) {
                final TagTextN formats = (TagTextN)tag2;
                formats.read(dim.rank);
                tag2.used = true;
                v.addAttribute(new Attribute("formats", formats.getList()));
            }
            if (tag2.code == 707) {
                final TagSDminmax minmax = (TagSDminmax)tag2;
                tag2.used = true;
                v.addAttribute(new Attribute("min", minmax.getMin(dataType)));
                v.addAttribute(new Attribute("max", minmax.getMax(dataType)));
            }
        }
        this.addVariableAttributes(group, vinfo);
        if (H4header.debugConstruct) {
            System.out.println("added variable " + v.getNameAndDimensions() + " from Group " + group);
            System.out.println("  SDdim= " + dim.detail());
        }
        return v;
    }
    
    private void addVariableAttributes(final TagGroup group, final Vinfo vinfo) throws IOException {
        for (int i = 0; i < group.nelems; ++i) {
            final Tag tag = this.tagMap.get(tagid(group.elem_ref[i], group.elem_tag[i]));
            if (tag == null) {
                throw new IllegalStateException();
            }
            if (tag.code == 1962) {
                final TagVH vh = (TagVH)tag;
                if (vh.className.startsWith("Att")) {
                    final Attribute att = this.makeAttribute(vh);
                    if (null != att) {
                        vinfo.v.addAttribute(att);
                        if (att.getName().equals("_FillValue")) {
                            vinfo.setFillValue(att);
                        }
                    }
                }
            }
        }
    }
    
    private long readDDH(final List<Tag> alltags, final long start) throws IOException {
        this.raf.seek(start);
        final int ndd = DataType.unsignedShortToInt(this.raf.readShort());
        final long link = DataType.unsignedIntToLong(this.raf.readInt());
        if (H4header.debugDD) {
            System.out.println(" DDHeader ndd=" + ndd + " link=" + link);
        }
        long pos = this.raf.getFilePointer();
        for (int i = 0; i < ndd; ++i) {
            this.raf.seek(pos);
            final Tag tag = this.factory();
            pos += 12L;
            if (tag.code > 1) {
                alltags.add(tag);
            }
        }
        this.memTracker.add("DD block", start, this.raf.getFilePointer());
        return link;
    }
    
    private Tag factory() throws IOException {
        final short code = this.raf.readShort();
        final int ccode = code & 0x3FFF;
        switch (ccode) {
            case 20: {
                return new TagLinkedBlock(code);
            }
            case 30: {
                return new TagVersion(code);
            }
            case 40:
            case 61:
            case 702:
            case 1963: {
                return new TagData(code);
            }
            case 100:
            case 101:
            case 708: {
                return new TagText(code);
            }
            case 104:
            case 105: {
                return new TagAnnotate(code);
            }
            case 106: {
                return new TagNumberType(code);
            }
            case 300:
            case 307:
            case 308: {
                return new TagRIDimension(code);
            }
            case 301: {
                return new TagRIPalette(code);
            }
            case 306:
            case 720: {
                return new TagGroup(code);
            }
            case 701: {
                return new TagSDDimension(code);
            }
            case 704:
            case 705:
            case 706: {
                return new TagTextN(code);
            }
            case 707: {
                return new TagSDminmax(code);
            }
            case 1962: {
                return new TagVH(code);
            }
            case 1965: {
                return new TagVGroup(code);
            }
            default: {
                return new Tag(code);
            }
        }
    }
    
    private String printa(final int[] array) {
        final StringBuilder sbuff = new StringBuilder();
        for (int i = 0; i < array.length; ++i) {
            sbuff.append(" ").append(array[i]);
        }
        return sbuff.toString();
    }
    
    private String readString(final int len) throws IOException {
        if (len < 0) {
            System.out.println("what");
        }
        final byte[] b = new byte[len];
        this.raf.read(b);
        int count;
        for (count = 0; count < len && b[count] != 0; ++count) {}
        return new String(b, 0, count, "UTF-8");
    }
    
    public List<Tag> getTags() {
        return this.alltags;
    }
    
    static void readAllDir(final String dirName, final boolean subdirs) throws IOException {
        System.out.println("---------------Reading directory " + dirName);
        final File allDir = new File(dirName);
        final File[] allFiles = allDir.listFiles();
        if (null == allFiles) {
            System.out.println("---------------INVALID " + dirName);
            return;
        }
        for (final File f : allFiles) {
            final String name = f.getAbsolutePath();
            if (name.endsWith(".hdf")) {
                test(name);
            }
        }
        for (final File f : allFiles) {
            if (f.isDirectory() && subdirs) {
                readAllDir(f.getAbsolutePath(), subdirs);
            }
        }
    }
    
    static void testPelim(final String filename) throws IOException {
        final RandomAccessFile raf = new RandomAccessFile(filename, "r");
        final NetcdfFile ncfile = new MyNetcdfFile();
        final H4header header = new H4header();
        header.read(raf, ncfile);
        if (H4header.showFile) {
            System.out.println(ncfile);
        }
    }
    
    static void test(final String filename) throws IOException {
        final NetcdfFile ncfile = NetcdfFile.open(filename);
        if (H4header.showFile) {
            System.out.println(ncfile);
        }
    }
    
    static void testTagid(final short tag, final short refno) throws IOException {
        System.out.format(" tag= %#x refno=%#x tagid=%#x \n", tag, refno, tagid(refno, tag));
    }
    
    public static void main(final String[] args) throws IOException {
        testTagid((short)123, (short)(-12));
        testTagid((short)123, (short)(-5385));
    }
    
    static {
        H4header.log = LoggerFactory.getLogger(H4header.class);
        head = new byte[] { 14, 3, 19, 1 };
        shead = new String(H4header.head);
        H4header.debugDD = false;
        H4header.debugTag1 = false;
        H4header.debugTag2 = false;
        H4header.debugTagDetail = false;
        H4header.debugConstruct = false;
        H4header.debugAtt = false;
        H4header.debugLinked = false;
        H4header.debugChunkTable = false;
        H4header.debugChunkDetail = false;
        H4header.debugTracker = false;
        H4header.warnings = false;
        H4header.debugHdfEosOff = false;
        H4header.showFile = true;
    }
    
    static class Minfo
    {
        short nelems;
        int offset;
        int nbytes;
        
        Minfo(final int offset, final int nbytes, final short nelems) {
            this.offset = offset;
            this.nbytes = nbytes;
            this.nelems = nelems;
        }
    }
    
    class Vinfo implements Comparable<Vinfo>
    {
        short refno;
        Variable v;
        List<Tag> tags;
        TagData data;
        int elemSize;
        Object fillValue;
        boolean isLinked;
        boolean isCompressed;
        boolean isChunked;
        boolean hasNoData;
        int start;
        int length;
        long[] segPos;
        int[] segSize;
        List<DataChunk> chunks;
        int[] chunkSize;
        
        Vinfo(final short refno) {
            this.tags = new ArrayList<Tag>();
            this.start = -1;
            this.refno = refno;
            H4header.this.refnoMap.put(refno, this);
        }
        
        void setVariable(final Variable v) {
            (this.v = v).setSPobject(this);
        }
        
        public int compareTo(final Vinfo o) {
            return this.refno - o.refno;
        }
        
        void setData(final TagData data, final int elemSize) throws IOException {
            this.data = data;
            this.elemSize = elemSize;
            this.hasNoData = (data == null);
        }
        
        void setFillValue(final Attribute att) {
            this.fillValue = ((this.v.getDataType() == DataType.STRING) ? att.getStringValue() : att.getNumericValue());
        }
        
        void setLayoutInfo() throws IOException {
            if (this.data == null) {
                return;
            }
            if (null != this.data.linked) {
                this.isLinked = true;
                this.setDataBlocks(this.data.linked.getLinkedDataBlocks(), this.elemSize);
            }
            else if (null != this.data.compress) {
                this.isCompressed = true;
                final TagData compData = this.data.compress.getDataTag();
                this.tags.add(compData);
                this.isLinked = (compData.linked != null);
                if (this.isLinked) {
                    this.setDataBlocks(compData.linked.getLinkedDataBlocks(), this.elemSize);
                }
                else {
                    this.start = compData.offset;
                    this.length = compData.length;
                    this.hasNoData = (this.start < 0 || this.length < 0);
                }
            }
            else if (null != this.data.chunked) {
                this.isChunked = true;
                this.chunks = this.data.chunked.getDataChunks();
                this.chunkSize = this.data.chunked.chunk_length;
                this.isCompressed = this.data.chunked.isCompressed;
            }
            else {
                this.start = this.data.offset;
                this.hasNoData = (this.start < 0);
            }
        }
        
        private void setDataBlocks(final List<TagLinkedBlock> linkedBlocks, final int elemSize) {
            final int nsegs = linkedBlocks.size();
            this.segPos = new long[nsegs];
            this.segSize = new int[nsegs];
            int count = 0;
            for (final TagLinkedBlock tag : linkedBlocks) {
                this.segPos[count] = tag.offset;
                this.segSize[count] = tag.length;
                ++count;
            }
        }
        
        @Override
        public String toString() {
            final Formatter sbuff = new Formatter();
            sbuff.format("refno=%d name=%s fillValue=%s %n", this.refno, this.v.getShortName(), this.fillValue);
            sbuff.format(" isChunked=%s isCompressed=%s isLinked=%s hasNoData=%s %n", this.isChunked, this.isCompressed, this.isLinked, this.hasNoData);
            sbuff.format(" elemSize=%d data start=%d length=%s %n%n", this.elemSize, this.start, this.length);
            for (final Tag t : this.tags) {
                sbuff.format(" %s%n", t.detail());
            }
            return sbuff.toString();
        }
    }
    
    public class Tag
    {
        short code;
        short refno;
        boolean extended;
        int offset;
        int length;
        TagEnum t;
        boolean used;
        Vinfo vinfo;
        
        Tag(final short code) throws IOException {
            this.extended = ((code & 0x4000) != 0x0);
            this.code = (short)(code & 0x3FFF);
            this.refno = H4header.this.raf.readShort();
            this.offset = H4header.this.raf.readInt();
            this.length = H4header.this.raf.readInt();
            this.t = TagEnum.getTag(this.code);
            if (code > 1 && H4header.debugTracker) {
                H4header.this.memTracker.add(this.t.getName() + " " + this.refno, this.offset, this.offset + this.length);
            }
        }
        
        void read() throws IOException {
        }
        
        public String detail() {
            return (this.used ? " " : "*") + "refno=" + this.refno + " tag= " + this.t + (this.extended ? " EXTENDED" : "") + " offset=" + this.offset + " length=" + this.length + ((this.vinfo != null && this.vinfo.v != null) ? (" VV=" + this.vinfo.v.getName()) : "");
        }
        
        @Override
        public String toString() {
            return (this.used ? " " : "*") + "refno=" + this.refno + " tag= " + this.t + (this.extended ? " EXTENDED" : (" length=" + this.length));
        }
        
        public short getCode() {
            return this.code;
        }
        
        public short getRefno() {
            return this.refno;
        }
        
        public boolean isExtended() {
            return this.extended;
        }
        
        public int getOffset() {
            return this.offset;
        }
        
        public int getLength() {
            return this.length;
        }
        
        public String getType() {
            return this.t.toString();
        }
        
        public boolean isUsed() {
            return this.used;
        }
        
        public String getVinfo() {
            return (this.vinfo == null) ? "" : this.vinfo.toString();
        }
        
        public String getVClass() {
            if (this instanceof TagVGroup) {
                return ((TagVGroup)this).className;
            }
            if (this instanceof TagVH) {
                return ((TagVH)this).className;
            }
            return "";
        }
    }
    
    class TagData extends Tag
    {
        short ext_type;
        SpecialLinked linked;
        SpecialComp compress;
        SpecialChunked chunked;
        int tag_len;
        
        TagData(final short code) throws IOException {
            super(code);
        }
        
        @Override
        void read() throws IOException {
            if (this.extended) {
                H4header.this.raf.seek(this.offset);
                this.ext_type = H4header.this.raf.readShort();
                if (this.ext_type == TagEnum.SPECIAL_LINKED) {
                    (this.linked = new SpecialLinked()).read();
                }
                else if (this.ext_type == TagEnum.SPECIAL_COMP) {
                    (this.compress = new SpecialComp()).read();
                }
                else if (this.ext_type == TagEnum.SPECIAL_CHUNKED) {
                    (this.chunked = new SpecialChunked()).read();
                }
                this.tag_len = (int)(H4header.this.raf.getFilePointer() - this.offset);
            }
        }
        
        @Override
        public String detail() {
            if (this.linked != null) {
                return super.detail() + " ext_tag= " + this.ext_type + " tag_len= " + this.tag_len + " " + this.linked.detail();
            }
            if (this.compress != null) {
                return super.detail() + " ext_tag= " + this.ext_type + " tag_len= " + this.tag_len + " " + this.compress.detail();
            }
            if (this.chunked != null) {
                return super.detail() + " ext_tag= " + this.ext_type + " tag_len= " + this.tag_len + " " + this.chunked.detail();
            }
            return super.detail();
        }
    }
    
    private class SpecialChunked
    {
        byte version;
        byte flag;
        short chunk_tbl_tag;
        short chunk_tbl_ref;
        int head_len;
        int elem_tot_length;
        int chunk_size;
        int nt_size;
        int ndims;
        int[] dim_length;
        int[] chunk_length;
        byte[][] dim_flag;
        boolean isCompressed;
        short sp_tag_desc;
        byte[] sp_tag_header;
        List<DataChunk> dataChunks;
        
        private SpecialChunked() {
            this.dataChunks = null;
        }
        
        void read() throws IOException {
            this.head_len = H4header.this.raf.readInt();
            this.version = H4header.this.raf.readByte();
            H4header.this.raf.skipBytes(3);
            this.flag = H4header.this.raf.readByte();
            this.elem_tot_length = H4header.this.raf.readInt();
            this.chunk_size = H4header.this.raf.readInt();
            this.nt_size = H4header.this.raf.readInt();
            this.chunk_tbl_tag = H4header.this.raf.readShort();
            this.chunk_tbl_ref = H4header.this.raf.readShort();
            H4header.this.raf.skipBytes(4);
            this.ndims = H4header.this.raf.readInt();
            this.dim_flag = new byte[this.ndims][4];
            this.dim_length = new int[this.ndims];
            this.chunk_length = new int[this.ndims];
            for (int i = 0; i < this.ndims; ++i) {
                H4header.this.raf.read(this.dim_flag[i]);
                this.dim_length[i] = H4header.this.raf.readInt();
                this.chunk_length[i] = H4header.this.raf.readInt();
            }
            final int fill_val_numtype = H4header.this.raf.readInt();
            final byte[] fill_value = new byte[fill_val_numtype];
            H4header.this.raf.read(fill_value);
            this.sp_tag_desc = H4header.this.raf.readShort();
            final int sp_header_len = H4header.this.raf.readInt();
            this.sp_tag_header = new byte[sp_header_len];
            H4header.this.raf.read(this.sp_tag_header);
        }
        
        List<DataChunk> getDataChunks() throws IOException {
            if (this.dataChunks == null) {
                this.dataChunks = new ArrayList<DataChunk>();
                if (H4header.debugChunkTable) {
                    System.out.println(" TagData getChunkedTable " + this.detail());
                }
                final TagVH chunkTableTag = H4header.this.tagMap.get(tagid(this.chunk_tbl_ref, this.chunk_tbl_tag));
                final Structure s = (Structure)H4header.this.makeVariable(chunkTableTag);
                final ArrayStructure sdata = (ArrayStructure)s.read();
                if (H4header.debugChunkDetail) {
                    System.out.println(NCdumpW.printArray(sdata, "getChunkedTable", null));
                }
                final StructureMembers members = sdata.getStructureMembers();
                final StructureMembers.Member originM = members.findMember("origin");
                final StructureMembers.Member tagM = members.findMember("chk_tag");
                final StructureMembers.Member refM = members.findMember("chk_ref");
                final int n = (int)sdata.getSize();
                if (H4header.debugChunkTable) {
                    System.out.println(" Reading " + n + " DataChunk tags");
                }
                for (int i = 0; i < n; ++i) {
                    final int[] origin = sdata.getJavaArrayInt(i, originM);
                    final short tag = sdata.getScalarShort(i, tagM);
                    final short ref = sdata.getScalarShort(i, refM);
                    final TagData data = H4header.this.tagMap.get(tagid(ref, tag));
                    this.dataChunks.add(new DataChunk(origin, this.chunk_length, data));
                    data.used = true;
                    if (data.compress != null) {
                        this.isCompressed = true;
                    }
                }
            }
            return this.dataChunks;
        }
        
        public String detail() {
            final StringBuilder sbuff = new StringBuilder("SPECIAL_CHUNKED ");
            sbuff.append(" head_len=").append(this.head_len).append(" version=").append(this.version).append(" special =").append(this.flag).append(" elem_tot_length=").append(this.elem_tot_length);
            sbuff.append(" chunk_size=").append(this.chunk_size).append(" nt_size=").append(this.nt_size).append(" chunk_tbl_tag=").append(this.chunk_tbl_tag).append(" chunk_tbl_ref=").append(this.chunk_tbl_ref);
            sbuff.append("\n flag  dim  chunk\n");
            for (int i = 0; i < this.ndims; ++i) {
                sbuff.append(" ").append(this.dim_flag[i][2]).append(",").append(this.dim_flag[i][3]).append(" ").append(this.dim_length[i]).append(" ").append(this.chunk_length[i]).append("\n");
            }
            sbuff.append(" special=").append(this.sp_tag_desc).append(" val=");
            for (int i = 0; i < this.sp_tag_header.length; ++i) {
                sbuff.append(" ").append(this.sp_tag_header[i]);
            }
            return sbuff.toString();
        }
    }
    
    class DataChunk
    {
        int[] origin;
        TagData data;
        
        DataChunk(final int[] origin, final int[] chunk_length, final TagData data) {
            assert origin.length == chunk_length.length;
            for (int i = 0; i < origin.length; ++i) {
                final int n = i;
                origin[n] *= chunk_length[i];
            }
            this.origin = origin;
            this.data = data;
            if (H4header.debugChunkTable) {
                System.out.print(" Chunk origin=");
                for (int i = 0; i < origin.length; ++i) {
                    System.out.print(origin[i] + " ");
                }
                System.out.println(" data=" + data.detail());
            }
        }
    }
    
    class SpecialComp
    {
        short version;
        short model_type;
        short compress_type;
        short data_ref;
        int uncomp_length;
        TagData dataTag;
        short signFlag;
        short fillValue;
        int nt;
        int startBit;
        int bitLength;
        short deflateLevel;
        
        void read() throws IOException {
            this.version = H4header.this.raf.readShort();
            this.uncomp_length = H4header.this.raf.readInt();
            this.data_ref = H4header.this.raf.readShort();
            this.model_type = H4header.this.raf.readShort();
            this.compress_type = H4header.this.raf.readShort();
            if (this.compress_type == TagEnum.COMP_CODE_NBIT) {
                this.nt = H4header.this.raf.readInt();
                this.signFlag = H4header.this.raf.readShort();
                this.fillValue = H4header.this.raf.readShort();
                this.startBit = H4header.this.raf.readInt();
                this.bitLength = H4header.this.raf.readInt();
            }
            else if (this.compress_type == TagEnum.COMP_CODE_DEFLATE) {
                this.deflateLevel = H4header.this.raf.readShort();
            }
        }
        
        TagData getDataTag() throws IOException {
            if (this.dataTag == null) {
                this.dataTag = H4header.this.tagMap.get(tagid(this.data_ref, TagEnum.COMPRESSED.getCode()));
                if (this.dataTag == null) {
                    throw new IllegalStateException("TagCompress not found for " + this.detail());
                }
                this.dataTag.used = true;
            }
            return this.dataTag;
        }
        
        public String detail() {
            final StringBuilder sbuff = new StringBuilder("SPECIAL_COMP ");
            sbuff.append(" version=").append(this.version).append(" uncompressed length =").append(this.uncomp_length).append(" link_ref=").append(this.data_ref);
            sbuff.append(" model_type=").append(this.model_type).append(" compress_type=").append(this.compress_type);
            if (this.compress_type == TagEnum.COMP_CODE_NBIT) {
                sbuff.append(" nt=").append(this.nt).append(" signFlag=").append(this.signFlag).append(" fillValue=").append(this.fillValue).append(" startBit=").append(this.startBit).append(" bitLength=").append(this.bitLength);
            }
            else if (this.compress_type == TagEnum.COMP_CODE_DEFLATE) {
                sbuff.append(" deflateLevel=").append(this.deflateLevel);
            }
            return sbuff.toString();
        }
    }
    
    class SpecialLinked
    {
        int length;
        int first_len;
        short blk_len;
        short num_blk;
        short link_ref;
        List<TagLinkedBlock> linkedDataBlocks;
        
        void read() throws IOException {
            this.length = H4header.this.raf.readInt();
            this.first_len = H4header.this.raf.readInt();
            this.blk_len = H4header.this.raf.readShort();
            this.num_blk = H4header.this.raf.readShort();
            this.link_ref = H4header.this.raf.readShort();
        }
        
        List<TagLinkedBlock> getLinkedDataBlocks() throws IOException {
            if (this.linkedDataBlocks == null) {
                this.linkedDataBlocks = new ArrayList<TagLinkedBlock>();
                if (H4header.debugLinked) {
                    System.out.println(" TagData readLinkTags " + this.detail());
                }
                TagLinkedBlock tag;
                for (short next = this.link_ref; next != 0; next = tag.next_ref) {
                    tag = H4header.this.tagMap.get(tagid(next, TagEnum.LINKED.getCode()));
                    if (tag == null) {
                        throw new IllegalStateException("TagLinkedBlock not found for " + this.detail());
                    }
                    tag.used = true;
                    tag.read2(this.num_blk, this.linkedDataBlocks);
                }
            }
            return this.linkedDataBlocks;
        }
        
        public String detail() {
            return "SPECIAL_LINKED length=" + this.length + " first_len=" + this.first_len + " blk_len=" + this.blk_len + " num_blk=" + this.num_blk + " link_ref=" + this.link_ref;
        }
    }
    
    class TagLinkedBlock extends Tag
    {
        short next_ref;
        short[] block_ref;
        int n;
        
        TagLinkedBlock(final short code) throws IOException {
            super(code);
        }
        
        void read2(final int nb, final List<TagLinkedBlock> dataBlocks) throws IOException {
            H4header.this.raf.seek(this.offset);
            this.next_ref = H4header.this.raf.readShort();
            this.block_ref = new short[nb];
            for (int i = 0; i < nb; ++i) {
                this.block_ref[i] = H4header.this.raf.readShort();
                if (this.block_ref[i] == 0) {
                    break;
                }
                ++this.n;
            }
            if (H4header.debugLinked) {
                System.out.println(" TagLinkedBlock read2 " + this.detail());
            }
            for (int i = 0; i < this.n; ++i) {
                final TagLinkedBlock tag = H4header.this.tagMap.get(tagid(this.block_ref[i], TagEnum.LINKED.getCode()));
                tag.used = true;
                dataBlocks.add(tag);
                if (H4header.debugLinked) {
                    System.out.println("   Linked data= " + tag.detail());
                }
            }
        }
        
        @Override
        public String detail() {
            if (this.block_ref == null) {
                return super.detail();
            }
            final StringBuilder sbuff = new StringBuilder(super.detail());
            sbuff.append(" next_ref= ").append(this.next_ref);
            sbuff.append(" dataBlks= ");
            for (int i = 0; i < this.n; ++i) {
                final short ref = this.block_ref[i];
                sbuff.append(ref).append(" ");
            }
            return sbuff.toString();
        }
    }
    
    private class TagVersion extends Tag
    {
        int major;
        int minor;
        int release;
        String name;
        
        TagVersion(final short code) throws IOException {
            super(code);
        }
        
        @Override
        void read() throws IOException {
            H4header.this.raf.seek(this.offset);
            this.major = H4header.this.raf.readInt();
            this.minor = H4header.this.raf.readInt();
            this.release = H4header.this.raf.readInt();
            this.name = H4header.this.readString(this.length - 12);
        }
        
        public String value() {
            return this.major + "." + this.minor + "." + this.release + " (" + this.name + ")";
        }
        
        @Override
        public String detail() {
            return super.detail() + " version= " + this.major + "." + this.minor + "." + this.release + " (" + this.name + ")";
        }
    }
    
    private class TagText extends Tag
    {
        String text;
        
        TagText(final short code) throws IOException {
            super(code);
        }
        
        @Override
        void read() throws IOException {
            H4header.this.raf.seek(this.offset);
            this.text = H4header.this.readString(this.length);
        }
        
        @Override
        public String detail() {
            final String t = (this.text.length() < 60) ? this.text : this.text.substring(0, 59);
            return super.detail() + " text= " + t;
        }
    }
    
    private class TagAnnotate extends Tag
    {
        String text;
        short obj_tagno;
        short obj_refno;
        
        TagAnnotate(final short code) throws IOException {
            super(code);
        }
        
        @Override
        void read() throws IOException {
            H4header.this.raf.seek(this.offset);
            this.obj_tagno = H4header.this.raf.readShort();
            this.obj_refno = H4header.this.raf.readShort();
            this.text = H4header.this.readString(this.length - 4).trim();
        }
        
        @Override
        public String detail() {
            final String t = (this.text.length() < 60) ? this.text : this.text.substring(0, 59);
            return super.detail() + " for=" + this.obj_refno + "/" + this.obj_tagno + " text=" + t;
        }
    }
    
    private class TagNumberType extends Tag
    {
        byte version;
        byte type;
        byte nbits;
        byte type_class;
        
        TagNumberType(final short code) throws IOException {
            super(code);
        }
        
        @Override
        void read() throws IOException {
            H4header.this.raf.seek(this.offset);
            this.version = H4header.this.raf.readByte();
            this.type = H4header.this.raf.readByte();
            this.nbits = H4header.this.raf.readByte();
            this.type_class = H4header.this.raf.readByte();
        }
        
        @Override
        public String detail() {
            return super.detail() + " version=" + this.version + " type=" + this.type + " nbits=" + this.nbits + " type_class=" + this.type_class;
        }
        
        @Override
        public String toString() {
            return super.toString() + " type=" + H4type.setDataType(this.type, null) + " nbits=" + this.nbits;
        }
    }
    
    private class TagRIDimension extends Tag
    {
        int xdim;
        int ydim;
        short nt_ref;
        short nelems;
        short interlace;
        short compress;
        short compress_ref;
        List<Dimension> dims;
        
        TagRIDimension(final short code) throws IOException {
            super(code);
        }
        
        @Override
        void read() throws IOException {
            H4header.this.raf.seek(this.offset);
            this.xdim = H4header.this.raf.readInt();
            this.ydim = H4header.this.raf.readInt();
            H4header.this.raf.skipBytes(2);
            this.nt_ref = H4header.this.raf.readShort();
            this.nelems = H4header.this.raf.readShort();
            this.interlace = H4header.this.raf.readShort();
            this.compress = H4header.this.raf.readShort();
            this.compress_ref = H4header.this.raf.readShort();
        }
        
        @Override
        public String detail() {
            return super.detail() + " xdim=" + this.xdim + " ydim=" + this.ydim + " nelems=" + this.nelems + " nt_ref=" + this.nt_ref + " interlace=" + this.interlace + " compress=" + this.compress;
        }
    }
    
    private class TagRIPalette extends Tag
    {
        int[] table;
        
        TagRIPalette(final short code) throws IOException {
            super(code);
        }
        
        void read(final int nx, final int ny) throws IOException {
            H4header.this.raf.seek(this.offset);
            this.table = new int[nx * ny];
            H4header.this.raf.readInt(this.table, 0, nx * ny);
        }
    }
    
    private class TagSDDimension extends Tag
    {
        short rank;
        short nt_ref;
        int[] shape;
        short[] nt_ref_scale;
        List<Dimension> dims;
        
        TagSDDimension(final short code) throws IOException {
            super(code);
        }
        
        @Override
        void read() throws IOException {
            H4header.this.raf.seek(this.offset);
            this.rank = H4header.this.raf.readShort();
            this.shape = new int[this.rank];
            for (int i = 0; i < this.rank; ++i) {
                this.shape[i] = H4header.this.raf.readInt();
            }
            H4header.this.raf.skipBytes(2);
            this.nt_ref = H4header.this.raf.readShort();
            this.nt_ref_scale = new short[this.rank];
            for (int i = 0; i < this.rank; ++i) {
                H4header.this.raf.skipBytes(2);
                this.nt_ref_scale[i] = H4header.this.raf.readShort();
            }
        }
        
        @Override
        public String detail() {
            final StringBuilder sbuff = new StringBuilder(super.detail());
            sbuff.append("   dims= ");
            for (int i = 0; i < this.rank; ++i) {
                sbuff.append(this.shape[i]).append(" ");
            }
            sbuff.append("   nt= ").append(this.nt_ref).append(" nt_scale=");
            for (int i = 0; i < this.rank; ++i) {
                sbuff.append(this.nt_ref_scale[i]).append(" ");
            }
            return sbuff.toString();
        }
        
        @Override
        public String toString() {
            final StringBuilder sbuff = new StringBuilder(super.toString());
            sbuff.append("   dims= ");
            for (int i = 0; i < this.rank; ++i) {
                sbuff.append(this.shape[i]).append(" ");
            }
            sbuff.append("   nt= ").append(this.nt_ref).append(" nt_scale=");
            for (int i = 0; i < this.rank; ++i) {
                sbuff.append(this.nt_ref_scale[i]).append(" ");
            }
            return sbuff.toString();
        }
    }
    
    private class TagTextN extends Tag
    {
        String[] text;
        
        TagTextN(final short code) throws IOException {
            super(code);
        }
        
        private List<String> getList() {
            final List<String> result = new ArrayList<String>(this.text.length);
            for (final String s : this.text) {
                if (s.trim().length() > 0) {
                    result.add(s.trim());
                }
            }
            return result;
        }
        
        void read(final int n) throws IOException {
            this.text = new String[n];
            H4header.this.raf.seek(this.offset);
            final byte[] b = new byte[this.length];
            H4header.this.raf.read(b);
            int count = 0;
            int start = 0;
            for (int i = 0; i < this.length; ++i) {
                if (b[i] == 0) {
                    this.text[count] = new String(b, start, i - start, "UTF-8");
                    if (++count == n) {
                        break;
                    }
                    start = i + 1;
                }
            }
        }
    }
    
    private class TagSDminmax extends Tag
    {
        ByteBuffer bb;
        DataType dt;
        
        TagSDminmax(final short code) throws IOException {
            super(code);
        }
        
        @Override
        void read() throws IOException {
            H4header.this.raf.seek(this.offset);
            final byte[] buff = new byte[this.length];
            H4header.this.raf.read(buff);
            this.bb = ByteBuffer.wrap(buff);
        }
        
        Number getMin(final DataType dataType) {
            this.dt = dataType;
            return this.get(dataType, 1);
        }
        
        Number getMax(final DataType dataType) {
            this.dt = dataType;
            return this.get(dataType, 0);
        }
        
        Number get(final DataType dataType, final int index) {
            if (dataType == DataType.BYTE) {
                return this.bb.get(index);
            }
            if (dataType == DataType.SHORT) {
                return this.bb.asShortBuffer().get(index);
            }
            if (dataType == DataType.INT) {
                return this.bb.asIntBuffer().get(index);
            }
            if (dataType == DataType.LONG) {
                return this.bb.asLongBuffer().get(index);
            }
            if (dataType == DataType.FLOAT) {
                return this.bb.asFloatBuffer().get(index);
            }
            if (dataType == DataType.DOUBLE) {
                return this.bb.asDoubleBuffer().get(index);
            }
            return Double.NaN;
        }
        
        @Override
        public String detail() {
            final StringBuilder sbuff = new StringBuilder(super.detail());
            sbuff.append("   min= ").append(this.getMin(this.dt));
            sbuff.append("   max= ").append(this.getMax(this.dt));
            return sbuff.toString();
        }
    }
    
    private class TagGroup extends Tag
    {
        int nelems;
        short[] elem_tag;
        short[] elem_ref;
        
        TagGroup(final short code) throws IOException {
            super(code);
        }
        
        @Override
        void read() throws IOException {
            H4header.this.raf.seek(this.offset);
            this.nelems = this.length / 4;
            this.elem_tag = new short[this.nelems];
            this.elem_ref = new short[this.nelems];
            for (int i = 0; i < this.nelems; ++i) {
                this.elem_tag[i] = H4header.this.raf.readShort();
                this.elem_ref[i] = H4header.this.raf.readShort();
            }
        }
        
        @Override
        public String detail() {
            final StringBuilder sbuff = new StringBuilder(super.detail());
            sbuff.append("\n");
            sbuff.append("   tag ref\n   ");
            for (int i = 0; i < this.nelems; ++i) {
                sbuff.append(this.elem_tag[i]).append(" ");
                sbuff.append(this.elem_ref[i]).append(" ");
                sbuff.append("\n   ");
            }
            return sbuff.toString();
        }
    }
    
    private class TagVGroup extends TagGroup
    {
        short extag;
        short exref;
        short version;
        String name;
        String className;
        Group group;
        
        TagVGroup(final short code) throws IOException {
            super(code);
        }
        
        @Override
        void read() throws IOException {
            H4header.this.raf.seek(this.offset);
            this.nelems = H4header.this.raf.readShort();
            this.elem_tag = new short[this.nelems];
            for (int i = 0; i < this.nelems; ++i) {
                this.elem_tag[i] = H4header.this.raf.readShort();
            }
            this.elem_ref = new short[this.nelems];
            for (int i = 0; i < this.nelems; ++i) {
                this.elem_ref[i] = H4header.this.raf.readShort();
            }
            short len = H4header.this.raf.readShort();
            this.name = H4header.this.readString(len);
            len = H4header.this.raf.readShort();
            this.className = H4header.this.readString(len);
            this.extag = H4header.this.raf.readShort();
            this.exref = H4header.this.raf.readShort();
            this.version = H4header.this.raf.readShort();
        }
        
        @Override
        public String toString() {
            return super.toString() + " class= " + this.className + " name= " + this.name;
        }
        
        @Override
        public String detail() {
            final StringBuilder sbuff = new StringBuilder();
            sbuff.append(this.used ? " " : "*").append("refno=").append(this.refno).append(" tag= ").append(this.t).append(this.extended ? " EXTENDED" : "").append(" offset=").append(this.offset).append(" length=").append(this.length).append((this.vinfo != null && this.vinfo.v != null) ? (" VV=" + this.vinfo.v.getName()) : "");
            sbuff.append(" class= ").append(this.className);
            sbuff.append(" extag= ").append(this.extag);
            sbuff.append(" exref= ").append(this.exref);
            sbuff.append(" version= ").append(this.version);
            sbuff.append("\n");
            sbuff.append(" name= ").append(this.name);
            sbuff.append("\n");
            sbuff.append("   tag ref\n   ");
            for (int i = 0; i < this.nelems; ++i) {
                sbuff.append(this.elem_tag[i]).append(" ");
                sbuff.append(this.elem_ref[i]).append(" ");
                sbuff.append("\n   ");
            }
            return sbuff.toString();
        }
    }
    
    private class TagVH extends Tag
    {
        short interlace;
        short nfields;
        short extag;
        short exref;
        short version;
        int ivsize;
        short[] fld_type;
        short[] fld_order;
        int[] fld_isize;
        int[] fld_offset;
        String[] fld_name;
        int nvert;
        String name;
        String className;
        int tag_len;
        
        TagVH(final short code) throws IOException {
            super(code);
        }
        
        @Override
        void read() throws IOException {
            H4header.this.raf.seek(this.offset);
            this.interlace = H4header.this.raf.readShort();
            this.nvert = H4header.this.raf.readInt();
            this.ivsize = DataType.unsignedShortToInt(H4header.this.raf.readShort());
            this.nfields = H4header.this.raf.readShort();
            this.fld_type = new short[this.nfields];
            for (int i = 0; i < this.nfields; ++i) {
                this.fld_type[i] = H4header.this.raf.readShort();
            }
            this.fld_isize = new int[this.nfields];
            for (int i = 0; i < this.nfields; ++i) {
                this.fld_isize[i] = DataType.unsignedShortToInt(H4header.this.raf.readShort());
            }
            this.fld_offset = new int[this.nfields];
            for (int i = 0; i < this.nfields; ++i) {
                this.fld_offset[i] = DataType.unsignedShortToInt(H4header.this.raf.readShort());
            }
            this.fld_order = new short[this.nfields];
            for (int i = 0; i < this.nfields; ++i) {
                this.fld_order[i] = H4header.this.raf.readShort();
            }
            this.fld_name = new String[this.nfields];
            for (int i = 0; i < this.nfields; ++i) {
                final short len = H4header.this.raf.readShort();
                this.fld_name[i] = H4header.this.readString(len);
            }
            short len2 = H4header.this.raf.readShort();
            this.name = H4header.this.readString(len2);
            len2 = H4header.this.raf.readShort();
            this.className = H4header.this.readString(len2);
            this.extag = H4header.this.raf.readShort();
            this.exref = H4header.this.raf.readShort();
            this.version = H4header.this.raf.readShort();
            this.tag_len = (int)(H4header.this.raf.getFilePointer() - this.offset);
        }
        
        @Override
        public String toString() {
            return super.toString() + " class= " + this.className + " name= " + this.name;
        }
        
        @Override
        public String detail() {
            final StringBuilder sbuff = new StringBuilder(super.detail());
            sbuff.append(" class= ").append(this.className);
            sbuff.append(" interlace= ").append(this.interlace);
            sbuff.append(" nvert= ").append(this.nvert);
            sbuff.append(" ivsize= ").append(this.ivsize);
            sbuff.append(" extag= ").append(this.extag);
            sbuff.append(" exref= ").append(this.exref);
            sbuff.append(" version= ").append(this.version);
            sbuff.append(" tag_len= ").append(this.tag_len);
            sbuff.append("\n");
            sbuff.append(" name= ").append(this.name);
            sbuff.append("\n");
            sbuff.append("   name    type  isize  offset  order\n   ");
            for (int i = 0; i < this.nfields; ++i) {
                sbuff.append(this.fld_name[i]).append(" ");
                sbuff.append(this.fld_type[i]).append(" ");
                sbuff.append(this.fld_isize[i]).append(" ");
                sbuff.append(this.fld_offset[i]).append(" ");
                sbuff.append(this.fld_order[i]).append(" ");
                sbuff.append("\n   ");
            }
            return sbuff.toString();
        }
    }
    
    private class MemTracker
    {
        private List<Mem> memList;
        private StringBuilder sbuff;
        private long fileSize;
        
        MemTracker(final long fileSize) {
            this.memList = new ArrayList<Mem>();
            this.sbuff = new StringBuilder();
            this.fileSize = fileSize;
        }
        
        void add(final String name, final long start, final long end) {
            this.memList.add(new Mem(name, start, end));
        }
        
        void addByLen(final String name, final long start, final long size) {
            this.memList.add(new Mem(name, start, start + size));
        }
        
        void report() {
            H4header.this.debugOut.println("======================================");
            H4header.this.debugOut.println("Memory used file size= " + this.fileSize);
            H4header.this.debugOut.println("  start    end   size   name");
            Collections.sort(this.memList);
            Mem prev = null;
            for (final Mem m : this.memList) {
                if (prev != null && m.start > prev.end) {
                    this.doOne('+', prev.end, m.start, m.start - prev.end, "*hole*");
                }
                final char c = (prev != null && prev.end != m.start) ? '*' : ' ';
                this.doOne(c, m.start, m.end, m.end - m.start, m.name);
                prev = m;
            }
            H4header.this.debugOut.println();
        }
        
        private void doOne(final char c, final long start, final long end, final long size, final String name) {
            this.sbuff.setLength(0);
            this.sbuff.append(c);
            this.sbuff.append(Format.l(start, 6));
            this.sbuff.append(" ");
            this.sbuff.append(Format.l(end, 6));
            this.sbuff.append(" ");
            this.sbuff.append(Format.l(size, 6));
            this.sbuff.append(" ");
            this.sbuff.append(name);
            H4header.this.debugOut.println(this.sbuff.toString());
        }
        
        class Mem implements Comparable
        {
            public String name;
            public long start;
            public long end;
            
            public Mem(final String name, final long start, final long end) {
                this.name = name;
                this.start = start;
                this.end = end;
            }
            
            public int compareTo(final Object o1) {
                final Mem m = (Mem)o1;
                return (int)(this.start - m.start);
            }
        }
    }
    
    static class MyNetcdfFile extends NetcdfFile
    {
    }
}
