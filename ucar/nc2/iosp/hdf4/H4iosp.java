// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.iosp.hdf4;

import java.nio.ByteBuffer;
import java.io.OutputStream;
import ucar.nc2.util.IO;
import java.io.ByteArrayOutputStream;
import java.util.List;
import org.slf4j.LoggerFactory;
import java.util.zip.InflaterInputStream;
import java.io.ByteArrayInputStream;
import java.util.Iterator;
import ucar.ma2.ArrayStructureBB;
import ucar.ma2.StructureMembers;
import ucar.ma2.ArrayStructure;
import ucar.ma2.InvalidRangeException;
import ucar.nc2.iosp.LayoutBB;
import java.io.InputStream;
import ucar.nc2.iosp.Layout;
import ucar.nc2.iosp.LayoutBBTiled;
import ucar.unidata.io.PositioningDataInputStream;
import ucar.nc2.iosp.LayoutTiled;
import ucar.nc2.iosp.LayoutSegmented;
import ucar.nc2.iosp.LayoutRegular;
import ucar.ma2.DataType;
import ucar.nc2.iosp.IospHelper;
import ucar.nc2.Structure;
import ucar.ma2.Array;
import ucar.ma2.Section;
import ucar.nc2.Variable;
import ucar.nc2.util.CancelTask;
import ucar.nc2.NetcdfFile;
import java.io.IOException;
import ucar.unidata.io.RandomAccessFile;
import org.slf4j.Logger;
import ucar.nc2.iosp.AbstractIOServiceProvider;

public class H4iosp extends AbstractIOServiceProvider
{
    private static Logger log;
    private static boolean showLayoutTypes;
    private H4header header;
    
    public H4iosp() {
        this.header = new H4header();
    }
    
    public boolean isValidFile(final RandomAccessFile raf) throws IOException {
        return H4header.isValidFile(raf);
    }
    
    public String getFileTypeId() {
        return "HDF4";
    }
    
    public String getFileTypeDescription() {
        return "Hierarchical Data Format, version 4";
    }
    
    @Override
    public void open(final RandomAccessFile raf, final NetcdfFile ncfile, final CancelTask cancelTask) throws IOException {
        this.raf = raf;
        this.header.read(raf, ncfile);
        ncfile.finish();
    }
    
    public Array readData(final Variable v, Section section) throws IOException, InvalidRangeException {
        if (v instanceof Structure) {
            return this.readStructureData((Structure)v, section);
        }
        final H4header.Vinfo vinfo = (H4header.Vinfo)v.getSPobject();
        final DataType dataType = v.getDataType();
        vinfo.setLayoutInfo();
        section = Section.fill(section, v.getShape());
        if (vinfo.hasNoData) {
            Object arr = (vinfo.fillValue == null) ? IospHelper.makePrimitiveArray((int)section.computeSize(), dataType) : IospHelper.makePrimitiveArray((int)section.computeSize(), dataType, vinfo.fillValue);
            if (dataType == DataType.CHAR) {
                arr = IospHelper.convertByteToChar((byte[])arr);
            }
            return Array.factory(dataType.getPrimitiveClassType(), section.getShape(), arr);
        }
        if (!vinfo.isCompressed) {
            if (!vinfo.isLinked && !vinfo.isChunked) {
                final Layout layout = new LayoutRegular(vinfo.start, v.getElementSize(), v.getShape(), section);
                final Object data = IospHelper.readDataFill(this.raf, layout, dataType, vinfo.fillValue, -1);
                return Array.factory(dataType.getPrimitiveClassType(), section.getShape(), data);
            }
            if (vinfo.isLinked) {
                final Layout layout = new LayoutSegmented(vinfo.segPos, vinfo.segSize, v.getElementSize(), v.getShape(), section);
                final Object data = IospHelper.readDataFill(this.raf, layout, dataType, vinfo.fillValue, -1);
                return Array.factory(dataType.getPrimitiveClassType(), section.getShape(), data);
            }
            if (vinfo.isChunked) {
                final H4ChunkIterator chunkIterator = new H4ChunkIterator(vinfo);
                final Layout layout2 = new LayoutTiled(chunkIterator, vinfo.chunkSize, v.getElementSize(), section);
                final Object data2 = IospHelper.readDataFill(this.raf, layout2, dataType, vinfo.fillValue, -1);
                return Array.factory(dataType.getPrimitiveClassType(), section.getShape(), data2);
            }
        }
        else {
            if (!vinfo.isLinked && !vinfo.isChunked) {
                if (H4iosp.showLayoutTypes) {
                    System.out.println("***notLinked, compressed");
                }
                final Layout index = new LayoutRegular(0L, v.getElementSize(), v.getShape(), section);
                final InputStream is = this.getCompressedInputStream(vinfo);
                final PositioningDataInputStream dataSource = new PositioningDataInputStream(is);
                final Object data3 = IospHelper.readDataFill(dataSource, index, dataType, vinfo.fillValue);
                return Array.factory(dataType.getPrimitiveClassType(), section.getShape(), data3);
            }
            if (vinfo.isLinked) {
                if (H4iosp.showLayoutTypes) {
                    System.out.println("***Linked, compressed");
                }
                final Layout index = new LayoutRegular(0L, v.getElementSize(), v.getShape(), section);
                final InputStream is = this.getLinkedCompressedInputStream(vinfo);
                final PositioningDataInputStream dataSource = new PositioningDataInputStream(is);
                final Object data3 = IospHelper.readDataFill(dataSource, index, dataType, vinfo.fillValue);
                return Array.factory(dataType.getPrimitiveClassType(), section.getShape(), data3);
            }
            if (vinfo.isChunked) {
                final LayoutBBTiled.DataChunkIterator chunkIterator2 = new H4CompressedChunkIterator(vinfo);
                final LayoutBB layout3 = new LayoutBBTiled(chunkIterator2, vinfo.chunkSize, v.getElementSize(), section);
                final Object data2 = IospHelper.readDataFill(layout3, dataType, vinfo.fillValue);
                return Array.factory(dataType.getPrimitiveClassType(), section.getShape(), data2);
            }
        }
        throw new IllegalStateException();
    }
    
    private ArrayStructure readStructureData(final Structure s, final Section section) throws IOException, InvalidRangeException {
        final H4header.Vinfo vinfo = (H4header.Vinfo)s.getSPobject();
        vinfo.setLayoutInfo();
        final int recsize = vinfo.elemSize;
        final StructureMembers members = s.makeStructureMembers();
        for (final StructureMembers.Member m : members.getMembers()) {
            final Variable v2 = s.findVariable(m.getName());
            final H4header.Minfo minfo = (H4header.Minfo)v2.getSPobject();
            m.setDataParam(minfo.offset);
        }
        members.setStructureSize(recsize);
        final ArrayStructureBB structureArray = new ArrayStructureBB(members, section.getShape());
        final byte[] result = structureArray.getByteBuffer().array();
        if (!vinfo.isLinked && !vinfo.isCompressed) {
            final Layout layout = new LayoutRegular(vinfo.start, recsize, s.getShape(), section);
            IospHelper.readData(this.raf, layout, DataType.STRUCTURE, result, -1, true);
        }
        else if (vinfo.isLinked && !vinfo.isCompressed) {
            final InputStream is = new LinkedInputStream(vinfo);
            final PositioningDataInputStream dataSource = new PositioningDataInputStream(is);
            final Layout layout2 = new LayoutRegular(0L, recsize, s.getShape(), section);
            IospHelper.readData(dataSource, layout2, DataType.STRUCTURE, result);
        }
        else if (!vinfo.isLinked && vinfo.isCompressed) {
            final InputStream is = this.getCompressedInputStream(vinfo);
            final PositioningDataInputStream dataSource = new PositioningDataInputStream(is);
            final Layout layout2 = new LayoutRegular(0L, recsize, s.getShape(), section);
            IospHelper.readData(dataSource, layout2, DataType.STRUCTURE, result);
        }
        else {
            if (!vinfo.isLinked || !vinfo.isCompressed) {
                throw new IllegalStateException();
            }
            final InputStream is = this.getLinkedCompressedInputStream(vinfo);
            final PositioningDataInputStream dataSource = new PositioningDataInputStream(is);
            final Layout layout2 = new LayoutRegular(0L, recsize, s.getShape(), section);
            IospHelper.readData(dataSource, layout2, DataType.STRUCTURE, result);
        }
        return structureArray;
    }
    
    @Override
    public String toStringDebug(final Object o) {
        if (o instanceof Variable) {
            final Variable v = (Variable)o;
            final H4header.Vinfo vinfo = (H4header.Vinfo)v.getSPobject();
            return (vinfo != null) ? vinfo.toString() : "";
        }
        return null;
    }
    
    private InputStream getCompressedInputStream(final H4header.Vinfo vinfo) throws IOException {
        final byte[] buffer = new byte[vinfo.length];
        this.raf.seek(vinfo.start);
        this.raf.readFully(buffer);
        final ByteArrayInputStream in = new ByteArrayInputStream(buffer);
        return new InflaterInputStream(in);
    }
    
    private InputStream getLinkedCompressedInputStream(final H4header.Vinfo vinfo) {
        return new InflaterInputStream(new LinkedInputStream(vinfo));
    }
    
    private InputStream getChunkedInputStream(final H4header.Vinfo vinfo) {
        return new ChunkedInputStream(vinfo);
    }
    
    @Override
    public Object sendIospMessage(final Object message) {
        if (message.toString().equals("header")) {
            return this.header;
        }
        return super.sendIospMessage(message);
    }
    
    static {
        H4iosp.log = LoggerFactory.getLogger(H4iosp.class);
        H4iosp.showLayoutTypes = false;
    }
    
    private class LinkedInputStream extends InputStream
    {
        byte[] buffer;
        long pos;
        int nsegs;
        long[] segPosA;
        int[] segSizeA;
        int segno;
        int segpos;
        int segSize;
        
        LinkedInputStream(final H4header.Vinfo vinfo) {
            this.pos = 0L;
            this.segno = -1;
            this.segpos = 0;
            this.segSize = 0;
            this.segPosA = vinfo.segPos;
            this.segSizeA = vinfo.segSize;
            this.nsegs = this.segSizeA.length;
        }
        
        LinkedInputStream(final H4header.SpecialLinked linked) throws IOException {
            this.pos = 0L;
            this.segno = -1;
            this.segpos = 0;
            this.segSize = 0;
            final List<H4header.TagLinkedBlock> linkedBlocks = linked.getLinkedDataBlocks();
            this.nsegs = linkedBlocks.size();
            this.segPosA = new long[this.nsegs];
            this.segSizeA = new int[this.nsegs];
            int count = 0;
            for (final H4header.TagLinkedBlock tag : linkedBlocks) {
                this.segPosA[count] = tag.offset;
                this.segSizeA[count] = tag.length;
                ++count;
            }
        }
        
        private boolean readSegment() throws IOException {
            ++this.segno;
            if (this.segno == this.nsegs) {
                return false;
            }
            this.segSize = this.segSizeA[this.segno];
            while (this.segSize == 0) {
                ++this.segno;
                if (this.segno == this.nsegs) {
                    return false;
                }
                this.segSize = this.segSizeA[this.segno];
            }
            this.buffer = new byte[this.segSize];
            H4iosp.this.raf.seek(this.segPosA[this.segno]);
            H4iosp.this.raf.readFully(this.buffer);
            this.segpos = 0;
            return true;
        }
        
        @Override
        public int read() throws IOException {
            if (this.segpos == this.segSize) {
                final boolean ok = this.readSegment();
                if (!ok) {
                    return -1;
                }
            }
            final int b = this.buffer[this.segpos] & 0xFF;
            ++this.segpos;
            return b;
        }
    }
    
    private class ChunkedInputStream extends InputStream
    {
        List<H4header.DataChunk> chunks;
        int chunkNo;
        byte[] buffer;
        int segPos;
        int segSize;
        
        ChunkedInputStream(final H4header.Vinfo vinfo) {
            this.chunks = vinfo.chunks;
            this.chunkNo = 0;
        }
        
        private void readChunk() throws IOException {
            final H4header.DataChunk chunk = this.chunks.get(this.chunkNo);
            final H4header.TagData chunkData = chunk.data;
            ++this.chunkNo;
            if (chunkData.ext_type == TagEnum.SPECIAL_COMP) {
                final H4header.TagData cdata = chunkData.compress.getDataTag();
                final byte[] cbuffer = new byte[cdata.length];
                H4iosp.this.raf.seek(cdata.offset);
                H4iosp.this.raf.readFully(cbuffer);
                if (chunkData.compress.compress_type != TagEnum.COMP_CODE_DEFLATE) {
                    throw new IllegalStateException("unknown compression type =" + chunkData.compress.compress_type);
                }
                final InputStream in = new InflaterInputStream(new ByteArrayInputStream(cbuffer));
                final ByteArrayOutputStream out = new ByteArrayOutputStream(chunkData.compress.uncomp_length);
                IO.copy(in, out);
                this.buffer = out.toByteArray();
            }
            else {
                this.buffer = new byte[chunkData.length];
                H4iosp.this.raf.seek(chunkData.offset);
                H4iosp.this.raf.readFully(this.buffer);
            }
            this.segPos = 0;
            this.segSize = this.buffer.length;
        }
        
        @Override
        public int read() throws IOException {
            if (this.segPos == this.segSize) {
                this.readChunk();
            }
            final int b = this.buffer[this.segPos] & 0xFF;
            ++this.segPos;
            return b;
        }
    }
    
    private class H4ChunkIterator implements LayoutTiled.DataChunkIterator
    {
        List<H4header.DataChunk> chunks;
        int chunkNo;
        byte[] buffer;
        int segPos;
        int segSize;
        
        H4ChunkIterator(final H4header.Vinfo vinfo) {
            this.chunks = vinfo.chunks;
            this.chunkNo = 0;
        }
        
        public boolean hasNext() {
            return this.chunkNo < this.chunks.size();
        }
        
        public LayoutTiled.DataChunk next() throws IOException {
            final H4header.DataChunk chunk = this.chunks.get(this.chunkNo);
            final H4header.TagData chunkData = chunk.data;
            ++this.chunkNo;
            return new LayoutTiled.DataChunk(chunk.origin, chunkData.offset);
        }
    }
    
    private class H4CompressedChunkIterator implements LayoutBBTiled.DataChunkIterator
    {
        List<H4header.DataChunk> chunks;
        int chunkNo;
        
        H4CompressedChunkIterator(final H4header.Vinfo vinfo) {
            this.chunks = vinfo.chunks;
            this.chunkNo = 0;
        }
        
        public boolean hasNext() {
            return this.chunkNo < this.chunks.size();
        }
        
        public LayoutBBTiled.DataChunk next() throws IOException {
            final H4header.DataChunk chunk = this.chunks.get(this.chunkNo);
            final H4header.TagData chunkData = chunk.data;
            assert chunkData.ext_type == TagEnum.SPECIAL_COMP;
            ++this.chunkNo;
            return new H4iosp.DataChunk(chunk.origin, chunkData.compress);
        }
    }
    
    private class DataChunk implements LayoutBBTiled.DataChunk
    {
        private int[] offset;
        private H4header.SpecialComp compress;
        private ByteBuffer bb;
        
        public DataChunk(final int[] offset, final H4header.SpecialComp compress) {
            this.offset = offset;
            this.compress = compress;
        }
        
        public int[] getOffset() {
            return this.offset;
        }
        
        public ByteBuffer getByteBuffer() throws IOException {
            if (this.bb == null) {
                final H4header.TagData cdata = this.compress.getDataTag();
                InputStream in;
                if (cdata.linked == null) {
                    final byte[] cbuffer = new byte[cdata.length];
                    H4iosp.this.raf.seek(cdata.offset);
                    H4iosp.this.raf.readFully(cbuffer);
                    in = new ByteArrayInputStream(cbuffer);
                }
                else {
                    in = new LinkedInputStream(cdata.linked);
                }
                if (this.compress.compress_type == TagEnum.COMP_CODE_DEFLATE) {
                    final InputStream zin = new InflaterInputStream(in);
                    final ByteArrayOutputStream out = new ByteArrayOutputStream(this.compress.uncomp_length);
                    IO.copy(zin, out);
                    final byte[] buffer = out.toByteArray();
                    this.bb = ByteBuffer.wrap(buffer);
                }
                else {
                    if (this.compress.compress_type != TagEnum.COMP_CODE_NONE) {
                        throw new IllegalStateException("unknown compression type =" + this.compress.compress_type);
                    }
                    final ByteArrayOutputStream out2 = new ByteArrayOutputStream(this.compress.uncomp_length);
                    IO.copy(in, out2);
                    final byte[] buffer2 = out2.toByteArray();
                    this.bb = ByteBuffer.wrap(buffer2);
                }
            }
            return this.bb;
        }
    }
}
