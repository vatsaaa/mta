// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.iosp.hdf4;

import org.slf4j.LoggerFactory;
import org.jdom.Content;
import java.util.StringTokenizer;
import org.jdom.Element;
import ucar.nc2.util.IO;
import java.io.IOException;
import org.jdom.output.XMLOutputter;
import org.jdom.output.Format;
import java.io.OutputStream;
import org.jdom.Document;
import org.slf4j.Logger;

public class ODLparser
{
    private static Logger log;
    private Document doc;
    private boolean debug;
    private boolean showRaw;
    private boolean show;
    
    public ODLparser() {
        this.debug = false;
        this.showRaw = false;
        this.show = false;
    }
    
    void showDoc(final OutputStream out) {
        final XMLOutputter fmt = new XMLOutputter(Format.getPrettyFormat());
        try {
            fmt.output(this.doc, out);
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    void parseFile(final String filename) throws IOException {
        final String text = new String(IO.readFileToByteArray(filename));
        this.parseFromString(text);
    }
    
    public Element parseFromString(final String text) throws IOException {
        if (this.showRaw) {
            System.out.println("Raw ODL=\n" + text);
        }
        final Element rootElem = new Element("odl");
        this.doc = new Document(rootElem);
        Element current = rootElem;
        final StringTokenizer lineFinder = new StringTokenizer(text, "\t\n\r\f");
        while (lineFinder.hasMoreTokens()) {
            final String line = lineFinder.nextToken();
            if (line.startsWith("GROUP")) {
                current = this.startGroup(current, line);
            }
            else if (line.startsWith("OBJECT")) {
                current = this.startObject(current, line);
            }
            else if (line.startsWith("END_OBJECT")) {
                this.endObject(current, line);
                current = current.getParentElement();
            }
            else if (line.startsWith("END_GROUP")) {
                this.endGroup(current, line);
                current = current.getParentElement();
            }
            else {
                this.addField(current, line);
            }
        }
        if (this.show) {
            this.showDoc(System.out);
        }
        return rootElem;
    }
    
    Element startGroup(final Element parent, final String line) throws IOException {
        final StringTokenizer stoke = new StringTokenizer(line, "=");
        final String toke = stoke.nextToken();
        assert toke.equals("GROUP");
        final String name = stoke.nextToken();
        final Element group = new Element(name);
        parent.addContent(group);
        return group;
    }
    
    void endGroup(final Element current, final String line) throws IOException {
        final StringTokenizer stoke = new StringTokenizer(line, "=");
        final String toke = stoke.nextToken();
        assert toke.equals("END_GROUP");
        final String name = stoke.nextToken();
        if (this.debug) {
            System.out.println(line + " -> " + current);
        }
        assert name.equals(current.getName());
    }
    
    Element startObject(final Element parent, final String line) throws IOException {
        final StringTokenizer stoke = new StringTokenizer(line, "=");
        final String toke = stoke.nextToken();
        assert toke.equals("OBJECT");
        final String name = stoke.nextToken();
        final Element obj = new Element(name);
        parent.addContent(obj);
        return obj;
    }
    
    void endObject(final Element current, final String line) throws IOException {
        final StringTokenizer stoke = new StringTokenizer(line, "=");
        final String toke = stoke.nextToken();
        assert toke.equals("END_OBJECT");
        final String name = stoke.nextToken();
        if (this.debug) {
            System.out.println(line + " -> " + current);
        }
        assert name.equals(current.getName()) : name + " !+ " + current.getName();
    }
    
    void addField(final Element parent, final String line) throws IOException {
        final StringTokenizer stoke = new StringTokenizer(line, "=");
        final String name = stoke.nextToken();
        if (stoke.hasMoreTokens()) {
            final Element field = new Element(name);
            parent.addContent(field);
            String value = stoke.nextToken();
            if (value.startsWith("(")) {
                this.parseValueCollection(field, value);
                return;
            }
            value = this.stripQuotes(value);
            field.addContent(value);
        }
    }
    
    void parseValueCollection(final Element field, String value) {
        if (value.startsWith("(")) {
            value = value.substring(1);
        }
        if (value.endsWith(")")) {
            value = value.substring(0, value.length() - 1);
        }
        final StringTokenizer stoke = new StringTokenizer(value, "\",");
        while (stoke.hasMoreTokens()) {
            field.addContent(new Element("value").addContent(this.stripQuotes(stoke.nextToken())));
        }
    }
    
    String stripQuotes(String name) {
        if (name.startsWith("\"")) {
            name = name.substring(1);
        }
        if (name.endsWith("\"")) {
            name = name.substring(0, name.length() - 1);
        }
        return name;
    }
    
    public static void main(final String[] args) throws IOException {
        final ODLparser p = new ODLparser();
        p.parseFile("c:/temp/odl.struct.txt");
    }
    
    static {
        ODLparser.log = LoggerFactory.getLogger(ODLparser.class);
    }
}
