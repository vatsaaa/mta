// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.iosp.hdf4;

import org.slf4j.LoggerFactory;
import ucar.nc2.dataset.CoordinateSystem;
import ucar.nc2.constants.AxisType;
import ucar.ma2.DataType;
import ucar.nc2.Structure;
import ucar.nc2.Dimension;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import ucar.nc2.Attribute;
import ucar.nc2.constants.FeatureType;
import org.jdom.Element;
import ucar.ma2.Array;
import ucar.nc2.Variable;
import ucar.ma2.ArrayChar;
import java.io.OutputStream;
import java.io.ByteArrayOutputStream;
import java.util.Formatter;
import java.io.IOException;
import ucar.nc2.Group;
import ucar.nc2.NetcdfFile;
import org.slf4j.Logger;

public class HdfEos
{
    private static Logger log;
    static boolean showWork;
    private static final String GEOLOC_FIELDS = "Geolocation Fields";
    private static final String DATA_FIELDS = "Data Fields";
    
    public static boolean amendFromODL(final NetcdfFile ncfile, final Group eosGroup) throws IOException {
        final String smeta = getStructMetadata(eosGroup);
        if (smeta == null) {
            return false;
        }
        new HdfEos().amendFromODL(ncfile, smeta);
        return true;
    }
    
    public static void getEosInfo(final NetcdfFile ncfile, final Group eosGroup, final Formatter f) throws IOException {
        final String smeta = getStructMetadata(eosGroup);
        if (smeta == null) {
            f.format("No StructMetadata variables in group %s %n", eosGroup.getName());
            return;
        }
        f.format("raw = %n%s%n", smeta);
        final ODLparser parser = new ODLparser();
        parser.parseFromString(smeta.toString());
        final ByteArrayOutputStream bos = new ByteArrayOutputStream(1000000);
        parser.showDoc(bos);
        f.format("parsed = %n%s%n", bos.toString());
    }
    
    private static String getStructMetadata(final Group eosGroup) throws IOException {
        StringBuilder sbuff = null;
        String structMetadata = null;
        int n = 0;
        while (true) {
            final Variable structMetadataVar = eosGroup.findVariable("StructMetadata." + n);
            if (structMetadataVar == null) {
                break;
            }
            if (structMetadata != null && sbuff == null) {
                sbuff = new StringBuilder(64000);
                sbuff.append(structMetadata);
            }
            final Array A = structMetadataVar.read();
            final ArrayChar ca = (ArrayChar)A;
            structMetadata = ca.getString();
            if (sbuff != null) {
                sbuff.append(structMetadata);
            }
            ++n;
        }
        return (sbuff != null) ? sbuff.toString() : structMetadata;
    }
    
    private void amendFromODL(final NetcdfFile ncfile, final String structMetadata) throws IOException {
        final Group rootg = ncfile.getRootGroup();
        final ODLparser parser = new ODLparser();
        final Element root = parser.parseFromString(structMetadata);
        FeatureType featureType = null;
        final Element swathStructure = root.getChild("SwathStructure");
        if (swathStructure != null) {
            final List<Element> swaths = (List<Element>)swathStructure.getChildren();
            for (final Element elemSwath : swaths) {
                final Element swathNameElem = elemSwath.getChild("SwathName");
                if (swathNameElem == null) {
                    HdfEos.log.warn("No SwathName element in " + elemSwath.getName());
                }
                else {
                    final String swathName = swathNameElem.getText();
                    final Group swathGroup = this.findGroupNested(rootg, swathName);
                    if (swathGroup != null) {
                        featureType = this.amendSwath(ncfile, elemSwath, swathGroup);
                    }
                    else {
                        HdfEos.log.warn("Cant find swath group " + swathName);
                    }
                }
            }
        }
        final Element gridStructure = root.getChild("GridStructure");
        if (gridStructure != null) {
            final List<Element> grids = (List<Element>)gridStructure.getChildren();
            for (final Element elemGrid : grids) {
                final Element gridNameElem = elemGrid.getChild("GridName");
                if (gridNameElem == null) {
                    HdfEos.log.warn("Ne GridName element in " + elemGrid.getName());
                }
                else {
                    final String gridName = gridNameElem.getText();
                    final Group gridGroup = this.findGroupNested(rootg, gridName);
                    if (gridGroup != null) {
                        featureType = this.amendGrid(elemGrid, gridGroup);
                    }
                    else {
                        HdfEos.log.warn("Cant find Grid group " + gridName);
                    }
                }
            }
        }
        final Element pointStructure = root.getChild("PointStructure");
        if (pointStructure != null) {
            final List<Element> pts = (List<Element>)pointStructure.getChildren();
            for (final Element elem : pts) {
                final Element nameElem = elem.getChild("PointName");
                if (nameElem == null) {
                    HdfEos.log.warn("No PointName element in " + elem.getName());
                }
                else {
                    final String name = nameElem.getText();
                    final Group ptGroup = this.findGroupNested(rootg, name);
                    if (ptGroup != null) {
                        featureType = FeatureType.POINT;
                    }
                    else {
                        HdfEos.log.warn("Cant find Point group " + name);
                    }
                }
            }
        }
        if (featureType != null) {
            if (HdfEos.showWork) {
                System.out.println("***EOS featureType= " + featureType.toString());
            }
            rootg.addAttribute(new Attribute("cdm_data_type", featureType.toString()));
        }
    }
    
    private FeatureType amendSwath(final NetcdfFile ncfile, final Element swathElem, final Group parent) {
        FeatureType featureType = FeatureType.SWATH;
        final List<Dimension> unknownDims = new ArrayList<Dimension>();
        final Element d = swathElem.getChild("Dimension");
        final List<Element> dims = (List<Element>)d.getChildren();
        for (final Element elem : dims) {
            String name = elem.getChild("DimensionName").getText();
            name = H4header.createValidObjectName(name);
            if (name.equalsIgnoreCase("scalar")) {
                continue;
            }
            final String sizeS = elem.getChild("Size").getText();
            final int length = Integer.parseInt(sizeS);
            if (length > 0) {
                final Dimension dim = new Dimension(name, length);
                parent.addDimension(dim);
                if (!HdfEos.showWork) {
                    continue;
                }
                System.out.printf(" Add dimension %s %n", dim);
            }
            else {
                HdfEos.log.warn("Dimension " + name + " has size " + sizeS);
                final Dimension udim = new Dimension(name, 1);
                udim.setGroup(parent);
                unknownDims.add(udim);
                if (!HdfEos.showWork) {
                    continue;
                }
                System.out.printf(" Add dimension %s %n", udim);
            }
        }
        final Element dmap = swathElem.getChild("DimensionMap");
        final List<Element> dimMaps = (List<Element>)dmap.getChildren();
        for (final Element elem2 : dimMaps) {
            String geoDimName = elem2.getChild("GeoDimension").getText();
            geoDimName = H4header.createValidObjectName(geoDimName);
            String dataDimName = elem2.getChild("DataDimension").getText();
            dataDimName = H4header.createValidObjectName(dataDimName);
            final String offsetS = elem2.getChild("Offset").getText();
            final String incrS = elem2.getChild("Increment").getText();
            final int offset = Integer.parseInt(offsetS);
            final int incr = Integer.parseInt(incrS);
            final Variable v = new Variable(ncfile, parent, null, dataDimName);
            v.setDimensions(geoDimName);
            v.setDataType(DataType.INT);
            final int npts = (int)v.getSize();
            final Array data = Array.makeArray(v.getDataType(), npts, offset, incr);
            v.setCachedData(data, true);
            v.addAttribute(new Attribute("_DimensionMap", ""));
            parent.addVariable(v);
            if (HdfEos.showWork) {
                System.out.printf(" Add dimensionMap %s %n", v);
            }
        }
        final Group geoFieldsG = parent.findGroup("Geolocation Fields");
        if (geoFieldsG != null) {
            Variable latAxis = null;
            Variable lonAxis = null;
            final Element floc = swathElem.getChild("GeoField");
            final List<Element> varsLoc = (List<Element>)floc.getChildren();
            for (final Element elem3 : varsLoc) {
                final String varname = elem3.getChild("GeoFieldName").getText();
                final Variable v = geoFieldsG.findVariable(varname);
                assert v != null : varname;
                final AxisType axis = this.addAxisType(v);
                if (axis == AxisType.Lat) {
                    latAxis = v;
                }
                if (axis == AxisType.Lon) {
                    lonAxis = v;
                }
                final Element dimList = elem3.getChild("DimList");
                final List<Element> values = (List<Element>)dimList.getChildren("value");
                this.setSharedDimensions(v, values, unknownDims);
                if (!HdfEos.showWork) {
                    continue;
                }
                System.out.printf(" set coordinate %s %n", v);
            }
            if (latAxis != null && lonAxis != null) {
                final List<Dimension> xyDomain = CoordinateSystem.makeDomain(new Variable[] { latAxis, lonAxis });
                if (xyDomain.size() < 2) {
                    featureType = FeatureType.PROFILE;
                }
            }
        }
        final Group dataG = parent.findGroup("Data Fields");
        if (dataG != null) {
            final Element f = swathElem.getChild("DataField");
            final List<Element> vars = (List<Element>)f.getChildren();
            for (final Element elem4 : vars) {
                final Element dataFieldNameElem = elem4.getChild("DataFieldName");
                if (dataFieldNameElem == null) {
                    continue;
                }
                final String varname = dataFieldNameElem.getText();
                final Variable v = dataG.findVariable(varname);
                if (v == null) {
                    HdfEos.log.error("Cant find variable " + varname);
                }
                else {
                    final Element dimList2 = elem4.getChild("DimList");
                    final List<Element> values2 = (List<Element>)dimList2.getChildren("value");
                    this.setSharedDimensions(v, values2, unknownDims);
                }
            }
        }
        return featureType;
    }
    
    private AxisType addAxisType(final Variable v) {
        final String name = v.getShortName();
        if (name.equalsIgnoreCase("Latitude") || name.equalsIgnoreCase("GeodeticLatitude")) {
            v.addAttribute(new Attribute("_CoordinateAxisType", AxisType.Lat.toString()));
            v.addAttribute(new Attribute("units", "degrees_north"));
            return AxisType.Lat;
        }
        if (name.equalsIgnoreCase("Longitude")) {
            v.addAttribute(new Attribute("_CoordinateAxisType", AxisType.Lon.toString()));
            v.addAttribute(new Attribute("units", "degrees_east"));
            return AxisType.Lon;
        }
        if (name.equalsIgnoreCase("Time")) {
            v.addAttribute(new Attribute("_CoordinateAxisType", AxisType.Time.toString()));
            if (v.findAttribute("units") == null) {
                v.addAttribute(new Attribute("units", "secs since 1970-01-01 00:00:00"));
            }
            return AxisType.Time;
        }
        if (name.equalsIgnoreCase("Pressure")) {
            v.addAttribute(new Attribute("_CoordinateAxisType", AxisType.Pressure.toString()));
            return AxisType.Pressure;
        }
        if (name.equalsIgnoreCase("Altitude")) {
            v.addAttribute(new Attribute("_CoordinateAxisType", AxisType.Height.toString()));
            v.addAttribute(new Attribute("positive", "up"));
            return AxisType.Height;
        }
        return null;
    }
    
    private FeatureType amendGrid(final Element gridElem, final Group parent) {
        final List<Dimension> unknownDims = new ArrayList<Dimension>();
        final String xdimSizeS = gridElem.getChild("XDim").getText();
        final String ydimSizeS = gridElem.getChild("YDim").getText();
        final int xdimSize = Integer.parseInt(xdimSizeS);
        final int ydimSize = Integer.parseInt(ydimSizeS);
        parent.addDimension(new Dimension("XDim", xdimSize));
        parent.addDimension(new Dimension("YDim", ydimSize));
        final Element d = gridElem.getChild("Dimension");
        final List<Element> dims = (List<Element>)d.getChildren();
        for (final Element elem : dims) {
            String name = elem.getChild("DimensionName").getText();
            name = H4header.createValidObjectName(name);
            if (name.equalsIgnoreCase("scalar")) {
                continue;
            }
            final String sizeS = elem.getChild("Size").getText();
            final int length = Integer.parseInt(sizeS);
            final Dimension old = parent.findDimension(name);
            if (old != null && old.getLength() == length) {
                continue;
            }
            if (length > 0) {
                final Dimension dim = new Dimension(name, length);
                parent.addDimension(dim);
                if (!HdfEos.showWork) {
                    continue;
                }
                System.out.printf(" Add dimension %s %n", dim);
            }
            else {
                HdfEos.log.warn("Dimension " + name + " has size " + sizeS);
                final Dimension udim = new Dimension(name, 1);
                udim.setGroup(parent);
                unknownDims.add(udim);
                if (!HdfEos.showWork) {
                    continue;
                }
                System.out.printf(" Add dimension %s %n", udim);
            }
        }
        final Group geoFieldsG = parent.findGroup("Geolocation Fields");
        if (geoFieldsG != null) {
            final Element floc = gridElem.getChild("GeoField");
            final List<Element> varsLoc = (List<Element>)floc.getChildren();
            for (final Element elem2 : varsLoc) {
                final String varname = elem2.getChild("GeoFieldName").getText();
                final Variable v = geoFieldsG.findVariable(varname);
                assert v != null : varname;
                final Element dimList = elem2.getChild("DimList");
                final List<Element> values = (List<Element>)dimList.getChildren("value");
                this.setSharedDimensions(v, values, unknownDims);
            }
        }
        final Group dataG = parent.findGroup("Data Fields");
        if (dataG != null) {
            final Element f = gridElem.getChild("DataField");
            final List<Element> vars = (List<Element>)f.getChildren();
            for (final Element elem3 : vars) {
                final String varname2 = elem3.getChild("DataFieldName").getText();
                final Variable v2 = dataG.findVariable(varname2);
                assert v2 != null : varname2;
                final Element dimList2 = elem3.getChild("DimList");
                final List<Element> values2 = (List<Element>)dimList2.getChildren("value");
                this.setSharedDimensions(v2, values2, unknownDims);
            }
        }
        String projS = null;
        final Element projElem = gridElem.getChild("Projection");
        if (projElem != null) {
            projS = projElem.getText();
        }
        final boolean isLatLon = "GCTP_GEO".equals(projS);
        if (isLatLon) {
            for (final Variable v : dataG.getVariables()) {
                if (v.isCoordinateVariable()) {
                    if (v.getShortName().equals("YDim")) {
                        v.addAttribute(new Attribute("_CoordinateAxisType", AxisType.Lat.toString()));
                    }
                    if (!v.getShortName().equals("XDim")) {
                        continue;
                    }
                    v.addAttribute(new Attribute("_CoordinateAxisType", AxisType.Lon.toString()));
                }
            }
        }
        return FeatureType.GRID;
    }
    
    private void setSharedDimensions(final Variable v, final List<Element> values, final List<Dimension> unknownDims) {
        if (values.size() == 0) {
            return;
        }
        final Iterator<Element> iter = values.iterator();
        while (iter.hasNext()) {
            final Element value = iter.next();
            final String dimName = value.getText();
            if (dimName.equalsIgnoreCase("scalar")) {
                iter.remove();
            }
        }
        final List<Dimension> oldDims = v.getDimensions();
        if (oldDims.size() != values.size()) {
            HdfEos.log.error("Different number of dimensions for " + v);
            return;
        }
        final List<Dimension> newDims = new ArrayList<Dimension>();
        final Group group = v.getParentGroup();
        for (int i = 0; i < values.size(); ++i) {
            final Element value2 = values.get(i);
            String dimName2 = value2.getText();
            dimName2 = H4header.createValidObjectName(dimName2);
            Dimension dim = group.findDimension(dimName2);
            final Dimension oldDim = oldDims.get(i);
            if (dim == null) {
                dim = this.checkUnknownDims(dimName2, unknownDims, oldDim);
            }
            if (dim == null) {
                HdfEos.log.error("Unknown Dimension= " + dimName2 + " for variable = " + v.getName());
                return;
            }
            if (dim.getLength() != oldDim.getLength()) {
                HdfEos.log.error("Shared dimension (" + dim.getName() + ") has different length than data dimension (" + oldDim.getName() + ") shared=" + dim.getLength() + " org=" + oldDim.getLength() + " for " + v);
                return;
            }
            newDims.add(dim);
        }
        v.setDimensions(newDims);
        if (HdfEos.showWork) {
            System.out.printf(" set shared dimensions for %s %n", v.getNameAndDimensions());
        }
    }
    
    private Dimension checkUnknownDims(final String wantDim, final List<Dimension> unknownDims, final Dimension oldDim) {
        for (final Dimension dim : unknownDims) {
            if (dim.getName().equals(wantDim)) {
                final int len = oldDim.getLength();
                if (len == 0) {
                    dim.setUnlimited(true);
                }
                dim.setLength(len);
                final Group parent = dim.getGroup();
                parent.addDimension(dim);
                unknownDims.remove(dim);
                HdfEos.log.warn("unknownDim " + wantDim + " length set to " + oldDim.getLength());
                return dim;
            }
        }
        return null;
    }
    
    private Group findGroupNested(final Group parent, final String name) {
        for (final Group g : parent.getGroups()) {
            if (g.getShortName().equals(name)) {
                return g;
            }
        }
        for (final Group g : parent.getGroups()) {
            final Group result = this.findGroupNested(g, name);
            if (result != null) {
                return result;
            }
        }
        return null;
    }
    
    static {
        HdfEos.log = LoggerFactory.getLogger(HdfEos.class);
        HdfEos.showWork = false;
    }
}
