// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.iosp.hdf4;

import ucar.nc2.Attribute;
import ucar.ma2.DataType;
import ucar.nc2.Variable;

public class H4type
{
    static String getNumberType(final byte type) {
        switch (type) {
            case 0: {
                return "NONE";
            }
            case 1: {
                return "IEEE";
            }
            case 2: {
                return "VAX";
            }
            case 3: {
                return "CRAY";
            }
            case 4: {
                return "PC";
            }
            case 5: {
                return "CONVEX";
            }
            default: {
                throw new IllegalStateException("unknown type= " + type);
            }
        }
    }
    
    static DataType setDataType(final short type, final Variable v) {
        boolean unsigned = false;
        DataType dt = null;
        switch (type) {
            case 3: {
                dt = DataType.BYTE;
                unsigned = true;
                break;
            }
            case 4: {
                dt = DataType.CHAR;
                break;
            }
            case 5: {
                dt = DataType.FLOAT;
                break;
            }
            case 6: {
                dt = DataType.DOUBLE;
                break;
            }
            case 21: {
                unsigned = true;
            }
            case 20: {
                dt = DataType.BYTE;
                break;
            }
            case 23: {
                unsigned = true;
            }
            case 22: {
                dt = DataType.SHORT;
                break;
            }
            case 25: {
                unsigned = true;
            }
            case 24: {
                dt = DataType.INT;
                break;
            }
            case 27: {
                unsigned = true;
            }
            case 26: {
                dt = DataType.LONG;
                break;
            }
            default: {
                throw new IllegalStateException("unknown type= " + type);
            }
        }
        if (v != null) {
            v.setDataType(dt);
            if (unsigned) {
                v.addAttribute(new Attribute("_Unsigned", "true"));
            }
        }
        return dt;
    }
}
