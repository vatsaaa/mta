// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.iosp.hdf4;

import java.util.HashMap;
import java.io.IOException;
import java.util.StringTokenizer;
import java.io.Reader;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.FileInputStream;
import java.util.Map;

public class TagEnum
{
    private static Map<Short, TagEnum> hash;
    public static int SPECIAL_LINKED;
    public static int SPECIAL_EXT;
    public static int SPECIAL_COMP;
    public static int SPECIAL_VLINKED;
    public static int SPECIAL_CHUNKED;
    public static int SPECIAL_BUFFERED;
    public static int SPECIAL_COMPRAS;
    public static int COMP_CODE_NONE;
    public static int COMP_CODE_RLE;
    public static int COMP_CODE_NBIT;
    public static int COMP_CODE_SKPHUFF;
    public static int COMP_CODE_DEFLATE;
    public static int COMP_CODE_SZIP;
    public static final TagEnum NONE;
    public static final TagEnum NULL;
    public static final TagEnum RLE;
    public static final TagEnum IMC;
    public static final TagEnum IMCOMP;
    public static final TagEnum JPEG;
    public static final TagEnum GREYJPEG;
    public static final TagEnum JPEG5;
    public static final TagEnum GREYJPEG5;
    public static final TagEnum LINKED;
    public static final TagEnum VERSION;
    public static final TagEnum COMPRESSED;
    public static final TagEnum VLINKED;
    public static final TagEnum VLINKED_DATA;
    public static final TagEnum CHUNKED;
    public static final TagEnum CHUNK;
    public static final TagEnum FID;
    public static final TagEnum FD;
    public static final TagEnum TID;
    public static final TagEnum TD;
    public static final TagEnum DIL;
    public static final TagEnum DIA;
    public static final TagEnum NT;
    public static final TagEnum MT;
    public static final TagEnum FREE;
    public static final TagEnum ID8;
    public static final TagEnum IP8;
    public static final TagEnum RI8;
    public static final TagEnum CI8;
    public static final TagEnum II8;
    public static final TagEnum ID;
    public static final TagEnum LUT;
    public static final TagEnum RI;
    public static final TagEnum CI;
    public static final TagEnum NRI;
    public static final TagEnum RIG;
    public static final TagEnum LD;
    public static final TagEnum MD;
    public static final TagEnum MA;
    public static final TagEnum CCN;
    public static final TagEnum CFM;
    public static final TagEnum AR;
    public static final TagEnum DRAW;
    public static final TagEnum RUN;
    public static final TagEnum XYP;
    public static final TagEnum MTO;
    public static final TagEnum T14;
    public static final TagEnum T105;
    public static final TagEnum SDG;
    public static final TagEnum SDD;
    public static final TagEnum SD;
    public static final TagEnum SDS;
    public static final TagEnum SDL;
    public static final TagEnum SDU;
    public static final TagEnum SDF;
    public static final TagEnum SDM;
    public static final TagEnum SDC;
    public static final TagEnum SDT;
    public static final TagEnum SDLNK;
    public static final TagEnum NDG;
    public static final TagEnum CAL;
    public static final TagEnum FV;
    public static final TagEnum BREQ;
    public static final TagEnum SDRAG;
    public static final TagEnum EREQ;
    public static final TagEnum VG;
    public static final TagEnum VH;
    public static final TagEnum VS;
    private String name;
    private String desc;
    private short code;
    
    private TagEnum(final String name, final String desc, final short code) {
        this.name = name;
        this.desc = desc;
        this.code = code;
        TagEnum.hash.put(code, this);
    }
    
    public String getDesc() {
        return this.desc;
    }
    
    public String getName() {
        return this.name;
    }
    
    public short getCode() {
        return this.code;
    }
    
    @Override
    public String toString() {
        return this.name + " (" + this.code + ") " + this.desc;
    }
    
    public static TagEnum getTag(final short code) {
        TagEnum te = TagEnum.hash.get(code);
        if (te == null) {
            te = new TagEnum("UNKNOWN", "UNKNOWN", code);
        }
        return te;
    }
    
    public static void main(final String[] args) throws IOException {
        final FileInputStream ios = new FileInputStream("C:/dev/hdf4/HDF4.2r1/hdf/src/htags.h");
        final BufferedReader dataIS = new BufferedReader(new InputStreamReader(ios));
        while (true) {
            final String line = dataIS.readLine();
            if (line == null) {
                break;
            }
            if (!line.startsWith("#define")) {
                continue;
            }
            final StringTokenizer stoker = new StringTokenizer(line, " ()");
            stoker.nextToken();
            String name = stoker.nextToken();
            if (!stoker.hasMoreTokens()) {
                continue;
            }
            if (name.startsWith("DFTAG_")) {
                name = name.substring(6);
            }
            String code = stoker.nextToken();
            if (code.startsWith("u")) {
                code = stoker.nextToken();
            }
            final int pos = line.indexOf("/*");
            String desc = "";
            if (pos > 0) {
                final int pos2 = line.indexOf("*/");
                desc = ((pos2 > 0) ? line.substring(pos + 3, pos2) : line.substring(pos + 3));
                desc = desc.trim();
            }
            System.out.println("  public final static Tags " + name + " = new Tags(\"" + name + "\", \"" + desc + "\", (short) " + code + ");");
        }
    }
    
    static {
        TagEnum.hash = new HashMap<Short, TagEnum>(100);
        TagEnum.SPECIAL_LINKED = 1;
        TagEnum.SPECIAL_EXT = 2;
        TagEnum.SPECIAL_COMP = 3;
        TagEnum.SPECIAL_VLINKED = 4;
        TagEnum.SPECIAL_CHUNKED = 5;
        TagEnum.SPECIAL_BUFFERED = 6;
        TagEnum.SPECIAL_COMPRAS = 7;
        TagEnum.COMP_CODE_NONE = 0;
        TagEnum.COMP_CODE_RLE = 1;
        TagEnum.COMP_CODE_NBIT = 2;
        TagEnum.COMP_CODE_SKPHUFF = 3;
        TagEnum.COMP_CODE_DEFLATE = 4;
        TagEnum.COMP_CODE_SZIP = 5;
        NONE = new TagEnum("NONE", "", (short)0);
        NULL = new TagEnum("NULL", "", (short)1);
        RLE = new TagEnum("RLE", "Run length encoding", (short)11);
        IMC = new TagEnum("IMC", "IMCOMP compression alias", (short)12);
        IMCOMP = new TagEnum("IMCOMP", "IMCOMP compression", (short)12);
        JPEG = new TagEnum("JPEG", "JPEG compression (24-bit data)", (short)13);
        GREYJPEG = new TagEnum("GREYJPEG", "JPEG compression (8-bit data)", (short)14);
        JPEG5 = new TagEnum("JPEG5", "JPEG compression (24-bit data)", (short)15);
        GREYJPEG5 = new TagEnum("GREYJPEG5", "JPEG compression (8-bit data)", (short)16);
        LINKED = new TagEnum("LINKED", "Linked-block special element", (short)20);
        VERSION = new TagEnum("VERSION", "Version", (short)30);
        COMPRESSED = new TagEnum("COMPRESSED", "Compressed special element", (short)40);
        VLINKED = new TagEnum("VLINKED", "Variable-len linked-block header", (short)50);
        VLINKED_DATA = new TagEnum("VLINKED_DATA", "Variable-len linked-block data", (short)51);
        CHUNKED = new TagEnum("CHUNKED", "Chunked special element header", (short)60);
        CHUNK = new TagEnum("CHUNK", "Chunk element", (short)61);
        FID = new TagEnum("FID", "File identifier", (short)100);
        FD = new TagEnum("FD", "File description", (short)101);
        TID = new TagEnum("TID", "Tag identifier", (short)102);
        TD = new TagEnum("TD", "Tag descriptor", (short)103);
        DIL = new TagEnum("DIL", "Data identifier label", (short)104);
        DIA = new TagEnum("DIA", "Data identifier annotation", (short)105);
        NT = new TagEnum("NT", "Number type", (short)106);
        MT = new TagEnum("MT", "Machine type", (short)107);
        FREE = new TagEnum("FREE", "Free space in the file", (short)108);
        ID8 = new TagEnum("ID8", "8-bit Image dimension", (short)200);
        IP8 = new TagEnum("IP8", "8-bit Image palette", (short)201);
        RI8 = new TagEnum("RI8", "Raster-8 image", (short)202);
        CI8 = new TagEnum("CI8", "RLE compressed 8-bit image", (short)203);
        II8 = new TagEnum("II8", "IMCOMP compressed 8-bit image", (short)204);
        ID = new TagEnum("ID", "Image DimRec", (short)300);
        LUT = new TagEnum("LUT", "Image Palette", (short)301);
        RI = new TagEnum("RI", "Raster Image", (short)302);
        CI = new TagEnum("CI", "Compressed Image", (short)303);
        NRI = new TagEnum("NRI", "New-format Raster Image", (short)304);
        RIG = new TagEnum("RIG", "Raster Image Group", (short)306);
        LD = new TagEnum("LD", "Palette DimRec", (short)307);
        MD = new TagEnum("MD", "Matte DimRec", (short)308);
        MA = new TagEnum("MA", "Matte Data", (short)309);
        CCN = new TagEnum("CCN", "Color correction", (short)310);
        CFM = new TagEnum("CFM", "Color format", (short)311);
        AR = new TagEnum("AR", "Cspect ratio", (short)312);
        DRAW = new TagEnum("DRAW", "Draw these images in sequence", (short)400);
        RUN = new TagEnum("RUN", "Cun this as a program/script", (short)401);
        XYP = new TagEnum("XYP", "X-Y position", (short)500);
        MTO = new TagEnum("MTO", "Machine-type override", (short)501);
        T14 = new TagEnum("T14", "TEK 4014 data", (short)602);
        T105 = new TagEnum("T105", "TEK 4105 data", (short)603);
        SDG = new TagEnum("SDG", "Scientific Data Group", (short)700);
        SDD = new TagEnum("SDD", "Scientific Data DimRec", (short)701);
        SD = new TagEnum("SD", "Scientific Data", (short)702);
        SDS = new TagEnum("SDS", "Scales", (short)703);
        SDL = new TagEnum("SDL", "Labels", (short)704);
        SDU = new TagEnum("SDU", "Units", (short)705);
        SDF = new TagEnum("SDF", "Formats", (short)706);
        SDM = new TagEnum("SDM", "Max/Min", (short)707);
        SDC = new TagEnum("SDC", "Coord sys", (short)708);
        SDT = new TagEnum("SDT", "Transpose", (short)709);
        SDLNK = new TagEnum("SDLNK", "Links related to the dataset", (short)710);
        NDG = new TagEnum("NDG", "Numeric Data Group", (short)720);
        CAL = new TagEnum("CAL", "Calibration information", (short)731);
        FV = new TagEnum("FV", "Fill Value information", (short)732);
        BREQ = new TagEnum("BREQ", "Beginning of required tags", (short)799);
        SDRAG = new TagEnum("SDRAG", "List of ragged array line lengths", (short)781);
        EREQ = new TagEnum("EREQ", "Current end of the range", (short)780);
        VG = new TagEnum("VG", "Vgroup", (short)1965);
        VH = new TagEnum("VH", "Vdata Header", (short)1962);
        VS = new TagEnum("VS", "Vdata Storage", (short)1963);
    }
}
