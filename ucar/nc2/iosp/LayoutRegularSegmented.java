// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.iosp;

import ucar.ma2.InvalidRangeException;
import ucar.ma2.Index;
import ucar.ma2.Section;

public class LayoutRegularSegmented implements Layout
{
    private long total;
    private long done;
    private long innerNelems;
    private long startPos;
    private long recSize;
    private int elemSize;
    private IndexChunker chunker;
    private IndexChunker.Chunk chunkOuter;
    private IndexChunker.Chunk chunkInner;
    private boolean debugNext;
    private int needInner;
    private int doneInner;
    
    public LayoutRegularSegmented(final long startPos, final int elemSize, final long recSize, final int[] srcShape, final Section wantSection) throws InvalidRangeException {
        this.chunkInner = new IndexChunker.Chunk(0L, 0, 0L);
        this.debugNext = false;
        this.needInner = 0;
        this.doneInner = 0;
        assert startPos > 0L;
        assert elemSize > 0;
        assert recSize > 0L;
        assert srcShape.length > 0;
        this.startPos = startPos;
        this.elemSize = elemSize;
        this.recSize = recSize;
        this.chunker = new IndexChunker(srcShape, wantSection);
        this.total = this.chunker.getTotalNelems();
        this.innerNelems = ((srcShape[0] == 0) ? 0L : (Index.computeSize(srcShape) / srcShape[0]));
        this.done = 0L;
    }
    
    public long getTotalNelems() {
        return this.total;
    }
    
    public int getElemSize() {
        return this.elemSize;
    }
    
    public boolean hasNext() {
        return this.done < this.total;
    }
    
    private long getFilePos(final long elem) {
        final long segno = elem / this.innerNelems;
        final long offset = elem % this.innerNelems;
        return this.startPos + segno * this.recSize + offset * this.elemSize;
    }
    
    private int getMaxElem(final long startElem) {
        return (int)(this.innerNelems - startElem % this.innerNelems);
    }
    
    public Chunk next() {
        IndexChunker.Chunk result = null;
        if (this.needInner > 0) {
            result = this.nextInner(false, 0);
        }
        else {
            result = this.nextOuter();
            final int nelems = this.getMaxElem(result.getSrcElem());
            if (nelems < result.getNelems()) {
                result = this.nextInner(true, nelems);
            }
        }
        this.done += result.getNelems();
        this.doneInner += result.getNelems();
        this.needInner -= result.getNelems();
        if (this.debugNext) {
            System.out.println(" next chunk: " + result);
        }
        return result;
    }
    
    private IndexChunker.Chunk nextInner(final boolean first, int nelems) {
        if (first) {
            this.chunkInner.setNelems(nelems);
            this.chunkInner.setDestElem(this.chunkOuter.getDestElem());
            this.needInner = this.chunkOuter.getNelems();
            this.doneInner = 0;
        }
        else {
            this.chunkInner.incrDestElem(this.chunkInner.getNelems());
            nelems = this.getMaxElem(this.chunkOuter.getSrcElem() + this.doneInner);
            nelems = Math.min(nelems, this.needInner);
            this.chunkInner.setNelems(nelems);
        }
        this.chunkInner.setSrcElem(this.chunkOuter.getSrcElem() + this.doneInner);
        this.chunkInner.setSrcPos(this.getFilePos(this.chunkOuter.getSrcElem() + this.doneInner));
        return this.chunkInner;
    }
    
    public IndexChunker.Chunk nextOuter() {
        (this.chunkOuter = this.chunker.next()).setSrcPos(this.getFilePos(this.chunkOuter.getSrcElem()));
        return this.chunkOuter;
    }
}
