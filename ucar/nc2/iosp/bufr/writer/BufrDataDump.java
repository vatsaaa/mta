// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.iosp.bufr.writer;

import ucar.ma2.Array;
import ucar.nc2.Variable;
import ucar.ma2.ArrayStructure;
import ucar.nc2.dataset.VariableDS;
import ucar.nc2.dataset.StructureDS;
import java.util.Iterator;
import ucar.ma2.StructureData;
import ucar.ma2.ArraySequence;
import ucar.ma2.DataType;
import ucar.ma2.StructureMembers;
import ucar.ma2.StructureDataIterator;
import ucar.nc2.dataset.SequenceDS;
import ucar.nc2.dataset.NetcdfDataset;
import ucar.nc2.NetcdfFile;
import ucar.nc2.iosp.bufr.Message;
import ucar.nc2.iosp.bufr.MessageScanner;
import ucar.unidata.io.RandomAccessFile;
import java.io.IOException;
import java.io.OutputStream;
import ucar.nc2.util.Indent;
import java.io.PrintStream;

public class BufrDataDump
{
    private PrintStream out;
    private Indent indent;
    
    public BufrDataDump(final String filename, final OutputStream os) throws IOException {
        this.indent = new Indent(2);
        (this.out = new PrintStream(os)).format("Dump %s%n", filename);
        this.indent.setIndentLevel(0);
        try {
            this.scanBufrFile(filename);
        }
        catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e.getMessage());
        }
        this.out.flush();
    }
    
    public void scanBufrFile(final String filename) throws Exception {
        int count = 0;
        RandomAccessFile raf = null;
        try {
            raf = new RandomAccessFile(filename, "r");
            final MessageScanner scan = new MessageScanner(raf);
            while (scan.hasNext()) {
                final Message m = scan.next();
                if (m == null) {
                    continue;
                }
                this.out.format("%sMessage %d header=%s%n", this.indent, count++, m.getHeader());
                this.processBufrMessageAsDataset(scan, m);
            }
        }
        finally {
            if (raf != null) {
                raf.close();
            }
        }
    }
    
    private void processBufrMessageAsDataset(final MessageScanner scan, final Message m) throws Exception {
        final byte[] mbytes = scan.getMessageBytes(m);
        final NetcdfFile ncfile = NetcdfFile.openInMemory("test", mbytes, "ucar.nc2.iosp.bufr.BufrIosp");
        final NetcdfDataset ncd = new NetcdfDataset(ncfile);
        final SequenceDS obs = (SequenceDS)ncd.findVariable("obs");
        final StructureDataIterator sdataIter = obs.getStructureIterator(-1);
        this.extractFirst(sdataIter, new Extract());
    }
    
    private void extractFirst(final StructureDataIterator sdataIter, final Extract result) throws IOException {
        while (sdataIter.hasNext()) {
            final StructureData sdata = sdataIter.next();
            for (final StructureMembers.Member m : sdata.getMembers()) {
                if (m.getName().equals("Buoy/platform identifier")) {
                    result.platformId = sdata.convertScalarDouble(m);
                }
                else {
                    if (m.getDataType() != DataType.SEQUENCE) {
                        continue;
                    }
                    final ArraySequence data = (ArraySequence)sdata.getArray(m);
                    this.extractNested(data.getStructureDataIterator(), result);
                }
            }
        }
    }
    
    private void extractNested(final StructureDataIterator sdataIter, final Extract result) throws IOException {
        while (sdataIter.hasNext()) {
            final StructureData sdata = sdataIter.next();
            for (final StructureMembers.Member m : sdata.getMembers()) {
                if (m.getName().equals("Year")) {
                    result.year = sdata.convertScalarInt(m);
                }
                else if (m.getName().equals("Month")) {
                    result.month = sdata.convertScalarInt(m);
                }
                else if (m.getName().equals("Day")) {
                    result.day = sdata.convertScalarInt(m);
                }
                else if (m.getName().equals("Hour")) {
                    result.hour = sdata.convertScalarInt(m);
                }
                else if (m.getName().equals("Minute")) {
                    result.min = sdata.convertScalarInt(m);
                }
                else if (m.getName().equals("Second")) {
                    result.sec = sdata.convertScalarInt(m);
                }
                else if (m.getName().equals("Time increment")) {
                    result.incr = sdata.convertScalarInt(m);
                }
                else if (m.getName().equals("Short time increment")) {
                    result.incrS = sdata.convertScalarInt(m);
                }
                else {
                    if (!m.getName().equals("Water column height")) {
                        continue;
                    }
                    result.value = sdata.getArray(m);
                    this.out.format("%s%n", result.toString());
                }
            }
        }
    }
    
    private void writeSequence(final StructureDS s, final StructureDataIterator sdataIter) throws IOException {
        this.indent.incr();
        int count = 0;
        while (sdataIter.hasNext()) {
            this.out.format("%sSequence %s count=%d%n", this.indent, s.getShortName(), count++);
            final StructureData sdata = sdataIter.next();
            this.indent.incr();
            for (final StructureMembers.Member m : sdata.getMembers()) {
                final Variable v = s.findVariable(m.getName());
                if (m.getDataType().isString() || m.getDataType().isNumeric()) {
                    this.writeVariable((VariableDS)v, sdata.getArray(m));
                }
                else if (m.getDataType() == DataType.STRUCTURE) {
                    final StructureDS sds = (StructureDS)v;
                    final ArrayStructure data = (ArrayStructure)sdata.getArray(m);
                    this.writeSequence(sds, data.getStructureDataIterator());
                }
                else {
                    if (m.getDataType() != DataType.SEQUENCE) {
                        continue;
                    }
                    final SequenceDS sds2 = (SequenceDS)v;
                    final ArraySequence data2 = (ArraySequence)sdata.getArray(m);
                    this.writeSequence(sds2, data2.getStructureDataIterator());
                }
            }
            this.indent.decr();
        }
        this.indent.decr();
    }
    
    private void writeVariable(final VariableDS v, final Array mdata) throws IOException {
        int count = 0;
        final String name = v.getShortName();
        final String units = v.getUnitsString();
        this.out.format("%svar='%s' units='%s' : ", this.indent, name, units);
        mdata.resetLocalIterator();
        while (mdata.hasNext()) {
            if (count++ > 0) {
                this.out.format(",", new Object[0]);
            }
            if (v.getDataType().isNumeric()) {
                final double val = mdata.nextDouble();
                if (v.isMissing(val)) {
                    this.out.format("missing", new Object[0]);
                }
                else {
                    this.out.format("%s", Double.toString(val));
                }
            }
            else {
                this.out.format("%s", mdata.next());
            }
        }
        this.out.format("%n", new Object[0]);
    }
    
    public static void main(final String[] args) throws IOException {
        new BufrDataDump("D:/work/michelle/TimeIncr.bufr", System.out);
    }
    
    private class Extract
    {
        double platformId;
        int year;
        int month;
        int day;
        int hour;
        int min;
        int sec;
        int incr;
        int incrS;
        Array value;
        
        @Override
        public String toString() {
            return "Extract{platformId=" + this.platformId + ", year=" + this.year + ", month=" + this.month + ", day=" + this.day + ", hour=" + this.hour + ", min=" + this.min + ", sec=" + this.sec + ", incr=" + this.incr + ", incrS=" + this.incrS + ", value=" + this.value + '}';
        }
    }
}
