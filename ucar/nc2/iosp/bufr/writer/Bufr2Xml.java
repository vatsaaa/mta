// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.iosp.bufr.writer;

import ucar.nc2.NetcdfFile;
import java.io.FileOutputStream;
import ucar.nc2.iosp.bufr.MessageScanner;
import ucar.unidata.io.RandomAccessFile;
import java.util.Formatter;
import ucar.nc2.Attribute;
import ucar.ma2.ArrayChar;
import ucar.ma2.Array;
import ucar.nc2.Variable;
import java.util.Iterator;
import ucar.ma2.StructureData;
import ucar.ma2.ArraySequence;
import ucar.ma2.ArrayStructure;
import ucar.ma2.DataType;
import ucar.nc2.dataset.VariableDS;
import ucar.ma2.StructureMembers;
import ucar.unidata.util.StringUtil;
import ucar.ma2.StructureDataIterator;
import ucar.nc2.dataset.StructureDS;
import ucar.nc2.dataset.SequenceDS;
import javax.xml.stream.XMLStreamException;
import java.io.IOException;
import javax.xml.stream.XMLOutputFactory;
import java.io.OutputStream;
import ucar.nc2.dataset.NetcdfDataset;
import ucar.nc2.iosp.bufr.Message;
import ucar.nc2.util.Indent;
import javax.xml.stream.XMLStreamWriter;

public class Bufr2Xml
{
    private XMLStreamWriter staxWriter;
    private Indent indent;
    private boolean skipMissing;
    
    public Bufr2Xml(final Message message, final NetcdfDataset ncfile, final OutputStream os, final boolean skipMissing) throws IOException {
        this.skipMissing = skipMissing;
        (this.indent = new Indent(2)).setIndentLevel(0);
        try {
            final XMLOutputFactory fac = XMLOutputFactory.newInstance();
            (this.staxWriter = fac.createXMLStreamWriter(os, "UTF-8")).writeStartDocument("UTF-8", "1.0");
            this.writeMessage(message, ncfile);
            this.staxWriter.writeCharacters("\n");
            this.staxWriter.writeEndDocument();
            this.staxWriter.flush();
        }
        catch (XMLStreamException e) {
            throw new IOException(e.getMessage());
        }
    }
    
    void writeMessage(final Message message, final NetcdfDataset ncfile) {
        try {
            this.staxWriter.writeCharacters("\n");
            this.staxWriter.writeCharacters(this.indent.toString());
            this.staxWriter.writeStartElement("bufrMessage");
            this.staxWriter.writeAttribute("nobs", Integer.toString(message.getNumberDatasets()));
            this.indent.incr();
            this.staxWriter.writeCharacters("\n");
            this.staxWriter.writeCharacters(this.indent.toString());
            this.staxWriter.writeCharacters("\n");
            this.staxWriter.writeCharacters(this.indent.toString());
            this.staxWriter.writeStartElement("edition");
            this.staxWriter.writeCharacters(Integer.toString(message.is.getBufrEdition()));
            this.staxWriter.writeEndElement();
            final String header = message.getHeader().trim();
            if (header.length() > 0) {
                this.staxWriter.writeCharacters("\n");
                this.staxWriter.writeCharacters(this.indent.toString());
                this.staxWriter.writeStartElement("header");
                this.staxWriter.writeCharacters(header);
                this.staxWriter.writeEndElement();
            }
            this.staxWriter.writeCharacters("\n");
            this.staxWriter.writeCharacters(this.indent.toString());
            this.staxWriter.writeStartElement("tableVersion");
            this.staxWriter.writeCharacters(message.getTableName());
            this.staxWriter.writeEndElement();
            this.staxWriter.writeStartElement("center");
            this.staxWriter.writeCharacters(message.getCenterName());
            this.staxWriter.writeEndElement();
            this.staxWriter.writeCharacters("\n");
            this.staxWriter.writeCharacters(this.indent.toString());
            this.staxWriter.writeStartElement("category");
            this.staxWriter.writeCharacters(message.getCategoryFullName());
            this.staxWriter.writeEndElement();
            final SequenceDS obs = (SequenceDS)ncfile.findVariable("obs");
            final StructureDataIterator sdataIter = obs.getStructureIterator(-1);
            this.writeSequence(obs, sdataIter);
            this.indent.decr();
            this.staxWriter.writeCharacters("\n");
            this.staxWriter.writeCharacters(this.indent.toString());
            this.staxWriter.writeEndElement();
        }
        catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e.getMessage());
        }
    }
    
    private void writeSequence(final StructureDS s, final StructureDataIterator sdataIter) throws IOException, XMLStreamException {
        int count = 0;
        while (sdataIter.hasNext()) {
            final StructureData sdata = sdataIter.next();
            this.staxWriter.writeCharacters("\n");
            this.staxWriter.writeCharacters(this.indent.toString());
            this.staxWriter.writeStartElement("struct");
            this.staxWriter.writeAttribute("name", StringUtil.quoteXmlAttribute(s.getShortName()));
            this.staxWriter.writeAttribute("count", Integer.toString(count++));
            for (final StructureMembers.Member m : sdata.getMembers()) {
                final Variable v = s.findVariable(m.getName());
                this.indent.incr();
                if (m.getDataType().isString() || m.getDataType().isNumeric()) {
                    this.writeVariable((VariableDS)v, sdata.getArray(m));
                }
                else if (m.getDataType() == DataType.STRUCTURE) {
                    final StructureDS sds = (StructureDS)v;
                    final ArrayStructure data = (ArrayStructure)sdata.getArray(m);
                    this.writeSequence(sds, data.getStructureDataIterator());
                }
                else if (m.getDataType() == DataType.SEQUENCE) {
                    final SequenceDS sds2 = (SequenceDS)v;
                    final ArraySequence data2 = (ArraySequence)sdata.getArray(m);
                    this.writeSequence(sds2, data2.getStructureDataIterator());
                }
                this.indent.decr();
            }
            this.staxWriter.writeCharacters("\n");
            this.staxWriter.writeCharacters(this.indent.toString());
            this.staxWriter.writeEndElement();
        }
    }
    
    void writeVariable(final VariableDS v, final Array mdata) throws XMLStreamException, IOException {
        this.staxWriter.writeCharacters("\n");
        this.staxWriter.writeCharacters(this.indent.toString());
        this.staxWriter.writeStartElement("data");
        final String name = v.getShortName();
        this.staxWriter.writeAttribute("name", StringUtil.quoteXmlAttribute(name));
        final String units = v.getUnitsString();
        if (units != null && !units.equals(name) && !units.startsWith("Code")) {
            this.staxWriter.writeAttribute("units", StringUtil.quoteXmlAttribute(v.getUnitsString()));
        }
        final Attribute att = v.findAttribute("BUFR:TableB_descriptor");
        final String desc = (att == null) ? "N/A" : att.getStringValue();
        this.staxWriter.writeAttribute("bufr", StringUtil.quoteXmlAttribute(desc));
        if (v.getDataType() == DataType.CHAR) {
            final ArrayChar ac = (ArrayChar)mdata;
            this.staxWriter.writeCharacters(ac.getString());
        }
        else {
            int count = 0;
            mdata.resetLocalIterator();
            while (mdata.hasNext()) {
                if (count > 0) {
                    this.staxWriter.writeCharacters(" ");
                }
                ++count;
                if (v.getDataType().isNumeric()) {
                    final double val = mdata.nextDouble();
                    if (v.isMissing(val)) {
                        this.staxWriter.writeCharacters("missing");
                    }
                    else if (v.getDataType() == DataType.FLOAT || v.getDataType() == DataType.DOUBLE) {
                        this.writeFloat(v, val);
                    }
                    else {
                        this.staxWriter.writeCharacters(mdata.toString());
                    }
                }
                else {
                    final String s = StringUtil.filter7bits(mdata.next().toString());
                    this.staxWriter.writeCharacters(StringUtil.quoteXmlContent(s));
                }
            }
        }
        this.staxWriter.writeEndElement();
    }
    
    private void writeFloat(final Variable v, final double val) throws XMLStreamException {
        final Attribute bitWidthAtt = v.findAttribute("BUFR:bitWidth");
        int sigDigits;
        if (bitWidthAtt == null) {
            sigDigits = 7;
        }
        else {
            final int bitWidth = bitWidthAtt.getNumericValue().intValue();
            if (bitWidth < 30) {
                final double sigDigitsD = Math.log10(2 << bitWidth);
                sigDigits = (int)(sigDigitsD + 1.0);
            }
            else {
                sigDigits = 7;
            }
        }
        final Formatter stringFormatter = new Formatter();
        final String format = "%." + sigDigits + "g";
        stringFormatter.format(format, val);
        this.staxWriter.writeCharacters(stringFormatter.toString());
    }
    
    public static void main(final String[] arg) throws Exception {
        final String filename = "C:/data/formats/bufr/uniqueExamples.bufr";
        Message message = null;
        RandomAccessFile raf = null;
        OutputStream out = null;
        int size = 0;
        try {
            int count = 0;
            raf = new RandomAccessFile(filename, "r");
            final MessageScanner scan = new MessageScanner(raf, 0L);
            while (scan.hasNext()) {
                message = scan.next();
                if (message.isTablesComplete()) {
                    if (!message.isBitCountOk()) {
                        continue;
                    }
                    final byte[] mbytes = scan.getMessageBytesFromLast(message);
                    try {
                        out = new FileOutputStream("C:/data/formats/bufr/uniqueE/" + count + ".xml");
                        final NetcdfFile ncfile = NetcdfFile.openInMemory("test", mbytes, "ucar.nc2.iosp.bufr.BufrIosp");
                        final NetcdfDataset ncd = new NetcdfDataset(ncfile);
                        new Bufr2Xml(message, ncd, out, true);
                        out.close();
                        ++count;
                        size += (int)message.getMessageSize();
                    }
                    catch (Throwable t) {}
                }
            }
        }
        finally {
            if (raf != null) {
                raf.close();
            }
            if (out != null) {
                out.close();
            }
        }
        System.out.printf("total size= %f Kb %n", 0.001 * size);
    }
}
