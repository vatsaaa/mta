// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.iosp.bufr.writer;

import ucar.nc2.iosp.bufr.BufrNumbers;
import ucar.nc2.Attribute;
import ucar.ma2.Array;
import ucar.nc2.Variable;
import java.util.Iterator;
import ucar.ma2.StructureData;
import ucar.ma2.ArraySequence;
import ucar.ma2.ArrayStructure;
import ucar.ma2.DataType;
import ucar.ma2.StructureMembers;
import ucar.ma2.StructureDataIterator;
import ucar.nc2.Structure;
import ucar.nc2.Sequence;
import ucar.nc2.NetcdfFile;
import ucar.nc2.iosp.bufr.Message;
import ucar.nc2.iosp.bufr.MessageScanner;
import ucar.unidata.io.RandomAccessFile;
import java.io.IOException;
import java.io.File;
import java.io.FileFilter;
import java.io.OutputStream;
import ucar.nc2.util.Indent;
import java.io.PrintStream;

public class BufrDataProcess
{
    private PrintStream out;
    private Indent indent;
    private boolean showData;
    private boolean showMess;
    private boolean showFile;
    
    public BufrDataProcess(final String filename, final OutputStream os, final FileFilter ff) throws IOException {
        this.indent = new Indent(2);
        this.showData = false;
        this.showMess = false;
        this.showFile = true;
        final File f = new File(filename);
        if (!f.exists()) {
            System.out.println(filename + " does not exist");
            return;
        }
        if (f.isDirectory()) {
            final Counter gtotal = new Counter();
            final int nmess = this.processAllInDir(f, os, ff, gtotal);
            this.out.format("%nGrand Total nmess=%d count=%d miss=%d %f %% %n", nmess, gtotal.nvals, gtotal.nmiss, gtotal.percent());
        }
        else {
            this.processOneFile(f.getPath(), os, null);
        }
    }
    
    public int processAllInDir(final File dir, final OutputStream os, final FileFilter ff, final Counter gtotal) throws IOException {
        int nmess = 0;
        System.out.println("---------------Reading directory " + dir.getPath());
        File[] arr$;
        final File[] allFiles = arr$ = dir.listFiles();
        for (final File f : arr$) {
            final String name = f.getAbsolutePath();
            if (!f.isDirectory()) {
                if (ff == null || ff.accept(f)) {
                    nmess += this.processOneFile(f.getPath(), os, gtotal);
                }
            }
        }
        arr$ = allFiles;
        for (final File f : arr$) {
            if (f.isDirectory()) {
                nmess += this.processAllInDir(f, os, ff, gtotal);
            }
        }
        return nmess;
    }
    
    int processOneFile(final String filename, final OutputStream os, final Counter gtotal) throws IOException {
        this.out = new PrintStream(os);
        if (this.showFile) {
            this.out.format("Process %s%n", filename);
        }
        this.indent.setIndentLevel(0);
        int nmess;
        try {
            final Counter total = new Counter();
            nmess = this.scanBufrFile(filename, total);
            if (this.showFile) {
                this.out.format("%nTotal nmess=%d count=%d miss=%d %f %% %n", nmess, total.nvals, total.nmiss, total.percent());
            }
            if (gtotal != null) {
                gtotal.add(total);
            }
        }
        catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e.getMessage());
        }
        this.out.flush();
        return nmess;
    }
    
    public int scanBufrFile(final String filename, final Counter total) throws Exception {
        int count = 0;
        RandomAccessFile raf = null;
        try {
            raf = new RandomAccessFile(filename, "r");
            final MessageScanner scan = new MessageScanner(raf);
            while (scan.hasNext()) {
                final Message m = scan.next();
                if (m == null) {
                    continue;
                }
                try {
                    if (this.showMess) {
                        this.out.format("%sMessage %d header=%s%n", this.indent, count, m.getHeader());
                    }
                    ++count;
                    final Counter counter = new Counter();
                    this.processBufrMessageAsDataset(scan, m, counter);
                    if (this.showMess) {
                        this.out.format("%scount=%d miss=%d%n", this.indent, counter.nvals, counter.nmiss);
                    }
                    total.add(counter);
                }
                catch (Exception e) {
                    System.out.printf("  BARF:%s on %s%n", e.getMessage(), m.getHeader());
                    this.indent.setIndentLevel(0);
                }
            }
        }
        finally {
            if (raf != null) {
                raf.close();
            }
        }
        return count;
    }
    
    private void processBufrMessageAsDataset(final MessageScanner scan, final Message m, final Counter counter) throws Exception {
        final byte[] mbytes = scan.getMessageBytes(m);
        final NetcdfFile ncfile = NetcdfFile.openInMemory("test", mbytes, "ucar.nc2.iosp.bufr.BufrIosp");
        final Sequence obs = (Sequence)ncfile.findVariable("obs");
        final StructureDataIterator sdataIter = obs.getStructureIterator(-1);
        this.processSequence(obs, sdataIter, counter);
    }
    
    private void processSequence(final Structure s, final StructureDataIterator sdataIter, final Counter counter) throws IOException {
        this.indent.incr();
        int count = 0;
        while (sdataIter.hasNext()) {
            if (this.showData) {
                this.out.format("%sSequence %s count=%d%n", this.indent, s.getShortName(), count++);
            }
            final StructureData sdata = sdataIter.next();
            this.indent.incr();
            for (final StructureMembers.Member m : sdata.getMembers()) {
                final Variable v = s.findVariable(m.getName());
                if (m.getDataType().isString() || m.getDataType().isNumeric()) {
                    this.processVariable(v, sdata.getArray(m), counter);
                }
                else if (m.getDataType() == DataType.STRUCTURE) {
                    final Structure sds = (Structure)v;
                    final ArrayStructure data = (ArrayStructure)sdata.getArray(m);
                    this.processSequence(sds, data.getStructureDataIterator(), counter);
                }
                else {
                    if (m.getDataType() != DataType.SEQUENCE) {
                        continue;
                    }
                    final Sequence sds2 = (Sequence)v;
                    final ArraySequence data2 = (ArraySequence)sdata.getArray(m);
                    this.processSequence(sds2, data2.getStructureDataIterator(), counter);
                }
            }
            this.indent.decr();
        }
        this.indent.decr();
    }
    
    private void processVariable(final Variable v, final Array mdata, final Counter count) throws IOException {
        final String name = v.getShortName();
        final String units = v.getUnitsString();
        final Attribute bwAtt = v.findAttribute("BUFR:bitWidth");
        final int bitWidth = (bwAtt == null) ? 0 : bwAtt.getNumericValue().intValue();
        if (this.showData) {
            this.out.format("%svar='%s' units='%s' : ", this.indent, name, units);
        }
        mdata.resetLocalIterator();
        while (mdata.hasNext()) {
            ++count.nvals;
            if (v.isUnsigned()) {
                if (!this.isMissingUnsigned(v, mdata, bitWidth)) {
                    continue;
                }
                ++count.nmiss;
            }
            else {
                if (!this.isMissing(v, mdata, bitWidth)) {
                    continue;
                }
                ++count.nmiss;
            }
        }
        if (this.showData) {
            this.out.format("%n", new Object[0]);
        }
    }
    
    private boolean isMissing(final Variable v, final Array mdata, final int bitWidth) {
        if (v.getDataType().isNumeric()) {
            final long val = mdata.nextLong();
            final boolean result = BufrNumbers.isMissing(val, bitWidth);
            if (this.showData) {
                this.out.format("%d %s,", val, result ? "(miss)" : "");
            }
            return result;
        }
        final Object s = mdata.next();
        if (this.showData) {
            this.out.format("%s,", s);
        }
        return false;
    }
    
    private boolean isMissingUnsigned(final Variable v, final Array mdata, final int bitWidth) {
        long val = 0L;
        switch (v.getDataType()) {
            case ENUM1:
            case BYTE: {
                val = DataType.unsignedByteToShort(mdata.nextByte());
                break;
            }
            case ENUM2:
            case SHORT: {
                val = DataType.unsignedShortToInt(mdata.nextShort());
                break;
            }
            case ENUM4:
            case INT: {
                val = DataType.unsignedIntToLong(mdata.nextInt());
                break;
            }
            default: {
                throw new RuntimeException("illegal datatype " + v.getDataType());
            }
        }
        final boolean result = BufrNumbers.isMissing(val, bitWidth);
        if (this.showData) {
            this.out.format("%d %s,", val, result ? "(miss)" : "");
        }
        return result;
    }
    
    public static void main(final String[] args) throws IOException {
        new BufrDataProcess("C:/data/formats/bufrRoy/", System.out, null);
    }
    
    private class Counter
    {
        int nvals;
        int nmiss;
        
        void add(final Counter c) {
            this.nvals += c.nvals;
            this.nmiss += c.nmiss;
        }
        
        double percent() {
            return 100.0 * this.nmiss / this.nvals;
        }
    }
}
