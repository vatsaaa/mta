// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.iosp.bufr.writer;

import ucar.nc2.dataset.NetcdfDataset;
import ucar.ma2.IndexIterator;
import ucar.ma2.Index;
import ucar.ma2.ArrayChar;
import ucar.ma2.Array;
import ucar.ma2.StructureMembers;
import ucar.ma2.ArraySequence;
import ucar.ma2.StructureData;
import ucar.ma2.StructureDataIterator;
import ucar.ma2.InvalidRangeException;
import java.io.IOException;
import java.util.Map;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import ucar.ma2.DataType;
import ucar.nc2.Variable;
import ucar.nc2.Structure;
import ucar.nc2.Dimension;
import java.util.HashMap;
import ucar.nc2.iosp.netcdf3.N3iosp;
import ucar.nc2.Attribute;
import ucar.nc2.NetcdfFileWriteable;
import ucar.nc2.NetcdfFile;

public class WriteT41_ncFlat
{
    private static boolean debug;
    private static long maxSize;
    
    public WriteT41_ncFlat(final NetcdfFile bufr, final String fileOutName, final boolean fill) throws IOException, InvalidRangeException {
        final NetcdfFileWriteable ncfile = NetcdfFileWriteable.createNew(fileOutName, fill);
        if (WriteT41_ncFlat.debug) {
            System.out.println("FileWriter write " + bufr.getLocation() + " to " + fileOutName);
        }
        final List<Attribute> glist = bufr.getGlobalAttributes();
        for (final Attribute att : glist) {
            final String useName = N3iosp.makeValidNetcdfObjectName(att.getName());
            Attribute useAtt;
            if (att.isArray()) {
                useAtt = ncfile.addGlobalAttribute(useName, att.getValues());
            }
            else if (att.isString()) {
                useAtt = ncfile.addGlobalAttribute(useName, att.getStringValue());
            }
            else {
                useAtt = ncfile.addGlobalAttribute(useName, att.getNumericValue());
            }
            if (WriteT41_ncFlat.debug) {
                System.out.println("add gatt= " + useAtt);
            }
        }
        Dimension obsDim = null;
        final Map<String, Dimension> dimHash = new HashMap<String, Dimension>();
        for (final Dimension oldD : bufr.getDimensions()) {
            final String useName2 = N3iosp.makeValidNetcdfObjectName(oldD.getName());
            final boolean isRecord = useName2.equals("record");
            final Dimension newD = ncfile.addDimension(useName2, oldD.getLength());
            dimHash.put(newD.getName(), newD);
            if (isRecord) {
                obsDim = newD;
            }
            if (WriteT41_ncFlat.debug) {
                System.out.println("add dim= " + newD);
            }
        }
        final Structure recordStruct = (Structure)bufr.findVariable("obs");
        for (final Variable oldVar : recordStruct.getVariables()) {
            if (oldVar.getDataType() == DataType.SEQUENCE) {
                continue;
            }
            final String varName = N3iosp.makeValidNetcdfObjectName(oldVar.getShortName());
            final DataType newType = oldVar.getDataType();
            final List<Dimension> newDims = new ArrayList<Dimension>();
            newDims.add(obsDim);
            for (final Dimension dim : oldVar.getDimensions()) {
                newDims.add(ncfile.addDimension(oldVar.getShortName() + "_strlen", dim.getLength()));
            }
            final Variable newVar = ncfile.addVariable(varName, newType, newDims);
            if (WriteT41_ncFlat.debug) {
                System.out.println("add var= " + newVar);
            }
            final List<Attribute> attList = oldVar.getAttributes();
            for (final Attribute att2 : attList) {
                final String useName3 = N3iosp.makeValidNetcdfObjectName(att2.getName());
                if (att2.isArray()) {
                    ncfile.addVariableAttribute(varName, useName3, att2.getValues());
                }
                else if (att2.isString()) {
                    ncfile.addVariableAttribute(varName, useName3, att2.getStringValue());
                }
                else {
                    ncfile.addVariableAttribute(varName, useName3, att2.getNumericValue());
                }
            }
        }
        final int total_seq = this.countSeq(recordStruct);
        final Dimension seqD = ncfile.addDimension("seq", total_seq, true, true, false);
        for (final Variable v : recordStruct.getVariables()) {
            if (v.getDataType() != DataType.SEQUENCE) {
                continue;
            }
            final Structure seq = (Structure)v;
            for (final Variable seqVar : seq.getVariables()) {
                final String varName2 = N3iosp.makeValidNetcdfObjectName(seqVar.getShortName());
                final DataType newType2 = seqVar.getDataType();
                final List<Dimension> newDims2 = new ArrayList<Dimension>();
                newDims2.add(seqD);
                for (final Dimension dim2 : seqVar.getDimensions()) {
                    newDims2.add(ncfile.addDimension(seqVar.getShortName() + "_strlen", dim2.getLength()));
                }
                final Variable newVar2 = ncfile.addVariable(varName2, newType2, newDims2);
                if (WriteT41_ncFlat.debug) {
                    System.out.println("add var= " + newVar2);
                }
                final List<Attribute> attList2 = seqVar.getAttributes();
                for (final Attribute att3 : attList2) {
                    final String useName4 = N3iosp.makeValidNetcdfObjectName(att3.getName());
                    if (att3.isArray()) {
                        ncfile.addVariableAttribute(varName2, useName4, att3.getValues());
                    }
                    else if (att3.isString()) {
                        ncfile.addVariableAttribute(varName2, useName4, att3.getStringValue());
                    }
                    else {
                        ncfile.addVariableAttribute(varName2, useName4, att3.getNumericValue());
                    }
                }
            }
        }
        ncfile.create();
        if (WriteT41_ncFlat.debug) {
            System.out.println("File Out= " + ncfile.toString());
        }
        final double total = this.copyVarData(bufr, ncfile, recordStruct);
        ncfile.flush();
        if (WriteT41_ncFlat.debug) {
            System.out.println("FileWriter done total bytes = " + total);
        }
        ncfile.close();
    }
    
    private int countSeq(final Structure recordStruct) throws IOException {
        int total = 0;
        int count = 0;
        int max = 0;
        final StructureDataIterator iter = recordStruct.getStructureIterator();
        while (iter.hasNext()) {
            final StructureData sdata = iter.next();
            final ArraySequence seq1 = sdata.getArraySequence("seq1");
            final int n = seq1.getStructureDataCount();
            total += n;
            ++count;
            max = Math.max(max, n);
        }
        final double avg = total / count;
        final int wasted = count * max - total;
        final double wp = wasted / (double)(count * max);
        System.out.println(" Max = " + max + " avg = " + avg + " wasted = " + wasted + " %= " + wp);
        return total;
    }
    
    private double copyVarData(final NetcdfFile bufr, final NetcdfFileWriteable ncfile, final Structure recordStruct) throws IOException, InvalidRangeException {
        final int nrecs = (int)recordStruct.getSize();
        final int sdataSize = recordStruct.getElementSize();
        int seqCount = 0;
        double total = 0.0;
        double totalRecordBytes = 0.0;
        for (int count = 0; count < nrecs; ++count) {
            final StructureData recordData = recordStruct.readStructure(count);
            for (final StructureMembers.Member m : recordData.getMembers()) {
                if (m.getDataType() == DataType.SEQUENCE) {
                    final ArraySequence seq1 = recordData.getArraySequence(m);
                    final StructureDataIterator iter = seq1.getStructureDataIterator();
                    while (iter.hasNext()) {
                        final StructureData seqData = iter.next();
                        for (final StructureMembers.Member seqm : seqData.getMembers()) {
                            final Array data = seqData.getArray(seqm);
                            final int[] shape = data.getShape();
                            final int[] newShape = new int[data.getRank() + 1];
                            newShape[0] = 1;
                            for (int i = 0; i < data.getRank(); ++i) {
                                newShape[i + 1] = shape[i];
                            }
                            final int[] origin = new int[data.getRank() + 1];
                            origin[0] = seqCount;
                            if (WriteT41_ncFlat.debug && count == 0 && seqCount == 0) {
                                System.out.println("write to = " + seqm.getName());
                            }
                            ncfile.write(seqm.getName(), origin, data.reshape(newShape));
                        }
                        ++seqCount;
                    }
                }
                else {
                    final Array data2 = recordData.getArray(m);
                    final int[] shape2 = data2.getShape();
                    final int[] newShape2 = new int[data2.getRank() + 1];
                    newShape2[0] = 1;
                    for (int j = 0; j < data2.getRank(); ++j) {
                        newShape2[j + 1] = shape2[j];
                    }
                    final int[] origin2 = new int[data2.getRank() + 1];
                    origin2[0] = count;
                    if (WriteT41_ncFlat.debug && count == 0) {
                        System.out.println("write to = " + m.getName());
                    }
                    ncfile.write(m.getName(), origin2, data2.reshape(newShape2));
                }
            }
            totalRecordBytes += sdataSize;
        }
        total += totalRecordBytes;
        totalRecordBytes /= 1000000.0;
        if (WriteT41_ncFlat.debug) {
            System.out.println("write record var; total = " + totalRecordBytes + " Mbytes # recs=" + nrecs);
        }
        return total;
    }
    
    private void copyAll(final NetcdfFileWriteable ncfile, final Variable oldVar) throws IOException {
        final String newName = N3iosp.makeValidNetcdfObjectName(oldVar.getName());
        Array data = oldVar.read();
        try {
            if (oldVar.getDataType() == DataType.STRING) {
                data = this.convertToChar(ncfile.findVariable(newName), data);
            }
            if (data.getSize() > 0L) {
                ncfile.write(newName, data);
            }
        }
        catch (InvalidRangeException e) {
            e.printStackTrace();
            throw new IOException(e.getMessage() + " for Variable " + oldVar.getName());
        }
    }
    
    private void copySome(final NetcdfFileWriteable ncfile, final Variable oldVar, final int nelems) throws IOException {
        final String newName = N3iosp.makeValidNetcdfObjectName(oldVar.getName());
        final int[] shape = oldVar.getShape();
        final int[] origin = new int[oldVar.getRank()];
        for (int size = shape[0], i = 0; i < size; i += nelems) {
            origin[0] = i;
            final int left = size - i;
            shape[0] = Math.min(nelems, left);
            try {
                Array data = oldVar.read(origin, shape);
                if (oldVar.getDataType() == DataType.STRING) {
                    data = this.convertToChar(ncfile.findVariable(newName), data);
                }
                if (data.getSize() > 0L) {
                    ncfile.write(newName, origin, data);
                    if (WriteT41_ncFlat.debug) {
                        System.out.println("write " + data.getSize() + " bytes");
                    }
                }
            }
            catch (InvalidRangeException e) {
                e.printStackTrace();
                throw new IOException(e.getMessage());
            }
        }
    }
    
    private Array convertToChar(final Variable newVar, final Array oldData) {
        final ArrayChar newData = (ArrayChar)Array.factory(DataType.CHAR, newVar.getShape());
        final Index ima = newData.getIndex();
        final IndexIterator ii = oldData.getIndexIterator();
        while (ii.hasNext()) {
            final String s = (String)ii.getObjectNext();
            final int[] c = ii.getCurrentCounter();
            for (int i = 0; i < c.length; ++i) {
                ima.setDim(i, c[i]);
            }
            newData.setString(ima, s);
        }
        return newData;
    }
    
    public static void main(final String[] args) throws Exception, IOException, InstantiationException, IllegalAccessException {
        final String fileIn = "D:/motherlode/bufr/cat.out";
        final NetcdfDataset ncf = NetcdfDataset.openDataset(fileIn);
        System.out.println(ncf.toString());
        new WriteT41_ncFlat(ncf, "D:/motherlode/bufr/cat2.nc", true);
    }
    
    static {
        WriteT41_ncFlat.debug = true;
        WriteT41_ncFlat.maxSize = 1000000L;
    }
}
