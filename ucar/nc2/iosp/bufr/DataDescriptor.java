// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.iosp.bufr;

import org.slf4j.LoggerFactory;
import java.util.Iterator;
import ucar.nc2.iosp.bufr.tables.TableB;
import ucar.nc2.iosp.bufr.tables.TableC;
import java.util.List;
import org.slf4j.Logger;

public class DataDescriptor
{
    private static Logger log;
    short fxy;
    int f;
    int x;
    int y;
    String name;
    String units;
    boolean bad;
    boolean localOverride;
    int scale;
    int refVal;
    int bitWidth;
    int type;
    List<DataDescriptor> subKeys;
    int replication;
    int replicationCountSize;
    int repetitionCountSize;
    AssociatedField assField;
    Object refersTo;
    DataDescriptorTreeConstructor.DataPresentIndicator dpi;
    private int total_nbytesCDM;
    boolean isVarLength;
    boolean isBad;
    int total_nbits;
    
    DataDescriptor() {
        this.replication = 1;
        this.total_nbytesCDM = 0;
    }
    
    public DataDescriptor(final short fxy, final TableLookup lookup) {
        this.replication = 1;
        this.total_nbytesCDM = 0;
        this.fxy = fxy;
        this.f = (fxy & 0xC000) >> 14;
        this.x = (fxy & 0x3F00) >> 8;
        this.y = (fxy & 0xFF);
        TableB.Descriptor db = null;
        if (this.f == 0) {
            db = lookup.getDescriptorTableB(fxy);
            if (db != null) {
                this.setDescriptor(db);
            }
            else {
                this.bad = true;
                if (this.f != 1) {
                    this.name = "*NOT FOUND";
                }
            }
        }
        else if (this.f == 1) {
            this.type = 3;
        }
        else if (this.f == 2) {
            this.name = TableC.getOperatorName(this.x);
        }
    }
    
    private void setDescriptor(final TableB.Descriptor d) {
        this.name = d.getName().trim();
        this.units = d.getUnits().trim();
        this.refVal = d.getRefVal();
        this.scale = d.getScale();
        this.bitWidth = d.getDataWidth();
        this.localOverride = d.getLocalOverride();
        if (this.units.equalsIgnoreCase("CCITT IA5") || this.units.equalsIgnoreCase("CCITT_IA5")) {
            this.type = 1;
        }
        if (this.units.equalsIgnoreCase("Code Table") || this.units.equalsIgnoreCase("Code_Table")) {
            this.type = 2;
        }
    }
    
    DataDescriptor makeAssociatedField(final int bitWidth) {
        final DataDescriptor assDD = new DataDescriptor();
        assDD.name = this.name + "_associated_field";
        assDD.units = "";
        assDD.refVal = 0;
        assDD.scale = 0;
        assDD.bitWidth = bitWidth;
        assDD.type = 0;
        assDD.f = 0;
        assDD.x = 31;
        assDD.y = 22;
        assDD.fxy = (short)((this.f << 14) + (this.x << 8) + this.y);
        return assDD;
    }
    
    public List<DataDescriptor> getSubKeys() {
        return this.subKeys;
    }
    
    public boolean isOkForVariable() {
        return this.f == 0 || this.f == 1 || (this.f == 2 && this.x == 5) || (this.f == 2 && this.x == 24 && this.y == 255);
    }
    
    public boolean isLocal() {
        return (this.f == 0 || this.f == 3) && (this.x >= 48 || this.y >= 192);
    }
    
    public boolean isLocalOverride() {
        return this.localOverride;
    }
    
    public String getFxyName() {
        return this.f + "-" + this.x + "-" + this.y;
    }
    
    public short getFxy() {
        return this.fxy;
    }
    
    public String getName() {
        return this.name;
    }
    
    public int getType() {
        return this.type;
    }
    
    public int getScale() {
        return this.scale;
    }
    
    public int getRefVal() {
        return this.refVal;
    }
    
    public String getUnits() {
        return this.units;
    }
    
    public float convert(final long raw) {
        if (BufrNumbers.isMissing(raw, this.bitWidth)) {
            return Float.NaN;
        }
        final float fscale = (float)Math.pow(10.0, -this.scale);
        final float fval = (float)(raw + this.refVal);
        return fscale * fval;
    }
    
    public static void transferInfo(final List<DataDescriptor> fromList, final List<DataDescriptor> toList) {
        if (fromList.size() != toList.size()) {
            throw new IllegalArgumentException("list sizes dont match " + fromList.size() + " != " + toList.size());
        }
        for (int i = 0; i < fromList.size(); ++i) {
            final DataDescriptor from = fromList.get(i);
            final DataDescriptor to = toList.get(i);
            to.refersTo = from.refersTo;
            to.name = from.name;
            if (from.getSubKeys() != null) {
                transferInfo(from.getSubKeys(), to.getSubKeys());
            }
        }
    }
    
    int countBits() {
        int total_nbits = 0;
        this.total_nbytesCDM = 0;
        for (final DataDescriptor dd : this.subKeys) {
            if (dd.subKeys != null) {
                total_nbits += dd.countBits();
                this.total_nbytesCDM += dd.total_nbytesCDM;
            }
            else {
                if (dd.f != 0) {
                    continue;
                }
                total_nbits += dd.bitWidth;
                this.total_nbytesCDM += dd.getByteWidthCDM();
            }
        }
        if (this.replication > 1) {
            total_nbits *= this.replication;
            this.total_nbytesCDM *= this.replication;
        }
        return total_nbits;
    }
    
    public int getBitWidth() {
        return this.bitWidth;
    }
    
    public int getByteWidthCDM() {
        if (this.type == 1) {
            return this.bitWidth / 8;
        }
        if (this.type == 3) {
            return this.total_nbytesCDM;
        }
        if (this.bitWidth < 9) {
            return 1;
        }
        if (this.bitWidth < 17) {
            return 2;
        }
        if (this.bitWidth < 33) {
            return 4;
        }
        return 8;
    }
    
    @Override
    public String toString() {
        final String id = this.getFxyName();
        final StringBuilder sbuff = new StringBuilder();
        if (this.f == 0) {
            sbuff.append(this.getFxyName()).append(": ");
            sbuff.append(this.name).append(" units=").append(this.units);
            if (this.type == 0) {
                sbuff.append(" scale=").append(this.scale).append(" refVal=").append(this.refVal);
                sbuff.append(" nbits=").append(this.bitWidth);
            }
            else if (this.type == 1) {
                sbuff.append(" nchars=").append(this.bitWidth / 8);
            }
            else {
                sbuff.append(" enum nbits=").append(this.bitWidth);
            }
        }
        else if (this.f == 1) {
            sbuff.append(id).append(": ").append("Replication");
            if (this.replication != 1) {
                sbuff.append(" count=").append(this.replication);
            }
            if (this.replicationCountSize != 0) {
                sbuff.append(" replicationCountSize=").append(this.replicationCountSize);
            }
            if (this.repetitionCountSize != 0) {
                sbuff.append(" repetitionCountSize=").append(this.repetitionCountSize);
            }
        }
        else if (this.f == 2) {
            String desc = TableC.getOperatorName(this.x);
            if (desc == null) {
                desc = "Operator";
            }
            sbuff.append(id).append(": ").append(desc);
        }
        else {
            sbuff.append(id).append(": ").append(this.name);
        }
        return sbuff.toString();
    }
    
    public int getTotalBits() {
        return this.total_nbits;
    }
    
    public boolean isVarLength() {
        return this.isVarLength;
    }
    
    static {
        DataDescriptor.log = LoggerFactory.getLogger(DataDescriptor.class);
    }
    
    static class AssociatedField
    {
        int nbits;
        int nfields;
        String dataFldName;
        
        AssociatedField(final int nbits) {
            this.nbits = nbits;
        }
    }
}
