// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.iosp.bufr;

public class BufrDataSection
{
    private long dataPos;
    private int dataLength;
    
    public BufrDataSection(final long dataPos, final int dataLength) {
        this.dataPos = dataPos;
        this.dataLength = dataLength;
    }
    
    public long getDataPos() {
        return this.dataPos;
    }
    
    public int getDataLength() {
        return this.dataLength;
    }
}
