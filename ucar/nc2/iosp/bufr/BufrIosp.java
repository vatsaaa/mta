// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.iosp.bufr;

import ucar.ma2.StructureData;
import org.slf4j.LoggerFactory;
import ucar.nc2.util.CompareNetcdf2;
import ucar.nc2.NCdumpW;
import java.io.OutputStream;
import java.io.PrintWriter;
import ucar.ma2.StructureMembers;
import ucar.ma2.IndexIterator;
import ucar.ma2.ArrayStructure;
import ucar.ma2.InvalidRangeException;
import ucar.nc2.Structure;
import ucar.ma2.StructureDataIterator;
import ucar.ma2.ArraySequence;
import ucar.ma2.Array;
import ucar.ma2.Section;
import ucar.nc2.Variable;
import java.util.Iterator;
import ucar.nc2.util.CancelTask;
import ucar.nc2.NetcdfFile;
import java.io.IOException;
import ucar.unidata.io.RandomAccessFile;
import ucar.nc2.util.DebugFlags;
import java.util.ArrayList;
import java.util.List;
import java.util.Formatter;
import org.slf4j.Logger;
import ucar.nc2.iosp.AbstractIOServiceProvider;

public class BufrIosp extends AbstractIOServiceProvider
{
    private static Logger log;
    public static final String obsRecord = "obs";
    static final String obsIndex = "obsRecordIndex";
    private static boolean debugCompress;
    private static boolean debugOpen;
    private Formatter parseInfo;
    private ConstructNC construct;
    private Message protoMessage;
    private List<Message> msgs;
    private int[] obsStart;
    private int nelems;
    
    public BufrIosp() {
        this.msgs = new ArrayList<Message>();
        this.nelems = -1;
    }
    
    public static void setDebugFlags(final DebugFlags debugFlag) {
        BufrIosp.debugOpen = debugFlag.isSet("Bufr/open");
    }
    
    public boolean isValidFile(final RandomAccessFile raf) throws IOException {
        return MessageScanner.isValidFile(raf);
    }
    
    @Override
    public void open(final RandomAccessFile raf, final NetcdfFile ncfile, final CancelTask cancelTask) throws IOException {
        final long start = System.nanoTime();
        if (BufrIosp.debugOpen) {
            (this.parseInfo = new Formatter()).format("\nOpen %s size = %d Kb \n", raf.getLocation(), raf.length() / 1000L);
        }
        this.raf = raf;
        final MessageScanner scan = new MessageScanner(raf);
        int count = 0;
        while (scan.hasNext()) {
            final Message m = scan.next();
            if (m == null) {
                continue;
            }
            if (this.protoMessage == null) {
                (this.protoMessage = m).getRootDataDescriptor();
                if (!this.protoMessage.isTablesComplete()) {
                    throw new IllegalStateException("BUFR file has incomplete tables");
                }
            }
            else if (!this.protoMessage.equals(m)) {
                BufrIosp.log.warn("File " + ncfile.getLocation() + " has different BUFR message types msgno=" + count + "; skipping");
                continue;
            }
            this.msgs.add(m);
            ++count;
        }
        this.obsStart = new int[this.msgs.size()];
        int mi = 0;
        int countObs = 0;
        for (final Message i : this.msgs) {
            this.obsStart[mi++] = countObs;
            countObs += i.getNumberDatasets();
        }
        if (BufrIosp.debugOpen) {
            final long took = (System.nanoTime() - start) / 1000000L;
            final double rate = (took > 0L) ? (count / (double)took) : 0.0;
            this.parseInfo.format("nmsgs= %d nobs = %d took %d msecs rate = %f msgs/msec\n", count, scan.getTotalObs(), took, rate);
        }
        this.construct = new ConstructNC(this.protoMessage, countObs, ncfile);
        ncfile.finish();
    }
    
    public Array readData(final Variable v2, final Section section) throws IOException, InvalidRangeException {
        final Structure s = this.construct.recordStructure;
        return new ArraySequence(s.makeStructureMembers(), new SeqIter(), this.nelems);
    }
    
    private void addTime(final ArrayStructure as) throws IOException {
        final int n = (int)as.getSize();
        final Array timeData = Array.factory(Double.TYPE, new int[] { n });
        final IndexIterator ii = timeData.getIndexIterator();
        final StructureDataIterator iter = as.getStructureDataIterator();
        while (iter.hasNext()) {
            ii.setDoubleNext(this.construct.makeObsTimeValue(iter.next()));
        }
        final StructureMembers.Member m = as.findMember("time");
        m.setDataArray(timeData);
    }
    
    @Override
    public StructureDataIterator getStructureIterator(final Structure s, final int bufferSize) throws IOException {
        return new SeqIter();
    }
    
    @Override
    public String getDetailInfo() {
        final Formatter ff = new Formatter();
        try {
            this.protoMessage.dump(ff);
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        if (this.parseInfo != null) {
            ff.format("%s", this.parseInfo.toString());
        }
        return ff.toString();
    }
    
    public String getFileTypeId() {
        return "BUFR";
    }
    
    public String getFileTypeDescription() {
        return "WMO Binary Universal Form";
    }
    
    public void readAll(final boolean dump) throws IOException, InvalidRangeException {
        final Formatter f = new Formatter(System.out);
        for (final Message m : this.msgs) {
            Array data;
            if (!m.dds.isCompressed()) {
                final MessageUncompressedDataReader reader = new MessageUncompressedDataReader();
                data = reader.readEntireMessage(this.construct.recordStructure, this.protoMessage, m, this.raf, null);
            }
            else {
                final MessageCompressedDataReader reader2 = new MessageCompressedDataReader();
                data = reader2.readEntireMessage(this.construct.recordStructure, this.protoMessage, m, this.raf, null);
            }
            if (dump) {
                NCdumpW.printArray(data, "test", new PrintWriter(System.out), null);
            }
        }
    }
    
    public void compare(final Structure obs) throws IOException, InvalidRangeException {
        int start = 0;
        for (final Message m : this.msgs) {
            if (!m.isTablesComplete()) {
                continue;
            }
            Array data1;
            if (!m.dds.isCompressed()) {
                final MessageUncompressedDataReader reader = new MessageUncompressedDataReader();
                data1 = reader.readEntireMessage(this.construct.recordStructure, this.protoMessage, m, this.raf, null);
            }
            else {
                final MessageCompressedDataReader reader2 = new MessageCompressedDataReader();
                data1 = reader2.readEntireMessage(this.construct.recordStructure, this.protoMessage, m, this.raf, null);
            }
            final int n = m.getNumberDatasets();
            m.calcTotalBits(null);
            final Array data2 = obs.read(new Section().appendRange(start, start + n - 1));
            final CompareNetcdf2 cn = new CompareNetcdf2(new Formatter(System.out), true, true, true);
            cn.compareData("all", data1, data2, true);
            start += n;
        }
    }
    
    public static void doon(final String filename) throws IOException, InvalidRangeException {
        System.out.printf("BufrIosp compare = %s%n", filename);
        final NetcdfFile ncfile = NetcdfFile.open(filename);
        final BufrIosp iosp = (BufrIosp)ncfile.getIosp();
        iosp.readAll(false);
    }
    
    public static void main(final String[] arg) throws IOException, InvalidRangeException {
        doon("D:/formats/bufr/tmp/IUST56.bufr");
        doon("D:/formats/bufr/tmp/dispatch/KWBC-IUST56.bufr");
    }
    
    static {
        BufrIosp.log = LoggerFactory.getLogger(BufrIosp.class);
        BufrIosp.debugCompress = false;
        BufrIosp.debugOpen = false;
    }
    
    private class MsgFinder
    {
        int msgIndex;
        
        private MsgFinder() {
            this.msgIndex = 0;
        }
        
        Message find(final int index) {
            while (this.msgIndex < BufrIosp.this.msgs.size()) {
                final Message m = BufrIosp.this.msgs.get(this.msgIndex);
                if (BufrIosp.this.obsStart[this.msgIndex] <= index && index < BufrIosp.this.obsStart[this.msgIndex] + m.getNumberDatasets()) {
                    return m;
                }
                ++this.msgIndex;
            }
            return null;
        }
        
        int obsOffsetInMessage(final int index) {
            return index - BufrIosp.this.obsStart[this.msgIndex];
        }
    }
    
    private class SeqIter implements StructureDataIterator
    {
        StructureDataIterator currIter;
        Iterator<Message> messIter;
        int recnum;
        int bufferSize;
        boolean addTime;
        
        SeqIter() {
            this.recnum = 0;
            this.bufferSize = -1;
            this.addTime = (BufrIosp.this.construct.recordStructure.findVariable("time") != null);
            this.reset();
        }
        
        public StructureDataIterator reset() {
            this.recnum = 0;
            this.messIter = BufrIosp.this.msgs.iterator();
            this.currIter = null;
            return this;
        }
        
        public boolean hasNext() throws IOException {
            if (this.currIter == null) {
                this.currIter = this.readNextMessage();
                if (this.currIter == null) {
                    BufrIosp.this.nelems = this.recnum;
                    return false;
                }
            }
            if (!this.currIter.hasNext()) {
                this.currIter = this.readNextMessage();
                return this.hasNext();
            }
            return true;
        }
        
        public StructureData next() throws IOException {
            ++this.recnum;
            return this.currIter.next();
        }
        
        private StructureDataIterator readNextMessage() throws IOException {
            if (!this.messIter.hasNext()) {
                return null;
            }
            final Message m = this.messIter.next();
            ArrayStructure as;
            if (m.dds.isCompressed()) {
                final MessageCompressedDataReader reader = new MessageCompressedDataReader();
                as = reader.readEntireMessage(BufrIosp.this.construct.recordStructure, BufrIosp.this.protoMessage, m, BufrIosp.this.raf, null);
            }
            else {
                final MessageUncompressedDataReader reader2 = new MessageUncompressedDataReader();
                as = reader2.readEntireMessage(BufrIosp.this.construct.recordStructure, BufrIosp.this.protoMessage, m, BufrIosp.this.raf, null);
            }
            if (this.addTime) {
                BufrIosp.this.addTime(as);
            }
            return as.getStructureDataIterator();
        }
        
        public void setBufferSize(final int bufferSize) {
            this.bufferSize = bufferSize;
        }
        
        public int getCurrentRecno() {
            return this.recnum - 1;
        }
    }
}
