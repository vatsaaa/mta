// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.iosp.bufr;

import java.util.Iterator;
import java.io.IOException;
import java.util.ArrayList;
import ucar.unidata.io.RandomAccessFile;
import java.util.List;

public class BufrDataDescriptionSection
{
    private final long offset;
    private final int ndatasets;
    private final int datatype;
    private final List<Short> descriptors;
    
    public BufrDataDescriptionSection(final RandomAccessFile raf) throws IOException {
        this.descriptors = new ArrayList<Short>();
        this.offset = raf.getFilePointer();
        final int length = BufrNumbers.uint3(raf);
        final long EOS = this.offset + length;
        raf.read();
        this.ndatasets = BufrNumbers.uint2(raf);
        this.datatype = raf.read();
        for (int ndesc = (length - 7) / 2, i = 0; i < ndesc; ++i) {
            final int ch1 = raf.read();
            final int ch2 = raf.read();
            final short fxy = (short)((ch1 << 8) + ch2);
            this.descriptors.add(fxy);
        }
        raf.seek(EOS);
    }
    
    public final long getOffset() {
        return this.offset;
    }
    
    public final int getNumberDatasets() {
        return this.ndatasets;
    }
    
    public final int getDataType() {
        return this.datatype;
    }
    
    public boolean isObserved() {
        return (this.datatype & 0x80) != 0x0;
    }
    
    public boolean isCompressed() {
        return (this.datatype & 0x40) != 0x0;
    }
    
    public final List<Short> getDataDescriptors() {
        return this.descriptors;
    }
    
    public final List<String> getDescriptors() {
        final List<String> desc = new ArrayList<String>();
        for (final short fxy : this.descriptors) {
            desc.add(Descriptor.makeString(fxy));
        }
        return desc;
    }
}
