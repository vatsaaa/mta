// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.iosp.bufr;

import java.util.ArrayList;
import ucar.ma2.StructureDataIterator;
import ucar.ma2.ArraySequence;
import ucar.ma2.SequenceIterator;
import ucar.ma2.ArrayObject;
import ucar.nc2.Sequence;
import java.util.List;
import ucar.ma2.IndexIterator;
import ucar.ma2.Array;
import java.util.Iterator;
import ucar.ma2.DataType;
import java.io.IOException;
import ucar.ma2.Range;
import ucar.ma2.StructureMembers;
import java.util.HashMap;
import ucar.ma2.ArrayStructureMA;
import ucar.ma2.ArrayStructure;
import java.util.Formatter;
import ucar.unidata.io.RandomAccessFile;
import ucar.nc2.Structure;

public class MessageCompressedDataReader
{
    public ArrayStructure readEntireMessage(final Structure s, final Message proto, final Message m, final RandomAccessFile raf, final Formatter f) throws IOException {
        DataDescriptor.transferInfo(proto.getRootDataDescriptor().getSubKeys(), m.getRootDataDescriptor().getSubKeys());
        final int n = m.getNumberDatasets();
        final ArrayStructureMA ama = ArrayStructureMA.factoryMA(s, new int[] { n });
        setIterators(ama);
        final HashMap<DataDescriptor, StructureMembers.Member> map = new HashMap<DataDescriptor, StructureMembers.Member>(100);
        this.associateMessage2Members(ama.getStructureMembers(), m.getRootDataDescriptor(), map);
        this.readData(m, raf, f, new Request(ama, map, null));
        return ama;
    }
    
    public void readData(final ArrayStructureMA ama, final Message m, final RandomAccessFile raf, final Range r, final Formatter f) throws IOException {
        HashMap<DataDescriptor, StructureMembers.Member> map = null;
        if (ama != null) {
            map = new HashMap<DataDescriptor, StructureMembers.Member>(2 * ama.getMembers().size());
            this.associateMessage2Members(ama.getStructureMembers(), m.getRootDataDescriptor(), map);
        }
        this.readData(m, raf, f, new Request(ama, map, r));
    }
    
    public static void setIterators(final ArrayStructureMA ama) {
        final StructureMembers sms = ama.getStructureMembers();
        for (final StructureMembers.Member sm : sms.getMembers()) {
            final Array data = sm.getDataArray();
            if (data instanceof ArrayStructureMA) {
                setIterators((ArrayStructureMA)data);
            }
            else {
                final int[] shape = data.getShape();
                if (shape.length > 1 && sm.getDataType() != DataType.CHAR) {
                    Array datap;
                    if (shape.length == 2) {
                        datap = data.transpose(0, 1);
                    }
                    else {
                        final int[] pdims = new int[shape.length];
                        for (int i = 0; i < shape.length - 1; ++i) {
                            pdims[i] = i + 1;
                        }
                        datap = data.permute(pdims);
                    }
                    sm.setDataObject(datap.getIndexIterator());
                }
                else {
                    sm.setDataObject(data.getIndexIterator());
                }
            }
        }
    }
    
    private void associateMessage2Members(final StructureMembers members, final DataDescriptor parent, final HashMap<DataDescriptor, StructureMembers.Member> map) throws IOException {
        for (final DataDescriptor dkey : parent.getSubKeys()) {
            if (dkey.name == null) {
                if (dkey.getSubKeys() == null) {
                    continue;
                }
                this.associateMessage2Members(members, dkey, map);
            }
            else {
                final StructureMembers.Member m = members.findMember(dkey.name);
                if (m != null) {
                    map.put(dkey, m);
                    if (m.getDataType() != DataType.STRUCTURE) {
                        continue;
                    }
                    final ArrayStructure nested = (ArrayStructure)m.getDataArray();
                    if (dkey.getSubKeys() == null) {
                        continue;
                    }
                    this.associateMessage2Members(nested.getStructureMembers(), dkey, map);
                }
                else {
                    if (dkey.getSubKeys() == null) {
                        continue;
                    }
                    this.associateMessage2Members(members, dkey, map);
                }
            }
        }
    }
    
    private int readData(final Message m, final RandomAccessFile raf, final Formatter f, final Request req) throws IOException {
        final BitReader reader = new BitReader(raf, m.dataSection.getDataPos() + 4L);
        final DataDescriptor root = m.getRootDataDescriptor();
        if (root.isBad) {
            return 0;
        }
        final DebugOut out = (f == null) ? null : new DebugOut(f);
        final BitCounterCompressed[] counterFlds = new BitCounterCompressed[root.subKeys.size()];
        this.readData(out, reader, counterFlds, root, 0, m.getNumberDatasets(), req);
        m.msg_nbits = 0;
        for (final BitCounterCompressed counter : counterFlds) {
            if (counter != null) {
                m.msg_nbits += counter.getTotalBits();
            }
        }
        return m.msg_nbits;
    }
    
    private int readData(final DebugOut out, final BitReader reader, final BitCounterCompressed[] fldCounters, final DataDescriptor parent, int bitOffset, final int ndatasets, final Request req) throws IOException {
        final List<DataDescriptor> flds = parent.getSubKeys();
        for (int fldidx = 0; fldidx < flds.size(); ++fldidx) {
            final DataDescriptor dkey = flds.get(fldidx);
            if (!dkey.isOkForVariable()) {
                if (dkey.f == 2 && dkey.x == 36) {
                    req.dpiTracker = new DpiTracker(dkey.dpi, dkey.dpi.getNfields());
                }
                if (out != null) {
                    out.f.format("%s %d %s (%s) %n", out.indent(), out.fldno++, dkey.name, dkey.getFxyName());
                }
            }
            else {
                final BitCounterCompressed counter = new BitCounterCompressed(dkey, ndatasets, bitOffset);
                fldCounters[fldidx] = counter;
                if (dkey.replication == 0) {
                    reader.setBitOffset(bitOffset);
                    final int count = (int)reader.bits2UInt(dkey.replicationCountSize);
                    bitOffset += dkey.replicationCountSize;
                    reader.bits2UInt(6);
                    if (null != out) {
                        out.f.format("%s--sequence %s bitOffset=%d replication=%s %n", out.indent(), dkey.getFxyName(), bitOffset, count);
                    }
                    bitOffset += 6;
                    counter.addNestedCounters(count);
                    bitOffset = this.makeArraySequenceCompressed(out, reader, counter, dkey, bitOffset, ndatasets, count, req);
                }
                else if (dkey.type == 3) {
                    if (null != out) {
                        out.f.format("%s--structure %s bitOffset=%d replication=%s %n", out.indent(), dkey.getFxyName(), bitOffset, dkey.replication);
                    }
                    counter.addNestedCounters(dkey.replication);
                    for (int i = 0; i < dkey.replication; ++i) {
                        final BitCounterCompressed[] nested = counter.getNestedCounters(i);
                        req.outerRow = i;
                        if (null != out) {
                            out.f.format("%n", new Object[0]);
                            out.indent.incr();
                            bitOffset = this.readData(out, reader, nested, dkey, bitOffset, ndatasets, req);
                            out.indent.decr();
                        }
                        else {
                            bitOffset = this.readData(null, reader, nested, dkey, bitOffset, ndatasets, req);
                        }
                    }
                }
                else {
                    IndexIterator iter = null;
                    ArrayStructure dataDpi = null;
                    if (req.map != null) {
                        final StructureMembers.Member m = req.map.get(dkey);
                        if (m == null) {
                            System.out.printf("HEY missing member %s%n", dkey);
                        }
                        iter = (IndexIterator)m.getDataObject();
                        if (iter == null) {
                            dataDpi = (ArrayStructure)m.getDataArray();
                        }
                    }
                    reader.setBitOffset(bitOffset);
                    if (dkey.type == 1) {
                        final int nc = dkey.bitWidth / 8;
                        final byte[] minValue = new byte[nc];
                        for (int j = 0; j < nc; ++j) {
                            minValue[j] = (byte)reader.bits2UInt(8);
                        }
                        final int dataWidth = (int)reader.bits2UInt(6);
                        counter.setDataWidth(8 * dataWidth);
                        final int totalWidth = dkey.bitWidth + 6 + 8 * dataWidth * ndatasets;
                        bitOffset += totalWidth;
                        if (null != out) {
                            out.f.format("%s read %d %s (%s) bitWidth=%d defValue=%s dataWidth=%d n=%d bitOffset=%d %n", out.indent(), out.fldno++, dkey.name, dkey.getFxyName(), dkey.bitWidth, new String(minValue, "UTF-8"), dataWidth, ndatasets, bitOffset);
                        }
                        for (int dataset = 0; dataset < ndatasets; ++dataset) {
                            if (dataWidth == 0) {
                                if (req.wantRow(dataset)) {
                                    for (int k = 0; k < nc; ++k) {
                                        iter.setCharNext((char)minValue[k]);
                                    }
                                }
                            }
                            else {
                                final int nt = Math.min(nc, dataWidth);
                                final byte[] incValue = new byte[nc];
                                for (int l = 0; l < nt; ++l) {
                                    incValue[l] = (byte)reader.bits2UInt(8);
                                }
                                for (int l = nt; l < nc; ++l) {
                                    incValue[l] = 0;
                                }
                                if (req.wantRow(dataset)) {
                                    for (int cval : incValue) {
                                        if (cval < 32 || cval > 126) {
                                            cval = 0;
                                        }
                                        iter.setCharNext((char)cval);
                                    }
                                }
                                if (out != null) {
                                    out.f.format(" %s,", new String(incValue, "UTF-8"));
                                }
                            }
                        }
                        if (out != null) {
                            out.f.format("%n", new Object[0]);
                        }
                    }
                    else {
                        int useBitWidth = dkey.bitWidth;
                        final boolean isDpi = dkey.f == 0 && dkey.x == 31 && dkey.y == 31;
                        boolean isDpiField = false;
                        if (dkey.f == 2 && dkey.x == 24 && dkey.y == 255) {
                            isDpiField = true;
                            final DataDescriptor dpiDD = req.dpiTracker.getDpiDD(req.outerRow);
                            useBitWidth = dpiDD.bitWidth;
                        }
                        final long dataMin = reader.bits2UInt(useBitWidth);
                        int dataWidth2 = (int)reader.bits2UInt(6);
                        if (dataWidth2 > useBitWidth && null != out) {
                            out.f.format(" BAD WIDTH ", new Object[0]);
                        }
                        if (dkey.type == 1) {
                            dataWidth2 *= 8;
                        }
                        counter.setDataWidth(dataWidth2);
                        final int totalWidth2 = useBitWidth + 6 + dataWidth2 * ndatasets;
                        bitOffset += totalWidth2;
                        if (null != out) {
                            out.f.format("%s read %d, %s (%s) bitWidth=%d dataMin=%d (%f) dataWidth=%d n=%d bitOffset=%d %n", out.indent(), out.fldno++, dkey.name, dkey.getFxyName(), useBitWidth, dataMin, dkey.convert(dataMin), dataWidth2, ndatasets, bitOffset);
                        }
                        for (int dataset2 = 0; dataset2 < ndatasets; ++dataset2) {
                            long value = dataMin;
                            if (dataWidth2 > 0) {
                                final long cv = reader.bits2UInt(dataWidth2);
                                if (BufrNumbers.isMissing(cv, dataWidth2)) {
                                    value = BufrNumbers.missingValue(useBitWidth);
                                }
                                else {
                                    value += cv;
                                }
                            }
                            if (dataWidth2 > useBitWidth) {
                                final long missingVal = BufrNumbers.missingValue(useBitWidth);
                                if ((value & missingVal) != value) {
                                    value = missingVal;
                                }
                            }
                            if (req.wantRow(dataset2)) {
                                if (isDpiField) {
                                    final DataDescriptor dpiDD2 = req.dpiTracker.getDpiDD(req.outerRow);
                                    final StructureMembers sms = dataDpi.getStructureMembers();
                                    final StructureMembers.Member m2 = sms.getMember(0);
                                    IndexIterator iter2 = (IndexIterator)m2.getDataObject();
                                    iter2.setObjectNext(dpiDD2.getName());
                                    final StructureMembers.Member m3 = sms.getMember(1);
                                    iter2 = (IndexIterator)m3.getDataObject();
                                    iter2.setFloatNext(dpiDD2.convert(value));
                                }
                                else {
                                    iter.setLongNext(value);
                                }
                            }
                            if (isDpi && dataset2 == 0) {
                                req.dpiTracker.setDpiValue(req.outerRow, value);
                            }
                            if (out != null && dataWidth2 > 0) {
                                out.f.format(" %d (%f)", value, dkey.convert(value));
                            }
                        }
                        if (out != null) {
                            out.f.format("%n", new Object[0]);
                        }
                    }
                }
            }
        }
        return bitOffset;
    }
    
    private int makeArraySequenceCompressed(final DebugOut out, final BitReader reader, final BitCounterCompressed bitCounterNested, final DataDescriptor seqdd, int bitOffset, final int ndatasets, final int count, final Request req) throws IOException {
        ArrayStructureMA ama = null;
        StructureMembers members = null;
        HashMap<DataDescriptor, StructureMembers.Member> nmap = null;
        if (req.map != null) {
            final Sequence seq = (Sequence)seqdd.refersTo;
            final int[] shape = { ndatasets, count };
            ama = ArrayStructureMA.factoryMA(seq, shape);
            setIterators(ama);
            members = ama.getStructureMembers();
            nmap = new HashMap<DataDescriptor, StructureMembers.Member>(2 * members.getMembers().size());
            this.associateMessage2Members(members, seqdd, nmap);
        }
        final Request nreq = new Request(ama, nmap, req.r);
        if (out != null) {
            out.indent.incr();
        }
        for (int i = 0; i < count; ++i) {
            final BitCounterCompressed[] nested = bitCounterNested.getNestedCounters(i);
            nreq.outerRow = i;
            bitOffset = this.readData(out, reader, nested, seqdd, bitOffset, ndatasets, nreq);
        }
        if (out != null) {
            out.indent.decr();
        }
        if (req.map != null) {
            final StructureMembers.Member m = req.map.get(seqdd);
            if (m == null) {
                System.out.printf("HEY missing seq %s%n", seqdd);
            }
            final ArrayObject arrObj = (ArrayObject)m.getDataArray();
            int start = 0;
            for (int j = 0; j < ndatasets; ++j) {
                final ArraySequence arrSeq = new ArraySequence(members, new SequenceIterator(start, count, ama), count);
                arrObj.setObject(j, arrSeq);
                start += count;
            }
        }
        return bitOffset;
    }
    
    private class Request
    {
        ArrayStructureMA ama;
        HashMap<DataDescriptor, StructureMembers.Member> map;
        Range r;
        DpiTracker dpiTracker;
        int outerRow;
        
        Request(final ArrayStructureMA ama, final HashMap<DataDescriptor, StructureMembers.Member> map, final Range r) {
            this.ama = ama;
            this.map = map;
            this.r = r;
        }
        
        boolean wantRow(final int row) {
            return this.ama != null && (this.r == null || this.r.contains(row));
        }
    }
    
    private class DpiTracker
    {
        DataDescriptorTreeConstructor.DataPresentIndicator dpi;
        boolean[] isPresent;
        List<DataDescriptor> dpiDD;
        
        DpiTracker(final DataDescriptorTreeConstructor.DataPresentIndicator dpi, final int nPresentFlags) {
            this.dpiDD = null;
            this.dpi = dpi;
            this.isPresent = new boolean[nPresentFlags];
        }
        
        void setDpiValue(final int fldidx, final long value) {
            this.isPresent[fldidx] = (value == 0L);
        }
        
        DataDescriptor getDpiDD(final int fldPresentIndex) {
            if (this.dpiDD == null) {
                this.dpiDD = new ArrayList<DataDescriptor>();
                for (int i = 0; i < this.isPresent.length; ++i) {
                    if (this.isPresent[i]) {
                        this.dpiDD.add(this.dpi.linear.get(i));
                    }
                }
            }
            return this.dpiDD.get(fldPresentIndex);
        }
        
        boolean isDpiDDs(final DataDescriptor dkey) {
            return dkey.f == 2 && dkey.x == 24 && dkey.y == 255;
        }
        
        boolean isDpiField(final DataDescriptor dkey) {
            return dkey.f == 2 && dkey.x == 24 && dkey.y == 255;
        }
    }
}
