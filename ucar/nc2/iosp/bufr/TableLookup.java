// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.iosp.bufr;

import org.slf4j.LoggerFactory;
import java.util.Iterator;
import java.util.ArrayList;
import java.util.List;
import java.io.IOException;
import ucar.nc2.iosp.bufr.tables.BufrTables;
import ucar.nc2.iosp.bufr.tables.TableD;
import ucar.nc2.iosp.bufr.tables.TableB;
import org.slf4j.Logger;

public final class TableLookup
{
    private static Logger log;
    private static final boolean showErrors = true;
    private TableB localTableB;
    private TableD localTableD;
    private TableB wmoTableB;
    private TableD wmoTableD;
    public BufrTables.Mode mode;
    
    public TableLookup(final BufrIdentificationSection ids) throws IOException {
        this.mode = BufrTables.Mode.wmoOnly;
        this.wmoTableB = BufrTables.getWmoTableB(ids);
        this.wmoTableD = BufrTables.getWmoTableD(ids);
        final BufrTables.Tables tables = BufrTables.getLocalTables(ids);
        if (tables != null) {
            this.localTableB = tables.b;
            this.localTableD = tables.d;
            this.mode = ((tables.mode == null) ? BufrTables.Mode.wmoOnly : tables.mode);
        }
    }
    
    public final String getWmoTableBName() {
        return this.wmoTableB.getName();
    }
    
    public final String getLocalTableBName() {
        return (this.localTableB == null) ? "none" : this.localTableB.getName();
    }
    
    public final String getLocalTableDName() {
        return (this.localTableD == null) ? "none" : this.localTableD.getName();
    }
    
    public final String getWmoTableDName() {
        return this.wmoTableD.getName();
    }
    
    public BufrTables.Mode getMode() {
        return this.mode;
    }
    
    public TableB.Descriptor getDescriptorTableB(final short fxy) {
        TableB.Descriptor b = null;
        final boolean isWmoRange = Descriptor.isWmoRange(fxy);
        if (isWmoRange && this.mode == BufrTables.Mode.wmoOnly) {
            b = this.wmoTableB.getDescriptor(fxy);
        }
        else if (isWmoRange && this.mode == BufrTables.Mode.wmoLocal) {
            b = this.wmoTableB.getDescriptor(fxy);
            if (b == null && this.localTableB != null) {
                b = this.localTableB.getDescriptor(fxy);
            }
        }
        else if (isWmoRange && this.mode == BufrTables.Mode.localOverride) {
            if (this.localTableB != null) {
                b = this.localTableB.getDescriptor(fxy);
            }
            if (b == null) {
                b = this.wmoTableB.getDescriptor(fxy);
            }
            else {
                b.setLocalOverride(true);
            }
        }
        else if (!isWmoRange && this.localTableB != null) {
            b = this.localTableB.getDescriptor(fxy);
        }
        if (b == null) {
            System.out.printf(" TableLookup cant find Table B descriptor = %s in tables %s, %s mode=%s%n", Descriptor.makeString(fxy), this.getLocalTableBName(), this.getWmoTableBName(), this.mode);
        }
        return b;
    }
    
    public List<Short> getDescriptorsTableD(final short id) {
        final TableD.Descriptor d = this.getDescriptorTableD(id);
        if (d != null) {
            return d.getSequence();
        }
        return null;
    }
    
    public List<String> getDescriptorsTableD(final String fxy) {
        final short id = Descriptor.getFxy(fxy);
        final List<Short> seq = this.getDescriptorsTableD(id);
        if (seq == null) {
            return null;
        }
        final List<String> result = new ArrayList<String>(seq.size());
        for (final Short s : seq) {
            result.add(Descriptor.makeString(s));
        }
        return result;
    }
    
    public TableD.Descriptor getDescriptorTableD(final short fxy) {
        TableD.Descriptor d = null;
        final boolean isWmoRange = Descriptor.isWmoRange(fxy);
        if (isWmoRange && this.mode == BufrTables.Mode.wmoOnly) {
            d = this.wmoTableD.getDescriptor(fxy);
        }
        else if (isWmoRange && this.mode == BufrTables.Mode.wmoLocal) {
            d = this.wmoTableD.getDescriptor(fxy);
            if (d == null && this.localTableD != null) {
                d = this.localTableD.getDescriptor(fxy);
            }
        }
        else if (isWmoRange && this.mode == BufrTables.Mode.localOverride) {
            if (this.localTableD != null) {
                d = this.localTableD.getDescriptor(fxy);
            }
            if (d == null) {
                d = this.wmoTableD.getDescriptor(fxy);
            }
            else {
                d.setLocalOverride(true);
            }
        }
        else if (this.localTableD != null) {
            d = this.localTableD.getDescriptor(fxy);
        }
        if (d == null) {
            System.out.printf(" TableLookup cant find Table D descriptor %s in tables %s,%s mode=%s%n", Descriptor.makeString(fxy), this.getLocalTableDName(), this.getWmoTableDName(), this.mode);
        }
        return d;
    }
    
    static {
        TableLookup.log = LoggerFactory.getLogger(TableLookup.class);
    }
}
