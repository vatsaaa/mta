// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.iosp.bufr;

import java.util.Date;
import java.util.GregorianCalendar;
import java.io.IOException;
import ucar.unidata.io.RandomAccessFile;

public class BufrIdentificationSection
{
    private final int master_table;
    private final int subcenter_id;
    private final int center_id;
    private final int update_sequence;
    private final boolean hasOptionalSection;
    private int optionalSectionLen;
    private long optionalSectionPos;
    private final int category;
    private final int subCategory;
    private final int localSubCategory;
    private final int master_table_version;
    private final int local_table_version;
    private final int year;
    private final int month;
    private final int day;
    private final int hour;
    private final int minute;
    private final int second;
    private final byte[] localUse;
    
    public BufrIdentificationSection(final RandomAccessFile raf, final BufrIndicatorSection is) throws IOException {
        final int length = BufrNumbers.int3(raf);
        this.master_table = raf.read();
        if (is.getBufrEdition() < 4) {
            if (is.getBufrEdition() == 2) {
                this.subcenter_id = 255;
                this.center_id = BufrNumbers.int2(raf);
            }
            else {
                this.subcenter_id = raf.read();
                this.center_id = raf.read();
            }
            this.update_sequence = raf.read();
            final int optional = raf.read();
            this.hasOptionalSection = ((optional & 0x80) != 0x0);
            this.category = raf.read();
            this.subCategory = raf.read();
            this.localSubCategory = -1;
            this.master_table_version = raf.read();
            this.local_table_version = raf.read();
            int lyear = raf.read();
            if (lyear > 100) {
                lyear -= 100;
            }
            this.year = lyear + 2000;
            this.month = raf.read();
            this.day = raf.read();
            this.hour = raf.read();
            this.minute = raf.read();
            this.second = 0;
            final int n = length - 17;
            raf.read(this.localUse = new byte[n]);
        }
        else {
            this.center_id = BufrNumbers.int2(raf);
            this.subcenter_id = BufrNumbers.int2(raf);
            this.update_sequence = raf.read();
            final int optional = raf.read();
            this.hasOptionalSection = ((optional & 0x40) != 0x0);
            this.category = raf.read();
            this.subCategory = raf.read();
            this.localSubCategory = raf.read();
            this.master_table_version = raf.read();
            this.local_table_version = raf.read();
            this.year = BufrNumbers.int2(raf);
            this.month = raf.read();
            this.day = raf.read();
            this.hour = raf.read();
            this.minute = raf.read();
            this.second = raf.read();
            final int n2 = length - 22;
            raf.read(this.localUse = new byte[n2]);
        }
        if (this.hasOptionalSection) {
            int optionalLen = BufrNumbers.int3(raf);
            if (optionalLen % 2 != 0) {
                ++optionalLen;
            }
            this.optionalSectionLen = optionalLen - 4;
            raf.skipBytes(1);
            this.optionalSectionPos = raf.getFilePointer();
            raf.skipBytes(this.optionalSectionLen);
        }
    }
    
    public final int getCenterId() {
        return this.center_id;
    }
    
    public final int getSubCenterId() {
        return this.subcenter_id;
    }
    
    public final int getUpdateSequence() {
        return this.update_sequence;
    }
    
    public final Date getReferenceTime(final GregorianCalendar cal) {
        cal.clear();
        cal.set(this.year, this.month - 1, this.day, this.hour, this.minute, this.second);
        return cal.getTime();
    }
    
    public final int getCategory() {
        return this.category;
    }
    
    public final int getSubCategory() {
        return this.subCategory;
    }
    
    public final int getLocalSubCategory() {
        return this.localSubCategory;
    }
    
    public final int getMasterTableId() {
        return this.master_table;
    }
    
    public final int getMasterTableVersion() {
        return this.master_table_version;
    }
    
    public final int getLocalTableVersion() {
        return this.local_table_version;
    }
    
    public final byte[] getLocalUseBytes() {
        return this.localUse;
    }
    
    public final byte[] getOptionalSection(final RandomAccessFile raf) throws IOException {
        if (!this.hasOptionalSection) {
            return null;
        }
        final byte[] optionalSection = new byte[this.optionalSectionLen - 4];
        raf.seek(this.optionalSectionPos);
        raf.read(optionalSection);
        return optionalSection;
    }
}
