// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.iosp.bufr.tables;

public class TableC
{
    private static final String[] tableCdesc;
    
    public static String getOperatorName(final int index) {
        if (index < 0 || index >= TableC.tableCdesc.length) {
            return "unknown";
        }
        return (TableC.tableCdesc[index] == null) ? "unknown" : TableC.tableCdesc[index];
    }
    
    static {
        (tableCdesc = new String[38])[1] = "change data width";
        TableC.tableCdesc[2] = "change scale";
        TableC.tableCdesc[3] = "change reference value";
        TableC.tableCdesc[4] = "add associated field";
        TableC.tableCdesc[5] = "signify character";
        TableC.tableCdesc[6] = "signify data width for next descriptor";
        TableC.tableCdesc[21] = "data not present";
        TableC.tableCdesc[22] = "quality information follows";
        TableC.tableCdesc[23] = "substituted values operator";
        TableC.tableCdesc[24] = "first order statistics";
        TableC.tableCdesc[25] = "difference statistics";
        TableC.tableCdesc[32] = "replaced/retained values";
        TableC.tableCdesc[35] = "cancel backward data reference";
        TableC.tableCdesc[36] = "define data present bit-map";
        TableC.tableCdesc[37] = "use/cancel data present bit-map";
    }
}
