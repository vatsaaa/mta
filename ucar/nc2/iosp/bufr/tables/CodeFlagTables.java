// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.iosp.bufr.tables;

import org.slf4j.LoggerFactory;
import java.io.IOException;
import ucar.unidata.util.StringUtil;
import java.io.Reader;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.List;
import java.util.Iterator;
import org.jdom.Document;
import java.io.InputStream;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;
import java.util.HashMap;
import java.util.Map;
import org.slf4j.Logger;

public class CodeFlagTables
{
    private static Logger log;
    static Map<Short, CodeFlagTables> tableMap;
    private static boolean showReadErrs;
    private static boolean showNameDiff;
    private short fxy;
    private String name;
    private Map<Integer, String> map;
    
    public static CodeFlagTables getTable(final short id) {
        if (CodeFlagTables.tableMap == null) {
            init();
        }
        return CodeFlagTables.tableMap.get(id);
    }
    
    public static boolean hasTable(final short id) {
        if (CodeFlagTables.tableMap == null) {
            init();
        }
        final CodeFlagTables result = CodeFlagTables.tableMap.get(id);
        return result != null;
    }
    
    private static void init() {
        init2(CodeFlagTables.tableMap = new HashMap<Short, CodeFlagTables>(300));
    }
    
    private static void initOld(final Map<Short, CodeFlagTables> table) {
        final String filename = "/resources/bufrTables/wmo/Code-FlagTables.xml";
        final InputStream is = CodeFlagTables.class.getResourceAsStream(filename);
        try {
            final SAXBuilder builder = new SAXBuilder();
            final Document tdoc = builder.build(is);
            final Element root = tdoc.getRootElement();
            for (final Element elem : root.getChildren("table")) {
                final String kind = elem.getAttributeValue("kind");
                if (kind != null) {
                    if (!kind.equals("code")) {
                        continue;
                    }
                    final List<Element> cElems = (List<Element>)elem.getChildren("code");
                    if (cElems.size() == 0) {
                        continue;
                    }
                    final String name = elem.getAttributeValue("name");
                    final String desc = elem.getAttributeValue("desc");
                    final CodeFlagTables ct = new CodeFlagTables(getFxy(name), desc);
                    table.put(ct.fxy, ct);
                    for (final Element cElem : cElems) {
                        final String valueS = cElem.getAttributeValue("value").trim();
                        final String text = cElem.getText();
                        if (text.toLowerCase().startsWith("reserved")) {
                            continue;
                        }
                        if (text.toLowerCase().startsWith("not used")) {
                            continue;
                        }
                        try {
                            final int value = Integer.parseInt(valueS);
                            ct.addValue(value, text);
                        }
                        catch (NumberFormatException e2) {
                            CodeFlagTables.log.warn("NumberFormatException on '" + valueS + "' for CodeTable " + name + " in " + filename);
                        }
                    }
                }
            }
        }
        catch (Exception e) {
            CodeFlagTables.log.error("Can't read BUFR code table " + filename, e);
        }
    }
    
    private static short getFxy(final String name) {
        try {
            final String[] tok = name.split(" ");
            final int f = (tok.length > 0) ? Integer.parseInt(tok[0]) : 0;
            final int x = (tok.length > 1) ? Integer.parseInt(tok[1]) : 0;
            final int y = (tok.length > 2) ? Integer.parseInt(tok[2]) : 0;
            return (short)((f << 14) + (x << 8) + y);
        }
        catch (NumberFormatException e) {
            CodeFlagTables.log.warn("Illegal table name=" + name);
            return 0;
        }
    }
    
    private static void init2(final Map<Short, CodeFlagTables> table) {
        final String filename = "/resources/bufrTables/wmo/BC_CodeFlagTable.csv";
        BufferedReader dataIS = null;
        try {
            final InputStream is = CodeFlagTables.class.getResourceAsStream(filename);
            dataIS = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF8")));
            int count = 0;
            while (true) {
                String line = dataIS.readLine();
                if (line == null) {
                    break;
                }
                if (line.startsWith("#")) {
                    continue;
                }
                if (++count == 1) {
                    if (!CodeFlagTables.showReadErrs) {
                        continue;
                    }
                    System.out.println("header line == " + line);
                }
                else {
                    final int pos1 = line.indexOf(34);
                    if (pos1 >= 0) {
                        final int pos2 = line.indexOf(34, pos1 + 1);
                        final StringBuffer sb = new StringBuffer(line);
                        for (int i = pos1; i < pos2; ++i) {
                            if (sb.charAt(i) == ',') {
                                sb.setCharAt(i, ' ');
                            }
                        }
                        line = sb.toString();
                    }
                    final String[] flds = line.split(",");
                    if (flds.length < 4) {
                        if (!CodeFlagTables.showReadErrs) {
                            continue;
                        }
                        System.out.printf("%d BAD split == %s%n", count, line);
                    }
                    else {
                        int fldidx = 0;
                        try {
                            final int sno = Integer.parseInt(flds[fldidx++].trim());
                            final int xy = Integer.parseInt(flds[fldidx++].trim());
                            int no = -1;
                            try {
                                no = Integer.parseInt(flds[fldidx++].trim());
                            }
                            catch (Exception e3) {
                                if (!CodeFlagTables.showReadErrs) {
                                    continue;
                                }
                                System.out.printf("%d skip == %s%n", count, line);
                                continue;
                            }
                            final String name = StringUtil.remove(flds[fldidx++], 34);
                            final String nameLow = name.toLowerCase();
                            if (nameLow.startsWith("reserved")) {
                                continue;
                            }
                            if (nameLow.startsWith("not used")) {
                                continue;
                            }
                            final int x = xy / 1000;
                            final int y = xy % 1000;
                            final int fxy = (x << 8) + y;
                            CodeFlagTables ct = table.get((short)fxy);
                            if (ct == null) {
                                ct = new CodeFlagTables((short)fxy, null);
                                table.put(ct.fxy, ct);
                            }
                            ct.addValue(no, name);
                        }
                        catch (Exception e4) {
                            if (!CodeFlagTables.showReadErrs) {
                                continue;
                            }
                            System.out.printf("%d %d BAD line == %s%n", count, fldidx, line);
                        }
                    }
                }
            }
        }
        catch (Exception e) {
            CodeFlagTables.log.error("Can't read BUFR code table " + filename, e);
        }
        finally {
            if (dataIS != null) {
                try {
                    dataIS.close();
                }
                catch (IOException e2) {
                    e2.printStackTrace();
                }
            }
        }
    }
    
    public static void main(final String[] arg) throws IOException {
        final HashMap<Short, CodeFlagTables> tableMap1 = new HashMap<Short, CodeFlagTables>(300);
        initOld(tableMap1);
        final HashMap<Short, CodeFlagTables> tableMap2 = new HashMap<Short, CodeFlagTables>(300);
        init2(tableMap2);
        System.out.printf("Compare 1 with 2%n", new Object[0]);
        for (final Short key : tableMap1.keySet()) {
            final CodeFlagTables t = tableMap1.get(key);
            final CodeFlagTables t2 = tableMap2.get(key);
            if (t2 == null) {
                System.out.printf(" NOT FOUND in 2: %s (%d)%n", t.fxy(), t.fxy);
            }
            else {
                if (t.fxy().equals("0-21-76")) {
                    System.out.println("HEY");
                }
                for (final Integer no : t.map.keySet()) {
                    final String name1 = t.map.get(no);
                    final String name2 = t2.map.get(no);
                    if (name2 == null) {
                        System.out.printf(" %s val %d name '%s' missing%n", t.fxy(), no, name1);
                    }
                    else {
                        if (!CodeFlagTables.showNameDiff || name1.equals(name2)) {
                            continue;
                        }
                        System.out.printf(" %s names different%n  %s%n  %s%n", t.fxy(), name1, name2);
                    }
                }
            }
        }
        System.out.printf("Compare 2 with 1%n", new Object[0]);
        for (final Short key : tableMap2.keySet()) {
            final CodeFlagTables t = tableMap2.get(key);
            final CodeFlagTables t3 = tableMap1.get(key);
            if (t3 == null) {
                System.out.printf(" NOT FOUND in 1: %s (%d)%n", t.fxy(), t.fxy);
            }
            else {
                for (final Integer no : t.map.keySet()) {
                    final String name3 = t.map.get(no);
                    final String name4 = t3.map.get(no);
                    if (name4 == null) {
                        System.out.printf(" %s val %d name '%s' missing%n", t.fxy(), no, name3);
                    }
                    else {
                        if (!CodeFlagTables.showNameDiff || name3.equals(name4)) {
                            continue;
                        }
                        System.out.printf(" %s names different%n  %s%n  %s%n", t.fxy(), name3, name4);
                    }
                }
            }
        }
    }
    
    private CodeFlagTables(final short fxy, final String name) {
        this.fxy = fxy;
        this.name = ((name == null) ? this.fxy() : (StringUtil.replace(name, ' ', "_") + "(" + this.fxy() + ")"));
        this.map = new HashMap<Integer, String>(20);
    }
    
    public String getName() {
        return this.name;
    }
    
    public Map<Integer, String> getMap() {
        return this.map;
    }
    
    private void addValue(final int value, final String text) {
        this.map.put(value, text);
    }
    
    String fxy() {
        final int f = this.fxy >> 16;
        final int x = (this.fxy & 0xFF00) >> 8;
        final int y = this.fxy & 0xFF;
        return f + "-" + x + "-" + y;
    }
    
    @Override
    public String toString() {
        return this.name;
    }
    
    static {
        CodeFlagTables.log = LoggerFactory.getLogger(CodeFlagTables.class);
        CodeFlagTables.showReadErrs = false;
        CodeFlagTables.showNameDiff = false;
    }
}
