// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.iosp.bufr.tables;

import net.jcip.annotations.Immutable;
import java.util.Iterator;
import java.util.List;
import java.util.Collections;
import java.util.ArrayList;
import java.util.Formatter;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class TableB
{
    private String name;
    private String location;
    private Map<Short, Descriptor> map;
    
    public TableB(final String name, final String location) {
        this.name = name;
        this.location = location;
        this.map = new HashMap<Short, Descriptor>();
    }
    
    void addDescriptor(final short x, final short y, final int scale, final int refVal, final int width, final String name, final String units) {
        final short id = (short)((x << 8) + y);
        this.map.put(id, new Descriptor(x, y, scale, refVal, width, name, units));
    }
    
    public String getName() {
        return this.name;
    }
    
    public String getLocation() {
        return this.location;
    }
    
    public Descriptor getDescriptor(final short id) {
        return this.map.get(id);
    }
    
    public Collection<Descriptor> getDescriptors() {
        return this.map.values();
    }
    
    public Collection<Short> getKeys() {
        return this.map.keySet();
    }
    
    public void show(final Formatter out) {
        final List<Short> sortKeys = new ArrayList<Short>(this.getKeys());
        Collections.sort(sortKeys);
        out.format("Table B %s %n", this.name);
        for (final Short key : sortKeys) {
            final Descriptor dd = this.getDescriptor(key);
            dd.show(out);
            out.format("%n", new Object[0]);
        }
    }
    
    public static class Composite extends TableB
    {
        List<TableB> list;
        
        public Composite(final String name, final String location) {
            super(name, location);
            this.list = new ArrayList<TableB>(3);
        }
        
        public void addTable(final TableB b) {
            this.list.add(b);
        }
        
        @Override
        public Descriptor getDescriptor(final short id) {
            for (final TableB b : this.list) {
                final Descriptor d = b.getDescriptor(id);
                if (d != null) {
                    return d;
                }
            }
            return null;
        }
        
        @Override
        public Collection<Descriptor> getDescriptors() {
            final ArrayList<Descriptor> result = new ArrayList<Descriptor>(3000);
            for (final TableB b : this.list) {
                result.addAll(b.getDescriptors());
            }
            return result;
        }
        
        @Override
        public Collection<Short> getKeys() {
            final ArrayList<Short> result = new ArrayList<Short>(3000);
            for (final TableB b : this.list) {
                result.addAll(b.getKeys());
            }
            return result;
        }
    }
    
    @Immutable
    public class Descriptor implements Comparable<Descriptor>
    {
        private final short x;
        private final short y;
        private final int scale;
        private final int refVal;
        private final int dataWidth;
        private final String units;
        private final String name;
        private final boolean numeric;
        private boolean localOverride;
        
        Descriptor(final short x, final short y, final int scale, final int refVal, final int width, final String name, final String units) {
            this.x = x;
            this.y = y;
            this.scale = scale;
            this.refVal = refVal;
            this.dataWidth = width;
            this.name = name.trim();
            this.units = units.trim().intern();
            this.numeric = !this.units.startsWith("CCITT");
        }
        
        public int getScale() {
            return this.scale;
        }
        
        public int getRefVal() {
            return this.refVal;
        }
        
        public int getDataWidth() {
            return this.dataWidth;
        }
        
        public String getUnits() {
            return this.units;
        }
        
        public String getName() {
            return this.name;
        }
        
        public short getId() {
            return (short)((this.x << 8) + this.y);
        }
        
        public String getFxy() {
            return "0-" + this.x + "-" + this.y;
        }
        
        public boolean isNumeric() {
            return this.numeric;
        }
        
        public boolean isLocal() {
            return this.x >= 48 || this.y >= 192;
        }
        
        public void setLocalOverride(final boolean isOverride) {
            this.localOverride = isOverride;
        }
        
        public boolean getLocalOverride() {
            return this.localOverride;
        }
        
        @Override
        public String toString() {
            final Formatter out = new Formatter();
            this.show(out);
            return out.toString();
        }
        
        void show(final Formatter out) {
            out.format(" %8s scale=%d refVal=%d width=%d  units=(%s) name=(%s)", this.getFxy(), this.scale, this.refVal, this.dataWidth, this.units, this.name);
        }
        
        public int compareTo(final Descriptor o) {
            return this.getId() - o.getId();
        }
    }
}
