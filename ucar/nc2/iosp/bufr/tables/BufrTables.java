// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.iosp.bufr.tables;

import java.util.concurrent.ConcurrentHashMap;
import org.slf4j.LoggerFactory;
import java.util.Formatter;
import java.io.FileInputStream;
import java.net.URL;
import java.util.regex.Matcher;
import ucar.nc2.iosp.bufr.Descriptor;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import ucar.nc2.util.TableParser;
import ucar.unidata.util.StringUtil;
import ucar.nc2.iosp.bufr.BufrIdentificationSection;
import java.io.InputStream;
import java.io.IOException;
import java.io.Reader;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.Iterator;
import java.io.FileNotFoundException;
import java.io.File;
import java.util.ArrayList;
import java.util.regex.Pattern;
import java.util.Map;
import java.util.List;
import org.slf4j.Logger;

public class BufrTables
{
    static final String RESOURCE_PATH = "/resources/bufrTables/";
    private static Logger log;
    private static final boolean debugTable = false;
    private static final boolean showReadErrs = false;
    private static List<TableConfig> tables;
    private static Map<String, TableB> tablesB;
    private static Map<String, TableD> tablesD;
    private static final String canonicalLookup = "resource:/resources/bufrTables/local/tablelookup.csv";
    private static List<String> lookups;
    private static String version13;
    private static String version14;
    private static final Pattern threeInts;
    private static final Pattern negOne;
    
    public static void addLookupFile(final String filename) throws FileNotFoundException {
        if (BufrTables.lookups == null) {
            BufrTables.lookups = new ArrayList<String>();
        }
        final File f = new File(filename);
        if (!f.exists()) {
            throw new FileNotFoundException(filename + " not found");
        }
        BufrTables.lookups.add(filename);
    }
    
    private static void readTableLookup() {
        BufrTables.tables = new ArrayList<TableConfig>();
        if (BufrTables.lookups != null) {
            BufrTables.lookups.add("resource:/resources/bufrTables/local/tablelookup.csv");
            for (final String fname : BufrTables.lookups) {
                readTableLookup(fname);
            }
        }
        else {
            readTableLookup("resource:/resources/bufrTables/local/tablelookup.csv");
        }
    }
    
    private static void readTableLookup(final String filename) {
        try {
            final InputStream ios = openStream(filename);
            final BufferedReader dataIS = new BufferedReader(new InputStreamReader(ios, Charset.forName("UTF8")));
            int count = 0;
            while (true) {
                final String line = dataIS.readLine();
                if (line == null) {
                    break;
                }
                if (line.startsWith("#")) {
                    continue;
                }
                ++count;
                final String[] flds = line.split(",");
                if (flds.length < 8) {
                    continue;
                }
                int fldidx = 0;
                try {
                    final TableConfig table = new TableConfig();
                    table.center = Integer.parseInt(flds[fldidx++].trim());
                    table.subcenter = Integer.parseInt(flds[fldidx++].trim());
                    table.master = Integer.parseInt(flds[fldidx++].trim());
                    table.local = Integer.parseInt(flds[fldidx++].trim());
                    table.cat = Integer.parseInt(flds[fldidx++].trim());
                    table.tableBname = flds[fldidx++].trim();
                    table.tableBformat = flds[fldidx++].trim();
                    table.tableDname = flds[fldidx++].trim();
                    table.tableDformat = flds[fldidx++].trim();
                    if (fldidx < flds.length) {
                        final String modes = flds[fldidx++].trim();
                        if (modes.equalsIgnoreCase("wmoLocal")) {
                            table.mode = Mode.wmoLocal;
                        }
                        else if (modes.equalsIgnoreCase("localWmo")) {
                            table.mode = Mode.localOverride;
                        }
                    }
                    BufrTables.tables.add(table);
                }
                catch (Exception ex) {}
            }
            dataIS.close();
        }
        catch (IOException ioe) {
            final String mess = "Need BUFR tables in path; looking for " + filename;
            throw new RuntimeException(mess, ioe);
        }
    }
    
    private static TableConfig matchTableConfig(final int center, final int subcenter, final int master, final int local, final int cat) {
        if (BufrTables.tables == null) {
            readTableLookup();
        }
        for (final TableConfig tc : BufrTables.tables) {
            if (tc.matches(center, subcenter, master, local, cat)) {
                return tc;
            }
        }
        return null;
    }
    
    private static TableConfig matchTableConfig(final BufrIdentificationSection ids) {
        if (BufrTables.tables == null) {
            readTableLookup();
        }
        final int center = ids.getCenterId();
        final int subcenter = ids.getSubCenterId();
        final int master = ids.getMasterTableVersion();
        final int local = ids.getLocalTableVersion();
        final int cat = ids.getCategory();
        return matchTableConfig(center, subcenter, master, local, cat);
    }
    
    public static Tables getLocalTables(final BufrIdentificationSection ids) throws IOException {
        final TableConfig tc = matchTableConfig(ids);
        if (tc == null) {
            return null;
        }
        if (!tc.tableBformat.equals("ncep-nm")) {
            final Tables tables = new Tables();
            tables.b = readTableB(tc.tableBname, tc.tableBformat, false);
            tables.d = readTableD(tc.tableDname, tc.tableDformat, false);
            tables.mode = tc.mode;
            return tables;
        }
        TableB b = BufrTables.tablesB.get(tc.tableBname);
        TableD d = BufrTables.tablesD.get(tc.tableBname);
        if (b != null && d != null) {
            return new Tables(b, d, tc.mode);
        }
        b = new TableB(tc.tableBname, tc.tableBname);
        d = new TableD(tc.tableBname, tc.tableBname);
        final Tables t = new Tables(b, d, tc.mode);
        final InputStream ios = openStream(tc.tableBname);
        NcepMnemonic.read(ios, t);
        BufrTables.tablesB.put(tc.tableBname, t.b);
        BufrTables.tablesD.put(tc.tableBname, t.d);
        return t;
    }
    
    public static TableB getWmoTableB(final BufrIdentificationSection ids) throws IOException {
        return getWmoTableB(ids.getMasterTableVersion());
    }
    
    public static TableB getWmoTableB(final int version) throws IOException {
        final String tableName = (version == 14) ? BufrTables.version14 : BufrTables.version13;
        final TableB tb = BufrTables.tablesB.get(tableName);
        if (tb != null) {
            return tb;
        }
        final TableConfig tc14 = matchTableConfig(0, 0, 14, 0, -1);
        TableB result = readTableB(tc14.tableBname, tc14.tableBformat, false);
        BufrTables.tablesB.put(BufrTables.version14, result);
        if (version < 14) {
            final TableConfig tc15 = matchTableConfig(0, 0, 13, 0, -1);
            final TableB b13 = readTableB(tc15.tableBname, tc15.tableBformat, false);
            final TableB.Composite bb = new TableB.Composite(BufrTables.version13, BufrTables.version13);
            bb.addTable(b13);
            bb.addTable(result);
            result = bb;
            BufrTables.tablesB.put(BufrTables.version13, result);
        }
        return result;
    }
    
    public static TableB readTableB(final String location, final String format, final boolean force) throws IOException {
        if (!force) {
            final TableB tb = BufrTables.tablesB.get(location);
            if (tb != null) {
                return tb;
            }
        }
        final InputStream ios = openStream(location);
        final TableB b = new TableB(location, location);
        if (format.equals("csv")) {
            readWmoTableB(ios, b);
        }
        else if (format.equals("ncep")) {
            readNcepTableB(ios, b);
        }
        else if (format.equals("ncep-nm")) {
            final Tables t = new Tables(b, null, null);
            NcepMnemonic.read(ios, t);
        }
        else if (format.equals("ecmwf")) {
            readEcmwfTableB(ios, b);
        }
        else if (format.equals("ukmet")) {
            readBmetTableB(ios, b);
        }
        else if (format.equals("mel-bufr")) {
            readMelbufrTableB(ios, b);
        }
        else if (format.equals("mel-tabs")) {
            readMeltabTableB(ios, b);
        }
        else {
            if (!format.equals("wmo-xml")) {
                System.out.printf("Unknown format= %s %n", format);
                return null;
            }
            readWmoXmlTableB(ios, b);
        }
        BufrTables.tablesB.put(location, b);
        return b;
    }
    
    private static void readWmoTableB(final InputStream ios, final TableB b) throws IOException {
        final BufferedReader dataIS = new BufferedReader(new InputStreamReader(ios, Charset.forName("UTF8")));
        int count = 0;
        while (true) {
            String line = dataIS.readLine();
            if (line == null) {
                break;
            }
            if (line.startsWith("#")) {
                continue;
            }
            if (++count == 1) {
                continue;
            }
            final int pos1 = line.indexOf(34);
            if (pos1 >= 0) {
                final int pos2 = line.indexOf(34, pos1 + 1);
                final StringBuffer sb = new StringBuffer(line);
                for (int i = pos1; i < pos2; ++i) {
                    if (sb.charAt(i) == ',') {
                        sb.setCharAt(i, ' ');
                    }
                }
                line = sb.toString();
            }
            final String[] flds = line.split(",");
            if (flds.length < 7) {
                continue;
            }
            int fldidx = 0;
            try {
                final int classId = Integer.parseInt(flds[fldidx++].trim());
                final int xy = Integer.parseInt(flds[fldidx++].trim());
                final String name = StringUtil.remove(flds[fldidx++], 34);
                final String units = StringUtil.filter(flds[fldidx++], " %+-_/()*");
                final int scale = Integer.parseInt(clean(flds[fldidx++].trim()));
                final int refVal = Integer.parseInt(clean(flds[fldidx++].trim()));
                final int width = Integer.parseInt(clean(flds[fldidx++].trim()));
                final int x = xy / 1000;
                final int y = xy % 1000;
                b.addDescriptor((short)x, (short)y, scale, refVal, width, name, units);
            }
            catch (Exception ex) {}
        }
        dataIS.close();
    }
    
    private static String clean(final String s) {
        return StringUtil.remove(s, 32);
    }
    
    private static TableB readMelbufrTableB(final InputStream ios, final TableB b) throws IOException {
        final BufferedReader dataIS = new BufferedReader(new InputStreamReader(ios));
        while (true) {
            final String line = dataIS.readLine();
            if (line == null) {
                break;
            }
            if (line.startsWith("#")) {
                continue;
            }
            if (line.length() == 0) {
                continue;
            }
            try {
                final String[] split = line.split(";");
                final short x = Short.parseShort(split[1].trim());
                final short y = Short.parseShort(split[2].trim());
                final int scale = Integer.parseInt(split[3].trim());
                final int refVal = Integer.parseInt(split[4].trim());
                final int width = Integer.parseInt(split[5].trim());
                b.addDescriptor(x, y, scale, refVal, width, split[7], split[6]);
            }
            catch (Exception e) {
                BufrTables.log.error("Bad table B entry: table=" + b.getName() + " entry=<" + line + ">", e.getMessage());
            }
        }
        dataIS.close();
        return b;
    }
    
    private static TableB readMeltabTableB(final InputStream ios, final TableB b) throws IOException {
        final BufferedReader dataIS = new BufferedReader(new InputStreamReader(ios));
        while (true) {
            final String line = dataIS.readLine();
            if (line == null) {
                break;
            }
            if (line.startsWith("#")) {
                continue;
            }
            if (line.length() == 0) {
                continue;
            }
            try {
                final String[] split = line.split("\t");
                final short x = Short.parseShort(split[1].trim());
                final short y = Short.parseShort(split[2].trim());
                final int scale = Integer.parseInt(split[3].trim());
                final int refVal = Integer.parseInt(split[4].trim());
                final int width = Integer.parseInt(split[5].trim());
                b.addDescriptor(x, y, scale, refVal, width, split[7], split[6]);
            }
            catch (Exception e) {
                BufrTables.log.error("Bad table " + b.getName() + " entry=<" + line + ">", e);
            }
        }
        dataIS.close();
        return b;
    }
    
    private static TableB readNcepTableB(final InputStream ios, final TableB b) throws IOException {
        final BufferedReader dataIS = new BufferedReader(new InputStreamReader(ios));
        dataIS.readLine();
        final int count = 0;
        while (true) {
            final String line = dataIS.readLine();
            if (line == null) {
                break;
            }
            if (line.startsWith("#")) {
                continue;
            }
            if (line.length() == 0) {
                continue;
            }
            try {
                final String[] flds = line.split("[\\|;]");
                if (flds[0].equals("END")) {
                    break;
                }
                if (flds.length < 8) {
                    BufrTables.log.error("Bad line in table " + b.getName() + " entry=<" + line + ">");
                }
                else {
                    final String fxys = flds[0];
                    final int scale = Integer.parseInt(clean(flds[1]));
                    final int refVal = Integer.parseInt(clean(flds[2]));
                    final int width = Integer.parseInt(clean(flds[3]));
                    final String units = StringUtil.remove(flds[4], 34);
                    final String name = StringUtil.remove(flds[7], 34);
                    final String[] xyflds = fxys.split("-");
                    final short x = Short.parseShort(clean(xyflds[1]));
                    final short y = Short.parseShort(clean(xyflds[2]));
                    b.addDescriptor(x, y, scale, refVal, width, name, units);
                }
            }
            catch (Exception e) {
                BufrTables.log.error("Bad table " + b.getName() + " entry=<" + line + ">", e);
            }
        }
        dataIS.close();
        return b;
    }
    
    private static TableB readEcmwfTableB(final InputStream ios, final TableB b) throws IOException {
        final int count = 0;
        final List<TableParser.Record> recs = TableParser.readTable(ios, "4i,7i,72,97,102i,114i,119i", 50000);
        for (final TableParser.Record record : recs) {
            if (record.nfields() < 7) {
                continue;
            }
            final int x = (int)record.get(0);
            final int y = (int)record.get(1);
            final String name = (String)record.get(2);
            final String units = (String)record.get(3);
            final int scale = (int)record.get(4);
            final int ref = (int)record.get(5);
            final int width = (int)record.get(6);
            b.addDescriptor((short)x, (short)y, scale, ref, width, name, units);
        }
        ios.close();
        return b;
    }
    
    private static void readBmetTableB(final InputStream ios, final TableB b) throws IOException {
        Document doc;
        try {
            final SAXBuilder builder = new SAXBuilder();
            doc = builder.build(ios);
        }
        catch (JDOMException e) {
            throw new IOException(e.getMessage());
        }
        final Element root = doc.getRootElement();
        final List<Element> featList = (List<Element>)root.getChildren("featureCatalogue");
        for (final Element featureCat : featList) {
            final List<Element> features = (List<Element>)featureCat.getChildren("feature");
            for (final Element feature : features) {
                final String name = feature.getChild("annotation").getChildTextNormalize("documentation");
                final int f = Integer.parseInt(feature.getChildText("F"));
                final int x = Integer.parseInt(feature.getChildText("X"));
                final int y = Integer.parseInt(feature.getChildText("Y"));
                final int fxy = (f << 16) + (x << 8) + y;
                final Element bufrElem = feature.getChild("BUFR");
                final String units = bufrElem.getChildTextNormalize("BUFR_units");
                int scale = 0;
                int reference = 0;
                int width = 0;
                String s = null;
                try {
                    s = bufrElem.getChildTextNormalize("BUFR_scale");
                    scale = Integer.parseInt(clean(s));
                }
                catch (NumberFormatException e2) {
                    System.out.printf(" key %s name '%s' has bad scale='%s'%n", fxy, name, s);
                }
                try {
                    s = bufrElem.getChildTextNormalize("BUFR_reference");
                    reference = Integer.parseInt(clean(s));
                }
                catch (NumberFormatException e2) {
                    System.out.printf(" key %s name '%s' has bad reference='%s' %n", fxy, name, s);
                }
                try {
                    s = bufrElem.getChildTextNormalize("BUFR_width");
                    width = Integer.parseInt(clean(s));
                }
                catch (NumberFormatException e2) {
                    System.out.printf(" key %s name '%s' has bad width='%s' %n", fxy, name, s);
                }
                b.addDescriptor((short)x, (short)y, scale, reference, width, name, units);
            }
        }
        ios.close();
    }
    
    private static void readWmoXmlTableB(final InputStream ios, final TableB b) throws IOException {
        Document doc;
        try {
            final SAXBuilder builder = new SAXBuilder();
            doc = builder.build(ios);
        }
        catch (JDOMException e) {
            throw new IOException(e.getMessage());
        }
        final Element root = doc.getRootElement();
        final List<Element> featList = (List<Element>)root.getChildren("BC_TableB_BUFR14_1_0_CREX_6_1_0");
        for (final Element elem : featList) {
            final String name = elem.getChildTextNormalize("ElementName_E");
            final String units = elem.getChildTextNormalize("BUFR_Unit");
            int x = 0;
            int y = 0;
            int scale = 0;
            int reference = 0;
            int width = 0;
            String fxy = null;
            String s = null;
            try {
                fxy = elem.getChildTextNormalize("FXY");
                final int xy = Integer.parseInt(clean(fxy));
                x = xy / 1000;
                y = xy % 1000;
            }
            catch (NumberFormatException e2) {
                System.out.printf(" key %s name '%s' has bad scale='%s'%n", fxy, name, s);
            }
            try {
                s = elem.getChildTextNormalize("BUFR_Scale");
                scale = Integer.parseInt(clean(s));
            }
            catch (NumberFormatException e2) {
                System.out.printf(" key %s name '%s' has bad scale='%s'%n", fxy, name, s);
            }
            try {
                s = elem.getChildTextNormalize("BUFR_ReferenceValue");
                reference = Integer.parseInt(clean(s));
            }
            catch (NumberFormatException e2) {
                System.out.printf(" key %s name '%s' has bad reference='%s' %n", fxy, name, s);
            }
            try {
                s = elem.getChildTextNormalize("BUFR_DataWidth_Bits");
                width = Integer.parseInt(clean(s));
            }
            catch (NumberFormatException e2) {
                System.out.printf(" key %s name '%s' has bad width='%s' %n", fxy, name, s);
            }
            b.addDescriptor((short)x, (short)y, scale, reference, width, name, units);
        }
        ios.close();
    }
    
    public static TableD getWmoTableD(final BufrIdentificationSection ids) throws IOException {
        final TableD tb = BufrTables.tablesD.get(BufrTables.version14);
        if (tb != null) {
            return tb;
        }
        final TableConfig tc14 = matchTableConfig(0, 0, 14, 0, -1);
        final TableD result = readTableD(tc14.tableDname, tc14.tableDformat, false);
        BufrTables.tablesD.put(BufrTables.version14, result);
        return result;
    }
    
    public static TableD readTableD(final String location, final String format, final boolean force) throws IOException {
        if (location == null) {
            return null;
        }
        if (location.trim().length() == 0) {
            return null;
        }
        if (!force) {
            final TableD tb = BufrTables.tablesD.get(location);
            if (tb != null) {
                return tb;
            }
        }
        final InputStream ios = openStream(location);
        final TableD d = new TableD(location, location);
        if (format.equals("csv")) {
            readWmoTableD(ios, d);
        }
        else if (format.equals("ncep")) {
            readNcepTableD(ios, d);
        }
        else if (format.equals("ncep-nm")) {
            final Tables t = new Tables(null, d, null);
            NcepMnemonic.read(ios, t);
        }
        else if (format.equals("ecmwf")) {
            readEcmwfTableD(ios, d);
        }
        else if (format.equals("mel-bufr")) {
            readMelbufrTableD(ios, d);
        }
        else {
            if (!format.equals("wmo-xml")) {
                System.out.printf("Unknown format= %s %n", format);
                return null;
            }
            readWmoXmlTableD(ios, d);
        }
        BufrTables.tablesD.put(location, d);
        return d;
    }
    
    private static void readWmoTableD(final InputStream ios, final TableD tableD) throws IOException {
        final BufferedReader dataIS = new BufferedReader(new InputStreamReader(ios, Charset.forName("UTF-8")));
        int count = 0;
        int currSeqno = -1;
        TableD.Descriptor currDesc = null;
        while (true) {
            String line = dataIS.readLine();
            if (line == null) {
                break;
            }
            if (line.startsWith("#")) {
                continue;
            }
            if (++count == 1) {
                continue;
            }
            final int pos1 = line.indexOf(34);
            if (pos1 >= 0) {
                final int pos2 = line.indexOf(34, pos1 + 1);
                final StringBuffer sb = new StringBuffer(line);
                for (int i = pos1; i < pos2; ++i) {
                    if (sb.charAt(i) == ',') {
                        sb.setCharAt(i, ' ');
                    }
                }
                line = sb.toString();
            }
            final String[] flds = line.split(",");
            if (flds.length < 5) {
                continue;
            }
            int fldidx = 0;
            try {
                final int sno = Integer.parseInt(flds[fldidx++]);
                final int cat = Integer.parseInt(flds[fldidx++]);
                final int seq = Integer.parseInt(flds[fldidx++]);
                String seqName = flds[fldidx++];
                final String featno = flds[fldidx++].trim();
                if (featno.length() == 0) {
                    continue;
                }
                final String featName = (flds.length > 5) ? flds[fldidx++] : "n/a";
                if (currSeqno != seq) {
                    final int y = seq % 1000;
                    final int w = seq / 1000;
                    final int x = w % 100;
                    seqName = StringUtil.remove(seqName, 34);
                    currDesc = tableD.addDescriptor((short)x, (short)y, seqName, new ArrayList<Short>());
                    currSeqno = seq;
                }
                final int fno = Integer.parseInt(featno);
                final int y2 = fno % 1000;
                final int w2 = fno / 1000;
                final int x2 = w2 % 100;
                final int f = w2 / 100;
                final int fxy = (f << 14) + (x2 << 8) + y2;
                currDesc.addFeature((short)fxy);
            }
            catch (Exception ex) {}
        }
        dataIS.close();
    }
    
    private static void readWmoXmlTableD(final InputStream ios, final TableD tableD) throws IOException {
        Document doc;
        try {
            final SAXBuilder builder = new SAXBuilder();
            doc = builder.build(ios);
        }
        catch (JDOMException e) {
            throw new IOException(e.getMessage());
        }
        int currSeqno = -1;
        TableD.Descriptor currDesc = null;
        final Element root = doc.getRootElement();
        final List<Element> featList = (List<Element>)root.getChildren("B_TableD_BUFR14_1_0_CREX_6_1_0");
        for (final Element elem : featList) {
            final String seqs = elem.getChildTextNormalize("FXY1");
            final int seq = Integer.parseInt(seqs);
            if (currSeqno != seq) {
                final int y = seq % 1000;
                final int w = seq / 1000;
                final int x = w % 100;
                final String seqName = elem.getChildTextNormalize("ElementName1_E");
                currDesc = tableD.addDescriptor((short)x, (short)y, seqName, new ArrayList<Short>());
                currSeqno = seq;
            }
            final String fnos = elem.getChildTextNormalize("FXY2");
            final int fno = Integer.parseInt(fnos);
            final int y2 = fno % 1000;
            final int w2 = fno / 1000;
            final int x2 = w2 % 100;
            final int f = w2 / 100;
            final int fxy = (f << 14) + (x2 << 8) + y2;
            currDesc.addFeature((short)fxy);
        }
        ios.close();
    }
    
    private static void readMelbufrTableD(final InputStream ios, final TableD t) throws IOException {
        final BufferedReader dataIS = new BufferedReader(new InputStreamReader(ios));
        int count = 0;
        while (true) {
            String line = dataIS.readLine();
            if (line == null) {
                break;
            }
            ++count;
            if (line.startsWith("#")) {
                continue;
            }
            if (line.length() == 0) {
                continue;
            }
            line = line.trim();
            final String[] split = line.split("[ \t]+");
            if (split.length < 3) {
                continue;
            }
            if (split[0].equals("END")) {
                break;
            }
            try {
                final short seqF = Short.parseShort(split[0]);
                final short seqX = Short.parseShort(split[1]);
                final short seqY = Short.parseShort(split[2]);
                assert seqF == 3;
                String seqName = "";
                if (split.length > 3) {
                    final StringBuilder sb = new StringBuilder(40);
                    for (int i = 3; i < split.length; ++i) {
                        sb.append(split[i]).append(" ");
                    }
                    seqName = sb.toString();
                    seqName = StringUtil.remove(seqName, "()");
                }
                final List<Short> seq = new ArrayList<Short>();
                while (true) {
                    line = dataIS.readLine();
                    if (line == null) {
                        break;
                    }
                    ++count;
                    if (line.startsWith("#")) {
                        continue;
                    }
                    if (line.length() == 0) {
                        continue;
                    }
                    Matcher m = BufrTables.threeInts.matcher(line);
                    if (m.find()) {
                        final short f = Short.parseShort(m.group(1));
                        final short x = Short.parseShort(m.group(2));
                        final short y = Short.parseShort(m.group(3));
                        seq.add(Descriptor.getFxy(f, x, y));
                    }
                    else {
                        m = BufrTables.negOne.matcher(line);
                        if (m.find()) {
                            t.addDescriptor(seqX, seqY, seqName, seq);
                            break;
                        }
                        continue;
                    }
                }
            }
            catch (Exception e) {
                BufrTables.log.warn("TableD " + t.getName() + " Failed on line " + count + " = " + line + "\n " + e);
            }
        }
        dataIS.close();
    }
    
    private static void readNcepTableD(final InputStream ios, final TableD t) throws IOException {
        final BufferedReader dataIS = new BufferedReader(new InputStreamReader(ios));
        dataIS.readLine();
        TableD.Descriptor currDesc = null;
        final int count = 0;
        while (true) {
            final String line = dataIS.readLine();
            if (line == null) {
                break;
            }
            if (line.startsWith("#")) {
                continue;
            }
            if (line.trim().length() == 0) {
                continue;
            }
            try {
                final String[] flds = line.split("[\\|;]");
                if (flds[0].equals("END")) {
                    break;
                }
                String fxys = flds[0].trim();
                if (fxys.length() > 0) {
                    final String[] xyflds = fxys.split("-");
                    final short x = Short.parseShort(clean(xyflds[1]));
                    final short y = Short.parseShort(clean(xyflds[2]));
                    final String seqName = (flds.length > 3) ? flds[3].trim() : "";
                    currDesc = t.addDescriptor(x, y, seqName, new ArrayList<Short>());
                }
                else {
                    fxys = StringUtil.remove(flds[1], 62);
                    final String[] xyflds = fxys.split("-");
                    final short f = Short.parseShort(clean(xyflds[0]));
                    final short x2 = Short.parseShort(clean(xyflds[1]));
                    final short y2 = Short.parseShort(clean(xyflds[2]));
                    final int fxy = (f << 14) + (x2 << 8) + y2;
                    currDesc.addFeature((short)fxy);
                }
            }
            catch (Exception e) {
                BufrTables.log.error("Bad table " + t.getName() + " entry=<" + line + ">", e);
            }
        }
        dataIS.close();
    }
    
    private static void readEcmwfTableD(final InputStream ios, final TableD t) throws IOException {
        final BufferedReader dataIS = new BufferedReader(new InputStreamReader(ios));
        TableD.Descriptor currDesc = null;
        int n = 0;
        while (true) {
            String line = dataIS.readLine();
            if (line == null) {
                break;
            }
            line = line.trim();
            if (line.startsWith("#")) {
                continue;
            }
            if (line.length() == 0) {
                continue;
            }
            try {
                final String[] flds = line.split("[\\s]+");
                String fxys;
                if (n == 0) {
                    fxys = flds[0].trim();
                    int fxy = Integer.parseInt(fxys);
                    final int y = fxy % 1000;
                    fxy /= 1000;
                    final int x = fxy % 100;
                    currDesc = t.addDescriptor((short)x, (short)y, "", new ArrayList<Short>());
                    n = Integer.parseInt(flds[1]);
                    fxys = flds[2].trim();
                }
                else {
                    fxys = flds[0].trim();
                }
                int fxy = Integer.parseInt(fxys);
                final int y = fxy % 1000;
                fxy /= 1000;
                final int x = fxy % 100;
                final int f;
                fxy = (f = fxy / 100);
                fxy = (f << 14) + (x << 8) + y;
                currDesc.addFeature((short)fxy);
                --n;
            }
            catch (Exception e) {
                BufrTables.log.error("Bad table " + t.getName() + " entry=<" + line + ">", e);
            }
        }
        dataIS.close();
    }
    
    private static InputStream open(final String location) throws IOException {
        InputStream ios = null;
        final String tmp = "/resources/bufrTables/local/" + location;
        ios = BufrTables.class.getResourceAsStream(tmp);
        if (ios != null) {
            return ios;
        }
        if (location.startsWith("http:")) {
            final URL url = new URL(location);
            ios = url.openStream();
        }
        else {
            ios = new FileInputStream(location);
        }
        return ios;
    }
    
    static InputStream openStream(String location) throws IOException {
        InputStream ios = null;
        if (!location.startsWith("resource:")) {
            if (location.startsWith("http:")) {
                final URL url = new URL(location);
                ios = url.openStream();
            }
            else {
                ios = new FileInputStream(location);
            }
            return ios;
        }
        location = location.substring(9);
        ios = BufrTables.class.getResourceAsStream(location);
        if (ios == null) {
            throw new RuntimeException("resource not found=<" + location + ">");
        }
        return ios;
    }
    
    public static void main(final String[] args) throws IOException {
        final Formatter out = new Formatter(System.out);
        final TableB tableB = getWmoTableB(13);
        tableB.show(out);
        final TableD tableD = getWmoTableD(null);
        tableD.show(out);
    }
    
    static {
        BufrTables.log = LoggerFactory.getLogger(BufrTables.class);
        BufrTables.tablesB = new ConcurrentHashMap<String, TableB>();
        BufrTables.tablesD = new ConcurrentHashMap<String, TableD>();
        BufrTables.lookups = null;
        BufrTables.version13 = "wmo.v13.composite";
        BufrTables.version14 = "wmo.v14";
        threeInts = Pattern.compile("^\\s*(\\d+)\\s+(\\d+)\\s+(\\d+)");
        negOne = Pattern.compile("^\\s*-1");
    }
    
    public enum Mode
    {
        wmoOnly, 
        wmoLocal, 
        localOverride;
    }
    
    private static class TableConfig
    {
        int center;
        int subcenter;
        int master;
        int local;
        int cat;
        String tableBname;
        String tableBformat;
        String tableDname;
        String tableDformat;
        Mode mode;
        
        private TableConfig() {
            this.mode = Mode.wmoLocal;
        }
        
        boolean matches(final int center, final int subcenter, final int master, final int local, final int cat) {
            return (this.center < 0 || center < 0 || center == this.center) && (this.subcenter < 0 || subcenter < 0 || subcenter == this.subcenter) && (this.master < 0 || master < 0 || master == this.master) && (this.local < 0 || local < 0 || local == this.local) && (this.cat < 0 || cat < 0 || cat == this.cat);
        }
        
        @Override
        public String toString() {
            return this.tableBname;
        }
    }
    
    public static class Tables
    {
        public TableB b;
        public TableD d;
        public Mode mode;
        
        Tables() {
        }
        
        Tables(final TableB b, final TableD d, final Mode mode) {
            this.b = b;
            this.d = d;
            this.mode = ((mode == null) ? Mode.wmoOnly : mode);
        }
    }
}
