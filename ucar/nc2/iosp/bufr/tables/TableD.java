// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.iosp.bufr.tables;

import ucar.nc2.iosp.bufr.Descriptor;
import java.util.Iterator;
import java.util.Collections;
import java.util.ArrayList;
import java.util.Formatter;
import java.util.Collection;
import java.util.List;
import java.util.HashMap;
import java.util.Map;

public class TableD
{
    private String name;
    private String location;
    private Map<Short, Descriptor> map;
    
    public TableD(final String name, final String location) {
        this.name = name;
        this.location = location;
        this.map = new HashMap<Short, Descriptor>();
    }
    
    public String getName() {
        return this.name;
    }
    
    public String getLocation() {
        return this.location;
    }
    
    Descriptor addDescriptor(final short x, final short y, final String name, final List<Short> seq) {
        final short id = (short)(49152 + (x << 8) + y);
        final Descriptor d = new Descriptor(x, y, name, seq);
        this.map.put(id, d);
        return d;
    }
    
    public Descriptor getDescriptor(final short id) {
        return this.map.get(id);
    }
    
    public Collection<Descriptor> getDescriptors() {
        return this.map.values();
    }
    
    public void show(final Formatter out) {
        final Collection<Short> keys = this.map.keySet();
        final List<Short> sortKeys = new ArrayList<Short>(keys);
        Collections.sort(sortKeys);
        out.format("Table D %s %n", this.name);
        for (final Short key : sortKeys) {
            final Descriptor dd = this.map.get(key);
            dd.show(out, true);
        }
    }
    
    public class Descriptor implements Comparable<Descriptor>
    {
        private short x;
        private short y;
        private String name;
        private List<Short> seq;
        private boolean localOverride;
        
        Descriptor(final short x, final short y, final String name, final List<Short> seq) {
            this.x = x;
            this.y = y;
            this.name = name;
            this.seq = seq;
        }
        
        public List<Short> getSequence() {
            return this.seq;
        }
        
        public void addFeature(final short f) {
            this.seq.add(f);
        }
        
        public String getName() {
            return this.name;
        }
        
        public short getId() {
            return (short)(49152 + (this.x << 8) + this.y);
        }
        
        public String getFxy() {
            return "3-" + this.x + "-" + this.y;
        }
        
        @Override
        public String toString() {
            return this.getFxy() + " " + this.getName();
        }
        
        public void show(final Formatter out, final boolean oneline) {
            out.format(" %8s: name=(%s) seq=", this.getFxy(), this.name);
            if (oneline) {
                for (final short s : this.seq) {
                    out.format(" %s,", ucar.nc2.iosp.bufr.Descriptor.makeString(s));
                }
                out.format("%n", new Object[0]);
            }
            else {
                for (final short s : this.seq) {
                    out.format("    %s%n", ucar.nc2.iosp.bufr.Descriptor.makeString(s));
                }
            }
        }
        
        public int compareTo(final Descriptor o) {
            return this.getId() - o.getId();
        }
        
        public boolean isLocal() {
            return this.x >= 48 || this.y >= 192;
        }
        
        public void setLocalOverride(final boolean isOverride) {
            this.localOverride = isOverride;
        }
        
        public boolean getLocalOverride() {
            return this.localOverride;
        }
    }
}
