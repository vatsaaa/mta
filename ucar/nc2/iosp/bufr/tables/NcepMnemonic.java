// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.iosp.bufr.tables;

import java.util.Formatter;
import java.io.IOException;
import java.util.List;
import java.util.Iterator;
import java.util.regex.Matcher;
import ucar.nc2.iosp.bufr.Descriptor;
import java.util.ArrayList;
import java.util.StringTokenizer;
import java.io.Reader;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.io.InputStream;
import java.util.regex.Pattern;

public class NcepMnemonic
{
    private static final Pattern fields3;
    private static final Pattern fields2;
    private static final Pattern fields5;
    private static final Pattern ints0123;
    private static final Pattern ints6;
    private static final int XlocalCutoff = 48;
    private static final int YlocalCutoff = 192;
    private static final boolean debugTable = false;
    
    public static boolean read(final InputStream ios, final BufrTables.Tables tables) throws IOException {
        if (ios == null) {
            return false;
        }
        if (tables.b == null) {
            tables.b = new TableB("fake", "fake");
        }
        if (tables.d == null) {
            tables.d = new TableD("fake", "fake");
        }
        final HashMap<String, String> number = new HashMap<String, String>();
        final HashMap<String, String> desc = new HashMap<String, String>();
        final HashMap<String, String> mnseq = new HashMap<String, String>();
        final BufferedReader dataIS = new BufferedReader(new InputStreamReader(ios));
        String line;
        do {
            line = dataIS.readLine();
        } while (!line.contains("MNEMONIC"));
        while (true) {
            line = dataIS.readLine();
            if (line.contains("MNEMONIC")) {
                break;
            }
            if (line.contains("----")) {
                continue;
            }
            if (line.startsWith("*")) {
                continue;
            }
            if (line.startsWith("|       ")) {
                continue;
            }
            final Matcher m = NcepMnemonic.fields3.matcher(line);
            if (!m.find()) {
                continue;
            }
            if (m.group(2).startsWith("3")) {
                number.put(m.group(1).trim(), m.group(2));
                desc.put(m.group(1).trim(), m.group(3).replace("TABLE D ENTRY - ", "").trim());
            }
            else if (m.group(2).startsWith("0")) {
                number.put(m.group(1).trim(), m.group(2));
                desc.put(m.group(1).trim(), m.group(3).replace("TABLE B ENTRY - ", "").trim());
            }
            else {
                if (!m.group(2).startsWith("A")) {
                    continue;
                }
                number.put(m.group(1).trim(), m.group(2));
                desc.put(m.group(1).trim(), m.group(3).replace("TABLE A ENTRY - ", "").trim());
            }
        }
        while (true) {
            line = dataIS.readLine();
            if (line.contains("MNEMONIC")) {
                break;
            }
            if (line.contains("----")) {
                continue;
            }
            if (line.startsWith("|       ")) {
                continue;
            }
            if (line.startsWith("*")) {
                continue;
            }
            final Matcher m = NcepMnemonic.fields2.matcher(line);
            if (!m.find()) {
                continue;
            }
            if (mnseq.containsKey(m.group(1).trim())) {
                String value = mnseq.get(m.group(1).trim());
                value = value + " " + m.group(2);
                mnseq.put(m.group(1).trim(), value);
            }
            else {
                mnseq.put(m.group(1).trim(), m.group(2));
            }
        }
        for (final String key : mnseq.keySet()) {
            String seq = mnseq.get(key);
            seq = seq.replaceAll("\\<", "1-1-0 0-31-0 ");
            seq = seq.replaceAll("\\>", "");
            seq = seq.replaceAll("\\{", "1-1-0 0-31-1 ");
            seq = seq.replaceAll("\\}", "");
            seq = seq.replaceAll("\\(", "1-1-0 0-31-2 ");
            seq = seq.replaceAll("\\)", "");
            final StringTokenizer stoke = new StringTokenizer(seq, " ");
            final List<Short> list = new ArrayList<Short>();
            while (stoke.hasMoreTokens()) {
                String mn = stoke.nextToken();
                if (mn.charAt(1) == '-') {
                    list.add(Descriptor.getFxy(mn));
                }
                else {
                    final Matcher m = NcepMnemonic.ints6.matcher(mn);
                    if (m.find()) {
                        final String F = mn.substring(0, 1);
                        final String X = removeLeading0(mn.substring(1, 3));
                        final String Y = removeLeading0(mn.substring(3));
                        list.add(Descriptor.getFxy(F + "-" + X + "-" + Y));
                    }
                    else {
                        if (mn.startsWith("\"")) {
                            final int idx = mn.lastIndexOf(34);
                            final String count = mn.substring(idx + 1);
                            list.add(Descriptor.getFxy("1-1-" + count));
                            mn = mn.substring(1, idx);
                        }
                        if (mn.startsWith(".")) {
                            final String des = mn.substring(mn.length() - 4);
                            mn = mn.replace(des, "....");
                        }
                        final String fxy = number.get(mn);
                        final String F2 = fxy.substring(0, 1);
                        final String X2 = removeLeading0(fxy.substring(1, 3));
                        final String Y2 = removeLeading0(fxy.substring(3));
                        list.add(Descriptor.getFxy(F2 + "-" + X2 + "-" + Y2));
                    }
                }
            }
            final String fxy2 = number.get(key);
            String F = fxy2.substring(0, 1);
            if (F.equals("A")) {
                F = "3";
            }
            final String X = removeLeading0(fxy2.substring(1, 3));
            final String Y = removeLeading0(fxy2.substring(3));
            if (48 > Integer.parseInt(X) && 192 > Integer.parseInt(Y)) {
                continue;
            }
            final short seqX = Short.parseShort(X.trim());
            final short seqY = Short.parseShort(Y.trim());
            tables.d.addDescriptor(seqX, seqY, key, list);
        }
        List<Short> list2 = new ArrayList<Short>();
        list2.add(Descriptor.getFxy("1-1-0"));
        list2.add(Descriptor.getFxy("0-31-2"));
        tables.d.addDescriptor((short)60, (short)1, "", list2);
        list2 = new ArrayList<Short>();
        list2.add(Descriptor.getFxy("1-1-0"));
        list2.add(Descriptor.getFxy("0-31-1"));
        tables.d.addDescriptor((short)60, (short)2, "", list2);
        list2 = new ArrayList<Short>();
        list2.add(Descriptor.getFxy("1-1-0"));
        list2.add(Descriptor.getFxy("0-31-1"));
        tables.d.addDescriptor((short)60, (short)3, "", list2);
        list2 = new ArrayList<Short>();
        list2.add(Descriptor.getFxy("1-1-0"));
        list2.add(Descriptor.getFxy("0-31-0"));
        tables.d.addDescriptor((short)60, (short)4, "", list2);
        while (true) {
            final String line2 = dataIS.readLine();
            if (line2 == null) {
                break;
            }
            if (line2.contains("MNEMONIC")) {
                break;
            }
            if (line2.startsWith("|       ")) {
                continue;
            }
            if (line2.startsWith("*")) {
                continue;
            }
            final Matcher m = NcepMnemonic.fields5.matcher(line2);
            if (!m.find()) {
                continue;
            }
            if (m.group(1).equals("")) {
                continue;
            }
            if (!number.containsKey(m.group(1).trim())) {
                continue;
            }
            final String fxy3 = number.get(m.group(1).trim());
            final String F3 = fxy3.substring(0, 1);
            final String X3 = fxy3.substring(1, 3);
            final String Y3 = fxy3.substring(3);
            final String name = desc.get(m.group(1).trim());
            if (48 > Integer.parseInt(X3) && 192 > Integer.parseInt(Y3)) {
                continue;
            }
            final short x = Short.parseShort(X3.trim());
            final short y = Short.parseShort(Y3.trim());
            final int scale = Integer.parseInt(m.group(2).trim());
            final int refVal = Integer.parseInt(m.group(3).trim());
            final int width = Integer.parseInt(m.group(4).trim());
            tables.b.addDescriptor(x, y, scale, refVal, width, name, m.group(5).trim());
        }
        tables.b.addDescriptor((short)63, (short)0, 0, 0, 16, "Byte count", "Numeric");
        dataIS.close();
        return true;
    }
    
    private static String removeLeading0(String number) {
        if (number.length() == 2 && number.startsWith("0")) {
            number = number.substring(1);
        }
        else if (number.length() == 3 && number.startsWith("00")) {
            number = number.substring(2);
        }
        else if (number.length() == 3 && number.startsWith("0")) {
            number = number.substring(1);
        }
        return number;
    }
    
    public static void main(final String[] args) throws IOException {
        final InputStream ios = BufrTables.openStream("bufrtab.ETACLS1");
        final BufrTables.Tables tables = new BufrTables.Tables();
        read(ios, tables);
        final Formatter out = new Formatter(System.out);
        tables.b.show(out);
        tables.d.show(out);
    }
    
    static {
        fields3 = Pattern.compile("^\\|\\s+(.*)\\s+\\|\\s+(.*)\\s+\\|\\s+(.*)\\s*\\|");
        fields2 = Pattern.compile("^\\|\\s+(.*)\\s+\\|\\s+(.*)\\s+\\|");
        fields5 = Pattern.compile("^\\|\\s+(.*)\\s+\\|\\s+(.*)\\s+\\|\\s+(.*)\\s+\\|\\s+(.*)\\s+\\|\\s+(.*)\\s+\\|");
        ints0123 = Pattern.compile("^(0|1|2|3)");
        ints6 = Pattern.compile("^\\d{6}");
    }
}
