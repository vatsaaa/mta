// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.iosp.bufr.tables;

import java.util.HashMap;
import java.io.Reader;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.Iterator;
import java.util.List;
import java.io.InputStream;
import java.io.IOException;
import ucar.nc2.util.TableParser;
import java.util.Map;

public class CommonCodeTables
{
    private static String[] tableC1;
    private static String[] tableA;
    private static Map<Integer, String> tableC12;
    private static Map<Integer, String> tableC13;
    
    private static void initTableA() {
        final String location = "/resources/bufrTables/wmo/TableA-11-2008.txt";
        final InputStream ios = BufrTables.class.getResourceAsStream(location);
        CommonCodeTables.tableA = new String[256];
        try {
            final List<TableParser.Record> recs = TableParser.readTable(ios, "3i,60", 255);
            for (final TableParser.Record record : recs) {
                final int no = (int)record.get(0);
                String name = (String)record.get(1);
                name = name.trim();
                CommonCodeTables.tableA[no] = name;
            }
        }
        catch (IOException ioe) {}
        finally {
            if (ios != null) {
                try {
                    ios.close();
                }
                catch (IOException ex) {}
            }
        }
    }
    
    private static void initC1() {
        final String location = "/resources/bufrTables/wmo/wmoTableC1.txt";
        final InputStream ios = BufrTables.class.getResourceAsStream(location);
        CommonCodeTables.tableC1 = new String[256];
        try {
            String prev = null;
            final List<TableParser.Record> recs = TableParser.readTable(ios, "8,13i,120", 500);
            for (final TableParser.Record record : recs) {
                final int no = (int)record.get(1);
                String name = (String)record.get(2);
                name = name.trim();
                CommonCodeTables.tableC1[no] = (name.equals(")") ? prev : name);
                prev = name;
            }
        }
        catch (IOException ioe) {}
        finally {
            if (ios != null) {
                try {
                    ios.close();
                }
                catch (IOException ex) {}
            }
        }
    }
    
    private static void initC12() {
        final String location = "/resources/bufrTables/wmo/wmoTableC12.txt";
        final InputStream ios = BufrTables.class.getResourceAsStream(location);
        final BufferedReader dataIS = new BufferedReader(new InputStreamReader(ios, Charset.forName("UTF-8")));
        CommonCodeTables.tableC12 = new HashMap<Integer, String>(200);
        int count = 0;
        int center_id = 0;
        int subcenter_id = 0;
        try {
            while (true) {
                final String line = dataIS.readLine();
                ++count;
                if (line == null) {
                    break;
                }
                if (line.startsWith("#")) {
                    continue;
                }
                final String[] flds = line.split("[ \t]+");
                if (flds[0].startsWith("00")) {
                    center_id = Integer.parseInt(flds[0]);
                }
                else {
                    subcenter_id = Integer.parseInt(flds[1]);
                    final StringBuffer sbuff = new StringBuffer();
                    for (int i = 2; i < flds.length; ++i) {
                        if (i > 2) {
                            sbuff.append(" ");
                        }
                        sbuff.append(flds[i]);
                    }
                    final int subid = center_id << 16 + subcenter_id;
                    CommonCodeTables.tableC12.put(subid, sbuff.toString());
                }
            }
        }
        catch (IOException ioe) {}
        finally {
            if (ios != null) {
                try {
                    ios.close();
                }
                catch (IOException ex) {}
            }
        }
    }
    
    private static void initC13() {
        final String location = "/resources/bufrTables/wmo/wmoTableC13.txt";
        final InputStream ios = BufrTables.class.getResourceAsStream(location);
        final BufferedReader dataIS = new BufferedReader(new InputStreamReader(ios, Charset.forName("UTF-8")));
        CommonCodeTables.tableC13 = new HashMap<Integer, String>(200);
        int count = 0;
        int cat = 0;
        int subcat = 0;
        try {
            while (true) {
                final String line = dataIS.readLine();
                ++count;
                if (line == null) {
                    break;
                }
                if (line.startsWith("#")) {
                    continue;
                }
                final String[] flds = line.split("[ \t]+");
                if (flds[0].length() > 0) {
                    cat = Integer.parseInt(flds[0]);
                }
                else {
                    subcat = Integer.parseInt(flds[1]);
                    final StringBuffer sbuff = new StringBuffer();
                    for (int i = 2; i < flds.length; ++i) {
                        if (i > 2) {
                            sbuff.append(" ");
                        }
                        sbuff.append(flds[i]);
                    }
                    final int subid = cat << 16 + subcat;
                    CommonCodeTables.tableC13.put(subid, sbuff.toString());
                }
            }
        }
        catch (IOException ioe) {}
        finally {
            if (ios != null) {
                try {
                    ios.close();
                }
                catch (IOException ex) {}
            }
        }
    }
    
    public static String getCenterName(final int center_id) {
        if (CommonCodeTables.tableC1 == null) {
            initC1();
        }
        final String result = (center_id < 0 || center_id > 255) ? null : CommonCodeTables.tableC1[center_id];
        return (result != null) ? result : ("Unknown center=" + center_id);
    }
    
    public static String getSubCenterName(final int center_id, final int subcenter_id) {
        if (CommonCodeTables.tableC12 == null) {
            initC12();
        }
        final int subid = center_id << 16 + subcenter_id;
        return CommonCodeTables.tableC12.get(subid);
    }
    
    public static String getDataSubcategoy(final int cat, final int subcat) {
        if (CommonCodeTables.tableC13 == null) {
            initC13();
        }
        final int subid = cat << 16 + subcat;
        return CommonCodeTables.tableC13.get(subid);
    }
    
    public static String getDataCategory(final int cat) {
        if (CommonCodeTables.tableA == null) {
            initTableA();
        }
        final String result = (cat < 0 || cat > 255) ? null : CommonCodeTables.tableA[cat];
        return (result != null) ? result : ("Unknown category=" + cat);
    }
    
    public static void main(final String[] arg) throws IOException {
        initTableA();
    }
    
    static {
        CommonCodeTables.tableC1 = null;
        CommonCodeTables.tableA = null;
        CommonCodeTables.tableC12 = null;
        CommonCodeTables.tableC13 = null;
    }
}
