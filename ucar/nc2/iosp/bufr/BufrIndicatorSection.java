// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.iosp.bufr;

import java.io.IOException;
import ucar.unidata.io.RandomAccessFile;

public class BufrIndicatorSection
{
    private long startPos;
    private final int bufrLength;
    private final int edition;
    
    public BufrIndicatorSection(final RandomAccessFile raf) throws IOException {
        this.startPos = raf.getFilePointer() - 4L;
        this.bufrLength = BufrNumbers.uint3(raf);
        this.edition = raf.read();
    }
    
    public final int getBufrLength() {
        return this.bufrLength;
    }
    
    public final int getBufrEdition() {
        return this.edition;
    }
    
    public final long getStartPos() {
        return this.startPos;
    }
}
