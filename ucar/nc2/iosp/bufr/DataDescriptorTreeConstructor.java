// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.iosp.bufr;

import org.slf4j.LoggerFactory;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import org.slf4j.Logger;

public class DataDescriptorTreeConstructor
{
    private static Logger log;
    private DataDescriptor root;
    private DataDescriptor changeWidth;
    private DataDescriptor changeScale;
    private DataDescriptor changeRefval;
    private DataPresentIndicator dpi;
    
    public DataDescriptorTreeConstructor() {
        this.changeWidth = null;
        this.changeScale = null;
        this.changeRefval = null;
        this.dpi = null;
    }
    
    public DataDescriptor factory(final TableLookup lookup, final BufrDataDescriptionSection dds) {
        this.root = new DataDescriptor();
        List<DataDescriptor> keys = this.decode(dds.getDataDescriptors(), lookup);
        keys = this.preflatten(keys);
        final List<DataDescriptor> tree = this.replicate(keys);
        this.flatten(this.root.subKeys = new ArrayList<DataDescriptor>(), tree);
        this.operate(this.root.subKeys);
        this.root.total_nbits = this.root.countBits();
        return this.root;
    }
    
    private List<DataDescriptor> decode(final List<Short> keyDesc, final TableLookup lookup) {
        if (keyDesc == null) {
            return null;
        }
        final List<DataDescriptor> keys = new ArrayList<DataDescriptor>();
        for (final short id : keyDesc) {
            final DataDescriptor dd = new DataDescriptor(id, lookup);
            keys.add(dd);
            if (dd.f == 3) {
                final List<Short> subDesc = lookup.getDescriptorsTableD(dd.fxy);
                if (subDesc == null) {
                    dd.bad = true;
                }
                else {
                    dd.subKeys = this.decode(subDesc, lookup);
                }
            }
        }
        return keys;
    }
    
    private List<DataDescriptor> replicate(final List<DataDescriptor> keys) {
        final List<DataDescriptor> tree = new ArrayList<DataDescriptor>();
        final Iterator<DataDescriptor> dkIter = keys.iterator();
        while (dkIter.hasNext()) {
            final DataDescriptor dk = dkIter.next();
            if (dk.f == 1) {
                dk.subKeys = new ArrayList<DataDescriptor>();
                dk.replication = dk.y;
                if (dk.replication == 0) {
                    this.root.isVarLength = true;
                    final DataDescriptor replication = dkIter.next();
                    if (replication.y == 0) {
                        dk.replicationCountSize = 1;
                    }
                    else if (replication.y == 1) {
                        dk.replicationCountSize = 8;
                    }
                    else if (replication.y == 2) {
                        dk.replicationCountSize = 16;
                    }
                    else if (replication.y == 11) {
                        dk.repetitionCountSize = 8;
                    }
                    else if (replication.y == 12) {
                        dk.repetitionCountSize = 16;
                    }
                    else {
                        DataDescriptorTreeConstructor.log.error("Unknown replication type= " + replication);
                    }
                }
                for (int j = 0; j < dk.x && dkIter.hasNext(); ++j) {
                    dk.subKeys.add(dkIter.next());
                }
                dk.subKeys = this.replicate(dk.subKeys);
            }
            else if (dk.f == 3 && dk.subKeys != null) {
                dk.subKeys = this.replicate(dk.subKeys);
            }
            tree.add(dk);
        }
        return tree;
    }
    
    private List<DataDescriptor> preflatten(final List<DataDescriptor> tree) {
        final List<DataDescriptor> result = new ArrayList<DataDescriptor>(tree.size());
        for (final DataDescriptor key : tree) {
            boolean preflatten = false;
            if (key.f == 3 && key.subKeys != null) {
                final List<DataDescriptor> subkeys = key.subKeys;
                for (int i = 0; i < subkeys.size(); ++i) {
                    final DataDescriptor subkey = subkeys.get(i);
                    if (subkey.f == 1) {
                        final int need = subkey.x;
                        int have = subkeys.size() - i - 1;
                        if (subkey.y == 0) {
                            --have;
                        }
                        if (need > have) {
                            preflatten = true;
                        }
                    }
                }
            }
            if (preflatten) {
                result.addAll(key.subKeys);
            }
            else {
                result.add(key);
            }
        }
        return result;
    }
    
    private void flatten(final List<DataDescriptor> result, final List<DataDescriptor> tree) {
        for (final DataDescriptor key : tree) {
            if (key.bad) {
                this.root.isBad = true;
                result.add(key);
            }
            else if (key.f == 3 && key.subKeys != null) {
                this.flatten(result, key.subKeys);
            }
            else if (key.f == 1) {
                final List<DataDescriptor> subTree = new ArrayList<DataDescriptor>();
                this.flatten(subTree, key.subKeys);
                key.subKeys = subTree;
                result.add(key);
            }
            else {
                result.add(key);
            }
        }
    }
    
    private void operate(final List<DataDescriptor> tree) {
        if (tree == null) {
            return;
        }
        boolean hasAssFields = false;
        DataDescriptor.AssociatedField assField = null;
        final Iterator<DataDescriptor> iter = tree.iterator();
        while (iter.hasNext()) {
            final DataDescriptor dd = iter.next();
            if (dd.f == 2) {
                if (dd.x == 1) {
                    this.changeWidth = ((dd.y == 0) ? null : dd);
                    iter.remove();
                }
                else if (dd.x == 2) {
                    this.changeScale = ((dd.y == 0) ? null : dd);
                    iter.remove();
                }
                else if (dd.x == 3) {
                    this.changeRefval = ((dd.y == 255) ? null : dd);
                    iter.remove();
                }
                else if (dd.x == 4) {
                    assField = ((dd.y == 0) ? null : new DataDescriptor.AssociatedField(dd.y));
                    iter.remove();
                    hasAssFields = true;
                }
                else if (dd.x == 5) {
                    dd.type = 1;
                    dd.bitWidth = dd.y * 8;
                    dd.name = "Note";
                }
                else if (dd.x == 6) {
                    iter.remove();
                    if (dd.y == 0 || !iter.hasNext()) {
                        continue;
                    }
                    final DataDescriptor next = iter.next();
                    next.bitWidth = dd.y;
                }
                else if (dd.x == 36) {
                    if (!iter.hasNext()) {
                        continue;
                    }
                    final DataDescriptor dpi_dd = iter.next();
                    this.dpi = new DataPresentIndicator(tree, dpi_dd);
                    dd.dpi = this.dpi;
                    dpi_dd.dpi = this.dpi;
                }
                else if (dd.x == 37 && dd.y == 255) {
                    this.dpi = null;
                }
                else {
                    if (dd.x != 24 || dd.y != 255) {
                        continue;
                    }
                    dd.dpi = this.dpi;
                }
            }
            else if (dd.subKeys != null) {
                this.operate(dd.subKeys);
            }
            else {
                if (dd.f != 0) {
                    continue;
                }
                if (dd.type != 3) {
                    if (this.changeWidth != null) {
                        final DataDescriptor dataDescriptor = dd;
                        dataDescriptor.bitWidth += this.changeWidth.y - 128;
                    }
                    if (this.changeScale != null) {
                        final DataDescriptor dataDescriptor2 = dd;
                        dataDescriptor2.scale += this.changeScale.y - 128;
                    }
                    if (this.changeRefval != null) {
                        final DataDescriptor dataDescriptor3 = dd;
                        dataDescriptor3.refVal += this.changeRefval.y - 128;
                    }
                }
                if (dd.f != 0 || assField == null) {
                    continue;
                }
                final DataDescriptor.AssociatedField associatedField = assField;
                ++associatedField.nfields;
                dd.assField = assField;
                assField.dataFldName = dd.name;
            }
        }
        if (hasAssFields) {
            this.addAssFields(tree);
        }
    }
    
    private void addAssFields(final List<DataDescriptor> tree) {
        if (tree == null) {
            return;
        }
        for (int index = 0; index < tree.size(); ++index) {
            final DataDescriptor dd = tree.get(index);
            if (dd.assField != null) {
                final DataDescriptor.AssociatedField assField = dd.assField;
                if (dd.f == 0 && dd.x == 31 && dd.y == 21) {
                    dd.name = assField.dataFldName + "_associated_field_significance";
                    dd.assField = null;
                }
                else {
                    final DataDescriptor assDD = dd.makeAssociatedField(assField.nbits);
                    tree.add(index, assDD);
                    ++index;
                }
            }
        }
    }
    
    static {
        DataDescriptorTreeConstructor.log = LoggerFactory.getLogger(DataDescriptorTreeConstructor.class);
    }
    
    static class DataPresentIndicator
    {
        DataDescriptor dataPresent;
        List<DataDescriptor> linear;
        
        DataPresentIndicator(final List<DataDescriptor> tree, final DataDescriptor dpi_dd) {
            this.linear = new ArrayList<DataDescriptor>();
            this.dataPresent = dpi_dd;
            this.linear = new ArrayList<DataDescriptor>();
            this.linearize(tree);
        }
        
        int getNfields() {
            return this.dataPresent.replication;
        }
        
        private void linearize(final List<DataDescriptor> tree) {
            for (final DataDescriptor dd : tree) {
                if (dd.f == 0) {
                    this.linear.add(dd);
                }
                else {
                    if (dd.f != 1) {
                        continue;
                    }
                    for (int i = 0; i < dd.replication; ++i) {
                        this.linearize(dd.getSubKeys());
                    }
                }
            }
        }
    }
}
