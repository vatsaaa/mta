// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.iosp.bufr;

import java.io.IOException;
import ucar.unidata.io.RandomAccessFile;

public final class BufrNumbers
{
    private static final long[] missing_value;
    public static final int UNDEFINED = -9999;
    
    public static boolean isMissing(final long raw, final int bitWidth) {
        return raw == BufrNumbers.missing_value[bitWidth];
    }
    
    static long missingValue(final int bitWidth) {
        return BufrNumbers.missing_value[bitWidth];
    }
    
    public static int int2(final RandomAccessFile raf) throws IOException {
        final int a = raf.read();
        final int b = raf.read();
        return int2(a, b);
    }
    
    public static int int2(final int a, final int b) {
        if (a == 255 && b == 255) {
            return -9999;
        }
        return (1 - ((a & 0x80) >> 6)) * ((a & 0x7F) << 8 | b);
    }
    
    public static int int3(final RandomAccessFile raf) throws IOException {
        final int a = raf.read();
        final int b = raf.read();
        final int c = raf.read();
        return int3(a, b, c);
    }
    
    private static int int3(final int a, final int b, final int c) {
        return (1 - ((a & 0x80) >> 6)) * ((a & 0x7F) << 16 | b << 8 | c);
    }
    
    public static int int4(final RandomAccessFile raf) throws IOException {
        final int a = raf.read();
        final int b = raf.read();
        final int c = raf.read();
        final int d = raf.read();
        return int4(a, b, c, d);
    }
    
    private static int int4(final int a, final int b, final int c, final int d) {
        if (a == 255 && b == 255 && c == 255 && d == 255) {
            return -9999;
        }
        return (1 - ((a & 0x80) >> 6)) * ((a & 0x7F) << 24 | b << 16 | c << 8 | d);
    }
    
    public static int uint2(final RandomAccessFile raf) throws IOException {
        final int a = raf.read();
        final int b = raf.read();
        return uint2(a, b);
    }
    
    private static int uint2(final int a, final int b) {
        return a << 8 | b;
    }
    
    public static int uint3(final RandomAccessFile raf) throws IOException {
        final int a = raf.read();
        final int b = raf.read();
        final int c = raf.read();
        return uint3(a, b, c);
    }
    
    public static int uint3(final int a, final int b, final int c) {
        return a << 16 | b << 8 | c;
    }
    
    public static float float4(final RandomAccessFile raf) throws IOException {
        final int a = raf.read();
        final int b = raf.read();
        final int c = raf.read();
        final int d = raf.read();
        return float4(a, b, c, d);
    }
    
    private static float float4(final int a, final int b, final int c, final int d) {
        final int mant = b << 16 | c << 8 | d;
        if (mant == 0) {
            return 0.0f;
        }
        final int sgn = -(((a & 0x80) >> 6) - 1);
        final int exp = (a & 0x7F) - 64;
        return (float)(sgn * Math.pow(16.0, exp - 6) * mant);
    }
    
    public static long int8(final RandomAccessFile raf) throws IOException {
        final int a = raf.read();
        final int b = raf.read();
        final int c = raf.read();
        final int d = raf.read();
        final int e = raf.read();
        final int f = raf.read();
        final int g = raf.read();
        final int h = raf.read();
        return (1 - ((a & 0x80) >> 6)) * ((a & 0x7F) << 56 | b << 48 | c << 40 | d << 32 | e << 24 | f << 16 | g << 8 | h);
    }
    
    static {
        missing_value = new long[65];
        long accum = 0L;
        for (int i = 0; i < 65; ++i) {
            BufrNumbers.missing_value[i] = accum;
            accum = accum * 2L + 1L;
        }
    }
}
