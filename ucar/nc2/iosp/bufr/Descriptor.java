// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.iosp.bufr;

import ucar.nc2.iosp.bufr.tables.TableD;
import ucar.nc2.iosp.bufr.tables.TableB;
import ucar.nc2.iosp.bufr.tables.TableC;
import java.util.Formatter;

public class Descriptor
{
    private static final String[] descType;
    
    public static String makeString(final short fxy) {
        final int f = (fxy & 0xC000) >> 14;
        final int x = (fxy & 0x3F00) >> 8;
        final int y = fxy & 0xFF;
        final Formatter out = new Formatter();
        out.format("%d-%02d-%03d", f, x, y);
        return out.toString();
    }
    
    public static boolean isWmoRange(final short fxy) {
        final int x = (fxy & 0x3F00) >> 8;
        final int y = fxy & 0xFF;
        return x < 48 && y < 192;
    }
    
    public static short getFxy(final String name) {
        final String[] tok = name.split("-");
        final int f = (tok.length > 0) ? Integer.parseInt(tok[0]) : 0;
        final int x = (tok.length > 1) ? Integer.parseInt(tok[1]) : 0;
        final int y = (tok.length > 2) ? Integer.parseInt(tok[2]) : 0;
        return (short)((f << 14) + (x << 8) + y);
    }
    
    public static short getFxy(final short f, final short x, final short y) {
        return (short)((f << 14) + (x << 8) + y);
    }
    
    public static void show(final Formatter out, final short fxy, final TableLookup lookup) {
        final int f = (fxy & 0xC000) >> 14;
        if (f == 0) {
            final TableB.Descriptor b = lookup.getDescriptorTableB(fxy);
            if (b == null) {
                out.format("%-8s: NOT FOUND!!", makeString(fxy));
            }
            else {
                out.format("%-8s: %s", b.getFxy(), b.getName());
            }
        }
        else if (f == 1) {
            out.format("%-8s: %s", makeString(fxy), Descriptor.descType[1]);
        }
        else if (f == 2) {
            final int x = (fxy & 0x3F00) >> 8;
            out.format("%-8s: Operator= %s", makeString(fxy), TableC.getOperatorName(x));
        }
        else if (f == 3) {
            final TableD.Descriptor d = lookup.getDescriptorTableD(fxy);
            if (d == null) {
                out.format("%-8s: NOT FOUND!!", makeString(fxy));
            }
            else {
                out.format("%-8s: %s", d.getFxy(), d.getName());
            }
        }
    }
    
    public static String getName(final short fxy, final TableLookup lookup) {
        final int f = (fxy & 0xC000) >> 14;
        if (f == 0) {
            final TableB.Descriptor b = lookup.getDescriptorTableB(fxy);
            if (b == null) {
                return "**NOT FOUND!!";
            }
            return b.getName();
        }
        else {
            if (f == 1) {
                return Descriptor.descType[1];
            }
            if (f == 2) {
                final int x = (fxy & 0x3F00) >> 8;
                return TableC.getOperatorName(x);
            }
            if (f != 3) {
                return "illegal F=" + f;
            }
            final TableD.Descriptor d = lookup.getDescriptorTableD(fxy);
            if (d == null) {
                return "**NOT FOUND!!";
            }
            return d.getName();
        }
    }
    
    static {
        descType = new String[] { "tableB", "replication", "tableC-operators", "tableD" };
    }
}
