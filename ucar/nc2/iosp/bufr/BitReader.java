// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.iosp.bufr;

import java.io.IOException;
import ucar.unidata.io.RandomAccessFile;

public class BitReader
{
    private RandomAccessFile raf;
    private long startPos;
    private byte[] test;
    private int testPos;
    private int bitBuf;
    private int bitPos;
    
    BitReader(final byte[] test) {
        this.testPos = 0;
        this.bitBuf = 0;
        this.bitPos = 0;
        this.test = test;
        this.testPos = 0;
        this.bitBuf = 0;
        this.bitPos = 0;
    }
    
    public BitReader(final RandomAccessFile raf, final long startPos) throws IOException {
        this.testPos = 0;
        this.bitBuf = 0;
        this.bitPos = 0;
        (this.raf = raf).seek(this.startPos = startPos);
    }
    
    private void setPos(final int bitPos, final int bitBuf) throws IOException {
        this.bitPos = bitPos;
        this.bitBuf = bitBuf;
    }
    
    public void setBitOffset(final int bitOffset) throws IOException {
        if (bitOffset % 8 == 0) {
            this.raf.seek(this.startPos + bitOffset / 8);
            this.bitPos = 0;
            this.bitBuf = 0;
        }
        else {
            this.raf.seek(this.startPos + bitOffset / 8);
            this.bitPos = 8 - bitOffset % 8;
            this.bitBuf = this.raf.read();
            this.bitBuf &= 255 >> 8 - this.bitPos;
        }
    }
    
    private int nextByte() throws IOException {
        if (this.raf != null) {
            return this.raf.read();
        }
        return this.test[this.testPos++];
    }
    
    public long bits2UInt(final int nb) throws IOException {
        int bitsLeft = nb;
        int result = 0;
        if (this.bitPos == 0) {
            this.bitBuf = this.nextByte();
            this.bitPos = 8;
        }
        int shift;
        while (true) {
            shift = bitsLeft - this.bitPos;
            if (shift <= 0) {
                break;
            }
            result |= this.bitBuf << shift;
            bitsLeft -= this.bitPos;
            this.bitBuf = this.nextByte();
            this.bitPos = 8;
        }
        result |= this.bitBuf >> -shift;
        this.bitPos -= bitsLeft;
        this.bitBuf &= 255 >> 8 - this.bitPos;
        return result;
    }
    
    public long getPos() throws IOException {
        return this.raf.getFilePointer();
    }
    
    public static void main(final String[] args) throws IOException {
        final BitReader bu = new BitReader(new byte[] { -1, 2, 4, 8 });
        bu.bits2UInt(7);
        bu.bits2UInt(1);
    }
}
