// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.iosp.bufr;

import java.util.Iterator;
import java.util.HashMap;
import java.util.Map;

public class BitCounterUncompressed
{
    private final DataDescriptor parent;
    private final int nrows;
    private final int replicationCountSize;
    private Map<DataDescriptor, Integer> bitPosition;
    private Map<DataDescriptor, BitCounterUncompressed[]> subCounters;
    private int[] startBit;
    private int countBits;
    private int bitOffset;
    private static boolean debug;
    
    public BitCounterUncompressed(final DataDescriptor parent, final int nrows, final int replicationCountSize) {
        this.bitOffset = 0;
        this.parent = parent;
        this.nrows = nrows;
        this.replicationCountSize = replicationCountSize;
    }
    
    public void setBitOffset(final DataDescriptor dkey) {
        if (this.bitPosition == null) {
            this.bitPosition = new HashMap<DataDescriptor, Integer>(2 * this.parent.getSubKeys().size());
        }
        this.bitPosition.put(dkey, this.bitOffset);
        this.bitOffset += dkey.getBitWidth();
    }
    
    public int getOffset(final DataDescriptor dkey) {
        return this.bitPosition.get(dkey);
    }
    
    public BitCounterUncompressed makeNested(final DataDescriptor subKey, final int n, final int row, final int replicationCountSize) {
        if (this.subCounters == null) {
            this.subCounters = new HashMap<DataDescriptor, BitCounterUncompressed[]>(5);
        }
        BitCounterUncompressed[] subCounter = this.subCounters.get(subKey);
        if (subCounter == null) {
            subCounter = new BitCounterUncompressed[this.nrows];
            this.subCounters.put(subKey, subCounter);
        }
        final BitCounterUncompressed rc = new BitCounterUncompressed(subKey, n, replicationCountSize);
        return subCounter[row] = rc;
    }
    
    public BitCounterUncompressed[] getNested(final DataDescriptor subKey) {
        return (BitCounterUncompressed[])((this.subCounters == null) ? null : ((BitCounterUncompressed[])this.subCounters.get(subKey)));
    }
    
    int countBits(final int startBit) {
        this.countBits = this.replicationCountSize;
        this.startBit = new int[this.nrows];
        for (int i = 0; i < this.nrows; ++i) {
            this.startBit[i] = startBit + this.countBits;
            if (BitCounterUncompressed.debug) {
                System.out.println(" BitCounterUncompressed row " + i + " startBit=" + this.startBit[i]);
            }
            for (final DataDescriptor nd : this.parent.subKeys) {
                final BitCounterUncompressed[] bitCounter = (BitCounterUncompressed[])((this.subCounters == null) ? null : ((BitCounterUncompressed[])this.subCounters.get(nd)));
                if (bitCounter == null) {
                    this.countBits += nd.getBitWidth();
                }
                else {
                    if (BitCounterUncompressed.debug) {
                        System.out.println(" ---------> nested " + nd.getFxyName() + " starts at =" + (startBit + this.countBits));
                    }
                    this.countBits += bitCounter[i].countBits(startBit + this.countBits);
                    if (!BitCounterUncompressed.debug) {
                        continue;
                    }
                    System.out.println(" <--------- nested " + nd.getFxyName() + " ends at =" + (startBit + this.countBits));
                }
            }
        }
        return this.countBits;
    }
    
    public int getCountBits() {
        return this.countBits;
    }
    
    public int getNumberRows() {
        return this.nrows;
    }
    
    public int getStartBit(final int row) {
        if (row >= this.startBit.length) {
            throw new IllegalStateException();
        }
        return this.startBit[row];
    }
    
    static {
        BitCounterUncompressed.debug = false;
    }
}
