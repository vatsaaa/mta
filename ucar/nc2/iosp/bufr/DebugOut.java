// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.iosp.bufr;

import ucar.nc2.util.Indent;
import java.util.Formatter;

class DebugOut
{
    Formatter f;
    Indent indent;
    int fldno;
    
    DebugOut(final Formatter f) {
        this.f = f;
        (this.indent = new Indent(2)).setIndentLevel(0);
        this.fldno = 1;
    }
    
    String indent() {
        return this.indent.toString();
    }
}
