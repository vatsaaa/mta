// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.iosp.bufr;

import org.slf4j.LoggerFactory;
import java.nio.channels.WritableByteChannel;
import java.util.TimeZone;
import java.io.IOException;
import java.util.GregorianCalendar;
import ucar.unidata.io.RandomAccessFile;
import ucar.unidata.io.KMPMatch;
import org.slf4j.Logger;

public class MessageScanner
{
    public static final int MAX_MESSAGE_SIZE = 500000;
    private static Logger log;
    private static final KMPMatch matcher;
    private RandomAccessFile raf;
    private GregorianCalendar cal;
    private int countMsgs;
    private int countObs;
    private byte[] header;
    private long startPos;
    private long lastPos;
    
    public static boolean isValidFile(final RandomAccessFile raf) throws IOException {
        raf.seek(0L);
        if (!raf.searchForward(MessageScanner.matcher, 8000)) {
            return false;
        }
        raf.skipBytes(4);
        final BufrIndicatorSection is = new BufrIndicatorSection(raf);
        return is.getBufrEdition() <= 4 && is.getBufrLength() <= raf.length();
    }
    
    public MessageScanner(final RandomAccessFile raf) throws IOException {
        this(raf, 0L);
    }
    
    public MessageScanner(final RandomAccessFile raf, long startPos) throws IOException {
        this.raf = null;
        this.cal = new GregorianCalendar(TimeZone.getTimeZone("GMT"));
        this.countMsgs = 0;
        this.countObs = 0;
        this.startPos = 0L;
        this.lastPos = 0L;
        startPos = ((startPos < 30L) ? 0L : (startPos - 30L));
        (this.raf = raf).seek(startPos);
        raf.order(0);
        this.lastPos = startPos;
    }
    
    public boolean hasNext() throws IOException {
        if (this.lastPos >= this.raf.length()) {
            return false;
        }
        this.raf.seek(this.lastPos);
        final boolean more = this.raf.searchForward(MessageScanner.matcher, -1);
        if (more) {
            final long stop = this.raf.getFilePointer();
            int sizeHeader = (int)(stop - this.lastPos);
            if (sizeHeader > 30) {
                sizeHeader = 30;
            }
            this.header = new byte[sizeHeader];
            this.startPos = stop - sizeHeader;
            this.raf.seek(this.startPos);
            this.raf.read(this.header);
        }
        return more;
    }
    
    public Message next() throws IOException {
        final long start = this.raf.getFilePointer();
        this.raf.seek(start + 4L);
        final BufrIndicatorSection is = new BufrIndicatorSection(this.raf);
        final BufrIdentificationSection ids = new BufrIdentificationSection(this.raf, is);
        final BufrDataDescriptionSection dds = new BufrDataDescriptionSection(this.raf);
        final long dataPos = this.raf.getFilePointer();
        final int dataLength = BufrNumbers.uint3(this.raf);
        final BufrDataSection dataSection = new BufrDataSection(dataPos, dataLength);
        this.lastPos = dataPos + dataLength + 4L;
        if (is.getBufrEdition() > 4) {
            MessageScanner.log.warn("Illegal edition - BUFR message at pos " + start + " header= " + cleanup(this.header));
            return null;
        }
        if (is.getBufrEdition() < 2) {
            MessageScanner.log.warn("Edition " + is.getBufrEdition() + " is not supported - BUFR message at pos " + start + " header= " + cleanup(this.header));
            return null;
        }
        final long ending = dataPos + dataLength;
        this.raf.seek(dataPos + dataLength);
        for (int i = 0; i < 3; ++i) {
            if (this.raf.read() != 55) {
                MessageScanner.log.warn("Missing End of BUFR message at pos=" + ending + " header= " + cleanup(this.header));
                return null;
            }
        }
        if (this.raf.read() != 55) {
            this.raf.seek(dataPos + dataLength - 1L);
            if (this.raf.read() != 55) {
                MessageScanner.log.warn("Missing End of BUFR message at pos=" + ending + " header= " + cleanup(this.header) + " edition= " + is.getBufrEdition());
                return null;
            }
            MessageScanner.log.warn("End of BUFR message off-by-one at pos= " + ending + " header= " + cleanup(this.header) + " edition= " + is.getBufrEdition());
            --this.lastPos;
        }
        final Message m = new Message(this.raf, is, ids, dds, dataSection, this.cal);
        m.setHeader(cleanup(this.header));
        m.setStartPos(start);
        ++this.countMsgs;
        this.countObs += dds.getNumberDatasets();
        this.raf.seek(start + is.getBufrLength());
        return m;
    }
    
    public byte[] getMessageBytesFromLast(final Message m) throws IOException {
        final long startPos = m.getStartPos();
        final int length = (int)(this.lastPos - startPos);
        final byte[] result = new byte[length];
        this.raf.seek(startPos);
        this.raf.readFully(result);
        return result;
    }
    
    public byte[] getMessageBytes(final Message m) throws IOException {
        final long startPos = m.getStartPos();
        final int length = m.is.getBufrLength();
        final byte[] result = new byte[length];
        this.raf.seek(startPos);
        this.raf.readFully(result);
        return result;
    }
    
    public int getTotalObs() {
        return this.countObs;
    }
    
    public int getTotalMessages() {
        return this.countMsgs;
    }
    
    private static String cleanup(final byte[] h) {
        final byte[] bb = new byte[h.length];
        int count = 0;
        for (final byte b : h) {
            if (b >= 32 && b < 127) {
                bb[count++] = b;
            }
        }
        return new String(bb, 0, count);
    }
    
    public long writeCurrentMessage(final WritableByteChannel out) throws IOException {
        final long nbytes = this.lastPos - this.startPos;
        return this.raf.readToByteChannel(out, this.startPos, nbytes);
    }
    
    static {
        MessageScanner.log = LoggerFactory.getLogger(MessageScanner.class);
        matcher = new KMPMatch("BUFR".getBytes());
    }
}
