// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.iosp.bufr;

import java.util.Formatter;

public class BitCounterCompressed
{
    private final DataDescriptor dkey;
    private final int nrows;
    private final int bitOffset;
    private int dataWidth;
    private BitCounterCompressed[][] nested;
    
    public BitCounterCompressed(final DataDescriptor dkey, final int n, final int bitOffset) {
        this.dkey = dkey;
        this.nrows = n;
        this.bitOffset = bitOffset;
    }
    
    void setDataWidth(final int dataWidth) {
        this.dataWidth = dataWidth;
    }
    
    public int getStartingBitPos() {
        return this.bitOffset;
    }
    
    public int getBitPos(final int msgOffset) {
        return this.bitOffset + this.dkey.bitWidth + 6 + this.dataWidth * msgOffset;
    }
    
    public int getTotalBits() {
        if (this.nested == null) {
            return this.dkey.bitWidth + 6 + this.dataWidth * this.nrows;
        }
        int totalBits = 0;
        for (final BitCounterCompressed[] counters : this.nested) {
            if (counters != null) {
                for (final BitCounterCompressed counter : counters) {
                    if (counter != null) {
                        totalBits += counter.getTotalBits();
                    }
                }
            }
        }
        if (this.dkey.replicationCountSize > 0) {
            totalBits += this.dkey.replicationCountSize + 6;
        }
        return totalBits;
    }
    
    public BitCounterCompressed[] getNestedCounters(final int innerIndex) {
        return this.nested[innerIndex];
    }
    
    public void addNestedCounters(final int innerDimensionSize) {
        this.nested = new BitCounterCompressed[innerDimensionSize][this.dkey.getSubKeys().size()];
    }
    
    public int ncounters() {
        if (this.nested == null) {
            return 1;
        }
        int ncounters = 0;
        for (final BitCounterCompressed[] counters : this.nested) {
            if (counters != null) {
                for (final BitCounterCompressed counter : counters) {
                    if (counter != null) {
                        ncounters += counter.ncounters();
                    }
                }
            }
        }
        return ncounters;
    }
    
    public void show(final Formatter out, final int indent) {
        for (int i = 0; i < indent; ++i) {
            out.format(" ", new Object[0]);
        }
        out.format("%8d %8d %4d %s %n", this.getTotalBits(), this.bitOffset, this.dataWidth, this.dkey.name);
        if (this.nested != null) {
            for (final BitCounterCompressed[] counters : this.nested) {
                if (counters != null) {
                    for (final BitCounterCompressed counter : counters) {
                        if (counter != null) {
                            counter.show(out, indent + 2);
                        }
                    }
                }
            }
        }
    }
}
