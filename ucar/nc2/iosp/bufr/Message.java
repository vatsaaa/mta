// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.iosp.bufr;

import ucar.ma2.ArrayStructureMA;
import ucar.ma2.Range;
import ucar.ma2.ArrayStructureBB;
import ucar.nc2.iosp.bufr.tables.TableB;
import java.util.List;
import java.util.Formatter;
import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.Date;
import ucar.nc2.iosp.bufr.tables.CommonCodeTables;
import java.io.IOException;
import java.util.GregorianCalendar;
import ucar.unidata.io.RandomAccessFile;
import java.util.regex.Pattern;

public class Message
{
    private static final Pattern wmoPattern;
    public BufrIndicatorSection is;
    public BufrIdentificationSection ids;
    public BufrDataDescriptionSection dds;
    public BufrDataSection dataSection;
    private RandomAccessFile raf;
    private TableLookup lookup;
    private DataDescriptor root;
    private GregorianCalendar cal;
    private String header;
    private long startPos;
    private byte[] raw;
    BitCounterUncompressed[] counterDatasets;
    BitCounterCompressed[] counterFlds;
    int msg_nbits;
    
    public Message(final RandomAccessFile raf, final BufrIndicatorSection is, final BufrIdentificationSection ids, final BufrDataDescriptionSection dds, final BufrDataSection dataSection, final GregorianCalendar cal) throws IOException {
        this.raf = raf;
        this.is = is;
        this.ids = ids;
        this.dds = dds;
        this.dataSection = dataSection;
        this.cal = cal;
        this.lookup = new TableLookup(ids);
    }
    
    public void close() throws IOException {
        if (this.raf != null) {
            this.raf.close();
        }
    }
    
    public int getNumberDatasets() {
        return this.dds.getNumberDatasets();
    }
    
    public String getCategoryName() {
        return CommonCodeTables.getDataCategory(this.ids.getCategory());
    }
    
    public String getCategoryNo() {
        String result = this.ids.getCategory() + "." + this.ids.getSubCategory();
        if (this.ids.getLocalSubCategory() >= 0) {
            result = result + "." + this.ids.getLocalSubCategory();
        }
        return result;
    }
    
    public String getCenterName() {
        String name = CommonCodeTables.getCenterName(this.ids.getCenterId());
        final String subname = CommonCodeTables.getSubCenterName(this.ids.getCenterId(), this.ids.getSubCenterId());
        if (subname != null) {
            name = name + " / " + subname;
        }
        return this.ids.getCenterId() + "." + this.ids.getSubCenterId() + " (" + name + ")";
    }
    
    public String getCenterNo() {
        return this.ids.getCenterId() + "." + this.ids.getSubCenterId();
    }
    
    public String getTableName() {
        return this.ids.getMasterTableId() + "." + this.ids.getMasterTableVersion() + "." + this.ids.getLocalTableVersion();
    }
    
    public final Date getReferenceTime() {
        return this.ids.getReferenceTime(this.cal);
    }
    
    public void setHeader(final String header) {
        this.header = header;
    }
    
    public String getHeader() {
        return this.header;
    }
    
    public void setStartPos(final long startPos) {
        this.startPos = startPos;
    }
    
    public long getStartPos() {
        return this.startPos;
    }
    
    public void setRawBytes(final byte[] raw) {
        this.raw = raw;
    }
    
    public byte[] getRawBytes() {
        return this.raw;
    }
    
    public String extractWMO() {
        final Matcher matcher = Message.wmoPattern.matcher(this.header);
        if (!matcher.matches()) {
            return "";
        }
        return matcher.group(1);
    }
    
    public long getMessageSize() {
        return this.is.getBufrLength();
    }
    
    public DataDescriptor getRootDataDescriptor() throws IOException {
        if (this.root == null) {
            this.root = new DataDescriptorTreeConstructor().factory(this.lookup, this.dds);
        }
        return this.root;
    }
    
    public boolean usesLocalTable() throws IOException {
        final DataDescriptor root = this.getRootDataDescriptor();
        return this.usesLocalTable(root);
    }
    
    private boolean usesLocalTable(final DataDescriptor dds) throws IOException {
        for (final DataDescriptor key : dds.getSubKeys()) {
            if (key.isLocal()) {
                return true;
            }
            if (key.getSubKeys() != null && this.usesLocalTable(key)) {
                return true;
            }
        }
        return false;
    }
    
    public boolean isTablesComplete() throws IOException {
        final DataDescriptor root = this.getRootDataDescriptor();
        return !root.isBad;
    }
    
    public void showMissingFields(final Formatter out) throws IOException {
        this.showMissingFields(this.dds.getDataDescriptors(), out);
    }
    
    private void showMissingFields(final List<Short> ddsList, final Formatter out) throws IOException {
        for (final short fxy : ddsList) {
            final int f = (fxy & 0xC000) >> 14;
            if (f == 3) {
                final List<Short> sublist = this.lookup.getDescriptorsTableD(fxy);
                if (sublist == null) {
                    out.format("%s, ", Descriptor.makeString(fxy));
                }
                else {
                    this.showMissingFields(sublist, out);
                }
            }
            else {
                if (f != 0) {
                    continue;
                }
                final TableB.Descriptor b = this.lookup.getDescriptorTableB(fxy);
                if (b != null) {
                    continue;
                }
                out.format("%s, ", Descriptor.makeString(fxy));
            }
        }
    }
    
    public TableLookup getTableLookup() {
        return this.lookup;
    }
    
    public boolean isBitCountOk() throws IOException {
        this.getRootDataDescriptor();
        this.getTotalBits();
        final int nbytesCounted = this.getCountedDataBytes();
        final int nbytesGiven = this.dataSection.getDataLength();
        return Math.abs(nbytesCounted - nbytesGiven) <= 1;
    }
    
    public int getCountedDataBytes() {
        int msg_nbytes = this.msg_nbits / 8;
        if (this.msg_nbits % 8 != 0) {
            ++msg_nbytes;
        }
        msg_nbytes += 4;
        if (msg_nbytes % 2 != 0) {
            ++msg_nbytes;
        }
        return msg_nbytes;
    }
    
    public int getCountedDataBits() {
        return this.msg_nbits;
    }
    
    public BitCounterUncompressed getBitCounterUncompressed(final int obsOffsetInMessage) {
        if (this.dds.isCompressed()) {
            throw new IllegalArgumentException("cant call BufrMessage.getBitOffset() on compressed message");
        }
        this.calcTotalBits(null);
        return this.counterDatasets[obsOffsetInMessage];
    }
    
    public int getTotalBits() {
        if (this.msg_nbits == 0) {
            this.calcTotalBits(null);
        }
        return this.msg_nbits;
    }
    
    public int calcTotalBits(final Formatter out) {
        try {
            if (!this.dds.isCompressed()) {
                final MessageUncompressedDataReader reader = new MessageUncompressedDataReader();
                reader.readData(null, this, this.raf, null, false, out);
            }
            else {
                final MessageCompressedDataReader reader2 = new MessageCompressedDataReader();
                reader2.readData(null, this, this.raf, null, out);
            }
        }
        catch (IOException ioe) {
            return 0;
        }
        return this.msg_nbits;
    }
    
    @Override
    public int hashCode() {
        int result = 17;
        result += 37 * result + this.dds.getDataDescriptors().hashCode();
        result += 37 * result + this.ids.getCenterId();
        result += 37 * result + this.ids.getCategory();
        result += 37 * result + this.ids.getSubCategory();
        return result;
    }
    
    @Override
    public boolean equals(final Object obj) {
        if (!(obj instanceof Message)) {
            return false;
        }
        final Message o = (Message)obj;
        return this.dds.getDataDescriptors().equals(o.dds.getDataDescriptors()) && this.ids.getCenterId() == o.ids.getCenterId() && this.ids.getCategory() == o.ids.getCategory() && this.ids.getSubCategory() == o.ids.getSubCategory();
    }
    
    public void dump(final Formatter out) throws IOException {
        final int listHash = this.dds.getDataDescriptors().hashCode();
        out.format(" BUFR edition %d time= %s wmoHeader=%s hash=[0x%x] listHash=[0x%x] (%d) %n", this.is.getBufrEdition(), this.getReferenceTime(), this.getHeader(), this.hashCode(), listHash, listHash);
        out.format("   Category= %s %n", this.getCategoryFullName());
        out.format("   Center= %s %n", this.getCenterName());
        out.format("   Table= %s %n", this.getTableName());
        out.format("    Table B= wmoTable= %s localTable= %s mode=%s%n", this.lookup.getWmoTableBName(), this.lookup.getLocalTableBName(), this.lookup.getMode());
        out.format("    Table D= wmoTable= %s localTable= %s%n", this.lookup.getWmoTableDName(), this.lookup.getLocalTableDName());
        out.format("  DDS nsubsets=%d type=0x%x isObs=%b isCompressed=%b\n", this.dds.getNumberDatasets(), this.dds.getDataType(), this.dds.isObserved(), this.dds.isCompressed());
        final long startPos = this.is.getStartPos();
        final long startData = this.dataSection.getDataPos();
        out.format("  startPos=%d len=%d endPos=%d dataStart=%d dataLen=%d dataEnd=%d %n", startPos, this.is.getBufrLength(), startPos + this.is.getBufrLength(), startData, this.dataSection.getDataLength(), startData + this.dataSection.getDataLength());
        this.dumpDesc(out, this.dds.getDataDescriptors(), this.lookup, 4);
        out.format("%n  CDM Nested Table=\n", new Object[0]);
        final DataDescriptor root = new DataDescriptorTreeConstructor().factory(this.lookup, this.dds);
        this.dumpKeys(out, root, 4);
    }
    
    private void dumpDesc(final Formatter out, final List<Short> desc, final TableLookup table, final int indent) {
        if (desc == null) {
            return;
        }
        for (final Short fxy : desc) {
            for (int i = 0; i < indent; ++i) {
                out.format(" ", new Object[0]);
            }
            Descriptor.show(out, fxy, table);
            out.format("%n", new Object[0]);
            final int f = (fxy & 0xC000) >> 14;
            if (f == 3) {
                final List<Short> sublist = table.getDescriptorsTableD(fxy);
                this.dumpDesc(out, sublist, table, indent + 2);
            }
        }
    }
    
    private void dumpKeys(final Formatter out, final DataDescriptor tree, final int indent) {
        for (final DataDescriptor key : tree.subKeys) {
            for (int i = 0; i < indent; ++i) {
                out.format(" ", new Object[0]);
            }
            out.format("%s\n", key);
            if (key.getSubKeys() != null) {
                this.dumpKeys(out, key, indent + 2);
            }
        }
    }
    
    public String getCategoryFullName() throws IOException {
        final String catName = this.getCategoryName();
        final String subcatName = CommonCodeTables.getDataSubcategoy(this.ids.getCategory(), this.ids.getSubCategory());
        if (subcatName != null) {
            return this.getCategoryNo() + "=" + catName + " / " + subcatName;
        }
        return this.getCategoryNo() + "=" + catName;
    }
    
    public void dumpHeader(final Formatter out) {
        out.format(" BUFR edition %d time= %s wmoHeader=%s %n", this.is.getBufrEdition(), this.getReferenceTime(), this.getHeader());
        out.format("   Category= %d %s %s %n", this.ids.getCategory(), this.getCategoryName(), this.getCategoryNo());
        out.format("   Center= %s %s %n", this.getCenterName(), this.getCenterNo());
        out.format("   Table= %d.%d local= %d wmoTables= %s,%s localTables= %s,%s %n", this.ids.getMasterTableId(), this.ids.getMasterTableVersion(), this.ids.getLocalTableVersion(), this.lookup.getWmoTableBName(), this.lookup.getWmoTableDName(), this.lookup.getLocalTableBName(), this.lookup.getLocalTableDName());
        out.format("  DDS nsubsets=%d type=0x%x isObs=%b isCompressed=%b\n", this.dds.getNumberDatasets(), this.dds.getDataType(), this.dds.isObserved(), this.dds.isCompressed());
    }
    
    public void dumpHeaderShort(final Formatter out) {
        out.format(" %s, Cat= %s, Center= %s (%s), Table= %d.%d.%d %n", this.getHeader(), this.getCategoryName(), this.getCenterName(), this.getCenterNo(), this.ids.getMasterTableId(), this.ids.getMasterTableVersion(), this.ids.getLocalTableVersion());
    }
    
    static {
        wmoPattern = Pattern.compile(".*([IJ]..... ....) .*");
    }
}
