// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.iosp.bufr;

import org.slf4j.LoggerFactory;
import ucar.ma2.StructureData;
import java.util.Date;
import ucar.ma2.ArrayStructure;
import java.util.GregorianCalendar;
import java.util.TimeZone;
import ucar.nc2.units.DateFormatter;
import ucar.nc2.constants.AxisType;
import ucar.nc2.EnumTypedef;
import ucar.nc2.iosp.bufr.tables.CodeFlagTables;
import ucar.ma2.InvalidRangeException;
import java.util.List;
import java.util.Iterator;
import ucar.ma2.DataType;
import ucar.nc2.Variable;
import ucar.nc2.Structure;
import java.io.IOException;
import ucar.nc2.Group;
import ucar.nc2.Attribute;
import java.util.Calendar;
import ucar.nc2.units.DateUnit;
import ucar.nc2.Sequence;
import ucar.nc2.NetcdfFile;
import org.slf4j.Logger;

class ConstructNC
{
    private static Logger log;
    static final String TIME_NAME = "time";
    private static final boolean warnUnits = false;
    private NetcdfFile ncfile;
    Sequence recordStructure;
    private Message proto;
    private int structNum;
    private int seqNum;
    boolean hasTime;
    private String yearName;
    private String monthName;
    private String dayName;
    private String hourName;
    private String minName;
    private String secName;
    private String doyName;
    private DateUnit dateUnit;
    private Calendar cal;
    
    ConstructNC(final Message proto, final int nobs, final NetcdfFile nc) throws IOException {
        this.structNum = 1;
        this.seqNum = 1;
        this.proto = proto;
        this.ncfile = nc;
        final int cat = proto.ids.getCategory();
        final int subcat = proto.ids.getSubCategory();
        this.ncfile.addAttribute(null, new Attribute("history", "Direct read of BUFR data by CDM version 4.1"));
        this.ncfile.addAttribute(null, new Attribute("location", nc.getLocation()));
        this.ncfile.addAttribute(null, new Attribute("BUFR:edition", proto.is.getBufrEdition()));
        this.ncfile.addAttribute(null, new Attribute("BUFR:categoryName", proto.getCategoryName()));
        this.ncfile.addAttribute(null, new Attribute("BUFR:category", cat));
        this.ncfile.addAttribute(null, new Attribute("BUFR:subCategory", subcat));
        this.ncfile.addAttribute(null, new Attribute("BUFR:localSubCategory", proto.ids.getLocalSubCategory()));
        this.ncfile.addAttribute(null, new Attribute("BUFR:centerName", proto.getCenterName()));
        this.ncfile.addAttribute(null, new Attribute("BUFR:center", proto.ids.getCenterId()));
        this.ncfile.addAttribute(null, new Attribute("BUFR:subCenter", proto.ids.getSubCenterId()));
        this.ncfile.addAttribute(null, new Attribute("BUFR:table", proto.ids.getMasterTableId()));
        this.ncfile.addAttribute(null, new Attribute("BUFR:tableVersion", proto.ids.getMasterTableVersion()));
        this.ncfile.addAttribute(null, new Attribute("BUFR:localTableVersion", proto.ids.getLocalTableVersion()));
        final String header = proto.getHeader();
        if (header != null) {
            this.ncfile.addAttribute(null, new Attribute("WMO Header", header));
        }
        this.ncfile.addAttribute(null, new Attribute("Conventions", "BUFR/CDM"));
        this.makeObsRecord();
        this.ncfile.finish();
    }
    
    private Structure makeReportIndexStructure() throws IOException {
        final Structure reportIndex = new Structure(this.ncfile, null, null, "obsRecordIndex");
        this.ncfile.addVariable(null, reportIndex);
        reportIndex.setDimensions("record");
        reportIndex.addAttribute(new Attribute("long_name", "index on report"));
        Variable v = reportIndex.addMemberVariable(new Variable(this.ncfile, null, reportIndex, "name", DataType.STRING, ""));
        v.addAttribute(new Attribute("long_name", "name of station"));
        v.addAttribute(new Attribute("standard_name", "station_name"));
        v = reportIndex.addMemberVariable(new Variable(this.ncfile, null, reportIndex, "time", DataType.LONG, ""));
        v.addAttribute(new Attribute("units", "msecs since 1970-01-01 00:00"));
        v.addAttribute(new Attribute("long_name", "observation time"));
        v.addAttribute(new Attribute("_CoordinateAxisType", "Time"));
        return reportIndex;
    }
    
    private void makeObsRecord() throws IOException {
        this.recordStructure = new Sequence(this.ncfile, null, null, "obs");
        this.ncfile.addVariable(null, this.recordStructure);
        final DataDescriptor root = this.proto.getRootDataDescriptor();
        if (this.hasTime()) {
            final Variable timev = this.recordStructure.addMemberVariable(new Variable(this.ncfile, null, this.recordStructure, "time", DataType.INT, ""));
            timev.addAttribute(new Attribute("units", this.dateUnit.getUnitsString()));
            timev.addAttribute(new Attribute("long_name", "time of observation"));
            timev.addAttribute(new Attribute("_CoordinateAxisType", "Time"));
        }
        for (final DataDescriptor dkey : root.subKeys) {
            if (!dkey.isOkForVariable()) {
                continue;
            }
            if (dkey.replication == 0) {
                this.addSequence(this.recordStructure, dkey);
            }
            else if (dkey.replication > 1) {
                final List<DataDescriptor> subKeys = dkey.subKeys;
                if (subKeys.size() == 1) {
                    final DataDescriptor sub = dkey.subKeys.get(0);
                    if (sub.dpi != null) {
                        this.addDpiStructure(this.recordStructure, dkey, sub);
                    }
                    else {
                        final Variable v = this.addVariable(this.recordStructure, sub, dkey.replication);
                        v.setSPobject(dkey);
                    }
                }
                else {
                    if (subKeys.size() <= 1) {
                        continue;
                    }
                    this.addStructure(this.recordStructure, dkey, dkey.replication);
                }
            }
            else {
                this.addVariable(this.recordStructure, dkey, dkey.replication);
            }
        }
    }
    
    private void addStructure(final Structure parent, final DataDescriptor dataDesc, final int count) {
        final String structName = "struct" + this.structNum;
        ++this.structNum;
        final Structure struct = new Structure(this.ncfile, null, parent, structName);
        try {
            struct.setDimensionsAnonymous(new int[] { count });
        }
        catch (InvalidRangeException e) {
            ConstructNC.log.error("illegal count= " + count + " for " + dataDesc);
        }
        for (final DataDescriptor subKey : dataDesc.getSubKeys()) {
            this.addMember(struct, subKey);
        }
        parent.addMemberVariable(struct);
        struct.setSPobject(dataDesc);
        dataDesc.name = structName;
        dataDesc.refersTo = struct;
    }
    
    private void addSequence(final Structure parent, final DataDescriptor dataDesc) {
        final String seqName = "seq" + this.seqNum;
        ++this.seqNum;
        final Sequence seq = new Sequence(this.ncfile, null, parent, seqName);
        seq.setDimensions("");
        for (final DataDescriptor dkey : dataDesc.getSubKeys()) {
            this.addMember(seq, dkey);
        }
        parent.addMemberVariable(seq);
        seq.setSPobject(dataDesc);
        dataDesc.name = seqName;
        dataDesc.refersTo = seq;
    }
    
    private void addMember(final Structure parent, final DataDescriptor dkey) {
        if (dkey.replication == 0) {
            this.addSequence(parent, dkey);
        }
        else if (dkey.replication > 1) {
            final List<DataDescriptor> subKeys = dkey.subKeys;
            if (subKeys.size() == 1) {
                final DataDescriptor sub = dkey.subKeys.get(0);
                final Variable v = this.addVariable(parent, sub, dkey.replication);
                v.setSPobject(dkey);
            }
            else {
                this.addStructure(parent, dkey, dkey.replication);
            }
        }
        else {
            this.addVariable(parent, dkey, dkey.replication);
        }
    }
    
    private void addDpiStructure(final Structure parent, final DataDescriptor parentDD, final DataDescriptor dpiField) {
        final String structName = this.findUnique(parent, dpiField.name);
        final Structure struct = new Structure(this.ncfile, null, parent, structName);
        final int n = parentDD.replication;
        try {
            struct.setDimensionsAnonymous(new int[] { n });
        }
        catch (InvalidRangeException e) {
            ConstructNC.log.error("illegal count= 1 for " + dpiField);
        }
        Variable v = new Variable(this.ncfile, null, struct, "name");
        v.setDataType(DataType.STRING);
        v.setDimensions("");
        struct.addMemberVariable(v);
        v = new Variable(this.ncfile, null, struct, "data");
        v.setDataType(DataType.FLOAT);
        v.setDimensions("");
        struct.addMemberVariable(v);
        parent.addMemberVariable(struct);
        struct.setSPobject(dpiField);
        dpiField.name = structName;
        dpiField.refersTo = struct;
    }
    
    private void addDpiSequence(final Structure parent, final DataDescriptor dataDesc) {
        final Structure struct = new Structure(this.ncfile, null, parent, "statistics");
        try {
            struct.setDimensionsAnonymous(new int[] { dataDesc.replication });
        }
        catch (InvalidRangeException e) {
            e.printStackTrace();
        }
        Variable v = new Variable(this.ncfile, null, struct, "name");
        v.setDataType(DataType.STRING);
        v.setDimensions("");
        struct.addMemberVariable(v);
        v = new Variable(this.ncfile, null, struct, "data");
        v.setDataType(DataType.FLOAT);
        v.setDimensions("");
        struct.addMemberVariable(v);
        parent.addMemberVariable(struct);
    }
    
    private Variable addVariable(final Structure struct, final DataDescriptor dataDesc, final int count) {
        final String name = this.findUnique(struct, dataDesc.name);
        dataDesc.name = name;
        final Variable v = new Variable(this.ncfile, null, struct, name);
        try {
            if (count > 1) {
                v.setDimensionsAnonymous(new int[] { count });
            }
            else {
                v.setDimensions("");
            }
        }
        catch (InvalidRangeException e2) {
            ConstructNC.log.error("illegal count= " + count + " for " + dataDesc);
        }
        if (dataDesc.units != null) {
            if (dataDesc == null || dataDesc.units == null) {
                System.out.println("HEY");
            }
            if (dataDesc.units.equalsIgnoreCase("Code_Table") || dataDesc.units.equalsIgnoreCase("Code Table")) {
                v.addAttribute(new Attribute("units", "CodeTable " + dataDesc.getFxyName()));
            }
            else if (dataDesc.units.equalsIgnoreCase("Flag_Table") || dataDesc.units.equalsIgnoreCase("Flag Table")) {
                v.addAttribute(new Attribute("units", "FlagTable " + dataDesc.getFxyName()));
            }
            else if (!dataDesc.units.startsWith("CCITT") && !dataDesc.units.startsWith("Numeric")) {
                v.addAttribute(new Attribute("units", dataDesc.units));
            }
        }
        if (dataDesc.type == 1) {
            v.setDataType(DataType.CHAR);
            final int size = dataDesc.bitWidth / 8;
            try {
                v.setDimensionsAnonymous(new int[] { size });
            }
            catch (InvalidRangeException e) {
                e.printStackTrace();
            }
        }
        else if (dataDesc.type == 2 && CodeFlagTables.hasTable(dataDesc.fxy)) {
            final int nbits = dataDesc.bitWidth;
            final int nbytes = (nbits % 8 == 0) ? (nbits / 8) : (nbits / 8 + 1);
            final CodeFlagTables ct = CodeFlagTables.getTable(dataDesc.fxy);
            if (nbytes == 1) {
                v.setDataType(DataType.ENUM1);
            }
            else if (nbytes == 2) {
                v.setDataType(DataType.ENUM2);
            }
            else if (nbytes == 4) {
                v.setDataType(DataType.ENUM4);
            }
            v.addAttribute(new Attribute("BUFR:CodeTable", ct.getName() + " (" + dataDesc.getFxyName() + ")"));
            final Group g = struct.getParentGroup();
            EnumTypedef enumTypedef = g.findEnumeration(ct.getName());
            if (enumTypedef == null) {
                enumTypedef = new EnumTypedef(ct.getName(), ct.getMap());
                g.addEnumeration(enumTypedef);
            }
            v.setEnumTypedef(enumTypedef);
        }
        else {
            final int nbits = dataDesc.bitWidth;
            if (nbits < 9) {
                v.setDataType(DataType.BYTE);
                if (nbits == 8) {
                    v.addAttribute(new Attribute("_Unsigned", "true"));
                    v.addAttribute(new Attribute("missing_value", (short)BufrNumbers.missingValue(nbits)));
                }
                else {
                    v.addAttribute(new Attribute("missing_value", (byte)BufrNumbers.missingValue(nbits)));
                }
            }
            else if (nbits < 17) {
                v.setDataType(DataType.SHORT);
                if (nbits == 16) {
                    v.addAttribute(new Attribute("_Unsigned", "true"));
                    v.addAttribute(new Attribute("missing_value", BufrNumbers.missingValue(nbits)));
                }
                else {
                    v.addAttribute(new Attribute("missing_value", (short)BufrNumbers.missingValue(nbits)));
                }
            }
            else if (nbits < 33) {
                v.setDataType(DataType.INT);
                if (nbits == 32) {
                    v.addAttribute(new Attribute("_Unsigned", "true"));
                    v.addAttribute(new Attribute("missing_value", (int)BufrNumbers.missingValue(nbits)));
                }
                else {
                    v.addAttribute(new Attribute("missing_value", BufrNumbers.missingValue(nbits)));
                }
            }
            else {
                v.setDataType(DataType.LONG);
                v.addAttribute(new Attribute("missing_value", BufrNumbers.missingValue(nbits)));
            }
            final int scale10 = dataDesc.scale;
            final double scale11 = (scale10 == 0) ? 1.0 : Math.pow(10.0, -scale10);
            if (scale10 != 0) {
                v.addAttribute(new Attribute("scale_factor", (float)scale11));
            }
            if (dataDesc.refVal != 0) {
                v.addAttribute(new Attribute("add_offset", (float)scale11 * dataDesc.refVal));
            }
        }
        this.annotate(v, dataDesc);
        v.addAttribute(new Attribute("BUFR:TableB_descriptor", dataDesc.getFxyName()));
        v.addAttribute(new Attribute("BUFR:bitWidth", dataDesc.bitWidth));
        struct.addMemberVariable(v);
        v.setSPobject(dataDesc);
        return v;
    }
    
    private String findUnique(final Structure struct, final String want) {
        Variable oldV = struct.findVariable(want);
        if (oldV == null) {
            return want;
        }
        int seq = 1;
        String wantSeq;
        while (true) {
            wantSeq = want + "-" + seq;
            oldV = struct.findVariable(wantSeq);
            if (oldV == null) {
                break;
            }
            ++seq;
        }
        return wantSeq;
    }
    
    private void annotate(final Variable v, final DataDescriptor dkey) {
        final String id = dkey.getFxyName();
        if (id.equals("0-5-1") || id.equals("0-5-2")) {
            v.addAttribute(new Attribute("units", "degrees_north"));
            v.addAttribute(new Attribute("_CoordinateAxisType", AxisType.Lat.toString()));
        }
        if (id.equals("0-6-1") || id.equals("0-6-2")) {
            v.addAttribute(new Attribute("units", "degrees_east"));
            v.addAttribute(new Attribute("_CoordinateAxisType", AxisType.Lon.toString()));
        }
        if (id.equals("0-7-1") || id.equals("0-7-2") || id.equals("0-7-10") || id.equals("0-7-30")) {
            v.addAttribute(new Attribute("_CoordinateAxisType", AxisType.Height.toString()));
        }
        if (id.equals("0-7-6") || id.equals("0-7-7")) {
            v.addAttribute(new Attribute("_CoordinateAxisType", AxisType.Height.toString()));
        }
        if (id.equals("0-1-7") || id.equals("0-1-11") || id.equals("0-1-18") || (id.equals("0-1-194") && this.proto.ids.getCenterId() == 59)) {
            v.addAttribute(new Attribute("standard_name", "station_id"));
        }
        if (id.equals("0-1-2")) {
            v.addAttribute(new Attribute("standard_name", "station_WMO_id"));
        }
    }
    
    private boolean hasTime() throws IOException {
        final DataDescriptor root = this.proto.getRootDataDescriptor();
        for (final DataDescriptor dkey : root.subKeys) {
            if (!dkey.isOkForVariable()) {
                continue;
            }
            final String key = dkey.getFxyName();
            if (key.equals("0-4-1") && this.yearName == null) {
                this.yearName = dkey.name;
            }
            if (key.equals("0-4-2") && this.monthName == null) {
                this.monthName = dkey.name;
            }
            if (key.equals("0-4-3") && this.dayName == null) {
                this.dayName = dkey.name;
            }
            if (key.equals("0-4-43") && this.doyName == null) {
                this.doyName = dkey.name;
            }
            if (key.equals("0-4-4") && this.hourName == null) {
                this.hourName = dkey.name;
            }
            if (key.equals("0-4-5") && this.minName == null) {
                this.minName = dkey.name;
            }
            if ((!key.equals("0-4-6") && !key.equals("0-4-7")) || this.secName != null) {
                continue;
            }
            this.secName = dkey.name;
        }
        this.hasTime = (this.yearName != null && ((this.monthName != null && this.dayName != null) || this.doyName != null) && this.hourName != null);
        if (this.hasTime) {
            String u;
            if (this.secName != null) {
                u = "secs";
            }
            else if (this.minName != null) {
                u = "minutes";
            }
            else {
                u = "hours";
            }
            try {
                final DateFormatter format = new DateFormatter();
                this.dateUnit = new DateUnit(u + " since " + format.toDateTimeStringISO(this.proto.getReferenceTime()));
            }
            catch (Exception e) {
                ConstructNC.log.error("BufrIosp failed to create date unit", e);
                this.hasTime = false;
            }
        }
        if (this.hasTime) {
            (this.cal = new GregorianCalendar(TimeZone.getTimeZone("GMT"))).clear();
        }
        return this.hasTime;
    }
    
    double makeObsTimeValue(final ArrayStructure abb) {
        final int year = abb.convertScalarInt(0, abb.findMember(this.yearName));
        final int hour = abb.convertScalarInt(0, abb.findMember(this.hourName));
        final int min = (this.minName == null) ? 0 : abb.convertScalarInt(0, abb.findMember(this.minName));
        final int sec = (this.secName == null) ? 0 : abb.convertScalarInt(0, abb.findMember(this.secName));
        if (this.dayName != null) {
            final int day = abb.convertScalarInt(0, abb.findMember(this.dayName));
            final int month = abb.convertScalarInt(0, abb.findMember(this.monthName));
            this.cal.set(year, month - 1, day, hour, min, sec);
        }
        else {
            final int doy = abb.convertScalarInt(0, abb.findMember(this.doyName));
            this.cal.set(1, year);
            this.cal.set(6, doy);
            this.cal.set(11, hour);
            this.cal.set(12, min);
            this.cal.set(13, sec);
        }
        final Date d = this.cal.getTime();
        return this.dateUnit.makeValue(d);
    }
    
    double makeObsTimeValue(final StructureData sdata) {
        final int year = sdata.convertScalarInt(this.yearName);
        final int hour = sdata.convertScalarInt(this.hourName);
        final int min = (this.minName == null) ? 0 : sdata.convertScalarInt(this.minName);
        final int sec = (this.secName == null) ? 0 : sdata.convertScalarInt(this.secName);
        if (this.dayName != null) {
            final int day = sdata.convertScalarInt(this.dayName);
            final int month = sdata.convertScalarInt(this.monthName);
            this.cal.set(year, month - 1, day, hour, min, sec);
        }
        else {
            final int doy = sdata.convertScalarInt(this.doyName);
            this.cal.set(1, year);
            this.cal.set(6, doy);
            this.cal.set(11, hour);
            this.cal.set(12, min);
            this.cal.set(13, sec);
        }
        final Date d = this.cal.getTime();
        return this.dateUnit.makeValue(d);
    }
    
    private static String identifyCoords(final String val) {
        if (val.equals("0-5-1") || val.equals("0-5-2")) {
            return AxisType.Lat.toString();
        }
        if (val.equals("0-6-1") || val.equals("0-6-2")) {
            return AxisType.Lon.toString();
        }
        if (val.equals("0-7-30") || val.equals("0-7-1") || val.equals("0-7-2") || val.equals("0-7-10")) {
            return AxisType.Height.toString();
        }
        if (val.equals("0-4-1")) {
            return "year";
        }
        if (val.equals("0-4-2")) {
            return "month";
        }
        if (val.equals("0-4-3")) {
            return "day";
        }
        if (val.equals("0-4-4")) {
            return "hour";
        }
        if (val.equals("0-4-5")) {
            return "minute";
        }
        if (val.equals("0-4-6") || val.equals("0-4-7")) {
            return "sec";
        }
        if (val.equals("0-1-1")) {
            return "wmo_block";
        }
        if (val.equals("0-1-2")) {
            return "wmo_id";
        }
        if (val.equals("0-1-7") || val.equals("0-1-194") || val.equals("0-1-11") || val.equals("0-1-18")) {
            return "station_id";
        }
        return null;
    }
    
    static {
        ConstructNC.log = LoggerFactory.getLogger(ConstructNC.class);
    }
}
