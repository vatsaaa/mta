// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.iosp.bufr;

import ucar.nc2.Variable;
import ucar.nc2.Sequence;
import ucar.ma2.ArraySequence;
import java.util.Iterator;
import java.util.List;
import java.io.IOException;
import java.nio.ByteBuffer;
import ucar.ma2.StructureMembers;
import ucar.ma2.Range;
import java.nio.ByteOrder;
import ucar.ma2.ArrayStructureBB;
import ucar.ma2.ArrayStructure;
import java.util.Formatter;
import ucar.unidata.io.RandomAccessFile;
import ucar.nc2.Structure;

public class MessageUncompressedDataReader
{
    public ArrayStructure readEntireMessage(final Structure s, final Message proto, final Message m, final RandomAccessFile raf, final Formatter f) throws IOException {
        DataDescriptor.transferInfo(proto.getRootDataDescriptor().getSubKeys(), m.getRootDataDescriptor().getSubKeys());
        final StructureMembers members = s.makeStructureMembers();
        ArrayStructureBB.setOffsets(members);
        final int n = m.getNumberDatasets();
        final ArrayStructureBB abb = new ArrayStructureBB(members, new int[] { n });
        final ByteBuffer bb = abb.getByteBuffer();
        bb.order(ByteOrder.BIG_ENDIAN);
        final boolean addTime = s.findVariable("time") != null;
        this.readData(abb, m, raf, null, addTime, f);
        return abb;
    }
    
    public int readData(final ArrayStructureBB abb, final Message m, final RandomAccessFile raf, final Range r, final boolean addTime, final Formatter f) throws IOException {
        final BitReader reader = new BitReader(raf, m.dataSection.getDataPos() + 4L);
        final DataDescriptor root = m.getRootDataDescriptor();
        if (root.isBad) {
            return 0;
        }
        final Request req = new Request(abb, r);
        final int n = m.getNumberDatasets();
        m.counterDatasets = new BitCounterUncompressed[n];
        m.msg_nbits = 0;
        int count = 0;
        for (int i = 0; i < n; ++i) {
            if (f != null) {
                f.format("Count bits in observation %d\n", i);
            }
            m.counterDatasets[i] = new BitCounterUncompressed(root, 1, 0);
            final DebugOut out = (f == null) ? null : new DebugOut(f);
            req.setRow(i);
            int timePos = 0;
            if (req.wantRow() && addTime) {
                timePos = req.bb.position();
                req.bb.putInt(0);
                ++count;
            }
            this.readData(out, reader, m.counterDatasets[i], root.subKeys, 0, req);
            m.msg_nbits += m.counterDatasets[i].countBits(m.msg_nbits);
        }
        return count;
    }
    
    private void readData(final DebugOut out, final BitReader reader, final BitCounterUncompressed table, final List<DataDescriptor> dkeys, final int nestedRow, final Request req) throws IOException {
        for (final DataDescriptor dkey : dkeys) {
            if (!dkey.isOkForVariable()) {
                if (out == null) {
                    continue;
                }
                out.f.format("%s %d %s (%s) %n", out.indent(), out.fldno++, dkey.name, dkey.getFxyName());
            }
            else if (dkey.replication == 0) {
                final int count = (int)reader.bits2UInt(dkey.replicationCountSize);
                if (out != null) {
                    out.f.format("%4d delayed replication count=%d %n", out.fldno++, count);
                }
                if (out != null && count > 0) {
                    out.f.format("%4d %s read sequence %s count= %d bitSize=%d start at=0x%x %n", out.fldno, out.indent(), dkey.getFxyName(), count, dkey.replicationCountSize, reader.getPos());
                }
                final BitCounterUncompressed bitCounterNested = table.makeNested(dkey, count, nestedRow, dkey.replicationCountSize);
                final ArraySequence seq = this.makeArraySequenceUncompressed(out, reader, bitCounterNested, dkey, req);
                if (!req.wantRow()) {
                    continue;
                }
                final int index = req.abb.addObjectToHeap(seq);
                req.bb.putInt(index);
            }
            else if (dkey.type == 3) {
                final BitCounterUncompressed nested = table.makeNested(dkey, dkey.replication, nestedRow, 0);
                if (out != null) {
                    out.f.format("%4d %s read structure %s count= %d\n", out.fldno, out.indent(), dkey.getFxyName(), dkey.replication);
                }
                for (int i = 0; i < dkey.replication; ++i) {
                    if (out != null) {
                        out.f.format("%s read row %d (struct %s) %n", out.indent(), i, dkey.getFxyName());
                        out.indent.incr();
                        this.readData(out, reader, nested, dkey.subKeys, i, req);
                        out.indent.decr();
                    }
                    else {
                        this.readData(null, reader, nested, dkey.subKeys, i, req);
                    }
                }
            }
            else if (dkey.type == 1) {
                final byte[] vals = this.readCharData(dkey, reader, req);
                if (out == null) {
                    continue;
                }
                final String s = new String(vals, "UTF-8");
                out.f.format("%4d %s read char %s (%s) width=%d end at= 0x%x val=<%s>\n", out.fldno++, out.indent(), dkey.getFxyName(), dkey.getName(), dkey.bitWidth, reader.getPos(), s);
            }
            else {
                final long val = this.readNumericData(dkey, reader, req);
                if (out == null) {
                    continue;
                }
                out.f.format("%4d %s read %s (%s %s) bitWidth=%d end at= 0x%x raw=%d convert=%f\n", out.fldno++, out.indent(), dkey.getFxyName(), dkey.getName(), dkey.getUnits(), dkey.bitWidth, reader.getPos(), val, dkey.convert(val));
            }
        }
    }
    
    private byte[] readCharData(final DataDescriptor dkey, final BitReader reader, final Request req) throws IOException {
        final int nchars = dkey.getByteWidthCDM();
        final byte[] b = new byte[nchars];
        for (int i = 0; i < nchars; ++i) {
            b[i] = (byte)reader.bits2UInt(8);
        }
        if (req.wantRow()) {
            for (int i = 0; i < nchars; ++i) {
                req.bb.put(b[i]);
            }
        }
        return b;
    }
    
    private long readNumericData(final DataDescriptor dkey, final BitReader reader, final Request req) throws IOException {
        final long result = reader.bits2UInt(dkey.bitWidth);
        if (req.wantRow()) {
            if (dkey.getByteWidthCDM() == 1) {
                req.bb.put((byte)result);
            }
            else if (dkey.getByteWidthCDM() == 2) {
                final byte b1 = (byte)(result & 0xFFL);
                final byte b2 = (byte)((result & 0xFF00L) >> 8);
                req.bb.put(b2);
                req.bb.put(b1);
            }
            else if (dkey.getByteWidthCDM() == 4) {
                final byte b1 = (byte)(result & 0xFFL);
                final byte b2 = (byte)((result & 0xFF00L) >> 8);
                final byte b3 = (byte)((result & 0xFF0000L) >> 16);
                final byte b4 = (byte)((result & 0xFFFFFFFFFF000000L) >> 24);
                req.bb.put(b4);
                req.bb.put(b3);
                req.bb.put(b2);
                req.bb.put(b1);
            }
            else {
                final byte b1 = (byte)(result & 0xFFL);
                final byte b2 = (byte)((result & 0xFF00L) >> 8);
                final byte b3 = (byte)((result & 0xFF0000L) >> 16);
                final byte b4 = (byte)((result & 0xFFFFFFFFFF000000L) >> 24);
                final byte b5 = (byte)((result & 0xFF00000000L) >> 32);
                final byte b6 = (byte)((result & 0xFF0000000000L) >> 40);
                final byte b7 = (byte)((result & 0xFF000000000000L) >> 48);
                final byte b8 = (byte)((result & 0xFF00000000000000L) >> 56);
                req.bb.put(b8);
                req.bb.put(b7);
                req.bb.put(b6);
                req.bb.put(b5);
                req.bb.put(b4);
                req.bb.put(b3);
                req.bb.put(b2);
                req.bb.put(b1);
            }
        }
        return result;
    }
    
    private ArraySequence makeArraySequenceUncompressed(final DebugOut out, final BitReader reader, final BitCounterUncompressed bitCounterNested, final DataDescriptor seqdd, final Request req) throws IOException {
        final int count = bitCounterNested.getNumberRows();
        ArrayStructureBB abb = null;
        StructureMembers members = null;
        if (req.wantRow()) {
            final Sequence seq = (Sequence)seqdd.refersTo;
            assert seq != null;
            final int[] shape = { count };
            int offset = 0;
            members = seq.makeStructureMembers();
            for (final StructureMembers.Member m : members.getMembers()) {
                m.setDataParam(offset);
                final Variable mv = seq.findVariable(m.getName());
                final DataDescriptor dk = (DataDescriptor)mv.getSPobject();
                if (dk.replication == 0) {
                    offset += 4;
                }
                else {
                    offset += dk.getByteWidthCDM();
                }
                if (m.getStructureMembers() != null) {
                    ArrayStructureBB.setOffsets(m.getStructureMembers());
                }
            }
            abb = new ArrayStructureBB(members, shape);
            final ByteBuffer bb = abb.getByteBuffer();
            bb.order(ByteOrder.BIG_ENDIAN);
        }
        final Request nreq = (req == null) ? null : new Request(abb, null);
        for (int i = 0; i < count; ++i) {
            if (out != null) {
                out.f.format("%s read row %d (seq %s) %n", out.indent(), i, seqdd.getFxyName());
                out.indent.incr();
                this.readData(out, reader, bitCounterNested, seqdd.getSubKeys(), i, nreq);
                out.indent.decr();
            }
            else {
                this.readData(null, reader, bitCounterNested, seqdd.getSubKeys(), i, nreq);
            }
        }
        return req.wantRow() ? new ArraySequence(members, abb.getStructureDataIterator(), count) : null;
    }
    
    private class Request
    {
        ArrayStructureBB abb;
        ByteBuffer bb;
        Range r;
        int row;
        
        Request(final ArrayStructureBB abb, final Range r) {
            this.abb = abb;
            if (abb != null) {
                this.bb = abb.getByteBuffer();
            }
            this.r = r;
            this.row = 0;
        }
        
        Request setRow(final int row) {
            this.row = row;
            return this;
        }
        
        boolean wantRow() {
            return this.abb != null && (this.r == null || this.r.contains(this.row));
        }
    }
}
