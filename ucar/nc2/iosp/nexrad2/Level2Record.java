// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.iosp.nexrad2;

import org.slf4j.LoggerFactory;
import ucar.ma2.IndexIterator;
import ucar.ma2.Range;
import java.io.PrintStream;
import java.io.IOException;
import ucar.unidata.io.RandomAccessFile;
import java.util.Date;
import org.slf4j.Logger;

public class Level2Record
{
    public static final int REFLECTIVITY = 1;
    public static final int VELOCITY_HI = 2;
    public static final int VELOCITY_LOW = 4;
    public static final int SPECTRUM_WIDTH = 3;
    public static final int DOPPLER_RESOLUTION_LOW_CODE = 4;
    public static final int DOPPLER_RESOLUTION_HIGH_CODE = 2;
    public static final float HORIZONTAL_BEAM_WIDTH = 1.5f;
    public static final int REFLECTIVITY_HIGH = 5;
    public static final int VELOCITY_HIGH = 6;
    public static final int SPECTRUM_WIDTH_HIGH = 7;
    public static final int DIFF_REFLECTIVITY_HIGH = 8;
    public static final int DIFF_PHASE = 9;
    public static final int CORRELATION_COEFFICIENT = 10;
    public static final byte MISSING_DATA = 1;
    public static final byte BELOW_THRESHOLD = 0;
    static final int FILE_HEADER_SIZE = 24;
    private static final int CTM_HEADER_SIZE = 12;
    private static final int MESSAGE_HEADER_SIZE = 28;
    private static final int RADAR_DATA_SIZE = 2432;
    private static Logger logger;
    int recno;
    long message_offset;
    boolean hasReflectData;
    boolean hasDopplerData;
    boolean hasHighResREFData;
    boolean hasHighResVELData;
    boolean hasHighResSWData;
    boolean hasHighResZDRData;
    boolean hasHighResPHIData;
    boolean hasHighResRHOData;
    short message_size;
    byte id_channel;
    public byte message_type;
    short id_sequence;
    short mess_julian_date;
    int mess_msecs;
    short seg_count;
    short seg_number;
    int data_msecs;
    short data_julian_date;
    short unamb_range;
    int azimuth_ang;
    short radial_num;
    short radial_status;
    short elevation_ang;
    short elevation_num;
    short reflect_first_gate;
    short reflect_gate_size;
    short reflect_gate_count;
    short doppler_first_gate;
    short doppler_gate_size;
    short doppler_gate_count;
    short cut;
    float calibration;
    short resolution;
    short vcp;
    short nyquist_vel;
    short attenuation;
    short threshhold;
    short ref_snr_threshold;
    short vel_snr_threshold;
    short sw_snr_threshold;
    short zdrHR_snr_threshold;
    short phiHR_snr_threshold;
    short rhoHR_snr_threshold;
    short ref_rf_threshold;
    short vel_rf_threshold;
    short sw_rf_threshold;
    short zdrHR_rf_threshold;
    short phiHR_rf_threshold;
    short rhoHR_rf_threshold;
    private short reflect_offset;
    private short velocity_offset;
    private short spectWidth_offset;
    short rlength;
    String id;
    float azimuth;
    byte compressIdx;
    byte sp;
    byte ars;
    byte rs;
    float elevation;
    byte rsbs;
    byte aim;
    short dcount;
    int dbp1;
    int dbp2;
    int dbp3;
    int dbp4;
    int dbp5;
    int dbp6;
    int dbp7;
    int dbp8;
    int dbp9;
    short reflectHR_gate_count;
    short velocityHR_gate_count;
    short spectrumHR_gate_count;
    float reflectHR_scale;
    float velocityHR_scale;
    float spectrumHR_scale;
    float zdrHR_scale;
    float phiHR_scale;
    float rhoHR_scale;
    float reflectHR_addoffset;
    float velocityHR_addoffset;
    float spectrumHR_addoffset;
    float zdrHR_addoffset;
    float phiHR_addoffset;
    float rhoHR_addoffset;
    short reflectHR_offset;
    short velocityHR_offset;
    short spectrumHR_offset;
    short zdrHR_offset;
    short phiHR_offset;
    short rhoHR_offset;
    short zdrHR_gate_count;
    short phiHR_gate_count;
    short rhoHR_gate_count;
    short reflectHR_gate_size;
    short velocityHR_gate_size;
    short spectrumHR_gate_size;
    short zdrHR_gate_size;
    short phiHR_gate_size;
    short rhoHR_gate_size;
    short reflectHR_first_gate;
    short velocityHR_first_gate;
    short spectrumHR_first_gate;
    short zdrHR_first_gate;
    short phiHR_first_gate;
    short rhoHR_first_gate;
    
    public static String getDatatypeName(final int datatype) {
        switch (datatype) {
            case 1: {
                return "Reflectivity";
            }
            case 2:
            case 4: {
                return "RadialVelocity";
            }
            case 3: {
                return "SpectrumWidth";
            }
            case 5: {
                return "Reflectivity_HI";
            }
            case 6: {
                return "RadialVelocity_HI";
            }
            case 7: {
                return "SpectrumWidth_HI";
            }
            case 8: {
                return "Reflectivity_DIFF";
            }
            case 9: {
                return "Phase";
            }
            case 10: {
                return "RHO";
            }
            default: {
                throw new IllegalArgumentException();
            }
        }
    }
    
    public static String getDatatypeUnits(final int datatype) {
        switch (datatype) {
            case 1: {
                return "dBz";
            }
            case 2:
            case 3:
            case 4: {
                return "m/s";
            }
            case 5: {
                return "dBz";
            }
            case 8: {
                return "dBz";
            }
            case 6:
            case 7: {
                return "m/s";
            }
            case 9: {
                return "deg";
            }
            case 10: {
                return "N/A";
            }
            default: {
                throw new IllegalArgumentException();
            }
        }
    }
    
    public short getDatatypeSNRThreshhold(final int datatype) {
        switch (datatype) {
            case 5: {
                return this.ref_snr_threshold;
            }
            case 6: {
                return this.vel_snr_threshold;
            }
            case 7: {
                return this.sw_snr_threshold;
            }
            case 8: {
                return this.zdrHR_snr_threshold;
            }
            case 9: {
                return this.phiHR_snr_threshold;
            }
            case 10: {
                return this.rhoHR_snr_threshold;
            }
            default: {
                throw new IllegalArgumentException();
            }
        }
    }
    
    public short getDatatypeRangeFoldingThreshhold(final int datatype) {
        switch (datatype) {
            case 5: {
                return this.ref_rf_threshold;
            }
            case 6: {
                return this.vel_rf_threshold;
            }
            case 7: {
                return this.sw_rf_threshold;
            }
            case 1:
            case 2:
            case 3:
            case 4: {
                return this.threshhold;
            }
            case 8: {
                return this.zdrHR_rf_threshold;
            }
            case 9: {
                return this.phiHR_rf_threshold;
            }
            case 10: {
                return this.rhoHR_rf_threshold;
            }
            default: {
                throw new IllegalArgumentException();
            }
        }
    }
    
    public float getDatatypeScaleFactor(final int datatype) {
        switch (datatype) {
            case 1: {
                return 0.5f;
            }
            case 4: {
                return 1.0f;
            }
            case 2:
            case 3: {
                return 0.5f;
            }
            case 5: {
                return 1.0f / this.reflectHR_scale;
            }
            case 6: {
                return 1.0f / this.velocityHR_scale;
            }
            case 7: {
                return 1.0f / this.spectrumHR_scale;
            }
            case 8: {
                return 1.0f / this.zdrHR_scale;
            }
            case 9: {
                return 1.0f / this.phiHR_scale;
            }
            case 10: {
                return 1.0f / this.rhoHR_scale;
            }
            default: {
                throw new IllegalArgumentException();
            }
        }
    }
    
    public float getDatatypeAddOffset(final int datatype) {
        switch (datatype) {
            case 1: {
                return -33.0f;
            }
            case 4: {
                return -129.0f;
            }
            case 2:
            case 3: {
                return -64.5f;
            }
            case 5: {
                return this.reflectHR_addoffset * -1.0f / this.reflectHR_scale;
            }
            case 6: {
                return this.velocityHR_addoffset * -1.0f / this.velocityHR_scale;
            }
            case 7: {
                return this.spectrumHR_addoffset * -1.0f / this.spectrumHR_scale;
            }
            case 8: {
                return this.zdrHR_addoffset * -1.0f / this.zdrHR_scale;
            }
            case 9: {
                return this.phiHR_addoffset * -1.0f / this.phiHR_scale;
            }
            case 10: {
                return this.rhoHR_addoffset * -1.0f / this.rhoHR_scale;
            }
            default: {
                throw new IllegalArgumentException();
            }
        }
    }
    
    public static String getMessageTypeName(final int code) {
        switch (code) {
            case 1: {
                return "digital radar data";
            }
            case 2: {
                return "RDA status data";
            }
            case 3: {
                return "performance/maintainence data";
            }
            case 4: {
                return "console message - RDA to RPG";
            }
            case 5: {
                return "maintainence log data";
            }
            case 6: {
                return "RDA control ocmmands";
            }
            case 7: {
                return "volume coverage pattern";
            }
            case 8: {
                return "clutter censor zones";
            }
            case 9: {
                return "request for data";
            }
            case 10: {
                return "console message - RPG to RDA";
            }
            case 11: {
                return "loop back test - RDA to RPG";
            }
            case 12: {
                return "loop back test - RPG to RDA";
            }
            case 13: {
                return "clutter filter bypass map - RDA to RPG";
            }
            case 14: {
                return "edited clutter filter bypass map - RDA to RPG";
            }
            case 15: {
                return "Notchwidth Map";
            }
            case 18: {
                return "RDA Adaptation data";
            }
            case 31: {
                return "Digitail Radar Data Generic Format";
            }
            default: {
                return "unknown " + code;
            }
        }
    }
    
    public static String getRadialStatusName(final int code) {
        switch (code) {
            case 0: {
                return "start of new elevation";
            }
            case 1: {
                return "intermediate radial";
            }
            case 2: {
                return "end of elevation";
            }
            case 3: {
                return "begin volume scan";
            }
            case 4: {
                return "end volume scan";
            }
            default: {
                return "unknown " + code;
            }
        }
    }
    
    public static String getVolumeCoveragePatternName(final int code) {
        switch (code) {
            case 11: {
                return "16 elevation scans every 5 mins";
            }
            case 12: {
                return "14 elevation scan every 4.1 mins";
            }
            case 21: {
                return "11 elevation scans every 6 mins";
            }
            case 31: {
                return "8 elevation scans every 10 mins";
            }
            case 32: {
                return "7 elevation scans every 10 mins";
            }
            case 121: {
                return "9 elevations, 20 scans every 5 minutes";
            }
            case 211: {
                return "14 elevations, 16 scans every 5 mins";
            }
            case 212: {
                return "14 elevations, 17 scans every 4 mins";
            }
            case 221: {
                return "9 elevations, 11 scans every 5 minutes";
            }
            default: {
                return "unknown " + code;
            }
        }
    }
    
    public static Date getDate(final int julianDays, final int msecs) {
        final long total = (julianDays - 1) * 24L * 3600L * 1000L + msecs;
        return new Date(total);
    }
    
    public static Level2Record factory(final RandomAccessFile din, final int record, final long message_offset31) throws IOException {
        final long offset = record * 2432 + 24 + message_offset31;
        if (offset >= din.length()) {
            return null;
        }
        return new Level2Record(din, record, message_offset31);
    }
    
    public Level2Record(final RandomAccessFile din, final int record, final long message_offset31) throws IOException {
        this.message_size = 0;
        this.id_channel = 0;
        this.message_type = 0;
        this.id_sequence = 0;
        this.mess_julian_date = 0;
        this.mess_msecs = 0;
        this.seg_count = 0;
        this.seg_number = 0;
        this.data_msecs = 0;
        this.data_julian_date = 0;
        this.unamb_range = 0;
        this.azimuth_ang = 0;
        this.radial_num = 0;
        this.radial_status = 0;
        this.elevation_ang = 0;
        this.elevation_num = 0;
        this.reflect_first_gate = 0;
        this.reflect_gate_size = 0;
        this.reflect_gate_count = 0;
        this.doppler_first_gate = 0;
        this.doppler_gate_size = 0;
        this.doppler_gate_count = 0;
        this.cut = 0;
        this.calibration = 0.0f;
        this.resolution = 0;
        this.vcp = 0;
        this.rlength = 0;
        this.reflectHR_gate_count = 0;
        this.velocityHR_gate_count = 0;
        this.spectrumHR_gate_count = 0;
        this.reflectHR_scale = 0.0f;
        this.velocityHR_scale = 0.0f;
        this.spectrumHR_scale = 0.0f;
        this.zdrHR_scale = 0.0f;
        this.phiHR_scale = 0.0f;
        this.rhoHR_scale = 0.0f;
        this.reflectHR_addoffset = 0.0f;
        this.velocityHR_addoffset = 0.0f;
        this.spectrumHR_addoffset = 0.0f;
        this.zdrHR_addoffset = 0.0f;
        this.phiHR_addoffset = 0.0f;
        this.rhoHR_addoffset = 0.0f;
        this.reflectHR_offset = 0;
        this.velocityHR_offset = 0;
        this.spectrumHR_offset = 0;
        this.zdrHR_offset = 0;
        this.phiHR_offset = 0;
        this.rhoHR_offset = 0;
        this.zdrHR_gate_count = 0;
        this.phiHR_gate_count = 0;
        this.rhoHR_gate_count = 0;
        this.reflectHR_gate_size = 0;
        this.velocityHR_gate_size = 0;
        this.spectrumHR_gate_size = 0;
        this.zdrHR_gate_size = 0;
        this.phiHR_gate_size = 0;
        this.rhoHR_gate_size = 0;
        this.reflectHR_first_gate = 0;
        this.velocityHR_first_gate = 0;
        this.spectrumHR_first_gate = 0;
        this.zdrHR_first_gate = 0;
        this.phiHR_first_gate = 0;
        this.rhoHR_first_gate = 0;
        this.recno = record;
        din.seek(this.message_offset = record * 2432 + 24 + message_offset31);
        din.skipBytes(12);
        this.message_size = din.readShort();
        this.id_channel = din.readByte();
        this.message_type = din.readByte();
        this.id_sequence = din.readShort();
        this.mess_julian_date = din.readShort();
        this.mess_msecs = din.readInt();
        this.seg_count = din.readShort();
        this.seg_number = din.readShort();
        if (this.message_type == 1) {
            this.data_msecs = din.readInt();
            this.data_julian_date = din.readShort();
            this.unamb_range = din.readShort();
            this.azimuth_ang = din.readUnsignedShort();
            this.radial_num = din.readShort();
            this.radial_status = din.readShort();
            this.elevation_ang = din.readShort();
            this.elevation_num = din.readShort();
            this.reflect_first_gate = din.readShort();
            this.doppler_first_gate = din.readShort();
            this.reflect_gate_size = din.readShort();
            this.doppler_gate_size = din.readShort();
            this.reflect_gate_count = din.readShort();
            this.doppler_gate_count = din.readShort();
            this.cut = din.readShort();
            this.calibration = din.readFloat();
            this.reflect_offset = din.readShort();
            this.velocity_offset = din.readShort();
            this.spectWidth_offset = din.readShort();
            this.resolution = din.readShort();
            this.vcp = din.readShort();
            din.skipBytes(14);
            this.nyquist_vel = din.readShort();
            this.attenuation = din.readShort();
            this.threshhold = din.readShort();
            this.hasReflectData = (this.reflect_gate_count > 0);
            this.hasDopplerData = (this.doppler_gate_count > 0);
            return;
        }
        if (this.message_type == 31) {
            this.id = din.readString(4);
            this.data_msecs = din.readInt();
            this.data_julian_date = din.readShort();
            this.radial_num = din.readShort();
            this.azimuth = din.readFloat();
            this.compressIdx = din.readByte();
            this.sp = din.readByte();
            this.rlength = din.readShort();
            this.ars = din.readByte();
            this.rs = din.readByte();
            this.elevation_num = din.readByte();
            this.cut = din.readByte();
            this.elevation = din.readFloat();
            this.rsbs = din.readByte();
            this.aim = din.readByte();
            this.dcount = din.readShort();
            this.dbp1 = din.readInt();
            this.dbp2 = din.readInt();
            this.dbp3 = din.readInt();
            this.dbp4 = din.readInt();
            this.dbp5 = din.readInt();
            this.dbp6 = din.readInt();
            this.dbp7 = din.readInt();
            this.dbp8 = din.readInt();
            this.dbp9 = din.readInt();
            this.vcp = this.getDataBlockValue(din, (short)this.dbp1, 40);
            int dbpp4 = 0;
            int dbpp5 = 0;
            int dbpp6 = 0;
            int dbpp7 = 0;
            int dbpp8 = 0;
            int dbpp9 = 0;
            if (this.dbp4 > 0) {
                final String tname = this.getDataBlockStringValue(din, (short)this.dbp4, 1, 3);
                if (tname.startsWith("REF")) {
                    this.hasHighResREFData = true;
                    dbpp4 = this.dbp4;
                }
                else if (tname.startsWith("VEL")) {
                    this.hasHighResVELData = true;
                    dbpp5 = this.dbp4;
                }
                else if (tname.startsWith("SW")) {
                    this.hasHighResSWData = true;
                    dbpp6 = this.dbp4;
                }
                else if (tname.startsWith("ZDR")) {
                    this.hasHighResZDRData = true;
                    dbpp7 = this.dbp4;
                }
                else if (tname.startsWith("PHI")) {
                    this.hasHighResPHIData = true;
                    dbpp8 = this.dbp4;
                }
                else if (tname.startsWith("RHO")) {
                    this.hasHighResRHOData = true;
                    dbpp9 = this.dbp4;
                }
                else {
                    System.out.println("Missing radial product");
                }
            }
            if (this.dbp5 > 0) {
                final String tname = this.getDataBlockStringValue(din, (short)this.dbp5, 1, 3);
                if (tname.startsWith("REF")) {
                    this.hasHighResREFData = true;
                    dbpp4 = this.dbp5;
                }
                else if (tname.startsWith("VEL")) {
                    this.hasHighResVELData = true;
                    dbpp5 = this.dbp5;
                }
                else if (tname.startsWith("SW")) {
                    this.hasHighResSWData = true;
                    dbpp6 = this.dbp5;
                }
                else if (tname.startsWith("ZDR")) {
                    this.hasHighResZDRData = true;
                    dbpp7 = this.dbp5;
                }
                else if (tname.startsWith("PHI")) {
                    this.hasHighResPHIData = true;
                    dbpp8 = this.dbp5;
                }
                else if (tname.startsWith("RHO")) {
                    this.hasHighResRHOData = true;
                    dbpp9 = this.dbp5;
                }
                else {
                    System.out.println("Missing radial product");
                }
            }
            if (this.dbp6 > 0) {
                final String tname = this.getDataBlockStringValue(din, (short)this.dbp6, 1, 3);
                if (tname.startsWith("REF")) {
                    this.hasHighResREFData = true;
                    dbpp4 = this.dbp6;
                }
                else if (tname.startsWith("VEL")) {
                    this.hasHighResVELData = true;
                    dbpp5 = this.dbp6;
                }
                else if (tname.startsWith("SW")) {
                    this.hasHighResSWData = true;
                    dbpp6 = this.dbp6;
                }
                else if (tname.startsWith("ZDR")) {
                    this.hasHighResZDRData = true;
                    dbpp7 = this.dbp6;
                }
                else if (tname.startsWith("PHI")) {
                    this.hasHighResPHIData = true;
                    dbpp8 = this.dbp6;
                }
                else if (tname.startsWith("RHO")) {
                    this.hasHighResRHOData = true;
                    dbpp9 = this.dbp6;
                }
                else {
                    System.out.println("Missing radial product");
                }
            }
            if (this.dbp7 > 0) {
                final String tname = this.getDataBlockStringValue(din, (short)this.dbp7, 1, 3);
                if (tname.startsWith("REF")) {
                    this.hasHighResREFData = true;
                    dbpp4 = this.dbp7;
                }
                else if (tname.startsWith("VEL")) {
                    this.hasHighResVELData = true;
                    dbpp5 = this.dbp7;
                }
                else if (tname.startsWith("SW")) {
                    this.hasHighResSWData = true;
                    dbpp6 = this.dbp7;
                }
                else if (tname.startsWith("ZDR")) {
                    this.hasHighResZDRData = true;
                    dbpp7 = this.dbp7;
                }
                else if (tname.startsWith("PHI")) {
                    this.hasHighResPHIData = true;
                    dbpp8 = this.dbp7;
                }
                else if (tname.startsWith("RHO")) {
                    this.hasHighResRHOData = true;
                    dbpp9 = this.dbp7;
                }
                else {
                    System.out.println("Missing radial product");
                }
            }
            if (this.dbp8 > 0) {
                final String tname = this.getDataBlockStringValue(din, (short)this.dbp8, 1, 3);
                if (tname.startsWith("REF")) {
                    this.hasHighResREFData = true;
                    dbpp4 = this.dbp8;
                }
                else if (tname.startsWith("VEL")) {
                    this.hasHighResVELData = true;
                    dbpp5 = this.dbp8;
                }
                else if (tname.startsWith("SW")) {
                    this.hasHighResSWData = true;
                    dbpp6 = this.dbp8;
                }
                else if (tname.startsWith("ZDR")) {
                    this.hasHighResZDRData = true;
                    dbpp7 = this.dbp8;
                }
                else if (tname.startsWith("PHI")) {
                    this.hasHighResPHIData = true;
                    dbpp8 = this.dbp8;
                }
                else if (tname.startsWith("RHO")) {
                    this.hasHighResRHOData = true;
                    dbpp9 = this.dbp8;
                }
                else {
                    System.out.println("Missing radial product");
                }
            }
            if (this.dbp9 > 0) {
                final String tname = this.getDataBlockStringValue(din, (short)this.dbp9, 1, 3);
                if (tname.startsWith("REF")) {
                    this.hasHighResREFData = true;
                    dbpp4 = this.dbp9;
                }
                else if (tname.startsWith("VEL")) {
                    this.hasHighResVELData = true;
                    dbpp5 = this.dbp9;
                }
                else if (tname.startsWith("SW")) {
                    this.hasHighResSWData = true;
                    dbpp6 = this.dbp9;
                }
                else if (tname.startsWith("ZDR")) {
                    this.hasHighResZDRData = true;
                    dbpp7 = this.dbp9;
                }
                else if (tname.startsWith("PHI")) {
                    this.hasHighResPHIData = true;
                    dbpp8 = this.dbp9;
                }
                else if (tname.startsWith("RHO")) {
                    this.hasHighResRHOData = true;
                    dbpp9 = this.dbp9;
                }
                else {
                    System.out.println("Missing radial product");
                }
            }
            if (this.hasHighResREFData) {
                this.reflectHR_gate_count = this.getDataBlockValue(din, (short)dbpp4, 8);
                this.reflectHR_first_gate = this.getDataBlockValue(din, (short)dbpp4, 10);
                this.reflectHR_gate_size = this.getDataBlockValue(din, (short)dbpp4, 12);
                this.ref_rf_threshold = this.getDataBlockValue(din, (short)dbpp4, 14);
                this.ref_snr_threshold = this.getDataBlockValue(din, (short)dbpp4, 16);
                this.reflectHR_scale = this.getDataBlockValue1(din, (short)dbpp4, 20);
                this.reflectHR_addoffset = this.getDataBlockValue1(din, (short)dbpp4, 24);
                this.reflectHR_offset = (short)(dbpp4 + 28);
            }
            if (this.hasHighResVELData) {
                this.velocityHR_gate_count = this.getDataBlockValue(din, (short)dbpp5, 8);
                this.velocityHR_first_gate = this.getDataBlockValue(din, (short)dbpp5, 10);
                this.velocityHR_gate_size = this.getDataBlockValue(din, (short)dbpp5, 12);
                this.vel_rf_threshold = this.getDataBlockValue(din, (short)dbpp5, 14);
                this.vel_snr_threshold = this.getDataBlockValue(din, (short)dbpp5, 16);
                this.velocityHR_scale = this.getDataBlockValue1(din, (short)dbpp5, 20);
                this.velocityHR_addoffset = this.getDataBlockValue1(din, (short)dbpp5, 24);
                this.velocityHR_offset = (short)(dbpp5 + 28);
            }
            if (this.hasHighResSWData) {
                this.spectrumHR_gate_count = this.getDataBlockValue(din, (short)dbpp6, 8);
                this.spectrumHR_first_gate = this.getDataBlockValue(din, (short)dbpp6, 10);
                this.spectrumHR_gate_size = this.getDataBlockValue(din, (short)dbpp6, 12);
                this.sw_rf_threshold = this.getDataBlockValue(din, (short)dbpp6, 14);
                this.sw_snr_threshold = this.getDataBlockValue(din, (short)dbpp6, 16);
                this.spectrumHR_scale = this.getDataBlockValue1(din, (short)dbpp6, 20);
                this.spectrumHR_addoffset = this.getDataBlockValue1(din, (short)dbpp6, 24);
                this.spectrumHR_offset = (short)(dbpp6 + 28);
            }
            if (this.hasHighResZDRData) {
                this.zdrHR_gate_count = this.getDataBlockValue(din, (short)dbpp7, 8);
                this.zdrHR_first_gate = this.getDataBlockValue(din, (short)dbpp7, 10);
                this.zdrHR_gate_size = this.getDataBlockValue(din, (short)dbpp7, 12);
                this.zdrHR_rf_threshold = this.getDataBlockValue(din, (short)dbpp7, 14);
                this.zdrHR_snr_threshold = this.getDataBlockValue(din, (short)dbpp7, 16);
                this.zdrHR_scale = this.getDataBlockValue1(din, (short)dbpp7, 20);
                this.zdrHR_addoffset = this.getDataBlockValue1(din, (short)dbpp7, 24);
                this.zdrHR_offset = (short)(dbpp7 + 28);
            }
            if (this.hasHighResPHIData) {
                this.phiHR_gate_count = this.getDataBlockValue(din, (short)dbpp8, 8);
                this.phiHR_first_gate = this.getDataBlockValue(din, (short)dbpp8, 10);
                this.phiHR_gate_size = this.getDataBlockValue(din, (short)dbpp8, 12);
                this.phiHR_rf_threshold = this.getDataBlockValue(din, (short)dbpp8, 14);
                this.phiHR_snr_threshold = this.getDataBlockValue(din, (short)dbpp8, 16);
                this.phiHR_scale = this.getDataBlockValue1(din, (short)dbpp8, 20);
                this.phiHR_addoffset = this.getDataBlockValue1(din, (short)dbpp8, 24);
                this.phiHR_offset = (short)(dbpp8 + 28);
            }
            if (this.hasHighResRHOData) {
                this.rhoHR_gate_count = this.getDataBlockValue(din, (short)dbpp9, 8);
                this.rhoHR_first_gate = this.getDataBlockValue(din, (short)dbpp9, 10);
                this.rhoHR_gate_size = this.getDataBlockValue(din, (short)dbpp9, 12);
                this.rhoHR_rf_threshold = this.getDataBlockValue(din, (short)dbpp9, 14);
                this.rhoHR_snr_threshold = this.getDataBlockValue(din, (short)dbpp9, 16);
                this.rhoHR_scale = this.getDataBlockValue1(din, (short)dbpp9, 20);
                this.rhoHR_addoffset = this.getDataBlockValue1(din, (short)dbpp9, 24);
                this.rhoHR_offset = (short)(dbpp9 + 28);
            }
        }
    }
    
    public void dumpMessage(final PrintStream out) {
        out.println(this.recno + " ---------------------");
        out.println(" message type = " + getMessageTypeName(this.message_type) + " (" + this.message_type + ")");
        out.println(" message size = " + this.message_size + " segment=" + this.seg_number + "/" + this.seg_count);
    }
    
    public void dump(final PrintStream out) {
        out.println(this.recno + " ------------------------------------------" + this.message_offset);
        out.println(" message type = " + getMessageTypeName(this.message_type));
        out.println(" data date = " + this.data_julian_date + " : " + this.data_msecs);
        out.println(" elevation = " + this.getElevation() + " (" + this.elevation_num + ")");
        out.println(" azimuth = " + this.getAzimuth());
        out.println(" radial = " + this.radial_num + " status= " + getRadialStatusName(this.radial_status) + " ratio = " + this.getAzimuth() / this.radial_num);
        out.println(" reflectivity first= " + this.reflect_first_gate + " size= " + this.reflect_gate_size + " count= " + this.reflect_gate_count);
        out.println(" doppler first= " + this.doppler_first_gate + " size= " + this.doppler_gate_size + " count= " + this.doppler_gate_count);
        out.println(" offset: reflect= " + this.reflect_offset + " velocity= " + this.velocity_offset + " spWidth= " + this.spectWidth_offset);
        out.println(" pattern = " + this.vcp + " cut= " + this.cut);
    }
    
    public void dump2(final PrintStream out) {
        out.println("recno= " + this.recno + " massType= " + this.message_type + " massSize = " + this.message_size);
    }
    
    public boolean checkOk() {
        boolean ok = true;
        if (Float.isNaN(this.getAzimuth())) {
            Level2Record.logger.warn("****" + this.recno + " HAS bad azimuth value = " + this.azimuth_ang);
            ok = false;
        }
        if (this.message_type != 1) {
            return ok;
        }
        if (this.seg_count != 1 || this.seg_number != 1) {
            Level2Record.logger.warn("*** segment = " + this.seg_number + "/" + this.seg_count + this.who());
        }
        if (this.reflect_offset < 0 || this.reflect_offset > 2432) {
            Level2Record.logger.warn("****" + this.recno + " HAS bad reflect offset= " + this.reflect_offset + this.who());
            ok = false;
        }
        if (this.velocity_offset < 0 || this.velocity_offset > 2432) {
            Level2Record.logger.warn("****" + this.recno + " HAS bad velocity offset= " + this.velocity_offset + this.who());
            ok = false;
        }
        if (this.spectWidth_offset < 0 || this.spectWidth_offset > 2432) {
            Level2Record.logger.warn("****" + this.recno + " HAS bad spwidth offset= " + this.reflect_offset + this.who());
            ok = false;
        }
        if (this.velocity_offset > 0 && this.spectWidth_offset <= 0) {
            Level2Record.logger.warn("****" + this.recno + " HAS velocity NOT spectWidth!!" + this.who());
            ok = false;
        }
        if (this.velocity_offset <= 0 && this.spectWidth_offset > 0) {
            Level2Record.logger.warn("****" + this.recno + " HAS spectWidth AND NOT velocity!!" + this.who());
            ok = false;
        }
        if (this.mess_julian_date != this.data_julian_date) {
            Level2Record.logger.warn("*** message date = " + this.mess_julian_date + " : " + this.mess_msecs + this.who() + "\n" + " data date = " + this.data_julian_date + " : " + this.data_msecs);
            ok = false;
        }
        if (!this.hasReflectData && !this.hasDopplerData) {
            Level2Record.logger.info("*** no reflect or dopplar = " + this.who());
        }
        return ok;
    }
    
    private String who() {
        return " message(" + this.recno + " " + this.message_offset + ")";
    }
    
    public float getAzimuth() {
        if (this.message_type == 31) {
            return this.azimuth;
        }
        if (this.message_type == 1) {
            return 180.0f * this.azimuth_ang / 32768.0f;
        }
        return -1.0f;
    }
    
    public float getElevation() {
        if (this.message_type == 31) {
            return this.elevation;
        }
        if (this.message_type == 1) {
            return 180.0f * this.elevation_ang / 32768.0f;
        }
        return -1.0f;
    }
    
    public int getGateSize(final int datatype) {
        switch (datatype) {
            case 1: {
                return this.reflect_gate_size;
            }
            case 2:
            case 3:
            case 4: {
                return this.doppler_gate_size;
            }
            case 5: {
                return this.reflectHR_gate_size;
            }
            case 6: {
                return this.velocityHR_gate_size;
            }
            case 7: {
                return this.spectrumHR_gate_size;
            }
            case 8: {
                return this.zdrHR_gate_size;
            }
            case 9: {
                return this.phiHR_gate_size;
            }
            case 10: {
                return this.rhoHR_gate_size;
            }
            default: {
                return -1;
            }
        }
    }
    
    public int getGateStart(final int datatype) {
        switch (datatype) {
            case 1: {
                return this.reflect_first_gate;
            }
            case 2:
            case 3:
            case 4: {
                return this.doppler_first_gate;
            }
            case 5: {
                return this.reflectHR_first_gate;
            }
            case 6: {
                return this.velocityHR_first_gate;
            }
            case 7: {
                return this.spectrumHR_first_gate;
            }
            case 8: {
                return this.zdrHR_first_gate;
            }
            case 9: {
                return this.phiHR_first_gate;
            }
            case 10: {
                return this.rhoHR_first_gate;
            }
            default: {
                return -1;
            }
        }
    }
    
    public int getGateCount(final int datatype) {
        switch (datatype) {
            case 1: {
                return this.reflect_gate_count;
            }
            case 2:
            case 3:
            case 4: {
                return this.doppler_gate_count;
            }
            case 5: {
                return this.reflectHR_gate_count;
            }
            case 6: {
                return this.velocityHR_gate_count;
            }
            case 7: {
                return this.spectrumHR_gate_count;
            }
            case 8: {
                return this.zdrHR_gate_count;
            }
            case 9: {
                return this.phiHR_gate_count;
            }
            case 10: {
                return this.rhoHR_gate_count;
            }
            default: {
                return 0;
            }
        }
    }
    
    private short getDataOffset(final int datatype) {
        switch (datatype) {
            case 1: {
                return this.reflect_offset;
            }
            case 2:
            case 4: {
                return this.velocity_offset;
            }
            case 3: {
                return this.spectWidth_offset;
            }
            case 5: {
                return this.reflectHR_offset;
            }
            case 6: {
                return this.velocityHR_offset;
            }
            case 7: {
                return this.spectrumHR_offset;
            }
            case 8: {
                return (short)this.dbp7;
            }
            case 9: {
                return (short)this.dbp8;
            }
            case 10: {
                return (short)this.dbp9;
            }
            default: {
                return -32768;
            }
        }
    }
    
    private short getDataBlockValue(final RandomAccessFile raf, final short offset, final int skip) throws IOException {
        final long off = offset + this.message_offset + 28L;
        raf.seek(off);
        raf.skipBytes(skip);
        return raf.readShort();
    }
    
    private String getDataBlockStringValue(final RandomAccessFile raf, final short offset, final int skip, final int size) throws IOException {
        final long off = offset + this.message_offset + 28L;
        raf.seek(off);
        raf.skipBytes(skip);
        return raf.readString(size);
    }
    
    private float getDataBlockValue1(final RandomAccessFile raf, final short offset, final int skip) throws IOException {
        final long off = offset + this.message_offset + 28L;
        raf.seek(off);
        raf.skipBytes(skip);
        return raf.readFloat();
    }
    
    public Date getDate() {
        return getDate(this.data_julian_date, this.data_msecs);
    }
    
    public void readData(final RandomAccessFile raf, final int datatype, final Range gateRange, final IndexIterator ii) throws IOException {
        long offset = this.message_offset;
        offset += 28L;
        offset += this.getDataOffset(datatype);
        raf.seek(offset);
        if (Level2Record.logger.isDebugEnabled()) {
            Level2Record.logger.debug("  read recno " + this.recno + " at offset " + offset + " count= " + this.getGateCount(datatype));
            Level2Record.logger.debug("   offset: reflect= " + this.reflect_offset + " velocity= " + this.velocity_offset + " spWidth= " + this.spectWidth_offset);
        }
        final int dataCount = this.getGateCount(datatype);
        final byte[] data = new byte[dataCount];
        raf.readFully(data);
        final short[] ds = this.convertunsignedByte2Short(data);
        for (int i = gateRange.first(); i <= gateRange.last(); i += gateRange.stride()) {
            if (i >= dataCount) {
                ii.setByteNext((byte)1);
            }
            else {
                ii.setByteNext(data[i]);
            }
        }
    }
    
    public short[] convertunsignedByte2Short(final byte[] inb) {
        final int len = inb.length;
        final short[] outs = new short[len];
        int i = 0;
        for (final byte b : inb) {
            outs[i++] = this.convertunsignedByte2Short(b);
        }
        return outs;
    }
    
    public short convertunsignedByte2Short(final byte b) {
        return (short)((b < 0) ? (b + 256) : ((short)b));
    }
    
    @Override
    public String toString() {
        return "elev= " + this.elevation_num + " radial_num = " + this.radial_num;
    }
    
    static {
        Level2Record.logger = LoggerFactory.getLogger(Level2Record.class);
    }
}
