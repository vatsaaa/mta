// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.iosp.nexrad2;

import org.slf4j.LoggerFactory;
import ucar.ma2.InvalidRangeException;
import ucar.ma2.Range;
import ucar.ma2.Section;
import ucar.ma2.Index;
import ucar.ma2.IndexIterator;
import java.util.Iterator;
import ucar.nc2.constants.AxisType;
import ucar.ma2.Array;
import ucar.ma2.DataType;
import ucar.nc2.Structure;
import ucar.nc2.Dimension;
import java.util.ArrayList;
import java.util.Date;
import ucar.nc2.constants.FeatureType;
import ucar.unidata.geoloc.Earth;
import ucar.nc2.Group;
import ucar.nc2.Attribute;
import java.util.List;
import ucar.nc2.util.CancelTask;
import ucar.nc2.NetcdfFile;
import java.io.IOException;
import ucar.unidata.io.RandomAccessFile;
import ucar.nc2.units.DateFormatter;
import ucar.nc2.Variable;
import org.slf4j.Logger;
import ucar.nc2.iosp.AbstractIOServiceProvider;

public class Nexrad2IOServiceProvider extends AbstractIOServiceProvider
{
    private static Logger logger;
    private static final int MISSING_INT = -9999;
    private static final float MISSING_FLOAT = Float.NaN;
    private Level2VolumeScan volScan;
    private double radarRadius;
    private Variable v0;
    private Variable v1;
    private DateFormatter formatter;
    
    public Nexrad2IOServiceProvider() {
        this.formatter = new DateFormatter();
    }
    
    public boolean isValidFile(final RandomAccessFile raf) throws IOException {
        try {
            raf.seek(0L);
            final byte[] b = new byte[8];
            raf.read(b);
            final String test = new String(b);
            return test.equals("ARCHIVE2") || test.equals("AR2V0001") || test.equals("AR2V0003") || test.equals("AR2V0004") || test.equals("AR2V0002") || test.equals("AR2V0006");
        }
        catch (IOException ioe) {
            return false;
        }
    }
    
    @Override
    public void open(final RandomAccessFile raf, final NetcdfFile ncfile, final CancelTask cancelTask) throws IOException {
        NexradStationDB.init();
        this.volScan = new Level2VolumeScan(raf, cancelTask);
        if (this.volScan.hasDifferentDopplarResolutions()) {
            throw new IllegalStateException("volScan.hasDifferentDopplarResolutions");
        }
        if (this.volScan.hasHighResolutions(0)) {
            if (this.volScan.getHighResReflectivityGroups() != null) {
                this.makeVariable2(ncfile, 5, "Reflectivity", "Reflectivity", "R", this.volScan);
            }
            if (this.volScan.getHighResVelocityGroups() != null) {
                this.makeVariable2(ncfile, 6, "RadialVelocity", "Radial Velocity", "V", this.volScan);
            }
            if (this.volScan.getHighResSpectrumGroups() != null) {
                final List<Level2Record> gps = (List<Level2Record>)this.volScan.getHighResSpectrumGroups();
                final List<Level2Record> gp = (List<Level2Record>)gps.get(0);
                final Level2Record record = gp.get(0);
                if (this.v1 != null) {
                    this.makeVariableNoCoords(ncfile, 7, "SpectrumWidth_HI", "Radial Spectrum_HI", this.v1, record);
                }
                if (this.v0 != null) {
                    this.makeVariableNoCoords(ncfile, 7, "SpectrumWidth", "Radial Spectrum", this.v0, record);
                }
            }
        }
        List<Level2Record> gps = (List<Level2Record>)this.volScan.getHighResDiffReflectGroups();
        if (gps != null) {
            this.makeVariable2(ncfile, 8, "DifferentialReflectivity", "Differential Reflectivity", "D", this.volScan);
        }
        gps = (List<Level2Record>)this.volScan.getHighResCoeffocientGroups();
        if (gps != null) {
            this.makeVariable2(ncfile, 10, "CorrelationCoefficient", "Correlation Coefficient", "C", this.volScan);
        }
        gps = (List<Level2Record>)this.volScan.getHighResDiffPhaseGroups();
        if (gps != null) {
            this.makeVariable2(ncfile, 9, "DifferentialPhase", "Differential Phase", "P", this.volScan);
        }
        gps = (List<Level2Record>)this.volScan.getReflectivityGroups();
        if (gps != null) {
            this.makeVariable(ncfile, 1, "Reflectivity", "Reflectivity", "R", this.volScan.getReflectivityGroups(), 0);
            final int velocity_type = (this.volScan.getDopplarResolution() == 2) ? 2 : 4;
            final Variable v = this.makeVariable(ncfile, velocity_type, "RadialVelocity", "Radial Velocity", "V", this.volScan.getVelocityGroups(), 0);
            gps = (List<Level2Record>)this.volScan.getVelocityGroups();
            final List<Level2Record> gp2 = (List<Level2Record>)gps.get(0);
            final Level2Record record2 = gp2.get(0);
            this.makeVariableNoCoords(ncfile, 3, "SpectrumWidth", "Spectrum Width", v, record2);
        }
        if (this.volScan.getStationId() != null) {
            ncfile.addAttribute(null, new Attribute("Station", this.volScan.getStationId()));
            ncfile.addAttribute(null, new Attribute("StationName", this.volScan.getStationName()));
            ncfile.addAttribute(null, new Attribute("StationLatitude", this.volScan.getStationLatitude()));
            ncfile.addAttribute(null, new Attribute("StationLongitude", this.volScan.getStationLongitude()));
            ncfile.addAttribute(null, new Attribute("StationElevationInMeters", this.volScan.getStationElevation()));
            final double latRadiusDegrees = Math.toDegrees(this.radarRadius / Earth.getRadius());
            ncfile.addAttribute(null, new Attribute("geospatial_lat_min", this.volScan.getStationLatitude() - latRadiusDegrees));
            ncfile.addAttribute(null, new Attribute("geospatial_lat_max", this.volScan.getStationLatitude() + latRadiusDegrees));
            final double cosLat = Math.cos(Math.toRadians(this.volScan.getStationLatitude()));
            final double lonRadiusDegrees = Math.toDegrees(this.radarRadius / cosLat / Earth.getRadius());
            ncfile.addAttribute(null, new Attribute("geospatial_lon_min", this.volScan.getStationLongitude() - lonRadiusDegrees));
            ncfile.addAttribute(null, new Attribute("geospatial_lon_max", this.volScan.getStationLongitude() + lonRadiusDegrees));
        }
        final DateFormatter formatter = new DateFormatter();
        ncfile.addAttribute(null, new Attribute("Conventions", "_Coordinates"));
        ncfile.addAttribute(null, new Attribute("format", this.volScan.getDataFormat()));
        ncfile.addAttribute(null, new Attribute("cdm_data_type", FeatureType.RADIAL.toString()));
        final Date d = Level2Record.getDate(this.volScan.getTitleJulianDays(), this.volScan.getTitleMsecs());
        ncfile.addAttribute(null, new Attribute("base_date", formatter.toDateOnlyString(d)));
        ncfile.addAttribute(null, new Attribute("time_coverage_start", formatter.toDateTimeStringISO(d)));
        ncfile.addAttribute(null, new Attribute("time_coverage_end", formatter.toDateTimeStringISO(this.volScan.getEndDate())));
        ncfile.addAttribute(null, new Attribute("history", "Direct read of Nexrad Level 2 file into NetCDF-Java 2.2 API"));
        ncfile.addAttribute(null, new Attribute("DataType", "Radial"));
        ncfile.addAttribute(null, new Attribute("Title", "Nexrad Level 2 Station " + this.volScan.getStationId() + " from " + formatter.toDateTimeStringISO(this.volScan.getStartDate()) + " to " + formatter.toDateTimeStringISO(this.volScan.getEndDate())));
        ncfile.addAttribute(null, new Attribute("Summary", "Weather Surveillance Radar-1988 Doppler (WSR-88D) Level II data are the three meteorological base data quantities: reflectivity, mean radial velocity, and spectrum width."));
        ncfile.addAttribute(null, new Attribute("keywords", "WSR-88D; NEXRAD; Radar Level II; reflectivity; mean radial velocity; spectrum width"));
        ncfile.addAttribute(null, new Attribute("VolumeCoveragePatternName", Level2Record.getVolumeCoveragePatternName(this.volScan.getVCP())));
        ncfile.addAttribute(null, new Attribute("VolumeCoveragePattern", this.volScan.getVCP()));
        ncfile.addAttribute(null, new Attribute("HorizonatalBeamWidthInDegrees", 1.5));
        ncfile.finish();
    }
    
    public void makeVariable2(final NetcdfFile ncfile, final int datatype, final String shortName, final String longName, final String abbrev, final Level2VolumeScan vScan) throws IOException {
        List groups = null;
        if (shortName.startsWith("Reflectivity")) {
            groups = vScan.getHighResReflectivityGroups();
        }
        else if (shortName.startsWith("RadialVelocity")) {
            groups = vScan.getHighResVelocityGroups();
        }
        else if (shortName.startsWith("DifferentialReflectivity")) {
            groups = vScan.getHighResDiffReflectGroups();
        }
        else if (shortName.startsWith("CorrelationCoefficient")) {
            groups = vScan.getHighResCoeffocientGroups();
        }
        else if (shortName.startsWith("DifferentialPhase")) {
            groups = vScan.getHighResDiffPhaseGroups();
        }
        final int nscans = groups.size();
        if (nscans == 0) {
            throw new IllegalStateException("No data for " + shortName);
        }
        final ArrayList firstGroup = new ArrayList(groups.size());
        final ArrayList secondGroup = new ArrayList(groups.size());
        for (int i = 0; i < nscans; ++i) {
            final List o = groups.get(i);
            final Level2Record firstRecord = o.get(0);
            final int ol = o.size();
            if (ol >= 720) {
                firstGroup.add(o);
            }
            else if (ol <= 360) {
                secondGroup.add(o);
            }
            else if (firstRecord.getGateCount(5) > 500 || firstRecord.getGateCount(6) > 1000) {
                firstGroup.add(o);
            }
            else {
                secondGroup.add(o);
            }
        }
        if (firstGroup != null && firstGroup.size() > 0) {
            this.v1 = this.makeVariable(ncfile, datatype, shortName + "_HI", longName + "_HI", abbrev + "_HI", firstGroup, 1);
        }
        if (secondGroup != null && secondGroup.size() > 0) {
            this.v0 = this.makeVariable(ncfile, datatype, shortName, longName, abbrev, secondGroup, 0);
        }
    }
    
    public int getMaxRadials(final List groups) {
        int maxRadials = 0;
        for (int i = 0; i < groups.size(); ++i) {
            final ArrayList group = groups.get(i);
            maxRadials = Math.max(maxRadials, group.size());
        }
        return maxRadials;
    }
    
    public Variable makeVariable(final NetcdfFile ncfile, final int datatype, final String shortName, final String longName, final String abbrev, final List groups, final int rd) throws IOException {
        final int nscans = groups.size();
        if (nscans == 0) {
            throw new IllegalStateException("No data for " + shortName + " file= " + ncfile.getLocation());
        }
        final List<Level2Record> firstGroup = groups.get(0);
        final Level2Record firstRecord = firstGroup.get(0);
        final int ngates = firstRecord.getGateCount(datatype);
        final String scanDimName = "scan" + abbrev;
        final String gateDimName = "gate" + abbrev;
        final String radialDimName = "radial" + abbrev;
        final Dimension scanDim = new Dimension(scanDimName, nscans);
        final Dimension gateDim = new Dimension(gateDimName, ngates);
        final Dimension radialDim = new Dimension(radialDimName, this.volScan.getMaxRadials(rd), true);
        ncfile.addDimension(null, scanDim);
        ncfile.addDimension(null, gateDim);
        ncfile.addDimension(null, radialDim);
        final List<Dimension> dims = new ArrayList<Dimension>();
        dims.add(scanDim);
        dims.add(radialDim);
        dims.add(gateDim);
        final Variable v = new Variable(ncfile, null, null, shortName);
        v.setDataType(DataType.BYTE);
        v.setDimensions(dims);
        ncfile.addVariable(null, v);
        v.addAttribute(new Attribute("units", Level2Record.getDatatypeUnits(datatype)));
        v.addAttribute(new Attribute("long_name", longName));
        final byte[] b = { 1, 0 };
        final Array missingArray = Array.factory(DataType.BYTE.getPrimitiveClassType(), new int[] { 2 }, b);
        v.addAttribute(new Attribute("missing_value", missingArray));
        v.addAttribute(new Attribute("signal_below_threshold", 0));
        v.addAttribute(new Attribute("scale_factor", firstRecord.getDatatypeScaleFactor(datatype)));
        v.addAttribute(new Attribute("add_offset", firstRecord.getDatatypeAddOffset(datatype)));
        v.addAttribute(new Attribute("_Unsigned", "true"));
        if (rd == 1) {
            v.addAttribute(new Attribute("SNR_threshold", firstRecord.getDatatypeSNRThreshhold(datatype)));
        }
        v.addAttribute(new Attribute("range_folding_threshold", firstRecord.getDatatypeRangeFoldingThreshhold(datatype)));
        final List<Dimension> dim2 = new ArrayList<Dimension>();
        dim2.add(scanDim);
        dim2.add(radialDim);
        final String timeCoordName = "time" + abbrev;
        final Variable timeVar = new Variable(ncfile, null, null, timeCoordName);
        timeVar.setDataType(DataType.INT);
        timeVar.setDimensions(dim2);
        ncfile.addVariable(null, timeVar);
        final Date d = Level2Record.getDate(this.volScan.getTitleJulianDays(), this.volScan.getTitleMsecs());
        final String units = "msecs since " + this.formatter.toDateTimeStringISO(d);
        timeVar.addAttribute(new Attribute("long_name", "time since base date"));
        timeVar.addAttribute(new Attribute("units", units));
        timeVar.addAttribute(new Attribute("missing_value", -9999));
        timeVar.addAttribute(new Attribute("_CoordinateAxisType", AxisType.Time.toString()));
        final String elevCoordName = "elevation" + abbrev;
        final Variable elevVar = new Variable(ncfile, null, null, elevCoordName);
        elevVar.setDataType(DataType.FLOAT);
        elevVar.setDimensions(dim2);
        ncfile.addVariable(null, elevVar);
        elevVar.addAttribute(new Attribute("units", "degrees"));
        elevVar.addAttribute(new Attribute("long_name", "elevation angle in degres: 0 = parallel to pedestal base, 90 = perpendicular"));
        elevVar.addAttribute(new Attribute("missing_value", Float.NaN));
        elevVar.addAttribute(new Attribute("_CoordinateAxisType", AxisType.RadialElevation.toString()));
        final String aziCoordName = "azimuth" + abbrev;
        final Variable aziVar = new Variable(ncfile, null, null, aziCoordName);
        aziVar.setDataType(DataType.FLOAT);
        aziVar.setDimensions(dim2);
        ncfile.addVariable(null, aziVar);
        aziVar.addAttribute(new Attribute("units", "degrees"));
        aziVar.addAttribute(new Attribute("long_name", "azimuth angle in degrees: 0 = true north, 90 = east"));
        aziVar.addAttribute(new Attribute("missing_value", Float.NaN));
        aziVar.addAttribute(new Attribute("_CoordinateAxisType", AxisType.RadialAzimuth.toString()));
        final String gateCoordName = "distance" + abbrev;
        final Variable gateVar = new Variable(ncfile, null, null, gateCoordName);
        gateVar.setDataType(DataType.FLOAT);
        gateVar.setDimensions(gateDimName);
        final Array data = Array.makeArray(DataType.FLOAT, ngates, firstRecord.getGateStart(datatype), firstRecord.getGateSize(datatype));
        gateVar.setCachedData(data, false);
        ncfile.addVariable(null, gateVar);
        this.radarRadius = firstRecord.getGateStart(datatype) + ngates * firstRecord.getGateSize(datatype);
        gateVar.addAttribute(new Attribute("units", "m"));
        gateVar.addAttribute(new Attribute("long_name", "radial distance to start of gate"));
        gateVar.addAttribute(new Attribute("_CoordinateAxisType", AxisType.RadialDistance.toString()));
        final String nradialsName = "numRadials" + abbrev;
        final Variable nradialsVar = new Variable(ncfile, null, null, nradialsName);
        nradialsVar.setDataType(DataType.INT);
        nradialsVar.setDimensions(scanDim.getName());
        nradialsVar.addAttribute(new Attribute("long_name", "number of valid radials in this scan"));
        ncfile.addVariable(null, nradialsVar);
        final String ngateName = "numGates" + abbrev;
        final Variable ngateVar = new Variable(ncfile, null, null, ngateName);
        ngateVar.setDataType(DataType.INT);
        ngateVar.setDimensions(scanDim.getName());
        ngateVar.addAttribute(new Attribute("long_name", "number of valid gates in this scan"));
        ncfile.addVariable(null, ngateVar);
        this.makeCoordinateDataWithMissing(datatype, timeVar, elevVar, aziVar, nradialsVar, ngateVar, groups);
        final String coordinates = timeCoordName + " " + elevCoordName + " " + aziCoordName + " " + gateCoordName;
        v.addAttribute(new Attribute("_CoordinateAxes", coordinates));
        final int nradials = radialDim.getLength();
        final Level2Record[][] map = new Level2Record[nscans][nradials];
        for (int i = 0; i < groups.size(); ++i) {
            final Level2Record[] mapScan = map[i];
            final List<Level2Record> group = groups.get(i);
            for (final Level2Record r : group) {
                final int radial = r.radial_num - 1;
                mapScan[radial] = r;
            }
        }
        final Vgroup vg = new Vgroup(datatype, map);
        v.setSPobject(vg);
        return v;
    }
    
    private void makeVariableNoCoords(final NetcdfFile ncfile, final int datatype, final String shortName, final String longName, final Variable from, final Level2Record record) {
        final Variable v = new Variable(ncfile, null, null, shortName);
        v.setDataType(DataType.BYTE);
        v.setDimensions(from.getDimensions());
        ncfile.addVariable(null, v);
        v.addAttribute(new Attribute("units", Level2Record.getDatatypeUnits(datatype)));
        v.addAttribute(new Attribute("long_name", longName));
        final byte[] b = { 1, 0 };
        final Array missingArray = Array.factory(DataType.BYTE.getPrimitiveClassType(), new int[] { 2 }, b);
        final Attribute scale = from.findAttribute("scale_factor");
        final Attribute offset = from.findAttribute("add_offset");
        v.addAttribute(new Attribute("missing_value", missingArray));
        v.addAttribute(new Attribute("signal_below_threshold", 0));
        v.addAttribute(new Attribute("scale_factor", record.getDatatypeScaleFactor(datatype)));
        v.addAttribute(new Attribute("add_offset", record.getDatatypeAddOffset(datatype)));
        v.addAttribute(new Attribute("_Unsigned", "true"));
        if (datatype == 7) {
            v.addAttribute(new Attribute("SNR_threshold", record.getDatatypeSNRThreshhold(datatype)));
        }
        v.addAttribute(new Attribute("range_folding_threshold", record.getDatatypeRangeFoldingThreshhold(datatype)));
        final Attribute fromAtt = from.findAttribute("_CoordinateAxes");
        v.addAttribute(new Attribute("_CoordinateAxes", fromAtt));
        final Vgroup vgFrom = (Vgroup)from.getSPobject();
        final Vgroup vg = new Vgroup(datatype, vgFrom.map);
        v.setSPobject(vg);
    }
    
    private void makeCoordinateData(final int datatype, final Variable time, final Variable elev, final Variable azi, final Variable nradialsVar, final Variable ngatesVar, final List groups) {
        final Array timeData = Array.factory(time.getDataType().getPrimitiveClassType(), time.getShape());
        final IndexIterator timeDataIter = timeData.getIndexIterator();
        final Array elevData = Array.factory(elev.getDataType().getPrimitiveClassType(), elev.getShape());
        final IndexIterator elevDataIter = elevData.getIndexIterator();
        final Array aziData = Array.factory(azi.getDataType().getPrimitiveClassType(), azi.getShape());
        final IndexIterator aziDataIter = aziData.getIndexIterator();
        final Array nradialsData = Array.factory(nradialsVar.getDataType().getPrimitiveClassType(), nradialsVar.getShape());
        final IndexIterator nradialsIter = nradialsData.getIndexIterator();
        final Array ngatesData = Array.factory(ngatesVar.getDataType().getPrimitiveClassType(), ngatesVar.getShape());
        final IndexIterator ngatesIter = ngatesData.getIndexIterator();
        int last_msecs = Integer.MIN_VALUE;
        final int nscans = groups.size();
        final int maxRadials = this.volScan.getMaxRadials(0);
        for (int i = 0; i < nscans; ++i) {
            final List scanGroup = groups.get(i);
            final int nradials = scanGroup.size();
            Level2Record first = null;
            for (int j = 0; j < nradials; ++j) {
                final Level2Record r = scanGroup.get(j);
                if (first == null) {
                    first = r;
                }
                timeDataIter.setIntNext(r.data_msecs);
                elevDataIter.setFloatNext(r.getElevation());
                aziDataIter.setFloatNext(r.getAzimuth());
                if (r.data_msecs < last_msecs) {
                    Nexrad2IOServiceProvider.logger.warn("makeCoordinateData time out of order " + r.data_msecs);
                }
                last_msecs = r.data_msecs;
            }
            for (int j = nradials; j < maxRadials; ++j) {
                timeDataIter.setIntNext(-9999);
                elevDataIter.setFloatNext(Float.NaN);
                aziDataIter.setFloatNext(Float.NaN);
            }
            nradialsIter.setIntNext(nradials);
            if (first != null) {
                ngatesIter.setIntNext(first.getGateCount(datatype));
            }
        }
        time.setCachedData(timeData, false);
        elev.setCachedData(elevData, false);
        azi.setCachedData(aziData, false);
        nradialsVar.setCachedData(nradialsData, false);
        ngatesVar.setCachedData(ngatesData, false);
    }
    
    private void makeCoordinateDataWithMissing(final int datatype, final Variable time, final Variable elev, final Variable azi, final Variable nradialsVar, final Variable ngatesVar, final List groups) {
        final Array timeData = Array.factory(time.getDataType().getPrimitiveClassType(), time.getShape());
        final Index timeIndex = timeData.getIndex();
        final Array elevData = Array.factory(elev.getDataType().getPrimitiveClassType(), elev.getShape());
        final Index elevIndex = elevData.getIndex();
        final Array aziData = Array.factory(azi.getDataType().getPrimitiveClassType(), azi.getShape());
        final Index aziIndex = aziData.getIndex();
        final Array nradialsData = Array.factory(nradialsVar.getDataType().getPrimitiveClassType(), nradialsVar.getShape());
        final IndexIterator nradialsIter = nradialsData.getIndexIterator();
        final Array ngatesData = Array.factory(ngatesVar.getDataType().getPrimitiveClassType(), ngatesVar.getShape());
        final IndexIterator ngatesIter = ngatesData.getIndexIterator();
        IndexIterator ii = timeData.getIndexIterator();
        while (ii.hasNext()) {
            ii.setIntNext(-9999);
        }
        ii = elevData.getIndexIterator();
        while (ii.hasNext()) {
            ii.setFloatNext(Float.NaN);
        }
        ii = aziData.getIndexIterator();
        while (ii.hasNext()) {
            ii.setFloatNext(Float.NaN);
        }
        int last_msecs = Integer.MIN_VALUE;
        for (int nscans = groups.size(), scan = 0; scan < nscans; ++scan) {
            final List scanGroup = groups.get(scan);
            final int nradials = scanGroup.size();
            Level2Record first = null;
            for (int j = 0; j < nradials; ++j) {
                final Level2Record r = scanGroup.get(j);
                if (first == null) {
                    first = r;
                }
                final int radial = r.radial_num - 1;
                timeData.setInt(timeIndex.set(scan, radial), r.data_msecs);
                elevData.setFloat(elevIndex.set(scan, radial), r.getElevation());
                aziData.setFloat(aziIndex.set(scan, radial), r.getAzimuth());
                if (r.data_msecs < last_msecs) {
                    Nexrad2IOServiceProvider.logger.warn("makeCoordinateData time out of order " + r.data_msecs);
                }
                last_msecs = r.data_msecs;
            }
            nradialsIter.setIntNext(nradials);
            if (first != null) {
                ngatesIter.setIntNext(first.getGateCount(datatype));
            }
        }
        time.setCachedData(timeData, false);
        elev.setCachedData(elevData, false);
        azi.setCachedData(aziData, false);
        nradialsVar.setCachedData(nradialsData, false);
        ngatesVar.setCachedData(ngatesData, false);
    }
    
    public Array readData(final Variable v2, final Section section) throws IOException, InvalidRangeException {
        final Vgroup vgroup = (Vgroup)v2.getSPobject();
        final Range scanRange = section.getRange(0);
        final Range radialRange = section.getRange(1);
        final Range gateRange = section.getRange(2);
        final Array data = Array.factory(v2.getDataType().getPrimitiveClassType(), section.getShape());
        final IndexIterator ii = data.getIndexIterator();
        for (int i = scanRange.first(); i <= scanRange.last(); i += scanRange.stride()) {
            final Level2Record[] mapScan = vgroup.map[i];
            this.readOneScan(mapScan, radialRange, gateRange, vgroup.datatype, ii);
        }
        return data;
    }
    
    private void readOneScan(final Level2Record[] mapScan, final Range radialRange, final Range gateRange, final int datatype, final IndexIterator ii) throws IOException {
        for (int i = radialRange.first(); i <= radialRange.last(); i += radialRange.stride()) {
            final Level2Record r = mapScan[i];
            this.readOneRadial(r, datatype, gateRange, ii);
        }
    }
    
    private void readOneRadial(final Level2Record r, final int datatype, final Range gateRange, final IndexIterator ii) throws IOException {
        if (r == null) {
            for (int i = gateRange.first(); i <= gateRange.last(); i += gateRange.stride()) {
                ii.setByteNext((byte)1);
            }
            return;
        }
        r.readData(this.volScan.raf, datatype, gateRange, ii);
    }
    
    @Override
    public void close() throws IOException {
        this.volScan.raf.close();
    }
    
    public String getFileTypeId() {
        return "NEXRAD-2";
    }
    
    public String getFileTypeDescription() {
        return "NEXRAD Level-II Base Data";
    }
    
    static {
        Nexrad2IOServiceProvider.logger = LoggerFactory.getLogger(Nexrad2IOServiceProvider.class);
    }
    
    private class Vgroup
    {
        Level2Record[][] map;
        int datatype;
        
        Vgroup(final int datatype, final Level2Record[][] map) {
            this.datatype = datatype;
            this.map = map;
        }
    }
}
