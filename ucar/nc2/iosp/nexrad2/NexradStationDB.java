// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.iosp.nexrad2;

import java.util.Iterator;
import ucar.nc2.util.TableParser;
import java.util.StringTokenizer;
import java.util.List;
import org.jdom.Document;
import java.io.InputStream;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import java.util.HashMap;
import java.io.IOException;
import java.util.Map;

public class NexradStationDB
{
    private static boolean showStations;
    private static Map<String, Station> stationTableHash;
    private static Map<String, Station> stationTableHash1;
    
    public static synchronized void init() throws IOException {
        if (NexradStationDB.stationTableHash == null) {
            readStationTableXML();
        }
    }
    
    public static Station get(final String id) {
        return NexradStationDB.stationTableHash.get(id);
    }
    
    public static Station getByIdNumber(final String idn) {
        return NexradStationDB.stationTableHash1.get(idn);
    }
    
    private static void readStationTableXML() throws IOException {
        NexradStationDB.stationTableHash = new HashMap<String, Station>();
        NexradStationDB.stationTableHash1 = new HashMap<String, Station>();
        final ClassLoader cl = Level2VolumeScan.class.getClassLoader();
        final InputStream is = cl.getResourceAsStream("resources/nj22/tables/nexradstns.xml");
        final SAXBuilder saxBuilder = new SAXBuilder();
        Document doc;
        try {
            doc = saxBuilder.build(is);
        }
        catch (JDOMException e) {
            throw new IOException(e.getMessage());
        }
        final Element root = doc.getRootElement();
        final List children = root.getChildren("station");
        for (int i = 0; i < children.size(); ++i) {
            final Element sElem = children.get(i);
            final String idn = sElem.getAttributeValue("idn");
            final String id = sElem.getAttributeValue("id");
            final String name = sElem.getAttributeValue("name");
            final String st = sElem.getAttributeValue("st");
            final String co = sElem.getAttributeValue("co");
            final String lat = sElem.getAttributeValue("lat");
            final String lon = sElem.getAttributeValue("lon");
            final String elev = sElem.getAttributeValue("elev");
            final Station s = new Station();
            s.id = "K" + id;
            s.name = name + "," + st + "," + co;
            s.lat = parseDegree(lat);
            s.lon = parseDegree(lon);
            s.elev = Double.parseDouble(elev);
            NexradStationDB.stationTableHash.put(s.id, s);
            NexradStationDB.stationTableHash1.put(idn, s);
            if (NexradStationDB.showStations) {
                System.out.println(" station= " + s);
            }
        }
    }
    
    private static double parseDegree(final String s) {
        final StringTokenizer stoke = new StringTokenizer(s, ":");
        final String degS = stoke.nextToken();
        final String minS = stoke.nextToken();
        final String secS = stoke.nextToken();
        try {
            final double deg = Double.parseDouble(degS);
            final double min = Double.parseDouble(minS);
            final double sec = Double.parseDouble(secS);
            if (deg < 0.0) {
                return deg - min / 60.0 - sec / 3600.0;
            }
            return deg + min / 60.0 + sec / 3600.0;
        }
        catch (NumberFormatException e) {
            e.printStackTrace();
            return 0.0;
        }
    }
    
    private static void readStationTable() throws IOException {
        NexradStationDB.stationTableHash = new HashMap<String, Station>();
        final ClassLoader cl = Level2VolumeScan.class.getClassLoader();
        final InputStream is = cl.getResourceAsStream("resources/nj22/tables/nexrad.tbl");
        final List<TableParser.Record> recs = TableParser.readTable(is, "3,15,46, 54,60d,67d,73d", 50000);
        for (final TableParser.Record record : recs) {
            final Station s = new Station();
            s.id = "K" + record.get(0);
            s.name = record.get(2) + " " + record.get(3);
            s.lat = (double)record.get(4) * 0.01;
            s.lon = (double)record.get(5) * 0.01;
            s.elev = (double)record.get(6);
            NexradStationDB.stationTableHash.put(s.id, s);
            if (NexradStationDB.showStations) {
                System.out.println(" station= " + s);
            }
        }
    }
    
    static {
        NexradStationDB.showStations = false;
        NexradStationDB.stationTableHash = null;
        NexradStationDB.stationTableHash1 = null;
    }
    
    public static class Station
    {
        public String id;
        public String name;
        public double lat;
        public double lon;
        public double elev;
        
        @Override
        public String toString() {
            return this.id + " <" + this.name + ">   " + this.lat + " " + this.lon + " " + this.elev;
        }
    }
}
