// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.iosp.nexrad2;

import org.slf4j.LoggerFactory;
import ucar.nc2.NetcdfFile;
import ucar.unidata.io.bzip2.BZip2ReadException;
import java.io.InputStream;
import java.io.ByteArrayInputStream;
import java.io.EOFException;
import ucar.unidata.io.bzip2.CBZip2InputStream;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import java.util.Comparator;
import java.util.Collections;
import java.util.Collection;
import java.util.HashMap;
import java.io.IOException;
import java.nio.channels.FileLock;
import java.io.File;
import java.nio.channels.OverlappingFileLockException;
import java.io.FileInputStream;
import ucar.nc2.util.DiskCache;
import ucar.nc2.util.CancelTask;
import java.util.ArrayList;
import java.util.List;
import ucar.unidata.io.RandomAccessFile;
import org.slf4j.Logger;

public class Level2VolumeScan
{
    public static final String ARCHIVE2 = "ARCHIVE2";
    public static final String AR2V0001 = "AR2V0001";
    public static final String AR2V0002 = "AR2V0002";
    public static final String AR2V0003 = "AR2V0003";
    public static final String AR2V0004 = "AR2V0004";
    public static final String AR2V0006 = "AR2V0006";
    private static Logger log;
    RandomAccessFile raf;
    private String dataFormat;
    private int title_julianDay;
    private int title_msecs;
    private String stationId;
    private NexradStationDB.Station station;
    private Level2Record first;
    private Level2Record last;
    private int vcp;
    private int max_radials;
    private int min_radials;
    private int max_radials_hr;
    private int min_radials_hr;
    private int dopplarResolution;
    private boolean hasDifferentDopplarResolutions;
    private boolean hasHighResolutionData;
    private boolean hasHighResolutionREF;
    private boolean hasHighResolutionVEL;
    private boolean hasHighResolutionSW;
    private boolean hasHighResolutionZDR;
    private boolean hasHighResolutionPHI;
    private boolean hasHighResolutionRHO;
    private List<List<Level2Record>> reflectivityGroups;
    private List<List<Level2Record>> dopplerGroups;
    private List<List<Level2Record>> reflectivityHighResGroups;
    private List<List<Level2Record>> velocityHighResGroups;
    private List<List<Level2Record>> spectrumHighResGroups;
    private ArrayList diffReflectHighResGroups;
    private ArrayList diffPhaseHighResGroups;
    private ArrayList coefficientHighResGroups;
    private boolean showMessages;
    private boolean showData;
    private boolean debugScans;
    private boolean debugGroups2;
    private boolean debugRadials;
    private boolean debugStats;
    private boolean runCheck;
    private int MAX_RADIAL;
    private int[] radial;
    
    Level2VolumeScan(final RandomAccessFile orgRaf, final CancelTask cancelTask) throws IOException {
        this.dataFormat = null;
        this.vcp = 0;
        this.max_radials = 0;
        this.min_radials = Integer.MAX_VALUE;
        this.max_radials_hr = 0;
        this.min_radials_hr = Integer.MAX_VALUE;
        this.showMessages = false;
        this.showData = false;
        this.debugScans = false;
        this.debugGroups2 = false;
        this.debugRadials = false;
        this.debugStats = false;
        this.runCheck = false;
        this.MAX_RADIAL = 721;
        this.radial = new int[this.MAX_RADIAL];
        this.raf = orgRaf;
        if (Level2VolumeScan.log.isDebugEnabled()) {
            Level2VolumeScan.log.debug("Level2VolumeScan on " + this.raf.getLocation());
        }
        this.raf.seek(0L);
        this.raf.order(0);
        this.dataFormat = this.raf.readString(8);
        this.raf.skipBytes(1);
        final String volumeNo = this.raf.readString(3);
        this.title_julianDay = this.raf.readInt();
        this.title_msecs = this.raf.readInt();
        this.stationId = this.raf.readString(4).trim();
        if (Level2VolumeScan.log.isDebugEnabled()) {
            Level2VolumeScan.log.debug(" dataFormat= " + this.dataFormat + " stationId= " + this.stationId);
        }
        if (this.stationId.length() == 0) {
            this.stationId = null;
        }
        if (this.stationId != null) {
            if (!this.stationId.startsWith("K") && this.stationId.length() == 4) {
                final String _stationId = "K" + this.stationId;
                this.station = NexradStationDB.get(_stationId);
            }
            else {
                this.station = NexradStationDB.get(this.stationId);
            }
        }
        if (this.dataFormat.equals("AR2V0001") || this.dataFormat.equals("AR2V0003") || this.dataFormat.equals("AR2V0004") || this.dataFormat.equals("AR2V0006")) {
            this.raf.skipBytes(4);
            final String BZ = this.raf.readString(2);
            if (BZ.equals("BZ")) {
                final File uncompressedFile = DiskCache.getFileStandardPolicy(this.raf.getLocation() + ".uncompress");
                RandomAccessFile uraf;
                if (uncompressedFile.exists() && uncompressedFile.length() > 0L) {
                    FileInputStream fstream = null;
                    FileLock lock = null;
                    try {
                        fstream = new FileInputStream(uncompressedFile);
                    Label_0543:
                        while (true) {
                            try {
                                lock = fstream.getChannel().lock(0L, 1L, true);
                            }
                            catch (OverlappingFileLockException oe) {
                                try {
                                    Thread.sleep(100L);
                                }
                                catch (InterruptedException e1) {
                                    break Label_0543;
                                }
                                continue;
                            }
                            break;
                        }
                    }
                    finally {
                        if (lock != null) {
                            lock.release();
                        }
                        if (fstream != null) {
                            fstream.close();
                        }
                    }
                    uraf = new RandomAccessFile(uncompressedFile.getPath(), "r");
                }
                else {
                    uraf = this.uncompress(this.raf, uncompressedFile.getPath());
                    if (Level2VolumeScan.log.isDebugEnabled()) {
                        Level2VolumeScan.log.debug("made uncompressed file= " + uncompressedFile.getPath());
                    }
                }
                this.raf.close();
                (this.raf = uraf).order(0);
            }
            this.raf.seek(24L);
        }
        final List<Level2Record> reflectivity = new ArrayList<Level2Record>();
        final List<Level2Record> doppler = new ArrayList<Level2Record>();
        final List<Level2Record> highReflectivity = new ArrayList<Level2Record>();
        final List<Level2Record> highVelocity = new ArrayList<Level2Record>();
        final List<Level2Record> highSpectrum = new ArrayList<Level2Record>();
        final List<Level2Record> highDiffReflectivity = new ArrayList<Level2Record>();
        final List<Level2Record> highDiffPhase = new ArrayList<Level2Record>();
        final List<Level2Record> highCorreCoefficient = new ArrayList<Level2Record>();
        long message_offset31 = 0L;
        int recno = 0;
        while (true) {
            final Level2Record r = Level2Record.factory(this.raf, recno++, message_offset31);
            if (r == null) {
                if (this.debugRadials) {
                    System.out.println(" reflect ok= " + reflectivity.size() + " doppler ok= " + doppler.size());
                }
                if (highReflectivity.size() == 0) {
                    this.reflectivityGroups = (List<List<Level2Record>>)this.sortScans("reflect", reflectivity, 600);
                    this.dopplerGroups = (List<List<Level2Record>>)this.sortScans("doppler", doppler, 600);
                }
                if (highReflectivity.size() > 0) {
                    this.reflectivityHighResGroups = (List<List<Level2Record>>)this.sortScans("reflect_HR", highReflectivity, 720);
                }
                if (highVelocity.size() > 0) {
                    this.velocityHighResGroups = (List<List<Level2Record>>)this.sortScans("velocity_HR", highVelocity, 720);
                }
                if (highSpectrum.size() > 0) {
                    this.spectrumHighResGroups = (List<List<Level2Record>>)this.sortScans("spectrum_HR", highSpectrum, 720);
                }
                if (highDiffReflectivity.size() > 0) {
                    this.diffReflectHighResGroups = this.sortScans("diffReflect_HR", highDiffReflectivity, 720);
                }
                if (highDiffPhase.size() > 0) {
                    this.diffPhaseHighResGroups = this.sortScans("diffPhase_HR", highDiffPhase, 720);
                }
                if (highCorreCoefficient.size() > 0) {
                    this.coefficientHighResGroups = this.sortScans("coefficient_HR", highCorreCoefficient, 720);
                }
                return;
            }
            if (this.showData) {
                r.dump2(System.out);
            }
            if (r.message_type == 31) {
                message_offset31 += r.message_size * 2 + 12 - 2432;
            }
            if (r.message_type != 1 && r.message_type != 31) {
                if (!this.showMessages) {
                    continue;
                }
                r.dumpMessage(System.out);
            }
            else {
                if (this.vcp == 0) {
                    this.vcp = r.vcp;
                }
                if (this.first == null) {
                    this.first = r;
                }
                this.last = r;
                if (this.runCheck && !r.checkOk()) {
                    continue;
                }
                if (r.hasReflectData) {
                    reflectivity.add(r);
                }
                if (r.hasDopplerData) {
                    doppler.add(r);
                }
                if (r.message_type == 31) {
                    if (r.hasHighResREFData) {
                        highReflectivity.add(r);
                    }
                    if (r.hasHighResVELData) {
                        highVelocity.add(r);
                    }
                    if (r.hasHighResSWData) {
                        highSpectrum.add(r);
                    }
                    if (r.hasHighResZDRData) {
                        highDiffReflectivity.add(r);
                    }
                    if (r.hasHighResPHIData) {
                        highDiffPhase.add(r);
                    }
                    if (r.hasHighResRHOData) {
                        highCorreCoefficient.add(r);
                    }
                }
                if (cancelTask != null && cancelTask.isCancel()) {
                    return;
                }
                continue;
            }
        }
    }
    
    private ArrayList sortScans(final String name, final List<Level2Record> scans, final int siz) {
        final Map<Short, List<Level2Record>> groupHash = new HashMap<Short, List<Level2Record>>(siz);
        for (final Level2Record record : scans) {
            List<Level2Record> group = groupHash.get(record.elevation_num);
            if (null == group) {
                group = new ArrayList<Level2Record>();
                groupHash.put(record.elevation_num, group);
            }
            group.add(record);
        }
        final ArrayList groups = new ArrayList((Collection<? extends E>)groupHash.values());
        Collections.sort((List<Object>)groups, (Comparator<? super Object>)new GroupComparator());
        for (int i = 0; i < groups.size(); ++i) {
            final ArrayList group2 = groups.get(i);
            final Level2Record r = group2.get(0);
            if (this.runCheck) {
                this.testScan(name, group2);
            }
            if (r.getGateCount(5) > 500 || r.getGateCount(6) > 1000) {
                if (group2.size() <= 360) {
                    this.max_radials = Math.max(this.max_radials, group2.size());
                    this.min_radials = Math.min(this.min_radials, group2.size());
                }
                else {
                    this.max_radials_hr = Math.max(this.max_radials_hr, group2.size());
                    this.min_radials_hr = Math.min(this.min_radials_hr, group2.size());
                }
            }
            else {
                this.max_radials = Math.max(this.max_radials, group2.size());
                this.min_radials = Math.min(this.min_radials, group2.size());
            }
        }
        if (this.debugRadials) {
            System.out.println(name + " min_radials= " + this.min_radials + " max_radials= " + this.max_radials);
            for (int i = 0; i < groups.size(); ++i) {
                final ArrayList group2 = groups.get(i);
                Level2Record lastr = group2.get(0);
                for (int j = 1; j < group2.size(); ++j) {
                    final Level2Record r2 = group2.get(j);
                    if (r2.data_msecs < lastr.data_msecs) {
                        System.out.println(" out of order " + j);
                    }
                    lastr = r2;
                }
            }
        }
        this.testVariable(name, groups);
        if (this.debugScans) {
            System.out.println("-----------------------------");
        }
        return groups;
    }
    
    public int getMaxRadials(final int r) {
        if (r == 0) {
            return this.max_radials;
        }
        if (r == 1) {
            return this.max_radials_hr;
        }
        return 0;
    }
    
    public int getMinRadials(final int r) {
        if (r == 0) {
            return this.min_radials;
        }
        if (r == 1) {
            return this.min_radials_hr;
        }
        return 0;
    }
    
    public int getDopplarResolution() {
        return this.dopplarResolution;
    }
    
    public boolean hasDifferentDopplarResolutions() {
        return this.hasDifferentDopplarResolutions;
    }
    
    public boolean hasHighResolutions(final int dt) {
        if (dt == 0) {
            return this.hasHighResolutionData;
        }
        if (dt == 1) {
            return this.hasHighResolutionREF;
        }
        if (dt == 2) {
            return this.hasHighResolutionVEL;
        }
        if (dt == 3) {
            return this.hasHighResolutionSW;
        }
        if (dt == 4) {
            return this.hasHighResolutionZDR;
        }
        if (dt == 5) {
            return this.hasHighResolutionPHI;
        }
        return dt == 6 && this.hasHighResolutionRHO;
    }
    
    private boolean testScan(final String name, final ArrayList group) {
        final int datatype = name.equals("reflect") ? 1 : 2;
        final Level2Record first = group.get(0);
        final int n = group.size();
        if (this.debugScans) {
            final boolean hasBoth = first.hasDopplerData && first.hasReflectData;
            System.out.println(name + " " + first + " has " + n + " radials resolution= " + first.resolution + " has both = " + hasBoth);
        }
        boolean ok = true;
        double sum = 0.0;
        double sum2 = 0.0;
        for (int i = 0; i < this.MAX_RADIAL; ++i) {
            this.radial[i] = 0;
        }
        for (int i = 0; i < group.size(); ++i) {
            final Level2Record r = group.get(i);
            if (r.getGateSize(datatype) != first.getGateSize(datatype)) {
                Level2VolumeScan.log.warn(this.raf.getLocation() + " different gate size (" + r.getGateSize(datatype) + ") in record " + name + " " + r);
                ok = false;
            }
            if (r.getGateStart(datatype) != first.getGateStart(datatype)) {
                Level2VolumeScan.log.warn(this.raf.getLocation() + " different gate start (" + r.getGateStart(datatype) + ") in record " + name + " " + r);
                ok = false;
            }
            if (r.resolution != first.resolution) {
                Level2VolumeScan.log.warn(this.raf.getLocation() + " different resolution (" + r.resolution + ") in record " + name + " " + r);
                ok = false;
            }
            if (r.radial_num < 0 || r.radial_num >= this.MAX_RADIAL) {
                Level2VolumeScan.log.info(this.raf.getLocation() + " radial out of range= " + r.radial_num + " in record " + name + " " + r);
            }
            else {
                if (this.radial[r.radial_num] > 0) {
                    Level2VolumeScan.log.warn(this.raf.getLocation() + " duplicate radial = " + r.radial_num + " in record " + name + " " + r);
                    ok = false;
                }
                this.radial[r.radial_num] = r.recno + 1;
                sum += r.getElevation();
                sum2 += r.getElevation() * r.getElevation();
            }
        }
        int i = 1;
        while (i < this.radial.length) {
            if (0 == this.radial[i]) {
                if (n != i - 1) {
                    Level2VolumeScan.log.warn(" missing radial(s)");
                    ok = false;
                    break;
                }
                break;
            }
            else {
                ++i;
            }
        }
        final double avg = sum / n;
        final double sd = Math.sqrt((n * sum2 - sum * sum) / (n * (n - 1)));
        return ok;
    }
    
    private boolean testVariable(final String name, final List scans) {
        final int datatype = name.equals("reflect") ? 1 : 2;
        if (scans.size() == 0) {
            Level2VolumeScan.log.warn(" No data for = " + name);
            return false;
        }
        boolean ok = true;
        final List firstScan = scans.get(0);
        final Level2Record firstRecord = firstScan.get(0);
        this.dopplarResolution = firstRecord.resolution;
        if (this.debugGroups2) {
            System.out.println("Group " + Level2Record.getDatatypeName(datatype) + " ngates = " + firstRecord.getGateCount(datatype) + " start = " + firstRecord.getGateStart(datatype) + " size = " + firstRecord.getGateSize(datatype));
        }
        for (int i = 1; i < scans.size(); ++i) {
            final List scan = scans.get(i);
            final Level2Record record = scan.get(0);
            if (datatype == 2 && record.resolution != firstRecord.resolution) {
                Level2VolumeScan.log.warn(name + " scan " + i + " diff resolutions = " + record.resolution + ", " + firstRecord.resolution + " elev= " + record.elevation_num + " " + record.getElevation());
                ok = false;
                this.hasDifferentDopplarResolutions = true;
            }
            if (record.getGateSize(datatype) != firstRecord.getGateSize(datatype)) {
                Level2VolumeScan.log.warn(name + " scan " + i + " diff gates size = " + record.getGateSize(datatype) + " " + firstRecord.getGateSize(datatype) + " elev= " + record.elevation_num + " " + record.getElevation());
                ok = false;
            }
            else if (this.debugGroups2) {
                System.out.println(" ok gates size elev= " + record.elevation_num + " " + record.getElevation());
            }
            if (record.getGateStart(datatype) != firstRecord.getGateStart(datatype)) {
                Level2VolumeScan.log.warn(name + " scan " + i + " diff gates start = " + record.getGateStart(datatype) + " " + firstRecord.getGateStart(datatype) + " elev= " + record.elevation_num + " " + record.getElevation());
                ok = false;
            }
            else if (this.debugGroups2) {
                System.out.println(" ok gates start elev= " + record.elevation_num + " " + record.getElevation());
            }
            if (record.message_type == 31) {
                this.hasHighResolutionData = true;
                if (record.hasHighResREFData) {
                    this.hasHighResolutionREF = true;
                }
                if (record.hasHighResVELData) {
                    this.hasHighResolutionVEL = true;
                }
                if (record.hasHighResSWData) {
                    this.hasHighResolutionSW = true;
                }
                if (record.hasHighResZDRData) {
                    this.hasHighResolutionZDR = true;
                }
                if (record.hasHighResPHIData) {
                    this.hasHighResolutionPHI = true;
                }
                if (record.hasHighResRHOData) {
                    this.hasHighResolutionRHO = true;
                }
            }
        }
        return ok;
    }
    
    public List getReflectivityGroups() {
        return this.reflectivityGroups;
    }
    
    public List getVelocityGroups() {
        return this.dopplerGroups;
    }
    
    public List getHighResVelocityGroups() {
        return this.velocityHighResGroups;
    }
    
    public List getHighResReflectivityGroups() {
        return this.reflectivityHighResGroups;
    }
    
    public List getHighResSpectrumGroups() {
        return this.spectrumHighResGroups;
    }
    
    public List getHighResDiffReflectGroups() {
        return this.diffReflectHighResGroups;
    }
    
    public List getHighResDiffPhaseGroups() {
        return this.diffPhaseHighResGroups;
    }
    
    public List getHighResCoeffocientGroups() {
        return this.coefficientHighResGroups;
    }
    
    public String getDataFormat() {
        return this.dataFormat;
    }
    
    public int getTitleJulianDays() {
        return this.title_julianDay;
    }
    
    public int getTitleMsecs() {
        return this.title_msecs;
    }
    
    public int getVCP() {
        return this.vcp;
    }
    
    public String getStationId() {
        return this.stationId;
    }
    
    public String getStationName() {
        return (this.station == null) ? "unknown" : this.station.name;
    }
    
    public double getStationLatitude() {
        return (this.station == null) ? 0.0 : this.station.lat;
    }
    
    public double getStationLongitude() {
        return (this.station == null) ? 0.0 : this.station.lon;
    }
    
    public double getStationElevation() {
        return (this.station == null) ? 0.0 : this.station.elev;
    }
    
    public Date getStartDate() {
        return this.first.getDate();
    }
    
    public Date getEndDate() {
        return this.last.getDate();
    }
    
    private RandomAccessFile uncompress(final RandomAccessFile inputRaf, final String ufilename) throws IOException {
        RandomAccessFile outputRaf = new RandomAccessFile(ufilename, "rw");
        FileLock lock = null;
        while (true) {
            try {
                lock = outputRaf.getRandomAccessFile().getChannel().lock(0L, 1L, false);
            }
            catch (OverlappingFileLockException oe) {
                try {
                    Thread.sleep(100L);
                }
                catch (InterruptedException ex) {}
                continue;
            }
            break;
        }
        try {
            inputRaf.seek(0L);
            final byte[] header = new byte[24];
            inputRaf.read(header);
            outputRaf.write(header);
            boolean eof = false;
            final byte[] ubuff = new byte[40000];
            byte[] obuff = new byte[40000];
            try {
                final CBZip2InputStream cbzip2 = new CBZip2InputStream();
                while (!eof) {
                    int numCompBytes;
                    try {
                        numCompBytes = inputRaf.readInt();
                        if (numCompBytes == -1) {
                            if (Level2VolumeScan.log.isDebugEnabled()) {
                                Level2VolumeScan.log.debug("  done: numCompBytes=-1 ");
                            }
                            break;
                        }
                    }
                    catch (EOFException ee) {
                        Level2VolumeScan.log.warn("  got EOFException ");
                        break;
                    }
                    if (Level2VolumeScan.log.isDebugEnabled()) {
                        Level2VolumeScan.log.debug("reading compressed bytes " + numCompBytes + " input starts at " + inputRaf.getFilePointer() + "; output starts at " + outputRaf.getFilePointer());
                    }
                    if (numCompBytes < 0) {
                        if (Level2VolumeScan.log.isDebugEnabled()) {
                            Level2VolumeScan.log.debug("last block?" + numCompBytes);
                        }
                        numCompBytes = -numCompBytes;
                        eof = true;
                    }
                    final byte[] buf = new byte[numCompBytes];
                    inputRaf.readFully(buf);
                    final ByteArrayInputStream bis = new ByteArrayInputStream(buf, 2, numCompBytes - 2);
                    cbzip2.setStream(bis);
                    int total = 0;
                    try {
                        int nread;
                        while ((nread = cbzip2.read(ubuff)) != -1) {
                            if (total + nread > obuff.length) {
                                final byte[] temp = obuff;
                                obuff = new byte[temp.length * 2];
                                System.arraycopy(temp, 0, obuff, 0, temp.length);
                            }
                            System.arraycopy(ubuff, 0, obuff, total, nread);
                            total += nread;
                        }
                        if (obuff.length >= 0) {
                            outputRaf.write(obuff, 0, total);
                        }
                    }
                    catch (BZip2ReadException ioe) {
                        Level2VolumeScan.log.warn("Nexrad2IOSP.uncompress ", ioe);
                    }
                    final float nrecords = (float)(total / 2432.0);
                    if (Level2VolumeScan.log.isDebugEnabled()) {
                        Level2VolumeScan.log.debug("  unpacked " + total + " num bytes " + nrecords + " records; ouput ends at " + outputRaf.getFilePointer());
                    }
                }
            }
            catch (Exception e) {
                if (outputRaf != null) {
                    outputRaf.close();
                }
                outputRaf = null;
                final File ufile = new File(ufilename);
                if (ufile.exists() && !ufile.delete()) {
                    Level2VolumeScan.log.warn("failed to delete uncompressed file (IOException)" + ufilename);
                }
                if (e instanceof IOException) {
                    throw (IOException)e;
                }
                throw new RuntimeException(e);
            }
        }
        finally {
            if (null != outputRaf) {
                outputRaf.flush();
            }
            if (lock != null) {
                lock.release();
            }
        }
        return outputRaf;
    }
    
    static void bdiff(final String filename) throws IOException {
        final InputStream in1 = new FileInputStream(filename + ".tmp");
        final InputStream in2 = new FileInputStream(filename + ".tmp2");
        int count = 0;
        int bad = 0;
        while (true) {
            final int b1 = in1.read();
            final int b2 = in2.read();
            if (b1 < 0) {
                break;
            }
            if (b2 < 0) {
                break;
            }
            if (b1 != b2) {
                System.out.println(count + " in1=" + b1 + " in2= " + b2);
                if (++bad > 130) {
                    break;
                }
            }
            ++count;
        }
        System.out.println("total read = " + count);
    }
    
    public static long testValid(final String ufilename) throws IOException {
        boolean lookForHeader = false;
        final RandomAccessFile raf = new RandomAccessFile(ufilename, "r");
        raf.order(0);
        raf.seek(0L);
        final byte[] b = new byte[8];
        raf.read(b);
        String test = new String(b);
        if (test.equals("ARCHIVE2") || test.equals("AR2V0001")) {
            System.out.println("--Good header= " + test);
            raf.seek(24L);
        }
        else {
            System.out.println("--No header ");
            lookForHeader = true;
            raf.seek(0L);
        }
        boolean eof = false;
        try {
            while (!eof) {
                if (lookForHeader) {
                    raf.read(b);
                    test = new String(b);
                    if (test.equals("ARCHIVE2") || test.equals("AR2V0001")) {
                        System.out.println("  found header= " + test);
                        raf.skipBytes(16);
                        lookForHeader = false;
                    }
                    else {
                        raf.skipBytes(-8);
                    }
                }
                int numCompBytes;
                try {
                    numCompBytes = raf.readInt();
                    if (numCompBytes == -1) {
                        System.out.println("\n--done: numCompBytes=-1 ");
                        break;
                    }
                }
                catch (EOFException ee) {
                    System.out.println("\n--got EOFException ");
                    break;
                }
                System.out.print(" " + numCompBytes + ",");
                if (numCompBytes < 0) {
                    System.out.println("\n--last block " + numCompBytes);
                    numCompBytes = -numCompBytes;
                    if (!lookForHeader) {
                        eof = true;
                    }
                }
                raf.skipBytes(numCompBytes);
            }
        }
        catch (EOFException e) {
            e.printStackTrace();
        }
        return raf.getFilePointer();
    }
    
    public static void main2(final String[] args) throws IOException {
        final File testDir = new File("C:/data/bad/radar2/");
        final File[] files = testDir.listFiles();
        for (int i = 0; i < files.length; ++i) {
            final File file = files[i];
            if (file.getPath().endsWith(".ar2v")) {
                System.out.println(file.getPath() + " " + file.length());
                final long pos = testValid(file.getPath());
                if (pos == file.length()) {
                    System.out.println("OK");
                    try {
                        NetcdfFile.open(file.getPath());
                    }
                    catch (Throwable t) {
                        System.out.println("ERROR=  " + t);
                    }
                }
                else {
                    System.out.println("NOT pos=" + pos);
                }
                System.out.println();
            }
        }
    }
    
    public static void main(final String[] args) throws IOException {
        NexradStationDB.init();
        final RandomAccessFile raf = new RandomAccessFile("/upc/share/testdata/radar/nexrad/level2/Level2_KFTG_20060818_1814.ar2v.uncompress.missingradials", "r");
        new Level2VolumeScan(raf, null);
    }
    
    static {
        Level2VolumeScan.log = LoggerFactory.getLogger(Level2VolumeScan.class);
    }
    
    private class GroupComparator implements Comparator<List<Level2Record>>
    {
        public int compare(final List<Level2Record> group1, final List<Level2Record> group2) {
            final Level2Record record1 = group1.get(0);
            final Level2Record record2 = group2.get(0);
            return record1.elevation_num - record2.elevation_num;
        }
    }
}
