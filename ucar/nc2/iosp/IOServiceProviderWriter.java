// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.iosp;

import ucar.nc2.Attribute;
import ucar.ma2.InvalidRangeException;
import ucar.ma2.Array;
import ucar.ma2.Section;
import ucar.nc2.Variable;
import java.io.IOException;
import ucar.nc2.NetcdfFile;

public interface IOServiceProviderWriter extends IOServiceProvider
{
    void create(final String p0, final NetcdfFile p1, final int p2, final long p3, final boolean p4) throws IOException;
    
    void setFill(final boolean p0);
    
    void writeData(final Variable p0, final Section p1, final Array p2) throws IOException, InvalidRangeException;
    
    boolean rewriteHeader(final boolean p0) throws IOException;
    
    void updateAttribute(final Variable p0, final Attribute p1) throws IOException;
    
    void flush() throws IOException;
}
