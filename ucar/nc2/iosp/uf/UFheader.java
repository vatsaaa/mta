// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.iosp.uf;

import java.util.Date;
import java.util.Comparator;
import java.util.Collections;
import java.util.Iterator;
import java.util.Set;
import java.util.List;
import java.util.ArrayList;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.HashMap;
import ucar.nc2.NetcdfFile;
import ucar.unidata.io.RandomAccessFile;

public class UFheader
{
    RandomAccessFile raf;
    NetcdfFile ncfile;
    static final boolean littleEndianData = true;
    String dataFormat;
    Ray firstRay;
    HashMap variableGroup;
    private int max_radials;
    private int min_radials;
    
    public UFheader() {
        this.dataFormat = "UNIVERSALFORMAT";
        this.firstRay = null;
        this.max_radials = 0;
        this.min_radials = Integer.MAX_VALUE;
    }
    
    public boolean isValidFile(final RandomAccessFile raf) {
        try {
            raf.seek(0L);
            raf.order(0);
            final byte[] b6 = new byte[6];
            final byte[] b7 = new byte[4];
            raf.read(b6, 0, 6);
            final String ufStr = new String(b6, 4, 2);
            if (!ufStr.equals("UF")) {
                return false;
            }
            raf.seek(0L);
            raf.read(b7, 0, 4);
            final int rsize = bytesToInt(b7, false);
            final byte[] buffer = new byte[rsize];
            final long offset = raf.getFilePointer();
            raf.read(buffer, 0, rsize);
            raf.read(b7, 0, 4);
            final int endPoint = bytesToInt(b7, false);
            if (endPoint != rsize) {
                return false;
            }
            final ByteBuffer bos = ByteBuffer.wrap(buffer);
            this.firstRay = new Ray(bos, rsize, offset);
        }
        catch (IOException e) {
            return false;
        }
        return true;
    }
    
    void read(final RandomAccessFile raf, final NetcdfFile ncfile) throws IOException {
        this.raf = raf;
        this.ncfile = ncfile;
        this.variableGroup = new HashMap();
        raf.seek(0L);
        raf.order(0);
        int rayNumber = 0;
        while (!raf.isAtEndOfFile()) {
            final byte[] b4 = new byte[4];
            raf.read(b4, 0, 4);
            final int rsize = bytesToInt(b4, false);
            final byte[] buffer = new byte[rsize];
            final long offset = raf.getFilePointer();
            raf.read(buffer, 0, rsize);
            raf.read(b4, 0, 4);
            final int endPoint = bytesToInt(b4, false);
            if (endPoint == rsize) {
                if (rsize == 0) {
                    continue;
                }
                final ByteBuffer bos = ByteBuffer.wrap(buffer);
                final Ray r = new Ray(bos, rsize, offset);
                if (this.firstRay == null) {
                    this.firstRay = r;
                }
                ++rayNumber;
                final HashMap rayMap = r.field_header_map;
                final Set kSet = rayMap.keySet();
                for (final String ab : kSet) {
                    ArrayList group = this.variableGroup.get(ab);
                    if (null == group) {
                        group = new ArrayList();
                        this.variableGroup.put(ab, group);
                    }
                    group.add(r);
                }
            }
        }
        final Set vSet = this.variableGroup.keySet();
        for (final String key : vSet) {
            final ArrayList group2 = this.variableGroup.get(key);
            final ArrayList sgroup = this.sortScans(key, group2);
            this.variableGroup.put(key, sgroup);
        }
    }
    
    private ArrayList sortScans(final String name, final List rays) {
        final HashMap groupHash = new HashMap(600);
        for (int i = 0; i < rays.size(); ++i) {
            final Ray r = rays.get(i);
            final Integer groupNo = new Integer(r.uf_header2.sweepNumber);
            ArrayList group = groupHash.get(groupNo);
            if (null == group) {
                group = new ArrayList();
                groupHash.put(groupNo, group);
            }
            group.add(r);
        }
        final ArrayList groups = new ArrayList(groupHash.values());
        Collections.sort((List<Object>)groups, new GroupComparator());
        for (int j = 0; j < groups.size(); ++j) {
            final ArrayList group2 = groups.get(j);
            this.max_radials = Math.max(this.max_radials, group2.size());
            this.min_radials = Math.min(this.min_radials, group2.size());
        }
        return groups;
    }
    
    public float getMeanElevation(final String key, final int eNum) {
        final ArrayList gp = (ArrayList)this.getGroup(key);
        final float meanEle = this.getMeanElevation(gp);
        return meanEle;
    }
    
    public float getMeanElevation(final ArrayList<Ray> gList) {
        float sum = 0.0f;
        int size = 0;
        for (final Ray r : gList) {
            sum += r.getElevation();
            ++size;
        }
        return sum / size;
    }
    
    public List getGroup(final String key) {
        return this.variableGroup.get(key);
    }
    
    public int getMaxRadials() {
        return this.max_radials;
    }
    
    public String getDataFormat() {
        return this.dataFormat;
    }
    
    public Date getStartDate() {
        return this.firstRay.getDate();
    }
    
    public Date getEndDate() {
        return this.firstRay.getDate();
    }
    
    public float getHorizontalBeamWidth(final String ab) {
        return this.firstRay.getHorizontalBeamWidth(ab);
    }
    
    public String getStationId() {
        return this.firstRay.uf_header2.siteName;
    }
    
    public Short getSweepMode() {
        return this.firstRay.uf_header2.sweepMode;
    }
    
    public float getStationLatitude() {
        return this.firstRay.getLatitude();
    }
    
    public float getStationLongitude() {
        return this.firstRay.getLongtitude();
    }
    
    public float getStationElevation() {
        return this.firstRay.getElevation();
    }
    
    public short getMissingData() {
        return this.firstRay.getMissingData();
    }
    
    protected short getShort(final byte[] bytes, final int offset) {
        final int ndx0 = offset + 1;
        final int ndx2 = offset + 0;
        return (short)(bytes[ndx0] << 8 | (bytes[ndx2] & 0xFF));
    }
    
    public static int bytesToShort(final byte a, final byte b, final boolean swapBytes) {
        if (swapBytes) {
            return (a & 0xFF) + (b << 8);
        }
        return (a << 8) + (b & 0xFF);
    }
    
    public static int bytesToInt(final byte[] bytes, final boolean swapBytes) {
        final byte a = bytes[0];
        final byte b = bytes[1];
        final byte c = bytes[2];
        final byte d = bytes[3];
        if (swapBytes) {
            return (a & 0xFF) + ((b & 0xFF) << 8) + ((c & 0xFF) << 16) + ((d & 0xFF) << 24);
        }
        return ((a & 0xFF) << 24) + ((b & 0xFF) << 16) + ((c & 0xFF) << 8) + (d & 0xFF);
    }
    
    private class GroupComparator implements Comparator
    {
        public int compare(final Object o1, final Object o2) {
            final List group1 = (List)o1;
            final List group2 = (List)o2;
            final Ray ray1 = group1.get(0);
            final Ray ray2 = group2.get(0);
            return (ray1.uf_header2.elevation - ray2.uf_header2.elevation >= 13) ? 1 : 0;
        }
    }
}
