// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.iosp.uf;

import java.io.IOException;
import ucar.ma2.IndexIterator;
import ucar.ma2.Range;
import ucar.unidata.io.RandomAccessFile;
import java.util.Date;
import java.util.Calendar;
import java.nio.ByteBuffer;
import java.util.HashMap;

public class Ray
{
    int raySize;
    long rayOffset;
    static final int UF_MANDATORY_HEADER2_LEN = 90;
    static final int UF_FIELD_HEADER2_LEN = 50;
    static final boolean littleEndianData = true;
    boolean debug;
    long data_msecs;
    UF_mandatory_header2 uf_header2;
    UF_optional_header uf_opt_header;
    short numberOfFields;
    short numberOfRecords;
    short numberOfFieldsInRecord;
    HashMap<String, UF_field_header2> field_header_map;
    
    public Ray(final ByteBuffer bos, final int raySize, final long rayOffset) {
        this.debug = false;
        this.data_msecs = 0L;
        this.raySize = raySize;
        this.rayOffset = rayOffset;
        this.field_header_map = new HashMap<String, UF_field_header2>();
        bos.position(0);
        byte[] data = new byte[90];
        bos.get(data);
        this.uf_header2 = new UF_mandatory_header2(data);
        if (this.uf_header2.offset2StartOfOptionalHeader > 0 && this.uf_header2.dataHeaderPosition != this.uf_header2.offset2StartOfOptionalHeader) {
            data = new byte[28];
            bos.get(data);
            this.uf_opt_header = new UF_optional_header(data);
        }
        final int position = this.uf_header2.dataHeaderPosition * 2 - 2;
        bos.position(position);
        this.data_msecs = this.setDateMesc();
        final byte[] b2 = new byte[2];
        bos.get(b2);
        this.numberOfFields = this.getShort(b2, 0);
        bos.get(b2);
        this.numberOfRecords = this.getShort(b2, 0);
        bos.get(b2);
        this.numberOfFieldsInRecord = this.getShort(b2, 0);
        data = new byte[50];
        for (int i = 0; i < this.numberOfFields; ++i) {
            bos.get(b2);
            final String type = new String(b2);
            bos.get(b2);
            final int offs = this.getShort(b2, 0);
            final int position2 = bos.position();
            bos.position(offs * 2 - 2);
            bos.get(data);
            final UF_field_header2 field_header = new UF_field_header2(data);
            bos.position(position2);
            this.field_header_map.put(type, field_header);
        }
    }
    
    public int getRaySize() {
        return this.raySize;
    }
    
    public int getGateCount(final String abbrev) {
        final UF_field_header2 header = this.field_header_map.get(abbrev);
        return header.binCount;
    }
    
    public String getDatatypeName(final String abbrev) {
        if (abbrev.equals("ZN") || abbrev.equals("ZS")) {
            return "Reflectivity";
        }
        if (abbrev.equals("ZF") || abbrev.equals("ZX") || abbrev.equals("DR")) {
            return "Reflectivity";
        }
        if (abbrev.equals("VR") || abbrev.equals("DN") || abbrev.equals("DS") || abbrev.equals("DF") || abbrev.equals("DX")) {
            return "RadialVelocity";
        }
        if (abbrev.equals("VN") || abbrev.equals("VF")) {
            return "CorrectedRadialVelocity";
        }
        if (abbrev.equals("SW") || abbrev.equals("WS") || abbrev.equals("WF") || abbrev.equals("WX") || abbrev.equals("WN")) {
            return "SpectrumWidth";
        }
        if (abbrev.equals("PN") || abbrev.equals("PS") || abbrev.equals("PF") || abbrev.equals("PX")) {
            return "Power";
        }
        if (abbrev.equals("MN") || abbrev.equals("MS") || abbrev.equals("MF") || abbrev.equals("MX")) {
            return "Power";
        }
        if (abbrev.equals("PH")) {
            return "PhiDP";
        }
        if (abbrev.equals("RH")) {
            return "RhoHV";
        }
        if (abbrev.equals("LH")) {
            return "LdrH";
        }
        if (abbrev.equals("KD")) {
            return "KDP";
        }
        if (abbrev.equals("LV")) {
            return "LdrV";
        }
        if (abbrev.equals("DR")) {
            return "ZDR";
        }
        if (abbrev.equals("CZ")) {
            return "CorrecteddBZ";
        }
        if (abbrev.equals("DZ")) {
            return "TotalReflectivity";
        }
        if (abbrev.equals("DR")) {
            return "ZDR";
        }
        return abbrev;
    }
    
    public String getDatatypeUnits(final String abbrev) {
        if (abbrev.equals("CZ") || abbrev.equals("DZ") || abbrev.equals("ZN") || abbrev.equals("ZS")) {
            return "dBz";
        }
        if (abbrev.equals("ZF") || abbrev.equals("ZX")) {
            return "dBz";
        }
        if (abbrev.equals("VR") || abbrev.equals("DN") || abbrev.equals("DS") || abbrev.equals("DF") || abbrev.equals("DX")) {
            return "m/s";
        }
        if (abbrev.equals("VN") || abbrev.equals("VF")) {
            return "m/s";
        }
        if (abbrev.equals("SW") || abbrev.equals("WS") || abbrev.equals("WF") || abbrev.equals("WX") || abbrev.equals("WN")) {
            return "m/s";
        }
        if (abbrev.equals("PN") || abbrev.equals("PS") || abbrev.equals("PF") || abbrev.equals("PX")) {
            return "dBM";
        }
        if (abbrev.equals("MN") || abbrev.equals("MS") || abbrev.equals("MF") || abbrev.equals("MX")) {
            return "dBM";
        }
        return abbrev;
    }
    
    public short getDatatypeRangeFoldingThreshhold(final String abbrev) {
        final UF_field_header2 header = this.field_header_map.get(abbrev);
        return header.thresholdValue;
    }
    
    public float getDatatypeScaleFactor(final String abbrev) {
        final UF_field_header2 header = this.field_header_map.get(abbrev);
        return 1.0f / header.scaleFactor;
    }
    
    public float getDatatypeAddOffset(final String abbrev) {
        return 0.0f;
    }
    
    public int getGateStart(final String abbrev) {
        final UF_field_header2 header = this.field_header_map.get(abbrev);
        return header.startRange;
    }
    
    public int getDataOffset(final String abbrev) {
        final UF_field_header2 header = this.field_header_map.get(abbrev);
        return header.dataOffset;
    }
    
    public int getGateSize(final String abbrev) {
        final UF_field_header2 header = this.field_header_map.get(abbrev);
        return header.binSpacing;
    }
    
    public float getElevation() {
        return this.uf_header2.elevation / 64.0f;
    }
    
    public float getAzimuth() {
        return this.uf_header2.azimuth / 64.0f;
    }
    
    public short getMissingData() {
        return this.uf_header2.missing;
    }
    
    public int getYear() {
        return this.getYear(this.uf_header2.year);
    }
    
    public float getLatitude() {
        return this.uf_header2.latitudeD + (this.uf_header2.latitudeM + this.uf_header2.latitudeS / 3840.0f) / 60.0f;
    }
    
    public float getLongtitude() {
        return this.uf_header2.longitudeD + (this.uf_header2.longitudeM + this.uf_header2.longitudeS / 3840.0f) / 60.0f;
    }
    
    public float getHorizontalBeamWidth(final String abbrev) {
        final UF_field_header2 header = this.field_header_map.get(abbrev);
        return header.HorizontalBeamWidth / 64.0f;
    }
    
    public int getYear(final int year) {
        if (year > 1970) {
            return year;
        }
        if (year > 70 && year < 100) {
            return 1900 + year;
        }
        if (year < 60) {
            return 2000 + year;
        }
        return 0;
    }
    
    public long getTitleMsecs() {
        return this.data_msecs;
    }
    
    public long setDateMesc() {
        final Calendar cal = Calendar.getInstance();
        cal.set(1, this.uf_header2.year);
        cal.set(2, this.uf_header2.month);
        cal.set(5, this.uf_header2.day);
        cal.set(11, this.uf_header2.hour);
        cal.set(12, this.uf_header2.minute);
        cal.set(13, this.uf_header2.second);
        return cal.getTimeInMillis();
    }
    
    public Date getDate() {
        final Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(this.data_msecs);
        return cal.getTime();
    }
    
    protected short getShort1(final byte[] bytes, final int offset) {
        final int ndx0 = offset + 1;
        final int ndx2 = offset + 0;
        return (short)(bytes[ndx0] << 8 | (bytes[ndx2] & 0xFF));
    }
    
    protected short getShort(final byte[] bytes, final int offset) {
        return (short)bytesToShort(bytes[offset], bytes[offset + 1], false);
    }
    
    public static int bytesToShort(final byte a, final byte b, final boolean swapBytes) {
        if (swapBytes) {
            return (a & 0xFF) + (b << 8);
        }
        return (a << 8) + (b & 0xFF);
    }
    
    public static int bytesToInt(final byte[] bytes, final boolean swapBytes) {
        final byte a = bytes[0];
        final byte b = bytes[1];
        final byte c = bytes[2];
        final byte d = bytes[3];
        if (swapBytes) {
            return (a & 0xFF) + ((b & 0xFF) << 8) + ((c & 0xFF) << 16) + ((d & 0xFF) << 24);
        }
        return ((a & 0xFF) << 24) + ((b & 0xFF) << 16) + ((c & 0xFF) << 8) + (d & 0xFF);
    }
    
    public short[] byte2short(final byte[] a, final int length) {
        final int len = length / 2;
        final short[] b = new short[len];
        final byte[] b2 = new byte[2];
        for (int i = 0; i < len; ++i) {
            b2[0] = a[2 * i];
            b2[1] = a[2 * i + 1];
            b[i] = this.getShort(b2, 0);
        }
        return b;
    }
    
    public void readData(final RandomAccessFile raf, final String abbrev, final Range gateRange, final IndexIterator ii) throws IOException {
        long offset = this.rayOffset;
        offset += this.getDataOffset(abbrev) * 2 - 2;
        raf.seek(offset);
        final byte[] b2 = new byte[2];
        final int dataCount = this.getGateCount(abbrev);
        final byte[] data = new byte[dataCount * 2];
        raf.readFully(data);
        final short[] tmp = this.byte2short(data, 2 * dataCount);
        for (int i = gateRange.first(); i <= gateRange.last(); i += gateRange.stride()) {
            if (i >= dataCount) {
                ii.setShortNext(this.uf_header2.missing);
            }
            else {
                b2[0] = data[i * 2];
                b2[1] = data[i * 2 + 1];
                final short value = this.getShort(b2, 0);
                ii.setShortNext(value);
            }
        }
    }
    
    class UF_mandatory_header2
    {
        String textUF;
        short recordSize;
        short offset2StartOfOptionalHeader;
        short localUseHeaderPosition;
        short dataHeaderPosition;
        short recordNumber;
        short volumeNumber;
        short rayNumber;
        short recordNumber1;
        short sweepNumber;
        String radarName;
        String siteName;
        short latitudeD;
        short latitudeM;
        short latitudeS;
        short longitudeD;
        short longitudeM;
        short longitudeS;
        short height;
        short year;
        short month;
        short day;
        short hour;
        short minute;
        short second;
        String timeZone;
        short azimuth;
        short elevation;
        short sweepMode;
        short fixedAngle;
        short sweepRate;
        short year1;
        short month1;
        short day1;
        String nameOfUFGeneratorProgram;
        short missing;
        
        public UF_mandatory_header2(final byte[] data) {
            this.textUF = new String(data, 0, 2);
            if (Ray.this.debug) {
                System.out.println(this.textUF);
            }
            this.recordSize = Ray.this.getShort(data, 2);
            this.offset2StartOfOptionalHeader = Ray.this.getShort(data, 4);
            this.localUseHeaderPosition = Ray.this.getShort(data, 6);
            this.dataHeaderPosition = Ray.this.getShort(data, 8);
            this.recordNumber = Ray.this.getShort(data, 10);
            this.volumeNumber = Ray.this.getShort(data, 12);
            this.rayNumber = Ray.this.getShort(data, 14);
            this.recordNumber1 = Ray.this.getShort(data, 16);
            this.sweepNumber = Ray.this.getShort(data, 18);
            this.radarName = new String(data, 20, 8);
            this.siteName = new String(data, 28, 8);
            this.latitudeD = Ray.this.getShort(data, 36);
            this.latitudeM = Ray.this.getShort(data, 38);
            this.latitudeS = Ray.this.getShort(data, 40);
            this.longitudeD = Ray.this.getShort(data, 42);
            this.longitudeM = Ray.this.getShort(data, 44);
            this.longitudeS = Ray.this.getShort(data, 46);
            this.height = Ray.this.getShort(data, 48);
            final int yearValue = Ray.this.getShort(data, 50);
            this.year = (short)Ray.this.getYear(yearValue);
            this.month = Ray.this.getShort(data, 52);
            this.day = Ray.this.getShort(data, 54);
            this.hour = Ray.this.getShort(data, 56);
            this.minute = Ray.this.getShort(data, 58);
            this.second = Ray.this.getShort(data, 60);
            this.timeZone = new String(data, 62, 2);
            this.azimuth = Ray.this.getShort(data, 64);
            this.elevation = Ray.this.getShort(data, 66);
            this.sweepMode = Ray.this.getShort(data, 68);
            this.fixedAngle = Ray.this.getShort(data, 70);
            this.sweepRate = Ray.this.getShort(data, 72);
            this.year1 = Ray.this.getShort(data, 74);
            this.month1 = Ray.this.getShort(data, 76);
            this.day1 = Ray.this.getShort(data, 78);
            this.nameOfUFGeneratorProgram = new String(data, 80, 8);
            this.missing = Ray.this.getShort(data, 88);
        }
    }
    
    class UF_optional_header
    {
        String sProjectName;
        short iBaselineAzimuth;
        short iBaselineelevation;
        short iVolumeScanHour;
        short iVolumeScanMinute;
        short iVolumeScanSecond;
        String sFieldTapeName;
        short iFlag;
        
        public UF_optional_header(final byte[] data) {
            this.sProjectName = new String(data, 0, 8);
            this.iBaselineAzimuth = Ray.this.getShort(data, 8);
            this.iBaselineelevation = Ray.this.getShort(data, 10);
            this.iVolumeScanHour = Ray.this.getShort(data, 12);
            this.iVolumeScanMinute = Ray.this.getShort(data, 14);
            this.iVolumeScanSecond = Ray.this.getShort(data, 16);
            this.sFieldTapeName = new String(data, 18, 8);
            this.iFlag = Ray.this.getShort(data, 26);
        }
    }
    
    class UF_field_header2
    {
        short dataOffset;
        short scaleFactor;
        short startRange;
        short startRange1;
        short binSpacing;
        short binCount;
        short pulseWidth;
        short HorizontalBeamWidth;
        short verticalBeamWidth;
        short receiverBandwidth;
        short polarization;
        short waveLength;
        short sampleSize;
        String typeOfData;
        short thresholdValue;
        short scale;
        String editCode;
        short prt;
        short bits;
        
        public UF_field_header2(final byte[] data) {
            this.dataOffset = Ray.this.getShort(data, 0);
            this.scaleFactor = Ray.this.getShort(data, 2);
            this.startRange = Ray.this.getShort(data, 4);
            this.startRange1 = Ray.this.getShort(data, 6);
            this.binSpacing = Ray.this.getShort(data, 8);
            this.binCount = Ray.this.getShort(data, 10);
            this.pulseWidth = Ray.this.getShort(data, 12);
            this.HorizontalBeamWidth = Ray.this.getShort(data, 14);
            this.verticalBeamWidth = Ray.this.getShort(data, 16);
            this.receiverBandwidth = Ray.this.getShort(data, 18);
            this.polarization = Ray.this.getShort(data, 20);
            this.waveLength = Ray.this.getShort(data, 22);
            this.sampleSize = Ray.this.getShort(data, 24);
            this.typeOfData = new String(data, 26, 2);
            this.thresholdValue = Ray.this.getShort(data, 28);
            this.scale = Ray.this.getShort(data, 30);
            this.editCode = new String(data, 32, 2);
            this.prt = Ray.this.getShort(data, 34);
            this.bits = Ray.this.getShort(data, 36);
        }
    }
    
    class UF_fsi2
    {
        short nyquistVelocity;
        short radarConstant;
        short noisePower;
        short receiverGain;
        short peakPower;
        short antennaGain;
        short pulseDuration;
        
        UF_fsi2(final byte[] data) {
            if (data.length > 4) {
                this.radarConstant = Ray.this.getShort(data, 0);
                this.noisePower = Ray.this.getShort(data, 2);
                this.receiverGain = Ray.this.getShort(data, 24);
                this.peakPower = Ray.this.getShort(data, 6);
                this.antennaGain = Ray.this.getShort(data, 8);
                this.pulseDuration = Ray.this.getShort(data, 10);
            }
            else {
                this.nyquistVelocity = Ray.this.getShort(data, 0);
            }
        }
    }
}
