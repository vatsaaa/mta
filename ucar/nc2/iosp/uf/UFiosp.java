// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.iosp.uf;

import ucar.ma2.InvalidRangeException;
import ucar.ma2.Range;
import ucar.ma2.Section;
import ucar.ma2.IndexIterator;
import ucar.ma2.Index;
import java.util.Date;
import ucar.ma2.Array;
import ucar.nc2.constants.AxisType;
import ucar.ma2.DataType;
import ucar.nc2.Structure;
import ucar.nc2.Dimension;
import ucar.nc2.Variable;
import java.io.IOException;
import java.util.Iterator;
import java.util.Set;
import java.util.HashMap;
import ucar.unidata.geoloc.Earth;
import ucar.nc2.constants.FeatureType;
import ucar.nc2.Group;
import ucar.nc2.Attribute;
import java.util.List;
import java.util.ArrayList;
import ucar.nc2.util.CancelTask;
import ucar.nc2.units.DateFormatter;
import ucar.unidata.io.RandomAccessFile;
import ucar.nc2.NetcdfFile;
import ucar.nc2.iosp.AbstractIOServiceProvider;

public class UFiosp extends AbstractIOServiceProvider
{
    private static final int MISSING_INT = -9999;
    private static final float MISSING_FLOAT = Float.NaN;
    private NetcdfFile ncfile;
    private RandomAccessFile myRaf;
    protected UFheader headerParser;
    private DateFormatter formatter;
    private double radarRadius;
    
    public UFiosp() {
        this.formatter = new DateFormatter();
        this.radarRadius = 100000.0;
    }
    
    public boolean isValidFile(final RandomAccessFile raf) {
        final UFheader localHeader = new UFheader();
        return localHeader.isValidFile(raf);
    }
    
    public String getFileTypeId() {
        return "UniversalRadarFormat";
    }
    
    public String getFileTypeDescription() {
        return "Universal Radar Format";
    }
    
    @Override
    public void open(final RandomAccessFile raf, final NetcdfFile file, final CancelTask cancelTask) throws IOException {
        this.ncfile = file;
        this.myRaf = raf;
        (this.headerParser = new UFheader()).read(this.myRaf, this.ncfile);
        final HashMap variables = this.headerParser.variableGroup;
        final Set vSet = variables.keySet();
        for (final String key : vSet) {
            final ArrayList group = variables.get(key);
            final List<Ray> firstGroup = group.get(0);
            final Ray ray0 = firstGroup.get(0);
            this.makeVariable(this.ncfile, ray0.getDatatypeName(key), ray0.getDatatypeName(key), key, group);
        }
        this.ncfile.addAttribute(null, new Attribute("Conventions", "_Coordinates"));
        this.ncfile.addAttribute(null, new Attribute("format", this.headerParser.getDataFormat()));
        this.ncfile.addAttribute(null, new Attribute("cdm_data_type", FeatureType.RADIAL.toString()));
        this.ncfile.addAttribute(null, new Attribute("StationLatitude", new Double(this.headerParser.getStationLatitude())));
        this.ncfile.addAttribute(null, new Attribute("StationLongitude", new Double(this.headerParser.getStationLongitude())));
        this.ncfile.addAttribute(null, new Attribute("StationElevationInMeters", new Double(this.headerParser.getStationElevation())));
        this.ncfile.addAttribute(null, new Attribute("time_coverage_start", this.formatter.toDateTimeStringISO(this.headerParser.getStartDate())));
        this.ncfile.addAttribute(null, new Attribute("time_coverage_end", this.formatter.toDateTimeStringISO(this.headerParser.getEndDate())));
        final double latRadiusDegrees = Math.toDegrees(this.radarRadius / Earth.getRadius());
        this.ncfile.addAttribute(null, new Attribute("geospatial_lat_min", new Double(this.headerParser.getStationLatitude() - latRadiusDegrees)));
        this.ncfile.addAttribute(null, new Attribute("geospatial_lat_max", new Double(this.headerParser.getStationLatitude() + latRadiusDegrees)));
        final double cosLat = Math.cos(Math.toRadians(this.headerParser.getStationLatitude()));
        final double lonRadiusDegrees = Math.toDegrees(this.radarRadius / cosLat / Earth.getRadius());
        this.ncfile.addAttribute(null, new Attribute("geospatial_lon_min", new Double(this.headerParser.getStationLongitude() - lonRadiusDegrees)));
        this.ncfile.addAttribute(null, new Attribute("geospatial_lon_max", new Double(this.headerParser.getStationLongitude() + lonRadiusDegrees)));
        this.ncfile.addAttribute(null, new Attribute("history", "Direct read of Nexrad Level 2 file into NetCDF-Java 2.2 API"));
        this.ncfile.addAttribute(null, new Attribute("DataType", "Radial"));
        this.ncfile.addAttribute(null, new Attribute("Title", "Nexrad Level 2 Station " + this.headerParser.getStationId() + " from " + this.formatter.toDateTimeStringISO(this.headerParser.getStartDate()) + " to " + this.formatter.toDateTimeStringISO(this.headerParser.getEndDate())));
        this.ncfile.addAttribute(null, new Attribute("Summary", "Weather Surveillance Radar-1988 Doppler (WSR-88D) Level II data are the three meteorological base data quantities: reflectivity, mean radial velocity, and spectrum width."));
        this.ncfile.addAttribute(null, new Attribute("keywords", "WSR-88D; NEXRAD; Radar Level II; reflectivity; mean radial velocity; spectrum width"));
        this.ncfile.addAttribute(null, new Attribute("SweepMode", new Short(this.headerParser.getSweepMode())));
        this.ncfile.finish();
    }
    
    public Variable makeVariable(final NetcdfFile ncfile, final String shortName, final String longName, final String abbrev, final List groups) throws IOException {
        final int nscans = groups.size();
        if (nscans == 0) {
            throw new IllegalStateException("No data for " + shortName);
        }
        final List<Ray> firstGroup = groups.get(0);
        final Ray firstRay = firstGroup.get(0);
        final int ngates = firstRay.getGateCount(abbrev);
        final String scanDimName = "scan" + abbrev;
        final String gateDimName = "gate" + abbrev;
        final String radialDimName = "radial" + abbrev;
        final Dimension scanDim = new Dimension(scanDimName, nscans);
        final Dimension gateDim = new Dimension(gateDimName, ngates);
        final Dimension radialDim = new Dimension(radialDimName, this.headerParser.getMaxRadials(), true);
        ncfile.addDimension(null, scanDim);
        ncfile.addDimension(null, gateDim);
        ncfile.addDimension(null, radialDim);
        final List<Dimension> dims = new ArrayList<Dimension>();
        dims.add(scanDim);
        dims.add(radialDim);
        dims.add(gateDim);
        final Variable v = new Variable(ncfile, null, null, shortName + abbrev);
        v.setDataType(DataType.SHORT);
        v.setDimensions(dims);
        ncfile.addVariable(null, v);
        v.addAttribute(new Attribute("units", firstRay.getDatatypeUnits(abbrev)));
        v.addAttribute(new Attribute("long_name", longName));
        v.addAttribute(new Attribute("abbrev", abbrev));
        v.addAttribute(new Attribute("missing_value", firstRay.getMissingData()));
        v.addAttribute(new Attribute("signal_below_threshold", firstRay.getDatatypeRangeFoldingThreshhold(abbrev)));
        v.addAttribute(new Attribute("scale_factor", firstRay.getDatatypeScaleFactor(abbrev)));
        v.addAttribute(new Attribute("add_offset", firstRay.getDatatypeAddOffset(abbrev)));
        v.addAttribute(new Attribute("range_folding_threshold", firstRay.getDatatypeRangeFoldingThreshhold(abbrev)));
        final List<Dimension> dim2 = new ArrayList<Dimension>();
        dim2.add(scanDim);
        dim2.add(radialDim);
        final String timeCoordName = "time" + abbrev;
        final Variable timeVar = new Variable(ncfile, null, null, timeCoordName);
        timeVar.setDataType(DataType.INT);
        timeVar.setDimensions(dim2);
        ncfile.addVariable(null, timeVar);
        final Date d = firstRay.getDate();
        final String units = "msecs since " + this.formatter.toDateTimeStringISO(d);
        timeVar.addAttribute(new Attribute("long_name", "time since base date"));
        timeVar.addAttribute(new Attribute("units", units));
        timeVar.addAttribute(new Attribute("missing_value", firstRay.getMissingData()));
        timeVar.addAttribute(new Attribute("_CoordinateAxisType", AxisType.Time.toString()));
        final String elevCoordName = "elevation" + abbrev;
        final Variable elevVar = new Variable(ncfile, null, null, elevCoordName);
        elevVar.setDataType(DataType.FLOAT);
        elevVar.setDimensions(dim2);
        ncfile.addVariable(null, elevVar);
        elevVar.addAttribute(new Attribute("units", "degrees"));
        elevVar.addAttribute(new Attribute("long_name", "elevation angle in degres: 0 = parallel to pedestal base, 90 = perpendicular"));
        elevVar.addAttribute(new Attribute("missing_value", firstRay.getMissingData()));
        elevVar.addAttribute(new Attribute("_CoordinateAxisType", AxisType.RadialElevation.toString()));
        final String aziCoordName = "azimuth" + abbrev;
        final Variable aziVar = new Variable(ncfile, null, null, aziCoordName);
        aziVar.setDataType(DataType.FLOAT);
        aziVar.setDimensions(dim2);
        ncfile.addVariable(null, aziVar);
        aziVar.addAttribute(new Attribute("units", "degrees"));
        aziVar.addAttribute(new Attribute("long_name", "azimuth angle in degrees: 0 = true north, 90 = east"));
        aziVar.addAttribute(new Attribute("missing_value", firstRay.getMissingData()));
        aziVar.addAttribute(new Attribute("_CoordinateAxisType", AxisType.RadialAzimuth.toString()));
        final String gateCoordName = "distance" + abbrev;
        final Variable gateVar = new Variable(ncfile, null, null, gateCoordName);
        gateVar.setDataType(DataType.FLOAT);
        gateVar.setDimensions(gateDimName);
        final Array data = Array.makeArray(DataType.FLOAT, ngates, firstRay.getGateStart(abbrev), firstRay.getGateSize(abbrev));
        gateVar.setCachedData(data, false);
        ncfile.addVariable(null, gateVar);
        gateVar.addAttribute(new Attribute("units", "m"));
        gateVar.addAttribute(new Attribute("long_name", "radial distance to start of gate"));
        gateVar.addAttribute(new Attribute("_CoordinateAxisType", AxisType.RadialDistance.toString()));
        final String nradialsName = "numRadials" + abbrev;
        final Variable nradialsVar = new Variable(ncfile, null, null, nradialsName);
        nradialsVar.setDataType(DataType.INT);
        nradialsVar.setDimensions(scanDim.getName());
        nradialsVar.addAttribute(new Attribute("long_name", "number of valid radials in this scan"));
        ncfile.addVariable(null, nradialsVar);
        final String ngateName = "numGates" + abbrev;
        final Variable ngateVar = new Variable(ncfile, null, null, ngateName);
        ngateVar.setDataType(DataType.INT);
        ngateVar.setDimensions(scanDim.getName());
        ngateVar.addAttribute(new Attribute("long_name", "number of valid gates in this scan"));
        ncfile.addVariable(null, ngateVar);
        this.makeCoordinateDataWithMissing(abbrev, timeVar, elevVar, aziVar, nradialsVar, ngateVar, groups);
        final String coordinates = timeCoordName + " " + elevCoordName + " " + aziCoordName + " " + gateCoordName;
        v.addAttribute(new Attribute("_CoordinateAxes", coordinates));
        final int nradials = radialDim.getLength();
        final Ray[][] map = new Ray[nscans][nradials];
        for (int i = 0; i < groups.size(); ++i) {
            final Ray[] mapScan = map[i];
            final List<Ray> group = groups.get(i);
            int radial = 0;
            for (final Ray r : group) {
                mapScan[radial] = r;
                ++radial;
            }
        }
        final Vgroup vg = new Vgroup(abbrev, map);
        v.setSPobject(vg);
        return v;
    }
    
    private void makeCoordinateDataWithMissing(final String abbrev, final Variable time, final Variable elev, final Variable azi, final Variable nradialsVar, final Variable ngatesVar, final List groups) {
        final Array timeData = Array.factory(time.getDataType().getPrimitiveClassType(), time.getShape());
        final Index timeIndex = timeData.getIndex();
        final Array elevData = Array.factory(elev.getDataType().getPrimitiveClassType(), elev.getShape());
        final Index elevIndex = elevData.getIndex();
        final Array aziData = Array.factory(azi.getDataType().getPrimitiveClassType(), azi.getShape());
        final Index aziIndex = aziData.getIndex();
        final Array nradialsData = Array.factory(nradialsVar.getDataType().getPrimitiveClassType(), nradialsVar.getShape());
        final IndexIterator nradialsIter = nradialsData.getIndexIterator();
        final Array ngatesData = Array.factory(ngatesVar.getDataType().getPrimitiveClassType(), ngatesVar.getShape());
        final IndexIterator ngatesIter = ngatesData.getIndexIterator();
        IndexIterator ii = timeData.getIndexIterator();
        while (ii.hasNext()) {
            ii.setIntNext(-9999);
        }
        ii = elevData.getIndexIterator();
        while (ii.hasNext()) {
            ii.setFloatNext(Float.NaN);
        }
        ii = aziData.getIndexIterator();
        while (ii.hasNext()) {
            ii.setFloatNext(Float.NaN);
        }
        long last_msecs = -2147483648L;
        final int nscans = groups.size();
        try {
            for (int scan = 0; scan < nscans; ++scan) {
                final List scanGroup = groups.get(scan);
                final int nradials = scanGroup.size();
                Ray first = null;
                int radial = 0;
                for (int j = 0; j < nradials; ++j) {
                    final Ray r = scanGroup.get(j);
                    if (first == null) {
                        first = r;
                    }
                    timeData.setLong(timeIndex.set(scan, radial), r.data_msecs);
                    elevData.setFloat(elevIndex.set(scan, radial), r.getElevation());
                    aziData.setFloat(aziIndex.set(scan, radial), r.getAzimuth());
                    ++radial;
                    last_msecs = r.data_msecs;
                }
                nradialsIter.setIntNext(nradials);
                ngatesIter.setIntNext(first.getGateCount(abbrev));
            }
        }
        catch (ArrayIndexOutOfBoundsException ex) {}
        time.setCachedData(timeData, false);
        elev.setCachedData(elevData, false);
        azi.setCachedData(aziData, false);
        nradialsVar.setCachedData(nradialsData, false);
        ngatesVar.setCachedData(ngatesData, false);
    }
    
    public void flush() throws IOException {
        this.myRaf.flush();
    }
    
    @Override
    public void close() throws IOException {
        this.myRaf.close();
    }
    
    public Array readData(final Variable v2, final Section section) throws IOException, InvalidRangeException {
        final Vgroup vgroup = (Vgroup)v2.getSPobject();
        final Range scanRange = section.getRange(0);
        final Range radialRange = section.getRange(1);
        final Range gateRange = section.getRange(2);
        final Array data = Array.factory(v2.getDataType().getPrimitiveClassType(), section.getShape());
        final IndexIterator ii = data.getIndexIterator();
        for (int i = scanRange.first(); i <= scanRange.last(); i += scanRange.stride()) {
            final Ray[] mapScan = vgroup.map[i];
            this.readOneScan(mapScan, radialRange, gateRange, vgroup.abbrev, ii);
        }
        return data;
    }
    
    private void readOneScan(final Ray[] mapScan, final Range radialRange, final Range gateRange, final String abbrev, final IndexIterator ii) throws IOException {
        for (int i = radialRange.first(); i <= radialRange.last(); i += radialRange.stride()) {
            final Ray r = mapScan[i];
            this.readOneRadial(r, abbrev, gateRange, ii);
        }
    }
    
    private void readOneRadial(final Ray r, final String abbrev, final Range gateRange, final IndexIterator ii) throws IOException {
        if (r == null) {
            for (int i = gateRange.first(); i <= gateRange.last(); i += gateRange.stride()) {
                ii.setShortNext(this.headerParser.getMissingData());
            }
            return;
        }
        r.readData(this.myRaf, abbrev, gateRange, ii);
    }
    
    public static void main(final String[] args) throws Exception, IOException, InstantiationException, IllegalAccessException {
        final String fileIn = "/home/yuanho/Desktop/ufData/KTLX__sur_20080624.214247.uf";
        final NetcdfFile ncf = NetcdfFile.open(fileIn);
        ncf.close();
    }
    
    private class Vgroup
    {
        Ray[][] map;
        String abbrev;
        
        Vgroup(final String abbrev, final Ray[][] map) {
            this.abbrev = abbrev;
            this.map = map;
        }
    }
}
