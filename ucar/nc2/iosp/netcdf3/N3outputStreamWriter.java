// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.iosp.netcdf3;

import java.io.OutputStream;
import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import ucar.nc2.iosp.IospHelper;
import ucar.ma2.IndexIterator;
import ucar.ma2.DataType;
import java.util.List;
import ucar.ma2.Array;
import java.io.IOException;
import ucar.ma2.StructureData;
import ucar.ma2.StructureDataIterator;
import ucar.nc2.Variable;
import java.util.Iterator;
import ucar.nc2.Structure;
import java.io.DataOutputStream;
import ucar.nc2.NetcdfFile;

public class N3outputStreamWriter extends N3streamWriter
{
    private int recno;
    private boolean first;
    
    public N3outputStreamWriter(final NetcdfFile ncfile) {
        super(ncfile);
        this.recno = 0;
        this.first = true;
    }
    
    public void writeDataAll(final DataOutputStream stream) throws IOException {
        for (final Vinfo vinfo : this.vinfoList) {
            if (!vinfo.isRecord) {
                final Variable v = vinfo.v;
                assert this.filePos == vinfo.offset;
                if (this.debugPos) {
                    System.out.println(" writing at " + this.filePos + " should be " + vinfo.offset + " " + v.getName());
                }
                final int nbytes = this.writeDataFast(v, stream, v.read());
                this.filePos += nbytes;
                this.filePos += this.pad(stream, nbytes, (byte)0);
            }
        }
        final boolean useRecordDimension = this.ncfile.hasUnlimitedDimension();
        if (useRecordDimension) {
            this.ncfile.sendIospMessage("AddRecordStructure");
        }
        if (useRecordDimension) {
            boolean first = true;
            int nrec = 0;
            final Structure recordVar = (Structure)this.ncfile.findVariable("record");
            final StructureDataIterator ii = recordVar.getStructureIterator();
            while (ii.hasNext()) {
                final StructureData sdata = ii.next();
                int count = 0;
                for (final Vinfo vinfo2 : this.vinfoList) {
                    if (vinfo2.isRecord) {
                        final Variable v2 = vinfo2.v;
                        final int nbytes2 = this.writeDataFast(v2, stream, sdata.getArray(v2.getName()));
                        count += nbytes2;
                        count += this.pad(stream, nbytes2, (byte)0);
                        if (!first || !this.debugWriteData) {
                            continue;
                        }
                        System.out.println(v2.getName() + " wrote " + count + " bytes");
                    }
                }
                if (first && this.debugWriteData) {
                    System.out.println("wrote " + count + " bytes");
                    first = false;
                }
                ++nrec;
            }
            if (this.debugWriteData) {
                System.out.println("wrote " + nrec + " records");
            }
            stream.flush();
            this.ncfile.sendIospMessage("RemoveRecordStructure");
            this.ncfile.finish();
        }
    }
    
    public void writeNonRecordData(final Variable v, final DataOutputStream stream, final Array data) throws IOException {
        final Vinfo vinfo = this.vinfoMap.get(v);
        if (this.debugWriteData) {
            System.out.println("Write " + v.getName() + " at filePos= " + this.filePos + " vinfo.offset= " + vinfo.offset);
        }
        if (this.filePos != vinfo.offset) {
            throw new IllegalStateException();
        }
        this.filePos += this.writeData(v, stream, data);
        if (vinfo.pad > 0) {
            final byte[] dummy = new byte[vinfo.pad];
            stream.write(dummy);
            this.filePos += vinfo.pad;
        }
    }
    
    public void writeRecordData(final DataOutputStream stream, final List<Variable> varList) throws IOException {
        final long want = this.recStart + this.recno * this.recSize;
        if (this.debugWriteData) {
            System.out.println("Write record at filePos= " + this.filePos + " should be= " + want);
        }
        if (this.filePos != want) {
            throw new IllegalStateException();
        }
        for (final Variable v : varList) {
            if (this.first && this.debugWriteData) {
                System.out.println("  write record var " + v.getNameAndDimensions() + " filePos=" + this.filePos);
            }
            this.filePos += this.writeData(v, stream, v.read());
            final Vinfo vinfo = this.vinfoMap.get(v);
            if (vinfo.pad > 0) {
                final byte[] dummy = new byte[vinfo.pad];
                stream.write(dummy);
                this.filePos += vinfo.pad;
            }
        }
        this.first = false;
        ++this.recno;
    }
    
    private long writeData(final Variable v, final DataOutputStream stream, final Array values) throws IOException {
        final DataType dataType = v.getDataType();
        final IndexIterator ii = values.getIndexIterator();
        if (dataType == DataType.BYTE) {
            while (ii.hasNext()) {
                stream.write(ii.getByteNext());
            }
            return values.getSize();
        }
        if (dataType == DataType.CHAR) {
            while (ii.hasNext()) {
                stream.write(ii.getByteNext());
            }
            return values.getSize();
        }
        if (dataType == DataType.SHORT) {
            while (ii.hasNext()) {
                stream.writeShort(ii.getShortNext());
            }
            return 2L * values.getSize();
        }
        if (dataType == DataType.INT) {
            while (ii.hasNext()) {
                stream.writeInt(ii.getIntNext());
            }
            return 4L * values.getSize();
        }
        if (dataType == DataType.FLOAT) {
            while (ii.hasNext()) {
                stream.writeFloat(ii.getFloatNext());
            }
            return 4L * values.getSize();
        }
        if (dataType == DataType.DOUBLE) {
            while (ii.hasNext()) {
                stream.writeDouble(ii.getDoubleNext());
            }
            return 8L * values.getSize();
        }
        throw new IllegalStateException("dataType= " + dataType);
    }
    
    private int writeDataFast(final Variable v, final DataOutputStream stream, final Array values) throws IOException {
        final DataType dataType = v.getDataType();
        if (dataType == DataType.BYTE) {
            final byte[] pa = (byte[])values.get1DJavaArray(Byte.TYPE);
            for (int i = 0; i < pa.length; ++i) {
                stream.write(pa[i]);
            }
            return pa.length;
        }
        if (dataType == DataType.CHAR) {
            final byte[] pa = IospHelper.convertCharToByte((char[])values.get1DJavaArray(Character.TYPE));
            for (int i = 0; i < pa.length; ++i) {
                stream.write(pa[i]);
            }
            return pa.length;
        }
        if (dataType == DataType.SHORT) {
            final short[] pa2 = (short[])values.get1DJavaArray(Short.TYPE);
            for (int i = 0; i < pa2.length; ++i) {
                stream.writeShort(pa2[i]);
            }
            return 2 * pa2.length;
        }
        if (dataType == DataType.INT) {
            final int[] pa3 = (int[])values.get1DJavaArray(Integer.TYPE);
            for (int i = 0; i < pa3.length; ++i) {
                stream.writeInt(pa3[i]);
            }
            return 4 * pa3.length;
        }
        if (dataType == DataType.FLOAT) {
            final float[] pa4 = (float[])values.get1DJavaArray(Float.TYPE);
            for (int i = 0; i < pa4.length; ++i) {
                stream.writeFloat(pa4[i]);
            }
            return 4 * pa4.length;
        }
        if (dataType == DataType.DOUBLE) {
            final double[] pa5 = (double[])values.get1DJavaArray(Double.TYPE);
            for (int i = 0; i < pa5.length; ++i) {
                stream.writeDouble(pa5[i]);
            }
            return 8 * pa5.length;
        }
        throw new IllegalStateException("dataType= " + dataType);
    }
    
    public static void writeFromFile(final NetcdfFile fileIn, final String fileOutName) throws IOException {
        final DataOutputStream stream = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(fileOutName), 10000));
        final N3outputStreamWriter writer = new N3outputStreamWriter(fileIn);
        final int numrec = (fileIn.getUnlimitedDimension() == null) ? 0 : fileIn.getUnlimitedDimension().getLength();
        writer.writeHeader(stream, numrec);
        writer.writeDataAll(stream);
        stream.close();
    }
    
    public static void main(final String[] args) throws IOException {
        writeFromFile(NetcdfFile.open("C:/data/metars/Surface_METAR_20070331_0000.nc"), "C:/temp/streamOut.nc");
    }
}
