// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.iosp.netcdf3;

import java.io.OutputStream;
import java.io.BufferedOutputStream;
import java.io.DataOutputStream;
import java.nio.channels.Channels;
import java.io.FileOutputStream;
import java.io.IOException;
import ucar.nc2.Variable;
import java.util.Iterator;
import ucar.ma2.InvalidRangeException;
import ucar.ma2.Range;
import ucar.ma2.Section;
import ucar.nc2.Structure;
import java.nio.channels.WritableByteChannel;
import ucar.nc2.NetcdfFile;
import java.nio.ByteBuffer;

public class N3channelWriter extends N3streamWriter
{
    private static int buffer_size;
    private static boolean debugWrite;
    private ByteBuffer padddingBB;
    
    public N3channelWriter(final NetcdfFile ncfile) {
        super(ncfile);
    }
    
    public void writeDataAll(final WritableByteChannel channel) throws IOException, InvalidRangeException {
        for (final Vinfo vinfo : this.vinfoList) {
            if (!vinfo.isRecord) {
                final Variable v = vinfo.v;
                assert this.filePos == vinfo.offset;
                if (this.debugPos) {
                    System.out.println(" writing at " + this.filePos + " should be " + vinfo.offset + " " + v.getName());
                }
                final int nbytes = (int)v.readToByteChannel(v.getShapeAsSection(), channel);
                this.filePos += nbytes;
                this.filePos += this.pad(channel, nbytes);
                if (!this.debugPos) {
                    continue;
                }
                System.out.println(" vinfo=" + vinfo);
            }
        }
        final boolean useRecordDimension = this.ncfile.hasUnlimitedDimension();
        if (useRecordDimension) {
            this.ncfile.sendIospMessage("AddRecordStructure");
            final Structure recordVar = (Structure)this.ncfile.findVariable("record");
            final Section section = new Section().appendRange(null);
            long bytesDone = 0L;
            long done = 0L;
            final long nrecs = (int)recordVar.getSize();
            final int structureSize = recordVar.getElementSize();
            for (int readAtaTime = Math.max(10, N3channelWriter.buffer_size / structureSize), count = 0; count < nrecs; count += readAtaTime) {
                final long last = Math.min(nrecs, done + readAtaTime);
                final int need = (int)(last - done);
                section.setRange(0, new Range(count, count + need - 1));
                try {
                    bytesDone += recordVar.readToByteChannel(section, channel);
                    done += need;
                }
                catch (InvalidRangeException e) {
                    e.printStackTrace();
                    break;
                }
            }
            assert done == nrecs;
            bytesDone /= 1000000L;
            if (N3channelWriter.debugWrite) {
                System.out.println("write record var; total = " + bytesDone + " Mbytes # recs=" + done);
            }
            this.ncfile.sendIospMessage("RemoveRecordStructure");
            this.ncfile.finish();
        }
    }
    
    private int pad(final WritableByteChannel channel, final int nbytes) throws IOException {
        final int pad = N3header.padding(nbytes);
        if (null != channel && pad > 0) {
            if (this.padddingBB == null) {
                this.padddingBB = ByteBuffer.allocate(4);
            }
            this.padddingBB.position(0);
            this.padddingBB.limit(pad);
            channel.write(this.padddingBB);
        }
        return pad;
    }
    
    public static void writeFromFile(final NetcdfFile fileIn, final String fileOutName) throws IOException, InvalidRangeException {
        final FileOutputStream stream = new FileOutputStream(fileOutName);
        final WritableByteChannel channel = stream.getChannel();
        final DataOutputStream dout = new DataOutputStream(Channels.newOutputStream(channel));
        final N3channelWriter writer = new N3channelWriter(fileIn);
        final int numrec = (fileIn.getUnlimitedDimension() == null) ? 0 : fileIn.getUnlimitedDimension().getLength();
        writer.writeHeader(dout, numrec);
        dout.flush();
        writer.writeDataAll(channel);
        channel.close();
    }
    
    public static void writeToChannel(final NetcdfFile ncfile, final WritableByteChannel wbc) throws IOException, InvalidRangeException {
        final DataOutputStream stream = new DataOutputStream(new BufferedOutputStream(Channels.newOutputStream(wbc), 8000));
        final N3channelWriter writer = new N3channelWriter(ncfile);
        final int numrec = (ncfile.getUnlimitedDimension() == null) ? 0 : ncfile.getUnlimitedDimension().getLength();
        writer.writeHeader(stream, numrec);
        stream.flush();
        writer.writeDataAll(wbc);
    }
    
    static {
        N3channelWriter.buffer_size = 1000000;
        N3channelWriter.debugWrite = false;
    }
}
