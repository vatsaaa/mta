// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.iosp.netcdf3;

import org.slf4j.LoggerFactory;
import ucar.nc2.Attribute;
import ucar.nc2.Dimension;
import java.util.List;
import ucar.ma2.ArrayStructure;
import java.nio.CharBuffer;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.nio.channels.WritableByteChannel;
import java.util.Iterator;
import ucar.ma2.Range;
import ucar.ma2.ArrayStructureBB;
import ucar.ma2.StructureMembers;
import ucar.ma2.InvalidRangeException;
import ucar.nc2.iosp.Layout;
import ucar.ma2.DataType;
import ucar.nc2.iosp.LayoutRegular;
import ucar.nc2.iosp.LayoutRegularSegmented;
import ucar.nc2.Structure;
import ucar.ma2.Array;
import ucar.ma2.Section;
import ucar.nc2.Variable;
import java.io.File;
import ucar.nc2.util.CancelTask;
import ucar.unidata.util.Format;
import java.util.Formatter;
import java.io.IOException;
import ucar.unidata.io.RandomAccessFile;
import java.util.regex.Matcher;
import java.util.HashMap;
import ucar.nc2.NetcdfFile;
import java.util.regex.Pattern;
import org.slf4j.Logger;
import ucar.nc2.iosp.IOServiceProviderWriter;
import ucar.nc2.iosp.AbstractIOServiceProvider;

public abstract class N3iosp extends AbstractIOServiceProvider implements IOServiceProviderWriter
{
    private static Logger log;
    public static final byte NC_FILL_BYTE = -127;
    public static final char NC_FILL_CHAR = '\0';
    public static final short NC_FILL_SHORT = -32767;
    public static final int NC_FILL_INT = -2147483647;
    public static final long NC_FILL_LONG = -9223372036854775806L;
    public static final float NC_FILL_FLOAT = 9.96921E36f;
    public static final double NC_FILL_DOUBLE = 9.969209968386869E36;
    public static final String FillValue = "_FillValue";
    public static final long MAX_VARSIZE = 4294967292L;
    public static final int MAX_NUMRECS = Integer.MAX_VALUE;
    private static boolean syncExtendOnly;
    private static final Pattern objectNamePattern;
    protected NetcdfFile ncfile;
    protected boolean readonly;
    protected N3header header;
    protected long lastModified;
    protected boolean debug;
    protected boolean debugSize;
    protected boolean debugSPIO;
    protected boolean debugRecord;
    protected boolean debugSync;
    protected boolean showHeaderBytes;
    protected boolean useRecordStructure;
    protected boolean fill;
    protected HashMap dimHash;
    
    public N3iosp() {
        this.debug = false;
        this.debugSize = false;
        this.debugSPIO = false;
        this.debugRecord = false;
        this.debugSync = false;
        this.showHeaderBytes = false;
        this.fill = true;
        this.dimHash = new HashMap(50);
    }
    
    public static void setProperty(final String name, final String value) {
        if (name.equalsIgnoreCase("syncExtendOnly")) {
            N3iosp.syncExtendOnly = value.equalsIgnoreCase("true");
        }
    }
    
    public static String makeValidNetcdfObjectName(final String name) {
        final StringBuilder sb = new StringBuilder(name.trim());
        while (sb.length() > 0) {
            final char c = sb.charAt(0);
            if (Character.isLetter(c) || Character.isDigit(c)) {
                break;
            }
            if (c == '_') {
                break;
            }
            sb.deleteCharAt(0);
        }
        for (int pos = 1; pos < sb.length(); ++pos) {
            final int c2 = sb.codePointAt(pos);
            if ((c2 >= 0 && c2 < 32) || c2 == 127) {
                sb.delete(pos, pos + 1);
                --pos;
            }
        }
        if (sb.length() == 0) {
            throw new IllegalArgumentException("Illegal name");
        }
        return sb.toString();
    }
    
    public static String makeValidNetcdfObjectNameOld(final String name) {
        final StringBuilder sb = new StringBuilder(name);
        while (sb.length() > 0) {
            final char c = sb.charAt(0);
            if (Character.isLetter(c)) {
                break;
            }
            if (c == '_') {
                break;
            }
            if (Character.isDigit(c)) {
                sb.insert(0, 'N');
                break;
            }
            sb.deleteCharAt(0);
        }
        for (int i = 1; i < sb.length(); ++i) {
            final char c2 = sb.charAt(i);
            if (c2 == ' ') {
                sb.setCharAt(i, '_');
            }
            else {
                final boolean ok = Character.isLetterOrDigit(c2) || c2 == '-' || c2 == '_';
                if (!ok) {
                    sb.delete(i, i + 1);
                    --i;
                }
            }
        }
        return sb.toString();
    }
    
    public static boolean isValidNetcdf3ObjectName(final String name) {
        final Matcher m = N3iosp.objectNamePattern.matcher(name);
        return m.matches();
    }
    
    public static Pattern getValidNetcdf3ObjectNamePattern() {
        return N3iosp.objectNamePattern;
    }
    
    public static String createValidNetcdf3ObjectName(final String name) {
        final StringBuilder sb = new StringBuilder(name);
        while (sb.length() > 0) {
            final char c = sb.charAt(0);
            if (Character.isLetter(c)) {
                break;
            }
            if (c == '_') {
                break;
            }
            if (Character.isDigit(c)) {
                sb.insert(0, 'N');
                break;
            }
            sb.deleteCharAt(0);
        }
        for (int i = 1; i < sb.length(); ++i) {
            final char c2 = sb.charAt(i);
            if (c2 == ' ') {
                sb.setCharAt(i, '_');
            }
            else {
                final boolean ok = Character.isLetterOrDigit(c2) || c2 == '-' || c2 == '_' || c2 == '@' || c2 == ':' || c2 == '(' || c2 == ')' || c2 == '+' || c2 == '.';
                if (!ok) {
                    sb.delete(i, i + 1);
                    --i;
                }
            }
        }
        return sb.toString();
    }
    
    public boolean isValidFile(final RandomAccessFile raf) throws IOException {
        return N3header.isValidFile(raf);
    }
    
    @Override
    public String getDetailInfo() {
        try {
            final Formatter fout = new Formatter();
            final double size = this.raf.length() / 1000000.0;
            fout.format(" raf = %s%n", this.raf.getLocation());
            fout.format(" size= %d (%s Mb)%n%n", this.raf.length(), Format.dfrac(size, 3));
            this.header.showDetail(fout);
            return fout.toString();
        }
        catch (IOException e) {
            return e.getMessage();
        }
    }
    
    @Override
    public void open(final RandomAccessFile raf, final NetcdfFile ncfile, final CancelTask cancelTask) throws IOException {
        this.raf = raf;
        this.ncfile = ncfile;
        final String location = raf.getLocation();
        if (!location.startsWith("http:")) {
            final File file = new File(location);
            if (file.exists()) {
                this.lastModified = file.lastModified();
            }
        }
        raf.order(0);
        (this.header = new N3header()).read(raf, ncfile, null);
        this._open(raf);
        ncfile.finish();
    }
    
    public void setFill(final boolean fill) {
        this.fill = fill;
    }
    
    public Array readData(final Variable v2, final Section section) throws IOException, InvalidRangeException {
        if (v2 instanceof Structure) {
            return this.readRecordData((Structure)v2, section);
        }
        final N3header.Vinfo vinfo = (N3header.Vinfo)v2.getSPobject();
        final DataType dataType = v2.getDataType();
        final Layout layout = v2.isUnlimited() ? new LayoutRegularSegmented(vinfo.begin, v2.getElementSize(), this.header.recsize, v2.getShape(), section) : new LayoutRegular(vinfo.begin, v2.getElementSize(), v2.getShape(), section);
        if (layout.getTotalNelems() == 0L) {
            return Array.factory(dataType.getPrimitiveClassType(), section.getShape());
        }
        final Object data = this.readData(layout, dataType);
        return Array.factory(dataType.getPrimitiveClassType(), section.getShape(), data);
    }
    
    private Array readRecordData(final Structure s, final Section section) throws IOException {
        final Range recordRange = section.getRange(0);
        final StructureMembers members = s.makeStructureMembers();
        for (final StructureMembers.Member m : members.getMembers()) {
            final Variable v2 = s.findVariable(m.getName());
            final N3header.Vinfo vinfo = (N3header.Vinfo)v2.getSPobject();
            m.setDataParam((int)(vinfo.begin - this.header.recStart));
        }
        if (this.header.recsize > 2147483647L) {
            throw new IllegalArgumentException("Cant read records when recsize > 2147483647");
        }
        final long nrecs = section.computeSize();
        if (nrecs * this.header.recsize > 2147483647L) {
            throw new IllegalArgumentException("Too large read: nrecs * recsize= " + nrecs * this.header.recsize + "bytes exceeds " + Integer.MAX_VALUE);
        }
        members.setStructureSize((int)this.header.recsize);
        final ArrayStructureBB structureArray = new ArrayStructureBB(members, new int[] { recordRange.length() });
        final byte[] result = structureArray.getByteBuffer().array();
        int count = 0;
        for (int recnum = recordRange.first(); recnum <= recordRange.last(); recnum += recordRange.stride()) {
            if (this.debugRecord) {
                System.out.println(" read record " + recnum);
            }
            this.raf.seek(this.header.recStart + recnum * this.header.recsize);
            if (recnum != this.header.numrecs - 1) {
                this.raf.readFully(result, (int)(count * this.header.recsize), (int)this.header.recsize);
            }
            else {
                this.raf.read(result, (int)(count * this.header.recsize), (int)this.header.recsize);
            }
            ++count;
        }
        return structureArray;
    }
    
    private Array readRecordDataSubset(final Structure s, final Section section) throws IOException {
        final Range recordRange = section.getRange(0);
        final int nrecords = recordRange.length();
        final StructureMembers members = s.makeStructureMembers();
        for (final StructureMembers.Member m : members.getMembers()) {
            final Variable v2 = s.findVariable(m.getName());
            final N3header.Vinfo vinfo = (N3header.Vinfo)v2.getSPobject();
            m.setDataParam((int)(vinfo.begin - this.header.recStart));
            final int rank = m.getShape().length;
            final int[] fullShape = new int[rank + 1];
            fullShape[0] = nrecords;
            System.arraycopy(m.getShape(), 0, fullShape, 1, rank);
            final Array data = Array.factory(m.getDataType(), fullShape);
            m.setDataArray(data);
            m.setDataObject(data.getIndexIterator());
        }
        return null;
    }
    
    public Array readNestedData(final Variable v2, final Section section) throws IOException, InvalidRangeException {
        final N3header.Vinfo vinfo = (N3header.Vinfo)v2.getSPobject();
        final DataType dataType = v2.getDataType();
        final int[] fullShape = new int[v2.getRank() + 1];
        fullShape[0] = this.header.numrecs;
        System.arraycopy(v2.getShape(), 0, fullShape, 1, v2.getRank());
        final Layout layout = new LayoutRegularSegmented(vinfo.begin, v2.getElementSize(), this.header.recsize, fullShape, section);
        final Object dataObject = this.readData(layout, dataType);
        return Array.factory(dataType.getPrimitiveClassType(), section.getShape(), dataObject);
    }
    
    @Override
    public long readToByteChannel(final Variable v2, final Section section, final WritableByteChannel channel) throws IOException, InvalidRangeException {
        if (v2 instanceof Structure) {
            return this.readRecordData((Structure)v2, section, channel);
        }
        final N3header.Vinfo vinfo = (N3header.Vinfo)v2.getSPobject();
        final DataType dataType = v2.getDataType();
        final Layout layout = v2.isUnlimited() ? new LayoutRegularSegmented(vinfo.begin, v2.getElementSize(), this.header.recsize, v2.getShape(), section) : new LayoutRegular(vinfo.begin, v2.getElementSize(), v2.getShape(), section);
        return this.readData(layout, dataType, channel);
    }
    
    private long readRecordData(final Structure s, final Section section, final WritableByteChannel out) throws IOException, InvalidRangeException {
        long count = 0L;
        final Range recordRange = section.getRange(0);
        final int stride = recordRange.stride();
        if (stride == 1) {
            final int first = recordRange.first();
            final int n = recordRange.length();
            return this.raf.readToByteChannel(out, this.header.recStart + first * this.header.recsize, n * this.header.recsize);
        }
        for (int recnum = recordRange.first(); recnum <= recordRange.last(); recnum += recordRange.stride()) {
            if (this.debugRecord) {
                System.out.println(" read record " + recnum);
            }
            this.raf.seek(this.header.recStart + recnum * this.header.recsize);
            count += this.raf.readToByteChannel(out, this.header.recStart + recnum * this.header.recsize, this.header.recsize);
        }
        return count;
    }
    
    protected static char[] convertByteToCharUTF(final byte[] byteArray) {
        final Charset c = Charset.forName("UTF-8");
        final CharBuffer output = c.decode(ByteBuffer.wrap(byteArray));
        return output.array();
    }
    
    protected static byte[] convertCharToByteUTF(final char[] from) {
        final Charset c = Charset.forName("UTF-8");
        final ByteBuffer output = c.encode(CharBuffer.wrap(from));
        return output.array();
    }
    
    public void create(final String filename, final NetcdfFile ncfile, final int extra, final long preallocateSize, final boolean largeFile) throws IOException {
        this.ncfile = ncfile;
        this.readonly = false;
        ncfile.finish();
        (this.raf = new RandomAccessFile(filename, "rw")).order(0);
        if (preallocateSize > 0L) {
            final java.io.RandomAccessFile myRaf = this.raf.getRandomAccessFile();
            myRaf.setLength(preallocateSize);
        }
        (this.header = new N3header()).create(this.raf, ncfile, extra, largeFile, null);
        this._create(this.raf);
        if (this.fill) {
            this.fillNonRecordVariables();
        }
    }
    
    public boolean rewriteHeader(final boolean largeFile) throws IOException {
        return this.header.rewriteHeader(largeFile, null);
    }
    
    public void writeData(final Variable v2, final Section section, final Array values) throws IOException, InvalidRangeException {
        final N3header.Vinfo vinfo = (N3header.Vinfo)v2.getSPobject();
        final DataType dataType = v2.getDataType();
        if (v2.isUnlimited()) {
            final Range firstRange = section.getRange(0);
            this.setNumrecs(firstRange.last() + 1);
        }
        if (v2 instanceof Structure) {
            this.writeRecordData((Structure)v2, section, values);
        }
        else {
            final Layout layout = v2.isUnlimited() ? new LayoutRegularSegmented(vinfo.begin, v2.getElementSize(), this.header.recsize, v2.getShape(), section) : new LayoutRegular(vinfo.begin, v2.getElementSize(), v2.getShape(), section);
            this.writeData(values, layout, dataType);
        }
    }
    
    private void writeRecordData(final Structure s, final Section section, final Array values) throws IOException, InvalidRangeException {
        if (!(values instanceof ArrayStructure)) {
            throw new IllegalArgumentException("writeRecordData: data must be ArrayStructure");
        }
        final ArrayStructure structureData = (ArrayStructure)values;
        final List<Variable> vars = s.getVariables();
        final StructureMembers members = structureData.getStructureMembers();
        final Range recordRange = section.getRange(0);
        int count = 0;
        for (int recnum = recordRange.first(); recnum <= recordRange.last(); recnum += recordRange.stride()) {
            for (final Variable v2 : vars) {
                final StructureMembers.Member m = members.findMember(v2.getShortName());
                if (null == m) {
                    continue;
                }
                final N3header.Vinfo vinfo = (N3header.Vinfo)v2.getSPobject();
                final long begin = vinfo.begin + recnum * this.header.recsize;
                final Layout layout = new LayoutRegular(begin, v2.getElementSize(), v2.getShape(), v2.getShapeAsSection());
                final Array data = structureData.getArray(count, m);
                this.writeData(data, layout, v2.getDataType());
            }
            ++count;
        }
    }
    
    protected void setNumrecs(final int n) throws IOException, InvalidRangeException {
        if (n <= this.header.numrecs) {
            return;
        }
        final int startRec = this.header.numrecs;
        if (this.debugSize) {
            System.out.println("extend records to = " + n);
        }
        this.header.setNumrecs(n);
        for (final Dimension dim : this.ncfile.getDimensions()) {
            if (dim.isUnlimited()) {
                dim.setLength(n);
            }
        }
        for (final Variable v : this.ncfile.getVariables()) {
            if (v.isUnlimited()) {
                v.resetShape();
                v.setCachedData(null, false);
            }
        }
        if (this.fill) {
            this.fillRecordVariables(startRec, n);
        }
        else {
            this.raf.setMinLength(this.header.calcFileSize());
        }
    }
    
    public void updateAttribute(final Variable v2, final Attribute att) throws IOException {
        this.header.updateAttribute(v2, att);
    }
    
    protected void fillNonRecordVariables() throws IOException {
        for (final Variable v : this.ncfile.getVariables()) {
            if (v.isUnlimited()) {
                continue;
            }
            try {
                this.writeData(v, v.getShapeAsSection(), this.makeConstantArray(v));
            }
            catch (InvalidRangeException e) {
                e.printStackTrace();
            }
        }
    }
    
    protected void fillRecordVariables(final int recStart, final int recEnd) throws IOException, InvalidRangeException {
        for (int i = recStart; i < recEnd; ++i) {
            final Range r = new Range(i, i);
            for (final Variable v : this.ncfile.getVariables()) {
                if (v.isUnlimited()) {
                    if (v instanceof Structure) {
                        continue;
                    }
                    final Section recordSection = new Section(v.getRanges());
                    recordSection.setRange(0, r);
                    this.writeData(v, recordSection, this.makeConstantArray(v));
                }
            }
        }
    }
    
    private Array makeConstantArray(final Variable v) {
        final Class classType = v.getDataType().getPrimitiveClassType();
        final Attribute att = v.findAttribute("_FillValue");
        Object storage = null;
        if (classType == Double.TYPE) {
            final double[] storageP = (double[])(storage = new double[] { (att == null) ? 9.969209968386869E36 : att.getNumericValue().doubleValue() });
        }
        else if (classType == Float.TYPE) {
            final float[] storageP2 = (float[])(storage = new float[] { (att == null) ? 9.96921E36f : att.getNumericValue().floatValue() });
        }
        else if (classType == Integer.TYPE) {
            final int[] storageP3 = (int[])(storage = new int[] { (att == null) ? -2147483647 : att.getNumericValue().intValue() });
        }
        else if (classType == Short.TYPE) {
            final short[] storageP4 = (short[])(storage = new short[] { (short)((att == null) ? -32767 : att.getNumericValue().shortValue()) });
        }
        else if (classType == Byte.TYPE) {
            final byte[] storageP5 = (byte[])(storage = new byte[] { (byte)((att == null) ? -127 : att.getNumericValue().byteValue()) });
        }
        else if (classType == Character.TYPE) {
            final char[] storageP6 = (char[])(storage = new char[] { (att != null && att.getStringValue().length() > 0) ? att.getStringValue().charAt(0) : '\0' });
        }
        return Array.factoryConstant(classType, v.getShape(), storage);
    }
    
    @Override
    public boolean syncExtend() throws IOException {
        final boolean result = this.header.synchNumrecs();
        if (result && N3iosp.log.isDebugEnabled()) {
            N3iosp.log.debug(" N3iosp syncExtend " + this.raf.getLocation() + " numrecs =" + this.header.numrecs);
        }
        return result;
    }
    
    @Override
    public boolean sync() throws IOException {
        if (N3iosp.syncExtendOnly) {
            return this.syncExtend();
        }
        if (this.lastModified == 0L) {
            return false;
        }
        final File file = new File(this.raf.getLocation());
        if (!file.exists()) {
            throw new IOException("File does not exist");
        }
        final long currentModified = file.lastModified();
        if (currentModified == this.lastModified) {
            return false;
        }
        this.ncfile.empty();
        this.open(this.raf, this.ncfile, null);
        if (N3iosp.log.isDebugEnabled()) {
            N3iosp.log.debug(" N3iosp resynced " + this.raf.getLocation() + " currentModified=" + currentModified + " lastModified= " + this.lastModified);
        }
        return true;
    }
    
    public void flush() throws IOException {
        this.raf.flush();
        this.header.writeNumrecs();
        this.raf.flush();
    }
    
    @Override
    public void close() throws IOException {
        if (this.raf != null) {
            final long size = this.header.calcFileSize();
            this.raf.setMinLength(size);
            this.raf.close();
        }
        this.raf = null;
    }
    
    @Override
    public String toStringDebug(final Object o) {
        return null;
    }
    
    @Override
    public Object sendIospMessage(final Object message) {
        if (null == this.header) {
            return null;
        }
        if (message == "AddRecordStructure") {
            return this.header.makeRecordStructure();
        }
        if (message == "RemoveRecordStructure") {
            return this.header.removeRecordStructure();
        }
        return super.sendIospMessage(message);
    }
    
    public String getFileTypeId() {
        return "netCDF";
    }
    
    public String getFileTypeDescription() {
        return "NetCDF classic format";
    }
    
    protected abstract Object readData(final Layout p0, final DataType p1) throws IOException;
    
    protected abstract long readData(final Layout p0, final DataType p1, final WritableByteChannel p2) throws IOException;
    
    protected abstract void writeData(final Array p0, final Layout p1, final DataType p2) throws IOException;
    
    protected abstract void _open(final RandomAccessFile p0) throws IOException;
    
    protected abstract void _create(final RandomAccessFile p0) throws IOException;
    
    static {
        N3iosp.log = LoggerFactory.getLogger(N3iosp.class);
        N3iosp.syncExtendOnly = false;
        objectNamePattern = Pattern.compile("[a-zA-Z0-9_][a-zA-Z0-9_@\\:\\(\\)\\.\\-\\+]*");
    }
}
