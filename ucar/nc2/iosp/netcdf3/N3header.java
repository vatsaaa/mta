// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.iosp.netcdf3;

import org.slf4j.LoggerFactory;
import ucar.ma2.IndexIterator;
import ucar.ma2.Array;
import ucar.nc2.Attribute;
import ucar.ma2.InvalidRangeException;
import ucar.ma2.DataType;
import java.util.Iterator;
import ucar.nc2.Structure;
import ucar.nc2.Group;
import java.util.Formatter;
import java.io.IOException;
import java.util.ArrayList;
import ucar.nc2.Dimension;
import ucar.nc2.Variable;
import java.util.List;
import ucar.nc2.NetcdfFile;
import ucar.unidata.io.RandomAccessFile;
import org.slf4j.Logger;

public class N3header
{
    private static Logger log;
    private static final long MAX_UNSIGNED_INT = 4294967295L;
    static final byte[] MAGIC;
    static final byte[] MAGIC_LONG;
    static final int MAGIC_DIM = 10;
    static final int MAGIC_VAR = 11;
    static final int MAGIC_ATT = 12;
    public static boolean disallowFileTruncation;
    public static boolean debugHeaderSize;
    private RandomAccessFile raf;
    private NetcdfFile ncfile;
    private List<Variable> uvars;
    private Dimension udim;
    boolean isStreaming;
    int numrecs;
    long recsize;
    long recStart;
    private boolean useLongOffset;
    private long nonRecordDataSize;
    private long dataStart;
    private long globalAttsPos;
    private boolean debugVariablePos;
    private boolean debugStreaming;
    
    public N3header() {
        this.uvars = new ArrayList<Variable>();
        this.isStreaming = false;
        this.numrecs = 0;
        this.recsize = 0L;
        this.recStart = 2147483647L;
        this.dataStart = Long.MAX_VALUE;
        this.globalAttsPos = 0L;
        this.debugVariablePos = false;
        this.debugStreaming = false;
    }
    
    public static boolean isValidFile(final RandomAccessFile raf) throws IOException {
        raf.seek(0L);
        final byte[] b = new byte[4];
        raf.read(b);
        for (int i = 0; i < 3; ++i) {
            if (b[i] != N3header.MAGIC[i]) {
                return false;
            }
        }
        return b[3] == 1 || b[3] == 2;
    }
    
    void read(final RandomAccessFile raf, final NetcdfFile ncfile, final Formatter fout) throws IOException {
        this.raf = raf;
        this.ncfile = ncfile;
        final long actualSize = raf.length();
        this.nonRecordDataSize = 0L;
        this.recsize = 0L;
        this.recStart = 2147483647L;
        long pos = 0L;
        raf.order(0);
        raf.seek(pos);
        final byte[] b = new byte[4];
        raf.read(b);
        for (int i = 0; i < 3; ++i) {
            if (b[i] != N3header.MAGIC[i]) {
                throw new IOException("Not a netCDF file");
            }
        }
        if (b[3] != 1 && b[3] != 2) {
            throw new IOException("Not a netCDF file");
        }
        this.useLongOffset = (b[3] == 2);
        this.numrecs = raf.readInt();
        if (fout != null) {
            fout.format("numrecs= %d\n", this.numrecs);
        }
        if (this.numrecs == -1) {
            this.isStreaming = true;
            this.numrecs = 0;
        }
        int numdims = 0;
        int magic = raf.readInt();
        if (magic == 0) {
            raf.readInt();
        }
        else {
            if (magic != 10) {
                throw new IOException("Misformed netCDF file - dim magic number wrong");
            }
            numdims = raf.readInt();
            if (fout != null) {
                fout.format("numdims= %d\n", numdims);
            }
        }
        for (int j = 0; j < numdims; ++j) {
            if (fout != null) {
                fout.format("  dim %d pos= %d\n", j, raf.getFilePointer());
            }
            final String name = this.readString();
            final int len = raf.readInt();
            Dimension dim;
            if (len == 0) {
                dim = new Dimension(name, this.numrecs, true, true, false);
                this.udim = dim;
            }
            else {
                dim = new Dimension(name, len, true, false, false);
            }
            ncfile.addDimension(null, dim);
            if (fout != null) {
                fout.format(" added dimension %s\n", dim);
            }
        }
        this.globalAttsPos = raf.getFilePointer();
        this.readAtts(ncfile.getRootGroup().getAttributes(), fout);
        int nvars = 0;
        magic = raf.readInt();
        if (magic == 0) {
            raf.readInt();
        }
        else {
            if (magic != 11) {
                throw new IOException("Misformed netCDF file  - var magic number wrong");
            }
            nvars = raf.readInt();
            if (fout != null) {
                fout.format("numdims= %d\n", numdims);
            }
        }
        if (fout != null) {
            fout.format("num variables= %d\n", nvars);
        }
        for (int k = 0; k < nvars; ++k) {
            final long startPos = raf.getFilePointer();
            final String name2 = this.readString();
            final Variable var = new Variable(ncfile, ncfile.getRootGroup(), null, name2);
            long velems = 1L;
            boolean isRecord = false;
            final int rank = raf.readInt();
            final List<Dimension> dims = new ArrayList<Dimension>();
            for (int l = 0; l < rank; ++l) {
                final int dimIndex = raf.readInt();
                final Dimension dim2 = ncfile.getRootGroup().getDimensions().get(dimIndex);
                if (dim2.isUnlimited()) {
                    isRecord = true;
                    this.uvars.add(var);
                }
                else {
                    velems *= dim2.getLength();
                }
                dims.add(dim2);
            }
            var.setDimensions(dims);
            if (fout != null) {
                fout.format("---name=<%s> dims = [", name2);
                for (final Dimension dim3 : dims) {
                    fout.format("%s ", dim3.getName());
                }
                fout.format("]\n", new Object[0]);
            }
            final long varAttsPos = raf.getFilePointer();
            this.readAtts(var.getAttributes(), fout);
            final int type = raf.readInt();
            final DataType dataType = this.getDataType(type);
            var.setDataType(dataType);
            long vsize = raf.readInt();
            final long begin = this.useLongOffset ? raf.readLong() : raf.readInt();
            if (fout != null) {
                fout.format(" name= %s type=%d vsize=%s velems=%d begin= %d isRecord=%s attsPos=%d\n", name2, type, vsize, velems, begin, isRecord, varAttsPos);
                final long calcVsize = (velems + padding(velems)) * dataType.getSize();
                if (vsize != calcVsize) {
                    fout.format(" *** readVsize %d != calcVsize %d\n", vsize, calcVsize);
                }
            }
            if (vsize < 0L) {
                vsize = (velems + padding(velems)) * dataType.getSize();
            }
            var.setSPobject(new Vinfo(vsize, begin, isRecord, varAttsPos));
            if (isRecord) {
                this.recsize += vsize;
                this.recStart = Math.min(this.recStart, begin);
            }
            else {
                this.nonRecordDataSize = Math.max(this.nonRecordDataSize, begin + vsize);
            }
            this.dataStart = Math.min(this.dataStart, begin);
            if (this.debugVariablePos) {
                System.out.printf("%s begin at=%d end=%d  isRecord=%s nonRecordDataSize=%d\n", var.getName(), begin, begin + vsize, isRecord, this.nonRecordDataSize);
            }
            if (fout != null) {
                fout.format("%s begin at=%d end=%d  isRecord=%s nonRecordDataSize=%d%n", var.getName(), begin, begin + vsize, isRecord, this.nonRecordDataSize);
            }
            if (N3header.debugHeaderSize) {
                System.out.printf("%s header size=%d data size= %d\n", var.getName(), raf.getFilePointer() - startPos, vsize);
            }
            ncfile.addVariable(null, var);
        }
        pos = raf.getFilePointer();
        if (this.dataStart == Long.MAX_VALUE) {
            this.dataStart = pos;
        }
        if (this.nonRecordDataSize > 0L) {
            this.nonRecordDataSize -= this.dataStart;
        }
        if (this.uvars.size() == 0) {
            this.recStart = 0L;
        }
        if (N3header.debugHeaderSize) {
            System.out.println("  filePointer = " + pos + " dataStart=" + this.dataStart);
            System.out.println("  recStart = " + this.recStart + " dataStart+nonRecordDataSize =" + (this.dataStart + this.nonRecordDataSize));
            System.out.println("  nonRecordDataSize size= " + this.nonRecordDataSize);
            System.out.println("  recsize= " + this.recsize);
            System.out.println("  numrecs= " + this.numrecs);
            System.out.println("  actualSize= " + actualSize);
        }
        if (this.isStreaming) {
            final long recordSpace = actualSize - this.recStart;
            this.numrecs = (int)(recordSpace / this.recsize);
            if (this.debugStreaming) {
                System.out.println(" isStreaming recordSpace=" + recordSpace + " numrecs=" + this.numrecs + " has extra bytes = " + recordSpace % this.recsize);
            }
            if (this.udim != null) {
                this.udim.setLength(this.numrecs);
                for (final Variable uvar : this.uvars) {
                    uvar.resetShape();
                    uvar.invalidateCache();
                }
            }
        }
        final long calcSize = this.dataStart + this.nonRecordDataSize + this.recsize * this.numrecs;
        if (calcSize > actualSize + 3L) {
            if (N3header.disallowFileTruncation) {
                throw new IOException("File is truncated calculated size= " + calcSize + " actual = " + actualSize);
            }
            raf.setExtendMode();
        }
    }
    
    long calcFileSize() {
        if (this.udim != null) {
            return this.recStart + this.recsize * this.numrecs;
        }
        return this.dataStart + this.nonRecordDataSize;
    }
    
    void showDetail(final Formatter out) throws IOException {
        final long actual = this.raf.length();
        out.format("  raf length= %s %n", actual);
        out.format("  isStreaming= %s %n", this.isStreaming);
        out.format("  useLongOffset= %s %n", this.useLongOffset);
        out.format("  dataStart= %d%n", this.dataStart);
        out.format("  nonRecordData size= %d %n", this.nonRecordDataSize);
        out.format("  unlimited dimension = %s %n", this.udim);
        if (this.udim != null) {
            out.format("  record Data starts = %d %n", this.recStart);
            out.format("  recsize = %d %n", this.recsize);
            out.format("  numrecs = %d %n", this.numrecs);
        }
        final long calcSize = this.calcFileSize();
        out.format("  computedSize = %d %n", calcSize);
        if (actual < calcSize) {
            out.format("  TRUNCATED!! actual size = %d (%d bytes) %n", actual, calcSize - actual);
        }
        else if (actual != calcSize) {
            out.format(" actual size larger = %d (%d byte extra) %n", actual, actual - calcSize);
        }
        out.format("%n  %20s____start_____size__unlim%n", "name");
        for (final Variable v : this.ncfile.getVariables()) {
            final Vinfo vinfo = (Vinfo)v.getSPobject();
            out.format("  %20s %8d %8d  %s %n", v.getShortName(), vinfo.begin, vinfo.vsize, vinfo.isRecord);
        }
    }
    
    synchronized boolean removeRecordStructure() {
        boolean found = false;
        for (final Variable v : this.uvars) {
            if (v.getName().equals("record")) {
                this.uvars.remove(v);
                this.ncfile.getRootGroup().getVariables().remove(v);
                found = true;
                break;
            }
        }
        this.ncfile.finish();
        return found;
    }
    
    synchronized boolean makeRecordStructure() {
        if (this.uvars.size() > 0) {
            final Structure recordStructure = new Structure(this.ncfile, this.ncfile.getRootGroup(), null, "record");
            recordStructure.setDimensions(this.udim.getName());
            for (final Variable v : this.uvars) {
                Variable memberV;
                try {
                    memberV = v.slice(0, 0);
                }
                catch (InvalidRangeException e) {
                    N3header.log.warn("N3header.makeRecordStructure cant slice variable " + v + " " + e.getMessage());
                    return false;
                }
                memberV.setParentStructure(recordStructure);
                recordStructure.addMemberVariable(memberV);
            }
            this.uvars.add(recordStructure);
            this.ncfile.getRootGroup().addVariable(recordStructure);
            this.ncfile.finish();
            return true;
        }
        return false;
    }
    
    private int readAtts(final List<Attribute> atts, final Formatter fout) throws IOException {
        int natts = 0;
        final int magic = this.raf.readInt();
        if (magic == 0) {
            this.raf.readInt();
        }
        else {
            if (magic != 12) {
                throw new IOException("Misformed netCDF file  - att magic number wrong");
            }
            natts = this.raf.readInt();
        }
        if (fout != null) {
            fout.format(" num atts= %d\n", natts);
        }
        for (int i = 0; i < natts; ++i) {
            if (fout != null) {
                fout.format("***att %d pos= %d\n", i, this.raf.getFilePointer());
            }
            final String name = this.readString();
            final int type = this.raf.readInt();
            Attribute att;
            if (type == 2) {
                if (fout != null) {
                    fout.format(" begin read String val pos= %d\n", this.raf.getFilePointer());
                }
                final String val = this.readString();
                if (fout != null) {
                    fout.format(" end read String val pos= %d\n", this.raf.getFilePointer());
                }
                att = new Attribute(name, val);
            }
            else {
                if (fout != null) {
                    fout.format(" begin read val pos= %d\n", this.raf.getFilePointer());
                }
                final int nelems = this.raf.readInt();
                final DataType dtype = this.getDataType(type);
                if (nelems == 0) {
                    att = new Attribute(name, dtype);
                }
                else {
                    final int[] shape = { nelems };
                    final Array arr = Array.factory(dtype.getPrimitiveClassType(), shape);
                    final IndexIterator ii = arr.getIndexIterator();
                    int nbytes = 0;
                    for (int j = 0; j < nelems; ++j) {
                        nbytes += this.readAttributeValue(dtype, ii);
                    }
                    att = new Attribute(name, arr);
                    this.skip(nbytes);
                }
                if (fout != null) {
                    fout.format(" end read val pos= %d\n", this.raf.getFilePointer());
                }
            }
            atts.add(att);
            if (fout != null) {
                fout.format("  %s\n", att);
            }
        }
        return natts;
    }
    
    private int readAttributeValue(final DataType type, final IndexIterator ii) throws IOException {
        if (type == DataType.BYTE) {
            final byte b = (byte)this.raf.read();
            ii.setByteNext(b);
            return 1;
        }
        if (type == DataType.CHAR) {
            final char c = (char)this.raf.read();
            ii.setCharNext(c);
            return 1;
        }
        if (type == DataType.SHORT) {
            final short s = this.raf.readShort();
            ii.setShortNext(s);
            return 2;
        }
        if (type == DataType.INT) {
            final int i = this.raf.readInt();
            ii.setIntNext(i);
            return 4;
        }
        if (type == DataType.FLOAT) {
            final float f = this.raf.readFloat();
            ii.setFloatNext(f);
            return 4;
        }
        if (type == DataType.DOUBLE) {
            final double d = this.raf.readDouble();
            ii.setDoubleNext(d);
            return 8;
        }
        return 0;
    }
    
    private String readString() throws IOException {
        final int nelems = this.raf.readInt();
        final byte[] b = new byte[nelems];
        this.raf.read(b);
        this.skip(nelems);
        int count;
        for (count = 0; count < nelems && b[count] != 0; ++count) {}
        return new String(b, 0, count, "UTF-8");
    }
    
    private void skip(final int nbytes) throws IOException {
        final int pad = padding(nbytes);
        if (pad > 0) {
            this.raf.seek(this.raf.getFilePointer() + pad);
        }
    }
    
    static int padding(final int nbytes) {
        int pad = nbytes % 4;
        if (pad != 0) {
            pad = 4 - pad;
        }
        return pad;
    }
    
    static int padding(final long nbytes) {
        int pad = (int)(nbytes % 4L);
        if (pad != 0) {
            pad = 4 - pad;
        }
        return pad;
    }
    
    private void printBytes(final int n, final Formatter fout) throws IOException {
        long pos;
        long savePos;
        for (savePos = (pos = this.raf.getFilePointer()); pos < savePos + n - 9L; pos += 10L) {
            fout.format("%d: ", pos);
            this._printBytes(10, fout);
        }
        if (pos < savePos + n) {
            fout.format("%d: ", pos);
            this._printBytes((int)(savePos + n - pos), fout);
        }
        this.raf.seek(savePos);
    }
    
    private void _printBytes(final int n, final Formatter fout) throws IOException {
        for (int i = 0; i < n; ++i) {
            final byte b = (byte)this.raf.read();
            final int ub = (b < 0) ? (b + 256) : b;
            fout.format(ub + "%d(%b) ", ub, b);
        }
        fout.format("\n", new Object[0]);
    }
    
    private DataType getDataType(final int type) {
        switch (type) {
            case 1: {
                return DataType.BYTE;
            }
            case 2: {
                return DataType.CHAR;
            }
            case 3: {
                return DataType.SHORT;
            }
            case 4: {
                return DataType.INT;
            }
            case 5: {
                return DataType.FLOAT;
            }
            case 6: {
                return DataType.DOUBLE;
            }
            default: {
                throw new IllegalArgumentException("unknown type == " + type);
            }
        }
    }
    
    static int getType(final DataType dt) {
        if (dt == DataType.BYTE) {
            return 1;
        }
        if (dt == DataType.CHAR || dt == DataType.STRING) {
            return 2;
        }
        if (dt == DataType.SHORT) {
            return 3;
        }
        if (dt == DataType.INT) {
            return 4;
        }
        if (dt == DataType.FLOAT) {
            return 5;
        }
        if (dt == DataType.DOUBLE) {
            return 6;
        }
        throw new IllegalArgumentException("unknown DataType == " + dt);
    }
    
    void create(final RandomAccessFile raf, final NetcdfFile ncfile, final int extra, final boolean largeFile, final Formatter fout) throws IOException {
        this.raf = raf;
        this.ncfile = ncfile;
        this.writeHeader(extra, largeFile, false, fout);
    }
    
    boolean rewriteHeader(final boolean largeFile, final Formatter fout) throws IOException {
        final int want = this.sizeHeader(largeFile);
        if (want > this.dataStart) {
            return false;
        }
        this.writeHeader(0, largeFile, true, fout);
        return true;
    }
    
    void writeHeader(final int extra, final boolean largeFile, final boolean keepDataStart, final Formatter fout) throws IOException {
        this.useLongOffset = largeFile;
        this.nonRecordDataSize = 0L;
        this.recsize = 0L;
        this.recStart = Long.MAX_VALUE;
        this.raf.seek(0L);
        this.raf.write(largeFile ? N3header.MAGIC_LONG : N3header.MAGIC);
        this.raf.writeInt(0);
        final List dims = this.ncfile.getDimensions();
        final int numdims = dims.size();
        if (numdims == 0) {
            this.raf.writeInt(0);
            this.raf.writeInt(0);
        }
        else {
            this.raf.writeInt(10);
            this.raf.writeInt(numdims);
        }
        for (int i = 0; i < numdims; ++i) {
            final Dimension dim = dims.get(i);
            if (fout != null) {
                fout.format("  dim %d pos %d\n", i, this.raf.getFilePointer());
            }
            this.writeString(dim.getName());
            this.raf.writeInt(dim.isUnlimited() ? 0 : dim.getLength());
            if (dim.isUnlimited()) {
                this.udim = dim;
            }
        }
        this.globalAttsPos = this.raf.getFilePointer();
        this.writeAtts(this.ncfile.getGlobalAttributes(), fout);
        final List<Variable> vars = this.ncfile.getVariables();
        this.writeVars(vars, largeFile, fout);
        if (!keepDataStart) {
            this.dataStart = this.raf.getFilePointer();
            if (extra > 0) {
                this.dataStart += extra;
            }
        }
        long pos = this.dataStart;
        for (final Variable var : vars) {
            final Vinfo vinfo = (Vinfo)var.getSPobject();
            if (!vinfo.isRecord) {
                this.raf.seek(vinfo.begin);
                if (largeFile) {
                    this.raf.writeLong(pos);
                }
                else {
                    if (pos > 2147483647L) {
                        throw new IllegalArgumentException("Variable starting pos=" + pos + " may not exceed " + Integer.MAX_VALUE);
                    }
                    this.raf.writeInt((int)pos);
                }
                vinfo.begin = pos;
                if (fout != null) {
                    fout.format("  %s begin at = %d end= %d\n", var.getName(), vinfo.begin, vinfo.begin + vinfo.vsize);
                }
                pos += vinfo.vsize;
                this.nonRecordDataSize = Math.max(this.nonRecordDataSize, vinfo.begin + vinfo.vsize);
            }
        }
        this.recStart = pos;
        for (final Variable var : vars) {
            final Vinfo vinfo = (Vinfo)var.getSPobject();
            if (vinfo.isRecord) {
                this.raf.seek(vinfo.begin);
                if (largeFile) {
                    this.raf.writeLong(pos);
                }
                else {
                    this.raf.writeInt((int)pos);
                }
                vinfo.begin = pos;
                if (fout != null) {
                    fout.format(" %s record begin at = %d\n", var.getName(), this.dataStart);
                }
                pos += vinfo.vsize;
                this.uvars.add(var);
                this.recsize += vinfo.vsize;
                this.recStart = Math.min(this.recStart, vinfo.begin);
            }
        }
        if (this.nonRecordDataSize > 0L) {
            this.nonRecordDataSize -= this.dataStart;
        }
        if (this.uvars.size() == 0) {
            this.recStart = 0L;
        }
    }
    
    int sizeHeader(final boolean largeFile) {
        int size = 4;
        size += 4;
        size += 8;
        for (final Dimension dim : this.ncfile.getDimensions()) {
            size += this.sizeString(dim.getName()) + 4;
        }
        size += this.sizeAtts(this.ncfile.getGlobalAttributes());
        size += 8;
        for (final Variable var : this.ncfile.getVariables()) {
            size += this.sizeString(var.getName());
            size += 4;
            size += 4 * var.getDimensions().size();
            size += this.sizeAtts(var.getAttributes());
            size += 8;
            size += (largeFile ? 8 : 4);
        }
        return size;
    }
    
    private void writeAtts(final List<Attribute> atts, final Formatter fout) throws IOException {
        final int n = atts.size();
        if (n == 0) {
            this.raf.writeInt(0);
            this.raf.writeInt(0);
        }
        else {
            this.raf.writeInt(12);
            this.raf.writeInt(n);
        }
        for (int i = 0; i < n; ++i) {
            if (fout != null) {
                fout.format("***att %d pos= %d\n", i, this.raf.getFilePointer());
            }
            final Attribute att = atts.get(i);
            this.writeString(att.getName());
            final int type = getType(att.getDataType());
            this.raf.writeInt(type);
            if (type == 2) {
                this.writeStringValues(att);
            }
            else {
                final int nelems = att.getLength();
                this.raf.writeInt(nelems);
                int nbytes = 0;
                for (int j = 0; j < nelems; ++j) {
                    nbytes += this.writeAttributeValue(att.getNumericValue(j));
                }
                this.pad(nbytes, (byte)0);
                if (fout != null) {
                    fout.format(" end write val pos= %d\n", this.raf.getFilePointer());
                }
            }
            if (fout != null) {
                fout.format("  %s\n", att);
            }
        }
    }
    
    private int sizeAtts(final List<Attribute> atts) {
        int size = 8;
        for (final Attribute att : atts) {
            size += this.sizeString(att.getName());
            size += 4;
            final int type = getType(att.getDataType());
            if (type == 2) {
                size += this.sizeStringValues(att);
            }
            else {
                size += 4;
                final int nelems = att.getLength();
                int nbytes = 0;
                for (int j = 0; j < nelems; ++j) {
                    nbytes += this.sizeAttributeValue(att.getNumericValue(j));
                }
                size += nbytes;
                size += padding(nbytes);
            }
        }
        return size;
    }
    
    private void writeStringValues(final Attribute att) throws IOException {
        final int n = att.getLength();
        if (n == 1) {
            this.writeString(att.getStringValue());
        }
        else {
            final StringBuilder values = new StringBuilder();
            for (int i = 0; i < n; ++i) {
                values.append(att.getStringValue(i));
            }
            this.writeString(values.toString());
        }
    }
    
    private int sizeStringValues(final Attribute att) {
        int size = 0;
        final int n = att.getLength();
        if (n == 1) {
            size += this.sizeString(att.getStringValue());
        }
        else {
            final StringBuilder values = new StringBuilder();
            for (int i = 0; i < n; ++i) {
                values.append(att.getStringValue(i));
            }
            size += this.sizeString(values.toString());
        }
        return size;
    }
    
    private int writeAttributeValue(final Number numValue) throws IOException {
        if (numValue instanceof Byte) {
            this.raf.write(numValue.byteValue());
            return 1;
        }
        if (numValue instanceof Short) {
            this.raf.writeShort(numValue.shortValue());
            return 2;
        }
        if (numValue instanceof Integer) {
            this.raf.writeInt(numValue.intValue());
            return 4;
        }
        if (numValue instanceof Float) {
            this.raf.writeFloat(numValue.floatValue());
            return 4;
        }
        if (numValue instanceof Double) {
            this.raf.writeDouble(numValue.doubleValue());
            return 8;
        }
        throw new IllegalStateException("unknown attribute type == " + numValue.getClass().getName());
    }
    
    private int sizeAttributeValue(final Number numValue) {
        if (numValue instanceof Byte) {
            return 1;
        }
        if (numValue instanceof Short) {
            return 2;
        }
        if (numValue instanceof Integer) {
            return 4;
        }
        if (numValue instanceof Float) {
            return 4;
        }
        if (numValue instanceof Double) {
            return 8;
        }
        throw new IllegalStateException("unknown attribute type == " + numValue.getClass().getName());
    }
    
    private void writeVars(final List<Variable> vars, final boolean largeFile, final Formatter fout) throws IOException {
        final int n = vars.size();
        if (n == 0) {
            this.raf.writeInt(0);
            this.raf.writeInt(0);
        }
        else {
            this.raf.writeInt(11);
            this.raf.writeInt(n);
        }
        final boolean usePadding = true;
        for (int i = 0; i < n; ++i) {
            final Variable var = vars.get(i);
            this.writeString(var.getName());
            long vsize = var.getDataType().getSize();
            final List<Dimension> dims = var.getDimensions();
            this.raf.writeInt(dims.size());
            for (final Dimension dim : dims) {
                final int dimIndex = this.findDimensionIndex(this.ncfile, dim);
                this.raf.writeInt(dimIndex);
                if (!dim.isUnlimited()) {
                    vsize *= dim.getLength();
                }
            }
            if (usePadding) {
                vsize += padding(vsize);
            }
            final long varAttsPos = this.raf.getFilePointer();
            this.writeAtts(var.getAttributes(), fout);
            final int type = getType(var.getDataType());
            this.raf.writeInt(type);
            final int vsizeWrite = (vsize < 4294967295L) ? ((int)vsize) : -1;
            this.raf.writeInt(vsizeWrite);
            final long pos = this.raf.getFilePointer();
            if (largeFile) {
                this.raf.writeLong(0L);
            }
            else {
                this.raf.writeInt(0);
            }
            var.setSPobject(new Vinfo(vsize, pos, var.isUnlimited(), varAttsPos));
        }
    }
    
    private void writeString(final String s) throws IOException {
        final byte[] b = s.getBytes("UTF-8");
        this.raf.writeInt(b.length);
        this.raf.write(b);
        this.pad(b.length, (byte)0);
    }
    
    private int sizeString(final String s) {
        final int size = s.length() + 4;
        return size + padding(s.length());
    }
    
    private int findDimensionIndex(final NetcdfFile ncfile, final Dimension wantDim) {
        final List<Dimension> dims = ncfile.getDimensions();
        for (int i = 0; i < dims.size(); ++i) {
            final Dimension dim = dims.get(i);
            if (dim.equals(wantDim)) {
                return i;
            }
        }
        throw new IllegalStateException("unknown Dimension == " + wantDim);
    }
    
    private void pad(final int nbytes, final byte fill) throws IOException {
        for (int pad = padding(nbytes), i = 0; i < pad; ++i) {
            this.raf.write(fill);
        }
    }
    
    void writeNumrecs() throws IOException {
        this.raf.seek(4L);
        this.raf.writeInt(this.numrecs);
    }
    
    void setNumrecs(final int n) throws IOException {
        this.numrecs = n;
    }
    
    synchronized boolean synchNumrecs() throws IOException {
        final int n = this.raf.readIntUnbuffered(4L);
        if (n == this.numrecs) {
            return false;
        }
        if (n < 0) {
            return false;
        }
        this.numrecs = n;
        this.udim.setLength(this.numrecs);
        for (final Variable uvar : this.uvars) {
            uvar.resetShape();
            uvar.invalidateCache();
        }
        return true;
    }
    
    void updateAttribute(final Variable v2, final Attribute att) throws IOException {
        long pos;
        if (v2 == null) {
            pos = this.findAtt(this.globalAttsPos, att.getName());
        }
        else {
            final Vinfo vinfo = (Vinfo)v2.getSPobject();
            pos = this.findAtt(vinfo.attsPos, att.getName());
        }
        this.raf.seek(pos);
        final int type = this.raf.readInt();
        final DataType have = this.getDataType(type);
        DataType want = att.getDataType();
        if (want == DataType.STRING) {
            want = DataType.CHAR;
        }
        if (want != have) {
            throw new IllegalArgumentException("Update Attribute must have same type or original = " + have);
        }
        if (type == 2) {
            final String s = att.getStringValue();
            final int org = this.raf.readInt();
            final int size = org + padding(org);
            final int max = Math.min(size, s.length());
            if (max > org) {
                this.raf.seek(pos + 4L);
                this.raf.writeInt(max);
            }
            final byte[] b = new byte[size];
            for (int i = 0; i < max; ++i) {
                b[i] = (byte)s.charAt(i);
            }
            this.raf.write(b);
        }
        else {
            final int nelems = this.raf.readInt();
            for (int max2 = Math.min(nelems, att.getLength()), j = 0; j < max2; ++j) {
                this.writeAttributeValue(att.getNumericValue(j));
            }
        }
    }
    
    private long findAtt(final long start_pos, final String want) throws IOException {
        this.raf.seek(start_pos + 4L);
        for (int natts = this.raf.readInt(), i = 0; i < natts; ++i) {
            final String name = this.readString();
            if (name.equals(want)) {
                return this.raf.getFilePointer();
            }
            final int type = this.raf.readInt();
            if (type == 2) {
                this.readString();
            }
            else {
                final int nelems = this.raf.readInt();
                final DataType dtype = this.getDataType(type);
                final int[] shape = { nelems };
                final Array arr = Array.factory(dtype.getPrimitiveClassType(), shape);
                final IndexIterator ii = arr.getIndexIterator();
                int nbytes = 0;
                for (int j = 0; j < nelems; ++j) {
                    nbytes += this.readAttributeValue(dtype, ii);
                }
                this.skip(nbytes);
            }
        }
        throw new IllegalArgumentException("no such attribute " + want);
    }
    
    private static void dump(final String filename) throws IOException {
        System.out.printf("Dump %s%n", filename);
        final RandomAccessFile raf = new RandomAccessFile(filename, "r");
        final NetcdfFile ncfile = new MyNetcdfFile();
        raf.order(0);
        final N3header headerParser = new N3header();
        headerParser.read(raf, ncfile, new Formatter(System.out));
        raf.close();
    }
    
    public static void main(final String[] args) throws IOException {
        dump("D:/work/csiro/testWrite.nc");
    }
    
    static {
        N3header.log = LoggerFactory.getLogger(N3header.class);
        MAGIC = new byte[] { 67, 68, 70, 1 };
        MAGIC_LONG = new byte[] { 67, 68, 70, 2 };
        N3header.disallowFileTruncation = false;
        N3header.debugHeaderSize = false;
    }
    
    static class Vinfo
    {
        long vsize;
        long begin;
        boolean isRecord;
        long attsPos;
        
        Vinfo(final long vsize, final long begin, final boolean isRecord, final long attsPos) {
            this.attsPos = 0L;
            this.vsize = vsize;
            this.begin = begin;
            this.isRecord = isRecord;
            this.attsPos = attsPos;
        }
    }
    
    private static class MyNetcdfFile extends NetcdfFile
    {
    }
}
