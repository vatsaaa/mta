// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.iosp.netcdf3;

import ucar.nc2.iosp.IOServiceProvider;

public class SPFactory
{
    private static Class spClass;
    private static boolean debug;
    
    public static IOServiceProvider getServiceProvider() {
        try {
            if (SPFactory.debug) {
                System.out.println("**********using Service Provider Class = " + SPFactory.spClass.getName());
            }
            return SPFactory.spClass.newInstance();
        }
        catch (InstantiationException e) {
            e.printStackTrace();
        }
        catch (IllegalAccessException e2) {
            e2.printStackTrace();
        }
        return null;
    }
    
    public static void setServiceProvider(final String spName) throws ClassNotFoundException, InstantiationException, IllegalAccessException {
        (SPFactory.spClass = Class.forName(spName)).newInstance();
        if (SPFactory.debug) {
            System.out.println("**********NetcCDF Service Provider Class set to = " + spName);
        }
    }
    
    static {
        SPFactory.spClass = N3raf.class;
        SPFactory.debug = false;
    }
}
