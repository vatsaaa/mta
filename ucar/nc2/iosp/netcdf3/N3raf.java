// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.iosp.netcdf3;

import ucar.ma2.IndexIterator;
import ucar.ma2.Array;
import java.nio.channels.WritableByteChannel;
import ucar.nc2.iosp.IospHelper;
import ucar.ma2.DataType;
import ucar.nc2.iosp.Layout;
import java.io.IOException;
import ucar.unidata.io.RandomAccessFile;

public class N3raf extends N3iosp
{
    @Override
    protected void _open(final RandomAccessFile raf) throws IOException {
    }
    
    @Override
    protected void _create(final RandomAccessFile raf) throws IOException {
    }
    
    @Override
    protected Object readData(final Layout index, final DataType dataType) throws IOException {
        return IospHelper.readDataFill(this.raf, index, dataType, null, -1);
    }
    
    @Override
    protected long readData(final Layout index, final DataType dataType, final WritableByteChannel out) throws IOException {
        long count = 0L;
        if (dataType == DataType.BYTE || dataType == DataType.CHAR || dataType == DataType.ENUM1) {
            while (index.hasNext()) {
                final Layout.Chunk chunk = index.next();
                count += this.raf.readToByteChannel(out, chunk.getSrcPos(), chunk.getNelems());
            }
        }
        else if (dataType == DataType.SHORT || dataType == DataType.ENUM2) {
            while (index.hasNext()) {
                final Layout.Chunk chunk = index.next();
                count += this.raf.readToByteChannel(out, chunk.getSrcPos(), 2 * chunk.getNelems());
            }
        }
        else if (dataType == DataType.INT || dataType == DataType.FLOAT || dataType == DataType.ENUM4) {
            while (index.hasNext()) {
                final Layout.Chunk chunk = index.next();
                count += this.raf.readToByteChannel(out, chunk.getSrcPos(), 4 * chunk.getNelems());
            }
        }
        else if (dataType == DataType.DOUBLE || dataType == DataType.LONG) {
            while (index.hasNext()) {
                final Layout.Chunk chunk = index.next();
                count += this.raf.readToByteChannel(out, chunk.getSrcPos(), 8 * chunk.getNelems());
            }
        }
        return count;
    }
    
    @Override
    protected void writeData(final Array values, final Layout index, final DataType dataType) throws IOException {
        if (dataType == DataType.BYTE || dataType == DataType.CHAR) {
            final IndexIterator ii = values.getIndexIterator();
            while (index.hasNext()) {
                final Layout.Chunk chunk = index.next();
                this.raf.seek(chunk.getSrcPos());
                for (int k = 0; k < chunk.getNelems(); ++k) {
                    this.raf.write(ii.getByteNext());
                }
            }
            return;
        }
        if (dataType == DataType.STRING) {
            final IndexIterator ii = values.getIndexIterator();
            while (index.hasNext()) {
                final Layout.Chunk chunk = index.next();
                this.raf.seek(chunk.getSrcPos());
                for (int k = 0; k < chunk.getNelems(); ++k) {
                    final String val = (String)ii.getObjectNext();
                    if (val != null) {
                        this.raf.write(val.getBytes("UTF-8"));
                    }
                }
            }
            return;
        }
        if (dataType == DataType.SHORT) {
            final IndexIterator ii = values.getIndexIterator();
            while (index.hasNext()) {
                final Layout.Chunk chunk = index.next();
                this.raf.seek(chunk.getSrcPos());
                for (int k = 0; k < chunk.getNelems(); ++k) {
                    this.raf.writeShort(ii.getShortNext());
                }
            }
            return;
        }
        if (dataType == DataType.INT) {
            final IndexIterator ii = values.getIndexIterator();
            while (index.hasNext()) {
                final Layout.Chunk chunk = index.next();
                this.raf.seek(chunk.getSrcPos());
                for (int k = 0; k < chunk.getNelems(); ++k) {
                    this.raf.writeInt(ii.getIntNext());
                }
            }
            return;
        }
        if (dataType == DataType.FLOAT) {
            final IndexIterator ii = values.getIndexIterator();
            while (index.hasNext()) {
                final Layout.Chunk chunk = index.next();
                this.raf.seek(chunk.getSrcPos());
                for (int k = 0; k < chunk.getNelems(); ++k) {
                    this.raf.writeFloat(ii.getFloatNext());
                }
            }
            return;
        }
        if (dataType == DataType.DOUBLE) {
            final IndexIterator ii = values.getIndexIterator();
            while (index.hasNext()) {
                final Layout.Chunk chunk = index.next();
                this.raf.seek(chunk.getSrcPos());
                for (int k = 0; k < chunk.getNelems(); ++k) {
                    this.raf.writeDouble(ii.getDoubleNext());
                }
            }
            return;
        }
        throw new IllegalStateException("dataType= " + dataType);
    }
}
