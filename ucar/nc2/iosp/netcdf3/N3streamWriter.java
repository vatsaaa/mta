// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.iosp.netcdf3;

import ucar.nc2.Attribute;
import java.util.Iterator;
import java.io.IOException;
import ucar.nc2.Structure;
import ucar.nc2.Dimension;
import java.io.DataOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import ucar.nc2.Variable;
import java.util.Map;
import ucar.nc2.NetcdfFile;

public abstract class N3streamWriter
{
    protected NetcdfFile ncfile;
    protected Map<Variable, Vinfo> vinfoMap;
    protected List<Vinfo> vinfoList;
    protected boolean debug;
    protected boolean debugPos;
    protected boolean debugWriteData;
    protected int recStart;
    protected int recSize;
    protected boolean usePadding;
    protected long filePos;
    
    protected N3streamWriter(final NetcdfFile ncfile) {
        this.vinfoMap = new HashMap<Variable, Vinfo>();
        this.vinfoList = new ArrayList<Vinfo>();
        this.debug = false;
        this.debugPos = false;
        this.debugWriteData = false;
        this.usePadding = true;
        this.filePos = 0L;
        this.ncfile = ncfile;
    }
    
    public void writeHeader(final DataOutputStream stream, int numrec) throws IOException {
        this.ncfile.finish();
        stream.write(N3header.MAGIC);
        int count = N3header.MAGIC.length;
        final Dimension udim = this.ncfile.getUnlimitedDimension();
        if (numrec < 0) {
            numrec = ((udim == null) ? 0 : -1);
        }
        stream.writeInt(numrec);
        count += 4;
        final List dims = this.ncfile.getDimensions();
        final int numdims = dims.size();
        if (numdims == 0) {
            stream.writeInt(0);
            stream.writeInt(0);
        }
        else {
            stream.writeInt(10);
            stream.writeInt(numdims);
        }
        count += 8;
        for (int i = 0; i < numdims; ++i) {
            final Dimension dim = dims.get(i);
            count += this.writeString(stream, N3iosp.makeValidNetcdfObjectName(dim.getName()));
            stream.writeInt(dim.isUnlimited() ? 0 : dim.getLength());
            count += 4;
        }
        count += this.writeAtts(stream, this.ncfile.getGlobalAttributes());
        if (this.debug) {
            System.out.println("vars header starts at " + count);
        }
        final List<Variable> vars = this.ncfile.getVariables();
        final int nvars = vars.size();
        if (nvars == 0) {
            stream.writeInt(0);
            stream.writeInt(0);
        }
        else {
            stream.writeInt(11);
            stream.writeInt(nvars);
        }
        count += 8;
        for (int j = 0; j < nvars; ++j) {
            final Variable var = vars.get(j);
            if (!(var instanceof Structure)) {
                final Vinfo vinfo = this.writeVar(null, var, 0);
                count += vinfo.hsize;
            }
        }
        int offset;
        final int dataStart = offset = count;
        if (this.debug) {
            System.out.println(" non-record vars start at " + dataStart);
        }
        for (int k = 0; k < nvars; ++k) {
            final Variable var2 = vars.get(k);
            if (!var2.isUnlimited()) {
                final Vinfo vinfo2 = this.writeVar(stream, var2, offset);
                this.vinfoMap.put(var2, vinfo2);
                if (this.debugPos) {
                    System.out.println(" " + var2.getNameAndDimensions() + " begin at = " + offset + " end=" + (offset + vinfo2.vsize));
                }
                offset += vinfo2.vsize;
                this.vinfoList.add(vinfo2);
            }
        }
        if (this.debug) {
            System.out.println(" record vars start at " + offset);
        }
        this.recStart = offset;
        this.recSize = 0;
        for (int k = 0; k < nvars; ++k) {
            final Variable var2 = vars.get(k);
            if (var2.isUnlimited()) {
                if (!(var2 instanceof Structure)) {
                    final Vinfo vinfo2 = this.writeVar(stream, var2, offset);
                    this.vinfoMap.put(var2, vinfo2);
                    if (this.debugPos) {
                        System.out.println(" " + var2.getNameAndDimensions() + "(record) begin at = " + offset + " end=" + (offset + vinfo2.vsize) + " size=" + vinfo2.vsize);
                    }
                    offset += vinfo2.vsize;
                    this.recSize += vinfo2.vsize;
                    this.vinfoList.add(vinfo2);
                }
            }
        }
        this.filePos = count;
        if (this.debugPos) {
            System.out.println("header written filePos= " + this.filePos + " recsize= " + this.recSize);
        }
    }
    
    private Vinfo writeVar(final DataOutputStream stream, final Variable var, final int offset) throws IOException {
        int hsize = 0;
        hsize += this.writeString(stream, N3iosp.makeValidNetcdfObjectName(var.getName()));
        int vsize = var.getDataType().getSize();
        final List<Dimension> dims = var.getDimensions();
        if (null != stream) {
            stream.writeInt(dims.size());
        }
        hsize += 4;
        for (final Dimension dim : dims) {
            final int dimIndex = this.findDimensionIndex(dim);
            if (null != stream) {
                stream.writeInt(dimIndex);
            }
            hsize += 4;
            if (!dim.isUnlimited()) {
                vsize *= dim.getLength();
            }
        }
        final int pad = this.usePadding ? N3header.padding(vsize) : 0;
        vsize += pad;
        hsize += this.writeAtts(stream, var.getAttributes());
        final int type = N3header.getType(var.getDataType());
        if (null != stream) {
            stream.writeInt(type);
            stream.writeInt(vsize);
            stream.writeInt(offset);
        }
        hsize += 12;
        return new Vinfo(var, hsize, vsize, offset, pad, var.isUnlimited());
    }
    
    private int writeAtts(final DataOutputStream stream, final List<Attribute> atts) throws IOException {
        final int natts = atts.size();
        if (null != stream) {
            if (natts == 0) {
                stream.writeInt(0);
                stream.writeInt(0);
            }
            else {
                stream.writeInt(12);
                stream.writeInt(natts);
            }
        }
        int hsize = 8;
        for (int i = 0; i < natts; ++i) {
            final Attribute att = atts.get(i);
            hsize += this.writeString(stream, N3iosp.makeValidNetcdfObjectName(att.getName()));
            final int type = N3header.getType(att.getDataType());
            if (null != stream) {
                stream.writeInt(type);
            }
            hsize += 4;
            if (type == 2) {
                hsize += this.writeStringValues(stream, att);
            }
            else {
                final int nelems = att.getLength();
                if (null != stream) {
                    stream.writeInt(nelems);
                }
                hsize += 4;
                int nbytes = 0;
                for (int j = 0; j < nelems; ++j) {
                    nbytes += this.writeAttributeValue(stream, att.getNumericValue(j));
                }
                hsize += nbytes;
                hsize += this.pad(stream, nbytes, (byte)0);
            }
        }
        return hsize;
    }
    
    private int writeStringValues(final DataOutputStream stream, final Attribute att) throws IOException {
        final int n = att.getLength();
        if (n == 1) {
            return this.writeString(stream, att.getStringValue());
        }
        final StringBuilder values = new StringBuilder();
        for (int i = 0; i < n; ++i) {
            values.append(att.getStringValue(i));
        }
        return this.writeString(stream, values.toString());
    }
    
    private int writeAttributeValue(final DataOutputStream stream, final Number numValue) throws IOException {
        if (numValue instanceof Byte) {
            if (null != stream) {
                stream.write(numValue.byteValue());
            }
            return 1;
        }
        if (numValue instanceof Short) {
            if (null != stream) {
                stream.writeShort(numValue.shortValue());
            }
            return 2;
        }
        if (numValue instanceof Integer) {
            if (null != stream) {
                stream.writeInt(numValue.intValue());
            }
            return 4;
        }
        if (numValue instanceof Float) {
            if (null != stream) {
                stream.writeFloat(numValue.floatValue());
            }
            return 4;
        }
        if (numValue instanceof Double) {
            if (null != stream) {
                stream.writeDouble(numValue.doubleValue());
            }
            return 8;
        }
        throw new IllegalStateException("unknown attribute type == " + numValue.getClass().getName());
    }
    
    private int writeString(final DataOutputStream stream, final String s) throws IOException {
        final byte[] b = s.getBytes();
        if (null != stream) {
            stream.writeInt(b.length);
            stream.write(b);
        }
        final int n = this.pad(stream, b.length, (byte)0);
        return n + 4 + b.length;
    }
    
    private int findDimensionIndex(final Dimension wantDim) {
        final List dims = this.ncfile.getDimensions();
        for (int i = 0; i < dims.size(); ++i) {
            final Dimension dim = dims.get(i);
            if (dim.equals(wantDim)) {
                return i;
            }
        }
        throw new IllegalStateException("unknown Dimension == " + wantDim);
    }
    
    protected int pad(final DataOutputStream stream, final int nbytes, final byte fill) throws IOException {
        final int pad = N3header.padding(nbytes);
        if (null != stream) {
            for (int i = 0; i < pad; ++i) {
                stream.write(fill);
            }
        }
        return pad;
    }
    
    protected static class Vinfo
    {
        Variable v;
        int hsize;
        int vsize;
        int offset;
        int pad;
        boolean isRecord;
        
        Vinfo(final Variable v, final int hsize, final int vsize, final int offset, final int pad, final boolean isRecord) {
            this.v = v;
            this.hsize = hsize;
            this.vsize = vsize;
            this.offset = offset;
            this.pad = pad;
            this.isRecord = isRecord;
        }
        
        @Override
        public String toString() {
            return this.v.getName() + " vsize= " + this.vsize + " pad=" + this.pad;
        }
    }
}
