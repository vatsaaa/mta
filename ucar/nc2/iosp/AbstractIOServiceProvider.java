// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.iosp;

import ucar.ma2.StructureDataIterator;
import ucar.nc2.Structure;
import ucar.nc2.ParsedSectionSpec;
import ucar.ma2.InvalidRangeException;
import ucar.ma2.Array;
import java.nio.channels.WritableByteChannel;
import ucar.ma2.Section;
import ucar.nc2.Variable;
import java.io.IOException;
import ucar.nc2.util.CancelTask;
import ucar.nc2.NetcdfFile;
import ucar.unidata.io.RandomAccessFile;

public abstract class AbstractIOServiceProvider implements IOServiceProvider
{
    protected RandomAccessFile raf;
    
    public static String createValidNetcdfObjectName(final String name) {
        return name;
    }
    
    public void open(final RandomAccessFile raf, final NetcdfFile ncfile, final CancelTask cancelTask) throws IOException {
        this.raf = raf;
    }
    
    public void close() throws IOException {
        if (this.raf != null) {
            this.raf.close();
        }
        this.raf = null;
    }
    
    public long readToByteChannel(final Variable v2, final Section section, final WritableByteChannel channel) throws IOException, InvalidRangeException {
        final Array data = this.readData(v2, section);
        return IospHelper.copyToByteChannel(data, channel);
    }
    
    public Array readSection(final ParsedSectionSpec cer) throws IOException, InvalidRangeException {
        return IospHelper.readSection(cer);
    }
    
    public StructureDataIterator getStructureIterator(final Structure s, final int bufferSize) throws IOException {
        return null;
    }
    
    public Object sendIospMessage(final Object message) {
        if (message == "RandomAccessFile") {
            return this.raf;
        }
        return null;
    }
    
    public boolean syncExtend() throws IOException {
        return false;
    }
    
    public boolean sync() throws IOException {
        return false;
    }
    
    public String toStringDebug(final Object o) {
        return "";
    }
    
    public String getDetailInfo() {
        return "";
    }
    
    public String getFileTypeVersion() {
        return "N/A";
    }
}
