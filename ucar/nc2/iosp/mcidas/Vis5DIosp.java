// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.iosp.mcidas;

import java.util.Date;
import ucar.grid.GridParameter;
import ucar.grid.GridRecord;
import ucar.ma2.Index;
import visad.data.vis5d.Vis5DCoordinateSystem;
import ucar.grid.GridTableLookup;
import ucar.grid.GridDefRecord;
import ucar.nc2.iosp.grid.GridHorizCoordSys;
import visad.VisADException;
import visad.data.vis5d.Vis5DVerticalSystem;
import ucar.ma2.ArrayFloat;
import ucar.nc2.iosp.IOServiceProvider;
import ucar.nc2.FileWriter;
import ucar.ma2.InvalidRangeException;
import ucar.ma2.IndexIterator;
import ucar.ma2.Range;
import ucar.ma2.Section;
import ucar.ma2.Array;
import visad.Set;
import ucar.ma2.ArrayInt;
import ucar.ma2.ArrayDouble;
import ucar.nc2.constants.AxisType;
import ucar.nc2.Attribute;
import ucar.ma2.DataType;
import ucar.nc2.Structure;
import ucar.nc2.Group;
import ucar.nc2.Dimension;
import ucar.nc2.util.CancelTask;
import java.io.IOException;
import visad.data.BadFormException;
import ucar.nc2.NetcdfFile;
import ucar.unidata.io.RandomAccessFile;
import ucar.nc2.Variable;
import java.util.Hashtable;
import ucar.nc2.iosp.AbstractIOServiceProvider;

public class Vis5DIosp extends AbstractIOServiceProvider
{
    private V5DStruct v5dstruct;
    private static final String V5D = "V5D";
    private static final int MAXVARS = 200;
    private static final int MAXTIMES = 400;
    private static final int MAXROWS = 400;
    private static final int MAXCOLUMNS = 400;
    private static final int MAXLEVELS = 400;
    private static final String ROW = "row";
    private static final String COLUMN = "col";
    private static final String LEVEL = "lev";
    private static final String TIME = "time";
    private static final String LAT = "lat";
    private static final String LON = "lon";
    private final int MAXPROJARGS = 801;
    private final int MAXVERTARGS = 401;
    private static Hashtable<String, String> unitTable;
    private static Hashtable<Variable, Integer> varTable;
    private RandomAccessFile raf;
    private NetcdfFile ncfile;
    
    public boolean isValidFile(final RandomAccessFile raf) throws IOException {
        raf.order(0);
        raf.seek(0L);
        final int n = "V5D".length();
        final byte[] b = new byte[n];
        raf.read(b);
        final String got = new String(b);
        if (got.equals("V5D")) {
            return true;
        }
        V5DStruct vv = null;
        try {
            vv = V5DStruct.v5dOpenFile(raf);
        }
        catch (BadFormException bfe) {
            vv = null;
        }
        return vv != null;
    }
    
    public String getFileTypeId() {
        return "Vis5D";
    }
    
    public String getFileTypeDescription() {
        return "Vis5D grid file";
    }
    
    @Override
    public void open(final RandomAccessFile raf, final NetcdfFile ncfile, final CancelTask cancelTask) throws IOException {
        final long start = System.currentTimeMillis();
        this.raf = raf;
        this.ncfile = ncfile;
        if (Vis5DIosp.unitTable == null) {
            initUnitTable();
        }
        if (this.v5dstruct == null) {
            this.makeFile(raf, ncfile, cancelTask);
        }
        final long end = System.currentTimeMillis() - start;
    }
    
    private void makeFile(final RandomAccessFile raf, final NetcdfFile ncfile, final CancelTask cancelTask) throws IOException {
        ncfile.empty();
        final int[] sizes = new int[5];
        final int[] map_proj = { 0 };
        final String[] varnames = new String[200];
        final String[] varunits = new String[200];
        final int[] n_levels = new int[200];
        final int[] vert_sys = { 0 };
        final float[] vertargs = new float[401];
        final double[] times = new double[400];
        final float[] projargs = new float[801];
        final V5DStruct vv = null;
        try {
            this.v5dstruct = V5DStruct.v5d_open(raf, sizes, n_levels, varnames, varunits, map_proj, projargs, vert_sys, vertargs, times);
        }
        catch (BadFormException bfe) {
            throw new IOException("Vis5DIosp.makeFile: bad file " + bfe.getMessage());
        }
        if (sizes[0] < 1) {
            throw new IOException("Vis5DIosp.makeFile: bad file");
        }
        final int nr = sizes[0];
        final int nc = sizes[1];
        final int nl = sizes[2];
        final int ntimes = sizes[3];
        final int nvars = sizes[4];
        final Dimension time = new Dimension("time", ntimes, true);
        final Dimension row = new Dimension("row", nr, true);
        final Dimension col = new Dimension("col", nc, true);
        ncfile.addDimension(null, time);
        ncfile.addDimension(null, row);
        ncfile.addDimension(null, col);
        final Variable timeVar = new Variable(ncfile, null, null, "time");
        timeVar.setDataType(DataType.DOUBLE);
        timeVar.setDimensions("time");
        timeVar.addAttribute(new Attribute("units", "seconds since 1900-01-01 00:00:00"));
        timeVar.addAttribute(new Attribute("long_name", "time"));
        timeVar.addAttribute(new Attribute("_CoordinateAxisType", AxisType.Time.toString()));
        Array varArray = new ArrayDouble.D1(ntimes);
        for (int i = 0; i < ntimes; ++i) {
            ((ArrayDouble.D1)varArray).set(i, times[i]);
        }
        timeVar.setCachedData(varArray, false);
        ncfile.addVariable(null, timeVar);
        final Variable rowVar = new Variable(ncfile, null, null, "row");
        rowVar.setDataType(DataType.INT);
        rowVar.setDimensions("row");
        varArray = new ArrayInt.D1(nr);
        for (int j = 0; j < nr; ++j) {
            ((ArrayInt.D1)varArray).set(j, j);
        }
        rowVar.setCachedData(varArray, false);
        ncfile.addVariable(null, rowVar);
        final Variable colVar = new Variable(ncfile, null, null, "col");
        colVar.setDataType(DataType.INT);
        colVar.setDimensions("col");
        varArray = new ArrayInt.D1(nc);
        for (int k = 0; k < nc; ++k) {
            ((ArrayInt.D1)varArray).set(k, k);
        }
        colVar.setCachedData(varArray, false);
        ncfile.addVariable(null, colVar);
        final Hashtable<Integer, Object> var_table = new Hashtable<Integer, Object>();
        boolean have3D = false;
        for (final int nlevs : n_levels) {
            if (!have3D && nlevs > 1) {
                have3D = true;
            }
            var_table.put(new Integer(nlevs), new Object());
        }
        final int n_var_groups = var_table.size();
        if (n_var_groups > 2) {
            throw new IOException("Vis5DIosp.makeFile: more than two variable groups by n_levels");
        }
        if (n_var_groups == 0) {
            throw new IOException("Vis5DIosp.makeFile: number of variable groups == 0");
        }
        Variable vert = null;
        if (have3D) {
            final Dimension lev = new Dimension("lev", nl, true);
            ncfile.addDimension(null, lev);
            vert = this.makeVerticalVariable(vert_sys[0], nl, vertargs);
            if (vert != null) {
                ncfile.addVariable(null, vert);
            }
        }
        Vis5DIosp.varTable = new Hashtable<Variable, Integer>();
        final String dim3D = "time lev col row";
        final String dim2D = "time col row";
        String coords3D = "unknown";
        if (vert != null) {
            coords3D = "time Height lat lon";
        }
        final String coords2D = "time lat lon";
        for (int m = 0; m < nvars; ++m) {
            final Variable v = new Variable(ncfile, null, null, varnames[m]);
            if (n_levels[m] > 1) {
                v.setDimensions(dim3D);
                v.addAttribute(new Attribute("coordinates", coords3D));
            }
            else {
                v.setDimensions(dim2D);
                v.addAttribute(new Attribute("coordinates", coords2D));
            }
            v.setDataType(DataType.FLOAT);
            String units = varunits[m].trim();
            if (units.equals("")) {
                final String key = varnames[m].trim().toLowerCase();
                units = Vis5DIosp.unitTable.get(key);
            }
            if (units != null) {
                v.addAttribute(new Attribute("units", units));
            }
            if (Vis5DIosp.varTable.get(v) == null) {
                Vis5DIosp.varTable.put(v, new Integer(m));
                ncfile.addVariable(null, v);
            }
        }
        final double[][] proj_args = Set.floatToDouble(new float[][] { projargs });
        this.addLatLonVariables(map_proj[0], proj_args[0], nr, nc);
        final Vis5DGridDefRecord gridDef = new Vis5DGridDefRecord(map_proj[0], proj_args[0], nr, nc);
        ncfile.addAttribute(null, new Attribute("Conventions", "CF-1.0"));
        ncfile.finish();
    }
    
    public Array readData(final Variable v2, final Section section) throws IOException, InvalidRangeException {
        final long startTime = System.currentTimeMillis();
        final Integer varIdx = Vis5DIosp.varTable.get(v2);
        if (varIdx == null) {
            throw new IOException("unable to find variable index");
        }
        int count = 0;
        final int[] shape = v2.getShape();
        final boolean haveZ = shape.length == 4;
        final int nt = shape[count++];
        final int nz = haveZ ? shape[count++] : 1;
        final int ny = shape[count++];
        final int nx = shape[count];
        count = 0;
        final Array dataArray = Array.factory(DataType.FLOAT, section.getShape());
        final Range timeRange = section.getRange(count++);
        final Range zRange = haveZ ? section.getRange(count++) : null;
        final Range yRange = section.getRange(count++);
        final Range xRange = section.getRange(count);
        final int grid_size = nx * ny * nz;
        final IndexIterator ii = dataArray.getIndexIterator();
        for (int timeIdx = timeRange.first(); timeIdx <= timeRange.last(); timeIdx += timeRange.stride()) {
            float[] data = new float[grid_size];
            final float[] ranges = new float[2];
            try {
                this.v5dstruct.v5d_read(timeIdx, varIdx, ranges, data);
            }
            catch (BadFormException bfe) {
                throw new IOException("Vis5DIosp.readData: " + bfe.getMessage());
            }
            if (ranges[0] < 9.9E29 || ranges[1] > -9.9E29) {
                if (ranges[0] > ranges[1]) {
                    throw new IOException("Vis5DIosp.readData: bad read " + v2.getName());
                }
            }
            final float[] tmp_data = new float[grid_size];
            if (zRange == null) {
                int cnt = 0;
                for (int mm = 0; mm < ny; ++mm) {
                    int start = (mm + 1) * nx - 1;
                    for (int nn = 0; nn < nx; ++nn) {
                        tmp_data[cnt++] = data[start--];
                    }
                }
            }
            else {
                int cnt = 0;
                for (int ll = 0; ll < nz; ++ll) {
                    for (int mm2 = 0; mm2 < ny; ++mm2) {
                        int start2 = (mm2 + 1) * nx - 1 + nx * ny * ll;
                        for (int nn2 = 0; nn2 < nx; ++nn2) {
                            tmp_data[cnt++] = data[start2--];
                        }
                    }
                }
            }
            data = tmp_data;
            if (zRange != null) {
                for (int z = zRange.first(); z <= zRange.last(); z += zRange.stride()) {
                    for (int y = yRange.first(); y <= yRange.last(); y += yRange.stride()) {
                        for (int x = xRange.first(); x <= xRange.last(); x += xRange.stride()) {
                            final int index = z * nx * ny + y * nx + x;
                            ii.setFloatNext(data[index]);
                        }
                    }
                }
            }
            else {
                for (int y2 = yRange.first(); y2 <= yRange.last(); y2 += yRange.stride()) {
                    for (int x2 = xRange.first(); x2 <= xRange.last(); x2 += xRange.stride()) {
                        final int index2 = y2 * nx + x2;
                        ii.setFloatNext(data[index2]);
                    }
                }
            }
        }
        final long end = System.currentTimeMillis() - startTime;
        return dataArray;
    }
    
    @Override
    public void close() throws IOException {
        if (this.v5dstruct != null) {
            this.v5dstruct = null;
        }
    }
    
    public static void main(final String[] args) throws IOException {
        final IOServiceProvider areaiosp = new Vis5DIosp();
        final RandomAccessFile rf = new RandomAccessFile(args[0], "r", 2048);
        final NetcdfFile ncfile = new MakeNetcdfFile(areaiosp, rf, args[0], null);
        System.out.println(ncfile);
        if (args.length > 1) {
            FileWriter.writeToFile(ncfile, args[1]);
        }
    }
    
    private static void initUnitTable() {
        (Vis5DIosp.unitTable = new Hashtable<String, String>()).put("t", "K");
        Vis5DIosp.unitTable.put("td", "K");
        Vis5DIosp.unitTable.put("thte", "K");
        Vis5DIosp.unitTable.put("u", "m/s");
        Vis5DIosp.unitTable.put("v", "m/s");
        Vis5DIosp.unitTable.put("w", "m/s");
        Vis5DIosp.unitTable.put("p", "hPa");
        Vis5DIosp.unitTable.put("mmsl", "hPa");
        Vis5DIosp.unitTable.put("rh", "%");
        Vis5DIosp.unitTable.put("rhfz", "%");
        Vis5DIosp.unitTable.put("zagl", "m");
    }
    
    private Variable makeVerticalVariable(final int vert_sys, final int n_levels, final float[] vert_args) throws IOException {
        String vert_unit = null;
        final ArrayFloat.D1 data = new ArrayFloat.D1(n_levels);
        AxisType axisType = null;
        String vert_type = null;
        switch (vert_sys) {
            case 0: {
                vert_unit = null;
                vert_type = "height";
                break;
            }
            case 1:
            case 2: {
                vert_unit = "km";
                vert_type = "altitude";
                axisType = AxisType.Height;
                break;
            }
            case 3: {
                vert_unit = "mbar";
                vert_type = "pressure";
                axisType = AxisType.Pressure;
                break;
            }
            default: {
                throw new IOException("vert_sys unknown");
            }
        }
        final Variable vertVar = new Variable(this.ncfile, null, null, vert_type);
        vertVar.setDimensions("lev");
        vertVar.setDataType(DataType.FLOAT);
        if (vert_unit != null) {
            vertVar.addAttribute(new Attribute("units", vert_unit));
        }
        if (axisType != null) {
            vertVar.addAttribute(new Attribute("_CoordinateAxisType", axisType.toString()));
        }
        Label_0395: {
            switch (vert_sys) {
                case 0:
                case 1: {
                    for (int i = 0; i < n_levels; ++i) {
                        data.set(i, vert_args[0] + vert_args[1] * i);
                    }
                    break Label_0395;
                }
                case 2: {
                    for (int i = 0; i < n_levels; ++i) {
                        data.set(i, vert_args[i]);
                    }
                    break Label_0395;
                }
                case 3: {
                    try {
                        final Vis5DVerticalSystem.Vis5DVerticalCoordinateSystem vert_cs = new Vis5DVerticalSystem.Vis5DVerticalCoordinateSystem();
                        float[][] pressures = new float[1][n_levels];
                        System.arraycopy(vert_args, 0, pressures[0], 0, n_levels);
                        for (int j = 0; j < n_levels; ++j) {
                            final float[] array = pressures[0];
                            final int n = j;
                            array[n] *= 1000.0f;
                        }
                        pressures = vert_cs.fromReference(pressures);
                        for (int j = 0; j < n_levels; ++j) {
                            data.set(j, pressures[0][j]);
                        }
                        break Label_0395;
                    }
                    catch (VisADException ve) {
                        throw new IOException("unable to make vertical system");
                    }
                    break;
                }
            }
            throw new IOException("vert_sys unknown");
        }
        vertVar.setCachedData(data, false);
        return vertVar;
    }
    
    private void addLatLonVariables(final int map_proj, final double[] proj_args, final int nr, final int nc) throws IOException {
        final Vis5DGridDefRecord vgd = new Vis5DGridDefRecord(map_proj, proj_args, nr, nc);
        final GridHorizCoordSys ghc = new GridHorizCoordSys(vgd, (GridTableLookup)new Vis5DLookup(), null);
        try {
            final Vis5DCoordinateSystem coord_sys = new Vis5DCoordinateSystem(map_proj, proj_args, (double)nr, (double)nc);
            final Variable lat = new Variable(this.ncfile, null, null, "lat");
            lat.setDimensions("col row");
            lat.setDataType(DataType.DOUBLE);
            lat.addAttribute(new Attribute("long_name", "latitude"));
            lat.addAttribute(new Attribute("units", "degrees_north"));
            lat.addAttribute(new Attribute("standard_name", "latitude"));
            lat.addAttribute(new Attribute("_CoordinateAxisType", AxisType.Lat.toString()));
            this.ncfile.addVariable(null, lat);
            final Variable lon = new Variable(this.ncfile, null, null, "lon");
            lon.setDimensions("col row");
            lon.setDataType(DataType.DOUBLE);
            lon.addAttribute(new Attribute("units", "degrees_east"));
            lon.addAttribute(new Attribute("long_name", "longitude"));
            lon.addAttribute(new Attribute("standard_name", "longitude"));
            lon.addAttribute(new Attribute("_CoordinateAxisType", AxisType.Lon.toString()));
            this.ncfile.addVariable(null, lon);
            final int[] shape = { nc, nr };
            final Array latArray = Array.factory(DataType.DOUBLE, shape);
            final Array lonArray = Array.factory(DataType.DOUBLE, shape);
            final double[][] rowcol = new double[2][nr * nc];
            for (int x = 0; x < nc; ++x) {
                for (int y = 0; y < nr; ++y) {
                    final int index = x * nr + y;
                    rowcol[0][index] = y;
                    rowcol[1][index] = x;
                }
            }
            final double[][] latlon = coord_sys.toReference(rowcol);
            final Index latIndex = latArray.getIndex();
            final Index lonIndex = lonArray.getIndex();
            for (int x2 = 0; x2 < nc; ++x2) {
                for (int y2 = 0; y2 < nr; ++y2) {
                    final int index2 = x2 * nr + y2;
                    latArray.setDouble(index2, latlon[0][index2]);
                    lonArray.setDouble(index2, latlon[1][index2]);
                }
            }
            lat.setCachedData(latArray, false);
            lon.setCachedData(lonArray, false);
        }
        catch (VisADException ve) {
            throw new IOException("Vis5DIosp.addLatLon: " + ve.getMessage());
        }
    }
    
    static {
        Vis5DIosp.unitTable = null;
    }
    
    protected static class MakeNetcdfFile extends NetcdfFile
    {
        MakeNetcdfFile(final IOServiceProvider spi, final RandomAccessFile raf, final String location, final CancelTask cancelTask) throws IOException {
            super(spi, raf, location, cancelTask);
        }
    }
    
    public class Vis5DLookup implements GridTableLookup
    {
        public String getShapeName(final GridDefRecord gds) {
            return "Spherical";
        }
        
        public final String getGridName(final GridDefRecord gds) {
            return gds.toString();
        }
        
        public final GridParameter getParameter(final GridRecord gr) {
            return null;
        }
        
        public final String getDisciplineName(final GridRecord gr) {
            return "Meteorological Products";
        }
        
        public final String getCategoryName(final GridRecord gr) {
            return "Meteorological Parameters";
        }
        
        public final String getLevelName(final GridRecord gr) {
            return null;
        }
        
        public final String getLevelDescription(final GridRecord gr) {
            return null;
        }
        
        public final String getLevelUnit(final GridRecord gr) {
            return null;
        }
        
        public final String getTimeRangeUnitName(final int tunit) {
            return "second";
        }
        
        public final Date getFirstBaseTime() {
            return new Date();
        }
        
        public final boolean isLatLon(final GridDefRecord gds) {
            return this.getProjectionName(gds).equals("GENERIC") || this.getProjectionName(gds).equals("LINEAR") || this.getProjectionName(gds).equals("CYLINDRICAL") || this.getProjectionName(gds).equals("SPHERICAL");
        }
        
        public final int getProjectionType(final GridDefRecord gds) {
            final String name = this.getProjectionName(gds).trim();
            if (name.equals("LAMBERT")) {
                return 2;
            }
            if (name.equals("STEREO")) {
                return 1;
            }
            return -1;
        }
        
        public final boolean isVerticalCoordinate(final GridRecord gr) {
            return false;
        }
        
        public final boolean isPositiveUp(final GridRecord gr) {
            return false;
        }
        
        public final float getFirstMissingValue() {
            return -9999.0f;
        }
        
        public boolean isLayer(final GridRecord gr) {
            return false;
        }
        
        private String getProjectionName(final GridDefRecord gds) {
            return gds.getParam("ProjFlag");
        }
        
        public final String getTitle() {
            return "GRID data";
        }
        
        public String getInstitution() {
            return null;
        }
        
        public final String getSource() {
            return null;
        }
        
        public final String getComment() {
            return null;
        }
        
        public String getGridType() {
            return "Vis5D";
        }
    }
}
