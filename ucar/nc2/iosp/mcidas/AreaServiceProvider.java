// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.iosp.mcidas;

import ucar.nc2.iosp.IOServiceProvider;
import ucar.nc2.FileWriter;
import ucar.ma2.InvalidRangeException;
import ucar.ma2.Array;
import ucar.ma2.Section;
import ucar.nc2.Variable;
import ucar.nc2.util.CancelTask;
import ucar.nc2.NetcdfFile;
import java.io.IOException;
import ucar.unidata.io.RandomAccessFile;
import ucar.nc2.iosp.AbstractIOServiceProvider;

public class AreaServiceProvider extends AbstractIOServiceProvider
{
    protected AreaReader areaReader;
    
    public boolean isValidFile(final RandomAccessFile raf) throws IOException {
        return AreaReader.isValidFile(raf);
    }
    
    public String getFileTypeId() {
        return "McIDASArea";
    }
    
    public String getFileTypeDescription() {
        return "McIDAS area file";
    }
    
    @Override
    public void open(final RandomAccessFile raf, final NetcdfFile ncfile, final CancelTask cancelTask) throws IOException {
        final long start = System.currentTimeMillis();
        if (this.areaReader == null) {
            this.areaReader = new AreaReader();
        }
        try {
            this.areaReader.init(raf, ncfile);
        }
        catch (Exception ex) {}
        final long end = System.currentTimeMillis() - start;
    }
    
    public Array readData(final Variable v2, final Section section) throws IOException, InvalidRangeException {
        final long start = System.currentTimeMillis();
        final Array array = this.areaReader.readVariable(v2, section);
        final long end = System.currentTimeMillis() - start;
        return array;
    }
    
    @Override
    public void close() throws IOException {
        if (this.areaReader != null) {
            this.areaReader = null;
        }
    }
    
    public static void main(final String[] args) throws IOException {
        final IOServiceProvider areaiosp = new AreaServiceProvider();
        final RandomAccessFile rf = new RandomAccessFile(args[0], "r", 2048);
        final NetcdfFile ncfile = new MakeNetcdfFile(areaiosp, rf, args[0], null);
        if (args.length > 1) {
            FileWriter.writeToFile(ncfile, args[1]);
        }
    }
    
    protected static class MakeNetcdfFile extends NetcdfFile
    {
        MakeNetcdfFile(final IOServiceProvider spi, final RandomAccessFile raf, final String location, final CancelTask cancelTask) throws IOException {
            super(spi, raf, location, cancelTask);
        }
    }
}
