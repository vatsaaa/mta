// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.iosp.mcidas;

import visad.data.BadFormException;
import java.io.IOException;
import ucar.unidata.io.RandomAccessFile;

public class V5DStruct
{
    private static final int MBS = 32;
    private static final String TOPOFILE = "EARTH.TOPO";
    private static final String WORLDFILE = "OUTLSUPW";
    private static final String USAFILE = "OUTLUSAM";
    private static final String TCL_STARTUP_FILE = "vis5d.tcl";
    private static final String FUNCTION_PATH = "userfuncs";
    private static final int ANIMRATE = 100;
    private static final double DEFAULT_LOG_SCALE = 1012.5;
    private static final double DEFAULT_LOG_EXP = -7.2;
    private static final boolean BIG_GFX = true;
    private static final int MAX_LABEL = 1000;
    private static final int MAX_FUNCS = 100;
    private static final int V5D_VERSION = 42;
    private static final float MISSING = Float.NaN;
    private static final int MAXVARS = 200;
    private static final int MAXTIMES = 400;
    private static final int MAXROWS = 400;
    private static final int MAXCOLUMNS = 400;
    private static final int MAXLEVELS = 400;
    private static final int MAXPROJARGS = 801;
    private static final int MAXVERTARGS = 401;
    private static final String FILE_VERSION = "4.3";
    private static final int DEFAULT_FILE_BUFFER = 204800;
    private static final int DEFAULT_HTTP_BUFFER = 204800;
    public static final int TAG_ID = 1446331402;
    public static final int TAG_VERSION = 1000;
    public static final int TAG_NUMTIMES = 1001;
    public static final int TAG_NUMVARS = 1002;
    public static final int TAG_VARNAME = 1003;
    public static final int TAG_NR = 1004;
    public static final int TAG_NC = 1005;
    public static final int TAG_NL = 1006;
    public static final int TAG_NL_VAR = 1007;
    public static final int TAG_LOWLEV_VAR = 1008;
    public static final int TAG_TIME = 1010;
    public static final int TAG_DATE = 1011;
    public static final int TAG_MINVAL = 1012;
    public static final int TAG_MAXVAL = 1013;
    public static final int TAG_COMPRESS = 1014;
    public static final int TAG_UNITS = 1015;
    public static final int TAG_VERTICAL_SYSTEM = 2000;
    public static final int TAG_VERT_ARGS = 2100;
    public static final int TAG_BOTTOMBOUND = 2001;
    public static final int TAG_LEVINC = 2002;
    public static final int TAG_HEIGHT = 2003;
    public static final int TAG_PROJECTION = 3000;
    public static final int TAG_PROJ_ARGS = 3100;
    public static final int TAG_NORTHBOUND = 3001;
    public static final int TAG_WESTBOUND = 3002;
    public static final int TAG_ROWINC = 3003;
    public static final int TAG_COLINC = 3004;
    public static final int TAG_LAT1 = 3005;
    public static final int TAG_LAT2 = 3006;
    public static final int TAG_POLE_ROW = 3007;
    public static final int TAG_POLE_COL = 3008;
    public static final int TAG_CENTLON = 3009;
    public static final int TAG_CENTLAT = 3010;
    public static final int TAG_CENTROW = 3011;
    public static final int TAG_CENTCOL = 3012;
    public static final int TAG_ROTATION = 3013;
    public static final int TAG_END = 9999;
    public int NumTimes;
    public int NumVars;
    public int Nr;
    public int Nc;
    public int[] Nl;
    public int[] LowLev;
    public char[][] VarName;
    public char[][] Units;
    public int[] TimeStamp;
    public int[] DateStamp;
    public float[] MinVal;
    public float[] MaxVal;
    public short[][] McFile;
    public short[][] McGrid;
    public int VerticalSystem;
    public float[] VertArgs;
    public int Projection;
    public float[] ProjArgs;
    public int CompressMode;
    public String FileVersion;
    private int FileFormat;
    private RandomAccessFile FileDesc;
    char Mode;
    int CurPos;
    int FirstGridPos;
    int[] GridSize;
    int SumGridSizes;
    private static boolean SIMPLE_COMPRESSION;
    private static boolean KLUDGE;
    private static boolean ORIGINAL;
    
    private static boolean IS_MISSING(final float x) {
        return Float.isNaN(x) || x >= 1.0E30;
    }
    
    public static V5DStruct v5d_open(final RandomAccessFile raf, final int[] sizes, final int[] n_levels, final String[] var_names, final String[] var_units, final int[] map_proj, final float[] projargs, final int[] vert_sys, final float[] vert_args, final double[] times) throws IOException, BadFormException {
        final byte[] varnames = new byte[2000];
        final byte[] varunits = new byte[4000];
        final V5DStruct v = v5dOpenFile(raf);
        if (v != null) {
            sizes[0] = v.Nr;
            sizes[1] = v.Nc;
            sizes[3] = v.NumTimes;
            sizes[4] = v.NumVars;
            for (int j = 0; j < v.NumVars; ++j) {
                final int k = 10 * j;
                for (int i = 0; i < 10; ++i) {
                    if (v.VarName[j][i] == '\0' || i >= 9) {
                        varnames[k + i] = 0;
                        break;
                    }
                    varnames[k + i] = (byte)v.VarName[j][i];
                }
            }
            for (int j = 0; j < v.NumVars; ++j) {
                final int k = 20 * j;
                for (int i = 0; i < 20; ++i) {
                    if (v.Units[j][i] == '\0' || i >= 19) {
                        varunits[k + i] = 0;
                        break;
                    }
                    varunits[k + i] = (byte)v.Units[j][i];
                }
            }
            for (int i = 0; i < v.NumVars; ++i) {
                final int k = 10 * i;
                final int k2 = 20 * i;
                int m = k;
                int m2 = k2;
                while (varnames[m] != 0) {
                    ++m;
                }
                while (varunits[m2] != 0) {
                    ++m2;
                }
                var_names[i] = new String(varnames, k, m - k);
                var_units[i] = new String(varunits, k2, m2 - k2);
            }
            int maxNl = v.Nl[0];
            for (int i = 0; i < v.NumVars; ++i) {
                if (v.Nl[i] > maxNl) {
                    maxNl = v.Nl[i];
                }
            }
            sizes[2] = maxNl;
            for (int i = 0; i < v.NumVars; ++i) {
                n_levels[i] = v.Nl[i];
            }
            vert_sys[0] = v.VerticalSystem;
            for (int kk = 0; kk < maxNl; ++kk) {
                vert_args[kk] = v.VertArgs[kk];
            }
            final int first_day = v5dYYDDDtoDays(v.DateStamp[0]);
            final int first_time = v5dHHMMSStoSeconds(v.TimeStamp[0]);
            for (int i = 0; i < v.NumTimes; ++i) {
                final int day = v5dYYDDDtoDays(v.DateStamp[i]);
                final int time = v5dHHMMSStoSeconds(v.TimeStamp[i]);
                final double ff = day * 24.0 * 60.0 * 60.0 + time;
                times[i] = ff;
            }
            map_proj[0] = v.Projection;
            for (int kk = 0; kk < 801; ++kk) {
                projargs[kk] = v.ProjArgs[kk];
            }
        }
        else {
            sizes[0] = -1;
        }
        return v;
    }
    
    public void v5d_read(final int time, final int vr, final float[] ranges, final float[] data) throws IOException, BadFormException {
        ranges[0] = this.MinVal[vr];
        ranges[1] = this.MaxVal[vr];
        final boolean status = this.v5dReadGrid(time, vr, data);
        if (!status) {
            ranges[0] = 1.0f;
            ranges[1] = -1.0f;
        }
    }
    
    public static int getUnsignedByte(final byte b) {
        final int i = (b >= 0) ? b : (b + 256);
        return i;
    }
    
    public static int getUnsignedShort(final byte b1, final byte b2) {
        final int i1 = getUnsignedByte(b1);
        final int i2 = getUnsignedByte(b2);
        return 256 * i1 + i2;
    }
    
    public static int getUnsignedInt(final byte b1, final byte b2, final byte b3, final byte b4) {
        final int i1 = getUnsignedByte(b1);
        final int i2 = getUnsignedByte(b2);
        final int i3 = getUnsignedByte(b3);
        final int i4 = getUnsignedByte(b4);
        return 16777216 * i1 + 65536 * i2 + 256 * i3 + i4;
    }
    
    private static float pressure_to_height(final float pressure) {
        return (float)(-7.2 * Math.log(pressure / 1012.5));
    }
    
    private static float height_to_pressure(final float height) {
        return (float)(1012.5 * Math.exp(height / -7.2));
    }
    
    private static int copy_string2(final char[] dst, final char[] src, final int maxlen) {
        for (int i = 0; i < maxlen; ++i) {
            dst[i] = src[i];
        }
        for (int i = maxlen - 1; i >= 0 && (dst[i] == ' ' || i == maxlen - 1); --i) {
            dst[i] = '\0';
        }
        return new String(dst).length();
    }
    
    private static int copy_string(final char[] dst, final char[] src, final int maxlen) {
        int i;
        for (i = 0; i < maxlen; ++i) {
            if (src[i] == ' ' || i == maxlen - 1) {
                dst[i] = '\0';
                break;
            }
            dst[i] = src[i];
        }
        return i;
    }
    
    private static int v5dYYDDDtoDays(final int yyddd) {
        int iy = yyddd / 1000;
        final int id = yyddd - 1000 * iy;
        if (iy >= 1900) {
            iy -= 1900;
        }
        else if (iy < 50) {
            iy += 100;
        }
        final int idays = 365 * iy + (iy - 1) / 4 - (iy - 1) / 100 + (iy + 299) / 400 + id - 1;
        return idays;
    }
    
    private static int v5dHHMMSStoSeconds(final int hhmmss) {
        final int h = hhmmss / 10000;
        final int m = hhmmss / 100 % 100;
        final int s = hhmmss % 100;
        return s + m * 60 + h * 60 * 60;
    }
    
    private static int v5dDaysToYYDDD(final int days) {
        int iy = 4 * days / 1461;
        final int id = days - (365 * iy + (iy - 1) / 4);
        if (iy > 99) {
            iy -= 100;
        }
        final int iyyddd = iy * 1000 + id;
        return iyyddd;
    }
    
    private static int v5dSecondsToHHMMSS(final int seconds) {
        final int hh = seconds / 3600;
        final int mm = seconds / 60 % 60;
        final int ss = seconds % 60;
        return hh * 10000 + mm * 100 + ss;
    }
    
    public static V5DStruct v5dOpenFile(final RandomAccessFile fd) throws IOException, BadFormException {
        if (fd == null) {
            System.out.println("null file");
            return null;
        }
        final V5DStruct v = new V5DStruct();
        v.FileDesc = fd;
        v.Mode = 'r';
        return v.read_v5d_header() ? v : null;
    }
    
    private static void compute_ga_gb(final int nr, final int nc, final int nl, final float[] data, final int compressmode, final float[] ga, final float[] gb, final float[] minval, final float[] maxval) throws BadFormException {
        if (V5DStruct.SIMPLE_COMPRESSION) {
            float min = 1.0E30f;
            float max = -1.0E30f;
            final int num = nr * nc * nl;
            boolean allmissing = true;
            for (int i = 0; i < num; ++i) {
                if (!IS_MISSING(data[i])) {
                    if (data[i] < min) {
                        min = data[i];
                    }
                    if (data[i] > max) {
                        max = data[i];
                    }
                    allmissing = false;
                }
            }
            float a;
            float b;
            if (allmissing) {
                a = 1.0f;
                b = 0.0f;
            }
            else {
                a = (float)((max - min) / 254.0);
                b = min;
            }
            for (int i = 0; i < nl; ++i) {
                ga[i] = a;
                gb[i] = b;
            }
            minval[0] = min;
            maxval[0] = max;
        }
        else {
            final float SMALLVALUE = -1.0E30f;
            final float BIGVALUE = 1.0E30f;
            final float[] levmin = new float[400];
            final float[] levmax = new float[400];
            final float[] d = new float[400];
            final int nrnc = nr * nc;
            float gridmin = 1.0E30f;
            float gridmax = -1.0E30f;
            int j = 0;
            for (int lev = 0; lev < nl; ++lev) {
                float min2 = 1.0E30f;
                float max2 = -1.0E30f;
                for (int k = 0; k < nrnc; ++k) {
                    if (!IS_MISSING(data[j]) && data[j] < min2) {
                        min2 = data[j];
                    }
                    if (!IS_MISSING(data[j]) && data[j] > max2) {
                        max2 = data[j];
                    }
                    ++j;
                }
                if (min2 < gridmin) {
                    gridmin = min2;
                }
                if (max2 > gridmax) {
                    gridmax = max2;
                }
                levmin[lev] = min2;
                levmax[lev] = max2;
            }
            if (V5DStruct.KLUDGE) {
                final int nrncnl = nrnc * nl;
                final float delt = (float)((gridmax - gridmin) / 100000.0);
                if (Math.abs(gridmin) < delt && gridmin != 0.0 && compressmode != 4) {
                    for (j = 0; j < nrncnl; ++j) {
                        if (!IS_MISSING(data[j]) && data[j] < delt) {
                            data[j] = delt;
                        }
                    }
                    gridmin = delt;
                    for (int lev = 0; lev < nl; ++lev) {
                        if (Math.abs(levmin[lev]) < delt) {
                            levmin[lev] = delt;
                        }
                        if (Math.abs(levmax[lev]) < delt) {
                            levmax[lev] = delt;
                        }
                    }
                }
            }
            float dmax = 0.0f;
            for (int lev = 0; lev < nl; ++lev) {
                if (levmin[lev] >= 1.0E30f && levmax[lev] <= -1.0E30f) {
                    d[lev] = 0.0f;
                }
                else {
                    d[lev] = levmax[lev] - levmin[lev];
                }
                if (d[lev] > dmax) {
                    dmax = d[lev];
                }
            }
            if (dmax == 0.0) {
                if (gridmin == gridmax) {
                    for (int lev = 0; lev < nl; ++lev) {
                        ga[lev] = gridmin;
                        gb[lev] = 0.0f;
                    }
                }
                else {
                    for (int lev = 0; lev < nl; ++lev) {
                        ga[lev] = levmin[lev];
                        gb[lev] = 0.0f;
                    }
                }
            }
            else if (compressmode == 1) {
                V5DStruct.ORIGINAL = true;
                if (V5DStruct.ORIGINAL) {
                    final float ival = dmax / 254.0f;
                    final float mval = gridmin;
                    for (int lev = 0; lev < nl; ++lev) {
                        ga[lev] = ival;
                        gb[lev] = mval + ival * (int)((levmin[lev] - mval) / ival);
                    }
                }
                else {
                    for (int lev = 0; lev < nl; ++lev) {
                        float ival;
                        if (d[lev] == 0.0) {
                            ival = 1.0f;
                        }
                        else {
                            ival = d[lev] / 254.0f;
                        }
                        ga[lev] = ival;
                        gb[lev] = levmin[lev];
                    }
                }
            }
            else if (compressmode == 2) {
                final float ival = dmax / 65534.0f;
                final float mval = gridmin;
                for (int lev = 0; lev < nl; ++lev) {
                    ga[lev] = ival;
                    gb[lev] = mval + ival * (int)((levmin[lev] - mval) / ival);
                }
            }
            else {
                V5Dassert(compressmode == 4);
                for (int lev = 0; lev < nl; ++lev) {
                    ga[lev] = 1.0f;
                    gb[lev] = 0.0f;
                }
            }
            minval[0] = gridmin;
            maxval[0] = gridmax;
        }
    }
    
    private void v5dDecompressGrid(final int nr, final int nc, final int nl, final int compressmode, final byte[] compdata1, final float[] ga, final float[] gb, final float[] data) {
        final int nrnc = nr * nc;
        final int nrncnl = nr * nc * nl;
        if (compressmode == 1) {
            int p = 0;
            for (int lev = 0; lev < nl; ++lev) {
                final float a = ga[lev];
                final float b = gb[lev];
                float d = 0.0f;
                float aa = 0.0f;
                int id;
                if (a > 1.0E-10) {
                    d = b / a;
                    id = (int)Math.floor(d);
                    d -= id;
                    aa = (float)(a * 1.0E-6);
                }
                else {
                    id = 1;
                }
                if (-254 <= id && id <= 0 && d < aa) {
                    for (int i = 0; i < nrnc; ++i, ++p) {
                        final int cd1p = getUnsignedByte(compdata1[p]);
                        if (cd1p == 255) {
                            data[p] = Float.NaN;
                        }
                        else {
                            data[p] = cd1p * a + b;
                            if (Math.abs(data[p]) < aa) {
                                data[p] = aa;
                            }
                        }
                    }
                }
                else {
                    for (int i = 0; i < nrnc; ++i, ++p) {
                        final int cd1p = getUnsignedByte(compdata1[p]);
                        if (cd1p == 255) {
                            data[p] = Float.NaN;
                        }
                        else {
                            data[p] = cd1p * a + b;
                        }
                    }
                }
            }
        }
        else if (compressmode == 2) {
            int p = 0;
            for (int lev = 0; lev < nl; ++lev) {
                final float a = ga[lev];
                final float b = gb[lev];
                for (int i = 0; i < nrnc; ++i, ++p) {
                    final int cd1p2 = getUnsignedShort(compdata1[2 * p], compdata1[2 * p + 1]);
                    if (cd1p2 == 65535) {
                        data[p] = Float.NaN;
                    }
                    else {
                        data[p] = cd1p2 * a + b;
                    }
                }
            }
        }
        else {
            for (int j = 0; j < nrncnl; ++j) {
                final int a2 = getUnsignedInt(compdata1[j * 4], compdata1[j * 4 + 1], compdata1[j * 4 + 2], compdata1[j * 4 + 3]);
                data[j] = Float.intBitsToFloat(a2);
            }
        }
    }
    
    private static final void V5Dassert(final boolean b) throws BadFormException {
        if (!b) {
            throw new BadFormException("Warning: assert failed");
        }
    }
    
    private static int read_block(final RandomAccessFile f, final byte[] data, final int elements, final int elsize) throws IOException {
        int n;
        if (elsize == 1) {
            n = f.read(data, 0, elements);
        }
        else if (elsize == 2) {
            n = f.read(data, 0, elements * 2) / 2;
        }
        else {
            if (elsize != 4) {
                throw new IOException("Fatal error in read_block(): bad elsize (" + elsize + ")");
            }
            n = f.read(data, 0, elements * 4) / 4;
        }
        return n;
    }
    
    private static int read_float4_array(final RandomAccessFile f, final float[] x, final int n) throws IOException {
        for (int i = 0; i < n; ++i) {
            x[i] = f.readFloat();
        }
        return n;
    }
    
    private static void v5dCompressGrid(final int nr, final int nc, final int nl, final int compressmode, final float[] data, final byte[] compdata1, final float[] ga, final float[] gb, final float[] minval, final float[] maxval) throws BadFormException {
        final int nrnc = nr * nc;
        final int nrncnl = nr * nc * nl;
        compute_ga_gb(nr, nc, nl, data, compressmode, ga, gb, minval, maxval);
        if (compressmode == 1) {
            int p = 0;
            for (int lev = 0; lev < nl; ++lev) {
                final float b = gb[lev] - 1.0E-4f;
                float one_over_a;
                if (ga[lev] == 0.0f) {
                    one_over_a = 1.0f;
                }
                else {
                    one_over_a = 1.0f / ga[lev];
                }
                for (int i = 0; i < nrnc; ++i, ++p) {
                    if (IS_MISSING(data[p])) {
                        compdata1[p] = -1;
                    }
                    else {
                        compdata1[p] = (byte)((data[p] - b) * one_over_a);
                    }
                }
            }
        }
        else if (compressmode == 2) {
            int p = 0;
            for (int lev = 0; lev < nl; ++lev) {
                final float b = gb[lev] - 1.0E-4f;
                float one_over_a;
                if (ga[lev] == 0.0f) {
                    one_over_a = 1.0f;
                }
                else {
                    one_over_a = 1.0f / ga[lev];
                }
                for (int i = 0; i < nrnc; ++i, ++p) {
                    if (IS_MISSING(data[p])) {
                        compdata1[2 * p + 1] = (compdata1[2 * p] = -1);
                    }
                    else {
                        final short s = (short)((data[p] - b) * one_over_a);
                        compdata1[2 * p] = (byte)(s / 256);
                        compdata1[2 * p + 1] = (byte)(s % 256);
                    }
                }
            }
        }
        else {
            System.arraycopy(compdata1, 0, data, 0, nrncnl * 4);
        }
    }
    
    private static int write_block(final RandomAccessFile f, final byte[] data, final int elements, final int elsize) throws IOException {
        if (elsize == 1) {
            f.write(data, 0, elements);
        }
        else if (elsize == 2) {
            f.write(data, 0, elements * 2);
        }
        else {
            if (elsize != 4) {
                throw new IOException("Fatal error in write_block(): bad elsize (" + elsize + ")");
            }
            f.write(data, 0, elements * 4);
        }
        return elements;
    }
    
    V5DStruct() {
        this.Nl = new int[200];
        this.LowLev = new int[200];
        this.VarName = new char[200][10];
        this.Units = new char[200][20];
        this.TimeStamp = new int[400];
        this.DateStamp = new int[400];
        this.MinVal = new float[200];
        this.MaxVal = new float[200];
        this.McFile = new short[400][200];
        this.McGrid = new short[400][200];
        this.VertArgs = new float[401];
        this.ProjArgs = new float[801];
        this.GridSize = new int[200];
        this.Projection = -1;
        this.VerticalSystem = -1;
        for (int i = 0; i < 200; ++i) {
            this.MinVal[i] = Float.NaN;
            this.MaxVal[i] = Float.NaN;
        }
        this.FileVersion = "4.3";
        this.CompressMode = 1;
        this.FileDesc = null;
    }
    
    int v5dSizeofGrid(final int time, final int vr) {
        return this.Nr * this.Nc * this.Nl[vr] * this.CompressMode;
    }
    
    int grid_position(final int time, final int vr) throws BadFormException {
        V5Dassert(time >= 0);
        V5Dassert(vr >= 0);
        V5Dassert(time < this.NumTimes);
        V5Dassert(vr < this.NumVars);
        int pos = this.FirstGridPos + time * this.SumGridSizes;
        for (int i = 0; i < vr; ++i) {
            pos += this.GridSize[i];
        }
        return pos;
    }
    
    boolean v5dVerifyStruct() {
        boolean valid = true;
        if (this.NumVars < 0) {
            System.err.println("Invalid number of variables: " + this.NumVars);
            valid = false;
        }
        else if (this.NumVars > 200) {
            System.err.println("Too many variables: " + this.NumVars + "  (Maximum is " + 200 + ")");
            valid = false;
        }
        for (int i = 0; i < this.NumVars; ++i) {
            if (this.VarName[i][0] == '\0') {
                System.err.println("Missing variable name: VarName[" + i + "]=\"\"");
                valid = false;
            }
        }
        if (this.NumTimes < 0) {
            System.err.println("Invalid number of timesteps: " + this.NumTimes);
            valid = false;
        }
        else if (this.NumTimes > 400) {
            System.err.println("Too many timesteps: " + this.NumTimes + "  (Maximum is " + 400 + ")");
            valid = false;
        }
        for (int i = 1; i < this.NumTimes; ++i) {
            final int date0 = v5dYYDDDtoDays(this.DateStamp[i - 1]);
            int date2 = v5dYYDDDtoDays(this.DateStamp[i]);
            final int time0 = v5dHHMMSStoSeconds(this.TimeStamp[i - 1]);
            int time2 = v5dHHMMSStoSeconds(this.TimeStamp[i]);
            if (date2 < date0 || (date2 == date0 && time2 <= time0)) {
                int inc = 1;
                if (i > 1) {
                    final int j = v5dHHMMSStoSeconds(this.TimeStamp[i - 1]) - v5dHHMMSStoSeconds(this.TimeStamp[i - 2]) + 86400 * (v5dYYDDDtoDays(this.DateStamp[i - 1]) - v5dYYDDDtoDays(this.DateStamp[i - 2]));
                    if (j > 0) {
                        inc = j;
                    }
                }
                time2 = time0 + inc;
                date2 = date0;
                if (time2 >= 86400) {
                    time2 = 0;
                    ++date2;
                }
                this.DateStamp[i] = v5dDaysToYYDDD(date2);
                this.TimeStamp[i] = v5dSecondsToHHMMSS(time2);
            }
        }
        if (this.Nr < 2) {
            System.err.println("Too few rows: " + this.Nr + " (2 is minimum)");
            valid = false;
        }
        if (this.Nc < 2) {
            System.err.println("Too few columns: " + this.Nc + " (2 is minimum)");
            valid = false;
        }
        int maxnl = 0;
        for (int vr = 0; vr < this.NumVars; ++vr) {
            if (this.LowLev[vr] < 0) {
                System.err.println("Low level cannot be negative for var " + (Object)this.VarName[vr] + ": " + this.LowLev[vr]);
                valid = false;
            }
            if (this.Nl[vr] < 1) {
                System.err.println("Too few levels for var " + (Object)this.VarName[vr] + ": " + this.Nl[vr] + " (1 is minimum)");
                valid = false;
            }
            if (this.Nl[vr] + this.LowLev[vr] > 400) {
                System.err.println("Too many levels for var " + (Object)this.VarName[vr] + ": " + (this.Nl[vr] + this.LowLev[vr]) + " (" + 400 + " is maximum)");
                valid = false;
            }
            if (this.Nl[vr] + this.LowLev[vr] > maxnl) {
                maxnl = this.Nl[vr] + this.LowLev[vr];
            }
        }
        if (this.CompressMode != 1 && this.CompressMode != 2 && this.CompressMode != 4) {
            System.err.println("Bad CompressMode: " + this.CompressMode + " (must be 1, 2 or 4)");
            valid = false;
        }
        switch (this.VerticalSystem) {
            case 0:
            case 1: {
                if (this.VertArgs[1] == 0.0) {
                    System.err.println("Vertical level increment is zero, must be non-zero");
                    valid = false;
                    break;
                }
                break;
            }
            case 2: {
                for (int i = 1; i < maxnl; ++i) {
                    if (this.VertArgs[i] <= this.VertArgs[i - 1]) {
                        System.err.println("Height[" + i + "]=" + this.VertArgs[i] + " <= Height[" + (i - 1) + "]=" + this.VertArgs[i - 1] + ", level heights must increase");
                        valid = false;
                        break;
                    }
                }
                break;
            }
            case 3: {
                for (int i = 1; i < maxnl; ++i) {
                    if (this.VertArgs[i] <= this.VertArgs[i - 1]) {
                        System.err.println("Pressure[" + i + "]=" + height_to_pressure(this.VertArgs[i]) + " >= Pressure[" + (i - 1) + "]=" + height_to_pressure(this.VertArgs[i - 1]) + ", level pressures must decrease");
                        valid = false;
                        break;
                    }
                }
                break;
            }
            default: {
                System.err.println("VerticalSystem = " + this.VerticalSystem + ", must be in 0..3");
                valid = false;
                break;
            }
        }
        switch (this.Projection) {
            case 0: {
                if (this.ProjArgs[2] == 0.0) {
                    System.err.println("Row Increment (ProjArgs[2]) can't be zero");
                    valid = false;
                }
                if (this.ProjArgs[3] == 0.0) {
                    System.err.println("Column increment (ProjArgs[3]) can't be zero");
                    valid = false;
                    break;
                }
                break;
            }
            case 1: {
                if (this.ProjArgs[2] < 0.0) {
                    System.err.println("Row Increment (ProjArgs[2]) = " + this.ProjArgs[2] + "  (must be >=0.0)");
                    valid = false;
                }
                if (this.ProjArgs[3] <= 0.0) {
                    System.err.println("Column Increment (ProjArgs[3]) = " + this.ProjArgs[3] + "  (must be >=0.0)");
                    valid = false;
                    break;
                }
                break;
            }
            case 2: {
                if (this.ProjArgs[0] < -90.0 || this.ProjArgs[0] > 90.0) {
                    System.err.println("Lat1 (ProjArgs[0]) out of range: " + this.ProjArgs[0]);
                    valid = false;
                }
                if (this.ProjArgs[1] < -90.0 || this.ProjArgs[1] > 90.0) {
                    System.err.println("Lat2 (ProjArgs[1] out of range: " + this.ProjArgs[1]);
                    valid = false;
                }
                if (this.ProjArgs[5] <= 0.0) {
                    System.err.println("ColInc (ProjArgs[5]) = " + this.ProjArgs[5] + "  (must be >=0.0)");
                    valid = false;
                    break;
                }
                break;
            }
            case 3: {
                if (this.ProjArgs[0] < -90.0 || this.ProjArgs[0] > 90.0) {
                    System.err.println("Central Latitude (ProjArgs[0]) out of range: " + this.ProjArgs[0] + "  (must be in +/-90)");
                    valid = false;
                }
                if (this.ProjArgs[1] < -180.0 || this.ProjArgs[1] > 180.0) {
                    System.err.println("Central Longitude (ProjArgs[1]) out of range: " + this.ProjArgs[1] + "  (must be in +/-180)");
                    valid = false;
                }
                if (this.ProjArgs[4] < 0.0f) {
                    System.err.println("Column spacing (ProjArgs[4]) = " + this.ProjArgs[4] + "  (must be positive)");
                    valid = false;
                    break;
                }
                break;
            }
            case 4: {
                if (this.ProjArgs[2] <= 0.0) {
                    System.err.println("Row Increment (ProjArgs[2]) = " + this.ProjArgs[2] + "  (must be >=0.0)");
                    valid = false;
                }
                if (this.ProjArgs[3] <= 0.0) {
                    System.err.println("Column Increment = (ProjArgs[3]) " + this.ProjArgs[3] + "  (must be >=0.0)");
                    valid = false;
                }
                if (this.ProjArgs[4] < -90.0 || this.ProjArgs[4] > 90.0) {
                    System.err.println("Central Latitude (ProjArgs[4]) out of range: " + this.ProjArgs[4] + "  (must be in +/-90)");
                    valid = false;
                }
                if (this.ProjArgs[5] < -180.0 || this.ProjArgs[5] > 180.0) {
                    System.err.println("Central Longitude (ProjArgs[5]) out of range: " + this.ProjArgs[5] + "  (must be in +/-180)");
                    valid = false;
                }
                if (this.ProjArgs[6] < -180.0 || this.ProjArgs[6] > 180.0) {
                    System.err.println("Central Longitude (ProjArgs[6]) out of range: " + this.ProjArgs[6] + "  (must be in +/-180)");
                    valid = false;
                    break;
                }
                break;
            }
            default: {
                System.err.println("Projection = " + this.Projection + ", must be in 0..4");
                valid = false;
                break;
            }
        }
        return valid;
    }
    
    boolean v5dGetMcIDASgrid(final int time, final int vr, final int[] mcfile, final int[] mcgrid) {
        if (time < 0 || time >= this.NumTimes) {
            System.err.println("Bad time argument to v5dGetMcIDASgrid: " + time);
            return false;
        }
        if (vr < 0 || vr >= this.NumVars) {
            System.err.println("Bad var argument to v5dGetMcIDASgrid: " + vr);
            return false;
        }
        mcfile[0] = this.McFile[time][vr];
        mcgrid[0] = this.McGrid[time][vr];
        return true;
    }
    
    boolean v5dSetMcIDASgrid(final int time, final int vr, final int mcfile, final int mcgrid) {
        if (time < 0 || time >= this.NumTimes) {
            System.err.println("Bad time argument to v5dSetMcIDASgrid: " + time);
            return false;
        }
        if (vr < 0 || vr >= this.NumVars) {
            System.err.println("Bad var argument to v5dSetMcIDASgrid: " + vr);
            return false;
        }
        this.McFile[time][vr] = (short)mcfile;
        this.McGrid[time][vr] = (short)mcgrid;
        return true;
    }
    
    boolean read_comp_header() throws IOException {
        final RandomAccessFile f = this.FileDesc;
        f.seek(0L);
        final int id = f.readInt();
        if (id == -2139062144 || id == -2139062143) {
            int gridtimes;
            int gridparms;
            if (id == -2139062144) {
                gridtimes = 300;
                gridparms = 20;
            }
            else {
                gridtimes = 400;
                gridparms = 30;
            }
            this.FirstGridPos = 48 + 8 * gridtimes + 4 * gridparms;
            this.NumTimes = f.readInt();
            this.NumVars = f.readInt();
            this.Nr = f.readInt();
            this.Nc = f.readInt();
            final int nl = f.readInt();
            for (int i = 0; i < this.NumVars; ++i) {
                this.Nl[i] = nl;
                this.LowLev[i] = 0;
            }
            this.ProjArgs[0] = f.readFloat();
            this.ProjArgs[1] = f.readFloat();
            final float hgttop = f.readFloat();
            this.ProjArgs[2] = f.readFloat();
            this.ProjArgs[3] = f.readFloat();
            final float hgtinc = f.readFloat();
            this.VerticalSystem = 1;
            this.VertArgs[0] = hgttop - hgtinc * (nl - 1);
            this.VertArgs[1] = hgtinc;
            for (int i = 0; i < gridtimes; ++i) {
                final int j = f.readInt();
                this.DateStamp[i] = v5dDaysToYYDDD(j);
            }
            for (int i = 0; i < gridtimes; ++i) {
                final int j = f.readInt();
                this.TimeStamp[i] = v5dSecondsToHHMMSS(j);
            }
            for (int i = 0; i < gridparms; ++i) {
                final char[] name = new char[4];
                for (int q = 0; q < 4; ++q) {
                    name[q] = (char)f.readByte();
                }
                for (int j = 3; j > 0 && (name[j] == ' ' || name[j] == '\0'); --j) {
                    name[j] = '\0';
                }
                System.arraycopy(name, 0, this.VarName[i], 0, 4);
                this.VarName[i][4] = '\0';
            }
            final int gridsize = (this.Nr * this.Nc * nl + 3) / 4 * 4;
            for (int i = 0; i < this.NumVars; ++i) {
                this.GridSize[i] = 8 + gridsize;
            }
            this.SumGridSizes = (8 + gridsize) * this.NumVars;
            for (int i = 0; i < this.NumVars; ++i) {
                this.MinVal[i] = 999999.9f;
                this.MaxVal[i] = -999999.9f;
            }
            for (int it = 0; it < this.NumTimes; ++it) {
                for (int iv = 0; iv < this.NumVars; ++iv) {
                    final float ga = f.readFloat();
                    final float gb = f.readFloat();
                    f.skipBytes(gridsize);
                    final float min = -(125.0f + gb) / ga;
                    final float max = (125.0f - gb) / ga;
                    if (min < this.MinVal[iv]) {
                        this.MinVal[iv] = min;
                    }
                    if (max > this.MaxVal[iv]) {
                        this.MaxVal[iv] = max;
                    }
                }
            }
        }
        else if (id == -2139062142 || id == -2139062141) {
            float delta = 0.0f;
            final int gridtimes = f.readInt();
            this.NumVars = f.readInt();
            this.NumTimes = f.readInt();
            this.Nr = f.readInt();
            this.Nc = f.readInt();
            final int nl2 = f.readInt();
            for (int k = 0; k < this.NumVars; ++k) {
                this.Nl[k] = nl2;
            }
            this.ProjArgs[2] = f.readFloat();
            this.ProjArgs[3] = f.readFloat();
            this.VerticalSystem = 1;
            for (int k = 0; k < nl2; ++k) {
                this.VertArgs[k] = f.readFloat();
                if (k == 1) {
                    delta = this.VertArgs[1] - this.VertArgs[0];
                }
                else if (k > 1 && delta != this.VertArgs[k] - this.VertArgs[k - 1]) {
                    this.VerticalSystem = 2;
                }
            }
            if (this.VerticalSystem == 1) {
                this.VertArgs[1] = delta;
            }
            for (int iv2 = 0; iv2 < this.NumVars; ++iv2) {
                final char[] name2 = new char[8];
                for (int q2 = 0; q2 < 8; ++q2) {
                    name2[q2] = (char)f.readByte();
                }
                for (int l = 7; l > 0 && (name2[l] == ' ' || name2[l] == '\0'); --l) {
                    name2[l] = '\0';
                }
                System.arraycopy(name2, 0, this.VarName[iv2], 0, 8);
                this.VarName[iv2][8] = '\0';
            }
            for (int iv2 = 0; iv2 < this.NumVars; ++iv2) {
                this.MinVal[iv2] = f.readFloat();
            }
            for (int iv2 = 0; iv2 < this.NumVars; ++iv2) {
                this.MaxVal[iv2] = f.readFloat();
            }
            for (int it2 = 0; it2 < gridtimes; ++it2) {
                final int l = f.readInt();
                this.TimeStamp[it2] = v5dSecondsToHHMMSS(l);
            }
            for (int it2 = 0; it2 < gridtimes; ++it2) {
                final int l = f.readInt();
                this.DateStamp[it2] = v5dDaysToYYDDD(l);
            }
            for (int it2 = 0; it2 < gridtimes; ++it2) {
                final float nlat = f.readFloat();
                if (it2 == 0) {
                    this.ProjArgs[0] = nlat;
                }
            }
            for (int it2 = 0; it2 < gridtimes; ++it2) {
                final float wlon = f.readFloat();
                if (it2 == 0) {
                    this.ProjArgs[1] = wlon;
                }
            }
            int gridsize2;
            if (id == -2139062142) {
                gridsize2 = nl2 * 2 * 4 + (this.Nr * this.Nc * nl2 + 3) / 4 * 4;
            }
            else {
                gridsize2 = 8 + nl2 * 2 * 4 + (this.Nr * this.Nc * nl2 + 3) / 4 * 4;
            }
            for (int k = 0; k < this.NumVars; ++k) {
                this.GridSize[k] = gridsize2;
            }
            this.SumGridSizes = gridsize2 * this.NumVars;
            this.FirstGridPos = 36 + this.Nl[0] * 4 + this.NumVars * 16 + gridtimes * 16;
        }
        this.CompressMode = 1;
        this.Projection = 1;
        this.FileVersion = "";
        return true;
    }
    
    boolean read_comp_grid(final int time, final int vr, final float[] ga, final float[] gb, final byte[] compdata1) throws IOException, BadFormException {
        final RandomAccessFile f = this.FileDesc;
        final long pos = this.grid_position(time, vr);
        f.seek(pos);
        if (this.FileFormat == -2139062141) {
            final int mcfile = f.readInt();
            final int mcgrid = f.readInt();
            this.McFile[time][vr] = (short)mcfile;
            this.McGrid[time][vr] = (short)mcgrid;
        }
        final int nl = this.Nl[vr];
        short bias;
        if (this.FileFormat == -2139062144 || this.FileFormat == -2139062143) {
            final float a = f.readFloat();
            final float b = f.readFloat();
            for (int i = 0; i < nl; ++i) {
                if (a == 0.0) {
                    ga[i] = (gb[i] = 0.0f);
                }
                else {
                    gb[i] = (b + 128.0f) / -a;
                    ga[i] = 1.0f / a;
                }
            }
            bias = 128;
        }
        else {
            read_float4_array(f, ga, this.Nl[vr]);
            read_float4_array(f, gb, this.Nl[vr]);
            for (int i = 0; i < nl; ++i) {
                if (ga[i] == 0.0) {
                    ga[i] = (gb[i] = 0.0f);
                }
                else {
                    gb[i] = (gb[i] + 128.0f) / -ga[i];
                    ga[i] = 1.0f / ga[i];
                }
            }
            bias = 128;
        }
        int n = this.Nr * this.Nc * this.Nl[vr];
        if (f.read(compdata1, 0, n) != n) {
            return false;
        }
        n = this.Nr * this.Nc * this.Nl[vr];
        for (int i = 0; i < n; ++i) {
            final int n2 = i;
            compdata1[n2] += (byte)bias;
        }
        return true;
    }
    
    boolean read_v5d_header() throws IOException, BadFormException {
        boolean end_of_header = false;
        final RandomAccessFile f = this.FileDesc;
        int order = 0;
        while (true) {
            f.seek(0L);
            f.order(order);
            final int id = f.readInt();
            final int idlen = f.readInt();
            if (id == 1446331402 && idlen == 0) {
                this.FileFormat = 0;
                this.CompressMode = 1;
                while (!end_of_header) {
                    final int tag = f.readInt();
                    final int length = f.readInt();
                    switch (tag) {
                        case 1000: {
                            V5Dassert(length == 10);
                            final byte[] b = new byte[10];
                            f.read(b, 0, 10);
                            int index = 10;
                            for (int q = 0; q < 10; ++q) {
                                if (b[q] == 0) {
                                    index = q;
                                    break;
                                }
                            }
                            this.FileVersion = new String(b, 0, index);
                            if (this.FileVersion.compareTo("4.3") > 0) {
                                System.err.println("Warning: Trying to read a version " + this.FileVersion + " file, you should upgrade Vis5D.");
                                continue;
                            }
                            continue;
                        }
                        case 1001: {
                            V5Dassert(length == 4);
                            this.NumTimes = f.readInt();
                            continue;
                        }
                        case 1002: {
                            V5Dassert(length == 4);
                            this.NumVars = f.readInt();
                            continue;
                        }
                        case 1003: {
                            V5Dassert(length == 14);
                            final int vr = f.readInt();
                            for (int q = 0; q < 10; ++q) {
                                this.VarName[vr][q] = (char)f.readByte();
                            }
                            continue;
                        }
                        case 1004: {
                            V5Dassert(length == 4);
                            this.Nr = f.readInt();
                            continue;
                        }
                        case 1005: {
                            V5Dassert(length == 4);
                            this.Nc = f.readInt();
                            continue;
                        }
                        case 1006: {
                            V5Dassert(length == 4);
                            final int nl = f.readInt();
                            for (int i = 0; i < this.NumVars; ++i) {
                                this.Nl[i] = nl;
                            }
                            continue;
                        }
                        case 1007: {
                            V5Dassert(length == 8);
                            final int vr = f.readInt();
                            this.Nl[vr] = f.readInt();
                            continue;
                        }
                        case 1008: {
                            V5Dassert(length == 8);
                            final int vr = f.readInt();
                            this.LowLev[vr] = f.readInt();
                            continue;
                        }
                        case 1010: {
                            V5Dassert(length == 8);
                            final int time = f.readInt();
                            this.TimeStamp[time] = f.readInt();
                            continue;
                        }
                        case 1011: {
                            V5Dassert(length == 8);
                            final int time = f.readInt();
                            this.DateStamp[time] = f.readInt();
                            continue;
                        }
                        case 1012: {
                            V5Dassert(length == 8);
                            final int vr = f.readInt();
                            this.MinVal[vr] = f.readFloat();
                            continue;
                        }
                        case 1013: {
                            V5Dassert(length == 8);
                            final int vr = f.readInt();
                            this.MaxVal[vr] = f.readFloat();
                            continue;
                        }
                        case 1014: {
                            V5Dassert(length == 4);
                            this.CompressMode = f.readInt();
                            continue;
                        }
                        case 1015: {
                            V5Dassert(length == 24);
                            final int vr = f.readInt();
                            for (int q = 0; q < 20; ++q) {
                                this.Units[vr][q] = (char)f.readByte();
                            }
                            continue;
                        }
                        case 2000: {
                            V5Dassert(length == 4);
                            this.VerticalSystem = f.readInt();
                            if (this.VerticalSystem < 0 || this.VerticalSystem > 3) {
                                System.err.println("Error: bad vertical coordinate system: " + this.VerticalSystem);
                                continue;
                            }
                            continue;
                        }
                        case 2100: {
                            final int numargs = f.readInt();
                            V5Dassert(numargs <= 401);
                            for (int q = 0; q < numargs; ++q) {
                                this.VertArgs[q] = f.readFloat();
                            }
                            V5Dassert(length == numargs * 4 + 4);
                            continue;
                        }
                        case 2003: {
                            V5Dassert(length == 8);
                            final int lev = f.readInt();
                            this.VertArgs[lev] = f.readFloat();
                            continue;
                        }
                        case 2001: {
                            V5Dassert(length == 4);
                            this.VertArgs[0] = f.readFloat();
                            continue;
                        }
                        case 2002: {
                            V5Dassert(length == 4);
                            this.VertArgs[1] = f.readFloat();
                            continue;
                        }
                        case 3000: {
                            V5Dassert(length == 4);
                            this.Projection = f.readInt();
                            if (this.Projection < 0 || this.Projection > 4) {
                                System.err.println("Error while reading header, bad projection (" + this.Projection + ")");
                                return false;
                            }
                            continue;
                        }
                        case 3100: {
                            final int numargs = f.readInt();
                            V5Dassert(numargs <= 801);
                            for (int q = 0; q < numargs; ++q) {
                                this.ProjArgs[q] = f.readFloat();
                            }
                            V5Dassert(length == 4 * numargs + 4);
                            continue;
                        }
                        case 3001: {
                            V5Dassert(length == 4);
                            if (this.Projection == 0 || this.Projection == 1 || this.Projection == 4) {
                                this.ProjArgs[0] = f.readFloat();
                                continue;
                            }
                            f.skipBytes(4);
                            continue;
                        }
                        case 3002: {
                            V5Dassert(length == 4);
                            if (this.Projection == 0 || this.Projection == 1 || this.Projection == 4) {
                                this.ProjArgs[1] = f.readFloat();
                                continue;
                            }
                            f.skipBytes(4);
                            continue;
                        }
                        case 3003: {
                            V5Dassert(length == 4);
                            if (this.Projection == 0 || this.Projection == 1 || this.Projection == 4) {
                                this.ProjArgs[2] = f.readFloat();
                                continue;
                            }
                            f.skipBytes(4);
                            continue;
                        }
                        case 3004: {
                            V5Dassert(length == 4);
                            if (this.Projection == 0 || this.Projection == 1 || this.Projection == 4) {
                                this.ProjArgs[3] = f.readFloat();
                                continue;
                            }
                            if (this.Projection == 2) {
                                this.ProjArgs[5] = f.readFloat();
                                continue;
                            }
                            if (this.Projection == 3) {
                                this.ProjArgs[4] = f.readFloat();
                                continue;
                            }
                            f.skipBytes(4);
                            continue;
                        }
                        case 3005: {
                            V5Dassert(length == 4);
                            if (this.Projection == 2) {
                                this.ProjArgs[0] = f.readFloat();
                                continue;
                            }
                            f.skipBytes(4);
                            continue;
                        }
                        case 3006: {
                            V5Dassert(length == 4);
                            if (this.Projection == 2) {
                                this.ProjArgs[1] = f.readFloat();
                                continue;
                            }
                            f.skipBytes(4);
                            continue;
                        }
                        case 3007: {
                            V5Dassert(length == 4);
                            if (this.Projection == 2) {
                                this.ProjArgs[2] = f.readFloat();
                                continue;
                            }
                            f.skipBytes(4);
                            continue;
                        }
                        case 3008: {
                            V5Dassert(length == 4);
                            if (this.Projection == 2) {
                                this.ProjArgs[3] = f.readFloat();
                                continue;
                            }
                            f.skipBytes(4);
                            continue;
                        }
                        case 3009: {
                            V5Dassert(length == 4);
                            if (this.Projection == 2) {
                                this.ProjArgs[4] = f.readFloat();
                                continue;
                            }
                            if (this.Projection == 3) {
                                this.ProjArgs[1] = f.readFloat();
                                continue;
                            }
                            if (this.Projection == 4) {
                                this.ProjArgs[5] = f.readFloat();
                                continue;
                            }
                            f.skipBytes(4);
                            continue;
                        }
                        case 3010: {
                            V5Dassert(length == 4);
                            if (this.Projection == 3) {
                                this.ProjArgs[0] = f.readFloat();
                                continue;
                            }
                            if (this.Projection == 4) {
                                this.ProjArgs[4] = f.readFloat();
                                continue;
                            }
                            f.skipBytes(4);
                            continue;
                        }
                        case 3011: {
                            V5Dassert(length == 4);
                            if (this.Projection == 3) {
                                this.ProjArgs[2] = f.readFloat();
                                continue;
                            }
                            f.skipBytes(4);
                            continue;
                        }
                        case 3012: {
                            V5Dassert(length == 4);
                            if (this.Projection == 3) {
                                this.ProjArgs[3] = f.readFloat();
                                continue;
                            }
                            f.skipBytes(4);
                            continue;
                        }
                        case 3013: {
                            V5Dassert(length == 4);
                            if (this.Projection == 4) {
                                this.ProjArgs[6] = f.readFloat();
                                continue;
                            }
                            f.skipBytes(4);
                            continue;
                        }
                        case 9999: {
                            end_of_header = true;
                            f.skipBytes(length);
                            continue;
                        }
                        default: {
                            System.err.println("Unknown tag: " + tag + "  length=" + length);
                            f.skipBytes(length);
                            continue;
                        }
                    }
                }
                this.v5dVerifyStruct();
                this.FirstGridPos = (int)f.getFilePointer();
                this.SumGridSizes = 0;
                for (int vr = 0; vr < this.NumVars; ++vr) {
                    this.GridSize[vr] = 8 * this.Nl[vr] + this.v5dSizeofGrid(0, vr);
                    this.SumGridSizes += this.GridSize[vr];
                }
                return true;
            }
            if (id >= -2139062144 && id <= -2139062141) {
                this.FileFormat = id;
                return this.read_comp_header();
            }
            if (order != 0) {
                System.out.println("unknown file type");
                return false;
            }
            order = 1;
        }
    }
    
    boolean v5dReadCompressedGrid(final int time, final int vr, final float[] ga, final float[] gb, final byte[] compdata) throws IOException, BadFormException {
        boolean k = false;
        if (time < 0 || time >= this.NumTimes) {
            throw new IOException("Error in v5dReadCompressedGrid: bad timestep argument (" + time + ")");
        }
        if (vr < 0 || vr >= this.NumVars) {
            throw new IOException("Error in v5dReadCompressedGrid: bad var argument (" + vr + ")");
        }
        if (this.FileFormat != 0) {
            return this.read_comp_grid(time, vr, ga, gb, compdata);
        }
        final int pos = this.grid_position(time, vr);
        this.FileDesc.seek(pos);
        read_float4_array(this.FileDesc, ga, this.Nl[vr]);
        read_float4_array(this.FileDesc, gb, this.Nl[vr]);
        final int n = this.Nr * this.Nc * this.Nl[vr];
        if (this.CompressMode == 1) {
            k = (read_block(this.FileDesc, compdata, n, 1) == n);
        }
        else if (this.CompressMode == 2) {
            k = (read_block(this.FileDesc, compdata, n, 2) == n);
        }
        else if (this.CompressMode == 4) {
            k = (read_block(this.FileDesc, compdata, n, 4) == n);
        }
        if (!k) {
            System.err.println("Error in v5dReadCompressedGrid: read failed, bad file?");
        }
        return k;
    }
    
    boolean v5dReadGrid(final int time, final int vr, final float[] data) throws IOException, BadFormException {
        final float[] ga = new float[400];
        final float[] gb = new float[400];
        if (time < 0 || time >= this.NumTimes) {
            System.err.println("Error in v5dReadGrid: bad timestep argument (" + time + ")");
            return false;
        }
        if (vr < 0 || vr >= this.NumVars) {
            System.err.println("Error in v5dReadGrid: bad variable argument (" + vr + ")");
            return false;
        }
        int bytes;
        if (this.CompressMode == 1) {
            bytes = this.Nr * this.Nc * this.Nl[vr] * 1;
        }
        else if (this.CompressMode == 2) {
            bytes = this.Nr * this.Nc * this.Nl[vr] * 2;
        }
        else {
            if (this.CompressMode != 4) {
                System.err.println("Error in v5dReadGrid: bad compression mode (" + this.CompressMode + ")");
                return false;
            }
            bytes = this.Nr * this.Nc * this.Nl[vr] * 4;
        }
        final byte[] compdata = new byte[bytes];
        if (!this.v5dReadCompressedGrid(time, vr, ga, gb, compdata)) {
            return false;
        }
        this.v5dDecompressGrid(this.Nr, this.Nc, this.Nl[vr], this.CompressMode, compdata, ga, gb, data);
        return true;
    }
    
    boolean write_tag(final int tag, final int length, final boolean newfile) throws IOException {
        if (!newfile && this.CurPos + 8 + length > this.FirstGridPos) {
            System.err.println("Error: out of header space!");
            return false;
        }
        this.FileDesc.writeInt(tag);
        this.FileDesc.writeInt(length);
        this.CurPos += 8 + length;
        return true;
    }
    
    boolean write_v5d_header() throws IOException {
        if (this.FileFormat != 0) {
            System.err.println("Error: v5d library can't write comp5d format files.");
            return false;
        }
        final RandomAccessFile f = this.FileDesc;
        if (!this.v5dVerifyStruct()) {
            return false;
        }
        final boolean newfile = this.FirstGridPos == 0;
        this.SumGridSizes = 0;
        for (int vr = 0; vr < this.NumVars; ++vr) {
            this.GridSize[vr] = 8 * this.Nl[vr] + this.v5dSizeofGrid(0, vr);
            this.SumGridSizes += this.GridSize[vr];
        }
        f.seek(0L);
        this.CurPos = 0;
        if (!this.write_tag(1446331402, 0, newfile)) {
            return false;
        }
        if (!this.write_tag(1000, 10, newfile)) {
            return false;
        }
        f.write("4.3".getBytes(), 0, 10);
        if (!this.write_tag(1001, 4, newfile)) {
            return false;
        }
        f.writeInt(this.NumTimes);
        if (!this.write_tag(1002, 4, newfile)) {
            return false;
        }
        f.writeInt(this.NumVars);
        for (int vr = 0; vr < this.NumVars; ++vr) {
            if (!this.write_tag(1003, 14, newfile)) {
                return false;
            }
            f.writeInt(vr);
            for (int q = 0; q < 10; ++q) {
                f.writeByte((byte)this.VarName[vr][q]);
            }
        }
        for (int vr = 0; vr < this.NumVars; ++vr) {
            if (!this.write_tag(1015, 24, newfile)) {
                return false;
            }
            f.writeInt(vr);
            for (int q = 0; q < 20; ++q) {
                f.writeByte((byte)this.Units[vr][q]);
            }
        }
        for (int time = 0; time < this.NumTimes; ++time) {
            if (!this.write_tag(1010, 8, newfile)) {
                return false;
            }
            f.writeInt(time);
            f.writeInt(this.TimeStamp[time]);
            if (!this.write_tag(1011, 8, newfile)) {
                return false;
            }
            f.writeInt(time);
            f.writeInt(this.DateStamp[time]);
        }
        if (!this.write_tag(1004, 4, newfile)) {
            return false;
        }
        f.writeInt(this.Nr);
        if (!this.write_tag(1005, 4, newfile)) {
            return false;
        }
        f.writeInt(this.Nc);
        int maxnl = 0;
        for (int vr = 0; vr < this.NumVars; ++vr) {
            if (!this.write_tag(1007, 8, newfile)) {
                return false;
            }
            f.writeInt(vr);
            f.writeInt(this.Nl[vr]);
            if (!this.write_tag(1008, 8, newfile)) {
                return false;
            }
            f.writeInt(vr);
            f.writeInt(this.LowLev[vr]);
            if (this.Nl[vr] + this.LowLev[vr] > maxnl) {
                maxnl = this.Nl[vr] + this.LowLev[vr];
            }
        }
        for (int vr = 0; vr < this.NumVars; ++vr) {
            if (!this.write_tag(1012, 8, newfile)) {
                return false;
            }
            f.writeInt(vr);
            f.writeFloat(this.MinVal[vr]);
            if (!this.write_tag(1013, 8, newfile)) {
                return false;
            }
            f.writeInt(vr);
            f.writeFloat(this.MaxVal[vr]);
        }
        if (!this.write_tag(1014, 4, newfile)) {
            return false;
        }
        f.writeInt(this.CompressMode);
        if (!this.write_tag(2000, 4, newfile)) {
            return false;
        }
        f.writeInt(this.VerticalSystem);
        if (!this.write_tag(2100, 1608, newfile)) {
            return false;
        }
        f.writeInt(401);
        for (int q = 0; q < 401; ++q) {
            f.writeFloat(this.VertArgs[q]);
        }
        if (!this.write_tag(3000, 4, newfile)) {
            return false;
        }
        f.writeInt(this.Projection);
        if (!this.write_tag(3100, 3208, newfile)) {
            return false;
        }
        f.writeInt(801);
        for (int q = 0; q < 801; ++q) {
            f.writeFloat(this.ProjArgs[q]);
        }
        if (newfile) {
            if (!this.write_tag(9999, 10000, newfile)) {
                return false;
            }
            f.skipBytes(10000);
            this.FirstGridPos = (int)f.getFilePointer();
        }
        else {
            final int filler = this.FirstGridPos - (int)f.getFilePointer();
            if (!this.write_tag(9999, filler - 8, newfile)) {
                return false;
            }
        }
        return true;
    }
    
    boolean v5dCreateFile(final String filename) throws IOException {
        final RandomAccessFile fd = new RandomAccessFile(filename, "rw");
        if (fd == null) {
            System.err.println("Error in v5dCreateFile: open failed");
            this.FileDesc = null;
            this.Mode = '\0';
            return false;
        }
        this.FileDesc = fd;
        this.Mode = 'w';
        return this.write_v5d_header();
    }
    
    boolean v5dWriteCompressedGrid(final int time, final int vr, final float[] ga, final float[] gb, final byte[] compdata) throws IOException, BadFormException {
        if (this.Mode != 'w') {
            System.err.println("Error in v5dWriteCompressedGrid: file opened for reading, not writing.");
            return false;
        }
        if (time < 0 || time >= this.NumTimes) {
            System.err.println("Error in v5dWriteCompressedGrid: bad timestep argument (" + time + ")");
            return false;
        }
        if (vr < 0 || vr >= this.NumVars) {
            System.err.println("Error in v5dWriteCompressedGrid: bad variable argument (" + vr + ")");
            return false;
        }
        final int pos = this.grid_position(time, vr);
        this.FileDesc.seek(pos);
        boolean k = false;
        for (int q = 0; q < this.Nl[vr]; ++q) {
            this.FileDesc.writeFloat(ga[q]);
        }
        for (int q = 0; q < this.Nl[vr]; ++q) {
            this.FileDesc.writeFloat(gb[q]);
        }
        final int n = this.Nr * this.Nc * this.Nl[vr];
        if (this.CompressMode == 1) {
            k = (write_block(this.FileDesc, compdata, n, 1) == n);
        }
        else if (this.CompressMode == 2) {
            k = (write_block(this.FileDesc, compdata, n, 2) == n);
        }
        else if (this.CompressMode == 4) {
            k = (write_block(this.FileDesc, compdata, n, 4) == n);
        }
        if (!k) {
            System.err.println("Error in v5dWrite[Compressed]Grid: write failed, disk full?");
        }
        return k;
    }
    
    boolean v5dWriteGrid(final int time, final int vr, final float[] data) throws IOException, BadFormException {
        final float[] ga = new float[400];
        final float[] gb = new float[400];
        if (this.Mode != 'w') {
            System.err.println("Error in v5dWriteGrid: file opened for reading, not writing.");
            return false;
        }
        if (time < 0 || time >= this.NumTimes) {
            System.err.println("Error in v5dWriteGrid: bad timestep argument (" + time + ")");
            return false;
        }
        if (vr < 0 || vr >= this.NumVars) {
            System.err.println("Error in v5dWriteGrid: bad variable argument (" + vr + ")");
            return false;
        }
        int bytes;
        if (this.CompressMode == 1) {
            bytes = this.Nr * this.Nc * this.Nl[vr] * 2;
        }
        else if (this.CompressMode == 2) {
            bytes = this.Nr * this.Nc * this.Nl[vr] * 2;
        }
        else {
            if (this.CompressMode != 4) {
                System.err.println("Error in v5dWriteGrid: bad compression mode (" + this.CompressMode + ")");
                return false;
            }
            bytes = this.Nr * this.Nc * this.Nl[vr] * 4;
        }
        final byte[] compdata = new byte[bytes];
        final float[] min1 = { 0.0f };
        final float[] max1 = { 0.0f };
        v5dCompressGrid(this.Nr, this.Nc, this.Nl[vr], this.CompressMode, data, compdata, ga, gb, min1, max1);
        final float min2 = min1[0];
        final float max2 = max1[0];
        if (min2 < this.MinVal[vr]) {
            this.MinVal[vr] = min2;
        }
        if (max2 > this.MaxVal[vr]) {
            this.MaxVal[vr] = max2;
        }
        return this.v5dWriteCompressedGrid(time, vr, ga, gb, compdata);
    }
    
    boolean v5dCloseFile() throws IOException {
        boolean status = true;
        if (this.Mode == 'w') {
            this.FileDesc.seek(0L);
            status = this.write_v5d_header();
            this.FileDesc.seek(this.FileDesc.length());
            this.FileDesc.close();
        }
        else {
            if (this.Mode != 'r') {
                System.err.println("Error in v5dCloseFile: bad V5DStruct argument");
                return false;
            }
            this.FileDesc.close();
        }
        this.FileDesc = null;
        this.Mode = '\0';
        return status;
    }
    
    static {
        V5DStruct.SIMPLE_COMPRESSION = false;
        V5DStruct.KLUDGE = false;
        V5DStruct.ORIGINAL = false;
    }
}
