// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.iosp.mcidas;

import ucar.ma2.InvalidRangeException;
import ucar.ma2.Index;
import ucar.ma2.Range;
import java.io.IOException;
import ucar.ma2.Section;
import ucar.unidata.geoloc.ProjectionImpl;
import java.util.List;
import java.util.Date;
import ucar.ma2.ArrayChar;
import ucar.unidata.util.Parameter;
import ucar.ma2.Array;
import ucar.ma2.ArrayInt;
import ucar.nc2.Attribute;
import ucar.ma2.DataType;
import ucar.nc2.Structure;
import ucar.nc2.Variable;
import ucar.nc2.Group;
import java.util.ArrayList;
import ucar.nc2.Dimension;
import edu.wisc.ssec.mcidas.CalibratorException;
import edu.wisc.ssec.mcidas.CalibratorFactory;
import edu.wisc.ssec.mcidas.McIDASUtil;
import edu.wisc.ssec.mcidas.McIDASException;
import edu.wisc.ssec.mcidas.AreaFileException;
import ucar.nc2.units.DateFormatter;
import ucar.nc2.NetcdfFile;
import ucar.unidata.io.RandomAccessFile;
import edu.wisc.ssec.mcidas.Calibrator;
import edu.wisc.ssec.mcidas.AreaDirectory;
import edu.wisc.ssec.mcidas.AREAnav;
import edu.wisc.ssec.mcidas.AreaFile;

public class AreaReader
{
    private AreaFile af;
    private AREAnav nav;
    private int[] dirBlock;
    private int[] navBlock;
    private AreaDirectory ad;
    Calibrator calibrator;
    int[] bandMap;
    private float calScale;
    private String calUnit;
    
    public AreaReader() {
        this.calibrator = null;
        this.bandMap = null;
        this.calScale = 1.0f;
    }
    
    public boolean init(final RandomAccessFile raf, final NetcdfFile ncfile) throws AreaFileException {
        this.af = new AreaFile(raf.getLocation());
        this.dirBlock = this.af.getDir();
        this.ad = this.af.getAreaDirectory();
        final int numElements = this.ad.getElements();
        final int numLines = this.ad.getLines();
        final int numBands = this.ad.getNumberOfBands();
        this.bandMap = this.ad.getBands();
        this.navBlock = this.af.getNav();
        final Date nomTime = this.ad.getNominalTime();
        final DateFormatter df = new DateFormatter();
        try {
            this.nav = AREAnav.makeAreaNav(this.navBlock, this.af.getAux());
        }
        catch (McIDASException me) {
            throw new AreaFileException(me.getMessage());
        }
        final int[] dirBlock = this.dirBlock;
        final AreaFile af = this.af;
        final int sensor = dirBlock[2];
        final int[] dirBlock2 = this.dirBlock;
        final AreaFile af2 = this.af;
        final String calName = McIDASUtil.intBitsToString(dirBlock2[52]);
        final int calType = this.getCalType(calName);
        if (this.af.getCal() != null && CalibratorFactory.hasCalibrator(sensor)) {
            try {
                this.calibrator = CalibratorFactory.getCalibrator(sensor, calType, this.af.getCal());
            }
            catch (CalibratorException ce) {
                this.calibrator = null;
            }
        }
        this.calUnit = this.ad.getCalibrationUnitName();
        this.calScale = 1.0f / this.ad.getCalibrationScaleFactor();
        final Dimension elements = new Dimension("elements", numElements, true);
        final Dimension lines = new Dimension("lines", numLines, true);
        final Dimension bands = new Dimension("bands", numBands, true);
        final Dimension time = new Dimension("time", 1, true);
        final String name = "dirSize";
        final AreaFile af3 = this.af;
        final Dimension dirDim = new Dimension(name, 64, true);
        final Dimension navDim = new Dimension("navSize", this.navBlock.length, true);
        final List<Dimension> image = new ArrayList<Dimension>();
        image.add(time);
        image.add(bands);
        image.add(lines);
        image.add(elements);
        ncfile.addDimension(null, elements);
        ncfile.addDimension(null, lines);
        ncfile.addDimension(null, bands);
        ncfile.addDimension(null, time);
        ncfile.addDimension(null, dirDim);
        ncfile.addDimension(null, navDim);
        final Variable timeVar = new Variable(ncfile, null, null, "time");
        timeVar.setDataType(DataType.INT);
        timeVar.setDimensions("time");
        timeVar.addAttribute(new Attribute("units", "seconds since " + df.toDateTimeString(nomTime)));
        timeVar.addAttribute(new Attribute("long_name", "time"));
        Array varArray = new ArrayInt.D1(1);
        ((ArrayInt.D1)varArray).set(0, 0);
        timeVar.setCachedData(varArray, false);
        ncfile.addVariable(null, timeVar);
        final Variable lineVar = new Variable(ncfile, null, null, "lines");
        lineVar.setDataType(DataType.INT);
        lineVar.setDimensions("lines");
        lineVar.addAttribute(new Attribute("standard_name", "projection_y_coordinate"));
        varArray = new ArrayInt.D1(numLines);
        for (int i = 0; i < numLines; ++i) {
            final int pos = this.nav.isFlippedLineCoordinates() ? i : (numLines - i - 1);
            ((ArrayInt.D1)varArray).set(i, pos);
        }
        lineVar.setCachedData(varArray, false);
        ncfile.addVariable(null, lineVar);
        final Variable elementVar = new Variable(ncfile, null, null, "elements");
        elementVar.setDataType(DataType.INT);
        elementVar.setDimensions("elements");
        elementVar.addAttribute(new Attribute("standard_name", "projection_x_coordinate"));
        varArray = new ArrayInt.D1(numElements);
        for (int j = 0; j < numElements; ++j) {
            ((ArrayInt.D1)varArray).set(j, j);
        }
        elementVar.setCachedData(varArray, false);
        ncfile.addVariable(null, elementVar);
        final Variable bandVar = new Variable(ncfile, null, null, "bands");
        bandVar.setDataType(DataType.INT);
        bandVar.setDimensions("bands");
        bandVar.addAttribute(new Attribute("long_name", "spectral band number"));
        bandVar.addAttribute(new Attribute("axis", "Z"));
        final Array bandArray = new ArrayInt.D1(numBands);
        for (int k = 0; k < numBands; ++k) {
            ((ArrayInt.D1)bandArray).set(k, this.bandMap[k]);
        }
        bandVar.setCachedData(bandArray, false);
        ncfile.addVariable(null, bandVar);
        final Variable imageVar = new Variable(ncfile, null, null, "image");
        imageVar.setDataType(DataType.INT);
        imageVar.setDimensions(image);
        this.setCalTypeAttributes(imageVar, this.getCalType(calName));
        final Variable variable = imageVar;
        final AreaFile af4 = this.af;
        variable.addAttribute(new Attribute(this.getADDescription(52), calName));
        imageVar.addAttribute(new Attribute("bands", bandArray));
        imageVar.addAttribute(new Attribute("grid_mapping", "AREAnav"));
        ncfile.addVariable(null, imageVar);
        final Variable dirVar = new Variable(ncfile, null, null, "areaDirectory");
        dirVar.setDataType(DataType.INT);
        dirVar.setDimensions("dirSize");
        this.setAreaDirectoryAttributes(dirVar);
        final ArrayInt.D1 dirArray = new ArrayInt.D1(64);
        for (int l = 0; l < 64; ++l) {
            dirArray.set(l, this.dirBlock[l]);
        }
        dirVar.setCachedData(dirArray, false);
        ncfile.addVariable(null, dirVar);
        final Variable navVar = new Variable(ncfile, null, null, "navBlock");
        navVar.setDataType(DataType.INT);
        navVar.setDimensions("navSize");
        this.setNavBlockAttributes(navVar);
        final ArrayInt.D1 navArray = new ArrayInt.D1(this.navBlock.length);
        for (int m = 0; m < this.navBlock.length; ++m) {
            navArray.set(m, this.navBlock[m]);
        }
        navVar.setCachedData(navArray, false);
        ncfile.addVariable(null, navVar);
        final ProjectionImpl projection = new McIDASAreaProjection(this.af);
        final Variable proj = new Variable(ncfile, null, null, "AREAnav");
        proj.setDataType(DataType.CHAR);
        proj.setDimensions("");
        final List params = projection.getProjectionParameters();
        for (int i2 = 0; i2 < params.size(); ++i2) {
            final Parameter p = params.get(i2);
            proj.addAttribute(new Attribute(p));
        }
        proj.addAttribute(new Attribute("grid_mapping_name", McIDASAreaProjection.GRID_MAPPING_NAME));
        varArray = new ArrayChar.D0();
        ((ArrayChar.D0)varArray).set(' ');
        proj.setCachedData(varArray, false);
        ncfile.addVariable(null, proj);
        ncfile.addAttribute(null, new Attribute("Conventions", "CF-1.0"));
        ncfile.addAttribute(null, new Attribute("netCDF-Java", "4.0"));
        ncfile.addAttribute(null, new Attribute("nominal_image_time", df.toDateTimeString(nomTime)));
        final String encStr = "netCDF encoded on " + df.toDateTimeString(new Date());
        ncfile.addAttribute(null, new Attribute("history", encStr));
        ncfile.finish();
        return true;
    }
    
    public static boolean isValidFile(final RandomAccessFile raf) {
        final String fileName = raf.getLocation();
        try {
            final AreaFile af = new AreaFile(fileName);
            return true;
        }
        catch (AreaFileException e) {
            return false;
        }
    }
    
    public Array readVariable(final Variable v2, final Section section) throws IOException, InvalidRangeException {
        Range timeRange = null;
        Range bandRange = null;
        Range geoXRange = null;
        Range geoYRange = null;
        if (section != null & section.getRank() > 0) {
            if (section.getRank() > 3) {
                timeRange = section.getRange(0);
                bandRange = section.getRange(1);
                geoYRange = section.getRange(2);
                geoXRange = section.getRange(3);
            }
            else if (section.getRank() > 2) {
                timeRange = section.getRange(0);
                geoYRange = section.getRange(1);
                geoXRange = section.getRange(2);
            }
            else if (section.getRank() > 1) {
                geoYRange = section.getRange(0);
                geoXRange = section.getRange(1);
            }
            else {
                geoXRange = section.getRange(0);
            }
        }
        final String varname = v2.getName();
        final Array dataArray = Array.factory(v2.getDataType().getPrimitiveClassType(), section.getShape());
        final Index dataIndex = dataArray.getIndex();
        if (varname.equals("latitude") || varname.equals("longitude")) {
            final double[][] pixel = new double[2][1];
            final double[][][] latLonValues = new double[geoXRange.length()][geoYRange.length()][2];
            for (int i = 0; i < geoXRange.length(); ++i) {
                for (int j = 0; j < geoYRange.length(); ++j) {
                    pixel[0][0] = geoXRange.element(i);
                    pixel[1][0] = geoYRange.element(j);
                    final double[][] latLon = this.nav.toLatLon(pixel);
                    if (varname.equals("lat")) {
                        dataArray.setFloat(dataIndex.set(j, i), (float)latLon[0][0]);
                    }
                    else {
                        dataArray.setFloat(dataIndex.set(j, i), (float)latLon[1][0]);
                    }
                }
            }
        }
        if (varname.equals("image")) {
            try {
                int[][] pixelData = new int[1][1];
                if (bandRange != null) {
                    for (int k = 0; k < bandRange.length(); ++k) {
                        final int bandIndex = bandRange.element(k) + 1;
                        for (int l = 0; l < geoYRange.length(); ++l) {
                            for (int m = 0; m < geoXRange.length(); ++m) {
                                pixelData = this.af.getData(geoYRange.element(l), geoXRange.element(m), 1, 1, bandIndex);
                                dataArray.setInt(dataIndex.set(0, k, l, m), pixelData[0][0]);
                            }
                        }
                    }
                }
                else {
                    for (int j2 = 0; j2 < geoYRange.length(); ++j2) {
                        for (int i2 = 0; i2 < geoXRange.length(); ++i2) {
                            pixelData = this.af.getData(geoYRange.element(j2), geoXRange.element(i2), 1, 1);
                            dataArray.setInt(dataIndex.set(0, j2, i2), pixelData[0][0]);
                        }
                    }
                }
            }
            catch (AreaFileException afe) {
                throw new IOException(afe.toString());
            }
        }
        return dataArray;
    }
    
    private void setAreaDirectoryAttributes(final Variable v) {
        if (this.dirBlock == null || this.ad == null) {
            return;
        }
        for (int i = 1; i < 14; ++i) {
            if (i != 7) {
                v.addAttribute(new Attribute(this.getADDescription(i), new Integer(this.dirBlock[i])));
            }
        }
    }
    
    private void setNavBlockAttributes(final Variable v) {
        if (this.navBlock == null || this.ad == null) {
            return;
        }
        v.addAttribute(new Attribute("navigation_type", McIDASUtil.intBitsToString(this.navBlock[0])));
    }
    
    private String getADDescription(final int index) {
        String desc = "dir(" + index + ")";
        switch (index) {
            case 0: {
                desc = "relative position of the image object in the ADDE dataset";
                break;
            }
            case 1: {
                desc = "AREA version";
                break;
            }
            case 2: {
                desc = "SSEC sensor source number";
                break;
            }
            case 3: {
                desc = "nominal year and Julian day of the image (yyyddd)";
                break;
            }
            case 4: {
                desc = "nominal time of the image (hhmmss)";
                break;
            }
            case 5: {
                desc = "upper-left image line coordinate";
                break;
            }
            case 6: {
                desc = "upper-left image element coordinate";
                break;
            }
            case 8: {
                desc = "number of lines in the image";
                break;
            }
            case 9: {
                desc = "number of data points per line";
                break;
            }
            case 10: {
                desc = "number of bytes per data point";
                break;
            }
            case 11: {
                desc = "line resolution";
                break;
            }
            case 12: {
                desc = "element resolution";
                break;
            }
            case 13: {
                desc = "number of spectral bands";
                break;
            }
            case 14: {
                desc = "length of the line prefix";
                break;
            }
            case 15: {
                desc = "SSEC project number used when creating the file";
                break;
            }
            case 16: {
                desc = "year and Julian day the image file was created (yyyddd)";
                break;
            }
            case 17: {
                desc = "image file creation time (hhmmss)";
                break;
            }
            case 18: {
                desc = "spectral band map: bands 1-32";
                break;
            }
            case 33: {
                desc = "byte offset to the start of the data block";
                break;
            }
            case 34: {
                desc = "byte offset to the start of the navigation block";
                break;
            }
            case 35: {
                desc = "validity code";
                break;
            }
            case 45: {
                desc = "actual image start year and Julian day (yyyddd)";
                break;
            }
            case 46: {
                desc = "actual image start time (hhmmss) in milliseconds for POES data";
                break;
            }
            case 47: {
                desc = "actual image start scan";
                break;
            }
            case 48: {
                desc = "length of the prefix documentation";
                break;
            }
            case 49: {
                desc = "length of the prefix calibration";
                break;
            }
            case 50: {
                desc = "length of the prefix band list";
                break;
            }
            case 51: {
                desc = "source type";
                break;
            }
            case 52: {
                desc = "calibration type";
                break;
            }
            case 56: {
                desc = "original source type";
                break;
            }
            case 57: {
                desc = "calibration unit";
                break;
            }
            case 58: {
                desc = "calibration scaling";
                break;
            }
            case 59: {
                desc = "byte offset to the supplemental block";
                break;
            }
            case 62: {
                desc = "byte offset to the calibration block";
                break;
            }
            case 63: {
                desc = "number of comment cards";
                break;
            }
        }
        desc = desc.replaceAll("\\s", "_");
        return desc;
    }
    
    private int getCalType(final String calName) {
        int calTypeOut = -1;
        if (calName.trim().equals("ALB")) {
            calTypeOut = 3;
        }
        else if (calName.trim().equals("BRIT")) {
            calTypeOut = 5;
        }
        else if (calName.trim().equals("RAD")) {
            calTypeOut = 2;
        }
        else if (calName.trim().equals("RAW")) {
            calTypeOut = 1;
        }
        else if (calName.trim().equals("TEMP")) {
            calTypeOut = 4;
        }
        return calTypeOut;
    }
    
    private void setCalTypeAttributes(final Variable image, final int calType) {
        String longName = "image values";
        final String unit = "";
        switch (calType) {
            case 3: {
                longName = "albedo";
                break;
            }
            case 5: {
                longName = "brightness values";
                break;
            }
            case 4: {
                longName = "temperature";
                break;
            }
            case 2: {
                longName = "pixel radiance values";
                break;
            }
            case 1: {
                longName = "raw image values";
                break;
            }
        }
        image.addAttribute(new Attribute("long_name", longName));
        if (this.calUnit != null) {
            image.addAttribute(new Attribute("units", this.calUnit));
        }
        if (this.calScale != 1.0f) {
            image.addAttribute(new Attribute("scale_factor", this.calScale));
        }
    }
}
