// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.iosp.mcidas;

import ucar.nc2.iosp.IOServiceProvider;
import ucar.grid.GridRecord;
import ucar.grid.GridTableLookup;
import ucar.nc2.iosp.grid.GridIndexToNC;
import ucar.grid.GridIndex;
import ucar.nc2.util.CancelTask;
import ucar.nc2.NetcdfFile;
import java.io.IOException;
import ucar.unidata.io.RandomAccessFile;
import ucar.nc2.iosp.grid.GridServiceProvider;

public class McIDASGridServiceProvider extends GridServiceProvider
{
    protected McIDASGridReader mcGridReader;
    
    public boolean isValidFile(final RandomAccessFile raf) throws IOException {
        try {
            (this.mcGridReader = new McIDASGridReader()).init(raf, false);
        }
        catch (IOException ioe) {
            return false;
        }
        return true;
    }
    
    public String getFileTypeId() {
        return "McIDASGrid";
    }
    
    public String getFileTypeDescription() {
        return "McIDAS Grid file";
    }
    
    @Override
    public void open(final RandomAccessFile raf, final NetcdfFile ncfile, final CancelTask cancelTask) throws IOException {
        super.open(raf, ncfile, cancelTask);
        final long start = System.currentTimeMillis();
        if (this.mcGridReader == null) {
            this.mcGridReader = new McIDASGridReader();
        }
        this.mcGridReader.init(raf);
        final GridIndex index = this.mcGridReader.getGridIndex();
        this.open(index, cancelTask);
        if (McIDASGridServiceProvider.debugOpen) {
            System.out.println(" GridServiceProvider.open " + ncfile.getLocation() + " took " + (System.currentTimeMillis() - start));
        }
    }
    
    @Override
    protected void open(final GridIndex index, final CancelTask cancelTask) throws IOException {
        final McIDASLookup lookup = new McIDASLookup(index.getGridRecords().get(0));
        final GridIndexToNC delegate = new GridIndexToNC(index.filename);
        delegate.open(index, (GridTableLookup)lookup, 4, this.ncfile, this.fmrcCoordSys, cancelTask);
        this.ncfile.finish();
    }
    
    @Override
    public boolean sync() {
        try {
            if (!this.mcGridReader.init()) {
                return false;
            }
            final GridIndex index = this.mcGridReader.getGridIndex();
            this.ncfile.empty();
            this.open(index, null);
            return true;
        }
        catch (IOException ioe) {
            return false;
        }
    }
    
    @Override
    protected float[] _readData(final GridRecord gr) throws IOException {
        return this.mcGridReader.readGrid((McIDASGridRecord)gr);
    }
    
    public static void main(final String[] args) throws IOException {
        final IOServiceProvider mciosp = new McIDASGridServiceProvider();
        final RandomAccessFile rf = new RandomAccessFile(args[0], "r", 2048);
        final NetcdfFile ncfile = new MakeNetcdfFile(mciosp, rf, args[0], null);
    }
    
    static class MakeNetcdfFile extends NetcdfFile
    {
        MakeNetcdfFile(final IOServiceProvider spi, final RandomAccessFile raf, final String location, final CancelTask cancelTask) throws IOException {
            super(spi, raf, location, cancelTask);
        }
    }
}
