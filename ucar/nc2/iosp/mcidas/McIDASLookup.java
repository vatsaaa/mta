// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.iosp.mcidas;

import java.util.Date;
import ucar.grib.grib1.GribPDSLevel;
import visad.jmet.MetUnits;
import ucar.grid.GridParameter;
import ucar.grid.GridRecord;
import ucar.grid.GridDefRecord;
import ucar.grid.GridTableLookup;

public final class McIDASLookup implements GridTableLookup
{
    private McIDASGridRecord sample;
    
    public McIDASLookup(final McIDASGridRecord sample) {
        this.sample = sample;
    }
    
    public String getShapeName(final GridDefRecord gds) {
        return "Spherical";
    }
    
    public final String getGridName(final GridDefRecord gds) {
        return gds.toString();
    }
    
    public final GridParameter getParameter(final GridRecord gr) {
        final McIDASGridRecord mgr = (McIDASGridRecord)gr;
        final String name = mgr.getParameterName();
        String desc = mgr.getGridDescription();
        if (desc.trim().equals("")) {
            desc = name;
        }
        final String unit = MetUnits.makeSymbol(mgr.getParamUnitName());
        return new GridParameter(0, name, desc, unit);
    }
    
    public final String getDisciplineName(final GridRecord gr) {
        return "Meteorological Products";
    }
    
    public final String getCategoryName(final GridRecord gr) {
        return "Meteorological Parameters";
    }
    
    public final String getLevelName(final GridRecord gr) {
        final String levelUnit = this.getLevelUnit(gr);
        final int level1 = (int)gr.getLevel1();
        final int level2 = (int)gr.getLevel2();
        final int levelType = gr.getLevelType1();
        if (((McIDASGridRecord)gr).hasGribInfo()) {
            return GribPDSLevel.getNameShort(levelType);
        }
        if (levelUnit.equalsIgnoreCase("hPa")) {
            return "pressure";
        }
        if (level1 == 1013) {
            return "mean sea level";
        }
        if (level1 == 0) {
            return "tropopause";
        }
        if (level1 == 1001) {
            return "surface";
        }
        if (level2 != 0) {
            return "layer";
        }
        return "";
    }
    
    public final String getLevelDescription(final GridRecord gr) {
        if (((McIDASGridRecord)gr).hasGribInfo()) {
            return GribPDSLevel.getNameShort(gr.getLevelType1());
        }
        return this.getLevelName(gr);
    }
    
    public final String getLevelUnit(final GridRecord gr) {
        return MetUnits.makeSymbol(((McIDASGridRecord)gr).getLevelUnitName());
    }
    
    public final String getTimeRangeUnitName(final int tunit) {
        return "hour";
    }
    
    public final Date getFirstBaseTime() {
        return this.sample.getReferenceTime();
    }
    
    public final boolean isLatLon(final GridDefRecord gds) {
        return this.getProjectionName(gds).equals("EQUI");
    }
    
    public final int getProjectionType(final GridDefRecord gds) {
        final String name = this.getProjectionName(gds).trim();
        if (name.equals("MERC")) {
            return 3;
        }
        if (name.equals("CONF")) {
            return 2;
        }
        if (name.equals("PS")) {
            return 1;
        }
        return -1;
    }
    
    public final boolean isVerticalCoordinate(final GridRecord gr) {
        final int type = gr.getLevelType1();
        if (((McIDASGridRecord)gr).hasGribInfo()) {
            if (type == 20) {
                return true;
            }
            if (type == 100) {
                return true;
            }
            if (type == 101) {
                return true;
            }
            if (type >= 103 && type <= 128) {
                return true;
            }
            if (type == 141) {
                return true;
            }
            if (type == 160) {
                return true;
            }
        }
        else if (this.getLevelUnit(gr).equals("hPa")) {
            return true;
        }
        return false;
    }
    
    public final boolean isPositiveUp(final GridRecord gr) {
        final int type = gr.getLevelType1();
        if (((McIDASGridRecord)gr).hasGribInfo()) {
            if (type == 103) {
                return true;
            }
            if (type == 104) {
                return true;
            }
            if (type == 105) {
                return true;
            }
            if (type == 106) {
                return true;
            }
            if (type == 111) {
                return true;
            }
            if (type == 112) {
                return true;
            }
            if (type == 125) {
                return true;
            }
            if (this.getLevelUnit(gr).equals("hPa")) {
                return false;
            }
        }
        return true;
    }
    
    public final float getFirstMissingValue() {
        return -2.13906214E9f;
    }
    
    public boolean isLayer(final GridRecord gr) {
        return gr.getLevel2() != 0.0;
    }
    
    private String getProjectionName(final GridDefRecord gds) {
        return gds.getParam("ProjFlag");
    }
    
    public final String getTitle() {
        return "GRID data";
    }
    
    public String getInstitution() {
        return null;
    }
    
    public final String getSource() {
        return null;
    }
    
    public final String getComment() {
        return null;
    }
    
    public String getGridType() {
        return "McIDAS";
    }
}
