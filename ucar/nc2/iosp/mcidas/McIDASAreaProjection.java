// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.iosp.mcidas;

import java.awt.geom.Rectangle2D;
import ucar.unidata.geoloc.ProjectionRect;
import ucar.unidata.geoloc.LatLonPointImpl;
import ucar.unidata.geoloc.ProjectionPoint;
import ucar.unidata.geoloc.ProjectionPointImpl;
import ucar.unidata.geoloc.LatLonPoint;
import ucar.unidata.util.Parameter;
import edu.wisc.ssec.mcidas.McIDASException;
import edu.wisc.ssec.mcidas.AreaFile;
import edu.wisc.ssec.mcidas.AREAnav;
import ucar.unidata.geoloc.ProjectionImpl;

public class McIDASAreaProjection extends ProjectionImpl
{
    public static String ATTR_AREADIR;
    public static String ATTR_NAVBLOCK;
    public static String ATTR_AUXBLOCK;
    public static String GRID_MAPPING_NAME;
    private AREAnav anav;
    private int lines;
    private int elements;
    private int[] dirBlock;
    private int[] navBlock;
    private int[] auxBlock;
    
    @Override
    public ProjectionImpl constructCopy() {
        return new McIDASAreaProjection(this.dirBlock, this.navBlock, this.auxBlock);
    }
    
    public McIDASAreaProjection() {
        this.anav = null;
    }
    
    public McIDASAreaProjection(final AreaFile af) {
        this(af.getDir(), af.getNav(), af.getAux());
    }
    
    public McIDASAreaProjection(final int[] dir, final int[] nav) {
        this(dir, nav, null);
    }
    
    public McIDASAreaProjection(final int[] dir, final int[] nav, final int[] aux) {
        this.anav = null;
        try {
            this.anav = AREAnav.makeAreaNav(nav, aux);
        }
        catch (McIDASException excp) {
            throw new IllegalArgumentException("McIDASAreaProjection: problem creating projection" + excp);
        }
        this.dirBlock = dir;
        this.navBlock = nav;
        this.auxBlock = aux;
        this.anav.setImageStart(dir[5], dir[6]);
        this.anav.setRes(dir[11], dir[12]);
        this.anav.setStart(0, 0);
        this.anav.setMag(1, 1);
        this.lines = dir[8];
        this.elements = dir[9];
        this.anav.setFlipLineCoordinates(dir[8]);
        this.addParameter("grid_mapping_name", McIDASAreaProjection.GRID_MAPPING_NAME);
        this.addParameter(new Parameter(McIDASAreaProjection.ATTR_AREADIR, this.makeDoubleArray(dir)));
        this.addParameter(new Parameter(McIDASAreaProjection.ATTR_NAVBLOCK, this.makeDoubleArray(nav)));
        if (aux != null) {
            this.addParameter(new Parameter(McIDASAreaProjection.ATTR_AUXBLOCK, this.makeDoubleArray(aux)));
        }
    }
    
    public int[] getDirBlock() {
        return this.dirBlock;
    }
    
    public int[] getNavBlock() {
        return this.navBlock;
    }
    
    public int[] getAuxBlock() {
        return this.auxBlock;
    }
    
    @Override
    public ProjectionPoint latLonToProj(final LatLonPoint latLon, final ProjectionPointImpl result) {
        final double fromLat = latLon.getLatitude();
        final double fromLon = latLon.getLongitude();
        final double[][] xy = this.anav.toLinEle(new double[][] { { fromLat }, { fromLon } });
        final double toX = xy[0][0];
        final double toY = xy[1][0];
        result.setLocation(toX, toY);
        return result;
    }
    
    @Override
    public LatLonPoint projToLatLon(final ProjectionPoint world, final LatLonPointImpl result) {
        final double fromX = world.getX();
        final double fromY = world.getY();
        final double[][] latlon = this.anav.toLatLon(new double[][] { { fromX }, { fromY } });
        final double toLat = latlon[0][0];
        final double toLon = latlon[1][0];
        result.setLatitude(toLat);
        result.setLongitude(toLon);
        return result;
    }
    
    @Override
    public float[][] latLonToProj(final float[][] from, final float[][] to, final int latIndex, final int lonIndex) {
        final float[] fromLatA = from[latIndex];
        final float[] fromLonA = from[lonIndex];
        final float[][] xy = this.anav.toLinEle(new float[][] { fromLatA, fromLonA });
        to[0] = xy[0];
        to[1] = xy[1];
        return to;
    }
    
    @Override
    public float[][] projToLatLon(final float[][] from, final float[][] to) {
        final float[] fromXA = from[0];
        final float[] fromYA = from[1];
        final float[][] latlon = this.anav.toLatLon(new float[][] { fromXA, fromYA });
        to[0] = latlon[0];
        to[1] = latlon[1];
        return to;
    }
    
    @Override
    public double[][] latLonToProj(final double[][] from, final double[][] to, final int latIndex, final int lonIndex) {
        final double[] fromLatA = from[latIndex];
        final double[] fromLonA = from[lonIndex];
        final double[][] xy = this.anav.toLinEle(new double[][] { fromLatA, fromLonA });
        to[0] = xy[0];
        to[1] = xy[1];
        return to;
    }
    
    @Override
    public double[][] projToLatLon(final double[][] from, final double[][] to) {
        final double[] fromXA = from[0];
        final double[] fromYA = from[1];
        final double[][] latlon = this.anav.toLatLon(new double[][] { fromXA, fromYA });
        to[0] = latlon[0];
        to[1] = latlon[1];
        return to;
    }
    
    @Override
    public ProjectionRect getDefaultMapArea() {
        return new ProjectionRect(new Rectangle2D.Float(0.0f, 0.0f, (float)this.elements, (float)this.lines));
    }
    
    @Override
    public boolean crossSeam(final ProjectionPoint pt1, final ProjectionPoint pt2) {
        return ProjectionPointImpl.isInfinite(pt1) || ProjectionPointImpl.isInfinite(pt2) || (Double.isNaN(pt1.getX()) || Double.isNaN(pt1.getY()) || Double.isNaN(pt2.getX()) || Double.isNaN(pt2.getY())) || (pt1.getX() * pt2.getX() < 0.0 && Math.abs(pt1.getX() - pt2.getX()) > 5000.0);
    }
    
    @Override
    public boolean equals(final Object obj) {
        if (!(obj instanceof McIDASAreaProjection)) {
            return false;
        }
        final McIDASAreaProjection that = (McIDASAreaProjection)obj;
        return this == that || (this.anav.equals((Object)that.anav) && this.lines == that.lines && this.elements == that.elements);
    }
    
    @Override
    public String toString() {
        return "Image (" + this.anav.toString() + ") Projection";
    }
    
    @Override
    public String paramsToString() {
        return " nav " + this.anav.toString();
    }
    
    public static void main(final String[] args) throws Exception {
        final String file = (args.length > 0) ? args[0] : "c:/data/satellite/AREA8760";
        final AreaFile af = new AreaFile(file);
        final McIDASAreaProjection proj = new McIDASAreaProjection(af);
        LatLonPoint llp = new LatLonPointImpl(45.0, -105.0);
        System.out.println("lat/lon = " + llp);
        final ProjectionPoint pp = proj.latLonToProj(llp);
        System.out.println("proj point = " + pp);
        llp = proj.projToLatLon(pp);
        System.out.println("reverse llp = " + llp);
        final double[][] latlons = { { 45.0 }, { -105.0 } };
        final double[][] linele = proj.latLonToProj(latlons);
        System.out.println("proj point = " + linele[0][0] + "," + linele[1][0]);
        final double[][] outll = proj.projToLatLon(linele);
        System.out.println("proj point = " + outll[0][0] + "," + outll[1][0]);
    }
    
    private double[] makeDoubleArray(final int[] ints) {
        final double[] newArray = new double[ints.length];
        for (int i = 0; i < ints.length; ++i) {
            newArray[i] = ints[i];
        }
        return newArray;
    }
    
    static {
        McIDASAreaProjection.ATTR_AREADIR = "AreaDirectory";
        McIDASAreaProjection.ATTR_NAVBLOCK = "NavBlock";
        McIDASAreaProjection.ATTR_AUXBLOCK = "AuxBlock";
        McIDASAreaProjection.GRID_MAPPING_NAME = "mcidas_area";
    }
}
