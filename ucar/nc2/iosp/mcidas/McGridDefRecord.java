// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.iosp.mcidas;

import edu.wisc.ssec.mcidas.McIDASException;
import edu.wisc.ssec.mcidas.GRIDnav;
import ucar.grid.GridDefRecord;

public class McGridDefRecord extends GridDefRecord
{
    int[] vals;
    private String proj;
    private static final int PSEUDO_MERCATOR = 1;
    private static final int PS_OR_LAMBERT_CONIC = 2;
    private static final int EQUIDISTANT = 3;
    private static final int PSEUDO_MERCATOR_GENERAL = 4;
    private static final int NO_NAV = 5;
    private static final int LAMBERT_CONFORMAL_TANGENT = 6;
    private static final double EARTH_RADIUS = 6371.23;
    private static int cnt;
    
    public McGridDefRecord() {
        this.vals = null;
    }
    
    public McGridDefRecord(final int[] words) {
        this.vals = null;
        this.setValues(words);
    }
    
    public void setValues(final int[] values) {
        this.vals = values;
        this.setParams();
        this.addParam("GDSkey", this.toString());
    }
    
    public String toString() {
        final StringBuffer buf = new StringBuffer(this.getParam("ProjFlag"));
        buf.append(" X:");
        buf.append(this.getParam("Nx"));
        buf.append(" ");
        buf.append("Y:");
        buf.append(this.getParam("Ny"));
        return buf.toString();
    }
    
    public boolean equals(final Object o) {
        return this == o || (o instanceof McGridDefRecord && this.toString().equals(o.toString()));
    }
    
    public int hashCode() {
        return this.toString().hashCode();
    }
    
    private void setParams() {
        try {
            final GRIDnav nav = new GRIDnav(this.vals);
            final int gridType = this.vals[33];
            final int navType = gridType % 10;
            this.addParam("Proj", String.valueOf(navType));
            final boolean wierd = gridType / 10 == 1;
            final int ny = this.vals[1];
            final int nx = this.vals[2];
            this.addParam("ProjFlag", this.getProjName(navType));
            this.addParam("Nx", String.valueOf(nx));
            this.addParam("Ny", String.valueOf(ny));
            double[][] input = new double[2][2];
            if (nav.isFlippedRowCoordinates()) {
                input = new double[][] { { 1.0, nx }, { 1.0, ny } };
            }
            else {
                input = new double[][] { { 1.0, nx }, { ny, 1.0 } };
            }
            final double[][] llur = nav.toLatLon(input);
            this.addParam("La1", String.valueOf(llur[0][0]));
            this.addParam("Lo1", String.valueOf(llur[1][0]));
            this.addParam("La2", String.valueOf(llur[0][1]));
            this.addParam("Lo2", String.valueOf(llur[1][1]));
            switch (navType) {
                case 1:
                case 4: {
                    final double glamx = this.vals[34] / 10000.0;
                    final double glomx = -this.vals[35] / 10000.0;
                    final double glamn = this.vals[34] / 10000.0;
                    final double glomn = -this.vals[35] / 10000.0;
                    final double ginct = this.vals[38] / 10000.0;
                    final double gincn = (navType == 4) ? (this.vals[39] / 10000.0) : ginct;
                    this.addParam("Latin", String.valueOf(20));
                    break;
                }
                case 2: {
                    final double xrowi = this.vals[34] / 10000.0;
                    final double xcoli = this.vals[35] / 10000.0;
                    final double xspace = this.vals[36];
                    final double xqlon = -this.vals[37] / 10000.0;
                    final double xt1 = this.vals[38] / 10000.0;
                    final double xt2 = this.vals[39] / 10000.0;
                    this.addParam("Latin1", String.valueOf(xt1));
                    this.addParam("LoV", String.valueOf(xqlon));
                    this.addParam("Latin2", String.valueOf(xt2));
                    this.addParam("Dx", String.valueOf(xspace));
                    this.addParam("Dy", String.valueOf(xspace));
                    break;
                }
                case 3: {
                    final double xrowi = 1.0;
                    final double xcoli = 1.0;
                    final double glamx = this.vals[34] / 10000.0;
                    final double glomx = -this.vals[35] / 10000.0;
                    final double xspace = this.vals[37] / 1000.0;
                    final double yspace = this.vals[38] / 1000.0;
                    this.addParam("La1", String.valueOf(glamx));
                    this.addParam("Lo1", String.valueOf(glomx));
                    this.addParam("Dx", String.valueOf(xspace));
                    this.addParam("Dy", String.valueOf(yspace));
                    break;
                }
                case 6: {
                    final double xrowi = this.vals[34] / 10000.0;
                    final double xcoli = this.vals[35] / 10000.0;
                    final double xspace = this.vals[36];
                    final double xqlon = -this.vals[37] / 10000.0;
                    final double xt1 = this.vals[38] / 10000.0;
                    double xt2 = this.vals[39] / 10000.0;
                    if (xt2 == -2.139062144E9 || xt2 == 0.0) {
                        xt2 = xt1;
                    }
                    this.addParam("Latin1", String.valueOf(xt1));
                    this.addParam("LoV", String.valueOf(xqlon));
                    this.addParam("Latin2", String.valueOf(xt2));
                    this.addParam("Dx", String.valueOf(xspace));
                    this.addParam("Dy", String.valueOf(xspace));
                    break;
                }
            }
        }
        catch (McIDASException me) {
            System.out.println("couldn't set nav");
        }
    }
    
    public String getGroupName() {
        final StringBuffer buf = new StringBuffer();
        buf.append(this.getParam("ProjFlag"));
        buf.append("_");
        buf.append(this.getParam("Nx"));
        buf.append("x");
        buf.append(this.getParam("Ny"));
        return buf.toString();
    }
    
    public String getProjName(final int type) {
        String projName = "UNKN";
        switch (type) {
            case 1:
            case 4: {
                projName = "MERC";
                break;
            }
            case 2: {
                projName = ((this.vals[38] == this.vals[39]) ? "PS" : "CONF");
                break;
            }
            case 3: {
                projName = "EQUI";
                break;
            }
            case 6: {
                projName = "CONF";
                break;
            }
            default: {
                projName = "NAV" + type;
                break;
            }
        }
        return projName;
    }
    
    static {
        McGridDefRecord.cnt = 0;
    }
}
