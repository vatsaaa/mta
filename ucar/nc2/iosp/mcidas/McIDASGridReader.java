// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.iosp.mcidas;

import java.util.List;
import edu.wisc.ssec.mcidas.McIDASException;
import ucar.grid.GridDefRecord;
import ucar.grid.GridRecord;
import edu.wisc.ssec.mcidas.McIDASUtil;
import java.io.IOException;
import java.util.HashMap;
import ucar.grid.GridIndex;
import ucar.unidata.io.RandomAccessFile;

public class McIDASGridReader
{
    protected RandomAccessFile rf;
    private String errorMessage;
    private GridIndex gridIndex;
    protected boolean needToSwap;
    private HashMap<String, McGridDefRecord> gdsMap;
    private static final int MAX_GRIDS = 999999;
    
    public McIDASGridReader() {
        this.needToSwap = false;
        this.gdsMap = new HashMap<String, McGridDefRecord>();
    }
    
    public McIDASGridReader(final String filename) throws IOException {
        this(new RandomAccessFile(filename, "r", 2048));
    }
    
    public McIDASGridReader(final RandomAccessFile raf) throws IOException {
        this.needToSwap = false;
        this.gdsMap = new HashMap<String, McGridDefRecord>();
        this.init(raf);
    }
    
    public final void init(final RandomAccessFile raf) throws IOException {
        this.init(raf, true);
    }
    
    public final void init(final RandomAccessFile raf, final boolean fullCheck) throws IOException {
        (this.rf = raf).order(0);
        final boolean ok = this.init(fullCheck);
        if (!ok) {
            throw new IOException("Unable to open McIDAS Grid file: " + this.errorMessage);
        }
    }
    
    protected boolean init() throws IOException {
        return this.init(true);
    }
    
    protected boolean init(final boolean fullCheck) throws IOException {
        if (this.rf == null) {
            this.logError("File is null");
            return false;
        }
        this.gridIndex = new GridIndex(this.rf.getLocation());
        this.rf.order(0);
        int numEntries = Math.abs(this.readInt(10));
        if (numEntries > 1000000) {
            this.needToSwap = true;
            numEntries = Math.abs(McIDASUtil.swbyt4(numEntries));
        }
        if (numEntries > 999999) {
            return false;
        }
        this.rf.seek(0L);
        final String label = this.rf.readString(32);
        if (label.indexOf("GEMPAK DATA MANAGEMENT FILE") >= 0) {
            this.logError("label indicates this is a GEMPAK grid");
            return false;
        }
        for (int i = 0; i < label.length(); ++i) {
            final String s0 = label.substring(i, i + 1);
            if (0 > s0.compareTo(" ") || s0.compareTo("~") > 0) {
                this.logError("bad label, not a McIDAS grid");
                return false;
            }
        }
        final int project = this.readInt(8);
        final int date = this.readInt(9);
        if (date < 10000 || date > 400000) {
            this.logError("date wrong, not a McIDAS grid");
            return false;
        }
        final int[] entries = new int[numEntries];
        for (int j = 0; j < numEntries; ++j) {
            entries[j] = this.readInt(j + 11);
            if (entries[j] < -1) {
                this.logError("bad grid offset " + j + ": " + entries[j]);
                return false;
            }
        }
        if (!fullCheck) {
            return true;
        }
        this.rf.order(0);
        for (int j = 0; j < numEntries; ++j) {
            if (entries[j] != -1) {
                final int[] header = new int[64];
                this.rf.seek(entries[j] * 4);
                this.rf.readInt(header, 0, 64);
                if (this.needToSwap) {
                    this.swapGridHeader(header);
                }
                try {
                    final McIDASGridRecord gr = new McIDASGridRecord(entries[j], header);
                    this.gridIndex.addGridRecord((GridRecord)gr);
                    if (this.gdsMap.get(gr.getGridDefRecordId()) == null) {
                        final McGridDefRecord mcdef = gr.getGridDefRecord();
                        this.gdsMap.put(mcdef.toString(), mcdef);
                        this.gridIndex.addHorizCoordSys((GridDefRecord)mcdef);
                    }
                }
                catch (McIDASException me) {
                    this.logError("problem creating grid dir");
                    return false;
                }
            }
        }
        if (this.gridIndex.getGridRecords().isEmpty()) {
            this.logError("no grids found");
            return false;
        }
        return true;
    }
    
    private void swapGridHeader(final int[] gh) {
        McIDASUtil.flip(gh, 0, 5);
        McIDASUtil.flip(gh, 7, 7);
        McIDASUtil.flip(gh, 9, 10);
        McIDASUtil.flip(gh, 12, 14);
        McIDASUtil.flip(gh, 32, 51);
    }
    
    public float[] readGrid(final McIDASGridRecord gr) {
        float[] data = null;
        try {
            final int te = (gr.getOffsetToHeader() + 64) * 4;
            final int rows = gr.getRows();
            final int cols = gr.getColumns();
            this.rf.seek(te);
            final float scale = (float)gr.getParamScale();
            data = new float[rows * cols];
            final RandomAccessFile rf = this.rf;
            int endian;
            if (this.needToSwap) {
                final RandomAccessFile rf2 = this.rf;
                endian = 1;
            }
            else {
                final RandomAccessFile rf3 = this.rf;
                endian = 0;
            }
            rf.order(endian);
            final int n = 0;
            for (int nc = 0; nc < cols; ++nc) {
                for (int nr = 0; nr < rows; ++nr) {
                    final int temp = this.rf.readInt();
                    data[(rows - nr - 1) * cols + nc] = ((temp == -2139062144) ? Float.NaN : (temp / scale));
                }
            }
            final RandomAccessFile rf4 = this.rf;
            final RandomAccessFile rf5 = this.rf;
            rf4.order(0);
        }
        catch (Exception esc) {
            System.out.println(esc);
        }
        return data;
    }
    
    public GridIndex getGridIndex() {
        return this.gridIndex;
    }
    
    public int readInt(final int word) throws IOException {
        if (this.rf == null) {
            throw new IOException("no file to read from");
        }
        this.rf.seek(word * 4);
        if (this.needToSwap) {
            this.rf.order(1);
        }
        else {
            this.rf.order(0);
        }
        final int idata = this.rf.readInt();
        this.rf.order(0);
        return idata;
    }
    
    private void logError(final String errMsg) {
        this.errorMessage = errMsg;
    }
    
    public static void main(final String[] args) throws IOException {
        String file = "GRID2001";
        if (args.length > 0) {
            file = args[0];
        }
        final McIDASGridReader mg = new McIDASGridReader(file);
        final GridIndex gridIndex = mg.getGridIndex();
        final List grids = gridIndex.getGridRecords();
        System.out.println("found " + grids.size() + " grids");
        for (int num = Math.min(grids.size(), 10), i = 0; i < num; ++i) {
            System.out.println(grids.get(i));
        }
    }
}
