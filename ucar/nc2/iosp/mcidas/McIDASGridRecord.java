// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.iosp.mcidas;

import java.util.Formatter;
import ucar.grid.GridTableLookup;
import edu.wisc.ssec.mcidas.McIDASException;
import ucar.grid.GridRecord;
import edu.wisc.ssec.mcidas.GridDirectory;

public class McIDASGridRecord extends GridDirectory implements GridRecord
{
    private int offsetToHeader;
    private McGridDefRecord gridDefRecord;
    private int decimalScale;
    
    public McIDASGridRecord(final int offset, final int[] header) throws McIDASException {
        super(header);
        this.decimalScale = 0;
        this.gridDefRecord = new McGridDefRecord(header);
        this.offsetToHeader = offset;
    }
    
    public double getLevel1() {
        return this.getLevelValue();
    }
    
    public double getLevel2() {
        return this.getSecondLevelValue();
    }
    
    public int getLevelType1() {
        final int gribLevel = this.getDirBlock()[51];
        int levelType = 0;
        if (gribLevel != -2139062144 && gribLevel != 0) {
            levelType = gribLevel;
        }
        else {
            levelType = 1;
        }
        return levelType;
    }
    
    public int getLevelType2() {
        return this.getLevelType1();
    }
    
    public int getValidTimeOffset() {
        return this.getForecastHour();
    }
    
    public String getParameterName() {
        return this.getParamName();
    }
    
    public String getParameterDescription() {
        return this.getParamName();
    }
    
    public int getDecimalScale() {
        return this.decimalScale;
    }
    
    public String getGridDefRecordId() {
        return this.gridDefRecord.toString();
    }
    
    public McGridDefRecord getGridDefRecord() {
        return this.gridDefRecord;
    }
    
    public int getOffsetToHeader() {
        return this.offsetToHeader;
    }
    
    public boolean hasGribInfo() {
        final int gribSection = this.getDirBlock()[48];
        return gribSection != -2139062144 && gribSection != 0;
    }
    
    public int getTimeUnit() {
        return 0;
    }
    
    public String getTimeUdunitName() {
        return "minutes";
    }
    
    public int cdmVariableHash() {
        return this.getParamName().hashCode() + 37 * this.getLevelType1();
    }
    
    public String cdmVariableName(final GridTableLookup lookup, final boolean useLevel, final boolean useStat) {
        final Formatter f = new Formatter();
        f.format("%s", this.getParameterName());
        if (useLevel) {
            final String levelName = lookup.getLevelName((GridRecord)this);
            if (levelName.length() != 0) {
                if (lookup.isLayer((GridRecord)this)) {
                    f.format("_%s_layer", lookup.getLevelName((GridRecord)this));
                }
                else {
                    f.format("_%s", lookup.getLevelName((GridRecord)this));
                }
            }
        }
        return f.toString();
    }
}
