// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.iosp.mcidas;

import ucar.grid.GridDefRecord;

public class Vis5DGridDefRecord extends GridDefRecord
{
    private static final int PROJ_GENERIC = 0;
    private static final int PROJ_LINEAR = 1;
    private static final int PROJ_CYLINDRICAL = 20;
    private static final int PROJ_SPHERICAL = 21;
    private static final int PROJ_LAMBERT = 2;
    private static final int PROJ_STEREO = 3;
    private static final int PROJ_ROTATED = 4;
    private int projection;
    
    public Vis5DGridDefRecord(final int Projection, final double[] projargs, final int nr, final int nc) {
        this.setParams(this.projection = Projection, projargs);
        this.addParam("GDSkey", this.toString());
    }
    
    public String getGroupName() {
        final StringBuffer buf = new StringBuffer();
        buf.append(this.getParam("ProjFlag"));
        buf.append("_");
        buf.append(this.getParam("Nx"));
        buf.append("x");
        buf.append(this.getParam("Ny"));
        return buf.toString();
    }
    
    public String toString() {
        final StringBuffer buf = new StringBuffer(this.getParam("ProjFlag"));
        buf.append(" X:");
        buf.append(this.getParam("Nx"));
        buf.append(" ");
        buf.append("Y:");
        buf.append(this.getParam("Ny"));
        return buf.toString();
    }
    
    public boolean equals(final Object o) {
        return this == o || (o instanceof McGridDefRecord && this.toString().equals(o.toString()));
    }
    
    public int hashCode() {
        return this.toString().hashCode();
    }
    
    private void setParams(final int Projection, final double[] projargs) {
        switch (Projection) {
            case 0: {
                this.addParam("ProjFlag", "GENERIC");
            }
            case 1: {
                this.addParam("ProjFlag", "LINEAR");
            }
            case 20: {
                this.addParam("ProjFlag", "CYLINDRICAL");
            }
            case 21: {
                this.addParam("ProjFlag", "SPHERICAL");
                final double NorthBound = projargs[0];
                final double WestBound = -projargs[1];
                final double RowInc = projargs[2];
                final double ColInc = projargs[3];
                this.addParam("La1", String.valueOf(NorthBound));
                this.addParam("Lo1", String.valueOf(WestBound));
                this.addParam("Dx", String.valueOf(ColInc));
                this.addParam("Dy", String.valueOf(RowInc));
                break;
            }
            case 4: {
                this.addParam("ProjFlag", "ROTATED");
                final double NorthBound = projargs[0];
                final double WestBound = projargs[1];
                final double RowInc = projargs[2];
                final double ColInc = projargs[3];
                final double CentralLat = projargs[4];
                final double CentralLon = projargs[5];
                final double Rotation = projargs[6];
                break;
            }
            case 2: {
                this.addParam("ProjFlag", "LAMBERT");
                final double Lat1 = projargs[0];
                final double Lat2 = projargs[1];
                final double PoleRow = projargs[2];
                final double PoleCol = projargs[3];
                final double CentralLon = projargs[4];
                final double ColInc = projargs[5];
                break;
            }
            case 3: {
                this.addParam("ProjFlag", "STEREO");
                final double CentralLat = projargs[0];
                final double CentralLon = projargs[1];
                final double CentralRow = projargs[2];
                final double CentralCol = projargs[3];
                final double ColInc = projargs[4];
                break;
            }
        }
    }
    
    public static void printProjArgs(final int Projection, final double[] projargs) {
        switch (Projection) {
            case 0:
            case 1:
            case 20:
            case 21: {
                final double NorthBound = projargs[0];
                final double WestBound = projargs[1];
                final double RowInc = projargs[2];
                final double ColInc = projargs[3];
                System.out.println("Generic, Linear, Cylindrical, Spherical:");
                System.out.println("NB: " + NorthBound + ", WB: " + WestBound + ", rowInc: " + RowInc + ", colInc: " + ColInc);
                break;
            }
            case 4: {
                final double NorthBound = projargs[0];
                final double WestBound = projargs[1];
                final double RowInc = projargs[2];
                final double ColInc = projargs[3];
                final double CentralLat = projargs[4];
                final double CentralLon = projargs[5];
                final double Rotation = projargs[6];
                System.out.println("Rotated:");
                System.out.println("NB: " + NorthBound + ", WB: " + WestBound + ", rowInc: " + RowInc + ", colInc: " + ColInc + ", clat: " + CentralLat + ", clon: " + CentralLon + ", rotation: " + Rotation);
                break;
            }
            case 2: {
                final double Lat1 = projargs[0];
                final double Lat2 = projargs[1];
                final double PoleRow = projargs[2];
                final double PoleCol = projargs[3];
                final double CentralLon = projargs[4];
                final double ColInc = projargs[5];
                System.out.println("Lambert: ");
                System.out.println("lat1: " + Lat1 + ", lat2: " + Lat2 + ", poleRow: " + PoleRow + ", PoleCol: " + PoleCol + ", clon: " + CentralLon + ", colInc: " + ColInc);
                break;
            }
            case 3: {
                final double CentralLat = projargs[0];
                final double CentralLon = projargs[1];
                final double CentralRow = projargs[2];
                final double CentralCol = projargs[3];
                final double ColInc = projargs[4];
                System.out.println("Stereo: ");
                System.out.println("clat: " + CentralLat + ", clon: " + CentralLon + ", cRow: " + CentralRow + ", cCol: " + CentralCol + ", colInc: " + ColInc);
                break;
            }
            default: {
                System.out.println("Projection unknown");
                break;
            }
        }
    }
}
