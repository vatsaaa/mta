// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.iosp.mcidas;

import ucar.ma2.Array;
import ucar.nc2.Attribute;
import ucar.unidata.geoloc.ProjectionImpl;
import ucar.nc2.dataset.ProjectionCT;
import ucar.nc2.dataset.CoordinateTransform;
import ucar.nc2.Variable;
import ucar.nc2.dataset.NetcdfDataset;
import ucar.nc2.dataset.TransformType;
import ucar.nc2.dataset.transform.AbstractCoordTransBuilder;

public class McIDASAreaTransformBuilder extends AbstractCoordTransBuilder
{
    public String getTransformName() {
        return McIDASAreaProjection.GRID_MAPPING_NAME;
    }
    
    public TransformType getTransformType() {
        return TransformType.Projection;
    }
    
    public CoordinateTransform makeCoordinateTransform(final NetcdfDataset ds, final Variable ctv) {
        final int[] area = this.getIntArray(ctv, McIDASAreaProjection.ATTR_AREADIR);
        final int[] nav = this.getIntArray(ctv, McIDASAreaProjection.ATTR_NAVBLOCK);
        int[] aux = null;
        if (ctv.findAttributeIgnoreCase(McIDASAreaProjection.ATTR_AUXBLOCK) != null) {
            aux = this.getIntArray(ctv, McIDASAreaProjection.ATTR_AUXBLOCK);
        }
        final McIDASAreaProjection proj = new McIDASAreaProjection(area, nav, aux);
        return new ProjectionCT(ctv.getShortName(), "FGDC", proj);
    }
    
    private int[] getIntArray(final Variable ctv, final String attName) {
        final Attribute att = ctv.findAttribute(attName);
        if (att == null) {
            throw new IllegalArgumentException("McIDASArea coordTransformVariable " + ctv.getName() + " must have " + attName + " attribute");
        }
        final Array arr = att.getValues();
        return (int[])arr.get1DJavaArray(Integer.TYPE);
    }
}
