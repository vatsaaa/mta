// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.iosp;

import java.io.IOException;

public interface Layout
{
    long getTotalNelems();
    
    int getElemSize();
    
    boolean hasNext();
    
    Chunk next() throws IOException;
    
    public interface Chunk
    {
        long getSrcPos();
        
        int getNelems();
        
        long getDestElem();
    }
}
