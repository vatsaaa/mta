// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.iosp.dmsp;

import java.util.TimeZone;
import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.Iterator;
import java.text.ParseException;
import ucar.nc2.Group;
import java.io.IOException;
import java.util.Date;
import ucar.nc2.Dimension;
import ucar.nc2.Attribute;
import ucar.nc2.NetcdfFile;
import ucar.unidata.io.RandomAccessFile;
import java.util.HashMap;

public class DMSPHeader
{
    private String[] header;
    private HashMap headerInfo;
    private int headerSizeInBytes;
    private int headerSizeInBytesGuess;
    private RandomAccessFile raFile;
    private NetcdfFile ncFile;
    private long actualSize;
    private String fileIdAttName;
    private Attribute fileIdAtt;
    private String datasetIdAttName;
    private Attribute datasetIdAtt;
    private int recordSizeInBytes;
    private int numHeaderRecords;
    private int numDataRecords;
    private String numDataRecordsDimName;
    private Dimension numDataRecordsDim;
    private int numArtificialDataRecords;
    private int numRecords;
    private String suborbitHistoryAttName;
    private Attribute suborbitHistoryAtt;
    private String processingSystemAttName;
    private Attribute processingSystemAtt;
    private Date processingDate;
    private String processingDateAttName;
    private Attribute processingDateAtt;
    private String spacecraftIdAttName;
    private Attribute spacecraftIdAtt;
    private String noradIdAttName;
    private Attribute noradIdAtt;
    private String startDateAttName;
    private Attribute startDateAtt;
    private Date startDate;
    private String endDateAttName;
    private Attribute endDateAtt;
    private Date endDate;
    private String startDateLocalAttName;
    private Attribute startDateLocalAtt;
    private String startTimeLocalAttName;
    private Attribute startTimeLocalAtt;
    private String startLatitudeAttName;
    private Attribute startLatitudeAtt;
    private String startLongitudeAttName;
    private Attribute startLongitudeAtt;
    private String endLatitudeAttName;
    private Attribute endLatitudeAtt;
    private String endLongitudeAttName;
    private Attribute endLongitudeAtt;
    private String startSubsolarCoordsAttName;
    private Attribute startSubsolarCoordsAtt;
    private String endSubsolarCoordsAttName;
    private Attribute endSubsolarCoordsAtt;
    private String startLunarCoordsAttName;
    private Attribute startLunarCoordsAtt;
    private String endLunarCoordsAttName;
    private Attribute endLunarCoordsAtt;
    private String ascendingNodeAttName;
    private Attribute ascendingNodeAtt;
    private String nodeHeadingAttName;
    private Attribute nodeHeadingAtt;
    private int numSamplesPerBand;
    private String numSamplesPerBandDimName;
    private Dimension numSamplesPerBandDim;
    private String nominalResolutionAttName;
    private Attribute nominalResolutionAtt;
    private String bandsPerScanlineAttName;
    private Attribute bandsPerScanlineAtt;
    private String bytesPerSampleAttName;
    private Attribute bytesPerSampleAtt;
    private String byteOffsetBand1AttName;
    private Attribute byteOffsetBand1Att;
    private String byteOffsetBand2AttName;
    private Attribute byteOffsetBand2Att;
    private String band1AttName;
    private Attribute band1Att;
    private String band2AttName;
    private Attribute band2Att;
    private String bandOrganizationAttName;
    private Attribute bandOrganizationAtt;
    private String thermalOffsetAttName;
    private Attribute thermalOffsetAtt;
    private String thermalScaleAttName;
    private Attribute thermalScaleAtt;
    private String percentDaylightAttName;
    private Attribute percentDaylightAtt;
    private String percentFullMoonAttName;
    private Attribute percentFullMoonAtt;
    private String percentTerminatorEvidentAttName;
    private Attribute percentTerminatorEvidentAtt;
    private String qcFlagsAttName;
    private Attribute qcFlagsAtt;
    
    public DMSPHeader() {
        this.header = null;
        this.headerInfo = new HashMap();
        this.headerSizeInBytes = 0;
        this.headerSizeInBytesGuess = 5000;
        this.fileIdAttName = "fileId";
        this.fileIdAtt = null;
        this.datasetIdAttName = "datasetId";
        this.datasetIdAtt = null;
        this.recordSizeInBytes = 0;
        this.numHeaderRecords = 0;
        this.numDataRecords = 0;
        this.numDataRecordsDimName = "numScans";
        this.numDataRecordsDim = null;
        this.numArtificialDataRecords = 0;
        this.numRecords = 0;
        this.suborbitHistoryAttName = "suborbitHistory";
        this.suborbitHistoryAtt = null;
        this.processingSystemAttName = "processingSystem";
        this.processingSystemAtt = null;
        this.processingDate = null;
        this.processingDateAttName = "processingDate";
        this.processingDateAtt = null;
        this.spacecraftIdAttName = "spacecraftId";
        this.spacecraftIdAtt = null;
        this.noradIdAttName = "noradId";
        this.noradIdAtt = null;
        this.startDateAttName = "startDate";
        this.startDateAtt = null;
        this.startDate = null;
        this.endDateAttName = "endDate";
        this.endDateAtt = null;
        this.endDate = null;
        this.startDateLocalAttName = "startDateLocal";
        this.startDateLocalAtt = null;
        this.startTimeLocalAttName = "startTimeLocal";
        this.startTimeLocalAtt = null;
        this.startLatitudeAttName = "startLatitude";
        this.startLatitudeAtt = null;
        this.startLongitudeAttName = "startLongitude";
        this.startLongitudeAtt = null;
        this.endLatitudeAttName = "endLatitude";
        this.endLatitudeAtt = null;
        this.endLongitudeAttName = "endLongitude";
        this.endLongitudeAtt = null;
        this.startSubsolarCoordsAttName = "startSubsolarCoords";
        this.startSubsolarCoordsAtt = null;
        this.endSubsolarCoordsAttName = "endSubsolarCoords";
        this.endSubsolarCoordsAtt = null;
        this.startLunarCoordsAttName = "startLunarCoords";
        this.startLunarCoordsAtt = null;
        this.endLunarCoordsAttName = "endLunarCoords";
        this.endLunarCoordsAtt = null;
        this.ascendingNodeAttName = "ascendingNode";
        this.ascendingNodeAtt = null;
        this.nodeHeadingAttName = "nodeHeading";
        this.nodeHeadingAtt = null;
        this.numSamplesPerBand = 0;
        this.numSamplesPerBandDimName = "numSamplesPerScan";
        this.numSamplesPerBandDim = null;
        this.nominalResolutionAttName = "nominalResolution";
        this.nominalResolutionAtt = null;
        this.bandsPerScanlineAttName = "bandsPerScanline";
        this.bandsPerScanlineAtt = null;
        this.bytesPerSampleAttName = "bytesPerSample";
        this.bytesPerSampleAtt = null;
        this.byteOffsetBand1AttName = "byteOffsetBand1";
        this.byteOffsetBand1Att = null;
        this.byteOffsetBand2AttName = "byteOffsetBand2";
        this.byteOffsetBand2Att = null;
        this.band1AttName = "band1";
        this.band1Att = null;
        this.band2AttName = "band2";
        this.band2Att = null;
        this.bandOrganizationAttName = "bandOrganization";
        this.bandOrganizationAtt = null;
        this.thermalOffsetAttName = "thermalOffset";
        this.thermalOffsetAtt = null;
        this.thermalScaleAttName = "thermalScale";
        this.thermalScaleAtt = null;
        this.percentDaylightAttName = "percentDaylight";
        this.percentDaylightAtt = null;
        this.percentFullMoonAttName = "percentFullMoon";
        this.percentFullMoonAtt = null;
        this.percentTerminatorEvidentAttName = "percentTerminatorEvident";
        this.percentTerminatorEvidentAtt = null;
        this.qcFlagsAttName = "qcFlags";
        this.qcFlagsAtt = null;
    }
    
    public Attribute getFileIdAtt() {
        return this.fileIdAtt;
    }
    
    public Attribute getDatasetIdAtt() {
        return this.datasetIdAtt;
    }
    
    public int getNumHeaderRecords() {
        return this.numHeaderRecords;
    }
    
    public int getNumDataRecords() {
        return this.numDataRecords;
    }
    
    public Dimension getNumDataRecordsDim() {
        return this.numDataRecordsDim;
    }
    
    public Dimension getNumSamplesPerBandDim() {
        return this.numSamplesPerBandDim;
    }
    
    public int getRecordSizeInBytes() {
        return this.recordSizeInBytes;
    }
    
    public Attribute getSuborbitHistoryAtt() {
        return this.suborbitHistoryAtt;
    }
    
    public Attribute getProcessingSystemAtt() {
        return this.processingSystemAtt;
    }
    
    public Attribute getProcessingDateAtt() {
        return this.processingDateAtt;
    }
    
    public Attribute getStartDateAtt() {
        return this.startDateAtt;
    }
    
    boolean isValidFile(final RandomAccessFile raFile) {
        this.raFile = raFile;
        try {
            this.actualSize = raFile.length();
        }
        catch (IOException e) {
            return false;
        }
        try {
            this.readHeaderFromFile(raFile);
            this.handleFileInformation();
            this.handleProcessingInformation();
            this.handleSatelliteInformation();
            this.handleSensorInformation();
        }
        catch (IOException e) {
            return false;
        }
        return true;
    }
    
    void read(final RandomAccessFile raFile, final NetcdfFile ncFile) throws IOException {
        this.raFile = raFile;
        this.ncFile = ncFile;
        this.actualSize = this.raFile.length();
        this.readHeaderFromFile(raFile);
        this.handleFileInformation();
        this.ncFile.addAttribute(null, this.fileIdAtt);
        this.ncFile.addAttribute(null, this.datasetIdAtt);
        this.ncFile.addDimension(null, this.numDataRecordsDim);
        this.handleProcessingInformation();
        this.ncFile.addAttribute(null, this.suborbitHistoryAtt);
        this.ncFile.addAttribute(null, this.processingSystemAtt);
        this.ncFile.addAttribute(null, this.processingDateAtt);
        this.handleSatelliteInformation();
        this.ncFile.addAttribute(null, this.spacecraftIdAtt);
        this.ncFile.addAttribute(null, this.noradIdAtt);
        this.handleOrbitInformation();
        this.ncFile.addAttribute(null, this.startDateAtt);
        this.ncFile.addAttribute(null, this.endDateAtt);
        this.ncFile.addAttribute(null, this.startDateLocalAtt);
        this.ncFile.addAttribute(null, this.startTimeLocalAtt);
        this.ncFile.addAttribute(null, this.startLatitudeAtt);
        this.ncFile.addAttribute(null, this.startLongitudeAtt);
        this.ncFile.addAttribute(null, this.endLatitudeAtt);
        this.ncFile.addAttribute(null, this.endLongitudeAtt);
        this.ncFile.addAttribute(null, this.startSubsolarCoordsAtt);
        this.ncFile.addAttribute(null, this.endSubsolarCoordsAtt);
        this.ncFile.addAttribute(null, this.startLunarCoordsAtt);
        this.ncFile.addAttribute(null, this.endLunarCoordsAtt);
        this.ncFile.addAttribute(null, this.ascendingNodeAtt);
        this.ncFile.addAttribute(null, this.nodeHeadingAtt);
        this.handleSensorInformation();
        this.ncFile.addDimension(null, this.numSamplesPerBandDim);
        this.ncFile.addAttribute(null, this.nominalResolutionAtt);
        this.ncFile.addAttribute(null, this.bandsPerScanlineAtt);
        this.ncFile.addAttribute(null, this.bytesPerSampleAtt);
        this.ncFile.addAttribute(null, this.byteOffsetBand1Att);
        this.ncFile.addAttribute(null, this.byteOffsetBand2Att);
        this.ncFile.addAttribute(null, this.band1Att);
        this.ncFile.addAttribute(null, this.band2Att);
        this.ncFile.addAttribute(null, this.bandOrganizationAtt);
        this.ncFile.addAttribute(null, this.thermalOffsetAtt);
        this.ncFile.addAttribute(null, this.thermalScaleAtt);
        this.ncFile.addAttribute(null, this.percentDaylightAtt);
        this.ncFile.addAttribute(null, this.percentFullMoonAtt);
        this.ncFile.addAttribute(null, this.percentTerminatorEvidentAtt);
        this.handleQCInformation();
        this.ncFile.addAttribute(null, this.qcFlagsAtt);
        this.ncFile.addAttribute(null, new Attribute("title", new StringBuffer("NGDC archived ").append(this.datasetIdAtt.getStringValue()).append(" data with start time ").append(this.startDateAtt.getStringValue()).toString()));
        this.ncFile.addAttribute(null, new Attribute("Convention", "_Coordinates"));
        this.ncFile.addAttribute(null, new Attribute("thredds_creator", "DOD/USAF/SMC > Space and Missile Systems Center (SMC), U.S. Air Force, U.S. Department of Defense"));
        this.ncFile.addAttribute(null, new Attribute("thredds_contributor", "DOC/NOAA/NESDIS/NGDC > National Geophysical Data Center, NESDIS, NOAA, U.S. Department of Commerce"));
        this.ncFile.addAttribute(null, new Attribute("thredds_contributor_role", "archive"));
        this.ncFile.addAttribute(null, new Attribute("thredds_publisher", "DOC/NOAA/NESDIS/NGDC > National Geophysical Data Center, NESDIS, NOAA, U.S. Department of Commerce"));
        this.ncFile.addAttribute(null, new Attribute("thredds_publisher_url", "http://dmsp.ngdc.noaa.gov/"));
        this.ncFile.addAttribute(null, new Attribute("thredds_publisher_email", "ngdc.dmsp@noaa.gov"));
        this.ncFile.addAttribute(null, new Attribute("thredds_summary", new StringBuffer("This dataset contains data from the DMSP ").append(this.spacecraftIdAtt.getStringValue()).append(" satellite OLS instrument and includes both visible smooth and thermal smooth imagery with 2.7km resolution.").append(" The start time for this data is ").append(this.startDateAtt.getStringValue()).append(" and the northerly equatorial crossing longitude is ").append(this.startLongitudeAtt.getNumericValue()).append(".  The DMSP satellite is a polar-orbiting satellite crossing the equator, depending on the satellite, at either dawn/dusk or noon/midnight.").append(" This data is in the NOAA/NGDC DMSP archive format.").toString()));
        this.ncFile.addAttribute(null, new Attribute("thredds_history", ""));
        this.ncFile.addAttribute(null, new Attribute("thredds_timeCoverage_start", this.startDateAtt.getStringValue()));
        this.ncFile.addAttribute(null, new Attribute("thredds_timeCoverage_end", this.endDateAtt.getStringValue()));
        this.ncFile.addAttribute(null, new Attribute("thredds_geospatialCoverage", new StringBuffer("Polar orbit with northerly equatorial crossing at longitude ").append(this.ascendingNodeAtt.getNumericValue()).append(".").toString()));
        this.raFile.seek(this.headerSizeInBytes);
    }
    
    private void readHeaderFromFile(final RandomAccessFile raFile) throws IOException {
        final long pos = 0L;
        raFile.seek(pos);
        this.headerSizeInBytes = ((raFile.length() > this.headerSizeInBytesGuess) ? this.headerSizeInBytesGuess : ((int)raFile.length()));
        final byte[] b = new byte[this.headerSizeInBytes];
        if (raFile.read(b) != this.headerSizeInBytes) {
            throw new IOException("Invalid DMSP file: could not read first " + this.headerSizeInBytes + " bytes.");
        }
        final String fullHeader = new String(b);
        if (!fullHeader.startsWith(HeaderInfoTitle.FILE_ID.toString())) {
            throw new IOException("Invalid DMSP file: header does not start with \"" + HeaderInfoTitle.FILE_ID.toString() + "\".");
        }
        final int endOfHeaderIndex = fullHeader.indexOf(HeaderInfoTitle.END_HEADER.toString());
        if (endOfHeaderIndex == -1) {
            throw new IOException("Invalid DMSP file: header does not end with \"" + HeaderInfoTitle.END_HEADER.toString() + "\".");
        }
        this.header = fullHeader.substring(0, endOfHeaderIndex - 1).split("\n");
        int lineSeperatorIndex = 0;
        String curHeaderLine = null;
        String curHeaderTitle = null;
        String curHeaderValue = null;
        for (int i = 0; i < this.header.length; ++i) {
            curHeaderLine = this.header[i].trim();
            lineSeperatorIndex = curHeaderLine.indexOf(58);
            if (lineSeperatorIndex == -1) {
                throw new IOException("Invalid DMSP file: header line <" + curHeaderLine + "> contains no seperator <:>.");
            }
            if (lineSeperatorIndex == 0) {
                throw new IOException("Invalid DMSP file: header line <" + curHeaderLine + "> contains no title.");
            }
            if (lineSeperatorIndex == curHeaderLine.length() - 1) {
                throw new IOException("Invalid DMSP file: header line <" + curHeaderLine + "> contains no value.");
            }
            curHeaderTitle = curHeaderLine.substring(0, lineSeperatorIndex).trim();
            if (HeaderInfoTitle.getTitle(curHeaderTitle) == null) {
                throw new IOException("Invalid DMSP file: header line <" + curHeaderLine + "> contains invalid title.");
            }
            curHeaderValue = curHeaderLine.substring(lineSeperatorIndex + 1).trim();
            if (curHeaderValue.equals("")) {
                throw new IOException("Invalid DMSP file: header line <" + curHeaderLine + "> contains no value.");
            }
            this.headerInfo.put(curHeaderTitle, curHeaderValue);
        }
    }
    
    private void handleFileInformation() throws IOException {
        this.fileIdAtt = new Attribute(this.fileIdAttName, this.headerInfo.get(HeaderInfoTitle.FILE_ID.toString()));
        this.datasetIdAtt = new Attribute(this.datasetIdAttName, this.headerInfo.get(HeaderInfoTitle.DATA_SET_ID.toString()));
        this.recordSizeInBytes = Integer.parseInt(this.headerInfo.get(HeaderInfoTitle.RECORD_BYTES.toString()));
        this.numRecords = Integer.parseInt(this.headerInfo.get(HeaderInfoTitle.NUM_RECORDS.toString()));
        this.numHeaderRecords = Integer.parseInt(this.headerInfo.get(HeaderInfoTitle.NUM_HEADER_RECORDS.toString()));
        this.numDataRecords = Integer.parseInt(this.headerInfo.get(HeaderInfoTitle.NUM_DATA_RECORDS.toString()));
        this.numDataRecordsDim = new Dimension(this.numDataRecordsDimName, this.numDataRecords, true, true, false);
        this.numArtificialDataRecords = Integer.parseInt(this.headerInfo.get(HeaderInfoTitle.NUM_ARTIFICIAL_DATA_RECORDS.toString()));
        if (this.numHeaderRecords + this.numDataRecordsDim.getLength() + this.numArtificialDataRecords != this.numRecords) {
            throw new IOException("Invalid DMSP file: the number of header records <" + this.numHeaderRecords + ">, data records <" + this.numDataRecordsDim.getLength() + ">, and artificial data records <" + this.numArtificialDataRecords + "> is not equal to total records <" + this.numRecords + ">.");
        }
        this.headerSizeInBytes = this.numHeaderRecords * this.recordSizeInBytes;
        if (this.numRecords * this.recordSizeInBytes != this.actualSize) {
            throw new IOException("Invalid DMSP file: the number of records <" + this.numRecords + "> times the record size <" + this.recordSizeInBytes + "> does not equal the size of the file <" + this.actualSize + ">.");
        }
    }
    
    private void handleProcessingInformation() throws IOException {
        this.suborbitHistoryAtt = new Attribute(this.suborbitHistoryAttName, this.headerInfo.get(HeaderInfoTitle.SUBORBIT_HISTORY.toString()));
        this.processingSystemAtt = new Attribute(this.processingSystemAttName, this.headerInfo.get(HeaderInfoTitle.PROCESSING_SYSTEM.toString()));
        final String processingDateString = this.headerInfo.get(HeaderInfoTitle.PROCESSING_DATE.toString());
        try {
            this.processingDate = DateFormatHandler.ALT_DATE_TIME.getDateFromDateTimeString(processingDateString);
        }
        catch (ParseException e) {
            throw new IOException("Invalid DMSP file: processing date string <" + processingDateString + "> not parseable: " + e.getMessage());
        }
        this.processingDateAtt = new Attribute(this.processingDateAttName, DateFormatHandler.ISO_DATE_TIME.getDateTimeStringFromDate(this.processingDate));
    }
    
    private void handleSatelliteInformation() {
        this.spacecraftIdAtt = new Attribute(this.spacecraftIdAttName, this.headerInfo.get(HeaderInfoTitle.SPACECRAFT_ID.toString()));
        this.noradIdAtt = new Attribute(this.noradIdAttName, this.headerInfo.get(HeaderInfoTitle.NORAD_ID.toString()));
    }
    
    private void handleOrbitInformation() throws IOException {
        String time = this.headerInfo.get(HeaderInfoTitle.START_TIME_UTC.toString());
        final String startDateTimeUTC = this.headerInfo.get(HeaderInfoTitle.START_DATE_UTC.toString()) + "T" + time.substring(0, time.indexOf(46) + 4) + "GMT";
        try {
            this.startDate = DateFormatHandler.ISO_DATE_TIME.getDateFromDateTimeString(startDateTimeUTC);
        }
        catch (ParseException e) {
            throw new IOException("Invalid DMSP file: start date/time string <" + startDateTimeUTC + "> not parseable: " + e.getMessage());
        }
        this.startDateAtt = new Attribute(this.startDateAttName, DateFormatHandler.ISO_DATE_TIME.getDateTimeStringFromDate(this.startDate));
        time = this.headerInfo.get(HeaderInfoTitle.END_TIME_UTC.toString());
        final String endDateTimeUTC = this.headerInfo.get(HeaderInfoTitle.END_DATE_UTC.toString()) + "T" + time.substring(0, time.indexOf(46) + 4) + "GMT";
        try {
            this.endDate = DateFormatHandler.ISO_DATE_TIME.getDateFromDateTimeString(endDateTimeUTC);
        }
        catch (ParseException e2) {
            throw new IOException("Invalid DMSP file: end date/time string <" + endDateTimeUTC + "> not parseable: " + e2.getMessage());
        }
        this.endDateAtt = new Attribute(this.endDateAttName, DateFormatHandler.ISO_DATE_TIME.getDateTimeStringFromDate(this.endDate));
        this.startDateLocalAtt = new Attribute(this.startDateLocalAttName, HeaderInfoTitle.START_DATE_LOCAL.toString());
        this.startTimeLocalAtt = new Attribute(this.startTimeLocalAttName, HeaderInfoTitle.START_TIME_LOCAL.toString());
        final String startLatLon = this.headerInfo.get(HeaderInfoTitle.START_LAT_LON.toString());
        String[] latLon = startLatLon.split(" ");
        if (latLon.length != 2) {
            throw new IOException("Invalid DMSP file: start lat/lon <" + startLatLon + "> invalid.");
        }
        Double lat;
        Double lon;
        try {
            lat = Double.valueOf(latLon[0]);
            lon = Double.valueOf(latLon[1]);
        }
        catch (NumberFormatException e3) {
            throw new IOException("Invalid DMSP file: start lat/lon string <" + startLatLon + "> not parseable: " + e3.getMessage());
        }
        this.startLatitudeAtt = new Attribute(this.startLatitudeAttName, lat);
        this.startLongitudeAtt = new Attribute(this.startLongitudeAttName, lon);
        final String endLatLon = this.headerInfo.get(HeaderInfoTitle.END_LAT_LON.toString());
        latLon = endLatLon.split(" ");
        if (latLon.length != 2) {
            throw new IOException("Invalid DMSP file: end lat/lon <" + endLatLon + "> invalid.");
        }
        try {
            lat = Double.valueOf(latLon[0]);
            lon = Double.valueOf(latLon[1]);
        }
        catch (NumberFormatException e4) {
            throw new IOException("Invalid DMSP file: end lat/lon string <" + endLatLon + "> not parseable: " + e4.getMessage());
        }
        this.endLatitudeAtt = new Attribute(this.endLatitudeAttName, lat);
        this.endLongitudeAtt = new Attribute(this.endLongitudeAttName, lon);
        this.startSubsolarCoordsAtt = new Attribute(this.startSubsolarCoordsAttName, this.headerInfo.get(HeaderInfoTitle.START_SUBSOLAR_COORD.toString()));
        this.endSubsolarCoordsAtt = new Attribute(this.endSubsolarCoordsAttName, this.headerInfo.get(HeaderInfoTitle.END_SUBSOLAR_COORD.toString()));
        this.startLunarCoordsAtt = new Attribute(this.startLunarCoordsAttName, this.headerInfo.get(HeaderInfoTitle.START_LUNAR_COORD.toString()));
        this.endLunarCoordsAtt = new Attribute(this.endLunarCoordsAttName, this.headerInfo.get(HeaderInfoTitle.END_LUNAR_COORD.toString()));
        final Double ascendingNode = Double.valueOf(this.headerInfo.get(HeaderInfoTitle.ASCENDING_NODE.toString()));
        this.ascendingNodeAtt = new Attribute(this.ascendingNodeAttName, ascendingNode);
        final Double nodeHeading = Double.valueOf(this.headerInfo.get(HeaderInfoTitle.NODE_HEADING.toString()));
        this.nodeHeadingAtt = new Attribute(this.nodeHeadingAttName, nodeHeading);
    }
    
    private void handleSensorInformation() {
        this.numSamplesPerBand = Integer.parseInt(this.headerInfo.get(HeaderInfoTitle.SAMPLES_PER_BAND.toString()));
        this.numSamplesPerBandDim = new Dimension(this.numSamplesPerBandDimName, this.numSamplesPerBand);
        this.nominalResolutionAtt = new Attribute(this.nominalResolutionAttName, this.headerInfo.get(HeaderInfoTitle.NOMINAL_RESOLUTION.toString()));
        this.bandsPerScanlineAtt = new Attribute(this.bandsPerScanlineAttName, Integer.valueOf(this.headerInfo.get(HeaderInfoTitle.BANDS_PER_SCANLINE.toString())));
        this.bytesPerSampleAtt = new Attribute(this.bytesPerSampleAttName, Integer.valueOf(this.headerInfo.get(HeaderInfoTitle.BYTES_PER_SAMPLE.toString())));
        this.byteOffsetBand1Att = new Attribute(this.byteOffsetBand1AttName, Integer.valueOf(this.headerInfo.get(HeaderInfoTitle.BYTE_OFFSET_BAND_1.toString())));
        this.byteOffsetBand2Att = new Attribute(this.byteOffsetBand2AttName, Integer.valueOf(this.headerInfo.get(HeaderInfoTitle.BYTE_OFFSET_BAND_2.toString())));
        this.band1Att = new Attribute(this.band1AttName, this.headerInfo.get(HeaderInfoTitle.BAND_1.toString()));
        this.band2Att = new Attribute(this.band2AttName, this.headerInfo.get(HeaderInfoTitle.BAND_2.toString()));
        this.bandOrganizationAtt = new Attribute(this.bandOrganizationAttName, this.headerInfo.get(HeaderInfoTitle.ORGANIZATION.toString()));
        this.thermalOffsetAtt = new Attribute(this.thermalOffsetAttName, this.headerInfo.get(HeaderInfoTitle.THERMAL_OFFSET.toString()));
        this.thermalScaleAtt = new Attribute(this.thermalScaleAttName, this.headerInfo.get(HeaderInfoTitle.THERMAL_SCALE.toString()));
        this.percentDaylightAtt = new Attribute(this.percentDaylightAttName, Double.valueOf(this.headerInfo.get(HeaderInfoTitle.PERCENT_DAYLIGHT.toString())));
        this.percentFullMoonAtt = new Attribute(this.percentFullMoonAttName, Double.valueOf(this.headerInfo.get(HeaderInfoTitle.PERCENT_FULL_MOON.toString())));
        this.percentTerminatorEvidentAtt = new Attribute(this.percentTerminatorEvidentAttName, Double.valueOf(this.headerInfo.get(HeaderInfoTitle.PERCENT_TERMINATOR_EVIDENT.toString())));
    }
    
    private void handleQCInformation() {
        this.qcFlagsAtt = new Attribute(this.qcFlagsAttName, this.headerInfo.get(HeaderInfoTitle.QC_FLAGS.toString()));
    }
    
    protected String headerInfoDump() {
        final StringBuffer retVal = new StringBuffer();
        final Iterator it = this.headerInfo.keySet().iterator();
        String curHeaderTitle = null;
        String curHeaderValue = null;
        while (it.hasNext()) {
            curHeaderTitle = it.next();
            curHeaderValue = this.headerInfo.get(curHeaderTitle);
            retVal.append(curHeaderTitle);
            retVal.append(":::::");
            retVal.append(curHeaderValue);
            retVal.append(":::::\n");
        }
        return retVal.toString();
    }
    
    @Override
    public String toString() {
        final StringBuffer retVal = new StringBuffer();
        retVal.append(HeaderInfoTitle.FILE_ID.toString());
        retVal.append(": ");
        retVal.append(this.fileIdAtt.getStringValue());
        retVal.append("\n");
        retVal.append(HeaderInfoTitle.DATA_SET_ID.toString());
        retVal.append(": ");
        retVal.append(this.datasetIdAtt.getStringValue());
        retVal.append("\n");
        retVal.append(HeaderInfoTitle.RECORD_BYTES.toString());
        retVal.append(": ");
        retVal.append(this.recordSizeInBytes);
        retVal.append("\n");
        retVal.append(HeaderInfoTitle.NUM_HEADER_RECORDS.toString());
        retVal.append(": ");
        retVal.append(this.numHeaderRecords);
        retVal.append("\n");
        retVal.append(HeaderInfoTitle.NUM_RECORDS.toString());
        retVal.append(": ");
        retVal.append(this.numRecords);
        retVal.append("\n");
        retVal.append(HeaderInfoTitle.SUBORBIT_HISTORY.toString());
        retVal.append(": ");
        retVal.append(this.suborbitHistoryAtt.getStringValue());
        retVal.append("\n");
        retVal.append(HeaderInfoTitle.PROCESSING_SYSTEM.toString());
        retVal.append(": ");
        retVal.append(this.processingSystemAtt.getStringValue());
        retVal.append("\n");
        retVal.append(HeaderInfoTitle.PROCESSING_DATE.toString());
        retVal.append(": ");
        retVal.append(DateFormatHandler.ALT_DATE_TIME.getDateTimeStringFromDate(this.processingDate));
        retVal.append("\n");
        retVal.append(HeaderInfoTitle.SPACECRAFT_ID.toString());
        retVal.append(": ");
        retVal.append(this.spacecraftIdAtt.getStringValue());
        retVal.append("\n");
        retVal.append(HeaderInfoTitle.NORAD_ID.toString());
        retVal.append(": ");
        retVal.append(this.noradIdAtt.getStringValue());
        retVal.append("\n");
        return retVal.toString();
    }
    
    static class DateFormatHandler
    {
        public static final DateFormatHandler ISO_DATE;
        public static final DateFormatHandler ISO_TIME;
        public static final DateFormatHandler ISO_DATE_TIME;
        public static final DateFormatHandler ALT_DATE_TIME;
        private String dateTimeFormatString;
        
        private DateFormatHandler(final String dateTimeFormatString) {
            this.dateTimeFormatString = null;
            this.dateTimeFormatString = dateTimeFormatString;
        }
        
        public String getDateTimeFormatString() {
            return this.dateTimeFormatString;
        }
        
        public Date getDateFromDateTimeString(final String dateTimeString) throws ParseException {
            Date theDate = null;
            final SimpleDateFormat dateFormat = new SimpleDateFormat(this.dateTimeFormatString, Locale.US);
            dateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
            theDate = dateFormat.parse(dateTimeString);
            return theDate;
        }
        
        public String getDateTimeStringFromDate(final Date date) {
            final SimpleDateFormat dateFormat = new SimpleDateFormat(this.dateTimeFormatString, Locale.US);
            dateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
            final String dateString = dateFormat.format(date);
            return dateString;
        }
        
        static {
            ISO_DATE = new DateFormatHandler("yyyy-MM-dd");
            ISO_TIME = new DateFormatHandler("HH:mm:ss.SSSz");
            ISO_DATE_TIME = new DateFormatHandler("yyyy-MM-dd'T'HH:mm:ss.SSSz");
            ALT_DATE_TIME = new DateFormatHandler("EEE MMM dd HH:mm:ss yyyy");
        }
    }
    
    static class HeaderInfoTitle
    {
        private static HashMap hash;
        public static final HeaderInfoTitle FILE_ID;
        public static final HeaderInfoTitle DATA_SET_ID;
        public static final HeaderInfoTitle RECORD_BYTES;
        public static final HeaderInfoTitle NUM_HEADER_RECORDS;
        public static final HeaderInfoTitle NUM_RECORDS;
        public static final HeaderInfoTitle SUBORBIT_HISTORY;
        public static final HeaderInfoTitle PROCESSING_SYSTEM;
        public static final HeaderInfoTitle PROCESSING_DATE;
        public static final HeaderInfoTitle SPACECRAFT_ID;
        public static final HeaderInfoTitle NORAD_ID;
        public static final HeaderInfoTitle START_DATE_UTC;
        public static final HeaderInfoTitle START_TIME_UTC;
        public static final HeaderInfoTitle END_DATE_UTC;
        public static final HeaderInfoTitle END_TIME_UTC;
        public static final HeaderInfoTitle START_DATE_LOCAL;
        public static final HeaderInfoTitle START_TIME_LOCAL;
        public static final HeaderInfoTitle START_LAT_LON;
        public static final HeaderInfoTitle END_LAT_LON;
        public static final HeaderInfoTitle START_SUBSOLAR_COORD;
        public static final HeaderInfoTitle END_SUBSOLAR_COORD;
        public static final HeaderInfoTitle START_LUNAR_COORD;
        public static final HeaderInfoTitle END_LUNAR_COORD;
        public static final HeaderInfoTitle ASCENDING_NODE;
        public static final HeaderInfoTitle NODE_HEADING;
        public static final HeaderInfoTitle EPHEMERIS_SOURCE;
        public static final HeaderInfoTitle NUM_DATA_RECORDS;
        public static final HeaderInfoTitle NUM_ARTIFICIAL_DATA_RECORDS;
        public static final HeaderInfoTitle NOMINAL_RESOLUTION;
        public static final HeaderInfoTitle BANDS_PER_SCANLINE;
        public static final HeaderInfoTitle SAMPLES_PER_BAND;
        public static final HeaderInfoTitle BYTES_PER_SAMPLE;
        public static final HeaderInfoTitle BYTE_OFFSET_BAND_1;
        public static final HeaderInfoTitle BYTE_OFFSET_BAND_2;
        public static final HeaderInfoTitle BAND_1;
        public static final HeaderInfoTitle BAND_2;
        public static final HeaderInfoTitle ORGANIZATION;
        public static final HeaderInfoTitle THERMAL_OFFSET;
        public static final HeaderInfoTitle THERMAL_SCALE;
        public static final HeaderInfoTitle QC_FLAGS;
        public static final HeaderInfoTitle PERCENT_DAYLIGHT;
        public static final HeaderInfoTitle PERCENT_FULL_MOON;
        public static final HeaderInfoTitle PERCENT_TERMINATOR_EVIDENT;
        public static final HeaderInfoTitle END_HEADER;
        private String HeaderInfoTitle;
        
        private HeaderInfoTitle(final String title) {
            this.HeaderInfoTitle = title;
            HeaderInfoTitle.hash.put(title, this);
        }
        
        public static HeaderInfoTitle getTitle(final String title) {
            if (title == null) {
                return null;
            }
            return HeaderInfoTitle.hash.get(title);
        }
        
        @Override
        public String toString() {
            return this.HeaderInfoTitle;
        }
        
        static {
            HeaderInfoTitle.hash = new HashMap(20);
            FILE_ID = new HeaderInfoTitle("file ID");
            DATA_SET_ID = new HeaderInfoTitle("data set ID");
            RECORD_BYTES = new HeaderInfoTitle("record bytes");
            NUM_HEADER_RECORDS = new HeaderInfoTitle("number of header records");
            NUM_RECORDS = new HeaderInfoTitle("number of records");
            SUBORBIT_HISTORY = new HeaderInfoTitle("suborbit history");
            PROCESSING_SYSTEM = new HeaderInfoTitle("processing system");
            PROCESSING_DATE = new HeaderInfoTitle("processing date");
            SPACECRAFT_ID = new HeaderInfoTitle("spacecraft ID");
            NORAD_ID = new HeaderInfoTitle("NORAD ID");
            START_DATE_UTC = new HeaderInfoTitle("start date UTC");
            START_TIME_UTC = new HeaderInfoTitle("start time UTC");
            END_DATE_UTC = new HeaderInfoTitle("end date UTC");
            END_TIME_UTC = new HeaderInfoTitle("end time UTC");
            START_DATE_LOCAL = new HeaderInfoTitle("start date local");
            START_TIME_LOCAL = new HeaderInfoTitle("start time local");
            START_LAT_LON = new HeaderInfoTitle("start lat,lon");
            END_LAT_LON = new HeaderInfoTitle("end lat,lon");
            START_SUBSOLAR_COORD = new HeaderInfoTitle("start sub-solar coord");
            END_SUBSOLAR_COORD = new HeaderInfoTitle("end sub-solar coord");
            START_LUNAR_COORD = new HeaderInfoTitle("start lunar coord");
            END_LUNAR_COORD = new HeaderInfoTitle("end lunar coord");
            ASCENDING_NODE = new HeaderInfoTitle("ascending node");
            NODE_HEADING = new HeaderInfoTitle("node heading");
            EPHEMERIS_SOURCE = new HeaderInfoTitle("ephemeris source");
            NUM_DATA_RECORDS = new HeaderInfoTitle("number of data records");
            NUM_ARTIFICIAL_DATA_RECORDS = new HeaderInfoTitle("number of artificial data records");
            NOMINAL_RESOLUTION = new HeaderInfoTitle("nominal resolution");
            BANDS_PER_SCANLINE = new HeaderInfoTitle("bands per scanline");
            SAMPLES_PER_BAND = new HeaderInfoTitle("samples per band");
            BYTES_PER_SAMPLE = new HeaderInfoTitle("bytes per sample");
            BYTE_OFFSET_BAND_1 = new HeaderInfoTitle("byte offset band 1");
            BYTE_OFFSET_BAND_2 = new HeaderInfoTitle("byte offset band 2");
            BAND_1 = new HeaderInfoTitle("band 1");
            BAND_2 = new HeaderInfoTitle("band 2");
            ORGANIZATION = new HeaderInfoTitle("organization");
            THERMAL_OFFSET = new HeaderInfoTitle("thermal offset");
            THERMAL_SCALE = new HeaderInfoTitle("thermal scale");
            QC_FLAGS = new HeaderInfoTitle("QC flags");
            PERCENT_DAYLIGHT = new HeaderInfoTitle("% daylight");
            PERCENT_FULL_MOON = new HeaderInfoTitle("% full moon");
            PERCENT_TERMINATOR_EVIDENT = new HeaderInfoTitle("% terminator evident");
            END_HEADER = new HeaderInfoTitle("end header");
        }
    }
}
