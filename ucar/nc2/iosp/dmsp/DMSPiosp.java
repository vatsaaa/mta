// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.iosp.dmsp;

import java.util.Set;
import ucar.ma2.DataType;
import java.util.HashMap;
import ucar.ma2.InvalidRangeException;
import ucar.ma2.Range;
import java.util.Calendar;
import java.util.Locale;
import java.util.TimeZone;
import ucar.ma2.Array;
import ucar.ma2.Section;
import java.util.Iterator;
import ucar.nc2.Group;
import java.text.ParseException;
import java.io.IOException;
import ucar.nc2.constants.AxisType;
import ucar.nc2.Attribute;
import ucar.nc2.Dimension;
import java.util.List;
import ucar.nc2.Structure;
import ucar.nc2.Variable;
import java.util.ArrayList;
import ucar.nc2.util.CancelTask;
import java.util.Date;
import ucar.unidata.io.RandomAccessFile;
import ucar.nc2.NetcdfFile;
import ucar.nc2.iosp.AbstractIOServiceProvider;

public class DMSPiosp extends AbstractIOServiceProvider
{
    private NetcdfFile ncFile;
    private RandomAccessFile raf;
    DMSPHeader header;
    private float[] calculatedTime;
    private String startDateString;
    private Date startDate;
    private int[] cachedYear;
    private int[] cachedDayOfYear;
    private double[] cachedSecondsOfDay;
    private float[] calculatedLatitude;
    private float[] calculatedLongitude;
    private float[] cachedSatEphemLatitude;
    private float[] cachedSatEphemLongitude;
    private float[] cachedSatEphemAltitude;
    private float[] cachedSatEphemHeading;
    private float[] cachedScannerOffset;
    private byte[] cachedScanDirection;
    
    public DMSPiosp() {
        this.ncFile = null;
        this.raf = null;
        this.header = null;
        this.calculatedTime = null;
        this.startDateString = null;
        this.startDate = null;
        this.cachedYear = null;
        this.cachedDayOfYear = null;
        this.cachedSecondsOfDay = null;
        this.calculatedLatitude = null;
        this.calculatedLongitude = null;
        this.cachedSatEphemLatitude = null;
        this.cachedSatEphemLongitude = null;
        this.cachedSatEphemAltitude = null;
        this.cachedSatEphemHeading = null;
        this.cachedScannerOffset = null;
        this.cachedScanDirection = null;
    }
    
    public boolean isValidFile(final RandomAccessFile raf) {
        final DMSPHeader localHeader = new DMSPHeader();
        return localHeader.isValidFile(raf);
    }
    
    public String getFileTypeId() {
        return "DMSP";
    }
    
    public String getFileTypeDescription() {
        return "Defense Meteorological Satellite Program";
    }
    
    @Override
    public void open(final RandomAccessFile raf, final NetcdfFile ncfile, final CancelTask cancelTask) throws IOException {
        this.ncFile = ncfile;
        (this.raf = raf).order(0);
        (this.header = new DMSPHeader()).read(this.raf, this.ncFile);
        final List nonScanDimList = new ArrayList();
        nonScanDimList.add(this.header.getNumDataRecordsDim());
        final List scanDimList = new ArrayList();
        scanDimList.add(this.header.getNumDataRecordsDim());
        scanDimList.add(this.header.getNumSamplesPerBandDim());
        final Iterator varInfoIt = VariableInfo.getAll().iterator();
        VariableInfo curVarInfo = null;
        Variable curVariable = null;
        while (varInfoIt.hasNext()) {
            curVarInfo = varInfoIt.next();
            curVariable = new Variable(this.ncFile, this.ncFile.getRootGroup(), null, curVarInfo.getName());
            curVariable.setDataType(curVarInfo.getDataType());
            if (curVarInfo.getNumElementsInRecord() == 1) {
                curVariable.setDimensions(nonScanDimList);
            }
            else {
                curVariable.setDimensions(scanDimList);
            }
            curVariable.addAttribute(new Attribute("long_name", curVarInfo.getLongName()));
            curVariable.addAttribute(new Attribute("units", curVarInfo.getUnits()));
            if (curVariable.getName().equals("latitude")) {
                curVariable.addAttribute(new Attribute("calculatedVariable", "Using the geometry of the satellite scans and an ellipsoidal earth (a=6378.14km and e=0.0818191830)."));
                curVariable.addAttribute(new Attribute("_CoordinateAxisType", AxisType.Lat.toString()));
            }
            else if (curVariable.getName().equals("longitude")) {
                curVariable.addAttribute(new Attribute("calculatedVariable", "Using the geometry of the satellite scans and an ellipsoidal earth (a=6378.14km and e=0.0818191830)."));
                curVariable.addAttribute(new Attribute("_CoordinateAxisType", AxisType.Lon.toString()));
            }
            else if (curVariable.getName().equals("time")) {
                curVariable.addAttribute(new Attribute("calculatedVariable", "Using the satellite epoch for each scan."));
                this.startDateString = this.header.getStartDateAtt().getStringValue();
                try {
                    this.startDate = DMSPHeader.DateFormatHandler.ISO_DATE_TIME.getDateFromDateTimeString(this.startDateString);
                }
                catch (ParseException e) {
                    throw new IOException("Invalid DMSP file: \"startDate\" attribute value <" + this.startDateString + "> not parseable with format string <" + DMSPHeader.DateFormatHandler.ISO_DATE_TIME.getDateTimeFormatString() + ">.");
                }
                curVariable.addAttribute(new Attribute("units", "seconds since " + this.startDateString));
                curVariable.addAttribute(new Attribute("_CoordinateAxisType", AxisType.Time.toString()));
            }
            else if (curVariable.getName().equals("infraredImagery")) {
                curVariable.addAttribute(new Attribute("_CoordinateAxes", "latitude longitude"));
                curVariable.addAttribute(new Attribute("_Unsigned", "true"));
                curVariable.addAttribute(new Attribute("scale_factor", new Float(0.47058823529411764)));
                curVariable.addAttribute(new Attribute("add_offset", new Float(190.0)));
                curVariable.addAttribute(new Attribute("description", "Infrared pixel values correspond to a temperature range of 190 to 310 Kelvins in 256 equally spaced steps. Onboard calibration is performed during each scan. -- From http://dmsp.ngdc.noaa.gov/html/sensors/doc_ols.html"));
            }
            else if (curVariable.getName().equals("visibleImagery")) {
                curVariable.addAttribute(new Attribute("_CoordinateAxes", "latitude longitude"));
                curVariable.addAttribute(new Attribute("_Unsigned", "true"));
                curVariable.addAttribute(new Attribute("description", "Visible pixels are relative values ranging from 0 to 63 rather than absolute values in Watts per m^2. Instrumental gain levels are adjusted to maintain constant cloud reference values under varying conditions of solar and lunar illumination. Telescope pixel values are replaced by Photo Multiplier Tube (PMT) values at night. -- From http://dmsp.ngdc.noaa.gov/html/sensors/doc_ols.html"));
            }
            this.ncFile.addVariable(null, curVariable);
        }
        this.ncFile.finish();
    }
    
    public Array readData(final Variable v2, final Section section) throws IOException, InvalidRangeException {
        if (v2 == null) {
            throw new IllegalArgumentException("Variable must not be null.");
        }
        if (section == null) {
            throw new IllegalArgumentException("Section must not be null.");
        }
        Object data = null;
        Array dataArray = null;
        final List<Range> ranges = section.getRanges();
        if (v2.getName().equals(VariableInfo.YEAR.getName())) {
            if (this.cachedYear == null) {
                this.cachedYear = (int[])this.readIntArray1D(VariableInfo.YEAR.getByteOffsetInRecord());
            }
            data = this.cachedYear;
            dataArray = Array.factory(Integer.TYPE, v2.getShape(), data);
            return dataArray.sectionNoReduce(ranges).copy();
        }
        if (v2.getName().equals(VariableInfo.DAY_OF_YEAR.getName())) {
            if (this.cachedDayOfYear == null) {
                this.cachedDayOfYear = (int[])this.readIntArray1D(VariableInfo.DAY_OF_YEAR.getByteOffsetInRecord());
            }
            data = this.cachedDayOfYear;
            dataArray = Array.factory(Integer.TYPE, v2.getShape(), data);
            return dataArray.sectionNoReduce(ranges).copy();
        }
        if (v2.getName().equals(VariableInfo.SECONDS_OF_DAY.getName())) {
            if (this.cachedSecondsOfDay == null) {
                this.cachedSecondsOfDay = (double[])this.readDoubleArray1D(VariableInfo.SECONDS_OF_DAY.getByteOffsetInRecord());
            }
            data = this.cachedSecondsOfDay;
            dataArray = Array.factory(Double.TYPE, v2.getShape(), data);
            return dataArray.sectionNoReduce(ranges).copy();
        }
        if (v2.getName().equals(VariableInfo.TIME.getName())) {
            if (this.calculatedTime == null) {
                this.calculatedTime = new float[v2.getShape()[0]];
                Variable curVar = this.ncFile.findVariable(VariableInfo.YEAR.getName());
                this.readData(curVar, curVar.getShapeAsSection());
                curVar = this.ncFile.findVariable(VariableInfo.DAY_OF_YEAR.getName());
                this.readData(curVar, curVar.getShapeAsSection());
                curVar = this.ncFile.findVariable(VariableInfo.SECONDS_OF_DAY.getName());
                this.readData(curVar, curVar.getShapeAsSection());
                final Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("GMT"), Locale.US);
                for (int i = 0; i < v2.getShape()[0]; ++i) {
                    calendar.clear();
                    calendar.set(1, this.cachedYear[i]);
                    calendar.set(6, this.cachedDayOfYear[i]);
                    final double secOfDay = this.cachedSecondsOfDay[i];
                    final double hours = Math.floor(secOfDay / 3600.0);
                    final double secOfHour = secOfDay % 3600.0;
                    final double mins = Math.floor(secOfHour / 60.0);
                    final double secOfMinute = secOfHour % 60.0;
                    final double secs = Math.floor(secOfMinute);
                    final double millis = Math.floor((secOfMinute - secs) * 1000.0);
                    calendar.add(11, (int)hours);
                    calendar.add(12, (int)mins);
                    calendar.add(13, (int)secs);
                    calendar.add(14, (int)millis);
                    this.calculatedTime[i] = (calendar.getTimeInMillis() - this.startDate.getTime()) / 1000.0f;
                }
                dataArray = Array.factory(Float.TYPE, v2.getShape(), this.calculatedTime);
                return dataArray.sectionNoReduce(ranges).copy();
            }
            return null;
        }
        else {
            if (v2.getName().equals(VariableInfo.SAT_EPHEM_LATITUDE.getName())) {
                if (this.cachedSatEphemLatitude == null) {
                    this.cachedSatEphemLatitude = (float[])this.readFloatArray1D(VariableInfo.SAT_EPHEM_LATITUDE.getByteOffsetInRecord());
                }
                data = this.cachedSatEphemLatitude;
                dataArray = Array.factory(Float.TYPE, v2.getShape(), data);
                return dataArray.sectionNoReduce(ranges).copy();
            }
            if (v2.getName().equals(VariableInfo.SAT_EPHEM_LONGITUDE.getName())) {
                if (this.cachedSatEphemLongitude == null) {
                    this.cachedSatEphemLongitude = (float[])this.readFloatArray1D(VariableInfo.SAT_EPHEM_LONGITUDE.getByteOffsetInRecord());
                }
                data = this.cachedSatEphemLongitude;
                dataArray = Array.factory(Float.TYPE, v2.getShape(), data);
                return dataArray.sectionNoReduce(ranges).copy();
            }
            if (v2.getName().equals(VariableInfo.SAT_EPHEM_ALTITUDE.getName())) {
                if (this.cachedSatEphemAltitude == null) {
                    this.cachedSatEphemAltitude = (float[])this.readFloatArray1D(VariableInfo.SAT_EPHEM_ALTITUDE.getByteOffsetInRecord());
                }
                data = this.cachedSatEphemAltitude;
                dataArray = Array.factory(Float.TYPE, v2.getShape(), data);
                return dataArray.sectionNoReduce(ranges).copy();
            }
            if (v2.getName().equals(VariableInfo.SAT_EPHEM_HEADING.getName())) {
                if (this.cachedSatEphemHeading == null) {
                    this.cachedSatEphemHeading = (float[])this.readFloatArray1D(VariableInfo.SAT_EPHEM_HEADING.getByteOffsetInRecord());
                }
                data = this.cachedSatEphemHeading;
                dataArray = Array.factory(Float.TYPE, v2.getShape(), data);
                return dataArray.sectionNoReduce(ranges).copy();
            }
            if (v2.getName().equals(VariableInfo.SCANNER_OFFSET.getName())) {
                if (this.cachedScannerOffset == null) {
                    this.cachedScannerOffset = (float[])this.readFloatArray1D(VariableInfo.SCANNER_OFFSET.getByteOffsetInRecord());
                }
                data = this.cachedScannerOffset;
                dataArray = Array.factory(Float.TYPE, v2.getShape(), data);
                return dataArray.sectionNoReduce(ranges).copy();
            }
            if (v2.getName().equals(VariableInfo.SCAN_DIRECTION.getName())) {
                if (this.cachedScanDirection == null) {
                    this.cachedScanDirection = (byte[])this.readUCharArray1D(VariableInfo.SCAN_DIRECTION.getByteOffsetInRecord());
                }
                data = this.cachedScanDirection;
                dataArray = Array.factory(Byte.TYPE, v2.getShape(), data);
                return dataArray.sectionNoReduce(ranges).copy();
            }
            if (v2.getName().equals(VariableInfo.SOLAR_ELEVATION.getName())) {
                data = this.readFloatArray1D(VariableInfo.SOLAR_ELEVATION.getByteOffsetInRecord());
                dataArray = Array.factory(Float.TYPE, v2.getShape(), data);
                return dataArray.sectionNoReduce(ranges);
            }
            if (v2.getName().equals(VariableInfo.SOLAR_AZIMUTH.getName())) {
                data = this.readFloatArray1D(VariableInfo.SOLAR_AZIMUTH.getByteOffsetInRecord());
                dataArray = Array.factory(Float.TYPE, v2.getShape(), data);
                return dataArray.sectionNoReduce(ranges);
            }
            if (v2.getName().equals(VariableInfo.LUNAR_ELEVATION.getName())) {
                data = this.readFloatArray1D(VariableInfo.LUNAR_ELEVATION.getByteOffsetInRecord());
                dataArray = Array.factory(Float.TYPE, v2.getShape(), data);
                return dataArray.sectionNoReduce(ranges);
            }
            if (v2.getName().equals(VariableInfo.LUNAR_AZIMUTH.getName())) {
                data = this.readFloatArray1D(VariableInfo.LUNAR_AZIMUTH.getByteOffsetInRecord());
                dataArray = Array.factory(Float.TYPE, v2.getShape(), data);
                return dataArray.sectionNoReduce(ranges);
            }
            if (v2.getName().equals(VariableInfo.LUNAR_PHASE.getName())) {
                data = this.readFloatArray1D(VariableInfo.LUNAR_PHASE.getByteOffsetInRecord());
                dataArray = Array.factory(Float.TYPE, v2.getShape(), data);
                return dataArray.sectionNoReduce(ranges);
            }
            if (v2.getName().equals(VariableInfo.GAIN_CODE.getName())) {
                data = this.readFloatArray1D(VariableInfo.GAIN_CODE.getByteOffsetInRecord());
                dataArray = Array.factory(Float.TYPE, v2.getShape(), data);
                return dataArray.sectionNoReduce(ranges);
            }
            if (v2.getName().equals(VariableInfo.GAIN_MODE.getName())) {
                data = this.readUCharArray1D(VariableInfo.GAIN_MODE.getByteOffsetInRecord());
                dataArray = Array.factory(Byte.TYPE, v2.getShape(), data);
                return dataArray.sectionNoReduce(ranges);
            }
            if (v2.getName().equals(VariableInfo.GAIN_SUB_MODE.getName())) {
                data = this.readUCharArray1D(VariableInfo.GAIN_SUB_MODE.getByteOffsetInRecord());
                dataArray = Array.factory(Byte.TYPE, v2.getShape(), data);
                return dataArray.sectionNoReduce(ranges);
            }
            if (v2.getName().equals(VariableInfo.T_CHANNEL_GAIN.getName())) {
                data = this.readFloatArray1D(VariableInfo.T_CHANNEL_GAIN.getByteOffsetInRecord());
                dataArray = Array.factory(Float.TYPE, v2.getShape(), data);
                return dataArray.sectionNoReduce(ranges);
            }
            if (v2.getName().equals(VariableInfo.HOT_T_CAL_SEGMENT_ID.getName())) {
                data = this.readUCharArray1D(VariableInfo.HOT_T_CAL_SEGMENT_ID.getByteOffsetInRecord());
                dataArray = Array.factory(Byte.TYPE, v2.getShape(), data);
                return dataArray.sectionNoReduce(ranges);
            }
            if (v2.getName().equals(VariableInfo.COLD_T_CAL_SEGMENT_ID.getName())) {
                data = this.readUCharArray1D(VariableInfo.COLD_T_CAL_SEGMENT_ID.getByteOffsetInRecord());
                dataArray = Array.factory(Byte.TYPE, v2.getShape(), data);
                return dataArray.sectionNoReduce(ranges);
            }
            if (v2.getName().equals(VariableInfo.HOT_T_CAL.getName())) {
                data = this.readUCharArray1D(VariableInfo.HOT_T_CAL.getByteOffsetInRecord());
                dataArray = Array.factory(Byte.TYPE, v2.getShape(), data);
                return dataArray.sectionNoReduce(ranges);
            }
            if (v2.getName().equals(VariableInfo.COLD_T_CAL.getName())) {
                data = this.readUCharArray1D(VariableInfo.COLD_T_CAL.getByteOffsetInRecord());
                dataArray = Array.factory(Byte.TYPE, v2.getShape(), data);
                return dataArray.sectionNoReduce(ranges);
            }
            if (v2.getName().equals(VariableInfo.PMT_CAL.getName())) {
                data = this.readUCharArray1D(VariableInfo.PMT_CAL.getByteOffsetInRecord());
                dataArray = Array.factory(Byte.TYPE, v2.getShape(), data);
                return dataArray.sectionNoReduce(ranges);
            }
            if (v2.getName().equals(VariableInfo.VISIBLE_SCAN_QUALITY_FLAG.getName())) {
                data = this.readIntArray1D(VariableInfo.VISIBLE_SCAN_QUALITY_FLAG.getByteOffsetInRecord());
                dataArray = Array.factory(Integer.TYPE, v2.getShape(), data);
                return dataArray.sectionNoReduce(ranges);
            }
            if (v2.getName().equals(VariableInfo.THERMAL_SCAN_QUALITY_FLAG.getName())) {
                data = this.readIntArray1D(VariableInfo.THERMAL_SCAN_QUALITY_FLAG.getByteOffsetInRecord());
                dataArray = Array.factory(Integer.TYPE, v2.getShape(), data);
                return dataArray.sectionNoReduce(ranges);
            }
            if (v2.getName().equals(VariableInfo.VISIBLE_SCAN.getName())) {
                data = this.readByteArray2D(VariableInfo.VISIBLE_SCAN.getByteOffsetInRecord(), VariableInfo.VISIBLE_SCAN.getNumElementsInRecord());
                dataArray = Array.factory(Byte.TYPE, v2.getShape(), data);
                return dataArray.sectionNoReduce(ranges);
            }
            if (v2.getName().equals(VariableInfo.THERMAL_SCAN.getName())) {
                data = this.readByteArray2D(VariableInfo.THERMAL_SCAN.getByteOffsetInRecord(), VariableInfo.THERMAL_SCAN.getNumElementsInRecord());
                dataArray = Array.factory(Byte.TYPE, v2.getShape(), data);
                return dataArray.sectionNoReduce(ranges);
            }
            if (v2.getName().equals(VariableInfo.LATITUDE.getName()) || v2.getName().equals(VariableInfo.LONGITUDE.getName())) {
                if (this.calculatedLatitude == null && this.calculatedLongitude == null) {
                    this.calculatedLatitude = new float[this.header.getNumDataRecords() * this.header.getNumSamplesPerBandDim().getLength()];
                    this.calculatedLongitude = new float[this.header.getNumDataRecords() * this.header.getNumSamplesPerBandDim().getLength()];
                    Variable curVar = this.ncFile.findVariable(VariableInfo.SCANNER_OFFSET.getName());
                    this.readData(curVar, curVar.getShapeAsSection());
                    curVar = this.ncFile.findVariable(VariableInfo.SAT_EPHEM_LATITUDE.getName());
                    this.readData(curVar, curVar.getShapeAsSection());
                    curVar = this.ncFile.findVariable(VariableInfo.SAT_EPHEM_LONGITUDE.getName());
                    this.readData(curVar, curVar.getShapeAsSection());
                    curVar = this.ncFile.findVariable(VariableInfo.SAT_EPHEM_ALTITUDE.getName());
                    this.readData(curVar, curVar.getShapeAsSection());
                    curVar = this.ncFile.findVariable(VariableInfo.SAT_EPHEM_HEADING.getName());
                    this.readData(curVar, curVar.getShapeAsSection());
                    final int satID = Integer.parseInt(this.ncFile.getRootGroup().findAttribute("spacecraftId").getStringValue().substring(1));
                    GeolocateOLS.geolocateOLS(satID, 0, this.header.getNumDataRecords(), this.cachedScannerOffset, this.cachedSatEphemLatitude, this.cachedSatEphemLongitude, this.cachedSatEphemAltitude, this.cachedSatEphemHeading, this.calculatedLatitude, this.calculatedLongitude);
                }
                if (v2.getName().equals(VariableInfo.LATITUDE.getName())) {
                    data = this.calculatedLatitude;
                }
                else {
                    data = this.calculatedLongitude;
                }
                dataArray = Array.factory(Float.TYPE, v2.getShape(), data);
                return dataArray.sectionNoReduce(ranges).copy();
            }
            throw new IOException("Requested variable <name=" + v2.getName() + "> not in DMSP file.");
        }
    }
    
    @Override
    public void close() throws IOException {
        if (this.raf != null) {
            this.raf.close();
        }
        this.header = null;
    }
    
    Object readUCharArray1D(final int offsetInRecord) throws IOException {
        final int elementSizeInBytes = 4;
        final byte[] elementArray = new byte[elementSizeInBytes];
        final byte[] array = new byte[this.header.getNumDataRecords()];
        this.raf.seek(this.header.getRecordSizeInBytes() * this.header.getNumHeaderRecords() + offsetInRecord);
        for (int i = 0; i < this.header.getNumDataRecords(); ++i) {
            this.raf.read(elementArray);
            array[i] = elementArray[3];
            this.raf.skipBytes(this.header.getRecordSizeInBytes() - elementSizeInBytes);
        }
        return array;
    }
    
    Object readIntArray1D(final int offsetInRecord) throws IOException {
        final int elementSizeInBytes = 4;
        final int[] array = new int[this.header.getNumDataRecords()];
        this.raf.seek(this.header.getRecordSizeInBytes() * this.header.getNumHeaderRecords() + offsetInRecord);
        for (int i = 0; i < this.header.getNumDataRecords(); ++i) {
            this.raf.readInt(array, i, 1);
            this.raf.skipBytes(this.header.getRecordSizeInBytes() - elementSizeInBytes);
        }
        return array;
    }
    
    Object readFloatArray1D(final int offsetInRecord) throws IOException {
        final int elementSizeInBytes = 4;
        final float[] array = new float[this.header.getNumDataRecords()];
        this.raf.seek(this.header.getRecordSizeInBytes() * this.header.getNumHeaderRecords() + offsetInRecord);
        for (int i = 0; i < this.header.getNumDataRecords(); ++i) {
            this.raf.readFloat(array, i, 1);
            this.raf.skipBytes(this.header.getRecordSizeInBytes() - elementSizeInBytes);
        }
        return array;
    }
    
    Object readDoubleArray1D(final int offsetInRecord) throws IOException {
        final int elementSizeInBytes = 8;
        final double[] array = new double[this.header.getNumDataRecords()];
        this.raf.seek(this.header.getRecordSizeInBytes() * this.header.getNumHeaderRecords() + offsetInRecord);
        for (int i = 0; i < this.header.getNumDataRecords(); ++i) {
            this.raf.readDouble(array, i, 1);
            this.raf.skipBytes(this.header.getRecordSizeInBytes() - elementSizeInBytes);
        }
        return array;
    }
    
    Object readByteArray2D(final int offsetInRecord, final int numElementsInRecord) throws IOException {
        final byte[] array = new byte[this.header.getNumDataRecords() * numElementsInRecord];
        this.raf.seek(this.header.getRecordSizeInBytes() * this.header.getNumHeaderRecords() + offsetInRecord);
        for (int i = 0; i < this.header.getNumDataRecords(); ++i) {
            this.raf.read(array, i * numElementsInRecord, numElementsInRecord);
            this.raf.skipBytes(this.header.getRecordSizeInBytes() - numElementsInRecord);
        }
        return array;
    }
    
    private static class VariableInfo
    {
        private static List list;
        private static HashMap hash;
        public static final VariableInfo YEAR;
        public static final VariableInfo DAY_OF_YEAR;
        public static final VariableInfo SECONDS_OF_DAY;
        public static final VariableInfo TIME;
        public static final VariableInfo SAT_EPHEM_LATITUDE;
        public static final VariableInfo SAT_EPHEM_LONGITUDE;
        public static final VariableInfo SAT_EPHEM_ALTITUDE;
        public static final VariableInfo SAT_EPHEM_HEADING;
        public static final VariableInfo SCANNER_OFFSET;
        public static final VariableInfo SCAN_DIRECTION;
        public static final VariableInfo SOLAR_ELEVATION;
        public static final VariableInfo SOLAR_AZIMUTH;
        public static final VariableInfo LUNAR_ELEVATION;
        public static final VariableInfo LUNAR_AZIMUTH;
        public static final VariableInfo LUNAR_PHASE;
        public static final VariableInfo GAIN_CODE;
        public static final VariableInfo GAIN_MODE;
        public static final VariableInfo GAIN_SUB_MODE;
        public static final VariableInfo HOT_T_CAL_SEGMENT_ID;
        public static final VariableInfo COLD_T_CAL_SEGMENT_ID;
        public static final VariableInfo HOT_T_CAL;
        public static final VariableInfo COLD_T_CAL;
        public static final VariableInfo PMT_CAL;
        public static final VariableInfo T_CHANNEL_GAIN;
        public static final VariableInfo VISIBLE_SCAN_QUALITY_FLAG;
        public static final VariableInfo THERMAL_SCAN_QUALITY_FLAG;
        public static final VariableInfo LATITUDE;
        public static final VariableInfo LONGITUDE;
        public static final VariableInfo VISIBLE_SCAN;
        public static final VariableInfo THERMAL_SCAN;
        private String name;
        private String longName;
        private String units;
        private DataType dataType;
        private int byteOffsetInRecord;
        private int numElementsInRecord;
        
        private VariableInfo(final String name, final String long_name, final String units, final DataType dataType, final int byteOffsetInRecord, final int numElementsInRecord) {
            this.name = null;
            this.longName = null;
            this.units = null;
            this.dataType = null;
            this.byteOffsetInRecord = -1;
            this.numElementsInRecord = 0;
            this.name = name;
            this.longName = long_name;
            this.units = units;
            this.dataType = dataType;
            this.byteOffsetInRecord = byteOffsetInRecord;
            this.numElementsInRecord = numElementsInRecord;
            VariableInfo.list.add(this);
            VariableInfo.hash.put(this.name, this);
        }
        
        public static VariableInfo findByName(final String name) {
            if (name == null) {
                return null;
            }
            return VariableInfo.hash.get(name);
        }
        
        public static List getAll() {
            return VariableInfo.list;
        }
        
        public static Set getAllNames() {
            return VariableInfo.hash.keySet();
        }
        
        public String getName() {
            return this.name;
        }
        
        public String getLongName() {
            return this.longName;
        }
        
        public String getUnits() {
            return this.units;
        }
        
        public DataType getDataType() {
            return this.dataType;
        }
        
        public int getByteOffsetInRecord() {
            return this.byteOffsetInRecord;
        }
        
        public int getNumElementsInRecord() {
            return this.numElementsInRecord;
        }
        
        @Override
        public String toString() {
            final StringBuffer retVal = new StringBuffer();
            retVal.append("Variable(").append(this.getName()).append(",").append(this.getLongName()).append(",").append(this.getUnits()).append(",").append(this.getDataType()).append(",").append(this.getByteOffsetInRecord()).append(",").append(this.getNumElementsInRecord()).append(")");
            return retVal.toString();
        }
        
        static {
            VariableInfo.list = new ArrayList(30);
            VariableInfo.hash = new HashMap(30);
            YEAR = new VariableInfo("year", "year at time of scan", "year", DataType.INT, 0, 1);
            DAY_OF_YEAR = new VariableInfo("dayOfYear", "day of year at time of scan", "day of year", DataType.INT, 4, 1);
            SECONDS_OF_DAY = new VariableInfo("secondsOfDay", "seconds of day at time of scan", "seconds of day", DataType.DOUBLE, 8, 1);
            TIME = new VariableInfo("time", "time of scan", "seconds since ??? (see above)", DataType.FLOAT, -1, 1);
            SAT_EPHEM_LATITUDE = new VariableInfo("satEphemLatitude", "geodetic latitude of the satellite for this scan", "degrees_north", DataType.FLOAT, 16, 1);
            SAT_EPHEM_LONGITUDE = new VariableInfo("satEphemLongitude", "longitude of the satellite for this scan", "degrees_east", DataType.FLOAT, 20, 1);
            SAT_EPHEM_ALTITUDE = new VariableInfo("satEphemAltitude", "altitude of the satellite for this scan", "kilometers", DataType.FLOAT, 24, 1);
            SAT_EPHEM_HEADING = new VariableInfo("satEphemHeading", "heading of the satellite (degrees west of north) for this scan", "degrees", DataType.FLOAT, 28, 1);
            SCANNER_OFFSET = new VariableInfo("scannerOffset", "scanner offset", "radians", DataType.FLOAT, 32, 1);
            SCAN_DIRECTION = new VariableInfo("scanDirection", "scan direction", "", DataType.BYTE, 36, 1);
            SOLAR_ELEVATION = new VariableInfo("solarElevation", "solar elevation", "degrees", DataType.FLOAT, 40, 1);
            SOLAR_AZIMUTH = new VariableInfo("solarAzimuth", "solar azimuth", "degrees", DataType.FLOAT, 44, 1);
            LUNAR_ELEVATION = new VariableInfo("lunarElevation", "lunar elevation", "degrees", DataType.FLOAT, 48, 1);
            LUNAR_AZIMUTH = new VariableInfo("lunarAzimuth", "lunar azimuth", "degrees", DataType.FLOAT, 52, 1);
            LUNAR_PHASE = new VariableInfo("lunarPhase", "lunar phase", "degrees", DataType.FLOAT, 56, 1);
            GAIN_CODE = new VariableInfo("gainCode", "gain code", "decibels", DataType.FLOAT, 60, 1);
            GAIN_MODE = new VariableInfo("gainMode", "gain mode (0=linear, 1=logrithmic)", "", DataType.BYTE, 64, 1);
            GAIN_SUB_MODE = new VariableInfo("gainSubMode", "gain sub-mode", "", DataType.BYTE, 68, 1);
            HOT_T_CAL_SEGMENT_ID = new VariableInfo("hotTCalSegmentID", "Hot T cal seg ID (0 = right, 1 = left)", "", DataType.BYTE, 72, 1);
            COLD_T_CAL_SEGMENT_ID = new VariableInfo("coldTCalSegmentID", "Cold T cal seg ID (0 = right, 1 = left)", "", DataType.BYTE, 76, 1);
            HOT_T_CAL = new VariableInfo("hotTCal", "Hot T calibration", "", DataType.BYTE, 80, 1);
            COLD_T_CAL = new VariableInfo("coldTCal", "Cold T calibration", "", DataType.BYTE, 84, 1);
            PMT_CAL = new VariableInfo("pmtCal", "Photomultiplier tube calibration", "", DataType.BYTE, 88, 1);
            T_CHANNEL_GAIN = new VariableInfo("tChannelGain", "T channel gain", "decibels", DataType.FLOAT, 92, 1);
            VISIBLE_SCAN_QUALITY_FLAG = new VariableInfo("visibleScanQualityFlag", "quality flag for the visible scan", "", DataType.INT, 96, 1);
            THERMAL_SCAN_QUALITY_FLAG = new VariableInfo("thermalScanQualityFlag", "quality flag for the thermal scan", "", DataType.INT, 1568, 1);
            LATITUDE = new VariableInfo("latitude", "latitude of pixel", "degrees_north", DataType.FLOAT, -1, 1465);
            LONGITUDE = new VariableInfo("longitude", "longitude of pixel", "degrees_east", DataType.FLOAT, -1, 1465);
            VISIBLE_SCAN = new VariableInfo("visibleImagery", "visible imagery  (6-bit per pixel)", "", DataType.BYTE, 100, 1465);
            THERMAL_SCAN = new VariableInfo("infraredImagery", "infrared imagery (8-bit per pixel)", "kelvin", DataType.BYTE, 1572, 1465);
        }
    }
    
    static class GeolocateOLS
    {
        static final double PI = 3.141592653589793;
        static final double TWO_PI = 6.283185307179586;
        static final double HALF_PI = 1.5707963267948966;
        static final double DEGREES_PER_RADIANS = 57.29577951308232;
        
        static void geolocateOLS(final int satID, final int dataType, final int numScans, final float[] scannerOffset, final float[] satEphemLatitude, final float[] satEphemLongitude, final float[] satEphemAltitude, final float[] satEphemHeading, final float[] latitude, final float[] longitude) {
            if (satID < OLSSensorModel.RANGE_SAT_GROUPS[0][0] || satID > OLSSensorModel.RANGE_SAT_GROUPS[1][1]) {
                throw new IllegalArgumentException("Satellite ID <" + satID + "> outside supported range <min=" + OLSSensorModel.RANGE_SAT_GROUPS[0][0] + ",max=" + OLSSensorModel.RANGE_SAT_GROUPS[1][1] + ">.");
            }
            if (dataType < 0 || dataType > 4) {
                throw new IllegalArgumentException("Data type <" + dataType + "> not in valid range <min=0,max=" + 4 + ">.");
            }
            if (scannerOffset.length != numScans) {
                throw new IllegalArgumentException("Size of scannerOffset vector <" + scannerOffset.length + "> not as expected <" + numScans + ">.");
            }
            if (satEphemLatitude.length != numScans) {
                throw new IllegalArgumentException("Size of satEphemLatitude vector <" + satEphemLatitude.length + "> not as expected <" + numScans + ">.");
            }
            if (satEphemLongitude.length != numScans) {
                throw new IllegalArgumentException("Size of satEphemLongitude vector <" + satEphemLongitude.length + "> not as expected <" + numScans + ">.");
            }
            if (satEphemAltitude.length != numScans) {
                throw new IllegalArgumentException("Size of satEphemAltitude vector <" + satEphemAltitude.length + "> not as expected <" + numScans + ">.");
            }
            if (satEphemHeading.length != numScans) {
                throw new IllegalArgumentException("Size of satEphemHeading vector <" + satEphemHeading.length + "> not as expected <" + numScans + ">.");
            }
            if (latitude.length != OLSSensorModel.numSamplesPerScan[dataType] * numScans) {
                throw new IllegalArgumentException("Size of latitude vector <" + latitude.length + "> not as expected <" + OLSSensorModel.numSamplesPerScan[dataType] + " * " + numScans + ">.");
            }
            if (longitude.length != OLSSensorModel.numSamplesPerScan[dataType] * numScans) {
                throw new IllegalArgumentException("Size of longitude vector <" + longitude.length + "> not as expected <" + OLSSensorModel.numSamplesPerScan[dataType] + " * " + numScans + ">.");
            }
            double[] subSatPoint = new double[3];
            final double[] surfaceNormal = new double[3];
            final double[] satPoint = new double[3];
            double[] north = new double[3];
            double[] west = new double[3];
            final double[] satHeading = new double[3];
            double[] scanLine = new double[3];
            final double[] scanPoint = new double[3];
            final double[] lineOfSightSlope = new double[3];
            final double[] geolocatedPoint = new double[3];
            final double[] scratchVec = new double[3];
            for (int swathIndex = 0; swathIndex < numScans; ++swathIndex) {
                final double gdLatitude = degreesToRadians(satEphemLatitude[swathIndex]);
                final double gdLongitude = degreesToRadians(satEphemLongitude[swathIndex]);
                final double satAltitude = satEphemAltitude[swathIndex];
                final double satHeadingAngle = degreesToRadians(satEphemHeading[swathIndex]);
                final double gcLatitude = EllipsoidalEarthModel.geodeticToGeocentric(gdLatitude);
                final double gcLongitude = gdLongitude;
                double cosLat = Math.cos(gcLatitude);
                double sinLat = Math.sin(gcLatitude);
                double cosLon = Math.cos(gcLongitude);
                double sinLon = Math.sin(gcLongitude);
                final double earthRadius = EllipsoidalEarthModel.earthRadiusKm(gcLatitude);
                scratchVec[0] = cosLat * cosLon;
                scratchVec[1] = cosLat * sinLon;
                scratchVec[2] = sinLat;
                subSatPoint = VectorMath.vectorScalarMultiplication(scratchVec, earthRadius);
                cosLat = Math.cos(gdLatitude);
                sinLat = Math.sin(gdLatitude);
                cosLon = Math.cos(gdLongitude);
                sinLon = Math.sin(gdLongitude);
                surfaceNormal[0] = cosLat * cosLon;
                surfaceNormal[1] = cosLat * sinLon;
                surfaceNormal[2] = sinLat;
                satPoint[0] = subSatPoint[0] + surfaceNormal[0] * satAltitude;
                satPoint[1] = subSatPoint[1] + surfaceNormal[1] * satAltitude;
                satPoint[2] = subSatPoint[2] + surfaceNormal[2] * satAltitude;
                north[0] = -subSatPoint[0];
                north[1] = -subSatPoint[1];
                north[2] = EllipsoidalEarthModel.earthRadiusKm(1.5707963267948966) - subSatPoint[2];
                final double projectMag = VectorMath.vectorDotProduct(north, surfaceNormal) / Math.pow(VectorMath.vectorMagnitude(surfaceNormal), 2.0);
                north[0] -= projectMag * surfaceNormal[0];
                north[1] -= projectMag * surfaceNormal[1];
                north[2] -= projectMag * surfaceNormal[2];
                north = VectorMath.unitVector(north);
                west = VectorMath.vectorCrossProduct(surfaceNormal, north);
                west = VectorMath.unitVector(west);
                final double cosHeading = Math.cos(satHeadingAngle);
                final double sinHeading = Math.sin(satHeadingAngle);
                satHeading[0] = sinHeading * west[0] + cosHeading * north[0];
                satHeading[1] = sinHeading * west[1] + cosHeading * north[1];
                satHeading[2] = sinHeading * west[2] + cosHeading * north[2];
                scanLine = VectorMath.vectorCrossProduct(surfaceNormal, satHeading);
                for (int sampleIndex = 0; sampleIndex < OLSSensorModel.numSamplesPerScan[dataType]; ++sampleIndex) {
                    final double scannerAngle = OLSSensorModel.scanAngleOLS(satID, dataType, sampleIndex, scannerOffset[swathIndex]);
                    final double scanPointMag = satAltitude * Math.tan(scannerAngle);
                    scanPoint[0] = subSatPoint[0] + scanPointMag * scanLine[0];
                    scanPoint[1] = subSatPoint[1] + scanPointMag * scanLine[1];
                    scanPoint[2] = subSatPoint[2] + scanPointMag * scanLine[2];
                    lineOfSightSlope[0] = scanPoint[0] - satPoint[0];
                    lineOfSightSlope[1] = scanPoint[1] - satPoint[1];
                    lineOfSightSlope[2] = scanPoint[2] - satPoint[2];
                    final double earthMajorAxis = EllipsoidalEarthModel.earthRadiusKm(0.0);
                    final double earthMinorAxis = EllipsoidalEarthModel.earthRadiusKm(1.5707963267948966);
                    final double earthMajorAxisSquared = Math.pow(earthMajorAxis, 2.0);
                    final double earthMinorAxisSquared = Math.pow(earthMinorAxis, 2.0);
                    final double quadraticEqnA = earthMinorAxisSquared * Math.pow(lineOfSightSlope[0], 2.0) + earthMinorAxisSquared * Math.pow(lineOfSightSlope[1], 2.0) + earthMajorAxisSquared * Math.pow(lineOfSightSlope[2], 2.0);
                    final double quadraticEqnB = 2.0 * (earthMinorAxisSquared * satPoint[0] * lineOfSightSlope[0] + earthMinorAxisSquared * satPoint[1] * lineOfSightSlope[1] + earthMajorAxisSquared * satPoint[2] * lineOfSightSlope[2]);
                    final double quadraticEqnC = earthMinorAxisSquared * Math.pow(satPoint[0], 2.0) + earthMinorAxisSquared * Math.pow(satPoint[1], 2.0) + earthMajorAxisSquared * Math.pow(satPoint[2], 2.0) - earthMajorAxisSquared * earthMinorAxisSquared;
                    final double scratch = Math.sqrt(Math.pow(quadraticEqnB, 2.0) - 4.0 * quadraticEqnA * quadraticEqnC);
                    final double quadraticSoln1 = (-quadraticEqnB + scratch) / (2.0 * quadraticEqnA);
                    final double quadraticSoln2 = (-quadraticEqnB - scratch) / (2.0 * quadraticEqnA);
                    final double lineParameter = (quadraticSoln1 < quadraticSoln2) ? quadraticSoln1 : quadraticSoln2;
                    geolocatedPoint[0] = lineOfSightSlope[0] * lineParameter + satPoint[0];
                    geolocatedPoint[1] = lineOfSightSlope[1] * lineParameter + satPoint[1];
                    geolocatedPoint[2] = lineOfSightSlope[2] * lineParameter + satPoint[2];
                    final double[] unitVecGeolocatedPoint = VectorMath.unitVector(geolocatedPoint);
                    final double gcLat = Math.asin(unitVecGeolocatedPoint[2]);
                    double gcLon = Math.acos(unitVecGeolocatedPoint[0] / Math.cos(gcLat));
                    gcLon = ((unitVecGeolocatedPoint[1] < 0.0) ? (-gcLon) : gcLon);
                    final int currentSampleIndex = swathIndex * OLSSensorModel.numSamplesPerScan[dataType] + sampleIndex;
                    latitude[currentSampleIndex] = (float)radiansToDegrees(gcLat);
                    longitude[currentSampleIndex] = (float)radiansToDegrees(gcLon);
                }
            }
        }
        
        static double degreesToRadians(final double angleInDegrees) {
            return angleInDegrees / 57.29577951308232;
        }
        
        static double radiansToDegrees(final double angleInRadians) {
            return angleInRadians * 57.29577951308232;
        }
    }
    
    static class OLSSensorModel
    {
        static final int NUM_DATA_TYPES = 4;
        static final int NUM_SAT_GROUPS = 2;
        static final int[][] RANGE_SAT_GROUPS;
        static final double peakScanAngle = 1.00967;
        static final double[] nominalTotalSamplePeriod;
        static final int[] numSamplesPerScan;
        static final double M = 2.66874;
        static final double[][] B;
        
        static double scanAngleOLS(final int satID, final int dataType, final int sampleNumber, final double scannerOffset) {
            if (sampleNumber > 1464) {
                throw new IllegalArgumentException("Sample number <" + sampleNumber + "> not within smooth sample range <0-1464> (fine data not currently supported).");
            }
            final int satGroup = (satID > 15) ? 1 : 0;
            return 1.00967 * Math.cos(sampleNumber / OLSSensorModel.nominalTotalSamplePeriod[dataType] * 2.66874 + OLSSensorModel.B[satGroup][dataType]) - scannerOffset;
        }
        
        static {
            RANGE_SAT_GROUPS = new int[][] { { 11, 16 }, { 15, 20 } };
            nominalTotalSamplePeriod = new double[] { 1464.436, 1464.436, 7322.179, 7322.179 };
            numSamplesPerScan = new int[] { 1465, 1465, 7322, 7322 };
            B = new double[][] { { 0.23686, 0.23591, 0.23665, 0.23665 }, { 0.23686, 0.23686, 0.23665, 0.23665 } };
        }
    }
    
    static class EllipsoidalEarthModel
    {
        static final double EARTH_MEAN_EQUATORIAL_RADIUS_KM = 6378.14;
        static final double EARTHS_ECCENTRICITY = 0.081819183;
        static final double E_ECC_SQUARED;
        
        static double earthRadius(final double gcLatitude) {
            return Math.sqrt(1.0 - EllipsoidalEarthModel.E_ECC_SQUARED) / Math.sqrt(1.0 - EllipsoidalEarthModel.E_ECC_SQUARED * Math.cos(gcLatitude));
        }
        
        static double earthRadiusKm(final double gcLatitude) {
            return earthRadius(gcLatitude) * 6378.14;
        }
        
        static double geodeticToGeocentric(final double geodeticLatitude) {
            return Math.atan((1.0 - EllipsoidalEarthModel.E_ECC_SQUARED) * Math.tan(geodeticLatitude));
        }
        
        static double geocentricToGeodetic(final double geocentricLatitude) {
            return Math.atan(Math.tan(geocentricLatitude) / (1.0 - EllipsoidalEarthModel.E_ECC_SQUARED));
        }
        
        static {
            E_ECC_SQUARED = Math.pow(0.081819183, 2.0);
        }
    }
    
    static class VectorMath
    {
        static double vectorMagnitude(final double[] vector) {
            if (vector.length != 3) {
                throw new IllegalArgumentException("Argument not a 3-D vector <dim=" + vector.length + ">.");
            }
            return Math.sqrt(Math.pow(vector[0], 2.0) + Math.pow(vector[1], 2.0) + Math.pow(vector[2], 2.0));
        }
        
        static double[] unitVector(final double[] vector) {
            if (vector.length != 3) {
                throw new IllegalArgumentException("Argument not a 3-D vector <dim=" + vector.length + ">.");
            }
            final double magnitude = vectorMagnitude(vector);
            final double[] resultingVector = { vector[0] / magnitude, vector[1] / magnitude, vector[2] / magnitude };
            return resultingVector;
        }
        
        static double[] vectorScalarMultiplication(final double[] vector, final double scalar) {
            if (vector.length != 3) {
                throw new IllegalArgumentException("Argument not a 3-D vector <dim=" + vector.length + ">.");
            }
            final double[] resultingVector = { scalar * vector[0], scalar * vector[1], scalar * vector[2] };
            return resultingVector;
        }
        
        static double vectorDotProduct(final double[] vectorA, final double[] vectorB) {
            if (vectorA.length != 3) {
                throw new IllegalArgumentException("First argument not a 3-D vector <dim=" + vectorA.length + ">.");
            }
            if (vectorB.length != 3) {
                throw new IllegalArgumentException("Second argument not a 3-D vector <dim=" + vectorB.length + ">.");
            }
            return vectorA[0] * vectorB[0] + vectorA[1] * vectorB[1] + vectorA[2] * vectorB[2];
        }
        
        static double[] vectorCrossProduct(final double[] vectorA, final double[] vectorB) {
            if (vectorA.length != 3) {
                throw new IllegalArgumentException("First argument not a 3-D vector <dim=" + vectorA.length + ">.");
            }
            if (vectorB.length != 3) {
                throw new IllegalArgumentException("Second argument not a 3-D vector <dim=" + vectorB.length + ">.");
            }
            final double[] resultingVector = { vectorA[1] * vectorB[2] - vectorA[2] * vectorB[1], vectorA[2] * vectorB[0] - vectorA[0] * vectorB[2], vectorA[0] * vectorB[1] - vectorA[1] * vectorB[0] };
            return resultingVector;
        }
    }
}
