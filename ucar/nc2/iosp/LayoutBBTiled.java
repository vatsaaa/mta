// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.iosp;

import java.nio.DoubleBuffer;
import java.nio.FloatBuffer;
import java.nio.LongBuffer;
import java.nio.IntBuffer;
import java.nio.ShortBuffer;
import java.nio.ByteBuffer;
import java.io.IOException;
import ucar.ma2.InvalidRangeException;
import ucar.ma2.Section;

public class LayoutBBTiled implements LayoutBB
{
    private Section want;
    private int[] chunkSize;
    private int elemSize;
    private DataChunkIterator chunkIterator;
    private IndexChunkerTiled index;
    private long totalNelems;
    private long totalNelemsDone;
    private boolean debug;
    private boolean debugIntersection;
    private Chunk next;
    
    public LayoutBBTiled(final DataChunkIterator chunkIterator, final int[] chunkSize, final int elemSize, final Section wantSection) throws InvalidRangeException, IOException {
        this.index = null;
        this.debug = false;
        this.debugIntersection = false;
        this.chunkIterator = chunkIterator;
        this.chunkSize = chunkSize;
        this.elemSize = elemSize;
        this.want = wantSection;
        if (this.debug) {
            System.out.println(" want section=" + this.want);
        }
        this.totalNelems = this.want.computeSize();
        this.totalNelemsDone = 0L;
    }
    
    public long getTotalNelems() {
        return this.totalNelems;
    }
    
    public int getElemSize() {
        return this.elemSize;
    }
    
    public boolean hasNext() {
        if (this.totalNelemsDone >= this.totalNelems) {
            return false;
        }
        Label_0255: {
            if (this.index != null) {
                if (this.index.hasNext()) {
                    break Label_0255;
                }
            }
            try {
                while (this.chunkIterator.hasNext()) {
                    DataChunk dataChunk;
                    try {
                        dataChunk = this.chunkIterator.next();
                    }
                    catch (IOException e) {
                        e.printStackTrace();
                        this.next = null;
                        return false;
                    }
                    final Section dataSection = new Section(dataChunk.getOffset(), this.chunkSize);
                    if (this.debugIntersection) {
                        System.out.println(" test intersecting: " + dataSection + " want: " + this.want);
                    }
                    if (dataSection.intersects(this.want)) {
                        if (this.debug) {
                            System.out.println(" found intersecting dataSection: " + dataSection + " intersect= " + dataSection.intersect(this.want));
                        }
                        this.index = new IndexChunkerTiled(dataSection, this.want);
                        this.next = new Chunk(dataChunk.getByteBuffer());
                        break Label_0255;
                    }
                }
                this.next = null;
                return false;
            }
            catch (InvalidRangeException e2) {
                throw new IllegalStateException(e2);
            }
            catch (IOException e3) {
                throw new IllegalStateException(e3);
            }
        }
        final IndexChunker.Chunk chunk = this.index.next();
        this.totalNelemsDone += chunk.getNelems();
        this.next.setDelegate(chunk);
        return true;
    }
    
    public LayoutBB.Chunk next() throws IOException {
        return this.next;
    }
    
    @Override
    public String toString() {
        final StringBuilder sbuff = new StringBuilder();
        sbuff.append("want=").append(this.want).append("; ");
        sbuff.append("chunkSize=[");
        for (int i = 0; i < this.chunkSize.length; ++i) {
            if (i > 0) {
                sbuff.append(",");
            }
            sbuff.append(this.chunkSize[i]);
        }
        sbuff.append("] totalNelems=").append(this.totalNelems);
        sbuff.append(" elemSize=").append(this.elemSize);
        return sbuff.toString();
    }
    
    private static class Chunk implements LayoutBB.Chunk
    {
        IndexChunker.Chunk delegate;
        private ByteBuffer bb;
        private ShortBuffer sb;
        private IntBuffer ib;
        private LongBuffer longb;
        private FloatBuffer fb;
        private DoubleBuffer db;
        
        public Chunk(final ByteBuffer bb) {
            this.bb = bb;
        }
        
        public void setDelegate(final IndexChunker.Chunk delegate) {
            this.delegate = delegate;
        }
        
        public int getSrcElem() {
            return (int)this.delegate.getSrcElem();
        }
        
        public int getNelems() {
            return this.delegate.getNelems();
        }
        
        public long getDestElem() {
            return this.delegate.getDestElem();
        }
        
        public ByteBuffer getByteBuffer() {
            return this.bb;
        }
        
        public ShortBuffer getShortBuffer() {
            if (this.sb == null) {
                this.sb = this.bb.asShortBuffer();
            }
            return this.sb;
        }
        
        public IntBuffer getIntBuffer() {
            if (this.ib == null) {
                this.ib = this.bb.asIntBuffer();
            }
            return this.ib;
        }
        
        public LongBuffer getLongBuffer() {
            if (this.longb == null) {
                this.longb = this.bb.asLongBuffer();
            }
            return this.longb;
        }
        
        public FloatBuffer getFloatBuffer() {
            if (this.fb == null) {
                this.fb = this.bb.asFloatBuffer();
            }
            return this.fb;
        }
        
        public DoubleBuffer getDoubleBuffer() {
            if (this.db == null) {
                this.db = this.bb.asDoubleBuffer();
            }
            return this.db;
        }
        
        public long getSrcPos() {
            throw new UnsupportedOperationException();
        }
    }
    
    public interface DataChunk
    {
        int[] getOffset();
        
        ByteBuffer getByteBuffer() throws IOException;
    }
    
    public interface DataChunkIterator
    {
        boolean hasNext();
        
        DataChunk next() throws IOException;
    }
}
