// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.iosp;

import ucar.ma2.InvalidRangeException;
import ucar.ma2.Section;

public class LayoutRegular implements Layout
{
    private IndexChunker chunker;
    private long startPos;
    private int elemSize;
    
    public LayoutRegular(final long startPos, final int elemSize, final int[] varShape, final Section wantSection) throws InvalidRangeException {
        assert startPos >= 0L;
        assert elemSize > 0;
        this.startPos = startPos;
        this.elemSize = elemSize;
        this.chunker = new IndexChunker(varShape, wantSection);
    }
    
    public long getTotalNelems() {
        return this.chunker.getTotalNelems();
    }
    
    public int getElemSize() {
        return this.elemSize;
    }
    
    public boolean hasNext() {
        return this.chunker.hasNext();
    }
    
    public Chunk next() {
        final IndexChunker.Chunk chunk = this.chunker.next();
        chunk.setSrcPos(this.startPos + chunk.getSrcElem() * this.elemSize);
        return chunk;
    }
}
