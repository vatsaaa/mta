// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.iosp;

import ucar.ma2.InvalidRangeException;
import ucar.ma2.Index;
import ucar.ma2.Section;

public class LayoutSegmented implements Layout
{
    private long total;
    private long done;
    private int elemSize;
    private long[] segPos;
    private long[] segMax;
    private long[] segMin;
    private IndexChunker chunker;
    private IndexChunker.Chunk chunkOuter;
    private IndexChunker.Chunk chunkInner;
    private boolean debugNext;
    private int needInner;
    private int doneInner;
    
    public LayoutSegmented(final long[] segPos, final int[] segSize, final int elemSize, final int[] srcShape, final Section wantSection) throws InvalidRangeException {
        this.chunkInner = new IndexChunker.Chunk(0L, 0, 0L);
        this.debugNext = false;
        this.needInner = 0;
        this.doneInner = 0;
        assert segPos.length == segSize.length;
        this.segPos = segPos;
        final int nsegs = segPos.length;
        this.segMin = new long[nsegs];
        this.segMax = new long[nsegs];
        long totalElems = 0L;
        for (int i = 0; i < nsegs; ++i) {
            assert segPos[i] >= 0L;
            assert segSize[i] > 0;
            assert segSize[i] % elemSize == 0;
            this.segMin[i] = totalElems;
            totalElems += segSize[i];
            this.segMax[i] = totalElems;
        }
        assert totalElems >= Index.computeSize(srcShape) * elemSize;
        this.chunker = new IndexChunker(srcShape, wantSection);
        this.total = this.chunker.getTotalNelems();
        this.done = 0L;
        this.elemSize = elemSize;
    }
    
    public long getTotalNelems() {
        return this.total;
    }
    
    public int getElemSize() {
        return this.elemSize;
    }
    
    public boolean hasNext() {
        return this.done < this.total;
    }
    
    private long getFilePos(final long elem) {
        int segno;
        for (segno = 0; elem >= this.segMax[segno]; ++segno) {}
        return this.segPos[segno] + elem - this.segMin[segno];
    }
    
    private int getMaxBytes(final long start) {
        int segno;
        for (segno = 0; start >= this.segMax[segno]; ++segno) {}
        return (int)(this.segMax[segno] - start);
    }
    
    public Chunk next() {
        Chunk result = null;
        if (this.needInner > 0) {
            result = this.nextInner(false, 0);
        }
        else {
            result = this.nextOuter();
            final int nbytes = this.getMaxBytes(this.chunkOuter.getSrcElem() * this.elemSize);
            if (nbytes < result.getNelems() * this.elemSize) {
                result = this.nextInner(true, nbytes);
            }
        }
        this.done += result.getNelems();
        this.doneInner += result.getNelems();
        this.needInner -= result.getNelems();
        if (this.debugNext) {
            System.out.println(" next chunk: " + result);
        }
        return result;
    }
    
    private Chunk nextInner(final boolean first, int nbytes) {
        if (first) {
            this.chunkInner.setNelems(nbytes / this.elemSize);
            this.chunkInner.setDestElem(this.chunkOuter.getDestElem());
            this.needInner = this.chunkOuter.getNelems();
            this.doneInner = 0;
        }
        else {
            this.chunkInner.incrDestElem(this.chunkInner.getNelems());
            nbytes = this.getMaxBytes((this.chunkOuter.getSrcElem() + this.doneInner) * this.elemSize);
            nbytes = Math.min(nbytes, this.needInner * this.elemSize);
            this.chunkInner.setNelems(nbytes / this.elemSize);
        }
        this.chunkInner.setSrcPos(this.getFilePos((this.chunkOuter.getSrcElem() + this.doneInner) * this.elemSize));
        return this.chunkInner;
    }
    
    public Chunk nextOuter() {
        this.chunkOuter = this.chunker.next();
        final long srcPos = this.getFilePos(this.chunkOuter.getSrcElem() * this.elemSize);
        this.chunkOuter.setSrcPos(srcPos);
        return this.chunkOuter;
    }
}
