// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.iosp;

import ucar.ma2.StructureDataIterator;
import ucar.nc2.Structure;
import ucar.nc2.ParsedSectionSpec;
import java.nio.channels.WritableByteChannel;
import ucar.ma2.InvalidRangeException;
import ucar.ma2.Array;
import ucar.ma2.Section;
import ucar.nc2.Variable;
import ucar.nc2.util.CancelTask;
import ucar.nc2.NetcdfFile;
import java.io.IOException;
import ucar.unidata.io.RandomAccessFile;

public interface IOServiceProvider
{
    boolean isValidFile(final RandomAccessFile p0) throws IOException;
    
    void open(final RandomAccessFile p0, final NetcdfFile p1, final CancelTask p2) throws IOException;
    
    Array readData(final Variable p0, final Section p1) throws IOException, InvalidRangeException;
    
    long readToByteChannel(final Variable p0, final Section p1, final WritableByteChannel p2) throws IOException, InvalidRangeException;
    
    Array readSection(final ParsedSectionSpec p0) throws IOException, InvalidRangeException;
    
    StructureDataIterator getStructureIterator(final Structure p0, final int p1) throws IOException;
    
    void close() throws IOException;
    
    boolean syncExtend() throws IOException;
    
    boolean sync() throws IOException;
    
    Object sendIospMessage(final Object p0);
    
    String toStringDebug(final Object p0);
    
    String getDetailInfo();
    
    String getFileTypeId();
    
    String getFileTypeVersion();
    
    String getFileTypeDescription();
}
