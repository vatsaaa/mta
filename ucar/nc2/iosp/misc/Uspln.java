// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.iosp.misc;

import java.util.Iterator;
import java.nio.ByteBuffer;
import ucar.ma2.StructureData;
import ucar.ma2.StructureDataIterator;
import ucar.ma2.InvalidRangeException;
import ucar.ma2.ArraySequence;
import ucar.ma2.Array;
import ucar.ma2.Section;
import java.text.ParseException;
import java.util.Date;
import java.util.StringTokenizer;
import java.util.ArrayList;
import ucar.nc2.Attribute;
import ucar.nc2.constants.AxisType;
import ucar.ma2.DataType;
import ucar.nc2.Structure;
import ucar.nc2.Sequence;
import ucar.ma2.ArrayStructureBB;
import ucar.nc2.Variable;
import ucar.nc2.Group;
import java.util.TimeZone;
import ucar.nc2.util.CancelTask;
import ucar.nc2.NetcdfFile;
import java.io.IOException;
import ucar.unidata.util.StringUtil;
import ucar.unidata.io.RandomAccessFile;
import java.text.SimpleDateFormat;
import ucar.ma2.StructureMembers;

public class Uspln extends AbstractLightningIOSP
{
    private static final String MAGIC = "LIGHTNING-..PLN1";
    private static final String MAGIC_OLD = "..PLN-LIGHTNING";
    private static final String MAGIC_EX = ".*PLN1EX.*";
    private static final String TIME_FORMAT = "yyyy-MM-dd'T'HH:mm:ss";
    private static final String TIME_FORMAT_EX = "yyyy-MM-dd'T'HH:mm:ss.SSS";
    private boolean isExtended;
    private long[] offsets;
    private double lat_min;
    private double lat_max;
    private double lon_min;
    private double lon_max;
    private double time_min;
    private double time_max;
    private StructureMembers sm;
    private SimpleDateFormat isoDateFormat;
    private int nelems;
    
    public Uspln() {
        this.isExtended = false;
        this.isoDateFormat = null;
        this.nelems = -1;
    }
    
    public boolean isValidFile(final RandomAccessFile raf) throws IOException {
        raf.seek(0L);
        final int n = "LIGHTNING-..PLN1".length();
        if (raf.length() < n) {
            return false;
        }
        final byte[] b = new byte[n];
        raf.read(b);
        final String got = new String(b);
        return StringUtil.regexpMatch(got, "LIGHTNING-..PLN1") || StringUtil.regexpMatch(got, "..PLN-LIGHTNING");
    }
    
    @Override
    public void open(final RandomAccessFile raf, final NetcdfFile ncfile, final CancelTask cancelTask) throws IOException {
        this.raf = raf;
        this.isExtended = this.checkFormat();
        (this.isoDateFormat = new SimpleDateFormat()).setTimeZone(TimeZone.getTimeZone("GMT"));
        this.isoDateFormat.applyPattern(this.isExtended ? "yyyy-MM-dd'T'HH:mm:ss.SSS" : "yyyy-MM-dd'T'HH:mm:ss");
        final Sequence seq = this.makeSequence(ncfile);
        ncfile.addVariable(null, seq);
        this.addLightningGlobalAttributes(ncfile);
        ncfile.finish();
        ArrayStructureBB.setOffsets(this.sm = seq.makeStructureMembers());
    }
    
    protected Sequence makeSequence(final NetcdfFile ncfile) {
        final Sequence seq = new Sequence(ncfile, null, null, "record");
        Variable v = this.makeLightningVariable(ncfile, null, seq, "time", DataType.DOUBLE, "", "time of stroke", null, "seconds since 1970-01-01 00:00:00", AxisType.Time);
        seq.addMemberVariable(v);
        v = this.makeLightningVariable(ncfile, null, seq, "lat", DataType.DOUBLE, "", "latitude", "latitude", "degrees_north", AxisType.Lat);
        seq.addMemberVariable(v);
        v = this.makeLightningVariable(ncfile, null, seq, "lon", DataType.DOUBLE, "", "longitude", "longitude", "degrees_east", AxisType.Lon);
        seq.addMemberVariable(v);
        v = this.makeLightningVariable(ncfile, null, seq, "sgnl", DataType.FLOAT, "", "signed peak amplitude (signal strength)", null, "kAmps", null);
        v.addAttribute(new Attribute("missing_value", new Double(999.0)));
        seq.addMemberVariable(v);
        if (this.isExtended) {
            v = this.makeLightningVariable(ncfile, null, seq, "majorAxis", DataType.FLOAT, "", "error ellipse semi-major axis", null, "km", null);
            seq.addMemberVariable(v);
            v = this.makeLightningVariable(ncfile, null, seq, "minorAxis", DataType.FLOAT, "", "error ellipse minor axis ", null, "km", null);
            seq.addMemberVariable(v);
            v = this.makeLightningVariable(ncfile, null, seq, "ellipseAngle", DataType.INT, "", "error ellipse axis angle of orientation ", null, "degrees", null);
            seq.addMemberVariable(v);
        }
        else {
            v = this.makeLightningVariable(ncfile, null, seq, "mult", DataType.INT, "", "multiplicity [#strokes per flash]", null, "", null);
            seq.addMemberVariable(v);
        }
        return seq;
    }
    
    @Override
    protected void addLightningGlobalAttributes(final NetcdfFile ncfile) {
        super.addLightningGlobalAttributes(ncfile);
        ncfile.addAttribute(null, new Attribute("title", "USPLN Lightning Data"));
        ncfile.addAttribute(null, new Attribute("file_format", "USPLN1 " + (this.isExtended ? "(extended)" : "(original)")));
    }
    
    private boolean checkFormat() throws IOException {
        this.raf.seek(0L);
        boolean extended = false;
        while (true) {
            final long offset = this.raf.getFilePointer();
            final String line = this.raf.readLine();
            if (line == null) {
                break;
            }
            if (StringUtil.regexpMatch(line, "LIGHTNING-..PLN1") || StringUtil.regexpMatch(line, "..PLN-LIGHTNING")) {
                extended = StringUtil.regexpMatch(line, ".*PLN1EX.*");
                break;
            }
        }
        return extended;
    }
    
    int readAllData(final RandomAccessFile raf) throws IOException, NumberFormatException, ParseException {
        final ArrayList offsetList = new ArrayList();
        final SimpleDateFormat isoDateTimeFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        isoDateTimeFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
        this.lat_min = 1000.0;
        this.lat_max = -1000.0;
        this.lon_min = 1000.0;
        this.lon_max = -1000.0;
        this.time_min = Double.POSITIVE_INFINITY;
        this.time_max = Double.NEGATIVE_INFINITY;
        raf.seek(0L);
        int count = 0;
        boolean knowExtended = false;
        while (true) {
            final long offset = raf.getFilePointer();
            final String line = raf.readLine();
            if (line == null) {
                break;
            }
            if (StringUtil.regexpMatch(line, "LIGHTNING-..PLN1") || StringUtil.regexpMatch(line, "..PLN-LIGHTNING")) {
                if (knowExtended) {
                    continue;
                }
                this.isExtended = StringUtil.regexpMatch(line, ".*PLN1EX.*");
                if (this.isExtended) {
                    isoDateTimeFormat.applyPattern("yyyy-MM-dd'T'HH:mm:ss.SSS");
                }
                knowExtended = true;
            }
            else {
                final StringTokenizer stoker = new StringTokenizer(line, ",\r\n");
                while (stoker.hasMoreTokens()) {
                    final Date date = isoDateTimeFormat.parse(stoker.nextToken());
                    final double lat = Double.parseDouble(stoker.nextToken());
                    final double lon = Double.parseDouble(stoker.nextToken());
                    final double amp = Double.parseDouble(stoker.nextToken());
                    int nstrokes = 1;
                    double axisMaj = Double.NaN;
                    double axisMin = Double.NaN;
                    int orient = 0;
                    if (this.isExtended) {
                        axisMaj = Double.parseDouble(stoker.nextToken());
                        axisMin = Double.parseDouble(stoker.nextToken());
                        orient = Integer.parseInt(stoker.nextToken());
                    }
                    else {
                        nstrokes = Integer.parseInt(stoker.nextToken());
                    }
                    final Stroke s = this.isExtended ? new Stroke(date, lat, lon, amp, axisMaj, axisMin, orient) : new Stroke(date, lat, lon, amp, nstrokes);
                    this.lat_min = Math.min(this.lat_min, s.lat);
                    this.lat_max = Math.max(this.lat_max, s.lat);
                    this.lon_min = Math.min(this.lon_min, s.lon);
                    this.lon_max = Math.max(this.lon_max, s.lon);
                    this.time_min = Math.min(this.time_min, s.secs);
                    this.time_max = Math.max(this.time_max, s.secs);
                }
                offsetList.add(new Long(offset));
                ++count;
            }
        }
        this.offsets = new long[count];
        for (int i = 0; i < offsetList.size(); ++i) {
            final Long off = offsetList.get(i);
            this.offsets[i] = off;
        }
        return count;
    }
    
    public Array readData(final Variable v2, final Section section) throws IOException, InvalidRangeException {
        return new ArraySequence(this.sm, this.getStructureIterator(null, 0), this.nelems);
    }
    
    @Override
    public StructureDataIterator getStructureIterator(final Structure s, final int bufferSize) throws IOException {
        return new UsplnSeqIter();
    }
    
    public String getFileTypeId() {
        return "USPLN";
    }
    
    public String getFileTypeDescription() {
        return "US Precision Lightning Network";
    }
    
    @Override
    public String getFileTypeVersion() {
        return "1";
    }
    
    private class UsplnSeqIter implements StructureDataIterator
    {
        private ArrayStructureBB asbb;
        int numFlashes;
        
        UsplnSeqIter() throws IOException {
            this.asbb = null;
            this.numFlashes = 0;
            Uspln.this.raf.seek(0L);
        }
        
        public StructureDataIterator reset() {
            this.numFlashes = 0;
            try {
                Uspln.this.raf.seek(0L);
            }
            catch (IOException e) {
                throw new RuntimeException(e);
            }
            return this;
        }
        
        public boolean hasNext() throws IOException {
            return this.readStroke();
        }
        
        public StructureData next() throws IOException {
            ++this.numFlashes;
            return this.asbb.getStructureData(0);
        }
        
        private boolean readStroke() throws IOException {
            final String line = Uspln.this.raf.readLine();
            if (line == null) {
                Uspln.this.nelems = this.numFlashes;
                return false;
            }
            if (StringUtil.regexpMatch(line, "LIGHTNING-..PLN1") || StringUtil.regexpMatch(line, "..PLN-LIGHTNING")) {
                return this.readStroke();
            }
            Stroke stroke = null;
            final StringTokenizer stoker = new StringTokenizer(line, ",\r\n");
            try {
                while (stoker.hasMoreTokens()) {
                    final Date date = Uspln.this.isoDateFormat.parse(stoker.nextToken());
                    final double lat = Double.parseDouble(stoker.nextToken());
                    final double lon = Double.parseDouble(stoker.nextToken());
                    final double amp = Double.parseDouble(stoker.nextToken());
                    int nstrokes = 1;
                    double axisMaj = Double.NaN;
                    double axisMin = Double.NaN;
                    int orient = 0;
                    if (Uspln.this.isExtended) {
                        axisMaj = Double.parseDouble(stoker.nextToken());
                        axisMin = Double.parseDouble(stoker.nextToken());
                        orient = Integer.parseInt(stoker.nextToken());
                    }
                    else {
                        nstrokes = Integer.parseInt(stoker.nextToken());
                    }
                    stroke = (Uspln.this.isExtended ? new Stroke(date, lat, lon, amp, axisMaj, axisMin, orient) : new Stroke(date, lat, lon, amp, nstrokes));
                }
            }
            catch (Exception e) {
                return false;
            }
            final byte[] data = new byte[Uspln.this.sm.getStructureSize()];
            final ByteBuffer bbdata = ByteBuffer.wrap(data);
            for (final String mbrName : Uspln.this.sm.getMemberNames()) {
                if (mbrName.equals("time")) {
                    bbdata.putDouble(stroke.secs);
                }
                else if (mbrName.equals("lat")) {
                    bbdata.putDouble(stroke.lat);
                }
                else if (mbrName.equals("lon")) {
                    bbdata.putDouble(stroke.lon);
                }
                else if (mbrName.equals("sgnl")) {
                    bbdata.putFloat((float)stroke.amp);
                }
                else if (mbrName.equals("mult")) {
                    bbdata.putInt(stroke.n);
                }
                else if (mbrName.equals("majorAxis")) {
                    bbdata.putFloat((float)stroke.axisMajor);
                }
                else if (mbrName.equals("minorAxis")) {
                    bbdata.putFloat((float)stroke.axisMinor);
                }
                else {
                    if (!mbrName.equals("ellipseAngle")) {
                        continue;
                    }
                    bbdata.putInt(stroke.axisOrient);
                }
            }
            this.asbb = new ArrayStructureBB(Uspln.this.sm, new int[] { 1 }, bbdata, 0);
            return true;
        }
        
        public void setBufferSize(final int bytes) {
        }
        
        public int getCurrentRecno() {
            return this.numFlashes - 1;
        }
    }
    
    private class Stroke
    {
        double secs;
        double lat;
        double lon;
        double amp;
        int n;
        double axisMajor;
        double axisMinor;
        int axisOrient;
        
        Stroke(final long offset, final SimpleDateFormat isoDateTimeFormat) throws IOException, ParseException {
            this.n = 1;
            Uspln.this.raf.seek(offset);
            final String line = Uspln.this.raf.readLine();
            if (line == null || StringUtil.regexpMatch(line, "LIGHTNING-..PLN1") || StringUtil.regexpMatch(line, "..PLN-LIGHTNING")) {
                throw new IllegalStateException();
            }
            final StringTokenizer stoker = new StringTokenizer(line, ",\r\n");
            final Date date = isoDateTimeFormat.parse(stoker.nextToken());
            this.makeSecs(date);
            this.lat = Double.parseDouble(stoker.nextToken());
            this.lon = Double.parseDouble(stoker.nextToken());
            this.amp = Double.parseDouble(stoker.nextToken());
            if (Uspln.this.isExtended) {
                this.n = 1;
                this.axisMajor = Double.parseDouble(stoker.nextToken());
                this.axisMinor = Double.parseDouble(stoker.nextToken());
                this.axisOrient = Integer.parseInt(stoker.nextToken());
            }
            else {
                this.n = Integer.parseInt(stoker.nextToken());
            }
        }
        
        Stroke(final Date date, final double lat, final double lon, final double amp, final int n) {
            this.n = 1;
            this.makeSecs(date);
            this.lat = lat;
            this.lon = lon;
            this.amp = amp;
            this.n = n;
        }
        
        Stroke(final Date date, final double lat, final double lon, final double amp, final double maj, final double min, final int orient) {
            this.n = 1;
            this.makeSecs(date);
            this.lat = lat;
            this.lon = lon;
            this.amp = amp;
            this.axisMajor = maj;
            this.axisMinor = min;
            this.axisOrient = orient;
        }
        
        void makeSecs(final Date date) {
            this.secs = (int)(date.getTime() / 1000L);
        }
        
        @Override
        public String toString() {
            return Uspln.this.isExtended ? (this.lat + " " + this.lon + " " + this.amp + " " + this.axisMajor + "/" + this.axisMinor + " " + this.axisOrient) : (this.lat + " " + this.lon + " " + this.amp + " " + this.n);
        }
    }
    
    private class IospData
    {
        int varno;
        
        IospData(final int varno) {
            this.varno = varno;
        }
    }
}
