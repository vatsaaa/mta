// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.iosp.misc;

import java.nio.ByteBuffer;
import ucar.ma2.StructureData;
import ucar.ma2.ArrayStructureBB;
import ucar.ma2.InvalidRangeException;
import ucar.ma2.StructureDataIterator;
import ucar.ma2.ArraySequence;
import ucar.ma2.Array;
import ucar.ma2.Section;
import ucar.nc2.Attribute;
import ucar.nc2.constants.AxisType;
import ucar.ma2.DataType;
import ucar.nc2.Variable;
import ucar.nc2.Group;
import ucar.nc2.Sequence;
import ucar.nc2.util.CancelTask;
import ucar.nc2.NetcdfFile;
import java.io.IOException;
import ucar.unidata.io.RandomAccessFile;
import ucar.ma2.StructureMembers;
import ucar.nc2.Structure;

public class Nldn extends AbstractLightningIOSP
{
    private static final String MAGIC = "NLDN";
    private Structure seq;
    private StructureMembers sm;
    private static final String TSEC = "tsec";
    private static final String NSEC = "nsec";
    private static final String CHISQR = "chisqr";
    private static final String FILL = "fill";
    private static final int recHeader = 84;
    private static final int recSize = 28;
    private int nelems;
    
    public Nldn() {
        this.nelems = -1;
    }
    
    public boolean isValidFile(final RandomAccessFile raf) throws IOException {
        raf.seek(0L);
        final int n = "NLDN".length();
        final byte[] b = new byte[n];
        raf.read(b);
        final String got = new String(b);
        return got.equals("NLDN");
    }
    
    public String getFileTypeId() {
        return "NLDN";
    }
    
    public String getFileTypeDescription() {
        return "National Lightning Detection Network";
    }
    
    @Override
    public void open(final RandomAccessFile raf, final NetcdfFile ncfile, final CancelTask cancelTask) throws IOException {
        this.raf = raf;
        ncfile.addVariable(null, this.seq = new Sequence(ncfile, null, null, "record"));
        Variable v = this.makeLightningVariable(ncfile, null, this.seq, "tsec", DataType.INT, "", "time of stroke", null, "seconds since 1970-01-01 00:00:00", AxisType.Time);
        this.seq.addMemberVariable(v);
        v = this.makeLightningVariable(ncfile, null, this.seq, "nsec", DataType.INT, "", "nanoseconds since tsec", null, "1.0e-9 s", null);
        this.seq.addMemberVariable(v);
        v = this.makeLightningVariable(ncfile, null, this.seq, "lat", DataType.INT, "", "latitude", "latitude", "degrees_north", AxisType.Lat);
        v.addAttribute(new Attribute("scale_factor", new Float(0.001)));
        this.seq.addMemberVariable(v);
        v = this.makeLightningVariable(ncfile, null, this.seq, "lon", DataType.INT, "", "longitude", "longitude", "degrees_east", AxisType.Lon);
        v.addAttribute(new Attribute("scale_factor", new Float(0.001)));
        this.seq.addMemberVariable(v);
        v = this.makeLightningVariable(ncfile, null, this.seq, "sgnl", DataType.SHORT, "", "signal strength/polarity [150 NLDN measures ~= 30 kAmps]", null, "", null);
        v.addAttribute(new Attribute("scale_factor", new Float(0.1)));
        this.seq.addMemberVariable(v);
        v = this.makeLightningVariable(ncfile, null, this.seq, "mult", DataType.BYTE, "", "multiplicity [#strokes per flash]", null, "", null);
        this.seq.addMemberVariable(v);
        v = new Variable(ncfile, null, this.seq, "fill");
        v.setDataType(DataType.BYTE);
        v.setDimensions("");
        this.seq.addMemberVariable(v);
        v = this.makeLightningVariable(ncfile, null, this.seq, "majorAxis", DataType.BYTE, "", "error ellipse semi-major axis", null, "", null);
        this.seq.addMemberVariable(v);
        v = this.makeLightningVariable(ncfile, null, this.seq, "eccent", DataType.BYTE, "", "error ellipse eccentricity ", null, "", null);
        this.seq.addMemberVariable(v);
        v = this.makeLightningVariable(ncfile, null, this.seq, "ellipseAngle", DataType.BYTE, "", "error ellipse axis angle of orientation ", null, "degrees", null);
        this.seq.addMemberVariable(v);
        v = this.makeLightningVariable(ncfile, null, this.seq, "chisqr", DataType.BYTE, "", "chi-squared", null, "", null);
        this.seq.addMemberVariable(v);
        this.addLightningGlobalAttributes(ncfile);
        ncfile.finish();
        this.sm = this.seq.makeStructureMembers();
        this.sm.findMember("tsec").setDataParam(0);
        this.sm.findMember("nsec").setDataParam(4);
        this.sm.findMember("lat").setDataParam(8);
        this.sm.findMember("lon").setDataParam(12);
        this.sm.findMember("sgnl").setDataParam(18);
        this.sm.findMember("mult").setDataParam(22);
        this.sm.findMember("fill").setDataParam(23);
        this.sm.findMember("majorAxis").setDataParam(24);
        this.sm.findMember("eccent").setDataParam(25);
        this.sm.findMember("ellipseAngle").setDataParam(26);
        this.sm.findMember("chisqr").setDataParam(27);
        this.sm.setStructureSize(28);
    }
    
    @Override
    protected void addLightningGlobalAttributes(final NetcdfFile ncfile) {
        super.addLightningGlobalAttributes(ncfile);
        ncfile.addAttribute(null, new Attribute("title", "NLDN Lightning Data"));
        ncfile.addAttribute(null, new Attribute("Conventions", "NLDN-CDM"));
    }
    
    public Array readData(final Variable v2, final Section section) throws IOException, InvalidRangeException {
        return new ArraySequence(this.sm, new SeqIter(), this.nelems);
    }
    
    @Override
    public StructureDataIterator getStructureIterator(final Structure s, final int bufferSize) throws IOException {
        return new SeqIter();
    }
    
    private class SeqIter implements StructureDataIterator
    {
        private int done;
        private int alreadyRead;
        private int nextIndex;
        private ArrayStructureBB asbb;
        private long totalBytes;
        private long bytesRead;
        
        SeqIter() throws IOException {
            this.done = 0;
            this.alreadyRead = 0;
            this.nextIndex = 0;
            this.asbb = null;
            this.totalBytes = (int)Nldn.this.raf.length();
            Nldn.this.raf.seek(0L);
        }
        
        public StructureDataIterator reset() {
            this.done = 0;
            this.alreadyRead = 0;
            this.bytesRead = 0L;
            this.nextIndex = 0;
            try {
                Nldn.this.raf.seek(0L);
            }
            catch (IOException e) {
                throw new RuntimeException(e);
            }
            return this;
        }
        
        public boolean hasNext() throws IOException {
            return this.done < this.alreadyRead || this.readHeader();
        }
        
        public StructureData next() throws IOException {
            ++this.done;
            return this.asbb.getStructureData(this.nextIndex++);
        }
        
        private boolean readHeader() throws IOException {
            if (this.bytesRead + 84L > this.totalBytes) {
                Nldn.this.nelems = this.done;
                return false;
            }
            final byte[] b = new byte[84];
            Nldn.this.raf.readFully(b);
            this.bytesRead += 84L;
            final ByteBuffer bb = ByteBuffer.wrap(b);
            final int count = bb.getInt(8);
            if (count == 0) {
                return this.readHeader();
            }
            if (this.bytesRead + count * 28 > this.totalBytes) {
                return false;
            }
            final byte[] data = new byte[count * 28];
            Nldn.this.raf.read(data);
            this.bytesRead += count * 28;
            this.alreadyRead += count;
            this.nextIndex = 0;
            final ByteBuffer bbdata = ByteBuffer.wrap(data);
            this.asbb = new ArrayStructureBB(Nldn.this.sm, new int[] { count }, bbdata, 0);
            return true;
        }
        
        public void setBufferSize(final int bytes) {
        }
        
        public int getCurrentRecno() {
            return this.done - 1;
        }
    }
}
