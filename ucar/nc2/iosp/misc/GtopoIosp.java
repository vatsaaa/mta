// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.iosp.misc;

import ucar.ma2.InvalidRangeException;
import ucar.nc2.iosp.Layout;
import ucar.nc2.iosp.LayoutRegular;
import ucar.ma2.Section;
import java.util.StringTokenizer;
import ucar.nc2.util.IO;
import ucar.ma2.Array;
import ucar.nc2.Attribute;
import ucar.ma2.DataType;
import ucar.nc2.Structure;
import ucar.nc2.Variable;
import ucar.nc2.Group;
import ucar.nc2.Dimension;
import ucar.nc2.util.CancelTask;
import ucar.nc2.NetcdfFile;
import java.io.IOException;
import java.io.File;
import ucar.unidata.io.RandomAccessFile;
import ucar.nc2.iosp.AbstractIOServiceProvider;

public class GtopoIosp extends AbstractIOServiceProvider
{
    private int nlats;
    private int nlons;
    private float incr;
    private float startx;
    private float starty;
    private RandomAccessFile raf;
    
    public GtopoIosp() {
        this.nlats = 6000;
        this.nlons = 4800;
        this.incr = 0.008333334f;
    }
    
    public boolean isValidFile(final RandomAccessFile raf) throws IOException {
        final String location = raf.getLocation();
        if (!location.endsWith(".DEM")) {
            return false;
        }
        final int pos = location.lastIndexOf(".");
        final String stub = location.substring(0, pos);
        final File hdrFile = new File(stub + ".HDR");
        return hdrFile.exists();
    }
    
    public String getFileTypeId() {
        return "GTOPO";
    }
    
    public String getFileTypeDescription() {
        return "USGS GTOPO digital elevation model";
    }
    
    @Override
    public void open(final RandomAccessFile raf, final NetcdfFile ncfile, final CancelTask cancelTask) throws IOException {
        this.raf = raf;
        this.readHDR();
        ncfile.addDimension(null, new Dimension("lat", this.nlats));
        ncfile.addDimension(null, new Dimension("lon", this.nlons));
        final Variable elev = new Variable(ncfile, null, null, "elevation");
        elev.setDataType(DataType.SHORT);
        elev.setDimensions("lat lon");
        elev.addAttribute(new Attribute("units", "m"));
        elev.addAttribute(new Attribute("units_desc", "meters above sea level"));
        elev.addAttribute(new Attribute("long_name", "digital elevation in meters above mean sea level"));
        elev.addAttribute(new Attribute("missing_value", -9999));
        ncfile.addVariable(null, elev);
        final Variable lat = new Variable(ncfile, null, null, "lat");
        lat.setDataType(DataType.FLOAT);
        lat.setDimensions("lat");
        lat.addAttribute(new Attribute("units", "degrees_north"));
        ncfile.addVariable(null, lat);
        final Array data = Array.makeArray(DataType.FLOAT, this.nlats, this.starty, -this.incr);
        lat.setCachedData(data, false);
        final Variable lon = new Variable(ncfile, null, null, "lon");
        lon.setDataType(DataType.FLOAT);
        lon.setDimensions("lon");
        lon.addAttribute(new Attribute("units", "degrees_east"));
        ncfile.addVariable(null, lon);
        final Array lonData = Array.makeArray(DataType.FLOAT, this.nlons, this.startx, this.incr);
        lon.setCachedData(lonData, false);
        ncfile.addAttribute(null, new Attribute("Conventions", "CF-1.0"));
        ncfile.addAttribute(null, new Attribute("History", "Direct read by Netcdf-Java CDM library"));
        ncfile.addAttribute(null, new Attribute("Source", "http://eros.usgs.gov/products/elevation/gtopo30.html"));
        ncfile.finish();
    }
    
    private void readHDR() throws IOException {
        final String location = this.raf.getLocation();
        final int pos = location.lastIndexOf(".");
        final String HDRname = location.substring(0, pos) + ".HDR";
        final String HDRcontents = IO.readFile(HDRname);
        final StringTokenizer stoke = new StringTokenizer(HDRcontents);
        while (stoke.hasMoreTokens()) {
            final String key = stoke.nextToken();
            if (key.equals("ULXMAP")) {
                this.startx = Float.parseFloat(stoke.nextToken());
            }
            else if (key.equals("ULYMAP")) {
                this.starty = Float.parseFloat(stoke.nextToken());
            }
            else {
                stoke.nextToken();
            }
        }
    }
    
    public Array readData(final Variable v2, final Section wantSection) throws IOException, InvalidRangeException {
        this.raf.order(0);
        final int size = (int)wantSection.computeSize();
        final short[] arr = new short[size];
        final LayoutRegular indexer = new LayoutRegular(0L, v2.getElementSize(), v2.getShape(), wantSection);
        while (indexer.hasNext()) {
            final Layout.Chunk chunk = indexer.next();
            this.raf.seek(chunk.getSrcPos());
            this.raf.readShort(arr, (int)chunk.getDestElem(), chunk.getNelems());
        }
        return Array.factory(v2.getDataType().getPrimitiveClassType(), wantSection.getShape(), arr);
    }
    
    @Override
    public void close() throws IOException {
        this.raf.close();
    }
}
