// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.iosp.misc;

import ucar.nc2.Sequence;
import ucar.ma2.StructureData;
import ucar.ma2.StructureDataIterator;
import ucar.ma2.ArraySequence;
import org.slf4j.LoggerFactory;
import ucar.nc2.NCdumpW;
import java.util.TimeZone;
import java.io.EOFException;
import java.util.Map;
import java.util.Collections;
import java.util.HashMap;
import ucar.ma2.Range;
import java.nio.ByteBuffer;
import ucar.ma2.StructureMembers;
import ucar.ma2.ArrayStructureBB;
import java.util.Iterator;
import ucar.nc2.constants.AxisType;
import ucar.ma2.DataType;
import ucar.ma2.Array;
import ucar.ma2.Section;
import ucar.nc2.Structure;
import ucar.ma2.InvalidRangeException;
import ucar.nc2.Variable;
import ucar.nc2.Dimension;
import ucar.nc2.constants.FeatureType;
import ucar.nc2.Group;
import ucar.nc2.Attribute;
import ucar.nc2.util.CancelTask;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import ucar.nc2.units.DateFormatter;
import java.util.Calendar;
import java.util.List;
import ucar.nc2.NetcdfFile;
import ucar.unidata.io.RandomAccessFile;
import org.slf4j.Logger;
import ucar.nc2.iosp.AbstractIOServiceProvider;

public class NmcObsLegacy extends AbstractIOServiceProvider
{
    private static Logger log;
    private RandomAccessFile raf;
    private NetcdfFile ncfile;
    private List<Station> stations;
    private List<Report> reports;
    private Calendar cal;
    private DateFormatter dateFormatter;
    private Date refDate;
    private String refString;
    private List<StructureCode> catStructures;
    private boolean showObs;
    private boolean showSkip;
    private boolean showOverflow;
    private boolean showData;
    private boolean showHeader;
    private boolean showTime;
    private boolean readData;
    private boolean summarizeData;
    private boolean showTimes;
    private boolean checkType;
    private boolean checkSort;
    private boolean checkPositions;
    private Report firstReport;
    private String[] catNames;
    private static float[] mandPressureLevel;
    
    public NmcObsLegacy() {
        this.stations = new ArrayList<Station>();
        this.reports = new ArrayList<Report>();
        this.cal = null;
        this.dateFormatter = new DateFormatter();
        this.catStructures = new ArrayList<StructureCode>(10);
        this.showObs = false;
        this.showSkip = false;
        this.showOverflow = false;
        this.showData = false;
        this.showHeader = false;
        this.showTime = false;
        this.readData = false;
        this.summarizeData = false;
        this.showTimes = false;
        this.checkType = false;
        this.checkSort = false;
        this.checkPositions = false;
        this.firstReport = null;
        this.catNames = new String[] { "", "Category 01: mandatory constant-pressure data", "Category 02: temperature/dewpoint at variable pressure-levels ", "Category 03: wind at variable pressure-levels ", "Category 04: wind at variable height-levels ", "Category 05: tropopause data", "", "Category 07: cloud cover", "Category 08: additional data", "", "", "Category 51: surface Data", "Category 52: ship surface Data" };
    }
    
    public boolean isValidFile(final RandomAccessFile raf) throws IOException {
        raf.seek(0L);
        if (raf.length() < 60L) {
            return false;
        }
        final byte[] h = raf.readBytes(60);
        for (int i = 32; i < 56; ++i) {
            if (h[i] != 88) {
                return false;
            }
        }
        try {
            final short hour = Short.parseShort(new String(h, 0, 2));
            final short minute = Short.parseShort(new String(h, 2, 2));
            final short year = Short.parseShort(new String(h, 4, 2));
            final short month = Short.parseShort(new String(h, 6, 2));
            final short day = Short.parseShort(new String(h, 8, 2));
            if (hour < 0 || hour > 24) {
                return false;
            }
            if (minute < 0 || minute > 60) {
                return false;
            }
            if (year < 0 || year > 100) {
                return false;
            }
            if (month < 0 || month > 12) {
                return false;
            }
            if (day < 0 || day > 31) {
                return false;
            }
        }
        catch (Exception e) {
            return false;
        }
        return true;
    }
    
    public String getFileTypeId() {
        return "NMCon29";
    }
    
    public String getFileTypeDescription() {
        return "NMC Office Note 29";
    }
    
    @Override
    public void open(final RandomAccessFile raf, final NetcdfFile ncfile, final CancelTask cancelTask) throws IOException {
        this.raf = raf;
        this.ncfile = ncfile;
        this.init();
        ncfile.addAttribute(null, new Attribute("history", "Direct read of NMC ON29 by CDM"));
        ncfile.addAttribute(null, new Attribute("Conventions", "Unidata"));
        ncfile.addAttribute(null, new Attribute("cdm_data_type", FeatureType.STATION_PROFILE.toString()));
        try {
            ncfile.addDimension(null, new Dimension("station", this.stations.size()));
            final Structure station = this.makeStationStructure();
            ncfile.addVariable(null, station);
            ncfile.addDimension(null, new Dimension("report", this.reports.size()));
            final Structure reportIndexVar = this.makeReportIndexStructure();
            ncfile.addVariable(null, reportIndexVar);
            final Structure reportVar = this.makeReportStructure();
            ncfile.addVariable(null, reportVar);
        }
        catch (InvalidRangeException e) {
            NmcObsLegacy.log.error("open ON29 File", e);
            throw new IllegalStateException(e.getMessage());
        }
    }
    
    @Override
    public void close() throws IOException {
        this.raf.close();
    }
    
    public Array readData(final Variable v, final Section section) throws IOException, InvalidRangeException {
        if (v.getName().equals("station")) {
            return this.readStation(v, section);
        }
        if (v.getName().equals("report")) {
            return this.readReport(v, section);
        }
        if (v.getName().equals("reportIndex")) {
            return this.readReportIndex(v, section);
        }
        throw new IllegalArgumentException("Unknown variable name= " + v.getName());
    }
    
    private Structure makeStationStructure() throws IOException, InvalidRangeException {
        final Structure station = new Structure(this.ncfile, null, null, "station");
        station.setDimensions("station");
        station.addAttribute(new Attribute("long_name", "unique stations within this file"));
        int pos = 0;
        Variable v = station.addMemberVariable(new Variable(this.ncfile, null, station, "stationName", DataType.CHAR, ""));
        v.setDimensionsAnonymous(new int[] { 6 });
        v.addAttribute(new Attribute("long_name", "name of station"));
        v.addAttribute(new Attribute("standard_name", "station_name"));
        v.setSPobject(new Vinfo(pos));
        pos += 6;
        v = station.addMemberVariable(new Variable(this.ncfile, null, station, "lat", DataType.FLOAT, ""));
        v.addAttribute(new Attribute("units", "degrees_north"));
        v.addAttribute(new Attribute("long_name", "geographic latitude"));
        v.addAttribute(new Attribute("accuracy", "degree/100"));
        v.addAttribute(new Attribute("_CoordinateAxisType", AxisType.Lat.toString()));
        v.setSPobject(new Vinfo(pos));
        pos += 4;
        v = station.addMemberVariable(new Variable(this.ncfile, null, station, "lon", DataType.FLOAT, ""));
        v.addAttribute(new Attribute("units", "degrees_east"));
        v.addAttribute(new Attribute("long_name", "geographic longitude"));
        v.addAttribute(new Attribute("accuracy", "degree/100"));
        v.addAttribute(new Attribute("_CoordinateAxisType", AxisType.Lon.toString()));
        v.setSPobject(new Vinfo(pos));
        pos += 4;
        v = station.addMemberVariable(new Variable(this.ncfile, null, station, "elev", DataType.FLOAT, ""));
        v.addAttribute(new Attribute("units", "meters"));
        v.addAttribute(new Attribute("long_name", "station elevation above MSL"));
        v.addAttribute(new Attribute("_CoordinateAxisType", AxisType.Height.toString()));
        v.setSPobject(new Vinfo(pos));
        pos += 4;
        v = station.addMemberVariable(new Variable(this.ncfile, null, station, "nrecords", DataType.INT, ""));
        v.addAttribute(new Attribute("long_name", "number of records"));
        v.addAttribute(new Attribute("standard_name", "npts"));
        v.setSPobject(new Vinfo(pos));
        pos += 4;
        return station;
    }
    
    private Structure makeReportIndexStructure() throws InvalidRangeException, IOException {
        final Structure reportIndex = new Structure(this.ncfile, null, null, "reportIndex");
        reportIndex.setDimensions("report");
        reportIndex.addAttribute(new Attribute("long_name", "index on report - in memory"));
        int pos = 0;
        Variable v = reportIndex.addMemberVariable(new Variable(this.ncfile, null, reportIndex, "stationName", DataType.CHAR, ""));
        v.setDimensionsAnonymous(new int[] { 6 });
        v.addAttribute(new Attribute("long_name", "name of station"));
        v.addAttribute(new Attribute("standard_name", "station_name"));
        v.setSPobject(new Vinfo(pos));
        pos += 6;
        v = reportIndex.addMemberVariable(new Variable(this.ncfile, null, reportIndex, "time", DataType.INT, ""));
        v.addAttribute(new Attribute("units", "secs since 1970-01-01 00:00"));
        v.addAttribute(new Attribute("long_name", "observation time"));
        v.setSPobject(new Vinfo(pos));
        pos += 4;
        return reportIndex;
    }
    
    private Structure makeReportStructure() throws InvalidRangeException, IOException {
        final Structure report = new Structure(this.ncfile, null, null, "report");
        report.setDimensions("report");
        report.addAttribute(new Attribute("long_name", "ON29 observation report"));
        int pos = 0;
        Variable v = report.addMemberVariable(new Variable(this.ncfile, null, report, "time", DataType.INT, ""));
        v.addAttribute(new Attribute("units", "secs since 1970-01-01 00:00"));
        v.addAttribute(new Attribute("long_name", "observation time"));
        v.addAttribute(new Attribute("_CoordinateAxisType", AxisType.Time.toString()));
        v.setSPobject(new Vinfo(pos));
        pos += 4;
        v = report.addMemberVariable(new Variable(this.ncfile, null, report, "timeISO", DataType.CHAR, ""));
        v.setDimensionsAnonymous(new int[] { 20 });
        v.addAttribute(new Attribute("long_name", "ISO formatted date/time"));
        v.setSPobject(new Vinfo(pos));
        pos += 20;
        v = report.addMemberVariable(new Variable(this.ncfile, null, report, "reportType", DataType.SHORT, ""));
        v.addAttribute(new Attribute("long_name", "report type from Table R.1"));
        v.setSPobject(new Vinfo(pos));
        pos += 2;
        v = report.addMemberVariable(new Variable(this.ncfile, null, report, "instType", DataType.SHORT, ""));
        v.addAttribute(new Attribute("long_name", "instrument type from Table R.2"));
        v.setSPobject(new Vinfo(pos));
        pos += 2;
        v = report.addMemberVariable(new Variable(this.ncfile, null, report, "reserved", DataType.CHAR, ""));
        v.setDimensionsAnonymous(new int[] { 7 });
        v.addAttribute(new Attribute("long_name", "reserved characters"));
        v.setSPobject(new Vinfo(pos));
        pos += 7;
        final List<Record> records = this.firstReport.readData();
        pos = this.makeInnerSequence(report, records, 1, pos);
        pos = this.makeInnerSequence(report, records, 2, pos);
        pos = this.makeInnerSequence(report, records, 3, pos);
        pos = this.makeInnerSequence(report, records, 4, pos);
        pos = this.makeInnerSequence(report, records, 5, pos);
        pos = this.makeInnerSequence(report, records, 7, pos);
        pos = this.makeInnerSequence(report, records, 8, pos);
        pos = this.makeInnerSequence(report, records, 51, pos);
        pos = this.makeInnerSequence(report, records, 52, pos);
        report.calcElementSize();
        return report;
    }
    
    private int makeInnerSequence(final Structure reportVar, final List<Record> records, final int code, int obs_pos) throws InvalidRangeException {
        for (final Record record : records) {
            if (record.code == code) {
                final Entry first = record.entries[0];
                final Structure s = first.makeStructure(reportVar);
                s.setSPobject(new Vinfo(obs_pos));
                obs_pos += 4;
                reportVar.addMemberVariable(s);
                this.catStructures.add(new StructureCode(s, code));
                break;
            }
        }
        return obs_pos;
    }
    
    private Array readStation(final Variable v, final Section section) throws IOException, InvalidRangeException {
        final Structure s = (Structure)v;
        final StructureMembers members = s.makeStructureMembers();
        for (final Variable v2 : s.getVariables()) {
            final Vinfo vinfo = (Vinfo)v2.getSPobject();
            final StructureMembers.Member m = members.findMember(v2.getShortName());
            if (vinfo != null) {
                m.setDataParam(vinfo.offset);
            }
        }
        final int size = (int)section.computeSize();
        final ArrayStructureBB abb = new ArrayStructureBB(members, new int[] { size });
        final ByteBuffer bb = abb.getByteBuffer();
        final Range r = section.getRange(0);
        for (int i = r.first(); i <= r.last(); i += r.stride()) {
            final Station station = this.stations.get(i);
            bb.put(station.r.stationId.getBytes());
            bb.putFloat(station.r.lat);
            bb.putFloat(station.r.lon);
            bb.putFloat(station.r.elevMeters);
            bb.putInt(station.nreports);
        }
        return abb;
    }
    
    public Array readReportIndex(final Variable v, final Section section) throws IOException, InvalidRangeException {
        final Structure s = (Structure)v;
        final StructureMembers members = s.makeStructureMembers();
        for (final Variable v2 : s.getVariables()) {
            final Vinfo vinfo = (Vinfo)v2.getSPobject();
            final StructureMembers.Member m = members.findMember(v2.getShortName());
            m.setDataParam(vinfo.offset);
        }
        final int size = (int)section.computeSize();
        final ArrayStructureBB abb = new ArrayStructureBB(members, new int[] { size });
        final ByteBuffer bb = abb.getByteBuffer();
        final Range r = section.getRange(0);
        for (int i = r.first(); i <= r.last(); i += r.stride()) {
            final Report report = this.reports.get(i);
            report.loadIndexData(bb);
        }
        return abb;
    }
    
    public Array readReport(final Variable v, final Section section) throws IOException, InvalidRangeException {
        final Structure s = (Structure)v;
        final StructureMembers members = s.makeStructureMembers();
        for (final Variable v2 : s.getVariables()) {
            final Vinfo vinfo = (Vinfo)v2.getSPobject();
            final StructureMembers.Member m = members.findMember(v2.getShortName());
            m.setDataParam(vinfo.offset);
        }
        final int size = (int)section.computeSize();
        final ArrayStructureBB abb = new ArrayStructureBB(members, new int[] { size });
        final ByteBuffer bb = abb.getByteBuffer();
        final Range r = section.getRange(0);
        for (int i = r.first(); i <= r.last(); i += r.stride()) {
            final Report report = this.reports.get(i);
            report.loadStructureData(abb, bb);
        }
        return abb;
    }
    
    private void init() throws IOException {
        int badPos = 0;
        int badType = 0;
        short firstType = -1;
        this.raf.seek(0L);
        this.readHeader(this.raf);
        final Map<String, Station> map = new HashMap<String, Station>();
        while (true) {
            final Report report = new Report();
            if (!report.readId(this.raf)) {
                break;
            }
            if (this.firstReport == null) {
                this.firstReport = report;
                firstType = this.firstReport.reportType;
            }
            if (this.checkType && report.reportType != firstType) {
                System.out.println(report.stationId + " type: " + report.reportType + " not " + firstType);
                ++badType;
            }
            Station stn = map.get(report.stationId);
            if (stn == null) {
                stn = new Station(report);
                map.put(report.stationId, stn);
                this.stations.add(stn);
            }
            else {
                final Station station = stn;
                ++station.nreports;
                if (this.checkPositions) {
                    final Report first = this.reports.get(0);
                    if (first.lat != report.lat) {
                        System.out.println(report.stationId + " lat: " + first.lat + " !=" + report.lat);
                        ++badPos;
                    }
                    if (first.lon != report.lon) {
                        System.out.println(report.stationId + " lon: " + first.lon + " !=" + report.lon);
                    }
                    if (first.elevMeters != report.elevMeters) {
                        System.out.println(report.stationId + " elev: " + first.elevMeters + " !=" + report.elevMeters);
                    }
                }
            }
            this.reports.add(report);
        }
        Collections.sort(this.stations);
        if (this.checkPositions) {
            System.out.println("\nnon matching lats= " + badPos);
        }
        if (this.checkType) {
            System.out.println("\nnon matching reportTypes= " + badType);
        }
    }
    
    private int readIntWithOverflow(final byte[] b, final int offset, final int len) {
        final String s = new String(b, offset, len);
        try {
            return Integer.parseInt(s);
        }
        catch (Exception e) {
            if (this.showOverflow) {
                System.out.println("OVERFLOW=" + s);
            }
            return 0;
        }
    }
    
    private boolean endRecord(final RandomAccessFile raf) throws IOException {
        if (this.showSkip) {
            System.out.print(" endRecord start at " + raf.getFilePointer());
        }
        int skipped;
        String endRecord;
        for (skipped = 0, endRecord = new String(raf.readBytes(10)); endRecord.equals("END RECORD"); endRecord = new String(raf.readBytes(10)), ++skipped) {}
        if (this.showSkip) {
            System.out.println(" last 10 chars= " + endRecord + " skipped= " + skipped);
        }
        return true;
    }
    
    private boolean endFile(final RandomAccessFile raf) throws IOException {
        if (this.showSkip) {
            System.out.println(" endFile start at " + raf.getFilePointer());
        }
        for (String endRecord = new String(raf.readBytes(10)); endRecord.equals("ENDOF FILE"); endRecord = new String(raf.readBytes(10))) {}
        try {
            while (raf.read() != 88) {}
            while (raf.read() == 88) {}
            raf.skipBytes(-1);
            this.readHeader(raf);
            return true;
        }
        catch (EOFException e) {
            return false;
        }
    }
    
    private void readHeader(final RandomAccessFile raf) throws IOException {
        final byte[] h = raf.readBytes(60);
        final short hour = Short.parseShort(new String(h, 0, 2));
        final short minute = Short.parseShort(new String(h, 2, 2));
        final short year = Short.parseShort(new String(h, 4, 2));
        final short month = Short.parseShort(new String(h, 6, 2));
        final short day = Short.parseShort(new String(h, 8, 2));
        final int fullyear = (year > 30) ? (1900 + year) : (2000 + year);
        if (this.cal == null) {
            (this.cal = Calendar.getInstance()).setTimeZone(TimeZone.getTimeZone("UTC"));
        }
        this.cal.clear();
        this.cal.set(fullyear, month - 1, day, hour, minute);
        this.refDate = this.cal.getTime();
        this.refString = new String(h, 0, 10);
        if (this.showHeader) {
            System.out.println("\nhead=" + new String(h) + " date= " + this.dateFormatter.toDateTimeString(this.refDate));
        }
        int count = 0;
        int b;
        while ((b = raf.read()) == 88) {
            ++count;
        }
        final char c = (char)b;
        if (this.showSkip) {
            System.out.println(" b=" + b + " c=" + c + " at " + raf.getFilePointer() + " skipped= " + count);
        }
        raf.skipBytes(-1);
    }
    
    public static void main(final String[] args) throws IOException, InvalidRangeException {
        final String filename = "C:/data/cadis/tempting";
        final NmcObsLegacy iosp = new NmcObsLegacy();
        final RandomAccessFile raf = new RandomAccessFile(filename, "r");
        final NetcdfFile ncfile = new MyNetcdfFile(iosp);
        ncfile.setLocation(filename);
        iosp.open(raf, ncfile, null);
        System.out.println("\n" + ncfile);
        Variable v = ncfile.findVariable("station");
        Array data = v.read(new Section().appendRange(0, 1));
        System.out.println(NCdumpW.printArray(data, "station", null));
        v = ncfile.findVariable("report");
        data = v.read(new Section().appendRange(0, 0));
        System.out.println(NCdumpW.printArray(data, "report", null));
        v = ncfile.findVariable("reportIndex");
        data = v.read();
        System.out.println(NCdumpW.printArray(data, "reportIndex", null));
        iosp.close();
    }
    
    static {
        NmcObsLegacy.log = LoggerFactory.getLogger(NmcObsLegacy.class);
        NmcObsLegacy.mandPressureLevel = new float[] { 1000.0f, 850.0f, 700.0f, 500.0f, 400.0f, 300.0f, 250.0f, 200.0f, 150.0f, 100.0f, 70.0f, 50.0f, 30.0f, 20.0f, 10.0f, 7.0f, 5.0f, 3.0f, 2.0f, 1.0f };
    }
    
    private class Vinfo
    {
        int offset;
        
        Vinfo(final int offset) {
            this.offset = offset;
        }
    }
    
    private class StructureCode
    {
        Structure s;
        int code;
        
        StructureCode(final Structure s, final int code) {
            this.s = s;
            this.code = code;
        }
    }
    
    private class Station implements Comparable<Station>
    {
        String name;
        Report r;
        int nreports;
        
        Station(final Report r) {
            this.name = r.stationId;
            this.r = r;
            this.nreports = 1;
        }
        
        public int compareTo(final Station o) {
            return this.name.compareTo(o.name);
        }
    }
    
    private class Report
    {
        float lat;
        float lon;
        float elevMeters;
        String stationId;
        byte[] reserved;
        short reportType;
        short instType;
        short obsTime;
        int reportLen;
        long filePos;
        Date date;
        String rString;
        
        private Report() {
            this.reserved = new byte[7];
        }
        
        boolean readId(final RandomAccessFile raf) throws IOException {
            this.filePos = raf.getFilePointer();
            byte[] reportId = raf.readBytes(40);
            String latS = new String(reportId, 0, 5);
            if (latS.equals("END R")) {
                raf.skipBytes(-40);
                NmcObsLegacy.this.endRecord(raf);
                this.filePos = raf.getFilePointer();
                reportId = raf.readBytes(40);
                latS = new String(reportId, 0, 5);
            }
            if (latS.equals("ENDOF")) {
                raf.skipBytes(-40);
                if (!NmcObsLegacy.this.endFile(raf)) {
                    return false;
                }
                this.filePos = raf.getFilePointer();
                reportId = raf.readBytes(40);
                latS = new String(reportId, 0, 5);
            }
            try {
                this.lat = (float)(0.01 * Float.parseFloat(latS));
                this.lon = (float)(360.0 - 0.01 * Float.parseFloat(new String(reportId, 5, 5)));
                this.stationId = new String(reportId, 10, 6);
                this.obsTime = Short.parseShort(new String(reportId, 16, 4));
                System.arraycopy(reportId, 20, this.reserved, 0, 7);
                this.reportType = Short.parseShort(new String(reportId, 27, 3));
                this.elevMeters = Float.parseFloat(new String(reportId, 30, 5));
                this.instType = Short.parseShort(new String(reportId, 35, 2));
                this.reportLen = 10 * Integer.parseInt(new String(reportId, 37, 3));
                NmcObsLegacy.this.cal.setTime(NmcObsLegacy.this.refDate);
                final int hour = NmcObsLegacy.this.cal.get(11);
                if (this.obsTime / 100 > hour + 4) {
                    NmcObsLegacy.this.cal.add(5, -1);
                }
                NmcObsLegacy.this.cal.set(11, this.obsTime / 100);
                NmcObsLegacy.this.cal.set(12, 6 * (this.obsTime % 100));
                this.date = NmcObsLegacy.this.cal.getTime();
                this.rString = NmcObsLegacy.this.refString;
                if (NmcObsLegacy.this.showObs) {
                    System.out.println(this);
                }
                else if (NmcObsLegacy.this.showTime) {
                    System.out.print("  time=" + this.obsTime + " date= " + NmcObsLegacy.this.dateFormatter.toDateTimeString(this.date));
                }
                raf.skipBytes(this.reportLen - 40);
                return this.reportLen < 30000;
            }
            catch (Exception e) {
                System.out.println("BAD reportId=" + new String(reportId));
                System.out.println("ReportId start at " + this.filePos);
                e.printStackTrace();
                System.exit(1);
                return false;
            }
        }
        
        @Override
        public String toString() {
            return "Report  stationId=" + this.stationId + " lat=" + this.lat + " lon=" + this.lon + " obsTime=" + this.obsTime + " date= " + NmcObsLegacy.this.dateFormatter.toDateTimeStringISO(this.date) + " reportType=" + this.reportType + " elevMeters=" + this.elevMeters + " instType=" + this.instType + " reserved=" + new String(this.reserved) + " start=" + this.filePos + " reportLen=" + this.reportLen;
        }
        
        List<Record> readData() throws IOException {
            final List<Record> records = new ArrayList<Record>();
            NmcObsLegacy.this.raf.seek(this.filePos + 40L);
            final byte[] b = NmcObsLegacy.this.raf.readBytes(this.reportLen - 40);
            if (NmcObsLegacy.this.showData) {
                System.out.println("\n" + new String(b));
            }
            if (NmcObsLegacy.this.showData) {
                System.out.println(this);
            }
            int offset = 0;
            Record record;
            do {
                record = new Record();
                offset = record.read(b, offset);
                records.add(record);
            } while (record.next < this.reportLen / 10);
            return records;
        }
        
        void show(final RandomAccessFile raf) throws IOException {
            raf.seek(this.filePos);
            final byte[] b = raf.readBytes(40);
            System.out.println(new String(b));
        }
        
        void loadIndexData(final ByteBuffer bb) throws IOException {
            bb.put(this.stationId.getBytes());
            bb.putInt((int)(this.date.getTime() / 1000L));
        }
        
        void loadStructureData(final ArrayStructureBB abb, final ByteBuffer bb) throws IOException {
            bb.putInt((int)(this.date.getTime() / 1000L));
            bb.put(NmcObsLegacy.this.dateFormatter.toDateTimeStringISO(this.date).getBytes());
            bb.putShort(this.reportType);
            bb.putShort(this.instType);
            bb.put(this.reserved);
            final List<Record> records = this.readData();
            for (final StructureCode sc : NmcObsLegacy.this.catStructures) {
                this.loadInnerSequence(abb, bb, records, sc.s, sc.code);
            }
        }
        
        private void loadInnerSequence(final ArrayStructureBB abb, final ByteBuffer bb, final List<Record> records, final Structure useStructure, final int code) {
            for (final Record record : records) {
                if (record.code == code) {
                    final CatIterator iter = new CatIterator(record.entries, useStructure);
                    final ArraySequence seq = new ArraySequence(iter.members, iter, record.entries.length);
                    final int index = abb.addObjectToHeap(seq);
                    bb.putInt(index);
                    return;
                }
            }
            final CatIterator iter2 = new CatIterator(new Entry[0], useStructure);
            final ArraySequence seq2 = new ArraySequence(iter2.members, iter2, -1);
            final int index2 = abb.addObjectToHeap(seq2);
            bb.putInt(index2);
        }
        
        private class CatIterator implements StructureDataIterator
        {
            Entry[] entries;
            int count;
            StructureMembers members;
            
            CatIterator(final Entry[] entries, final Structure useStructure) {
                this.count = 0;
                this.entries = entries;
                this.members = useStructure.makeStructureMembers();
                for (final Variable v2 : useStructure.getVariables()) {
                    final Vinfo vinfo = (Vinfo)v2.getSPobject();
                    final StructureMembers.Member m = this.members.findMember(v2.getShortName());
                    m.setDataParam(vinfo.offset);
                }
            }
            
            public boolean hasNext() throws IOException {
                return this.count < this.entries.length;
            }
            
            public StructureData next() throws IOException {
                final Entry entry = this.entries[this.count++];
                final ArrayStructureBB abb = new ArrayStructureBB(this.members, new int[] { 1 });
                final ByteBuffer bb = abb.getByteBuffer();
                bb.position(0);
                entry.loadStructureData(bb);
                return abb.getStructureData(0);
            }
            
            public void setBufferSize(final int bytes) {
            }
            
            public StructureDataIterator reset() {
                this.count = 0;
                return this;
            }
            
            public int getCurrentRecno() {
                return this.count - 1;
            }
        }
    }
    
    private class Record
    {
        int code;
        int next;
        int nlevels;
        int nbytes;
        Entry[] entries;
        
        int read(final byte[] b, int offset) throws IOException {
            this.code = Integer.parseInt(new String(b, offset, 2));
            this.next = Integer.parseInt(new String(b, offset + 2, 3));
            this.nlevels = Integer.parseInt(new String(b, offset + 5, 2));
            this.nbytes = NmcObsLegacy.this.readIntWithOverflow(b, offset + 7, 3);
            if (NmcObsLegacy.this.showData) {
                System.out.println("\n" + this);
            }
            offset += 10;
            if (this.code == 1) {
                if (NmcObsLegacy.this.showData) {
                    System.out.println(NmcObsLegacy.this.catNames[1] + ":");
                }
                this.entries = new Cat01[this.nlevels];
                for (int i = 0; i < this.nlevels; ++i) {
                    this.entries[i] = new Cat01(b, offset, i);
                    if (NmcObsLegacy.this.showData) {
                        System.out.println(" " + i + ": " + this.entries[i]);
                    }
                    offset += 22;
                }
            }
            else if (this.code == 2) {
                if (NmcObsLegacy.this.showData) {
                    System.out.println(NmcObsLegacy.this.catNames[2] + ":");
                }
                this.entries = new Cat02[this.nlevels];
                for (int i = 0; i < this.nlevels; ++i) {
                    this.entries[i] = new Cat02(b, offset);
                    if (NmcObsLegacy.this.showData) {
                        System.out.println(" " + i + ": " + this.entries[i]);
                    }
                    offset += 15;
                }
            }
            else if (this.code == 3) {
                if (NmcObsLegacy.this.showData) {
                    System.out.println(NmcObsLegacy.this.catNames[3] + ":");
                }
                this.entries = new Cat03[this.nlevels];
                for (int i = 0; i < this.nlevels; ++i) {
                    this.entries[i] = new Cat03(b, offset);
                    if (NmcObsLegacy.this.showData) {
                        System.out.println(" " + i + ": " + this.entries[i]);
                    }
                    offset += 13;
                }
            }
            else if (this.code == 4) {
                if (NmcObsLegacy.this.showData) {
                    System.out.println(NmcObsLegacy.this.catNames[4] + ":");
                }
                this.entries = new Cat04[this.nlevels];
                for (int i = 0; i < this.nlevels; ++i) {
                    this.entries[i] = new Cat04(b, offset);
                    if (NmcObsLegacy.this.showData) {
                        System.out.println(" " + i + ": " + this.entries[i]);
                    }
                    offset += 13;
                }
            }
            else if (this.code == 5) {
                if (NmcObsLegacy.this.showData) {
                    System.out.println(NmcObsLegacy.this.catNames[5] + ":");
                }
                this.entries = new Cat05[this.nlevels];
                for (int i = 0; i < this.nlevels; ++i) {
                    this.entries[i] = new Cat05(b, offset);
                    if (NmcObsLegacy.this.showData) {
                        System.out.println(" " + i + ": " + this.entries[i]);
                    }
                    offset += 22;
                }
            }
            else if (this.code == 7) {
                if (NmcObsLegacy.this.showData) {
                    System.out.println(NmcObsLegacy.this.catNames[7] + ":");
                }
                this.entries = new Cat07[this.nlevels];
                for (int i = 0; i < this.nlevels; ++i) {
                    this.entries[i] = new Cat07(b, offset);
                    if (NmcObsLegacy.this.showData) {
                        System.out.println(" " + i + ": " + this.entries[i]);
                    }
                    offset += 10;
                }
            }
            else if (this.code == 8) {
                if (NmcObsLegacy.this.showData) {
                    System.out.println(NmcObsLegacy.this.catNames[8] + ":");
                }
                this.entries = new Cat08[this.nlevels];
                for (int i = 0; i < this.nlevels; ++i) {
                    this.entries[i] = new Cat08(b, offset);
                    if (NmcObsLegacy.this.showData) {
                        System.out.println(" " + i + ": " + this.entries[i]);
                    }
                    offset += 10;
                }
            }
            else if (this.code == 51) {
                if (NmcObsLegacy.this.showData) {
                    System.out.println(NmcObsLegacy.this.catNames[10] + ":");
                }
                this.entries = new Cat51[this.nlevels];
                for (int i = 0; i < this.nlevels; ++i) {
                    this.entries[i] = new Cat51(b, offset);
                    if (NmcObsLegacy.this.showData) {
                        System.out.println(" " + i + ": " + this.entries[i]);
                    }
                    offset += 60;
                }
            }
            else {
                if (this.code != 52) {
                    throw new UnsupportedOperationException("code= " + this.code);
                }
                if (NmcObsLegacy.this.showData) {
                    System.out.println(NmcObsLegacy.this.catNames[10] + ":");
                }
                this.entries = new Cat52[this.nlevels];
                for (int i = 0; i < this.nlevels; ++i) {
                    this.entries[i] = new Cat52(b, offset);
                    if (NmcObsLegacy.this.showData) {
                        System.out.println(" " + i + ": " + this.entries[i]);
                    }
                    offset += 40;
                }
            }
            final int skip = offset % 10;
            if (skip > 0) {
                offset += 10 - skip;
            }
            return offset;
        }
        
        @Override
        public String toString() {
            return "Category/Group  code=" + this.code + " next= " + this.next + " nlevels=" + this.nlevels + " nbytes=" + this.nbytes;
        }
    }
    
    private abstract class Entry
    {
        abstract Structure makeStructure(final Structure p0) throws InvalidRangeException;
        
        abstract void loadStructureData(final ByteBuffer p0);
    }
    
    private class Cat01 extends Entry
    {
        short windDir;
        short windSpeed;
        float geopot;
        float press;
        float temp;
        float dewp;
        byte[] quality;
        
        Cat01(final byte[] b, final int offset, final int level) throws IOException {
            this.quality = new byte[4];
            this.press = NmcObsLegacy.mandPressureLevel[level];
            this.geopot = Float.parseFloat(new String(b, offset, 5));
            this.temp = 0.1f * Float.parseFloat(new String(b, offset + 5, 4));
            this.dewp = 0.1f * Float.parseFloat(new String(b, offset + 9, 3));
            this.windDir = Short.parseShort(new String(b, offset + 12, 3));
            this.windSpeed = Short.parseShort(new String(b, offset + 15, 3));
            System.arraycopy(b, offset + 18, this.quality, 0, 4);
        }
        
        @Override
        public String toString() {
            return "Cat01: press= " + this.press + " geopot=" + this.geopot + " temp= " + this.temp + " dewp=" + this.dewp + " windDir=" + this.windDir + " windSpeed=" + this.windSpeed + " qs=" + new String(this.quality);
        }
        
        @Override
        Structure makeStructure(final Structure parent) throws InvalidRangeException {
            final Sequence seq = new Sequence(NmcObsLegacy.this.ncfile, null, parent, "mandatoryLevels");
            seq.addAttribute(new Attribute("long_name", NmcObsLegacy.this.catNames[1]));
            int pos = 0;
            Variable v = seq.addMemberVariable(new Variable(NmcObsLegacy.this.ncfile, null, parent, "pressure", DataType.FLOAT, ""));
            v.addAttribute(new Attribute("units", "mbars"));
            v.addAttribute(new Attribute("long_name", "pressure level"));
            v.addAttribute(new Attribute("_CoordinateAxisType", AxisType.Pressure.toString()));
            v.setSPobject(new Vinfo(pos));
            pos += 4;
            v = seq.addMemberVariable(new Variable(NmcObsLegacy.this.ncfile, null, parent, "geopotential", DataType.FLOAT, ""));
            v.addAttribute(new Attribute("units", "meter"));
            v.addAttribute(new Attribute("long_name", "geopotential"));
            v.addAttribute(new Attribute("missing_value", 99999.0f));
            v.setSPobject(new Vinfo(pos));
            pos += 4;
            v = seq.addMemberVariable(new Variable(NmcObsLegacy.this.ncfile, null, parent, "temperature", DataType.FLOAT, ""));
            v.addAttribute(new Attribute("units", "celsius"));
            v.addAttribute(new Attribute("long_name", "temperature"));
            v.addAttribute(new Attribute("accuracy", "celsius/10"));
            v.addAttribute(new Attribute("missing_value", 999.9f));
            v.setSPobject(new Vinfo(pos));
            pos += 4;
            v = seq.addMemberVariable(new Variable(NmcObsLegacy.this.ncfile, null, parent, "dewpoint", DataType.FLOAT, ""));
            v.addAttribute(new Attribute("units", "celsius"));
            v.addAttribute(new Attribute("long_name", "dewpoint depression"));
            v.addAttribute(new Attribute("accuracy", "celsius/10"));
            v.addAttribute(new Attribute("missing_value", 99.9f));
            v.setSPobject(new Vinfo(pos));
            pos += 4;
            v = seq.addMemberVariable(new Variable(NmcObsLegacy.this.ncfile, null, parent, "windDir", DataType.SHORT, ""));
            v.addAttribute(new Attribute("units", "degrees"));
            v.addAttribute(new Attribute("long_name", "wind direction"));
            v.addAttribute(new Attribute("missing_value", 999));
            v.setSPobject(new Vinfo(pos));
            pos += 2;
            v = seq.addMemberVariable(new Variable(NmcObsLegacy.this.ncfile, null, parent, "windSpeed", DataType.SHORT, ""));
            v.addAttribute(new Attribute("units", "knots"));
            v.addAttribute(new Attribute("long_name", "wind speed"));
            v.addAttribute(new Attribute("missing_value", 999));
            v.setSPobject(new Vinfo(pos));
            pos += 2;
            v = seq.addMemberVariable(new Variable(NmcObsLegacy.this.ncfile, null, parent, "qualityFlags", DataType.CHAR, ""));
            v.setDimensionsAnonymous(new int[] { 4 });
            v.addAttribute(new Attribute("long_name", "quality marks: 0=geopot, 1=temp, 2=dewpoint, 3=wind"));
            v.setSPobject(new Vinfo(pos));
            pos += 4;
            return seq;
        }
        
        @Override
        void loadStructureData(final ByteBuffer bb) {
            bb.putFloat(this.press);
            bb.putFloat(this.geopot);
            bb.putFloat(this.temp);
            bb.putFloat(this.dewp);
            bb.putShort(this.windDir);
            bb.putShort(this.windSpeed);
            bb.put(this.quality);
        }
    }
    
    private class Cat02 extends Entry
    {
        float press;
        float temp;
        float dewp;
        byte[] quality;
        String qs;
        
        Cat02(final byte[] b, final int offset) throws IOException {
            this.quality = new byte[3];
            this.press = 0.1f * Float.parseFloat(new String(b, offset, 5));
            this.temp = 0.1f * Float.parseFloat(new String(b, offset + 5, 4));
            this.dewp = 0.1f * Float.parseFloat(new String(b, offset + 9, 3));
            System.arraycopy(b, offset + 12, this.quality, 0, 3);
            this.qs = new String(this.quality);
        }
        
        @Override
        public String toString() {
            return "Cat02: press=" + this.press + " temp= " + this.temp + " dewp=" + this.dewp + " qs=" + this.qs;
        }
        
        @Override
        Structure makeStructure(final Structure parent) throws InvalidRangeException {
            final Sequence seq = new Sequence(NmcObsLegacy.this.ncfile, null, parent, "tempPressureLevels");
            seq.addAttribute(new Attribute("long_name", NmcObsLegacy.this.catNames[2]));
            int pos = 0;
            Variable v = seq.addMemberVariable(new Variable(NmcObsLegacy.this.ncfile, null, parent, "pressure", DataType.FLOAT, ""));
            v.addAttribute(new Attribute("units", "mbars"));
            v.addAttribute(new Attribute("long_name", "pressure level"));
            v.addAttribute(new Attribute("accuracy", "mbar/10"));
            v.addAttribute(new Attribute("_CoordinateAxisType", AxisType.Pressure.toString()));
            v.setSPobject(new Vinfo(pos));
            pos += 4;
            v = seq.addMemberVariable(new Variable(NmcObsLegacy.this.ncfile, null, parent, "temperature", DataType.FLOAT, ""));
            v.addAttribute(new Attribute("units", "celsius"));
            v.addAttribute(new Attribute("long_name", "temperature"));
            v.addAttribute(new Attribute("accuracy", "celsius/10"));
            v.addAttribute(new Attribute("missing_value", 999.9f));
            v.setSPobject(new Vinfo(pos));
            pos += 4;
            v = seq.addMemberVariable(new Variable(NmcObsLegacy.this.ncfile, null, parent, "dewpoint", DataType.FLOAT, ""));
            v.addAttribute(new Attribute("units", "celsius"));
            v.addAttribute(new Attribute("long_name", "dewpoint depression"));
            v.addAttribute(new Attribute("accuracy", "celsius/10"));
            v.addAttribute(new Attribute("missing_value", 99.9f));
            v.setSPobject(new Vinfo(pos));
            pos += 4;
            v = seq.addMemberVariable(new Variable(NmcObsLegacy.this.ncfile, null, parent, "qualityFlags", DataType.CHAR, ""));
            v.setDimensionsAnonymous(new int[] { 3 });
            v.addAttribute(new Attribute("long_name", "quality marks: 0=pressure, 1=temp, 2=dewpoint"));
            v.setSPobject(new Vinfo(pos));
            pos += 3;
            return seq;
        }
        
        @Override
        void loadStructureData(final ByteBuffer bb) {
            bb.putFloat(this.press);
            bb.putFloat(this.temp);
            bb.putFloat(this.dewp);
            bb.put(this.quality);
        }
    }
    
    private class Cat03 extends Entry
    {
        float press;
        short windDir;
        short windSpeed;
        byte[] quality;
        String qs;
        
        Cat03(final byte[] b, final int offset) throws IOException {
            this.press = 0.1f * Float.parseFloat(new String(b, offset, 5));
            this.windDir = Short.parseShort(new String(b, offset + 5, 3));
            this.windSpeed = Short.parseShort(new String(b, offset + 8, 3));
            System.arraycopy(b, offset + 11, this.quality = new byte[2], 0, 2);
            this.qs = new String(this.quality);
        }
        
        @Override
        public String toString() {
            return "Cat03: press=" + this.press + " windDir=" + this.windDir + " windSpeed=" + this.windSpeed + " qs=" + this.qs;
        }
        
        @Override
        Structure makeStructure(final Structure parent) throws InvalidRangeException {
            final Sequence seq = new Sequence(NmcObsLegacy.this.ncfile, null, parent, "windPressureLevels");
            seq.addAttribute(new Attribute("long_name", NmcObsLegacy.this.catNames[3]));
            int pos = 0;
            Variable v = seq.addMemberVariable(new Variable(NmcObsLegacy.this.ncfile, null, parent, "pressure", DataType.FLOAT, ""));
            v.addAttribute(new Attribute("units", "mbars"));
            v.addAttribute(new Attribute("long_name", "pressure level"));
            v.addAttribute(new Attribute("accuracy", "mbar/10"));
            v.addAttribute(new Attribute("_CoordinateAxisType", AxisType.Pressure.toString()));
            v.setSPobject(new Vinfo(pos));
            pos += 4;
            v = seq.addMemberVariable(new Variable(NmcObsLegacy.this.ncfile, null, parent, "windDir", DataType.SHORT, ""));
            v.addAttribute(new Attribute("units", "degrees"));
            v.addAttribute(new Attribute("long_name", "wind direction"));
            v.addAttribute(new Attribute("missing_value", 999));
            v.setSPobject(new Vinfo(pos));
            pos += 2;
            v = seq.addMemberVariable(new Variable(NmcObsLegacy.this.ncfile, null, parent, "windSpeed", DataType.SHORT, ""));
            v.addAttribute(new Attribute("units", "knots"));
            v.addAttribute(new Attribute("long_name", "wind speed"));
            v.addAttribute(new Attribute("missing_value", 999));
            v.setSPobject(new Vinfo(pos));
            pos += 2;
            v = seq.addMemberVariable(new Variable(NmcObsLegacy.this.ncfile, null, parent, "qualityFlags", DataType.CHAR, ""));
            v.setDimensionsAnonymous(new int[] { 2 });
            v.addAttribute(new Attribute("long_name", "quality marks: 0=pressure, 1=wind"));
            v.setSPobject(new Vinfo(pos));
            pos += 2;
            return seq;
        }
        
        @Override
        void loadStructureData(final ByteBuffer bb) {
            bb.putFloat(this.press);
            bb.putShort(this.windDir);
            bb.putShort(this.windSpeed);
            bb.put(this.quality);
        }
    }
    
    private class Cat04 extends Entry
    {
        float geopot;
        short windDir;
        short windSpeed;
        byte[] quality;
        String qs;
        
        Cat04(final byte[] b, final int offset) throws IOException {
            this.geopot = Float.parseFloat(new String(b, offset, 5));
            this.windDir = Short.parseShort(new String(b, offset + 5, 3));
            this.windSpeed = Short.parseShort(new String(b, offset + 8, 3));
            System.arraycopy(b, offset + 11, this.quality = new byte[2], 0, 2);
            this.qs = new String(this.quality);
        }
        
        @Override
        public String toString() {
            return "Cat04: geopot=" + this.geopot + " windDir=" + this.windDir + " windSpeed=" + this.windSpeed + " qs=" + this.qs;
        }
        
        @Override
        Structure makeStructure(final Structure parent) throws InvalidRangeException {
            final Sequence seq = new Sequence(NmcObsLegacy.this.ncfile, null, parent, "windHeightLevels");
            seq.addAttribute(new Attribute("long_name", NmcObsLegacy.this.catNames[4]));
            int pos = 0;
            Variable v = seq.addMemberVariable(new Variable(NmcObsLegacy.this.ncfile, null, parent, "geopotential", DataType.FLOAT, ""));
            v.addAttribute(new Attribute("units", "meter"));
            v.addAttribute(new Attribute("long_name", "geopotential"));
            v.addAttribute(new Attribute("missing_value", 99999.0f));
            v.setSPobject(new Vinfo(pos));
            pos += 4;
            v = seq.addMemberVariable(new Variable(NmcObsLegacy.this.ncfile, null, parent, "windDir", DataType.SHORT, ""));
            v.addAttribute(new Attribute("units", "degrees"));
            v.addAttribute(new Attribute("long_name", "wind direction"));
            v.addAttribute(new Attribute("missing_value", 999));
            v.setSPobject(new Vinfo(pos));
            pos += 2;
            v = seq.addMemberVariable(new Variable(NmcObsLegacy.this.ncfile, null, parent, "windSpeed", DataType.SHORT, ""));
            v.addAttribute(new Attribute("units", "knots"));
            v.addAttribute(new Attribute("long_name", "wind speed"));
            v.addAttribute(new Attribute("missing_value", 999));
            v.setSPobject(new Vinfo(pos));
            pos += 2;
            v = seq.addMemberVariable(new Variable(NmcObsLegacy.this.ncfile, null, parent, "qualityFlags", DataType.CHAR, ""));
            v.setDimensionsAnonymous(new int[] { 2 });
            v.addAttribute(new Attribute("long_name", "quality marks: 0=geopot, 1=wind"));
            v.setSPobject(new Vinfo(pos));
            pos += 2;
            return seq;
        }
        
        @Override
        void loadStructureData(final ByteBuffer bb) {
            bb.putFloat(this.geopot);
            bb.putShort(this.windDir);
            bb.putShort(this.windSpeed);
            bb.put(this.quality);
        }
    }
    
    private class Cat05 extends Entry
    {
        float press;
        float temp;
        float dewp;
        short windDir;
        short windSpeed;
        byte[] quality;
        String qs;
        
        Cat05(final byte[] b, final int offset) throws IOException {
            this.press = 0.1f * Float.parseFloat(new String(b, offset, 5));
            this.temp = 0.1f * Float.parseFloat(new String(b, offset + 5, 4));
            this.dewp = 0.1f * Float.parseFloat(new String(b, offset + 9, 3));
            this.windDir = Short.parseShort(new String(b, offset + 12, 3));
            this.windSpeed = Short.parseShort(new String(b, offset + 15, 3));
            System.arraycopy(b, offset + 18, this.quality = new byte[4], 0, 4);
            this.qs = new String(this.quality);
        }
        
        @Override
        public String toString() {
            return "Cat05: press= " + this.press + " temp= " + this.temp + " dewp=" + this.dewp + " windDir=" + this.windDir + " windSpeed=" + this.windSpeed + " qs=" + this.qs;
        }
        
        @Override
        Structure makeStructure(final Structure parent) throws InvalidRangeException {
            final Sequence seq = new Sequence(NmcObsLegacy.this.ncfile, null, parent, "tropopause");
            seq.addAttribute(new Attribute("long_name", NmcObsLegacy.this.catNames[5]));
            int pos = 0;
            Variable v = seq.addMemberVariable(new Variable(NmcObsLegacy.this.ncfile, null, parent, "pressure", DataType.FLOAT, ""));
            v.addAttribute(new Attribute("units", "mbars"));
            v.addAttribute(new Attribute("long_name", "pressure level"));
            v.addAttribute(new Attribute("_CoordinateAxisType", AxisType.Pressure.toString()));
            v.setSPobject(new Vinfo(pos));
            pos += 4;
            v = seq.addMemberVariable(new Variable(NmcObsLegacy.this.ncfile, null, parent, "temperature", DataType.FLOAT, ""));
            v.addAttribute(new Attribute("units", "celsius"));
            v.addAttribute(new Attribute("long_name", "temperature"));
            v.addAttribute(new Attribute("accuracy", "celsius/10"));
            v.addAttribute(new Attribute("missing_value", 999.9f));
            v.setSPobject(new Vinfo(pos));
            pos += 4;
            v = seq.addMemberVariable(new Variable(NmcObsLegacy.this.ncfile, null, parent, "dewpoint", DataType.FLOAT, ""));
            v.addAttribute(new Attribute("units", "celsius"));
            v.addAttribute(new Attribute("long_name", "dewpoint depression"));
            v.addAttribute(new Attribute("accuracy", "celsius/10"));
            v.addAttribute(new Attribute("missing_value", 99.9f));
            v.setSPobject(new Vinfo(pos));
            pos += 4;
            v = seq.addMemberVariable(new Variable(NmcObsLegacy.this.ncfile, null, parent, "windDir", DataType.SHORT, ""));
            v.addAttribute(new Attribute("units", "degrees"));
            v.addAttribute(new Attribute("long_name", "wind direction"));
            v.addAttribute(new Attribute("missing_value", 999));
            v.setSPobject(new Vinfo(pos));
            pos += 2;
            v = seq.addMemberVariable(new Variable(NmcObsLegacy.this.ncfile, null, parent, "windSpeed", DataType.SHORT, ""));
            v.addAttribute(new Attribute("units", "knots"));
            v.addAttribute(new Attribute("long_name", "wind speed"));
            v.addAttribute(new Attribute("missing_value", 999));
            v.setSPobject(new Vinfo(pos));
            pos += 2;
            v = seq.addMemberVariable(new Variable(NmcObsLegacy.this.ncfile, null, parent, "qualityFlags", DataType.CHAR, ""));
            v.setDimensionsAnonymous(new int[] { 4 });
            v.addAttribute(new Attribute("long_name", "quality marks: 0=pressure, 1=temp, 2=dewpoint, 3=wind"));
            v.setSPobject(new Vinfo(pos));
            pos += 4;
            return seq;
        }
        
        @Override
        void loadStructureData(final ByteBuffer bb) {
            bb.putFloat(this.press);
            bb.putFloat(this.temp);
            bb.putFloat(this.dewp);
            bb.putShort(this.windDir);
            bb.putShort(this.windSpeed);
            bb.put(this.quality);
        }
    }
    
    private class Cat07 extends Entry
    {
        float press;
        short percentClouds;
        byte[] quality;
        String qs;
        
        Cat07(final byte[] b, final int offset) throws IOException {
            this.press = 0.1f * Float.parseFloat(new String(b, offset, 5));
            this.percentClouds = Short.parseShort(new String(b, offset + 5, 3));
            System.arraycopy(b, offset + 8, this.quality = new byte[2], 0, 2);
            this.qs = new String(this.quality);
        }
        
        @Override
        public String toString() {
            return "Cat07: press=" + this.press + " percentClouds=" + this.percentClouds + " qs=" + this.qs;
        }
        
        @Override
        Structure makeStructure(final Structure parent) throws InvalidRangeException {
            final Sequence seq = new Sequence(NmcObsLegacy.this.ncfile, null, parent, "clouds");
            seq.addAttribute(new Attribute("long_name", NmcObsLegacy.this.catNames[7]));
            int pos = 0;
            Variable v = seq.addMemberVariable(new Variable(NmcObsLegacy.this.ncfile, null, parent, "pressure", DataType.FLOAT, ""));
            v.addAttribute(new Attribute("units", "mbars"));
            v.addAttribute(new Attribute("long_name", "pressure level"));
            v.addAttribute(new Attribute("_CoordinateAxisType", AxisType.Pressure.toString()));
            v.setSPobject(new Vinfo(pos));
            pos += 4;
            v = seq.addMemberVariable(new Variable(NmcObsLegacy.this.ncfile, null, parent, "percentClouds", DataType.SHORT, ""));
            v.addAttribute(new Attribute("units", ""));
            v.addAttribute(new Attribute("long_name", "amount of cloudiness (%)"));
            v.addAttribute(new Attribute("missing_value", 999));
            v.setSPobject(new Vinfo(pos));
            pos += 2;
            v = seq.addMemberVariable(new Variable(NmcObsLegacy.this.ncfile, null, parent, "qualityFlags", DataType.CHAR, ""));
            v.setDimensionsAnonymous(new int[] { 2 });
            v.addAttribute(new Attribute("long_name", "quality marks: 0=pressure, 1=percentClouds"));
            v.setSPobject(new Vinfo(pos));
            pos += 2;
            return seq;
        }
        
        @Override
        void loadStructureData(final ByteBuffer bb) {
            bb.putFloat(this.press);
            bb.putShort(this.percentClouds);
            bb.put(this.quality);
        }
    }
    
    private class Cat08 extends Entry
    {
        int data;
        short table101code;
        byte[] quality;
        String qs;
        
        Cat08(final byte[] b, final int offset) throws IOException {
            this.data = Integer.parseInt(new String(b, offset, 5));
            this.table101code = Short.parseShort(new String(b, offset + 5, 3));
            System.arraycopy(b, offset + 8, this.quality = new byte[2], 0, 2);
            this.qs = new String(this.quality);
        }
        
        @Override
        public String toString() {
            return "Cat08: data=" + this.data + " table101code=" + this.table101code + " qs=" + this.qs;
        }
        
        @Override
        Structure makeStructure(final Structure parent) throws InvalidRangeException {
            final Sequence seq = new Sequence(NmcObsLegacy.this.ncfile, null, parent, "otherData");
            seq.addAttribute(new Attribute("long_name", NmcObsLegacy.this.catNames[8]));
            int pos = 0;
            Variable v = seq.addMemberVariable(new Variable(NmcObsLegacy.this.ncfile, null, parent, "data", DataType.INT, ""));
            v.addAttribute(new Attribute("long_name", "additional data specified in table 101.1"));
            v.addAttribute(new Attribute("_CoordinateAxisType", AxisType.Pressure.toString()));
            v.setSPobject(new Vinfo(pos));
            pos += 4;
            v = seq.addMemberVariable(new Variable(NmcObsLegacy.this.ncfile, null, parent, "table101code", DataType.SHORT, ""));
            v.addAttribute(new Attribute("long_name", "code figure from table 101"));
            v.addAttribute(new Attribute("missing_value", 999));
            v.setSPobject(new Vinfo(pos));
            pos += 2;
            v = seq.addMemberVariable(new Variable(NmcObsLegacy.this.ncfile, null, parent, "indicatorFlags", DataType.CHAR, ""));
            v.setDimensionsAnonymous(new int[] { 2 });
            v.addAttribute(new Attribute("long_name", "quality marks: 0=data, 1=form"));
            v.setSPobject(new Vinfo(pos));
            pos += 2;
            return seq;
        }
        
        @Override
        void loadStructureData(final ByteBuffer bb) {
            bb.putInt(this.data);
            bb.putShort(this.table101code);
            bb.put(this.quality);
        }
    }
    
    private class Cat51 extends Entry
    {
        short windDir;
        short windSpeed;
        float pressSeaLevel;
        float pressStation;
        float geopot;
        float press;
        float temp;
        float dewp;
        float maxTemp;
        float minTemp;
        float pressureTendency;
        byte[] quality;
        byte pastWeatherW2;
        byte pressureTendencyChar;
        byte[] horizVis;
        byte[] presentWeather;
        byte[] pastWeatherW1;
        byte[] fracCloudN;
        byte[] fracCloudNh;
        byte[] cloudCl;
        byte[] cloudBaseHeight;
        byte[] cloudCm;
        byte[] cloudCh;
        
        Cat51(final byte[] b, final int offset) throws IOException {
            this.quality = new byte[4];
            this.horizVis = new byte[3];
            this.presentWeather = new byte[3];
            this.pastWeatherW1 = new byte[2];
            this.fracCloudN = new byte[2];
            this.fracCloudNh = new byte[2];
            this.cloudCl = new byte[2];
            this.cloudBaseHeight = new byte[2];
            this.cloudCm = new byte[2];
            this.cloudCh = new byte[2];
            this.pressSeaLevel = Float.parseFloat(new String(b, offset, 5));
            this.pressStation = Float.parseFloat(new String(b, offset + 5, 5));
            this.windDir = Short.parseShort(new String(b, offset + 10, 3));
            this.windSpeed = Short.parseShort(new String(b, offset + 13, 3));
            this.temp = 0.1f * Float.parseFloat(new String(b, offset + 16, 4));
            this.dewp = 0.1f * Float.parseFloat(new String(b, offset + 20, 3));
            this.maxTemp = 0.1f * Float.parseFloat(new String(b, offset + 23, 4));
            this.minTemp = 0.1f * Float.parseFloat(new String(b, offset + 27, 4));
            System.arraycopy(b, offset + 31, this.quality, 0, 4);
            this.pastWeatherW2 = b[offset + 35];
            System.arraycopy(b, offset + 36, this.horizVis, 0, 3);
            System.arraycopy(b, offset + 39, this.presentWeather, 0, 3);
            System.arraycopy(b, offset + 42, this.pastWeatherW1, 0, 2);
            System.arraycopy(b, offset + 44, this.fracCloudN, 0, 2);
            System.arraycopy(b, offset + 46, this.fracCloudNh, 0, 2);
            System.arraycopy(b, offset + 48, this.cloudCl, 0, 2);
            System.arraycopy(b, offset + 50, this.cloudBaseHeight, 0, 2);
            System.arraycopy(b, offset + 52, this.cloudCm, 0, 2);
            System.arraycopy(b, offset + 54, this.cloudCh, 0, 2);
            this.pressureTendencyChar = b[offset + 56];
            this.pressureTendency = 0.1f * Float.parseFloat(new String(b, offset + 57, 3));
        }
        
        @Override
        public String toString() {
            return "Cat51: press= " + this.press + " geopot=" + this.geopot + " temp= " + this.temp + " dewp=" + this.dewp + " windDir=" + this.windDir + " windSpeed=" + this.windSpeed + " qs=" + new String(this.quality) + " pressureTendency=" + this.pressureTendency;
        }
        
        @Override
        Structure makeStructure(final Structure parent) throws InvalidRangeException {
            final Sequence seq = new Sequence(NmcObsLegacy.this.ncfile, null, parent, "surfaceData");
            seq.addAttribute(new Attribute("long_name", NmcObsLegacy.this.catNames[11]));
            int pos = 0;
            Variable v = seq.addMemberVariable(new Variable(NmcObsLegacy.this.ncfile, null, parent, "pressureSeaLevel", DataType.FLOAT, ""));
            v.addAttribute(new Attribute("units", "mbars"));
            v.addAttribute(new Attribute("long_name", "sea level pressure"));
            v.addAttribute(new Attribute("accuracy", "mbars/10"));
            v.addAttribute(new Attribute("missing_value", 9999.9f));
            v.addAttribute(new Attribute("_CoordinateAxisType", AxisType.Pressure.toString()));
            v.setSPobject(new Vinfo(pos));
            pos += 4;
            v = seq.addMemberVariable(new Variable(NmcObsLegacy.this.ncfile, null, parent, "pressure", DataType.FLOAT, ""));
            v.addAttribute(new Attribute("units", "mbars"));
            v.addAttribute(new Attribute("long_name", "station pressure"));
            v.addAttribute(new Attribute("accuracy", "mbars/10"));
            v.addAttribute(new Attribute("missing_value", 9999.9f));
            v.addAttribute(new Attribute("_CoordinateAxisType", AxisType.Pressure.toString()));
            v.setSPobject(new Vinfo(pos));
            pos += 4;
            v = seq.addMemberVariable(new Variable(NmcObsLegacy.this.ncfile, null, parent, "windDir", DataType.SHORT, ""));
            v.addAttribute(new Attribute("units", "degrees"));
            v.addAttribute(new Attribute("long_name", "wind direction"));
            v.addAttribute(new Attribute("missing_value", 999));
            v.setSPobject(new Vinfo(pos));
            pos += 2;
            v = seq.addMemberVariable(new Variable(NmcObsLegacy.this.ncfile, null, parent, "windSpeed", DataType.SHORT, ""));
            v.addAttribute(new Attribute("units", "knots"));
            v.addAttribute(new Attribute("long_name", "wind speed"));
            v.addAttribute(new Attribute("missing_value", 999));
            v.setSPobject(new Vinfo(pos));
            pos += 2;
            v = seq.addMemberVariable(new Variable(NmcObsLegacy.this.ncfile, null, parent, "temperature", DataType.FLOAT, ""));
            v.addAttribute(new Attribute("units", "celsius"));
            v.addAttribute(new Attribute("long_name", "air temperature"));
            v.addAttribute(new Attribute("accuracy", "celsius/10"));
            v.addAttribute(new Attribute("missing_value", 999.9f));
            v.setSPobject(new Vinfo(pos));
            pos += 4;
            v = seq.addMemberVariable(new Variable(NmcObsLegacy.this.ncfile, null, parent, "dewpoint", DataType.FLOAT, ""));
            v.addAttribute(new Attribute("units", "celsius"));
            v.addAttribute(new Attribute("long_name", "dewpoint depression"));
            v.addAttribute(new Attribute("accuracy", "celsius/10"));
            v.addAttribute(new Attribute("missing_value", 99.9f));
            v.setSPobject(new Vinfo(pos));
            pos += 4;
            v = seq.addMemberVariable(new Variable(NmcObsLegacy.this.ncfile, null, parent, "temperatureMax", DataType.FLOAT, ""));
            v.addAttribute(new Attribute("units", "celsius"));
            v.addAttribute(new Attribute("long_name", "maximum temperature"));
            v.addAttribute(new Attribute("accuracy", "celsius/10"));
            v.addAttribute(new Attribute("missing_value", 999.9f));
            v.setSPobject(new Vinfo(pos));
            pos += 4;
            v = seq.addMemberVariable(new Variable(NmcObsLegacy.this.ncfile, null, parent, "temperatureMin", DataType.FLOAT, ""));
            v.addAttribute(new Attribute("units", "celsius"));
            v.addAttribute(new Attribute("long_name", "minimum temperature"));
            v.addAttribute(new Attribute("accuracy", "celsius/10"));
            v.addAttribute(new Attribute("missing_value", 999.9f));
            v.setSPobject(new Vinfo(pos));
            pos += 4;
            v = seq.addMemberVariable(new Variable(NmcObsLegacy.this.ncfile, null, parent, "qualityFlags", DataType.CHAR, ""));
            v.setDimensionsAnonymous(new int[] { 4 });
            v.addAttribute(new Attribute("long_name", "quality marks: 0=pressureSeaLevel, 1=pressure, 2=wind, 3=temperature"));
            v.setSPobject(new Vinfo(pos));
            pos += 4;
            v = seq.addMemberVariable(new Variable(NmcObsLegacy.this.ncfile, null, parent, "pastWeatherW2", DataType.CHAR, ""));
            v.addAttribute(new Attribute("long_name", "past weather (W2): WMO table 4561"));
            v.setSPobject(new Vinfo(pos));
            ++pos;
            v = seq.addMemberVariable(new Variable(NmcObsLegacy.this.ncfile, null, parent, "horizViz", DataType.CHAR, ""));
            v.setDimensionsAnonymous(new int[] { 3 });
            v.addAttribute(new Attribute("long_name", "horizontal visibility: WMO table 4300"));
            v.setSPobject(new Vinfo(pos));
            pos += 3;
            v = seq.addMemberVariable(new Variable(NmcObsLegacy.this.ncfile, null, parent, "presentWeatherWW", DataType.CHAR, ""));
            v.setDimensionsAnonymous(new int[] { 3 });
            v.addAttribute(new Attribute("long_name", "present weather (WW): WMO table 4677"));
            v.setSPobject(new Vinfo(pos));
            pos += 3;
            v = seq.addMemberVariable(new Variable(NmcObsLegacy.this.ncfile, null, parent, "pastWeatherW1", DataType.CHAR, ""));
            v.setDimensionsAnonymous(new int[] { 2 });
            v.addAttribute(new Attribute("long_name", "past weather (WW): WMO table 4561"));
            v.setSPobject(new Vinfo(pos));
            pos += 2;
            v = seq.addMemberVariable(new Variable(NmcObsLegacy.this.ncfile, null, parent, "cloudFractionN", DataType.CHAR, ""));
            v.setDimensionsAnonymous(new int[] { 2 });
            v.addAttribute(new Attribute("long_name", "cloud fraction (N): WMO table 2700"));
            v.setSPobject(new Vinfo(pos));
            pos += 2;
            v = seq.addMemberVariable(new Variable(NmcObsLegacy.this.ncfile, null, parent, "cloudFractionNh", DataType.CHAR, ""));
            v.setDimensionsAnonymous(new int[] { 2 });
            v.addAttribute(new Attribute("long_name", "cloud fraction (Nh): WMO table 2700"));
            v.setSPobject(new Vinfo(pos));
            pos += 2;
            v = seq.addMemberVariable(new Variable(NmcObsLegacy.this.ncfile, null, parent, "cloudFractionCL", DataType.CHAR, ""));
            v.setDimensionsAnonymous(new int[] { 2 });
            v.addAttribute(new Attribute("long_name", "cloud fraction (CL): WMO table 0513"));
            v.setSPobject(new Vinfo(pos));
            pos += 2;
            v = seq.addMemberVariable(new Variable(NmcObsLegacy.this.ncfile, null, parent, "cloudHeightCL", DataType.CHAR, ""));
            v.setDimensionsAnonymous(new int[] { 2 });
            v.addAttribute(new Attribute("long_name", "cloud base height above ground (h): WMO table 1600"));
            v.setSPobject(new Vinfo(pos));
            pos += 2;
            v = seq.addMemberVariable(new Variable(NmcObsLegacy.this.ncfile, null, parent, "cloudFractionCM", DataType.CHAR, ""));
            v.setDimensionsAnonymous(new int[] { 2 });
            v.addAttribute(new Attribute("long_name", "cloud fraction (CM): WMO table 0515"));
            v.setSPobject(new Vinfo(pos));
            pos += 2;
            v = seq.addMemberVariable(new Variable(NmcObsLegacy.this.ncfile, null, parent, "cloudFractionCH", DataType.CHAR, ""));
            v.setDimensionsAnonymous(new int[] { 2 });
            v.addAttribute(new Attribute("long_name", "cloud fraction (CH): WMO table 0509"));
            v.setSPobject(new Vinfo(pos));
            pos += 2;
            v = seq.addMemberVariable(new Variable(NmcObsLegacy.this.ncfile, null, parent, "pressureTendencyCharacteristic", DataType.CHAR, ""));
            v.addAttribute(new Attribute("long_name", "pressure tendency characteristic for 3 hours previous to obs time: WMO table 0200"));
            v.setSPobject(new Vinfo(pos));
            ++pos;
            v = seq.addMemberVariable(new Variable(NmcObsLegacy.this.ncfile, null, parent, "pressureTendency", DataType.FLOAT, ""));
            v.addAttribute(new Attribute("units", "mbars"));
            v.addAttribute(new Attribute("long_name", "pressure tendency magnitude"));
            v.addAttribute(new Attribute("accuracy", "mbars/10"));
            v.addAttribute(new Attribute("missing_value", 99.9f));
            v.setSPobject(new Vinfo(pos));
            pos += 4;
            return seq;
        }
        
        @Override
        void loadStructureData(final ByteBuffer bb) {
            bb.putFloat(this.pressSeaLevel);
            bb.putFloat(this.pressStation);
            bb.putShort(this.windDir);
            bb.putShort(this.windSpeed);
            bb.putFloat(this.temp);
            bb.putFloat(this.dewp);
            bb.putFloat(this.maxTemp);
            bb.putFloat(this.minTemp);
            bb.put(this.quality);
            bb.put(this.pastWeatherW2);
            bb.put(this.horizVis);
            bb.put(this.presentWeather);
            bb.put(this.pastWeatherW1);
            bb.put(this.fracCloudN);
            bb.put(this.fracCloudNh);
            bb.put(this.cloudCl);
            bb.put(this.cloudBaseHeight);
            bb.put(this.cloudCm);
            bb.put(this.cloudCh);
            bb.put(this.pressureTendencyChar);
            bb.putFloat(this.pressureTendency);
        }
    }
    
    private class Cat52 extends Entry
    {
        short snowDepth;
        short wavePeriod;
        short waveHeight;
        short waveSwellPeriod;
        short waveSwellHeight;
        float precip6hours;
        float precip24hours;
        float sst;
        float waterEquiv;
        byte precipDuration;
        byte shipCourse;
        byte[] waveDirection;
        byte[] special;
        byte[] special2;
        byte[] shipSpeed;
        
        Cat52(final byte[] b, final int offset) throws IOException {
            this.waveDirection = new byte[2];
            this.special = new byte[2];
            this.special2 = new byte[2];
            this.shipSpeed = new byte[2];
            this.precip6hours = 0.01f * Float.parseFloat(new String(b, offset, 4));
            this.snowDepth = Short.parseShort(new String(b, offset + 4, 3));
            this.precip24hours = 0.01f * Float.parseFloat(new String(b, offset + 7, 4));
            this.precipDuration = b[offset + 11];
            this.wavePeriod = Short.parseShort(new String(b, offset + 12, 2));
            this.waveHeight = Short.parseShort(new String(b, offset + 14, 2));
            System.arraycopy(b, offset + 16, this.waveDirection, 0, 2);
            this.waveSwellPeriod = Short.parseShort(new String(b, offset + 18, 2));
            this.waveSwellHeight = Short.parseShort(new String(b, offset + 20, 2));
            this.sst = 0.1f * Float.parseFloat(new String(b, offset + 22, 4));
            System.arraycopy(b, offset + 26, this.special, 0, 2);
            System.arraycopy(b, offset + 28, this.special2, 0, 2);
            this.shipCourse = b[offset + 30];
            System.arraycopy(b, offset + 31, this.shipSpeed, 0, 2);
            this.waterEquiv = 0.001f * Float.parseFloat(new String(b, offset + 33, 7));
        }
        
        @Override
        void loadStructureData(final ByteBuffer bb) {
            bb.putFloat(this.precip6hours);
            bb.putShort(this.snowDepth);
            bb.putFloat(this.precip24hours);
            bb.put(this.precipDuration);
            bb.putShort(this.wavePeriod);
            bb.putShort(this.waveHeight);
            bb.put(this.waveDirection);
            bb.putShort(this.waveSwellPeriod);
            bb.putShort(this.waveSwellHeight);
            bb.putFloat(this.sst);
            bb.put(this.special);
            bb.put(this.special2);
            bb.put(this.shipCourse);
            bb.put(this.shipSpeed);
            bb.putFloat(this.waterEquiv);
        }
        
        @Override
        public String toString() {
            return "Cat52: precip6hours= " + this.precip6hours + " precip24hours=" + this.precip24hours + " sst= " + this.sst + " waterEquiv=" + this.waterEquiv + " snowDepth=" + this.snowDepth + " wavePeriod=" + this.wavePeriod + " waveHeight=" + this.waveHeight + " waveSwellPeriod=" + this.waveSwellPeriod + " waveSwellHeight=" + this.waveSwellHeight;
        }
        
        @Override
        Structure makeStructure(final Structure parent) throws InvalidRangeException {
            final Sequence seq = new Sequence(NmcObsLegacy.this.ncfile, null, parent, "surfaceData2");
            seq.addAttribute(new Attribute("long_name", NmcObsLegacy.this.catNames[12]));
            int pos = 0;
            Variable v = seq.addMemberVariable(new Variable(NmcObsLegacy.this.ncfile, null, parent, "precip6hours", DataType.FLOAT, ""));
            v.addAttribute(new Attribute("units", "inch"));
            v.addAttribute(new Attribute("long_name", "precipitation past 6 hours"));
            v.addAttribute(new Attribute("accuracy", "inch/100"));
            v.addAttribute(new Attribute("missing_value", 99.99f));
            v.setSPobject(new Vinfo(pos));
            pos += 4;
            v = seq.addMemberVariable(new Variable(NmcObsLegacy.this.ncfile, null, parent, "snowDepth", DataType.SHORT, ""));
            v.addAttribute(new Attribute("units", "inch"));
            v.addAttribute(new Attribute("long_name", "total depth of snow on ground"));
            v.addAttribute(new Attribute("missing_value", 999));
            v.setSPobject(new Vinfo(pos));
            pos += 2;
            v = seq.addMemberVariable(new Variable(NmcObsLegacy.this.ncfile, null, parent, "precip24hours", DataType.FLOAT, ""));
            v.addAttribute(new Attribute("units", "inch"));
            v.addAttribute(new Attribute("long_name", "precipitation past 24 hours"));
            v.addAttribute(new Attribute("accuracy", "inch/100"));
            v.addAttribute(new Attribute("missing_value", 99.99f));
            v.setSPobject(new Vinfo(pos));
            pos += 4;
            v = seq.addMemberVariable(new Variable(NmcObsLegacy.this.ncfile, null, parent, "precipDuration", DataType.BYTE, ""));
            v.addAttribute(new Attribute("units", "6 hours"));
            v.addAttribute(new Attribute("long_name", "duration of precipitation observation"));
            v.addAttribute(new Attribute("missing_value", 9));
            v.setSPobject(new Vinfo(pos));
            pos += 4;
            v = seq.addMemberVariable(new Variable(NmcObsLegacy.this.ncfile, null, parent, "wavePeriod", DataType.SHORT, ""));
            v.addAttribute(new Attribute("units", "second"));
            v.addAttribute(new Attribute("long_name", "period of waves"));
            v.addAttribute(new Attribute("missing_value", 99));
            v.setSPobject(new Vinfo(pos));
            pos += 2;
            v = seq.addMemberVariable(new Variable(NmcObsLegacy.this.ncfile, null, parent, "waveHeight", DataType.SHORT, ""));
            v.addAttribute(new Attribute("units", "meter/2"));
            v.addAttribute(new Attribute("long_name", "height of waves"));
            v.addAttribute(new Attribute("missing_value", 99));
            v.setSPobject(new Vinfo(pos));
            pos += 2;
            v = seq.addMemberVariable(new Variable(NmcObsLegacy.this.ncfile, null, parent, "swellWaveDir", DataType.CHAR, ""));
            v.setDimensionsAnonymous(new int[] { 2 });
            v.addAttribute(new Attribute("long_name", "direction from which swell waves are moving: WMO table 0877"));
            v.setSPobject(new Vinfo(pos));
            pos += 2;
            v = seq.addMemberVariable(new Variable(NmcObsLegacy.this.ncfile, null, parent, "swellWavePeriod", DataType.SHORT, ""));
            v.addAttribute(new Attribute("units", "second"));
            v.addAttribute(new Attribute("long_name", "period of swell waves"));
            v.addAttribute(new Attribute("missing_value", 99));
            v.setSPobject(new Vinfo(pos));
            pos += 2;
            v = seq.addMemberVariable(new Variable(NmcObsLegacy.this.ncfile, null, parent, "swellWaveHeight", DataType.SHORT, ""));
            v.addAttribute(new Attribute("units", "meter/2"));
            v.addAttribute(new Attribute("long_name", "height of waves"));
            v.addAttribute(new Attribute("missing_value", 99));
            v.setSPobject(new Vinfo(pos));
            pos += 2;
            v = seq.addMemberVariable(new Variable(NmcObsLegacy.this.ncfile, null, parent, "sst", DataType.FLOAT, ""));
            v.addAttribute(new Attribute("units", "celsius"));
            v.addAttribute(new Attribute("long_name", "sea surface temperature"));
            v.addAttribute(new Attribute("accuracy", "celsius/10"));
            v.addAttribute(new Attribute("missing_value", 999.9f));
            v.setSPobject(new Vinfo(pos));
            pos += 4;
            v = seq.addMemberVariable(new Variable(NmcObsLegacy.this.ncfile, null, parent, "special", DataType.CHAR, ""));
            v.addAttribute(new Attribute("long_name", "special phenomena - general"));
            v.setSPobject(new Vinfo(pos));
            pos += 2;
            v = seq.addMemberVariable(new Variable(NmcObsLegacy.this.ncfile, null, parent, "specialDetail", DataType.CHAR, ""));
            v.addAttribute(new Attribute("long_name", "special phenomena - detailed"));
            v.setSPobject(new Vinfo(pos));
            pos += 2;
            v = seq.addMemberVariable(new Variable(NmcObsLegacy.this.ncfile, null, parent, "shipCourse", DataType.CHAR, ""));
            v.addAttribute(new Attribute("long_name", "ships course: WMO table 0700"));
            v.setSPobject(new Vinfo(pos));
            ++pos;
            v = seq.addMemberVariable(new Variable(NmcObsLegacy.this.ncfile, null, parent, "shipSpeed", DataType.CHAR, ""));
            v.addAttribute(new Attribute("long_name", "ships average speed: WMO table 4451"));
            v.setSPobject(new Vinfo(pos));
            ++pos;
            v = seq.addMemberVariable(new Variable(NmcObsLegacy.this.ncfile, null, parent, "waterEquiv", DataType.SHORT, ""));
            v.addAttribute(new Attribute("units", "inch"));
            v.addAttribute(new Attribute("long_name", "water equivalent of snow and/or ice"));
            v.addAttribute(new Attribute("accuracy", "inch/100"));
            v.addAttribute(new Attribute("missing_value", 99999.99f));
            v.setSPobject(new Vinfo(pos));
            pos += 4;
            return seq;
        }
    }
    
    static class MyNetcdfFile extends NetcdfFile
    {
        MyNetcdfFile(final NmcObsLegacy iosp) {
            this.spi = iosp;
        }
    }
}
