// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.iosp.misc;

import ucar.nc2.constants.CF;
import ucar.nc2.Attribute;
import ucar.nc2.Variable;
import ucar.nc2.constants.AxisType;
import ucar.ma2.DataType;
import ucar.nc2.Structure;
import ucar.nc2.Group;
import ucar.nc2.NetcdfFile;
import ucar.nc2.iosp.AbstractIOServiceProvider;

public abstract class AbstractLightningIOSP extends AbstractIOServiceProvider
{
    public static final String TIME = "time";
    public static final String LAT = "lat";
    public static final String LON = "lon";
    public static final String SIGNAL = "sgnl";
    public static final String MULTIPLICITY = "mult";
    public static final String MAJOR_AXIS = "majorAxis";
    public static final String MINOR_AXIS = "minorAxis";
    public static final String ELLIPSE_ANGLE = "ellipseAngle";
    public static final String ECCENTRICITY = "eccent";
    public static final String RECORD = "record";
    public static final String secondsSince1970 = "seconds since 1970-01-01 00:00:00";
    
    protected Variable makeLightningVariable(final NetcdfFile ncfile, final Group group, final Structure seq, final String name, final DataType dataType, final String dims, final String longName, final String cfName, final String units, final AxisType type) {
        final Variable v = new Variable(ncfile, group, seq, name);
        v.setDataType(dataType);
        v.setDimensions(dims);
        v.addAttribute(new Attribute("long_name", longName));
        if (cfName != null) {
            v.addAttribute(new Attribute("standard_name", cfName));
        }
        if (units != null) {
            v.addAttribute(new Attribute("units", units));
        }
        if (type != null) {
            v.addAttribute(new Attribute("_CoordinateAxisType", type.toString()));
        }
        return v;
    }
    
    protected void addLightningGlobalAttributes(final NetcdfFile ncfile) {
        ncfile.addAttribute(null, new Attribute("CF:featureType", CF.FeatureType.point.toString()));
        ncfile.addAttribute(null, new Attribute("history", "Read directly by Netcdf Java IOSP"));
    }
}
