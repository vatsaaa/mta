// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.iosp;

import java.io.IOException;
import ucar.ma2.InvalidRangeException;
import ucar.ma2.Section;

public class LayoutTiled implements Layout
{
    private Section want;
    private int[] chunkSize;
    private int elemSize;
    private long startSrcPos;
    private DataChunkIterator chunkIterator;
    private IndexChunkerTiled index;
    private long totalNelems;
    private long totalNelemsDone;
    private boolean debug;
    private boolean debugNext;
    private Chunk next;
    
    public LayoutTiled(final DataChunkIterator chunkIterator, final int[] chunkSize, final int elemSize, final Section wantSection) throws InvalidRangeException, IOException {
        this.index = null;
        this.debug = false;
        this.debugNext = false;
        this.next = null;
        this.chunkIterator = chunkIterator;
        this.chunkSize = chunkSize;
        this.elemSize = elemSize;
        this.want = wantSection;
        this.totalNelems = this.want.computeSize();
        this.totalNelemsDone = 0L;
    }
    
    public long getTotalNelems() {
        return this.totalNelems;
    }
    
    public int getElemSize() {
        return this.elemSize;
    }
    
    public boolean hasNext() {
        if (this.totalNelemsDone >= this.totalNelems) {
            return false;
        }
        Label_0186: {
            if (this.index != null) {
                if (this.index.hasNext()) {
                    break Label_0186;
                }
            }
            try {
                while (this.chunkIterator.hasNext()) {
                    DataChunk dataChunk;
                    try {
                        dataChunk = this.chunkIterator.next();
                    }
                    catch (IOException e) {
                        e.printStackTrace();
                        this.next = null;
                        return false;
                    }
                    final Section dataSection = new Section(dataChunk.offset, this.chunkSize);
                    if (dataSection.intersects(this.want)) {
                        if (this.debug) {
                            System.out.println(" found intersecting section: " + dataSection + " for filePos " + dataChunk.filePos);
                        }
                        this.index = new IndexChunkerTiled(dataSection, this.want);
                        this.startSrcPos = dataChunk.filePos;
                        break Label_0186;
                    }
                }
                this.next = null;
                return false;
            }
            catch (InvalidRangeException e2) {
                throw new IllegalStateException(e2);
            }
        }
        final IndexChunker.Chunk chunk = this.index.next();
        this.totalNelemsDone += chunk.getNelems();
        chunk.setSrcPos(this.startSrcPos + chunk.getSrcElem() * this.elemSize);
        this.next = chunk;
        return true;
    }
    
    public Chunk next() throws IOException {
        if (this.debugNext) {
            System.out.println("  next=" + this.next);
        }
        return this.next;
    }
    
    @Override
    public String toString() {
        final StringBuilder sbuff = new StringBuilder();
        sbuff.append("want=").append(this.want).append("; ");
        sbuff.append("chunkSize=[");
        for (int i = 0; i < this.chunkSize.length; ++i) {
            if (i > 0) {
                sbuff.append(",");
            }
            sbuff.append(this.chunkSize[i]);
        }
        sbuff.append("] totalNelems=").append(this.totalNelems);
        sbuff.append(" elemSize=").append(this.elemSize);
        return sbuff.toString();
    }
    
    public static class DataChunk
    {
        public int[] offset;
        public long filePos;
        
        public DataChunk(final int[] offset, final long filePos) {
            this.offset = offset;
            this.filePos = filePos;
        }
    }
    
    public interface DataChunkIterator
    {
        boolean hasNext();
        
        DataChunk next() throws IOException;
    }
}
