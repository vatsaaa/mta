// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.iosp.nowrad;

import ucar.ma2.ArrayByte;
import ucar.ma2.InvalidRangeException;
import ucar.ma2.Range;
import java.util.List;
import java.nio.ByteBuffer;
import ucar.ma2.Array;
import ucar.ma2.Section;
import ucar.nc2.Variable;
import java.io.IOException;
import ucar.nc2.util.CancelTask;
import ucar.nc2.NetcdfFile;
import ucar.unidata.io.RandomAccessFile;
import java.util.HashMap;
import ucar.nc2.iosp.AbstractIOServiceProvider;

public class NOWRadiosp extends AbstractIOServiceProvider
{
    static final int DEF_WBITS = 15;
    static final int Z_DEFLATED = 8;
    protected int fileUsed;
    protected int recStart;
    protected boolean debug;
    protected boolean debugSize;
    protected boolean debugSPIO;
    protected boolean showHeaderBytes;
    protected HashMap dimHash;
    protected boolean fill;
    protected NOWRadheader headerParser;
    private RandomAccessFile myRaf;
    private NetcdfFile ncfile;
    private int pcode;
    protected boolean readonly;
    
    public NOWRadiosp() {
        this.fileUsed = 0;
        this.recStart = 0;
        this.debug = false;
        this.debugSize = false;
        this.debugSPIO = false;
        this.showHeaderBytes = false;
        this.dimHash = new HashMap(50);
    }
    
    public boolean isValidFile(final RandomAccessFile raf) {
        final NOWRadheader localHeader = new NOWRadheader();
        return localHeader.isValidFile(raf);
    }
    
    public String getFileTypeId() {
        return "NOWRAD";
    }
    
    public String getFileTypeDescription() {
        return "NOWRAD Products";
    }
    
    @Override
    public void open(final RandomAccessFile raf, final NetcdfFile file, final CancelTask cancelTask) throws IOException {
        this.ncfile = file;
        this.myRaf = raf;
        this.headerParser = new NOWRadheader();
        try {
            this.headerParser.read(this.myRaf, this.ncfile);
        }
        catch (Exception ex) {}
        this.pcode = 0;
        this.ncfile.finish();
    }
    
    public Array readData(final Variable v2, final Section section) throws IOException, InvalidRangeException {
        byte[] vdata = null;
        final List<Range> ranges = section.getRanges();
        final NOWRadheader.Vinfo vinfo = (NOWRadheader.Vinfo)v2.getSPobject();
        try {
            vdata = this.headerParser.getData((int)vinfo.hoff);
        }
        catch (Exception ex) {}
        final ByteBuffer bos = ByteBuffer.wrap(vdata);
        final Object data = this.readOneScanData(bos, vinfo, v2.getName());
        Array outputData = Array.factory(v2.getDataType().getPrimitiveClassType(), v2.getShape(), data);
        outputData = outputData.flip(1);
        return outputData.sectionNoReduce(ranges).copy();
    }
    
    public Object readOneScanData(final ByteBuffer bos, final NOWRadheader.Vinfo vinfo, final String vName) throws IOException, InvalidRangeException {
        final int doff = (int)vinfo.hoff;
        final int npixel = vinfo.yt * vinfo.xt;
        byte[] rdata = null;
        byte[] ldata = new byte[vinfo.xt];
        final byte[] pdata = new byte[npixel];
        final byte[] b2 = new byte[2];
        bos.position(doff);
        if (this.convertunsignedByte2Short(bos.get()) != 240 || bos.get() != 12) {
            return null;
        }
        int offset = 0;
        int roffset = 0;
        boolean newline = true;
        int linenum = 0;
        while (true) {
            if (newline) {
                bos.get(b2);
                linenum = (this.convertunsignedByte2Short(b2[1]) << 8) + this.convertunsignedByte2Short(b2[0]);
            }
            final short b3 = this.convertunsignedByte2Short(bos.get());
            final int color = b3 & 0xF;
            final int ecode = b3 >> 4;
            int datapos = bos.position();
            int datarun;
            if (ecode == 15) {
                final byte bb1 = bos.get(datapos - 2);
                final byte bb2 = bos.get(datapos);
                if (color == 0 && bb1 == 0 && bb2 == 0) {
                    ++datapos;
                }
                bos.position(datapos);
                datarun = 0;
            }
            else if (ecode == 14) {
                final byte b4 = bos.get(datapos);
                datarun = this.convertunsignedByte2Short(b4) + 1;
                ++datapos;
                bos.position(datapos);
            }
            else if (ecode == 13) {
                b2[0] = bos.get(datapos);
                b2[1] = bos.get(datapos + 1);
                datarun = (this.convertunsignedByte2Short(b2[1]) << 8) + this.convertunsignedByte2Short(b2[0]) + 1;
                datapos += 2;
                bos.position(datapos);
            }
            else {
                datarun = ecode + 1;
            }
            rdata = new byte[datarun];
            for (int i = 0; i < datarun; ++i) {
                rdata[i] = (byte)color;
            }
            System.arraycopy(rdata, 0, ldata, roffset, datarun);
            roffset += datarun;
            final short c0 = this.convertunsignedByte2Short(bos.get());
            if (c0 == 0) {
                final short c2 = this.convertunsignedByte2Short(bos.get());
                final short c3 = this.convertunsignedByte2Short(bos.get());
                if (c0 == 0 && c2 == 240 && c3 == 12) {
                    System.arraycopy(ldata, 0, pdata, offset, roffset);
                    offset += vinfo.xt;
                    roffset = 0;
                    newline = true;
                    ldata = new byte[vinfo.xt];
                }
                else {
                    if (c2 == 240 && c3 == 2) {
                        break;
                    }
                    datapos = bos.position() - 3;
                    bos.position(datapos);
                    newline = false;
                }
            }
            else {
                newline = false;
                datapos = bos.position();
                bos.position(datapos - 1);
            }
        }
        return pdata;
    }
    
    int getUInt(final byte[] b, final int num) {
        int base = 1;
        int word = 0;
        final int[] bv = new int[num];
        for (int i = 0; i < num; ++i) {
            bv[i] = this.convertunsignedByte2Short(b[i]);
        }
        for (int i = num - 1; i >= 0; --i) {
            word += base * bv[i];
            base *= 256;
        }
        return word;
    }
    
    public static int bytesToInt(final short a, final short b, final boolean swapBytes) {
        if (swapBytes) {
            return (a & 0xFF) + (b << 8);
        }
        return (a << 8) + (b & 0xFF);
    }
    
    public static int bytesToInt(final byte a, final byte b, final boolean swapBytes) {
        if (swapBytes) {
            return (a & 0xFF) + (b << 8);
        }
        return (a << 8) + (b & 0xFF);
    }
    
    public byte[] readOneRowData(final byte[] ddata, final int rLen, final int xt) throws IOException, InvalidRangeException {
        final byte[] bdata = new byte[xt];
        int nbin = 0;
        int total = 0;
        for (int run = 0; run < rLen; ++run) {
            final int drun = this.convertunsignedByte2Short(ddata[run]) >> 4;
            final byte dcode1 = (byte)(this.convertunsignedByte2Short(ddata[run]) & 0xF);
            for (int i = 0; i < drun; ++i) {
                bdata[nbin++] = dcode1;
                ++total;
            }
        }
        if (total < xt) {
            for (int run = total; run < xt; ++run) {
                bdata[run] = 0;
            }
        }
        return bdata;
    }
    
    int getUInt(final byte[] b, final int offset, final int num) {
        int base = 1;
        int word = 0;
        final int[] bv = new int[num];
        for (int i = 0; i < num; ++i) {
            bv[i] = this.convertunsignedByte2Short(b[offset + i]);
        }
        for (int i = num - 1; i >= 0; --i) {
            word += base * bv[i];
            base *= 256;
        }
        return word;
    }
    
    int getInt(final byte[] b, final int offset, final int num) {
        int base = 1;
        int word = 0;
        final int[] bv = new int[num];
        for (int i = 0; i < num; ++i) {
            bv[i] = this.convertunsignedByte2Short(b[offset + i]);
        }
        if (bv[0] > 127) {
            final int[] array = bv;
            final int n = 0;
            array[n] -= 128;
            base = -1;
        }
        for (int i = num - 1; i >= 0; --i) {
            word += base * bv[i];
            base *= 256;
        }
        return word;
    }
    
    public short convertunsignedByte2Short(final byte b) {
        return (short)((b < 0) ? (b + 256) : ((short)b));
    }
    
    public static int unsignedByteToInt(final byte b) {
        return b & 0xFF;
    }
    
    public void flush() throws IOException {
        this.myRaf.flush();
    }
    
    @Override
    public void close() throws IOException {
        this.myRaf.close();
    }
    
    public static void main(final String[] args) throws Exception, IOException, InstantiationException, IllegalAccessException {
        final String fileIn = "z:/nowrad/BREF_951207_2230";
        NetcdfFile.registerIOProvider(NOWRadiosp.class);
        final NetcdfFile ncf = NetcdfFile.open(fileIn);
        final Variable v = ncf.findVariable("BaseReflectivity");
        final int[] origin = { 0, 0 };
        final int[] shape = { 300, 36 };
        final ArrayByte data = (ArrayByte)v.read(origin, shape);
        ncf.close();
    }
}
