// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.iosp.nowrad;

import org.slf4j.LoggerFactory;
import ucar.unidata.util.Parameter;
import ucar.unidata.geoloc.LatLonPoint;
import ucar.unidata.geoloc.LatLonPointImpl;
import ucar.unidata.geoloc.ProjectionPointImpl;
import ucar.unidata.geoloc.projection.LambertConformal;
import ucar.nc2.constants.FeatureType;
import ucar.ma2.Array;
import ucar.nc2.constants.AxisType;
import ucar.nc2.Attribute;
import ucar.ma2.DataType;
import ucar.nc2.Structure;
import ucar.nc2.Variable;
import ucar.nc2.Group;
import ucar.nc2.Dimension;
import java.util.ArrayList;
import java.util.List;
import ucar.unidata.geoloc.ProjectionImpl;
import java.util.Date;
import ucar.unidata.util.StringUtil;
import ucar.unidata.util.DateUtil;
import java.text.SimpleDateFormat;
import java.io.IOException;
import ucar.unidata.io.RandomAccessFile;
import ucar.nc2.NetcdfFile;
import ucar.nc2.units.DateFormatter;
import org.slf4j.Logger;

public class NOWRadheader
{
    static final int NEXET = 2;
    static final int NEXLH = 5;
    static final int NEXLL = 3;
    static final int NEXLM = 4;
    static final int NEXVI = 6;
    static final int NOWRADHF = 0;
    static final int USRADHF = 1;
    public static String[] mons;
    private static final boolean useStationDB = false;
    private static Logger log;
    DateFormatter formatter;
    private boolean isR;
    private String cmemo;
    private String ctilt;
    private String ctitle;
    private String cunit;
    private String cname;
    private NetcdfFile ncfile;
    private boolean noHeader;
    private int numX;
    private int numY;
    private RandomAccessFile raf;
    
    public NOWRadheader() {
        this.formatter = new DateFormatter();
        this.isR = false;
    }
    
    public boolean isValidFile(final RandomAccessFile raf) {
        try {
            final long t = raf.length();
            if (t == 0L) {
                throw new IOException("zero length file ");
            }
        }
        catch (IOException e) {
            return false;
        }
        try {
            final int p = this.readTop(raf);
            if (p == 0) {
                return false;
            }
        }
        catch (IOException e) {
            return false;
        }
        return true;
    }
    
    int readTop(final RandomAccessFile raf) throws IOException {
        final int pos = 0;
        raf.seek(pos);
        final int readLen = 35;
        int rc = 0;
        final byte[] b = new byte[readLen];
        rc = raf.read(b);
        if (rc != readLen) {
            return 0;
        }
        if (this.convertunsignedByte2Short(b[0]) != 0 || this.convertunsignedByte2Short(b[1]) != 240 || this.convertunsignedByte2Short(b[2]) != 9) {
            return 0;
        }
        final int hsize = b[3];
        final String pidd = new String(b, 15, 5);
        if (pidd.contains("NOWRA") || pidd.contains("USRAD") || pidd.contains("NEX")) {
            return 1;
        }
        return 0;
    }
    
    public byte[] getData(final int offset) throws Exception {
        final int readLen = (int)this.raf.length();
        final byte[] b = new byte[readLen];
        final int pos = 0;
        this.raf.seek(pos);
        final int rc = this.raf.read(b);
        return b;
    }
    
    public void setProperty(final String name, final String value) {
    }
    
    void read(final RandomAccessFile raf, final NetcdfFile ncfile) throws Exception {
        this.raf = raf;
        int hoffset = 0;
        final int readLen = 250;
        this.ncfile = ncfile;
        final int pos = 0;
        raf.seek(pos);
        final byte[] b = new byte[readLen];
        final int rc = raf.read(b);
        if (rc != readLen) {
            NOWRadheader.log.warn(" error reading nids product header " + raf.getLocation());
        }
        final int hsize = b[3];
        final String product = new String(b, 15, 8);
        final int imgres = b[4 + hsize];
        final String head = new String(b);
        byte[] bt = { -16, 10 };
        String t0 = new String(bt);
        int t2 = head.indexOf(t0);
        final String lstr = this.trim(new String(b, t2 + 2, 4));
        this.numY = Integer.parseInt(lstr);
        final String estr = this.trim(new String(b, t2 + 6, 5));
        this.numX = Integer.parseInt(estr);
        bt = new byte[] { -16, 3 };
        t0 = new String(bt);
        t2 = head.indexOf(t0);
        final String ss = new String(b, t2 + 2, 20);
        int off = 0;
        if (product.contains("USRADHF")) {
            off = 3;
        }
        String ts = new String(b, t2 + 22 + off, 2);
        final int hr = Integer.parseInt(ts);
        ts = new String(b, t2 + 25 + off, 2);
        final int min = Integer.parseInt(ts);
        ts = new String(b, t2 + 28 + off, 2);
        final int dd = Integer.parseInt(ts);
        final String mon;
        ts = (mon = new String(b, t2 + 31 + off, 3));
        final int month = this.getMonth(mon);
        ts = new String(b, t2 + 35 + off, 2);
        final int year = Integer.parseInt(ts);
        final SimpleDateFormat sdf = new SimpleDateFormat();
        sdf.setTimeZone(DateUtil.TIMEZONE_GMT);
        sdf.applyPattern("yyyy/MM/dd HH:mm");
        final Date date = sdf.parse(year + "/" + month + "/" + dd + " " + hr + ":" + min);
        String ot = new String(b, t2 + 40, 45);
        bt = new byte[] { -16, 11 };
        t0 = new String(bt);
        t2 = head.indexOf(t0);
        final ProjectionImpl projection = null;
        if (product.contains("NOWRAD")) {
            ot = new String(b, t2 + 2, 68);
            final List<String> toks = StringUtil.split(ot, " ", true, true);
            final String pj = toks.get(0);
            final double nav1 = Math.toDegrees(Double.parseDouble(toks.get(1)));
            final double nav2 = Math.toDegrees(Double.parseDouble(toks.get(2)));
            final double nav3 = Math.toDegrees(Double.parseDouble(toks.get(3)));
            final double nav4 = Math.toDegrees(Double.parseDouble(toks.get(4)));
            final double nav5 = Math.toDegrees(Double.parseDouble(toks.get(5)));
            final float rlat1 = (float)(nav2 - (this.numY - 1) * nav4);
            final float rlon1 = (float)(nav1 + nav3);
            final float rlat2 = (float)nav2;
            final float rlon2 = (float)(nav1 - nav3);
            hoffset = t2 + 71;
            if (this.convertunsignedByte2Short(b[172 + hsize]) != 240 || this.convertunsignedByte2Short(b[173 + hsize]) != 12) {
                return;
            }
            this.setProductInfo(product, date);
            this.nowrad(hoffset, rlat1, rlon1, rlat2, rlon2, (float)nav4, (float)nav5, date);
        }
        else if (product.contains("USRADHF")) {
            ot = new String(b, t2 + 2, 107);
            final List<String> toks = StringUtil.split(ot, " ", true, true);
            final String pj = toks.get(0);
            final double nav1 = Math.toDegrees(Double.parseDouble(toks.get(1)));
            final double nav2 = Math.toDegrees(Double.parseDouble(toks.get(2)));
            final double nav3 = Math.toDegrees(Double.parseDouble(toks.get(3)));
            final double nav4 = Math.toDegrees(Double.parseDouble(toks.get(4)));
            final double nav5 = Math.toDegrees(Double.parseDouble(toks.get(5)));
            final double nav6 = Math.toDegrees(Double.parseDouble(toks.get(6)));
            final double nav7 = Math.toDegrees(Double.parseDouble(toks.get(7)));
            final double nav8 = Math.toDegrees(Double.parseDouble(toks.get(8)));
            hoffset = t2 + 110;
            if (this.convertunsignedByte2Short(b[t2 + 110]) != 240 || this.convertunsignedByte2Short(b[t2 + 111]) != 12) {
                return;
            }
            this.setProductInfo(product, date);
            this.nowradL(hoffset, (float)nav1, (float)nav2, (float)nav3, (float)nav4, (float)nav5, (float)nav6, date);
        }
        ncfile.finish();
    }
    
    String trim(final String str) {
        final int len = str.length();
        final StringBuffer ostr = new StringBuffer();
        for (int i = 0; i < len; ++i) {
            final char sc = str.charAt(i);
            if (Character.isDigit(sc)) {
                ostr.append(sc);
            }
        }
        return ostr.toString();
    }
    
    int getMonth(final String m) {
        for (int i = 0; i < 12; ++i) {
            if (m.equalsIgnoreCase(NOWRadheader.mons[i])) {
                return i + 1;
            }
        }
        return 0;
    }
    
    ProjectionImpl nowradL(final int hoff, final float lat1, final float lat2, final float clat, final float clon, final float lat, final float lon, final Date dd) {
        final ArrayList dims = new ArrayList();
        final Dimension dimT = new Dimension("time", 1, true, false, false);
        this.ncfile.addDimension(null, dimT);
        final String timeCoordName = "time";
        final Variable taxis = new Variable(this.ncfile, null, null, timeCoordName);
        taxis.setDataType(DataType.DOUBLE);
        taxis.setDimensions("time");
        taxis.addAttribute(new Attribute("long_name", "time since base date"));
        taxis.addAttribute(new Attribute("_CoordinateAxisType", AxisType.Time.toString()));
        final double[] tdata = { (double)dd.getTime() };
        final Array dataT = Array.factory(DataType.DOUBLE.getPrimitiveClassType(), new int[] { 1 }, tdata);
        taxis.setCachedData(dataT, false);
        final DateFormatter formatter = new DateFormatter();
        taxis.addAttribute(new Attribute("units", "msecs since " + formatter.toDateTimeStringISO(new Date(0L))));
        this.ncfile.addVariable(null, taxis);
        dims.add(dimT);
        final Dimension jDim = new Dimension("y", this.numY, true, false, false);
        final Dimension iDim = new Dimension("x", this.numX, true, false, false);
        dims.add(jDim);
        dims.add(iDim);
        this.ncfile.addDimension(null, iDim);
        this.ncfile.addDimension(null, jDim);
        this.ncfile.addAttribute(null, new Attribute("cdm_data_type", FeatureType.GRID.toString()));
        final String coordinates = "time y x";
        final Variable v = new Variable(this.ncfile, null, null, this.cname);
        v.setDataType(DataType.BYTE);
        v.setDimensions(dims);
        this.ncfile.addVariable(null, v);
        v.addAttribute(new Attribute("long_name", this.ctitle));
        v.addAttribute(new Attribute("units", this.cunit));
        v.setSPobject(new Vinfo(this.numX, this.numY, hoff, false));
        v.addAttribute(new Attribute("_CoordinateAxes", coordinates));
        final Variable xaxis = new Variable(this.ncfile, null, null, "x");
        xaxis.setDataType(DataType.DOUBLE);
        xaxis.setDimensions("x");
        xaxis.addAttribute(new Attribute("standard_name", "projection x coordinate"));
        xaxis.addAttribute(new Attribute("units", "km"));
        xaxis.addAttribute(new Attribute("_CoordinateAxisType", "GeoX"));
        double[] data1 = new double[this.numX];
        final ProjectionImpl projection = new LambertConformal(clat, clon, lat1, lat2);
        final double ullat = 51.8294;
        final double ullon = -135.8736;
        final double lrlat = 17.2454;
        final double lrlon = -70.1154;
        final ProjectionPointImpl ptul = (ProjectionPointImpl)projection.latLonToProj(new LatLonPointImpl(ullat, ullon));
        final ProjectionPointImpl ptlr = (ProjectionPointImpl)projection.latLonToProj(new LatLonPointImpl(lrlat, lrlon));
        final ProjectionPointImpl ptc = (ProjectionPointImpl)projection.latLonToProj(new LatLonPointImpl(clat, clon));
        final double startX = ptul.getX();
        final double startY = ptlr.getY();
        final double dx = (ptlr.getX() - ptul.getX()) / (this.numX - 1);
        for (int i = 0; i < this.numX; ++i) {
            data1[i] = startX + i * dx;
        }
        Array dataA = Array.factory(DataType.DOUBLE.getPrimitiveClassType(), new int[] { this.numX }, data1);
        xaxis.setCachedData(dataA, false);
        this.ncfile.addVariable(null, xaxis);
        final Variable yaxis = new Variable(this.ncfile, null, null, "y");
        yaxis.setDataType(DataType.DOUBLE);
        yaxis.setDimensions("y");
        yaxis.addAttribute(new Attribute("standard_name", "projection y coordinate"));
        yaxis.addAttribute(new Attribute("units", "km"));
        yaxis.addAttribute(new Attribute("_CoordinateAxisType", "GeoY"));
        data1 = new double[this.numY];
        final double dy = (ptul.getY() - ptlr.getY()) / (this.numY - 1);
        for (int j = 0; j < this.numY; ++j) {
            data1[j] = startY + j * dy;
        }
        dataA = Array.factory(DataType.DOUBLE.getPrimitiveClassType(), new int[] { this.numY }, data1);
        yaxis.setCachedData(dataA, false);
        this.ncfile.addVariable(null, yaxis);
        final Variable ct = new Variable(this.ncfile, null, null, projection.getClassName());
        ct.setDataType(DataType.CHAR);
        ct.setDimensions("");
        final List params = projection.getProjectionParameters();
        for (int k = 0; k < params.size(); ++k) {
            final Parameter p = params.get(k);
            ct.addAttribute(new Attribute(p));
        }
        ct.addAttribute(new Attribute("_CoordinateTransformType", "Projection"));
        ct.addAttribute(new Attribute("_CoordinateAxes", "x y "));
        dataA = Array.factory(DataType.CHAR.getPrimitiveClassType(), new int[0]);
        dataA.setChar(dataA.getIndex(), ' ');
        ct.setCachedData(dataA, false);
        this.ncfile.addVariable(null, ct);
        return projection;
    }
    
    ProjectionImpl nowrad(final int hoff, final float rlat1, final float rlon1, final float rlat2, final float rlon2, final float dlat, final float dlon, final Date dd) {
        final ArrayList dims = new ArrayList();
        final Dimension dimT = new Dimension("time", 1, true, false, false);
        this.ncfile.addDimension(null, dimT);
        final String timeCoordName = "time";
        final Variable taxis = new Variable(this.ncfile, null, null, timeCoordName);
        taxis.setDataType(DataType.DOUBLE);
        taxis.setDimensions("time");
        taxis.addAttribute(new Attribute("long_name", "time since base date"));
        taxis.addAttribute(new Attribute("_CoordinateAxisType", AxisType.Time.toString()));
        final double[] tdata = { (double)dd.getTime() };
        final Array dataT = Array.factory(DataType.DOUBLE.getPrimitiveClassType(), new int[] { 1 }, tdata);
        taxis.setCachedData(dataT, false);
        final DateFormatter formatter = new DateFormatter();
        taxis.addAttribute(new Attribute("units", "msecs since " + formatter.toDateTimeStringISO(new Date(0L))));
        this.ncfile.addVariable(null, taxis);
        dims.add(dimT);
        final Dimension jDim = new Dimension("lat", this.numY, true, false, false);
        final Dimension iDim = new Dimension("lon", this.numX, true, false, false);
        dims.add(jDim);
        dims.add(iDim);
        this.ncfile.addDimension(null, iDim);
        this.ncfile.addDimension(null, jDim);
        this.ncfile.addAttribute(null, new Attribute("cdm_data_type", FeatureType.GRID.toString()));
        final String coordinates = "time lat lon";
        final Variable v = new Variable(this.ncfile, null, null, this.cname);
        v.setDataType(DataType.BYTE);
        v.setDimensions(dims);
        this.ncfile.addVariable(null, v);
        v.addAttribute(new Attribute("long_name", this.ctitle));
        v.addAttribute(new Attribute("units", this.cunit));
        v.setSPobject(new Vinfo(this.numX, this.numY, hoff, false));
        v.addAttribute(new Attribute("_CoordinateAxes", coordinates));
        final Variable xaxis = new Variable(this.ncfile, null, null, "lon");
        xaxis.setDataType(DataType.DOUBLE);
        xaxis.setDimensions("lon");
        xaxis.addAttribute(new Attribute("long_name", "longitude"));
        xaxis.addAttribute(new Attribute("units", "degree"));
        xaxis.addAttribute(new Attribute("_CoordinateAxisType", "Lon"));
        double[] data1 = new double[this.numX];
        for (int i = 0; i < this.numX; ++i) {
            data1[i] = rlon1 + i * dlon;
        }
        Array dataA = Array.factory(DataType.DOUBLE.getPrimitiveClassType(), new int[] { this.numX }, data1);
        xaxis.setCachedData(dataA, false);
        this.ncfile.addVariable(null, xaxis);
        final Variable yaxis = new Variable(this.ncfile, null, null, "lat");
        yaxis.setDataType(DataType.DOUBLE);
        yaxis.setDimensions("lat");
        yaxis.addAttribute(new Attribute("long_name", "latitude"));
        yaxis.addAttribute(new Attribute("units", "degree"));
        yaxis.addAttribute(new Attribute("_CoordinateAxisType", "Lat"));
        data1 = new double[this.numY];
        for (int j = 0; j < this.numY; ++j) {
            data1[j] = rlat1 + j * dlat;
        }
        dataA = Array.factory(DataType.DOUBLE.getPrimitiveClassType(), new int[] { this.numY }, data1);
        yaxis.setCachedData(dataA, false);
        this.ncfile.addVariable(null, yaxis);
        return null;
    }
    
    void setProductInfo(final String prod, final Date dd) {
        int radial = 0;
        String summary = null;
        if (prod.contains("NOWRADHF")) {
            radial = 0;
            this.cmemo = "NOWRAD  Base Reflectivity at Tilt 1";
            this.ctitle = "BREF: Base Reflectivity [dBZ]";
            this.cunit = "dBZ";
            this.cname = "Reflectivity";
            summary = "NOWRAD Product";
        }
        else if (prod.contains("USRADHF")) {
            radial = 0;
            this.cmemo = "NOWRAD  Base Reflectivity at Tilt 1";
            this.ctitle = "BREF: Base Reflectivity [dBZ]";
            this.cunit = "dBZ";
            this.cname = "Reflectivity";
            summary = "NOWRAD Product";
        }
        else if (prod.contains("NEXET")) {
            radial = 0;
            this.cmemo = "NOWRAD Echo Tops";
            this.ctitle = "Echo Tops Composite";
            this.cunit = "K FT";
            this.cname = "EchoTopsComposite";
            summary = "NOWRAD Product";
        }
        else if (prod.contains("NEXLL")) {
            radial = 0;
            this.cmemo = "NOWRAD Layer Comp. Reflectivity - Low";
            this.ctitle = "LayerReflectivityLow";
            this.cunit = "dBZ";
            this.cname = "Reflectivity";
            summary = "NOWRAD Product";
        }
        else if (prod.contains("NEXLM")) {
            radial = 0;
            this.cmemo = "NOWRAD Layer Comp. Reflectivity - Mid";
            this.ctitle = "LayerReflectivityMid";
            this.cunit = "dBZ";
            this.cname = "Reflectivity";
            summary = "NOWRAD Product";
        }
        else if (prod.contains("NEXLH")) {
            radial = 0;
            this.cmemo = "NOWRAD Layer Comp. Reflectivity - High";
            this.ctitle = "LayerReflectivityHigh";
            this.cunit = "dBZ";
            this.cname = "ReflectivityHigh";
            summary = "NOWRAD Product";
        }
        else if (prod.contains("NEXVI")) {
            radial = 0;
            this.cmemo = "NOWRAD ";
            this.ctitle = "Vert. Integrated Liquid Water";
            this.cunit = "Knots";
            this.cname = "VILwater";
            summary = "NOWRAD ";
        }
        else {
            this.ctilt = "error";
            this.ctitle = "error";
            this.cunit = "error";
            this.cname = "error";
        }
        this.ncfile.addAttribute(null, new Attribute("summary", "NOWRAD radar composite products." + summary));
        this.ncfile.addAttribute(null, new Attribute("title", "NOWRAD"));
        this.ncfile.addAttribute(null, new Attribute("keywords", "NOWRAD"));
        this.ncfile.addAttribute(null, new Attribute("creator_name", "NOAA/NWS"));
        this.ncfile.addAttribute(null, new Attribute("creator_url", "http://www.ncdc.noaa.gov/oa/radar/radarproducts.html"));
        this.ncfile.addAttribute(null, new Attribute("naming_authority", "NOAA/NCDC"));
        this.ncfile.addAttribute(null, new Attribute("base_date", this.formatter.toDateOnlyString(dd)));
        this.ncfile.addAttribute(null, new Attribute("conventions", "_Coordinates"));
        this.ncfile.addAttribute(null, new Attribute("cdm_data_type", FeatureType.GRID.toString()));
    }
    
    public static int shortsToInt(final short s1, final short s2, final boolean swapBytes) {
        final byte[] b = { (byte)(s1 >>> 8), (byte)(s1 >>> 0), (byte)(s2 >>> 8), (byte)(s2 >>> 0) };
        return bytesToInt(b, false);
    }
    
    public static int bytesToInt(final byte[] bytes, final boolean swapBytes) {
        final byte a = bytes[0];
        final byte b = bytes[1];
        final byte c = bytes[2];
        final byte d = bytes[3];
        if (swapBytes) {
            return (a & 0xFF) + ((b & 0xFF) << 8) + ((c & 0xFF) << 16) + ((d & 0xFF) << 24);
        }
        return ((a & 0xFF) << 24) + ((b & 0xFF) << 16) + ((c & 0xFF) << 8) + (d & 0xFF);
    }
    
    int getUInt(final byte[] b, final int num) {
        int base = 1;
        int word = 0;
        final int[] bv = new int[num];
        for (int i = 0; i < num; ++i) {
            bv[i] = this.convertunsignedByte2Short(b[i]);
        }
        for (int i = num - 1; i >= 0; --i) {
            word += base * bv[i];
            base *= 256;
        }
        return word;
    }
    
    int getInt(final byte[] b, final int num) {
        int base = 1;
        int word = 0;
        final int[] bv = new int[num];
        for (int i = 0; i < num; ++i) {
            bv[i] = this.convertunsignedByte2Short(b[i]);
        }
        if (bv[0] > 127) {
            final int[] array = bv;
            final int n = 0;
            array[n] -= 128;
            base = -1;
        }
        for (int i = num - 1; i >= 0; --i) {
            word += base * bv[i];
            base *= 256;
        }
        return word;
    }
    
    public static int bytesToInt(final byte a, final byte b, final boolean swapBytes) {
        if (swapBytes) {
            return (a & 0xFF) + (b << 8);
        }
        return (a << 8) + (b & 0xFF);
    }
    
    public short convertunsignedByte2Short(final byte b) {
        return (short)((b < 0) ? (b + 256) : ((short)b));
    }
    
    public int convertShort2unsignedInt(final short b) {
        return (b < 0) ? (-1 * b + 32768) : b;
    }
    
    public static Date getDate(final int julianDays, final int msecs) {
        final long total = (julianDays - 1) * 24L * 3600L * 1000L + msecs;
        return new Date(total);
    }
    
    public void flush() throws IOException {
        this.raf.flush();
    }
    
    public void close() throws IOException {
        if (this.raf != null) {
            this.raf.close();
        }
    }
    
    static {
        NOWRadheader.mons = new String[] { "JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC" };
        NOWRadheader.log = LoggerFactory.getLogger(NOWRadheader.class);
    }
    
    class Vinfo
    {
        long hoff;
        boolean isRadial;
        int xt;
        int yt;
        
        Vinfo(final int xt, final int yt, final long hoff, final boolean isRadial) {
            this.xt = xt;
            this.yt = yt;
            this.hoff = hoff;
            this.isRadial = isRadial;
        }
    }
}
