// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.iosp;

public class IndexLong
{
    private int[] shape;
    private long[] stride;
    private int rank;
    private int offset;
    private int[] current;
    
    public IndexLong() {
        this.shape = new int[] { 1 };
        this.stride = new long[] { 1L };
        this.rank = this.shape.length;
        this.current = new int[this.rank];
        this.stride[0] = 1L;
        this.offset = 0;
    }
    
    public IndexLong(final int[] _shape, final long[] _stride) {
        System.arraycopy(_shape, 0, this.shape = new int[_shape.length], 0, _shape.length);
        System.arraycopy(_stride, 0, this.stride = new long[_stride.length], 0, _stride.length);
        this.rank = this.shape.length;
        this.current = new int[this.rank];
        this.offset = 0;
    }
    
    public static long computeSize(final int[] shape) {
        long product = 1L;
        for (int ii = shape.length - 1; ii >= 0; --ii) {
            product *= shape[ii];
        }
        return product;
    }
    
    public long incr() {
        for (int digit = this.rank - 1; digit >= 0; --digit) {
            final int[] current = this.current;
            final int n = digit;
            ++current[n];
            if (this.current[digit] < this.shape[digit]) {
                break;
            }
            this.current[digit] = 0;
        }
        return this.currentElement();
    }
    
    public long currentElement() {
        long value = this.offset;
        for (int ii = 0; ii < this.rank; ++ii) {
            value += this.current[ii] * this.stride[ii];
        }
        return value;
    }
}
