// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.iosp.cinrad;

import org.slf4j.LoggerFactory;
import ucar.nc2.NetcdfFile;
import java.io.FileInputStream;
import ucar.unidata.io.bzip2.BZip2ReadException;
import java.io.InputStream;
import java.io.ByteArrayInputStream;
import java.io.EOFException;
import ucar.unidata.io.bzip2.CBZip2InputStream;
import java.util.Comparator;
import java.util.Collections;
import java.util.HashMap;
import java.util.StringTokenizer;
import java.io.IOException;
import java.io.File;
import java.util.Date;
import java.util.List;
import ucar.nc2.util.DiskCache;
import ucar.nc2.util.CancelTask;
import java.util.ArrayList;
import ucar.nc2.iosp.nexrad2.NexradStationDB;
import ucar.unidata.io.RandomAccessFile;
import org.slf4j.Logger;

public class Cinrad2VolumeScan
{
    public static final String ARCHIVE2 = "ARCHIVE2";
    public static final String AR2V0001 = "AR2V0001";
    private static Logger log;
    RandomAccessFile raf;
    private String dataFormat;
    private String volumeNo;
    private int title_julianDay;
    private int title_msecs;
    private String stationId;
    private NexradStationDB.Station station;
    private Cinrad2Record first;
    private Cinrad2Record last;
    private int vcp;
    private int max_radials;
    private int min_radials;
    private int dopplarResolution;
    private boolean hasDifferentDopplarResolutions;
    private ArrayList reflectivityGroups;
    private ArrayList dopplerGroups;
    private boolean showMessages;
    private boolean showData;
    private boolean debugScans;
    private boolean debugGroups2;
    private boolean debugRadials;
    private int MAX_RADIAL;
    private int[] radial;
    
    Cinrad2VolumeScan(final RandomAccessFile orgRaf, final CancelTask cancelTask) throws IOException {
        this.dataFormat = null;
        this.volumeNo = null;
        this.vcp = 0;
        this.max_radials = 0;
        this.min_radials = Integer.MAX_VALUE;
        this.showMessages = false;
        this.showData = false;
        this.debugScans = false;
        this.debugGroups2 = false;
        this.debugRadials = false;
        this.MAX_RADIAL = 401;
        this.radial = new int[this.MAX_RADIAL];
        this.raf = orgRaf;
        final boolean debug = Cinrad2VolumeScan.log.isDebugEnabled();
        if (debug) {
            Cinrad2VolumeScan.log.debug("Cinrad2VolumeScan on " + this.raf.getLocation());
        }
        this.raf.seek(0L);
        this.raf.order(1);
        final String loc = this.raf.getLocation();
        this.stationId = this.getStationID(loc);
        this.dataFormat = this.raf.readString(8);
        this.raf.skipBytes(1);
        this.volumeNo = this.raf.readString(3);
        this.title_julianDay = this.raf.readInt();
        this.title_msecs = this.raf.readInt();
        if (debug) {
            Cinrad2VolumeScan.log.debug(" dataFormat= " + this.dataFormat + " stationId= " + this.stationId);
        }
        if (this.stationId.length() == 0) {
            this.stationId = null;
        }
        if (this.stationId != null) {
            this.station = NexradStationDB.get("K" + this.stationId);
            this.dataFormat = "CINRAD-SA";
        }
        if (this.dataFormat.equals("AR2V0001")) {
            this.raf.skipBytes(4);
            final String BZ = this.raf.readString(2);
            if (BZ.equals("BZ")) {
                RandomAccessFile uraf = null;
                final File uncompressedFile = DiskCache.getFileStandardPolicy(this.raf.getLocation() + ".uncompress");
                if (uncompressedFile.exists()) {
                    uraf = new RandomAccessFile(uncompressedFile.getPath(), "r");
                }
                else {
                    uraf = this.uncompress(this.raf, uncompressedFile.getPath(), debug);
                    uraf.flush();
                    if (debug) {
                        Cinrad2VolumeScan.log.debug("flushed uncompressed file= " + uncompressedFile.getPath());
                    }
                }
                this.raf.close();
                (this.raf = uraf).order(0);
            }
            this.raf.seek(0L);
        }
        final ArrayList reflectivity = new ArrayList();
        final ArrayList doppler = new ArrayList();
        int recno = 0;
        while (true) {
            final Cinrad2Record r = Cinrad2Record.factory(this.raf, recno++);
            if (r == null) {
                if (this.debugRadials) {
                    System.out.println(" reflect ok= " + reflectivity.size() + " doppler ok= " + doppler.size());
                }
                this.reflectivityGroups = this.sortScans("reflect", reflectivity);
                this.dopplerGroups = this.sortScans("doppler", doppler);
                return;
            }
            if (r.message_type != 1) {
                if (!this.showMessages) {
                    continue;
                }
                r.dumpMessage(System.out, null);
            }
            else {
                if (this.showData) {
                    r.dump2(System.out);
                }
                if (this.vcp == 0) {
                    this.vcp = r.vcp;
                }
                if (this.first == null) {
                    this.first = r;
                }
                this.last = r;
                if (!r.checkOk()) {
                    continue;
                }
                if (r.hasReflectData) {
                    reflectivity.add(r);
                }
                if (r.hasDopplerData) {
                    doppler.add(r);
                }
                if (cancelTask != null && cancelTask.isCancel()) {
                    return;
                }
                continue;
            }
        }
    }
    
    public String getStationID(final String location) {
        int posFirst = location.lastIndexOf(47) + 1;
        if (posFirst < 0) {
            posFirst = 0;
        }
        final String stationID = location.substring(posFirst, posFirst + 4);
        return stationID;
    }
    
    private static double parseDegree(final String s) {
        final StringTokenizer stoke = new StringTokenizer(s, ":");
        final String degS = stoke.nextToken();
        final String minS = stoke.nextToken();
        final String secS = stoke.nextToken();
        try {
            final double deg = Double.parseDouble(degS);
            final double min = Double.parseDouble(minS);
            final double sec = Double.parseDouble(secS);
            if (deg < 0.0) {
                return deg - min / 60.0 - sec / 3600.0;
            }
            return deg + min / 60.0 + sec / 3600.0;
        }
        catch (NumberFormatException e) {
            e.printStackTrace();
            return 0.0;
        }
    }
    
    private ArrayList sortScans(final String name, final List scans) {
        final HashMap groupHash = new HashMap(600);
        for (int i = 0; i < scans.size(); ++i) {
            final Cinrad2Record record = scans.get(i);
            final Integer groupNo = new Integer(record.elevation_num);
            ArrayList group = groupHash.get(groupNo);
            if (null == group) {
                group = new ArrayList();
                groupHash.put(groupNo, group);
            }
            group.add(record);
        }
        final ArrayList groups = new ArrayList(groupHash.values());
        Collections.sort((List<Object>)groups, new GroupComparator());
        for (int j = 0; j < groups.size(); ++j) {
            final ArrayList group2 = groups.get(j);
            this.testScan(name, group2);
            this.max_radials = Math.max(this.max_radials, group2.size());
            this.min_radials = Math.min(this.min_radials, group2.size());
        }
        if (this.debugRadials) {
            System.out.println(name + " min_radials= " + this.min_radials + " max_radials= " + this.max_radials);
            for (int j = 0; j < groups.size(); ++j) {
                final ArrayList group2 = groups.get(j);
                Cinrad2Record lastr = group2.get(0);
                for (int k = 1; k < group2.size(); ++k) {
                    final Cinrad2Record r = group2.get(k);
                    if (r.data_msecs < lastr.data_msecs) {
                        System.out.println(" out of order " + k);
                    }
                    lastr = r;
                }
            }
        }
        this.testVariable(name, groups);
        if (this.debugScans) {
            System.out.println("-----------------------------");
        }
        return groups;
    }
    
    public int getMaxRadials() {
        return this.max_radials;
    }
    
    public int getMinRadials() {
        return this.min_radials;
    }
    
    public int getDopplarResolution() {
        return this.dopplarResolution;
    }
    
    public boolean hasDifferentDopplarResolutions() {
        return this.hasDifferentDopplarResolutions;
    }
    
    private boolean testScan(final String name, final ArrayList group) {
        final int datatype = name.equals("reflect") ? 1 : 2;
        final Cinrad2Record first = group.get(0);
        final int n = group.size();
        if (this.debugScans) {
            final boolean hasBoth = first.hasDopplerData && first.hasReflectData;
            System.out.println(name + " " + first + " has " + n + " radials resolution= " + first.resolution + " has both = " + hasBoth);
        }
        boolean ok = true;
        double sum = 0.0;
        double sum2 = 0.0;
        for (int i = 0; i < this.MAX_RADIAL; ++i) {
            this.radial[i] = 0;
        }
        for (int i = 0; i < group.size(); ++i) {
            final Cinrad2Record r = group.get(i);
            if (r.getGateSize(datatype) != first.getGateSize(datatype)) {
                Cinrad2VolumeScan.log.warn(this.raf.getLocation() + " different gate size (" + r.getGateSize(datatype) + ") in record " + name + " " + r);
                ok = false;
            }
            if (r.getGateStart(datatype) != first.getGateStart(datatype)) {
                Cinrad2VolumeScan.log.warn(this.raf.getLocation() + " different gate start (" + r.getGateStart(datatype) + ") in record " + name + " " + r);
                ok = false;
            }
            if (r.resolution != first.resolution) {
                Cinrad2VolumeScan.log.warn(this.raf.getLocation() + " different resolution (" + r.resolution + ") in record " + name + " " + r);
                ok = false;
            }
            if (r.radial_num < 0 || r.radial_num >= this.MAX_RADIAL) {
                Cinrad2VolumeScan.log.info(this.raf.getLocation() + " radial out of range= " + r.radial_num + " in record " + name + " " + r);
            }
            else {
                if (this.radial[r.radial_num] > 0) {
                    Cinrad2VolumeScan.log.warn(this.raf.getLocation() + " duplicate radial = " + r.radial_num + " in record " + name + " " + r);
                    ok = false;
                }
                this.radial[r.radial_num] = r.recno + 1;
                sum += r.getElevation();
                sum2 += r.getElevation() * r.getElevation();
            }
        }
        int i = 1;
        while (i < this.radial.length) {
            if (0 == this.radial[i]) {
                if (n != i - 1) {
                    Cinrad2VolumeScan.log.warn(" missing radial(s)");
                    ok = false;
                    break;
                }
                break;
            }
            else {
                ++i;
            }
        }
        final double avg = sum / n;
        final double sd = Math.sqrt((n * sum2 - sum * sum) / (n * (n - 1)));
        return ok;
    }
    
    private boolean testVariable(final String name, final List scans) {
        final int datatype = name.equals("reflect") ? 1 : 2;
        if (scans.size() == 0) {
            Cinrad2VolumeScan.log.warn(" No data for = " + name);
            return false;
        }
        boolean ok = true;
        final List firstScan = scans.get(0);
        final Cinrad2Record firstRecord = firstScan.get(0);
        this.dopplarResolution = firstRecord.resolution;
        if (this.debugGroups2) {
            System.out.println("Group " + Cinrad2Record.getDatatypeName(datatype) + " ngates = " + firstRecord.getGateCount(datatype) + " start = " + firstRecord.getGateStart(datatype) + " size = " + firstRecord.getGateSize(datatype));
        }
        for (int i = 1; i < scans.size(); ++i) {
            final List scan = scans.get(i);
            final Cinrad2Record record = scan.get(0);
            if (datatype == 2 && record.resolution != firstRecord.resolution) {
                Cinrad2VolumeScan.log.warn(name + " scan " + i + " diff resolutions = " + record.resolution + ", " + firstRecord.resolution + " elev= " + record.elevation_num + " " + record.getElevation());
                ok = false;
                this.hasDifferentDopplarResolutions = true;
            }
            if (record.getGateSize(datatype) != firstRecord.getGateSize(datatype)) {
                Cinrad2VolumeScan.log.warn(name + " scan " + i + " diff gates size = " + record.getGateSize(datatype) + " " + firstRecord.getGateSize(datatype) + " elev= " + record.elevation_num + " " + record.getElevation());
                ok = false;
            }
            else if (this.debugGroups2) {
                System.out.println(" ok gates size elev= " + record.elevation_num + " " + record.getElevation());
            }
            if (record.getGateStart(datatype) != firstRecord.getGateStart(datatype)) {
                Cinrad2VolumeScan.log.warn(name + " scan " + i + " diff gates start = " + record.getGateStart(datatype) + " " + firstRecord.getGateStart(datatype) + " elev= " + record.elevation_num + " " + record.getElevation());
                ok = false;
            }
            else if (this.debugGroups2) {
                System.out.println(" ok gates start elev= " + record.elevation_num + " " + record.getElevation());
            }
        }
        return ok;
    }
    
    public List getReflectivityGroups() {
        return this.reflectivityGroups;
    }
    
    public List getVelocityGroups() {
        return this.dopplerGroups;
    }
    
    public String getDataFormat() {
        return this.dataFormat;
    }
    
    public int getTitleJulianDays() {
        return this.title_julianDay;
    }
    
    public int getTitleMsecs() {
        return this.title_msecs;
    }
    
    public int getVCP() {
        return this.vcp;
    }
    
    public String getStationId() {
        return this.stationId;
    }
    
    public String getStationName() {
        return (this.station == null) ? "unknown" : this.station.name;
    }
    
    public double getStationLatitude() {
        return (this.station == null) ? 0.0 : this.station.lat;
    }
    
    public double getStationLongitude() {
        return (this.station == null) ? 0.0 : this.station.lon;
    }
    
    public double getStationElevation() {
        return (this.station == null) ? 0.0 : this.station.elev;
    }
    
    public Date getStartDate() {
        return this.first.getDate();
    }
    
    public Date getEndDate() {
        return this.last.getDate();
    }
    
    private RandomAccessFile uncompress(final RandomAccessFile raf2, final String ufilename, final boolean debug) throws IOException {
        raf2.seek(0L);
        final byte[] header = new byte[0];
        raf2.read(header);
        final RandomAccessFile dout2 = new RandomAccessFile(ufilename, "rw");
        dout2.write(header);
        boolean eof = false;
        final byte[] ubuff = new byte[40000];
        byte[] obuff = new byte[40000];
        try {
            final CBZip2InputStream cbzip2 = new CBZip2InputStream();
            while (!eof) {
                int numCompBytes;
                try {
                    numCompBytes = raf2.readInt();
                    if (numCompBytes == -1) {
                        if (debug) {
                            Cinrad2VolumeScan.log.debug("  done: numCompBytes=-1 ");
                        }
                        break;
                    }
                }
                catch (EOFException ee) {
                    if (debug) {
                        Cinrad2VolumeScan.log.debug("  got EOFException ");
                    }
                    break;
                }
                if (debug) {
                    Cinrad2VolumeScan.log.debug("reading compressed bytes " + numCompBytes + " input starts at " + raf2.getFilePointer() + "; output starts at " + dout2.getFilePointer());
                }
                if (numCompBytes < 0) {
                    if (debug) {
                        Cinrad2VolumeScan.log.debug("last block?" + numCompBytes);
                    }
                    numCompBytes = -numCompBytes;
                    eof = true;
                }
                final byte[] buf = new byte[numCompBytes];
                raf2.readFully(buf);
                final ByteArrayInputStream bis = new ByteArrayInputStream(buf, 2, numCompBytes - 2);
                cbzip2.setStream(bis);
                int total = 0;
                try {
                    int nread;
                    while ((nread = cbzip2.read(ubuff)) != -1) {
                        if (total + nread > obuff.length) {
                            final byte[] temp = obuff;
                            obuff = new byte[temp.length * 2];
                            System.arraycopy(temp, 0, obuff, 0, temp.length);
                        }
                        System.arraycopy(ubuff, 0, obuff, total, nread);
                        total += nread;
                    }
                    if (obuff.length >= 0) {
                        dout2.write(obuff, 0, total);
                    }
                }
                catch (BZip2ReadException ioe) {
                    Cinrad2VolumeScan.log.debug("Cinrad2IOSP.uncompress ", ioe);
                }
                final float nrecords = (float)(total / 2432.0);
                if (debug) {
                    Cinrad2VolumeScan.log.debug("  unpacked " + total + " num bytes " + nrecords + " records; ouput ends at " + dout2.getFilePointer());
                }
            }
        }
        catch (EOFException e) {
            e.printStackTrace();
        }
        dout2.flush();
        return dout2;
    }
    
    static void bdiff(final String filename) throws IOException {
        final InputStream in1 = new FileInputStream(filename + ".tmp");
        final InputStream in2 = new FileInputStream(filename + ".tmp2");
        int count = 0;
        int bad = 0;
        while (true) {
            final int b1 = in1.read();
            final int b2 = in2.read();
            if (b1 < 0) {
                break;
            }
            if (b2 < 0) {
                break;
            }
            if (b1 != b2) {
                System.out.println(count + " in1=" + b1 + " in2= " + b2);
                if (++bad > 130) {
                    break;
                }
            }
            ++count;
        }
        System.out.println("total read = " + count);
    }
    
    public static long testValid(final String ufilename) throws IOException {
        boolean lookForHeader = false;
        final RandomAccessFile raf = new RandomAccessFile(ufilename, "r");
        raf.order(1);
        raf.seek(0L);
        final byte[] b = new byte[8];
        raf.read(b);
        String test = new String(b);
        if (test.equals("ARCHIVE2") || test.equals("AR2V0001")) {
            System.out.println("--Good header= " + test);
            raf.seek(24L);
        }
        else {
            System.out.println("--No header ");
            lookForHeader = true;
            raf.seek(0L);
        }
        boolean eof = false;
        try {
            while (!eof) {
                if (lookForHeader) {
                    raf.read(b);
                    test = new String(b);
                    if (test.equals("ARCHIVE2") || test.equals("AR2V0001")) {
                        System.out.println("  found header= " + test);
                        raf.skipBytes(16);
                        lookForHeader = false;
                    }
                    else {
                        raf.skipBytes(-8);
                    }
                }
                int numCompBytes;
                try {
                    numCompBytes = raf.readInt();
                    if (numCompBytes == -1) {
                        System.out.println("\n--done: numCompBytes=-1 ");
                        break;
                    }
                }
                catch (EOFException ee) {
                    System.out.println("\n--got EOFException ");
                    break;
                }
                System.out.print(" " + numCompBytes + ",");
                if (numCompBytes < 0) {
                    System.out.println("\n--last block " + numCompBytes);
                    numCompBytes = -numCompBytes;
                    if (!lookForHeader) {
                        eof = true;
                    }
                }
                raf.skipBytes(numCompBytes);
            }
        }
        catch (EOFException e) {
            e.printStackTrace();
        }
        return raf.getFilePointer();
    }
    
    public static void main2(final String[] args) throws IOException {
        final File testDir = new File("C:/data/bad/radar2/");
        final File[] files = testDir.listFiles();
        for (int i = 0; i < files.length; ++i) {
            final File file = files[i];
            if (file.getPath().endsWith(".ar2v")) {
                System.out.println(file.getPath() + " " + file.length());
                final long pos = testValid(file.getPath());
                if (pos == file.length()) {
                    System.out.println("OK");
                    try {
                        NetcdfFile.open(file.getPath());
                    }
                    catch (Throwable t) {
                        System.out.println("ERROR=  " + t);
                    }
                }
                else {
                    System.out.println("NOT pos=" + pos);
                }
                System.out.println();
            }
        }
    }
    
    public static void main(final String[] args) throws IOException {
        NexradStationDB.init();
        final RandomAccessFile raf = new RandomAccessFile("R:/testdata/radar/nexrad/Cinrad2/problem/KCCX_20060627_1701", "r");
        new Cinrad2VolumeScan(raf, null);
    }
    
    static {
        Cinrad2VolumeScan.log = LoggerFactory.getLogger(Cinrad2VolumeScan.class);
    }
    
    private class GroupComparator implements Comparator
    {
        public int compare(final Object o1, final Object o2) {
            final List group1 = (List)o1;
            final List group2 = (List)o2;
            final Cinrad2Record record1 = group1.get(0);
            final Cinrad2Record record2 = group2.get(0);
            return record1.elevation_num - record2.elevation_num;
        }
    }
}
