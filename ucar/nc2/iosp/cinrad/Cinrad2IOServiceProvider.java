// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.iosp.cinrad;

import org.slf4j.LoggerFactory;
import ucar.ma2.InvalidRangeException;
import ucar.ma2.Range;
import ucar.ma2.Section;
import ucar.ma2.Index;
import ucar.ma2.IndexIterator;
import ucar.nc2.constants.AxisType;
import java.util.ArrayList;
import java.util.List;
import ucar.ma2.Array;
import ucar.ma2.DataType;
import ucar.nc2.Structure;
import ucar.nc2.Variable;
import ucar.unidata.geoloc.Earth;
import ucar.nc2.Attribute;
import ucar.nc2.Group;
import ucar.nc2.util.CancelTask;
import ucar.nc2.NetcdfFile;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;
import java.util.GregorianCalendar;
import java.util.SimpleTimeZone;
import java.io.IOException;
import ucar.nc2.iosp.nexrad2.NexradStationDB;
import ucar.unidata.io.RandomAccessFile;
import ucar.nc2.units.DateFormatter;
import ucar.nc2.Dimension;
import org.slf4j.Logger;
import ucar.nc2.iosp.AbstractIOServiceProvider;

public class Cinrad2IOServiceProvider extends AbstractIOServiceProvider
{
    private static Logger logger;
    private static final int MISSING_INT = -9999;
    private static final float MISSING_FLOAT = Float.NaN;
    private Cinrad2VolumeScan volScan;
    private Dimension radialDim;
    private double radarRadius;
    private DateFormatter formatter;
    
    public Cinrad2IOServiceProvider() {
        this.formatter = new DateFormatter();
    }
    
    public boolean isValidFileOld(final RandomAccessFile raf) {
        try {
            final String loc = raf.getLocation();
            int posFirst = loc.lastIndexOf(47) + 1;
            if (posFirst < 0) {
                posFirst = 0;
            }
            final String stationId = loc.substring(posFirst, posFirst + 4);
            NexradStationDB.init();
            final NexradStationDB.Station station = NexradStationDB.get("K" + stationId);
            return station != null;
        }
        catch (IOException ioe) {
            return false;
        }
    }
    
    public boolean isValidFile(final RandomAccessFile raf) {
        int data_msecs = 0;
        short data_julian_date = 0;
        try {
            raf.order(1);
            raf.seek(0L);
            raf.skipBytes(14);
            final short message_type = raf.readShort();
            if (message_type != 1) {
                return false;
            }
            raf.skipBytes(12);
            final byte[] b4 = raf.readBytes(4);
            data_msecs = bytesToInt(b4, true);
            final byte[] b5 = raf.readBytes(2);
            data_julian_date = (short)bytesToShort(b5, true);
            final Date dd = Cinrad2Record.getDate(data_julian_date, data_msecs);
            final Calendar cal = new GregorianCalendar(new SimpleTimeZone(0, "GMT"));
            cal.clear();
            cal.setTime(dd);
            final int year = cal.get(1);
            cal.setTime(new Date());
            final int cyear = cal.get(1);
            return year >= 1990 && year <= cyear;
        }
        catch (IOException ioe) {
            return false;
        }
    }
    
    public static int bytesToInt(final byte[] bytes, final boolean swapBytes) {
        final byte a = bytes[0];
        final byte b = bytes[1];
        final byte c = bytes[2];
        final byte d = bytes[3];
        if (swapBytes) {
            return (a & 0xFF) + ((b & 0xFF) << 8) + ((c & 0xFF) << 16) + ((d & 0xFF) << 24);
        }
        return ((a & 0xFF) << 24) + ((b & 0xFF) << 16) + ((c & 0xFF) << 8) + (d & 0xFF);
    }
    
    public static int bytesToShort(final byte[] bytes, final boolean swapBytes) {
        final byte a = bytes[0];
        final byte b = bytes[1];
        if (swapBytes) {
            return (a & 0xFF) + ((b & 0xFF) << 8);
        }
        return ((a & 0xFF) << 24) + ((b & 0xFF) << 16);
    }
    
    public String getFileTypeId() {
        return "CINRAD";
    }
    
    public String getFileTypeDescription() {
        return "Chinese Level-II Base Data";
    }
    
    @Override
    public void open(final RandomAccessFile raf, final NetcdfFile ncfile, final CancelTask cancelTask) throws IOException {
        NexradStationDB.init();
        this.volScan = new Cinrad2VolumeScan(raf, cancelTask);
        if (this.volScan.hasDifferentDopplarResolutions()) {
            throw new IllegalStateException("volScan.hasDifferentDopplarResolutions");
        }
        ncfile.addDimension(null, this.radialDim = new Dimension("radial", this.volScan.getMaxRadials()));
        this.makeVariable(ncfile, 1, "Reflectivity", "Reflectivity", "R", this.volScan.getReflectivityGroups());
        final int velocity_type = (this.volScan.getDopplarResolution() == 2) ? 2 : 4;
        final Variable v = this.makeVariable(ncfile, velocity_type, "RadialVelocity", "Radial Velocity", "V", this.volScan.getVelocityGroups());
        this.makeVariableNoCoords(ncfile, 3, "SpectrumWidth", "Spectrum Width", v);
        if (this.volScan.getStationId() != null) {
            ncfile.addAttribute(null, new Attribute("Station", this.volScan.getStationId()));
            ncfile.addAttribute(null, new Attribute("StationName", this.volScan.getStationName()));
            ncfile.addAttribute(null, new Attribute("StationLatitude", new Double(this.volScan.getStationLatitude())));
            ncfile.addAttribute(null, new Attribute("StationLongitude", new Double(this.volScan.getStationLongitude())));
            ncfile.addAttribute(null, new Attribute("StationElevationInMeters", new Double(this.volScan.getStationElevation())));
            final double latRadiusDegrees = Math.toDegrees(this.radarRadius / Earth.getRadius());
            ncfile.addAttribute(null, new Attribute("geospatial_lat_min", new Double(this.volScan.getStationLatitude() - latRadiusDegrees)));
            ncfile.addAttribute(null, new Attribute("geospatial_lat_max", new Double(this.volScan.getStationLatitude() + latRadiusDegrees)));
            final double cosLat = Math.cos(Math.toRadians(this.volScan.getStationLatitude()));
            final double lonRadiusDegrees = Math.toDegrees(this.radarRadius / cosLat / Earth.getRadius());
            ncfile.addAttribute(null, new Attribute("geospatial_lon_min", new Double(this.volScan.getStationLongitude() - lonRadiusDegrees)));
            ncfile.addAttribute(null, new Attribute("geospatial_lon_max", new Double(this.volScan.getStationLongitude() + lonRadiusDegrees)));
            final Variable ct = new Variable(ncfile, null, null, "radialCoordinateTransform");
            ct.setDataType(DataType.CHAR);
            ct.setDimensions("");
            ct.addAttribute(new Attribute("transform_name", "Radial"));
            ct.addAttribute(new Attribute("center_latitude", new Double(this.volScan.getStationLatitude())));
            ct.addAttribute(new Attribute("center_longitude", new Double(this.volScan.getStationLongitude())));
            ct.addAttribute(new Attribute("center_elevation", new Double(this.volScan.getStationElevation())));
            ct.addAttribute(new Attribute("_CoordinateTransformType", "Radial"));
            ct.addAttribute(new Attribute("_CoordinateAxisTypes", "RadialElevation RadialAzimuth RadialDistance"));
            final Array data = Array.factory(DataType.CHAR.getPrimitiveClassType(), new int[0], new char[] { ' ' });
            ct.setCachedData(data, true);
            ncfile.addVariable(null, ct);
        }
        final DateFormatter formatter = new DateFormatter();
        ncfile.addAttribute(null, new Attribute("Conventions", "_Coordinates"));
        ncfile.addAttribute(null, new Attribute("format", this.volScan.getDataFormat()));
        ncfile.addAttribute(null, new Attribute("time_coverage_start", formatter.toDateTimeStringISO(this.volScan.getStartDate())));
        ncfile.addAttribute(null, new Attribute("time_coverage_end", formatter.toDateTimeStringISO(this.volScan.getEndDate())));
        ncfile.addAttribute(null, new Attribute("history", "Direct read of Nexrad Level 2 file into NetCDF-Java 2.2 API"));
        ncfile.addAttribute(null, new Attribute("DataType", "Radial"));
        ncfile.addAttribute(null, new Attribute("Title", "Nexrad Level 2 Station " + this.volScan.getStationId() + " from " + formatter.toDateTimeStringISO(this.volScan.getStartDate()) + " to " + formatter.toDateTimeStringISO(this.volScan.getEndDate())));
        ncfile.addAttribute(null, new Attribute("Summary", "Weather Surveillance Radar-1988 Doppler (WSR-88D) Level II data are the three meteorological base data quantities: reflectivity, mean radial velocity, and spectrum width."));
        ncfile.addAttribute(null, new Attribute("keywords", "WSR-88D; NEXRAD; Radar Level II; reflectivity; mean radial velocity; spectrum width"));
        ncfile.addAttribute(null, new Attribute("VolumeCoveragePatternName", Cinrad2Record.getVolumeCoveragePatternName(this.volScan.getVCP())));
        ncfile.addAttribute(null, new Attribute("VolumeCoveragePattern", new Integer(this.volScan.getVCP())));
        ncfile.addAttribute(null, new Attribute("HorizonatalBeamWidthInDegrees", new Double(1.5)));
        ncfile.finish();
    }
    
    public Variable makeVariable(final NetcdfFile ncfile, final int datatype, final String shortName, final String longName, final String abbrev, final List groups) throws IOException {
        final int nscans = groups.size();
        if (nscans == 0) {
            throw new IllegalStateException("No data for " + shortName);
        }
        final List firstGroup = groups.get(0);
        final Cinrad2Record firstRecord = firstGroup.get(0);
        final int ngates = firstRecord.getGateCount(datatype);
        final String scanDimName = "scan" + abbrev;
        final String gateDimName = "gate" + abbrev;
        final Dimension scanDim = new Dimension(scanDimName, nscans);
        final Dimension gateDim = new Dimension(gateDimName, ngates);
        ncfile.addDimension(null, scanDim);
        ncfile.addDimension(null, gateDim);
        final ArrayList dims = new ArrayList();
        dims.add(scanDim);
        dims.add(this.radialDim);
        dims.add(gateDim);
        final Variable v = new Variable(ncfile, null, null, shortName);
        v.setDataType(DataType.BYTE);
        v.setDimensions(dims);
        ncfile.addVariable(null, v);
        v.addAttribute(new Attribute("units", Cinrad2Record.getDatatypeUnits(datatype)));
        v.addAttribute(new Attribute("long_name", longName));
        final byte[] b = { 1, 0 };
        final Array missingArray = Array.factory(DataType.BYTE.getPrimitiveClassType(), new int[] { 2 }, b);
        v.addAttribute(new Attribute("missing_value", missingArray));
        v.addAttribute(new Attribute("signal_below_threshold", new Byte((byte)0)));
        v.addAttribute(new Attribute("scale_factor", new Float(Cinrad2Record.getDatatypeScaleFactor(datatype))));
        v.addAttribute(new Attribute("add_offset", new Float(Cinrad2Record.getDatatypeAddOffset(datatype))));
        v.addAttribute(new Attribute("_Unsigned", "true"));
        final ArrayList dim2 = new ArrayList();
        dim2.add(scanDim);
        dim2.add(this.radialDim);
        final String timeCoordName = "time" + abbrev;
        final Variable timeVar = new Variable(ncfile, null, null, timeCoordName);
        timeVar.setDataType(DataType.INT);
        timeVar.setDimensions(dim2);
        ncfile.addVariable(null, timeVar);
        final Date d = this.volScan.getStartDate();
        final String units = "msecs since " + this.formatter.toDateTimeStringISO(d);
        timeVar.addAttribute(new Attribute("long_name", "time since base date"));
        timeVar.addAttribute(new Attribute("units", units));
        timeVar.addAttribute(new Attribute("missing_value", new Integer(-9999)));
        timeVar.addAttribute(new Attribute("_CoordinateAxisType", AxisType.Time.toString()));
        final String elevCoordName = "elevation" + abbrev;
        final Variable elevVar = new Variable(ncfile, null, null, elevCoordName);
        elevVar.setDataType(DataType.FLOAT);
        elevVar.setDimensions(dim2);
        ncfile.addVariable(null, elevVar);
        elevVar.addAttribute(new Attribute("units", "degrees"));
        elevVar.addAttribute(new Attribute("long_name", "elevation angle in degres: 0 = parallel to pedestal base, 90 = perpendicular"));
        elevVar.addAttribute(new Attribute("missing_value", new Float(Float.NaN)));
        elevVar.addAttribute(new Attribute("_CoordinateAxisType", AxisType.RadialElevation.toString()));
        final String aziCoordName = "azimuth" + abbrev;
        final Variable aziVar = new Variable(ncfile, null, null, aziCoordName);
        aziVar.setDataType(DataType.FLOAT);
        aziVar.setDimensions(dim2);
        ncfile.addVariable(null, aziVar);
        aziVar.addAttribute(new Attribute("units", "degrees"));
        aziVar.addAttribute(new Attribute("long_name", "azimuth angle in degrees: 0 = true north, 90 = east"));
        aziVar.addAttribute(new Attribute("missing_value", new Float(Float.NaN)));
        aziVar.addAttribute(new Attribute("_CoordinateAxisType", AxisType.RadialAzimuth.toString()));
        final String gateCoordName = "distance" + abbrev;
        final Variable gateVar = new Variable(ncfile, null, null, gateCoordName);
        gateVar.setDataType(DataType.FLOAT);
        gateVar.setDimensions(gateDimName);
        final Array data = Array.makeArray(DataType.FLOAT, ngates, firstRecord.getGateStart(datatype), firstRecord.getGateSize(datatype));
        gateVar.setCachedData(data, false);
        ncfile.addVariable(null, gateVar);
        this.radarRadius = firstRecord.getGateStart(datatype) + ngates * firstRecord.getGateSize(datatype);
        gateVar.addAttribute(new Attribute("units", "m"));
        gateVar.addAttribute(new Attribute("long_name", "radial distance to start of gate"));
        gateVar.addAttribute(new Attribute("_CoordinateAxisType", AxisType.RadialDistance.toString()));
        final String nradialsName = "numRadials" + abbrev;
        final Variable nradialsVar = new Variable(ncfile, null, null, nradialsName);
        nradialsVar.setDataType(DataType.INT);
        nradialsVar.setDimensions(scanDim.getName());
        nradialsVar.addAttribute(new Attribute("long_name", "number of valid radials in this scan"));
        ncfile.addVariable(null, nradialsVar);
        final String ngateName = "numGates" + abbrev;
        final Variable ngateVar = new Variable(ncfile, null, null, ngateName);
        ngateVar.setDataType(DataType.INT);
        ngateVar.setDimensions(scanDim.getName());
        ngateVar.addAttribute(new Attribute("long_name", "number of valid gates in this scan"));
        ncfile.addVariable(null, ngateVar);
        this.makeCoordinateDataWithMissing(datatype, timeVar, elevVar, aziVar, nradialsVar, ngateVar, groups);
        final String coordinates = timeCoordName + " " + elevCoordName + " " + aziCoordName + " " + gateCoordName;
        v.addAttribute(new Attribute("_CoordinateAxes", coordinates));
        final int nradials = this.radialDim.getLength();
        final Cinrad2Record[][] map = new Cinrad2Record[nscans][nradials];
        for (int i = 0; i < groups.size(); ++i) {
            final Cinrad2Record[] mapScan = map[i];
            final List group = groups.get(i);
            for (int j = 0; j < group.size(); ++j) {
                final Cinrad2Record r = group.get(j);
                final int radial = r.radial_num - 1;
                mapScan[radial] = r;
            }
        }
        final Vgroup vg = new Vgroup(datatype, map);
        v.setSPobject(vg);
        return v;
    }
    
    private void makeVariableNoCoords(final NetcdfFile ncfile, final int datatype, final String shortName, final String longName, final Variable from) {
        final Variable v = new Variable(ncfile, null, null, shortName);
        v.setDataType(DataType.BYTE);
        v.setDimensions(from.getDimensions());
        ncfile.addVariable(null, v);
        v.addAttribute(new Attribute("units", Cinrad2Record.getDatatypeUnits(datatype)));
        v.addAttribute(new Attribute("long_name", longName));
        final byte[] b = { 1, 0 };
        final Array missingArray = Array.factory(DataType.BYTE.getPrimitiveClassType(), new int[] { 2 }, b);
        v.addAttribute(new Attribute("missing_value", missingArray));
        v.addAttribute(new Attribute("signal_below_threshold", new Byte((byte)0)));
        v.addAttribute(new Attribute("scale_factor", new Float(Cinrad2Record.getDatatypeScaleFactor(datatype))));
        v.addAttribute(new Attribute("add_offset", new Float(Cinrad2Record.getDatatypeAddOffset(datatype))));
        v.addAttribute(new Attribute("_Unsigned", "true"));
        final Attribute fromAtt = from.findAttribute("_CoordinateAxes");
        v.addAttribute(new Attribute("_CoordinateAxes", fromAtt));
        final Vgroup vgFrom = (Vgroup)from.getSPobject();
        final Vgroup vg = new Vgroup(datatype, vgFrom.map);
        v.setSPobject(vg);
    }
    
    private void makeCoordinateData(final int datatype, final Variable time, final Variable elev, final Variable azi, final Variable nradialsVar, final Variable ngatesVar, final List groups) {
        final Array timeData = Array.factory(time.getDataType().getPrimitiveClassType(), time.getShape());
        final IndexIterator timeDataIter = timeData.getIndexIterator();
        final Array elevData = Array.factory(elev.getDataType().getPrimitiveClassType(), elev.getShape());
        final IndexIterator elevDataIter = elevData.getIndexIterator();
        final Array aziData = Array.factory(azi.getDataType().getPrimitiveClassType(), azi.getShape());
        final IndexIterator aziDataIter = aziData.getIndexIterator();
        final Array nradialsData = Array.factory(nradialsVar.getDataType().getPrimitiveClassType(), nradialsVar.getShape());
        final IndexIterator nradialsIter = nradialsData.getIndexIterator();
        final Array ngatesData = Array.factory(ngatesVar.getDataType().getPrimitiveClassType(), ngatesVar.getShape());
        final IndexIterator ngatesIter = ngatesData.getIndexIterator();
        int last_msecs = Integer.MIN_VALUE;
        final int nscans = groups.size();
        final int maxRadials = this.volScan.getMaxRadials();
        for (int i = 0; i < nscans; ++i) {
            final List scanGroup = groups.get(i);
            final int nradials = scanGroup.size();
            Cinrad2Record first = null;
            for (int j = 0; j < nradials; ++j) {
                final Cinrad2Record r = scanGroup.get(j);
                if (first == null) {
                    first = r;
                }
                timeDataIter.setIntNext(r.data_msecs);
                elevDataIter.setFloatNext(r.getElevation());
                aziDataIter.setFloatNext(r.getAzimuth());
                if (r.data_msecs < last_msecs) {
                    Cinrad2IOServiceProvider.logger.warn("makeCoordinateData time out of order " + r.data_msecs);
                }
                last_msecs = r.data_msecs;
            }
            for (int j = nradials; j < maxRadials; ++j) {
                timeDataIter.setIntNext(-9999);
                elevDataIter.setFloatNext(Float.NaN);
                aziDataIter.setFloatNext(Float.NaN);
            }
            nradialsIter.setIntNext(nradials);
            ngatesIter.setIntNext(first.getGateCount(datatype));
        }
        time.setCachedData(timeData, false);
        elev.setCachedData(elevData, false);
        azi.setCachedData(aziData, false);
        nradialsVar.setCachedData(nradialsData, false);
        ngatesVar.setCachedData(ngatesData, false);
    }
    
    private void makeCoordinateDataWithMissing(final int datatype, final Variable time, final Variable elev, final Variable azi, final Variable nradialsVar, final Variable ngatesVar, final List groups) {
        final Array timeData = Array.factory(time.getDataType().getPrimitiveClassType(), time.getShape());
        final Index timeIndex = timeData.getIndex();
        final Array elevData = Array.factory(elev.getDataType().getPrimitiveClassType(), elev.getShape());
        final Index elevIndex = elevData.getIndex();
        final Array aziData = Array.factory(azi.getDataType().getPrimitiveClassType(), azi.getShape());
        final Index aziIndex = aziData.getIndex();
        final Array nradialsData = Array.factory(nradialsVar.getDataType().getPrimitiveClassType(), nradialsVar.getShape());
        final IndexIterator nradialsIter = nradialsData.getIndexIterator();
        final Array ngatesData = Array.factory(ngatesVar.getDataType().getPrimitiveClassType(), ngatesVar.getShape());
        final IndexIterator ngatesIter = ngatesData.getIndexIterator();
        IndexIterator ii = timeData.getIndexIterator();
        while (ii.hasNext()) {
            ii.setIntNext(-9999);
        }
        ii = elevData.getIndexIterator();
        while (ii.hasNext()) {
            ii.setFloatNext(Float.NaN);
        }
        ii = aziData.getIndexIterator();
        while (ii.hasNext()) {
            ii.setFloatNext(Float.NaN);
        }
        int last_msecs = Integer.MIN_VALUE;
        final int nscans = groups.size();
        try {
            for (int scan = 0; scan < nscans; ++scan) {
                final List scanGroup = groups.get(scan);
                final int nradials = scanGroup.size();
                Cinrad2Record first = null;
                for (int j = 0; j < nradials; ++j) {
                    final Cinrad2Record r = scanGroup.get(j);
                    if (first == null) {
                        first = r;
                    }
                    final int radial = r.radial_num - 1;
                    timeData.setInt(timeIndex.set(scan, radial), r.data_msecs);
                    elevData.setFloat(elevIndex.set(scan, radial), r.getElevation());
                    aziData.setFloat(aziIndex.set(scan, radial), r.getAzimuth());
                    if (r.data_msecs < last_msecs) {
                        Cinrad2IOServiceProvider.logger.warn("makeCoordinateData time out of order " + r.data_msecs);
                    }
                    last_msecs = r.data_msecs;
                }
                nradialsIter.setIntNext(nradials);
                ngatesIter.setIntNext(first.getGateCount(datatype));
            }
        }
        catch (ArrayIndexOutOfBoundsException ae) {
            Cinrad2IOServiceProvider.logger.debug("Cinrad2IOSP.uncompress ", ae);
        }
        time.setCachedData(timeData, false);
        elev.setCachedData(elevData, false);
        azi.setCachedData(aziData, false);
        nradialsVar.setCachedData(nradialsData, false);
        ngatesVar.setCachedData(ngatesData, false);
    }
    
    public Array readData(final Variable v2, final Section section) throws IOException, InvalidRangeException {
        final Vgroup vgroup = (Vgroup)v2.getSPobject();
        final Range scanRange = section.getRange(0);
        final Range radialRange = section.getRange(1);
        final Range gateRange = section.getRange(2);
        final Array data = Array.factory(v2.getDataType().getPrimitiveClassType(), section.getShape());
        final IndexIterator ii = data.getIndexIterator();
        for (int i = scanRange.first(); i <= scanRange.last(); i += scanRange.stride()) {
            final Cinrad2Record[] mapScan = vgroup.map[i];
            this.readOneScan(mapScan, radialRange, gateRange, vgroup.datatype, ii);
        }
        return data;
    }
    
    private void readOneScan(final Cinrad2Record[] mapScan, final Range radialRange, final Range gateRange, final int datatype, final IndexIterator ii) throws IOException {
        for (int i = radialRange.first(); i <= radialRange.last(); i += radialRange.stride()) {
            final Cinrad2Record r = mapScan[i];
            this.readOneRadial(r, datatype, gateRange, ii);
        }
    }
    
    private void readOneRadial(final Cinrad2Record r, final int datatype, final Range gateRange, final IndexIterator ii) throws IOException {
        if (r == null) {
            for (int i = gateRange.first(); i <= gateRange.last(); i += gateRange.stride()) {
                ii.setByteNext((byte)1);
            }
            return;
        }
        r.readData(this.volScan.raf, datatype, gateRange, ii);
    }
    
    @Override
    public void close() throws IOException {
        this.volScan.raf.close();
    }
    
    static {
        Cinrad2IOServiceProvider.logger = LoggerFactory.getLogger(Cinrad2IOServiceProvider.class);
    }
    
    private class Vgroup
    {
        Cinrad2Record[][] map;
        int datatype;
        
        Vgroup(final int datatype, final Cinrad2Record[][] map) {
            this.datatype = datatype;
            this.map = map;
        }
    }
}
