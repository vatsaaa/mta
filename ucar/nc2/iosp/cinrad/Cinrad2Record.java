// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.iosp.cinrad;

import org.slf4j.LoggerFactory;
import ucar.ma2.IndexIterator;
import ucar.ma2.Range;
import java.io.PrintStream;
import java.io.IOException;
import ucar.unidata.io.RandomAccessFile;
import java.util.Date;
import org.slf4j.Logger;

public class Cinrad2Record
{
    public static final int REFLECTIVITY = 1;
    public static final int VELOCITY_HI = 2;
    public static final int VELOCITY_LOW = 4;
    public static final int SPECTRUM_WIDTH = 3;
    public static final int DOPPLER_RESOLUTION_LOW_CODE = 4;
    public static final int DOPPLER_RESOLUTION_HIGH_CODE = 2;
    public static final float HORIZONTAL_BEAM_WIDTH = 1.5f;
    public static final byte MISSING_DATA = 1;
    public static final byte BELOW_THRESHOLD = 0;
    static final int FILE_HEADER_SIZE = 0;
    private static final int CTM_HEADER_SIZE = 14;
    private static final int MESSAGE_HEADER_SIZE = 28;
    private static final int RADAR_DATA_SIZE = 2432;
    private static Logger logger;
    int recno;
    long message_offset;
    boolean hasReflectData;
    boolean hasDopplerData;
    short message_size;
    byte id_channel;
    public byte message_type;
    short id_sequence;
    short mess_julian_date;
    int mess_msecs;
    short seg_count;
    short seg_number;
    int data_msecs;
    short data_julian_date;
    short unamb_range;
    int azimuth_ang;
    short radial_num;
    short radial_status;
    short elevation_ang;
    short elevation_num;
    short reflect_first_gate;
    short reflect_gate_size;
    short reflect_gate_count;
    short doppler_first_gate;
    short doppler_gate_size;
    short doppler_gate_count;
    short cut;
    float calibration;
    short resolution;
    short vcp;
    short nyquist_vel;
    short attenuation;
    short threshhold;
    private short reflect_offset;
    private short velocity_offset;
    private short spectWidth_offset;
    
    public static String getDatatypeName(final int datatype) {
        switch (datatype) {
            case 1: {
                return "Reflectivity";
            }
            case 2:
            case 4: {
                return "RadialVelocity";
            }
            case 3: {
                return "SpectrumWidth";
            }
            default: {
                throw new IllegalArgumentException();
            }
        }
    }
    
    public static String getDatatypeUnits(final int datatype) {
        switch (datatype) {
            case 1: {
                return "dBz";
            }
            case 2:
            case 3:
            case 4: {
                return "m/s";
            }
            default: {
                throw new IllegalArgumentException();
            }
        }
    }
    
    public static float getDatatypeScaleFactor(final int datatype) {
        switch (datatype) {
            case 1: {
                return 0.5f;
            }
            case 4: {
                return 1.0f;
            }
            case 2:
            case 3: {
                return 0.5f;
            }
            default: {
                throw new IllegalArgumentException();
            }
        }
    }
    
    public static float getDatatypeAddOffset(final int datatype) {
        switch (datatype) {
            case 1: {
                return -33.0f;
            }
            case 4: {
                return -129.0f;
            }
            case 2:
            case 3: {
                return -64.5f;
            }
            default: {
                throw new IllegalArgumentException();
            }
        }
    }
    
    public static String getMessageTypeName(final int code) {
        switch (code) {
            case 1: {
                return "digital radar data";
            }
            case 2: {
                return "RDA status data";
            }
            case 3: {
                return "performance/maintainence data";
            }
            case 4: {
                return "console message - RDA to RPG";
            }
            case 5: {
                return "maintainence log data";
            }
            case 6: {
                return "RDA control ocmmands";
            }
            case 7: {
                return "volume coverage pattern";
            }
            case 8: {
                return "clutter censor zones";
            }
            case 9: {
                return "request for data";
            }
            case 10: {
                return "console message - RPG to RDA";
            }
            case 11: {
                return "loop back test - RDA to RPG";
            }
            case 12: {
                return "loop back test - RPG to RDA";
            }
            case 13: {
                return "clutter filter bypass map - RDA to RPG";
            }
            case 14: {
                return "edited clutter filter bypass map - RDA to RPG";
            }
            case 15: {
                return "Notchwidth Map";
            }
            case 18: {
                return "RDA Adaptation data";
            }
            default: {
                return "unknown " + code;
            }
        }
    }
    
    public static String getRadialStatusName(final int code) {
        switch (code) {
            case 0: {
                return "start of new elevation";
            }
            case 1: {
                return "intermediate radial";
            }
            case 2: {
                return "end of elevation";
            }
            case 3: {
                return "begin volume scan";
            }
            case 4: {
                return "end volume scan";
            }
            default: {
                return "unknown " + code;
            }
        }
    }
    
    public static String getVolumeCoveragePatternName(final int code) {
        switch (code) {
            case 11: {
                return "16 elevation scans every 5 mins";
            }
            case 12: {
                return "14 elevation scan every 4.1 mins";
            }
            case 21: {
                return "11 elevation scans every 6 mins";
            }
            case 31: {
                return "8 elevation scans every 10 mins";
            }
            case 32: {
                return "7 elevation scans every 10 mins";
            }
            case 121: {
                return "9 elevations, 20 scans every 5 minutes";
            }
            default: {
                return "unknown " + code;
            }
        }
    }
    
    public static Date getDate(final int julianDays, final int msecs) {
        final long total = (julianDays - 1) * 24L * 3600L * 1000L + msecs;
        return new Date(total);
    }
    
    public static Cinrad2Record factory(final RandomAccessFile din, final int record) throws IOException {
        final long offset = record * 2432 + 0;
        if (offset >= din.length()) {
            return null;
        }
        return new Cinrad2Record(din, record);
    }
    
    int getUInt(final byte[] b, final int num) {
        int base = 1;
        int word = 0;
        final int[] bv = new int[num];
        for (int i = 0; i < num; ++i) {
            bv[i] = this.convertunsignedByte2Short(b[i]);
        }
        for (int i = num - 1; i >= 0; --i) {
            word += base * b[i];
            base *= 256;
        }
        return word;
    }
    
    public short convertunsignedByte2Short(final byte b) {
        return (short)((b < 0) ? (b + 256) : ((short)b));
    }
    
    public Cinrad2Record(final RandomAccessFile din, final int record) throws IOException {
        this.message_size = 0;
        this.id_channel = 0;
        this.message_type = 0;
        this.id_sequence = 0;
        this.mess_julian_date = 0;
        this.mess_msecs = 0;
        this.seg_count = 0;
        this.seg_number = 0;
        this.data_msecs = 0;
        this.data_julian_date = 0;
        this.unamb_range = 0;
        this.azimuth_ang = 0;
        this.radial_num = 0;
        this.radial_status = 0;
        this.elevation_ang = 0;
        this.elevation_num = 0;
        this.reflect_first_gate = 0;
        this.reflect_gate_size = 0;
        this.reflect_gate_count = 0;
        this.doppler_first_gate = 0;
        this.doppler_gate_size = 0;
        this.doppler_gate_count = 0;
        this.cut = 0;
        this.calibration = 0.0f;
        this.resolution = 0;
        this.vcp = 0;
        this.recno = record;
        din.seek(this.message_offset = record * 2432 + 0);
        din.skipBytes(14);
        this.message_type = din.readByte();
        din.skipBytes(13);
        if (this.message_type != 1) {
            return;
        }
        final byte[] b4 = din.readBytes(4);
        this.data_msecs = bytesToInt(b4, true);
        final byte[] b5 = din.readBytes(2);
        this.data_julian_date = (short)bytesToShort(b5, true);
        final Date dd = this.getDate();
        this.unamb_range = din.readShort();
        this.azimuth_ang = din.readUnsignedShort();
        this.radial_num = din.readShort();
        this.radial_status = din.readShort();
        this.elevation_ang = din.readShort();
        this.elevation_num = din.readShort();
        this.reflect_first_gate = din.readShort();
        this.doppler_first_gate = din.readShort();
        this.reflect_gate_size = din.readShort();
        this.doppler_gate_size = din.readShort();
        this.reflect_gate_count = din.readShort();
        this.doppler_gate_count = din.readShort();
        din.skipBytes(6);
        this.reflect_offset = din.readShort();
        this.velocity_offset = din.readShort();
        this.spectWidth_offset = din.readShort();
        this.resolution = din.readShort();
        this.vcp = din.readShort();
        din.skipBytes(14);
        this.nyquist_vel = din.readShort();
        din.skipBytes(38);
        this.hasReflectData = (this.reflect_gate_count > 0);
        this.hasDopplerData = (this.doppler_gate_count > 0);
    }
    
    public static int bytesToInt(final byte[] bytes, final boolean swapBytes) {
        final byte a = bytes[0];
        final byte b = bytes[1];
        final byte c = bytes[2];
        final byte d = bytes[3];
        if (swapBytes) {
            return (a & 0xFF) + ((b & 0xFF) << 8) + ((c & 0xFF) << 16) + ((d & 0xFF) << 24);
        }
        return ((a & 0xFF) << 24) + ((b & 0xFF) << 16) + ((c & 0xFF) << 8) + (d & 0xFF);
    }
    
    public static int bytesToShort(final byte[] bytes, final boolean swapBytes) {
        final byte a = bytes[0];
        final byte b = bytes[1];
        if (swapBytes) {
            return (a & 0xFF) + ((b & 0xFF) << 8);
        }
        return ((a & 0xFF) << 24) + ((b & 0xFF) << 16);
    }
    
    public void dumpMessage(final PrintStream out, final Date d) {
        out.println(this.recno + " ---------------------");
        out.println(" message type = " + getMessageTypeName(this.message_type) + " (" + this.message_type + ")");
        out.println(" message size = " + this.message_size + " segment=" + this.seg_number + "/" + this.seg_count);
        out.println(" message date = " + d.toString());
        out.println(" channel id = " + this.id_channel);
    }
    
    public void dump(final PrintStream out) {
        out.println(this.recno + " ------------------------------------------" + this.message_offset);
        out.println(" message type = " + getMessageTypeName(this.message_type));
        out.println(" data date = " + this.getDate().toString());
        out.println(" elevation = " + this.getElevation() + " (" + this.elevation_num + ")");
        out.println(" azimuth = " + this.getAzimuth());
        out.println(" radial = " + this.radial_num + " status= " + getRadialStatusName(this.radial_status) + " ratio = " + this.getAzimuth() / this.radial_num);
        out.println(" reflectivity first= " + this.reflect_first_gate + " size= " + this.reflect_gate_size + " count= " + this.reflect_gate_count);
        out.println(" doppler first= " + this.doppler_first_gate + " size= " + this.doppler_gate_size + " count= " + this.doppler_gate_count);
        out.println(" offset: reflect= " + this.reflect_offset + " velocity= " + this.velocity_offset + " spWidth= " + this.spectWidth_offset);
        out.println(" pattern = " + this.vcp);
    }
    
    public void dump2(final PrintStream out) {
        out.println(this.recno + "= " + this.elevation_num + " size = " + this.message_size);
    }
    
    public boolean checkOk() {
        boolean ok = true;
        if (this.message_type != 1) {
            return ok;
        }
        if (this.reflect_offset < 0 || this.reflect_offset > 2432) {
            Cinrad2Record.logger.warn("****" + this.recno + " HAS bad reflect offset= " + this.reflect_offset + this.who());
            ok = false;
        }
        if (this.velocity_offset < 0 || this.velocity_offset > 2432) {
            Cinrad2Record.logger.warn("****" + this.recno + " HAS bad velocity offset= " + this.velocity_offset + this.who());
            ok = false;
        }
        if (this.spectWidth_offset < 0 || this.spectWidth_offset > 2432) {
            Cinrad2Record.logger.warn("****" + this.recno + " HAS bad spwidth offset= " + this.reflect_offset + this.who());
            ok = false;
        }
        if (this.velocity_offset > 0 && this.spectWidth_offset <= 0) {
            Cinrad2Record.logger.warn("****" + this.recno + " HAS velocity NOT spectWidth!!" + this.who());
            ok = false;
        }
        if (this.velocity_offset <= 0 && this.spectWidth_offset > 0) {
            Cinrad2Record.logger.warn("****" + this.recno + " HAS spectWidth AND NOT velocity!!" + this.who());
            ok = false;
        }
        if (!this.hasReflectData && !this.hasDopplerData) {
            Cinrad2Record.logger.info("*** no reflect or dopplar = " + this.who());
        }
        return ok;
    }
    
    private String who() {
        return " message(" + this.recno + " " + this.message_offset + ")";
    }
    
    public float getAzimuth() {
        if (this.message_type != 1) {
            return -1.0f;
        }
        return 180.0f * this.azimuth_ang / 32768.0f;
    }
    
    public float getElevation() {
        if (this.message_type != 1) {
            return -1.0f;
        }
        return 180.0f * this.elevation_ang / 32768.0f;
    }
    
    public int getGateSize(final int datatype) {
        switch (datatype) {
            case 1: {
                return this.reflect_gate_size;
            }
            case 2:
            case 3:
            case 4: {
                return this.doppler_gate_size;
            }
            default: {
                return -1;
            }
        }
    }
    
    public int getGateStart(final int datatype) {
        switch (datatype) {
            case 1: {
                return this.reflect_first_gate;
            }
            case 2:
            case 3:
            case 4: {
                return this.doppler_first_gate;
            }
            default: {
                return -1;
            }
        }
    }
    
    public int getGateCount(final int datatype) {
        switch (datatype) {
            case 1: {
                return this.reflect_gate_count;
            }
            case 2:
            case 3:
            case 4: {
                return this.doppler_gate_count;
            }
            default: {
                return 0;
            }
        }
    }
    
    private short getDataOffset(final int datatype) {
        switch (datatype) {
            case 1: {
                return this.reflect_offset;
            }
            case 2:
            case 4: {
                return this.velocity_offset;
            }
            case 3: {
                return this.spectWidth_offset;
            }
            default: {
                return -32768;
            }
        }
    }
    
    public Date getDate() {
        return getDate(this.data_julian_date, this.data_msecs);
    }
    
    public void readData(final RandomAccessFile raf, final int datatype, final Range gateRange, final IndexIterator ii) throws IOException {
        long offset = this.message_offset;
        offset += 28L;
        offset += this.getDataOffset(datatype);
        raf.seek(offset);
        if (Cinrad2Record.logger.isDebugEnabled()) {
            Cinrad2Record.logger.debug("  read recno " + this.recno + " at offset " + offset + " count= " + this.getGateCount(datatype));
            Cinrad2Record.logger.debug("   offset: reflect= " + this.reflect_offset + " velocity= " + this.velocity_offset + " spWidth= " + this.spectWidth_offset);
        }
        final int dataCount = this.getGateCount(datatype);
        final byte[] data = new byte[dataCount];
        raf.readFully(data);
        for (int i = gateRange.first(); i <= gateRange.last(); i += gateRange.stride()) {
            if (i >= dataCount) {
                ii.setByteNext((byte)1);
            }
            else {
                ii.setByteNext(data[i]);
            }
        }
    }
    
    @Override
    public String toString() {
        return "elev= " + this.elevation_num + " radial_num = " + this.radial_num;
    }
    
    static {
        Cinrad2Record.logger = LoggerFactory.getLogger(Cinrad2Record.class);
    }
}
