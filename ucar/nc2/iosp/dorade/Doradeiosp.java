// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.iosp.dorade;

import ucar.ma2.InvalidRangeException;
import ucar.atd.dorade.DoradePARM;
import ucar.ma2.IndexIterator;
import ucar.ma2.Range;
import java.util.Date;
import ucar.ma2.DataType;
import ucar.ma2.Array;
import ucar.ma2.Section;
import ucar.nc2.Variable;
import java.io.PrintStream;
import ucar.atd.dorade.ScanMode;
import java.io.IOException;
import ucar.nc2.util.CancelTask;
import ucar.nc2.NetcdfFile;
import java.util.HashMap;
import ucar.atd.dorade.DoradeSweep;
import ucar.unidata.io.RandomAccessFile;
import ucar.nc2.iosp.AbstractIOServiceProvider;

public class Doradeiosp extends AbstractIOServiceProvider
{
    protected boolean readonly;
    private RandomAccessFile myRaf;
    protected Doradeheader headerParser;
    public DoradeSweep mySweep;
    boolean littleEndian;
    protected boolean fill;
    protected HashMap dimHash;
    
    public Doradeiosp() {
        this.mySweep = null;
        this.dimHash = new HashMap(50);
    }
    
    public boolean isValidFile(final RandomAccessFile raf) {
        return Doradeheader.isValidFile(raf);
    }
    
    public String getFileTypeId() {
        return "DORADE";
    }
    
    public String getFileTypeDescription() {
        return "DOppler RAdar Data Exchange Format";
    }
    
    @Override
    public void open(final RandomAccessFile raf, final NetcdfFile ncfile, final CancelTask cancelTask) throws IOException {
        this.myRaf = raf;
        try {
            this.mySweep = new DoradeSweep(raf.getRandomAccessFile());
        }
        catch (DoradeSweep.DoradeSweepException ex) {
            ex.printStackTrace();
        }
        catch (IOException ex2) {
            ex2.printStackTrace();
        }
        while (true) {
            if (this.mySweep.getScanMode(0) != ScanMode.MODE_SUR) {
                try {
                    (this.headerParser = new Doradeheader()).read(this.mySweep, ncfile, null);
                }
                catch (DoradeSweep.DoradeSweepException e) {
                    e.printStackTrace();
                }
                ncfile.finish();
                return;
            }
            continue;
        }
    }
    
    public Array readData(final Variable v2, final Section section) throws IOException, InvalidRangeException {
        final int nSensor = this.mySweep.getNSensors();
        final int nRays = this.mySweep.getNRays();
        Array outputData;
        if (v2.getName().equals("elevation")) {
            final float[] elev = this.mySweep.getElevations();
            outputData = this.readData1(v2, section, elev);
        }
        else if (v2.getName().equals("rays_time")) {
            final Date[] dd = this.mySweep.getTimes();
            final double[] d = new double[dd.length];
            for (int i = 0; i < dd.length; ++i) {
                d[i] = (double)dd[i].getTime();
            }
            outputData = this.readData2(v2, section, d);
        }
        else if (v2.getName().equals("azimuth")) {
            final float[] azim = this.mySweep.getAzimuths();
            outputData = this.readData1(v2, section, azim);
        }
        else if (v2.getName().startsWith("latitudes_")) {
            final float[] allLats = new float[nSensor * nRays];
            for (int i = 0; i < nSensor; ++i) {
                final float[] lats = this.mySweep.getLatitudes(i);
                System.arraycopy(lats, 0, allLats, i * nRays, nRays);
            }
            outputData = this.readData1(v2, section, allLats);
        }
        else if (v2.getName().startsWith("longitudes_")) {
            final float[] allLons = new float[nSensor * nRays];
            for (int i = 0; i < nSensor; ++i) {
                final float[] lons = this.mySweep.getLongitudes(i);
                System.arraycopy(lons, 0, allLons, i * nRays, nRays);
            }
            outputData = this.readData1(v2, section, allLons);
        }
        else if (v2.getName().startsWith("altitudes_")) {
            final float[] allAlts = new float[nSensor * nRays];
            for (int i = 0; i < nSensor; ++i) {
                final float[] alts = this.mySweep.getAltitudes(i);
                System.arraycopy(alts, 0, allAlts, i * nRays, nRays);
            }
            outputData = this.readData1(v2, section, allAlts);
        }
        else if (v2.getName().startsWith("distance_")) {
            int j = 0;
            for (int i = 0; i < nSensor; ++i) {
                final String t = "" + i;
                if (v2.getName().endsWith(t)) {
                    j = i;
                    break;
                }
            }
            final int nc = this.mySweep.getNCells(j);
            final Array data = Array.makeArray(DataType.FLOAT, nc, this.mySweep.getRangeToFirstCell(j), this.mySweep.getCellSpacing(j));
            final float[] dist = (float[])data.get1DJavaArray(Float.class);
            outputData = this.readData1(v2, section, dist);
        }
        else {
            if (!v2.isScalar()) {
                final Range radialRange = section.getRange(0);
                final Range gateRange = section.getRange(1);
                final Array data2 = Array.factory(v2.getDataType().getPrimitiveClassType(), section.getShape());
                final IndexIterator ii = data2.getIndexIterator();
                final DoradePARM dp = this.mySweep.lookupParamIgnoreCase(v2.getName());
                final int ncells = dp.getNCells();
                float[] rayValues = new float[ncells];
                for (int r = radialRange.first(); r <= radialRange.last(); r += radialRange.stride()) {
                    try {
                        rayValues = this.mySweep.getRayData(dp, r, rayValues);
                    }
                    catch (DoradeSweep.DoradeSweepException ex) {
                        ex.printStackTrace();
                    }
                    for (int k = gateRange.first(); k <= gateRange.last(); k += gateRange.stride()) {
                        ii.setFloatNext(rayValues[k]);
                    }
                }
                return data2;
            }
            float d2 = 0.0f;
            if (v2.getName().equals("Range_to_First_Cell")) {
                d2 = this.mySweep.getRangeToFirstCell(0);
            }
            else if (v2.getName().equals("Cell_Spacing")) {
                d2 = this.mySweep.getCellSpacing(0);
            }
            else if (v2.getName().equals("Fixed_Angle")) {
                d2 = this.mySweep.getFixedAngle();
            }
            else if (v2.getName().equals("Nyquist_Velocity")) {
                d2 = this.mySweep.getUnambiguousVelocity(0);
            }
            else if (v2.getName().equals("Unambiguous_Range")) {
                d2 = this.mySweep.getunambiguousRange(0);
            }
            else if (v2.getName().equals("Radar_Constant")) {
                d2 = this.mySweep.getradarConstant(0);
            }
            else if (v2.getName().equals("rcvr_gain")) {
                d2 = this.mySweep.getrcvrGain(0);
            }
            else if (v2.getName().equals("ant_gain")) {
                d2 = this.mySweep.getantennaGain(0);
            }
            else if (v2.getName().equals("sys_gain")) {
                d2 = this.mySweep.getsystemGain(0);
            }
            else if (v2.getName().equals("bm_width")) {
                d2 = this.mySweep.gethBeamWidth(0);
            }
            final float[] dd2 = { d2 };
            outputData = Array.factory(v2.getDataType().getPrimitiveClassType(), v2.getShape(), dd2);
        }
        return outputData;
    }
    
    public Array readData1(final Variable v2, final Section section, final float[] values) {
        final Array data = Array.factory(v2.getDataType().getPrimitiveClassType(), section.getShape());
        final IndexIterator ii = data.getIndexIterator();
        final Range radialRange = section.getRange(0);
        for (int r = radialRange.first(); r <= radialRange.last(); r += radialRange.stride()) {
            ii.setFloatNext(values[r]);
        }
        return data;
    }
    
    public Array readData2(final Variable v2, final Section section, final double[] values) {
        final Array data = Array.factory(v2.getDataType().getPrimitiveClassType(), section.getShape());
        final IndexIterator ii = data.getIndexIterator();
        final Range radialRange = section.getRange(0);
        for (int r = radialRange.first(); r <= radialRange.last(); r += radialRange.stride()) {
            ii.setDoubleNext(values[r]);
        }
        return data;
    }
    
    public void flush() throws IOException {
        this.myRaf.flush();
    }
    
    @Override
    public void close() throws IOException {
        this.myRaf.close();
    }
    
    public static void main(final String[] args) throws IOException, InstantiationException, IllegalAccessException {
        final String fileIn = "/home/yuanho/dorade/swp.1020511015815.SP0L.573.1.2_SUR_v1";
        NetcdfFile.registerIOProvider(Doradeiosp.class);
        final NetcdfFile ncf = NetcdfFile.open(fileIn);
        final RandomAccessFile file = new RandomAccessFile(fileIn, "r");
    }
}
