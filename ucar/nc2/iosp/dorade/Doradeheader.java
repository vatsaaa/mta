// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.iosp.dorade;

import java.util.Date;
import java.util.List;
import ucar.nc2.Structure;
import ucar.nc2.Variable;
import ucar.ma2.Array;
import java.io.IOException;
import ucar.atd.dorade.DoradePARM;
import ucar.ma2.DataType;
import ucar.nc2.Attribute;
import ucar.nc2.constants.AxisType;
import ucar.ma2.MAMath;
import java.util.ArrayList;
import ucar.nc2.Group;
import ucar.nc2.Dimension;
import ucar.atd.dorade.DoradeSweep;
import ucar.unidata.io.RandomAccessFile;
import java.util.HashMap;
import java.io.PrintStream;
import ucar.nc2.NetcdfFile;

public class Doradeheader
{
    private boolean debug;
    private boolean debugPos;
    private boolean debugString;
    private boolean debugHeaderSize;
    private NetcdfFile ncfile;
    private PrintStream out;
    private HashMap paramMap;
    private float[] lat_min;
    private float[] lat_max;
    private float[] lon_min;
    private float[] lon_max;
    private float[] hi_max;
    private float[] hi_min;
    
    public Doradeheader() {
        this.debug = false;
        this.debugPos = false;
        this.debugString = false;
        this.debugHeaderSize = false;
        this.out = System.out;
    }
    
    public static boolean isValidFile(final RandomAccessFile raf) {
        try {
            final java.io.RandomAccessFile file = raf.getRandomAccessFile();
            if (file == null) {
                return false;
            }
            final boolean t = DoradeSweep.isDoradeSweep(file);
            if (!t) {
                return false;
            }
        }
        catch (DoradeSweep.DoradeSweepException ex) {
            ex.printStackTrace();
            return false;
        }
        return true;
    }
    
    void read(final DoradeSweep mySweep, final NetcdfFile ncfile, final PrintStream out) throws IOException, DoradeSweep.DoradeSweepException {
        this.ncfile = ncfile;
        final DoradePARM[] parms = mySweep.getParamList();
        final int nRays = mySweep.getNRays();
        if (this.debug) {
            System.out.println(parms.length + " params in file");
        }
        final int numSensor = mySweep.getNSensors();
        final int[] ncells = new int[numSensor];
        final Dimension[] gateDim = new Dimension[numSensor];
        for (int i = 0; i < numSensor; ++i) {
            try {
                final int j = i + 1;
                ncells[i] = mySweep.getNCells(i);
                ncfile.addDimension(null, gateDim[i] = new Dimension("gate_" + j, ncells[i]));
            }
            catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        final ArrayList[] dims = new ArrayList[numSensor];
        final ArrayList dims2 = new ArrayList();
        final ArrayList[] dims3 = new ArrayList[numSensor];
        int nCells = mySweep.getNCells(0);
        final Dimension radialDim = new Dimension("radial", nRays);
        ncfile.addDimension(null, radialDim);
        for (int k = 0; k < numSensor; ++k) {
            dims[k] = new ArrayList();
            dims3[k] = new ArrayList();
            dims[k].add(radialDim);
            dims[k].add(gateDim[k]);
            dims3[k].add(gateDim[k]);
        }
        dims2.add(radialDim);
        final float[][] altitudes = new float[numSensor][];
        final float[][] latitudes = new float[numSensor][];
        final float[][] longitudes = new float[numSensor][];
        this.lat_min = new float[numSensor];
        this.lat_max = new float[numSensor];
        this.lon_min = new float[numSensor];
        this.lon_max = new float[numSensor];
        this.hi_min = new float[numSensor];
        this.hi_max = new float[numSensor];
        final MAMath.MinMax[] latMinMax = new MAMath.MinMax[numSensor];
        final MAMath.MinMax[] lonMinMax = new MAMath.MinMax[numSensor];
        final MAMath.MinMax[] hiMinMax = new MAMath.MinMax[numSensor];
        final boolean[] isMoving = new boolean[numSensor];
        for (int l = 0; l < numSensor; ++l) {
            try {
                final int nc = mySweep.getNCells(l);
                altitudes[l] = new float[nRays];
                latitudes[l] = new float[nRays];
                longitudes[l] = new float[nRays];
                isMoving[l] = mySweep.sensorIsMoving(l);
                altitudes[l] = mySweep.getAltitudes(l);
                latitudes[l] = mySweep.getLatitudes(l);
                longitudes[l] = mySweep.getLongitudes(l);
            }
            catch (Exception ex2) {
                ex2.printStackTrace();
            }
            latMinMax[l] = this.getMinMaxData(latitudes[l]);
            lonMinMax[l] = this.getMinMaxData(longitudes[l]);
            hiMinMax[l] = this.getMinMaxData(altitudes[l]);
            final float dis = (float)((mySweep.getRangeToFirstCell(l) + (mySweep.getNCells(l) - 1) * mySweep.getCellSpacing(l)) * 1.853 / 111.26 / 1000.0);
            this.lat_min[l] = (float)(latMinMax[l].min - dis);
            this.lat_max[l] = (float)(latMinMax[l].max + dis);
            this.lon_min[l] = (float)(lonMinMax[l].min + dis * Math.cos(latitudes[l][0]));
            this.lon_max[l] = (float)(lonMinMax[l].max - dis * Math.cos(latitudes[l][0]));
            this.hi_min[l] = (float)hiMinMax[l].min;
            this.hi_max[l] = (float)hiMinMax[l].max;
        }
        this.addNCAttributes(ncfile, mySweep);
        String vName = "elevation";
        String lName = "elevation angle in degres: 0 = parallel to pedestal base, 90 = perpendicular";
        Attribute att = new Attribute("_CoordinateAxisType", AxisType.RadialElevation.toString());
        this.addParameter(vName, lName, ncfile, dims2, att, DataType.FLOAT, "degrees");
        vName = "azimuth";
        lName = "azimuth angle in degrees: 0 = true north, 90 = east";
        att = new Attribute("_CoordinateAxisType", AxisType.RadialAzimuth.toString());
        this.addParameter(vName, lName, ncfile, dims2, att, DataType.FLOAT, "degrees");
        for (int m = 0; m < numSensor; ++m) {
            final int j2 = m + 1;
            vName = "distance_" + j2;
            lName = "Radial distance to the start of gate";
            att = new Attribute("_CoordinateAxisType", AxisType.RadialDistance.toString());
            this.addParameter(vName, lName, ncfile, dims3[m], att, DataType.FLOAT, "meters");
        }
        for (int m = 0; m < numSensor; ++m) {
            final int j2 = m + 1;
            vName = "latitudes_" + j2;
            lName = "Latitude of the instrument " + j2;
            att = new Attribute("_CoordinateAxisType", AxisType.Lat.toString());
            this.addParameter(vName, lName, ncfile, dims2, att, DataType.FLOAT, "degrees");
            vName = "longitudes_" + j2;
            lName = "Longitude of the instrument " + j2;
            att = new Attribute("_CoordinateAxisType", AxisType.Lon.toString());
            this.addParameter(vName, lName, ncfile, dims2, att, DataType.FLOAT, "degrees");
            vName = "altitudes_" + j2;
            lName = "Altitude in meters (asl) of the instrument " + j2;
            att = new Attribute("_CoordinateAxisType", AxisType.Height.toString());
            this.addParameter(vName, lName, ncfile, dims2, att, DataType.FLOAT, "meters");
            vName = "rays_time";
            lName = "rays time";
            att = new Attribute("_CoordinateAxisType", AxisType.Time.toString());
            this.addParameter(vName, lName, ncfile, dims2, att, DataType.DOUBLE, "milliseconds since 1970-01-01 00:00 UTC");
        }
        vName = "Range_to_First_Cell";
        lName = "Range to the center of the first cell";
        att = new Attribute("_CoordinateAxes", "latitudes_1 longitudes_1 altitudes_1");
        this.addParameter(vName, lName, ncfile, null, null, DataType.FLOAT, "meters");
        vName = "Cell_Spacing";
        lName = "Distance between cells";
        this.addParameter(vName, lName, ncfile, null, null, DataType.FLOAT, "meters");
        vName = "Fixed_Angle";
        lName = "Targeted fixed angle for this scan";
        this.addParameter(vName, lName, ncfile, null, null, DataType.FLOAT, "degrees");
        vName = "Nyquist_Velocity";
        lName = "Effective unambigous velocity";
        this.addParameter(vName, lName, ncfile, null, null, DataType.FLOAT, "m/s");
        vName = "Unambiguous_Range";
        lName = "Effective unambigous range";
        this.addParameter(vName, lName, ncfile, null, null, DataType.FLOAT, "meters");
        vName = "Radar_Constant";
        lName = "Radar constant";
        this.addParameter(vName, lName, ncfile, null, null, DataType.FLOAT, "c");
        vName = "rcvr_gain";
        lName = "Receiver Gain";
        this.addParameter(vName, lName, ncfile, null, null, DataType.FLOAT, "db");
        vName = "ant_gain";
        lName = "Antenna Gain";
        this.addParameter(vName, lName, ncfile, null, null, DataType.FLOAT, "db");
        vName = "sys_gain";
        lName = "System Gain";
        this.addParameter(vName, lName, ncfile, null, null, DataType.FLOAT, "db");
        vName = "bm_width";
        lName = "Beam Width";
        this.addParameter(vName, lName, ncfile, null, null, DataType.FLOAT, "degrees");
        try {
            for (int p = 0; p < parms.length; ++p) {
                final String pval = parms[p].getDescription();
                final int ptype = parms[p].getBinaryFormat();
                nCells = parms[p].getNCells();
                final int ii = this.getGateDimsIndex(nCells, gateDim, numSensor);
                if (this.debug) {
                    System.out.println("Param " + p + " name " + pval + " and ncel " + nCells);
                }
                this.addVariable(ncfile, dims[ii], parms[p]);
            }
        }
        catch (Exception ex3) {
            ex3.printStackTrace();
        }
        ncfile.finish();
    }
    
    public MAMath.MinMax getMinMaxData(final float[] data) {
        final int[] shape = { data.length };
        final Array a = Array.factory(DataType.FLOAT.getPrimitiveClassType(), shape, data);
        return MAMath.getMinMax(a);
    }
    
    int getGateDimsIndex(final int cell, final Dimension[] dList, final int numSensor) {
        int j = 0;
        for (int i = 0; i < numSensor; ++i) {
            final Dimension d = new Dimension("gate_" + i, cell);
            if (dList[i].equals(d)) {
                j = i;
                break;
            }
        }
        return j;
    }
    
    private void makeCoordinateData(final Variable elev, final Variable azim, final DoradeSweep mySweep) {
        final Object ele = mySweep.getElevations();
        final Object azi = mySweep.getAzimuths();
        final Array elevData = Array.factory(elev.getDataType().getPrimitiveClassType(), elev.getShape(), ele);
        final Array aziData = Array.factory(azim.getDataType().getPrimitiveClassType(), azim.getShape(), azi);
        elev.setCachedData(elevData, false);
        azim.setCachedData(aziData, false);
    }
    
    void addParameter(final String pName, final String longName, final NetcdfFile nc, final ArrayList dims, final Attribute att, final DataType dtype, final String ut) {
        final String vName = pName;
        final Variable vVar = new Variable(nc, null, null, vName);
        vVar.setDataType(dtype);
        if (dims != null) {
            vVar.setDimensions(dims);
        }
        else {
            vVar.setDimensions("");
        }
        if (att != null) {
            vVar.addAttribute(att);
        }
        vVar.addAttribute(new Attribute("units", ut));
        vVar.addAttribute(new Attribute("long_name", longName));
        nc.addVariable(null, vVar);
    }
    
    void addVariable(final NetcdfFile nc, final ArrayList dims, final DoradePARM dparm) {
        final Variable v = new Variable(nc, null, null, dparm.getName());
        v.setDataType(DataType.FLOAT);
        v.setDimensions(dims);
        this.ncfile.addVariable(null, v);
        v.addAttribute(new Attribute("long_name", dparm.getDescription()));
        v.addAttribute(new Attribute("units", dparm.getUnits()));
        final String coordinates = "elevation azimuth distance_1 latitudes_1 longitudes_1 altitudes_1";
        v.addAttribute(new Attribute("_CoordinateAxes", coordinates));
    }
    
    void addNCAttributes(final NetcdfFile nc, final DoradeSweep mySweep) throws DoradeSweep.DoradeSweepException {
        nc.addAttribute(null, new Attribute("summary", "Dorade radar data from radar " + mySweep.getSensorName(0) + " in the project " + mySweep.getProjectName()));
        nc.addAttribute(null, new Attribute("radar_name", mySweep.getSensorName(0)));
        nc.addAttribute(null, new Attribute("project_name", mySweep.getProjectName()));
        nc.addAttribute(null, new Attribute("keywords_vocabulary", "dorade"));
        nc.addAttribute(null, new Attribute("geospatial_lat_min", new Float(this.lat_min[0])));
        nc.addAttribute(null, new Attribute("geospatial_lat_max", new Float(this.lat_max[0])));
        nc.addAttribute(null, new Attribute("geospatial_lon_min", new Float(this.lon_min[0])));
        nc.addAttribute(null, new Attribute("geospatial_lon_max", new Float(this.lon_max[0])));
        nc.addAttribute(null, new Attribute("geospatial_vertical_min", new Float(this.lon_min[0])));
        nc.addAttribute(null, new Attribute("geospatial_vertical_max", new Float(this.lon_max[0])));
        final Date[] dd = mySweep.getTimes();
        nc.addAttribute(null, new Attribute("time_coverage_start", dd[0].toString()));
        nc.addAttribute(null, new Attribute("time_coverage_end", dd[dd.length - 1].toString()));
        nc.addAttribute(null, new Attribute("Content", "This file contains one scan of remotely sensed data"));
        nc.addAttribute(null, new Attribute("Conventions", "_Coordinates"));
        nc.addAttribute(null, new Attribute("format", "Unidata/netCDF/Dorade"));
        nc.addAttribute(null, new Attribute("Radar_Name", mySweep.getSensorName(0)));
        nc.addAttribute(null, new Attribute("Project_name", "" + mySweep.getProjectName()));
        nc.addAttribute(null, new Attribute("VolumeCoveragePatternName", mySweep.getScanMode(0).getName()));
        nc.addAttribute(null, new Attribute("Volume_Number", "" + mySweep.getVolumnNumber()));
        nc.addAttribute(null, new Attribute("Sweep_Number", "" + mySweep.getSweepNumber()));
        nc.addAttribute(null, new Attribute("Sweep_Date", mySweep.getTime().toString()));
        if (mySweep.sensorIsMoving(0)) {
            nc.addAttribute(null, new Attribute("IsStationary", "0"));
        }
        else {
            nc.addAttribute(null, new Attribute("IsStationary", "1"));
        }
    }
    
    DataType getDataType(final int format) {
        DataType p = null;
        switch (format) {
            case 1: {
                p = DataType.SHORT;
                break;
            }
            case 2: {
                p = DataType.FLOAT;
                break;
            }
            case 3: {
                p = DataType.LONG;
                break;
            }
            case 4: {
                p = DataType.FLOAT;
                break;
            }
            case 5: {
                p = DataType.DOUBLE;
                break;
            }
            default: {
                p = null;
                break;
            }
        }
        return p;
    }
    
    class Vinfo
    {
        int vsize;
        long begin;
        boolean isRecord;
        int nx;
        int ny;
        
        Vinfo(final int vsize, final long begin, final boolean isRecord, final int x, final int y) {
            this.vsize = vsize;
            this.begin = begin;
            this.isRecord = isRecord;
            this.nx = x;
            this.ny = y;
        }
    }
}
