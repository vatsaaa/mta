// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.iosp;

import ucar.ma2.ArrayStructureW;
import ucar.ma2.StructureData;
import ucar.ma2.StructureDataIterator;
import ucar.ma2.ArraySequence;
import ucar.ma2.MAMath;
import java.util.List;
import ucar.nc2.Variable;
import ucar.ma2.ArrayStructure;
import ucar.nc2.Structure;
import ucar.ma2.Section;
import java.util.Collection;
import ucar.ma2.Range;
import java.util.ArrayList;
import ucar.nc2.ParsedSectionSpec;
import ucar.ma2.StructureMembers;
import ucar.ma2.InvalidRangeException;
import ucar.ma2.IndexIterator;
import java.io.OutputStream;
import ucar.nc2.stream.NcStream;
import java.io.DataOutputStream;
import java.nio.channels.Channels;
import ucar.ma2.ArrayStructureBB;
import java.nio.channels.WritableByteChannel;
import ucar.ma2.Array;
import java.nio.LongBuffer;
import java.nio.DoubleBuffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.nio.ShortBuffer;
import java.nio.ByteBuffer;
import ucar.unidata.io.PositioningDataInputStream;
import java.io.IOException;
import ucar.ma2.DataType;
import ucar.unidata.io.RandomAccessFile;

public class IospHelper
{
    private static boolean showLayoutTypes;
    
    public static Object readDataFill(final RandomAccessFile raf, final Layout index, final DataType dataType, final Object fillValue, final int byteOrder) throws IOException {
        final Object arr = (fillValue == null) ? makePrimitiveArray((int)index.getTotalNelems(), dataType) : makePrimitiveArray((int)index.getTotalNelems(), dataType, fillValue);
        return readData(raf, index, dataType, arr, byteOrder, true);
    }
    
    public static Object readDataFill(final RandomAccessFile raf, final Layout index, final DataType dataType, final Object fillValue, final int byteOrder, final boolean convertChar) throws IOException {
        final Object arr = (fillValue == null) ? makePrimitiveArray((int)index.getTotalNelems(), dataType) : makePrimitiveArray((int)index.getTotalNelems(), dataType, fillValue);
        return readData(raf, index, dataType, arr, byteOrder, convertChar);
    }
    
    public static Object readData(final RandomAccessFile raf, final Layout layout, final DataType dataType, final Object arr, final int byteOrder, final boolean convertChar) throws IOException {
        if (IospHelper.showLayoutTypes) {
            System.out.println("***RAF LayoutType=" + layout.getClass().getName());
        }
        if (dataType == DataType.BYTE || dataType == DataType.CHAR || dataType == DataType.ENUM1) {
            final byte[] pa = (byte[])arr;
            while (layout.hasNext()) {
                final Layout.Chunk chunk = layout.next();
                raf.order(byteOrder);
                raf.seek(chunk.getSrcPos());
                raf.read(pa, (int)chunk.getDestElem(), chunk.getNelems());
            }
            if (convertChar && dataType == DataType.CHAR) {
                return convertByteToChar(pa);
            }
            return pa;
        }
        else {
            if (dataType == DataType.SHORT || dataType == DataType.ENUM2) {
                final short[] pa2 = (short[])arr;
                while (layout.hasNext()) {
                    final Layout.Chunk chunk = layout.next();
                    raf.order(byteOrder);
                    raf.seek(chunk.getSrcPos());
                    raf.readShort(pa2, (int)chunk.getDestElem(), chunk.getNelems());
                }
                return pa2;
            }
            if (dataType == DataType.INT || dataType == DataType.ENUM4) {
                final int[] pa3 = (int[])arr;
                while (layout.hasNext()) {
                    final Layout.Chunk chunk = layout.next();
                    raf.order(byteOrder);
                    raf.seek(chunk.getSrcPos());
                    raf.readInt(pa3, (int)chunk.getDestElem(), chunk.getNelems());
                }
                return pa3;
            }
            if (dataType == DataType.FLOAT) {
                final float[] pa4 = (float[])arr;
                while (layout.hasNext()) {
                    final Layout.Chunk chunk = layout.next();
                    raf.order(byteOrder);
                    raf.seek(chunk.getSrcPos());
                    raf.readFloat(pa4, (int)chunk.getDestElem(), chunk.getNelems());
                }
                return pa4;
            }
            if (dataType == DataType.DOUBLE) {
                final double[] pa5 = (double[])arr;
                while (layout.hasNext()) {
                    final Layout.Chunk chunk = layout.next();
                    raf.order(byteOrder);
                    raf.seek(chunk.getSrcPos());
                    raf.readDouble(pa5, (int)chunk.getDestElem(), chunk.getNelems());
                }
                return pa5;
            }
            if (dataType == DataType.LONG) {
                final long[] pa6 = (long[])arr;
                while (layout.hasNext()) {
                    final Layout.Chunk chunk = layout.next();
                    raf.order(byteOrder);
                    raf.seek(chunk.getSrcPos());
                    raf.readLong(pa6, (int)chunk.getDestElem(), chunk.getNelems());
                }
                return pa6;
            }
            if (dataType == DataType.STRUCTURE) {
                final byte[] pa = (byte[])arr;
                final int recsize = layout.getElemSize();
                while (layout.hasNext()) {
                    final Layout.Chunk chunk2 = layout.next();
                    raf.order(byteOrder);
                    raf.seek(chunk2.getSrcPos());
                    raf.read(pa, (int)chunk2.getDestElem() * recsize, chunk2.getNelems() * recsize);
                }
                return pa;
            }
            throw new IllegalStateException("unknown type= " + dataType);
        }
    }
    
    public static Object readDataFill(final PositioningDataInputStream is, final Layout index, final DataType dataType, final Object fillValue) throws IOException {
        final Object arr = (fillValue == null) ? makePrimitiveArray((int)index.getTotalNelems(), dataType) : makePrimitiveArray((int)index.getTotalNelems(), dataType, fillValue);
        return readData(is, index, dataType, arr);
    }
    
    public static Object readData(final PositioningDataInputStream raf, final Layout index, final DataType dataType, final Object arr) throws IOException {
        if (IospHelper.showLayoutTypes) {
            System.out.println("***PositioningDataInputStream LayoutType=" + index.getClass().getName());
        }
        if (dataType == DataType.BYTE || dataType == DataType.CHAR || dataType == DataType.OPAQUE || dataType == DataType.ENUM1) {
            final byte[] pa = (byte[])arr;
            while (index.hasNext()) {
                final Layout.Chunk chunk = index.next();
                raf.read(chunk.getSrcPos(), pa, (int)chunk.getDestElem(), chunk.getNelems());
            }
            if (dataType == DataType.CHAR) {
                return convertByteToChar(pa);
            }
            return pa;
        }
        else {
            if (dataType == DataType.SHORT || dataType == DataType.ENUM2) {
                final short[] pa2 = (short[])arr;
                while (index.hasNext()) {
                    final Layout.Chunk chunk = index.next();
                    raf.readShort(chunk.getSrcPos(), pa2, (int)chunk.getDestElem(), chunk.getNelems());
                }
                return pa2;
            }
            if (dataType == DataType.INT || dataType == DataType.ENUM4) {
                final int[] pa3 = (int[])arr;
                while (index.hasNext()) {
                    final Layout.Chunk chunk = index.next();
                    raf.readInt(chunk.getSrcPos(), pa3, (int)chunk.getDestElem(), chunk.getNelems());
                }
                return pa3;
            }
            if (dataType == DataType.FLOAT) {
                final float[] pa4 = (float[])arr;
                while (index.hasNext()) {
                    final Layout.Chunk chunk = index.next();
                    raf.readFloat(chunk.getSrcPos(), pa4, (int)chunk.getDestElem(), chunk.getNelems());
                }
                return pa4;
            }
            if (dataType == DataType.DOUBLE) {
                final double[] pa5 = (double[])arr;
                while (index.hasNext()) {
                    final Layout.Chunk chunk = index.next();
                    raf.readDouble(chunk.getSrcPos(), pa5, (int)chunk.getDestElem(), chunk.getNelems());
                }
                return pa5;
            }
            if (dataType == DataType.LONG) {
                final long[] pa6 = (long[])arr;
                while (index.hasNext()) {
                    final Layout.Chunk chunk = index.next();
                    raf.readLong(chunk.getSrcPos(), pa6, (int)chunk.getDestElem(), chunk.getNelems());
                }
                return pa6;
            }
            if (dataType == DataType.STRUCTURE) {
                final int recsize = index.getElemSize();
                final byte[] pa7 = (byte[])arr;
                while (index.hasNext()) {
                    final Layout.Chunk chunk2 = index.next();
                    raf.read(chunk2.getSrcPos(), pa7, (int)chunk2.getDestElem() * recsize, chunk2.getNelems() * recsize);
                }
                return pa7;
            }
            throw new IllegalStateException();
        }
    }
    
    public static Object readDataFill(final LayoutBB layout, final DataType dataType, final Object fillValue) throws IOException {
        long size = layout.getTotalNelems();
        if (dataType == DataType.STRUCTURE) {
            size *= layout.getElemSize();
        }
        final Object arr = (fillValue == null) ? makePrimitiveArray((int)size, dataType) : makePrimitiveArray((int)size, dataType, fillValue);
        return readData(layout, dataType, arr);
    }
    
    public static Object readData(final LayoutBB layout, final DataType dataType, final Object arr) throws IOException {
        if (IospHelper.showLayoutTypes) {
            System.out.println("***BB LayoutType=" + layout.getClass().getName());
        }
        if (dataType == DataType.BYTE || dataType == DataType.CHAR || dataType == DataType.ENUM1) {
            final byte[] pa = (byte[])arr;
            while (layout.hasNext()) {
                final LayoutBB.Chunk chunk = layout.next();
                final ByteBuffer bb = chunk.getByteBuffer();
                bb.position(chunk.getSrcElem());
                int pos = (int)chunk.getDestElem();
                for (int i = 0; i < chunk.getNelems(); ++i) {
                    pa[pos++] = bb.get();
                }
            }
            if (dataType == DataType.CHAR) {
                return convertByteToChar(pa);
            }
            return pa;
        }
        else {
            if (dataType == DataType.SHORT || dataType == DataType.ENUM2) {
                final short[] pa2 = (short[])arr;
                while (layout.hasNext()) {
                    final LayoutBB.Chunk chunk = layout.next();
                    final ShortBuffer buff = chunk.getShortBuffer();
                    buff.position(chunk.getSrcElem());
                    int pos = (int)chunk.getDestElem();
                    for (int i = 0; i < chunk.getNelems(); ++i) {
                        pa2[pos++] = buff.get();
                    }
                }
                return pa2;
            }
            if (dataType == DataType.INT || dataType == DataType.ENUM4) {
                final int[] pa3 = (int[])arr;
                while (layout.hasNext()) {
                    final LayoutBB.Chunk chunk = layout.next();
                    final IntBuffer buff2 = chunk.getIntBuffer();
                    buff2.position(chunk.getSrcElem());
                    int pos = (int)chunk.getDestElem();
                    for (int i = 0; i < chunk.getNelems(); ++i) {
                        pa3[pos++] = buff2.get();
                    }
                }
                return pa3;
            }
            if (dataType == DataType.FLOAT) {
                final float[] pa4 = (float[])arr;
                while (layout.hasNext()) {
                    final LayoutBB.Chunk chunk = layout.next();
                    final FloatBuffer buff3 = chunk.getFloatBuffer();
                    buff3.position(chunk.getSrcElem());
                    int pos = (int)chunk.getDestElem();
                    for (int i = 0; i < chunk.getNelems(); ++i) {
                        pa4[pos++] = buff3.get();
                    }
                }
                return pa4;
            }
            if (dataType == DataType.DOUBLE) {
                final double[] pa5 = (double[])arr;
                while (layout.hasNext()) {
                    final LayoutBB.Chunk chunk = layout.next();
                    final DoubleBuffer buff4 = chunk.getDoubleBuffer();
                    buff4.position(chunk.getSrcElem());
                    int pos = (int)chunk.getDestElem();
                    for (int i = 0; i < chunk.getNelems(); ++i) {
                        pa5[pos++] = buff4.get();
                    }
                }
                return pa5;
            }
            if (dataType == DataType.LONG) {
                final long[] pa6 = (long[])arr;
                while (layout.hasNext()) {
                    final LayoutBB.Chunk chunk = layout.next();
                    final LongBuffer buff5 = chunk.getLongBuffer();
                    buff5.position(chunk.getSrcElem());
                    int pos = (int)chunk.getDestElem();
                    for (int i = 0; i < chunk.getNelems(); ++i) {
                        pa6[pos++] = buff5.get();
                    }
                }
                return pa6;
            }
            if (dataType == DataType.STRUCTURE) {
                final byte[] pa = (byte[])arr;
                final int recsize = layout.getElemSize();
                while (layout.hasNext()) {
                    final LayoutBB.Chunk chunk2 = layout.next();
                    final ByteBuffer bb2 = chunk2.getByteBuffer();
                    bb2.position(chunk2.getSrcElem() * recsize);
                    int pos2 = (int)chunk2.getDestElem() * recsize;
                    for (int j = 0; j < chunk2.getNelems() * recsize; ++j) {
                        pa[pos2++] = bb2.get();
                    }
                }
                return pa;
            }
            throw new IllegalStateException();
        }
    }
    
    public static long copyToByteChannel(final Array data, final WritableByteChannel channel) throws IOException, InvalidRangeException {
        if (data instanceof ArrayStructureBB) {
            final ArrayStructureBB dataBB = (ArrayStructureBB)data;
            final ByteBuffer bb = dataBB.getByteBuffer();
            bb.rewind();
            channel.write(bb);
            return bb.limit();
        }
        final DataOutputStream outStream = new DataOutputStream(Channels.newOutputStream(channel));
        final IndexIterator iterA = data.getIndexIterator();
        final Class classType = data.getElementType();
        if (classType == Double.TYPE) {
            while (iterA.hasNext()) {
                outStream.writeDouble(iterA.getDoubleNext());
            }
        }
        else if (classType == Float.TYPE) {
            while (iterA.hasNext()) {
                outStream.writeFloat(iterA.getFloatNext());
            }
        }
        else if (classType == Long.TYPE) {
            while (iterA.hasNext()) {
                outStream.writeLong(iterA.getLongNext());
            }
        }
        else if (classType == Integer.TYPE) {
            while (iterA.hasNext()) {
                outStream.writeInt(iterA.getIntNext());
            }
        }
        else if (classType == Short.TYPE) {
            while (iterA.hasNext()) {
                outStream.writeShort(iterA.getShortNext());
            }
        }
        else if (classType == Character.TYPE) {
            while (iterA.hasNext()) {
                outStream.writeChar(iterA.getCharNext());
            }
        }
        else if (classType == Byte.TYPE) {
            while (iterA.hasNext()) {
                outStream.writeByte(iterA.getByteNext());
            }
        }
        else if (classType == Boolean.TYPE) {
            while (iterA.hasNext()) {
                outStream.writeBoolean(iterA.getBooleanNext());
            }
        }
        else {
            if (classType == String.class) {
                long size = 0L;
                while (iterA.hasNext()) {
                    final String s = (String)iterA.getObjectNext();
                    size += NcStream.writeVInt(outStream, s.length());
                    final byte[] b = s.getBytes("UTF-8");
                    outStream.write(b);
                    size += b.length;
                }
                return size;
            }
            if (classType == ByteBuffer.class) {
                long size = 0L;
                while (iterA.hasNext()) {
                    final ByteBuffer bb2 = (ByteBuffer)iterA.getObjectNext();
                    size += NcStream.writeVInt(outStream, bb2.limit());
                    bb2.rewind();
                    channel.write(bb2);
                    size += bb2.limit();
                }
                return size;
            }
            throw new UnsupportedOperationException("Class type = " + classType.getName());
        }
        return data.getSizeBytes();
    }
    
    public static void copyFromByteBuffer(final ByteBuffer bb, final StructureMembers.Member m, final IndexIterator result) {
        final int offset = m.getDataParam();
        final int count = m.getSize();
        final DataType dtype = m.getDataType();
        if (dtype == DataType.FLOAT) {
            for (int i = 0; i < count; ++i) {
                result.setFloatNext(bb.getFloat(offset + i * 4));
            }
        }
        else if (dtype == DataType.DOUBLE) {
            for (int i = 0; i < count; ++i) {
                result.setDoubleNext(bb.getDouble(offset + i * 8));
            }
        }
        else if (dtype == DataType.INT || dtype == DataType.ENUM4) {
            for (int i = 0; i < count; ++i) {
                result.setIntNext(bb.getInt(offset + i * 4));
            }
        }
        else if (dtype == DataType.SHORT || dtype == DataType.ENUM2) {
            for (int i = 0; i < count; ++i) {
                result.setShortNext(bb.getShort(offset + i * 2));
            }
        }
        else if (dtype == DataType.BYTE || dtype == DataType.ENUM1) {
            for (int i = 0; i < count; ++i) {
                result.setByteNext(bb.get(offset + i));
            }
        }
        else if (dtype == DataType.CHAR) {
            for (int i = 0; i < count; ++i) {
                result.setCharNext((char)bb.get(offset + i));
            }
        }
        else {
            if (dtype != DataType.LONG) {
                throw new IllegalStateException();
            }
            for (int i = 0; i < count; ++i) {
                result.setLongNext(bb.get(offset + i * 8));
            }
        }
    }
    
    public static Object makePrimitiveArray(final int size, final DataType dataType) {
        Object arr = null;
        if (dataType == DataType.BYTE || dataType == DataType.CHAR || dataType == DataType.ENUM1 || dataType == DataType.OPAQUE || dataType == DataType.STRUCTURE) {
            arr = new byte[size];
        }
        else if (dataType == DataType.SHORT || dataType == DataType.ENUM2) {
            arr = new short[size];
        }
        else if (dataType == DataType.INT || dataType == DataType.ENUM4) {
            arr = new int[size];
        }
        else if (dataType == DataType.LONG) {
            arr = new long[size];
        }
        else if (dataType == DataType.FLOAT) {
            arr = new float[size];
        }
        else if (dataType == DataType.DOUBLE) {
            arr = new double[size];
        }
        return arr;
    }
    
    public static Object makePrimitiveArray(final int size, final DataType dataType, final Object fillValue) {
        if (dataType == DataType.BYTE || dataType == DataType.CHAR || dataType == DataType.ENUM1) {
            final byte[] pa = new byte[size];
            final byte val = (byte)fillValue;
            if (val != 0) {
                for (int i = 0; i < size; ++i) {
                    pa[i] = val;
                }
            }
            return pa;
        }
        if (dataType == DataType.OPAQUE) {
            return new byte[size];
        }
        if (dataType == DataType.SHORT || dataType == DataType.ENUM2) {
            final short[] pa2 = new short[size];
            final short val2 = (short)fillValue;
            if (val2 != 0) {
                for (int i = 0; i < size; ++i) {
                    pa2[i] = val2;
                }
            }
            return pa2;
        }
        if (dataType == DataType.INT || dataType == DataType.ENUM4) {
            final int[] pa3 = new int[size];
            final int val3 = (int)fillValue;
            if (val3 != 0) {
                for (int i = 0; i < size; ++i) {
                    pa3[i] = val3;
                }
            }
            return pa3;
        }
        if (dataType == DataType.LONG) {
            final long[] pa4 = new long[size];
            final long val4 = (long)fillValue;
            if (val4 != 0L) {
                for (int j = 0; j < size; ++j) {
                    pa4[j] = val4;
                }
            }
            return pa4;
        }
        if (dataType == DataType.FLOAT) {
            final float[] pa5 = new float[size];
            final float val5 = (float)fillValue;
            if (val5 != 0.0) {
                for (int i = 0; i < size; ++i) {
                    pa5[i] = val5;
                }
            }
            return pa5;
        }
        if (dataType == DataType.DOUBLE) {
            final double[] pa6 = new double[size];
            final double val6 = (double)fillValue;
            if (val6 != 0.0) {
                for (int j = 0; j < size; ++j) {
                    pa6[j] = val6;
                }
            }
            return pa6;
        }
        if (dataType == DataType.STRING) {
            final String[] pa7 = new String[size];
            for (int k = 0; k < size; ++k) {
                pa7[k] = (String)fillValue;
            }
            return pa7;
        }
        if (dataType == DataType.STRUCTURE) {
            final byte[] pa = new byte[size];
            final byte[] val7 = (byte[])fillValue;
            int count = 0;
            while (count < size) {
                for (int j = 0; j < val7.length; ++j) {
                    pa[count++] = val7[j];
                }
            }
            return pa;
        }
        throw new IllegalStateException();
    }
    
    public static char[] convertByteToChar(final byte[] byteArray) {
        final int size = byteArray.length;
        final char[] cbuff = new char[size];
        for (int i = 0; i < size; ++i) {
            cbuff[i] = (char)DataType.unsignedByteToShort(byteArray[i]);
        }
        return cbuff;
    }
    
    public static byte[] convertCharToByte(final char[] from) {
        final int size = from.length;
        final byte[] to = new byte[size];
        for (int i = 0; i < size; ++i) {
            to[i] = (byte)from[i];
        }
        return to;
    }
    
    public static long transferData(final Array result, final WritableByteChannel channel) throws IOException, InvalidRangeException {
        final DataOutputStream outStream = new DataOutputStream(Channels.newOutputStream(channel));
        final IndexIterator iterA = result.getIndexIterator();
        final Class classType = result.getElementType();
        if (classType == Double.TYPE) {
            while (iterA.hasNext()) {
                outStream.writeDouble(iterA.getDoubleNext());
            }
        }
        else if (classType == Float.TYPE) {
            while (iterA.hasNext()) {
                outStream.writeFloat(iterA.getFloatNext());
            }
        }
        else if (classType == Long.TYPE) {
            while (iterA.hasNext()) {
                outStream.writeLong(iterA.getLongNext());
            }
        }
        else if (classType == Integer.TYPE) {
            while (iterA.hasNext()) {
                outStream.writeInt(iterA.getIntNext());
            }
        }
        else if (classType == Short.TYPE) {
            while (iterA.hasNext()) {
                outStream.writeShort(iterA.getShortNext());
            }
        }
        else if (classType == Character.TYPE) {
            while (iterA.hasNext()) {
                outStream.writeChar(iterA.getCharNext());
            }
        }
        else if (classType == Byte.TYPE) {
            while (iterA.hasNext()) {
                outStream.writeByte(iterA.getByteNext());
            }
        }
        else {
            if (classType != Boolean.TYPE) {
                throw new UnsupportedOperationException("Class type = " + classType.getName());
            }
            while (iterA.hasNext()) {
                outStream.writeBoolean(iterA.getBooleanNext());
            }
        }
        return 0L;
    }
    
    public static Array readSection(final ParsedSectionSpec cer) throws IOException, InvalidRangeException {
        Variable inner = null;
        final List<Range> totalRanges = new ArrayList<Range>();
        for (ParsedSectionSpec current = cer; current != null; current = current.child) {
            totalRanges.addAll(current.section.getRanges());
            inner = current.v;
        }
        final Section total = new Section(totalRanges);
        final Array result = Array.factory(inner.getDataType(), total.getShape());
        final Structure outer = (Structure)cer.v;
        final Structure outerSubset = outer.select(cer.child.v.getShortName());
        final ArrayStructure outerData = (ArrayStructure)outerSubset.read(cer.section);
        extractSection(cer.child, outerData, result.getIndexIterator());
        result.setUnsigned(cer.v.isUnsigned());
        return result;
    }
    
    private static void extractSection(final ParsedSectionSpec child, final ArrayStructure outerData, final IndexIterator to) throws IOException, InvalidRangeException {
        final long wantNelems = child.section.computeSize();
        final StructureMembers.Member m = outerData.findMember(child.v.getShortName());
        for (int recno = 0; recno < outerData.getSize(); ++recno) {
            Array innerData = outerData.getArray(recno, m);
            if (child.child == null) {
                if (wantNelems != innerData.getSize()) {
                    innerData = innerData.section(child.section.getRanges());
                }
                MAMath.copy(child.v.getDataType(), innerData.getIndexIterator(), to);
            }
            else if (innerData instanceof ArraySequence) {
                extractSectionFromSequence(child.child, (ArraySequence)innerData, to);
            }
            else {
                if (wantNelems != innerData.getSize()) {
                    innerData = sectionArrayStructure(child, (ArrayStructure)innerData, m);
                }
                extractSection(child.child, (ArrayStructure)innerData, to);
            }
        }
    }
    
    private static void extractSectionFromSequence(final ParsedSectionSpec child, final ArraySequence outerData, final IndexIterator to) throws IOException, InvalidRangeException {
        final StructureDataIterator sdataIter = outerData.getStructureDataIterator();
        while (sdataIter.hasNext()) {
            final StructureData sdata = sdataIter.next();
            final StructureMembers.Member m = outerData.findMember(child.v.getShortName());
            final Array innerData = sdata.getArray(child.v.getShortName());
            MAMath.copy(m.getDataType(), innerData.getIndexIterator(), to);
        }
    }
    
    private static ArrayStructure sectionArrayStructure(final ParsedSectionSpec child, final ArrayStructure innerData, final StructureMembers.Member m) throws IOException, InvalidRangeException {
        final StructureMembers membersw = new StructureMembers(m.getStructureMembers());
        final ArrayStructureW result = new ArrayStructureW(membersw, child.section.getShape());
        int count = 0;
        final Section.Iterator iter = child.section.getIterator(child.v.getShape());
        while (iter.hasNext()) {
            final int recno = iter.next();
            final StructureData sd = innerData.getStructureData(recno);
            result.setStructureData(sd, count++);
        }
        return result;
    }
    
    static {
        IospHelper.showLayoutTypes = false;
    }
}
