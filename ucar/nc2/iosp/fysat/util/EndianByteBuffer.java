// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.iosp.fysat.util;

import java.io.UnsupportedEncodingException;
import java.nio.ByteOrder;
import java.nio.ByteBuffer;

public class EndianByteBuffer
{
    public static short LITTLE_ENDIAN;
    public static short BIG_ENDIAN;
    private short awxEndian;
    private ByteBuffer byteBuffer;
    
    public EndianByteBuffer(final byte[] byteArray, final short endian) {
        this.awxEndian = EndianByteBuffer.LITTLE_ENDIAN;
        if (endian == 0) {
            this.awxEndian = EndianByteBuffer.LITTLE_ENDIAN;
        }
        else {
            this.awxEndian = EndianByteBuffer.BIG_ENDIAN;
        }
        (this.byteBuffer = ByteBuffer.allocate(byteArray.length)).put(byteArray);
        this.byteBuffer.position(0);
    }
    
    public EndianByteBuffer(final byte[] byteArray) {
        this(byteArray, EndianByteBuffer.LITTLE_ENDIAN);
    }
    
    public void setEndian(final short endian) {
        if (endian == 0) {
            this.awxEndian = EndianByteBuffer.LITTLE_ENDIAN;
        }
        else {
            this.awxEndian = EndianByteBuffer.BIG_ENDIAN;
        }
    }
    
    public int getInt() {
        int v;
        if (this.awxEndian == EndianByteBuffer.LITTLE_ENDIAN) {
            v = this.byteBuffer.order(ByteOrder.LITTLE_ENDIAN).getInt();
        }
        else {
            v = this.byteBuffer.getInt();
        }
        return v;
    }
    
    public short getShort() {
        short v;
        if (this.awxEndian == EndianByteBuffer.LITTLE_ENDIAN) {
            v = this.byteBuffer.order(ByteOrder.LITTLE_ENDIAN).getShort();
        }
        else {
            v = this.byteBuffer.getShort();
        }
        return v;
    }
    
    public long getLong() {
        long v;
        if (this.awxEndian == EndianByteBuffer.LITTLE_ENDIAN) {
            v = this.byteBuffer.order(ByteOrder.LITTLE_ENDIAN).getLong();
        }
        else {
            v = this.byteBuffer.getLong();
        }
        return v;
    }
    
    public float getFloat() {
        float v;
        if (this.awxEndian == EndianByteBuffer.LITTLE_ENDIAN) {
            v = this.byteBuffer.order(ByteOrder.LITTLE_ENDIAN).getFloat();
        }
        else {
            v = this.byteBuffer.getFloat();
        }
        return v;
    }
    
    public double getDouble() {
        double v;
        if (this.awxEndian == EndianByteBuffer.LITTLE_ENDIAN) {
            v = this.byteBuffer.order(ByteOrder.LITTLE_ENDIAN).getDouble();
        }
        else {
            v = this.byteBuffer.getDouble();
        }
        return v;
    }
    
    public String getString(final int byteCount) {
        final byte[] buf = new byte[byteCount];
        this.byteBuffer.get(buf);
        String v = null;
        try {
            v = new String(buf, "US-ASCII");
        }
        catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return v;
    }
    
    public short[] getShortArray() {
        final int count = (this.byteBuffer.array().length - this.byteBuffer.position()) / 2;
        final short[] va = new short[count];
        if (this.awxEndian == EndianByteBuffer.LITTLE_ENDIAN) {
            for (int i = 0; i < count; ++i) {
                va[i] = this.byteBuffer.order(ByteOrder.LITTLE_ENDIAN).getShort();
            }
        }
        else {
            for (int i = 0; i < count; ++i) {
                va[i] = this.byteBuffer.getShort();
            }
        }
        return va;
    }
    
    public long[] getLongArray() {
        final int count = (this.byteBuffer.array().length - this.byteBuffer.position()) / 8;
        final long[] va = new long[count];
        if (this.awxEndian == EndianByteBuffer.LITTLE_ENDIAN) {
            for (int i = 0; i < count; ++i) {
                va[i] = this.byteBuffer.order(ByteOrder.LITTLE_ENDIAN).getLong();
            }
        }
        else {
            for (int i = 0; i < count; ++i) {
                va[i] = this.byteBuffer.getLong();
            }
        }
        return va;
    }
    
    public int[] getIntArray() {
        final int count = (this.byteBuffer.array().length - this.byteBuffer.position()) / 4;
        final int[] va = new int[count];
        if (this.awxEndian == EndianByteBuffer.LITTLE_ENDIAN) {
            for (int i = 0; i < count; ++i) {
                va[i] = this.byteBuffer.order(ByteOrder.LITTLE_ENDIAN).getInt();
            }
        }
        else {
            for (int i = 0; i < count; ++i) {
                va[i] = this.byteBuffer.getInt();
            }
        }
        return va;
    }
    
    public final void position(final int newPosition) {
        this.byteBuffer.position(newPosition);
    }
    
    static {
        EndianByteBuffer.LITTLE_ENDIAN = 0;
        EndianByteBuffer.BIG_ENDIAN = -1;
    }
}
