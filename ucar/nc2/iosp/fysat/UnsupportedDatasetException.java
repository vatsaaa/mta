// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.iosp.fysat;

import java.io.IOException;

public class UnsupportedDatasetException extends IOException
{
    private static final long serialVersionUID = 1L;
    
    public UnsupportedDatasetException() {
    }
    
    public UnsupportedDatasetException(final String s) {
        super(s);
    }
}
