// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.iosp.fysat;

public class AwxFileGridProductExtendedPart
{
    char[] sat2004FileName;
    char[] version;
    char[] manufacturers;
    char[] satelliteName;
    char[] instrument;
    char[] processProgram;
    char[] reserved1;
    char[] copyright;
    char[] fillLength;
    char[] fillData;
    
    public AwxFileGridProductExtendedPart() {
        this.sat2004FileName = new char[64];
        this.version = new char[8];
        this.manufacturers = new char[8];
        this.satelliteName = new char[8];
        this.instrument = new char[8];
        this.processProgram = new char[8];
        this.reserved1 = new char[8];
        this.copyright = new char[8];
        this.fillLength = new char[8];
        this.fillData = new char[234];
    }
}
