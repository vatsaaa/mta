// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.iosp.fysat;

import ucar.nc2.iosp.fysat.util.EndianByteBuffer;

public class AwxFileGeoSatelliteSecondHeader extends AwxFileSecondHeader
{
    String satelliteName;
    short year;
    short month;
    short day;
    short hour;
    short minute;
    short channel;
    short flagOfProjection;
    short widthOfImage;
    short heightOfImage;
    short scanLineNumberOfImageTopLeft;
    short pixelNumberOfImageTopLeft;
    short sampleRatio;
    float latitudeOfNorth;
    float latitudeOfSouth;
    float longitudeOfWest;
    float longitudeOfEast;
    float centerLatitudeOfProjection;
    float centerLongitudeOfProjection;
    float standardLatitude1;
    float standardLatitude2;
    short horizontalResolution;
    short verticalResolution;
    short overlapFlagGeoGrid;
    short overlapValueGeoGrid;
    short dataLengthOfColorTable;
    short dataLengthOfCalibration;
    short dataLengthOfGeolocation;
    short reserved;
    
    @Override
    public void fillHeader(final EndianByteBuffer byteBuffer) {
        this.satelliteName = byteBuffer.getString(8).trim();
        this.year = byteBuffer.getShort();
        this.month = byteBuffer.getShort();
        this.day = byteBuffer.getShort();
        this.hour = byteBuffer.getShort();
        this.minute = byteBuffer.getShort();
        this.channel = byteBuffer.getShort();
        this.flagOfProjection = byteBuffer.getShort();
        this.widthOfImage = byteBuffer.getShort();
        this.heightOfImage = byteBuffer.getShort();
        this.scanLineNumberOfImageTopLeft = byteBuffer.getShort();
        this.pixelNumberOfImageTopLeft = byteBuffer.getShort();
        this.sampleRatio = byteBuffer.getShort();
        this.latitudeOfNorth = byteBuffer.getShort() / 100.0f;
        this.latitudeOfSouth = byteBuffer.getShort() / 100.0f;
        this.longitudeOfWest = byteBuffer.getShort() / 100.0f;
        this.longitudeOfEast = byteBuffer.getShort() / 100.0f;
        this.centerLatitudeOfProjection = byteBuffer.getShort() / 100.0f;
        this.centerLongitudeOfProjection = byteBuffer.getShort() / 100.0f;
        this.standardLatitude1 = byteBuffer.getShort() / 100.0f;
        this.standardLatitude2 = byteBuffer.getShort() / 100.0f;
        this.horizontalResolution = byteBuffer.getShort();
        this.verticalResolution = byteBuffer.getShort();
        this.overlapFlagGeoGrid = byteBuffer.getShort();
        this.overlapValueGeoGrid = byteBuffer.getShort();
        this.dataLengthOfColorTable = byteBuffer.getShort();
        this.dataLengthOfCalibration = byteBuffer.getShort();
        this.dataLengthOfGeolocation = byteBuffer.getShort();
        this.reserved = byteBuffer.getShort();
    }
}
