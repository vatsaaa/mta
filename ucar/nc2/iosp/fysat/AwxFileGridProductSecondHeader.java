// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.iosp.fysat;

import ucar.nc2.iosp.fysat.util.EndianByteBuffer;

public final class AwxFileGridProductSecondHeader extends AwxFileSecondHeader
{
    String satelliteName;
    short gridFeature;
    short byteAmountofData;
    short dataBaseValue;
    short dataScale;
    short timePeriodCode;
    short startYear;
    short startMonth;
    short startDay;
    short startHour;
    short startMinute;
    short endYear;
    short endMonth;
    short endDay;
    short endHour;
    short endMinute;
    float leftTopLat;
    float leftTopLon;
    float rightBottomLat;
    float rightBottomLon;
    short gridSpacingUnit;
    short horizontalSpacing;
    short verticalSpacing;
    short amountofHorizontalSpacing;
    short amountofVerticalSpacing;
    short hasLand;
    short landCode;
    short hasCloud;
    short cloudCode;
    short hasWater;
    short waterCode;
    short hasIce;
    short iceCode;
    short hasQualityControl;
    short qualityControlCeiling;
    short qualityControlFloor;
    short reserved;
    public static short GRID_FEATURE_OCEAN_TEMPERATURE;
    public static short GRID_FEATURE_SEA_ICE_DISTRIBUTION;
    public static short GRID_FEATURE_SEA_ICE_DENSITY;
    public static short GRID_FEATURE_OUTGOING_LONGWAVE_RADIATION;
    public static short GRID_FEATURE_NORMALIZE_VEGETATIOIN_INDEX;
    public static short GRID_FEATURE_RATIO_VEGETATIOIN_INDEX;
    public static short GRID_FEATURE_SNOW_DISTRIBUTION;
    public static short GRID_FEATURE_SOIL_HUMIDITY;
    public static short GRID_FEATURE_SUNLIGHT_DURATION;
    public static short GRID_FEATURE_CLOUD_TOP_HEIGHT;
    public static short GRID_FEATURE_CLOUD_TOP_TEMPERATURE;
    public static short GRID_FEATURE_LOW_CLOUD_AMOUNT;
    public static short GRID_FEATURE_HIGH_CLOUD_AMOUNT;
    public static short GRID_FEATURE_RAIN_INDEX_PER_HOUR;
    public static short GRID_FEATURE_RAIN_INDEX_PER_6HOUR;
    public static short GRID_FEATURE_RAIN_INDEX_PER_12HOUR;
    public static short GRID_FEATURE_RAIN_INDEX_PER_24HOUR;
    public static short GRID_SPACING_UNIT_POINT001DEGREE;
    public static short GRID_SPACING_UNIT_KILOMETER;
    public static short GRID_SPACING_UNIT_METER;
    
    @Override
    public void fillHeader(final EndianByteBuffer byteBuffer) {
        this.satelliteName = byteBuffer.getString(8).trim();
        this.gridFeature = byteBuffer.getShort();
        this.byteAmountofData = byteBuffer.getShort();
        this.dataBaseValue = byteBuffer.getShort();
        this.dataScale = byteBuffer.getShort();
        this.timePeriodCode = byteBuffer.getShort();
        this.startYear = byteBuffer.getShort();
        this.startMonth = byteBuffer.getShort();
        this.startDay = byteBuffer.getShort();
        this.startHour = byteBuffer.getShort();
        this.startMinute = byteBuffer.getShort();
        this.endYear = byteBuffer.getShort();
        this.endMonth = byteBuffer.getShort();
        this.endDay = byteBuffer.getShort();
        this.endHour = byteBuffer.getShort();
        this.endMinute = byteBuffer.getShort();
        this.leftTopLat = byteBuffer.getShort() / 100.0f;
        this.leftTopLon = byteBuffer.getShort() / 100.0f;
        this.rightBottomLat = byteBuffer.getShort() / 100.0f;
        this.rightBottomLon = byteBuffer.getShort() / 100.0f;
        this.gridSpacingUnit = byteBuffer.getShort();
        this.horizontalSpacing = byteBuffer.getShort();
        this.verticalSpacing = byteBuffer.getShort();
        this.amountofHorizontalSpacing = byteBuffer.getShort();
        this.amountofVerticalSpacing = byteBuffer.getShort();
        this.hasLand = byteBuffer.getShort();
        this.landCode = byteBuffer.getShort();
        this.hasCloud = byteBuffer.getShort();
        this.cloudCode = byteBuffer.getShort();
        this.hasWater = byteBuffer.getShort();
        this.waterCode = byteBuffer.getShort();
        this.hasIce = byteBuffer.getShort();
        this.iceCode = byteBuffer.getShort();
        this.hasQualityControl = byteBuffer.getShort();
        this.qualityControlCeiling = byteBuffer.getShort();
        this.qualityControlFloor = byteBuffer.getShort();
        this.reserved = byteBuffer.getShort();
    }
    
    public String getSpacingUnit() {
        switch (this.gridSpacingUnit) {
            case 0: {
                return "0.01degree";
            }
            case 1: {
                return "kilometer";
            }
            case 2: {
                return "meter";
            }
            default: {
                return "";
            }
        }
    }
    
    static {
        AwxFileGridProductSecondHeader.GRID_FEATURE_OCEAN_TEMPERATURE = 1;
        AwxFileGridProductSecondHeader.GRID_FEATURE_SEA_ICE_DISTRIBUTION = 2;
        AwxFileGridProductSecondHeader.GRID_FEATURE_SEA_ICE_DENSITY = 3;
        AwxFileGridProductSecondHeader.GRID_FEATURE_OUTGOING_LONGWAVE_RADIATION = 4;
        AwxFileGridProductSecondHeader.GRID_FEATURE_NORMALIZE_VEGETATIOIN_INDEX = 5;
        AwxFileGridProductSecondHeader.GRID_FEATURE_RATIO_VEGETATIOIN_INDEX = 6;
        AwxFileGridProductSecondHeader.GRID_FEATURE_SNOW_DISTRIBUTION = 7;
        AwxFileGridProductSecondHeader.GRID_FEATURE_SOIL_HUMIDITY = 8;
        AwxFileGridProductSecondHeader.GRID_FEATURE_SUNLIGHT_DURATION = 9;
        AwxFileGridProductSecondHeader.GRID_FEATURE_CLOUD_TOP_HEIGHT = 10;
        AwxFileGridProductSecondHeader.GRID_FEATURE_CLOUD_TOP_TEMPERATURE = 11;
        AwxFileGridProductSecondHeader.GRID_FEATURE_LOW_CLOUD_AMOUNT = 12;
        AwxFileGridProductSecondHeader.GRID_FEATURE_HIGH_CLOUD_AMOUNT = 13;
        AwxFileGridProductSecondHeader.GRID_FEATURE_RAIN_INDEX_PER_HOUR = 14;
        AwxFileGridProductSecondHeader.GRID_FEATURE_RAIN_INDEX_PER_6HOUR = 15;
        AwxFileGridProductSecondHeader.GRID_FEATURE_RAIN_INDEX_PER_12HOUR = 16;
        AwxFileGridProductSecondHeader.GRID_FEATURE_RAIN_INDEX_PER_24HOUR = 17;
        AwxFileGridProductSecondHeader.GRID_SPACING_UNIT_POINT001DEGREE = 0;
        AwxFileGridProductSecondHeader.GRID_SPACING_UNIT_KILOMETER = 1;
        AwxFileGridProductSecondHeader.GRID_SPACING_UNIT_METER = 2;
    }
}
