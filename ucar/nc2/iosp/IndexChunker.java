// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.iosp;

import ucar.ma2.Range;
import ucar.ma2.InvalidRangeException;
import java.util.Iterator;
import java.util.ArrayList;
import ucar.ma2.Section;
import java.util.List;

public class IndexChunker
{
    private static final boolean debug = false;
    private static final boolean debugMerge = false;
    private static final boolean debugNext = false;
    private List<Dim> dimList;
    private IndexLong chunkIndex;
    private Chunk chunk;
    private int nelems;
    private long start;
    private long total;
    private long done;
    
    public IndexChunker(final int[] srcShape, Section wantSection) throws InvalidRangeException {
        this.dimList = new ArrayList<Dim>();
        wantSection = Section.fill(wantSection, srcShape);
        this.total = wantSection.computeSize();
        this.done = 0L;
        this.start = 0L;
        if (wantSection.equivalent(srcShape)) {
            this.nelems = (int)this.total;
            this.chunkIndex = new IndexLong();
            return;
        }
        final int varRank = srcShape.length;
        long stride = 1L;
        for (int ii = varRank - 1; ii >= 0; --ii) {
            this.dimList.add(new Dim(stride, srcShape[ii], wantSection.getRange(ii)));
            stride *= srcShape[ii];
        }
        int merge = 0;
        for (int i = 0; i < this.dimList.size() - 1; ++i) {
            final Dim elem = this.dimList.get(i);
            final Dim elem2 = this.dimList.get(i + 1);
            if (elem.maxSize != elem.wantSize || elem2.want.stride() != 1) {
                break;
            }
            ++merge;
        }
        for (int i = 0; i < merge; ++i) {
            final Dim elem = this.dimList.get(i);
            final Dim dim3;
            final Dim elem2 = dim3 = this.dimList.get(i + 1);
            dim3.maxSize *= elem.maxSize;
            final Dim dim4 = elem2;
            dim4.wantSize *= elem.wantSize;
            if (elem2.wantSize < 0) {
                throw new IllegalArgumentException("array size may not exceed 2^31");
            }
        }
        for (int i = 0; i < merge; ++i) {
            this.dimList.remove(0);
        }
        if (varRank == 0 || this.dimList.get(0).want.stride() > 1) {
            this.nelems = 1;
        }
        else {
            final Dim innerDim = this.dimList.get(0);
            this.nelems = innerDim.wantSize;
            innerDim.wantSize = 1;
        }
        this.start = 0L;
        for (final Dim dim : this.dimList) {
            this.start += dim.stride * dim.want.first();
        }
        final int rank = this.dimList.size();
        final long[] wstride = new long[rank];
        final int[] shape = new int[rank];
        for (int j = 0; j < rank; ++j) {
            final Dim dim2 = this.dimList.get(j);
            wstride[rank - j - 1] = dim2.stride * dim2.want.stride();
            shape[rank - j - 1] = dim2.wantSize;
        }
        this.chunkIndex = new IndexLong(shape, wstride);
        assert IndexLong.computeSize(shape) * this.nelems == this.total;
    }
    
    public long getTotalNelems() {
        return this.total;
    }
    
    public boolean hasNext() {
        return this.done < this.total;
    }
    
    public Chunk next() {
        if (this.chunk == null) {
            this.chunk = new Chunk(this.start, this.nelems, 0L);
        }
        else {
            this.chunkIndex.incr();
            this.chunk.incrDestElem(this.nelems);
        }
        this.chunk.setSrcElem(this.start + this.chunkIndex.currentElement());
        this.done += this.nelems;
        return this.chunk;
    }
    
    @Override
    public String toString() {
        final StringBuilder sbuff = new StringBuilder();
        sbuff.append("wantSize=");
        for (int i = 0; i < this.dimList.size(); ++i) {
            final Dim elem = this.dimList.get(i);
            if (i > 0) {
                sbuff.append(",");
            }
            sbuff.append(elem.wantSize);
        }
        sbuff.append(" maxSize=");
        for (int i = 0; i < this.dimList.size(); ++i) {
            final Dim elem = this.dimList.get(i);
            if (i > 0) {
                sbuff.append(",");
            }
            sbuff.append(elem.maxSize);
        }
        sbuff.append(" wantStride=");
        for (int i = 0; i < this.dimList.size(); ++i) {
            final Dim elem = this.dimList.get(i);
            if (i > 0) {
                sbuff.append(",");
            }
            sbuff.append(elem.want.stride());
        }
        sbuff.append(" stride=");
        for (int i = 0; i < this.dimList.size(); ++i) {
            final Dim elem = this.dimList.get(i);
            if (i > 0) {
                sbuff.append(",");
            }
            sbuff.append(elem.stride);
        }
        return sbuff.toString();
    }
    
    protected static String printa(final int[] a) {
        final StringBuilder sbuff = new StringBuilder();
        for (int i = 0; i < a.length; ++i) {
            sbuff.append(a[i] + " ");
        }
        return sbuff.toString();
    }
    
    protected static void printa(final String name, final int[] a) {
        System.out.print(name + "= ");
        for (int i = 0; i < a.length; ++i) {
            System.out.print(a[i] + " ");
        }
        System.out.println();
    }
    
    protected static void printl(final String name, final long[] a) {
        System.out.print(name + "= ");
        for (int i = 0; i < a.length; ++i) {
            System.out.print(a[i] + " ");
        }
        System.out.println();
    }
    
    private class Dim
    {
        long stride;
        long maxSize;
        Range want;
        int wantSize;
        
        Dim(final long byteStride, final int maxSize, final Range want) {
            this.stride = byteStride;
            this.maxSize = maxSize;
            this.wantSize = want.length();
            this.want = want;
        }
    }
    
    public static class Chunk implements Layout.Chunk
    {
        private long srcElem;
        private int nelems;
        private long destElem;
        private long srcPos;
        
        public Chunk(final long srcElem, final int nelems, final long destElem) {
            this.srcElem = srcElem;
            this.nelems = nelems;
            this.destElem = destElem;
        }
        
        public long getSrcElem() {
            return this.srcElem;
        }
        
        public void setSrcElem(final long srcElem) {
            this.srcElem = srcElem;
        }
        
        public void incrSrcElem(final int incr) {
            this.srcElem += incr;
        }
        
        public int getNelems() {
            return this.nelems;
        }
        
        public void setNelems(final int nelems) {
            this.nelems = nelems;
        }
        
        public long getDestElem() {
            return this.destElem;
        }
        
        public void setDestElem(final long destElem) {
            this.destElem = destElem;
        }
        
        public void incrDestElem(final int incr) {
            this.destElem += incr;
        }
        
        @Override
        public String toString() {
            return " srcPos=" + this.srcPos + " srcElem=" + this.srcElem + " nelems=" + this.nelems + " destElem=" + this.destElem;
        }
        
        public long getSrcPos() {
            return this.srcPos;
        }
        
        public void setSrcPos(final long srcPos) {
            this.srcPos = srcPos;
        }
        
        public void incrSrcPos(final int incr) {
            this.srcPos += incr;
        }
    }
}
