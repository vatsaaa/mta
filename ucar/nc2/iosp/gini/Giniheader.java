// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.iosp.gini;

import org.slf4j.LoggerFactory;
import java.nio.DoubleBuffer;
import java.nio.FloatBuffer;
import java.nio.LongBuffer;
import java.nio.IntBuffer;
import java.nio.ShortBuffer;
import java.nio.ByteOrder;
import ucar.unidata.geoloc.ProjectionImpl;
import java.text.DateFormat;
import ucar.unidata.util.Parameter;
import ucar.unidata.geoloc.LatLonPoint;
import ucar.unidata.geoloc.LatLonPointImpl;
import ucar.unidata.geoloc.ProjectionPointImpl;
import java.util.List;
import java.util.ArrayList;
import ucar.nc2.constants.FeatureType;
import ucar.unidata.geoloc.projection.Stereographic;
import ucar.unidata.geoloc.projection.LambertConformal;
import ucar.unidata.geoloc.projection.Mercator;
import java.util.Date;
import ucar.nc2.units.DateFormatter;
import ucar.ma2.Array;
import ucar.nc2.constants.AxisType;
import ucar.ma2.DataType;
import ucar.nc2.Structure;
import ucar.nc2.Variable;
import ucar.nc2.Dimension;
import java.util.Calendar;
import java.util.TimeZone;
import java.text.SimpleDateFormat;
import ucar.nc2.Group;
import ucar.nc2.Attribute;
import java.nio.ByteBuffer;
import java.io.PrintStream;
import java.util.zip.DataFormatException;
import java.util.zip.Inflater;
import java.io.IOException;
import org.slf4j.Logger;
import ucar.nc2.NetcdfFile;
import ucar.unidata.io.RandomAccessFile;

class Giniheader
{
    static final byte[] MAGIC;
    static final int MAGIC_DIM = 10;
    static final int MAGIC_VAR = 11;
    static final int MAGIC_ATT = 12;
    private boolean debug;
    private boolean debugPos;
    private boolean debugString;
    private boolean debugHeaderSize;
    private RandomAccessFile raf;
    private NetcdfFile ncfile;
    private static Logger log;
    int numrecs;
    int recsize;
    int dataStart;
    int recStart;
    int GINI_PIB_LEN;
    int GINI_PDB_LEN;
    int GINI_HED_LEN;
    double DEG_TO_RAD;
    double EARTH_RAD_KMETERS;
    byte Z_DEFLATED;
    byte DEF_WBITS;
    private long actualSize;
    private long calcSize;
    protected int Z_type;
    
    Giniheader() {
        this.debug = false;
        this.debugPos = false;
        this.debugString = false;
        this.debugHeaderSize = false;
        this.numrecs = 0;
        this.recsize = 0;
        this.dataStart = 0;
        this.recStart = 0;
        this.GINI_PIB_LEN = 21;
        this.GINI_PDB_LEN = 512;
        this.GINI_HED_LEN = 533;
        this.DEG_TO_RAD = 0.017453292;
        this.EARTH_RAD_KMETERS = 6371.2;
        this.Z_DEFLATED = 8;
        this.DEF_WBITS = 15;
        this.Z_type = 0;
    }
    
    public boolean isValidFile(final RandomAccessFile raf) {
        try {
            return this.validatePIB(raf);
        }
        catch (IOException e) {
            return false;
        }
    }
    
    boolean validatePIB(final RandomAccessFile raf) throws IOException {
        this.raf = raf;
        this.actualSize = raf.length();
        int pos = 0;
        raf.seek(pos);
        final byte[] b = new byte[this.GINI_PIB_LEN + this.GINI_HED_LEN];
        raf.read(b);
        final String pib = new String(b);
        pos = pib.indexOf("KNES");
        if (pos == -1) {
            pos = pib.indexOf("CHIZ");
        }
        if (pos != -1) {
            pos = pib.indexOf("\r\r\n");
            if (pos != -1) {
                pos += 3;
            }
            return true;
        }
        pos = 0;
        return false;
    }
    
    byte[] readPIB(final RandomAccessFile raf) throws IOException {
        this.raf = raf;
        this.actualSize = raf.length();
        final int doff = 0;
        int pos = 0;
        raf.seek(pos);
        final byte[] b = new byte[this.GINI_PIB_LEN + this.GINI_HED_LEN];
        final byte[] buf = new byte[this.GINI_HED_LEN];
        final byte[] head = new byte[this.GINI_PDB_LEN];
        raf.read(b);
        final String pib = new String(b);
        pos = pib.indexOf("KNES");
        if (pos == -1) {
            pos = pib.indexOf("CHIZ");
        }
        if (pos != -1) {
            pos = pib.indexOf("\r\r\n");
            if (pos != -1) {
                pos += 3;
            }
        }
        else {
            pos = 0;
        }
        this.dataStart = pos + this.GINI_PDB_LEN;
        final byte[] b2 = { b[pos], b[pos + 1] };
        final Inflater inflater = new Inflater(false);
        int resultLength = 0;
        int inflatedLen = 0;
        int pos2 = 0;
        if (this.isZlibHed(b2) == 1) {
            this.Z_type = 1;
            inflater.setInput(b, pos, this.GINI_HED_LEN);
            try {
                resultLength = inflater.inflate(buf, 0, this.GINI_HED_LEN);
            }
            catch (DataFormatException ex) {
                Giniheader.log.warn("ERROR on inflation " + ex.getMessage());
                ex.printStackTrace();
                throw new IOException(ex.getMessage());
            }
            if (resultLength != this.GINI_HED_LEN) {
                System.out.println("Zlib inflated image header size error");
            }
            inflatedLen = this.GINI_HED_LEN - inflater.getRemaining();
            final String inf = new String(buf);
            pos2 = inf.indexOf("KNES");
            if (pos2 == -1) {
                pos2 = inf.indexOf("CHIZ");
            }
            if (pos2 != -1) {
                pos2 = inf.indexOf("\r\r\n");
                if (pos2 != -1) {
                    pos2 += 3;
                }
            }
            else {
                pos2 = 0;
            }
            System.arraycopy(buf, pos2, head, 0, this.GINI_PDB_LEN);
            this.dataStart = pos + inflatedLen;
        }
        else {
            System.arraycopy(b, pos, head, 0, this.GINI_PDB_LEN);
        }
        if (pos == 0 && pos2 == 0) {
            return null;
        }
        return head;
    }
    
    void read(final RandomAccessFile raf, final NetcdfFile ncfile, final PrintStream out) throws IOException {
        this.raf = raf;
        this.ncfile = ncfile;
        double lon1 = 0.0;
        double lon2 = 0.0;
        double lat1 = 0.0;
        double lat2 = 0.0;
        double latt = 0.0;
        final double lont = 0.0;
        double imageScale = 0.0;
        final byte[] head = this.readPIB(raf);
        final ByteBuffer bos = ByteBuffer.wrap(head);
        this.actualSize = raf.length();
        Attribute att = new Attribute("Conventions", "GRIB");
        this.ncfile.addAttribute(null, att);
        bos.position(0);
        final byte[] b2 = new byte[2];
        Byte nv = new Byte(bos.get());
        att = new Attribute("source_id", nv);
        this.ncfile.addAttribute(null, att);
        nv = new Byte(bos.get());
        final int ent_id = nv;
        att = new Attribute("entity_id", nv);
        this.ncfile.addAttribute(null, att);
        nv = new Byte(bos.get());
        final int sec_id = nv;
        att = new Attribute("sector_id", nv);
        this.ncfile.addAttribute(null, att);
        nv = new Byte(bos.get());
        final int phys_elem = nv;
        att = new Attribute("phys_elem", nv);
        this.ncfile.addAttribute(null, att);
        bos.position(bos.position() + 4);
        int gyear = bos.get();
        gyear += ((gyear < 50) ? 2000 : 1900);
        final int gmonth = bos.get();
        final int gday = bos.get();
        final int ghour = bos.get();
        final int gminute = bos.get();
        final int gsecond = bos.get();
        final DateFormat dformat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        dformat.setTimeZone(TimeZone.getTimeZone("GMT"));
        final Calendar cal = Calendar.getInstance();
        cal.set(gyear, gmonth - 1, gday, ghour, gminute, gsecond);
        cal.setTimeZone(TimeZone.getTimeZone("GMT"));
        final String dstring = dformat.format(cal.getTime());
        final Dimension dimT = new Dimension("time", 1, true, false, false);
        ncfile.addDimension(null, dimT);
        final String timeCoordName = "time";
        final Variable taxis = new Variable(ncfile, null, null, timeCoordName);
        taxis.setDataType(DataType.DOUBLE);
        taxis.setDimensions("time");
        taxis.addAttribute(new Attribute("long_name", "time since base date"));
        taxis.addAttribute(new Attribute("_CoordinateAxisType", AxisType.Time.toString()));
        final double[] tdata = { (double)cal.getTimeInMillis() };
        Array dataA = Array.factory(DataType.DOUBLE.getPrimitiveClassType(), new int[] { 1 }, tdata);
        taxis.setCachedData(dataA, false);
        final DateFormatter formatter = new DateFormatter();
        taxis.addAttribute(new Attribute("units", "msecs since " + formatter.toDateTimeStringISO(new Date(0L))));
        ncfile.addVariable(null, taxis);
        this.ncfile.addAttribute(null, new Attribute("time_coverage_start", dstring));
        this.ncfile.addAttribute(null, new Attribute("time_coverage_end", dstring));
        bos.get();
        nv = new Byte(bos.get());
        att = new Attribute("ProjIndex", nv);
        this.ncfile.addAttribute(null, att);
        final int proj = nv;
        if (proj == 1) {
            att = new Attribute("ProjName", "MERCATOR");
        }
        else if (proj == 3) {
            att = new Attribute("ProjName", "LAMBERT_CONFORNAL");
        }
        else if (proj == 5) {
            att = new Attribute("ProjName", "POLARSTEREOGRAPHIC");
        }
        this.ncfile.addAttribute(null, att);
        final byte[] b3 = new byte[3];
        bos.get(b2, 0, 2);
        final int nx = this.getInt(b2, 2);
        Integer ni = new Integer(nx);
        att = new Attribute("NX", ni);
        this.ncfile.addAttribute(null, att);
        bos.get(b2, 0, 2);
        final int ny = this.getInt(b2, 2);
        ni = new Integer(ny);
        att = new Attribute("NY", ni);
        this.ncfile.addAttribute(null, att);
        ProjectionImpl projection = null;
        double dxKm = 0.0;
        double dyKm = 0.0;
        switch (proj) {
            case 1: {
                bos.get(b3, 0, 3);
                int nn = this.getInt(b3, 3);
                lat1 = nn / 10000.0;
                Double nd = new Double(lat1);
                att = new Attribute("Latitude0", nd);
                this.ncfile.addAttribute(null, att);
                bos.get(b3, 0, 3);
                nn = this.getInt(b3, 3);
                int b4 = b3[0] & 0x80;
                nd = new Double(nn / 10000.0);
                lon1 = nd;
                att = new Attribute("Longitude0", nd);
                this.ncfile.addAttribute(null, att);
                bos.get();
                bos.get(b3, 0, 3);
                nn = this.getInt(b3, 3);
                nd = new Double(nn / 10000.0);
                lat2 = nd;
                att = new Attribute("LatitudeN", nd);
                this.ncfile.addAttribute(null, att);
                bos.get(b3, 0, 3);
                nn = this.getInt(b3, 3);
                b4 = (b3[0] & 0x80);
                nd = new Double(nn / 10000.0);
                lon2 = nd;
                att = new Attribute("LongitudeN", nd);
                this.ncfile.addAttribute(null, att);
                double lon_1 = lon1;
                double lon_2 = lon2;
                if (lon1 < 0.0) {
                    lon_1 += 360.0;
                }
                if (lon2 < 0.0) {
                    lon_2 += 360.0;
                }
                double lonv = lon_1 - (lon_1 - lon_2) / 2.0;
                if (lonv > 180.0) {
                    lonv -= 360.0;
                }
                if (lonv < -180.0) {
                    lonv += 360.0;
                }
                bos.getInt();
                bos.get();
                bos.get(b3, 0, 3);
                nn = this.getInt(b3, 3);
                nd = new Double(nn / 10000.0);
                att = new Attribute("LatitudeX", nd);
                final double latin = nd;
                this.ncfile.addAttribute(null, att);
                latt = 0.0;
                projection = new Mercator(lonv, latin);
                break;
            }
            case 3:
            case 5: {
                bos.get(b3, 0, 3);
                int nn = this.getInt(b3, 3);
                Double nd = new Double(nn / 10000.0);
                lat1 = nd;
                bos.get(b3, 0, 3);
                nn = this.getInt(b3, 3);
                nd = new Double(nn / 10000.0);
                lon1 = nd;
                bos.get();
                bos.get(b3, 0, 3);
                nn = this.getInt(b3, 3);
                nd = new Double(nn / 10000.0);
                final double lonProjectionOrigin;
                double lonv = lonProjectionOrigin = nd;
                att = new Attribute("Lov", nd);
                this.ncfile.addAttribute(null, att);
                bos.get(b3, 0, 3);
                nn = this.getInt(b3, 3);
                dxKm = nn / 10000.0;
                nd = new Double(nn / 10000.0);
                att = new Attribute("DxKm", nd);
                this.ncfile.addAttribute(null, att);
                bos.get(b3, 0, 3);
                nn = this.getInt(b3, 3);
                dyKm = nn / 10000.0;
                nd = new Double(nn / 10000.0);
                att = new Attribute("DyKm", nd);
                this.ncfile.addAttribute(null, att);
                if (proj == 5) {
                    latt = 60.0;
                    imageScale = (1.0 + Math.sin(this.DEG_TO_RAD * latt)) / 2.0;
                }
                lat2 = lat1 + dyKm * (ny - 1) / 111.26;
                if (lonv < 0.0) {
                    lonv += 360.0;
                }
                if (lon1 < 0.0) {
                    lon1 += 360.0;
                }
                lon2 = lon1 + dxKm * (nx - 1) / 111.26 * Math.cos(this.DEG_TO_RAD * lat1);
                double diff_lon = lonv - lon1;
                if (diff_lon > 180.0) {
                    diff_lon -= 360.0;
                }
                if (diff_lon < -180.0) {
                    diff_lon += 360.0;
                }
                lonv = ((lonv > 180.0) ? (-(360.0 - lonv)) : lonv);
                lon1 = ((lon1 > 180.0) ? (-(360.0 - lon1)) : lon1);
                lon2 = ((lon2 > 180.0) ? (-(360.0 - lon2)) : lon2);
                nv = new Byte(bos.get());
                int pole = nv;
                pole = ((pole > 127) ? -1 : 1);
                ni = new Integer(pole);
                att = new Attribute("ProjCenter", ni);
                this.ncfile.addAttribute(null, att);
                bos.get();
                bos.get(b3, 0, 3);
                nn = this.getInt(b3, 3);
                final double latin = nn / 10000.0;
                nd = new Double(nn / 10000.0);
                att = new Attribute("Latin", nd);
                this.ncfile.addAttribute(null, att);
                if (proj == 3) {
                    projection = new LambertConformal(latin, lonProjectionOrigin, latin, latin);
                    break;
                }
                projection = new Stereographic(90.0, lonv, imageScale);
                break;
            }
            default: {
                System.out.println("unimplemented projection");
                break;
            }
        }
        this.ncfile.addAttribute(null, new Attribute("title", this.gini_GetEntityID(ent_id)));
        this.ncfile.addAttribute(null, new Attribute("summary", this.getPhysElemSummary(phys_elem, ent_id)));
        this.ncfile.addAttribute(null, new Attribute("id", this.gini_GetSectorID(sec_id)));
        this.ncfile.addAttribute(null, new Attribute("keywords_vocabulary", this.gini_GetPhysElemID(phys_elem, ent_id)));
        this.ncfile.addAttribute(null, new Attribute("cdm_data_type", FeatureType.GRID.toString()));
        this.ncfile.addAttribute(null, new Attribute("standard_name_vocabulary", this.getPhysElemLongName(phys_elem, ent_id)));
        this.ncfile.addAttribute(null, new Attribute("creator_name", "UNIDATA"));
        this.ncfile.addAttribute(null, new Attribute("creator_url", "http://www.unidata.ucar.edu/"));
        this.ncfile.addAttribute(null, new Attribute("naming_authority", "UCAR/UOP"));
        this.ncfile.addAttribute(null, new Attribute("geospatial_lat_min", new Float(lat1)));
        this.ncfile.addAttribute(null, new Attribute("geospatial_lat_max", new Float(lat2)));
        this.ncfile.addAttribute(null, new Attribute("geospatial_lon_min", new Float(lon1)));
        this.ncfile.addAttribute(null, new Attribute("geospatial_lon_max", new Float(lon2)));
        bos.position(41);
        nv = new Byte(bos.get());
        att = new Attribute("imageResolution", nv);
        this.ncfile.addAttribute(null, att);
        nv = new Byte(bos.get());
        att = new Attribute("compressionFlag", nv);
        this.ncfile.addAttribute(null, att);
        if (this.convertunsignedByte2Short(nv) == 128) {
            this.Z_type = 2;
        }
        bos.position(46);
        nv = new Byte(bos.get());
        final int navcal = this.convertunsignedByte2Short(nv);
        int[] calcods = null;
        if (navcal == 128) {
            calcods = this.getCalibrationInfo(bos, phys_elem, ent_id);
        }
        final String vname = this.gini_GetPhysElemID(phys_elem, ent_id);
        final Variable var = new Variable(ncfile, ncfile.getRootGroup(), null, vname);
        var.addAttribute(new Attribute("long_name", this.getPhysElemLongName(phys_elem, ent_id)));
        var.addAttribute(new Attribute("units", this.getPhysElemUnits(phys_elem, ent_id)));
        final boolean isRecord = false;
        final ArrayList dims = new ArrayList();
        final Dimension dimX = new Dimension("x", nx, true, false, false);
        final Dimension dimY = new Dimension("y", ny, true, false, false);
        ncfile.addDimension(null, dimY);
        ncfile.addDimension(null, dimX);
        final int velems = dimX.getLength() * dimY.getLength();
        dims.add(dimT);
        dims.add(dimY);
        dims.add(dimX);
        var.setDimensions(dims);
        final int vsize = velems;
        final long begin = this.dataStart;
        if (this.debug) {
            Giniheader.log.warn(" name= " + vname + " vsize=" + vsize + " velems=" + velems + " begin= " + begin + " isRecord=" + isRecord + "\n");
        }
        if (navcal == 128) {
            var.setDataType(DataType.FLOAT);
            var.setSPobject(new Vinfo(vsize, begin, isRecord, nx, ny, calcods));
        }
        else {
            var.setDataType(DataType.BYTE);
            var.addAttribute(new Attribute("_Unsigned", "true"));
            var.addAttribute(new Attribute("_missing_value", new Short((short)255)));
            var.addAttribute(new Attribute("scale_factor", new Short((short)1)));
            var.addAttribute(new Attribute("add_offset", new Short((short)0)));
            var.setSPobject(new Vinfo(vsize, begin, isRecord, nx, ny));
        }
        final String coordinates = "x y time";
        var.addAttribute(new Attribute("_CoordinateAxes", coordinates));
        ncfile.addVariable(null, var);
        final ProjectionPointImpl start = (ProjectionPointImpl)projection.latLonToProj(new LatLonPointImpl(lat1, lon1));
        if (this.debug) {
            Giniheader.log.warn("start at proj coord " + start);
        }
        final double startx = start.getX();
        final double starty = start.getY();
        final Variable xaxis = new Variable(ncfile, null, null, "x");
        xaxis.setDataType(DataType.DOUBLE);
        xaxis.setDimensions("x");
        xaxis.addAttribute(new Attribute("long_name", "projection x coordinate"));
        xaxis.addAttribute(new Attribute("units", "km"));
        xaxis.addAttribute(new Attribute("_CoordinateAxisType", "GeoX"));
        double[] data = new double[nx];
        if (proj == 1) {
            double lon_3 = lon1;
            double lon_4 = lon2;
            if (lon1 < 0.0) {
                lon_3 += 360.0;
            }
            if (lon2 < 0.0) {
                lon_4 += 360.0;
            }
            final double dx = (lon_4 - lon_3) / (nx - 1);
            for (int i = 0; i < data.length; ++i) {
                final double ln = lon1 + i * dx;
                final ProjectionPointImpl pt = (ProjectionPointImpl)projection.latLonToProj(new LatLonPointImpl(lat1, ln));
                data[i] = pt.getX();
            }
        }
        else {
            for (int j = 0; j < data.length; ++j) {
                data[j] = startx + j * dxKm;
            }
        }
        dataA = Array.factory(DataType.DOUBLE.getPrimitiveClassType(), new int[] { nx }, data);
        xaxis.setCachedData(dataA, false);
        ncfile.addVariable(null, xaxis);
        final Variable yaxis = new Variable(ncfile, null, null, "y");
        yaxis.setDataType(DataType.DOUBLE);
        yaxis.setDimensions("y");
        yaxis.addAttribute(new Attribute("long_name", "projection y coordinate"));
        yaxis.addAttribute(new Attribute("units", "km"));
        yaxis.addAttribute(new Attribute("_CoordinateAxisType", "GeoY"));
        data = new double[ny];
        final double endy = starty + dyKm * (data.length - 1);
        if (proj == 1) {
            final double dy = (lat2 - lat1) / (ny - 1);
            for (int k = 0; k < data.length; ++k) {
                final double la = lat2 - k * dy;
                final ProjectionPointImpl pt2 = (ProjectionPointImpl)projection.latLonToProj(new LatLonPointImpl(la, lon1));
                data[k] = pt2.getY();
            }
        }
        else {
            for (int l = 0; l < data.length; ++l) {
                data[l] = endy - l * dyKm;
            }
        }
        dataA = Array.factory(DataType.DOUBLE.getPrimitiveClassType(), new int[] { ny }, data);
        yaxis.setCachedData(dataA, false);
        ncfile.addVariable(null, yaxis);
        final Variable ct = new Variable(ncfile, null, null, projection.getClassName());
        ct.setDataType(DataType.CHAR);
        ct.setDimensions("");
        final List params = projection.getProjectionParameters();
        for (int k = 0; k < params.size(); ++k) {
            final Parameter p = params.get(k);
            ct.addAttribute(new Attribute(p));
        }
        ct.addAttribute(new Attribute("_CoordinateTransformType", "Projection"));
        ct.addAttribute(new Attribute("_CoordinateAxes", "x y "));
        dataA = Array.factory(DataType.CHAR.getPrimitiveClassType(), new int[0]);
        dataA.setChar(dataA.getIndex(), ' ');
        ct.setCachedData(dataA, false);
        ncfile.addVariable(null, ct);
        ncfile.addAttribute(null, new Attribute("Conventions", "_Coordinates"));
        ncfile.finish();
    }
    
    int[] getCalibrationInfo(final ByteBuffer bos, final int phys_elem, final int ent_id) {
        bos.position(46);
        byte nv = new Byte(bos.get());
        final int navcal = this.convertunsignedByte2Short(nv);
        int[] calcods = null;
        if (navcal == 128) {
            int scale = 10000;
            int jscale = 100000000;
            final byte[] unsb = new byte[8];
            final byte[] b4 = new byte[4];
            bos.get(unsb);
            final String unitStr = new String(unsb).toUpperCase();
            bos.position(55);
            nv = new Byte(bos.get());
            final int calcod = this.convertunsignedByte2Short(nv);
            if (unitStr.contains("INCH")) {
                final String iname = new String("RAIN");
                final String iunit = new String("IN  ");
            }
            else if (unitStr.contains("dBz")) {
                final String iname = new String("ECHO");
                final String iunit = new String("dBz ");
            }
            else if (unitStr.contains("KFT")) {
                final String iname = new String("TOPS");
                final String iunit = new String("KFT ");
            }
            else if (unitStr.contains("KG/M")) {
                final String iname = new String("VIL ");
                final String iunit = new String("mm  ");
            }
            else {
                final String iname = new String("    ");
                final String iunit = new String("    ");
            }
            if (calcod > 0) {
                calcods = new int[5 * calcod + 1];
                calcods[0] = calcod;
                for (int i = 0; i < calcod; ++i) {
                    bos.position(56 + i * 16);
                    bos.get(b4);
                    final int minb = this.getInt(b4, 4) / 10000;
                    bos.get(b4);
                    final int maxb = this.getInt(b4, 4) / 10000;
                    bos.get(b4);
                    final int mind = this.getInt(b4, 4);
                    bos.get(b4);
                    int maxd;
                    int idscal;
                    for (maxd = this.getInt(b4, 4), idscal = 1; mind % idscal == 0 && maxd % idscal == 0; idscal *= 10) {}
                    idscal /= 10;
                    if (idscal < jscale) {
                        jscale = idscal;
                    }
                    calcods[1 + i * 5] = mind;
                    calcods[2 + i * 5] = maxd;
                    calcods[3 + i * 5] = minb;
                    calcods[4 + i * 5] = maxb;
                    calcods[5 + i * 5] = 0;
                }
                if (jscale > scale) {
                    jscale = scale;
                }
                scale /= jscale;
                if (this.gini_GetPhysElemID(phys_elem, ent_id).contains("Precipitation") && scale < 100) {
                    jscale /= 100 / scale;
                    scale = 100;
                }
                for (int i = 0; i < calcod; ++i) {
                    final int[] array = calcods;
                    final int n = 1 + i * 5;
                    array[n] /= jscale;
                    final int[] array2 = calcods;
                    final int n2 = 2 + i * 5;
                    array2[n2] /= jscale;
                    calcods[5 + i * 5] = scale;
                }
            }
        }
        return calcods;
    }
    
    int gini_GetCompressType() {
        return this.Z_type;
    }
    
    String gini_GetSectorID(final int ent_id) {
        String name = null;
        switch (ent_id) {
            case 0: {
                name = "Northern Hemisphere Composite";
                break;
            }
            case 1: {
                name = "East CONUS";
                break;
            }
            case 2: {
                name = "West CONUS";
                break;
            }
            case 3: {
                name = "Alaska Regional";
                break;
            }
            case 4: {
                name = "Alaska National";
                break;
            }
            case 5: {
                name = "Hawaii Regional";
                break;
            }
            case 6: {
                name = "Hawaii National";
                break;
            }
            case 7: {
                name = "Puerto Rico Regional";
                break;
            }
            case 8: {
                name = "Puerto Rico National";
                break;
            }
            case 9: {
                name = "Supernational";
                break;
            }
            case 10: {
                name = "NH Composite - Meteosat/GOES E/ GOES W/GMS";
                break;
            }
            default: {
                name = "Unknown-ID";
                break;
            }
        }
        return name;
    }
    
    String gini_GetEntityID(final int ent_id) {
        String name = null;
        switch (ent_id) {
            case 99: {
                name = "RADAR-MOSIAC Composite Image";
                break;
            }
            case 6: {
                name = "Composite";
                break;
            }
            case 7: {
                name = "DMSP satellite Image";
                break;
            }
            case 8: {
                name = "GMS satellite Image";
                break;
            }
            case 9: {
                name = "METEOSAT satellite Image";
                break;
            }
            case 10: {
                name = "GOES-7 satellite Image";
                break;
            }
            case 11: {
                name = "GOES-8 satellite Image";
                break;
            }
            case 12: {
                name = "GOES-9 satellite Image";
                break;
            }
            case 13: {
                name = "GOES-10 satellite Image";
                break;
            }
            case 14: {
                name = "GOES-11 satellite Image";
                break;
            }
            case 15: {
                name = "GOES-12 satellite Image";
                break;
            }
            case 16: {
                name = "GOES-13 satellite Image";
                break;
            }
            default: {
                name = "Unknown";
                break;
            }
        }
        return name;
    }
    
    String gini_GetPhysElemID(final int phys_elem, final int ent_id) {
        String name = null;
        switch (phys_elem) {
            case 1: {
                name = "VIS";
                break;
            }
            case 3: {
                name = "IR_WV";
                break;
            }
            case 2:
            case 4:
            case 5:
            case 6:
            case 7: {
                name = "IR";
                break;
            }
            case 13: {
                name = "LI";
                break;
            }
            case 14: {
                name = "PW";
                break;
            }
            case 15: {
                name = "SFC_T";
                break;
            }
            case 16: {
                name = "LI";
                break;
            }
            case 17: {
                name = "PW";
                break;
            }
            case 18: {
                name = "SFC_T";
                break;
            }
            case 19: {
                name = "CAPE";
                break;
            }
            case 20: {
                name = "T";
                break;
            }
            case 21: {
                name = "WINDEX";
                break;
            }
            case 22: {
                name = "DMPI";
                break;
            }
            case 23: {
                name = "MDPI";
                break;
            }
            case 25: {
                if (ent_id == 99) {
                    name = "Reflectivity";
                    break;
                }
                name = "Volcano_imagery";
                break;
            }
            case 27: {
                if (ent_id == 99) {
                    name = "Reflectivity";
                    break;
                }
                name = "CTP";
                break;
            }
            case 28: {
                if (ent_id == 99) {
                    name = "Reflectivity";
                    break;
                }
                name = "Cloud_Amount";
                break;
            }
            case 26: {
                name = "EchoTops";
                break;
            }
            case 29: {
                name = "VIL";
                break;
            }
            case 30:
            case 31: {
                name = "Precipitation";
                break;
            }
            case 40:
            case 41:
            case 42:
            case 43:
            case 44:
            case 45:
            case 46:
            case 47:
            case 48:
            case 49:
            case 50:
            case 51:
            case 52:
            case 53:
            case 54:
            case 55:
            case 56:
            case 57:
            case 58: {
                name = "sounder_imagery";
                break;
            }
            case 59: {
                name = "VIS_sounder";
                break;
            }
            default: {
                name = "Unknown";
                break;
            }
        }
        return name;
    }
    
    String getPhysElemUnits(final int phys_elem, final int ent_id) {
        switch (phys_elem) {
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 13:
            case 14:
            case 15:
            case 16:
            case 17:
            case 18:
            case 19:
            case 20:
            case 21:
            case 22:
            case 23:
            case 29:
            case 43:
            case 48:
            case 50:
            case 51:
            case 52:
            case 55:
            case 57:
            case 59: {
                return "N/A";
            }
            case 26: {
                return "K FT";
            }
            case 25: {
                if (ent_id == 99) {
                    return "dBz";
                }
                return "N/A";
            }
            case 27: {
                if (ent_id == 99) {
                    return "dBz";
                }
                return "N/A";
            }
            case 28: {
                if (ent_id == 99) {
                    return "dBz";
                }
                return "N/A";
            }
            case 30: {
                return "IN";
            }
            case 31: {
                return "IN";
            }
            default: {
                return "Unknown";
            }
        }
    }
    
    String getPhysElemLongName(final int phys_elem, final int ent_id) {
        switch (phys_elem) {
            case 1: {
                return "Imager Visible";
            }
            case 2: {
                return "Imager 3.9 micron IR";
            }
            case 3: {
                return "Imager 6.7/6.5 micron IR (WV)";
            }
            case 4: {
                return "Imager 11 micron IR";
            }
            case 5: {
                return "Imager 12 micron IR";
            }
            case 6: {
                return "Imager 13 micron IR";
            }
            case 7: {
                return "Imager 1.3 micron IR";
            }
            case 13: {
                return "Lifted Index LI";
            }
            case 14: {
                return "Precipitable Water PW";
            }
            case 15: {
                return "Surface Skin Temperature";
            }
            case 16: {
                return "Lifted Index LI";
            }
            case 17: {
                return "Precipitable Water PW";
            }
            case 18: {
                return "Surface Skin Temperature";
            }
            case 19: {
                return "Convective Available Potential Energy";
            }
            case 20: {
                return "land-sea Temperature";
            }
            case 21: {
                return "Wind Index";
            }
            case 22: {
                return "Dry Microburst Potential Index";
            }
            case 23: {
                return "Microburst Potential Index";
            }
            case 25: {
                if (ent_id == 99) {
                    return "2 km National 248 nm Base Composite Reflectivity";
                }
                return "Volcano_imagery";
            }
            case 26: {
                return "4 km National Echo Tops";
            }
            case 27: {
                if (ent_id == 99) {
                    return "1 km National Base Reflectivity Composite (Unidata)";
                }
                return "Cloud Top Pressure or Height";
            }
            case 28: {
                if (ent_id == 99) {
                    return "1 km National Composite Reflectivity (Unidata)";
                }
                return "Cloud Amount";
            }
            case 29: {
                return "4 km National Vertically Integrated Liquid Water";
            }
            case 30: {
                return "2 km National 1-hour Precipitation (Unidata)";
            }
            case 31: {
                return "4 km National Storm Total Precipitation (Unidata)";
            }
            case 43: {
                return "14.06 micron sounder image";
            }
            case 48: {
                return "11.03 micron sounder image";
            }
            case 50: {
                return "7.43 micron sounder image";
            }
            case 51: {
                return "7.02 micron sounder image";
            }
            case 52: {
                return "6.51 micron sounder image";
            }
            case 55: {
                return "4.45 micron sounder image";
            }
            case 57: {
                return "3.98 micron sounder image";
            }
            case 59: {
                return "VIS sounder image ";
            }
            default: {
                return "unknown physical element " + phys_elem;
            }
        }
    }
    
    String getPhysElemSummary(final int phys_elem, final int ent_id) {
        switch (phys_elem) {
            case 1: {
                return "Satellite Product Imager Visible";
            }
            case 2: {
                return "Satellite Product Imager 3.9 micron IR";
            }
            case 3: {
                return "Satellite Product Imager 6.7/6.5 micron IR (WV)";
            }
            case 4: {
                return "Satellite Product Imager 11 micron IR";
            }
            case 5: {
                return "Satellite Product Imager 12 micron IR";
            }
            case 6: {
                return "Satellite Product Imager 13 micron IR";
            }
            case 7: {
                return "Satellite Product Imager 1.3 micron IR";
            }
            case 13: {
                return "Imager Based Derived Lifted Index LI";
            }
            case 14: {
                return "Imager Based Derived Precipitable Water PW";
            }
            case 15: {
                return "Imager Based Derived Surface Skin Temperature";
            }
            case 16: {
                return "Sounder Based Derived Lifted Index LI";
            }
            case 17: {
                return "Sounder Based Derived Precipitable Water PW";
            }
            case 18: {
                return "Sounder Based Derived Surface Skin Temperature";
            }
            case 19: {
                return "Derived Convective Available Potential Energy CAPE";
            }
            case 20: {
                return "Derived land-sea Temperature";
            }
            case 21: {
                return "Derived Wind Index WINDEX";
            }
            case 22: {
                return "Derived Dry Microburst Potential Index DMPI";
            }
            case 23: {
                return "Derived Microburst Day Potential Index MDPI";
            }
            case 43: {
                return "Satellite Product 14.06 micron sounder image";
            }
            case 48: {
                return "Satellite Product 11.03 micron sounder image";
            }
            case 50: {
                return "Satellite Product 7.43 micron sounder image";
            }
            case 51: {
                return "Satellite Product 7.02 micron sounder image";
            }
            case 52: {
                return "Satellite Product 6.51 micron sounder image";
            }
            case 55: {
                return "Satellite Product 4.45 micron sounder image";
            }
            case 57: {
                return "Satellite Product 3.98 micron sounder image";
            }
            case 59: {
                return "Satellite Product VIS sounder visible image ";
            }
            case 25: {
                if (ent_id == 99) {
                    return "Nexrad Level 3 National 248 nm Base Composite Reflectivity at Resolution 2 km";
                }
                return "Satellite Derived Volcano_imagery";
            }
            case 26: {
                return "Nexrad Level 3 National Echo Tops at Resolution 4 km";
            }
            case 29: {
                return "Nexrad Level 3 National Vertically Integrated Liquid Water at Resolution 4 km";
            }
            case 28: {
                if (ent_id == 99) {
                    return "Nexrad Level 3 National 248 nm Base Composite Reflectivity at Resolution 2 km";
                }
                return "Gridded Cloud Amount";
            }
            case 27: {
                if (ent_id == 99) {
                    return "Nexrad Level 3 Base Reflectivity National Composition at Resolution 1 km";
                }
                return "Gridded Cloud Top Pressure or Height";
            }
            case 30: {
                return "Nexrad Level 3 1 hour precipitation National Composition at Resolution 2 km";
            }
            case 31: {
                return "Nexrad Level 3 total precipitation National Composition at Resolution 4 km";
            }
            default: {
                return "unknown";
            }
        }
    }
    
    int getInt(final byte[] b, final int num) {
        int base = 1;
        int word = 0;
        final int[] bv = new int[num];
        for (int i = 0; i < num; ++i) {
            bv[i] = this.convertunsignedByte2Short(b[i]);
        }
        if (bv[0] > 127) {
            final int[] array = bv;
            final int n = 0;
            array[n] -= 128;
            base = -1;
        }
        for (int i = num - 1; i >= 0; --i) {
            word += base * bv[i];
            base *= 256;
        }
        return word;
    }
    
    public short convertunsignedByte2Short(final byte b) {
        return (short)((b < 0) ? (b + 256) : ((short)b));
    }
    
    protected Object convert(final byte[] barray, final DataType dataType, final int byteOrder) {
        if (dataType == DataType.BYTE) {
            return new Byte(barray[0]);
        }
        if (dataType == DataType.CHAR) {
            return new Character((char)barray[0]);
        }
        final ByteBuffer bbuff = ByteBuffer.wrap(barray);
        if (byteOrder >= 0) {
            bbuff.order((byteOrder == 1) ? ByteOrder.LITTLE_ENDIAN : ByteOrder.BIG_ENDIAN);
        }
        if (dataType == DataType.SHORT) {
            final ShortBuffer tbuff = bbuff.asShortBuffer();
            return new Short(tbuff.get());
        }
        if (dataType == DataType.INT) {
            final IntBuffer tbuff2 = bbuff.asIntBuffer();
            return new Integer(tbuff2.get());
        }
        if (dataType == DataType.LONG) {
            final LongBuffer tbuff3 = bbuff.asLongBuffer();
            return new Long(tbuff3.get());
        }
        if (dataType == DataType.FLOAT) {
            final FloatBuffer tbuff4 = bbuff.asFloatBuffer();
            return new Float(tbuff4.get());
        }
        if (dataType == DataType.DOUBLE) {
            final DoubleBuffer tbuff5 = bbuff.asDoubleBuffer();
            return new Double(tbuff5.get());
        }
        throw new IllegalStateException();
    }
    
    int isZlibHed(final byte[] buf) {
        final short b0 = this.convertunsignedByte2Short(buf[0]);
        final short b2 = this.convertunsignedByte2Short(buf[1]);
        if ((b0 & 0xF) == this.Z_DEFLATED && (b0 >> 4) + 8 <= this.DEF_WBITS && ((b0 << 8) + b2) % 31 == 0) {
            return 1;
        }
        return 0;
    }
    
    static {
        MAGIC = new byte[] { 67, 68, 70, 1 };
        Giniheader.log = LoggerFactory.getLogger(Giniheader.class);
    }
    
    class Vinfo
    {
        int vsize;
        long begin;
        boolean isRecord;
        int nx;
        int ny;
        int[] levels;
        
        Vinfo(final int vsize, final long begin, final boolean isRecord, final int x, final int y) {
            this.vsize = vsize;
            this.begin = begin;
            this.isRecord = isRecord;
            this.nx = x;
            this.ny = y;
        }
        
        Vinfo(final int vsize, final long begin, final boolean isRecord, final int x, final int y, final int[] levels) {
            this.vsize = vsize;
            this.begin = begin;
            this.isRecord = isRecord;
            this.nx = x;
            this.ny = y;
            this.levels = levels;
        }
    }
}
