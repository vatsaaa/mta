// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.iosp.gini;

import java.util.zip.DataFormatException;
import java.util.zip.Inflater;
import java.awt.image.DataBuffer;
import java.awt.image.Raster;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.io.InputStream;
import javax.imageio.ImageIO;
import java.io.ByteArrayInputStream;
import ucar.ma2.Index;
import ucar.ma2.ArrayByte;
import ucar.ma2.DataType;
import ucar.ma2.Section;
import java.io.PrintStream;
import ucar.nc2.util.CancelTask;
import ucar.ma2.InvalidRangeException;
import java.io.IOException;
import ucar.ma2.Array;
import java.util.List;
import ucar.nc2.Variable;
import java.util.HashMap;
import ucar.unidata.io.RandomAccessFile;
import ucar.nc2.NetcdfFile;
import ucar.nc2.iosp.AbstractIOServiceProvider;

public class Giniiosp extends AbstractIOServiceProvider
{
    protected boolean readonly;
    private NetcdfFile ncfile;
    private RandomAccessFile myRaf;
    protected Giniheader headerParser;
    static final int Z_DEFLATED = 8;
    static final int DEF_WBITS = 15;
    protected int fileUsed;
    protected int recStart;
    protected boolean debug;
    protected boolean debugSize;
    protected boolean debugSPIO;
    protected boolean showHeaderBytes;
    protected boolean fill;
    protected HashMap dimHash;
    
    public Giniiosp() {
        this.fileUsed = 0;
        this.recStart = 0;
        this.debug = false;
        this.debugSize = false;
        this.debugSPIO = false;
        this.showHeaderBytes = false;
        this.dimHash = new HashMap(50);
    }
    
    public Array readNestedData(final Variable v2, final List section) throws IOException, InvalidRangeException {
        throw new UnsupportedOperationException("Gini IOSP does not support nested variables");
    }
    
    public boolean isValidFile(final RandomAccessFile raf) {
        final Giniheader localHeader = new Giniheader();
        return localHeader.isValidFile(raf);
    }
    
    @Override
    public void open(final RandomAccessFile raf, final NetcdfFile file, final CancelTask cancelTask) throws IOException {
        this.ncfile = file;
        this.myRaf = raf;
        (this.headerParser = new Giniheader()).read(this.myRaf, this.ncfile, null);
        this.ncfile.finish();
    }
    
    public Array readData(final Variable v2, final Section section) throws IOException, InvalidRangeException {
        final int[] origin = section.getOrigin();
        final int[] shape = section.getShape();
        final int[] stride = section.getStride();
        final Giniheader.Vinfo vinfo = (Giniheader.Vinfo)v2.getSPobject();
        final int[] levels = vinfo.levels;
        if (this.headerParser.gini_GetCompressType() == 0) {
            return this.readData(v2, vinfo.begin, origin, shape, stride, levels);
        }
        if (this.headerParser.gini_GetCompressType() == 2) {
            return this.readCompressedData(v2, vinfo.begin, origin, shape, stride, levels);
        }
        if (this.headerParser.gini_GetCompressType() == 1) {
            return this.readCompressedZlib(v2, vinfo.begin, vinfo.nx, vinfo.ny, origin, shape, stride, levels);
        }
        return null;
    }
    
    private Array readData(final Variable v2, final long dataPos, final int[] origin, final int[] shape, final int[] stride, final int[] levels) throws IOException, InvalidRangeException {
        final long length = this.myRaf.length();
        this.myRaf.seek(dataPos);
        final int data_size = (int)(length - dataPos);
        final byte[] data = new byte[data_size];
        this.myRaf.read(data);
        if (levels == null) {
            final Array array = Array.factory(DataType.BYTE.getPrimitiveClassType(), v2.getShape(), data);
            return array.sectionNoReduce(origin, shape, stride);
        }
        final int level = levels[0];
        final float[] a = new float[level];
        final float[] b = new float[level];
        final float[] fdata = new float[data_size];
        int scale = 1;
        for (int i = 0; i < level; ++i) {
            final int numer = levels[1 + 5 * i] - levels[2 + 5 * i];
            final int denom = levels[3 + 5 * i] - levels[4 + 5 * i];
            a[i] = numer * 1.0f / (1.0f * denom);
            b[i] = levels[1 + 5 * i] - a[i] * levels[3 + 5 * i];
        }
        for (int j = 0; j < data_size; ++j) {
            final int ival = this.convertUnsignedByte2Short(data[j]);
            int k = -1;
            for (int l = 0; l < level; ++l) {
                if (levels[3 + l * 5] <= ival && ival <= levels[4 + l * 5]) {
                    k = l;
                    scale = levels[5 + l * 5];
                }
            }
            if (k >= 0) {
                fdata[j] = (a[k] * ival + b[k]) / scale;
            }
            else {
                fdata[j] = 0.0f;
            }
        }
        final Array array2 = Array.factory(DataType.FLOAT.getPrimitiveClassType(), v2.getShape(), fdata);
        return array2.sectionNoReduce(origin, shape, stride);
    }
    
    public Array readDataOld(final Variable v2, final long dataPos, int[] origin, int[] shape, final int[] stride) throws IOException, InvalidRangeException {
        if (origin == null) {
            origin = new int[v2.getRank()];
        }
        if (shape == null) {
            shape = v2.getShape();
        }
        final Giniheader.Vinfo vinfo = (Giniheader.Vinfo)v2.getSPobject();
        final DataType dataType = v2.getDataType();
        final int nx = vinfo.nx;
        final int ny = vinfo.ny;
        int start_l = origin[0];
        int stride_l = stride[0];
        int stop_l = origin[0] + shape[0] - 1;
        int start_p = origin[1];
        int stride_p = stride[1];
        int stop_p = origin[1] + shape[1] - 1;
        if (start_l + stop_l + stride_l == 0) {
            start_l = 0;
            stride_l = 1;
            stop_l = ny - 1;
        }
        if (start_p + stop_p + stride_p == 0) {
            start_p = 0;
            stride_p = 1;
            stop_p = nx - 1;
        }
        final int Len = shape[1];
        final DataType ConvertFrom = DataType.BYTE;
        final ArrayByte adata = new ArrayByte(new int[] { shape[0], shape[1] });
        final Index indx = adata.getIndex();
        final long doff = dataPos + start_p;
        if (ConvertFrom == DataType.BYTE) {
            for (int iline = start_l; iline <= stop_l; iline += stride_l) {
                final byte[] buf = this.getGiniLine(nx, ny, doff, iline, Len, stride_p);
                for (int i = 0; i < Len; ++i) {
                    adata.setByte(indx.set(iline - start_l, i), buf[i]);
                }
            }
        }
        return adata;
    }
    
    public Array readCompressedData(final Variable v2, final long dataPos, final int[] origin, final int[] shape, final int[] stride, final int[] levels) throws IOException, InvalidRangeException {
        final long length = this.myRaf.length();
        this.myRaf.seek(dataPos);
        int data_size = (int)(length - dataPos);
        final byte[] data = new byte[data_size];
        this.myRaf.read(data);
        final ByteArrayInputStream ios = new ByteArrayInputStream(data);
        final BufferedImage image = ImageIO.read(ios);
        final Raster raster = image.getData();
        final DataBuffer db = raster.getDataBuffer();
        if (!(db instanceof DataBufferByte)) {
            return null;
        }
        final DataBufferByte dbb = (DataBufferByte)db;
        final int t = dbb.getNumBanks();
        final byte[] udata = dbb.getData();
        if (levels == null) {
            final Array array = Array.factory(DataType.BYTE.getPrimitiveClassType(), v2.getShape(), udata);
            v2.setCachedData(array, false);
            return array.sectionNoReduce(origin, shape, stride);
        }
        data_size = udata.length;
        final int level = levels[0];
        final float[] a = new float[level];
        final float[] b = new float[level];
        final float[] fdata = new float[data_size];
        int scale = 1;
        for (int i = 0; i < level; ++i) {
            final int numer = levels[1 + 5 * i] - levels[2 + 5 * i];
            final int denom = levels[3 + 5 * i] - levels[4 + 5 * i];
            a[i] = numer * 1.0f / (1.0f * denom);
            b[i] = levels[1 + 5 * i] - a[i] * levels[3 + 5 * i];
        }
        for (int j = 0; j < data_size; ++j) {
            final int ival = this.convertUnsignedByte2Short(udata[j]);
            int k = -1;
            for (int l = 0; l < level; ++l) {
                if (levels[3 + l * 5] <= ival && ival <= levels[4 + l * 5]) {
                    k = l;
                    scale = levels[5 + l * 5];
                }
            }
            if (k >= 0) {
                fdata[j] = (a[k] * ival + b[k]) / scale;
            }
            else {
                fdata[j] = 0.0f;
            }
        }
        final Array array2 = Array.factory(DataType.FLOAT.getPrimitiveClassType(), v2.getShape(), fdata);
        return array2.sectionNoReduce(origin, shape, stride);
    }
    
    public Array readCompressedZlib(final Variable v2, final long dataPos, final int nx, final int ny, final int[] origin, final int[] shape, final int[] stride, final int[] levels) throws IOException, InvalidRangeException {
        final long length = this.myRaf.length();
        this.myRaf.seek(dataPos);
        int data_size = (int)(length - dataPos);
        final byte[] data = new byte[data_size];
        this.myRaf.read(data);
        int resultLength = 0;
        int result = 0;
        final byte[] inflateData = new byte[nx * ny];
        byte[] uncomp = new byte[nx * (ny + 1) + 4000];
        final Inflater inflater = new Inflater(false);
        inflater.setInput(data, 0, data_size);
        int offset = 0;
        final int limit = nx * ny + nx;
        while (inflater.getRemaining() > 0) {
            try {
                resultLength = inflater.inflate(uncomp, offset, 4000);
            }
            catch (DataFormatException ex) {
                System.out.println("ERROR on inflation " + ex.getMessage());
                ex.printStackTrace();
                throw new IOException(ex.getMessage());
            }
            offset += resultLength;
            result += resultLength;
            if (result > limit) {
                final byte[] tmp = new byte[result];
                System.arraycopy(uncomp, 0, tmp, 0, result);
                final int uncompLen = result + 4000;
                uncomp = new byte[uncompLen];
                System.arraycopy(tmp, 0, uncomp, 0, result);
            }
            if (resultLength == 0) {
                final int tt = inflater.getRemaining();
                final byte[] b2 = new byte[2];
                System.arraycopy(data, data_size - tt, b2, 0, 2);
                if (this.isZlibHed(b2) == 0) {
                    System.arraycopy(data, data_size - tt, uncomp, result, tt);
                    result += tt;
                    break;
                }
                inflater.reset();
                inflater.setInput(data, data_size - tt, tt);
            }
        }
        inflater.end();
        System.arraycopy(uncomp, 0, inflateData, 0, nx * ny);
        if (data == null) {
            return null;
        }
        if (levels == null) {
            final Array array = Array.factory(DataType.BYTE.getPrimitiveClassType(), v2.getShape(), uncomp);
            if (array.getSize() < 4000L) {
                v2.setCachedData(array, false);
            }
            return array.sectionNoReduce(origin, shape, stride);
        }
        data_size = uncomp.length;
        final int level = levels[0];
        final float[] a = new float[level];
        final float[] b3 = new float[level];
        final float[] fdata = new float[data_size];
        int scale = 1;
        for (int i = 0; i < level; ++i) {
            final int numer = levels[1 + 5 * i] - levels[2 + 5 * i];
            final int denom = levels[3 + 5 * i] - levels[4 + 5 * i];
            a[i] = numer * 1.0f / (1.0f * denom);
            b3[i] = levels[1 + 5 * i] - a[i] * levels[3 + 5 * i];
        }
        for (int j = 0; j < data_size; ++j) {
            final int ival = this.convertUnsignedByte2Short(uncomp[j]);
            int k = -1;
            for (int l = 0; l < level; ++l) {
                if (levels[3 + l * 5] <= ival && ival <= levels[4 + l * 5]) {
                    k = l;
                    scale = levels[5 + l * 5];
                }
            }
            if (k >= 0) {
                fdata[j] = (a[k] * ival + b3[k]) / scale;
            }
            else {
                fdata[j] = 0.0f;
            }
        }
        final Array array2 = Array.factory(DataType.FLOAT.getPrimitiveClassType(), v2.getShape(), fdata);
        return array2.sectionNoReduce(origin, shape, stride);
    }
    
    private byte[] getGiniLine(final int nx, final int ny, final long doff, final int lineNumber, final int len, final int stride) throws IOException {
        final byte[] data = new byte[len];
        this.myRaf.seek(doff);
        if (lineNumber >= ny) {
            throw new IOException("Try to access the file at line number= " + lineNumber + " larger then last line number = " + ny);
        }
        int offset = lineNumber * nx + (int)doff;
        for (int i = 0; i < len; ++i) {
            this.myRaf.seek(offset);
            data[i] = this.myRaf.readByte();
            offset += stride;
        }
        return data;
    }
    
    int issZlibed(final byte[] buf) {
        if ((buf[0] & 0xF) == 0x8 && (buf[0] >> 4) + 8 <= 15 && ((buf[0] << 8) + buf[1]) % 31 == 0) {
            return 1;
        }
        return 0;
    }
    
    public void flush() throws IOException {
        this.myRaf.flush();
    }
    
    @Override
    public void close() throws IOException {
        this.myRaf.close();
    }
    
    private final short convertUnsignedByte2Short(final byte b) {
        return (short)((b < 0) ? (b + 256) : ((short)b));
    }
    
    private int isZlibHed(final byte[] buf) {
        final short b0 = this.convertUnsignedByte2Short(buf[0]);
        final short b2 = this.convertUnsignedByte2Short(buf[1]);
        if ((b0 & 0xF) == 0x8 && (b0 >> 4) + 8 <= 15 && ((b0 << 8) + b2) % 31 == 0) {
            return 1;
        }
        return 0;
    }
    
    public String getFileTypeId() {
        return "GINI";
    }
    
    public String getFileTypeDescription() {
        return "GOES Ingest and NOAAPORT Interface";
    }
    
    public static void main(final String[] args) throws Exception, IOException, InstantiationException, IllegalAccessException {
        final String fileIn = "c:/data/image/gini/n0r_20041013_1852";
        NetcdfFile.registerIOProvider(Giniiosp.class);
        final NetcdfFile ncf = NetcdfFile.open(fileIn);
        final List alist = ncf.getGlobalAttributes();
        final Variable v = ncf.findVariable("BaseReflectivity");
        final int[] origin = { 0, 0 };
        final int[] shape = { 3000, 4736 };
        final ArrayByte data = (ArrayByte)v.read(origin, shape);
        ncf.close();
    }
}
