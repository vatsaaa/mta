// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.iosp.sigmet;

import java.util.Arrays;
import java.util.Iterator;
import java.util.Comparator;
import java.util.Collections;
import java.util.Collection;
import java.util.HashMap;
import java.io.IOException;
import java.util.Map;
import ucar.nc2.Variable;
import java.util.ArrayList;
import ucar.nc2.NetcdfFile;
import ucar.unidata.io.RandomAccessFile;
import java.util.List;

public class SigmetVolumeScan
{
    String[] data_name;
    private List<List<Ray>> differentialReflectivityGroups;
    private List<List<Ray>> reflectivityGroups;
    private List<List<Ray>> totalPowerGroups;
    private List<List<Ray>> velocityGroups;
    private List<List<Ray>> widthGroups;
    private List<List<Ray>> timeGroups;
    private int[] num_gates;
    public int[] base_time;
    public short[] year;
    public short[] month;
    public short[] day;
    public Ray firstRay;
    public Ray lastRay;
    public RandomAccessFile raf;
    public boolean hasReflectivity;
    public boolean hasVelocity;
    public boolean hasWidth;
    public boolean hasTotalPower;
    public boolean hasDifferentialReflectivity;
    public boolean hasTime;
    private int max_radials;
    private int min_radials;
    private boolean debugRadials;
    
    SigmetVolumeScan(final RandomAccessFile raf, final NetcdfFile ncfile, final ArrayList<Variable> varList) throws IOException {
        this.data_name = new String[] { " ", "TotalPower", "Reflectivity", "Velocity", "Width", "DifferentialReflectivity" };
        this.firstRay = null;
        this.lastRay = null;
        this.hasReflectivity = false;
        this.hasVelocity = false;
        this.hasWidth = false;
        this.hasTotalPower = false;
        this.hasDifferentialReflectivity = false;
        this.hasTime = false;
        this.max_radials = 0;
        this.min_radials = Integer.MAX_VALUE;
        this.debugRadials = false;
        final int REC_SIZE = 6144;
        int len = 12288;
        short nrec = 0;
        short nsweep = 1;
        short nray = 0;
        short byteoff = 0;
        int nwords = 0;
        int end_words = 0;
        int data_read = 0;
        int num_zero = 0;
        int rays_count = 0;
        int nb = 0;
        int pos = 0;
        int pos_ray_hdr = 0;
        final int t = 0;
        short a0 = 0;
        short a2 = 0;
        short dty = 1;
        short beg_az = 0;
        short beg_elev = 0;
        short end_az = 0;
        short end_elev = 0;
        short num_bins = 0;
        short time_start_sw = 0;
        float az = 0.0f;
        float elev = 0.0f;
        final float d = 0.0f;
        float step = 0.0f;
        boolean beg_rec = true;
        boolean end_rec = true;
        boolean read_ray_hdr = true;
        final boolean begin = true;
        int cur_len = len;
        int beg = 1;
        int kk = 0;
        int col = 0;
        int nu = 0;
        final int bt0 = 0;
        final int bt2 = 0;
        final int start_sweep = 1;
        int end_sweep = 1;
        final int start_ray = 1;
        int end_ray = 1;
        (this.raf = raf).order(1);
        final int fileLength = (int)raf.length();
        final Map<String, Number> recHdr = SigmetIOServiceProvider.readRecordsHdr(raf);
        final int nparams = recHdr.get("nparams");
        final short number_sweeps = recHdr.get("number_sweeps");
        final short num_rays = recHdr.get("num_rays");
        final int range_1st = recHdr.get("range_first");
        final float range_first = range_1st * 0.01f;
        final int stepp = recHdr.get("range_last");
        final float range_last = stepp * 0.01f;
        final short bins = recHdr.get("bins");
        final short[] num_sweep = new short[nparams];
        final short[] num_rays_swp = new short[nparams];
        final short[] indx_1ray = new short[nparams];
        final short[] num_rays_act = new short[nparams];
        final short[] angl_swp = new short[nparams];
        final short[] bin_len = new short[nparams];
        final short[] data_type = new short[nparams];
        this.num_gates = new int[number_sweeps];
        end_sweep = number_sweeps;
        end_ray = num_rays;
        this.base_time = new int[nparams * number_sweeps];
        this.year = new short[nparams * number_sweeps];
        this.month = new short[nparams * number_sweeps];
        this.day = new short[nparams * number_sweeps];
        final List<Ray> totalPower = new ArrayList<Ray>();
        final List<Ray> velocity = new ArrayList<Ray>();
        final List<Ray> reflectivity = new ArrayList<Ray>();
        final List<Ray> width = new ArrayList<Ray>();
        final List<Ray> diffReflectivity = new ArrayList<Ray>();
        final List<Ray> time = new ArrayList<Ray>();
        final int irays = num_rays;
        Ray ray = null;
        int two = 0;
        final float[] val = new float[bins];
        while (len < fileLength) {
            int rayoffset = 0;
            int rayoffset2 = 0;
            int datalen = 0;
            cur_len = len;
            if (nsweep == number_sweeps & rays_count == beg) {
                return;
            }
            if (beg_rec) {
                raf.seek(cur_len);
                nrec = raf.readShort();
                nsweep = raf.readShort();
                byteoff = raf.readShort();
                len += 2;
                nray = raf.readShort();
                len += 2;
                cur_len += 12;
                beg_rec = false;
            }
            if (nsweep <= number_sweeps & rays_count % beg == 0) {
                beg = 0;
                for (int i = 0; i < nparams; ++i) {
                    final int idh_len = cur_len + 12 + i * 76;
                    raf.seek(idh_len);
                    this.base_time[nu] = raf.readInt();
                    raf.skipBytes(2);
                    this.year[nu] = raf.readShort();
                    this.month[nu] = raf.readShort();
                    this.day[nu] = raf.readShort();
                    ++nu;
                    num_sweep[i] = raf.readShort();
                    num_rays_swp[i] = raf.readShort();
                    indx_1ray[i] = raf.readShort();
                    raf.skipBytes(2);
                    num_rays_act[i] = raf.readShort();
                    beg += num_rays_act[i];
                    angl_swp[i] = raf.readShort();
                    bin_len[i] = raf.readShort();
                    data_type[i] = raf.readShort();
                }
                cur_len += nparams * 76;
            }
            len = cur_len;
            if (end_rec) {
                raf.seek(cur_len);
                a0 = raf.readShort();
                cur_len += 2;
                if (a0 == 1) {
                    if (cur_len % 6144 == 0) {
                        beg_rec = true;
                        end_rec = true;
                        ++rays_count;
                        read_ray_hdr = true;
                        pos = 0;
                        data_read = 0;
                        nb = 0;
                        len = cur_len;
                        continue;
                    }
                    end_rec = true;
                    len = cur_len;
                    ++rays_count;
                    continue;
                }
                else {
                    nwords = (a0 & 0x7FFF);
                    end_words = nwords - 6;
                    data_read = end_words * 2;
                    end_rec = false;
                    if (cur_len % 6144 == 0) {
                        len = cur_len;
                        read_ray_hdr = true;
                        beg_rec = true;
                        continue;
                    }
                }
            }
            len = cur_len;
            dty = data_type[0];
            if (nparams > 1) {
                kk = rays_count % nparams;
                col = rays_count / nparams;
                dty = data_type[kk];
            }
            else if (number_sweeps > 1) {
                kk = nsweep - 1;
                col = rays_count % irays;
            }
            final String var_name = this.data_name[dty];
            if (read_ray_hdr) {
                if (pos_ray_hdr < 2) {
                    raf.seek(cur_len);
                    beg_az = raf.readShort();
                    cur_len = (len = cur_len + 2);
                    if (cur_len % 6144 == 0) {
                        pos_ray_hdr = 2;
                        beg_rec = true;
                        read_ray_hdr = true;
                        continue;
                    }
                }
                if (pos_ray_hdr < 4) {
                    raf.seek(cur_len);
                    beg_elev = raf.readShort();
                    cur_len = (len = cur_len + 2);
                    if (cur_len % 6144 == 0) {
                        pos_ray_hdr = 4;
                        beg_rec = true;
                        read_ray_hdr = true;
                        continue;
                    }
                }
                if (pos_ray_hdr < 6) {
                    raf.seek(cur_len);
                    end_az = raf.readShort();
                    cur_len = (len = cur_len + 2);
                    if (cur_len % 6144 == 0) {
                        pos_ray_hdr = 6;
                        beg_rec = true;
                        read_ray_hdr = true;
                        continue;
                    }
                }
                if (pos_ray_hdr < 8) {
                    raf.seek(cur_len);
                    end_elev = raf.readShort();
                    cur_len = (len = cur_len + 2);
                    if (cur_len % 6144 == 0) {
                        pos_ray_hdr = 8;
                        beg_rec = true;
                        read_ray_hdr = true;
                        continue;
                    }
                }
                if (pos_ray_hdr < 10) {
                    raf.seek(cur_len);
                    num_bins = raf.readShort();
                    cur_len = (len = cur_len + 2);
                    if (num_bins % 2 != 0) {
                        ++num_bins;
                    }
                    this.num_gates[nsweep - 1] = num_bins;
                    if (cur_len % 6144 == 0) {
                        pos_ray_hdr = 10;
                        beg_rec = true;
                        read_ray_hdr = true;
                        continue;
                    }
                }
                if (pos_ray_hdr < 12) {
                    raf.seek(cur_len);
                    time_start_sw = raf.readShort();
                    cur_len = (len = cur_len + 2);
                }
            }
            az = SigmetIOServiceProvider.calcAz(beg_az, end_az);
            elev = SigmetIOServiceProvider.calcElev(end_elev);
            step = SigmetIOServiceProvider.calcStep(range_first, range_last, num_bins);
            if (cur_len % 6144 == 0) {
                len = cur_len;
                beg_rec = true;
                read_ray_hdr = false;
            }
            else {
                if (pos > 0) {
                    data_read -= pos;
                    pos = 0;
                }
                if (data_read > 0) {
                    raf.seek(cur_len);
                    rayoffset = cur_len;
                    datalen = data_read;
                    for (int j = 0; j < data_read; ++j) {
                        ++cur_len;
                        ++nb;
                        if (cur_len % 6144 == 0) {
                            pos = j + 1;
                            beg_rec = true;
                            read_ray_hdr = false;
                            len = cur_len;
                            raf.seek(cur_len);
                            break;
                        }
                    }
                    raf.seek(cur_len);
                    if (pos > 0) {
                        continue;
                    }
                }
                if (cur_len % 6144 == 0) {
                    pos = 0;
                    beg_rec = true;
                    read_ray_hdr = false;
                    data_read = 0;
                    len = cur_len;
                }
                else {
                    raf.seek(cur_len);
                    rayoffset2 = cur_len;
                    while (nb < num_bins) {
                        a2 = raf.readShort();
                        cur_len += 2;
                        if (a2 == 1) {
                            ray = new Ray(-999.99f, -999.99f, -999.99f, -999.99f, num_bins, (short)(-99), -999, 0, -999, nsweep, var_name, dty);
                            ++rays_count;
                            beg_rec = false;
                            end_rec = true;
                            break;
                        }
                        if (a2 < 0) {
                            nwords = (a2 & 0x7FFF);
                            data_read = nwords * 2;
                            if (cur_len % 6144 == 0) {
                                pos = 0;
                                beg_rec = true;
                                end_rec = false;
                                len = cur_len;
                                read_ray_hdr = false;
                                break;
                            }
                            raf.seek(cur_len);
                            for (int ii = 0; ii < data_read; ++ii) {
                                ++cur_len;
                                ++nb;
                                if (cur_len % 6144 == 0) {
                                    pos = ii + 1;
                                    beg_rec = true;
                                    end_rec = false;
                                    len = cur_len;
                                    read_ray_hdr = false;
                                    raf.seek(cur_len);
                                    break;
                                }
                            }
                            raf.seek(cur_len);
                            if (pos > 0) {
                                break;
                            }
                            continue;
                        }
                        else {
                            if (!(a2 > 0 & a2 != 1)) {
                                continue;
                            }
                            num_zero = a2 * 2;
                            nb += num_zero;
                            if (cur_len % 6144 == 0) {
                                beg_rec = true;
                                end_rec = false;
                                read_ray_hdr = false;
                                pos = 0;
                                data_read = 0;
                                len = cur_len;
                                break;
                            }
                            continue;
                        }
                    }
                    if (cur_len % 6144 == 0) {
                        len = cur_len;
                    }
                    else {
                        raf.seek(cur_len);
                        if (nb == num_bins) {
                            a2 = raf.readShort();
                            cur_len += 2;
                            end_rec = true;
                            ray = new Ray(range_first, step, az, elev, num_bins, time_start_sw, rayoffset, datalen, rayoffset2, nsweep, var_name, dty);
                            ++rays_count;
                            ++two;
                            if (nsweep == number_sweeps & rays_count % beg == 0) {
                                if (var_name.trim().equalsIgnoreCase("TotalPower")) {
                                    totalPower.add(ray);
                                    break;
                                }
                                if (var_name.trim().equalsIgnoreCase("Reflectivity")) {
                                    reflectivity.add(ray);
                                    break;
                                }
                                if (var_name.trim().equalsIgnoreCase("Velocity")) {
                                    velocity.add(ray);
                                    break;
                                }
                                if (var_name.trim().equalsIgnoreCase("Width")) {
                                    width.add(ray);
                                    break;
                                }
                                if (var_name.trim().equalsIgnoreCase("DifferentialReflectivity")) {
                                    diffReflectivity.add(ray);
                                    break;
                                }
                                System.out.println(" Error: Unknown Radial Variable found!!");
                                break;
                            }
                            else if (cur_len % 6144 == 0) {
                                beg_rec = true;
                                end_rec = true;
                                read_ray_hdr = true;
                                pos = 0;
                                data_read = 0;
                                nb = 0;
                                len = cur_len;
                                if (var_name.trim().equalsIgnoreCase("TotalPower")) {
                                    totalPower.add(ray);
                                    continue;
                                }
                                if (var_name.trim().equalsIgnoreCase("Reflectivity")) {
                                    reflectivity.add(ray);
                                    continue;
                                }
                                if (var_name.trim().equalsIgnoreCase("Velocity")) {
                                    velocity.add(ray);
                                    continue;
                                }
                                if (var_name.trim().equalsIgnoreCase("Width")) {
                                    width.add(ray);
                                    continue;
                                }
                                if (var_name.trim().equalsIgnoreCase("DifferentialReflectivity")) {
                                    diffReflectivity.add(ray);
                                    continue;
                                }
                                System.out.println(" Error: Unknown Radial Variable found!!");
                                continue;
                            }
                        }
                        if (this.firstRay == null) {
                            this.firstRay = ray;
                        }
                        if (var_name.trim().equalsIgnoreCase("TotalPower")) {
                            totalPower.add(ray);
                        }
                        else if (var_name.trim().equalsIgnoreCase("Reflectivity")) {
                            reflectivity.add(ray);
                        }
                        else if (var_name.trim().equalsIgnoreCase("Velocity")) {
                            velocity.add(ray);
                        }
                        else if (var_name.trim().equalsIgnoreCase("Width")) {
                            width.add(ray);
                        }
                        else if (var_name.trim().equalsIgnoreCase("DifferentialReflectivity")) {
                            diffReflectivity.add(ray);
                        }
                        else {
                            System.out.println(" Error: Unknown Radial Variable found!!");
                        }
                        pos = 0;
                        data_read = 0;
                        nb = 0;
                        read_ray_hdr = true;
                        pos_ray_hdr = 0;
                        if (nsweep <= number_sweeps & rays_count % beg == 0) {
                            beg_rec = true;
                            end_rec = true;
                            rays_count = 0;
                            nb = 0;
                            cur_len = (len = 6144 * (nrec + 1));
                            read_ray_hdr = true;
                        }
                        len = cur_len;
                    }
                }
            }
        }
        this.lastRay = ray;
        if (reflectivity.size() > 0) {
            this.reflectivityGroups = (List<List<Ray>>)this.sortScans("reflectivity", reflectivity, 1000);
            this.hasReflectivity = true;
        }
        if (velocity.size() > 0) {
            this.velocityGroups = (List<List<Ray>>)this.sortScans("velocity", velocity, 1000);
            this.hasVelocity = true;
        }
        if (totalPower.size() > 0) {
            this.totalPowerGroups = (List<List<Ray>>)this.sortScans("totalPower", totalPower, 1000);
            this.hasTotalPower = true;
        }
        if (width.size() > 0) {
            this.widthGroups = (List<List<Ray>>)this.sortScans("width", width, 1000);
            this.hasWidth = true;
        }
        if (diffReflectivity.size() > 0) {
            this.differentialReflectivityGroups = (List<List<Ray>>)this.sortScans("diffReflectivity", diffReflectivity, 1000);
            this.hasDifferentialReflectivity = true;
        }
        if (time.size() > 0) {
            this.timeGroups = (List<List<Ray>>)this.sortScans("diffReflectivity", diffReflectivity, 1000);
            this.hasTime = true;
        }
    }
    
    private ArrayList sortScans(final String name, final List<Ray> scans, final int siz) {
        final Map<Short, List<Ray>> groupHash = new HashMap<Short, List<Ray>>(siz);
        for (final Ray ray : scans) {
            List<Ray> group = groupHash.get((short)ray.nsweep);
            if (null == group) {
                group = new ArrayList<Ray>();
                groupHash.put((short)ray.nsweep, group);
            }
            group.add(ray);
        }
        final Iterator itt = groupHash.keySet().iterator();
        final ArrayList groups0 = new ArrayList();
        while (itt.hasNext()) {
            final List<Ray> group = groupHash.get(itt.next());
            final Ray[] rr = new Ray[group.size()];
            group.toArray(rr);
            this.checkSort(rr);
        }
        final ArrayList groups2 = new ArrayList((Collection<? extends E>)groupHash.values());
        Collections.sort((List<Object>)groups2, (Comparator<? super Object>)new GroupComparator());
        for (int i = 0; i < groups2.size(); ++i) {
            final ArrayList group2 = groups2.get(i);
            final Ray r = group2.get(0);
            this.max_radials = Math.max(this.max_radials, group2.size());
            this.min_radials = Math.min(this.min_radials, group2.size());
        }
        if (this.debugRadials) {
            System.out.println(name + " min_radials= " + this.min_radials + " max_radials= " + this.max_radials);
            for (int i = 0; i < groups2.size(); ++i) {
                final ArrayList group2 = groups2.get(i);
                Ray lastr = group2.get(0);
                for (int j = 1; j < group2.size(); ++j) {
                    final Ray r2 = group2.get(j);
                    if (r2.getTime() < lastr.getTime()) {
                        System.out.println(" out of order " + j);
                    }
                    lastr = r2;
                }
            }
        }
        return groups2;
    }
    
    public List getTotalPowerGroups() {
        return this.totalPowerGroups;
    }
    
    public List getVelocityGroups() {
        return this.velocityGroups;
    }
    
    public List getWidthGroups() {
        return this.widthGroups;
    }
    
    public List getReflectivityGroups() {
        return this.reflectivityGroups;
    }
    
    public List getDifferentialReflectivityGroups() {
        return this.differentialReflectivityGroups;
    }
    
    public int[] getNumberGates() {
        return this.num_gates;
    }
    
    public int[] getStartSweep() {
        return this.base_time;
    }
    
    void checkSort(final Ray[] r) {
        int j = 0;
        int n = 0;
        int n2 = 0;
        int n3 = 0;
        short time1 = 0;
        short time2 = 0;
        final int[] k1 = new int[300];
        final int[] k2 = new int[300];
        for (int i = 0; i < r.length - 1; ++i) {
            time1 = r[i].getTime();
            time2 = r[i + 1].getTime();
            if (time1 != time2) {
                k2[j] = i;
                ++j;
                k1[j] = i + 1;
            }
        }
        if (k2[j] < r.length - 1) {
            k1[j] = k2[j - 1] + 1;
            k2[j] = r.length - 1;
            n = j + 1;
        }
        int it1 = 0;
        int it2 = 0;
        for (int ii = 0; ii < j + 1; ++ii) {
            n2 = k1[ii];
            for (int l = 0; l < j + 1; ++l) {
                if (l != ii) {
                    n3 = k1[l];
                    if (r[n2].getTime() == r[n3].getTime()) {
                        it1 = ii;
                        it2 = l;
                    }
                }
            }
        }
        n2 = k1[it1];
        n3 = k1[it2];
        final int s1 = k2[it1] - k1[it1] + 1;
        final int s2 = k2[it2] - k1[it2] + 1;
        final float[] t0 = new float[s1];
        final float[] t2 = new float[s2];
        for (int m = 0; m < s1; ++m) {
            t0[m] = r[n2 + m].getAz();
        }
        for (int m = 0; m < s2; ++m) {
            t2[m] = r[n3 + m].getAz();
        }
        float mx0 = t0[0];
        for (int i2 = 0; i2 < s1; ++i2) {
            if (mx0 < t0[i2]) {
                mx0 = t0[i2];
            }
        }
        float mx2 = t2[0];
        for (int i3 = 0; i3 < s2; ++i3) {
            if (mx2 < t2[i3]) {
                mx2 = t2[i3];
            }
        }
        if (mx0 > 330.0f & mx2 < 50.0f) {
            for (int i3 = 0; i3 < s1; ++i3) {
                final float q = r[n2 + i3].getAz();
                r[n2 + i3].setAz(q - 360.0f);
            }
        }
        Arrays.sort(r, new RayComparator());
        for (int i3 = 0; i3 < r.length; ++i3) {
            final float a = r[i3].getAz();
            if (a < 0.0f & a > -361.0f) {
                final float qa = r[i3].getAz();
                r[i3].setAz(qa + 360.0f);
            }
        }
    }
    
    private class GroupComparator implements Comparator<List<Ray>>
    {
        public int compare(final List<Ray> group1, final List<Ray> group2) {
            final Ray record1 = group1.get(0);
            final Ray record2 = group2.get(0);
            return record1.nsweep - record2.nsweep;
        }
    }
    
    class RayComparator implements Comparator<Ray>
    {
        public int compare(final Ray ray1, final Ray ray2) {
            if (ray1.getTime() < ray2.getTime()) {
                return -1;
            }
            if (ray1.getTime() == ray2.getTime()) {
                if (ray1.getAz() < ray2.getAz()) {
                    return -1;
                }
                if (ray1.getAz() > ray2.getAz()) {
                    return 1;
                }
                if (ray1.getAz() == ray2.getAz()) {
                    return 0;
                }
            }
            else if (ray1.getTime() > ray2.getTime()) {
                return 1;
            }
            return 0;
        }
    }
}
