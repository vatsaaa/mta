// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.iosp.sigmet;

import java.io.IOException;
import ucar.ma2.IndexIterator;
import ucar.ma2.Range;
import ucar.unidata.io.RandomAccessFile;

public class Ray
{
    private short bins;
    int dataRead;
    int offset;
    int offset1;
    private float range;
    private float step;
    private float az;
    private float elev;
    private short time;
    String varName;
    int nsweep;
    short datatype;
    
    public Ray(final float range, final float step, final float az, final float elev, final short bins, final short time, final int offset, final int dataRead, final int offset1, final int nsweep, final String name, final short datatype) {
        this.setRange(range);
        this.setStep(step);
        this.setAz(az);
        this.setElev(elev);
        this.setBins(bins);
        this.setTime(time);
        this.setOffset(offset);
        this.setDataRead(dataRead);
        this.setOffset1(offset1);
        this.setName(name);
        this.setNsweep(nsweep);
        this.setDataType(datatype);
    }
    
    public short getDataType() {
        return this.datatype;
    }
    
    public void setDataType(final short datatype) {
        this.datatype = datatype;
    }
    
    public float getRange() {
        return this.range;
    }
    
    public void setRange(final float range) {
        this.range = range;
    }
    
    public float getStep() {
        return this.step;
    }
    
    public void setStep(final float step) {
        this.step = step;
    }
    
    public int getNsweep() {
        return this.nsweep;
    }
    
    public void setNsweep(final int nsweep) {
        this.nsweep = nsweep;
    }
    
    public float getAz() {
        if (this.az < 0.0f & this.az > -361.0f) {
            this.az += 360.0f;
        }
        return this.az;
    }
    
    public void setAz(final float az) {
        this.az = az;
    }
    
    public float getElev() {
        return this.elev;
    }
    
    public void setElev(final float elev) {
        this.elev = elev;
    }
    
    public short getBins() {
        return this.bins;
    }
    
    public void setBins(final short bins) {
        this.bins = bins;
    }
    
    public short getTime() {
        return this.time;
    }
    
    public void setTime(final short time) {
        this.time = time;
    }
    
    public int getOffset() {
        return this.offset;
    }
    
    public void setOffset(final int offset) {
        this.offset = offset;
    }
    
    public int getDataRead() {
        return this.dataRead;
    }
    
    public void setDataRead(final int dataRead) {
        this.dataRead = dataRead;
    }
    
    public int getOffset1() {
        return this.offset1;
    }
    
    public void setOffset1(final int offset1) {
        this.offset1 = offset1;
    }
    
    public void setName(final String name) {
        this.varName = name;
    }
    
    public String getName() {
        return this.varName;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o instanceof Ray) {
            final Ray oo = (Ray)o;
            return this.range == oo.range & this.step == oo.step & this.az == oo.az & this.elev == oo.elev & this.bins == oo.bins & this.time == oo.time;
        }
        return false;
    }
    
    @Override
    public int hashCode() {
        return new Float(this.range).hashCode() + new Float(this.step).hashCode() + new Float(this.az).hashCode() + new Float(this.elev).hashCode() + new Short(this.bins).hashCode() + new Short(this.time).hashCode();
    }
    
    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer();
        sb.append("Range=" + this.range);
        sb.append(" Step=" + this.step);
        if (this.az > -361.0f & this.az < 0.0f) {
            this.az += 360.0f;
        }
        sb.append(" Az=" + this.az);
        sb.append(" Elev=" + this.elev);
        sb.append(" Bins=" + this.bins);
        sb.append(" Time=" + this.time);
        return sb.toString();
    }
    
    public void readData(final RandomAccessFile raf, final Range gateRange, final IndexIterator ii) throws IOException {
        final int REC_SIZE = 6144;
        raf.seek(this.offset);
        final byte[] data = new byte[this.bins];
        final float[] dd = new float[this.bins];
        int nb = 0;
        if (this.dataRead > 0) {
            raf.seek(this.offset);
            for (int i = 0; i < this.dataRead; ++i) {
                final byte d = raf.readByte();
                dd[i] = SigmetIOServiceProvider.calcData(SigmetIOServiceProvider.recHdr, this.getDataType(), d);
                ++nb;
            }
        }
        raf.seek(this.offset1);
        int cur_len = this.offset1;
        while (nb < this.bins) {
            final short a00 = raf.readShort();
            cur_len += 2;
            if (a00 == 1) {
                for (int uk = 0; uk < this.bins; ++uk) {
                    dd[uk] = -999.99f;
                }
                break;
            }
            if (a00 < 0) {
                final int nwords = a00 & 0x7FFF;
                final int dataRead1 = nwords * 2;
                int pos = 0;
                if (cur_len % 6144 == 0) {
                    pos = 0;
                    break;
                }
                raf.seek(cur_len);
                for (int j = 0; j < dataRead1; ++j) {
                    final byte d = raf.readByte();
                    dd[nb] = SigmetIOServiceProvider.calcData(SigmetIOServiceProvider.recHdr, this.getDataType(), d);
                    ++nb;
                    ++cur_len;
                    if (nb % 6144 == 0) {
                        pos = j + 1;
                        break;
                    }
                }
                if (pos > 0) {
                    break;
                }
                continue;
            }
            else {
                if (!(a00 > 0 & a00 != 1)) {
                    continue;
                }
                int dataRead1;
                for (int num_zero = dataRead1 = a00 * 2, k = 0; k < dataRead1; ++k) {
                    dd[nb + k] = SigmetIOServiceProvider.calcData(SigmetIOServiceProvider.recHdr, this.getDataType(), (byte)0);
                }
                nb += dataRead1;
                if (cur_len % 6144 == 0) {
                    break;
                }
                continue;
            }
        }
        for (int l = gateRange.first(); l <= gateRange.last(); l += gateRange.stride()) {
            if (l >= this.bins) {
                ii.setFloatNext(Float.NaN);
            }
            else {
                ii.setFloatNext(dd[l]);
            }
        }
    }
}
