// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.iosp.sigmet;

import java.math.RoundingMode;
import java.math.BigDecimal;
import java.nio.ByteBuffer;
import java.nio.channels.WritableByteChannel;
import ucar.nc2.iosp.Layout;
import ucar.ma2.Range;
import ucar.ma2.IndexIterator;
import ucar.ma2.InvalidRangeException;
import ucar.nc2.iosp.LayoutRegular;
import ucar.ma2.Section;
import ucar.ma2.ArrayInt;
import ucar.ma2.Array;
import ucar.ma2.Index;
import ucar.ma2.ArrayFloat;
import ucar.nc2.constants.AxisType;
import ucar.nc2.Attribute;
import java.util.List;
import ucar.ma2.DataType;
import ucar.nc2.Structure;
import ucar.nc2.Group;
import ucar.nc2.Dimension;
import java.util.HashMap;
import ucar.nc2.util.CancelTask;
import java.io.IOException;
import java.util.Map;
import ucar.unidata.io.RandomAccessFile;
import ucar.nc2.Variable;
import java.util.ArrayList;
import ucar.nc2.NetcdfFile;
import ucar.nc2.iosp.AbstractIOServiceProvider;

public class SigmetIOServiceProvider extends AbstractIOServiceProvider
{
    private static final String def_datafile = "SIGMET-IRIS";
    private NetcdfFile ncfile;
    ArrayList<Variable> varList;
    RandomAccessFile myRaf;
    int[] tsu_sec;
    int[] sweep_bins;
    String date0;
    public static Map<String, Number> recHdr;
    private SigmetVolumeScan volScan;
    
    public SigmetIOServiceProvider() {
        this.ncfile = null;
        this.varList = null;
        this.myRaf = null;
        this.tsu_sec = null;
        this.sweep_bins = null;
    }
    
    public static void main(final String[] args) {
        String infile = " ";
        if (args.length == 1) {
            infile = args[0];
        }
        else {
            System.out.println("Usage: java SigmetIOServiceProvider inputFile");
            System.exit(0);
        }
        try {
            NetcdfFile.registerIOProvider(SigmetIOServiceProvider.class);
            final NetcdfFile ncfile = NetcdfFile.open(infile);
            System.out.println("ncfile = \n" + ncfile);
        }
        catch (Exception e) {
            System.out.println("MAIN!!!   " + e.toString());
            e.printStackTrace();
        }
    }
    
    public String getFileTypeDescription() {
        return "SIGMET-IRIS";
    }
    
    @Override
    public String getFileTypeVersion() {
        return "SIGMET-IRIS";
    }
    
    public String getFileTypeId() {
        return "SIGMET";
    }
    
    public boolean isValidFile(final RandomAccessFile raf) {
        try {
            raf.order(1);
            raf.seek(24L);
            return raf.readShort() == 15;
        }
        catch (IOException ioe) {
            System.out.println("In isValidFile(): " + ioe.toString());
            return false;
        }
    }
    
    @Override
    public void open(final RandomAccessFile raf, final NetcdfFile ncfile, final CancelTask cancelTask) throws IOException {
        this.ncfile = ncfile;
        this.myRaf = raf;
        final Map<String, String> hdrNames = new HashMap<String, String>();
        this.volScan = new SigmetVolumeScan(raf, ncfile, this.varList);
        this.varList = this.init(raf, ncfile, hdrNames);
    }
    
    public static Map<String, Number> readRecordsHdr(final RandomAccessFile raf) {
        final Map<String, Number> recHdr1 = new HashMap<String, Number>();
        try {
            int nparams = 0;
            raf.seek(452L);
            final int prf = raf.readInt();
            raf.seek(480L);
            final int wave = raf.readInt();
            final float vNyq = calcNyquist(prf, wave);
            recHdr1.put("vNyq", new Float(vNyq));
            raf.seek(6324L);
            final int radar_lat = raf.readInt();
            final int radar_lon = raf.readInt();
            final short ground_height = raf.readShort();
            final short radar_height = raf.readShort();
            raf.skipBytes(4);
            final short num_rays = raf.readShort();
            raf.skipBytes(2);
            final int radar_alt = raf.readInt();
            raf.seek(6648L);
            final int time_beg = raf.readInt();
            raf.seek(6652L);
            final int time_end = raf.readInt();
            raf.seek(6772L);
            final int data_mask = raf.readInt();
            for (int j = 0; j < 32; ++j) {
                nparams += (data_mask >> j & 0x1);
            }
            raf.seek(6912L);
            final short multiprf = raf.readShort();
            raf.seek(7408L);
            final int range_first = raf.readInt();
            final int range_last = raf.readInt();
            raf.skipBytes(2);
            short bins = raf.readShort();
            if (bins % 2 != 0) {
                ++bins;
            }
            raf.skipBytes(4);
            final int step = raf.readInt();
            raf.seek(7574L);
            final short number_sweeps = raf.readShort();
            raf.seek(12312L);
            final int base_time = raf.readInt();
            raf.skipBytes(2);
            final short year = raf.readShort();
            final short month = raf.readShort();
            final short day = raf.readShort();
            recHdr1.put("radar_lat", new Float(calcAngle(radar_lat)));
            recHdr1.put("radar_lon", new Float(calcAngle(radar_lon)));
            recHdr1.put("range_first", new Integer(range_first));
            recHdr1.put("range_last", new Integer(range_last));
            recHdr1.put("ground_height", new Short(ground_height));
            recHdr1.put("radar_height", new Short(radar_height));
            recHdr1.put("radar_alt", new Integer(radar_alt));
            recHdr1.put("step", new Integer(step));
            recHdr1.put("bins", new Short(bins));
            recHdr1.put("num_rays", new Short(num_rays));
            recHdr1.put("nparams", new Integer(nparams));
            recHdr1.put("multiprf", new Short(multiprf));
            recHdr1.put("number_sweeps", new Short(number_sweeps));
            recHdr1.put("year", new Short(year));
            recHdr1.put("month", new Short(month));
            recHdr1.put("day", new Short(day));
            recHdr1.put("base_time", new Integer(base_time));
        }
        catch (Exception e) {
            System.out.println(e.toString());
            e.printStackTrace();
        }
        return recHdr1;
    }
    
    public Map<String, String> readStnNames(final RandomAccessFile raf) {
        final Map<String, String> hdrNames = new HashMap<String, String>();
        try {
            raf.seek(6288L);
            final String stnName = raf.readString(16);
            raf.seek(6306L);
            final String stnName_util = raf.readString(16);
            hdrNames.put("StationName", stnName.trim());
            hdrNames.put("StationName_SetupUtility", stnName_util.trim());
        }
        catch (Exception e) {
            System.out.println(e.toString());
            e.printStackTrace();
        }
        return hdrNames;
    }
    
    public ArrayList<Variable> init(final RandomAccessFile raf, final NetcdfFile ncfile, Map<String, String> hdrNames) throws IOException {
        final String[] data_name = { " ", "TotalPower", "Reflectivity", "Velocity", "Width", "Differential_Reflectivity" };
        final String[] unit = { " ", "dbZ", "dbZ", "m/sec", "m/sec", "dB" };
        final int[] type = { 1, 2, 3, 4, 5 };
        final String def_datafile = "SIGMET-IRIS";
        final String tim = "";
        final int ngates = 0;
        SigmetIOServiceProvider.recHdr = readRecordsHdr(raf);
        hdrNames = this.readStnNames(raf);
        final String stnName = hdrNames.get("StationName");
        final String stnName_util = hdrNames.get("StationName_SetupUtility");
        final float radar_lat = SigmetIOServiceProvider.recHdr.get("radar_lat");
        final float radar_lon = SigmetIOServiceProvider.recHdr.get("radar_lon");
        final short ground_height = SigmetIOServiceProvider.recHdr.get("ground_height");
        final short radar_height = SigmetIOServiceProvider.recHdr.get("radar_height");
        final int radar_alt = SigmetIOServiceProvider.recHdr.get("radar_alt") / 100;
        final short num_rays = SigmetIOServiceProvider.recHdr.get("num_rays");
        final short bins = SigmetIOServiceProvider.recHdr.get("bins");
        final float range_first = SigmetIOServiceProvider.recHdr.get("range_first") * 0.01f;
        final float range_last = SigmetIOServiceProvider.recHdr.get("range_last") * 0.01f;
        final short number_sweeps = SigmetIOServiceProvider.recHdr.get("number_sweeps");
        final int nparams = SigmetIOServiceProvider.recHdr.get("nparams");
        final short year = SigmetIOServiceProvider.recHdr.get("year");
        final short month = SigmetIOServiceProvider.recHdr.get("month");
        final short day = SigmetIOServiceProvider.recHdr.get("day");
        final int base_time = SigmetIOServiceProvider.recHdr.get("base_time");
        this.sweep_bins = new int[nparams * number_sweeps];
        if (number_sweeps > 1) {
            this.sweep_bins = this.volScan.getNumberGates();
        }
        else {
            for (int kk = 0; kk < nparams; ++kk) {
                this.sweep_bins[kk] = bins;
            }
        }
        final Dimension scanR = new Dimension("scanR", number_sweeps, true);
        final Dimension radial = new Dimension("radial", num_rays, true);
        final Dimension[] gateR = new Dimension[number_sweeps];
        String dim_name = "gateR";
        for (int j = 0; j < number_sweeps; ++j) {
            if (number_sweeps > 1) {
                dim_name = "gateR_sweep_" + (j + 1);
            }
            gateR[j] = new Dimension(dim_name, this.sweep_bins[j], true);
        }
        ncfile.addDimension(null, scanR);
        ncfile.addDimension(null, radial);
        for (int j = 0; j < number_sweeps; ++j) {
            ncfile.addDimension(null, gateR[j]);
        }
        final ArrayList<Dimension> dims0 = new ArrayList<Dimension>();
        final ArrayList<Dimension> dims2 = new ArrayList<Dimension>();
        final ArrayList<Dimension> dims3 = new ArrayList<Dimension>();
        final ArrayList<Dimension> dims4 = new ArrayList<Dimension>();
        final ArrayList<Variable> varList = new ArrayList<Variable>();
        final Variable[][] v = new Variable[nparams][number_sweeps];
        String var_name = "";
        for (int i = 0; i < nparams; ++i) {
            final int tp = type[i];
            var_name = data_name[tp];
            for (int jj = 0; jj < number_sweeps; ++jj) {
                if (number_sweeps > 1) {
                    var_name = data_name[tp] + "_sweep_" + (jj + 1);
                }
                (v[i][jj] = new Variable(ncfile, null, null, var_name)).setDataType(DataType.FLOAT);
                dims3.add(radial);
                dims3.add(gateR[jj]);
                v[i][jj].setDimensions(dims3);
                v[i][jj].addAttribute(new Attribute("long_name", var_name));
                v[i][jj].addAttribute(new Attribute("units", unit[tp]));
                final String coordinates = "time elevationR azimuthR distanceR";
                v[i][jj].addAttribute(new Attribute("_CoordinateAxes", coordinates));
                v[i][jj].addAttribute(new Attribute("missing_value", -999.99f));
                ncfile.addVariable(null, v[i][jj]);
                varList.add(v[i][jj]);
                dims3.clear();
            }
        }
        this.tsu_sec = new int[number_sweeps];
        final String[] tsu = new String[number_sweeps];
        final String[] time_units = new String[number_sweeps];
        this.tsu_sec = this.volScan.getStartSweep();
        for (int k = 0; k < number_sweeps; ++k) {
            String st1 = Short.toString(month);
            if (st1.length() < 2) {
                st1 = "0" + st1;
            }
            String st2 = Short.toString(day);
            if (st2.length() < 2) {
                st2 = "0" + st2;
            }
            this.date0 = String.valueOf(year) + "-" + st1 + "-" + st2;
            tsu[k] = this.date0 + "T" + calcTime(this.tsu_sec[k], 0) + "Z";
        }
        for (int l = 0; l < number_sweeps; ++l) {
            time_units[l] = "secs since " + tsu[l];
        }
        dims0.add(radial);
        final Variable[] time = new Variable[number_sweeps];
        final String tm = "time";
        String tm_name = "";
        for (int m = 0; m < number_sweeps; ++m) {
            tm_name = tm;
            if (number_sweeps > 1) {
                tm_name = tm + "_sweep_" + (m + 1);
            }
            (time[m] = new Variable(ncfile, null, null, tm_name)).setDataType(DataType.INT);
            time[m].setDimensions(dims0);
            time[m].addAttribute(new Attribute("long_name", "time from start of sweep"));
            time[m].addAttribute(new Attribute("units", time_units[m]));
            time[m].addAttribute(new Attribute("_CoordinateAxisType", AxisType.Time.toString()));
            time[m].addAttribute(new Attribute("missing_value", -99));
            ncfile.addVariable(null, time[m]);
            varList.add(time[m]);
        }
        final Variable[] elevationR = new Variable[number_sweeps];
        final String ele = "elevationR";
        String ele_name = "";
        for (int j2 = 0; j2 < number_sweeps; ++j2) {
            ele_name = ele;
            if (number_sweeps > 1) {
                ele_name = ele + "_sweep_" + (j2 + 1);
            }
            (elevationR[j2] = new Variable(ncfile, null, null, ele_name)).setDataType(DataType.FLOAT);
            elevationR[j2].setDimensions(dims0);
            elevationR[j2].addAttribute(new Attribute("long_name", "elevation angle"));
            elevationR[j2].addAttribute(new Attribute("units", "degrees"));
            elevationR[j2].addAttribute(new Attribute("_CoordinateAxisType", AxisType.RadialElevation.toString()));
            elevationR[j2].addAttribute(new Attribute("missing_value", -999.99f));
            ncfile.addVariable(null, elevationR[j2]);
            varList.add(elevationR[j2]);
        }
        final Variable[] azimuthR = new Variable[number_sweeps];
        final String azim = "azimuthR";
        String azim_name = "";
        for (int j3 = 0; j3 < number_sweeps; ++j3) {
            azim_name = azim;
            if (number_sweeps > 1) {
                azim_name = azim + "_sweep_" + (j3 + 1);
            }
            (azimuthR[j3] = new Variable(ncfile, null, null, azim_name)).setDataType(DataType.FLOAT);
            azimuthR[j3].setDimensions(dims0);
            azimuthR[j3].addAttribute(new Attribute("long_name", "azimuth angle"));
            azimuthR[j3].addAttribute(new Attribute("units", "degrees"));
            azimuthR[j3].addAttribute(new Attribute("_CoordinateAxisType", AxisType.RadialAzimuth.toString()));
            azimuthR[j3].addAttribute(new Attribute("missing_value", -999.99f));
            ncfile.addVariable(null, azimuthR[j3]);
            varList.add(azimuthR[j3]);
        }
        final Variable[] distanceR = new Variable[number_sweeps];
        final String dName = "distanceR";
        String dist_name = "";
        for (int j4 = 0; j4 < number_sweeps; ++j4) {
            dist_name = dName;
            if (number_sweeps > 1) {
                dist_name = dName + "_sweep_" + (j4 + 1);
            }
            (distanceR[j4] = new Variable(ncfile, null, null, dist_name)).setDataType(DataType.FLOAT);
            dims2.add(gateR[j4]);
            distanceR[j4].setDimensions(dims2);
            distanceR[j4].addAttribute(new Attribute("long_name", "radial distance"));
            distanceR[j4].addAttribute(new Attribute("units", "m"));
            distanceR[j4].addAttribute(new Attribute("_CoordinateAxisType", AxisType.RadialDistance.toString()));
            ncfile.addVariable(null, distanceR[j4]);
            varList.add(distanceR[j4]);
            dims2.clear();
        }
        dims4.add(scanR);
        final Variable numGates = new Variable(ncfile, null, null, "numGates");
        numGates.setDataType(DataType.INT);
        numGates.setDimensions(dims4);
        numGates.addAttribute(new Attribute("long_name", "number of gates in the sweep"));
        ncfile.addVariable(null, numGates);
        varList.add(numGates);
        ncfile.addAttribute(null, new Attribute("definition", "SIGMET-IRIS RAW"));
        ncfile.addAttribute(null, new Attribute("description", "SIGMET-IRIS data are reading by Netcdf IOSP"));
        ncfile.addAttribute(null, new Attribute("StationName", stnName));
        ncfile.addAttribute(null, new Attribute("StationName_SetupUtility", stnName_util));
        ncfile.addAttribute(null, new Attribute("radar_lat", new Float(radar_lat)));
        ncfile.addAttribute(null, new Attribute("radar_lon", new Float(radar_lon)));
        ncfile.addAttribute(null, new Attribute("ground_height", new Short(ground_height)));
        ncfile.addAttribute(null, new Attribute("radar_height", new Short(radar_height)));
        ncfile.addAttribute(null, new Attribute("radar_alt", new Integer(radar_alt)));
        ncfile.addAttribute(null, new Attribute("num_data_types", new Integer(nparams)));
        ncfile.addAttribute(null, new Attribute("number_sweeps", new Short(number_sweeps)));
        final String sn = "start_sweep";
        String snn = "";
        for (int j5 = 0; j5 < number_sweeps; ++j5) {
            snn = sn;
            if (number_sweeps > 1) {
                snn = sn + "_" + (j5 + 1);
            }
            ncfile.addAttribute(null, new Attribute(snn, tsu[j5]));
        }
        ncfile.addAttribute(null, new Attribute("num_rays", new Short(num_rays)));
        ncfile.addAttribute(null, new Attribute("max_number_gates", new Short(bins)));
        ncfile.addAttribute(null, new Attribute("range_first", new Float(range_first)));
        ncfile.addAttribute(null, new Attribute("range_last", new Float(range_last)));
        ncfile.addAttribute(null, new Attribute("DataType", "Radial"));
        ncfile.addAttribute(null, new Attribute("Conventions", "_Coordinates"));
        this.doNetcdfFileCoordinate(ncfile, this.volScan.base_time, this.volScan.year, this.volScan.month, this.volScan.day, varList, SigmetIOServiceProvider.recHdr);
        ncfile.finish();
        return varList;
    }
    
    public void doNetcdfFileCoordinate(final NetcdfFile ncfile, final int[] bst, final short[] yr, final short[] m, final short[] dda, final ArrayList<Variable> varList, final Map<String, Number> recHdr) {
        final String[] unit = { " ", "dbZ", "dbZ", "m/sec", "m/sec", "dB" };
        final String def_datafile = "SIGMET-IRIS";
        final Short header_length = 80;
        final Short ray_header_length = 6;
        int ngates = 0;
        final float radar_lat = recHdr.get("radar_lat");
        final float radar_lon = recHdr.get("radar_lon");
        final short ground_height = recHdr.get("ground_height");
        final short radar_height = recHdr.get("radar_height");
        final int radar_alt = recHdr.get("radar_alt") / 100;
        final short num_rays = recHdr.get("num_rays");
        final float range_first = recHdr.get("range_first") * 0.01f;
        final float range_last = recHdr.get("range_last") * 0.01f;
        final short number_sweeps = recHdr.get("number_sweeps");
        final int nparams = recHdr.get("nparams");
        final int last_t = this.volScan.lastRay.getTime();
        String sss1 = Short.toString(m[0]);
        if (sss1.length() < 2) {
            sss1 = "0" + sss1;
        }
        String sss2 = Short.toString(dda[0]);
        if (sss2.length() < 2) {
            sss2 = "0" + sss2;
        }
        final String base_date0 = String.valueOf(yr[0]) + "-" + sss1 + "-" + sss2;
        String sss3 = Short.toString(m[number_sweeps - 1]);
        if (sss3.length() < 2) {
            sss3 = "0" + sss3;
        }
        String sss4 = Short.toString(dda[number_sweeps - 1]);
        if (sss4.length() < 2) {
            sss4 = "0" + sss4;
        }
        final String base_date2 = String.valueOf(yr[number_sweeps - 1]) + "-" + sss3 + "-" + sss4;
        final String start_time = base_date0 + "T" + calcTime(bst[0], 0) + "Z";
        final String end_time = base_date2 + "T" + calcTime(bst[number_sweeps - 1], last_t) + "Z";
        ncfile.addAttribute(null, new Attribute("time_coverage_start", start_time));
        ncfile.addAttribute(null, new Attribute("time_coverage_end", end_time));
        try {
            final int sz = varList.size();
            final ArrayFloat.D2[] dataArr = new ArrayFloat.D2[nparams * number_sweeps];
            final Index[] dataIndex = new Index[nparams * number_sweeps];
            Variable var = null;
            final String var_name = "";
            final Ray[] rtemp = new Ray[num_rays];
            final Variable[] distanceR = new Variable[number_sweeps];
            final ArrayFloat.D1[] distArr = new ArrayFloat.D1[number_sweeps];
            final Index[] distIndex = new Index[number_sweeps];
            String distName = "distanceR";
            for (int i = 0; i < number_sweeps; ++i) {
                if (number_sweeps > 1) {
                    distName = "distanceR_sweep_" + (i + 1);
                }
                for (int ix = 0; ix < sz; ++ix) {
                    var = varList.get(ix);
                    if (var.getName().equals(distName.trim())) {
                        distanceR[i] = var;
                        break;
                    }
                }
                distArr[i] = (ArrayFloat.D1)Array.factory(DataType.FLOAT, distanceR[i].getShape());
                distIndex[i] = distArr[i].getIndex();
                ngates = this.sweep_bins[i];
                final float stp = calcStep(range_first, range_last, (short)ngates);
                for (int ii = 0; ii < ngates; ++ii) {
                    distArr[i].setFloat(distIndex[i].set(ii), range_first + ii * stp);
                }
            }
            List rgp = this.volScan.getTotalPowerGroups();
            if (rgp.size() == 0) {
                rgp = this.volScan.getReflectivityGroups();
            }
            final List[] sgp = new ArrayList[number_sweeps];
            for (int j = 0; j < number_sweeps; ++j) {
                sgp[j] = rgp.get((short)j);
            }
            final Variable[] time = new Variable[number_sweeps];
            final ArrayInt.D1[] timeArr = new ArrayInt.D1[number_sweeps];
            final Index[] timeIndex = new Index[number_sweeps];
            String t_n = "time";
            for (int k = 0; k < number_sweeps; ++k) {
                if (number_sweeps > 1) {
                    t_n = "time_sweep_" + (k + 1);
                }
                for (int ix2 = 0; ix2 < sz; ++ix2) {
                    var = varList.get(ix2);
                    if (var.getName().equals(t_n.trim())) {
                        time[k] = var;
                        break;
                    }
                }
                timeArr[k] = (ArrayInt.D1)Array.factory(DataType.INT, time[k].getShape());
                timeIndex[k] = timeArr[k].getIndex();
                final List rlist = sgp[k];
                for (int jj = 0; jj < num_rays; ++jj) {
                    rtemp[jj] = rlist.get(jj);
                }
                for (int jj = 0; jj < num_rays; ++jj) {
                    timeArr[k].setInt(timeIndex[k].set(jj), rtemp[jj].getTime());
                }
            }
            final Variable[] azimuthR = new Variable[number_sweeps];
            final ArrayFloat.D1[] azimArr = new ArrayFloat.D1[number_sweeps];
            final Index[] azimIndex = new Index[number_sweeps];
            String azimName = "azimuthR";
            for (int l = 0; l < number_sweeps; ++l) {
                if (number_sweeps > 1) {
                    azimName = "azimuthR_sweep_" + (l + 1);
                }
                for (int ix3 = 0; ix3 < sz; ++ix3) {
                    var = varList.get(ix3);
                    if (var.getName().equals(azimName.trim())) {
                        azimuthR[l] = var;
                        break;
                    }
                }
                azimArr[l] = (ArrayFloat.D1)Array.factory(DataType.FLOAT, azimuthR[l].getShape());
                azimIndex[l] = azimArr[l].getIndex();
                final List rlist2 = sgp[l];
                for (int jj2 = 0; jj2 < num_rays; ++jj2) {
                    rtemp[jj2] = rlist2.get(jj2);
                }
                for (int jj2 = 0; jj2 < num_rays; ++jj2) {
                    azimArr[l].setFloat(azimIndex[l].set(jj2), rtemp[jj2].getAz());
                }
            }
            final Variable[] elevationR = new Variable[number_sweeps];
            final ArrayFloat.D1[] elevArr = new ArrayFloat.D1[number_sweeps];
            final Index[] elevIndex = new Index[number_sweeps];
            String elevName = "elevationR";
            for (int i2 = 0; i2 < number_sweeps; ++i2) {
                if (number_sweeps > 1) {
                    elevName = "elevationR_sweep_" + (i2 + 1);
                }
                for (int ix4 = 0; ix4 < sz; ++ix4) {
                    var = varList.get(ix4);
                    if (var.getName().equals(elevName.trim())) {
                        elevationR[i2] = var;
                        break;
                    }
                }
                elevArr[i2] = (ArrayFloat.D1)Array.factory(DataType.FLOAT, elevationR[i2].getShape());
                elevIndex[i2] = elevArr[i2].getIndex();
                final List rlist3 = sgp[i2];
                for (int jj3 = 0; jj3 < num_rays; ++jj3) {
                    rtemp[jj3] = rlist3.get(jj3);
                }
                for (int jj3 = 0; jj3 < num_rays; ++jj3) {
                    elevArr[i2].setFloat(elevIndex[i2].set(jj3), rtemp[jj3].getElev());
                }
            }
            Variable numGates = null;
            for (int i3 = 0; i3 < number_sweeps; ++i3) {
                for (int ix5 = 0; ix5 < sz; ++ix5) {
                    var = varList.get(ix5);
                    if (var.getName().equals("numGates")) {
                        numGates = var;
                        break;
                    }
                }
            }
            final ArrayInt.D1 gatesArr = (ArrayInt.D1)Array.factory(DataType.INT, numGates.getShape());
            final Index gatesIndex = gatesArr.getIndex();
            for (int i4 = 0; i4 < number_sweeps; ++i4) {
                final List rlist4 = sgp[i4];
                for (int jj4 = 0; jj4 < num_rays; ++jj4) {
                    rtemp[jj4] = rlist4.get(jj4);
                }
                ngates = rtemp[0].getBins();
                gatesArr.setInt(gatesIndex.set(i4), ngates);
            }
            for (int i4 = 0; i4 < number_sweeps; ++i4) {
                distanceR[i4].setCachedData(distArr[i4], false);
                time[i4].setCachedData(timeArr[i4], false);
                azimuthR[i4].setCachedData(azimArr[i4], false);
                elevationR[i4].setCachedData(elevArr[i4], false);
            }
            numGates.setCachedData(gatesArr, false);
        }
        catch (Exception e) {
            System.out.println(e.toString());
            e.printStackTrace();
        }
    }
    
    public Array readData1(final Variable v2, final Section section) throws IOException, InvalidRangeException {
        final int[] sh = section.getShape();
        Array temp = Array.factory(v2.getDataType(), sh);
        final long pos0 = 0L;
        final LayoutRegular index = new LayoutRegular(pos0, v2.getElementSize(), v2.getShape(), section);
        if (v2.getName().startsWith("time") | v2.getName().startsWith("numGates")) {
            temp = this.readIntData(index, v2);
        }
        else {
            temp = this.readFloatData(index, v2);
        }
        return temp;
    }
    
    public Array readData(final Variable v2, final Section section) throws IOException, InvalidRangeException {
        final Array data = Array.factory(v2.getDataType().getPrimitiveClassType(), section.getShape());
        final IndexIterator ii = data.getIndexIterator();
        List groups = null;
        final String shortName = v2.getName();
        if (shortName.startsWith("Reflectivity")) {
            groups = this.volScan.getReflectivityGroups();
        }
        else if (shortName.startsWith("Velocity")) {
            groups = this.volScan.getVelocityGroups();
        }
        else if (shortName.startsWith("TotalPower")) {
            groups = this.volScan.getTotalPowerGroups();
        }
        else if (shortName.startsWith("Width")) {
            groups = this.volScan.getWidthGroups();
        }
        else if (shortName.startsWith("DiffReflectivity")) {
            groups = this.volScan.getDifferentialReflectivityGroups();
        }
        if (section.getRank() == 2) {
            final Range radialRange = section.getRange(0);
            final Range gateRange = section.getRange(1);
            final List lli = groups.get(0);
            this.readOneScan(lli, radialRange, gateRange, ii);
        }
        else {
            final Range scanRange = section.getRange(0);
            final Range radialRange2 = section.getRange(1);
            final Range gateRange2 = section.getRange(2);
            for (int i = scanRange.first(); i <= scanRange.last(); i += scanRange.stride()) {
                this.readOneScan(groups.get(i), radialRange2, gateRange2, ii);
            }
        }
        return data;
    }
    
    private void readOneScan(final List mapScan, final Range radialRange, final Range gateRange, final IndexIterator ii) throws IOException {
        final int siz = mapScan.size();
        for (int i = radialRange.first(); i <= radialRange.last(); i += radialRange.stride()) {
            if (i >= siz) {
                this.readOneRadial(null, gateRange, ii);
            }
            else {
                final Ray r = mapScan.get(i);
                this.readOneRadial(r, gateRange, ii);
            }
        }
    }
    
    private void readOneRadial(final Ray r, final Range gateRange, final IndexIterator ii) throws IOException {
        if (r == null) {
            for (int i = gateRange.first(); i <= gateRange.last(); i += gateRange.stride()) {
                ii.setFloatNext(Float.NaN);
            }
            return;
        }
        r.readData(this.volScan.raf, gateRange, ii);
    }
    
    public Array readIntData(final LayoutRegular index, final Variable v2) throws IOException {
        final int[] var = (int[])v2.read().get1DJavaArray(v2.getDataType().getPrimitiveClassType());
        final int[] data = new int[(int)index.getTotalNelems()];
        while (index.hasNext()) {
            final Layout.Chunk chunk = index.next();
            System.arraycopy(var, (int)chunk.getSrcPos() / 4, data, (int)chunk.getDestElem(), chunk.getNelems());
        }
        return Array.factory(data);
    }
    
    public Array readFloatData(final LayoutRegular index, final Variable v2) throws IOException {
        final float[] var = (float[])v2.read().get1DJavaArray(v2.getDataType().getPrimitiveClassType());
        final float[] data = new float[(int)index.getTotalNelems()];
        while (index.hasNext()) {
            final Layout.Chunk chunk = index.next();
            System.arraycopy(var, (int)chunk.getSrcPos() / 4, data, (int)chunk.getDestElem(), chunk.getNelems());
        }
        return Array.factory(data);
    }
    
    public long readToByteChannel11(final Variable v2, final Section section, final WritableByteChannel channel) throws IOException, InvalidRangeException {
        final Array data = this.readData(v2, section);
        final float[] ftdata = new float[(int)data.getSize()];
        final byte[] bytedata = new byte[(int)data.getSize() * 4];
        final IndexIterator iter = data.getIndexIterator();
        int i = 0;
        ByteBuffer buffer = ByteBuffer.allocateDirect(bytedata.length);
        while (iter.hasNext()) {
            ftdata[i] = iter.getFloatNext();
            buffer.put(bytedata[i] = new Float(ftdata[i]).byteValue());
            ++i;
        }
        buffer = ByteBuffer.wrap(bytedata);
        final int count = channel.write(buffer);
        System.out.println("COUNT=" + count);
        if (buffer.hasRemaining()) {
            buffer.compact();
        }
        else {
            buffer.clear();
        }
        return count;
    }
    
    static float calcAngle(final short angle) {
        final double maxval = 65536.0;
        double ang = angle;
        if (ang < 0.0) {
            ang += 65536.0;
        }
        final double temp = ang / 65536.0 * 360.0;
        final BigDecimal bd = new BigDecimal(temp);
        final BigDecimal result = bd.setScale(2, RoundingMode.HALF_DOWN);
        return result.floatValue();
    }
    
    static float calcAngle(final int ang) {
        final double maxval = 4.294967296E9;
        final double temp = ang / 4.294967296E9 * 360.0;
        final BigDecimal bd = new BigDecimal(temp);
        final BigDecimal result = bd.setScale(3, RoundingMode.HALF_DOWN);
        return result.floatValue();
    }
    
    static float calcElev(final short angle) {
        final double maxval = 65536.0;
        double ang = angle;
        if (angle < 0) {
            ang = ~angle + 1;
        }
        final double temp = ang / 65536.0 * 360.0;
        final BigDecimal bd = new BigDecimal(temp);
        final BigDecimal result = bd.setScale(2, RoundingMode.HALF_DOWN);
        return result.floatValue();
    }
    
    static float calcStep(final float range_first, final float range_last, final short num_bins) {
        final float step = (range_last - range_first) / (num_bins - 1);
        final BigDecimal bd = new BigDecimal(step);
        final BigDecimal result = bd.setScale(2, RoundingMode.HALF_DOWN);
        return result.floatValue();
    }
    
    static float calcAz(final short az0, final short az1) {
        final float azim0 = calcAngle(az0);
        final float azim2 = calcAngle(az1);
        float d = 0.0f;
        d = Math.abs(azim0 - azim2);
        if (az0 < 0 & az1 > 0) {
            d = Math.abs(360.0f - azim0) + Math.abs(azim2);
        }
        double temp = azim0 + d * 0.5;
        if (temp > 360.0) {
            temp -= 360.0;
        }
        final BigDecimal bd = new BigDecimal(temp);
        final BigDecimal result = bd.setScale(2, RoundingMode.HALF_DOWN);
        return result.floatValue();
    }
    
    static float calcData(final Map<String, Number> recHdr, final short dty, final byte data) {
        final short[] coef = { 1, 2, 3, 4 };
        final short multiprf = recHdr.get("multiprf");
        final float vNyq = recHdr.get("vNyq");
        double temp = -999.99;
        switch (dty) {
            default: {
                if (data != 0) {
                    temp = ((data & 0xFF) - 64) * 0.5;
                    break;
                }
                break;
            }
            case 3: {
                if (data != 0) {
                    temp = ((data & 0xFF) - 128) / 127.0 * vNyq * coef[multiprf];
                    break;
                }
                break;
            }
            case 4: {
                if (data != 0) {
                    final double v = ((data & 0xFF) - 128) / 127.0 * vNyq * coef[multiprf];
                    temp = (data & 0xFF) / 256.0 * v;
                    break;
                }
                break;
            }
            case 5: {
                if (data != 0) {
                    temp = ((data & 0xFF) - 128) / 16.0;
                    break;
                }
                break;
            }
        }
        final BigDecimal bd = new BigDecimal(temp);
        final BigDecimal result = bd.setScale(2, RoundingMode.HALF_DOWN);
        return result.floatValue();
    }
    
    static String calcTime(final int t, final int t0) {
        final StringBuffer tim = new StringBuffer();
        final int[] tt = new int[3];
        final int mmh = (t + t0) / 60;
        tt[2] = (t + t0) % 60;
        tt[0] = mmh / 60;
        tt[1] = mmh % 60;
        for (int i = 0; i < 3; ++i) {
            String s = Integer.toString(tt[i]);
            final int len = s.length();
            if (len < 2) {
                s = "0" + tt[i];
            }
            if (i != 2) {
                s += ":";
            }
            tim.append(s);
        }
        return tim.toString();
    }
    
    static float calcNyquist(final int prf, final int wave) {
        double tmp = prf * wave * 0.01 * 0.25;
        tmp *= 0.01;
        final BigDecimal bd = new BigDecimal(tmp);
        final BigDecimal result = bd.setScale(2, RoundingMode.HALF_DOWN);
        return result.floatValue();
    }
    
    public Array readNestedData(final Variable v2, final List section) throws IOException, InvalidRangeException {
        return null;
    }
    
    @Override
    public void close() throws IOException {
        this.myRaf.close();
    }
    
    public void setSpecial(final Object special) {
    }
    
    @Override
    public String toStringDebug(final Object o) {
        return null;
    }
    
    @Override
    public String getDetailInfo() {
        return null;
    }
    
    static {
        SigmetIOServiceProvider.recHdr = new HashMap<String, Number>();
    }
}
