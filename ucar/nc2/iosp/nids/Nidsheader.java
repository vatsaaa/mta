// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.iosp.nids;

import org.slf4j.LoggerFactory;
import java.util.zip.DataFormatException;
import java.util.zip.Inflater;
import java.nio.LongBuffer;
import java.nio.DoubleBuffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.nio.ShortBuffer;
import java.nio.ByteOrder;
import ucar.nc2.iosp.IospHelper;
import ucar.unidata.io.bzip2.BZip2ReadException;
import java.io.InputStream;
import java.io.ByteArrayInputStream;
import ucar.unidata.io.bzip2.CBZip2InputStream;
import java.util.Date;
import ucar.nc2.constants.AxisType;
import ucar.unidata.geoloc.ProjectionImpl;
import ucar.unidata.util.Parameter;
import ucar.unidata.geoloc.projection.FlatEarth;
import ucar.ma2.Array;
import ucar.nc2.constants.FeatureType;
import ucar.nc2.Attribute;
import ucar.ma2.DataType;
import java.util.List;
import ucar.nc2.Structure;
import ucar.nc2.Variable;
import ucar.nc2.Group;
import ucar.nc2.Dimension;
import java.util.ArrayList;
import java.nio.ByteBuffer;
import ucar.nc2.iosp.nexrad2.NexradStationDB;
import java.io.IOException;
import ucar.nc2.NetcdfFile;
import ucar.unidata.io.RandomAccessFile;
import ucar.nc2.units.DateFormatter;
import org.slf4j.Logger;

class Nidsheader
{
    private static final boolean useStationDB = false;
    private static Logger log;
    static final int NEXR_PID_READ = 100;
    static final int DEF_NUM_ELEMS = 640;
    static final int DEF_NUM_LINES = 480;
    static final int NEXR_FILE_READ = -1;
    static final int NEXR_DIR_READ = 356;
    static final int READ_BUFFER_SIZE = 1;
    static final int ZLIB_BUF_LEN = 4000;
    byte Z_DEFLATED;
    byte DEF_WBITS;
    static final int Other = 0;
    static final int Base_Reflect = 1;
    static final int Velocity = 2;
    static final int Comp_Reflect = 3;
    static final int Layer_Reflect_Avg = 4;
    static final int Layer_Reflect_Max = 5;
    static final int Echo_Tops = 6;
    static final int Vert_Liquid = 7;
    static final int Precip_1 = 8;
    static final int Precip_3 = 9;
    static final int Precip_Accum = 10;
    static final int Precip_Array = 11;
    static final int BaseReflect248 = 12;
    static final int StrmRelMeanVel = 13;
    static final int VAD = 14;
    static final int SPECTRUM = 15;
    static final int DigitalHybridReflect = 16;
    static final int DigitalStormTotalPrecip = 17;
    static final int Reflect1 = 18;
    static final int Velocity1 = 19;
    static final int SPECTRUM1 = 20;
    static final int BaseReflectivityDR = 21;
    static final int BaseVelocityDV = 22;
    static final int EnhancedEcho_Tops = 23;
    static final int DigitalVert_Liquid = 24;
    short mcode;
    short mdate;
    int mtime;
    int mlength;
    short msource;
    short mdestId;
    short mNumOfBlock;
    short divider;
    double latitude;
    double lat_min;
    double lat_max;
    double longitude;
    double lon_min;
    double lon_max;
    double height;
    short pcode;
    short opmode;
    short volumnScanPattern;
    short sequenceNumber;
    short volumeScanNumber;
    short volumeScanDate;
    int volumeScanTime;
    short productDate;
    int productTime;
    short p1;
    short p2;
    short elevationNumber;
    short p3;
    short[] threshold;
    short p4;
    short p5;
    short p6;
    short p7;
    short p8;
    short p9;
    short p10;
    short numberOfMaps;
    int offsetToSymbologyBlock;
    int offsetToGraphicBlock;
    int offsetToTabularBlock;
    int block_length;
    short number_layers;
    String stationId;
    String stationName;
    private boolean noHeader;
    DateFormatter formatter;
    private RandomAccessFile raf;
    private NetcdfFile ncfile;
    private String cmemo;
    private String ctilt;
    private String ctitle;
    private String cunit;
    private String cname;
    private int numX;
    private int numX0;
    private int numY;
    private int numY0;
    private boolean isR;
    private byte[] uncompdata;
    
    Nidsheader() {
        this.Z_DEFLATED = 8;
        this.DEF_WBITS = 15;
        this.mcode = 0;
        this.mdate = 0;
        this.mtime = 0;
        this.mlength = 0;
        this.msource = 0;
        this.mdestId = 0;
        this.mNumOfBlock = 0;
        this.divider = 0;
        this.latitude = 0.0;
        this.lat_min = 0.0;
        this.lat_max = 0.0;
        this.longitude = 0.0;
        this.lon_min = 0.0;
        this.lon_max = 0.0;
        this.height = 0.0;
        this.pcode = 0;
        this.opmode = 0;
        this.volumnScanPattern = 0;
        this.sequenceNumber = 0;
        this.volumeScanNumber = 0;
        this.volumeScanDate = 0;
        this.volumeScanTime = 0;
        this.productDate = 0;
        this.productTime = 0;
        this.p1 = 0;
        this.p2 = 0;
        this.elevationNumber = 0;
        this.p3 = 0;
        this.threshold = new short[16];
        this.p4 = 0;
        this.p5 = 0;
        this.p6 = 0;
        this.p7 = 0;
        this.p8 = 0;
        this.p9 = 0;
        this.p10 = 0;
        this.numberOfMaps = 0;
        this.offsetToSymbologyBlock = 0;
        this.offsetToGraphicBlock = 0;
        this.offsetToTabularBlock = 0;
        this.block_length = 0;
        this.number_layers = 0;
        this.stationName = "XXX";
        this.formatter = new DateFormatter();
        this.isR = false;
        this.uncompdata = null;
    }
    
    public boolean isValidFile(final RandomAccessFile raf) {
        try {
            final long t = raf.length();
            if (t == 0L) {
                throw new IOException("zero length file ");
            }
        }
        catch (IOException e) {
            return false;
        }
        try {
            final int p = this.readWMO(raf);
            if (p == 0) {
                return false;
            }
        }
        catch (IOException e) {
            return false;
        }
        return true;
    }
    
    int readWMO(final RandomAccessFile raf) throws IOException {
        final int pos = 0;
        raf.seek(pos);
        final int readLen = 35;
        int rc = 0;
        final byte[] b = new byte[readLen];
        rc = raf.read(b);
        if (rc != readLen) {
            return 0;
        }
        final int iarr2_1 = bytesToInt(b[0], b[1], false);
        final int iarr2_2 = bytesToInt(b[30], b[31], false);
        final int iarr2_3 = bytesToInt(b[18], b[19], false);
        final int iarr2_4 = bytesToInt(b[12], b[13], false);
        if (iarr2_1 == iarr2_2 && iarr2_1 >= 16 && iarr2_1 <= 299 && iarr2_3 == -1 && iarr2_4 < 10000) {
            this.noHeader = true;
            return 1;
        }
        final String pib = new String(b);
        if (pib.indexOf("SDUS") != -1) {
            this.noHeader = false;
            return 1;
        }
        if (raf.getLocation().indexOf(".nids") != -1) {
            this.noHeader = true;
            return 1;
        }
        return 0;
    }
    
    public byte[] getUncompData(final int offset, int len) {
        if (len == 0) {
            len = this.uncompdata.length - offset;
        }
        final byte[] data = new byte[len];
        System.arraycopy(this.uncompdata, offset, data, 0, len);
        return data;
    }
    
    public void setProperty(final String name, final String value) {
    }
    
    void read(final RandomAccessFile raf, final NetcdfFile ncfile) throws IOException {
        int hoff = 0;
        boolean isZ = false;
        final int p = this.readWMO(raf);
        this.ncfile = ncfile;
        final long actualSize = raf.length();
        int pos = 0;
        raf.seek(pos);
        final int readLen = (int)actualSize;
        final byte[] b = new byte[readLen];
        int rc = raf.read(b);
        if (rc != readLen) {
            Nidsheader.log.warn(" error reading nids product header " + raf.getLocation());
        }
        if (!this.noHeader) {
            final String pib = new String(b, 0, 100);
            int type = 0;
            for (pos = pib.indexOf("\r\r\n"); pos != -1; pos = pib.indexOf("\r\r\n", pos + 1)) {
                hoff = pos + 3;
                ++type;
            }
            raf.seek(hoff);
            final byte[] b2 = new byte[2];
            System.arraycopy(b, hoff, b2, 0, 2);
            final int zlibed = this.isZlibHed(b2);
            if (zlibed == 0) {
                final int encrypt = this.IsEncrypt(b2);
                if (encrypt == 1) {
                    Nidsheader.log.error("error reading encryted product " + raf.getLocation());
                    throw new IOException("unable to handle the product with encrypt code " + encrypt);
                }
            }
            final byte[] b3 = new byte[3];
            switch (type) {
                case 0: {
                    Nidsheader.log.warn("ReadNexrInfo:: Unable to seek to ID " + raf.getLocation());
                    break;
                }
                case 1:
                case 2:
                case 3:
                case 4: {
                    System.arraycopy(b, hoff - 6, b3, 0, 3);
                    this.stationId = new String(b3);
                    try {
                        NexradStationDB.init();
                        final NexradStationDB.Station station = NexradStationDB.get("K" + this.stationId);
                        if (station != null) {
                            this.stationName = station.name;
                        }
                    }
                    catch (IOException ioe) {
                        Nidsheader.log.error("NexradStationDB.init " + raf.getLocation(), ioe);
                    }
                    break;
                }
            }
            if (zlibed == 1) {
                isZ = true;
                this.uncompdata = this.GetZlibedNexr(b, readLen, hoff);
                if (this.uncompdata == null) {
                    Nidsheader.log.warn("ReadNexrInfo: error uncompressing image " + raf.getLocation());
                }
            }
            else {
                System.arraycopy(b, hoff, this.uncompdata = new byte[b.length - hoff], 0, b.length - hoff);
            }
        }
        else {
            System.arraycopy(b, 0, this.uncompdata = new byte[b.length], 0, b.length);
        }
        final byte[] b4 = new byte[2];
        ByteBuffer bos = ByteBuffer.wrap(this.uncompdata);
        rc = this.read_msghead(bos, 0);
        int hedsiz = 18;
        final Pinfo pinfo = this.read_proddesc(bos, hedsiz);
        bos.position();
        hedsiz += 102;
        final int prod_type = code_typelookup(pinfo.pcode);
        this.setProductInfo(prod_type, pinfo);
        int pcode1Number = 0;
        int pcode2Number = 0;
        int pcode8Number = 0;
        int pcode4Number = 0;
        int pcode5Number = 0;
        int pcode10Number = 0;
        int pcode6Number = 0;
        int pcode25Number = 0;
        int pcode12Number = 0;
        int pcode13Number = 0;
        int pcode14Number = 0;
        int pcode15Number = 0;
        int pcode16Number = 0;
        int pcode19Number = 0;
        int pcode20Number = 0;
        int[] pkcode1Doff = null;
        int[] pkcode2Doff = null;
        int[] pkcode8Doff = null;
        int[] pkcode1Size = null;
        int[] pkcode2Size = null;
        int[] pkcode8Size = null;
        int[] pkcode4Doff = null;
        int[] pkcode5Doff = null;
        int[] pkcode10Doff = null;
        int[] pkcode10Dlen = null;
        int[] pkcode6Doff = null;
        int[] pkcode6Dlen = null;
        int[] pkcode25Doff = null;
        int[] pkcode12Doff = null;
        int[] pkcode13Doff = null;
        int[] pkcode14Doff = null;
        int[] pkcode12Dlen = null;
        int[] pkcode13Dlen = null;
        int[] pkcode14Dlen = null;
        int[] pkcode15Dlen = null;
        int[] pkcode15Doff = null;
        int[] pkcode16Dlen = null;
        int[] pkcode16Doff = null;
        int[] pkcode19Dlen = null;
        int[] pkcode19Doff = null;
        int[] pkcode20Dlen = null;
        int[] pkcode20Doff = null;
        if (pinfo.offsetToSymbologyBlock != 0) {
            if (pinfo.p8 == 1) {
                final int size = shortsToInt(pinfo.p9, pinfo.p10, false);
                this.uncompdata = this.uncompressed(bos, hedsiz, size);
                bos = ByteBuffer.wrap(this.uncompdata);
            }
            final Sinfo sinfo = this.read_dividlen(bos, hedsiz);
            if (rc == 0 || pinfo.divider != -1) {
                Nidsheader.log.warn("error in product symbology header " + raf.getLocation());
            }
            if (sinfo.id != 1) {
                if (pinfo.pcode == 82) {
                    this.read_SATab(bos, hedsiz);
                }
            }
            else {
                hedsiz += 10;
                int klayer = pinfo.offsetToSymbologyBlock * 2 + 10;
                for (int i = 0; i < sinfo.nlayers; ++i) {
                    hedsiz = klayer;
                    bos.position(hedsiz);
                    final short Divlen_divider = bos.getShort();
                    hedsiz += 2;
                    final int Divlen_length = bos.getInt();
                    hedsiz += 4;
                    if (Divlen_divider != -1) {
                        Nidsheader.log.warn("error reading length divider " + raf.getLocation());
                    }
                    int plen = 0;
                    for (int icount = 0; icount < Divlen_length; icount = icount + plen + 4) {
                        int boff = klayer + icount + 6;
                        bos.position(boff);
                        bos.get(b4);
                        final int pkcode = this.getUInt(b4, 2);
                        hedsiz += 2;
                        boff += 2;
                        switch (pkcode) {
                            case 17:
                            case 18: {
                                hedsiz += 8;
                                plen = this.pcode_DPA(bos, boff, hoff, hedsiz, isZ, i, pkcode);
                                break;
                            }
                            case 10: {
                                if (pkcode10Doff == null) {
                                    pkcode10Doff = new int[250];
                                    pkcode10Dlen = new int[250];
                                }
                                plen = bos.getShort();
                                pkcode10Doff[pcode10Number] = boff + 2;
                                pkcode10Dlen[pcode10Number] = (plen - 2) / 8;
                                ++pcode10Number;
                                break;
                            }
                            case 1: {
                                if (pkcode1Doff == null) {
                                    pkcode1Doff = new int[250];
                                    pkcode1Size = new int[250];
                                }
                                plen = bos.getShort();
                                pkcode1Doff[pcode1Number] = boff + 2;
                                pkcode1Size[pcode1Number] = plen - 4;
                                ++pcode1Number;
                                break;
                            }
                            case 2: {
                                if (pkcode2Doff == null) {
                                    pkcode2Doff = new int[250];
                                    pkcode2Size = new int[250];
                                }
                                plen = bos.getShort();
                                pkcode2Doff[pcode2Number] = boff + 2;
                                pkcode2Size[pcode2Number] = plen - 4;
                                ++pcode2Number;
                                break;
                            }
                            case 8: {
                                if (pkcode8Doff == null) {
                                    pkcode8Doff = new int[550];
                                    pkcode8Size = new int[550];
                                }
                                plen = bos.getShort();
                                pkcode8Doff[pcode8Number] = boff + 2;
                                pkcode8Size[pcode8Number] = plen - 6;
                                ++pcode8Number;
                                break;
                            }
                            case 3:
                            case 11:
                            case 25: {
                                if (pkcode25Doff == null) {
                                    pkcode25Doff = new int[250];
                                }
                                plen = bos.getShort();
                                pkcode25Doff[pcode25Number] = boff + 2;
                                ++pcode25Number;
                                break;
                            }
                            case 12: {
                                if (pkcode12Doff == null) {
                                    pkcode12Doff = new int[250];
                                    pkcode12Dlen = new int[250];
                                }
                                plen = bos.getShort();
                                pkcode12Doff[pcode12Number] = boff + 2;
                                pkcode12Dlen[pcode12Number] = plen / 4;
                                ++pcode12Number;
                                break;
                            }
                            case 13: {
                                if (pkcode13Doff == null) {
                                    pkcode13Doff = new int[250];
                                    pkcode13Dlen = new int[250];
                                }
                                plen = bos.getShort();
                                pkcode13Doff[pcode13Number] = boff + 2;
                                pkcode13Dlen[pcode13Number] = plen / 4;
                                ++pcode13Number;
                                break;
                            }
                            case 14: {
                                if (pkcode14Doff == null) {
                                    pkcode14Doff = new int[250];
                                    pkcode14Dlen = new int[250];
                                }
                                plen = bos.getShort();
                                pkcode14Doff[pcode14Number] = boff + 2;
                                pkcode14Dlen[pcode14Number] = plen / 4;
                                ++pcode14Number;
                                break;
                            }
                            case 15: {
                                if (pkcode15Doff == null) {
                                    pkcode15Doff = new int[250];
                                    pkcode15Dlen = new int[250];
                                }
                                plen = bos.getShort();
                                pkcode15Doff[pcode15Number] = boff + 2;
                                pkcode15Dlen[pcode15Number] = plen / 4;
                                ++pcode15Number;
                                break;
                            }
                            case 166: {
                                if (pkcode16Doff == null) {
                                    pkcode16Doff = new int[250];
                                    pkcode16Dlen = new int[250];
                                }
                                plen = bos.getShort();
                                pkcode16Doff[pcode16Number] = boff + 2;
                                pkcode16Dlen[pcode16Number] = plen / 4;
                                ++pcode16Number;
                                break;
                            }
                            case 19: {
                                if (pkcode19Doff == null) {
                                    pkcode19Doff = new int[250];
                                    pkcode19Dlen = new int[250];
                                }
                                plen = bos.getShort();
                                pkcode19Doff[pcode19Number] = boff + 2;
                                pkcode19Dlen[pcode19Number] = plen / 4;
                                ++pcode19Number;
                                break;
                            }
                            case 20: {
                                if (pkcode20Doff == null) {
                                    pkcode20Doff = new int[250];
                                    pkcode20Dlen = new int[250];
                                }
                                plen = bos.getShort();
                                pkcode20Doff[pcode20Number] = boff + 2;
                                pkcode20Dlen[pcode20Number] = plen / 4;
                                ++pcode20Number;
                                break;
                            }
                            case 4: {
                                if (pkcode4Doff == null) {
                                    pkcode4Doff = new int[1000];
                                }
                                plen = bos.getShort();
                                pkcode4Doff[pcode4Number] = boff + 2;
                                ++pcode4Number;
                                break;
                            }
                            case 5: {
                                if (pkcode5Doff == null) {
                                    pkcode5Doff = new int[1000];
                                }
                                plen = bos.getShort();
                                pkcode5Doff[pcode5Number] = boff + 2;
                                ++pcode5Number;
                                break;
                            }
                            case 43: {
                                plen = bos.getShort();
                                break;
                            }
                            case 23:
                            case 24: {
                                plen = bos.getShort();
                                int len;
                                for (int poff = 2; poff < plen; poff = poff + len + 4) {
                                    final int pcode = bos.getShort();
                                    len = bos.getShort();
                                    switch (pcode) {
                                        case 2: {
                                            if (pkcode2Doff == null) {
                                                pkcode2Doff = new int[250];
                                                pkcode2Size = new int[250];
                                            }
                                            pkcode2Doff[pcode2Number] = boff + poff + 4;
                                            pkcode2Size[pcode2Number] = len - 4;
                                            ++pcode2Number;
                                            break;
                                        }
                                        case 6: {
                                            if (pkcode6Doff == null) {
                                                pkcode6Doff = new int[250];
                                                pkcode6Dlen = new int[250];
                                            }
                                            pkcode6Doff[pcode6Number] = boff + poff + 4;
                                            pkcode6Dlen[pcode6Number] = (len - 6) / 4;
                                            ++pcode6Number;
                                            break;
                                        }
                                        case 25: {
                                            if (pkcode25Doff == null) {
                                                pkcode25Doff = new int[250];
                                            }
                                            pkcode25Doff[pcode25Number] = boff + poff + 4;
                                            ++pcode25Number;
                                            break;
                                        }
                                        default: {
                                            Nidsheader.log.error("error reading pcode= " + pcode + " " + raf.getLocation());
                                            throw new IOException("error reading pcode, unable to handle the product with code " + pcode);
                                        }
                                    }
                                }
                                break;
                            }
                            default: {
                                if (pkcode == 44831 || pkcode == 16) {
                                    hedsiz += this.pcode_radial(bos, hoff, hedsiz, isZ, this.uncompdata, pinfo.threshold);
                                    plen = Divlen_length;
                                    break;
                                }
                                if (pkcode == 47631 || pkcode == 47623) {
                                    hedsiz += this.pcode_raster(bos, (short)pkcode, hoff, hedsiz, isZ, this.uncompdata);
                                    plen = Divlen_length;
                                    break;
                                }
                                Nidsheader.log.error("error reading pkcode equals " + pkcode + " " + raf.getLocation());
                                throw new IOException("error reading pkcode, unable to handle the product with code " + pkcode);
                            }
                        }
                    }
                    klayer = klayer + Divlen_length + 6;
                }
                if (pkcode8Doff != null) {
                    this.pcode_128(pkcode8Doff, pkcode8Size, 8, hoff, pcode8Number, "textStruct_code8", "", isZ);
                }
                if (pkcode1Doff != null) {
                    this.pcode_128(pkcode1Doff, pkcode1Size, 1, hoff, pcode1Number, "textStruct_code1", "", isZ);
                }
                if (pkcode2Doff != null) {
                    this.pcode_128(pkcode2Doff, pkcode2Size, 2, hoff, pcode2Number, "textStruct_code2", "", isZ);
                }
                if (pkcode10Doff != null) {
                    this.pcode_10n9(pkcode10Doff, pkcode10Dlen, hoff, pcode10Number, isZ);
                }
                if (pkcode4Doff != null) {
                    this.pcode_4(pkcode4Doff, hoff, pcode4Number, isZ);
                }
                if (pkcode5Doff != null) {
                    this.pcode_5(pkcode5Doff, hoff, pcode5Number, isZ);
                }
                if (pkcode6Doff != null) {
                    this.pcode_6n7(pkcode6Doff, pkcode6Dlen, hoff, pcode6Number, isZ, "linkedVector", 6);
                }
                if (pkcode25Doff != null) {
                    this.pcode_25(pkcode25Doff, hoff, pcode25Number, isZ);
                }
                if (pkcode12Doff != null) {
                    this.pcode_12n13n14(pkcode12Doff, pkcode12Dlen, hoff, pcode12Number, isZ, "TVS", 12);
                }
                if (pkcode13Doff != null) {
                    this.pcode_12n13n14(pkcode13Doff, pkcode13Dlen, hoff, pcode13Number, isZ, "hailPositive", 13);
                }
                if (pkcode14Doff != null) {
                    this.pcode_12n13n14(pkcode14Doff, pkcode14Dlen, hoff, pcode14Number, isZ, "hailProbable", 14);
                }
                if (pkcode19Doff != null) {
                    this.pcode_12n13n14(pkcode19Doff, pkcode19Dlen, hoff, pcode19Number, isZ, "hailIndex", 19);
                }
                if (pkcode20Doff != null) {
                    this.pcode_12n13n14(pkcode20Doff, pkcode20Dlen, hoff, pcode20Number, isZ, "mesocyclone", 20);
                }
            }
        }
        else {
            Nidsheader.log.warn("GetNexrDirs:: no product symbology block found (no image data) " + raf.getLocation());
        }
        if (pinfo.offsetToTabularBlock != 0) {
            final int tlayer = pinfo.offsetToTabularBlock * 2;
            bos.position(tlayer);
            if (bos.hasRemaining()) {
                short tab_divider = bos.getShort();
                if (tab_divider != -1) {
                    Nidsheader.log.error("Block divider not found " + raf.getLocation());
                    throw new IOException("error reading graphic alphanumeric block");
                }
                final short tab_bid = bos.getShort();
                final int tblen = bos.getInt();
                bos.position(tlayer + 116);
                final int inc = bos.getInt();
                bos.position(tlayer + 128);
                tab_divider = bos.getShort();
                if (tab_divider != -1) {
                    Nidsheader.log.error("tab divider not found " + raf.getLocation());
                    throw new IOException("error reading graphic alphanumeric block");
                }
                final int npage = bos.getShort();
                final int ppos = bos.position();
                final ArrayList dims = new ArrayList();
                final Dimension tbDim = new Dimension("pageNumber", npage);
                ncfile.addDimension(null, tbDim);
                dims.add(tbDim);
                final Variable ppage = new Variable(ncfile, null, null, "TabMessagePage");
                ppage.setDimensions(dims);
                ppage.setDataType(DataType.STRING);
                ppage.addAttribute(new Attribute("long_name", "Graphic Product Message"));
                ncfile.addVariable(null, ppage);
                ppage.setSPobject(new Vinfo(npage, 0, tblen, 0, hoff, ppos, this.isR, isZ, null, null, tab_bid, 0));
            }
        }
        if (pinfo.offsetToGraphicBlock != 0) {
            int[] gpkcode1Doff = null;
            final int[] gpkcode2Doff = null;
            int[] gpkcode10Doff = null;
            int[] gpkcode10Dlen = null;
            int[] gpkcode8Doff = null;
            int[] gpkcode1Size = null;
            final int[] gpkcode2Size = null;
            int[] gpkcode8Size = null;
            int gpcode1Number = 0;
            int gpcode10Number = 0;
            int gpcode8Number = 0;
            final int gpcode2Number = 0;
            final int tlayer2 = pinfo.offsetToGraphicBlock * 2;
            bos.position(tlayer2);
            final short graphic_divider = bos.getShort();
            final short graphic_bid = bos.getShort();
            if (graphic_divider != -1 || graphic_bid != 2) {
                Nidsheader.log.error("error reading graphic alphanumeric block " + raf.getLocation());
                throw new IOException("error reading graphic alphanumeric block");
            }
            int lpage;
            for (int blen = bos.getInt(), clen = 0, npage2 = bos.getShort(), ipage = 0; clen < blen && ipage < npage2; clen = clen + lpage + 4) {
                int ppos2 = bos.position();
                ipage = bos.getShort();
                lpage = bos.getShort();
                int icnt = 0;
                ppos2 += 4;
                while (icnt < lpage) {
                    bos.position(ppos2 + icnt);
                    final int pkcode2 = bos.getShort();
                    if (pkcode2 == 8) {
                        if (gpkcode8Doff == null) {
                            gpkcode8Doff = new int[550];
                            gpkcode8Size = new int[550];
                        }
                        final int plen2 = bos.getShort();
                        gpkcode8Doff[gpcode8Number] = ppos2 + 4 + icnt;
                        gpkcode8Doff[gpcode8Number] = plen2 - 6;
                        icnt += plen2 + 4;
                        ++gpcode8Number;
                    }
                    else if (pkcode2 == 1) {
                        if (gpkcode1Doff == null) {
                            gpkcode1Doff = new int[550];
                            gpkcode1Size = new int[550];
                        }
                        final int plen2 = bos.getShort();
                        gpkcode1Doff[gpcode1Number] = ppos2 + 4 + icnt;
                        gpkcode1Size[gpcode1Number] = plen2 - 4;
                        icnt += plen2 + 4;
                        ++gpcode1Number;
                    }
                    else if (pkcode2 == 10) {
                        if (gpkcode10Doff == null) {
                            gpkcode10Doff = new int[250];
                            gpkcode10Dlen = new int[250];
                        }
                        final int plen2 = bos.getShort();
                        gpkcode10Doff[gpcode10Number] = ppos2 + 4 + icnt;
                        gpkcode10Dlen[gpcode10Number] = (plen2 - 2) / 8;
                        icnt += plen2 + 4;
                        ++gpcode10Number;
                    }
                    else {
                        final int plen2 = bos.getShort();
                        icnt += plen2 + 4;
                    }
                }
                ppos2 = ppos2 + lpage + 4;
            }
            if (gpkcode8Doff != null) {
                this.pcode_128(gpkcode8Doff, gpkcode8Size, 8, hoff, gpcode8Number, "textStruct_code8g", "g", isZ);
            }
            if (gpkcode2Doff != null) {
                this.pcode_128(gpkcode2Doff, gpkcode2Size, 2, hoff, gpcode8Number, "textStruct_code2g", "g", isZ);
            }
            if (gpkcode1Doff != null) {
                this.pcode_128(gpkcode1Doff, gpkcode1Size, 1, hoff, gpcode1Number, "textStruct_code1g", "g", isZ);
            }
            if (gpkcode10Doff != null) {
                this.pcode_10n9(gpkcode10Doff, gpkcode10Dlen, hoff, gpcode10Number, isZ);
            }
        }
        ncfile.finish();
    }
    
    int pcode_12n13n14(final int[] pos, final int[] dlen, final int hoff, final int len, final boolean isZ, final String structName, final int code) {
        int vlen = 0;
        for (int i = 0; i < len; ++i) {
            vlen += dlen[i];
        }
        final ArrayList dims = new ArrayList();
        final Dimension sDim = new Dimension("graphicSymbolSize", vlen);
        this.ncfile.addDimension(null, sDim);
        dims.add(sDim);
        final Structure dist = new Structure(this.ncfile, null, null, structName);
        dist.setDimensions(dims);
        this.ncfile.addVariable(null, dist);
        dist.addAttribute(new Attribute("long_name", "special graphic symbol for code " + code));
        final Variable i2 = new Variable(this.ncfile, null, dist, "x_start");
        i2.setDimensions((String)null);
        i2.setDataType(DataType.SHORT);
        i2.addAttribute(new Attribute("units", "KM"));
        dist.addMemberVariable(i2);
        final Variable j0 = new Variable(this.ncfile, null, dist, "y_start");
        j0.setDimensions((String)null);
        j0.setDataType(DataType.SHORT);
        j0.addAttribute(new Attribute("units", "KM"));
        dist.addMemberVariable(j0);
        final int[] pos2 = new int[len];
        final int[] dlen2 = new int[len];
        System.arraycopy(dlen, 0, dlen2, 0, len);
        System.arraycopy(pos, 0, pos2, 0, len);
        dist.setSPobject(new Vinfo(0, 0, 0, 0, hoff, 0L, this.isR, isZ, pos2, dlen2, code, 0));
        return 1;
    }
    
    int pcode_25(final int[] pos, final int hoff, final int len, final boolean isZ) {
        final ArrayList dims = new ArrayList();
        final Dimension sDim = new Dimension("circleSize", len);
        this.ncfile.addDimension(null, sDim);
        dims.add(sDim);
        final Structure dist = new Structure(this.ncfile, null, null, "circleStruct");
        dist.setDimensions(dims);
        this.ncfile.addVariable(null, dist);
        dist.addAttribute(new Attribute("long_name", "Circle Packet"));
        final Variable ii0 = new Variable(this.ncfile, null, dist, "x_center");
        ii0.setDimensions((String)null);
        ii0.setDataType(DataType.SHORT);
        dist.addMemberVariable(ii0);
        final Variable ii2 = new Variable(this.ncfile, null, dist, "y_center");
        ii2.setDimensions((String)null);
        ii2.setDataType(DataType.SHORT);
        dist.addMemberVariable(ii2);
        final Variable jj0 = new Variable(this.ncfile, null, dist, "radius");
        jj0.setDimensions((String)null);
        jj0.setDataType(DataType.SHORT);
        dist.addMemberVariable(jj0);
        final int[] pos2 = new int[len];
        System.arraycopy(pos, 0, pos2, 0, len);
        dist.setSPobject(new Vinfo(0, 0, 0, 0, hoff, 0L, this.isR, isZ, pos2, null, 25, 0));
        return 1;
    }
    
    int pcode_6n7(final int[] pos, final int[] dlen, final int hoff, final int len, final boolean isZ, final String vname, final int code) {
        final ArrayList dims = new ArrayList();
        int vlen = 0;
        for (int i = 0; i < len; ++i) {
            vlen += dlen[i];
        }
        final Dimension sDim = new Dimension(vname + "Size", vlen);
        this.ncfile.addDimension(null, sDim);
        dims.add(sDim);
        final Structure dist = new Structure(this.ncfile, null, null, vname + "Struct");
        dist.setDimensions(dims);
        this.ncfile.addVariable(null, dist);
        dist.addAttribute(new Attribute("long_name", vname + " Packet"));
        final Variable ii0 = new Variable(this.ncfile, null, dist, "x_start");
        ii0.setDimensions((String)null);
        ii0.setDataType(DataType.SHORT);
        dist.addMemberVariable(ii0);
        final Variable ii2 = new Variable(this.ncfile, null, dist, "y_start");
        ii2.setDimensions((String)null);
        ii2.setDataType(DataType.SHORT);
        dist.addMemberVariable(ii2);
        final Variable jj0 = new Variable(this.ncfile, null, dist, "x_end");
        jj0.setDimensions((String)null);
        jj0.setDataType(DataType.SHORT);
        dist.addMemberVariable(jj0);
        final Variable jj2 = new Variable(this.ncfile, null, dist, "y_end");
        jj2.setDimensions((String)null);
        jj2.setDataType(DataType.SHORT);
        dist.addMemberVariable(jj2);
        final int[] pos2 = new int[len];
        final int[] dlen2 = new int[len];
        System.arraycopy(pos, 0, pos2, 0, len);
        System.arraycopy(dlen, 0, dlen2, 0, len);
        dist.setSPobject(new Vinfo(0, 0, 0, 0, hoff, 0L, this.isR, isZ, pos2, dlen2, code, 0));
        return 1;
    }
    
    int pcode_4(final int[] pos, final int hoff, final int len, final boolean isZ) {
        final ArrayList dims = new ArrayList();
        final Dimension sDim = new Dimension("windBarbSize", len);
        this.ncfile.addDimension(null, sDim);
        dims.add(sDim);
        final Structure dist = new Structure(this.ncfile, null, null, this.cname);
        dist.setDimensions(dims);
        this.ncfile.addVariable(null, dist);
        dist.addAttribute(new Attribute("long_name", "Wind Barb Data"));
        final Variable value = new Variable(this.ncfile, null, dist, "value");
        value.setDimensions((String)null);
        value.setDataType(DataType.SHORT);
        value.addAttribute(new Attribute("units", "RMS"));
        dist.addMemberVariable(value);
        final Variable i0 = new Variable(this.ncfile, null, dist, "x_start");
        i0.setDimensions((String)null);
        i0.setDataType(DataType.SHORT);
        i0.addAttribute(new Attribute("units", "KM"));
        dist.addMemberVariable(i0);
        final Variable j0 = new Variable(this.ncfile, null, dist, "y_start");
        j0.setDimensions((String)null);
        j0.setDataType(DataType.SHORT);
        j0.addAttribute(new Attribute("units", "KM"));
        dist.addMemberVariable(j0);
        final Variable direct = new Variable(this.ncfile, null, dist, "direction");
        direct.setDimensions((String)null);
        direct.setDataType(DataType.SHORT);
        direct.addAttribute(new Attribute("units", "degree"));
        dist.addMemberVariable(direct);
        final Variable speed = new Variable(this.ncfile, null, dist, "speed");
        speed.setDimensions((String)null);
        speed.setDataType(DataType.SHORT);
        speed.addAttribute(new Attribute("units", "knots"));
        dist.addMemberVariable(speed);
        final int[] pos2 = new int[len];
        System.arraycopy(pos, 0, pos2, 0, len);
        dist.setSPobject(new Vinfo(0, 0, 0, 0, hoff, 0L, this.isR, isZ, pos2, null, 4, 0));
        return 1;
    }
    
    int checkMsgHeader(final RandomAccessFile raf) throws IOException {
        final long actualSize = raf.length();
        final int pos = 0;
        raf.seek(pos);
        final int readLen = (int)actualSize;
        final byte[] b = new byte[readLen];
        final int rc = raf.read(b);
        if (rc != readLen) {
            Nidsheader.log.warn(" error reading nids product header " + raf.getLocation());
        }
        final ByteBuffer bos = ByteBuffer.wrap(b);
        return this.read_msghead(bos, 0);
    }
    
    int pcode_5(final int[] pos, final int hoff, final int len, final boolean isZ) {
        final ArrayList dims = new ArrayList();
        final Dimension sDim = new Dimension("windBarbSize", len);
        this.ncfile.addDimension(null, sDim);
        dims.add(sDim);
        final Structure dist = new Structure(this.ncfile, null, null, "vectorArrow");
        dist.setDimensions(dims);
        this.ncfile.addVariable(null, dist);
        dist.addAttribute(new Attribute("long_name", "Vector Arrow Data"));
        final Variable i0 = new Variable(this.ncfile, null, dist, "x_start");
        i0.setDimensions((String)null);
        i0.setDataType(DataType.SHORT);
        i0.addAttribute(new Attribute("units", "KM"));
        dist.addMemberVariable(i0);
        final Variable j0 = new Variable(this.ncfile, null, dist, "y_start");
        j0.setDimensions((String)null);
        j0.setDataType(DataType.SHORT);
        j0.addAttribute(new Attribute("units", "KM"));
        dist.addMemberVariable(j0);
        final Variable direct = new Variable(this.ncfile, null, dist, "direction");
        direct.setDimensions((String)null);
        direct.setDataType(DataType.SHORT);
        direct.addAttribute(new Attribute("units", "degree"));
        dist.addMemberVariable(direct);
        final Variable speed = new Variable(this.ncfile, null, dist, "arrowLength");
        speed.setDimensions((String)null);
        speed.setDataType(DataType.SHORT);
        speed.addAttribute(new Attribute("units", "pixels"));
        dist.addMemberVariable(speed);
        final Variable speed2 = new Variable(this.ncfile, null, dist, "arrowHeadLength");
        speed2.setDimensions((String)null);
        speed2.setDataType(DataType.SHORT);
        speed2.addAttribute(new Attribute("units", "pixels"));
        dist.addMemberVariable(speed2);
        final int[] pos2 = new int[len];
        System.arraycopy(pos, 0, pos2, 0, len);
        dist.setSPobject(new Vinfo(0, 0, 0, 0, hoff, 0L, this.isR, isZ, pos2, null, 4, 0));
        return 1;
    }
    
    int pcode_128(final int[] pos, final int[] size, final int code, final int hoff, final int len, final String structName, final String abbre, final boolean isZ) {
        final ArrayList dims = new ArrayList();
        final Dimension sDim = new Dimension("textStringSize" + abbre + code, len);
        this.ncfile.addDimension(null, sDim);
        dims.add(sDim);
        final Structure dist = new Structure(this.ncfile, null, null, structName + abbre);
        dist.setDimensions(dims);
        this.ncfile.addVariable(null, dist);
        dist.addAttribute(new Attribute("long_name", "text and special symbol for code " + code));
        if (code == 8) {
            final Variable strVal = new Variable(this.ncfile, null, dist, "strValue");
            strVal.setDimensions((String)null);
            strVal.setDataType(DataType.SHORT);
            strVal.addAttribute(new Attribute("units", ""));
            dist.addMemberVariable(strVal);
        }
        final Variable i0 = new Variable(this.ncfile, null, dist, "x_start");
        i0.setDimensions((String)null);
        i0.setDataType(DataType.SHORT);
        i0.addAttribute(new Attribute("units", "KM"));
        dist.addMemberVariable(i0);
        final Variable j0 = new Variable(this.ncfile, null, dist, "y_start");
        j0.setDimensions((String)null);
        j0.setDataType(DataType.SHORT);
        j0.addAttribute(new Attribute("units", "KM"));
        dist.addMemberVariable(j0);
        final Variable tstr = new Variable(this.ncfile, null, dist, "textString");
        tstr.setDimensions((String)null);
        tstr.setDataType(DataType.STRING);
        tstr.addAttribute(new Attribute("units", ""));
        dist.addMemberVariable(tstr);
        final int[] pos2 = new int[len];
        System.arraycopy(pos, 0, pos2, 0, len);
        dist.setSPobject(new Vinfo(0, 0, 0, 0, hoff, 0L, this.isR, isZ, pos2, size, code, 0));
        return 1;
    }
    
    int pcode_10n9(final int[] pos, final int[] dlen, final int hoff, final int len, final boolean isZ) {
        final ArrayList dims = new ArrayList();
        int vlen = 0;
        for (int i = 0; i < len; ++i) {
            vlen += dlen[i];
        }
        final Dimension sDim = new Dimension("unlinkedVectorSize", vlen);
        this.ncfile.addDimension(null, sDim);
        dims.add(sDim);
        final Structure dist = new Structure(this.ncfile, null, null, "unlinkedVectorStruct");
        dist.setDimensions(dims);
        this.ncfile.addVariable(null, dist);
        dist.addAttribute(new Attribute("long_name", "Unlinked Vector Packet"));
        final Variable v = new Variable(this.ncfile, null, null, "iValue");
        v.setDataType(DataType.SHORT);
        v.setDimensions((String)null);
        dist.addMemberVariable(v);
        final Variable ii0 = new Variable(this.ncfile, null, dist, "x_start");
        ii0.setDimensions((String)null);
        ii0.setDataType(DataType.SHORT);
        dist.addMemberVariable(ii0);
        final Variable ii2 = new Variable(this.ncfile, null, dist, "y_start");
        ii2.setDimensions((String)null);
        ii2.setDataType(DataType.SHORT);
        dist.addMemberVariable(ii2);
        final Variable jj0 = new Variable(this.ncfile, null, dist, "x_end");
        jj0.setDimensions((String)null);
        jj0.setDataType(DataType.SHORT);
        dist.addMemberVariable(jj0);
        final Variable jj2 = new Variable(this.ncfile, null, dist, "y_end");
        jj2.setDimensions((String)null);
        jj2.setDataType(DataType.SHORT);
        dist.addMemberVariable(jj2);
        final int[] pos2 = new int[len];
        final int[] dlen2 = new int[len];
        System.arraycopy(pos, 0, pos2, 0, len);
        System.arraycopy(dlen, 0, dlen2, 0, len);
        dist.setSPobject(new Vinfo(0, 0, 0, 0, hoff, 0L, this.isR, isZ, pos2, dlen2, 10, 0));
        return 1;
    }
    
    int pcode_DPA(final ByteBuffer bos, final int pos, final int hoff, final int hedsiz, final boolean isZ, final int slayer, final int code) {
        final byte[] b2 = new byte[2];
        final ArrayList dims = new ArrayList();
        bos.position(pos);
        bos.get(b2, 0, 2);
        bos.get(b2, 0, 2);
        bos.get(b2, 0, 2);
        final short numBox = (short)this.getInt(b2, 2);
        bos.get(b2, 0, 2);
        final short numRow = (short)this.getInt(b2, 2);
        int soff = 8;
        this.numY0 = 0;
        this.numX0 = 0;
        this.numX = numBox;
        this.numY = numRow;
        Dimension jDim = new Dimension("Row", this.numY);
        Dimension iDim = new Dimension("Box", this.numX);
        if (slayer == 0) {
            jDim = new Dimension("y", this.numY);
            iDim = new Dimension("x", this.numX);
            this.ncfile.addDimension(null, iDim);
            this.ncfile.addDimension(null, jDim);
            dims.add(jDim);
            dims.add(iDim);
            final Variable v = new Variable(this.ncfile, null, null, this.cname + "_" + slayer);
            v.setDataType(DataType.SHORT);
            v.setDimensions(dims);
            this.ncfile.addVariable(null, v);
            v.addAttribute(new Attribute("long_name", this.ctitle + " at Symbology Layer " + slayer));
            v.setSPobject(new Vinfo(this.numX, this.numX0, this.numY, this.numY0, hoff, hedsiz, this.isR, isZ, null, null, code, 0));
            v.addAttribute(new Attribute("units", this.cunit));
            v.addAttribute(new Attribute("missing_value", 255));
        }
        for (int row = 0; row < numRow; ++row) {
            final int runLen = bos.getShort();
            final byte[] rdata = new byte[runLen];
            bos.get(rdata, 0, runLen);
            if (runLen < 2) {
                return soff;
            }
            soff += runLen + 2;
        }
        if (slayer == 0) {
            final double ddx = code_reslookup(this.pcode);
            this.ncfile.addAttribute(null, new Attribute("cdm_data_type", FeatureType.GRID.toString()));
            final String coordinates = "x y time latitude longitude altitude";
            final Variable xaxis = new Variable(this.ncfile, null, null, "x");
            xaxis.setDataType(DataType.DOUBLE);
            xaxis.setDimensions("x");
            xaxis.addAttribute(new Attribute("long_name", "projection x coordinate"));
            xaxis.addAttribute(new Attribute("units", "km"));
            xaxis.addAttribute(new Attribute("_CoordinateAxisType", "GeoX"));
            double[] data1 = new double[this.numX];
            for (int i = 0; i < this.numX; ++i) {
                data1[i] = this.numX0 + i * ddx;
            }
            Array dataA = Array.factory(DataType.DOUBLE.getPrimitiveClassType(), new int[] { this.numX }, data1);
            xaxis.setCachedData(dataA, false);
            this.ncfile.addVariable(null, xaxis);
            final Variable yaxis = new Variable(this.ncfile, null, null, "y");
            yaxis.setDataType(DataType.DOUBLE);
            yaxis.setDimensions("y");
            yaxis.addAttribute(new Attribute("long_name", "projection y coordinate"));
            yaxis.addAttribute(new Attribute("units", "km"));
            yaxis.addAttribute(new Attribute("_CoordinateAxisType", "GeoY"));
            data1 = new double[this.numY];
            for (int j = 0; j < this.numY; ++j) {
                data1[j] = this.numY0 + j * ddx;
            }
            dataA = Array.factory(DataType.DOUBLE.getPrimitiveClassType(), new int[] { this.numY }, data1);
            yaxis.setCachedData(dataA, false);
            this.ncfile.addVariable(null, yaxis);
            final ProjectionImpl projection = new FlatEarth(this.lat_min, this.lon_max);
            final Variable ct = new Variable(this.ncfile, null, null, projection.getClassName());
            ct.setDataType(DataType.CHAR);
            ct.setDimensions("");
            final List params = projection.getProjectionParameters();
            for (int k = 0; k < params.size(); ++k) {
                final Parameter p = params.get(k);
                ct.addAttribute(new Attribute(p));
            }
            ct.addAttribute(new Attribute("_CoordinateTransformType", "Projection"));
            ct.addAttribute(new Attribute("_CoordinateAxes", "x y"));
            dataA = Array.factory(DataType.CHAR.getPrimitiveClassType(), new int[0]);
            dataA.setChar(dataA.getIndex(), ' ');
            ct.setCachedData(dataA, false);
            this.ncfile.addVariable(null, ct);
        }
        return soff;
    }
    
    int pcode_raster(final ByteBuffer bos, final short pkcode, final int hoff, int hedsiz, final boolean isZ, final byte[] data) {
        final byte[] b2 = new byte[2];
        final ArrayList dims = new ArrayList();
        int iscale = 1;
        final int ival = this.convertShort2unsignedInt(this.threshold[0]);
        if ((ival & 0x2000) != 0x0) {
            iscale = 20;
        }
        if ((ival & 0x1000) != 0x0) {
            iscale = 10;
        }
        final short[] rasp_code = { pkcode, 0, 0 };
        bos.get(b2, 0, 2);
        rasp_code[1] = (short)this.getInt(b2, 2);
        bos.get(b2, 0, 2);
        rasp_code[2] = (short)this.getInt(b2, 2);
        bos.get(b2, 0, 2);
        final short rasp_i = (short)this.getInt(b2, 2);
        bos.get(b2, 0, 2);
        final short rasp_j = (short)this.getInt(b2, 2);
        bos.get(b2, 0, 2);
        final short rasp_xscale = (short)this.getInt(b2, 2);
        bos.get(b2, 0, 2);
        final short rasp_xscalefract = (short)this.getInt(b2, 2);
        bos.get(b2, 0, 2);
        final short rasp_yscale = (short)this.getInt(b2, 2);
        bos.get(b2, 0, 2);
        final short rasp_yscalefract = (short)this.getInt(b2, 2);
        bos.get(b2, 0, 2);
        final short num_rows = (short)this.getInt(b2, 2);
        bos.get(b2, 0, 2);
        final short packing = (short)this.getInt(b2, 2);
        final int soff = 20;
        hedsiz += soff;
        final int nlevel = code_levelslookup(this.pcode);
        double ddx = code_reslookup(this.pcode);
        final int[] levels = this.getLevels(nlevel, this.threshold);
        this.numY0 = 0;
        this.numX0 = 0;
        this.numX = num_rows;
        this.numY = num_rows;
        final Dimension jDim = new Dimension("y", this.numY, true, false, false);
        final Dimension iDim = new Dimension("x", this.numX, true, false, false);
        dims.add(jDim);
        dims.add(iDim);
        this.ncfile.addDimension(null, iDim);
        this.ncfile.addDimension(null, jDim);
        if (this.cname.startsWith("Precip")) {
            this.ncfile.addAttribute(null, new Attribute("isRadial", new Integer(3)));
            ddx *= rasp_xscale;
        }
        this.ncfile.addAttribute(null, new Attribute("cdm_data_type", FeatureType.GRID.toString()));
        final String coordinates = "x y time latitude longitude altitude";
        final Variable v = new Variable(this.ncfile, null, null, this.cname + "_RAW");
        v.setDataType(DataType.BYTE);
        v.setDimensions(dims);
        this.ncfile.addVariable(null, v);
        v.addAttribute(new Attribute("long_name", this.ctitle));
        v.addAttribute(new Attribute("units", this.cunit));
        v.setSPobject(new Vinfo(this.numX, this.numX0, this.numY, this.numY0, hoff, hedsiz, this.isR, isZ, null, null, pkcode, 0));
        v.addAttribute(new Attribute("_CoordinateAxes", coordinates));
        if (this.cname.startsWith("VertLiquid")) {
            this.addVariable(this.cname, this.ctitle, this.ncfile, dims, coordinates, DataType.FLOAT, this.cunit, hoff, hedsiz, isZ, nlevel, levels, iscale);
        }
        else if (this.cname.startsWith("EchoTop")) {
            this.addVariable(this.cname, this.ctitle, this.ncfile, dims, coordinates, DataType.FLOAT, this.cunit, hoff, hedsiz, isZ, nlevel, levels, iscale);
        }
        else if (this.cname.startsWith("BaseReflectivityComp") || this.cname.startsWith("LayerCompReflect")) {
            this.addVariable(this.cname, this.ctitle, this.ncfile, dims, coordinates, DataType.FLOAT, this.cunit, hoff, hedsiz, isZ, nlevel, levels, iscale);
        }
        else if (this.cname.startsWith("Precip")) {
            this.addVariable(this.cname, this.ctitle, this.ncfile, dims, coordinates, DataType.FLOAT, this.cunit, hoff, hedsiz, isZ, nlevel, levels, iscale);
        }
        final Variable xaxis = new Variable(this.ncfile, null, null, "x");
        xaxis.setDataType(DataType.DOUBLE);
        xaxis.setDimensions("x");
        xaxis.addAttribute(new Attribute("long_name", "projection x coordinate"));
        xaxis.addAttribute(new Attribute("units", "km"));
        xaxis.addAttribute(new Attribute("_CoordinateAxisType", "GeoX"));
        double[] data2 = new double[this.numX];
        for (int i = 0; i < this.numX; ++i) {
            data2[i] = this.numX0 + i * ddx;
        }
        Array dataA = Array.factory(DataType.DOUBLE.getPrimitiveClassType(), new int[] { this.numX }, data2);
        xaxis.setCachedData(dataA, false);
        this.ncfile.addVariable(null, xaxis);
        final Variable yaxis = new Variable(this.ncfile, null, null, "y");
        yaxis.setDataType(DataType.DOUBLE);
        yaxis.setDimensions("y");
        yaxis.addAttribute(new Attribute("long_name", "projection y coordinate"));
        yaxis.addAttribute(new Attribute("units", "km"));
        yaxis.addAttribute(new Attribute("_CoordinateAxisType", "GeoY"));
        data2 = new double[this.numY];
        for (int j = 0; j < this.numY; ++j) {
            data2[j] = this.numY0 + j * ddx;
        }
        dataA = Array.factory(DataType.DOUBLE.getPrimitiveClassType(), new int[] { this.numY }, data2);
        yaxis.setCachedData(dataA, false);
        this.ncfile.addVariable(null, yaxis);
        final ProjectionImpl projection = new FlatEarth(this.lat_min, this.lon_max);
        final Variable ct = new Variable(this.ncfile, null, null, projection.getClassName());
        ct.setDataType(DataType.CHAR);
        ct.setDimensions("");
        final List params = projection.getProjectionParameters();
        for (int k = 0; k < params.size(); ++k) {
            final Parameter p = params.get(k);
            ct.addAttribute(new Attribute(p));
        }
        ct.addAttribute(new Attribute("_CoordinateTransformType", "Projection"));
        ct.addAttribute(new Attribute("_CoordinateAxes", "x y"));
        dataA = Array.factory(DataType.CHAR.getPrimitiveClassType(), new int[0]);
        dataA.setChar(dataA.getIndex(), ' ');
        ct.setCachedData(dataA, false);
        this.ncfile.addVariable(null, ct);
        return soff;
    }
    
    int pcode_radial(final ByteBuffer bos, final int hoff, int hedsiz, final boolean isZ, final byte[] data, final short[] threshold) throws IOException {
        final byte[] b2 = new byte[2];
        final ArrayList dims = new ArrayList();
        int iscale = 1;
        final int ival = this.convertShort2unsignedInt(threshold[0]);
        if ((ival & 0x2000) != 0x0) {
            iscale = 20;
        }
        if ((ival & 0x1000) != 0x0) {
            iscale = 10;
        }
        bos.get(b2, 0, 2);
        final short first_bin = (short)this.getInt(b2, 2);
        bos.get(b2, 0, 2);
        short num_bin = (short)this.getUInt(b2, 2);
        if (this.pcode == 94 || this.pcode == 99) {
            num_bin = this.addBinSize(num_bin);
        }
        bos.get(b2, 0, 2);
        final short radp_i = (short)this.getInt(b2, 2);
        bos.get(b2, 0, 2);
        final short radp_j = (short)this.getInt(b2, 2);
        bos.get(b2, 0, 2);
        short radp_scale = (short)this.getInt(b2, 2);
        if (this.pcode == 134 || this.pcode == 135) {
            radp_scale *= 1000;
        }
        bos.get(b2, 0, 2);
        final short num_radials = (short)this.getInt(b2, 2);
        final int soff = 12;
        hedsiz += soff;
        this.numY0 = 0;
        this.numY = num_radials;
        this.numX0 = first_bin;
        this.numX = num_bin;
        final int nlevel = code_levelslookup(this.pcode);
        this.ncfile.addAttribute(null, new Attribute("cdm_data_type", FeatureType.RADIAL.toString()));
        final Dimension radialDim = new Dimension("azimuth", num_radials);
        this.ncfile.addDimension(null, radialDim);
        final Dimension binDim = new Dimension("gate", num_bin);
        this.ncfile.addDimension(null, binDim);
        dims.add(radialDim);
        dims.add(binDim);
        final ArrayList dims2 = new ArrayList();
        final ArrayList dims3 = new ArrayList();
        dims2.add(radialDim);
        dims3.add(binDim);
        this.isR = true;
        String vName = "elevation";
        String lName = "elevation angle in degres: 0 = parallel to pedestal base, 90 = perpendicular";
        Attribute att = new Attribute("_CoordinateAxisType", AxisType.RadialElevation.toString());
        this.addParameter(vName, lName, this.ncfile, dims2, att, DataType.FLOAT, "degrees", hoff, hedsiz, isZ, this.p3);
        vName = "azimuth";
        lName = "azimuth angle in degrees: 0 = true north, 90 = east";
        att = new Attribute("_CoordinateAxisType", AxisType.RadialAzimuth.toString());
        this.addParameter(vName, lName, this.ncfile, dims2, att, DataType.FLOAT, "degrees", hoff, hedsiz, isZ, 0);
        vName = "gate";
        lName = "Radial distance to the start of gate";
        att = new Attribute("_CoordinateAxisType", AxisType.RadialDistance.toString());
        this.addParameter(vName, lName, this.ncfile, dims3, att, DataType.FLOAT, "meters", hoff, hedsiz, isZ, radp_scale);
        vName = "latitude";
        lName = "Latitude of the instrument";
        att = new Attribute("_CoordinateAxisType", AxisType.Lat.toString());
        this.addParameter(vName, lName, this.ncfile, dims2, att, DataType.FLOAT, "degrees", hoff, hedsiz, isZ, 0);
        vName = "longitude";
        lName = "Longitude of the instrument";
        att = new Attribute("_CoordinateAxisType", AxisType.Lon.toString());
        this.addParameter(vName, lName, this.ncfile, dims2, att, DataType.FLOAT, "degrees", hoff, hedsiz, isZ, 0);
        vName = "altitude";
        lName = "Altitude in meters (asl) of the instrument";
        att = new Attribute("_CoordinateAxisType", AxisType.Height.toString());
        this.addParameter(vName, lName, this.ncfile, dims2, att, DataType.FLOAT, "meters", hoff, hedsiz, isZ, 0);
        vName = "rays_time";
        lName = "rays time";
        att = new Attribute("_CoordinateAxisType", AxisType.Time.toString());
        this.addParameter(vName, lName, this.ncfile, dims2, att, DataType.DOUBLE, "milliseconds since 1970-01-01 00:00 UTC", hoff, hedsiz, isZ, 0);
        int[] levels;
        if (this.pcode == 182 || this.pcode == 99) {
            levels = this.getTDWRLevels(nlevel, threshold);
            iscale = 10;
        }
        else if (this.pcode == 186 || this.pcode == 94) {
            threshold[0] = -320;
            threshold[1] = 5;
            threshold[2] = 254;
            levels = this.getTDWRLevels(nlevel, threshold);
            iscale = 10;
        }
        else if (this.pcode == 32) {
            levels = this.getTDWRLevels1(nlevel, threshold);
            iscale = 10;
        }
        else if (this.pcode == 138) {
            levels = this.getTDWRLevels1(nlevel, threshold);
            iscale = 100;
        }
        else if (this.pcode == 134 || this.pcode == 135) {
            levels = this.getTDWRLevels2(nlevel, threshold);
            iscale = 1;
        }
        else {
            levels = this.getLevels(nlevel, threshold);
        }
        final Variable v = new Variable(this.ncfile, null, null, this.cname + "_RAW");
        v.setDataType(DataType.BYTE);
        v.setDimensions(dims);
        this.ncfile.addVariable(null, v);
        v.addAttribute(new Attribute("units", this.cunit));
        final String coordinates = "elevation azimuth gate rays_time latitude longitude altitude";
        v.addAttribute(new Attribute("_CoordinateAxes", coordinates));
        v.addAttribute(new Attribute("_unsigned", "true"));
        v.setSPobject(new Vinfo(this.numX, this.numX0, this.numY, this.numY0, hoff, hedsiz, this.isR, isZ, null, levels, 0, nlevel));
        if (this.cname.startsWith("BaseReflectivity") || this.cname.endsWith("Reflectivity") || this.cname.startsWith("SpectrumWidth")) {
            this.addVariable(this.cname, this.ctitle, this.ncfile, dims, coordinates, DataType.FLOAT, this.cunit, hoff, hedsiz, isZ, nlevel, levels, iscale);
        }
        else if (this.cname.startsWith("RadialVelocity") || this.cname.startsWith("StormMeanVelocity") || this.cname.startsWith("BaseVelocity")) {
            this.addVariable(this.cname, this.ctitle, this.ncfile, dims, coordinates, DataType.FLOAT, this.cunit, hoff, hedsiz, isZ, nlevel, levels, iscale);
        }
        else if (this.cname.startsWith("Precip") || this.cname.endsWith("Precip") || this.cname.startsWith("EnhancedEchoTop") || this.cname.startsWith("DigitalIntegLiquid")) {
            this.addVariable(this.cname, this.ctitle, this.ncfile, dims, coordinates, DataType.FLOAT, this.cunit, hoff, hedsiz, isZ, nlevel, levels, iscale);
        }
        return soff;
    }
    
    public short addBinSize(final short num_bin) {
        if (num_bin % 2 == 0) {
            return num_bin;
        }
        return (short)(num_bin + 1);
    }
    
    public int[] getLevels(final int nlevel, final short[] th) {
        final int[] levels = new int[nlevel];
        for (int i = 0; i < nlevel; ++i) {
            final int ival = this.convertShort2unsignedInt(th[i]);
            if ((ival & 0x8000) == 0x0) {
                int isign = -1;
                if ((ival & 0x100) == 0x0) {
                    isign = 1;
                }
                levels[i] = isign * (ival & 0xFF);
            }
            else {
                levels[i] = -9999 + (ival & 0xFF);
            }
        }
        return levels;
    }
    
    public int[] getTDWRLevels(final int nlevel, final short[] th) {
        final int[] levels = new int[nlevel];
        final int inc = th[1];
        levels[1] = (levels[0] = -9866);
        for (int i = 2; i < nlevel; ++i) {
            levels[i] = th[0] + (i - 2) * inc;
        }
        return levels;
    }
    
    public int[] getTDWRLevels1(final int nlevel, final short[] th) {
        final int[] levels = new int[nlevel];
        final int inc = th[1];
        for (int i = 0; i < nlevel; ++i) {
            levels[i] = th[0] + i * inc;
        }
        return levels;
    }
    
    public int[] getTDWRLevels2(final int nlevel, final short[] th) {
        final int inc = th.length;
        final int[] levels = new int[inc];
        for (int i = 0; i < inc; ++i) {
            levels[i] = th[i];
        }
        return levels;
    }
    
    void addVariable(final String pName, final String longName, final NetcdfFile nc, final ArrayList dims, final String coordinates, final DataType dtype, final String ut, final long hoff, final long hedsiz, final boolean isZ, final int nlevel, final int[] levels, final int iscale) {
        final Variable v = new Variable(nc, null, null, pName);
        v.setDataType(dtype);
        v.setDimensions(dims);
        this.ncfile.addVariable(null, v);
        v.addAttribute(new Attribute("long_name", longName));
        v.addAttribute(new Attribute("units", ut));
        v.addAttribute(new Attribute("_CoordinateAxes", coordinates));
        v.setSPobject(new Vinfo(this.numX, this.numX0, this.numY, this.numY0, hoff, hedsiz, this.isR, isZ, null, levels, iscale, nlevel));
    }
    
    void addParameter(final String pName, final String longName, final NetcdfFile nc, final ArrayList dims, final Attribute att, final DataType dtype, final String ut, final long hoff, final long doff, final boolean isZ, final int y0) {
        final String vName = pName;
        final Variable vVar = new Variable(nc, null, null, vName);
        vVar.setDataType(dtype);
        if (dims != null) {
            vVar.setDimensions(dims);
        }
        else {
            vVar.setDimensions("");
        }
        if (att != null) {
            vVar.addAttribute(att);
        }
        vVar.addAttribute(new Attribute("units", ut));
        vVar.addAttribute(new Attribute("long_name", longName));
        nc.addVariable(null, vVar);
        vVar.setSPobject(new Vinfo(this.numX, this.numX0, this.numY, y0, hoff, doff, this.isR, isZ, null, null, 0, 0));
    }
    
    String StnIdFromLatLon(final float lat, final float lon) {
        return "ID";
    }
    
    void setProductInfo(final int prod_type, final Pinfo pinfo) {
        final String[] cmode = { "Maintenance", "Clear Air", "Precip Mode" };
        short prod_max = pinfo.p4;
        short prod_min = 0;
        int prod_elevation = 0;
        int radial = 0;
        String summary = null;
        double t1 = 2.0651806579183893;
        double t2 = 230.0 / (111.26 * Math.cos(Math.toRadians(this.latitude)));
        this.lat_min = this.latitude - t1;
        this.lat_max = this.latitude + t1;
        this.lon_min = this.longitude + t2;
        this.lon_max = this.longitude - t2;
        final Date startDate = getDate(this.volumeScanDate, this.volumeScanTime * 1000);
        Date endDate = getDate(this.volumeScanDate, this.volumeScanTime * 1000);
        if (prod_type == 15) {
            radial = 1;
            prod_elevation = pinfo.p3;
            this.cmemo = "Base Specturm Width " + prod_elevation / 10 + " DEG " + cmode[pinfo.opmode];
            this.ctilt = pname_lookup(this.pcode, prod_elevation / 10);
            this.ctitle = "BREF: Base Spectrum Width";
            this.cunit = "Knots";
            this.cname = "SpectrumWidth";
            summary = this.ctilt + " is a radial image of base reflectivity at tilt " + (prod_elevation / 10 + 1) + " and range 124 nm";
            if (this.pcode == 28) {
                t1 *= 0.25;
                t2 *= 0.25;
                this.lat_min = this.latitude - t1;
                this.lat_max = this.latitude + t1;
                this.lon_min = this.longitude + t2;
                this.lon_max = this.longitude - t2;
                summary = this.ctilt + " is a radial image of base reflectivity at tilt " + (prod_elevation / 10 + 1) + " and range 32 nm";
            }
        }
        else if (prod_type == 21) {
            radial = 1;
            prod_elevation = pinfo.p3;
            this.cmemo = "Base Reflectivity DR " + prod_elevation / 10 + " DEG " + cmode[pinfo.opmode];
            if (prod_elevation == 5) {
                this.ctilt = pname_lookup(94, 0);
            }
            else if (prod_elevation == 9) {
                this.ctilt = pname_lookup(94, 1);
            }
            else if (prod_elevation == 13 || prod_elevation == 15) {
                this.ctilt = pname_lookup(94, 2);
            }
            else if (prod_elevation == 18) {
                this.ctilt = pname_lookup(94, 3);
            }
            else if (prod_elevation == 24) {
                this.ctilt = pname_lookup(94, 4);
            }
            else if (prod_elevation == 31) {
                this.ctilt = pname_lookup(94, 6);
            }
            this.ctitle = "HighResolution: Base Reflectivity";
            this.cunit = "dBz";
            this.cname = "BaseReflectivityDR";
            summary = this.ctilt + " is a radial image of base reflectivity field and its range 248 nm";
        }
        else if (prod_type == 22) {
            radial = 1;
            prod_elevation = pinfo.p3;
            this.cmemo = "Base Velocity DR " + prod_elevation / 10 + " DEG " + cmode[pinfo.opmode];
            if (prod_elevation == 5) {
                this.ctilt = pname_lookup(99, 0);
            }
            else if (prod_elevation == 9) {
                this.ctilt = pname_lookup(99, 1);
            }
            else if (prod_elevation == 13 || prod_elevation == 15) {
                this.ctilt = pname_lookup(99, 2);
            }
            else if (prod_elevation == 18) {
                this.ctilt = pname_lookup(99, 3);
            }
            else if (prod_elevation == 24) {
                this.ctilt = pname_lookup(99, 4);
            }
            else if (prod_elevation == 31) {
                this.ctilt = pname_lookup(99, 6);
            }
            this.ctitle = "HighResolution: Base Velocity";
            this.cunit = "KT";
            this.cname = "BaseVelocityDV";
            summary = this.ctilt + " is a radial image of base velocity field and its range 124 nm";
        }
        else if (prod_type == 24) {
            radial = 1;
            prod_elevation = pinfo.p3;
            this.cmemo = "Digital Hybrid Reflect " + prod_elevation / 10 + " DEG " + cmode[pinfo.opmode];
            this.ctilt = pname_lookup(134, prod_elevation / 10);
            this.ctitle = "Digital: Vertical Integ Liquid";
            this.cunit = "kg/m^2";
            this.cname = "DigitalIntegLiquid";
            summary = this.ctilt + " is a radial image high resolution vertical integral liquid and range 248 nm";
        }
        else if (prod_type == 16) {
            radial = 1;
            prod_elevation = pinfo.p3;
            this.cmemo = "Digital Hybrid Reflect " + prod_elevation / 10 + " DEG " + cmode[pinfo.opmode];
            this.ctilt = pname_lookup(19, prod_elevation / 10);
            this.ctitle = "DigitalHybrid: Reflectivity";
            this.cunit = "dBz";
            this.cname = "DigitalHybridReflectivity";
            summary = this.ctilt + " is a radial image of base reflectivity at tilt " + (prod_elevation / 10 + 1) + " and range 124 nm";
        }
        else if (prod_type == 1 || prod_type == 18) {
            radial = 1;
            prod_elevation = pinfo.p3;
            this.cmemo = "Base Reflct " + prod_elevation / 10 + " DEG " + cmode[pinfo.opmode];
            if (prod_type == 18) {
                this.ctilt = "R" + prod_elevation / 10;
                summary = this.ctilt + " is a radial image of base reflectivity at tilt " + (prod_elevation / 10 + 1);
            }
            else {
                this.ctilt = pname_lookup(19, prod_elevation / 10);
                summary = this.ctilt + " is a radial image of base reflectivity at tilt " + (prod_elevation / 10 + 1) + " and range 124 nm";
            }
            this.ctitle = "BREF: Base Reflectivity";
            this.cunit = "dBz";
            this.cname = "BaseReflectivity";
        }
        else if (prod_type == 12) {
            radial = 1;
            prod_elevation = pinfo.p3;
            this.cmemo = "Base Reflct 248 " + prod_elevation / 10 + " DEG " + cmode[pinfo.opmode];
            this.ctilt = pname_lookup(20, prod_elevation / 10);
            this.ctitle = "BREF: 248 nm Base Reflectivity";
            this.cunit = "dBz";
            this.cname = "BaseReflectivity248";
            summary = this.ctilt + " is a radial image of base reflectivity at tilt " + (prod_elevation / 10 + 1) + " and range 248 nm";
            t1 = 4.130361315836779;
            t2 = 460.0 / (111.26 * Math.cos(Math.toRadians(this.latitude)));
            this.lat_min = this.latitude - t1;
            this.lat_max = this.latitude + t1;
            this.lon_min = this.longitude + t2;
            this.lon_max = this.longitude - t2;
        }
        else if (prod_type == 3) {
            radial = 3;
            prod_elevation = -1;
            this.ctilt = pname_lookup(pinfo.pcode, this.elevationNumber);
            if (pinfo.pcode == 36 || pinfo.pcode == 38) {
                t1 *= 2.0;
                t2 *= 2.0;
                this.lat_min = this.latitude - t1;
                this.lat_max = this.latitude + t1;
                this.lon_min = this.longitude + t2;
                this.lon_max = this.longitude - t2;
            }
            summary = this.ctilt + "is a raster image of composite reflectivity";
            this.cmemo = "Composite Reflectivity at " + cmode[pinfo.opmode];
            this.ctitle = "CREF Composite Reflectivity" + this.ctilt;
            this.cunit = "dBz";
            this.cname = "BaseReflectivityComp";
        }
        else if (prod_type == 4 || prod_type == 5) {
            radial = 3;
            prod_elevation = pinfo.p5;
            final int prod_top = pinfo.p6;
            this.ctilt = pname_lookup(this.pcode, 0);
            summary = this.ctilt + " is a raster image of composite reflectivity at range 124 nm";
            this.cmemo = "Layer Reflct " + prod_elevation + " - " + prod_top + cmode[pinfo.opmode];
            t1 *= 4.0;
            t2 *= 4.0;
            this.lat_min = this.latitude - t1;
            this.lat_max = this.latitude + t1;
            this.lon_min = this.longitude + t2;
            this.lon_max = this.longitude - t2;
            this.ctitle = "LREF: Layer Composite Reflectivity";
            this.cunit = "dBz";
            this.cname = "LayerCompReflect";
        }
        else if (prod_type == 23) {
            radial = 1;
            prod_elevation = -1;
            summary = "EET is a radial image of echo tops at range 186 nm";
            this.cmemo = "Enhanced Echo Tops [K FT] " + cmode[pinfo.opmode];
            this.ctilt = pname_lookup(135, this.elevationNumber);
            this.ctitle = "TOPS: Enhanced Echo Tops";
            this.cunit = "K FT";
            this.cname = "EnhancedEchoTop";
            t1 *= 4.0;
            t2 *= 4.0;
            this.lat_min = this.latitude - t1;
            this.lat_max = this.latitude + t1;
            this.lon_min = this.longitude + t2;
            this.lon_max = this.longitude - t2;
        }
        else if (prod_type == 6) {
            radial = 3;
            prod_elevation = -1;
            summary = "NET is a raster image of echo tops at range 124 nm";
            this.cmemo = "Echo Tops [K FT] " + cmode[pinfo.opmode];
            this.ctilt = pname_lookup(41, this.elevationNumber);
            this.ctitle = "TOPS: Echo Tops";
            this.cunit = "K FT";
            this.cname = "EchoTop";
            t1 *= 4.0;
            t2 *= 4.0;
            this.lat_min = this.latitude - t1;
            this.lat_max = this.latitude + t1;
            this.lon_min = this.longitude + t2;
            this.lon_max = this.longitude - t2;
        }
        else if (prod_type == 8) {
            radial = 1;
            prod_elevation = -1;
            prod_max /= 10;
            endDate = getDate(pinfo.p7, pinfo.p8 * 60 * 1000);
            summary = "N1P is a raster image of 1 hour surface rainfall accumulation at range 124 nm";
            this.cmemo = "1-hr Rainfall [IN] " + cmode[pinfo.opmode];
            this.ctilt = pname_lookup(78, this.elevationNumber);
            this.ctitle = "PRE1: Surface 1-hour Rainfall Total";
            this.cunit = "IN";
            this.cname = "Precip1hr";
            t1 *= 2.0;
            t2 *= 2.0;
            this.lat_min = this.latitude - t1;
            this.lat_max = this.latitude + t1;
            this.lon_min = this.longitude + t2;
            this.lon_max = this.longitude - t2;
        }
        else if (prod_type == 9) {
            radial = 1;
            prod_elevation = -1;
            prod_max /= 10;
            endDate = getDate(pinfo.p7, pinfo.p8 * 60 * 1000);
            summary = "N3P is a raster image of 3 hour surface rainfall accumulation at range 124 nm";
            this.cmemo = "3-hr Rainfall [IN] " + cmode[pinfo.opmode];
            this.ctilt = pname_lookup(79, this.elevationNumber);
            this.ctitle = "PRE3: Surface 3-hour Rainfall Total";
            this.cunit = "IN";
            this.cname = "Precip3hr";
            t1 *= 2.0;
            t2 *= 2.0;
            this.lat_min = this.latitude - t1;
            this.lat_max = this.latitude + t1;
            this.lon_min = this.longitude + t2;
            this.lon_max = this.longitude - t2;
        }
        else if (prod_type == 17) {
            radial = 1;
            prod_elevation = -1;
            endDate = getDate(pinfo.p7, pinfo.p8 * 60 * 1000);
            summary = "DSP is a radial image of digital storm total rainfall";
            this.cmemo = "Digital Strm Total Precip [IN] " + cmode[pinfo.opmode];
            this.ctilt = pname_lookup(80, this.elevationNumber);
            this.ctitle = "DPRE: Digital Storm Total Rainfall";
            this.cunit = "IN";
            this.cname = "DigitalPrecip";
            t1 *= 2.0;
            t2 *= 2.0;
            this.lat_min = this.latitude - t1;
            this.lat_max = this.latitude + t1;
            this.lon_min = this.longitude + t2;
            this.lon_max = this.longitude - t2;
        }
        else if (prod_type == 10) {
            radial = 1;
            prod_elevation = -1;
            endDate = getDate(pinfo.p7, pinfo.p8 * 60 * 1000);
            summary = "NTP is a raster image of storm total rainfall accumulation at range 124 nm";
            this.cmemo = "Strm Tot Rain [IN] " + cmode[pinfo.opmode];
            this.ctilt = pname_lookup(80, this.elevationNumber);
            this.ctitle = "PRET: Surface Storm Total Rainfall";
            this.cunit = "IN";
            this.cname = "PrecipAccum";
            t1 *= 2.0;
            t2 *= 2.0;
            this.lat_min = this.latitude - t1;
            this.lat_max = this.latitude + t1;
            this.lon_min = this.longitude + t2;
            this.lon_max = this.longitude - t2;
        }
        else if (prod_type == 11) {
            radial = 3;
            prod_elevation = -1;
            summary = "DPA is a raster image of hourly digital precipitation array at range 124 nm";
            endDate = getDate(pinfo.p7, pinfo.p8 * 60 * 1000);
            this.cmemo = "Precip Array [IN] " + cmode[pinfo.opmode];
            this.ctilt = pname_lookup(81, this.elevationNumber);
            this.ctitle = "PRET: Hourly Digital Precipitation Array";
            this.cunit = "dBA";
            this.cname = "PrecipArray";
        }
        else if (prod_type == 7) {
            radial = 3;
            prod_elevation = -1;
            summary = "NVL is a raster image of verticalintegrated liguid at range 124 nm";
            this.cmemo = "Vert Int Lq H2O [mm] " + cmode[pinfo.opmode];
            this.ctilt = pname_lookup(57, this.elevationNumber);
            this.ctitle = "VIL: Vertically-integrated Liquid Water";
            this.cunit = "kg/m^2";
            this.cname = "VertLiquid";
            t1 *= 4.0;
            t2 *= 4.0;
            this.lat_min = this.latitude - t1;
            this.lat_max = this.latitude + t1;
            this.lon_min = this.longitude + t2;
            this.lon_max = this.longitude - t2;
        }
        else if (prod_type == 2 || prod_type == 19) {
            radial = 1;
            prod_elevation = pinfo.p3;
            prod_min = pinfo.p4;
            prod_max = pinfo.p5;
            if (prod_type == 2) {
                this.ctilt = pname_lookup(pinfo.pcode, prod_elevation / 10);
            }
            else {
                this.ctilt = "V" + prod_elevation / 10;
            }
            if (pinfo.pcode == 25) {
                t1 = 0.5329498472047456;
                t2 = 64.0 / (111.26 * Math.cos(Math.toRadians(this.latitude)));
                this.lat_min = this.latitude - t1;
                this.lat_max = this.latitude + t1;
                this.lon_min = this.longitude + t2;
                this.lon_max = this.longitude - t2;
                summary = this.ctilt + " is a radial image of base velocity" + (prod_elevation / 10 + 1) + " and  range 32 nm";
                this.cunit = "KT";
            }
            else {
                summary = this.ctilt + " is a radial image of base velocity at tilt " + (prod_elevation / 10 + 1);
                this.cunit = "KT";
            }
            this.cmemo = "Rad Vel " + prod_elevation / 10.0 + " DEG " + cmode[pinfo.opmode];
            this.ctitle = "VEL: Radial Velocity";
            this.cname = "RadialVelocity";
        }
        else if (prod_type == 13) {
            radial = 1;
            prod_elevation = pinfo.p3;
            prod_min = pinfo.p4;
            prod_max = pinfo.p5;
            this.ctilt = pname_lookup(56, prod_elevation / 10);
            summary = this.ctilt + " is a radial image of storm relative mean radial velocity at tilt " + (prod_elevation / 10 + 1) + " and  range 124 nm";
            this.cmemo = "StrmRelMnVl " + prod_elevation / 10.0 + " DEG " + cmode[pinfo.opmode];
            this.ctitle = "SRMV: Storm Relative Mean Velocity";
            this.cunit = "KT";
            this.cname = "StormMeanVelocity";
        }
        else if (prod_type == 14) {
            radial = 0;
            prod_elevation = pinfo.p3;
            prod_min = pinfo.p4;
            prod_max = pinfo.p5;
            summary = "NVW is VAD wind profile which contains wind barbs and alpha numeric data";
            this.cmemo = "StrmRelMnVl " + prod_elevation / 10.0 + " DEG " + cmode[pinfo.opmode];
            this.ctilt = pname_lookup(48, this.elevationNumber);
            this.ctitle = "SRMV: Velocity Azimuth Display";
            this.cunit = "KT";
            this.cname = "VADWindSpeed";
            this.lat_min = this.latitude;
            this.lat_max = this.latitude;
            this.lon_min = this.longitude;
            this.lon_max = this.longitude;
        }
        else {
            this.ctilt = "error";
            this.ctitle = "error";
            this.cunit = "error";
            this.cname = "error";
        }
        this.ncfile.addAttribute(null, new Attribute("summary", "Nexrad level 3 data are WSR-88D radar products." + summary));
        this.ncfile.addAttribute(null, new Attribute("keywords_vocabulary", this.ctilt));
        this.ncfile.addAttribute(null, new Attribute("conventions", "_Coordinates"));
        this.ncfile.addAttribute(null, new Attribute("format", "Level3/NIDS"));
        this.ncfile.addAttribute(null, new Attribute("geospatial_lat_min", new Float(this.lat_min)));
        this.ncfile.addAttribute(null, new Attribute("geospatial_lat_max", new Float(this.lat_max)));
        this.ncfile.addAttribute(null, new Attribute("geospatial_lon_min", new Float(this.lon_min)));
        this.ncfile.addAttribute(null, new Attribute("geospatial_lon_max", new Float(this.lon_max)));
        this.ncfile.addAttribute(null, new Attribute("geospatial_vertical_min", new Float(this.height)));
        this.ncfile.addAttribute(null, new Attribute("geospatial_vertical_max", new Float(this.height)));
        this.ncfile.addAttribute(null, new Attribute("RadarElevationNumber", new Integer(prod_elevation)));
        String dstring = this.formatter.toDateTimeStringISO(startDate);
        this.ncfile.addAttribute(null, new Attribute("time_coverage_start", dstring));
        dstring = this.formatter.toDateTimeStringISO(endDate);
        this.ncfile.addAttribute(null, new Attribute("time_coverage_end", dstring));
        this.ncfile.addAttribute(null, new Attribute("data_min", new Float(prod_min)));
        this.ncfile.addAttribute(null, new Attribute("data_max", new Float(prod_max)));
        this.ncfile.addAttribute(null, new Attribute("isRadial", new Integer(radial)));
    }
    
    byte[] uncompressed(final ByteBuffer buf, final int offset, final int uncomplen) throws IOException {
        final byte[] header = new byte[offset];
        buf.position(0);
        buf.get(header);
        final byte[] out = new byte[offset + uncomplen];
        System.arraycopy(header, 0, out, 0, offset);
        final CBZip2InputStream cbzip2 = new CBZip2InputStream();
        final int numCompBytes = buf.remaining();
        final byte[] bufc = new byte[numCompBytes];
        buf.get(bufc, 0, numCompBytes);
        final ByteArrayInputStream bis = new ByteArrayInputStream(bufc, 2, numCompBytes - 2);
        cbzip2.setStream(bis);
        int total = 0;
        final byte[] ubuff = new byte[40000];
        byte[] obuff = new byte[40000];
        try {
            int nread;
            while ((nread = cbzip2.read(ubuff)) != -1) {
                if (total + nread > obuff.length) {
                    final byte[] temp = obuff;
                    obuff = new byte[temp.length * 2];
                    System.arraycopy(temp, 0, obuff, 0, temp.length);
                }
                System.arraycopy(ubuff, 0, obuff, total, nread);
                total += nread;
            }
            if (obuff.length >= 0) {
                System.arraycopy(obuff, 0, out, offset, total);
            }
        }
        catch (BZip2ReadException ioe) {
            Nidsheader.log.warn("Nexrad2IOSP.uncompress " + this.raf.getLocation(), ioe);
        }
        return out;
    }
    
    public static int shortsToInt(final short s1, final short s2, final boolean swapBytes) {
        final byte[] b = { (byte)(s1 >>> 8), (byte)(s1 >>> 0), (byte)(s2 >>> 8), (byte)(s2 >>> 0) };
        return bytesToInt(b, false);
    }
    
    public static int bytesToInt(final byte[] bytes, final boolean swapBytes) {
        final byte a = bytes[0];
        final byte b = bytes[1];
        final byte c = bytes[2];
        final byte d = bytes[3];
        if (swapBytes) {
            return (a & 0xFF) + ((b & 0xFF) << 8) + ((c & 0xFF) << 16) + ((d & 0xFF) << 24);
        }
        return ((a & 0xFF) << 24) + ((b & 0xFF) << 16) + ((c & 0xFF) << 8) + (d & 0xFF);
    }
    
    Sinfo read_dividlen(final ByteBuffer buf, final int offset) {
        int off = offset;
        final byte[] b2 = new byte[2];
        final byte[] b3 = new byte[4];
        buf.position(offset);
        buf.get(b2, 0, 2);
        final Short tShort = (Short)this.convert(b2, DataType.SHORT, -1);
        final short D_divider = tShort;
        buf.get(b2, 0, 2);
        final short D_id = (short)this.getInt(b2, 2);
        buf.get(b3, 0, 4);
        this.block_length = this.getInt(b3, 4);
        buf.get(b2, 0, 2);
        this.number_layers = (short)this.getInt(b2, 2);
        off += 10;
        return new Sinfo(D_divider, D_id, this.block_length, this.number_layers);
    }
    
    void read_SATab(final ByteBuffer buf, final int offset) {
        final byte[] b2 = new byte[2];
        buf.position(offset);
        buf.get(b2, 0, 2);
        final Short tShort = (Short)this.convert(b2, DataType.SHORT, -1);
        final short B_divider = tShort;
        if (B_divider != -1) {
            Nidsheader.log.warn("error reading stand alone tab message " + this.raf.getLocation());
        }
        buf.get(b2, 0, 2);
        final short numPages = (short)this.convert(b2, DataType.SHORT, -1);
        final int ppos = buf.position();
        for (int i = 0; i < numPages; ++i) {
            buf.get(b2, 0, 2);
            while (this.getInt(b2, 2) != -1) {
                final short numChars = (short)this.getInt(b2, 2);
                if (numChars < 0) {
                    break;
                }
                final byte[] tmp = new byte[numChars];
                buf.get(tmp);
                final String text = new String(tmp);
                buf.get(b2, 0, 2);
            }
        }
        final ArrayList dims = new ArrayList();
        final Dimension tbDim = new Dimension("pageNumber", numPages);
        this.ncfile.addDimension(null, tbDim);
        dims.add(tbDim);
        final Variable ppage = new Variable(this.ncfile, null, null, "TabMessagePage");
        ppage.setDimensions(dims);
        ppage.setDataType(DataType.STRING);
        ppage.addAttribute(new Attribute("long_name", "Stand Alone Tabular Alphanumeric Product Message"));
        this.ncfile.addVariable(null, ppage);
    }
    
    int read_msghead(final ByteBuffer buf, final int offset) {
        final byte[] b2 = new byte[2];
        final byte[] b3 = new byte[4];
        buf.position(0);
        buf.get(b2, 0, 2);
        this.mcode = (short)this.getInt(b2, 2);
        buf.get(b2, 0, 2);
        this.mdate = (short)this.getInt(b2, 2);
        buf.get(b3, 0, 4);
        this.mtime = this.getInt(b3, 4);
        buf.get(b3, 0, 4);
        final Date volumnDate = getDate(this.mdate, this.mtime * 1000);
        final String dstring = this.formatter.toDateTimeStringISO(volumnDate);
        this.mlength = this.getInt(b3, 4);
        buf.get(b2, 0, 2);
        this.msource = (short)this.getInt(b2, 2);
        Label_0251: {
            if (this.stationId != null) {
                if (this.stationName != null) {
                    break Label_0251;
                }
            }
            try {
                NexradStationDB.init();
                final NexradStationDB.Station station = NexradStationDB.getByIdNumber("000" + Short.toString(this.msource));
                if (station != null) {
                    this.stationId = station.id;
                    this.stationName = station.name;
                }
            }
            catch (IOException ioe) {
                Nidsheader.log.error("NexradStationDB.init " + this.raf.getLocation(), ioe);
            }
        }
        buf.get(b2, 0, 2);
        this.mdestId = (short)this.getInt(b2, 2);
        buf.get(b2, 0, 2);
        this.mNumOfBlock = (short)this.getInt(b2, 2);
        return 1;
    }
    
    int getUInt(final byte[] b, final int num) {
        int base = 1;
        int word = 0;
        final int[] bv = new int[num];
        for (int i = 0; i < num; ++i) {
            bv[i] = this.convertunsignedByte2Short(b[i]);
        }
        for (int i = num - 1; i >= 0; --i) {
            word += base * bv[i];
            base *= 256;
        }
        return word;
    }
    
    int getInt(final byte[] b, final int num) {
        int base = 1;
        int word = 0;
        final int[] bv = new int[num];
        for (int i = 0; i < num; ++i) {
            bv[i] = this.convertunsignedByte2Short(b[i]);
        }
        if (bv[0] > 127) {
            final int[] array = bv;
            final int n = 0;
            array[n] -= 128;
            base = -1;
        }
        for (int i = num - 1; i >= 0; --i) {
            word += base * bv[i];
            base *= 256;
        }
        return word;
    }
    
    public static int bytesToInt(final byte a, final byte b, final boolean swapBytes) {
        if (swapBytes) {
            return (a & 0xFF) + (b << 8);
        }
        return (a << 8) + (b & 0xFF);
    }
    
    public short convertunsignedByte2Short(final byte b) {
        return (short)((b < 0) ? (b + 256) : ((short)b));
    }
    
    public int convertShort2unsignedInt(final short b) {
        return (b < 0) ? (-1 * b + 32768) : b;
    }
    
    public static Date getDate(final int julianDays, final int msecs) {
        final long total = (julianDays - 1) * 24L * 3600L * 1000L + msecs;
        return new Date(total);
    }
    
    Pinfo read_proddesc(final ByteBuffer buf, final int offset) {
        final byte[] b2 = new byte[2];
        final byte[] b3 = new byte[4];
        int off = offset;
        this.ncfile.addAttribute(null, new Attribute("title", "Nexrad Level 3 Data"));
        this.ncfile.addAttribute(null, new Attribute("keywords", "WSR-88D; NIDS"));
        this.ncfile.addAttribute(null, new Attribute("creator_name", "NOAA/NWS"));
        this.ncfile.addAttribute(null, new Attribute("creator_url", "http://www.ncdc.noaa.gov/oa/radar/radarproducts.html"));
        this.ncfile.addAttribute(null, new Attribute("naming_authority", "NOAA/NCDC"));
        buf.position(offset);
        buf.get(b2, 0, 2);
        final Short tShort = (Short)this.convert(b2, DataType.SHORT, -1);
        this.divider = tShort;
        this.ncfile.addAttribute(null, new Attribute("Divider", tShort));
        buf.get(b3, 0, 4);
        Integer tInt = (Integer)this.convert(b3, DataType.INT, -1);
        this.latitude = tInt / 1000.0;
        buf.get(b3, 0, 4);
        tInt = (Integer)this.convert(b3, DataType.INT, -1);
        this.longitude = tInt / 1000.0;
        buf.get(b2, 0, 2);
        this.height = this.getInt(b2, 2) * 0.3048;
        this.ncfile.addAttribute(null, new Attribute("RadarLatitude", new Double(this.latitude)));
        this.ncfile.addAttribute(null, new Attribute("RadarLongitude", new Double(this.longitude)));
        this.ncfile.addAttribute(null, new Attribute("RadarAltitude", new Double(this.height)));
        buf.get(b2, 0, 2);
        this.pcode = (short)this.getInt(b2, 2);
        this.ncfile.addAttribute(null, new Attribute("ProductStation", this.stationId));
        this.ncfile.addAttribute(null, new Attribute("ProductStationName", this.stationName));
        buf.get(b2, 0, 2);
        this.opmode = (short)this.getInt(b2, 2);
        this.ncfile.addAttribute(null, new Attribute("OperationalMode", new Short(this.opmode)));
        buf.get(b2, 0, 2);
        this.volumnScanPattern = (short)this.getInt(b2, 2);
        this.ncfile.addAttribute(null, new Attribute("VolumeCoveragePatternName", new Short(this.volumnScanPattern)));
        buf.get(b2, 0, 2);
        this.sequenceNumber = (short)this.getInt(b2, 2);
        this.ncfile.addAttribute(null, new Attribute("SequenceNumber", new Short(this.sequenceNumber)));
        buf.get(b2, 0, 2);
        this.volumeScanNumber = (short)this.getInt(b2, 2);
        this.ncfile.addAttribute(null, new Attribute("VolumeScanNumber", new Short(this.volumeScanNumber)));
        buf.get(b2, 0, 2);
        this.volumeScanDate = (short)this.getUInt(b2, 2);
        buf.get(b3, 0, 4);
        this.volumeScanTime = this.getUInt(b3, 4);
        buf.get(b2, 0, 2);
        this.productDate = (short)this.getUInt(b2, 2);
        buf.get(b3, 0, 4);
        this.productTime = this.getUInt(b3, 4);
        final Date pDate = getDate(this.productDate, this.productTime * 1000);
        final String dstring = this.formatter.toDateTimeStringISO(pDate);
        this.ncfile.addAttribute(null, new Attribute("DateCreated", dstring));
        buf.get(b2, 0, 2);
        this.p1 = (short)this.getInt(b2, 2);
        buf.get(b2, 0, 2);
        this.p2 = (short)this.getInt(b2, 2);
        buf.get(b2, 0, 2);
        this.elevationNumber = (short)this.getInt(b2, 2);
        this.ncfile.addAttribute(null, new Attribute("ElevationNumber", new Short(this.elevationNumber)));
        buf.get(b2, 0, 2);
        this.p3 = (short)this.getInt(b2, 2);
        off += 40;
        if (this.pcode == 182 || this.pcode == 186 || this.pcode == 32 || this.pcode == 94 || this.pcode == 99) {
            for (int i = 0; i < 16; ++i) {
                buf.get(b2, 0, 2);
                this.threshold[i] = (short)bytesToInt(b2[0], b2[1], false);
            }
        }
        else {
            for (int i = 0; i < 16; ++i) {
                buf.get(b2, 0, 2);
                this.threshold[i] = (short)this.getInt(b2, 2);
            }
        }
        off += 32;
        buf.get(b2, 0, 2);
        this.p4 = (short)this.getInt(b2, 2);
        buf.get(b2, 0, 2);
        this.p5 = (short)this.getInt(b2, 2);
        buf.get(b2, 0, 2);
        this.p6 = (short)this.getInt(b2, 2);
        buf.get(b2, 0, 2);
        this.p7 = (short)this.getInt(b2, 2);
        buf.get(b2, 0, 2);
        this.p8 = (short)this.getInt(b2, 2);
        buf.get(b2, 0, 2);
        this.p9 = (short)this.getInt(b2, 2);
        buf.get(b2, 0, 2);
        this.p10 = (short)this.getUInt(b2, 2);
        off += 14;
        buf.get(b2, 0, 2);
        this.numberOfMaps = (short)this.getInt(b2, 2);
        this.ncfile.addAttribute(null, new Attribute("NumberOfMaps", new Short(this.numberOfMaps)));
        off += 2;
        buf.get(b3, 0, 4);
        this.offsetToSymbologyBlock = this.getInt(b3, 4);
        off += 4;
        buf.get(b3, 0, 4);
        this.offsetToGraphicBlock = this.getInt(b3, 4);
        off += 4;
        buf.get(b3, 0, 4);
        this.offsetToTabularBlock = this.getInt(b3, 4);
        off += 4;
        return new Pinfo(this.divider, this.latitude, this.longitude, this.height, this.pcode, this.opmode, this.threshold, this.sequenceNumber, this.volumeScanNumber, this.volumeScanDate, this.volumeScanTime, this.productDate, this.productTime, this.p1, this.p2, this.p3, this.p4, this.p5, this.p6, this.p7, this.p8, this.p9, this.p10, this.elevationNumber, this.numberOfMaps, this.offsetToSymbologyBlock, this.offsetToGraphicBlock, this.offsetToTabularBlock);
    }
    
    protected Object convert(final byte[] barray, final DataType dataType, final int nelems, final int byteOrder) {
        if (dataType == DataType.BYTE) {
            return barray;
        }
        if (dataType == DataType.CHAR) {
            return IospHelper.convertByteToChar(barray);
        }
        final ByteBuffer bbuff = ByteBuffer.wrap(barray);
        if (byteOrder >= 0) {
            bbuff.order((byteOrder == 1) ? ByteOrder.LITTLE_ENDIAN : ByteOrder.BIG_ENDIAN);
        }
        if (dataType == DataType.SHORT) {
            final ShortBuffer tbuff = bbuff.asShortBuffer();
            final short[] pa = new short[nelems];
            tbuff.get(pa);
            return pa;
        }
        if (dataType == DataType.INT) {
            final IntBuffer tbuff2 = bbuff.asIntBuffer();
            final int[] pa2 = new int[nelems];
            tbuff2.get(pa2);
            return pa2;
        }
        if (dataType == DataType.FLOAT) {
            final FloatBuffer tbuff3 = bbuff.asFloatBuffer();
            final float[] pa3 = new float[nelems];
            tbuff3.get(pa3);
            return pa3;
        }
        if (dataType == DataType.DOUBLE) {
            final DoubleBuffer tbuff4 = bbuff.asDoubleBuffer();
            final double[] pa4 = new double[nelems];
            tbuff4.get(pa4);
            return pa4;
        }
        throw new IllegalStateException();
    }
    
    protected Object convert(final byte[] barray, final DataType dataType, final int byteOrder) {
        if (dataType == DataType.BYTE) {
            return new Byte(barray[0]);
        }
        if (dataType == DataType.CHAR) {
            return new Character((char)barray[0]);
        }
        final ByteBuffer bbuff = ByteBuffer.wrap(barray);
        if (byteOrder >= 0) {
            bbuff.order((byteOrder == 1) ? ByteOrder.LITTLE_ENDIAN : ByteOrder.BIG_ENDIAN);
        }
        if (dataType == DataType.SHORT) {
            final ShortBuffer tbuff = bbuff.asShortBuffer();
            return new Short(tbuff.get());
        }
        if (dataType == DataType.INT) {
            final IntBuffer tbuff2 = bbuff.asIntBuffer();
            return new Integer(tbuff2.get());
        }
        if (dataType == DataType.LONG) {
            final LongBuffer tbuff3 = bbuff.asLongBuffer();
            return new Long(tbuff3.get());
        }
        if (dataType == DataType.FLOAT) {
            final FloatBuffer tbuff4 = bbuff.asFloatBuffer();
            return new Float(tbuff4.get());
        }
        if (dataType == DataType.DOUBLE) {
            final DoubleBuffer tbuff5 = bbuff.asDoubleBuffer();
            return new Double(tbuff5.get());
        }
        throw new IllegalStateException();
    }
    
    public void flush() throws IOException {
        this.raf.flush();
    }
    
    public void close() throws IOException {
        if (this.raf != null) {
            this.raf.close();
        }
    }
    
    int isZlibHed(final byte[] buf) {
        final short b0 = this.convertunsignedByte2Short(buf[0]);
        final short b2 = this.convertunsignedByte2Short(buf[1]);
        if ((b0 & 0xF) == this.Z_DEFLATED && (b0 >> 4) + 8 <= this.DEF_WBITS && ((b0 << 8) + b2) % 31 == 0) {
            return 1;
        }
        return 0;
    }
    
    int IsEncrypt(final byte[] buf) {
        final String b = new String(buf);
        if (b.startsWith("R3")) {
            return 1;
        }
        return 0;
    }
    
    byte[] GetZlibedNexr(final byte[] buf, final int buflen, final int hoff) throws IOException {
        final int numin = buflen - hoff;
        if (numin <= 0) {
            Nidsheader.log.warn(" No compressed data to inflate " + this.raf.getLocation());
            return null;
        }
        System.arraycopy(buf, hoff, buf, hoff, numin - 4);
        int result = 0;
        int uncompLen = 24500;
        byte[] uncomp = new byte[uncompLen];
        final Inflater inflater = new Inflater(false);
        inflater.setInput(buf, hoff, numin - 4);
        int offset = 0;
        final int limit = 20000;
        while (inflater.getRemaining() > 0) {
            int resultLength;
            try {
                resultLength = inflater.inflate(uncomp, offset, 4000);
            }
            catch (DataFormatException ex) {
                System.out.println("ERROR on inflation " + ex.getMessage());
                ex.printStackTrace();
                throw new IOException(ex.getMessage());
            }
            offset += resultLength;
            result += resultLength;
            if (result > limit) {
                final byte[] tmp = new byte[result];
                System.arraycopy(uncomp, 0, tmp, 0, result);
                uncompLen += 10000;
                uncomp = new byte[uncompLen];
                System.arraycopy(tmp, 0, uncomp, 0, result);
            }
            if (resultLength == 0) {
                final int tt = inflater.getRemaining();
                final byte[] b2 = new byte[2];
                System.arraycopy(buf, hoff + numin - 4 - tt, b2, 0, 2);
                if (result + tt > uncompLen) {
                    final byte[] tmp = new byte[result];
                    System.arraycopy(uncomp, 0, tmp, 0, result);
                    uncompLen += 10000;
                    uncomp = new byte[uncompLen];
                    System.arraycopy(tmp, 0, uncomp, 0, result);
                }
                if (this.isZlibHed(b2) == 0) {
                    System.arraycopy(buf, hoff + numin - 4 - tt, uncomp, result, tt);
                    result += tt;
                    break;
                }
                inflater.reset();
                inflater.setInput(buf, hoff + numin - 4 - tt, tt);
            }
        }
        inflater.end();
        final byte b3 = uncomp[0];
        final byte b4 = uncomp[1];
        int doff = 2 * (((b3 & 0x3F) << 8) + b4);
        for (int i = 0; i < 2; ++i) {
            while (doff < result && uncomp[doff] != 10) {
                ++doff;
            }
            ++doff;
        }
        final byte[] data = new byte[result - doff];
        System.arraycopy(uncomp, doff, data, 0, result - doff);
        return data;
    }
    
    static int code_typelookup(final int code) {
        final int[] types = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 12, 1, 2, 2, 2, 2, 2, 2, 15, 15, 15, 0, 16, 0, 0, 3, 3, 3, 3, 0, 0, 6, 0, 0, 0, 0, 0, 0, 14, 0, 0, 0, 0, 0, 0, 13, 13, 7, 0, 0, 0, 0, 0, 4, 4, 5, 5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8, 9, 10, 11, 0, 0, 0, 0, 0, 0, 0, 4, 5, 0, 0, 0, 21, 0, 0, 0, 0, 22, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 24, 23, 0, 0, 17, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 18, 18, 19, 19, 0, 20, 18, 18, 0, 0 };
        int type;
        if (code < 0 || code > 189) {
            type = 0;
        }
        else {
            type = types[code];
        }
        return type;
    }
    
    static String pname_lookup(final int code, final int elevation) {
        String pname = null;
        switch (code) {
            case 19: {
                pname = "N" + elevation + "R";
                break;
            }
            case 20: {
                pname = "N0Z";
                break;
            }
            case 25: {
                pname = "N0W";
                break;
            }
            case 27: {
                pname = "N" + elevation + "V";
                break;
            }
            case 28: {
                pname = "NSP";
                break;
            }
            case 30: {
                pname = "NSW";
                break;
            }
            case 36: {
                pname = "NCO";
                break;
            }
            case 37: {
                pname = "NCR";
                break;
            }
            case 38: {
                pname = "NCZ";
                break;
            }
            case 41: {
                pname = "NET";
                break;
            }
            case 48: {
                pname = "NVW";
            }
            case 56: {
                pname = "N" + elevation + "S";
                break;
            }
            case 57: {
                pname = "NVL";
                break;
            }
            case 65: {
                pname = "NLL";
                break;
            }
            case 66: {
                pname = "NML";
                break;
            }
            case 78: {
                pname = "N1P";
                break;
            }
            case 79: {
                pname = "N3P";
                break;
            }
            case 80: {
                pname = "NTP";
                break;
            }
            case 81: {
                pname = "DPA";
                break;
            }
            case 90: {
                pname = "NHL";
                break;
            }
            case 94: {
                if (elevation == 1) {
                    pname = "NAQ";
                    break;
                }
                if (elevation == 3) {
                    pname = "NBQ";
                    break;
                }
                pname = "N" + elevation / 2 + "Q";
                break;
            }
            case 99: {
                if (elevation == 1) {
                    pname = "NAU";
                    break;
                }
                if (elevation == 3) {
                    pname = "NBU";
                    break;
                }
                pname = "N" + elevation / 2 + "U";
                break;
            }
            case 134: {
                pname = "DVL";
                break;
            }
            case 135: {
                pname = "EET";
                break;
            }
            case 182: {
                pname = "DV";
                break;
            }
            case 181:
            case 187: {
                pname = "R";
                break;
            }
            case 180:
            case 186: {
                pname = "DR";
                break;
            }
            case 183: {
                pname = "V";
                break;
            }
            case 185: {
                pname = "SW";
                break;
            }
        }
        return pname;
    }
    
    static double code_reslookup(final int code) {
        final double[] res = { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 2.0, 4.0, 1.0, 2.0, 4.0, 0.25, 0.5, 1.0, 0.25, 0.5, 1.0, 0.25, 0.0, 1.0, 0.0, 1.0, 0.0, 0.0, 1.0, 4.0, 1.0, 4.0, 0.0, 0.0, 4.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.5, 1.0, 4.0, 0.0, 0.0, 0.0, 0.0, 0.0, 4.0, 4.0, 4.0, 4.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 1.0, 1.0, 4.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 4.0, 4.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.25, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 1.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 150.0, 150.0, 0.0, 0.0, 0.0, 300.0, 0.0, 0.0, 0.0 };
        double data_res;
        if (code < 0 || code > 189) {
            data_res = 0.0;
        }
        else {
            data_res = res[code];
        }
        return data_res;
    }
    
    static int code_levelslookup(final int code) {
        final int[] levels = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8, 8, 8, 16, 16, 16, 8, 8, 8, 16, 16, 16, 8, 0, 8, 0, 256, 0, 0, 8, 8, 16, 16, 0, 0, 16, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 16, 16, 16, 0, 0, 0, 0, 0, 8, 8, 8, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 16, 16, 16, 256, 0, 0, 0, 0, 0, 0, 0, 8, 8, 0, 0, 0, 256, 0, 0, 0, 0, 256, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 256, 199, 0, 0, 256, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 16, 256, 0, 0, 0, 256, 0, 0, 0 };
        int level;
        if (code < 0 || code > 189) {
            level = 0;
        }
        else {
            level = levels[code];
        }
        return level;
    }
    
    static {
        Nidsheader.log = LoggerFactory.getLogger(Nidsheader.class);
    }
    
    class Sinfo
    {
        short divider;
        short id;
        int blockLength;
        short nlayers;
        
        Sinfo(final short divider, final short id, final int length, final short layers) {
            this.divider = divider;
            this.id = id;
            this.blockLength = length;
            this.nlayers = layers;
        }
    }
    
    class Vinfo
    {
        int xt;
        int x0;
        int yt;
        int y0;
        boolean isRadial;
        long hoff;
        long doff;
        boolean isZlibed;
        int[] pos;
        int[] len;
        int code;
        int level;
        
        Vinfo(final int xt, final int x0, final int yt, final int y0, final long hoff, final long doff, final boolean isRadial, final boolean isZ, final int[] pos, final int[] len, final int code, final int level) {
            this.xt = xt;
            this.yt = yt;
            this.x0 = x0;
            this.y0 = y0;
            this.hoff = hoff;
            this.doff = doff;
            this.isRadial = isRadial;
            this.isZlibed = isZ;
            this.pos = pos;
            this.len = len;
            this.code = code;
            this.level = level;
        }
    }
    
    class Pinfo
    {
        short divider;
        short pcode;
        short opmode;
        short sequenceNumber;
        short volumeScanNumber;
        short volumeScanDate;
        short productDate;
        double latitude;
        double longitude;
        double height;
        int volumeScanTime;
        int productTime;
        short p1;
        short p2;
        short p3;
        short p4;
        short p5;
        short p6;
        short p7;
        short p8;
        short p9;
        short p10;
        short elevationNumber;
        short numberOfMaps;
        int offsetToSymbologyBlock;
        int offsetToGraphicBlock;
        int offsetToTabularBlock;
        short[] threshold;
        
        Pinfo() {
        }
        
        Pinfo(final short divider, final double latitude, final double longitude, final double height, final short pcode, final short opmode, final short[] threshold, final short sequenceNumber, final short volumeScanNumber, final short volumeScanDate, final int volumeScanTime, final short productDate, final int productTime, final short p1, final short p2, final short p3, final short p4, final short p5, final short p6, final short p7, final short p8, final short p9, final short p10, final short elevationNumber, final short numberOfMaps, final int offsetToSymbologyBlock, final int offsetToGraphicBlock, final int offsetToTabularBlock) {
            this.divider = divider;
            this.latitude = latitude;
            this.longitude = longitude;
            this.height = height;
            this.pcode = pcode;
            this.opmode = opmode;
            this.sequenceNumber = sequenceNumber;
            this.volumeScanNumber = volumeScanNumber;
            this.volumeScanDate = volumeScanDate;
            this.volumeScanTime = volumeScanTime;
            this.productDate = productDate;
            this.productTime = productTime;
            this.p1 = p1;
            this.p2 = p2;
            this.p3 = p3;
            this.p4 = p4;
            this.p5 = p5;
            this.p6 = p6;
            this.p7 = p7;
            this.p8 = p8;
            this.p9 = p9;
            this.p10 = p10;
            this.threshold = threshold;
            this.elevationNumber = elevationNumber;
            this.numberOfMaps = numberOfMaps;
            this.offsetToSymbologyBlock = offsetToSymbologyBlock;
            this.offsetToGraphicBlock = offsetToGraphicBlock;
            this.offsetToTabularBlock = offsetToTabularBlock;
        }
    }
}
