// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.iosp.nids;

import ucar.ma2.DataType;
import ucar.ma2.ArrayByte;
import java.util.zip.DataFormatException;
import java.util.zip.Inflater;
import ucar.ma2.ArrayStructureBBpos;
import ucar.ma2.ArrayStructureMA;
import ucar.ma2.ArrayStructure;
import ucar.nc2.Structure;
import ucar.ma2.StructureMembers;
import java.util.Date;
import ucar.nc2.units.DateUnit;
import ucar.ma2.InvalidRangeException;
import ucar.ma2.Range;
import java.util.List;
import java.nio.ByteBuffer;
import ucar.ma2.Array;
import ucar.ma2.Section;
import ucar.nc2.Variable;
import java.io.IOException;
import ucar.nc2.util.CancelTask;
import java.util.HashMap;
import ucar.unidata.io.RandomAccessFile;
import ucar.nc2.NetcdfFile;
import ucar.nc2.iosp.AbstractIOServiceProvider;

public class Nidsiosp extends AbstractIOServiceProvider
{
    protected boolean readonly;
    private NetcdfFile ncfile;
    private RandomAccessFile myRaf;
    protected Nidsheader headerParser;
    private int pcode;
    static final int Z_DEFLATED = 8;
    static final int DEF_WBITS = 15;
    protected int fileUsed;
    protected int recStart;
    protected boolean debug;
    protected boolean debugSize;
    protected boolean debugSPIO;
    protected boolean showHeaderBytes;
    protected boolean fill;
    protected HashMap dimHash;
    
    public Nidsiosp() {
        this.fileUsed = 0;
        this.recStart = 0;
        this.debug = false;
        this.debugSize = false;
        this.debugSPIO = false;
        this.showHeaderBytes = false;
        this.dimHash = new HashMap(50);
    }
    
    public boolean isValidFile(final RandomAccessFile raf) {
        final Nidsheader localHeader = new Nidsheader();
        return localHeader.isValidFile(raf);
    }
    
    public String getFileTypeId() {
        return "NEXRAD-3";
    }
    
    public String getFileTypeDescription() {
        return "NEXRAD Level-III Products";
    }
    
    @Override
    public void open(final RandomAccessFile raf, final NetcdfFile file, final CancelTask cancelTask) throws IOException {
        this.ncfile = file;
        this.myRaf = raf;
        (this.headerParser = new Nidsheader()).read(this.myRaf, this.ncfile);
        this.pcode = this.headerParser.pcode;
        this.ncfile.finish();
    }
    
    public Array readNestedData(final Variable v2, final Section section) throws IOException, InvalidRangeException {
        final Variable vp = v2.getParentStructure();
        final List<Range> ranges = section.getRanges();
        final Nidsheader.Vinfo vinfo = (Nidsheader.Vinfo)vp.getSPobject();
        final byte[] vdata = this.headerParser.getUncompData((int)vinfo.doff, 0);
        final ByteBuffer bos = ByteBuffer.wrap(vdata);
        if (vp.getName().startsWith("VADWindSpeed")) {
            return this.readNestedWindBarbData(vp.getShortName(), v2.getShortName(), bos, vinfo, ranges);
        }
        if (vp.getName().startsWith("unlinkedVectorStruct")) {
            return this.readNestedDataUnlinkVector(vp.getShortName(), v2.getShortName(), bos, vinfo, ranges);
        }
        if (vp.getName().equals("linkedVectorStruct")) {
            return this.readNestedLinkedVectorData(vp.getShortName(), v2.getShortName(), bos, vinfo, ranges);
        }
        if (vp.getName().startsWith("textStruct")) {
            return this.readNestedTextStringData(vp.getShortName(), v2.getShortName(), bos, vinfo, ranges);
        }
        if (vp.getName().startsWith("VectorArrow")) {
            return this.readNestedVectorArrowData(vp.getShortName(), v2.getShortName(), bos, vinfo, ranges);
        }
        if (vp.getName().startsWith("circleStruct")) {
            return this.readNestedCircleStructData(vp.getShortName(), v2.getShortName(), bos, vinfo, ranges);
        }
        throw new UnsupportedOperationException("Unknown nested variable " + v2.getName());
    }
    
    public Array readData(final Variable v2, final Section section) throws IOException, InvalidRangeException {
        final List<Range> ranges = section.getRanges();
        final Nidsheader.Vinfo vinfo = (Nidsheader.Vinfo)v2.getSPobject();
        final byte[] vdata = this.headerParser.getUncompData((int)vinfo.doff, 0);
        final ByteBuffer bos = ByteBuffer.wrap(vdata);
        Array outputData;
        if (v2.getName().equals("azimuth")) {
            final Object data = this.readRadialDataAzi(bos, vinfo);
            outputData = Array.factory(v2.getDataType().getPrimitiveClassType(), v2.getShape(), data);
        }
        else if (v2.getName().equals("gate")) {
            final Object data = this.readRadialDataGate(vinfo);
            outputData = Array.factory(v2.getDataType().getPrimitiveClassType(), v2.getShape(), data);
        }
        else if (v2.getName().equals("elevation")) {
            final Object data = this.readRadialDataEle(bos, vinfo);
            outputData = Array.factory(v2.getDataType().getPrimitiveClassType(), v2.getShape(), data);
        }
        else if (v2.getName().equals("latitude")) {
            final double lat = this.ncfile.findGlobalAttribute("RadarLatitude").getNumericValue().doubleValue();
            final Object data = this.readRadialDataLatLonAlt(lat, vinfo);
            outputData = Array.factory(v2.getDataType().getPrimitiveClassType(), v2.getShape(), data);
        }
        else if (v2.getName().equals("longitude")) {
            final double lon = this.ncfile.findGlobalAttribute("RadarLongitude").getNumericValue().doubleValue();
            final Object data = this.readRadialDataLatLonAlt(lon, vinfo);
            outputData = Array.factory(v2.getDataType().getPrimitiveClassType(), v2.getShape(), data);
        }
        else if (v2.getName().equals("altitude")) {
            final double alt = this.ncfile.findGlobalAttribute("RadarAltitude").getNumericValue().doubleValue();
            final Object data = this.readRadialDataLatLonAlt(alt, vinfo);
            outputData = Array.factory(v2.getDataType().getPrimitiveClassType(), v2.getShape(), data);
        }
        else if (v2.getName().equals("distance")) {
            final Object data = this.readDistance(vinfo);
            outputData = Array.factory(v2.getDataType().getPrimitiveClassType(), v2.getShape(), data);
        }
        else if (v2.getName().equals("rays_time")) {
            final String rt = this.ncfile.findGlobalAttribute("DateCreated").getStringValue();
            final Date pDate = DateUnit.getStandardOrISO(rt);
            final double lt = (double)pDate.getTime();
            final double[] dd = new double[vinfo.yt];
            for (int radial = 0; radial < vinfo.yt; ++radial) {
                dd[radial] = (float)lt;
            }
            outputData = Array.factory(v2.getDataType().getPrimitiveClassType(), v2.getShape(), dd);
        }
        else if (v2.getName().startsWith("EchoTop") || v2.getName().startsWith("VertLiquid") || v2.getName().startsWith("BaseReflectivityComp") || v2.getName().startsWith("LayerCompReflect")) {
            final Object data = this.readOneArrayData(bos, vinfo, v2.getName());
            outputData = Array.factory(v2.getDataType().getPrimitiveClassType(), v2.getShape(), data);
        }
        else if (v2.getName().startsWith("PrecipArray")) {
            final Object data = this.readOneArrayData1(bos, vinfo);
            outputData = Array.factory(v2.getDataType().getPrimitiveClassType(), v2.getShape(), data);
        }
        else if (v2.getName().startsWith("Precip") && !vinfo.isRadial) {
            final Object data = this.readOneArrayData(bos, vinfo, v2.getName());
            outputData = Array.factory(v2.getDataType().getPrimitiveClassType(), v2.getShape(), data);
        }
        else {
            if (v2.getName().equals("unlinkedVectorStruct")) {
                return this.readUnlinkedVectorData(v2.getName(), bos, vinfo);
            }
            if (v2.getName().equals("linkedVectorStruct")) {
                return this.readLinkedVectorData(v2.getName(), bos, vinfo);
            }
            if (v2.getName().startsWith("textStruct")) {
                return this.readTextStringData(v2.getName(), bos, vinfo);
            }
            if (v2.getName().startsWith("VADWindSpeed")) {
                return this.readWindBarbData(v2.getName(), bos, vinfo, null);
            }
            if (v2.getName().startsWith("VectorArrow")) {
                return this.readVectorArrowData(v2.getName(), bos, vinfo);
            }
            if (v2.getName().startsWith("TabMessagePage")) {
                final Object data = this.readTabAlphaNumData(bos, vinfo);
                outputData = Array.factory(v2.getDataType().getPrimitiveClassType(), v2.getShape(), data);
            }
            else {
                if (v2.getName().startsWith("circleStruct")) {
                    return this.readCircleStructData(v2.getName(), bos, vinfo);
                }
                if (v2.getName().startsWith("hail") || v2.getName().startsWith("TVS")) {
                    return this.readGraphicSymbolData(v2.getName(), bos, vinfo);
                }
                final Object data = this.readOneScanData(bos, vinfo, v2.getName());
                outputData = Array.factory(v2.getDataType().getPrimitiveClassType(), v2.getShape(), data);
            }
        }
        return outputData.sectionNoReduce(ranges).copy();
    }
    
    public Array readNestedGraphicSymbolData(final String name, final StructureMembers.Member m, final ByteBuffer bos, final Nidsheader.Vinfo vinfo, final List section) throws IOException, InvalidRangeException {
        final int[] pos = vinfo.pos;
        final int size = pos.length;
        final Structure pdata = (Structure)this.ncfile.findVariable(name);
        final ArrayStructure ma = this.readCircleStructData(name, bos, vinfo);
        final short[] pa = new short[size];
        for (int i = 0; i < size; ++i) {
            pa[i] = ma.getScalarShort(i, m);
        }
        final Array ay = Array.factory(Short.TYPE, pdata.getShape(), pa);
        return ay.sectionNoReduce(section);
    }
    
    public ArrayStructure readGraphicSymbolData(final String name, final ByteBuffer bos, final Nidsheader.Vinfo vinfo) throws IOException, InvalidRangeException {
        final int[] pos = vinfo.pos;
        final int[] sizes = vinfo.len;
        final int size = pos.length;
        final Structure pdata = (Structure)this.ncfile.findVariable(name);
        final StructureMembers members = pdata.makeStructureMembers();
        members.findMember("x_start").setDataParam(0);
        members.findMember("y_start").setDataParam(2);
        return new MyArrayStructureBBpos(members, new int[] { size }, bos, pos, sizes);
    }
    
    public Array readNestedLinkedVectorData(final String name, final String memberName, final ByteBuffer bos, final Nidsheader.Vinfo vinfo, final List section) throws IOException, InvalidRangeException {
        final Structure pdata = (Structure)this.ncfile.findVariable(name);
        final ArrayStructure ma = this.readLinkedVectorData(name, bos, vinfo);
        final int size = (int)pdata.getSize();
        final StructureMembers members = ma.getStructureMembers();
        final StructureMembers.Member m = members.findMember(memberName);
        final short[] pa = new short[size];
        for (int i = 0; i < size; ++i) {
            pa[i] = ma.getScalarShort(i, m);
        }
        final Array ay = Array.factory(Short.TYPE, pdata.getShape(), pa);
        return ay.sectionNoReduce(section);
    }
    
    public ArrayStructure readLinkedVectorData(final String name, final ByteBuffer bos, final Nidsheader.Vinfo vinfo) throws IOException, InvalidRangeException {
        final int[] pos = vinfo.pos;
        final int[] dlen = vinfo.len;
        bos.position(0);
        final int size = pos.length;
        int vlen = 0;
        for (int i = 0; i < size; ++i) {
            vlen += dlen[i];
        }
        final Structure pdata = (Structure)this.ncfile.findVariable(name);
        final StructureMembers members = pdata.makeStructureMembers();
        short sValue = 0;
        int ii = 0;
        final short[][] sArray = new short[5][vlen];
        for (int j = 0; j < size; ++j) {
            bos.position(pos[j]);
            if (vinfo.code == 9) {
                sValue = bos.getShort();
            }
            final short istart = bos.getShort();
            final short jstart = bos.getShort();
            for (int k = 0; k < dlen[j]; ++k) {
                final short iend = bos.getShort();
                final short jend = bos.getShort();
                if (vinfo.code == 9) {
                    sArray[0][ii] = sValue;
                }
                sArray[1][ii] = istart;
                sArray[2][ii] = jstart;
                sArray[3][ii] = iend;
                sArray[4][ii] = jend;
                ++ii;
            }
        }
        final ArrayStructureMA asma = new ArrayStructureMA(members, new int[] { vlen });
        Array data = Array.factory(Short.TYPE, new int[] { vlen }, sArray[0]);
        StructureMembers.Member m = members.findMember("sValue");
        if (m != null) {
            m.setDataArray(data);
        }
        data = Array.factory(Short.TYPE, new int[] { vlen }, sArray[1]);
        m = members.findMember("x_start");
        m.setDataArray(data);
        data = Array.factory(Short.TYPE, new int[] { vlen }, sArray[2]);
        m = members.findMember("y_start");
        m.setDataArray(data);
        data = Array.factory(Short.TYPE, new int[] { vlen }, sArray[3]);
        m = members.findMember("x_end");
        m.setDataArray(data);
        data = Array.factory(Short.TYPE, new int[] { vlen }, sArray[4]);
        m = members.findMember("y_end");
        m.setDataArray(data);
        return asma;
    }
    
    public Array readNestedCircleStructData(final String name, final String memberName, final ByteBuffer bos, final Nidsheader.Vinfo vinfo, final List section) throws IOException, InvalidRangeException {
        final Structure pdata = (Structure)this.ncfile.findVariable(name);
        final ArrayStructure ma = this.readCircleStructData(name, bos, vinfo);
        final int size = (int)pdata.getSize();
        final StructureMembers members = ma.getStructureMembers();
        final StructureMembers.Member m = members.findMember(memberName);
        final short[] pa = new short[size];
        for (int i = 0; i < size; ++i) {
            pa[i] = ma.getScalarShort(i, m);
        }
        final Array ay = Array.factory(Short.TYPE, pdata.getShape(), pa);
        return ay.sectionNoReduce(section);
    }
    
    public ArrayStructure readCircleStructData(final String name, final ByteBuffer bos, final Nidsheader.Vinfo vinfo) throws IOException, InvalidRangeException {
        final int[] pos = vinfo.pos;
        final int size = pos.length;
        final Structure pdata = (Structure)this.ncfile.findVariable(name);
        final int recsize = pos[1] - pos[0];
        for (int i = 1; i < size; ++i) {
            final int r = pos[i] - pos[i - 1];
            if (r != recsize) {
                System.out.println(" PROBLEM at " + i + " == " + r);
            }
        }
        final StructureMembers members = pdata.makeStructureMembers();
        members.findMember("x_center").setDataParam(0);
        members.findMember("y_center").setDataParam(2);
        members.findMember("radius").setDataParam(4);
        members.setStructureSize(recsize);
        return new ArrayStructureBBpos(members, new int[] { size }, bos, pos);
    }
    
    public Object readTabAlphaNumData(final ByteBuffer bos, final Nidsheader.Vinfo vinfo) throws IOException, InvalidRangeException {
        final int plen = vinfo.xt;
        final int tablen = vinfo.yt;
        final String[] pdata = new String[plen];
        bos.position(0);
        int ipage = 0;
        int icnt = 4;
        StringBuilder sbuf = new StringBuilder();
        while (ipage < plen && tablen > 128 + icnt) {
            final int llen = bos.getShort();
            if (llen == -1) {
                pdata[ipage] = new String(sbuf);
                sbuf = new StringBuilder();
                ++ipage;
                icnt += 2;
            }
            else {
                final byte[] b = new byte[llen];
                bos.get(b);
                final String sl = new String(b) + "\n";
                sbuf.append(sl);
                icnt = icnt + llen + 2;
            }
        }
        return pdata;
    }
    
    public Object readOneScanData(final ByteBuffer bos, final Nidsheader.Vinfo vinfo, final String vName) throws IOException, InvalidRangeException {
        int doff = 0;
        final int npixel = vinfo.yt * vinfo.xt;
        final byte[] odata = new byte[vinfo.xt];
        final byte[] pdata = new byte[npixel];
        bos.position(0);
        for (int radial = 0; radial < vinfo.yt; ++radial) {
            final int runLen = bos.getShort();
            doff += 2;
            if (vinfo.isRadial) {
                final int radialAngle = bos.getShort();
                doff += 2;
                final int radialAngleD = bos.getShort();
                doff += 2;
            }
            byte[] rdata = null;
            byte[] bdata = null;
            if (vinfo.xt != runLen) {
                rdata = new byte[runLen * 2];
                bos.get(rdata, 0, runLen * 2);
                doff += runLen * 2;
                bdata = this.readOneBeamData(rdata, runLen, vinfo.xt, vinfo.level);
            }
            else {
                rdata = new byte[runLen];
                bos.get(rdata, 0, runLen);
                doff += runLen;
                bdata = rdata.clone();
            }
            if (vinfo.x0 > 0) {
                for (int i = 0; i < vinfo.x0; ++i) {
                    odata[i] = 0;
                }
            }
            System.arraycopy(bdata, 0, odata, vinfo.x0, bdata.length);
            System.arraycopy(odata, 0, pdata, vinfo.xt * radial, vinfo.xt);
        }
        final int offset = 0;
        if (vName.endsWith("_RAW")) {
            return pdata;
        }
        if (vName.startsWith("BaseReflectivity") || vName.startsWith("BaseVelocity")) {
            final int[] levels = vinfo.len;
            final int iscale = vinfo.code;
            final float[] fdata = new float[npixel];
            for (int i = 0; i < npixel; ++i) {
                final int ival = levels[unsignedByteToInt(pdata[i])];
                if (ival > -9997 && ival != -9866) {
                    fdata[i] = ival / (float)iscale + offset;
                }
                else {
                    fdata[i] = Float.NaN;
                }
            }
            return fdata;
        }
        if (vName.startsWith("DigitalHybridReflectivity")) {
            final int[] levels = vinfo.len;
            final int iscale = vinfo.code;
            final float[] fdata = new float[npixel];
            for (int i = 0; i < npixel; ++i) {
                final int ival = levels[unsignedByteToInt(pdata[i])];
                if (ival != levels[0] && ival != levels[1]) {
                    fdata[i] = ival / (float)iscale + offset;
                }
                else {
                    fdata[i] = Float.NaN;
                }
            }
            return fdata;
        }
        if (vName.startsWith("RadialVelocity") || vName.startsWith("SpectrumWidth")) {
            final int[] levels = vinfo.len;
            final int iscale = vinfo.code;
            final float[] fdata = new float[npixel];
            for (int i = 0; i < npixel; ++i) {
                final int ival = levels[this.convertunsignedByte2Short(pdata[i])];
                if (ival > -9996 && ival != -9866) {
                    fdata[i] = ival / (float)iscale + offset;
                }
                else {
                    fdata[i] = Float.NaN;
                }
            }
            return fdata;
        }
        if (vName.startsWith("StormMeanVelocity")) {
            final int[] levels = vinfo.len;
            final int iscale = vinfo.code;
            final float[] fdata = new float[npixel];
            for (int i = 0; i < npixel; ++i) {
                final int ival = levels[pdata[i]];
                if (ival > -9996) {
                    fdata[i] = ival / (float)iscale + offset;
                }
                else {
                    fdata[i] = Float.NaN;
                }
            }
            return fdata;
        }
        if (vName.startsWith("Precip") || vName.startsWith("DigitalPrecip")) {
            final int[] levels = vinfo.len;
            final int iscale = vinfo.code;
            final float[] fdata = new float[npixel];
            for (int i = 0; i < npixel; ++i) {
                int ival;
                if (pdata[i] < 0) {
                    ival = -9997;
                }
                else {
                    ival = levels[pdata[i]];
                }
                if (ival > -9996) {
                    fdata[i] = ival / (float)iscale + offset;
                }
                else {
                    fdata[i] = Float.NaN;
                }
            }
            return fdata;
        }
        if (vName.startsWith("EnhancedEchoTop")) {
            final int[] levels = vinfo.len;
            final int iscale = vinfo.code;
            final float[] fdata = new float[npixel];
            for (int i = 0; i < npixel; ++i) {
                final int ival = unsignedByteToInt(pdata[i]);
                if (ival == 0 && ival == 1) {
                    fdata[i] = Float.NaN;
                }
                else {
                    fdata[i] = (ival & levels[0]) / (float)levels[1] - levels[2];
                }
            }
            return fdata;
        }
        if (vName.startsWith("DigitalIntegLiquid")) {
            final int[] levels = vinfo.len;
            final int iscale = vinfo.code;
            final float[] fdata = new float[npixel];
            final float a = this.getHexDecodeValue((short)levels[0]);
            final float b = this.getHexDecodeValue((short)levels[1]);
            final float c = this.getHexDecodeValue((short)levels[3]);
            final float d = this.getHexDecodeValue((short)levels[4]);
            for (int j = 0; j < npixel; ++j) {
                final int ival2 = unsignedByteToInt(pdata[j]);
                if (ival2 == 0 || ival2 == 1) {
                    fdata[j] = Float.NaN;
                }
                else if (ival2 < 20) {
                    fdata[j] = (ival2 - b) / a;
                }
                else {
                    final float t = (ival2 - d) / c;
                    fdata[j] = (float)Math.exp(t);
                }
            }
            return fdata;
        }
        return null;
    }
    
    public float getHexDecodeValue(final short val) {
        final int s = val >> 15 & 0x1;
        final int e = val >> 10 & 0x1F;
        final int f = val & 0x3FF;
        float deco;
        if (e == 0) {
            deco = (float)Math.pow(-1.0, s) * 2.0f * (0.0f + f / 1024.0f);
        }
        else {
            deco = (float)(Math.pow(-1.0, s) * Math.pow(2.0, e - 16) * (1.0f + f / 1024.0f));
        }
        return deco;
    }
    
    public byte[] readOneBeamData(final byte[] ddata, final int rLen, final int xt, final int level) throws IOException, InvalidRangeException {
        final byte[] bdata = new byte[xt];
        int nbin = 0;
        int total = 0;
        for (int run = 0; run < rLen * 2; ++run) {
            final int drun = this.convertunsignedByte2Short(ddata[run]) >> 4;
            final byte dcode1 = (byte)(this.convertunsignedByte2Short(ddata[run]) & 0xF);
            for (int i = 0; i < drun; ++i) {
                bdata[nbin++] = dcode1;
                ++total;
            }
        }
        if (total < xt) {
            for (int run = total; run < xt; ++run) {
                bdata[run] = 0;
            }
        }
        return bdata;
    }
    
    public short[] readOneBeamShortData(final byte[] ddata, final int rLen, final int xt, final int level) throws IOException, InvalidRangeException {
        final short[] sdata = new short[xt];
        int total = 0;
        for (int run = 0; run < rLen; ++run) {
            final short dcode1 = this.convertunsignedByte2Short(ddata[run]);
            sdata[run] = dcode1;
            ++total;
        }
        if (total < xt) {
            for (int run = total; run < xt; ++run) {
                sdata[run] = 0;
            }
        }
        return sdata;
    }
    
    public Array readNestedWindBarbData(final String name, final String memberName, final ByteBuffer bos, final Nidsheader.Vinfo vinfo, final List section) throws IOException, InvalidRangeException {
        final Structure pdata = (Structure)this.ncfile.findVariable(name);
        final ArrayStructure ma = this.readWindBarbData(name, bos, vinfo, null);
        final int size = (int)pdata.getSize();
        final StructureMembers members = ma.getStructureMembers();
        final StructureMembers.Member m = members.findMember(memberName);
        final short[] pa = new short[size];
        for (int i = 0; i < size; ++i) {
            pa[i] = ma.getScalarShort(i, m);
        }
        final Array ay = Array.factory(Short.TYPE, pdata.getShape(), pa);
        return ay.sectionNoReduce(section);
    }
    
    public ArrayStructure readWindBarbData(final String name, final ByteBuffer bos, final Nidsheader.Vinfo vinfo, final List sList) throws IOException, InvalidRangeException {
        final int[] pos = vinfo.pos;
        final int size = pos.length;
        final Structure pdata = (Structure)this.ncfile.findVariable(name);
        int recsize;
        if (size > 1) {
            recsize = pos[1] - pos[0];
            for (int i = 1; i < size; ++i) {
                final int r = pos[i] - pos[i - 1];
                if (r != recsize) {
                    System.out.println(" PROBLEM at " + i + " == " + r);
                }
            }
        }
        else {
            recsize = 1;
        }
        final StructureMembers members = pdata.makeStructureMembers();
        members.findMember("value").setDataParam(0);
        members.findMember("x_start").setDataParam(2);
        members.findMember("y_start").setDataParam(4);
        members.findMember("direction").setDataParam(6);
        members.findMember("speed").setDataParam(8);
        members.setStructureSize(recsize);
        final ArrayStructure ay = new ArrayStructureBBpos(members, new int[] { size }, bos, pos);
        return (ArrayStructure)((sList != null) ? ay.sectionNoReduce(sList) : ay);
    }
    
    public Array readNestedVectorArrowData(final String name, final String memberName, final ByteBuffer bos, final Nidsheader.Vinfo vinfo, final List section) throws IOException, InvalidRangeException {
        final Structure pdata = (Structure)this.ncfile.findVariable(name);
        final ArrayStructure ma = this.readVectorArrowData(name, bos, vinfo);
        final int size = (int)pdata.getSize();
        final StructureMembers members = ma.getStructureMembers();
        final StructureMembers.Member m = members.findMember(memberName);
        final short[] pa = new short[size];
        for (int i = 0; i < size; ++i) {
            pa[i] = ma.getScalarShort(i, m);
        }
        final Array ay = Array.factory(Short.TYPE, pdata.getShape(), pa);
        return ay.sectionNoReduce(section);
    }
    
    public ArrayStructure readVectorArrowData(final String name, final ByteBuffer bos, final Nidsheader.Vinfo vinfo) throws IOException, InvalidRangeException {
        final int[] pos = vinfo.pos;
        final int size = pos.length;
        final Structure pdata = (Structure)this.ncfile.findVariable(name);
        final int recsize = pos[1] - pos[0];
        for (int i = 1; i < size; ++i) {
            final int r = pos[i] - pos[i - 1];
            if (r != recsize) {
                System.out.println(" PROBLEM at " + i + " == " + r);
            }
        }
        final StructureMembers members = pdata.makeStructureMembers();
        members.findMember("x_start").setDataParam(0);
        members.findMember("y_start").setDataParam(2);
        members.findMember("direction").setDataParam(4);
        members.findMember("arrowLength").setDataParam(6);
        members.findMember("arrowHeadLength").setDataParam(8);
        members.setStructureSize(recsize);
        return new ArrayStructureBBpos(members, new int[] { size }, bos, pos);
    }
    
    public Array readNestedTextStringData(final String name, final String memberName, final ByteBuffer bos, final Nidsheader.Vinfo vinfo, final List section) throws IOException, InvalidRangeException {
        final Structure pdata = (Structure)this.ncfile.findVariable(name);
        final ArrayStructure ma = this.readTextStringData(name, bos, vinfo);
        final int size = (int)pdata.getSize();
        final StructureMembers members = ma.getStructureMembers();
        final StructureMembers.Member m = members.findMember(memberName);
        final short[] pa = new short[size];
        final String[] ps = new String[size];
        Array ay;
        if (m.getName().equalsIgnoreCase("testString")) {
            for (int i = 0; i < size; ++i) {
                ps[i] = ma.getScalarString(i, m);
            }
            ay = Array.factory(String.class, pdata.getShape(), ps);
        }
        else {
            for (int i = 0; i < size; ++i) {
                pa[i] = ma.getScalarShort(i, m);
            }
            ay = Array.factory(Short.TYPE, pdata.getShape(), pa);
        }
        return ay.sectionNoReduce(section);
    }
    
    public ArrayStructure readTextStringData(final String name, final ByteBuffer bos, final Nidsheader.Vinfo vinfo) throws IOException, InvalidRangeException {
        final int[] pos = vinfo.pos;
        final int[] sizes = vinfo.len;
        final int size = pos.length;
        final Structure pdata = (Structure)this.ncfile.findVariable(name);
        final StructureMembers members = pdata.makeStructureMembers();
        if (vinfo.code == 8) {
            members.findMember("strValue").setDataParam(0);
            members.findMember("x_start").setDataParam(2);
            members.findMember("y_start").setDataParam(4);
            members.findMember("textString").setDataParam(6);
        }
        else {
            members.findMember("x_start").setDataParam(0);
            members.findMember("y_start").setDataParam(2);
            members.findMember("textString").setDataParam(4);
        }
        return new MyArrayStructureBBpos(members, new int[] { size }, bos, pos, sizes);
    }
    
    public Array readNestedDataUnlinkVector(final String name, final String memberName, final ByteBuffer bos, final Nidsheader.Vinfo vinfo, final List section) throws IOException, InvalidRangeException {
        final Structure pdata = (Structure)this.ncfile.findVariable(name);
        final ArrayStructure ma = this.readUnlinkedVectorData(name, bos, vinfo);
        final int size = (int)pdata.getSize();
        final StructureMembers members = ma.getStructureMembers();
        final StructureMembers.Member m = members.findMember(memberName);
        final short[] pa = new short[size];
        for (int i = 0; i < size; ++i) {
            pa[i] = ma.getScalarShort(i, m);
        }
        final Array ay = Array.factory(Short.TYPE, pdata.getShape(), pa);
        return ay.sectionNoReduce(section);
    }
    
    public ArrayStructure readUnlinkedVectorData(final String name, final ByteBuffer bos, final Nidsheader.Vinfo vinfo) throws IOException, InvalidRangeException {
        final int[] pos = vinfo.pos;
        final int[] dlen = vinfo.len;
        bos.position(0);
        final int size = pos.length;
        int vlen = 0;
        for (int i = 0; i < size; ++i) {
            vlen += dlen[i];
        }
        final Structure pdata = (Structure)this.ncfile.findVariable(name);
        final StructureMembers members = pdata.makeStructureMembers();
        final ArrayStructureMA asma = new ArrayStructureMA(members, new int[] { vlen });
        int ii = 0;
        final short[][] sArray = new short[5][vlen];
        for (int j = 0; j < size; ++j) {
            bos.position(pos[j]);
            final short vlevel = bos.getShort();
            for (int k = 0; k < dlen[j]; ++k) {
                final short istart = bos.getShort();
                final short jstart = bos.getShort();
                final short iend = bos.getShort();
                final short jend = bos.getShort();
                sArray[0][ii] = vlevel;
                sArray[1][ii] = istart;
                sArray[2][ii] = jstart;
                sArray[3][ii] = iend;
                sArray[4][ii] = jend;
                ++ii;
            }
        }
        Array data = Array.factory(Short.TYPE, new int[] { vlen }, sArray[0]);
        StructureMembers.Member m = members.findMember("iValue");
        m.setDataArray(data);
        data = Array.factory(Short.TYPE, new int[] { vlen }, sArray[1]);
        m = members.findMember("x_start");
        m.setDataArray(data);
        data = Array.factory(Short.TYPE, new int[] { vlen }, sArray[2]);
        m = members.findMember("y_start");
        m.setDataArray(data);
        data = Array.factory(Short.TYPE, new int[] { vlen }, sArray[3]);
        m = members.findMember("x_end");
        m.setDataArray(data);
        data = Array.factory(Short.TYPE, new int[] { vlen }, sArray[4]);
        m = members.findMember("y_end");
        m.setDataArray(data);
        return asma;
    }
    
    public Object readOneArrayData(final ByteBuffer bos, final Nidsheader.Vinfo vinfo, final String vName) throws IOException, InvalidRangeException {
        int doff = 0;
        final int offset = 0;
        final byte[] pdata = new byte[vinfo.yt * vinfo.xt];
        final byte[] b2 = new byte[2];
        final int npixel = vinfo.yt * vinfo.xt;
        bos.position(0);
        for (int radial = 0; radial < vinfo.yt; ++radial) {
            bos.get(b2);
            final int runLen = this.getUInt(b2, 0, 2);
            doff += 2;
            final byte[] rdata = new byte[runLen];
            final int tmpp = bos.remaining();
            bos.get(rdata, 0, runLen);
            doff += runLen;
            final byte[] bdata = this.readOneRowData(rdata, runLen, vinfo.xt);
            System.arraycopy(bdata, 0, pdata, vinfo.xt * radial, vinfo.xt);
        }
        if (vName.endsWith("_RAW")) {
            return pdata;
        }
        if (vName.equals("EchoTop") || vName.equals("VertLiquid") || vName.startsWith("Precip")) {
            final int[] levels = vinfo.len;
            final int iscale = vinfo.code;
            final float[] fdata = new float[npixel];
            for (int i = 0; i < npixel; ++i) {
                final int ival = levels[pdata[i]];
                if (ival > -9996) {
                    fdata[i] = ival / (float)iscale + offset;
                }
                else {
                    fdata[i] = Float.NaN;
                }
            }
            return fdata;
        }
        if (vName.startsWith("BaseReflectivityComp") || vName.startsWith("LayerCompReflect")) {
            final int[] levels = vinfo.len;
            final int iscale = vinfo.code;
            final float[] fdata = new float[npixel];
            for (int i = 0; i < npixel; ++i) {
                final int ival = levels[pdata[i]];
                if (ival > -9997) {
                    fdata[i] = ival / (float)iscale + offset;
                }
                else {
                    fdata[i] = Float.NaN;
                }
            }
            return fdata;
        }
        return null;
    }
    
    public Object readOneArrayData1(final ByteBuffer bos, final Nidsheader.Vinfo vinfo) throws IOException, InvalidRangeException {
        int doff = 0;
        final short[] pdata = new short[vinfo.yt * vinfo.xt];
        bos.position(0);
        for (int row = 0; row < vinfo.yt; ++row) {
            final int runLen = bos.getShort();
            doff += 2;
            final byte[] rdata = new byte[runLen];
            final int tmpp = bos.remaining();
            bos.get(rdata, 0, runLen);
            doff += runLen;
            short[] bdata;
            if (vinfo.code == 17) {
                bdata = this.readOneRowData1(rdata, runLen, vinfo.xt);
            }
            else {
                bdata = this.readOneRowData2(rdata, runLen, vinfo.xt);
            }
            System.arraycopy(bdata, 0, pdata, vinfo.xt * row, vinfo.xt);
        }
        return pdata;
    }
    
    public short[] readOneRowData1(final byte[] ddata, final int rLen, final int xt) throws IOException, InvalidRangeException {
        final short[] bdata = new short[xt];
        int nbin = 0;
        int total = 0;
        for (int run = 0; run < rLen - 1; ++run) {
            final int drun = this.convertunsignedByte2Short(ddata[run]);
            ++run;
            final short dcode1 = this.convertunsignedByte2Short(ddata[run]);
            for (int i = 0; i < drun; ++i) {
                bdata[nbin++] = dcode1;
                ++total;
            }
        }
        if (total < xt) {
            for (int run = total; run < xt; ++run) {
                bdata[run] = 0;
            }
        }
        return bdata;
    }
    
    public short[] readOneRowData2(final byte[] ddata, final int rLen, final int xt) throws IOException, InvalidRangeException {
        final short[] bdata = new short[xt];
        int nbin = 0;
        int total = 0;
        for (int run = 0; run < rLen; ++run) {
            final int drun = this.convertunsignedByte2Short(ddata[run]) >> 4;
            final short dcode1 = (short)(this.convertunsignedByte2Short(ddata[run]) & 0xF);
            for (int i = 0; i < drun; ++i) {
                bdata[nbin++] = dcode1;
                ++total;
            }
        }
        if (total < xt) {
            for (int run = total; run < xt; ++run) {
                bdata[run] = 0;
            }
        }
        return bdata;
    }
    
    public byte[] readOneRowData(final byte[] ddata, final int rLen, final int xt) throws IOException, InvalidRangeException {
        final byte[] bdata = new byte[xt];
        int nbin = 0;
        int total = 0;
        for (int run = 0; run < rLen; ++run) {
            final int drun = this.convertunsignedByte2Short(ddata[run]) >> 4;
            final byte dcode1 = (byte)(this.convertunsignedByte2Short(ddata[run]) & 0xF);
            for (int i = 0; i < drun; ++i) {
                bdata[nbin++] = dcode1;
                ++total;
            }
        }
        if (total < xt) {
            for (int run = total; run < xt; ++run) {
                bdata[run] = 0;
            }
        }
        return bdata;
    }
    
    public Object readRadialDataEle(final ByteBuffer bos, final Nidsheader.Vinfo vinfo) throws IOException, InvalidRangeException {
        final float[] elvdata = new float[vinfo.yt];
        final float elvAngle = vinfo.y0 * 0.1f;
        for (int radial = 0; radial < vinfo.yt; ++radial) {
            elvdata[radial] = elvAngle;
        }
        return elvdata;
    }
    
    public Object readRadialDataLatLonAlt(final double t, final Nidsheader.Vinfo vinfo) throws IOException, InvalidRangeException {
        final float[] vdata = new float[vinfo.yt];
        for (int radial = 0; radial < vinfo.yt; ++radial) {
            vdata[radial] = (float)t;
        }
        return vdata;
    }
    
    public Object readRadialDataAzi(final ByteBuffer bos, final Nidsheader.Vinfo vinfo) throws IOException, InvalidRangeException {
        int doff = 0;
        final float[] azidata = new float[vinfo.yt];
        for (int radial = 0; radial < vinfo.yt; ++radial) {
            final int runLen = bos.getShort();
            doff += 2;
            final float radialAngle = bos.getShort() / 10.0f;
            doff += 2;
            final int radialAngleD = bos.getShort();
            doff += 2;
            if (vinfo.xt != runLen) {
                doff += runLen * 2;
            }
            else {
                doff += runLen;
            }
            bos.position(doff);
            final Float ra = new Float(radialAngle);
            azidata[radial] = ra;
        }
        return azidata;
    }
    
    public Object readDistance(final Nidsheader.Vinfo vinfo) throws IOException, InvalidRangeException {
        final int[] data = new int[vinfo.yt * vinfo.xt];
        for (int row = 0; row < vinfo.yt; ++row) {
            for (int col = 0; col < vinfo.xt; ++col) {
                final int i = row * vinfo.yt + col;
                data[i] = col + vinfo.x0;
            }
        }
        return data;
    }
    
    public Object readRadialDataGate(final Nidsheader.Vinfo vinfo) throws IOException, InvalidRangeException {
        final float[] gatedata = new float[vinfo.xt];
        final double ddg = Nidsheader.code_reslookup(this.pcode);
        final float sc = vinfo.y0 * 1.0f;
        for (int rad = 0; rad < vinfo.xt; ++rad) {
            gatedata[rad] = (vinfo.x0 + rad) * sc * (float)ddg;
        }
        return gatedata;
    }
    
    public byte[] readCompData1(final byte[] uncomp, final long hoff, final long doff) throws IOException {
        final byte b1 = uncomp[0];
        final byte b2 = uncomp[1];
        int off = 2 * (((b1 & 0x3F) << 8) + b2);
        for (int i = 0; i < 2; ++i) {
            while (off < uncomp.length && uncomp[off] != 10) {
                ++off;
            }
            ++off;
        }
        final byte[] data = new byte[(int)(uncomp.length - off - doff)];
        System.arraycopy(uncomp, off + (int)doff, data, 0, uncomp.length - off - (int)doff);
        return data;
    }
    
    public byte[] readCompData(final long hoff, final long doff) throws IOException {
        final long pos = 0L;
        final long len = this.myRaf.length();
        this.myRaf.seek(pos);
        final int numin = (int)(len - hoff);
        final byte[] b = new byte[(int)len];
        this.myRaf.readFully(b);
        final Inflater inf = new Inflater(false);
        int result = 0;
        int uncompLen = 24500;
        byte[] uncomp = new byte[uncompLen];
        inf.setInput(b, (int)hoff, numin - 4);
        final int limit = 20000;
        while (inf.getRemaining() > 0) {
            int resultLength;
            try {
                resultLength = inf.inflate(uncomp, result, 4000);
            }
            catch (DataFormatException ex) {
                System.out.println("ERROR on inflation " + ex.getMessage());
                ex.printStackTrace();
                throw new IOException(ex.getMessage());
            }
            result += resultLength;
            if (result > limit) {
                final byte[] tmp = new byte[result];
                System.arraycopy(uncomp, 0, tmp, 0, result);
                uncompLen += 10000;
                uncomp = new byte[uncompLen];
                System.arraycopy(tmp, 0, uncomp, 0, result);
            }
            if (resultLength == 0) {
                final int tt = inf.getRemaining();
                final byte[] b2 = new byte[2];
                System.arraycopy(b, (int)hoff + numin - 4 - tt, b2, 0, 2);
                if (this.headerParser.isZlibHed(b2) == 0) {
                    System.arraycopy(b, (int)hoff + numin - 4 - tt, uncomp, result, tt);
                    result += tt;
                    break;
                }
                inf.reset();
                inf.setInput(b, (int)hoff + numin - 4 - tt, tt);
            }
        }
        inf.end();
        final byte b3 = uncomp[0];
        final byte b4 = uncomp[1];
        int off = 2 * (((b3 & 0x3F) << 8) + b4);
        for (int i = 0; i < 2; ++i) {
            while (off < result && uncomp[off] != 10) {
                ++off;
            }
            ++off;
        }
        final byte[] data = new byte[(int)(result - off - doff)];
        System.arraycopy(uncomp, off + (int)doff, data, 0, result - off - (int)doff);
        return data;
    }
    
    public byte[] readUCompData(final long hoff, final long doff) throws IOException {
        final long pos = 0L;
        final long len = this.myRaf.length();
        this.myRaf.seek(pos);
        final int numin = (int)(len - hoff);
        final byte[] b = new byte[(int)len];
        this.myRaf.readFully(b);
        final byte[] ucomp = new byte[numin - 4];
        System.arraycopy(b, (int)hoff, ucomp, 0, numin - 4);
        final byte[] data = new byte[(int)(ucomp.length - doff)];
        System.arraycopy(ucomp, (int)doff, data, 0, ucomp.length - (int)doff);
        return data;
    }
    
    int issZlibed(final byte[] buf) {
        if ((buf[0] & 0xF) == 0x8 && (buf[0] >> 4) + 8 <= 15 && ((buf[0] << 8) + buf[1]) % 31 == 0) {
            return 1;
        }
        return 0;
    }
    
    int getUInt(final byte[] b, final int offset, final int num) {
        int base = 1;
        int word = 0;
        final int[] bv = new int[num];
        for (int i = 0; i < num; ++i) {
            bv[i] = this.convertunsignedByte2Short(b[offset + i]);
        }
        for (int i = num - 1; i >= 0; --i) {
            word += base * bv[i];
            base *= 256;
        }
        return word;
    }
    
    int getInt(final byte[] b, final int offset, final int num) {
        int base = 1;
        int word = 0;
        final int[] bv = new int[num];
        for (int i = 0; i < num; ++i) {
            bv[i] = this.convertunsignedByte2Short(b[offset + i]);
        }
        if (bv[0] > 127) {
            final int[] array = bv;
            final int n = 0;
            array[n] -= 128;
            base = -1;
        }
        for (int i = num - 1; i >= 0; --i) {
            word += base * bv[i];
            base *= 256;
        }
        return word;
    }
    
    public short convertunsignedByte2Short(final byte b) {
        return (short)((b < 0) ? (b + 256) : ((short)b));
    }
    
    public static int unsignedByteToInt(final byte b) {
        return b & 0xFF;
    }
    
    public void flush() throws IOException {
        this.myRaf.flush();
    }
    
    @Override
    public void close() throws IOException {
        this.myRaf.close();
    }
    
    public static void main(final String[] args) throws Exception, IOException, InstantiationException, IllegalAccessException {
        final String fileIn = "/home/yuanho/NIDS/N0R_20041102_2111";
        NetcdfFile.registerIOProvider(Nidsiosp.class);
        final NetcdfFile ncf = NetcdfFile.open(fileIn);
        final Variable v = ncf.findVariable("BaseReflectivity");
        final int[] origin = { 0, 0 };
        final int[] shape = { 300, 36 };
        final ArrayByte data = (ArrayByte)v.read(origin, shape);
        ncf.close();
    }
    
    private class MyArrayStructureBBpos extends ArrayStructureBBpos
    {
        int[] size;
        
        MyArrayStructureBBpos(final StructureMembers members, final int[] shape, final ByteBuffer bbuffer, final int[] positions, final int[] size) {
            super(members, shape, bbuffer, positions);
            this.size = size;
        }
        
        @Override
        public String getScalarString(final int recnum, final StructureMembers.Member m) {
            if (m.getDataType() == DataType.CHAR || m.getDataType() == DataType.STRING) {
                final int offset = this.calcOffsetSetOrder(recnum, m);
                final int count = this.size[recnum];
                final byte[] pa = new byte[count];
                int i;
                for (i = 0; i < count; ++i) {
                    pa[i] = this.bbuffer.get(offset + i);
                    if (0 == pa[i]) {
                        break;
                    }
                }
                return new String(pa, 0, i);
            }
            throw new IllegalArgumentException("Type is " + m.getDataType() + ", must be String or char");
        }
    }
}
