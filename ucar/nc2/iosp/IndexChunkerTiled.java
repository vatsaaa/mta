// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.iosp;

import ucar.ma2.InvalidRangeException;
import ucar.ma2.Range;
import java.util.ArrayList;
import ucar.ma2.Section;
import ucar.ma2.Index;
import java.util.List;

public class IndexChunkerTiled
{
    private List<Dim> dimList;
    private IndexLong dataIndex;
    private Index resultIndex;
    private IndexChunker.Chunk chunk;
    private int nelems;
    private long total;
    private long done;
    private int startDestElem;
    private int startSrcElem;
    private boolean debug;
    private boolean debugMerge;
    private boolean debugDetail;
    private boolean debugNext;
    private boolean debugStartingElems;
    
    public IndexChunkerTiled(final Section dataSection, final Section wantSection) throws InvalidRangeException {
        this.dimList = new ArrayList<Dim>();
        this.debug = false;
        this.debugMerge = false;
        this.debugDetail = false;
        this.debugNext = false;
        this.debugStartingElems = false;
        this.done = 0L;
        final Section intersect = dataSection.intersect(wantSection);
        this.total = intersect.computeSize();
        if (this.total <= 0L) {
            System.out.println("hey");
        }
        assert this.total > 0L;
        final int varRank = intersect.getRank();
        int wantStride = 1;
        int dataStride = 1;
        for (int ii = varRank - 1; ii >= 0; --ii) {
            final Range dr = dataSection.getRange(ii);
            final Range wr = wantSection.getRange(ii);
            final Range ir = intersect.getRange(ii);
            this.dimList.add(new Dim(dr, wr, ir, dataStride, wantStride));
            dataStride *= dr.length();
            wantStride *= wr.length();
        }
        this.startDestElem = wantSection.offset(intersect);
        this.startSrcElem = dataSection.offset(intersect);
        if (this.debugStartingElems) {
            System.out.println(" startDestElem=" + this.startDestElem + " startSrcElem=" + this.startSrcElem);
        }
        if (varRank == 0) {
            this.nelems = 1;
        }
        else {
            final Dim innerDim = this.dimList.get(0);
            this.nelems = innerDim.ncontigElements;
            if (innerDim.ncontigElements > 1) {
                innerDim.wantNelems = 1;
                innerDim.wantStride = innerDim.ncontigElements;
            }
        }
        final int rank = this.dimList.size();
        final long[] dataStrides = new long[rank];
        final int[] resultStrides = new int[rank];
        final int[] shape = new int[rank];
        for (int i = 0; i < this.dimList.size(); ++i) {
            final Dim dim = this.dimList.get(i);
            dataStrides[rank - i - 1] = dim.dataStride * dim.want.stride();
            resultStrides[rank - i - 1] = dim.wantStride;
            shape[rank - i - 1] = dim.wantNelems;
        }
        if (this.debugDetail) {
            IndexChunker.printa(" indexShape=", shape);
            IndexChunker.printl(" dataStrides=", dataStrides);
            IndexChunker.printa(" wantStride=", resultStrides);
            System.out.println(" indexChunks=" + Index.computeSize(shape));
        }
        this.dataIndex = new IndexLong(shape, dataStrides);
        this.resultIndex = new Index(shape, resultStrides);
        if (this.debugDetail) {
            System.out.println(" dataIndex=" + this.dataIndex.toString());
            System.out.println(" resultIndex=" + this.resultIndex.toStringDebug());
        }
        final long nchunks = Index.computeSize(shape);
        assert nchunks * this.nelems == this.total;
        if (this.debug) {
            System.out.println("RegularSectionLayout total = " + this.total + " nchunks= " + nchunks + " nelems= " + this.nelems + " dataSection= " + dataSection + " wantSection= " + wantSection + " intersect= " + intersect + this);
        }
    }
    
    public long getTotalNelems() {
        return this.total;
    }
    
    public boolean hasNext() {
        return this.done < this.total;
    }
    
    public IndexChunker.Chunk next() {
        if (this.chunk == null) {
            this.chunk = new IndexChunker.Chunk(0L, this.nelems, this.startDestElem);
        }
        else {
            this.dataIndex.incr();
            this.resultIndex.incr();
        }
        this.chunk.setSrcElem(this.startSrcElem + this.dataIndex.currentElement());
        this.chunk.setDestElem(this.startDestElem + this.resultIndex.currentElement());
        if (this.debugNext) {
            System.out.println(" chunk: " + this.chunk);
        }
        if (this.debugDetail) {
            System.out.println(" dataIndex: " + this.dataIndex);
            System.out.println(" wantIndex: " + this.resultIndex);
        }
        this.done += this.nelems;
        return this.chunk;
    }
    
    @Override
    public String toString() {
        final StringBuilder sbuff = new StringBuilder();
        for (int i = 0; i < this.dimList.size(); ++i) {
            final Dim elem = this.dimList.get(i);
            sbuff.append("\n");
            sbuff.append(elem);
        }
        return sbuff.toString();
    }
    
    private class Dim
    {
        Range data;
        Range want;
        Range intersect;
        int dataStride;
        int wantStride;
        int wantNelems;
        int ncontigElements;
        
        Dim(final Range data, final Range want, final Range intersect, final int dataStride, final int wantStride) {
            this.data = data;
            this.want = want;
            this.intersect = intersect;
            this.dataStride = dataStride;
            this.wantStride = wantStride;
            this.ncontigElements = ((intersect.stride() == 1) ? intersect.length() : 1);
            this.wantNelems = intersect.length();
            if (IndexChunkerTiled.this.debugMerge) {
                System.out.println("Dim=" + this);
            }
        }
        
        @Override
        public String toString() {
            return "  data = " + this.data + " want = " + this.want + " intersect = " + this.intersect + " ncontigElements = " + this.ncontigElements;
        }
    }
}
