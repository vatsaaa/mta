// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.iosp.grid;

import ucar.ma2.Array;
import ucar.nc2.constants.AxisType;
import ucar.nc2.units.SimpleUnit;
import ucar.nc2.Attribute;
import ucar.ma2.DataType;
import ucar.nc2.Structure;
import ucar.nc2.Variable;
import ucar.nc2.Dimension;
import ucar.nc2.Group;
import ucar.nc2.NetcdfFile;
import java.util.Iterator;
import java.util.Collections;
import java.util.ArrayList;
import java.util.List;
import ucar.grid.GridTableLookup;
import ucar.grid.GridRecord;

public class GridCoordSys
{
    private GridHorizCoordSys hcs;
    private GridRecord record;
    private String verticalName;
    private GridTableLookup lookup;
    private List<Double> levels;
    boolean dontUseVertical;
    String positive;
    String units;
    
    GridCoordSys(final GridHorizCoordSys hcs, final GridRecord record, final String name, final GridTableLookup lookup) {
        this.dontUseVertical = false;
        this.positive = "up";
        this.hcs = hcs;
        this.record = record;
        this.verticalName = name;
        this.lookup = lookup;
        this.levels = new ArrayList<Double>();
        this.dontUseVertical = !lookup.isVerticalCoordinate(record);
        this.positive = (lookup.isPositiveUp(record) ? "up" : "down");
        this.units = lookup.getLevelUnit(record);
        if (GridServiceProvider.debugVert) {
            System.out.println("GridCoordSys: " + this.getVerticalDesc() + " useVertical= " + !this.dontUseVertical + " positive=" + this.positive + " units=" + this.units);
        }
    }
    
    String getCoordSysName() {
        return this.verticalName + "_CoordSys";
    }
    
    String getVerticalName() {
        return this.verticalName;
    }
    
    String getVerticalDesc() {
        return this.verticalName + "(" + this.record.getLevelType1() + ")";
    }
    
    int getNLevels() {
        return this.dontUseVertical ? 1 : this.levels.size();
    }
    
    void addLevels(final List<GridRecord> records) {
        for (final GridRecord record : records) {
            final Double d = new Double(record.getLevel1());
            if (!this.levels.contains(d)) {
                this.levels.add(d);
            }
            if (this.dontUseVertical && this.levels.size() > 1 && GridServiceProvider.debugVert) {
                System.out.println("GribCoordSys: unused level coordinate has > 1 levels = " + this.verticalName + " " + record.getLevelType1() + " " + this.levels.size());
            }
        }
        Collections.sort(this.levels);
        if (this.positive.equals("down")) {
            Collections.reverse(this.levels);
        }
    }
    
    boolean matchLevels(final List<GridRecord> records) {
        final List<Double> levelList = new ArrayList<Double>(records.size());
        for (final GridRecord record : records) {
            final Double d = new Double(record.getLevel1());
            if (!levelList.contains(d)) {
                levelList.add(d);
            }
        }
        Collections.sort(levelList);
        if (this.positive.equals("down")) {
            Collections.reverse(levelList);
        }
        return levelList.equals(this.levels);
    }
    
    void addDimensionsToNetcdfFile(final NetcdfFile ncfile, final Group g) {
        if (this.dontUseVertical) {
            return;
        }
        final int nlevs = this.levels.size();
        ncfile.addDimension(g, new Dimension(this.verticalName, nlevs, true));
    }
    
    void addToNetcdfFile(final NetcdfFile ncfile, Group g) {
        if (this.dontUseVertical) {
            return;
        }
        if (g == null) {
            g = ncfile.getRootGroup();
        }
        String dims = "time";
        if (!this.dontUseVertical) {
            dims = dims + " " + this.verticalName;
        }
        if (this.hcs.isLatLon()) {
            dims += " lat lon";
        }
        else {
            dims += " y x";
        }
        final int nlevs = this.levels.size();
        final Variable v = new Variable(ncfile, g, null, this.verticalName);
        v.setDataType(DataType.DOUBLE);
        v.addAttribute(new Attribute("long_name", this.lookup.getLevelDescription(this.record)));
        v.addAttribute(new Attribute("units", this.lookup.getLevelUnit(this.record)));
        if (this.positive != null) {
            v.addAttribute(new Attribute("positive", this.positive));
        }
        if (this.units != null) {
            AxisType axisType;
            if (SimpleUnit.isCompatible("millibar", this.units)) {
                axisType = AxisType.Pressure;
            }
            else if (SimpleUnit.isCompatible("m", this.units)) {
                axisType = AxisType.Height;
            }
            else {
                axisType = AxisType.GeoZ;
            }
            v.addAttribute(new Attribute("grid_level_type", Integer.toString(this.record.getLevelType1())));
            v.addAttribute(new Attribute("_CoordinateAxisType", axisType.toString()));
            v.addAttribute(new Attribute("_CoordinateAxes", dims));
            if (!this.hcs.isLatLon()) {
                v.addAttribute(new Attribute("_CoordinateTransforms", this.hcs.getGridName()));
            }
        }
        final double[] data = new double[nlevs];
        for (int i = 0; i < this.levels.size(); ++i) {
            final Double d = this.levels.get(i);
            data[i] = d;
        }
        final Array dataArray = Array.factory(DataType.DOUBLE, new int[] { nlevs }, data);
        v.setDimensions(this.verticalName);
        v.setCachedData(dataArray, false);
        ncfile.addVariable(g, v);
        if (this.record.getLevelType1() == 109) {
            this.findCoordinateTransform(g, "Pressure", this.record.getLevelType1());
        }
    }
    
    void findCoordinateTransform(final Group g, final String nameStartsWith, final int levelType) {
        final List<Variable> vars = g.getVariables();
        for (final Variable v : vars) {
            if (v.getName().equals(nameStartsWith)) {
                final Attribute att = v.findAttribute("grid_level_type");
                if (att == null) {
                    continue;
                }
                if (att.getNumericValue().intValue() != levelType) {
                    continue;
                }
                v.addAttribute(new Attribute("_CoordinateTransformType", "Vertical"));
                v.addAttribute(new Attribute("transform_name", "Existing3DField"));
            }
        }
    }
    
    int getIndex(final GridRecord record) {
        final Double d = new Double(record.getLevel1());
        return this.levels.indexOf(d);
    }
}
