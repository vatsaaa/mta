// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.iosp.grid;

import ucar.nc2.Attribute;
import ucar.grid.GridRecord;
import ucar.ma2.InvalidRangeException;
import ucar.ma2.IndexIterator;
import ucar.ma2.Range;
import ucar.ma2.DataType;
import ucar.ma2.Array;
import ucar.ma2.Section;
import ucar.nc2.Variable;
import ucar.unidata.io.RandomAccessFile;
import java.io.IOException;
import ucar.nc2.util.CancelTask;
import ucar.grid.GridIndex;
import ucar.nc2.util.DebugFlags;
import ucar.nc2.NetcdfFile;
import ucar.nc2.dt.fmr.FmrcCoordSys;
import ucar.nc2.iosp.AbstractIOServiceProvider;

public abstract class GridServiceProvider extends AbstractIOServiceProvider
{
    protected static IndexExtendMode indexFileModeOnOpen;
    protected static IndexExtendMode indexFileModeOnSync;
    protected static boolean addLatLon;
    protected static boolean useMaximalCoordSys;
    protected static boolean forceNewIndex;
    protected static boolean alwaysInCache;
    public static boolean debugOpen;
    public static boolean debugMissing;
    public static boolean debugMissingDetails;
    public static boolean debugProj;
    public static boolean debugTiming;
    public static boolean debugVert;
    protected FmrcCoordSys fmrcCoordSys;
    protected NetcdfFile ncfile;
    
    public static void useMaximalCoordSys(final boolean b) {
        GridServiceProvider.useMaximalCoordSys = b;
    }
    
    public static void forceNewIndex(final boolean b) {
        GridServiceProvider.forceNewIndex = b;
    }
    
    public static void setDebugFlags(final DebugFlags debugFlag) {
        GridServiceProvider.debugOpen = debugFlag.isSet("Grid/open");
        GridServiceProvider.debugMissing = debugFlag.isSet("Grid/missing");
        GridServiceProvider.debugMissingDetails = debugFlag.isSet("Grid/missingDetails");
        GridServiceProvider.debugProj = debugFlag.isSet("Grid/projection");
        GridServiceProvider.debugVert = debugFlag.isSet("Grid/vertical");
        GridServiceProvider.debugTiming = debugFlag.isSet("Grid/timing");
    }
    
    public static void setIndexFileModeOnOpen(final IndexExtendMode mode) {
        GridServiceProvider.indexFileModeOnOpen = mode;
    }
    
    public static void setIndexFileModeOnSync(final IndexExtendMode mode) {
        GridServiceProvider.indexFileModeOnSync = mode;
    }
    
    @Deprecated
    public static void setExtendIndex(final boolean b) {
        GridServiceProvider.indexFileModeOnOpen = (b ? IndexExtendMode.extendwrite : IndexExtendMode.readonly);
        GridServiceProvider.indexFileModeOnSync = (b ? IndexExtendMode.extendwrite : IndexExtendMode.readonly);
    }
    
    public static void setIndexAlwaysInCache(final boolean b) {
        GridServiceProvider.alwaysInCache = b;
    }
    
    protected abstract void open(final GridIndex p0, final CancelTask p1) throws IOException;
    
    @Override
    public void open(final RandomAccessFile raf, final NetcdfFile ncfile, final CancelTask cancelTask) throws IOException {
        this.raf = raf;
        this.ncfile = ncfile;
    }
    
    @Override
    public String getDetailInfo() {
        return "";
    }
    
    @Override
    public Object sendIospMessage(final Object special) {
        if (special instanceof FmrcCoordSys) {
            this.fmrcCoordSys = (FmrcCoordSys)special;
        }
        return super.sendIospMessage(special);
    }
    
    public Array readData(final Variable v2, final Section section) throws IOException, InvalidRangeException {
        final long start = System.currentTimeMillis();
        final Array dataArray = Array.factory(DataType.FLOAT, section.getShape());
        final GridVariable pv = (GridVariable)v2.getSPobject();
        int rangeIdx = 0;
        final Range ensRange = pv.hasEnsemble() ? section.getRange(rangeIdx++) : new Range(0, 0);
        final Range timeRange = (section.getRank() > 2) ? section.getRange(rangeIdx++) : new Range(0, 0);
        final Range levRange = pv.hasVert() ? section.getRange(rangeIdx++) : new Range(0, 0);
        final Range yRange = section.getRange(rangeIdx++);
        final Range xRange = section.getRange(rangeIdx);
        final IndexIterator ii = dataArray.getIndexIterator();
        for (int ensIdx = ensRange.first(); ensIdx <= ensRange.last(); ensIdx += ensRange.stride()) {
            for (int timeIdx = timeRange.first(); timeIdx <= timeRange.last(); timeIdx += timeRange.stride()) {
                for (int levelIdx = levRange.first(); levelIdx <= levRange.last(); levelIdx += levRange.stride()) {
                    this.readXY(v2, ensIdx, timeIdx, levelIdx, yRange, xRange, ii);
                }
            }
        }
        if (GridServiceProvider.debugTiming) {
            final long took = System.currentTimeMillis() - start;
            System.out.println("  read data took=" + took + " msec ");
        }
        return dataArray;
    }
    
    private void readXY(final Variable v2, final int ensIdx, final int timeIdx, final int levIdx, final Range yRange, final Range xRange, final IndexIterator ii) throws IOException, InvalidRangeException {
        final GridVariable pv = (GridVariable)v2.getSPobject();
        final GridHorizCoordSys hsys = pv.getHorizCoordSys();
        final int nx = hsys.getNx();
        final GridRecord record = pv.findRecord(ensIdx, timeIdx, levIdx);
        if (record == null) {
            final Attribute att = v2.findAttribute("missing_value");
            final float missing_value = (att == null) ? -9999.0f : att.getNumericValue().floatValue();
            for (int xyCount = yRange.length() * xRange.length(), j = 0; j < xyCount; ++j) {
                ii.setFloatNext(missing_value);
            }
            return;
        }
        final float[] data = this._readData(record);
        for (int y = yRange.first(); y <= yRange.last(); y += yRange.stride()) {
            for (int x = xRange.first(); x <= xRange.last(); x += xRange.stride()) {
                final int index = y * nx + x;
                ii.setFloatNext(data[index]);
            }
        }
    }
    
    public boolean isMissingXY(final Variable v2, final int timeIdx, final int ensIdx, final int levIdx) throws InvalidRangeException {
        final GridVariable pv = (GridVariable)v2.getSPobject();
        if (timeIdx < 0 || timeIdx >= pv.getNTimes()) {
            throw new InvalidRangeException("timeIdx=" + timeIdx);
        }
        if (levIdx < 0 || levIdx >= pv.getVertNlevels()) {
            throw new InvalidRangeException("levIdx=" + levIdx);
        }
        if (ensIdx < 0 || ensIdx >= pv.getNEnsembles()) {
            throw new InvalidRangeException("ensIdx=" + ensIdx);
        }
        return null == pv.findRecord(ensIdx, timeIdx, levIdx);
    }
    
    protected abstract float[] _readData(final GridRecord p0) throws IOException;
    
    static {
        GridServiceProvider.indexFileModeOnOpen = IndexExtendMode.rewrite;
        GridServiceProvider.indexFileModeOnSync = IndexExtendMode.extendwrite;
        GridServiceProvider.addLatLon = false;
        GridServiceProvider.useMaximalCoordSys = false;
        GridServiceProvider.forceNewIndex = false;
        GridServiceProvider.alwaysInCache = false;
        GridServiceProvider.debugOpen = false;
        GridServiceProvider.debugMissing = false;
        GridServiceProvider.debugMissingDetails = false;
        GridServiceProvider.debugProj = false;
        GridServiceProvider.debugTiming = false;
        GridServiceProvider.debugVert = false;
    }
    
    public enum IndexExtendMode
    {
        rewrite, 
        extendwrite, 
        readonly;
    }
}
