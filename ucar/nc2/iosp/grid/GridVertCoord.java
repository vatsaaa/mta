// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.iosp.grid;

import org.slf4j.LoggerFactory;
import ucar.nc2.util.Misc;
import ucar.ma2.Index;
import ucar.nc2.dataset.DatasetConstructor;
import ucar.ma2.Array;
import ucar.nc2.constants.AxisType;
import ucar.nc2.units.SimpleUnit;
import ucar.nc2.Attribute;
import ucar.grib.grib2.Grib2GridTableLookup;
import ucar.ma2.DataType;
import ucar.nc2.Structure;
import ucar.nc2.Variable;
import ucar.nc2.Dimension;
import ucar.nc2.Group;
import ucar.nc2.NetcdfFile;
import ucar.grid.GridDefRecord;
import ucar.grib.grib1.Grib1GDSVariables;
import java.util.Iterator;
import ucar.grib.grib1.Grib1GridTableLookup;
import java.util.Collections;
import java.util.ArrayList;
import java.util.List;
import ucar.grid.GridTableLookup;
import ucar.grid.GridRecord;
import org.slf4j.Logger;

public class GridVertCoord implements Comparable<GridVertCoord>
{
    private static Logger logger;
    private GridRecord typicalRecord;
    private String levelName;
    private GridTableLookup lookup;
    private int seq;
    private double[] coordValues;
    private boolean usesBounds;
    private boolean isVerticalCoordinate;
    private double[] factors;
    private String positive;
    private String units;
    private List<LevelCoord> levels;
    
    GridVertCoord(final String name) {
        this.seq = 0;
        this.coordValues = null;
        this.usesBounds = false;
        this.isVerticalCoordinate = false;
        this.factors = null;
        this.positive = "up";
        this.levels = new ArrayList<LevelCoord>();
        this.levelName = name;
    }
    
    GridVertCoord(final List<GridRecord> records, final String levelName, final GridTableLookup lookup, final GridHorizCoordSys hcs) {
        this.seq = 0;
        this.coordValues = null;
        this.usesBounds = false;
        this.isVerticalCoordinate = false;
        this.factors = null;
        this.positive = "up";
        this.levels = new ArrayList<LevelCoord>();
        this.typicalRecord = records.get(0);
        this.levelName = levelName;
        this.lookup = lookup;
        this.isVerticalCoordinate = lookup.isVerticalCoordinate(this.typicalRecord);
        this.positive = (lookup.isPositiveUp(this.typicalRecord) ? "up" : "down");
        this.units = lookup.getLevelUnit(this.typicalRecord);
        this.usesBounds = lookup.isLayer(this.typicalRecord);
        for (final GridRecord record : records) {
            if (this.coordIndex(record) < 0) {
                this.levels.add(new LevelCoord(record.getLevel1(), record.getLevel2()));
            }
        }
        Collections.sort(this.levels);
        if (this.positive.equals("down")) {
            Collections.reverse(this.levels);
        }
        if (this.typicalRecord.getLevelType1() == 109 && lookup instanceof Grib1GridTableLookup) {
            this.checkForPressureLevels(records, hcs);
        }
        if (GridServiceProvider.debugVert) {
            System.out.println("GribVertCoord: " + this.getVariableName() + "(" + this.typicalRecord.getLevelType1() + ") isVertDimensionUsed= " + this.isVertDimensionUsed() + " positive=" + this.positive + " units=" + this.units);
        }
    }
    
    void setSequence(final int seq) {
        this.seq = seq;
    }
    
    String getLevelName() {
        return this.levelName;
    }
    
    String getVariableName() {
        return (this.seq == 0) ? this.levelName : (this.levelName + this.seq);
    }
    
    int getNLevels() {
        return this.levels.size();
    }
    
    boolean isVertDimensionUsed() {
        return this.getNLevels() != 1 || this.isVerticalCoordinate;
    }
    
    boolean matchLevels(final List<GridRecord> records) {
        final List<LevelCoord> levelList = new ArrayList<LevelCoord>(records.size());
        for (final GridRecord record : records) {
            final LevelCoord lc = new LevelCoord(record.getLevel1(), record.getLevel2());
            if (!levelList.contains(lc)) {
                levelList.add(lc);
            }
        }
        Collections.sort(levelList);
        if (this.positive.equals("down")) {
            Collections.reverse(levelList);
        }
        return levelList.equals(this.levels);
    }
    
    boolean checkForPressureLevels(final List<GridRecord> records, final GridHorizCoordSys hcs) {
        final GridDefRecord gdr = hcs.getGds();
        final Grib1GDSVariables g1dr = (Grib1GDSVariables)gdr.getGdsv();
        if (g1dr == null || !g1dr.hasVerticalPressureLevels()) {
            return false;
        }
        this.coordValues = new double[this.levels.size()];
        for (int i = 0; i < this.levels.size(); ++i) {
            final LevelCoord lc = this.levels.get(i);
            this.coordValues[i] = lc.value1;
        }
        final int NV = g1dr.getNV();
        if (NV > 2 && NV < 255) {
            this.factors = g1dr.getVerticalPressureLevels();
        }
        return true;
    }
    
    void addDimensionsToNetcdfFile(final NetcdfFile ncfile, final Group g) {
        if (!this.isVertDimensionUsed()) {
            return;
        }
        int nlevs = this.levels.size();
        if (this.coordValues != null) {
            nlevs = this.coordValues.length;
        }
        ncfile.addDimension(g, new Dimension(this.getVariableName(), nlevs, true));
    }
    
    void addToNetcdfFile(final NetcdfFile ncfile, Group g) {
        if (!this.isVertDimensionUsed()) {
            this.typicalRecord = null;
            return;
        }
        if (g == null) {
            g = ncfile.getRootGroup();
        }
        final Variable v = new Variable(ncfile, g, null, this.getVariableName());
        v.setDataType(DataType.DOUBLE);
        String desc = this.lookup.getLevelDescription(this.typicalRecord);
        if (this.lookup instanceof Grib2GridTableLookup && this.usesBounds) {
            desc = "Layer between " + desc;
        }
        v.addAttribute(new Attribute("long_name", desc));
        v.addAttribute(new Attribute("units", this.lookup.getLevelUnit(this.typicalRecord)));
        if (this.positive != null) {
            v.addAttribute(new Attribute("positive", this.positive));
        }
        if (this.units != null) {
            AxisType axisType;
            if (SimpleUnit.isCompatible("millibar", this.units)) {
                axisType = AxisType.Pressure;
            }
            else if (SimpleUnit.isCompatible("m", this.units)) {
                axisType = AxisType.Height;
            }
            else {
                axisType = AxisType.GeoZ;
            }
            if (this.lookup instanceof Grib2GridTableLookup || this.lookup instanceof Grib1GridTableLookup) {
                v.addAttribute(new Attribute("GRIB_level_type", Integer.toString(this.typicalRecord.getLevelType1())));
            }
            else {
                v.addAttribute(new Attribute("level_type", Integer.toString(this.typicalRecord.getLevelType1())));
            }
            v.addAttribute(new Attribute("_CoordinateAxisType", axisType.toString()));
        }
        if (this.coordValues == null) {
            this.coordValues = new double[this.levels.size()];
            for (int i = 0; i < this.levels.size(); ++i) {
                final LevelCoord lc = this.levels.get(i);
                this.coordValues[i] = lc.mid;
            }
        }
        final Array dataArray = Array.factory(DataType.DOUBLE, new int[] { this.coordValues.length }, this.coordValues);
        v.setDimensions(this.getVariableName());
        v.setCachedData(dataArray, true);
        ncfile.addVariable(g, v);
        if (this.usesBounds) {
            final Dimension bd = DatasetConstructor.getBoundsDimension(ncfile);
            final String bname = this.getVariableName() + "_bounds";
            v.addAttribute(new Attribute("bounds", bname));
            v.addAttribute(new Attribute("_CoordinateZisLayer", "true"));
            final Variable b = new Variable(ncfile, g, null, bname);
            b.setDataType(DataType.DOUBLE);
            b.setDimensions(this.getVariableName() + " " + bd.getName());
            b.addAttribute(new Attribute("long_name", "bounds for " + v.getName()));
            b.addAttribute(new Attribute("units", this.lookup.getLevelUnit(this.typicalRecord)));
            final Array boundsArray = Array.factory(DataType.DOUBLE, new int[] { this.coordValues.length, 2 });
            final Index ima = boundsArray.getIndex();
            for (int j = 0; j < this.coordValues.length; ++j) {
                final LevelCoord lc2 = this.levels.get(j);
                boundsArray.setDouble(ima.set(j, 0), lc2.value1);
                boundsArray.setDouble(ima.set(j, 1), lc2.value2);
            }
            b.setCachedData(boundsArray, true);
            ncfile.addVariable(g, b);
        }
        if (this.factors != null) {
            if (g == null) {
                g = ncfile.getRootGroup();
            }
            if (g.findVariable("hybrida") != null) {
                return;
            }
            v.addAttribute(new Attribute("standard_name", "atmosphere_hybrid_sigma_pressure_coordinate"));
            v.addAttribute(new Attribute("formula_terms", "ap: hybrida b: hybridb ps: Pressure"));
            final Variable ha = new Variable(ncfile, g, null, "hybrida");
            ha.setDataType(DataType.DOUBLE);
            ha.addAttribute(new Attribute("long_name", "level_a_factor"));
            ha.addAttribute(new Attribute("units", ""));
            ha.setDimensions(this.getVariableName());
            final int middle = this.factors.length / 2;
            double[] adata;
            double[] bdata;
            if (this.levels.size() < middle) {
                adata = new double[this.levels.size()];
                bdata = new double[this.levels.size()];
            }
            else {
                adata = new double[middle];
                bdata = new double[middle];
            }
            for (int k = 0; k < middle && k < this.levels.size(); ++k) {
                adata[k] = this.factors[k];
            }
            final Array haArray = Array.factory(DataType.DOUBLE, new int[] { adata.length }, adata);
            ha.setCachedData(haArray, true);
            ncfile.addVariable(g, ha);
            final Variable hb = new Variable(ncfile, g, null, "hybridb");
            hb.setDataType(DataType.DOUBLE);
            hb.addAttribute(new Attribute("long_name", "level_b_factor"));
            hb.addAttribute(new Attribute("units", ""));
            hb.setDimensions(this.getVariableName());
            for (int l = 0; l < middle && l < this.levels.size(); ++l) {
                bdata[l] = this.factors[l + middle];
            }
            final Array hbArray = Array.factory(DataType.DOUBLE, new int[] { bdata.length }, bdata);
            hb.setCachedData(hbArray, true);
            ncfile.addVariable(g, hb);
        }
    }
    
    int getIndex(final GridRecord record) {
        if (!this.isVertDimensionUsed()) {
            return 0;
        }
        return this.coordIndex(record);
    }
    
    public int compareTo(final GridVertCoord gv) {
        return this.getLevelName().compareToIgnoreCase(gv.getLevelName());
    }
    
    public double getCoord(final int i) {
        return (this.coordValues == null) ? 0.0 : this.coordValues[i];
    }
    
    private int coordIndex(final GridRecord record) {
        double val = record.getLevel1();
        double val2 = record.getLevel2();
        if (this.usesBounds && val > val2) {
            val = record.getLevel2();
            val2 = record.getLevel1();
        }
        for (int i = 0; i < this.levels.size(); ++i) {
            final LevelCoord lc = this.levels.get(i);
            if (this.usesBounds) {
                if (Misc.closeEnough(lc.value1, val) && Misc.closeEnough(lc.value2, val2)) {
                    return i;
                }
            }
            else if (Misc.closeEnough(lc.value1, val)) {
                return i;
            }
        }
        return -1;
    }
    
    @Override
    public String toString() {
        return "GridVertCoord{levelName='" + this.levelName + '\'' + ", seq=" + this.seq + '}';
    }
    
    static {
        GridVertCoord.logger = LoggerFactory.getLogger(GridVertCoord.class);
    }
    
    private class LevelCoord implements Comparable
    {
        double mid;
        double value1;
        double value2;
        
        LevelCoord(final double value1, final double value2) {
            this.value1 = value1;
            this.value2 = value2;
            if (GridVertCoord.this.usesBounds && value1 > value2) {
                this.value1 = value2;
                this.value2 = value1;
            }
            this.mid = (GridVertCoord.this.usesBounds ? ((value1 + value2) / 2.0) : value1);
        }
        
        public int compareTo(final Object o) {
            final LevelCoord other = (LevelCoord)o;
            if (this.mid < other.mid) {
                return -1;
            }
            if (this.mid > other.mid) {
                return 1;
            }
            return 0;
        }
        
        @Override
        public boolean equals(final Object oo) {
            if (this == oo) {
                return true;
            }
            if (!(oo instanceof LevelCoord)) {
                return false;
            }
            final LevelCoord other = (LevelCoord)oo;
            return Misc.closeEnough(this.value1, other.value1) && Misc.closeEnough(this.value2, other.value2);
        }
        
        @Override
        public int hashCode() {
            return (int)(this.value1 * 100000.0 + this.value2 * 100.0);
        }
    }
}
