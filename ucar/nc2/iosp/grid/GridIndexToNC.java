// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.iosp.grid;

import org.slf4j.LoggerFactory;
import ucar.nc2.Variable;
import java.util.TimeZone;
import java.util.Calendar;
import java.io.IOException;
import java.util.Iterator;
import java.util.Comparator;
import java.util.Collections;
import java.util.Collection;
import java.util.Formatter;
import ucar.nc2.constants.FeatureType;
import ucar.grib.grib1.Grib1GridTableLookup;
import ucar.grib.GribGridRecord;
import ucar.nc2.Attribute;
import java.util.ArrayList;
import java.util.List;
import ucar.nc2.Group;
import ucar.grid.GridDefRecord;
import ucar.nc2.util.CancelTask;
import ucar.nc2.dt.fmr.FmrcCoordSys;
import ucar.nc2.NetcdfFile;
import ucar.grid.GridIndex;
import java.util.HashMap;
import ucar.grib.grib2.Grib2GridTableLookup;
import ucar.grid.GridTableLookup;
import ucar.grid.GridRecord;
import ucar.unidata.io.RandomAccessFile;
import ucar.nc2.units.DateFormatter;
import java.util.Map;
import org.slf4j.Logger;

public class GridIndexToNC
{
    private static Logger logger;
    private Map<String, GridHorizCoordSys> hcsHash;
    private DateFormatter formatter;
    private boolean debug;
    private String indexFilename;
    private RandomAccessFile raf;
    
    static String makeLevelName(final GridRecord gr, final GridTableLookup lookup) {
        if (lookup instanceof Grib2GridTableLookup) {
            final String vname = lookup.getLevelName(gr);
            return lookup.isLayer(gr) ? (vname + "_layer") : vname;
        }
        return lookup.getLevelName(gr);
    }
    
    public GridIndexToNC(final String filename) {
        this.hcsHash = new HashMap<String, GridHorizCoordSys>(10);
        this.formatter = new DateFormatter();
        this.debug = false;
        this.indexFilename = filename;
    }
    
    public GridIndexToNC(final RandomAccessFile raf) {
        this.hcsHash = new HashMap<String, GridHorizCoordSys>(10);
        this.formatter = new DateFormatter();
        this.debug = false;
        this.indexFilename = raf.getLocation();
        this.raf = raf;
    }
    
    public void open(final GridIndex index, final GridTableLookup lookup, final int version, final NetcdfFile ncfile, final FmrcCoordSys fmrcCoordSys, final CancelTask cancelTask) throws IOException {
        final List<GridDefRecord> hcsList = (List<GridDefRecord>)index.getHorizCoordSys();
        final boolean needGroups = hcsList.size() > 1;
        for (final GridDefRecord gds : hcsList) {
            Group g = null;
            if (needGroups) {
                g = new Group(ncfile, null, gds.getGroupName());
                ncfile.addGroup(null, g);
            }
            final GridHorizCoordSys hcs = new GridHorizCoordSys(gds, lookup, g);
            this.hcsHash.put(gds.getParam("GDSkey"), hcs);
        }
        GridRecord firstRecord = null;
        final List<GridRecord> records = (List<GridRecord>)index.getGridRecords();
        for (final GridRecord gridRecord : records) {
            if (firstRecord == null) {
                firstRecord = gridRecord;
            }
            final GridHorizCoordSys hcs2 = this.hcsHash.get(gridRecord.getGridDefRecordId());
            final int cdmHash = gridRecord.cdmVariableHash();
            GridVariable pv = hcs2.varHash.get(cdmHash);
            if (null == pv) {
                final String name = gridRecord.cdmVariableName(lookup, true, true);
                pv = new GridVariable(this.indexFilename, name, hcs2, lookup);
                hcs2.varHash.put(cdmHash, pv);
                final String simpleName = gridRecord.getParameterDescription();
                List<GridVariable> plist = hcs2.productHash.get(simpleName);
                if (null == plist) {
                    plist = new ArrayList<GridVariable>();
                    hcs2.productHash.put(simpleName, plist);
                }
                plist.add(pv);
            }
            pv.addProduct(gridRecord);
        }
        ncfile.addAttribute(null, new Attribute("Conventions", "CF-1.4"));
        String center = null;
        String subcenter = null;
        if (lookup instanceof Grib2GridTableLookup) {
            final Grib2GridTableLookup g2lookup = (Grib2GridTableLookup)lookup;
            final GribGridRecord ggr = (GribGridRecord)firstRecord;
            center = g2lookup.getFirstCenterName();
            ncfile.addAttribute(null, new Attribute("Originating_center", center));
            subcenter = g2lookup.getFirstSubcenterName();
            if (subcenter != null) {
                ncfile.addAttribute(null, new Attribute("Originating_subcenter", subcenter));
            }
            final String model = g2lookup.getModel();
            if (model != null) {
                ncfile.addAttribute(null, new Attribute("Generating_Model", model));
            }
            if (null != g2lookup.getFirstProductStatusName()) {
                ncfile.addAttribute(null, new Attribute("Product_Status", g2lookup.getFirstProductStatusName()));
            }
            ncfile.addAttribute(null, new Attribute("Product_Type", g2lookup.getFirstProductTypeName()));
        }
        else if (lookup instanceof Grib1GridTableLookup) {
            final Grib1GridTableLookup g1lookup = (Grib1GridTableLookup)lookup;
            center = g1lookup.getFirstCenterName();
            subcenter = g1lookup.getFirstSubcenterName();
            ncfile.addAttribute(null, new Attribute("Originating_center", center));
            if (subcenter != null) {
                ncfile.addAttribute(null, new Attribute("Originating_subcenter", subcenter));
            }
            final String model2 = g1lookup.getModel();
            if (model2 != null) {
                ncfile.addAttribute(null, new Attribute("Generating_Model", model2));
            }
            if (null != g1lookup.getFirstProductStatusName()) {
                ncfile.addAttribute(null, new Attribute("Product_Status", g1lookup.getFirstProductStatusName()));
            }
            ncfile.addAttribute(null, new Attribute("Product_Type", g1lookup.getFirstProductTypeName()));
        }
        ncfile.addAttribute(null, new Attribute("title", lookup.getTitle()));
        if (lookup.getInstitution() != null) {
            ncfile.addAttribute(null, new Attribute("institution", lookup.getInstitution()));
        }
        final String source = lookup.getSource();
        if (source != null && !source.startsWith("Unknown")) {
            ncfile.addAttribute(null, new Attribute("source", source));
        }
        ncfile.addAttribute(null, new Attribute("history", "Direct read of " + lookup.getGridType() + " into NetCDF-Java 4 API"));
        if (lookup.getComment() != null) {
            ncfile.addAttribute(null, new Attribute("comment", lookup.getComment()));
        }
        ncfile.addAttribute(null, new Attribute("CF:feature_type", FeatureType.GRID.toString()));
        ncfile.addAttribute(null, new Attribute("file_format", lookup.getGridType()));
        ncfile.addAttribute(null, new Attribute("location", ncfile.getLocation()));
        ncfile.addAttribute(null, new Attribute("_CoordinateModelRunDate", this.formatter.toDateTimeStringISO(lookup.getFirstBaseTime())));
        this.makeDenseCoordSys(ncfile, lookup, cancelTask);
        if (GridServiceProvider.debugMissing) {
            final Formatter f = new Formatter(System.out);
            int count = 0;
            final Collection<GridHorizCoordSys> hcset = this.hcsHash.values();
            for (final GridHorizCoordSys hcs3 : hcset) {
                final List<GridVariable> gribvars = new ArrayList<GridVariable>(hcs3.varHash.values());
                for (final GridVariable gv : gribvars) {
                    count += gv.showMissingSummary(f);
                }
            }
            System.out.println(" total missing= " + count);
        }
        if (GridServiceProvider.debugMissingDetails) {
            final Formatter f = new Formatter(System.out);
            final Collection<GridHorizCoordSys> hcset2 = this.hcsHash.values();
            for (final GridHorizCoordSys hcs4 : hcset2) {
                f.format("******** Horiz Coordinate= %s%n", hcs4.getGridName());
                String lastVertDesc = null;
                final List<GridVariable> gribvars = new ArrayList<GridVariable>(hcs4.varHash.values());
                Collections.sort(gribvars, new CompareGridVariableByVertName());
                for (final GridVariable gv : gribvars) {
                    final String vertDesc = gv.getVertName();
                    if (!vertDesc.equals(lastVertDesc)) {
                        f.format("---Vertical Coordinate= %s%n", vertDesc);
                        lastVertDesc = vertDesc;
                    }
                    gv.showMissing(f);
                }
            }
        }
    }
    
    public GridHorizCoordSys getHorizCoordSys(final GridRecord gribRecord) {
        return this.hcsHash.get(gribRecord.getGridDefRecordId());
    }
    
    public Map<String, GridHorizCoordSys> getHorizCoordSystems() {
        return this.hcsHash;
    }
    
    private void makeDenseCoordSys(final NetcdfFile ncfile, final GridTableLookup lookup, final CancelTask cancelTask) throws IOException {
        final List<GridTimeCoord> timeCoords = new ArrayList<GridTimeCoord>();
        final List<GridVertCoord> vertCoords = new ArrayList<GridVertCoord>();
        final List<GridEnsembleCoord> ensembleCoords = new ArrayList<GridEnsembleCoord>();
        final Calendar calendar = Calendar.getInstance();
        calendar.setTimeZone(TimeZone.getTimeZone("GMT"));
        final Collection<GridHorizCoordSys> hcset = this.hcsHash.values();
        for (final GridHorizCoordSys hcs : hcset) {
            if (cancelTask != null && cancelTask.isCancel()) {
                break;
            }
            final List<GridVariable> gribvars = new ArrayList<GridVariable>(hcs.varHash.values());
            for (final GridVariable gv : gribvars) {
                if (cancelTask != null && cancelTask.isCancel()) {
                    break;
                }
                final List<GridRecord> recordList = gv.getRecords();
                final GridRecord record = recordList.get(0);
                final String vname = makeLevelName(record, lookup);
                GridVertCoord useVertCoord = null;
                for (final GridVertCoord gvcs : vertCoords) {
                    if (vname.equals(gvcs.getLevelName()) && gvcs.matchLevels(recordList)) {
                        useVertCoord = gvcs;
                    }
                }
                if (useVertCoord == null) {
                    useVertCoord = new GridVertCoord(recordList, vname, lookup, hcs);
                    vertCoords.add(useVertCoord);
                }
                gv.setVertCoord(useVertCoord);
                GridTimeCoord useTimeCoord = null;
                for (final GridTimeCoord gtc : timeCoords) {
                    if (gtc.matchTimes(recordList)) {
                        useTimeCoord = gtc;
                        break;
                    }
                }
                if (useTimeCoord == null) {
                    useTimeCoord = new GridTimeCoord(recordList);
                    timeCoords.add(useTimeCoord);
                }
                gv.setTimeCoord(useTimeCoord);
                if (!gv.isEnsemble()) {
                    continue;
                }
                GridEnsembleCoord useEnsembleCoord = null;
                final GridEnsembleCoord ensembleCoord = new GridEnsembleCoord(recordList);
                for (final GridEnsembleCoord gec : ensembleCoords) {
                    if (ensembleCoord.equals(gec)) {
                        useEnsembleCoord = gec;
                        break;
                    }
                }
                if (useEnsembleCoord == null) {
                    useEnsembleCoord = ensembleCoord;
                    ensembleCoords.add(ensembleCoord);
                }
                gv.setEnsembleCoord(useEnsembleCoord);
            }
            Collections.sort(timeCoords);
            int count = 0;
            for (final GridTimeCoord tcs : timeCoords) {
                tcs.setSequence(count++);
                tcs.addDimensionsToNetcdfFile(ncfile, hcs.getGroup());
            }
            int seqno = 0;
            for (final GridEnsembleCoord gec2 : ensembleCoords) {
                gec2.setSequence(seqno++);
                gec2.addDimensionsToNetcdfFile(ncfile, hcs.getGroup());
            }
            hcs.addDimensionsToNetcdfFile(ncfile);
            Collections.sort(vertCoords);
            int vcIndex = 0;
            String listName = null;
            int start = 0;
            for (vcIndex = 0; vcIndex < vertCoords.size(); ++vcIndex) {
                final GridVertCoord gvcs2 = vertCoords.get(vcIndex);
                final String vname2 = gvcs2.getLevelName();
                if (listName == null) {
                    listName = vname2;
                }
                if (!vname2.equals(listName)) {
                    this.makeVerticalDimensions(vertCoords.subList(start, vcIndex), ncfile, hcs.getGroup());
                    listName = vname2;
                    start = vcIndex;
                }
            }
            this.makeVerticalDimensions(vertCoords.subList(start, vcIndex), ncfile, hcs.getGroup());
            final List<List<GridVariable>> products = new ArrayList<List<GridVariable>>(hcs.productHash.values());
            for (final List<GridVariable> plist : products) {
                if (cancelTask != null && cancelTask.isCancel()) {
                    break;
                }
                if (plist.size() == 1) {
                    final GridVariable pv = plist.get(0);
                    final String name = pv.getFirstRecord().cdmVariableName(lookup, false, false);
                    final Variable v = pv.makeVariable(ncfile, hcs.getGroup(), name, this.raf);
                    ncfile.addVariable(hcs.getGroup(), v);
                }
                else {
                    final Map<GridVertCoord, VertCollection> vcMap = new HashMap<GridVertCoord, VertCollection>();
                    for (final GridVariable gv2 : plist) {
                        VertCollection vc = vcMap.get(gv2.getVertCoord());
                        if (vc == null) {
                            vc = new VertCollection(gv2);
                            vcMap.put(gv2.getVertCoord(), vc);
                        }
                        vc.list.add(gv2);
                    }
                    final List<VertCollection> vclist = new ArrayList<VertCollection>(vcMap.values());
                    Collections.sort(vclist);
                    boolean firstVertCoord = true;
                    for (final VertCollection vc2 : vclist) {
                        final boolean hasMultipleLevels = vc2.vc.getNLevels() > 1;
                        final boolean noLevelOk = firstVertCoord;
                        final List<GridVariable> list = vc2.list;
                        if (list.size() == 1) {
                            final GridVariable gv3 = list.get(0);
                            final String name2 = gv3.getFirstRecord().cdmVariableName(lookup, !noLevelOk, false);
                            ncfile.addVariable(hcs.getGroup(), gv3.makeVariable(ncfile, hcs.getGroup(), name2, this.raf));
                        }
                        else {
                            for (final GridVariable gv4 : list) {
                                final String name3 = gv4.getFirstRecord().cdmVariableName(lookup, !noLevelOk, true);
                                ncfile.addVariable(hcs.getGroup(), gv4.makeVariable(ncfile, hcs.getGroup(), name3, this.raf));
                            }
                        }
                        firstVertCoord = false;
                    }
                }
            }
            for (final GridTimeCoord tcs2 : timeCoords) {
                tcs2.addToNetcdfFile(ncfile, hcs.getGroup());
            }
            for (final GridEnsembleCoord ens : ensembleCoords) {
                ens.addToNetcdfFile(ncfile, hcs.getGroup());
            }
            hcs.addToNetcdfFile(ncfile);
            for (final GridVertCoord gvcs : vertCoords) {
                gvcs.addToNetcdfFile(ncfile, hcs.getGroup());
            }
        }
    }
    
    private void makeVerticalDimensions(final List<GridVertCoord> vertCoordList, final NetcdfFile ncfile, final Group group) {
        GridVertCoord gvcs0 = null;
        int maxLevels = 0;
        for (final GridVertCoord gvcs2 : vertCoordList) {
            if (gvcs2.getNLevels() > maxLevels) {
                gvcs0 = gvcs2;
                maxLevels = gvcs2.getNLevels();
            }
        }
        int seqno = 1;
        for (final GridVertCoord gvcs3 : vertCoordList) {
            if (gvcs3 != gvcs0) {
                gvcs3.setSequence(seqno++);
            }
            gvcs3.addDimensionsToNetcdfFile(ncfile, group);
        }
    }
    
    static {
        GridIndexToNC.logger = LoggerFactory.getLogger(GridIndexToNC.class);
    }
    
    private class VertCollection implements Comparable<VertCollection>
    {
        GridVertCoord vc;
        List<GridVariable> list;
        
        VertCollection(final GridVariable gv) {
            this.list = new ArrayList<GridVariable>(3);
            this.vc = gv.getVertCoord();
        }
        
        @Override
        public int hashCode() {
            return this.vc.hashCode();
        }
        
        @Override
        public boolean equals(final Object obj) {
            final VertCollection oc = (VertCollection)obj;
            return this.vc.equals(oc.vc);
        }
        
        public int compareTo(final VertCollection o) {
            int ret = o.vc.getNLevels() - this.vc.getNLevels();
            if (ret == 0) {
                ret = this.vc.getLevelName().compareTo(o.vc.getLevelName());
            }
            return ret;
        }
    }
    
    private class CompareGridVariableByVertName implements Comparator
    {
        public int compare(final Object o1, final Object o2) {
            final GridVariable gv1 = (GridVariable)o1;
            final GridVariable gv2 = (GridVariable)o2;
            return gv1.getVertName().compareToIgnoreCase(gv2.getVertName());
        }
    }
    
    private class CompareGridVariableByNumberVertLevels implements Comparator
    {
        public int compare(final Object o1, final Object o2) {
            final GridVariable gv1 = (GridVariable)o1;
            final GridVariable gv2 = (GridVariable)o2;
            final int n1 = gv1.getVertCoord().getNLevels();
            final int n2 = gv2.getVertCoord().getNLevels();
            if (n1 == n2) {
                return gv1.getVertCoord().getLevelName().compareTo(gv2.getVertCoord().getLevelName());
            }
            return n2 - n1;
        }
    }
}
