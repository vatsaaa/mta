// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.iosp.grid;

public class GridCF
{
    public static final String EARTH_RADIUS = "earth_radius";
    public static final String SEMI_MAJOR_AXIS = "semi_major_axis";
    public static final String SEMI_MINOR_AXIS = "semi_minor_axis";
    public static final String GRID_MAPPING_NAME = "grid_mapping_name";
    public static final String EARTH_SHAPE = "earth_shape";
    public static final String STANDARD_PARALLEL = "standard_parallel";
    public static final String LONGITUDE_OF_CENTRAL_MERIDIAN = "longitude_of_central_meridian";
    public static final String LATITUDE_OF_PROJECTION_ORIGIN = "latitude_of_projection_origin";
    public static final String LONGITUDE_OF_PROJECTION_ORIGIN = "longitude_of_projection_origin";
    public static final String STRAIGHT_VERTICAL_LONGITUDE_FROM_POLE = "straight_vertical_longitude_from_pole";
    public static final String SCALE_FACTOR_AT_PROJECTION_ORIGIN = "scale_factor_at_projection_origin";
}
