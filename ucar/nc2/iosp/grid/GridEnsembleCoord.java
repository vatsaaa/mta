// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.iosp.grid;

import org.slf4j.LoggerFactory;
import ucar.ma2.Array;
import ucar.grib.grib2.Grib2Tables;
import ucar.nc2.constants.AxisType;
import ucar.nc2.Attribute;
import ucar.ma2.DataType;
import ucar.nc2.Structure;
import ucar.nc2.Variable;
import ucar.nc2.Dimension;
import ucar.nc2.Group;
import ucar.nc2.NetcdfFile;
import java.util.Iterator;
import java.util.Map;
import java.util.Collections;
import java.util.Collection;
import java.util.ArrayList;
import ucar.grib.GribGridRecord;
import java.util.HashMap;
import ucar.grid.GridRecord;
import java.util.List;
import org.slf4j.Logger;

public class GridEnsembleCoord
{
    private static Logger log;
    private List<EnsCoord> ensCoords;
    private int seq;
    
    GridEnsembleCoord(final List<GridRecord> records) {
        this.seq = 0;
        final Map<Integer, EnsCoord> map = new HashMap<Integer, EnsCoord>();
        for (final GridRecord record : records) {
            final GribGridRecord ggr = (GribGridRecord)record;
            final int ensNumber = ggr.getPds().getPerturbationNumber();
            final int ensType = ggr.getPds().getPerturbationType();
            map.put(ensNumber, new EnsCoord(ensNumber, ensType));
        }
        Collections.sort(this.ensCoords = new ArrayList<EnsCoord>(map.values()));
    }
    
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || this.getClass() != o.getClass()) {
            return false;
        }
        final GridEnsembleCoord that = (GridEnsembleCoord)o;
        return this.ensCoords.equals(that.ensCoords);
    }
    
    @Override
    public int hashCode() {
        return this.ensCoords.hashCode();
    }
    
    void setSequence(final int seq) {
        this.seq = seq;
    }
    
    String getName() {
        return (this.seq == 0) ? "ens" : ("ens" + this.seq);
    }
    
    void addDimensionsToNetcdfFile(final NetcdfFile ncfile, final Group g) {
        ncfile.addDimension(g, new Dimension(this.getName(), this.getNEnsembles(), true));
    }
    
    void addToNetcdfFile(final NetcdfFile ncfile, final Group g) {
        final Variable v = new Variable(ncfile, g, null, this.getName());
        v.setDimensions(v.getShortName());
        v.setDataType(DataType.STRING);
        v.addAttribute(new Attribute("long_name", "ensemble"));
        v.addAttribute(new Attribute("_CoordinateAxisType", AxisType.Ensemble.toString()));
        final String[] data = new String[this.getNEnsembles()];
        for (int i = 0; i < this.getNEnsembles(); ++i) {
            final EnsCoord ec = this.ensCoords.get(i);
            data[i] = Grib2Tables.codeTable4_6(ec.type) + " " + ec.number;
        }
        final Array dataArray = Array.factory(DataType.STRING, new int[] { this.getNEnsembles() }, data);
        v.setCachedData(dataArray, false);
        ncfile.addVariable(g, v);
    }
    
    int getIndex(final GribGridRecord ggr) {
        final int ensNumber = ggr.getPds().getPerturbationNumber();
        final int ensType = ggr.getPds().getPerturbationType();
        int count = 0;
        for (final EnsCoord coord : this.ensCoords) {
            if (coord.number == ensNumber && coord.type == ensType) {
                return count;
            }
            ++count;
        }
        return -1;
    }
    
    int getNEnsembles() {
        return this.ensCoords.size();
    }
    
    static {
        GridEnsembleCoord.log = LoggerFactory.getLogger(GridEnsembleCoord.class);
    }
    
    private class EnsCoord implements Comparable<EnsCoord>
    {
        int number;
        int type;
        
        private EnsCoord(final int number, final int type) {
            this.number = number;
            this.type = type;
        }
        
        public int compareTo(final EnsCoord o) {
            return this.number - o.number;
        }
        
        @Override
        public boolean equals(final Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || this.getClass() != o.getClass()) {
                return false;
            }
            final EnsCoord ensCoord = (EnsCoord)o;
            return this.number == ensCoord.number && this.type == ensCoord.type;
        }
        
        @Override
        public int hashCode() {
            int result = this.number;
            result = 31 * result + this.type;
            return result;
        }
    }
}
