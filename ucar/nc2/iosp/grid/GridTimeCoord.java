// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.iosp.grid;

import org.slf4j.LoggerFactory;
import ucar.nc2.dataset.DatasetConstructor;
import java.util.Formatter;
import ucar.nc2.Attribute;
import ucar.ma2.Array;
import ucar.nc2.units.DateUnit;
import ucar.nc2.units.DateFormatter;
import ucar.ma2.DataType;
import ucar.nc2.Structure;
import ucar.nc2.Variable;
import ucar.nc2.Dimension;
import ucar.nc2.Group;
import ucar.nc2.NetcdfFile;
import ucar.grib.GribPds;
import java.util.Iterator;
import java.util.Collections;
import java.util.ArrayList;
import ucar.grib.GribGridRecord;
import ucar.grid.GridRecord;
import java.util.List;
import java.util.Date;
import org.slf4j.Logger;

public class GridTimeCoord implements Comparable<GridTimeCoord>
{
    private static Logger log;
    private int seq;
    private String timeUdunit;
    private int timeUnit;
    private Date baseDate;
    private List<Date> times;
    private List<TimeCoordWithInterval> timeIntvs;
    private int constantInterval;
    private int[] coordData;
    
    GridTimeCoord(final List<GridRecord> records) {
        this.seq = 0;
        this.constantInterval = -1;
        for (final GridRecord record : records) {
            if (this.baseDate == null) {
                this.baseDate = record.getReferenceTime();
                this.timeUdunit = record.getTimeUdunitName();
                this.timeUnit = record.getTimeUnit();
            }
            final Date ref = record.getReferenceTime();
            if (ref.before(this.baseDate)) {
                this.baseDate = ref;
            }
        }
        if (records.get(0) instanceof GribGridRecord) {
            GribGridRecord ggr = (GribGridRecord)records.get(0);
            if (ggr.isInterval()) {
                this.timeIntvs = new ArrayList<TimeCoordWithInterval>();
                boolean same = true;
                int intv = -1;
                for (final GridRecord gr : records) {
                    ggr = (GribGridRecord)gr;
                    final Date ref2 = gr.getReferenceTime();
                    if (!this.baseDate.equals(ref2)) {
                        GridTimeCoord.log.warn(gr + " does not have same base date= " + this.baseDate + " != " + ref2);
                    }
                    final GribPds pds = ggr.getPds();
                    final int[] timeInv = pds.getForecastTimeInterval(this.timeUnit);
                    final int start = timeInv[0];
                    final int end = timeInv[1];
                    final int intv2 = end - start;
                    if (intv2 > 0) {
                        if (intv < 0) {
                            intv = intv2;
                        }
                        else {
                            same = (same && intv == intv2);
                        }
                    }
                    final Date validTime = gr.getValidTime();
                    final TimeCoordWithInterval timeCoordIntv = new TimeCoordWithInterval(validTime, start, intv2);
                    if (!this.timeIntvs.contains(timeCoordIntv)) {
                        this.timeIntvs.add(timeCoordIntv);
                    }
                }
                if (same) {
                    this.constantInterval = intv;
                }
                Collections.sort(this.timeIntvs);
                return;
            }
        }
        this.times = new ArrayList<Date>();
        for (final GridRecord gr2 : records) {
            Date validTime2 = gr2.getValidTime();
            if (validTime2 == null) {
                validTime2 = gr2.getReferenceTime();
            }
            if (!this.times.contains(validTime2)) {
                this.times.add(validTime2);
            }
        }
        Collections.sort(this.times);
    }
    
    boolean matchTimes(final List<GridRecord> records) {
        for (final GridRecord record : records) {
            if (!this.timeUdunit.equals(record.getTimeUdunitName())) {
                return false;
            }
        }
        if (records.get(0) instanceof GribGridRecord) {
            final GribGridRecord ggr = (GribGridRecord)records.get(0);
            if (ggr.isInterval() != this.isInterval()) {
                return false;
            }
        }
        if (this.isInterval()) {
            final List<TimeCoordWithInterval> timeList = new ArrayList<TimeCoordWithInterval>(records.size());
            for (final GridRecord record2 : records) {
                final Date ref = record2.getReferenceTime();
                if (!this.baseDate.equals(ref)) {
                    return false;
                }
                final GribGridRecord ggr2 = (GribGridRecord)record2;
                final GribPds pds = ggr2.getPds();
                final int[] timeInv = pds.getForecastTimeInterval(this.timeUnit);
                final int start = timeInv[0];
                final int end = timeInv[1];
                final int intv2 = end - start;
                final Date validTime = record2.getValidTime();
                final TimeCoordWithInterval timeCoordIntv = new TimeCoordWithInterval(validTime, start, intv2);
                if (timeList.contains(timeCoordIntv)) {
                    continue;
                }
                timeList.add(timeCoordIntv);
            }
            Collections.sort(timeList);
            return timeList.equals(this.timeIntvs);
        }
        final List<Date> timeList2 = new ArrayList<Date>(records.size());
        for (final GridRecord record2 : records) {
            Date validTime2 = record2.getValidTime();
            if (validTime2 == null) {
                validTime2 = record2.getReferenceTime();
            }
            if (!timeList2.contains(validTime2)) {
                timeList2.add(validTime2);
            }
        }
        Collections.sort(timeList2);
        return timeList2.equals(this.times);
    }
    
    void setSequence(final int seq) {
        this.seq = seq;
    }
    
    String getName() {
        return (this.seq == 0) ? "time" : ("time" + this.seq);
    }
    
    void addDimensionsToNetcdfFile(final NetcdfFile ncfile, final Group g) {
        ncfile.addDimension(g, new Dimension(this.getName(), this.getNTimes(), true));
    }
    
    void addToNetcdfFile(final NetcdfFile ncfile, Group g) {
        final Variable v = new Variable(ncfile, g, null, this.getName());
        v.setDataType(DataType.INT);
        final DateFormatter formatter = new DateFormatter();
        final String refDate = formatter.toDateTimeStringISO(this.baseDate);
        final String udunit = this.timeUdunit + " since " + refDate;
        DateUnit dateUnit = null;
        try {
            dateUnit = new DateUnit(udunit);
        }
        catch (Exception e) {
            GridTimeCoord.log.error("TimeCoord not added, cant make DateUnit from String '" + udunit + "'", e);
            return;
        }
        Array coordArray = null;
        Array boundsArray = null;
        final int ntimes = this.getNTimes();
        this.coordData = new int[ntimes];
        if (!this.isInterval()) {
            for (int i = 0; i < this.times.size(); ++i) {
                this.coordData[i] = (int)dateUnit.makeValue(this.times.get(i));
            }
            coordArray = Array.factory(DataType.INT, new int[] { ntimes }, this.coordData);
        }
        else {
            final int[] boundsData = new int[ntimes * 2];
            for (int j = 0; j < this.timeIntvs.size(); ++j) {
                final TimeCoordWithInterval tintv = this.timeIntvs.get(j);
                this.coordData[j] = tintv.start + tintv.interval;
                boundsData[2 * j + 1] = tintv.start + tintv.interval;
                boundsData[2 * j] = tintv.start;
            }
            coordArray = Array.factory(DataType.INT, new int[] { ntimes }, this.coordData);
            boundsArray = Array.factory(DataType.INT, new int[] { ntimes, 2 }, boundsData);
        }
        v.setDimensions(v.getShortName());
        v.setCachedData(coordArray, false);
        if (!this.isInterval()) {
            v.addAttribute(new Attribute("long_name", "forecast time"));
            v.addAttribute(new Attribute("units", this.timeUdunit + " since " + refDate));
        }
        else {
            final Formatter intervalName = new Formatter();
            if (this.constantInterval < 0) {
                intervalName.format("(mixed intervals)", new Object[0]);
            }
            else {
                intervalName.format("(%d %s intervals)", this.constantInterval, this.timeUdunit);
            }
            v.addAttribute(new Attribute("long_name", "forecast time for " + intervalName.toString()));
            v.addAttribute(new Attribute("units", this.timeUdunit + " since " + refDate));
            v.addAttribute(new Attribute("bounds", this.getName() + "_bounds"));
            if (g == null) {
                g = ncfile.getRootGroup();
            }
            final Dimension bd = DatasetConstructor.getBoundsDimension(ncfile);
            final Variable vb = new Variable(ncfile, g, null, this.getName() + "_bounds");
            vb.setDataType(DataType.INT);
            vb.setDimensions(this.getName() + " " + bd.getName());
            vb.addAttribute(new Attribute("long_name", "bounds for " + this.getName()));
            vb.addAttribute(new Attribute("units", this.timeUdunit + " since " + refDate));
            vb.setCachedData(boundsArray, false);
            ncfile.addVariable(g, vb);
        }
        ncfile.addVariable(g, v);
    }
    
    int findIndex(final GridRecord record) {
        final Date validTime = record.getValidTime();
        if (!this.isInterval()) {
            return this.times.indexOf(validTime);
        }
        int index = 0;
        for (final TimeCoordWithInterval t : this.timeIntvs) {
            final GribGridRecord ggr = (GribGridRecord)record;
            final GribPds pds = ggr.getPds();
            final int[] intv = pds.getForecastTimeInterval(this.timeUnit);
            final int diff = intv[1] - intv[0];
            if (t.coord.equals(validTime) && t.interval == diff) {
                return index;
            }
            ++index;
        }
        return -1;
    }
    
    int getNTimes() {
        return this.isInterval() ? this.timeIntvs.size() : this.times.size();
    }
    
    int getConstantInterval() {
        return this.constantInterval;
    }
    
    String getTimeUnit() {
        return this.timeUdunit;
    }
    
    boolean isInterval() {
        return this.timeIntvs != null;
    }
    
    public String getCoord(final int i) {
        if (this.timeIntvs == null) {
            return this.coordData[i] + " ";
        }
        final TimeCoordWithInterval ti = this.timeIntvs.get(i);
        return this.coordData[i] + "=" + ti.start + "/" + ti.interval;
    }
    
    public int compareTo(final GridTimeCoord o) {
        return o.getNTimes() - this.getNTimes();
    }
    
    static {
        GridTimeCoord.log = LoggerFactory.getLogger(GridTimeCoord.class);
    }
    
    private class TimeCoordWithInterval implements Comparable<TimeCoordWithInterval>
    {
        Date coord;
        int start;
        int interval;
        
        private TimeCoordWithInterval(final Date coord, final int start, final int interval) {
            this.coord = coord;
            this.start = start;
            this.interval = interval;
        }
        
        public int compareTo(final TimeCoordWithInterval o) {
            final int diff = this.coord.compareTo(o.coord);
            return (diff == 0) ? (o.interval - this.interval) : diff;
        }
        
        @Override
        public int hashCode() {
            return 17 * this.coord.hashCode() + this.interval;
        }
        
        @Override
        public boolean equals(final Object obj) {
            final TimeCoordWithInterval o = (TimeCoordWithInterval)obj;
            return this.coord.equals(o.coord) && this.interval == o.interval;
        }
        
        @Override
        public String toString() {
            return "start=" + this.start + ", interval=" + this.interval;
        }
    }
}
