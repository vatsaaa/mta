// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.iosp.grid;

import org.slf4j.LoggerFactory;
import ucar.nc2.units.DateFormatter;
import java.io.IOException;
import ucar.grib.grib1.Grib1Data;
import ucar.grib.grib2.Grib2Data;
import java.util.Iterator;
import ucar.grid.GridParameter;
import ucar.grib.grib1.Grib1GridTableLookup;
import ucar.ma2.Array;
import ucar.grib.grib2.Grib2Pds;
import ucar.grib.grib2.Grib2GridTableLookup;
import ucar.grib.grib2.Grib2Tables;
import ucar.nc2.iosp.grib.GribGridServiceProvider;
import ucar.nc2.constants.CF;
import ucar.nc2.Attribute;
import java.util.Formatter;
import ucar.ma2.DataType;
import ucar.nc2.Structure;
import ucar.nc2.iosp.AbstractIOServiceProvider;
import ucar.unidata.util.StringUtil;
import ucar.nc2.Variable;
import ucar.unidata.io.RandomAccessFile;
import ucar.nc2.Group;
import ucar.nc2.NetcdfFile;
import ucar.grib.GribGridRecord;
import java.util.ArrayList;
import java.util.List;
import ucar.grid.GridTableLookup;
import ucar.grid.GridRecord;
import org.slf4j.Logger;

public class GridVariable
{
    private static Logger log;
    private static boolean warnOk;
    private static boolean compareData;
    private static boolean sendAll;
    private final String filename;
    private final String name;
    private String vname;
    private GridRecord firstRecord;
    private GridTableLookup lookup;
    private GridHorizCoordSys hcs;
    private GridTimeCoord tcs;
    private GridEnsembleCoord ecs;
    private GridVertCoord vc;
    private List<GridRecord> records;
    private int nlevels;
    private int nens;
    private int ntimes;
    private GridRecord[] recordTracker;
    private boolean hasVert;
    private volatile int hashCode;
    
    GridVariable(final String filename, final String name, final GridHorizCoordSys hcs, final GridTableLookup lookup) {
        this.tcs = null;
        this.ecs = null;
        this.vc = null;
        this.records = new ArrayList<GridRecord>();
        this.hasVert = false;
        this.hashCode = 0;
        this.filename = filename;
        this.name = name;
        this.hcs = hcs;
        this.lookup = lookup;
    }
    
    void addProduct(final GridRecord record) {
        this.records.add(record);
        if (this.firstRecord == null) {
            this.firstRecord = record;
        }
    }
    
    List<GridRecord> getRecords() {
        return this.records;
    }
    
    GridRecord getFirstRecord() {
        return this.records.get(0);
    }
    
    GridHorizCoordSys getHorizCoordSys() {
        return this.hcs;
    }
    
    GridVertCoord getVertCoord() {
        return this.vc;
    }
    
    boolean hasVert() {
        return this.hasVert;
    }
    
    void setVarName(final String vname) {
        this.vname = vname;
    }
    
    void setVertCoord(final GridVertCoord vc) {
        this.vc = vc;
    }
    
    void setTimeCoord(final GridTimeCoord tcs) {
        this.tcs = tcs;
    }
    
    void setEnsembleCoord(final GridEnsembleCoord ecs) {
        this.ecs = ecs;
    }
    
    public int getNEnsembles() {
        return (this.ecs == null) ? 1 : this.ecs.getNEnsembles();
    }
    
    boolean hasEnsemble() {
        return this.ecs != null;
    }
    
    boolean isEnsemble() {
        if (this.firstRecord instanceof GribGridRecord) {
            final GribGridRecord ggr = (GribGridRecord)this.firstRecord;
            return ggr.getPds().isEnsemble();
        }
        return false;
    }
    
    int getVertNlevels() {
        return this.vc.getNLevels();
    }
    
    String getVertName() {
        return this.vc.getVariableName();
    }
    
    String getVertLevelName() {
        return this.vc.getLevelName();
    }
    
    boolean getVertIsUsed() {
        return this.vc.isVertDimensionUsed();
    }
    
    int getVertIndex(final GridRecord p) {
        return this.vc.getIndex(p);
    }
    
    int getNTimes() {
        return (this.tcs == null) ? 1 : this.tcs.getNTimes();
    }
    
    Variable makeVariable(final NetcdfFile ncfile, final Group g, String useName, final RandomAccessFile raf) {
        assert this.records.size() > 0 : "no records for this variable";
        this.nlevels = this.getVertNlevels();
        this.ntimes = this.tcs.getNTimes();
        if (this.vname == null) {
            useName = StringUtil.replace(useName, ' ', "_");
            this.vname = AbstractIOServiceProvider.createValidNetcdfObjectName(useName);
        }
        final Variable v = new Variable(ncfile, g, null, this.vname);
        v.setDataType(DataType.FLOAT);
        final Formatter dims = new Formatter();
        if (this.hasEnsemble()) {
            dims.format("ens ", new Object[0]);
        }
        dims.format("%s ", this.tcs.getName());
        if (this.getVertIsUsed()) {
            dims.format("%s ", this.getVertName());
            this.hasVert = true;
        }
        if (this.hcs.isLatLon()) {
            dims.format("lat lon", new Object[0]);
        }
        else {
            dims.format("y x", new Object[0]);
        }
        v.setDimensions(dims.toString());
        final GridParameter param = this.lookup.getParameter(this.firstRecord);
        String unit = param.getUnit();
        if (unit == null) {
            unit = "";
        }
        v.addAttribute(new Attribute("units", unit));
        v.addAttribute(new Attribute("long_name", this.makeLongName()));
        if (this.firstRecord instanceof GribGridRecord) {
            final GribGridRecord ggr = (GribGridRecord)this.firstRecord;
            if (ggr.isInterval()) {
                final CF.CellMethods cm = CF.CellMethods.convertGribCodeTable4_10(ggr.getStatisticalProcessType());
                if (cm != null) {
                    v.addAttribute(new Attribute("cell_methods", this.tcs.getName() + ": " + cm.toString()));
                }
            }
        }
        v.addAttribute(new Attribute("missing_value", new Float(this.lookup.getFirstMissingValue())));
        if (!this.hcs.isLatLon()) {
            if (GribGridServiceProvider.addLatLon) {
                v.addAttribute(new Attribute("coordinates", "lat lon"));
            }
            v.addAttribute(new Attribute("grid_mapping", this.hcs.getGridName()));
        }
        final int icf = this.hcs.getGds().getInt("VectorComponentFlag");
        String flag;
        if (icf == 0) {
            flag = Grib2Tables.VectorComponentFlag.easterlyNortherlyRelative.toString();
        }
        else {
            flag = Grib2Tables.VectorComponentFlag.gridRelative.toString();
        }
        if (this.lookup instanceof Grib2GridTableLookup) {
            final Grib2GridTableLookup g2lookup = (Grib2GridTableLookup)this.lookup;
            final GribGridRecord ggr2 = (GribGridRecord)this.firstRecord;
            final Grib2Pds pds2 = (Grib2Pds)ggr2.getPds();
            final int[] paramId = g2lookup.getParameterId(this.firstRecord);
            v.addAttribute(new Attribute("GRIB_param_discipline", this.lookup.getDisciplineName(this.firstRecord)));
            v.addAttribute(new Attribute("GRIB_param_category", this.lookup.getCategoryName(this.firstRecord)));
            v.addAttribute(new Attribute("GRIB_param_name", param.getName()));
            v.addAttribute(new Attribute("GRIB_generating_process_type", g2lookup.getGenProcessTypeName(this.firstRecord)));
            v.addAttribute(new Attribute("GRIB_param_id", Array.factory(Integer.TYPE, new int[] { paramId.length }, paramId)));
            v.addAttribute(new Attribute("GRIB_product_definition_template", pds2.getProductDefinitionTemplate()));
            v.addAttribute(new Attribute("GRIB_product_definition_template_desc", Grib2Tables.codeTable4_0(pds2.getProductDefinitionTemplate())));
            v.addAttribute(new Attribute("GRIB_level_type", new Integer(pds2.getLevelType1())));
            v.addAttribute(new Attribute("GRIB_level_type_name", this.lookup.getLevelName(this.firstRecord)));
            if (pds2.isInterval()) {
                v.addAttribute(new Attribute("GRIB_interval_stat_type", ggr2.getStatisticalProcessTypeName()));
            }
            if (pds2.isEnsembleDerived()) {
                final Grib2Pds.PdsEnsembleDerived pdsDerived = (Grib2Pds.PdsEnsembleDerived)pds2;
                v.addAttribute(new Attribute("GRIB_ensemble_derived_type", new Integer(pdsDerived.getDerivedForecastType())));
            }
            if (pds2.isEnsemble()) {
                v.addAttribute(new Attribute("GRIB_ensemble", "true"));
            }
            if (pds2.isProbability()) {
                final Grib2Pds.PdsProbability pdsProb = (Grib2Pds.PdsProbability)pds2;
                v.addAttribute(new Attribute("GRIB_probability_type", new Integer(pdsProb.getProbabilityType())));
                v.addAttribute(new Attribute("GRIB_probability_lower_limit", new Double(pdsProb.getProbabilityLowerLimit())));
                v.addAttribute(new Attribute("GRIB_probability_upper_limit", new Double(pdsProb.getProbabilityUpperLimit())));
            }
            v.addAttribute(new Attribute("GRIB_VectorComponentFlag", flag));
        }
        else if (this.lookup instanceof Grib1GridTableLookup) {
            final Grib1GridTableLookup g1lookup = (Grib1GridTableLookup)this.lookup;
            final int[] paramId2 = g1lookup.getParameterId(this.firstRecord);
            v.addAttribute(new Attribute("GRIB_param_name", param.getDescription()));
            v.addAttribute(new Attribute("GRIB_param_short_name", param.getName()));
            v.addAttribute(new Attribute("GRIB_center_id", new Integer(paramId2[1])));
            v.addAttribute(new Attribute("GRIB_table_id", new Integer(paramId2[2])));
            v.addAttribute(new Attribute("GRIB_param_number", new Integer(paramId2[3])));
            v.addAttribute(new Attribute("GRIB_param_id", Array.factory(Integer.TYPE, new int[] { paramId2.length }, paramId2)));
            v.addAttribute(new Attribute("GRIB_product_definition_type", g1lookup.getProductDefinitionName(this.firstRecord)));
            v.addAttribute(new Attribute("GRIB_level_type", new Integer(this.firstRecord.getLevelType1())));
            v.addAttribute(new Attribute("GRIB_VectorComponentFlag", flag));
        }
        else {
            v.addAttribute(new Attribute("VectorComponentFlag", flag));
        }
        v.setSPobject(this);
        int nrecs = this.ntimes * this.nlevels;
        if (this.hasEnsemble()) {
            nrecs *= this.ecs.getNEnsembles();
        }
        this.recordTracker = new GridRecord[nrecs];
        if (GridVariable.log.isDebugEnabled()) {
            GridVariable.log.debug("Record Assignment for Variable " + this.getName());
        }
        boolean oneSent = false;
        for (final GridRecord p : this.records) {
            int level = this.getVertIndex(p);
            if (!this.getVertIsUsed() && level > 0) {
                GridVariable.log.warn("inconsistent level encoding=" + level);
                level = 0;
            }
            final int time = this.tcs.findIndex(p);
            if (level < 0) {
                GridVariable.log.warn("NOT FOUND record; level=" + level + " time= " + time + " for " + this.getName() + " file=" + ncfile.getLocation() + "\n" + "   " + this.getVertLevelName() + " (type=" + p.getLevelType1() + "," + p.getLevelType2() + ")  value=" + p.getLevel1() + "," + p.getLevel2() + "\n");
                this.getVertIndex(p);
            }
            else if (time < 0) {
                GridVariable.log.warn("NOT FOUND record; level=" + level + " time= " + time + " for " + this.getName() + " file=" + ncfile.getLocation() + "\n" + " validTime= " + p.getValidTime() + "\n");
                this.tcs.findIndex(p);
            }
            else {
                int recno;
                if (this.hasEnsemble()) {
                    final GribGridRecord ggr3 = (GribGridRecord)p;
                    final int ens = this.ecs.getIndex((GribGridRecord)p);
                    recno = ens * (this.ntimes * this.nlevels) + time * this.nlevels + level;
                }
                else {
                    recno = time * this.nlevels + level;
                }
                boolean sentMessage = false;
                if (p instanceof GribGridRecord) {
                    final GribGridRecord ggp = (GribGridRecord)p;
                    if (ggp.getBelongs() != null) {
                        GridVariable.log.warn("GribGridRecord " + ggp.cdmVariableName(this.lookup, true, true) + " recno = " + recno + " already belongs to = " + ggp.getBelongs());
                    }
                    ggp.setBelongs((Object)new Belongs(recno, this));
                    if (this.recordTracker[recno] != null) {
                        final GribGridRecord ggq = (GribGridRecord)this.recordTracker[recno];
                        if (GridVariable.compareData && !this.compareData(ggq, ggp, raf)) {
                            GridVariable.log.warn("GridVariable " + this.vname + " recno = " + recno + " already has in slot = " + ggq.toString() + " with different data for " + this.filename);
                            sentMessage = true;
                        }
                    }
                }
                if (this.recordTracker[recno] == null) {
                    this.recordTracker[recno] = p;
                    if (!GridVariable.log.isDebugEnabled()) {
                        continue;
                    }
                    GridVariable.log.debug(" " + this.vc.getVariableName() + " (type=" + p.getLevelType1() + "," + p.getLevelType2() + ")  value=" + p.getLevel1() + "," + p.getLevel2());
                }
                else {
                    if (p instanceof GribGridRecord && !sentMessage && GridVariable.warnOk && !oneSent) {
                        final GribGridRecord gp = (GribGridRecord)p;
                        final GribGridRecord qp = (GribGridRecord)this.recordTracker[recno];
                        GridVariable.log.warn("Duplicate record for " + this.filename + "\n " + gp.toString() + "\n " + qp.toString());
                    }
                    if (!GridVariable.sendAll) {
                        oneSent = true;
                    }
                    this.recordTracker[recno] = p;
                }
            }
        }
        this.records.clear();
        return v;
    }
    
    private boolean compareData(final GribGridRecord ggr1, final GribGridRecord ggr2, final RandomAccessFile raf) {
        if (raf == null) {
            return false;
        }
        float[] data1 = null;
        float[] data2 = null;
        try {
            if (ggr1.getEdition() == 2) {
                final Grib2Data g2read = new Grib2Data(raf);
                data1 = g2read.getData(ggr1.getGdsOffset(), ggr1.getPdsOffset(), ggr1.getReferenceTimeInMsecs());
                data2 = g2read.getData(ggr2.getGdsOffset(), ggr2.getPdsOffset(), ggr2.getReferenceTimeInMsecs());
            }
            else {
                final Grib1Data g1read = new Grib1Data(raf);
                data1 = g1read.getData(ggr1.getGdsOffset(), ggr1.getPdsOffset(), ggr1.getDecimalScale(), ggr1.isBmsExists());
                data2 = g1read.getData(ggr2.getGdsOffset(), ggr2.getPdsOffset(), ggr2.getDecimalScale(), ggr2.isBmsExists());
            }
        }
        catch (IOException e) {
            GridVariable.log.error("Failed to read data", e);
            return false;
        }
        if (data1.length != data2.length) {
            return false;
        }
        for (int i = 0; i < data1.length; ++i) {
            if (data1[i] != data2[i] && !Double.isNaN(data1[i]) && !Double.isNaN(data2[i])) {
                return false;
            }
        }
        return true;
    }
    
    public void showRecord(final int recnum, final Formatter f) {
        if (recnum < 0 || recnum > this.recordTracker.length - 1) {
            f.format("%d out of range [0,%d]%n", recnum, this.recordTracker.length - 1);
            return;
        }
        final GridRecord gr = this.recordTracker[recnum];
        if (this.hasEnsemble()) {
            final int ens = recnum / (this.nlevels * this.ntimes);
            final int tmp = recnum - ens * (this.nlevels * this.ntimes);
            final int time = tmp / this.nlevels;
            final int level = tmp % this.nlevels;
            f.format("recnum=%d (record hash=%d) ens=%d time=%s(%d) level=%f(%d)%n", recnum, gr.hashCode(), ens, this.tcs.getCoord(time), time, this.vc.getCoord(level), level);
        }
        else {
            final int time2 = recnum / this.nlevels;
            final int level2 = recnum % this.nlevels;
            f.format("recnum=%d (record hash=%d) time=%s(%d) level=%f(%d)%n", recnum, gr.hashCode(), this.tcs.getCoord(time2), time2, this.vc.getCoord(level2), level2);
        }
    }
    
    public void showMissing(final Formatter f) {
        int count = 0;
        int total = 0;
        f.format("  %s%n", this.name);
        for (int j = 0; j < this.nlevels; ++j) {
            f.format("   ", new Object[0]);
            for (int i = 0; i < this.ntimes; ++i) {
                final boolean missing = this.recordTracker[i * this.nlevels + j] == null;
                f.format("%s", missing ? "-" : "X");
                if (missing) {
                    ++count;
                }
                ++total;
            }
            f.format("%n", new Object[0]);
        }
        f.format("  MISSING= %d / %d for %s%n", count, total, this.name);
    }
    
    public int showMissingSummary(final Formatter f) {
        int count = 0;
        final int total = this.recordTracker.length;
        for (int i = 0; i < total; ++i) {
            if (this.recordTracker[i] == null) {
                ++count;
            }
        }
        f.format("  MISSING= %d / %d for %s%n", count, total, this.name);
        return count;
    }
    
    public GridRecord findRecord(final int ens, final int time, final int level) {
        if (this.hasEnsemble()) {
            return this.recordTracker[ens * (this.ntimes * this.nlevels) + time * this.nlevels + level];
        }
        return this.recordTracker[time * this.nlevels + level];
    }
    
    @Override
    public boolean equals(final Object oo) {
        return this == oo || (oo instanceof GridVariable && this.hashCode() == oo.hashCode());
    }
    
    public String getName() {
        return this.name;
    }
    
    @Override
    public int hashCode() {
        if (this.hashCode == 0) {
            int result = 17;
            result = 37 * result + this.name.hashCode();
            result += 37 * result + this.firstRecord.getLevelType1();
            result += 37 * result + this.hcs.getID().hashCode();
            this.hashCode = result;
        }
        return this.hashCode;
    }
    
    @Override
    public String toString() {
        return (this.vname == null) ? this.name : this.vname;
    }
    
    public String dump() {
        final DateFormatter formatter = new DateFormatter();
        final Formatter sbuff = new Formatter();
        sbuff.format("%s %d %n", this.name, this.records.size());
        for (final GridRecord record : this.records) {
            sbuff.format(" level = %d %f", record.getLevelType1(), record.getLevel1());
            if (null != record.getValidTime()) {
                sbuff.format(" time = %s", formatter.toDateTimeString(record.getValidTime()));
            }
            sbuff.format("%n", new Object[0]);
        }
        return sbuff.toString();
    }
    
    private String makeLongName() {
        final Formatter f = new Formatter();
        final GridParameter param = this.lookup.getParameter(this.firstRecord);
        f.format("%s", param.getDescription());
        if (this.firstRecord instanceof GribGridRecord) {
            final GribGridRecord ggr = (GribGridRecord)this.firstRecord;
            if (ggr.getEdition() == 2) {
                final Grib2Pds pds2 = (Grib2Pds)ggr.getPds();
                final String useGenType = pds2.getUseGenProcessType();
                if (useGenType != null) {
                    f.format("_%s", useGenType);
                }
            }
            final String suffixName = ggr.makeSuffix();
            if (suffixName != null && suffixName.length() != 0) {
                f.format("%s", suffixName);
            }
            if (ggr.isInterval()) {
                final String intervalName = this.makeIntervalName();
                if (intervalName.length() != 0) {
                    final String stat = ggr.getStatisticalProcessTypeNameShort();
                    if (stat != null) {
                        f.format(" (%s for %s)", ggr.getStatisticalProcessTypeName(), intervalName);
                    }
                    else {
                        f.format(" (%s)", intervalName);
                    }
                }
            }
        }
        final String levelName = GridIndexToNC.makeLevelName(this.firstRecord, this.lookup);
        if (levelName.length() != 0) {
            f.format(" @ %s", levelName);
        }
        return f.toString();
    }
    
    private String makeIntervalName() {
        if (this.tcs.getConstantInterval() < 0) {
            return " Mixed Intervals";
        }
        return this.tcs.getConstantInterval() + " " + this.tcs.getTimeUnit() + " Intervals";
    }
    
    static {
        GridVariable.log = LoggerFactory.getLogger(GridVariable.class);
        GridVariable.warnOk = true;
        GridVariable.compareData = false;
        GridVariable.sendAll = false;
    }
    
    public class Belongs
    {
        public int recnum;
        public GridVariable gv;
        
        private Belongs(final int recnum, final GridVariable gv) {
            this.recnum = recnum;
            this.gv = gv;
        }
        
        @Override
        public String toString() {
            return "Belongs{recnum=" + this.recnum + ", gv=" + this.gv.vname + '}';
        }
    }
}
