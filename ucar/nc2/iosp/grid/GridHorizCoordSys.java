// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.iosp.grid;

import org.slf4j.LoggerFactory;
import ucar.unidata.geoloc.projection.sat.MSGnavigation;
import ucar.unidata.geoloc.projection.VerticalPerspectiveView;
import ucar.unidata.geoloc.projection.Orthographic;
import ucar.unidata.geoloc.Earth;
import ucar.unidata.geoloc.projection.RotatedLatLon;
import ucar.unidata.geoloc.projection.Mercator;
import ucar.unidata.geoloc.projection.Stereographic;
import ucar.unidata.geoloc.LatLonPoint;
import ucar.unidata.geoloc.projection.LambertConformal;
import ucar.grib.grib2.Grib2Tables;
import ucar.grib.grib1.Grib1GridTableLookup;
import ucar.grib.grib2.Grib2GridTableLookup;
import java.util.Collections;
import java.util.Collection;
import java.util.Iterator;
import ucar.unidata.geoloc.ProjectionPoint;
import ucar.unidata.geoloc.LatLonPointImpl;
import ucar.unidata.geoloc.ProjectionPointImpl;
import ucar.unidata.util.GaussianLatitudes;
import ucar.ma2.Array;
import ucar.ma2.DataType;
import ucar.nc2.Structure;
import ucar.nc2.Variable;
import ucar.nc2.constants.AxisType;
import ucar.nc2.Dimension;
import ucar.nc2.NetcdfFile;
import ucar.nc2.units.SimpleUnit;
import ucar.unidata.util.StringUtil;
import ucar.nc2.iosp.AbstractIOServiceProvider;
import java.util.ArrayList;
import java.util.HashMap;
import ucar.nc2.Attribute;
import ucar.unidata.geoloc.ProjectionImpl;
import ucar.nc2.Group;
import ucar.grid.GridDefRecord;
import ucar.grid.GridTableLookup;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;

public class GridHorizCoordSys
{
    private static Logger log;
    Map<Integer, GridVariable> varHash;
    Map<String, List<GridVariable>> productHash;
    private GridTableLookup lookup;
    private GridDefRecord gds;
    private Group g;
    private String grid_name;
    private String shape_name;
    private String id;
    private boolean isLatLon;
    private boolean isGaussian;
    private double startx;
    private double starty;
    private double incrx;
    private double incry;
    private ProjectionImpl proj;
    private List<Attribute> attributes;
    
    public GridHorizCoordSys(final GridDefRecord gds, final GridTableLookup lookup, final Group g) {
        this.varHash = new HashMap<Integer, GridVariable>(200);
        this.productHash = new HashMap<String, List<GridVariable>>(100);
        this.isLatLon = true;
        this.isGaussian = false;
        this.attributes = new ArrayList<Attribute>();
        this.gds = gds;
        this.lookup = lookup;
        this.g = g;
        this.grid_name = AbstractIOServiceProvider.createValidNetcdfObjectName(lookup.getGridName(gds));
        this.shape_name = lookup.getShapeName(gds);
        this.isLatLon = lookup.isLatLon(gds);
        this.grid_name = StringUtil.replace(this.grid_name, ' ', "_");
        this.id = ((g == null) ? this.grid_name : g.getName());
        if (this.isLatLon && lookup.getProjectionType(gds) == 8) {
            this.isGaussian = true;
            double np = gds.getDouble("Np");
            np = (Double.isNaN(np) ? 90.0 : np);
            gds.addParam("Dy", String.valueOf(np));
        }
    }
    
    public String getID() {
        return this.id;
    }
    
    public String getGridName() {
        return this.grid_name;
    }
    
    public Group getGroup() {
        return this.g;
    }
    
    public boolean isLatLon() {
        return this.isLatLon;
    }
    
    public int getNx() {
        return this.gds.getInt("Nx");
    }
    
    public int getNy() {
        return this.gds.getInt("Ny");
    }
    
    public double getDxInKm() {
        return this.getGridSpacingInKm("Dx");
    }
    
    public double getDyInKm() {
        return this.getGridSpacingInKm("Dy");
    }
    
    private double getGridSpacingInKm(final String type) {
        double value = this.gds.getDouble(type);
        if (Double.isNaN(value)) {
            return value;
        }
        final String gridUnit = this.gds.getParam("grid_units");
        SimpleUnit unit = null;
        if (gridUnit == null || gridUnit.equals("")) {
            unit = SimpleUnit.meterUnit;
        }
        else {
            unit = SimpleUnit.factory(gridUnit);
        }
        if (unit != null && SimpleUnit.isCompatible(unit.getUnitString(), "km")) {
            value = unit.convertTo(value, SimpleUnit.kmUnit);
        }
        return value;
    }
    
    void addDimensionsToNetcdfFile(final NetcdfFile ncfile) {
        if (this.isLatLon) {
            ncfile.addDimension(this.g, new Dimension("lat", this.gds.getInt("Ny"), true));
            ncfile.addDimension(this.g, new Dimension("lon", this.gds.getInt("Nx"), true));
        }
        else {
            ncfile.addDimension(this.g, new Dimension("y", this.gds.getInt("Ny"), true));
            ncfile.addDimension(this.g, new Dimension("x", this.gds.getInt("Nx"), true));
        }
    }
    
    void addToNetcdfFile(final NetcdfFile ncfile) {
        if (this.isLatLon) {
            double dy;
            if (this.gds.getDouble("Dy") == -9999.0) {
                dy = this.setLatLonDxDy();
            }
            else {
                dy = ((this.gds.getDouble("La2") < this.gds.getDouble("La1")) ? (-this.gds.getDouble("Dy")) : this.gds.getDouble("Dy"));
            }
            if (this.isGaussian) {
                this.addGaussianLatAxis(ncfile, "lat", "degrees_north", "latitude coordinate", "latitude", AxisType.Lat);
            }
            else {
                this.addCoordAxis(ncfile, "lat", this.gds.getInt("Ny"), this.gds.getDouble("La1"), dy, "degrees_north", "latitude coordinate", "latitude", AxisType.Lat);
            }
            this.addCoordAxis(ncfile, "lon", this.gds.getInt("Nx"), this.gds.getDouble("Lo1"), this.gds.getDouble("Dx"), "degrees_east", "longitude coordinate", "longitude", AxisType.Lon);
            this.addCoordSystemVariable(ncfile, "latLonCoordSys", "time lat lon");
        }
        else {
            final int projType = this.lookup.getProjectionType(this.gds);
            if (this.makeProjection(ncfile, projType)) {
                double[] yData;
                double[] xData;
                if (projType == 10) {
                    final double dy2 = (this.gds.getDouble("La2") < this.gds.getDouble("La1")) ? (-this.gds.getDouble("Dy")) : this.gds.getDouble("Dy");
                    yData = this.addCoordAxis(ncfile, "y", this.gds.getInt("Ny"), this.gds.getDouble("La1"), dy2, "degrees", "y coordinate of projection", "projection_y_coordinate", AxisType.GeoY);
                    xData = this.addCoordAxis(ncfile, "x", this.gds.getInt("Nx"), this.gds.getDouble("Lo1"), this.gds.getDouble("Dx"), "degrees", "x coordinate of projection", "projection_x_coordinate", AxisType.GeoX);
                }
                else if (projType == 7) {
                    yData = this.addCoordAxis(ncfile, "y", this.gds.getInt("Ny"), this.starty, this.incry, "km", "y coordinate of projection", "projection_y_coordinate", AxisType.GeoY);
                    xData = this.addCoordAxis(ncfile, "x", this.gds.getInt("Nx"), this.startx, this.incrx, "km", "x coordinate of projection", "projection_x_coordinate", AxisType.GeoX);
                }
                else if (projType == 100) {
                    yData = null;
                    xData = null;
                }
                else {
                    yData = this.addCoordAxis(ncfile, "y", this.gds.getInt("Ny"), this.starty, this.getDyInKm(), "km", "y coordinate of projection", "projection_y_coordinate", AxisType.GeoY);
                    xData = this.addCoordAxis(ncfile, "x", this.gds.getInt("Nx"), this.startx, this.getDxInKm(), "km", "x coordinate of projection", "projection_x_coordinate", AxisType.GeoX);
                }
                if (GridServiceProvider.addLatLon && projType != 100) {
                    this.addLatLon2D(ncfile, xData, yData);
                }
            }
        }
    }
    
    void empty() {
        this.gds = null;
        this.varHash = null;
        this.productHash = null;
    }
    
    private double[] addCoordAxis(final NetcdfFile ncfile, final String name, final int n, final double start, final double incr, final String units, final String desc, final String standard_name, final AxisType axis) {
        final Variable v = new Variable(ncfile, this.g, null, name);
        v.setDataType(DataType.DOUBLE);
        v.setDimensions(name);
        final double[] data = new double[n];
        for (int i = 0; i < n; ++i) {
            data[i] = start + incr * i;
        }
        final Array dataArray = Array.factory(DataType.DOUBLE, new int[] { n }, data);
        v.setCachedData(dataArray, false);
        v.addAttribute(new Attribute("units", units));
        v.addAttribute(new Attribute("long_name", desc));
        v.addAttribute(new Attribute("standard_name", standard_name));
        v.addAttribute(new Attribute("grid_spacing", incr + " " + units));
        v.addAttribute(new Attribute("_CoordinateAxisType", axis.toString()));
        ncfile.addVariable(this.g, v);
        return data;
    }
    
    private double[] addGaussianLatAxis(final NetcdfFile ncfile, final String name, final String units, final String desc, final String standard_name, final AxisType axis) {
        double np = this.gds.getDouble("NumberParallels");
        if (Double.isNaN(np)) {
            np = this.gds.getDouble("Np");
        }
        if (Double.isNaN(np)) {
            throw new IllegalArgumentException("Gaussian Lat/Lon grid must have 'NumberParallels' or 'Np' (number of parallels) parameter");
        }
        final double startLat = this.gds.getDouble("La1");
        final double endLat = this.gds.getDouble("La2");
        final int nlats = (int)(2.0 * np);
        final GaussianLatitudes gaussLats = new GaussianLatitudes(nlats);
        int bestStartIndex = 0;
        int bestEndIndex = 0;
        double bestStartDiff = Double.MAX_VALUE;
        double bestEndDiff = Double.MAX_VALUE;
        for (int i = 0; i < nlats; ++i) {
            double diff = Math.abs(gaussLats.latd[i] - startLat);
            if (diff < bestStartDiff) {
                bestStartDiff = diff;
                bestStartIndex = i;
            }
            diff = Math.abs(gaussLats.latd[i] - endLat);
            if (diff < bestEndDiff) {
                bestEndDiff = diff;
                bestEndIndex = i;
            }
        }
        assert Math.abs(bestEndIndex - bestStartIndex + 1) == this.gds.getInt("Ny");
        final boolean goesUp = bestEndIndex > bestStartIndex;
        Variable v = new Variable(ncfile, this.g, null, name);
        v.setDataType(DataType.DOUBLE);
        v.setDimensions(name);
        final int n = this.gds.getInt("Ny");
        int useIndex = bestStartIndex;
        final double[] data = new double[n];
        final double[] gaussw = new double[n];
        for (int j = 0; j < n; ++j) {
            data[j] = gaussLats.latd[useIndex];
            gaussw[j] = gaussLats.gaussw[useIndex];
            if (goesUp) {
                ++useIndex;
            }
            else {
                --useIndex;
            }
        }
        Array dataArray = Array.factory(DataType.DOUBLE, new int[] { n }, data);
        v.setCachedData(dataArray, false);
        v.addAttribute(new Attribute("units", units));
        v.addAttribute(new Attribute("long_name", desc));
        v.addAttribute(new Attribute("standard_name", standard_name));
        v.addAttribute(new Attribute("weights", "gaussw"));
        v.addAttribute(new Attribute("_CoordinateAxisType", axis.toString()));
        ncfile.addVariable(this.g, v);
        v = new Variable(ncfile, this.g, null, "gaussw");
        v.setDataType(DataType.DOUBLE);
        v.setDimensions(name);
        v.addAttribute(new Attribute("long_name", "gaussian weights (unnormalized)"));
        dataArray = Array.factory(DataType.DOUBLE, new int[] { n }, gaussw);
        v.setCachedData(dataArray, false);
        ncfile.addVariable(this.g, v);
        return data;
    }
    
    private void addLatLon2D(final NetcdfFile ncfile, final double[] xData, final double[] yData) {
        final Variable latVar = new Variable(ncfile, this.g, null, "lat");
        latVar.setDataType(DataType.DOUBLE);
        latVar.setDimensions("y x");
        latVar.addAttribute(new Attribute("units", "degrees_north"));
        latVar.addAttribute(new Attribute("long_name", "latitude coordinate"));
        latVar.addAttribute(new Attribute("standard_name", "latitude"));
        latVar.addAttribute(new Attribute("_CoordinateAxisType", AxisType.Lat.toString()));
        final Variable lonVar = new Variable(ncfile, this.g, null, "lon");
        lonVar.setDataType(DataType.DOUBLE);
        lonVar.setDimensions("y x");
        lonVar.addAttribute(new Attribute("units", "degrees_east"));
        lonVar.addAttribute(new Attribute("long_name", "longitude coordinate"));
        lonVar.addAttribute(new Attribute("standard_name", "longitude"));
        lonVar.addAttribute(new Attribute("_CoordinateAxisType", AxisType.Lon.toString()));
        final int nx = xData.length;
        final int ny = yData.length;
        final ProjectionPointImpl projPoint = new ProjectionPointImpl();
        final LatLonPointImpl latlonPoint = new LatLonPointImpl();
        final double[] latData = new double[nx * ny];
        final double[] lonData = new double[nx * ny];
        for (int i = 0; i < ny; ++i) {
            for (int j = 0; j < nx; ++j) {
                projPoint.setLocation(xData[j], yData[i]);
                this.proj.projToLatLon(projPoint, latlonPoint);
                latData[i * nx + j] = latlonPoint.getLatitude();
                lonData[i * nx + j] = latlonPoint.getLongitude();
            }
        }
        final Array latDataArray = Array.factory(DataType.DOUBLE, new int[] { ny, nx }, latData);
        latVar.setCachedData(latDataArray, false);
        final Array lonDataArray = Array.factory(DataType.DOUBLE, new int[] { ny, nx }, lonData);
        lonVar.setCachedData(lonDataArray, false);
        ncfile.addVariable(this.g, latVar);
        ncfile.addVariable(this.g, lonVar);
    }
    
    private boolean makeProjection(final NetcdfFile ncfile, final int projType) {
        switch (projType) {
            case 10: {
                this.makeRotatedLatLon(ncfile);
                break;
            }
            case 1: {
                this.makePS();
                break;
            }
            case 2: {
                this.makeLC();
                break;
            }
            case 3: {
                this.makeMercator();
                break;
            }
            case 7: {
                this.makeMSGgeostationary();
                break;
            }
            case 100: {
                this.makeCurvilinearAxis(ncfile);
                break;
            }
            default: {
                throw new UnsupportedOperationException("unknown projection = " + this.gds.getInt("grid_type"));
            }
        }
        final Variable v = new Variable(ncfile, this.g, null, this.grid_name);
        v.setDataType(DataType.CHAR);
        v.setDimensions("");
        final char[] data = { 'd' };
        final Array dataArray = Array.factory(DataType.CHAR, new int[0], data);
        v.setCachedData(dataArray, false);
        for (final Attribute att : this.attributes) {
            v.addAttribute(att);
        }
        v.addAttribute(new Attribute("earth_shape", this.shape_name));
        double radius_spherical_earth = this.gds.getDouble("grid_radius_spherical_earth");
        if (Double.isNaN(radius_spherical_earth)) {
            radius_spherical_earth = this.gds.getDouble("radius_spherical_earth");
        }
        if (!Double.isNaN(radius_spherical_earth)) {
            v.addAttribute(new Attribute("earth_radius", new Double(radius_spherical_earth)));
        }
        else {
            double major_axis = this.gds.getDouble("grid_major_axis_earth");
            if (Double.isNaN(major_axis)) {
                major_axis = this.gds.getDouble("major_axis_earth");
            }
            double minor_axis = this.gds.getDouble("grid_minor_axis_earth");
            if (Double.isNaN(minor_axis)) {
                minor_axis = this.gds.getDouble("minor_axis_earth");
            }
            if (!Double.isNaN(major_axis) && !Double.isNaN(minor_axis)) {
                v.addAttribute(new Attribute("semi_major_axis", new Double(major_axis)));
                v.addAttribute(new Attribute("semi_minor_axis", new Double(minor_axis)));
            }
        }
        this.addGDSparams(v);
        ncfile.addVariable(this.g, v);
        return true;
    }
    
    private void addGDSparams(final Variable v) {
        final List<String> keyList = new ArrayList<String>(this.gds.getKeys());
        Collections.sort(keyList);
        final String pre = (this.lookup instanceof Grib2GridTableLookup || this.lookup instanceof Grib1GridTableLookup) ? "GRIB" : "GDS";
        for (final String key : keyList) {
            final String name = AbstractIOServiceProvider.createValidNetcdfObjectName(pre + "_param_" + key);
            final String vals = this.gds.getParam(key);
            try {
                final int vali = Integer.parseInt(vals);
                if (key.equals("VectorComponentFlag")) {
                    String cf;
                    if (vali == 0) {
                        cf = Grib2Tables.VectorComponentFlag.easterlyNortherlyRelative.toString();
                    }
                    else {
                        cf = Grib2Tables.VectorComponentFlag.gridRelative.toString();
                    }
                    v.addAttribute(new Attribute(name, cf));
                }
                else {
                    v.addAttribute(new Attribute(name, new Integer(vali)));
                }
            }
            catch (Exception e) {
                try {
                    final double vald = Double.parseDouble(vals);
                    v.addAttribute(new Attribute(name, new Double(vald)));
                }
                catch (Exception e2) {
                    v.addAttribute(new Attribute(name, vals));
                }
            }
        }
    }
    
    private void addCoordSystemVariable(final NetcdfFile ncfile, final String name, final String dims) {
        final Variable v = new Variable(ncfile, this.g, null, name);
        v.setDataType(DataType.CHAR);
        v.setDimensions("");
        final Array dataArray = Array.factory(DataType.CHAR, new int[0], new char[] { '0' });
        v.setCachedData(dataArray, false);
        v.addAttribute(new Attribute("_CoordinateAxes", dims));
        if (this.isLatLon()) {
            v.addAttribute(new Attribute("_CoordinateTransforms", ""));
        }
        else {
            v.addAttribute(new Attribute("_CoordinateTransforms", this.getGridName()));
        }
        this.addGDSparams(v);
        ncfile.addVariable(this.g, v);
    }
    
    private void makeLC() {
        this.proj = new LambertConformal(this.gds.getDouble("Latin1"), this.gds.getDouble("LoV"), this.gds.getDouble("Latin1"), this.gds.getDouble("Latin2"));
        final LatLonPointImpl startLL = new LatLonPointImpl(this.gds.getDouble("La1"), this.gds.getDouble("Lo1"));
        final ProjectionPointImpl start = (ProjectionPointImpl)this.proj.latLonToProj(startLL);
        this.startx = start.getX();
        this.starty = start.getY();
        if (Double.isNaN(this.getDxInKm())) {
            this.setDxDy(this.startx, this.starty, this.proj);
        }
        if (GridServiceProvider.debugProj) {
            System.out.println("GridHorizCoordSys.makeLC start at latlon " + startLL);
            final double Lo2 = this.gds.getDouble("Lo2");
            final double La2 = this.gds.getDouble("La2");
            final LatLonPointImpl endLL = new LatLonPointImpl(La2, Lo2);
            System.out.println("GridHorizCoordSys.makeLC end at latlon " + endLL);
            final ProjectionPointImpl endPP = (ProjectionPointImpl)this.proj.latLonToProj(endLL);
            System.out.println("   end at proj coord " + endPP);
            final double endx = this.startx + this.getNx() * this.getDxInKm();
            final double endy = this.starty + this.getNy() * this.getDyInKm();
            System.out.println("   should be x=" + endx + " y=" + endy);
        }
        this.attributes.add(new Attribute("grid_mapping_name", "lambert_conformal_conic"));
        if (this.gds.getDouble("Latin1") == this.gds.getDouble("Latin2")) {
            this.attributes.add(new Attribute("standard_parallel", new Double(this.gds.getDouble("Latin1"))));
        }
        else {
            final double[] data = { this.gds.getDouble("Latin1"), this.gds.getDouble("Latin2") };
            this.attributes.add(new Attribute("standard_parallel", Array.factory(DataType.DOUBLE, new int[] { 2 }, data)));
        }
        this.attributes.add(new Attribute("longitude_of_central_meridian", new Double(this.gds.getDouble("LoV"))));
        this.attributes.add(new Attribute("latitude_of_projection_origin", new Double(this.gds.getDouble("Latin1"))));
    }
    
    private void makePS() {
        final String nproj = this.gds.getParam("NpProj");
        final double latOrigin = (nproj == null || nproj.equalsIgnoreCase("true")) ? 90.0 : -90.0;
        final double lad = this.gds.getDouble("LaD");
        double scale;
        if (Double.isNaN(lad)) {
            scale = 0.933;
        }
        else {
            scale = (1.0 + Math.sin(Math.toRadians(lad))) / 2.0;
        }
        this.proj = new Stereographic(latOrigin, this.gds.getDouble("LoV"), scale);
        final ProjectionPointImpl start = (ProjectionPointImpl)this.proj.latLonToProj(new LatLonPointImpl(this.gds.getDouble("La1"), this.gds.getDouble("Lo1")));
        this.startx = start.getX();
        this.starty = start.getY();
        if (Double.isNaN(this.getDxInKm())) {
            this.setDxDy(this.startx, this.starty, this.proj);
        }
        if (GridServiceProvider.debugProj) {
            System.out.printf("starting proj coord %s lat/lon %s%n", start, this.proj.projToLatLon(start));
            System.out.println("   should be LA1=" + this.gds.getDouble("La1") + " l)1=" + this.gds.getDouble("Lo1"));
        }
        this.attributes.add(new Attribute("grid_mapping_name", "polar_stereographic"));
        this.attributes.add(new Attribute("longitude_of_projection_origin", new Double(this.gds.getDouble("LoV"))));
        this.attributes.add(new Attribute("straight_vertical_longitude_from_pole", new Double(this.gds.getDouble("LoV"))));
        this.attributes.add(new Attribute("scale_factor_at_projection_origin", new Double(scale)));
        this.attributes.add(new Attribute("latitude_of_projection_origin", new Double(latOrigin)));
    }
    
    private void makeMercator() {
        double Latin = this.gds.getDouble("LaD");
        if (Double.isNaN(Latin)) {
            Latin = this.gds.getDouble("Latin");
        }
        final double Lo1 = this.gds.getDouble("Lo1");
        final double La1 = this.gds.getDouble("La1");
        this.proj = new Mercator(Lo1, Latin);
        final ProjectionPoint startP = this.proj.latLonToProj(new LatLonPointImpl(La1, Lo1));
        this.startx = startP.getX();
        this.starty = startP.getY();
        if (Double.isNaN(this.getDxInKm())) {
            this.setDxDy(this.startx, this.starty, this.proj);
        }
        this.attributes.add(new Attribute("grid_mapping_name", "mercator"));
        this.attributes.add(new Attribute("standard_parallel", Latin));
        this.attributes.add(new Attribute("longitude_of_projection_origin", Lo1));
        if (GridServiceProvider.debugProj) {
            double Lo2 = this.gds.getDouble("Lo2");
            if (Lo2 < Lo1) {
                Lo2 += 360.0;
            }
            final double La2 = this.gds.getDouble("La2");
            final LatLonPointImpl endLL = new LatLonPointImpl(La2, Lo2);
            System.out.println("GridHorizCoordSys.makeMercator: end at latlon= " + endLL);
            final ProjectionPointImpl endPP = (ProjectionPointImpl)this.proj.latLonToProj(endLL);
            System.out.println("   start at proj coord " + new ProjectionPointImpl(this.startx, this.starty));
            System.out.println("   end at proj coord " + endPP);
            final double endx = this.startx + (this.getNx() - 1) * this.getDxInKm();
            final double endy = this.starty + (this.getNy() - 1) * this.getDyInKm();
            System.out.println("   should be x=" + endx + " y=" + endy);
        }
    }
    
    private void makeRotatedLatLon(final NetcdfFile ncfile) {
        final double splat = this.gds.getDouble("SpLat");
        final double splon = this.gds.getDouble("SpLon");
        final double spangle = this.gds.getDouble("RotationAngle");
        this.proj = new RotatedLatLon(splat, splon, spangle);
        final LatLonPoint startLL = this.proj.projToLatLon(new ProjectionPointImpl(this.gds.getDouble("Lo1"), this.gds.getDouble("La1")));
        this.startx = startLL.getLongitude();
        this.starty = startLL.getLatitude();
        this.addCoordSystemVariable(ncfile, "latLonCoordSys", "time y x");
        this.attributes.add(new Attribute("grid_mapping_name", "rotated_latlon_grib"));
        this.attributes.add(new Attribute("grid_south_pole_latitude", new Double(splat)));
        this.attributes.add(new Attribute("grid_south_pole_longitude", new Double(splon)));
        this.attributes.add(new Attribute("grid_south_pole_angle", new Double(spangle)));
        if (GridServiceProvider.debugProj) {
            System.out.println("Location of pole of rotated grid:");
            System.out.println("Lon=" + splon + ", Lat=" + splat);
            System.out.println("Axial rotation about pole of rotated grid:" + spangle);
            System.out.println("Location of LL in rotated grid:");
            System.out.println("Lon=" + this.gds.getDouble("Lo1") + ", " + "Lat=" + this.gds.getDouble("La1"));
            System.out.println("Location of LL in non-rotated grid:");
            System.out.println("Lon=" + this.startx + ", Lat=" + this.starty);
            final double Lo2 = this.gds.getDouble("Lo2");
            final double La2 = this.gds.getDouble("La2");
            System.out.println("Location of UR in rotated grid:");
            System.out.println("Lon=" + Lo2 + ", Lat=" + La2);
            System.out.println("Location of UR in non-rotated grid:");
            final LatLonPoint endUR = this.proj.projToLatLon(new ProjectionPointImpl(Lo2, La2));
            System.out.println("Lon=" + endUR.getLongitude() + ", Lat=" + endUR.getLatitude());
            final double dy = (La2 < this.gds.getDouble("La1")) ? (-this.gds.getDouble("Dy")) : this.gds.getDouble("Dy");
            final double endx = this.gds.getDouble("Lo1") + (this.getNx() - 1) * this.gds.getDouble("Dx");
            final double endy = this.gds.getDouble("La1") + (this.getNy() - 1) * dy;
            System.out.println("End point rotated grid should be x=" + endx + " y=" + endy);
        }
    }
    
    private void makeSpaceViewOrOthographic() {
        final double Lat0 = this.gds.getDouble("Lap");
        final double Lon0 = this.gds.getDouble("Lop");
        final double xp = this.gds.getDouble("Xp");
        final double yp = this.gds.getDouble("Yp");
        final double dx = this.gds.getDouble("Dx");
        final double dy = this.gds.getDouble("Dy");
        double major_axis = this.gds.getDouble("grid_major_axis_earth");
        if (Double.isNaN(major_axis)) {
            major_axis = this.gds.getDouble("major_axis_earth");
        }
        double minor_axis = this.gds.getDouble("grid_minor_axis_earth");
        if (Double.isNaN(minor_axis)) {
            minor_axis = this.gds.getDouble("minor_axis_earth");
        }
        final double nr = this.gds.getDouble("Nr") * 1.0E-6;
        final double apparentDiameter = 2.0 * Math.sqrt((nr - 1.0) / (nr + 1.0));
        final double gridLengthX = major_axis * apparentDiameter / dx;
        final double gridLengthY = minor_axis * apparentDiameter / dy;
        this.gds.addParam("Dx", String.valueOf(1000.0 * gridLengthX));
        this.gds.addParam("Dx", new Double(1000.0 * gridLengthX));
        this.gds.addParam("Dy", String.valueOf(1000.0 * gridLengthY));
        this.gds.addParam("Dy", new Double(1000.0 * gridLengthY));
        this.startx = -gridLengthX * xp;
        this.starty = -gridLengthY * yp;
        final double radius = Earth.getRadius() / 1000.0;
        if (nr == 1.111111111E9) {
            this.proj = new Orthographic(Lat0, Lon0, radius);
            this.attributes.add(new Attribute("grid_mapping_name", "orthographic"));
            this.attributes.add(new Attribute("longitude_of_projection_origin", new Double(Lon0)));
            this.attributes.add(new Attribute("latitude_of_projection_origin", new Double(Lat0)));
        }
        else {
            final double height = (nr - 1.0) * radius;
            this.proj = new VerticalPerspectiveView(Lat0, Lon0, radius, height);
            this.attributes.add(new Attribute("grid_mapping_name", "vertical_perspective"));
            this.attributes.add(new Attribute("longitude_of_projection_origin", new Double(Lon0)));
            this.attributes.add(new Attribute("latitude_of_projection_origin", new Double(Lat0)));
            this.attributes.add(new Attribute("height_above_earth", new Double(height)));
        }
        if (GridServiceProvider.debugProj) {
            final double Lo2 = this.gds.getDouble("Lo2") + 360.0;
            final double La2 = this.gds.getDouble("La2");
            final LatLonPointImpl endLL = new LatLonPointImpl(La2, Lo2);
            System.out.println("GridHorizCoordSys.makeOrthographic end at latlon " + endLL);
            final ProjectionPointImpl endPP = (ProjectionPointImpl)this.proj.latLonToProj(endLL);
            System.out.println("   end at proj coord " + endPP);
            final double endx = this.startx + this.getNx() * this.getDxInKm();
            final double endy = this.starty + this.getNy() * this.getDyInKm();
            System.out.println("   should be x=" + endx + " y=" + endy);
        }
    }
    
    private void makeMSGgeostationary() {
        final double Lat0 = this.gds.getDouble("Lap");
        final double Lon0 = this.gds.getDouble("Lop");
        final int nx = this.gds.getInt("Nx");
        final int ny = this.gds.getInt("Ny");
        final int x_off = this.gds.getInt("Xp");
        final int y_off = this.gds.getInt("Yp");
        double dx = this.gds.getDouble("Dx");
        double dy = this.gds.getDouble("Dy");
        if (dy < 2100.0) {
            dx = 1207.0;
            dy = 1203.0;
        }
        else {
            dx = 3622.0;
            dy = 3610.0;
        }
        double major_axis = this.gds.getDouble("grid_major_axis_earth");
        if (Double.isNaN(major_axis)) {
            major_axis = this.gds.getDouble("major_axis_earth");
        }
        double minor_axis = this.gds.getDouble("grid_minor_axis_earth");
        if (Double.isNaN(minor_axis)) {
            minor_axis = this.gds.getDouble("minor_axis_earth");
        }
        final double nr = this.gds.getDouble("Nr") * 1.0E-6;
        final double as = 2.0 * Math.asin(1.0 / nr);
        final double cfac = dx / as;
        final double lfac = dy / as;
        final double scale_x;
        final double scale_factor = scale_x = (nr - 1.0) * major_axis / 1000.0;
        final double scale_y = -scale_factor;
        this.startx = scale_factor * (1 - x_off) / cfac;
        this.starty = scale_factor * (y_off - ny) / lfac;
        this.incrx = scale_factor / cfac;
        this.incry = scale_factor / lfac;
        this.attributes.add(new Attribute("grid_mapping_name", "MSGnavigation"));
        this.attributes.add(new Attribute("longitude_of_projection_origin", new Double(Lon0)));
        this.attributes.add(new Attribute("latitude_of_projection_origin", new Double(Lat0)));
        this.attributes.add(new Attribute("height_from_earth_center", new Double(nr * major_axis)));
        this.attributes.add(new Attribute("scale_x", new Double(scale_x)));
        this.attributes.add(new Attribute("scale_y", new Double(scale_y)));
        this.proj = new MSGnavigation(Lat0, Lon0, major_axis, minor_axis, nr * major_axis, scale_x, scale_y);
        if (GridServiceProvider.debugProj) {
            final double Lo2 = this.gds.getDouble("Lo2") + 360.0;
            final double La2 = this.gds.getDouble("La2");
            final LatLonPointImpl endLL = new LatLonPointImpl(La2, Lo2);
            System.out.println("GridHorizCoordSys.makeMSGgeostationary end at latlon " + endLL);
            final ProjectionPointImpl endPP = (ProjectionPointImpl)this.proj.latLonToProj(endLL);
            System.out.println("   end at proj coord " + endPP);
            final double endx = 1 + this.getNx();
            final double endy = 1 + this.getNy();
            System.out.println("   should be x=" + endx + " y=" + endy);
        }
    }
    
    private void makeCurvilinearAxis(final NetcdfFile ncfile) {
        final List<Variable> vars = ncfile.getRootGroup().getVariables();
        String latpp = null;
        String lonpp = null;
        String latU = null;
        String lonU = null;
        String latV = null;
        String lonV = null;
        final List<String> timeDimLL = new ArrayList<String>();
        final List<String> timeDimV = new ArrayList<String>();
        for (final Variable var : vars) {
            if (var.getName().startsWith("Latitude")) {
                final int[] shape = var.getShape();
                if (var.getRank() == 3 && shape[0] == 1) {
                    final List<Dimension> dims = var.getDimensions();
                    if (!timeDimLL.contains(dims.get(0).getName())) {
                        timeDimLL.add(dims.get(0).getName());
                    }
                    dims.remove(0);
                    var.setDimensions(dims);
                }
                var.addAttribute(new Attribute("units", "degrees_north"));
                var.addAttribute(new Attribute("long_name", "latitude coordinate"));
                var.addAttribute(new Attribute("standard_name", "latitude"));
                var.addAttribute(new Attribute("_CoordinateAxisType", AxisType.Lat.toString()));
                if (var.getName().contains("U_Wind_Component")) {
                    latU = var.getName();
                }
                else if (var.getName().contains("V_Wind_Component")) {
                    latV = var.getName();
                }
                else {
                    latpp = var.getName();
                }
            }
            else {
                if (!var.getName().startsWith("Longitude")) {
                    continue;
                }
                final int[] shape = var.getShape();
                if (var.getRank() == 3 && shape[0] == 1) {
                    final List<Dimension> dims = var.getDimensions();
                    if (!timeDimLL.contains(dims.get(0).getName())) {
                        timeDimLL.add(dims.get(0).getName());
                    }
                    dims.remove(0);
                    var.setDimensions(dims);
                }
                var.addAttribute(new Attribute("units", "degrees_east"));
                var.addAttribute(new Attribute("long_name", "longitude coordinate"));
                var.addAttribute(new Attribute("standard_name", "longitude"));
                var.addAttribute(new Attribute("_CoordinateAxisType", AxisType.Lon.toString()));
                if (var.getName().contains("U_Wind_Component")) {
                    lonU = var.getName();
                }
                else if (var.getName().contains("V_Wind_Component")) {
                    lonV = var.getName();
                }
                else {
                    lonpp = var.getName();
                }
            }
        }
        for (final Variable var : vars) {
            final List<Dimension> dims2 = var.getDimensions();
            if (var.getName().startsWith("U-component")) {
                var.addAttribute(new Attribute("coordinates", latU + " " + lonU));
                if (timeDimV.contains(dims2.get(0).getName())) {
                    continue;
                }
                timeDimV.add(dims2.get(0).getName());
            }
            else if (var.getName().startsWith("V-component")) {
                var.addAttribute(new Attribute("coordinates", latV + " " + lonV));
                if (timeDimV.contains(dims2.get(0).getName())) {
                    continue;
                }
                timeDimV.add(dims2.get(0).getName());
            }
            else {
                var.addAttribute(new Attribute("coordinates", latpp + " " + lonpp));
                if (timeDimV.contains(dims2.get(0).getName())) {
                    continue;
                }
                timeDimV.add(dims2.get(0).getName());
            }
        }
    }
    
    private void setDxDy(final double startx, final double starty, final ProjectionImpl proj) {
        final double Lo2 = this.gds.getDouble("Lo2");
        final double La2 = this.gds.getDouble("La2");
        if (Double.isNaN(Lo2) || Double.isNaN(La2)) {
            return;
        }
        final LatLonPointImpl endLL = new LatLonPointImpl(La2, Lo2);
        final ProjectionPointImpl end = (ProjectionPointImpl)proj.latLonToProj(endLL);
        final double dx = Math.abs(end.getX() - startx) / (this.gds.getInt("Nx") - 1);
        final double dy = Math.abs(end.getY() - starty) / (this.gds.getInt("Ny") - 1);
        this.gds.addParam("Dx", String.valueOf(dx));
        this.gds.addParam("Dy", String.valueOf(dy));
        this.gds.addParam("grid_units", "km");
    }
    
    private double setLatLonDxDy() {
        final double lo1 = this.gds.getDouble("Lo1");
        final double la1 = this.gds.getDouble("La1");
        double lo2 = this.gds.getDouble("Lo2");
        final double la2 = this.gds.getDouble("La2");
        if (Double.isNaN(lo2) || Double.isNaN(la2)) {
            return Double.NaN;
        }
        if (lo2 < lo1) {
            lo2 += 360.0;
        }
        final double dx = Math.abs(lo2 - lo1) / (this.gds.getInt("Nx") - 1);
        final double dy = Math.abs(la2 - la1) / (this.gds.getInt("Ny") - 1);
        this.gds.addParam("Dx", String.valueOf(dx));
        this.gds.addParam("Dy", String.valueOf(dy));
        this.gds.addParam("Dx", new Double(dx));
        this.gds.addParam("Dy", new Double(dy));
        this.gds.addParam("grid_units", "degree");
        return dy;
    }
    
    public GridDefRecord getGds() {
        return this.gds;
    }
    
    static {
        GridHorizCoordSys.log = LoggerFactory.getLogger(GridHorizCoordSys.class);
    }
}
