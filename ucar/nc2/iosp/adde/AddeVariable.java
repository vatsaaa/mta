// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.iosp.adde;

import ucar.nc2.Group;
import ucar.ma2.DataType;
import ucar.nc2.Structure;
import ucar.nc2.dataset.NetcdfDataset;
import ucar.nc2.dataset.VariableDS;

public class AddeVariable extends VariableDS
{
    private int nparam;
    
    public AddeVariable(final NetcdfDataset ncfile, final Structure parentStructure, final String shortName, final DataType dataType, final String dims, final String units, final String desc, final int nparam) {
        super(ncfile, null, parentStructure, shortName, dataType, dims, units, desc);
        this.nparam = nparam;
    }
    
    int getParam() {
        return this.nparam;
    }
}
