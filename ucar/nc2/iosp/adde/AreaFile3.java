// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.iosp.adde;

import java.awt.geom.Rectangle2D;
import edu.wisc.ssec.mcidas.TANCnav;
import edu.wisc.ssec.mcidas.LAMBnav;
import edu.wisc.ssec.mcidas.MERCnav;
import edu.wisc.ssec.mcidas.PSnav;
import edu.wisc.ssec.mcidas.GOESnav;
import edu.wisc.ssec.mcidas.GMSXnav;
import edu.wisc.ssec.mcidas.RECTnav;
import edu.wisc.ssec.mcidas.RADRnav;
import edu.wisc.ssec.mcidas.MSATnav;
import edu.wisc.ssec.mcidas.MOLLnav;
import edu.wisc.ssec.mcidas.GVARnav;
import ucar.ma2.Index;
import ucar.ma2.ArrayInt;
import ucar.ma2.ArrayShort;
import edu.wisc.ssec.mcidas.AreaFileException;
import edu.wisc.ssec.mcidas.McIDASUtil;
import java.net.MalformedURLException;
import java.io.IOException;
import java.net.URL;
import java.io.InputStream;
import java.io.DataInputStream;
import java.io.BufferedInputStream;
import ucar.nc2.util.net.URLStreamHandlerFactory;
import edu.wisc.ssec.mcidas.AREAnav;
import edu.wisc.ssec.mcidas.AreaDirectory;
import ucar.ma2.Array;

public class AreaFile3
{
    private static final int indexLine = 1;
    private static final int indexEle = 0;
    private static final int indexLat = 0;
    private static final int indexLon = 1;
    private int numBands;
    private int numLines;
    private int numElems;
    private int dataSize;
    private int datLoc;
    private int linePrefixLength;
    private int lineDataLength;
    private int lineLength;
    private long position;
    private int[] dir;
    private int[] navBlock;
    private int[] cal;
    private int[] aux;
    private Array data;
    private AreaDirectory areaDirectory;
    private AREAnav areaNavigation;
    private String imageSource;
    private boolean showBBcalc;
    
    public AreaFile3(final String urlString) throws IOException, MalformedURLException {
        this.position = 0L;
        this.areaDirectory = null;
        this.areaNavigation = null;
        this.showBBcalc = false;
        final long timeStart = System.currentTimeMillis();
        final URL url = URLStreamHandlerFactory.makeURL(urlString);
        final DataInputStream af = new DataInputStream(new BufferedInputStream(url.openStream()));
        this.readMetaData(af);
        this.readData(af);
    }
    
    public Array getData() {
        return this.data;
    }
    
    private void readMetaData(final DataInputStream af) throws IOException {
        boolean flipwords = false;
        this.dir = new int[64];
        for (int i = 0; i < 64; ++i) {
            this.dir[i] = af.readInt();
        }
        this.position += 256L;
        if (this.dir[1] != 4) {
            McIDASUtil.flip(this.dir, 0, 19);
            if (this.dir[1] != 4) {
                throw new IOException("Invalid version number - probably not an AREA file");
            }
            if ((this.dir[20] & 0xFFFF) == 0x0) {
                McIDASUtil.flip(this.dir, 20, 20);
            }
            McIDASUtil.flip(this.dir, 21, 23);
            McIDASUtil.flip(this.dir, 32, 50);
            McIDASUtil.flip(this.dir, 53, 55);
            McIDASUtil.flip(this.dir, 57, 63);
            flipwords = true;
        }
        try {
            this.areaDirectory = new AreaDirectory(this.dir);
        }
        catch (AreaFileException afe) {
            throw new IOException(afe.getMessage());
        }
        this.numBands = this.dir[13];
        this.numLines = this.dir[8];
        this.numElems = this.dir[9];
        this.dataSize = this.dir[10];
        final int navLoc = this.dir[34];
        final int calLoc = this.dir[62];
        final int auxLoc = this.dir[59];
        this.datLoc = this.dir[33];
        this.linePrefixLength = this.dir[48] + this.dir[49] + this.dir[50];
        if (this.dir[35] != 0) {
            this.linePrefixLength += 4;
        }
        if (this.linePrefixLength != this.dir[14]) {
            throw new IOException("Invalid line prefix length in AREA file.");
        }
        this.lineDataLength = this.numBands * this.numElems * this.dir[10];
        this.lineLength = this.linePrefixLength + this.lineDataLength;
        int navbytes = 0;
        int calbytes = 0;
        int auxbytes = 0;
        if (this.datLoc > 0 && this.datLoc != -2139062144) {
            navbytes = this.datLoc - navLoc;
            calbytes = this.datLoc - calLoc;
            auxbytes = this.datLoc - auxLoc;
        }
        if (auxLoc > 0 && auxLoc != -2139062144) {
            navbytes = auxLoc - navLoc;
            calbytes = auxLoc - calLoc;
        }
        if (calLoc > 0 && calLoc != -2139062144) {
            navbytes = calLoc - navLoc;
        }
        if (navLoc > 0 && navbytes > 0) {
            this.navBlock = new int[navbytes / 4];
            final long newPosition = navLoc;
            final int skipByteCount = (int)(newPosition - this.position);
            af.skipBytes(skipByteCount);
            for (int i = 0; i < navbytes / 4; ++i) {
                this.navBlock[i] = af.readInt();
            }
            if (flipwords) {
                this.flipnav(this.navBlock);
            }
            this.position = navLoc + navbytes;
        }
        if (calLoc > 0 && calbytes > 0) {
            this.cal = new int[calbytes / 4];
            final long newPosition = calLoc;
            final int skipByteCount = (int)(newPosition - this.position);
            af.skipBytes(skipByteCount);
            for (int i = 0; i < calbytes / 4; ++i) {
                this.cal[i] = af.readInt();
            }
            this.position = calLoc + calbytes;
        }
        if (auxLoc > 0 && auxbytes > 0) {
            this.aux = new int[auxbytes / 4];
            final long newPosition = auxLoc;
            final int skipByteCount = (int)(newPosition - this.position);
            af.skipBytes(skipByteCount);
            for (int i = 0; i < auxbytes / 4; ++i) {
                this.aux[i] = af.readInt();
            }
            this.position = auxLoc + auxbytes;
        }
    }
    
    public int[] getDir() {
        return this.dir;
    }
    
    public AreaDirectory getAreaDirectory() {
        return this.areaDirectory;
    }
    
    public int[] getCal() throws AreaFileException {
        return this.cal;
    }
    
    public int[] getAux() throws AreaFileException {
        return this.aux;
    }
    
    private void readData(final DataInputStream af) {
        if (this.dataSize == 1) {
            this.data = new ArrayShort.D3(this.numBands, this.numLines, this.numElems);
        }
        else if (this.dataSize == 2) {
            this.data = new ArrayShort.D3(this.numBands, this.numLines, this.numElems);
        }
        else if (this.dataSize == 4) {
            this.data = new ArrayInt.D3(this.numBands, this.numLines, this.numElems);
        }
        final Index ima = this.data.getIndex();
        for (int i = 0; i < this.numLines; ++i) {
            try {
                final long newPosition = this.datLoc + this.linePrefixLength + i * this.lineLength;
                final int skipByteCount = (int)(newPosition - this.position);
                af.skipBytes(skipByteCount);
                this.position = newPosition;
            }
            catch (IOException e) {
                e.printStackTrace();
                break;
            }
            for (int j = 0; j < this.numElems; ++j) {
                for (int k = 0; k < this.numBands; ++k) {
                    if (j > this.lineDataLength) {
                        this.data.setInt(ima.set(k, i, j), 0);
                    }
                    else {
                        try {
                            if (this.dataSize == 1) {
                                short ival = af.readByte();
                                if (ival < 0) {
                                    ival += 256;
                                }
                                this.data.setShort(ima.set(k, i, j), ival);
                                ++this.position;
                            }
                            if (this.dataSize == 2) {
                                this.data.setShort(ima.set(k, i, j), af.readShort());
                                this.position += 2L;
                            }
                            if (this.dataSize == 4) {
                                this.data.setInt(ima.set(k, i, j), af.readInt());
                                this.position += 4L;
                            }
                        }
                        catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }
    }
    
    private void flipnav(final int[] nav) {
        if (nav[0] == 1196835154) {
            McIDASUtil.flip(nav, 2, 126);
            McIDASUtil.flip(nav, 129, 254);
            McIDASUtil.flip(nav, 257, 382);
            McIDASUtil.flip(nav, 385, 510);
            McIDASUtil.flip(nav, 513, 638);
        }
        else if (nav[0] == 1145918032) {
            McIDASUtil.flip(nav, 1, 43);
            McIDASUtil.flip(nav, 45, 51);
        }
        else if (nav[0] == 1414091343) {
            McIDASUtil.flip(nav, 1, 119);
        }
        else {
            McIDASUtil.flip(nav, 1, nav.length - 1);
        }
    }
    
    public AREAnav getAreaNavigation() throws AreaFileException {
        if (this.areaNavigation != null) {
            return this.areaNavigation;
        }
        try {
            switch (this.navBlock[0]) {
                case 1196835154: {
                    this.areaNavigation = (AREAnav)new GVARnav(this.navBlock);
                    break;
                }
                case 1297042508: {
                    this.areaNavigation = (AREAnav)new MOLLnav(this.navBlock);
                    break;
                }
                case 1297301844: {
                    this.areaNavigation = (AREAnav)new MSATnav(this.navBlock);
                    break;
                }
                case 1380009042: {
                    this.areaNavigation = (AREAnav)new RADRnav(this.navBlock);
                    break;
                }
                case 1380270932: {
                    this.areaNavigation = (AREAnav)new RECTnav(this.navBlock);
                    break;
                }
                case 1196249944: {
                    this.areaNavigation = (AREAnav)new GMSXnav(this.navBlock);
                    break;
                }
                case 1196377427: {
                    this.areaNavigation = (AREAnav)new GOESnav(this.navBlock);
                    break;
                }
                case 1347624992: {
                    this.areaNavigation = (AREAnav)new PSnav(this.navBlock);
                    break;
                }
                case 1296388675: {
                    this.areaNavigation = (AREAnav)new MERCnav(this.navBlock);
                    break;
                }
                case 1279348034: {
                    this.areaNavigation = (AREAnav)new LAMBnav(this.navBlock);
                    break;
                }
                case 1413566019: {
                    this.areaNavigation = (AREAnav)new TANCnav(this.navBlock);
                    break;
                }
                default: {
                    throw new AreaFileException("AreaFile2.getAreaNav: Unknown navigation type" + this.navBlock[0]);
                }
            }
        }
        catch (IllegalArgumentException excp) {
            throw new AreaFileException("AreaFile2.getAreaNav: bad nav block " + excp.getMessage());
        }
        this.areaNavigation.setImageStart(this.dir[5], this.dir[6]);
        this.areaNavigation.setRes(this.dir[11], this.dir[12]);
        this.areaNavigation.setStart(1, 1);
        this.areaNavigation.setMag(1, 1);
        return this.areaNavigation;
    }
    
    public Rectangle2D getBoundingBox() throws AreaFileException {
        final AREAnav anav = this.getAreaNavigation();
        final double[][] linelem = this.makeArea(this.numElems, this.numLines);
        final int size = linelem[0].length;
        final double[][] latlon = anav.toLatLon(linelem);
        double maxLon = -1.7976931348623157E308;
        double maxLat = Double.MIN_VALUE;
        double minLon = -1.7976931348623157E308;
        double minLat = Double.MAX_VALUE;
        for (int i = 0; i < size; ++i) {
            final double lat = latlon[0][i];
            final double lon = latlon[1][i];
            if (this.showBBcalc && i % 300 == 0) {
                System.out.println("  " + linelem[0][i] + " (elem) " + linelem[1][i] + " (line) = " + latlon[0][i] + " (lat) " + latlon[1][i] + " (lon) ");
            }
            if (!Double.isNaN(lat)) {
                if (lat > maxLat) {
                    maxLat = lat;
                }
                if (lat < minLat) {
                    minLat = lat;
                }
            }
            if (!Double.isNaN(lon)) {
                if (lon > maxLon) {
                    maxLon = lon;
                }
                if (lon < minLon) {
                    minLon = lon;
                }
            }
        }
        return new Rectangle2D.Double(minLon, minLat, maxLon - minLon, maxLat - minLat);
    }
    
    private double[][] makePerimeter(final int numElems, final int numLines) {
        final int size = 2 * (numElems + numLines);
        final double[][] linelem = new double[2][size];
        int count = 0;
        for (int i = 0; i < numElems; ++i) {
            linelem[1][count] = 0.0;
            linelem[0][count] = i;
            ++count;
        }
        for (int i = 0; i < numElems; ++i) {
            linelem[1][count] = numLines - 1;
            linelem[0][count] = i;
            ++count;
        }
        for (int j = 0; j < numLines; ++j) {
            linelem[1][count] = j;
            linelem[0][count] = 0.0;
            ++count;
        }
        for (int j = 0; j < numLines; ++j) {
            linelem[1][count] = j;
            linelem[0][count] = numElems - 1;
            ++count;
        }
        return linelem;
    }
    
    private double[][] makeArea(final int numElems, final int numLines) {
        final int size = numElems * numLines;
        final double[][] linelem = new double[2][size];
        int count = 0;
        for (int j = 0; j < numLines; ++j) {
            for (int i = 0; i < numElems; ++i) {
                linelem[1][count] = j;
                linelem[0][count] = i;
                ++count;
            }
        }
        return linelem;
    }
}
