// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.iosp.adde;

import edu.wisc.ssec.mcidas.McIDASUtil;
import ucar.ma2.DataType;
import ucar.ma2.StructureData;
import ucar.ma2.Array;
import ucar.ma2.Index;
import ucar.ma2.StructureMembers;
import ucar.ma2.ArrayStructure;

public class ArrayStructureAdde extends ArrayStructure
{
    private int MISSING_VALUE_INT;
    private double MISSING_VALUE_DOUBLE;
    protected int[][] data;
    protected double[] scaleFactor;
    
    public ArrayStructureAdde(final StructureMembers members, final int[] shape, final int[][] data, final double[] scaleFactor) {
        super(members, shape);
        this.MISSING_VALUE_INT = -9999;
        this.MISSING_VALUE_DOUBLE = Double.NaN;
        this.data = data;
        this.scaleFactor = scaleFactor;
    }
    
    @Override
    public Array createView(final Index index) {
        return new ArrayStructureAdde(this.members, index, this.nelems, this.sdata, this.data, this.scaleFactor);
    }
    
    @Override
    protected StructureData makeStructureData(final ArrayStructure as, final int index) {
        return null;
    }
    
    ArrayStructureAdde(final StructureMembers members, final Index ima, final int nelems, final StructureData[] sdata, final int[][] storage, final double[] scaleFactor) {
        super(members, ima);
        this.MISSING_VALUE_INT = -9999;
        this.MISSING_VALUE_DOUBLE = Double.NaN;
        this.nelems = nelems;
        this.sdata = sdata;
        this.data = storage;
        this.scaleFactor = scaleFactor;
    }
    
    @Override
    public Object getStorage() {
        return this.data;
    }
    
    @Override
    public double getScalarDouble(final int recnum, final StructureMembers.Member m) {
        if (m.getDataType() != DataType.DOUBLE) {
            throw new IllegalArgumentException("Type is " + m.getDataType() + ", must be double");
        }
        final int param = m.getDataParam();
        final int v = this.data[param][recnum];
        if (v == -2139062144) {
            return this.MISSING_VALUE_DOUBLE;
        }
        if (this.scaleFactor[param] != 0.0) {
            return v * this.scaleFactor[param];
        }
        return v;
    }
    
    @Override
    public double[] getJavaArrayDouble(final int recnum, final StructureMembers.Member m) {
        final double[] data = { this.getScalarDouble(recnum, m) };
        return data;
    }
    
    @Override
    public float getScalarFloat(final int recnum, final StructureMembers.Member m) {
        throw new IllegalArgumentException("Unsupported type");
    }
    
    @Override
    public float[] getJavaArrayFloat(final int recnum, final StructureMembers.Member m) {
        throw new IllegalArgumentException("Unsupported type");
    }
    
    @Override
    public byte getScalarByte(final int recnum, final StructureMembers.Member m) {
        throw new IllegalArgumentException("Unsupported type");
    }
    
    @Override
    public byte[] getJavaArrayByte(final int recnum, final StructureMembers.Member m) {
        throw new IllegalArgumentException("Unsupported type");
    }
    
    @Override
    public short getScalarShort(final int recnum, final StructureMembers.Member m) {
        throw new IllegalArgumentException("Unsupported type");
    }
    
    @Override
    public short[] getJavaArrayShort(final int recnum, final StructureMembers.Member m) {
        throw new IllegalArgumentException("Unsupported type");
    }
    
    @Override
    public int getScalarInt(final int recnum, final StructureMembers.Member m) {
        if (m.getDataType() != DataType.INT) {
            throw new IllegalArgumentException("Type is " + m.getDataType() + ", must be int");
        }
        final int param = m.getDataParam();
        final int v = this.data[param][recnum];
        if (v == -2139062144) {
            return this.MISSING_VALUE_INT;
        }
        return v;
    }
    
    @Override
    public int[] getJavaArrayInt(final int recnum, final StructureMembers.Member m) {
        final int[] data = { this.getScalarInt(recnum, m) };
        return data;
    }
    
    @Override
    public long getScalarLong(final int recnum, final StructureMembers.Member m) {
        throw new IllegalArgumentException("Unsupported type");
    }
    
    @Override
    public long[] getJavaArrayLong(final int recnum, final StructureMembers.Member m) {
        throw new IllegalArgumentException("Unsupported type");
    }
    
    @Override
    public char getScalarChar(final int recnum, final StructureMembers.Member m) {
        throw new IllegalArgumentException("Unsupported type");
    }
    
    @Override
    public char[] getJavaArrayChar(final int recnum, final StructureMembers.Member m) {
        throw new IllegalArgumentException("Unsupported type");
    }
    
    @Override
    public String getScalarString(final int recnum, final StructureMembers.Member m) {
        if (m.getDataType() == DataType.CHAR || m.getDataType() == DataType.STRING) {
            final int param = m.getDataParam();
            return McIDASUtil.intBitsToString(this.data[param][recnum]);
        }
        throw new IllegalArgumentException("Type is " + m.getDataType() + ", must be String or char");
    }
    
    @Override
    public String[] getJavaArrayString(final int recnum, final StructureMembers.Member m) {
        return new String[0];
    }
    
    @Override
    public StructureData getScalarStructure(final int recnum, final StructureMembers.Member m) {
        return null;
    }
    
    @Override
    public ArrayStructure getArrayStructure(final int recnum, final StructureMembers.Member m) {
        return null;
    }
}
