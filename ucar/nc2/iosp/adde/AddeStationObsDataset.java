// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.iosp.adde;

import ucar.nc2.dt.point.StationObsDatatypeImpl;
import ucar.nc2.Attribute;
import ucar.nc2.VariableSimpleIF;
import ucar.nc2.dt.DataIterator;
import ucar.unidata.geoloc.LatLonPointImpl;
import ucar.unidata.geoloc.LatLonPoint;
import ucar.unidata.geoloc.LatLonRect;
import ucar.ma2.IndexIterator;
import ucar.ma2.ArrayStructure;
import ucar.ma2.StructureData;
import java.util.ArrayList;
import ucar.unidata.geoloc.Station;
import java.util.List;
import visad.jmet.MetUnits;
import ucar.ma2.DataType;
import ucar.nc2.units.DateUnit;
import java.util.TimeZone;
import ucar.nc2.units.DateRange;
import thredds.catalog.ThreddsMetadata;
import thredds.catalog.InvDataset;
import java.util.Date;
import thredds.catalog.InvDatasetImpl;
import ucar.nc2.util.CancelTask;
import thredds.catalog.InvAccess;
import edu.wisc.ssec.mcidas.adde.AddeException;
import java.io.IOException;
import edu.wisc.ssec.mcidas.adde.AddePointDataReader;
import ucar.ma2.StructureMembers;
import java.util.GregorianCalendar;
import ucar.nc2.dt.point.StationObsDatasetImpl;

public class AddeStationObsDataset extends StationObsDatasetImpl
{
    private String addeURL;
    private StationDB stationDB;
    private String stationDBlocation;
    private GregorianCalendar calendar;
    private double[] scaleFactor;
    private StructureMembers members;
    private boolean debugHead;
    private boolean debugAddeCall;
    
    static AddePointDataReader callAdde(final String request) throws IOException {
        try {
            System.out.println("Call ADDE request= " + request);
            final long start = System.currentTimeMillis();
            final AddePointDataReader reader = new AddePointDataReader(request);
            System.out.println(" took= " + (System.currentTimeMillis() - start) + " msec");
            return reader;
        }
        catch (AddeException e) {
            e.printStackTrace();
            throw new IOException(e.getMessage());
        }
    }
    
    public AddeStationObsDataset(final InvAccess access, final CancelTask cancelTask) throws IOException {
        this.debugHead = false;
        this.debugAddeCall = false;
        final InvDataset invDs = access.getDataset();
        this.location = ((invDs.getID() != null) ? ("thredds:" + access.getDataset().getCatalogUrl()) : access.getStandardUrlName());
        this.addeURL = access.getStandardUrlName();
        final InvDataset invds = access.getDataset();
        final String pv = invds.findProperty("_StationDBlocation");
        if (pv != null) {
            this.stationDBlocation = InvDatasetImpl.resolve(invds, pv);
        }
        this.init();
        final ThreddsMetadata.GeospatialCoverage geoCoverage = invds.getGeospatialCoverage();
        if (null != geoCoverage) {
            this.boundingBox = geoCoverage.getBoundingBox();
        }
        else {
            this.boundingBox = this.stationHelper.getBoundingBox();
        }
        final DateRange timeCoverage = invds.getTimeCoverage();
        if (timeCoverage != null) {
            this.startDate = timeCoverage.getStart().getDate();
            this.endDate = timeCoverage.getEnd().getDate();
        }
        else {
            this.startDate = new Date(0L);
            this.endDate = new Date();
        }
    }
    
    public AddeStationObsDataset(final String location, final CancelTask cancelTask) throws IOException {
        this.debugHead = false;
        this.debugAddeCall = false;
        this.location = location;
        this.addeURL = location;
        this.init();
        this.startDate = new Date(0L);
        this.endDate = new Date();
        this.boundingBox = this.stationHelper.getBoundingBox();
    }
    
    private void init() throws IOException {
        this.members = new StructureMembers("stationObs");
        (this.calendar = new GregorianCalendar()).setTimeZone(TimeZone.getTimeZone("GMT"));
        this.timeUnit = DateUnit.getUnixDateUnit();
        try {
            final AddePointDataReader reader = callAdde(this.addeURL);
            final String[] params = reader.getParams();
            final String[] units = reader.getUnits();
            final int[] scales = reader.getScales();
            this.scaleFactor = new double[params.length];
            if (this.debugHead) {
                System.out.println(" Param  Unit Scale");
            }
            for (int paramNo = 0; paramNo < params.length; ++paramNo) {
                if (this.debugHead) {
                    System.out.println(" " + params[paramNo] + " " + units[paramNo] + " " + scales[paramNo]);
                }
                if (scales[paramNo] != 0) {
                    this.scaleFactor[paramNo] = 1.0 / Math.pow(10.0, scales[paramNo]);
                }
                DataType dt = null;
                if ("CHAR".equals(units[paramNo])) {
                    dt = DataType.STRING;
                }
                else if (this.scaleFactor[paramNo] == 0.0) {
                    dt = DataType.INT;
                }
                else {
                    dt = DataType.DOUBLE;
                }
                String unitString = null;
                if (units[paramNo] != null && units[paramNo].length() > 0) {
                    unitString = MetUnits.makeSymbol(units[paramNo]);
                }
                final AddeTypedDataVariable tdv = new AddeTypedDataVariable(params[paramNo], unitString, dt);
                this.dataVariables.add(tdv);
                final StructureMembers.Member m = this.members.addMember(tdv.getShortName(), tdv.getDescription(), tdv.getUnitsString(), tdv.getDataType(), tdv.getShape());
                m.setDataParam(paramNo);
                this.members.addMember(m);
            }
        }
        catch (AddeException e) {
            e.printStackTrace();
            throw new IOException(e.getMessage());
        }
    }
    
    @Override
    protected void setTimeUnits() {
    }
    
    @Override
    protected void setStartDate() {
    }
    
    @Override
    protected void setEndDate() {
    }
    
    @Override
    protected void setBoundingBox() {
    }
    
    private ArrayStructureAdde readData(final String selectClause, final CancelTask cancel) {
        try {
            final String urlString = (selectClause == null) ? (this.addeURL + "&num=all") : (this.addeURL + "&num=all&select='" + selectClause + "'");
            final AddePointDataReader reader = callAdde(urlString);
            final int[][] stationObsData = reader.getData();
            final int nparams = stationObsData.length;
            final int nobs = stationObsData[0].length;
            if (this.debugAddeCall) {
                System.out.println("CALL ADDE= " + urlString);
                System.out.println(" nparams= " + nparams + " nobs=" + nobs);
                System.out.println(" size= " + nparams * nobs * 4 + " bytes");
            }
            return new ArrayStructureAdde(this.members, new int[] { nobs }, stationObsData, this.scaleFactor);
        }
        catch (AddeException e) {
            e.printStackTrace();
            throw new RuntimeException(e.getMessage());
        }
        catch (IOException e2) {
            e2.printStackTrace();
            throw new RuntimeException(e2.getMessage());
        }
    }
    
    @Override
    public List getStations(final CancelTask cancel) throws IOException {
        if (this.stationDB == null) {
            this.readStations(cancel);
        }
        return this.stationDB.getStations();
    }
    
    @Override
    public int getStationDataCount(final Station s) {
        return -1;
    }
    
    public List getData(final CancelTask cancel) throws IOException {
        return null;
    }
    
    public int getDataCount() {
        return -1;
    }
    
    public List getData(final Station s, final CancelTask cancel) throws IOException {
        final ArrayStructure stationData = this.readData("ID " + s.getName(), cancel);
        final ArrayList stationObs = new ArrayList();
        final IndexIterator ii = stationData.getIndexIterator();
        while (ii.hasNext()) {
            stationObs.add(new StationObs(s, (StructureData)ii.getObjectNext()));
        }
        return stationObs;
    }
    
    @Override
    public List getData(final Station s, final Date start, final Date end, final CancelTask cancel) throws IOException {
        final double startTime = this.timeUnit.makeValue(start);
        final double endTime = this.timeUnit.makeValue(end);
        return this.stationHelper.getStationObs(s, startTime, endTime, cancel);
    }
    
    @Override
    public List getData(final List stations, final CancelTask cancel) throws IOException {
        return this.stationHelper.getStationObs(stations, cancel);
    }
    
    @Override
    public List getData(final List stations, final Date start, final Date end, final CancelTask cancel) throws IOException {
        final double startTime = this.timeUnit.makeValue(start);
        final double endTime = this.timeUnit.makeValue(end);
        return this.stationHelper.getStationObs(stations, startTime, endTime, cancel);
    }
    
    @Override
    public List getData(final LatLonRect boundingBox, final CancelTask cancel) throws IOException {
        return this.stationHelper.getStationObs(boundingBox, cancel);
    }
    
    @Override
    public List getData(final LatLonRect boundingBox, final Date start, final Date end, final CancelTask cancel) throws IOException {
        final double startTime = this.timeUnit.makeValue(start);
        final double endTime = this.timeUnit.makeValue(end);
        return this.stationHelper.getStationObs(boundingBox, startTime, endTime, cancel);
    }
    
    private static void makeSelectBB(final StringBuffer sbuff, final LatLonRect bb) {
        final LatLonPoint ll = bb.getLowerLeftPoint();
        final LatLonPoint ur = bb.getUpperRightPoint();
        sbuff.append("LAT ");
        sbuff.append(ll.getLatitude());
        sbuff.append(" ");
        sbuff.append(ur.getLatitude());
        sbuff.append(";LON ");
        sbuff.append(ll.getLongitude());
        sbuff.append(" ");
        sbuff.append(ur.getLongitude());
    }
    
    private void makeSelectTime(final StringBuffer sbuff, final Date begin, final Date end) {
        sbuff.append("DAY ");
    }
    
    private void makeSelectStations(final StringBuffer sbuff, final List stations) {
        sbuff.append("ID ");
        for (int i = 0; i < stations.size(); ++i) {
            final Station s = stations.get(i);
            if (i > 0) {
                sbuff.append(",");
            }
            sbuff.append(s.getName());
        }
    }
    
    private void readStations(final CancelTask cancel) throws IOException {
        try {
            if (this.stationDBlocation != null) {
                this.stationDB = new StationDB(this.stationDBlocation);
            }
        }
        catch (IOException ioe) {
            System.out.println("++ AddeStationDataset cant find stationDBlocation= " + this.stationDBlocation);
        }
        if (this.stationDB == null) {
            this.stationDB = new StationDB(this.location, cancel);
        }
    }
    
    static void test(final String urlString) {
        try {
            final long start = System.currentTimeMillis();
            System.out.println(" get " + urlString);
            final AddePointDataReader reader = new AddePointDataReader(urlString);
            System.out.println(" took= " + (System.currentTimeMillis() - start) + " msec");
            System.out.println(reader.toString());
            System.out.println(" Param  Unit Scale");
            final String[] params = reader.getParams();
            final String[] units = reader.getUnits();
            final int[] scales = reader.getScales();
            for (int i = 0; i < params.length; ++i) {
                System.out.println(" " + params[i] + " " + units[i] + " " + scales[i]);
            }
            final int[][] data = reader.getData();
            System.out.println(" nparams= " + params.length);
            System.out.println(" n= " + data.length);
            System.out.println(" m= " + data[0].length);
        }
        catch (AddeException e) {
            e.printStackTrace();
        }
    }
    
    public static void main(final String[] args) {
        final String loc = "adde://adde.ucar.edu/point?group=rtptsrc&descr=sfchourly&num=10";
        final StringBuffer sbuff = new StringBuffer();
        sbuff.append(loc);
        sbuff.append("&num=all&select='");
        makeSelectBB(sbuff, new LatLonRect(new LatLonPointImpl(10.0, 10.0), new LatLonPointImpl(20.0, 20.0)));
        sbuff.append("'");
        test(sbuff.toString());
        try {
            final AddeStationObsDataset ads = new AddeStationObsDataset(loc, null);
            System.out.println(loc + " =\n" + ads);
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    public DataIterator getDataIterator(final int bufferSize) throws IOException {
        return null;
    }
    
    private class AddeTypedDataVariable implements VariableSimpleIF
    {
        String name;
        String units;
        DataType dt;
        
        AddeTypedDataVariable(final String name, final String units, final DataType dt) {
            this.name = name;
            this.units = units;
            this.dt = dt;
        }
        
        public String getName() {
            return this.name;
        }
        
        public String getShortName() {
            return this.name;
        }
        
        public String getDescription() {
            return null;
        }
        
        public String getUnitsString() {
            return this.units;
        }
        
        public int getRank() {
            return 0;
        }
        
        public int[] getShape() {
            return new int[0];
        }
        
        public List getDimensions() {
            return new ArrayList();
        }
        
        public DataType getDataType() {
            return this.dt;
        }
        
        public List getAttributes() {
            return new ArrayList();
        }
        
        public Attribute findAttributeIgnoreCase(final String name) {
            return null;
        }
        
        public int compareTo(final VariableSimpleIF o) {
            return this.getName().compareTo(o.getName());
        }
    }
    
    private class StationObs extends StationObsDatatypeImpl
    {
        private StructureData data;
        
        StationObs(final Station s, final StructureData data) {
            this.station = s;
            this.data = data;
            final int cyd = data.getScalarInt("DAY");
            final int year = cyd / 1000;
            final int doy = cyd % 1000;
            int hms = data.getScalarInt("TIME");
            final int hour = hms / 10000;
            hms %= 10000;
            final int min = hms / 100;
            final int sec = hms % 100;
            AddeStationObsDataset.this.calendar.clear();
            AddeStationObsDataset.this.calendar.set(1, year);
            AddeStationObsDataset.this.calendar.set(6, doy);
            AddeStationObsDataset.this.calendar.set(11, hour);
            AddeStationObsDataset.this.calendar.set(12, min);
            AddeStationObsDataset.this.calendar.set(13, sec);
            this.obsTime = AddeStationObsDataset.this.calendar.getTimeInMillis() / 1000.0;
            this.nomTime = this.obsTime;
        }
        
        public StructureData getData() throws IOException {
            return this.data;
        }
        
        public Date getNominalTimeAsDate() {
            return AddeStationObsDataset.this.timeUnit.makeDate(this.getNominalTime());
        }
        
        public Date getObservationTimeAsDate() {
            return AddeStationObsDataset.this.timeUnit.makeDate(this.getObservationTime());
        }
    }
}
