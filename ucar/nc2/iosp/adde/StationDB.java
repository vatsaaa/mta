// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.iosp.adde;

import ucar.unidata.geoloc.StationImpl;
import edu.wisc.ssec.mcidas.adde.AddePointDataReader;
import edu.wisc.ssec.mcidas.adde.AddeException;
import java.util.List;
import java.util.Collections;
import java.util.Collection;
import edu.wisc.ssec.mcidas.McIDASUtil;
import java.util.HashMap;
import ucar.nc2.util.CancelTask;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.FileInputStream;
import java.net.URL;
import java.util.ArrayList;

public class StationDB
{
    private static boolean debugOpen;
    private static boolean debugCall;
    private static boolean debugParse;
    private ArrayList stations;
    static String testName;
    static String testName2;
    
    public StationDB(final String urlString) throws IOException {
        this.stations = new ArrayList();
        final long start = System.currentTimeMillis();
        InputStream ios = null;
        if (urlString.startsWith("http:")) {
            final URL url = new URL(urlString);
            ios = url.openStream();
            if (StationDB.debugOpen) {
                System.out.println("opened URL " + urlString);
            }
        }
        else if (urlString.startsWith("resource:")) {
            final ClassLoader cl = this.getClass().getClassLoader();
            ios = cl.getResourceAsStream(urlString.substring(9));
            if (StationDB.debugOpen) {
                System.out.println("opened resource " + urlString);
            }
        }
        else {
            ios = new FileInputStream(urlString);
            if (StationDB.debugOpen) {
                System.out.println("opened file " + urlString);
            }
        }
        final BufferedReader dataIS = new BufferedReader(new InputStreamReader(ios));
        dataIS.readLine();
        dataIS.readLine();
        int count = 0;
        while (true) {
            final String line = dataIS.readLine();
            if (line == null) {
                break;
            }
            if (line.length() < 85) {
                break;
            }
            final String idn = line.substring(0, 5);
            final String id = line.substring(6, 10);
            final String name = line.substring(13, 33);
            final String type = line.substring(34, 51);
            final String state = line.substring(52, 54);
            final String country = line.substring(55, 57);
            final String latS = line.substring(58, 68);
            final String lonS = line.substring(69, 79);
            final String elevS = line.substring(80, 85);
            final Station s = new Station(idn, id, name, type, state, country, latS, lonS, elevS);
            this.stations.add(s);
            if (StationDB.debugParse) {
                System.out.println(s);
            }
            ++count;
        }
        dataIS.close();
        if (StationDB.debugCall) {
            final long took = System.currentTimeMillis() - start;
            System.out.println(" read " + urlString + " count=" + this.stations.size() + " took=" + took + " msec ");
        }
    }
    
    StationDB(final String location, final CancelTask cancel) throws IOException {
        this.stations = new ArrayList();
        final HashMap hashStations = new HashMap(5000);
        try {
            if (StationDB.debugOpen) {
                System.out.println("Call ADDE Server " + location);
            }
            final long start = System.currentTimeMillis();
            final AddePointDataReader reader = AddeStationObsDataset.callAdde(location + "&num=all&param=ID LAT LON ZS");
            final long took = System.currentTimeMillis() - start;
            if (StationDB.debugOpen) {
                System.out.println(" time = " + took);
            }
            final int[][] stationData = reader.getData();
            final int nparams = stationData.length;
            final int nstations = stationData[0].length;
            System.out.println(" nparams= " + nparams + " nstations=" + nstations);
            System.out.println(" size= " + nparams * nstations * 4 + " bytes");
            final int[] scales = reader.getScales();
            final double[] scaleFactor = new double[scales.length];
            for (int i = 0; i < nparams; ++i) {
                scaleFactor[i] = ((scales[i] == 0) ? 1.0 : (1.0 / Math.pow(10.0, scales[i])));
            }
            if (cancel != null && cancel.isCancel()) {
                return;
            }
            int last = 0;
            for (int j = 0; j < nstations; ++j) {
                final String stnId = McIDASUtil.intBitsToString(stationData[0][j]);
                if (!hashStations.containsKey(stnId)) {
                    last = j;
                    final double lat = stationData[1][j] * scaleFactor[1];
                    final double lon = stationData[2][j] * scaleFactor[2];
                    final double elev = stationData[3][j] * scaleFactor[3];
                    hashStations.put(stnId, new Station(stnId, "", lat, lon, elev));
                }
                if (cancel != null && cancel.isCancel()) {
                    return;
                }
            }
            if (StationDB.debugCall) {
                System.out.println(" hashStations count= " + hashStations.size() + " last = " + last);
            }
            final List stationList = new ArrayList(hashStations.keySet());
            Collections.sort((List<Comparable>)stationList);
            this.stations = new ArrayList();
            for (int k = 0; k < stationList.size(); ++k) {
                final String id = stationList.get(k);
                this.stations.add(hashStations.get(id));
                if (cancel != null && cancel.isCancel()) {
                    return;
                }
            }
        }
        catch (AddeException e) {
            e.printStackTrace();
            throw new IOException(e.getMessage());
        }
    }
    
    public ArrayList getStations() {
        return this.stations;
    }
    
    public static void main(final String[] args) throws IOException {
        final long start = System.currentTimeMillis();
        final StationDB stnDB = new StationDB(StationDB.testName);
        final long took = System.currentTimeMillis() - start;
        final List list = stnDB.getStations();
        double sum = 0.0;
        for (int i = 0; i < list.size(); ++i) {
            final Station s = list.get(i);
            sum += s.getLatitude();
        }
        System.out.println(" read " + StationDB.testName + " count=" + list.size() + " took=" + took + " msec " + " sum= " + sum);
    }
    
    static {
        StationDB.debugOpen = false;
        StationDB.debugCall = false;
        StationDB.debugParse = false;
        StationDB.testName = "C:/data/station/adde/STNDB.TXT";
        StationDB.testName2 = "http://localhost:8080/test/STNDB.TXT";
    }
    
    public class Station extends StationImpl
    {
        String idn;
        String id;
        String name;
        String type;
        String state;
        String country;
        String desc;
        double lat;
        double lon;
        double elev;
        
        Station(final String idn, final String id, final String name, final String type, final String state, final String country, final String latS, final String lonS, final String elevS) {
            if (StationDB.debugParse) {
                System.out.println("-" + idn + "-" + id + "-" + name + "-" + type + "-" + state + "-" + country + "-" + latS + "-" + lonS + "-" + elevS);
            }
            this.idn = idn.trim();
            this.id = id.trim();
            this.name = name.trim();
            this.type = type;
            this.state = state.trim();
            this.country = country.trim();
            this.lat = this.parseDegree(latS);
            this.lon = -1.0 * this.parseDegree(lonS);
            if (this.state.length() > 0) {
                this.desc = this.name + ", " + this.state + ", " + this.country;
            }
            else {
                this.desc = this.name + ", " + this.country;
            }
            try {
                this.elev = Double.parseDouble(elevS);
            }
            catch (NumberFormatException e) {
                e.printStackTrace();
            }
        }
        
        private double parseDegree(final String s) {
            final String degS = s.substring(0, 4);
            final String minS = s.substring(5, 7);
            final String secS = s.substring(8, 10);
            try {
                final double deg = Double.parseDouble(degS);
                final double min = Double.parseDouble(minS);
                final double sec = Double.parseDouble(secS);
                if (deg < 0.0) {
                    return deg - min / 60.0 - sec / 3600.0;
                }
                return deg + min / 60.0 + sec / 3600.0;
            }
            catch (NumberFormatException e) {
                e.printStackTrace();
                return 0.0;
            }
        }
        
        Station(final String id, final String desc, final double lat, final double lon, final double elev) {
            this.id = id;
            this.desc = desc;
            this.lat = lat;
            this.lon = lon;
            this.elev = elev;
        }
        
        @Override
        public String toString() {
            return this.idn + " " + this.id + " " + this.name + " " + this.type + " " + this.state + " " + this.country + " " + this.lat + " " + this.lon + " " + this.elev;
        }
        
        @Override
        public String getName() {
            return this.id;
        }
        
        @Override
        public String getDescription() {
            return this.desc;
        }
        
        @Override
        public String getWmoId() {
            return this.idn;
        }
        
        @Override
        public double getLatitude() {
            return this.lat;
        }
        
        @Override
        public double getLongitude() {
            return this.lon;
        }
        
        @Override
        public double getAltitude() {
            return this.elev;
        }
        
        public int compareTo(final Station so) {
            return this.name.compareTo(so.getName());
        }
    }
}
