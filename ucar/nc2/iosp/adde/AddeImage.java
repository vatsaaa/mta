// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.iosp.adde;

import ucar.nc2.dt.image.ImageArrayAdapter;
import java.awt.Dimension;
import java.util.Iterator;
import java.net.MalformedURLException;
import java.io.IOException;
import ucar.ma2.Array;
import java.awt.image.BufferedImage;
import java.util.LinkedHashMap;

public class AddeImage
{
    private static int initialCapacity;
    private static LinkedHashMap hash;
    private static double memUsed;
    private static int maxCacheSize;
    private static boolean debugCache;
    private String urlName;
    private int nelems;
    private int nlines;
    private BufferedImage image;
    private Array ma;
    private boolean debug;
    
    public static AddeImage factory(final String urlName) throws IOException, MalformedURLException {
        AddeImage image = AddeImage.hash.get(urlName);
        if (image == null) {
            if (AddeImage.debugCache) {
                System.out.println("ADDE/AddeImage/ShowCache: cache miss " + urlName);
            }
            image = new AddeImage(urlName);
            AddeImage.hash.put(urlName, image);
            AddeImage.memUsed += image.getSize();
            adjustCache();
        }
        else if (AddeImage.debugCache) {
            System.out.println("ADDE/AddeImage/ShowCache: cache hit " + urlName);
        }
        if (AddeImage.debugCache) {
            System.out.println("  memUsed = " + AddeImage.memUsed * 1.0E-6 + " Mb");
        }
        return image;
    }
    
    public static int getMaxCacheSize() {
        return AddeImage.maxCacheSize;
    }
    
    public static void setMaxCacheSize(final int max) {
        AddeImage.maxCacheSize = max;
    }
    
    public static int getCacheSize() {
        return (int)(AddeImage.memUsed * 1.0E-6);
    }
    
    private static void adjustCache() {
        final double max = AddeImage.maxCacheSize * 1000000.0;
        if (AddeImage.memUsed <= max) {
            return;
        }
        final Iterator iter = AddeImage.hash.values().iterator();
        while (iter.hasNext() && AddeImage.memUsed > max) {
            final AddeImage image = iter.next();
            AddeImage.memUsed -= image.getSize();
            if (AddeImage.debugCache) {
                System.out.println("  remove = " + image.getName() + " size= " + image.getSize() * 1.0E-6 + "  memUsed = " + AddeImage.memUsed * 1.0E-6);
            }
            iter.remove();
        }
    }
    
    public AddeImage(final String urlName) throws IOException, MalformedURLException {
        this.nelems = 0;
        this.nlines = 0;
        this.image = null;
        this.debug = false;
        this.urlName = urlName;
        final long timeStart = System.currentTimeMillis();
        this.debug = false;
        final AreaFile3 areaFile2 = new AreaFile3(urlName);
        this.ma = areaFile2.getData();
        if (this.ma.getRank() == 3) {
            this.ma = this.ma.slice(0, 0);
        }
        this.nlines = this.ma.getShape()[0];
        this.nelems = this.ma.getShape()[1];
        final long timeEnd = System.currentTimeMillis();
    }
    
    private long getSize() {
        return this.getSize(this.ma);
    }
    
    private long getSize(final Array ma) {
        final long size = ma.getSize();
        final Class maType = ma.getElementType();
        if (maType == Byte.TYPE) {
            return size;
        }
        if (maType == Short.TYPE) {
            return 2L * size;
        }
        if (maType == Integer.TYPE) {
            return 4L * size;
        }
        if (maType == Float.TYPE) {
            return 4L * size;
        }
        if (maType == Double.TYPE) {
            return 8L * size;
        }
        throw new IllegalArgumentException("DataBufferMultiArray ma has illegal data type = " + maType.getName());
    }
    
    public Dimension getPreferredSize() {
        return new Dimension(this.nelems, this.nlines);
    }
    
    public BufferedImage getImage() {
        if (this.image == null) {
            this.image = ImageArrayAdapter.makeGrayscaleImage(this.ma);
        }
        return this.image;
    }
    
    public String getName() {
        return this.urlName;
    }
    
    static {
        AddeImage.initialCapacity = 100;
        AddeImage.hash = new LinkedHashMap(AddeImage.initialCapacity, 0.75f, true);
        AddeImage.memUsed = 0.0;
        AddeImage.maxCacheSize = 30;
        AddeImage.debugCache = false;
    }
    
    private class LRUCache extends LinkedHashMap
    {
        protected int maxsize;
        
        public LRUCache(final int maxsize) {
            super(maxsize * 4 / 3 + 1, 0.75f, true);
            this.maxsize = maxsize;
        }
        
        protected boolean removeEldestEntry() {
            return this.size() > this.maxsize;
        }
    }
}
