// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.iosp.uamiv;

import org.slf4j.LoggerFactory;
import ucar.ma2.InvalidRangeException;
import ucar.ma2.Section;
import ucar.nc2.Variable;
import java.io.Writer;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.Reader;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.File;
import ucar.ma2.Array;
import ucar.nc2.Attribute;
import ucar.ma2.DataType;
import ucar.nc2.Group;
import ucar.nc2.Dimension;
import ucar.nc2.util.CancelTask;
import java.io.IOException;
import ucar.nc2.NetcdfFile;
import ucar.unidata.io.RandomAccessFile;
import org.slf4j.Logger;
import ucar.nc2.iosp.AbstractIOServiceProvider;

public class UAMIVServiceProvider extends AbstractIOServiceProvider
{
    private static Logger log;
    private static final String AVERAGE = "A   V   E   R   A   G   E               ";
    private static final String EMISSIONS = "E   M   I   S   S   I   O   N   S       ";
    private static final String AIRQUALITY = "A   I   R   Q   U   A   L   I   T   Y   ";
    private static final String INSTANT = "I   N   S   T   A   N   T               ";
    private static final String HEIGHT = "HEIGHT";
    private static final String PBL = "PBL";
    private static final String TEMP = "TEMP";
    private static final String PRESS = "PRESS";
    private static final String WINDX = "WINDX";
    private static final String WINDY = "WINDY";
    private static final String VERTDIFF = "Kv";
    private static final String SPEED = "SPEED";
    private static final String CLDOD = "CLD OPDEP";
    private static final String CLDWATER = "CLD WATER";
    private static final String PRECIP = "PCP WATER";
    private static final String RAIN = "RAIN";
    private RandomAccessFile raf;
    private NetcdfFile ncfile;
    private String[] species_names;
    private long data_start;
    private int n2dvals;
    private int n3dvals;
    private int spc_2D_block;
    private int spc_3D_block;
    private int data_block;
    private int date_block;
    private int nspec;
    
    public boolean isValidFile(final RandomAccessFile raf) throws IOException {
        try {
            raf.order(0);
            raf.seek(0L);
            raf.skipBytes(4);
            final byte[] b = new byte[40];
            raf.read(b);
            final String test = new String(b);
            return test.equals("E   M   I   S   S   I   O   N   S       ") || test.equals("A   V   E   R   A   G   E               ") || test.equals("A   I   R   Q   U   A   L   I   T   Y   ") || test.equals("I   N   S   T   A   N   T               ");
        }
        catch (IOException ioe) {
            return false;
        }
    }
    
    public String getFileTypeId() {
        return "UAMIV";
    }
    
    public String getFileTypeDescription() {
        return "CAMx UAM-IV formatted files";
    }
    
    @Override
    public void open(final RandomAccessFile raf, final NetcdfFile ncfile, final CancelTask cancelTask) throws IOException {
        this.raf = raf;
        this.ncfile = ncfile;
        raf.order(0);
        raf.seek(0L);
        raf.skipBytes(4);
        final String name = raf.readString(40);
        final String note = raf.readString(240);
        raf.skipBytes(4);
        final int nspec = raf.readInt();
        this.nspec = nspec;
        int bdate = raf.readInt();
        final float btime = raf.readFloat();
        int edate = raf.readInt();
        final float etime = raf.readFloat();
        int btimei = (int)btime;
        if (btimei < 100) {
            btimei *= 100;
        }
        if (btimei < 10000) {
            btimei *= 100;
        }
        if (bdate < 70000) {
            edate += 2000000;
            bdate += 2000000;
        }
        else {
            edate += 1900000;
            bdate += 1900000;
        }
        raf.skipBytes(4);
        raf.skipBytes(4);
        raf.skipBytes(8);
        final int iutm = raf.readInt();
        final float xorg = raf.readFloat();
        final float yorg = raf.readFloat();
        final float delx = raf.readFloat();
        final float dely = raf.readFloat();
        final int nx = raf.readInt();
        final int ny = raf.readInt();
        int nz = raf.readInt();
        raf.skipBytes(20);
        raf.skipBytes(4);
        raf.skipBytes(4);
        raf.skipBytes(8);
        final int nx2 = raf.readInt();
        final int ny2 = raf.readInt();
        raf.skipBytes(8);
        nz = Math.max(nz, 1);
        int count;
        String[] spc_names;
        String spc;
        for (count = 0, spc_names = new String[nspec]; count < nspec; spc_names[count++] = spc.replace(" ", "")) {
            spc = raf.readString(40);
        }
        this.species_names = spc_names;
        raf.skipBytes(4);
        this.data_start = raf.getFilePointer();
        final int data_length_float_equivalents = ((int)raf.length() - (int)this.data_start) / 4;
        this.n2dvals = nx * ny;
        this.n3dvals = nx * ny * nz;
        this.spc_2D_block = nx * ny + 10 + 2 + 1;
        this.spc_3D_block = this.spc_2D_block * nz;
        this.data_block = this.spc_3D_block * nspec + 6;
        final int ntimes = data_length_float_equivalents / this.data_block;
        ncfile.addDimension(null, new Dimension("TSTEP", ntimes, true));
        ncfile.addDimension(null, new Dimension("LAY", nz, true));
        ncfile.addDimension(null, new Dimension("ROW", ny, true));
        ncfile.addDimension(null, new Dimension("COL", nx, true));
        ncfile.finish();
        count = 0;
        while (count < nspec) {
            final String spc2 = spc_names[count++];
            final Variable temp = ncfile.addVariable(null, spc2, DataType.FLOAT, "TSTEP LAY ROW COL");
            if (spc2.equals("WINDX") || spc2.equals("WINDY") || spc2.equals("SPEED")) {
                temp.addAttribute(new Attribute("units", "m/s"));
            }
            else if (spc2.equals("Kv")) {
                temp.addAttribute(new Attribute("units", "m**2/s"));
            }
            else if (spc2.equals("TEMP")) {
                temp.addAttribute(new Attribute("units", "K"));
            }
            else if (spc2.equals("PRESS")) {
                temp.addAttribute(new Attribute("units", "hPa"));
            }
            else if (spc2.equals("HEIGHT") || spc2.equals("PBL")) {
                temp.addAttribute(new Attribute("units", "m"));
            }
            else if (spc2.equals("CLD WATER") || spc2.equals("PCP WATER") || spc2.equals("RAIN")) {
                temp.addAttribute(new Attribute("units", "g/m**3"));
            }
            else if (spc2.equals("CLD OPDEP")) {
                temp.addAttribute(new Attribute("units", "none"));
            }
            else if (spc2.startsWith("SOA") || spc2.equals("PSO4") || spc2.equals("PNO3") || spc2.equals("PNH4") || spc2.equals("PH2O") || spc2.equals("SOPA") || spc2.equals("SOPB") || spc2.equals("NA") || spc2.equals("PCL") || spc2.equals("POA") || spc2.equals("PEC") || spc2.equals("FPRM") || spc2.equals("FCRS") || spc2.equals("CPRM") || spc2.equals("CCRS")) {
                temp.addAttribute(new Attribute("units", "ug/m**3"));
            }
            else {
                temp.addAttribute(new Attribute("units", "ppm"));
            }
            temp.addAttribute(new Attribute("long_name", spc2));
            temp.addAttribute(new Attribute("var_desc", spc2));
        }
        double[] sigma;
        for (sigma = new double[nz + 1], count = 0; count < nz + 1; sigma[count] = ++count) {}
        final int[] size = { nz + 1 };
        final Array sigma_arr = Array.factory(DataType.DOUBLE.getPrimitiveClassType(), size, sigma);
        ncfile.addAttribute(null, new Attribute("VGLVLS", sigma_arr));
        ncfile.addAttribute(null, new Attribute("SDATE", new Integer(bdate)));
        ncfile.addAttribute(null, new Attribute("STIME", new Integer(btimei)));
        ncfile.addAttribute(null, new Attribute("TSTEP", new Integer(10000)));
        ncfile.addAttribute(null, new Attribute("NSTEPS", new Integer(ntimes)));
        ncfile.addAttribute(null, new Attribute("NLAYS", new Integer(nz)));
        ncfile.addAttribute(null, new Attribute("NROWS", new Integer(ny)));
        ncfile.addAttribute(null, new Attribute("NCOLS", new Integer(nx)));
        ncfile.addAttribute(null, new Attribute("XORIG", new Double(xorg)));
        ncfile.addAttribute(null, new Attribute("YORIG", new Double(yorg)));
        ncfile.addAttribute(null, new Attribute("XCELL", new Double(delx)));
        ncfile.addAttribute(null, new Attribute("YCELL", new Double(dely)));
        Integer gdtyp = 2;
        Double p_alp = 20.0;
        Double p_bet = 60.0;
        Double p_gam = 0.0;
        Double xcent = -95.0;
        Double ycent = 25.0;
        String[] key_value = null;
        String projpath = raf.getLocation();
        Boolean lgdtyp = false;
        Boolean lp_alp = false;
        Boolean lp_bet = false;
        Boolean lp_gam = false;
        Boolean lxcent = false;
        Boolean lycent = false;
        int lastIndex = projpath.lastIndexOf(File.separator);
        if (lastIndex <= 0) {
            lastIndex = projpath.lastIndexOf("/");
        }
        if (lastIndex > 0) {
            projpath = projpath.substring(0, lastIndex);
        }
        projpath = projpath + File.separator + "camxproj.txt";
        final File paramFile = new File(projpath);
        if (paramFile.exists()) {
            final BufferedReader br = new BufferedReader(new FileReader(paramFile));
            String thisLine;
            while ((thisLine = br.readLine()) != null) {
                if (thisLine.substring(0, 1) != "#") {
                    key_value = thisLine.split("=");
                    if (key_value[0].equals("GDTYP")) {
                        gdtyp = Integer.parseInt(key_value[1]);
                        lgdtyp = true;
                    }
                    else if (key_value[0].equals("P_ALP")) {
                        p_alp = Double.parseDouble(key_value[1]);
                        lp_alp = true;
                    }
                    else if (key_value[0].equals("P_BET")) {
                        p_bet = Double.parseDouble(key_value[1]);
                        lp_bet = true;
                    }
                    else if (key_value[0].equals("P_GAM")) {
                        p_gam = Double.parseDouble(key_value[1]);
                        lp_gam = true;
                    }
                    else if (key_value[0].equals("YCENT")) {
                        ycent = Double.parseDouble(key_value[1]);
                        lycent = true;
                    }
                    else {
                        if (!key_value[0].equals("XCENT")) {
                            continue;
                        }
                        xcent = Double.parseDouble(key_value[1]);
                        lxcent = true;
                    }
                }
            }
            if (!lgdtyp) {
                UAMIVServiceProvider.log.warn("GDTYP not found; using " + gdtyp.toString());
            }
            if (!lp_alp) {
                UAMIVServiceProvider.log.warn("P_ALP not found; using " + p_alp.toString());
            }
            if (!lp_bet) {
                UAMIVServiceProvider.log.warn("P_BET not found; using " + p_bet.toString());
            }
            if (!lp_gam) {
                UAMIVServiceProvider.log.warn("P_GAM not found; using " + p_gam.toString());
            }
            if (!lxcent) {
                UAMIVServiceProvider.log.warn("XCENT not found; using " + xcent.toString());
            }
            if (!lycent) {
                UAMIVServiceProvider.log.warn("YCENT not found; using " + ycent.toString());
            }
        }
        else {
            if (UAMIVServiceProvider.log.isDebugEnabled()) {
                UAMIVServiceProvider.log.debug("UAMIVServiceProvider: adding projection file");
            }
            final BufferedWriter bw = new BufferedWriter(new FileWriter(paramFile));
            bw.write("# Projection parameters are based on IOAPI.  For details, see www.baronsams.com/products/ioapi");
            bw.newLine();
            bw.write("GDTYP=");
            bw.write(gdtyp.toString());
            bw.newLine();
            bw.write("P_ALP=");
            bw.write(p_alp.toString());
            bw.newLine();
            bw.write("P_BET=");
            bw.write(p_bet.toString());
            bw.newLine();
            bw.write("P_GAM=");
            bw.write(p_gam.toString());
            bw.newLine();
            bw.write("XCENT=");
            bw.write(xcent.toString());
            bw.newLine();
            bw.write("YCENT=");
            bw.write(ycent.toString());
            bw.newLine();
            bw.flush();
            bw.close();
        }
        ncfile.addAttribute(null, new Attribute("GDTYP", gdtyp));
        ncfile.addAttribute(null, new Attribute("P_ALP", p_alp));
        ncfile.addAttribute(null, new Attribute("P_BET", p_bet));
        ncfile.addAttribute(null, new Attribute("P_GAM", p_gam));
        ncfile.addAttribute(null, new Attribute("XCENT", xcent));
        ncfile.addAttribute(null, new Attribute("YCENT", ycent));
    }
    
    public Array readData(final Variable v2, final Section wantSection) throws IOException, InvalidRangeException {
        this.raf.order(0);
        final int size = (int)v2.getSize();
        final float[] arr = new float[size];
        this.raf.seek(this.data_start);
        int pad1 = this.raf.readInt();
        this.raf.skipBytes(16);
        int pad2 = this.raf.readInt();
        if (pad1 != pad2) {
            throw new IOException("Asymmetric fortran buffer values: 1");
        }
        int spcid = -1;
        for (String spc = ""; spc != v2.getShortName(); spc = this.species_names[++spcid]) {}
        this.raf.skipBytes(this.spc_3D_block * spcid * 4);
        int count = 0;
        while (count < size) {
            if (count == 0) {
                pad1 = this.raf.readInt();
                final int ione = this.raf.readInt();
                final String spc = this.raf.readString(40);
            }
            if (count != 0 && count % this.n2dvals == 0) {
                pad2 = this.raf.readInt();
                if (pad1 != pad2) {
                    throw new IOException("Asymmetric fortran buffer values: 2");
                }
                if (count % this.n3dvals == 0) {
                    this.raf.skipBytes((this.data_block - this.spc_3D_block) * 4);
                }
                pad1 = this.raf.readInt();
                final int ione = this.raf.readInt();
                final String spc = this.raf.readString(40);
            }
            try {
                arr[count++] = this.raf.readFloat();
                continue;
            }
            catch (ArrayIndexOutOfBoundsException io) {
                throw new IOException(io.getMessage());
            }
            break;
        }
        final Array data = Array.factory(DataType.FLOAT.getPrimitiveClassType(), v2.getShape(), arr);
        return data.sectionNoReduce(wantSection.getRanges());
    }
    
    @Override
    public void close() throws IOException {
        this.raf.close();
    }
    
    static {
        UAMIVServiceProvider.log = LoggerFactory.getLogger(UAMIVServiceProvider.class);
    }
}
