// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.iosp.grib.tables;

import ucar.nc2.iosp.netcdf3.N3iosp;
import ucar.unidata.util.StringUtil;
import ucar.grid.GridParameter;
import ucar.grib.grib2.ParameterTable;
import org.jdom.Document;
import java.util.Collections;
import java.util.Collection;
import org.jdom.Element;
import java.util.HashMap;
import org.jdom.JDOMException;
import java.io.IOException;
import org.jdom.input.SAXBuilder;
import java.io.InputStream;
import java.util.Iterator;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class GribCodeTable implements Comparable<GribCodeTable>
{
    private static Map<String, GribCodeTable> tables;
    public String name;
    public int m1;
    public int m2;
    public boolean isParameter;
    public int discipline;
    public int category;
    public List<TableEntry> entries;
    private static int[] badones;
    static String resourceName;
    static boolean showDiff;
    
    public static TableEntry getEntry(final int discipline, final int category, final int number) {
        final GribCodeTable table = GribCodeTable.tables.get(getId(discipline, category));
        if (table == null) {
            return null;
        }
        return table.get(number);
    }
    
    private static String getId(final int discipline, final int category) {
        return discipline + "." + category;
    }
    
    GribCodeTable(final String name) {
        this.discipline = -1;
        this.category = -1;
        this.entries = new ArrayList<TableEntry>();
        this.name = name;
        final String[] s = name.split(" ");
        final String id = s[2];
        final String[] slist2 = id.split("\\.");
        if (slist2.length == 2) {
            this.m1 = Integer.parseInt(slist2[0]);
            this.m2 = Integer.parseInt(slist2[1]);
        }
        else {
            System.out.println("HEY bad= %s%n" + name);
        }
    }
    
    GribCodeTable(final String tableName, final String subtableName) {
        this.discipline = -1;
        this.category = -1;
        this.entries = new ArrayList<TableEntry>();
        final String[] s = tableName.split(" ");
        final String id = s[2];
        final String[] slist2 = id.split("\\.");
        if (slist2.length == 2) {
            this.m1 = Integer.parseInt(slist2[0]);
            this.m2 = Integer.parseInt(slist2[1]);
        }
        else {
            System.out.println("HEY bad= %s%n" + this.name);
        }
        this.name = subtableName;
        final String[] slist3 = this.name.split("[ :]+");
        try {
            for (int i = 0; i < slist3.length; ++i) {
                if (slist3[i].equalsIgnoreCase("discipline")) {
                    this.discipline = Integer.parseInt(slist3[i + 1]);
                }
                if (slist3[i].equalsIgnoreCase("category")) {
                    this.category = Integer.parseInt(slist3[i + 1]);
                }
            }
        }
        catch (Exception ex) {}
        this.isParameter = (this.discipline >= 0 && this.category >= 0);
    }
    
    void add(final String line, final String code, final String meaning, final String unit, final String status) {
        this.entries.add(new TableEntry(line, code, meaning, unit, status));
    }
    
    TableEntry get(final int value) {
        for (final TableEntry p : this.entries) {
            if (p.start == value) {
                return p;
            }
        }
        return null;
    }
    
    public int compareTo(final GribCodeTable o) {
        if (this.m1 != o.m1) {
            return this.m1 - o.m1;
        }
        if (this.m2 != o.m2) {
            return this.m2 - o.m2;
        }
        if (this.discipline != o.discipline) {
            return this.discipline - o.discipline;
        }
        return this.category - o.category;
    }
    
    @Override
    public String toString() {
        return "GribCodeTable{name='" + this.name + '\'' + ", m1=" + this.m1 + ", m2=" + this.m2 + ", isParameter=" + this.isParameter + ", discipline=" + this.discipline + ", category=" + this.category + '}';
    }
    
    private boolean remove(final TableEntry entry) {
        for (int i = 0; i < GribCodeTable.badones.length; i += 3) {
            if (this.discipline == GribCodeTable.badones[i] && this.category == GribCodeTable.badones[i + 1] && entry.number == GribCodeTable.badones[i + 2]) {
                return true;
            }
        }
        return false;
    }
    
    private static List<GribCodeTable> readGribCodes(final InputStream ios) throws IOException {
        Document doc;
        try {
            final SAXBuilder builder = new SAXBuilder();
            doc = builder.build(ios);
        }
        catch (JDOMException e) {
            throw new IOException(e.getMessage());
        }
        final Element root = doc.getRootElement();
        final Map<String, GribCodeTable> map = new HashMap<String, GribCodeTable>();
        GribCodeTable.tables = new HashMap<String, GribCodeTable>();
        final List<Element> featList = (List<Element>)root.getChildren("ForExport_CodeFlag_E");
        for (final Element elem : featList) {
            final String line = elem.getChildTextNormalize("No");
            final String tableName = elem.getChildTextNormalize("TableTitle_E");
            final String code = elem.getChildTextNormalize("CodeFlag");
            final String meaning = elem.getChildTextNormalize("Meaning_E");
            GribCodeTable ct = map.get(tableName);
            if (ct == null) {
                ct = new GribCodeTable(tableName);
                map.put(tableName, ct);
            }
            final Element unitElem = elem.getChild("AsciiUnit_x002F_Description_E");
            final String unit = (unitElem == null) ? null : unitElem.getTextNormalize();
            final Element statusElem = elem.getChild("Status");
            final String status = (statusElem == null) ? null : statusElem.getTextNormalize();
            final Element subtableElem = elem.getChild("TableSubTitle_E");
            if (subtableElem != null) {
                final String subTableName = subtableElem.getTextNormalize();
                GribCodeTable cst = map.get(subTableName);
                if (cst == null) {
                    cst = new GribCodeTable(tableName, subTableName);
                    map.put(subTableName, cst);
                    GribCodeTable.tables.put(getId(cst.discipline, cst.category), cst);
                }
                cst.add(line, code, meaning, unit, status);
            }
            else {
                ct.add(line, code, meaning, unit, status);
            }
        }
        ios.close();
        final List<GribCodeTable> tlist = new ArrayList<GribCodeTable>(map.values());
        Collections.sort(tlist);
        for (final GribCodeTable gt : tlist) {
            Collections.sort(gt.entries);
        }
        return tlist;
    }
    
    public static Map<String, GribCodeTable> readGribCodes() throws IOException {
        final Class c = GribCodeTable.class;
        final InputStream in = c.getResourceAsStream(GribCodeTable.resourceName);
        if (in == null) {
            System.out.printf("cant open %s%n", GribCodeTable.resourceName);
            return null;
        }
        final List<GribCodeTable> tlist = readGribCodes(in);
        final Map<String, GribCodeTable> map = new HashMap<String, GribCodeTable>(2 * tlist.size());
        for (final GribCodeTable ct : tlist) {
            String id = ct.m1 + "." + ct.m2;
            if (ct.isParameter) {
                id = id + "." + ct.discipline + "." + ct.category;
            }
            map.put(id, ct);
        }
        return map;
    }
    
    public static List<GribCodeTable> getWmoStandard() throws IOException {
        final Class c = GribCodeTable.class;
        final InputStream in = c.getResourceAsStream(GribCodeTable.resourceName);
        if (in == null) {
            System.out.printf("cant open %s%n", GribCodeTable.resourceName);
            return null;
        }
        try {
            return readGribCodes(in);
        }
        finally {
            in.close();
        }
    }
    
    public static void main(final String[] arg) throws IOException {
        final Class c = GribCodeTable.class;
        final InputStream in = c.getResourceAsStream(GribCodeTable.resourceName);
        if (in == null) {
            System.out.printf("cant open %s%n", GribCodeTable.resourceName);
            return;
        }
        final List<GribCodeTable> tlist = readGribCodes(in);
        for (final GribCodeTable gt : tlist) {
            System.out.printf("%d.%d (%d,%d) %s %n", gt.m1, gt.m2, gt.discipline, gt.category, gt.name);
            for (final TableEntry p : gt.entries) {
                System.out.printf("  %s (%d-%d) = %s %n", p.code, p.start, p.stop, p.meaning);
            }
        }
        if (GribCodeTable.showDiff) {
            int total = 0;
            int nsame = 0;
            int nsameIgn = 0;
            int ndiff = 0;
            int unknown = 0;
            System.out.printf("DIFFERENCES with current parameter table", new Object[0]);
            for (final GribCodeTable gt2 : tlist) {
                if (!gt2.isParameter) {
                    continue;
                }
                for (final TableEntry p2 : gt2.entries) {
                    if (p2.meaning.equalsIgnoreCase("Missing")) {
                        continue;
                    }
                    if (p2.start != p2.stop) {
                        continue;
                    }
                    final GridParameter gp = ParameterTable.getParameter(gt2.discipline, gt2.category, p2.start);
                    final String paramDesc = gp.getDescription();
                    if (paramDesc.startsWith("Unknown")) {
                        ++unknown;
                    }
                    final boolean same = paramDesc.equals(p2.meaning);
                    if (same) {
                        ++nsame;
                    }
                    final boolean sameIgnore = paramDesc.equalsIgnoreCase(p2.meaning);
                    if (sameIgnore) {
                        ++nsameIgn;
                    }
                    else {
                        ++ndiff;
                    }
                    ++total;
                    final String state = same ? "  " : (sameIgnore ? "* " : "**");
                    System.out.printf("%s%d %d %d%n %s%n %s%n", state, gt2.discipline, gt2.category, p2.start, p2.meaning, paramDesc);
                }
            }
            System.out.printf("Total=%d same=%d sameIgn=%d dif=%d unknown=%d%n", total, nsame, nsameIgn, ndiff, unknown);
        }
    }
    
    static {
        GribCodeTable.badones = new int[] { 0, 1, 51, 0, 6, 25, 0, 19, 22, 0, 191, 0, 1, 0, 0, 1, 0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 2, 2, 0, 0, 10, 191, 0 };
        GribCodeTable.resourceName = "/resources/grib/wmo/GRIB2_5_2_0_CodeFlag_E.xml";
        GribCodeTable.showDiff = true;
    }
    
    public class TableEntry implements Comparable<TableEntry>
    {
        public int start;
        public int stop;
        public int line;
        public int number;
        public String code;
        public String meaning;
        public String name;
        public String unit;
        public String status;
        
        TableEntry(final String line, final String code, String meaning, String unit, final String status) {
            this.number = -1;
            this.line = Integer.parseInt(line);
            this.code = code;
            this.meaning = meaning;
            this.status = status;
            try {
                final int pos = code.indexOf(45);
                if (pos > 0) {
                    this.start = Integer.parseInt(code.substring(0, pos));
                    final String stops = code.substring(pos + 1);
                    this.stop = Integer.parseInt(stops);
                }
                else {
                    this.start = Integer.parseInt(code);
                    this.stop = this.start;
                    this.number = this.start;
                }
            }
            catch (Exception e) {
                this.start = -1;
                this.stop = 0;
            }
            if (GribCodeTable.this.isParameter) {
                if (GribCodeTable.this.remove(this)) {
                    final int pos2 = meaning.indexOf(40);
                    final int pos3 = meaning.indexOf(41);
                    if (pos2 > 0 && pos3 > 0) {
                        meaning = meaning.substring(0, pos2).trim();
                    }
                }
                meaning = StringUtil.replace(meaning, '/', "-");
                meaning = StringUtil.replace(meaning, '.', "p");
                meaning = StringUtil.remove(meaning, 40);
                meaning = StringUtil.remove(meaning, 41);
                this.name = N3iosp.createValidNetcdf3ObjectName(meaning);
                if (unit != null) {
                    if (unit.equalsIgnoreCase("Proportion")) {
                        unit = "";
                    }
                    if (unit.equalsIgnoreCase("Numeric")) {
                        unit = "";
                    }
                }
                this.unit = unit;
            }
        }
        
        public int compareTo(final TableEntry o) {
            return this.start - o.start;
        }
        
        @Override
        public String toString() {
            return "TableEntry{, discipline=" + GribCodeTable.this.discipline + ", category=" + GribCodeTable.this.category + ", number=" + this.number + ", org='" + this.meaning + '\'' + ", name='" + this.name + '\'' + ", unit='" + this.unit + '\'' + ", status='" + this.status + '\'' + '}';
        }
    }
}
