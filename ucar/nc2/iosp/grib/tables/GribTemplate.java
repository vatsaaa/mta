// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.iosp.grib.tables;

import ucar.grib.GribNumbers;
import org.jdom.Document;
import java.util.Collections;
import java.util.Collection;
import org.jdom.Element;
import java.util.HashMap;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import java.io.InputStream;
import java.util.Iterator;
import java.util.Formatter;
import java.util.ArrayList;
import java.io.IOException;
import java.util.List;
import java.util.Map;

public class GribTemplate implements Comparable<GribTemplate>
{
    static Map<String, String> convertMap;
    static Map<String, GribCodeTable> gribCodes;
    public String name;
    public String desc;
    public int m1;
    public int m2;
    public List<Field> flds;
    static String resourceName;
    
    static String convert(final String table, final int value) {
        if (GribTemplate.gribCodes == null) {
            try {
                GribTemplate.gribCodes = GribCodeTable.readGribCodes();
            }
            catch (IOException e) {
                return "Read GridCodes failed";
            }
        }
        final GribCodeTable gct = GribTemplate.gribCodes.get(table);
        if (gct == null) {
            return table + " not found";
        }
        final GribCodeTable.TableEntry entry = gct.get(value);
        if (entry != null) {
            return entry.meaning;
        }
        return "Table " + table + " code " + value + " not found";
    }
    
    GribTemplate(final String desc) {
        this.flds = new ArrayList<Field>();
        this.desc = desc;
        final String[] slist = desc.split(" ");
        int i = 0;
        while (i < slist.length) {
            if (slist[i].equalsIgnoreCase("template")) {
                this.name = slist[i + 1];
                final String[] slist2 = this.name.split("\\.");
                if (slist2.length == 2) {
                    this.m1 = Integer.parseInt(slist2[0]);
                    this.m2 = Integer.parseInt(slist2[1]);
                    break;
                }
                System.out.println("HEY bad= %s%n" + this.name);
                break;
            }
            else {
                ++i;
            }
        }
    }
    
    void add(final String octet, final String content) {
        this.flds.add(new Field(octet, content));
    }
    
    void add(final int start, final int nbytes, final String content) {
        this.flds.add(new Field(start, nbytes, content));
    }
    
    public int compareTo(final GribTemplate o) {
        if (this.m1 == o.m1) {
            return this.m2 - o.m2;
        }
        return this.m1 - o.m1;
    }
    
    public void showInfo(final byte[] pds, final Formatter f) {
        f.format("%n(%s) %s %n", this.name, this.desc);
        for (final Field fld : this.flds) {
            if (fld.start < 0) {
                continue;
            }
            final String info = GribTemplate.convertMap.get(fld.content);
            if (info == null) {
                f.format("%3d: %90s == %d %n", fld.start, fld.content, fld.value(pds));
            }
            else {
                final String desc = convert(info, fld.value(pds));
                if (desc == null) {
                    f.format("%3d: %90s == %d (%s) %n", fld.start, fld.content, fld.value(pds), convert(info, fld.value(pds)));
                }
                else {
                    f.format("%3d: %90s == %d (table %s: %s) %n", fld.start, fld.content, fld.value(pds), info, desc);
                }
            }
        }
    }
    
    public static List<GribTemplate> readXml(final InputStream ios) throws IOException {
        Document doc;
        try {
            final SAXBuilder builder = new SAXBuilder();
            doc = builder.build(ios);
        }
        catch (JDOMException e) {
            throw new IOException(e.getMessage());
        }
        final Map<String, GribTemplate> map = new HashMap<String, GribTemplate>();
        final Element root = doc.getRootElement();
        final List<Element> featList = (List<Element>)root.getChildren("ForExport_Templates_E");
        for (final Element elem : featList) {
            final String desc = elem.getChildTextNormalize("TemplateName_E");
            final String octet = elem.getChildTextNormalize("OctetNo");
            final String content = elem.getChildTextNormalize("Contents_E");
            GribTemplate t = map.get(desc);
            if (t == null) {
                t = new GribTemplate(desc);
                map.put(desc, t);
            }
            t.add(octet, content);
        }
        ios.close();
        final List<GribTemplate> tlist = new ArrayList<GribTemplate>(map.values());
        Collections.sort(tlist);
        for (final GribTemplate t2 : tlist) {
            if (t2.m1 == 3) {
                t2.add(1, 4, "GDS length");
                t2.add(5, 1, "Section");
                t2.add(6, 1, "Source of Grid Definition (see code table 3.0)");
                t2.add(7, 4, "Number of data points");
                t2.add(11, 1, "Number of octects for optional list of numbers");
                t2.add(12, 1, "Interpretation of list of numbers");
                t2.add(13, 2, "Grid Definition Template Number");
            }
            else if (t2.m1 == 4) {
                t2.add(1, 4, "PDS length");
                t2.add(5, 1, "Section");
                t2.add(6, 2, "Number of coordinates values after Template");
                t2.add(8, 2, "Product Definition Template Number");
            }
            Collections.sort(t2.flds);
        }
        return tlist;
    }
    
    public static Map<String, GribTemplate> getParameterTemplates() throws IOException {
        final Class c = GribCodeTable.class;
        final InputStream in = c.getResourceAsStream(GribTemplate.resourceName);
        if (in == null) {
            System.out.printf("cant open %s%n", GribTemplate.resourceName);
            return null;
        }
        final List<GribTemplate> tlist = readXml(in);
        final Map<String, GribTemplate> map = new HashMap<String, GribTemplate>(100);
        for (final GribTemplate t : tlist) {
            map.put(t.m1 + "." + t.m2, t);
        }
        return map;
    }
    
    public static List<GribTemplate> getWmoStandard() throws IOException {
        final Class c = GribCodeTable.class;
        final InputStream in = c.getResourceAsStream(GribTemplate.resourceName);
        if (in == null) {
            System.out.printf("cant open %s%n", GribTemplate.resourceName);
            return null;
        }
        try {
            return readXml(in);
        }
        finally {
            in.close();
        }
    }
    
    public static void main(final String[] arg) throws IOException {
        final Class c = GribCodeTable.class;
        final InputStream in = c.getResourceAsStream(GribTemplate.resourceName);
        if (in == null) {
            System.out.printf("cant open %s%n", GribTemplate.resourceName);
            return;
        }
        final List<GribTemplate> tlist = readXml(in);
        for (final GribTemplate t : tlist) {
            System.out.printf("%n(%s) %s %n", t.name, t.desc);
            for (final Field f : t.flds) {
                System.out.printf(" (%d,%d) %10s : %s %n", f.start, f.nbytes, f.octet, f.content);
            }
        }
        for (final GribTemplate t : tlist) {
            System.out.printf("%n(%s) %s %n", t.name, t.desc);
            int start = -1;
            int next = 0;
            for (final Field f2 : t.flds) {
                if (f2.start < 0) {
                    continue;
                }
                if (start < 0) {
                    start = f2.start;
                    next = start + f2.nbytes;
                }
                else {
                    if (f2.start != next) {
                        System.out.printf(" missing %d to %d %n", next, start);
                    }
                    next = f2.start + f2.nbytes;
                }
            }
            System.out.printf(" range %d-%d %n", start, next);
        }
    }
    
    static {
        (GribTemplate.convertMap = new HashMap<String, String>()).put("Type of generating process", "4.3");
        GribTemplate.convertMap.put("Indicator of unit of time range", "4.4");
        GribTemplate.convertMap.put("Indicator of unit of time for time range over which statistical processing is done", "4.4");
        GribTemplate.convertMap.put("Indicator of unit of time for the increment between the successive fields used", "4.4");
        GribTemplate.convertMap.put("Type of first fixed surface", "4.5");
        GribTemplate.convertMap.put("Type of second fixed surface", "4.5");
        GribTemplate.convertMap.put("Derived forecast", "4.7");
        GribTemplate.convertMap.put("Probability type", "4.9");
        GribTemplate.convertMap.put("Statistical process used to calculate the processed field from the field at each time increment during the time range", "4.10");
        GribTemplate.convertMap.put("Type of time increment between successive fields used in the statistical processing", "4.11");
        GribTemplate.resourceName = "/resources/grib/wmo/GRIB2_5_2_0_Templates_E.xml";
    }
    
    public class Field implements Comparable<Field>
    {
        public String octet;
        public String content;
        public int start;
        public int nbytes;
        
        Field(final String octet, final String content) {
            this.octet = octet;
            this.content = content;
            try {
                final int pos = octet.indexOf(45);
                if (pos > 0) {
                    this.start = Integer.parseInt(octet.substring(0, pos));
                    final String stops = octet.substring(pos + 1);
                    int stop = -1;
                    try {
                        stop = Integer.parseInt(stops);
                        this.nbytes = stop - this.start + 1;
                    }
                    catch (Exception ex) {}
                }
                else {
                    this.start = Integer.parseInt(octet);
                    this.nbytes = 1;
                }
            }
            catch (Exception e) {
                this.start = -1;
                this.nbytes = 0;
            }
        }
        
        Field(final int start, final int nbytes, final String content) {
            this.start = start;
            this.nbytes = nbytes;
            this.content = content;
            this.octet = start + "-" + (start + nbytes - 1);
        }
        
        public int compareTo(final Field o) {
            return this.start - o.start;
        }
        
        int value(final byte[] pds) {
            switch (this.nbytes) {
                case 1: {
                    return this.get(pds, this.start);
                }
                case 2: {
                    return GribNumbers.int2(this.get(pds, this.start), this.get(pds, this.start + 1));
                }
                case 4: {
                    return GribNumbers.int4(this.get(pds, this.start), this.get(pds, this.start + 1), this.get(pds, this.start + 2), this.get(pds, this.start + 3));
                }
                default: {
                    return -9999;
                }
            }
        }
        
        int get(final byte[] pds, final int offset) {
            return pds[offset - 1] & 0xFF;
        }
    }
}
