// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.iosp.grib;

import ucar.nc2.NetcdfFile;
import java.util.Date;
import java.io.FileNotFoundException;
import ucar.nc2.FileWriter;
import ucar.nc2.util.CancelTask;
import java.io.IOException;
import ucar.nc2.iosp.IOServiceProvider;
import ucar.unidata.io.RandomAccessFile;
import java.util.Calendar;
import java.util.TimeZone;

public class Grib2Netcdf
{
    public void usage(final String className) {
        System.out.println();
        System.out.println("Usage of " + className + ":");
        System.out.println("Parameters:");
        System.out.println("<GribFileToRead> reads/scans metadata");
        System.out.println("<NetCDF output file> file to store results");
        System.out.println();
        System.out.println("java -Xmx256m " + className + " <GribFileToRead> <NetCDF output file>");
        System.exit(0);
    }
    
    public static void main(final String[] args) throws IOException {
        final Grib2Netcdf func = new Grib2Netcdf();
        if (args.length != 2) {
            final Class cl = func.getClass();
            func.usage(cl.getName());
        }
        final TimeZone tz = TimeZone.getTimeZone("127");
        TimeZone.setDefault(tz);
        Date now = Calendar.getInstance().getTime();
        System.out.println(now.toString() + " ... Start of Grib2Netcdf");
        System.out.println("read grib file=" + args[0] + " write to netCDF file=" + args[1]);
        try {
            final RandomAccessFile raf = new RandomAccessFile(args[0], "r");
            raf.order(0);
            final Class c = GribGridServiceProvider.class;
            IOServiceProvider iosp = null;
            try {
                iosp = c.newInstance();
            }
            catch (InstantiationException e2) {
                throw new IOException("IOServiceProvider " + c.getName() + "must have no-arg constructor.");
            }
            catch (IllegalAccessException e) {
                throw new IOException("IOServiceProvider " + c.getName() + " IllegalAccessException: " + e.getMessage());
            }
            final NetcdfFile ncfile = new MakeNetcdfFile(iosp, raf, args[0], null);
            final NetcdfFile nc = FileWriter.writeToFile(ncfile, args[1]);
            nc.close();
            raf.close();
        }
        catch (FileNotFoundException noFileError) {
            System.err.println("FileNotFoundException : " + noFileError);
        }
        catch (IOException ioError) {
            System.err.println("IOException : " + ioError);
        }
        now = Calendar.getInstance().getTime();
        System.out.println(now.toString() + " ... End of Grib2Netcdf!");
    }
    
    static class MakeNetcdfFile extends NetcdfFile
    {
        MakeNetcdfFile(final IOServiceProvider spi, final RandomAccessFile raf, final String location, final CancelTask cancelTask) throws IOException {
            super(spi, raf, location, cancelTask);
        }
    }
}
