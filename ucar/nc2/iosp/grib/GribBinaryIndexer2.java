// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.iosp.grib;

import ucar.grib.grib2.Grib2WriteIndex;
import ucar.grib.grib1.Grib1WriteIndex;
import ucar.grib.GribChecker;
import ucar.unidata.io.RandomAccessFile;
import java.util.Calendar;
import java.util.Iterator;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.List;

public class GribBinaryIndexer2
{
    private static boolean removeGBX;
    private List<String> dirs;
    
    public GribBinaryIndexer2() {
        this.dirs = new ArrayList<String>();
    }
    
    private boolean readConf(final String conf) throws IOException {
        final InputStream ios = new FileInputStream(conf);
        final BufferedReader dataIS = new BufferedReader(new InputStreamReader(ios));
        while (true) {
            final String line = dataIS.readLine();
            if (line == null) {
                break;
            }
            if (line.startsWith("#")) {
                continue;
            }
            this.dirs.add(line);
        }
        ios.close();
        return true;
    }
    
    private void clearLocks() {
        for (final String dir : this.dirs) {
            final File f = new File(dir + "/IndexLock");
            if (f.exists()) {
                f.delete();
                System.out.println("Cleared lock " + dir + "/IndexLock");
            }
            else {
                System.out.println("In directory " + dir);
            }
        }
    }
    
    private void indexer() throws IOException {
        System.out.println("Start " + Calendar.getInstance().getTime().toString());
        for (final String dir : this.dirs) {
            final File d = new File(dir);
            if (!d.exists()) {
                System.out.println("Dir " + dir + " doesn't exists");
            }
            else {
                final File dl = new File(dir + "/IndexLock");
                if (dl.exists()) {
                    System.out.println("Exiting " + dir + " another Indexer working here");
                }
                else {
                    dl.createNewFile();
                    this.checkDirs(d);
                    dl.delete();
                }
            }
        }
        System.out.println("End " + Calendar.getInstance().getTime().toString());
    }
    
    private void checkDirs(final File dir) throws IOException {
        if (dir.isDirectory()) {
            System.out.println("In directory " + dir.getParent() + "/" + dir.getName());
            final String[] arr$;
            final String[] children = arr$ = dir.list();
            for (final String aChildren : arr$) {
                if (!aChildren.equals("IndexLock")) {
                    final File child = new File(dir, aChildren);
                    if (child.isDirectory()) {
                        this.checkDirs(child);
                    }
                    else if (!aChildren.endsWith("gbx") && !aChildren.endsWith("xml") && !aChildren.endsWith("gbx2") && !aChildren.endsWith("tmp")) {
                        if (aChildren.length() != 0) {
                            this.checkIndex(dir, child);
                        }
                    }
                }
            }
        }
    }
    
    private void checkIndex(final File dir, final File grib) throws IOException {
        final String[] args = new String[2];
        final File gbx = new File(dir, grib.getName() + ".gbx2");
        if (GribBinaryIndexer2.removeGBX && gbx.exists()) {
            gbx.delete();
        }
        args[0] = grib.getParent() + "/" + grib.getName();
        args[1] = grib.getParent() + "/" + gbx.getName();
        if (gbx.exists()) {
            if (System.currentTimeMillis() - grib.lastModified() > 10800000L) {
                return;
            }
            if (gbx.length() == 0L) {
                System.out.println("ERROR " + args[1] + " has length zero");
                return;
            }
        }
        if (grib.getName().endsWith("grib1")) {
            this.grib1check(grib, gbx, args);
        }
        else if (grib.getName().endsWith("grib2")) {
            this.grib2check(grib, gbx, args);
        }
        else {
            final RandomAccessFile raf = new RandomAccessFile(args[0], "r");
            final int result = GribChecker.getEdition(raf);
            if (result == 2) {
                this.grib2check(grib, gbx, args);
            }
            else if (result == 1) {
                this.grib1check(grib, gbx, args);
            }
            else {
                System.out.println("Not a Grib File " + args[0]);
            }
            raf.close();
        }
    }
    
    private void grib1check(final File grib, final File gbx, final String[] args) {
        try {
            if (gbx.exists()) {
                if (grib.lastModified() < gbx.lastModified()) {
                    return;
                }
                System.out.println("IndexExtending " + grib.getName() + " " + Calendar.getInstance().getTime().toString());
                new Grib1WriteIndex().extendGribIndex(grib, gbx, args[0], args[1], false);
            }
            else {
                System.out.println("Indexing " + grib.getName() + " " + Calendar.getInstance().getTime().toString());
                new Grib1WriteIndex().writeGribIndex(grib, args[0], args[1], false);
            }
        }
        catch (Exception e) {
            e.printStackTrace();
            System.out.println("Caught Exception doing index or inventory for " + grib.getName());
        }
    }
    
    private void grib2check(final File grib, final File gbx, final String[] args) {
        try {
            if (gbx.exists()) {
                if (grib.lastModified() < gbx.lastModified()) {
                    return;
                }
                System.out.println("IndexExtending " + grib.getName() + " " + Calendar.getInstance().getTime().toString());
                new Grib2WriteIndex().extendGribIndex(grib, gbx, args[0], args[1], false);
            }
            else {
                System.out.println("Indexing " + grib.getName() + " " + Calendar.getInstance().getTime().toString());
                new Grib2WriteIndex().writeGribIndex(grib, args[0], args[1], false);
            }
        }
        catch (Exception e) {
            e.printStackTrace();
            System.out.println("Caught Exception doing index or inventory for " + grib.getName());
        }
    }
    
    public static boolean test() throws IOException {
        final GribBinaryIndexer2 gi2 = new GribBinaryIndexer2();
        final String[] args = { "C:/data/gefs/GFS_Global_1p0deg_Ensemble_20090303_0000.grib2", null };
        args[1] = args[0] + ".gbx";
        final File grib = new File(args[0]);
        final File gbx = new File(args[1]);
        gi2.grib2check(grib, gbx, args);
        return true;
    }
    
    public static void main(final String[] args) throws IOException {
        final GribBinaryIndexer2 gi2 = new GribBinaryIndexer2();
        boolean clear = false;
        for (final String arg : args) {
            if (arg.equals("clear")) {
                clear = true;
                System.out.println("Clearing Index locks");
            }
            else if (arg.equals("remove")) {
                GribBinaryIndexer2.removeGBX = true;
                System.out.println("Removing all indexes");
            }
            else {
                final File f = new File(arg);
                if (!f.exists()) {
                    System.out.println("Conf file " + arg + " doesn't exist: ");
                    return;
                }
                gi2.readConf(arg);
            }
        }
        if (clear) {
            gi2.clearLocks();
            return;
        }
        gi2.indexer();
    }
    
    static {
        GribBinaryIndexer2.removeGBX = false;
    }
}
