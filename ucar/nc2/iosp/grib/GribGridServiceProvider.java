// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.iosp.grib;

import org.slf4j.LoggerFactory;
import ucar.grib.GribGridRecord;
import ucar.grid.GridRecord;
import java.net.URL;
import ucar.grib.grib2.Grib2WriteIndex;
import ucar.grib.grib1.Grib1WriteIndex;
import ucar.nc2.util.DiskCache;
import java.io.InputStream;
import java.io.File;
import ucar.grib.GribIndexReader;
import ucar.grib.GribIndexName;
import ucar.grib.grib1.Grib1GridTableLookup;
import ucar.grib.NoValidGribException;
import ucar.grib.grib1.Grib1Record;
import ucar.grib.grib1.Grib1Input;
import java.util.List;
import ucar.grib.grib2.Grib2GridTableLookup;
import ucar.grib.NotSupportedException;
import ucar.grib.grib2.Grib2Record;
import ucar.grid.GridTableLookup;
import java.util.Map;
import ucar.nc2.iosp.grid.GridIndexToNC;
import ucar.nc2.util.CancelTask;
import ucar.nc2.NetcdfFile;
import java.io.IOException;
import ucar.grib.grib2.Grib2Input;
import ucar.unidata.io.RandomAccessFile;
import ucar.grid.GridIndex;
import ucar.grib.grib2.Grib2Data;
import ucar.grib.grib1.Grib1Data;
import org.slf4j.Logger;
import ucar.nc2.iosp.grid.GridServiceProvider;

public class GribGridServiceProvider extends GridServiceProvider
{
    private static Logger log;
    private long rafLength;
    private long indexLength;
    private int saveEdition;
    private float version;
    private Grib1Data dataReaderGrib1;
    private Grib2Data dataReaderGrib2;
    private GridIndex gridIndexSave;
    
    public GribGridServiceProvider() {
        this.saveEdition = 0;
        this.version = 0.0f;
        this.gridIndexSave = null;
    }
    
    public boolean isValidFile(final RandomAccessFile raf) {
        try {
            return Grib2Input.isValidFile(raf);
        }
        catch (Exception e) {
            return false;
        }
    }
    
    public String getFileTypeId() {
        return (this.saveEdition == 2) ? "GRIB2" : "GRIB1";
    }
    
    public String getFileTypeDescription() {
        return (this.saveEdition == 2) ? "WMO GRIB Edition 2" : "WMO GRIB Edition 1";
    }
    
    @Override
    public Object sendIospMessage(final Object special) {
        if (special instanceof String) {
            final String s = (String)special;
            if (s.equalsIgnoreCase("GridIndex")) {
                if (this.gridIndexSave != null) {
                    return this.gridIndexSave;
                }
                try {
                    return this.getIndex(this.raf.getLocation());
                }
                catch (IOException e) {
                    return null;
                }
            }
        }
        return super.sendIospMessage(special);
    }
    
    @Override
    public void open(final RandomAccessFile raf, final NetcdfFile ncfile, final CancelTask cancelTask) throws IOException {
        this.raf = raf;
        this.ncfile = ncfile;
        this.rafLength = raf.length();
        raf.order(0);
        final long start = System.currentTimeMillis();
        final GridIndex index = this.getIndex(raf.getLocation());
        final Map<String, String> attr = (Map<String, String>)index.getGlobalAttributes();
        this.saveEdition = (attr.get("grid_edition").equals("2") ? 2 : 1);
        this.version = Float.parseFloat(attr.get("index_version"));
        final GridTableLookup lookup = (this.saveEdition == 2) ? this.getLookup2() : this.getLookup1();
        final GridIndexToNC convert = new GridIndexToNC(raf);
        convert.open(index, lookup, this.saveEdition, ncfile, this.fmrcCoordSys, cancelTask);
        ncfile.finish();
        if (GridServiceProvider.debugOpen) {
            this.gridIndexSave = index;
        }
        if (GribGridServiceProvider.debugTiming) {
            final long took = System.currentTimeMillis() - start;
            System.out.println(" open " + ncfile.getLocation() + " took=" + took + " msec ");
        }
        GribGridServiceProvider.log.debug(" open() " + ncfile.getLocation() + " took " + (System.currentTimeMillis() - start));
    }
    
    @Override
    protected void open(final GridIndex index, final CancelTask cancelTask) throws IOException {
        final long start = System.currentTimeMillis();
        final GridTableLookup lookup = (this.saveEdition == 2) ? this.getLookup2() : this.getLookup1();
        final GridIndexToNC convert = new GridIndexToNC(index.filename);
        convert.open(index, lookup, this.saveEdition, this.ncfile, this.fmrcCoordSys, cancelTask);
        this.ncfile.finish();
        if (GribGridServiceProvider.debugTiming) {
            final long took = System.currentTimeMillis() - start;
            System.out.println(" open() " + this.ncfile.getLocation() + " took=" + took + " msec ");
        }
        GribGridServiceProvider.log.debug(" open() from sync" + this.ncfile.getLocation() + " took " + (System.currentTimeMillis() - start));
    }
    
    public GridTableLookup getLookup() throws IOException {
        GridTableLookup lookup;
        if (this.saveEdition == 2) {
            lookup = this.getLookup2();
        }
        else {
            lookup = this.getLookup1();
        }
        return lookup;
    }
    
    protected GridTableLookup getLookup2() throws IOException {
        Grib2Record firstRecord = null;
        try {
            final Grib2Input g2i = new Grib2Input(this.raf);
            final long start2 = System.currentTimeMillis();
            this.raf.seek(0L);
            g2i.scan(false, true);
            final List records = g2i.getRecords();
            firstRecord = records.get(0);
            if (GribGridServiceProvider.debugTiming) {
                final long took = System.currentTimeMillis() - start2;
                System.out.println("  read one record took=" + took + " msec ");
            }
        }
        catch (NotSupportedException noSupport) {
            System.err.println("NotSupportedException : " + noSupport);
        }
        final Grib2GridTableLookup lookup = new Grib2GridTableLookup(firstRecord);
        this.dataReaderGrib2 = new Grib2Data(this.raf);
        return (GridTableLookup)lookup;
    }
    
    protected GridTableLookup getLookup1() throws IOException {
        Grib1Record firstRecord = null;
        try {
            final Grib1Input g1i = new Grib1Input(this.raf);
            final long start2 = System.currentTimeMillis();
            this.raf.seek(0L);
            g1i.scan(false, true);
            final List records = g1i.getRecords();
            firstRecord = records.get(0);
            if (GribGridServiceProvider.debugTiming) {
                final long took = System.currentTimeMillis() - start2;
                System.out.println("  read one record took=" + took + " msec ");
            }
        }
        catch (NotSupportedException noSupport) {
            System.err.println("NotSupportedException : " + noSupport);
        }
        catch (NoValidGribException noValid) {
            System.err.println("NoValidGribException : " + noValid);
        }
        final Grib1GridTableLookup lookup = new Grib1GridTableLookup(firstRecord);
        this.dataReaderGrib1 = new Grib1Data(this.raf);
        return (GridTableLookup)lookup;
    }
    
    protected GridIndex getIndex(final String dataLocation) throws IOException {
        if (dataLocation.startsWith("http:")) {
            final String indexLocation = GribIndexName.get(dataLocation);
            final InputStream ios = this.indexExistsAsURL(indexLocation);
            if (ios != null) {
                GribGridServiceProvider.log.debug(" getIndex() HTTP index = " + indexLocation);
                return new GribIndexReader().open(indexLocation, ios);
            }
        }
        final File indexFile = this.getIndexFile(dataLocation);
        GridIndex index = null;
        if (!GribGridServiceProvider.forceNewIndex && indexFile.exists()) {
            try {
                index = new GribIndexReader().open(indexFile.getPath());
                GribGridServiceProvider.log.debug("  opened index = " + indexFile.getPath());
                if (GribGridServiceProvider.indexFileModeOnOpen != IndexExtendMode.readonly) {
                    final String lengthS = index.getGlobalAttributes().get("length");
                    final long indexRafLength = (lengthS == null) ? 0L : Long.parseLong(lengthS);
                    if (indexRafLength != this.rafLength) {
                        if (GribGridServiceProvider.log.isDebugEnabled()) {
                            GribGridServiceProvider.log.debug("  dataFile " + dataLocation + " length has changed: indexRafLength= " + indexRafLength + " rafLength= " + this.rafLength);
                        }
                        if (GribGridServiceProvider.indexFileModeOnOpen == IndexExtendMode.extendwrite) {
                            if (indexRafLength < this.rafLength) {
                                if (GribGridServiceProvider.log.isDebugEnabled()) {
                                    GribGridServiceProvider.log.debug("  extend Index = " + indexFile.getPath());
                                }
                                index = this.extendIndex(new File(this.raf.getLocation()), indexFile, this.raf);
                            }
                            else {
                                if (GribGridServiceProvider.log.isDebugEnabled()) {
                                    GribGridServiceProvider.log.debug("  rewrite index = " + indexFile.getPath());
                                }
                                index = this.writeIndex(indexFile, this.raf);
                            }
                        }
                        else if (GribGridServiceProvider.indexFileModeOnOpen == IndexExtendMode.rewrite) {
                            if (GribGridServiceProvider.log.isDebugEnabled()) {
                                GribGridServiceProvider.log.debug("  rewrite index = " + indexFile.getPath());
                            }
                            index = this.writeIndex(indexFile, this.raf);
                        }
                    }
                }
            }
            catch (Exception e) {
                GribGridServiceProvider.log.warn("GribReadIndex() failed, will try to rewrite at " + indexFile.getPath(), e);
                index = this.writeIndex(indexFile, this.raf);
            }
        }
        else {
            GribGridServiceProvider.log.debug("  write index = " + indexFile.getPath());
            index = this.writeIndex(indexFile, this.raf);
        }
        this.indexLength = indexFile.length();
        return index;
    }
    
    private File getIndexFile(final String dataLocation) throws IOException {
        final String indexLocation = GribIndexName.getIndex(dataLocation, false);
        File indexFile = null;
        if (indexLocation.startsWith("http:")) {
            indexFile = DiskCache.getCacheFile(indexLocation);
            GribGridServiceProvider.log.debug("  HTTP index = " + indexFile.getPath());
        }
        else {
            indexFile = new File(indexLocation);
            if (!indexFile.exists()) {
                GribGridServiceProvider.log.debug(" saveIndexFile not exist " + indexFile.getPath() + " ++ " + indexLocation);
                indexFile = DiskCache.getFile(indexLocation, GribGridServiceProvider.alwaysInCache);
                GribGridServiceProvider.log.debug(" use " + indexFile.getPath());
            }
        }
        return indexFile;
    }
    
    private GridIndex writeIndex(final File indexFile, final RandomAccessFile raf) throws IOException {
        GridIndex index = null;
        if (indexFile.exists()) {
            final boolean ok = indexFile.delete();
            GribGridServiceProvider.log.debug("Deleted old index " + indexFile.getPath() + " = " + ok);
        }
        if (this.saveEdition == 0) {
            raf.seek(0L);
            final Grib2Input g2i = new Grib2Input(raf);
            this.saveEdition = g2i.getEdition();
        }
        final File gribFile = new File(raf.getLocation());
        if (this.saveEdition == 1) {
            index = new Grib1WriteIndex().writeGribIndex(gribFile, indexFile.getPath(), raf, true);
        }
        else if (this.saveEdition == 2) {
            index = new Grib2WriteIndex().writeGribIndex(gribFile, indexFile.getPath(), raf, true);
        }
        return index;
    }
    
    @Override
    public boolean sync() throws IOException {
        final File indexFile = this.getIndexFile(this.raf.getLocation());
        if (this.rafLength != this.raf.length() || this.indexLength != indexFile.length()) {
            GridIndex index = null;
            Label_0327: {
                if (GribGridServiceProvider.indexFileModeOnSync == IndexExtendMode.readonly) {
                    GribGridServiceProvider.log.debug("  sync() read Index = " + indexFile.getPath());
                    try {
                        index = new GribIndexReader().open(indexFile.getPath());
                        break Label_0327;
                    }
                    catch (Exception e) {
                        GribGridServiceProvider.log.warn("  sync() return false: GribReadIndex() failed = " + indexFile.getPath());
                        return false;
                    }
                }
                if (GribGridServiceProvider.indexFileModeOnSync == IndexExtendMode.extendwrite) {
                    if (this.rafLength <= this.raf.length() && this.indexLength <= indexFile.length()) {
                        if (GribGridServiceProvider.log.isDebugEnabled()) {
                            GribGridServiceProvider.log.debug("  sync() extend Index = " + indexFile.getPath());
                        }
                        index = this.extendIndex(new File(this.raf.getLocation()), indexFile, this.raf);
                    }
                    else {
                        if (GribGridServiceProvider.log.isDebugEnabled()) {
                            GribGridServiceProvider.log.debug("  sync() rewrite index = " + indexFile.getPath());
                        }
                        index = this.writeIndex(indexFile, this.raf);
                    }
                }
                else {
                    GribGridServiceProvider.log.debug("  sync() rewrite index = " + indexFile.getPath());
                    index = this.writeIndex(indexFile, this.raf);
                }
            }
            this.rafLength = this.raf.length();
            this.indexLength = indexFile.length();
            this.ncfile.empty();
            this.open(index, null);
            return true;
        }
        return false;
    }
    
    private GridIndex extendIndex(final File gribFile, final File indexFile, final RandomAccessFile raf) throws IOException {
        GridIndex index = null;
        if (this.saveEdition == 0) {
            raf.seek(0L);
            final Grib2Input g2i = new Grib2Input(raf);
            this.saveEdition = g2i.getEdition();
        }
        if (this.saveEdition == 1) {
            index = new Grib1WriteIndex().extendGribIndex(gribFile, indexFile, indexFile.getPath(), raf, true);
        }
        else if (this.saveEdition == 2) {
            index = new Grib2WriteIndex().extendGribIndex(gribFile, indexFile, indexFile.getPath(), raf, true);
        }
        return index;
    }
    
    private InputStream indexExistsAsURL(final String indexLocation) {
        try {
            final URL url = new URL(indexLocation);
            return url.openStream();
        }
        catch (Exception e) {
            return null;
        }
    }
    
    @Override
    protected float[] _readData(final GridRecord gr) throws IOException {
        final GribGridRecord ggr = (GribGridRecord)gr;
        if (this.saveEdition == 2) {
            return this.dataReaderGrib2.getData(ggr.getGdsOffset(), ggr.getPdsOffset(), ggr.getReferenceTimeInMsecs());
        }
        if (this.version >= 8.0f) {
            return this.dataReaderGrib1.getData(ggr.getGdsOffset(), ggr.getPdsOffset(), ggr.getDecimalScale(), ggr.isBmsExists());
        }
        return this.dataReaderGrib1.getData(ggr.getGdsOffset(), ggr.getDecimalScale(), ggr.isBmsExists());
    }
    
    static {
        GribGridServiceProvider.log = LoggerFactory.getLogger(GribGridServiceProvider.class);
    }
}
