// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.iosp.hdf5;

import java.io.OutputStream;
import ucar.nc2.util.IO;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.zip.InflaterInputStream;
import java.io.ByteArrayInputStream;
import java.nio.ByteBuffer;
import ucar.nc2.iosp.Layout;
import java.io.IOException;
import ucar.ma2.InvalidRangeException;
import ucar.ma2.DataType;
import ucar.nc2.Variable;
import ucar.ma2.Section;
import java.nio.ByteOrder;
import ucar.unidata.io.RandomAccessFile;
import ucar.nc2.iosp.LayoutBBTiled;
import ucar.nc2.iosp.LayoutBB;

class H5tiledLayoutBB implements LayoutBB
{
    private LayoutBBTiled delegate;
    private RandomAccessFile raf;
    private H5header.Filter[] filters;
    private ByteOrder byteOrder;
    private Section want;
    private int[] chunkSize;
    private int elemSize;
    private int nChunkDims;
    private boolean debug;
    
    H5tiledLayoutBB(final Variable v2, Section wantSection, final RandomAccessFile raf, final H5header.Filter[] filters, final ByteOrder byteOrder) throws InvalidRangeException, IOException {
        this.debug = false;
        wantSection = Section.fill(wantSection, v2.getShape());
        final H5header.Vinfo vinfo = (H5header.Vinfo)v2.getSPobject();
        assert vinfo.isChunked;
        assert vinfo.btree != null;
        this.raf = raf;
        this.filters = filters;
        this.byteOrder = byteOrder;
        final DataType dtype = v2.getDataType();
        if (dtype == DataType.CHAR && wantSection.getRank() < vinfo.storageSize.length) {
            this.want = new Section(wantSection).appendRange(1);
        }
        else {
            this.want = wantSection;
        }
        this.nChunkDims = ((dtype == DataType.CHAR) ? vinfo.storageSize.length : (vinfo.storageSize.length - 1));
        this.chunkSize = new int[this.nChunkDims];
        System.arraycopy(vinfo.storageSize, 0, this.chunkSize, 0, this.nChunkDims);
        this.elemSize = vinfo.storageSize[vinfo.storageSize.length - 1];
        final H5header.DataBTree.DataChunkIterator iter = vinfo.btree.getDataChunkIterator(this.want);
        final DataChunkIterator dcIter = new DataChunkIterator(iter);
        this.delegate = new LayoutBBTiled(dcIter, this.chunkSize, this.elemSize, this.want);
        if (this.debug) {
            System.out.println(" H5tiledLayout: " + this);
        }
    }
    
    public long getTotalNelems() {
        return this.delegate.getTotalNelems();
    }
    
    public int getElemSize() {
        return this.delegate.getElemSize();
    }
    
    public boolean hasNext() {
        return this.delegate.hasNext();
    }
    
    public Chunk next() throws IOException {
        return this.delegate.next();
    }
    
    @Override
    public String toString() {
        final StringBuilder sbuff = new StringBuilder();
        sbuff.append("want=").append(this.want).append("; ");
        sbuff.append("chunkSize=[");
        for (int i = 0; i < this.chunkSize.length; ++i) {
            if (i > 0) {
                sbuff.append(",");
            }
            sbuff.append(this.chunkSize[i]);
        }
        sbuff.append("] totalNelems=").append(this.getTotalNelems());
        sbuff.append(" elemSize=").append(this.elemSize);
        return sbuff.toString();
    }
    
    private class DataChunkIterator implements LayoutBBTiled.DataChunkIterator
    {
        H5header.DataBTree.DataChunkIterator delegate;
        
        DataChunkIterator(final H5header.DataBTree.DataChunkIterator delegate) {
            this.delegate = delegate;
        }
        
        public boolean hasNext() {
            return this.delegate.hasNext();
        }
        
        public LayoutBBTiled.DataChunk next() throws IOException {
            return new H5tiledLayoutBB.DataChunk(this.delegate.next());
        }
    }
    
    private class DataChunk implements LayoutBBTiled.DataChunk
    {
        H5header.DataBTree.DataChunk delegate;
        
        DataChunk(final H5header.DataBTree.DataChunk delegate) {
            this.delegate = delegate;
        }
        
        public int[] getOffset() {
            int[] offset = this.delegate.offset;
            if (offset.length > H5tiledLayoutBB.this.nChunkDims) {
                offset = new int[H5tiledLayoutBB.this.nChunkDims];
                System.arraycopy(this.delegate.offset, 0, offset, 0, H5tiledLayoutBB.this.nChunkDims);
            }
            return offset;
        }
        
        public ByteBuffer getByteBuffer() throws IOException {
            byte[] data = new byte[this.delegate.size];
            H5tiledLayoutBB.this.raf.seek(this.delegate.filePos);
            H5tiledLayoutBB.this.raf.readFully(data);
            for (int i = H5tiledLayoutBB.this.filters.length - 1; i >= 0; --i) {
                final H5header.Filter f = H5tiledLayoutBB.this.filters[i];
                if (this.isBitSet(this.delegate.filterMask, i)) {
                    if (H5tiledLayoutBB.this.debug) {
                        System.out.println("skip for chunk " + this.delegate);
                    }
                }
                else if (f.id == 1) {
                    data = this.inflate(data);
                }
                else if (f.id == 2) {
                    data = this.shuffle(data, f.data[0]);
                }
                else {
                    if (f.id != 3) {
                        throw new RuntimeException("Unknown filter type=" + f.id);
                    }
                    data = this.checkfletcher32(data);
                }
            }
            final ByteBuffer result = ByteBuffer.wrap(data);
            result.order(H5tiledLayoutBB.this.byteOrder);
            return result;
        }
        
        private byte[] inflate(final byte[] compressed) throws IOException {
            final ByteArrayInputStream in = new ByteArrayInputStream(compressed);
            final InflaterInputStream inflater = new InflaterInputStream(in);
            final ByteArrayOutputStream out = new ByteArrayOutputStream(8 * compressed.length);
            IO.copy(inflater, out);
            final byte[] uncomp = out.toByteArray();
            if (H5tiledLayoutBB.this.debug) {
                System.out.println(" inflate bytes in= " + compressed.length + " bytes out= " + uncomp.length);
            }
            return uncomp;
        }
        
        private byte[] checkfletcher32(final byte[] org) throws IOException {
            final byte[] result = new byte[org.length - 4];
            System.arraycopy(org, 0, result, 0, result.length);
            if (H5tiledLayoutBB.this.debug) {
                System.out.println(" checkfletcher32 bytes in= " + org.length + " bytes out= " + result.length);
            }
            return result;
        }
        
        private byte[] shuffle(final byte[] data, final int n) throws IOException {
            if (H5tiledLayoutBB.this.debug) {
                System.out.println(" shuffle bytes in= " + data.length + " n= " + n);
            }
            assert data.length % n == 0;
            if (n <= 1) {
                return data;
            }
            final int m = data.length / n;
            final int[] count = new int[n];
            for (int k = 0; k < n; ++k) {
                count[k] = k * m;
            }
            final byte[] result = new byte[data.length];
            for (int i = 0; i < m; ++i) {
                for (int j = 0; j < n; ++j) {
                    result[i * n + j] = data[i + count[j]];
                }
            }
            return result;
        }
        
        boolean isBitSet(final int val, final int bitno) {
            return (val >>> bitno & 0x1) != 0x0;
        }
    }
}
