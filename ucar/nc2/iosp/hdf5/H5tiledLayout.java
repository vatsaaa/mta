// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.iosp.hdf5;

import java.io.IOException;
import ucar.ma2.InvalidRangeException;
import ucar.ma2.DataType;
import ucar.ma2.Section;
import ucar.nc2.iosp.LayoutTiled;
import ucar.nc2.iosp.Layout;

class H5tiledLayout implements Layout
{
    private LayoutTiled delegate;
    private Section want;
    private int[] chunkSize;
    private int elemSize;
    private boolean debug;
    
    H5tiledLayout(final H5header.Vinfo vinfo, final DataType dtype, final Section wantSection) throws InvalidRangeException, IOException {
        this.debug = false;
        assert vinfo.isChunked;
        assert vinfo.btree != null;
        if (dtype == DataType.CHAR && wantSection.getRank() < vinfo.storageSize.length) {
            this.want = new Section(wantSection).appendRange(1);
        }
        else {
            this.want = wantSection;
        }
        final int nChunkDims = (dtype == DataType.CHAR) ? vinfo.storageSize.length : (vinfo.storageSize.length - 1);
        this.chunkSize = new int[nChunkDims];
        System.arraycopy(vinfo.storageSize, 0, this.chunkSize, 0, nChunkDims);
        this.elemSize = vinfo.storageSize[vinfo.storageSize.length - 1];
        if (this.debug) {
            System.out.println(" H5tiledLayout: " + this);
        }
        final LayoutTiled.DataChunkIterator iter = vinfo.btree.getDataChunkIterator2(this.want, nChunkDims);
        this.delegate = new LayoutTiled(iter, this.chunkSize, this.elemSize, this.want);
    }
    
    public long getTotalNelems() {
        return this.delegate.getTotalNelems();
    }
    
    public int getElemSize() {
        return this.delegate.getElemSize();
    }
    
    public boolean hasNext() {
        return this.delegate.hasNext();
    }
    
    public Chunk next() throws IOException {
        return this.delegate.next();
    }
    
    @Override
    public String toString() {
        final StringBuilder sbuff = new StringBuilder();
        sbuff.append("want=").append(this.want).append("; ");
        sbuff.append("chunkSize=[");
        for (int i = 0; i < this.chunkSize.length; ++i) {
            if (i > 0) {
                sbuff.append(",");
            }
            sbuff.append(this.chunkSize[i]);
        }
        sbuff.append("] totalNelems=").append(this.getTotalNelems());
        sbuff.append(" elemSize=").append(this.elemSize);
        return sbuff.toString();
    }
}
