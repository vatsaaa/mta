// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.iosp.hdf5;

import ucar.unidata.util.Format;
import ucar.unidata.util.SpecialMathFunction;
import ucar.nc2.iosp.LayoutTiled;
import java.util.TreeMap;
import java.nio.DoubleBuffer;
import java.nio.FloatBuffer;
import java.nio.LongBuffer;
import java.nio.IntBuffer;
import java.nio.ShortBuffer;
import org.slf4j.LoggerFactory;
import java.util.Formatter;
import java.util.Collections;
import java.util.Comparator;
import java.util.Collection;
import java.nio.ByteBuffer;
import java.util.TimeZone;
import java.text.ParseException;
import java.util.Date;
import ucar.ma2.IndexIterator;
import java.io.UnsupportedEncodingException;
import ucar.nc2.iosp.Layout;
import ucar.ma2.ArrayObject;
import ucar.ma2.ArrayStructureBB;
import ucar.nc2.iosp.LayoutRegular;
import ucar.ma2.Section;
import java.nio.ByteOrder;
import java.util.ArrayList;
import ucar.ma2.Array;
import ucar.ma2.StructureMembers;
import ucar.ma2.DataType;
import ucar.ma2.ArrayStructure;
import ucar.nc2.Dimension;
import ucar.nc2.Attribute;
import ucar.nc2.Variable;
import java.util.Iterator;
import ucar.ma2.InvalidRangeException;
import ucar.nc2.Structure;
import ucar.nc2.EnumTypedef;
import ucar.nc2.Group;
import java.util.List;
import java.util.HashMap;
import java.io.IOException;
import ucar.nc2.util.DebugFlags;
import java.io.PrintStream;
import java.text.SimpleDateFormat;
import ucar.nc2.units.DateFormatter;
import java.util.Map;
import ucar.nc2.NetcdfFile;
import ucar.unidata.io.RandomAccessFile;
import java.nio.charset.Charset;
import org.slf4j.Logger;

public class H5header
{
    private static Logger log;
    private static String utf8CharsetName;
    private static Charset utf8Charset;
    private static boolean debugEnum;
    private static boolean debugVlen;
    private static boolean debug1;
    private static boolean debugDetail;
    private static boolean debugPos;
    private static boolean debugHeap;
    private static boolean debugV;
    private static boolean debugGroupBtree;
    private static boolean debugDataBtree;
    private static boolean debugDataChunk;
    private static boolean debugBtree2;
    private static boolean debugContinueMessage;
    private static boolean debugTracker;
    private static boolean debugSoftLink;
    private static boolean debugSymbolTable;
    private static boolean warnings;
    private static boolean debugReference;
    private static boolean debugRegionReference;
    private static boolean debugCreationOrder;
    private static boolean debugFractalHeap;
    private static boolean debugDimensionScales;
    private static final byte[] head;
    private static final String hdf5magic;
    private static final long maxHeaderPos = 500000L;
    private static boolean transformReference;
    private RandomAccessFile raf;
    private NetcdfFile ncfile;
    private H5iosp h5iosp;
    private long baseAddress;
    private byte sizeOffsets;
    private byte sizeLengths;
    private boolean isOffsetLong;
    private boolean isLengthLong;
    private H5Group rootGroup;
    private Map<String, DataObjectFacade> symlinkMap;
    private Map<Long, DataObject> addressMap;
    private Map<Long, GlobalHeap> heapMap;
    DateFormatter formatter;
    private SimpleDateFormat hdfDateParser;
    private PrintStream debugOut;
    private MemTracker memTracker;
    private String[] filterName;
    
    public static void setDebugFlags(final DebugFlags debugFlag) {
        H5header.debug1 = debugFlag.isSet("H5header/header");
        H5header.debugBtree2 = debugFlag.isSet("H5header/btree2");
        H5header.debugContinueMessage = debugFlag.isSet("H5header/continueMessage");
        H5header.debugDetail = debugFlag.isSet("H5header/headerDetails");
        H5header.debugDataBtree = debugFlag.isSet("H5header/dataBtree");
        H5header.debugDataChunk = debugFlag.isSet("H5header/dataChunk");
        H5header.debugGroupBtree = debugFlag.isSet("H5header/groupBtree");
        H5header.debugFractalHeap = debugFlag.isSet("H5header/fractalHeap");
        H5header.debugHeap = debugFlag.isSet("H5header/Heap");
        H5header.debugPos = debugFlag.isSet("H5header/filePos");
        H5header.debugReference = debugFlag.isSet("H5header/reference");
        H5header.debugSoftLink = debugFlag.isSet("H5header/softLink");
        H5header.debugSymbolTable = debugFlag.isSet("H5header/symbolTable");
        H5header.debugTracker = debugFlag.isSet("H5header/memTracker");
        H5header.debugV = debugFlag.isSet("H5header/Variable");
    }
    
    static boolean isValidFile(final RandomAccessFile raf) throws IOException {
        long filePos = 0L;
        final long size = raf.length();
        final byte[] b = new byte[8];
        while (filePos < size && filePos < 500000L) {
            raf.seek(filePos);
            raf.read(b);
            final String magic = new String(b);
            if (magic.equals(H5header.hdf5magic)) {
                return true;
            }
            filePos = ((filePos == 0L) ? 512L : (2L * filePos));
        }
        return false;
    }
    
    H5header(final RandomAccessFile myRaf, final NetcdfFile ncfile, final H5iosp h5iosp) {
        this.symlinkMap = new HashMap<String, DataObjectFacade>(200);
        this.addressMap = new HashMap<Long, DataObject>(200);
        this.heapMap = new HashMap<Long, GlobalHeap>();
        this.formatter = new DateFormatter();
        this.debugOut = System.out;
        this.filterName = new String[] { "", "deflate", "shuffle", "fletcher32", "szip", "nbit", "scaleoffset" };
        this.ncfile = ncfile;
        this.raf = myRaf;
        this.h5iosp = h5iosp;
    }
    
    public void read(final PrintStream debugPS) throws IOException {
        if (debugPS != null) {
            this.debugOut = debugPS;
        }
        final long actualSize = this.raf.length();
        this.memTracker = new MemTracker(actualSize);
        if (!isValidFile(this.raf)) {
            throw new IOException("Not a netCDF4/HDF5 file ");
        }
        if (H5header.debug1) {
            this.debugOut.println("H5header 0pened file to read:'" + this.raf.getLocation() + "' size= " + actualSize);
        }
        this.raf.order(1);
        final long superblockStart = this.raf.getFilePointer() - 8L;
        this.memTracker.add("header", 0L, superblockStart);
        final byte versionSB = this.raf.readByte();
        if (versionSB < 2) {
            this.readSuperBlock1(superblockStart, versionSB);
        }
        else {
            if (versionSB != 2) {
                throw new IOException("Unknown superblock version= " + versionSB);
            }
            this.readSuperBlock2(superblockStart);
        }
        this.replaceSymbolicLinks(this.rootGroup);
        this.makeNetcdfGroup(this.ncfile.getRootGroup(), this.rootGroup);
        if (H5header.debugTracker) {
            this.memTracker.report();
        }
    }
    
    private void readSuperBlock1(final long superblockStart, final byte versionSB) throws IOException {
        final byte versionFSS = this.raf.readByte();
        final byte versionGroup = this.raf.readByte();
        this.raf.readByte();
        final byte versionSHMF = this.raf.readByte();
        if (H5header.debugDetail) {
            this.debugOut.println(" versionSB= " + versionSB + " versionFSS= " + versionFSS + " versionGroup= " + versionGroup + " versionSHMF= " + versionSHMF);
        }
        this.sizeOffsets = this.raf.readByte();
        this.isOffsetLong = (this.sizeOffsets == 8);
        this.sizeLengths = this.raf.readByte();
        this.isLengthLong = (this.sizeLengths == 8);
        if (H5header.debugDetail) {
            this.debugOut.println(" sizeOffsets= " + this.sizeOffsets + " sizeLengths= " + this.sizeLengths);
        }
        if (H5header.debugDetail) {
            this.debugOut.println(" isLengthLong= " + this.isLengthLong + " isOffsetLong= " + this.isOffsetLong);
        }
        this.raf.read();
        final short btreeLeafNodeSize = this.raf.readShort();
        final short btreeInternalNodeSize = this.raf.readShort();
        if (H5header.debugDetail) {
            this.debugOut.println(" btreeLeafNodeSize= " + btreeLeafNodeSize + " btreeInternalNodeSize= " + btreeInternalNodeSize);
        }
        final int fileFlags = this.raf.readInt();
        if (H5header.debugDetail) {
            this.debugOut.println(" fileFlags= 0x" + Integer.toHexString(fileFlags));
        }
        if (versionSB == 1) {
            final short storageInternalNodeSize = this.raf.readShort();
            this.raf.skipBytes(2);
        }
        this.baseAddress = this.readOffset();
        final long heapAddress = this.readOffset();
        long eofAddress = this.readOffset();
        final long driverBlockAddress = this.readOffset();
        if (this.baseAddress != superblockStart) {
            this.baseAddress = superblockStart;
            eofAddress += superblockStart;
            if (H5header.debugDetail) {
                this.debugOut.println(" baseAddress set to superblockStart");
            }
        }
        if (H5header.debugDetail) {
            this.debugOut.println(" baseAddress= 0x" + Long.toHexString(this.baseAddress));
            this.debugOut.println(" global free space heap Address= 0x" + Long.toHexString(heapAddress));
            this.debugOut.println(" eof Address=" + eofAddress);
            this.debugOut.println(" driver BlockAddress= 0x" + Long.toHexString(driverBlockAddress));
            this.debugOut.println();
        }
        this.memTracker.add("superblock", superblockStart, this.raf.getFilePointer());
        final long fileSize = this.raf.length();
        if (fileSize < eofAddress) {
            throw new IOException("File is truncated should be= " + eofAddress + " actual = " + fileSize + "%nlocation= " + this.raf.getLocation());
        }
        final SymbolTableEntry rootEntry = new SymbolTableEntry(this.raf.getFilePointer());
        final long rootObjectAddress = rootEntry.getObjectAddress();
        final DataObjectFacade f = new DataObjectFacade(null, "", rootObjectAddress);
        this.rootGroup = new H5Group(f);
    }
    
    private void readSuperBlock2(final long superblockStart) throws IOException {
        this.sizeOffsets = this.raf.readByte();
        this.isOffsetLong = (this.sizeOffsets == 8);
        this.sizeLengths = this.raf.readByte();
        this.isLengthLong = (this.sizeLengths == 8);
        if (H5header.debugDetail) {
            this.debugOut.println(" sizeOffsets= " + this.sizeOffsets + " sizeLengths= " + this.sizeLengths);
        }
        if (H5header.debugDetail) {
            this.debugOut.println(" isLengthLong= " + this.isLengthLong + " isOffsetLong= " + this.isOffsetLong);
        }
        final byte fileFlags = this.raf.readByte();
        if (H5header.debugDetail) {
            this.debugOut.println(" fileFlags= 0x" + Integer.toHexString(fileFlags));
        }
        this.baseAddress = this.readOffset();
        final long extensionAddress = this.readOffset();
        long eofAddress = this.readOffset();
        final long rootObjectAddress = this.readOffset();
        final int checksum = this.raf.readInt();
        if (H5header.debugDetail) {
            this.debugOut.println(" baseAddress= 0x" + Long.toHexString(this.baseAddress));
            this.debugOut.println(" extensionAddress= 0x" + Long.toHexString(extensionAddress));
            this.debugOut.println(" eof Address=" + eofAddress);
            this.debugOut.println(" rootObjectAddress= 0x" + Long.toHexString(rootObjectAddress));
            this.debugOut.println();
        }
        this.memTracker.add("superblock", superblockStart, this.raf.getFilePointer());
        if (this.baseAddress != superblockStart) {
            this.baseAddress = superblockStart;
            eofAddress += superblockStart;
            if (H5header.debugDetail) {
                this.debugOut.println(" baseAddress set to superblockStart");
            }
        }
        final long fileSize = this.raf.length();
        if (fileSize < eofAddress) {
            throw new IOException("File is truncated should be= " + eofAddress + " actual = " + fileSize);
        }
        final DataObjectFacade f = new DataObjectFacade(null, "", rootObjectAddress);
        this.rootGroup = new H5Group(f);
    }
    
    private void replaceSymbolicLinks(final H5Group group) {
        if (group == null) {
            return;
        }
        final List<DataObjectFacade> objList = group.nestedObjects;
        int count = 0;
        while (count < objList.size()) {
            final DataObjectFacade dof = objList.get(count);
            if (dof.group != null) {
                this.replaceSymbolicLinks(dof.group);
            }
            else if (dof.linkName != null) {
                final DataObjectFacade link = this.symlinkMap.get(dof.linkName);
                if (link == null) {
                    H5header.log.error(" WARNING Didnt find symbolic link=" + dof.linkName + " from " + dof.name);
                    objList.remove(count);
                    continue;
                }
                if (link.group != null && group.isChildOf(link.group)) {
                    H5header.log.error(" ERROR Symbolic Link loop found =" + dof.linkName);
                    objList.remove(count);
                    continue;
                }
                objList.set(count, link);
                if (H5header.debugSoftLink) {
                    this.debugOut.println("  Found symbolic link=" + dof.linkName);
                }
            }
            ++count;
        }
    }
    
    private void makeNetcdfGroup(final Group ncGroup, final H5Group h5group) throws IOException {
        if (h5group == null) {
            return;
        }
        for (final DataObjectFacade facade : h5group.nestedObjects) {
            if (facade.isVariable) {
                this.findDimensionScales(ncGroup, h5group, facade);
            }
        }
        for (final DataObjectFacade facade : h5group.nestedObjects) {
            if (facade.isVariable) {
                this.findDimensionLists(ncGroup, h5group, facade);
            }
        }
        this.createDimensions(ncGroup, h5group);
        for (final DataObjectFacade facadeNested : h5group.nestedObjects) {
            if (facadeNested.isGroup) {
                final Group nestedGroup = new Group(this.ncfile, ncGroup, facadeNested.name);
                ncGroup.addGroup(nestedGroup);
                if (H5header.debug1) {
                    this.debugOut.println("--made Group " + nestedGroup.getName() + " add to " + ncGroup.getName());
                }
                final H5Group h5groupNested = new H5Group(facadeNested);
                this.makeNetcdfGroup(nestedGroup, h5groupNested);
            }
            else if (facadeNested.isVariable) {
                if (H5header.debugReference && facadeNested.dobj.mdt.type == 7) {
                    this.debugOut.println(facadeNested);
                }
                final Variable v = this.makeVariable(ncGroup, facadeNested);
                if (v == null || v.getDataType() == null) {
                    continue;
                }
                v.setParentGroup(ncGroup);
                ncGroup.addVariable(v);
                if (v.getDataType().isEnum()) {
                    EnumTypedef enumTypedef = ncGroup.findEnumeration(facadeNested.name);
                    if (enumTypedef == null) {
                        enumTypedef = new EnumTypedef(facadeNested.name, facadeNested.dobj.mdt.map);
                        ncGroup.addEnumeration(enumTypedef);
                    }
                    v.setEnumTypedef(enumTypedef);
                }
                final Vinfo vinfo = (Vinfo)v.getSPobject();
                if (!H5header.debugV) {
                    continue;
                }
                this.debugOut.println("  made Variable " + v.getName() + "  vinfo= " + vinfo + "\n" + v);
            }
            else if (facadeNested.isTypedef) {
                if (H5header.debugReference && facadeNested.dobj.mdt.type == 7) {
                    this.debugOut.println(facadeNested);
                }
                if (facadeNested.dobj.mdt.map != null) {
                    EnumTypedef enumTypedef2 = ncGroup.findEnumeration(facadeNested.name);
                    if (enumTypedef2 == null) {
                        enumTypedef2 = new EnumTypedef(facadeNested.name, facadeNested.dobj.mdt.map);
                        ncGroup.addEnumeration(enumTypedef2);
                    }
                }
                if (!H5header.debugV) {
                    continue;
                }
                this.debugOut.println("  made enumeration " + facadeNested.name);
            }
            else {
                if (facadeNested.isDimensionNotVariable || !H5header.warnings) {
                    continue;
                }
                this.debugOut.println("WARN:  DataObject ndo " + facadeNested + " not a Group or a Variable");
            }
        }
        this.filterAttributes(h5group.facade.dobj.attributes);
        for (final MessageAttribute matt : h5group.facade.dobj.attributes) {
            try {
                this.makeAttributes(null, matt, ncGroup.getAttributes());
            }
            catch (InvalidRangeException e) {
                throw new IOException(e.getMessage());
            }
        }
        this.processSystemAttributes(h5group.facade.dobj.messages, ncGroup.getAttributes());
    }
    
    private void findDimensionScales(final Group g, final H5Group h5group, final DataObjectFacade facade) throws IOException {
        final Iterator<MessageAttribute> iter = facade.dobj.attributes.iterator();
        while (iter.hasNext()) {
            final MessageAttribute matt = iter.next();
            if (matt.name.equals("CLASS")) {
                final Attribute att = this.makeAttribute(matt);
                final String val = att.getStringValue();
                if (!val.equals("DIMENSION_SCALE")) {
                    continue;
                }
                facade.dimList = this.addDimension(g, h5group, facade.name, facade.dobj.mds.dimLength[0], facade.dobj.mds.maxLength[0] == -1);
                iter.remove();
                if (!H5header.debugDimensionScales) {
                    continue;
                }
                System.out.printf("Found dimScale %s for group '%s' matt=%s %n", facade.dimList, g.getName(), matt);
            }
        }
    }
    
    private void findDimensionLists(final Group g, final H5Group h5group, final DataObjectFacade facade) throws IOException {
        final Iterator<MessageAttribute> iter = facade.dobj.attributes.iterator();
        while (iter.hasNext()) {
            final MessageAttribute matt = iter.next();
            if (matt.name.equals("DIMENSION_LIST")) {
                final Attribute att = this.makeAttribute(matt);
                final StringBuilder sbuff = new StringBuilder();
                for (int i = 0; i < att.getLength(); ++i) {
                    final String name = att.getStringValue(i);
                    final String dimName = this.extendDimension(g, h5group, name, facade.dobj.mds.dimLength[i]);
                    sbuff.append(dimName).append(" ");
                }
                facade.dimList = sbuff.toString();
                if (H5header.debugDimensionScales) {
                    System.out.printf("Found dimList '%s' for group '%s' matt=%s %n", facade.dimList, g.getName(), matt);
                }
                iter.remove();
            }
            else if (matt.name.equals("NAME")) {
                final Attribute att = this.makeAttribute(matt);
                final String val = att.getStringValue();
                if (val.startsWith("This is a netCDF dimension but not a netCDF variable")) {
                    facade.isVariable = false;
                    facade.isDimensionNotVariable = true;
                }
                iter.remove();
                if (!H5header.debugDimensionScales) {
                    continue;
                }
                System.out.printf("Found %s %n", val);
            }
            else {
                if (!matt.name.equals("REFERENCE_LIST")) {
                    continue;
                }
                iter.remove();
            }
        }
    }
    
    private String addDimension(final Group g, final H5Group h5group, final String name, final int length, final boolean isUnlimited) {
        final int pos = name.lastIndexOf("/");
        final String dimName = (pos > 0) ? name.substring(pos + 1) : name;
        Dimension d = h5group.dimMap.get(dimName);
        if (d == null) {
            d = new Dimension(dimName, length, true, isUnlimited, false);
            d.setGroup(g);
            h5group.dimMap.put(dimName, d);
            h5group.dimList.add(d);
            if (H5header.debugDimensionScales) {
                this.debugOut.println("addDimension name=" + name + " dim= " + d + " to group " + g);
            }
        }
        return d.getName();
    }
    
    private String extendDimension(final Group g, final H5Group h5group, final String name, final int length) {
        final int pos = name.lastIndexOf("/");
        final String dimName = (pos > 0) ? name.substring(pos + 1) : name;
        Dimension d = h5group.dimMap.get(dimName);
        if (d == null) {
            d = g.findDimension(dimName);
        }
        if (d != null) {
            if (length > d.getLength()) {
                d.setLength(length);
            }
            return d.getName();
        }
        return null;
    }
    
    private void createDimensions(final Group g, final H5Group h5group) throws IOException {
        for (final Dimension d : h5group.dimList) {
            g.addDimension(d);
        }
    }
    
    private void filterAttributes(final List<MessageAttribute> attList) {
        final Iterator<MessageAttribute> iter = attList.iterator();
        while (iter.hasNext()) {
            final MessageAttribute matt = iter.next();
            if (matt.name.equals("_nc3_strict")) {
                iter.remove();
            }
        }
    }
    
    private void makeAttributes(final Structure s, final MessageAttribute matt, final List<Attribute> attList) throws IOException, InvalidRangeException {
        final MessageDatatype mdt = matt.mdt;
        if (mdt.type == 6) {
            final Vinfo vinfo = new Vinfo(matt.mdt, matt.mds, matt.dataPos);
            final ArrayStructure attData = (ArrayStructure)this.readAttributeData(matt, vinfo, DataType.STRUCTURE);
            if (null == s) {
                for (final StructureMembers.Member sm : attData.getStructureMembers().getMembers()) {
                    final Array memberData = attData.extractMemberArray(sm);
                    attList.add(new Attribute(matt.name + "." + sm.getName(), memberData));
                }
            }
            else {
                final StructureMembers smember = attData.getStructureMembers();
                for (final Variable v : s.getVariables()) {
                    final StructureMembers.Member sm2 = smember.findMember(v.getShortName());
                    if (null != sm2) {
                        final Array memberData2 = attData.extractMemberArray(sm2);
                        v.addAttribute(new Attribute(matt.name, memberData2));
                    }
                }
                for (final StructureMembers.Member sm3 : attData.getStructureMembers().getMembers()) {
                    if (s.findVariable(sm3.getName()) == null) {
                        final Array memberData3 = attData.extractMemberArray(sm3);
                        attList.add(new Attribute(matt.name + "." + sm3.getName(), memberData3));
                    }
                }
            }
        }
        else {
            attList.add(this.makeAttribute(matt));
        }
        this.raf.order(1);
    }
    
    private Attribute makeAttribute(final MessageAttribute matt) throws IOException {
        final Vinfo vinfo = new Vinfo(matt.mdt, matt.mds, matt.dataPos);
        final DataType dtype = vinfo.getNCDataType();
        if (matt.mds.type == 2) {
            return new Attribute(matt.name, dtype);
        }
        Array attData = null;
        try {
            attData = this.readAttributeData(matt, vinfo, dtype);
            attData.setUnsigned(matt.mdt.unsigned);
        }
        catch (InvalidRangeException e) {
            H5header.log.error("failed to read Attribute " + matt.name, e);
            return null;
        }
        Attribute result;
        if (attData.getElementType() == Array.class) {
            final List<Object> dataList = new ArrayList<Object>();
            while (attData.hasNext()) {
                final Array nested = (Array)attData.next();
                while (nested.hasNext()) {
                    dataList.add(nested.next());
                }
            }
            result = new Attribute(matt.name, dataList);
        }
        else {
            result = new Attribute(matt.name, attData);
        }
        this.raf.order(1);
        return result;
    }
    
    private Array readAttributeData(final MessageAttribute matt, final Vinfo vinfo, final DataType dataType) throws IOException, InvalidRangeException {
        final boolean debugStructure = false;
        int[] shape = matt.mds.dimLength;
        if (dataType == DataType.STRUCTURE) {
            boolean hasStrings = false;
            final StructureMembers sm = new StructureMembers(matt.name);
            for (final StructureMember h5sm : matt.mdt.members) {
                DataType dt = null;
                int[] dim = null;
                switch (h5sm.mdt.type) {
                    case 9: {
                        dt = DataType.STRING;
                        dim = new int[] { 1 };
                        break;
                    }
                    case 10: {
                        dt = this.getNCtype(h5sm.mdt.base.type, h5sm.mdt.base.byteSize);
                        dim = h5sm.mdt.dim;
                        break;
                    }
                    default: {
                        dt = this.getNCtype(h5sm.mdt.type, h5sm.mdt.byteSize);
                        dim = new int[] { 1 };
                        break;
                    }
                }
                final StructureMembers.Member m = sm.addMember(h5sm.name, null, null, dt, dim);
                if (h5sm.mdt.byteOrder >= 0) {
                    m.setDataObject((h5sm.mdt.byteOrder == 1) ? ByteOrder.LITTLE_ENDIAN : ByteOrder.BIG_ENDIAN);
                }
                m.setDataParam(h5sm.offset);
                if (dt == DataType.STRING) {
                    hasStrings = true;
                }
            }
            final int recsize = matt.mdt.byteSize;
            final Layout layout = new LayoutRegular(matt.dataPos, recsize, shape, new Section(shape));
            sm.setStructureSize(recsize);
            final ArrayStructureBB asbb = new ArrayStructureBB(sm, shape);
            final byte[] byteArray = asbb.getByteBuffer().array();
            while (layout.hasNext()) {
                final Layout.Chunk chunk = layout.next();
                if (chunk == null) {
                    continue;
                }
                if (debugStructure) {
                    System.out.println(" readStructure " + matt.name + " chunk= " + chunk + " index.getElemSize= " + layout.getElemSize());
                }
                this.raf.seek(chunk.getSrcPos());
                this.raf.read(byteArray, (int)chunk.getDestElem() * recsize, chunk.getNelems() * recsize);
            }
            if (hasStrings) {
                int destPos = 0;
                for (int i = 0; i < layout.getTotalNelems(); ++i) {
                    this.h5iosp.convertStrings(asbb, destPos, sm);
                    destPos += layout.getElemSize();
                }
            }
            return asbb;
        }
        if (vinfo.typeInfo.hdfType == 9 && vinfo.typeInfo.isVString) {
            final Layout layout2 = new LayoutRegular(matt.dataPos, matt.mdt.byteSize, shape, new Section(shape));
            final ArrayObject.D1 data = new ArrayObject.D1(String.class, (int)layout2.getTotalNelems());
            int count = 0;
            while (layout2.hasNext()) {
                final Layout.Chunk chunk2 = layout2.next();
                if (chunk2 == null) {
                    continue;
                }
                for (int j = 0; j < chunk2.getNelems(); ++j) {
                    final long address = chunk2.getSrcPos() + layout2.getElemSize() * j;
                    final String sval = this.readHeapString(address);
                    data.set(count++, sval);
                }
            }
            return data;
        }
        if (vinfo.typeInfo.hdfType == 9) {
            DataType readType = dataType;
            if (vinfo.typeInfo.base.hdfType == 7) {
                readType = DataType.LONG;
            }
            final Layout layout3 = new LayoutRegular(matt.dataPos, matt.mdt.byteSize, shape, new Section(shape));
            final boolean scalar = layout3.getTotalNelems() == 1L;
            final Array[] data2 = new Array[(int)layout3.getTotalNelems()];
            int count2 = 0;
            while (layout3.hasNext()) {
                final Layout.Chunk chunk3 = layout3.next();
                if (chunk3 == null) {
                    continue;
                }
                for (int k = 0; k < chunk3.getNelems(); ++k) {
                    final long address2 = chunk3.getSrcPos() + layout3.getElemSize() * k;
                    final Array vlenArray = this.getHeapDataArray(address2, readType, vinfo.typeInfo.byteOrder);
                    if (vinfo.typeInfo.base.hdfType == 7) {
                        data2[count2++] = this.h5iosp.convertReference(vlenArray);
                    }
                    else {
                        data2[count2++] = vlenArray;
                    }
                }
            }
            return scalar ? data2[0] : Array.factory(Array.class, shape, data2);
        }
        DataType readDtype = dataType;
        int elemSize = dataType.getSize();
        int byteOrder = vinfo.typeInfo.byteOrder;
        if (vinfo.typeInfo.hdfType == 2) {
            readDtype = vinfo.mdt.timeType;
            elemSize = readDtype.getSize();
        }
        else if (vinfo.typeInfo.hdfType == 3) {
            if (vinfo.mdt.byteSize > 1) {
                final int[] newShape = new int[shape.length + 1];
                System.arraycopy(shape, 0, newShape, 0, shape.length);
                newShape[shape.length] = vinfo.mdt.byteSize;
                shape = newShape;
            }
        }
        else if (vinfo.typeInfo.hdfType == 5) {
            elemSize = vinfo.mdt.byteSize;
        }
        else if (vinfo.typeInfo.hdfType == 8) {
            final TypeInfo baseInfo = vinfo.typeInfo.base;
            readDtype = baseInfo.dataType;
            elemSize = readDtype.getSize();
            byteOrder = baseInfo.byteOrder;
        }
        final Layout layout = new LayoutRegular(matt.dataPos, elemSize, shape, new Section(shape));
        final Object data3 = this.h5iosp.readDataPrimitive(layout, dataType, shape, null, byteOrder, false);
        Array dataArray = null;
        if (dataType == DataType.CHAR) {
            if (vinfo.mdt.byteSize > 1) {
                final byte[] bdata = (byte[])data3;
                final int strlen = vinfo.mdt.byteSize;
                final int n = bdata.length / strlen;
                final ArrayObject.D1 sarray = new ArrayObject.D1(String.class, n);
                for (int l = 0; l < n; ++l) {
                    final String sval2 = this.convertString(bdata, l * strlen, strlen);
                    sarray.set(l, sval2);
                }
                dataArray = sarray;
            }
            else {
                final String sval3 = this.convertString((byte[])data3);
                final ArrayObject.D1 sarray2 = new ArrayObject.D1(String.class, 1);
                sarray2.set(0, sval3);
                dataArray = sarray2;
            }
        }
        else {
            dataArray = (Array)((data3 instanceof Array) ? data3 : Array.factory(readDtype, shape, data3));
        }
        if (vinfo.typeInfo.hdfType == 8 && matt.mdt.map != null) {
            dataArray = this.convertEnums(matt.mdt.map, dataArray);
        }
        return dataArray;
    }
    
    private String convertString(final byte[] b) throws UnsupportedEncodingException {
        int count;
        for (count = 0; count < b.length && b[count] != 0; ++count) {}
        return new String(b, 0, count, "UTF-8");
    }
    
    private String convertString(final byte[] b, final int start, final int len) throws UnsupportedEncodingException {
        int count;
        for (count = start; count < start + len && b[count] != 0; ++count) {}
        return new String(b, start, count - start, "UTF-8");
    }
    
    protected Array convertEnums(final Map<Integer, String> map, final Array values) {
        final Array result = Array.factory(DataType.STRING, values.getShape());
        final IndexIterator ii = result.getIndexIterator();
        values.resetLocalIterator();
        while (values.hasNext()) {
            final String sval = map.get(values.nextInt());
            ii.setObjectNext((sval == null) ? "Unknown" : sval);
        }
        return result;
    }
    
    private Variable makeVariable(final Group ncGroup, final DataObjectFacade facade) throws IOException {
        final Vinfo vinfo = new Vinfo(facade);
        if (vinfo.getNCDataType() == null) {
            this.debugOut.println("SKIPPING DataType= " + vinfo.typeInfo.hdfType + " for variable " + facade.name);
            return null;
        }
        if (facade.dobj.mfp != null) {
            for (final Filter f : facade.dobj.mfp.filters) {
                if (f.id == 4) {
                    this.debugOut.println("SKIPPING variable with SZIP Filter= " + facade.dobj.mfp + " for variable " + facade.name);
                    return null;
                }
            }
        }
        Attribute fillAttribute = null;
        for (final HeaderMessage mess : facade.dobj.messages) {
            if (mess.mtype == MessageType.FillValue) {
                final MessageFillValue fvm = (MessageFillValue)mess.messData;
                if (fvm.hasFillValue) {
                    vinfo.fillValue = fvm.value;
                }
            }
            else if (mess.mtype == MessageType.FillValueOld) {
                final MessageFillValueOld fvm2 = (MessageFillValueOld)mess.messData;
                if (fvm2.size > 0) {
                    vinfo.fillValue = fvm2.value;
                }
            }
            final Object fillValue = vinfo.getFillValueNonDefault();
            if (fillValue != null) {
                final Object defFillValue = vinfo.getFillValueDefault(vinfo.typeInfo.dataType);
                if (fillValue.equals(defFillValue)) {
                    continue;
                }
                fillAttribute = new Attribute("_FillValue", (Number)fillValue);
            }
        }
        final long dataAddress = facade.dobj.msl.dataAddress;
        if (dataAddress == -1L) {
            vinfo.useFillValue = true;
            if (vinfo.fillValue == null) {
                vinfo.fillValue = new byte[vinfo.typeInfo.dataType.getSize()];
            }
        }
        Structure s = null;
        Variable v;
        if (facade.dobj.mdt.type == 6) {
            final String vname = facade.name;
            s = (Structure)(v = new Structure(this.ncfile, ncGroup, null, vname));
            if (!this.makeVariableShapeAndType(v, facade.dobj.mdt, facade.dobj.mds, vinfo, facade.dimList)) {
                return null;
            }
            this.addMembersToStructure(ncGroup, s, vinfo, facade.dobj.mdt);
            v.setElementSize(facade.dobj.mdt.byteSize);
        }
        else {
            final String vname = facade.name;
            v = new Variable(this.ncfile, ncGroup, null, vname);
            if (!this.makeVariableShapeAndType(v, facade.dobj.mdt, facade.dobj.mds, vinfo, facade.dimList)) {
                return null;
            }
        }
        if (v.getDataType() == DataType.STRING) {
            v.setElementSize(16);
        }
        v.setSPobject(vinfo);
        for (final MessageAttribute matt : facade.dobj.attributes) {
            try {
                this.makeAttributes(s, matt, v.getAttributes());
            }
            catch (InvalidRangeException e) {
                throw new IOException(e.getMessage());
            }
        }
        this.processSystemAttributes(facade.dobj.messages, v.getAttributes());
        if (fillAttribute != null && v.findAttribute("_FillValue") == null) {
            v.addAttribute(fillAttribute);
        }
        if (vinfo.typeInfo.unsigned) {
            v.addAttribute(new Attribute("_Unsigned", "true"));
        }
        if (facade.dobj.mdt.type == 5) {
            final String desc = facade.dobj.mdt.opaque_desc;
            if (desc != null && desc.length() > 0) {
                v.addAttribute(new Attribute("_opaqueDesc", desc));
            }
        }
        if (vinfo.isChunked) {
            vinfo.btree = new DataBTree(dataAddress, v.getShape(), vinfo.storageSize);
        }
        if (H5header.transformReference && facade.dobj.mdt.type == 7 && facade.dobj.mdt.referenceType == 0) {
            final Array newData = this.findReferenceObjectNames(v.read());
            v.setDataType(DataType.STRING);
            v.setCachedData(newData, true);
            v.addAttribute(new Attribute("_HDF5ReferenceType", "values are names of referenced Variables"));
        }
        if (H5header.transformReference && facade.dobj.mdt.type == 7 && facade.dobj.mdt.referenceType == 1) {
            H5header.log.warn("transform region Reference: facade=" + facade.name + " variable name=" + v.getName());
            final int nelems = (int)v.getSize();
            final int heapIdSize = 12;
            v.setDataType(DataType.LONG);
            final Array newData2 = Array.factory(DataType.LONG, v.getShape());
            v.setCachedData(newData2, true);
            v.addAttribute(new Attribute("_HDF5ReferenceType", "values are regions of referenced Variables"));
        }
        vinfo.setOwner(v);
        if (vinfo.typeInfo.hdfType == 7 && H5header.warnings) {
            this.debugOut.println("WARN:  Variable " + facade.name + " is a Reference type");
        }
        if (vinfo.mfp != null && vinfo.mfp.filters[0].id != 1 && H5header.warnings) {
            this.debugOut.println("WARN:  Variable " + facade.name + " has a Filter = " + vinfo.mfp);
        }
        if (H5header.debug1) {
            this.debugOut.println("makeVariable " + v.getName() + "; vinfo= " + vinfo);
        }
        return v;
    }
    
    private Array findReferenceObjectNames(final Array data) throws IOException {
        final IndexIterator ii = data.getIndexIterator();
        final Array newData = Array.factory(DataType.STRING, data.getShape());
        final IndexIterator ii2 = newData.getIndexIterator();
        while (ii.hasNext()) {
            final long objId = ii.getLongNext();
            final DataObject dobj = this.getDataObject(objId, null);
            if (dobj == null) {
                H5header.log.error("readReferenceObjectNames cant find obj= " + objId);
            }
            else {
                if (H5header.debugReference) {
                    System.out.println(" Referenced object= " + dobj.who);
                }
                ii2.setObjectNext(dobj.who);
            }
        }
        return newData;
    }
    
    private void addMembersToStructure(final Group g, final Structure s, final Vinfo parentVinfo, final MessageDatatype mdt) throws IOException {
        for (final StructureMember m : mdt.members) {
            final Variable v = this.makeVariableMember(g, s, m.name, m.offset, m.mdt);
            if (v != null) {
                s.addMemberVariable(v);
                if (!H5header.debug1) {
                    continue;
                }
                this.debugOut.println("  made Member Variable " + v.getName() + "\n" + v);
            }
        }
    }
    
    private Variable makeVariableMember(final Group g, final Structure s, final String name, final long dataPos, final MessageDatatype mdt) throws IOException {
        final Vinfo vinfo = new Vinfo(mdt, null, dataPos);
        if (vinfo.getNCDataType() == null) {
            this.debugOut.println("SKIPPING DataType= " + vinfo.typeInfo.hdfType + " for variable " + name);
            return null;
        }
        Variable v;
        if (mdt.type == 6) {
            v = new Structure(this.ncfile, g, s, name);
            this.makeVariableShapeAndType(v, mdt, null, vinfo, null);
            this.addMembersToStructure(g, (Structure)v, vinfo, mdt);
            v.setElementSize(mdt.byteSize);
        }
        else {
            v = new Variable(this.ncfile, g, s, name);
            this.makeVariableShapeAndType(v, mdt, null, vinfo, null);
        }
        if (v.getDataType() == DataType.STRING) {
            v.setElementSize(16);
        }
        v.setSPobject(vinfo);
        vinfo.setOwner(v);
        if (vinfo.typeInfo.unsigned) {
            v.addAttribute(new Attribute("_Unsigned", "true"));
        }
        return v;
    }
    
    private void processSystemAttributes(final List<HeaderMessage> messages, final List<Attribute> attributes) {
        for (final HeaderMessage mess : messages) {
            if (mess.mtype == MessageType.LastModified) {
                final MessageLastModified m = (MessageLastModified)mess.messData;
                final Date d = new Date(m.secs * 1000L);
                attributes.add(new Attribute("_lastModified", this.formatter.toDateTimeStringISO(d)));
            }
            else if (mess.mtype == MessageType.LastModifiedOld) {
                final MessageLastModifiedOld i = (MessageLastModifiedOld)mess.messData;
                try {
                    final Date d = this.getHdfDateFormatter().parse(i.datemod);
                    attributes.add(new Attribute("_lastModified", this.formatter.toDateTimeStringISO(d)));
                }
                catch (ParseException ex) {
                    this.debugOut.println("ERROR parsing date from MessageLastModifiedOld = " + i.datemod);
                }
            }
            else {
                if (mess.mtype != MessageType.Comment) {
                    continue;
                }
                final MessageComment j = (MessageComment)mess.messData;
                attributes.add(new Attribute("_comment", j.comment));
            }
        }
    }
    
    private SimpleDateFormat getHdfDateFormatter() {
        if (this.hdfDateParser == null) {
            (this.hdfDateParser = new SimpleDateFormat("yyyyMMddHHmmss")).setTimeZone(TimeZone.getTimeZone("GMT"));
        }
        return this.hdfDateParser;
    }
    
    private boolean makeVariableShapeAndType(final Variable v, final MessageDatatype mdt, final MessageDataspace msd, final Vinfo vinfo, final String dims) {
        int[] dim = (msd != null) ? msd.dimLength : new int[0];
        if (dim == null) {
            dim = new int[0];
        }
        if (mdt.type == 10) {
            final int len = dim.length + mdt.dim.length;
            final int[] combinedDim = new int[len];
            for (int i = 0; i < dim.length; ++i) {
                combinedDim[i] = dim[i];
            }
            for (int i = 0; i < mdt.dim.length; ++i) {
                combinedDim[dim.length + i] = mdt.dim[i];
            }
            dim = combinedDim;
        }
        try {
            if (dims != null) {
                if (mdt.type == 9 && !mdt.isVString) {
                    v.setDimensions(dims + " *");
                }
                else {
                    v.setDimensions(dims);
                }
            }
            else if (mdt.type == 3) {
                if (mdt.byteSize == 1) {
                    v.setDimensionsAnonymous(dim);
                }
                else {
                    final int[] shape = new int[dim.length + 1];
                    System.arraycopy(dim, 0, shape, 0, dim.length);
                    shape[dim.length] = mdt.byteSize;
                    v.setDimensionsAnonymous(shape);
                }
            }
            else if (mdt.type == 9 && !mdt.isVString) {
                if (dim.length == 1 && dim[0] == 1) {
                    final int[] shape = { -1 };
                    v.setDimensionsAnonymous(shape);
                }
                else {
                    final int[] shape = new int[dim.length + 1];
                    System.arraycopy(dim, 0, shape, 0, dim.length);
                    shape[dim.length] = -1;
                    v.setDimensionsAnonymous(shape);
                }
            }
            else {
                v.setDimensionsAnonymous(dim);
            }
        }
        catch (InvalidRangeException ee) {
            H5header.log.error(ee.getMessage());
            this.debugOut.println("ERROR: makeVariableShapeAndType " + ee.getMessage());
            return false;
        }
        final DataType dt = vinfo.getNCDataType();
        if (dt == null) {
            return false;
        }
        v.setDataType(dt);
        if (dt.isEnum()) {
            final Group ncGroup = v.getParentGroup();
            EnumTypedef enumTypedef = ncGroup.findEnumeration(mdt.enumTypeName);
            if (enumTypedef == null) {
                enumTypedef = new EnumTypedef(mdt.enumTypeName, mdt.map);
            }
            v.setEnumTypedef(enumTypedef);
        }
        return true;
    }
    
    private DataType getNCtype(final int hdfType, final int size) {
        if (hdfType == 0 || hdfType == 4) {
            if (size == 1) {
                return DataType.BYTE;
            }
            if (size == 2) {
                return DataType.SHORT;
            }
            if (size == 4) {
                return DataType.INT;
            }
            if (size == 8) {
                return DataType.LONG;
            }
            if (H5header.warnings) {
                this.debugOut.println("WARNING HDF5 file " + this.ncfile.getLocation() + " not handling hdf integer type (" + hdfType + ") with size= " + size);
                H5header.log.warn("HDF5 file " + this.ncfile.getLocation() + " not handling hdf integer type (" + hdfType + ") with size= " + size);
                return null;
            }
        }
        else if (hdfType == 1) {
            if (size == 4) {
                return DataType.FLOAT;
            }
            if (size == 8) {
                return DataType.DOUBLE;
            }
            if (H5header.warnings) {
                this.debugOut.println("WARNING HDF5 file " + this.ncfile.getLocation() + " not handling hdf float type with size= " + size);
                H5header.log.warn("HDF5 file " + this.ncfile.getLocation() + " not handling hdf float type with size= " + size);
                return null;
            }
        }
        else {
            if (hdfType == 3) {
                return DataType.CHAR;
            }
            if (hdfType == 7) {
                return DataType.LONG;
            }
            if (H5header.warnings) {
                this.debugOut.println("WARNING not handling hdf type = " + hdfType + " size= " + size);
                H5header.log.warn("HDF5 file " + this.ncfile.getLocation() + " not handling hdf type = " + hdfType + " size= " + size);
            }
        }
        return null;
    }
    
    private DataObject getDataObject(final long address, final String name) throws IOException {
        DataObject dobj = this.addressMap.get(address);
        if (dobj != null) {
            if (dobj.who == null && name != null) {
                dobj.who = name;
            }
            return dobj;
        }
        dobj = new DataObject(address, name);
        this.addressMap.put(address, dobj);
        return dobj;
    }
    
    private DataObject getSharedDataObject(final MessageType mtype) throws IOException {
        final byte sharedVersion = this.raf.readByte();
        final byte sharedType = this.raf.readByte();
        if (sharedVersion == 1) {
            this.raf.skipBytes(6);
        }
        if (sharedVersion == 3 && sharedType == 1) {
            final long heapId = this.raf.readLong();
            if (H5header.debug1) {
                this.debugOut.println("     Shared Message " + sharedVersion + " type=" + sharedType + " heapId = " + heapId);
            }
            if (H5header.debugPos) {
                this.debugOut.println("  --> Shared Message reposition to =" + this.raf.getFilePointer());
            }
            throw new UnsupportedOperationException("****SHARED MESSAGE type = " + mtype + " heapId = " + heapId);
        }
        final long address = this.readOffset();
        if (H5header.debug1) {
            this.debugOut.println("     Shared Message " + sharedVersion + " type=" + sharedType + " address = " + address);
        }
        final DataObject dobj = this.getDataObject(address, null);
        if (null == dobj) {
            throw new IllegalStateException("cant find data object at" + address);
        }
        if (mtype == MessageType.Datatype) {
            return dobj;
        }
        throw new UnsupportedOperationException("****SHARED MESSAGE type = " + mtype);
    }
    
    private void readGroupNew(final H5Group group, final MessageGroupNew groupNewMessage, final DataObject dobj) throws IOException {
        if (H5header.debug1) {
            this.debugOut.println("\n--> GroupNew read <" + group.displayName + ">");
        }
        if (groupNewMessage.fractalHeapAddress >= 0L) {
            final FractalHeap fractalHeap = new FractalHeap(group.displayName, groupNewMessage.fractalHeapAddress);
            final long btreeAddress = (groupNewMessage.v2BtreeAddressCreationOrder >= 0L) ? groupNewMessage.v2BtreeAddressCreationOrder : groupNewMessage.v2BtreeAddress;
            if (btreeAddress < 0L) {
                throw new IllegalStateException("no valid btree for GroupNew with Fractal Heap");
            }
            final BTree2 btree = new BTree2(group.displayName, btreeAddress);
            for (final BTree2.Entry2 e : btree.entryList) {
                byte[] heapId = null;
                switch (btree.btreeType) {
                    case 5: {
                        heapId = ((BTree2.Record5)e.record).heapId;
                        break;
                    }
                    case 6: {
                        heapId = ((BTree2.Record6)e.record).heapId;
                        break;
                    }
                    default: {
                        continue;
                    }
                }
                final long pos = fractalHeap.getHeapId(heapId).getPos();
                if (pos < 0L) {
                    continue;
                }
                this.raf.seek(pos);
                final MessageLink linkMessage = new MessageLink();
                linkMessage.read();
                if (H5header.debugBtree2) {
                    System.out.println("    linkMessage=" + linkMessage);
                }
                group.nestedObjects.add(new DataObjectFacade(group, linkMessage.linkName, linkMessage.linkAddress));
            }
        }
        else {
            for (final HeaderMessage mess : dobj.messages) {
                if (mess.mtype == MessageType.Link) {
                    final MessageLink linkMessage2 = (MessageLink)mess.messData;
                    if (linkMessage2.linkType != 0) {
                        continue;
                    }
                    group.nestedObjects.add(new DataObjectFacade(group, linkMessage2.linkName, linkMessage2.linkAddress));
                }
            }
        }
        if (H5header.debug1) {
            this.debugOut.println("<-- end GroupNew read <" + group.displayName + ">");
        }
    }
    
    private void readGroupOld(final H5Group group, final long btreeAddress, final long nameHeapAddress) throws IOException {
        if (H5header.debug1) {
            this.debugOut.println("\n--> GroupOld read <" + group.displayName + ">");
        }
        final LocalHeap nameHeap = new LocalHeap(group, nameHeapAddress);
        final GroupBTree btree = new GroupBTree(group.displayName, btreeAddress);
        for (final SymbolTableEntry s : btree.getSymbolTableEntries()) {
            final String sname = nameHeap.getString((int)s.getNameOffset());
            if (H5header.debugSoftLink) {
                this.debugOut.println("\n   Symbol name=" + sname);
            }
            if (s.cacheType == 2) {
                final String linkName = nameHeap.getString(s.linkOffset);
                if (H5header.debugSoftLink) {
                    this.debugOut.println("   Symbolic link name=" + linkName);
                }
                group.nestedObjects.add(new DataObjectFacade(group, sname, linkName));
            }
            else {
                group.nestedObjects.add(new DataObjectFacade(group, sname, s.getObjectAddress()));
            }
        }
        if (H5header.debug1) {
            this.debugOut.println("<-- end GroupOld read <" + group.displayName + ">");
        }
    }
    
    Array getHeapDataArray(final long globalHeapIdAddress, final DataType dataType, final int byteOrder) throws IOException {
        final HeapIdentifier heapId = new HeapIdentifier(globalHeapIdAddress);
        if (H5header.debugHeap) {
            this.debugOut.println(" heapId= " + heapId);
        }
        final GlobalHeap.HeapObject ho = heapId.getHeapObject();
        if (H5header.debugHeap) {
            this.debugOut.println(" HeapObject= " + ho);
        }
        if (byteOrder >= 0) {
            this.raf.order(byteOrder);
        }
        if (DataType.FLOAT == dataType) {
            final float[] pa = new float[heapId.nelems];
            this.raf.seek(ho.dataPos);
            this.raf.readFloat(pa, 0, pa.length);
            return Array.factory(dataType.getPrimitiveClassType(), new int[] { pa.length }, pa);
        }
        if (DataType.DOUBLE == dataType) {
            final double[] pa2 = new double[heapId.nelems];
            this.raf.seek(ho.dataPos);
            this.raf.readDouble(pa2, 0, pa2.length);
            return Array.factory(dataType.getPrimitiveClassType(), new int[] { pa2.length }, pa2);
        }
        if (DataType.BYTE == dataType) {
            final byte[] pa3 = new byte[heapId.nelems];
            this.raf.seek(ho.dataPos);
            this.raf.read(pa3, 0, pa3.length);
            return Array.factory(dataType.getPrimitiveClassType(), new int[] { pa3.length }, pa3);
        }
        if (DataType.SHORT == dataType) {
            final short[] pa4 = new short[heapId.nelems];
            this.raf.seek(ho.dataPos);
            this.raf.readShort(pa4, 0, pa4.length);
            return Array.factory(dataType.getPrimitiveClassType(), new int[] { pa4.length }, pa4);
        }
        if (DataType.INT == dataType) {
            final int[] pa5 = new int[heapId.nelems];
            this.raf.seek(ho.dataPos);
            this.raf.readInt(pa5, 0, pa5.length);
            return Array.factory(dataType.getPrimitiveClassType(), new int[] { pa5.length }, pa5);
        }
        if (DataType.LONG == dataType) {
            final long[] pa6 = new long[heapId.nelems];
            this.raf.seek(ho.dataPos);
            this.raf.readLong(pa6, 0, pa6.length);
            return Array.factory(dataType.getPrimitiveClassType(), new int[] { pa6.length }, pa6);
        }
        throw new UnsupportedOperationException("getHeapDataAsArray dataType=" + dataType);
    }
    
    String readHeapString(final long heapIdAddress) throws IOException {
        final HeapIdentifier heapId = new HeapIdentifier(heapIdAddress);
        final GlobalHeap.HeapObject ho = heapId.getHeapObject();
        this.raf.seek(ho.dataPos);
        return this.readStringFixedLength((int)ho.dataSize);
    }
    
    String readHeapString(final ByteBuffer bb, final int pos) throws IOException {
        final HeapIdentifier heapId = new HeapIdentifier(bb, pos);
        final GlobalHeap.HeapObject ho = heapId.getHeapObject();
        this.raf.seek(ho.dataPos);
        return this.readStringFixedLength((int)ho.dataSize);
    }
    
    public List<DataObject> getDataObjects() {
        final ArrayList<DataObject> result = new ArrayList<DataObject>(this.addressMap.values());
        Collections.sort(result, new Comparator<DataObject>() {
            public int compare(final DataObject o1, final DataObject o2) {
                return (o1.address < o2.address) ? -1 : ((o1.address == o2.address) ? 0 : 1);
            }
        });
        return result;
    }
    
    String getDataObjectName(final long objId) throws IOException {
        final DataObject dobj = this.getDataObject(objId, null);
        if (dobj == null) {
            H5header.log.error("H5iosp.readVlenData cant find dataObject id= " + objId);
            return null;
        }
        if (H5header.debugVlen) {
            System.out.println(" Referenced object= " + dobj.who);
        }
        return dobj.who;
    }
    
    private int makeIntFromBytes(final byte[] bb, final int start, final int n) {
        int result = 0;
        for (int i = start + n - 1; i >= start; --i) {
            result <<= 8;
            final byte b = bb[i];
            result += ((b < 0) ? (b + 256) : b);
        }
        return result;
    }
    
    private String readString(final RandomAccessFile raf) throws IOException {
        final long filePos = raf.getFilePointer();
        int count = 0;
        while (raf.readByte() != 0) {
            ++count;
        }
        raf.seek(filePos);
        final byte[] s = new byte[count];
        raf.read(s);
        raf.readByte();
        return new String(s, H5header.utf8CharsetName);
    }
    
    private String readString8(final RandomAccessFile raf) throws IOException {
        final long filePos = raf.getFilePointer();
        int count = 0;
        while (raf.readByte() != 0) {
            ++count;
        }
        raf.seek(filePos);
        final byte[] s = new byte[count];
        raf.read(s);
        count = ++count + this.padding(count, 8);
        raf.seek(filePos + count);
        try {
            return new String(s, H5header.utf8CharsetName);
        }
        catch (UnsupportedEncodingException e) {
            throw new IllegalStateException(e.getMessage());
        }
    }
    
    private String readStringFixedLength(final int size) throws IOException {
        final byte[] s = new byte[size];
        this.raf.read(s);
        return new String(s, H5header.utf8CharsetName);
    }
    
    private long readLength() throws IOException {
        return this.isLengthLong ? this.raf.readLong() : this.raf.readInt();
    }
    
    private long readOffset() throws IOException {
        return this.isOffsetLong ? this.raf.readLong() : this.raf.readInt();
    }
    
    private int getNumBytesFromMax(long maxNumber) {
        int size = 0;
        while (maxNumber != 0L) {
            ++size;
            maxNumber >>>= 8;
        }
        return size;
    }
    
    private long readVariableSizeMax(final int maxNumber) throws IOException {
        final int size = this.getNumBytesFromMax(maxNumber);
        return this.readVariableSizeUnsigned(size);
    }
    
    private long readVariableSizeFactor(final int sizeFactor) throws IOException {
        final int size = (int)Math.pow(2.0, sizeFactor);
        return this.readVariableSizeUnsigned(size);
    }
    
    private long readVariableSizeUnsigned(final int size) throws IOException {
        long vv;
        if (size == 1) {
            vv = DataType.unsignedByteToShort(this.raf.readByte());
        }
        else if (size == 2) {
            if (H5header.debugPos) {
                this.debugOut.println("position=" + this.raf.getFilePointer());
            }
            final short s = this.raf.readShort();
            vv = DataType.unsignedShortToInt(s);
        }
        else if (size == 4) {
            vv = DataType.unsignedIntToLong(this.raf.readInt());
        }
        else if (size == 8) {
            vv = this.raf.readLong();
        }
        else {
            vv = this.readVariableSizeN(size);
        }
        return vv;
    }
    
    private int readVariableSize(final int size) throws IOException {
        if (size == 1) {
            return this.raf.readByte();
        }
        if (size == 2) {
            return this.raf.readShort();
        }
        if (size == 4) {
            return this.raf.readInt();
        }
        throw new IllegalArgumentException("Dont support int size == " + size);
    }
    
    private long readVariableSizeN(final int nbytes) throws IOException {
        final int[] ch = new int[nbytes];
        for (int i = 0; i < nbytes; ++i) {
            ch[i] = this.raf.read();
        }
        long result = ch[nbytes - 1];
        for (int j = nbytes - 2; j >= 0; --j) {
            result <<= 8;
            result += ch[j];
        }
        return result;
    }
    
    private long getFileOffset(final long address) throws IOException {
        return this.baseAddress + address;
    }
    
    private int makeUnsignedIntFromBytes(final byte upper, final byte lower) {
        return DataType.unsignedByteToShort(upper) * 256 + DataType.unsignedByteToShort(lower);
    }
    
    private int padding(final int nbytes, final int multipleOf) {
        int pad = nbytes % multipleOf;
        if (pad != 0) {
            pad = multipleOf - pad;
        }
        return pad;
    }
    
    void dump(final String head, final long filePos, final int nbytes, final boolean count) throws IOException {
        final long savePos = this.raf.getFilePointer();
        if (filePos >= 0L) {
            this.raf.seek(filePos);
        }
        final byte[] mess = new byte[nbytes];
        this.raf.read(mess);
        printBytes(head, mess, nbytes, false, this.debugOut);
        this.raf.seek(savePos);
    }
    
    public static String showBytes(final byte[] buff) {
        final StringBuilder sbuff = new StringBuilder();
        for (int i = 0; i < buff.length; ++i) {
            final byte b = buff[i];
            final int ub = (b < 0) ? (b + 256) : b;
            if (i > 0) {
                sbuff.append(" ");
            }
            sbuff.append(ub);
        }
        return sbuff.toString();
    }
    
    public static void showBytes(final byte[] buff, final Formatter f) {
        for (int i = 0; i < buff.length; ++i) {
            final byte b = buff[i];
            final int ub = (b < 0) ? (b + 256) : b;
            f.format("%3d ", ub);
        }
    }
    
    static void printBytes(final String head, final byte[] buff, final int n, final boolean count, final PrintStream ps) {
        ps.print(head + " == ");
        for (int i = 0; i < n; ++i) {
            final byte b = buff[i];
            final int ub = (b < 0) ? (b + 256) : b;
            if (count) {
                ps.print(i + ":");
            }
            ps.print(ub);
            if (!count) {
                ps.print("(");
                ps.print(b);
                ps.print(")");
            }
            ps.print(" ");
        }
        ps.println();
    }
    
    static void printBytes(final String head, final byte[] buff, final int offset, final int n, final PrintStream ps) {
        ps.print(head + " == ");
        for (int i = 0; i < n; ++i) {
            final byte b = buff[offset + i];
            final int ub = (b < 0) ? (b + 256) : b;
            ps.print(ub);
            ps.print(" ");
        }
        ps.println();
    }
    
    public void close() {
        if (H5header.debugTracker) {
            this.memTracker.report();
        }
    }
    
    static {
        H5header.log = LoggerFactory.getLogger(H5header.class);
        H5header.utf8CharsetName = "UTF-8";
        H5header.utf8Charset = Charset.forName(H5header.utf8CharsetName);
        H5header.debugEnum = false;
        H5header.debugVlen = false;
        H5header.debug1 = false;
        H5header.debugDetail = false;
        H5header.debugPos = false;
        H5header.debugHeap = false;
        H5header.debugV = false;
        H5header.debugGroupBtree = false;
        H5header.debugDataBtree = false;
        H5header.debugDataChunk = false;
        H5header.debugBtree2 = false;
        H5header.debugContinueMessage = false;
        H5header.debugTracker = false;
        H5header.debugSoftLink = false;
        H5header.debugSymbolTable = false;
        H5header.warnings = false;
        H5header.debugReference = false;
        H5header.debugRegionReference = false;
        H5header.debugCreationOrder = false;
        H5header.debugFractalHeap = false;
        H5header.debugDimensionScales = false;
        head = new byte[] { -119, 72, 68, 70, 13, 10, 26, 10 };
        hdf5magic = new String(H5header.head);
        H5header.transformReference = true;
    }
    
    class Vinfo
    {
        Variable owner;
        DataObjectFacade facade;
        long dataPos;
        TypeInfo typeInfo;
        int[] storageSize;
        boolean isChunked;
        DataBTree btree;
        MessageDatatype mdt;
        MessageDataspace mds;
        MessageFilter mfp;
        boolean useFillValue;
        byte[] fillValue;
        
        Vinfo(final DataObjectFacade facade) throws IOException {
            this.isChunked = false;
            this.btree = null;
            this.useFillValue = false;
            this.facade = facade;
            this.dataPos = H5header.this.getFileOffset(facade.dobj.msl.dataAddress);
            this.mdt = facade.dobj.mdt;
            this.mds = facade.dobj.mds;
            this.mfp = facade.dobj.mfp;
            if (!facade.dobj.mdt.isOK && H5header.warnings) {
                H5header.this.debugOut.println("WARNING HDF5 file " + H5header.this.ncfile.getLocation() + " not handling " + facade.dobj.mdt);
                return;
            }
            this.isChunked = (facade.dobj.msl.type == 2);
            if (this.isChunked) {
                this.storageSize = facade.dobj.msl.chunkSize;
            }
            else {
                this.storageSize = facade.dobj.mds.dimLength;
            }
            this.typeInfo = this.calcNCtype(facade.dobj.mdt);
        }
        
        Vinfo(final MessageDatatype mdt, final MessageDataspace mds, final long dataPos) throws IOException {
            this.isChunked = false;
            this.btree = null;
            this.useFillValue = false;
            this.mdt = mdt;
            this.mds = mds;
            this.dataPos = dataPos;
            if (!mdt.isOK && H5header.warnings) {
                H5header.this.debugOut.println("WARNING HDF5 file " + H5header.this.ncfile.getLocation() + " not handling " + mdt);
                return;
            }
            this.typeInfo = this.calcNCtype(mdt);
        }
        
        void setOwner(final Variable owner) {
            this.owner = owner;
            if (this.btree != null) {
                this.btree.setOwner(owner);
            }
        }
        
        private TypeInfo calcNCtype(final MessageDatatype mdt) {
            final int hdfType = mdt.type;
            final int byteSize = mdt.byteSize;
            final byte[] flags = mdt.flags;
            final TypeInfo tinfo = new TypeInfo(hdfType, byteSize);
            if (hdfType == 0) {
                tinfo.dataType = H5header.this.getNCtype(hdfType, byteSize);
                tinfo.byteOrder = (((flags[0] & 0x1) == 0x0) ? 1 : 0);
                tinfo.unsigned = ((flags[0] & 0x8) == 0x0);
            }
            else if (hdfType == 1) {
                tinfo.dataType = H5header.this.getNCtype(hdfType, byteSize);
                tinfo.byteOrder = (((flags[0] & 0x1) == 0x0) ? 1 : 0);
            }
            else if (hdfType == 2) {
                tinfo.dataType = DataType.STRING;
                tinfo.byteOrder = (((flags[0] & 0x1) == 0x0) ? 1 : 0);
            }
            else if (hdfType == 3) {
                tinfo.dataType = DataType.CHAR;
                tinfo.vpad = (flags[0] & 0xF);
            }
            else if (hdfType == 4) {
                tinfo.dataType = H5header.this.getNCtype(hdfType, byteSize);
            }
            else if (hdfType == 5) {
                tinfo.dataType = DataType.OPAQUE;
            }
            else if (hdfType == 6) {
                tinfo.dataType = DataType.STRUCTURE;
            }
            else if (hdfType == 7) {
                tinfo.byteOrder = 1;
                tinfo.dataType = DataType.LONG;
            }
            else if (hdfType == 8) {
                if (tinfo.byteSize == 1) {
                    tinfo.dataType = DataType.ENUM1;
                }
                else if (tinfo.byteSize == 2) {
                    tinfo.dataType = DataType.ENUM2;
                }
                else {
                    if (tinfo.byteSize != 4) {
                        H5header.log.warn("Illegal byte suze for enum type = " + tinfo.byteSize);
                        throw new IllegalStateException("Illegal byte suze for enum type = " + tinfo.byteSize);
                    }
                    tinfo.dataType = DataType.ENUM4;
                }
            }
            else if (hdfType == 9) {
                tinfo.isVString = mdt.isVString;
                if (mdt.isVString) {
                    tinfo.vpad = (flags[0] >> 4 & 0xF);
                    tinfo.dataType = DataType.STRING;
                }
                else {
                    tinfo.dataType = H5header.this.getNCtype(mdt.getBaseType(), mdt.getBaseSize());
                }
            }
            else if (hdfType == 10) {
                tinfo.byteOrder = (((mdt.getFlags()[0] & 0x1) == 0x0) ? 1 : 0);
                if (mdt.base.type == 9 && mdt.base.isVString) {
                    tinfo.dataType = DataType.STRING;
                }
                else {
                    tinfo.dataType = H5header.this.getNCtype(mdt.getBaseType(), mdt.getBaseSize());
                }
            }
            else if (H5header.warnings) {
                H5header.this.debugOut.println("WARNING not handling hdf dataType = " + hdfType + " size= " + byteSize);
            }
            if (mdt.base != null) {
                tinfo.base = this.calcNCtype(mdt.base);
            }
            return tinfo;
        }
        
        @Override
        public String toString() {
            final StringBuilder buff = new StringBuilder();
            buff.append("dataPos=").append(this.dataPos).append(" datatype=").append(this.typeInfo);
            if (this.isChunked) {
                buff.append(" isChunked (");
                for (final int size : this.storageSize) {
                    buff.append(size).append(" ");
                }
                buff.append(")");
            }
            if (this.mfp != null) {
                buff.append(" hasFilter");
            }
            buff.append("; // ").append(this.extraInfo());
            if (null != this.facade) {
                buff.append("\n").append(this.facade);
            }
            return buff.toString();
        }
        
        public String extraInfo() {
            final StringBuilder buff = new StringBuilder();
            if (this.typeInfo.dataType != DataType.CHAR && this.typeInfo.dataType != DataType.STRING) {
                buff.append(this.typeInfo.unsigned ? " unsigned" : " signed");
            }
            if (this.typeInfo.byteOrder >= 0) {
                buff.append((this.typeInfo.byteOrder == 1) ? " LittleEndian" : " BigEndian");
            }
            if (this.useFillValue) {
                buff.append(" useFillValue");
            }
            return buff.toString();
        }
        
        DataType getNCDataType() {
            return this.typeInfo.dataType;
        }
        
        Object getFillValue() {
            return (this.fillValue == null) ? this.getFillValueDefault(this.typeInfo.dataType) : this.getFillValueNonDefault();
        }
        
        Object getFillValueDefault(final DataType dtype) {
            if (dtype == DataType.BYTE || dtype == DataType.ENUM1) {
                return -127;
            }
            if (dtype == DataType.CHAR) {
                return 0;
            }
            if (dtype == DataType.SHORT || dtype == DataType.ENUM2) {
                return -32767;
            }
            if (dtype == DataType.INT || dtype == DataType.ENUM4) {
                return -2147483647;
            }
            if (dtype == DataType.LONG) {
                return -9223372036854775806L;
            }
            if (dtype == DataType.FLOAT) {
                return 9.96921E36f;
            }
            if (dtype == DataType.DOUBLE) {
                return 9.969209968386869E36;
            }
            return null;
        }
        
        Object getFillValueNonDefault() {
            if (this.fillValue == null) {
                return null;
            }
            if (this.typeInfo.dataType == DataType.BYTE || this.typeInfo.dataType == DataType.CHAR || this.typeInfo.dataType == DataType.ENUM1) {
                return this.fillValue[0];
            }
            final ByteBuffer bbuff = ByteBuffer.wrap(this.fillValue);
            if (this.typeInfo.byteOrder >= 0) {
                bbuff.order((this.typeInfo.byteOrder == 1) ? ByteOrder.LITTLE_ENDIAN : ByteOrder.BIG_ENDIAN);
            }
            if (this.typeInfo.dataType == DataType.SHORT || this.typeInfo.dataType == DataType.ENUM2) {
                final ShortBuffer tbuff = bbuff.asShortBuffer();
                return tbuff.get();
            }
            if (this.typeInfo.dataType == DataType.INT || this.typeInfo.dataType == DataType.ENUM4) {
                final IntBuffer tbuff2 = bbuff.asIntBuffer();
                return tbuff2.get();
            }
            if (this.typeInfo.dataType == DataType.LONG) {
                final LongBuffer tbuff3 = bbuff.asLongBuffer();
                return tbuff3.get();
            }
            if (this.typeInfo.dataType == DataType.FLOAT) {
                final FloatBuffer tbuff4 = bbuff.asFloatBuffer();
                return tbuff4.get();
            }
            if (this.typeInfo.dataType == DataType.DOUBLE) {
                final DoubleBuffer tbuff5 = bbuff.asDoubleBuffer();
                return tbuff5.get();
            }
            return null;
        }
    }
    
    class TypeInfo
    {
        int hdfType;
        int byteSize;
        DataType dataType;
        int byteOrder;
        boolean unsigned;
        boolean isVString;
        int vpad;
        TypeInfo base;
        
        TypeInfo(final int hdfType, final int byteSize) {
            this.byteOrder = -1;
            this.hdfType = hdfType;
            this.byteSize = byteSize;
        }
        
        @Override
        public String toString() {
            final StringBuilder buff = new StringBuilder();
            buff.append("hdfType=").append(this.hdfType).append(" byteSize=").append(this.byteSize).append(" dataType=").append(this.dataType);
            buff.append(" unsigned=").append(this.unsigned).append(" isVString=").append(this.isVString).append(" vpad=").append(this.vpad).append(" byteOrder=").append(this.byteOrder);
            if (this.base != null) {
                buff.append("\n   base=").append(this.base);
            }
            return buff.toString();
        }
    }
    
    private class DataObjectFacade
    {
        H5Group parent;
        String name;
        String displayName;
        DataObject dobj;
        boolean isGroup;
        boolean isVariable;
        boolean isTypedef;
        boolean isDimensionNotVariable;
        H5Group group;
        String dimList;
        List<HeaderMessage> dimMessages;
        String linkName;
        
        DataObjectFacade(final H5Group parent, final String name, final String linkName) {
            this.dimMessages = new ArrayList<HeaderMessage>();
            this.linkName = null;
            this.parent = parent;
            this.name = name;
            this.linkName = linkName;
        }
        
        DataObjectFacade(final H5Group parent, final String name, final long address) throws IOException {
            this.dimMessages = new ArrayList<HeaderMessage>();
            this.linkName = null;
            this.parent = parent;
            this.name = name;
            this.displayName = ((name.length() == 0) ? "root" : name);
            this.dobj = H5header.this.getDataObject(address, this.displayName);
            H5header.this.symlinkMap.put(this.getName(), this);
            if (this.dobj.groupMessage != null || this.dobj.groupNewMessage != null) {
                this.isGroup = true;
            }
            else if (this.dobj.mdt != null && this.dobj.msl != null) {
                this.isVariable = true;
            }
            else if (this.dobj.mdt != null) {
                this.isTypedef = true;
            }
            else if (H5header.warnings) {
                H5header.this.debugOut.println("WARNING Unknown DataObjectFacade = " + this);
            }
        }
        
        String getName() {
            return (this.parent == null) ? this.name : (this.parent.getName() + "/" + this.name);
        }
        
        @Override
        public String toString() {
            final StringBuilder sbuff = new StringBuilder();
            sbuff.append(this.getName());
            if (this.dobj == null) {
                sbuff.append(" dobj is NULL! ");
            }
            else {
                sbuff.append(" id= ").append(this.dobj.address);
                sbuff.append(" messages= ");
                for (final HeaderMessage message : this.dobj.messages) {
                    sbuff.append("\n  ").append(message);
                }
            }
            return sbuff.toString();
        }
    }
    
    private class H5Group
    {
        H5Group parent;
        String name;
        String displayName;
        DataObjectFacade facade;
        List<DataObjectFacade> nestedObjects;
        Map<String, Dimension> dimMap;
        List<Dimension> dimList;
        
        private H5Group(final DataObjectFacade facade) throws IOException {
            this.nestedObjects = new ArrayList<DataObjectFacade>();
            this.dimMap = new HashMap<String, Dimension>();
            this.dimList = new ArrayList<Dimension>();
            this.facade = facade;
            this.parent = facade.parent;
            this.name = facade.name;
            this.displayName = ((this.name.length() == 0) ? "root" : this.name);
            if (facade.dobj.groupMessage != null) {
                H5header.this.readGroupOld(this, facade.dobj.groupMessage.btreeAddress, facade.dobj.groupMessage.nameHeapAddress);
            }
            else {
                if (facade.dobj.groupNewMessage == null) {
                    throw new IllegalStateException("H5Group needs group messages " + facade.getName());
                }
                H5header.this.readGroupNew(this, facade.dobj.groupNewMessage, facade.dobj);
            }
            facade.group = this;
        }
        
        String getName() {
            return (this.parent == null) ? this.name : (this.parent.getName() + "/" + this.name);
        }
        
        boolean isChildOf(final H5Group that) {
            return this.parent != null && (this.parent == that || this.parent.isChildOf(that));
        }
    }
    
    public class DataObject implements Named
    {
        long address;
        String who;
        List<HeaderMessage> messages;
        List<MessageAttribute> attributes;
        MessageGroup groupMessage;
        MessageGroupNew groupNewMessage;
        MessageDatatype mdt;
        MessageDataspace mds;
        MessageLayout msl;
        MessageFilter mfp;
        byte version;
        
        public long getAddress() {
            return this.address;
        }
        
        public String getName() {
            return this.who;
        }
        
        public List<HeaderMessage> getMessages() {
            final List<HeaderMessage> result = new ArrayList<HeaderMessage>(100);
            for (final HeaderMessage m : this.messages) {
                if (!(m.messData instanceof MessageAttribute)) {
                    result.add(m);
                }
            }
            return result;
        }
        
        public List<MessageAttribute> getAttributes() {
            return this.attributes;
        }
        
        private DataObject(final long address, final String who) throws IOException {
            this.messages = new ArrayList<HeaderMessage>();
            this.attributes = new ArrayList<MessageAttribute>();
            this.groupMessage = null;
            this.groupNewMessage = null;
            this.mdt = null;
            this.mds = null;
            this.msl = null;
            this.mfp = null;
            this.address = address;
            this.who = who;
            if (H5header.debug1) {
                H5header.this.debugOut.println("\n--> DataObject.read parsing <" + who + "> object ID/address=" + address);
            }
            if (H5header.debugPos) {
                H5header.this.debugOut.println("      DataObject.read now at position=" + H5header.this.raf.getFilePointer() + " for <" + who + "> reposition to " + H5header.this.getFileOffset(address));
            }
            H5header.this.raf.seek(H5header.this.getFileOffset(address));
            this.version = H5header.this.raf.readByte();
            if (this.version == 1) {
                H5header.this.raf.readByte();
                final short nmess = H5header.this.raf.readShort();
                if (H5header.debugDetail) {
                    H5header.this.debugOut.println(" version=" + this.version + " nmess=" + nmess);
                }
                final int referenceCount = H5header.this.raf.readInt();
                final int headerSize = H5header.this.raf.readInt();
                if (H5header.debugDetail) {
                    H5header.this.debugOut.println(" referenceCount=" + referenceCount + " headerSize=" + headerSize);
                }
                H5header.this.raf.skipBytes(4);
                final long posMess = H5header.this.raf.getFilePointer();
                final int count = this.readMessagesVersion1(posMess, nmess, Integer.MAX_VALUE, this.who);
                if (H5header.debugContinueMessage) {
                    H5header.this.debugOut.println(" nmessages read = " + count);
                }
                if (H5header.debugPos) {
                    H5header.this.debugOut.println("<--done reading messages for <" + who + ">; position=" + H5header.this.raf.getFilePointer());
                }
                if (H5header.debugTracker) {
                    H5header.this.memTracker.addByLen("Object " + who, H5header.this.getFileOffset(address), headerSize + 16);
                }
            }
            else {
                final byte[] name = new byte[3];
                H5header.this.raf.read(name);
                final String magic = new String(name);
                if (!magic.equals("HDR")) {
                    throw new IllegalStateException("DataObject doesnt start with OHDR");
                }
                this.version = H5header.this.raf.readByte();
                final byte flags = H5header.this.raf.readByte();
                if (H5header.debugDetail) {
                    H5header.this.debugOut.println(" version=" + this.version + " flags=" + Integer.toBinaryString(flags));
                }
                if ((flags >> 5 & 0x1) == 0x1) {
                    final int accessTime = H5header.this.raf.readInt();
                    final int modTime = H5header.this.raf.readInt();
                    final int changeTime = H5header.this.raf.readInt();
                    final int birthTime = H5header.this.raf.readInt();
                }
                if ((flags >> 4 & 0x1) == 0x1) {
                    final short maxCompactAttributes = H5header.this.raf.readShort();
                    final short minDenseAttributes = H5header.this.raf.readShort();
                }
                final long sizeOfChunk = H5header.this.readVariableSizeFactor(flags & 0x3);
                if (H5header.debugDetail) {
                    H5header.this.debugOut.println(" sizeOfChunk=" + sizeOfChunk);
                }
                final long posMess2 = H5header.this.raf.getFilePointer();
                final int count2 = this.readMessagesVersion2(posMess2, sizeOfChunk, (flags & 0x4) != 0x0, this.who);
                if (H5header.debugContinueMessage) {
                    H5header.this.debugOut.println(" nmessages read = " + count2);
                }
                if (H5header.debugPos) {
                    H5header.this.debugOut.println("<--done reading messages for <" + name + ">; position=" + H5header.this.raf.getFilePointer());
                }
            }
            for (final HeaderMessage mess : this.messages) {
                if (H5header.debugTracker) {
                    H5header.this.memTracker.addByLen("Message (" + who + ") " + mess.mtype, mess.start, mess.size + 8);
                }
                if (mess.mtype == MessageType.Group) {
                    this.groupMessage = (MessageGroup)mess.messData;
                }
                else if (mess.mtype == MessageType.GroupNew) {
                    this.groupNewMessage = (MessageGroupNew)mess.messData;
                }
                else if (mess.mtype == MessageType.SimpleDataspace) {
                    this.mds = (MessageDataspace)mess.messData;
                }
                else if (mess.mtype == MessageType.Datatype) {
                    this.mdt = (MessageDatatype)mess.messData;
                }
                else if (mess.mtype == MessageType.Layout) {
                    this.msl = (MessageLayout)mess.messData;
                }
                else if (mess.mtype == MessageType.Group) {
                    this.groupMessage = (MessageGroup)mess.messData;
                }
                else if (mess.mtype == MessageType.FilterPipeline) {
                    this.mfp = (MessageFilter)mess.messData;
                }
                else if (mess.mtype == MessageType.Attribute) {
                    this.attributes.add((MessageAttribute)mess.messData);
                }
                else {
                    if (mess.mtype != MessageType.AttributeInfo) {
                        continue;
                    }
                    this.processAttributeInfoMessage((MessageAttributeInfo)mess.messData, this.attributes);
                }
            }
            if (H5header.debug1) {
                H5header.this.debugOut.println("<-- end DataObject " + who);
            }
        }
        
        private void processAttributeInfoMessage(final MessageAttributeInfo attInfo, final List<MessageAttribute> list) throws IOException {
            final long btreeAddress = (attInfo.v2BtreeAddressCreationOrder > 0L) ? attInfo.v2BtreeAddressCreationOrder : attInfo.v2BtreeAddress;
            if (btreeAddress < 0L || attInfo.fractalHeapAddress < 0L) {
                return;
            }
            final BTree2 btree = new BTree2(this.who, btreeAddress);
            final FractalHeap fractalHeap = new FractalHeap(this.who, attInfo.fractalHeapAddress);
            for (final BTree2.Entry2 e : btree.entryList) {
                byte[] heapId = null;
                switch (btree.btreeType) {
                    case 8: {
                        heapId = ((BTree2.Record8)e.record).heapId;
                        break;
                    }
                    case 9: {
                        heapId = ((BTree2.Record9)e.record).heapId;
                        break;
                    }
                    default: {
                        continue;
                    }
                }
                final long pos = fractalHeap.getHeapId(heapId).getPos();
                if (pos > 0L) {
                    H5header.this.raf.seek(pos);
                    final MessageAttribute attMessage = new MessageAttribute();
                    if (attMessage.read()) {
                        list.add(attMessage);
                    }
                    if (!H5header.debugBtree2) {
                        continue;
                    }
                    System.out.println("    attMessage=" + attMessage);
                }
            }
        }
        
        private int readMessagesVersion1(long pos, final int maxMess, final int maxBytes, final String objectName) throws IOException {
            if (H5header.debugContinueMessage) {
                H5header.this.debugOut.println(" readMessages start at =" + pos + " maxMess= " + maxMess + " maxBytes= " + maxBytes);
            }
            int count = 0;
            int bytesRead = 0;
            while (count < maxMess && bytesRead < maxBytes) {
                final HeaderMessage mess = new HeaderMessage();
                final int n = mess.read(pos, 1, false, objectName);
                pos += n;
                bytesRead += n;
                ++count;
                if (H5header.debugContinueMessage) {
                    H5header.this.debugOut.println("   count=" + count + " bytesRead=" + bytesRead);
                }
                if (mess.mtype == MessageType.ObjectHeaderContinuation) {
                    final MessageContinue c = (MessageContinue)mess.messData;
                    if (H5header.debugContinueMessage) {
                        H5header.this.debugOut.println(" ---ObjectHeaderContinuation--- ");
                    }
                    count += this.readMessagesVersion1(H5header.this.getFileOffset(c.offset), maxMess - count, (int)c.length, objectName);
                    if (!H5header.debugContinueMessage) {
                        continue;
                    }
                    H5header.this.debugOut.println(" ---ObjectHeaderContinuation return --- ");
                }
                else {
                    if (mess.mtype == MessageType.NIL) {
                        continue;
                    }
                    this.messages.add(mess);
                }
            }
            return count;
        }
        
        private int readMessagesVersion2(long filePos, long maxBytes, final boolean creationOrderPresent, final String objectName) throws IOException {
            if (H5header.debugContinueMessage) {
                H5header.this.debugOut.println(" readMessages2 starts at =" + filePos + " maxBytes= " + maxBytes);
            }
            maxBytes -= 3L;
            int count = 0;
            int bytesRead = 0;
            while (bytesRead < maxBytes) {
                final HeaderMessage mess = new HeaderMessage();
                final int n = mess.read(filePos, 2, creationOrderPresent, objectName);
                filePos += n;
                bytesRead += n;
                ++count;
                if (H5header.debugContinueMessage) {
                    H5header.this.debugOut.println("   mess size=" + n + " bytesRead=" + bytesRead + " maxBytes=" + maxBytes);
                }
                if (mess.mtype == MessageType.ObjectHeaderContinuation) {
                    final MessageContinue c = (MessageContinue)mess.messData;
                    final long continuationBlockFilePos = H5header.this.getFileOffset(c.offset);
                    if (H5header.debugContinueMessage) {
                        H5header.this.debugOut.println(" ---ObjectHeaderContinuation filePos= " + continuationBlockFilePos);
                    }
                    H5header.this.raf.seek(continuationBlockFilePos);
                    final String sig = H5header.this.readStringFixedLength(4);
                    if (!sig.equals("OCHK")) {
                        throw new IllegalStateException(" ObjectHeaderContinuation Missing signature");
                    }
                    count += this.readMessagesVersion2(continuationBlockFilePos + 4L, (int)c.length - 8, creationOrderPresent, objectName);
                    if (H5header.debugContinueMessage) {
                        H5header.this.debugOut.println(" ---ObjectHeaderContinuation return --- ");
                    }
                    if (!H5header.debugContinueMessage) {
                        continue;
                    }
                    H5header.this.debugOut.println("   continuationMessages =" + count + " bytesRead=" + bytesRead + " maxBytes=" + maxBytes);
                }
                else {
                    if (mess.mtype == MessageType.NIL) {
                        continue;
                    }
                    this.messages.add(mess);
                }
            }
            return count;
        }
    }
    
    public static class MessageType
    {
        private static int MAX_MESSAGE;
        private static Map<String, MessageType> hash;
        private static MessageType[] mess;
        public static final MessageType NIL;
        public static final MessageType SimpleDataspace;
        public static final MessageType GroupNew;
        public static final MessageType Datatype;
        public static final MessageType FillValueOld;
        public static final MessageType FillValue;
        public static final MessageType Link;
        public static final MessageType ExternalDataFiles;
        public static final MessageType Layout;
        public static final MessageType GroupInfo;
        public static final MessageType FilterPipeline;
        public static final MessageType Attribute;
        public static final MessageType Comment;
        public static final MessageType LastModifiedOld;
        public static final MessageType SharedObject;
        public static final MessageType ObjectHeaderContinuation;
        public static final MessageType Group;
        public static final MessageType LastModified;
        public static final MessageType AttributeInfo;
        public static final MessageType ObjectReferenceCount;
        private String name;
        private int num;
        
        private MessageType(final String name, final int num) {
            this.name = name;
            this.num = num;
            MessageType.hash.put(name, this);
            MessageType.mess[num] = this;
        }
        
        public static MessageType getType(final String name) {
            if (name == null) {
                return null;
            }
            return MessageType.hash.get(name);
        }
        
        public static MessageType getType(final int num) {
            if (num < 0 || num >= MessageType.MAX_MESSAGE) {
                return null;
            }
            return MessageType.mess[num];
        }
        
        @Override
        public String toString() {
            return this.name + "(" + this.num + ")";
        }
        
        public int getNum() {
            return this.num;
        }
        
        static {
            MessageType.MAX_MESSAGE = 23;
            MessageType.hash = new HashMap<String, MessageType>(10);
            MessageType.mess = new MessageType[MessageType.MAX_MESSAGE];
            NIL = new MessageType("NIL", 0);
            SimpleDataspace = new MessageType("SimpleDataspace", 1);
            GroupNew = new MessageType("GroupNew", 2);
            Datatype = new MessageType("Datatype", 3);
            FillValueOld = new MessageType("FillValueOld", 4);
            FillValue = new MessageType("FillValue", 5);
            Link = new MessageType("Link", 6);
            ExternalDataFiles = new MessageType("ExternalDataFiles", 7);
            Layout = new MessageType("Layout", 8);
            GroupInfo = new MessageType("GroupInfo", 10);
            FilterPipeline = new MessageType("FilterPipeline", 11);
            Attribute = new MessageType("Attribute", 12);
            Comment = new MessageType("Comment", 13);
            LastModifiedOld = new MessageType("LastModifiedOld", 14);
            SharedObject = new MessageType("SharedObject", 15);
            ObjectHeaderContinuation = new MessageType("ObjectHeaderContinuation", 16);
            Group = new MessageType("Group", 17);
            LastModified = new MessageType("LastModified", 18);
            AttributeInfo = new MessageType("AttributeInfo", 21);
            ObjectReferenceCount = new MessageType("ObjectReferenceCount", 22);
        }
    }
    
    public class HeaderMessage implements Comparable
    {
        long start;
        byte headerMessageFlags;
        short type;
        short size;
        short header_length;
        Named messData;
        MessageType mtype;
        short creationOrder;
        
        public HeaderMessage() {
            this.creationOrder = -1;
        }
        
        public MessageType getMtype() {
            return this.mtype;
        }
        
        public String getName() {
            return this.messData.getName();
        }
        
        public short getSize() {
            return this.size;
        }
        
        public short getType() {
            return this.type;
        }
        
        public byte getFlags() {
            return this.headerMessageFlags;
        }
        
        public long getStart() {
            return this.start;
        }
        
        int read(final long filePos, final int version, final boolean creationOrderPresent, final String objectName) throws IOException {
            this.start = filePos;
            H5header.this.raf.seek(filePos);
            if (H5header.debugPos) {
                H5header.this.debugOut.println("  --> Message Header starts at =" + H5header.this.raf.getFilePointer());
            }
            if (version == 1) {
                this.type = H5header.this.raf.readShort();
                this.size = H5header.this.raf.readShort();
                this.headerMessageFlags = H5header.this.raf.readByte();
                H5header.this.raf.skipBytes(3);
                this.header_length = 8;
            }
            else {
                this.type = H5header.this.raf.readByte();
                this.size = H5header.this.raf.readShort();
                this.headerMessageFlags = H5header.this.raf.readByte();
                this.header_length = 4;
                if (creationOrderPresent) {
                    this.creationOrder = H5header.this.raf.readShort();
                    this.header_length += 2;
                }
            }
            this.mtype = MessageType.getType(this.type);
            if (H5header.debug1) {
                H5header.this.debugOut.println("  -->" + this.mtype + " messageSize=" + this.size + " flags = " + Integer.toBinaryString(this.headerMessageFlags));
                if (creationOrderPresent && H5header.debugCreationOrder) {
                    H5header.this.debugOut.println("     creationOrder = " + this.creationOrder);
                }
            }
            if (H5header.debugPos) {
                H5header.this.debugOut.println("  --> Message Data starts at=" + H5header.this.raf.getFilePointer());
            }
            if ((this.headerMessageFlags & 0x2) != 0x0) {
                this.messData = H5header.this.getSharedDataObject(this.mtype).mdt;
                return this.header_length + this.size;
            }
            if (this.mtype != MessageType.NIL) {
                if (this.mtype == MessageType.SimpleDataspace) {
                    final MessageDataspace data = new MessageDataspace();
                    data.read();
                    this.messData = data;
                }
                else if (this.mtype == MessageType.GroupNew) {
                    final MessageGroupNew data2 = new MessageGroupNew();
                    data2.read();
                    this.messData = data2;
                }
                else if (this.mtype == MessageType.Datatype) {
                    final MessageDatatype data3 = new MessageDatatype();
                    data3.read(objectName);
                    this.messData = data3;
                }
                else if (this.mtype == MessageType.FillValueOld) {
                    final MessageFillValueOld data4 = new MessageFillValueOld();
                    data4.read();
                    this.messData = data4;
                }
                else if (this.mtype == MessageType.FillValue) {
                    final MessageFillValue data5 = new MessageFillValue();
                    data5.read();
                    this.messData = data5;
                }
                else if (this.mtype == MessageType.Link) {
                    final MessageLink data6 = new MessageLink();
                    data6.read();
                    this.messData = data6;
                }
                else if (this.mtype == MessageType.Layout) {
                    final MessageLayout data7 = new MessageLayout();
                    data7.read();
                    this.messData = data7;
                }
                else if (this.mtype == MessageType.GroupInfo) {
                    final MessageGroupInfo data8 = new MessageGroupInfo();
                    data8.read();
                    this.messData = data8;
                }
                else if (this.mtype == MessageType.FilterPipeline) {
                    final MessageFilter data9 = new MessageFilter();
                    data9.read();
                    this.messData = data9;
                }
                else if (this.mtype == MessageType.Attribute) {
                    final MessageAttribute data10 = new MessageAttribute();
                    data10.read();
                    this.messData = data10;
                }
                else if (this.mtype == MessageType.Comment) {
                    final MessageComment data11 = new MessageComment();
                    data11.read();
                    this.messData = data11;
                }
                else if (this.mtype == MessageType.LastModifiedOld) {
                    final MessageLastModifiedOld data12 = new MessageLastModifiedOld();
                    data12.read();
                    this.messData = data12;
                }
                else if (this.mtype == MessageType.ObjectHeaderContinuation) {
                    final MessageContinue data13 = new MessageContinue();
                    data13.read();
                    this.messData = data13;
                }
                else if (this.mtype == MessageType.Group) {
                    final MessageGroup data14 = new MessageGroup();
                    data14.read();
                    this.messData = data14;
                }
                else if (this.mtype == MessageType.LastModified) {
                    final MessageLastModified data15 = new MessageLastModified();
                    data15.read();
                    this.messData = data15;
                }
                else if (this.mtype == MessageType.AttributeInfo) {
                    final MessageAttributeInfo data16 = new MessageAttributeInfo();
                    data16.read();
                    this.messData = data16;
                }
                else {
                    if (this.mtype != MessageType.ObjectReferenceCount) {
                        H5header.this.debugOut.println("****UNPROCESSED MESSAGE type = " + this.mtype + " raw = " + this.type);
                        throw new UnsupportedOperationException("****UNPROCESSED MESSAGE type = " + this.mtype + " raw = " + this.type);
                    }
                    final MessageObjectReferenceCount data17 = new MessageObjectReferenceCount();
                    data17.read();
                    this.messData = data17;
                }
            }
            return this.header_length + this.size;
        }
        
        public int compareTo(final Object o) {
            return this.type - ((HeaderMessage)o).type;
        }
        
        @Override
        public String toString() {
            return "  type = " + this.mtype + "; " + this.messData;
        }
        
        public void showFractalHeap(final Formatter f) {
            if (this.mtype != MessageType.AttributeInfo) {
                f.format("No fractal heap", new Object[0]);
                return;
            }
            final MessageAttributeInfo info = (MessageAttributeInfo)this.messData;
            info.showFractalHeap(f);
        }
    }
    
    public class MessageDataspace implements Named
    {
        byte ndims;
        byte flags;
        byte type;
        int[] dimLength;
        int[] maxLength;
        
        public String getName() {
            final StringBuilder sbuff = new StringBuilder();
            sbuff.append("(");
            for (final int size : this.dimLength) {
                sbuff.append(size).append(",");
            }
            sbuff.append(")");
            return sbuff.toString();
        }
        
        @Override
        public String toString() {
            final Formatter sbuff = new Formatter();
            sbuff.format(" ndims=%d flags=%x type=%d ", this.ndims, this.flags, this.type);
            sbuff.format(" length=(", new Object[0]);
            for (final int size : this.dimLength) {
                sbuff.format("%d,", size);
            }
            sbuff.format(") max=(", new Object[0]);
            for (final int aMaxLength : this.maxLength) {
                sbuff.format("%d,", aMaxLength);
            }
            sbuff.format(")", new Object[0]);
            return sbuff.toString();
        }
        
        void read() throws IOException {
            if (H5header.debugPos) {
                H5header.this.debugOut.println("   *MessageSimpleDataspace start pos= " + H5header.this.raf.getFilePointer());
            }
            final byte version = H5header.this.raf.readByte();
            if (version == 1) {
                this.ndims = H5header.this.raf.readByte();
                this.flags = H5header.this.raf.readByte();
                this.type = (byte)((this.ndims != 0) ? 1 : 0);
                H5header.this.raf.skipBytes(5);
            }
            else {
                if (version != 2) {
                    throw new IllegalStateException("MessageDataspace: unknown version= " + version);
                }
                this.ndims = H5header.this.raf.readByte();
                this.flags = H5header.this.raf.readByte();
                this.type = H5header.this.raf.readByte();
            }
            if (H5header.debug1) {
                H5header.this.debugOut.println("   SimpleDataspace version= " + version + " flags=" + Integer.toBinaryString(this.flags) + " ndims=" + this.ndims + " type=" + this.type);
            }
            this.dimLength = new int[this.ndims];
            for (int i = 0; i < this.ndims; ++i) {
                this.dimLength[i] = (int)H5header.this.readLength();
            }
            final boolean hasMax = (this.flags & 0x1) != 0x0;
            this.maxLength = new int[this.ndims];
            if (hasMax) {
                for (int j = 0; j < this.ndims; ++j) {
                    this.maxLength[j] = (int)H5header.this.readLength();
                }
            }
            else {
                System.arraycopy(this.dimLength, 0, this.maxLength, 0, this.ndims);
            }
            if (H5header.debug1) {
                for (int j = 0; j < this.ndims; ++j) {
                    H5header.this.debugOut.println("    dim length = " + this.dimLength[j] + " max = " + this.maxLength[j]);
                }
            }
        }
    }
    
    private class MessageGroup implements Named
    {
        long btreeAddress;
        long nameHeapAddress;
        
        void read() throws IOException {
            this.btreeAddress = H5header.this.readOffset();
            this.nameHeapAddress = H5header.this.readOffset();
            if (H5header.debug1) {
                H5header.this.debugOut.println("   Group btreeAddress=" + this.btreeAddress + " nameHeapAddress=" + this.nameHeapAddress);
            }
        }
        
        @Override
        public String toString() {
            final StringBuilder sbuff = new StringBuilder();
            sbuff.append(" btreeAddress=").append(this.btreeAddress);
            sbuff.append(" nameHeapAddress=").append(this.nameHeapAddress);
            return sbuff.toString();
        }
        
        public String getName() {
            return Long.toString(this.btreeAddress);
        }
    }
    
    private class MessageGroupNew implements Named
    {
        byte version;
        byte flags;
        long maxCreationIndex;
        long fractalHeapAddress;
        long v2BtreeAddress;
        long v2BtreeAddressCreationOrder;
        
        private MessageGroupNew() {
            this.maxCreationIndex = -2L;
            this.v2BtreeAddressCreationOrder = -2L;
        }
        
        @Override
        public String toString() {
            final Formatter f = new Formatter();
            f.format("   GroupNew fractalHeapAddress=%d v2BtreeAddress=%d ", this.fractalHeapAddress, this.v2BtreeAddress);
            if (this.v2BtreeAddressCreationOrder > -2L) {
                f.format(" v2BtreeAddressCreationOrder=%d ", this.v2BtreeAddressCreationOrder);
            }
            if (this.maxCreationIndex > -2L) {
                f.format(" maxCreationIndex=%d", this.maxCreationIndex);
            }
            f.format(" %n%n", new Object[0]);
            if (this.fractalHeapAddress > 0L) {
                try {
                    f.format("\n\n", new Object[0]);
                    final FractalHeap fractalHeap = new FractalHeap("", this.fractalHeapAddress);
                    fractalHeap.showDetails(f);
                }
                catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return f.toString();
        }
        
        void read() throws IOException {
            if (H5header.debugPos) {
                H5header.this.debugOut.println("   *MessageGroupNew start pos= " + H5header.this.raf.getFilePointer());
            }
            final byte version = H5header.this.raf.readByte();
            final byte flags = H5header.this.raf.readByte();
            if ((flags & 0x1) != 0x0) {
                this.maxCreationIndex = H5header.this.raf.readLong();
            }
            this.fractalHeapAddress = H5header.this.readOffset();
            this.v2BtreeAddress = H5header.this.readOffset();
            if ((flags & 0x2) != 0x0) {
                this.v2BtreeAddressCreationOrder = H5header.this.readOffset();
            }
            if (H5header.debug1) {
                H5header.this.debugOut.println("   MessageGroupNew version= " + version + " flags = " + flags + this);
            }
        }
        
        public String getName() {
            return Long.toString(this.fractalHeapAddress);
        }
    }
    
    private class MessageGroupInfo implements Named
    {
        byte flags;
        short maxCompactValue;
        short minDenseValue;
        short estNumEntries;
        short estLengthEntryName;
        
        private MessageGroupInfo() {
            this.maxCompactValue = -1;
            this.minDenseValue = -1;
            this.estNumEntries = -1;
            this.estLengthEntryName = -1;
        }
        
        @Override
        public String toString() {
            final StringBuilder sbuff = new StringBuilder();
            sbuff.append("   MessageGroupInfo ");
            if ((this.flags & 0x1) != 0x0) {
                sbuff.append(" maxCompactValue=").append(this.maxCompactValue).append(" minDenseValue=").append(this.minDenseValue);
            }
            if ((this.flags & 0x2) != 0x0) {
                sbuff.append(" estNumEntries=").append(this.estNumEntries).append(" estLengthEntryName=").append(this.estLengthEntryName);
            }
            return sbuff.toString();
        }
        
        void read() throws IOException {
            if (H5header.debugPos) {
                H5header.this.debugOut.println("   *MessageGroupInfo start pos= " + H5header.this.raf.getFilePointer());
            }
            final byte version = H5header.this.raf.readByte();
            this.flags = H5header.this.raf.readByte();
            if ((this.flags & 0x1) != 0x0) {
                this.maxCompactValue = H5header.this.raf.readShort();
                this.minDenseValue = H5header.this.raf.readShort();
            }
            if ((this.flags & 0x2) != 0x0) {
                this.estNumEntries = H5header.this.raf.readShort();
                this.estLengthEntryName = H5header.this.raf.readShort();
            }
            if (H5header.debug1) {
                H5header.this.debugOut.println("   MessageGroupInfo version= " + version + " flags = " + this.flags + this);
            }
        }
        
        public String getName() {
            return "";
        }
    }
    
    private class MessageLink implements Named
    {
        byte version;
        byte flags;
        byte encoding;
        byte linkType;
        long creationOrder;
        String linkName;
        String link;
        long linkAddress;
        
        @Override
        public String toString() {
            final StringBuilder sbuff = new StringBuilder();
            sbuff.append("   MessageLink ");
            sbuff.append(" name=").append(this.linkName).append(" type=").append(this.linkType);
            if (this.linkType == 0) {
                sbuff.append(" linkAddress=" + this.linkAddress);
            }
            else {
                sbuff.append(" link=").append(this.link);
            }
            if ((this.flags & 0x4) != 0x0) {
                sbuff.append(" creationOrder=" + this.creationOrder);
            }
            if ((this.flags & 0x10) != 0x0) {
                sbuff.append(" encoding=" + this.encoding);
            }
            return sbuff.toString();
        }
        
        void read() throws IOException {
            if (H5header.debugPos) {
                H5header.this.debugOut.println("   *MessageLink start pos= " + H5header.this.raf.getFilePointer());
            }
            this.version = H5header.this.raf.readByte();
            this.flags = H5header.this.raf.readByte();
            if ((this.flags & 0x8) != 0x0) {
                this.linkType = H5header.this.raf.readByte();
            }
            if ((this.flags & 0x4) != 0x0) {
                this.creationOrder = H5header.this.raf.readLong();
            }
            if ((this.flags & 0x10) != 0x0) {
                this.encoding = H5header.this.raf.readByte();
            }
            final int linkNameLength = (int)H5header.this.readVariableSizeFactor(this.flags & 0x3);
            this.linkName = H5header.this.readStringFixedLength(linkNameLength);
            if (this.linkType == 0) {
                this.linkAddress = H5header.this.readOffset();
            }
            else if (this.linkType == 1) {
                final short len = H5header.this.raf.readShort();
                this.link = H5header.this.readStringFixedLength(len);
            }
            else if (this.linkType == 64) {
                final short len = H5header.this.raf.readShort();
                this.link = H5header.this.readStringFixedLength(len);
            }
            if (H5header.debug1) {
                H5header.this.debugOut.println("   MessageLink version= " + this.version + " flags = " + Integer.toBinaryString(this.flags) + this);
            }
        }
        
        public String getName() {
            return this.linkName;
        }
    }
    
    public class MessageDatatype implements Named
    {
        int type;
        int version;
        byte[] flags;
        int byteSize;
        int byteOrder;
        boolean isOK;
        boolean unsigned;
        DataType timeType;
        String opaque_desc;
        short nmembers;
        List<StructureMember> members;
        int referenceType;
        Map<Integer, String> map;
        String enumTypeName;
        MessageDatatype base;
        boolean isVString;
        int[] dim;
        
        public MessageDatatype() {
            this.flags = new byte[3];
            this.isOK = true;
        }
        
        @Override
        public String toString() {
            final StringBuilder sbuff = new StringBuilder();
            sbuff.append(" datatype= ").append(this.type);
            sbuff.append(" byteSize= ").append(this.byteSize);
            final DataType dtype = H5header.this.getNCtype(this.type, this.byteSize);
            sbuff.append(" NCtype= ").append(dtype);
            sbuff.append(" flags= ");
            for (int i = 0; i < 3; ++i) {
                sbuff.append(this.flags[i]).append(" ");
            }
            if (this.type == 2) {
                sbuff.append(" timeType= ").append(this.timeType);
            }
            else if (this.type == 6) {
                sbuff.append(" nmembers= ").append(this.nmembers);
            }
            else if (this.type == 7) {
                sbuff.append(" referenceType= ").append(this.referenceType);
            }
            else if (this.type == 9) {
                sbuff.append(" isVString= ").append(this.isVString);
            }
            if (this.type == 9 || this.type == 10) {
                sbuff.append(" parent= ").append(this.base);
            }
            return sbuff.toString();
        }
        
        public String getName() {
            final DataType dtype = H5header.this.getNCtype(this.type, this.byteSize);
            if (dtype != null) {
                return dtype.toString() + " size= " + this.byteSize;
            }
            return "type=" + Integer.toString(this.type) + " size= " + this.byteSize;
        }
        
        void read(final String objectName) throws IOException {
            if (H5header.debugPos) {
                H5header.this.debugOut.println("   *MessageDatatype start pos= " + H5header.this.raf.getFilePointer());
            }
            final byte tandv = H5header.this.raf.readByte();
            this.type = (tandv & 0xF);
            this.version = (tandv & 0xF0) >> 4;
            H5header.this.raf.read(this.flags);
            this.byteSize = H5header.this.raf.readInt();
            this.byteOrder = (((this.flags[0] & 0x1) == 0x0) ? 1 : 0);
            if (H5header.debug1) {
                H5header.this.debugOut.println("   Datatype type=" + this.type + " version= " + this.version + " flags = " + this.flags[0] + " " + this.flags[1] + " " + this.flags[2] + " byteSize=" + this.byteSize + " byteOrder=" + ((this.byteOrder == 0) ? "BIG" : "LITTLE"));
            }
            if (this.type == 0) {
                this.unsigned = ((this.flags[0] & 0x8) == 0x0);
                final short bitOffset = H5header.this.raf.readShort();
                final short bitPrecision = H5header.this.raf.readShort();
                if (H5header.debug1) {
                    H5header.this.debugOut.println("   type 0 (fixed point): bitOffset= " + bitOffset + " bitPrecision= " + bitPrecision + " unsigned= " + this.unsigned);
                }
                this.isOK = (bitOffset == 0 && bitPrecision % 8 == 0);
            }
            else if (this.type == 1) {
                final short bitOffset = H5header.this.raf.readShort();
                final short bitPrecision = H5header.this.raf.readShort();
                final byte expLocation = H5header.this.raf.readByte();
                final byte expSize = H5header.this.raf.readByte();
                final byte manLocation = H5header.this.raf.readByte();
                final byte manSize = H5header.this.raf.readByte();
                final int expBias = H5header.this.raf.readInt();
                if (H5header.debug1) {
                    H5header.this.debugOut.println("   type 1 (floating point): bitOffset= " + bitOffset + " bitPrecision= " + bitPrecision + " expLocation= " + expLocation + " expSize= " + expSize + " manLocation= " + manLocation + " manSize= " + manSize + " expBias= " + expBias);
                }
            }
            else if (this.type == 2) {
                final short bitPrecision2 = H5header.this.raf.readShort();
                if (bitPrecision2 == 16) {
                    this.timeType = DataType.SHORT;
                }
                else if (bitPrecision2 == 32) {
                    this.timeType = DataType.INT;
                }
                else if (bitPrecision2 == 64) {
                    this.timeType = DataType.LONG;
                }
                if (H5header.debug1) {
                    H5header.this.debugOut.println("   type 2 (time): bitPrecision= " + bitPrecision2 + " timeType = " + this.timeType);
                }
            }
            else if (this.type == 3) {
                final int ptype = this.flags[0] & 0xF;
                if (H5header.debug1) {
                    H5header.this.debugOut.println("   type 3 (String): pad type= " + ptype);
                }
            }
            else if (this.type == 4) {
                final short bitOffset = H5header.this.raf.readShort();
                final short bitPrecision = H5header.this.raf.readShort();
                if (H5header.debug1) {
                    H5header.this.debugOut.println("   type 4 (bit field): bitOffset= " + bitOffset + " bitPrecision= " + bitPrecision);
                }
            }
            else if (this.type == 5) {
                final byte len = this.flags[0];
                this.opaque_desc = ((len > 0) ? H5header.this.readString(H5header.this.raf).trim() : null);
                if (H5header.debug1) {
                    H5header.this.debugOut.println("   type 5 (opaque): len= " + len + " desc= " + this.opaque_desc);
                }
            }
            else if (this.type == 6) {
                final int nmembers = H5header.this.makeUnsignedIntFromBytes(this.flags[1], this.flags[0]);
                if (H5header.debug1) {
                    H5header.this.debugOut.println("   --type 6(compound): nmembers=" + nmembers);
                }
                this.members = new ArrayList<StructureMember>();
                for (int i = 0; i < nmembers; ++i) {
                    this.members.add(new StructureMember(this.version, this.byteSize));
                }
                if (H5header.debugDetail) {
                    H5header.this.debugOut.println("   --done with compound type");
                }
            }
            else if (this.type == 7) {
                this.referenceType = (this.flags[0] & 0xF);
                if (H5header.debug1 || H5header.debugReference) {
                    H5header.this.debugOut.println("   --type 7(reference): type= " + this.referenceType);
                }
            }
            else if (this.type == 8) {
                final int nmembers = H5header.this.makeUnsignedIntFromBytes(this.flags[1], this.flags[0]);
                final boolean saveDebugDetail = H5header.debugDetail;
                if (H5header.debug1 || H5header.debugEnum) {
                    H5header.this.debugOut.println("   --type 8(enums): nmembers=" + nmembers);
                    H5header.debugDetail = true;
                }
                (this.base = new MessageDatatype()).read(objectName);
                H5header.debugDetail = saveDebugDetail;
                final String[] enumName = new String[nmembers];
                for (int j = 0; j < nmembers; ++j) {
                    if (this.version < 3) {
                        enumName[j] = H5header.this.readString8(H5header.this.raf);
                    }
                    else {
                        enumName[j] = H5header.this.readString(H5header.this.raf);
                    }
                }
                if (this.base.byteOrder >= 0) {
                    H5header.this.raf.order(this.base.byteOrder);
                }
                final int[] enumValue = new int[nmembers];
                for (int k = 0; k < nmembers; ++k) {
                    enumValue[k] = H5header.this.readVariableSize(this.base.byteSize);
                }
                H5header.this.raf.order(1);
                this.enumTypeName = objectName;
                this.map = new TreeMap<Integer, String>();
                for (int k = 0; k < nmembers; ++k) {
                    this.map.put(enumValue[k], enumName[k]);
                }
                if (H5header.debugEnum) {
                    for (int k = 0; k < nmembers; ++k) {
                        H5header.this.debugOut.println("   " + enumValue[k] + "=" + enumName[k]);
                    }
                }
            }
            else if (this.type == 9) {
                this.isVString = ((this.flags[0] & 0xF) == 0x1);
                if (H5header.debug1) {
                    H5header.this.debugOut.println("   type 9(variable length): type= " + (this.isVString ? "string" : "sequence of type:"));
                }
                (this.base = new MessageDatatype()).read(objectName);
            }
            else if (this.type == 10) {
                if (H5header.debug1) {
                    H5header.this.debugOut.print("   type 10(array) lengths= ");
                }
                final int ndims = H5header.this.raf.readByte();
                if (this.version < 3) {
                    H5header.this.raf.skipBytes(3);
                }
                this.dim = new int[ndims];
                for (int i = 0; i < ndims; ++i) {
                    this.dim[i] = H5header.this.raf.readInt();
                    if (H5header.debug1) {
                        H5header.this.debugOut.print(" " + this.dim[i]);
                    }
                }
                if (this.version < 3) {
                    final int[] pdim = new int[ndims];
                    for (int l = 0; l < ndims; ++l) {
                        pdim[l] = H5header.this.raf.readInt();
                    }
                }
                if (H5header.debug1) {
                    H5header.this.debugOut.println();
                }
                (this.base = new MessageDatatype()).read(objectName);
            }
            else if (H5header.warnings) {
                H5header.this.debugOut.println(" WARNING not dealing with type= " + this.type);
            }
        }
        
        int getBaseType() {
            return (this.base != null) ? this.base.getBaseType() : this.type;
        }
        
        int getBaseSize() {
            return (this.base != null) ? this.base.getBaseSize() : this.byteSize;
        }
        
        byte[] getFlags() {
            return (this.base != null) ? this.base.getFlags() : this.flags;
        }
    }
    
    private class StructureMember
    {
        String name;
        int offset;
        byte dims;
        MessageDatatype mdt;
        
        StructureMember(final int version, final int byteSize) throws IOException {
            if (H5header.debugPos) {
                H5header.this.debugOut.println("   *StructureMember now at position=" + H5header.this.raf.getFilePointer());
            }
            this.name = H5header.this.readString(H5header.this.raf);
            if (version < 3) {
                H5header.this.raf.skipBytes(H5header.this.padding(this.name.length() + 1, 8));
                this.offset = H5header.this.raf.readInt();
            }
            else {
                this.offset = (int)H5header.this.readVariableSizeMax(byteSize);
            }
            if (H5header.debug1) {
                H5header.this.debugOut.println("   Member name=" + this.name + " offset= " + this.offset);
            }
            if (version == 1) {
                this.dims = H5header.this.raf.readByte();
                H5header.this.raf.skipBytes(3);
                H5header.this.raf.skipBytes(24);
            }
            (this.mdt = new MessageDatatype()).read(this.name);
            if (H5header.debugDetail) {
                H5header.this.debugOut.println("   ***End Member name=" + this.name);
            }
        }
    }
    
    private class MessageFillValueOld implements Named
    {
        byte[] value;
        int size;
        
        void read() throws IOException {
            this.size = H5header.this.raf.readInt();
            this.value = new byte[this.size];
            H5header.this.raf.read(this.value);
            if (H5header.debug1) {
                H5header.this.debugOut.println(this);
            }
        }
        
        @Override
        public String toString() {
            final StringBuilder sbuff = new StringBuilder();
            sbuff.append("   FillValueOld size= ").append(this.size).append(" value=");
            for (int i = 0; i < this.size; ++i) {
                sbuff.append(" ").append(this.value[i]);
            }
            return sbuff.toString();
        }
        
        public String getName() {
            final StringBuilder sbuff = new StringBuilder();
            for (int i = 0; i < this.size; ++i) {
                sbuff.append(" ").append(this.value[i]);
            }
            return sbuff.toString();
        }
    }
    
    private class MessageFillValue implements Named
    {
        byte version;
        byte spaceAllocateTime;
        byte fillWriteTime;
        int size;
        byte[] value;
        boolean hasFillValue;
        byte flags;
        
        private MessageFillValue() {
            this.hasFillValue = false;
        }
        
        void read() throws IOException {
            this.version = H5header.this.raf.readByte();
            if (this.version < 3) {
                this.spaceAllocateTime = H5header.this.raf.readByte();
                this.fillWriteTime = H5header.this.raf.readByte();
                this.hasFillValue = (H5header.this.raf.readByte() != 0);
            }
            else {
                this.flags = H5header.this.raf.readByte();
                this.spaceAllocateTime = (byte)(this.flags & 0x3);
                this.fillWriteTime = (byte)(this.flags >> 2 & 0x3);
                this.hasFillValue = ((this.flags & 0x20) != 0x0);
            }
            if (this.hasFillValue) {
                this.size = H5header.this.raf.readInt();
                if (this.size > 0) {
                    this.value = new byte[this.size];
                    H5header.this.raf.read(this.value);
                    this.hasFillValue = true;
                }
                else {
                    this.hasFillValue = false;
                }
            }
            if (H5header.debug1) {
                H5header.this.debugOut.println(this);
            }
        }
        
        @Override
        public String toString() {
            final StringBuilder sbuff = new StringBuilder();
            sbuff.append("   FillValue version= ").append(this.version).append(" spaceAllocateTime = ").append(this.spaceAllocateTime).append(" fillWriteTime=").append(this.fillWriteTime).append(" hasFillValue= ").append(this.hasFillValue);
            sbuff.append("\n size = ").append(this.size).append(" value=");
            for (int i = 0; i < this.size; ++i) {
                sbuff.append(" ").append(this.value[i]);
            }
            return sbuff.toString();
        }
        
        public String getName() {
            final StringBuilder sbuff = new StringBuilder();
            for (int i = 0; i < this.size; ++i) {
                sbuff.append(" ").append(this.value[i]);
            }
            return sbuff.toString();
        }
    }
    
    private class MessageLayout implements Named
    {
        byte type;
        long dataAddress;
        long contiguousSize;
        int[] chunkSize;
        int dataSize;
        
        private MessageLayout() {
            this.dataAddress = -1L;
        }
        
        @Override
        public String toString() {
            final StringBuilder sbuff = new StringBuilder();
            sbuff.append(" type= ").append(this.type).append(" (");
            switch (this.type) {
                case 0: {
                    sbuff.append("compact");
                    break;
                }
                case 1: {
                    sbuff.append("contiguous");
                    break;
                }
                case 2: {
                    sbuff.append("chunked");
                    break;
                }
                default: {
                    sbuff.append("unknown type= ").append(this.type);
                    break;
                }
            }
            sbuff.append(")");
            if (this.chunkSize != null) {
                sbuff.append(" storageSize = (");
                for (int i = 0; i < this.chunkSize.length; ++i) {
                    if (i > 0) {
                        sbuff.append(",");
                    }
                    sbuff.append(this.chunkSize[i]);
                }
                sbuff.append(")");
            }
            sbuff.append(" dataSize=").append(this.dataSize);
            sbuff.append(" dataAddress=").append(this.dataAddress);
            return sbuff.toString();
        }
        
        public String getName() {
            final StringBuilder sbuff = new StringBuilder();
            switch (this.type) {
                case 0: {
                    sbuff.append("compact");
                    break;
                }
                case 1: {
                    sbuff.append("contiguous");
                    break;
                }
                case 2: {
                    sbuff.append("chunked");
                    break;
                }
                default: {
                    sbuff.append("unknown type= ").append(this.type);
                    break;
                }
            }
            if (this.chunkSize != null) {
                sbuff.append(" chunk = (");
                for (int i = 0; i < this.chunkSize.length; ++i) {
                    if (i > 0) {
                        sbuff.append(",");
                    }
                    sbuff.append(this.chunkSize[i]);
                }
                sbuff.append(")");
            }
            return sbuff.toString();
        }
        
        void read() throws IOException {
            final byte version = H5header.this.raf.readByte();
            if (version < 3) {
                final int ndims = H5header.this.raf.readByte();
                this.type = H5header.this.raf.readByte();
                H5header.this.raf.skipBytes(5);
                final boolean isCompact = this.type == 0;
                if (!isCompact) {
                    this.dataAddress = H5header.this.readOffset();
                }
                this.chunkSize = new int[ndims];
                for (int i = 0; i < ndims; ++i) {
                    this.chunkSize[i] = H5header.this.raf.readInt();
                }
                if (isCompact) {
                    this.dataSize = H5header.this.raf.readInt();
                    this.dataAddress = H5header.this.raf.getFilePointer();
                }
            }
            else {
                this.type = H5header.this.raf.readByte();
                if (this.type == 0) {
                    this.dataSize = H5header.this.raf.readShort();
                    this.dataAddress = H5header.this.raf.getFilePointer();
                }
                else if (this.type == 1) {
                    this.dataAddress = H5header.this.readOffset();
                    this.contiguousSize = H5header.this.readLength();
                }
                else if (this.type == 2) {
                    final int ndims = H5header.this.raf.readByte();
                    this.dataAddress = H5header.this.readOffset();
                    this.chunkSize = new int[ndims];
                    for (int j = 0; j < ndims; ++j) {
                        this.chunkSize[j] = H5header.this.raf.readInt();
                    }
                }
            }
            if (H5header.debug1) {
                H5header.this.debugOut.println("   StorageLayout version= " + version + this);
            }
        }
    }
    
    class MessageFilter implements Named
    {
        Filter[] filters;
        
        void read() throws IOException {
            final byte version = H5header.this.raf.readByte();
            final byte nfilters = H5header.this.raf.readByte();
            if (version == 1) {
                H5header.this.raf.skipBytes(6);
            }
            this.filters = new Filter[nfilters];
            for (int i = 0; i < nfilters; ++i) {
                this.filters[i] = new Filter(version);
            }
            if (H5header.debug1) {
                H5header.this.debugOut.println("   MessageFilter version=" + version + this);
            }
        }
        
        public Filter[] getFilters() {
            return this.filters;
        }
        
        @Override
        public String toString() {
            final StringBuilder sbuff = new StringBuilder();
            sbuff.append("   MessageFilter filters=\n");
            for (final Filter f : this.filters) {
                sbuff.append(" ").append(f).append("\n");
            }
            return sbuff.toString();
        }
        
        public String getName() {
            final StringBuilder sbuff = new StringBuilder();
            for (final Filter f : this.filters) {
                sbuff.append(f.name).append(", ");
            }
            return sbuff.toString();
        }
    }
    
    class Filter
    {
        short id;
        short flags;
        String name;
        short nValues;
        int[] data;
        
        Filter(final byte version) throws IOException {
            this.id = H5header.this.raf.readShort();
            final short nameSize = (short)((version > 1 && this.id < 256) ? 0 : H5header.this.raf.readShort());
            this.flags = H5header.this.raf.readShort();
            this.nValues = H5header.this.raf.readShort();
            if (version == 1) {
                this.name = ((nameSize > 0) ? H5header.this.readString8(H5header.this.raf) : this.getFilterName(this.id));
            }
            else {
                this.name = ((nameSize > 0) ? H5header.this.readStringFixedLength(nameSize) : this.getFilterName(this.id));
            }
            this.data = new int[this.nValues];
            for (int i = 0; i < this.nValues; ++i) {
                this.data[i] = H5header.this.raf.readInt();
            }
            if (this.nValues % 2 == 1) {
                H5header.this.raf.skipBytes(4);
            }
            if (H5header.debug1) {
                H5header.this.debugOut.println(this);
            }
        }
        
        String getFilterName(final int id) {
            return (id < H5header.this.filterName.length) ? H5header.this.filterName[id] : ("StandardFilter " + id);
        }
        
        @Override
        public String toString() {
            final StringBuilder sbuff = new StringBuilder();
            sbuff.append("   Filter id= ").append(this.id).append(" flags = ").append(this.flags).append(" nValues=").append(this.nValues).append(" name= ").append(this.name).append(" data = ");
            for (int i = 0; i < this.nValues; ++i) {
                sbuff.append(this.data[i]).append(" ");
            }
            return sbuff.toString();
        }
    }
    
    public class MessageAttribute implements Named
    {
        byte version;
        String name;
        MessageDatatype mdt;
        MessageDataspace mds;
        long dataPos;
        
        public MessageAttribute() {
            this.mdt = new MessageDatatype();
            this.mds = new MessageDataspace();
        }
        
        public byte getVersion() {
            return this.version;
        }
        
        public MessageDatatype getMdt() {
            return this.mdt;
        }
        
        public MessageDataspace getMds() {
            return this.mds;
        }
        
        public long getDataPos() {
            return this.dataPos;
        }
        
        @Override
        public String toString() {
            final StringBuilder sbuff = new StringBuilder();
            sbuff.append("   Name= ").append(this.name);
            sbuff.append(" dataPos = ").append(this.dataPos);
            if (this.mdt != null) {
                sbuff.append("\n mdt=");
                sbuff.append(this.mdt.toString());
            }
            if (this.mds != null) {
                sbuff.append("\n mds=");
                sbuff.append(this.mds.toString());
            }
            return sbuff.toString();
        }
        
        public String getName() {
            return this.name;
        }
        
        boolean read() throws IOException {
            if (H5header.debugPos) {
                H5header.this.debugOut.println("   *MessageAttribute start pos= " + H5header.this.raf.getFilePointer());
            }
            byte flags = 0;
            byte encoding = 0;
            this.version = H5header.this.raf.readByte();
            short nameSize;
            short typeSize;
            short spaceSize;
            if (this.version == 1) {
                H5header.this.raf.read();
                nameSize = H5header.this.raf.readShort();
                typeSize = H5header.this.raf.readShort();
                spaceSize = H5header.this.raf.readShort();
            }
            else {
                if (this.version != 2 && this.version != 3) {
                    H5header.log.error("bad version " + this.version + " at filePos " + H5header.this.raf.getFilePointer());
                    return false;
                }
                flags = H5header.this.raf.readByte();
                nameSize = H5header.this.raf.readShort();
                typeSize = H5header.this.raf.readShort();
                spaceSize = H5header.this.raf.readShort();
                if (this.version == 3) {
                    encoding = H5header.this.raf.readByte();
                }
            }
            long filePos = H5header.this.raf.getFilePointer();
            this.name = H5header.this.readString(H5header.this.raf);
            if (this.version == 1) {
                nameSize += (short)H5header.this.padding(nameSize, 8);
            }
            H5header.this.raf.seek(filePos + nameSize);
            if (H5header.debug1) {
                H5header.this.debugOut.println("   MessageAttribute version= " + this.version + " flags = " + Integer.toBinaryString(flags) + " nameSize = " + nameSize + " typeSize=" + typeSize + " spaceSize= " + spaceSize + " name= " + this.name);
            }
            filePos = H5header.this.raf.getFilePointer();
            if (H5header.debugPos) {
                H5header.this.debugOut.println("   *MessageAttribute before mdt pos= " + filePos);
            }
            final boolean isShared = (flags & 0x1) != 0x0;
            if (isShared) {
                this.mdt = H5header.this.getSharedDataObject(MessageType.Datatype).mdt;
                if (H5header.debug1) {
                    H5header.this.debugOut.println("    MessageDatatype: " + this.mdt);
                }
            }
            else {
                this.mdt.read(this.name);
                if (this.version == 1) {
                    typeSize += (short)H5header.this.padding(typeSize, 8);
                }
            }
            H5header.this.raf.seek(filePos + typeSize);
            filePos = H5header.this.raf.getFilePointer();
            if (H5header.debugPos) {
                H5header.this.debugOut.println("   *MessageAttribute before mds = " + filePos);
            }
            this.mds.read();
            if (this.version == 1) {
                spaceSize += (short)H5header.this.padding(spaceSize, 8);
            }
            H5header.this.raf.seek(filePos + spaceSize);
            this.dataPos = H5header.this.raf.getFilePointer();
            if (H5header.debug1) {
                H5header.this.debugOut.println("   *MessageAttribute dataPos= " + this.dataPos);
            }
            return true;
        }
    }
    
    private class MessageAttributeInfo implements Named
    {
        byte version;
        byte flags;
        short maxCreationIndex;
        long fractalHeapAddress;
        long v2BtreeAddress;
        long v2BtreeAddressCreationOrder;
        
        private MessageAttributeInfo() {
            this.maxCreationIndex = -1;
            this.fractalHeapAddress = -2L;
            this.v2BtreeAddress = -2L;
            this.v2BtreeAddressCreationOrder = -2L;
        }
        
        public String getName() {
            final long btreeAddress = (this.v2BtreeAddressCreationOrder > 0L) ? this.v2BtreeAddressCreationOrder : this.v2BtreeAddress;
            return Long.toString(btreeAddress);
        }
        
        @Override
        public String toString() {
            final Formatter f = new Formatter();
            f.format("   MessageAttributeInfo ", new Object[0]);
            if ((this.flags & 0x1) != 0x0) {
                f.format(" maxCreationIndex=" + this.maxCreationIndex, new Object[0]);
            }
            f.format(" fractalHeapAddress=%d v2BtreeAddress=%d", this.fractalHeapAddress, this.v2BtreeAddress);
            if ((this.flags & 0x2) != 0x0) {
                f.format(" v2BtreeAddressCreationOrder=%d", this.v2BtreeAddressCreationOrder);
            }
            this.showFractalHeap(f);
            return f.toString();
        }
        
        void showFractalHeap(final Formatter f) {
            final long btreeAddress = (this.v2BtreeAddressCreationOrder > 0L) ? this.v2BtreeAddressCreationOrder : this.v2BtreeAddress;
            if (this.fractalHeapAddress > 0L && btreeAddress > 0L) {
                try {
                    final FractalHeap fractalHeap = new FractalHeap("", this.fractalHeapAddress);
                    fractalHeap.showDetails(f);
                    f.format(" Btree:%n", new Object[0]);
                    f.format("  type n m  offset size pos       attName%n", new Object[0]);
                    final BTree2 btree = new BTree2("", btreeAddress);
                    for (final BTree2.Entry2 e : btree.entryList) {
                        byte[] heapId = null;
                        switch (btree.btreeType) {
                            case 8: {
                                heapId = ((BTree2.Record8)e.record).heapId;
                                break;
                            }
                            case 9: {
                                heapId = ((BTree2.Record9)e.record).heapId;
                                break;
                            }
                            default: {
                                f.format(" unknown btreetype %d\n", btree.btreeType);
                                continue;
                            }
                        }
                        final FractalHeap.DHeapId dh = fractalHeap.getHeapId(heapId);
                        f.format("   %2d %2d %2d %6d %4d %8d", dh.type, dh.n, dh.m, dh.offset, dh.size, dh.getPos());
                        if (dh.getPos() > 0L) {
                            H5header.this.raf.seek(dh.getPos());
                            final MessageAttribute attMessage = new MessageAttribute();
                            attMessage.read();
                            f.format(" %-30s", this.trunc(attMessage.getName(), 30));
                        }
                        f.format(" heapId=:", new Object[0]);
                        H5header.showBytes(heapId, f);
                        f.format("%n", new Object[0]);
                    }
                }
                catch (IOException e2) {
                    e2.printStackTrace();
                }
            }
        }
        
        String trunc(final String s, final int max) {
            if (s == null) {
                return null;
            }
            if (s.length() < max) {
                return s;
            }
            return s.substring(0, max);
        }
        
        void read() throws IOException {
            if (H5header.debugPos) {
                H5header.this.debugOut.println("   *MessageAttributeInfo start pos= " + H5header.this.raf.getFilePointer());
            }
            final byte version = H5header.this.raf.readByte();
            final byte flags = H5header.this.raf.readByte();
            if ((flags & 0x1) != 0x0) {
                this.maxCreationIndex = H5header.this.raf.readShort();
            }
            this.fractalHeapAddress = H5header.this.readOffset();
            this.v2BtreeAddress = H5header.this.readOffset();
            if ((flags & 0x2) != 0x0) {
                this.v2BtreeAddressCreationOrder = H5header.this.readOffset();
            }
            if (H5header.debug1) {
                H5header.this.debugOut.println("   MessageAttributeInfo version= " + version + " flags = " + flags + this);
            }
        }
    }
    
    private class MessageComment implements Named
    {
        String comment;
        
        void read() throws IOException {
            this.comment = H5header.this.readString(H5header.this.raf);
        }
        
        @Override
        public String toString() {
            return this.comment;
        }
        
        public String getName() {
            return this.comment;
        }
    }
    
    private class MessageLastModified implements Named
    {
        byte version;
        int secs;
        
        void read() throws IOException {
            this.version = H5header.this.raf.readByte();
            H5header.this.raf.skipBytes(3);
            this.secs = H5header.this.raf.readInt();
        }
        
        @Override
        public String toString() {
            return new Date(this.secs * 1000).toString();
        }
        
        public String getName() {
            return this.toString();
        }
    }
    
    private class MessageLastModifiedOld implements Named
    {
        String datemod;
        
        void read() throws IOException {
            final byte[] s = new byte[14];
            H5header.this.raf.read(s);
            this.datemod = new String(s);
            if (H5header.debug1) {
                H5header.this.debugOut.println("   MessageLastModifiedOld=" + this.datemod);
            }
        }
        
        @Override
        public String toString() {
            return this.datemod;
        }
        
        public String getName() {
            return this.toString();
        }
    }
    
    private class MessageContinue implements Named
    {
        long offset;
        long length;
        
        void read() throws IOException {
            this.offset = H5header.this.readOffset();
            this.length = H5header.this.readLength();
            if (H5header.debug1) {
                H5header.this.debugOut.println("   Continue offset=" + this.offset + " length=" + this.length);
            }
        }
        
        public String getName() {
            return "";
        }
    }
    
    private class MessageObjectReferenceCount implements Named
    {
        int refCount;
        
        void read() throws IOException {
            final int version = H5header.this.raf.readByte();
            this.refCount = H5header.this.raf.readInt();
            if (H5header.debug1) {
                H5header.this.debugOut.println("   ObjectReferenceCount=" + this.refCount);
            }
        }
        
        public String getName() {
            return Integer.toString(this.refCount);
        }
    }
    
    private class GroupBTree
    {
        protected String owner;
        protected int wantType;
        private List<SymbolTableEntry> sentries;
        final /* synthetic */ H5header this$0;
        
        GroupBTree(final String owner) {
            this.wantType = 0;
            this.sentries = new ArrayList<SymbolTableEntry>();
            this.owner = owner;
        }
        
        GroupBTree(final String owner, final long address) throws IOException {
            this.wantType = 0;
            this.sentries = new ArrayList<SymbolTableEntry>();
            this.owner = owner;
            final List<Entry> entryList = new ArrayList<Entry>();
            this.readAllEntries(address, entryList);
            for (final Entry e : entryList) {
                final GroupNode node = new GroupNode(e.address);
                this.sentries.addAll(node.getSymbols());
            }
        }
        
        List<SymbolTableEntry> getSymbolTableEntries() {
            return this.sentries;
        }
        
        protected void readAllEntries(final long address, final List<Entry> entryList) throws IOException {
            H5header.this.raf.seek(H5header.this.getFileOffset(address));
            if (H5header.debugGroupBtree) {
                H5header.this.debugOut.println("\n--> GroupBTree read tree at position=" + H5header.this.raf.getFilePointer());
            }
            final byte[] name = new byte[4];
            H5header.this.raf.read(name);
            final String magic = new String(name);
            if (!magic.equals("TREE")) {
                throw new IllegalStateException("BtreeGroup doesnt start with TREE");
            }
            final int type = H5header.this.raf.readByte();
            final int level = H5header.this.raf.readByte();
            final int nentries = H5header.this.raf.readShort();
            if (H5header.debugGroupBtree) {
                H5header.this.debugOut.println("    type=" + type + " level=" + level + " nentries=" + nentries);
            }
            if (type != this.wantType) {
                throw new IllegalStateException("BtreeGroup must be type " + this.wantType);
            }
            final long size = 8 + 2 * H5header.this.sizeOffsets + nentries * (H5header.this.sizeOffsets + H5header.this.sizeLengths);
            if (H5header.debugTracker) {
                H5header.this.memTracker.addByLen("Group BTree (" + this.owner + ")", address, size);
            }
            final long leftAddress = H5header.this.readOffset();
            final long rightAddress = H5header.this.readOffset();
            if (H5header.debugGroupBtree) {
                H5header.this.debugOut.println("    leftAddress=" + leftAddress + " " + Long.toHexString(leftAddress) + " rightAddress=" + rightAddress + " " + Long.toHexString(rightAddress));
            }
            final List<Entry> myEntries = new ArrayList<Entry>();
            for (int i = 0; i < nentries; ++i) {
                myEntries.add(new Entry());
            }
            if (level == 0) {
                entryList.addAll(myEntries);
            }
            else {
                for (final Entry entry : myEntries) {
                    if (H5header.debugDataBtree) {
                        H5header.this.debugOut.println("  nonzero node entry at =" + entry.address);
                    }
                    this.readAllEntries(entry.address, entryList);
                }
            }
        }
        
        class Entry
        {
            long key;
            long address;
            
            Entry() throws IOException {
                this.key = H5header.this.readLength();
                this.address = H5header.this.readOffset();
                if (H5header.debugGroupBtree) {
                    GroupBTree.this.this$0.debugOut.println("     GroupEntry key=" + this.key + " address=" + this.address);
                }
            }
        }
        
        class GroupNode
        {
            long address;
            byte version;
            short nentries;
            List<SymbolTableEntry> symbols;
            
            GroupNode(final long address) throws IOException {
                this.symbols = new ArrayList<SymbolTableEntry>();
                this.address = address;
                GroupBTree.this.this$0.raf.seek(H5header.this.getFileOffset(address));
                if (H5header.debugDetail) {
                    GroupBTree.this.this$0.debugOut.println("--Group Node position=" + GroupBTree.this.this$0.raf.getFilePointer());
                }
                final byte[] sig = new byte[4];
                GroupBTree.this.this$0.raf.read(sig);
                final String magic = new String(sig);
                if (!magic.equals("SNOD")) {
                    throw new IllegalStateException(magic + " should equal SNOD");
                }
                this.version = GroupBTree.this.this$0.raf.readByte();
                GroupBTree.this.this$0.raf.readByte();
                this.nentries = GroupBTree.this.this$0.raf.readShort();
                if (H5header.debugDetail) {
                    GroupBTree.this.this$0.debugOut.println("   version=" + this.version + " nentries=" + this.nentries);
                }
                long posEntry = GroupBTree.this.this$0.raf.getFilePointer();
                for (int i = 0; i < this.nentries; ++i) {
                    final SymbolTableEntry entry = GroupBTree.this.this$0.new SymbolTableEntry(posEntry);
                    posEntry += entry.getSize();
                    if (entry.objectHeaderAddress != 0L) {
                        if (H5header.debug1) {
                            GroupBTree.this.this$0.debugOut.printf("   add %s%n", entry);
                        }
                        this.symbols.add(entry);
                    }
                    else if (H5header.debug1) {
                        GroupBTree.this.this$0.debugOut.printf("   BAD objectHeaderAddress==0 !! %s%n", entry);
                    }
                }
                if (H5header.debugDetail) {
                    GroupBTree.this.this$0.debugOut.println("-- Group Node end position=" + GroupBTree.this.this$0.raf.getFilePointer());
                }
                final long size = 8 + this.nentries * 40;
                if (H5header.debugTracker) {
                    GroupBTree.this.this$0.memTracker.addByLen("Group BtreeNode (" + GroupBTree.this.owner + ")", address, size);
                }
            }
            
            List<SymbolTableEntry> getSymbols() {
                return this.symbols;
            }
        }
    }
    
    private class SymbolTableEntry
    {
        long nameOffset;
        long objectHeaderAddress;
        long btreeAddress;
        long nameHeapAddress;
        int cacheType;
        int linkOffset;
        long posData;
        boolean isSymbolicLink;
        
        SymbolTableEntry(final long filePos) throws IOException {
            this.isSymbolicLink = false;
            H5header.this.raf.seek(filePos);
            if (H5header.debugSymbolTable) {
                H5header.this.debugOut.println("--> readSymbolTableEntry position=" + H5header.this.raf.getFilePointer());
            }
            this.nameOffset = H5header.this.readOffset();
            this.objectHeaderAddress = H5header.this.readOffset();
            this.cacheType = H5header.this.raf.readInt();
            H5header.this.raf.skipBytes(4);
            if (H5header.debugSymbolTable) {
                H5header.this.debugOut.print(" nameOffset=" + this.nameOffset);
                H5header.this.debugOut.print(" objectHeaderAddress=" + this.objectHeaderAddress);
                H5header.this.debugOut.println(" cacheType=" + this.cacheType);
            }
            this.posData = H5header.this.raf.getFilePointer();
            if (H5header.debugSymbolTable) {
                H5header.this.dump("Group Entry scratch pad", this.posData, 16, false);
            }
            if (this.cacheType == 1) {
                this.btreeAddress = H5header.this.readOffset();
                this.nameHeapAddress = H5header.this.readOffset();
                if (H5header.debugSymbolTable) {
                    H5header.this.debugOut.println("btreeAddress=" + this.btreeAddress + " nameHeadAddress=" + this.nameHeapAddress);
                }
            }
            if (this.cacheType == 2) {
                this.linkOffset = H5header.this.raf.readInt();
                if (H5header.debugSymbolTable) {
                    H5header.this.debugOut.println("WARNING Symbolic Link linkOffset=" + this.linkOffset);
                }
                this.isSymbolicLink = true;
            }
            if (H5header.debugSymbolTable) {
                H5header.this.debugOut.println("<-- end readSymbolTableEntry position=" + H5header.this.raf.getFilePointer());
            }
            H5header.this.memTracker.add("SymbolTableEntry", filePos, this.posData + 16L);
        }
        
        public int getSize() {
            return H5header.this.isOffsetLong ? 40 : 32;
        }
        
        public long getObjectAddress() {
            return this.objectHeaderAddress;
        }
        
        public long getNameOffset() {
            return this.nameOffset;
        }
        
        @Override
        public String toString() {
            return "SymbolTableEntry{nameOffset=" + this.nameOffset + ", objectHeaderAddress=" + this.objectHeaderAddress + ", btreeAddress=" + this.btreeAddress + ", nameHeapAddress=" + this.nameHeapAddress + ", cacheType=" + this.cacheType + ", linkOffset=" + this.linkOffset + ", posData=" + this.posData + ", isSymbolicLink=" + this.isSymbolicLink + '}';
        }
    }
    
    private class BTree2
    {
        private String owner;
        private byte btreeType;
        private int nodeSize;
        private short recordSize;
        private short numRecordsRootNode;
        private List<Entry2> entryList;
        final /* synthetic */ H5header this$0;
        
        BTree2(final String owner, final long address) throws IOException {
            this.entryList = new ArrayList<Entry2>();
            this.owner = owner;
            H5header.this.raf.seek(H5header.this.getFileOffset(address));
            final byte[] heapname = new byte[4];
            H5header.this.raf.read(heapname);
            final String magic = new String(heapname);
            if (!magic.equals("BTHD")) {
                throw new IllegalStateException(magic + " should equal BTHD");
            }
            final byte version = H5header.this.raf.readByte();
            this.btreeType = H5header.this.raf.readByte();
            this.nodeSize = H5header.this.raf.readInt();
            this.recordSize = H5header.this.raf.readShort();
            final short treeDepth = H5header.this.raf.readShort();
            final byte split = H5header.this.raf.readByte();
            final byte merge = H5header.this.raf.readByte();
            final long rootNodeAddress = H5header.this.readOffset();
            this.numRecordsRootNode = H5header.this.raf.readShort();
            final long totalRecords = H5header.this.readLength();
            final int checksum = H5header.this.raf.readInt();
            if (H5header.debugBtree2) {
                H5header.this.debugOut.println("BTree2 version=" + version + " type=" + this.btreeType + " treeDepth=" + treeDepth);
                H5header.this.debugOut.println(" nodeSize=" + this.nodeSize + " recordSize=" + this.recordSize + " numRecordsRootNode=" + this.numRecordsRootNode + " totalRecords=" + totalRecords + " rootNodeAddress=" + rootNodeAddress);
            }
            if (treeDepth > 0) {
                final InternalNode node = new InternalNode(rootNodeAddress, this.numRecordsRootNode, this.recordSize, treeDepth);
                node.recurse();
            }
            else {
                final LeafNode leaf = new LeafNode(rootNodeAddress, this.numRecordsRootNode);
                leaf.addEntries(this.entryList);
            }
        }
        
        Object readRecord(final int type) throws IOException {
            switch (type) {
                case 1: {
                    return new Record1();
                }
                case 2: {
                    return new Record2();
                }
                case 3: {
                    return new Record3();
                }
                case 4: {
                    return new Record4();
                }
                case 5: {
                    return new Record5();
                }
                case 6: {
                    return new Record6();
                }
                case 7: {
                    return new Record70();
                }
                case 8: {
                    return new Record8();
                }
                case 9: {
                    return new Record9();
                }
                default: {
                    throw new IllegalStateException();
                }
            }
        }
        
        class Entry2
        {
            long childAddress;
            long nrecords;
            long totNrecords;
            Object record;
        }
        
        class InternalNode
        {
            Entry2[] entries;
            int depth;
            
            InternalNode(final long address, final short nrecords, final short recordSize, final int depth) throws IOException {
                this.depth = depth;
                BTree2.this.this$0.raf.seek(H5header.this.getFileOffset(address));
                if (H5header.debugPos) {
                    BTree2.this.this$0.debugOut.println("--Btree2 InternalNode position=" + BTree2.this.this$0.raf.getFilePointer());
                }
                final byte[] sig = new byte[4];
                BTree2.this.this$0.raf.read(sig);
                final String magic = new String(sig);
                if (!magic.equals("BTIN")) {
                    throw new IllegalStateException(magic + " should equal BTIN");
                }
                final byte version = BTree2.this.this$0.raf.readByte();
                final byte nodeType = BTree2.this.this$0.raf.readByte();
                if (nodeType != BTree2.this.btreeType) {
                    throw new IllegalStateException();
                }
                if (H5header.debugBtree2) {
                    BTree2.this.this$0.debugOut.println("   BTree2 InternalNode version=" + version + " type=" + nodeType + " nrecords=" + nrecords);
                }
                this.entries = new Entry2[nrecords + 1];
                for (int i = 0; i < nrecords; ++i) {
                    this.entries[i] = new Entry2();
                    this.entries[i].record = BTree2.this.readRecord(BTree2.this.btreeType);
                }
                this.entries[nrecords] = new Entry2();
                final int maxNumRecords = BTree2.this.nodeSize / recordSize;
                final int maxNumRecordsPlusDesc = BTree2.this.nodeSize / recordSize;
                for (int j = 0; j < nrecords + 1; ++j) {
                    final Entry2 e = this.entries[j];
                    e.childAddress = H5header.this.readOffset();
                    e.nrecords = H5header.this.readVariableSizeUnsigned(1);
                    if (depth > 1) {
                        e.totNrecords = H5header.this.readVariableSizeUnsigned(2);
                    }
                    if (H5header.debugBtree2) {
                        BTree2.this.this$0.debugOut.println(" BTree2 entry childAddress=" + e.childAddress + " nrecords=" + e.nrecords + " totNrecords=" + e.totNrecords);
                    }
                }
                final int checksum = BTree2.this.this$0.raf.readInt();
            }
            
            void recurse() throws IOException {
                for (final Entry2 e : this.entries) {
                    if (this.depth > 1) {
                        final InternalNode node = new InternalNode(e.childAddress, (short)e.nrecords, BTree2.this.recordSize, this.depth - 1);
                        node.recurse();
                    }
                    else {
                        final long nrecs = e.nrecords;
                        final LeafNode leaf = new LeafNode(e.childAddress, (short)nrecs);
                        leaf.addEntries(BTree2.this.entryList);
                    }
                    if (e.record != null) {
                        BTree2.this.entryList.add(e);
                    }
                }
            }
        }
        
        class LeafNode
        {
            Entry2[] entries;
            
            LeafNode(final long address, final short nrecords) throws IOException {
                BTree2.this.this$0.raf.seek(H5header.this.getFileOffset(address));
                if (H5header.debugPos) {
                    BTree2.this.this$0.debugOut.println("--Btree2 InternalNode position=" + BTree2.this.this$0.raf.getFilePointer());
                }
                final byte[] sig = new byte[4];
                BTree2.this.this$0.raf.read(sig);
                final String magic = new String(sig);
                if (!magic.equals("BTLF")) {
                    throw new IllegalStateException(magic + " should equal BTLF");
                }
                final byte version = BTree2.this.this$0.raf.readByte();
                final byte nodeType = BTree2.this.this$0.raf.readByte();
                if (nodeType != BTree2.this.btreeType) {
                    throw new IllegalStateException();
                }
                if (H5header.debugBtree2) {
                    BTree2.this.this$0.debugOut.println("   BTree2 LeafNode version=" + version + " type=" + nodeType + " nrecords=" + nrecords);
                }
                this.entries = new Entry2[nrecords];
                for (int i = 0; i < nrecords; ++i) {
                    this.entries[i] = new Entry2();
                    this.entries[i].record = BTree2.this.readRecord(BTree2.this.btreeType);
                }
                final int checksum = BTree2.this.this$0.raf.readInt();
            }
            
            void addEntries(final List<Entry2> list) {
                for (int i = 0; i < this.entries.length; ++i) {
                    list.add(this.entries[i]);
                }
            }
        }
        
        class Record1
        {
            long hugeObjectAddress;
            long hugeObjectLength;
            long hugeObjectID;
            
            Record1() throws IOException {
                this.hugeObjectAddress = H5header.this.readOffset();
                this.hugeObjectLength = H5header.this.readLength();
                this.hugeObjectID = H5header.this.readLength();
            }
        }
        
        class Record2
        {
            long hugeObjectAddress;
            long hugeObjectLength;
            long hugeObjectID;
            long hugeObjectSize;
            int filterMask;
            
            Record2() throws IOException {
                this.hugeObjectAddress = H5header.this.readOffset();
                this.hugeObjectLength = H5header.this.readLength();
                this.filterMask = BTree2.this.this$0.raf.readInt();
                this.hugeObjectSize = H5header.this.readLength();
                this.hugeObjectID = H5header.this.readLength();
            }
        }
        
        class Record3
        {
            long hugeObjectAddress;
            long hugeObjectLength;
            
            Record3() throws IOException {
                this.hugeObjectAddress = H5header.this.readOffset();
                this.hugeObjectLength = H5header.this.readLength();
            }
        }
        
        class Record4
        {
            long hugeObjectAddress;
            long hugeObjectLength;
            long hugeObjectID;
            long hugeObjectSize;
            int filterMask;
            
            Record4() throws IOException {
                this.hugeObjectAddress = H5header.this.readOffset();
                this.hugeObjectLength = H5header.this.readLength();
                this.filterMask = BTree2.this.this$0.raf.readInt();
                this.hugeObjectSize = H5header.this.readLength();
            }
        }
        
        class Record5
        {
            int nameHash;
            byte[] heapId;
            
            Record5() throws IOException {
                this.heapId = new byte[7];
                this.nameHash = BTree2.this.this$0.raf.readInt();
                BTree2.this.this$0.raf.read(this.heapId);
                if (H5header.debugBtree2) {
                    BTree2.this.this$0.debugOut.println("  record5 nameHash=" + this.nameHash + " heapId=" + H5header.showBytes(this.heapId));
                }
            }
        }
        
        class Record6
        {
            long creationOrder;
            byte[] heapId;
            
            Record6() throws IOException {
                this.heapId = new byte[7];
                this.creationOrder = BTree2.this.this$0.raf.readLong();
                BTree2.this.this$0.raf.read(this.heapId);
                if (H5header.debugBtree2) {
                    BTree2.this.this$0.debugOut.println("  record6 creationOrder=" + this.creationOrder + " heapId=" + H5header.showBytes(this.heapId));
                }
            }
        }
        
        class Record70
        {
            byte location;
            int refCount;
            byte[] id;
            
            Record70() throws IOException {
                this.id = new byte[8];
                this.location = BTree2.this.this$0.raf.readByte();
                this.refCount = BTree2.this.this$0.raf.readInt();
                BTree2.this.this$0.raf.read(this.id);
            }
        }
        
        class Record71
        {
            byte location;
            byte messtype;
            short index;
            long address;
            
            Record71() throws IOException {
                this.location = BTree2.this.this$0.raf.readByte();
                BTree2.this.this$0.raf.readByte();
                this.messtype = BTree2.this.this$0.raf.readByte();
                this.index = BTree2.this.this$0.raf.readShort();
                this.address = H5header.this.readOffset();
            }
        }
        
        class Record8
        {
            byte flags;
            int creationOrder;
            int nameHash;
            byte[] heapId;
            
            Record8() throws IOException {
                this.heapId = new byte[8];
                BTree2.this.this$0.raf.read(this.heapId);
                this.flags = BTree2.this.this$0.raf.readByte();
                this.creationOrder = BTree2.this.this$0.raf.readInt();
                this.nameHash = BTree2.this.this$0.raf.readInt();
                if (H5header.debugBtree2) {
                    BTree2.this.this$0.debugOut.println("  record8 creationOrder=" + this.creationOrder + " heapId=" + H5header.showBytes(this.heapId));
                }
            }
        }
        
        class Record9
        {
            byte flags;
            int creationOrder;
            byte[] heapId;
            
            Record9() throws IOException {
                this.heapId = new byte[8];
                BTree2.this.this$0.raf.read(this.heapId);
                this.flags = BTree2.this.this$0.raf.readByte();
                this.creationOrder = BTree2.this.this$0.raf.readInt();
            }
        }
    }
    
    class DataBTree
    {
        private Variable owner;
        private long rootNodeAddress;
        private Tiling tiling;
        private int ndimStorage;
        private int wantType;
        final /* synthetic */ H5header this$0;
        
        DataBTree(final long rootNodeAddress, final int[] varShape, final int[] storageSize) throws IOException {
            this.rootNodeAddress = rootNodeAddress;
            this.tiling = new Tiling(varShape, storageSize);
            this.ndimStorage = storageSize.length;
            this.wantType = 1;
        }
        
        void setOwner(final Variable owner) {
            this.owner = owner;
        }
        
        DataChunkIterator getDataChunkIterator(final Section want) throws IOException {
            return new DataChunkIterator(want);
        }
        
        LayoutTiled.DataChunkIterator getDataChunkIterator2(final Section want, final int nChunkDim) throws IOException {
            return new DataChunkIterator2(want, nChunkDim);
        }
        
        class DataChunkIterator2 implements LayoutTiled.DataChunkIterator
        {
            private Node root;
            private int nChunkDim;
            
            DataChunkIterator2(final Section want, final int nChunkDim) throws IOException {
                this.nChunkDim = nChunkDim;
                this.root = new Node(DataBTree.this.rootNodeAddress, -1L);
                final int[] wantOrigin = (int[])((want != null) ? want.getOrigin() : null);
                this.root.first(wantOrigin);
            }
            
            public boolean hasNext() {
                return this.root.hasNext();
            }
            
            public LayoutTiled.DataChunk next() throws IOException {
                final DataBTree.DataChunk dc = this.root.next();
                int[] offset = dc.offset;
                if (offset.length > this.nChunkDim) {
                    offset = new int[this.nChunkDim];
                    System.arraycopy(dc.offset, 0, offset, 0, this.nChunkDim);
                }
                return new LayoutTiled.DataChunk(offset, dc.filePos);
            }
        }
        
        class DataChunkIterator
        {
            private Node root;
            private int[] wantOrigin;
            
            DataChunkIterator(final Section want) throws IOException {
                this.root = new Node(DataBTree.this.rootNodeAddress, -1L);
                this.wantOrigin = (int[])((want != null) ? want.getOrigin() : null);
                this.root.first(this.wantOrigin);
            }
            
            public boolean hasNext() {
                return this.root.hasNext();
            }
            
            public DataChunk next() throws IOException {
                return this.root.next();
            }
        }
        
        class Node
        {
            private long address;
            private int level;
            private int nentries;
            private Node currentNode;
            private List<DataChunk> myEntries;
            private int[][] offset;
            private long[] childPointer;
            private int currentEntry;
            
            Node(final long address, final long parent) throws IOException {
                if (H5header.debugDataBtree) {
                    DataBTree.this.this$0.debugOut.println("\n--> DataBTree read tree at address=" + address + " parent= " + parent + " owner= " + DataBTree.this.owner.getNameAndDimensions());
                }
                DataBTree.this.this$0.raf.order(1);
                DataBTree.this.this$0.raf.seek(H5header.this.getFileOffset(address));
                this.address = address;
                final byte[] name = new byte[4];
                DataBTree.this.this$0.raf.read(name);
                final String magic = new String(name);
                if (!magic.equals("TREE")) {
                    throw new IllegalStateException("DataBTree doesnt start with TREE");
                }
                final int type = DataBTree.this.this$0.raf.readByte();
                this.level = DataBTree.this.this$0.raf.readByte();
                this.nentries = DataBTree.this.this$0.raf.readShort();
                if (type != DataBTree.this.wantType) {
                    throw new IllegalStateException("DataBTree must be type " + DataBTree.this.wantType);
                }
                final long size = 8 + 2 * DataBTree.this.this$0.sizeOffsets + this.nentries * (8 + DataBTree.this.this$0.sizeOffsets + 8 + DataBTree.this.ndimStorage);
                if (H5header.debugTracker) {
                    DataBTree.this.this$0.memTracker.addByLen("Data BTree (" + DataBTree.this.owner + ")", address, size);
                }
                if (H5header.debugDataBtree) {
                    DataBTree.this.this$0.debugOut.println("    type=" + type + " level=" + this.level + " nentries=" + this.nentries + " size = " + size);
                }
                final long leftAddress = H5header.this.readOffset();
                final long rightAddress = H5header.this.readOffset();
                if (H5header.debugDataBtree) {
                    DataBTree.this.this$0.debugOut.println("    leftAddress=" + leftAddress + " =0x" + Long.toHexString(leftAddress) + " rightAddress=" + rightAddress + " =0x" + Long.toHexString(rightAddress));
                }
                if (this.level == 0) {
                    this.myEntries = new ArrayList<DataChunk>();
                    for (int i = 0; i <= this.nentries; ++i) {
                        final DataChunk dc = new DataChunk(DataBTree.this.ndimStorage, i == this.nentries);
                        this.myEntries.add(dc);
                        if (H5header.debugDataChunk) {
                            DataBTree.this.this$0.debugOut.println(dc);
                        }
                    }
                }
                else {
                    this.offset = new int[this.nentries + 1][DataBTree.this.ndimStorage];
                    this.childPointer = new long[this.nentries + 1];
                    for (int i = 0; i <= this.nentries; ++i) {
                        DataBTree.this.this$0.raf.skipBytes(8);
                        for (int j = 0; j < DataBTree.this.ndimStorage; ++j) {
                            final long loffset = DataBTree.this.this$0.raf.readLong();
                            assert loffset < 2147483647L;
                            this.offset[i][j] = (int)loffset;
                        }
                        this.childPointer[i] = ((i == this.nentries) ? -1L : H5header.this.readOffset());
                        if (H5header.debugDataBtree) {
                            DataBTree.this.this$0.debugOut.print("    childPointer=" + this.childPointer[i] + " =0x" + Long.toHexString(this.childPointer[i]));
                            for (final long anOffset : this.offset[i]) {
                                DataBTree.this.this$0.debugOut.print(" " + anOffset);
                            }
                            DataBTree.this.this$0.debugOut.println();
                        }
                    }
                }
            }
            
            void first(final int[] wantOrigin) throws IOException {
                if (this.level == 0) {
                    this.currentEntry = 0;
                    while (this.currentEntry < this.nentries - 1) {
                        final DataChunk entry = this.myEntries.get(this.currentEntry + 1);
                        if (wantOrigin == null) {
                            break;
                        }
                        if (DataBTree.this.tiling.compare(wantOrigin, entry.offset) < 0) {
                            break;
                        }
                        ++this.currentEntry;
                    }
                }
                else {
                    this.currentNode = null;
                    this.currentEntry = 0;
                    while (this.currentEntry < this.nentries) {
                        if (wantOrigin == null || DataBTree.this.tiling.compare(wantOrigin, this.offset[this.currentEntry + 1]) < 0) {
                            (this.currentNode = new Node(this.childPointer[this.currentEntry], this.address)).first(wantOrigin);
                            break;
                        }
                        ++this.currentEntry;
                    }
                    if (this.currentNode == null) {
                        this.currentEntry = this.nentries - 1;
                        (this.currentNode = new Node(this.childPointer[this.currentEntry], this.address)).first(wantOrigin);
                    }
                }
                assert this.currentEntry < this.nentries : this.currentEntry + " >= " + this.nentries;
            }
            
            boolean hasNext() {
                if (this.level == 0) {
                    return this.currentEntry < this.nentries;
                }
                return this.currentNode.hasNext() || this.currentEntry < this.nentries - 1;
            }
            
            DataChunk next() throws IOException {
                if (this.level == 0) {
                    return this.myEntries.get(this.currentEntry++);
                }
                if (this.currentNode.hasNext()) {
                    return this.currentNode.next();
                }
                ++this.currentEntry;
                (this.currentNode = new Node(this.childPointer[this.currentEntry], this.address)).first(null);
                return this.currentNode.next();
            }
        }
        
        class DataChunk
        {
            int size;
            int filterMask;
            int[] offset;
            long filePos;
            
            DataChunk(final int ndim, final boolean last) throws IOException {
                this.size = DataBTree.this.this$0.raf.readInt();
                this.filterMask = DataBTree.this.this$0.raf.readInt();
                this.offset = new int[ndim];
                for (int i = 0; i < ndim; ++i) {
                    final long loffset = DataBTree.this.this$0.raf.readLong();
                    assert loffset < 2147483647L;
                    this.offset[i] = (int)loffset;
                }
                this.filePos = (last ? -1L : H5header.this.getFileOffset(H5header.this.readOffset()));
                if (H5header.debugTracker) {
                    DataBTree.this.this$0.memTracker.addByLen("Chunked Data (" + DataBTree.this.owner + ")", this.filePos, this.size);
                }
            }
            
            @Override
            public String toString() {
                final StringBuilder sbuff = new StringBuilder();
                sbuff.append("  ChunkedDataNode size=").append(this.size).append(" filterMask=").append(this.filterMask).append(" filePos=").append(this.filePos).append(" offsets= ");
                for (final long anOffset : this.offset) {
                    sbuff.append(anOffset).append(" ");
                }
                return sbuff.toString();
            }
        }
    }
    
    class HeapIdentifier
    {
        private int nelems;
        private long heapAddress;
        private int index;
        
        HeapIdentifier(final long address) throws IOException {
            H5header.this.raf.order(1);
            H5header.this.raf.seek(H5header.this.getFileOffset(address));
            this.nelems = H5header.this.raf.readInt();
            this.heapAddress = H5header.this.readOffset();
            this.index = H5header.this.raf.readInt();
            if (H5header.debugDetail) {
                H5header.this.debugOut.println("   read HeapIdentifier address=" + address + this);
            }
            if (H5header.debugHeap) {
                H5header.this.dump("heapIdentifier", H5header.this.getFileOffset(address), 16, true);
            }
        }
        
        HeapIdentifier(final ByteBuffer bb, final int pos) throws IOException {
            bb.order(ByteOrder.LITTLE_ENDIAN);
            bb.position(pos);
            this.nelems = bb.getInt();
            this.heapAddress = (H5header.this.isOffsetLong ? bb.getLong() : bb.getInt());
            this.index = bb.getInt();
            if (H5header.debugDetail) {
                H5header.this.debugOut.println("   read HeapIdentifier from ByteBuffer=" + this);
            }
        }
        
        @Override
        public String toString() {
            return " nelems=" + this.nelems + " heapAddress=" + this.heapAddress + " index=" + this.index;
        }
        
        GlobalHeap.HeapObject getHeapObject() throws IOException {
            GlobalHeap gheap;
            if (null == (gheap = H5header.this.heapMap.get(this.heapAddress))) {
                gheap = new GlobalHeap(this.heapAddress);
                H5header.this.heapMap.put(this.heapAddress, gheap);
            }
            for (final GlobalHeap.HeapObject ho : gheap.hos) {
                if (ho.id == this.index) {
                    return ho;
                }
            }
            throw new IllegalStateException("cant find HeapObject");
        }
    }
    
    private class RegionReference
    {
        private long heapAddress;
        private int index;
        
        RegionReference(final long filePos) throws IOException {
            H5header.this.raf.order(1);
            H5header.this.raf.seek(filePos);
            this.heapAddress = H5header.this.readOffset();
            this.index = H5header.this.raf.readInt();
            GlobalHeap gheap;
            if (null == (gheap = H5header.this.heapMap.get(this.heapAddress))) {
                gheap = new GlobalHeap(this.heapAddress);
                H5header.this.heapMap.put(this.heapAddress, gheap);
            }
            for (final GlobalHeap.HeapObject ho : gheap.hos) {
                if (ho.id == this.index) {
                    final GlobalHeap.HeapObject want = ho;
                    if (H5header.debugRegionReference) {
                        System.out.println(" found ho=" + ho);
                    }
                    H5header.this.raf.seek(ho.dataPos);
                    final long objId = H5header.this.raf.readLong();
                    final DataObject ndo = H5header.this.getDataObject(objId, null);
                    if (H5header.debugRegionReference) {
                        System.out.println(" objId=" + objId + " DataObject= " + ndo);
                    }
                    if (null == ndo) {
                        throw new IllegalStateException("cant find data object at" + objId);
                    }
                    return;
                }
            }
            throw new IllegalStateException("cant find HeapObject");
        }
    }
    
    private class GlobalHeap
    {
        byte version;
        int size;
        List<HeapObject> hos;
        HeapObject freeSpace;
        
        GlobalHeap(final long address) throws IOException {
            this.hos = new ArrayList<HeapObject>();
            this.freeSpace = null;
            H5header.this.raf.order(1);
            H5header.this.raf.seek(H5header.this.getFileOffset(address));
            final byte[] heapname = new byte[4];
            H5header.this.raf.read(heapname);
            final String magic = new String(heapname);
            if (!magic.equals("GCOL")) {
                throw new IllegalStateException(magic + " should equal GCOL");
            }
            this.version = H5header.this.raf.readByte();
            H5header.this.raf.skipBytes(3);
            this.size = H5header.this.raf.readInt();
            if (H5header.debugDetail) {
                H5header.this.debugOut.println("-- readGlobalHeap address=" + address + " version= " + this.version + " size = " + this.size);
            }
            H5header.this.raf.skipBytes(4);
            HeapObject o;
            for (int count = 0; count + 16 < this.size; count += (int)(o.dataSize + 16L)) {
                final long startPos = H5header.this.raf.getFilePointer();
                o = new HeapObject();
                o.id = H5header.this.raf.readShort();
                o.refCount = H5header.this.raf.readShort();
                H5header.this.raf.skipBytes(4);
                o.dataSize = H5header.this.readLength();
                o.dataPos = H5header.this.raf.getFilePointer();
                if (H5header.debugDetail) {
                    H5header.this.debugOut.println("   HeapObject  position=" + startPos + " id=" + o.id + " refCount= " + o.refCount + " dataSize = " + o.dataSize + " dataPos = " + o.dataPos);
                }
                if (o.id == 0) {
                    break;
                }
                final int nskip = (int)o.dataSize + H5header.this.padding((int)o.dataSize, 8);
                H5header.this.raf.skipBytes(nskip);
                this.hos.add(o);
            }
            if (H5header.debugDetail) {
                H5header.this.debugOut.println("-- endGlobalHeap position=" + H5header.this.raf.getFilePointer());
            }
            if (H5header.debugTracker) {
                H5header.this.memTracker.addByLen("GlobalHeap", address, this.size);
            }
        }
        
        class HeapObject
        {
            short id;
            short refCount;
            long dataSize;
            long dataPos;
            
            @Override
            public String toString() {
                return "dataPos = " + this.dataPos + " dataSize = " + this.dataSize;
            }
        }
    }
    
    private class LocalHeap
    {
        H5Group group;
        int size;
        long freelistOffset;
        long dataAddress;
        byte[] heap;
        byte version;
        
        LocalHeap(final H5Group group, final long address) throws IOException {
            this.group = group;
            H5header.this.raf.order(1);
            H5header.this.raf.seek(H5header.this.getFileOffset(address));
            if (H5header.debugDetail) {
                H5header.this.debugOut.println("-- readLocalHeap position=" + H5header.this.raf.getFilePointer());
            }
            final byte[] heapname = new byte[4];
            H5header.this.raf.read(heapname);
            final String magic = new String(heapname);
            if (!magic.equals("HEAP")) {
                throw new IllegalStateException(magic + " should equal HEAP");
            }
            this.version = H5header.this.raf.readByte();
            H5header.this.raf.skipBytes(3);
            this.size = (int)H5header.this.readLength();
            this.freelistOffset = H5header.this.readLength();
            this.dataAddress = H5header.this.readOffset();
            if (H5header.debugDetail) {
                H5header.this.debugOut.println(" version=" + this.version + " size=" + this.size + " freelistOffset=" + this.freelistOffset + " heap starts at dataAddress=" + this.dataAddress);
            }
            if (H5header.debugPos) {
                H5header.this.debugOut.println("    *now at position=" + H5header.this.raf.getFilePointer());
            }
            H5header.this.raf.seek(H5header.this.getFileOffset(this.dataAddress));
            this.heap = new byte[this.size];
            H5header.this.raf.read(this.heap);
            if (H5header.debugDetail) {
                H5header.this.debugOut.println("-- endLocalHeap position=" + H5header.this.raf.getFilePointer());
            }
            final int hsize = 8 + 2 * H5header.this.sizeLengths + H5header.this.sizeOffsets;
            if (H5header.debugTracker) {
                H5header.this.memTracker.addByLen("Group LocalHeap (" + group.displayName + ")", address, hsize);
            }
            if (H5header.debugTracker) {
                H5header.this.memTracker.addByLen("Group LocalHeapData (" + group.displayName + ")", this.dataAddress, this.size);
            }
        }
        
        public String getString(final int offset) {
            int count;
            for (count = 0; this.heap[offset + count] != 0; ++count) {}
            try {
                return new String(this.heap, offset, count, H5header.utf8CharsetName);
            }
            catch (UnsupportedEncodingException e) {
                throw new IllegalStateException(e.getMessage());
            }
        }
    }
    
    private class FractalHeap
    {
        int version;
        short heapIdLen;
        byte flags;
        int maxSizeOfObjects;
        long nextHugeObjectId;
        long freeSpace;
        long managedSpace;
        long allocatedManagedSpace;
        long offsetDirectBlock;
        long nManagedObjects;
        long sizeHugeObjects;
        long nHugeObjects;
        long sizeTinyObjects;
        long nTinyObjects;
        long btreeAddress;
        long freeSpaceTrackerAddress;
        short maxHeapSize;
        short startingNumRows;
        short currentNumRows;
        long maxDirectBlockSize;
        short tableWidth;
        long startingBlockSize;
        long rootBlockAddress;
        IndirectBlock rootBlock;
        short ioFilterLen;
        long sizeFilteredRootDirectBlock;
        int ioFilterMask;
        byte[] ioFilterInfo;
        DoublingTable doublingTable;
        final /* synthetic */ H5header this$0;
        
        FractalHeap(final String forWho, final long address) throws IOException {
            H5header.this.raf.order(1);
            H5header.this.raf.seek(H5header.this.getFileOffset(address));
            if (H5header.debugDetail) {
                H5header.this.debugOut.println("-- readFractalHeap position=" + H5header.this.raf.getFilePointer());
            }
            final byte[] heapname = new byte[4];
            H5header.this.raf.read(heapname);
            final String magic = new String(heapname);
            if (!magic.equals("FRHP")) {
                throw new IllegalStateException(magic + " should equal FRHP");
            }
            this.version = H5header.this.raf.readByte();
            this.heapIdLen = H5header.this.raf.readShort();
            this.ioFilterLen = H5header.this.raf.readShort();
            this.flags = H5header.this.raf.readByte();
            this.maxSizeOfObjects = H5header.this.raf.readInt();
            this.nextHugeObjectId = H5header.this.readLength();
            this.btreeAddress = H5header.this.readOffset();
            this.freeSpace = H5header.this.readLength();
            this.freeSpaceTrackerAddress = H5header.this.readOffset();
            this.managedSpace = H5header.this.readLength();
            this.allocatedManagedSpace = H5header.this.readLength();
            this.offsetDirectBlock = H5header.this.readLength();
            this.nManagedObjects = H5header.this.readLength();
            this.sizeHugeObjects = H5header.this.readLength();
            this.nHugeObjects = H5header.this.readLength();
            this.sizeTinyObjects = H5header.this.readLength();
            this.nTinyObjects = H5header.this.readLength();
            this.tableWidth = H5header.this.raf.readShort();
            this.startingBlockSize = H5header.this.readLength();
            this.maxDirectBlockSize = H5header.this.readLength();
            this.maxHeapSize = H5header.this.raf.readShort();
            this.startingNumRows = H5header.this.raf.readShort();
            this.rootBlockAddress = H5header.this.readOffset();
            this.currentNumRows = H5header.this.raf.readShort();
            final boolean hasFilters = this.ioFilterLen > 0;
            if (hasFilters) {
                this.sizeFilteredRootDirectBlock = H5header.this.readLength();
                this.ioFilterMask = H5header.this.raf.readInt();
                this.ioFilterInfo = new byte[this.ioFilterLen];
                H5header.this.raf.read(this.ioFilterInfo);
            }
            final int checksum = H5header.this.raf.readInt();
            if (H5header.debugDetail || H5header.debugFractalHeap) {
                H5header.this.debugOut.println("FractalHeap for " + forWho + " version=" + this.version + " heapIdLen=" + this.heapIdLen + " ioFilterLen=" + this.ioFilterLen + " flags= " + this.flags);
                H5header.this.debugOut.println(" maxSizeOfObjects=" + this.maxSizeOfObjects + " nextHugeObjectId=" + this.nextHugeObjectId + " btreeAddress=" + this.btreeAddress + " managedSpace=" + this.managedSpace + " allocatedManagedSpace=" + this.allocatedManagedSpace + " freeSpace=" + this.freeSpace);
                H5header.this.debugOut.println(" nManagedObjects=" + this.nManagedObjects + " nHugeObjects= " + this.nHugeObjects + " nTinyObjects=" + this.nTinyObjects + " maxDirectBlockSize=" + this.maxDirectBlockSize + " maxHeapSize= 2^" + this.maxHeapSize);
                H5header.this.debugOut.println(" DoublingTable: tableWidth=" + this.tableWidth + " startingBlockSize=" + this.startingBlockSize);
                H5header.this.debugOut.println(" rootBlockAddress=" + this.rootBlockAddress + " startingNumRows=" + this.startingNumRows + " currentNumRows=" + this.currentNumRows);
            }
            if (H5header.debugPos) {
                H5header.this.debugOut.println("    *now at position=" + H5header.this.raf.getFilePointer());
            }
            final long pos = H5header.this.raf.getFilePointer();
            if (H5header.debugDetail) {
                H5header.this.debugOut.println("-- end FractalHeap position=" + H5header.this.raf.getFilePointer());
            }
            final int hsize = 8 + 2 * H5header.this.sizeLengths + H5header.this.sizeOffsets;
            if (H5header.debugTracker) {
                H5header.this.memTracker.add("Group FractalHeap (" + forWho + ")", address, pos);
            }
            this.doublingTable = new DoublingTable(this.tableWidth, this.startingBlockSize, this.allocatedManagedSpace, this.maxDirectBlockSize);
            this.rootBlock = new IndirectBlock(this.currentNumRows, this.startingBlockSize);
            if (this.currentNumRows == 0) {
                final DataBlock dblock = new DataBlock();
                this.doublingTable.blockList.add(dblock);
                this.readDirectBlock(H5header.this.getFileOffset(this.rootBlockAddress), address, dblock);
                dblock.size = this.startingBlockSize - dblock.extraBytes;
                this.rootBlock.add(dblock);
            }
            else {
                this.readIndirectBlock(this.rootBlock, H5header.this.getFileOffset(this.rootBlockAddress), address, hasFilters);
                for (final DataBlock dblock2 : this.doublingTable.blockList) {
                    if (dblock2.address > 0L) {
                        this.readDirectBlock(H5header.this.getFileOffset(dblock2.address), address, dblock2);
                        final DataBlock dataBlock = dblock2;
                        dataBlock.size -= dblock2.extraBytes;
                    }
                }
            }
        }
        
        void showDetails(final Formatter f) {
            f.format("FractalHeap version=" + this.version + " heapIdLen=" + this.heapIdLen + " ioFilterLen=" + this.ioFilterLen + " flags= " + this.flags + "\n", new Object[0]);
            f.format(" maxSizeOfObjects=" + this.maxSizeOfObjects + " nextHugeObjectId=" + this.nextHugeObjectId + " btreeAddress=" + this.btreeAddress + " managedSpace=" + this.managedSpace + " allocatedManagedSpace=" + this.allocatedManagedSpace + " freeSpace=" + this.freeSpace + "\n", new Object[0]);
            f.format(" nManagedObjects=" + this.nManagedObjects + " nHugeObjects= " + this.nHugeObjects + " nTinyObjects=" + this.nTinyObjects + " maxDirectBlockSize=" + this.maxDirectBlockSize + " maxHeapSize= 2^" + this.maxHeapSize + "\n", new Object[0]);
            f.format(" rootBlockAddress=" + this.rootBlockAddress + " startingNumRows=" + this.startingNumRows + " currentNumRows=" + this.currentNumRows + "\n\n", new Object[0]);
            this.rootBlock.showDetails(f);
        }
        
        DHeapId getHeapId(final byte[] heapId) throws IOException {
            return new DHeapId(heapId);
        }
        
        void readIndirectBlock(final IndirectBlock iblock, final long pos, final long heapAddress, final boolean hasFilter) throws IOException {
            H5header.this.raf.seek(pos);
            final byte[] heapname = new byte[4];
            H5header.this.raf.read(heapname);
            final String magic = new String(heapname);
            if (!magic.equals("FHIB")) {
                throw new IllegalStateException(magic + " should equal FHIB");
            }
            final byte version = H5header.this.raf.readByte();
            final long heapHeaderAddress = H5header.this.readOffset();
            if (heapAddress != heapHeaderAddress) {
                throw new IllegalStateException();
            }
            int nbytes = this.maxHeapSize / 8;
            if (this.maxHeapSize % 8 != 0) {
                ++nbytes;
            }
            final long blockOffset = H5header.this.readVariableSizeUnsigned(nbytes);
            if (H5header.debugDetail || H5header.debugFractalHeap) {
                H5header.this.debugOut.println(" -- FH IndirectBlock version=" + version + " blockOffset= " + blockOffset);
            }
            final long npos = H5header.this.raf.getFilePointer();
            if (H5header.debugPos) {
                H5header.this.debugOut.println("    *now at position=" + npos);
            }
            long blockSize = this.startingBlockSize;
            for (int row = 0; row < iblock.directRows; ++row) {
                if (row > 1) {
                    blockSize *= 2L;
                }
                for (int i = 0; i < this.doublingTable.tableWidth; ++i) {
                    final DataBlock directBlock = new DataBlock();
                    iblock.add(directBlock);
                    directBlock.address = H5header.this.readOffset();
                    if (hasFilter) {
                        directBlock.sizeFilteredDirectBlock = H5header.this.readLength();
                        directBlock.filterMask = H5header.this.raf.readInt();
                    }
                    if (H5header.debugDetail || H5header.debugFractalHeap) {
                        H5header.this.debugOut.println("  DirectChild " + i + " address= " + directBlock.address);
                    }
                    directBlock.size = blockSize;
                    this.doublingTable.blockList.add(directBlock);
                }
            }
            for (int row = 0; row < iblock.indirectRows; ++row) {
                blockSize *= 2L;
                for (int i = 0; i < this.doublingTable.tableWidth; ++i) {
                    final IndirectBlock iblock2 = new IndirectBlock(-1, blockSize);
                    iblock.add(iblock2);
                    final long childIndirectAddress = H5header.this.readOffset();
                    if (H5header.debugDetail || H5header.debugFractalHeap) {
                        H5header.this.debugOut.println("  InDirectChild " + row + " address= " + childIndirectAddress);
                    }
                    if (childIndirectAddress >= 0L) {
                        this.readIndirectBlock(iblock2, childIndirectAddress, heapAddress, hasFilter);
                    }
                }
            }
        }
        
        void readDirectBlock(final long pos, final long heapAddress, final DataBlock dblock) throws IOException {
            H5header.this.raf.seek(pos);
            final byte[] heapname = new byte[4];
            H5header.this.raf.read(heapname);
            final String magic = new String(heapname);
            if (!magic.equals("FHDB")) {
                throw new IllegalStateException(magic + " should equal FHDB");
            }
            final byte version = H5header.this.raf.readByte();
            final long heapHeaderAddress = H5header.this.readOffset();
            if (heapAddress != heapHeaderAddress) {
                throw new IllegalStateException();
            }
            dblock.extraBytes = 5;
            dblock.extraBytes += (H5header.this.isOffsetLong ? 8 : 4);
            int nbytes = this.maxHeapSize / 8;
            if (this.maxHeapSize % 8 != 0) {
                ++nbytes;
            }
            dblock.offset = H5header.this.readVariableSizeUnsigned(nbytes);
            dblock.dataPos = pos;
            dblock.extraBytes += nbytes;
            if ((this.flags & 0x2) != 0x0) {
                dblock.extraBytes += 4;
            }
            if (H5header.debugDetail || H5header.debugFractalHeap) {
                H5header.this.debugOut.println("  DirectBlock offset= " + dblock.offset + " dataPos = " + dblock.dataPos);
            }
        }
        
        private class DHeapId
        {
            int type;
            int n;
            int m;
            int offset;
            int size;
            
            DHeapId(final byte[] heapId) throws IOException {
                this.type = (heapId[0] & 0x30) >> 4;
                this.n = FractalHeap.this.maxHeapSize / 8;
                this.m = H5header.this.getNumBytesFromMax(FractalHeap.this.maxDirectBlockSize - 1L);
                this.offset = H5header.this.makeIntFromBytes(heapId, 1, this.n);
                this.size = H5header.this.makeIntFromBytes(heapId, 1 + this.n, this.m);
            }
            
            long getPos() {
                return FractalHeap.this.doublingTable.getPos(this.offset);
            }
            
            @Override
            public String toString() {
                return this.type + " " + this.n + " " + this.m + " " + this.offset + " " + this.size;
            }
        }
        
        private class DoublingTable
        {
            int tableWidth;
            long startingBlockSize;
            long managedSpace;
            long maxDirectBlockSize;
            List<DataBlock> blockList;
            
            DoublingTable(final int tableWidth, final long startingBlockSize, final long managedSpace, final long maxDirectBlockSize) {
                this.tableWidth = tableWidth;
                this.startingBlockSize = startingBlockSize;
                this.managedSpace = managedSpace;
                this.maxDirectBlockSize = maxDirectBlockSize;
                this.blockList = new ArrayList<DataBlock>(tableWidth * FractalHeap.this.currentNumRows);
            }
            
            private int calcNrows(final long max) {
                int n = 0;
                long sizeInBytes = 0L;
                long blockSize = this.startingBlockSize;
                while (sizeInBytes < max) {
                    sizeInBytes += blockSize * this.tableWidth;
                    if (++n > 1) {
                        blockSize *= 2L;
                    }
                }
                return n;
            }
            
            private void assignSizes() {
                int block = 0;
                long blockSize = this.startingBlockSize;
                for (final DataBlock db : this.blockList) {
                    db.size = blockSize;
                    if (++block % this.tableWidth == 0 && block / this.tableWidth > 1) {
                        blockSize *= 2L;
                    }
                }
            }
            
            long getPos(final long offset) {
                int block = 0;
                for (final DataBlock db : this.blockList) {
                    if (db.address < 0L) {
                        continue;
                    }
                    if (offset >= db.offset && offset < db.offset + db.size) {
                        final long localOffset = offset - db.offset;
                        return db.dataPos + localOffset;
                    }
                    ++block;
                }
                H5header.log.error("DoublingTable: illegal offset=" + offset);
                return -1L;
            }
            
            void showDetails(final Formatter f) {
                f.format(" DoublingTable: tableWidth= %d startingBlockSize = %d managedSpace=%d maxDirectBlockSize=%d%n", this.tableWidth, this.startingBlockSize, this.managedSpace, this.maxDirectBlockSize);
                f.format(" DataBlocks:\n", new Object[0]);
                f.format("  address            dataPos            offset size\n", new Object[0]);
                for (final DataBlock dblock : this.blockList) {
                    f.format("  %#-18x %#-18x %5d  %4d%n", dblock.address, dblock.dataPos, dblock.offset, dblock.size);
                }
            }
        }
        
        private class IndirectBlock
        {
            long size;
            int nrows;
            int directRows;
            int indirectRows;
            List<DataBlock> directBlocks;
            List<IndirectBlock> indirectBlocks;
            
            IndirectBlock(int nrows, final long iblock_size) {
                this.nrows = nrows;
                this.size = iblock_size;
                if (nrows < 0) {
                    final double n = SpecialMathFunction.log2((double)iblock_size) - SpecialMathFunction.log2((double)(FractalHeap.this.startingBlockSize * FractalHeap.this.tableWidth)) + 1.0;
                    nrows = (int)n;
                }
                final int maxrows_directBlocks = (int)(SpecialMathFunction.log2((double)FractalHeap.this.maxDirectBlockSize) - SpecialMathFunction.log2((double)FractalHeap.this.startingBlockSize)) + 2;
                if (nrows < maxrows_directBlocks) {
                    this.directRows = nrows;
                    this.indirectRows = 0;
                }
                else {
                    this.directRows = maxrows_directBlocks;
                    this.indirectRows = nrows - maxrows_directBlocks;
                }
                if (H5header.debugFractalHeap) {
                    FractalHeap.this.this$0.debugOut.println("  readIndirectBlock directChildren" + this.directRows + " indirectChildren= " + this.indirectRows);
                }
            }
            
            void add(final DataBlock dblock) {
                if (this.directBlocks == null) {
                    this.directBlocks = new ArrayList<DataBlock>();
                }
                this.directBlocks.add(dblock);
            }
            
            void add(final IndirectBlock iblock) {
                if (this.indirectBlocks == null) {
                    this.indirectBlocks = new ArrayList<IndirectBlock>();
                }
                this.indirectBlocks.add(iblock);
            }
            
            void showDetails(final Formatter f) {
                f.format("%n IndirectBlock: nrows= %d directRows = %d indirectRows=%d startingSize=%d%n", this.nrows, this.directRows, this.indirectRows, this.size);
                f.format(" DataBlocks:\n", new Object[0]);
                f.format("  address            dataPos            offset size end\n", new Object[0]);
                if (this.directBlocks != null) {
                    for (final DataBlock dblock : this.directBlocks) {
                        f.format("  %#-18x %#-18x %5d  %4d %5d %n", dblock.address, dblock.dataPos, dblock.offset, dblock.size, dblock.offset + dblock.size);
                    }
                }
                if (this.indirectBlocks != null) {
                    for (final IndirectBlock iblock : this.indirectBlocks) {
                        iblock.showDetails(f);
                    }
                }
            }
        }
        
        private class DataBlock
        {
            long address;
            long sizeFilteredDirectBlock;
            int filterMask;
            long dataPos;
            long offset;
            long size;
            int extraBytes;
        }
    }
    
    private class MemTracker
    {
        private List<Mem> memList;
        private StringBuilder sbuff;
        private long fileSize;
        
        MemTracker(final long fileSize) {
            this.memList = new ArrayList<Mem>();
            this.sbuff = new StringBuilder();
            this.fileSize = fileSize;
        }
        
        void add(final String name, final long start, final long end) {
            this.memList.add(new Mem(name, start, end));
        }
        
        void addByLen(final String name, final long start, final long size) {
            this.memList.add(new Mem(name, start, start + size));
        }
        
        void report() {
            H5header.this.debugOut.println("Memory used file size= " + this.fileSize);
            H5header.this.debugOut.println("  start    end   size   name");
            Collections.sort(this.memList);
            Mem prev = null;
            for (final Mem m : this.memList) {
                if (prev != null && m.start > prev.end) {
                    this.doOne('+', prev.end, m.start, m.start - prev.end, "HOLE");
                }
                final char c = (prev != null && prev.end != m.start) ? '*' : ' ';
                this.doOne(c, m.start, m.end, m.end - m.start, m.name);
                prev = m;
            }
            H5header.this.debugOut.println();
        }
        
        private void doOne(final char c, final long start, final long end, final long size, final String name) {
            this.sbuff.setLength(0);
            this.sbuff.append(c);
            this.sbuff.append(Format.l(start, 6));
            this.sbuff.append(" ");
            this.sbuff.append(Format.l(end, 6));
            this.sbuff.append(" ");
            this.sbuff.append(Format.l(size, 6));
            this.sbuff.append(" ");
            this.sbuff.append(name);
            H5header.this.debugOut.println(this.sbuff.toString());
        }
        
        class Mem implements Comparable
        {
            public String name;
            public long start;
            public long end;
            
            public Mem(final String name, final long start, final long end) {
                this.name = name;
                this.start = start;
                this.end = end;
            }
            
            public int compareTo(final Object o1) {
                final Mem m = (Mem)o1;
                return (int)(this.start - m.start);
            }
        }
    }
    
    interface Named
    {
        String getName();
    }
}
