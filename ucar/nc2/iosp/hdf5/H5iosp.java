// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.iosp.hdf5;

import org.slf4j.LoggerFactory;
import java.io.OutputStream;
import java.io.ByteArrayOutputStream;
import ucar.ma2.StructureDataW;
import ucar.ma2.StructureData;
import ucar.ma2.ArrayStructureW;
import java.util.Iterator;
import ucar.ma2.ArrayStructureBB;
import java.nio.ByteBuffer;
import ucar.ma2.StructureMembers;
import ucar.ma2.ArrayStructure;
import ucar.ma2.Index;
import ucar.ma2.ArrayObject;
import java.util.Date;
import ucar.nc2.iosp.Layout;
import ucar.nc2.Structure;
import ucar.nc2.iosp.LayoutRegular;
import ucar.nc2.iosp.LayoutBB;
import java.nio.ByteOrder;
import ucar.ma2.DataType;
import ucar.nc2.iosp.IospHelper;
import ucar.ma2.InvalidRangeException;
import ucar.ma2.Array;
import ucar.ma2.Section;
import ucar.nc2.Variable;
import ucar.nc2.Group;
import ucar.nc2.iosp.hdf4.HdfEos;
import java.io.PrintStream;
import ucar.nc2.util.CancelTask;
import ucar.nc2.NetcdfFile;
import java.io.IOException;
import ucar.nc2.iosp.hdf4.H4header;
import ucar.nc2.util.DebugFlags;
import ucar.unidata.io.RandomAccessFile;
import org.slf4j.Logger;
import ucar.nc2.iosp.AbstractIOServiceProvider;

public class H5iosp extends AbstractIOServiceProvider
{
    static boolean debug;
    static boolean debugPos;
    static boolean debugHeap;
    static boolean debugFilter;
    static boolean debugFilterDetails;
    static boolean debugString;
    static boolean debugFilterIndexer;
    static boolean debugChunkIndexer;
    static boolean debugVlen;
    static boolean debugStructure;
    private static Logger log;
    private RandomAccessFile myRaf;
    private H5header headerParser;
    
    public static void setDebugFlags(final DebugFlags debugFlag) {
        H5iosp.debug = debugFlag.isSet("H5iosp/read");
        H5iosp.debugPos = debugFlag.isSet("H5iosp/filePos");
        H5iosp.debugHeap = debugFlag.isSet("H5iosp/Heap");
        H5iosp.debugFilter = debugFlag.isSet("H5iosp/filter");
        H5iosp.debugFilterIndexer = debugFlag.isSet("H5iosp/filterIndexer");
        H5iosp.debugChunkIndexer = debugFlag.isSet("H5iosp/chunkIndexer");
        H5iosp.debugVlen = debugFlag.isSet("H5iosp/vlen");
        H5header.setDebugFlags(debugFlag);
        H4header.setDebugFlags(debugFlag);
    }
    
    public boolean isValidFile(final RandomAccessFile raf) throws IOException {
        return H5header.isValidFile(raf);
    }
    
    public String getFileTypeId() {
        return "HDF5";
    }
    
    public String getFileTypeDescription() {
        return "Hierarchical Data Format, version 5";
    }
    
    @Override
    public void open(final RandomAccessFile raf, final NetcdfFile ncfile, final CancelTask cancelTask) throws IOException {
        this.myRaf = raf;
        (this.headerParser = new H5header(this.myRaf, ncfile, this)).read(null);
        final Group eosInfo = ncfile.getRootGroup().findGroup("HDFEOS INFORMATION");
        if (eosInfo != null) {
            HdfEos.amendFromODL(ncfile, eosInfo);
        }
        ncfile.finish();
    }
    
    public Array readData(final Variable v2, final Section section) throws IOException, InvalidRangeException {
        final H5header.Vinfo vinfo = (H5header.Vinfo)v2.getSPobject();
        return this.readData(v2, vinfo.dataPos, section);
    }
    
    private Array readData(final Variable v2, final long dataPos, Section wantSection) throws IOException, InvalidRangeException {
        final H5header.Vinfo vinfo = (H5header.Vinfo)v2.getSPobject();
        final DataType dataType = v2.getDataType();
        if (vinfo.useFillValue) {
            Object pa = IospHelper.makePrimitiveArray((int)wantSection.computeSize(), dataType, vinfo.getFillValue());
            if (dataType == DataType.CHAR) {
                pa = IospHelper.convertByteToChar((byte[])pa);
            }
            return Array.factory(dataType.getPrimitiveClassType(), wantSection.getShape(), pa);
        }
        Layout layout;
        Object data;
        if (vinfo.mfp != null) {
            if (H5iosp.debugFilter) {
                System.out.println("read variable filtered " + v2.getName() + " vinfo = " + vinfo);
            }
            assert vinfo.isChunked;
            final ByteOrder bo = (vinfo.typeInfo.byteOrder == 0) ? ByteOrder.BIG_ENDIAN : ByteOrder.LITTLE_ENDIAN;
            layout = new H5tiledLayoutBB(v2, wantSection, this.myRaf, vinfo.mfp.getFilters(), bo);
            data = IospHelper.readDataFill((LayoutBB)layout, v2.getDataType(), vinfo.getFillValue());
        }
        else {
            if (H5iosp.debug) {
                System.out.println("read variable " + v2.getName() + " vinfo = " + vinfo);
            }
            DataType readDtype = v2.getDataType();
            int elemSize = v2.getElementSize();
            Object fillValue = vinfo.getFillValue();
            int byteOrder = vinfo.typeInfo.byteOrder;
            wantSection = Section.fill(wantSection, v2.getShape());
            if (vinfo.typeInfo.hdfType == 2) {
                readDtype = vinfo.mdt.timeType;
                elemSize = readDtype.getSize();
                fillValue = vinfo.getFillValueDefault(readDtype);
            }
            else if (vinfo.typeInfo.hdfType == 8) {
                final H5header.TypeInfo baseInfo = vinfo.typeInfo.base;
                readDtype = baseInfo.dataType;
                elemSize = readDtype.getSize();
                fillValue = vinfo.getFillValueDefault(readDtype);
                byteOrder = baseInfo.byteOrder;
            }
            else if (vinfo.typeInfo.hdfType == 9) {
                elemSize = vinfo.typeInfo.byteSize;
                byteOrder = vinfo.typeInfo.byteOrder;
                wantSection = wantSection.removeVlen();
            }
            if (vinfo.isChunked) {
                layout = new H5tiledLayout((H5header.Vinfo)v2.getSPobject(), readDtype, wantSection);
            }
            else {
                layout = new LayoutRegular(dataPos, elemSize, v2.getShape(), wantSection);
            }
            data = this.readData(vinfo, v2, layout, readDtype, wantSection.getShape(), fillValue, byteOrder);
        }
        if (data instanceof Array) {
            return (Array)data;
        }
        if (dataType == DataType.STRUCTURE) {
            return this.convertStructure((Structure)v2, layout, wantSection.getShape(), (byte[])data);
        }
        return Array.factory(dataType.getPrimitiveClassType(), wantSection.getShape(), data);
    }
    
    private Object readData(final H5header.Vinfo vinfo, final Variable v, final Layout layout, final DataType dataType, final int[] shape, final Object fillValue, final int byteOrder) throws IOException, InvalidRangeException {
        final H5header.TypeInfo typeInfo = vinfo.typeInfo;
        if (typeInfo.hdfType == 2) {
            final Object data = IospHelper.readDataFill(this.myRaf, layout, dataType, fillValue, byteOrder, true);
            final Array timeArray = Array.factory(dataType.getPrimitiveClassType(), shape, data);
            final String[] stringData = new String[(int)timeArray.getSize()];
            int count = 0;
            while (timeArray.hasNext()) {
                final long time = timeArray.nextLong();
                stringData[count++] = this.headerParser.formatter.toDateTimeStringISO(new Date(time));
            }
            return Array.factory(String.class, shape, stringData);
        }
        if (typeInfo.hdfType == 8) {
            final Object data = IospHelper.readDataFill(this.myRaf, layout, dataType, fillValue, byteOrder);
            return Array.factory(dataType.getPrimitiveClassType(), shape, data);
        }
        if (typeInfo.hdfType == 9 && !typeInfo.isVString) {
            DataType readType = dataType;
            if (typeInfo.isVString) {
                readType = DataType.BYTE;
            }
            else if (typeInfo.base.hdfType == 7) {
                readType = DataType.LONG;
            }
            final boolean scalar = layout.getTotalNelems() == 1L;
            final Object[] data2 = new Object[(int)layout.getTotalNelems()];
            int count = 0;
            while (layout.hasNext()) {
                final Layout.Chunk chunk = layout.next();
                if (chunk == null) {
                    continue;
                }
                for (int i = 0; i < chunk.getNelems(); ++i) {
                    final long address = chunk.getSrcPos() + layout.getElemSize() * i;
                    final Array vlenArray = this.headerParser.getHeapDataArray(address, readType, byteOrder);
                    data2[count++] = ((typeInfo.base.hdfType == 7) ? this.convertReference(vlenArray) : vlenArray);
                }
            }
            return scalar ? data2[0] : new ArrayObject(data2[0].getClass(), shape, data2);
        }
        if (dataType == DataType.STRUCTURE) {
            final int recsize = layout.getElemSize();
            final long size = recsize * layout.getTotalNelems();
            final byte[] byteArray = new byte[(int)size];
            while (layout.hasNext()) {
                final Layout.Chunk chunk = layout.next();
                if (chunk == null) {
                    continue;
                }
                if (H5iosp.debugStructure) {
                    System.out.println(" readStructure " + v.getName() + " chunk= " + chunk + " index.getElemSize= " + layout.getElemSize());
                }
                this.myRaf.seek(chunk.getSrcPos());
                this.myRaf.read(byteArray, (int)chunk.getDestElem() * recsize, chunk.getNelems() * recsize);
            }
            return this.convertStructure((Structure)v, layout, shape, byteArray);
        }
        return this.readDataPrimitive(layout, dataType, shape, fillValue, byteOrder, true);
    }
    
    Array convertReference(final Array refArray) throws IOException {
        final int nelems = (int)refArray.getSize();
        final Index ima = refArray.getIndex();
        final String[] result = new String[nelems];
        for (int i = 0; i < nelems; ++i) {
            final long reference = refArray.getLong(ima.set(i));
            result[i] = this.headerParser.getDataObjectName(reference);
            if (H5iosp.debugVlen) {
                System.out.printf(" convertReference 0x%x to %s %n", reference, result[i]);
            }
        }
        return Array.factory(String.class, new int[] { nelems }, result);
    }
    
    private ArrayStructure convertStructure(final Structure s, final Layout layout, final int[] shape, final byte[] byteArray) throws IOException {
        boolean hasStrings = false;
        final StructureMembers sm = s.makeStructureMembers();
        for (final StructureMembers.Member m : sm.getMembers()) {
            final Variable v2 = s.findVariable(m.getName());
            final H5header.Vinfo vm = (H5header.Vinfo)v2.getSPobject();
            if (vm.typeInfo.byteOrder >= 0) {
                m.setDataObject((vm.typeInfo.byteOrder == 1) ? ByteOrder.LITTLE_ENDIAN : ByteOrder.BIG_ENDIAN);
            }
            m.setDataParam((int)vm.dataPos);
            if (v2.getDataType() == DataType.STRING) {
                hasStrings = true;
            }
        }
        final int recsize = layout.getElemSize();
        sm.setStructureSize(recsize);
        final ByteBuffer bb = ByteBuffer.wrap(byteArray);
        final ArrayStructureBB asbb = new ArrayStructureBB(sm, shape, bb, 0);
        if (hasStrings) {
            int destPos = 0;
            for (int i = 0; i < layout.getTotalNelems(); ++i) {
                this.convertStrings(asbb, destPos, sm);
                destPos += layout.getElemSize();
            }
        }
        return asbb;
    }
    
    void convertStrings(final ArrayStructureBB asbb, final int pos, final StructureMembers sm) throws IOException {
        final ByteBuffer bb = asbb.getByteBuffer();
        for (final StructureMembers.Member m : sm.getMembers()) {
            if (m.getDataType() == DataType.STRING) {
                m.setDataObject(ByteOrder.nativeOrder());
                final int size = m.getSize();
                final int destPos = pos + m.getDataParam();
                final String[] result = new String[size];
                for (int i = 0; i < size; ++i) {
                    result[i] = this.headerParser.readHeapString(bb, destPos + i * 16);
                }
                final int index = asbb.addObjectToHeap(result);
                bb.order(ByteOrder.nativeOrder());
                bb.putInt(destPos, index);
            }
        }
    }
    
    Object readDataPrimitive(final Layout layout, final DataType dataType, final int[] shape, final Object fillValue, final int byteOrder, final boolean convertChar) throws IOException, InvalidRangeException {
        if (dataType == DataType.STRING) {
            final int size = (int)layout.getTotalNelems();
            final String[] sa = new String[size];
            int count = 0;
            while (layout.hasNext()) {
                final Layout.Chunk chunk = layout.next();
                if (chunk == null) {
                    continue;
                }
                for (int i = 0; i < chunk.getNelems(); ++i) {
                    sa[count++] = this.headerParser.readHeapString(chunk.getSrcPos() + layout.getElemSize() * i);
                }
            }
            return sa;
        }
        if (dataType != DataType.OPAQUE) {
            return IospHelper.readDataFill(this.myRaf, layout, dataType, fillValue, byteOrder, convertChar);
        }
        final Array opArray = new ArrayObject(ByteBuffer.class, shape);
        assert new Section(shape).computeSize() == layout.getTotalNelems();
        int count2 = 0;
        while (layout.hasNext()) {
            final Layout.Chunk chunk2 = layout.next();
            if (chunk2 == null) {
                continue;
            }
            final int recsize = layout.getElemSize();
            for (int i = 0; i < chunk2.getNelems(); ++i) {
                final byte[] pa = new byte[recsize];
                this.myRaf.seek(chunk2.getSrcPos() + i * recsize);
                this.myRaf.read(pa, 0, recsize);
                opArray.setObject(count2++, ByteBuffer.wrap(pa));
            }
        }
        return opArray;
    }
    
    private StructureData readStructure(final Structure s, final ArrayStructureW asw, final long dataPos) throws IOException, InvalidRangeException {
        final StructureDataW sdata = new StructureDataW(asw.getStructureMembers());
        if (H5iosp.debug) {
            System.out.println(" readStructure " + s.getName() + " dataPos = " + dataPos);
        }
        for (final Variable v2 : s.getVariables()) {
            final H5header.Vinfo vinfo = (H5header.Vinfo)v2.getSPobject();
            if (H5iosp.debug) {
                System.out.println(" readStructureMember " + v2.getName() + " vinfo = " + vinfo);
            }
            final Array dataArray = this.readData(v2, dataPos + vinfo.dataPos, v2.getShapeAsSection());
            sdata.setMemberData(v2.getShortName(), dataArray);
        }
        return sdata;
    }
    
    @Override
    public void close() throws IOException {
        if (this.myRaf != null) {
            this.myRaf.close();
        }
        this.myRaf = null;
        this.headerParser.close();
    }
    
    @Override
    public String toStringDebug(final Object o) {
        if (o instanceof Variable) {
            final Variable v = (Variable)o;
            final H5header.Vinfo vinfo = (H5header.Vinfo)v.getSPobject();
            return vinfo.toString();
        }
        return null;
    }
    
    @Override
    public String getDetailInfo() {
        final ByteArrayOutputStream ff = new ByteArrayOutputStream(100000);
        try {
            final NetcdfFile ncfile = new FakeNetcdfFile();
            final H5header detailParser = new H5header(this.myRaf, ncfile, this);
            detailParser.read(new PrintStream(ff));
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        return ff.toString();
    }
    
    @Override
    public Object sendIospMessage(final Object message) {
        if (message.toString().equals("header")) {
            return this.headerParser;
        }
        if (message.toString().equals("headerEmpty")) {
            final NetcdfFile ncfile = new FakeNetcdfFile();
            final H5header headerEmpty = new H5header(this.myRaf, ncfile, this);
            return headerEmpty;
        }
        return super.sendIospMessage(message);
    }
    
    static {
        H5iosp.debug = false;
        H5iosp.debugPos = false;
        H5iosp.debugHeap = false;
        H5iosp.debugFilter = false;
        H5iosp.debugFilterDetails = false;
        H5iosp.debugString = false;
        H5iosp.debugFilterIndexer = false;
        H5iosp.debugChunkIndexer = false;
        H5iosp.debugVlen = false;
        H5iosp.debugStructure = false;
        H5iosp.log = LoggerFactory.getLogger(H5iosp.class);
    }
    
    private static class FakeNetcdfFile extends NetcdfFile
    {
    }
}
