// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.iosp.hdf5;

public class Tiling
{
    private int rank;
    private int[] shape;
    private int[] tileSize;
    private int[] tile;
    private int[] stride;
    
    public Tiling(final int[] shape, final int[] tileSize) {
        assert shape.length <= tileSize.length;
        this.rank = shape.length;
        this.tileSize = tileSize;
        this.shape = new int[this.rank];
        for (int i = 0; i < this.rank; ++i) {
            this.shape[i] = Math.max(shape[i], tileSize[i]);
        }
        this.tile = this.tile(this.shape);
        this.stride = new int[this.rank];
        int strider = 1;
        for (int k = this.rank - 1; k >= 0; --k) {
            this.stride[k] = strider;
            strider *= this.tile[k];
        }
    }
    
    public int[] tile(final int[] pt) {
        final int[] tile = new int[this.rank];
        for (int i = 0; i < this.rank; ++i) {
            if (this.shape[i] < pt[i]) {
                System.out.println("HEY shape[i] < pt[i]");
            }
            assert this.shape[i] >= pt[i];
            tile[i] = pt[i] / this.tileSize[i];
        }
        return tile;
    }
    
    public int order(final int[] pt) {
        final int[] tile = this.tile(pt);
        int order = 0;
        for (int i = 0; i < this.rank; ++i) {
            order += this.stride[i] * tile[i];
        }
        return order;
    }
    
    public int compare(final int[] p1, final int[] p2) {
        return this.order(p1) - this.order(p2);
    }
}
