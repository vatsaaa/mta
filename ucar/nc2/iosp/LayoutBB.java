// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.iosp;

import java.nio.LongBuffer;
import java.nio.DoubleBuffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.nio.ShortBuffer;
import java.nio.ByteBuffer;
import java.io.IOException;

public interface LayoutBB extends Layout
{
    long getTotalNelems();
    
    int getElemSize();
    
    boolean hasNext();
    
    Chunk next() throws IOException;
    
    public interface Chunk extends Layout.Chunk
    {
        int getSrcElem();
        
        ByteBuffer getByteBuffer();
        
        ShortBuffer getShortBuffer();
        
        IntBuffer getIntBuffer();
        
        FloatBuffer getFloatBuffer();
        
        DoubleBuffer getDoubleBuffer();
        
        LongBuffer getLongBuffer();
        
        int getNelems();
        
        long getDestElem();
    }
}
