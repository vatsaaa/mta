// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2;

import java.util.EnumSet;
import org.slf4j.LoggerFactory;
import ucar.ma2.ArrayChar;
import ucar.ma2.ArrayObject;
import ucar.ma2.Section;
import ucar.ma2.InvalidRangeException;
import java.io.File;
import java.util.Iterator;
import java.util.StringTokenizer;
import java.util.List;
import java.util.Collection;
import java.util.Arrays;
import java.util.ArrayList;
import ucar.ma2.Array;
import ucar.nc2.iosp.netcdf3.N3iosp;
import ucar.nc2.util.CancelTask;
import ucar.nc2.iosp.netcdf3.SPFactory;
import ucar.unidata.io.RandomAccessFile;
import java.io.IOException;
import ucar.nc2.iosp.IOServiceProviderWriter;
import ucar.ma2.DataType;
import java.util.Set;
import org.slf4j.Logger;

public class NetcdfFileWriteable extends NetcdfFile
{
    private static Logger log;
    private static Set<DataType> valid;
    private IOServiceProviderWriter spiw;
    private boolean defineMode;
    private boolean isNewFile;
    private boolean isLargeFile;
    private boolean fill;
    private int extraHeader;
    private long preallocateSize;
    
    public static NetcdfFileWriteable openExisting(final String location) throws IOException {
        return new NetcdfFileWriteable(location, true, true);
    }
    
    public static NetcdfFileWriteable openExisting(final String location, final boolean fill) throws IOException {
        return new NetcdfFileWriteable(location, fill, true);
    }
    
    public static NetcdfFileWriteable createNew(final String location) throws IOException {
        return new NetcdfFileWriteable(location, true, false);
    }
    
    public static NetcdfFileWriteable createNew(final String location, final boolean fill) throws IOException {
        return new NetcdfFileWriteable(location, fill, false);
    }
    
    private NetcdfFileWriteable(final String location, final boolean fill, final boolean isExisting) throws IOException {
        this.location = location;
        this.fill = fill;
        if (isExisting) {
            final RandomAccessFile raf = new RandomAccessFile(location, "rw");
            this.spi = SPFactory.getServiceProvider();
            (this.spiw = (IOServiceProviderWriter)this.spi).open(raf, this, null);
            this.spiw.setFill(fill);
        }
        else {
            this.defineMode = true;
            this.isNewFile = true;
        }
    }
    
    public void setFill(final boolean fill) {
        this.fill = fill;
        if (this.spiw != null) {
            this.spiw.setFill(fill);
        }
    }
    
    public void setLength(final long size) {
        if (!this.defineMode) {
            throw new UnsupportedOperationException("not in define mode");
        }
        this.preallocateSize = size;
    }
    
    public void setLargeFile(final boolean isLargeFile) {
        if (!this.defineMode) {
            throw new UnsupportedOperationException("not in define mode");
        }
        this.isLargeFile = isLargeFile;
    }
    
    public void setExtraHeaderBytes(final int extraHeaderBytes) {
        if (!this.defineMode) {
            throw new UnsupportedOperationException("not in define mode");
        }
        this.extraHeader = extraHeaderBytes;
    }
    
    public boolean isDefineMode() {
        return this.defineMode;
    }
    
    public Dimension addDimension(final String dimName, final int length) {
        if (!this.defineMode) {
            throw new UnsupportedOperationException("not in define mode");
        }
        if (length <= 0) {
            throw new IllegalArgumentException("dimension length must be > 0 :" + length);
        }
        if (!N3iosp.isValidNetcdf3ObjectName(dimName)) {
            throw new IllegalArgumentException("illegal netCDF-3 dimension name: " + dimName);
        }
        final Dimension dim = new Dimension(dimName, length, true, false, false);
        super.addDimension(null, dim);
        return dim;
    }
    
    public Dimension addDimension(final String dimName, final int length, final boolean isShared, final boolean isUnlimited, final boolean isVariableLength) {
        if (!this.defineMode) {
            throw new UnsupportedOperationException("not in define mode");
        }
        if (!N3iosp.isValidNetcdf3ObjectName(dimName)) {
            throw new IllegalArgumentException("illegal netCDF-3 dimension name " + dimName);
        }
        final Dimension dim = new Dimension(dimName, length, isShared, isUnlimited, isVariableLength);
        super.addDimension(null, dim);
        return dim;
    }
    
    public Dimension addUnlimitedDimension(final String dimName) {
        return this.addDimension(dimName, 0, true, true, false);
    }
    
    public Dimension renameDimension(final String oldName, final String newName) {
        if (!this.defineMode) {
            throw new UnsupportedOperationException("not in define mode");
        }
        final Dimension dim = this.findDimension(oldName);
        if (null != dim) {
            dim.setName(newName);
        }
        return dim;
    }
    
    public Attribute addGlobalAttribute(Attribute att) {
        if (!this.defineMode) {
            throw new UnsupportedOperationException("not in define mode");
        }
        if (!N3iosp.isValidNetcdf3ObjectName(att.getName())) {
            final String attName = N3iosp.createValidNetcdf3ObjectName(att.getName());
            NetcdfFileWriteable.log.warn("illegal netCDF-3 attribute name= " + att.getName() + " change to " + attName);
            att = new Attribute(attName, att.getValues());
        }
        return super.addAttribute(null, att);
    }
    
    public Attribute addGlobalAttribute(final String name, final String value) {
        return this.addGlobalAttribute(new Attribute(name, value));
    }
    
    public Attribute addGlobalAttribute(final String name, final Number value) {
        return this.addGlobalAttribute(new Attribute(name, value));
    }
    
    public Attribute addGlobalAttribute(final String name, final Array values) {
        return this.addGlobalAttribute(new Attribute(name, values));
    }
    
    public Attribute deleteGlobalAttribute(final String attName) {
        if (!this.defineMode) {
            throw new UnsupportedOperationException("not in define mode");
        }
        final Attribute att = this.findGlobalAttribute(attName);
        if (null == att) {
            return null;
        }
        this.rootGroup.remove(att);
        return att;
    }
    
    public Attribute renameGlobalAttribute(final String oldName, final String newName) {
        if (!this.defineMode) {
            throw new UnsupportedOperationException("not in define mode");
        }
        Attribute att = this.findGlobalAttribute(oldName);
        if (null == att) {
            return null;
        }
        this.rootGroup.remove(att);
        att = new Attribute(newName, att.getValues());
        this.rootGroup.addAttribute(att);
        return att;
    }
    
    public Variable addVariable(final String varName, final DataType dataType, final Dimension[] dims) {
        final ArrayList<Dimension> list = new ArrayList<Dimension>();
        list.addAll(Arrays.asList(dims));
        return this.addVariable(varName, dataType, list);
    }
    
    public Variable addVariable(final String varName, final DataType dataType, final String dims) {
        final ArrayList<Dimension> list = new ArrayList<Dimension>();
        final StringTokenizer stoker = new StringTokenizer(dims);
        while (stoker.hasMoreTokens()) {
            final String tok = stoker.nextToken();
            final Dimension d = this.rootGroup.findDimension(tok);
            if (null == d) {
                throw new IllegalArgumentException("Cant find dimension " + tok);
            }
            list.add(d);
        }
        return this.addVariable(varName, dataType, list);
    }
    
    public Variable addVariable(final String varName, final DataType dataType, final List<Dimension> dims) {
        if (!this.defineMode) {
            throw new UnsupportedOperationException("not in define mode");
        }
        if (!N3iosp.isValidNetcdf3ObjectName(varName)) {
            throw new IllegalArgumentException("illegal netCDF-3 variable name: " + varName);
        }
        if (!NetcdfFileWriteable.valid.contains(dataType)) {
            throw new IllegalArgumentException("illegal dataType for netcdf-3 format: " + dataType);
        }
        int count = 0;
        for (final Dimension d : dims) {
            if (d.isUnlimited() && count != 0) {
                throw new IllegalArgumentException("Unlimited dimension " + d + " must be first instead its  =" + count);
            }
            ++count;
        }
        final Variable v = new Variable(this, this.rootGroup, null, varName);
        v.setDataType(dataType);
        v.setDimensions(dims);
        final long size = v.getSize() * v.getElementSize();
        if (size > 4294967292L) {
            throw new IllegalArgumentException("Variable size in bytes " + size + " may not exceed " + 4294967292L);
        }
        super.addVariable(null, v);
        return v;
    }
    
    public Variable addStringVariable(final String varName, final List<Dimension> dims, final int max_strlen) {
        if (!this.defineMode) {
            throw new UnsupportedOperationException("not in define mode");
        }
        if (!N3iosp.isValidNetcdf3ObjectName(varName)) {
            throw new IllegalArgumentException("illegal netCDF-3 variable name: " + varName);
        }
        final Variable v = new Variable(this, this.rootGroup, null, varName);
        v.setDataType(DataType.CHAR);
        final Dimension d = this.addDimension(varName + "_strlen", max_strlen);
        final ArrayList<Dimension> sdims = new ArrayList<Dimension>(dims);
        sdims.add(d);
        v.setDimensions(sdims);
        super.addVariable(null, v);
        return v;
    }
    
    public Variable renameVariable(final String oldName, final String newName) {
        if (!this.defineMode) {
            throw new UnsupportedOperationException("not in define mode");
        }
        final Variable v = this.findVariable(oldName);
        if (null != v) {
            v.setName(newName);
        }
        return v;
    }
    
    public void addVariableAttribute(final String varName, Attribute att) {
        if (!this.defineMode) {
            throw new UnsupportedOperationException("not in define mode");
        }
        if (!N3iosp.isValidNetcdf3ObjectName(att.getName())) {
            final String attName = N3iosp.createValidNetcdf3ObjectName(att.getName());
            NetcdfFileWriteable.log.warn("illegal netCDF-3 attribute name= " + att.getName() + " change to " + attName);
            att = new Attribute(attName, att.getValues());
        }
        final Variable v = this.rootGroup.findVariable(varName);
        if (null == v) {
            throw new IllegalArgumentException("addVariableAttribute variable name not found = <" + varName + ">");
        }
        v.addAttribute(att);
    }
    
    public void addVariableAttribute(final String varName, final String attName, final String value) {
        this.addVariableAttribute(varName, new Attribute(attName, value));
    }
    
    public void addVariableAttribute(final String varName, final String attName, final Number value) {
        this.addVariableAttribute(varName, new Attribute(attName, value));
    }
    
    public void addVariableAttribute(final String varName, final String attName, final Array value) {
        final Attribute att = new Attribute(attName, value);
        this.addVariableAttribute(varName, att);
    }
    
    public Attribute deleteVariableAttribute(final String varName, final String attName) {
        if (!this.defineMode) {
            throw new UnsupportedOperationException("not in define mode");
        }
        final Variable v = this.findVariable(varName);
        if (v == null) {
            return null;
        }
        final Attribute att = v.findAttribute(attName);
        if (null == att) {
            return null;
        }
        v.remove(att);
        return att;
    }
    
    public Attribute renameVariableAttribute(final String varName, final String attName, final String newName) {
        if (!this.defineMode) {
            throw new UnsupportedOperationException("not in define mode");
        }
        final Variable v = this.findVariable(varName);
        if (v == null) {
            return null;
        }
        Attribute att = v.findAttribute(attName);
        if (null == att) {
            return null;
        }
        v.remove(att);
        att = new Attribute(newName, att.getValues());
        v.addAttribute(att);
        return att;
    }
    
    public void updateAttribute(final Variable v2, final Attribute att) throws IOException {
        if (this.defineMode) {
            throw new UnsupportedOperationException("in define mode");
        }
        this.spiw.updateAttribute(v2, att);
    }
    
    public void create() throws IOException {
        if (!this.defineMode) {
            throw new UnsupportedOperationException("not in define mode");
        }
        this.spi = SPFactory.getServiceProvider();
        (this.spiw = (IOServiceProviderWriter)this.spi).setFill(this.fill);
        this.spiw.create(this.location, this, this.extraHeader, this.preallocateSize, this.isLargeFile);
        this.defineMode = false;
    }
    
    public boolean setRedefineMode(final boolean redefineMode) throws IOException {
        if (redefineMode && !this.defineMode) {
            this.defineMode = true;
        }
        else if (!redefineMode && this.defineMode) {
            this.defineMode = false;
            this.finish();
            final boolean ok = this.spiw.rewriteHeader(this.isLargeFile);
            if (!ok) {
                this.rewrite();
            }
            return !ok;
        }
        return false;
    }
    
    private void rewrite() throws IOException {
        this.spiw.flush();
        this.spiw.close();
        final File prevFile = new File(this.location);
        final File tmpFile = new File(this.location + ".tmp");
        if (tmpFile.exists()) {
            tmpFile.delete();
        }
        if (!prevFile.renameTo(tmpFile)) {
            System.out.println(prevFile.getPath() + " prevFile.exists " + prevFile.exists() + " canRead = " + prevFile.canRead());
            System.out.println(tmpFile.getPath() + " tmpFile.exists " + tmpFile.exists() + " canWrite " + tmpFile.canWrite());
            throw new RuntimeException("Cant rename " + prevFile.getAbsolutePath() + " to " + tmpFile.getAbsolutePath());
        }
        final NetcdfFile oldFile = NetcdfFile.open(tmpFile.getPath());
        Structure recordVar = null;
        if (oldFile.hasUnlimitedDimension()) {
            oldFile.sendIospMessage("AddRecordStructure");
            recordVar = (Structure)oldFile.findVariable("record");
        }
        this.spiw.create(this.location, this, this.extraHeader, this.preallocateSize, this.isLargeFile);
        this.spiw.setFill(this.fill);
        if (recordVar != null) {
            final Boolean result = (Boolean)this.spiw.sendIospMessage("AddRecordStructure");
            if (!result) {
                recordVar = null;
            }
        }
        final List<Variable> oldList = new ArrayList<Variable>(this.getVariables().size());
        for (final Variable v : this.getVariables()) {
            final Variable oldVar = oldFile.findVariable(v.getName());
            if (oldVar != null) {
                oldList.add(oldVar);
            }
        }
        FileWriter.copyVarData(this, oldList, recordVar, 0L);
        this.flush();
        oldFile.close();
        if (!tmpFile.delete()) {
            throw new RuntimeException("Cant delete " + this.location);
        }
    }
    
    public void write(final String varName, final Array values) throws IOException, InvalidRangeException {
        this.write(varName, new int[values.getRank()], values);
    }
    
    public void write(final String varName, final int[] origin, final Array values) throws IOException, InvalidRangeException {
        if (this.defineMode) {
            throw new UnsupportedOperationException("in define mode");
        }
        final Variable v2 = this.findVariable(varName);
        if (v2 == null) {
            throw new IllegalArgumentException("NetcdfFileWriteable.write illegal variable name = " + varName);
        }
        this.spiw.writeData(v2, new Section(origin, values.getShape()), values);
        v2.invalidateCache();
    }
    
    public void writeStringData(final String varName, final Array values) throws IOException, InvalidRangeException {
        this.writeStringData(varName, new int[values.getRank()], values);
    }
    
    public void writeStringData(final String varName, final int[] origin, final Array values) throws IOException, InvalidRangeException {
        if (values.getElementType() != String.class) {
            throw new IllegalArgumentException("Must be ArrayObject of String ");
        }
        final Variable v2 = this.findVariable(varName);
        if (v2 == null) {
            throw new IllegalArgumentException("illegal variable name = " + varName);
        }
        if (v2.getDataType() != DataType.CHAR) {
            throw new IllegalArgumentException("variable " + varName + " is not type CHAR");
        }
        final int rank = v2.getRank();
        final int strlen = v2.getShape(rank - 1);
        final ArrayChar cvalues = ArrayChar.makeFromStringArray((ArrayObject)values, strlen);
        final int[] corigin = new int[rank];
        System.arraycopy(origin, 0, corigin, 0, rank - 1);
        this.write(varName, corigin, cvalues);
    }
    
    public void flush() throws IOException {
        this.spiw.flush();
    }
    
    @Override
    public synchronized void close() throws IOException {
        if (this.spiw != null) {
            this.flush();
            this.spiw.close();
            this.spiw = null;
        }
        this.spi = null;
    }
    
    @Override
    public String getFileTypeId() {
        return "netCDF";
    }
    
    @Override
    public String getFileTypeDescription() {
        return "netCDF classic format - writer";
    }
    
    @Deprecated
    public NetcdfFileWriteable(final String location, final boolean fill) {
        this.location = location;
        this.fill = fill;
        this.defineMode = true;
    }
    
    @Deprecated
    public NetcdfFileWriteable() {
        this.defineMode = true;
    }
    
    @Deprecated
    public NetcdfFileWriteable(final String location) throws IOException {
        this.location = location;
        final RandomAccessFile raf = new RandomAccessFile(location, "rw");
        this.spi = SPFactory.getServiceProvider();
        (this.spiw = (IOServiceProviderWriter)this.spi).open(raf, this, null);
    }
    
    @Deprecated
    public void setName(final String filename) {
        this.location = filename;
    }
    
    @Deprecated
    public Variable addVariable(final String varName, final Class componentType, final Dimension[] dims) {
        final List<Dimension> list = new ArrayList<Dimension>();
        list.addAll(Arrays.asList(dims));
        return this.addVariable(varName, DataType.getType(componentType), list);
    }
    
    static {
        NetcdfFileWriteable.log = LoggerFactory.getLogger(NetcdfFileWriteable.class);
        NetcdfFileWriteable.valid = EnumSet.of(DataType.BYTE, new DataType[] { DataType.CHAR, DataType.SHORT, DataType.INT, DataType.DOUBLE, DataType.FLOAT });
    }
}
