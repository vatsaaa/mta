// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2;

import java.io.IOException;
import ucar.ma2.Array;
import ucar.ma2.Section;
import ucar.ma2.InvalidRangeException;
import ucar.ma2.Range;
import java.util.List;
import ucar.ma2.DataType;
import java.util.Formatter;

public interface VariableIF extends VariableSimpleIF
{
    String getName();
    
    String getNameEscaped();
    
    String getShortName();
    
    void getNameAndDimensions(final Formatter p0, final boolean p1, final boolean p2);
    
    boolean isUnlimited();
    
    boolean isUnsigned();
    
    DataType getDataType();
    
    int getRank();
    
    boolean isScalar();
    
    long getSize();
    
    int getElementSize();
    
    int[] getShape();
    
    List<Dimension> getDimensions();
    
    Dimension getDimension(final int p0);
    
    int findDimensionIndex(final String p0);
    
    List<Attribute> getAttributes();
    
    Attribute findAttribute(final String p0);
    
    Attribute findAttributeIgnoreCase(final String p0);
    
    Group getParentGroup();
    
    Variable section(final List<Range> p0) throws InvalidRangeException;
    
    Section getShapeAsSection();
    
    List<Range> getRanges();
    
    Array read(final int[] p0, final int[] p1) throws IOException, InvalidRangeException;
    
    Array read(final String p0) throws IOException, InvalidRangeException;
    
    Array read(final Section p0) throws IOException, InvalidRangeException;
    
    Array read() throws IOException;
    
    boolean isCoordinateVariable();
    
    boolean isMemberOfStructure();
    
    boolean isVariableLength();
    
    boolean isMetadata();
    
    Structure getParentStructure();
    
    String getDescription();
    
    String getUnitsString();
    
    List<Dimension> getDimensionsAll();
    
    byte readScalarByte() throws IOException;
    
    short readScalarShort() throws IOException;
    
    int readScalarInt() throws IOException;
    
    long readScalarLong() throws IOException;
    
    float readScalarFloat() throws IOException;
    
    double readScalarDouble() throws IOException;
    
    String readScalarString() throws IOException;
    
    String toStringDebug();
}
