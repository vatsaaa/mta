// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2;

import ucar.ma2.Range;
import org.slf4j.LoggerFactory;
import java.util.Formatter;
import ucar.ma2.StructureDataIterator;
import ucar.ma2.InvalidRangeException;
import ucar.ma2.Index;
import ucar.ma2.Section;
import java.io.IOException;
import ucar.ma2.Array;
import ucar.ma2.ArrayStructure;
import ucar.ma2.StructureData;
import ucar.ma2.StructureMembers;
import java.util.Collections;
import java.util.Iterator;
import java.util.Map;
import java.util.Collection;
import java.util.ArrayList;
import ucar.ma2.DataType;
import java.util.HashMap;
import java.util.List;
import org.slf4j.Logger;

public class Structure extends Variable
{
    protected static Logger log;
    protected static int defaultBufferSize;
    protected List<Variable> members;
    protected HashMap<String, Variable> memberHash;
    protected boolean isSubset;
    
    public Structure(final NetcdfFile ncfile, final Group group, final Structure parent, final String shortName) {
        super(ncfile, group, parent, shortName);
        this.dataType = DataType.STRUCTURE;
        this.elementSize = -1;
        this.members = new ArrayList<Variable>();
        this.memberHash = new HashMap<String, Variable>();
    }
    
    protected Structure(final Structure from) {
        super(from);
        this.members = new ArrayList<Variable>(from.members);
        this.memberHash = new HashMap<String, Variable>(from.memberHash);
        this.isSubset = from.isSubset();
    }
    
    public Structure select(final List<String> memberNames) {
        final Structure result = (Structure)this.copy();
        final List<Variable> members = new ArrayList<Variable>();
        for (final String name : memberNames) {
            final Variable m = this.findVariable(name);
            if (null != m) {
                members.add(m);
            }
        }
        result.setMemberVariables(members);
        result.isSubset = true;
        return result;
    }
    
    public Structure select(final String varName) {
        final List<String> memberNames = new ArrayList<String>(1);
        memberNames.add(varName);
        return this.select(memberNames);
    }
    
    public boolean isSubset() {
        return this.isSubset;
    }
    
    @Override
    protected Variable copy() {
        return new Structure(this);
    }
    
    protected int calcStructureSize() {
        int structureSize = 0;
        for (final Variable member : this.members) {
            structureSize += (int)(member.getSize() * member.getElementSize());
        }
        return structureSize;
    }
    
    @Override
    public boolean isCaching() {
        return false;
    }
    
    @Override
    public void setCaching(final boolean caching) {
        this.cache.isCaching = false;
        this.cache.cachingSet = true;
    }
    
    public Variable addMemberVariable(final Variable v) {
        if (this.isImmutable()) {
            throw new IllegalStateException("Cant modify");
        }
        this.members.add(v);
        this.memberHash.put(v.getShortName(), v);
        v.setParentStructure(this);
        return v;
    }
    
    public void setMemberVariables(final List<Variable> vars) {
        if (this.isImmutable()) {
            throw new IllegalStateException("Cant modify");
        }
        this.members = new ArrayList<Variable>();
        this.memberHash = new HashMap<String, Variable>(2 * vars.size());
        for (final Variable v : vars) {
            this.members.add(v);
            this.memberHash.put(v.getShortName(), v);
        }
    }
    
    public boolean removeMemberVariable(final Variable v) {
        if (this.isImmutable()) {
            throw new IllegalStateException("Cant modify");
        }
        if (v == null) {
            return false;
        }
        final java.util.Iterator<Variable> iter = this.members.iterator();
        while (iter.hasNext()) {
            final Variable mv = iter.next();
            if (mv.getShortName().equals(v.getShortName())) {
                iter.remove();
                this.memberHash.remove(v.getShortName());
                return true;
            }
        }
        return false;
    }
    
    public boolean replaceMemberVariable(final Variable newVar) {
        if (this.isImmutable()) {
            throw new IllegalStateException("Cant modify");
        }
        boolean found = false;
        for (int i = 0; i < this.members.size(); ++i) {
            final Variable v = this.members.get(i);
            if (v.getShortName() == null) {
                System.out.println("BAD null short name");
            }
            if (v.getShortName().equals(newVar.getShortName())) {
                this.members.set(i, newVar);
                found = true;
            }
        }
        if (!found) {
            this.members.add(newVar);
        }
        return found;
    }
    
    @Override
    public void setParentGroup(final Group group) {
        if (this.isImmutable()) {
            throw new IllegalStateException("Cant modify");
        }
        super.setParentGroup(group);
        for (final Variable v : this.members) {
            v.setParentGroup(group);
        }
    }
    
    @Override
    public Variable setImmutable() {
        this.members = Collections.unmodifiableList((List<? extends Variable>)this.members);
        for (final Variable m : this.members) {
            m.setImmutable();
        }
        super.setImmutable();
        return this;
    }
    
    public List<Variable> getVariables() {
        return this.isImmutable() ? this.members : new ArrayList<Variable>(this.members);
    }
    
    public int getNumberOfMemberVariables() {
        return this.members.size();
    }
    
    public List<String> getVariableNames() {
        return new ArrayList<String>(this.memberHash.keySet());
    }
    
    public Variable findVariable(final String shortName) {
        if (shortName == null) {
            return null;
        }
        return this.memberHash.get(shortName);
    }
    
    public StructureMembers makeStructureMembers() {
        final StructureMembers smembers = new StructureMembers(this.getName());
        for (final Variable v2 : this.getVariables()) {
            final StructureMembers.Member m = smembers.addMember(v2.getShortName(), v2.getDescription(), v2.getUnitsString(), v2.getDataType(), v2.getShape());
            if (v2 instanceof Structure) {
                m.setStructureMembers(((Structure)v2).makeStructureMembers());
            }
        }
        return smembers;
    }
    
    @Override
    public int getElementSize() {
        if (this.elementSize == -1) {
            this.calcElementSize();
        }
        return this.elementSize;
    }
    
    public void calcElementSize() {
        int total = 0;
        for (final Variable v : this.members) {
            total += (int)(v.getElementSize() * v.getSize());
        }
        this.elementSize = total;
    }
    
    public StructureData readStructure() throws IOException {
        if (this.getRank() != 0) {
            throw new UnsupportedOperationException("not a scalar structure");
        }
        final Array dataArray = this.read();
        final ArrayStructure data = (ArrayStructure)dataArray;
        return data.getStructureData(0);
    }
    
    public StructureData readStructure(final int index) throws IOException, InvalidRangeException {
        Section section = null;
        if (this.getRank() == 1) {
            section = new Section().appendRange(index, index);
        }
        else if (this.getRank() > 1) {
            final Index ii = Index.factory(this.shape);
            ii.setCurrentCounter(index);
            final int[] origin = ii.getCurrentCounter();
            section = new Section();
            for (int i = 0; i < origin.length; ++i) {
                section.appendRange(origin[i], origin[i]);
            }
        }
        final Array dataArray = this.read(section);
        final ArrayStructure data = (ArrayStructure)dataArray;
        return data.getStructureData(0);
    }
    
    public ArrayStructure readStructure(final int start, final int count) throws IOException, InvalidRangeException {
        if (this.getRank() != 1) {
            throw new UnsupportedOperationException("not a vector structure");
        }
        final int[] origin = { start };
        final int[] shape = { count };
        if (NetcdfFile.debugStructureIterator) {
            System.out.println("readStructure " + start + " " + count);
        }
        return (ArrayStructure)this.read(origin, shape);
    }
    
    public StructureDataIterator getStructureIterator() throws IOException {
        return this.getStructureIterator(Structure.defaultBufferSize);
    }
    
    public StructureDataIterator getStructureIterator(final int bufferSize) throws IOException {
        return (this.getRank() < 2) ? new IteratorRank1(bufferSize) : new Iterator(bufferSize);
    }
    
    public String getNameAndAttributes() {
        final Formatter sbuff = new Formatter();
        sbuff.format("Structure ", new Object[0]);
        this.getNameAndDimensions(sbuff, false, true);
        sbuff.format("\n", new Object[0]);
        for (final Attribute att : this.attributes) {
            sbuff.format("  %s:%s;\n", this.getShortName(), att.toString());
        }
        return sbuff.toString();
    }
    
    @Override
    protected void writeCDL(final Formatter buf, final String indent, final boolean useFullName, final boolean strict) {
        buf.format("\n%s%s {\n", indent, this.dataType);
        final String nestedSpace = "  " + indent;
        for (final Variable v : this.members) {
            v.writeCDL(buf, nestedSpace, useFullName, strict);
        }
        buf.format("%s} ", indent);
        this.getNameAndDimensions(buf, useFullName, strict);
        buf.format(";%s\n", this.extraInfo());
        for (final Attribute att : this.getAttributes()) {
            buf.format("%s  ", nestedSpace);
            if (strict) {
                buf.format(NetcdfFile.escapeName(this.getShortName()), new Object[0]);
            }
            buf.format(":%s;", att.toString(strict));
            if (!strict && att.getDataType() != DataType.STRING) {
                buf.format(" // %s", att.getDataType());
            }
            buf.format("\n", new Object[0]);
        }
        buf.format("\n", new Object[0]);
    }
    
    static {
        Structure.log = LoggerFactory.getLogger(Structure.class);
        Structure.defaultBufferSize = 500000;
    }
    
    private class IteratorRank1 implements StructureDataIterator
    {
        private int count;
        private int recnum;
        private int readStart;
        private int readCount;
        private int readAtaTime;
        private ArrayStructure as;
        
        protected IteratorRank1(final int bufferSize) {
            this.count = 0;
            this.recnum = (int)Structure.this.getSize();
            this.readStart = 0;
            this.readCount = 0;
            this.as = null;
            this.setBufferSize(bufferSize);
        }
        
        public boolean hasNext() {
            return this.count < this.recnum;
        }
        
        public StructureDataIterator reset() {
            this.count = 0;
            this.readStart = 0;
            this.readCount = 0;
            return this;
        }
        
        public StructureData next() throws IOException {
            if (this.count >= this.readStart) {
                this.readNext();
            }
            ++this.count;
            return this.as.getStructureData(this.readCount++);
        }
        
        public int getCurrentRecno() {
            return this.count - 1;
        }
        
        private void readNext() throws IOException {
            final int left = Math.min(this.recnum, this.readStart + this.readAtaTime);
            final int need = left - this.readStart;
            try {
                this.as = Structure.this.readStructure(this.readStart, need);
                if (NetcdfFile.debugStructureIterator) {
                    System.out.println("readNext " + this.count + " " + this.readStart);
                }
            }
            catch (InvalidRangeException e) {
                Structure.log.error("Structure.IteratorRank1.readNext() ", e);
                throw new IllegalStateException("Structure.Iterator.readNext() ", e);
            }
            this.readStart += need;
            this.readCount = 0;
        }
        
        public void setBufferSize(int bytes) {
            if (this.count > 0) {
                return;
            }
            int structureSize = Structure.this.calcStructureSize();
            if (structureSize <= 0) {
                structureSize = 1;
            }
            if (bytes <= 0) {
                bytes = Structure.defaultBufferSize;
            }
            this.readAtaTime = Math.max(10, bytes / structureSize);
            if (NetcdfFile.debugStructureIterator) {
                System.out.println("Iterator structureSize= " + structureSize + " readAtaTime= " + this.readAtaTime);
            }
        }
    }
    
    private class Iterator implements StructureDataIterator
    {
        private int count;
        private int total;
        private int readStart;
        private int readCount;
        private int outerCount;
        private int readAtaTime;
        private ArrayStructure as;
        
        protected Iterator(final int bufferSize) {
            this.as = null;
            this.reset();
        }
        
        public boolean hasNext() {
            return this.count < this.total;
        }
        
        public StructureDataIterator reset() {
            this.count = 0;
            this.total = (int)Structure.this.getSize();
            this.readStart = 0;
            this.readCount = 0;
            this.outerCount = 0;
            this.readAtaTime = (int)Structure.this.getSize() / Structure.this.shape[0];
            return this;
        }
        
        public int getCurrentRecno() {
            return this.count - 1;
        }
        
        public StructureData next() throws IOException {
            if (this.count >= this.readStart) {
                this.readNextGeneralRank();
            }
            ++this.count;
            return this.as.getStructureData(this.readCount++);
        }
        
        private void readNextGeneralRank() throws IOException {
            try {
                final Section section = new Section(Structure.this.shape);
                section.setRange(0, new Range(this.outerCount, this.outerCount));
                this.as = (ArrayStructure)Structure.this.read(section);
                if (NetcdfFile.debugStructureIterator) {
                    System.out.println("readNext inner=" + this.outerCount + " total=" + this.outerCount);
                }
                ++this.outerCount;
            }
            catch (InvalidRangeException e) {
                Structure.log.error("Structure.Iterator.readNext() ", e);
                throw new IllegalStateException("Structure.Iterator.readNext() ", e);
            }
            this.readStart += (int)this.as.getSize();
            this.readCount = 0;
        }
        
        public void setBufferSize(final int bytes) {
        }
    }
}
