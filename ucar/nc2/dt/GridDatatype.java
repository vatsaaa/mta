// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.dt;

import ucar.nc2.dataset.VariableDS;
import ucar.unidata.geoloc.LatLonRect;
import ucar.ma2.InvalidRangeException;
import ucar.ma2.Range;
import java.io.IOException;
import ucar.ma2.MAMath;
import ucar.ma2.Array;
import ucar.unidata.geoloc.ProjectionImpl;
import ucar.nc2.Dimension;
import ucar.nc2.Attribute;
import java.util.List;
import ucar.ma2.DataType;

public interface GridDatatype extends Comparable<GridDatatype>
{
    String getName();
    
    String getNameEscaped();
    
    String getDescription();
    
    String getUnitsString();
    
    DataType getDataType();
    
    int getRank();
    
    int[] getShape();
    
    List<Attribute> getAttributes();
    
    Attribute findAttributeIgnoreCase(final String p0);
    
    String findAttValueIgnoreCase(final String p0, final String p1);
    
    List<Dimension> getDimensions();
    
    Dimension getDimension(final int p0);
    
    Dimension getTimeDimension();
    
    Dimension getZDimension();
    
    Dimension getYDimension();
    
    Dimension getXDimension();
    
    Dimension getEnsembleDimension();
    
    Dimension getRunTimeDimension();
    
    int getTimeDimensionIndex();
    
    int getZDimensionIndex();
    
    int getYDimensionIndex();
    
    int getXDimensionIndex();
    
    int getEnsembleDimensionIndex();
    
    int getRunTimeDimensionIndex();
    
    GridCoordSystem getCoordinateSystem();
    
    ProjectionImpl getProjection();
    
    boolean hasMissingData();
    
    boolean isMissingData(final double p0);
    
    MAMath.MinMax getMinMaxSkipMissingData(final Array p0);
    
    float[] setMissingToNaN(final float[] p0);
    
    Array readDataSlice(final int p0, final int p1, final int p2, final int p3, final int p4, final int p5) throws IOException;
    
    Array readDataSlice(final int p0, final int p1, final int p2, final int p3) throws IOException;
    
    Array readVolumeData(final int p0) throws IOException;
    
    GridDatatype makeSubset(final Range p0, final Range p1, final Range p2, final Range p3, final Range p4, final Range p5) throws InvalidRangeException;
    
    GridDatatype makeSubset(final Range p0, final Range p1, final LatLonRect p2, final int p3, final int p4, final int p5) throws InvalidRangeException;
    
    String getInfo();
    
    VariableDS getVariable();
}
