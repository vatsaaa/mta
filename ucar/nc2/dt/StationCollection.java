// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.dt;

import java.util.Date;
import ucar.unidata.geoloc.LatLonRect;
import ucar.nc2.util.CancelTask;
import java.io.IOException;
import ucar.unidata.geoloc.Station;
import java.util.List;

public interface StationCollection extends PointCollection
{
    List<Station> getStations() throws IOException;
    
    List<Station> getStations(final CancelTask p0) throws IOException;
    
    List<Station> getStations(final LatLonRect p0) throws IOException;
    
    List<Station> getStations(final LatLonRect p0, final CancelTask p1) throws IOException;
    
    Station getStation(final String p0);
    
    int getStationDataCount(final Station p0);
    
    List getData(final Station p0) throws IOException;
    
    List getData(final Station p0, final CancelTask p1) throws IOException;
    
    List getData(final Station p0, final Date p1, final Date p2) throws IOException;
    
    List getData(final Station p0, final Date p1, final Date p2, final CancelTask p3) throws IOException;
    
    List getData(final List<Station> p0) throws IOException;
    
    List getData(final List<Station> p0, final CancelTask p1) throws IOException;
    
    List getData(final List<Station> p0, final Date p1, final Date p2) throws IOException;
    
    List getData(final List<Station> p0, final Date p1, final Date p2, final CancelTask p3) throws IOException;
    
    DataIterator getDataIterator(final Station p0);
    
    DataIterator getDataIterator(final Station p0, final Date p1, final Date p2);
}
