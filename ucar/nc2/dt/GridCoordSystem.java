// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.dt;

import ucar.nc2.units.DateRange;
import ucar.unidata.geoloc.LatLonPoint;
import ucar.ma2.InvalidRangeException;
import ucar.ma2.Range;
import ucar.unidata.geoloc.ProjectionRect;
import ucar.unidata.geoloc.LatLonRect;
import ucar.unidata.geoloc.vertical.VerticalTransform;
import ucar.nc2.dataset.VerticalCT;
import ucar.unidata.geoloc.ProjectionImpl;
import ucar.nc2.dataset.ProjectionCT;
import ucar.nc2.dataset.CoordinateTransform;
import ucar.nc2.dataset.CoordinateAxis1DTime;
import ucar.nc2.dataset.CoordinateAxis1D;
import ucar.nc2.dataset.CoordinateAxis;
import ucar.nc2.Dimension;
import java.util.List;

public interface GridCoordSystem
{
    String getName();
    
    List<Dimension> getDomain();
    
    List<CoordinateAxis> getCoordinateAxes();
    
    boolean isProductSet();
    
    CoordinateAxis getXHorizAxis();
    
    CoordinateAxis getYHorizAxis();
    
    CoordinateAxis1D getVerticalAxis();
    
    CoordinateAxis getTimeAxis();
    
    CoordinateAxis1D getEnsembleAxis();
    
    CoordinateAxis1DTime getRunTimeAxis();
    
    List<CoordinateTransform> getCoordinateTransforms();
    
    ProjectionCT getProjectionCT();
    
    ProjectionImpl getProjection();
    
    void setProjectionBoundingBox();
    
    VerticalCT getVerticalCT();
    
    VerticalTransform getVerticalTransform();
    
    boolean isLatLon();
    
    LatLonRect getLatLonBoundingBox();
    
    ProjectionRect getBoundingBox();
    
    boolean isRegularSpatial();
    
    List<Range> getRangesFromLatLonRect(final LatLonRect p0) throws InvalidRangeException;
    
    int[] findXYindexFromCoord(final double p0, final double p1, final int[] p2);
    
    int[] findXYindexFromCoordBounded(final double p0, final double p1, final int[] p2);
    
    int[] findXYindexFromLatLon(final double p0, final double p1, final int[] p2);
    
    int[] findXYindexFromLatLonBounded(final double p0, final double p1, final int[] p2);
    
    LatLonPoint getLatLon(final int p0, final int p1);
    
    boolean isZPositive();
    
    DateRange getDateRange();
    
    boolean hasTimeAxis();
    
    boolean hasTimeAxis1D();
    
    CoordinateAxis1DTime getTimeAxis1D();
    
    CoordinateAxis1DTime getTimeAxisForRun(final int p0);
}
