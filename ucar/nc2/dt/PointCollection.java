// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.dt;

import java.util.Date;
import ucar.unidata.geoloc.LatLonRect;
import ucar.nc2.util.CancelTask;
import java.io.IOException;
import java.util.List;
import ucar.nc2.units.DateUnit;

public interface PointCollection
{
    Class getDataClass();
    
    DateUnit getTimeUnits();
    
    List getData() throws IOException;
    
    List getData(final CancelTask p0) throws IOException;
    
    int getDataCount();
    
    List getData(final LatLonRect p0) throws IOException;
    
    List getData(final LatLonRect p0, final CancelTask p1) throws IOException;
    
    List getData(final LatLonRect p0, final Date p1, final Date p2) throws IOException;
    
    List getData(final LatLonRect p0, final Date p1, final Date p2, final CancelTask p3) throws IOException;
    
    DataIterator getDataIterator(final int p0) throws IOException;
}
