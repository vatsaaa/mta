// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.dt;

import ucar.nc2.dt.trajectory.ARMTrajectoryObsDataset;
import ucar.nc2.dt.trajectory.ZebraClassTrajectoryObsDataset;
import ucar.nc2.dt.trajectory.Float10TrajectoryObsDataset;
import ucar.nc2.dt.trajectory.SimpleTrajectoryObsDataset;
import ucar.nc2.dt.trajectory.UnidataTrajectoryObsDataset;
import ucar.nc2.dt.trajectory.RafTrajectoryObsDataset;
import ucar.nc2.dt.radial.UF2Dataset;
import ucar.nc2.dt.radial.Nids2Dataset;
import ucar.nc2.dt.radial.LevelII2Dataset;
import ucar.nc2.dt.radial.Dorade2Dataset;
import ucar.nc2.dt.radial.Netcdf2Dataset;
import ucar.nc2.dt.point.OldUnidataPointObsDataset;
import ucar.nc2.dt.point.OldUnidataStationObsDataset;
import ucar.nc2.dt.point.MadisStationObsDataset;
import ucar.nc2.dt.point.NdbcDataset;
import ucar.nc2.dt.point.UnidataStationObsDataset2;
import ucar.nc2.dt.point.SequenceObsDataset;
import ucar.nc2.dt.point.DapperDataset;
import ucar.nc2.dt.point.UnidataStationObsMultidimDataset;
import ucar.nc2.dt.point.UnidataStationObsDataset;
import ucar.nc2.dt.point.UnidataPointObsDataset;
import java.util.ArrayList;
import java.util.Iterator;
import ucar.nc2.dt.grid.GridDataset;
import java.io.IOException;
import ucar.nc2.dataset.NetcdfDataset;
import ucar.nc2.util.CancelTask;
import ucar.nc2.constants.FeatureType;
import java.util.List;

public class TypedDatasetFactory
{
    private static List<Factory> transformList;
    private static boolean userMode;
    
    public static void registerFactory(final FeatureType datatype, final String className) throws ClassNotFoundException {
        final Class c = Class.forName(className);
        registerFactory(datatype, c);
    }
    
    public static void registerFactory(final FeatureType datatype, final Class c) {
        if (!TypedDatasetFactoryIF.class.isAssignableFrom(c)) {
            throw new IllegalArgumentException("Class " + c.getName() + " must implement TypedDatasetFactoryIF");
        }
        Object instance;
        try {
            instance = c.newInstance();
        }
        catch (InstantiationException e) {
            throw new IllegalArgumentException("CoordTransBuilderIF Class " + c.getName() + " cannot instantiate, probably need default Constructor");
        }
        catch (IllegalAccessException e2) {
            throw new IllegalArgumentException("CoordTransBuilderIF Class " + c.getName() + " is not accessible");
        }
        if (TypedDatasetFactory.userMode) {
            TypedDatasetFactory.transformList.add(0, new Factory(datatype, c, (TypedDatasetFactoryIF)instance));
        }
        else {
            TypedDatasetFactory.transformList.add(new Factory(datatype, c, (TypedDatasetFactoryIF)instance));
        }
    }
    
    public static TypedDataset open(final FeatureType datatype, final String location, final CancelTask task, final StringBuilder errlog) throws IOException {
        final NetcdfDataset ncd = NetcdfDataset.acquireDataset(location, task);
        return open(datatype, ncd, task, errlog);
    }
    
    public static TypedDataset open(final FeatureType datatype, final NetcdfDataset ncd, final CancelTask task, final StringBuilder errlog) throws IOException {
        Class useClass = null;
        for (final Factory fac : TypedDatasetFactory.transformList) {
            if (datatype != null && datatype != fac.datatype) {
                continue;
            }
            if (fac.instance.isMine(ncd)) {
                useClass = fac.c;
                break;
            }
        }
        if (null == useClass) {
            if (datatype == FeatureType.POINT) {
                return open(FeatureType.STATION, ncd, task, errlog);
            }
            if (datatype == FeatureType.GRID) {
                return new GridDataset(ncd);
            }
            if (null == datatype) {
                final GridDataset gds = new GridDataset(ncd);
                if (gds.getGrids().size() > 0) {
                    return gds;
                }
            }
            errlog.append("**Failed to find Datatype Factory for= ").append(ncd.getLocation()).append(" datatype= ").append(datatype).append("\n");
            return null;
        }
        else {
            TypedDatasetFactoryIF builder = null;
            try {
                builder = useClass.newInstance();
            }
            catch (InstantiationException e) {
                errlog.append(e.getMessage()).append("\n");
            }
            catch (IllegalAccessException e2) {
                errlog.append(e2.getMessage()).append("\n");
            }
            if (null == builder) {
                errlog.append("**Error on TypedDatasetFactory object from class= ").append(useClass.getName()).append("\n");
                return null;
            }
            return builder.open(ncd, task, errlog);
        }
    }
    
    static {
        TypedDatasetFactory.transformList = new ArrayList<Factory>();
        TypedDatasetFactory.userMode = false;
        registerFactory(FeatureType.POINT, UnidataPointObsDataset.class);
        registerFactory(FeatureType.STATION, UnidataStationObsDataset.class);
        registerFactory(FeatureType.STATION, UnidataStationObsMultidimDataset.class);
        registerFactory(FeatureType.POINT, DapperDataset.class);
        registerFactory(FeatureType.STATION, SequenceObsDataset.class);
        registerFactory(FeatureType.STATION, UnidataStationObsDataset2.class);
        registerFactory(FeatureType.STATION, NdbcDataset.class);
        registerFactory(FeatureType.STATION, MadisStationObsDataset.class);
        registerFactory(FeatureType.STATION, OldUnidataStationObsDataset.class);
        registerFactory(FeatureType.POINT, OldUnidataPointObsDataset.class);
        registerFactory(FeatureType.RADIAL, Netcdf2Dataset.class);
        registerFactory(FeatureType.RADIAL, Dorade2Dataset.class);
        registerFactory(FeatureType.RADIAL, LevelII2Dataset.class);
        registerFactory(FeatureType.RADIAL, Nids2Dataset.class);
        registerFactory(FeatureType.RADIAL, UF2Dataset.class);
        registerFactory(FeatureType.TRAJECTORY, RafTrajectoryObsDataset.class);
        registerFactory(FeatureType.TRAJECTORY, UnidataTrajectoryObsDataset.class);
        registerFactory(FeatureType.TRAJECTORY, SimpleTrajectoryObsDataset.class);
        registerFactory(FeatureType.TRAJECTORY, Float10TrajectoryObsDataset.class);
        registerFactory(FeatureType.TRAJECTORY, ZebraClassTrajectoryObsDataset.class);
        registerFactory(FeatureType.TRAJECTORY, ARMTrajectoryObsDataset.class);
        TypedDatasetFactory.userMode = true;
    }
    
    private static class Factory
    {
        FeatureType datatype;
        Class c;
        TypedDatasetFactoryIF instance;
        
        Factory(final FeatureType datatype, final Class c, final TypedDatasetFactoryIF instance) {
            this.datatype = datatype;
            this.c = c;
            this.instance = instance;
        }
    }
}
