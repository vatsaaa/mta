// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.dt;

import java.io.IOException;
import ucar.ma2.StructureData;
import ucar.unidata.geoloc.EarthLocation;
import java.util.Date;

public interface PointObsDatatype
{
    double getNominalTime();
    
    double getObservationTime();
    
    Date getNominalTimeAsDate();
    
    Date getObservationTimeAsDate();
    
    EarthLocation getLocation();
    
    StructureData getData() throws IOException;
}
