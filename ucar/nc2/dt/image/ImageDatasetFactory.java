// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.dt.image;

import java.util.ArrayList;
import java.net.MalformedURLException;
import javax.imageio.ImageIO;
import java.net.URL;
import ucar.nc2.iosp.adde.AddeImage;
import java.io.IOException;
import ucar.ma2.Array;
import ucar.nc2.dt.GridCoordSystem;
import java.awt.image.BufferedImage;
import ucar.nc2.dt.GridDatatype;
import java.util.List;
import java.io.File;

public class ImageDatasetFactory
{
    private StringBuffer log;
    private File currentFile;
    private File currentDir;
    private List<File> currentDirFileList;
    private int currentDirFileNo;
    private GridDatatype grid;
    private int time;
    private int ntimes;
    
    public ImageDatasetFactory() {
        this.currentDir = null;
        this.currentDirFileNo = 0;
        this.grid = null;
        this.time = 0;
        this.ntimes = 1;
    }
    
    public String getErrorMessages() {
        return (this.log == null) ? "" : this.log.toString();
    }
    
    public BufferedImage openDataset(final GridDatatype grid) throws IOException {
        this.grid = grid;
        this.time = 0;
        final GridCoordSystem gcsys = grid.getCoordinateSystem();
        if (gcsys.getTimeAxis() != null) {
            this.ntimes = (int)gcsys.getTimeAxis().getSize();
        }
        final Array data = grid.readDataSlice(this.time, 0, -1, -1);
        return ImageArrayAdapter.makeGrayscaleImage(data);
    }
    
    public BufferedImage open(String location) throws IOException {
        this.log = new StringBuffer();
        if (location.startsWith("adde:")) {
            try {
                final AddeImage addeImage = AddeImage.factory(location);
                this.currentFile = null;
                return addeImage.getImage();
            }
            catch (Exception e) {
                this.log.append(e.getMessage());
                return null;
            }
        }
        if (location.startsWith("http:")) {
            try {
                final URL url = new URL(location);
                this.currentFile = null;
                return ImageIO.read(url);
            }
            catch (MalformedURLException e2) {
                this.log.append(e2.getMessage());
                return null;
            }
            catch (IOException e3) {
                this.log.append(e3.getMessage());
                return null;
            }
        }
        if (location.startsWith("file:)")) {
            location = location.substring(5);
        }
        try {
            final File f = new File(location);
            if (!f.exists()) {
                return null;
            }
            this.currentFile = f;
            this.currentDir = null;
            return ImageIO.read(f);
        }
        catch (MalformedURLException e2) {
            this.log.append(e2.getMessage());
            return null;
        }
        catch (IOException e3) {
            this.log.append(e3.getMessage());
            return null;
        }
    }
    
    public BufferedImage getNextImage(final boolean forward) {
        if (this.grid != null) {
            if (forward) {
                ++this.time;
                if (this.time >= this.ntimes) {
                    this.time = 0;
                }
            }
            else {
                --this.time;
                if (this.time < 0) {
                    this.time = this.ntimes - 1;
                }
            }
            try {
                final Array data = this.grid.readDataSlice(this.time, 0, -1, -1);
                return ImageArrayAdapter.makeGrayscaleImage(data);
            }
            catch (IOException e) {
                e.printStackTrace();
                return null;
            }
        }
        if (this.currentFile == null) {
            return null;
        }
        if (this.currentDir == null) {
            this.currentDirFileNo = 0;
            this.currentDir = this.currentFile.getParentFile();
            this.currentDirFileList = new ArrayList<File>();
            this.addToList(this.currentDir, this.currentDirFileList);
            for (int i = 0; i < this.currentDirFileList.size(); ++i) {
                final File file = this.currentDirFileList.get(i);
                if (file.equals(this.currentFile)) {
                    this.currentDirFileNo = i;
                }
            }
        }
        if (forward) {
            ++this.currentDirFileNo;
            if (this.currentDirFileNo >= this.currentDirFileList.size()) {
                this.currentDirFileNo = 0;
            }
        }
        else {
            --this.currentDirFileNo;
            if (this.currentDirFileNo < 0) {
                this.currentDirFileNo = this.currentDirFileList.size() - 1;
            }
        }
        final File nextFile = this.currentDirFileList.get(this.currentDirFileNo);
        try {
            System.out.println("Open image " + nextFile);
            return ImageIO.read(nextFile);
        }
        catch (IOException e) {
            System.out.println("Failed to open image " + nextFile);
            return this.getNextImage(forward);
        }
    }
    
    private void addToList(final File dir, final List<File> list) {
        for (final File file : dir.listFiles()) {
            if (file.isDirectory()) {
                this.addToList(file, list);
            }
            else if (file.getName().endsWith(".jpg") || file.getName().endsWith(".JPG")) {
                list.add(file);
            }
        }
    }
}
