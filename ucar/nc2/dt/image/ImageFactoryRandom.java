// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.dt.image;

import java.io.IOException;
import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.util.Collection;
import java.util.ArrayList;
import java.util.Random;
import java.io.File;
import java.util.List;

public class ImageFactoryRandom
{
    private List<File> holdList;
    private List<File> fileList;
    private Random random;
    File nextFile;
    
    public ImageFactoryRandom(final File topDir) {
        this.random = new Random(System.currentTimeMillis());
        this.nextFile = null;
        if (!topDir.exists()) {
            return;
        }
        this.addToList(topDir, this.fileList = new ArrayList<File>(1000));
        System.out.println("nfiles= " + this.fileList.size());
        this.holdList = new ArrayList<File>(this.fileList);
    }
    
    private void addToList(final File dir, final List<File> list) {
        for (final File file : dir.listFiles()) {
            if (file.isDirectory()) {
                this.addToList(file, list);
            }
            else if (file.getName().endsWith(".jpg") || file.getName().endsWith(".JPG")) {
                list.add(file);
            }
        }
    }
    
    public BufferedImage getNextImage() {
        if (this.holdList.size() == 0) {
            this.holdList = new ArrayList<File>(this.fileList);
        }
        final int next = Math.abs(this.random.nextInt()) % this.holdList.size();
        this.nextFile = this.holdList.get(next);
        this.holdList.remove(this.nextFile);
        try {
            System.out.printf("next %d %s %n", next, this.nextFile);
            return ImageIO.read(this.nextFile);
        }
        catch (IOException e) {
            System.out.println("Failed to open image " + this.nextFile);
            this.fileList.remove(this.nextFile);
            return this.getNextImage();
        }
    }
    
    public void delete() {
        if (this.nextFile == null) {
            return;
        }
        this.fileList.remove(this.nextFile);
        final File f = new File("C:/tmp/deleted/" + this.nextFile.getName());
        this.nextFile.renameTo(f);
    }
}
