// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.dt.image;

import java.awt.image.SampleModel;
import java.awt.Component;
import javax.swing.JPanel;
import java.awt.event.WindowListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowAdapter;
import javax.swing.JFrame;
import java.awt.image.AffineTransformOp;
import java.awt.geom.AffineTransform;
import java.awt.Graphics2D;
import javax.swing.Icon;
import java.awt.Image;
import javax.swing.ImageIcon;
import java.awt.geom.Rectangle2D;
import java.awt.Composite;
import java.awt.AlphaComposite;
import java.awt.Shape;
import java.awt.geom.Ellipse2D;
import java.awt.Color;
import javax.swing.JLabel;
import ucar.ma2.IndexIterator;
import java.awt.image.DataBufferByte;
import ucar.ma2.MAMath;
import ucar.ma2.ArrayByte;
import java.awt.image.WritableRaster;
import java.awt.image.DataBuffer;
import java.util.Hashtable;
import java.awt.image.ColorModel;
import java.awt.image.ComponentColorModel;
import java.awt.color.ColorSpace;
import java.awt.Point;
import java.awt.image.Raster;
import java.awt.image.BufferedImage;
import ucar.ma2.Array;

public class ImageArrayAdapter
{
    public static BufferedImage makeGrayscaleImage(Array ma) {
        if (ma.getRank() == 3) {
            ma = ma.reduce();
        }
        if (ma.getRank() == 3) {
            ma = ma.slice(0, 0);
        }
        final int h = ma.getShape()[0];
        final int w = ma.getShape()[1];
        final DataBuffer dataBuffer = makeDataBuffer(ma);
        final WritableRaster raster = Raster.createInterleavedRaster(dataBuffer, w, h, w, 1, new int[] { 0 }, null);
        final ColorSpace cs = ColorSpace.getInstance(1003);
        final ComponentColorModel colorModel = new ComponentColorModel(cs, new int[] { 8 }, false, false, 1, 0);
        return new BufferedImage(colorModel, raster, false, null);
    }
    
    private static DataBuffer makeDataBuffer(final Array ma) {
        if (ma instanceof ArrayByte) {
            return makeByteDataBuffer((ArrayByte)ma);
        }
        final int h = ma.getShape()[0];
        final int w = ma.getShape()[1];
        final double min = MAMath.getMinimum(ma);
        final double max = MAMath.getMaximum(ma);
        double scale = max - min;
        if (scale > 0.0) {
            scale = 255.0 / scale;
        }
        final IndexIterator ii = ma.getIndexIterator();
        final byte[] byteData = new byte[h * w];
        for (int i = 0; i < byteData.length; ++i) {
            final double val = ii.getDoubleNext();
            final double sval = (val - min) * scale;
            byteData[i] = (byte)sval;
        }
        return new DataBufferByte(byteData, byteData.length);
    }
    
    private static DataBuffer makeByteDataBuffer(final ArrayByte ma) {
        final byte[] byteData = (byte[])ma.copyTo1DJavaArray();
        return new DataBufferByte(byteData, byteData.length);
    }
    
    private static JLabel test() {
        final byte[] tmp2 = new byte[576900];
        for (int i = 0; i < tmp2.length; ++i) {
            final int row = i / 640;
            final int col = i % 640;
            tmp2[i] = (byte)(row * 255.0 / 900.0);
        }
        final DataBuffer db = new DataBufferByte(tmp2, tmp2.length);
        final WritableRaster raster = Raster.createInterleavedRaster(db, 900, 640, 900, 1, new int[] { 0 }, null);
        final ColorSpace cs = ColorSpace.getInstance(1003);
        final ComponentColorModel cm = new ComponentColorModel(cs, new int[] { 8 }, false, false, 1, 0);
        final BufferedImage image = new BufferedImage(cm, raster, true, null);
        final Graphics2D g2d = image.createGraphics();
        g2d.setColor(Color.red);
        g2d.draw(new Ellipse2D.Float(0.0f, 0.0f, 200.0f, 100.0f));
        g2d.dispose();
        final Color transparent = new Color(0, 0, 0, 0);
        g2d.setColor(transparent);
        g2d.setComposite(AlphaComposite.Src);
        g2d.fill(new Rectangle2D.Float(320.0f, 20.0f, 100.0f, 20.0f));
        g2d.dispose();
        return new JLabel(new ImageIcon(image));
    }
    
    public static void main(final String[] args) {
        final Array data = Array.factory(Integer.TYPE, new int[] { 255, 100 });
        int count = 0;
        final IndexIterator ii = data.getIndexIterator();
        while (ii.hasNext()) {
            final int row = count / 100;
            ii.setIntNext(row * 255);
            ++count;
        }
        final BufferedImage image = makeGrayscaleImage(data);
        final Raster raster = image.getRaster();
        final DataBuffer ds = raster.getDataBuffer();
        final SampleModel sm = raster.getSampleModel();
        System.out.println(" image type = " + image.getType());
        System.out.println(" transfer type = " + sm.getTransferType());
        final AffineTransform at = new AffineTransform();
        final AffineTransformOp op = new AffineTransformOp(at, 1);
        final BufferedImage targetImage = new BufferedImage(image.getWidth(), image.getHeight(), 5);
        final BufferedImage image2 = op.filter(image, targetImage);
        System.out.println("ok!");
        final boolean display = true;
        if (!display) {
            return;
        }
        final JFrame frame = new JFrame("Test");
        frame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(final WindowEvent e) {
                System.exit(0);
            }
        });
        final ImageIcon icon = new ImageIcon(image2);
        final JLabel lab = new JLabel(icon);
        final JPanel main = new JPanel();
        main.add(lab);
        frame.getContentPane().add(main);
        frame.pack();
        frame.setLocation(300, 300);
        frame.setVisible(true);
    }
}
