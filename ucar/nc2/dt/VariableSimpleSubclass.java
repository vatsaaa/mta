// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.dt;

import ucar.nc2.Attribute;
import ucar.nc2.Dimension;
import java.util.List;
import ucar.ma2.DataType;
import ucar.nc2.VariableSimpleIF;

public class VariableSimpleSubclass implements VariableSimpleIF
{
    protected VariableSimpleIF v;
    
    public VariableSimpleSubclass(final VariableSimpleIF v) {
        this.v = v;
    }
    
    public String getName() {
        return this.v.getName();
    }
    
    public String getShortName() {
        return this.v.getShortName();
    }
    
    public DataType getDataType() {
        return this.v.getDataType();
    }
    
    public String getDescription() {
        return this.v.getDescription();
    }
    
    public String getInfo() {
        return this.v.toString();
    }
    
    public String getUnitsString() {
        return this.v.getUnitsString();
    }
    
    public int getRank() {
        return this.v.getRank();
    }
    
    public int[] getShape() {
        return this.v.getShape();
    }
    
    public List<Dimension> getDimensions() {
        return this.v.getDimensions();
    }
    
    public List<Attribute> getAttributes() {
        return this.v.getAttributes();
    }
    
    public Attribute findAttributeIgnoreCase(final String attName) {
        return this.v.findAttributeIgnoreCase(attName);
    }
    
    @Override
    public String toString() {
        return this.v.toString();
    }
    
    public int compareTo(final VariableSimpleIF o) {
        return this.getName().compareTo(o.getName());
    }
}
