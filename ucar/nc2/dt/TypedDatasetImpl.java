// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.dt;

import ucar.nc2.units.DateFormatter;
import java.io.IOException;
import ucar.nc2.Attribute;
import ucar.nc2.NetcdfFile;
import java.util.Iterator;
import ucar.nc2.Variable;
import java.util.ArrayList;
import ucar.nc2.VariableSimpleIF;
import java.util.List;
import ucar.unidata.geoloc.LatLonRect;
import java.util.Date;
import ucar.nc2.dataset.NetcdfDataset;

public abstract class TypedDatasetImpl implements TypedDataset
{
    protected NetcdfDataset ncfile;
    protected String title;
    protected String desc;
    protected String location;
    protected Date startDate;
    protected Date endDate;
    protected LatLonRect boundingBox;
    protected List<VariableSimpleIF> dataVariables;
    protected StringBuffer parseInfo;
    
    public TypedDatasetImpl() {
        this.dataVariables = new ArrayList<VariableSimpleIF>();
        this.parseInfo = new StringBuffer();
    }
    
    public TypedDatasetImpl(final String title, final String description, final String location) {
        this.dataVariables = new ArrayList<VariableSimpleIF>();
        this.parseInfo = new StringBuffer();
        this.title = title;
        this.desc = description;
        this.location = location;
    }
    
    public TypedDatasetImpl(final NetcdfDataset ncfile) {
        this.dataVariables = new ArrayList<VariableSimpleIF>();
        this.parseInfo = new StringBuffer();
        this.ncfile = ncfile;
        this.location = ncfile.getLocation();
        this.title = ncfile.getTitle();
        if (this.title == null) {
            this.title = ncfile.findAttValueIgnoreCase(null, "title", null);
        }
        if (this.desc == null) {
            this.desc = ncfile.findAttValueIgnoreCase(null, "description", null);
        }
    }
    
    public void setTitle(final String title) {
        this.title = title;
    }
    
    public void setDescription(final String desc) {
        this.desc = desc;
    }
    
    public void setLocationURI(final String location) {
        this.location = location;
    }
    
    protected abstract void setStartDate();
    
    protected abstract void setEndDate();
    
    protected abstract void setBoundingBox();
    
    protected void removeDataVariable(final String varName) {
        final Iterator iter = this.dataVariables.iterator();
        while (iter.hasNext()) {
            final VariableSimpleIF v = iter.next();
            if (v.getName().equals(varName)) {
                iter.remove();
            }
        }
    }
    
    public NetcdfFile getNetcdfFile() {
        return this.ncfile;
    }
    
    public String getTitle() {
        return this.title;
    }
    
    public String getDescription() {
        return this.desc;
    }
    
    public String getLocationURI() {
        return this.location;
    }
    
    public String getLocation() {
        return this.location;
    }
    
    public List<Attribute> getGlobalAttributes() {
        if (this.ncfile == null) {
            return new ArrayList<Attribute>();
        }
        return this.ncfile.getGlobalAttributes();
    }
    
    public Attribute findGlobalAttributeIgnoreCase(final String name) {
        if (this.ncfile == null) {
            return null;
        }
        return this.ncfile.findGlobalAttributeIgnoreCase(name);
    }
    
    public void close() throws IOException {
        if (this.ncfile != null) {
            this.ncfile.close();
        }
    }
    
    public String getDetailInfo() {
        final DateFormatter formatter = new DateFormatter();
        final StringBuffer sbuff = new StringBuffer();
        sbuff.append("  location= ").append(this.getLocation()).append("\n");
        sbuff.append("  title= ").append(this.getTitle()).append("\n");
        sbuff.append("  desc= ").append(this.getDescription()).append("\n");
        sbuff.append("  start= ").append(formatter.toDateTimeString(this.getStartDate())).append("\n");
        sbuff.append("  end  = ").append(formatter.toDateTimeString(this.getEndDate())).append("\n");
        sbuff.append("  bb   = ").append(this.getBoundingBox()).append("\n");
        if (this.getBoundingBox() != null) {
            sbuff.append("  bb   = ").append(this.getBoundingBox().toString2()).append("\n");
        }
        sbuff.append("  has netcdf = ").append(this.getNetcdfFile() != null).append("\n");
        final List<Attribute> ga = this.getGlobalAttributes();
        if (ga.size() > 0) {
            sbuff.append("  Attributes\n");
            for (final Attribute a : ga) {
                sbuff.append("    ").append(a).append("\n");
            }
        }
        final List<VariableSimpleIF> vars = this.getDataVariables();
        sbuff.append("  Variables (").append(vars.size()).append(")\n");
        for (final VariableSimpleIF v : vars) {
            sbuff.append("    name='").append(v.getShortName()).append("' desc='").append(v.getDescription()).append("' units='").append(v.getUnitsString()).append("' type=").append(v.getDataType()).append("\n");
        }
        sbuff.append("\nparseInfo=\n");
        sbuff.append(this.parseInfo);
        sbuff.append("\n");
        return sbuff.toString();
    }
    
    public Date getStartDate() {
        return this.startDate;
    }
    
    public Date getEndDate() {
        return this.endDate;
    }
    
    public LatLonRect getBoundingBox() {
        return this.boundingBox;
    }
    
    public List<VariableSimpleIF> getDataVariables() {
        return this.dataVariables;
    }
    
    public VariableSimpleIF getDataVariable(final String shortName) {
        for (final VariableSimpleIF s : this.dataVariables) {
            final String ss = s.getShortName();
            if (shortName.equals(ss)) {
                return s;
            }
        }
        return null;
    }
}
