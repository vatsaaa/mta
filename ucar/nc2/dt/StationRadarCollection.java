// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.dt;

import ucar.unidata.util.Product;
import ucar.nc2.util.CancelTask;
import ucar.unidata.geoloc.LatLonRect;
import java.io.IOException;
import ucar.unidata.geoloc.Station;
import java.util.List;

public interface StationRadarCollection
{
    List<Station> getStations() throws IOException;
    
    List<Station> getStations(final LatLonRect p0, final CancelTask p1) throws IOException;
    
    boolean checkStationProduct(final Product p0);
    
    boolean checkStationProduct(final String p0, final Product p1);
    
    int getStationProductCount(final String p0);
}
