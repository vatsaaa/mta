// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.dt;

import java.util.ArrayList;
import java.io.IOException;
import java.util.List;

public class StationImpl extends ucar.unidata.geoloc.StationImpl
{
    protected List<StationObsDatatype> obsList;
    protected int count;
    
    public StationImpl() {
        this.count = -1;
    }
    
    public StationImpl(final String name, final String desc, final double lat, final double lon, final double alt) {
        super(name, desc, "", lat, lon, alt);
        this.count = -1;
    }
    
    public StationImpl(final String name, final String desc, final double lat, final double lon, final double alt, final int count) {
        this(name, desc, lat, lon, alt);
        this.count = count;
    }
    
    public int getNumObservations() {
        return (this.obsList == null) ? this.count : this.obsList.size();
    }
    
    public void incrCount() {
        ++this.count;
    }
    
    public List getObservations() throws IOException {
        if (this.obsList == null) {
            this.obsList = this.readObservations();
        }
        return this.obsList;
    }
    
    public void addObs(final StationObsDatatype sobs) {
        if (null == this.obsList) {
            this.obsList = new ArrayList<StationObsDatatype>();
        }
        this.obsList.add(sobs);
    }
    
    protected List<StationObsDatatype> readObservations() throws IOException {
        return null;
    }
}
