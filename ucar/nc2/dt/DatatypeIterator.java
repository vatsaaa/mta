// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.dt;

import ucar.nc2.Structure;
import java.io.IOException;
import ucar.ma2.StructureData;
import ucar.ma2.StructureDataIterator;

public abstract class DatatypeIterator implements DataIterator
{
    private StructureDataIterator structIter;
    private int recnum;
    
    protected abstract Object makeDatatypeWithData(final int p0, final StructureData p1) throws IOException;
    
    protected DatatypeIterator(final Structure struct, final int bufferSize) {
        this.recnum = 0;
        try {
            this.structIter = struct.getStructureIterator(bufferSize);
        }
        catch (IOException e) {
            throw new IllegalStateException(e.getMessage());
        }
    }
    
    public boolean hasNext() {
        try {
            return this.structIter.hasNext();
        }
        catch (IOException e) {
            throw new IllegalStateException(e.getMessage());
        }
    }
    
    public Object nextData() throws IOException {
        final StructureData sdata = this.structIter.next();
        return this.makeDatatypeWithData(this.recnum++, sdata);
    }
    
    public Object next() {
        StructureData sdata;
        try {
            sdata = this.structIter.next();
        }
        catch (IOException e) {
            throw new RuntimeException(e);
        }
        try {
            return this.makeDatatypeWithData(this.recnum++, sdata);
        }
        catch (IOException e) {
            throw new RuntimeException(e.getMessage());
        }
    }
    
    public void remove() {
        throw new UnsupportedOperationException();
    }
}
