// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.dt;

import ucar.ma2.Range;
import ucar.ma2.Array;
import ucar.ma2.InvalidRangeException;
import ucar.ma2.StructureData;
import ucar.unidata.geoloc.EarthLocation;
import ucar.unidata.geoloc.LatLonRect;
import java.util.Date;
import java.io.IOException;
import ucar.nc2.VariableSimpleIF;
import java.util.List;

public interface TrajectoryObsDatatype
{
    String getId();
    
    String getDescription();
    
    int getNumberPoints();
    
    List getDataVariables();
    
    VariableSimpleIF getDataVariable(final String p0);
    
    PointObsDatatype getPointObsData(final int p0) throws IOException;
    
    Date getStartDate();
    
    Date getEndDate();
    
    LatLonRect getBoundingBox();
    
    Date getTime(final int p0) throws IOException;
    
    EarthLocation getLocation(final int p0) throws IOException;
    
    double getTimeValue(final int p0) throws IOException;
    
    String getTimeUnitsIdentifier();
    
    double getLatitude(final int p0) throws IOException;
    
    double getLongitude(final int p0) throws IOException;
    
    double getElevation(final int p0) throws IOException;
    
    StructureData getData(final int p0) throws IOException, InvalidRangeException;
    
    Array getData(final int p0, final String p1) throws IOException;
    
    DataIterator getDataIterator(final int p0) throws IOException;
    
    Range getFullRange();
    
    Range getPointRange(final int p0) throws InvalidRangeException;
    
    Range getRange(final int p0, final int p1, final int p2) throws InvalidRangeException;
    
    Array getTime(final Range p0) throws IOException, InvalidRangeException;
    
    Array getLatitude(final Range p0) throws IOException, InvalidRangeException;
    
    Array getLongitude(final Range p0) throws IOException, InvalidRangeException;
    
    Array getElevation(final Range p0) throws IOException, InvalidRangeException;
    
    Array getData(final Range p0, final String p1) throws IOException, InvalidRangeException;
}
