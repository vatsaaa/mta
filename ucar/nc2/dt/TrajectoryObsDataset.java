// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.dt;

import java.util.List;

public interface TrajectoryObsDataset extends TypedDataset
{
    List<String> getTrajectoryIds();
    
    List getTrajectories();
    
    TrajectoryObsDatatype getTrajectory(final String p0);
    
    boolean syncExtend();
}
