// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.dt.trajectory;

import ucar.nc2.dataset.NetcdfDataset;
import java.io.IOException;
import ucar.nc2.util.CancelTask;
import ucar.nc2.dt.TrajectoryObsDataset;

public class TrajectoryObsDatasetFactory
{
    public static TrajectoryObsDataset open(final String netcdfFileURI) throws IOException {
        return open(netcdfFileURI, null);
    }
    
    public static TrajectoryObsDataset open(final String netcdfFileURI, final CancelTask cancelTask) throws IOException {
        final NetcdfDataset ds = NetcdfDataset.acquireDataset(netcdfFileURI, cancelTask);
        if (RafTrajectoryObsDataset.isValidFile(ds)) {
            return new RafTrajectoryObsDataset(ds);
        }
        if (SimpleTrajectoryObsDataset.isValidFile(ds)) {
            return new SimpleTrajectoryObsDataset(ds);
        }
        if (Float10TrajectoryObsDataset.isValidFile(ds)) {
            return new Float10TrajectoryObsDataset(ds);
        }
        if (ZebraClassTrajectoryObsDataset.isValidFile(ds)) {
            return new ZebraClassTrajectoryObsDataset(ds);
        }
        if (ARMTrajectoryObsDataset.isValidFile(ds)) {
            return new ARMTrajectoryObsDataset(ds);
        }
        return null;
    }
}
