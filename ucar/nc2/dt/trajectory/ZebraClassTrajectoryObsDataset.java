// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.dt.trajectory;

import ucar.nc2.constants.FeatureType;
import java.io.IOException;
import ucar.nc2.dt.TypedDataset;
import ucar.nc2.util.CancelTask;
import java.util.Date;
import ucar.nc2.Variable;
import java.util.List;
import ucar.nc2.Attribute;
import ucar.nc2.units.SimpleUnit;
import ucar.nc2.units.DateUnit;
import ucar.nc2.Dimension;
import ucar.nc2.dataset.NetcdfDataset;
import ucar.nc2.dt.TypedDatasetFactoryIF;

public class ZebraClassTrajectoryObsDataset extends SingleTrajectoryObsDataset implements TypedDatasetFactoryIF
{
    private static String timeDimName;
    private static String timeVarName;
    private static String latVarName;
    private static String lonVarName;
    private static String elevVarName;
    private static String trajId;
    
    public static boolean isValidFile(final NetcdfDataset ncd) {
        return buildConfig(ncd) != null;
    }
    
    private static Config buildConfig(final NetcdfDataset ncd) {
        Attribute attrib = ncd.findGlobalAttributeIgnoreCase("zebra_platform");
        if (attrib == null) {
            return null;
        }
        if (!attrib.isString()) {
            return null;
        }
        if (!attrib.getStringValue().equals("class")) {
            return null;
        }
        attrib = ncd.findGlobalAttributeIgnoreCase("history");
        if (attrib == null) {
            return null;
        }
        if (!attrib.isString()) {
            return null;
        }
        if (!attrib.getStringValue().equals("ClassNcFile")) {
            return null;
        }
        attrib = ncd.findGlobalAttributeIgnoreCase("AvapsEditorVersion");
        if (attrib == null) {
            return null;
        }
        if (!attrib.isString()) {
            return null;
        }
        attrib = ncd.findGlobalAttributeIgnoreCase("SoundingDescription");
        if (attrib == null) {
            return null;
        }
        if (!attrib.isString()) {
            return null;
        }
        List list = ncd.getRootGroup().getDimensions();
        if (list.size() != 1) {
            return null;
        }
        Dimension d = list.get(0);
        if (!d.getName().equals(ZebraClassTrajectoryObsDataset.timeDimName)) {
            return null;
        }
        final Config trajConfig = new Config();
        trajConfig.setTimeDim(d);
        Variable var = ncd.getRootGroup().findVariable(ZebraClassTrajectoryObsDataset.timeVarName);
        if (var == null) {
            return null;
        }
        list = var.getDimensions();
        if (list.size() != 1) {
            return null;
        }
        d = list.get(0);
        if (!d.getName().equals(ZebraClassTrajectoryObsDataset.timeDimName)) {
            return null;
        }
        String units = var.findAttribute("units").getStringValue();
        final Date date = DateUnit.getStandardDate("0 " + units);
        if (date == null) {
            return null;
        }
        trajConfig.setTimeVar(var);
        var = ncd.getRootGroup().findVariable(ZebraClassTrajectoryObsDataset.latVarName);
        if (var == null) {
            return null;
        }
        list = var.getDimensions();
        if (list.size() != 1) {
            return null;
        }
        d = list.get(0);
        if (!d.getName().equals(ZebraClassTrajectoryObsDataset.timeDimName)) {
            return null;
        }
        units = var.findAttribute("units").getStringValue();
        if (!SimpleUnit.isCompatible(units, "degrees_north")) {
            return null;
        }
        trajConfig.setLatVar(var);
        var = ncd.getRootGroup().findVariable(ZebraClassTrajectoryObsDataset.lonVarName);
        if (var == null) {
            return null;
        }
        list = var.getDimensions();
        if (list.size() != 1) {
            return null;
        }
        d = list.get(0);
        if (!d.getName().equals(ZebraClassTrajectoryObsDataset.timeDimName)) {
            return null;
        }
        units = var.findAttribute("units").getStringValue();
        if (!SimpleUnit.isCompatible(units, "degrees_east")) {
            return null;
        }
        trajConfig.setLonVar(var);
        var = ncd.getRootGroup().findVariable(ZebraClassTrajectoryObsDataset.elevVarName);
        if (var == null) {
            return null;
        }
        list = var.getDimensions();
        if (list.size() != 1) {
            return null;
        }
        d = list.get(0);
        if (!d.getName().equals(ZebraClassTrajectoryObsDataset.timeDimName)) {
            return null;
        }
        units = var.findAttribute("units").getStringValue();
        if (!SimpleUnit.isCompatible(units, "meters")) {
            return null;
        }
        trajConfig.setElevVar(var);
        trajConfig.setTrajectoryId(ZebraClassTrajectoryObsDataset.trajId);
        return trajConfig;
    }
    
    public boolean isMine(final NetcdfDataset ds) {
        return isValidFile(ds);
    }
    
    public TypedDataset open(final NetcdfDataset ncd, final CancelTask task, final StringBuilder errlog) throws IOException {
        return new ZebraClassTrajectoryObsDataset(ncd);
    }
    
    public FeatureType getScientificDataType() {
        return FeatureType.TRAJECTORY;
    }
    
    public ZebraClassTrajectoryObsDataset() {
    }
    
    public ZebraClassTrajectoryObsDataset(final NetcdfDataset ncd) throws IOException {
        super(ncd);
        final Config trajConfig = buildConfig(ncd);
        this.setTrajectoryInfo(trajConfig);
    }
    
    static {
        ZebraClassTrajectoryObsDataset.timeDimName = "time";
        ZebraClassTrajectoryObsDataset.timeVarName = "time";
        ZebraClassTrajectoryObsDataset.latVarName = "lat";
        ZebraClassTrajectoryObsDataset.lonVarName = "lon";
        ZebraClassTrajectoryObsDataset.elevVarName = "alt";
        ZebraClassTrajectoryObsDataset.trajId = "trajectory data";
    }
}
