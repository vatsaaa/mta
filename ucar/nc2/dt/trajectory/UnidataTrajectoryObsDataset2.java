// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.dt.trajectory;

import ucar.nc2.dt.TrajectoryObsDatatype;
import ucar.nc2.VariableSimpleIF;
import ucar.unidata.geoloc.LatLonRect;
import java.util.Date;
import java.util.List;
import ucar.nc2.dt.point.UnidataObsDatasetHelper;
import ucar.nc2.constants.AxisType;
import java.io.IOException;
import ucar.nc2.dt.TypedDataset;
import ucar.nc2.util.CancelTask;
import ucar.nc2.dataset.NetcdfDataset;
import ucar.nc2.Attribute;
import java.util.StringTokenizer;
import ucar.nc2.constants.FeatureType;
import ucar.nc2.NetcdfFile;
import ucar.nc2.Structure;
import ucar.nc2.Dimension;
import ucar.nc2.Variable;
import ucar.nc2.dt.TypedDatasetFactoryIF;
import ucar.nc2.dt.TrajectoryObsDataset;
import ucar.nc2.dt.TypedDatasetImpl;

public class UnidataTrajectoryObsDataset2 extends TypedDatasetImpl implements TrajectoryObsDataset, TypedDatasetFactoryIF
{
    protected Variable trajVar;
    protected Dimension trajDim;
    protected Variable timeVar;
    protected Dimension timeDim;
    protected Structure recordVar;
    protected Variable latVar;
    protected Variable lonVar;
    protected Variable elevVar;
    protected String trajDimName;
    protected String trajVarName;
    protected String timeDimName;
    protected String timeVarName;
    protected String latVarName;
    protected String lonVarName;
    protected String elevVarName;
    protected boolean isMultiTrajStructure;
    protected boolean isTimeDimensionFirst;
    protected TrajectoryObsDataset backingTraj;
    
    public static boolean isValidFile(final NetcdfFile ds) {
        Attribute cdmDtAtt = ds.findGlobalAttributeIgnoreCase("cdm_data_type");
        if (cdmDtAtt == null) {
            cdmDtAtt = ds.findGlobalAttributeIgnoreCase("cdm_datatype");
        }
        if (cdmDtAtt == null) {
            return false;
        }
        if (!cdmDtAtt.isString()) {
            return false;
        }
        final String cdmDtString = cdmDtAtt.getStringValue();
        if (cdmDtString == null) {
            return false;
        }
        if (!cdmDtString.equalsIgnoreCase(FeatureType.TRAJECTORY.toString())) {
            return false;
        }
        final Attribute conventionsAtt = ds.findGlobalAttributeIgnoreCase("Conventions");
        if (conventionsAtt == null) {
            return false;
        }
        if (!conventionsAtt.isString()) {
            return false;
        }
        final String convString = conventionsAtt.getStringValue();
        final StringTokenizer stoke = new StringTokenizer(convString, ",");
        while (stoke.hasMoreTokens()) {
            final String toke = stoke.nextToken().trim();
            if (toke.equalsIgnoreCase("Unidata Observation Dataset v1.0")) {
                return true;
            }
        }
        return false;
    }
    
    public boolean isMine(final NetcdfDataset ds) {
        return isValidFile(ds);
    }
    
    public TypedDataset open(final NetcdfDataset ncd, final CancelTask task, final StringBuilder errlog) throws IOException {
        return new UnidataTrajectoryObsDataset(ncd);
    }
    
    public FeatureType getScientificDataType() {
        return FeatureType.TRAJECTORY;
    }
    
    public UnidataTrajectoryObsDataset2() {
    }
    
    public UnidataTrajectoryObsDataset2(final NetcdfDataset ncd) throws IOException {
        super(ncd);
        this.timeVar = UnidataObsDatasetHelper.getCoordinate(ncd, AxisType.Time);
        if (this.timeVar == null) {
            throw new IllegalArgumentException("Dataset has no time coordinate variable.");
        }
        final int timeVarNumOfDims = this.timeVar.getDimensions().size();
        if (timeVarNumOfDims != 1) {
            throw new IllegalArgumentException("Dataset time variable does not have exactly one (1) dimension [" + timeVarNumOfDims + "].");
        }
        this.timeVarName = this.timeVar.getName();
        this.timeDim = this.timeVar.getDimension(0);
        this.timeDimName = this.timeDim.getName();
        this.latVar = UnidataObsDatasetHelper.getCoordinate(ncd, AxisType.Lat);
        if (this.latVar == null) {
            throw new IllegalArgumentException("Dataset has no Latitude/GeoY variable.");
        }
        final List<Dimension> latVarDimList = this.latVar.getDimensions();
        final int latVarNumOfDims = latVarDimList.size();
        if (latVarNumOfDims == 1) {
            this.isMultiTrajStructure = false;
        }
        if (latVarNumOfDims != 2) {
            throw new IllegalArgumentException("Dataset Latitude/GeoY variable does not have 1 or 2 dimensions [" + latVarNumOfDims + "].");
        }
        this.isMultiTrajStructure = true;
        if (latVarDimList.get(0).equals(this.timeDim)) {
            this.isTimeDimensionFirst = true;
            this.trajDim = latVarDimList.get(1);
            this.trajDimName = this.trajDim.getName();
        }
        else {
            if (!latVarDimList.get(1).equals(this.timeDim)) {
                throw new IllegalArgumentException("Dataset Latitude/GeoY variable has no time dimension. ");
            }
            this.isTimeDimensionFirst = false;
            this.trajDim = latVarDimList.get(0);
            this.trajDimName = this.trajDim.getName();
        }
        if (this.isMultiTrajStructure) {
            this.trajVarName = this.trajDimName;
            this.trajVar = ncd.findTopVariable(this.trajVarName);
        }
        this.lonVar = UnidataObsDatasetHelper.getCoordinate(ncd, AxisType.Lon);
        if (this.lonVar == null) {
            throw new IllegalArgumentException("Missing Longitude/GeoX coordinate variable.");
        }
        this.elevVar = UnidataObsDatasetHelper.getCoordinate(ncd, AxisType.Height);
        this.timeDimName = this.timeVar.getDimension(0).getName();
        this.timeVarName = this.timeVar.getName();
        this.latVarName = this.latVar.getName();
        this.lonVarName = this.lonVar.getName();
        this.elevVarName = ((this.elevVar != null) ? this.elevVar.getName() : null);
        if (!this.isMultiTrajStructure) {
            this.backingTraj = new SingleTrajectoryObsDataset(ncd);
            final SingleTrajectoryObsDataset.Config trajConfig = new SingleTrajectoryObsDataset.Config("1Hz data", ncd.getRootGroup().findDimension(this.timeDimName), ncd.getRootGroup().findVariable(this.timeVarName), ncd.getRootGroup().findVariable(this.latVarName), ncd.getRootGroup().findVariable(this.lonVarName), ncd.getRootGroup().findVariable(this.elevVarName));
            ((SingleTrajectoryObsDataset)this.backingTraj).setTrajectoryInfo(trajConfig);
        }
        else {
            this.backingTraj = new MultiTrajectoryObsDataset(ncd);
            ((MultiTrajectoryObsDataset)this.backingTraj).setTrajectoryInfo(this.ncfile.getRootGroup().findDimension(this.trajDimName), this.ncfile.getRootGroup().findVariable(this.trajVarName), this.ncfile.getRootGroup().findDimension(this.timeDimName), this.ncfile.getRootGroup().findVariable(this.timeVarName), this.ncfile.getRootGroup().findVariable(this.latVarName), this.ncfile.getRootGroup().findVariable(this.lonVarName), this.ncfile.getRootGroup().findVariable(this.elevVarName));
        }
    }
    
    @Override
    public String getDetailInfo() {
        return this.backingTraj.getDetailInfo();
    }
    
    @Override
    public String getTitle() {
        return this.backingTraj.getTitle();
    }
    
    @Override
    public String getDescription() {
        return this.backingTraj.getDescription();
    }
    
    @Override
    public String getLocation() {
        return this.backingTraj.getLocationURI();
    }
    
    @Override
    protected void setStartDate() {
    }
    
    @Override
    public Date getStartDate() {
        return this.backingTraj.getStartDate();
    }
    
    @Override
    protected void setEndDate() {
    }
    
    @Override
    public Date getEndDate() {
        return this.backingTraj.getEndDate();
    }
    
    @Override
    protected void setBoundingBox() {
    }
    
    @Override
    public LatLonRect getBoundingBox() {
        return this.backingTraj.getBoundingBox();
    }
    
    @Override
    public List<Attribute> getGlobalAttributes() {
        return this.backingTraj.getGlobalAttributes();
    }
    
    @Override
    public Attribute findGlobalAttributeIgnoreCase(final String name) {
        return this.backingTraj.findGlobalAttributeIgnoreCase(name);
    }
    
    @Override
    public List<VariableSimpleIF> getDataVariables() {
        return this.backingTraj.getDataVariables();
    }
    
    @Override
    public VariableSimpleIF getDataVariable(final String shortName) {
        return this.backingTraj.getDataVariable(shortName);
    }
    
    @Override
    public NetcdfFile getNetcdfFile() {
        return this.backingTraj.getNetcdfFile();
    }
    
    @Override
    public void close() throws IOException {
        this.backingTraj.close();
    }
    
    public List<String> getTrajectoryIds() {
        return this.backingTraj.getTrajectoryIds();
    }
    
    public List getTrajectories() {
        return this.backingTraj.getTrajectories();
    }
    
    public TrajectoryObsDatatype getTrajectory(final String trajectoryId) {
        return this.backingTraj.getTrajectory(trajectoryId);
    }
    
    public boolean syncExtend() {
        return this.backingTraj.syncExtend();
    }
}
