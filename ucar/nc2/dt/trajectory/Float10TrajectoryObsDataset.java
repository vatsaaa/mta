// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.dt.trajectory;

import ucar.nc2.Attribute;
import ucar.nc2.constants.FeatureType;
import java.io.IOException;
import ucar.nc2.dt.TypedDataset;
import ucar.nc2.util.CancelTask;
import java.util.Date;
import ucar.nc2.Variable;
import java.util.List;
import ucar.nc2.units.SimpleUnit;
import ucar.nc2.units.DateUnit;
import ucar.nc2.Dimension;
import ucar.nc2.dataset.NetcdfDataset;
import ucar.nc2.dt.TypedDatasetFactoryIF;

public class Float10TrajectoryObsDataset extends MultiTrajectoryObsDataset implements TypedDatasetFactoryIF
{
    private static String trajDimNameDefault;
    private static String trajVarNameDefault;
    private static String timeDimNameDefault;
    private static String timeVarNameDefault;
    private static String latVarNameDefault;
    private static String lonVarNameDefault;
    private static String elevVarNameDefault;
    private String trajDimName;
    private String trajVarName;
    private String timeDimName;
    private String timeVarName;
    private String latVarName;
    private String lonVarName;
    private String elevVarName;
    
    public static boolean isValidFile(final NetcdfDataset ds) {
        List list = ds.getRootGroup().getDimensions();
        if (list.size() != 2) {
            return false;
        }
        for (int i = 0; i < 2; ++i) {
            final Dimension d = list.get(i);
            if (!d.getName().equals(Float10TrajectoryObsDataset.timeDimNameDefault) && !d.getName().equals(Float10TrajectoryObsDataset.trajDimNameDefault)) {
                return false;
            }
        }
        Variable var = ds.getRootGroup().findVariable(Float10TrajectoryObsDataset.trajVarNameDefault);
        if (var == null) {
            return false;
        }
        list = var.getDimensions();
        if (list.size() != 1) {
            return false;
        }
        Dimension d = list.get(0);
        if (!d.getName().equals(Float10TrajectoryObsDataset.trajDimNameDefault)) {
            return false;
        }
        var = ds.getRootGroup().findVariable(Float10TrajectoryObsDataset.timeVarNameDefault);
        if (var == null) {
            return false;
        }
        list = var.getDimensions();
        if (list.size() != 1) {
            return false;
        }
        d = list.get(0);
        if (!d.getName().equals(Float10TrajectoryObsDataset.timeDimNameDefault)) {
            return false;
        }
        String units = var.findAttribute("units").getStringValue();
        final Date date = DateUnit.getStandardDate("0 " + units);
        if (date == null) {
            return false;
        }
        var = ds.getRootGroup().findVariable(Float10TrajectoryObsDataset.latVarNameDefault);
        if (var == null) {
            return false;
        }
        list = var.getDimensions();
        if (list.size() != 2) {
            return false;
        }
        for (int j = 0; j < 2; ++j) {
            d = list.get(j);
            if (!d.getName().equals(Float10TrajectoryObsDataset.timeDimNameDefault) && !d.getName().equals(Float10TrajectoryObsDataset.trajDimNameDefault)) {
                return false;
            }
        }
        var = ds.getRootGroup().findVariable(Float10TrajectoryObsDataset.lonVarNameDefault);
        if (var == null) {
            return false;
        }
        list = var.getDimensions();
        if (list.size() != 2) {
            return false;
        }
        for (int j = 0; j < 2; ++j) {
            d = list.get(j);
            if (!d.getName().equals(Float10TrajectoryObsDataset.timeDimNameDefault) && !d.getName().equals(Float10TrajectoryObsDataset.trajDimNameDefault)) {
                return false;
            }
        }
        var = ds.getRootGroup().findVariable(Float10TrajectoryObsDataset.elevVarNameDefault);
        if (var == null) {
            return false;
        }
        list = var.getDimensions();
        if (list.size() != 2) {
            return false;
        }
        for (int j = 0; j < 2; ++j) {
            d = list.get(j);
            if (!d.getName().equals(Float10TrajectoryObsDataset.timeDimNameDefault) && !d.getName().equals(Float10TrajectoryObsDataset.trajDimNameDefault)) {
                return false;
            }
        }
        units = var.findAttribute("units").getStringValue();
        return SimpleUnit.isCompatible(units, "m");
    }
    
    public boolean isMine(final NetcdfDataset ds) {
        return isValidFile(ds);
    }
    
    public TypedDataset open(final NetcdfDataset ncd, final CancelTask task, final StringBuilder errlog) throws IOException {
        return new Float10TrajectoryObsDataset(ncd);
    }
    
    public FeatureType getScientificDataType() {
        return FeatureType.TRAJECTORY;
    }
    
    public Float10TrajectoryObsDataset() {
    }
    
    public Float10TrajectoryObsDataset(final NetcdfDataset ncd) throws IOException {
        super(ncd);
        this.trajDimName = Float10TrajectoryObsDataset.trajDimNameDefault;
        this.trajVarName = Float10TrajectoryObsDataset.trajVarNameDefault;
        this.timeDimName = Float10TrajectoryObsDataset.timeDimNameDefault;
        this.timeVarName = Float10TrajectoryObsDataset.timeVarNameDefault;
        this.latVarName = Float10TrajectoryObsDataset.latVarNameDefault;
        this.lonVarName = Float10TrajectoryObsDataset.lonVarNameDefault;
        this.elevVarName = Float10TrajectoryObsDataset.elevVarNameDefault;
        final Variable latVar = ncd.getRootGroup().findVariable(this.latVarName);
        latVar.addAttribute(new Attribute("units", "degrees_north"));
        final Variable lonVar = ncd.getRootGroup().findVariable(this.lonVarName);
        lonVar.addAttribute(new Attribute("units", "degrees_east"));
        this.setTrajectoryInfo(this.ncfile.getRootGroup().findDimension(this.trajDimName), this.ncfile.getRootGroup().findVariable(this.trajVarName), this.ncfile.getRootGroup().findDimension(this.timeDimName), this.ncfile.getRootGroup().findVariable(this.timeVarName), this.ncfile.getRootGroup().findVariable(this.latVarName), this.ncfile.getRootGroup().findVariable(this.lonVarName), this.ncfile.getRootGroup().findVariable(this.elevVarName));
    }
    
    static {
        Float10TrajectoryObsDataset.trajDimNameDefault = "DRIFTER100_110";
        Float10TrajectoryObsDataset.trajVarNameDefault = "DRIFTER100_110";
        Float10TrajectoryObsDataset.timeDimNameDefault = "TIME1";
        Float10TrajectoryObsDataset.timeVarNameDefault = "TIME1";
        Float10TrajectoryObsDataset.latVarNameDefault = "LATITUDE";
        Float10TrajectoryObsDataset.lonVarNameDefault = "LONGITUDE";
        Float10TrajectoryObsDataset.elevVarNameDefault = "DEPTH";
    }
}
