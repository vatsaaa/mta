// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.dt.trajectory;

import ucar.ma2.IndexIterator;
import java.util.List;
import ucar.ma2.Range;
import java.util.ArrayList;
import ucar.ma2.Array;
import java.util.Date;
import ucar.nc2.Variable;
import ucar.nc2.units.SimpleUnit;
import ucar.nc2.units.DateFormatter;
import ucar.ma2.InvalidRangeException;
import java.util.Calendar;
import java.util.Locale;
import java.util.TimeZone;
import ucar.nc2.units.DateUnit;
import ucar.nc2.constants.FeatureType;
import java.io.IOException;
import ucar.nc2.dt.TypedDataset;
import ucar.nc2.util.CancelTask;
import ucar.nc2.Group;
import ucar.nc2.Attribute;
import ucar.nc2.dataset.NetcdfDataset;
import ucar.nc2.dt.TypedDatasetFactoryIF;

public class RafTrajectoryObsDataset extends SingleTrajectoryObsDataset implements TypedDatasetFactoryIF
{
    private String timeDimName;
    private String timeVarName;
    private String latVarName;
    private String lonVarName;
    private String elevVarName;
    
    public static boolean isValidFile(final NetcdfDataset ds) {
        Attribute conventionsAtt = ds.findGlobalAttribute("Conventions");
        if (conventionsAtt == null) {
            conventionsAtt = ds.findGlobalAttributeIgnoreCase("Conventions");
        }
        if (conventionsAtt == null) {
            return false;
        }
        if (!conventionsAtt.isString()) {
            return false;
        }
        if (!conventionsAtt.getStringValue().equals("NCAR-RAF/nimbus")) {
            return false;
        }
        Attribute versionAtt = ds.findGlobalAttributeIgnoreCase("Version");
        if (versionAtt == null) {
            versionAtt = new Attribute("Version", "1.3");
            ds.addAttribute(null, versionAtt);
            ds.finish();
            return true;
        }
        return versionAtt.isString() && (versionAtt.getStringValue().equals("1.2") || versionAtt.getStringValue().equals("1.3"));
    }
    
    public boolean isMine(final NetcdfDataset ds) {
        return isValidFile(ds);
    }
    
    public TypedDataset open(final NetcdfDataset ncd, final CancelTask task, final StringBuilder errlog) throws IOException {
        return new RafTrajectoryObsDataset(ncd);
    }
    
    public FeatureType getScientificDataType() {
        return FeatureType.TRAJECTORY;
    }
    
    public RafTrajectoryObsDataset() {
    }
    
    public RafTrajectoryObsDataset(final NetcdfDataset ncf) throws IOException {
        super(ncf);
        Attribute conventionsAtt = ncf.findGlobalAttribute("Conventions");
        if (conventionsAtt == null) {
            conventionsAtt = ncf.findGlobalAttributeIgnoreCase("Conventions");
        }
        if (conventionsAtt == null) {
            throw new IllegalArgumentException("File <" + ncf.getId() + "> not a \"NCAR-RAF/nimbus\" convention file.");
        }
        if (!conventionsAtt.getStringValue().equals("NCAR-RAF/nimbus")) {
            throw new IllegalArgumentException("File <" + ncf.getId() + "> not a \"NCAR-RAF/nimbus\" convention file.");
        }
        final Attribute versionAtt = ncf.findGlobalAttributeIgnoreCase("Version");
        if (versionAtt.getStringValue().equals("1.2")) {
            this.timeDimName = "Time";
            this.timeVarName = "time_offset";
            this.latVarName = "LAT";
            this.lonVarName = "LON";
            this.elevVarName = "ALT";
            final String baseTimeVarName = "base_time";
            final Variable baseTimeVar = this.ncfile.findVariable(baseTimeVarName);
            final int baseTime = baseTimeVar.readScalarInt();
            Date baseTimeDate;
            if (baseTime != 0) {
                final String baseTimeString = baseTime + " seconds since 1970-01-01T00:00:00";
                baseTimeDate = DateUnit.getStandardDate(baseTimeString);
            }
            else {
                final Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("GMT"), Locale.US);
                Array yearArray;
                Array monthArray;
                Array dayArray;
                Array hourArray;
                Array minuteArray;
                Array secondArray;
                Array tmpTimeArray;
                try {
                    yearArray = ncf.findVariable("YEAR").read("0");
                    monthArray = ncf.findVariable("MONTH").read("0");
                    dayArray = ncf.findVariable("DAY").read("0");
                    hourArray = ncf.findVariable("HOUR").read("0");
                    minuteArray = ncf.findVariable("MINUTE").read("0");
                    secondArray = ncf.findVariable("SECOND").read("0");
                    tmpTimeArray = ncf.findVariable(this.timeVarName).read("0");
                }
                catch (InvalidRangeException e) {
                    throw new IOException("Failed while reading first value of YEAR, MONTH, DAY, HOUR, MINUTE, SECOND, or time_offset: " + e.getMessage());
                }
                calendar.clear();
                calendar.set(1, (int)yearArray.getFloat(yearArray.getIndex()));
                calendar.set(2, (int)monthArray.getFloat(monthArray.getIndex()));
                calendar.set(5, (int)dayArray.getFloat(dayArray.getIndex()));
                calendar.set(11, (int)hourArray.getFloat(hourArray.getIndex()));
                calendar.set(12, (int)minuteArray.getFloat(minuteArray.getIndex()));
                calendar.set(13, (int)secondArray.getFloat(secondArray.getIndex()));
                calendar.set(14, 0);
                calendar.add(13, -(int)tmpTimeArray.getFloat(tmpTimeArray.getIndex()));
                baseTimeDate = calendar.getTime();
            }
            final DateFormatter formatter = new DateFormatter();
            final String timeUnitsString = "seconds since " + formatter.toDateTimeStringISO(baseTimeDate);
            this.ncfile.findVariable(this.timeVarName).addAttribute(new Attribute("units", timeUnitsString));
            final String elevVarUnitsString = this.ncfile.findVariable(this.elevVarName).findAttribute("units").getStringValue();
            if (!SimpleUnit.isCompatible(elevVarUnitsString, "meters") && elevVarUnitsString.equals("M")) {
                this.ncfile.findVariable(this.elevVarName).addAttribute(new Attribute("units", "meters"));
            }
        }
        else {
            if (!versionAtt.getStringValue().equals("1.3")) {
                throw new IllegalArgumentException("File <" + ncf.getId() + "> not a version 1.2 or 1.3 \"NCAR-RAF/nimbus\" convention file.");
            }
            this.timeDimName = "Time";
            this.timeVarName = "Time";
            this.latVarName = "LAT";
            this.lonVarName = "LON";
            this.elevVarName = "ALT";
            final Attribute coordsAttrib = this.ncfile.findGlobalAttribute("coordinates");
            if (coordsAttrib != null) {
                final String coordsAttribValue = coordsAttrib.getStringValue();
                if (coordsAttribValue != null) {
                    final String[] varNames = coordsAttribValue.split(" ");
                    this.latVarName = varNames[1];
                    this.lonVarName = varNames[0];
                    this.elevVarName = varNames[2];
                    this.timeVarName = varNames[3];
                    this.timeDimName = this.timeVarName;
                }
            }
            if (this.timeVarAllZeros()) {
                this.timeVarName = "time_offset";
            }
            String varUnitsString = this.ncfile.findVariable(this.latVarName).findAttributeIgnoreCase("units").getStringValue();
            if (!SimpleUnit.isCompatible(varUnitsString, "degrees_north")) {
                throw new IllegalStateException("Latitude variable <" + this.latVarName + "> units not udunits compatible w/ \"degrees_north\".");
            }
            varUnitsString = this.ncfile.findVariable(this.lonVarName).findAttributeIgnoreCase("units").getStringValue();
            if (!SimpleUnit.isCompatible(varUnitsString, "degrees_east")) {
                throw new IllegalStateException("Longitude variable <" + this.lonVarName + "> units not udunits compatible w/ \"degrees_east\".");
            }
            varUnitsString = this.ncfile.findVariable(this.elevVarName).findAttributeIgnoreCase("units").getStringValue();
            if (!SimpleUnit.isCompatible(varUnitsString, "meters")) {
                throw new IllegalStateException("Elevation variable <" + this.elevVarName + "> units not udunits compatible w/ \"m\".");
            }
            final String timeUnitsString2 = this.ncfile.findVariable(this.timeVarName).findAttributeIgnoreCase("units").getStringValue();
            if (!SimpleUnit.isCompatible(timeUnitsString2, "seconds since 1970-01-01 00:00:00")) {
                throw new IllegalStateException("Time variable units <" + timeUnitsString2 + "> not udunits compatible w/ \"seconds since 1970-01-01 00:00:00\".");
            }
        }
        final Config trajConfig = new Config("1Hz data", ncf.getRootGroup().findDimension(this.timeDimName), ncf.getRootGroup().findVariable(this.timeVarName), ncf.getRootGroup().findVariable(this.latVarName), ncf.getRootGroup().findVariable(this.lonVarName), ncf.getRootGroup().findVariable(this.elevVarName));
        this.setTrajectoryInfo(trajConfig);
    }
    
    private boolean timeVarAllZeros() throws IOException {
        final Variable curTimeVar = this.ncfile.getRootGroup().findVariable(this.timeVarName);
        final List section = new ArrayList(1);
        Array a = null;
        try {
            section.add(new Range(0, 2));
            a = curTimeVar.read(section);
        }
        catch (InvalidRangeException e) {
            throw new IOException("Invalid range (0,2): " + e.getMessage());
        }
        final IndexIterator it = a.getIndexIterator();
        while (it.hasNext()) {
            if (it.getDoubleNext() != 0.0) {
                return false;
            }
        }
        return true;
    }
}
