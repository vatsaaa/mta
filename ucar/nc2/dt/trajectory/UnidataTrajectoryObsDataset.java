// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.dt.trajectory;

import ucar.nc2.dt.point.UnidataObsDatasetHelper;
import ucar.nc2.constants.AxisType;
import java.io.IOException;
import ucar.nc2.dt.TypedDataset;
import ucar.nc2.util.CancelTask;
import ucar.nc2.dataset.NetcdfDataset;
import ucar.nc2.Attribute;
import java.util.StringTokenizer;
import ucar.nc2.constants.FeatureType;
import ucar.nc2.NetcdfFile;
import ucar.nc2.dt.TypedDatasetFactoryIF;

public class UnidataTrajectoryObsDataset extends SingleTrajectoryObsDataset implements TypedDatasetFactoryIF
{
    private String timeDimName;
    private String timeVarName;
    private String latVarName;
    private String lonVarName;
    private String elevVarName;
    
    public static boolean isValidFile(final NetcdfFile ds) {
        Attribute cdmDtAtt = ds.findGlobalAttributeIgnoreCase("cdm_data_type");
        if (cdmDtAtt == null) {
            cdmDtAtt = ds.findGlobalAttributeIgnoreCase("cdm_datatype");
        }
        if (cdmDtAtt == null) {
            return false;
        }
        if (!cdmDtAtt.isString()) {
            return false;
        }
        final String cdmDtString = cdmDtAtt.getStringValue();
        if (cdmDtString == null) {
            return false;
        }
        if (!cdmDtString.equalsIgnoreCase(FeatureType.TRAJECTORY.toString())) {
            return false;
        }
        final Attribute conventionsAtt = ds.findGlobalAttributeIgnoreCase("Conventions");
        if (conventionsAtt == null) {
            return false;
        }
        if (!conventionsAtt.isString()) {
            return false;
        }
        final String convString = conventionsAtt.getStringValue();
        final StringTokenizer stoke = new StringTokenizer(convString, ",");
        while (stoke.hasMoreTokens()) {
            final String toke = stoke.nextToken().trim();
            if (toke.equalsIgnoreCase("Unidata Observation Dataset v1.0")) {
                return true;
            }
        }
        return false;
    }
    
    public boolean isMine(final NetcdfDataset ds) {
        return isValidFile(ds);
    }
    
    public TypedDataset open(final NetcdfDataset ncd, final CancelTask task, final StringBuilder errlog) throws IOException {
        return new UnidataTrajectoryObsDataset(ncd);
    }
    
    public FeatureType getScientificDataType() {
        return FeatureType.TRAJECTORY;
    }
    
    public UnidataTrajectoryObsDataset() {
    }
    
    public UnidataTrajectoryObsDataset(final NetcdfDataset ncd) throws IOException {
        super(ncd);
        this.latVar = UnidataObsDatasetHelper.getCoordinate(ncd, AxisType.Lat);
        this.lonVar = UnidataObsDatasetHelper.getCoordinate(ncd, AxisType.Lon);
        this.timeVar = UnidataObsDatasetHelper.getCoordinate(ncd, AxisType.Time);
        this.elevVar = UnidataObsDatasetHelper.getCoordinate(ncd, AxisType.Height);
        if (this.latVar == null) {
            throw new IllegalStateException("Missing latitude variable");
        }
        if (this.lonVar == null) {
            throw new IllegalStateException("Missing longitude coordinate variable");
        }
        if (this.timeVar == null) {
            throw new IllegalStateException("Missing time coordinate variable");
        }
        if (this.elevVar == null) {
            throw new IllegalStateException("Missing height coordinate variable");
        }
        this.timeDimName = this.timeVar.getDimension(0).getName();
        this.timeVarName = this.timeVar.getName();
        this.latVarName = this.latVar.getName();
        this.lonVarName = this.lonVar.getName();
        this.elevVarName = this.elevVar.getName();
        final Config trajConfig = new Config("1Hz data", ncd.getRootGroup().findDimension(this.timeDimName), ncd.getRootGroup().findVariable(this.timeVarName), ncd.getRootGroup().findVariable(this.latVarName), ncd.getRootGroup().findVariable(this.lonVarName), ncd.getRootGroup().findVariable(this.elevVarName));
        this.setTrajectoryInfo(trajConfig);
    }
}
