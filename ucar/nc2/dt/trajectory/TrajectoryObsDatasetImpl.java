// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.dt.trajectory;

import ucar.nc2.dt.VariableSimpleSubclass;
import ucar.unidata.geoloc.EarthLocationImpl;
import ucar.nc2.dt.DatatypeIterator;
import ucar.nc2.dt.DataIterator;
import ucar.ma2.IndexIterator;
import ucar.ma2.DataType;
import ucar.ma2.Range;
import ucar.ma2.StructureData;
import ucar.ma2.Array;
import ucar.ma2.ArrayInt;
import ucar.ma2.ArrayFloat;
import ucar.ma2.ArrayDouble;
import ucar.ma2.InvalidRangeException;
import ucar.unidata.geoloc.EarthLocation;
import ucar.nc2.units.DateUnit;
import ucar.unidata.geoloc.LatLonRect;
import ucar.nc2.dt.PointObsDatatype;
import ucar.nc2.VariableSimpleIF;
import java.util.Date;
import java.util.ArrayList;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import ucar.nc2.dataset.VariableDS;
import ucar.nc2.Attribute;
import ucar.nc2.Group;
import ucar.nc2.NetcdfFile;
import ucar.nc2.StructurePseudo;
import ucar.nc2.units.SimpleUnit;
import ucar.nc2.dataset.NetcdfDataset;
import ucar.nc2.dt.TrajectoryObsDatatype;
import ucar.nc2.Structure;
import ucar.nc2.Variable;
import ucar.nc2.Dimension;
import java.util.HashMap;
import ucar.nc2.dt.TrajectoryObsDataset;
import ucar.nc2.dt.TypedDatasetImpl;

public abstract class TrajectoryObsDatasetImpl extends TypedDatasetImpl implements TrajectoryObsDataset
{
    protected String trajectoryId;
    protected int trajectoryNumPoint;
    protected HashMap trajectoryVarsMap;
    protected Dimension trajectoryDim;
    protected Variable dimVar;
    protected Structure recordVar;
    protected Variable latVar;
    protected Variable lonVar;
    protected Variable elevVar;
    protected String dimVarUnitsString;
    protected double elevVarUnitsConversionFactor;
    protected TrajectoryObsDatatype trajectory;
    
    public TrajectoryObsDatasetImpl() {
    }
    
    public TrajectoryObsDatasetImpl(final NetcdfDataset ncfile) {
        super(ncfile);
    }
    
    public void setTrajectoryInfo(final Config trajConfig) throws IOException {
        if (this.trajectoryDim != null) {
            throw new IllegalStateException("The setTrajectoryInfo() method can only be called once.");
        }
        this.trajectoryId = trajConfig.getTrajectoryId();
        this.trajectoryDim = trajConfig.getTrajectoryDim();
        this.dimVar = trajConfig.getDimensionVar();
        this.latVar = trajConfig.getLatVar();
        this.lonVar = trajConfig.getLonVar();
        this.elevVar = trajConfig.getElevVar();
        this.trajectoryNumPoint = this.trajectoryDim.getLength();
        this.dimVarUnitsString = this.dimVar.findAttribute("units").getStringValue();
        final String latVarUnitsString = this.latVar.findAttribute("units").getStringValue();
        if (!SimpleUnit.isCompatible(latVarUnitsString, "deg")) {
            throw new IllegalArgumentException("Units of lat var <" + latVarUnitsString + "> not compatible with \"degrees_north\".");
        }
        final String lonVarUnitsString = this.lonVar.findAttribute("units").getStringValue();
        if (!SimpleUnit.isCompatible(lonVarUnitsString, "deg")) {
            throw new IllegalArgumentException("Units of lon var <" + lonVarUnitsString + "> not compatible with \"degrees_east\".");
        }
        final String elevVarUnitsString = this.elevVar.findAttribute("units").getStringValue();
        if (!SimpleUnit.isCompatible(elevVarUnitsString, "meters")) {
            throw new IllegalArgumentException("Units of elev var <" + elevVarUnitsString + "> not compatible with \"meters\".");
        }
        try {
            this.elevVarUnitsConversionFactor = getMetersConversionFactor(elevVarUnitsString);
        }
        catch (Exception e) {
            throw new IllegalArgumentException("Exception on getMetersConversionFactor() for the units of elev var <" + elevVarUnitsString + ">.");
        }
        if (this.ncfile.hasUnlimitedDimension() && this.ncfile.getUnlimitedDimension().equals(this.trajectoryDim)) {
            final Object result = this.ncfile.sendIospMessage("AddRecordStructure");
            if (result != null && (boolean)result) {
                this.recordVar = (Structure)this.ncfile.getRootGroup().findVariable("record");
            }
            else {
                this.recordVar = new StructurePseudo(this.ncfile, null, "record", this.trajectoryDim);
            }
        }
        else {
            this.recordVar = new StructurePseudo(this.ncfile, null, "record", this.trajectoryDim);
        }
        final Variable elevVarInRecVar = this.recordVar.findVariable(this.elevVar.getName());
        if (!elevVarUnitsString.equals(elevVarInRecVar.findAttribute("units").getStringValue())) {
            elevVarInRecVar.addAttribute(new Attribute("units", elevVarUnitsString));
        }
        this.trajectoryVarsMap = new HashMap();
        for (final Variable curVar : this.ncfile.getRootGroup().getVariables()) {
            if (curVar.getRank() > 0 && !curVar.equals(this.dimVar) && !curVar.equals(this.latVar) && !curVar.equals(this.lonVar) && !curVar.equals(this.elevVar)) {
                if (this.recordVar != null) {
                    if (curVar.equals(this.recordVar)) {
                        continue;
                    }
                }
                final MyTypedDataVariable typedVar = new MyTypedDataVariable(new VariableDS(null, curVar, true));
                this.dataVariables.add(typedVar);
                this.trajectoryVarsMap.put(typedVar.getName(), typedVar);
            }
        }
        this.trajectory = new Trajectory(this.trajectoryId, this.trajectoryNumPoint, this.dimVar, this.dimVarUnitsString, this.latVar, this.lonVar, this.elevVar, (List)this.dataVariables, this.trajectoryVarsMap);
        this.startDate = this.getStartDate();
        this.endDate = this.getEndDate();
        ((Trajectory)this.trajectory).setStartDate(this.startDate);
        ((Trajectory)this.trajectory).setEndDate(this.endDate);
    }
    
    protected static double getMetersConversionFactor(final String unitsString) throws Exception {
        final SimpleUnit unit = SimpleUnit.factoryWithExceptions(unitsString);
        return unit.convertTo(1.0, SimpleUnit.meterUnit);
    }
    
    @Override
    protected void setBoundingBox() {
    }
    
    public List getTrajectoryIds() {
        final List l = new ArrayList();
        l.add(this.trajectoryId);
        return l;
    }
    
    public List getTrajectories() {
        final List l = new ArrayList();
        l.add(this.trajectory);
        return l;
    }
    
    public TrajectoryObsDatatype getTrajectory(final String trajectoryId) {
        if (!trajectoryId.equals(this.trajectoryId)) {
            return null;
        }
        return this.trajectory;
    }
    
    @Override
    public String getDetailInfo() {
        final StringBuffer sbuff = new StringBuffer();
        sbuff.append("TrajectoryObsDataset\n");
        sbuff.append("  adapter   = " + this.getClass().getName() + "\n");
        sbuff.append("  trajectories:\n");
        final Iterator it = this.getTrajectoryIds().iterator();
        while (it.hasNext()) {
            sbuff.append("      " + it.next() + "\n");
        }
        sbuff.append(super.getDetailInfo());
        return sbuff.toString();
    }
    
    public boolean syncExtend() {
        if (!this.ncfile.hasUnlimitedDimension()) {
            return false;
        }
        try {
            if (!this.ncfile.syncExtend()) {
                return false;
            }
        }
        catch (IOException e) {
            return false;
        }
        final int newNumPoints = this.trajectoryDim.getLength();
        if (this.trajectoryNumPoint >= newNumPoints) {
            return false;
        }
        this.trajectoryNumPoint = newNumPoints;
        ((Trajectory)this.trajectory).setNumPoints(this.trajectoryNumPoint);
        try {
            this.endDate = this.trajectory.getTime(this.trajectoryNumPoint - 1);
        }
        catch (IOException e2) {
            return false;
        }
        ((Trajectory)this.trajectory).setEndDate(this.endDate);
        return true;
    }
    
    public static class Config
    {
        protected String trajectoryId;
        protected Dimension trajectoryDim;
        protected Variable dimVar;
        protected Variable latVar;
        protected Variable lonVar;
        protected Variable elevVar;
        
        public Config() {
        }
        
        public Config(final String trajectoryId, final Dimension trajectoryDim, final Variable dimVar, final Variable latVar, final Variable lonVar, final Variable elevVar) {
            this.trajectoryId = trajectoryId;
            this.trajectoryDim = trajectoryDim;
            this.dimVar = dimVar;
            this.latVar = latVar;
            this.lonVar = lonVar;
            this.elevVar = elevVar;
        }
        
        public void setTrajectoryId(final String trajectoryId) {
            this.trajectoryId = trajectoryId;
        }
        
        public void setTrajectoryDim(final Dimension trajectoryDim) {
            this.trajectoryDim = trajectoryDim;
        }
        
        public void setDimensionVar(final Variable dimVar) {
            this.dimVar = dimVar;
        }
        
        public void setLatVar(final Variable latVar) {
            this.latVar = latVar;
        }
        
        public void setLonVar(final Variable lonVar) {
            this.lonVar = lonVar;
        }
        
        public void setElevVar(final Variable elevVar) {
            this.elevVar = elevVar;
        }
        
        public String getTrajectoryId() {
            return this.trajectoryId;
        }
        
        public Dimension getTrajectoryDim() {
            return this.trajectoryDim;
        }
        
        public Variable getDimensionVar() {
            return this.dimVar;
        }
        
        public Variable getLatVar() {
            return this.latVar;
        }
        
        public Variable getLonVar() {
            return this.lonVar;
        }
        
        public Variable getElevVar() {
            return this.elevVar;
        }
    }
    
    private class Trajectory implements TrajectoryObsDatatype
    {
        private String id;
        private String description;
        private int numPoints;
        private Date startDate;
        private Date endDate;
        private String dimVarUnitsString;
        private Variable dimVar;
        private Variable latVar;
        private Variable lonVar;
        private Variable elevVar;
        private List variables;
        private HashMap variablesMap;
        final /* synthetic */ TrajectoryObsDatasetImpl this$0;
        
        private Trajectory(final String id, final int numPoints, final Variable dimVar, final String dimVarUnitsString, final Variable latVar, final Variable lonVar, final Variable elevVar, final List variables, final HashMap variablesMap) {
            this.description = null;
            this.id = id;
            this.numPoints = numPoints;
            this.dimVarUnitsString = dimVarUnitsString;
            this.dimVar = dimVar;
            this.variables = variables;
            this.variablesMap = variablesMap;
            this.latVar = latVar;
            this.lonVar = lonVar;
            this.elevVar = elevVar;
        }
        
        protected void setNumPoints(final int numPoints) {
            this.numPoints = numPoints;
        }
        
        protected void setStartDate(final Date startDate) {
            if (this.startDate != null) {
                throw new IllegalStateException("Can only call setStartDate() once.");
            }
            this.startDate = startDate;
        }
        
        protected void setEndDate(final Date endDate) {
            this.endDate = endDate;
        }
        
        public String getId() {
            return this.id;
        }
        
        public String getDescription() {
            return this.description;
        }
        
        public int getNumberPoints() {
            return this.numPoints;
        }
        
        public List getDataVariables() {
            return this.variables;
        }
        
        public VariableSimpleIF getDataVariable(final String name) {
            return this.variablesMap.get(name);
        }
        
        public PointObsDatatype getPointObsData(final int point) throws IOException {
            return new MyPointObsDatatype(point);
        }
        
        public Date getStartDate() {
            return this.startDate;
        }
        
        public Date getEndDate() {
            return this.endDate;
        }
        
        public LatLonRect getBoundingBox() {
            return null;
        }
        
        public Date getTime(final int point) throws IOException {
            if (SimpleUnit.isDateUnit(this.dimVarUnitsString)) {
                return DateUnit.getStandardDate(this.getTimeValue(point) + " " + this.dimVarUnitsString);
            }
            return this.startDate;
        }
        
        public EarthLocation getLocation(final int point) throws IOException {
            return new MyEarthLocation(point);
        }
        
        public String getTimeUnitsIdentifier() {
            return this.dimVarUnitsString;
        }
        
        public double getTimeValue(final int point) throws IOException {
            Array array = null;
            try {
                array = this.getTime(this.getPointRange(point));
            }
            catch (InvalidRangeException e) {
                final IllegalArgumentException iae = new IllegalArgumentException("Point <" + point + "> not in valid range <0, " + (this.getNumberPoints() - 1) + ">: " + e.getMessage());
                iae.initCause(e);
                throw iae;
            }
            if (array instanceof ArrayDouble) {
                return array.getDouble(array.getIndex());
            }
            if (array instanceof ArrayFloat) {
                return array.getFloat(array.getIndex());
            }
            if (array instanceof ArrayInt) {
                return array.getInt(array.getIndex());
            }
            throw new IOException("Time variable not float, double, or integer <" + array.getElementType().toString() + ">.");
        }
        
        public double getLatitude(final int point) throws IOException {
            Array array = null;
            try {
                array = this.getLatitude(this.getPointRange(point));
            }
            catch (InvalidRangeException e) {
                final IllegalArgumentException iae = new IllegalArgumentException("Point <" + point + "> not in valid range <0, " + (this.getNumberPoints() - 1) + ">: " + e.getMessage());
                iae.initCause(e);
                throw iae;
            }
            if (array instanceof ArrayDouble) {
                return array.getDouble(array.getIndex());
            }
            if (array instanceof ArrayFloat) {
                return array.getFloat(array.getIndex());
            }
            throw new IOException("Latitude variable not float or double <" + array.getElementType().toString() + ">.");
        }
        
        public double getLongitude(final int point) throws IOException {
            Array array = null;
            try {
                array = this.getLongitude(this.getPointRange(point));
            }
            catch (InvalidRangeException e) {
                final IllegalArgumentException iae = new IllegalArgumentException("Point <" + point + "> not in valid range <0, " + (this.getNumberPoints() - 1) + ">: " + e.getMessage());
                iae.initCause(e);
                throw iae;
            }
            if (array instanceof ArrayDouble) {
                return array.getDouble(array.getIndex());
            }
            if (array instanceof ArrayFloat) {
                return array.getFloat(array.getIndex());
            }
            throw new IOException("Longitude variable not float or double <" + array.getElementType().toString() + ">.");
        }
        
        public double getElevation(final int point) throws IOException {
            Array array = null;
            try {
                array = this.getElevation(this.getPointRange(point));
            }
            catch (InvalidRangeException e) {
                final IllegalArgumentException iae = new IllegalArgumentException("Point <" + point + "> not in valid range <0, " + (this.getNumberPoints() - 1) + ">: " + e.getMessage());
                iae.initCause(e);
                throw iae;
            }
            if (array instanceof ArrayDouble) {
                return array.getDouble(array.getIndex());
            }
            if (array instanceof ArrayFloat) {
                return array.getFloat(array.getIndex());
            }
            throw new IOException("Elevation variable not float or double <" + array.getElementType().toString() + ">.");
        }
        
        public StructureData getData(final int point) throws IOException, InvalidRangeException {
            return TrajectoryObsDatasetImpl.this.recordVar.readStructure(point);
        }
        
        public Array getData(final int point, final String parameterName) throws IOException {
            try {
                return this.getData(this.getPointRange(point), parameterName);
            }
            catch (InvalidRangeException e) {
                final IllegalArgumentException iae = new IllegalArgumentException("Point <" + point + "> not in valid range <0, " + (this.getNumberPoints() - 1) + ">: " + e.getMessage());
                iae.initCause(e);
                throw iae;
            }
        }
        
        public Range getFullRange() {
            Range range = null;
            try {
                range = new Range(0, this.getNumberPoints() - 1);
            }
            catch (InvalidRangeException e) {
                final IllegalStateException ise = new IllegalStateException("Full trajectory range invalid <0, " + (this.getNumberPoints() - 1) + ">: " + e.getMessage());
                ise.initCause(e);
                throw ise;
            }
            return range;
        }
        
        public Range getPointRange(final int point) throws InvalidRangeException {
            if (point >= this.getNumberPoints()) {
                throw new InvalidRangeException("Point <" + point + "> not in acceptible range <0, " + (this.getNumberPoints() - 1) + ">.");
            }
            return new Range(point, point);
        }
        
        public Range getRange(final int start, final int end, final int stride) throws InvalidRangeException {
            if (end >= this.getNumberPoints()) {
                throw new InvalidRangeException("End point <" + end + "> not in acceptible range <0, " + (this.getNumberPoints() - 1) + ">.");
            }
            return new Range(start, end, stride);
        }
        
        public Array getTime(final Range range) throws IOException, InvalidRangeException {
            Array array = null;
            final List section = new ArrayList(1);
            section.add(range);
            array = this.dimVar.read(section);
            if (SimpleUnit.isDateUnit(this.dimVarUnitsString)) {
                return array;
            }
            final int size = (int)array.getSize();
            final double[] pa = new double[size];
            for (int i = 0; i < size; ++i) {
                pa[i] = (double)this.startDate.getTime();
            }
            final Array ay = Array.factory(Double.TYPE, array.getShape(), pa);
            return ay;
        }
        
        public Array getLatitude(final Range range) throws IOException, InvalidRangeException {
            final List section = new ArrayList(1);
            section.add(range);
            return this.latVar.read(section);
        }
        
        public Array getLongitude(final Range range) throws IOException, InvalidRangeException {
            final List section = new ArrayList(1);
            section.add(range);
            return this.lonVar.read(section);
        }
        
        public Array getElevation(final Range range) throws IOException, InvalidRangeException {
            final List section = new ArrayList(1);
            section.add(range);
            final Array a = this.elevVar.read(section);
            if (TrajectoryObsDatasetImpl.this.elevVarUnitsConversionFactor == 1.0) {
                return a;
            }
            final IndexIterator it = a.getIndexIterator();
            while (it.hasNext()) {
                if (this.elevVar.getDataType() == DataType.DOUBLE) {
                    final double val = it.getDoubleNext();
                    it.setDoubleCurrent(val * TrajectoryObsDatasetImpl.this.elevVarUnitsConversionFactor);
                }
                else if (this.elevVar.getDataType() == DataType.FLOAT) {
                    final float val2 = it.getFloatNext();
                    it.setFloatCurrent((float)(val2 * TrajectoryObsDatasetImpl.this.elevVarUnitsConversionFactor));
                }
                else if (this.elevVar.getDataType() == DataType.INT) {
                    final int val3 = it.getIntNext();
                    it.setIntCurrent((int)(val3 * TrajectoryObsDatasetImpl.this.elevVarUnitsConversionFactor));
                }
                else {
                    if (this.elevVar.getDataType() != DataType.LONG) {
                        throw new IllegalStateException("Elevation variable type <" + this.elevVar.getDataType().toString() + "> not double, float, int, or long.");
                    }
                    final long val4 = it.getLongNext();
                    it.setLongCurrent((long)(val4 * TrajectoryObsDatasetImpl.this.elevVarUnitsConversionFactor));
                }
            }
            return a;
        }
        
        public Array getData(final Range range, final String parameterName) throws IOException, InvalidRangeException {
            final Variable variable = TrajectoryObsDatasetImpl.this.ncfile.getRootGroup().findVariable(parameterName);
            final int varRank = variable.getRank();
            final int[] varShape = variable.getShape();
            final List section = new ArrayList(varRank);
            section.add(range);
            for (int i = 1; i < varRank; ++i) {
                section.add(new Range(0, varShape[i] - 1));
            }
            final Array array = variable.read(section);
            if (array.getShape()[0] == 1) {
                return array.reduce(0);
            }
            return array;
        }
        
        public DataIterator getDataIterator(final int bufferSize) throws IOException {
            return new PointDatatypeIterator(TrajectoryObsDatasetImpl.this.recordVar, bufferSize);
        }
        
        private class PointDatatypeIterator extends DatatypeIterator
        {
            @Override
            protected Object makeDatatypeWithData(final int recnum, final StructureData sdata) {
                return new MyPointObsDatatype(recnum, sdata);
            }
            
            PointDatatypeIterator(final Structure struct, final int bufferSize) {
                super(struct, bufferSize);
            }
        }
        
        private class MyPointObsDatatype implements PointObsDatatype
        {
            private int point;
            private StructureData sdata;
            private double time;
            private EarthLocation earthLoc;
            
            private MyPointObsDatatype(final int point) throws IOException {
                this.point = point;
                this.time = Trajectory.this.getTimeValue(point);
                this.earthLoc = Trajectory.this.getLocation(point);
            }
            
            private MyPointObsDatatype(final int point, final StructureData sdata) {
                this.point = point;
                this.sdata = sdata;
                this.time = sdata.convertScalarDouble(Trajectory.this.dimVar.getName());
                this.earthLoc = new MyEarthLocation(sdata);
            }
            
            public double getNominalTime() {
                return this.time;
            }
            
            public double getObservationTime() {
                return this.time;
            }
            
            public Date getNominalTimeAsDate() {
                final String dateStr = this.getNominalTime() + " " + Trajectory.this.dimVarUnitsString;
                return DateUnit.getStandardDate(dateStr);
            }
            
            public Date getObservationTimeAsDate() {
                final String dateStr = this.getObservationTime() + " " + Trajectory.this.dimVarUnitsString;
                return DateUnit.getStandardDate(dateStr);
            }
            
            public EarthLocation getLocation() {
                return this.earthLoc;
            }
            
            public StructureData getData() throws IOException {
                if (this.sdata != null) {
                    return this.sdata;
                }
                try {
                    return Trajectory.this.getData(this.point);
                }
                catch (InvalidRangeException e) {
                    throw new IllegalStateException(e.getMessage());
                }
            }
        }
        
        private class MyEarthLocation extends EarthLocationImpl
        {
            private double latitude;
            private double longitude;
            private double elevation;
            
            private MyEarthLocation(final int point) throws IOException {
                this.latitude = Trajectory.this.getLatitude(point);
                this.longitude = Trajectory.this.getLongitude(point);
                this.elevation = Trajectory.this.getElevation(point);
            }
            
            private MyEarthLocation(final StructureData sdata) {
                this.latitude = sdata.convertScalarDouble(Trajectory.this.latVar.getName());
                this.longitude = sdata.convertScalarDouble(Trajectory.this.lonVar.getName());
                this.elevation = sdata.convertScalarDouble(Trajectory.this.elevVar.getName());
                if (Trajectory.this.this$0.elevVarUnitsConversionFactor != 1.0) {
                    this.elevation *= Trajectory.this.this$0.elevVarUnitsConversionFactor;
                }
            }
            
            @Override
            public double getLatitude() {
                return this.latitude;
            }
            
            @Override
            public double getLongitude() {
                return this.longitude;
            }
            
            @Override
            public double getAltitude() {
                return this.elevation;
            }
        }
    }
    
    private class MyTypedDataVariable extends VariableSimpleSubclass
    {
        private int rank;
        private int[] shape;
        
        private MyTypedDataVariable(final VariableDS v) {
            super(v);
            this.rank = super.getRank() - 1;
            final int[] varShape = super.getShape();
            this.shape = new int[varShape.length - 1];
            final int trajDimIndex = v.findDimensionIndex(TrajectoryObsDatasetImpl.this.trajectoryDim.getName());
            int i = 0;
            int j = 0;
            while (i < varShape.length) {
                if (i != trajDimIndex) {
                    this.shape[j++] = varShape[i];
                }
                ++i;
            }
        }
        
        @Override
        public int getRank() {
            return this.rank;
        }
        
        @Override
        public int[] getShape() {
            return this.shape;
        }
    }
}
