// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.dt.trajectory;

import ucar.nc2.constants.FeatureType;
import java.io.IOException;
import ucar.nc2.dt.TypedDataset;
import ucar.nc2.util.CancelTask;
import java.util.GregorianCalendar;
import java.util.TimeZone;
import java.util.Calendar;
import java.util.Date;
import ucar.nc2.Variable;
import java.util.List;
import ucar.nc2.units.SimpleUnit;
import ucar.nc2.Dimension;
import ucar.nc2.dataset.NetcdfDataset;
import ucar.nc2.dt.TypedDatasetFactoryIF;

public class COSMICTrajectoryObsDataset extends TrajectoryObsDatasetImpl implements TypedDatasetFactoryIF
{
    private static String dimName;
    private static String dimVarName;
    private static String latVarName;
    private static String lonVarName;
    private static String elevVarName;
    private static String trajId;
    NetcdfDataset localNCD;
    
    public static boolean isValidFile(final NetcdfDataset ncd) {
        return buildConfig(ncd) != null;
    }
    
    private static Config buildConfig(final NetcdfDataset ncd) {
        List list = ncd.getRootGroup().getDimensions();
        if (list.size() != 1) {
            return null;
        }
        Dimension d = list.get(0);
        if (!d.getName().equals(COSMICTrajectoryObsDataset.dimName)) {
            return null;
        }
        final Config trajConfig = new Config();
        trajConfig.setTrajectoryDim(d);
        Variable var = ncd.getRootGroup().findVariable(COSMICTrajectoryObsDataset.dimVarName);
        if (var == null) {
            return null;
        }
        list = var.getDimensions();
        if (list.size() != 1) {
            return null;
        }
        d = list.get(0);
        if (!d.getName().equals(COSMICTrajectoryObsDataset.dimName)) {
            return null;
        }
        String units = var.findAttribute("units").getStringValue();
        if (!SimpleUnit.isCompatible(units, "km")) {
            return null;
        }
        trajConfig.setDimensionVar(var);
        var = ncd.getRootGroup().findVariable(COSMICTrajectoryObsDataset.latVarName);
        if (var == null) {
            return null;
        }
        list = var.getDimensions();
        if (list.size() != 1) {
            return null;
        }
        d = list.get(0);
        if (!d.getName().equals(COSMICTrajectoryObsDataset.dimName)) {
            return null;
        }
        units = var.findAttribute("units").getStringValue();
        if (!SimpleUnit.isCompatible(units, "deg")) {
            return null;
        }
        trajConfig.setLatVar(var);
        var = ncd.getRootGroup().findVariable(COSMICTrajectoryObsDataset.lonVarName);
        if (var == null) {
            return null;
        }
        list = var.getDimensions();
        if (list.size() != 1) {
            return null;
        }
        d = list.get(0);
        if (!d.getName().equals(COSMICTrajectoryObsDataset.dimName)) {
            return null;
        }
        units = var.findAttribute("units").getStringValue();
        if (!SimpleUnit.isCompatible(units, "deg")) {
            return null;
        }
        trajConfig.setLonVar(var);
        var = ncd.getRootGroup().findVariable(COSMICTrajectoryObsDataset.elevVarName);
        if (var == null) {
            return null;
        }
        list = var.getDimensions();
        if (list.size() != 1) {
            return null;
        }
        d = list.get(0);
        if (!d.getName().equals(COSMICTrajectoryObsDataset.dimName)) {
            return null;
        }
        units = var.findAttribute("units").getStringValue();
        if (!SimpleUnit.isCompatible(units, "km")) {
            return null;
        }
        trajConfig.setElevVar(var);
        trajConfig.setTrajectoryId(COSMICTrajectoryObsDataset.trajId);
        return trajConfig;
    }
    
    @Override
    public Date getStartDate() {
        final Calendar cal = Calendar.getInstance();
        final double timeValue = this.localNCD.findGlobalAttribute("start_time").getNumericValue().doubleValue();
        cal.setTimeInMillis((long)timeValue * 1000L);
        final Date dd = this.getTime(this.localNCD);
        return dd;
    }
    
    @Override
    public Date getEndDate() {
        final Calendar cal = Calendar.getInstance();
        final double timeValue = this.localNCD.findGlobalAttribute("stop_time").getNumericValue().doubleValue() - this.localNCD.findGlobalAttribute("start_time").getNumericValue().doubleValue();
        final Date dd = this.getTime(this.localNCD);
        final long dl = dd.getTime() + (long)timeValue;
        cal.setTimeInMillis(dl);
        return cal.getTime();
    }
    
    Date getTime(final NetcdfDataset ds) {
        final int year = ds.readAttributeInteger(null, "year", 0);
        final int month = ds.readAttributeInteger(null, "month", 0);
        final int dayOfMonth = ds.readAttributeInteger(null, "day", 0);
        final int hourOfDay = ds.readAttributeInteger(null, "hour", 0);
        final int minute = ds.readAttributeInteger(null, "minute", 0);
        final int second = ds.readAttributeInteger(null, "second", 0);
        final Calendar cal = new GregorianCalendar(TimeZone.getTimeZone("UTC"));
        cal.clear();
        cal.set(year, month - 1, dayOfMonth, hourOfDay, minute, second);
        return cal.getTime();
    }
    
    @Override
    protected void setStartDate() {
    }
    
    @Override
    protected void setEndDate() {
    }
    
    public boolean isMine(final NetcdfDataset ds) {
        return isValidFile(ds);
    }
    
    public TypedDataset open(final NetcdfDataset ncd, final CancelTask task, final StringBuilder errlog) throws IOException {
        return new COSMICTrajectoryObsDataset(ncd);
    }
    
    public FeatureType getScientificDataType() {
        return FeatureType.TRAJECTORY;
    }
    
    public COSMICTrajectoryObsDataset() {
    }
    
    public COSMICTrajectoryObsDataset(final NetcdfDataset ncd) throws IOException {
        super(ncd);
        this.localNCD = ncd;
        final Config trajConfig = buildConfig(ncd);
        this.setTrajectoryInfo(trajConfig);
    }
    
    static {
        COSMICTrajectoryObsDataset.dimName = "MSL_alt";
        COSMICTrajectoryObsDataset.dimVarName = "MSL_alt";
        COSMICTrajectoryObsDataset.latVarName = "Lat";
        COSMICTrajectoryObsDataset.lonVarName = "Lon";
        COSMICTrajectoryObsDataset.elevVarName = "MSL_alt";
        COSMICTrajectoryObsDataset.trajId = "trajectory data";
    }
}
