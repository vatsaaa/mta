// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.dt.trajectory;

import ucar.nc2.dt.VariableSimpleSubclass;
import ucar.unidata.geoloc.EarthLocationImpl;
import ucar.nc2.dt.DataIterator;
import ucar.ma2.IndexIterator;
import ucar.ma2.StructureData;
import ucar.ma2.ArrayInt;
import ucar.ma2.ArrayFloat;
import ucar.ma2.ArrayDouble;
import ucar.unidata.geoloc.EarthLocation;
import ucar.unidata.geoloc.LatLonRect;
import ucar.nc2.dt.PointObsDatatype;
import ucar.nc2.VariableSimpleIF;
import java.util.Date;
import ucar.nc2.dt.TrajectoryObsDatatype;
import ucar.ma2.Index;
import ucar.ma2.Array;
import java.util.Iterator;
import ucar.ma2.DataType;
import java.util.ArrayList;
import ucar.ma2.InvalidRangeException;
import java.io.IOException;
import ucar.ma2.Range;
import ucar.nc2.dataset.VariableDS;
import ucar.nc2.Attribute;
import ucar.nc2.Group;
import ucar.nc2.NetcdfFile;
import ucar.nc2.StructurePseudo;
import ucar.nc2.units.SimpleUnit;
import ucar.nc2.units.DateUnit;
import ucar.nc2.dataset.NetcdfDataset;
import java.util.HashMap;
import java.util.List;
import ucar.nc2.Structure;
import ucar.nc2.Variable;
import ucar.nc2.Dimension;
import ucar.nc2.dt.TrajectoryObsDataset;
import ucar.nc2.dt.TypedDatasetImpl;

public class MultiTrajectoryObsDataset extends TypedDatasetImpl implements TrajectoryObsDataset
{
    protected Dimension trajDim;
    protected Variable trajVar;
    protected Dimension timeDim;
    protected Variable timeVar;
    protected Structure recordVar;
    protected Variable latVar;
    protected Variable lonVar;
    protected Variable elevVar;
    protected String timeVarUnitsString;
    protected double elevVarUnitsConversionFactor;
    protected List trajectoryIds;
    protected List trajectories;
    protected HashMap trajectoriesMap;
    protected int trajectoryNumPoint;
    protected HashMap trajectoryVarsMap;
    
    public MultiTrajectoryObsDataset() {
        this.recordVar = null;
    }
    
    public MultiTrajectoryObsDataset(final NetcdfDataset ncfile) {
        super(ncfile);
        this.recordVar = null;
    }
    
    public void setTrajectoryInfo(final Dimension trajDim, final Variable trajVar, final Dimension timeDim, final Variable timeVar, final Variable latVar, final Variable lonVar, final Variable elevVar) throws IOException {
        this.trajDim = trajDim;
        this.trajVar = trajVar;
        this.timeDim = timeDim;
        this.timeVar = timeVar;
        this.latVar = latVar;
        this.lonVar = lonVar;
        this.elevVar = elevVar;
        this.trajectoryNumPoint = this.timeDim.getLength();
        this.timeVarUnitsString = this.timeVar.findAttribute("units").getStringValue();
        if (DateUnit.getStandardDate(this.timeVarUnitsString) == null) {
            throw new IllegalArgumentException("Units of time variable <" + this.timeVarUnitsString + "> not a date unit.");
        }
        final String latVarUnitsString = this.latVar.findAttribute("units").getStringValue();
        if (!SimpleUnit.isCompatible(latVarUnitsString, "degrees_north")) {
            throw new IllegalArgumentException("Units of lat var <" + latVarUnitsString + "> not compatible with \"degrees_north\".");
        }
        final String lonVarUnitsString = this.lonVar.findAttribute("units").getStringValue();
        if (!SimpleUnit.isCompatible(lonVarUnitsString, "degrees_east")) {
            throw new IllegalArgumentException("Units of lon var <" + lonVarUnitsString + "> not compatible with \"degrees_east\".");
        }
        final String elevVarUnitsString = this.elevVar.findAttribute("units").getStringValue();
        if (!SimpleUnit.isCompatible(elevVarUnitsString, "meters")) {
            throw new IllegalArgumentException("Units of elev var <" + elevVarUnitsString + "> not compatible with \"meters\".");
        }
        try {
            this.elevVarUnitsConversionFactor = getMetersConversionFactor(elevVarUnitsString);
        }
        catch (Exception e3) {
            throw new IllegalArgumentException("Exception on getMetersConversionFactor() for the units of elev var <" + elevVarUnitsString + ">.");
        }
        if (this.ncfile.hasUnlimitedDimension() && this.ncfile.getUnlimitedDimension().equals(timeDim)) {
            this.ncfile.sendIospMessage("AddRecordStructure");
            this.recordVar = (Structure)this.ncfile.getRootGroup().findVariable("record");
        }
        else {
            this.recordVar = new StructurePseudo(this.ncfile, null, "record", timeDim);
        }
        final Variable latVarInRecVar = this.recordVar.findVariable(this.latVar.getName());
        final Attribute latVarUnitsAtt = latVarInRecVar.findAttribute("units");
        if (latVarUnitsAtt != null && !latVarUnitsString.equals(latVarUnitsAtt.getStringValue())) {
            latVarInRecVar.addAttribute(new Attribute("units", latVarUnitsString));
        }
        final Variable lonVarInRecVar = this.recordVar.findVariable(this.lonVar.getName());
        final Attribute lonVarUnitsAtt = lonVarInRecVar.findAttribute("units");
        if (lonVarUnitsAtt != null && !lonVarUnitsString.equals(lonVarUnitsAtt.getStringValue())) {
            lonVarInRecVar.addAttribute(new Attribute("units", lonVarUnitsString));
        }
        final Variable elevVarInRecVar = this.recordVar.findVariable(this.elevVar.getName());
        final Attribute elevVarUnitsAtt = elevVarInRecVar.findAttribute("units");
        if (elevVarUnitsAtt != null && !elevVarUnitsString.equals(elevVarUnitsAtt.getStringValue())) {
            elevVarInRecVar.addAttribute(new Attribute("units", elevVarUnitsString));
        }
        this.trajectoryVarsMap = new HashMap();
        for (final Variable curVar : this.ncfile.getRootGroup().getVariables()) {
            if (curVar.getRank() >= 2 && !curVar.equals(this.latVar) && !curVar.equals(this.lonVar) && !curVar.equals(this.elevVar)) {
                if (this.recordVar != null) {
                    if (curVar.equals(this.recordVar)) {
                        continue;
                    }
                }
                final MyTypedDataVariable typedVar = new MyTypedDataVariable(new VariableDS(null, curVar, true));
                this.dataVariables.add(typedVar);
                this.trajectoryVarsMap.put(typedVar.getName(), typedVar);
            }
        }
        Range startPointRange = null;
        Range endPointRange = null;
        try {
            startPointRange = new Range(0, 0);
            endPointRange = new Range(this.trajectoryNumPoint - 1, this.trajectoryNumPoint - 1);
        }
        catch (InvalidRangeException e) {
            final IOException ioe = new IOException("Start or end point range invalid: " + e.getMessage());
            ioe.initCause(e);
            throw ioe;
        }
        final List section0 = new ArrayList(1);
        final List section2 = new ArrayList(1);
        section0.add(startPointRange);
        section2.add(endPointRange);
        Array startTimeArray;
        Array endTimeArray;
        try {
            startTimeArray = this.timeVar.read(section0);
            endTimeArray = this.timeVar.read(section2);
        }
        catch (InvalidRangeException e2) {
            final IOException ioe2 = new IOException("Invalid range during read of start or end point: " + e2.getMessage());
            ioe2.initCause(e2);
            throw ioe2;
        }
        String startTimeString;
        String endTimeString;
        if (this.timeVar.getDataType().equals(DataType.DOUBLE)) {
            startTimeString = startTimeArray.getDouble(startTimeArray.getIndex()) + " " + this.timeVarUnitsString;
            endTimeString = endTimeArray.getDouble(endTimeArray.getIndex()) + " " + this.timeVarUnitsString;
        }
        else if (this.timeVar.getDataType().equals(DataType.FLOAT)) {
            startTimeString = startTimeArray.getFloat(startTimeArray.getIndex()) + " " + this.timeVarUnitsString;
            endTimeString = endTimeArray.getFloat(endTimeArray.getIndex()) + " " + this.timeVarUnitsString;
        }
        else {
            if (!this.timeVar.getDataType().equals(DataType.INT)) {
                final String tmpMsg = "Time var <" + this.timeVar.getName() + "> is not a double, float, or integer <" + timeVar.getDataType().toString() + ">.";
                throw new IllegalArgumentException(tmpMsg);
            }
            startTimeString = startTimeArray.getInt(startTimeArray.getIndex()) + " " + this.timeVarUnitsString;
            endTimeString = endTimeArray.getInt(endTimeArray.getIndex()) + " " + this.timeVarUnitsString;
        }
        this.startDate = DateUnit.getStandardDate(startTimeString);
        this.endDate = DateUnit.getStandardDate(endTimeString);
        this.trajectoryIds = new ArrayList();
        this.trajectories = new ArrayList();
        this.trajectoriesMap = new HashMap();
        final Array trajArray = this.trajVar.read();
        final Index index = trajArray.getIndex();
        for (int i = 0; i < trajArray.getSize(); ++i) {
            String curTrajId;
            if (this.trajVar.getDataType().equals(DataType.STRING)) {
                curTrajId = (String)trajArray.getObject(index.set(i));
            }
            else if (this.trajVar.getDataType().equals(DataType.DOUBLE)) {
                curTrajId = String.valueOf(trajArray.getDouble(index.set(i)));
            }
            else if (this.trajVar.getDataType().equals(DataType.FLOAT)) {
                curTrajId = String.valueOf(trajArray.getFloat(index.set(i)));
            }
            else {
                if (!this.trajVar.getDataType().equals(DataType.INT)) {
                    final String tmpMsg2 = "Trajectory var <" + this.trajVar.getName() + "> is not a string, double, float, or integer <" + this.trajVar.getDataType().toString() + ">.";
                    throw new IllegalStateException(tmpMsg2);
                }
                curTrajId = String.valueOf(trajArray.getInt(index.set(i)));
            }
            final MultiTrajectory curTraj = new MultiTrajectory(curTrajId, i, this.trajectoryNumPoint, this.startDate, this.endDate, this.trajVar, this.timeVar, this.timeVarUnitsString, this.latVar, this.lonVar, this.elevVar, (List)this.dataVariables, this.trajectoryVarsMap);
            this.trajectoryIds.add(curTrajId);
            this.trajectories.add(curTraj);
            this.trajectoriesMap.put(curTrajId, curTraj);
        }
    }
    
    protected static double getMetersConversionFactor(final String unitsString) throws Exception {
        final SimpleUnit unit = SimpleUnit.factoryWithExceptions(unitsString);
        return unit.convertTo(1.0, SimpleUnit.meterUnit);
    }
    
    public boolean syncExtend() {
        return false;
    }
    
    @Override
    protected void setStartDate() {
    }
    
    @Override
    protected void setEndDate() {
    }
    
    @Override
    protected void setBoundingBox() {
    }
    
    public List getTrajectoryIds() {
        return this.trajectoryIds;
    }
    
    public List getTrajectories() {
        return this.trajectories;
    }
    
    public TrajectoryObsDatatype getTrajectory(final String trajectoryId) {
        if (trajectoryId == null) {
            return null;
        }
        return this.trajectoriesMap.get(trajectoryId);
    }
    
    @Override
    public String getDetailInfo() {
        final StringBuffer sbuff = new StringBuffer();
        sbuff.append("TrajectoryObsDataset\n");
        sbuff.append("  adapter   = " + this.getClass().getName() + "\n");
        sbuff.append("  trajectories:\n");
        final Iterator it = this.getTrajectoryIds().iterator();
        while (it.hasNext()) {
            sbuff.append("      " + it.next() + "\n");
        }
        sbuff.append(super.getDetailInfo());
        return sbuff.toString();
    }
    
    private class MultiTrajectory implements TrajectoryObsDatatype
    {
        private String id;
        private int trajNum;
        private String description;
        private int numPoints;
        private Date startDate;
        private Date endDate;
        private String timeVarUnitsString;
        private Variable trajVar;
        private Variable timeVar;
        private Variable latVar;
        private Variable lonVar;
        private Variable elevVar;
        private List variables;
        private HashMap variablesMap;
        private Range trajPointRange;
        
        private MultiTrajectory(final String id, final int idNum, final int numPoints, final Date startDate, final Date endDate, final Variable trajVar, final Variable timeVar, final String timeVarUnitsString, final Variable latVar, final Variable lonVar, final Variable elevVar, final List variables, final HashMap variablesMap) {
            this.description = null;
            this.id = id;
            this.trajNum = idNum;
            this.numPoints = numPoints;
            this.startDate = startDate;
            this.endDate = endDate;
            this.timeVarUnitsString = timeVarUnitsString;
            this.timeVar = timeVar;
            this.trajVar = trajVar;
            this.variables = variables;
            this.variablesMap = variablesMap;
            this.latVar = latVar;
            this.lonVar = lonVar;
            this.elevVar = elevVar;
            try {
                this.trajPointRange = new Range(this.trajNum, this.trajNum);
            }
            catch (InvalidRangeException e) {
                final IllegalArgumentException iae = new IllegalArgumentException(e.getMessage());
                iae.initCause(e);
                throw iae;
            }
        }
        
        public String getId() {
            return this.id;
        }
        
        public String getDescription() {
            return this.description;
        }
        
        public int getNumberPoints() {
            return this.numPoints;
        }
        
        public List getDataVariables() {
            return this.variables;
        }
        
        public VariableSimpleIF getDataVariable(final String name) {
            return this.variablesMap.get(name);
        }
        
        public PointObsDatatype getPointObsData(final int point) throws IOException {
            return new MyPointObsDatatype(point);
        }
        
        public Date getStartDate() {
            return this.startDate;
        }
        
        public Date getEndDate() {
            return this.endDate;
        }
        
        public LatLonRect getBoundingBox() {
            return null;
        }
        
        public Date getTime(final int point) throws IOException {
            return DateUnit.getStandardDate(this.getTimeValue(point) + " " + this.timeVarUnitsString);
        }
        
        public EarthLocation getLocation(final int point) throws IOException {
            return new MyEarthLocation(point);
        }
        
        public String getTimeUnitsIdentifier() {
            return this.timeVarUnitsString;
        }
        
        public double getTimeValue(final int point) throws IOException {
            Array array = null;
            try {
                array = this.getTime(this.getPointRange(point));
            }
            catch (InvalidRangeException e) {
                final IllegalArgumentException iae = new IllegalArgumentException("Point <" + point + "> not in valid range <0, " + (this.getNumberPoints() - 1) + ">: " + e.getMessage());
                iae.initCause(e);
                throw iae;
            }
            if (array instanceof ArrayDouble) {
                return array.getDouble(array.getIndex());
            }
            if (array instanceof ArrayFloat) {
                return array.getFloat(array.getIndex());
            }
            if (array instanceof ArrayInt) {
                return array.getInt(array.getIndex());
            }
            throw new IOException("Time variable not float, double, or integer <" + array.getElementType().toString() + ">.");
        }
        
        public double getLatitude(final int point) throws IOException {
            Array array = null;
            try {
                array = this.getLatitude(this.getPointRange(point));
            }
            catch (InvalidRangeException e) {
                final IllegalArgumentException iae = new IllegalArgumentException("Point <" + point + "> not in valid range <0, " + (this.getNumberPoints() - 1) + ">: " + e.getMessage());
                iae.initCause(e);
                throw iae;
            }
            if (array instanceof ArrayDouble) {
                return array.getDouble(array.getIndex());
            }
            if (array instanceof ArrayFloat) {
                return array.getFloat(array.getIndex());
            }
            throw new IOException("Latitude variable not float or double <" + array.getElementType().toString() + ">.");
        }
        
        public double getLongitude(final int point) throws IOException {
            Array array = null;
            try {
                array = this.getLongitude(this.getPointRange(point));
            }
            catch (InvalidRangeException e) {
                final IllegalArgumentException iae = new IllegalArgumentException("Point <" + point + "> not in valid range <0, " + (this.getNumberPoints() - 1) + ">: " + e.getMessage());
                iae.initCause(e);
                throw iae;
            }
            if (array instanceof ArrayDouble) {
                return array.getDouble(array.getIndex());
            }
            if (array instanceof ArrayFloat) {
                return array.getFloat(array.getIndex());
            }
            throw new IOException("Longitude variable not float or double <" + array.getElementType().toString() + ">.");
        }
        
        public double getElevation(final int point) throws IOException {
            Array array = null;
            try {
                array = this.getElevation(this.getPointRange(point));
            }
            catch (InvalidRangeException e) {
                final IllegalArgumentException iae = new IllegalArgumentException("Point <" + point + "> not in valid range <0, " + (this.getNumberPoints() - 1) + ">: " + e.getMessage());
                iae.initCause(e);
                throw iae;
            }
            if (array instanceof ArrayDouble) {
                return array.getDouble(array.getIndex());
            }
            if (array instanceof ArrayFloat) {
                return array.getFloat(array.getIndex());
            }
            throw new IOException("Elevation variable not float or double <" + array.getElementType().toString() + ">.");
        }
        
        public StructureData getData(final int point) throws IOException, InvalidRangeException {
            return MultiTrajectoryObsDataset.this.recordVar.readStructure(point);
        }
        
        public Array getData(final int point, final String parameterName) throws IOException {
            try {
                return this.getData(this.getPointRange(point), parameterName);
            }
            catch (InvalidRangeException e) {
                final IllegalArgumentException iae = new IllegalArgumentException("Point <" + point + "> not in valid range <0, " + (this.getNumberPoints() - 1) + ">: " + e.getMessage());
                iae.initCause(e);
                throw iae;
            }
        }
        
        public Range getFullRange() {
            Range range = null;
            try {
                range = new Range(0, this.getNumberPoints() - 1);
            }
            catch (InvalidRangeException e) {
                final IllegalStateException ise = new IllegalStateException("Full trajectory range invalid <0, " + (this.getNumberPoints() - 1) + ">: " + e.getMessage());
                ise.initCause(e);
                throw ise;
            }
            return range;
        }
        
        public Range getPointRange(final int point) throws InvalidRangeException {
            if (point >= this.getNumberPoints()) {
                throw new InvalidRangeException("Point <" + point + "> not in acceptible range <0, " + (this.getNumberPoints() - 1) + ">.");
            }
            return new Range(point, point);
        }
        
        public Range getRange(final int start, final int end, final int stride) throws InvalidRangeException {
            if (end >= this.getNumberPoints()) {
                throw new InvalidRangeException("End point <" + end + "> not in acceptible range <0, " + (this.getNumberPoints() - 1) + ">.");
            }
            return new Range(start, end, stride);
        }
        
        public Array getTime(final Range range) throws IOException, InvalidRangeException {
            final List section = new ArrayList(1);
            section.add(range);
            return this.timeVar.read(section);
        }
        
        public Array getLatitude(final Range range) throws IOException, InvalidRangeException {
            final List section = new ArrayList(2);
            section.add(range);
            section.add(this.trajPointRange);
            return this.latVar.read(section).reduce();
        }
        
        public Array getLongitude(final Range range) throws IOException, InvalidRangeException {
            final List section = new ArrayList(2);
            section.add(range);
            section.add(this.trajPointRange);
            return this.lonVar.read(section);
        }
        
        public Array getElevation(final Range range) throws IOException, InvalidRangeException {
            final List section = new ArrayList(2);
            section.add(range);
            section.add(this.trajPointRange);
            final Array a = this.elevVar.read(section);
            if (MultiTrajectoryObsDataset.this.elevVarUnitsConversionFactor == 1.0) {
                return a;
            }
            final IndexIterator it = a.getIndexIterator();
            while (it.hasNext()) {
                if (this.elevVar.getDataType() == DataType.DOUBLE) {
                    final double val = it.getDoubleNext();
                    it.setDoubleCurrent(val * MultiTrajectoryObsDataset.this.elevVarUnitsConversionFactor);
                }
                else if (this.elevVar.getDataType() == DataType.FLOAT) {
                    final float val2 = it.getFloatNext();
                    it.setFloatCurrent((float)(val2 * MultiTrajectoryObsDataset.this.elevVarUnitsConversionFactor));
                }
                else if (this.elevVar.getDataType() == DataType.INT) {
                    final int val3 = it.getIntNext();
                    it.setIntCurrent((int)(val3 * MultiTrajectoryObsDataset.this.elevVarUnitsConversionFactor));
                }
                else {
                    if (this.elevVar.getDataType() != DataType.LONG) {
                        throw new IllegalStateException("Elevation variable type <" + this.elevVar.getDataType().toString() + "> not double, float, int, or long.");
                    }
                    final long val4 = it.getLongNext();
                    it.setLongCurrent((long)(val4 * MultiTrajectoryObsDataset.this.elevVarUnitsConversionFactor));
                }
            }
            return a;
        }
        
        public Array getData(final Range range, final String parameterName) throws IOException, InvalidRangeException {
            final Variable variable = MultiTrajectoryObsDataset.this.ncfile.getRootGroup().findVariable(parameterName);
            final int varRank = variable.getRank();
            final int[] varShape = variable.getShape();
            final List section = new ArrayList(varRank);
            section.add(range);
            section.add(this.trajPointRange);
            for (int i = 2; i < varRank; ++i) {
                section.add(new Range(0, varShape[i] - 1));
            }
            Array array = variable.read(section);
            array = array.reduce(1);
            if (array.getShape()[0] == 1) {
                return array.reduce(0);
            }
            return array;
        }
        
        public DataIterator getDataIterator(final int bufferSize) throws IOException {
            return null;
        }
        
        private class MyPointObsDatatype implements PointObsDatatype
        {
            private int point;
            private double time;
            private EarthLocation earthLoc;
            
            private MyPointObsDatatype(final int point) throws IOException {
                this.point = point;
                this.time = MultiTrajectory.this.getTimeValue(point);
                this.earthLoc = MultiTrajectory.this.getLocation(point);
            }
            
            public double getNominalTime() {
                return this.time;
            }
            
            public double getObservationTime() {
                return this.time;
            }
            
            public Date getNominalTimeAsDate() {
                final String dateStr = this.getNominalTime() + " " + MultiTrajectory.this.timeVarUnitsString;
                return DateUnit.getStandardDate(dateStr);
            }
            
            public Date getObservationTimeAsDate() {
                final String dateStr = this.getObservationTime() + " " + MultiTrajectory.this.timeVarUnitsString;
                return DateUnit.getStandardDate(dateStr);
            }
            
            public EarthLocation getLocation() {
                return this.earthLoc;
            }
            
            public StructureData getData() throws IOException {
                try {
                    return MultiTrajectory.this.getData(this.point);
                }
                catch (InvalidRangeException e) {
                    throw new IllegalStateException(e.getMessage());
                }
            }
        }
        
        private class MyEarthLocation extends EarthLocationImpl
        {
            private double latitude;
            private double longitude;
            private double elevation;
            
            private MyEarthLocation(final int point) throws IOException {
                this.latitude = MultiTrajectory.this.getLatitude(point);
                this.longitude = MultiTrajectory.this.getLongitude(point);
                this.elevation = MultiTrajectory.this.getElevation(point);
            }
            
            @Override
            public double getLatitude() {
                return this.latitude;
            }
            
            @Override
            public double getLongitude() {
                return this.longitude;
            }
            
            @Override
            public double getAltitude() {
                return this.elevation;
            }
        }
    }
    
    private class MyTypedDataVariable extends VariableSimpleSubclass
    {
        private int rank;
        private int[] shape;
        
        private MyTypedDataVariable(final VariableDS v) {
            super(v);
            this.rank = super.getRank();
            if (MultiTrajectoryObsDataset.this.timeDim != null) {
                --this.rank;
            }
            if (MultiTrajectoryObsDataset.this.trajDim != null) {
                --this.rank;
            }
            this.shape = new int[this.rank];
            final int[] varShape = super.getShape();
            final int trajDimIndex = v.findDimensionIndex(MultiTrajectoryObsDataset.this.trajDim.getName());
            final int timeDimIndex = v.findDimensionIndex(MultiTrajectoryObsDataset.this.timeDim.getName());
            int j = 0;
            for (int i = 0; i < varShape.length; ++i) {
                if (i != trajDimIndex) {
                    if (i != timeDimIndex) {
                        this.shape[j++] = varShape[i];
                    }
                }
            }
        }
        
        @Override
        public int getRank() {
            return this.rank;
        }
        
        @Override
        public int[] getShape() {
            return this.shape;
        }
    }
}
