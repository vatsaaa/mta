// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.dt;

import java.io.IOException;
import ucar.nc2.NetcdfFile;
import ucar.nc2.VariableSimpleIF;
import ucar.nc2.Attribute;
import java.util.List;
import ucar.unidata.geoloc.LatLonRect;
import java.util.Date;

public interface TypedDataset
{
    String getTitle();
    
    String getDescription();
    
    String getLocationURI();
    
    Date getStartDate();
    
    Date getEndDate();
    
    LatLonRect getBoundingBox();
    
    List<Attribute> getGlobalAttributes();
    
    Attribute findGlobalAttributeIgnoreCase(final String p0);
    
    List<VariableSimpleIF> getDataVariables();
    
    VariableSimpleIF getDataVariable(final String p0);
    
    NetcdfFile getNetcdfFile();
    
    void close() throws IOException;
    
    String getDetailInfo();
}
