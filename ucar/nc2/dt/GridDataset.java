// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.dt;

import java.util.List;

public interface GridDataset extends TypedDataset
{
    List<GridDatatype> getGrids();
    
    GridDatatype findGridDatatype(final String p0);
    
    List<Gridset> getGridsets();
    
    public interface Gridset
    {
        List<GridDatatype> getGrids();
        
        GridCoordSystem getGeoCoordSystem();
    }
}
