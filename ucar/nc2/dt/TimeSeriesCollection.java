// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.dt;

import ucar.nc2.units.DateUnit;

public interface TimeSeriesCollection
{
    Class getDataClass();
    
    int getNumTimes();
    
    double getTime(final int p0);
    
    DateUnit getTimeUnits();
    
    Object getData(final int p0);
}
