// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.dt;

import java.io.IOException;
import java.util.Iterator;

public interface DataIterator extends Iterator
{
    boolean hasNext();
    
    Object nextData() throws IOException;
    
    @Deprecated
    Object next();
}
