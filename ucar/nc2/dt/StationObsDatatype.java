// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.dt;

import ucar.unidata.geoloc.Station;

public interface StationObsDatatype extends PointObsDatatype, Comparable
{
    Station getStation();
}
