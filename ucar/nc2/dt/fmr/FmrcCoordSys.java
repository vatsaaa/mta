// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.dt.fmr;

import java.util.Date;

public interface FmrcCoordSys
{
    boolean hasVariable(final String p0);
    
    VertCoord findVertCoordForVariable(final String p0);
    
    TimeCoord findTimeCoordForVariable(final String p0, final Date p1);
    
    public interface TimeCoord
    {
        String getName();
        
        double[] getOffsetHours();
    }
    
    public interface EnsCoord
    {
        String getName();
        
        int getNEnsembles();
        
        int getPDN();
        
        int[] getEnsTypes();
    }
    
    public interface VertCoord
    {
        String getName();
        
        double[] getValues1();
        
        double[] getValues2();
    }
}
