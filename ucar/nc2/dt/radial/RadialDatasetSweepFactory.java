// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.dt.radial;

import ucar.nc2.Variable;
import java.io.IOException;
import ucar.nc2.dataset.NetcdfDataset;
import ucar.nc2.dt.RadialDatasetSweep;
import ucar.nc2.util.CancelTask;

public class RadialDatasetSweepFactory
{
    private StringBuffer log;
    
    public String getErrorMessages() {
        return (this.log == null) ? "" : this.log.toString();
    }
    
    public RadialDatasetSweep open(final String location, final CancelTask cancelTask) throws IOException {
        this.log = new StringBuffer();
        final NetcdfDataset ncd = NetcdfDataset.acquireDataset(location, cancelTask);
        return this.open(ncd);
    }
    
    public RadialDatasetSweep open(final NetcdfDataset ncd) {
        final String convention = ncd.findAttValueIgnoreCase(null, "Conventions", null);
        if (null != convention && convention.equals("_Coordinates")) {
            final String format = ncd.findAttValueIgnoreCase(null, "Format", null);
            if (format.equals("Unidata/netCDF/Dorade")) {
                return new Dorade2Dataset(ncd);
            }
            if (format.equals("ARCHIVE2") || format.equals("AR2V0001")) {
                return new LevelII2Dataset(ncd);
            }
            if (format.equals("Level3/NIDS")) {
                return new Nids2Dataset(ncd);
            }
        }
        return null;
    }
}
