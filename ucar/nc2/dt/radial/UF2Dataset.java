// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.dt.radial;

import ucar.ma2.Index;
import ucar.ma2.MAMath;
import ucar.ma2.IndexIterator;
import ucar.ma2.InvalidRangeException;
import ucar.ma2.Array;
import java.util.ArrayList;
import java.util.Date;
import ucar.nc2.dt.TypedDatasetFactory;
import ucar.nc2.VariableSimpleIF;
import java.util.List;
import ucar.nc2.units.DateUnit;
import ucar.nc2.constants.AxisType;
import ucar.nc2.dataset.CoordinateAxis;
import ucar.unidata.geoloc.EarthLocation;
import ucar.nc2.Attribute;
import ucar.unidata.geoloc.EarthLocationImpl;
import java.util.Iterator;
import ucar.nc2.dt.RadialDatasetSweep;
import ucar.unidata.geoloc.LatLonPoint;
import ucar.unidata.geoloc.LatLonRect;
import ucar.unidata.geoloc.LatLonPointImpl;
import ucar.unidata.geoloc.Earth;
import ucar.nc2.constants.FeatureType;
import java.io.IOException;
import ucar.nc2.dt.TypedDataset;
import ucar.nc2.util.CancelTask;
import ucar.nc2.Variable;
import ucar.nc2.units.DateFormatter;
import ucar.nc2.dataset.NetcdfDataset;
import ucar.nc2.dt.TypedDatasetFactoryIF;

public class UF2Dataset extends RadialDatasetSweepAdapter implements TypedDatasetFactoryIF
{
    private NetcdfDataset ds;
    double latv;
    double lonv;
    double elev;
    DateFormatter formatter;
    
    public boolean isMine(final NetcdfDataset ds) {
        final String convention = ds.findAttValueIgnoreCase(null, "Conventions", null);
        if (null != convention && convention.equals("_Coordinates")) {
            final String format = ds.findAttValueIgnoreCase(null, "Format", null);
            if (format.equals("UNIVERSALFORMAT")) {
                return true;
            }
        }
        return false;
    }
    
    public TypedDataset open(final NetcdfDataset ncd, final CancelTask task, final StringBuilder errlog) throws IOException {
        return new UF2Dataset(ncd);
    }
    
    public FeatureType getScientificDataType() {
        return FeatureType.RADIAL;
    }
    
    public UF2Dataset() {
        this.formatter = new DateFormatter();
    }
    
    public UF2Dataset(final NetcdfDataset ds) {
        super(ds);
        this.formatter = new DateFormatter();
        this.ds = ds;
        this.desc = "UF 2 radar dataset";
        this.setEarthLocation();
        try {
            this.setTimeUnits();
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }
        this.setStartDate();
        this.setEndDate();
        this.setBoundingBox();
    }
    
    @Override
    protected void setBoundingBox() {
        if (this.origin == null) {
            return;
        }
        final double dLat = Math.toDegrees(this.getMaximumRadialDist() / Earth.getRadius());
        final double latRadians = Math.toRadians(this.origin.getLatitude());
        final double dLon = dLat * Math.cos(latRadians);
        final double lat1 = this.origin.getLatitude() - dLat / 2.0;
        final double lon1 = this.origin.getLongitude() - dLon / 2.0;
        final LatLonRect bb = new LatLonRect(new LatLonPointImpl(lat1, lon1), dLat, dLon);
        this.boundingBox = bb;
    }
    
    double getMaximumRadialDist() {
        double maxdist = 0.0;
        for (final RadialDatasetSweep.RadialVariable rv : this.dataVariables) {
            final RadialDatasetSweep.Sweep sp = rv.getSweep(0);
            final double dist = sp.getGateNumber() * sp.getGateSize();
            if (dist > maxdist) {
                maxdist = dist;
            }
        }
        return maxdist;
    }
    
    @Override
    protected void setEarthLocation() {
        Attribute ga = this.ds.findGlobalAttribute("StationLatitude");
        if (ga != null) {
            this.latv = ga.getNumericValue().doubleValue();
        }
        else {
            this.latv = 0.0;
        }
        ga = this.ds.findGlobalAttribute("StationLongitude");
        if (ga != null) {
            this.lonv = ga.getNumericValue().doubleValue();
        }
        else {
            this.lonv = 0.0;
        }
        ga = this.ds.findGlobalAttribute("StationElevationInMeters");
        if (ga != null) {
            this.elev = ga.getNumericValue().doubleValue();
        }
        else {
            this.elev = 0.0;
        }
        this.origin = new EarthLocationImpl(this.latv, this.lonv, this.elev);
    }
    
    public EarthLocation getCommonOrigin() {
        return this.origin;
    }
    
    public String getRadarID() {
        final Attribute ga = this.ds.findGlobalAttribute("Station");
        if (ga != null) {
            return ga.getStringValue();
        }
        return "XXXX";
    }
    
    public String getRadarName() {
        final Attribute ga = this.ds.findGlobalAttribute("StationName");
        if (ga != null) {
            return ga.getStringValue();
        }
        return "Unknown Station";
    }
    
    public String getDataFormat() {
        return "Universal Format";
    }
    
    public boolean isVolume() {
        return true;
    }
    
    public boolean isStationary() {
        return true;
    }
    
    @Override
    protected void setTimeUnits() throws Exception {
        final List axes = this.ds.getCoordinateAxes();
        for (int i = 0; i < axes.size(); ++i) {
            final CoordinateAxis axis = axes.get(i);
            if (axis.getAxisType() == AxisType.Time) {
                final String units = axis.getUnitsString();
                this.dateUnits = new DateUnit(units);
                return;
            }
        }
        this.parseInfo.append("*** Time Units not Found\n");
    }
    
    @Override
    protected void setStartDate() {
        final String start_datetime = this.ds.findAttValueIgnoreCase(null, "time_coverage_start", null);
        if (start_datetime != null) {
            this.startDate = this.formatter.getISODate(start_datetime);
        }
        else {
            this.parseInfo.append("*** start_datetime not Found\n");
        }
    }
    
    @Override
    protected void setEndDate() {
        final String end_datetime = this.ds.findAttValueIgnoreCase(null, "time_coverage_end", null);
        if (end_datetime != null) {
            this.endDate = this.formatter.getISODate(end_datetime);
        }
        else {
            this.parseInfo.append("*** end_datetime not Found\n");
        }
    }
    
    public void clearDatasetMemory() {
        final List rvars = this.getDataVariables();
        for (final RadialDatasetSweep.RadialVariable radVar : rvars) {
            radVar.clearVariableMemory();
        }
    }
    
    @Override
    protected void addRadialVariable(final NetcdfDataset nds, final Variable var) {
        RadialDatasetSweep.RadialVariable rsvar = null;
        final String vName = var.getShortName();
        final int rnk = var.getRank();
        if (rnk == 3) {
            final VariableSimpleIF v = new MyRadialVariableAdapter(vName, var.getAttributes());
            rsvar = this.makeRadialVariable(nds, v, var);
        }
        if (rsvar != null) {
            this.dataVariables.add(rsvar);
        }
    }
    
    @Override
    protected RadialDatasetSweep.RadialVariable makeRadialVariable(final NetcdfDataset nds, final VariableSimpleIF v, final Variable v0) {
        return new UF2Variable(nds, v, v0);
    }
    
    public String getInfo() {
        final StringBuffer sbuff = new StringBuffer();
        sbuff.append("UF2Dataset\n");
        sbuff.append(super.getDetailInfo());
        sbuff.append("\n\n");
        sbuff.append(this.parseInfo.toString());
        return sbuff.toString();
    }
    
    private static void testRadialVariable(final RadialDatasetSweep.RadialVariable rv) throws IOException {
        for (int nsweep = rv.getNumSweeps(), i = 0; i < nsweep; ++i) {
            final RadialDatasetSweep.Sweep sw = rv.getSweep(i);
            final float me = sw.getMeanElevation();
            System.out.println("*** radar Sweep mean elevation of sweep " + i + " is: " + me);
            final int nrays = sw.getRadialNumber();
            final float[] az = new float[nrays];
            for (int j = 0; j < nrays; ++j) {
                final float azi = sw.getAzimuth(j);
                az[j] = azi;
            }
            final float[] azz = sw.getAzimuth();
            final float[] dat = sw.readData();
        }
        final RadialDatasetSweep.Sweep sw = rv.getSweep(0);
        final float[] data = rv.readAllData();
        final float[] ddd = sw.readData();
        final float[] da = sw.getAzimuth();
        final float[] de = sw.getElevation();
        assert null != ddd;
        final int nrays2 = sw.getRadialNumber();
        final float[] az2 = new float[nrays2];
        for (int k = 0; k < nrays2; ++k) {
            final int ngates = sw.getGateNumber();
            assert ngates > 0;
            final float[] d = sw.readData(k);
            assert null != d;
            final float azi2 = sw.getAzimuth(k);
            assert azi2 > 0.0f;
            az2[k] = azi2;
            final float ele = sw.getElevation(k);
            assert ele > 0.0f;
            final float la = (float)sw.getOrigin(k).getLatitude();
            assert la > 0.0f;
            final float lo = (float)sw.getOrigin(k).getLongitude();
            assert lo > 0.0f;
            final float al = (float)sw.getOrigin(k).getAltitude();
            assert al > 0.0f;
        }
        assert 0 != nrays2;
    }
    
    public static void main(final String[] args) throws Exception, IOException, InstantiationException, IllegalAccessException {
        final String fileIn = "/home/yuanho/Desktop/idv/dorade/KATX_20040113_0107";
        final long start = System.currentTimeMillis();
        final RadialDatasetSweep rds = (RadialDatasetSweep)TypedDatasetFactory.open(FeatureType.RADIAL, fileIn, null, new StringBuilder());
        final long took = System.currentTimeMillis() - start;
        System.out.println("that took = " + took + " msec");
        System.exit(0);
        final String st = rds.getStartDate().toString();
        final String et = rds.getEndDate().toString();
        final String id = rds.getRadarID();
        final String name = rds.getRadarName();
        if (rds.isStationary()) {
            System.out.println("*** radar is stationary with name and id: " + name + " " + id);
        }
        final List rvars = rds.getDataVariables();
        final RadialDatasetSweep.RadialVariable vDM = (RadialDatasetSweep.RadialVariable)rds.getDataVariable("Reflectivity");
        testRadialVariable(vDM);
        for (int i = 0; i < rvars.size(); ++i) {
            final RadialDatasetSweep.RadialVariable rv = rvars.get(i);
            testRadialVariable(rv);
        }
    }
    
    private class UF2Variable extends MyRadialVariableAdapter implements RadialDatasetSweep.RadialVariable
    {
        int nsweeps;
        ArrayList sweeps;
        String name;
        
        private UF2Variable(final NetcdfDataset nds, final VariableSimpleIF v, final Variable v0) {
            super(v.getName(), v0.getAttributes());
            this.sweeps = new ArrayList();
            this.name = v.getName();
            final int[] shape = v0.getShape();
            int count = v0.getRank() - 1;
            final int ngates = shape[count];
            --count;
            final int nrays = shape[count];
            --count;
            this.nsweeps = shape[count];
            for (int i = 0; i < this.nsweeps; ++i) {
                this.sweeps.add(new UF2Sweep(v0, i, nrays, ngates));
            }
        }
        
        @Override
        public String toString() {
            return this.name;
        }
        
        public int getNumSweeps() {
            return this.nsweeps;
        }
        
        public RadialDatasetSweep.Sweep getSweep(final int sweepNo) {
            return this.sweeps.get(sweepNo);
        }
        
        public int getNumRadials() {
            return 0;
        }
        
        public float[] readAllData() throws IOException {
            final Array hrData = null;
            final RadialDatasetSweep.Sweep spn = this.sweeps.get(this.sweeps.size() - 1);
            final Variable v = spn.getsweepVar();
            Array allData;
            try {
                allData = v.read();
            }
            catch (IOException e) {
                throw new IOException(e.getMessage());
            }
            return (float[])allData.get1DJavaArray(Float.TYPE);
        }
        
        public void clearVariableMemory() {
            for (int i = 0; i < this.nsweeps; ++i) {}
        }
        
        private class UF2Sweep implements RadialDatasetSweep.Sweep
        {
            double meanElevation;
            double meanAzimuth;
            int nrays;
            int ngates;
            int sweepno;
            Variable sweepVar;
            String abbrev;
            
            UF2Sweep(final Variable v, final int sweepno, final int rays, final int gates) {
                this.meanElevation = Double.NaN;
                this.meanAzimuth = Double.NaN;
                this.sweepVar = v;
                this.sweepno = sweepno;
                this.nrays = rays;
                this.ngates = gates;
                final Attribute att = this.sweepVar.findAttribute("abbrev");
                this.abbrev = att.getStringValue();
            }
            
            public Variable getsweepVar() {
                return this.sweepVar;
            }
            
            public float[] readData() throws IOException {
                return this.sweepData(this.sweepno);
            }
            
            private float[] sweepData(final int swpNumber) {
                final int[] shape = this.sweepVar.getShape();
                final int[] origin = new int[3];
                Array sweepTmp = null;
                origin[0] = swpNumber;
                shape[0] = 1;
                try {
                    sweepTmp = this.sweepVar.read(origin, shape).reduce();
                }
                catch (InvalidRangeException e) {
                    e.printStackTrace();
                }
                catch (IOException e2) {
                    e2.printStackTrace();
                }
                return (float[])sweepTmp.get1DJavaArray(Float.TYPE);
            }
            
            public float[] readData(final int ray) throws IOException {
                return this.rayData(this.sweepno, ray);
            }
            
            public float[] rayData(final int swpNumber, final int ray) throws IOException {
                final int[] shape = this.sweepVar.getShape();
                final int[] origin = new int[3];
                Array sweepTmp = null;
                origin[0] = swpNumber;
                origin[1] = ray;
                shape[shape[0] = 1] = 1;
                try {
                    sweepTmp = this.sweepVar.read(origin, shape).reduce();
                }
                catch (InvalidRangeException e) {
                    e.printStackTrace();
                }
                catch (IOException e2) {
                    e2.printStackTrace();
                }
                return (float[])sweepTmp.get1DJavaArray(Float.TYPE);
            }
            
            public void setMeanElevation() {
                final String eleName = "elevation" + this.abbrev;
                this.setMeanEle(eleName, this.sweepno);
            }
            
            private void setMeanEle(final String elevName, final int swpNumber) {
                Array eleData = null;
                float sum = 0.0f;
                int sumSize = 0;
                try {
                    final Array eleTmp = UF2Dataset.this.ds.findVariable(elevName).read();
                    final int[] eleOrigin = { swpNumber, 0 };
                    final int[] eleShape = { 1, this.getRadialNumber() };
                    eleData = eleTmp.section(eleOrigin, eleShape);
                }
                catch (IOException e) {
                    e.printStackTrace();
                }
                catch (InvalidRangeException e2) {
                    e2.printStackTrace();
                }
                final float[] eleArray = (float[])eleData.get1DJavaArray(Float.TYPE);
                for (int size = (int)eleData.getSize(), i = 0; i < size; ++i) {
                    if (!Float.isNaN(eleArray[i])) {
                        sum += eleArray[i];
                        ++sumSize;
                    }
                }
                this.meanElevation = sum / sumSize;
            }
            
            public float getMeanElevation() {
                if (Double.isNaN(this.meanElevation)) {
                    this.setMeanElevation();
                }
                return (float)this.meanElevation;
            }
            
            public double meanDouble(final Array a) {
                double sum = 0.0;
                int size = 0;
                final IndexIterator iterA = a.getIndexIterator();
                while (iterA.hasNext()) {
                    final double s = iterA.getDoubleNext();
                    if (!Double.isNaN(s)) {
                        sum += s;
                        ++size;
                    }
                }
                return sum / size;
            }
            
            public int getGateNumber() {
                return this.ngates;
            }
            
            public int getRadialNumber() {
                return this.nrays;
            }
            
            public RadialDatasetSweep.Type getType() {
                return null;
            }
            
            public EarthLocation getOrigin(final int ray) {
                return UF2Dataset.this.origin;
            }
            
            public Date getStartingTime() {
                return UF2Dataset.this.startDate;
            }
            
            public Date getEndingTime() {
                return UF2Dataset.this.endDate;
            }
            
            public int getSweepIndex() {
                return this.sweepno;
            }
            
            public void setMeanAzimuth() {
                final String aziName = "azimuth" + this.abbrev;
                this.setMeanAzi(aziName, this.sweepno);
            }
            
            private void setMeanAzi(final String aziName, final int swpNumber) {
                Array aziData = null;
                if (this.getType() != null) {
                    try {
                        final Array data = UF2Dataset.this.ds.findVariable(aziName).read();
                        final int[] aziOrigin = { swpNumber, 0 };
                        final int[] aziShape = { 1, this.getRadialNumber() };
                        aziData = data.section(aziOrigin, aziShape);
                        this.meanAzimuth = MAMath.sumDouble(aziData) / aziData.getSize();
                    }
                    catch (IOException e) {
                        e.printStackTrace();
                        this.meanAzimuth = 0.0;
                    }
                    catch (InvalidRangeException e2) {
                        e2.printStackTrace();
                    }
                }
                else {
                    this.meanAzimuth = 0.0;
                }
            }
            
            public float getMeanAzimuth() {
                if (Double.isNaN(this.meanAzimuth)) {
                    this.setMeanAzimuth();
                }
                return (float)this.meanAzimuth;
            }
            
            public boolean isConic() {
                return true;
            }
            
            public float getElevation(final int ray) throws IOException {
                final String eleName = "elevation" + this.abbrev;
                return this.getEle(eleName, this.sweepno, ray);
            }
            
            public float getEle(final String elevName, final int swpNumber, final int ray) throws IOException {
                Array eleData = null;
                try {
                    final Array eleTmp = UF2Dataset.this.ds.findVariable(elevName).read();
                    final int[] eleOrigin = { swpNumber, 0 };
                    final int[] eleShape = { 1, this.getRadialNumber() };
                    eleData = eleTmp.section(eleOrigin, eleShape);
                }
                catch (IOException e) {
                    e.printStackTrace();
                }
                catch (InvalidRangeException e2) {
                    e2.printStackTrace();
                }
                final Index index = eleData.getIndex();
                return eleData.getFloat(index.set(ray));
            }
            
            public float[] getElevation() throws IOException {
                final String eleName = "elevation" + this.abbrev;
                return this.getEle(eleName, this.sweepno);
            }
            
            public float[] getEle(final String elevName, final int swpNumber) throws IOException {
                Array eleData = null;
                if (eleData == null) {
                    try {
                        final Array eleTmp = UF2Dataset.this.ds.findVariable(elevName).read();
                        final int[] eleOrigin = { swpNumber, 0 };
                        final int[] eleShape = { 1, this.getRadialNumber() };
                        eleData = eleTmp.section(eleOrigin, eleShape);
                    }
                    catch (IOException e) {
                        e.printStackTrace();
                    }
                    catch (InvalidRangeException e2) {
                        e2.printStackTrace();
                    }
                }
                return (float[])eleData.get1DJavaArray(Float.TYPE);
            }
            
            public float[] getAzimuth() throws IOException {
                final String aziName = "azimuth" + this.abbrev;
                return this.getAzi(aziName, this.sweepno);
            }
            
            public float[] getAzi(final String aziName, final int swpNumber) throws IOException {
                Array aziData = null;
                if (aziData == null) {
                    try {
                        final Array aziTmp = UF2Dataset.this.ds.findVariable(aziName).read();
                        final int[] aziOrigin = { swpNumber, 0 };
                        final int[] aziShape = { 1, this.getRadialNumber() };
                        aziData = aziTmp.section(aziOrigin, aziShape);
                    }
                    catch (IOException e) {
                        e.printStackTrace();
                    }
                    catch (InvalidRangeException e2) {
                        e2.printStackTrace();
                    }
                }
                return (float[])aziData.get1DJavaArray(Float.TYPE);
            }
            
            public float getAzimuth(final int ray) throws IOException {
                final String aziName = "azimuth" + this.abbrev;
                return this.getAzi(aziName, this.sweepno, ray);
            }
            
            public float getAzi(final String aziName, final int swpNumber, final int ray) throws IOException {
                Array aziData = null;
                if (aziData == null) {
                    try {
                        final Array aziTmp = UF2Dataset.this.ds.findVariable(aziName).read();
                        final int[] aziOrigin = { swpNumber, 0 };
                        final int[] aziShape = { 1, this.getRadialNumber() };
                        aziData = aziTmp.section(aziOrigin, aziShape);
                    }
                    catch (IOException e) {
                        e.printStackTrace();
                    }
                    catch (InvalidRangeException e2) {
                        e2.printStackTrace();
                    }
                }
                final Index index = aziData.getIndex();
                return aziData.getFloat(index.set(ray));
            }
            
            public float getRadialDistance(final int gate) throws IOException {
                final String disName = "distance" + this.abbrev;
                return this.getRadialDist(disName, gate);
            }
            
            public float getRadialDist(final String dName, final int gate) throws IOException {
                final Array data = UF2Dataset.this.ds.findVariable(dName).read();
                final Index index = data.getIndex();
                return data.getFloat(index.set(gate));
            }
            
            public float getTime(final int ray) throws IOException {
                final String tName = "time" + this.abbrev;
                return this.getT(tName, this.sweepno, ray);
            }
            
            public float getT(final String tName, final int swpNumber, final int ray) throws IOException {
                final Array timeData = UF2Dataset.this.ds.findVariable(tName).read();
                final Index timeIndex = timeData.getIndex();
                return timeData.getFloat(timeIndex.set(swpNumber, ray));
            }
            
            public float getBeamWidth() {
                return 0.95f;
            }
            
            public float getNyquistFrequency() {
                return 0.0f;
            }
            
            public float getRangeToFirstGate() {
                try {
                    return this.getRadialDistance(0);
                }
                catch (IOException e) {
                    e.printStackTrace();
                    return 0.0f;
                }
            }
            
            public float getGateSize() {
                try {
                    return this.getRadialDistance(1) - this.getRadialDistance(0);
                }
                catch (IOException e) {
                    e.printStackTrace();
                    return 0.0f;
                }
            }
            
            public boolean isGateSizeConstant() {
                return true;
            }
            
            public void clearSweepMemory() {
            }
        }
    }
}
