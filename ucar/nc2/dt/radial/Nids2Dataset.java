// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.dt.radial;

import ucar.ma2.Index;
import ucar.ma2.InvalidRangeException;
import ucar.ma2.MAMath;
import ucar.ma2.Array;
import java.util.ArrayList;
import java.util.Date;
import ucar.nc2.dt.TypedDatasetFactory;
import java.util.Iterator;
import java.util.List;
import ucar.nc2.VariableSimpleIF;
import ucar.nc2.dt.RadialDatasetSweep;
import ucar.nc2.dataset.CoordinateAxis;
import ucar.nc2.units.DateUnit;
import ucar.nc2.constants.AxisType;
import ucar.unidata.geoloc.EarthLocationImpl;
import ucar.nc2.Attribute;
import ucar.unidata.geoloc.EarthLocation;
import ucar.nc2.constants.FeatureType;
import java.io.IOException;
import ucar.nc2.dt.TypedDataset;
import ucar.nc2.util.CancelTask;
import ucar.nc2.Variable;
import ucar.nc2.dataset.NetcdfDataset;
import ucar.nc2.dt.TypedDatasetFactoryIF;

public class Nids2Dataset extends RadialDatasetSweepAdapter implements TypedDatasetFactoryIF
{
    private NetcdfDataset ds;
    
    public boolean isMine(final NetcdfDataset ds) {
        final String convention = ds.findAttValueIgnoreCase(null, "Conventions", null);
        if (null != convention && convention.equals("_Coordinates")) {
            final String format = ds.findAttValueIgnoreCase(null, "Format", null);
            if (format.equals("Level3/NIDS")) {
                return true;
            }
        }
        return false;
    }
    
    public TypedDataset open(final NetcdfDataset ncd, final CancelTask task, final StringBuilder errlog) throws IOException {
        return new Nids2Dataset(ncd);
    }
    
    public FeatureType getScientificDataType() {
        return FeatureType.RADIAL;
    }
    
    public Nids2Dataset() {
    }
    
    public Nids2Dataset(final NetcdfDataset ds) {
        super(ds);
        this.ds = ds;
        this.desc = "Nids 2 radar dataset";
        try {
            if (ds.findGlobalAttribute("isRadial").getNumericValue().intValue() == 0) {
                this.parseInfo.append("*** Dataset is not a radial data\n");
                throw new IOException("Dataset is not a radial data\n");
            }
            this.setEarthLocation();
            this.setTimeUnits();
            this.setStartDate();
            this.setEndDate();
            this.setBoundingBox();
        }
        catch (Throwable e) {
            System.err.println("CDM radial dataset failed to open this dataset " + e);
        }
    }
    
    public EarthLocation getCommonOrigin() {
        return this.origin;
    }
    
    public String getRadarID() {
        Attribute att = this.ds.findGlobalAttribute("ProductStation");
        if (att == null) {
            att = this.ds.findGlobalAttribute("Product_station");
        }
        return att.getStringValue();
    }
    
    public boolean isStationary() {
        return true;
    }
    
    public String getRadarName() {
        return this.ds.findGlobalAttribute("ProductStationName").getStringValue();
    }
    
    public String getDataFormat() {
        return "Level III";
    }
    
    public boolean isVolume() {
        return false;
    }
    
    @Override
    protected void setEarthLocation() {
        double lat = 0.0;
        double lon = 0.0;
        double elev = 0.0;
        final Attribute attLat = this.ds.findGlobalAttribute("RadarLatitude");
        final Attribute attLon = this.ds.findGlobalAttribute("RadarLongitude");
        final Attribute attElev = this.ds.findGlobalAttribute("RadarAltitude");
        try {
            if (attLat == null) {
                throw new IOException("Unable to init radar location!\n");
            }
            lat = attLat.getNumericValue().doubleValue();
            if (attLon == null) {
                throw new IOException("Unable to init radar location!\n");
            }
            lon = attLon.getNumericValue().doubleValue();
            if (attElev == null) {
                throw new IOException("Unable to init radar location!\n");
            }
            elev = attElev.getNumericValue().intValue();
        }
        catch (Throwable e) {
            System.err.println("CDM radial dataset failed to open this dataset " + e);
        }
        this.origin = new EarthLocationImpl(lat, lon, elev);
    }
    
    @Override
    protected void setTimeUnits() throws Exception {
        final CoordinateAxis axis = this.ds.findCoordinateAxis(AxisType.Time);
        if (axis == null) {
            this.parseInfo.append("*** Time Units not Found\n");
        }
        else {
            final String units = axis.getUnitsString();
            this.dateUnits = new DateUnit(units);
        }
    }
    
    @Override
    protected void setStartDate() {
        final String start_datetime = this.ds.findAttValueIgnoreCase(null, "time_coverage_start", null);
        if (start_datetime != null) {
            this.startDate = DateUnit.getStandardOrISO(start_datetime);
            return;
        }
        final CoordinateAxis axis = this.ds.findCoordinateAxis(AxisType.Time);
        if (axis != null) {
            final double val = axis.getMinValue();
            this.startDate = this.dateUnits.makeDate(val);
            return;
        }
        this.parseInfo.append("*** start_datetime not Found\n");
    }
    
    @Override
    protected void setEndDate() {
        final String end_datetime = this.ds.findAttValueIgnoreCase(null, "time_coverage_end", null);
        if (end_datetime != null) {
            this.endDate = DateUnit.getStandardOrISO(end_datetime);
        }
        else {
            final CoordinateAxis axis = this.ds.findCoordinateAxis(AxisType.Time);
            if (axis != null) {
                final double val = axis.getMaxValue();
                this.endDate = this.dateUnits.makeDate(val);
                return;
            }
        }
        this.parseInfo.append("*** end_datetime not Found\n");
    }
    
    @Override
    protected void addRadialVariable(final NetcdfDataset nds, final Variable var) {
        RadialDatasetSweep.RadialVariable rsvar = null;
        final String vName = var.getShortName();
        final int rnk = var.getRank();
        if (!var.getName().endsWith("RAW") && rnk == 2) {
            final VariableSimpleIF v = new MyRadialVariableAdapter(vName, var.getAttributes());
            rsvar = new Nids2Variable(nds, v, var);
        }
        if (rsvar != null) {
            this.dataVariables.add(rsvar);
        }
    }
    
    public void clearDatasetMemory() {
        final List rvars = this.getDataVariables();
        for (final RadialDatasetSweep.RadialVariable radVar : rvars) {
            radVar.clearVariableMemory();
        }
    }
    
    @Override
    protected RadialDatasetSweep.RadialVariable makeRadialVariable(final NetcdfDataset nds, final VariableSimpleIF v, final Variable v0) {
        return null;
    }
    
    public String getInfo() {
        final StringBuffer sbuff = new StringBuffer();
        sbuff.append("Nids2Dataset\n");
        sbuff.append(super.getDetailInfo());
        sbuff.append("\n\n");
        sbuff.append(this.parseInfo.toString());
        return sbuff.toString();
    }
    
    private static void testRadialVariable(final RadialDatasetSweep.RadialVariable rv) throws IOException {
        final int nsweep = rv.getNumSweeps();
        System.out.println("*** radar Sweep number is: \n" + nsweep);
        final RadialDatasetSweep.Sweep sw = rv.getSweep(0);
        final float[] ddd = sw.readData();
        final float gsize = sw.getGateSize();
        System.out.println("*** radar Sweep gate is: \n" + gsize);
    }
    
    public static void main(final String[] args) throws Exception, IOException, InstantiationException, IllegalAccessException {
        final String fileIn1 = "/home/yuanho/Desktop/TBWI/TBWI.78ohp.20080829_1619";
        final String fileIn2 = "/home/yuanho/Desktop/TBWI/TBWI.181r0.20080829_1620";
        final RadialDatasetSweep rds = (RadialDatasetSweep)TypedDatasetFactory.open(FeatureType.RADIAL, fileIn2, null, new StringBuilder());
        final RadialDatasetSweep rds2 = (RadialDatasetSweep)TypedDatasetFactory.open(FeatureType.RADIAL, fileIn1, null, new StringBuilder());
        final String st = rds.getStartDate().toString();
        final String et = rds.getEndDate().toString();
        final String id = rds.getRadarID();
        final String name = rds.getRadarName();
        rds.getRadarID();
        final RadialDatasetSweep.RadialVariable rf = (RadialDatasetSweep.RadialVariable)rds.getDataVariable("BaseReflectivity");
        testRadialVariable(rf);
        final RadialDatasetSweep.RadialVariable rf2 = (RadialDatasetSweep.RadialVariable)rds2.getDataVariable("Precip1hr");
        testRadialVariable(rf2);
    }
    
    private class Nids2Variable extends MyRadialVariableAdapter implements RadialDatasetSweep.RadialVariable
    {
        ArrayList sweeps;
        int nsweeps;
        String name;
        
        private Nids2Variable(final NetcdfDataset nds, final VariableSimpleIF v, final Variable v0) {
            super(v.getName(), v0.getAttributes());
            this.sweeps = new ArrayList();
            this.nsweeps = 0;
            this.name = v.getName();
            final int[] shape = v0.getShape();
            int count = v0.getRank() - 1;
            final int ngates = shape[count];
            --count;
            final int nrays = shape[count];
            --count;
            this.sweeps.add(new Nids2Sweep(nds, v0, 0, nrays, ngates));
        }
        
        @Override
        public String toString() {
            return this.name;
        }
        
        public float[] readAllData() throws IOException {
            final RadialDatasetSweep.Sweep spn = this.sweeps.get(0);
            final Variable v = spn.getsweepVar();
            Array allData;
            try {
                allData = v.read();
            }
            catch (IOException e) {
                throw new IOException(e.getMessage());
            }
            return (float[])allData.get1DJavaArray(Float.TYPE);
        }
        
        public int getNumSweeps() {
            return 1;
        }
        
        public RadialDatasetSweep.Sweep getSweep(final int sn) {
            if (sn != 0) {
                return null;
            }
            return this.sweeps.get(sn);
        }
        
        public void clearVariableMemory() {
        }
        
        private class Nids2Sweep implements RadialDatasetSweep.Sweep
        {
            double meanElevation;
            double meanAzimuth;
            double gateSize;
            int sweepno;
            int nrays;
            int ngates;
            Variable sweepVar;
            NetcdfDataset ds;
            
            Nids2Sweep(final NetcdfDataset nds, final Variable v, final int sweepno, final int rays, final int gates) {
                this.meanElevation = Double.NaN;
                this.meanAzimuth = Double.NaN;
                this.gateSize = Double.NaN;
                this.sweepVar = v;
                this.nrays = rays;
                this.ngates = gates;
                this.sweepno = sweepno;
                this.ds = nds;
            }
            
            public Variable getsweepVar() {
                return this.sweepVar;
            }
            
            private void setMeanElevation() {
                Array spData = null;
                if (Double.isNaN(this.meanElevation)) {
                    try {
                        final Variable sp = this.ds.findVariable("elevation");
                        spData = sp.read();
                        sp.setCachedData(spData, false);
                    }
                    catch (IOException e) {
                        e.printStackTrace();
                        this.meanElevation = 0.0;
                    }
                    this.meanElevation = MAMath.sumDouble(spData) / spData.getSize();
                }
            }
            
            public float getMeanElevation() {
                if (Double.isNaN(this.meanElevation)) {
                    this.setMeanElevation();
                }
                return (float)this.meanElevation;
            }
            
            private void setMeanAzimuth() {
                if (this.getType() != null) {
                    Array spData = null;
                    try {
                        final Variable sp = this.ds.findVariable("azimuth");
                        spData = sp.read();
                        sp.setCachedData(spData, false);
                    }
                    catch (IOException e) {
                        e.printStackTrace();
                        this.meanAzimuth = 0.0;
                    }
                    this.meanAzimuth = MAMath.sumDouble(spData) / spData.getSize();
                }
                else {
                    this.meanAzimuth = 0.0;
                }
            }
            
            public float getMeanAzimuth() {
                if (Double.isNaN(this.meanAzimuth)) {
                    this.setMeanAzimuth();
                }
                return (float)this.meanAzimuth;
            }
            
            public int getNumRadials() {
                return this.nrays;
            }
            
            public int getNumGates() {
                return this.ngates;
            }
            
            public float[] readData() throws IOException {
                Array allData = null;
                final int[] shape = this.sweepVar.getShape();
                final int[] origind = new int[this.sweepVar.getRank()];
                try {
                    allData = this.sweepVar.read(origind, shape);
                }
                catch (InvalidRangeException e) {
                    throw new IOException(e.getMessage());
                }
                return (float[])allData.get1DJavaArray(Float.TYPE);
            }
            
            public float[] readData(final int ray) throws IOException {
                final int[] shape = this.sweepVar.getShape();
                final int[] origind = new int[this.sweepVar.getRank()];
                shape[0] = 1;
                origind[0] = ray;
                Array rayData;
                try {
                    rayData = this.sweepVar.read(origind, shape);
                }
                catch (InvalidRangeException e) {
                    throw new IOException(e.getMessage());
                }
                return (float[])rayData.get1DJavaArray(Float.TYPE);
            }
            
            public RadialDatasetSweep.Type getType() {
                return null;
            }
            
            public boolean isConic() {
                return true;
            }
            
            public float getElevation(final int ray) throws IOException {
                return (float)this.meanElevation;
            }
            
            public float[] getElevation() throws IOException {
                float[] spArray = null;
                try {
                    final Variable sp = this.ds.findVariable("elevation");
                    final Array spData = sp.read();
                    sp.setCachedData(spData, false);
                    spArray = (float[])spData.get1DJavaArray(Float.TYPE);
                }
                catch (IOException e) {
                    e.printStackTrace();
                }
                return spArray;
            }
            
            public float getAzimuth(final int ray) throws IOException {
                Array spData = null;
                try {
                    final Variable sp = this.ds.findVariable("azimuth");
                    spData = sp.read();
                    sp.setCachedData(spData, false);
                }
                catch (IOException e) {
                    e.printStackTrace();
                }
                final Index index = spData.getIndex();
                return spData.getFloat(index.set(ray));
            }
            
            public float[] getAzimuth() throws IOException {
                float[] spArray = null;
                try {
                    final Variable sp = this.ds.findVariable("azimuth");
                    final Array spData = sp.read();
                    sp.setCachedData(spData, false);
                    spArray = (float[])spData.get1DJavaArray(Float.TYPE);
                }
                catch (IOException e) {
                    e.printStackTrace();
                }
                return spArray;
            }
            
            public float getRadialDistance(final int gate) throws IOException {
                Array spData = null;
                try {
                    final Variable sp = this.ds.findVariable("gate");
                    spData = sp.read();
                    sp.setCachedData(spData, false);
                }
                catch (IOException e) {
                    e.printStackTrace();
                }
                final Index index = spData.getIndex();
                return spData.getFloat(index.set(gate));
            }
            
            public float getTime(final int ray) throws IOException {
                Array timeData = null;
                try {
                    final Variable sp = this.ds.findVariable("rays_time");
                    timeData = sp.read();
                    sp.setCachedData(timeData, false);
                }
                catch (IOException e) {
                    e.printStackTrace();
                }
                final Index index = timeData.getIndex();
                return timeData.getFloat(index.set(ray));
            }
            
            public float getBeamWidth() {
                return 0.95f;
            }
            
            public float getNyquistFrequency() {
                return 0.0f;
            }
            
            public float getRangeToFirstGate() {
                return 0.0f;
            }
            
            public float getGateSize() {
                try {
                    if (Double.isNaN(this.gateSize)) {
                        this.gateSize = this.getRadialDistance(1) - this.getRadialDistance(0);
                    }
                    return (float)this.gateSize;
                }
                catch (IOException e) {
                    e.printStackTrace();
                    return 0.0f;
                }
            }
            
            public Date getStartingTime() {
                return Nids2Dataset.this.startDate;
            }
            
            public Date getEndingTime() {
                return Nids2Dataset.this.endDate;
            }
            
            public boolean isGateSizeConstant() {
                return true;
            }
            
            public int getGateNumber() {
                return this.ngates;
            }
            
            public int getRadialNumber() {
                return this.nrays;
            }
            
            public EarthLocation getOrigin(final int ray) {
                return Nids2Dataset.this.origin;
            }
            
            public int getSweepIndex() {
                return 0;
            }
            
            public void clearSweepMemory() {
            }
        }
    }
}
