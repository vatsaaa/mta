// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.dt.radial;

import ucar.unidata.util.Product;
import java.util.ArrayList;
import ucar.nc2.util.CancelTask;
import java.util.List;
import ucar.unidata.geoloc.LatLonPoint;
import ucar.unidata.geoloc.LatLonPointImpl;
import thredds.catalog.query.Station;
import java.io.IOException;
import ucar.unidata.geoloc.LatLonRect;
import ucar.nc2.units.DateUnit;
import java.util.HashMap;
import ucar.nc2.dt.StationRadarCollection;
import ucar.nc2.dt.TypedDatasetImpl;

public abstract class StationRadarCollectionImpl extends TypedDatasetImpl implements StationRadarCollection
{
    private StationRadarCollection radarCollection;
    protected HashMap stations;
    protected HashMap relTimesList;
    protected HashMap absTimesList;
    protected DateUnit timeUnit;
    private LatLonRect rect;
    
    public StationRadarCollectionImpl() {
    }
    
    public StationRadarCollectionImpl(final StationRadarCollection radarDataset) {
        this.radarCollection = radarDataset;
    }
    
    @Override
    public LatLonRect getBoundingBox() {
        if (this.rect == null) {
            List stations = null;
            try {
                stations = this.radarCollection.getStations();
            }
            catch (IOException e) {
                return null;
            }
            if (stations.size() == 0) {
                return null;
            }
            Station s = stations.get(0);
            final LatLonPointImpl llpt = new LatLonPointImpl();
            llpt.set(s.getLocation().getLatitude(), s.getLocation().getLongitude());
            this.rect = new LatLonRect(llpt, 0.001, 0.001);
            for (int i = 1; i < stations.size(); ++i) {
                s = stations.get(i);
                llpt.set(s.getLocation().getLatitude(), s.getLocation().getLongitude());
                this.rect.extend(llpt);
            }
        }
        return this.rect;
    }
    
    public List getStations(final LatLonRect boundingBox, final CancelTask cancel) throws IOException {
        final LatLonPointImpl latlonPt = new LatLonPointImpl();
        final ArrayList result = new ArrayList();
        final List stationC = this.radarCollection.getStations();
        for (int i = 0; i < stationC.size(); ++i) {
            final Station s = stationC.get(i);
            latlonPt.set(s.getLocation().getLatitude(), s.getLocation().getLongitude());
            if (boundingBox.contains(latlonPt)) {
                result.add(s);
            }
            if (cancel != null && cancel.isCancel()) {
                return null;
            }
        }
        return result;
    }
    
    public boolean checkStationProduct(final String stationName, final Product product) {
        return false;
    }
    
    public Station getStation(final String name) throws IOException {
        if (this.stations == null) {
            List stationC = null;
            try {
                stationC = this.radarCollection.getStations();
            }
            catch (IOException e) {
                return null;
            }
            this.stations = new HashMap(2 * this.stations.size());
            for (int i = 0; i < stationC.size(); ++i) {
                final Station s = stationC.get(i);
                this.stations.put(s.getName(), s);
            }
        }
        return this.stations.get(name);
    }
    
    public List getStations() throws IOException {
        return this.radarCollection.getStations();
    }
    
    public List getStations(final CancelTask cancel) throws IOException {
        if (cancel != null && cancel.isCancel()) {
            return null;
        }
        return this.getStations();
    }
    
    public List getStations(final LatLonRect boundingBox) throws IOException {
        final LatLonPointImpl latlonPt = new LatLonPointImpl();
        final ArrayList result = new ArrayList();
        final List stationC = this.radarCollection.getStations();
        for (int i = 0; i < stationC.size(); ++i) {
            final Station s = stationC.get(i);
            latlonPt.set(s.getLocation().getLatitude(), s.getLocation().getLongitude());
            if (boundingBox.contains(latlonPt)) {
                result.add(s);
            }
        }
        return result;
    }
}
