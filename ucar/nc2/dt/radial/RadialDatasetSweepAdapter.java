// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.dt.radial;

import ucar.nc2.Dimension;
import ucar.ma2.DataType;
import ucar.nc2.Attribute;
import ucar.nc2.util.cache.FileCacheable;
import java.util.Formatter;
import ucar.nc2.units.DateRange;
import ucar.nc2.constants.FeatureType;
import java.io.IOException;
import java.util.Iterator;
import ucar.unidata.geoloc.LatLonRect;
import ucar.nc2.VariableSimpleIF;
import java.util.List;
import ucar.nc2.Variable;
import ucar.nc2.dataset.NetcdfDataset;
import ucar.nc2.util.cache.FileCache;
import ucar.nc2.units.DateUnit;
import java.util.HashMap;
import ucar.unidata.geoloc.EarthLocation;
import ucar.nc2.dt.RadialDatasetSweep;
import ucar.nc2.dt.TypedDatasetImpl;

public abstract class RadialDatasetSweepAdapter extends TypedDatasetImpl implements RadialDatasetSweep
{
    protected EarthLocation origin;
    protected HashMap csHash;
    protected DateUnit dateUnits;
    protected FileCache fileCache;
    
    public RadialDatasetSweepAdapter() {
        this.csHash = new HashMap();
    }
    
    public RadialDatasetSweepAdapter(final NetcdfDataset ds) {
        super(ds);
        this.csHash = new HashMap();
        this.parseInfo.append("RadialDatasetAdapter look for RadialVariables\n");
        final List vars = ds.getVariables();
        for (int i = 0; i < vars.size(); ++i) {
            this.addRadialVariable(ds, vars.get(i));
        }
    }
    
    protected abstract void addRadialVariable(final NetcdfDataset p0, final Variable p1);
    
    protected abstract RadialVariable makeRadialVariable(final NetcdfDataset p0, final VariableSimpleIF p1, final Variable p2);
    
    protected abstract void setTimeUnits() throws Exception;
    
    @Override
    public String getDetailInfo() {
        final StringBuilder sbuff = new StringBuilder();
        sbuff.append(" Radar ID = " + this.getRadarID() + "\n");
        sbuff.append(" Radar Name = " + this.getRadarName() + "\n");
        sbuff.append(" Data Format Name= " + this.getDataFormat() + "\n");
        sbuff.append(" Common Type = " + this.getCommonType() + "\n");
        sbuff.append(" Common Origin = " + this.getCommonOrigin() + "\n");
        sbuff.append(" Date Unit = " + this.getTimeUnits().getUnitsString() + "\n");
        sbuff.append(" isStationary = " + this.isStationary() + "\n");
        sbuff.append(" isVolume = " + this.isVolume() + "\n");
        sbuff.append("\n");
        sbuff.append(super.getDetailInfo());
        return sbuff.toString();
    }
    
    protected abstract void setEarthLocation();
    
    public Type getCommonType() {
        return null;
    }
    
    public DateUnit getTimeUnits() {
        return this.dateUnits;
    }
    
    public EarthLocation getEarthLocation() {
        return this.origin;
    }
    
    @Override
    protected void setBoundingBox() {
        LatLonRect largestBB = null;
        for (final Object o : this.csHash.values()) {
            final RadialCoordSys sys = (RadialCoordSys)o;
            sys.setOrigin(this.origin);
            final LatLonRect bb = sys.getBoundingBox();
            if (largestBB == null) {
                largestBB = bb;
            }
            else {
                largestBB.extend(bb);
            }
        }
        this.boundingBox = largestBB;
    }
    
    public void calcBounds() throws IOException {
        this.setBoundingBox();
        try {
            this.setTimeUnits();
        }
        catch (Exception e) {
            throw new IOException(e.getMessage());
        }
        this.setStartDate();
        this.setEndDate();
    }
    
    public FeatureType getFeatureType() {
        return FeatureType.RADIAL;
    }
    
    public DateRange getDateRange() {
        return new DateRange(this.getStartDate(), this.getEndDate());
    }
    
    public void getDetailInfo(final Formatter sf) {
        sf.format("%s", this.getDetailInfo());
    }
    
    public String getImplementationName() {
        return this.getClass().getName();
    }
    
    @Override
    public synchronized void close() throws IOException {
        if (this.fileCache != null) {
            this.fileCache.release(this);
        }
        else {
            try {
                if (this.ncfile != null) {
                    this.ncfile.close();
                }
            }
            finally {
                this.ncfile = null;
            }
        }
    }
    
    public boolean sync() throws IOException {
        return false;
    }
    
    public void setFileCache(final FileCache fileCache) {
        this.fileCache = fileCache;
    }
    
    public class MyRadialVariableAdapter implements VariableSimpleIF
    {
        private int rank;
        private int[] shape;
        private String name;
        private String desp;
        private List<Attribute> attributes;
        
        public MyRadialVariableAdapter(final String vName, final List<Attribute> atts) {
            this.rank = 1;
            this.shape = new int[] { 1 };
            this.name = vName;
            this.desp = "A radial variable holding a list of radial sweeps";
            this.attributes = atts;
        }
        
        @Override
        public String toString() {
            return this.name;
        }
        
        public int compareTo(final VariableSimpleIF o) {
            return this.getName().compareTo(o.getName());
        }
        
        public String getName() {
            return this.name;
        }
        
        public String getShortName() {
            return this.name;
        }
        
        public DataType getDataType() {
            return DataType.FLOAT;
        }
        
        public String getDescription() {
            return this.desp;
        }
        
        public String getInfo() {
            return this.desp;
        }
        
        public String getUnitsString() {
            return "N/A";
        }
        
        public int getRank() {
            return this.rank;
        }
        
        public int[] getShape() {
            return this.shape;
        }
        
        public List<Dimension> getDimensions() {
            return null;
        }
        
        public List<Attribute> getAttributes() {
            return this.attributes;
        }
        
        public Attribute findAttributeIgnoreCase(final String attName) {
            final Iterator it = this.attributes.iterator();
            Attribute at = null;
            while (it.hasNext()) {
                at = it.next();
                if (attName.equalsIgnoreCase(at.getName())) {
                    break;
                }
            }
            return at;
        }
    }
}
