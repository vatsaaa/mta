// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.dt.radial;

import ucar.ma2.Index;
import ucar.ma2.IndexIterator;
import ucar.ma2.InvalidRangeException;
import java.util.ArrayList;
import java.util.Date;
import ucar.nc2.dt.TypedDatasetFactory;
import java.util.Iterator;
import ucar.nc2.VariableSimpleIF;
import ucar.nc2.dt.RadialDatasetSweep;
import java.util.List;
import ucar.nc2.units.DateUnit;
import ucar.unidata.geoloc.EarthLocationImpl;
import ucar.unidata.geoloc.EarthLocation;
import ucar.ma2.Array;
import ucar.nc2.dataset.CoordinateAxis;
import ucar.nc2.Attribute;
import ucar.nc2.constants.AxisType;
import ucar.nc2.dataset.CoordinateAxis1D;
import ucar.ma2.DataType;
import ucar.nc2.Group;
import ucar.nc2.constants.FeatureType;
import java.io.IOException;
import ucar.nc2.dt.TypedDataset;
import ucar.nc2.util.CancelTask;
import ucar.nc2.Dimension;
import ucar.nc2.Variable;
import ucar.nc2.dataset.NetcdfDataset;
import ucar.nc2.dt.TypedDatasetFactoryIF;

public class Netcdf2Dataset extends RadialDatasetSweepAdapter implements TypedDatasetFactoryIF
{
    private NetcdfDataset ds;
    private boolean isVolume;
    private double latv;
    private double lonv;
    private double elev;
    
    public boolean isMine(final NetcdfDataset ds) {
        final String format = ds.findAttValueIgnoreCase(null, "format", null);
        if (format != null && format.startsWith("nssl/netcdf")) {
            return true;
        }
        final Dimension az = ds.findDimension("Azimuth");
        final Dimension gt = ds.findDimension("Gate");
        return null != az && null != gt;
    }
    
    public TypedDataset open(final NetcdfDataset ncd, final CancelTask task, final StringBuilder errlog) throws IOException {
        return new Netcdf2Dataset(ncd);
    }
    
    public FeatureType getScientificDataType() {
        return FeatureType.RADIAL;
    }
    
    public Netcdf2Dataset() {
    }
    
    public Netcdf2Dataset(final NetcdfDataset ds) {
        super(ds);
        this.ds = ds;
        this.desc = "Netcdf/NCML 2 radar dataset";
        this.setEarthLocation();
        try {
            this.setTimeUnits();
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }
        this.setStartDate();
        this.setEndDate();
        this.setBoundingBox();
        try {
            this.addCoordSystem(ds);
        }
        catch (IOException e2) {
            e2.printStackTrace();
        }
    }
    
    private void addCoordSystem(final NetcdfDataset ds) throws IOException {
        double ele = 0.0;
        Attribute attr = ds.findGlobalAttributeIgnoreCase("Elevation");
        if (attr != null) {
            ele = attr.getNumericValue().doubleValue();
        }
        final Variable sp = ds.findVariable("sweep");
        if (sp == null) {
            ds.addDimension(null, new Dimension("Elevation", 1, true));
            final String lName = "elevation angle in degres: 0 = parallel to pedestal base, 90 = perpendicular";
            final CoordinateAxis v = new CoordinateAxis1D(ds, null, "Elevation", DataType.DOUBLE, "Elevation", "degrees", lName);
            ds.setValues(v, 1, ele, 0.0);
            v.addAttribute(new Attribute("_CoordinateAxisType", AxisType.RadialElevation.toString()));
            ds.addVariable(null, v);
        }
        else {
            final Array spdata = sp.read();
            final float[] spd = (float[])spdata.get1DJavaArray(Float.TYPE);
            final int spsize = spd.length;
            ds.addDimension(null, new Dimension("Elevation", spsize, true));
            final String lName2 = "elevation angle in degres: 0 = parallel to pedestal base, 90 = perpendicular";
            final CoordinateAxis v2 = new CoordinateAxis1D(ds, null, "Elevation", DataType.DOUBLE, "Elevation", "degrees", lName2);
            v2.addAttribute(new Attribute("_CoordinateAxisType", AxisType.RadialElevation.toString()));
            ds.addVariable(null, v2);
        }
        ds.addAttribute(null, new Attribute("IsRadial", new Integer(1)));
        attr = ds.findGlobalAttributeIgnoreCase("vcp-value");
        String vcp;
        if (attr == null) {
            vcp = "11";
        }
        else {
            vcp = attr.getStringValue();
        }
        ds.addAttribute(null, new Attribute("VolumeCoveragePatternName", vcp));
        ds.finish();
    }
    
    public EarthLocation getCommonOrigin() {
        return this.origin;
    }
    
    public String getRadarID() {
        final Attribute ga = this.ds.findGlobalAttribute("radarName-value");
        if (ga != null) {
            return ga.getStringValue();
        }
        return "XXXX";
    }
    
    public boolean isStationary() {
        return true;
    }
    
    public String getRadarName() {
        return this.ds.findGlobalAttribute("ProductStationName").getStringValue();
    }
    
    public String getDataFormat() {
        return "Level II";
    }
    
    public void setIsVolume(final NetcdfDataset nds) {
        final String format = nds.findAttValueIgnoreCase(null, "volume", null);
        if (format == null) {
            this.isVolume = false;
            return;
        }
        if (format.equals("true")) {
            this.isVolume = true;
        }
        else {
            this.isVolume = false;
        }
    }
    
    public boolean isVolume() {
        return this.isVolume;
    }
    
    @Override
    protected void setEarthLocation() {
        Attribute ga = this.ds.findGlobalAttribute("Latitude");
        if (ga != null) {
            this.latv = ga.getNumericValue().doubleValue();
        }
        else {
            this.latv = 0.0;
        }
        ga = this.ds.findGlobalAttribute("Longitude");
        if (ga != null) {
            this.lonv = ga.getNumericValue().doubleValue();
        }
        else {
            this.lonv = 0.0;
        }
        ga = this.ds.findGlobalAttribute("Height");
        if (ga != null) {
            this.elev = ga.getNumericValue().doubleValue();
        }
        else {
            this.elev = 0.0;
        }
        this.origin = new EarthLocationImpl(this.latv, this.lonv, this.elev);
    }
    
    @Override
    protected void setTimeUnits() throws Exception {
        final List axes = this.ds.getCoordinateAxes();
        for (int i = 0; i < axes.size(); ++i) {
            final CoordinateAxis axis = axes.get(i);
            if (axis.getAxisType() == AxisType.Time) {
                final String units = axis.getUnitsString();
                this.dateUnits = new DateUnit(units);
                return;
            }
        }
        this.parseInfo.append("*** Time Units not Found\n");
    }
    
    @Override
    protected void setStartDate() {
        final String start_datetime = this.ds.findAttValueIgnoreCase(null, "time_coverage_start", null);
        if (start_datetime != null) {
            this.startDate = DateUnit.getStandardOrISO(start_datetime);
        }
        else {
            this.parseInfo.append("*** start_datetime not Found\n");
        }
    }
    
    @Override
    protected void setEndDate() {
        final String end_datetime = this.ds.findAttValueIgnoreCase(null, "time_coverage_end", null);
        if (end_datetime != null) {
            this.endDate = DateUnit.getStandardOrISO(end_datetime);
        }
        else {
            this.parseInfo.append("*** end_datetime not Found\n");
        }
    }
    
    @Override
    protected void addRadialVariable(final NetcdfDataset nds, final Variable var) {
        RadialDatasetSweep.RadialVariable rsvar = null;
        final String vName = var.getShortName();
        final int rnk = var.getRank();
        this.setIsVolume(nds);
        if (this.isVolume && rnk == 3) {
            final VariableSimpleIF v = new MyRadialVariableAdapter(vName, var.getAttributes());
            rsvar = this.makeRadialVariable(nds, v, var);
        }
        else if (!this.isVolume && rnk == 2) {
            final VariableSimpleIF v = new MyRadialVariableAdapter(vName, var.getAttributes());
            rsvar = this.makeRadialVariable(nds, v, var);
        }
        if (rsvar != null) {
            this.dataVariables.add(rsvar);
        }
    }
    
    @Override
    protected RadialDatasetSweep.RadialVariable makeRadialVariable(final NetcdfDataset nds, final VariableSimpleIF v, final Variable v0) {
        return new Netcdf2Variable(nds, v, v0);
    }
    
    public void clearDatasetMemory() {
        final List rvars = this.getDataVariables();
        for (final RadialDatasetSweep.RadialVariable radVar : rvars) {
            radVar.clearVariableMemory();
        }
    }
    
    public String getInfo() {
        final StringBuffer sbuff = new StringBuffer();
        sbuff.append("Netcdfs2Dataset\n");
        sbuff.append(super.getDetailInfo());
        sbuff.append("\n\n");
        sbuff.append(this.parseInfo.toString());
        return sbuff.toString();
    }
    
    private static void testRadialVariable(final RadialDatasetSweep.RadialVariable rv) throws IOException {
        for (int nsweep = rv.getNumSweeps(), i = 0; i < nsweep; ++i) {
            final RadialDatasetSweep.Sweep sw = rv.getSweep(i);
            final float mele = sw.getMeanElevation();
            final float me = sw.getMeanElevation();
            System.out.println("*** radar Sweep mean elevation of sweep " + i + " is: " + me);
            final int nrays = sw.getRadialNumber();
            final float[] az = new float[nrays];
            for (int j = 0; j < nrays; ++j) {
                final float azi = sw.getAzimuth(j);
                az[j] = azi;
            }
        }
        final RadialDatasetSweep.Sweep sw = rv.getSweep(0);
        final float[] ddd = sw.readData();
        final float[] da = sw.getAzimuth();
        final float[] de = sw.getElevation();
        assert null != ddd;
        final int nrays2 = sw.getRadialNumber();
        final float[] az = new float[nrays2];
        for (int k = 0; k < nrays2; ++k) {
            final int ngates = sw.getGateNumber();
            assert ngates > 0;
            final float[] d = sw.readData(k);
            assert null != d;
            final float azi2 = sw.getAzimuth(k);
            assert azi2 > 0.0f;
            az[k] = azi2;
            final float ele = sw.getElevation(k);
            assert ele > 0.0f;
            final float la = (float)sw.getOrigin(k).getLatitude();
            assert la > 0.0f;
            final float lo = (float)sw.getOrigin(k).getLongitude();
            assert lo > 0.0f;
            final float al = (float)sw.getOrigin(k).getAltitude();
            assert al > 0.0f;
        }
        assert 0 != nrays2;
    }
    
    public static void main(final String[] args) throws Exception, IOException, InstantiationException, IllegalAccessException {
        final String fileIn = "/home/yuanho/nssl/netcdf.ncml";
        final RadialDatasetSweep rds = (RadialDatasetSweep)TypedDatasetFactory.open(FeatureType.RADIAL, fileIn, null, new StringBuilder());
        rds.getRadarID();
        final List rvars = rds.getDataVariables();
        final RadialDatasetSweep.RadialVariable rf = (RadialDatasetSweep.RadialVariable)rds.getDataVariable("Reflectivity");
        rf.getSweep(0);
        testRadialVariable(rf);
    }
    
    private class Netcdf2Variable extends MyRadialVariableAdapter implements RadialDatasetSweep.RadialVariable
    {
        ArrayList sweeps;
        int nsweeps;
        String name;
        
        private Netcdf2Variable(final NetcdfDataset nds, final VariableSimpleIF v, final Variable v0) {
            super(v.getName(), v0.getAttributes());
            this.sweeps = new ArrayList();
            this.nsweeps = 0;
            this.name = v.getName();
            final int[] shape = v0.getShape();
            int count = v0.getRank() - 1;
            final int ngates = shape[count];
            --count;
            final int nrays = shape[count];
            --count;
            if (shape.length == 3) {
                this.nsweeps = shape[count];
            }
            else {
                this.nsweeps = 1;
            }
            for (int i = 0; i < this.nsweeps; ++i) {
                this.sweeps.add(new Netcdf2Sweep(v0, i, nrays, ngates));
            }
        }
        
        @Override
        public String toString() {
            return this.name;
        }
        
        public float[] readAllData() throws IOException {
            final RadialDatasetSweep.Sweep spn = this.sweeps.get(0);
            final Variable v = spn.getsweepVar();
            Array allData;
            try {
                allData = v.read();
            }
            catch (IOException e) {
                throw new IOException(e.getMessage());
            }
            return (float[])allData.get1DJavaArray(Float.TYPE);
        }
        
        public int getNumSweeps() {
            return this.nsweeps;
        }
        
        public RadialDatasetSweep.Sweep getSweep(final int sweepNo) {
            return this.sweeps.get(sweepNo);
        }
        
        public void clearVariableMemory() {
        }
        
        private class Netcdf2Sweep implements RadialDatasetSweep.Sweep
        {
            double meanElevation;
            double meanAzimuth;
            int sweepno;
            int nrays;
            int ngates;
            Variable sweepVar;
            
            Netcdf2Sweep(final Variable v, final int sweepno, final int rays, final int gates) {
                this.meanElevation = Double.NaN;
                this.meanAzimuth = Double.NaN;
                this.sweepno = sweepno;
                this.nrays = rays;
                this.ngates = gates;
                this.sweepVar = v;
            }
            
            public Variable getsweepVar() {
                return this.sweepVar;
            }
            
            public float[] readData() throws IOException {
                final int[] shape = this.sweepVar.getShape();
                final int[] origind = new int[this.sweepVar.getRank()];
                if (Netcdf2Dataset.this.isVolume) {
                    origind[0] = this.sweepno;
                    shape[origind[1] = 0] = 1;
                }
                Array allData;
                try {
                    allData = this.sweepVar.read(origind, shape);
                }
                catch (InvalidRangeException e) {
                    throw new IOException(e.getMessage());
                }
                final int nradials = shape[0];
                final int ngates = shape[1];
                final IndexIterator dataIter = allData.getIndexIterator();
                for (int j = 0; j < nradials; ++j) {
                    for (int i = 0; i < ngates; ++i) {
                        final float tp = dataIter.getFloatNext();
                        if (tp == -32768.0f || tp == -99900.0f) {
                            dataIter.setFloatCurrent(Float.NaN);
                        }
                    }
                }
                return (float[])allData.get1DJavaArray(Float.TYPE);
            }
            
            public float[] readData(final int ray) throws IOException {
                final int[] shape = this.sweepVar.getShape();
                final int[] origind = new int[this.sweepVar.getRank()];
                if (Netcdf2Dataset.this.isVolume) {
                    origind[0] = this.sweepno;
                    origind[1] = ray;
                    shape[shape[0] = 1] = 1;
                }
                else {
                    shape[0] = 1;
                    origind[0] = ray;
                }
                Array rayData;
                try {
                    rayData = this.sweepVar.read(origind, shape);
                }
                catch (InvalidRangeException e) {
                    throw new IOException(e.getMessage());
                }
                final int ngates = shape[1];
                final IndexIterator dataIter = rayData.getIndexIterator();
                for (int i = 0; i < ngates; ++i) {
                    final float tp = dataIter.getFloatNext();
                    if (tp == -32768.0f || tp == -99900.0f) {
                        dataIter.setFloatCurrent(Float.NaN);
                    }
                }
                return (float[])rayData.get1DJavaArray(Float.TYPE);
            }
            
            private void setMeanElevation() {
                if (Netcdf2Dataset.this.isVolume) {
                    try {
                        final Variable sp = Netcdf2Dataset.this.ds.findVariable("sweep");
                        final Array spData = sp.read();
                        final float[] spArray = (float[])spData.get1DJavaArray(Float.TYPE);
                        this.meanElevation = spArray[this.sweepno];
                    }
                    catch (IOException e) {
                        e.printStackTrace();
                        this.meanElevation = 0.0;
                    }
                }
                else {
                    final Attribute data = Netcdf2Dataset.this.ds.findGlobalAttribute("Elevation");
                    this.meanElevation = data.getNumericValue().doubleValue();
                }
            }
            
            public float getMeanElevation() {
                if (Double.isNaN(this.meanElevation)) {
                    this.setMeanElevation();
                }
                return (float)this.meanElevation;
            }
            
            public double meanDouble(final Array a) {
                double sum = 0.0;
                int size = 0;
                final IndexIterator iterA = a.getIndexIterator();
                while (iterA.hasNext()) {
                    final double s = iterA.getDoubleNext();
                    if (!Double.isNaN(s)) {
                        sum += s;
                        ++size;
                    }
                }
                return sum / size;
            }
            
            public int getGateNumber() {
                return this.ngates;
            }
            
            public int getRadialNumber() {
                return this.nrays;
            }
            
            public RadialDatasetSweep.Type getType() {
                return null;
            }
            
            public EarthLocation getOrigin(final int ray) {
                return Netcdf2Dataset.this.origin;
            }
            
            public Date getStartingTime() {
                return Netcdf2Dataset.this.startDate;
            }
            
            public Date getEndingTime() {
                return Netcdf2Dataset.this.endDate;
            }
            
            public int getSweepIndex() {
                return this.sweepno;
            }
            
            private void setMeanAzimuth() {
                this.meanAzimuth = 0.0;
            }
            
            public float getMeanAzimuth() {
                if (Double.isNaN(this.meanAzimuth)) {
                    this.setMeanAzimuth();
                }
                return (float)this.meanAzimuth;
            }
            
            public boolean isConic() {
                return true;
            }
            
            public float getElevation(final int ray) throws IOException {
                return (float)this.meanElevation;
            }
            
            public float[] getElevation() throws IOException {
                final float[] dataValue = new float[this.nrays];
                for (int i = 0; i < this.nrays; ++i) {
                    dataValue[i] = (float)this.meanElevation;
                }
                return dataValue;
            }
            
            public float[] getAzimuth() throws IOException {
                Array aziData = null;
                try {
                    final Variable azi = Netcdf2Dataset.this.ds.findVariable("Azimuth");
                    aziData = azi.read();
                }
                catch (IOException e) {
                    e.printStackTrace();
                    this.meanElevation = 0.0;
                }
                return (float[])aziData.get1DJavaArray(Float.TYPE);
            }
            
            public float getAzimuth(final int ray) throws IOException {
                final String aziName = "Azimuth";
                Array aziData = null;
                if (aziData == null) {
                    try {
                        final Array aziTmp = Netcdf2Dataset.this.ds.findVariable(aziName).read();
                        if (Netcdf2Dataset.this.isVolume) {
                            final int[] aziOrigin = { this.sweepno, 0 };
                            final int[] aziShape = { 1, this.getRadialNumber() };
                            aziData = aziTmp.section(aziOrigin, aziShape);
                        }
                        else {
                            aziData = aziTmp;
                        }
                    }
                    catch (IOException e) {
                        e.printStackTrace();
                    }
                    catch (InvalidRangeException e2) {
                        e2.printStackTrace();
                    }
                }
                final Index index = aziData.getIndex();
                return aziData.getFloat(index.set(ray));
            }
            
            public float getRadialDistance(final int gate) throws IOException {
                final float gateStart = this.getRangeToFirstGate();
                final Variable gateSize = Netcdf2Dataset.this.ds.findVariable("GateWidth");
                final float[] data = (float[])gateSize.read().get1DJavaArray(Float.TYPE);
                final float dist = gateStart + gate * data[0];
                return dist;
            }
            
            public float getTime(final int ray) throws IOException {
                return (float)Netcdf2Dataset.this.startDate.getTime();
            }
            
            public float getBeamWidth() {
                return 0.95f;
            }
            
            public float getNyquistFrequency() {
                return 0.0f;
            }
            
            public float getRangeToFirstGate() {
                final Attribute firstGate = Netcdf2Dataset.this.ds.findGlobalAttributeIgnoreCase("RangeToFirstGate");
                final double gateStart = firstGate.getNumericValue().doubleValue();
                return (float)gateStart;
            }
            
            public float getGateSize() {
                try {
                    return this.getRadialDistance(1) - this.getRadialDistance(0);
                }
                catch (IOException e) {
                    e.printStackTrace();
                    return 0.0f;
                }
            }
            
            public boolean isGateSizeConstant() {
                return true;
            }
            
            public void clearSweepMemory() {
            }
        }
    }
}
