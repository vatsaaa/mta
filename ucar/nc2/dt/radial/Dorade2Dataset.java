// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.dt.radial;

import ucar.ma2.MAMath;
import ucar.ma2.InvalidRangeException;
import ucar.ma2.Array;
import java.util.ArrayList;
import ucar.nc2.dt.TypedDatasetFactory;
import java.util.Iterator;
import ucar.nc2.constants.AxisType;
import ucar.nc2.dataset.CoordinateAxis;
import java.util.Date;
import java.util.List;
import ucar.nc2.VariableSimpleIF;
import ucar.nc2.dt.RadialDatasetSweep;
import ucar.unidata.geoloc.EarthLocationImpl;
import ucar.unidata.geoloc.EarthLocation;
import ucar.nc2.constants.FeatureType;
import java.io.IOException;
import ucar.nc2.dt.TypedDataset;
import ucar.nc2.util.CancelTask;
import ucar.nc2.Variable;
import ucar.nc2.dataset.NetcdfDataset;
import ucar.nc2.units.DateUnit;
import ucar.nc2.dt.TypedDatasetFactoryIF;

public class Dorade2Dataset extends RadialDatasetSweepAdapter implements TypedDatasetFactoryIF
{
    protected DateUnit dateUnits;
    private NetcdfDataset ncd;
    float[] elev;
    float[] aziv;
    float[] disv;
    float[] lonv;
    float[] altv;
    float[] latv;
    double[] timv;
    float ranv;
    float cellv;
    float angv;
    float nyqv;
    float rangv;
    float contv;
    float rgainv;
    float bwidthv;
    
    public boolean isMine(final NetcdfDataset ds) {
        final String convention = ds.findAttValueIgnoreCase(null, "Conventions", null);
        if (null != convention && convention.equals("_Coordinates")) {
            final String format = ds.findAttValueIgnoreCase(null, "Format", null);
            if (format.equals("Unidata/netCDF/Dorade")) {
                return true;
            }
        }
        return false;
    }
    
    public TypedDataset open(final NetcdfDataset ncd, final CancelTask task, final StringBuilder errlog) throws IOException {
        return new Dorade2Dataset(ncd);
    }
    
    public FeatureType getScientificDataType() {
        return FeatureType.RADIAL;
    }
    
    public Dorade2Dataset() {
    }
    
    public Dorade2Dataset(final NetcdfDataset ds) {
        super(ds);
        this.ncd = ds;
        this.desc = "dorade radar dataset";
        try {
            this.elev = (float[])this.ncd.findVariable("elevation").read().get1DJavaArray(Float.TYPE);
            this.aziv = (float[])this.ncd.findVariable("azimuth").read().get1DJavaArray(Float.TYPE);
            this.altv = (float[])this.ncd.findVariable("altitudes_1").read().get1DJavaArray(Float.TYPE);
            this.lonv = (float[])this.ncd.findVariable("longitudes_1").read().get1DJavaArray(Float.TYPE);
            this.latv = (float[])this.ncd.findVariable("latitudes_1").read().get1DJavaArray(Float.TYPE);
            this.disv = (float[])this.ncd.findVariable("distance_1").read().get1DJavaArray(Float.TYPE);
            this.timv = (double[])this.ncd.findVariable("rays_time").read().get1DJavaArray(Double.TYPE);
            this.angv = this.ncd.findVariable("Fixed_Angle").readScalarFloat();
            this.nyqv = this.ncd.findVariable("Nyquist_Velocity").readScalarFloat();
            this.rangv = this.ncd.findVariable("Unambiguous_Range").readScalarFloat();
            this.contv = this.ncd.findVariable("Radar_Constant").readScalarFloat();
            this.rgainv = this.ncd.findVariable("rcvr_gain").readScalarFloat();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        this.setStartDate();
        this.setEndDate();
    }
    
    public String getRadarID() {
        return this.ncd.findGlobalAttribute("radar_name").getStringValue();
    }
    
    public String getRadarName() {
        return "Dorade Radar";
    }
    
    public String getDataFormat() {
        return "DORADE";
    }
    
    public EarthLocation getCommonOrigin() {
        if (this.isStationary()) {
            return new EarthLocationImpl(this.latv[0], this.lonv[0], this.elev[0]);
        }
        return null;
    }
    
    public boolean isStationary() {
        final String t = this.ncd.findGlobalAttribute("IsStationary").getStringValue();
        return t.equals("1");
    }
    
    public boolean isVolume() {
        return false;
    }
    
    @Override
    protected void setEarthLocation() {
        if (this.isStationary()) {
            this.origin = new EarthLocationImpl(this.latv[0], this.lonv[0], this.elev[0]);
        }
        this.origin = null;
    }
    
    @Override
    protected void addRadialVariable(final NetcdfDataset nds, final Variable var) {
        RadialDatasetSweep.RadialVariable rsvar = null;
        final String vName = var.getShortName();
        final int rnk = var.getRank();
        if (rnk == 2) {
            final VariableSimpleIF v = new MyRadialVariableAdapter(vName, var.getAttributes());
            rsvar = this.makeRadialVariable(nds, v, var);
        }
        if (rsvar != null) {
            this.dataVariables.add(rsvar);
        }
    }
    
    @Override
    protected RadialDatasetSweep.RadialVariable makeRadialVariable(final NetcdfDataset nds, final VariableSimpleIF v, final Variable v0) {
        return new Dorade2Variable(nds, v, v0);
    }
    
    @Override
    public List getDataVariables() {
        return this.dataVariables;
    }
    
    @Override
    protected void setStartDate() {
        final Date da = new Date((long)this.timv[0]);
        final String start_datetime = da.toString();
        if (start_datetime != null) {
            this.startDate = da;
        }
        else {
            this.parseInfo.append("*** start_datetime not Found\n");
        }
    }
    
    @Override
    protected void setEndDate() {
        final Date da = new Date((long)this.timv[this.timv.length - 1]);
        final String end_datetime = da.toString();
        if (end_datetime != null) {
            this.endDate = da;
        }
        else {
            this.parseInfo.append("*** end_datetime not Found\n");
        }
    }
    
    @Override
    protected void setTimeUnits() throws Exception {
        final List axes = this.ncd.getCoordinateAxes();
        for (int i = 0; i < axes.size(); ++i) {
            final CoordinateAxis axis = axes.get(i);
            if (axis.getAxisType() == AxisType.Time) {
                final String units = axis.getUnitsString();
                this.dateUnits = new DateUnit(units);
                return;
            }
        }
        this.parseInfo.append("*** Time Units not Found\n");
    }
    
    public List getAttributes() {
        return this.ncd.getRootGroup().getAttributes();
    }
    
    @Override
    public DateUnit getTimeUnits() {
        return this.dateUnits;
    }
    
    public void getTimeUnits(final DateUnit dateUnits) {
        this.dateUnits = dateUnits;
    }
    
    public void clearDatasetMemory() {
        final List rvars = this.getDataVariables();
        for (final RadialDatasetSweep.RadialVariable radVar : rvars) {
            radVar.clearVariableMemory();
        }
    }
    
    private static void testRadialVariable(final RadialDatasetSweep.RadialVariable rv) throws IOException {
        final int nsweep = rv.getNumSweeps();
        if (nsweep != 1) {}
        final RadialDatasetSweep.Sweep sw = rv.getSweep(0);
        final int nrays = sw.getRadialNumber();
        final float[] ddd = sw.readData();
        for (int i = 0; i < nrays; ++i) {
            final int ngates = sw.getGateNumber();
            final float[] d = sw.readData(i);
            final float azi = sw.getAzimuth(i);
            final float ele = sw.getElevation(i);
            final double t = sw.getTime(i);
            final Date da = new Date((long)t);
            final String start_datetime = da.toString();
            final float dis = sw.getRangeToFirstGate();
            final float beamW = sw.getBeamWidth();
            final float gsize = sw.getGateSize();
            final float la = (float)sw.getOrigin(i).getLatitude();
            final float lo = (float)sw.getOrigin(i).getLongitude();
            final float al = (float)sw.getOrigin(i).getAltitude();
        }
    }
    
    public static void main(final String[] args) throws Exception, IOException, InstantiationException, IllegalAccessException {
        final String fileIn = "/home/yuanho/dorade/swp.1020511015815.SP0L.573.1.2_SUR_v1";
        final RadialDatasetSweep rds = (RadialDatasetSweep)TypedDatasetFactory.open(FeatureType.RADIAL, fileIn, null, new StringBuilder());
        final String st = rds.getStartDate().toString();
        final String et = rds.getEndDate().toString();
        if (rds.isStationary()) {
            System.out.println("*** radar is stationary\n");
        }
        final List rvars = rds.getDataVariables();
        final RadialDatasetSweep.RadialVariable vDM = (RadialDatasetSweep.RadialVariable)rds.getDataVariable("DM");
        testRadialVariable(vDM);
        for (int i = 0; i < rvars.size(); ++i) {
            final RadialDatasetSweep.RadialVariable rv = rvars.get(i);
            testRadialVariable(rv);
        }
    }
    
    private class Dorade2Variable extends MyRadialVariableAdapter implements RadialDatasetSweep.RadialVariable
    {
        ArrayList sweeps;
        int nsweeps;
        String name;
        float ele;
        float azi;
        float alt;
        float lon;
        float lat;
        
        public int getNumSweeps() {
            return 1;
        }
        
        public RadialDatasetSweep.Sweep getSweep(final int nsw) {
            return this.sweeps.get(nsw);
        }
        
        private Dorade2Variable(final NetcdfDataset nds, final VariableSimpleIF v, final Variable v0) {
            super(v.getName(), v0.getAttributes());
            this.sweeps = new ArrayList();
            this.nsweeps = 0;
            this.name = v.getName();
            final int[] shape = v0.getShape();
            int count = v0.getRank() - 1;
            final int ngates = shape[count];
            --count;
            final int nrays = shape[count];
            this.sweeps.add(new Dorade2Sweep(v0, 0, nrays, ngates));
        }
        
        @Override
        public String toString() {
            return this.name;
        }
        
        public float[] readAllData() throws IOException {
            final RadialDatasetSweep.Sweep spn = this.sweeps.get(0);
            final Variable v = spn.getsweepVar();
            Array allData;
            try {
                allData = v.read();
            }
            catch (IOException e) {
                throw new IOException(e.getMessage());
            }
            return (float[])allData.get1DJavaArray(Float.TYPE);
        }
        
        public void clearVariableMemory() {
        }
        
        private class Dorade2Sweep implements RadialDatasetSweep.Sweep
        {
            int nrays;
            int ngates;
            double meanElevation;
            Variable sweepVar;
            int sweepno;
            
            Dorade2Sweep(final Variable v, final int sweepno, final int rays, final int gates) {
                this.meanElevation = Double.NaN;
                this.sweepVar = v;
                this.nrays = rays;
                this.ngates = gates;
                this.sweepno = sweepno;
            }
            
            public Variable getsweepVar() {
                return this.sweepVar;
            }
            
            public RadialDatasetSweep.Type getType() {
                return null;
            }
            
            public float getLon(final int ray) {
                return Dorade2Dataset.this.lonv[ray];
            }
            
            public int getGateNumber() {
                return this.ngates;
            }
            
            public int getRadialNumber() {
                return this.nrays;
            }
            
            public int getSweepIndex() {
                return 0;
            }
            
            public float[] readData() throws IOException {
                return Dorade2Variable.this.readAllData();
            }
            
            public float[] readData(final int ray) throws IOException {
                final int[] shape = this.sweepVar.getShape();
                final int[] origi = new int[this.sweepVar.getRank()];
                shape[0] = 1;
                origi[0] = ray;
                Array rayData;
                try {
                    rayData = this.sweepVar.read(origi, shape);
                }
                catch (InvalidRangeException e) {
                    throw new IOException(e.getMessage());
                }
                return (float[])rayData.get1DJavaArray(Float.TYPE);
            }
            
            public float getBeamWidth() {
                try {
                    Dorade2Dataset.this.bwidthv = Dorade2Dataset.this.ncd.findVariable("bm_width").readScalarFloat();
                }
                catch (IOException e) {
                    e.printStackTrace();
                }
                return Dorade2Dataset.this.bwidthv;
            }
            
            public float getNyquistFrequency() {
                return 0.0f;
            }
            
            public float getRangeToFirstGate() {
                try {
                    Dorade2Dataset.this.ranv = Dorade2Dataset.this.ncd.findVariable("Range_to_First_Cell").readScalarFloat();
                }
                catch (IOException e) {
                    e.printStackTrace();
                }
                return Dorade2Dataset.this.ranv;
            }
            
            public float getGateSize() {
                try {
                    Dorade2Dataset.this.cellv = Dorade2Dataset.this.ncd.findVariable("Cell_Spacing").readScalarFloat();
                }
                catch (IOException e) {
                    e.printStackTrace();
                }
                return Dorade2Dataset.this.cellv;
            }
            
            public float getMeanElevation() {
                final int[] shapeRadial = { this.nrays };
                final Array data = Array.factory(Float.class, shapeRadial, Dorade2Dataset.this.elev);
                this.meanElevation = MAMath.sumDouble(data) / data.getSize();
                return (float)this.meanElevation;
            }
            
            public float getElevation(final int ray) {
                return Dorade2Dataset.this.elev[ray];
            }
            
            public float[] getElevation() {
                return Dorade2Dataset.this.elev;
            }
            
            public float getAzimuth(final int ray) {
                return Dorade2Dataset.this.aziv[ray];
            }
            
            public float[] getAzimuth() {
                return Dorade2Dataset.this.aziv;
            }
            
            public float getTime() {
                return (float)Dorade2Dataset.this.timv[0];
            }
            
            public float getTime(final int ray) {
                return (float)Dorade2Dataset.this.timv[ray];
            }
            
            public EarthLocation getOrigin(final int ray) {
                return new EarthLocationImpl(Dorade2Dataset.this.latv[ray], Dorade2Dataset.this.lonv[ray], Dorade2Dataset.this.altv[ray]);
            }
            
            public float getMeanAzimuth() {
                if (this.getType() != null) {
                    return Dorade2Variable.this.azi;
                }
                return 0.0f;
            }
            
            public Date getStartingTime() {
                return Dorade2Dataset.this.startDate;
            }
            
            public Date getEndingTime() {
                return Dorade2Dataset.this.endDate;
            }
            
            public void clearSweepMemory() {
            }
        }
    }
}
