// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.dt.radial;

import ucar.unidata.geoloc.LatLonPoint;
import ucar.unidata.geoloc.LatLonPointImpl;
import ucar.unidata.geoloc.Earth;
import ucar.nc2.units.SimpleUnit;
import ucar.ma2.MAMath;
import java.io.IOException;
import java.util.Comparator;
import java.util.List;
import java.util.Collections;
import ucar.nc2.VariableIF;
import ucar.nc2.dataset.VariableEnhanced;
import ucar.nc2.dataset.CoordinateSystem;
import java.util.Formatter;
import ucar.unidata.geoloc.LatLonRect;
import ucar.unidata.geoloc.EarthLocation;
import ucar.nc2.units.DateUnit;
import java.util.ArrayList;
import ucar.ma2.Array;
import ucar.nc2.dataset.CoordinateAxis;

public class RadialCoordSys
{
    private CoordinateAxis aziAxis;
    private CoordinateAxis elevAxis;
    private CoordinateAxis radialAxis;
    private CoordinateAxis timeAxis;
    private Array aziData;
    private Array elevData;
    private Array radialData;
    private Array timeData;
    private String name;
    private ArrayList coordAxes;
    private DateUnit dateUnit;
    private EarthLocation origin;
    private LatLonRect bb;
    private double maxRadial;
    
    public static boolean isRadialCoordSys(final Formatter parseInfo, final CoordinateSystem cs) {
        return cs.getAzimuthAxis() != null && cs.getRadialAxis() != null && cs.getElevationAxis() != null;
    }
    
    public static RadialCoordSys makeRadialCoordSys(final Formatter parseInfo, final CoordinateSystem cs, final VariableEnhanced v) {
        if (parseInfo != null) {
            parseInfo.format(" ", new Object[0]);
            v.getNameAndDimensions(parseInfo, true, false);
            parseInfo.format(" check CS " + cs.getName(), new Object[0]);
        }
        if (isRadialCoordSys(parseInfo, cs)) {
            final RadialCoordSys rcs = new RadialCoordSys(cs);
            if (cs.isComplete(v)) {
                if (parseInfo != null) {
                    parseInfo.format(" OK\n", new Object[0]);
                }
                return rcs;
            }
            if (parseInfo != null) {
                parseInfo.format(" NOT complete\n", new Object[0]);
            }
        }
        return null;
    }
    
    public RadialCoordSys(final CoordinateSystem cs) {
        this.coordAxes = new ArrayList();
        this.aziAxis = cs.getAzimuthAxis();
        this.radialAxis = cs.getRadialAxis();
        this.elevAxis = cs.getElevationAxis();
        this.timeAxis = cs.getTaxis();
        this.coordAxes.add(this.aziAxis);
        this.coordAxes.add(this.radialAxis);
        this.coordAxes.add(this.elevAxis);
        Collections.sort((List<Object>)this.coordAxes, (Comparator<? super Object>)new CoordinateAxis.AxisComparator());
        this.name = CoordinateSystem.makeName(this.coordAxes);
    }
    
    public String getName() {
        return this.name;
    }
    
    public List getCoordAxes() {
        return this.coordAxes;
    }
    
    public CoordinateAxis getAzimuthAxis() {
        return this.aziAxis;
    }
    
    public CoordinateAxis getElevationAxis() {
        return this.elevAxis;
    }
    
    public CoordinateAxis getRadialAxis() {
        return this.radialAxis;
    }
    
    public CoordinateAxis getTimeAxis() {
        return this.timeAxis;
    }
    
    public Array getAzimuthAxisDataCached() throws IOException {
        if (this.aziData == null) {
            this.aziData = this.aziAxis.read();
        }
        return this.aziData;
    }
    
    public Array getElevationAxisDataCached() throws IOException {
        if (this.elevData == null) {
            this.elevData = this.elevAxis.read();
        }
        return this.elevData;
    }
    
    public Array getRadialAxisDataCached() throws IOException {
        if (this.radialData == null) {
            this.radialData = this.radialAxis.read();
        }
        return this.radialData;
    }
    
    public Array getTimeAxisDataCached() throws IOException {
        if (this.timeData == null) {
            this.timeData = this.timeAxis.read();
        }
        return this.timeData;
    }
    
    public EarthLocation getOrigin() {
        return this.origin;
    }
    
    public void setOrigin(final EarthLocation origin) {
        this.origin = origin;
    }
    
    public double getMaximumRadial() {
        if (this.maxRadial == 0.0) {
            try {
                final Array radialData = this.getRadialAxisDataCached();
                this.maxRadial = MAMath.getMaximum(radialData);
                final String units = this.getRadialAxis().getUnitsString();
                final SimpleUnit radialUnit = SimpleUnit.factory(units);
                this.maxRadial = radialUnit.convertTo(this.maxRadial, SimpleUnit.kmUnit);
            }
            catch (IOException e) {
                e.printStackTrace();
            }
            catch (IllegalArgumentException e2) {
                e2.printStackTrace();
            }
        }
        return this.maxRadial;
    }
    
    public LatLonRect getBoundingBox() {
        if (this.bb != null) {
            return this.bb;
        }
        if (this.origin == null) {
            return null;
        }
        final double dLat = Math.toDegrees(this.getMaximumRadial() / Earth.getRadius());
        final double latRadians = Math.toRadians(this.origin.getLatitude());
        final double dLon = dLat * Math.cos(latRadians);
        final double lat1 = this.origin.getLatitude() - dLat / 2.0;
        final double lon1 = this.origin.getLongitude() - dLon / 2.0;
        return this.bb = new LatLonRect(new LatLonPointImpl(lat1, lon1), dLat, dLon);
    }
    
    public DateUnit getTimeUnits() throws Exception {
        if (null == this.dateUnit) {
            this.dateUnit = new DateUnit(this.timeAxis.getUnitsString());
        }
        return this.dateUnit;
    }
    
    public static void main(final String[] args) {
        System.out.println("1 Deg=" + Math.toDegrees(111000.0 / Earth.getRadius()));
    }
}
