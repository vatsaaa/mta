// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.dt.radial;

import ucar.ma2.Index;
import ucar.ma2.MAMath;
import ucar.ma2.IndexIterator;
import ucar.ma2.Array;
import ucar.ma2.InvalidRangeException;
import ucar.ma2.Section;
import java.util.ArrayList;
import java.util.Date;
import ucar.nc2.dt.TypedDatasetFactory;
import ucar.nc2.VariableSimpleIF;
import java.util.List;
import ucar.nc2.units.DateUnit;
import ucar.nc2.constants.AxisType;
import ucar.nc2.dataset.CoordinateAxis;
import ucar.nc2.Dimension;
import ucar.unidata.geoloc.EarthLocation;
import ucar.nc2.Attribute;
import ucar.unidata.geoloc.EarthLocationImpl;
import java.util.Iterator;
import ucar.nc2.dt.RadialDatasetSweep;
import ucar.unidata.geoloc.LatLonPoint;
import ucar.unidata.geoloc.LatLonRect;
import ucar.unidata.geoloc.LatLonPointImpl;
import ucar.unidata.geoloc.Earth;
import ucar.nc2.constants.FeatureType;
import java.io.IOException;
import ucar.nc2.dt.TypedDataset;
import ucar.nc2.util.CancelTask;
import ucar.nc2.Variable;
import ucar.nc2.units.DateFormatter;
import ucar.nc2.dataset.NetcdfDataset;
import ucar.nc2.dt.TypedDatasetFactoryIF;

public class LevelII2Dataset extends RadialDatasetSweepAdapter implements TypedDatasetFactoryIF
{
    private NetcdfDataset ds;
    double latv;
    double lonv;
    double elev;
    DateFormatter formatter;
    
    public boolean isMine(final NetcdfDataset ds) {
        final String convention = ds.findAttValueIgnoreCase(null, "Conventions", null);
        if (null != convention && convention.equals("_Coordinates")) {
            final String format = ds.findAttValueIgnoreCase(null, "Format", null);
            if (format.equals("ARCHIVE2") || format.equals("AR2V0001") || format.equals("CINRAD-SA") || format.equals("AR2V0003") || format.equals("AR2V0002") || format.equals("AR2V0004") || format.equals("AR2V0006")) {
                return true;
            }
        }
        return false;
    }
    
    public TypedDataset open(final NetcdfDataset ncd, final CancelTask task, final StringBuilder errlog) throws IOException {
        return new LevelII2Dataset(ncd);
    }
    
    public FeatureType getScientificDataType() {
        return FeatureType.RADIAL;
    }
    
    public LevelII2Dataset() {
        this.formatter = new DateFormatter();
    }
    
    public LevelII2Dataset(final NetcdfDataset ds) {
        super(ds);
        this.formatter = new DateFormatter();
        this.ds = ds;
        this.desc = "Nexrad 2 radar dataset";
        this.setEarthLocation();
        try {
            this.setTimeUnits();
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }
        this.setStartDate();
        this.setEndDate();
        this.setBoundingBox();
    }
    
    @Override
    protected void setBoundingBox() {
        if (this.origin == null) {
            return;
        }
        final double dLat = Math.toDegrees(this.getMaximumRadialDist() / Earth.getRadius());
        final double latRadians = Math.toRadians(this.origin.getLatitude());
        final double dLon = dLat * Math.cos(latRadians);
        final double lat1 = this.origin.getLatitude() - dLat / 2.0;
        final double lon1 = this.origin.getLongitude() - dLon / 2.0;
        final LatLonRect bb = new LatLonRect(new LatLonPointImpl(lat1, lon1), dLat, dLon);
        this.boundingBox = bb;
    }
    
    double getMaximumRadialDist() {
        double maxdist = 0.0;
        for (final RadialDatasetSweep.RadialVariable rv : this.dataVariables) {
            final RadialDatasetSweep.Sweep sp = rv.getSweep(0);
            final double dist = sp.getGateNumber() * sp.getGateSize();
            if (dist > maxdist) {
                maxdist = dist;
            }
        }
        return maxdist;
    }
    
    @Override
    protected void setEarthLocation() {
        Attribute ga = this.ds.findGlobalAttribute("StationLatitude");
        if (ga != null) {
            this.latv = ga.getNumericValue().doubleValue();
        }
        else {
            this.latv = 0.0;
        }
        ga = this.ds.findGlobalAttribute("StationLongitude");
        if (ga != null) {
            this.lonv = ga.getNumericValue().doubleValue();
        }
        else {
            this.lonv = 0.0;
        }
        ga = this.ds.findGlobalAttribute("StationElevationInMeters");
        if (ga != null) {
            this.elev = ga.getNumericValue().doubleValue();
        }
        else {
            this.elev = 0.0;
        }
        this.origin = new EarthLocationImpl(this.latv, this.lonv, this.elev);
    }
    
    public EarthLocation getCommonOrigin() {
        return this.origin;
    }
    
    public String getRadarID() {
        final Attribute ga = this.ds.findGlobalAttribute("Station");
        if (ga != null) {
            return ga.getStringValue();
        }
        return "XXXX";
    }
    
    public String getRadarName() {
        final Attribute ga = this.ds.findGlobalAttribute("StationName");
        if (ga != null) {
            return ga.getStringValue();
        }
        return "Unknown Station";
    }
    
    public String getDataFormat() {
        return "Level II";
    }
    
    public boolean isVolume() {
        return true;
    }
    
    public boolean isHighResolution(final NetcdfDataset nds) {
        final Dimension r = nds.findDimension("scanR_HI");
        final Dimension v = nds.findDimension("scanV_HI");
        return r != null || v != null;
    }
    
    public boolean isStationary() {
        return true;
    }
    
    @Override
    protected void setTimeUnits() throws Exception {
        final List axes = this.ds.getCoordinateAxes();
        for (int i = 0; i < axes.size(); ++i) {
            final CoordinateAxis axis = axes.get(i);
            if (axis.getAxisType() == AxisType.Time) {
                final String units = axis.getUnitsString();
                this.dateUnits = new DateUnit(units);
                return;
            }
        }
        this.parseInfo.append("*** Time Units not Found\n");
    }
    
    @Override
    protected void setStartDate() {
        final String start_datetime = this.ds.findAttValueIgnoreCase(null, "time_coverage_start", null);
        if (start_datetime != null) {
            this.startDate = this.formatter.getISODate(start_datetime);
        }
        else {
            this.parseInfo.append("*** start_datetime not Found\n");
        }
    }
    
    @Override
    protected void setEndDate() {
        final String end_datetime = this.ds.findAttValueIgnoreCase(null, "time_coverage_end", null);
        if (end_datetime != null) {
            this.endDate = this.formatter.getISODate(end_datetime);
        }
        else {
            this.parseInfo.append("*** end_datetime not Found\n");
        }
    }
    
    public void clearDatasetMemory() {
        final List rvars = this.getDataVariables();
        for (final RadialDatasetSweep.RadialVariable radVar : rvars) {
            radVar.clearVariableMemory();
        }
    }
    
    public void getRadialsNum() {
        final Variable rayVars = this.ds.findVariable("numRadialsV");
    }
    
    @Override
    protected void addRadialVariable(final NetcdfDataset nds, final Variable var) {
        RadialDatasetSweep.RadialVariable rsvar = null;
        final String vName = var.getShortName();
        final int rnk = var.getRank();
        if (rnk == 3) {
            if (!this.isHighResolution(nds)) {
                final VariableSimpleIF v = new MyRadialVariableAdapter(vName, var.getAttributes());
                rsvar = this.makeRadialVariable(nds, v, var);
            }
            else if (!vName.endsWith("_HI")) {
                final VariableSimpleIF v = new MyRadialVariableAdapter(vName, var.getAttributes());
                rsvar = this.makeRadialVariable(nds, v, var);
            }
        }
        if (rsvar != null) {
            this.dataVariables.add(rsvar);
        }
    }
    
    @Override
    protected RadialDatasetSweep.RadialVariable makeRadialVariable(final NetcdfDataset nds, final VariableSimpleIF v, final Variable v0) {
        return new LevelII2Variable(nds, v, v0);
    }
    
    public String getInfo() {
        final StringBuffer sbuff = new StringBuffer();
        sbuff.append("LevelII2Dataset\n");
        sbuff.append(super.getDetailInfo());
        sbuff.append("\n\n");
        sbuff.append(this.parseInfo.toString());
        return sbuff.toString();
    }
    
    private static void testRadialVariable(final RadialDatasetSweep.RadialVariable rv) throws IOException {
        for (int nsweep = rv.getNumSweeps(), i = 0; i < nsweep; ++i) {
            final RadialDatasetSweep.Sweep sw = rv.getSweep(i);
            final float me = sw.getMeanElevation();
            System.out.println("*** radar Sweep mean elevation of sweep " + i + " is: " + me);
            final int nrays = sw.getRadialNumber();
            final float[] az = new float[nrays];
            for (int j = 0; j < nrays; ++j) {
                final float azi = sw.getAzimuth(j);
                az[j] = azi;
            }
            final float[] azz = sw.getAzimuth();
            final float[] dat = sw.readData();
        }
        final RadialDatasetSweep.Sweep sw = rv.getSweep(0);
        final float[] data = rv.readAllData();
        final float[] ddd = sw.readData();
        final float[] da = sw.getAzimuth();
        final float[] de = sw.getElevation();
        assert null != ddd;
        final int nrays2 = sw.getRadialNumber();
        final float[] az2 = new float[nrays2];
        for (int k = 0; k < nrays2; ++k) {
            final int ngates = sw.getGateNumber();
            assert ngates > 0;
            final float[] d = sw.readData(k);
            assert null != d;
            final float azi2 = sw.getAzimuth(k);
            assert azi2 > 0.0f;
            az2[k] = azi2;
            final float ele = sw.getElevation(k);
            assert ele > 0.0f;
            final float la = (float)sw.getOrigin(k).getLatitude();
            assert la > 0.0f;
            final float lo = (float)sw.getOrigin(k).getLongitude();
            assert lo > 0.0f;
            final float al = (float)sw.getOrigin(k).getAltitude();
            assert al > 0.0f;
        }
        assert 0 != nrays2;
    }
    
    public static void main(final String[] args) throws Exception, IOException, InstantiationException, IllegalAccessException {
        final String fileIn = "C:/Users/yuanho/Downloads/RADAR/KOUN20100405042306V06.raw.uncompress";
        final RadialDatasetSweep rds = (RadialDatasetSweep)TypedDatasetFactory.open(FeatureType.RADIAL, fileIn, null, new StringBuilder());
        final String st = rds.getStartDate().toString();
        final String et = rds.getEndDate().toString();
        final String id = rds.getRadarID();
        final String name = rds.getRadarName();
        if (rds.isStationary()) {
            System.out.println("*** radar is stationary with name and id: " + name + " " + id);
        }
        final List rvars = rds.getDataVariables();
        final RadialDatasetSweep.RadialVariable vDM = (RadialDatasetSweep.RadialVariable)rds.getDataVariable("Reflectivity");
        final float[] adata = vDM.readAllData();
        testRadialVariable(vDM);
        for (int i = 0; i < rvars.size(); ++i) {
            final RadialDatasetSweep.RadialVariable rv = rvars.get(i);
            testRadialVariable(rv);
        }
    }
    
    private class LevelII2Variable extends MyRadialVariableAdapter implements RadialDatasetSweep.RadialVariable
    {
        int nsweeps;
        int nsweepsHR;
        ArrayList sweeps;
        String name;
        
        private LevelII2Variable(final NetcdfDataset nds, final VariableSimpleIF v, final Variable v0) {
            super(v.getName(), v0.getAttributes());
            this.nsweepsHR = 0;
            this.sweeps = new ArrayList();
            this.name = v.getName();
            if (LevelII2Dataset.this.isHighResolution(nds)) {
                final String vname = v0.getName();
                final Variable vehr = nds.findVariable(vname + "_HI");
                if (vehr != null) {
                    final int[] shape1 = vehr.getShape();
                    int count1 = vehr.getRank() - 1;
                    final int ngatesHR = shape1[count1];
                    --count1;
                    final int nraysHR = shape1[count1];
                    --count1;
                    this.nsweepsHR = shape1[count1];
                    for (int i = 0; i < this.nsweepsHR; ++i) {
                        this.sweeps.add(new LevelII2Sweep(vehr, i, nraysHR, ngatesHR));
                    }
                }
            }
            final int[] shape2 = v0.getShape();
            int count2 = v0.getRank() - 1;
            final int ngates = shape2[count2];
            --count2;
            final int nrays = shape2[count2];
            --count2;
            this.nsweeps = shape2[count2];
            for (int j = this.nsweepsHR; j < this.nsweeps + this.nsweepsHR; ++j) {
                this.sweeps.add(new LevelII2Sweep(v0, j, nrays, ngates));
            }
        }
        
        @Override
        public String toString() {
            return this.name;
        }
        
        public int getNumSweeps() {
            if (LevelII2Dataset.this.isHighResolution(LevelII2Dataset.this.ds)) {
                return this.nsweepsHR + this.nsweeps;
            }
            return this.nsweeps;
        }
        
        public RadialDatasetSweep.Sweep getSweep(final int sweepNo) {
            return this.sweeps.get(sweepNo);
        }
        
        public int getNumRadials() {
            return 0;
        }
        
        public float[] readAllData() throws IOException {
            Array hrData = null;
            final RadialDatasetSweep.Sweep spn = this.sweeps.get(this.sweeps.size() - 1);
            final Variable v = spn.getsweepVar();
            Array allData;
            try {
                allData = v.read();
            }
            catch (IOException e) {
                throw new IOException(e.getMessage());
            }
            if (!LevelII2Dataset.this.isHighResolution(LevelII2Dataset.this.ds)) {
                return (float[])allData.get1DJavaArray(Float.TYPE);
            }
            final RadialDatasetSweep.Sweep sp0 = this.sweeps.get(0);
            final Variable v2 = sp0.getsweepVar();
            int[] stride;
            if (v2.getName().startsWith("Reflect")) {
                stride = new int[] { 1, 2, 4 };
            }
            else {
                stride = new int[] { 1, 2, 1 };
            }
            final int[] shape1 = v.getShape();
            final int[] shape2 = v2.getShape();
            final int shp1 = (shape1[1] * stride[1] > shape2[1]) ? shape2[1] : (shape1[1] * stride[1]);
            if (shape2[2] == shape1[2]) {
                stride = new int[] { 1, 2, 1 };
            }
            final int shp2 = (shape1[2] * stride[2] > shape2[2]) ? shape2[2] : (shape1[2] * stride[2]);
            final int[] shape3 = { shape2[0], shp1, shp2 };
            final int[] origin = { 0, 0, 0 };
            Section section = null;
            try {
                section = new Section(origin, shape3, stride);
            }
            catch (InvalidRangeException e2) {
                e2.printStackTrace();
            }
            try {
                hrData = v2.read(section);
            }
            catch (InvalidRangeException e2) {
                e2.printStackTrace();
            }
            final float[] fa1 = (float[])hrData.get1DJavaArray(Float.TYPE);
            final float[] fa2 = (float[])allData.get1DJavaArray(Float.TYPE);
            final float[] fa3 = new float[fa1.length + fa2.length];
            System.arraycopy(fa1, 0, fa3, 0, fa1.length);
            System.arraycopy(fa2, 0, fa3, fa1.length, fa2.length);
            return fa3;
        }
        
        public void clearVariableMemory() {
            for (int i = 0; i < this.nsweeps; ++i) {}
        }
        
        private class LevelII2Sweep implements RadialDatasetSweep.Sweep
        {
            double meanElevation;
            double meanAzimuth;
            int nrays;
            int ngates;
            int sweepno;
            Variable sweepVar;
            
            LevelII2Sweep(final Variable v, final int sweepno, final int rays, final int gates) {
                this.meanElevation = Double.NaN;
                this.meanAzimuth = Double.NaN;
                this.sweepVar = v;
                this.sweepno = sweepno;
                this.nrays = rays;
                this.ngates = gates;
            }
            
            public Variable getsweepVar() {
                return this.sweepVar;
            }
            
            public float[] readData() throws IOException {
                if (!LevelII2Dataset.this.isHighResolution(LevelII2Dataset.this.ds)) {
                    return this.sweepData(this.sweepno);
                }
                if (this.sweepno > LevelII2Variable.this.nsweepsHR - 1) {
                    final int swpNo = this.sweepno - LevelII2Variable.this.nsweepsHR;
                    return this.sweepData(swpNo);
                }
                return this.sweepData(this.sweepno);
            }
            
            private float[] sweepData(final int swpNumber) {
                final int[] shape = this.sweepVar.getShape();
                final int[] origin = new int[3];
                Array sweepTmp = null;
                origin[0] = swpNumber;
                shape[0] = 1;
                try {
                    sweepTmp = this.sweepVar.read(origin, shape).reduce();
                }
                catch (InvalidRangeException e) {
                    e.printStackTrace();
                }
                catch (IOException e2) {
                    e2.printStackTrace();
                }
                return (float[])sweepTmp.get1DJavaArray(Float.TYPE);
            }
            
            public float[] readData(final int ray) throws IOException {
                if (!LevelII2Dataset.this.isHighResolution(LevelII2Dataset.this.ds)) {
                    return this.rayData(this.sweepno, ray);
                }
                if (this.sweepno >= LevelII2Variable.this.nsweepsHR) {
                    final int swpNo = this.sweepno - LevelII2Variable.this.nsweepsHR;
                    return this.rayData(swpNo, ray);
                }
                return this.rayData(this.sweepno, ray);
            }
            
            public float[] rayData(final int swpNumber, final int ray) throws IOException {
                final int[] shape = this.sweepVar.getShape();
                final int[] origin = new int[3];
                Array sweepTmp = null;
                origin[0] = swpNumber;
                origin[1] = ray;
                shape[shape[0] = 1] = 1;
                try {
                    sweepTmp = this.sweepVar.read(origin, shape).reduce();
                }
                catch (InvalidRangeException e) {
                    e.printStackTrace();
                }
                catch (IOException e2) {
                    e2.printStackTrace();
                }
                return (float[])sweepTmp.get1DJavaArray(Float.TYPE);
            }
            
            public void setMeanElevation() {
                String eleName;
                if (this.sweepVar.getName().startsWith("Reflectivity")) {
                    eleName = "elevationR";
                }
                else {
                    eleName = "elevationV";
                }
                if (!LevelII2Dataset.this.isHighResolution(LevelII2Dataset.this.ds)) {
                    this.setMeanEle(eleName, this.sweepno);
                }
                else if (this.sweepno >= LevelII2Variable.this.nsweepsHR) {
                    final int swpNo = this.sweepno - LevelII2Variable.this.nsweepsHR;
                    this.setMeanEle(eleName, swpNo);
                }
                else {
                    this.setMeanEle(eleName + "_HI", this.sweepno);
                }
            }
            
            private void setMeanEle(final String elevName, final int swpNumber) {
                Array eleData = null;
                float sum = 0.0f;
                int sumSize = 0;
                try {
                    final Variable evar = LevelII2Dataset.this.ds.findVariable(elevName);
                    final Array eleTmp = evar.read();
                    evar.setCachedData(eleTmp, false);
                    final int[] eleOrigin = { swpNumber, 0 };
                    final int[] eleShape = { 1, this.getRadialNumber() };
                    eleData = eleTmp.section(eleOrigin, eleShape);
                }
                catch (IOException e) {
                    e.printStackTrace();
                }
                catch (InvalidRangeException e2) {
                    e2.printStackTrace();
                }
                final float[] eleArray = (float[])eleData.get1DJavaArray(Float.TYPE);
                for (int size = (int)eleData.getSize(), i = 0; i < size; ++i) {
                    if (!Float.isNaN(eleArray[i])) {
                        sum += eleArray[i];
                        ++sumSize;
                    }
                }
                this.meanElevation = sum / sumSize;
            }
            
            public float getMeanElevation() {
                if (Double.isNaN(this.meanElevation)) {
                    this.setMeanElevation();
                }
                return (float)this.meanElevation;
            }
            
            public double meanDouble(final Array a) {
                double sum = 0.0;
                int size = 0;
                final IndexIterator iterA = a.getIndexIterator();
                while (iterA.hasNext()) {
                    final double s = iterA.getDoubleNext();
                    if (!Double.isNaN(s)) {
                        sum += s;
                        ++size;
                    }
                }
                return sum / size;
            }
            
            public int getGateNumber() {
                return this.ngates;
            }
            
            public int getRadialNumber() {
                return this.nrays;
            }
            
            public RadialDatasetSweep.Type getType() {
                return null;
            }
            
            public EarthLocation getOrigin(final int ray) {
                return LevelII2Dataset.this.origin;
            }
            
            public Date getStartingTime() {
                return LevelII2Dataset.this.startDate;
            }
            
            public Date getEndingTime() {
                return LevelII2Dataset.this.endDate;
            }
            
            public int getSweepIndex() {
                return this.sweepno;
            }
            
            public void setMeanAzimuth() {
                String aziName;
                if (this.sweepVar.getName().startsWith("Reflectivity")) {
                    aziName = "azimuthR";
                }
                else {
                    aziName = "azimuthV";
                }
                if (!LevelII2Dataset.this.isHighResolution(LevelII2Dataset.this.ds)) {
                    this.setMeanAzi(aziName, this.sweepno);
                }
                else if (this.sweepno >= LevelII2Variable.this.nsweepsHR) {
                    final int swpNo = this.sweepno - LevelII2Variable.this.nsweepsHR;
                    this.setMeanAzi(aziName, swpNo);
                }
                else {
                    this.setMeanAzi(aziName + "_HI", this.sweepno);
                }
            }
            
            private void setMeanAzi(final String aziName, final int swpNumber) {
                Array aziData = null;
                if (this.getType() != null) {
                    try {
                        final Array data = LevelII2Dataset.this.ds.findVariable(aziName).read();
                        final int[] aziOrigin = { swpNumber, 0 };
                        final int[] aziShape = { 1, this.getRadialNumber() };
                        aziData = data.section(aziOrigin, aziShape);
                        this.meanAzimuth = MAMath.sumDouble(aziData) / aziData.getSize();
                    }
                    catch (IOException e) {
                        e.printStackTrace();
                        this.meanAzimuth = 0.0;
                    }
                    catch (InvalidRangeException e2) {
                        e2.printStackTrace();
                    }
                }
                else {
                    this.meanAzimuth = 0.0;
                }
            }
            
            public float getMeanAzimuth() {
                if (Double.isNaN(this.meanAzimuth)) {
                    this.setMeanAzimuth();
                }
                return (float)this.meanAzimuth;
            }
            
            public boolean isConic() {
                return true;
            }
            
            public float getElevation(final int ray) throws IOException {
                String eleName;
                if (this.sweepVar.getName().startsWith("Reflectivity")) {
                    eleName = "elevationR";
                }
                else {
                    eleName = "elevationV";
                }
                if (!LevelII2Dataset.this.isHighResolution(LevelII2Dataset.this.ds)) {
                    return this.getEle(eleName, this.sweepno, ray);
                }
                if (this.sweepno >= LevelII2Variable.this.nsweepsHR) {
                    final int swpNo = this.sweepno - LevelII2Variable.this.nsweepsHR;
                    return this.getEle(eleName, swpNo, ray);
                }
                return this.getEle(eleName + "_HI", this.sweepno, ray);
            }
            
            public float getEle(final String elevName, final int swpNumber, final int ray) throws IOException {
                Array eleData = null;
                try {
                    final Variable evar = LevelII2Dataset.this.ds.findVariable(elevName);
                    final Array eleTmp = evar.read();
                    evar.setCachedData(eleTmp, false);
                    final int[] eleOrigin = { swpNumber, 0 };
                    final int[] eleShape = { 1, this.getRadialNumber() };
                    eleData = eleTmp.section(eleOrigin, eleShape);
                }
                catch (IOException e) {
                    e.printStackTrace();
                }
                catch (InvalidRangeException e2) {
                    e2.printStackTrace();
                }
                final Index index = eleData.getIndex();
                return eleData.getFloat(index.set(ray));
            }
            
            public float[] getElevation() throws IOException {
                String eleName;
                if (this.sweepVar.getName().startsWith("Reflectivity")) {
                    eleName = "elevationR";
                }
                else {
                    eleName = "elevationV";
                }
                if (!LevelII2Dataset.this.isHighResolution(LevelII2Dataset.this.ds)) {
                    return this.getEle(eleName, this.sweepno);
                }
                if (this.sweepno >= LevelII2Variable.this.nsweepsHR) {
                    final int swpNo = this.sweepno - LevelII2Variable.this.nsweepsHR;
                    return this.getEle(eleName, swpNo);
                }
                return this.getEle(eleName + "_HI", this.sweepno);
            }
            
            public float[] getEle(final String elevName, final int swpNumber) throws IOException {
                Array eleData = null;
                if (eleData == null) {
                    try {
                        final Variable evar = LevelII2Dataset.this.ds.findVariable(elevName);
                        final Array eleTmp = evar.read();
                        evar.setCachedData(eleTmp, false);
                        final int[] eleOrigin = { swpNumber, 0 };
                        final int[] eleShape = { 1, this.getRadialNumber() };
                        eleData = eleTmp.section(eleOrigin, eleShape);
                    }
                    catch (IOException e) {
                        e.printStackTrace();
                    }
                    catch (InvalidRangeException e2) {
                        e2.printStackTrace();
                    }
                }
                return (float[])eleData.get1DJavaArray(Float.TYPE);
            }
            
            public float[] getAzimuth() throws IOException {
                String aziName;
                if (this.sweepVar.getName().startsWith("Reflectivity")) {
                    aziName = "azimuthR";
                }
                else {
                    aziName = "azimuthV";
                }
                if (!LevelII2Dataset.this.isHighResolution(LevelII2Dataset.this.ds)) {
                    return this.getAzi(aziName, this.sweepno);
                }
                if (this.sweepno >= LevelII2Variable.this.nsweepsHR) {
                    final int swpNo = this.sweepno - LevelII2Variable.this.nsweepsHR;
                    return this.getAzi(aziName, swpNo);
                }
                return this.getAzi(aziName + "_HI", this.sweepno);
            }
            
            public float[] getAzi(final String aziName, final int swpNumber) throws IOException {
                Array aziData = null;
                if (aziData == null) {
                    try {
                        final Variable avar = LevelII2Dataset.this.ds.findVariable(aziName);
                        final Array aziTmp = avar.read();
                        avar.setCachedData(aziTmp, false);
                        final int[] aziOrigin = { swpNumber, 0 };
                        final int[] aziShape = { 1, this.getRadialNumber() };
                        aziData = aziTmp.section(aziOrigin, aziShape);
                    }
                    catch (IOException e) {
                        e.printStackTrace();
                    }
                    catch (InvalidRangeException e2) {
                        e2.printStackTrace();
                    }
                }
                return (float[])aziData.get1DJavaArray(Float.TYPE);
            }
            
            public float getAzimuth(final int ray) throws IOException {
                String aziName;
                if (this.sweepVar.getName().startsWith("Reflectivity")) {
                    aziName = "azimuthR";
                }
                else {
                    aziName = "azimuthV";
                }
                if (!LevelII2Dataset.this.isHighResolution(LevelII2Dataset.this.ds)) {
                    return this.getAzi(aziName, this.sweepno, ray);
                }
                if (this.sweepno >= LevelII2Variable.this.nsweepsHR) {
                    final int swpNo = this.sweepno - LevelII2Variable.this.nsweepsHR;
                    return this.getAzi(aziName, swpNo, ray);
                }
                return this.getAzi(aziName + "_HI", this.sweepno, ray);
            }
            
            public float getAzi(final String aziName, final int swpNumber, final int ray) throws IOException {
                Array aziData = null;
                if (aziData == null) {
                    try {
                        final Variable avar = LevelII2Dataset.this.ds.findVariable(aziName);
                        final Array aziTmp = avar.read();
                        avar.setCachedData(aziTmp, false);
                        final int[] aziOrigin = { swpNumber, 0 };
                        final int[] aziShape = { 1, this.getRadialNumber() };
                        aziData = aziTmp.section(aziOrigin, aziShape);
                    }
                    catch (IOException e) {
                        e.printStackTrace();
                    }
                    catch (InvalidRangeException e2) {
                        e2.printStackTrace();
                    }
                }
                final Index index = aziData.getIndex();
                return aziData.getFloat(index.set(ray));
            }
            
            public float getRadialDistance(final int gate) throws IOException {
                String disName;
                if (this.sweepVar.getName().startsWith("Reflectivity")) {
                    disName = "distanceR";
                }
                else {
                    disName = "distanceV";
                }
                if (!LevelII2Dataset.this.isHighResolution(LevelII2Dataset.this.ds)) {
                    return this.getRadialDist(disName, gate);
                }
                if (this.sweepno >= LevelII2Variable.this.nsweepsHR) {
                    return this.getRadialDist(disName, gate);
                }
                return this.getRadialDist(disName + "_HI", gate);
            }
            
            public float getRadialDist(final String dName, final int gate) throws IOException {
                final Variable dvar = LevelII2Dataset.this.ds.findVariable(dName);
                final Array data = dvar.read();
                dvar.setCachedData(data, false);
                final Index index = data.getIndex();
                return data.getFloat(index.set(gate));
            }
            
            public float getTime(final int ray) throws IOException {
                String tName;
                if (this.sweepVar.getName().startsWith("Reflectivity")) {
                    tName = "timeR";
                }
                else {
                    tName = "timeV";
                }
                if (!LevelII2Dataset.this.isHighResolution(LevelII2Dataset.this.ds)) {
                    return this.getT(tName, this.sweepno, ray);
                }
                if (this.sweepno >= LevelII2Variable.this.nsweepsHR) {
                    final int swpNo = this.sweepno - LevelII2Variable.this.nsweepsHR;
                    return this.getT(tName, swpNo, ray);
                }
                return this.getT(tName + "_HI", this.sweepno, ray);
            }
            
            public float getT(final String tName, final int swpNumber, final int ray) throws IOException {
                final Variable tvar = LevelII2Dataset.this.ds.findVariable(tName);
                final Array timeData = tvar.read();
                tvar.setCachedData(timeData, false);
                final Index timeIndex = timeData.getIndex();
                return timeData.getFloat(timeIndex.set(swpNumber, ray));
            }
            
            public float getBeamWidth() {
                return 0.95f;
            }
            
            public float getNyquistFrequency() {
                return 0.0f;
            }
            
            public float getRangeToFirstGate() {
                try {
                    return this.getRadialDistance(0);
                }
                catch (IOException e) {
                    e.printStackTrace();
                    return 0.0f;
                }
            }
            
            public float getGateSize() {
                try {
                    return this.getRadialDistance(1) - this.getRadialDistance(0);
                }
                catch (IOException e) {
                    e.printStackTrace();
                    return 0.0f;
                }
            }
            
            public boolean isGateSizeConstant() {
                return true;
            }
            
            public void clearSweepMemory() {
            }
        }
    }
}
