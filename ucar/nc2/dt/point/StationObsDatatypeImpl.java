// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.dt.point;

import ucar.unidata.geoloc.EarthLocation;
import ucar.unidata.geoloc.Station;
import ucar.nc2.dt.StationObsDatatype;

public abstract class StationObsDatatypeImpl extends PointObsDatatypeImpl implements StationObsDatatype
{
    protected Station station;
    
    public StationObsDatatypeImpl() {
    }
    
    public StationObsDatatypeImpl(final Station station, final double obsTime, final double nomTime) {
        super(station, obsTime, nomTime);
        this.station = station;
    }
    
    public Station getStation() {
        return this.station;
    }
}
