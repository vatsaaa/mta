// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.dt.point;

import ucar.nc2.dt.DatatypeIterator;
import java.util.Collections;
import ucar.nc2.dt.StationImpl;
import org.slf4j.LoggerFactory;
import java.util.Date;
import ucar.nc2.dt.DataIterator;
import java.text.ParseException;
import ucar.ma2.InvalidRangeException;
import java.util.ArrayList;
import ucar.ma2.Array;
import ucar.ma2.StructureData;
import ucar.nc2.Dimension;
import ucar.unidata.geoloc.Station;
import java.util.HashMap;
import ucar.ma2.ArrayStructure;
import ucar.nc2.Group;
import ucar.nc2.StructurePseudo;
import java.util.List;
import ucar.nc2.constants.AxisType;
import java.io.IOException;
import ucar.nc2.dt.TypedDataset;
import ucar.nc2.util.CancelTask;
import ucar.nc2.dataset.NetcdfDataset;
import java.util.StringTokenizer;
import ucar.nc2.constants.FeatureType;
import ucar.nc2.NetcdfFile;
import ucar.nc2.Structure;
import ucar.nc2.Variable;
import org.slf4j.Logger;
import ucar.nc2.dt.TypedDatasetFactoryIF;

public class CFstationObsDataset extends StationObsDatasetImpl implements TypedDatasetFactoryIF
{
    private static Logger log;
    private Variable latVar;
    private Variable lonVar;
    private Variable altVar;
    private Variable timeVar;
    private Variable lastVar;
    private Variable prevVar;
    private Variable firstVar;
    private Variable nextVar;
    private Variable numChildrenVar;
    private Variable stationIndexVar;
    private Variable stationIdVar;
    private Variable stationDescVar;
    private Variable numStationsVar;
    private boolean hasForwardLinkedList;
    private boolean hasBackwardLinkedList;
    private boolean hasContiguousList;
    private Structure recordVar;
    private RecordDatasetHelper recordHelper;
    private boolean debugRead;
    private int firstRecord;
    
    public static boolean isValidFile(final NetcdfFile ds) {
        if (!ds.findAttValueIgnoreCase(null, "cdm_datatype", "").equalsIgnoreCase(FeatureType.STATION.toString())) {
            return false;
        }
        final String conv = ds.findAttValueIgnoreCase(null, "Conventions", null);
        if (conv == null) {
            return false;
        }
        final StringTokenizer stoke = new StringTokenizer(conv, ",");
        while (stoke.hasMoreTokens()) {
            final String toke = stoke.nextToken().trim();
            if (toke.equalsIgnoreCase("CF-1.0")) {
                return true;
            }
        }
        return false;
    }
    
    public boolean isMine(final NetcdfDataset ds) {
        return isValidFile(ds);
    }
    
    public TypedDataset open(final NetcdfDataset ncd, final CancelTask task, final StringBuilder errlog) throws IOException {
        return new CFstationObsDataset(ncd);
    }
    
    public CFstationObsDataset() {
        this.debugRead = false;
        this.firstRecord = 0;
    }
    
    public CFstationObsDataset(final NetcdfDataset ds) throws IOException {
        super(ds);
        this.debugRead = false;
        this.firstRecord = 0;
        this.latVar = UnidataObsDatasetHelper.getCoordinate(ds, AxisType.Lat);
        this.lonVar = UnidataObsDatasetHelper.getCoordinate(ds, AxisType.Lon);
        this.altVar = UnidataObsDatasetHelper.getCoordinate(ds, AxisType.Height);
        this.timeVar = UnidataObsDatasetHelper.getCoordinate(ds, AxisType.Time);
        if (this.latVar == null) {
            throw new IllegalStateException("Missing latitude variable");
        }
        if (this.lonVar == null) {
            throw new IllegalStateException("Missing longitude coordinate variable");
        }
        if (this.timeVar == null) {
            throw new IllegalStateException("Missing time coordinate variable");
        }
        this.lastVar = UnidataObsDatasetHelper.findVariable(ds, "lastChild");
        this.prevVar = UnidataObsDatasetHelper.findVariable(ds, "prevChild");
        this.firstVar = UnidataObsDatasetHelper.findVariable(ds, "firstChild");
        this.nextVar = UnidataObsDatasetHelper.findVariable(ds, "nextChild");
        this.numChildrenVar = UnidataObsDatasetHelper.findVariable(ds, "numChildren");
        this.stationIndexVar = UnidataObsDatasetHelper.findVariable(ds, "parent_index");
        if (this.stationIndexVar == null) {
            throw new IllegalStateException("Missing parent_index variable");
        }
        this.hasForwardLinkedList = (this.firstVar != null && this.nextVar != null);
        this.hasBackwardLinkedList = (this.lastVar != null && this.prevVar != null);
        this.hasContiguousList = (this.firstVar != null && this.numChildrenVar != null);
        this.stationIdVar = UnidataObsDatasetHelper.findVariable(ds, "station_id");
        this.stationDescVar = UnidataObsDatasetHelper.findVariable(ds, "station_description");
        this.numStationsVar = UnidataObsDatasetHelper.findVariable(ds, "number_stations");
        if (this.stationIdVar == null) {
            throw new IllegalStateException("Missing station id variable");
        }
        (this.recordHelper = new RecordDatasetHelper(ds, this.timeVar.getName(), null, this.dataVariables, this.parseInfo)).setStationInfo(this.stationIndexVar.getName(), (this.stationDescVar == null) ? null : this.stationDescVar.getName());
        this.removeDataVariable(this.stationIndexVar.getName());
        this.removeDataVariable(this.timeVar.getName());
        if (this.prevVar != null) {
            this.removeDataVariable(this.prevVar.getName());
        }
        if (this.nextVar != null) {
            this.removeDataVariable(this.nextVar.getName());
        }
        this.recordVar = this.recordHelper.recordVar;
        this.timeUnit = this.recordHelper.timeUnit;
        this.title = ds.findAttValueIgnoreCase(null, "title", "");
        this.desc = ds.findAttValueIgnoreCase(null, "description", "");
        this.readStationTable();
    }
    
    private void readStationTable() throws IOException {
        final Dimension stationDim = this.ncfile.findDimension("station");
        final StructurePseudo stationTable = new StructurePseudo(this.ncfile, null, "stationTable", stationDim);
        final ArrayStructure stationData = (ArrayStructure)stationTable.read();
        int nstations = 0;
        if (this.numStationsVar != null) {
            nstations = this.numStationsVar.readScalarInt();
        }
        else {
            nstations = stationDim.getLength();
        }
        this.recordHelper.stnHash = new HashMap<Object, Station>(2 * nstations);
        for (int i = 0; i < nstations; ++i) {
            final StructureData sdata = stationData.getStructureData(i);
            final CFStationImpl bean = new CFStationImpl(sdata.getScalarString(this.stationIdVar.getName()), sdata.getScalarString(this.stationDescVar.getName()), sdata.convertScalarDouble(this.latVar.getName()), sdata.convertScalarDouble(this.lonVar.getName()), sdata.convertScalarDouble(this.altVar.getName()));
            this.stations.add(bean);
            this.recordHelper.stnHash.put(i, bean);
        }
    }
    
    private void readStationIndex() throws IOException {
        final Array stationIndexArray = this.stationIndexVar.read();
        final Dimension stationDim = this.ncfile.findDimension("station");
        final StructurePseudo stationTable = new StructurePseudo(this.ncfile, null, "stationTable", stationDim);
        final ArrayStructure stationData = (ArrayStructure)stationTable.read();
        int nstations = 0;
        if (this.numStationsVar != null) {
            nstations = this.numStationsVar.readScalarInt();
        }
        else {
            nstations = stationDim.getLength();
        }
        this.recordHelper.stnHash = new HashMap<Object, Station>(2 * nstations);
        for (int i = 0; i < nstations; ++i) {
            final StructureData sdata = stationData.getStructureData(i);
            final CFStationImpl bean = new CFStationImpl(sdata.getScalarString(this.stationIdVar.getName()), sdata.getScalarString(this.stationDescVar.getName()), sdata.convertScalarDouble(this.latVar.getName()), sdata.convertScalarDouble(this.lonVar.getName()), sdata.convertScalarDouble(this.altVar.getName()));
            this.stations.add(bean);
            this.recordHelper.stnHash.put(i, bean);
        }
    }
    
    @Override
    protected void setTimeUnits() {
    }
    
    @Override
    protected void setStartDate() {
    }
    
    @Override
    protected void setEndDate() {
    }
    
    @Override
    protected void setBoundingBox() {
        this.boundingBox = this.stationHelper.getBoundingBox();
    }
    
    public List getData(final CancelTask cancel) throws IOException {
        final ArrayList allData = new ArrayList();
        final int n = this.getDataCount();
        return allData;
    }
    
    public int getDataCount() {
        final Dimension unlimitedDim = this.ncfile.getUnlimitedDimension();
        return unlimitedDim.getLength();
    }
    
    public List getData(final Station s, final CancelTask cancel) throws IOException {
        return ((CFStationImpl)s).getObservations();
    }
    
    protected RecordDatasetHelper.RecordStationObs makeObs(final int recno, final boolean storeData, StructureData sdata) throws IOException {
        try {
            if (recno > this.getDataCount()) {
                final int n = this.getDataCount();
                this.ncfile.syncExtend();
                CFstationObsDataset.log.info("UnidataStationObsDataset.makeObs recno=" + recno + " > " + n + "; after sync= " + this.getDataCount());
            }
            if (null == sdata) {
                sdata = this.recordVar.readStructure(recno);
            }
            final int stationIndex = sdata.getScalarInt(this.stationIndexVar.getShortName());
            if (stationIndex < 0 || stationIndex >= this.stations.size()) {
                this.parseInfo.append("cant find station at index = " + stationIndex + "\n");
                return null;
            }
            final Station station = this.stations.get(stationIndex);
            if (station == null) {
                this.parseInfo.append("cant find station at index = " + stationIndex + "\n");
                return null;
            }
            final double nomTime;
            final double obsTime = nomTime = this.getTime(this.timeVar, sdata);
            if (storeData) {
                final RecordDatasetHelper recordHelper = this.recordHelper;
                recordHelper.getClass();
                return recordHelper.new RecordStationObs(station, obsTime, nomTime, sdata);
            }
            final RecordDatasetHelper recordHelper2 = this.recordHelper;
            recordHelper2.getClass();
            return recordHelper2.new RecordStationObs(station, obsTime, nomTime, recno);
        }
        catch (InvalidRangeException e) {
            CFstationObsDataset.log.error("CFStationObsDataset.makeObs recno=" + recno, e);
            throw new IOException(e.getMessage());
        }
        catch (ParseException e2) {
            CFstationObsDataset.log.error("CFStationObsDataset.makeObs recno=" + recno, e2);
            throw new IOException(e2.getMessage());
        }
    }
    
    @Override
    public DataIterator getDataIterator(final Station s) {
        return ((CFStationImpl)s).iterator();
    }
    
    @Override
    public DataIterator getDataIterator(final Station s, final Date start, final Date end) {
        return ((CFStationImpl)s).iterator(start, end);
    }
    
    public DataIterator getDataIterator(final int bufferSize) throws IOException {
        return new StationDatatypeIterator(this.recordHelper.recordVar, bufferSize);
    }
    
    static {
        CFstationObsDataset.log = LoggerFactory.getLogger(UnidataStationObsDataset.class);
    }
    
    private class CFStationImpl extends StationImpl
    {
        final /* synthetic */ CFstationObsDataset this$0;
        
        private CFStationImpl(final String name, final String desc, final double lat, final double lon, final double elev) {
            super(name, desc, lat, lon, elev, -1);
        }
        
        @Override
        protected ArrayList readObservations() throws IOException {
            final ArrayList obs = new ArrayList();
            int recno = CFstationObsDataset.this.firstRecord;
            final int end = CFstationObsDataset.this.firstRecord + this.count - 1;
            final int nextRecord = CFstationObsDataset.this.firstRecord;
            while (recno >= 0) {
                try {
                    if (recno > CFstationObsDataset.this.getDataCount()) {
                        final int n = CFstationObsDataset.this.getDataCount();
                        CFstationObsDataset.this.ncfile.syncExtend();
                        CFstationObsDataset.log.info("UnidataStationObsDataset.makeObs recno=" + recno + " > " + n + "; after sync= " + CFstationObsDataset.this.getDataCount());
                    }
                    final StructureData sdata = CFstationObsDataset.this.recordVar.readStructure(recno);
                    recno = nextRecord;
                    continue;
                }
                catch (InvalidRangeException e) {
                    CFstationObsDataset.log.error("UnidataStationObsDataset.readObservation recno=" + recno, e);
                    throw new IOException(e.getMessage());
                }
                break;
            }
            Collections.sort((List<Comparable>)obs);
            return obs;
        }
        
        DataIterator iterator() {
            return new StationIterator();
        }
        
        DataIterator iterator(final Date start, final Date end) {
            return new StationIterator(start, end);
        }
        
        private class StationIterator implements DataIterator
        {
            int nextRecno;
            int last;
            double startTime;
            double endTime;
            boolean hasDateRange;
            
            StationIterator() {
                this.nextRecno = CFstationObsDataset.this.firstRecord;
                this.last = CFstationObsDataset.this.firstRecord + CFStationImpl.this.count - 1;
            }
            
            StationIterator(final Date start, final Date end) {
                this.nextRecno = CFstationObsDataset.this.firstRecord;
                this.last = CFstationObsDataset.this.firstRecord + CFStationImpl.this.count - 1;
                this.startTime = CFStationImpl.this.this$0.timeUnit.makeValue(start);
                this.endTime = CFStationImpl.this.this$0.timeUnit.makeValue(end);
                this.hasDateRange = true;
            }
            
            public boolean hasNext() {
                return this.nextRecno >= 0;
            }
            
            public Object nextData() throws IOException {
                final RecordDatasetHelper.RecordStationObs sobs = CFstationObsDataset.this.makeObs(this.nextRecno, true, null);
                if (!sobs.getStation().getName().equals(CFStationImpl.this.getName())) {
                    throw new IllegalStateException("BAD Station link (" + this.nextRecno + ") station name=" + sobs.getStation().getName() + " should be " + CFStationImpl.this.getName());
                }
                if (this.hasDateRange) {
                    final double timeValue = sobs.getObservationTime();
                    if (timeValue < this.startTime || timeValue > this.endTime) {
                        return this.nextData();
                    }
                }
                return sobs;
            }
            
            public Object next() {
                try {
                    return this.nextData();
                }
                catch (IOException e) {
                    CFstationObsDataset.log.error("CFStationObsDataset.StationIterator.next recno=" + this.nextRecno, e);
                    throw new IllegalStateException(e.getMessage());
                }
            }
            
            public void remove() {
                throw new UnsupportedOperationException();
            }
        }
    }
    
    private class StationDatatypeIterator extends DatatypeIterator
    {
        @Override
        protected Object makeDatatypeWithData(final int recnum, final StructureData sdata) throws IOException {
            return CFstationObsDataset.this.makeObs(recnum, true, sdata);
        }
        
        StationDatatypeIterator(final Structure struct, final int bufferSize) {
            super(struct, bufferSize);
        }
    }
}
