// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.dt.point;

import ucar.nc2.dt.DatatypeIterator;
import java.util.Collections;
import ucar.nc2.dt.StationImpl;
import ucar.nc2.dt.DataIterator;
import ucar.nc2.Dimension;
import ucar.nc2.dt.StationObsDatatype;
import java.util.ArrayList;
import ucar.ma2.Array;
import ucar.ma2.StructureData;
import ucar.ma2.MAMath;
import ucar.ma2.InvalidRangeException;
import ucar.unidata.geoloc.Station;
import java.util.HashMap;
import ucar.ma2.ArrayInt;
import java.util.List;
import java.util.StringTokenizer;
import ucar.nc2.Variable;
import java.io.IOException;
import ucar.nc2.dt.TypedDataset;
import ucar.nc2.util.CancelTask;
import ucar.nc2.dataset.NetcdfDataset;
import ucar.nc2.Attribute;
import ucar.nc2.NetcdfFile;
import ucar.nc2.Structure;
import ucar.nc2.dt.TypedDatasetFactoryIF;

public class MadisStationObsDataset extends StationObsDatasetImpl implements TypedDatasetFactoryIF
{
    private Structure recordVar;
    private RecordDatasetHelper recordHelper;
    private String obsTimeVName;
    private String nomTimeVName;
    private String stnIdVName;
    private String stnDescVName;
    private String altVName;
    private boolean debug;
    private boolean debugLinks;
    
    public static boolean isValidFile(final NetcdfFile ds) {
        if (ds.findVariable("staticIds") == null) {
            return false;
        }
        if (ds.findVariable("nStaticIds") == null) {
            return false;
        }
        if (ds.findVariable("lastRecord") == null) {
            return false;
        }
        if (ds.findVariable("prevRecord") == null) {
            return false;
        }
        if (ds.findVariable("latitude") == null) {
            return false;
        }
        if (ds.findVariable("longitude") == null) {
            return false;
        }
        if (!ds.hasUnlimitedDimension()) {
            return false;
        }
        if (ds.findGlobalAttribute("timeVariables") == null) {
            return false;
        }
        if (ds.findGlobalAttribute("idVariables") == null) {
            return false;
        }
        final Attribute att = ds.findGlobalAttribute("title");
        return att == null || !att.getStringValue().equals("MADIS ACARS data");
    }
    
    public boolean isMine(final NetcdfDataset ds) {
        return isValidFile(ds);
    }
    
    public TypedDataset open(final NetcdfDataset ncd, final CancelTask task, final StringBuilder errlog) throws IOException {
        return new MadisStationObsDataset(ncd);
    }
    
    public MadisStationObsDataset() {
        this.debug = false;
        this.debugLinks = false;
    }
    
    public MadisStationObsDataset(final NetcdfDataset ds) throws IOException {
        super(ds);
        this.debug = false;
        this.debugLinks = false;
        this.altVName = "elevation";
        final String timeNames = ds.findAttValueIgnoreCase(null, "timeVariables", null);
        StringTokenizer stoker = new StringTokenizer(timeNames, ", ");
        this.obsTimeVName = stoker.nextToken();
        if (ds.findVariable("timeNominal") != null) {
            this.nomTimeVName = "timeNominal";
        }
        final String idNames = ds.findAttValueIgnoreCase(null, "idVariables", null);
        stoker = new StringTokenizer(idNames, ", ");
        this.stnIdVName = stoker.nextToken();
        if (this.stnIdVName.equals("stationName")) {
            if (ds.findVariable("locationName") != null) {
                this.stnDescVName = "locationName";
            }
            if (this.debug) {
                System.out.println("filetype 1 (metars)");
            }
        }
        else if (this.stnIdVName.equals("latitude")) {
            if (ds.findVariable("en_tailNumber") != null) {
                this.stnIdVName = "en_tailNumber";
            }
            this.altVName = "altitude";
            if (this.debug) {
                System.out.println("filetype 3 (acars)");
            }
        }
        else {
            if (ds.findVariable("stationId") != null) {
                this.stnIdVName = "stationId";
            }
            if (ds.findVariable("stationName") != null) {
                this.stnDescVName = "stationName";
            }
            if (this.debug) {
                System.out.println("filetype 2 (mesonet)");
            }
        }
        if (this.debug) {
            System.out.println("title= " + this.ncfile.findAttValueIgnoreCase(null, "title", null));
        }
        (this.recordHelper = new RecordDatasetHelper(ds, this.obsTimeVName, this.nomTimeVName, this.dataVariables, this.parseInfo)).setStationInfo(this.stnIdVName, this.stnDescVName);
        this.removeDataVariable(this.obsTimeVName);
        this.removeDataVariable(this.nomTimeVName);
        this.removeDataVariable("latitude");
        this.removeDataVariable("longitude");
        this.removeDataVariable(this.altVName);
        this.removeDataVariable("prevRecord");
        this.removeDataVariable(this.stnIdVName);
        this.removeDataVariable(this.stnDescVName);
        this.timeUnit = this.recordHelper.timeUnit;
        final Variable lastRecordVar = ds.findVariable("lastRecord");
        final ArrayInt.D1 lastRecord = (ArrayInt.D1)lastRecordVar.read();
        final Variable inventoryVar = ds.findVariable("inventory");
        final ArrayInt.D1 inventoryData = (ArrayInt.D1)inventoryVar.read();
        final Variable v = ds.findVariable("nStaticIds");
        final int n = v.readScalarInt();
        this.recordHelper.stnHash = new HashMap<Object, Station>(2 * n);
        this.recordVar = (Structure)ds.findVariable("record");
        for (int stnIndex = 0; stnIndex < n; ++stnIndex) {
            final int lastValue = lastRecord.get(stnIndex);
            final int inventory = inventoryData.get(stnIndex);
            if (lastValue >= 0) {
                StructureData sdata = null;
                try {
                    sdata = this.recordVar.readStructure(lastValue);
                }
                catch (InvalidRangeException e) {
                    this.parseInfo.append("Invalid lastValue=" + lastValue + " for station at index " + stnIndex + "\n");
                    continue;
                }
                final String stationId = sdata.getScalarString(this.stnIdVName).trim();
                final String stationDesc = (this.stnDescVName == null) ? null : sdata.getScalarString(this.stnDescVName);
                final float lat = sdata.convertScalarFloat("latitude");
                final float lon = sdata.convertScalarFloat("longitude");
                final float alt = sdata.convertScalarFloat(this.altVName);
                final MadisStationImpl stn = this.recordHelper.stnHash.get(stationId);
                if (stn != null) {
                    if (this.debugLinks) {
                        this.parseInfo.append("Already have=" + stationId + " for station at index " + lastValue + " lastRecord= " + stn.lastRecord + "\n");
                        this.parseInfo.append("  old lat=" + stn.getLatitude() + " lon= " + stn.getLongitude() + " alt= " + stn.getAltitude() + "\n");
                        this.parseInfo.append("  new lat=" + lat + " lon= " + lon + " alt= " + alt + "\n");
                    }
                    stn.addLinkedList(lastValue, inventory);
                }
                else {
                    final MadisStationImpl station = new MadisStationImpl(stationId, stationDesc, (double)lat, (double)lon, (double)alt, lastValue, inventory);
                    this.recordHelper.stnHash.put(stationId, station);
                    this.stations.add(station);
                }
            }
        }
        if (this.debug) {
            System.out.println("total stations " + this.stations.size() + " should be = " + n);
        }
        final Variable timeVar = ds.findVariable(this.obsTimeVName);
        final Array timeData = timeVar.read();
        final MAMath.MinMax minmax = MAMath.getMinMax(timeData);
        this.startDate = this.timeUnit.makeDate(minmax.min);
        this.endDate = this.timeUnit.makeDate(minmax.max);
        this.setBoundingBox();
    }
    
    @Override
    protected void setTimeUnits() {
    }
    
    @Override
    protected void setStartDate() {
    }
    
    @Override
    protected void setEndDate() {
    }
    
    @Override
    protected void setBoundingBox() {
        this.boundingBox = this.stationHelper.getBoundingBox();
    }
    
    public List getData(final CancelTask cancel) throws IOException {
        final ArrayList allData = new ArrayList();
        for (int i = 0; i < this.getDataCount(); ++i) {
            allData.add(this.makeObs(i));
            if (cancel != null && cancel.isCancel()) {
                return null;
            }
        }
        return allData;
    }
    
    protected StationObsDatatype makeObs(final int recno) throws IOException {
        try {
            final StructureData sdata = this.recordVar.readStructure(recno);
            final String stationId = sdata.getScalarString(this.stnIdVName);
            final Station s = this.recordHelper.stnHash.get(stationId);
            final float obsTime = sdata.convertScalarFloat(this.obsTimeVName);
            final float nomTime = (this.nomTimeVName == null) ? obsTime : sdata.convertScalarFloat(this.nomTimeVName);
            final RecordDatasetHelper recordHelper = this.recordHelper;
            recordHelper.getClass();
            return recordHelper.new RecordStationObs(s, obsTime, nomTime, recno);
        }
        catch (InvalidRangeException e) {
            e.printStackTrace();
            throw new IOException(e.getMessage());
        }
    }
    
    public List getData(final Station s, final CancelTask cancel) throws IOException {
        return ((MadisStationImpl)s).getObservations();
    }
    
    public int getDataCount() {
        final Dimension unlimitedDim = this.ncfile.getUnlimitedDimension();
        return unlimitedDim.getLength();
    }
    
    public DataIterator getDataIterator(final int bufferSize) throws IOException {
        return new MadisDatatypeIterator(this.recordHelper.recordVar, bufferSize);
    }
    
    private class MadisStationImpl extends StationImpl
    {
        private int lastRecord;
        private ArrayList lastRecords;
        
        private MadisStationImpl(final String name, final String desc, final double lat, final double lon, final double alt, final int lastRecord, final int inventory) {
            super(name, desc, lat, lon, alt);
            this.lastRecord = lastRecord;
            this.count = this.count(inventory);
        }
        
        private void addLinkedList(final int lastRecord, final int inventory) {
            if (this.lastRecords == null) {
                this.lastRecords = new ArrayList();
            }
            this.lastRecords.add(new Integer(lastRecord));
            this.count += this.count(inventory);
        }
        
        private int count(int inventory) {
            int cnt = 0;
            for (int bitno = 0; bitno < 24; ++bitno) {
                cnt += (inventory & 0x1);
                inventory >>= 1;
            }
            return cnt;
        }
        
        @Override
        protected ArrayList readObservations() throws IOException {
            final ArrayList result = new ArrayList();
            this.readLinkedList(this, result, this.lastRecord);
            if (this.lastRecords == null) {
                return result;
            }
            for (int i = 0; i < this.lastRecords.size(); ++i) {
                final Integer lastLink = this.lastRecords.get(i);
                this.readLinkedList(this, result, lastLink);
            }
            Collections.sort((List<Comparable>)result);
            return result;
        }
        
        protected void readLinkedList(final StationImpl s, final ArrayList result, final int lastLink) throws IOException {
            int recnum = lastLink;
            while (recnum >= 0) {
                try {
                    final StructureData sdata = MadisStationObsDataset.this.recordVar.readStructure(recnum);
                    final int prevRecord = sdata.getScalarInt("prevRecord");
                    final float obsTime = sdata.convertScalarFloat(MadisStationObsDataset.this.obsTimeVName);
                    final float nomTime = (MadisStationObsDataset.this.nomTimeVName == null) ? obsTime : sdata.convertScalarFloat(MadisStationObsDataset.this.nomTimeVName);
                    final int index = 0;
                    final RecordDatasetHelper access$600 = MadisStationObsDataset.this.recordHelper;
                    access$600.getClass();
                    result.add(index, access$600.new RecordStationObs(s, obsTime, nomTime, recnum));
                    recnum = prevRecord;
                    continue;
                }
                catch (InvalidRangeException e) {
                    e.printStackTrace();
                    throw new IOException(e.getMessage());
                }
                break;
            }
        }
    }
    
    private class MadisDatatypeIterator extends DatatypeIterator
    {
        @Override
        protected Object makeDatatypeWithData(final int recnum, final StructureData sdata) {
            return MadisStationObsDataset.this.recordHelper.new RecordStationObs(recnum, sdata);
        }
        
        MadisDatatypeIterator(final Structure struct, final int bufferSize) {
            super(struct, bufferSize);
        }
    }
}
