// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.dt.point;

import java.util.Iterator;
import ucar.nc2.dt.DataIteratorAdapter;
import ucar.nc2.dt.DataIterator;
import java.util.Date;
import ucar.nc2.dt.StationImpl;
import ucar.unidata.geoloc.LatLonRect;
import java.io.IOException;
import ucar.nc2.util.CancelTask;
import ucar.nc2.dt.StationObsDatatype;
import ucar.nc2.constants.FeatureType;
import ucar.nc2.dataset.NetcdfDataset;
import java.util.ArrayList;
import ucar.unidata.geoloc.Station;
import java.util.List;
import ucar.nc2.dt.StationObsDataset;

public abstract class StationObsDatasetImpl extends PointObsDatasetImpl implements StationObsDataset
{
    protected StationDatasetHelper stationHelper;
    protected List<Station> stations;
    
    public StationObsDatasetImpl() {
        this.stations = new ArrayList<Station>();
        this.stationHelper = new StationDatasetHelper(this);
    }
    
    public StationObsDatasetImpl(final String title, final String description, final String location) {
        super(title, description, location);
        this.stations = new ArrayList<Station>();
        this.stationHelper = new StationDatasetHelper(this);
    }
    
    public StationObsDatasetImpl(final NetcdfDataset ncfile) {
        super(ncfile);
        this.stations = new ArrayList<Station>();
        this.stationHelper = new StationDatasetHelper(this);
    }
    
    @Override
    public String getDetailInfo() {
        final StringBuffer sbuff = new StringBuffer();
        sbuff.append("StationObsDataset\n");
        sbuff.append(super.getDetailInfo());
        return sbuff.toString();
    }
    
    @Override
    public FeatureType getScientificDataType() {
        return FeatureType.STATION;
    }
    
    @Override
    public Class getDataClass() {
        return StationObsDatatype.class;
    }
    
    public List<Station> getStations() throws IOException {
        return this.getStations((CancelTask)null);
    }
    
    public List<Station> getStations(final CancelTask cancel) throws IOException {
        return this.stations;
    }
    
    public List<Station> getStations(final LatLonRect boundingBox) throws IOException {
        return this.getStations(boundingBox, null);
    }
    
    public List<Station> getStations(final LatLonRect boundingBox, final CancelTask cancel) throws IOException {
        return this.stationHelper.getStations(boundingBox, cancel);
    }
    
    public Station getStation(final String id) {
        return this.stationHelper.getStation(id);
    }
    
    public int getStationDataCount(final Station s) {
        final StationImpl si = (StationImpl)s;
        return si.getNumObservations();
    }
    
    public List getData(final Station s) throws IOException {
        return this.getData(s, null);
    }
    
    public List getData(final Station s, final Date start, final Date end) throws IOException {
        return this.getData(s, start, end, null);
    }
    
    public List getData(final Station s, final Date start, final Date end, final CancelTask cancel) throws IOException {
        final double startTime = this.timeUnit.makeValue(start);
        final double endTime = this.timeUnit.makeValue(end);
        return this.stationHelper.getStationObs(s, startTime, endTime, cancel);
    }
    
    public List getData(final List<Station> stations) throws IOException {
        return this.getData(stations, null);
    }
    
    public List getData(final List<Station> stations, final CancelTask cancel) throws IOException {
        return this.stationHelper.getStationObs(stations, cancel);
    }
    
    public List getData(final List<Station> stations, final Date start, final Date end) throws IOException {
        return this.getData(stations, start, end, null);
    }
    
    public List getData(final List<Station> stations, final Date start, final Date end, final CancelTask cancel) throws IOException {
        final double startTime = this.timeUnit.makeValue(start);
        final double endTime = this.timeUnit.makeValue(end);
        return this.stationHelper.getStationObs(stations, startTime, endTime, cancel);
    }
    
    public List getData(final LatLonRect boundingBox, final CancelTask cancel) throws IOException {
        return this.stationHelper.getStationObs(boundingBox, cancel);
    }
    
    public List getData(final LatLonRect boundingBox, final Date start, final Date end, final CancelTask cancel) throws IOException {
        final double startTime = this.timeUnit.makeValue(start);
        final double endTime = this.timeUnit.makeValue(end);
        return this.stationHelper.getStationObs(boundingBox, startTime, endTime, cancel);
    }
    
    public void sortByTime(final List<StationObsDatatype> stationObs) {
        this.stationHelper.sortByTime(stationObs);
    }
    
    public DataIterator getDataIterator(final Station s) {
        try {
            return new DataIteratorAdapter(this.getData(s).iterator());
        }
        catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
    
    public DataIterator getDataIterator(final Station s, final Date start, final Date end) {
        try {
            return new DataIteratorAdapter(this.getData(s, start, end).iterator());
        }
        catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}
