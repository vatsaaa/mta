// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.dt.point;

import ucar.ma2.InvalidRangeException;
import java.util.Date;
import java.io.IOException;
import ucar.ma2.StructureMembers;
import ucar.ma2.StructureData;
import ucar.ma2.StructureDataIterator;
import ucar.unidata.geoloc.LatLonPoint;
import ucar.unidata.geoloc.LatLonPointImpl;
import ucar.unidata.geoloc.EarthLocation;
import ucar.unidata.geoloc.EarthLocationImpl;
import ucar.nc2.dt.StationObsDatatype;
import ucar.nc2.dt.StationImpl;
import java.util.HashMap;
import java.util.ArrayList;
import ucar.nc2.util.CancelTask;
import java.util.Iterator;
import ucar.nc2.Variable;
import ucar.nc2.Structure;
import ucar.nc2.Group;
import ucar.nc2.NetcdfFile;
import ucar.nc2.StructurePseudo;
import java.util.List;
import ucar.nc2.units.DateUnit;
import ucar.unidata.geoloc.LatLonRect;
import ucar.nc2.Dimension;
import ucar.nc2.dataset.StructureDS;
import ucar.unidata.geoloc.Station;
import java.util.Map;
import ucar.ma2.DataType;
import ucar.nc2.dataset.NetcdfDataset;

public class RecordDatasetHelper
{
    protected NetcdfDataset ncfile;
    protected String obsTimeVName;
    protected String nomTimeVName;
    protected String stnIdVName;
    protected String stnNameVName;
    protected String stnDescVName;
    protected String latVName;
    protected String lonVName;
    protected String altVName;
    protected DataType stationIdType;
    protected Map<Object, Station> stnHash;
    protected StructureDS recordVar;
    protected Dimension obsDim;
    protected LatLonRect boundingBox;
    protected double minDate;
    protected double maxDate;
    protected DateUnit timeUnit;
    protected double altScaleFactor;
    protected StringBuffer errs;
    protected boolean showErrors;
    private boolean debugBB;
    
    public RecordDatasetHelper(final NetcdfDataset ncfile, final String obsTimeVName, final String nomTimeVName, final List typedDataVariables) {
        this(ncfile, obsTimeVName, nomTimeVName, typedDataVariables, null, null);
    }
    
    public RecordDatasetHelper(final NetcdfDataset ncfile, final String obsTimeVName, final String nomTimeVName, final List typedDataVariables, final StringBuffer errBuffer) {
        this(ncfile, obsTimeVName, nomTimeVName, typedDataVariables, null, errBuffer);
    }
    
    public RecordDatasetHelper(final NetcdfDataset ncfile, final String obsTimeVName, final String nomTimeVName, final List typedDataVariables, final String recDimName, final StringBuffer errBuffer) {
        this.altScaleFactor = 1.0;
        this.errs = null;
        this.showErrors = true;
        this.debugBB = false;
        this.ncfile = ncfile;
        this.obsTimeVName = obsTimeVName;
        this.nomTimeVName = nomTimeVName;
        this.errs = errBuffer;
        if (this.ncfile.hasUnlimitedDimension()) {
            this.ncfile.sendIospMessage("AddRecordStructure");
            this.recordVar = (StructureDS)this.ncfile.getRootGroup().findVariable("record");
            this.obsDim = ncfile.getUnlimitedDimension();
        }
        else {
            if (recDimName == null) {
                throw new IllegalArgumentException("File <" + this.ncfile.getLocation() + "> has no unlimited dimension, specify psuedo record dimension with observationDimension global attribute.");
            }
            this.obsDim = this.ncfile.getRootGroup().findDimension(recDimName);
            this.recordVar = new StructureDS(null, new StructurePseudo(this.ncfile, null, "record", this.obsDim));
        }
        final List<Variable> recordMembers = ncfile.getVariables();
        for (final Variable v : recordMembers) {
            if (v == this.recordVar) {
                continue;
            }
            if (v.isScalar()) {
                continue;
            }
            if (v.getDimension(0) != this.obsDim) {
                continue;
            }
            typedDataVariables.add(v);
        }
        final Variable timeVar = ncfile.findVariable(obsTimeVName);
        final String timeUnitString = ncfile.findAttValueIgnoreCase(timeVar, "units", "seconds since 1970-01-01");
        try {
            this.timeUnit = new DateUnit(timeUnitString);
        }
        catch (Exception e) {
            if (null != this.errs) {
                this.errs.append("Error on string = " + timeUnitString + " == " + e.getMessage() + "\n");
            }
            try {
                this.timeUnit = new DateUnit("seconds since 1970-01-01");
            }
            catch (Exception ex) {}
        }
    }
    
    public void setStationInfo(final String stnIdVName, final String stnDescVName) {
        this.stnIdVName = stnIdVName;
        this.stnDescVName = stnDescVName;
        final Variable stationVar = this.ncfile.findVariable(stnIdVName);
        this.stationIdType = stationVar.getDataType();
    }
    
    public void setLocationInfo(final String latVName, final String lonVName, final String altVName) {
        this.latVName = latVName;
        this.lonVName = lonVName;
        this.altVName = altVName;
        if (altVName != null) {
            final Variable v = this.ncfile.findVariable(altVName);
            final String units = this.ncfile.findAttValueIgnoreCase(v, "units", null);
            if (units != null) {
                try {
                    this.altScaleFactor = PointObsDatasetImpl.getMetersConversionFactor(units);
                }
                catch (Exception e) {
                    if (this.errs != null) {
                        this.errs.append(e.getMessage());
                    }
                }
            }
        }
    }
    
    public Structure getRecordVar() {
        return this.recordVar;
    }
    
    public int getRecordCount() {
        final Dimension unlimitedDim = this.ncfile.getUnlimitedDimension();
        return unlimitedDim.getLength();
    }
    
    public void setTimeUnit(final DateUnit timeUnit) {
        this.timeUnit = timeUnit;
    }
    
    public DateUnit getTimeUnit() {
        return this.timeUnit;
    }
    
    public ArrayList readAllCreateObs(final CancelTask cancel) throws IOException {
        final boolean hasStations = this.stnIdVName != null;
        if (hasStations) {
            this.stnHash = new HashMap<Object, Station>();
        }
        double minDate = Double.MAX_VALUE;
        double maxDate = -1.7976931348623157E308;
        double minLat = Double.MAX_VALUE;
        double maxLat = -1.7976931348623157E308;
        double minLon = Double.MAX_VALUE;
        double maxLon = -1.7976931348623157E308;
        final ArrayList records = new ArrayList();
        int recno = 0;
        final StructureDataIterator ii = this.recordVar.getStructureIterator();
        while (ii.hasNext()) {
            final StructureData sdata = ii.next();
            final StructureMembers members = sdata.getStructureMembers();
            Object stationId = null;
            if (hasStations) {
                if (this.stationIdType == DataType.INT) {
                    final int stationNum = sdata.getScalarInt(this.stnIdVName);
                    stationId = new Integer(stationNum);
                }
                else {
                    stationId = sdata.getScalarString(this.stnIdVName).trim();
                }
            }
            final String desc = (this.stnDescVName == null) ? null : sdata.getScalarString(this.stnDescVName);
            final double lat = sdata.convertScalarDouble(this.latVName);
            final double lon = sdata.convertScalarDouble(this.lonVName);
            final double alt = (this.altVName == null) ? Double.NaN : (this.altScaleFactor * sdata.convertScalarDouble(this.altVName));
            final double obsTime = sdata.convertScalarDouble(members.findMember(this.obsTimeVName));
            final double nomTime = (this.nomTimeVName == null) ? obsTime : sdata.convertScalarDouble(members.findMember(this.nomTimeVName));
            if (hasStations) {
                StationImpl stn = this.stnHash.get(stationId);
                if (stn == null) {
                    stn = new StationImpl(stationId.toString(), desc, lat, lon, alt);
                    this.stnHash.put(stationId, stn);
                }
                final RecordStationObs stnObs = new RecordStationObs(stn, obsTime, nomTime, recno);
                records.add(stnObs);
                stn.addObs(stnObs);
            }
            else {
                records.add(new RecordPointObs(new EarthLocationImpl(lat, lon, alt), obsTime, nomTime, recno));
            }
            minDate = Math.min(minDate, obsTime);
            maxDate = Math.max(maxDate, obsTime);
            minLat = Math.min(minLat, lat);
            maxLat = Math.max(maxLat, lat);
            minLon = Math.min(minLon, lon);
            maxLon = Math.max(maxLon, lon);
            ++recno;
            if (cancel != null && cancel.isCancel()) {
                return null;
            }
        }
        this.boundingBox = new LatLonRect(new LatLonPointImpl(minLat, minLon), new LatLonPointImpl(maxLat, maxLon));
        return records;
    }
    
    public void setShortNames(final String latVName, final String lonVName, final String altVName, final String obsTimeVName, final String nomTimeVName) {
        this.latVName = latVName;
        this.lonVName = lonVName;
        this.altVName = altVName;
        this.obsTimeVName = obsTimeVName;
        this.nomTimeVName = nomTimeVName;
    }
    
    public List getData(final ArrayList records, final LatLonRect boundingBox, final CancelTask cancel) throws IOException {
        if (this.debugBB) {
            System.out.println("Want bb= " + boundingBox);
        }
        final ArrayList result = new ArrayList();
        for (int i = 0; i < records.size(); ++i) {
            final RecordPointObs r = records.get(i);
            if (boundingBox.contains(r.getLatLon())) {
                if (this.debugBB) {
                    System.out.println(" ok latlon= " + r.getLatLon());
                }
                result.add(r);
            }
            if (cancel != null && cancel.isCancel()) {
                return null;
            }
        }
        return result;
    }
    
    public List getData(final ArrayList records, final LatLonRect boundingBox, final double startTime, final double endTime, final CancelTask cancel) throws IOException {
        if (this.debugBB) {
            System.out.println("Want bb= " + boundingBox);
        }
        final ArrayList result = new ArrayList();
        for (int i = 0; i < records.size(); ++i) {
            final RecordPointObs r = records.get(i);
            if (boundingBox.contains(r.getLatLon())) {
                if (this.debugBB) {
                    System.out.println(" ok latlon= " + r.getLatLon());
                }
                final double timeValue = r.getObservationTime();
                if (timeValue >= startTime && timeValue <= endTime) {
                    result.add(r);
                }
            }
            if (cancel != null && cancel.isCancel()) {
                return null;
            }
        }
        return result;
    }
    
    public class RecordPointObs extends PointObsDatatypeImpl
    {
        protected int recno;
        protected LatLonPointImpl llpt;
        protected StructureData sdata;
        
        protected RecordPointObs() {
            this.llpt = null;
        }
        
        protected RecordPointObs(final EarthLocation location, final double obsTime, final double nomTime, final int recno) {
            super(location, obsTime, nomTime);
            this.llpt = null;
            this.recno = recno;
        }
        
        public RecordPointObs(final int recno, final StructureData sdata) {
            this.llpt = null;
            this.recno = recno;
            this.sdata = sdata;
            final StructureMembers members = sdata.getStructureMembers();
            this.obsTime = sdata.convertScalarDouble(members.findMember(RecordDatasetHelper.this.obsTimeVName));
            this.nomTime = ((RecordDatasetHelper.this.nomTimeVName == null) ? this.obsTime : sdata.convertScalarDouble(members.findMember(RecordDatasetHelper.this.nomTimeVName)));
            final double lat = sdata.convertScalarDouble(RecordDatasetHelper.this.latVName);
            final double lon = sdata.convertScalarDouble(RecordDatasetHelper.this.lonVName);
            final double alt = (RecordDatasetHelper.this.altVName == null) ? Double.NaN : (RecordDatasetHelper.this.altScaleFactor * sdata.convertScalarDouble(RecordDatasetHelper.this.altVName));
            this.location = new EarthLocationImpl(lat, lon, alt);
        }
        
        public LatLonPoint getLatLon() {
            if (this.llpt == null) {
                this.llpt = new LatLonPointImpl(this.location.getLatitude(), this.location.getLongitude());
            }
            return this.llpt;
        }
        
        public Date getNominalTimeAsDate() {
            return RecordDatasetHelper.this.timeUnit.makeDate(this.getNominalTime());
        }
        
        public Date getObservationTimeAsDate() {
            return RecordDatasetHelper.this.timeUnit.makeDate(this.getObservationTime());
        }
        
        public StructureData getData() throws IOException {
            if (null != this.sdata) {
                return this.sdata;
            }
            try {
                return RecordDatasetHelper.this.recordVar.readStructure(this.recno);
            }
            catch (InvalidRangeException e) {
                e.printStackTrace();
                throw new IOException(e.getMessage());
            }
        }
    }
    
    public class RecordStationObs extends RecordPointObs implements StationObsDatatype
    {
        private Station station;
        
        protected RecordStationObs(final Station station, final double obsTime, final double nomTime, final int recno) {
            super(station, obsTime, nomTime, recno);
            this.station = station;
        }
        
        protected RecordStationObs(final int recno, final StructureData sdata) {
            this.recno = recno;
            this.sdata = sdata;
            final StructureMembers members = sdata.getStructureMembers();
            this.obsTime = sdata.convertScalarDouble(members.findMember(RecordDatasetHelper.this.obsTimeVName));
            this.nomTime = ((RecordDatasetHelper.this.nomTimeVName == null) ? this.obsTime : sdata.convertScalarDouble(members.findMember(RecordDatasetHelper.this.nomTimeVName)));
            Object stationId;
            if (RecordDatasetHelper.this.stationIdType == DataType.INT) {
                stationId = sdata.getScalarInt(RecordDatasetHelper.this.stnIdVName);
            }
            else {
                stationId = sdata.getScalarString(RecordDatasetHelper.this.stnIdVName).trim();
            }
            this.station = RecordDatasetHelper.this.stnHash.get(stationId);
            this.location = this.station;
            if (this.station == null) {
                if (null != RecordDatasetHelper.this.errs) {
                    RecordDatasetHelper.this.errs.append(" cant find station = <").append(stationId).append(">when reading record ").append(recno).append("\n");
                }
                if (RecordDatasetHelper.this.showErrors) {
                    System.out.println(" cant find station = <" + stationId + ">" + "when reading record " + recno);
                }
            }
        }
        
        protected RecordStationObs(final Station station, final double obsTime, final double nomTime, final StructureData sdata) {
            this.station = station;
            this.location = station;
            this.obsTime = obsTime;
            this.nomTime = nomTime;
            this.sdata = sdata;
        }
        
        protected RecordStationObs(final Station station, final StructureData sdata) {
            this.station = station;
            this.location = station;
            this.sdata = sdata;
            final StructureMembers members = sdata.getStructureMembers();
            this.obsTime = sdata.convertScalarDouble(members.findMember(RecordDatasetHelper.this.obsTimeVName));
            this.nomTime = ((RecordDatasetHelper.this.nomTimeVName == null) ? this.obsTime : sdata.convertScalarDouble(members.findMember(RecordDatasetHelper.this.nomTimeVName)));
        }
        
        public Station getStation() {
            return this.station;
        }
        
        @Override
        public StructureData getData() throws IOException {
            if (null != this.sdata) {
                return this.sdata;
            }
            try {
                return RecordDatasetHelper.this.recordVar.readStructure(this.recno);
            }
            catch (InvalidRangeException e) {
                e.printStackTrace();
                throw new IOException(e.getMessage());
            }
        }
    }
}
