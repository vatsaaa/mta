// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.dt.point;

import ucar.unidata.geoloc.EarthLocationImpl;
import ucar.unidata.geoloc.EarthLocation;
import java.util.Iterator;
import ucar.nc2.dt.DataIteratorAdapter;
import ucar.nc2.dt.DataIterator;
import ucar.unidata.geoloc.Station;
import ucar.nc2.dt.StationImpl;
import ucar.ma2.StructureData;
import java.util.Date;
import java.util.ArrayList;
import ucar.ma2.ArrayStructure;
import ucar.unidata.geoloc.LatLonPoint;
import ucar.unidata.geoloc.LatLonRect;
import ucar.unidata.geoloc.LatLonPointImpl;
import ucar.nc2.Attribute;
import ucar.nc2.units.DateUnit;
import ucar.nc2.dt.TypedDataset;
import ucar.nc2.util.CancelTask;
import java.io.IOException;
import ucar.nc2.Structure;
import java.util.List;
import ucar.nc2.constants.AxisType;
import ucar.nc2.dataset.CoordinateAxis;
import ucar.nc2.dt.PointObsDataset;
import ucar.nc2.dataset.NetcdfDataset;
import java.util.StringTokenizer;
import ucar.nc2.NetcdfFile;
import ucar.ma2.StructureMembers;
import ucar.nc2.dataset.StructureDS;
import ucar.nc2.Variable;
import ucar.nc2.dods.DODSNetcdfFile;
import ucar.nc2.dt.TypedDatasetFactoryIF;

public class DapperDataset extends PointObsDatasetImpl implements TypedDatasetFactoryIF
{
    private static final String ID = "_id";
    protected DODSNetcdfFile dodsFile;
    protected Variable latVar;
    protected Variable lonVar;
    protected Variable altVar;
    protected Variable timeVar;
    protected StructureDS innerSequence;
    protected StructureDS outerSequence;
    protected boolean isProfile;
    protected boolean fatal;
    private StructureMembers.Member latMember;
    private StructureMembers.Member lonMember;
    private StructureMembers.Member innerMember;
    private StructureMembers.Member altMember;
    private StructureMembers.Member timeMember;
    
    public static boolean isValidFile(final NetcdfFile ds) {
        final String conv = ds.findAttValueIgnoreCase(null, "Conventions", null);
        if (conv == null) {
            return false;
        }
        final StringTokenizer stoke = new StringTokenizer(conv, ",");
        while (stoke.hasMoreTokens()) {
            final String toke = stoke.nextToken().trim();
            if (toke.equalsIgnoreCase("epic-insitu-1.0")) {
                return true;
            }
        }
        return false;
    }
    
    public static PointObsDataset factory(final NetcdfDataset ds) throws IOException {
        Variable latVar = null;
        Variable timeVar = null;
        final List axes = ds.getCoordinateAxes();
        for (int i = 0; i < axes.size(); ++i) {
            final CoordinateAxis axis = axes.get(i);
            if (axis.getAxisType() == AxisType.Lat) {
                latVar = axis;
            }
            if (axis.getAxisType() == AxisType.Time) {
                timeVar = axis;
            }
        }
        final Structure outerSequence = getWrappingParent(ds, latVar);
        final boolean isProfile = getWrappingParent(ds, timeVar) == outerSequence;
        if (isProfile) {
            return new DapperPointDataset(ds);
        }
        return new DapperStationDataset(ds);
    }
    
    private static StructureDS getWrappingParent(final NetcdfDataset ds, final Variable v) {
        final String name = v.getParentStructure().getName();
        return (StructureDS)ds.findVariable(name);
    }
    
    public boolean isMine(final NetcdfDataset ds) {
        return isValidFile(ds);
    }
    
    public TypedDataset open(final NetcdfDataset ncd, final CancelTask task, final StringBuilder errlog) throws IOException {
        return new DapperDataset(ncd);
    }
    
    public DapperDataset() {
        this.isProfile = false;
        this.fatal = false;
    }
    
    public DapperDataset(final NetcdfDataset ds) throws IOException {
        super(ds);
        this.isProfile = false;
        this.fatal = false;
        final List axes = ds.getCoordinateAxes();
        for (int i = 0; i < axes.size(); ++i) {
            final CoordinateAxis axis = axes.get(i);
            if (axis.getAxisType() == AxisType.Lat) {
                this.latVar = axis;
            }
            if (axis.getAxisType() == AxisType.Lon) {
                this.lonVar = axis;
            }
            if (axis.getAxisType() == AxisType.Height) {
                this.altVar = axis;
            }
            if (axis.getAxisType() == AxisType.Time) {
                this.timeVar = axis;
            }
        }
        if (this.latVar == null) {
            this.parseInfo.append("Missing latitude variable");
            this.fatal = true;
        }
        if (this.lonVar == null) {
            this.parseInfo.append("Missing longitude variable");
            this.fatal = true;
        }
        if (this.altVar == null) {
            this.parseInfo.append("Missing altitude variable");
        }
        if (this.timeVar == null) {
            this.parseInfo.append("Missing time variable");
            this.fatal = true;
        }
        this.outerSequence = getWrappingParent(ds, this.latVar);
        final boolean isProfile = getWrappingParent(ds, this.timeVar) == this.outerSequence;
        this.innerSequence = (isProfile ? getWrappingParent(ds, this.altVar) : getWrappingParent(ds, this.timeVar));
        NetcdfFile refFile = ds.getReferencedFile();
        while (this.dodsFile == null) {
            if (refFile instanceof DODSNetcdfFile) {
                this.dodsFile = (DODSNetcdfFile)refFile;
            }
            else {
                if (!(refFile instanceof NetcdfDataset)) {
                    throw new IllegalArgumentException("Must be a DODSNetcdfFile");
                }
                refFile = ((NetcdfDataset)refFile).getReferencedFile();
            }
        }
        List recordMembers = this.outerSequence.getVariables();
        for (int j = 0; j < recordMembers.size(); ++j) {
            final Variable v = recordMembers.get(j);
            this.dataVariables.add(v);
        }
        recordMembers = this.innerSequence.getVariables();
        for (int j = 0; j < recordMembers.size(); ++j) {
            final Variable v = recordMembers.get(j);
            this.dataVariables.add(v);
        }
        this.dataVariables.remove(this.latVar);
        this.dataVariables.remove(this.lonVar);
        this.dataVariables.remove(this.altVar);
        this.dataVariables.remove(this.timeVar);
        this.dataVariables.remove(this.innerSequence);
        this.dataVariables.remove(ds.findVariable("_id"));
        this.dataVariables.remove(ds.findVariable("attributes"));
        this.dataVariables.remove(ds.findVariable("variable_attributes"));
        this.setBoundingBox();
        try {
            this.timeUnit = new DateUnit(this.timeVar.getUnitsString());
        }
        catch (Exception e) {
            this.parseInfo.append("Bad time units= " + this.timeVar.getUnitsString());
            this.fatal = true;
        }
        final Attribute time_range = this.ncfile.findGlobalAttribute("time_range");
        final double time_start = time_range.getNumericValue(0).doubleValue();
        final double time_end = time_range.getNumericValue(1).doubleValue();
        this.startDate = this.timeUnit.makeDate(time_start);
        this.endDate = this.timeUnit.makeDate(time_end);
        this.title = ds.findAttValueIgnoreCase(null, "title", "");
        this.desc = ds.findAttValueIgnoreCase(null, "description", "");
    }
    
    @Override
    protected void setTimeUnits() {
    }
    
    @Override
    protected void setStartDate() {
    }
    
    @Override
    protected void setEndDate() {
    }
    
    @Override
    protected void setBoundingBox() {
        final Attribute lon_range = this.ncfile.findGlobalAttribute("lon_range");
        final double lon_start = lon_range.getNumericValue(0).doubleValue();
        final double lon_end = lon_range.getNumericValue(1).doubleValue();
        final Attribute lat_range = this.ncfile.findGlobalAttribute("lat_range");
        final double lat_start = lat_range.getNumericValue(0).doubleValue();
        final double lat_end = lat_range.getNumericValue(1).doubleValue();
        this.boundingBox = new LatLonRect(new LatLonPointImpl(lat_start, lon_start), new LatLonPointImpl(lat_end, lon_end));
    }
    
    public int getDataCount() {
        return -1;
    }
    
    public List getData(final CancelTask cancel) throws IOException {
        final String CE = this.outerSequence.getName();
        final ArrayStructure as = (ArrayStructure)this.dodsFile.readWithCE(this.outerSequence, CE);
        this.extractMembers(as);
        final int n = (int)as.getSize();
        final List<SeqPointObs> dataList = new ArrayList<SeqPointObs>(n);
        for (int i = 0; i < n; ++i) {
            dataList.add(new SeqPointObs(i, as.getStructureData(i)));
        }
        return dataList;
    }
    
    public List getData(final LatLonRect boundingBox, final CancelTask cancel) throws IOException {
        final String CE = this.outerSequence.getName() + "&" + this.makeBB(boundingBox);
        final ArrayStructure as = (ArrayStructure)this.dodsFile.readWithCE(this.outerSequence, CE);
        this.extractMembers(as);
        final int n = (int)as.getSize();
        final ArrayList dataList = new ArrayList(n);
        for (int i = 0; i < n; ++i) {
            dataList.add(new SeqPointObs(i, as.getStructureData(i)));
        }
        return dataList;
    }
    
    public List getData(final LatLonRect boundingBox, final Date start, final Date end, final CancelTask cancel) throws IOException {
        final String CE = this.outerSequence.getName() + "&" + this.makeBB(boundingBox) + "&" + this.makeTimeRange(start, end);
        final ArrayStructure as = (ArrayStructure)this.dodsFile.readWithCE(this.outerSequence, CE);
        this.extractMembers(as);
        final int n = (int)as.getSize();
        final ArrayList dataList = new ArrayList(n);
        for (int i = 0; i < n; ++i) {
            dataList.add(new SeqPointObs(i, as.getStructureData(i)));
        }
        return dataList;
    }
    
    private String makeBB(final LatLonRect bb) {
        return this.latVar.getName() + ">=" + bb.getLowerLeftPoint().getLatitude() + "&" + this.latVar.getName() + "<=" + bb.getUpperRightPoint().getLatitude() + "&" + this.lonVar.getName() + ">=" + bb.getLowerLeftPoint().getLongitude() + "&" + this.lonVar.getName() + "<=" + bb.getUpperRightPoint().getLongitude();
    }
    
    private String makeTimeRange(final Date start, final Date end) {
        final double startValue = this.timeUnit.makeValue(start);
        final double endValue = this.timeUnit.makeValue(end);
        return this.timeVar.getName() + ">=" + startValue + "&" + this.timeVar.getName() + "<=" + endValue;
    }
    
    private void extractMembers(final ArrayStructure as) {
        final StructureMembers members = as.getStructureMembers();
        this.latMember = members.findMember(this.latVar.getShortName());
        this.lonMember = members.findMember(this.lonVar.getShortName());
        this.innerMember = members.findMember(this.innerSequence.getShortName());
        final StructureData first = as.getStructureData(0);
        final StructureData innerFirst = first.getScalarStructure(this.innerMember);
        final StructureMembers innerMembers = innerFirst.getStructureMembers();
        if (this.isProfile) {
            this.timeMember = members.findMember(this.timeVar.getShortName());
            this.altMember = innerMembers.findMember(this.altVar.getShortName());
        }
        else {
            this.timeMember = innerMembers.findMember(this.timeVar.getShortName());
            this.altMember = members.findMember(this.altVar.getShortName());
        }
    }
    
    public void readStations(final List stations) throws IOException {
        final String CE = this.latVar.getShortName() + "," + this.lonVar.getShortName() + "," + this.altVar.getShortName() + "," + "_id";
        final ArrayStructure as = (ArrayStructure)this.dodsFile.readWithCE(this.outerSequence, CE);
        final StructureMembers members = as.getStructureMembers();
        final StructureMembers.Member latMember = members.findMember(this.latVar.getShortName());
        final StructureMembers.Member lonMember = members.findMember(this.lonVar.getShortName());
        final StructureMembers.Member altMember = members.findMember(this.altVar.getShortName());
        final StructureMembers.Member idMember = members.findMember("_id");
        for (int n = (int)as.getSize(), i = 0; i < n; ++i) {
            final StructureData sdata = as.getStructureData(i);
            final double lat = sdata.convertScalarDouble(latMember);
            final double lon = sdata.convertScalarDouble(lonMember);
            final double alt = sdata.convertScalarDouble(altMember);
            final int id = sdata.getScalarInt(idMember);
            final StationImpl s = new StationImpl(Integer.toString(id), "Station" + i, lat, lon, alt);
            stations.add(s);
        }
    }
    
    public List readStationData(final Station s, final CancelTask cancel) throws IOException {
        final String CE = this.outerSequence.getShortName() + "." + this.innerSequence.getShortName() + "&" + this.outerSequence.getShortName() + "." + "_id" + "=" + s.getName();
        final ArrayStructure as = (ArrayStructure)this.dodsFile.readWithCE(this.outerSequence, CE);
        final StructureData outerStructure = as.getStructureData(0);
        final ArrayStructure asInner = (ArrayStructure)outerStructure.getArray(this.innerSequence.getShortName());
        final StructureMembers innerMembers = asInner.getStructureMembers();
        final StructureMembers.Member timeMember = innerMembers.findMember(this.timeVar.getShortName());
        final int n = (int)asInner.getSize();
        final ArrayList stationData = new ArrayList(n);
        for (int i = 0; i < n; ++i) {
            final StructureData sdata = asInner.getStructureData(i);
            final double obsTime = sdata.convertScalarDouble(timeMember);
            stationData.add(new SeqStationObs(s, obsTime, sdata));
        }
        return stationData;
    }
    
    public DataIterator getDataIterator(final int bufferSize) throws IOException {
        return new DataIteratorAdapter(this.getData((CancelTask)null).iterator());
    }
    
    public static void main(final String[] args) throws IOException {
        final String url = "http://dapper.pmel.noaa.gov/dapper/epic/woce_sl_time_monthly.cdp";
        final NetcdfDataset ncd = NetcdfDataset.openDataset(url);
        factory(ncd);
    }
    
    public class SeqPointObs extends PointObsDatatypeImpl
    {
        protected int recno;
        protected LatLonPointImpl llpt;
        protected StructureData sdata;
        
        protected SeqPointObs(final EarthLocation location, final double obsTime, final double nomTime, final int recno) {
            super(location, obsTime, nomTime);
            this.llpt = null;
            this.recno = recno;
        }
        
        public SeqPointObs(final int recno, final StructureData sdata) {
            this.llpt = null;
            this.recno = recno;
            this.sdata = sdata;
            final double lat = sdata.convertScalarDouble(DapperDataset.this.latMember);
            final double lon = sdata.convertScalarDouble(DapperDataset.this.lonMember);
            final StructureData inner = sdata.getScalarStructure(DapperDataset.this.innerMember);
            double alt = 0.0;
            if (DapperDataset.this.isProfile) {
                this.obsTime = sdata.convertScalarDouble(DapperDataset.this.timeMember);
                alt = inner.convertScalarDouble(DapperDataset.this.altMember);
            }
            else {
                this.obsTime = inner.convertScalarDouble(DapperDataset.this.timeMember);
                alt = sdata.convertScalarDouble(DapperDataset.this.altMember);
            }
            this.nomTime = this.obsTime;
            this.location = new EarthLocationImpl(lat, lon, alt);
        }
        
        public LatLonPoint getLatLon() {
            if (this.llpt == null) {
                this.llpt = new LatLonPointImpl(this.location.getLatitude(), this.location.getLongitude());
            }
            return this.llpt;
        }
        
        public Date getNominalTimeAsDate() {
            return DapperDataset.this.timeUnit.makeDate(this.getNominalTime());
        }
        
        public Date getObservationTimeAsDate() {
            return DapperDataset.this.timeUnit.makeDate(this.getObservationTime());
        }
        
        public StructureData getData() throws IOException {
            return this.sdata;
        }
    }
    
    public class SeqStationObs extends StationObsDatatypeImpl
    {
        protected StructureData sdata;
        
        public SeqStationObs(final Station s, final double obsTime, final StructureData sdata) {
            super(s, obsTime, obsTime);
            this.sdata = sdata;
        }
        
        public Date getNominalTimeAsDate() {
            return DapperDataset.this.timeUnit.makeDate(this.getNominalTime());
        }
        
        public Date getObservationTimeAsDate() {
            return DapperDataset.this.timeUnit.makeDate(this.getObservationTime());
        }
        
        public StructureData getData() throws IOException {
            return this.sdata;
        }
    }
    
    private static class DapperPointDataset extends PointObsDatasetImpl
    {
        DapperDataset dd;
        
        DapperPointDataset(final NetcdfDataset ds) throws IOException {
            super(ds);
            this.dd = new DapperDataset(ds);
        }
        
        @Override
        protected void setTimeUnits() {
        }
        
        @Override
        protected void setStartDate() {
        }
        
        @Override
        protected void setEndDate() {
        }
        
        @Override
        protected void setBoundingBox() {
        }
        
        public List getData(final CancelTask cancel) throws IOException {
            return this.dd.getData(cancel);
        }
        
        public int getDataCount() {
            return this.dd.getDataCount();
        }
        
        public List getData(final LatLonRect boundingBox, final CancelTask cancel) throws IOException {
            return this.dd.getData(boundingBox, cancel);
        }
        
        public List getData(final LatLonRect boundingBox, final Date start, final Date end, final CancelTask cancel) throws IOException {
            return this.dd.getData(boundingBox, start, end, cancel);
        }
        
        public DataIterator getDataIterator(final int bufferSize) throws IOException {
            return this.dd.getDataIterator(bufferSize);
        }
    }
    
    private static class DapperStationDataset extends StationObsDatasetImpl
    {
        DapperDataset dd;
        
        DapperStationDataset(final NetcdfDataset ds) throws IOException {
            super(ds);
            (this.dd = new DapperDataset(ds)).readStations(this.stations);
        }
        
        public List getData(final Station s, final CancelTask cancel) throws IOException {
            return this.dd.readStationData(s, cancel);
        }
        
        @Override
        protected void setTimeUnits() {
        }
        
        @Override
        protected void setStartDate() {
        }
        
        @Override
        protected void setEndDate() {
        }
        
        @Override
        protected void setBoundingBox() {
        }
        
        public List getData(final CancelTask cancel) throws IOException {
            return this.dd.getData(cancel);
        }
        
        public int getDataCount() {
            return this.dd.getDataCount();
        }
        
        @Override
        public List getData(final LatLonRect boundingBox, final CancelTask cancel) throws IOException {
            return this.dd.getData(boundingBox, cancel);
        }
        
        @Override
        public List getData(final LatLonRect boundingBox, final Date start, final Date end, final CancelTask cancel) throws IOException {
            return this.dd.getData(boundingBox, start, end, cancel);
        }
        
        public DataIterator getDataIterator(final int bufferSize) throws IOException {
            return this.dd.getDataIterator(bufferSize);
        }
    }
}
