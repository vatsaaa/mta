// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.dt.point;

import ucar.unidata.geoloc.EarthLocation;
import ucar.nc2.dt.PointObsDatatype;

public abstract class PointObsDatatypeImpl implements PointObsDatatype, Comparable
{
    protected EarthLocation location;
    protected double obsTime;
    protected double nomTime;
    
    public PointObsDatatypeImpl() {
    }
    
    public PointObsDatatypeImpl(final EarthLocation location, final double obsTime, final double nomTime) {
        this.location = location;
        this.obsTime = obsTime;
        this.nomTime = nomTime;
    }
    
    public EarthLocation getLocation() {
        return this.location;
    }
    
    public double getNominalTime() {
        return this.nomTime;
    }
    
    public double getObservationTime() {
        return this.obsTime;
    }
    
    public int compareTo(final Object o) {
        final PointObsDatatypeImpl other = (PointObsDatatypeImpl)o;
        if (this.obsTime < other.obsTime) {
            return -1;
        }
        if (this.obsTime > other.obsTime) {
            return 1;
        }
        return 0;
    }
}
