// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.dt.point;

import java.util.Iterator;
import java.text.ParseException;
import ucar.ma2.StructureData;
import ucar.nc2.dt.StationObsDatatype;
import ucar.nc2.units.DateFormatter;
import java.util.Date;
import ucar.nc2.dt.DataIterator;
import ucar.unidata.geoloc.Station;
import ucar.unidata.geoloc.LatLonRect;
import java.util.List;
import java.io.IOException;
import ucar.nc2.util.CancelTask;
import ucar.nc2.dt.TypedDatasetFactory;
import ucar.nc2.constants.FeatureType;
import ucar.nc2.dt.StationObsDataset;
import java.util.ArrayList;

public class StationDatasetCollection
{
    private boolean debug;
    private ArrayList sobsList;
    private StationObsDataset typical;
    private StringBuilder log;
    
    public StationDatasetCollection() {
        this.debug = false;
        this.sobsList = new ArrayList();
        this.typical = null;
        this.log = new StringBuilder();
    }
    
    public void add(final String location) throws IOException {
        final StationObsDataset sobs = (StationObsDataset)TypedDatasetFactory.open(FeatureType.STATION, location, null, this.log);
        if (this.typical == null) {
            this.typical = sobs;
        }
        this.sobsList.add(sobs);
    }
    
    public List getStations() throws IOException {
        return this.typical.getStations();
    }
    
    public List getStations(final LatLonRect boundingBox) throws IOException {
        return this.typical.getStations(boundingBox);
    }
    
    public Station getStation(final String name) {
        return this.typical.getStation(name);
    }
    
    public DataIterator getDataIterator(final Station s) throws IOException {
        return new StationDataIterator(s);
    }
    
    public DataIterator getDataIterator(final Station s, final Date start, final Date end) throws IOException {
        return new StationDateDataIterator(s, start, end);
    }
    
    public static void main(final String[] args) throws IOException, ParseException {
        final StationDatasetCollection sdc = new StationDatasetCollection();
        final DateFormatter format = new DateFormatter();
        sdc.add("C:/data/metars/Surface_METAR_20070326_0000.nc");
        sdc.add("C:/data/metars/Surface_METAR_20070329_0000.nc");
        sdc.add("C:/data/metars/Surface_METAR_20070330_0000.nc");
        sdc.add("C:/data/metars/Surface_METAR_20070331_0000.nc");
        final Station s = sdc.getStation("ACK");
        DataIterator iter = sdc.getDataIterator(s);
        while (iter.hasNext()) {
            final Object o = iter.nextData();
            assert o instanceof StationObsDatatype;
            final StationObsDatatype sod = (StationObsDatatype)o;
            final Station ss = sod.getStation();
            assert ss.getName().equals(s.getName());
            System.out.println(ss.getName() + " " + format.toDateTimeStringISO(sod.getObservationTimeAsDate()));
            final StructureData sdata = sod.getData();
            assert sdata != null;
        }
        System.out.println("------------------\n");
        final Date start = format.isoDateTimeFormat("2007-03-27T09:18:56Z");
        final Date end = format.isoDateTimeFormat("2007-03-30T10:52:48Z");
        iter = sdc.getDataIterator(s, start, end);
        while (iter.hasNext()) {
            final Object o2 = iter.nextData();
            assert o2 instanceof StationObsDatatype;
            final StationObsDatatype sod2 = (StationObsDatatype)o2;
            final Station ss2 = sod2.getStation();
            assert ss2.getName().equals(s.getName());
            System.out.println(ss2.getName() + " " + format.toDateTimeStringISO(sod2.getObservationTimeAsDate()));
            final StructureData sdata2 = sod2.getData();
            assert sdata2 != null;
        }
    }
    
    private class StationDataIterator implements DataIterator
    {
        String stationName;
        Iterator iterSobs;
        DataIterator dataIter;
        
        StationDataIterator(final Station s) throws IOException {
            this.stationName = s.getName();
            this.iterSobs = StationDatasetCollection.this.sobsList.iterator();
        }
        
        public boolean hasNext() {
            if (this.dataIter == null) {
                this.dataIter = this.getNextDataIterator();
            }
            if (this.dataIter == null) {
                return false;
            }
            if (this.dataIter.hasNext()) {
                return true;
            }
            this.dataIter = this.getNextDataIterator();
            return this.dataIter != null && this.hasNext();
        }
        
        public Object nextData() throws IOException {
            return this.dataIter.nextData();
        }
        
        public Object next() {
            return this.dataIter.next();
        }
        
        public void remove() {
            throw new UnsupportedOperationException();
        }
        
        private DataIterator getNextDataIterator() {
            if (!this.iterSobs.hasNext()) {
                return null;
            }
            final StationObsDataset sobs = this.iterSobs.next();
            final DataIterator dataIter = this.makeDataIterator(sobs);
            if (StationDatasetCollection.this.debug && dataIter != null) {
                System.out.println("next sobs =" + sobs.getLocationURI());
            }
            return (dataIter == null) ? this.getNextDataIterator() : dataIter;
        }
        
        protected DataIterator makeDataIterator(final StationObsDataset sobs) {
            final Station s = sobs.getStation(this.stationName);
            if (s == null) {
                return null;
            }
            return sobs.getDataIterator(s);
        }
    }
    
    private class StationDateDataIterator extends StationDataIterator
    {
        private Date want_start;
        private Date want_end;
        
        StationDateDataIterator(final Station s, final Date start, final Date end) throws IOException {
            super(s);
            this.want_start = start;
            this.want_end = end;
        }
        
        @Override
        protected DataIterator makeDataIterator(final StationObsDataset sobs) {
            final Date start = sobs.getStartDate();
            if (start.after(this.want_end)) {
                return null;
            }
            final Date end = sobs.getEndDate();
            if (end.before(this.want_start)) {
                return null;
            }
            final Station s = sobs.getStation(this.stationName);
            if (s == null) {
                return null;
            }
            return sobs.getDataIterator(s, start, end);
        }
    }
}
