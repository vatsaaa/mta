// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.dt.point;

import ucar.unidata.geoloc.LatLonPoint;
import ucar.unidata.geoloc.EarthLocationImpl;
import ucar.unidata.geoloc.EarthLocation;
import ucar.unidata.geoloc.LatLonPointImpl;
import java.util.Iterator;
import ucar.nc2.dt.DataIteratorAdapter;
import ucar.nc2.dt.DataIterator;
import ucar.ma2.StructureData;
import java.util.Date;
import ucar.unidata.geoloc.LatLonRect;
import java.io.IOException;
import java.util.ArrayList;
import ucar.ma2.ArrayStructure;
import ucar.nc2.util.CancelTask;
import java.util.List;
import ucar.nc2.dataset.NetcdfDataset;
import ucar.ma2.StructureMembers;
import ucar.nc2.units.DateFormatter;
import ucar.nc2.units.DateUnit;
import ucar.nc2.Variable;
import ucar.nc2.dataset.StructureDS;
import ucar.nc2.dods.DODSNetcdfFile;
import ucar.nc2.NetcdfFile;

public class SequenceHelper
{
    protected NetcdfFile ncfile;
    protected DODSNetcdfFile dodsFile;
    protected StructureDS sequenceOuter;
    protected StructureDS sequenceInner;
    protected Variable latVar;
    protected Variable lonVar;
    protected Variable altVar;
    protected Variable timeVar;
    protected boolean isProfile;
    protected DateUnit timeUnit;
    protected DateFormatter formatter;
    private StructureMembers.Member latMember;
    private StructureMembers.Member lonMember;
    private StructureMembers.Member innerMember;
    private StructureMembers.Member altMember;
    private StructureMembers.Member timeMember;
    
    public SequenceHelper(final NetcdfDataset ncfile, final boolean isProfile, final StructureDS sequenceOuter, final StructureDS sequenceInner, final Variable latVar, final Variable lonVar, final Variable altVar, final Variable timeVar, final List typedDataVariables, final StringBuffer errBuffer) {
        this.ncfile = ncfile;
        this.isProfile = isProfile;
        this.sequenceOuter = sequenceOuter;
        this.sequenceInner = sequenceInner;
        this.latVar = latVar;
        this.lonVar = lonVar;
        this.altVar = altVar;
        this.timeVar = timeVar;
        NetcdfFile refFile = ncfile.getReferencedFile();
        while (this.dodsFile == null) {
            if (refFile instanceof DODSNetcdfFile) {
                this.dodsFile = (DODSNetcdfFile)refFile;
            }
            else {
                if (!(refFile instanceof NetcdfDataset)) {
                    throw new IllegalArgumentException("Must be a DODSNetcdfFile");
                }
                refFile = ((NetcdfDataset)refFile).getReferencedFile();
            }
        }
        List recordMembers = sequenceOuter.getVariables();
        for (int i = 0; i < recordMembers.size(); ++i) {
            final Variable v = recordMembers.get(i);
            typedDataVariables.add(v);
        }
        recordMembers = sequenceInner.getVariables();
        for (int i = 0; i < recordMembers.size(); ++i) {
            final Variable v = recordMembers.get(i);
            typedDataVariables.add(v);
        }
        typedDataVariables.remove(latVar);
        typedDataVariables.remove(lonVar);
        typedDataVariables.remove(altVar);
        typedDataVariables.remove(timeVar);
        typedDataVariables.remove(sequenceInner);
    }
    
    public void setTimeUnit(final DateUnit timeUnit) {
        this.timeUnit = timeUnit;
    }
    
    public DateUnit getTimeUnit() {
        return this.timeUnit;
    }
    
    public List getData(final CancelTask cancel) throws IOException {
        final String CE = this.sequenceOuter.getName();
        final ArrayStructure as = (ArrayStructure)this.dodsFile.readWithCE(this.sequenceOuter, CE);
        this.extractMembers(as);
        final int n = (int)as.getSize();
        final ArrayList dataList = new ArrayList(n);
        for (int i = 0; i < n; ++i) {
            dataList.add(new SeqPointObs(i, as.getStructureData(i)));
        }
        return dataList;
    }
    
    public List getData(final LatLonRect boundingBox, final CancelTask cancel) throws IOException {
        final String CE = this.sequenceOuter.getName() + "&" + this.makeBB(boundingBox);
        final ArrayStructure as = (ArrayStructure)this.dodsFile.readWithCE(this.sequenceOuter, CE);
        this.extractMembers(as);
        final int n = (int)as.getSize();
        final ArrayList dataList = new ArrayList(n);
        for (int i = 0; i < n; ++i) {
            dataList.add(new SeqPointObs(i, as.getStructureData(i)));
        }
        return dataList;
    }
    
    public List getData(final LatLonRect boundingBox, final Date start, final Date end, final CancelTask cancel) throws IOException {
        final String CE = this.sequenceOuter.getName() + "&" + this.makeBB(boundingBox) + "&" + this.makeTimeRange(start, end);
        final ArrayStructure as = (ArrayStructure)this.dodsFile.readWithCE(this.sequenceOuter, CE);
        this.extractMembers(as);
        final int n = (int)as.getSize();
        final ArrayList dataList = new ArrayList(n);
        for (int i = 0; i < n; ++i) {
            dataList.add(new SeqPointObs(i, as.getStructureData(i)));
        }
        return dataList;
    }
    
    private String makeBB(final LatLonRect bb) {
        return this.latVar.getName() + ">=" + bb.getLowerLeftPoint().getLatitude() + "&" + this.latVar.getName() + "<=" + bb.getUpperRightPoint().getLatitude() + "&" + this.lonVar.getName() + ">=" + bb.getLowerLeftPoint().getLongitude() + "&" + this.lonVar.getName() + "<=" + bb.getUpperRightPoint().getLongitude();
    }
    
    private String makeTimeRange(final Date start, final Date end) {
        final double startValue = this.timeUnit.makeValue(start);
        final double endValue = this.timeUnit.makeValue(end);
        return this.timeVar.getName() + ">=" + startValue + "&" + this.timeVar.getName() + "<=" + endValue;
    }
    
    private void extractMembers(final ArrayStructure as) {
        final StructureMembers members = as.getStructureMembers();
        this.latMember = members.findMember(this.latVar.getShortName());
        this.lonMember = members.findMember(this.lonVar.getShortName());
        this.innerMember = members.findMember(this.sequenceInner.getShortName());
        final StructureData first = as.getStructureData(0);
        final StructureData innerFirst = first.getScalarStructure(this.innerMember);
        final StructureMembers innerMembers = innerFirst.getStructureMembers();
        if (this.isProfile) {
            this.timeMember = members.findMember(this.timeVar.getShortName());
            this.altMember = innerMembers.findMember(this.altVar.getShortName());
        }
        else {
            this.timeMember = innerMembers.findMember(this.timeVar.getShortName());
            this.altMember = members.findMember(this.altVar.getShortName());
        }
    }
    
    public DataIterator getDataIterator(final int bufferSize) throws IOException {
        return new DataIteratorAdapter(this.getData(null).iterator());
    }
    
    public class SeqPointObs extends PointObsDatatypeImpl
    {
        protected int recno;
        protected LatLonPointImpl llpt;
        protected StructureData sdata;
        
        protected SeqPointObs(final EarthLocation location, final double obsTime, final double nomTime, final int recno) {
            super(location, obsTime, nomTime);
            this.llpt = null;
            this.recno = recno;
        }
        
        public SeqPointObs(final int recno, final StructureData sdata) {
            this.llpt = null;
            this.recno = recno;
            this.sdata = sdata;
            final double lat = sdata.convertScalarDouble(SequenceHelper.this.latMember);
            final double lon = sdata.convertScalarDouble(SequenceHelper.this.lonMember);
            final StructureData inner = sdata.getScalarStructure(SequenceHelper.this.innerMember);
            double alt = 0.0;
            if (SequenceHelper.this.isProfile) {
                this.obsTime = sdata.convertScalarDouble(SequenceHelper.this.timeMember);
                alt = inner.convertScalarDouble(SequenceHelper.this.altMember);
            }
            else {
                this.obsTime = inner.convertScalarDouble(SequenceHelper.this.timeMember);
                alt = sdata.convertScalarDouble(SequenceHelper.this.altMember);
            }
            this.nomTime = this.obsTime;
            this.location = new EarthLocationImpl(lat, lon, alt);
        }
        
        public LatLonPoint getLatLon() {
            if (this.llpt == null) {
                this.llpt = new LatLonPointImpl(this.location.getLatitude(), this.location.getLongitude());
            }
            return this.llpt;
        }
        
        public Date getNominalTimeAsDate() {
            return SequenceHelper.this.timeUnit.makeDate(this.getNominalTime());
        }
        
        public Date getObservationTimeAsDate() {
            return SequenceHelper.this.timeUnit.makeDate(this.getObservationTime());
        }
        
        public StructureData getData() throws IOException {
            return this.sdata;
        }
    }
}
