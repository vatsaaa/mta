// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.dt.point;

import java.text.ParseException;
import ucar.ma2.DataType;
import ucar.ma2.StructureData;
import ucar.nc2.Variable;
import java.util.Date;
import ucar.unidata.geoloc.LatLonRect;
import java.io.IOException;
import ucar.nc2.util.CancelTask;
import java.util.List;
import ucar.nc2.dt.PointObsDatatype;
import ucar.nc2.constants.FeatureType;
import ucar.nc2.dataset.NetcdfDataset;
import ucar.nc2.units.SimpleUnit;
import ucar.nc2.units.DateFormatter;
import ucar.nc2.units.DateUnit;
import ucar.nc2.dt.PointObsDataset;
import ucar.nc2.dt.TypedDatasetImpl;

public abstract class PointObsDatasetImpl extends TypedDatasetImpl implements PointObsDataset
{
    protected DateUnit timeUnit;
    protected DateFormatter formatter;
    
    protected static double getMetersConversionFactor(final String unitsString) throws Exception {
        final SimpleUnit unit = SimpleUnit.factoryWithExceptions(unitsString);
        return unit.convertTo(1.0, SimpleUnit.meterUnit);
    }
    
    public PointObsDatasetImpl() {
    }
    
    public PointObsDatasetImpl(final String title, final String description, final String location) {
        super(title, description, location);
    }
    
    public PointObsDatasetImpl(final NetcdfDataset ncfile) {
        super(ncfile);
    }
    
    protected abstract void setTimeUnits();
    
    @Override
    public String getDetailInfo() {
        final StringBuffer sbuff = new StringBuffer();
        sbuff.append("PointObsDataset\n");
        sbuff.append("  adapter   = ").append(this.getClass().getName()).append("\n");
        sbuff.append("  timeUnit  = ").append(this.getTimeUnits()).append("\n");
        sbuff.append("  dataClass = ").append(this.getDataClass()).append("\n");
        sbuff.append("  dataCount = ").append(this.getDataCount()).append("\n");
        sbuff.append(super.getDetailInfo());
        return sbuff.toString();
    }
    
    public FeatureType getScientificDataType() {
        return FeatureType.POINT;
    }
    
    public Class getDataClass() {
        return PointObsDatatype.class;
    }
    
    public DateUnit getTimeUnits() {
        return this.timeUnit;
    }
    
    public List getData() throws IOException {
        return this.getData((CancelTask)null);
    }
    
    public List getData(final LatLonRect boundingBox) throws IOException {
        return this.getData(boundingBox, null);
    }
    
    public List getData(final LatLonRect boundingBox, final Date start, final Date end) throws IOException {
        return this.getData(boundingBox, start, end, null);
    }
    
    protected double getTime(final Variable timeVar, final StructureData sdata) throws ParseException {
        if (timeVar == null) {
            return 0.0;
        }
        if (timeVar.getDataType() == DataType.CHAR || timeVar.getDataType() == DataType.STRING) {
            final String time = sdata.getScalarString(timeVar.getShortName());
            if (null == this.formatter) {
                this.formatter = new DateFormatter();
            }
            final Date date = this.formatter.isoDateTimeFormat(time);
            return date.getTime() / 1000.0;
        }
        return sdata.convertScalarFloat(timeVar.getShortName());
    }
}
