// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.dt.point;

import java.io.File;
import java.util.zip.GZIPOutputStream;
import java.io.FileOutputStream;
import ucar.nc2.util.CancelTask;
import ucar.nc2.dt.TypedDatasetFactory;
import ucar.nc2.constants.FeatureType;
import java.util.Iterator;
import ucar.ma2.DataType;
import org.jdom.Namespace;
import ucar.nc2.ncml.NcMLWriter;
import ucar.nc2.Attribute;
import ucar.unidata.util.Parameter;
import ucar.nc2.dataset.CoordinateTransform;
import ucar.nc2.dataset.CoordinateAxis;
import ucar.nc2.dt.GridCoordSystem;
import java.util.Date;
import ucar.unidata.geoloc.LatLonRect;
import ucar.nc2.units.DateFormatter;
import ucar.nc2.VariableSimpleIF;
import java.util.Collections;
import java.util.List;
import org.jdom.Content;
import ucar.unidata.geoloc.Station;
import org.jdom.Element;
import org.jdom.Document;
import java.io.IOException;
import java.io.OutputStream;
import org.jdom.output.XMLOutputter;
import org.jdom.output.Format;
import ucar.nc2.dt.StationObsDataset;

public class StationObsDatasetInfo
{
    private StationObsDataset sobs;
    private String path;
    
    public StationObsDatasetInfo(final StationObsDataset sobs, final String path) {
        this.sobs = sobs;
        this.path = path;
    }
    
    public String writeStationObsDatasetXML() {
        final XMLOutputter fmt = new XMLOutputter(Format.getPrettyFormat());
        return fmt.outputString(this.makeStationObsDatasetDocument());
    }
    
    public void writeStationObsDatasetXML(final OutputStream os) throws IOException {
        final XMLOutputter fmt = new XMLOutputter(Format.getPrettyFormat());
        fmt.output(this.makeStationObsDatasetDocument(), os);
    }
    
    public String writeStationCollectionXML() throws IOException {
        final XMLOutputter fmt = new XMLOutputter(Format.getPrettyFormat());
        return fmt.outputString(this.makeStationCollectionDocument());
    }
    
    public void writeStationCollectionXML(final OutputStream os) throws IOException {
        final XMLOutputter fmt = new XMLOutputter(Format.getPrettyFormat());
        fmt.output(this.makeStationCollectionDocument(), os);
    }
    
    public Document makeStationCollectionDocument() throws IOException {
        final Element rootElem = new Element("stationCollection");
        final Document doc = new Document(rootElem);
        final List stns = this.sobs.getStations();
        System.out.println("nstns = " + stns.size());
        for (int i = 0; i < stns.size(); ++i) {
            final Station s = stns.get(i);
            final Element sElem = new Element("station");
            sElem.setAttribute("name", s.getName());
            if (s.getWmoId() != null) {
                sElem.setAttribute("wmo_id", s.getWmoId());
            }
            if (s.getDescription() != null) {
                sElem.addContent(new Element("description").addContent(s.getDescription()));
            }
            sElem.addContent(new Element("longitude").addContent(ucar.unidata.util.Format.d(s.getLongitude(), 6)));
            sElem.addContent(new Element("latitide").addContent(ucar.unidata.util.Format.d(s.getLatitude(), 6)));
            if (!Double.isNaN(s.getAltitude())) {
                sElem.addContent(new Element("altitude").addContent(ucar.unidata.util.Format.d(s.getAltitude(), 6)));
            }
            rootElem.addContent(sElem);
        }
        return doc;
    }
    
    public Document makeStationObsDatasetDocument() {
        final Element rootElem = new Element("stationObsDataset");
        final Document doc = new Document(rootElem);
        rootElem.setAttribute("location", this.sobs.getLocationURI());
        if (null != this.path) {
            rootElem.setAttribute("path", this.path);
        }
        final List vars = this.sobs.getDataVariables();
        Collections.sort((List<Comparable>)vars);
        for (int i = 0; i < vars.size(); ++i) {
            final VariableSimpleIF v = vars.get(i);
            rootElem.addContent(this.writeVariable(v));
        }
        final LatLonRect bb = this.sobs.getBoundingBox();
        if (bb != null) {
            rootElem.addContent(this.writeBoundingBox(bb));
        }
        final Date start = this.sobs.getStartDate();
        final Date end = this.sobs.getEndDate();
        if (start != null && end != null) {
            final DateFormatter format = new DateFormatter();
            final Element dateRange = new Element("TimeSpan");
            dateRange.addContent(new Element("begin").addContent(format.toDateTimeStringISO(start)));
            dateRange.addContent(new Element("end").addContent(format.toDateTimeStringISO(end)));
            rootElem.addContent(dateRange);
        }
        final Element elem = new Element("AcceptList");
        elem.addContent(new Element("accept").addContent("raw"));
        elem.addContent(new Element("accept").addContent("xml"));
        elem.addContent(new Element("accept").addContent("csv"));
        elem.addContent(new Element("accept").addContent("netcdf"));
        elem.addContent(new Element("accept").addContent("netcdfStream"));
        rootElem.addContent(elem);
        return doc;
    }
    
    private Element writeBoundingBox(final LatLonRect bb) {
        final Element bbElem = new Element("LatLonBox");
        bbElem.addContent(new Element("west").addContent(ucar.unidata.util.Format.dfrac(bb.getLonMin(), 4)));
        bbElem.addContent(new Element("east").addContent(ucar.unidata.util.Format.dfrac(bb.getLonMax(), 4)));
        bbElem.addContent(new Element("south").addContent(ucar.unidata.util.Format.dfrac(bb.getLatMin(), 4)));
        bbElem.addContent(new Element("north").addContent(ucar.unidata.util.Format.dfrac(bb.getLatMax(), 4)));
        return bbElem;
    }
    
    private Element writeCoordSys(final GridCoordSystem cs) {
        final Element csElem = new Element("coordSys");
        csElem.setAttribute("name", cs.getName());
        final List axes = cs.getCoordinateAxes();
        for (int i = 0; i < axes.size(); ++i) {
            final CoordinateAxis axis = axes.get(i);
            final Element axisElem = new Element("axisRef");
            axisElem.setAttribute("name", axis.getName());
            csElem.addContent(axisElem);
        }
        final List cts = cs.getCoordinateTransforms();
        for (int j = 0; j < cts.size(); ++j) {
            final CoordinateTransform ct = cts.get(j);
            final Element elem = new Element("coordTransRef");
            elem.setAttribute("name", ct.getName());
            csElem.addContent(elem);
        }
        return csElem;
    }
    
    private Element writeCoordTransform(final CoordinateTransform ct) {
        final Element ctElem = new Element("coordTransform");
        ctElem.setAttribute("name", ct.getName());
        ctElem.setAttribute("transformType", ct.getTransformType().toString());
        final List params = ct.getParameters();
        for (int i = 0; i < params.size(); ++i) {
            final Parameter param = params.get(i);
            final Element pElem = new Element("parameter");
            pElem.setAttribute("name", param.getName());
            pElem.setAttribute("value", param.getStringValue());
            ctElem.addContent(pElem);
        }
        return ctElem;
    }
    
    private Element writeVariable(final VariableSimpleIF v) {
        final Element varElem = new Element("variable");
        varElem.setAttribute("name", v.getName());
        final DataType dt = v.getDataType();
        if (dt != null) {
            varElem.setAttribute("type", dt.toString());
        }
        for (final Attribute att : v.getAttributes()) {
            varElem.addContent(NcMLWriter.writeAttribute(att, "attribute", null));
        }
        return varElem;
    }
    
    public static void main(final String[] args) throws IOException {
        final String url = "C:/data/metars/Surface_METAR_20060326_0000.nc";
        final StationObsDataset ncd = (StationObsDataset)TypedDatasetFactory.open(FeatureType.STATION, url, null, new StringBuilder());
        final StationObsDatasetInfo info = new StationObsDatasetInfo(ncd, null);
        final FileOutputStream fos2 = new FileOutputStream("C:/TEMP/stationCollection.xml");
        final GZIPOutputStream zout = new GZIPOutputStream(fos2);
        info.writeStationObsDatasetXML(System.out);
        info.writeStationCollectionXML(zout);
        zout.close();
        final File f = new File("C:/TEMP/stationCollection.xml");
        System.out.println(" size=" + f.length());
    }
}
