// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.dt.point;

import ucar.nc2.dt.DatatypeIterator;
import java.util.Collections;
import ucar.nc2.dt.StationImpl;
import org.slf4j.LoggerFactory;
import java.util.Date;
import ucar.nc2.dt.DataIterator;
import java.text.ParseException;
import ucar.ma2.InvalidRangeException;
import java.util.Set;
import ucar.ma2.IndexIterator;
import java.util.HashSet;
import ucar.ma2.StructureData;
import ucar.nc2.dt.StationObsDatatype;
import java.util.ArrayList;
import ucar.ma2.Index;
import ucar.nc2.Dimension;
import ucar.ma2.Array;
import ucar.unidata.geoloc.Station;
import java.util.HashMap;
import ucar.ma2.ArrayChar;
import java.util.List;
import ucar.nc2.constants.AxisType;
import java.io.IOException;
import ucar.nc2.dt.TypedDataset;
import ucar.nc2.util.CancelTask;
import ucar.nc2.dataset.NetcdfDataset;
import java.util.StringTokenizer;
import ucar.nc2.constants.FeatureType;
import ucar.nc2.NetcdfFile;
import ucar.nc2.Structure;
import ucar.nc2.Variable;
import org.slf4j.Logger;
import ucar.nc2.dt.TypedDatasetFactoryIF;

public class UnidataStationObsDataset extends StationObsDatasetImpl implements TypedDatasetFactoryIF
{
    private static Logger log;
    private Variable latVar;
    private Variable lonVar;
    private Variable altVar;
    private Variable timeVar;
    private Variable timeNominalVar;
    private Variable lastVar;
    private Variable prevVar;
    private Variable firstVar;
    private Variable nextVar;
    private Variable numChildrenVar;
    private Variable stationIndexVar;
    private Variable stationIdVar;
    private Variable stationDescVar;
    private Variable numStationsVar;
    private boolean isForwardLinkedList;
    private boolean isBackwardLinkedList;
    private boolean isContiguousList;
    private Structure recordVar;
    private RecordDatasetHelper recordHelper;
    
    public static boolean isValidFile(final NetcdfFile ds) {
        if (!ds.findAttValueIgnoreCase(null, "cdm_data_type", "").equalsIgnoreCase(FeatureType.STATION.toString()) && !ds.findAttValueIgnoreCase(null, "cdm_datatype", "").equalsIgnoreCase(FeatureType.STATION.toString())) {
            return false;
        }
        final String conv = ds.findAttValueIgnoreCase(null, "Conventions", null);
        if (conv == null) {
            return false;
        }
        boolean convOk = false;
        final StringTokenizer stoke = new StringTokenizer(conv, ",");
        while (stoke.hasMoreTokens()) {
            final String toke = stoke.nextToken().trim();
            if (toke.equalsIgnoreCase("Unidata Observation Dataset v1.0")) {
                convOk = true;
            }
        }
        if (!convOk) {
            return false;
        }
        final Variable stationIndexVar = UnidataObsDatasetHelper.findVariable(ds, "parent_index");
        return stationIndexVar != null;
    }
    
    public boolean isMine(final NetcdfDataset ds) {
        return isValidFile(ds);
    }
    
    public TypedDataset open(final NetcdfDataset ncd, final CancelTask task, final StringBuilder errlog) throws IOException {
        return new UnidataStationObsDataset(ncd);
    }
    
    public UnidataStationObsDataset() {
    }
    
    public UnidataStationObsDataset(final NetcdfDataset ds) throws IOException {
        super(ds);
        this.latVar = UnidataObsDatasetHelper.getCoordinate(ds, AxisType.Lat);
        this.lonVar = UnidataObsDatasetHelper.getCoordinate(ds, AxisType.Lon);
        this.altVar = UnidataObsDatasetHelper.getCoordinate(ds, AxisType.Height);
        this.timeVar = UnidataObsDatasetHelper.getCoordinate(ds, AxisType.Time);
        if (this.latVar == null) {
            throw new IllegalStateException("Missing latitude variable");
        }
        if (this.lonVar == null) {
            throw new IllegalStateException("Missing longitude coordinate variable");
        }
        if (this.timeVar == null) {
            throw new IllegalStateException("Missing time coordinate variable");
        }
        this.timeNominalVar = UnidataObsDatasetHelper.findVariable(ds, "time_nominal");
        this.lastVar = UnidataObsDatasetHelper.findVariable(ds, "lastChild");
        this.prevVar = UnidataObsDatasetHelper.findVariable(ds, "prevChild");
        this.firstVar = UnidataObsDatasetHelper.findVariable(ds, "firstChild");
        this.nextVar = UnidataObsDatasetHelper.findVariable(ds, "nextChild");
        this.numChildrenVar = UnidataObsDatasetHelper.findVariable(ds, "numChildren");
        this.stationIndexVar = UnidataObsDatasetHelper.findVariable(ds, "parent_index");
        this.isForwardLinkedList = (this.firstVar != null && this.nextVar != null);
        this.isBackwardLinkedList = (this.lastVar != null && this.prevVar != null);
        this.isContiguousList = (!this.isForwardLinkedList && !this.isBackwardLinkedList && this.firstVar != null && this.numChildrenVar != null);
        if (!this.isForwardLinkedList && !this.isBackwardLinkedList && !this.isContiguousList) {
            if (this.firstVar != null) {
                throw new IllegalStateException("Missing nextVar (linked list) or numChildrenVar (contiguous list) variable");
            }
            if (this.lastVar != null) {
                throw new IllegalStateException("Missing prevVar (linked list) variable");
            }
            if (this.nextVar != null) {
                throw new IllegalStateException("Missing firstVar (linked list) variable");
            }
            if (this.prevVar != null) {
                throw new IllegalStateException("Missing lastVar (linked list) variable");
            }
        }
        this.stationIdVar = UnidataObsDatasetHelper.findVariable(ds, "station_id");
        this.stationDescVar = UnidataObsDatasetHelper.findVariable(ds, "station_description");
        this.numStationsVar = UnidataObsDatasetHelper.findVariable(ds, "number_stations");
        if (this.stationIdVar == null) {
            throw new IllegalStateException("Missing station id variable");
        }
        (this.recordHelper = new RecordDatasetHelper(ds, this.timeVar.getName(), (this.timeNominalVar == null) ? null : this.timeNominalVar.getName(), this.dataVariables, this.parseInfo)).setStationInfo(this.stationIndexVar.getName(), (this.stationDescVar == null) ? null : this.stationDescVar.getName());
        this.removeDataVariable(this.stationIndexVar.getName());
        this.removeDataVariable(this.timeVar.getName());
        if (this.timeNominalVar != null) {
            this.removeDataVariable(this.timeNominalVar.getName());
        }
        if (this.prevVar != null) {
            this.removeDataVariable(this.prevVar.getName());
        }
        if (this.nextVar != null) {
            this.removeDataVariable(this.nextVar.getName());
        }
        this.recordVar = this.recordHelper.recordVar;
        this.timeUnit = this.recordHelper.timeUnit;
        this.readStations();
        this.startDate = UnidataObsDatasetHelper.getStartDate(ds);
        this.endDate = UnidataObsDatasetHelper.getEndDate(ds);
        this.boundingBox = UnidataObsDatasetHelper.getBoundingBox(ds);
        if (null == this.boundingBox) {
            this.setBoundingBox();
        }
        if (null == this.startDate) {
            final Variable minTimeVar = ds.findVariable("minimum_time_observation");
            final int minTimeValue = minTimeVar.readScalarInt();
            this.startDate = this.timeUnit.makeDate(minTimeValue);
        }
        if (null == this.endDate) {
            final Variable maxTimeVar = ds.findVariable("maximum_time_observation");
            final int maxTimeValue = maxTimeVar.readScalarInt();
            this.endDate = this.timeUnit.makeDate(maxTimeValue);
        }
        this.title = ds.findAttValueIgnoreCase(null, "title", "");
        this.desc = ds.findAttValueIgnoreCase(null, "description", "");
    }
    
    private void readStations() throws IOException {
        final Array stationIdArray = this.stationIdVar.read();
        ArrayChar stationDescArray = null;
        if (this.stationDescVar != null) {
            stationDescArray = (ArrayChar)this.stationDescVar.read();
        }
        final Array latArray = this.latVar.read();
        final Array lonArray = this.lonVar.read();
        final Array elevArray = (this.altVar != null) ? this.altVar.read() : null;
        final Array firstRecordArray = (this.isForwardLinkedList || this.isContiguousList) ? this.firstVar.read() : this.lastVar.read();
        Array numChildrenArray = null;
        if (this.numChildrenVar != null) {
            numChildrenArray = this.numChildrenVar.read();
        }
        final Dimension stationDim = UnidataObsDatasetHelper.findDimension(this.ncfile, "station");
        int n = 0;
        if (this.numStationsVar != null) {
            n = this.numStationsVar.readScalarInt();
        }
        else {
            n = stationDim.getLength();
        }
        final Index ima = latArray.getIndex();
        this.recordHelper.stnHash = new HashMap<Object, Station>(2 * n);
        for (int i = 0; i < n; ++i) {
            ima.set(i);
            String stationName;
            String stationDesc;
            if (stationIdArray instanceof ArrayChar) {
                stationName = ((ArrayChar)stationIdArray).getString(i).trim();
                stationDesc = ((this.stationDescVar != null) ? stationDescArray.getString(i) : null);
                if (stationDesc != null) {
                    stationDesc = stationDesc.trim();
                }
            }
            else {
                stationName = stationIdArray.getObject(ima).toString();
                stationDesc = ((this.stationDescVar != null) ? ((String)stationDescArray.getObject(ima)) : null);
            }
            final UnidataStationImpl bean = new UnidataStationImpl(stationName, stationDesc, (double)latArray.getFloat(ima), (double)lonArray.getFloat(ima), (this.altVar != null) ? ((double)elevArray.getFloat(ima)) : Double.NaN, firstRecordArray.getInt(ima), (this.numChildrenVar != null) ? numChildrenArray.getInt(ima) : -1);
            this.stations.add(bean);
            this.recordHelper.stnHash.put(i, bean);
        }
    }
    
    @Override
    protected void setTimeUnits() {
    }
    
    @Override
    protected void setStartDate() {
    }
    
    @Override
    protected void setEndDate() {
    }
    
    @Override
    protected void setBoundingBox() {
        this.boundingBox = this.stationHelper.getBoundingBox();
    }
    
    public List getData(final CancelTask cancel) throws IOException {
        final List<StationObsDatatype> allData = new ArrayList<StationObsDatatype>();
        for (int n = this.getDataCount(), i = 0; i < n; ++i) {
            final StationObsDatatype obs = this.makeObs(i, false, null);
            if (obs != null) {
                allData.add(obs);
            }
            if (cancel != null && cancel.isCancel()) {
                return null;
            }
        }
        return allData;
    }
    
    public int getDataCount() {
        final Dimension unlimitedDim = this.ncfile.getUnlimitedDimension();
        return unlimitedDim.getLength();
    }
    
    public List getData(final Station s, final CancelTask cancel) throws IOException {
        return ((UnidataStationImpl)s).getObservations();
    }
    
    public void checkLinks(final StringBuffer sbuff) throws IOException {
        int countErrs = 0;
        if (this.isBackwardLinkedList) {
            final Array lastArray = this.lastVar.read();
            final Array prevArray = this.prevVar.read();
            final Array stnArray = this.stationIndexVar.read();
            final Index prevIndex = prevArray.getIndex();
            final Index stnIndex = stnArray.getIndex();
            int stnIdx = 0;
            final IndexIterator stnIter = lastArray.getIndexIterator();
            while (stnIter.hasNext()) {
                final Set<Integer> records = new HashSet<Integer>(500);
                int recNo = stnIter.getIntNext();
                System.out.print("Station " + stnIdx);
                while (recNo >= 0) {
                    System.out.print(" " + recNo);
                    records.add(recNo);
                    final int stnFromRecord = stnArray.getInt(stnIndex.set(recNo));
                    if (stnFromRecord != stnIdx) {
                        sbuff.append("recno ").append(recNo).append(" has bad station index\n");
                        if (++countErrs > 10) {
                            return;
                        }
                    }
                    recNo = prevArray.getInt(prevIndex.set(recNo));
                    if (records.contains(recNo)) {
                        sbuff.append("stn ").append(stnIdx).append(" has circular links\n");
                        if (++countErrs > 10) {
                            return;
                        }
                        break;
                    }
                }
                System.out.println();
                ++stnIdx;
            }
        }
        sbuff.append("done");
    }
    
    public static void main(final String[] args) throws IOException {
        final String filename = "C:/data/199707010000.LAKEOUT_DOMAIN2";
        final UnidataStationObsDataset ods = new UnidataStationObsDataset(NetcdfDataset.openDataset(filename));
        final StringBuffer sbuff = new StringBuffer(50000);
        ods.checkLinks(sbuff);
        System.out.println("\n\n" + sbuff.toString());
    }
    
    protected RecordDatasetHelper.RecordStationObs makeObs(final int recno, final boolean storeData, StructureData sdata) throws IOException {
        try {
            if (recno > this.getDataCount()) {
                final int n = this.getDataCount();
                this.ncfile.syncExtend();
                UnidataStationObsDataset.log.info("UnidataStationObsDataset.makeObs recno=" + recno + " > " + n + "; after sync= " + this.getDataCount());
            }
            if (null == sdata) {
                sdata = this.recordVar.readStructure(recno);
            }
            final int stationIndex = sdata.getScalarInt(this.stationIndexVar.getShortName());
            if (stationIndex < 0 || stationIndex >= this.stations.size()) {
                this.parseInfo.append("cant find station at index = ").append(stationIndex).append("\n");
                return null;
            }
            final Station station = this.stations.get(stationIndex);
            if (station == null) {
                this.parseInfo.append("cant find station at index = ").append(stationIndex).append("\n");
                return null;
            }
            final double obsTime = this.getTime(this.timeVar, sdata);
            final double nomTime = (this.timeNominalVar == null) ? obsTime : this.getTime(this.timeNominalVar, sdata);
            if (storeData) {
                final RecordDatasetHelper recordHelper = this.recordHelper;
                recordHelper.getClass();
                return recordHelper.new RecordStationObs(station, obsTime, nomTime, sdata);
            }
            final RecordDatasetHelper recordHelper2 = this.recordHelper;
            recordHelper2.getClass();
            return recordHelper2.new RecordStationObs(station, obsTime, nomTime, recno);
        }
        catch (InvalidRangeException e) {
            UnidataStationObsDataset.log.error("UnidataStationObsDataset.makeObs recno=" + recno, e);
            throw new IOException(e.getMessage());
        }
        catch (ParseException e2) {
            UnidataStationObsDataset.log.error("UnidataStationObsDataset.makeObs recno=" + recno, e2);
            throw new IOException(e2.getMessage());
        }
    }
    
    @Override
    public DataIterator getDataIterator(final Station s) {
        return ((UnidataStationImpl)s).iterator();
    }
    
    @Override
    public DataIterator getDataIterator(final Station s, final Date start, final Date end) {
        return ((UnidataStationImpl)s).iterator(start, end);
    }
    
    public DataIterator getDataIterator(final int bufferSize) throws IOException {
        return new StationDatatypeIterator(this.recordHelper.recordVar, bufferSize);
    }
    
    static {
        UnidataStationObsDataset.log = LoggerFactory.getLogger(UnidataStationObsDataset.class);
    }
    
    private class UnidataStationImpl extends StationImpl
    {
        private int firstRecord;
        private Variable next;
        final /* synthetic */ UnidataStationObsDataset this$0;
        
        private UnidataStationImpl(final String name, final String desc, final double lat, final double lon, final double elev, final int firstRecord, final int count) {
            super(name, desc, lat, lon, elev, count);
            this.firstRecord = firstRecord;
            this.next = (UnidataStationObsDataset.this.isForwardLinkedList ? UnidataStationObsDataset.this.nextVar : UnidataStationObsDataset.this.prevVar);
        }
        
        @Override
        protected List<StationObsDatatype> readObservations() throws IOException {
            final List<StationObsDatatype> obs = new ArrayList<StationObsDatatype>();
            int recno = this.firstRecord;
            final int end = this.firstRecord + this.count - 1;
            int nextRecord = this.firstRecord;
            while (recno >= 0) {
                try {
                    if (recno > UnidataStationObsDataset.this.getDataCount()) {
                        final int n = UnidataStationObsDataset.this.getDataCount();
                        UnidataStationObsDataset.this.ncfile.syncExtend();
                        UnidataStationObsDataset.log.info("UnidataStationObsDataset.makeObs recno=" + recno + " > " + n + "; after sync= " + UnidataStationObsDataset.this.getDataCount());
                    }
                    final StructureData sdata = UnidataStationObsDataset.this.recordVar.readStructure(recno);
                    if (UnidataStationObsDataset.this.isContiguousList) {
                        if (nextRecord++ > end) {
                            break;
                        }
                    }
                    else {
                        nextRecord = sdata.getScalarInt(this.next.getName());
                    }
                    final double obsTime = UnidataStationObsDataset.this.getTime(UnidataStationObsDataset.this.timeVar, sdata);
                    final double nomTime = (UnidataStationObsDataset.this.timeNominalVar == null) ? obsTime : UnidataStationObsDataset.this.getTime(UnidataStationObsDataset.this.timeNominalVar, sdata);
                    final List<StationObsDatatype> list = obs;
                    final RecordDatasetHelper access$1000 = UnidataStationObsDataset.this.recordHelper;
                    access$1000.getClass();
                    list.add(access$1000.new RecordStationObs(this, obsTime, nomTime, recno));
                    recno = nextRecord;
                    continue;
                }
                catch (InvalidRangeException e) {
                    UnidataStationObsDataset.log.error("UnidataStationObsDataset.readObservation recno=" + recno, e);
                    throw new IOException(e.getMessage());
                }
                catch (ParseException e2) {
                    UnidataStationObsDataset.log.error("UnidataStationObsDataset.readObservation recno=" + recno, e2);
                    throw new IOException(e2.getMessage());
                }
                break;
            }
            Collections.sort(obs);
            return obs;
        }
        
        DataIterator iterator() {
            return new StationIterator();
        }
        
        DataIterator iterator(final Date start, final Date end) {
            return new StationIterator(start, end);
        }
        
        private class StationIterator implements DataIterator
        {
            int nextRecno;
            int last;
            double startTime;
            double endTime;
            boolean hasDateRange;
            
            StationIterator() {
                this.nextRecno = UnidataStationImpl.this.firstRecord;
                this.last = UnidataStationImpl.this.firstRecord + UnidataStationImpl.this.count - 1;
            }
            
            StationIterator(final Date start, final Date end) {
                this.nextRecno = UnidataStationImpl.this.firstRecord;
                this.last = UnidataStationImpl.this.firstRecord + UnidataStationImpl.this.count - 1;
                this.startTime = UnidataStationImpl.this.this$0.timeUnit.makeValue(start);
                this.endTime = UnidataStationImpl.this.this$0.timeUnit.makeValue(end);
                this.hasDateRange = true;
            }
            
            public boolean hasNext() {
                return this.nextRecno >= 0;
            }
            
            public Object nextData() throws IOException {
                final RecordDatasetHelper.RecordStationObs sobs = UnidataStationObsDataset.this.makeObs(this.nextRecno, true, null);
                if (!sobs.getStation().getName().equals(UnidataStationImpl.this.getName())) {
                    throw new IllegalStateException("BAD Station link (" + this.nextRecno + ") station name=" + sobs.getStation().getName() + " should be " + UnidataStationImpl.this.getName());
                }
                if (UnidataStationObsDataset.this.isContiguousList) {
                    ++this.nextRecno;
                    if (this.nextRecno > this.last) {
                        this.nextRecno = -1;
                    }
                }
                else {
                    this.nextRecno = sobs.sdata.getScalarInt(UnidataStationImpl.this.next.getName());
                }
                if (this.hasDateRange) {
                    final double timeValue = sobs.getObservationTime();
                    if (timeValue < this.startTime || timeValue > this.endTime) {
                        return this.nextData();
                    }
                }
                return sobs;
            }
            
            public Object next() {
                try {
                    return this.nextData();
                }
                catch (IOException e) {
                    UnidataStationObsDataset.log.error("UnidataStationObsDataset.StationIterator.next recno=" + this.nextRecno, e);
                    throw new IllegalStateException(e.getMessage());
                }
            }
            
            public void remove() {
                throw new UnsupportedOperationException();
            }
        }
    }
    
    private class StationDatatypeIterator extends DatatypeIterator
    {
        @Override
        protected Object makeDatatypeWithData(final int recnum, final StructureData sdata) throws IOException {
            return UnidataStationObsDataset.this.makeObs(recnum, true, sdata);
        }
        
        StationDatatypeIterator(final Structure struct, final int bufferSize) {
            super(struct, bufferSize);
        }
    }
}
