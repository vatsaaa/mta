// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.dt.point;

import ucar.ma2.StructureDataW;
import ucar.ma2.StructureData;
import ucar.nc2.VariableSimpleIF;
import java.util.Collections;
import ucar.nc2.dt.StationImpl;
import org.slf4j.LoggerFactory;
import ucar.nc2.dt.DataIteratorAdapter;
import java.util.Date;
import ucar.nc2.dt.DataIterator;
import java.util.Collection;
import ucar.unidata.geoloc.Station;
import ucar.nc2.dt.StationObsDatatype;
import java.util.ArrayList;
import java.util.List;
import ucar.nc2.units.DateUnit;
import ucar.ma2.InvalidRangeException;
import ucar.ma2.Index;
import ucar.ma2.Array;
import ucar.ma2.ArrayChar;
import java.util.Iterator;
import ucar.nc2.constants.AxisType;
import java.io.IOException;
import ucar.nc2.dt.TypedDataset;
import ucar.nc2.util.CancelTask;
import ucar.nc2.dataset.NetcdfDataset;
import java.util.StringTokenizer;
import ucar.nc2.constants.FeatureType;
import ucar.nc2.NetcdfFile;
import ucar.ma2.StructureMembers;
import ucar.nc2.Variable;
import ucar.nc2.Dimension;
import org.slf4j.Logger;
import ucar.nc2.dt.TypedDatasetFactoryIF;

public class UnidataStationObsMultidimDataset extends StationObsDatasetImpl implements TypedDatasetFactoryIF
{
    private static Logger log;
    private Dimension stationDim;
    private Dimension obsDim;
    private Variable latVar;
    private Variable lonVar;
    private Variable altVar;
    private Variable timeVar;
    private Variable timeNominalVar;
    private Variable stationIdVar;
    private Variable stationDescVar;
    private Variable numStationsVar;
    private StructureMembers structureMembers;
    
    public static boolean isValidFile(final NetcdfFile ds) {
        if (!ds.findAttValueIgnoreCase(null, "cdm_data_type", "").equalsIgnoreCase(FeatureType.STATION.toString()) && !ds.findAttValueIgnoreCase(null, "cdm_datatype", "").equalsIgnoreCase(FeatureType.STATION.toString())) {
            return false;
        }
        final String conv = ds.findAttValueIgnoreCase(null, "Conventions", null);
        if (conv == null) {
            return false;
        }
        boolean convOk = false;
        final StringTokenizer stoke = new StringTokenizer(conv, ",");
        while (stoke.hasMoreTokens()) {
            final String toke = stoke.nextToken().trim();
            if (toke.equalsIgnoreCase("Unidata Observation Dataset v1.0")) {
                convOk = true;
            }
        }
        if (!convOk) {
            return false;
        }
        if (null == UnidataObsDatasetHelper.findDimension(ds, "station")) {
            return false;
        }
        final Variable stationIndexVar = UnidataObsDatasetHelper.findVariable(ds, "parent_index");
        return stationIndexVar == null;
    }
    
    public boolean isMine(final NetcdfDataset ds) {
        return isValidFile(ds);
    }
    
    public TypedDataset open(final NetcdfDataset ncd, final CancelTask task, final StringBuilder errlog) throws IOException {
        return new UnidataStationObsMultidimDataset(ncd);
    }
    
    public UnidataStationObsMultidimDataset() {
    }
    
    public UnidataStationObsMultidimDataset(final NetcdfDataset ds) throws IOException {
        super(ds);
        this.stationDim = UnidataObsDatasetHelper.findDimension(ds, "station");
        this.obsDim = UnidataObsDatasetHelper.findDimension(ds, "observation");
        if (this.obsDim == null) {
            this.obsDim = ds.getUnlimitedDimension();
        }
        if (this.obsDim == null) {
            throw new IllegalStateException("must specify the observation dimension or use unlimited dimension");
        }
        this.latVar = UnidataObsDatasetHelper.getCoordinate(ds, AxisType.Lat);
        this.lonVar = UnidataObsDatasetHelper.getCoordinate(ds, AxisType.Lon);
        this.altVar = UnidataObsDatasetHelper.getCoordinate(ds, AxisType.Height);
        this.timeVar = UnidataObsDatasetHelper.getCoordinate(ds, AxisType.Time);
        this.timeNominalVar = UnidataObsDatasetHelper.findVariable(ds, "time_nominal");
        if (this.latVar == null) {
            throw new IllegalStateException("Missing latitude variable");
        }
        if (this.lonVar == null) {
            throw new IllegalStateException("Missing longitude coordinate variable");
        }
        if (this.timeVar == null) {
            throw new IllegalStateException("Missing time coordinate variable");
        }
        if (!this.latVar.getDimension(0).equals(this.stationDim)) {
            throw new IllegalStateException("latitude variable must use the station dimension");
        }
        if (!this.lonVar.getDimension(0).equals(this.stationDim)) {
            throw new IllegalStateException("longitude variable must use the station dimension");
        }
        if (!this.timeVar.getDimension(0).equals(this.stationDim)) {
            throw new IllegalStateException("time variable must use the station dimension");
        }
        if (this.altVar != null && !this.altVar.getDimension(0).equals(this.stationDim)) {
            throw new IllegalStateException("altitude variable must use the station dimension");
        }
        if (this.timeNominalVar != null && !this.timeNominalVar.getDimension(0).equals(this.stationDim)) {
            throw new IllegalStateException("timeNominal variable must use the station dimension");
        }
        this.stationIdVar = UnidataObsDatasetHelper.findVariable(ds, "station_id");
        this.stationDescVar = UnidataObsDatasetHelper.findVariable(ds, "station_description");
        this.numStationsVar = UnidataObsDatasetHelper.findVariable(ds, "number_stations");
        if (this.stationIdVar == null) {
            throw new IllegalStateException("Missing station id variable");
        }
        if (!this.stationIdVar.getDimension(0).equals(this.stationDim)) {
            throw new IllegalStateException("stationId variable must use the station dimension");
        }
        if (this.stationDescVar != null && !this.stationDescVar.getDimension(0).equals(this.stationDim)) {
            throw new IllegalStateException("stationDesc variable must use the station dimension");
        }
        this.structureMembers = new StructureMembers("UnidataStationObsMultidimDataset_obsStructure");
        for (final Variable v : this.ncfile.getVariables()) {
            if (v.getRank() < 2) {
                continue;
            }
            if (!v.getDimension(0).equals(this.stationDim) || !v.getDimension(1).equals(this.obsDim)) {
                continue;
            }
            this.dataVariables.add(v);
            final int[] shape = v.getShape();
            shape[shape[0] = 1] = 1;
            this.structureMembers.addMember(v.getShortName(), v.getDescription(), v.getUnitsString(), v.getDataType(), shape);
        }
        this.readStations();
        this.startDate = UnidataObsDatasetHelper.getStartDate(ds);
        this.endDate = UnidataObsDatasetHelper.getEndDate(ds);
        this.boundingBox = UnidataObsDatasetHelper.getBoundingBox(ds);
        if (null == this.boundingBox) {
            this.setBoundingBox();
        }
        this.setTimeUnits();
        this.title = ds.findAttValueIgnoreCase(null, "title", "");
        this.desc = ds.findAttValueIgnoreCase(null, "description", "");
    }
    
    private void readStations() throws IOException {
        final Array stationIdArray = this.stationIdVar.read();
        ArrayChar stationDescArray = null;
        if (this.stationDescVar != null) {
            stationDescArray = (ArrayChar)this.stationDescVar.read();
        }
        final Array latArray = this.readStationVariable(this.latVar);
        final Array lonArray = this.readStationVariable(this.lonVar);
        final Array elevArray = (this.altVar != null) ? this.readStationVariable(this.altVar) : null;
        int n = 0;
        if (this.numStationsVar != null) {
            n = this.numStationsVar.readScalarInt();
        }
        else {
            n = this.stationDim.getLength();
        }
        final Index ima = stationIdArray.getIndex();
        for (int i = 0; i < n; ++i) {
            ima.set(i);
            String stationName;
            String stationDesc;
            if (stationIdArray instanceof ArrayChar) {
                stationName = ((ArrayChar)stationIdArray).getString(i).trim();
                stationDesc = ((this.stationDescVar != null) ? stationDescArray.getString(i) : null);
                if (stationDesc != null) {
                    stationDesc = stationDesc.trim();
                }
            }
            else {
                stationName = stationIdArray.getObject(ima).toString();
                stationDesc = ((this.stationDescVar != null) ? ((String)stationDescArray.getObject(ima)) : null);
            }
            final MStationImpl bean = new MStationImpl(stationName, stationDesc, (double)latArray.getFloat(ima), (double)lonArray.getFloat(ima), (this.altVar != null) ? ((double)elevArray.getFloat(ima)) : Double.NaN, i, this.obsDim.getLength());
            this.stations.add(bean);
        }
    }
    
    private Array readStationVariable(final Variable svar) throws IOException {
        if (svar.getRank() == 1) {
            return svar.read();
        }
        if (svar.getRank() == 2) {
            final int[] shape = svar.getShape();
            shape[1] = 1;
            try {
                return svar.read(new int[2], shape).reduce(1);
            }
            catch (InvalidRangeException e) {
                throw new IllegalStateException(e.getMessage());
            }
        }
        throw new IllegalStateException("Station variables must have rank 1 or 2");
    }
    
    @Override
    protected void setTimeUnits() {
        final String timeUnitString = this.ncfile.findAttValueIgnoreCase(this.timeVar, "units", "seconds since 1970-01-01");
        try {
            this.timeUnit = new DateUnit(timeUnitString);
        }
        catch (Exception e) {
            this.parseInfo.append("Error on units = ").append(timeUnitString).append(" == ").append(e.getMessage()).append("\n");
            try {
                this.timeUnit = new DateUnit("seconds since 1970-01-01");
            }
            catch (Exception ex) {}
        }
    }
    
    @Override
    protected void setStartDate() {
    }
    
    @Override
    protected void setEndDate() {
    }
    
    @Override
    protected void setBoundingBox() {
        this.boundingBox = this.stationHelper.getBoundingBox();
    }
    
    public List getData(final CancelTask cancel) throws IOException {
        final List<StationObsDatatype> allData = new ArrayList<StationObsDatatype>();
        for (final Station s : this.getStations()) {
            final MStationImpl ms = (MStationImpl)s;
            allData.addAll(ms.readObservations());
            if (cancel != null && cancel.isCancel()) {
                return null;
            }
        }
        return allData;
    }
    
    public int getDataCount() {
        return this.stationDim.getLength() * this.obsDim.getLength();
    }
    
    public List getData(final Station s, final CancelTask cancel) throws IOException {
        return ((MStationImpl)s).getObservations();
    }
    
    public static void main(final String[] args) throws IOException {
        final String filename = "C:/data/199707010000.LAKEOUT_DOMAIN2";
        final UnidataStationObsDataset ods = new UnidataStationObsDataset(NetcdfDataset.openDataset(filename));
        final StringBuffer sbuff = new StringBuffer(50000);
        ods.checkLinks(sbuff);
        System.out.println("\n\n" + sbuff.toString());
    }
    
    private Array readData(final Variable v, final int stationIndex, final int obsIndex) throws IOException {
        final int[] shape = v.getShape();
        final int[] origin = new int[v.getRank()];
        origin[0] = stationIndex;
        origin[1] = obsIndex;
        shape[shape[0] = 1] = 1;
        Array data = null;
        try {
            data = v.read(origin, shape);
        }
        catch (InvalidRangeException e) {
            throw new IllegalStateException(e.getMessage());
        }
        return data;
    }
    
    @Override
    public DataIterator getDataIterator(final Station s) {
        return ((MStationImpl)s).iterator();
    }
    
    @Override
    public DataIterator getDataIterator(final Station s, final Date start, final Date end) {
        return ((MStationImpl)s).iterator(start, end);
    }
    
    public DataIterator getDataIterator(final int bufferSize) throws IOException {
        return new DataIteratorAdapter(this.getData().iterator());
    }
    
    static {
        UnidataStationObsMultidimDataset.log = LoggerFactory.getLogger(UnidataStationObsMultidimDataset.class);
    }
    
    private class MStationImpl extends StationImpl
    {
        int station_index;
        final /* synthetic */ UnidataStationObsMultidimDataset this$0;
        
        private MStationImpl(final String name, final String desc, final double lat, final double lon, final double elev, final int station_index, final int count) {
            super(name, desc, lat, lon, elev, count);
            this.station_index = station_index;
        }
        
        @Override
        protected List<StationObsDatatype> readObservations() throws IOException {
            final List<StationObsDatatype> obs = new ArrayList<StationObsDatatype>();
            final StationIterator siter = new StationIterator();
            while (siter.hasNext()) {
                obs.add((StationObsDatatype)siter.nextData());
            }
            Collections.sort(obs);
            return obs;
        }
        
        DataIterator iterator() {
            return new StationIterator();
        }
        
        DataIterator iterator(final Date start, final Date end) {
            return new StationIterator(start, end);
        }
        
        private class StationIterator implements DataIterator
        {
            int obsIndex;
            double startTime;
            double endTime;
            boolean hasDateRange;
            
            StationIterator() {
                this.obsIndex = 0;
            }
            
            StationIterator(final Date start, final Date end) {
                this.obsIndex = 0;
                this.startTime = MStationImpl.this.this$0.timeUnit.makeValue(start);
                this.endTime = MStationImpl.this.this$0.timeUnit.makeValue(end);
                this.hasDateRange = true;
            }
            
            public boolean hasNext() {
                return this.obsIndex < MStationImpl.this.count - 1;
            }
            
            public Object nextData() throws IOException {
                if (!this.hasNext()) {
                    return null;
                }
                final MStationObsImpl sobs = new MStationObsImpl(MStationImpl.this, MStationImpl.this.station_index, this.obsIndex, UnidataStationObsMultidimDataset.this.dataVariables, UnidataStationObsMultidimDataset.this.structureMembers);
                ++this.obsIndex;
                if (this.hasDateRange) {
                    final double timeValue = sobs.getObservationTime();
                    if (timeValue < this.startTime || timeValue > this.endTime) {
                        return this.nextData();
                    }
                }
                return sobs;
            }
            
            public Object next() {
                try {
                    return this.nextData();
                }
                catch (IOException e) {
                    throw new IllegalStateException(e.getMessage());
                }
            }
            
            public void remove() {
                throw new UnsupportedOperationException();
            }
        }
    }
    
    private class MStationObsImpl extends StationObsDatatypeImpl
    {
        int stationIndex;
        int obsIndex;
        List<VariableSimpleIF> dataVariables;
        StructureMembers sm;
        
        MStationObsImpl(final Station s, final int stationIndex, final int obsIndex, final List<VariableSimpleIF> dataVariables, final StructureMembers sm) throws IOException {
            super(s, 0.0, 0.0);
            this.stationIndex = stationIndex;
            this.obsIndex = obsIndex;
            this.dataVariables = dataVariables;
            this.sm = sm;
            final Array timeData = UnidataStationObsMultidimDataset.this.readData(UnidataStationObsMultidimDataset.this.timeVar, stationIndex, obsIndex);
            this.obsTime = timeData.getDouble(timeData.getIndex());
        }
        
        public Date getNominalTimeAsDate() {
            return UnidataStationObsMultidimDataset.this.timeUnit.makeDate(this.getNominalTime());
        }
        
        public Date getObservationTimeAsDate() {
            return UnidataStationObsMultidimDataset.this.timeUnit.makeDate(this.getObservationTime());
        }
        
        public StructureData getData() throws IOException {
            final StructureDataW sdata = new StructureDataW(this.sm);
            for (final VariableSimpleIF vs : this.dataVariables) {
                final Array data = UnidataStationObsMultidimDataset.this.readData((Variable)vs, this.stationIndex, this.obsIndex);
                sdata.setMemberData(vs.getShortName(), data);
            }
            return sdata;
        }
    }
}
