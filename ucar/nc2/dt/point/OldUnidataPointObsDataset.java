// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.dt.point;

import ucar.ma2.StructureData;
import ucar.nc2.dt.DatatypeIterator;
import ucar.nc2.Structure;
import ucar.nc2.dt.DataIterator;
import java.util.Date;
import ucar.unidata.geoloc.LatLonRect;
import java.util.List;
import java.io.IOException;
import ucar.nc2.dt.TypedDataset;
import ucar.nc2.util.CancelTask;
import ucar.nc2.dataset.NetcdfDataset;
import ucar.nc2.NetcdfFile;
import java.util.ArrayList;
import ucar.nc2.dt.TypedDatasetFactoryIF;

public class OldUnidataPointObsDataset extends PointObsDatasetImpl implements TypedDatasetFactoryIF
{
    private static String latName;
    private static String lonName;
    private static String altName;
    private static String timeName;
    private RecordDatasetHelper recordHelper;
    private ArrayList records;
    
    public static boolean isValidFile(final NetcdfFile ds) {
        return ds.hasUnlimitedDimension() && ds.findVariable(OldUnidataPointObsDataset.latName) != null && ds.findVariable(OldUnidataPointObsDataset.lonName) != null && ds.findVariable(OldUnidataPointObsDataset.altName) != null && ds.findVariable(OldUnidataPointObsDataset.timeName) != null;
    }
    
    public boolean isMine(final NetcdfDataset ds) {
        return isValidFile(ds);
    }
    
    public TypedDataset open(final NetcdfDataset ncd, final CancelTask task, final StringBuilder errlog) throws IOException {
        return new OldUnidataPointObsDataset(ncd);
    }
    
    public OldUnidataPointObsDataset() {
    }
    
    public OldUnidataPointObsDataset(final NetcdfDataset ds) throws IOException {
        super(ds);
        (this.recordHelper = new RecordDatasetHelper(ds, OldUnidataPointObsDataset.timeName, OldUnidataPointObsDataset.timeName, this.dataVariables)).setLocationInfo(OldUnidataPointObsDataset.latName, OldUnidataPointObsDataset.lonName, OldUnidataPointObsDataset.altName);
        this.records = this.recordHelper.readAllCreateObs(null);
        this.removeDataVariable(OldUnidataPointObsDataset.timeName);
        this.removeDataVariable(OldUnidataPointObsDataset.latName);
        this.removeDataVariable(OldUnidataPointObsDataset.lonName);
        this.removeDataVariable(OldUnidataPointObsDataset.altName);
        this.setTimeUnits();
        this.setStartDate();
        this.setEndDate();
        this.setBoundingBox();
    }
    
    @Override
    protected void setTimeUnits() {
        this.timeUnit = this.recordHelper.timeUnit;
    }
    
    @Override
    protected void setStartDate() {
        this.startDate = this.timeUnit.makeDate(this.recordHelper.minDate);
    }
    
    @Override
    protected void setEndDate() {
        this.endDate = this.timeUnit.makeDate(this.recordHelper.maxDate);
    }
    
    @Override
    protected void setBoundingBox() {
        this.boundingBox = this.recordHelper.boundingBox;
    }
    
    public List getData(final CancelTask cancel) throws IOException {
        return this.records;
    }
    
    public int getDataCount() {
        return this.records.size();
    }
    
    public List getData(final LatLonRect boundingBox, final CancelTask cancel) throws IOException {
        return this.recordHelper.getData(this.records, boundingBox, cancel);
    }
    
    public List getData(final LatLonRect boundingBox, final Date start, final Date end, final CancelTask cancel) throws IOException {
        final double startTime = this.timeUnit.makeValue(start);
        final double endTime = this.timeUnit.makeValue(end);
        return this.recordHelper.getData(this.records, boundingBox, startTime, endTime, cancel);
    }
    
    public DataIterator getDataIterator(final int bufferSize) throws IOException {
        return new PointDatatypeIterator(this.recordHelper.recordVar, bufferSize);
    }
    
    static {
        OldUnidataPointObsDataset.latName = "lat";
        OldUnidataPointObsDataset.lonName = "lon";
        OldUnidataPointObsDataset.altName = "Depth";
        OldUnidataPointObsDataset.timeName = "timeObs";
    }
    
    private class PointDatatypeIterator extends DatatypeIterator
    {
        @Override
        protected Object makeDatatypeWithData(final int recnum, final StructureData sdata) {
            return OldUnidataPointObsDataset.this.recordHelper.new RecordPointObs(recnum, sdata);
        }
        
        PointDatatypeIterator(final Structure struct, final int bufferSize) {
            super(struct, bufferSize);
        }
    }
}
