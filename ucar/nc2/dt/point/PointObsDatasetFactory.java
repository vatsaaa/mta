// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.dt.point;

import ucar.nc2.NetcdfFile;
import ucar.nc2.dataset.NetcdfDataset;
import ucar.nc2.iosp.adde.AddeStationObsDataset;
import thredds.catalog.ServiceType;
import java.io.IOException;
import ucar.nc2.util.CancelTask;
import ucar.nc2.dt.PointObsDataset;
import thredds.catalog.InvAccess;

public class PointObsDatasetFactory
{
    public static PointObsDataset open(final InvAccess access, final StringBuffer logMessages) throws IOException {
        return open(access, null, logMessages);
    }
    
    public static PointObsDataset open(final InvAccess access, final CancelTask task, final StringBuffer logMessages) throws IOException {
        if (access.getService().getServiceType() == ServiceType.ADDE) {
            return new AddeStationObsDataset(access, task);
        }
        return open(access.getStandardUrlName(), task, logMessages);
    }
    
    public static PointObsDataset open(final String location) throws IOException {
        return open(location, null, null);
    }
    
    public static PointObsDataset open(final String location, final StringBuffer log) throws IOException {
        return open(location, null, log);
    }
    
    public static PointObsDataset open(final String location, final CancelTask task, final StringBuffer log) throws IOException {
        if (location.startsWith("adde:")) {
            return new AddeStationObsDataset(location, task);
        }
        final NetcdfDataset ncfile = NetcdfDataset.acquireDataset(location, task);
        ncfile.sendIospMessage("AddRecordStructure");
        if (UnidataStationObsDataset.isValidFile(ncfile)) {
            return new UnidataStationObsDataset(ncfile);
        }
        if (UnidataPointObsDataset.isValidFile(ncfile)) {
            return new UnidataPointObsDataset(ncfile);
        }
        if (DapperDataset.isValidFile(ncfile)) {
            return DapperDataset.factory(ncfile);
        }
        if (SequenceObsDataset.isValidFile(ncfile)) {
            return new SequenceObsDataset(ncfile, task);
        }
        if (UnidataStationObsDataset2.isValidFile(ncfile)) {
            return new UnidataStationObsDataset2(ncfile);
        }
        if (NdbcDataset.isValidFile(ncfile)) {
            return new NdbcDataset(ncfile);
        }
        if (MadisStationObsDataset.isValidFile(ncfile)) {
            return new MadisStationObsDataset(ncfile);
        }
        if (OldUnidataStationObsDataset.isValidFile(ncfile)) {
            return new OldUnidataStationObsDataset(ncfile);
        }
        if (OldUnidataPointObsDataset.isValidFile(ncfile)) {
            return new OldUnidataPointObsDataset(ncfile);
        }
        if (null != log) {
            log.append("Cant find a Point/Station adapter for ").append(location);
        }
        ncfile.close();
        return null;
    }
}
