// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.dt.point;

import ucar.nc2.dt.DatatypeIterator;
import ucar.nc2.dt.DataIterator;
import java.util.Date;
import ucar.unidata.geoloc.LatLonRect;
import ucar.nc2.Dimension;
import ucar.unidata.geoloc.EarthLocation;
import ucar.unidata.geoloc.EarthLocationImpl;
import ucar.nc2.dt.PointObsDatatype;
import java.util.ArrayList;
import ucar.ma2.Array;
import ucar.ma2.StructureData;
import ucar.ma2.MAMath;
import ucar.ma2.InvalidRangeException;
import ucar.unidata.geoloc.Station;
import java.util.HashMap;
import ucar.ma2.ArrayInt;
import java.util.List;
import java.util.StringTokenizer;
import ucar.nc2.Variable;
import java.io.IOException;
import ucar.nc2.dt.TypedDataset;
import ucar.nc2.util.CancelTask;
import ucar.nc2.dataset.NetcdfDataset;
import ucar.nc2.Attribute;
import ucar.nc2.NetcdfFile;
import ucar.nc2.Structure;
import ucar.nc2.dt.TypedDatasetFactoryIF;

public class MadisPointObsDataset extends PointObsDatasetImpl implements TypedDatasetFactoryIF
{
    private Structure recordVar;
    private RecordDatasetHelper recordHelper;
    private String obsTimeVName;
    private String nomTimeVName;
    private String stnIdVName;
    private String stnDescVName;
    private String altVName;
    private boolean debug;
    private boolean debugLinks;
    
    public static boolean isValidFile(final NetcdfFile ds) {
        if (ds.findVariable("staticIds") == null) {
            return false;
        }
        if (ds.findVariable("nStaticIds") == null) {
            return false;
        }
        if (ds.findVariable("lastRecord") == null) {
            return false;
        }
        if (ds.findVariable("prevRecord") == null) {
            return false;
        }
        if (ds.findVariable("latitude") == null) {
            return false;
        }
        if (ds.findVariable("longitude") == null) {
            return false;
        }
        if (!ds.hasUnlimitedDimension()) {
            return false;
        }
        if (ds.findGlobalAttribute("timeVariables") == null) {
            return false;
        }
        if (ds.findGlobalAttribute("idVariables") == null) {
            return false;
        }
        final Attribute att = ds.findGlobalAttribute("title");
        return att == null || !att.getStringValue().equals("MADIS ACARS data");
    }
    
    public boolean isMine(final NetcdfDataset ds) {
        return isValidFile(ds);
    }
    
    public TypedDataset open(final NetcdfDataset ncd, final CancelTask task, final StringBuilder errlog) throws IOException {
        return new MadisStationObsDataset(ncd);
    }
    
    public MadisPointObsDataset() {
        this.debug = false;
        this.debugLinks = false;
    }
    
    public MadisPointObsDataset(final NetcdfDataset ds) throws IOException {
        super(ds);
        this.debug = false;
        this.debugLinks = false;
        this.altVName = "elevation";
        final String timeNames = ds.findAttValueIgnoreCase(null, "timeVariables", null);
        StringTokenizer stoker = new StringTokenizer(timeNames, ", ");
        this.obsTimeVName = stoker.nextToken();
        if (ds.findVariable("timeNominal") != null) {
            this.nomTimeVName = "timeNominal";
        }
        final String idNames = ds.findAttValueIgnoreCase(null, "idVariables", null);
        stoker = new StringTokenizer(idNames, ", ");
        this.stnIdVName = stoker.nextToken();
        if (this.stnIdVName.equals("stationName")) {
            if (ds.findVariable("locationName") != null) {
                this.stnDescVName = "locationName";
            }
            if (this.debug) {
                System.out.println("filetype 1 (metars)");
            }
        }
        else if (this.stnIdVName.equals("latitude")) {
            if (ds.findVariable("en_tailNumber") != null) {
                this.stnIdVName = "en_tailNumber";
            }
            this.altVName = "altitude";
            if (this.debug) {
                System.out.println("filetype 3 (acars)");
            }
        }
        else {
            if (ds.findVariable("stationId") != null) {
                this.stnIdVName = "stationId";
            }
            if (ds.findVariable("stationName") != null) {
                this.stnDescVName = "stationName";
            }
            if (this.debug) {
                System.out.println("filetype 2 (mesonet)");
            }
        }
        if (this.debug) {
            System.out.println("title= " + this.ncfile.findAttValueIgnoreCase(null, "title", null));
        }
        this.recordHelper = new RecordDatasetHelper(ds, this.obsTimeVName, this.nomTimeVName, this.dataVariables, this.parseInfo);
        this.removeDataVariable("prevRecord");
        this.timeUnit = this.recordHelper.timeUnit;
        final Variable lastRecordVar = ds.findVariable("lastRecord");
        final ArrayInt.D1 lastRecord = (ArrayInt.D1)lastRecordVar.read();
        final Variable inventoryVar = ds.findVariable("inventory");
        final ArrayInt.D1 inventoryData = (ArrayInt.D1)inventoryVar.read();
        final Variable v = ds.findVariable("nStaticIds");
        final int n = v.readScalarInt();
        this.recordHelper.stnHash = new HashMap<Object, Station>(2 * n);
        this.recordVar = (Structure)ds.findVariable("record");
        for (int stnIndex = 0; stnIndex < n; ++stnIndex) {
            final int lastValue = lastRecord.get(stnIndex);
            final int inventory = inventoryData.get(stnIndex);
            if (lastValue >= 0) {
                StructureData sdata = null;
                try {
                    sdata = this.recordVar.readStructure(lastValue);
                }
                catch (InvalidRangeException e) {
                    this.parseInfo.append("Invalid lastValue=" + lastValue + " for station at index " + stnIndex + "\n");
                    continue;
                }
                final String stationId = sdata.getScalarString(this.stnIdVName).trim();
                final String stationDesc = (this.stnDescVName == null) ? null : sdata.getScalarString(this.stnDescVName);
            }
        }
        final Variable timeVar = ds.findVariable(this.obsTimeVName);
        final Array timeData = timeVar.read();
        final MAMath.MinMax minmax = MAMath.getMinMax(timeData);
        this.startDate = this.timeUnit.makeDate(minmax.min);
        this.endDate = this.timeUnit.makeDate(minmax.max);
        this.setBoundingBox();
    }
    
    @Override
    protected void setTimeUnits() {
    }
    
    @Override
    protected void setStartDate() {
    }
    
    @Override
    protected void setEndDate() {
    }
    
    @Override
    protected void setBoundingBox() {
    }
    
    public List getData(final CancelTask cancel) throws IOException {
        final ArrayList allData = new ArrayList();
        for (int i = 0; i < this.getDataCount(); ++i) {
            allData.add(this.makeObs(i));
            if (cancel != null && cancel.isCancel()) {
                return null;
            }
        }
        return allData;
    }
    
    protected PointObsDatatype makeObs(final int recno) throws IOException {
        try {
            final StructureData sdata = this.recordVar.readStructure(recno);
            final float obsTime = sdata.convertScalarFloat(this.obsTimeVName);
            final float nomTime = (this.nomTimeVName == null) ? obsTime : sdata.convertScalarFloat(this.nomTimeVName);
            final float lat = sdata.convertScalarFloat("latitude");
            final float lon = sdata.convertScalarFloat("longitude");
            final float alt = sdata.convertScalarFloat(this.altVName);
            final RecordDatasetHelper recordHelper = this.recordHelper;
            recordHelper.getClass();
            return recordHelper.new RecordPointObs(new EarthLocationImpl(lat, lon, alt), obsTime, nomTime, recno);
        }
        catch (InvalidRangeException e) {
            e.printStackTrace();
            throw new IOException(e.getMessage());
        }
    }
    
    public int getDataCount() {
        final Dimension unlimitedDim = this.ncfile.getUnlimitedDimension();
        return unlimitedDim.getLength();
    }
    
    public List getData(final LatLonRect boundingBox, final CancelTask cancel) throws IOException {
        return null;
    }
    
    public List getData(final LatLonRect boundingBox, final Date start, final Date end, final CancelTask cancel) throws IOException {
        return null;
    }
    
    public DataIterator getDataIterator(final int bufferSize) throws IOException {
        return new MadisDatatypeIterator(this.recordHelper.recordVar, bufferSize);
    }
    
    private class MadisDatatypeIterator extends DatatypeIterator
    {
        @Override
        protected Object makeDatatypeWithData(final int recnum, final StructureData sdata) {
            return MadisPointObsDataset.this.recordHelper.new RecordStationObs(recnum, sdata);
        }
        
        MadisDatatypeIterator(final Structure struct, final int bufferSize) {
            super(struct, bufferSize);
        }
    }
}
