// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.dt.point;

import ucar.ma2.StructureData;
import ucar.nc2.dt.DatatypeIterator;
import ucar.nc2.Structure;
import ucar.nc2.dt.DataIterator;
import ucar.nc2.dt.StationObsDatatype;
import ucar.nc2.Dimension;
import ucar.unidata.geoloc.Station;
import java.util.List;
import java.io.IOException;
import ucar.nc2.dt.TypedDataset;
import ucar.nc2.util.CancelTask;
import ucar.nc2.dataset.NetcdfDataset;
import ucar.nc2.Variable;
import ucar.nc2.NetcdfFile;
import ucar.nc2.dt.StationImpl;
import ucar.ma2.ArrayInt;
import ucar.nc2.dt.TypedDatasetFactoryIF;

public class NdbcDataset extends StationObsDatasetImpl implements TypedDatasetFactoryIF
{
    private ArrayInt.D1 dates;
    private RecordDatasetHelper recordHelper;
    private StationImpl station;
    
    public static boolean isValidFile(final NetcdfFile ds) {
        return ds.findAttValueIgnoreCase(null, "Conventions", "").equalsIgnoreCase("COARDS") && ds.findAttValueIgnoreCase(null, "data_provider", "").equalsIgnoreCase("National Data Buoy Center") && null != ds.findAttValueIgnoreCase(null, "station", null) && null != ds.findAttValueIgnoreCase(null, "location", null) && ds.findVariable("lat") != null && ds.findVariable("lon") != null && ds.hasUnlimitedDimension();
    }
    
    public boolean isMine(final NetcdfDataset ds) {
        return isValidFile(ds);
    }
    
    public TypedDataset open(final NetcdfDataset ncd, final CancelTask task, final StringBuilder errlog) throws IOException {
        return new NdbcDataset(ncd);
    }
    
    public NdbcDataset() {
    }
    
    public NdbcDataset(final NetcdfDataset ds) throws IOException {
        super(ds);
        this.recordHelper = new RecordDatasetHelper(ds, "time", null, this.dataVariables);
        this.removeDataVariable("time");
        this.timeUnit = this.recordHelper.timeUnit;
        final Variable latVar = ds.findVariable("lat");
        final double lat = latVar.readScalarDouble();
        final Variable lonVar = ds.findVariable("lon");
        final double lon = lonVar.readScalarDouble();
        final Variable dateVar = ds.findVariable("time");
        this.dates = (ArrayInt.D1)dateVar.read();
        final int count = (int)this.dates.getSize();
        final int firstDate = this.dates.get(0);
        final int lastDate = this.dates.get(count - 1);
        this.startDate = this.timeUnit.makeDate(firstDate);
        this.endDate = this.timeUnit.makeDate(lastDate);
        final String name = ds.findAttValueIgnoreCase(null, "station", null);
        final String stationDesc = ds.findAttValueIgnoreCase(null, "description", null);
        this.station = new StationImpl(name, stationDesc, lat, lon, Double.NaN, count);
        this.stations.add(this.station);
        this.title = ds.findAttValueIgnoreCase(null, "data_provider", null) + " Station " + name;
        this.desc = this.title + "\n" + ds.findAttValueIgnoreCase(null, "data_quality", null);
        this.setBoundingBox();
    }
    
    @Override
    protected void setTimeUnits() {
    }
    
    @Override
    protected void setStartDate() {
    }
    
    @Override
    protected void setEndDate() {
    }
    
    @Override
    protected void setBoundingBox() {
        this.boundingBox = this.stationHelper.getBoundingBox();
    }
    
    public List getData(final CancelTask cancel) throws IOException {
        return this.getData(this.station, cancel);
    }
    
    public int getDataCount() {
        final Dimension unlimitedDim = this.ncfile.getUnlimitedDimension();
        return unlimitedDim.getLength();
    }
    
    public List getData(final Station s, final CancelTask cancel) throws IOException {
        final StationImpl si = (StationImpl)s;
        final int count = this.getDataCount();
        if (null == si.getObservations()) {
            for (int recno = 0; recno < count; ++recno) {
                final double time = this.dates.get(recno);
                si.addObs(this.recordHelper.new RecordStationObs(s, time, time, recno));
                if (cancel != null && cancel.isCancel()) {
                    return null;
                }
            }
        }
        return si.getObservations();
    }
    
    public DataIterator getDataIterator(final int bufferSize) throws IOException {
        return new NdbcDatatypeIterator(this.recordHelper.recordVar, bufferSize);
    }
    
    private class NdbcDatatypeIterator extends DatatypeIterator
    {
        @Override
        protected Object makeDatatypeWithData(final int recnum, final StructureData sdata) {
            return NdbcDataset.this.recordHelper.new RecordStationObs(NdbcDataset.this.station, sdata);
        }
        
        NdbcDatatypeIterator(final Structure struct, final int bufferSize) {
            super(struct, bufferSize);
        }
    }
}
