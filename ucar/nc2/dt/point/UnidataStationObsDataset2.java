// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.dt.point;

import ucar.nc2.dt.DatatypeIterator;
import ucar.nc2.dt.StationImpl;
import ucar.nc2.dt.DataIterator;
import ucar.ma2.StructureData;
import ucar.ma2.InvalidRangeException;
import ucar.nc2.dt.StationObsDatatype;
import ucar.nc2.Dimension;
import java.util.ArrayList;
import ucar.ma2.Index;
import ucar.ma2.Array;
import ucar.unidata.geoloc.Station;
import java.util.HashMap;
import ucar.ma2.ArrayChar;
import java.util.List;
import java.io.IOException;
import ucar.nc2.dt.TypedDataset;
import ucar.nc2.util.CancelTask;
import ucar.nc2.dataset.NetcdfDataset;
import ucar.nc2.Variable;
import ucar.nc2.NetcdfFile;
import ucar.nc2.Structure;
import ucar.nc2.dt.TypedDatasetFactoryIF;

public class UnidataStationObsDataset2 extends StationObsDatasetImpl implements TypedDatasetFactoryIF
{
    private Structure recordVar;
    private RecordDatasetHelper recordHelper;
    private boolean debugRead;
    
    public static boolean isValidFile(final NetcdfFile ds) {
        return ds.findAttValueIgnoreCase(null, "Conventions", "").equalsIgnoreCase("Unidata Station Format v1.0");
    }
    
    public boolean isMine(final NetcdfDataset ds) {
        return isValidFile(ds);
    }
    
    public TypedDataset open(final NetcdfDataset ncd, final CancelTask task, final StringBuilder errlog) throws IOException {
        return new UnidataStationObsDataset2(ncd);
    }
    
    public UnidataStationObsDataset2() {
        this.debugRead = false;
    }
    
    public UnidataStationObsDataset2(final NetcdfDataset ds) throws IOException {
        super(ds);
        this.debugRead = false;
        this.title = "Station Data from NWS";
        this.desc = "Station Data from NWS distributed through the Unidata IDD realtime datastream. Decoded into netCDF files by metar2nc (new). Usually 1 hour of data";
        (this.recordHelper = new RecordDatasetHelper(ds, "time_observation", "time_nominal", this.dataVariables, this.parseInfo)).setStationInfo("station_index", "location");
        this.removeDataVariable("time_observation");
        this.removeDataVariable("time_nominal");
        this.removeDataVariable("previousReport");
        this.removeDataVariable("station_index");
        this.recordVar = this.recordHelper.recordVar;
        this.timeUnit = this.recordHelper.timeUnit;
        final ArrayChar stationIdArray = (ArrayChar)ds.findVariable("id").read();
        final ArrayChar stationDescArray = (ArrayChar)ds.findVariable("location").read();
        final Array latArray = ds.findVariable("latitude").read();
        final Array lonArray = ds.findVariable("longitude").read();
        final Array elevArray = ds.findVariable("elevation").read();
        final Array lastRecordArray = ds.findVariable("lastReport").read();
        final Array numReportsArray = ds.findVariable("numberReports").read();
        final Index ima = lastRecordArray.getIndex();
        final int n = ds.findVariable("number_stations").readScalarInt();
        this.recordHelper.stnHash = new HashMap<Object, Station>(2 * n);
        for (int i = 0; i < n; ++i) {
            ima.set(i);
            final String stationName = stationIdArray.getString(i).trim();
            final String stationDesc = stationDescArray.getString(i).trim();
            final UnidataStationImpl bean = new UnidataStationImpl(stationName, stationDesc, (double)latArray.getFloat(ima), (double)lonArray.getFloat(ima), (double)elevArray.getFloat(ima), lastRecordArray.getInt(ima), numReportsArray.getInt(ima));
            this.stations.add(bean);
            this.recordHelper.stnHash.put(new Integer(i), bean);
        }
        this.setBoundingBox();
        final Variable minTimeVar = ds.findVariable("minimum_time_observation");
        final int minTimeValue = minTimeVar.readScalarInt();
        this.startDate = this.timeUnit.makeDate(minTimeValue);
        final Variable maxTimeVar = ds.findVariable("maximum_time_observation");
        final int maxTimeValue = maxTimeVar.readScalarInt();
        this.endDate = this.timeUnit.makeDate(maxTimeValue);
    }
    
    @Override
    protected void setTimeUnits() {
    }
    
    @Override
    protected void setStartDate() {
    }
    
    @Override
    protected void setEndDate() {
    }
    
    @Override
    protected void setBoundingBox() {
        this.boundingBox = this.stationHelper.getBoundingBox();
    }
    
    public List getData(final CancelTask cancel) throws IOException {
        final ArrayList allData = new ArrayList();
        for (int i = 0; i < this.getDataCount(); ++i) {
            allData.add(this.makeObs(i));
            if (cancel != null && cancel.isCancel()) {
                return null;
            }
        }
        return allData;
    }
    
    public int getDataCount() {
        final Dimension unlimitedDim = this.ncfile.getUnlimitedDimension();
        return unlimitedDim.getLength();
    }
    
    public List getData(final Station s, final CancelTask cancel) throws IOException {
        return ((UnidataStationImpl)s).getObservations();
    }
    
    protected StationObsDatatype makeObs(final int recno) throws IOException {
        try {
            final StructureData sdata = this.recordVar.readStructure(recno);
            final int stationIndex = sdata.getScalarInt("station_index");
            final Station station = this.stations.get(stationIndex);
            if (station == null) {
                this.parseInfo.append("cant find station at index = " + stationIndex + "\n");
            }
            final float obsTime = sdata.convertScalarFloat("time_observation");
            final float nomTime = sdata.convertScalarFloat("time_nominal");
            final RecordDatasetHelper recordHelper = this.recordHelper;
            recordHelper.getClass();
            return recordHelper.new RecordStationObs(station, obsTime, nomTime, recno);
        }
        catch (InvalidRangeException e) {
            e.printStackTrace();
            throw new IOException(e.getMessage());
        }
    }
    
    public DataIterator getDataIterator(final int bufferSize) throws IOException {
        return new StationDatatypeIterator(this.recordHelper.recordVar, bufferSize);
    }
    
    private class UnidataStationImpl extends StationImpl
    {
        private int lastRecord;
        
        private UnidataStationImpl(final String name, final String desc, final double lat, final double lon, final double elev, final int lastRecord, final int count) {
            super(name, desc, lat, lon, elev, count);
            this.lastRecord = lastRecord;
        }
        
        @Override
        protected ArrayList readObservations() throws IOException {
            final ArrayList obs = new ArrayList();
            int recno = this.lastRecord;
            while (recno >= 0) {
                try {
                    if (UnidataStationObsDataset2.this.debugRead) {
                        System.out.println(this.name + " try to read at record " + recno);
                    }
                    final StructureData sdata = UnidataStationObsDataset2.this.recordVar.readStructure(recno);
                    final int prevRecord = sdata.getScalarInt("previousReport");
                    final float obsTime = sdata.convertScalarFloat("time_observation");
                    final float nomTime = sdata.convertScalarFloat("time_nominal");
                    final ArrayList list = obs;
                    final int index = 0;
                    final RecordDatasetHelper access$300 = UnidataStationObsDataset2.this.recordHelper;
                    access$300.getClass();
                    list.add(index, access$300.new RecordStationObs(this, obsTime, nomTime, recno));
                    recno = prevRecord;
                    continue;
                }
                catch (InvalidRangeException e) {
                    e.printStackTrace();
                    throw new IOException(e.getMessage());
                }
                break;
            }
            return obs;
        }
    }
    
    private class StationDatatypeIterator extends DatatypeIterator
    {
        @Override
        protected Object makeDatatypeWithData(final int recnum, final StructureData sdata) {
            return UnidataStationObsDataset2.this.recordHelper.new RecordStationObs(recnum, sdata);
        }
        
        StationDatatypeIterator(final Structure struct, final int bufferSize) {
            super(struct, bufferSize);
        }
    }
}
