// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.dt.point;

import ucar.ma2.StructureData;
import ucar.nc2.dt.DatatypeIterator;
import ucar.nc2.Structure;
import ucar.nc2.dt.DataIterator;
import ucar.nc2.dt.StationImpl;
import java.util.Date;
import ucar.unidata.geoloc.LatLonRect;
import java.util.Collection;
import ucar.unidata.geoloc.Station;
import java.util.List;
import ucar.nc2.ncml.NcMLReader;
import java.io.IOException;
import ucar.nc2.dt.TypedDataset;
import ucar.nc2.util.CancelTask;
import ucar.nc2.Variable;
import ucar.nc2.NetcdfFile;
import java.util.ArrayList;
import ucar.nc2.dataset.NetcdfDataset;
import ucar.nc2.dt.TypedDatasetFactoryIF;

public class OldUnidataStationObsDataset extends StationObsDatasetImpl implements TypedDatasetFactoryIF
{
    private NetcdfDataset dataset;
    private RecordDatasetHelper recordHelper;
    private ArrayList records;
    private String latName;
    private String lonName;
    private String elevName;
    private String descName;
    private String timeName;
    private String timeNominalName;
    private String stationIdName;
    
    public static boolean isValidFile(final NetcdfFile ds) {
        final String kind = ds.findAttValueIgnoreCase(null, "title", null);
        return kind != null && ("METAR definition".equals(kind) || "SYNOPTIC definition".equals(kind) || "BUOY definition".equals(kind));
    }
    
    public boolean isMine(final NetcdfDataset ds) {
        return isValidFile(ds);
    }
    
    public TypedDataset open(final NetcdfDataset ncd, final CancelTask task, final StringBuilder errlog) throws IOException {
        return new OldUnidataStationObsDataset(ncd);
    }
    
    public OldUnidataStationObsDataset() {
    }
    
    public OldUnidataStationObsDataset(final NetcdfDataset ds) throws IOException {
        super(ds);
        this.dataset = ds;
        String ncmlURL = null;
        final String kind = ds.findAttValueIgnoreCase(null, "title", null);
        if ("METAR definition".equals(kind)) {
            final Variable v = ds.findVariable("station");
            if (v != null) {
                ncmlURL = "resources/nj22/coords/metar2ncMetar.ncml";
            }
            else {
                ncmlURL = "resources/nj22/coords/metar2ncMetar2.ncml";
            }
        }
        else if ("SYNOPTIC definition".equals(kind)) {
            ncmlURL = "resources/nj22/coords/metar2ncSynoptic.ncml";
        }
        else if ("BUOY definition".equals(kind)) {
            ncmlURL = "resources/nj22/coords/metar2ncBuoy.ncml";
        }
        if (ncmlURL == null) {
            throw new IOException("unknown StationObsDataset type " + ds.getLocation());
        }
        this.init(ncmlURL);
    }
    
    public OldUnidataStationObsDataset(final NetcdfDataset ds, final String ncmlURL) throws IOException {
        super(ds);
        this.dataset = ds;
        this.init(ncmlURL);
    }
    
    protected void init(final String ncmlURL) throws IOException {
        NcMLReader.wrapNcMLresource(this.dataset, ncmlURL, null);
        this.stationIdName = this.dataset.findAttValueIgnoreCase(null, "_StationIdVar", null);
        this.descName = this.dataset.findAttValueIgnoreCase(null, "_StationDescVar", null);
        this.latName = this.dataset.findAttValueIgnoreCase(null, "_StationLatVar", null);
        this.lonName = this.dataset.findAttValueIgnoreCase(null, "_StationLonVar", null);
        this.elevName = this.dataset.findAttValueIgnoreCase(null, "_StationElevVar", null);
        this.timeName = this.dataset.findAttValueIgnoreCase(null, "_StationTimeVar", null);
        this.timeNominalName = this.dataset.findAttValueIgnoreCase(null, "_StationTimeNominalVar", null);
        (this.recordHelper = new RecordDatasetHelper(this.dataset, this.timeName, this.timeNominalName, this.dataVariables)).setStationInfo(this.stationIdName, this.descName);
        this.recordHelper.setLocationInfo(this.latName, this.lonName, this.elevName);
        this.removeDataVariable(this.latName);
        this.removeDataVariable(this.lonName);
        this.removeDataVariable(this.elevName);
        this.removeDataVariable(this.timeName);
        this.removeDataVariable(this.timeNominalName);
        this.records = this.recordHelper.readAllCreateObs(null);
        this.stations = new ArrayList<Station>(this.recordHelper.stnHash.values());
        this.setTimeUnits();
        this.setStartDate();
        this.setEndDate();
        this.setBoundingBox();
    }
    
    @Override
    protected void setTimeUnits() {
        this.timeUnit = this.recordHelper.timeUnit;
    }
    
    @Override
    protected void setStartDate() {
        this.startDate = this.timeUnit.makeDate(this.recordHelper.minDate);
    }
    
    @Override
    protected void setEndDate() {
        this.endDate = this.timeUnit.makeDate(this.recordHelper.maxDate);
    }
    
    @Override
    protected void setBoundingBox() {
        this.boundingBox = this.recordHelper.boundingBox;
    }
    
    public List getData(final CancelTask cancel) throws IOException {
        return this.records;
    }
    
    public int getDataCount() {
        return this.records.size();
    }
    
    @Override
    public List getData(final LatLonRect boundingBox, final CancelTask cancel) throws IOException {
        return this.recordHelper.getData(this.records, boundingBox, cancel);
    }
    
    @Override
    public List getData(final LatLonRect boundingBox, final Date start, final Date end, final CancelTask cancel) throws IOException {
        final double startTime = this.timeUnit.makeValue(start);
        final double endTime = this.timeUnit.makeValue(end);
        return this.recordHelper.getData(this.records, boundingBox, startTime, endTime, cancel);
    }
    
    @Override
    public int getStationDataCount(final Station s) {
        final StationImpl si = (StationImpl)s;
        return si.getNumObservations();
    }
    
    public List getData(final Station s, final CancelTask cancel) throws IOException {
        return ((StationImpl)s).getObservations();
    }
    
    public DataIterator getDataIterator(final int bufferSize) throws IOException {
        return new StationDatatypeIterator(this.recordHelper.recordVar, bufferSize);
    }
    
    private class StationDatatypeIterator extends DatatypeIterator
    {
        @Override
        protected Object makeDatatypeWithData(final int recno, final StructureData sdata) {
            return OldUnidataStationObsDataset.this.recordHelper.new RecordStationObs(recno, sdata);
        }
        
        StationDatatypeIterator(final Structure struct, final int bufferSize) {
            super(struct, bufferSize);
        }
    }
}
