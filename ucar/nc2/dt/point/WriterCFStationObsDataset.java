// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.dt.point;

import ucar.nc2.iosp.netcdf3.N3outputStreamWriter;
import java.io.File;
import ucar.nc2.dataset.NetcdfDataset;
import ucar.nc2.NetcdfFile;
import ucar.nc2.dt.DataIterator;
import java.io.OutputStream;
import java.io.FileOutputStream;
import ucar.nc2.util.CancelTask;
import ucar.nc2.dt.TypedDatasetFactory;
import ucar.nc2.constants.FeatureType;
import ucar.nc2.dt.StationObsDataset;
import ucar.unidata.geoloc.LatLonPoint;
import ucar.unidata.geoloc.LatLonPointImpl;
import ucar.ma2.StructureData;
import ucar.nc2.dt.StationObsDatatype;
import ucar.ma2.ArrayChar;
import ucar.ma2.ArrayDouble;
import java.util.Iterator;
import java.util.Collection;
import ucar.ma2.Array;
import ucar.unidata.geoloc.LatLonRect;
import ucar.ma2.DataType;
import ucar.nc2.Group;
import ucar.nc2.Attribute;
import java.io.IOException;
import ucar.nc2.VariableSimpleIF;
import java.util.ArrayList;
import java.util.HashSet;
import java.io.DataOutputStream;
import java.util.HashMap;
import ucar.ma2.ArrayInt;
import java.util.Date;
import ucar.nc2.Variable;
import ucar.unidata.geoloc.Station;
import java.util.List;
import ucar.nc2.Dimension;
import java.util.Set;
import ucar.nc2.units.DateFormatter;

public class WriterCFStationObsDataset
{
    private static final String recordDimName = "record";
    private static final String stationDimName = "station";
    private static final String latName = "latitude";
    private static final String lonName = "longitude";
    private static final String altName = "altitude";
    private static final String idName = "station_id";
    private static final String descName = "station_description";
    private static final String wmoName = "wmo_id";
    private static final String timeName = "time";
    private static final String parentName = "parent_index";
    private DateFormatter dateFormatter;
    private int name_strlen;
    private int desc_strlen;
    private int wmo_strlen;
    private NetcdfFileStream ncfile;
    private String title;
    private Set<Dimension> dimSet;
    private List<Station> stnList;
    private List<Variable> recordVars;
    private Date minDate;
    private Date maxDate;
    private boolean useAlt;
    private boolean useWmoId;
    private boolean debug;
    private ArrayInt.D1 timeArray;
    private ArrayInt.D1 parentArray;
    private HashMap<String, StationTracker> stationMap;
    
    public WriterCFStationObsDataset(final DataOutputStream stream, final String title) {
        this.dateFormatter = new DateFormatter();
        this.dimSet = new HashSet<Dimension>();
        this.recordVars = new ArrayList<Variable>();
        this.minDate = null;
        this.maxDate = null;
        this.useAlt = false;
        this.useWmoId = false;
        this.debug = false;
        this.timeArray = new ArrayInt.D1(1);
        this.parentArray = new ArrayInt.D1(1);
        this.ncfile = new NetcdfFileStream(stream);
        this.title = title;
    }
    
    public void writeHeader(final List<Station> stns, final List<VariableSimpleIF> vars, final int numrec) throws IOException {
        this.createGlobalAttributes();
        this.createStations(stns);
        this.createRecordVariables(vars);
        this.ncfile.finish();
        this.ncfile.writeHeader(numrec);
        this.writeStationData(stns);
    }
    
    private void createGlobalAttributes() {
        this.ncfile.addAttribute(null, new Attribute("Conventions", "CF-1.0"));
        this.ncfile.addAttribute(null, new Attribute("cdm_datatype", "Station"));
        this.ncfile.addAttribute(null, new Attribute("title", this.title));
    }
    
    private void createStations(final List<Station> stnList) throws IOException {
        final int nstns = stnList.size();
        for (int i = 0; i < nstns; ++i) {
            final Station stn = stnList.get(i);
            if (!Double.isNaN(stn.getAltitude())) {
                this.useAlt = true;
            }
            if (stn.getWmoId() != null && stn.getWmoId().trim().length() > 0) {
                this.useWmoId = true;
            }
        }
        for (int i = 0; i < nstns; ++i) {
            final Station station = stnList.get(i);
            this.name_strlen = Math.max(this.name_strlen, station.getName().length());
            this.desc_strlen = Math.max(this.desc_strlen, station.getDescription().length());
            if (this.useWmoId) {
                this.wmo_strlen = Math.max(this.wmo_strlen, station.getName().length());
            }
        }
        final LatLonRect llbb = this.getBoundingBox(stnList);
        this.ncfile.addAttribute(null, new Attribute("geospatial_lat_min", Double.toString(llbb.getLowerLeftPoint().getLatitude())));
        this.ncfile.addAttribute(null, new Attribute("geospatial_lat_max", Double.toString(llbb.getUpperRightPoint().getLatitude())));
        this.ncfile.addAttribute(null, new Attribute("geospatial_lon_min", Double.toString(llbb.getLowerLeftPoint().getLongitude())));
        this.ncfile.addAttribute(null, new Attribute("geospatial_lon_max", Double.toString(llbb.getUpperRightPoint().getLongitude())));
        this.ncfile.addDimension(null, new Dimension("record", 0, true, true, false));
        this.ncfile.addDimension(null, new Dimension("station", nstns));
        Variable v = this.ncfile.addVariable(null, "latitude", DataType.DOUBLE, "station");
        v.addAttribute(new Attribute("units", "degrees_north"));
        v.addAttribute(new Attribute("long_name", "station latitude"));
        v = this.ncfile.addVariable(null, "longitude", DataType.DOUBLE, "station");
        v.addAttribute(new Attribute("units", "degrees_east"));
        v.addAttribute(new Attribute("long_name", "station longitude"));
        if (this.useAlt) {
            v = this.ncfile.addVariable(null, "altitude", DataType.DOUBLE, "station");
            v.addAttribute(new Attribute("units", "meters"));
            v.addAttribute(new Attribute("long_name", "station altitude"));
            v.addAttribute(new Attribute("positive", "up"));
        }
        v = this.ncfile.addStringVariable(null, "station_id", "station", this.name_strlen);
        v.addAttribute(new Attribute("long_name", "station identifier"));
        v = this.ncfile.addStringVariable(null, "station_description", "station", this.desc_strlen);
        v.addAttribute(new Attribute("long_name", "station description"));
        if (this.useWmoId) {
            v = this.ncfile.addStringVariable(null, "wmo_id", "station", this.wmo_strlen);
            v.addAttribute(new Attribute("long_name", "station WMO id"));
        }
    }
    
    private void createRecordVariables(final List<VariableSimpleIF> dataVars) {
        final Variable timeVar = this.ncfile.addVariable(null, "time", DataType.INT, "record");
        timeVar.addAttribute(new Attribute("units", "secs since 1970-01-01 00:00:00"));
        timeVar.addAttribute(new Attribute("long_name", "calendar date"));
        this.recordVars.add(timeVar);
        timeVar.setCachedData(this.timeArray, false);
        final Variable parentVar = this.ncfile.addVariable(null, "parent_index", DataType.INT, "record");
        parentVar.addAttribute(new Attribute("long_name", "index of parent station"));
        this.recordVars.add(parentVar);
        parentVar.setCachedData(this.parentArray, false);
        final Attribute coordAtt = new Attribute("coordinates", this.useAlt ? "latitude longitude altitude time" : "latitude longitude time");
        for (final VariableSimpleIF var : dataVars) {
            final List<Dimension> dims = var.getDimensions();
            this.dimSet.addAll(dims);
        }
        for (final Dimension d : this.dimSet) {
            if (!d.isUnlimited()) {
                this.ncfile.addDimension(null, new Dimension(d.getName(), d.getLength(), d.isShared(), false, d.isVariableLength()));
            }
        }
        for (final VariableSimpleIF oldVar : dataVars) {
            final List<Dimension> dims = oldVar.getDimensions();
            final StringBuffer dimNames = new StringBuffer("record");
            for (final Dimension d2 : dims) {
                if (!d2.isUnlimited()) {
                    dimNames.append(" ").append(d2.getName());
                }
            }
            final Variable newVar = this.ncfile.addVariable(null, oldVar.getName(), oldVar.getDataType(), dimNames.toString());
            this.recordVars.add(newVar);
            final List<Attribute> atts = oldVar.getAttributes();
            for (final Attribute att : atts) {
                newVar.addAttribute(att);
            }
            newVar.addAttribute(coordAtt);
        }
    }
    
    private void writeStationData(final List<Station> stnList) throws IOException {
        this.stnList = stnList;
        final int nstns = stnList.size();
        this.stationMap = new HashMap<String, StationTracker>(2 * nstns);
        if (this.debug) {
            System.out.println("stationMap created");
        }
        final ArrayDouble.D1 latArray = new ArrayDouble.D1(nstns);
        final ArrayDouble.D1 lonArray = new ArrayDouble.D1(nstns);
        final ArrayDouble.D1 altArray = new ArrayDouble.D1(nstns);
        final ArrayChar.D2 idArray = new ArrayChar.D2(nstns, this.name_strlen);
        final ArrayChar.D2 descArray = new ArrayChar.D2(nstns, this.desc_strlen);
        final ArrayChar.D2 wmoArray = new ArrayChar.D2(nstns, this.wmo_strlen);
        for (int i = 0; i < stnList.size(); ++i) {
            final Station stn = stnList.get(i);
            this.stationMap.put(stn.getName(), new StationTracker(i));
            latArray.set(i, stn.getLatitude());
            lonArray.set(i, stn.getLongitude());
            if (this.useAlt) {
                altArray.set(i, stn.getAltitude());
            }
            idArray.setString(i, stn.getName());
            descArray.setString(i, stn.getDescription());
            if (this.useWmoId) {
                wmoArray.setString(i, stn.getWmoId());
            }
        }
        try {
            this.ncfile.writeNonRecordData("latitude", latArray);
            this.ncfile.writeNonRecordData("longitude", lonArray);
            if (this.useAlt) {
                this.ncfile.writeNonRecordData("altitude", altArray);
            }
            this.ncfile.writeNonRecordData("station_id", idArray);
            this.ncfile.writeNonRecordData("station_description", descArray);
            if (this.useWmoId) {
                this.ncfile.writeNonRecordData("wmo_id", wmoArray);
            }
        }
        catch (Exception e) {
            e.printStackTrace();
            throw new IllegalStateException(e);
        }
    }
    
    public void writeRecord(final StationObsDatatype sobs, final StructureData sdata) throws IOException {
        if (this.debug) {
            System.out.println("sobs= " + sobs + "; station = " + sobs.getStation());
        }
        for (final Variable v : this.recordVars) {
            if ("time".equals(v.getShortName())) {
                final Date d = sobs.getObservationTimeAsDate();
                final int secs = (int)(d.getTime() / 1000L);
                this.timeArray.set(0, secs);
            }
            else if ("parent_index".equals(v.getShortName())) {
                final int stationIndex = this.stnList.indexOf(sobs.getStation());
                this.parentArray.set(0, stationIndex);
            }
            else {
                v.setCachedData(sdata.getArray(v.getShortName()), false);
            }
        }
        this.ncfile.writeRecordData(this.recordVars);
    }
    
    public void finish() throws IOException {
        this.ncfile.close();
    }
    
    private LatLonRect getBoundingBox(final List stnList) {
        Station s = stnList.get(0);
        final LatLonPointImpl llpt = new LatLonPointImpl();
        llpt.set(s.getLatitude(), s.getLongitude());
        final LatLonRect rect = new LatLonRect(llpt, 0.001, 0.001);
        for (int i = 1; i < stnList.size(); ++i) {
            s = stnList.get(i);
            llpt.set(s.getLatitude(), s.getLongitude());
            rect.extend(llpt);
        }
        return rect;
    }
    
    public static void main3(final String[] args) throws IOException {
        final long start = System.currentTimeMillis();
        final String location = "C:/data/metars/Surface_METAR_20070329_0000.nc";
        final StringBuilder errlog = new StringBuilder();
        final StationObsDataset sobs = (StationObsDataset)TypedDatasetFactory.open(FeatureType.STATION, location, null, errlog);
        final String fileOut = "C:/temp/Surface_METAR_20070329_0000.stream.nc";
        final FileOutputStream fos = new FileOutputStream(fileOut);
        final DataOutputStream out = new DataOutputStream(fos);
        System.out.println("Read " + location + " write to " + fileOut);
        final WriterCFStationObsDataset writer = new WriterCFStationObsDataset(out, "test");
        final List stns = sobs.getStations();
        final List<Station> stnList = new ArrayList<Station>();
        final Station s = stns.get(0);
        stnList.add(s);
        final List<VariableSimpleIF> varList = new ArrayList<VariableSimpleIF>();
        varList.add(sobs.getDataVariable("wind_speed"));
        writer.writeHeader(stnList, varList, -1);
        final DataIterator iter = sobs.getDataIterator(s);
        while (iter.hasNext()) {
            final StationObsDatatype sobsData = (StationObsDatatype)iter.nextData();
            final StructureData data = sobsData.getData();
            writer.writeRecord(sobsData, data);
        }
        writer.finish();
        final long took = System.currentTimeMillis() - start;
        System.out.println("That took = " + took + " msecs");
    }
    
    public static void rewrite(final String fileIn, final String fileOut, final boolean inMemory, final boolean sort) throws IOException {
        System.out.println("Rewrite .nc files from " + fileIn + " to " + fileOut + "inMem= " + inMemory + " sort= " + sort);
        final long start = System.currentTimeMillis();
        final NetcdfFile ncfile = inMemory ? NetcdfFile.openInMemory(fileIn) : NetcdfFile.open(fileIn);
        final NetcdfDataset ncd = new NetcdfDataset(ncfile);
        final StringBuilder errlog = new StringBuilder();
        final StationObsDataset sobs = (StationObsDataset)TypedDatasetFactory.open(FeatureType.STATION, ncd, null, errlog);
        final List<Station> stns = sobs.getStations();
        final List<VariableSimpleIF> vars = sobs.getDataVariables();
        final FileOutputStream fos = new FileOutputStream(fileOut);
        final DataOutputStream out = new DataOutputStream(fos);
        final WriterCFStationObsDataset writer = new WriterCFStationObsDataset(out, "rewrite " + fileIn);
        writer.writeHeader(stns, vars, -1);
        if (sort) {
            for (final Station s : stns) {
                final DataIterator iter = sobs.getDataIterator(s);
                while (iter.hasNext()) {
                    final StationObsDatatype sobsData = (StationObsDatatype)iter.nextData();
                    final StructureData data = sobsData.getData();
                    writer.writeRecord(sobsData, data);
                }
            }
        }
        else {
            final DataIterator iter2 = sobs.getDataIterator(1000000);
            while (iter2.hasNext()) {
                final StationObsDatatype sobsData2 = (StationObsDatatype)iter2.nextData();
                final StructureData data2 = sobsData2.getData();
                writer.writeRecord(sobsData2, data2);
            }
        }
        writer.finish();
        final long took = System.currentTimeMillis() - start;
        System.out.println("Rewrite " + fileIn + " to " + fileOut + " took = " + took);
    }
    
    public static void main(final String[] args) throws IOException {
        final String location = "C:/data/metars/Surface_METAR_20070329_0000.nc";
        final File file = new File(location);
        rewrite(location, "C:/temp/FU" + file.getName(), false, false);
        rewrite(location, "C:/temp/FS" + file.getName(), false, true);
        rewrite(location, "C:/temp/MU" + file.getName(), true, false);
        rewrite(location, "C:/temp/MS" + file.getName(), true, true);
    }
    
    public static void main2(final String[] args) throws IOException {
        final long start = System.currentTimeMillis();
        String toLocation = "C:/temp2/";
        String fromLocation = "C:/data/metars/";
        if (args.length > 1) {
            fromLocation = args[0];
            toLocation = args[1];
        }
        System.out.println("Rewrite .nc files from " + fromLocation + " to " + toLocation);
        final File dir = new File(fromLocation);
        final File[] arr$;
        final File[] files = arr$ = dir.listFiles();
        for (final File file : arr$) {
            if (file.getName().endsWith(".nc")) {
                rewrite(file.getAbsolutePath(), toLocation + file.getName(), true, true);
            }
        }
        final long took = System.currentTimeMillis() - start;
        System.out.println("That took = " + took);
    }
    
    private class NetcdfFileStream extends NetcdfFile
    {
        N3outputStreamWriter swriter;
        DataOutputStream stream;
        
        NetcdfFileStream(final DataOutputStream stream) {
            this.stream = stream;
            this.swriter = new N3outputStreamWriter(this);
        }
        
        void writeHeader(final int numrec) throws IOException {
            this.swriter.writeHeader(this.stream, numrec);
        }
        
        void writeNonRecordData(final String varName, final Array data) throws IOException {
            this.swriter.writeNonRecordData(this.findVariable(varName), this.stream, data);
        }
        
        void writeRecordData(final List<Variable> varList) throws IOException {
            this.swriter.writeRecordData(this.stream, varList);
        }
    }
    
    private class StationTracker
    {
        int numChildren;
        int lastChild;
        int parent_index;
        List<Integer> link;
        
        StationTracker(final int parent_index) {
            this.numChildren = 0;
            this.lastChild = -1;
            this.link = new ArrayList<Integer>();
            this.parent_index = parent_index;
        }
    }
}
