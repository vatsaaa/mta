// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.dt.point;

import ucar.ma2.StructureData;
import ucar.nc2.dt.DatatypeIterator;
import ucar.nc2.dods.DODSNetcdfFile;
import ucar.nc2.dt.DataIterator;
import ucar.unidata.geoloc.Station;
import java.util.ArrayList;
import ucar.nc2.Dimension;
import java.util.List;
import ucar.nc2.constants.AxisType;
import java.io.IOException;
import ucar.nc2.dt.TypedDataset;
import ucar.nc2.util.CancelTask;
import ucar.nc2.dataset.NetcdfDataset;
import java.util.StringTokenizer;
import ucar.nc2.constants.FeatureType;
import ucar.nc2.NetcdfFile;
import ucar.nc2.Structure;
import ucar.nc2.Variable;
import ucar.nc2.dt.TypedDatasetFactoryIF;

public class SequenceObsDataset extends StationObsDatasetImpl implements TypedDatasetFactoryIF
{
    private Variable latVar;
    private Variable lonVar;
    private Variable altVar;
    private Variable timeVar;
    private Variable timeNominalVar;
    private Variable stationIdVar;
    private Variable stationDescVar;
    private Variable numStationsVar;
    private Structure sequenceVar;
    private SequenceHelper sequenceHelper;
    private boolean debugRead;
    private boolean fatal;
    
    public static boolean isValidFile(final NetcdfFile ds) {
        if (!ds.findAttValueIgnoreCase(null, "cdm_data_type", "").equalsIgnoreCase(FeatureType.STATION.toString()) && !ds.findAttValueIgnoreCase(null, "cdm_datatype", "").equalsIgnoreCase(FeatureType.STATION.toString())) {
            return false;
        }
        final String conv = ds.findAttValueIgnoreCase(null, "Conventions", null);
        if (conv == null) {
            return false;
        }
        final StringTokenizer stoke = new StringTokenizer(conv, ",");
        while (stoke.hasMoreTokens()) {
            final String toke = stoke.nextToken().trim();
            if (toke.equalsIgnoreCase("Unidata Sequence Observation Dataset v1.0")) {
                return true;
            }
        }
        return false;
    }
    
    public boolean isMine(final NetcdfDataset ds) {
        return isValidFile(ds);
    }
    
    public TypedDataset open(final NetcdfDataset ncd, final CancelTask task, final StringBuilder errlog) throws IOException {
        return new SequenceObsDataset(ncd, task);
    }
    
    public SequenceObsDataset() {
        this.debugRead = false;
        this.fatal = false;
    }
    
    public SequenceObsDataset(final NetcdfDataset ds, final CancelTask task) throws IOException {
        super(ds);
        this.debugRead = false;
        this.fatal = false;
        this.sequenceVar = (Structure)this.findVariable(ds, "obs_sequence");
        this.latVar = this.findVariable(ds, "latitude_coordinate");
        this.lonVar = this.findVariable(ds, "longitude_coordinate");
        this.altVar = this.findVariable(ds, "zaxis_coordinate");
        this.timeVar = this.findVariable(ds, "time_coordinate");
        if (this.latVar == null) {
            this.parseInfo.append("Missing latitude variable");
            this.fatal = true;
        }
        if (this.lonVar == null) {
            this.parseInfo.append("Missing longitude variable");
            this.fatal = true;
        }
        if (this.altVar == null) {
            this.parseInfo.append("Missing altitude variable");
        }
        if (this.timeVar == null) {
            this.parseInfo.append("Missing time variable");
            this.fatal = true;
        }
        this.timeNominalVar = this.findVariable(ds, "time_nominal");
        this.stationIdVar = this.findVariable(ds, "station_id");
        this.stationDescVar = this.findVariable(ds, "station_description");
        this.numStationsVar = this.findVariable(ds, "number_stations");
        if (this.stationIdVar == null) {
            this.parseInfo.append("Missing station id variable");
            this.fatal = true;
        }
        this.title = ds.findAttValueIgnoreCase(null, "title", "");
        this.desc = ds.findAttValueIgnoreCase(null, "description", "");
    }
    
    private Variable getCoordinate(final NetcdfDataset ds, final Structure sequenceVar, final AxisType a) {
        List varList = ds.getVariables();
        for (int i = 0; i < varList.size(); ++i) {
            final Variable v = varList.get(i);
            final String axisType = ds.findAttValueIgnoreCase(v, "_CoordinateAxisType", null);
            if (axisType != null && axisType.equals(a.toString())) {
                return v;
            }
        }
        varList = sequenceVar.getVariables();
        for (int i = 0; i < varList.size(); ++i) {
            final Variable v = varList.get(i);
            final String axisType = ds.findAttValueIgnoreCase(v, "_CoordinateAxisType", null);
            if (axisType != null && axisType.equals(a.toString())) {
                return v;
            }
        }
        return null;
    }
    
    private Variable findVariable(final NetcdfDataset ds, final String name) {
        Variable result = ds.findVariable(name);
        if (result == null) {
            String aname = ds.findAttValueIgnoreCase(null, name + "_variable", null);
            if (aname == null) {
                aname = ds.findAttValueIgnoreCase(null, name, null);
            }
            if (aname != null) {
                result = ds.findVariable(aname);
            }
        }
        return result;
    }
    
    private Dimension findDimension(final NetcdfDataset ds, final String name) {
        Dimension result = ds.findDimension(name);
        if (result == null) {
            final String aname = ds.findAttValueIgnoreCase(null, name + "Dimension", null);
            if (aname != null) {
                result = ds.findDimension(aname);
            }
        }
        return result;
    }
    
    @Override
    protected void setTimeUnits() {
    }
    
    @Override
    protected void setStartDate() {
    }
    
    @Override
    protected void setEndDate() {
    }
    
    @Override
    protected void setBoundingBox() {
        this.boundingBox = this.stationHelper.getBoundingBox();
    }
    
    public List getData(final CancelTask cancel) throws IOException {
        final ArrayList allData = new ArrayList();
        for (int i = 0; i < this.getDataCount(); ++i) {
            if (cancel != null && cancel.isCancel()) {
                return null;
            }
        }
        return allData;
    }
    
    public int getDataCount() {
        final Dimension unlimitedDim = this.ncfile.getUnlimitedDimension();
        return unlimitedDim.getLength();
    }
    
    public List getData(final Station s, final CancelTask cancel) throws IOException {
        return null;
    }
    
    public DataIterator getDataIterator(final int bufferSize) throws IOException {
        return null;
    }
    
    public static void main(final String[] args) throws IOException {
        DODSNetcdfFile.debugServerCall = true;
        DODSNetcdfFile.debugCE = true;
        DODSNetcdfFile.debugDataResult = true;
        DODSNetcdfFile.debugConvertData = true;
        final NetcdfDataset ds = NetcdfDataset.openDataset("C:/data/ncml/oceanwatch.ncml");
        new SequenceObsDataset(ds, null);
    }
    
    private class SeqDatatypeIterator extends DatatypeIterator
    {
        @Override
        protected Object makeDatatypeWithData(final int recnum, final StructureData sdata) {
            return null;
        }
        
        SeqDatatypeIterator(final Structure struct, final int bufferSize) {
            super(struct, bufferSize);
        }
    }
}
