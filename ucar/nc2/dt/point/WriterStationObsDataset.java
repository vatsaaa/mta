// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.dt.point;

import java.io.File;
import ucar.nc2.dataset.NetcdfDataset;
import ucar.nc2.NetcdfFile;
import ucar.nc2.util.CancelTask;
import ucar.nc2.dt.TypedDatasetFactory;
import ucar.nc2.constants.FeatureType;
import ucar.nc2.dt.DataIterator;
import ucar.nc2.dt.StationObsDataset;
import ucar.unidata.geoloc.LatLonPoint;
import ucar.unidata.geoloc.LatLonPointImpl;
import ucar.ma2.ArrayStructureW;
import ucar.ma2.StructureData;
import ucar.nc2.dt.StationObsDatatype;
import ucar.ma2.InvalidRangeException;
import ucar.ma2.Array;
import ucar.ma2.ArrayDouble;
import java.util.Iterator;
import java.util.Collection;
import ucar.nc2.Variable;
import ucar.unidata.geoloc.LatLonRect;
import ucar.nc2.Attribute;
import ucar.ma2.DataType;
import ucar.nc2.VariableSimpleIF;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import ucar.ma2.ArrayInt;
import ucar.ma2.ArrayObject;
import java.util.HashMap;
import java.util.Date;
import ucar.unidata.geoloc.Station;
import java.util.List;
import ucar.nc2.Dimension;
import java.util.Set;
import ucar.nc2.NetcdfFileWriteable;
import ucar.nc2.units.DateFormatter;

public class WriterStationObsDataset
{
    private static final String recordDimName = "record";
    private static final String stationDimName = "station";
    private static final String latName = "latitude";
    private static final String lonName = "longitude";
    private static final String altName = "altitude";
    private static final String timeName = "time";
    private static final String idName = "station_id";
    private static final String descName = "station_description";
    private static final String wmoName = "wmo_id";
    private static final String firstChildName = "firstChild";
    private static final String lastChildName = "lastChild";
    private static final String numChildName = "numChildren";
    private static final String nextChildName = "nextChild";
    private static final String prevChildName = "prevChild";
    private static final String parentName = "parent_index";
    private DateFormatter dateFormatter;
    private int name_strlen;
    private int desc_strlen;
    private int wmo_strlen;
    private NetcdfFileWriteable ncfile;
    private String title;
    private Set<Dimension> dimSet;
    private List<Dimension> recordDims;
    private List<Dimension> stationDims;
    private List<Station> stnList;
    private Date minDate;
    private Date maxDate;
    private boolean useAlt;
    private boolean useWmoId;
    private boolean debug;
    private HashMap<String, StationTracker> stationMap;
    private int recno;
    private ArrayObject.D1 timeArray;
    private ArrayInt.D1 prevArray;
    private ArrayInt.D1 parentArray;
    private int[] origin;
    private int[] originTime;
    
    public WriterStationObsDataset(final String fileOut, final String title) throws IOException {
        this.dateFormatter = new DateFormatter();
        this.dimSet = new HashSet<Dimension>();
        this.recordDims = new ArrayList<Dimension>();
        this.stationDims = new ArrayList<Dimension>();
        this.minDate = null;
        this.maxDate = null;
        this.useAlt = false;
        this.useWmoId = false;
        this.debug = false;
        this.recno = 0;
        this.timeArray = new ArrayObject.D1(String.class, 1);
        this.prevArray = new ArrayInt.D1(1);
        this.parentArray = new ArrayInt.D1(1);
        this.origin = new int[1];
        this.originTime = new int[2];
        (this.ncfile = NetcdfFileWriteable.createNew(fileOut, false)).setFill(false);
        this.title = title;
    }
    
    public void setLength(final long size) {
        this.ncfile.setLength(size);
    }
    
    public void writeHeader(final List<Station> stns, final List<VariableSimpleIF> vars) throws IOException {
        this.createGlobalAttributes();
        this.createStations(stns);
        this.ncfile.addGlobalAttribute("time_coverage_start", this.dateFormatter.toDateTimeStringISO(new Date()));
        this.ncfile.addGlobalAttribute("time_coverage_end", this.dateFormatter.toDateTimeStringISO(new Date()));
        this.createDataVariables(vars);
        this.ncfile.create();
        this.writeStationData(stns);
        if (!(boolean)this.ncfile.sendIospMessage("AddRecordStructure")) {
            throw new IllegalStateException("can't add record variable");
        }
    }
    
    private void createGlobalAttributes() {
        this.ncfile.addGlobalAttribute("Conventions", "Unidata Observation Dataset v1.0");
        this.ncfile.addGlobalAttribute("cdm_datatype", "Station");
        this.ncfile.addGlobalAttribute("title", this.title);
        this.ncfile.addGlobalAttribute("desc", "Extracted by THREDDS/Netcdf Subset Service");
    }
    
    private void createStations(final List<Station> stnList) throws IOException {
        final int nstns = stnList.size();
        for (int i = 0; i < nstns; ++i) {
            final Station stn = stnList.get(i);
            if (!Double.isNaN(stn.getAltitude())) {
                this.useAlt = true;
            }
            if (stn.getWmoId() != null && stn.getWmoId().trim().length() > 0) {
                this.useWmoId = true;
            }
        }
        for (int i = 0; i < nstns; ++i) {
            final Station station = stnList.get(i);
            this.name_strlen = Math.max(this.name_strlen, station.getName().length());
            this.desc_strlen = Math.max(this.desc_strlen, station.getDescription().length());
            if (this.useWmoId) {
                this.wmo_strlen = Math.max(this.wmo_strlen, station.getName().length());
            }
        }
        final LatLonRect llbb = this.getBoundingBox(stnList);
        this.ncfile.addGlobalAttribute("geospatial_lat_min", Double.toString(llbb.getLowerLeftPoint().getLatitude()));
        this.ncfile.addGlobalAttribute("geospatial_lat_max", Double.toString(llbb.getUpperRightPoint().getLatitude()));
        this.ncfile.addGlobalAttribute("geospatial_lon_min", Double.toString(llbb.getLowerLeftPoint().getLongitude()));
        this.ncfile.addGlobalAttribute("geospatial_lon_max", Double.toString(llbb.getUpperRightPoint().getLongitude()));
        final Dimension recordDim = this.ncfile.addUnlimitedDimension("record");
        this.recordDims.add(recordDim);
        final Dimension stationDim = this.ncfile.addDimension("station", nstns);
        this.stationDims.add(stationDim);
        Variable v = this.ncfile.addVariable("latitude", DataType.DOUBLE, "station");
        this.ncfile.addVariableAttribute(v, new Attribute("units", "degrees_north"));
        this.ncfile.addVariableAttribute(v, new Attribute("long_name", "station latitude"));
        v = this.ncfile.addVariable("longitude", DataType.DOUBLE, "station");
        this.ncfile.addVariableAttribute(v, new Attribute("units", "degrees_east"));
        this.ncfile.addVariableAttribute(v, new Attribute("long_name", "station longitude"));
        if (this.useAlt) {
            v = this.ncfile.addVariable("altitude", DataType.DOUBLE, "station");
            this.ncfile.addVariableAttribute(v, new Attribute("units", "meters"));
            this.ncfile.addVariableAttribute(v, new Attribute("long_name", "station altitude"));
        }
        v = this.ncfile.addStringVariable("station_id", this.stationDims, this.name_strlen);
        this.ncfile.addVariableAttribute(v, new Attribute("long_name", "station identifier"));
        v = this.ncfile.addStringVariable("station_description", this.stationDims, this.desc_strlen);
        this.ncfile.addVariableAttribute(v, new Attribute("long_name", "station description"));
        if (this.useWmoId) {
            v = this.ncfile.addStringVariable("wmo_id", this.stationDims, this.wmo_strlen);
            this.ncfile.addVariableAttribute(v, new Attribute("long_name", "station WMO id"));
        }
        v = this.ncfile.addVariable("numChildren", DataType.INT, "station");
        this.ncfile.addVariableAttribute(v, new Attribute("long_name", "number of children in linked list for this station"));
        v = this.ncfile.addVariable("lastChild", DataType.INT, "station");
        this.ncfile.addVariableAttribute(v, new Attribute("long_name", "record number of last child in linked list for this station"));
        v = this.ncfile.addVariable("firstChild", DataType.INT, "station");
        this.ncfile.addVariableAttribute(v, new Attribute("long_name", "record number of first child in linked list for this station"));
        final Variable timeVar = this.ncfile.addStringVariable("time", this.recordDims, 20);
        this.ncfile.addVariableAttribute(timeVar, new Attribute("long_name", "ISO-8601 Date"));
        v = this.ncfile.addVariable("prevChild", DataType.INT, "record");
        this.ncfile.addVariableAttribute(v, new Attribute("long_name", "record number of previous child in linked list"));
        v = this.ncfile.addVariable("parent_index", DataType.INT, "record");
        this.ncfile.addVariableAttribute(v, new Attribute("long_name", "index of parent station"));
        v = this.ncfile.addVariable("nextChild", DataType.INT, "record");
        this.ncfile.addVariableAttribute(v, new Attribute("long_name", "record number of next child in linked list"));
    }
    
    private void createDataVariables(final List<VariableSimpleIF> dataVars) throws IOException {
        for (final VariableSimpleIF var : dataVars) {
            final List<Dimension> dims = var.getDimensions();
            this.dimSet.addAll(dims);
        }
        for (final Dimension d : this.dimSet) {
            if (!d.isUnlimited()) {
                this.ncfile.addDimension(d.getName(), d.getLength(), d.isShared(), false, d.isVariableLength());
            }
        }
        for (final VariableSimpleIF oldVar : dataVars) {
            final List<Dimension> dims = oldVar.getDimensions();
            final StringBuffer dimNames = new StringBuffer("record");
            for (final Dimension d2 : dims) {
                if (!d2.isUnlimited()) {
                    dimNames.append(" ").append(d2.getName());
                }
            }
            final Variable newVar = this.ncfile.addVariable(oldVar.getName(), oldVar.getDataType(), dimNames.toString());
            final List<Attribute> atts = oldVar.getAttributes();
            for (final Attribute att : atts) {
                this.ncfile.addVariableAttribute(newVar, att);
            }
        }
    }
    
    private void writeStationData(final List<Station> stnList) throws IOException {
        this.stnList = stnList;
        final int nstns = stnList.size();
        this.stationMap = new HashMap<String, StationTracker>(2 * nstns);
        if (this.debug) {
            System.out.println("stationMap created");
        }
        final ArrayDouble.D1 latArray = new ArrayDouble.D1(nstns);
        final ArrayDouble.D1 lonArray = new ArrayDouble.D1(nstns);
        final ArrayDouble.D1 altArray = new ArrayDouble.D1(nstns);
        final ArrayObject.D1 idArray = new ArrayObject.D1(String.class, nstns);
        final ArrayObject.D1 descArray = new ArrayObject.D1(String.class, nstns);
        final ArrayObject.D1 wmoArray = new ArrayObject.D1(String.class, nstns);
        for (int i = 0; i < stnList.size(); ++i) {
            final Station stn = stnList.get(i);
            this.stationMap.put(stn.getName(), new StationTracker(i));
            latArray.set(i, stn.getLatitude());
            lonArray.set(i, stn.getLongitude());
            if (this.useAlt) {
                altArray.set(i, stn.getAltitude());
            }
            idArray.set(i, stn.getName());
            descArray.set(i, stn.getDescription());
            if (this.useWmoId) {
                wmoArray.set(i, stn.getWmoId());
            }
        }
        try {
            this.ncfile.write("latitude", latArray);
            this.ncfile.write("longitude", lonArray);
            if (this.useAlt) {
                this.ncfile.write("altitude", altArray);
            }
            this.ncfile.writeStringData("station_id", idArray);
            this.ncfile.writeStringData("station_description", descArray);
            if (this.useWmoId) {
                this.ncfile.writeStringData("wmo_id", wmoArray);
            }
        }
        catch (InvalidRangeException e) {
            e.printStackTrace();
            throw new IllegalStateException(e);
        }
    }
    
    private void writeDataFinish() throws IOException {
        final ArrayInt.D1 nextChildArray = new ArrayInt.D1(this.recno);
        final int nstns = this.stnList.size();
        final ArrayInt.D1 firstArray = new ArrayInt.D1(nstns);
        final ArrayInt.D1 lastArray = new ArrayInt.D1(nstns);
        final ArrayInt.D1 numArray = new ArrayInt.D1(nstns);
        for (int i = 0; i < this.stnList.size(); ++i) {
            final Station stn = this.stnList.get(i);
            final StationTracker tracker = this.stationMap.get(stn.getName());
            lastArray.set(i, tracker.lastChild);
            numArray.set(i, tracker.numChildren);
            final int first = (tracker.link.size() > 0) ? tracker.link.get(0) : -1;
            firstArray.set(i, first);
            if (tracker.link.size() > 0) {
                final List<Integer> nextList = tracker.link;
                for (int j = 0; j < nextList.size() - 1; ++j) {
                    final Integer curr = nextList.get(j);
                    final Integer next = nextList.get(j + 1);
                    nextChildArray.set(curr, next);
                }
                final Integer curr2 = nextList.get(nextList.size() - 1);
                nextChildArray.set(curr2, -1);
            }
        }
        try {
            this.ncfile.write("firstChild", firstArray);
            this.ncfile.write("lastChild", lastArray);
            this.ncfile.write("numChildren", numArray);
            this.ncfile.write("nextChild", nextChildArray);
        }
        catch (InvalidRangeException e) {
            e.printStackTrace();
            throw new IllegalStateException(e);
        }
        if (this.minDate == null) {
            this.minDate = new Date();
        }
        if (this.maxDate == null) {
            this.maxDate = new Date();
        }
        this.ncfile.updateAttribute(null, new Attribute("time_coverage_start", this.dateFormatter.toDateTimeStringISO(this.minDate)));
        this.ncfile.updateAttribute(null, new Attribute("time_coverage_end", this.dateFormatter.toDateTimeStringISO(this.maxDate)));
    }
    
    public void writeRecord(final StationObsDatatype sobs, final StructureData sdata) throws IOException {
        if (this.debug) {
            System.out.println("sobs= " + sobs + "; station = " + sobs.getStation());
        }
        this.writeRecord(sobs.getStation().getName(), sobs.getObservationTimeAsDate(), sdata);
    }
    
    public void writeRecord(final String stnName, final Date obsDate, final StructureData sdata) throws IOException {
        final StationTracker tracker = this.stationMap.get(stnName);
        final ArrayStructureW sArray = new ArrayStructureW(sdata.getStructureMembers(), new int[] { 1 });
        sArray.setStructureData(sdata, 0);
        if (this.minDate == null || this.minDate.after(obsDate)) {
            this.minDate = obsDate;
        }
        if (this.maxDate == null || this.maxDate.before(obsDate)) {
            this.maxDate = obsDate;
        }
        this.timeArray.set(0, this.dateFormatter.toDateTimeStringISO(obsDate));
        this.prevArray.set(0, tracker.lastChild);
        this.parentArray.set(0, tracker.parent_index);
        tracker.link.add(this.recno);
        tracker.lastChild = this.recno;
        final StationTracker stationTracker = tracker;
        ++stationTracker.numChildren;
        this.origin[0] = this.recno;
        this.originTime[0] = this.recno;
        try {
            this.ncfile.write("record", this.origin, sArray);
            this.ncfile.writeStringData("time", this.originTime, this.timeArray);
            this.ncfile.write("prevChild", this.originTime, this.prevArray);
            this.ncfile.write("parent_index", this.originTime, this.parentArray);
        }
        catch (InvalidRangeException e) {
            e.printStackTrace();
            throw new IllegalStateException(e);
        }
        ++this.recno;
    }
    
    public void finish() throws IOException {
        this.writeDataFinish();
        this.ncfile.close();
    }
    
    private LatLonRect getBoundingBox(final List stnList) {
        Station s = stnList.get(0);
        final LatLonPointImpl llpt = new LatLonPointImpl();
        llpt.set(s.getLatitude(), s.getLongitude());
        final LatLonRect rect = new LatLonRect(llpt, 0.001, 0.001);
        for (int i = 1; i < stnList.size(); ++i) {
            s = stnList.get(i);
            llpt.set(s.getLatitude(), s.getLongitude());
            rect.extend(llpt);
        }
        return rect;
    }
    
    private void write(final StationObsDataset sobsDataset) throws IOException {
        this.createGlobalAttributes();
        this.createStations(sobsDataset.getStations());
        this.ncfile.addGlobalAttribute("time_coverage_start", this.dateFormatter.toDateTimeStringISO(sobsDataset.getStartDate()));
        this.ncfile.addGlobalAttribute("time_coverage_end", this.dateFormatter.toDateTimeStringISO(sobsDataset.getEndDate()));
        this.createDataVariables(sobsDataset.getDataVariables());
        final List gatts = sobsDataset.getGlobalAttributes();
        for (int i = 0; i < gatts.size(); ++i) {
            final Attribute att = gatts.get(i);
            this.ncfile.addGlobalAttribute(att);
        }
        this.ncfile.create();
        this.writeStationData(sobsDataset.getStations());
        if (!(boolean)this.ncfile.sendIospMessage("AddRecordStructure")) {
            throw new IllegalStateException("can't add record variable");
        }
        final int[] origin = { 0 };
        final int[] originTime = new int[2];
        int recno = 0;
        ArrayStructureW sArray = null;
        final ArrayObject.D1 timeArray = new ArrayObject.D1(String.class, 1);
        final DataIterator diter = sobsDataset.getDataIterator(1000000);
        while (diter.hasNext()) {
            final StationObsDatatype sobs = (StationObsDatatype)diter.nextData();
            final StructureData recordData = sobs.getData();
            if (sArray == null) {
                sArray = new ArrayStructureW(recordData.getStructureMembers(), new int[] { 1 });
            }
            sArray.setStructureData(recordData, 0);
            timeArray.set(0, this.dateFormatter.toDateTimeStringISO(sobs.getObservationTimeAsDate()));
            originTime[0] = (origin[0] = recno);
            try {
                this.ncfile.write("record", origin, sArray);
                this.ncfile.writeStringData("time", originTime, timeArray);
            }
            catch (InvalidRangeException e) {
                e.printStackTrace();
                throw new IllegalStateException(e);
            }
            ++recno;
        }
        this.ncfile.close();
    }
    
    public static void main(final String[] args) throws IOException {
        final long start = System.currentTimeMillis();
        final String location = "C:/data/metars/Surface_METAR_20070513_0000.nc";
        final StringBuilder errlog = new StringBuilder();
        final StationObsDataset sobs = (StationObsDataset)TypedDatasetFactory.open(FeatureType.STATION, location, null, errlog);
        final String fileOut = "C:/temp/Surface_METAR_20070513_0000.rewrite.nc";
        final WriterStationObsDataset writer = new WriterStationObsDataset(fileOut, "test");
        final List stns = sobs.getStations();
        final List<Station> stnList = new ArrayList<Station>();
        final Station s = stns.get(0);
        stnList.add(s);
        final List<VariableSimpleIF> varList = new ArrayList<VariableSimpleIF>();
        varList.add(sobs.getDataVariable("wind_speed"));
        writer.writeHeader(stnList, varList);
        final DataIterator iter = sobs.getDataIterator(s);
        while (iter.hasNext()) {
            final StationObsDatatype sobsData = (StationObsDatatype)iter.nextData();
            final StructureData data = sobsData.getData();
            writer.writeRecord(sobsData, data);
        }
        writer.finish();
        final long took = System.currentTimeMillis() - start;
        System.out.println("That took = " + took);
    }
    
    public static void rewrite(final String fileIn, final String fileOut) throws IOException {
        final long start = System.currentTimeMillis();
        final NetcdfFile ncfile = NetcdfFile.openInMemory(fileIn);
        final NetcdfDataset ncd = new NetcdfDataset(ncfile);
        final StringBuilder errlog = new StringBuilder();
        final StationObsDataset sobs = (StationObsDataset)TypedDatasetFactory.open(FeatureType.STATION, ncd, null, errlog);
        final List<Station> stns = sobs.getStations();
        final List<VariableSimpleIF> vars = sobs.getDataVariables();
        final WriterStationObsDataset writer = new WriterStationObsDataset(fileOut, "rewrite " + fileIn);
        final File f = new File(fileIn);
        writer.setLength(f.length());
        writer.writeHeader(stns, vars);
        for (final Station s : stns) {
            final DataIterator iter = sobs.getDataIterator(s);
            while (iter.hasNext()) {
                final StationObsDatatype sobsData = (StationObsDatatype)iter.nextData();
                final StructureData data = sobsData.getData();
                writer.writeRecord(sobsData, data);
            }
        }
        writer.finish();
        final long took = System.currentTimeMillis() - start;
        System.out.println("Rewrite " + fileIn + " to " + fileOut + " took = " + took);
    }
    
    public static void main2(final String[] args) throws IOException {
        final long start = System.currentTimeMillis();
        String toLocation = "C:/temp2/";
        String fromLocation = "C:/data/metars/";
        if (args.length > 1) {
            fromLocation = args[0];
            toLocation = args[1];
        }
        System.out.println("Rewrite .nc files from " + fromLocation + " to " + toLocation);
        final File dir = new File(fromLocation);
        final File[] arr$;
        final File[] files = arr$ = dir.listFiles();
        for (final File file : arr$) {
            if (file.getName().endsWith(".nc")) {
                rewrite(file.getAbsolutePath(), toLocation + file.getName());
            }
        }
        final long took = System.currentTimeMillis() - start;
        System.out.println("That took = " + took);
    }
    
    private class StationTracker
    {
        int numChildren;
        int lastChild;
        int parent_index;
        List<Integer> link;
        
        StationTracker(final int parent_index) {
            this.numChildren = 0;
            this.lastChild = -1;
            this.link = new ArrayList<Integer>();
            this.parent_index = parent_index;
        }
    }
}
