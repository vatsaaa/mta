// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.dt.point.decode;

import java.io.IOException;
import java.util.Iterator;
import java.io.InputStream;
import java.io.Reader;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.FileInputStream;
import java.util.regex.Matcher;
import java.util.HashMap;
import java.util.LinkedHashMap;

public class MetarParseReport
{
    private LinkedHashMap field;
    private HashMap unit;
    
    public MetarParseReport() {
        this.field = new LinkedHashMap();
        this.unit = new HashMap();
    }
    
    public boolean parseReport(final String input) {
        this.field.clear();
        this.unit.clear();
        String report = null;
        String remark = null;
        if (MP.B_CR.matcher(input).find()) {
            return false;
        }
        final String[] split = MP.REMARKS.split(input);
        report = split[0] + " ";
        if (split.length == 2) {
            remark = " " + split[1] + " ";
        }
        Matcher m = MP.B_metar.matcher(report);
        if (m.find()) {
            this.field.put("Report_Type", m.group(1));
            report = m.replaceFirst("");
        }
        else {
            this.field.put("Report_Type", "METAR");
        }
        this.unit.put("Report_Type", "");
        m = MP.station.matcher(report);
        if (!m.find()) {
            return false;
        }
        this.field.put("Station", m.group(1));
        this.unit.put("Station", "");
        report = m.replaceFirst(" ");
        m = MP.ddhhmmZ.matcher(report);
        if (!m.find()) {
            return false;
        }
        this.field.put("Day", m.group(1));
        this.unit.put("Day", "");
        this.field.put("Hour", m.group(2));
        this.unit.put("Hour", "");
        this.field.put("Minute", m.group(3));
        this.unit.put("Minute", "");
        report = m.replaceFirst(" ");
        if (MP.NIL.matcher(report).find()) {
            return false;
        }
        m = MP.COR.matcher(report);
        if (m.find()) {
            report = m.replaceFirst(" ");
        }
        m = MP.AUTOS.matcher(report);
        if (m.find()) {
            this.field.put("AUTOS", "yes");
            this.unit.put("AUTOS", "");
            report = m.replaceFirst(" ");
        }
        m = MP.wind_direction_speed.matcher(report);
        if (m.find()) {
            if (m.group(2).equals("VRB")) {
                if (m.group(1) == null) {
                    this.field.put("Variable_Wind_direction", "");
                }
                else {
                    this.field.put("Variable_Wind_direction", m.group(1));
                }
                this.unit.put("Variable_Wind_direction", "");
            }
            else {
                this.field.put("Wind_Direction", m.group(2));
                this.unit.put("Wind_Direction", "degrees");
            }
            this.field.put("Wind_Speed", m.group(3));
            if (m.group(4) != null && m.group(4).equals("G")) {
                this.field.put("Wind_Gust", m.group(5));
            }
            this.unit.put("Wind_Speed", m.group(6));
            this.unit.put("Wind_Gust", m.group(6));
            report = m.replaceFirst(" ");
        }
        m = MP.min_max_wind_dir.matcher(report);
        if (m.find()) {
            this.field.put("Wind_Direction_Min", m.group(1));
            this.unit.put("Wind_Direction_Min", "degrees");
            this.field.put("Wind_Direction_Max", m.group(2));
            this.unit.put("Wind_Direction_Max", "degrees");
            report = m.replaceFirst(" ");
        }
        report = MP.N9999.matcher(report).replaceFirst(" ");
        boolean done = false;
        m = MP.visibilitySM.matcher(report);
        if (m.find()) {
            this.field.put("Visibility", "0.0");
            this.unit.put("Visibility", "miles");
            report = m.replaceFirst(" ");
            done = true;
        }
        if (!done) {
            m = MP.visibilityKM.matcher(report);
            if (m.find()) {
                this.field.put("Visibility", "0.0");
                this.unit.put("Visibility", "kilometer");
                report = m.replaceFirst(" ");
                done = true;
            }
        }
        if (!done) {
            m = MP.visibility1.matcher(report);
            if (m.find()) {
                float var1 = Float.parseFloat(m.group(1));
                final float var2 = Float.parseFloat(m.group(2));
                final float var3 = Float.parseFloat(m.group(3));
                var1 += var2 / var3;
                if (m.group(4).equals("SM")) {
                    this.field.put("Visibility", Float.toString(var1));
                    this.unit.put("Visibility", "miles");
                }
                else {
                    this.field.put("Visibility", Float.toString(var1));
                    this.unit.put("Visibility", "kilometer");
                }
                report = m.replaceFirst(" ");
                done = true;
            }
        }
        if (!done) {
            m = MP.visibility2.matcher(report);
            if (m.find()) {
                float var1 = Float.parseFloat(m.group(1));
                final float var2 = Float.parseFloat(m.group(2));
                var1 /= var2;
                if (m.group(3).equals("SM")) {
                    this.field.put("Visibility", Float.toString(var1));
                    this.unit.put("Visibility", "miles");
                }
                else {
                    this.field.put("Visibility", Float.toString(var1));
                    this.unit.put("Visibility", "kilometer");
                }
                report = m.replaceFirst(" ");
                done = true;
            }
        }
        if (!done) {
            m = MP.visibility3.matcher(report);
            if (m.find()) {
                if (m.group(2).equals("SM")) {
                    this.field.put("Visibility", m.group(1));
                    this.unit.put("Visibility", "miles");
                }
                else {
                    this.field.put("Visibility", m.group(1));
                    this.unit.put("Visibility", "kilometer");
                }
                report = m.replaceFirst(" ");
                done = true;
            }
        }
        if (!done) {
            m = MP.visibility_direction.matcher(report);
            if (m.find()) {
                this.field.put("Visibility", m.group(1));
                this.unit.put("Visibility", "meters");
                this.field.put("Visibility_Direction", m.group(2));
                this.unit.put("Visibility_Direction", "");
                report = m.replaceFirst(" ");
                done = true;
            }
        }
        m = MP.CAVOKS.matcher(report);
        if (m.find()) {
            this.field.put("Clear_Air", "yes");
            this.unit.put("Clear_Air", "");
            report = m.replaceFirst(" ");
        }
        m = MP.RVRNO.matcher(report);
        if (m.find()) {
            this.field.put("RunwayReports", "No");
            this.unit.put("RunwayReports", "");
            report = m.replaceFirst(" ");
        }
        for (int i = 0; i < 4; ++i) {
            m = MP.runway.matcher(report);
            if (!m.find()) {
                break;
            }
            final String RV = "RV" + Integer.toString(i + 1);
            this.field.put(RV, m.group(1));
            this.unit.put(RV, "");
            report = m.replaceFirst(" ");
        }
        done = true;
        final StringBuilder WX = new StringBuilder();
        for (int j = 0; j < 4; ++j) {
            m = MP.WeatherPrecip.matcher(report);
            if (m.find()) {
                done = false;
                WX.append(m.group(1));
                if (m.group(2) != null) {
                    WX.append(m.group(2));
                }
                WX.append(m.group(3));
                report = m.replaceFirst(" ");
            }
            m = MP.WeatherObs.matcher(report);
            if (m.find()) {
                done = false;
                WX.append(m.group(1));
                if (m.group(2) != null) {
                    WX.append(m.group(2));
                }
                WX.append(m.group(3));
                report = m.replaceFirst(" ");
            }
            m = MP.WeatherOther.matcher(report);
            if (m.find()) {
                done = false;
                WX.append(m.group(1));
                if (m.group(2) != null) {
                    WX.append(m.group(2));
                }
                WX.append(m.group(3));
                report = m.replaceFirst(" ");
            }
            if (done) {
                break;
            }
            done = true;
            WX.append(" ");
        }
        if (WX.length() > 0) {
            WX.setLength(WX.length() - 1);
            this.field.put("Weather", WX.toString());
            this.unit.put("Weather", "");
        }
        m = MP.CLR_or_SKC.matcher(report);
        if (m.find()) {
            this.field.put("Cloud_Type", m.group(1));
            this.unit.put("Cloud_Type", "");
            report = m.replaceFirst(" ");
        }
        m = MP.vertical_VIS.matcher(report);
        if (m.find()) {
            this.field.put("Vertical_Visibility", this.cloud_hgt2_meters(m.group(1)));
            this.unit.put("Vertical_Visibility", "meters");
            report = m.replaceFirst(" ");
        }
        for (int j = 0; j < 6; ++j) {
            m = MP.cloud_cover.matcher(report);
            if (!m.find()) {
                break;
            }
            final String cloud = "Cloud_Layer_" + Integer.toString(j + 1);
            if (m.group(1) == null) {
                this.field.put(cloud + "_Type", m.group(2));
            }
            else {
                this.field.put(cloud + "_Type", m.group(1) + m.group(2));
            }
            this.unit.put(cloud + "_Type", "");
            this.field.put(cloud + "_Height", this.cloud_hgt2_meters(m.group(3)));
            this.unit.put(cloud + "_Height", "meters");
            if (m.group(4) != null) {
                this.field.put(cloud + "_Phenom", m.group(4));
                this.unit.put(cloud + "_Phenom", "");
            }
            report = m.replaceFirst(" ");
        }
        float air_temperature = -999.0f;
        float dew_point_temperature = -999.0f;
        if (remark != null) {
            m = MP.Temperature_tenths.matcher(remark);
        }
        if (remark != null && m.find()) {
            air_temperature = (float)(Float.parseFloat(m.group(2)) * 0.1);
            if (m.group(1).equals("1")) {
                air_temperature *= -1.0f;
            }
            this.field.put("Temperature", Float.toString(air_temperature));
            this.unit.put("Temperature", "Celsius");
            if (m.group(3) != null) {
                dew_point_temperature = (float)(Float.parseFloat(m.group(4)) * 0.1);
                if (m.group(3).equals("1")) {
                    dew_point_temperature *= -1.0f;
                }
                this.field.put("DewPoint", Float.toString(dew_point_temperature));
                this.unit.put("DewPoint", "Celsius");
            }
            remark = m.replaceFirst(" ");
        }
        else {
            m = MP.Temperature.matcher(report);
            if (m.find()) {
                air_temperature = Float.parseFloat(m.group(2));
                if (m.group(1) != null) {
                    air_temperature *= -1.0f;
                }
                this.field.put("Temperature", Float.toString(air_temperature));
                this.unit.put("Temperature", "Celsius");
                if (m.group(4) != null) {
                    dew_point_temperature = Float.parseFloat(m.group(4));
                    if (m.group(3) != null) {
                        dew_point_temperature *= -1.0f;
                    }
                    this.field.put("DewPoint", Float.toString(dew_point_temperature));
                    this.unit.put("DewPoint", "Celsius");
                }
                report = m.replaceFirst(" ");
            }
        }
        m = MP.altimeter.matcher(report);
        if (m.find()) {
            if (m.group(1).equals("A")) {
                this.field.put("Altimeter", Double.toString(Float.parseFloat(m.group(2)) * 0.01));
                this.unit.put("Altimeter", "inches");
            }
            else {
                this.field.put("Altimeter", m.group(2));
                this.unit.put("Altimeter", "hectopascal");
            }
            report = m.replaceFirst(" ");
        }
        m = MP.NOSIG.matcher(report);
        if (m.find()) {
            this.field.put("Weather", "No");
            this.unit.put("Weather", "");
            report = m.replaceFirst(" ");
        }
        if (remark == null) {
            return true;
        }
        m = MP.automatic_report.matcher(remark);
        if (m.find()) {
            this.field.put("Automatic_Report", m.group(1));
            this.unit.put("Automatic_Report", "");
            remark = m.replaceFirst(" ");
        }
        if (MP.spaces.matcher(remark).matches()) {
            return true;
        }
        m = MP.SLPNO.matcher(remark);
        if (m.find()) {
            this.field.put("Sea_Level_Pressure_available", "No");
            this.unit.put("Sea_Level_Pressure_available", "");
            remark = m.replaceFirst(" ");
        }
        m = MP.SLP.matcher(remark);
        if (m.find()) {
            if (Integer.parseInt(m.group(1)) >= 550) {
                this.field.put("Sea_Level_Pressure", Double.toString(Float.parseFloat(m.group(1)) * 0.1 + 900.0));
            }
            else {
                this.field.put("Sea_Level_Pressure", Double.toString(Float.parseFloat(m.group(1)) * 0.1 + 1000.0));
            }
            this.unit.put("Sea_Level_Pressure", "hectopascal");
            remark = m.replaceFirst(" ");
        }
        if (MP.spaces.matcher(remark).matches()) {
            return true;
        }
        m = MP.hourly_precip.matcher(remark);
        if (m.find()) {
            this.field.put("Hourly_Precipitation", Double.toString(Float.parseFloat(m.group(1)) * 0.01));
            this.unit.put("Hourly_Precipitation", "inches");
            remark = m.replaceFirst(" ");
        }
        if (MP.spaces.matcher(remark).matches()) {
            return true;
        }
        m = MP.PWINO.matcher(remark);
        if (m.find()) {
            this.field.put("PRECIP_sensor_working", "No");
            this.unit.put("PRECIP_sensor_working", "");
            remark = m.replaceFirst(" ");
        }
        if (MP.spaces.matcher(remark).matches()) {
            return true;
        }
        m = MP.TSNO.matcher(remark);
        if (m.find()) {
            this.field.put("Lightning_sensor_working", "No");
            this.unit.put("Lightning_sensor_working", "");
            remark = m.replaceFirst(" ");
        }
        if (MP.spaces.matcher(remark).matches()) {
            return true;
        }
        m = MP.tornado.matcher(remark);
        if (m.find()) {
            this.field.put("TornadicType", m.group(1));
            this.unit.put("TornadicType", "");
            remark = m.replaceFirst(" ");
            m = MP.tornadoTime.matcher(remark);
            if (m.find()) {
                String time;
                String units;
                if (m.group(2) == null) {
                    time = m.group(3);
                    units = "mm";
                }
                else {
                    time = m.group(2) + m.group(3);
                    units = "hhmm";
                }
                if (m.group(1).equals("B")) {
                    this.field.put("Begin_Tornado", time);
                    this.unit.put("Begin_Tornado", units);
                }
                else {
                    this.field.put("End_Tornado", time);
                    this.unit.put("End_Tornado", units);
                }
                remark = m.replaceFirst(" ");
            }
            m = MP.tornadoLocation.matcher(remark);
            if (m.find()) {
                this.field.put("Tornado_Location", m.group(1));
                this.unit.put("Tornado_Location", "");
                remark = m.replaceFirst(" ");
            }
            m = MP.tornadoDirection.matcher(remark);
            if (m.find()) {
                this.field.put("Tornado_Direction", m.group(1));
                this.unit.put("Tornado_Direction", "");
                remark = m.replaceFirst(" ");
            }
        }
        m = MP.peakWind.matcher(remark);
        if (m.find()) {
            this.field.put("Peak_Wind_Direction", m.group(1));
            this.unit.put("Peak_Wind_Direction", "degrees");
            this.field.put("Peak_Wind_Speed", m.group(2));
            this.unit.put("Peak_Wind_Speed", "knots");
            String time;
            String units;
            if (m.group(3) == null) {
                time = m.group(4);
                units = "mm";
            }
            else {
                time = m.group(3) + m.group(4);
                units = "hhmm";
            }
            this.field.put("Peak_Wind_Time", time);
            this.unit.put("Peak_Wind_Time", units);
            remark = m.replaceFirst(" ");
        }
        m = MP.windShift.matcher(remark);
        if (m.find()) {
            String time;
            String units;
            if (m.group(1) == null) {
                time = m.group(2);
                units = "mm";
            }
            else {
                time = m.group(1) + m.group(2);
                units = "hhmm";
            }
            this.field.put("Wind_Shift", time);
            this.unit.put("Wind_Shift", units);
            remark = m.replaceFirst(" ");
        }
        m = MP.FROPA.matcher(remark);
        if (m.find()) {
            this.field.put("Wind_Shift_Frontal_Passage", "Yes");
            this.unit.put("Wind_Shift_Frontal_Passage", "");
            remark = m.replaceFirst(" ");
        }
        m = MP.towerVisibility1.matcher(remark);
        if (m.find()) {
            float var1 = Float.parseFloat(m.group(2));
            final float var2 = Float.parseFloat(m.group(3));
            final float var3 = Float.parseFloat(m.group(4));
            var1 += var2 / var3;
            this.field.put("Tower_Visibility", Float.toString(var1));
            this.unit.put("Tower_Visibility", "miles");
            remark = m.replaceFirst(" ");
        }
        else {
            m = MP.towerVisibility2.matcher(remark);
            if (m.find()) {
                float var1 = Float.parseFloat(m.group(2));
                final float var2 = Float.parseFloat(m.group(3));
                var1 /= var2;
                this.field.put("Tower_Visibility", Float.toString(var1));
                this.unit.put("Tower_Visibility", "miles");
                remark = m.replaceFirst(" ");
            }
            else {
                m = MP.towerVisibility3.matcher(remark);
                if (m.find()) {
                    this.field.put("Tower_Visibility", m.group(2));
                    this.unit.put("Tower_Visibility", "miles");
                    remark = m.replaceFirst(" ");
                }
            }
        }
        m = MP.surfaceVisibility1.matcher(remark);
        if (m.find()) {
            float var1 = Float.parseFloat(m.group(2));
            final float var2 = Float.parseFloat(m.group(3));
            final float var3 = Float.parseFloat(m.group(4));
            var1 += var2 / var3;
            this.field.put("Surface_Visibility", Float.toString(var1));
            this.unit.put("Surface_Visibility", "miles");
            remark = m.replaceFirst(" ");
        }
        else {
            m = MP.surfaceVisibility2.matcher(remark);
            if (m.find()) {
                float var1 = Float.parseFloat(m.group(2));
                final float var2 = Float.parseFloat(m.group(3));
                var1 /= var2;
                this.field.put("Surface_Visibility", Float.toString(var1));
                this.unit.put("Surface_Visibility", "miles");
                remark = m.replaceFirst(" ");
            }
            else {
                m = MP.surfaceVisibility3.matcher(remark);
                if (m.find()) {
                    this.field.put("Surface_Visibility", m.group(2));
                    this.unit.put("Surface_Visibility", "miles");
                    remark = m.replaceFirst(" ");
                }
            }
        }
        m = MP.variableVisibility1.matcher(remark);
        if (m.find()) {
            float var1 = Float.parseFloat(m.group(2));
            float var2 = Float.parseFloat(m.group(3));
            float var3 = Float.parseFloat(m.group(4));
            var1 += var2 / var3;
            this.field.put("Variable_Visibility_Min", Float.toString(var1));
            this.unit.put("Variable_Visibility_Min", "miles");
            var1 = Float.parseFloat(m.group(5));
            var2 = Float.parseFloat(m.group(6));
            var3 = Float.parseFloat(m.group(7));
            var1 += var2 / var3;
            this.field.put("Variable_Visibility_Max", Float.toString(var1));
            this.unit.put("Variable_Visibility_Max", "miles");
            remark = m.replaceFirst(" ");
        }
        else {
            m = MP.variableVisibility2.matcher(remark);
            if (m.find()) {
                this.field.put("Variable_Visibility_Min", m.group(2));
                this.unit.put("Variable_Visibility_Min", "miles");
                float var1 = Float.parseFloat(m.group(3));
                final float var2 = Float.parseFloat(m.group(4));
                final float var3 = Float.parseFloat(m.group(5));
                var1 += var2 / var3;
                this.field.put("Variable_Visibility_Max", Float.toString(var1));
                this.unit.put("Variable_Visibility_Max", "miles");
                remark = m.replaceFirst(" ");
            }
            else {
                m = MP.variableVisibility3.matcher(remark);
                if (m.find()) {
                    float var1 = Float.parseFloat(m.group(2));
                    float var2 = Float.parseFloat(m.group(3));
                    var1 /= var2;
                    this.field.put("Variable_Visibility_Min", Float.toString(var1));
                    this.unit.put("Variable_Visibility_Min", "miles");
                    var1 = Float.parseFloat(m.group(4));
                    var2 = Float.parseFloat(m.group(5));
                    final float var3 = Float.parseFloat(m.group(6));
                    var1 += var2 / var3;
                    this.field.put("Variable_Visibility_Max", Float.toString(var1));
                    this.unit.put("Variable_Visibility_Max", "miles");
                    remark = m.replaceFirst(" ");
                }
                else {
                    m = MP.variableVisibility4.matcher(remark);
                    if (m.find()) {
                        float var1 = Float.parseFloat(m.group(2));
                        final float var2 = Float.parseFloat(m.group(3));
                        final float var3 = Float.parseFloat(m.group(4));
                        var1 += var2 / var3;
                        this.field.put("Variable_Visibility_Min", Float.toString(var1));
                        this.unit.put("Variable_Visibility_Min", "miles");
                        this.field.put("Variable_Visibility_Max", m.group(5));
                        this.unit.put("Variable_Visibility_Max", "miles");
                        remark = m.replaceFirst(" ");
                    }
                    else {
                        m = MP.variableVisibility5.matcher(remark);
                        if (m.find()) {
                            this.field.put("Variable_Visibility_Min", m.group(2));
                            this.unit.put("Variable_Visibility_Min", "miles");
                            this.field.put("Variable_Visibility_Max", m.group(3));
                            this.unit.put("Variable_Visibility_Max", "miles");
                            remark = m.replaceFirst(" ");
                        }
                        else {
                            m = MP.variableVisibility6.matcher(remark);
                            if (m.find()) {
                                float var1 = Float.parseFloat(m.group(2));
                                final float var2 = Float.parseFloat(m.group(3));
                                var1 /= var2;
                                this.field.put("Variable_Visibility_Min", Float.toString(var1));
                                this.unit.put("Variable_Visibility_Min", "miles");
                                this.field.put("Variable_Visibility_Max", m.group(4));
                                this.unit.put("Variable_Visibility_Max", "miles");
                                remark = m.replaceFirst(" ");
                            }
                        }
                    }
                }
            }
        }
        m = MP.Visibility2ndSite1.matcher(remark);
        if (m.find()) {
            float var1 = Float.parseFloat(m.group(2));
            final float var2 = Float.parseFloat(m.group(3));
            final float var3 = Float.parseFloat(m.group(4));
            var1 += var2 / var3;
            this.field.put("Second_Site_Visibility", Float.toString(var1));
            this.unit.put("Second_Site_Visibility", "miles");
            this.field.put("Second_Site_Location", m.group(5));
            this.unit.put("Second_Site_Location", "");
            remark = m.replaceFirst(" ");
        }
        else {
            m = MP.Visibility2ndSite2.matcher(remark);
            if (m.find()) {
                this.field.put("Second_Site_Visibility", m.group(2));
                this.unit.put("Second_Site_Visibility", "miles");
                this.field.put("Second_Site_Location", m.group(3));
                this.unit.put("Second_Site_Location", "");
                remark = m.replaceFirst(" ");
            }
            else {
                m = MP.Visibility2ndSite3.matcher(remark);
                if (m.find()) {
                    float var1 = Float.parseFloat(m.group(2));
                    final float var2 = Float.parseFloat(m.group(3));
                    var1 /= var2;
                    this.field.put("Second_Site_Visibility", Float.toString(var1));
                    this.unit.put("Second_Site_Visibility", "miles");
                    this.field.put("Second_Site_Location", m.group(4));
                    this.unit.put("Second_Site_Location", "");
                    remark = m.replaceFirst(" ");
                }
            }
        }
        m = MP.CIG.matcher(remark);
        if (m.find()) {
            this.field.put("Ceiling_Min", Integer.toString(Integer.parseInt(m.group(1)) * 100));
            this.unit.put("Ceiling_Min", "feet");
            this.field.put("Ceiling_Max", Integer.toString(Integer.parseInt(m.group(2)) * 100));
            this.unit.put("Ceiling_Max", "feet");
            remark = m.replaceFirst(" ");
        }
        m = MP.CIG_RY.matcher(remark);
        if (m.find()) {
            final float var1 = Float.parseFloat(m.group(1)) * 10.0f;
            this.field.put("Second_Site_Sky", Float.toString(var1));
            this.unit.put("Second_Site_Sky", "feet");
            this.field.put("Second_Site_Sky_Location", m.group(2));
            this.unit.put("Second_Site_Sky_Location", "");
            remark = m.replaceFirst(" ");
        }
        m = MP.PRESFR.matcher(remark);
        if (m.find()) {
            this.field.put("Pressure_Falling_Rapidly", "Yes");
            this.unit.put("Pressure_Falling_Rapidly", "");
            remark = m.replaceFirst(" ");
        }
        m = MP.PRESRR.matcher(remark);
        if (m.find()) {
            this.field.put("Pressure_Rising_Rapidly", "Yes");
            this.unit.put("Pressure_Rising_Rapidly", "");
            remark = m.replaceFirst(" ");
        }
        m = MP.sectorVisibility1.matcher(remark);
        if (m.find()) {
            this.field.put("Sector_Visibility_Direction", m.group(2));
            this.unit.put("Sector_Visibility_Direction", "");
            float var1 = Float.parseFloat(m.group(3));
            final float var2 = Float.parseFloat(m.group(4));
            final float var3 = Float.parseFloat(m.group(5));
            var1 += var2 / var3;
            this.field.put("Sector_Visibility", Float.toString(var1));
            this.unit.put("Sector_Visibility", "miles");
            remark = m.replaceFirst(" ");
        }
        else {
            m = MP.sectorVisibility2.matcher(remark);
            if (m.find()) {
                this.field.put("Sector_Visibility_Direction", m.group(2));
                this.unit.put("Sector_Visibility_Direction", "");
                float var1 = Float.parseFloat(m.group(3));
                final float var2 = Float.parseFloat(m.group(4));
                var1 /= var2;
                this.field.put("Sector_Visibility", Float.toString(var1));
                this.unit.put("Sector_Visibility", "miles");
                remark = m.replaceFirst(" ");
            }
            else {
                m = MP.sectorVisibility3.matcher(remark);
                if (m.find()) {
                    this.field.put("Sector_Visibility_Direction", m.group(2));
                    this.unit.put("Sector_Visibility_Direction", "");
                    this.field.put("Sector_Visibility", m.group(3));
                    this.unit.put("Sector_Visibility", "miles");
                    remark = m.replaceFirst(" ");
                }
            }
        }
        m = MP.GR1.matcher(remark);
        if (m.find()) {
            this.field.put("Hailstone_Activity", "yes");
            this.unit.put("Hailstone_Activity", "");
            this.field.put("Hailstone_Size", "0.25");
            this.unit.put("Hailstone_Size", "");
            remark = m.replaceFirst(" ");
        }
        else {
            m = MP.GR2.matcher(remark);
            if (m.find()) {
                this.field.put("Hailstone_Activity", "yes");
                this.unit.put("Hailstone_Activity", "");
                float var1 = Float.parseFloat(m.group(1));
                final float var2 = Float.parseFloat(m.group(2));
                final float var3 = Float.parseFloat(m.group(3));
                var1 += var2 / var3;
                this.field.put("Hailstone_Size", Float.toString(var1));
                this.unit.put("Hailstone_Size", "");
                remark = m.replaceFirst(" ");
            }
            else {
                m = MP.GR3.matcher(remark);
                if (m.find()) {
                    this.field.put("Hailstone_Activity", "yes");
                    this.unit.put("Hailstone_Activity", "");
                    float var1 = Float.parseFloat(m.group(1));
                    final float var2 = Float.parseFloat(m.group(2));
                    var1 /= var2;
                    this.field.put("Hailstone_Size", Float.toString(var1));
                    this.unit.put("Hailstone_Size", "");
                    remark = m.replaceFirst(" ");
                }
                else {
                    m = MP.GR4.matcher(remark);
                    if (m.find()) {
                        this.field.put("Hailstone_Activity", "yes");
                        this.unit.put("Hailstone_Activity", "");
                        this.field.put("Hailstone_Size", m.group(1));
                        this.unit.put("Hailstone_Size", "");
                        remark = m.replaceFirst(" ");
                    }
                }
            }
        }
        m = MP.GR.matcher(remark);
        if (m.find()) {
            this.field.put("Hailstone_Activity", "yes");
            this.unit.put("Hailstone_Activity", "");
            remark = m.replaceFirst(" ");
        }
        m = MP.VIRGA.matcher(remark);
        if (m.find()) {
            this.field.put("Virga_Activity", "yes");
            this.unit.put("Virga_Activity", "");
            if (m.group(1) != null) {
                this.field.put("Virga_Direction", m.group(1));
                this.unit.put("Virga_Direction", "");
            }
            if (m.group(2) != null) {
                this.field.put("Virga_Direction", m.group(2));
                this.unit.put("Virga_Direction", "");
            }
            remark = m.replaceFirst(" ");
        }
        m = MP.CIGNO.matcher(remark);
        if (m.find()) {
            this.field.put("Ceiling_height_available", "No");
            this.unit.put("Ceiling_height_available", "");
            remark = m.replaceFirst(" ");
        }
        m = MP.CIG_EST.matcher(remark);
        if (m.find()) {
            final String est = Integer.toString(Integer.parseInt(m.group(2)) * 100);
            this.field.put("Ceiling", est);
            this.unit.put("Ceiling", "feet");
            remark = m.replaceFirst(" ");
        }
        m = MP.variableSky.matcher(remark);
        if (m.find()) {
            this.field.put("Variable_Sky_Below", m.group(1));
            this.unit.put("Variable_Sky_Below", "feet");
            if (m.group(2) != null) {
                this.field.put("Variable_Sky_Height", Integer.toString(Integer.parseInt(m.group(2)) * 100));
                this.unit.put("Variable_Sky_Height", "feet");
            }
            this.field.put("Variable_Sky_Above", m.group(3));
            this.unit.put("Variable_Sky_Above", "feet");
            remark = m.replaceFirst(" ");
        }
        m = MP.ACFT.matcher(remark);
        if (m.find()) {
            this.field.put("Air_craft_mishap", "yes");
            this.unit.put("Air_craft_mishap", "");
            remark = m.replaceFirst(" ");
        }
        m = MP.NOSPECI.matcher(remark);
        if (m.find()) {
            this.field.put("Changes_in_weather", "No");
            this.unit.put("Changes_in_weather", "");
            remark = m.replaceFirst(" ");
        }
        m = MP.FIRST.matcher(remark);
        if (m.find()) {
            this.field.put("First_Report_Today", "yes");
            this.unit.put("First_Report_Today", "");
            remark = m.replaceFirst(" ");
        }
        m = MP.LAST.matcher(remark);
        if (m.find()) {
            this.field.put("Last_Report_Today", "yes");
            this.unit.put("Last_Report_Today", "");
            remark = m.replaceFirst(" ");
        }
        m = MP.cloud_height.matcher(remark);
        if (m.find()) {
            if (!m.group(1).equals("/")) {
                this.field.put("Cloud_Low", m.group(1));
                this.unit.put("Cloud_Low", "");
            }
            if (!m.group(2).equals("/")) {
                this.field.put("Cloud_Medium", m.group(2));
                this.unit.put("Cloud_Medium", "");
            }
            if (!m.group(3).equals("/")) {
                this.field.put("Cloud_High", m.group(3));
                this.unit.put("Cloud_High", "");
            }
            remark = m.replaceFirst(" ");
        }
        m = MP.SNINCR.matcher(remark);
        if (m.find()) {
            this.field.put("Snow_Increasing_Rapidly", m.group(1));
            this.unit.put("Snow_Increasing_Rapidly", "inches");
            this.field.put("Snow_Increasing_Depth", m.group(2));
            this.unit.put("Snow_Increasing_Depth", "inches");
            remark = m.replaceFirst(" ");
        }
        m = MP.snowDepth.matcher(remark);
        if (m.find()) {
            this.field.put("Snow_Depth", m.group(1));
            this.unit.put("Snow_Depth", "inches");
            remark = m.replaceFirst(" ");
        }
        m = MP.waterEquiv.matcher(remark);
        if (m.find()) {
            this.field.put("Water_Equivalent_of_Snow", Double.toString(Float.parseFloat(m.group(1)) * 0.1));
            this.unit.put("Water_Equivalent_of_Snow", "");
            remark = m.replaceFirst(" ");
        }
        m = MP.sunShine.matcher(remark);
        if (m.find()) {
            if (m.group(1).equals("///")) {
                this.field.put("Sun_Sensor_working", "No");
                this.unit.put("Sun_Sensor_working", "");
            }
            else {
                this.field.put("Sun_Sensor_Duration", m.group(1));
                this.unit.put("Sun_Sensor_Duration", "");
            }
            remark = m.replaceFirst(" ");
        }
        m = MP.precipitation.matcher(remark);
        if (m.find()) {
            if (!m.group(1).equals("////")) {
                this.field.put("Precipitation_amount", Double.toString(Float.parseFloat(m.group(1)) * 0.01));
                this.unit.put("Precipitation_amount", "inches");
            }
            remark = m.replaceFirst(" ");
        }
        m = MP.precipitation24.matcher(remark);
        if (m.find()) {
            if (!m.group(1).equals("////")) {
                this.field.put("Precipitation_amount_24Hours", Double.toString(Float.parseFloat(m.group(1)) * 0.01));
                this.unit.put("Precipitation_amount_24Hours", "inches");
            }
            remark = m.replaceFirst(" ");
        }
        m = MP.maxTemperature.matcher(remark);
        if (m.find()) {
            if (!m.group(2).equals("///")) {
                double maxtemp = Float.parseFloat(m.group(2));
                if (m.group(1).equals("1")) {
                    maxtemp *= -0.1;
                }
                else if (m.group(1).equals("0")) {
                    maxtemp *= 0.1;
                }
                this.field.put("Max_Temperature", Double.toString(maxtemp));
                this.unit.put("Max_Temperature", "Celsius");
            }
            remark = m.replaceFirst(" ");
        }
        m = MP.minTemperature.matcher(remark);
        if (m.find()) {
            if (!m.group(2).equals("///")) {
                double mintemp = Float.parseFloat(m.group(2));
                if (m.group(1).equals("1")) {
                    mintemp *= -0.1;
                }
                else if (m.group(1).equals("0")) {
                    mintemp *= 0.1;
                }
                this.field.put("Min_Temperature", Double.toString(mintemp));
                this.unit.put("Min_Temperature", "Celsius");
            }
            remark = m.replaceFirst(" ");
        }
        m = MP.maxMinTemp24.matcher(remark);
        if (m.find()) {
            if (!m.group(2).equals("///")) {
                double maxtemp = Float.parseFloat(m.group(2));
                if (m.group(1).equals("1")) {
                    maxtemp *= -0.1;
                }
                else if (m.group(1).equals("0")) {
                    maxtemp *= 0.1;
                }
                this.field.put("Max_Temperature_24Hour", Double.toString(maxtemp));
                this.unit.put("Max_Temperature_24Hour", "Celsius");
            }
            if (!m.group(4).equals("///")) {
                double mintemp = Float.parseFloat(m.group(4));
                if (m.group(3).equals("1")) {
                    mintemp *= -0.1;
                }
                else if (m.group(3).equals("0")) {
                    mintemp *= 0.1;
                }
                this.field.put("Min_Temperature_24Hour", Double.toString(mintemp));
                this.unit.put("Min_Temperature_24Hour", "Celsius");
            }
            remark = m.replaceFirst(" ");
        }
        m = MP.pressureTendency.matcher(remark);
        if (m.find()) {
            this.field.put("Presure_Tendency_char", m.group(1));
            this.unit.put("Presure_Tendency_char", "");
            if (!m.group(2).equals("///")) {
                this.field.put("Presure_Tendency", Double.toString(Float.parseFloat(m.group(2)) * 0.1));
                this.unit.put("Presure_Tendency", "hectopascals");
            }
            remark = m.replaceFirst(" ");
        }
        m = MP.FZRANO.matcher(remark);
        if (m.find()) {
            this.field.put("Freezing_Rain_sensor_working", "No");
            this.unit.put("Freezing_Rain_sensor_working", "");
            remark = m.replaceFirst(" ");
        }
        m = MP.PNO.matcher(remark);
        if (m.find()) {
            this.field.put("Tipping_bucket_rain_gauge_working", "No");
            this.unit.put("Tipping_bucket_rain_gauge_working", "");
            remark = m.replaceFirst(" ");
        }
        m = MP.maintenace.matcher(remark);
        if (m.find()) {
            this.field.put("Maintenance_needed", "yes");
            this.unit.put("Maintenance_needed", "");
            remark = m.replaceFirst(" ");
        }
        m = MP.spaces.matcher(remark);
        if (m.find()) {
            remark = m.replaceFirst("");
        }
        if (!remark.equals("")) {
            this.field.put("Extra_fields", remark);
            this.unit.put("Extra_fields", "");
        }
        return true;
    }
    
    private String cloud_hgt2_meters(final String height) {
        if (height.equals("999")) {
            return "30000";
        }
        return Integer.toString(30 * Integer.parseInt(height));
    }
    
    public LinkedHashMap getFields() {
        return this.field;
    }
    
    public HashMap getUnits() {
        return this.unit;
    }
    
    public static void main(final String[] args) throws IOException {
        String report = null;
        report = "SOCA 202000Z 10003KT RMK 98015 933002 CIGNO CIG 125 OVC V BKN -FZRA $";
        final MetarParseReport func = new MetarParseReport();
        if (args.length == 1) {
            System.out.println(args[0] + " " + args.length);
            final InputStream ios = new FileInputStream(args[0]);
            final BufferedReader dataIS = new BufferedReader(new InputStreamReader(ios));
            while (true) {
                report = dataIS.readLine();
                if (report == null) {
                    break;
                }
                System.out.println("\n" + report);
                if (!func.parseReport(report)) {
                    System.out.println("badly formed report, can't parse");
                }
                else {
                    final LinkedHashMap field = func.getFields();
                    final HashMap unit = func.getUnits();
                    System.out.println("<report>");
                    for (final String key : field.keySet()) {
                        System.out.println("\t<parameter name=\"" + key + "\" value=\"" + field.get(key) + "\" units=\"" + unit.get(key) + "\" />");
                    }
                    System.out.println("</report>");
                }
            }
            return;
        }
        System.out.println(report);
        if (!func.parseReport(report)) {
            System.out.println("badly formed report, can't parse");
            System.exit(1);
        }
        final LinkedHashMap field2 = func.getFields();
        final HashMap unit2 = func.getUnits();
        System.out.println("<report>");
        for (final String key2 : field2.keySet()) {
            System.out.println("\t<parameter name=\"" + key2 + "\" value=\"" + field2.get(key2) + "\" units=\"" + unit2.get(key2) + "\" />");
        }
        System.out.println("</report>");
    }
}
