// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.dt.point;

import ucar.ma2.StructureDataIterator;
import java.util.Map;
import java.util.Collections;
import java.util.Arrays;
import ucar.nc2.dt.StationImpl;
import ucar.nc2.units.DateUnit;
import ucar.unidata.util.StringUtil;
import ucar.ma2.ArrayStructureMA;
import ucar.nc2.Group;
import ucar.nc2.dataset.VariableDS;
import ucar.nc2.Structure;
import ucar.ma2.StructureMembers;
import ucar.nc2.dataset.NetcdfDataset;
import ucar.unidata.geoloc.LatLonPoint;
import ucar.unidata.geoloc.LatLonPointImpl;
import ucar.ma2.ArrayStructureW;
import ucar.ma2.StructureData;
import ucar.nc2.dt.StationObsDatatype;
import ucar.ma2.InvalidRangeException;
import ucar.ma2.Array;
import ucar.ma2.ArrayDouble;
import java.util.Iterator;
import java.util.Collection;
import ucar.nc2.Variable;
import ucar.unidata.geoloc.LatLonRect;
import ucar.nc2.Attribute;
import ucar.ma2.DataType;
import ucar.nc2.VariableSimpleIF;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import ucar.ma2.ArrayInt;
import ucar.ma2.ArrayObject;
import java.util.HashMap;
import java.util.Date;
import ucar.unidata.geoloc.Station;
import java.util.List;
import ucar.nc2.Dimension;
import java.util.Set;
import ucar.nc2.NetcdfFileWriteable;
import ucar.nc2.units.DateFormatter;

public class WriterProfileObsDataset
{
    private static final String recordDimName = "record";
    private static final String stationDimName = "station";
    private static final String profileDimName = "profile";
    private static final String latName = "latitude";
    private static final String lonName = "longitude";
    private static final String altName = "altitude";
    private static final String timeName = "time";
    private static final String idName = "station_id";
    private static final String descName = "station_description";
    private static final String wmoName = "wmo_id";
    private static final String firstProfileName = "firstProfile";
    private static final String nextProfileName = "nextProfile";
    private static final String numProfilesName = "numProfiles";
    private static final String numProfilesTotalName = "numProfilesTotal";
    private static final String firstObsName = "firstChild";
    private static final String numObsName = "numChildren";
    private static final String nextObsName = "nextChild";
    private static final String parentStationIndex = "station_index";
    private static final String parentProfileIndex = "profile_index";
    private DateFormatter dateFormatter;
    private int name_strlen;
    private int desc_strlen;
    private int wmo_strlen;
    private NetcdfFileWriteable ncfile;
    private String title;
    private Set<Dimension> dimSet;
    private List<Dimension> recordDims;
    private List<Dimension> stationDims;
    private List<Dimension> profileDims;
    private List<Station> stnList;
    private Date minDate;
    private Date maxDate;
    private int nprofiles;
    private int profileIndex;
    private boolean useAlt;
    private boolean useWmoId;
    private boolean debug;
    private HashMap<String, StationTracker> stationMap;
    private int recno;
    private ArrayObject.D1 timeArray;
    private ArrayInt.D1 prevArray;
    private ArrayInt.D1 parentArray;
    private int[] origin;
    private int[] originTime;
    
    public WriterProfileObsDataset(final String fileOut, final String title) throws IOException {
        this.dateFormatter = new DateFormatter();
        this.name_strlen = 1;
        this.desc_strlen = 1;
        this.wmo_strlen = 1;
        this.dimSet = new HashSet<Dimension>();
        this.recordDims = new ArrayList<Dimension>();
        this.stationDims = new ArrayList<Dimension>();
        this.profileDims = new ArrayList<Dimension>();
        this.minDate = null;
        this.maxDate = null;
        this.profileIndex = 0;
        this.useAlt = false;
        this.useWmoId = false;
        this.debug = false;
        this.recno = 0;
        this.timeArray = new ArrayObject.D1(String.class, 1);
        this.prevArray = new ArrayInt.D1(1);
        this.parentArray = new ArrayInt.D1(1);
        this.origin = new int[1];
        this.originTime = new int[2];
        (this.ncfile = NetcdfFileWriteable.createNew(fileOut, false)).setFill(false);
        this.title = title;
    }
    
    public void setLength(final long size) {
        this.ncfile.setLength(size);
    }
    
    public void writeHeader(final List<Station> stns, final List<VariableSimpleIF> vars, final int nprofiles, final String altVarName) throws IOException {
        this.createGlobalAttributes();
        this.createStations(stns);
        this.createProfiles(nprofiles);
        this.ncfile.addGlobalAttribute("zaxis_coordinate", altVarName);
        this.ncfile.addGlobalAttribute("time_coverage_start", this.dateFormatter.toDateTimeStringISO(new Date()));
        this.ncfile.addGlobalAttribute("time_coverage_end", this.dateFormatter.toDateTimeStringISO(new Date()));
        this.createDataVariables(vars);
        this.ncfile.create();
        this.writeStationData(stns);
        if (!(boolean)this.ncfile.sendIospMessage("AddRecordStructure")) {
            throw new IllegalStateException("can't add record variable");
        }
    }
    
    private void createGlobalAttributes() {
        this.ncfile.addGlobalAttribute("Conventions", "Unidata Observation Dataset v1.0");
        this.ncfile.addGlobalAttribute("cdm_datatype", "Profile");
        this.ncfile.addGlobalAttribute("title", this.title);
        this.ncfile.addGlobalAttribute("desc", "Extracted by THREDDS/Netcdf Subset Service");
    }
    
    private void createStations(final List<Station> stnList) throws IOException {
        final int nstns = stnList.size();
        for (int i = 0; i < nstns; ++i) {
            final Station stn = stnList.get(i);
            if (stn.getWmoId() != null && stn.getWmoId().trim().length() > 0) {
                this.useWmoId = true;
            }
        }
        for (int i = 0; i < nstns; ++i) {
            final Station station = stnList.get(i);
            this.name_strlen = Math.max(this.name_strlen, station.getName().length());
            this.desc_strlen = Math.max(this.desc_strlen, station.getDescription().length());
            if (this.useWmoId) {
                this.wmo_strlen = Math.max(this.wmo_strlen, station.getName().length());
            }
        }
        final LatLonRect llbb = this.getBoundingBox(stnList);
        this.ncfile.addGlobalAttribute("geospatial_lat_min", Double.toString(llbb.getLowerLeftPoint().getLatitude()));
        this.ncfile.addGlobalAttribute("geospatial_lat_max", Double.toString(llbb.getUpperRightPoint().getLatitude()));
        this.ncfile.addGlobalAttribute("geospatial_lon_min", Double.toString(llbb.getLowerLeftPoint().getLongitude()));
        this.ncfile.addGlobalAttribute("geospatial_lon_max", Double.toString(llbb.getUpperRightPoint().getLongitude()));
        final Dimension recordDim = this.ncfile.addUnlimitedDimension("record");
        this.recordDims.add(recordDim);
        final Dimension stationDim = this.ncfile.addDimension("station", nstns);
        this.stationDims.add(stationDim);
        Variable v = this.ncfile.addVariable("latitude", DataType.DOUBLE, "station");
        this.ncfile.addVariableAttribute(v, new Attribute("units", "degrees_north"));
        this.ncfile.addVariableAttribute(v, new Attribute("long_name", "station latitude"));
        v = this.ncfile.addVariable("longitude", DataType.DOUBLE, "station");
        this.ncfile.addVariableAttribute(v, new Attribute("units", "degrees_east"));
        this.ncfile.addVariableAttribute(v, new Attribute("long_name", "station longitude"));
        if (this.useAlt) {
            v = this.ncfile.addVariable("altitude", DataType.DOUBLE, "station");
            this.ncfile.addVariableAttribute(v, new Attribute("units", "meters"));
            this.ncfile.addVariableAttribute(v, new Attribute("long_name", "station altitude"));
        }
        v = this.ncfile.addStringVariable("station_id", this.stationDims, this.name_strlen);
        this.ncfile.addVariableAttribute(v, new Attribute("long_name", "station identifier"));
        v = this.ncfile.addStringVariable("station_description", this.stationDims, this.desc_strlen);
        this.ncfile.addVariableAttribute(v, new Attribute("long_name", "station description"));
        if (this.useWmoId) {
            v = this.ncfile.addStringVariable("wmo_id", this.stationDims, this.wmo_strlen);
            this.ncfile.addVariableAttribute(v, new Attribute("long_name", "station WMO id"));
        }
        v = this.ncfile.addVariable("numProfiles", DataType.INT, "station");
        this.ncfile.addVariableAttribute(v, new Attribute("long_name", "number of profiles in linked list for this station"));
        v = this.ncfile.addVariable("firstProfile", DataType.INT, "station");
        this.ncfile.addVariableAttribute(v, new Attribute("long_name", "index of first profile in linked list for this station"));
    }
    
    private void createProfiles(final int nprofiles) throws IOException {
        this.nprofiles = nprofiles;
        final Dimension profileDim = this.ncfile.addDimension("profile", nprofiles);
        this.profileDims.add(profileDim);
        Variable v = this.ncfile.addVariable("numChildren", DataType.INT, "profile");
        this.ncfile.addVariableAttribute(v, new Attribute("long_name", "number of children in linked list for this profile"));
        v = this.ncfile.addVariable("numProfilesTotal", DataType.INT, "");
        this.ncfile.addVariableAttribute(v, new Attribute("long_name", "number of valid profiles"));
        v = this.ncfile.addVariable("firstChild", DataType.INT, "profile");
        this.ncfile.addVariableAttribute(v, new Attribute("long_name", "record number of first obs in linked list for this profile"));
        final Variable timeVar = this.ncfile.addStringVariable("time", this.profileDims, 20);
        this.ncfile.addVariableAttribute(timeVar, new Attribute("long_name", "ISO-8601 Date - time of observation"));
        v = this.ncfile.addVariable("station_index", DataType.INT, "profile");
        this.ncfile.addVariableAttribute(v, new Attribute("long_name", "index of parent station"));
        v = this.ncfile.addVariable("nextProfile", DataType.INT, "profile");
        this.ncfile.addVariableAttribute(v, new Attribute("long_name", "index of next profile in linked list for this station"));
    }
    
    private void createDataVariables(final List<VariableSimpleIF> dataVars) throws IOException {
        Variable v = this.ncfile.addVariable("profile_index", DataType.INT, "record");
        this.ncfile.addVariableAttribute(v, new Attribute("long_name", "index of parent profile"));
        v = this.ncfile.addVariable("nextChild", DataType.INT, "record");
        this.ncfile.addVariableAttribute(v, new Attribute("long_name", "record number of next obs in linked list for this profile"));
        for (final VariableSimpleIF var : dataVars) {
            final List<Dimension> dims = var.getDimensions();
            this.dimSet.addAll(dims);
        }
        for (final Dimension d : this.dimSet) {
            if (!d.isUnlimited()) {
                this.ncfile.addDimension(d.getName(), d.getLength(), d.isShared(), false, d.isVariableLength());
            }
        }
        for (final VariableSimpleIF oldVar : dataVars) {
            final List<Dimension> dims = oldVar.getDimensions();
            final StringBuffer dimNames = new StringBuffer("record");
            for (final Dimension d2 : dims) {
                if (!d2.isUnlimited()) {
                    dimNames.append(" ").append(d2.getName());
                }
            }
            final Variable newVar = this.ncfile.addVariable(oldVar.getName(), oldVar.getDataType(), dimNames.toString());
            final List<Attribute> atts = oldVar.getAttributes();
            for (final Attribute att : atts) {
                this.ncfile.addVariableAttribute(newVar, att);
            }
        }
    }
    
    private void writeStationData(final List<Station> stnList) throws IOException {
        this.stnList = stnList;
        final int nstns = stnList.size();
        this.stationMap = new HashMap<String, StationTracker>(2 * nstns);
        if (this.debug) {
            System.out.println("stationMap created");
        }
        final ArrayDouble.D1 latArray = new ArrayDouble.D1(nstns);
        final ArrayDouble.D1 lonArray = new ArrayDouble.D1(nstns);
        final ArrayDouble.D1 altArray = new ArrayDouble.D1(nstns);
        final ArrayObject.D1 idArray = new ArrayObject.D1(String.class, nstns);
        final ArrayObject.D1 descArray = new ArrayObject.D1(String.class, nstns);
        final ArrayObject.D1 wmoArray = new ArrayObject.D1(String.class, nstns);
        for (int i = 0; i < stnList.size(); ++i) {
            final Station stn = stnList.get(i);
            this.stationMap.put(stn.getName(), new StationTracker(i));
            latArray.set(i, stn.getLatitude());
            lonArray.set(i, stn.getLongitude());
            if (this.useAlt) {
                altArray.set(i, stn.getAltitude());
            }
            idArray.set(i, stn.getName());
            descArray.set(i, stn.getDescription());
            if (this.useWmoId) {
                wmoArray.set(i, stn.getWmoId());
            }
        }
        try {
            this.ncfile.write("latitude", latArray);
            this.ncfile.write("longitude", lonArray);
            if (this.useAlt) {
                this.ncfile.write("altitude", altArray);
            }
            this.ncfile.writeStringData("station_id", idArray);
            this.ncfile.writeStringData("station_description", descArray);
            if (this.useWmoId) {
                this.ncfile.writeStringData("wmo_id", wmoArray);
            }
        }
        catch (InvalidRangeException e) {
            e.printStackTrace();
            throw new IllegalStateException(e);
        }
    }
    
    private void writeDataFinish() throws IOException {
        final ArrayInt.D0 totalArray = new ArrayInt.D0();
        totalArray.set(this.profileIndex);
        try {
            this.ncfile.write("numProfilesTotal", totalArray);
        }
        catch (InvalidRangeException e) {
            e.printStackTrace();
            throw new IllegalStateException(e);
        }
        final int nstns = this.stnList.size();
        final ArrayInt.D1 firstProfileArray = new ArrayInt.D1(nstns);
        final ArrayInt.D1 numProfileArray = new ArrayInt.D1(nstns);
        final ArrayInt.D1 nextProfileArray = new ArrayInt.D1(this.nprofiles);
        for (int i = 0; i < this.stnList.size(); ++i) {
            final Station stn = this.stnList.get(i);
            final StationTracker tracker = this.stationMap.get(stn.getName());
            numProfileArray.set(i, tracker.numChildren);
            final int first = (tracker.link.size() > 0) ? tracker.link.get(0) : -1;
            firstProfileArray.set(i, first);
            if (tracker.link.size() > 0) {
                final List<Integer> nextList = tracker.link;
                for (int j = 0; j < nextList.size() - 1; ++j) {
                    final Integer curr = nextList.get(j);
                    final Integer next = nextList.get(j + 1);
                    nextProfileArray.set(curr, next);
                }
                final Integer curr2 = nextList.get(nextList.size() - 1);
                nextProfileArray.set(curr2, -1);
            }
        }
        try {
            this.ncfile.write("firstProfile", firstProfileArray);
            this.ncfile.write("numProfiles", numProfileArray);
            this.ncfile.write("nextProfile", nextProfileArray);
        }
        catch (InvalidRangeException e2) {
            e2.printStackTrace();
            throw new IllegalStateException(e2);
        }
        final ArrayInt.D1 nextObsArray = new ArrayInt.D1(this.recno);
        final ArrayInt.D1 firstObsArray = new ArrayInt.D1(this.nprofiles);
        final ArrayInt.D1 numObsArray = new ArrayInt.D1(this.nprofiles);
        for (int k = 0; k < this.stnList.size(); ++k) {
            final Station stn2 = this.stnList.get(k);
            final StationTracker stnTracker = this.stationMap.get(stn2.getName());
            final Set<Date> dates = stnTracker.profileMap.keySet();
            for (final Date date : dates) {
                final ProfileTracker proTracker = stnTracker.profileMap.get(date);
                final int trackerIndex = proTracker.parent_index;
                numObsArray.set(trackerIndex, proTracker.numChildren);
                final int first2 = (proTracker.link.size() > 0) ? proTracker.link.get(0) : -1;
                firstObsArray.set(trackerIndex, first2);
                if (proTracker.link.size() > 0) {
                    final List<Integer> nextList2 = proTracker.link;
                    for (int l = 0; l < nextList2.size() - 1; ++l) {
                        final Integer curr3 = nextList2.get(l);
                        final Integer next2 = nextList2.get(l + 1);
                        nextObsArray.set(curr3, next2);
                    }
                    final Integer curr4 = nextList2.get(nextList2.size() - 1);
                    nextObsArray.set(curr4, -1);
                }
            }
        }
        try {
            this.ncfile.write("firstChild", firstObsArray);
            this.ncfile.write("numChildren", numObsArray);
            this.ncfile.write("nextChild", nextObsArray);
        }
        catch (InvalidRangeException e3) {
            e3.printStackTrace();
            throw new IllegalStateException(e3);
        }
        this.ncfile.updateAttribute(null, new Attribute("time_coverage_start", this.dateFormatter.toDateTimeStringISO(this.minDate)));
        this.ncfile.updateAttribute(null, new Attribute("time_coverage_end", this.dateFormatter.toDateTimeStringISO(this.maxDate)));
    }
    
    public void writeRecord(final StationObsDatatype sobs, final StructureData sdata) throws IOException {
        if (this.debug) {
            System.out.println("sobs= " + sobs + "; station = " + sobs.getStation());
        }
        this.writeRecord(sobs.getStation().getName(), sobs.getObservationTimeAsDate(), sdata);
    }
    
    public void writeRecord(final String stnName, final Date obsDate, final StructureData sdata) throws IOException {
        final StationTracker stnTracker = this.stationMap.get(stnName);
        ProfileTracker proTracker = stnTracker.profileMap.get(obsDate);
        if (proTracker == null) {
            proTracker = new ProfileTracker(this.profileIndex);
            stnTracker.profileMap.put(obsDate, proTracker);
            stnTracker.link.add(this.profileIndex);
            stnTracker.lastChild = this.profileIndex;
            final StationTracker stationTracker = stnTracker;
            ++stationTracker.numChildren;
            try {
                this.originTime[0] = this.profileIndex;
                this.timeArray.set(0, this.dateFormatter.toDateTimeStringISO(obsDate));
                this.parentArray.set(0, stnTracker.parent_index);
                this.ncfile.writeStringData("time", this.originTime, this.timeArray);
                this.ncfile.write("station_index", this.originTime, this.parentArray);
            }
            catch (InvalidRangeException e) {
                e.printStackTrace();
                throw new IllegalStateException(e);
            }
            ++this.profileIndex;
        }
        final ArrayStructureW sArray = new ArrayStructureW(sdata.getStructureMembers(), new int[] { 1 });
        sArray.setStructureData(sdata, 0);
        if (this.minDate == null || this.minDate.after(obsDate)) {
            this.minDate = obsDate;
        }
        if (this.maxDate == null || this.maxDate.before(obsDate)) {
            this.maxDate = obsDate;
        }
        this.timeArray.set(0, this.dateFormatter.toDateTimeStringISO(obsDate));
        this.parentArray.set(0, proTracker.parent_index);
        proTracker.link.add(this.recno);
        proTracker.lastChild = this.recno;
        final ProfileTracker profileTracker = proTracker;
        ++profileTracker.numChildren;
        this.origin[0] = this.recno;
        this.originTime[0] = this.recno;
        try {
            this.ncfile.write("record", this.origin, sArray);
            this.ncfile.write("profile_index", this.originTime, this.parentArray);
        }
        catch (InvalidRangeException e2) {
            e2.printStackTrace();
            throw new IllegalStateException(e2);
        }
        ++this.recno;
    }
    
    public void finish() throws IOException {
        this.writeDataFinish();
        this.ncfile.close();
    }
    
    private LatLonRect getBoundingBox(final List stnList) {
        Station s = stnList.get(0);
        final LatLonPointImpl llpt = new LatLonPointImpl();
        llpt.set(s.getLatitude(), s.getLongitude());
        final LatLonRect rect = new LatLonRect(llpt, 0.001, 0.001);
        for (int i = 1; i < stnList.size(); ++i) {
            s = stnList.get(i);
            llpt.set(s.getLatitude(), s.getLongitude());
            rect.extend(llpt);
        }
        return rect;
    }
    
    public static void main(final String[] args) throws Exception {
        final long start = System.currentTimeMillis();
        final Map<String, Station> staHash = new HashMap<String, Station>();
        final String location = "R:/testdata/sounding/netcdf/Upperair_20070401_0000.nc";
        final NetcdfDataset ncfile = NetcdfDataset.openDataset(location);
        ncfile.sendIospMessage("AddRecordStructure");
        final StructureMembers sm = new StructureMembers("manLevel");
        final Dimension manDim = ncfile.findDimension("manLevel");
        final Structure record = (Structure)ncfile.findVariable("record");
        final List<Variable> allList = record.getVariables();
        final List<VariableSimpleIF> varList = new ArrayList<VariableSimpleIF>();
        for (final Variable v : allList) {
            if (v.getRank() == 1 && v.getDimension(0).equals(manDim)) {
                varList.add(new VariableDS(ncfile, null, null, v.getShortName(), v.getDataType(), "", v.getUnitsString(), v.getDescription()));
                sm.addMember(v.getShortName(), v.getDescription(), v.getUnitsString(), v.getDataType(), new int[0]);
            }
        }
        final ArrayStructureMA manAS = new ArrayStructureMA(sm, new int[] { manDim.getLength() });
        final Variable time = ncfile.findVariable("synTime");
        String timeUnits = ncfile.findAttValueIgnoreCase(time, "units", null);
        timeUnits = StringUtil.remove(timeUnits, 40);
        timeUnits = StringUtil.remove(timeUnits, 41);
        final DateUnit timeUnit = new DateUnit(timeUnits);
        int nrecs = 0;
        StructureDataIterator iter = record.getStructureIterator();
        while (iter.hasNext()) {
            final StructureData sdata = iter.next();
            final String name = sdata.getScalarString("staName");
            Station s = staHash.get(name);
            if (s == null) {
                final float lat = sdata.convertScalarFloat("staLat");
                final float lon = sdata.convertScalarFloat("staLon");
                final float elev = sdata.convertScalarFloat("staElev");
                s = new StationImpl(name, "", lat, lon, elev);
                staHash.put(name, s);
            }
            ++nrecs;
        }
        final List<Station> stnList = Arrays.asList((Station[])staHash.values().toArray((T[])new Station[staHash.size()]));
        Collections.sort(stnList);
        final WriterProfileObsDataset writer = new WriterProfileObsDataset(location + ".out", "rewrite " + location);
        writer.writeHeader(stnList, varList, nrecs, "prMan");
        iter = record.getStructureIterator();
        while (iter.hasNext()) {
            final StructureData sdata2 = iter.next();
            final String name2 = sdata2.getScalarString("staName");
            final double timeValue = sdata2.convertScalarDouble("synTime");
            final Date date = timeUnit.makeDate(timeValue);
            final List<String> names = sm.getMemberNames();
            for (final String mname : names) {
                manAS.setMemberArray(mname, sdata2.getArray(mname));
            }
            final int numMand = sdata2.getScalarInt("numMand");
            if (numMand >= manDim.getLength()) {
                continue;
            }
            for (int i = 0; i < numMand; ++i) {
                final StructureData useData = manAS.getStructureData(i);
                writer.writeRecord(name2, date, useData);
            }
        }
        writer.finish();
        final long took = System.currentTimeMillis() - start;
        System.out.println("That took = " + took);
    }
    
    private class StationTracker
    {
        int numChildren;
        int lastChild;
        int parent_index;
        List<Integer> link;
        HashMap<Date, ProfileTracker> profileMap;
        
        StationTracker(final int parent_index) {
            this.numChildren = 0;
            this.lastChild = -1;
            this.link = new ArrayList<Integer>();
            this.parent_index = parent_index;
            this.profileMap = new HashMap<Date, ProfileTracker>();
        }
    }
    
    private class ProfileTracker
    {
        int numChildren;
        int lastChild;
        int parent_index;
        List<Integer> link;
        
        ProfileTracker(final int parent_index) {
            this.numChildren = 0;
            this.lastChild = -1;
            this.link = new ArrayList<Integer>();
            this.parent_index = parent_index;
        }
    }
}
