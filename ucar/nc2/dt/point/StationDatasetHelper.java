// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.dt.point;

import java.util.Comparator;
import java.util.Collections;
import java.util.Collection;
import ucar.nc2.dt.StationObsDatatype;
import java.util.HashMap;
import java.util.Iterator;
import java.util.ArrayList;
import ucar.nc2.util.CancelTask;
import java.util.List;
import ucar.unidata.geoloc.LatLonPoint;
import ucar.unidata.geoloc.LatLonPointImpl;
import java.io.IOException;
import ucar.unidata.geoloc.LatLonRect;
import ucar.unidata.geoloc.Station;
import java.util.Map;
import ucar.nc2.dt.StationObsDataset;

public class StationDatasetHelper
{
    private StationObsDataset obsDataset;
    private Map<String, Station> stationHash;
    private boolean debug;
    private LatLonRect rect;
    
    public StationDatasetHelper(final StationObsDataset obsDataset) {
        this.debug = false;
        this.obsDataset = obsDataset;
    }
    
    public LatLonRect getBoundingBox() {
        if (this.rect == null) {
            List stations;
            try {
                stations = this.obsDataset.getStations();
            }
            catch (IOException e) {
                return null;
            }
            if (stations.size() == 0) {
                return null;
            }
            Station s = stations.get(0);
            final LatLonPointImpl llpt = new LatLonPointImpl();
            llpt.set(s.getLatitude(), s.getLongitude());
            this.rect = new LatLonRect(llpt, 0.001, 0.001);
            if (this.debug) {
                System.out.println("start=" + s.getLatitude() + " " + s.getLongitude() + " rect= " + this.rect.toString2());
            }
            for (int i = 1; i < stations.size(); ++i) {
                s = stations.get(i);
                llpt.set(s.getLatitude(), s.getLongitude());
                this.rect.extend(llpt);
                if (this.debug) {
                    System.out.println("add=" + s.getLatitude() + " " + s.getLongitude() + " rect= " + this.rect.toString2());
                }
            }
        }
        if (this.rect.crossDateline() && this.rect.getWidth() > 350.0) {
            final double lat_min = this.rect.getLowerLeftPoint().getLatitude();
            final double deltaLat = this.rect.getUpperLeftPoint().getLatitude() - lat_min;
            this.rect = new LatLonRect(new LatLonPointImpl(lat_min, -180.0), deltaLat, 360.0);
        }
        return this.rect;
    }
    
    public List<Station> getStations(final LatLonRect boundingBox, final CancelTask cancel) throws IOException {
        final LatLonPointImpl latlonPt = new LatLonPointImpl();
        final List<Station> result = new ArrayList<Station>();
        final List<Station> stations = this.obsDataset.getStations();
        for (final Station s : stations) {
            latlonPt.set(s.getLatitude(), s.getLongitude());
            if (boundingBox.contains(latlonPt)) {
                result.add(s);
            }
            if (cancel != null && cancel.isCancel()) {
                return null;
            }
        }
        return result;
    }
    
    public Station getStation(final String name) {
        if (this.stationHash == null) {
            List<Station> stations;
            try {
                stations = this.obsDataset.getStations();
            }
            catch (IOException e) {
                return null;
            }
            this.stationHash = new HashMap<String, Station>(2 * stations.size());
            for (final Station s : stations) {
                this.stationHash.put(s.getName(), s);
            }
        }
        return this.stationHash.get(name);
    }
    
    public List getStationObs(final Station s, final double startTime, final double endTime, final CancelTask cancel) throws IOException {
        final ArrayList result = new ArrayList();
        final List stationObs = this.obsDataset.getData(s, cancel);
        for (int i = 0; i < stationObs.size(); ++i) {
            final StationObsDatatype obs = stationObs.get(i);
            final double timeValue = obs.getObservationTime();
            if (timeValue >= startTime && timeValue <= endTime) {
                result.add(obs);
            }
            if (cancel != null && cancel.isCancel()) {
                return null;
            }
        }
        return result;
    }
    
    public List getStationObs(final List<Station> stations, final CancelTask cancel) throws IOException {
        final ArrayList result = new ArrayList();
        for (int i = 0; i < stations.size(); ++i) {
            final Station s = stations.get(i);
            result.addAll(this.obsDataset.getData(s, cancel));
            if (cancel != null && cancel.isCancel()) {
                return null;
            }
        }
        return result;
    }
    
    public List getStationObs(final List<Station> stations, final double startTime, final double endTime, final CancelTask cancel) throws IOException {
        final ArrayList result = new ArrayList();
        for (int i = 0; i < stations.size(); ++i) {
            final Station s = stations.get(i);
            result.addAll(this.getStationObs(s, startTime, endTime, cancel));
            if (cancel != null && cancel.isCancel()) {
                return null;
            }
        }
        return result;
    }
    
    public List getStationObs(final LatLonRect boundingBox, final CancelTask cancel) throws IOException {
        final List stations = this.obsDataset.getStations(boundingBox, cancel);
        if (stations == null) {
            return null;
        }
        return this.getStationObs(stations, cancel);
    }
    
    public List getStationObs(final LatLonRect boundingBox, final double startTime, final double endTime, final CancelTask cancel) throws IOException {
        final List stations = this.obsDataset.getStations(boundingBox);
        if (stations == null) {
            return null;
        }
        return this.getStationObs(stations, startTime, endTime, cancel);
    }
    
    public void sortByTime(final List<StationObsDatatype> stationObs) {
        Collections.sort(stationObs, new StationObsComparator());
    }
    
    private class StationObsComparator implements Comparator
    {
        public int compare(final Object o1, final Object o2) {
            final StationObsDatatype s1 = (StationObsDatatype)o1;
            final StationObsDatatype s2 = (StationObsDatatype)o2;
            return (int)(s1.getObservationTime() - s2.getObservationTime());
        }
    }
}
