// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.dt.point;

import ucar.ma2.StructureData;
import ucar.nc2.dt.DatatypeIterator;
import ucar.nc2.Structure;
import ucar.nc2.dt.DataIterator;
import java.util.Date;
import ucar.unidata.geoloc.LatLonRect;
import java.util.List;
import ucar.nc2.constants.AxisType;
import java.io.IOException;
import ucar.nc2.dt.TypedDataset;
import ucar.nc2.util.CancelTask;
import ucar.nc2.dataset.NetcdfDataset;
import java.util.StringTokenizer;
import ucar.nc2.constants.FeatureType;
import ucar.nc2.NetcdfFile;
import java.util.ArrayList;
import ucar.nc2.Variable;
import ucar.nc2.dt.TypedDatasetFactoryIF;

public class UnidataPointObsDataset extends PointObsDatasetImpl implements TypedDatasetFactoryIF
{
    private Variable latVar;
    private Variable lonVar;
    private Variable altVar;
    private Variable timeVar;
    private Variable timeNominalVar;
    private RecordDatasetHelper recordHelper;
    private ArrayList allData;
    
    public static boolean isValidFile(final NetcdfFile ds) {
        if (!ds.findAttValueIgnoreCase(null, "cdm_data_type", "").equalsIgnoreCase(FeatureType.POINT.toString()) && !ds.findAttValueIgnoreCase(null, "cdm_datatype", "").equalsIgnoreCase(FeatureType.POINT.toString())) {
            return false;
        }
        final String conv = ds.findAttValueIgnoreCase(null, "Conventions", null);
        if (conv == null) {
            return false;
        }
        final StringTokenizer stoke = new StringTokenizer(conv, ",");
        while (stoke.hasMoreTokens()) {
            final String toke = stoke.nextToken().trim();
            if (toke.equalsIgnoreCase("Unidata Observation Dataset v1.0")) {
                return true;
            }
        }
        return false;
    }
    
    public boolean isMine(final NetcdfDataset ds) {
        return isValidFile(ds);
    }
    
    public TypedDataset open(final NetcdfDataset ncd, final CancelTask task, final StringBuilder errlog) throws IOException {
        return new UnidataPointObsDataset(ncd);
    }
    
    public UnidataPointObsDataset() {
    }
    
    public UnidataPointObsDataset(final NetcdfDataset ds) throws IOException {
        super(ds);
        this.latVar = UnidataObsDatasetHelper.getCoordinate(ds, AxisType.Lat);
        this.lonVar = UnidataObsDatasetHelper.getCoordinate(ds, AxisType.Lon);
        this.timeVar = UnidataObsDatasetHelper.getCoordinate(ds, AxisType.Time);
        if (this.latVar == null) {
            throw new IllegalStateException("Missing latitude variable");
        }
        if (this.lonVar == null) {
            throw new IllegalStateException("Missing longitude coordinate variable");
        }
        if (this.timeVar == null) {
            throw new IllegalStateException("Missing time coordinate variable");
        }
        this.altVar = UnidataObsDatasetHelper.getCoordinate(ds, AxisType.Height);
        this.timeNominalVar = UnidataObsDatasetHelper.findVariable(ds, "record.time_nominal");
        final String recDimName = ds.findAttValueIgnoreCase(null, "observationDimension", null);
        (this.recordHelper = new RecordDatasetHelper(ds, this.timeVar.getName(), (this.timeNominalVar == null) ? null : this.timeNominalVar.getName(), this.dataVariables, recDimName, this.parseInfo)).setLocationInfo(this.latVar.getName(), this.lonVar.getName(), (this.altVar == null) ? null : this.altVar.getName());
        this.recordHelper.setShortNames(this.latVar.getShortName(), this.lonVar.getShortName(), (this.altVar == null) ? null : this.altVar.getShortName(), this.timeVar.getShortName(), (this.timeNominalVar == null) ? null : this.timeNominalVar.getShortName());
        this.allData = this.recordHelper.readAllCreateObs(null);
        this.removeDataVariable(this.timeVar.getName());
        if (this.timeNominalVar != null) {
            this.removeDataVariable(this.timeNominalVar.getName());
        }
        this.removeDataVariable(this.latVar.getName());
        this.removeDataVariable(this.lonVar.getName());
        if (this.altVar != null) {
            this.removeDataVariable(this.altVar.getName());
        }
        this.timeUnit = this.recordHelper.timeUnit;
        try {
            this.startDate = UnidataObsDatasetHelper.getStartDate(ds);
            this.endDate = UnidataObsDatasetHelper.getEndDate(ds);
        }
        catch (IllegalArgumentException e) {
            this.parseInfo.append("Missing time_coverage_start or end attributes");
        }
        try {
            this.boundingBox = UnidataObsDatasetHelper.getBoundingBox(ds);
        }
        catch (IllegalArgumentException e) {
            this.parseInfo.append("Missing geospatial_lat(lon)_min(max) attributes");
        }
        this.setTimeUnits();
        this.title = ds.findAttValueIgnoreCase(null, "title", null);
        this.desc = ds.findAttValueIgnoreCase(null, "description", null);
    }
    
    @Override
    protected void setTimeUnits() {
        this.timeUnit = this.recordHelper.timeUnit;
    }
    
    @Override
    protected void setStartDate() {
        this.startDate = this.timeUnit.makeDate(this.recordHelper.minDate);
    }
    
    @Override
    protected void setEndDate() {
        this.endDate = this.timeUnit.makeDate(this.recordHelper.maxDate);
    }
    
    @Override
    protected void setBoundingBox() {
        this.boundingBox = this.recordHelper.boundingBox;
    }
    
    public List getData(final CancelTask cancel) throws IOException {
        return this.allData;
    }
    
    public int getDataCount() {
        return (int)this.recordHelper.getRecordVar().getSize();
    }
    
    public List getData(final LatLonRect boundingBox, final CancelTask cancel) throws IOException {
        return this.recordHelper.getData(this.allData, boundingBox, cancel);
    }
    
    public List getData(final LatLonRect boundingBox, final Date start, final Date end, final CancelTask cancel) throws IOException {
        final double startTime = this.timeUnit.makeValue(start);
        final double endTime = this.timeUnit.makeValue(end);
        return this.recordHelper.getData(this.allData, boundingBox, startTime, endTime, cancel);
    }
    
    public DataIterator getDataIterator(final int bufferSize) throws IOException {
        return new PointDatatypeIterator(this.recordHelper.recordVar, bufferSize);
    }
    
    private class PointDatatypeIterator extends DatatypeIterator
    {
        @Override
        protected Object makeDatatypeWithData(final int recnum, final StructureData sdata) {
            return UnidataPointObsDataset.this.recordHelper.new RecordPointObs(recnum, sdata);
        }
        
        PointDatatypeIterator(final Structure struct, final int bufferSize) {
            super(struct, bufferSize);
        }
    }
}
