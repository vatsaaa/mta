// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.dt;

import ucar.nc2.constants.FeatureType;
import java.io.IOException;
import ucar.nc2.util.CancelTask;
import ucar.nc2.dataset.NetcdfDataset;

public interface TypedDatasetFactoryIF
{
    boolean isMine(final NetcdfDataset p0);
    
    TypedDataset open(final NetcdfDataset p0, final CancelTask p1, final StringBuilder p2) throws IOException;
    
    FeatureType getScientificDataType();
}
