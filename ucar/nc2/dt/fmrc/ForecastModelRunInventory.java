// 
// Decompiled by Procyon v0.5.36
// 

package ucar.nc2.dt.fmrc;

import ucar.nc2.util.Misc;
import ucar.nc2.dt.fmr.FmrcCoordSys;
import org.slf4j.LoggerFactory;
import ucar.nc2.util.IO;
import java.io.File;
import ucar.nc2.util.DiskCache2;
import ucar.unidata.geoloc.LatLonPointImpl;
import java.util.StringTokenizer;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import java.io.InputStream;
import java.io.BufferedInputStream;
import java.io.FileInputStream;
import ucar.unidata.geoloc.LatLonPoint;
import org.jdom.Content;
import java.util.Collections;
import org.jdom.Element;
import org.jdom.Document;
import org.jdom.output.XMLOutputter;
import org.jdom.output.Format;
import java.io.OutputStream;
import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import ucar.ma2.InvalidRangeException;
import ucar.nc2.iosp.IOServiceProvider;
import ucar.nc2.dataset.NetcdfDataset;
import java.io.IOException;
import ucar.nc2.dataset.CoordinateAxis1D;
import ucar.nc2.dataset.VariableEnhanced;
import ucar.nc2.dt.GridCoordSystem;
import java.util.Iterator;
import ucar.nc2.NetcdfFile;
import ucar.nc2.dt.GridDatatype;
import ucar.nc2.units.DateFormatter;
import ucar.nc2.units.DateUnit;
import ucar.nc2.Variable;
import java.util.ArrayList;
import ucar.nc2.iosp.grid.GridServiceProvider;
import ucar.unidata.geoloc.LatLonRect;
import ucar.nc2.dt.GridDataset;
import java.util.Date;
import java.util.List;
import org.slf4j.Logger;

public class ForecastModelRunInventory
{
    public static final int OPEN_NORMAL = 1;
    public static final int OPEN_FORCE_NEW = 2;
    public static final int OPEN_XML_ONLY = 3;
    private static Logger log;
    private String name;
    private List<TimeCoord> times;
    private List<VertCoord> vaxes;
    private List<EnsCoord> eaxes;
    private Date runDate;
    private String runTime;
    private GridDataset gds;
    private LatLonRect bb;
    private boolean debugMissing;
    private GridServiceProvider gribIosp;
    private int tc_seqno;
    private int vc_seqno;
    private int ec_seqno;
    private static boolean debug;
    private static boolean showXML;
    
    private ForecastModelRunInventory() {
        this.times = new ArrayList<TimeCoord>();
        this.vaxes = new ArrayList<VertCoord>();
        this.eaxes = new ArrayList<EnsCoord>();
        this.debugMissing = false;
        this.tc_seqno = 0;
        this.vc_seqno = 0;
        this.ec_seqno = 0;
    }
    
    private ForecastModelRunInventory(final GridDataset gds, final Date runDate) {
        this.times = new ArrayList<TimeCoord>();
        this.vaxes = new ArrayList<VertCoord>();
        this.eaxes = new ArrayList<EnsCoord>();
        this.debugMissing = false;
        this.tc_seqno = 0;
        this.vc_seqno = 0;
        this.ec_seqno = 0;
        this.gds = gds;
        this.name = gds.getTitle();
        final NetcdfFile ncfile = gds.getNetcdfFile();
        if (runDate == null) {
            this.runTime = ncfile.findAttValueIgnoreCase(null, "_CoordinateModelBaseDate", null);
            if (this.runTime == null) {
                this.runTime = ncfile.findAttValueIgnoreCase(null, "_CoordinateModelRunDate", null);
            }
            if (this.runTime == null) {
                throw new IllegalArgumentException("File must have _CoordinateModelBaseDate or _CoordinateModelRunDate attribute ");
            }
            this.runDate = DateUnit.getStandardOrISO(this.runTime);
            if (this.runDate == null) {
                throw new IllegalArgumentException("_CoordinateModelRunDate must be ISO date string " + this.runTime);
            }
        }
        else {
            this.runDate = runDate;
            final DateFormatter df = new DateFormatter();
            this.runTime = df.toDateTimeStringISO(runDate);
        }
        this.getIosp();
        for (final GridDatatype gg : gds.getGrids()) {
            final GridCoordSystem gcs = gg.getCoordinateSystem();
            final Grid grid = new Grid(gg.getName());
            final VariableEnhanced ve = gg.getVariable();
            final Variable v = ve.getOriginalVariable();
            this.addMissing(v, gcs, grid);
            final CoordinateAxis1D axis = gcs.getTimeAxis1D();
            if (axis != null) {
                final TimeCoord tc = this.getTimeCoordinate(axis);
                tc.vars.add(grid);
                grid.parent = tc;
            }
            final CoordinateAxis1D eaxis = gcs.getEnsembleAxis();
            if (eaxis != null) {
                final int[] einfo = this.getEnsInfo(v);
                grid.ec = this.getEnsCoordinate(eaxis, einfo);
            }
            final CoordinateAxis1D vaxis = gcs.getVerticalAxis();
            if (vaxis != null) {
                grid.vc = this.getVertCoordinate(vaxis);
            }
            final LatLonRect rect = gcs.getLatLonBoundingBox();
            if (null == this.bb) {
                this.bb = rect;
            }
            else {
                if (this.bb.equals(rect)) {
                    continue;
                }
                this.bb.extend(rect);
            }
        }
    }
    
    public void close() throws IOException {
        if (null != this.gds) {
            this.gds.close();
        }
    }
    
    public void setName(final String name) {
        this.name = name;
    }
    
    public String getName() {
        return this.name;
    }
    
    public Date getRunDate() {
        return this.runDate;
    }
    
    public String getRunDateString() {
        return this.runTime;
    }
    
    public List<TimeCoord> getTimeCoords() {
        return this.times;
    }
    
    public List<VertCoord> getVertCoords() {
        return this.vaxes;
    }
    
    public LatLonRect getBB() {
        return this.bb;
    }
    
    public void releaseDataset() throws IOException {
        if (this.gds == null) {
            return;
        }
        this.gds.close();
        for (final TimeCoord tc : this.times) {
            tc.axis = null;
        }
        for (final VertCoord vc : this.vaxes) {
            vc.axis = null;
        }
    }
    
    public Grid findGrid(final String name) {
        for (final TimeCoord tc : this.times) {
            final List<Grid> grids = tc.getGrids();
            for (final Grid g : grids) {
                if (g.name.equals(name)) {
                    return g;
                }
            }
        }
        return null;
    }
    
    private void getIosp() {
        NetcdfDataset ncd;
        NetcdfFile ncfile;
        for (ncd = (NetcdfDataset)this.gds.getNetcdfFile(), ncfile = ncd.getReferencedFile(); ncfile instanceof NetcdfDataset; ncfile = ncd.getReferencedFile()) {
            ncd = (NetcdfDataset)ncfile;
        }
        if (ncfile == null) {
            return;
        }
        final IOServiceProvider iosp = ncfile.getIosp();
        if (iosp == null) {
            return;
        }
        if (!(iosp instanceof GridServiceProvider)) {
            return;
        }
        this.gribIosp = (GridServiceProvider)iosp;
    }
    
    private void addMissing(final Variable v, final GridCoordSystem gcs, final Grid grid) {
        if (this.gribIosp == null) {
            return;
        }
        if (gcs.getVerticalAxis() == null && gcs.getEnsembleAxis() == null) {
            return;
        }
        final int ntimes = (int)gcs.getTimeAxis().getSize();
        int nverts = 1;
        if (gcs.getVerticalAxis() != null) {
            nverts = (int)gcs.getVerticalAxis().getSize();
        }
        int nens = 1;
        if (gcs.getEnsembleAxis() != null) {
            nens = (int)gcs.getEnsembleAxis().getSize();
        }
        final int total = ntimes * nens * nverts;
        final List<Missing> missing = new ArrayList<Missing>();
        for (int timeIndex = 0; timeIndex < ntimes; ++timeIndex) {
            for (int ensIndex = 0; ensIndex < nens; ++ensIndex) {
                for (int vertIndex = 0; vertIndex < nverts; ++vertIndex) {
                    try {
                        if (this.gribIosp.isMissingXY(v, timeIndex, ensIndex, vertIndex)) {
                            missing.add(new Missing(timeIndex, ensIndex, vertIndex));
                        }
                    }
                    catch (InvalidRangeException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        if (missing.size() > 0) {
            grid.missing = missing;
            if (this.debugMissing) {
                System.out.println("Missing " + this.gds.getTitle() + " " + v.getName() + " # =" + missing.size() + "/" + total);
            }
        }
        else if (this.debugMissing) {
            System.out.println(" None missing for " + this.gds.getTitle() + " " + v.getName() + " total = " + total);
        }
    }
    
    private int[] getEnsInfo(final Variable v) {
        return null;
    }
    
    private TimeCoord getTimeCoordinate(final CoordinateAxis1D axis) {
        for (final TimeCoord tc : this.times) {
            if (tc.axis != null && tc.axis == axis) {
                return tc;
            }
        }
        final TimeCoord want = new TimeCoord(this.runDate, axis);
        for (final TimeCoord tc2 : this.times) {
            if (tc2.equalsData(want)) {
                return tc2;
            }
        }
        this.times.add(want);
        want.setId(Integer.toString(this.tc_seqno));
        ++this.tc_seqno;
        return want;
    }
    
    private VertCoord getVertCoordinate(final String vert_id) {
        if (vert_id == null) {
            return null;
        }
        for (final VertCoord vc : this.vaxes) {
            if (vc.id.equals(vert_id)) {
                return vc;
            }
        }
        return null;
    }
    
    private VertCoord getVertCoordinate(final CoordinateAxis1D axis) {
        for (final VertCoord vc : this.vaxes) {
            if (vc.axis != null && vc.axis == axis) {
                return vc;
            }
        }
        final VertCoord want = new VertCoord(axis);
        for (final VertCoord vc2 : this.vaxes) {
            if (vc2.equalsData(want)) {
                return vc2;
            }
        }
        this.vaxes.add(want);
        want.setId(Integer.toString(this.vc_seqno));
        ++this.vc_seqno;
        return want;
    }
    
    private EnsCoord getEnsCoordinate(final String ens_id) {
        if (ens_id == null) {
            return null;
        }
        for (final EnsCoord ec : this.eaxes) {
            if (ec.id.equals(ens_id)) {
                return ec;
            }
        }
        return null;
    }
    
    private EnsCoord getEnsCoordinate(final CoordinateAxis1D axis, final int[] einfo) {
        for (final EnsCoord ec : this.eaxes) {
            if (ec.axis != null && ec.axis == axis) {
                return ec;
            }
        }
        final EnsCoord want = new EnsCoord(axis, einfo);
        for (final EnsCoord ec2 : this.eaxes) {
            if (ec2.equalsData(want)) {
                return ec2;
            }
        }
        this.eaxes.add(want);
        want.setId(Integer.toString(this.ec_seqno));
        ++this.ec_seqno;
        return want;
    }
    
    public static double getOffsetInHours(final Date origin, final Date date) {
        final double secs = (double)(date.getTime() / 1000L);
        final double origin_secs = (double)(origin.getTime() / 1000L);
        final double diff = secs - origin_secs;
        return diff / 3600.0;
    }
    
    public void writeXML(final String filename) throws IOException {
        final OutputStream out = new BufferedOutputStream(new FileOutputStream(filename));
        final XMLOutputter fmt = new XMLOutputter(Format.getPrettyFormat());
        fmt.output(this.writeDocument(), out);
        out.close();
    }
    
    public void writeXML(final OutputStream out) throws IOException {
        final XMLOutputter fmt = new XMLOutputter(Format.getPrettyFormat());
        fmt.output(this.writeDocument(), out);
    }
    
    public String writeXML() {
        final XMLOutputter fmt = new XMLOutputter(Format.getPrettyFormat());
        return fmt.outputString(this.writeDocument());
    }
    
    public Document writeDocument() {
        final Element rootElem = new Element("forecastModelRun");
        final Document doc = new Document(rootElem);
        rootElem.setAttribute("name", this.getName());
        rootElem.setAttribute("runTime", this.runTime);
        Collections.sort(this.eaxes);
        for (final EnsCoord ec : this.eaxes) {
            final Element ecElem = new Element("ensCoord");
            rootElem.addContent(ecElem);
            ecElem.setAttribute("id", ec.id);
            ecElem.setAttribute("name", ec.name);
            ecElem.setAttribute("product_definition", Integer.toString(ec.pdn));
            final StringBuilder sbuff = new StringBuilder();
            for (int j = 0; j < ec.ensTypes.length; ++j) {
                if (j > 0) {
                    sbuff.append(" ");
                }
                sbuff.append(Integer.toString(ec.ensTypes[j]));
            }
            ecElem.addContent(sbuff.toString());
        }
        Collections.sort(this.vaxes);
        for (final VertCoord vc : this.vaxes) {
            final Element vcElem = new Element("vertCoord");
            rootElem.addContent(vcElem);
            vcElem.setAttribute("id", vc.id);
            vcElem.setAttribute("name", vc.name);
            if (vc.units != null) {
                vcElem.setAttribute("units", vc.units);
            }
            final StringBuilder sbuff = new StringBuilder();
            for (int j = 0; j < vc.values1.length; ++j) {
                if (j > 0) {
                    sbuff.append(" ");
                }
                sbuff.append(Double.toString(vc.values1[j]));
                if (vc.values2 != null) {
                    sbuff.append(",");
                    sbuff.append(Double.toString(vc.values2[j]));
                }
            }
            vcElem.addContent(sbuff.toString());
        }
        for (final TimeCoord tc : this.times) {
            final Element offsetElem = new Element("offsetHours");
            rootElem.addContent(offsetElem);
            offsetElem.setAttribute("id", tc.id);
            final StringBuilder sbuff = new StringBuilder();
            for (int j = 0; j < tc.offset.length; ++j) {
                if (j > 0) {
                    sbuff.append(" ");
                }
                sbuff.append(Double.toString(tc.offset[j]));
            }
            offsetElem.addContent(sbuff.toString());
            Collections.sort((List<Comparable>)tc.vars);
            for (final Grid grid : tc.vars) {
                final Element varElem = new Element("variable");
                offsetElem.addContent(varElem);
                varElem.setAttribute("name", grid.name);
                if (grid.ec != null) {
                    varElem.setAttribute("ens_id", grid.ec.id);
                }
                if (grid.vc != null) {
                    varElem.setAttribute("vert_id", grid.vc.id);
                }
                if (grid.missing != null && grid.missing.size() > 0) {
                    final Element missingElem = new Element("missing");
                    varElem.addContent(missingElem);
                    sbuff.setLength(0);
                    for (int k = 0; k < grid.missing.size(); ++k) {
                        final Missing m = grid.missing.get(k);
                        if (k > 0) {
                            sbuff.append(" ");
                        }
                        sbuff.append(m.timeIndex);
                        if (grid.ec != null) {
                            sbuff.append(",");
                            sbuff.append(m.ensIndex);
                        }
                        if (grid.vc != null) {
                            sbuff.append(",");
                            sbuff.append(m.vertIndex);
                        }
                    }
                    missingElem.addContent(sbuff.toString());
                }
            }
            if (this.bb != null) {
                final Element bbElem = new Element("horizBB");
                rootElem.addContent(bbElem);
                final LatLonPoint llpt = this.bb.getLowerLeftPoint();
                final LatLonPoint urpt = this.bb.getUpperRightPoint();
                bbElem.setAttribute("west", ucar.unidata.util.Format.dfrac(llpt.getLongitude(), 3));
                bbElem.setAttribute("east", ucar.unidata.util.Format.dfrac(urpt.getLongitude(), 3));
                bbElem.setAttribute("south", ucar.unidata.util.Format.dfrac(llpt.getLatitude(), 3));
                bbElem.setAttribute("north", ucar.unidata.util.Format.dfrac(urpt.getLatitude(), 3));
            }
        }
        return doc;
    }
    
    public static ForecastModelRunInventory readXML(final String xmlLocation) throws IOException {
        if (ForecastModelRunInventory.debug) {
            System.out.println(" read from XML " + xmlLocation);
        }
        final InputStream is = new BufferedInputStream(new FileInputStream(xmlLocation));
        Document doc;
        try {
            final SAXBuilder builder = new SAXBuilder();
            doc = builder.build(is);
        }
        catch (JDOMException e) {
            throw new IOException(e.getMessage() + " reading from XML " + xmlLocation);
        }
        final Element rootElem = doc.getRootElement();
        final ForecastModelRunInventory fmr = new ForecastModelRunInventory();
        fmr.runTime = rootElem.getAttributeValue("runTime");
        final DateFormatter formatter = new DateFormatter();
        fmr.runDate = formatter.getISODate(fmr.runTime);
        final List<Element> eList = (List<Element>)rootElem.getChildren("ensCoord");
        for (final Element ensElem : eList) {
            final EnsCoord ec = new EnsCoord();
            fmr.eaxes.add(ec);
            ec.id = ensElem.getAttributeValue("id");
            ec.name = ensElem.getAttributeValue("name");
            ec.pdn = Integer.parseInt(ensElem.getAttributeValue("product_definition"));
            final String values = ensElem.getText();
            final StringTokenizer stoke = new StringTokenizer(values);
            ec.ensembles = stoke.countTokens();
            ec.ensTypes = new int[ec.ensembles];
            int count = 0;
            while (stoke.hasMoreTokens()) {
                final String toke = stoke.nextToken();
                final int pos = toke.indexOf(44);
                if (pos < 0) {
                    ec.ensTypes[count] = Integer.parseInt(toke);
                }
                ++count;
            }
        }
        final List<Element> vList = (List<Element>)rootElem.getChildren("vertCoord");
        for (final Element vertElem : vList) {
            final VertCoord vc = new VertCoord();
            fmr.vaxes.add(vc);
            vc.id = vertElem.getAttributeValue("id");
            vc.name = vertElem.getAttributeValue("name");
            vc.units = vertElem.getAttributeValue("units");
            final String values2 = vertElem.getText();
            final StringTokenizer stoke2 = new StringTokenizer(values2);
            final int n = stoke2.countTokens();
            vc.values1 = new double[n];
            int count2 = 0;
            while (stoke2.hasMoreTokens()) {
                final String toke2 = stoke2.nextToken();
                final int pos2 = toke2.indexOf(44);
                if (pos2 < 0) {
                    vc.values1[count2] = Double.parseDouble(toke2);
                }
                else {
                    if (vc.values2 == null) {
                        vc.values2 = new double[n];
                    }
                    final String val1 = toke2.substring(0, pos2);
                    final String val2 = toke2.substring(pos2 + 1);
                    vc.values1[count2] = Double.parseDouble(val1);
                    vc.values2[count2] = Double.parseDouble(val2);
                }
                ++count2;
            }
        }
        final List<Element> tList = (List<Element>)rootElem.getChildren("offsetHours");
        for (final Element timeElem : tList) {
            final TimeCoord tc = new TimeCoord();
            fmr.times.add(tc);
            tc.id = timeElem.getAttributeValue("id");
            String values3 = timeElem.getText();
            StringTokenizer stoke3 = new StringTokenizer(values3);
            final int n2 = stoke3.countTokens();
            tc.offset = new double[n2];
            int count3 = 0;
            while (stoke3.hasMoreTokens()) {
                tc.offset[count3++] = Double.parseDouble(stoke3.nextToken());
            }
            final List<Element> varList = (List<Element>)timeElem.getChildren("variable");
            for (final Element vElem : varList) {
                final Grid grid = new Grid(vElem.getAttributeValue("name"));
                grid.ec = fmr.getEnsCoordinate(vElem.getAttributeValue("ens_id"));
                grid.vc = fmr.getVertCoordinate(vElem.getAttributeValue("vert_id"));
                tc.vars.add(grid);
                grid.parent = tc;
                final List<Element> mList = (List<Element>)vElem.getChildren("missing");
                for (final Element mElem : mList) {
                    grid.missing = new ArrayList<Missing>();
                    values3 = mElem.getText();
                    stoke3 = new StringTokenizer(values3, " ,");
                    while (stoke3.hasMoreTokens()) {
                        final int timeIdx = Integer.parseInt(stoke3.nextToken());
                        int ensIdx = 0;
                        if (grid.ec != null) {
                            ensIdx = Integer.parseInt(stoke3.nextToken());
                        }
                        int vertIdx = 0;
                        if (grid.vc != null) {
                            vertIdx = Integer.parseInt(stoke3.nextToken());
                        }
                        grid.missing.add(new Missing(timeIdx, ensIdx, vertIdx));
                    }
                }
            }
        }
        final Element bbElem = rootElem.getChild("horizBB");
        if (bbElem != null) {
            final double west = Double.parseDouble(bbElem.getAttributeValue("west"));
            final double east = Double.parseDouble(bbElem.getAttributeValue("east"));
            final double north = Double.parseDouble(bbElem.getAttributeValue("north"));
            final double south = Double.parseDouble(bbElem.getAttributeValue("south"));
            fmr.bb = new LatLonRect(new LatLonPointImpl(south, west), new LatLonPointImpl(north, east));
        }
        return fmr;
    }
    
    public static ForecastModelRunInventory open(final DiskCache2 cache, final String ncfileLocation, final int mode, final boolean isFile) throws IOException {
        final boolean force = mode == 2;
        final boolean xml_only = mode == 3;
        String summaryFileLocation = ncfileLocation + ".fmrInv.xml";
        File summaryFile = new File(summaryFileLocation);
        if (!summaryFile.exists() && null != cache) {
            summaryFile = cache.getCacheFile(summaryFileLocation);
            summaryFileLocation = summaryFile.getPath();
        }
        final boolean haveOne = summaryFile != null && summaryFile.exists();
        if (xml_only && !haveOne) {
            return null;
        }
        if (!force && haveOne) {
            if (isFile) {
                final File ncdFile = new File(ncfileLocation);
                if (!ncdFile.exists()) {
                    throw new IllegalArgumentException("Data File must exist = " + ncfileLocation);
                }
                Label_0232: {
                    if (!xml_only) {
                        if (summaryFile.lastModified() < ncdFile.lastModified()) {
                            break Label_0232;
                        }
                    }
                    try {
                        return readXML(summaryFileLocation);
                    }
                    catch (Exception ee) {
                        ForecastModelRunInventory.log.error("Failed to read FmrcInventory " + summaryFileLocation, ee);
                    }
                }
            }
            else {
                try {
                    return readXML(summaryFileLocation);
                }
                catch (Exception ee2) {
                    ForecastModelRunInventory.log.error("Failed to read FmrcInventory " + summaryFileLocation, ee2);
                }
            }
        }
        if (ForecastModelRunInventory.debug) {
            System.out.println(" read from dataset " + ncfileLocation + " write to XML " + summaryFileLocation);
        }
        ucar.nc2.dt.grid.GridDataset gds = null;
        ForecastModelRunInventory fmr = null;
        try {
            gds = ucar.nc2.dt.grid.GridDataset.open(ncfileLocation);
            fmr = new ForecastModelRunInventory(gds, null);
        }
        catch (IOException ioe) {
            if (ForecastModelRunInventory.debug) {
                ioe.printStackTrace();
            }
            return null;
        }
        finally {
            if (gds != null) {
                gds.close();
            }
        }
        if (summaryFileLocation != null) {
            try {
                fmr.writeXML(summaryFileLocation);
            }
            catch (Throwable t) {
                ForecastModelRunInventory.log.error("Failed to write FmrcInventory to " + summaryFileLocation, t);
            }
        }
        if (ForecastModelRunInventory.showXML) {
            IO.copyFile(summaryFileLocation, System.out);
        }
        fmr.releaseDataset();
        return fmr;
    }
    
    public static ForecastModelRunInventory open(final GridDataset gds, final Date runDate) {
        return new ForecastModelRunInventory(gds, runDate);
    }
    
    public static void main2(final String[] args) throws Exception {
        final String def = "R:/testdata/motherlode/grid/NAM_CONUS_80km_20060728_1200.grib1";
        final String datasetName = (args.length < 1) ? def : args[0];
        final ForecastModelRunInventory fmr = open(null, datasetName, 2, true);
        fmr.writeXML(System.out);
    }
    
    public static void main(final String[] args) throws IOException {
        if (args.length == 1) {
            open(null, args[0], 2, true);
            readXML(args[0] + ".fmrInv.xml");
            return;
        }
        final DiskCache2 cache = new DiskCache2("fmrcInventory/", true, 432000, 3600);
        final String url = "http://motherlode.ucar.edu:9080/thredds/dodsC/fmrc/NCEP/NAM/CONUS_12km/files/NAM_CONUS_12km_20070419_1800.grib2";
        final ForecastModelRunInventory fmr = open(cache, url, 1, false);
        fmr.writeXML(System.out);
    }
    
    static {
        ForecastModelRunInventory.log = LoggerFactory.getLogger(ForecastModelRunInventory.class);
        ForecastModelRunInventory.debug = false;
        ForecastModelRunInventory.showXML = false;
    }
    
    public static class TimeCoord implements FmrcCoordSys.TimeCoord, Comparable
    {
        private CoordinateAxis1D axis;
        private List<Grid> vars;
        private String id;
        private double[] offset;
        
        TimeCoord() {
            this.vars = new ArrayList<Grid>();
        }
        
        TimeCoord(final int num, final TimeCoord from) {
            this.vars = new ArrayList<Grid>();
            this.id = Integer.toString(num);
            this.offset = from.offset;
        }
        
        TimeCoord(final Date runDate, final CoordinateAxis1D axis) {
            this.vars = new ArrayList<Grid>();
            this.axis = axis;
            DateUnit unit = null;
            try {
                unit = new DateUnit(axis.getUnitsString());
            }
            catch (Exception e) {
                throw new IllegalArgumentException("Not a unit of time " + axis.getUnitsString());
            }
            final int n = (int)axis.getSize();
            this.offset = new double[n];
            for (int i = 0; i < axis.getSize(); ++i) {
                final Date d = unit.makeDate(axis.getCoordValue(i));
                this.offset[i] = ForecastModelRunInventory.getOffsetInHours(runDate, d);
            }
        }
        
        public List<Grid> getGrids() {
            return this.vars;
        }
        
        public String getId() {
            return this.id;
        }
        
        public void setId(final String id) {
            this.id = id;
        }
        
        public String getName() {
            return this.id.equals("0") ? "time" : ("time" + this.id);
        }
        
        public double[] getOffsetHours() {
            return this.offset;
        }
        
        public void setOffsetHours(final double[] offset) {
            this.offset = offset;
        }
        
        public boolean equalsData(final TimeCoord tother) {
            if (this.offset.length != tother.offset.length) {
                return false;
            }
            for (int i = 0; i < this.offset.length; ++i) {
                if (!Misc.closeEnough(this.offset[i], tother.offset[i])) {
                    return false;
                }
            }
            return true;
        }
        
        int findIndex(final double offsetHour) {
            for (int i = 0; i < this.offset.length; ++i) {
                if (this.offset[i] == offsetHour) {
                    return i;
                }
            }
            return -1;
        }
        
        public int compareTo(final Object o) {
            final TimeCoord ot = (TimeCoord)o;
            return this.id.compareTo(ot.id);
        }
    }
    
    public static class Grid implements Comparable
    {
        String name;
        TimeCoord parent;
        EnsCoord ec;
        VertCoord vc;
        List<Missing> missing;
        
        Grid(final String name) {
            this.parent = null;
            this.ec = null;
            this.vc = null;
            this.name = name;
        }
        
        public int compareTo(final Object o) {
            final Grid other = (Grid)o;
            return this.name.compareTo(other.name);
        }
        
        public int countInventory() {
            return this.countTotal() - this.countMissing();
        }
        
        public int countTotal() {
            final int ntimes = this.parent.getOffsetHours().length;
            return ntimes * this.getVertCoordLength();
        }
        
        public int countMissing() {
            return (this.missing == null) ? 0 : this.missing.size();
        }
        
        int getVertCoordLength() {
            return (this.vc == null) ? 1 : this.vc.getValues1().length;
        }
        
        public int countInventory(final double hourOffset) {
            final int timeIndex = this.parent.findIndex(hourOffset);
            if (timeIndex < 0) {
                return 0;
            }
            if (this.missing == null) {
                return this.getVertCoordLength();
            }
            int count = 0;
            for (final Missing m : this.missing) {
                if (m.timeIndex == timeIndex) {
                    ++count;
                }
            }
            return this.getVertCoordLength() - count;
        }
        
        public double[] getVertCoords(final double hourOffset) {
            final int timeIndex = this.parent.findIndex(hourOffset);
            if (timeIndex < 0) {
                return new double[0];
            }
            if (this.vc == null) {
                final double[] result = { -0.0 };
                return result;
            }
            final double[] result = this.vc.getValues1().clone();
            if (null != this.missing) {
                for (final Missing m : this.missing) {
                    if (m.timeIndex == timeIndex) {
                        result[m.vertIndex] = Double.NaN;
                    }
                }
            }
            return result;
        }
    }
    
    public static class Missing
    {
        int timeIndex;
        int ensIndex;
        int vertIndex;
        
        Missing(final int timeIndex, final int ensIndex, final int vertIndex) {
            this.timeIndex = timeIndex;
            this.ensIndex = ensIndex;
            this.vertIndex = vertIndex;
        }
    }
    
    public static class VertCoord implements FmrcCoordSys.VertCoord, Comparable
    {
        CoordinateAxis1D axis;
        private String name;
        private String units;
        private String id;
        double[] values1;
        double[] values2;
        
        VertCoord() {
        }
        
        VertCoord(final CoordinateAxis1D axis) {
            this.axis = axis;
            this.name = axis.getName();
            this.units = axis.getUnitsString();
            final int n = (int)axis.getSize();
            if (axis.isInterval()) {
                this.values1 = axis.getBound1();
                this.values2 = axis.getBound2();
            }
            else {
                this.values1 = new double[n];
                for (int i = 0; i < axis.getSize(); ++i) {
                    this.values1[i] = axis.getCoordValue(i);
                }
            }
        }
        
        VertCoord(final VertCoord vc) {
            this.name = vc.getName();
            this.units = vc.getUnits();
            this.id = vc.getId();
            this.values1 = vc.getValues1().clone();
            this.values2 = (double[])((vc.getValues2() == null) ? null : ((double[])vc.getValues2().clone()));
        }
        
        public String getId() {
            return this.id;
        }
        
        public void setId(final String id) {
            this.id = id;
        }
        
        public String getName() {
            return this.name;
        }
        
        public void setName(final String name) {
            this.name = name;
        }
        
        public String getUnits() {
            return this.units;
        }
        
        public void setUnits(final String units) {
            this.units = units;
        }
        
        public double[] getValues1() {
            return this.values1;
        }
        
        public void setValues1(final double[] values) {
            this.values1 = values;
        }
        
        public double[] getValues2() {
            return this.values2;
        }
        
        public void setValues2(final double[] values) {
            this.values2 = values;
        }
        
        public int getSize() {
            return this.values1.length;
        }
        
        public boolean equalsData(final VertCoord other) {
            if (this.values1.length != other.values1.length) {
                return false;
            }
            for (int i = 0; i < this.values1.length; ++i) {
                if (!Misc.closeEnough(this.values1[i], other.values1[i])) {
                    return false;
                }
            }
            if (this.values2 == null && other.values2 == null) {
                return true;
            }
            if (this.values2 == null || other.values2 == null) {
                return false;
            }
            if (this.values2.length != other.values2.length) {
                return false;
            }
            for (int i = 0; i < this.values2.length; ++i) {
                if (!Misc.closeEnough(this.values2[i], other.values2[i])) {
                    return false;
                }
            }
            return true;
        }
        
        public int compareTo(final Object o) {
            final VertCoord other = (VertCoord)o;
            return this.name.compareTo(other.name);
        }
    }
    
    public static class EnsCoord implements FmrcCoordSys.EnsCoord, Comparable
    {
        CoordinateAxis1D axis;
        private String name;
        private String id;
        private int ensembles;
        private int pdn;
        private int[] ensTypes;
        
        EnsCoord() {
        }
        
        EnsCoord(final CoordinateAxis1D axis, final int[] einfo) {
            this.axis = axis;
            this.name = axis.getName();
            this.ensembles = einfo[0];
            this.pdn = einfo[1];
            System.arraycopy(einfo, 2, this.ensTypes = new int[this.ensembles], 0, this.ensembles);
        }
        
        EnsCoord(final EnsCoord ec) {
            this.name = ec.getName();
            this.id = ec.getId();
            this.ensembles = ec.getNEnsembles();
            this.pdn = ec.getPDN();
            this.ensTypes = ec.getEnsTypes().clone();
        }
        
        public String getId() {
            return this.id;
        }
        
        public void setId(final String id) {
            this.id = id;
        }
        
        public String getName() {
            return this.name;
        }
        
        public void setName(final String name) {
            this.name = name;
        }
        
        public int getNEnsembles() {
            return this.ensembles;
        }
        
        public void setNEnsembles(final int ensembles) {
            this.ensembles = ensembles;
        }
        
        public int getPDN() {
            return this.pdn;
        }
        
        public void setPDN(final int pdn) {
            this.pdn = pdn;
        }
        
        public int[] getEnsTypes() {
            return this.ensTypes;
        }
        
        public void setEnsTypes(final int[] ensTypes) {
            this.ensTypes = ensTypes;
        }
        
        public int getSize() {
            return this.ensembles;
        }
        
        public boolean equalsData(final EnsCoord other) {
            if (this.ensembles != other.ensembles) {
                return false;
            }
            if (this.pdn != other.pdn) {
                return false;
            }
            for (int i = 0; i < this.ensTypes.length; ++i) {
                if (this.ensTypes[i] != other.ensTypes[i]) {
                    return false;
                }
            }
            return true;
        }
        
        public int compareTo(final Object o) {
            final EnsCoord other = (EnsCoord)o;
            return this.name.compareTo(other.name);
        }
    }
}
